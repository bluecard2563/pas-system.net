<?php
/*
File: pForgotPasswordAjax.php
Author: Santiago Borja Lopez
Creation Date: 01/05/2010 10:00
*/
	set_time_limit(36000);
	
	Request::setString('email');
	
	$user = new User($db);
	$user -> setEmail_usr($email);
	$user -> getData(true);
	if($user -> existsUserEmail($email,NULL)){
		$password = GlobalFunctions::generatePassword(6, 8, false);
		$user -> setPassword_usr(md5($password));

		$misc['fullname'] = $user -> getFirstname_usr() . ' ' . $user -> getLastname_usr();
		$misc['username'] = $user -> getUsername_usr();
	    $misc['pswd'] = $password;
	    $misc['email'] = $user -> getEmail_usr();
	    if(!GlobalFunctions::sendMail($misc, 'forgotPassword')){
	    	$error = 2;
	    }
		$user -> update();
		echo 'ok';return;
	}
	
	echo 'badEmail';return;
?>