<?php
if ($user->getStatus_usr() == 'ACTIVE') {

	if ($_POST['txtSystemPassword'] == $user->getPassword_usr()) {
		//THE USER IDENTITY HAS BEEN VALIDATED
		$_SESSION['ds_serial_usr'] = $user->getSerial_usr();
		$_SESSION['ds_serial_pbc'] = $user->getSerial_pbc();
		if ($user->getSerial_mbc()) {
			$_SESSION['ds_serial_mbc'] = $user->getSerial_mbc();
		} else if ($user->getSerial_dea()) {
			$dealer = new Dealer($db, $user->getSerial_dea());
			$dealer->getData();
			$_SESSION['ds_serial_mbc'] = $dealer->getSerial_mbc();
			//User's branch-serial
			$_SESSION['ds_serial_dea'] = $dealer->getSerial_dea();
			//User's dealer-serial
			$_SESSION['ds_dea_serial_dea'] = $dealer->getDea_serial_dea();
		}
		$_SESSION['ds_user_name'] = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
		$_SESSION['ds_user_type'] = $user->getBelongsto_usr();
		$_SESSION['ds_sellFree'] = $user->getSellFree_usr();
		$_SESSION['ds_cityForReports'] = $user->getCityLoggedUser();
		$_SESSION['ds_firstAccess'] = $user->getFirstAccess_usr();

		$ubc = new UserbyCity($db);
		$aux = $ubc->getCitiesByUser($user->getSerial_usr());
		if ($aux) {
			$_SESSION['cityList'] = implode(',', $aux);

			$city = new City($db);
			$auxC = $city->getOwnCountriesByCity($_SESSION['cityList']);
			if ($auxC) {
				$_SESSION['countryList'] = implode(',', $auxC);
			}
		}

		/* GET SECURITY CONTROLS */
		$_SESSION['ds_suitable_pages'] = GlobalFunctions::getAvailablePagesForProfile($db, $user->getSerial_pbc());
		/* END SECURITY CONTROLS */

		/* REGISTER LOGIN AND SEND MAIL NOTIFICATION */
		$loginControl = new LoginControl($db);
		$login_ip = GlobalFunctions::getCurrentIp();
		$loginControl->setIp_lgn($login_ip);
		$loginControl->setSerial_usr($user->getSerial_usr());
		$serial_lng = $loginControl->insert();
		if ($serial_lng) {
			$_SESSION['ds_serial_lgn'] = $serial_lng;
			// CALL SEND MAIL FUNCTION FOR LOGIN REGISTER
			$misc['serial_lgn'] = $serial_lng;
			//GlobalFunctions::sendMail($misc, 'login');
		}
		/* END REGISTER LOGIN */

		http_redirect('main');
	} else {
		$error = 1;
	}
} else {
	$error = 5;
}
?>
