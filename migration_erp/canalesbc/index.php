<?php 
if(isset($_SESSION[$portal_prefix.'serial_usr'])){
	/*END REGISTER LOGOUT*/
    unset($_SESSION[$portal_prefix.'serial_usr']);
    unset($_SESSION[$portal_prefix.'serial_pbc']);
    unset($_SESSION[$portal_prefix.'user_name']);
    unset($_SESSION['cityList']);
    unset($_SESSION['countryList']);
	unset($_SESSION[$portal_prefix.'user_type']);
	unset($_SESSION[$portal_prefix.'sellFree']);
	unset($_SESSION[$portal_prefix.'cityForReports']);
	unset($_SESSION[$portal_prefix.'serial_mbc']);
	unset($_SESSION[$portal_prefix.'serial_dea']);
	unset($_SESSION[$portal_prefix.'dea_serial_dea']);
	unset($_SESSION[$portal_prefix.'suitable_pages']);
	unset($_SESSION[$portal_prefix.'serial_customer_estimator']);
	unset($_SESSION[$portal_prefix.'system_estimator_shopping_cart_products']);
	unset($_SESSION[$portal_prefix.'firstAccess']);
	unset($_SESSION[$portal_prefix.'serial_lgn']);
}

Request::setInteger('0:error');
global $systemToken;

$_SESSION[$portal_prefix.'serial_lang'] = '2';
if($_POST['txtSystemUser']){
	//Security check
	if(!$_POST['systemToken'] || $_POST['systemToken'] != $systemToken){
		http_redirect('index/9');
	}
	
	if(isset($_POST['autoLogin']) && $_POST['autoLogin'] == '1'){
		$_POST['txtSystemPassword'] = md5(mysql_real_escape_string($_POST['txtSystemPassword']));
	}
	
	//HOST NAME CHECK:
	$host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
	$host = str_replace("www.", "", $host);

	$allowedReferers = array();
	array_push($allowedReferers, 'localhost'); //TODO REMOVE ON AIR!!!
	
	if(!in_array($host, $allowedReferers)){
		//http_redirect('index/9');
	}
	
	$user=new User($db);
	$user -> setUsername_usr(mysql_real_escape_string($_POST['txtSystemUser']));
	if($user->getDataLogin()){
		require_once('pLoginValidationProcess.php');
	}else{
		$error = 2;
	}
}

$smarty->register('error,systemToken');
$smarty->assign('container','index');
$smarty->display('index.'.$_SESSION['language'].'.tpl');
?>