<?php
/*
File: InsertGeneralCities
Author: David Bergmann
Creation Date: 28/07/2010
Last Modified:
Modified By:
*/


function getCountries($db){
	$sql="SELECT serial_cou, name_cou
		  FROM country
		  WHERE serial_cou NOT IN (SELECT cou.serial_cou
								   FROM country cou
								   JOIN city cit ON cit.serial_cou = cou.serial_cou
								   WHERE cit.name_cit LIKE 'General')";
	$result = $db -> Execute($sql);
	$num = $result -> RecordCount();

	if($num > 0){
		$cont=0;
		do{
			$arreglo[$cont]=$result->GetRowAssoc(false);
			$result->MoveNext();
			$cont++;
		}while($cont<$num);
	}
	return $arreglo;
}

function insert($db,$serial_cou) {
	$sql = "INSERT INTO city(serial_cou, name_cit, code_cit) VALUES ('$serial_cou','General','GC')";
	$result = $db->Execute($sql);
		if ($result==true)
			return true;
		else
			return false;
}

$countries = getCountries($db);
if(is_array($countries)) {
	foreach($countries as $c) {
		if(insert($db, $c['serial_cou'])){
			echo "Ciudad 'General' insertada en pa&iacute;s ".$c['name_cou'].".<br/>";
		}
	}
} else {
	echo "Todos los pa&iacute;ses tienen ciudad 'General'.";
}
?>