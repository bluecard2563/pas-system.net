<?php

Request::setInteger('0:error');

/*Select all assistance bill with null type_prq*/
	$sql= "SELECT prq.serial_prq, dbf.name_dbf, dbf.comment_dbf, prq.amount_reclaimed_prq, prq.amount_authorized_prq, prq.request_date_prq, prq.status_prq
			FROM payment_request prq
			JOIN documents_by_file dbf ON prq.serial_dbf=dbf.serial_dbf
			WHERE prq.type_prq is NULL";

	$result = $db -> getAll($sql);
	$assistance_bill_list=$result;
	
	$type_prq_list=PaymentRequest::getPaymentRequestTypes($db);

$smarty->register('type_prq_list,assistance_bill_list,error');
$smarty->display('fAssistanceBillList.tpl');

?>
