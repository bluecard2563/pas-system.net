<?php
/*
    Document   : laodAssistGroupByCountry.rpc.php
    Created on : Apr 26, 2010, 12:22:07 PM
    Author     : H3Dur4k
    Description:
        Get all the assistance groups based on a country (serial_cou).
*/
Request::setInteger('serial_cou');
$assistanceGroup=new AssistanceGroup($db);
$assistanceGroup->setSerial_cou($serial_cou);
$groupList=$assistanceGroup->getGroupsByCountry();
$smarty->assign('groupList',$groupList);
$smarty->assign('container','none');
$smarty->display('helpers/loadsAssistanceGroups/loadAssistGroupByCountry.'.$_SESSION['language'].'.tpl');
?>
