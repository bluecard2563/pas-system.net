<?php
/*
    Document   : loadUsersForGroups.rpc.php
    Created on : Apr 26, 2010, 5:58:30 PM
    Author     : H3Dur4k
    Description:
       Retrieve users available for assistance group..
*/
Request::setString('type');
Request::setString('selUsers');
Request::setInteger('serial_dea');
Request::setInteger('serial_mbc');

$user = new User($db);

if($type=='planet'){
	$userList=$user->getMainManagerUsers($selUsers,true);
}elseif($type=='dealer'){
	$userList=$user->getUsersByDealer($serial_dea,$selUsers,true);
}else{//Manager
	$userList=$user->getUsersByManagerForAssistances($serial_mbc, $selUsers);
}

$smarty->assign('container','none');
$smarty->register('userList');
$smarty->assign('type',$type);
$smarty->assign('serial_dea',$serial_dea);
$smarty->assign('serial_mbc',$serial_mbc);
$smarty->display('helpers/loadsAssistanceGroups/loadUsersForGroups.'.$_SESSION['language'].'.tpl');
?>
