<?php
/*
    Document   : userToAvailablerpc.php
    Created on : Apr 29, 2010, 9:14:56 AM
    Author     : H3Dur4k
    Description:
        change a user , sets in_assistance_group to NO and to INACTIVE in the group where he actually is.
 *		It is only for OPERATORS
*/
Request::setInteger('serial_usr');
$error=false;
if(!$serial_usr){
	$error=true;
}else{
	$user=new User($db,$serial_usr);
	$user->getData();
	$user->setIn_assistance_group_usr('NO');
	if($user->update()){
		$assistanceGroupUser=new AssistanceGroupsByUsers($db);
		$assistanceGroupUser->setSerial_usr($serial_usr);
		$serial_asg=$assistanceGroupUser->getCurrentUserGroup();
		$assistanceGroupUser->setSerial_asg($serial_asg);
		$assistanceGroupUser->setStatus_agu('INACTIVE');
		$assistanceGroupUser->setType_agu('OPERATOR');
		if(!$assistanceGroupUser->update()){
			$error=true;
		}
	}else{
		$error=true;
	}
}
if($error){
	echo json_encode(false);
}else{
	echo json_encode(true);
}

?>
