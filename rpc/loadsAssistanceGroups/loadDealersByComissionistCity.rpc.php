<?php
/*
    Document   : loadDealersByComissionistCity.rpc.php
    Created on : Apr 27, 2010, 2:05:21 PM
    Author     : H3Dur4k
    Description:
        load all dealers by city and comissionist.
*/
$dealer=new Dealer($db);
$dealerList=array();
if($_POST['serial_cit']){
	$params=array('0'=>array('field'=> 'c.serial_cit',
							  'value'=> $_POST['serial_cit'],
							  'like'=> 1),
				   '1'=>array('field'=> 'd.dea_serial_dea',
							  'value'=> 'NULL',
							  'like'=> 1),
				   '2'=>array('field'=> 'd.status_dea',
							  'value'=> 'ACTIVE',
							  'like'=> 1),
				   '3'=>array('field'=>'dtl.serial_lang',
							  'value'=> $_SESSION['serial_lang'],
							  'like'=> 1),
				   '4'=>array('field'=>'ubd.serial_usr',
							  'value'=> $_POST['serial_usr'],
							  'like'=> 1),
				   '5'=>array('field'=>'d.phone_sales_dea',
									   'value'=> 'NO',
									   'like'=> 1)
		);
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}

	$dealerList=$dealer->getDealers($params);
}
if(is_array($dealerList)){
	foreach($dealerList as &$dl){
		$dl['name_dea']=utf8_encode($dl['name_dea']);
	}
}
$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadsAssistanceGroups/loadDealersByComissionistCity.'.$_SESSION['language'].'.tpl');
?>