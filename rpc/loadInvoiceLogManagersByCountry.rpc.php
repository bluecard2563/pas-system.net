<?php
/*
File: loadInvoiceManagersByCountry.rpc.php
Author: Santiago Benitez
Creation Date: 06/04/2010 13:38
Last Modified: 06/04/2010 13:38
Modified By: SSantiago Benitez
Description: File which loads all the managers of a country that have pending invoices
*/

	Request::setString('serial_cou');
	//UPDATE OVER
	Request::setString('manManager');

	//MANAGERS
	$managerBC = new ManagerbyCountry($db);
	if($serial_cou){
	    $managerList=$managerBC->getInvoiceLogManagersByCountry($serial_cou, $_SESSION['serial_mbc']);
	}

	$smarty->register('managerList,serial_cou,manManager');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>