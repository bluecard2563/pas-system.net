<?php
/*
File: loadProductsByCountry.rpc
Author: David Bergmann
Creation Date: 20/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');
Request::setString('serial_dea');

$productByCountry = new ProductByCountry($db);
$products = $productByCountry->getProductsByCountry($serial_cou, $_SESSION['serial_lang'], "ACTIVE", $serial_dea);

if(is_array($products)) {
	foreach($products as &$p) {
		$p['name_pbl'] = utf8_encode($p['name_pbl']);
	}
}

$smarty->register('products');
$smarty->assign('container','none');
$smarty->display('helpers/loadProductsByCountry.'.$_SESSION['language'].'.tpl');
?>