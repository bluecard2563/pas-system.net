<?php 
/*
File: loadManagerData.rpc.php
Author: Santiago Benítez
Creation Date: 09/02/2010 14:02
Last Modified: 09/02/2010 14:02
Modified By: Santiago Benítez
Description: File which loads all managers from an specific user
*/
Request::setString('serial_user');
$manager=new ManagerbyCountry($db);
$hiddensList=$manager->getSerialCountriesByUser($serial_user);
$smarty->register('hiddensList');
$smarty->assign('container','none');
$smarty->display('helpers/loadManagerHiddens.'.$_SESSION['language'].'.tpl');
?>