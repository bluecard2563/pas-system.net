<?php 
/*
File: loadBranchesByDealer.php
Author: Gabriela Guerrero
Creation Date: 24/06/2010
Last Modified: 
Modified By: 
*/

$dea_serial_dea=$_POST['dea_serial_dea'];
$repors=$_POST['reports'];
Request::setString('reports');

$branch = new Dealer($db);

//If $_SESSION['serial_dea'] is set, loads only the user's branch
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$branchList = $branch->getBranchByDealer($dea_serial_dea,$_SESSION['serial_dea']);
} else {
	$branchList = $branch->getBranchByDealer($dea_serial_dea);
}

if(User::isSubManagerUser($db, $_SESSION['serial_usr'])):
	$dealer = new Dealer($db, $_SESSION['serial_dea']);
	$dealer -> getData();
	
	$branchList = Dealer::getAllBranchesByDealers($db, $dealer->getDea_serial_dea());
endif;

$smarty->register('branchList,reports,dea_serial_dea');
$smarty->assign('container','none');
$smarty->display('helpers/loadBranchesByDealer.'.$_SESSION['language'].'.tpl');
?>