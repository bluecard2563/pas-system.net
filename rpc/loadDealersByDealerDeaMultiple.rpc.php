<?php
/*
File: loadDealersByCityMultiple.rpc.php
Author: Santiago Borja
Creation Date: 17/03/2010
*/

$dealer=new Dealer($db);
if($_POST['serial_dea']){
    $count = $_POST['count'];
    $params=array(
                '0'=>array('field'=> 'd.dea_serial_dea',
                           'value'=> $_POST['serial_dea'],
                           'like'=> 1),
                '1'=>array('field'=> 'd.status_dea',
                           'value'=> 'ACTIVE',
                           'like'=> 1)
    );
    if($_POST['serial_dealers']){
        array_push($params,array('field'=> 'd.serial_dea',
                                      'value'=> explode(',', $_POST['serial_dealers']),
                                      'like'=> 0));
    }
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
	if($_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_mbc',
				  'value'=> $_SESSION['serial_mbc'],
				  'like'=> 1);
		array_push($params,$aux);
	}

    $dealerList=$dealer->getDealers($params);
}else{
    echo 'Seleccione una ciudad';
}

if(is_array($dealerList)){
    echo '<div align="center">Existentes</div><select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" dealer="'.$_POST['serial_dea'].'">';
    foreach($dealerList as &$dl){
	$dl['name_dea']=utf8_encode($dl['name_dea']);
        echo '<option value="'.$dl['serial_dea'].'" dealer="'.$_POST['serial_dea'].'">'.$dl['name_dea'].'</option>';
    }
    echo '</select>';
}else{
    echo '<br><select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" dealer="'.$_POST['serial_dea'].'" style="display:none;">';
    echo '</select>';
    echo 'No existen sucursales en el Comercializador escogido';
}
?>