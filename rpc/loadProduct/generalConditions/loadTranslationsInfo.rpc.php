<?php
/*
 * File: loadTranslationsInfo.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 31-ago-2010, 12:02:49
 * Modified By: Patricio Astudillo
 */

Request::setString('serial_gcn');
Request::setString('serial_lang');

$gconditions_list=GConditionByLanguageByCountry::getTranslationsInfo($db, $serial_gcn, $serial_lang);

$smarty->register('gconditions_list');
$smarty->assign('container','none');
$smarty->display('helpers/loadProduct/generalConditions/loadTranslationsInfo.'.$_SESSION['language'].".tpl");
?>
