<?php
/*
 * File: loadRemainingLanguages.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 31-ago-2010, 16:43:29
 * Modified By: Patricio Astudillo
 */

Request::setString('serial_glc');
Request::setString('serial_gcn');

$glc=new GConditionByLanguageByCountry($db, $serial_glc);
$glc->getData();

$remainingLanguages=GConditionByLanguageByCountry::getRemainingLanguagesToTranslate($db, $glc->getSerial_cou(), $serial_gcn);

$smarty->register('remainingLanguages');
$smarty->assign('container','none');
$smarty->display('helpers/loadProduct/generalConditions/loadRemainingLanguages.'.$_SESSION['language'].'.tpl');
?>
