<?php
/*
 * File: loadProductsToAssign.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 03-ago-2010, 16:36:46
 * Modified By: Patricio Astudillo
 */

Request::setString('dealer_number');
Request::setString('serials');
Request::setString('serial_cou');

if($dealer_number==1){ //GET A PRODUCT LIST WITH THE ONES ALREADY ASIGNED, WITH A CHECKED FLAG
	$productList=ProductByCountry::getProductsByCountryWithAsignedFlag($db, $serial_cou, $serials);
}else{ //GET A PRODUCT LIST AVAILABLE FOR ALL THE DEALERS IN A THE $serials VAR.
	$productList=ProductByCountry::getProductsByCountryAvailableForMassiveAsign($db, $serial_cou, $serials);
}

if(count($productList)>0){
	$items=count($productList);
	$pages=(int)($items/7)+1;
	$smarty->register('pages');
}

$smarty->register('productList');
$smarty->assign('container','none');
$smarty->display('helpers/loadProduct/loadProductsToAssign.'.$_SESSION['language'].'.tpl');
?>