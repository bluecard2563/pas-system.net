<?php
/**
 * File: loadExistingPConditions
 * Author: Patricio Astudillo
 * Creation Date: 03-sep-2012
 * Last Modified: 03-sep-2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_prc');
	Request::setString('serial_cou');
	Request::setString('serial_pro');

	$serial_pxc = ProductByCountry::getPxCSerial($db, $serial_cou, $serial_pro);
	
	$pconditions_list = ParticularConditions::getPConditionsForProduct($db, $serial_pxc, $_SESSION['serial_lang'], 'ALL');
	
	$smarty->register('pconditions_list');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/loadProduct/particularConditions/loadExistingPConditions.'.$_SESSION['language'].'.tpl');
?>
