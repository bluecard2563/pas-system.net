<?php
/**
 * File: modifyPCondition
 * Author: Patricio Astudillo
 * Creation Date: 04-sep-2012
 * Last Modified: 04-sep-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('serial_pcp');
	Request::setString('status_to');
	
	$pcp = new PConditionsByProductCountry($db, $serial_pcp);
	
	if($pcp ->getData()){
		$pcp ->setStatus_pcp($status_to);
		
		if($pcp->update()){
			echo json_encode('success');
		}else{
			echo json_encode('error_update');
		}
	}else{
		echo json_encode('error_load');
	}
?>
