<?php
/**
 * File: translationSection
 * Author: Patricio Astudillo
 * Creation Date: 05-sep-2012
 * Last Modified: 05-sep-2012
 * Modified By: Patricio Astudillo
 */
	
	Request::setString('serial_lang');
	Request::setString('serial_prc');

	$serial_prc_translation = ParticularConditions::translationExist($db, $serial_lang, $serial_prc);
	
	if($serial_prc_translation){
		$translation = new ParticularConditions($db, $serial_prc_translation);
		$translation ->getData();
		
		$translation = get_object_vars($translation);
		unset($translation['db']);
		
		$smarty->register('translation');
	}
	
	$smarty->register('serial_lang,serial_prc,serial_prc_translation');
	$smarty->assign('container', 'none');
	$smarty ->display('helpers/loadProduct/particularConditions/translationSection.'.$_SESSION['language'].'.tpl');
?>
