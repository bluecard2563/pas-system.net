<?php
/*
 * File: loadDealersForAssingProducts.php
 * Author: Patricio Astudillo
 * Creation Date: 03-ago-2010, 15:04:20
 * Modified By: Patricio Astudillo
 */

Request::setString('serial_mbc');
Request::setString('serial_cit');
Request::setString('serial_usr');

$dealer=new Dealer($db);
$params=array();
$aux=array('field'=>'d.status_dea',
		   'value'=> 'ACTIVE',
		   'like'=> 1);
array_push($params, $aux);

$aux=array('field'=>'d.serial_mbc',
		   'value'=> $serial_mbc,
		   'like'=> 1);
array_push($params, $aux);

$aux=array('field'=>'d.dea_serial_dea',
		   'value'=> 'NULL',
		   'like'=> 1);
array_push($params, $aux);

$aux=array('field'=>'d.phone_sales_dea',
		   'value'=> 'NO',
		   'like'=> 1);
array_push($params, $aux);

$aux=array('field'=>'c.serial_cit',
		   'value'=> $serial_cit,
		   'like'=> 1);
array_push($params, $aux);

if($serial_usr) {
	$aux = array('field'=>'ubd.serial_usr',
			   'value'=> $serial_usr,
			   'like'=> 1);
	array_push($params, $aux);
}

$dealerList = $dealer -> getDealers($params);

$items=count($dealerList);
if($items>0){
	$pages=(int)($items/7)+1;
}

$smarty->register('dealerList,pages');
$smarty->assign('container','none');
$smarty->display('helpers/loadProduct/loadDealersForAssingProducts.'.$_SESSION['language'].'.tpl');
?>