<?php
/*
File: loadCreditNoteCities.rpc.php
Author: Santiago Benitez
Creation Date: 17/05/2010
Last Modified:
Modified By: 
*/

$city=new City($db);
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}
if($_POST['serial_cou']){
	$serial_cou=$_POST['serial_cou'];
    $cityList=$city->getCreditNoteCitiesByCountry($_POST['serial_cou'],$_SESSION['serial_mbc'], $_SESSION['cityList']);
}

if(is_array($cityList)){
	foreach($cityList as &$cl){
		$cl['name_cit']=utf8_encode($cl['name_cit']);
	}
}

$smarty->register('cityList,serial_cou,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadCities.'.$_SESSION['language'].'.tpl');
?>