<?php
/*
File: loadCreditNoteCountries.rpc.php
Author: Santiago Benitez
Creation Date: 17/05/2010
Last Modified: 
Modified By: 
*/

//$_SESSION['countryList']='2,3';
$country=new Country($db);
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}

if($_POST['serial_zon']){
	$serial_zon=$_POST['serial_zon'];
	$countryList=$country->getCreditNoteCountriesByZone($_POST['serial_zon'],$_SESSION['serial_mbc'],$_SESSION['countryList']);
}

if(is_array($countryList)){
	foreach($countryList as &$cl){
		$cl['name_cou']=utf8_encode($cl['name_cou']);
	}
}

$smarty->register('countryList,serial_zon,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadCountries.'.$_SESSION['language'].'.tpl');
?>