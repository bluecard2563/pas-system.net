<?php
/*
* File: loadCreditNotes.php
* Author: Santiago Benitez
* Creation Date: 13/05/2010
* Last Modified:
* Modified By:
*/

$cNote=new CreditNote($db);
if($_POST['serial']){
    $serial=$_POST['serial'];//setting vars
}
if($_POST['cnfor']){
    $cnfor=$_POST['cnfor'];//setting vars
}

if($cnfor=='dea'){
    $serial_dea=$serial;
    $serial_cus=null;
}else{
    $serial_dea=null;
    $serial_cus=$serial;
}
$creditNoteList=$cNote->getCreditNotesByOwner($serial_dea,$serial_cus);
$exist=0;
if($creditNoteList){
    foreach($creditNoteList as $cl){
        if($cl['name_cus']||$cl['name_dea']){
            $exist=1;
        }
    }
}
$smarty->register('creditNoteList,cnfor,exist');
$smarty->assign('container','none');
$smarty->display('helpers/loadCNotes/loadCreditNotes.'.$_SESSION['language'].'.tpl');
?>