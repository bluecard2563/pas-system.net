<?php
/*
* File: loadCustomersByCity.php
* Author: Santiago Benitez
* Creation Date: 13/05/2010
* Last Modified:
* Modified By:
*/

$customer=new Customer($db);
if($_POST['serial_cit']){
    $serial_cit=$_POST['serial_cit'];
    $customerList=$customer->getCreditNoteCustomersByCity($serial_cit, $_SESSION['serial_mbc']);
}
else {
    $noData=1;
}

if(is_array($customerList)){
	foreach($customerList as &$cl){
		$cl['complete_name']=utf8_encode($cl['complete_name']);
	}
}

$smarty->register('customerList,noData');
$smarty->assign('container','none');
$smarty->display('helpers/loadCNotes/loadCustomersByCity.'.$_SESSION['language'].'.tpl');
?>