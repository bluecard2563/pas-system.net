<?php
/*
 * File: getPrintCNoteFormrpc.php
 * Author: Patricio Astudillo
 * Creation Date: 30/04/2010, 09:25:10 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/04/2010, 09:25:10 AM
 */

$cNoteNumber=$_POST['cNoteNumber'];
$serial_mbc=$_POST['serial_mbc'];
$type=$_POST['type'];

$mbc=new ManagerbyCountry($db, $_POST['serial_mbc']);
$mbc->getData();

$dbm=new DocumentByManager($db);
$available_documents=$dbm->getDocumentsByManager($mbc->getSerial_man(), 'CREDIT_NOTE');

$cNote=new CreditNote($db);
foreach($available_documents as $ad){
	$cNote->setNumber_cn($cNoteNumber);
	$serialCN=$cNote->getCreditNoteRegister($ad['serial_dbm'],$type);

	if($serialCN){
		break;
	}
}

if(is_array($serialCN)){
	if(sizeof($serialCN)==1){
		$cNote->setSerial_cn($serialCN[0]['serial_cn']);
		if($cNote->getData()){
			$cNote=get_object_vars($cNote);
			$smarty->register('cNote,type');
		}else{
			$error=3;
		}
	}else{
		$error=1;
	}
}else{
	$error=2;
}

$smarty->register('error');
$smarty->assign('container','none');
$smarty->display('helpers/loadCNotes/getPrintCNoteForm.'.$_SESSION['language'].'.tpl');
?>