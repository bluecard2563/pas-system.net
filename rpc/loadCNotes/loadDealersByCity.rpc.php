<?php
/*
* File: loadDealersByCity.php
* Author: Santiago Benitez
* Creation Date: 13/05/2010
* Last Modified:
* Modified By:
* Description: File which loads all the dealeres from a specific city
	     in a select object.
*/

$dealer=new Dealer($db);
if($_POST['serial_cit']){
    $serial_cit=$_POST['serial_cit'];
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$dealerList=$dealer->getCreditNoteDealersByCity($serial_cit, $_SESSION['dea_serial_dea']);
	} else {
		$dealerList=$dealer->getCreditNoteDealersByCity($serial_cit);
	}
}
else {
    $noData=1;
}

if(is_array($dealerList)){
	foreach($dealerList as &$dl){
		$dl['name_dea']=utf8_encode($dl['name_dea']);
	}
}

$smarty->register('dealerList,noData');
$smarty->assign('container','none');
$smarty->display('helpers/loadCNotes/loadDealersByCity.'.$_SESSION['language'].'.tpl');
?>