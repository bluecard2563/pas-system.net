<?php
/*
File: changeCurrencyByCountryStatus.rpc
Author: David Bergmann
Creation Date: 24/03/2010
Last Modified:
Modified By:
*/

$currency = new CurrenciesByCountry($db);
$currency->setSerial_cur($_POST['serial_cur']);
$currency->setSerial_cou($_POST['serial_cou']);
$currency->setOfficial_cbc($_POST['official_cbc']);
$currency->setStatus_cbc($_POST['status_cbc']);


if($currency->update()){
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>