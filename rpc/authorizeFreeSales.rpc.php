<?php
/*
 * File: fAuthorizeFreeSales.php
 * Author: Patricio Astudillo
 * Creation Date: 24/03/2010
 * Modified By:
 * Last Modified: 
*/

Request::setInteger('serial_cou');
Request::setInteger('serial_cit');
Request::setInteger('serial_usr');
Request::setInteger('serial_dea');
Request::setInteger('dea_serial_dea');

$pendingFreeSales=Sales::getFreeSales($db,$serial_cou,$serial_cit,$serial_usr,$serial_dea,$dea_serial_dea);
$saleStatus=array('APROVED'=>'Aprobar','DENIED'=>'Negar','INVOICE'=>'Facturar');

$misc=array();
$misc['0']=array('#','No. de Tarjeta','Fecha de Solicitud','Valor Total','Producto');
$misc['1']='serial_sal';
$misc['2']=array('card_number_sal','in_date_sal','name_pbl','total_sal');
$misc['3']='Registrar Cambios';

$smarty->register('pendingFreeSales,misc,saleStatus');
$smarty->assign('container','none');
$smarty->display('helpers/authorizeFreeSales.'.$_SESSION['language'].'.tpl');
?>
