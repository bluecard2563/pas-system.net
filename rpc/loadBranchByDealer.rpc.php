<?php 
/*
File: loadBranchByDealer.php
Author: David Bergmann
Creation Date: 01/02/2010
Last Modified: 05/07/2010
Modified By: David Bergmann
*/
$dealer=new Dealer($db);
$serial_dea = $_POST['serial_dea'];
$selBranches = $_POST['selBranches'];
$selBranches = split(',',$selBranches);

if($serial_dea){
	$params=array('0'=>array('field'=> 'd.dea_serial_dea', 
                                              'value'=> $serial_dea,
                                              'like'=> 1),
                       '1'=>array('field'=> 'd.status_dea',
                                              'value'=> 'ACTIVE',
                                              'like'=> 1),
                       '2'=>array('field'=> 'd.serial_dea',
                                              'value'=> $selBranches,
                                              'like'=> 0),
                       '3'=>array('field'=> 'dtl.serial_lang',
                                              'value'=> $_SESSION['serial_lang'],
                                              'like'=> 1),
					   '4'=>array('field'=>'d.phone_sales_dea',
										   'value'=> 'NO',
										   'like'=> 1)
					   );
	//If $_SESSION['serial_dea'] is set, loads only the user's dealer
	if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}

	$branchList=$dealer->getDealers($params);
}
if(is_array($branchList)){
	foreach($branchList as &$bl){
		$bl['name_dea']=$bl['name_dea'];
	}
}
$smarty->register('branchList,serial_dea');
$smarty->assign('container','none');
$smarty->display('helpers/loadBranchByDealer.'.$_SESSION['language'].'.tpl');
?>