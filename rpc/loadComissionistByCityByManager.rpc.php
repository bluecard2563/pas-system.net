<?php
/*
File: loadComissionistByCityByDealer
Author: David Bergmann
Creation Date: 08/12/2010
Last Modified:
Modified By:
*/

$user=new User($db);
$text = $_POST['text'];

if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}

if($_POST['serial_cit'] && $_POST['serial_mbc']){
	$serial_cit=$_POST['serial_cit'];
        if($_POST['all']){
            $comissionistList=$user->getComissionistByCityByManager($_POST['serial_mbc'],$_POST['serial_cit']);
        }
        else{
            $comissionistList=$user->getComissionistByCityByManager($_POST['serial_mbc'],$_POST['serial_cit'],$_SESSION['cityList']);
        }
}

if(is_array($comissionistList)){
	foreach($comissionistList as &$cl){
		$cl['name_usr']=utf8_encode($cl['name_usr']);
	}
}

$smarty->register('comissionistList,serial_cit,serial_sel,text');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissionistByCityByManager.'.$_SESSION['language'].'.tpl');
?>