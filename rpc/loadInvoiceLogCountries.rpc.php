<?php
/*
File: loadInvoiceLogCountries.rpc.php
Author: Santiago Benitez
Creation Date: 12/04/2009
Last Modified: 12/04/2009
Modified By: Santiago Benitez
*/

$country=new Country($db);
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}
if($_POST['serial_zon']){
	$serial_zon=$_POST['serial_zon'];
	$countryList=$country->getInvoiceLogCountriesByZone($_POST['serial_zon'],$_SESSION['countryList']);
}

if(is_array($countryList)){
	foreach($countryList as &$cl){
		$cl['name_cou']=utf8_encode($cl['name_cou']);
	}
}
$smarty->register('countryList,serial_zon,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadCountries.'.$_SESSION['language'].'.tpl');
?>