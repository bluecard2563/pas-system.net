<?php session_start();
/*
Author:Miguel Ponce
Modified date:03/03/2010
Last modified:03/03/2010
*/
$code_lang=$_SESSION['language'];

$condition	=   new Condition($db);
$currency	=   new Currency($db);
$restriction    =   new Restriction($db);
$conditions	=   $condition->getConditions($code_lang);
$restrictions   =   $restriction->getRestrictions($code_lang);
$currencies 	=   $currency->getCurrency();

foreach($conditions as &$condition){
    $condition['description_cbl']=utf8_encode($condition['description_cbl']);
}
foreach($restrictions as &$restriction){
    $restriction['description_rbl']=utf8_encode($restriction['description_rbl']);
}
foreach($currencies as &$currency){
    $currency['symbol_cur']=utf8_encode(htmlentities($currency['symbol_cur']));
}

$benefitsInfo=array("conditions"=>$conditions,"restrictions"=>$restrictions,"currencies"=>$currencies);

echo json_encode($benefitsInfo);


?>