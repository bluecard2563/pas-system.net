<?php
/*
 * File: loadAppliedProducts
 * Author: Patricio Astudillo
 * Creation Date: Feb 14, 2011, 11:48:59 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setInteger('serial_cou');
Request::setString('multiple');

$pxc = new ProductByCountry($db);
$product_list = $pxc -> getProductsByCountry($serial_cou, $_SESSION['serial_lang'], 'ACTIVE', 'NO');

if($multiple == 'NO'){
	$id_name_value = "selProduct";
	$multiple_choice = "";
}else{
	$id_name_value = "selAppliedProduct";
	$multiple_choice = "multiple";
}

$smarty -> register('product_list,multiple_choice,id_name_value,multiple');
$smarty -> assign('container','none');
$smarty -> display('helpers/loadClientPromo/loadAppliedProducts.'.$_SESSION['language'].'.tpl');
?>
