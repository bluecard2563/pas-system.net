<?php
/*
 * File: loadPromosForUpdate
 * Author: Patricio Astudillo
 * Creation Date: Feb 15, 2011, 10:47:54 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setString('serial_cou');
Request::setString('status_ccp');

$promo_list = CustomerPromo::getCustomerPromoByCountryAndStatus($db, $serial_cou, $status_ccp);
$pages_number = (int)(count($promo_list)/10) + 1;

$smarty -> register('promo_list,pages_number');
$smarty -> assign('container', 'none');
$smarty -> display('helpers/loadClientPromo/loadPromosForUpdate.'.$_SESSION['language'].'.tpl');
?>