<?php
/**
 * File:  validateCupon
 * Author: Patricio Astudillo
 * Creation Date: 10-mar-2011, 15:25:56
 * Description:
 * Last Modified: 10-mar-2011, 15:25:56
 * Modified by:
 */

Request::setString('cupon');
Request::setString('serial_pbd');

$pxc_data = ProductByCountry::getPxCDataByPbDSerial($db, $serial_pbd);
$serial_cup = Cupon::validateCupon($db, $cupon, $pxc_data['serial_pxc']);

if($serial_cup){
	$cupon = new Cupon($db, $serial_cup);
	$cupon -> getData();
}

$smarty -> register('h');
$smarty -> assign('container', 'none');
$smarty -> display('helpers/loadClientPromo/validateCupon.'.$_SESSION['language'].'.tpl');
?>
