<?php
/*
File: loadUsersByManager.rpc.php
Author: David Bergmann
Creation Date: 15/02/2010
Last Modified:
Modified By:
Description: File which loads all the data for the management
			 of credit days by country.
*/

Request::setString('serial_mbc');


$user = new User($db, $_SESSION['serial_usr']);
$user->getData();

if($serial_mbc) {
    $comissionistList=$user->getUsersByManager($serial_mbc);
}

//PLANETASSIST USERS
$mainComissionistList=$user->getMainManagerUsers(NULL,false,TRUE);
$paUser = !$user->isPlanetAssistUser();

$smarty->register('comissionistList,mainComissionistList,serial_mbc,paUser');
$smarty->assign('container','none');
$smarty->display('helpers/loadUsersByManager.'.$_SESSION['language'].'.tpl');
?>