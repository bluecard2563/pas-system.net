<?php 
/*
File: loadCreditDayInfo.rpc.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 18:16
Last Modified: 06/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the data for the management 
			 of credit days by country.
*/

Request::setString('serial_cou');
$creditD=new CreditDay($db);
$data=$creditD->getAllCreditDays($serial_cou);
$titles=array('#','D&iacute;as de Cr&eacute;dito','Estado','Actualizar');
$text="Registrar nuevo";
$smarty->register('data,titles,text');
$smarty->assign('container','none');
$smarty->display('helpers/loadAdminTable.'.$_SESSION['language'].'.tpl');
?>