<?php 
/*
File: checkFreelanceEmail.php
Author: Esteban Angulo
Creation Date: 4/01/2010 16:20
Last Modified:
Modified By:
Description: File which verifies if there is another freelance with 
			 the same email address in the system.
*/

Request::setString('txtMail');
if($_POST['txtSerial_fre']){
	$serial_fre=$_POST['txtSerial_fre'];
	echo $serial_fre;
}

//$txtEmail=trim(strtolower(utf8_decode($txtEmail)));

$freelance=new Freelance($db);
if($freelance->existsFreelanceEmail($txtMail,$serial_fre)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>