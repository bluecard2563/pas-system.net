<?php
/*
File: loadProductsMultiple.php
Author: Santiago Benitez
Creation Date: 17/03/2010
Last Modified:
Modified By:
*/
Request::setInteger('serial_cou');
$produc=new City($db);
if($serial_cou){
    $product = new Product($db);
    $productList = $product->getProductsByCountry($serial_cou, $_SESSION['serial_lang']);
}else{
    echo 'Seleccione un pa&iacute;s';
}

if($productList){
echo '<div align="center">Existentes</div><select multiple id="productsFrom" name="countries[count][productsFrom][]" class="selectMultiple span-12 last" city="'.$_POST['serial_cit'].'">';
    
        foreach($productList as &$pl){
            $pl['name_pbl'] = utf8_encode($pl['name_pbl']);
            echo '<option value="'.$pl['serial_pxc'].'" used="0">'.$pl['name_pbl'].'</option>';
        }
echo '</select>';
}else{
    echo '<br>*No existen productos para este pa&iacute;s.';
}
?>