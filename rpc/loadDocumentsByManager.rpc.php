<?php
/*
File: loadDocumentsByCountry.rpc.php
Author: Santiago Benitez
Creation Date: 01/04/2010 16:18
Last Modified: 01/04/2010 16:18
Modified By: Santiago Benitez
*/

	Request::setString('serial_mbc');
	
	$mbc=new ManagerbyCountry($db, $serial_mbc);
	$mbc->getData();
        
	$dbm = new DocumentByManager($db);
	$documentList=$dbm->getDocumentsByManager($mbc->getSerial_man(),'INVOICE');
	
	$smarty->register('documentList,global_typeManager');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadDocumentsByManager.'.$_SESSION['language'].'.tpl');
?>