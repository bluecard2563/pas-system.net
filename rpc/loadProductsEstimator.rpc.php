<?php 
/*
File: loadProducts.php
Author: Edwin Salvador
Creation Date: 08/03/2010
Last Modified: 
Modified By: 
*/
Request::setInteger('serial_cou');
$produc=new City($db);

if($serial_cou){
    $product = new Product($db);
    $productList = $product -> getEstimatorProductsListByCountry($serial_cou, $_SESSION['serial_lang']);
}

$smarty->register('productList');
$smarty->assign('container','none');
$smarty->display('helpers/loadProductsEstimator.'.$_SESSION['language'].'.tpl');
?>