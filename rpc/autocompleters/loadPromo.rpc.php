<?php 
/*
File: loadPromo.php
Author: Patricio Astudillo
Creation Date: 07/02/2010 14:09
Last Modified: 07/02/2010
Modified By: Patricio Astudillo
Description: File which loads all promotions according with a
             pattern in a autocompleter
*/

switch($_GET['appliedTo']){
    case 'MANAGER':
        $promo=new PromoByManager($db);
        $serial=$_GET['serial_info'];
        break;
    case 'CITY':
        $promo=new PromoByCity($db);
        $serial=$_GET['serial_info'];
        break;
    case 'COUNTRY':
        $promo=new PromoByCountry($db);
        $serial=$_GET['serial_info'];
        break;
    case 'DEALER':
        $promo=new PromoByDealer($db);
        $serial='stop';
        break;
}
if($_GET['all']){
	$all=$_GET['all'];
}
if($serial){
    $auxList=$promo->getPromoAutocompleter(utf8_decode($_GET['q']),$serial,$all);
    if(is_array($auxList)){
        foreach($auxList as $c){
                echo utf8_encode($c['name_prm'])."|".$c['serial_prm']."\n";
        }
    }else{
        echo 'No existen promociones para el pa&iacute;s seleccionado.';
    }
}else{
    echo 'No existen promociones con ese nombre.';
}
?>