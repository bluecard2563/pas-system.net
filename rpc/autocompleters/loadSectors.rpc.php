<?php 
/*
File: loadSectors.php
Author: David Bergmann
Creation Date: 07/01/2010 15:39
Last Modified:
Modified By:
Description: File which loads all the sectors according with a
			 pattern in a autocompleter
*/

$sector=new Sector($db);
$auxList=$sector->getSectorsAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_sec']."|".$c['name_cit'])."|".$c['serial_sec']."\n";
	}
}
?>