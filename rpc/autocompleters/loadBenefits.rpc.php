<?php 
/*
File: loadBenefits.php
Author: Patricio Astudillo M.
Creation Date: 05/01/2010 11:52
Last Modified: 27/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the benefits acording a 
			 specific pattern in an autocompleter
*/

$benefit=new Benefit($db);
$auxList=$benefit->getBenefitsAutocompleter(utf8_decode($_GET['q']),$_GET['serial_lang']);

if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode(trim($c['description_bbl']))."|".$c['serial_ben']." \n";
	}
}else{
	echo '0|0 \n';
}
?>