<?php 
/*
File: loadConditions.php
Author: Patricio Astudillo M.
Creation Date: 04/01/2010 17:38
Last Modified: 04/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the Conditions acording a
	     specific language and  pattern in an autocompleter.
*/

$condition=new Condition($db);
$auxList=$condition->getConditionsAutocompleter(utf8_decode($_GET['q']),$_GET['serial_lang']);

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['description_cbl'])."|".$c['serial_con']."\n";
	}
}
?>