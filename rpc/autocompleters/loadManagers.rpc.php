<?php
/*
File: loadManagers.rpc.php
Author: Santiago Ben�tez
Creation Date: 11/02/2010 14:09
Last Modified: 11/02/2010 14:09
Modified By: Santiago Ben�tez
*/

$manager =new Manager($db);
$auxList=$manager->getManagersAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode($c['document_man']." - ".$c['name_man'])."|".$c['serial_man']."\n";
	}
}else{
	echo '0|0';
}
?>