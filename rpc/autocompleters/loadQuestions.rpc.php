<?php 
/*
File: loadQuestions.php
Author: Santiago Benítez
Creation Date: 12/01/2010 11:52
Last Modified: 12/01/2010 11:52
Modified By: Santiago Benítez
*/
	
$question=new QuestionByLanguage($db);
$question->setSerial_lang($_GET['serial_lang']);
$auxList=$question->getQuestionsByLanguangeAutocompleter(utf8_decode($_GET['q']));
if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['text_qbl'])."|".$c['serial_qbl']."\n";
	}
}
?>