<?php 
/*
File: loadServices.php
Author: Gabriela Guerrero
Creation Date: 02/07/2010
Last Modified:
Modified By:
Description: File which loads all the services according with a
			 pattern in a autocompleter
*/

Request::setString('serial_lang');
Request::setString('q');

$service=new Service($db);
$serviceList=$service->getServicesAutocompleter($q, $serial_lang);

if(is_array($serviceList)){
	foreach($serviceList as $s){
		echo utf8_encode($s['name_sbl'])."|".$s['serial_sbl']."\n";
	}
}else{
	echo '0'."|".'0'."\n";
}
?>