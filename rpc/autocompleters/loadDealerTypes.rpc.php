<?php 
/*
File: loadDealerTypes.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 15:05
Last Modified: 22/01/2010
Modified By: David Bergmann
Description: File which loads all the dealer types 
			 acording a specific pattern in an autocompleter.
*/

$dealerT=new DealerType($db);
$dealerT->setSerial_lang($_GET['serial_lang']);
$auxList=$dealerT->getDealerTypesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode($c['name_dlt'].' - '.$c['description_dtl'])."|".$c['serial_dlt']."\n";
	}
}
?>