<?php 
/*
File: loadCountries.php
Author: David Bergmann
Creation Date: 05/01/2010 18:09
Last Modified:
Modified By:
Description: File which loads all the countries according with a
			 pattern in a autocompleter
*/

$country=new Country($db);
$auxList=$country->getCountriesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_cou']."|".$c['name_zon']."|".$c['serial_cou'])."|".$c['serial_zon']."\n";
	}
}
?>