<?php 
/*
File: loadDealers.php
Author: Patricio Astudillo
Creation Date: 11/01/2010 14:09
Last Modified: 11/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the dealers according with a
			 pattern in a autocompleter
*/

$dealer =new Dealer($db);
//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$auxList=$dealer->getDealersAutocompleter(utf8_decode($_GET['q']), $_SESSION['serial_mbc'], $_SESSION['dea_serial_dea']);
} else {
	$auxList=$dealer->getDealersAutocompleter(utf8_decode($_GET['q']), $_SESSION['serial_mbc']);
}

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['code_dea']." - ".$c['name_dea'])."|".$c['serial_dea']."\n";
	}
}
?>