<?php 
/*
File: loadCities.php
Author: David Bergmann
Creation Date: 05/01/2010 16:07
Last Modified:
Modified By:
Description: File which loads all the cities according with a
			 pattern in a autocompleter
*/

$city=new City($db);
$auxList=$city->getCitiesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_cit']."|".$c['name_cou'])."|".$c['serial_cit']."\n";
	}
}
?>