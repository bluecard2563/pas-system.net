<?php 
/*
File: loadDocumentTypes.rpc.php
Author: Santiago Benítez
Creation Date: 11/01/2010 14:34
Last Modified: 11/01/2010 14:34
Modified By: Santiago Benítez
Description: File which loads all the document types according to a
			 pattern in an autocompleter
*/

$documentType=new DocumentType($db);
$auxList=$documentType->getDocumentTypesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_doc'])."|".$c['serial_doc']."\n";
	}
}
?>