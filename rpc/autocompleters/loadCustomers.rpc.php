<?php 
/*
File: loadCustomers.php
Author: David Bergmann
Creation Date: 11/01/2010
Last Modified:
Modified By:
Description: File which loads all the customers according with a
			 pattern in a autocompleter
*/

$customer =new Customer($db);
$auxList=$customer->getCustomersAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['document_cus']." - ".$c['first_name_cus']." ".$c['last_name_cus'])."|".$c['serial_cus']."\n";
	}
}else{
	echo '0'."|".'0'."\n";
}
?>