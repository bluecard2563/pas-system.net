<?php 
/*
File: loadPrizes.php
Author: Nicolas Flores.
Creation Date: 05/01/2010 17:38
Description: File which loads all the restrictions acording a 
			 specific pattern in an autocompleter
*/

$prize=new Prize($db);
$auxList=$prize->getPrizesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_pri'])."|".$c['serial_pri']."\n";
	}
}
?>