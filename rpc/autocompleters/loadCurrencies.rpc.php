<?php 
/*
File: loadCurrencies.php
Author: David Bergmann
Creation Date: 06/01/2010 12:34
Last Modified:
Modified By:
Description: File which loads all the currencies according with a
			 pattern in a autocompleter
*/

$currency=new Currency($db);
$auxList=$currency->getCurrenciesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_cur'])."|".$c['serial_cur']."\n";
	}
}
?>