<?php
/*
File: loadInvoices.php
Author: Santiago Benitez
Creation Date: 01/04/2010 14:25
Last Modified: 01/04/2010
Modified By: Santiago Benitez
*/

$invoice=new Invoice($db);
$auxList=$invoice->getInvoicesAutocompleter($_GET['q'],$_GET['serial_mbc'],$_GET['type_dbc']);
if(is_array($auxList)){
	foreach($auxList as $c){
                $number=$c['number_inv'];
                while(strlen($number)<8){
                    $number='0'.$number;
                }
		echo $number."|".$c['serial_inv']."\n";
	}
}else{
    echo 'No existe una factura con este numero.';
}
?>