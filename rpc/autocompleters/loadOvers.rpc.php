<?php 
/*
File: loadOvers.php
Author: Santiago Borja
Creation Date: 18/03/2010
*/
	$over = new Over($db);
	$is_for_payment = $_GET['for_payment'];
    $auxList=$over->getOverAutocompleter(utf8_decode($_GET['q']),$is_for_payment);
    if(is_array($auxList)){
        foreach($auxList as $c){
                echo utf8_encode($c['name_ove'])."|".$c['serial_ove']."\n";
        }
    }
?>