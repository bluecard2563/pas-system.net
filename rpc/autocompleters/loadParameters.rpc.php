<?php 
/*
File: loadParameters.php
Author: Santiago Benítez
Creation Date: 13/01/2010 10:28
Last Modified: 13/01/2010 10:28
Modified By: Santiago Benítez
*/

$parameter=new Parameter($db);
$auxList=$parameter->getParametersAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_par'])."|".$c['serial_par']."\n";
	}
}
?>