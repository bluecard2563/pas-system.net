<?php 
/*
File: loadCounter.php
Author: Patricio Astudillo
Creation Date: 13/01/2010 15:21
Last Modified: 13/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the users according with a
			 pattern in a autocompleter
*/

$counter =new Counter($db);
if($_GET['serial_dea']){
    $auxList=$counter->getCountersAutocompleter(utf8_decode($_GET['q']), $_GET['serial_dea']);

    if(is_array($auxList)){
            foreach($auxList as $c){
                    echo utf8_encode($c['document_usr']." - ".$c['first_name_usr']." ".$c['last_name_usr'])."|".$c['serial_cnt']."\n";
            }
    }else{
        echo 'No existe un counter con esos datos.';
    }
}else{
    echo 'Defina un filtro a aplicarse.';
}
?>