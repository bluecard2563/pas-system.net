<?php
/*
File: loadServiceProvider.rpc.php
Author: MArio L�pez.
Creation Date: 19/02/2010 15:05
Last Modified:
Modified By:
Description: File which loads all the Service Providers
			 acording a specific pattern in an autocompleter.
*/
$serviceProvider=new ServiceProvider($db);
$auxList=$serviceProvider->getActiveServiceProviderAutocompleter(utf8_decode($_GET['q']));
if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode($c['name_spv'].' - '.$c['name_cou'])."|".$c['serial_spv']."\n";
	}
}
?>