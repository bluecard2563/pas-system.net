<?php
/*
File: loadExecutiveCustomers.php
Author: Nicolas Flores
Creation Date: 06/04/2010 
Description: File which loads all the customers with executive cards
			 acording a specific pattern in an autocompleter.
*/
$customer=new Customer($db);
$auxList=$customer->getExecutiveCustomerAutocompleter(utf8_decode($_GET['q']));
if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode($c['customer_name'])."|".$c['serial_cus']."\n";
	}
}else{
		echo '0'."|".'0'."\n";
}
?>