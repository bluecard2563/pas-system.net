<?php 
/*
/*
File: loadLanguages.rpc.php
Author: Santiago Benítez
Creation Date: 20/01/2010 14:30
Last Modified: 20/01/2010 14:30
Modified By: Santiago Benítez
*/

$language=new Language($db);
$auxList=$language->getLanguagesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_lang'])."|".$c['serial_lang']."\n";
	}
}
?>