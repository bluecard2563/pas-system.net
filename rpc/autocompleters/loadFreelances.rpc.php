<?php 
/*
File: loadFreelances.php
Author: Esteban Angulo
Creation Date: 05/01/2010 12:25
Last Modified: 05/01/2010
Modified By: Esteban Angulo
Description: File which loads all freelances according to the
			 pattern in an autocompleter
*/

$freelance=new Freelance($db);
$auxList=$freelance->getFreelanceAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['document_fre'].' - '.$c['name_fre'].' '.$c['lastname_fre'])."|".$c['serial_fre']."\n";
	}
}else{
    echo 'No existe un agente libre con esos datos.';
}
?>