<?php
/*
File: loadContactsByServiceProvider.rpc.php
Author: Santiago Ben�tez
Creation Date: 19/02/2010 12:09
Last Modified: 19/02/2010 12:09
Modified By: Santiago Ben�tez
*/

$contact =new ContactByServiceProvider($db);
$auxList=$contact->getContactsByServiceProviderAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode($c['first_name_csp']." ".$c['last_name_csp']." - ".$c['email_csp'])."|".$c['serial_csp']."\n";
	}
}
?>