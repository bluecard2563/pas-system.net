<?php 
/*
File: loadUsers.php
Author: Esteban Angulo
Creation Date: 05/01/2010 11:48
Last Modified: 05/01/2010
Modified By: Esteban Angulo
Description: File which loads all the suitable counters
*/

$counter=new Counter($db);
$dealer=new Dealer($db);
$dealer->setSerial_dea($_GET['serial_dea']);
$dealer->getData();
$offSeler=$dealer->getOfficial_seller_dea();
$serial_mbc=$dealer->getSerial_mbc();
/*echo "exclusivo: ";
echo $offSeler;*/


$auxList=$counter->getSuitableCountersByDealer($_GET['q'], $_GET['serial_dea'], $offSeler, $serial_mbc);

if(is_array($auxList)){	
    foreach($auxList as $c){
            echo utf8_encode($c['document_usr'].' - '.$c['first_name_usr'].' '.$c['last_name_usr'])."|".$c['serial_usr']."\n";
    }
}else{
    echo 'Counter no encontrado';
}
?>