<?php 
/*
File: loadRestrictions.php
Author: Patricio Astudillo M.
Creation Date: 04/01/2010 17:38
Last Modified: 04/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the restrictions acording a 
	     specific language and  pattern in an autocompleter.
*/

$restriction=new Restriction($db);
$auxList=$restriction->getRestrictionsAutocompleter(utf8_decode($_GET['q']),$_GET['serial_lang']);

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['description_rbl'])."|".$c['serial_rst']."\n";
	}
}
?>