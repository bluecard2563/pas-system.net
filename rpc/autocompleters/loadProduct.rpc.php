<?php 
/*
File: loadDealerTypes.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 15:05
Last Modified: 22/01/2010
Modified By: David Bergmann
Description: File which loads all the dealer types 
			 acording a specific pattern in an autocompleter.
*/
$product=new Product($db);
$auxList=$product->getProductAutocompleter($_GET['serial_lang'],utf8_decode($_GET['q']),$_GET['allow_inactive']);
if(is_array($auxList)){
	foreach($auxList as $c){
		echo utf8_encode($c['name_pbl'])."|".$c['serial_pro']."\n";
	}
}else{
	echo 'No existen resultados | 0';
}
?>