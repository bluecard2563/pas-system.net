<?php 
/*
File: loadTaxes.php
Author: David Bergmann
Creation Date: 11/01/2010 08:59
Last Modified:
Modified By:
Description: File which loads all the taxes according with a
			 pattern in a autocompleter
*/

$tax = new Tax($db);
$auxList=$tax->getTaxesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_tax']."|".$c['name_cou'])."|".$c['serial_tax']."\n";
	}
}
?>