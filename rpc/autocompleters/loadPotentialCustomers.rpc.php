<?php 
/*
File: loadPotentialCustomers.php
Author: Esteban Angulo
Creation Date: 14/07/2010
Description: File which loads all the potential customers according to the
			 pattern inserted in an autocompleter
*/

$potentialCustomer =new PotentialCustomers($db);
$auxList=$potentialCustomer->getPotentialCustomersAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['document_pcs']." - ".$c['first_name_pcs']." ".$c['last_name_pcs'])."|".$c['serial_pcs']."\n";
	}
}else{
	echo '0'."|".'0'."\n";
}
?>