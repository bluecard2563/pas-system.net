<?php 
/*
File: loadUsers.php
Author: Esteban Angulo
Creation Date: 05/01/2010 11:48
Last Modified: 05/01/2010
Modified By: Esteban Angulo
Description: File which loads all  users according to the
			 pattern in an autocompleter
*/

$user=new User($db);
$auxList=$user->getUsersAutocompleter($_GET['q']);

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['username_usr'].' - '.$c['first_name_usr'].' '.$c['last_name_usr'])."|".$c['serial_usr']."\n";
	}
}else{
    echo 'No existe un usuario con esos datos.';
}
?>