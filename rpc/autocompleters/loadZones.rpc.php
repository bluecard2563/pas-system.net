<?php 
/*
File: loadZones.php
Author: Patricio Astudillo M.
Creation Date: 04/01/2010 12:34
Last Modified: 04/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the zones according with a
			 pattern in a autocompleter
*/

$zone=new Zone($db);
$auxList=$zone->getZonesAutocompleter(utf8_decode($_GET['q']));

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['name_zon'])."|".$c['serial_zon']."\n";
	}
}
?>