<?php 
/*
File: loadDealerTypes.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 15:05
Last Modified: 22/01/2010
Modified By: David Bergmann
Description: File which loads all the dealer types 
			 acording a specific pattern in an autocompleter.
*/
$productType=new ProductType($db);
$auxList=$productType->getProductTypesAutocompleter($_GET['serial_lang'],utf8_decode($_GET['q']));

if(is_array($auxList)){
	foreach($auxList as $c){
		//echo utf8_encode($c['name_ptl'].' - '.$c['description_ptl'])."|".$c['serial_tpp']."\n";
		echo utf8_encode($c['name_ptl'])."|".$c['serial_tpp']."\n";
	}
}
?>