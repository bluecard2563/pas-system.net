<?php 
/*
File: loadComitionists.php
Author: David Bergmann
Creation Date: 20/01/2010
Last Modified:
Modified By:
Description: File which loads all the comitionists according with a
			 pattern in a autocompleter
*/
$user=new User($db);
$auxList=$user->getComissionistAutocompleter(utf8_decode($_GET['q']),$_GET['serial_cit'], $_SESSION['serial_mbc']);

if(is_array($auxList)){	
	foreach($auxList as $c){
		echo utf8_encode($c['first_name_usr']." ".$c['last_name_usr'])."|".$c['serial_usr']."|".$c['serial_cit']."|".$c['is_comissionist']."\n";
	}
}else{
	echo 'none';
}
?>