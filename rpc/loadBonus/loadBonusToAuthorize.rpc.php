<?php
/*
    Document   : loadBonusToAuthorize
    Created on : 21-jun-2010, 13:54:26
    Author     : Nicolas
    Description:
        get all the out date bonus to be authorized or denied
*/
Request::setInteger('serial');
Request::setString('bonus_to');

if($serial && $bonus_to){
	$apBonus=new AppliedBonus($db);
    $bonusToAuthorize=$apBonus->getOutDateBonusToAuthorize($serial, $bonus_to);
}
if($bonusToAuthorize){
	$smarty->register('bonusToAuthorize');
}
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadBonusToAuthorize.'.$_SESSION['language'].'.tpl');
?>
