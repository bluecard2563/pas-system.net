<?php
/*
File: loadResponsiblesByCity.rpc
Author: David Bergmann
Creation Date: 23/12/2010
Last Modified:
Modified By:
*/

Request::setInteger('serial_cit');

$data=  UserByDealer::getResponsibleByCity($db, $serial_cit);

$smarty->register('serial_cit,data');
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadResponsiblesByCity.'.$_SESSION['language'].'.tpl');
?>