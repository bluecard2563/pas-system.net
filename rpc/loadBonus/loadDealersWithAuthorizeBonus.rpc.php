<?php 
/*
* File: loadDealersWithAuthorizeBonus.php
* Author: Nicolas Flores
* Creation Date: 21/06/2010

* Description: File which loads all dealers with pending bonus, by city
*/
Request::setInteger('serial_cit');
Request::setString('bonus_to');

if($serial_cit) {
    $dealer = new Dealer($db);
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$dealerList =  $dealer->getDealersByCityWithBonusToAuthorize($serial_cit,$bonus_to,$_SESSION['dea_serial_dea']);
	} else {
		$dealerList =  $dealer->getDealersByCityWithBonusToAuthorize($serial_cit,$bonus_to);
	}
	if($dealerList){
		 $smarty->register('dealerList');
	}
}
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadDealersWithBonus.'.$_SESSION['language'].'.tpl');
?>