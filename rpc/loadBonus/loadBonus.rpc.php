<?php
	/*
	  Document   : loadBonus.rpc
	  Created on : 10-jun-2010, 15:00:23
	  Author     : Nicolas
	  Description:
	  Get the pending bonus for a dealer or counter.
	 */
	Request::setInteger('serial');
	Request::setString('bonus_to');

	if ($serial && $bonus_to) {
		$apBonus = new AppliedBonus($db);
		$creditOnDate = 0;
		$againstOnDate = 0;
		$totalOnDate = 0;
		$creditOutDate = 0;
		$againstOutDate = 0;
		$totalOutDate = 0;
		$toPayOptions = array();
		$onDateBonus = $apBonus->getOnDateBonus($serial, $bonus_to);

		if ($onDateBonus) {
			foreach ($onDateBonus as $od) {
				if ($od['status_apb'] == 'ACTIVE') {
					$creditOnDate+=$od['pending_amount_apb'];
				} elseif ($od['status_apb'] == 'REFUNDED') {
					$againstOnDate+=$od['pending_amount_apb'];
				}
			}
			$totalOnDate = $creditOnDate - $againstOnDate;
			array_push($toPayOptions, array('value' => 'ON', 'option' => 'Dentro de d&iacute;as de Cr&eacute;dito'));
			$smarty->register('totalOnDate,creditOnDate,againstOnDate');
		}

		$outDateBonus = $apBonus->getOutDateBonus($serial, $bonus_to);

		if ($outDateBonus) {
			foreach ($outDateBonus as $oud) {
				if ($oud['status_apb'] == 'ACTIVE') {
					$creditOutDate+=$oud['pending_amount_apb'];
				} elseif ($oud['status_apb'] == 'REFUNDED') {
					$againstOutDate+=$oud['pending_amount_apb'];
				}
			}
			$totalOutDate = $creditOutDate - $againstOutDate;
			array_push($toPayOptions, array('value' => 'OUT', 'option' => 'Fuera de d&iacute;as de Cr&eacute;dito'));
			$smarty->register('totalOutDate,creditOutDate,againstOutDate');
		}
	}

	if (sizeof($toPayOptions) >= 2) {
		array_push($toPayOptions, array('value' => 'BOTH', 'option' => 'Ambos'));
	}

	$total_bonus_to_pay = $totalOutDate + $totalOnDate;

	$smarty->register('toPayOptions,bonus_to,serial,total_bonus_to_pay');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/loadBonus/loadBonus.' . $_SESSION['language'] . '.tpl');
?>
