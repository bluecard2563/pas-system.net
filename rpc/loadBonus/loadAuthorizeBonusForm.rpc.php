<?php
/*
    Document   : loadAuthorizeBonusForm.rpc
    Created on : 21-jun-2010, 15:47:35
    Author     : Nicolas
    Description:
       charge the required fields for bonus authorizatrion based on type : COUNTER or DEALER
*/
Request::setString('bonus_to');
$country=new Country($db);
$countryList=$country->getOwnCountriesWithBonusToAuthorize($_SESSION['countryList'],$bonus_to);
if($countryList)
	$smarty->register('countryList');
$smarty->register('bonus_to');
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadAuthorizeBonusForm.'.$_SESSION['language'].'.tpl');
?>
