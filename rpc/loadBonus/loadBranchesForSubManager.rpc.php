<?php
    Request::setInteger('dea_serial_dea');
    $params=array(
                
                '1'=>array('field'=> 'd.dea_serial_dea',
                           'value'=> $dea_serial_dea,
                           'like'=> 1),
                '2'=>array('field'=> 'd.status_dea',
                           'value'=> 'ACTIVE',
                           'like'=> 1),
                '3'=>array('field'=> 'dtl.serial_lang',
                           'value'=> $_SESSION['serial_lang'],
                           'like'=> 1),
		'4'=>array('field'=>'d.phone_sales_dea',
			   'value'=> 'NO',
			   'like'=> 1)
		
    );
    $dealer = new Dealer($db);
    if($_POST['selected_dea']){
		$selected_dea = explode(",", substr($_POST['selected_dea'], 0, strlen($_POST['selected_dea'])-1));
		array_push($params,array('field'=> 'd.serial_dea',
                                 'value'=> $selected_dea,
                                 'like'=> 0));
    }
    if($dea_serial_dea){
        $dealer = new Dealer($db);
       // $dealerList = $dealer->getDealerBySubmanager($dea_serial_dea);
        $dealerList=$dealer->getDealers($params);
    }else{
        echo 'Seleccione un Subrepresentante';
    }
    
    if($dealerList){
        echo '<div align="center">Existentes</div><select multiple id="dealersFrom" name="dealersFrom[]" 
            class="selectMultiple span-10 last" dealer="'.$_POST['serial_dea'].'">';

                foreach($dealerList as &$dl){
                    $dl['name_dea'] = utf8_encode($dl['name_dea']);
                    echo '<option value="'.$dl['serial_dea'].'" dealer="'.$_POST['serial_dea'].'">'.$dl['name_dea'].' - '.$dl['code_dea'].'</option>';
                }
        echo '</select>';
    }else{
        echo '<br>*No existen Comercializadores para este Subrepresentante';
    }
?>
