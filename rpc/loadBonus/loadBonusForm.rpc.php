<?php
/*
    Document   : loadBonusForm.rpc
    Created on : 09-jun-2010, 15:47:35
    Author     : Nicolas
    Description:
       charge the required fields for bonus liquidation based on type : COUNTER or DEALER
*/
Request::setString('bonus_to');
$country=new Country($db);
$countryList=$country->getOwnCountriesWithBonus($_SESSION['countryList'],$bonus_to, $_SESSION['serial_mbc']);
if($countryList)
	$smarty->register('countryList');
//Debug::print_r($countryList);
$smarty->register('bonus_to');
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadBonusForm.'.$_SESSION['language'].'.tpl');
?>
