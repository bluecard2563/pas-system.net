<?php
/*
File: loadDealersByResponsible.rpc
Author: David Bergmann
Creation Date: 23/12/2010
Last Modified:
Modified By:
*/

Request::setInteger('serial_cit');
Request::setInteger('serial_usr');

$dealer=new Dealer($db);
if($serial_usr){
    $params=array(
                '0'=>array('field'=> 'c.serial_cit',
						  'value'=> $serial_cit,
						  'like'=> 1),
                '1'=>array('field'=> 'd.dea_serial_dea',
                           'value'=> 'NULL',
                           'like'=> 1),
                '2'=>array('field'=> 'd.status_dea',
                           'value'=> 'ACTIVE',
                           'like'=> 1),
                '3'=>array('field'=> 'dtl.serial_lang',
                           'value'=> $_SESSION['serial_lang'],
                           'like'=> 1),
				'4'=>array('field'=>'d.phone_sales_dea',
						   'value'=> 'NO',
						   'like'=> 1),
				'4'=>array('field'=>'ubd.serial_usr',
						   'value'=> $_POST['serial_usr'],
						   'like'=> 1)
    );

	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
	if($_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_mbc',
				  'value'=> $_SESSION['serial_mbc'],
				  'like'=> 1);
		array_push($params,$aux);
	}

    $data = $dealer->getDealers($params);
}

$smarty->register('serial_usr,data');
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadDealersByResponsible.'.$_SESSION['language'].'.tpl');
?>