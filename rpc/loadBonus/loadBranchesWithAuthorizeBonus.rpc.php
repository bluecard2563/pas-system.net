<?php 
/*
* File: loadDealersWithBonus.php
* Author: Nicolas Flores
* Creation Date: 10/06/2010

* Description: File which loads all branches with pending bonus, by dealer
*/
Request::setInteger('serial_dea');
Request::setString('bonus_to');


if($serial_dea) {
	$dealer = new Dealer($db);
	//If $_SESSION['serial_dea'] is set, loads only the user's dealer
	if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$dealerList =  $dealer->getBranchesByDealerWithBonusToAuthorize($serial_dea,$bonus_to,$_SESSION['serial_dea']);
	} else {
		$dealerList =  $dealer->getBranchesByDealerWithBonusToAuthorize($serial_dea,$bonus_to);
	}
	if($dealerList){
		$smarty->register('dealerList');
	}
}
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadBranchesWithAuthorizeBonus.'.$_SESSION['language'].'.tpl');
?>