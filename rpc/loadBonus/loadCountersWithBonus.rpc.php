<?php 
/*
* File: loadCountersWithBonus.php
* Author: Nicolas Flores
* Creation Date: 10/06/2010

* Description: File which loads all Counters with pending bonus, by branch
*/
Request::setInteger('serial_dea');

if($serial_dea) {
    $counter = new Counter($db);
		$counterList =  $counter->getCountersByCityWithBonus($serial_dea);
	if($counterList){
		 $smarty->register('counterList');
	}
}
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadCountersWithBonus.'.$_SESSION['language'].'.tpl');
?>