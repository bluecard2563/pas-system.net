<?php
/*
    Document   : loadCitiesForAuthorizeBonus.rpc
    Created on : 21-jun-2010, 9:23:52
    Author     : Nicolas
    Description:
       Get all the cities with bonus pending to be paid.
*/
Request::setString('serial_cou');
Request::setString('bonus_to');

$city=new City($db);
if($serial_cou && $bonus_to){
		$cityList=$city->getCitiesByCountryWithBonusToAuthorize($serial_cou,$bonus_to,$_SESSION['serial_mbc'],$_SESSION['cityList']);
		if($cityList){
			$smarty->register('cityList');
		}
}
$smarty->register('serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadBonus/loadCitiesForAuthorizeBonus.'.$_SESSION['language'].'.tpl');
?>
