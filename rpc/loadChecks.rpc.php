<?php 
/*
File: loadChecks.php
Author: Miguel Ponce.
Creation Date: 31/12/2009
Last Modified: 31/12/2009
*/


$productType = new ProductType($db);
$serial_tpp = $_POST['serial_tpp'];

$productType	=	new ProductType($db);
$benefit		=	new Benefit($db);
$condition		=	new Condition($db);
$currency		=	new Currency($db);
$restriction	=	new Restriction($db);
$generalCondition	=	new GeneralCondition($db);


$benefits	=	$benefit	->	getActiveBenefits();
$conditions	=	$condition	->	getConditions();
$restrictions=	$restriction ->	 getRestrictions();
$currencies =	$currency -> getCurrency();
$generalConditions = $generalCondition	-> getGeneralConditions();	

	$productType -> setSerial_tpp($serial_tpp);

	$productType -> getData();
	
	$name_tpp = $productType -> getName_tpp();
	$has_comision_tpp = $productType -> getHas_comision_tpp();
	$schengen_tpp = $productType -> getSchengen_tpp();
	$in_web_tpp = $productType -> getIn_web_tpp();
	$max_duration_tpp = $productType -> getMax_duration_tpp();
	$min_duration_tpp = $productType -> getMin_duration_tpp();
	$flights_tpp = $productType -> getFlights_tpp();
	$max_extras_tpp = $productType -> getMax_extras_tpp();
	$min_extras_tpp = $productType -> getMin_extras_tpp();
	$children_tpp = $productType -> getChildren_tpp();
	$spouse_tpp = $productType -> getSpouse_tpp();
	$relative_tpp = $productType -> getRelative_tpp();
	$extras_by_contract_tpp = $productType -> getExtras_by_contract_tpp();
	$masive_tpp = $productType -> getMasive_tpp();
	$emission_country_tpp = $productType -> getEmission_country_tpp();
	$residence_country_tpp = $productType -> getResidence_country_tpp();
	
	

	$benefitsByProductType = new BenefitsByProductType($db); 
	$benefitsByProductType -> setSerial_tpp($serial_tpp);
	$benefitsByType = $benefitsByProductType -> getBenefits();
	
	
	$conditionsByProductType = new ConditionsByProductType($db); 
	$conditionsByProductType -> setSerial_tpp($serial_tpp);
	$conditionsByType = $conditionsByProductType -> getConditions();
	
	
	
$smarty->register('serial_tpp,name_tpp,has_comision_tpp,schengen_tpp,in_web_tpp,max_duration_tpp,min_duration_tpp,flights_tpp,max_extras_tpp,min_extras_tpp,children_tpp,spouse_tpp,relative_tpp,extras_by_contract_tpp,masive_tpp,emission_country_tpp,residence_country_tpp,benefits,conditions,benefitsByType,conditionsByType,restrictions,currencies,generalConditions');

$smarty->assign('container','none');
$smarty->display('helpers/loadChecks.tpl');
?>