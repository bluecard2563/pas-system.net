<?php
/*
File: loadUsersByManager
Author: David Bergmann
Creation Date: 29/12/2010
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
Request::setString('not_in');

$data = User::getUserByMbc($db, $serial_mbc, substr($not_in, 0, strlen($not_in)-1));

foreach ($data as &$d) {
	$d[1] = utf8_encode($d[1]);
}

echo json_encode($data);
?>