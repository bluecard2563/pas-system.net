<?php
/*
File: loadAssistanceMails.rpc
Author: David Bergmann
Creation Date: 29/12/2010
Last Modified:
Modified By:
*/

Request::setString('type');

$parameter = new Parameter($db, '34'); //Parameters for Liquidation Permitions.
$parameter->getData();

$value_par = unserialize($parameter->getValue_par());

if(is_array($value_par[$type])) {
	$data = array();
	foreach ($value_par[$type] as $v) {
		$aux = User::getUserByEmail($db, $v);
		$aux[1] = utf8_encode($aux[1]);
		array_push($data, $aux);
	}
}

echo json_encode($data);

?>