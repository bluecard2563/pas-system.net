<?php 
/*
File: loadPricesServices.rpc.php
Author: Miguel Ponce
Creation Date: 17/03/2010
Last Modified: 
Modified By: 
*/

$serviceByCountry=new ServicesByCountry($db);
if($_POST['count'] != '' && $_POST['serial_ser'] != ''){
        $count = $_POST['count'];
        $serial_cou=$_POST['serial_cou'];
        $serial_ser=$_POST['serial_ser'];
        
        $serviceByCountry->setSerial_cou($serial_cou);
        $serviceByCountry->setSerial_ser($serial_ser);


        if(!$serviceByCountry->getDataBySerial_ser()){
            $serviceByCountry->setSerial_cou('1');
            $servicesByCountry=$serviceByCountry->getDataBySerial_ser();
        }

        $cost_sbc=$serviceByCountry->getCost_sbc();
        $coverage_sbc=$serviceByCountry->getCoverage_sbc();
        $price_sbc=$serviceByCountry->getPrice_sbc();
}

$smarty->register('cost_sbc,coverage_sbc,price_sbc,count');
$smarty->assign('container','none');
$smarty->display('helpers/loadPricesServices.'.$_SESSION['language'].'.tpl');
?>