<?php 
/*
File: loadProducts.php
Author: Edwin Salvador
Creation Date: 08/03/2010
Last Modified: 
Modified By: 
*/
Request::setInteger('serial_cou');
$produc=new City($db);
Request::setString('opt_text');
if($serial_cou){
    $product = new Product($db);
    $productList = $product->getProductsByCountry($serial_cou, $_SESSION['serial_lang']);
}
if($productList){
    foreach($productList as &$pl){
        $pl['name_pbl'] = utf8_encode($pl['name_pbl']);
    }
}
$smarty->register('productList,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadProducts.'.$_SESSION['language'].'.tpl');
?>