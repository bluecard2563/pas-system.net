<?php
/*
File: loadKitTotal.rpc.php
Author: Santiago Ben�tez
Creation Date: 24/02/2010 12:02
Last Modified: 24/02/2010 12:02
Modified By: Santiago Ben�tez
*/
Request::setString('serial_dea');
$kit=new KitByDealer($db);
$kitDetails=new KitDetails($db);
$kit->setSerial_dea($serial_dea);
if($kit->getDataByDealer()){
    $kitsTotal=$kit->getTotal_kbd();
    $kitByDealerID=$kit->getSerial_kbd();
}
$smarty->register('kitsTotal,kitByDealerID');
$smarty->assign('container','none');
$smarty->display('helpers/loadKitTotal.'.$_SESSION['language'].'.tpl');
?>