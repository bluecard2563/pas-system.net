<?php
/*
File: pDeactivateBonus
Author: David Bergmann
Creation Date: 22/10/2010
Last Modified:
Modified By:
*/

$bonus = new Bonus($db, $_POST['serial_bon']);

if($bonus->deactivate()) {
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>
