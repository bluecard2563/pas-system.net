<?php
/*
    Document   : checkUserAlerts.rpc.php
    Created on : Apr 20, 2010, 4:49:12 PM
    Author     : H3Dur4k
    Description:
        Check if the current user has pending alerts.
*/
	$number_of_alerts = Alert::getNumberPendingAlerts($db, $_SESSION['serial_usr']);
	if($number_of_alerts)
		echo $number_of_alerts;
	else
		echo json_encode(false);
?>
