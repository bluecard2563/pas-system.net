<?php
/*
    Document   : loadGroupAlertsrpc.php
    Created on : Apr 29, 2010, 2:41:29 PM
    Author     : H3Dur4k
    Description:
        Retrieves all the pending alert by group
*/

 Request::setInteger('serial_asg');
 $assistanceGroup= new AssistanceGroup($db,$serial_asg);
 $alertsList=$assistanceGroup->getGroupPendingAlerts();
  $smarty->register('alertsList');
  $smarty->assign('container','none');
  $smarty->display('helpers/alerts/loadGroupAlerts.'.$_SESSION['language'].'.tpl');
?>
