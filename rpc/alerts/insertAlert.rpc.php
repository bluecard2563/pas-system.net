<?php
/*
    Document   : insertAlert.rpc.php
    Created on : Apr 20, 2010, 10:35:35 AM
    Author     : H3Dur4k
    Description:
        Inserts an alert.
 * Parameters:
 * date: the date of the alarm to be triggered
 * time: time of the alarm to be triggered
 * Description: text to be displayed on the alarm
 * remote_id: id of the register which the alarm will be related to.
 * table_name: link to attend the alert.
*/
	Request::setString('date,time,description,table_name,type,user,file');
	Request::setInteger('remote_id');
	
	if(!$date || !$time || !$description ||!$type || !$user)
		die('Missing argument for insertAlertRPC');
		//echo "Dia:$date Hora:$time descripcion:$description tabla:$table_name id:$remote_id";
	if($type =='FILE'){
		Request::setInteger('file');
	}
	
	$alert = new Alert($db);
	$alert->setMessage_alt(utf8_decode($description));
	$alert -> setType_alt($type);
	$alert -> setStatus_alt('PENDING');
	$alert -> setRemote_id($remote_id);
	$alert -> setTable_name($table_name);
	if($file){$alert -> setSerial_fle($file);}
	
	if(!$alert -> autoSend($_SESSION['serial_usr'])){
		echo json_encode(false);
	}else{
		echo json_encode(true);
	}
?>