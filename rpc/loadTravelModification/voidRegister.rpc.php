<?php
/*
    Document   : voidRegister.rpc.php
    Created on : May 19, 2010
    Author     : Nicolas Flores
    Description:
        changes to DELETED a register in traveler_log
 * Parameters:
 * serial_trl: serial of the register to be changed

*/
Request::setInteger('serial_trl');
if(!$serial_trl){
	echo json_encode(false);
	die;
}
		
$travelLog=new TravelerLog($db,$serial_trl);
$travelLog->getData();

$travelLog->setStatus_trl('DELETED');
	if($travelLog->update())
		echo json_encode(true);
	else
		echo json_encode(false);
?>
