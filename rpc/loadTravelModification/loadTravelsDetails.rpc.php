<?php
/*
    Document   : loadTravelsDetails.rpc
    Created on : 19-may-2010, 11:09:32
    Author     : Nicolas
    Description:
        Retrieves register travel or travels details based on card number.
*/
Request::setInteger('card_number');
 $sale=new Sales($db);
 $register=$sale->validateCardForModify($card_number,false);
 if($register){
	 switch($register){
		 case 'sale':
			 $sale=new $sale($db);
			 $sale->setCardNumber_sal($card_number);
			 $sale->getDataByCardNumber();
			 $travelLog= new TravelerLog($db);
			 $travelLog->setSerial_sal($sale->getSerial_sal());
			 $activeRegisterTravels=$travelLog->getActiveRegisterTravels();
			 break;
		 case 'travel_log':
			 $activeRegisterTravels=array();
			 $travelLog= new TravelerLog($db);
			 $travelLog->setCard_number_trl($card_number);
			 $activeRegisterTravels=$travelLog->getRegisterByCardNumber($card_number);
			 break;
	 }
 }
 if(is_array($activeRegisterTravels)){
	 $smarty->register('activeRegisterTravels');
 }
 $smarty->register('register');
 $smarty->assign('container','none');
 $smarty->display('helpers/loadTravelModification/loadTravelsDetails.'.$_SESSION['language'].'.tpl');
?>
