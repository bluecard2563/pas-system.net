<?php
/*
* File: loadExtraInfo.php
* Author: Esteban Angulo
* Creation Date: 12/02/2010
* Last Modified:
* Modified By:
* Description: File which loads extra fields acording a specific parameter.
*/

$country=new Country($db);
$aux=$_POST['param'];
$countryList=$country->getOwnCountries($_SESSION['countryList']);

$smarty->register('countryList,aux');
$smarty->assign('container','none');
$smarty->display('helpers/loadExtraInfo.'.$_SESSION['language'].'.tpl');

?>
