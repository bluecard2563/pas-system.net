<?php 
/*
File: loadTaxesChecks.rpc.php
Author: Edwin Salvador
Creation Date: 17/02/2010
Last Modified: 
Modified By: 
*/

$tax=new Tax($db);
if($_POST['serial_cou'] != '' && $_POST['count'] != ''){
    $count = $_POST['count'];
    $serial_cou=$_POST['serial_cou'];
    $taxList=$tax->getTaxes($serial_cou);

    $check_values = array();
    $check_output = array();

    if(is_array($taxList)){
        foreach($taxList as $tl){
            $check_values[] = $tl['serial_tax'];
            $check_output[] = utf8_encode($tl['name_tax']);
        }
    }
}
$smarty->register('check_values,check_output,count');
$smarty->assign('container','none');
$smarty->display('helpers/loadTaxesChecks.'.$_SESSION['language'].'.tpl');
?>