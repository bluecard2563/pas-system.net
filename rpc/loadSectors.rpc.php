<?php 
/*
File: loadSectors.php
Author: Patricio Astudillo M.
Creation Date: 11/01/2010 09:45
Last Modified: 11/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the sectors from a specific city 
			 in a select objetc
*/

$sector=new Sector($db);
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}
if($_POST['serial_cit']){
	$serial_cit=$_POST['serial_cit'];
	$sectorList=$sector->getSectorsByCity($_POST['serial_cit']);
}

if(is_array($sectorList)){
	foreach($sectorList as &$cl){
		$cl['name_sec']=utf8_encode($cl['name_sec']);
	}
}

$smarty->register('sectorList,serial_cit,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadSectors.'.$_SESSION['language'].'.tpl');
?>