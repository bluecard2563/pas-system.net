<?php
/*
File: loadDealersFiltered.rpc.php
Author: Nicolas Flores
Creation Date: 03/03/2010
LastModified:
Modified By:
*/
Request::setInteger('serial_mbc');
Request::setInteger('serial_usr');
Request::setString('dealer_cat');
Request::setInteger('serial_dlt');
Request::setInteger('serial_cit');
Request::setInteger('serial_cou');

$dealer=new Dealer($db);
$params=array();
	if($serial_cit){
         array_push($params,array('field'=> 'c.serial_cit',
                                      'value'=> $serial_cit,
                                      'like'=> 1));
    }
    if($serial_mbc){
         array_push($params,array('field'=> 'd.serial_mbc',
                                      'value'=> $serial_mbc,
                                      'like'=> 1));
    }
    if($serial_usr){
         array_push($params,array('field'=> 'ubd.serial_usr',
                                      'value'=> $serial_usr,
                                      'like'=> 1));
    }
    if($dealer_cat!='false'){
        array_push($params,array('field'=> 'd.category_dea',
                                      'value'=> $dealer_cat,
                                      'like'=> 1));
    }
    if($serial_dlt){
        array_push($params,array('field'=> 'd.serial_dlt',
                                      'value'=> $serial_dlt,
                                      'like'=> 1));
    }
	if($serial_cou){
        array_push($params,array('field'=> 'c.serial_cou',
                                      'value'=> $serial_cou,
                                      'like'=> 1));
    }
	array_push($params,array('field'=> 'd.dea_serial_dea',
                                      'value'=> 'NULL',
                                      'like'=> 1));
	array_push($params,array('field'=>'d.phone_sales_dea',
						     'value'=> 'NO',
						     'like'=> 1));

  $dealerList=$dealer->getDealers($params);
  $smarty->register('dealerList');
  $smarty->assign('container','none');
  $smarty->display('helpers/loadDealersFiltered.'.$_SESSION['language'].'.tpl');
?>
