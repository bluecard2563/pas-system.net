<?php 
/*
	File: loadComissionistsByManager.rpc.php
	Author: Santiago Borja.
	Creation Date: 13/04/2010
*/
Request::setString('serial_mbc');
Request::setString('paymentsFilter');


$comissionistList = User::getComissionistsByManager($db, $serial_mbc, $paymentsFilter);

if(is_array($comissionistList)){
	foreach($comissionistList as &$cl){
		$cl['name_usr'] = utf8_encode($cl['name_usr']);
	}
}

$smarty->register('serial_mbc,comissionistList');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissionistsByManager.'.$_SESSION['language'].'.tpl');
?>