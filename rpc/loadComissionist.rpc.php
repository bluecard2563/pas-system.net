<?php 
/*
File: loadCities.php
Author: Miguel Ponce.
Creation Date: 13/04/2010
*/
$user=new User($db);
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}
if($_POST['serial_cit']){
	$serial_cit=$_POST['serial_cit'];
        if($_POST['all']){
            $comissionistList=$user->getComissionistByCity($_POST['serial_cit']);
        }
        else{
            $comissionistList=$user->getComissionistByCity($_POST['serial_cit'],$_SESSION['cityList']);
        }
}

if(is_array($comissionistList)){
	foreach($comissionistList as &$cl){
		$cl['name_usr']=utf8_encode($cl['name_usr']);
	}
}

$smarty->register('comissionistList,serial_cit,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissionist.'.$_SESSION['language'].'.tpl');
?>