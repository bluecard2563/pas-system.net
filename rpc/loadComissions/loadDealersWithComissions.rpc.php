<?php 
/*
* File: loadDealersWithComissions.php
* Author: Nicolas Flores
* Creation Date: 03/06/2010  

* Description: File which loads all dealers with pending comissions, by city
*/
Request::setInteger('serial_cit');
Request::setString('opt_text');
Request::setString('is_report');
Request::setString('comission_to');


if($serial_cit) {
    $dealer = new Dealer($db);
	if($is_report){
		//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
		if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$dealerList =  $dealer->getDealersByCityWithComissions($serial_cit, $_SESSION['dea_serial_dea'], $comission_to, true);
		} else {
			$dealerList =  $dealer->getDealersByCityWithComissions($serial_cit,"", $comission_to, true);
		}
	}else{
		//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
		if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$dealerList =  $dealer->getDealersByCityWithComissions($serial_cit, $_SESSION['dea_serial_dea']);
		} else {
			$dealerList =  $dealer->getDealersByCityWithComissions($serial_cit);
		}
	}
	if($dealerList){
		 $smarty->register('dealerList');
	}
}
$smarty->register('opt_text,is_report');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissions/loadDealersWithComissions.'.$_SESSION['language'].'.tpl');
?>