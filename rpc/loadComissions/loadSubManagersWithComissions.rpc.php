<?php

/**
 * File: loadSubManagersWithComissions
 * Author: Patricio Astudillo
 * Creation Date: 09/05/2012
 * Last Modified: 09/05/2012
 * Modified By: Patricio Astudillo
 * 
 */

	Request::setString('serial_mbc');
	Request::setString('for_report');
	
	$sub_managers = ManagerbyCountry::getSubManagersWithComissions($db, $serial_mbc, $_SESSION['dea_serial_dea'], $for_report);
	
	$smarty->register('sub_managers,for_report');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/loadComissions/loadSubManagersWithComissions.'.$_SESSION['language'].'.tpl');

?>
