<?php
/*
 * File: loadAvailableComissions.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 04/05/2010, 02:46:19 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 04/05/2010, 02:46:19 PM
 */

Request::setString('type');
Request::setString('serial');
Request::setString('dateTo');

$apComission = new AppliedComission($db);
$totalSold = 0;
$totalRefunded = 0;
$totalComission = 0;

switch($type){
	case 'DEALER':
		$dealer=new Dealer($db, $serial);
		$dealer -> getData();
		$salesInfo=$apComission -> getDealersComissionableValues($serial, $dealer -> getLast_commission_paid_dea(), $dateTo);
		if($salesInfo){
			foreach($salesInfo as $s){
				if($s['type_com']=='IN'){
					$totalSold+=$s['value_dea_com'];
				}else{
					$totalRefunded+=$s['value_dea_com'];
				}
			}
		}

		break;
	case 'RESPONSIBLE':
		$salesInfo=$apComission -> getUsersComissionableSales($serial, $dateTo);
		if($salesInfo){
			foreach($salesInfo as $s){
				if($s['type_com']=='IN'){
					$totalSold+=$s['value_ubd_com'];
				}else{
					$totalRefunded+=$s['value_ubd_com'];
				}
			}
		}

		break;
	case 'MANAGER':
		$manager=new ManagerbyCountry($db,$serial);
		$manager -> getData();
		$salesInfo=$apComission -> getManagersComissionableValues($serial, $manager -> getLast_commission_paid_mbc(), $dateTo);
		if($salesInfo){
			foreach($salesInfo as $s){
				if($s['type_com']=='IN'){
					$totalSold+=$s['value_mbc_com'];
				}else{
					$totalRefunded+=$s['value_mbc_com'];
				}
			}
		}

		break;
	case 'SUBMANAGER':
		$dealer=new Dealer($db, $serial);
		$dealer -> getData();
		$salesInfo = $apComission -> getSubManagerComissionableValues($serial, $dateTo);
		if($salesInfo){
			foreach($salesInfo as $s){
				if($s['type_com']=='IN'){
					$totalSold+=$s['value_dea_com'];
				}else{
					$totalRefunded+=$s['value_dea_com'];
				}
			}
		}
		
		break;
	default: //FREELANCE
		$freelance = new Freelance($db, $serial);
		$freelance -> getData();
		$salesInfo = AppliedComission::getFreelanceComissionableValues($db, $freelance->getSerial_fre(), $freelance->getLast_commission_paid_fre(), $dateTo);
		if($salesInfo){
			foreach($salesInfo as $s){
				if($s['type_com']=='IN'){
					$totalSold+=$s['value_fre_com'];
				}else{
					$totalRefunded+=$s['value_fre_com'];
				}
			}
		}
		break;
}
	
$totalComission=$totalSold-$totalRefunded;
$totalComission=number_format($totalComission, 2, '.', '');
$totalSold=number_format($totalSold, 2, '.', '');
$totalRefunded=number_format($totalRefunded, 2, '.', '');

$smarty -> register('salesInfo,totalComission,totalSold,totalRefunded,type,serial');
$smarty -> assign('container','none');
$smarty -> display('helpers/loadComissions/loadAvailableComissions.'.$_SESSION['language'].'.tpl');
?>
