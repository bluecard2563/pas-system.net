<?php
/**
 * File: loadManagersForSubManagers
 * Author: Patricio Astudillo
 * Creation Date: 09/05/2012
 * Last Modified: 09/05/2012
 * Modified By: Patricio Astudillo
 * 
 */

	Request::setString('serial_cou');
	Request::setString('for_report');
	
	$managers = ManagerbyCountry::getMbcWithComissionsForSubManager($db, $serial_cou, $for_report);
	
	$smarty->register('managers,for_report');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/loadComissions/loadManagersForSubManagers.'.$_SESSION['language'].'.tpl');
?>
