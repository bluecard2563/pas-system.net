<?php
/*
    Document   : loadCitiesForComissions.rpc
    Created on : 03-jun-2010, 9:46:45
    Author     : Nicolas
    Description:
       Get all the cities by a country, which has pending comissions
*/
Request::setString('serial_cou');
Request::setString('comission_to');
Request::setString('opt_text');
Request::setString('is_report');
$city=new City($db);
if($serial_cou && $comission_to){
	if($is_report){
	    $cityList=$city->getPaymentCitiesByCountryWithComission($serial_cou,$comission_to,$_SESSION['cityList'],true);
	}else{
		$cityList=$city->getPaymentCitiesByCountryWithComission($serial_cou,$comission_to,$_SESSION['cityList']);
	}
}
$smarty->register('cityList,serial_cou,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissions/loadCitiesForComissions.'.$_SESSION['language'].'.tpl');
?>