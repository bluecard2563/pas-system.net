<?php
/**
 * File: loadManagersForDealerComissions
 * Author: Patricio Astudillo
 * Creation Date: 28-ene-2013
 * Last Modified: 28-ene-2013
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_cou');
	Request::setString('is_report');
	
	$managerList = AppliedComission::getDealersManagersWithComissionsInCountry($db, $serial_cou, $_SESSION['serial_mbc']);
	
	$smarty->assign('container','none');
	$smarty->register('managerList');
	$smarty->display('helpers/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>
