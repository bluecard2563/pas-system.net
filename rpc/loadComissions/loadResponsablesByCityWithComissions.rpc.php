<?php
/*
File: loadResponsablesByCityWithComissions.rpc.php
Author: Nicolas Flores
Creation Date: 03/06/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
Request::setString('is_report');

if($serial_cit) {
    $dealer = new Dealer($db);
	if($is_report){
		$responsablesList =  $dealer->getResponsablesByCityWithComissions($serial_cit,$_SESSION['serial_mbc'],true);
	}else{
		$responsablesList =  $dealer->getResponsablesByCityWithComissions($serial_cit,$_SESSION['serial_mbc']);
	}
	if($responsablesList){
		$smarty->register('responsablesList');
	}
}
$smarty->register('is_report');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissions/loadResponsablesByCityWithComissions.'.$_SESSION['language'].'.tpl');
?>