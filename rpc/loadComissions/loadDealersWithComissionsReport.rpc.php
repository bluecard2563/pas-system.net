<?php
/**
 * File: loadDealersWithComissionsReport
 * Author: Patricio Astudillo
 * Creation Date: 28-ene-2013
 * Last Modified: 28-ene-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('serial_mbc');
	Request::setInteger('serial_usr');
	Request::setInteger('serial_cit');
	Request::setString('commissionsTo');
	Request::setString('is_report');
	
	if(isset($_SESSION['serial_dea'])){
		$serial_dea = $_SESSION['serial_dea'];
	}
	if($commissionsTo == 'SUBMANAGER'){
		$submanager = true;
	}
	
	$dealerList = AppliedComission::getDealersWithComissions($db, $serial_mbc, $serial_cit, $submanager, $serial_usr, $serial_dea);
	
	$smarty->register('dealerList,is_report');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadComissions/loadDealersWithComissions.'.$_SESSION['language'].'.tpl');
?>
