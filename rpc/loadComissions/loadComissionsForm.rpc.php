<?php
/*
 * File: loadComissionsForm.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 04/05/2010, 11:36:58 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 04/05/2010, 11:36:58 AM
 * * Modifies By: Nicolas Flores ($is_report)
 * Last Modified: 01/06/2010, 11:36:58 AM
 */

Request::setString('comissions_to');
Request::setString('is_report');
//check if is for report , if so, get the comission last paid date
$country=new Country($db);
if($is_report){
	$countryList=$country->getOwnCountriesWithComission($_SESSION['countryList'],$comissions_to, $_SESSION['serial_mbc'],true);
}else{
	$countryList=$country->getOwnCountriesWithComission($_SESSION['countryList'],$comissions_to, $_SESSION['serial_mbc']);
}
if($countryList)
	$smarty->register('countryList');
//Debug::print_r($countryList);
$smarty->register('comissions_to,is_report');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissions/loadComissionsForm.'.$_SESSION['language'].'.tpl');
?>
