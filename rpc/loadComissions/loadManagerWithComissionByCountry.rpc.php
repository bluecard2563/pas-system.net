<?php
/*
    Document   : loadManagerWithComissionByCountry.rpc.php
    Created on : 12-may-2010, 10:34:03
    Author     : Nicolas Flores
    Description:
        Load all de managers which has pending comissions, by country.
*/
	Request::setString('serial_cou');
	Request::setString('is_report');
	$managerBC = new ManagerbyCountry($db);
	if($serial_cou){
		if($is_report){
			$managerList=$managerBC->getManagerWithComissionByCountry($serial_cou, $_SESSION['serial_mbc'], TRUE);
		}else{
			$managerList=$managerBC->getManagerWithComissionByCountry($serial_cou, $_SESSION['serial_mbc']);
		}
	}
	if($managerList){
		$smarty->register('managerList');
	}
	$smarty->assign('container','none');
	$smarty->display('helpers/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>
