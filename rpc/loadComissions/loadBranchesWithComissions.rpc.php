<?php 
/*
* File: loadDealersWithComissions.php
* Author: Nicolas Flores
* Creation Date: 03/06/2010  

* Description: File which loads all branches with pending comissions, by dealer
*/
Request::setInteger('serial_dea');
Request::setString('opt_text');
Request::setString('is_report');


if($serial_dea) {
    $dealer = new Dealer($db);
	if($is_report){
		//If $_SESSION['serial_dea'] is set, loads only the user's dealer
		if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$dealerList =  $dealer->getBranchesByDealerWithComissions($serial_dea, $_SESSION['serial_dea'], true);
		} else {
			$dealerList =  $dealer->getBranchesByDealerWithComissions($serial_dea, "", true);
		}
	}else{
		//If $_SESSION['serial_dea'] is set, loads only the user's dealer
		if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$dealerList =  $dealer->getBranchesByDealerWithComissions($serial_dea, $_SESSION['serial_dea']);
		} else {
			$dealerList =  $dealer->getBranchesByDealerWithComissions($serial_dea);
		}
	}
	if($dealerList){
		$smarty->register('dealerList');
	}
}
$smarty->register('opt_text,is_report');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissions/loadBranchesWithComissions.'.$_SESSION['language'].'.tpl');
?>