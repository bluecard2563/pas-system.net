<?php
/**
 * File: loadResponsibleForDealerComissions
 * Author: Patricio Astudillo
 * Creation Date: 28-ene-2013
 * Last Modified: 28-ene-2013
 * Modified By: Patricio Astudillo
 */

Request::setString('serial_mbc');
Request::setString('is_report');

$responsablesList = AppliedComission::getDealersResponsiblesWithComissionsInMBC($db, $serial_mbc);

$smarty->register('is_report,responsablesList');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissions/loadResponsablesByCityWithComissions.'.$_SESSION['language'].'.tpl');
?>
