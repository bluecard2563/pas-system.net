<?php 
/*
File: loadSale.php
Author: Miguel Ponce
Creation Date: 28/01/2010
Last Modified:
Modified By:
Description:
*/

$sale=new Sales($db);
$serial_sal=$_POST['serial_sal'];
$sale->setSerial_sal($serial_sal);
$sale->getData();

$code_lang=$_SESSION['language'];
$language = new Language($db);
$language->getDataByCode($code_lang);
$serial_lang=$language->getSerial_lang();


$total_sal=$sale->getTotal_sal();
$cardNumber_sal=$sale->getCardNumber_sal();
$emissionDate_sal=$sale->getEmissionDate_sal();
$beginDate_sal=$sale->getBeginDate_sal();
$endDate_sal=$sale->getEndDate_sal();
$serial_cus=$sale->getSerial_cus();
$serial_pbd=$sale->getSerial_pbd();

$productByDealer=new ProductByDealer($db);
$productByDealer->setSerial_pbd($serial_pbd);
$productByDealer->getData();
$serial_pxc=$productByDealer->getSerial_pxc();

$productByCountry=new ProductByCountry($db);
$productByCountry->setSerial_pxc($serial_pxc);
$productByCountry->getData();
$serial_pro=$productByCountry->getSerial_pro();

$productbyLanguage=new ProductbyLanguage($db);
$productbyLanguage->setSerial_pro($serial_pro);
$productbyLanguage->setSerial_lang($serial_lang);
$productbyLanguage->getDataByProductAndLanguage();
$name_pbl=$productbyLanguage->getName_pbl();

$product=new Product($db);
$product->setSerial_pro($serial_pro);
$product->getData();

$customer=new Customer($db);
$customer->setserial_cus($serial_cus);
$customer->getData();

$document_cus=$customer->getDocument_cus();
$firstname_cus=$customer->getFirstname_cus();
$lastname_cus=$customer->getLastname_cus();

$currency=new Currency($db, $sale->getSerial_cur());
$currency->getData();
$currency=get_object_vars($currency);
unset($currency['db']);

$smarty->register('total_sal,cardNumber_sal,emissionDate_sal,beginDate_sal,endDate_sal,serial_sal,document_cus');
$smarty->register('firstname_cus,lastname_cus,name_pbl,currency');
$smarty->assign('container','none');

$smarty->display('helpers/loadSale.'.$_SESSION['language'].'.tpl');
?>