<?php
/*
File: loadCurrenciesByCountry.rpc
Author: David Bergmann
Creation Date: 23/03/2010
Last Modified:
Modified By:
*/
$serial_cou = $_POST['serial_cou'];
if($serial_cou) {
    $currency = new CurrenciesByCountry($db);
    $currency->setSerial_cou($serial_cou);
    $currencyList = $currency->getCurrenciesByCountry();
    $currency = $currency->getNonAssignedCurrencyesByCountry();

    $titles = array("#","Pa&iacute;s","Moneda","Moneda Oficial","Estado","Activar/Desactivar");

    if(is_array($currencyList)){
            foreach($currencyList as &$cl){
                $cl['name_cur']=utf8_encode($cl['name_cur']);
                $cl['name_cou']=utf8_encode($cl['name_cou']);
            }
            $smarty->register('currencyList,titles');
    }

    if(is_array($currency)){
            foreach($currency as &$cl){
                $cl['name_cur']=utf8_encode($cl['name_cur']);
            }
            $smarty->register('currency');
    }
    $smarty->register('serial_cou');
}
$smarty->assign('container','none');
$smarty->display('helpers/loadCurrenciesByCountry.'.$_SESSION['language'].'.tpl');
?>