<?php 
/*
* File: loadDealersByComissionist.rpc.php
* Author: David Bergmann
* Creation Date: 01/02/2010
* Last Modified:
* Modified By: 
*/

$dealer=new Dealer($db);
if($_POST['serial_cit']){
	$dealerList=  Dealer::getDealersByComissionistAndCity($db, $_POST['serial_cit'], $_POST['serial_usr']);
}else{
	$noData=1;
}

$smarty->register('dealerList,noData');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersByComissionist.'.$_SESSION['language'].'.tpl');
?>