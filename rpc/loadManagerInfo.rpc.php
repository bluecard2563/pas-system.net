<?php 
/*
File: loadManagerInfo.php
Author: Gabriela Guerrero
Creation Date: 01/07/2010
Last Modified:
Modified By:
Description: File which loads data from an specific dealer
*/ 

Request::setString('id_dea');
Request::setInteger('confirmation');

$dealer=new Dealer($db);
$dealer->setSerial_dea($id_dea);

if($dealer->getData()){
	if($confirmation) {
		$data['manager_name'] = $dealer->getManager_name_dea();
		$data['manager_phone'] = $dealer->getManager_phone_dea();
		$data['manager_email'] = $dealer->getManager_email_dea();
		$data['manager_birthday'] = $dealer->getManager_birthday_dea();
	}
}

$smarty->register('data');
$smarty->assign('container','none');
$smarty->display('helpers/loadManagerInfo.'.$_SESSION['language'].'.tpl');
?>