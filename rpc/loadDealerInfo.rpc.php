<?php 
/*
File: loadDealerInfo.php
Author: David Bergmann
Creation Date: 10/01/2010
Last Modified:
Modified By:
Description: File which loads all the data from an specific dealer
*/ 

Request::setString('id_dea');
Request::setInteger('confirmation');

$dealer=new Dealer($db);
$dealer->setSerial_dea($id_dea);

if($dealer->getData()){
	if($confirmation) {
		$data['id_dea'] = $dealer->getId_dea();
		$data['code_dea'] = $dealer->getCode_dea();
		$data['serial_mbc'] = $dealer->getSerial_mbc();
		$data['serial_dlt'] = $dealer->getSerial_dlt();
		$data['category_dea'] = $dealer->getCategory_dea();
		$data['name_dea'] = $dealer->getName_dea();
		$data['status_dea'] = $dealer->getStatus_dea();
		$data['address_dea'] = $dealer->getAddress_dea();
		$data['phone1_dea'] = $dealer->getPhone1_dea();
		$data['phone2_dea'] = $dealer->getPhone2_dea();
		$data['contract_date_dea'] = $dealer->getContract_date_dea();
		$data['fax_dea'] = $dealer->getFax_dea();
		$data['email1_dea'] = $dealer->getEmail1_dea();
		$data['email2_dea'] = $dealer->getEmail2_dea();
		$data['visits_number'] = $dealer->getVisits_number_dea();
	}
	$data['serial_dea'] = $dealer->getSerial_dea();
	$data['manager_rights'] = $dealer->getManager_rights_dea();
	$data['parent_comission'] = $dealer->getPercentage_dea();
	$data['serial_mbc'] = $dealer->getSerial_mbc();
}

//Language
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//Dealer Types
$dealerT=new DealerType($db);
$dTypesList=$dealerT->getDealerTypes(NULL,$language->getSerial_lang());
foreach($dTypesList as &$type) {
	$type['name']=utf8_encode($type['name']);
}

//CATEGORY
$categoryList=$dealer->getCategories();

//STATUS
$statusList=$dealer->getStatusList();

$smarty->register('data,dTypesList,categoryList,statusList');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealerInfo.'.$_SESSION['language'].'.tpl');
?>