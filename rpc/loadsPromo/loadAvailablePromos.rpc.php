<?php
/*
 * File: loadAvailablePromos.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 07/06/2010, 11:18:03 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 10/06/2010, 11:18:03 AM
 */

Request::setString('applied_to_prm');

$promoList=Promo::getAvailablePromos($db, $applied_to_prm);

$smarty->register('promoList');
$smarty->assign('container','none');
$smarty->display('helpers/loadsPromo/loadAvailablePromos.'.$_SESSION['language'].'.tpl');
?>
