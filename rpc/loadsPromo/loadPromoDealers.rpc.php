<?php
/*
File: loadPromoDealers.rpc.php
Author: Patricio Astudillo
Creation Date: 04/02/2010
LastModified:
Modified By:
*/


$pbc=new ProductByCountry($db);
$dealer=new Dealer($db);

$productByCountryList=$pbc->getProductByCountrySerials($_POST['serial_cou'], $_POST['products']);
foreach($productByCountryList as $k => $p){
    $productByCountryList[$k]=$p['serial_pxc'];
}
$productByCountryList=implode(',', $productByCountryList);
$dealerList=$dealer->getPromoDealers($_POST['serial_cit'], $productByCountryList, $_POST['serial_dealers']);


if(is_array($dealerList)){
    echo '<div align="center">Existentes</div>';
    echo '<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" city="'.$_POST['serial_cit'].'">';
    foreach($dealerList as &$dl){
	$dl['name_dea']=utf8_encode($dl['name_dea']);
        echo '<option value="'.$dl['serial_dea'].'" city="'.$_POST['serial_cit'].'">'.$dl['name_dea'].'</option>';
    }
    echo '</select>';
}else{
    echo '<div align="center">Existentes</div>';
    echo '<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" city="'.$_POST['serial_cit'].'">';
    echo '</select>';
    echo 'No existen comercializadores en la ciudad escogida';
}
?>
