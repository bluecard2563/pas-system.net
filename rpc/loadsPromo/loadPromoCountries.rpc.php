<?php 
/*
File: loadPromoCountries.rpc.php
Author: Patricio Astudillo
Creation Date: 18/02/2010
Last Modified: 18/02/2010
Modified By: Patricio Astudillo
*/

Request::setString('serial_zon');
Request::setString('serial_pro');

if($serial_zon){
    $country=new Country($db);
    $countryList=$country->getPromoCountries($serial_zon,$_SESSION['countryList'], $serial_pro);


    if(is_array($countryList)){
            foreach($countryList as &$cl){
                    $cl['name_cou']=utf8_encode($cl['name_cou']);
            }
    }
}

$smarty->register('countryList,serial_zon,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadCountries.'.$_SESSION['language'].'.tpl');
?>