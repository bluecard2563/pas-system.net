<?php
/*
 * File: reasignPrizeStockrpc.php
 * Author: Patricio Astudillo
 * Creation Date: 14/06/2010, 12:53:21 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 14/06/2010, 12:53:21 PM
 */

Request::setString('serials');
Request::setString('quantity');

$serials=explode(',',$serials);
$quantity=explode(',',$quantity);
$prize=new Prize($db);
$result=false;

if($serials){
	foreach($serials as $key=>$p){
		$prize->setSerial_pri($p);

		if($prize->getData()){
			$prize->setStock_pri($quantity[$key]+1);
			if($prize->update()){
				$result=true;
			}else{
				echo 'Update Failed';
				break;
			}
		}else{
			echo 'Get Data Failed';
			break;
		}
	}

	if($result){
		echo 'true';
	}else{
		echo 'false';
	}
}else{
	echo 'No Array Given';
}
?>
