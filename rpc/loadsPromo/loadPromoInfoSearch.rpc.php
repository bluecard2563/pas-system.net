<?php
/*
File: loadPromoInfo.rpc.php
Author: Patricio Astudillo
Creation Date: 04/02/2010
LastModified: 04/02/2010
Modified By: Patricio Astudillo
*/

Request::setString('appliedTo');
$zone=new Zone($db);
$zoneList=$zone->getZones();

$smarty->register('zoneList,appliedTo');
$smarty->assign('container','none');
$smarty->display('helpers/loadsPromo/loadPromoInfoSearch.'.$_SESSION['language'].'.tpl');
?>
