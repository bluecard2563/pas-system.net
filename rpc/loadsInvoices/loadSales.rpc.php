<?php
/*
File:loadSales.rpc.php
Author: Nicolas Flores
Creation Date:04/03/2010
Modified By:
Last Modified: 
*/
  Request::setInteger('serial_dea');
  Request::setInteger('serial_cou');
  $branch= new Dealer($db,$serial_dea);
  $branch->getData();

  $serial_man = ManagerbyCountry::getManagerByCountryGeneralData($db, $branch->getSerial_mbc());
  $serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $serial_man, 'LEGAL_ENTITY', 'INVOICE');

  $salesList=$branch->getSalesForInvoice();
  $tax=new TaxesByProduct($db);
  $erp_connection = NULL;
  
  if(is_array($salesList)){
	  foreach($salesList as &$sl){
		$tax->setSerial_pxc($sl['serial_pxc']);
		$taxes=$tax->getTaxesByProduct();
		$serials=array();
		$names=array();
		if($taxes){
			foreach($taxes as $k=>&$t){
				$serials[$k]=$t['serial_tax'];
				$names[$k]=$t['name_tax'];
			}
			$sl['taxes']=implode(',', $serials);
			$sl['names_taxes']=implode('<br />', $names);
		}else{
			$sl['taxes']=0;
		}
		
		$erp_connection = $sl['serial_man'];
	  }
	  
	  $erp_connection = ERPConnectionFunctions::invoiceHasERPConnection($branch->getSerial_mbc());
	  $total_pages=(int)(count($salesList)/10)+1;
  }
  
  $smarty->register('salesList,global_salesStatus,serial_dbm,total_pages,erp_connection');
  $smarty->assign('container','none');
  $smarty->display('helpers/loadsInvoices/loadSales.'.$_SESSION['language'].'.tpl');
?>
