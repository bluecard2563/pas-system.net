<?php
/*
File: loadInvoiceNumber.rpc.php
Author: Santiago Benitez
Creation Date: 22/03/2010
*/

$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $_POST['serial_man'], $_POST['type_dbm'], 'INVOICE');

if($serial_dbm){
	$number=DocumentByManager::retrieveNextDocumentNumber($db, $_POST['serial_man'], $_POST['type_dbm'], 'INVOICE', FALSE);
	$number.="";
	//add zeros until the number has 8 digits.
	while(strlen($number)<8){
		$number='0'.$number;
	}
	$smarty->register('number,serial_dbm');
}

$smarty->assign('container','none');
$smarty->display('helpers/loadsInvoices/loadInvoiceNumber.'.$_SESSION['language'].'.tpl');
?>
