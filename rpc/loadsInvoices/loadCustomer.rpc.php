<?php
/*
File: loadCustomer.rpc.php
Author: Santiago Benitez
Creation Date: 18/03/2010
*/

	Request::setString('document_validation');
	Request::setString('small_container');
			
	$customer = new Customer($db);
	$typesList = $customer->getAllTypes();
	$sessionLanguage = $_SESSION['serial_lang'];

	if($_POST['document_cus']){
        $document_cus = $_POST['document_cus'];
        $countryList = Country::getAllAvailableCountries($db);
		if(is_array($countryList)){
			foreach($countryList as &$cl){
				$cl['name_cou']=utf8_encode($cl['name_cou']);
            }
		}
        if($customer -> existsCustomer($document_cus, NULL)){
			$customer -> setserial_cus($customer -> getCustomerSerialbyDocument($document_cus));
            $customer -> getData();

            $customerData['serial_cus']=$customer->getSerial_cus();
            $customerData['document_cus']=$customer->getDocument_cus();
            $customerData['firstname_cus']=$customer->getFirstname_cus();
            $customerData['lastname_cus']=$customer->getLastname_cus();
            $customerData['serial_cit']=$customer->getSerial_cit();
            $customerData['birthdate_cus']=$customer->getBirthdate_cus();
            $customerData['address_cus']=$customer->getAddress_cus();
            $customerData['phone1_cus']=$customer->getPhone1_cus();
            $customerData['phone2_cus']=$customer->getPhone2_cus();
            $customerData['cellphone_cus']=$customer->getCellphone_cus();
            $customerData['email_cus']=$customer->getEmail_cus();
            $customerData['relative_cus']=$customer->getRelative_cus();
            $customerData['relative_phone_cus']=$customer->getRelative_phone_cus();
            $customerData['vip_cus']=$customer->getVip_cus();
            $customerData['type_cus']=$customer->getType_cus();
            $customerData['type_cus']=$customer->getType_cus();           

            $city = new City($db);
            $customerData['serial_cou']= Country::getCountryByCity($db, $customerData['serial_cit']);
			$cityList = $city -> getCitiesByCountry($customerData['serial_cou'], NULL);
        }
    }
    
    $smarty->register('customerData,countryList,cityList,typesList,document_cus,document_validation,small_container');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsInvoices/loadCustomer.'.$_SESSION['language'].'.tpl');
?>