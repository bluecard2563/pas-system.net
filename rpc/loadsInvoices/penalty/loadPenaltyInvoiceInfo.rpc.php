<?php
/*
 * File: loadPenaltyInvoiceInfo.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 21/06/2010, 01:41:51 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 21/06/2010, 01:41:51 PM
 */

Request::setString('number_card');

$sale=new Sales($db);
$sale->setCardNumber_sal($number_card);
$sale->getDataByCardNumber();

if($sale->getSerial_sal()){
    if(GlobalFunctions::isMyCard($db, $sale->getSerial_sal(), $_SESSION['serial_mbc'])){
	if(!CreditNote::hasPenaltyCreditNote($db, $sale->getSerial_sal())){
		if($sale->getSerial_inv()){
			if($sale->getStatus_sal() == 'REGISTERED' || $sale->getStatus_sal() == 'STANDBY'){
				$invoice=new Invoice($db, $sale->getSerial_inv());
				$sale=get_object_vars($sale);
				unset($sale['db']);
				if($invoice->getData()){
					if($invoice->getSerial_pay()==''){
						$customer=new Customer($db, $sale['serial_cus']);
						$customer->getData();

						$auxData['product']=$sale['aux']['product'];
						$auxData['customer']=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
						$auxData['dealer']=$sale['aux']['name_dea'];
					}else{
						$paymentDetail=new PaymentDetail($db);
						$paymentDetail->setSerial_pay($invoice->getSerial_pay());
						$details = $paymentDetail->getPaymentDetails();
						$paid=false;

						if($details){
							foreach($details as $d){
								if($d['comments_pyf']!='N.C. por Penalidad'){
									$paid=true;
								}
							}
						}

						if($paid){//CARD ALREADY PAID
							$error=4;
						}else{//THERE ISN'T A PAYMENT FORM OR ONLY PENALTY CREDIT NOTES
							$customer=new Customer($db, $sale['serial_cus']);
							$customer->getData();

							$auxData['product']=$sale['aux']['product'];
							$auxData['customer']=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
							$auxData['dealer']=$sale['aux']['name_dea'];
						}
					}

					//CHANGE REQUESTED FOR CLIENT: LUIS SALVADOR 12/22/2010 09:23am
					//IF THE CARD IS IN STAND-BY WE NEVER SHOW THE INTERNATIONAL_FEE SECTION.
					if($sale['status_sal'] != 'STANDBY'){
						/* VERIFY IF THE CARD IS ALREADY IN COVERGARE TIME */
						$begin_coverage=split('/', $sale['begin_date_sal']);
						$begin_coverage=strtotime($begin_coverage['2'].'/'.$begin_coverage['1'].'/'.$begin_coverage['0']);
						$today_time=strtotime(date('Y/m/d'));

						if($begin_coverage<=$today_time){
							$began_coverage='YES';
						}else{
							$began_coverage='NO';
						}
						/* END COVERAGE */
					}else{
						$began_coverage='NO';
					}
				}else{ //The invoice info didn't load.
					$error=3;
				}
			}else{//Sale isn't Registered
				$error=5;
				$status=$sale->getStatus_sal();
			}
		}else{ //The card isn't invoiced.
			$error=2;
		}
	}else{ //Already Has penalty invoice
		$error = 6;
	}
    }else{ //Card isn't mine
        $error = 7;
    }
}else{ //The card doesn't exist.
	$error=1;
}

$smarty->assign('container','none');
$smarty->register('error,sale,available,auxData,status,number_card,began_coverage');
$smarty->display('helpers/loadsInvoices/penalty/loadPenaltyInvoiceInfo.'.$_SESSION['language'].'.tpl');
?>
