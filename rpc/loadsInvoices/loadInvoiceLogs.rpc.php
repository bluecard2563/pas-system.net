<?php
/*
File: loadInvoiceLogs.rpc.php
Author: Santiago Benitez
Creation Date: 12/04/2010
*/
$invoiceLog=new InvoiceLog($db);
$invoiceLogList=$invoiceLog->getInvoiceLogsByManager($_POST['serial_mbc'],$_POST['type_dbc']);
$serial_mbc=$_POST['serial_mbc'];

$smarty->register('invoiceLogList,serial_mbc');
$smarty->assign('container','none');
$smarty->display('helpers/loadsInvoices/loadInvoiceLogs.'.$_SESSION['language'].'.tpl');
?>
