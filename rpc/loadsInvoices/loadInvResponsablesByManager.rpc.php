<?php
/*
File: loadInvResponsablesByManager.rpc.php
Author: Santiago Benitez
Creation Date: 20/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
$manager=new ManagerbyCountry($db,$serial_mbc);

if($serial_mbc) {
    $responsablesList=$manager->getInvoiceResponsablesByManager();
}

$smarty->register('responsablesList');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByManager.'.$_SESSION['language'].'.tpl');
?>