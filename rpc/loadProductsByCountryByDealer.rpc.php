<?php
/*
File: loadProductsByCountryByDealer.rpc
Author: David Bergmann
Creation Date: 20/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');
Request::setString('serial_dea');

$productByCountry = new ProductByCountry($db);
$productList = $productByCountry->getProductsByCountry($serial_cou, $_SESSION['serial_lang'], "ACTIVE");

$dealer = new Dealer($db, $serial_dea);
$dealer->getData();

$productByDealer = new ProductByDealer($db);
$assignedProduct = $productByDealer->getproductByDealer($dealer->getDea_serial_dea(), $_SESSION['serial_lang'],NULL, "ACTIVE");

if(is_array($productList)) {
	foreach($productList as &$p) {
		$p['name_pbl'] = utf8_encode($p['name_pbl']);
		if(is_array($assignedProduct)) {
			foreach($assignedProduct as $p2) {
				if($p['serial_pxc'] == $p2['serial_pxc']) {
					$p['active'] = active;
				}
			}
		}
	}
}

$smarty->register('productList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadProductsByCountryByDealer.'.$_SESSION['language'].'.tpl');
?>