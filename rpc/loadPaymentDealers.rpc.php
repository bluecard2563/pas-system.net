<?php
/*
File: loadPaymentDealers.rpc.php
Author: Santiago Benitez
Creation Date: 19/04/2010
LastModified:
Modified By:
*/
Request::setInteger('serial_usr');
Request::setString('dealer_cat');
Request::setInteger('serial_cit');

if(!$dealer_cat || $dealer_cat == 'false'){
    $dealer_cat = null;
}
if(!$serial_usr || $serial_usr == 'false'){
    $serial_usr = null;
}

$dealer=new Dealer($db);

if(isset($_SESSION['dea_serial_dea'])){
	$dealerList=$dealer->getPaymentDealers($serial_cit,$serial_usr,$dealer_cat, $_SESSION['dea_serial_dea']);
}else{
	$dealerList=$dealer->getPaymentDealers($serial_cit,$serial_usr,$dealer_cat);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersFiltered.'.$_SESSION['language'].'.tpl');
?>