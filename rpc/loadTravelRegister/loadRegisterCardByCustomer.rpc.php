<?php

/*
  File:loadCorpCardByCustomer
  Author: Nicolas Flores
  Creation Date:29.03.2010
  Modified By: Patricio Astudillo
  Last Modified: 27/04/2012
 */

Request::setInteger("serial_cus");
Request::setString("front_type");
$customer = new Customer($db, $serial_cus);

if ($front_type == 'LOSS_RATE') {
	$list = $customer->getRegisterCardByCustomer(TRUE);
} else {
	if($front_type == 'report'){
		$list = $customer->getRegisterCardByCustomer(TRUE, FALSE);
	}
	else{
	$list = $customer->getRegisterCardByCustomer(TRUE, TRUE);
	}
	
	}

$smarty->assign('container', 'none');
$smarty->register('list,front_type,serial_cus');
$smarty->display('helpers/loadTravelRegister/loadRegisterCardByCustomer.' . $_SESSION['language'] . '.tpl');
?>
