<?php
/*
  File:loadCardDescription.rpc.php
  Author: Nicolas Flores
  Creation Date:08.04.2010
  Modified By:
  Last Modified:
 */

	Request::setInteger("card_number");
	Request::setString("front_type");
	$type = $_POST['front_type'];
	
        $sale = new Sales($db);
	$sale->setCardNumber_sal($card_number);
	$cardInfo = $sale->getCardInfo($type);
	
	if (!$front_type && $cardInfo['status_sal'] == 'VOID') {
		$cardInfo = false;
	}

        //Validate sale date
        $cardValidation = $sale->validateDates();
        
	$smarty->assign('container', 'none');
	$smarty->register('cardInfo,front_type,cardValidation');
	$smarty->display('helpers/loadTravelRegister/loadCardDescription.' . $_SESSION['language'] . '.tpl');
?>
