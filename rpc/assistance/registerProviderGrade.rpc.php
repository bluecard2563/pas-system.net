<?php

/*
  Document   : registerProviderGrade
  Created on : 15/09/2011, 10:45:48 AM
  Author     : Crifa-516
 */
Request::setString("serial_fle,serial_spv,grade,comment");
$commentServiceProvider = new CommentsByServiceProvider($db);
$commentServiceProvider->setSerial_fle($serial_fle);
$commentServiceProvider->setSerial_spv($serial_spv);
$commentServiceProvider->setSerial_usr($_SESSION['serial_usr']);
$commentServiceProvider->setObservations_csp(specialchars($comment));
$commentServiceProvider->setCalification_csp($grade);

if($commentServiceProvider->insert()){
	echo json_encode(true);
}else{
	echo json_encode(false);
}
?>
