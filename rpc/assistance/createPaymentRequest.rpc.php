<?php
/*
File: createPaymentRequest.rpc
Author: David Bergmann
Creation Date: 19/05/2010
Last Modified: 21/05/2010
Modified By: David Bergmann
*/

Request::setString('serial_fle');
Request::setString('serial_usr');
Request::setString('serial_spv');

$file = new File($db,$serial_fle);
$file->getData();

$paymentRequest = new PaymentRequest($db);
$paymentRequest->setSerial_usr($_SESSION['serial_usr']);
$paymentRequest->setSerialMedicalUser($serial_usr);

if($serial_spv) {
    $paymentRequest->setSerial_spv($serial_spv);
    $paymentRequest->setPayment_to_prq("PROVIDER");
} else {
    $paymentRequest->setPayment_to_prq("CUSTOMER");
}
$paymentRequest->setSerial_fle($serial_fle);
$paymentRequest->setStatus_prq("AUDIT_REQUEST");

/*E-MAIL INFO*/
$user = new User($db,$serial_usr);
$user->getData();
$misc['userName']=$user->getFirstname_usr()." ".$user->getLastname_usr();
$misc['serial_fle'].=$serial_fle;
$misc['email'] = $user->getEmail_usr();
/*END E-MAIL INFO*/

if($paymentRequest->insert()) {
    GlobalFunctions::sendMail($misc,'medicalAuditRequest');
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>