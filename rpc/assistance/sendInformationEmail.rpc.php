<?php
/*
File: sendInformationEmail.rpc
Author: David Bergmann
Creation Date: 09/08/2010
Last Modified:
Modified By:
*/

Request::setString('card_number');
Request::setString('serial_fle');
Request::setString('diagnosis');

$misc['card_number'] = $card_number;
$misc['file_number'] = $serial_fle;
$misc['diagnosis'] = utf8_decode($diagnosis);
$misc['date'] = date("d/m/Y");
$misc['time'] = date("H:i");

$file = new File($db, $serial_fle);
$file->getData();

$sale = new Sales($db, $file->getSerial_sal());
$sale->getData();
$prodByDea = new ProductByDealer($db, $sale->getSerial_pbd());
$prodByDea->getData();
$prodByCou = new ProductByCountry($db, $prodByDea->getSerial_pxc());
$prodByCou->getData();
$product = new Product($db, $prodByCou->getSerial_pro());
$productInfo = $product->getInfoProduct($_SESSION['language']);

$misc['product'] = $productInfo[0]['name_pbl'];


$user = new User($db, $file->getOpening_usr());
$user->getData();

$misc['counter'] = $user->getFirstname_usr()." ".$user->getLastname_usr();

$customer = new Customer($db, $file->getSerial_cus());
$customer->getData();

$misc['customer'] = $customer->getFirstname_cus()." ".$customer->getLastname_cus();

$user = new User($db, $_SESSION['serial_usr']);
$user->getData();

$misc['user'] = $user->getFirstname_usr()." ".$user->getLastname_usr();

$paramBody = new Parameter($db,'21');
$paramBody->getData();
$arr = unserialize($paramBody->getValue_par());
//die(Debug::print_r($arr));
$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);

if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
	$usersCountry='62';
}
$user_list = explode(",",$arr[$usersCountry]);
if(!is_array($user_list)) {
	$user_list = explode(",",$arr[62]);
}

$misc['email_receivers'] = array();

foreach($user_list as $u) {
	$user = new User($db, $u);
	$user->getData();
	$email = $user->getEmail_usr();
	array_push($misc['email_receivers'], $email);
}

$paramBody = new Parameter($db,'31');
$paramBody->getData();
$email_list = unserialize($paramBody->getValue_par());

foreach($email_list as $e) {
	array_push($misc['email_receivers'], $e);
}

if(GlobalFunctions::sendMail($misc, 'file_report')){
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}

?>