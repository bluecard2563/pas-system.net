<?php

/*
  Document   : sendToLiquidation.rpc.php
  Created on : 23/06/2011
  Author     : Nicolas Flores
  Description:
  changes to in_liquidation a register in file
 * Parameters:
 * serial_fle: serial of the file

 */
Request::setInteger('serial_fle');
if (!$serial_fle) {
	echo json_encode(false);
	die;
}

$file = new File($db, $serial_fle);
$file->getData();

$file->setStatus_fle('in_liquidation');
if ($file->update()){
	$tempFLog = FilesLog::getActiveLogForFileAndStatus($db, $serial_fle, 'open');
	
	if($tempFLog){
		$closingFLog = new FilesLog($db, $tempFLog['id']);
		$closingFLog ->setFinished_user($_SESSION['serial_usr']);
		$closingFLog ->save();
	}
	
	$openingFLog = new FilesLog($db);
	$openingFLog->setSerial_fle($serial_fle);
	$openingFLog->setStatus('in_liquidation');
	$openingFLog->setOpening_user($_SESSION['serial_usr']);
	$openingFLog->save();
	
	echo json_encode(true);
}else{
	echo json_encode(false);
}
?>
