<?php
/**
 * File: simpleLoadFilesByCard
 * Author: Patricio Astudillo
 * Creation Date: 26/09/2011, 11:28:03 AM
 * Last Modified: 26/09/2011, 11:28:03 AM
 * Modified By:
 */

	Request::setInteger('card_number');
		
	$files_list = File::getFilesToVoid($db, $card_number);
	
	$smarty -> register('files_list');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/assistance/simpleLoadFilesByCard.'.$_SESSION['language'].'.tpl');
?>
