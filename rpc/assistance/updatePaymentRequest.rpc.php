<?php
/*
File: updatePaymentRequest.rpc
Author: David Bergmann
Creation Date: 10/02/2011
Last Modified:
Modified By:
*/

Request::setString('serial_prq');
Request::setString('presented');
Request::setString('authorized');

$paymentRequest = new PaymentRequest($db,$serial_prq);
$paymentRequest->getData();
$paymentRequest->setAmount_reclaimed_prq($presented);
$paymentRequest->setAmount_authorized_prq($authorized);

if($paymentRequest->update()) {
    echo json_encode(TRUE);
} else {
    echo json_encode(FALSE);
}

?>