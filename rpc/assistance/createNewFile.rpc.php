<?php
/*
File: createNewFile.rpc
Author: David Bergmann
Creation Date: 09/04/2010
Last Modified:
Modified By:
*/

Request::setString('serial_sal');
Request::setString('serial_trl');
Request::setString('serial_cus');

$file = new File($db);
$file->setOpening_usr($_SESSION['serial_usr']);
$file->setSerial_cus($serial_cus);
$file->setSerial_sal($serial_sal);
if($serial_trl) {
    $file->setSerial_trl($serial_trl);
}

$user = new User($db, $_SESSION['serial_usr']);
$user->getData();

if($file->insert()){
    $serial_fle = $db->insert_ID();
    $blogHeader="Usuario: ".$user->getFirstname_usr()." ".$user->getLastname_usr().utf8_decode("\nFecha de creación: ").date("j/n/Y H:i:s");
    $date = date('d/m/Y H:i:s');

    $blog = new Blog($db);
    //CLIENT BLOG
    $blog->setSerial_usr($_SESSION['serial_usr']);
    $blog->setSerial_fle($serial_fle);
    $blog->setType_blg('CLIENT');
    $blog->setEntry_blg($blogHeader);
    $blog->setDate_blg($date);

    //FILE LOG
	$openingFLog = new FilesLog($db);
	$openingFLog->setSerial_fle($serial_fle);
	$openingFLog->setStatus('open');
	$openingFLog->setOpening_user($_SESSION['serial_usr']);
	$openingFLog->save();

    if($blog->insert()){
        //OPERATOR BLOG
        $blog->setType_blg('OPERATOR');
        if($blog->insert()){
            echo json_encode(array('serial_fle'=>$serial_fle));
        } else {
            echo json_encode(false);
        }
    } else {
        echo json_encode(false);
    }
} else {
    echo json_encode(false);
}
?>
