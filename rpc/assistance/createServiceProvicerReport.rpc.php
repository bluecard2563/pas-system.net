<?php
/*
File: createServiceProvicerReport.rpc
Author: David Bergmann
Creation Date: 17/05/2010
Last Modified: 18/05/2010
Modified By: David Bergmann
*/
ini_set('memory_limit','256M');
ini_set('max_execution_time','3600');
set_include_path(get_include_path().PATH_SEPARATOR.DOCUMENT_ROOT.'lib/Excel/Classes/');
require_once("PHPExcel.php");
require_once ("PHPExcel/Writer/Excel5.php");
require_once ("PHPExcel/Reader/Excel5.php");



/*******************************************
 * EXCEL FILE CREATION BY SERVICE PROVIDER *
 *******************************************/
$file = new File($db,$_POST['hdnSerialFle']);
$file->getData();

$serviceProvider = new ServiceProvider($db,$_POST['serial_spv']);
$serviceProvider->getData();

$serviceProviderByFile = new ServiceProviderByFile($db);
$benefit_list = $serviceProviderByFile->getBenefitsByServiceProviderByFile($file->getSerial_fle(),$serviceProvider->getSerial_spv());
$service_list = $serviceProviderByFile->getServicesByServiceProviderByFile($file->getSerial_fle(), $serviceProvider->getSerial_spv());
global $global_system_name;

if(is_array($benefit_list)) {
    //Objeto PHPExcel
    $objPHPExcel = new PHPExcel();

    //SET PROPERTIES
    $objPHPExcel->getProperties()->setCreator($global_system_name);
    $objPHPExcel->getProperties()->setTitle("Nuevo expediente #".$file->getSerial_fle());
    $objPHPExcel->getProperties()->setSubject("Asistencias");
    $objPHPExcel->getProperties()->setDescription("Informaci&oacute;n General de Nuevo Expediente Ingresado");
    $objPHPExcel->getProperties()->setCategory("Reporte");

    $objPHPExcel->createSheet();
    $objPHPExcel->getActiveSheet()->setTitle('Formulario de Nuevo Caso');

    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Calibri');
    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(9);
    $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(10.71);
    $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15.75);

    $column_array = array('B','C','D','E','F','G','H','I','J','K','L','M','N','O');

    //CELL STYLES
    foreach($column_array as $ca) {
        for($i=2;$i<7;$i++) {
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //TITLE ROWS
            if($i==2 || $i==4 || $i==6) {
                $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getFill()->getStartColor()->setARGB('00DFDFDF');
            }

        }
    }
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(26.25);

    //GET DATA
    $sale = new Sales($db, $file->getSerial_sal());
    $sale->getData();
    $city = new City($db,$sale->getSerial_cit());
    $city->getData();
    $country = new Country($db, $city->getSerial_cou());
    $country->getData();

    $city2 = new City($db,$_POST['selCity']);
    $city2->getData();
    $country2 = new Country($db, $city2->getSerial_cou());
    $country2->getData();

    $customer = new Customer($db,$sale->getSerial_cus());
    $customer->getData();
    
    //SET DATA
    $objPHPExcel->getActiveSheet()->setCellValue('B2',"NÚMERO DE CASO");
    $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->setCellValue('B3',$_POST['card_number']);
    
    $objPHPExcel->getActiveSheet()->mergeCells('C2:D2');
    $objPHPExcel->getActiveSheet()->setCellValue('C2',"PAÍS DE ORIGEN");
    $objPHPExcel->getActiveSheet()->mergeCells('C3:D3');
    $objPHPExcel->getActiveSheet()->setCellValue('C3',utf8_encode($country->getName_cou()));

    $objPHPExcel->getActiveSheet()->mergeCells('E2:F2');
    $objPHPExcel->getActiveSheet()->setCellValue('E2',"NOMBRE COMPLETO DEL TITULAR");
    $objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->mergeCells('E3:F3');
    $objPHPExcel->getActiveSheet()->setCellValue('E3',utf8_encode($customer->getFirstName_cus()).' '.utf8_encode($customer->getLastName_cus()));
    
    $objPHPExcel->getActiveSheet()->mergeCells('G2:H2');
    $objPHPExcel->getActiveSheet()->setCellValue('G2',"UBICACIÓN (CIUDAD/PAÍS)");
    $objPHPExcel->getActiveSheet()->mergeCells('G3:H3');
    $objPHPExcel->getActiveSheet()->setCellValue('G3',$city2->getName_cit().'/'.$country2->getName_cou());
    

    $objPHPExcel->getActiveSheet()->mergeCells('I2:N2');
    $objPHPExcel->getActiveSheet()->setCellValue('I2',"FECHA DE NACIMIENTO");
    $objPHPExcel->getActiveSheet()->setCellValue('I3',"DÍA");
	if(substr($customer->getBirthdate_cus (),0,2)>0) {
		$objPHPExcel->getActiveSheet()->setCellValue('J3',substr($customer->getBirthdate_cus (),0,2));
	} else {
		$objPHPExcel->getActiveSheet()->setCellValue('J3',"-");
	}
    $objPHPExcel->getActiveSheet()->setCellValue('K3',"MES");
	if(substr($customer->getBirthdate_cus (),3,2)>0){
		$objPHPExcel->getActiveSheet()->setCellValue('L3',$global_weekMonthsTwoDigits[substr($customer->getBirthdate_cus (),3,2)]);
	} else {
		$objPHPExcel->getActiveSheet()->setCellValue('L3',"-");
	}
    
    $objPHPExcel->getActiveSheet()->setCellValue('M3',"AÑO");
	if(substr($customer->getBirthdate_cus (),6)>0) {
		$objPHPExcel->getActiveSheet()->setCellValue('N3',substr($customer->getBirthdate_cus (),6));
	} else {
		$objPHPExcel->getActiveSheet()->setCellValue('N3',"-");
	}
    $objPHPExcel->getActiveSheet()->setCellValue('O2',"DEDUCIBLE");
    //IF PRODUCT HAS DEDUCTIBLE
    //$objPHPExcel->getActiveSheet()->setCellValue('03','--');
    //ELSE
    $objPHPExcel->getActiveSheet()->setCellValue('O3','$0');

    //CONTACT INFO
    $row=4;
    $contact_info = unserialize($file->getContanct_info_fle());
    if(is_array($contact_info)) {
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':D'.$row);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,"NOMBRE DE CONTACTO");
        $objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':H'.$row);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,"TELÉFONOS DE CONTACTO");
        $objPHPExcel->getActiveSheet()->mergeCells('I'.$row.':O'.$row);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row,"DIRECCIÓN COMPLETA");
        $row++;
        foreach($contact_info as $ci) {
            foreach($column_array as $ca) {
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':D'.$row);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$ci['name']);
            $objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':H'.$row);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,$ci['phone']);
            $objPHPExcel->getActiveSheet()->mergeCells('I'.$row.':O'.$row);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row,$ci['address']);
            $row++;
        }
    }
    //END CONTACT INFO

    //BENEFITS STYLES
    foreach($column_array as $ca) {
        for($i=$row;$i<$row+5;$i++) {
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //TITLE ROWS
            if($i==$row || $i==$row+2 || $i==$row+4) {
                $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$i)->getFill()->getStartColor()->setARGB('00DFDFDF');
            }

        }
    }

    $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':J'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,"DESCRIPCIÓN DEL PROBLEMA (SIGNOS/SINTOMAS)");
    $objPHPExcel->getActiveSheet()->mergeCells('K'.$row.':O'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$row,"TIPO DE ASISTENCIA");
    $row++;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':J'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$_POST['txtDiagnosis']);
    $objPHPExcel->getActiveSheet()->mergeCells('K'.$row.':O'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$row,$_POST['txtAssistanceType']);
    $row++;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':O'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,"ANTECEDENTES MÉDICOS Y ALERGIAS");
    $row++;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':O'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,utf8_encode($_POST['txtMedicalBackground']));
    $row++;

    //SET BENEFITS & SERVICES
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,"BENEFICIO / SERVICIO");
    $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':O'.$row);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"LÍMITE DE COBERTURA (USD)");
    $row++;

    //Benefits
    if(is_array($benefit_list)) {
        foreach($benefit_list as $bl) {
            $total += $bl['estimated_amount_spf'];
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
            $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':O'.$row);
            foreach($column_array as $ca) {
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,utf8_encode($bl['description_bbl']));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$bl['estimated_amount_spf']);
            $row++;
        }
    }

    //Services
    if(is_array($service_list)) {
        foreach($service_list as $sl) {
            $total += $sl['estimated_amount_spf'];
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
            $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':O'.$row);
            foreach($column_array as $ca) {
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
                $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,utf8_encode($sl['name_sbl']));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$sl['estimated_amount_spf']);
            $row++;
        }
    }

    $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
    $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':O'.$row);
    foreach($column_array as $ca) {
        $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
        $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
        $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
        $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
        $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    }
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,"TOTAL");
    $objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$total);

    $row++;
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,"OBSERVACIONES / NOTAS");
    $objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getFill()->getStartColor()->setARGB('00DFDFDF');

    $i=0;
    while($i<3) {
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':O'.$row);
        foreach($column_array as $ca) {
            $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
            $objPHPExcel->getActiveSheet()->getStyle($ca.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $row++;
        $i++;
    }

    $archivo = $serviceProvider->getName_spv()." - Expediente ".$_POST['hdnSerialFle'];

    //SAVE FILE
    $objWriter = new PHPExcel_Writer_Excel5 ($objPHPExcel);
    $objWriter->save($archivo.'.xls');
    $objReader = new PHPExcel_Reader_Excel5();
    $objPHPExcel = $objReader->load($archivo.'.xls');

    echo $archivo.'.xls';
} else {
    echo false;
}
/***********************************************
 * END EXCEL FILE CREATION BY SERVICE PROVIDER *
 ***********************************************/
?>