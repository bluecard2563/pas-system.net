<?php
/*
File: addAssistanceLocations.rpc
Author: David Bergmann
Creation Date: 15/04/2010
Last Modified:
Modified By:
*/
Request::setInteger('serial_fle');
Request::setString('contact_name');
Request::setString('contact_phone');
Request::setString('contact_address');

$titles = array("#","Nombre","Tel&eacute;fono(s)","Direcci&oacute;n","Fecha");

$file = new File($db,$serial_fle);
$file->getData();
//die(Debug::print_r($file));
$contact_info = unserialize($file->getContanct_info_fle());

if(is_array($contact_info)) {
    $aux=array('name'=>utf8_decode($contact_name),
               'phone'=>$contact_phone,
               'address'=> utf8_decode($contact_address),
               'date'=>date('d-m-Y'));
    array_unshift($contact_info,$aux);
} else {
    $contact_info = array( array('name'=>utf8_decode($contact_name),
                                 'phone'=>$contact_phone,
                                 'address'=>utf8_decode($contact_address),
                                 'date'=>date('d-m-Y')));
}
$file->setContanct_info_fle(serialize($contact_info));

if($file->update()){
    $smarty->register('titles,contact_info');
    $smarty->assign('container','none');
    $smarty->display('helpers/assistance/addAssistanceLocations.'.$_SESSION['language'].'.tpl');
} else {
    echo "Se produjo un error. Por favor vuelva a intentarlo.";
}
?>