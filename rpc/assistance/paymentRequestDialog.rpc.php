<?php
/*
File: paymentRequestDialog.rpc
Author: David Bergmann
Creation Date: 19/05/2010
Last Modified:
Modified By:
*/

$parameter = new Parameter($db,'25');
$parameter->getData();
$auditor_array = unserialize($parameter->getValue_par());

if(is_array($auditor_array)) {
    $country_list=array();
    foreach($auditor_array as $key => $a) {
        $country = new Country($db,$key);
        $country->getData();
        array_push($country_list, array('serial_cou'=>$key, 'name_cou'=>$country->getName_cou()));
    }
}

$smarty->register('country_list');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/paymentRequestDialog.'.$_SESSION['language'].'.tpl');

?>
