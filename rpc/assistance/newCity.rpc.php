<?php
/**
 * File: newCity
 * Author: Patricio Astudillo
 * Creation Date: 13-nov-2012
 * Last Modified: 13-nov-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('serial_cou');
	Request::setString('name_cit');
	Request::setString('code_cit');

	$city = new City($db);
	
	if(!$city->cityExists(strtolower($name_cit), $serial_cou)){
		if(!$city->cityCodeExists(strtolower($code_cit))){
			$city->setSerial_cou($serial_cou);
			$city->setName_cit(utf8_decode($name_cit));		
			$city->setCode_cit(utf8_decode($code_cit));
			
			$serial_cit = $city->insert();
			
			if($serial_cit){
				echo ('sucess%S%'.$serial_cit);
			}else{
				echo ('La ciudad no pudo ser ingresada. Intente de nuevo.');
			}
		}else{
			echo utf8_encode('El c�digo ingresado ya existe!');
		}
	}else{
		echo ('El nombre ingresado ya existe!');
	}
	
?>
