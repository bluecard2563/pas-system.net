<?php
/*
File: loadBenefitsByServiceProvider.rpc
Author: David Bergmann
Creation Date: 23/04/2010
Last Modified: 21/06/2010
Modified By: David Bergmann
*/

Request::setInteger('serial_spv');
Request::setInteger('card_number');
Request::setInteger('serial_fle');
Request::setString('edit_dialog');
Request::setString('liquidation');
Request::setString('acces_request');
Request::setString('status');

$sale = new Sales($db);
$sale->setCardNumber_sal($card_number);
$sale->getDataByCardNumber();

/*CARD BENEFITS*/
$titles = array("Beneficio / Servicio","Monto M&aacute;ximo Disponible ($)","Monto Estimado ($)");
$titles_liq = array("Beneficio / Servicio","Monto M&aacute;ximo Disponible ($)","Monto Estimado ($)","Restricci&oacute;n");
$benefits = File::getBenefitsInFile($db, $serial_fle);

if(!$benefits){
	$benefits = Sales::getCardBenefits($db,$card_number);
}
$totalMaxAmount = 0;
if(is_array($benefits)) {
    foreach ($benefits as &$b) {
        $b['description_bbl'] = utf8_encode($b['description_bbl']);
        $totalMaxAmount+=$b['price_bxp'];
        if($b['price_bxp']>0) {
            $b['price_bxp'] = $b['price_bxp'] - ServiceProviderByFile::getUsedAmountByBenefit($db,$serial_fle,$b['serial_bxp']);
        } else {
            $b['price_bxp'] = utf8_encode($b['alias_con']);
        }
    }
}

/*CARD SERVICES*/
$serviceTitles = array("#","Servicio","Monto");
$servicesByCountryBySale = new ServicesByCountryBySale($db);
$counter = new Counter($db, $sale->getSerial_cnt());
$counter->getData();
$dealer = new Dealer($db, $counter->getSerial_dea());
$dealer->getData();
$services = $servicesByCountryBySale->getServicesBySale($sale->getSerial_sal(), $dealer->getDea_serial_dea());
if(is_array($services)) {
    foreach ($services as &$s) {
        $s['name_sbl'] = utf8_encode($s['name_sbl']);
        $totalMaxAmount+=$s['coverage_sbc'];
        $s['coverage_sbc'] = $s['coverage_sbc'] - ServiceProviderByFile::getUsedAmountByService($db,$serial_fle,$s['serial_sbc']);
    }
}
$totalMaxAmount = number_format($totalMaxAmount, 2, ".","");

/*ServiceProviderByFile*/
/*Benefits*/
$serviceProviderByFile = new ServiceProviderByFile($db);
$benefitsByServiceProvider = $serviceProviderByFile->getBenefitsByServiceProviderByFile($serial_fle, $serial_spv);

if(is_array($benefitsByServiceProvider)) {
    $totalEstimatedAmount = 0;
    $benefitsList = array();
    foreach ($benefitsByServiceProvider as $bsp) {
        $medical_tpe = $bsp['medical_type_spf'];
        $benefitsList[$bsp['serial_bxp']] = $bsp;
        $totalEstimatedAmount+=$bsp['estimated_amount_spf'];
    }
}
/*Services*/
$servicesByServiceProvider = $serviceProviderByFile->getServicesByServiceProviderByFile($serial_fle, $serial_spv);

if(is_array($servicesByServiceProvider)) {
    $servicesList = array();
    foreach ($servicesByServiceProvider as $ssp) {
        $medical_tpe = $ssp['medical_type_spf'];
        $servicesList[$ssp['serial_sbc']] = $ssp;
        $totalEstimatedAmount+=$ssp['estimated_amount_spf'];
    }
}
$totalEstimatedAmount = number_format($totalEstimatedAmount, 2, ".","");

/*ServiceProvider Assistance Cost*/
$serviceProvider = new ServiceProvider($db, $serial_spv);
    $serviceProvider->getData();

    if($serviceProvider->getType_spv()=="MEDICAL" || $serviceProvider->getType_spv()=="BOTH") {
        if($medical_tpe=="SIMPLE") {
            $assistanceCost=$serviceProvider->getMedical_simple_spv();
        } elseif($medical_tpe=="COMPLEX") {
            $assistanceCost=$serviceProvider->getMedical_complex_spv();
        } else {
			$assistanceCost=$serviceProvider->getTechnical_spv();
		}
    } else {
        $assistanceCost=$serviceProvider->getTechnical_spv();
    }

//PROVIDER GRADE 
$commentsByServiceProvider = new CommentsByServiceProvider($db);
$comment = $commentsByServiceProvider->getCommentsByServiceProviderByFile($serial_fle, $serial_spv);
	

$smarty->register('comment,benefits,benefitsList,services,servicesList,titles,titles_liq,totalMaxAmount,totalEstimatedAmount,serial_spv,edit_dialog,medical_tpe,acces_request,assistanceCost,status');
$smarty->assign('container','none');
if($liquidation) {
    $smarty->display('helpers/assistance/loadBenefitsByServiceProviderLiquidation.'.$_SESSION['language'].'.tpl');
} else {
    $smarty->display('helpers/assistance/loadBenefitsByServiceProvider.'.$_SESSION['language'].'.tpl');
}
?>