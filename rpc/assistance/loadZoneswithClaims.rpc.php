<?php
/**
 * @author: Luis Salvador
 * File: loadZoneswithClaimsrpc
 * Creation Date: 06-oct-2011, 9:47:11
 * Last modified: 06-oct-2011, 9:47:11
 * Modified by:
 */

$document_to = $_POST['document_to'];
$zoneList = PaymentRequest::getZonesWithClaims($db, $document_to);

if(is_array($zoneList)){
	foreach($zoneList as &$cl){
		$cl['name_zon']=utf8_encode($cl['name_zon']);
	}
}

$smarty->register('zoneList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadZonesWithClaims.'.$_SESSION['language'].'.tpl');
?>
