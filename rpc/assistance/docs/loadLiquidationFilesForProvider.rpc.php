<?php

	Request::setInteger('provider_id');
		
	$files_list = CustomLiquidationFunctions::getLiquidationFilesForProvider($db, $provider_id);
	
	$smarty -> register('files_list,provider_id');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/assistance/docs/loadLiquidationFilesForProvider.'.$_SESSION['language'].'.tpl');

