<?php
/**
 * File: customDiagnosisUpdate
 * Author: Patricio Astudillo
 * Creation Date: 25/08/2015
 * Last Modified By: 25/08/2015
 */

	Request::setInteger('file_id');
	Request::setString('diagnosis');

	$file = new File($db, $file_id);
	$file->getData();
	$file->setDiagnosis_fle($diagnosis);
	
	if($file->update()){
		echo 'ok';
	}else{
		echo 'error';
	}