<?php
/*
File: checkPendingRequests.rpc
Author: David Bergmann
Creation Date: 21/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_fle');

if(PaymentRequest::fileHasPendingRequests($db,$serial_fle)) {
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>
