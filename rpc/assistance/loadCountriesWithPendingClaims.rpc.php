<?php
/*
File: loadCountriesWithPendingClaims.rpc
Author: David Bergmann
Creation Date: 17/06/2010
Last Modified:
Modified By:
*/

$serial_zon=$_POST['serial_zon'];
$countryList = PaymentRequest::getCountriesWithPendingClaims($db, $serial_zon);

if(is_array($countryList)){
	foreach($countryList as &$cl){
		$cl['name_cou']=utf8_encode($cl['name_cou']);
	}
}

$smarty->register('countryList,serial_zon');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadCountriesWithPendingClaims.'.$_SESSION['language'].'.tpl');
?>