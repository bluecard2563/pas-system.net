<?php
/*
File: loadCustomersWithPendingClaims.rpc
Author: David Bergmann
Creation Date: 17/06/2010
Last Modified:
Modified By:
*/

$serial_cit=$_POST['serial_cit'];
$customerList = PaymentRequest::getCustomersWithPendingClaims($db, $serial_cit);

if(is_array($customerList)){
	foreach($customerList as &$cl){
		$cl['name_cus']=utf8_encode($cl['name_cus']);
	}
}

$smarty->register('customerList,serial_cit');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadCustomersWithPendingClaims.'.$_SESSION['language'].'.tpl');
?>