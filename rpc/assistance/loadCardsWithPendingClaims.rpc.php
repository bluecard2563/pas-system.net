<?php
/*
File: loadCardsWithPendingClaims.rpc
Author: David Bergmann
Creation Date: 17/06/2010
Last Modified:
Modified By:
*/

$serial_cus=$_POST['serial_cus'];
$cardList = PaymentRequest::getCardsWithPendingClaims($db, $serial_cus);

$smarty->register('cardList,serial_cus');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadCardsWithPendingClaims.'.$_SESSION['language'].'.tpl');
?>