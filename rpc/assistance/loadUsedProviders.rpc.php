<?php
/*
File: loadUsedProviders.rpc
Author: David Bergmann
Creation Date: 16/12/2010
Last Modified:
Modified By:
*/


Request::setString('serial_fle');

$serviceProviderByFile = new ServiceProviderByFile($db);
$data = $serviceProviderByFile->getServiceProvidersByFile($serial_fle);

$smarty->register('data');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadUsedProviders.'.$_SESSION['language'].'.tpl');
?>