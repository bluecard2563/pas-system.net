<?php
/*
File: createMedicalCheckup.rpc
Author: David Bergmann
Creation Date: 19/05/2010
Last Modified: 07/06/2010
Modified By: David Bergmann
*/

Request::setString('serial_fle');
Request::setString('serial_usr');

$file = new File($db,$serial_fle);
$file->getData();

/* GET THE CARD NUMBER */
if(!$file->getSerial_trl()){
	$tr_log=new TravelerLog($db, $file->getSerial_trl());
	$tr_log->getData();
	$card_number=$tr_log->getCard_number_trl();
}else{
	$sale=new Sales($db, $file->getSerial_sal());
	$sale->getData();
	$card_number=$sale->getCardNumber_sal();
}
/* GET THE CARD NUMBER */

$paymentRequest = new PaymentRequest($db);
$paymentRequest->setSerial_usr($_SESSION['serial_usr']);
$paymentRequest->setSerialMedicalUser($serial_usr);

$paymentRequest->setSerial_fle($serial_fle);
$paymentRequest->setStatus_prq("AUDIT_REQUEST");

/*E-MAIL INFO*/
$user = new User($db,$serial_usr);
$user->getData();
$misc['userName']=$user->getFirstname_usr()." ".$user->getLastname_usr();
$misc['serial_fle'].=$serial_fle;
$misc['email'] = $user->getEmail_usr();
$misc['card_number'] = $card_number;
/*END E-MAIL INFO*/

if($paymentRequest->insert()) {
    GlobalFunctions::sendMail($misc,'medicalCheckupRequest');
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>