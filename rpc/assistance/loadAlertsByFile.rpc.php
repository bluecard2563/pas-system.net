<?php
/*
File: loadAlertsByFile.rpc
Author: David Bergmann
Creation Date: 05/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_fle');
$file = new File($db, $serial_fle);
$file->getData();
$alerts = $file->getAlertsByFile();

if(is_array($alerts)) {
    $titles = array('#','Descripci&oacute;n','Fecha de Creaci&oacute;n','Fecha a Atender','Atendido');
 $smarty->register('alerts,titles');
}
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadAlertsByFile.'.$_SESSION['language'].'.tpl');
?>
