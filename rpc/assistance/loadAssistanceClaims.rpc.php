<?php
/*
File: loadAssistanceClaims.rpc
Author: David Bergmann
Creation Date: 11/06/2010
Last Modified: 17/06/2010
Modified By: David Bergmann
*/

Request::setString('document_to_dbf');
Request::setInteger('serial_spv');
Request::setInteger('serial_fle'); 
Request::setString('processAS');

$claimList = PaymentRequest::getPendingClaimsByCountry($db, $document_to_dbf, $processAS, $serial_spv, $serial_fle);
if(is_array($claimList)) {
    foreach ($claimList as &$c) {
        $c['name_dbf'] = utf8_encode($c['name_dbf']);
        $c['name_cus'] = utf8_encode($c['name_cus']);
		if($c['type_prq'] == 'CREDIT_NOTE'){
			$c['amount_authorized_prq'] *= -1;
		}		
		$c['type_prq'] = $global_payment_request_type[$c['type_prq']];
    }
	$count = sizeof($claimList);
}

$titles = array('Documento','Pa&iacute;s','No. Documento','Concepto','Valor Aprobado','Fecha de Carga', 'Fecha del Documento','Cliente','Beneficios','Archivo');

$smarty->register('document_to_dbf,count,claimList,titles,global_billTo');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadAssistanceClaims.'.$_SESSION['language'].'.tpl');
?>
