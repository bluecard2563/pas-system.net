<?php
/*
File: loadFilesByCard.rpc
Author: David Bergmann
Creation Date: 15/06/2010
Last Modified:
Modified By:
*/

Request::setString('card_number');
$card_number = $_POST['card_number'];

$files = PaymentRequest::getFilesByCardWithPendingClaims($db, $card_number);

if(is_array($files)) {
    foreach ($files as &$fl) {
        $fl['name_cou'] = utf8_encode($fl['name_cou']);
        $fl['name_cit'] = utf8_encode($fl['name_cit']);
        $fl['name_cus'] = utf8_encode($fl['name_cus']);
        $fl['diagnosis_fle'] = utf8_encode($fl['diagnosis_fle']);
    }
	$count = sizeof($files);
}



$titles = array("","Expediente #","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia");

$smarty->register('card_number,files,titles,count');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadFilesByCard.'.$_SESSION['language'].'.tpl');
?>