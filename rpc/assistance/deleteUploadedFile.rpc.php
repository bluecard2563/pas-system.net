<?php
/*
File: deleteUploadedFile.rpc
Author: David Bergmann
Creation Date: 18/01/2011
Last Modified:
Modified By:
*/

Request::setString('serial_dbf');

$documentByFile = new DocumentsByFile($db, $serial_dbf);
$documentByFile->getData();

if(!unlink(DOCUMENT_ROOT.$documentByFile->getUrl_dbf())){
	ErrorLog::log($db, 'DELETED DOCUMENT FAILED - '.get_class($documentByFile), $documentByFile->getUrl_dbf());
}

if($documentByFile->delete()){
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}


?>