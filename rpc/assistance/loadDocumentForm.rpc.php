<?php

/*
  Document   : loadDocumentForm
  Created on : 07/09/2011, 09:11:03 AM
  Author     : Crifa-516
 */
Request::setString('serial_fle');

$services = new ServiceProviderByFile($db);
$providers = $services->getServiceProvidersByFile($serial_fle);
$file = new File($db,$serial_fle);
$file->getData();
$serial_cus=$file->getSerial_cus();
// get already registered pending documents
$registeredDocs = PendingDocumentsByFile::getPendingDocumentsByFile($db, $serial_fle);
if(is_array($registeredDocs)){
	foreach($registeredDocs as &$doc){
		if($doc['serial_spv']!=''){
			$doc['pendingFrom']='PROVIDER';
			$provider = new ServiceProvider($db,$doc['serial_spv']);
			$provider->getData();
			$doc['namePendingFrom']=$provider->getName_spv();
		}elseif($doc['serial_cus']!=''){
			$doc['pendingFrom']='CLIENT';
			$customer = new Customer($db,$doc['serial_cus']);
			$customer->getData();
			$doc['namePendingFrom']=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
		}
	}
	$smarty->register('registeredDocs');
}
$smarty->register('providers,serial_cus,serial_fle');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadDocumentForm.'.$_SESSION['language'].'.tpl');
?>
