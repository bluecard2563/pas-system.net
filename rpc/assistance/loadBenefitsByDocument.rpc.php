<?php
/*
File: loadBenefitsByDocument.rpc
Author: David Bergmann
Creation Date: 04/06/2010
Last Modified:
Modified By:
*/

Request::setString('serial_dbf');
Request::setString('serial_prq');
Request::setString('edit');

$benefitList = BenefitsByDocument::getBenefitsByDocuments($db, $serial_dbf);
$serviceList = BenefitsByDocument::getServicesByDocuments($db, $serial_dbf);

//Benefits
if(is_array($benefitList)) {
    foreach ($benefitList as &$bl) {
        $bl['description_bbl'] = utf8_encode($bl['description_bbl']);
        if($bl['price_bxp']==0){
            $bl['price_bxp'] = "N/A";
        } else {
            $bl['price_bxp'] = "$".$bl['price_bxp'];
        }
        if(!$bl['restriction_price_bxp']){
            $bl['restriction_price_bxp'] = "-";
        } else {
            $bl['restriction_price_bxp'] = '$'.$bl['restriction_price_bxp'];
        }
    }
    $smarty->register('benefitList');
}


//Services
if(is_array($serviceList)) {
    die("entra");
    foreach ($serviceList as &$sl) {
        $sl['name_sbl'] = utf8_encode($sl['name_sbl']);
        $sl['coverage_sbc'] = "$".$sl['coverage_sbc'];
    }
    $smarty->register('serviceList');
}

$titles = array('Beneficio','Monto M&aacute;ximo','Restricci&oacute;n','Monto Presentado','Monto Aprobado');

$smarty->register('titles,serial_dbf,serial_prq,edit');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadBenefitsByDocument.'.$_SESSION['language'].'.tpl');
?>
