<?php
/*
File: loadCitiesWithPendingClaims.rpc
Author: David Bergmann
Creation Date: 17/06/2010
Last Modified:
Modified By:
*/

$serial_cou=$_POST['serial_cou'];
$cityList = PaymentRequest::getCitiesWithPendingClaims($db, $serial_cou);

if(is_array($cityList)){
	foreach($cityList as &$cl){
		$cl['name_cit']=utf8_encode($cl['name_cit']);
	}
}

$smarty->register('cityList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadCitiesWithPendingClaims.'.$_SESSION['language'].'.tpl');
?>