<?php
/*
File: loadAlertsDialog.rpc
Author: David Bergmann
Creation Date: 04/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_fle');

$assistanceGroupsByUsers = new AssistanceGroupsByUsers($db, $_SESSION['serial_usr']);
$group_id = $assistanceGroupsByUsers->getCurrentUserGroup();

$table_name = "assistance/fEditAssistance/0/";

$smarty->register('serial_fle,group_id,table_name');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadAlertsDialog.'.$_SESSION['language'].'.tpl');
?>
