<?php
/*
  Document   : loadProviderFileCurrentGrade
  Created on : 14/09/2011, 09:58:55 AM
  Author     : Nicol�s Flores
 */

Request::setString('serial_fle,serial_spv');

$commentDescription = CommentsByServiceProvider::getGradeForServiceProvider($db, $serial_fle, $serial_spv);
$grades = CommentsByServiceProvider::getCalificationTypes($db);

$smarty->register('commentDescription,grades');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadProviderFileCurrentGrade.'.$_SESSION['language'].'.tpl');
?>
