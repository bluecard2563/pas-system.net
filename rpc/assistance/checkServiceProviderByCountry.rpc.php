<?php
/*
File: checkServiceProviderByCountry.rpc
Author: David Bergmann
Creation Date: 19/04/2010
Last Modified:
Modified By:
*/

$servProvByCountry = new ServiceProviderByCountry($db);

if($servProvByCountry->existServiceProviders()) {
    echo json_encode(true);
} else {
    echo json_encode(false);
}

?>
