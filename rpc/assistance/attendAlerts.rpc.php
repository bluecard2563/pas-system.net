<?php
/*
File: attendAlerts.rpc
Author: David Bergmann
Creation Date: 19/05/2010
Last Modified:
Modified By:
*/

	Request::setString('serial_alt');
	
	$alert = new Alert($db, $serial_alt);
	$alert -> getData();
	
	$alert -> setStatus_alt('DONE');
	
	if($alert -> update()) {
	    echo json_encode(true);
	}else{
	    echo json_encode(false);
	}
?>
