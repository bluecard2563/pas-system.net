<?php

/*
  Document   : receivePendingDocument
  Created on : 12/09/2011, 10:36:54 AM
  Author     : Crifa-516
 */

Request::setString('serial_pdfs');
if($serial_pdfs){
	if(PendingDocumentsByFile::receiveDocumentForFile($db,$serial_pdfs,$_SESSION['serial_usr'],TRUE))
		echo json_encode(true);
	else
		echo json_encode(false);
}else{
	echo json_encode(false);
}
?>
