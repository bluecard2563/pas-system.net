<?php
/*
File: cancelMedicalCheckups.rpc
Author: David Bergmann
Creation Date: 07/07/2010
Modified By:
Last Modified:
*/

Request::setString('serial_fle');

$request = PaymentRequest::getPendingMedicalCheckupsByFile($db, $serial_fle);

if(is_array($request)) {
	$paymentRequest = new PaymentRequest($db, $request['serial_prq']);
	$paymentRequest->getData();
	$paymentRequest->setStatus_prq('AUDITED');
	$paymentRequest->setAuditor_comments_prq("Expediente cerrado antes de ser auditado.");

	if($paymentRequest->update()) {
		echo json_encode(TRUE);
	} else {
		echo json_encode(FALSE);
	}
} else {
	echo json_encode(TRUE);
}


?>