<?php
/*
File: addServiceProviderByFile.rpc
Author: David Bergmann
Creation Date: 27/04/2010
Last Modified: 21/05/2010
Modified By: David Bergmann
*/



Request::setInteger('serial_bxp');
Request::setInteger('serial_fle');
Request::setInteger('serial_spv');
Request::setInteger('serial_sbc');
Request::setString('estimated_amt');
Request::setString('real_amt');
Request::setString('medical_tpe');

$serviceProviderByFile = new ServiceProviderByFile($db);
$serial_spf = $serviceProviderByFile->serviceProviderExists($serial_fle,$serial_spv,$serial_bxp,$serial_sbc);

if($serial_spf) {
    $serviceProviderByFile = new ServiceProviderByFile($db,$serial_spf);
    $serviceProviderByFile->getData();

    $serviceProviderByFile->setSerial_bxp($serial_bxp);
    $serviceProviderByFile->setSerial_fle($serial_fle);
    $serviceProviderByFile->setSerial_spv($serial_spv);
    $serviceProviderByFile->setSerial_sbc($serial_sbc);
    $serviceProviderByFile->setEstimated_amount_spf($estimated_amt);
    $serviceProviderByFile->setMedical_type_spf($medical_tpe);

    if($real_amt) {
        $serviceProviderByFile->setReal_amount_spf($real_amt);
    }

    if($serviceProviderByFile->update()){
        if($serial_bxp) {
            echo json_encode($serial_bxp);
        } else if($serial_sbc) {
            echo json_encode($serial_sbc);
        }
    } else {
        echo json_encode(FALSE);
    }
} else {
    $serviceProviderByFile = new ServiceProviderByFile($db);
	
    $serviceProviderByFile->setSerial_bxp($serial_bxp);
    $serviceProviderByFile->setSerial_fle($serial_fle);
    $serviceProviderByFile->setSerial_spv($serial_spv);
    $serviceProviderByFile->setSerial_sbc($serial_sbc);
    $serviceProviderByFile->setEstimated_amount_spf($estimated_amt);
    $serviceProviderByFile->setMedical_type_spf($medical_tpe);

    if($real_amt) {
        $serviceProviderByFile->setReal_amount_spf($real_amt);
    }

    if($serviceProviderByFile->insert()){
        if($serial_bxp) {
            echo json_encode($serial_bxp);
        } else if($serial_sbc) {
            echo json_encode($serial_sbc);
        }
    } else {
        echo json_encode(FALSE);
    }
}
?>