<?php
/*
File: loadBeneficiaries.rpc
Author: David Bergmann
Creation Date: 02/02/2011
Last Modified:
Modified By:
*/

Request::setString('card_number');
Request::setString('serial_sal');
set_time_limit(0);
/*CARD BENEFICIARIES*/
$data['customer'] = Sales::getCardBeneficiaries($db,$card_number);
$file = new File($db);
if(is_array($data['customer'])) {
	foreach ($data['customer'] as &$c) {
		$c['other_cards'] = $file->getOtherCardFilesByCustomer($serial_sal, $c['serial_cus']);
	}
}
/*END CARD BENEFICIARIES*/


$smarty->register('data');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/file/loadBeneficiaries.'.$_SESSION['language'].'.tpl');
?>