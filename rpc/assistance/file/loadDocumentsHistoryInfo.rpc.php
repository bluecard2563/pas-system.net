<?php
/**
 * File: loadDocumentsHistoryInfo
 * Author: Patricio Astudillo M.
 * Creation Date: 01/03/2012 01:19:14 PM
 * Last Modified: 01/03/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_fle');
	
	$serviceProviderByFile = new ServiceProviderByFile($db);
	$providerList = $serviceProviderByFile->getServiceProvidersByFile($serial_fle);

	
	$smarty->register('providerList');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/assistance/file/loadDocumentsHistoryInfo.'.$_SESSION['language'].'.tpl');
?>
