<?php
/*
File: loadBenefits.rpc
Author: David Bergmann
Creation Date: 02/02/2011
Last Modified:
Modified By:
*/

Request::setString('card_number');
Request::setString('serial_sal');
Request::setString('serial_dea');

/*CARD BENEFITS*/
$data['benefit_titles'] = array("#","Beneficio","Condiciones","Monto","Restricci&oacute;n");
$data['benefits'] = Sales::getCardBenefits($db,$card_number);

/*CARD SERVICES*/
$data['service_titles'] = array("#","Servicio","Monto");
$servicesByCountryBySale = new ServicesByCountryBySale($db);
$data['services'] = $servicesByCountryBySale->getServicesBySale($serial_sal, $serial_dea);

/*SALES LOG*/
$salesLog = new SalesLog($db);
$salesHistory = $salesLog->getAuthorizadSalesLogBySale($serial_sal);
$sl_titles = array("#","Fecha","Tipo","Estado");
if(is_array($salesHistory)) {
	$smarty->register('salesHistory,sl_titles,global_sales_log_status');
}

$smarty->register('data');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/file/loadBenefits.'.$_SESSION['language'].'.tpl');
?>