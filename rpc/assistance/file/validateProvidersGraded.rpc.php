<?php
/**
 * File: validateProvidersGraded
 * Author: Patricio Astudillo M.
 * Creation Date: 16/02/2012 
 * Last Modified: 16/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_fle');
	
	$grades = CommentsByServiceProvider::getGradeStatusForProviders($db, $serial_fle);
	$providers_grade_status = true;
	if($grades){
		foreach($grades as $g){
			if($g['graded'] == 'NO'){
				$providers_grade_status = false;
				break;
			}
		}
	}else{
		$providers_grade_status = false;
	}		
	
	echo json_encode($providers_grade_status);
?>
