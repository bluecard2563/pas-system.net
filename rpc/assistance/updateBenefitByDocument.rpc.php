<?php
/*
File: updateBenefitByDocument.rpc
Author: David Bergmann
Creation Date: 09/02/2011
Last Modified:
Modified By:
*/

Request::setString('serial_dbf');
Request::setString('serial_spf');
Request::setString('presented');
Request::setString('authorized');

$benefitByDocument = new BenefitsByDocument($db, $serial_spf, $serial_dbf, $authorized, $presented);

if($benefitByDocument->update()) {
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}
?>