<?php
/*
File: loadContactsByServiceProvider.rpc
Author: David Bergmann
Creation Date: 12/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_spv');
$contactByServiceProvider = new ContactByServiceProvider($db);
$contactList = $contactByServiceProvider->getContactsByServiceProviderInfo($serial_spv,'ASSISTANCE');
$titles = array('Pa&iacute;s','Ciudad','Operador','Tel&eacute;fono','E-Mail 1','E-Mail 2');

if(is_array($contactList)) {
    foreach ($contactList as &$cl) {
        $cl['name_cou'] = utf8_encode($cl['name_cou']);
        $cl['name_cit'] = utf8_encode($cl['name_cit']);
        $cl['name_csp'] = utf8_encode($cl['name_csp']);
    }
    $smarty->register('contactList,titles');
}

//die(Debug::print_r($contactList));

$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadContactsByServiceProvider.'.$_SESSION['language'].'.tpl');
?>
