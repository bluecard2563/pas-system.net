<?php
/*
Author: David Bergmann
Modified date: 29/07/2010
Last modified:
*/

Request::setString('serial_sal');
Request::setString('serial_cou');
Request::setString('serial_cus');

$customer = new Customer($db, $serial_cus);
$customer->getData();
$city = new City($db, $customer->getSerial_cit());
$city->getData();

if(Product::checkEmissionCountry($db, $serial_sal, $serial_cou) && Product::checkResidenceCountry($db, $serial_sal, $serial_cou, $city->getSerial_cou())) {
	echo json_encode(1);
} elseif(Product::checkEmissionCountry($db, $serial_sal, $serial_cou)) {
	echo json_encode(2);
} elseif(Product::checkResidenceCountry($db, $serial_sal, $serial_cou, $city->getSerial_cou())) {
	echo json_encode(3);
} else {
	echo json_encode(false);
}
?>