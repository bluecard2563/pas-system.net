<?php
/*
File: loadMedicalAuditors.rpc
Author: David Bergmann
Creation Date: 21/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');

$parameter = new Parameter($db,'25');
$parameter->getData();
$auditor_array = unserialize($parameter->getValue_par());

if(is_array($auditor_array)) {
        $auditor_array[$serial_cou] = explode(",", $auditor_array[$serial_cou]);
        $auditor_list = array();

        if(is_array($auditor_array[$serial_cou])) {
            foreach ($auditor_array[$serial_cou] as $key =>$a) {
                $user = new User($db, $a);
                $user->getData();
                $paymentRequest = new PaymentRequest($db);
                array_push($auditor_list, array('serial_usr'=>$a,
                                                'name_usr'=>utf8_encode($user->getFirstname_usr())." ".utf8_encode($user->getLastname_usr()),
                                                'pending_req'=>$paymentRequest->getAmountOfRequestsByUser($a)));
            }
            $titles = array('#','Nombre','Auditor&iacute;as pendientes','Asignar');
            $smarty->register('titles,auditor_list');

            
        }
}
$smarty->register('serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadMedicalAuditors.'.$_SESSION['language'].'.tpl');

?>
