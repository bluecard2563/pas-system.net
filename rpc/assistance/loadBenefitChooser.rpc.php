<?php
/*
File: loadBenefitChooser.rpc
Author: David Bergmann
Creation Date: 04/06/2010
Last Modified: 22/06/2010
Modified By: David Bergmann
*/

if(!$customUploadScript){
	Request::setString('serial_fle');
	Request::setString('serial_spv');
}

/*ServiceProviderByFile*/
$serviceProviderByFile = new ServiceProviderByFile($db);
$benefitsByServiceProvider = $serviceProviderByFile->getBenefitsByServiceProviderByFile($serial_fle, $serial_spv);
$servicesByServiceProvider = $serviceProviderByFile->getServicesByServiceProviderByFile($serial_fle, $serial_spv);

/*Benefits*/
if(is_array($benefitsByServiceProvider)) {
    foreach ($benefitsByServiceProvider as &$b) {
        $b['description_bbl'] = utf8_encode($b['description_bbl']);
        if($b['price_bxp']==0){
            $b['price_bxp'] = "N/A";
        } else {
            $b['price_bxp'] = $b['price_bxp'];
        }
        if(!$b['restriction_price_bxp']){
            $b['restriction_price_bxp'] = "-";
        } else {
            $b['restriction_price_bxp'] = '$'.$b['restriction_price_bxp'];
        }
    }
}

/*Services*/
if(is_array($servicesByServiceProvider)) {
    foreach ($servicesByServiceProvider as &$s) {
        $s['name_sbl'] = utf8_encode($s['name_sbl']);
        $s['coverage_sbc'] = '$'.$s['coverage_sbc'];
    }
}

$titles = array('Beneficio','Monto M&aacute;ximo','Restricci&oacute;n','Monto Presentado','Monto Aprobado / Negado');

$smarty->register('benefitsByServiceProvider,servicesByServiceProvider,titles');

if(!$customUploadScript){
	$smarty->assign('container','none');
	$smarty->display('helpers/assistance/loadBenefitChooser.'.$_SESSION['language'].'.tpl');
}
?>