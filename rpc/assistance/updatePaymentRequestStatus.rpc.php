<?php
/*
File: updatePaymentRequestStatus.rpc
Author: David Bergmann
Creation Date: 08/06/2010
Last Modified:
Modified By:
*/
Request::setString('serial_prq');
Request::setString('status_prq');

$paymentRequest = new PaymentRequest($db,$serial_prq);
$paymentRequest->getData();
$paymentRequest->setStatus_prq($status_prq);

if($paymentRequest->update()) {
    echo json_encode(TRUE);
} else {
    echo json_encode(FALSE);
}

?>