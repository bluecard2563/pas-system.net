<?php

/*
  Document   : loadGradeProviderForm
  Created on : 12/09/2011, 04:44:41 PM
  Author     : Crifa-516
 */
Request::setString('serial_fle');
if($serial_fle){
	$spv = new ServiceProviderByFile($db);
	$fileComments = $spv->getServiceProvidersByFile($serial_fle);
	$grades=CommentsByServiceProvider::getCalificationTypes($db);
}	

$smarty->register('fileComments,grades,serial_fle');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadGradeProviderForm.'.$_SESSION['language'].'.tpl');
?>
