<?php
/*
File: loadUploadedFiles.rpc
Author: David Bergmann
Creation Date: 03/06/2010
Last Modified: 08/06/2010
Modified By: David Bergmann
*/

Request::setString('serial_fle');
Request::setString('acces_permition');
Request::setString('serial_spv');
Request::setString('extended');

/*UPLOADED FILES*/
$fileList = DocumentsByFile::getDocumentsInfoByFile($db, $serial_fle,$serial_spv);
if(is_array($fileList)) {
    foreach ($fileList as &$fl) {
        /*$fl['name_dbf'] = utf8_encode($fl['name_dbf']);
        $fl['comment_dbf'] = utf8_encode($fl['comment_dbf']);
        $fl['url_dbf'] = utf8_encode($fl['url_dbf']);
        $fl['name_spv'] = utf8_encode($fl['name_spv']);
        $fl['observations_prq'] = utf8_encode($fl['observations_prq']);*/
		$fl['file_url_prq'] = str_replace(DOCUMENT_ROOT, '', $fl['file_url_prq']);
		$fl['type_prq'] = $global_payment_request_type[$fl['type_prq']];
    }
}
$data['fileList'] = $fileList;

$types_prq = PaymentRequest::getPaymentRequestTypes($db);

$titles = array('Documento','No. Documento','Monto Presentado ($)','Monto Aprobado / Negado ($)',
				'Pago','Fecha del Documento','Estado','Beneficios','Observaciones','Tipo de Archivo','Archivo');

if($extended){
	array_push($titles, 'Fecha de Pago');
	array_push($titles, 'Ver Pago');
}

$date = getdate();
$data['date'] = $date['mday'].'/'.$date['mon'].'/'.$date['year'];
$smarty->register('titles,data,global_billTo,global_paymentRequestStatus,acces_permition,types_prq');
$smarty->assign('container','none');

if($extended){		//*********** DISPLAY AT HISTORY TAB IN EDITASSISTANCE
	$smarty->display('helpers/assistance/file/loadHistoryDocumentsTable.'.$_SESSION['language'].'.tpl');
}else{				//*********** DISPLAY IN LIQUIDATION SCREEN
	$smarty->display('helpers/assistance/loadUploadedFiles.'.$_SESSION['language'].'.tpl');
}
?>