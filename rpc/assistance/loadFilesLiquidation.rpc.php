<?php
/*
File: loadFilesLiquidation
Author: David Bergmann
Creation Date: 19/01/2011
Last Modified:
Modified By:
*/

Request::setString('card_num');

$files = PaymentRequest::getFilesAndClaimsByCard($db, $card_num);

if(is_array($files)) {
    foreach ($files as &$fl) {
        $fl['name_cus'] = utf8_encode($fl['name_cus']);
    
		/*Gets documents by file*/
		$fl['fileList'] = DocumentsByFile::getDocumentsInfoByFile($db, $fl['serial_fle'], NULL, TRUE);
		if(is_array($fl['fileList'])) {
			foreach ($fl['fileList'] as &$fl) {
				$fl['name_dbf'] = utf8_encode($fl['name_dbf']);
				$fl['comment_dbf'] = utf8_encode($fl['comment_dbf']);
				$fl['url_dbf'] = utf8_encode($fl['url_dbf']);
				$fl['name_spv'] = utf8_encode($fl['name_spv']);
				$fl['observations_prq'] = utf8_encode($fl['observations_prq']);
				$fl['type_prq'] = $global_payment_request_type[$fl['type_prq']];
			}
			$smarty->register('files');
		}
	}
}

$titles = array('Documento','No. Documento','Monto ($)','Valor Aprobado ($)','Pago','Fecha','Estado','Beneficios','Observaciones','Tipo de Archivo','Archivo','Eliminar');




$smarty->register('titles');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadFilesLiquidation.'.$_SESSION['language'].'.tpl');

?>