<?php
/*
  File: createLiquidationDocument.rpc
  Author: David Bergmann
  Creation Date: 17/06/2010
  Last Modified: 12/07/2010
  Modified By: David Bergmann
 */

include_once DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';
$process_result;
Request::setString('chkClaims');

if(!$chkClaims && isset($_POST['chkClaims'])){
	$chkClaims = $_POST['chkClaims'];
}

if ($chkClaims) {

	$paymentRequests = PaymentRequest::getLiquidationDocument($db, $chkClaims);

	//HEADER
	if ($_POST['liquidationType'] == 'preliquidation') {
		if ($_POST['selProvider']) {
			$serviceProvider = new ServiceProvider($db, $_POST['selProvider']);
			$serviceProvider->getData();
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("PRELIQUIDACI&Oacute;N DE SERVICIOS")) . '</b>', 12, array('justification' => 'center'));
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("Coordinador: ")) . '</b>' . $serviceProvider->getName_spv(), 12, array('justification' => 'left'));
			$pdf->ezSetDy(-10);
		} else {
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("PRELIQUIDACI&Oacute;N DE SERVICIOS")) . '</b>', 12, array('justification' => 'center'));
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("Cliente: ")) . '</b>' . $paymentRequests['0']['name_cus'], 12, array('justification' => 'left'));
			$pdf->ezSetDy(-10);
		}
	} else {
		if ($_POST['selProvider']) {
			$serviceProvider = new ServiceProvider($db, $_POST['selProvider']);
			$serviceProvider->getData();
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("LIQUIDACI&Oacute;N DE SERVICIOS")) . '</b>', 12, array('justification' => 'center'));
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("Coordinador: ")) . '</b>' . $serviceProvider->getName_spv(), 12, array('justification' => 'left'));
			$pdf->ezSetDy(-10);
		} else {
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("LIQUIDACI&Oacute;N DE SERVICIOS")) . '</b>', 12, array('justification' => 'center'));
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>' . utf8_decode(html_entity_decode("Cliente: ")) . '</b>' . $paymentRequests['0']['name_cus'], 12, array('justification' => 'left'));
			$pdf->ezSetDy(-10);
		}
	}

	$report = array();

	$totalReclaimed = 0;
	$totalAuthorised = 0;

	if (is_array($paymentRequests)) {
		foreach ($paymentRequests as &$r) {
			$aux = array();
			$aux['col1'] = $r['name_cus'];
			$aux['col2'] = $r['serial_fle'];
			$aux['col3'] = $r['amount_reclaimed_prq'];
			if ($r['type_prq'] == 'CREDIT_NOTE') {
				$aux['col3'] = '0';
				$aux['col9'] = $r['amount_authorized_prq'];
				$aux['col4'] = '0';
			} else {
				$aux['col4'] = $r['amount_authorized_prq'];
				$aux['col9'] = '0';
			}
			$aux['col5'] = $r['card_number'];
                        $aux['col10'] = $r['creation_date_fle'];
			$aux['col6'] = $r['diagnosis_fle'];
			$aux['col7'] = $r['comment_dbf'];
			//benefits
			$benefits = BenefitsByDocument::getBenefitsByDocuments($db, $r['serial_dbf']);
			if (is_array($benefits)) {
				$end = end($benefits);
				foreach ($benefits as $b) {
					$aux['col8'] .= "{$b['description_bbl']} \n                                     (\${$b['amount_authorized_bbd']})";
					if ($b != $end) {
						$aux['col8'] .= ",\n";
					}
				}
			}
			//services
			$services = BenefitsByDocument::getServicesByDocuments($db, $r['serial_dbf']);
			if (is_array($services)) {
				$end = end($services);
				if (is_array($benefits)) {
					$aux['col8'] .= ",\n";
				}
				foreach ($services as $s) {
					$aux['col8'] .= "{$s['name_sbl']} \n                                     (\${$s['amount_authorized_bbd']})";
					if ($s != $end) {
						$aux['col8'] .= ",\n";
					}
				}
			}

			array_push($report, $aux);
			if ($r['type_prq'] == 'CREDIT_NOTE') {
				$totalToRefund += $r['amount_authorized_prq'];
			} else {
				$totalAuthorised += $r['amount_authorized_prq'];
				$totalReclaimed+=$r['amount_reclaimed_prq'];
			}
		}
	}
	$titles = array('col1' => '<b>Cliente</b>',
		'col2' => '<b>Expediente #</b>',
		'col3' => '<b>Valor presentado</b>',
		'col4' => '<b>Valor pagado</b>',
		'col9' => utf8_decode(html_entity_decode('<b>Nota de Cr&eacute;dito</b>')),
		'col5' => '<b>Tarjeta #</b>',
                'col10' => '<b>Fecha de Ocurrencia</b>',
		'col6' => '<b>Causa</b>',
		'col7' => '<b>Documento #</b>',
		'col8' => '<b>Beneficios</b>');
	$pdf->ezTable($report, $titles, '', array('showHeadings' => 1,
		'shaded' => 1,
		'showLines' => 1,
		'xPos' => '428',
		'fontSize' => 10,
		'titleFontSize' => 11,
		'cols' => array(
			'col1' => array('justification' => 'center', 'width' => 105),
			'col2' => array('justification' => 'center', 'width' => 70),
			'col3' => array('justification' => 'center', 'width' => 70),
			'col4' => array('justification' => 'center', 'width' => 70),
			'col9' => array('justification' => 'center', 'width' => 70),
			'col5' => array('justification' => 'center', 'width' => 60),
                        'col10' => array('justification' => 'center', 'width' => 50),
			'col6' => array('justification' => 'center', 'width' => 105),
			'col7' => array('justification' => 'center', 'width' => 70),
			'col8' => array('justification' => 'center', 'width' => 110)
			)));

	$total = array(
		array('col1' => '<b>Sub Totales</b>',
			'col2' => number_format($totalReclaimed, 2),
			'col3' => number_format($totalAuthorised, 2),
			'col4' => number_format($totalToRefund, 2)
		)
	);

	$pdf->ezTable($total, array('col1' => 'd',
		'col2' => 's',
		'col3' => 'd',
		'col4' => '2'), '', array('showHeadings' => 0,
		'shaded' => 0,
		'showLines' => 1,
		'xPos' => '251',
		'fontSize' => 10,
		'titleFontSize' => 11,
		'cols' => array(
			'col1' => array('justification' => 'center', 'width' => 185),
			'col2' => array('justification' => 'center', 'width' => 75),
			'col3' => array('justification' => 'center', 'width' => 75),
			'col4' => array('justification' => 'center', 'width' => 75)
			)));

	$total = array(
		array('col1' => '<b>Total</b>',
			'col2' => '$' . number_format($totalAuthorised - $totalToRefund, 2)
		)
	);

	$pdf->ezTable($total, array('col1' => 'd',
		'col2' => 's'), '', array('showHeadings' => 0,
		'shaded' => 0,
		'showLines' => 1,
		'xPos' => '251',
		'fontSize' => 10,
		'titleFontSize' => 11,
		'cols' => array(
			'col1' => array('justification' => 'center', 'width' => 185),
			'col2' => array('justification' => 'center', 'width' => 225)
			)));


	$pdfcode = $pdf->ezOutput();
	$pdfname = 'liquidacion_' . date('U');
	$fp = fopen(DOCUMENT_ROOT . 'assistanceFiles/assistanceLiquidations/' . $pdfname . '.pdf', 'wb');
	fwrite($fp, $pdfcode);
	fclose($fp);


	if ($_POST['liquidationType'] != 'preliquidation') {
		/* E-MAIL INFO */
		$misc['textForEmail'] = "Se ha realizado la liquidaci&oacute;n de reclamos de asistencias. Informaci&oacute;n en el documento adjunto.";
		$misc['filePath'] = DOCUMENT_ROOT . 'assistanceFiles/assistanceLiquidations/' . $pdfname . '.pdf';

		$parameter = new Parameter($db, '21'); //Parameters for Stand-by Requests.
		$parameter->getData();
		$liquidationUsers = unserialize($parameter->getValue_par());

		$usersCountry = GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);

		if ($usersCountry == '1') { //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
			$usersCountry = '62';
		}
		$liquidationUsers[$usersCountry] = explode(',', $liquidationUsers[$usersCountry]);

		if (is_array($liquidationUsers[$usersCountry])) {
			foreach ($liquidationUsers[$usersCountry] as $key => $p) {
				$user = new User($db, $p);
				$user->getData();
				$misc['email'][$key] = $user->getEmail_usr();
			}
		}

		/*		 * * CHANGE FOR SET COORDINATOR PAYMENTS EMAILS ** */
		if ($payment_emails && sizeof($payment_emails) > 0) {
			$misc['email'] = $payment_emails;
		}
		/* END E-MAIL INFO */
        
        /*echo '<pre>';
        print_r($misc);
        echo '</pre>';
        die();*/

		if (GlobalFunctions::sendMail($misc, 'assistaceLiquidation')) {
			//@unlink(DOCUMENT_ROOT.'assistanceFiles/assistanceLiquidations/'.$pdfname.'.pdf');
			$process_result = json_encode(TRUE);
		} else {
			$process_result = json_encode(FALSE);
		}
	} else {
		$process_result = json_encode($pdfname);
	}
} else {
	$process_result = json_encode(FALSE);
}

if (!$provider_liquidation):
	echo $process_result;
endif;
?>