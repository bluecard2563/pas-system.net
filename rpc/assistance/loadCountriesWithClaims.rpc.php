<?php
/**
 * @author: Luis Salvador
 * File: loadCountriesWithClaimsrpc
 * Creation Date: 06-oct-2011, 9:18:02
 * Last modified: 06-oct-2011, 9:18:02
 * Modified by:
 */
$serial_zon=$_POST['serial_zon'];
$document_to = $_POST['document_to'];
$countryList = PaymentRequest::getCountriesWithClaims($db, $serial_zon, $document_to);

if(is_array($countryList)){
	foreach($countryList as &$cl){
		$cl['name_cou']=utf8_encode($cl['name_cou']);
	}
}

$smarty->register('countryList,serial_zon');
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadCountriesWithClaims.'.$_SESSION['language'].'.tpl');
?>
