<?php
/*
File: uploadFile.rpc
Author: David Bergmann
Creation Date: 27/25/2010
Last Modified: 15/06/2010
Modified By: David Bergmann
*/
Request::setString('serial_fle');
Request::setString('name');
Request::setString('description');
Request::setString('txtTotalPresentedAmount');
Request::setString('totalAuthorizadAmount');
Request::setString('serial_spf');
Request::setString('billTo');
Request::setString('serial_bxp');
Request::setString('authorized_amount:');
Request::setString('presented_amount');
Request::setString('status_prq');
Request::setString('observations');
Request::setString('type_prq');
Request::setString('document_date');
Request::setString('process_as');

if(!$serial_bxp) {
	die("error");
}

$cur_file_name=$_FILES['userfile']['name'];
$file_name = explode(".", $cur_file_name);
$new_file_name="file_".time('U').".".$file_name[sizeof($file_name)-1];
$path=DOCUMENT_ROOT."assistanceFiles/file_$serial_fle/$new_file_name";

$documentByFile = new DocumentsByFile($db);
$documentByFile->setSerial_fle($serial_fle);
$documentByFile->setName_dbf($name);
$documentByFile->setComment_dbf($description);
$documentByFile->setUrl_dbf("assistanceFiles/file_$serial_fle/$new_file_name");
$documentByFile->setDocument_to_dbf($billTo);
$documentByFile->setDocument_date_dbf($document_date);
$serial_dbf = $documentByFile->insert();

if($serial_dbf) {
    //assign benefits to document
    $benefitsByDocument = new BenefitsByDocument($db);
    $benefitsByDocument->setSerial_dbf($serial_dbf);
    $error = false;

    $paymentRequest = new PaymentRequest($db);
    $paymentRequest->setSerial_usr($_SESSION['serial_usr']);
    $paymentRequest->setSerial_fle($serial_fle);
    $paymentRequest->setSerial_dbf($serial_dbf);
    $paymentRequest->setStatus_prq($status_prq);
    $paymentRequest->setAmount_reclaimed_prq($txtTotalPresentedAmount);
    $paymentRequest->setAmount_authorized_prq($totalAuthorizadAmount);
    $paymentRequest->setObservations_prq($observations);
	$paymentRequest->setType_prq($type_prq);
	 $paymentRequest->setProcessAS($process_as);

    if(!$paymentRequest->insert()) {
        die("error");
    }

    $benefits = explode(",", $serial_bxp);
    $authorized_amounts = explode(",", $authorized_amount);
	$presented_amounts = explode(",", $presented_amount);
    
    foreach ($benefits as $key=>$b) {
        $benefitsByDocument->setSerial_spf($b);
        $benefitsByDocument->setAmount_authorized_bbd($authorized_amounts[$key]);
		$benefitsByDocument->setAmount_presented_bbd($presented_amounts[$key]);

        if(!$benefitsByDocument->insert()) {
            $error = true;
        }
    }
    if(!$error) {
        //check if directory for this file exists.
        if(!is_dir(DOCUMENT_ROOT."assistanceFiles/file_$serial_fle")){
            mkdir(DOCUMENT_ROOT."assistanceFiles/file_$serial_fle", 0777);
        }
        //move the file to the final path
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $path)) {
            echo "success";
        } else {
            echo "error";
        }
    } else {
        echo "error";
    }

} else {
    echo "error";
}
?>