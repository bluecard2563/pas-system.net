<?php
/**
 * File: invoiceSelection
 * Author: Patricio Astudillo
 * Creation Date: 10/03/2014
 * Last Modified: 10/03/2014
 * Modified By: Patricio Astudillo
*/

	Request::setInteger('serial_fle');

	$invoice_list = CaseLiquidationGlobals::retrieveNonSettledInvoicesForCase($db, $serial_fle);

	$smarty->register('invoice_list');
	$smarty->assign('container','none');
	$smarty->display('helpers/assistance/liquidation/invoiceSelection.'.$_SESSION['language'].'.tpl');
?>