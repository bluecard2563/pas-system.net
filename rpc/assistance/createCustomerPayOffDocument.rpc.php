<?php
/*
  File: createCustomerPayOffDocument.rpc
  Author: David Bergmann
  Creation Date: 18/06/2010
  Last Modified: 07/08/2013
  Modified By: Patricio Astudillo
 */

$pdf = & new Cezpdf('A4', 'portrait');
//WIDTH: 600	HEIGHT: 800
set_time_limit(36000);
ini_set('memory_limit', '1024M');

$pdf->selectFont(DOCUMENT_ROOT . 'lib/PDF/fonts/Helvetica.afm');
$pdf->ezSetCmMargins(2, 1, 2, 1);
$all = $pdf->openObject();
$pdf->saveState();

$pdf->setColor(0, 0, 0);

$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);
$pdf->restoreState();
$pdf->closeObject();
$pdf->addObject($all, 'all');
$pdf->saveState();

$document = PaymentRequest::getPayOffDocument($db, $_POST['chkClaims']);

if($document['serial_cou'] == 62){
	$company_name = "BLUE CARD ECUADOR S.A.";
} else {
	$company_name = "PLANET ASSIST";
}

//GETTING CARD ATTRIBUTES INFORMATION FOR CUSTOMIZED TEXT
$serial_sal = $document['serial_sal'];
$sale_info = Sales::getSoldProductInformation($db, $serial_sal);
$product_sold = new Product($db, $sale_info['serial_pro']);
$product_sold -> getData();

//HEADER
if($_POST['pre_liquidation']=='yes'){
	$pdf->ezText('<b>'.utf8_decode(html_entity_decode("ACTA DE FINIQUITO Y LIQUIDACI&Oacute;N")).'</b>', 14, array('justification'=>'center'));
}else{
	$pdf->ezText('<b>'.utf8_decode(html_entity_decode("ACTA DE FINIQUITO Y LIQUIDACI&Oacute;N")).'</b>', 14, array('justification'=>'center'));
}

$pdf->ezSetDy(-20);

$text = "Comparecen a la celebración del presente documento, por una parte la compañía $company_name (la \"Compañía\") debidamente representada por quien suscribe";
$text .= " el presente contrato; y, por otra, el señor(a) ".utf8_encode($document['name_cus'])." (el \"Beneficiario\"), quien comparece por sus propios derechos";

//IF THE CARD ADMITS ADDITIONALS
if($product_sold ->getMax_extras_pro() > 0){
	$text .= " y en representación de los beneficiarios menores de edad a los que le corresponda representar, así como de ser el caso, actuando a nombre de ";
	$text .= utf8_encode($document['name_owner'])." (la \"Contratante\") como su representante o agente oficioso";
}

//COMMON ENDING OF THE LETTER
$text .= ", para suscribir la presente Acta de Finiquito y Liquidación (el \"Acta\") al tenor de las cláusulas que siguen:";
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-20);

$pdf->ezText('<b>'.html_entity_decode("PRIMERA: ANTECEDENTES.-").'</b>', 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$text = "1. Con fecha {$document['emission_day']} de {$global_weekMonths[$document['emission_month']]} de {$document['emission_year']}, la Contratante suscribió";
$text .= " el contrato número {$document['card_number']} (el \"Contrato\"), mediante el cual contrató los servicios de asistencia en viajes que presta la Compañía";

//IF THE CARD ADMITS ADDITIONALS
if($product_sold ->getMax_extras_pro() > 0){
	$text .= ", señalando como beneficiario de los mismos al Beneficiario y/o a sus hijos o representados por los cuales pudiera comparecer a suscribir la presente Acta.";
}

$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-10);

$text = "2. Los servicios objeto del Contrato que motivaron la asistencia y/o el reembolso de gastos cuyo cumplimiento da lugar a la suscripción de la presente Acta, son los que se describen en la tabla que consta a continuación:";
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-10);

$document['medical_type_spf'];
$document['type_spv'];

if($document['type_spv'] == "BOTH") {
	if($document['medical_type_spf']) {
		$document['type_spv'] = "MEDICAL";
	} else {
		$document['type_spv'] = "TECHNICAL";
	}
}

$aux = array(array('col1' => '<b>'.utf8_decode('Beneficiario:').'</b>', 'col2' => $document['name_cus'],
                   'col3' => '<b>'.utf8_decode('NÚMERO DE CONTRATO:').'</b>', 'col4' => $document['card_number']),
             array('col1' => '<b>'.utf8_decode('C. I./Pass:').'</b>','col2' => $document['document_cus'],
                   'col3' => '<b>'.utf8_decode('Tipo de servicio:').'</b>','col4' => utf8_decode(html_entity_decode($global_serviceProviderType[$document['type_spv']]))));

$pdf->ezTable($aux, array('col1'=>'', 'col2'=>'', 'col3'=>'', 'col4'=>''),'',
                          array('showHeadings'=>0,
                                'shaded'=>0,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'fontSize' => 10,
                                'titleFontSize' => 11,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>140),
                                        'col2'=>array('justification'=>'center','width'=>130),
                                        'col3'=>array('justification'=>'center','width'=>120),
                                        'col4'=>array('justification'=>'center','width'=>90))));

$pdf->ezSetDy(-15);

$aux = array(array('col1' => '<b>'.utf8_decode('Vigencia de servicio desde:').'</b>', 'col2' => $document['begin_date_sal'],
                   'col3' => '<b>'.utf8_decode('Fecha de ocurrencia:').'</b>','col4' => $document['incident_date_fle']),
             array('col1' => '<b>'.utf8_decode('Vigencia de servicio hasta:').'</b>', 'col2' => $document['end_date_sal']));

$pdf->ezTable($aux, array('col1'=>'', 'col2'=>'', 'col3'=>'', 'col4'=>''),'',
                          array('showHeadings'=>0,
                                'shaded'=>0,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'fontSize' => 10,
                                'titleFontSize' => 11,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>180),
                                        'col2'=>array('justification'=>'center','width'=>90),
                                        'col3'=>array('justification'=>'center','width'=>120),
                                        'col4'=>array('justification'=>'center','width'=>90))));

$pdf->ezSetDy(-20);

$pdf->ezText('<b>'.html_entity_decode("SEGUNDA: REEMBOLSO, ACUERDO Y FINIQUITO.-").'</b>', 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$text = "Con tales antecedentes la Compañía entrega al Beneficiario y éste declara recibir por concepto de reembolso de gastos producidos al amparo del Contrato la cantidad que a continuación se detalla:";
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-20);


//BENEFITS & SERVICES
$titles = array('col1'=>'<b>'.utf8_decode('Beneficio (concepto de reembolso)').'</b>',
				'col2'=>'<b>'.utf8_decode('Monto máximo de cobertura').'</b>',
				'col3'=>'<b>'.utf8_decode('Monto presentado').'</b>',
				'col4'=>'<b>'.utf8_decode('Valor total del reembolso').'</b>');

$aux = array();
$total = 0;
$total_req = 0;
$total_max = 0;

//Benefits
$data = BenefitsByDocument::getBenefitsByPaymentRequest($db, $_POST['chkClaims']);
if(is_array($data)) {
    foreach ($data as $d) {
        $benefit['col1'] = $d['description_bbl'];
		if($d['price_bxp'] > 0) {
			$benefit['col2'] = $d['price_bxp'];
		} else {
			$benefit['col2'] = $d['alias_con'];
		}
		$benefit['col3'] = $d['amount_presented_bbd'];
        $benefit['col4'] = $d['amount_authorized_bbd'];
		$total_req += $d['amount_presented_bbd'];
        $total += $d['amount_authorized_bbd'];
		$total_max+= $d['price_bxp'];
        array_push($aux, $benefit);
    }
}

//Services
$data = BenefitsByDocument::getServicesByPaymentRequest($db, $_POST['chkClaims']);
if(is_array($data)) {
    foreach ($data as $d) {
        $benefit['col1'] = $d['name_sbl'];
		$benefit['col2'] = $d['coverage_sbc'];
        $benefit['col3'] = $d['amount_presented_bbd'];
        $benefit['col4'] = $d['amount_authorized_bbd'];
		$total_req += $d['amount_presented_bbd'];
        $total += $d['amount_authorized_bbd'];
		$total_max+= $d['coverage_sbc'];
        array_push($aux, $benefit);
    }
}

$pdf->ezTable($aux, $titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>1,
                                'xPos'=>'center',
                                'fontSize' => 10,
                                'titleFontSize' => 10,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>220),
										'col2'=>array('justification'=>'center','width'=>100),
                                        'col3'=>array('justification'=>'center','width'=>80),
                                        'col4'=>array('justification'=>'center','width'=>80))));

$totals[0] = array('col1'=>'Total:',
				   'col2'=>number_format($total_max,2,'.',''),
				   'col3'=>number_format($total_req,2,'.',''),
				   'col4'=>number_format($total,2,'.',''));

$pdf->ezTable($totals, array('col1'=>'', 'col2'=>'', 'col3'=>'', 'col4'=>''),'',
                          array('showHeadings'=>0,
                                'shaded'=>0,
                                'showLines'=>1,
                                'xPos'=>'center',
                                'fontSize' => 10,
                                'titleFontSize' => 10,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>220),
										'col2'=>array('justification'=>'center','width'=>100),
                                        'col3'=>array('justification'=>'center','width'=>80),
                                        'col4'=>array('justification'=>'center','width'=>80))));
$pdf->ezSetDy(-20);

//converting total to text.
$numalet= new CNumeroaLetra();
$numalet->setNumero($total);
$numalet->setMayusculas(0);
$numalet->setGenero(1);
$numalet->setMoneda("con");
$numalet->setPrefijo("");
$numalet->setSufijo("");
$totalLetras = $numalet->letra();

$text = "Son: $totalLetras dólares de los Estados Unidos de América";
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-10);

$text = "En virtud de lo expuesto, de la asistencia brindada y del reembolso recibido a satisfacción las partes se otorgan amplio finiquito y declaran que nada tienen que reclamarse en el presente ni en el futuro por la relación comercial descrita en los antecedentes.";
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-10);


$text = "El Beneficiario por sus propios derechos ni por los que representa en la calidad que comparece, declara que no existe ningún reclamo pendiente contra a la Compañía ni sus afiliadas, vinculadas, relacionadas, ni contra sus representantes, funcionarios ni empleados. Así mismo declara que a solicitud de la Compañía, reconocerá ante Juez o Notario, la autenticidad de sus firmas y rúbricas que consigna en el presente contrato.";
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-10);

$text = "Las partes aceptan el contenido íntegro del presente documento, lo suscriben en tres ejemplares de igual contenido y valor, a ".date('d')." de {$global_weekMonthsTwoDigits[date('m')]} de ".date('Y');
$pdf->ezText(utf8_decode($text), 10, array('justification'=>'full'));
$pdf->ezSetDy(-30);

$aux = array(array('col1'=>'<b>'.utf8_decode("Por la Compañía").'</b>',
                   'col2'=>'<b>'.utf8_decode("Beneficiario").'</b>'));

$pdf->ezTable($aux, array('col1'=>'','col2'=>''),'',
                          array('showHeadings'=>0,
                                'shaded'=>0,
                                'showLines'=>1,
                                'xPos'=>'center',
                                'fontSize' => 10,
                                'titleFontSize' => 10,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>200),
                                        'col2'=>array('justification'=>'center','width'=>200))));

$pdfcode = $pdf->ezOutput();
$pdfname = 'acta_finiquito_'.date('U');
$fp=fopen(DOCUMENT_ROOT.'assistanceFiles/assistanceLiquidations/'.$pdfname.'.pdf','wb');
fwrite($fp,$pdfcode);
fclose($fp);

echo json_encode($pdfname);
?>