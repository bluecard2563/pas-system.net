<?php
/*
File: loadFile.rpc
Author: David Bergmann
Creation Date: 17/04/2010
Last Modified: 03/06/2010
Modified By: David Bergmann
*/
Request::setString('serial_fle');
Request::setString('card_number');
Request::setString('status');

$file = new File($db,$serial_fle);
if($file->getData()){
    $data['serial_fle'] = $serial_fle;
    $data['diagnosis_fle'] = $file->getDiagnosis_fle();
    $data['type_fle'] = $file->getType_fle();
    $data['cause_fle'] = $file->getCause_fle();
    $data['serial_cit'] = $file->getSerial_cit();
    $data['type_fle'] = $file->getType_fle();
	$data['incident_date_fle'] = $file->getIncident_date_fle();
	$aux_data = $file->getAux_data();
	$data['medical_type'] = $aux_data['medical_type'];
	$data['service_type'] = $aux_data['type_spv'];

	if($data['service_type'] == "BOTH") {
		if($data['medical_type']) {
			$data['service_type'] = "MEDICAL";
		} else {
			$data['service_type'] = "TECHNICAL";
		}
	}
	$data['medical_background_cus'] = $aux_data['medical_background_cus'];
	$data['name_cit'] = $aux_data['name_cit'];
	$data['serial_cou'] = $aux_data['serial_cou'];
	$data['name_cou'] = $aux_data['name_cou'];
	$fileTypes = File::getTypeValues($db);
}
/*CONTACT INFO*/
$titles = array("#","Nombre","Tel&eacute;fono","Direcci&oacute;n","Fecha");
$contact_info = unserialize($file->getContanct_info_fle());

/*CITY LIST*/
if($file->getSerial_cit()) {
	$city = new City($db);
	$cityList = $city->getCitiesByCountry($data['serial_cou']);
	if(is_array($cityList)){
		foreach($cityList as &$cl) {
			$cl["name_cit"] = utf8_encode($cl["name_cit"]);
		}
	}
}

/*COUNTRY LIST*/
$countryList = Country::getAllAvailableCountries($db);
if(is_array($countryList)){
	foreach($countryList as &$cl) {
		$cl["name_cou"] = utf8_encode($cl["name_cou"]);
	}
}

/*SERVICE PROVIDER LIST*/
if($data['serial_cou'] && $data['service_type']) {
	$serviceProvider = new ServiceProviderByCountry($db);
	$providerList = $serviceProvider->loadServiceProviderByCountry($data['serial_cou'],$data['service_type']);
}

/*BLOG*/
if($status == 'first_open' || $status == 'closed') {
	$blog = new Blog($db);
	$blog->setSerial_fle($serial_fle);
	$blog->getDataByFile("CLIENT");
	$client_blog = utf8_encode(specialchars($blog->getEntry_blg()))."\n\n";
	$blog->getDataByFile("OPERATOR");
	$operator_blog = utf8_encode(specialchars($blog->getEntry_blg()))."\n\n";
} else {
	$blog = new Blog($db);
	$blog->setSerial_fle($serial_fle);

	$user = new User($db, $_SESSION['serial_usr']);
	$user->getData();

	$blogHeader="\n----------------------------------------------------------------------\nUsuario: ";
	$blogHeader.=$user->getFirstname_usr()." ".$user->getLastname_usr()."\nFecha de actualización: ";
	$blog->getDataByFile("CLIENT");
	$client_blog = utf8_encode(specialchars($blog->getEntry_blg()));
	$blog->getDataByFile("OPERATOR");
	$operator_blog = utf8_encode(specialchars($blog->getEntry_blg()));
}


/*SERVICE TYPES*/
$servProvider = new ServiceProvider($db);
$serviceTypes = $servProvider->getAllTypes();

/*MEDICAL TYPES*/
$serviceProviderByFile = new ServiceProviderByFile($db);
$medicalTypes = $serviceProviderByFile->getMedicalTypes();


/* REOPEN FILES PERMISSION */
if($status == 'closed') {
	$parameter=new Parameter($db, '32'); //Reopen Files Parameter
	$parameter->getData();
	$permitedUsers=unserialize($parameter->getValue_par());

	$serial_mbc = User::getManagerByUser($db, $_SESSION['serial_usr']);

    if($serial_mbc=='1'){ //If the user is a PLANETASSIST's user, we set the country to Ecuador 62.
            $serial_mbc='2';
    }

	$permitedUsers[$serial_mbc]=explode(',', $permitedUsers[$serial_mbc]);
	
	if(is_array($permitedUsers[$serial_mbc])) {
		foreach ($permitedUsers[$serial_mbc] as $p) {
			if($_SESSION['serial_usr'] == $p) {
				$reopen_permission='allowed';
				break;
			}
		}
	}

	//For the Moment allow all users to access
    //$reopen_permission='allowed';
}
/* REOPEN FILES PERMISSION */

$smarty->register('fileTypes,card_number,data,countryList,cityList,titles_dialog,titles,contact_info,client_blog');
$smarty->register('operator_blog,serviceTypes,medicalTypes,providerList,global_medicalType,status,reopen_permission,blogHeader');
$smarty->assign('container','none');
$smarty->display('helpers/assistance/loadFile.'.$_SESSION['language'].'.tpl');
?>
