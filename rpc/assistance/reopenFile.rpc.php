<?php
/*
File: reopenFile.rpc
Author: David Bergmann
Creation Date: 14/06/2010
Last Modified:
Modified By:
*/
Request::setString('serial_fle');

$file = new File($db, $serial_fle);
$file->getData();
$file->setStatus_fle('open');

if($file->update()) {
    echo json_encode(TRUE);
} else {
    echo json_encode(FALSE);
}
?>