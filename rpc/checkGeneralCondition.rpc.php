<?php 
/*
File: checkGeneralCondition.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 18:53
Last Modified:
Modified By:
Description: File which verifies if there is another general condition
			 with the same name in the system.
*/

Request::setString('txtCondition');
if($_POST['serial_gcn']){
	$serial_gcn=$_POST['serial_gcn'];
}

$txtCondition=trim(strtolower(utf8_decode($txtCondition)));

$condition=new GeneralCondition($db);
if($condition->existGeneralCondition($txtCondition,$serial_gcn)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>