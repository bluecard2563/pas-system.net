<?php
/*
File: loadMasiveManagersByCountry.rpc.php
Author: Miguel Ponce
Creation Date:
Last Modified:
Modified By:
Description: File which loads all the managers of a country that have masive sales
*/

	Request::setString('serial_cou');
	//UPDATE OVER
	Request::setString('manManager');

	//MANAGERS
	$managerBC = new ManagerbyCountry($db);
	if($serial_cou){
	    $managerList=$managerBC->getMasiveManagersByCountry($serial_cou, $_SESSION['serial_mbc']);
	}

	$smarty->register('managerList,serial_cou,manManager');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>