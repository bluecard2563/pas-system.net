<?php
/*
File:loadReassignSale.rpc
Author: Esteban Angulo
Creation Date: 13/04/2010
*/

$country=new Country($db);
$countryList=$country->getCountriesWithDealer();
$smarty->register('countryList');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/loadReassignSale.'.$_SESSION['language'].'.tpl');
?>
