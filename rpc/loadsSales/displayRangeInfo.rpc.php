<?php
/* File: displayRangeInfo.rpc
 * Author: Patricio Astudillo
 * Creation Date: 10/03/2010
 * Descripction: Displays the range fee of a product and a textfield to put the price.
 * Params: PriceByProductByCountry ID  (serial_ppc)
 */

Request::setString('serial_ppc');
Request::setString('change_fee');

$price= new PriceByProductByCountry($db, $serial_ppc);
$price->getData();

$price=get_object_vars($price);
unset($price['db']);

$min_price=number_format(round($price['min_ppc']*$change_fee, 4),4);
$max_price=number_format(round($price['max_ppc']*$change_fee, 2),2);
//Debug::print_r($price);

$smarty->register('price,min_price,max_price');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/displayRangeInfo.'.$_SESSION['language'].'.tpl');
?>
