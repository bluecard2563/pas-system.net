<?php
/*
File:loasProducts.rpc.php
Author: Esteban Angulo
Creation Date: 
MODIFIED BY: Santiago Borja
*/
	Request::setString('multiple_sales');
	$serial_cnt = $_POST['counter'];
	$counter = new Counter($db);
	$dealer = new Dealer($db);
	
	$counter -> setSerial_cnt($serial_cnt);
	$counter -> getData();
	$data['dealerOfUser'] = $counter -> getSerial_dea();
	
	$dealer -> setSerial_dea($data['dealerOfUser']);
	$dealer -> getData();
	$data['name_dea'] = $dealer -> getName_dea();
	$data['serialDealer'] = $dealer -> getDea_serial_dea();
	$masive = $_POST['masive'];
	if(!$masive){
		$masive='NO';
	}
	if($multiple_sales){
		$multiple_sales = TRUE;
	}
	
	/*products by Dealer*/
	$productByDealer = new ProductByDealer($db);
	$productListByDealer = $productByDealer -> getproductByDealer($data['serialDealer'], $_SESSION['serial_lang'], $masive, 'ACTIVE', $multiple_sales);
	
	foreach($productListByDealer as &$singleProListed){
		$pxc = new ProductByCountry($db, $singleProListed['serial_pxc']);
		$pxc -> getData();
		if(PriceByProductByCountry::pricesByProductExist($db, $singleProListed['serial_pxc'])){
			//If there is a price for the product in the country of analisis.
			$percentages = ProductByCountry::getPercentages_by_Product($db, $singleProListed['serial_pro'], $pxc -> getSerial_cou());
		}else{ 
			//If not, I take the prices for ALL COUNTRIES
			$percentages = ProductByCountry::getPercentages_by_Product($db, $singleProListed['serial_pro'],'1');
		}
		$singleProListed['percentage_spouse'] = $percentages['percentage_spouse_pxc'];
		$singleProListed['percentage_extras'] = $percentages['percentage_extras_pxc'];
		$singleProListed['percentage_children'] = $percentages['percentage_children_pxc'];
	}
	
	//Get the Country Serial for calculate the price
	//of the product to be sold.
	$serial_cou = $deaData['serial_cou'];
	$smarty -> register('productListByDealer,serial_cou');
	$smarty -> assign('container','none');
	$smarty -> display('helpers/loadsSales/loadProducts.'.$_SESSION['language'].'.tpl');
?>