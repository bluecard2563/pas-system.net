<?php
/*
File: loadMasiveResponsablesByManager.rpc.php
Author: Miguel Ponce
Creation Date: 
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
$manager=new ManagerbyCountry($db,$serial_mbc);

if($serial_mbc) {
    $responsablesList=$manager->getMasiveResponsablesByManager();
}

$smarty->register('responsablesList');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByManager.'.$_SESSION['language'].'.tpl');
?>