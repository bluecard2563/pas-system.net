<?php
/**
 * File:  validateCoupon
 * Author: Patricio Astudillo
 * Creation Date: 11-abr-2011, 15:15:19
 * Description:
 * Last Modified: 11-abr-2011, 15:15:19
 * Modified by:
 */

	Request::setString('promo_code');
	Request::setInteger('serial_pxc');
	Request::setFloat('total_sal');
	Request::setString('symbol');
	
	$code_parts = split('-', $promo_code);

	$serial_cup = Cupon::validateCupon($db, trim($code_parts['1']), strtoupper(trim($code_parts['0'])), $serial_pxc);
	
	if($serial_cup){
		$data = Cupon::getCuponData($db, $serial_cup);
		$promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);			
		$previous_limit = $data['limit_amount_cup'];
		
		if($data['generating_sal'] != ''){ //COUPON CAME FROM A PREVIOUS ONE
			$promo_info['discount_percentage_ccp'] = $promo_info['other_discount_ccp'];
			$promo_info['other_discount_ccp'] = 0;			
			
			//IF THE LIMIT AMOUNT IS GREATER THAN THE SALE, WE USE THE PERCENTAGE OF THE COUPON
			if($previous_limit > $total_sal){
				$promo_info['current_sale'] = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_sal, 2);
			}else{ //IF NOT, WE USE THE LIMIT AMOUNT
				$promo_info['current_sale'] = $previous_limit;
			}						
			
			//FORMATING THE REST OF THE VALUES
			$promo_info['next_sale'] = number_format(0, 2);			
		}else{ //IF NOT
			$promo_info['current_sale'] = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_sal, 2);
						
			//GET THE NEXT SALE MAXIMUM DISCOUNT.
			$promo_info['next_sale'] = number_format(($promo_info['other_discount_ccp'] / 100) * $total_sal, 2);
		}				

		$smarty -> register('promo_info,serial_cup,symbol,promo_info');
	}

	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/loadsSales/validateCoupon.'.$_SESSION['language'].'.tpl');
?>
