<?php
/*
 * File: getCardChangesHistory.rpc
 * Author: Patricio Astudillo
 * Description: Loads the information for a new change of status to "Stand-by" or "Blocked". It's posible to change
 *				the status many times, but only for the number of months specified in the $time_limit
 *				parameter.
 * Creation Date: 12/04/2010
 * Modifies By:
 * Last Modified:
 */

Request::setString('card_number');
Request::setString('time_limit');
Request::setString('type');

$sale=new Sales($db);
$customer=new Customer($db);
$saleLog=new SalesLog($db);
$sale->setCardNumber_sal($card_number);
$logs='';
if($sale->getDataByCardNumber()){
	$sale=get_object_vars($sale);
	unset($sale['db']);
	$counter=new Counter ($db, $sale['serial_cnt']);
	$counter->getData();
	$auxData=$counter->getAux_cnt();
	$customer->setserial_cus($sale['serial_cus']);
	$customer->getData();
	$customer=get_object_vars($customer);
	unset($customer['db']);
	if($type=='STAND_BY'){
		$saleLog->getSalesLogBySale($sale['serial_sal'],'STAND_BY');
	}
	else{
		$saleLog->getSalesLogBySale($sale['serial_sal'],'BLOCKED');
	}
	$logs=$saleLog->getAux_slg();
	if($logs){
		foreach($logs as &$a){
			$a['description_slg']=unserialize($a['description_slg']);
		}
	}
	
	if(Sales::isMySale($db, $card_number, $_SESSION['serial_mbc'], $_SESSION['serial_dea'])){
		if($type=='BLOCKED' || ($type=='STAND_BY' && $sale['serial_inv']!='')) {
			if($sale['status_sal']!='VOID' and $sale['status_sal']!='EXPIRED' and $sale['status_sal']!='REFUNDED' and $sale['status_sal']!='BLOCKED'){
				if(!File::getFilesBySaleReport($db, $sale['serial_sal'])){
					if(!Refund::hasRefundApplications($db, $sale['serial_sal'])){
						if(!Sales::hasBeenReactivated($db, $sale['serial_sal'])){
							if(!SalesLog::hasPendingBlockedApplication($db, $sale['serial_sal'])){
									if(!SalesLog::hasPendingStandByApplication($db, $sale['serial_sal']) ){
											/*VERIFY IF WE NEED AN AUTHORIZATION FOR THE CHANGE*/
													$beginDate = GlobalFunctions::strToTimeDdMmYyyy($sale['begin_date_sal']);
													$endDate = GlobalFunctions::strToTimeDdMmYyyy($sale['end_date_sal']);
													$today = GlobalFunctions::strToTimeDdMmYyyy(date("d-m-Y G:i:s"));
                                                    //14-Nov-2019: Solicitud de Operaciones para que el cambio de estado siempre solicite autorización $needsAproval = 1;
                                                    $needsAproval = 1;
                                                    /*
													if($today >= $beginDate){//and $today <= $endDate
															$needsAproval = 1;
													}else{
															$needsAproval = 0;
													}
                                                    */
													$smarty -> register('needsAproval');
											/*END AUTHORIZATION*/
											if($type=='STAND_BY'){
												$data=SalesLog::verifyStandByChanges($db, $sale['serial_sal']);
											}
											if($type=='BLOCKED'){
												$data=SalesLog::verifyBlockedChanges($db, $sale['serial_sal']);
											}

											if($data['first_request']!=''){
													$parameter=new Parameter($db, '9');
													$parameter->getData();
													$parameter=get_object_vars($parameter);
													unset($paramter['db']);

													$firstRequest = $data['first_request'];
													$maxChangeDate = strtotime('+'.$parameter['value_par'].' month', strtotime($firstRequest));

													if($type=='STAND_BY'){
														if($today<=$maxChangeDate){
															$oldStatus=SalesLog::getPreviousStatus($db, $sale['serial_sal'],$type);
															$status=$sale['status_sal'];
															$smarty->register('sale,status,oldStatus,customer,logs,card_number,type');

														}else{//There's not posible to change the status anymore.
															$smarty->register('parameter');
																$error=3;
														}
													}
													if($type=='BLOCKED'){
														$oldStatus=SalesLog::getPreviousStatus($db, $sale['serial_sal'],$type);
														$status=$sale['status_sal'];
														$smarty->register('sale,status,oldStatus,customer,logs,card_number,type');
													}
											}else{//It's the first request of change
													$status=$sale['status_sal'];
													$oldStatus=$sale['status_sal'];
													$smarty->register('sale,status,customer,card_number,type,oldStatus');
											}
									}else{//There's another application pending for the sale.
											$error=5;
									}
							}else{//There's another application pending for the sale.
								$error=6;
							}
						}else{//Has been reactivated.
							$error=7;
						}
					}else{//Has any refund applications pending or they have been aproved.
						$error=4;
					}
				}else{ //Has assistances related.
					$error=8;
				}
			}else{//It's not posible to change the sale to new status.
				$error=2;
			}
		} else {
			$error=9;
		}
	}else {
		$error = 10;
	}
}else{//The cardNumber doesn't exist.
	$error=1;
}

$smarty->register('error,auxData');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/modifications/getCardChangesHistory.'.$_SESSION['language'].'.tpl');
?>