<?php
/*
File: getExpiredCards.rpc
Author: David Bergmann
Creation Date: 23/08/2010
Last Modified: David Bergmann
Modified By: 24/08/2010
*/

Request::setString('card_number');

$sale = new Sales($db);
$sale->setCardNumber_sal($card_number);
$sale->getDataByCardNumber();

$data['serial_sal'] = $sale->getSerial_sal();

if($data['serial_sal']) {
	$data['status_sal'] = $sale->getStatus_sal();
	$customer = new Customer($db,$sale->getSerial_cus());
	$customer->getData();
	$data['name_cus'] = utf8_encode($customer->getFirstname_cus())." ".utf8_encode($customer->getLastname_cus());
	$data['name_pro'] = utf8_encode(ProductByDealer::getProductsByDealerByLanguage($db, $sale->getSerial_pbd(), $_SESSION['serial_lang']));
	$data['emission_date'] = $sale->getEmissionDate_sal();
	$data['begin_date'] = $sale->getBeginDate_sal();
	$data['end_date'] = $sale->getEndDate_sal();
	$data['total_sal'] = $sale->getTotal_sal();
	$invoice = new Invoice($db, $sale->getSerial_inv());
	$invoice->getData();
	$data['total_inv'] = $invoice->getTotal_inv();
	$extra = new Extras($db);
	$data['extras'] = Extras::getExtrasBySale($db, $sale->getSerial_sal());
	if(is_array($data['extras'])) {
		foreach($data['extras'] as &$e) {
			$e['first_name_cus'] = utf8_encode($e['first_name_cus']);
			$e['last_name_cus'] = utf8_encode($e['last_name_cus']);
		}
	}
	$sales_log = new SalesLog($db);
	$data['sales_log'] = $sales_log->getSalesLogBySale($sale->getSerial_sal());
	if(is_array($data['sales_log'])) {
		foreach($data['sales_log'] as &$sl) {
			$sl['description_slg'] = unserialize($sl['description_slg']);
		}
	}
	$data['days_sal'] = $sale->getDays_sal();
	$data['titles'] = array('Titular','Producto Vendido','Fecha de Emisi&oacute;n','Fecha anterior de inicio','Fecha anterior de fin','Precio de la Venta','Precio Facturado');

	if(Sales::hasBeenReactivated($db, $sale->getSerial_sal())) {
		$data['reactivated'] = "reactivated";
	}
}

$smarty->register('data,global_salesStatus,global_extraRelationshipType');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/modifications/getExpiredCards.'.$_SESSION['language'].'.tpl');
?>