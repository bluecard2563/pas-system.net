<?php
/*
 * File: getCardChangesHistoryReserved.rpc.php
 * Author: Valeria Andino
 * Creation Date: 27/07/2016
 * Modifies By:
 * Last Modified:
 */

Request::setString('card_number');
Request::setString('time_limit');
Request::setString('type');

$serialSal = Sales::getSerialSalByCardNumber($db, $card_number);
$langCode = 2;
$sale=new Sales($db);
$customer=new Customer($db);
$saleLog=new SalesLog($db);
$sale->setSerial_sal($serialSal);



if($sale->getData()){	
    //Data by Customer
	$customer->setserial_cus($sale->getSerial_cus());
	$customer->getData();
	$customer=get_object_vars($customer);  
    //Data Country 
    $serial_cou = Country::getCountryByCity($db, $sale->getSerial_cit());    
    $country = new Country($db, $serial_cou);
    $country->getData();
    $name_cou = $country->getName_cou();    
    //Data Person by Sale
    $extras = Extras::getExtrasBySale($db, $serialSal);
    $totalCus = count($extras)+1;
    
	if($type=='RESERVED' ) {
                                                $status = 'REGISTERED';
                                                $today = GlobalFunctions::strToTimeDdMmYyyy(date("d-m-Y G:i:s"));																			
												$oldStatus=$sale->getStatus_sal();
                                                $inDate= $sale->getInDate_sal();
                                                $beginDate = $sale->getBeginDate_sal();
                                                $endDate = $sale->getEndDate_sal();
                                                $price  = $sale->getTotal_sal();                                              
												$smarty->register('sale,customer,card_number,oldStatus,serialSal,name_cou,price,inDate,beginDate,endDate, status,totalCus');
                                                
																	 
}}else{//The cardNumber doesn't exist.
	$error=1;
}
$smarty->register('error');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/modifications/getCardChangesHistoryReserved.'.$_SESSION['language'].'.tpl');
?>