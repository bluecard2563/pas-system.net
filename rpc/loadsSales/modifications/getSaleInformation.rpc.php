<?php
/*
 * File: getSaleInformation.rpc
 * Author: Patricio Astudillo
 * Description: Loads the information of a sale with all it's fields as non-editable.
 *				Only the end date of thavel can be modified.
 * Creation Date: 08/04/2010
 * Modifies By:
 * Last Modified:
 */

Request::setString('card_number');
$sale=new Sales($db);
$sale->setCardNumber_sal($card_number);
if($sale->getDataByCardNumber()){
	$sale=get_object_vars($sale);
	unset($sale['db']);

	if(!Sales::hasBeenReactivated($db, $sale['serial_sal'])){
		if(!SalesLog::hasNotExecutive_CorporativeBehaviour($db, $sale['serial_sal'])){
			if(!SalesLog::verifyAddDatesChanges($db, $sale['serial_sal'])){
				//Get the customer Information.
				$customer=new Customer($db, $sale['serial_cus']);
				$customer->getData();
				$customer=get_object_vars($customer);

				$smarty->register('sale,customer');
			}else{//The card is associated to a massive/executive/corporative style of sale.
				$error=2;
			}
		}else{//The card allows multiple travles and can't add days.
			$error=3;
		}
	}else{
		$error=4;
	}
}else{//The cardNumber doesn't exist.
	$error=1;
}

$smarty->register('error');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/modifications/getSaleInformation.'.$_SESSION['language'].'.tpl');
?>
