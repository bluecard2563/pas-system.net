<?php
/*
File:loadInvoiceData.rpc.php
Author: Carlos Oberto
Creation Date: 17/08/2018
 */
	global $db;
        //Load Customer Data
        $document_cus = trim($_POST['document_cus']);
        $customer = new Customer($db, $document_cus);
        $customer -> setserial_cus($customer -> getCustomerSerialbyDocument($document_cus));
        $customer -> getData();
        //Load CustomerCity
        $city = new City($db,$customer->getSerial_cit());
        $city->getData();
        //Load CustomerCountry
        $country = new Country($db,$city->getSerial_cou());
        $country->getData();
        
        //Load All Countries
        $countryList = Country::getAllAvailableCountries($db);
        
        //Load All Cities
        $city = new City($db);
        $cityList = $city->getCitiesByCountry($country->getSerial_cou());
        
        if($customer->getSerial_cus()){
            $customerData['document_cus'] = $customer->getDocument_cus();
            $customerData['first_name_cus'] = $customer->getFirstname_cus();
            $customerData['last_name_cus'] = $customer->getLastname_cus();
            $customerData['address_cus'] = $customer->getAddress_cus();
            $customerData['phone1_cus'] = $customer->getPhone1_cus();
            $customerData['phone2_cus'] = $customer->getPhone2_cus();
            $customerData['email_cus'] = $customer->getEmail_cus();
            $customerData['serial_cou'] = $country->getSerial_cou();
            $customerData['serial_cit'] = $customer->getSerial_cit();
        }else{
            $customerData['document_cus'] = trim($_POST['document_cus']);
        }
//        Debug::print_r($customerData);die();
	$smarty->register('customerData,countryList,cityList');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsSales/loadInvoiceCustomer.'.$_SESSION['language'].'.tpl')
?>