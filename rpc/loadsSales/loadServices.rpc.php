<?php
/*
File:loadServices.rpc.php
Author: Esteban Angulo
Creation Date: 18/03/2010
*/

    $serial_cnt=$_POST['counter'];
	Request::setString('has_services');
	Request::setString('multisales');
	$counter=new Counter($db);
    $dealer=new Dealer($db);

    $counter->setSerial_cnt($serial_cnt);
    $counter->getData();

    $data['dealerOfUser']=$counter->getSerial_dea();

    $dealer->setSerial_dea($data['dealerOfUser']);
    $dealer->getData();
    $data['name_dea']=$dealer->getName_dea();
    $data['serialDealer']=$dealer->getDea_serial_dea();
	$data['aux']=$dealer->getAuxData_dea();

    //Services by dealer
    $servicesByDealer= new ServicesByDealer($db);
	if($has_services == 'YES'){
		if(ServicesByDealer::serviceByCountryExist($db, $data['aux']['serial_cou'])){
			$servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $data['aux']['serial_cou']);
		}else{
			$servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
		}    
	}    

    $smarty->register('servicesListByDealer,multisales');
    $smarty->assign('container','none');
    $smarty->display('helpers/loadsSales/loadServices.'.$_SESSION['language'].'.tpl');
?>
