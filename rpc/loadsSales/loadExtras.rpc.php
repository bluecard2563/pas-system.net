<?php
/*
File:loadExtras.rpc.php
Author: Esteban Angulo
Creation Date: 15/03/2010
*/

if($_POST['serial_pro']){
    $product= new Product($db);
    $product->setSerial_pro($_POST['serial_pro']);
    $product->getData();
    $max_extras=$product->getMax_extras_pro();
	$pos=$_POST['count'];

    if($max_extras>0){
        $smarty->register('max_extras');
      }

	  $priceObject= new PriceByProductByCountry($db);
		if(PriceByProductByCountry::pricesByProductExist($db, $_POST['serial_pxc'])){ //If there is a price for the product in the country of analisis.
			$percentages= ProductByCountry::getPercentages_by_Product($db, $_POST['serial_pro'],$_POST['serial_cou']);
		}else{ //If not, I take the prices for ALL COUNTRIES
			$percentages= ProductByCountry::getPercentages_by_Product($db, $_POST['serial_pro'],'1');
		}

   $relationshipTypeList = Extras::getAllRelationships($db);

$smarty->register('relationshipTypeList,pos,percentages');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/loadExtras.'.$_SESSION['language'].'.tpl');
}
?>
