<?php
/*
 * File: uploadMultiSalesFile.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 19-oct-2010, 10:13:45
 * Modified By: Patricio Astudillo
 */

Request::setString('serial_pbd');

$productByDealer = new ProductByDealer($db,$serial_pbd);
$productByDealer -> getData();
$productByCountry = new ProductByCountry($db, $productByDealer->getSerial_pxc());
$productByCountry -> getData();
$product = new Product($db,$productByCountry->getSerial_pro());
$product -> getData();

/* INFO FOR PRICES */
$serial_pxc = $productByCountry -> getSerial_pxc();
$serial_pro = $product -> getSerial_pro();
$serial_cou = $productByCountry -> getSerial_cou();
$priceObject = new PriceByProductByCountry($db);
/* INFO FOR PRICES */

$response = GlobalFunctions::masiveSaleProcess($db, $_FILES, $serial_pbd);
if($response[0] == 'success'){
	$toShow = $response[2];
	ksort($toShow);
	
	/* CALCULATE THE PRICE FOR THE FINAL CUSTOMERS ARRAY */
	foreach($toShow as $key => &$item){
		$start_chl = date('Y-m-d',strtotime('-'.$childAge.' year', strtotime($item['start_trl'])));
		$birthdate = date('Y-m-d',strtotime($item['birthdate_cus']));
		$travel_days = $item['travel_days'];
	
		if($travel_days<=0){//Travel Dates not allowed
			ErrorLog::log($db, 'PROCESS UPLOAD ERROR - DAYS UNACC row '.$row, $travel_days);
			$response = GlobalFunctions::masive_fixed_error_log(18);
			break;
		}
	
		$children = ($start_chl < $birthdate);
	
		if($item['cus_serial_cus_xls']){//IF IT IS AN EXTRA
			if(is_array($toShow[$item['cus_serial_cus_xls']]['extras'])){
				$current_extras = count($toShow[$item['cus_serial_cus_xls']]['extras']);
			}else{
				$current_extras=0;
			}
	
			if($current_extras < $product -> getMax_extras_pro()){ //NO <= DUE TO $current_extras = 0 COUNTING AS THE FIRST
				$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['relationship_ext']=$item['relationship_ext'];
				if($children){
					$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['children']='YES';
				}else{
					$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['children']='NO';
				}
	
				switch($item['relationship_ext']){
					case 'SPOUSE':
						if($product->getSpouse_pro()=='YES'){
							/* SEARCH FOR ANOTHER SPOUSE */
							if($current_extras > 0){
								foreach($toShow[$item['cus_serial_cus_xls']]['extras'] as $extra){
									if($extra['relationship_ext']=='SPOUSE'){
										ErrorLog::log($db, 'PROCESS UPLOAD ERROR - DUP SPOUSE row '.$row, $toShow);
										$response = GlobalFunctions::masive_fixed_error_log(14);
									}
								}
							}
							/* SEARCH FOR ANOTHER SPOUSE */
	
							$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['price']=number_format((1-($productByCountry->getPercentage_spouse_pxc()/100))*$toShow[$item['cus_serial_cus_xls']]['price'], 2);
							$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['cost']=number_format((1-($productByCountry->getPercentage_spouse_pxc()/100))*$toShow[$item['cus_serial_cus_xls']]['cost'], 2);
						}else{
							$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['price']=$toShow[$item['cus_serial_cus_xls']]['price'];
							$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['cost']=$toShow[$item['cus_serial_cus_xls']]['cost'];
						}
						break;
					case 'RELATIVE':
					case 'OTHER':
						//IF IT IS A CHILDREN && WE ACCEPT CHILDREN AS EXTRAS
						if($children && ($product->getExtras_restricted_to_pro()=='CHILDREN' || $product->getExtras_restricted_to_pro()=='BOTH')){
							/* SEARCH FOR ANOTHER SPOUSE */
							if($current_extras > 0){
								$total_children=0;
								foreach($toShow[$item['cus_serial_cus_xls']]['extras'] as $extra){
									if($extra['children']=='YES'){
										$total_children++;
									}
								}
							}
							/* SEARCH FOR ANOTHER SPOUSE */
							if($total_children<=$product->getChildren_pro() && $product->getChildren_pro()>0){
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['price']=number_format(0, 2);
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['cost']=number_format(0, 2);
							}else{
								if($productByCountry->getPercentage_children_pxc()==0 || $productByCountry->getPercentage_children_pxc()==''){
									$discount_children=0;
								}else{
									$discount_children=$productByCountry->getPercentage_children_pxc();
								}
	
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['price']=number_format((1-($discount_children/100))*$toShow[$item['cus_serial_cus_xls']]['price'], 2);
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['cost']=number_format((1-($discount_children/100))*$toShow[$item['cus_serial_cus_xls']]['cost'], 2);
							}
						//ELSE IF IT'S AN ADULT && WE ACCEPT AN ADULT AS EXTRA
						}else if(!$children && ($product->getExtras_restricted_to_pro()=='ADULTS' || $product->getExtras_restricted_to_pro()=='BOTH')){
							/* SEARCH FOR ANOTHER SPOUSE */
							if($current_extras > 0){	
								$total_adults=0;
								foreach($toShow[$item['cus_serial_cus_xls']]['extras'] as $extra){
									if($extra['children']=='NO'){
										$total_adults++;
									}
								}
							}
							/* SEARCH FOR ANOTHER SPOUSE */
							 
							if($total_adults<=$product->getAdults_pro() && $product->getAdults_pro()>0){
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['price']=number_format(0, 2);
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['cost']=number_format(0, 2);
							}else{
								if($productByCountry->getPercentage_extras_pxc()==0 || $productByCountry->getPercentage_children_pxc()==''){
									$discount_adults=0;
								}else{
									$discount_adults=$productByCountry->getPercentage_extras_pxc();
								}
	
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['price']=number_format((1-($discount_adults/100))*$toShow[$item['cus_serial_cus_xls']]['price'], 2);
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['cost']=number_format((1-($discount_adults/100))*$toShow[$item['cus_serial_cus_xls']]['cost'], 2);
							}
						//ELSE EXTRA NOT ADMITED.
						}else{
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - NO EXTRAS row '.$row, $product);
							$response = GlobalFunctions::masive_fixed_error_log(15);
							break 2;
						}
						break;
					default:
						ErrorLog::log($db, 'PROCESS UPLOAD ERROR - UNK REL row '.$row, $item);
						$response = GlobalFunctions::masive_fixed_error_log(16);
						break 2;
				}
			}else{ //NO MORE EXTRAS ALLOWED FOR THIS CARD.
				$price = GlobalFunctions::getMainPrice($travel_days, $serial_pxc, $serial_pro, $serial_cou,$priceObject);
				if($price['price_ppc']){
					$item['price']=$price['price_ppc'];
					$item['cost']=$price['cost_ppc'];
				}else{
					ErrorLog::log($db, 'PROCESS UPLOAD ERROR - NO PRICE row '.$row, $price);
					$response = GlobalFunctions::masive_fixed_error_log(13);
					break;
				}
			}
		}else{
			$price=GlobalFunctions::getMainPrice($travel_days, $serial_pxc, $serial_pro, $serial_cou,$priceObject);
			if($price['price_ppc']){
				$item['price']=$price['price_ppc'];
				$item['cost']=$price['cost_ppc'];
			}else{
				ErrorLog::log($db, 'PROCESS UPLOAD ERROR - NO PRICE row '.$row, $price);
				$response = GlobalFunctions::masive_fixed_error_log(13);
				break;
			}
		}
	}//END foreach 
	
	if($response[0] == 'success'){
		$response[2] = $toShow;
	}
	/* CALCULATE THE PRICE FOR THE FINAL CUSTOMERS ARRAY */
}

echo json_encode($response);
?>