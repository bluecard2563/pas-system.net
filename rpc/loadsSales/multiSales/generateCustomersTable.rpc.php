<?php
/*
 * File: generateCustomersTable.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 20-oct-2010, 8:46:44
 * Modified By: Patricio Astudillo
 */

Request::setString('customers_list');
Request::setString('services');

$customers_list=json_decode($customers_list);
$customers_list=$customers_list['2'];
$customers_list=get_object_vars($customers_list);
$total_items=count($customers_list);

/* SERVICES IF ANY */
if($services){
	$services_prices = array();
	$services = unserialize($services);

	if(is_array($services)){
		foreach($services as $s){
			array_push($services_prices, ServicesByDealer::getServiceData($db, $s));
		}
	}
}
/* SERVICES IF ANY */

if(is_array($customers_list)){
	/* GETTING THE TOTAL PRICE & COST FOR EVERY CUSTOMER */
	foreach($customers_list as $key=>&$c){		
		if(is_object($c)){
			$c=get_object_vars($c);
		}
		
		$c['birthdate_cus'] = GlobalFunctions::changeDateFormat($c['birthdate_cus']);
		
		if($c['extras']){
			if(is_object($c['extras'])){
				$c['extras']=get_object_vars($c['extras']);
			}
			
			foreach($c['extras'] as $key2=>&$extra){
				if(is_object($extra)){
					$extra=get_object_vars($extra);
				}
				$duplicated_key=get_extra_info($extra, $customers_list, $key2);

				if($duplicated_key){
					unset($customers_list[$duplicated_key]);
				}
				
				$c['total_price'] += $extra['price'];
				$c['total_cost'] += $extra['cost'];
				$extra['birthdate_cus'] = GlobalFunctions::changeDateFormat($extra['birthdate_cus']);
				$total_items++;
				
				//SERVICES PRICE AND COST ADDED FOR EACH ADDITIONAL TRAVELER
				/* SERVICES PRICES ADJUSTEMENT */
				if(is_array($services_prices)){
					foreach($services_prices as $sp){
						if($sp['fee_type_ser'] == 'PERDAY'){
							$c['total_price'] += $c['travel_days'] * $sp['price_sbc'];
							$c['total_cost'] += $c['travel_days'] * $sp['cost_sbc'];
						}else{
							$c['total_price'] += $sp['price_sbc'];
							$c['total_cost'] += $sp['cost_sbc'];
						}
					}
				}
				/* SERVICES PRICES ADJUSTEMENT */
			}
			
			$c['total_price'] += $c['price'];
			$c['total_cost'] += $c['cost'];
			
			//SERVICES PRICE AND COST ADDED FOR CARDHOLDER
			/* SERVICES PRICES ADJUSTEMENT */
				if(is_array($services_prices)){
					foreach($services_prices as $sp){
						if($sp['fee_type_ser'] == 'PERDAY'){
							$c['total_price'] += $c['travel_days'] * $sp['price_sbc'];
							$c['total_cost'] += $c['travel_days'] * $sp['cost_sbc'];
						}else{
							$c['total_price'] += $sp['price_sbc'];
							$c['total_cost'] += $sp['cost_sbc'];
						}
					}
				}
			/* SERVICES PRICES ADJUSTEMENT */
		}else{			
			$c['total_price']=$c['price'];
			$c['total_cost']=$c['cost'];
			
			/* SERVICES PRICES ADJUSTEMENT */
			if(is_array($services_prices)){
				foreach($services_prices as $sp){
					if($sp['fee_type_ser'] == 'PERDAY'){
						$c['total_price'] += $c['travel_days'] * $sp['price_sbc'];
						$c['total_cost'] += $c['travel_days'] * $sp['cost_sbc'];
					}else{
						$c['total_price'] += $sp['price_sbc'];
						$c['total_cost'] += $sp['cost_sbc'];
					}
				}
			}
			/* SERVICES PRICES ADJUSTEMENT */
		}
        
        //SAVING ARRAY IN SESSION VAR
		$_SESSION['customer_list'][$c['serial_cus']]['serial'] = $c['serial_cus'];
		$_SESSION['customer_list'][$c['serial_cus']]['price'] = $c['price'];
		$_SESSION['customer_list'][$c['serial_cus']]['cost'] = $c['cost'];
		$_SESSION['customer_list'][$c['serial_cus']]['total_price'] = $c['total_price'];
		$_SESSION['customer_list'][$c['serial_cus']]['total_cost'] = $c['total_cost'];
		$_SESSION['customer_list'][$c['serial_cus']]['destination_cit'] = $c['destination_cit'];
		$_SESSION['customer_list'][$c['serial_cus']]['start_trl'] = $c['start_trl'];
		$_SESSION['customer_list'][$c['serial_cus']]['end_trl'] = $c['end_trl'];
		$_SESSION['customer_list'][$c['serial_cus']]['travel_days'] = $c['travel_days'];
					
		if($c['extras']){
			foreach($c['extras'] as $e){
				$_SESSION['customer_list'][$c['serial_cus']]['extras'][$e['serial_cus']]['serial'] = $e['serial_cus'];
				$_SESSION['customer_list'][$c['serial_cus']]['extras'][$e['serial_cus']]['relationship'] = $e['relationship_ext'];
				$_SESSION['customer_list'][$c['serial_cus']]['extras'][$e['serial_cus']]['price'] = $e['price'];
				$_SESSION['customer_list'][$c['serial_cus']]['extras'][$e['serial_cus']]['cost'] = $e['cost'];
			}
		}
	}
	/* GETTING THE TOTAL PRICE & COST FOR EVERY CUSTOMER */
}

$smarty->register('customers_list,total_items');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/multiSales/generateCustomersTable.'.$_SESSION['language'].'.tpl');

/**
 * @name: get_extra_info
 * @return: Returns the name and birthdate for a specific extra and unsets the element from the array.
 */
function get_extra_info(&$extra, &$full_array, $key){
	foreach($full_array as $full_key=>&$fa){
		if(is_object($fa)){
			$fa=get_object_vars($fa);
		}		
		if($key==$fa['serial_cus']){
			$extra['first_name_cus']=$fa['first_name_cus'];
			$extra['last_name_cus']=$fa['last_name_cus'];
			$extra['birthdate_cus']=$fa['birthdate_cus'];
			$extra['serial_cus']=$key;
			return $full_key;
			break;
		}
	}
	return FALSE;
}
?>