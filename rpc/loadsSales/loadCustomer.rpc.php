<?php
/*
File: loadCustomer.rpc.php
Author: Esteban Angulo
Creation Date: 01/03/2010
*/
$customer = new Customer($db);
$typesList = $customer->getAllTypes();
$sessionLanguage = $_SESSION['serial_lang'];
$question = new Question($db);
$maxSerial = $question->getMaxSerial($sessionLanguage);

if($_POST['document_cus']){
	$document_cus = trim($_POST['document_cus']);
	$data['document_cus'] = $document_cus;
	$cardNumber = trim($_POST['card_number']);
    $documentType = trim($_POST['document_type']);
    $data['document_type'] = $documentType;
	$systemOrigin = isset($_POST['system_origin']) ? trim($_POST['system_origin']) : null;

	if($customer->existsCustomer($document_cus, NULL)){
		$customer->setserial_cus($customer->getCustomerSerialbyDocument($document_cus));
		$customer->getData();

        //Update customer document type if the customer exists.
        if (!empty($documentType)) {
            $customer->updateDocumentType($customer->getSerial_cus(), $documentType);
        }

		$data['serial_cus'] = $customer->getSerial_cus();
		$data['firstname_cus'] = $customer->getFirstname_cus();
		$data['lastname_cus'] = $customer->getLastname_cus();
		$data['serial_cit'] = $customer->getSerial_cit();
		$data['birthdate_cus'] = $customer->getBirthdate_cus();
		$data['address_cus'] = $customer->getAddress_cus();
		$data['phone1_cus'] = $customer->getPhone1_cus();
		$data['phone2_cus'] = $customer->getPhone2_cus();
		$data['cellphone_cus'] = $customer->getCellphone_cus();
		$data['email_cus'] = $customer->getEmail_cus();
		$data['relative_cus'] = $customer->getRelative_cus();
		$data['relative_phone_cus'] = $customer->getRelative_phone_cus();
		$data['vip_cus'] = $customer->getVip_cus();
		$data['type_cus'] = $customer->getType_cus();
		$data['status_cus'] = $customer->getStatus_cus();
		$data['serial_cou'] = Country::getCountryByCity($db, $data['serial_cit']);
		$data['gender_cus'] = $customer->getGender_cus();
		$data['document_type'] = $customer->getDocumentType();

		$city = new City($db);
		$cityList = $city->getCitiesByCountry($data['serial_cou'],NULL);
	
		$questionList = Question::getActiveQuestions($db, $sessionLanguage, $data['serial_cus']);
		$answer=new Answer($db);
		$answer->setSerial_cus($data['serial_cus']);
		$answerList=$answer->getAnswers();

		//Health Statement: Connect to api by curl to check if the customer already has a statement by current card number and document.
		$statement = new Statement();
		$statementData = $statement->existCustomerStatement($cardNumber, $document_cus, $systemOrigin);
		if ($statementData['exist']) {
			$data['customer_statement'] = true;
		}
		else {
			$data['customer_statement'] = false;
		}

	}else{
		$questionList = Question::getActiveQuestions($db, $sessionLanguage);
	}
}

$countryList = Country::getAllAvailableCountries($db);
	
$smarty->register('data,countryList,cityList,typesList,answerList,questionList');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/loadCustomer.'.$_SESSION['language'].'.tpl');
?>
