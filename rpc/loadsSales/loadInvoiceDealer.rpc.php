<?php
/*
File:loadInvoiceData.rpc.php
Author: Carlos Oberto
Creation Date: 17/08/2018
 */
	global $db;
        //Load Customer Data
        $serial_dea = trim($_POST['dea_serial_dea']);
//        Debug::print_r($serial_dea);die();
        /**************** BRANCH INFO FOR INVOICE HEADER *********************/
	$branch = new Dealer($db, $serial_dea);
	$branch->getData();
	$branchData = get_object_vars($branch);
	unset($branchData['db']);
	$branchData['today'] = date('d/m/Y');
	
	/* RETRIEVE SALES'S TOTAL PRICE */
	$sales = new Sales($db);
	$salesT = $sales->getSalesTotal($salesIdString);
	$salesTotal = number_format($salesT, 2, '.', '');
	
	/* RETRIEVE SALES'S TOTAL COST */
	$salesCost = number_format($sales->getCostTotal($salesIdString), 2, '.', '');
	if ($branchData['type_percentage_dea'] == 'DISCOUNT') {
		$totalAfterD = $salesT - ($salesT * $branchData['percentage_dea'] / 100);
	} else {
		if($branchData['percentage_dea'] > 0){
			$branchData['commission'] = $branchData['percentage_dea'] / 100;
		}else{
			$branchData['commission'] = 0;
		}
		
		$totalAfterD = $salesT;
	}
//        Debug::print_r($branchData);die();
	$smarty->register('branchData');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsSales/loadInvoiceDealer.'.$_SESSION['language'].'.tpl')
?>