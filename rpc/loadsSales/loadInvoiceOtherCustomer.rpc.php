<?php
/*
File:loadInvoiceData.rpc.php
Author: Carlos Oberto
Creation Date: 17/08/2018
 */
	global $db;
        //Load Customer Data
        $document_cus = trim($_POST['document_cus']);
        $documentType = trim($_POST['document_type']);
        $customer = new Customer($db, $document_cus);
        $customer -> setserial_cus($customer -> getCustomerSerialbyDocument($document_cus));
        $customer -> getData();
        //Load CustomerCity
        $city = new City($db,$customer->getSerial_cit());
        $city->getData();
        //Load CustomerCountry
        $country = new Country($db,$city->getSerial_cou());
        $country->getData();
        
        //Load All Countries
        $countryList = Country::getAllAvailableCountries($db);
        
        //Load All Cities
        $city = new City($db);
        $cityList = $city->getCitiesByCountry($country->getSerial_cou());
        if($customer->getSerial_cus()){

            //Update customer document type if the customer exists.
            if (!empty($documentType)) {
                $customer->updateDocumentType($customer->getSerial_cus(), $documentType);
            }

            $customerData['document_cus'] = $customer->getDocument_cus();
            $customerData['first_name_cus'] = $customer->getFirstname_cus();
            $customerData['last_name_cus'] = $customer->getLastname_cus();
            $customerData['address_cus'] = $customer->getAddress_cus();
            $customerData['phone1_cus'] = $customer->getPhone1_cus();
            $customerData['phone2_cus'] = $customer->getPhone2_cus();
            $customerData['email_cus'] = $customer->getEmail_cus();
            $customerData['serial_cou'] = $country->getSerial_cou();
            $customerData['serial_cit'] = $customer->getSerial_cit();
            $customerData['serial_cit'] = $customer->getSerial_cit();
            $customerData['gender_cus'] = $customer->getGender_cus();
            $customerData['birthdate_cus'] = $customer->getBirthdate_cus();
            $customerData['type_cus'] = $customer->getType_cus();
            $customerData['document_type_cus'] = $customer->getDocumentType();
        }else{
            $customerData['document_cus'] = trim($_POST['document_cus']);
        }
//        Debug::print_r($customerData);die();
	$smarty->register('customerData,countryList,cityList');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsSales/loadInvoiceOtherCustomer.'.$_SESSION['language'].'.tpl')
?>
