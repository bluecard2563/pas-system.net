<?php
/* File: calculatePrice.rpc
 * Author: Patricio Astudillo
 * Creation Date: 05/03/2010
 * Descripction: Calculates the fee of a certain product according to
 *               a specific ammount of days and a specific product
 * Params: Number of days (days),
 *         Product to be sold (serial_pro),
 *         Dealer ID (serial_dea),
 *         Country ID  (serial_cou)
 */

Request::setInteger('days');
Request::setInteger('serial_pxc');
Request::setInteger('serial_cou');
Request::setInteger('serial_pro');
Request::setString('range_price');
Request::setString('symbol');
Request::setString('masive_pro');
Request::setString('birthday');
$change_fee=$_POST['change_fee'];

// Calcular la edad en la fecha de inicio y fin del viaje
try {
    $ageStart = calculateAgeG($birthday, $_POST['beginDate']);
    $ageEnd = calculateAgeG($birthday, $_POST['endDate']);
} catch (Exception $e) {
    $message = 'Debe llenar los campos completos del cliente para calcular tarifa.';
    $smarty->register('closestDurationPrice,closestDurationCost,closestDurationRangeMax,closestDurationRangeMin,fixedPrice,message');
$smarty->assign('container','none');
    $smarty->display('helpers/loadsSales/calculatePrice.'.$_SESSION['language'].'.tpl');

}

$priceObject= new PriceByProductByCountry($db);
if(!PriceByProductByCountry::pricesByProductExist($db, $serial_pxc)){
	$serial_cou = 1;
}
$prices = $priceObject -> getPricesByProductByCountry($serial_pro, $serial_cou, $days);

$targetDays = $days;
if(is_array($prices)){ //Evaluation of the
    foreach($prices as $p){
		$currentDuration = $p['duration_ppc'];
		if(	($currentDuration<=$targetDays && $currentDuration+15>=$targetDays)//IS THIS DURATION ELIGIBLE?? [EQUAL OR LOWER BUT WITHIN RANGE]
			|| ($currentDuration>$targetDays && $closestDuration ==0)){ //IS THE SUPERIOR OUR BEST SHOT?, THEN USE IT
				$closestDuration = $currentDuration;
				$closestDurationPrice = $p['price_ppc'];
				$closestDurationCost = $p['cost_ppc'];
				$closestDurationRangeMax = $p['max_ppc'];
				$closestDurationRangeMin = $p['min_ppc'];
				$closestDurationPXC = $p['serial_pxc'];
		}
	}
	$productExtraDays = $targetDays - $closestDuration;

	if($productExtraDays < 0 ){
		$productExtraDays = 0;
	}
	
	$pxcObject = new ProductByCountry($db, $closestDurationPXC);
    $pxcObject -> getData();
    $ageHolder = GlobalFunctions::getMyAge($birthday, $actualYear);
	$closestDurationPrice = number_format(round($closestDurationPrice+($productExtraDays * $pxcObject -> getAditional_day_pxc()), 2), 2, '.', '');
	$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');
    $ageHolder = GlobalFunctions::getMyAge($birthday, $actualYear);
    //Debug::print_r($ageHolder);die();
    $elderlyAgeParameter = new Parameter($db,1);
    $elderlyAgeParameter -> getData();
    $elderlyAge = $elderlyAgeParameter -> getValue_par();

    // Get the value of the parameter where the 65 years old of eldery is setted
	$elderlyMinAgeHandler = new Parameter($db, 55);
	$elderlyMinAgeHandler->getData();
	$elderlyMinAge = $elderlyMinAgeHandler->getValue_par();

	// Get the value of the parameter where the 86 years old of eldery is setted
	$elderlyMaxAgeHandler = new Parameter($db, 56);
	$elderlyMaxAgeHandler->getData();
	$elderlyMaxAge = $elderlyMaxAgeHandler->getValue_par();

	// Get the value of the parameter where the 80 years old of eldery is setted
	$elderlyMiddleAgeHandler = new Parameter($db, 61);
	$elderlyMiddleAgeHandler->getData();
	$elderlyMiddleAge = $elderlyMiddleAgeHandler->getValue_par();

	// Get value percentage senior
	$percentageSeniorOneParameter = new Parameter($db, 57);
	$percentageSeniorOneParameter->getData();
	$percentageSeniorOne = $percentageSeniorOneParameter->getValue_par();

	$percentageSeniorTwoParameter = new Parameter($db, 58);
	$percentageSeniorTwoParameter->getData();
	$percentageSeniorTwo = $percentageSeniorTwoParameter->getValue_par();

	$percentageSeniorThreeParameter = new Parameter($db, 59);
	$percentageSeniorThreeParameter->getData();
	$percentageSeniorThree = $percentageSeniorThreeParameter->getValue_par();

	$percentageSeniorFourParameter = new Parameter($db, 60);
	$percentageSeniorFourParameter->getData();
	$percentageSeniorFour = $percentageSeniorFourParameter->getValue_par();


    // fixed price with 50% surcharge if the age of the older adult is between 65 and 75 years old
	if ($ageEnd >= $elderlyMinAge && $ageEnd < $elderlyAge) {
		if($serial_pro==217){
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorOne) / 100),2), 2), 2, '.', '');
		}else {
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorOne) / 100)), 2), 2, '.', '');
		}
		
	} else if ($ageEnd >= $elderlyAge && $ageEnd < $elderlyMiddleAge) {
		if($serial_pro==217){
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorTwo) / 100),2), 2), 2, '.', '');
		}else {
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorTwo) / 100)), 2), 2, '.', '');
		}
		
	}else if ($ageEnd >= $elderlyMiddleAge && $ageEnd < $elderlyMaxAge) {
		if($serial_pro==217){
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorThree) / 100),2), 2), 2, '.', '');
		}else {
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorThree) / 100)), 2), 2, '.', '');
		}}
	// fixed price with 100% surcharge if the age of the older adult is older than 86 years old
	elseif ($ageEnd >= $elderlyMaxAge) {
		if($serial_pro==217){
		$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorFour) / 100),2), 2), 2, '.', '');
		}else{
			$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorFour) / 100)), 2), 2, '.', '');
		}
	}
	// fixed price for customers who are not are older adult or age between 76 and 85 years old
	else {
		$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');
	}

}else{ 
	$ageStart = calculateAgeG($birthday, $_POST['beginDate']);
$ageEnd = calculateAgeG($birthday, $_POST['endDate']);
	//A set of fees doesn't exist. We retrieve the next fee available.
	$serial_ppc = $priceObject -> getMaxPricePosible($serial_pro, $days, $serial_cou);
    if($serial_ppc){
        $priceObject -> setSerial_ppc($serial_ppc);
        $priceObject -> getData();
        $priceObject = get_object_vars($priceObject);
		
		$closestDurationPrice = $priceObject['price_ppc'];
		$closestDurationCost = $priceObject['cost_ppc'];
		$closestDurationRangeMax = $priceObject['max_ppc'];
		$closestDurationRangeMin = $priceObject['min_ppc'];
		$closestDurationPXC = $priceObject['serial_pxc'];
		$pxcObject = new ProductByCountry($db, $closestDurationPXC);
		$pxcObject -> getData();
		$productExtraDays = 0;
        $closestDurationPrice = number_format(round($closestDurationPrice, 2), 2, '.', '');
		$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');
        $ageHolder = GlobalFunctions::getMyAge($birthday, $actualYear);
        //Debug::print_r($ageHolder);die();
        $elderlyAgeParameter = new Parameter($db,1);
        $elderlyAgeParameter -> getData();
        $elderlyAge = $elderlyAgeParameter -> getValue_par();

        // Get the value of the parameter where the 65 years old of eldery is setted
		$elderlyMinAgeHandler = new Parameter($db, 55);
		$elderlyMinAgeHandler->getData();
		$elderlyMinAge = $elderlyMinAgeHandler->getValue_par();

		// Get the value of the parameter where the 86 years old of eldery is setted
		$elderlyMaxAgeHandler = new Parameter($db, 56);
		$elderlyMaxAgeHandler->getData();
		$elderlyMaxAge = $elderlyMaxAgeHandler->getValue_par();

		// Get the value of the parameter where the 80 years old of eldery is setted
	$elderlyMiddleAgeHandler = new Parameter($db, 61);
	$elderlyMiddleAgeHandler->getData();
	$elderlyMiddleAge = $elderlyMiddleAgeHandler->getValue_par();

		// Get value percentage senior
		$percentageSeniorOneParameter = new Parameter($db, 57);
		$percentageSeniorOneParameter->getData();
		$percentageSeniorOne = $percentageSeniorOneParameter->getValue_par();

		$percentageSeniorTwoParameter = new Parameter($db, 58);
		$percentageSeniorTwoParameter->getData();
		$percentageSeniorTwo = $percentageSeniorTwoParameter->getValue_par();

		$percentageSeniorThreeParameter = new Parameter($db, 59);
		$percentageSeniorThreeParameter->getData();
		$percentageSeniorThree = $percentageSeniorThreeParameter->getValue_par();

		$percentageSeniorFourParameter = new Parameter($db, 60);
	$percentageSeniorFourParameter->getData();
	$percentageSeniorFour = $percentageSeniorFourParameter->getValue_par();
		
        // fixed price with 50% surcharge if the age of the older adult is between 65 and 75 years old
		if ($ageEnd >= $elderlyMinAge && $ageEnd < $elderlyAge) {
			if($serial_pro==217){
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorOne) / 100),2), 2), 2, '.', '');
			}else{
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorOne) / 100)), 2), 2, '.', '');
			}
			
		} else if ($ageEnd >= $elderlyAge && $ageEnd < $elderlyMiddleAge) {
			if($serial_pro==217){
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorTwo) / 100),2), 2), 2, '.', '');
			}else{
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorTwo) / 100)), 2), 2, '.', '');
			}
			
		} else if ($ageEnd >= $elderlyMiddleAge && $ageEnd < $elderlyMaxAge) {
			if($serial_pro==217){
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorThree) / 100),2), 2), 2, '.', '');
			}else{
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorThree) / 100)), 2), 2, '.', '');
			}}
		// fixed price with 100% surcharge if the age of the older adult is older than 86 years old
		elseif ($ageEnd >= $elderlyMaxAge) {
			if($serial_pro==217){
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorFour) / 100),2), 2), 2, '.', '');
			}else{
				$fixedPrice = number_format(round($fixedPrice + round((($fixedPrice * $percentageSeniorFour) / 100)), 2), 2, '.', '');
			}
			
		}
		// fixed price for customers who are not are older adult or age between 76 and 85 years old
		else {
			$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');
		}
    }else{//If there's not a fee, the operator can't make the sale for tha amount of days given.
        $message='No existe una tarifa en este producto para los '.$days.' d&iacute;as seleccionados.';
    }
}

// Función para calcular la edad en una fecha específica
function calculateAgeG($birthday, $date) {
	$birthDate = DateTime::createFromFormat('d/m/Y', $birthday);
    $currentDate = DateTime::createFromFormat('d/m/Y', $date);
    if (!$birthDate || !$currentDate) {
        throw new Exception("Invalid date format");
    }
    $age = $birthDate->diff($currentDate)->y;
    return $age;
}
$smarty->register('closestDurationPrice,closestDurationCost,closestDurationRangeMax,closestDurationRangeMin,fixedPrice,message');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/calculatePrice.'.$_SESSION['language'].'.tpl');
?>
