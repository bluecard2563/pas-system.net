<?php
/*
File:loadMasiveSales.rpc.php
Author: Miguel Ponce
Creation:
Modified By:
Last Modified: 
*/

Request::setInteger('serial_dea');
Request::setInteger('serial_cou');

$branch= new Dealer($db,$serial_dea);
//$dbc=new DocumentByCountry($db);
//$dlist=$dbc->getDocumentsByCountry($serial_cou,'INVOICE');
$branch->getData();

$mbc = new ManagerbyCountry($db);
$mbc->setSerial_mbc($branch->getSerial_mbc());
$mbc->getData();

$salesList=$branch->getMasiveSales();

$tax=new TaxesByProduct($db);
$sale=new Sales($db);
if(is_array($salesList)){
	foreach($salesList as &$sl){
		$tax->setSerial_pxc($sl['serial_pxc']);
		$taxes=$tax->getTaxesByProduct();
		$serials=array();
		$names=array();
		if($taxes){
			foreach($taxes as $k=>&$t){
				$serials[$k]=$t['serial_tax'];
				$names[$k]=$t['name_tax'];
			}
			$sl['taxes']=implode(',', $serials);
			$sl['names_taxes']=implode('<br />', $names);
		}else{
			$sl['taxes']=0;
		}

		$sale->setSerial_sal($sl['serial_sal']);
		$sl['sales']=$sale->getSubMasiveSales();
		//debug::print_r($sl['sales']);
	}
	$total_masive_sales=count($salesList);

	//Country currencies
	$counter=new Counter($db);
	$serial_cnt=$counter->getCounters($_SESSION['serial_usr']);
	if(count($serial_cnt)==1){
		$serial_cnt=$serial_cnt['0']['serial_cnt'];
		$counter->setSerial_cnt($serial_cnt);
		$counter->getData();
		$curBycountry=new CurrenciesByCountry($db);
		$currencies=$curBycountry->getCurrenciesDealersCountry($counter->getSerial_dea());
		$currenciesNumber=sizeof($currencies);
	}
}

$smarty->register('salesList,global_salesStatus,total_masive_sales,currencies,currenciesNumber');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/loadMasiveSales.'.$_SESSION['language'].'.tpl');
?>