<?php
/*
 * File: loadMultipleSaleInforpc.php
 * Author: Patricio Astudillo
 * Creation Date: 18-oct-2010, 16:46:51
 * Modified By: Patricio Astudillo
 */

if($_POST['serial_counter']){
    $serial_cnt=$_POST['serial_counter'];
    $counter=new Counter($db,$serial_cnt);
    $counter->getData();
	$auxData=$counter->getAux_cnt();

    $city = new City($db);
    $dealer = new Dealer($db);
    $countryList = Country::getAllAvailableCountries($db);

    //PRE-LOAD INFORMATION
    //CounterName
	$sale= new Sales($db);
	$ofCounter=$sale->isDirectSale($serial_cnt);
	$counterName=$counter->getAux_cnt();

    //Dealer Data
    $counter_info=Dealer::getDealerCity($db, $serial_cnt);
    $counter_info['dealerOfUser']=$counter->getSerial_dea();
    $dealer->setSerial_dea($counter_info['dealerOfUser']);
    $dealer->getData();
    $counter_info['name_dea']=$dealer->getName_dea();
    $counter_info['serialDealer']=$dealer->getDea_serial_dea();

    //Makes Seller Code
	$counter_info['sellerCode']=$counter_info['code_cou'].'-'.$counter_info['code_cit'].'-'.$dealer->getCode_dea().'-'.$counter_info['serialDealer'].'-'.$serial_cnt;
	//EmissionCity
	$counter_info['emissionPlace']=$counter_info['name_cit'];
	//EmissionDate
	$counter_info['emissionDate']=date("d/m/Y");
	//Get the Country Serial for calculate the price of the product to be sold.
	$counter_info['serial_cou']=$counter_info['serial_cou'];
	//Counter Name
	$counter_info['counterName']=$counterName['counter'];

	$smarty->register('counter_info,serial_cnt');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsSales/loadMultipleSaleInfo.'.$_SESSION['language'].'.tpl');
    
}
?>
