<?php
/* File: retrieveCandidateVoidSales
 * Author: Santiago Borja
 * Creation Date: 10/03/2010
 */

	Request::setInteger('selBranch');
	
	$myStock = Stock::getArrayAllAvailableForBranch($db, $selBranch, 'MANUAL');
	$totalCards = count($myStock);
	
	$smarty->register('myStock,totalCards');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsSales/retrieveCandidateVoidSales.'.$_SESSION['language'].'.tpl');
?>