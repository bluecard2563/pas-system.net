<?php

/*
 * File:loadCardsByCustomer
 * Author: Santiago Borja
 * Creation Date: 25/01/2011
 *	 Last modified: Patricio Astudillo
 * Modification Date: 23-04-13
 */

	$own_branch_cards_only = FALSE;
	$exclude_free = TRUE;

	if (isset($_SESSION['serial_dea']) && !User::isSubManagerUser($db, $_SESSION['serial_usr']))
		$own_branch_cards_only = TRUE;

	if ($_POST['serial_cus']) {
		$list = $cardsInfo = Sales::getCardsByCustomerReport($db, $_POST['serial_cus'], $own_branch_cards_only, $exclude_free);
		if ($list['0']['card_number_sal'] != '') {
			foreach ($list as &$li) {
				if (Sales::checkIfSaleIsMasiveByCardNumber($db, $li['card_number_sal'])) {
					$li['masive'] = 'YES';
				} else {
					$li['masive'] = 'NO';
				}
			}
		}

		if ($list) {
			$error = 0; //SUCCESS
		} else {
			$error = 1;
		}
	} else if ($_POST['card_number']) {

		if (Sales::checkIfSaleIsMasiveByCardNumber($db, $_POST['card_number'])) {
			$sales = Sales::getMasiveCardsByCardNumber($db, $_POST['card_number'], $own_branch_cards_only);
			if ($sales['0']['serial_sal'] != '') {
				foreach ($sales as &$s) {
					$s['masive'] = 'YES';
				}
				if (!$own_branch_cards_only) {
					if ($sales['free_sal'] == 'YES' && $sales['status_sal'] == 'REQUESTED') {
						$error = 3; //UNAUTHORIZED FREE SALE
					}
				} else { //PPERMISSION TO SEE ONLY THE CARDS FROM MY DEALER.
					$cnt = new Counter($db, $sales[0]['serial_cnt']);
					$cnt->getData();
					if ($cnt->getSerial_dea() != $_SESSION['serial_dea']) {
						$error = 4; //NO PERMISSION TO SEE THIS CARD
					}
				}
			} else {
				$error = 2;
			}
		} else {
			$sales = Sales::getNormalCardsByCardNumber($db, $_POST['card_number'], $own_branch_cards_only);
			if ($sales['0']['serial_sal'] != '') {
				foreach ($sales as &$s) {
					$s['masive'] = 'NO';
				}

				if (!$own_branch_cards_only) {
					if ($sales['free_sal'] == 'YES' && $sales['status_sal'] == 'REQUESTED') {
						$error = 3; //UNAUTHORIZED FREE SALE
					}
				} else { //PPERMISSION TO SEE ONLY THE CARDS FROM MY DEALER.
					$cnt = new Counter($db, $sales[0]['serial_cnt']);
					$cnt->getData();
					if ($cnt->getSerial_dea() != $_SESSION['serial_dea']) {
						$error = 4; //NO PERMISSION TO SEE THIS CARD
					}
				}
			} else {
				$error = 2;
			}
		}
	}

	$smarty->register('list,error,sales');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/loadsSales/loadCardsByCustomer.' . $_SESSION['language'] . '.tpl');
?>