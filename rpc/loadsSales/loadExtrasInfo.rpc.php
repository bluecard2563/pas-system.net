<?php
/*
File:loadExtrasInfo.rpc.php
Author: Esteban Angulo
Creation Date: 22/03/2010
Modified By: Santiago Borja
*/
Request::setString('extraPosition');
Request::setString('editionAllowed');

$relationshipTypeList = Extras::getAllRelationships($db);

$customer = new Customer($db);
$sessionLanguage = $_SESSION['serial_lang'];
$question = new Question($db);

$data = array();
$answerList = array();
if($_POST['document_cus'] && $_POST['document_cus'] != 'undefined'){
    $document_cus = $_POST['document_cus'];
	if($customer->existsCustomer($document_cus,NULL)){
		$countryList = Country::getAllAvailableCountries($db);
	
		$city = new City($db);
		$customer->setserial_cus($customer->getCustomerSerialbyDocument($document_cus));
		$customer->getData();

		$data['serial_cus'] = $customer->getSerial_cus();
		$data['document_cus'] = $customer->getDocument_cus();
		$data['firstname_cus'] = $customer->getFirstname_cus();
		$data['lastname_cus'] = $customer->getLastname_cus();
		$data['birthdate_cus'] = $customer->getBirthdate_cus();
		$data['email_cus'] = $customer->getEmail_cus();
        $data['customerExist'] = true;
		
		$extra = new Extras($db, NULL, $_POST['serialSale'], $customer->getSerial_cus());
		$extra->getData();
		$data['relationship_cus'] = $extra->getRelationship_ext();
		
		$questionList = Question::getActiveQuestions($db, $sessionLanguage,$data['serial_cus']); 
		$answer = new Answer($db);
		$answer->setSerial_cus($data['serial_cus']);
		$answerList = $answer->getAnswers(); //TODO STATIC FUNCTION
    }else{
		$data['document_cus'] = $document_cus;
		$data['customerExist'] = false;
		$questionList = Question::getActiveQuestions($db, $sessionLanguage);
	}
}
    
$smarty->register('data,questionList,answerList,extraPosition,percentages,relationshipTypeList,editionAllowed');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/loadExtrasInfo.'.$_SESSION['language'].'.tpl');
?>
