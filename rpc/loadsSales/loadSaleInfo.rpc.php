<?php
/*
File:loadSaleInfo.rpc.php
Author: Esteban Angulo
Creation Date: 18/03/2010
*/
Request::setString('show_line');

if($_POST['serial_counter']){
    $serial_cnt=$_POST['serial_counter'];
	//echo $serial_cnt;

    $counter=new Counter($db,$serial_cnt);
    $counter->getData();
	$auxData=$counter->getAux_cnt();
	$sellFree=$auxData['sell_free_usr'];

    //check for the next available number from vritual stock
	$nextAvailableNumber = Stock::getNextStockNumber($db, false);
    if(!$nextAvailableNumber){
        http_redirect('main/sales/19');
    }

    $city= new City($db);
    $dealer=new Dealer($db);

    $countryList = Country::getAllAvailableCountries($db);

    //PRE-LOAD INFORMATION
    //CounterName

	$sale= new Sales($db);
	$ofCounter=$sale->isDirectSale($serial_cnt);
	$counterName=$counter->getAux_cnt();
	$data['counterName']=$counterName['counter'];
	
    //Dealer Data
    $deaData=Dealer::getDealerCity($db, $serial_cnt);
    $data['dealerOfUser']=$counter->getSerial_dea();
	//Country currencies
	$curBycountry=new CurrenciesByCountry($db);
	$data['currencies']=$curBycountry->getCurrenciesDealersCountry($counter->getSerial_dea());
	$data['currenciesNumber']=sizeof($data['currencies']);

    $dealer->setSerial_dea($data['dealerOfUser']);
    $dealer->getData();
    $data['name_dea']=$dealer->getName_dea();
    $data['serialDealer']=$dealer->getDea_serial_dea();

    /*products by Dealer*/
    $productByDealer= new ProductByDealer($db);
    $productListByDealer=$productByDealer->getproductByDealer($data['serialDealer'], $_SESSION['serial_lang']);

    //Services by dealer
    $servicesByDealer= new ServicesByDealer($db);
    if(ServicesByDealer::serviceByCountryExist($db, $deaData['serial_cou'])){
        $servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $deaData['serial_cou']);
    }else{
        $servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
    }


    //If the dealer doesn't have any products to sale,
    //he can't see the sales interface.
    if(!is_array($productListByDealer)){
        //http_redirect('main/sales/3');
        $hasProducts="NO";
        $smarty->register('hasProducts,sellFree');
    }else{
        //Makes Seller Code
        $sellerCode=$deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer->getCode_dea().'-'.$data['serialDealer'].'-'.$serial_cnt;

        //EmissionCity
        $data['emissionPlace']=$deaData['name_cit'];

        //EmissionDate
        $data['emissionDate']=date("d/m/Y");

        //Get the Country Serial for calculate the price
        //of the product to be sold.
        $data['serial_cou']=$deaData['serial_cou'];

        //Debug::print_r($data);

        $smarty->register('countryList,data,nextAvailableNumber,productListByDealer,servicesListByDealer,sellerCode,typesList,serial_cou,serial_cnt,maxSerial,sellFree,ofCounter');
    }
    
    $smarty->register('show_line');
    $smarty->assign('container','none');
    $smarty->display('helpers/loadsSales/loadSaleInfo.'.$_SESSION['language'].'.tpl');
}

?>