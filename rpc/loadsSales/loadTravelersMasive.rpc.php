<?php
/*
File: loadTravelersMasive.rpc
Author: David Bergmann
Creation Date: 02/09/2010
Last Modified:
Modified By:
*/

Request::setString('serial_sal');

$customers = Sales::getMasiveSalesCustomerReport($db,$serial_sal);

if(is_array($customers)) {
	foreach ($customers as &$c) {
		$c['name_cus'] = utf8_encode($c['name_cus']);
	}
}

$smarty->register('customers');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/loadTravelersMasive.'.$_SESSION['language'].'.tpl');
?>