<?php
/* File: calculateFixPrice.rpc
 * Author: Patricio Astudillo
 * Creation Date: 10/03/2010
 * Descripction: Displays the fee to be charged for the sale.
 * Params: Number of days (days),
 *         Product to be sold (serial_pro),
 *         Dealer ID (serial_dea),
 *         Country ID  (serial_cou)
 * Modified By: Patricio Astudillo
 * Modification Date: 19/04/2010
 * Modification Description: Delete the param range_price from be displayed and catched.
 */

Request::setInteger('days');
Request::setInteger('serial_pxc');
Request::setInteger('serial_cou');
Request::setInteger('serial_pro');

$priceObject= new PriceByProductByCountry($db);
if(PriceByProductByCountry::pricesByProductExist($db, $serial_pxc)){ //If there is a price for the product in the country of analisis.
    $prices=$priceObject->getPricesByProductByCountry($serial_pro, $serial_cou);
}else{ //If not, I take the prices for ALL COUNTRIES
    $prices=$priceObject->getPricesByProductByCountry($serial_pro, '1');
}

//Debug::print_r($prices);

$smarty->register('prices');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/calculateFixPrice.'.$_SESSION['language'].'.tpl');
?>
