<?php
/**
 * Name: displayInvoicingInfo
 * Created by: pastudillo
 * Created on: 17-nov-2016
 * Description:
 * Modified by: pastudillo
 * Modified on: 17-nov-2016
 */

Request::setString('countryID');

$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/displayInvoicingInfo.'.$_SESSION['language'].'.tpl');