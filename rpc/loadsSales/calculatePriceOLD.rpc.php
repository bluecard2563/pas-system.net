<?php
/* File: calculatePrice.rpc
 * Author: Patricio Astudillo
 * Creation Date: 05/03/2010
 * Descripction: Calculates the fee of a certain product according to
 *               a specific ammount of days and a specific product
 * Params: Number of days (days),
 *         Product to be sold (serial_pro),
 *         Dealer ID (serial_dea),
 *         Country ID  (serial_cou)
 */

Request::setInteger('days');
Request::setInteger('serial_pxc');
Request::setInteger('serial_cou');
Request::setInteger('serial_pro');
Request::setString('range_price');
Request::setString('symbol');
Request::setString('masive_pro');
$change_fee=$_POST['change_fee'];

$priceObject= new PriceByProductByCountry($db);
if(!PriceByProductByCountry::pricesByProductExist($db, $serial_pxc)){
	$serial_cou = 1;
}
$prices = $priceObject -> getPricesByProductByCountry($serial_pro, $serial_cou, $days);

$targetDays = $days;
if(is_array($prices)){ //Evaluation of the
    foreach($prices as $p){
		$currentDuration = $p['duration_ppc'];
		if(	($currentDuration<=$targetDays && $currentDuration+10>=$targetDays)//IS THIS DURATION ELIGIBLE?? [EQUAL OR LOWER BUT WITHIN RANGE]
			|| ($currentDuration>$targetDays && $closestDuration ==0)){ //IS THE SUPERIOR OUR BEST SHOT?, THEN USE IT
				$closestDuration = $currentDuration;
				$closestDurationPrice = $p['price_ppc'];
				$closestDurationCost = $p['cost_ppc'];
				$closestDurationRangeMax = $p['max_ppc'];
				$closestDurationRangeMin = $p['min_ppc'];
				$closestDurationPXC = $p['serial_pxc'];
		}
	}
	$productExtraDays = $targetDays - $closestDuration;
	if($productExtraDays < 0 ){
		$productExtraDays = 0;
	}
	
	$pxcObject = new ProductByCountry($db, $closestDurationPXC);
    $pxcObject -> getData();
    
    $closestDurationPrice = number_format(round($closestDurationPrice+($productExtraDays * $pxcObject -> getAditional_day_pxc()), 2), 2, '.', '');
	$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');	
	
}else{ //A set of fees doesn't exist. We retrieve the next fee available.
	$serial_ppc = $priceObject -> getMaxPricePosible($serial_pro, $days, $serial_cou);

    if($serial_ppc){
        $priceObject -> setSerial_ppc($serial_ppc);
        $priceObject -> getData();
        $priceObject = get_object_vars($priceObject);
		
		$closestDurationPrice = $priceObject['price_ppc'];
		$closestDurationCost = $priceObject['cost_ppc'];
		$closestDurationRangeMax = $priceObject['max_ppc'];
		$closestDurationRangeMin = $priceObject['min_ppc'];
		$closestDurationPXC = $priceObject['serial_pxc'];
		$productExtraDays = 0;
        $closestDurationPrice = number_format(round($closestDurationPrice, 2), 2, '.', '');
		$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');	
    }else{//If there's not a fee, the operator can't make the sale for tha amount of days given.
        $message='No existe una tarifa en este producto para los '.$days.' d&iacute;as seleccionados.';
    }
}

$smarty->register('closestDurationPrice,closestDurationCost,closestDurationRangeMax,closestDurationRangeMin,fixedPrice,message');
$smarty->assign('container','none');
$smarty->display('helpers/loadsSales/calculatePrice.'.$_SESSION['language'].'.tpl');
?>
