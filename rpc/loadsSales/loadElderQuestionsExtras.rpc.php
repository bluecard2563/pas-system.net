<?php
/*
File:loadElderQuestionsExtras.rpc.php
Author: Esteban Angulo
Creation Date: 22/03/2010
*/
	Request::setString('editionAllowed');	

	$customer = new Customer($db);
	$sessionLanguage = $_SESSION['serial_lang'];
	$question = new Question($db);
	$maxSerial = $question -> getMaxSerial($sessionLanguage);
	
	$serial_cus = $_POST['customer'];
	$extraPosition = $_POST['count'];
	
	$questionList = Question::getActiveQuestions($db, $sessionLanguage, $serial_cus);
	
	$smarty -> register('questionList,extraPosition,editionAllowed');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/loadsSales/loadElderQuestionsExtras.' . $_SESSION['language'] . '.tpl');
?>
