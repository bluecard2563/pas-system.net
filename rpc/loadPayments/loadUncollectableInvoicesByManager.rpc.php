<?php
/*
 * File: loadUncollectableInvoicesByManager.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 23/06/2010, 09:38:31 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 23/06/2010, 09:38:31 AM
 */

Request::setString('serial_mbc');
Request::setString('parameter');

$uncollectables=Invoice::getUncollectableInvoices($db, $parameter, $serial_mbc);
$items=sizeof($uncollectables);
$pages=round($items/15)+1;

$smarty->assign('container','none');
$smarty->register('uncollectables,pages');
$smarty->display('helpers/loadPayments/loadUncollectableInvoicesByManager.'.$_SESSION['language'].'.tpl');
?>
