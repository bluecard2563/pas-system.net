<?php
/*
 * File: loadExcessPaymentsTablerpc.php
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2010, 03:17:24 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 17/05/2010, 03:17:24 PM
 */

  Request::setInteger('serial_dea');
  Request::setInteger('serial_cou');

  $excessPaymentsList=Payment::getExcessPaymentsByBranch($db, $serial_dea, $serial_cou);

  $smarty->register('excessPaymentsList');
  $smarty->assign('container','none');
  $smarty->display('helpers/loadPayments/loadExcessPaymentsTable.'.$_SESSION['language'].'.tpl');
?>
