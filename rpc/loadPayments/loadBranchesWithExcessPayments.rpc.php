<?php
/*
 * File: loadBranchesWithExcessPaymentsrpc.php
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2010, 03:10:35 PM
 * Modifies By: David Bergmann
 * Last Modified: 05/07/2010
 */

Request::setString('dea_serial_dea');

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$branchList=Dealer::getBranchesWithExcessPayments($db, $dea_serial_dea, $_SESSION['serial_dea']);
} else {
	$branchList=Dealer::getBranchesWithExcessPayments($db, $dea_serial_dea);
}

$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/loadBranches.'.$_SESSION['language'].'.tpl');
?>
