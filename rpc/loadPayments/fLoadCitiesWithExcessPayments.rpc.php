<?php
/*
 * File: fLoadCitiesWithExcessPayments.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2010, 01:48:56 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 17/05/2010, 01:48:56 PM
 */

Request::setString('serial_cou');

$cityExcess=City::getCitiesWithExcessPayments($db, $serial_cou, $_SESSION['serial_mbc']);

$smarty->register('cityExcess');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/fLoadCitiesWithExcessPayments.'.$_SESSION['language'].".tpl");
?>
