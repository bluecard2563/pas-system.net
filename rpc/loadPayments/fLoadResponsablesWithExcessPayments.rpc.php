<?php
/*
 * File: fLoadCitiesWithExcessPayments.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2010, 01:48:56 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 17/05/2010, 01:48:56 PM
 */

Request::setString('serial_cit');

$responsablesList=UserByDealer::getResponsiblesWithExcessPayments($db, $serial_cit, $_SESSION['serial_mbc']);

$smarty->register('responsablesList');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>
