<?php 
/*
File: loadBranchesByDealer.php
Author: Gabriela Guerrero
Creation Date: 20/05/2010
Last Modified: 
Modified By: 
*/

$dea_serial_dea=$_POST['dea_serial_dea'];
$serial_cit=$_POST['serial_cit'];
$branch = new Dealer($db);

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$branchList = $branch->getBranchesByInvoice($dea_serial_dea, $serial_cit, $_SESSION['serial_dea']);
} else {
	$branchList = $branch->getBranchesByInvoice($dea_serial_dea, $serial_cit);
}

$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/reports/loadBranchesByDealer.'.$_SESSION['language'].'.tpl');
?>