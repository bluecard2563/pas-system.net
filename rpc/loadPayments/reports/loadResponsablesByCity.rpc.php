<?php 
/*
File: loadResponsablesByCity.php
Author: Gabriela Guerrero
Creation Date: 20/05/2010
Last Modified: 
Modified By: 
*/

$serial_cit=$_POST['serial_cit'];
$user = new User($db);
$userList = $user->getUsersByInvoice($serial_cit, $_SESSION['serial_mbc']);

$smarty->register('userList');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/reports/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>