<?php 
/*
File: loadDealersByResponsable.php
Author: Gabriela Guerrero
Creation Date: 20/05/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$serial_usr=$_POST['serial_usr'];
$dealer = new Dealer($db);

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList = $dealer->getDealersByInvoice($serial_cou, $serial_usr, $_SESSION['dea_serial_dea']);
} else {
	$dealerList = $dealer->getDealersByInvoice($serial_cou, $serial_usr);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/reports/loadDealersByResponsable.'.$_SESSION['language'].'.tpl');
?>