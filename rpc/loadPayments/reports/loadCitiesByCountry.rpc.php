<?php 
/*
File: loadCitiesByCountry.php
Author: Gabriela Guerrero
Creation Date: 20/05/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$city = new City($db);
$cityList = $city->getCitiesByInvoice($serial_cou, $_SESSION['serial_mbc']);

$smarty->register('cityList');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/reports/loadCitiesByCountry.'.$_SESSION['language'].'.tpl');
?>