<?php
/*
 * File: loadPaymentDetailTable.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 17/06/2010
 * Modifies By: 
 * Last Modified: 
 */
$serial_cou=$_POST['serial_cou'];
$serial_mbc=$_POST['serial_mbc'];
$dateFrom=$_POST['dateFrom'];
$dateTo=$_POST['dateTo'];
	
$payment = new Payment($db);
$detailPaymentsList = $payment->getPaymentDetailByCountry($serial_cou,$serial_mbc,$dateFrom,$dateTo);
$total_pages = (int)(count($detailPaymentsList)/10)+1;

if(is_array($detailPaymentsList)){
	$total_to_verify=0;
	foreach($detailPaymentsList as $p){
		$total_to_verify+=$p['amount_pyf'];
	}
}

$smarty->register('detailPaymentsList,total_to_verify,total_pages');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/loadPaymentDetailTable.'.$_SESSION['language'].'.tpl');
?>
