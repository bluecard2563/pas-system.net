<?php
/*
 * File: loadDealersWithExcessPayments.php
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2010, 02:33:30 PM
 * Modifies By: David Bergmann
 * Last Modified: 05/07/2010
 */

Request::setString('serial_cit');
Request::setString('serial_usr');
Request::setString('dealer_cat');

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList=Dealer::getDealersWithExcessPayments($db, $serial_cit, $serial_usr, $dealer_cat, $_SESSION['dea_serial_dea']);
} else {
	$dealerList=Dealer::getDealersWithExcessPayments($db, $serial_cit, $serial_usr, $dealer_cat);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersFiltered.'.$_SESSION['language'].'.tpl');
?>
