<?php 
/*
File: loadManagersByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 17/06/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$mbc = new ManagerbyCountry($db);
$mbcList = $mbc->getManagersWithPaymentDetail($serial_cou, $_SESSION['serial_mbc']);


$smarty->register('mbcList');
$smarty->assign('container','none');
$smarty->display('helpers/loadPayments/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>