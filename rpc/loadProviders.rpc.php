<?php
/*
File: loadProviders.php
Author: Santiago Ben�tez
Creation Date: 19/02/2010 9:45
Last Modified: 19/02/2010 09:45
Modified By: Santiago Ben�tez
*/

$provider=new ServiceProvider($db);
if($_POST['serial_cou']){
	$serial_cou=$_POST['serial_cou'];
	$providerList=$provider->getProviders($serial_cou);
}

if(is_array($providerList)){
	foreach($providerList as &$pl){
		$pl['name_spv']=utf8_encode($pl['name_spv']);
	}
}

$smarty->register('providerList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadProviders.'.$_SESSION['language'].'.tpl');
?>