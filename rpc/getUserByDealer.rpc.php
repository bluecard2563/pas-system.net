<?php 
/*
File: getUserByDealer.php
Author: Gabriela Guerrero
Creation Date: 25/06/2010
Last Modified: 
Modified By: 
*/

$serial_dea=$_POST['serial_dea'];
$type=$_POST['type'];
$data = array();
$user = new User($db);

$serial_usr = $user->getUserByDealer($serial_dea,$type);
$user->setSerial_usr($serial_usr);
$user->getData();
if($user->isPlanetAssistUser()){
	$data[] = array('0'=>$serial_usr, '1'=>'1');
}
else{
	$data[] = array('0'=>$serial_usr, '1'=>'0');
}
echo json_encode($data);
?>