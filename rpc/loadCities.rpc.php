<?php 
/*
File: loadCities.php
Author: Patricio Astudillo M.
Creation Date: 30/12/2009 16:39
Last Modified: 11/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the cities from a specific country 
			 in a select object
*/

$city=new City($db);
//REPORTS
Request::setString('opt_text');
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}
if($_POST['serial_cou']){
	$serial_cou=$_POST['serial_cou'];
        if($_POST['all']){
            $cityList=$city->getCitiesByCountry($_POST['serial_cou']);
        }
        else{
            $cityList=$city->getCitiesByCountry($_POST['serial_cou'],$_SESSION['cityList']);
        }
}
if(is_array($cityList)){
	foreach($cityList as &$cl){
		$cl['name_cit']=($cl['name_cit']);
	}
}

$smarty->register('cityList,serial_cou,serial_sel,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadCities.'.$_SESSION['language'].'.tpl');
?>