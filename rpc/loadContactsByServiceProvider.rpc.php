<?php
/*
File: loadProviders.php
Author: Santiago Ben�tez
Creation Date: 30/03/2010 13:45
Last Modified: 30/03/2010 13:45
Modified By: Santiago Ben�tez
*/

$contactByServiceProvider=new ContactByServiceProvider($db);
if($_POST['serial_spv']){
	$serial_spv=$_POST['serial_spv'];
	$contactByServiceProviderList=$contactByServiceProvider->getContactsByServiceProvider($serial_spv);
}

if(is_array($contactByServiceProviderList)){
	foreach($contactByServiceProviderList as &$pl){
		$pl['name_csp']=utf8_encode($pl['name_csp']);
	}
}

$smarty->register('contactByServiceProviderList,serial_spv');
$smarty->assign('container','none');
$smarty->display('helpers/loadContactsByServiceProvider.'.$_SESSION['language'].'.tpl');
?>