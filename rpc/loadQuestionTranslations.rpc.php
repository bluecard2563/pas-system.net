<?php 
/*
File: loadQuestionTranslations.rpc.php
Author: Santiago Benítez
Creation Date: 22/01/2010 14:54
Last Modified: 22/01/2010 14:54
Modified By: Santiago Benítez
*/

Request::setString('serial_que');
$questionbl=new QuestionByLanguage($db);
$questionbl->setSerial_que($serial_que);
$data=$questionbl->getQuestionsBySerial(); //data that is going to be displayed in the table
$titles=array('#','Idioma','Pregunta','Actualizar','Eliminar'); //table's titles
$text="Nueva Traducci&oacute;n"; //button's text
$smarty->register('data,titles,text');
$smarty->assign('container','none');
$smarty->display('helpers/loadAdminTable.'.$_SESSION['language'].'.tpl');
?>