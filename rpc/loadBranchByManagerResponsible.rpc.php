<?php

/*
  Document   : loadBranchByManagerResponsible
  Created on : 18/08/2011, 04:42:26 PM
  Author     : Nicolás Flores
 */

Request::setString('serial_mbc,serial_usr,serial_branches');

$listOfBranches = Dealer::getBranchByManagerResponsible($db,$serial_mbc,$serial_usr,$serial_branches);

$smarty->register('listOfBranches,serial_mbc,serial_usr');
$smarty->assign('container','none');
$smarty->display('helpers/loadBranchByManagerResponsible.'.$_SESSION['language'].'.tpl');
?>
