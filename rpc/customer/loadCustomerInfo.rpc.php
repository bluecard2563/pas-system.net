<?php
/*
File: loadCustomerInfo.rpc
Author: David Bergmann
Creation Date: 11/02/2011
Last Modified:
Modified By:
*/

Request::setString('serial_cus');

$customer = new Customer($db, $serial_cus);
$customer->getData();

foreach ($customer as $c) {
	$data['first_name_cus'] = $customer->getFirstname_cus();
	$data['last_name_cus'] = $customer->getLastname_cus();
	$data['document_cus'] = $customer->getDocument_cus();
}

$smarty->register('data');
$smarty->assign('container','none');
$smarty->display('helpers/customer/loadCustomerInfo.'.$_SESSION['language'].'.tpl');
?>