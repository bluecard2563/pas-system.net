<?php 
/*
File: loadCountries.rpc.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 11/01/2010
Modified By: Patricio Astudillo
*/

Request::setString('view_all');
$country=new Country($db);
if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}

if($_POST['serial_zon']){
	$serial_zon=$_POST['serial_zon'];
	if($view_all != 'YES'){
		$restricted_view = $_SESSION['countryList'];
	}
	$countryList=$country->getCountriesByZone($_POST['serial_zon'], $restricted_view);
}

if(is_array($countryList)){
	foreach($countryList as &$cl){
		$cl['name_cou']=utf8_encode($cl['name_cou']);
	}
}

$smarty->register('countryList,serial_zon,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadCountries.'.$_SESSION['language'].'.tpl');
?>