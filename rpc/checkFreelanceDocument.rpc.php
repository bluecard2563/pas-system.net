<?php 
/*
File: checkFreelanceDocument.php
Author: Esteban Angulo
Creation Date: 31/12/2009 11:56
Last Modified:
Modified By:
Description: File which verifies if there is another freelance with 
			 the same document number in the system.
*/

Request::setString('txtDocument');
if($_POST['txtSerial_fre']){
	$serial_fre=$_POST['txtSerial_fre'];
	echo $serial_fre;
}

$txtDocument=trim(strtolower(utf8_decode($txtDocument)));

$freelance=new Freelance($db);
if($freelance->existsFreelanceDocument($txtDocument,$serial_fre)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>