<?php 
/*
File: loadDealers.php
Author: Patricio Astudillo
Creation Date: 14/01/2010
Last Modified:
Modified By:
Description: File which loads all branches acording a specific dealer.
*/

$dealer = new Dealer($db);
$params = array();
$paramItem = 0;

Request::setString('opt_text');

$aux=array('field'=>'d.phone_sales_dea',
		   'value'=> 'NO',
		   'like'=> 1);
array_push($params,$aux);

if($_SESSION['isManager']){
	$aux = array('field'=>'mbc.serial_usr', 
			   'value'=> $_SESSION['serial_usr'],
			   'like'=> 1);
	array_push($params,$aux);
}

if($_POST['serial_cou']){
	$aux=array('field'=>'mbc.serial_cou', 
			   'value'=> $_POST['serial_cou'],
			   'like'=> 1);
	array_push($params,$aux);
}


//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$aux=array('field'=>'d.serial_dea',
	  'value'=> $_SESSION['serial_dea'],
	  'like'=> 1);
	array_push($params,$aux);
}

if($_POST['dea_serial_dea']){
	$aux=array('0' => array('field'=>'d.dea_serial_dea',
			  'value'=> $_POST['dea_serial_dea'],
			  'like'=> 1));
}

if($_POST['calendar']){
	$calendar = $_POST['calendar'];
	$smarty->register('calendar');
}


$branchList = $dealer -> getDealers($aux);

if(is_array($branchList)){
	foreach($branchList as &$dl){
		$dl['name_dea']=$dl['name_dea'];
	}
}

$smarty->register('branchList,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadBranches.'.$_SESSION['language'].'.tpl');
?>