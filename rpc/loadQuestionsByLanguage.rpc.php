<?php
/*
File: loadQuestionsByLanguage.rpc.php
Author: Santiago Benitez
Creation Date: 05/05/2010 14:54
Last Modified: 05/05/2010 14:54
Modified By: Santiago Benitez
*/
Request::setString('serial_lang');
$question=new QuestionByLanguage($db);
$question->setSerial_lang($serial_lang);
$questions=$question->getQuestionsByLanguage();
$smarty->register('questions');
$smarty->assign('container','none');
$smarty->display('helpers/loadQuestionsByLanguage.'.$_SESSION['language'].'.tpl');

?>
