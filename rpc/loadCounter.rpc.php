<?php
/*
File:loadCounter.rpc.php
Author: Esteban Angulo
Creation Date: 13/04/2010
*/
	Request::setString('opt_text');
	Request::setString('serial_dea');
	$counter = new Counter($db);
	$counterList = "";
	
	if($serial_dea && $serial_dea != 'stop'){
		if($_POST['all'])
			$counterList = $counter -> getCountersByBranch($serial_dea, true);
		else
			$counterList = $counter -> getCountersByBranch($serial_dea);
	}
	
	
	$smarty->register('counterList,opt_text');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadCounter.'.$_SESSION['language'].'.tpl')
?>