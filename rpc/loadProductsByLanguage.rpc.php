<?php
/*
File: loadProductsByLanguage.rpc
Author: David Bergmann
Creation Date: 27/07/2010
Last Modified:
Modified By:
*/

Request::setString('code_lang');

$product = new Product($db);
$productList = $product->getProducts($code_lang);

$smarty->register('productList');
$smarty->assign('container','none');
$smarty->display('helpers/loadProductsByLanguage.'.$_SESSION['language'].'.tpl');
?>