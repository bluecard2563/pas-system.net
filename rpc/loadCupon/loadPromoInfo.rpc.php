<?php
/**
 * File:  loadPromoInfo
 * Author: Patricio Astudillo
 * Creation Date: 11-abr-2011, 11:07:38
 * Description:
 * Last Modified: 26-abr-2011, 15:26:00
 * Modified by: Santiago Arellano
 */

	Request::setInteger('serial_ccp');


	if($serial_ccp != ''){
		$cupon_count = CustomerPromo::countActiveCouponsByPromo($db, $serial_ccp);
	}

	$smarty -> register('cupon_count,serial_ccp');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/loadCupon/loadPromoInfo.'.$_SESSION['language'].'.tpl');
?>
