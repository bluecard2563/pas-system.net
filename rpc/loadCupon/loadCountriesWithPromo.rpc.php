<?php
/**
 * File:  loadCountriesWithPromo
 * Author: Patricio Astudillo
 * Creation Date: 11-abr-2011, 9:46:57
 * Description:
 * Last Modified: 11-abr-2011, 9:46:57
 * Modified by:
 */

	Request::setInteger('serial_cou');

	$registered_promos = CustomerPromo::getCustomerPromoByCountryAndStatus($db, $serial_cou, 'REGISTERED');
	if(!$registered_promos) $registered_promos = array();
	$active_promos = CustomerPromo::getCustomerPromoByCountryAndStatus($db, $serial_cou, 'ACTIVE');
	if(!$active_promos) $active_promos = array();

	$promos_list = array_merge($registered_promos, $active_promos);
	//Debug::print_r($promos_list);

	$smarty -> register('promos_list');
	$smarty -> assign('container','none');
	$smarty -> display('helpers/loadCupon/loadCountriesWithPromo.'.$_SESSION['language'].'.tpl');
?>