<?php
/*
File: fDeletePrintedInvoice.rpc
Author: Nicolas Flores
Creation Date: 14/06/2011
Last Modified:
Modified By:
*/
if(GlobalFunctions::deleteFile(DOCUMENT_ROOT.'modules/billingToManager/reports/'.$_POST['fileName'].'.pdf')){
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>