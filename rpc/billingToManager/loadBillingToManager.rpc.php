<?php
/*
File: loadBillingToManager.rpc
Author: David Bergmann
Creation Date: 11/10/2010
Last Modified:
Modified By:
*/

Request::setString('serial_man');

$bill = BillingToManager::getBillingToManager($db, $serial_man);

$smarty->register('bill,serial_man');
$smarty->assign('container','none');
$smarty->display('helpers/billingToManager/loadBillingToManager.'.$_SESSION['language'].'.tpl');
?>