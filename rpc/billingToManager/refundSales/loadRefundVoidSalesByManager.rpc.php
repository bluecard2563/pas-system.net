<?php
/*
 * File: loadRefundVoidSalesByManager.rpc.php
 * Author: Nicolas Flores
 * Creation Date: 21/06/2011
 */

Request::setString('serial_mbc,dateFrom,dateTo');

$selectableSales = Sales::getVoidAndRefundedSalesByManager($db, $serial_mbc, $dateFrom, $dateTo);
if($selectableSales){
	foreach($selectableSales as &$st){
		//DETAILED INFORMATION FOR THE LOG
		$log_description = unserialize($st['description_slg']);
		
		if(is_array($log_description['global_info'])){ //IF THE LOG HAS THE LATEST LOG STRUCTURE
			//USER WHO AUTHORIZED THE REFUND
			$auth_user_info = User::getBasicInformationForUser($db, $log_description['global_info']['authorizing_usr']);
			$st['first_name_usr'] = $auth_user_info['first_name_usr'];
			$st['last_name_usr'] = $auth_user_info['last_name_usr'];				
			
			$st['observations'] = $log_description['global_info']['authorizing_obs'];
			$standby_auth_date = $log_description['global_info']['authorizing_date'];
			
			$st['request_obs'] = $log_description['global_info']['request_obs'];
		}

		//GETTING LATEST STAND-BY CHANGE
		$standby_log = SalesLog::getLastAuthorizedLogForSerial($db, $st['serial_sal'], 'STAND_BY');
		if(is_array($standby_log)){
			$standby_log_observations = unserialize($standby_log['description_slg']);
			
			//STAND-BY AUTHORIZATION OBSERVATIONS
			if(is_array($standby_log_observations['global_info'])){ //IF THE LOG HAS THE LATEST LOG STRUCTURE
				$standby_auth_date = $standby_log_observations['global_info']['authorizing_date'];
			}else{
				$standby_auth_date = $standby_log['date_slg'];
			}

			//DAYS AFTER STAND-BY
			// check stand by date format 
			if(strpos($standby_auth_date, '/')){
				//first explode in case it has time included
				$auxTemp = explode(' ', $standby_auth_date);
				//use date part to give the standard format Y-m-d
				$tempDate = explode('/', $auxTemp[0]);
				$standby_auth_date = $tempDate[2].'-'.$tempDate['1'].'-'.$tempDate[0];
				$standby_auth_date.=($auxTemp[1]!='')?' '.$auxTemp[1]:'';
			}
			$standby_auth_date = strtotime($standby_auth_date);
			$begin_date_sal = strtotime($st['begin_date_sal']);
			$st['days_after_stand_by'] = round(abs($begin_date_sal-$standby_auth_date)/60/60/24);
		}else{ //FOR MIGRATED CARD WHICH DOESN'T HAVE ANY STANDBY LOG
			$st['days_after_stand_by'] = 'N/A';
		}	
				
	}
	$smarty->register('selectableSales');
}

$smarty->assign('container','none');
$smarty->display('helpers/billingToManager/refundSales/loadRefundVoidSalesByManager.'.$_SESSION['language'].'.tpl');
?>
