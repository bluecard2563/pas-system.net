<?php
/*
 * File: loadManagersByCountry.rpc.php
 * Author: Nicolas Flores
 * Creation Date: 21/06/2011
 */

Request::setString('serial_cou');

if($_SESSION['serial_mbc'] != 1){
	$serial_mbc = $_SESSION['serial_mbc'];
}else{
	$serial_mbc = NULL;
}

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$mbcList = ManagerbyCountry::loadManagersWithSales($db, $serial_mbc, $serial_dea, $serial_cou);
if($mbcList){
	$smarty->register('mbcList');
}

$smarty->assign('container','none');
$smarty->display('helpers/billingToManager/refundSales/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>
