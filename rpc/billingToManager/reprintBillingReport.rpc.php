<?php
/*
File: reprintBillingReport.rpc
Author: David Bergmann
Creation Date: 11/10/2010
Last Modified:
Modified By:
*/

set_time_limit(36000);
ini_set('memory_limit','512M');
include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
//check if is called by ajax or is included
if(!$pay_bill){
	Request::setString('serial_btm');
	Request::setString('serial_man');
}
$sales = Manager::getBillingToManagerReport($db, $serial_man, 'dea.name_dea',$serial_btm);

if(is_array($sales)){
	//HEADER
	$pdf->ezText('<b>'.utf8_decode("FACTURACIÓN A REPRESENTANTES").'</b>', 14, array('justification'=>'center'));
	$pdf->ezSetDy(-20);

	$billingToManager = new BillingToManager($db, $serial_btm);
	$billingToManager ->getData();
	$aux_data = $billingToManager ->getAux();

	$pdf->ezText('<b>'.utf8_decode("Representante: ").'</b>'.$aux_data['name_man'], 9, array('justification'=>'left'));
	$pdf->ezSetDy(-1);

	$date = $billingToManager ->getDate_btm();
	$billingToManager=new BillingToManager($db);
	if($billingToManager->getLastBillToManager($_POST['selManager'])) {
		$lastDate = $billingToManager->getLastBillToManager($_POST['selManager']);
	} else {
		$lastDate = 'N/A';
	}

	$pdf->ezText('<b>'.utf8_decode("Fecha de reporte anterior: ").'</b>'.$lastDate, 9, array('justification'=>'left'));
	$pdf->ezSetDy(-1);
	$pdf->ezText('<b>'.utf8_decode("Fecha de reporte actual: ").'</b>'.$date, 9, array('justification'=>'left'));
	$pdf->ezSetDy(-1);
	$pdf->ezText('<b>'.utf8_decode("Número de tarjetas vendidas: ").'</b>'.sizeof($sales), 9, array('justification'=>'left'));

	$pdf->ezSetDy(-20);

	$salesToShow = array();
	$total = 0;

	foreach($sales as $s){
		//data for PDF
		$aux = array();
		$aux['card_number_sal'] = $s['card_number_sal'];
		$aux['status_sal'] = $global_salesStatus[$s['status_sal']];
		$aux['in_date_sal'] = $s['in_date_sal'];
		$aux['name_cus'] = $s['name_cus'];
		$aux['name_pbl'] = $s['name_pbl'];
		$aux['begin_date_sal'] = $s['begin_date_sal'];
		$aux['end_date_sal'] = $s['end_date_sal'];
		$aux['days_sal'] = $s['days_sal'];
		$aux['total_sal'] = $s['total_sal'];
		$aux['name_dea'] = $s['name_dea'];
		$aux['name_usr'] = $s['name_usr'];

		if($s['international_fee_status'] == 'TO_REFUND') {
			$aux['international_fee_status'] = "A Reembolsar";
			$aux['international_fee_amount'] = "(".$s['international_fee_amount'].")";
			$total -= $s['international_fee_amount'];
		} else {
			$aux['international_fee_status'] = "A Cobrar";
			$aux['international_fee_amount'] = $s['international_fee_amount'];
			$total += $s['international_fee_amount'];
		}
		array_push($salesToShow, $aux);
	}

	//adds total row
	$aux = array();
	$aux['card_number_sal'] = '';
	$aux['status_sal'] = '';
	$aux['in_date_sal'] = '';
	$aux['name_cus'] = '';
	$aux['name_pbl'] = '';
	$aux['begin_date_sal'] = '';
	$aux['end_date_sal'] = '';
	$aux['days_sal'] = '';
	$aux['total_sal'] = '';
	$aux['name_dea'] = '';
	$aux['name_usr'] = '';
	$aux['international_fee_status'] = "<b>Total:</b>";
	$aux['international_fee_amount'] = number_format($total, 2,'.','');

	array_push($salesToShow, $aux);

	$titles=array('card_number_sal'=>utf8_decode('<b># Tarjeta</b>'),
				  'status_sal'=>  '<b>Estado de la Tarjeta</b>',
                  'in_date_sal'=>utf8_decode('<b>Fec. Ingreso</b>'),
                  'name_cus'=>utf8_decode('<b>Cliente</b>'),
                  'name_pbl'=>utf8_decode('<b>Producto</b>'),
                  'begin_date_sal'=>utf8_decode('<b>Desde</b>'),
				  'end_date_sal'=>utf8_decode('<b>Hasta</b>'),
				  'days_sal'=>utf8_decode('<b>Tiempo</b>'),
                  'total_sal'=>utf8_decode('<b>Precio</b>'),
                  'name_dea'=>utf8_decode('<b>Comercializador</b>'),
                  'name_usr'=>utf8_decode('<b>Counter</b>'),
				  'international_fee_status'=>utf8_decode('<b>Estado</b>'),
				  'international_fee_amount'=>utf8_decode('<b>Monto</b>'));

	$pdf->ezTable($salesToShow,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'card_number_sal'=>array('justification'=>'center','width'=>40),
										'status_sal'=>array('justification'=>'center','width'=>55),
                                        'in_date_sal'=>array('justification'=>'center','width'=>55),
                                        'name_cus'=>array('justification'=>'center','width'=>90),
                                        'name_pbl'=>array('justification'=>'center','width'=>60),
                                        'begin_date_sal'=>array('justification'=>'center','width'=>55),
                                        'end_date_sal'=>array('justification'=>'center','width'=>55),
                                        'days_sal'=>array('justification'=>'center','width'=>40),
                                        'total_sal'=>array('justification'=>'center','width'=>50),
                                        'name_dea'=>array('justification'=>'center','width'=>90),
										'name_usr'=>array('justification'=>'center','width'=>90),
										'international_fee_status'=>array('justification'=>'center','width'=>70),
										'international_fee_amount'=>array('justification'=>'center','width'=>50))));

	$pdf->ezSetDy(-20);
	$pdf->ezText('<b>'.utf8_decode("Nota: ").'</b>'.  utf8_decode("Si el monto aparece con paréntesis '()', éste monto es a favor del representante."), 8, array('justification'=>'left'));


	$pdfcode = $pdf->ezOutput();
	$pdfname = 'billing_'.date('U');
	$fp=fopen(DOCUMENT_ROOT.'modules/billingToManager/reports/'.$pdfname.'.pdf','wb');
	fwrite($fp,$pdfcode);
	fclose($fp);
	 if(!$pay_bill){
		echo json_encode($pdfname.'.pdf');
	 }else{
		 $pdfReady=TRUE;
	 }	
} else {
	 if(!$pay_bill){
		echo json_encode(FALSE);
	 }else{
		 $pdfReady=FALSE;
	 }	
}

?>