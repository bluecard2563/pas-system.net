<?php
/*
File: loadBillingCountry.rpc
Author: Nicolas Flores
Creation Date: 16/06/2011
Last Modified:
Modified By:
*/

Request::setInteger('serial_cou');

$bill = BillingToManager::getBillingToManagerByCountry($db, $serial_cou);

$smarty->register('bill,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/billingToManager/loadBillingCountry.'.$_SESSION['language'].'.tpl');
?>