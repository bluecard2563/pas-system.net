<?php
/*
 * File: loadFreeSalesByManager.rpc.php
 * Author: Nicolas Flores
 * Creation Date: 27/06/2011
 */

Request::setString('serial_mbc,dateFrom,dateTo');

$selectableSales = Sales::getAuthorizedFreeSalesByManager($db,$serial_mbc,$dateFrom,$dateTo);
if($selectableSales){
	foreach($selectableSales as &$st){
		$st['description_slg']= unserialize($st['description_slg']);
	}
	//Debug::print_r($selectableSales);
	$smarty->register('selectableSales');
}

$smarty->assign('container','none');
$smarty->display('helpers/billingToManager/billingFree/loadFreeSalesByManager.'.$_SESSION['language'].'.tpl');
?>
