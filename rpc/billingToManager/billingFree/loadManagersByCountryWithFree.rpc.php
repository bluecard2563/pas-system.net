<?php
/*
 * File: loadManagersByCountryWithFree.rpc.php
 * Author: Nicolas Flores
 * Creation Date: 27/06/2011
 */

Request::setString('serial_cou');

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$managerByCountry = new ManagerbyCountry($db);
$mbcList = $managerByCountry->getManagersWithFreeCard($serial_cou,$_SESSION['serial_mbc'],$serial_dea);
if($mbcList){
	$smarty->register('mbcList');
}
$smarty->assign('container','none');
$smarty->display('helpers/billingToManager/billingFree/loadManagersByCountryWithFree.'.$_SESSION['language'].'.tpl');
?>
