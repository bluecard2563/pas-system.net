<?php
	if($_POST['status_pay'] == 'CxC'){
		$invoice = new Invoice($db);
		$invoiceRaw = $invoice->getNotPayedInvoices($_POST['serial_cou'], $_POST['serial_cit'], $_POST['serial_usr'], $_POST['category_dea'], $_POST['serial_dea'], $_POST['dea_serial_dea']);
		if($invoiceRaw){
			echo json_encode(TRUE);
		}else{
			echo json_encode(FALSE);
		}
	}
	else{
		$payments = new Payment($db);
		$invoicesRaw = $payments->getPayedInvoices($_POST['serial_cou'], $_POST['serial_cit'], $_POST['serial_usr'], $_POST['category_dea'], $_POST['serial_dea'], $_POST['dea_serial_dea'], $_POST['status_pay']);
		if($invoicesRaw){
			echo json_encode(TRUE);
		}else{
			echo json_encode(FALSE);
		}
	}
?>
