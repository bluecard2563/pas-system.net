<?php
/*
File: loadOversPayResults.rpc.php
Author: Santiago Borja
Creation Date: 17/03/2010
*/
	Request::setString('overId');
	Request::setString('appliedTo');
	Request::setString('countrySel');
	
	$oversToPay = Over::getFilteredOversToPay($db,$overId,$appliedTo,$countrySel);

	$smarty->register('oversToPay');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsOvers/loadOversPayResults.'.$_SESSION['language'].'.tpl');
?>