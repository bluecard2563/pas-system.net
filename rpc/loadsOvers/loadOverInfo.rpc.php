<?php
/*
File: loadOverInfo.rpc.php
Author: Santiago Borja
Creation Date: 17/03/2010
*/

	Request::setString('appliedTo');
	$country = new Country($db);
	$loadedCountries = $country->getOwnCountries($_SESSION['countryList']);
	
	//ONLY FOR UPDATE OVER
	switch($appliedTo){
        case 'MANAGER': //SENDS DATA TO loadManagersByCountry.rpc to load the manager
        	Request::setString('manManager');
        	Request::setString('myCountry');
        	break;
        case 'DEALER':
        	Request::setString('serial_ove');     
			if($serial_ove){
				$over = new Over($db,$serial_ove);
				$over -> getData();
				$myDealers = $over -> getOverDealers(false);
			}
			$myCountry = $myDealers[0]['serial_cou'];
            break;
    }
	
	$smarty->register('loadedCountries,appliedTo,myDealers,myCountry,manManager');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadsOvers/loadOverInfo.'.$_SESSION['language'].'.tpl');
?>