<?php
/*
File: checkInvoice.php
Author: Santiago Benitez
Creation Date: 08/04/2010 10:20
Last Modified:
Modified By:
*/

if($_POST['txtInvoiceNumber']){
	$number_inv=$_POST['txtInvoiceNumber'];
}
if($_POST['type_dbc']){
	$type_dbm=$_POST['type_dbc'];
}
if($_POST['serial_mbc']){
	$serial_mbc=$_POST['serial_mbc'];
}

$mbc=new ManagerbyCountry($db, $serial_mbc);
$mbc->getData();

$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $mbc->getSerial_man(), $type_dbm, 'INVOICE');

$invoice=new Invoice($db);
$invoiceData=$invoice->getInvoice($number_inv, $serial_dbm);

$serial_inv = $invoiceData['serial_inv'];
$total_payed_pay = $invoiceData['total_payed_pay'];
$status_inv = $invoiceData['status_inv'];

if(!$serial_inv || $total_payed_pay!=0 || $status_inv=='VOID'){
    echo json_encode(false);
}else{
    echo json_encode(true);
}
?>