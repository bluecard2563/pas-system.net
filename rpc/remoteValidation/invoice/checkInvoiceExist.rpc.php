<?php
/*
File: checkInvoiceExist.php
Author: Nicolas Flores
Creation Date: 22/06/2010 10:20
 * check if an invoice is available to be reprinted
Last Modified:
Modified By:
*/
if($_POST['txtInvoiceNumber']){
	$number_inv=$_POST['txtInvoiceNumber'];
}
if($_POST['type_dbc']){
	$type_dbc=$_POST['type_dbc'];
}
if($_POST['serial_mbc']){
	$serial_mbc=$_POST['serial_mbc'];
}
$invoice=new Invoice($db);
$serial_inv=$invoice->getInvoiceExistToPrint($number_inv, $serial_mbc, $type_dbc);
if(!$serial_inv){
    echo json_encode(false);
}else{
    echo json_encode(true);
}
?>