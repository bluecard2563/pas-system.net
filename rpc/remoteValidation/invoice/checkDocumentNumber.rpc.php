<?php
/*
File: checkDocumentNumber.php
Author: Santiago Benitez
Creation Date: 31/03/2010 15:20
Last Modified:
Modified By:
*/

$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $_POST['serial_man'], $_POST['type_dbm'], 'INVOICE');

if(DocumentByManager::isNumberUsed($db, $serial_dbm, $_POST['number_inv'], 'INVOICE')){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>