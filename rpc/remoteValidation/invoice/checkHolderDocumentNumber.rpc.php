<?php
/**
 * file: checkHolderDocumentNumber
 * Author: Patricio Astudillo
 * Creation Date: 08/05/2012
 * Modification Date:  08/05/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('branch_id');
	Request::setString('customer_id');
	Request::setString('document_type');
	Request::setString('invoice_to');
	Request::setString('validate_document');
	
	if($validate_document == 'NO'){
		switch($document_type){
			case 'CI': //VALIDATE CI DOCUMENT
				if(GlobalFunctions::validate_ci($customer_id)):
					echo json_encode(true);
				else:
					echo json_encode(false);
				endif;
				
				break;
			case 'RUC':
				if($invoice_to == 'CUSTOMER'): //IF INVOICE TO CUSTOMER, VALIDATE RUC RIGHT AWAY.
					if(GlobalFunctions::validate_ruc($customer_id)):
						echo json_encode(true);
					else:
						echo json_encode(false);
					endif;
				else: //IF INVOICE TO DEALER, EVALUATE IF DOCUMENT HAS 10 OR 13 CHARS
					if(strlen($branch_id) == 10){ //10 CHARS: EVALUATE CI
						if(GlobalFunctions::validate_ci($branch_id)):
							echo json_encode(true);
						else:
							echo json_encode(false);
						endif;
					}else{ //13 CHARS EVALUATE RUC
						if(GlobalFunctions::validate_ruc($branch_id)):
							echo json_encode(true);
						else:
							echo json_encode(false);
						endif;
					}
				endif;				
				break;
			default:
				echo json_encode(true);
				break;
		}	
	}else{
		echo json_encode(true);
	}
?>
