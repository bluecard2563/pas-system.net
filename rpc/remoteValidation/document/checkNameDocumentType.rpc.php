<?php 
/*
File: checkNameDocumentType.rpc.php
Author: Santiago Benítez
Creation Date: 11/01/2010 
Last Modified: 11/01/2010  12:22
Modified By: Santiago Benítez
*/

Request::setString('txtNameDocumentType');
if($_POST['serial_doc']){
	$serial_doc = $_POST['serial_doc'];
}

$documentType = new DocumentType($db);
if($documentType -> documentTypeExists($txtNameDocumentType,$serial_doc)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>