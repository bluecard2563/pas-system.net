<?php
/*
File: checkRepeatedDocument.rpc.php
Author: Patricio Astudillo
Creation Date: 22/03/2010
Last Modified: 22/03/2010  12:22
Modified By: Patricio Astudillo
*/

Request::setInteger('serial_man');
Request::setString('type_dbm');
Request::setString('purpose_dbm');

if($_POST['serial_dbm']){
	$serial_dbm=$_POST['serial_dbm'];
}

$dbm = new DocumentByManager($db,NULL,NULL,$serial_man,$type_dbm, $purpose_dbm,NULL);
if($dbm->similarDocTypeExists($serial_dbm)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>
