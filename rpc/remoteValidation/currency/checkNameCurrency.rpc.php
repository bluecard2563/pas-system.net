<?php 
/*
File: checkNameCurrency.rpc.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 07/01/2010 11:38
Modified By: David Bergmann
*/

Request::setString('txtNameCurrency');

if($_POST['serial_cur']){
	$serial_cur = $_POST['serial_cur'];
}
$txtNameCurrency=trim(strtolower(utf8_decode($txtNameCurrency)));
$currency=new Currency($db);
if($currency->currencyExist($txtNameCurrency, $serial_cur )){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>