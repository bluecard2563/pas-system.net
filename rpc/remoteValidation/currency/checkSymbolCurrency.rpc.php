<?php 
/*
File: checkSymbolCurrency.rpc.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 07/01/2010 11:38
Modified By: David Bergmann
*/

Request::setString('txtSymbol_cur');

/*if(htmlentities($_POST['txtSymbol_cur'])){
	$serial_cur = htmlentities($_POST['txtSymbol_cur']);
}*/
if($_POST['serial_cur']){
	$serial_cur=$_POST['serial_cur'];
}

$txtSymbol_cur=trim(strtolower(utf8_decode($txtSymbol_cur)));
$currency=new Currency($db);
if($currency->currencySymbolExist($txtSymbol_cur, $serial_cur)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>