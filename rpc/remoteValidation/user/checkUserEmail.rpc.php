<?php 
/*
File: checkUserEmail.php
Author: Esteban Angulo
Creation Date: 4/01/2010 16:20
Last Modified:
Modified By:
Description: File which verifies if there is another user with 
			 the same email address in the system.
*/

Request::setString('txtMail');
if($_POST['serial_usr']){
	$serial_usr=$_POST['serial_usr'];
}
$txtMail=trim(strtolower(utf8_decode($txtMail)));
$user=new User($db);
if($user->existsUserEmail($txtMail,$serial_usr)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>