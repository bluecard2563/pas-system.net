<?php 
/*
File: checkTypeUser.php
Author: Esteban Angulo
Creation Date: 08/02/2010 16:20
Last Modified:
Modified By:
Description: File which verifies if a user has dealers assosiated 
			 in the system.
*/

$serial_usr=$_POST['serial_usr'];
$profile=$_POST['selProfile'];

if($_POST['selStatus']=='INACTIVE'){
	if($profile!='COUNTER'){
		$user=new User($db);
		if($user->hasDealers($serial_usr)){
			echo json_encode(false);
		}else{
			echo json_encode(true);
		}
	}
}
else{
	echo json_encode(true);
}

?>