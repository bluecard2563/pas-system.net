<?php 
/*
File: checkUserDocument.php
Author: Esteban Angulo
Creation Date: 4/01/2010 16:20
Last Modified:
Modified By:
Description: File which verifies if there is another user with 
			 the same document number in the system.
*/

Request::setString('txtDocument');
if($_POST['serial_usr']){
	$serial_usr=$_POST['serial_usr'];
}

$txtDocument=trim(strtolower(utf8_decode($txtDocument)));

$user=new User($db);
if($user->existsUserDocument($txtDocument,$serial_usr)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>