<?php 
/*
File: checkUsername.php
Author: Esteban Angulo
Creation Date: 29/12/2009 12:00
Last Modified:
Modified By:
Description: File that verifies if there is another user with 
			 the same username in the system.
*/

Request::setString('txtUsername');
//echo $txtUsername;
if($_POST['serial_usr']){
	$serial_usr=$_POST['serial_usr'];
}

$txtUsername=trim(strtolower(utf8_decode($txtUsername)));
//echo $txtUsername;
$user=new User($db);
$user-> setUsername_usr($txtUsername);

if($user->existsUser($serial_usr)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}

?>