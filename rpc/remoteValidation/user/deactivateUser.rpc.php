<?php 
/*
File: deactivateUser.php
Author: Esteban Angulo
Creation Date: 01/07/2010
Description: File which verifies if there a user can be deactivated or not.
*/

$serial_usr=$_POST['serial_usr'];
$user=new User($db);
$deactivate=$user->checkDeactivateUser($serial_usr);
//Debug::print_r($deactivate);
foreach($deactivate as $key=>$k){
	if($k!='0'){
		switch ($key) {
			case '0': 
					  $desc='responsible';
					  break;
			case '1':
					  $desc='comissionist';
					  break;
			case '2':
					  $desc='audit';
					  break;

			default: $desc='';
		}
	}
}

$smarty->register('desc');
$smarty->assign('container','none');
$smarty->display('helpers/deactivateUserDialog.'.$_SESSION['language'].'.tpl');

?>