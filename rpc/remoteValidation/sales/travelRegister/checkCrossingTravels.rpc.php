<?php
/*
File:checkCrossingTravels.rpc.php
Author: Nicolas Flores
Creation Date:14.04.2010
Modified By:
Last Modified:
 * Description: Verify if a client already has a travel between the dates that are going to register
*/

Request::setInteger('serial_cus');
Request::setString('start_dt');
Request::setString('end_dt');
Request::setString('exclude_ids');

$travelLog= new TravelerLog($db);

if($travelLog->existCrossingTravel($serial_cus,$start_dt,$end_dt,$exclude_ids)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}

?>
