<?php
/*
File:evaluateCardDates.rpc.php
Author: Esteban Angulo
Creation Date: 09/04/2010
*/


if($_POST['serialCus']){
	$serial_cus=$_POST['serialCus'];
	$sale=new Sales($db);
	$sale->getData();
	$salesPeriod=$sale->getSaleDates($serial_cus,$_POST['beginDate'],$_POST['endDate'], $_POST['serialSal']);

	if($salesPeriod==1){
		echo json_encode(true);
	}
	else{
		echo json_encode(false);
	}
}
?>
