<?php
/*
File: checkDestinationRestricted.rpc
Author: David Bergmann
Creation Date: 30/07/2010
Last modified:
Modified By:
*/

Request::setString('serial_pro');

$product = new Product($db, $serial_pro);
$product->getData();

if($product->getDestination_restricted_pro() == 'YES') {
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}
?>