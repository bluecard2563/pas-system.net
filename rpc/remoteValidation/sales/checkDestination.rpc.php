<?php
/**
 * File: checkDestination
 * Author: Patricio Astudillo
 * Creation Date: 18-feb-2013
 * Last Modified: 18-feb-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('serial_pxc');
	Request::setInteger('serial_cou');
	
	if(AllowedDestinations::canSellProductToDestination($db, $serial_pxc, $serial_cou)){
		echo json_encode(true);
	}else{
		echo json_encode(false);
	}
?>
