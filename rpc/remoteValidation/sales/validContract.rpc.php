<?php
/**
 * File: validContract
 * Author: Patricio Astudillo
 * Creation Date: 23/06/2012
 * Modified By: Patricio Astudillo
 * Last Modified: 23/06/2012
 */

    Request::setInteger('card_number_sal');
    Request::setInteger('code');
    
    $sale = new Sales($db);
    $sale ->setCardNumber_sal($card_number_sal);
    if($sale->getDataByCardNumber()):
        $verification_code = substr( (string)($sale->getSerial_sal().$sale->getSerial_cus().$sale->getSerial_pbd()), 3, 10);
    
        if($verification_code == $code):
            $valid_contract = true;
        else:
            $valid_contract = false;
        endif;
        
    else:
        $valid_contract = false;
    endif;
    
    $smarty -> register('valid_contract');
    $smarty -> assign('container', 'none');
    $smarty -> display('helpers/loadsSales/validContract.'.$_SESSION['language'].'.tpl');
?>
