<?php
/*
File:checkCardNumber.rpc
Author: Esteban Angulo
Creation Date: 10/03/2010
*/

Request::setString('txtCardNum');

$stock=new Stock($db);
$manual_number_data=$stock->validateManualNumber($txtCardNum);
$return_information=array();

if($manual_number_data['is_sold']=='FALSE'){
	if($manual_number_data['owner']=='TRUE'){
		$return_information['error']='with_owner';
	    $counter=new Counter($db,$manual_number_data['owner_id']);
	    $city= new City($db);
	    $dealer=new Dealer($db);
	    
	    $counter->getData();
		$auxData=$counter->getAux_cnt();
		$countryList = Country::getAllAvailableCountries($db);
	
	    //PRE-LOAD INFORMATION
	    //CounterName
		$counterName=$counter->getAux_cnt();
		$return_information['counterName'] = utf8_encode($counterName['counter']);
		$return_information['counter_id']=$manual_number_data['owner_id'];
		
	    //Dealer Data
	    $deaData=Dealer::getDealerCity($db, $manual_number_data['owner_id']);
	    $return_information['dealerOfUser']=$counter->getSerial_dea();
	
	    //Country currencies
		$curBycountry=new CurrenciesByCountry($db);
		$return_information['currencies']=$curBycountry->getCurrenciesDealersCountry($counter->getSerial_dea());
		$return_information['currenciesNumber']=sizeof($return_information['currencies']);

	    $dealer->setSerial_dea($return_information['dealerOfUser']);
	    $dealer->getData();
	    $return_information['name_dea']=utf8_encode($dealer->getName_dea());
	    $return_information['serialDealer']=$dealer->getDea_serial_dea();

		//Makes Seller Code
		$return_information['sellerCode']=$deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer->getCode_dea().'-'.$return_information['serialDealer'].'-'.$manual_number_data['owner_id'];
		
		//EmissionCity
		$return_information['emissionPlace']=$deaData['name_cit'];
		
		//Get the Country Serial for calculate the price of the product to be sold.
		$return_information['serial_cou']=$deaData['serial_cou'];	
	}else{
		$return_information['error']='no_owner';
		
		if($manual_number_data['in_range']=='FALSE'){
			$return_information['error']='out_of_range';
		}		
	}
}else{
	$return_information['error']='was_sold';
}


echo json_encode($return_information);