<?php 
/*
File: checkQuestion.php
Author: Santiago Benítez
Creation Date: 12/01/2010 12:43
Last Modified: 12/01/2010 12:43
Modified By: Santiago Benítez
*/

Request::setString('txtText');
if($_POST['serial_que']){
	$serial_que=$_POST['serial_que'];
}
$txtText=trim(strtolower(utf8_decode($txtText)));
$questionbl=new QuestionByLanguage($db);
if($questionbl->questionByLanguangeExists($txtText,$serial_que)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}

?>