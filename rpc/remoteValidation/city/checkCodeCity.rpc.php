<?php
/*
File: checkCodeCity.rpc.php
Author: David Bergmann
Creation Date: 11/02/2010
Last Modified:
Modified By:
*/

Request::setString('txtCodeCity');
Request::setInteger('serial_cou');

if($_POST['serial_cit']){
	$serial_cit = $_POST['serial_cit'];
}

$txtCodeCity=trim(strtolower(utf8_decode($txtCodeCity)));

$city = new City($db);
if($city -> cityCodeExists($txtCodeCity,$serial_cit)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>