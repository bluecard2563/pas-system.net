<?php 
/*
File: checkNameCity.rpc.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 15:57
Modified By: David Bergmann
*/

Request::setString('txtNameCity');
Request::setInteger('serial_cou');

if($_POST['serial_cit']){
	$serial_cit = $_POST['serial_cit'];
}

$txtNameCity=trim(strtolower(utf8_decode($txtNameCity)));

$city = new City($db);
if($city -> cityExists($txtNameCity,$serial_cou,$serial_cit)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>