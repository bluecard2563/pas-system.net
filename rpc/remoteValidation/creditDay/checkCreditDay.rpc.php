<?php 
/*
File: checkRestriction.rpc.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 14:14
Last Modified:
Modified By:
Description: File which verifies if there is another type of restriction
			 in the system.
*/

Request::setString('serial_cou');
if($_POST['txtCDay']){
	$dayInfo=$_POST['txtCDay'];
}else if($_POST['txtUpCreditD']){
	$dayInfo=$_POST['txtUpCreditD'];
}

if($_POST['serial_cdd']){
	$serial_cdd=$_POST['serial_cdd'];
}

$dayInfo=trim(strtolower(utf8_decode($dayInfo)));

$creditD=new CreditDay($db);
if($creditD->creditDayExists($dayInfo,$serial_cou,$serial_cdd)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>