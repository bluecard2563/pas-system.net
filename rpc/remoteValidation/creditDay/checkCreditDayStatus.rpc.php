<?php 
/*
File: checkCreditDayStatus.rpc.php
Author: 
Creation Date: 08/02/2010 
Last Modified:
Modified By:
Description: File which verifies if there is another type of restriction
			 in the system.
*/

Request::setString('serial_cdd');
if($_POST['selStatus']=='INACTIVE'){
	$creditD=new CreditDay($db);
	if($creditD->creditDayStatus($serial_cdd)){
		echo json_encode(false);
	}else{
		echo json_encode(true);
	}
}
else{
	echo json_encode(true);
}
?>