<?php 
/*
File: checkNameCountry.rpc.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 
Modified By: 
*/

Request::setString('txtNameCountry');

if($_POST['serial_cou']){
	$serial_cou = $_POST['serial_cou'];
}

$txtNameCountry=trim(strtolower(utf8_decode($txtNameCountry)));

$country=new Country($db);
if($country->countryExists($txtNameCountry,$serial_cou)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>