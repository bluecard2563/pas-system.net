<?php
/*
File: checkContactByServiceProviderEmail.rpc.php
Author: Santiago Ben�tez
Creation Date: 19/02/2010 10:35
Last Modified: 19/02/2010 10:35
Modified By: Santiago Ben�tez
*/

Request::setString('email');
Request::setString('service_provider');
Request::setString('serial_spc');

$contact=new ContactByServiceProvider($db);
$txtEmail=trim(strtolower($email));

if($txtEmail){
	if($contact->contactByServiceProviderEmailExists($txtEmail,$service_provider,$serial_spc)){
		echo json_encode(false);
	}else{
		echo json_encode(true);
	}
}else{
	echo json_encode(true);
}
?>