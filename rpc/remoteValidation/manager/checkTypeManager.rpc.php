<?php 
/*
File: checkTypeManager.php
Author: Santiago Benítez
Creation Date: 08/02/2010 19:45
Last Modified:
Modified By:
Description: Verifies if a user has managers assosiated 
			 in the system.
*/
$serial_mbc=$_POST['serial_mbc'];
if($_POST['selStatusUpdate']=='INACTIVE'){
	$user=new User($db);
	if($user->hasDealersByManager($serial_mbc)){
		echo json_encode(false);
	}else{
		echo json_encode(true);
	}
}
else{
	echo json_encode(true);
}

?>