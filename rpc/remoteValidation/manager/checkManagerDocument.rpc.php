<?php
/*
File: checkManagerDocument.rpc.php
Author: Santiago Ben�tez
Creation Date: 12/02/2010 14:25
Last Modified: 12/02/2010 14:25
Modified By: Santiago Ben�tez
*/

Request::setString('txtDocument');
if($_POST['serial_man']){
	$serial_man = $_POST['serial_man'];
}
$txtDocument=trim(strtolower(utf8_decode($txtDocument)));
$manager = new Manager($db);
if($manager -> managerDocumentExists($txtDocument,$serial_man)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>