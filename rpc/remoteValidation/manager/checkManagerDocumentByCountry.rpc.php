<?php
/*
File: checkManagerDocumentByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 05/07/2010
Last Modified: 
*/

$serial_cou = $_POST['serial_cou'];
$document = $_POST['document'];
$serial_man = $_POST['serial_man'];

$manager = new Manager($db);
if($serial_cou && $document){
	if(!$manager -> managerDocumentByCountryExists($serial_cou, $document, $serial_man)) {
		echo json_encode(true);
	} else {
		echo json_encode(false);
	}
}
?>