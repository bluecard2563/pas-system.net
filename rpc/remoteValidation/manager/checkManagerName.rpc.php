<?php
/*
File: checkManagerName.rpc.php
Author: Santiago Ben�tez
Creation Date: 12/02/2010 14:25
Last Modified: 12/02/2010 14:25
Modified By: Santiago Ben�tez
*/

Request::setString('txtName');
if($_POST['serial_man']){
	$serial_man = $_POST['serial_man'];
}
$txtName=trim(strtolower(utf8_decode($txtName)));
$manager = new Manager($db);
if($manager -> managerNameExists($txtName,$serial_man)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>