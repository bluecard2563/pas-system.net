<?php 
/*
File: checkCndDescription.rpc.php
Author: Miguel Ponce
Creation Date: 29/12/2009 14:14
Last Modified: 19/08/2010
Modified By: Patricio Astudillo
Description: File which verifies if there is another condition
			 in the system.
*/

Request::setString('txtDescription');
Request::setString('txtDescription_up');
if($txtDescription_up){
	$txtDescription=$txtDescription_up;
}

$serial_lang=Language::getSerialLangByCode($db, $_SESSION['language']);

if($_POST['serial_cbl']){
	$serial_cbl=$_POST['serial_cbl'];
}

if($_POST['serial_lang']){
	$serial_lang=$_POST['serial_lang'];
}

$cbl=new ConditionByLanguage($db);
$txtDescription=trim(strtolower(utf8_decode($txtDescription)));

if($cbl->cblExists($txtDescription,$serial_lang,$serial_cbl)){
    echo json_encode(false);
}else{
    echo json_encode(true);
}
?>