<?php 
/*
File: checkRestriction.rpc.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 14:14
Last Modified:
Modified By:
Description: File which verifies if there is another type of restriction
			 in the system.
*/

Request::setString('txtTypeRestriction');
if($_POST['serial_rst']){
	$serial_ben=$_POST['serial_rst'];
}

$txtTypeRestriction=trim(strtolower(utf8_decode($txtTypeRestriction)));

$restriction=new Restriction($db);
if($restriction->restrictionExists($txtTypeRestriction,$serial_rst)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>