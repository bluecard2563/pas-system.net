<?php
/**
 * File: checkAssignedConditions
 * Author: Patricio Astudillo
 * Creation Date: 30-ago-2012
 * Last Modified: 30-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('serial_pro');
	Request::setInteger('serial_cou');
	Request::setInteger('serial_prc');

	$serial_pxc = ProductByCountry::getPxCSerial($db, $serial_cou, $serial_pro);
	
	if(ParticularConditions::conditionIsAllReadyAssigned($db, $serial_prc, $serial_pxc)){
		echo json_encode(false);
	}else{
		echo json_encode(true);
	}
?>
