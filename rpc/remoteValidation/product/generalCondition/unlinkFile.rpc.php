<?php
/*
File: unlinkFile.rpc
Author: David Bergmann
Creation Date: 01/09/2010
Last Modified:
Modified By:
*/
Request::setString('fileName');

if(unlink(DOCUMENT_ROOT."pdfs/generalConditions/$fileName")) {
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}

?>