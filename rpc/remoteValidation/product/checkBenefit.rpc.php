<?php 
/*
File: checkBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 11:43
Last Modified:
Modified By:
Description: File which verifies if there is another benefit with 
			 the same name in the system.
*/

$serial_lang = $_SESSION['serial_lang'];

Request::setString('txtDescBenefit');
if($_POST['txtBenefit']){
    $txtDescBenefit=$_POST['txtBenefit'];
}
if($_POST['serial_ben']){
    $serial_ben=$_POST['serial_ben'];
}
if($_POST['serial_lang']){
    $serial_lang=$_POST['serial_lang'];
}

$txtDescBenefit=trim(strtolower(utf8_decode($txtDescBenefit)));
$benefit=new BenefitbyLanguage($db);

if($benefit->benefitExists($txtDescBenefit,$serial_lang,$serial_ben)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>