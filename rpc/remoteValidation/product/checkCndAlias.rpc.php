<?php 
/*
File: checkCndAlias.rpc.php
Author: Patricio Astudillo
Creation Date: 19/08/2010 8:56
Last Modified: 19/08/2010
Modified By: Patricio Astudillo
Description: File which verifies if there is another condition alias
			 in the system.
*/

Request::setString('txtAlias');

$serial_lang=Language::getSerialLangByCode($db, $_SESSION['language']);

if($_POST['serial_con']){
	$serial_con=$_POST['serial_con'];
}

if($_POST['serial_lang']){
	$serial_lang=$_POST['serial_lang'];
}

$cbl=new ConditionByLanguage($db);
$txtAlias=trim(strtolower(utf8_decode($txtAlias)));

if($cbl->cblAliasExists($txtAlias,$serial_lang,$serial_con)){
    echo json_encode(false);
}else{
    echo json_encode(true);
}
?>