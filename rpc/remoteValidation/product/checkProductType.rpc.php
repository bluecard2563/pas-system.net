<?php 

$productTypebyLanguage=new PtypebyLanguage($db);

Request::setString('hdnSerial_tpp');
if(isset($hdnSerial_tpp)){
	$productTypebyLanguage->setSerial_tpp($hdnSerial_tpp);
}	

Request::setString('txtName_tpp');
$txtName_tpp=trim(strtolower(utf8_decode($txtName_tpp)));
$productTypebyLanguage->setName_ptl($txtName_tpp);
if($productTypebyLanguage->existProdutTypeName()){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>