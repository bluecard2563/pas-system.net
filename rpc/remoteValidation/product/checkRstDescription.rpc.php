<?php 
/*
File: checkRstDescripcion.rpc.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 14:14
Last Modified: 25/01/2010
Modified By: Patricio Astudillo
Description: File which verifies if there is another type of restriction
			 in the system.
*/

Request::setString('txtDescription');
$serial_lang = $_SESSION['serial_lang'];

if($_POST['serial_lang']){
	$serial_rst=$_POST['serial_lang'];
}
if($_POST['txtRestriction']){
	$txtDescription=$_POST['txtRestriction'];
}
if($_POST['serial_rst']){
	$serial_rst=$_POST['serial_rst'];
}

$txtDescription=trim(strtolower(utf8_decode($txtDescription)));
$rbl=new RestrictionByLanguage($db);

if($rbl->rblExists($txtDescription,$serial_lang,$serial_rst)){
    echo json_encode(false);
}else{
    echo json_encode(true);
}
?>