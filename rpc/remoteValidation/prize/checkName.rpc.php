<?php 
/*
File: checkName.rpc.php
Author: Patricio Astudillo
Creation Date: 28/01/2010
Last Modified: 28/01/2010 09:38
Modified By: Patricio Astudillo
*/

Request::setString('txtName');
Request::setString('type');

if($_POST['serial_pri']){
    $serial_pri = $_POST['serial_pri'];
}

$txtName=trim(strtolower(utf8_decode($txtName)));

$prize = new Prize($db);
if($prize -> prizeExist($txtName,$type,$serial_pri)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>