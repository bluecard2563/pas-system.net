<?php
/**
 * Created by Santi Albuja.
 * Date: 5/11/2019
 * Time: 15:21
 */

Request::setInteger('statementId');
Request::setInteger('cardNumber');
Request::setString('customerDocument');
Request::setString('statementStatus');
Request::setString('systemOrigin');
Request::setString('action');
$action = trim($action);

if ($action === 'customerHasStatement') {
    $customerDocument = trim($customerDocument);
    $cardNumber = trim($cardNumber);
    $systemOrigin = $systemOrigin ? trim($systemOrigin) : null;
    $statement = new Statement();
    $response = $statement->existCustomerStatement($cardNumber, $customerDocument, $systemOrigin);
    echo json_encode($response);
    exit();
}

if ($action === 'changeStatementStatus') {
    $statementId = trim($statementId);
    $statementStatus = trim($statementStatus);
    $statement = new Statement();
    $response = $statement->changeStatementStatus($statementId, $statementStatus);
    //$statement->sendCustomerStatementEmail($response['statement']);
    echo json_encode($response);
    exit();
}

if ($action === 'sendCustomerStatementEmail') {
    $statementId = trim($statementId);
    $statement = new Statement();
    $response = $statement->sendCustomerStatementEmail($statementId);
    echo json_encode($response);
    exit();
}

if ($action === 'getCustomerStatementsByDocument') {
    $customerDocument = trim($customerDocument);
    $statement = new Statement();
    $response = $statement->getCustomerStatementsByDocument($customerDocument);
    echo json_encode($response);
    exit();
}

if ($action === 'getCustomerStatementDataTable') {
    $statements = json_decode($_POST['statements'], true);
    $customerStatements = array();
    $i = 0;

    // Show only the records that have an associated link from the statement file.
    if (is_array($statements)) {
        foreach ($statements as $statement) {
            //Get customer section from full response and rebuild in a new array to template.
            $customerStatements[$i] = $statement['customer'];
            $customerStatements[$i]['system_origin_es'] = getSystemOrigin($customerStatements[$i]['system_origin']);
            $customerStatements[$i]['disabled'] = $customerStatements[$i]['status'] === 'INACTIVE' || $customerStatements[$i]['statement_link'] === '' ? 'disabled' : '';
            // $customerStatements[$i]['hidden'] = $customerStatements[$i]['status'] === 'INACTIVE' || $customerStatements[$i]['statement_link'] === '' ? 'hidden' : '';
            $i++;
        }
    }

    $smarty->register('customerStatements');
    $smarty->assign('container','none');
    $smarty->display('helpers/statements/loadCustomerStatementsByDocument.'.$_SESSION['language'].'.tpl');


}

function getSystemOrigin($systemOrigin)
{
    switch ($systemOrigin) {
        case 'BAS_NEW_SALE':
            $origin = 'NUEVA VENTA';
            break;
        case 'BAS_TRAVEL_RECORD':
            $origin = 'REGISTRO DE VIAJES';
            break;
    }
    return $origin;
}

