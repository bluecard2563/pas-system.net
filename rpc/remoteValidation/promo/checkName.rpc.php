<?php 
/*
* File: checkName.php
* Author: Patricio Astudillo
* Creation Date: 02/01/2010 15:15
* Last Modified: 02/01/2010
* Modified By: Patricio Astudillo
* Description: File which verifies if there is another promo name in the same period of time
*/

Request::setString('txtName');
Request::setString('beginDate');
Request::setString('endDate');

if($_POST['serial_prm']){
	$serial_prm=$_POST['serial_prm'];
}

$txtName=trim(strtolower(utf8_decode($txtName)));

$promo=new Promo($db);
if($promo->checkNameBetweenDates($txtName,$beginDate,$endDate,$serial_prm)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>