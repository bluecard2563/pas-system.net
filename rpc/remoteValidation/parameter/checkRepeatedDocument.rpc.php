<?php
/*
File: checkRepeatedDocument.rpc.php
Author: Patricio Astudillo
Creation Date: 22/03/2010
Last Modified: 22/03/2010  12:22
Modified By: Patricio Astudillo
*/

Request::setInteger('serial_cou');
Request::setString('type_dbc');
Request::setString('purpose_dbc');

if($_POST['serial_dbc']){
	$serial_dbc=$_POST['serial_dbc'];
}

$dbc = new DocumentByCountry($db,NULL,$serial_cou,NULL,$type_dbc, $purpose_dbc);
if($dbc->similarDocTypeExists($serial_dbc)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>
