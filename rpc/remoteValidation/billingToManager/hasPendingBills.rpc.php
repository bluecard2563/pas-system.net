<?php
/*
File: hasPendingBills.rpc
Author: David Bergmann
Creation Date: 11/10/2010
Last Modified:
Modified By:
*/

Request::setString('serial_man');

if(BillingToManager::hasPendingBills($db, $serial_man)){
	echo json_encode(false);
} else {
	echo json_encode(true);
}

?>