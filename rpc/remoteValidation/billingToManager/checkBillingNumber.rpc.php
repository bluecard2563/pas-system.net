<?php
/*
File: checkBillingNumber.rpc
Author: David Bergmann
Creation Date: 07/10/2010
Last Modified:
Modified By:
*/

Request::setString('billing_number');

if(BillingToManager::billingNumberExists($db, $billing_number)){
	echo json_encode(false);
} else {
	echo json_encode(true);
}

?>