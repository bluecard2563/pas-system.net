<?php
/*
File: hasPendingBillsByCountry.rpc
Author: Nicolás Flores
Creation Date: 14/06/2011
Last Modified:
Modified By:
*/

Request::setString('selCountry');

if(BillingToManager::hasPendingBillsByCountry($db, $selCountry)){
	echo json_encode(false);
} else {
	echo json_encode(true);
}

?>