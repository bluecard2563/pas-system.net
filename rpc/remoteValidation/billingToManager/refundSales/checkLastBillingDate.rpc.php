<?php
/*
File: checkLastBillingDate.rpc
Author: Nicolás Flores
Creation Date: 04/07/2011
Last Modified:
Modified By:
*/

Request::setString('txtBeginDate');
Request::setInteger('serial_man');

if(BillingToManager::checkLastBillingDate($db, $serial_man,$txtBeginDate)){
	echo json_encode(false);
} else {
	echo json_encode(true);
}

?>