<?php 
/*
File: checkProfileName.php
Author: Esteban Angulo
Creation Date: 31/12/2009 12:33
Last Modified:
Modified By:
Description: File which verifies if there is another freelance with 
			 the same document number in the system.
*/

Request::setString('txtProfileName');
Request::setString('serial_cou');

if($_POST['serial_pbc']){
	$serial_pbc=$_POST['serial_pbc'];
}

$txtProfileName=trim(strtolower(utf8_decode($txtProfileName)));

$profile=new ProfileByCountry($db);
if(!$profile->existsprofile_by_country($txtProfileName,$serial_cou,$serial_pbc)){
	echo json_encode(true);
}else{
	echo json_encode(false);
}
?>