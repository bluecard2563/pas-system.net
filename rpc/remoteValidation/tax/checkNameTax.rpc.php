<?php 
/*
File: checkNameTax.rpc.php
Author: David Bergmann
Creation Date: 11/01/2010
Last Modified:
Modified By: David Bergmann
*/

Request::setString('txtNameTax');
Request::setInteger('serial_cou');
if($_POST['serial_tax']){
	$serial_tax = $_POST['serial_tax'];
}
$txtNameTax=trim(strtolower(utf8_decode($txtNameTax)));
$tax = new Tax($db);
if($tax -> taxExists($txtNameTax,$serial_tax,$serial_cou)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>