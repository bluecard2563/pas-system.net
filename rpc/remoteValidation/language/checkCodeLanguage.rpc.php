<?php 
/*
File: checkCodeLanguage.rpc.php
Author: Santiago Benítez
Creation Date: 20/01/2010 12:25 
Last Modified: 20/01/2010 12:25
Modified By: Santiago Benítez
*/

Request::setString('txtCodeLanguage');
if($_POST['serial_lang']){
	$serial_lang = $_POST['serial_lang'];
}
$txtCodeLanguage=trim(strtolower(utf8_decode($txtCodeLanguage)));
$language = new Language($db);
if($language -> codeExists($txtCodeLanguage,$serial_lang)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>