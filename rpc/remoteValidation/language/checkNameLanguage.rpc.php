<?php 
/*
File: checkNameLanguage.rpc.php
Author: Santiago Benítez
Creation Date: 20/01/2010 12:25 
Last Modified: 20/01/2010 12:25
Modified By: Santiago Benítez
*/

Request::setString('txtNameLanguage');
if($_POST['serial_lang']){
	$serial_lang = $_POST['serial_lang'];
}
$txtNameLanguage=trim(strtolower(utf8_decode($txtNameLanguage)));
$language = new Language($db);
if($language -> languageExists($txtNameLanguage,$serial_lang)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>