<?php
/*
File: checkServiceNameAvailability.rpc.php
Author: Mario L�pez
Creation Date: 19/02/2010
Last Modified:
Modified By:
Description: Checks if a service provider name already exists in the country
*/

Request::setString('txtServProvName');
Request::setString('hdnServProvName');

if($hdnServProvName == $txtServProvName){
    echo json_encode(true);
}else{
	$serviceProvider = new ServiceProvider($db);
    echo json_encode($serviceProvider->checkServiceNameAvailability($txtServProvName));
}
?>