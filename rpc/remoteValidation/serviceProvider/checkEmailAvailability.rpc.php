<?php
/*
File: checkEmailAvailability.rpc.php
Author: Mario L�pez
Creation Date: 19/02/2010
Last Modified:
Modified By:
Description: Checks if a service provider email already exists
*/

Request::setString('txtServProvEmail');
Request::setString('hdnServProvEmail');

if($hdnServProvEmail == $txtServProvEmail){
    echo json_encode(true);
}elseif($txtServProvEmail){
    $serviceProvider = new ServiceProvider($db);
    echo json_encode($serviceProvider->checkEmailAvailability($txtServProvEmail));
}else{
    echo json_encode(false);
}
?>
