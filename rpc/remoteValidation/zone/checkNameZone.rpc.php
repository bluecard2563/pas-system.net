<?php 
/*
File: checkNameZone.rpc.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 14:50
Modified By: David Bergmann
*/

Request::setString('txtNameZone');
if($_POST['serial_zon']){
	$serial_zon = $_POST['serial_zon'];
}

$txtNameZone=trim(strtolower(utf8_decode($txtNameZone)));

$zone = new Zone($db);
if($zone -> zoneExists($txtNameZone,$serial_zon)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>