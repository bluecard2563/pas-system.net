<?php
/*
 * File: checkName
 * Author: Patricio Astudillo
 * Creation Date: Feb 14, 2011, 2:54:56 PM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setString('txtName');
Request::setString('begin_date');
Request::setString('end_date');
Request::setString('serial_ccp');

if($txtName && $begin_date && $end_date){
	if(!CustomerPromo::nameExist($db, $txtName, $begin_date, $end_date, $serial_ccp)){
		echo json_encode(true);
	}else{
		echo json_encode(false);
	}
}else{
	echo json_encode(true);
}
?>