<?php
/*
 * File: checkCode
 * Author: Patricio Astudillo
 * Creation Date: Feb 14, 2011, 2:54:56 PM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setString('txtCode');
Request::setString('serial_ccp');

if(!CustomerPromo::codeExist($db, $txtCode, $serial_ccp)){
	echo json_encode(true);
}else{
	echo json_encode(false);
}
?>