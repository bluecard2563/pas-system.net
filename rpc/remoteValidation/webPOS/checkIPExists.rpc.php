<?php
/**
 * File: checkIPExists
 * Author: Patricio Astudillo
 * Creation Date: 04-oct-2012
 * Last Modified: 04-oct-2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('txtIP');
	Request::setInteger('serial_dea');
	
	$txtIP = mysql_escape_string(strtolower($txtIP));
	
	if(WebPOS::ipExist($db, $serial_dea, $txtIP)) {
		echo json_encode(false);
	} else {
		echo json_encode(true);
	}
	
?>
