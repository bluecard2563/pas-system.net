<?php
/**
 * File: checkUsernameExists
 * Author: Patricio Astudillo
 * Creation Date: 04-oct-2012
 * Last Modified: 04-oct-2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('txtUsername');
	Request::setInteger('serial_dea');
	
	$txtUsername = mysql_escape_string(strtolower($txtUsername));
	
	if(WebPOS::usernameExist($db, $serial_dea, $txtUsername)) {
		echo json_encode(false);
	} else {
		echo json_encode(true);
	}
?>
