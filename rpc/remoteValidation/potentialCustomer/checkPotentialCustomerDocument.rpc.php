<?php 
/*
File: checkPotentialCustomerDocument.rpc
Author: Esteban Angulo
Creation Date: 04/07/2010
Description: Verifies if there is another potential customer with
			 the same document in the system.
*/

Request::setString('txtDocument');
if($_POST['serialPcs']){
	$serial_pcs=$_POST['serialPcs'];
}

$txtDocument=trim(strtolower(utf8_decode($txtDocument)));

$potentialCustomer=new PotentialCustomers($db);
if($potentialCustomer->existsPotentialCustomer($txtDocument,$serial_pcs)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>