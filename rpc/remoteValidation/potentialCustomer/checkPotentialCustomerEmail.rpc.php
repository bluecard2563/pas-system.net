<?php 
/*
File: checkCustomerEmail.php
Author: David Bergmann
Creation Date: 12/01/2010
Last Modified:
Modified By:
Description: File which verifies if there is another customer with 
			 the same email address in the system.
*/

//Debug::print_r($_POST);
Request::setString('txtMail');
if($_POST['serialPcs']){
	$serial_pcs=$_POST['serialPcs'];
}

$txtMail=trim(strtolower(utf8_decode($txtMail)));
$potentialCustomer=new PotentialCustomers($db);
if($potentialCustomer->existsPotentialCustomerEmail($txtMail,$serial_pcs)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>