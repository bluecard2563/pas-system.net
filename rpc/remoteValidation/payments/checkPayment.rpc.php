<?php 
/*
File: checkPayment.rpc.php
Author: Edwin Salvador
Creation Date: 19/03/2010
Last Modified:
Modified By: 
*/

Request::setString('txtInvoice');
echo $paymentData;
$txtInvoice = trim(strtolower($txtInvoice));

if($txtInvoice){
	$invoice = new Invoice($db);

	$serial_pay = $invoice->getSerialPayByNumber($txtInvoice);

	//$sale = new Sales($db);
	$sales_status = $invoice->getSalesStatusByInvoice($serial_pay);
	//Debug::print_r($sales_status);
	if(in_array('REFUNDED', $sales_status)){
		$serial_pay = NULL;
	}
	if($serial_pay){
		echo json_encode(true);
	}else{
		echo json_encode(false);
	}
}
?>