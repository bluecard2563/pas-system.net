<?php
/*
File: checkOldPassword.php
Author: Esteban Angulo
Creation Date: 17/02/2010
Last Modified:
Modified By:
Description: File that verifies if the password inserted as old password is correct
*/

Request::setString('txtOldPassword');

$user=new User($db);
$user->setSerial_usr($_SESSION['serial_usr']);
$user->getData();
$oldPass= $user->getPassword_usr();
//echo $oldPass;
//echo '      -       ';

$txtOldPassword=utf8_decode($txtOldPassword);
//echo md5($txtOldPassword);
if(md5($txtOldPassword)==$oldPass){
	echo json_encode(true);
}else{
	echo json_encode(false);
}
?>