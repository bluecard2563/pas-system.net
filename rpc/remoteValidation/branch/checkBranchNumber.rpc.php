<?php 
/*
File: checkBranchNumber.rpc.php
Author: David Bergmann
Creation Date: 14/02/2010
Last Modified:
Modified By:
*/

$dealer=new Dealer($db);
Request::setInteger('txtBranchNumber');
Request::setInteger('serial_dea');
Request::setInteger('dea_serial_dea');

$txtBranchNumber=trim(strtolower(utf8_decode($txtBranchNumber)));

if($dealer->existBranchNumber($txtBranchNumber,$dea_serial_dea,$serial_dea)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>