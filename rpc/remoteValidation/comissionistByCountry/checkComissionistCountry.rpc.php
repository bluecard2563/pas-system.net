<?php 
/*
File: checkComissionistCountry.rpc.php
Author: David Bergmann
Creation Date: 05/02/2010
Last Modified:
Modified By:
*/

Request::setInteger('selUser');
Request::setInteger('selCountryAssign');

$commissionistByCountry = new ComissionistByCountry($db);
if($commissionistByCountry -> comissionistExists($selUser,$selCountryAssign)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>