<?php 
/*
File: checkFreelanceEmail.php
Author: Esteban Angulo
Creation Date: 4/01/2010 16:20
Last Modified:
Modified By:
Description: File which verifies if there is another freelance with 
			 the same email address in the system.
*/
Request::setString('txtMail');
if($_POST['serial_fre']){
	$serial_fre=$_POST['serial_fre'];
	//echo $serial_fre;
}

$txtMail=trim(strtolower(utf8_decode($txtMail)));
$freelance=new Freelance($db);
if($freelance->existsFreelanceEmail($txtMail,$serial_fre)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>