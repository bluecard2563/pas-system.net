<?php
/**
 * File: checkValidDocument
 * Author: Patricio Astudillo
 * Creation Date: 04/12/2013
 * Last Modified: 04/12/2013
 * Modified By: Patricio Astudillo
 */

Request::setString('txtID');
Request::setString('country');

if($country == 62){
	if(GlobalFunctions::validate_ruc($txtID, TRUE)) {
		echo json_encode(true);
	} else {
		echo json_encode(false);
	}
}else{
	echo json_encode(true);
}
?>
