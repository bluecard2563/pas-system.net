<?php 
/*
File: checkDealerType.rpc.php
Author: Patricio Astudillo	
Creation Date: 06/01/2010
Last Modified: 06/01/2010
Modified By: Patricio Astudillo
*/

$dealerT=new DealerType($db);
Request::setString('txtAlias');
if($_POST['serial_dlt']){
	$serial_dlt=$_POST['serial_dlt'];
}

$txtAlias=trim(strtolower(utf8_decode($txtAlias)));

if($dealerT->existDealerType($txtAlias,$serial_dlt)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>