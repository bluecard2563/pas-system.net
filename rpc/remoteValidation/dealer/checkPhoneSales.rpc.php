<?php
/*
File: checkPhoneSales.rpc
Author: David Bergmann
Creation Date: 14/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');

if(Country::hasPhoneSales($db, $serial_cou)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>
