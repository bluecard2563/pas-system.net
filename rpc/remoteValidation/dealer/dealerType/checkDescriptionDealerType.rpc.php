<?php 
/*
File: checkDescriptionDealerType.rpc.php
Author: David Bergmann
Creation Date: 27/01/2010
Last Modified:
Modified By:
*/

$dealerT=new DealerType($db);

Request::setString('txtDesc');
Request::setString('serial_lang');
Request::setString('serial_delaerTyByLang');

$txtDesc=trim(strtolower(utf8_decode($txtDesc)));

if($dealerT->existDealerTypeByLanguage($txtDesc,$serial_lang,$serial_delaerTyByLang)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>