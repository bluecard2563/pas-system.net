<?php 
/*
File: checkDealerCode.rpc.php
Author: Patricio Astudillo	
Creation Date: 12/01/2010 09:47
Last Modified: 12/01/2010
Modified By: Patricio Astudillo
*/

$dealer=new Dealer($db);
Request::setString('txtCode');
if($_POST['serial_dea']){
	$serial_dea=$_POST['serial_dea'];
}

$txtCode=trim(strtolower(utf8_decode($txtCode)));

if($dealer->existDealerCode($txtCode,$serial_dea)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>