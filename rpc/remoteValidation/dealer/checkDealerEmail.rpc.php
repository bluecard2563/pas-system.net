<?php 
/*
File: checkDealerEmail.rpc.php
Author: Patricio Astudillo	
Creation Date: 12/01/2010 10:35
Last Modified: 12/01/2010
Modified By: Patricio Astudillo
*/

$dealer=new Dealer($db);
if($_POST['txtMail1']){
	$txtMail=$_POST['txtMail1'];
}
if($_POST['txtMail2']){
	$txtMail=$_POST['txtMail2'];
}

if($_POST['serial_dea']){
	$serial_dea=$_POST['serial_dea'];
}

if($_POST['serial_father']){
	$serial_father=$_POST['serial_father'];
}

$txtMail=trim(strtolower(utf8_decode($txtMail)));

if($dealer->existDealerEmail($txtMail,$serial_dea,$serial_father)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>