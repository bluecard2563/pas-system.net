<?php
/*
 * File: checkTranslationName.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 14/07/2010, 10:52:19 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 14/07/2010, 10:52:19 AM
 */

Request::setString('serial_lang');
Request::setString('txtSblName');

$txtSblName = trim(strtolower(utf8_decode($txtSblName)));
$service=new ServicesbyLanguage($db);

if($service->serviceNameExists($txtSblName,$serial_lang)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>
