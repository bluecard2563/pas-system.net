<?php 
/*
File: checkService.php
Author: Edwin Salvador
Creation Date: 11/02/2010
Last Modified:04/03/2010
Modified By:Nicolas Flores
Description: File which verifies if there is another service with
			 the same name or the same code in the system.
*/

$serial_lang = $_SESSION['serial_lang'];

Request::setString('name_sbl');
Request::setString('txtSblName');

if($txtSblName){
    $name_sbl = $_POST['txtSblName'];
}
if($_POST['serial_ser']){
    $serial_ser = $_POST['serial_ser'];
}
if($_POST['serial_lang']){
    $serial_lang = $_POST['serial_lang'];
}
if($name_sbl != NULL){
    $name_sbl = trim(strtolower(utf8_decode($name_sbl)));
    $service=new ServicesbyLanguage($db);

    if($service->serviceNameExists($name_sbl,$serial_lang,$serial_ser)){
        echo json_encode(false);
    }else{
        echo json_encode(true);
    }
}
?>