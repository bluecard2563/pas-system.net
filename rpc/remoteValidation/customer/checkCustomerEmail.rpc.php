<?php 
/*
File: checkCustomerEmail.php
Author: David Bergmann
Creation Date: 12/01/2010
Last Modified:
Modified By:
Description: File which verifies if there is another customer with 
			 the same email address in the system.
*/

//Debug::print_r($_POST);
Request::setString('txtMail');
if($_POST['serialCus']){
	$serial_cus=$_POST['serialCus'];
}

$txtMail=trim(strtolower(utf8_decode($txtMail)));
$customer=new Customer($db);
if($customer->existsCustomerEmail($txtMail,$serial_cus)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>