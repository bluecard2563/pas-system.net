<?php 
/*
File: checkCustomerDocument.php
Author: Santiago Borja
Description: File which verifies teh user type
*/

Request::setString('txtDocument');

$txtDocument = trim(strtolower(utf8_decode($txtDocument)));

$customer = new Customer($db);
$customer-> setDocument_cus($txtDocument);
$customer -> getData();

if($customer -> existsCustomer($txtDocument)){
	echo $customer -> getType_cus();
}else{
	echo "NOCUSTOMER";
}
?>