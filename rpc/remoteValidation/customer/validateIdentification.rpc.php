<?php
/**
 * Created by Santi Albuja.
 * Date: 7/11/2019
 * Time: 13:58
 */

Request::setString('documentType');
Request::setString('customerDocument');
Request::setString('action');

$documentType = trim($documentType);
$customerDocument = trim($customerDocument);
$action = trim($action);
$validateIdentification = new ValidarIdentificacion();
$response = array();

if ($action === "validateCustomerCardId") {
    if (!empty($customerDocument)) {
        $valid = $validateIdentification->validarCedula($customerDocument);
        if (!$valid) {
            $response["success"] = false;
            $response["message"] = "Cédula de identidad incorrecta - " . $validateIdentification->getError();
            $response["error"] = $validateIdentification->getError();
        }
        else {
            $response["success"] = true;
            $response["message"] = "Cédula de identidad correcta";
            $response["error"] = "";
        }
    }
    echo json_encode($response);
    exit();
}

if ($action === 'validateCustomerRuc') {
    if (!empty($customerDocument)) {
        $rucType = $validateIdentification->obtenerTipoRuc($customerDocument);
        $valid = $validateIdentification->validarRuc($customerDocument);
        if (!$valid) {
            $response["type"] = $rucType;
            $response["success"] = false;
            $response["message"] = "Número de RUC incorrecto - " . $validateIdentification->getError();
            $response["error"] = $validateIdentification->getError();
        }
        else {
            $response["type"] = $rucType;
            $response["success"] = true;
            $response["message"] = "Número de RUC correcto";
            $response["error"] = "";
        }

    }
    echo json_encode($response);
    exit();
}

if ($action === 'validateCustomerPassport') {
    $response["success"] = true;
    $response["message"] = "Número de Pasaporte correcto";
    $response["error"] = "";
    echo json_encode($response);
    exit();
}




