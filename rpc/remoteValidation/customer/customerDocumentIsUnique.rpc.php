<?php 
/*
File: customerDocumentIsUnique.rpc.php
Author: Santiago Borja
Creation Date: 30/12/2009 16:01
Last Modified:
Modified By:
Description: File which verifies if there is another customer with 
			 the same document number in the system.
*/

Request::setString('txtDocument');
if($_POST['serialCus']){
	$serial_cus=$_POST['serialCus'];
}

$txtDocument=trim(strtolower(utf8_decode($txtDocument)));

$customer=new Customer($db);

//HERE DIFFERS WITH checkCustomerDocument, we need the OPPOSITE VALUE
if(!$customer->existsCustomer($txtDocument,$serial_cus)){
	echo json_encode(true);
}else{
	echo json_encode(false);
}
?>