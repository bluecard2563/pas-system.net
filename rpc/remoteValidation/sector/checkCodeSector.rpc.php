<?php 
/*
File: checkNameSector.rpc.php
Author: David Bergmann
Creation Date: 07/01/2010
Last Modified:
Modified By:
*/

Request::setString('txtCodeSector');
if($_POST['serial_sec']){
	$serial_sec = $_POST['serial_sec'];
}
if($_POST['serial_cit']){
	$serial_cit = $_POST['serial_cit'];
}

$txtCodeSector=trim(strtolower(utf8_decode($txtCodeSector)));

$sector = new Sector($db);
if($sector -> sectorCodeExists($txtCodeSector,$serial_cit,$serial_sec)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>