<?php 
/*
File: checkNameSector.rpc.php
Author: David Bergmann
Creation Date: 07/01/2010
Last Modified:
Modified By:
*/

Request::setString('txtNameSector');

if($_POST['serial_sec']){
	$serial_sec = $_POST['serial_sec'];
}
if($_POST['serial_cit']){
	$serial_cit = $_POST['serial_cit'];
}

$txtNameSector=trim(strtolower(utf8_decode($txtNameSector)));

$sector = new Sector($db);
if($sector -> sectorExists($txtNameSector,$serial_cit,$serial_sec)) {
	echo json_encode(false);
} else {
	echo json_encode(true);
}
?>