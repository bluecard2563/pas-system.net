<?php 
/*
File: checkCounterExists.rpc.php
Author: Patricio Astudillo	
Creation Date: 14/01/2010 10:30
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

$counter=new Counter($db);
Request::setString('serial_usr');

if($_POST['serial_cnt']){
	$serial_cnt=$_POST['serial_cnt'];
}

if($counter->counterExists($serial_usr,$serial_cnt)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>