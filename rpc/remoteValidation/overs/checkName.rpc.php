<?php 
/*
* File: checkName.php
* Author: Santiago Borja
* Creation Date: 17/03/2010
* Description: File which verifies if there is another over name in the same period of time
*/

Request::setString('txtName');
Request::setString('beginDate');
Request::setString('endDate');

if($_POST['serial_ove']){
	$serial_ove=$_POST['serial_ove'];
}

$txtName=trim(strtolower(utf8_decode($txtName)));

$over=new Over($db);
if($over->overNameExists($txtName,$serial_ove)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>