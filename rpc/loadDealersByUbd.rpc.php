<?php
/*
File: loadDealersByUbd.rpc.php
Author: Luis Salvador
Creation Date: 06/02/2013
*/

Request::setString('category');
Request::setString('serial_mbc');
Request::setString('serialComissionist');
Request::setString('serial_type');

if($_SESSION['serial_mbc'] != 1) {
	$dealerList = Dealer::getDealersByComissionist($db, $serial_mbc, $serialComissionist, $serial_type);
}else{
	$dealerList = Dealer::getDealersByComissionist($db, $serial_mbc, $serialComissionist, $serial_type);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadDealersByManager.'.$_SESSION['language'].'.tpl');
?>