<?php 
/*
File: checkProfileName.php
Author: Esteban Angulo
Creation Date: 31/12/2009 12:33
Last Modified:
Modified By:
Description: File which verifies if there is another freelance with 
			 the same document number in the system.
*/
Request::setString('txtProfileName');
if($_POST['serial_prf']){
	$serial_prf=$_POST['serial_prf'];
}

$txtProfileName=trim(strtolower(utf8_decode($txtProfileName)));

$profile=new Profile($db);
if($profile->existsProfile($txtProfileName,$serial_prf)){
	echo json_encode(false);
}else{
	echo json_encode(true);
}
?>