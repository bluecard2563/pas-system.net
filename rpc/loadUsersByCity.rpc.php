<?php
/*
File: loadUsersByCity
Author: Gabriela Guerrero
Creation Date: 15/02/2010
Last Modified:
Modified By:
Description: File which loads users from a specific city and manager
*/
	Request::setString('serial_cit');
	Request::setString('count');
	Request::setString('planet');
	if(!$planet){
		$planet='NO';
	}
	
	$user = new User($db);
	$usersList = $user -> getUsersByCity($serial_cit, $planet);
	
	$smarty->register('usersList,count');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadUsersByCity.'.$_SESSION['language'].'.tpl');
?>