<?php
/*
 * File: loadCustomersInfo.php
 * Author: Patricio Astudillo
 * Creation Date: 03/06/2010, 10:44:19 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 03/06/2010, 10:44:19 AM
 */

Request::setString('pattern');

$customer=new Customer($db);
$customerList=$customer->getCustomers($pattern);

$smarty->register('customerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadCustomers/loadCustomersInfo.'.$_SESSION['language'].'.tpl');
?>
