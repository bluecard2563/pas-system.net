<?php 
/*
File: loadProductBenefitsDialogData.rpc.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('selProduct');
	Request::setString('useDatabase');
	
	if($useDatabase == 'true'){
		$preOrder = new PreOrder($db,NULL,$_SESSION['serial_customer_estimator'],'CUSTOMER',time());
		$allProducts = $preOrder -> getAllMyCartProductItems(Language::getSerialLangByCode($db, $_SESSION['language']));
		$pro = $allProducts[$selProduct];
		
		$prodByCou = new ProductByCountry($db, $pro['serial_pxc']);
		$prodByCou -> getData();
		$proId = $prodByCou -> getSerial_pro();
	}else{
		$allProducts = array();
		if(isset($_SESSION['system_estimator_shopping_cart_products'])){
			$cartProductsInformation = $_SESSION['system_estimator_shopping_cart_products'];
			foreach ($cartProductsInformation as $sessProd){
				$allProducts[] = $sessProd;
			}
		}
		if(isset($_SESSION['web_estimator_session_products'])){
			$sessionProductsInformation = $_SESSION['web_estimator_session_products'];
			foreach ($sessionProductsInformation as $sessProd){
				$allProducts[] = $sessProd;
			}
		}
		$pro = $allProducts[$selProduct];
		
		$prodByCou = new ProductByCountry($db, $pro['serial_pxc']);
		$prodByCou -> getData();
		$proId = $prodByCou -> getSerial_pro();
	}
	
	//LOAD ALL THIS PRODUCT BENEFITS:
	$benefitsByProduct = new BenefitsByProduct($db); 
	$benefitsByProduct -> setSerial_pro($proId);
	$allBenefits = $benefitsByProduct -> getMyBenefits(Language::getSerialLangByCode($db, $_SESSION['language']));
	$pro['allBenefits'] = $allBenefits;
	
	$productTemp = new Product($db, $proId);
	$productTemp -> getData();
	
	$adultsIncluded = $productTemp -> getAdults_pro();
	$childrenIncluded = $productTemp -> getChildren_pro();
	$maxExtras = $productTemp -> getMax_extras_pro()==0?'N/A':$productTemp -> getMax_extras_pro();
	
	$smarty->register('pro,adultsIncluded,childrenIncluded,maxExtras');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadProductBenefitsDialogData.'.$_SESSION['language'].'.tpl');
?>