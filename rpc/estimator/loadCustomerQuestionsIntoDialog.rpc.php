<?php
/*
File: loadCustomerQuestionsIntoDialog.rpc.php
Author: Santiago Borja
Creation Date: 22/03/2010
*/

	Request::setString('customerLocation');
	Request::setString('customerDocument');
	
	$sessionLanguage = Language::getSerialLangByCode($db, $_SESSION['language']);
	
	$customerH = new Customer($db);
	$customerSerial = NULL;
	
	if($customerH -> existsCustomer($customerDocument, NULL)){
		$customerH -> setDocument_cus($customerDocument);
		$customerH -> getData(false);
		$customerSerial = $customerH -> getSerial_cus();
	}
	
	$questionList = Question::getActiveQuestions($db, $sessionLanguage, $customerSerial);
	
	$smarty->register('customerLocation,questionList');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadCustomerQuestionsIntoDialog.'.$_SESSION['language'].'.tpl');
?>