<?php 
/*
File: loadEstimatorTripDays.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/

	Request::setString('serial_pxc');
	Request::setString('serial_cou');
	Request::setString('selectedVal');
	Request::setString('tripDaysCalc');
	Request::setString('isSmall');
	
	//THIS EXPRESSION IS REDUNDANT BECAUSE IT VALIDATES NOT ONLY NUMBERS, BUT TEXT EITHER.
	if(!($tripDaysCalc > 0)){
		echo 'Fechas de viaje Incorrectas';return;
	}
	
	if($serial_pxc != "-1"){
		$products = array(array('serial_pxc' => $serial_pxc));
	}else{
		if($serial_cou){
		    $product = new Product($db);
		    $products = $product -> getEstimatorProductsListByCountry($serial_cou, $_SESSION['language']);
		}
	}
	
	$prices = array();
	foreach($products as $produ){
		$productByCountry = new ProductByCountry($db, $produ['serial_pxc']);
		$productByCountry -> getData();
		
		$product = new Product($db, $productByCountry -> getSerial_pro());
		$product -> getData();
		
		$priceObject = new PriceByProductByCountry($db);
	    
	    //WE DON'T CHECK THE COUNTRY PASSED TO THE FOLLOWING SENTENCE BECAUSE THE PXC ALREADY HAS THE CORRECT COUNTRY:
		$partialPrices = $priceObject->getPricesByProductByCountryHigherThanDays($productByCountry -> getSerial_pro(), $productByCountry -> getSerial_cou(), $tripDaysCalc);
		
		if($partialPrices && $product -> getFlights_pro()=='0'){
			$prices = GlobalFunctions::mergeArraysNoRepeat($prices, $partialPrices);
		}
	}
	
	if(is_array($prices)){
		$newTotal = sort($prices);
	}
	
	$smarty->register('prices,selectedVal,isSmall');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadEstimatorTripDays.'.$_SESSION['language'].'.tpl');
?>