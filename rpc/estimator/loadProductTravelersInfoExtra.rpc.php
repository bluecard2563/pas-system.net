<?php 
/*
File: loadProductTravelersInfo.rpc.php
Author: Santiago Borja
Creation Date: 11/04/2010
*/
	Request::setString('extraIndex');
	Request::setString('prodIndex');
	Request::setString('userDocument');
	
	$countries = Country::getAllCountries($db,false);
	
	$customer = new Customer($db,NULL,NULL,NULL,$userDocument);
	$customer -> getData();
	
	$city = new City($db,$customer -> getSerial_cit());
	$city -> getData();
	$customerCountry = $city -> getSerial_cou();
	$cityList = $city->getCitiesByCountry($customerCountry);
	
	if($customer->getStatus_cus() != 'INACTIVE'){
		$extra = array(
				'idExtra' => $userDocument,
				'serialCus' => $customer -> getSerial_cus(),
				'selRelationship' => '',
				'nameExtra' => $customer -> getFirstname_cus(),
				'lastnameExtra' => $customer -> getLastname_cus(),
				'birthdayExtra' => $customer -> getBirthdate_cus(),
				'emailExtra' => $customer -> getEmail_cus(),
			);
	}
	
	//Relationship Type List:
	$extrasP= new Extras($db);
	$relationshipTypeList = Extras::getAllRelationships($db);
	$sessionProductTravelersData = $_SESSION['cart_products_travelers_information'];
	$productData =  $sessionProductTravelersData[$prodIndex];
	$prodByCou = new ProductByCountry($db,$productData['serial_pxc']);
	$prodByCou -> getData();
	$productX = new Product($db,$prodByCou -> getSerial_pro());
	$productX -> getData();
	if($productX -> getSpouse_pro() != 'YES'){
		foreach ($relationshipTypeList as $key=>$type){
			if($type == 'SPOUSE'){
				unset($relationshipTypeList[$key]);
			}
		}
	}
	
	$cid = $extraIndex;
	$smarty->register('countries,cityList,extra,cid,relationshipTypeList');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadProductTravelersInfoExtra.'.$_SESSION['language'].'.tpl');
?>