<?php 
/*
File: loadCreditCardForm.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/

	$productVal = $_SESSION['web_productVal'];
	$countries = Country::getAllCountries($db);

	$smarty->register('countries');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadCreditCardForm.'.$_SESSION['language'].'.tpl');
?>