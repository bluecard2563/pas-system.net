<?php 
/*
File: loadEstimatorBenefits.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('selProduct');
	Request::setString('selDeparture');
	Request::setString('selDestination');
	Request::setString('checkPartner');
	Request::setString('selTripDays');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('txtOverAge');
	Request::setString('txtUnderAge');
	Request::setString('useDatabase');
	Request::setString('destinationRestricted');
	Request::setString('extrasRestricted');
	
	unset($errorMessage);
	
	$targetDays = GlobalFunctions::getDaysDiffDdMmYyyy($txtBeginDate, $txtEndDate);
	$useDatabase = $useDatabase == 'true'? 1 : 0;
	
	if($selTripDays == 'null'){
		$selTripDays = false;
	}
	
	//BRING ALL THE CURRENT CART PRODUCTS FROM PREVIOUS ESTIMATIONS
	$allProducts = array();
	if(!$useDatabase){
		if(isset($_SESSION['system_estimator_shopping_cart_products'])){
			$currentCartProducts = $_SESSION['system_estimator_shopping_cart_products'];
			foreach($currentCartProducts as $prod){$allProducts[] = $prod;}
		}
	}
	
	//BUILD UP THE PRODUCT(S) LIST WITH ALL BENEFITS, ALL READY TO BE DISPLAYED:
	$allEstimatorProducts = array();
	//THIS VALIDATION IS TO AVOID RE INSERTS WHEN GOING THRU NEXT AND PREVIOUS WITHIN THE ESTIMATOR DUE TO PERSISTENT POST DATA.
	if(!(isset($_SESSION['data_already_added']) && $useDatabase)){
		if($selProduct == "-1"){
			if($selDeparture){
			    $product = new Product($db);
			    $products = $product -> getEstimatorProductsListByCountry($selDeparture, $_SESSION['serial_lang']); //TODO STATIC !!
			}
			$allEstimatorProducts = $products;
		}elseif($selProduct){
			$prodByCou = new ProductByCountry($db, $selProduct);
			$prodByCou -> getData();
			$prodP = new Product($db, $prodByCou -> getSerial_pro());
			$prod = $prodP -> getInfoProduct($_SESSION['language']);
			$prod[0]['serial_pxc'] = $selProduct;
			$prod[0]['serial_pro'] = $prodByCou -> getSerial_pro();
			$allEstimatorProducts = $prod;
		}
	}
	
	$newProducts = array();
	foreach ($allEstimatorProducts as $prod){
		if($destinationRestricted != '0'){
			$selDestination = $selDeparture;
		}
		switch($extrasRestricted){
			case 'ADULTS':
				$txtUnderAge = 0;
				break;
			case 'CHILDREN':
				//LEAVE FORM INTACT
				break;
			default: //BOTH
				break;
		}
		
		$singleProduct = array(
			'serial_pxc' => $prod['serial_pxc'],
			'name_pbl' => $prod['name_pbl'],
			'departure'=> $selDeparture,
			'destination'=> $selDestination,
			'spouseTraveling'=> $checkPartner,
			'beginDate'=> $txtBeginDate,
			'endDate'=> $txtEndDate,
			'days' => $targetDays,
			'adults' => $txtOverAge,
			'children' => $txtUnderAge,
		);
		
		if(GlobalFunctions::assignProductCalculatedPrices($db, $singleProduct, $selTripDays, $extrasRestricted) === 'NO_PRICES'){
			$errorMessage = 'NO_PRICES_ERROR';
		}
		
		if($useDatabase){
			$error = GlobalFunctions::saveCartProductToDB($db, $singleProduct, $_SESSION['serial_customer_estimator']);
			if($error != 5){
				ErrorLog::log($db, 'PRE ORDER SAVE ERROR ' . $error, $singleProduct);
				echo 'Error # '.$error;
			}
		}
		$newProducts[] = $singleProduct;
	}
	
	$showDates = true;
	if($useDatabase){
		$preOrder = new PreOrder($db, NULL, $_SESSION['serial_customer_estimator'], 'CUSTOMER', time());
		$allProducts = $preOrder -> getAllMyCartProductItems(Language::getSerialLangByCode($db, $_SESSION['language']));
		$_SESSION['data_already_added'] = true;
		$referenceBeginDate = $allProducts[0]['begin_date_pod'];
		$referenceEndDate = $allProducts[0]['end_date_pod'];
		foreach($allProducts as &$prodMy){
			$prodByCou = new ProductByCountry($db,$prodMy['serial_pxc']);
			$prodByCou -> getData();
			$product = new Product($db, $prodByCou -> getSerial_pro());
			$product -> getData();
			$prodMy['schengen'] =  $product -> getSchengen_pro();
			
			if($prodMy['begin_date_pod'] != $referenceBeginDate || $prodMy['end_date_pod'] != $referenceEndDate){
				$showDates = false;
			}
		}
		$txtBeginDate = $allProducts[0]['begin_date_pod'];
		$txtEndDate = $allProducts[0]['end_date_pod'];
		$targetDays = $allProducts[0]['days_pod'];
	}else{
		//WE MIX CART & SESSION PRODUCTS ONLY TO BE DISPLAYED BUT BEHIND WE STILL HAVE 2 LISTS!
		foreach ($newProducts as &$newP){
			//WE SET THE FIRST BENFIT TO BE DISPLAYED:
			$tempPXC = new ProductByCountry($db, $newP['serial_pxc']);
			$tempPXC -> getData();
			
			$prodSc = new Product($db, $tempPXC -> getSerial_pro());
			$prodSc -> getData();
			
			$benefitsByProduct = new BenefitsByProduct($db); 
			$benefitsByProduct -> setSerial_pro($prodSc -> getSerial_pro());
			$allBenefits = $benefitsByProduct -> getMyBenefits(Language::getSerialLangByCode($db, $_SESSION['language']));
			$ben = $allBenefits[0];
			
			//new: get the schengen field:
			$newP['schengen'] =  $prodSc -> getSchengen_pro();
			$newP['symbol_cur'] = $ben['symbol_cur'];
			$newP['pro_benefit_name'] = $ben['description_bbl'];
			if($ben['price_bxp'] == '0.00'){
				$newP['pro_benefit_cost'] = $ben['description_cbl'];
			}else{
				$newP['pro_benefit_cost'] = $ben['symbol_cur'].' '.$ben['price_bxp'];
			}
			if($ben['restriction_price_bxp'] > 0){
				$newP['pro_benefit_cost'] .= '('.$ben['symbol_cur'].' '.$ben['restriction_price_bxp'].' '.$ben['description_rbl'].')';
			}
			
			$allProducts[] = $newP;
		}
		$targetDays = $allProducts[0]['days'];
		$referenceBeginDate = $allProducts[0]['beginDate'];
		$referenceEndDate = $allProducts[0]['endDate'];
		foreach($allProducts as $prodMy){
			if($prodMy['beginDate'] != $referenceBeginDate || $prodMy['endDate'] != $referenceEndDate){
				$showDates = false;
			}
		}
		$txtBeginDate = $allProducts[0]['beginDate'];
		$txtEndDate = $allProducts[0]['endDate'];
		//SAVE SESSION ARRAY:
		$_SESSION['web_estimator_session_products'] = $newProducts;
	}
	
	//SUM ALL PRICES:
	$totalEstimatorPrice = 0;
	foreach($allProducts as $prodSingle){
		$totalEstimatorPrice += $prodSingle['total_price_pod'];
	}
	
	$smarty->register('allProducts,targetDays,txtBeginDate,txtEndDate,totalEstimatorPrice,useDatabase,showDates,errorMessage');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadEstimatorBenefits.'.$_SESSION['language'].'.tpl');
?>