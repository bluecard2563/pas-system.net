<?php 
/*
File: loadProductTravelersInfo.rpc.php
Author: Santiago Borja
Creation Date: 11/04/2010
*/
	Request::setString('productIndex');
	Request::setString('cardIndex');
	$countries = Country::getAllCountries($db,false);
	
	$customer = new Customer($db);
	$typesList = $customer->getAllTypes();
	
	$sessionProductTravelersData = $_SESSION['cart_products_travelers_information'];
	$productData =  $sessionProductTravelersData[$productIndex];
	$product = $productData['cards'][$cardIndex];
	
	$city = new City($db,$product['holder']['serial_cit']);
	$city -> getData();
	$product['holder']['serial_cou'] = $city -> getSerial_cou();
	$cityList = $city->getCitiesByCountry($product['holder']['serial_cou']);
	
	$extrasP= new Extras($db);
	$relationshipTypeList = Extras::getAllRelationships($db);
	
	$prodByCou = new ProductByCountry($db,$productData['serial_pxc']);
	$prodByCou -> getData();
	$productX = new Product($db,$prodByCou -> getSerial_pro());
	$productX -> getData();
	
	if($productX -> getSpouse_pro() != 'YES'){
		foreach ($relationshipTypeList as $key=>$type){
			if($type == 'SPOUSE'){
				unset($relationshipTypeList[$key]);
			}
		}
	}
	
	//Only for several_flights = 0
	$validityParam = new Parameter($db,'14');
    $validityParam->getData();
    $validityPeriod = $validityParam -> getValue_par();
	$validUntil = date("Y-m-d",strtotime($productData['begin_date_pod']. ' +'.$validityPeriod.' days'));
	
	$questionList = Question::getActiveQuestions($db, Language::getSerialLangByCode($db, $_SESSION['language']));
	
	$smarty->register('countries,cityList,typesList,product,productData,productIndex,cardIndex,relationshipTypeList,validityPeriod,validUntil,questionList');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadProductTravelersInfo.'.$_SESSION['language'].'.tpl');
?>