<?php
/**
 * File: removeNewHolderInfo
 * Author: Patricio Astudillo
 * Creation Date: 06-may-2013
 * Last Modified: 06-may-2013
 * Modified By: Patricio Astudillo
 */

	if(isset($_SESSION['web_estimator_altern_invoice_holder'])){
		unset($_SESSION['web_estimator_altern_invoice_holder']);
	}
	
	echo json_encode(true);
?>
