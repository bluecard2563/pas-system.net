<?php 
/*
File: loadProductTravelersInfo.rpc.php
Author: Santiago Borja
Creation Date: 11/04/2010
*/
	Request::setString('userDocument');
	
	$countries = Country::getAllCountries($db,false);
	
	$customer = new Customer($db,NULL,NULL,NULL,$userDocument);
	$customer -> getData();
	
	$city = new City($db,$customer -> getSerial_cit());
	$city -> getData();
	
	$customerCountry = $city -> getSerial_cou();
	$cityList = $city->getCitiesByCountry($customerCountry);
	
	if($customer->getStatus_cus() != 'INACTIVE'){
		$holder = array(
			'serial_cus' => $customer -> getSerial_cus(),
			'serial_cit' => $customer -> getSerial_cit(),
			'serial_cou' => $customerCountry,
			'type_cus' => $customer -> getType_cus(),
			'document_cus' => $userDocument,
			'first_name_cus' => $customer -> getFirstname_cus(),
			'last_name_cus' => $customer -> getLastname_cus(),
			'birthdate_cus' => $customer -> getBirthdate_cus(),
			'phone1_cus' => $customer -> getPhone1_cus(),
			'phone2_cus' => $customer -> getPhone2_cus(),
			'cellphone_cus' => $customer -> getCellphone_cus(),
			'email_cus' => $customer -> getEmail_cus(),
			'address_cus' => $customer -> getAddress_cus(),
			'relative_cus' => $customer -> getRelative_cus(),
			'relative_phone_cus' => $customer -> getRelative_phone_cus(),
		);
	}
	
	
	$smarty->register('countries,cityList,holder');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadProductTravelersInfoHolder.'.$_SESSION['language'].'.tpl');
?>