<?php
/**
 * File: logNewHolderInfo
 * Author: Patricio Astudillo
 * Creation Date: 06-may-2013
 * Last Modified: 06-may-2013
 * Modified By: Patricio Astudillo
 */

	Request::setString('customer_id');
	Request::setString('document_number');
	Request::setString('customer_type');
	Request::setString('customer_name');
	Request::setString('customer_lastname');
	Request::setString('customer_address');
	Request::setString('customer_phone1');
	Request::setString('customer_phone2');
	Request::setString('customer_city');
	
	$mix_array = array();
	$tempCux = new Customer($db, $customer_id);
	$process_successfull = true;
	
	if(!$tempCux->getData()){
		$tempCux->setDocument_cus($document_number);
		$tempCux->setFirstname_cus(utf8_decode($customer_name));
		$tempCux->setLastname_cus(utf8_decode($customer_lastname));
		$tempCux->setAddress_cus(utf8_decode($customer_address));
		$tempCux->setPhone1_cus($customer_phone1);
		$tempCux->setPhone2_cus($customer_phone2);
		$tempCux->setSerial_cit($customer_city);
		$tempCux->setType_cus($customer_type);
		
		$serial_cus = $tempCux->insert();
		
		if($serial_cus){
			$tempCux->setserial_cus($serial_cus);
		}else{		
			$process_successfull = false;
		}
	}
	
	
	if($process_successfull){
		$_SESSION['web_estimator_altern_invoice_holder'] = $tempCux->getSerial_cus();
		
		$message_string = 'success%%'.$tempCux->getDocument_cus().'%%'.utf8_encode($tempCux->getFirstname_cus().' '.$tempCux->getLastname_cus());
	}else{
		$message_string = 'failure';
	}
	
	echo $message_string;
?>
