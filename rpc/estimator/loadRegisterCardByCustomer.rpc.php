<?php
/*
File:loadCorpCardByCustomer
Author: Nicolas Flores
Creation Date:29.03.2010
Modified By:
Last Modified: 
*/
Request::setInteger("serial_cus");
$customer=new Customer($db,$serial_cus);
$list=$customer->getRegisterCardByCustomer();
$smarty->assign('container','none');
$smarty->register('list');
$smarty->display('helpers/customer/loadRegisterCardByCustomer.'.$_SESSION['language'].'.tpl');
?>
