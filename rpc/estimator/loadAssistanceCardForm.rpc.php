<?php 
/*
File: loadAssistanceCardForm.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/

	$productVal = $_SESSION['web_productVal'];
	$totalPrice = 0;
	$countries = Country::getAllCountries($db);
	
	$allProducts = array();
	
	if($productVal == "-1"){
		$serial_cou = $_SESSION['country'];
		if($serial_cou){
		    $product = new Product($db);
		    $products = $product->getProductsByCountryEstimator($serial_cou, $_SESSION['serial_lang']);
		}
		
		if(!$products){
			 $defaultDealer = new Parameter($db,'28');
		     $defaultDealer -> getData();
			 $products = $product->getProductsByCountryEstimator($defaultDealer -> getValue_par(), $_SESSION['serial_lang']);
		}
		
		if($products){
		    foreach($products as &$pl){
		        $pl['name_pbl'] = utf8_encode($pl['name_pbl']);
		    }
		}
		
		foreach($products as $pro){
			$product = new Product($db,$pro['serial_pro']);
			$infoProduct = $product -> getInfoProduct($_SESSION['language']);
			$sinlgeProduct = array(	'quantity' => 1,
									'name' => $infoProduct[0][name_pbl],
									'totalCost' => '200');
			$allProducts[] = $sinlgeProduct;
			$totalPrice += $sinlgeProduct[totalCost];
		}
	}else{
		$product = new Product($db,$productVal);
		$product -> getData();
		$infoProduct = $product -> getInfoProduct($_SESSION['language']);
		$sinlgeProduct = array(	'quantity' => 1,
								'name' => $infoProduct[0][name_pbl],
								'totalCost' => '200');
		$allProducts[] = $sinlgeProduct;
		$totalPrice += $sinlgeProduct[totalCost];
	}
	
	$card1 = array('number'=>1);
	$card2 = array('number'=>2);
	
	$assistCards = array($card1,$card2);
	
	
	$smarty->register('allProducts,totalPrice,assistCards,countries');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadAssistanceCardForm.'.$_SESSION['language'].'.tpl');
?>