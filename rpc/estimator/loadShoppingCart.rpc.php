<?php 
/*
File: loadShoppingCart.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('useDatabase');
	$useDatabase = $useDatabase == 'true' ? 1 : 0;
	
	$cartProducts = array();
	$totalCartPrice = 0;
	
	if($useDatabase){
		$preOrder = new PreOrder($db, NULL, $_SESSION['serial_customer_estimator'], 'CUSTOMER', time());
		$cartProducts = $preOrder -> getAllMyCartProductItems(Language::getSerialLangByCode($db, $_SESSION['language']));
		
		$estimatorUrl = "modules/estimator/registered/estimatorCustomer";
	}else{
		if(isset($_SESSION['system_estimator_shopping_cart_products'])){
			foreach ($_SESSION['system_estimator_shopping_cart_products'] as $sessProd){
				$cartProducts[] = $sessProd;
			}
		}
		if(isset($_SESSION['web_estimator_session_products'])){
			foreach ($_SESSION['web_estimator_session_products'] as $sessProd){
				$cartProducts[] = $sessProd;
			}
		}
		$estimatorUrl = "modules/estimator/estimator";
	}
	
	foreach ($cartProducts as &$sessProd){
		$prodByCou = new ProductByCountry($db, $sessProd['serial_pxc']);
		$prodByCou -> getData();
		$sessProd['serial_pro'] = $prodByCou -> getSerial_pro();
		$departureCou = $prodByCou -> getSerial_cou();
		$sessProd['departure_cou'] = $departureCou;
		$totalCartPrice += $sessProd['total_price_pod'];
	}
	
	$smarty->register('cartProducts,totalCartPrice,estimatorUrl');
	$smarty->assign('container','none');
	$smarty->display('helpers/estimator/loadShoppingCart.'.$_SESSION['language'].'.tpl');
?>