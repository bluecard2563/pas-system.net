<?php
/*
File: loadBonusList.rpc
Author: David Bergmann
Creation Date: 18/03/2010
Last Modified:
Modified By:
*/

Request::setInteger('serial_cou');
Request::setInteger('serial_cit');
Request::setInteger('serial_dea');

$titles = array("Pa&iacute;s","Ciudad","Comercializador","Producto","Porcentaje","Editar","Desactivar");

$bonus = new Bonus($db);
$bonusList = $bonus->getBonusTable($serial_cou,$serial_cit,$serial_dea);

if(is_array($bonusList)){
	foreach($bonusList as &$bl){
            $bl['name_dea']=utf8_encode($bl['name_dea']);
            $bl['name_pbl']=utf8_encode($bl['name_pbl']);
	}
}

$smarty->register('bonusList,titles,serial_dea');
$smarty->assign('container','none');
$smarty->display('helpers/loadBonusList.'.$_SESSION['language'].'.tpl');
?>