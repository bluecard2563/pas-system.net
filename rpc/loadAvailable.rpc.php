<?php
/*
File:loadAvailable.rpc.php
Author: Nicolas Flores
Creation Date: 25/01/2010
Modified By:
Last Modified:
Description:Get the available Stock per manager, dealer, branch.
Parameters:
Id: element value
type: element type
*/

if($_POST['id'] && $_POST['type']  ){
	$stock=new Stock($db);
	$typeList=$stock->getTypeValues();
	$availableStock=array();
	switch($_POST['type']){
		case 'manager':
			 foreach ($typeList as $tl){
				  $availableStock[$tl]=$stock->getAvailableForDealer($_POST['id'],$tl);
			 }
			break;
		case 'dealer':
			foreach ($typeList as $tl){
				  $availableStock[$tl]=$stock->getAvailableForBranch($_POST['id'],$tl);
			 }
			break;
		case 'branch':
			foreach ($typeList as $tl){
				  $availableStock[$tl]=$stock->getAvailableForCounter($_POST['id'],$tl);
			 }
			break;

	}
}else{
    return 'rpc getAvailable failed';
}

$smarty->register('availableStock,typeList');
$smarty->assign('container','none');
$smarty->display('helpers/loadAvailable.'.$_SESSION['language'].'.tpl');
?>
