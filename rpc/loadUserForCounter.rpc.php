<?php 
/*
File: loadUsers.php
Author: Esteban Angulo
Creation Date: 05/01/2010 11:48
Last Modified: 05/01/2010
Modified By: Esteban Angulo
Description: File which loads all the suitable counters
*/

$counter=new Counter($db);
$dealer=new Dealer($db);

if($_POST['serial_dea']!='stop'){
    $dealer->setSerial_dea($_POST['serial_dea']);
    $dealer->getData();
    $offSeler=$dealer->getOfficial_seller_dea();
    $serial_mbc=$dealer->getSerial_mbc();

    $mbc=new ManagerbyCountry($db);
    $mbc->setSerial_mbc($serial_mbc);
    $mbc->getData();
    $offManager=$mbc->getOfficial_mbc();

    $auxList=$counter->getSuitableCountersByDealer($_POST['serial_dea'], $offSeler, $serial_mbc, $offManager);
    $smarty->register('auxList');
}

$smarty->assign('container','none');
$smarty->display('helpers/loadUserForCounter.'.$_SESSION['language'].'.tpl');
?>