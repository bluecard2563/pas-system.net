<?php
/*
File: loadDealersByManagerAndComissionistAndCity.rpc.php
Author: Santiago Borja
Creation Date: 22/02/2010
*/

Request::setString('category');
Request::setString('serial_mbc');
Request::setString('serialComissionist');
Request::setString('city');

if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList = Dealer::getDealersByManagerAndCityAndComissionistAndCategory($db, $serial_mbc, $serialComissionist, $category, $city, $_SESSION['dea_serial_dea']);
}else{
	$dealerList = Dealer::getDealersByManagerAndCityAndComissionistAndCategory($db, $serial_mbc, $serialComissionist, $category, $city);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadDealersByManager.'.$_SESSION['language'].'.tpl');
?>