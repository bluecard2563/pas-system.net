<?php
/*
File: loadServiceProviderCountries.rpc.php
Author: Mario Lopez
Creation Date: 19/02/2010
LastModified: Edwin Salvador
Modified By: 01/03/2010
*/

Request::setInteger('zone');
Request::setString('serial_countries');
//Debug::print_r($_SESSION);die();
if($zone){
    $country=new Country($db);
//    $countriesList=$country->getUnselectedCountriesByZone($zone, $_SESSION['countryList'],$serial_countries);
    $countriesList=$country->getUnselectedCountriesByZone($zone,NULL, $serial_countries);
    echo '<div align="center">Existentes</div>';
    echo '<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" zone="'.$zone.'">';
    if(is_array($countriesList)){
        foreach($countriesList as &$dl){
            echo '<option value="'.$dl['serial_cou'].'" zone="'.$zone.'">'.utf8_encode($dl['name_cou']).'</option>';
        }
    }
    echo '</select>';
}
?>
