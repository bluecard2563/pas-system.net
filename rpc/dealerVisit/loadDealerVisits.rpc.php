<?php
/*
File: loadDealersVisits.rpc
Author: David Bergmann
Creation Date: 10/03/2010
Last Modified: 21/07/2010
Modified By: David Bergmann
*/

Request::setInteger('serial_usr');
//EVENTS FOR ACTUAL USER
$dealerVisit = new DealerVisit($db);
$usr_events = $dealerVisit->getCalendar($serial_usr);

$events = array();

if(is_array($usr_events)) {
	foreach($usr_events as &$evt) {
		//Format Classname
		if($evt['status'] == "VISITED") {
			$evt['className'] = "visited";
		} else if($evt['status']=="SKIPPED"){
			$evt['className'] = "skipped";
		} else {
			$evt['className'] = "event";
		}
		//Format allday
		if($evt['allday'] == "YES") {
			$evt['allday'] = true;
		} else if($evt['allday'] == "NO"){
			$evt['allday'] = false;
		}
		
		$aux = array('id' => $evt['id'],
				   'serial' => $evt['serial'],
				   'user' => $evt['user'],
				   'title'  => $evt['title'],
				   'start'  => $evt['start'],
				   'end' => $evt['end'],
				   'type' => $evt['type'],
				   'status' => $evt['status'],
				   'allDay' => $evt['allday'],
				   'className' => $evt['className']
		);

		array_push($events,$aux);
	}
	echo json_encode($events);;
} else {
	echo json_encode(FALSE);
}

?>
