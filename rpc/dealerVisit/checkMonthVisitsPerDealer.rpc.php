<?php
/*
File: checkMonthVisitsPerDealer.rpc
Author: David Bergmann
Creation Date: 15/03/2010
Last Modified:
Modified By:
*/

Request::setString('serial_vis');

$dealerVisit = new DealerVisit($db,$serial_vis);
$dealerVisit->getData();
$dealer = new Dealer($db,$dealerVisit->getSerial_dea());
$dealer->getData();
$visit_number = $dealer->getVisits_number_dea();
if($dealerVisit->getMonthVisitsPerDealer() <= $visit_number) {
    echo json_encode(false);
} else {
    echo json_encode(true);
}

?>
