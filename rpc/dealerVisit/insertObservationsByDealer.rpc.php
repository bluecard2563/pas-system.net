<?php
/*
File: insertObservationsByDealer.rpc
Author: David Bergmann
Creation Date: 12/03/2010
Last Modified:
Modified By:
*/

Request::setString('serial_vis');
Request::setString('host_obv');
Request::setString('comments_obv');
Request::setString('strategy_obv');

$observationsByVisit = new ObservationsByVisit($db);
$observationsByVisit -> setSerial_vis($serial_vis);
$observationsByVisit -> setHost_obv(utf8_decode($host_obv));
$observationsByVisit -> setComments_obv(utf8_decode($comments_obv));
$observationsByVisit -> setStrategy_obv(utf8_decode($strategy_obv));

if($observationsByVisit->insert()) {
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>