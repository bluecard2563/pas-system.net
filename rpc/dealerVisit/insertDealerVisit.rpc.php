<?php
/*
File: insertDealerVisit.rpc
Author: David Bergmann
Creation Date: 11/03/2010
Last Modified:
Modified By:
*/

Request::setString('newEvent');
$event = explode(',',$newEvent);

$dealerVisit = new DealerVisit($db);
$dealerVisit->setSerial_vis($event[0]);
$dealerVisit->setSerial_dea($event[1]);
$dealerVisit->setSerial_usr($event[2]);
$dealerVisit->setTitle_vis($event[3]);
$dealerVisit->setStart_vis($event[4]);
$dealerVisit->setEnd_vis($event[5]);
$dealerVisit->setType_vis($event[6]);
$dealerVisit->setStatus_vis($event[7]);
$dealerVisit->setAllday_vis($event[8]);

if($dealerVisit->insert()) {
    echo json_encode(true);
} else {
    echo json_encode(false);
}
?>
