<?php
/*
File: loadObservationsByVisit.rpc
Author: David Bergmann
Creation Date: 23/03/2010
Last Modified:
Modified By:
*/

Request::setString('serial_vis');

$observations = new ObservationsByVisit($db);
$observations->setSerial_vis($serial_vis);
$data = $observations->getDataByVisit();
echo json_encode($data);
?>