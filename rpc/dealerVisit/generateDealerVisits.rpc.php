<?php

/*
  File: generateDealerVisits.rpc
  Author: David Bergmann
  Creation Date: 03/03/2010
  Last Modified: 20/05/2011
  Modified By: Santiago Arellano
 */

Request::setInteger('serial_usr');
Request::setInteger('month');
Request::setInteger('year');

/* Create Calendar parameters */
$start_hou_visits = 9;
$start_min_visits = 0;
$finish_hou_visits = 18;
//$hour_interval_visits = 2;
//$minutes_interval_visits = 0;
$min_interval_visits = 15; //minutes
//$user = new User($db, $serial_usr);
//$visitsNumber = $user->getVisitsNumber_usr();
//$user->getData();
//Obtains al the Dealers this user is responsible for
$dealer = new Dealer($db);
$dealerList = $dealer->getBranchesForCalendar($serial_usr);
$events = array();

if (is_array($dealerList)) {
	$count = $dealer->getTotalVisitsPerMonthByUser($serial_usr);
	$visitsNumber = ceil($count / 21);
	$buildEvents = array();

	createCalendar($dealerList, $events, $buildEvents, $serial_usr, $year, $month, $visitsNumber, $start_hou_visits, $start_min_visits, $finish_hou_visits, $min_interval_visits);
}

//Registers events into Database
if ($events != '') {
	$dealerVisit = new DealerVisit($db);
	$return = TRUE;
	foreach ($events as $evt) {
		$dealerVisit->setSerial_vis($evt[id]);
		$dealerVisit->setSerial_dea($evt[serial]);
		$dealerVisit->setSerial_usr($evt[user]);
		$dealerVisit->setTitle_vis($evt[title]);
		$dealerVisit->setStart_vis($evt[start]);
		$dealerVisit->setEnd_vis($evt[end]);
		$dealerVisit->setType_vis($evt[type]);
		$dealerVisit->setStatus_vis($evt[status]);
		$dealerVisit->setAllday_vis($evt[allDay]);

		if (!$dealerVisit->insert()) {
			$return = FALSE;
			break;
		}
	}
} elseif (!$events) {
	$return = 'to much visits';
} else {
	$return = FALSE;
}

echo json_encode($return);

/* FUNCTIONS */

/**
 *
 * @param <int> $day
 * @param <int> $month
 * @param <int> $year
 * @return <date> The next available day from monday - fryday
 */
function notWeekend($day, $month, $year) {
	//echo date("N", mktime(0, 0, 0, $month, $day, $year))."-";
	if (date("N", mktime(0, 0, 0, $month, $day, $year)) == 6 || date("N", mktime(0, 0, 0, $month, $day, $year)) == 7) {
		$day++;
		$day = notWeekend($day, $month, $year);
	}
	return $day;
}

//event-creation algorithm
/**
 * @name createCalendar
 * @param <array> $dealerList
 * @param <array> $events
 * @param <array> $buildEvents
 * @param <int> $serial_usr
 * @param <int> $year
 * @param <int> $month
 * @param <int> $visitsNumber
 * @param <int> $start_hou_visits
 * @param <int> $start_min_visits
 * @param <int> $finish_hou_visits
 * @param <int> $min_interval_visits
 * @param <int> $day
 * @param <int> $hour
 * @param <int> $event_id*/

function createCalendar($dealerList, &$events, $buildEvents, $serial_usr, $year, $month, $visitsNumber, $start_hou_visits, $start_min_visits, $finish_hou_visits, $min_interval_visits, $day=1, $event_id=1) {
	$valid = true;
	$interval_visits = 9 / $visitsNumber;
	if ($interval_visits > $min_interval_visits / 60) {

		$minutes_interval_visits = ($interval_visits) * 60;

		foreach ($dealerList as $dea) {

			$position = ceil(10 / $dea['visits_number_dea']);
			$day = 1;
			$day = notWeekend($day, $month, $year);
			$hour = $start_hou_visits;
			$minute = $start_min_visits;

			while ($dea['visits_number_dea'] > 0) {


				$today = getdate();
				$hour = $start_hou_visits;
				$minute = $start_min_visits;
				$eventInsertedBand = 0;

				while ($eventInsertedBand == 0) {
					if (is_array($buildEvents[$day])) {
						$visits_made = count($buildEvents[$day]);
					} else {
						$buildEvents[$day] = array();
						$visits_made = count($buildEvents[$day]);
					}

					if ($visits_made < $visitsNumber) {

						while ($date == '') {
							if (is_array($buildEvents[$day])) {
								$startHour = end($buildEvents[$day]);
								if ($startHour) {
									$startHour = explode(" ", $startHour['start']);
									$startHour = explode(":", $startHour['1']);
									$hour = $startHour['0'];
									$minute = $startHour['1'];
								}

								if (date("H", mktime($hour, $minute + $minutes_interval_visits, 0, $month, $day, $year)) > $finish_hou_visits) {
									$hour = $start_hou_visits;
									$minute = $start_min_visits;
									$day+=1;
									$day = notWeekend($day, $month, $year);
								} else {

									if ($startHour) {
										$date = mktime($hour, $minute + $minutes_interval_visits, 0, $month, $day, $year);
									} else {
										$date = mktime($hour, $minute, 0, $month, $day, $year);
									}
								}
							} else {
								$buildEvents[$day] = array();
								$hour = $start_hou_visits;
								$minute = $start_min_visits;

								$date = mktime($hour, $minute, 0, $month, $day, $year);
							}
						}

						array_push($buildEvents[$day],
								array('id' => $today[0] . $event_id,
									'serial' => $dea['serial_dea'],
									'user' => $serial_usr,
									'title' => utf8_encode($dea['name_dea'] . " - " . $dea['code_dea']),
									'start' => date("Y-m-d H:i", $date), //mktime(16, 30, 0, 3, 1, 2010)
									'end' => date("Y-m-d H:i", mktime(date("H", $date), date("i", $date) + $minutes_interval_visits, 0, date("m", $date), date("d", $date), date("Y", $date))),
									'type' => "",
									'status' => "PENDING",
									'allDay' => false,
									'className' => "event"
						));
						$event_id++;
						$dea['visits_number_dea']-=1;
						if ($dea['visits_number_dea'] > 0) {

							if ($day + $position > 21) {
								$day+=2;
							} else {
								$day+=$position;
							}
							$day = notWeekend($day, $month, $year);
						}
						$eventInsertedBand = 1;
						$date = '';

						if (count($buildEvents) > 21) {

							$valid = false;
							break;
						}
					} else {
						$day+=1;

						$day = notWeekend($day, $month, $year);
					}
				}
			}

			if ($valid == false) {
				break;
			}
		}
		if ($valid) {
			foreach ($buildEvents as $eve) {
				foreach ($eve as $eveDays) {
					array_push($events, $eveDays);
				}
			}

			return $events;
		} else {
			$events = FALSE;
			return FALSE;
		}
	} else {
		$events = FALSE;
		return FALSE;
	}
}

  /* FUNCTIONS */
?>