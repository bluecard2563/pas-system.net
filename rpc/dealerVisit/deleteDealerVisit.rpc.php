<?php
/*
File: deleteDealerVisit.rpc
Author: David Bergmann
Creation Date: 10/03/2010
Last Modified:
Modified By:
*/

Request::setString('serial_vis');
if($serial_vis) {
    $dealerVisit = new DealerVisit($db,$serial_vis);
    if($dealerVisit->delete()) {
        echo json_encode(true);
    } else {
        echo json_encode(false);
    }
}

?>
