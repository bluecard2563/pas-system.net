<?php
/*
File: updateDealerVisit.rpc
Author: David Bergmann
Creation Date: 11/03/2010
Last Modified:
Modified By:
*/
Request::setString('id');
Request::setString('type');
Request::setString('status');
Request::setString('start');
Request::setString('end');
Request::setString('allDay');

$dealerVisit = new DealerVisit($db,$id);

if($type) {
    $dealerVisit->setType_vis($type);
}
if($status) {
    $dealerVisit->setStatus_vis($status);
}
if($start) {
    $dealerVisit->setStart_vis($start);
}
if($end) {
    $dealerVisit->setEnd_vis($end);
}
if($allDay) {
    $dealerVisit->setAllday_vis($allDay);
}
if($dealerVisit->update()) {
    echo json_encode(true);
} else {
    echo json_encode(false);
}

?>
