<?php
/*
File: loadDealersByUser.rpc
Author: David Bergmann
Creation Date: 15/03/2010
Last Modified:
Modified By:
*/
Request::setInteger('serial_usr');

$report = $_POST['report'];
if($report) {
    $smarty->register('report');
}

$dealer = new Dealer($db);
$dealerList=$dealer->getDealersForCalendar($serial_usr);

if(is_array($dealerList)){
	foreach($dealerList as &$dl){
		$dl['name_dea']=utf8_encode($dl['name_dea']);
	}
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/dealerVisit/loadDealersByUser.'.$_SESSION['language'].'.tpl');
?>
