<?php
/*
File: clearMonthVisits.rpc
Author: David Bergmann
Creation Date: 21/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_usr');
Request::setString('month');
Request::setString('year');

$dealerVisit = new DealerVisit($db);
$dealerVisit->setSerial_usr($serial_usr);

if($dealerVisit->deleteEventsByUser($month, $year)) {
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}
?>