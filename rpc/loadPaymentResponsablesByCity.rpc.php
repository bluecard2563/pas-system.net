<?php
/*
File: loadResponsablesByCity.rpc.php
Author: Edwin Salvador
Creation Date: 11/03/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
//REPORTS
Request::setString('opt_text');

if($serial_cit) {
    $dealer = new Dealer($db);
	if($_SESSION['serial_dea']!=''){
		$responsablesList =  $dealer->getPaymentResponsablesByCity($serial_cit, $_SESSION['serial_mbc'], $_SESSION['serial_dea']);
	}else{
		$responsablesList =  $dealer->getPaymentResponsablesByCity($serial_cit, $_SESSION['serial_mbc']);
	}

    if(is_array($responsablesList)){
        foreach($responsablesList as &$rl){
            $cl['name_usr']=utf8_encode($cl['name_usr']);
        }
    }
    $smarty->register('responsablesList');
}
$smarty->register('opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>