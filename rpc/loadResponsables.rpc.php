<?php
/*
File: loadUsersByManager.rpc.php
Author: David Bergmann
Creation Date: 15/02/2010
Last Modified:
Modified By:
Description: File which loads all the data for the management
			 of credit days by country.
*/

Request::setString('serial_mbc');
Request::setString('opt_text');


$user = new User($db);
if($serial_mbc) {
    $comissionistList=$user->getUsersByManager($serial_mbc);
}

$smarty->register('comissionistList,serial_mbc,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsables.'.$_SESSION['language'].'.tpl');
?>