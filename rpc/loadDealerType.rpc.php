<?php 
/*
File: loadDealerTypeInfo.rpc.php
Author: David Bergmann
Creation Date: 25/01/2010
Last Modified:
Modified By:
Description: File which loads all the data for the management 
			 of dealer type by language.
*/

Request::setString('serialDea');
$dealerType=new DealerType($db);
$dataByLanguage=$dealerType->getDealerTypeInfo();

$smarty->register('dataByLanguage');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersType.'.$_SESSION['language'].'.tpl');
?>