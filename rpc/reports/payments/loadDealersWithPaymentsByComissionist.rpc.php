<?php 
/*
	File: loadComissionistsByManager.rpc.php
	Author: Santiago Borja.
	Creation Date: 13/04/2010
*/
Request::setString('serial_usr');
Request::setString('serial_cit');

$dealersList = Dealer::getDealersWithPaymentsByComissionist($db, $serial_usr, $serial_cit);

$smarty->register('dealersList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadDealersWithPaymentsByComissionist.'.$_SESSION['language'].'.tpl');
?>