<?php 
/*
File: loadCitiesByCountry.php
Author: Gabriela Guerrero
Creation Date: 20/05/2010
Last Modified: 
Modified By: 
*/

$city = new City($db);
$cityList = $city->getPaymentCitiesByCountry($_POST['serial_cou'],$_SESSION['serial_mbc'],$_SESSION['cityList']);

$smarty->register('cityList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadCitiesByCountry.'.$_SESSION['language'].'.tpl');
?>