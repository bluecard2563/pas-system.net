<?php 
/*
File: loadUsersByBranch.php
Author: Gabriela Guerrero
Creation Date: 07/06/2010
Last Modified: 
Modified By: 
*/

$serial_bra=$_POST['serial_bra'];


$user = new User($db);
$dealer=new Dealer($db);
$mbc=new ManagerbyCountry($db);

	$dealer->setSerial_dea($serial_bra);
    $dealer->getData();
    $offSeller=$dealer->getOfficial_seller_dea();
    $serial_mbc=$dealer->getSerial_mbc();

    $mbc->setSerial_mbc($serial_mbc);
    $mbc->getData();
    $offManager=$mbc->getOfficial_mbc();
	$userList=$user->getUsersByBranch($serial_bra, $offSeller, $serial_mbc, $offManager);


$smarty->register('userList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadUsersByBranch.'.$_SESSION['language'].'.tpl');
?>