<?php 
/*
File: loadManagersByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 31/05/2010
Last Modified: 
Modified By: 
*/
	
	set_time_limit(36000);
	$serial_cou=$_POST['serial_cou'];
	$required = $_POST['required'];
	
	$mbc = new ManagerbyCountry($db);
	if($_POST['type'] == 'excess'){
		$mbcList = $mbc->getManagersWithExcessPayments($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
	}
	else{
		if($_POST['type'] == 'payments'){
			$mbcList = $mbc->getManagersWithPayments($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
		}
		else{
			$mbcList = $mbc->getManagersByCountryWithInvoices($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
		}
	}

	$smarty->register('mbcList,required');
	$smarty->assign('container','none');
	$smarty->display('helpers/reports/payments/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>