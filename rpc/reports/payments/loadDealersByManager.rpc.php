<?php 
/*
File: loadDealersByManager.php
Author: Gabriela Guerrero
Creation Date: 31/05/2010
Last Modified: 
Modified By: 
*/

Request::setString('serial_mbc');
$dealer = new Dealer($db);

if($_POST['type'] == 'excess'){
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$dealerList = $dealer->getDealersExcessPayments($serial_mbc,$_SESSION['dea_serial_dea']);
	}else {
		$dealerList = $dealer->getDealersExcessPayments($serial_mbc);
	}
}else{
	if($_POST['type'] == 'payments'){
		//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
		if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$dealerList = $dealer->getDealersWithPayments($serial_mbc,$_SESSION['dea_serial_dea']);
		}else{
			$dealerList = $dealer->getDealersWithPayments($serial_mbc);
		}
	}else{
		$category=$_POST['category'];
		//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
		if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$dealerList = $dealer->getDealersByManagerWithInvoices($serial_mbc, $category, $_SESSION['dea_serial_dea']);
		}else{
			$dealerList = $dealer->getDealersByManagerWithInvoices($serial_mbc, $category);
		}
	}
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadDealersByManager.'.$_SESSION['language'].'.tpl');
?>