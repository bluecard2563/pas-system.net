<?php
	$invoice = new Invoice($db);
	$invoiceRaw = $invoice -> checkHasNotPayedInvoices(	$_POST['serial_zon'],
													$_POST['serial_cou'],
													$_POST['serial_mbc'],
													$_SESSION['serial_mbc'],
													$_SESSION['dea_serial_dea'],
													$_POST['serial_cit'],
													$_POST['serial_dea'],
													$_POST['serial_bra'],
													$_POST['date_from'],
													$_POST['date_to'],
													$_POST['days'],
													$_POST['category_dea'],
													$_POST['selComissionist']);
	if($invoiceRaw){
		echo json_encode(TRUE);
	}else{
		echo json_encode(FALSE);
	}
?>