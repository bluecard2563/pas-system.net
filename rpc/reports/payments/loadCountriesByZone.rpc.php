<?php 
/*
File: loadCountriesByZone.rpc.php
Author: Gabriela Guerrero
Creation Date: 31/05/2010
Last Modified: 
Modified By: 
*/

$serial_zon=$_POST['serial_zon'];
$required = $_POST['required'];
$country = new Country($db);
$countryList = $country->getCountriesByZoneWithInvoices($serial_zon,$_SESSION['countryList'], $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
$smarty->register('countryList,required');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadCountriesByZone.'.$_SESSION['language'].'.tpl');
?>