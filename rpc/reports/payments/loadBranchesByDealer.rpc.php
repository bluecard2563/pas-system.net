<?php 
/*
File: loadBranchesByDealer.php
Author: Gabriela Guerrero
Creation Date: 31/05/2010
Last Modified: 
Modified By: 
*/

$dea_serial_dea=$_POST['dea_serial_dea'];

$branch = new Dealer($db);

if($_POST['type']=='excess'){
	//If $_SESSION['serial_dea'] is set, loads only the user's dealer
	if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$branchList = $branch->getBranchesExcessPayments($dea_serial_dea, $_SESSION['serial_dea']);
	} else{
		$branchList = $branch->getBranchesExcessPayments($dea_serial_dea);
	}
}
else{
	if($_POST['type']=='payments'){
		//If $_SESSION['serial_dea'] is set, loads only the user's dealer
		if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$branchList = $branch->getBranchesWithPayments($dea_serial_dea, $_SESSION['serial_dea']);
		} else {
			$branchList = $branch->getBranchesWithPayments($dea_serial_dea);
		}
	}
	else{
		//If $_SESSION['serial_dea'] is set, loads only the user's dealer
		if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$branchList = $branch->getBranchesByDealerWithInvoices($dea_serial_dea, $_SESSION['serial_dea']);
		} else {
			$branchList = $branch->getBranchesByDealerWithInvoices($dea_serial_dea);
		}
	}
}

$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/payments/loadBranchesByDealer.'.$_SESSION['language'].'.tpl');
?>