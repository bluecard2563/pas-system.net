<?php
/*
File: loadResponsiblesByDealer.rpc
Author: David Bergmann
Creation Date: 23/12/2010
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');

$data = UserByDealer::getResponsibleByDeaByMbc($db, $serial_mbc);


$smarty->register('serial_mbc,data');
$smarty->assign('container','none');
$smarty->display('helpers/reports/loadResponsiblesByDealer.'.$_SESSION['language'].'.tpl');
?>