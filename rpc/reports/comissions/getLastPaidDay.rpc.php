<?php
/*
    Document   : getLastPaidDay.rpc
    Created on : 01-jun-2010, 11:02:55
    Author     : Nicolas
    Description:
        gets the last comission payment date based on type.
*/
Request::setString('type');
Request::setInteger('id');
$types=array('MANAGER','DEALER','RESPONSIBLE');
if(in_array($type,$types)){
	switch($type){
		case 'DEALER':
			$dealer=new Dealer($db, $id);
			$dealer->getData();
			$date_last_comission=$dealer->getLast_commission_paid_dea();
			break;
		case 'RESPONSIBLE':
			$responsible=new UserByDealer($db);
			$date_last_comission=$responsible->getLast_commission_paid_by_usr($id);
			
			break;
		case 'MANAGER':
			$manager=new ManagerbyCountry($db,$id);
			$manager->getData();
			$date_last_comission=$manager->getLast_commission_paid_mbc();
			break;
		default:
			break;
	}
	if($date_last_comission)
		echo substr($date_last_comission,0,10);
}else{
	echo false;
}
?>
