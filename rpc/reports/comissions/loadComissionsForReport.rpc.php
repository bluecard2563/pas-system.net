<?php
/*
    Document   : loadComissionsForReport.rpc
    Created on : 08/06/2010, 16:36:55
    Author     : Nicolas
    Description:
       Get all comission according to the parameters recieved.
*/
Request::setString('comission_to');
Request::setString('serial');
Request::setString('type');
Request::setString('startDate');
Request::setString('endDate');
Request::setInteger('serial_mbc');
Request::setInteger('serial_usr');
Request::setInteger('serial_dea');

if($comission_to == 'DEALER'){
	$misc['serial_mbc'] = $serial_mbc;
	$misc['serial_usr'] = $serial_usr;
	$misc['serial_dea'] = $serial_dea;
}

$apComission=new AppliedComission($db);
$comissionsData=$apComission->getComissionForReport($comission_to,$serial,$type,$startDate,$endDate, NULL, $misc);
if($comissionsData){
	$smarty->register('comissionsData');
}

$smarty->register('type,serial');
$smarty->assign('container','none');
$smarty->display('helpers/reports/comissions/loadComissionsForReport.'.$_SESSION['language'].'.tpl');
?>
