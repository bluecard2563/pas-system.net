<?php
/**
 * File:  loadAverageComissionsInfo.rpc
 * Author: Patricio Astudillo
 * Creation Date: 28-abr-2011, 10:21:24
 * Description:
 * Last Modified: 28-abr-2011, 10:21:24
 * Modified by:
 */

	Request::setInteger('serial_cou');
	Request::setInteger('serial_mbc');
	Request::setInteger('serial_ubd');
	Request::setString('begin_date');
	Request::setString('end_date');

	$gross_comissions = Dealer::getGrossComissionsInfo($db, $serial_cou, $begin_date, $end_date, $serial_mbc, $serial_ubd);

	$smarty -> register('gross_comissions');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/reports/comissions/loadAverageComissionsInfo.'.$_SESSION['language'].'.tpl');
?>