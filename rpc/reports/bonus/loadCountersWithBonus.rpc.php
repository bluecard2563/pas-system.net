<?php
/*
 * File: loadCountersWithBonus.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 02:21:16 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:21:16 PM
 */

Request::setString('serial_dea');
Request::setString('bonus_to');
Request::setString('at_least_one_paid');
Request::setString('authorized');

$countersWithBonus=Counter::getCountersWithBonus($db, $serial_dea, $bonus_to, $at_least_one_paid, $authorized);

$smarty->assign('counterList',$countersWithBonus);
$smarty->assign('container','none');
$smarty->display('helpers/loadCounter.'.$_SESSION['language'].".tpl");
?>
