<?php
/*
 * File: loadManagersWithBonus.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 02:46:16 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:46:16 PM
 */

Request::setString('serial_cou');
Request::setString('bonus_to');
Request::setString('at_least_one_paid');
Request::setString('authorized');

$managersWithBonus=ManagerByCountry::getManagersWithBonus($db, $serial_cou, $_SESSION['serial_mbc'], $bonus_to, $at_least_one_paid, $authorized);

$smarty->assign('managerList',$managersWithBonus);
$smarty->assign('container','none');
$smarty->display('helpers/loadManagersByCountry.'.$_SESSION['language'].".tpl");
?>
