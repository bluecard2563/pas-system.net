<?php
/*
 * File: loadDealersWithBonus.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 03:56:16 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:21:16 PM
 */

Request::setString('serial_cit');
Request::setString('serial_mbc');
Request::setString('bonus_to');
Request::setString('at_least_one_paid');
Request::setString('authorized');

$dealersWithBonus=Dealer::getDealersWithBonus($db,$serial_mbc, $serial_cit, $bonus_to, $at_least_one_paid, $authorized);

$smarty->assign('dealerList',$dealersWithBonus);
$smarty->assign('container','none');
$smarty->display('helpers/loadDealers.'.$_SESSION['language'].".tpl");
?>
