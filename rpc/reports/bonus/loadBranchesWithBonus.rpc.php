<?php
/*
 * File: loadBranchesWithBonus.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 02:21:16 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:21:16 PM
 */

Request::setString('serial_dea');
Request::setString('bonus_to');
Request::setString('at_least_one_paid');
Request::setString('authorized');

$dealersWithBonus=Dealer::getBranchesWithBonus($db, $serial_dea, $bonus_to, $at_least_one_paid, $authorized);
$branch=1;

$smarty->assign('dealerList',$dealersWithBonus);
$smarty->register('branch');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealers.'.$_SESSION['language'].".tpl");
?>
