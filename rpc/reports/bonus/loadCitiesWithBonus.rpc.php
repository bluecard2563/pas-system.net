<?php
/*
 * File: loadCitiesWithBonus.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 02:21:16 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:21:16 PM
 */

Request::setString('serial_cou');
Request::setString('bonus_to');
Request::setString('at_least_one_paid');
Request::setString('authorized');

$citiesWithBonus=City::getCitiesWithBonus($db, $serial_cou, $bonus_to, $at_least_one_paid, $authorized);

$smarty->assign('cityList',$citiesWithBonus);
$smarty->assign('container','none');
$smarty->display('helpers/loadCities.'.$_SESSION['language'].".tpl");
?>
