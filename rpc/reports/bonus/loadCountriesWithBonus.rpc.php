<?php
/*
 * File: loadCountriesWithBonus.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 02:21:16 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:21:16 PM
 */

Request::setString('serial_zon');
$countriesWithBonus=Country::getCountriesWithBonus($db, $_SESSION['countryList'], $serial_zon);

$smarty->assign('countryList',$countriesWithBonus);
$smarty->assign('container','none');
$smarty->display('helpers/loadCountries.'.$_SESSION['language'].".tpl");
?>
