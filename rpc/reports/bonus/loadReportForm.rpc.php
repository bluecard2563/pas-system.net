<?php
/**
 * File:  loadReportForm
 * Author: Patricio Astudillo
 * Creation Date: 01-abr-2011, 10:38:07
 * Description:
 * Last Modified: 01-abr-2011, 10:38:07
 * Modified by:
 */

	Request::setString('bonus_to');
	Request::setString('at_least_one_paid');
	Request::setString('authorized');

	$countriesWithBonus = Country::getCountriesWithBonus($db,$_SESSION['countryList'], NULL, $bonus_to, $at_least_one_paid, $authorized);

	$smarty -> register('bonus_to,countriesWithBonus');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/reports/bonus/loadReportForm.'.$_SESSION['language'].'.tpl');
?>
