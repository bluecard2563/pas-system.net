<?php
/*
File: loadTaxByCountry.rpc
Author: David Bergmann
Creation Date: 28/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');

$tax=new Tax($db);
$taxList=$tax->getTaxes($_POST['serial_cou']);
$titles = array('#','Nombre','Valor');

if(is_array($taxList)){
    foreach($taxList as &$tl){
            $tl['name_tax']=utf8_encode($tl['name_tax']);
    }
    $smarty->register('taxList,titles');
}


$smarty->register('serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/reports/loadTaxByCountry.'.$_SESSION['language'].'.tpl');
?>
