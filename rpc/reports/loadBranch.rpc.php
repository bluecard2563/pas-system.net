<?php
/*
File: loadBranch.rpc
Author: David Bergmann
Creation Date: 26/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');
Request::setString('serial_mbc');
Request::setString('serial_dea');
Request::setString('serial_usr');
Request::setString('status_dea');

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1 && !User::isSubManagerUser($db, $_SESSION['serial_usr'])) {
	$dea_serial_dea = $_SESSION['serial_dea'];
}
global $global_pType;
$dealerList = Dealer::getBranchReport($db, $serial_cou, $serial_mbc, $serial_dea, $serial_usr, $status_dea, $dea_serial_dea);

$titles = array('Nombre','RUC','Porcentaje','Cr&eacute;dito','Ciudad','Sector','Direcci&oacute;n','Tel&eacute;fono','Responsable','Gerente','Contacto','Mail de contacto');

if(is_array($dealerList)) {
	foreach ($dealerList as &$d) {
		$d['name_dea'] = utf8_encode($d['name_dea']);
		$d['name_cit'] = utf8_encode($d['name_cit']);
		$d['name_sec'] = utf8_encode($d['name_sec']);
		$d['address_dea'] = utf8_encode($d['address_dea']);
		$d['manager_name_dea'] = utf8_encode($d['manager_name_dea']);
		$d['contact_dea'] = utf8_encode($d['contact_dea']);
		$d['type_percentage_dea'] = $global_pType[$d['type_percentage_dea']];
		$d['responsible'] = utf8_encode($d['responsible']);		
	}
	$smarty->register('dealerList,titles');
}

$count=count($dealerList);

$smarty->register('count');
$smarty->assign('container','none');
$smarty->display('helpers/reports/loadBranch.'.$_SESSION['language'].'.tpl');
?>