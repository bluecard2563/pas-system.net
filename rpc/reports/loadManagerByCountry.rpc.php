<?php
/*
File: loadManagerByCountry.rpc
Author: David Bergmann
Creation Date: 26/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');

$managerByCountry = new ManagerbyCountry($db);
$managerByCountryList = $managerByCountry->getManagerByCountry($serial_cou, $_SESSION['serial_mbc']);
$titles = array('#','Nombre','Persona de Contacto','E-mail de contacto','Tipo de %','Valor de %','Fecha límite de Pago','Exclusivo','Oficial');
if(is_array($managerByCountryList)){
    foreach ($managerByCountryList as &$ml) {
        $ml['name_man'] = utf8_encode($ml['name_man']);
        $ml['contact_man'] = utf8_encode($ml['contact_man']);
    }
    $smarty->register('titles,managerByCountryList');
}
$smarty->register('global_yes_no,global_pType,global_weekDays');
$smarty->assign('container','none');
$smarty->display('helpers/reports/loadManagerByCountry.'.$_SESSION['language'].'.tpl');
?>
