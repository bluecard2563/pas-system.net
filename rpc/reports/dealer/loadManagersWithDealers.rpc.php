<?php
/*
 * File: loadManagersWithDealers.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 30/06/2010, 09:31:03 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/06/2010, 09:31:03 AM
 */

Request::setString('serial_cou');
Request::setString('opt_text');
Request::setString('comissionableOnly');

if($comissionableOnly == 'TRUE'){
	$specificMbc = $mbcAllowedToComissionate;
}else{
	$specificMbc = $_SESSION['serial_mbc'];
}

$managerList=ManagerbyCountry::getManagersWithDealers($db, $serial_cou, $specificMbc);
if($opt_text)
	$smarty->register('opt_text');
$smarty->register('managerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadManagersWithDealers.'.$_SESSION['language'].'.tpl');
?>
