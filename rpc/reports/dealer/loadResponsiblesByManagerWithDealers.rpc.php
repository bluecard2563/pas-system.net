<?php
/*
 * File: loadResponsiblesByManagerWithDealers.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 05/05/2010, 05:20 PM
 */

Request::setString('serial_mbc');

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$userByDealer = new UserByDealer($db);
$responsiblesList = $userByDealer->getResponsiblesWithAssignedDealers($db, $serial_mbc, $serial_dea);
if($responsiblesList){
	$smarty->register('responsiblesList');
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadResponsiblesByCityAndManager.'.$_SESSION['language'].'.tpl');
?>
