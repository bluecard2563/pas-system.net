<?php
/*
 * File: loadDealerCountries.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 30/06/2010, 09:10:48 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/06/2010, 09:10:48 AM
 */

Request::setString('serial_zon');

($_SESSION['serial_mbc']==1)?$serial_mbc=NULL:$serial_mbc=$_SESSION['serial_mbc'];

$countryList=Country::getOwnCountriesWithDealers($db, $serial_zon, $_SESSION['countryList'], $serial_mbc);

$smarty->register('countryList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadDealerCountries.'.$_SESSION['language'].'.tpl');
?>
