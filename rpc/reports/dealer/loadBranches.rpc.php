<?php
/*
 * File: loadBranches.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 30/06/2010, 12:19:15 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/06/2010, 12:19:15 PM
 */

Request::setString('dea_serial_dea');
Request::setString('serial_mbc');
Request::setString('serial_usr');

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$branchList=Dealer::getBranchesByDealerByResponsible($db, $dea_serial_dea, $serial_usr, $serial_mbc, $_SESSION['serial_dea']);
} else {
	$branchList=Dealer::getBranchesByDealerByResponsible($db, $dea_serial_dea, $serial_usr, $serial_mbc);
}

if(User::isSubManagerUser($db, $_SESSION['serial_usr'])):
	$dealer = new Dealer($db, $_SESSION['serial_dea']);
	$dealer -> getData();

	$branchList = Dealer::getAllBranchesByDealers($db, $dealer->getDea_serial_dea());
endif;

$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadBranches.'.$_SESSION['language'].'.tpl');
?>
