<?php
/*
 * File: loadCardsRelationTable.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2010, 01:22 PM
 */

Request::setString('serial_cou,serial_cit,serial_mbc,serial_usr,begin_date,end_date');

$sales = new Sales($db);
$cardsResume = $sales->getTotalCardsSoldResume($db, $serial_cou, $serial_cit, $serial_mbc, $serial_usr, $begin_date, $end_date);
if($cardsResume){
	$total = $cardsResume['VIRTUAL']+$cardsResume['MANUAL'];
	$virtualPercentage = number_format(round($cardsResume['VIRTUAL']*100/$total, 2), 2);
	$manualPercentage = number_format(round($cardsResume['MANUAL']*100/$total, 2), 2);
	$smarty->register('cardsResume,total,virtualPercentage,manualPercentage');
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadCardsRelationTable.'.$_SESSION['language'].'.tpl');
?>
