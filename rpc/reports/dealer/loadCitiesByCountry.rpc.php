<?php
/*
 * File: loadCitiesByCountry.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2010, 01:22 PM
 */

Request::setString('serial_cou');

if($_SESSION['serial_mbc'] != 1){
	$serial_mbc = $_SESSION['serial_mbc'];
}else{
	$serial_mbc = NULL;
}

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$city = new City($db);
$cityList = $city->getCitiesWithSales($db, $serial_mbc, $serial_dea, $serial_cou);
if($cityList){
	$smarty->register('cityList');
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadCitiesByCountry.'.$_SESSION['language'].'.tpl');
?>
