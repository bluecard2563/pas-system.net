<?php
/*
 * File: loadResponsiblesByCityAndManager.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2010, 01:22 PM
 */

Request::setString('serial_cit,serial_mbc');

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$userByDealer = new UserByDealer($db);
$responsiblesList = $userByDealer->getResponsiblesWithSales($db, $serial_mbc, $serial_dea, $serial_cit);
if($responsiblesList){
	$smarty->register('responsiblesList');
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadResponsiblesByCityAndManager.'.$_SESSION['language'].'.tpl');
?>
