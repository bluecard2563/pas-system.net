<?php
/*
 * File: loadResponsiblesByManager.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 30/06/2010, 09:46:39 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/06/2010, 09:46:39 AM
 */

Request::setString('serial_mbc');

$responsiblesList= UserByDealer::getResponsiblesByManager($db, $serial_mbc);

$smarty->register('responsiblesList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadResponsiblesByManager.'.$_SESSION['language'].'.tpl');
?>
