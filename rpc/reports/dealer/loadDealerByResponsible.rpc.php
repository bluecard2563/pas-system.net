<?php
/*
 * File: loadDealerByResponsible.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 30/06/2010, 11:23:36 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/06/2010, 11:23:36 AM
 */

Request::setString('serial_usr');
Request::setString('serial_mbc');

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList=Dealer::getDealerByResponsible($db, $serial_usr, $serial_mbc, $_SESSION['dea_serial_dea']);
} else {
	$dealerList=Dealer::getDealerByResponsible($db, $serial_usr, $serial_mbc);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadDealerByResponsible.'.$_SESSION['language'].'.tpl');
?>
