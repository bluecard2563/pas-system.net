<?php
/*
 * File: loadEffectivenessTable.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 06/05/2010, 08:5 PM
 */

Request::setString('serial_cou,serial_cit,serial_mbc,serial_usr,begin_date,end_date');

//GETTING NUMBER OF ASSIGNED DEALERS, ACCORDING TO FILTERS
	$dealer = new Dealer($db);
	$assignedDealers = $dealer->getAssignedDealers($db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr);
	if($assignedDealers){		
		$smarty->register('assignedDealers');
		//GETTING NUMBER OF DEALERS WHO SOLD VIRTUAL CARDS
			$dealersSelling = Sales::getDealersSellingCards($db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr);
			if($dealersSelling){
				$sellingPercentage = number_format(round($dealersSelling*100/$assignedDealers, 2), 2);		
				
				//GETTING BEST SELLER IN MONEY
				$bestSellerArray = Sales::getTotalAmountsSoldByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr);
				if($bestSellerArray){
					$nameDealer = '';
					foreach($bestSellerArray as $key=>$seller){
						$nameDealer .= $seller['name_dea'].' ($ '.$seller['total_sales'].'), ';
						if($seller['total_sales'] != $bestSellerArray[$key+1]['total_sales']){
							break;
						}
					}
					$nameDealer = trim($nameDealer, ", ");
					$smarty->register('nameDealer');
				}
				
				//GETTING BEST SELLER IN # OF CARDS
				$bestSellerArray = Sales::getCardsSoldByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr);
				if($bestSellerArray){
					$nameDealerCards = '';
					foreach($bestSellerArray as $key=>$seller_cards){
						$nameDealerCards .= $seller_cards['name_dea'].' ('.$seller_cards['total_cards'].' tarjetas), ';
						if($seller_cards['total_cards'] != $bestSellerArray[$key+1]['total_cards']){
							break;
						}
					}
					$nameDealerCards = trim($nameDealerCards, ", ");
					$smarty->register('nameDealerCards');
				}
			}
			else{
				$dealersSelling = '0';
				$sellingPercentage = '0.00';
			}
			$smarty->register('dealersSelling,sellingPercentage');
	}



$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadEffectivenessTable.'.$_SESSION['language'].'.tpl');
?>
