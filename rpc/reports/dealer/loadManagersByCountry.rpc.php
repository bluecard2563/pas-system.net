<?php
/*
 * File: loadManagersByCountry.rpc.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2010, 01:22 PM
 */

Request::setString('serial_cou');

if($_SESSION['serial_mbc'] != 1){
	$serial_mbc = $_SESSION['serial_mbc'];
}else{
	$serial_mbc = NULL;
}

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$managerByCountry = new ManagerbyCountry($db);
$mbcList = $managerByCountry->loadManagersWithSales($db, $serial_mbc, $serial_dea, $serial_cou);
if($mbcList){
	$smarty->register('mbcList');
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/dealer/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>
