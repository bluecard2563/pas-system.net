<?php
/*
File: loadFileByCustomer.rpc
Author: David Bergmann
Creation Date: 30/11/2010
Last Modified:
Modified By:
*/

$data = File::getFileByCustomer($db,$_POST['serial_cus']);

if($data) {
	$titles = array("Expediente No.","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de asistencia","Estado","Reporte");
	$smarty->register("data,titles");
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadFileByCustomer.'.$_SESSION['language'].'.tpl');
?>