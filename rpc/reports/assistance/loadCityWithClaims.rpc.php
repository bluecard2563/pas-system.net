<?php
/**
 * @author: Luis Salvador
 * File: loadCityWithClaimsrpc
 * Creation Date: 28-sep-2011, 10:29:22
 * Last modified: 28-sep-2011, 10:29:22
 * Modified by:
 */
$serial_cou=$_POST['serial_cou'];
$document_to = $_POST['document_to'];
$cityList = PaymentRequest::getCitiesWithClaims($db, $serial_cou, $document_to);

if(is_array($cityList)){
	foreach($cityList as &$cl){
		$cl['name_cit']=utf8_encode($cl['name_cit']);
	}
}

$smarty->register('cityList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadCitiesWithClaims.'.$_SESSION['language'].'.tpl');
?>
