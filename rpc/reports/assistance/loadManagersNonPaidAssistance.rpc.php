<?php
/**
 * File: loadManagersNonPaidAssistance
 * Author: Patricio Astudillo M.
 * Creation Date: 22/02/2012 
 * Last Modified: 22/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_cou');
	
	$manager_list = ManagerbyCountry::getManagersWithNonPaidInvoicesAndAssistance($db, $serial_cou);

	$smarty->register('manager_list,serial_cou');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/reports/assistances/loadManagersNonPaidAssistance.'.$_SESSION['language'].'.tpl');
?>
