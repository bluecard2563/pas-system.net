<?php
/**
 * @author: Luis Salvador
 * File: loadAssistanceClaimsrpc
 * Creation Date: 27-sep-2011, 16:23:40
 * Last modified: 27-sep-2011, 16:23:40
 * Modified by:
 */

	Request::setString('date_from');
	Request::setString('date_end');
	Request::setString('document_to_dbf');
	Request::setString('serial_cit');
	Request::setString('serial_cou');
	Request::setString('serial_spv');
	Request::setString('processAs');

	$claimList = PaymentRequest::getClaimsInformationForReport($db, $document_to_dbf, $date_from, 
																$date_end, $processAs, $serial_cou, $serial_spv, $serial_cit);

	if(is_array($claimList)) {
		foreach ($claimList as &$c) {
			$c['name_dbf'] = utf8_encode($c['name_dbf']);
			$c['name_cus'] = utf8_encode($c['name_cus']);
			$c['type_prq'] = $global_payment_request_type[$c['type_prq']];
		}
		$count = sizeof($claimList);
	}

	$titles = array('Documento','Pa&iacute;s','No. Documento','Concepto','Valor Aprobado','Fecha','Cliente');

	$smarty->register('document_to_dbf,count,claimList,titles');
	$smarty->assign('container','none');
	$smarty->display('helpers/reports/assistances/loadAssistanceClaims.'.$_SESSION['language'].'.tpl');
?>
