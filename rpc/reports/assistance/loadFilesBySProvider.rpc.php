<?php
/**
 * File: loadFilesBySProvider
 * Author: Patricio Astudillo
 * Creation Date: 26/06/2012
 * Last Modified: 26/06/2012
 * Modified By: Patricio Astudillo
 * 
 */

	Request::setInteger('serial_spv');
	Request::setString('date_from');
	Request::setString('date_to');
	
	$summary = ServiceProvider::getFilesByProvidedSummary($db, $serial_spv, $date_from, $date_to, TRUE);
	//Debug::print_r($summary);
	
	$smarty -> assign('container', 'none');
	$smarty -> register('summary');
	$smarty -> display('helpers/reports/assistances/loadFilesBySProvider.'.$_SESSION['language'].'.tpl');	
?>
