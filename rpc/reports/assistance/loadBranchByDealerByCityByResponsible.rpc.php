<?php
/*
File: loadBranchByDealerByCityByResponsible.rpc
Author: David Bergmann
Creation Date: 30/11/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
Request::setString('serial_mbc');
Request::setString('serial_usr');
Request::setString('serial_dea');

$dealerList = Dealer::getBranchByDealerByCityByResponsible($db, $serial_cit, $serial_usr, $serial_mbc,$serial_dea);

$branch = true;

$smarty->register('serial_cit,serial_mbc,serial_usr,serial_dea,serial_dea,dealerList,branch');
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadDealerByCityByResponsible.'.$_SESSION['language'].'.tpl');

?>