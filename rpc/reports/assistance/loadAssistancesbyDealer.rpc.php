<?php
/*
File:loadAssistancesbyDealer
Author: Esteban Angulo
Creation Date: 14/06/2010
*/


$serial_dea=$_POST['serial_dea'];
if($serial_dea!=''){
	$files = new File($db);
	$fileList= $files->getFilesbyDealer($serial_dea);

	if(is_array($fileList)) {
		$fileTitles = array("#","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia","Estado","Reporte");
		$size=count($fileList);
		if($size>100){
			$pages=round($size/10)+1;
		}
		else{
			$pages=10;
		}
		$smarty->register('fileList,fileTitles,pages');
	}
	$smarty->assign('container','none');
	$smarty->display('helpers/reports/assistances/loadAssistancesbyDealer.'.$_SESSION['language'].'.tpl');
}
?>
