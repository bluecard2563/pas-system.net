<?php
/**
 * File: loadDealersNonPaidAssistance
 * Author: Patricio Astudillo M.
 * Creation Date: 23/02/2012 12:08:13 PM
 * Last Modified: 23/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_ubd');
	
	$dealer_list = File::getDealerWithNonPaidInvoicesAndAssistance($db, $serial_ubd);

	$smarty->register('dealer_list,serial_ubd');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/reports/assistances/loadDealersNonPaidAssistance.'.$_SESSION['language'].'.tpl');
?>
