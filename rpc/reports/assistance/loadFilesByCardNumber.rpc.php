<?php
/*
File: loadFilesByCardNumber.rpc
Author: David Bergmann
Creation Date: 15/12/2010
Last Modified:
Modified By:
*/


$card_number = $_POST['card_number'];
$file = new File($db);
$data = $file->getFilesByCard($card_number);

if(is_array($data)){
    //Customer information
    $titles = array("Expediente No.","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de asistencia","Estado","Reporte");

    foreach($data as &$d) {
        $d['name_cou']=$d['name_cou'];
        $d['name_cit']=$d['name_cit'];
        $d['name_cus']=$d['name_cus'];
        $d['diagnosis_fle']=$d['diagnosis_fle'];
    }

    $smarty->register('titles,data');
}

$smarty->register("card_number");
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadFilesByCardNumber.'.$_SESSION['language'].'.tpl');
?>