<?php
/**
 * File: loadResponsiblesNonPaidAssistance
 * Author: Patricio Astudillo M.
 * Creation Date: 23/02/2012 11:46:09 AM
 * Last Modified: 23/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_mbc');
	
	$responsible_list = UserByDealer::getResponsiblesWithNonPaidInvoicesAndAssistance($db, $serial_mbc);

	$smarty->register('responsible_list,serial_mbc');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/reports/assistances/loadResponsiblesNonPaidAssistance.'.$_SESSION['language'].'.tpl');

?>
