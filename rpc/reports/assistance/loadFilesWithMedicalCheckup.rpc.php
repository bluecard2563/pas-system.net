<?php
/*
File: loadFilesWithMedicalCheckup.rpc
Author: David Bergmann
Creation Date: 16/12/2010
Last Modified: 23/12/2010
Modified By: David Bergmann
*/


$card_number = $_POST['card_number'];
$file = new File($db);
$data = $file->getMedicalCheckupFilesByCard($card_number);

if(is_array($data)){
    $titles = array("Expediente No.","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de asistencia","Estado","Fecha de Auditor&iacute;a","Reporte");
    $smarty->register('titles,data');
}

$smarty->register("card_number");
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadFilesWithMedicalCheckup.'.$_SESSION['language'].'.tpl');
?>