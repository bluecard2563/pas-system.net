<?php
/*
File: loadDealerByCityByResponsible.rpc
Author: David Bergmann
Creation Date: 30/11/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
Request::setString('serial_mbc');
Request::setString('serial_usr');

$dealerList = Dealer::getDealerByCityByResponsible($db, $serial_cit, $serial_usr, $serial_mbc);

$smarty->register('serial_cit,serial_mbc,serial_usr,dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadDealerByCityByResponsible.'.$_SESSION['language'].'.tpl');
?>