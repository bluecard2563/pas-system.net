<?php
/**
 * File: loadAssistancesWithNonPaidInvoice
 * Author: Patricio Astudillo M.
 * Creation Date: 22/02/2012 
 * Last Modified: 22/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_cou');
	Request::setString('serial_mbc');
	Request::setString('serial_usr');
	Request::setString('serial_dea');
	Request::setString('card_number');

	if(!$card_number){
		$file_list = File::getAssistancesWithNonPaidInvoices($db, true, $serial_cou, $serial_mbc, $serial_usr, $serial_dea);
	}else{
		$sale = new Sales($db);
		$sale->setCardNumber_sal($card_number);
		$sale->getDataByCardNumber();
		$aux_data = $sale->getAux();
		
		$file_list = File::getAssistancesWithNonPaidInvoicesBySale($db, $sale->getSerial_sal(), $aux_data['serial_trl']);
	}
	
	$smarty->register('file_list');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/reports/assistances/loadAssistancesWithNonPaidInvoice.'.$_SESSION['language'].'.tpl');
?>
