<?php
/*
File: loadFileByNumber.rpc
Author: David Bergmann
Creation Date: 29/11/2010
Last Modified:
Modified By:
*/

$data = File::getFileByNumber($db,$_POST['file_num']);

if($data) {
	$titles = array("#","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de asistencia","Estado","Reporte");
	$smarty->register("data,titles");
}

$smarty->assign('container','none');
$smarty->display('helpers/reports/assistances/loadFileByNumber.'.$_SESSION['language'].'.tpl');
?>