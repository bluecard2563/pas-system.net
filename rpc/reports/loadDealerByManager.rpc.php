<?php
/*
File: loadDealerByManager.rpc
Author: David Bergmann
Creation Date: 26/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
Request::setString('opt_text');
$notRequired=$_POST['notRequired'];
if($notRequired){
	$opt_text = 'Todos';
}
$dealer=new Dealer($db);

$params=array(
            '0'=>array('field'=> 'd.serial_mbc',
                       'value'=> $serial_mbc,
                       'like'=> 1),
            '1'=>array('field'=> 'd.dea_serial_dea',
                       'value'=> 'NULL',
                       'like'=> 1),
            '2'=>array('field'=> 'd.status_dea',
                       'value'=> 'ACTIVE',
                       'like'=> 1),
            '3'=>array('field'=> 'dtl.serial_lang',
                       'value'=> $_SESSION['serial_lang'],
                       'like'=> 1),
			'4'=>array('field'=>'d.phone_sales_dea',
								'value'=> 'NO',
								'like'=> 1)
);

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$aux=array('field'=>'d.serial_dea',
	  'value'=> $_SESSION['dea_serial_dea'],
	  'like'=> 1);
	array_push($params,$aux);
}
$dealerList=$dealer->getDealers($params);

if(is_array($dealerList)) {
    $smarty->register('dealerList');
}

$smarty->register('serial_mbc,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/reports/loadDealerByManager.'.$_SESSION['language'].'.tpl');
?>