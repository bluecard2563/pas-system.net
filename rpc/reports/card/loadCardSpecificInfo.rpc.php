<?php
/*
File: loadCardSpecificInfo.rpc
Author: Santiago Borja
Creation Date: 28/05/2010
Last Modified:
Modified By:
*/

Request::setString('selCreditNote');
Request::setString('txtNumCard');
Request::setString('txtNumInvoice');
Request::setString('hdnClientID');
Request::setString('selManager');
Request::setString('selDocument');

$resultList = array();
switch($selCreditNote){
	case 'CARD':
		$sale = new Sales($db);
		$sale -> setCardNumber_sal($txtNumCard);
		$sale -> getDataByCardNumber();

		if($sale -> getSerial_sal()){
			$cnt = new Counter($db, $sale -> getSerial_cnt());
			$cnt -> getData();

			$usr = new User($db, $cnt -> getSerial_usr());
			$usr -> getData();

			$dealer = new Dealer($db, $cnt -> getSerial_dea());
			$dealer -> getData();

			$nameDea = $dealer -> getName_dea();
			$pbd = new ProductByDealer($db, $sale -> getSerial_pbd());
			$pbd -> getData();

			$pxc = new ProductByCountry($db, $pbd -> getSerial_pxc());
			$pxc -> getData();

			$pbl = new ProductbyLanguage($db, NULL, Language::getSerialLangByCode($db,$_SESSION['language']) ,$pxc -> getSerial_pro());
			$pbl -> getDataByProductAndLanguage();

			$cus = new Customer($db, $sale -> getSerial_cus());
			$cus -> getData();

			$inv = new Invoice($db, $sale -> getSerial_inv());
			$inv -> getData();
			
			$invoice_data = Invoice::getBasicInvoiceInformation($db, $sale -> getSerial_inv());
			if($invoice_data){
				$serial_mbc = $invoice_data['serial_mbc'];                                          
				$erp_bodega = ERPConnectionFunctions::getStoreID($serial_mbc); 	    
				$erp_pde = ERPConnectionFunctions::getDepartmentID($serial_mbc);

				if (strlen($erp_bodega) > 10){
					$number_inv = substr($invoice_data['number_inv'], 1);
					$cadena = $erp_bodega.$erp_pde.str_pad($number_inv, 9, "0", STR_PAD_LEFT);
				}else{
					$cadena = $erp_bodega.$erp_pde.str_pad($invoice_data['number_inv'], 9, "0", STR_PAD_LEFT)."";
				}   

				$printInvoiceData['total_number_inv'] = $cadena;
				$printInvoiceData['document'] = $invoice_data['holder_document'];
				
				//Format date_inv from d/m/Y to Y-m-d before sent int the array.
			    $date_inv = DateTime::createFromFormat('d/m/Y', $inv->getDate_inv())->format('Y-m-d');
			}
			
			/*if($sale['serial_inv'] == null){
				 $date_inv = null;
			}
		    else{
				$date_inv = date_format(DateTime::createFromFormat('d/m/Y', $inv -> getDate_inv()), 'Y-m-d');
			}*/

			
			$resultList[] = array(
				'serial_sal' => $sale -> getSerial_sal(),
				'num_card' => $txtNumCard,
				'cus_name' => $cus -> getFirstname_cus().' '.$cus -> getLastname_cus(),
				'prod_name' => $pbl -> getName_pbl(),
				'status' => $global_salesStatus[$sale -> getStatus_sal()],
				'num_inv' => ($sale->getFree_sal()=='YES')?'FREE':$inv -> getNumber_inv(),
				'status_inv' => $inv -> getStatus_inv(),
				'user_name' => $usr -> getFirstname_usr() .' '. $usr -> getLastname_usr(),
				'liq_number' => AppliedBonusLiquidation::getLiquidationActInfo($db, $sale -> getSerial_sal()),
				'erp_number_inv' => $printInvoiceData['total_number_inv'],
				'erp_holder_document' => $printInvoiceData['document'],
                'date_inv' => $date_inv
			);
		}else{
			//no existe
		}
		break;
	case 'INVOICE':
		$inv = new Invoice($db);
		$serial_inv = $inv -> getInvoiceForCardReport($txtNumInvoice, $selManager, $selDocument);
		$serial_inv = $serial_inv['serial_inv'];
		
		$invoice_data = Invoice::getBasicInvoiceInformation($db, $serial_inv);
		if($invoice_data){
			$serial_mbc = $invoice_data['serial_mbc'];                                          
			$erp_bodega = ERPConnectionFunctions::getStoreID($serial_mbc); 	    
			$erp_pde = ERPConnectionFunctions::getDepartmentID($serial_mbc); 	                                         

			if (strlen($erp_bodega) > 10){
				$number_inv = substr ($invoice_data['number_inv'], 1);
				$cadena = $erp_bodega.$erp_pde.str_pad($number_inv, 9, "0", STR_PAD_LEFT);
			}else{
				$cadena = $erp_bodega.$erp_pde.str_pad($invoice_data['number_inv'], 9, "0", STR_PAD_LEFT)."";
			}   

			$printInvoiceData['total_number_inv'] = $cadena;
			$printInvoiceData['document'] = $invoice_data['holder_document'];
		}

		if($serial_inv){
			$sal = new Sales($db);
			$sales = $sal -> getSalesByInvoice($serial_inv);
			foreach ($sales as $sale){
				$cnt = new Counter($db, $sale['serial_cnt']);
				$cnt -> getData();

				$usr = new User($db, $cnt -> getSerial_usr());
				$usr -> getData();

				$inv = new Invoice($db, $serial_inv);
				$inv -> getData();

                //Format date_inv from d/m/Y to Y-m-d before sent int the array.
                $date_inv = DateTime::createFromFormat('d/m/Y', $inv->getDate_inv())->format('Y-m-d');
				$resultList[] = array(
					'serial_sal' => $sale['serial_sal'],
					'num_card' => $sale['card_number_sal'],
					'cus_name' => $sale['customer_name'],
					'prod_name' => $sale['name_pbl'],
					'status' => $global_salesStatus[$sale['status_sal']],
					'num_inv' => $inv -> getNumber_inv(),
					'status_inv' => $inv -> getStatus_inv(),
					'user_name' => $usr -> getFirstname_usr() .' '. $usr -> getLastname_usr(),
					'liq_number' => AppliedBonusLiquidation::getLiquidationActInfo($db, $sale['serial_sal']),
					'erp_number_inv' => $printInvoiceData['total_number_inv'],
					'erp_holder_document' => $printInvoiceData['document'],
                    'date_inv' => $date_inv
				);
			}
		}

		break;
	case 'CUSTOMER':
		$sales = Sales::getCardsByCustomerReport($db, $hdnClientID);
		if($sales){
			foreach ($sales as $sale){
				$cus = new Customer($db, $hdnClientID);
				$cus -> getData();

				$cnt = new Counter($db, $sale['serial_cnt']);
				$cnt -> getData();

				$usr = new User($db, $cnt -> getSerial_usr());
				$usr -> getData();

				$inv = new Invoice($db, $sale['serial_inv']);
				$inv -> getData();
				
				$invoice_data = Invoice::getBasicInvoiceInformation($db, $inv -> getSerial_inv());
				if($invoice_data){
					$serial_mbc = $invoice_data['serial_mbc'];                                          
					$erp_bodega = ERPConnectionFunctions::getStoreID($serial_mbc); 	    
					$erp_pde = ERPConnectionFunctions::getDepartmentID($serial_mbc); 	                                         

					if (strlen($erp_bodega) > 10){
						$number_inv = substr ($invoice_data['number_inv'], 1);
						$cadena = $erp_bodega.$erp_pde.str_pad($number_inv, 9, "0", STR_PAD_LEFT);
					}else{
						$cadena = $erp_bodega.$erp_pde.str_pad($invoice_data['number_inv'], 9, "0", STR_PAD_LEFT)."";
					}   

					$printInvoiceData['total_number_inv'] = $cadena;
					$printInvoiceData['document'] = $invoice_data['holder_document'];
				}

                //Format date_inv from d/m/Y to Y-m-d before sent int the array.
                
//                $date_inv = DateTime::createFromFormat('d/m/Y', $inv -> getDate_inv())->format('Y-m-d');
                                if($sale['serial_inv'] == null){
                                    $date_inv2 = null;
                                }else{
                                 $date_inv2 = date_format(DateTime::createFromFormat('d/m/Y', $inv -> getDate_inv()), 'Y-m-d');
                                }
				$resultList[] = array(
					'serial_sal' => $sale['serial_sal'],
					'num_card' => $sale['card_number_sal'],
					'cus_name' => $cus -> getFirstname_cus().' '.$cus -> getLastname_cus(),
					'prod_name' => $sale['name_pbl'],
					'status' => $global_salesStatus[$sale['status_sal']],
					'num_inv' => ($sale['free_sal']=='YES')?'FREE':$inv -> getNumber_inv(),
					'status_inv' => $inv -> getStatus_inv(),
					'user_name' => $usr -> getFirstname_usr() .' '. $usr -> getLastname_usr(),
					'liq_number' => AppliedBonusLiquidation::getLiquidationActInfo($db, $sale['serial_sal']),
					'erp_number_inv' => $printInvoiceData['total_number_inv'],
					'erp_holder_document' => $printInvoiceData['document'],
                                        'date_inv' => $date_inv2
				);
			}
		}
		break;
}

$smarty->register('resultList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/card/loadCardSpecificInfo.'.$_SESSION['language'].'.tpl');
?>
