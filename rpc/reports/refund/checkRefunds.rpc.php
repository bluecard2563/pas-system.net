<?php
/*
File: checkRefunds.rpc.php
Author: Gabriela Guerrero
Creation Date: 31/05/2010
Last Modified:
Modified By:
*/
		$refunds = new Refund($db);

		if($_POST['flag'] == 2){
			$refundRaw = $refunds->getRefundsByManager( $_POST['serial_cou'],$_POST['serial_mbc'],
												$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'],
												$_POST['status_ref'],$_POST['serial_cit'],$_POST['serial_usr'],
												$_POST['serial_dea'],$_POST['serial_bra'],
												$_POST['dateFrom'],$_POST['dateTo']);
		}
		else{
			$refundRaw = $refunds->getRefundsByCustomer($_POST['serial_cus'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
		}
		
		if($refundRaw){
			echo json_encode(TRUE);
		}else{
			echo json_encode(FALSE);
		}
?>
