<?php 
/*
File: loadBranchesByDealer.rpc.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Last Modified: 05/07/2010
Modified By: David Bergmann
*/

$dea_serial_dea=$_POST['dea_serial_dea'];
$branch = new Dealer($db);

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$branchList = $branch->getBranchesWithRefunds($dea_serial_dea, $_SESSION['serial_dea']);
} else {
	$branchList = $branch->getBranchesWithRefunds($dea_serial_dea);
}

$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/refund/loadBranchesByDealer.'.$_SESSION['language'].'.tpl');
?>