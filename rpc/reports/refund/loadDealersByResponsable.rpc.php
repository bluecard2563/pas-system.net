<?php 
/*
File: loadDealersByResponsable.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Last Modified: 05/07/2010
Modified By: David Bergmann
*/

$serial_usr=$_POST['serial_usr'];
$dealer = new Dealer($db);

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList = $dealer->getDealersWithRefunds($serial_usr, $_SESSION['dea_serial_dea']);
} else {
	$dealerList = $dealer->getDealersWithRefunds($serial_usr);
}


$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/refund/loadDealersByResponsable.'.$_SESSION['language'].'.tpl');
?>