<?php 
/*
File: loadResponsablesByCity.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Last Modified: 
Modified By: 
*/

$serial_mbc=$_POST['serial_mbc'];
$user = new UserByDealer($db);
$responsibleList = $user->getResponsiblesByManagerWithRefunds($serial_mbc);

$smarty->register('responsibleList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/refund/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>