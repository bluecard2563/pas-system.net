<?php 
/*
File: loadCitiesByCountry.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$city = new City($db);
$cityList = $city->getCitiesWithRefunds($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty->register('cityList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/refund/loadCitiesByCountry.'.$_SESSION['language'].'.tpl');
?>