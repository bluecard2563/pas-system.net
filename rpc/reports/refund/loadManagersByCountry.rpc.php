<?php 
/*
File: loadManagersByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];

$mbc = new ManagerbyCountry($db);
$mbcList = $mbc->getManagersByCountryWithRefunds($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty->register('mbcList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/refund/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>