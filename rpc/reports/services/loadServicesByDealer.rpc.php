<?php 
/*
File: loadServicesByDealer.rpc.php
Author: Valeria Andino
Creation Date: 04/08/2016
Last Modified: 
Modified By: 
*/

$serial_dea=$_POST['serial_dea'];
$serial_cou=$_POST['serial_cou'];
 
$services = new ServicesByDealer($db);
$servicesList = $services->getServicesByDealer($serial_dea,$_SESSION['serial_lang'],$serial_cou);


$smarty->register('servicesList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/services/loadServicesByDealer.'.$_SESSION['language'].'.tpl');
?>