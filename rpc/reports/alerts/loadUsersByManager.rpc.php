<?php
/*
File: loadUsersByManager.rpc
Author: David Bergmann
Creation Date: 16/02/2011
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
$user = new User($db);

//Manager's users
if($serial_mbc) {
    $comissionistList=$user->getUsersByManager($serial_mbc, NULL, FALSE);

	//Planetassist users
	$mainComissionistList=$user->getMainManagerUsers();

	if(is_array($comissionistList)) {
		$comissionistList = array_merge($comissionistList, $mainComissionistList);
		sort($comissionistList);
	} else {
		$comissionistList = $mainComissionistList;
	}
}

$smarty->register('comissionistList,serial_mbc');
$smarty->assign('container','none');
$smarty->display('helpers/reports/alerts/loadUsersByManager.'.$_SESSION['language'].'.tpl');
?>