<?php
/*
* File: loadDealersByCity.php
* Author: David Bergmann
* Creation Date: 24/06/2010
* Last Modified:
* Modified By:
*/

$dealer=new Dealer($db);
$serial_cit = $_POST['serial_cit'];
if($serial_cit){
    $params=array('0'=>array('field'=> 'c.serial_cit',
                             'value'=> $serial_cit,
                             'like'=> 1),
                  '1'=>array('field'=> 'd.dea_serial_dea',
                             'value'=> 'NULL',
                             'like'=> 1),
                  '2'=>array('field'=> 'd.status_dea',
                             'value'=> 'ACTIVE',
                             'like'=> 1),
                  '3'=>array('field'=> 'dtl.serial_lang',
                             'value'=> $_SESSION['serial_lang'],
                             'like'=> 1),
				  '4'=>array('field'=>'d.phone_sales_dea',
									  'value'=> 'NO',
									  'like'=> 1));

	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}

    $dealerList=$dealer->getDealers($params);
}

$smarty->register('dealerList,serial_cit');
$smarty->assign('container','none');
$smarty->display('helpers/reports/credit_note/loadDealersByCity.'.$_SESSION['language'].'.tpl');
?>