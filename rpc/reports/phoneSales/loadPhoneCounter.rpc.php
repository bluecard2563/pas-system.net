<?php
/*
File: loadPhoneCounter.rpc
Author: David Bergmann
Creation Date: 20/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_dea');

if($serial_dea) {
	$counter = new Counter($db);
	$counterList = $counter->getCountersByBranch($serial_dea, true);

	if(is_array($counterList)) {
		foreach ($counterList as &$cl) {
			$cl['cnt_name'] = utf8_encode($cl['cnt_name']);
		}
	}
}

$smarty->register('counterList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/phoneSales/loadPhoneCounter.'.$_SESSION['language'].'.tpl');
?>