<?php
/*
File: loadDealersByManager.rpc
Author: Santiago Arellano
Creation Date: 04/07/2011
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
$dealer = new Dealer($db);

$params = array('0' => array(	'field'=> 'd.serial_mbc',
								'value'=> $serial_mbc,
								'like'=> 1),
				'1' => array(	'field'=> 'd.dea_serial_dea',
								'value'=> 'NULL',
								'like'=> 1),
				'2' => array(	'field'=> 'd.status_dea',
								'value'=> 'ACTIVE',
								'like'=> 1)
			);

$dealerList = $dealer ->getDealers($params);

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/system/login/loadDealersByManager.'.$_SESSION['language'].'.tpl');
?>