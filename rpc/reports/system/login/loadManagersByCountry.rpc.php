<?php
/*
File: loadManagersByCountry.rpc.php
Author: Santiago Arellano
Creation Date: 04/07/2011
Last Modified:
Modified By:
*/

$serial_cou=$_POST['serial_cou'];
$required = $_POST['required'];

$mbc = new ManagerbyCountry($db);
$mbcList = $mbc->getManagerByCountry($serial_cou, $_SESSION['serial_mbc']);

$smarty->register('mbcList,required');
$smarty->assign('container','none');
$smarty->display('helpers/reports/system/login/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>