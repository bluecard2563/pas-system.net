<?php
/*
File: loadCounter.rpc
Author: David Bergmann
Creation Date: 27/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_dea');

$counter = new Counter($db);
$counterList = $counter->getCountersByBranch($serial_dea);
$titles = array('#','Nombre Completo','Fecha de Nacimiento','Nombre de Usuario','Tel&eacute;fono','Correo electr&oacute;nico','Vende free ');

if(is_array($counterList)) {
    foreach ($counterList as &$cl) {
        $cl['cnt_name'] = utf8_encode($cl['cnt_name']);
        $cl['username_usr'] = utf8_encode($cl['username_usr']);
    }
    $smarty->register('counterList,titles');
}
//die(Debug::print_r($counterList));
$smarty->register('global_yes_no');
$smarty->assign('container','none');
$smarty->display('helpers/reports/loadCounter.'.$_SESSION['language'].'.tpl');
?>
