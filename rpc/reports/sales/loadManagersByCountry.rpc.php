<?php 
/*
File: loadManagersByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$mbc = new ManagerbyCountry($db);
if($_POST['type'] == 'analysis'){
	$mbcList = $mbc->getManagersSalesAnalysis($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
}
else{
	$mbcList = $mbc->getManagersWithFreeCard($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
}


$smarty->register('mbcList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>