<?php 
/*
File: loadDealersByManager.php
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Last Modified: 05/07/2010
Modified By: David Bergmann
*/

$serial_mbc=$_POST['serial_mbc'];
$opt_text=$_POST['opt_text'];
$dealer = new Dealer($db);

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList = $dealer->getDealersByManager($serial_mbc, $_SESSION['dea_serial_dea']);
} else {
	$dealerList = $dealer->getDealersByManager($serial_mbc);
}

$smarty->register('dealerList,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadDealersByManager.'.$_SESSION['language'].'.tpl');
?>