<?php
/*
 * File: loadCardsByCustomer.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 28/05/2010, 10:45:42 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 28/05/2010, 10:45:42 AM
 */

Request::setInteger('serial_cus');

if($serial_cus){
	$cardsInfo=Sales::getCardsByCustomerReport($db, $serial_cus);
}

$smarty->register('cardsInfo,serial_cus');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadCardsByCustomer.'.$_SESSION['language'].'.tpl');
?>
