<?php
/*
File: loadComissionistByCountry.rpc
Author: David Bergmann
Creation Date: 28/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');

$comissionist=new ComissionistByCountry($db);
$comissionistList=$comissionist->getActiveComissionistsByCountry($serial_cou);


if(is_array($comissionistList)){
	foreach($comissionistList as &$cl){
		$dl['first_name_usr']=utf8_encode($dl['first_name_usr']);
		$dl['last_name_usr']=utf8_encode($dl['last_name_usr']);
	}
        $smarty->register('comissionistList');
}

$smarty->register('serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadComissionistByCountry.'.$_SESSION['language'].'.tpl');
?>
