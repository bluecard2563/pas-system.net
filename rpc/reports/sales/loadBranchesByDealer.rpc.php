<?php 
/*
File: loadBranchesByDealer.php
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Last Modified: 
Modified By: 
*/

$dea_serial_dea=$_POST['dea_serial_dea'];
$branch = new Dealer($db);

if($_POST['type'] == 'analysis'){
	//If $_SESSION['serial_dea'] is set, loads only the user's dealer
	if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$branchList = $branch->getBranchesSalesAnalysis($dea_serial_dea, $_SESSION['serial_dea']);
	} else {
		$branchList = $branch->getBranchesSalesAnalysis($dea_serial_dea);
	}
}
else{
	//If $_SESSION['serial_dea'] is set, loads only the user's dealer
	if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$branchList = $branch->getBranchesByDealer($dea_serial_dea, $_SESSION['serial_dea']);
	} else {
		$branchList = $branch->getBranchesByDealer($dea_serial_dea);
	}
}

$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadBranchesByDealer.'.$_SESSION['language'].'.tpl');
?>