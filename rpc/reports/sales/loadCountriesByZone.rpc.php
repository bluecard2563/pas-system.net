<?php 
/*
File: loadCountriesByZone.rpc.php
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Last Modified: 
Modified By: 
*/

$serial_zon=$_POST['serial_zon'];
$country = new Country($db);
if($_POST['type'] == 'analysis'){
	$countryList = $country->getCountriesSalesAnalysis($serial_zon,$_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
}
else{
	$countryList = $country->getCountriesWithFreeCard($serial_zon,$_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
}
$smarty->register('countryList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadCountriesByZone.'.$_SESSION['language'].'.tpl');
?>