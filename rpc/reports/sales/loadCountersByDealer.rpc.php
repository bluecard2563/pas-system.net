<?php
/*
File: loadResponsibleByCity.rpc
Author: David Rosales
Creation Date: 05/03/2015
Last Modified:
Modified By:
*/

Request::setString('serial_dea');

if($serial_dea) {
    $counter = new Counter($db);
    $counterList = $counter->getCountersByBranch($serial_dea,true);
    if(is_array($counterList)) {
            foreach($counterList as &$c) {
                    $c['serial_usr'] = utf8_encode($c['cnt_name']);
            }
    }
}

$smarty->register('counterList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadCountersByDealer.'.$_SESSION['language'].'.tpl');
?>