<?php 
/*
File: loadResponsablesByCity.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Last Modified: 
Modified By: 
*/

$serial_cit=$_POST['serial_cit'];
$user = new UserByDealer($db);
$responsibleList = $user->getResponsiblesSalesAnalysis($serial_cit, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty->register('responsibleList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>