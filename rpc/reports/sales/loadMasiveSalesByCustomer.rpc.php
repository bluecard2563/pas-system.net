<?php
/*
File: loadMasiveSalesByCustomer.rpc
Author: David Bergmann
Creation Date: 19/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cus');
Request::setString('serial_sal');
$sale = new Sales($db);
$msales = $sale->getMasiveSalesReport($_SESSION['language'], $serial_cus, $serial_sal);

if(is_array($msales)) {
	foreach ($msales as &$m) {
		$m['name_pbl'] = utf8_encode($m['name_pbl']);
	}
	$titles = array('Tarjeta No.','Producto','Fecha de emisi&oacute;n','Fecha de Inicio','Fecha Fin','Estado');
}

$smarty->register('msales,titles,global_salesStatus,serial_sal');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadMasiveSalesByCustomer.'.$_SESSION['language'].'.tpl');
?>