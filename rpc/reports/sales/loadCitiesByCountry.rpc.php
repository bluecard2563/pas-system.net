<?php 
/*
File: loadCitiesByCountry.php
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$city = new City($db);
if($_POST['type'] == 'analysis'){
	$cityList = $city->getCitiesSalesAnalysis($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
}
else{
	$cityList = $city->getCitiesWithFreeCard($serial_cou, $_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
}

$smarty->register('cityList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadCitiesByCountry.'.$_SESSION['language'].'.tpl');
?>