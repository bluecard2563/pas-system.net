<?php
/*
File: loadProductsByCountry.rpc
Author: David Bergmann
Creation Date: 28/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');
Request::setString('reports');
Request::setString('multiple');

$product = new Product($db);
$productList = $product->getProductsByCountry($serial_cou, $_SESSION['serial_lang']);

$smarty->register('serial_cou,reports,productList,multiple');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadProductsByCountry.'.$_SESSION['language'].'.tpl');
?>
