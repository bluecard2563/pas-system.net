<?php
/*
File: loadResponsibleByCity.rpc
Author: David Bergmann
Creation Date: 28/05/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
Request::setString('reports');

$responsibleList = UserByDealer::getResponsiblesByCity($db, $serial_cit, $_SESSION['serial_mbc']);

$smarty->register('serial_cit,responsibleList,reports');
$smarty->assign('container','none');
$smarty->display('helpers/reports/sales/loadResponsibleByCity.'.$_SESSION['language'].'.tpl');
?>
