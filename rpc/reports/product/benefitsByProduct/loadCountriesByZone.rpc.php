<?php 
/*
File: loadCountriesByZone.rpc.php
Author: Gabriela Guerrero
Creation Date: 12/07/2010
Last Modified: 
Modified By: 
*/

$serial_zon=$_POST['serial_zon'];
$country = new Country($db);
$countryList = $country->getCountriesBenefitsByProduct($serial_zon,$_SESSION['countryList'],$_SESSION['serial_lang'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty->register('countryList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/product/benefitsByProduct/loadCountriesByZone.'.$_SESSION['language'].'.tpl');
?>