<?php 
/*
File: loadProductsByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 12/07/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$product = new Product($db);
$productList = $product->getProductsBenefitsByProduct($serial_cou,$_SESSION['serial_lang'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty->register('productList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/product/benefitsByProduct/loadProductsByCountry.'.$_SESSION['language'].'.tpl');
?>