<?php 
/*
File: loadCountriesByZone.rpc.php
Author: Gabriela Guerrero
Creation Date: 09/07/2010
Last Modified: 
Modified By: 
*/

$serial_zon=$_POST['serial_zon'];
$country = new Country($db);
$countryList = $country->getCountriesPricesByProduct($serial_zon,$_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty->register('countryList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/product/pricesByProduct/loadCountriesByZone.'.$_SESSION['language'].'.tpl');
?>