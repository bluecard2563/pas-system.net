<?php 
/*
File: loadProductsByCountry.rpc.php
Author: Gabriela Guerrero
Creation Date: 09/07/2010
Last Modified: 
Modified By: 
*/

$serial_cou=$_POST['serial_cou'];
$product = new Product($db);
$productList = $product->getProductsPricesByProduct($serial_cou,$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'], $_SESSION['language']);

$smarty->register('productList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/product/pricesByProduct/loadProductsByCountry.'.$_SESSION['language'].'.tpl');
?>