<?php
/*
 * File: loadDealersWithProducts.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 04-nov-2010, 13:29:43
 * Modified By: Patricio Astudillo
 */

Request::setString('serial_mbc');
if(User::isSubManagerUser($db, $_SESSION['serial_usr'])):
	$dealer = new Dealer($db, $_SESSION['serial_dea']);
	$dealer -> getData();
	
	$serial_dea = $dealer ->getDea_serial_dea();
endif;


$dealerList=Dealer::getDealersWithProducts($db, $serial_mbc, $serial_dea);

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/reports/productByDealer/loadDealersWithProducts.'.$_SESSION['language'].'.tpl');
?>
