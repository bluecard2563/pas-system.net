<?php 
/*
File: loadOversReportResult.rpc.php
Author: Santiago Borja
Creation Date: 31/05/2010 
Description: File which loads the overs report result
*/

	Request::setString('serialZone');
	Request::setString('serialCountry');
	Request::setString('serialMan');
	Request::setString('serialDea');
	
	$overs = Over::getFilteredOversReport($db, $serialMan, $serialDea, $serialCountry);
	
	$smarty->register('overs');
	$smarty->assign('container','none');
	$smarty->display('helpers/reports/overs/loadOversReportResult.'.$_SESSION['language'].'.tpl');
?>