<?php 
/*
File: loadOversReportResult.rpc.php
Author: Santiago Borja
Creation Date: 31/05/2010 
Description: File which loads the overs report result
*/

	Request::setString('serialZone');
	Request::setString('serialCountry');
	Request::setString('selAppTo');
	Request::setString('serialMan');
	Request::setString('serialResp');
	Request::setString('serialDealer');
	Request::setString('serialBranch');
	
	foreach($_POST as $key => $var){
		if($var == 'undefined' || $var == NULL){
			unset($_POST[$key]);
		}
	}
	
	$overs = Over::getFilteredPendingOversReport($db, $selAppTo, $serialMan, $serialDealer, $serialCountry, $serialResp,$serialBranch);
	
	if($overs){
		foreach($overs as &$ove){
			$evaluationYear = date("Y",strtotime($ove['begin_date_ino']));
			$ove['evaluationYear'] = $evaluationYear;
		}
	}
	
	$itemsPerPage = 2; //DECLARE THE VARIABLE
	$maxPages = ceil(count($overs)/$itemsPerPage);
	
	$smarty->register('overs,evaluationPeriod,maxPages,itemsPerPage');
	$smarty->assign('container','none');
	$smarty->display('helpers/reports/overs/loadPendingOversReportResult.'.$_SESSION['language'].'.tpl');
?>