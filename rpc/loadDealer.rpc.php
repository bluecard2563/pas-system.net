<?php 
/*
File: loadDealer.php
Author: Gabriela Guerrero
Creation Date: 24/06/2010
Last Modified: 05/07/2010
Modified By: David Bergmann
*/

$serial_man=$_POST['serial_man'];
$serial_cit=$_POST['serial_cit'];
$phone_sales=$_POST['phone_sales'];

$dealer = new Dealer($db);

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList = $dealer->getDealerByManager($serial_man, $serial_cit, $phone_sales, $_SESSION['dea_serial_dea']);
} else {
	$dealerList = $dealer->getDealerByManager($serial_man, $serial_cit, $phone_sales);
}

$smarty->register('dealerList,serial_cit');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealer.'.$_SESSION['language'].'.tpl');
?>