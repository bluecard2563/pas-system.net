<?php 
/*
* File: loadDealers.php
* Author: Patricio Astudillo
* Creation Date: 13/01/2010  11:07
* Last Modified: 15/03/2010
* Modified By: David Bergmann
* Description: File which loads all dealers acording a specific parameter.
* BreakRules: If you don't need to load the list of dealers, you have to
*            send 'stop' on any of the parameters.
*/

$dealer=new Dealer($db);
$params=array();
$paramItem=0;

$aux=array('field'=>'d.status_dea', 
		   'value'=> 'ACTIVE',
		   'like'=> 1);	
array_push($params,$aux);

if(!$_POST['phone_sales']){
	$aux=array('field'=>'d.phone_sales_dea',
		   'value'=> 'NO',
		   'like'=> 1);
}

array_push($params,$aux);

if($_POST['serial_cou']){
	$aux=array('field'=>'cou.serial_cou',
			   'value'=> $_POST['serial_cou'],
			   'like'=> 1);
	array_push($params,$aux);
}

if($_POST['serial_cit']){
	$aux=array('field'=>'c.serial_cit', 
			   'value'=> $_POST['serial_cit'],
			   'like'=> 1);
	array_push($params,$aux);
}

if($_POST['serial_sec']){
	$aux=array('field'=>'d.serial_sec', 
			  'value'=> $_POST['serial_sec'],
			  'like'=> 1);
	array_push($params,$aux);
}

if($_POST['serial_mbc']){
	$aux=array('field'=>'d.serial_mbc', 
			  'value'=> $_POST['serial_mbc'],
			  'like'=> 1);
	array_push($params,$aux);
}else{
	if($_SESSION['serial_mbc']!='1'){
		$aux=array('field'=>'d.serial_mbc',
			  'value'=> $_SESSION['serial_mbc'],
			  'like'=> 1);
		array_push($params,$aux);
	}
}
if($_POST['serial_usr']){
	$aux=array('field'=>'ubd.serial_usr',
			  'value'=> $_POST['serial_usr'],
			  'like'=> 1);
	array_push($params,$aux);
}

if($_POST['dea_serial_dea']){
	$aux=array('field'=>'d.dea_serial_dea', 
			  'value'=> $_POST['dea_serial_dea'],
			  'like'=> 1);
	array_push($params,$aux);
	
	/*For loading the select with ID & NAME 'selBranch'*/
	if($_POST['dea_serial_dea']!='NULL'){
		//If $_SESSION['serial_dea'] is set, loads only the user's branch
		if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$aux=array('field'=>'d.dea_serial_dea',
			  'value'=> $_SESSION['dea_serial_dea'],
			  'like'=> 1);
			array_push($params,$aux);
		}
		$branch=1;
	} else {
		//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
		if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
			$aux=array('field'=>'d.serial_dea',
			  'value'=> $_SESSION['dea_serial_dea'],
			  'like'=> 1);
			array_push($params,$aux);
		}
	}
	/*end if*/
} else {
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
}

/*Search for a 'stop' value*/
foreach($params as $p){
    if($p['value']=='stop'){
        $noData=1;
    }
}

/*Evaluates if the function needs to search in DB*/
if($noData!=1){
    $dealerList=$dealer->getDealers($params);
}
Request::setString('opt_text');
//unset($dealerList[1]);
$smarty->register('dealerList,branch,params,noData,opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealers.'.$_SESSION['language'].'.tpl');
?>