<?php
/*
    Document   : loadComissionistByCity.rpc.php
    Created on : Apr 27, 2010, 3:04:24 PM
    Author     : H3Dur4k
    Description:
        get a list of comissionist by city.
*/
$user=new User($db);
$text = $_POST['text'];

if($_POST['serial_sel']){
	$serial_sel=$_POST['serial_sel'];
}
if($_POST['serial_cit']){
	$serial_cit=$_POST['serial_cit'];
        if($_POST['all']){
            $comissionistList=$user->getComissionistByCity($_POST['serial_cit']);
        }
        else{
            $comissionistList=$user->getComissionistByCity($_POST['serial_cit'],$_SESSION['cityList']);
        }
}

if(is_array($comissionistList)){
	foreach($comissionistList as &$cl){
		$cl['name_usr']=utf8_encode($cl['name_usr']);
	}
}

$smarty->register('comissionistList,serial_cit,serial_sel,text');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissionistByCity.'.$_SESSION['language'].'.tpl');
?>
