<?php
/*
    Document   : fUpdateTravel
    Created on : 20-may-2010, 11:02:45
    Author     : Nicolas
    Description:
        Retrieves all data to update a a travel register.
*/
Request::setInteger('0:serial_trl');
$error=0;
if(!$serial_trl){
	$error=1;
}else{
	//retrieve data of the registered travel
	$travelerLog=new TravelerLog($db,$serial_trl);
	$travelerLog->getData();
	$travelsRegistered=$travelerLog->getTravelLogBySale();
	$travelData=get_object_vars($travelerLog);
	unset($travelData['db']);
	//get cities for the travel cities select
	$city=new City($db,$travelerLog->getSerial_cit());
	$city->getData();
	$country=new Country($db);
	$countryList=$country->getCountry();
	$cityList=$city->getCitiesByCountry($city->getSerial_cou());
	$travelData['serial_cou']=$city->getSerial_cou();
	//Debug::print_r($travelData);
	//check if the customer info is neccesary
	if($travelerLog->getSerial_cnt()!='' && $travelerLog->getCard_number_trl()!=''){
		$customer=new Customer($db,$travelerLog->getSerial_cus());
		$customer->getData();
		$typesList=$customer->getAllTypes();
		$smarty->register('typesList');
		$customerData=get_object_vars($customer);
		unset($customerData['db']);
		$cityListCustomer=$city->getCitiesByCountry($customerData['auxData']['serial_cou']);
		//Debug::print_r($customerData);
	}
	/*QUESTIONS*/
    $language=new Language($db);
    $sessionLanguage=$language->getSerialByCode($_SESSION['language']);
    $question=new Question($db);
    $maxSerial=$question->getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
    /*END QUESTIONS*/
	/*PARAMETERS*/
    $parameter=new Parameter($db,1); // --> PARAMETER FOR ELDER QUESTIONS
    $parameter->getData();
    $parameterValue=$parameter->getValue_par(); //value of the paramater used to display or not the questions
    $parameterCondition=$parameter->getCondition_par(); //condition of the paramater used to display or not the questions
	//check if the register is active
	if($travelerLog->getStatus_trl()=='ACTIVE'){
		$sale=new Sales($db,$travelerLog->getSerial_sal());
		$sale->getData();
		$serial_cus=$sale->getSerial_cus();
		$days_available = $sale->getAvailableDays();
		$cardInfo=$sale->getCardDaysAvailability();
		$productByDealer=new ProductByDealer($db, $sale->getSerial_pbd());
		$productByDealer->getData();
		$productByCountry= new ProductByCountry($db,$productByDealer->getSerial_pxc());
		$productByCountry->getData();
		$product = new Product($db,$productByCountry->getSerial_pro());
		$product->getData();
		$productData=get_object_vars($product);
		unset($productData['db']);
		//check if the card should validate the travel dates to be higher than the last travel end_date_sal
		//Debug::print_r($travelsRegistered);
		if($productData['third_party_register_pro'] == 'NO'){
			if(sizeof($travelsRegistered)>1){
				$date_last_travel=$travelsRegistered[0]['end_trl'];
			}
			else{
				$date_last_travel=date('d/m/Y');
			}
		}
		if($product->getDestination_restricted_pro() == "YES") {
			$destinationRestricted = "YES";
			$smarty->register('destinationRestricted');
		}
	}else{
		$error=2;
	}
}
//Debug::print_r($productData);
$today=date('d/m/Y');
$smarty->register('serial_cus,questionList,today,travelData,cardInfo,customerData,productData,countryList,cityList,cityListCustomer,date_last_travel,days_available,parameterValue,parameterCondition');
$smarty->display();
?>
