<?php
/*
File: fNewSale.php
Author: Esteban Angulo
Creation Date: 22/02/2010
Modified By: Patricio Astudillo
Last Modified: 24/03/2010
*/

//check if who is trying to sell is a counter.
$counter=new Counter($db);
$serial_cnt=$counter->getCounters($_SESSION['serial_usr']);
$sellFree=$_SESSION['sellFree'];
$parameter= new Parameter($db);
$parameter->setSerial_par('7');
$parameter->getData();
$age=$parameter->getValue_par();
$planetUser=0;
$user=new User($db);
$user->setSerial_usr($_SESSION['serial_usr']);
$user->getData();
//$planetUser=$user->isPlanetAssistUser();
$planetUser=$user->getBelongsto_usr();

if(count($serial_cnt)==1){
    $serial_cnt=$serial_cnt['0']['serial_cnt'];
    //die($serial_cnt);
	$sale=new Sales($db);
	$ofCounter=$sale->isDirectSale($serial_cnt);
    $counter=new Counter($db,$serial_cnt);
    $counter->getData();
    $stock=new Stock($db);
    //check for the next available number from vritual stock
    $nextAvailableNumber=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
    if(!$nextAvailableNumber){
        $stock->autoAssign($serial_cnt);
        $nextAvailableNumber=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
    }
    $country=new Country($db);
    $city= new City($db);
    $dealer=new Dealer($db);

    $countryList=$country->getCountry();

    //PRE-LOAD INFORMATION
    //CounterName
    $data['counterName']=$_SESSION['user_name'];

    //Dealer Data
    //$deaData=Dealer::getDealerCity($db, $_SESSION['serial_usr']);
	$deaData=Dealer::getDealerCity($db, $serial_cnt);
    $data['dealerOfUser']=$counter->getSerial_dea();
	//Country currencies
	$curBycountry=new CurrenciesByCountry($db);
	$data['currencies']=$curBycountry->getCurrenciesDealersCountry($counter->getSerial_dea());
	$data['currenciesNumber']=sizeof($data['currencies']);
    
    $dealer->setSerial_dea($data['dealerOfUser']);
    if($dealer->getData()){
		$data['name_dea']=$dealer->getName_dea();
		$data['serialDealer']=$dealer->getDea_serial_dea();

		/*products by Dealer*/
		$productByDealer= new ProductByDealer($db);
		$productListByDealer=$productByDealer->getproductByDealer($data['serialDealer'], $_SESSION['serial_lang'], 'NO', 'ACTIVE');

		//Services by dealer
		$servicesByDealer= new ServicesByDealer($db);
		if(ServicesByDealer::serviceByCountryExist($db, $deaData['serial_cou'])){
			$servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $deaData['serial_cou']);
		}else{
			$servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
		}
	}
	else{
		  http_redirect('main/sales/13');
	}


    //If the dealer doesn't have any products to sale,
    //he can't see the sales interface.
    if(!is_array($productListByDealer)){
        http_redirect('main/sales/3'); 
    }

    //Makes Seller Code
    $sellerCode=$deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer->getCode_dea().'-'.$data['serialDealer'].'-'.$serial_cnt;

    //EmissionCity
    $data['emissionPlace']=$deaData['name_cit'];

    //EmissionDate
    $data['emissionDate']=date("d/m/Y");


    //Customer type
    $customer=new Customer($db);
    $typesList=$customer->getAllTypes();

    /*QUESTIONS*/
    $language=new Language($db);
    $sessionLanguage=$language->getSerialByCode($_SESSION['language']);
    $question=new Question($db);
    $maxSerial=$question->getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
    /*END QUESTIONS*/

    /*PARAMETERS*/
    $parameter=new Parameter($db,1); // --> PARAMETER FOR ELDER QUESTIONS
    $parameter->getData();
    $elderlyAgeLimit=$parameter->getValue_par(); //value of the paramater used to display or not the questions
    /*END PARAMETERS*/

    //Get the Country Serial for calculate the price
    //of the product to be sold.
    $serial_cou=$deaData['serial_cou'];
    $smarty->register('countryList,data,nextAvailableNumber,productListByDealer,servicesListByDealer,sellerCode,typesList,questionList,elderlyAgeLimit,serial_cou,serial_cnt,maxSerial,hasDealers,ofCounter');
}else{
    if(count($serial_cnt)==0){
        //Only a Counter can sell, other users go to main page
         http_redirect('main/sales/2');
    }else{
	    //Customer type
	    $customer=new Customer($db);
	    $typesList=$customer->getAllTypes();
	
	    /*QUESTIONS*/
	    $language=new Language($db);
	    $sessionLanguage=$language->getSerialByCode($_SESSION['language']);
	    $question=new Question($db);
	    $maxSerial=$question->getMaxSerial($sessionLanguage);
	    $questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
	    /*END QUESTIONS*/
	
	    /*PARAMETERS*/
	    $parameter=new Parameter($db,1); // --> PARAMETER FOR ELDER QUESTIONS
	    $parameter->getData();
	    $elderlyAgeLimit=$parameter->getValue_par(); //value of the paramater used to display or not the questions
	    /*END PARAMETERS*/
	
	    $hasDealers=count($serial_cnt);
		$country=new Country($db);
		$countryList=$country->getCountry();
	
	    $smarty->register('questionList,elderlyAgeLimit,serial_cou,serial_cnt,hasDealers,countryList,typesList');
    }
}
$parameter = new Parameter($db);
$parameter->setSerial_par('14');
$parameter->getData();
$maxCoverTime=$parameter->getValue_par();


$smarty->register('sellFree,age,planetUser,maxCoverTime');
$smarty->display();
?>