<?php

	/*
	Document		:	pLoadCustomersMasive.php
	Author			:	Miguel Ponce
	Modified date	:	24/12/2009
	Last modified	:	24/12/2009
	Description		:	Uploads a xls or xlsx file with a list of customers
	*					reads it and insert them into the database if they don�t exist.
	*					Then it links the customers with a travelers log.
	*/

	$path_info = pathinfo($_FILES['travelers_list']['name']);
	$ext = $path_info['extension'];

	$serial_sal=$_POST['hdnSubSaleID'];
	$sale = new Sales($db, $serial_sal);
	$sale->getData();

	$travelerLog = new TravelerLog($db);
	$travelerLog->setSerial_sal($serial_sal);

	$productByDealer=new ProductByDealer($db,$sale->getSerial_pbd());
	$productByDealer->getData();

	$productByCountry=new ProductByCountry($db, $productByDealer->getSerial_pxc());
	$productByCountry->getData();

	$product=new Product($db,$productByCountry->getSerial_pro());
	$product->getData();

	if($_FILES['travelers_list']['name']){
		if($ext!='xls' && $ext!='xlsx'){
			$error=2;//error, el archivo no es excel
		}else{
			$prefix = substr(md5(uniqid(rand())),0,6);
			$fileName = $prefix.'_'.$_FILES["travelers_list"]['name'];
			$path=''.$fileName;
			if(copy($_FILES['travelers_list']['tmp_name'],$path)){
				require_once ("PHPExcel.php");
				require_once ("PHPExcel/Reader/Excel5.php");
				require_once ("PHPExcel/Reader/Excel2007.php");

				if($ext=='xls'){
					$objReader = new PHPExcel_Reader_Excel5();
				}else{
					$objReader = new PHPExcel_Reader_Excel2007();
				}

				$objReader->setReadDataOnly(true);

				ini_set('memory_limit', '512M');
				ini_set('max_execution_time','3600');
				if (file_exists($path)) {
					$objPHPExcel = @$objReader->load($path);
				}
				//$objPHPExcel = @$objReader->load('../../'.$path);

				$row=1;//Empieza en 1 porque la cabecera de t�tulos
				$iteration=0;

				while(!isEmpty($objPHPExcel->getActiveSheet(),++$row)){
					if($objPHPExcel->getActiveSheet()->getCell(A.$row)->getValue() == ($row-1)){
						$data['serial_cus_xls'][$row] = especialCharFix($objPHPExcel->getActiveSheet()->getCell(A.$row)->getValue());
					}
					else{
						$error=4; // secuence error
						break;
					}
					if($objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue()=="" || $objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue() < ($row-1)){
						if($objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue()!="") {
							if($product->getMax_extras_pro() >0) {
								$data['cus_serial_cus_xls'][$row] =	$objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue();
							} else {
								$error=5;// extras not acepted
								break;
							}
						} else {
							$data['cus_serial_cus_xls'][$row] =	$objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue();
						}

					}
					else{
						$error=6; // extras secuence error
						break;
					}
					$data['document_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(C.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(C.$row)->getValue()) : NULL;

					$first_name_cus=$objPHPExcel->getActiveSheet()->getCell(D.$row)->getValue();
					if ( eregi('^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$',$first_name_cus)){
						$data['first_name_cus'][$row] = $first_name_cus;
					}
					else{
						$error=7; // first name error
						break;
					}

					$last_name_cus=$objPHPExcel->getActiveSheet()->getCell(E.$row)->getValue();
					if ( eregi('^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$',$first_name_cus)){
						$data['last_name_cus'][$row] = $last_name_cus;
					}
					else{
						$error=8; // last name error
						break;
					}

					$data['phone1_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(F.$row)->getValue();
					$data['phone2_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(G.$row)->getValue();
					$data['cellphone_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(H.$row)->getValue();
					$data['email_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(I.$row)->getValue();
					$data['address_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(J.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(J.$row)->getValue()) : NULL;
					$data['relative_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(K.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(K.$row)->getValue()) : NULL;
					$data['relative_phone_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(L.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(L.$row)->getValue()) : NULL;

					//Valisate date
					$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(M.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(M.$row)->getValue()) : NULL;
					$timeStamp = mktime(0,0,0,1,$excelWinDate-1,1900);
					if($excelWinDate!=NULL){


						$age = calculateAge($timeStamp);
						$param = new Parameter($db,'1');
						$param->getData();
						$seniorAgeParam = $param->getValue_par();

						//echo $product->getSenior_pro()." && ".$age.">=".$seniorAgeParam;
						if($product->getSenior_pro() == 'NO' && $age>=$seniorAgeParam) {
							$error = 13; //seniors not accepted
							break;
						} else {
							$childAgeParam = new Parameter($db,'7');
							$childAgeParam->getData();
							$childAge = $childAgeParam->getValue_par();
							if($product->getExtras_restricted_to_pro() == "CHILDREN") {
								if($age<$childAge) {
									$data['birthdate_cus'][$row] =	date("Y-m-d",$timeStamp);
								} else {
									$error = 9; //only children accepted
									break;
								}
							} elseif($product->getExtras_restricted_to_pro() == "ADULT") {
								if($age>=$childAge) {
									$data['birthdate_cus'][$row] =	date("Y-m-d",$timeStamp);
								} else {
									$error = 10; //only adults accepted
									break;
								}
							} else {
								$data['birthdate_cus'][$row] =	date("Y-m-d",$timeStamp);
							}
						}

					}
					else{
						$data['birthdate_cus'][$row] = NULL;
					}

					$data['serial_cit'][$row] = $objPHPExcel->getActiveSheet()->getCell(N.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(N.$row)->getValue()) : NULL;

					$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(O.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(O.$row)->getValue()) : NULL;
					$timeStamp = mktime(0,0,0,1,$excelWinDate-1,1900);
					$data['start_trl'][$row] = date("Y-m-d",$timeStamp);

					$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(P.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(P.$row)->getValue()) : NULL;
					$timeStamp = mktime(0,0,0,1,$excelWinDate-1,1900);
					$data['end_trl'][$row] = date("Y-m-d",$timeStamp);
					$data['relationship_ext'][$row] = $objPHPExcel->getActiveSheet()->getCell(Q.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(Q.$row)->getValue()) : NULL;
				}

				if(!$error) {
					$customer = new Customer($db);
					$array=$customer->loadData($data);

					//Inserts non registered customers in DB
					if(is_array($array['insert']['document_cus'])){
						foreach($array['insert']['document_cus'] as $key=>$document_cus){
							$count++;
							$customer->setDocument_cus($array['insert']['document_cus'][$key]);
							$customer->setFirstname_cus($array['insert']['first_name_cus'][$key]);
							$customer->setLastname_cus($array['insert']['last_name_cus'][$key]);
							$customer->setPhone1_cus($array['insert']['phone1_cus'][$key]);
							$customer->setPhone2_cus($array['insert']['phone2_cus'][$key]);
							$customer->setCellphone_cus($array['insert']['cellphone_cus'][$key]);
							$customer->setEmail_cus($array['insert']['email_cus'][$key]);
							$customer->setAddress_cus($array['insert']['address_cus'][$key]);
							$customer->setRelative_cus($array['insert']['relative_cus'][$key]);
							$customer->setRelative_phone_cus($array['insert']['relative_phone_cus'][$key]);
							$customer->setBirthdate_cus(date('d/m/Y',strtotime($array['insert']['birthdate_cus'][$key])));
							$customer->setSerial_cit($array['insert']['serial_cit'][$key]);
							//Inserts customers that aren't in the DB yet.
							$serial_cus=$customer->insert();
							
							ERP_logActivity('new', $customer);//ERP ACTIVITY
							
							//Links customers with sales
							$travelerLog->setSerial_cus($serial_cus);
							$travelerLog->setCard_number_trl($count);
							$travelerLog->setStart_trl($sale->getBeginDate_sal());
							$travelerLog->setEnd_trl($sale->getEndDate_sal());
							$travelerLog->setDays_trl($sale->getDays_sal());

							$serial_trl=$travelerLog->insert();
							
							if(!$serial_cus) {
								$error=11;// return error
								break;
							}
							if(!$serial_trl) {
								$error=12;// return error
								break;
							}
						}
					}
					if(is_array($array['exist'])){
						foreach($array['exist'] as $key=>$existCustomer){
							//Links customers with sales
							$travelerLog->setSerial_cus($existCustomer['serial_cus']);
							$travelerLog->setCard_number_trl($count);
							$travelerLog->setStart_trl($sale->getBeginDate_sal());
							$travelerLog->setEnd_trl($sale->getEndDate_sal());
							$travelerLog->setDays_trl($sale->getDays_sal());
							$serial_trl=$travelerLog->insert();

							if(!$serial_trl) {
								$error=12;// return error
								break;
							}

						}
					}
				}
			}else{
				$error=3;//error al subir el archivo
			}
		}
	} else {
		$error=2;
	}

	if(!$error)
		$error=1;
	unlink($path);
	http_redirect('modules/sales/fChooseMasiveSales/'.$error);

	function especialCharFix($text){
		return html_entity_decode(htmlentities($text,ENT_COMPAT, 'UTF-8'),ENT_COMPAT, 'iso-8859-1');
	}

	function isEmpty($sheet, $row){
		//echo ' wiii'.$sheet->getCell(A.$row)->getValue();
		if($sheet->getCell(A.$row)->getValue()!="" ||  //serial_cus_xls
		   $sheet->getCell(B.$row)->getValue()!="" ||  //cus_serial_cus_xls
		   $sheet->getCell(C.$row)->getValue()!="" ||  //document_cus
		   $sheet->getCell(D.$row)->getValue()!="" ||  //first_name_cus
		   $sheet->getCell(E.$row)->getValue()!="" ||  //last_name_cus
		   $sheet->getCell(F.$row)->getValue()!="" ||  //phone1_cus
		   $sheet->getCell(G.$row)->getValue()!="" ||  //phone2_cus
		   $sheet->getCell(H.$row)->getValue()!="" ||  //cellphone_cus
		   $sheet->getCell(I.$row)->getValue()!="" ||  //email_cus
		   $sheet->getCell(J.$row)->getValue()!="" ||  //address_cus
		   $sheet->getCell(K.$row)->getValue()!="" ||  //relative_cus
		   $sheet->getCell(L.$row)->getValue()!="" ||  //relative_phone_cus
		   $sheet->getCell(M.$row)->getValue()!="" ||  //birthdate_cus
		   $sheet->getCell(N.$row)->getValue()!="" ||  //serial_cit
		   $sheet->getCell(O.$row)->getValue()!="" ||  //start_trl
		   $sheet->getCell(P.$row)->getValue()!="" ||  //end_trl
		   $sheet->getCell(Q.$row)->getValue()!=""	   //relationship_ext
		){
			return false;
		}else{
			return true;
		}
	}
	function cellFilter($value){
		$value = str_replace (" ", "", $value);

		return $value;
	}

	function calculateAge($date) {
		//actual date
		$day=date(j);
		$month=date(n);
		$year=date(Y);

		//fecha de nacimiento

		$bDay=date("d",$date);
		$bMonth=date("m",$date);
		$bYear=date("Y",$date);


		//If bMonth is the same as month but bDay is smaller than day, we substract one year
		if (($bMonth == $month) && ($bDay > $day)) {
		$year=($year-1); }

		//if bMonth is biger than month, we substract one year
		if ($bMonth > $month) {
		$year=($year-1);}

		//we calculate the diference between year and bYear
		$age=($year-$bYear);
		return $age;
	}
?>