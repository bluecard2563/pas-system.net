<?php
/*
File:fRegisterTravel.php
Author: Nicolas Flores
Creation Date:09.04.2010
Modified By:
Last Modified: 
*/
Request::setInteger('0:serial_sal');
Request::setString('1:error');
Request::setString('2:success');
$sale=new Sales($db,$serial_sal);
$sale->getData();
$customer=new Customer($db,$sale->getSerial_cus());
$customer->getData();
$serial_cus=$sale->getSerial_cus();
$typesList=$customer->getAllTypes();
$country=new Country($db);
$countryList=$country->getCountry();
$travelLog= new TravelerLog($db);
$travelLog->setSerial_sal($serial_sal);
$travelsRegistered=$travelLog->getTravelLogBySale();
$sale = new Sales($db, $serial_sal);
$sale->getData();
$saleData=get_object_vars($sale);
unset($saleData['db']);
$productByDealer=new ProductByDealer($db, $sale->getSerial_pbd());
$productByDealer->getData();
$productByCountry= new ProductByCountry($db,$productByDealer->getSerial_pxc());
$productByCountry->getData();
$product = new Product($db,$productByCountry->getSerial_pro());
$product->getData();

if($product->getDestination_restricted_pro() == "YES") {
	$destinationRestricted = "YES";
	$country = new Country($db, $productByCountry->getSerial_cou());
	$country->getData();
	$serial_cou = $country->getSerial_cou();
	$smarty->register('destinationRestricted,serial_cou');
}

$productData=get_object_vars($product);
unset($productData['db']);
if($product->getGenerate_number_pro()=='NO'){
	$counter=new Counter($db, $sale->getSerial_cnt());
	$counter->getData();
	if($counter->getStatus_cnt()=='ACTIVE'){
		$serial_cnt = $counter->getSerial_cnt();
	}else{
		$serial_cnt= $counter->getCounterForCorpStock();
	}
	$stock=new Stock($db);
	//check for the next available number from vritual stock
    $nextAvailableNumber=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
    if(!$nextAvailableNumber){
        $stock->autoAssign($serial_cnt);
        $nextAvailableNumber=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
    }
}
$days_available = $sale->getAvailableDays();
/*QUESTIONS*/
    $language=new Language($db);
    $sessionLanguage=$language->getSerialByCode($_SESSION['language']);
    $question=new Question($db);
    $maxSerial=$question->getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
    /*END QUESTIONS*/
 /*PARAMETERS*/
    $parameter=new Parameter($db,1); // --> PARAMETER FOR ELDER QUESTIONS
    $parameter->getData();
    $parameterValue=$parameter->getValue_par(); //value of the paramater used to display or not the questions
    $parameterCondition=$parameter->getCondition_par(); //condition of the paramater used to display or not the questions
//check if the card should validate the travel dates to be higher than the last travel end_date_sal
if($productData['third_party_register_pro'] == 'NO'){
	if($travelsRegistered){
		$date_last_travel=$travelsRegistered[0]['end_trl'];
	}
	else{
		$date_last_travel=date('d/m/Y');
	}
}

$today=date('d/m/Y');
$smarty->register('serial_cus,error,success,today,date_last_travel,serial_cnt,serial_sal,maxSerial,questionList,parameterValue,parameterCondition,saleData,days_available,travelsRegistered,productData,typesList,countryList,nextAvailableNumber');
$smarty->display();
?>
