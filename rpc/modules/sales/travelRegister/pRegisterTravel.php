<?php
/*
File:pRegisterTravel.php
Author: Nicolas Flores
Creation Date:13.04.2010
Modified By:15.04.2010
Last Modified: 
*/
//Debug::print_r($_POST);
Request::setInteger('serialCus');
if($serialCus!='')
	$serial_cus=$serialCus;
else
	Request::setInteger('serial_cus');
if($serial_cus=='')
	die('error missing customer parameter');

$error=0;
//define a customer object to insert or update the customer who is being registered.
$customer =new Customer($db);
//check if the customer already exists
if($serial_cus){//if user exist , update his info
	$customer->setserial_cus($serial_cus);
	$customer->getData();
	$shouldUpdate=0;
	if($_POST['txtNameCustomer']){
		if($_POST['txtNameCustomer']!=$customer->getFirstname_cus()){
			$customer->setFirstname_cus($_POST['txtNameCustomer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtLastnameCustomer']){
		if($_POST['txtLastnameCustomer']!=$customer->getLastname_cus()){
			$customer->setLastname_cus($_POST['txtLastnameCustomer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['selCityCustomer']){
		if($_POST['selCityCustomer']!=$customer->getSerial_cit()){
			$customer->setSerial_cit($_POST['selCityCustomer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtAddress']){
		if($_POST['txtAddress']!=$customer->getAddress_cus()){
			$customer->setAddress_cus($_POST['txtAddress']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtPhone1Customer']){
		if($_POST['txtPhone1Customer']!=$customer->getPhone1_cus()){
			$customer->setPhone1_cus($_POST['txtPhone1Customer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtPhone2Customer']){
		if($_POST['txtPhone2Customer']!=$customer->getPhone2_cus()){
			$customer->setPhone2_cus($_POST['txtPhone2Customer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtCellphoneCustomer']){
		if($_POST['txtCellphoneCustomer']!=$customer->getCellphone_cus()){
			$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtMailCustomer']){
		if($_POST['txtMailCustomer']!=$customer->getEmail_cus()){
			$customer->setEmail_cus($_POST['txtMailCustomer']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtRelative']){
		if($_POST['txtRelative']!=$customer->getRelative_cus()){
			$customer->setRelative_cus($_POST['txtRelative']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtPhoneRelative']){
		if($_POST['txtPhoneRelative']!=$customer->getRelative_phone_cus()){
			$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtManager']){
		if($_POST['txtManager']!=$customer->getRelative_cus()){
			$customer->setRelative_cus($_POST['txtManager']);
			$shouldUpdate=1;
		}
	}
	if($_POST['txtPhoneManager']){
		if($_POST['txtPhoneManager']!=$customer->getRelative_phone_cus()){
			$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
			$shouldUpdate=1;
		}
	}
	if($customer->getStatus_cus()=='INACTIVE'){
		$customer->setStatus_cus()=='ACTIVE';
		$shouldUpdate=1;
	}
	//update the register
	if($shouldUpdate==1){
		if($customer->update()){
			ERP_logActivity('update', $customer);//ERP ACTIVITY
			
			$error=0;
		}else{
			$error=1;//error while updating
		}
	}
}else{//if customer doesn't exist , insert it
	if($_POST['txtDocumentCustomer']){
		$customer->setDocument_cus($_POST['txtDocumentCustomer']);
	}
	if($_POST['selType']){
		$customer->setType_cus($_POST['selType']);
	}

	if($_POST['txtNameCustomer']){
			$customer->setFirstname_cus($_POST['txtNameCustomer']);
	}
	if($_POST['txtLastnameCustomer']){
			$customer->setLastname_cus($_POST['txtLastnameCustomer']);
	}
	if($_POST['selCityCustomer']){
			$customer->setSerial_cit($_POST['selCityCustomer']);
	}
	if($_POST['txtBirthdayCustomer']){
			$customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
	}

	if($_POST['txtAddress']){
			$customer->setAddress_cus($_POST['txtAddress']);
	}
	if($_POST['txtPhone1Customer']){
			$customer->setPhone1_cus($_POST['txtPhone1Customer']);
	}
	if($_POST['txtPhone2Customer']){
			$customer->setPhone2_cus($_POST['txtPhone2Customer']);
	}
	if($_POST['txtCellphoneCustomer']){
			$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
	}
	if($_POST['txtMailCustomer']){
			$customer->setEmail_cus($_POST['txtMailCustomer']);
	}
	if($_POST['txtRelative']){
			$customer->setRelative_cus($_POST['txtRelative']);
	}
	if($_POST['txtPhoneRelative']){
			$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
			$customer->setRelative_cus($_POST['txtManager']);
	}
	if($_POST['txtPhoneManager']){
			$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}
		$customer->setStatus_cus('ACTIVE');
	$serial_cus=$customer->insert();
	
	ERP_logActivity('new', $customer);//ERP ACTIVITY
}
if($serial_cus){
//check if the user has entered answer, applies only in case the the customer is older person
	if($_POST['all_answered']==1){
		$language=new Language($db);
		$sessionLanguage=$language->getSerialByCode($_SESSION['language']);
		$question=new Question($db);
		$questionList = Question::getActiveQuestions($db, $sessionLanguage,$serial_cus);

		foreach($questionList as $q){
			if($q['serial_ans']){
				$answer=new Answer($db,$q['serial_ans']);
			}else{
				$answer=new Answer($db);
			}
			$answer->setSerial_cus($serial_cus);
			$answer->setSerial_que($q['serial_que']);
			$ans=$_POST['question_'.$q['serial_qbl']];
			if($ans){
				if($q['type_que']=='YN'){
					$answer->setYesno_ans($ans);
				}else{
					$answer->setAnswer_ans($ans);
				}

				if($q['yesno_ans'] or $q['answer_ans']){
					if($answer->update()){
						$flag=0;
					}else{
						$flag=1;
						break;
					}
				}else{
					if($answer->insert()){
						$flag=0;
					}else{
						$flag=1;
						break;
					}
				}
			}
		}
		if($flag==1)
			$error=3;//register error in case answers where not inserted
			$success=0;//register unsuccessful insert of travel
	}
	if($error!=3){
		//register the travel
			$sale=new Sales($db,$_POST['hdnSale']);
			$sale->getData();
			//check if a card number is required
			if($_POST['generates_card_number']==1){
				$counter=new Counter($db, $sale->getSerial_cnt());
				$counter->getData();
				if($counter->getStatus_cnt()=='ACTIVE'){
					$serial_cnt = $counter->getSerial_cnt();
				}else{
					$serial_cnt= $counter->getCounterForCorpStock();
				}
				$stock=new Stock($db);
				//check for the next available number from virtual stock
				$nextAvailableNumber=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
				if(!$nextAvailableNumber){
					$stock->autoAssign($serial_cnt);
					$nextAvailableNumber=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
				}
			}
			$travelLog=new TravelerLog($db);
			$travelLog->setSerial_sal($_POST['hdnSale']);
			$travelLog->setSerial_usr($_SESSION['serial_usr']);
			$travelLog->setSerial_cus($serial_cus);
			$travelLog->setSerial_cit($_POST['selCityDestination']);
			if($_POST['generates_card_number']==1){
				$travelLog->setCard_number_trl($nextAvailableNumber);
				$travelLog->setSerial_cnt($serial_cnt);
			}
			$travelLog->setStart_trl($_POST['txtDepartureDate']);
			$travelLog->setEnd_trl($_POST['txtArrivalDate']);
			$travelLog->setDays_trl($_POST['hddDays']);			
			$serial_trl=$travelLog->insert();
			if($serial_trl){
				$success=1;
			}else{
				$success=0;
			}
	}
}else{
	$error=2;//error while creating the client
	$success=0;
}
 http_redirect("main/travelerRegister/$error/$success/$serial_trl");
?>
