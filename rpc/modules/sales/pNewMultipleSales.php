<?php
/*
 * File: pNewMultipleSales.php
 * Author: Patricio Astudillo
 * Creation Date: 18-oct-2010, 15:52:50
 * Modified By: Patricio Astudillo
 */

if($_POST['customer_list']){
	$serial_cnt=$_POST['hdnCounter_id_toUse'];
	$serial_pbd=$_POST['selProduct'];

	$stock=new Stock($db);
	$extra=new Extras($db);
	$counter=new Counter($db, $serial_cnt);
	$counter->getData();
	$branch=new Dealer($db, $counter->getSerial_dea());
	$branch->getData();
	$geo_information=$branch->getAuxData_dea();
	$currency_data=CurrenciesByCountry::getOfficialCurrencyData($db, $geo_information['serial_cou']);

	$sale=new Sales($db);
	$sale->setSerial_cnt($serial_cnt);
	$sale->setSerial_pbd($serial_pbd);
	$sale->setFree_sal('NO');
	$sale->setObservations_sal('Multiple Sale');
	$sale->setStatus_sal('REGISTERED');
	$sale->setStockType_sal('VIRTUAL');
	$sale->setEmissionDate_sal(date('d/m/Y'));
	$sale->setType_sal('SYSTEM');
	$sale->setInternationalFeeStatus_sal('TO_COLLECT');
	$sale->setInternationalFeeUsed_sal('NO');
	$sale->setDirectSale_sal($sale->isDirectSale($serial_cnt));
	$sale->setSerial_cur($currency_data['serial_cur']);
	$sale->setChange_fee_sal($currency_data['exchange_fee_cur']);

	foreach($_POST['customer_list'] as $c){
		/* DATES */
		$start_trl=strtotime($c['start_trl']);
		$end_trl=strtotime($c['end_trl']);
		$travel_days=($end_trl-$start_trl)/ (60 * 60 * 24);

		$star_date=$c['start_trl'];
		$star_date=split('-', $star_date);
		$star_date=$star_date['2'].'/'.$star_date['1'].'/'.$star_date['0'];

		$end_date=$c['end_trl'];
		$end_date=split('-', $end_date);
		$end_date=$end_date['2'].'/'.$end_date['1'].'/'.$end_date['0'];
		/* DATES */

		/* CARD NUMBER */
		$card_number=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
		if(!$card_number){
			$stock->autoAssign($serial_cnt);
			$card_number=$stock->getNextStockNumber($serial_cnt,$migration_date_for_stock);
		}
		/* CARD NUMBER */

		$sale->setSerial_cit($c['destination_cit']);
		$sale->setSerial_cus($c['serial']);
		$sale->setBeginDate_sal($star_date);
		$sale->setEndDate_sal($end_date);
		$sale->setCardNumber_sal($card_number);
		$sale->setDays_sal($travel_days);
		$sale->setFee_sal($c['price']);
		$sale->setCost_sal($c['cost']);		
		$sale->setTotal_sal($c['total_price']);
		$sale->setTotalCost_sal($c['total_cost']);
		$sale->setInternationalFeeAmount_sal($c['total_cost']);
		$sales_id=$sale->insert();

		if($sales_id){
			if(is_array($c['extras'])){
				$extra->setSerial_sal($sales_id);
				
				foreach($c['extras'] as $e){
					$extra->setSerial_cus($e['serial']);
					$extra->setRelationship_ext($e['relationship']);
					$extra->setCost_ext($e['cost']);
					$extra->setFee_ext($e['price']);

					if(!$extra->insert()){
						$error=4;
						break;
					}
				}
				if(!$error){
					$error=1;
				}
			}else{
				$error=1;
			}
		}else{
			$error=3;
		}
	}

	http_redirect('main/multi_sales/'.$error);
}else{
	http_redirect('main/multi_sales/2');
}
?>
