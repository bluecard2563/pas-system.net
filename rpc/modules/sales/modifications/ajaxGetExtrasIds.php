<?php 
/*
File: ajaxGetExtras.php
Author: Santiago Borja
Creation Date: 18/04/2010
*/
	Request::setString('serialSale');
	
	$extras = Extras::getExtrasBySale($db, $serialSale);
	
	$arrayIds = array();
	if(is_array($extras)){
		foreach ($extras as $extra){
			$arrayIds[] =  $extra['document_cus'];
		}
	}
	
	if(true){
		echo utf8_encode(implode('%%%', $arrayIds));	
	}else{
		echo utf8_encode('false');
	}
?>