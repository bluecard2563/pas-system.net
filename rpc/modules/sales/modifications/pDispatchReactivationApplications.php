<?php
/*
File: pDispatchReactivationApplications
Author: David Bergmann
Creation Date: 24/08/2010
Last Modified:
Modified By:
*/

$sales_log = new SalesLog($db,$_POST['hdnSerial_slg']);
$sales_log->getData();
$sales_log->setUsr_serial_usr($_SESSION['serial_usr']);
$description = unserialize($sales_log->getDescription_slg());
$description['comments_aproval'] = $_POST['txtComments'];
$sales_log->setDescription_slg(serialize($description));

if($_POST['selStatus'] == 'AUTHORIZED') {
	
	$sale = new Sales($db, $sales_log->getSerial_sal());
	$sale->getData();
	
	$sale->setBeginDate_sal($description['new_begin_date']);
	$sale->setEndDate_sal($description['new_end_date']);
	$sale->setStatus_sal($description['new_status_sal']);

	if($sale->update()) {
		$sales_log->setStatus_slg($_POST['selStatus']);
		$serial_slg = $sales_log->update();
	}

	
} else if($_POST['selStatus'] == 'DENIED') {
	$sales_log->setStatus_slg($_POST['selStatus']);
	$serial_slg = $sales_log->update();
}


if($serial_slg) {
	$alert = new Alert($db);
	if($alert->setServeAlert("REACTIVATED", $_POST['hdnSerial_slg'])) {
		http_redirect('main/sales_modifications/1');
	}
}

http_redirect('main/sales_modifications/2');
?>