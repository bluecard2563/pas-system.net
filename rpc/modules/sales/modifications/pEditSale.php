<?php
/*
File:pEditSale.php
Author: Esteban Angulo
Creation Date: 
*/

$slg = new SalesLog($db);
$saleNew =  new Sales($db, $_POST['hdnSerial_sal']);
$saleNew -> getData();
$saleOld =  new Sales($db, $_POST['hdnSerial_sal']);
$customer = new Customer($db);

/********************************************************************************************************************************/
/*****************************************************  New Sale Info START  ****************************************************/
/********************************************************************************************************************************/

/********************** THE CUSTOMER *************************/
if($_POST['serialCus'] == ''){ //IF THE CUSTOMER DOES NOT EXIST
    $customer -> setType_cus($_POST['selType']);
}else{
	$customer -> setserial_cus($_POST['serialCus']);
	$customer -> getData();
}

$customer -> setSerial_cit($_POST['selCityCustomer']);
$customer -> setDocument_cus($_POST['txtDocumentCustomer']);
if($_POST['txtNameCustomer']){
	$customer -> setFirstname_cus(specialchars($_POST['txtNameCustomer']));
}
if($_POST['txtNameCustomer1']){
	$customer -> setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
}
$customer -> setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
$customer -> setBirthdate_cus($_POST['txtBirthdayCustomer']);
$customer -> setPhone1_cus($_POST['txtPhone1Customer']);
$customer -> setPhone2_cus($_POST['txtPhone2Customer']);
$customer -> setCellphone_cus($_POST['txtCellphoneCustomer']);
$customer -> setEmail_cus($_POST['txtMailCustomer']);

if($_POST['txtAddress']){
	$customer -> setAddress_cus(specialchars($_POST['txtAddress']));
}
if($_POST['txtAddress1']){
	$customer -> setAddress_cus(specialchars($_POST['txtAddress1']));
}
if($_POST['txtRelative']){
	$customer -> setRelative_cus($_POST['txtRelative']);
	$customer -> setRelative_phone_cus($_POST['txtPhoneRelative']);
}
if($_POST['txtManager']){
	$customer -> setRelative_cus(specialchars($_POST['txtManager']));
	$customer -> setRelative_phone_cus($_POST['txtPhoneManager']);
}
if($_POST['hdnVip']){
	$customer -> setVip_cus($_POST['hdnVip']);
}else{
	$customer -> setVip_cus('0');
}
$customer -> setStatus_cus('ACTIVE');

if($_POST['serialCus'] == ''){ //IF THE CUSTOMER DOES NOT EXIST
    $password = GlobalFunctions::generatePassword(6, 8, false);
    $customer -> setPassword_cus(md5($password));

    $serialNewCus = $customer -> insert();
    ERP_logActivity('new', $customer);//ERP ACTIVITY
    
	if($serialNewCus){
	    /*Generating the info for sending the e-mail*/
		if($_POST['txtNameCustomer']){
			$misc['customer'] = $_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
		}
		if($_POST['txtNameCustomer1']){
			$misc['customer'] = $_POST['txtNameCustomer1'];
		}
		$misc['textForEmail'] = 'Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href = "http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
		$misc['username'] = $customer -> getEmail_cus();
		$misc['pswd'] = $password;
		$misc['email'] = $customer -> getEmail_cus();
		/*END E-mail Info*/

		if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
			$error = 1;
		}else{
			$error = 6;
		}
		$saleNew -> setSerial_cus($serialNewCus);
	}
	else{
		$error = 5;
		http_redirect('main/editSale/'.$error);
	}
}
else{
	if($customer -> update()){
		ERP_logActivity('update', $customer);//ERP ACTIVITY
		$error = 1;
	}else{
		$error = 7;
	}
	$saleNew -> setSerial_cus($_POST['serialCus']);
}


/********************** THE SALE *************************/
$saleNew -> setSerial_pbd($_POST['selProduct']);
$saleNew -> setSerial_cnt($_POST['hdnSerial_cnt']);
$saleNew -> setSerial_inv($_POST['hdnSerial_inv']);
$saleNew -> setCardNumber_sal($_POST['txtCardNum']);
$saleNew -> setEmissionDate_sal($_POST['txtEmissionDate']);
$saleNew -> setBeginDate_sal($_POST['txtDepartureDate']);
$saleNew -> setEndDate_sal($_POST['txtArrivalDate']);
$saleNew -> setInDate_sal($_POST['hdnInDateSal']);
$saleNew -> setDays_sal($_POST['hddDays']);
$saleNew -> setFee_sal($_POST['hdnPrice']);
//$saleNew -> setObservations_sal(specialchars($_POST['observations']));
$saleNew -> setCost_sal($_POST['hdnCost']);
if($_POST['hdnDirectSale']){
	$saleNew -> setDirectSale_sal('YES');
}else{
	$saleNew -> setDirectSale_sal('NO');
}

$saleNew -> setTotal_sal($_POST['hdnTotal']);
$saleNew -> setTotalCost_sal($_POST['hdnTotalCost']);
$saleNew -> setChange_fee_sal($_POST['rdCurrency']);
if($_POST['selCityDestination']){
	$saleNew -> setSerial_cit($_POST['selCityDestination']);
}
else{
	$saleNew -> setSerial_cit('NULL');
}
$saleNew -> setType_sal('SYSTEM');
$saleNew -> setStockType_sal($_POST['hdnStockType']);
if($_POST['rdIsFree']){
	if($_POST['rdIsFree'] == 'NO'){
		$saleNew -> setStatus_sal('REGISTERED');
	}else{
		$saleNew -> setStatus_sal('REQUESTED');
	}
	$saleNew -> setFree_sal($data['rdIsFree']);
}else{
	$saleNew -> setFree_sal('NO');
	$saleNew -> setStatus_sal('REGISTERED');
}

if(!$saleNew -> onCoverageTime()){
	$saleNew -> setInternationalFeeAmount_sal($_POST['hdnTotalCost']);
}




/********************** THE EXTRAS *************************/
$extra = new Extras($db);
$extrasOld = Extras::getExtrasBySale($db, $_POST['hdnSerial_sal']);
$extrasNew = $_POST['extra'];

if(is_array($extrasNew)){
	foreach($extrasNew as $key => $extra ){
		$custExt = new Customer($db);
		if($custExt -> existsCustomer($extra['idExtra'],NULL)){
			$customerID = $custExt -> getCustomerSerialbyDocument($extra['idExtra']);
			$custExt -> setSerial_cus($customerID);
			$custExt -> getData();
			$custExt -> setType_cus('PERSON');
			$custExt -> setFirstname_cus(specialchars($extra['nameExtra']));
			$custExt -> setLastname_cus(specialchars($extra['lastnameExtra']));
			$custExt -> setEmail_cus($extra['emailExtra']);
			$custExt -> setSerial_cit($_POST['selCityCustomer']);
			$custExt -> setStatus_cus('ACTIVE');
			$custExt -> update();
			ERP_logActivity('update', $custExt);//ERP ACTIVITY
		}else{
			$custExt -> setSerial_cit($_POST['selCityCustomer']);
			$custExt -> setDocument_cus($extra['idExtra']);
			$custExt -> setFirstname_cus(specialchars($extra['nameExtra']));
			$custExt -> setLastname_cus(specialchars($extra['lastnameExtra']));
			$custExt -> setBirthdate_cus($extra['birthdayExtra']);
			$custExt -> setType_cus('PERSON');
			$custExt -> setStatus_cus('ACTIVE');
			$custExt -> setEmail_cus($extra['emailExtra']);
			$passwordExt = GlobalFunctions::generatePassword(6, 8, false);
			$custExt -> setPassword_cus(md5($passwordExt));
			$customerID = $custExt -> insert();
			ERP_logActivity('new', $custExt);//ERP ACTIVITY
		}
		
		//SAVE QUESTIONS:
		$sessionLanguage = Language::getSerialLangByCode($db, $_SESSION['language']);
		$questionList = Question::getActiveQuestions($db, $sessionLanguage);
		
		Answer::deleteMyAnswers($db, $customerID);
		foreach($questionList as $q){
			$answer = new Answer($db);
			$answer -> setSerial_cus($customerID);
			$answer -> setSerial_que($q['serial_que']);
			$ans = $_POST['question'.$key.'_'.$q['serial_qbl']];
			if($ans){
				$answer -> setYesno_ans($ans);
				$answer -> insert();
			}else{
				break;
			}
		 }
	}//END FOREACH
}

/********************** LOG THE MODIFICATION *************************/

/* New Sale Info END  */

if($saleOld->getData()){
	$saleOld=get_object_vars($saleOld);
	unset($saleOld['db']);
	$saleNew=get_object_vars($saleNew);
	unset($saleNew['db']);
	$saleNew['modifier']=$_POST['hdnModifier'];//saves who modified the sale.

	foreach($_POST as $key=>$arr){
		if(!(strpos($key,"chkService_")===false)){
			$saleNew['Services'][$arr]=$arr;
		}
	}

	//Save the LOG for this sale modification.
	$slg=new SalesLog($db);
	$slg->setType_slg('MODIFICATION');
	$slg->setSerial_usr($_SESSION['serial_usr']);
	$slg->setSerial_sal($_POST['hdnSerial_sal']);
	$misc = array('old_sale'=>$saleOld,
				'old_extras'=>$extrasOld,
				'new_sale'=>$saleNew,
				'new_extras'=>$_POST['extra'],
				'modifyObs'=>specialchars($_POST['observations']),
				'Description'=>'Sales Modification Request.');
	
	$slg->setDescription_slg(serialize($misc));
	$slg->setStatus_slg('PENDING');
	$serial_slg=$slg->insert();
	if($serial_slg){
		if($_POST['hdnSerial_inv']!=""){
			/*GENERATING THE ALERTS*/
			$parameter = new Parameter($db, '17'); //Parameters for Stand-by Requests.
			$parameter -> getData();
			$destinationUsers = unserialize($parameter->getValue_par());
			$usersCountry = GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
			if($usersCountry == '1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
				$usersCountry = '62';
			}
			$alert=new Alert($db);
			$alert->setMessage_alt('Solicitud de Modificacion de Venta pendiente.');
			$alert->setType_alt('SALE');
			$alert->setStatus_alt('PENDING');
			$alert->setRemote_id($serial_slg);
			$alert->setTable_name('sales/modifications/fCheckSalesApplication/');
			$destinationUsers[$usersCountry]=explode(',', $destinationUsers[$usersCountry]);
			foreach($destinationUsers[$usersCountry] as $d){
				$alert->setSerial_usr($d);
				if($alert->insert()){
					$error=1;
				}else{
					$error=3;
					break;
				}
			}
		}else{

			$url = URL."modules/sales/modifications/pCheckSalesApplication";

			if (!function_exists('curl_init')){
				die('CURL is not installed!');
			}
			// create a new curl resource

			$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';

			session_write_close();
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "hdnSerial_sal=".$_POST['hdnSerial_sal']."&selAuthorize=AUTHORIZED&hdnSerial_slog=".$serial_slg."&authorizeObs=Venta no facturada modificacion automatica");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_COOKIE, $strCookie );
			
			// download the given URL, and return output
			$output = curl_exec($ch);
			// close the curl resource, and free system resources
			curl_close($ch);
			$error=9;
		}
	}else{//The sale_log wasn't inserted.
		$error=2;
	}
}
http_redirect('main/editSale/'.$error);
?>