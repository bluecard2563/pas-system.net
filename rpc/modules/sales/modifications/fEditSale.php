<?php
/*
File:editSale.php
Author: Esteban Angulo
Creation Date: 14/04/2010
*/
	$parameter= new Parameter($db);
	$parameter -> setSerial_par('7');
	$parameter -> getData();
	$age = $parameter -> getValue_par();
	$sale = new Sales($db);
	$sale -> setCardNumber_sal($_POST['txtCardNumber']);
	if(!$sale -> getDataByCardNumber()){
		$travelerLog = new TravelerLog($db);
		if($travelerLog -> getDataByCardNumber($_POST['txtCardNumber'])){//TODO STATIC
			$error = 2;
		}else{
			$error = 1;
		}
		http_redirect('modules/sales/modifications/fSearchCard/'.$error);
	}

	$nameUser = $_SESSION['user_name'];
	$sale = get_object_vars($sale);
	unset($sale['db']);

	$sLog2 = new SalesLog($db);
	if($sLog2 -> getPendingSalesLogBySale($sale['serial_sal'], 'MODIFICATION')!=false){
		$error = 4;
		http_redirect('modules/sales/modifications/fSearchCard/'.$error);
	}

	if($sale['free_sal']=='YES'){
		if($sale['status_sal']=='REQUESTED' || $sale['status_sal']=='DENIED' ){
			$error=5;
			http_redirect('modules/sales/modifications/fSearchCard/'.$error);
		}
	}

	if($sale['status_sal']=='REQUESTED' || $sale['status_sal']=='DENIED' || $sale['status_sal']=='VOID' || $sale['status_sal']=='EXPIRED' || $sale['status_sal']=='BLOCKED'){
		$error=6;
		http_redirect('modules/sales/modifications/fSearchCard/'.$error);
	}
	$date = date("d/m/Y");
	if($_SESSION['serial_dea']!='' && $date>$sale['begin_date_sal']){
		$error=7;
		http_redirect('modules/sales/modifications/fSearchCard/'.$error);
	}
	
	//PRE-LOAD INFORMATION
    //CounterName
    $counter=new Counter($db);
	$counter -> setSerial_cnt($sale['serial_cnt']);
	$counter -> getData();
	$counter=get_object_vars($counter);
	unset($counter['db']);

	$serial_cnt = $sale['serial_cnt'];
	$data['counterName'] = $counter['aux_cnt']['counter'];
	$deaData = Dealer::getDealerCity($db, $serial_cnt);
    $data['dealerOfUser'] = $counter['serial_dea'];
    
    $dealer = new Dealer($db);
	$dealer -> setSerial_dea($data['dealerOfUser']);
    $dealer -> getData();
	//Debug::print_r($dealer);
    $data['name_dea'] = $dealer -> getName_dea();
    $data['serialDealer'] = $dealer -> getDea_serial_dea();
    
	//Makes Seller Code
    $sellerCode = $deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer -> getCode_dea().'-'.$data['serialDealer'].'-'.$serial_cnt;
	//EmissionCity
    $data['emissionPlace'] = $deaData['name_cit'];
    //EmissionDate
	$emissionDate=date("d/m/Y", strtotime(str_replace("/", "-", $sale['emission_date_sal'])));
	//Country currencies
	$curBycountry=new CurrenciesByCountry($db);
	$data['currencies'] = $curBycountry -> getCurrenciesDealersCountry($counter['serial_dea'],$sale['serial_sal']);
	$data['currenciesNumber']=sizeof($data['currencies']);

	//Country and City Lists
	$country=new Country($db);
    $city= new City($db);
	$countryList = $country -> getCountry();

	//MAIN CUSTOMER INFO
	$customer=new Customer($db);
	$customer -> setserial_cus($sale['serial_cus']);
	$customer -> getData();
	$customer=get_object_vars($customer);
	unset($customer['db']);

	//Destination Country
	$sale['serial_cou'] = $country -> getCountryByCity($sale['serial_cit']);
	$cityList = $city -> getCitiesByCountry($sale['serial_cou']);

	//COUNTRY OF SALE
	$product_of_sale_info=ProductByCountry::getPxCDataByPbDSerial($db, $sale['serial_pbd']);

	/*products by Dealer*/
    $productByDealer= new ProductByDealer($db);
    $productListByDealer = $productByDealer -> getproductByDealer($data['serialDealer'], $_SESSION['serial_lang']);
	$fligths = $productByDealer -> getFligths($sale['serial_pbd']);

    //Services by dealer
    $servicesByDealer= new ServicesByDealer($db);
    if(ServicesByDealer::serviceByCountryExist($db, $deaData['serial_cou'])){
        $servicesListByDealer = $servicesByDealer -> getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $deaData['serial_cou']);
    }else{
        $servicesListByDealer = $servicesByDealer -> getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
    }

	$sbcbs= new ServicesByCountryBySale($db);
	$sbcbs = $sbcbs -> getServicesBySale($sale['serial_sal'], $data['serialDealer']);


	$extra=new Extras($db);
	$extraArray = Extras::getExtrasBySale($db, $sale['serial_sal']);
    if(is_array($extraArray)){
		$pbc=new ProductByCountry($db);
		$percentages = $pbc -> getPercentages_by_Product($sale['serial_pbd'],$deaData['serial_cou']);
		$relationshipTypeList = Extras::getAllRelationships($db);		
		$smarty -> register('relationshipTypeList,percentages');
		$totalFeeExtras=0;
		$totalCostExtras=0;
		foreach($extraArray as $key=>$ext){
			
			$totalFeeExtras = $totalFeeExtras+(float)$ext['fee_ext'];
			$totalCostExtras = $totalCostExtras+(float)$ext['cost_ext'];
			//echo $totalFeeExtras."<br>";
		}
		$sale['totalFeeExtras'] = $totalFeeExtras;
		$sale['totalCostExtras'] = $totalCostExtras;
	}
    //If the dealer doesn't have any products to sale,
    //he can't see the sales interface.
    if(!is_array($productListByDealer)){
		$error=3;
        http_redirect('modules/sales/modifications/fSearchCard/'.$error);
    }

		

	if($sale['serial_inv']==''){
		if($sale['status_sal']=='ACTIVE'){
			$sale['modify']='NO';
		}
		else{
			$sale['modify']='YES';
		}
	}
	else{
		$sale['modify']='NO';
	}
	//Debug::print_r($sale);
	$sizeExtras=count($extraArray)-1;
	$parameter= new Parameter($db);
	$parameter -> setSerial_par('14');
	$parameter -> getData();
	$maxCoverTime = $parameter -> getValue_par();
	
	$parameter2 = new Parameter($db,1); // - ->  PARAMETER FOR ELDER QUESTIONS
    $parameter2 -> getData();
    $elderlyAgeLimit = $parameter2 -> getValue_par(); //value of the paramater used to display or not the questions

	  /*QUESTIONS*/
    $language=new Language($db);
    $sessionLanguage = $language -> getSerialByCode($_SESSION['language']);
    $question=new Question($db);
    $maxSerial = $question -> getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
    /*END QUESTIONS*/

	/*HISTROY OF CHANGES*/
	$sLog2= new SalesLog($db);
	if($sLog2 -> getSalesLogBySale($sale['serial_sal'], 'MODIFICATION')!=false){
		$auxSlog = $sLog2 -> getAux_slg();
		//Debug::print_r($auxSlog);
		$history= array();
		foreach($auxSlog as $key=>$slg){
			$descrip=unserialize($slg['description_slg']);
			//Debug::print_r($descrip);
			$history[$key]['obs'] = $descrip['modifyObs'];
			$history[$key]['status'] = $slg['status_slg'];
			$history[$key]['applicant'] = $slg['first_name_usr']." ".$slg['last_name_usr'];
			$history[$key]['modDate'] = $slg['date_slg'];
		}
		$smarty -> register('history');
	}
	/*END HISTORY OF CHANGES*/
	
	//echo '<pre>';print_r($sale);echo '</pre>';
	//echo '<pre>';print_r($productListByDealer);echo '</pre>';
	
	
	//Debug::print_r($history);
	//Debug::print_r($sale);
	$smarty -> register('sale,sellerCode,data,customer,countryList,cityList,productListByDealer,servicesListByDealer');
	$smarty -> register('sbcbs,emissionDate,maxCoverTime,elderlyAgeLimit,extraArray,sizeExtras,nameUser,questionList');
	$smarty -> register('fligths,age,product_of_sale_info');
	$smarty -> display();
?>