<?php
/*
 * File: pAddDays.php
 * Author: Patricio Astudillo
 * Creation Date: 09/04/2010
 * Modifies By: David Bergmann
 * Last Modified: 13/07/2010
 */

$sale=new Sales($db, $_POST['serial_sal']);
if($sale->getData()){
	$sale->setEndDate_sal($_POST['txtArrivalDate']);
	$sale->setDays_sal($_POST['hddDays']);

	if($sale->update()){
		//Save the LOG for this sale modification.
		$slg=new SalesLog($db);
		$slg->setType_slg('ADD_DAYS');
		$slg->setSerial_usr($_SESSION['serial_usr']);
		$slg->setUsr_serial_usr($_SESSION['serial_usr']);
		$slg->setSerial_sal($_POST['serial_sal']);
		$misc=array('old_end_date_sal'=>$_POST['oldArrivalDate'],
					'new_end_date_sal'=>$_POST['txtArrivalDate'],
					'old_days_sal'=>$_POST['hddOldDays'],
					'new_days_sal'=>$_POST['hddDays'],
					'Description'=>'Extension of days for a travel.');
		$slg->setDescription_slg(serialize($misc));

		if($slg->insert()){
			$error=1;
		}else{//The sale_log wasn't inserted.
			$error=2;
		}
	}else{//The sale wasn't updated.
		$error=3;
	}
}else{//The card number doesn't exist.
	$error=4;
}

http_redirect('main/sales_modifications/'.$error);
?>
