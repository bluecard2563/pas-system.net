<?php
/*
File:pCheckSalesAplication
Author: Esteban Angulo
Creation Date: 05/05/2010
Modifies By: David Bergmann
Last Modified: 13/07/2010
*/
//Debug::print_r($_POST);
//die();
if($_POST['hdnSerial_sal'] && $_POST['selAuthorize']=='AUTHORIZED'){
	 $sLog=new SalesLog($db,$_POST['hdnSerial_slog']);
	 $sLog->getData();
	 $description=unserialize($sLog->getDescription_slg());
	 $description['authorize']=$_POST['authorizeObs'];
	 $slg = new SalesLog($db,$_POST['hdnSerial_slog']);
	 $sLog->setStatus_slg('AUTHORIZED');
	 $sLog->setDescription_slg(serialize($description));
	 $sLog->setUsr_serial_usr($_SESSION['serial_usr']);

	 if($sLog->update()){
		//Debug::print_r($description);
		
		$serial_sal=$description['new_sale']['serial_sal'];
		$sale=new Sales($db,$serial_sal);
		$sale->getData();
		$sale->setSerial_pbd($description['new_sale']['serial_pbd']);
		$sale->setSerial_cus($description['new_sale']['serial_cus']);
		$sale->setSerial_cit($description['new_sale']['serial_cit']);
		$sale->setCardNumber_sal($description['new_sale']['card_number_sal']);
		$sale->setBeginDate_sal($description['new_sale']['begin_date_sal']);
		$sale->setEndDate_sal($description['new_sale']['end_date_sal']);
		$sale->setDays_sal($description['new_sale']['days_sal']);
		$sale->setFee_sal($description['new_sale']['fee_sal']);
		$sale->setObservations_sal(specialchars($description['new_sale']['observations_sal']));
		$sale->setCost_sal($description['new_sale']['cost_sal']);
		$sale->setFree_sal($description['new_sale']['free_sal']);
		$sale->setTotal_sal($description['new_sale']['total_sal']);
		$sale->setTotalCost_sal($description['new_sale']['total_cost_sal']);
		$sale->setChange_fee_sal($description['new_sale']['change_fee_sal']);
		$sale->setInternationalFeeAmount_sal($description['new_sale']['international_fee_amount']);

		if($sale -> update()){
			if($description['new_extras']){
				$extras = new Extras($db);
				$extras -> delete($serial_sal);
				foreach($description['new_extras'] as $extra){
					$extras -> setSerial_sal($serial_sal);
					$extras -> setSerial_cus($extra['serialCus']);//TODO IF NO SERIAL_CUS, INSERT CUSTOMER
					$extras -> setRelationship_ext($extra['selRelationship']);
					$extras -> setFee_ext($extra['hdnPriceExtra']);
					$extras -> setCost_ext($extra['hdnCostExtra']);
					if(!$extras -> insert()){
						$error = 5;
						http_redirect('modules/sales/modifications/fSearchPendingModifyApplications/'.$error);
					}else{
						$error = 1;
					}
				}
			}

			$alert=new Alert($db);
			if($alert->setServeAlert('SALE',$_POST['hdnSerial_slog']) && $error!=5){
				$error=1;
			}else{
				$error=6;
			}
		}else{
			$error=5;
		}
	 }else{
		$error=4;
	 }
}else{
	$sLog=new SalesLog($db,$_POST['hdnSerial_slog']);
	$sLog->getData();
	$sLog->setStatus_slg('DENIED');
	$sLog->setUsr_serial_usr($_SESSION['serial_usr']);
	if($sLog->update()){
		$alert=new Alert($db);
		if($alert->setServeAlert('SALE',$_POST['hdnSerial_slog'])){
			$error=2;
		}else{
			$error=3;
		}
	}else{
		$error=4;
	}
}

http_redirect('modules/sales/modifications/fSearchPendingModifyApplications/'.$error);
?>