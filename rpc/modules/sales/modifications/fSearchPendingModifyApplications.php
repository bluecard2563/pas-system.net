<?php
/*
 * File: fSearchPendingApplications.php
 * Author: Esteban Angulo
 * Creation Date: 06/05/2010
 */

Request::setString('0:error');

$parameter=new Parameter($db, '17'); //Parameters for Stand-by Requests.
$parameter->getData();
$destinationUsers=unserialize($parameter->getValue_par());
$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
	$usersCountry='62';
}

$destinationUsers[$usersCountry]=explode(',', $destinationUsers[$usersCountry]);
if(in_array($_SESSION['serial_usr'], $destinationUsers[$usersCountry])){
	$saleModifyAplications=SalesLog::getSalesLogPendingSaleModify($db);
}else{
	$not_allowed_user=1;
}

$smarty->register('error,saleModifyAplications,not_allowed_user');
$smarty->display();
?>