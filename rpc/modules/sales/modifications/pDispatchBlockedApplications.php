<?php
/*
 * File: pDispatchBlockedApplications.php
 * Author: Patricio Astudillo
 * Creation Date: 03/05/2010, 05:38:02 PM
 * Modifies By: David Bergmann
 * Last Modified: 13/07/2010
 */

$sLog=new SalesLog($db, $_POST['hdnSerial_slg']);
$sLog->getData();
$sLog->setStatus_slg($_POST['selStatus']);
$sLog->setUsr_serial_usr($_SESSION['serial_usr']);
if($sLog->update()){
	if($_POST['selStatus']=='AUTHORIZED'){
		$sale=new Sales($db, $sLog->getSerial_sal());
		$sale->getData();
		$sale->setStatus_sal('BLOCKED');

		if($sale->update()){
			$alert=new Alert($db);
			if($alert->setServeAlert('BLOCKED', $sLog->getSerial_slg())){
				$error=1;
			}else{//The alert wasn't deactivated
				$error=2;
			}
		}else{//Sale Update Failed
			$error=4;
		}
	}else{//If it's DENIED
		$alert=new Alert($db);

		if($alert->setServeAlert('BLOCKED', $sLog->getSerial_slg())){
			$error=1;
		}else{//The alert wasn't deactivated
			$error=2;
		}
	}
}else{//El historial no se ha actualizado.
	$error=3;
}

http_redirect('modules/sales/modifications/fChooseBlockedSales/'.$error);
?>
