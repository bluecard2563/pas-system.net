<?php
/*
 * File: fChangeStandBy.php
 * Author: Patricio Astudillo
 * Creation Date: 12/04/2010
 * Modifies By:
 * Last Modified:
 */

$parameter=new Parameter($db, '9'); //It's the parameter for the max number of added days.
$parameter->getData();
$parameter=get_object_vars($parameter);
unset($parameter['db']);

$smarty->register('parameter');
$smarty->display();
?>
