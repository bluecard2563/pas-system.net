<?php
/*
 * File: fDispatchStandByApplications.php
 * Author: Patricio Astudillo
 * Creation Date: 03/05/2010, 03:19:40 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 03/05/2010, 03:19:40 PM
 */

Request::setInteger('0:serial_slg');
$sLog=new SalesLog($db,$serial_slg);

if($sLog->getData()){
	$sLog=get_object_vars($sLog);
	$sLogStatus=SalesLog::getSalesLogStatus($db);
	$sale=new Sales($db,$sLog['serial_sal']);
	$sale->getData();
        $sale->setCardNumber_sal($sale->getCardNumber_sal());
        $sale->getDataByCardNumber();
	$sale=get_object_vars($sale);
        $customer=new Customer($db);
        $saleLog=new SalesLog($db);
        $logs='';
        $customer->setserial_cus($sale['serial_cus']);
        $customer->getData();
        $customer=get_object_vars($customer);
        unset($customer['db']);
        $saleLog->getSalesLogBySale($sale['serial_sal'],'STAND_BY');
        $logs=$saleLog->getAux_slg();
        if($logs){
            foreach($logs as &$a){
                $a['description_slg']=unserialize($a['description_slg']);
				$a['description_slg']['observations'] = html_entity_decode($a['description_slg']['observations']);
            }
        }
		//die(Debug::print_r($logs));
	$smarty->register('sale,sLog,sLogStatus,customer,logs');
}else{//Can`t load all the info.
	$error=1;
}

$smarty->display();
?>
