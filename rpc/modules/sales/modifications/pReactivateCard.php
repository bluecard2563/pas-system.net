<?php
/*
File: pReactivateCard
Author: David Bergmann
Creation Date: 23/08/2010
Last Modified:
Modified By:
*/

//die(Debug::print_r($_POST));

$sale = new Sales($db,$_POST['serial_sal']);
$sale->getData();

$sales_log = new SalesLog($db);
$sales_log->setSerial_usr($_SESSION['serial_usr']);
$sales_log->setSerial_sal($sale->getSerial_sal());
$sales_log->setType_slg('REACTIVATED');
$sales_log->setStatus_slg('PENDING');
$description = array(
	'old_status_sal'=>$sale->getStatus_sal(),
	'new_status_sal'=>'ACTIVE',
	'Description'=>'Change of state to ACTIVE',
	'old_begin_date'=>$sale->getBeginDate_sal(),
	'new_begin_date'=>$_POST['txtBeginDateSal'],
	'old_end_date'=>$sale->getEndDate_sal(),
	'new_end_date'=>$_POST['txtEndDateSal'],
	'comments_request'=>$_POST['txtDescription']
);
$sales_log->setDescription_slg(serialize($description));

$serial_slg = $sales_log->insert();

if($serial_slg) {
	$alert = new Alert($db);
	$alert->setSerial_usr($_SESSION['serial_usr']);
	$alert->setMessage_alt(utf8_decode("Solicitud de reactivación de tarjeta expirada."));
	$alert->setType_alt('REACTIVATED');
	$alert->setStatus_alt('PENDING');
	$alert->setRemote_id($serial_slg);
	$alert->setTable_name("sales/modifications/fDispatchReactivationApplications/");
	
	$serial_alt = $alert->insert();
	if($serial_alt) {
		/*E-mail Data*/
		$misc['card_number'] = $sale->getCardNumber_sal();
		$user = new User($db, $_SESSION['serial_usr']);
		$user->getData();
		$misc['user_name'] = $user->getFirstname_usr()." ".$user->getLastname_usr();
		$customer = new Customer($db,$sale->getSerial_cus());
		$customer->getData();
		$misc['customer_name'] = $customer->getFirstname_cus()." ".$customer->getLastname_cus();
		//E-MAIL LIST
		$parameter=new Parameter($db, '30'); //Parameters for Liquidation Permitions.
		$parameter->getData();
		$permitedUsers=unserialize($parameter->getValue_par());

		$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);

		if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
				$usersCountry='62';
		}
		$permitedUsers[$usersCountry]=explode(',', $permitedUsers[$usersCountry]);

		//Assistance responsible contact
		if(is_array($permitedUsers[$usersCountry])){
			foreach ($permitedUsers[$usersCountry] as $key=>$p) {
				$user = new User($db,$p);
				$user->getData();
				$misc['email'][$key] = $user->getEmail_usr();
			}
		}
		/*End e-mail Data*/


		if(GlobalFunctions::sendMail($misc, 'card_reactivation')){
			$error=1;
		} else {
			$error=4;
		}
	} else {
		$error = 3;
	}
} else {
	$error = 2;
}

http_redirect("modules/sales/modifications/fReactivateCard/".$error);
?>