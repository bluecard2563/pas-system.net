<?php
/*
File:pLoadCustomersSaleMasive.php
Author: Esteban Angulo
Creation Date: 15/03/2010
*/

$path_info = pathinfo($_FILES['uploadFile']['name']);
$ext = $path_info['extension'];

if($_FILES['uploadFile']['name']){
	if($ext!='xls' && $ext!='xlsx'){
		$error=2;//error, el archivo no es excel
	}else{
		$prefix = substr(md5(uniqid(rand())),0,6);
		$fileName = $prefix.'_'.$_FILES["uploadFile"]['name'];
		$path=''.$fileName;
		if(copy($_FILES['uploadFile']['tmp_name'],$path)){

                    require_once ("PHPExcel.php");
                    require_once ("PHPExcel/Reader/Excel5.php");
                    require_once ("PHPExcel/Reader/Excel2007.php");

                        // echo chr(65); //character 'A'
			if($ext=='xls'){
				$objReader = new PHPExcel_Reader_Excel5();
			}else{
				$objReader = new PHPExcel_Reader_Excel2007();
			}

			$objReader->setReadDataOnly(true);

			ini_set('memory_limit', '512M');
			ini_set('max_execution_time','3600');
                        if (file_exists($path)) {
                            $objPHPExcel = @$objReader->load($path);
                        }
			//$objPHPExcel = @$objReader->load('../../'.$path);

			$row=1;//Empieza en 1 porque la cabecera de t�tulos
			$iteration=0;
			$ingresados = 0;
			$noIngresados = 0;
			$duplicadosCedula = array();
			$duplicadosEmail = array();
			$votantes=array();

			function especialCharFix($text){
				return html_entity_decode(htmlentities($text,ENT_COMPAT, 'UTF-8'),ENT_COMPAT, 'iso-8859-1');
			}
			function isEmpty($sheet, $row){
                //echo ' wiii'.$sheet->getCell(A.$row)->getValue();
                                
				if($sheet->getCell(A.$row)->getValue()!="" ||  //document_cus
				   $sheet->getCell(B.$row)->getValue()!="" ||  //document_cbm
                   $sheet->getCell(C.$row)->getValue()!="" ||  //type_cus
				   $sheet->getCell(D.$row)->getValue()!="" ||  //first_name_cus
				   $sheet->getCell(E.$row)->getValue()!="" ||  //last_name_cus
				   $sheet->getCell(F.$row)->getValue()!="" ||  //phone1_cus
				   $sheet->getCell(G.$row)->getValue()!="" ||  //phone2_cus
				   $sheet->getCell(H.$row)->getValue()!="" ||  //cellphone_cus
				   $sheet->getCell(I.$row)->getValue()!="" ||  //email_cus
				   $sheet->getCell(J.$row)->getValue()!="" ||  //address_cus
				   $sheet->getCell(K.$row)->getValue()!="" ||  //relative_cus
				   $sheet->getCell(L.$row)->getValue()!="" ||  //relative_phone_cus
				   $sheet->getCell(M.$row)->getValue()!="" ||  //birthdate_cus
				   $sheet->getCell(N.$row)->getValue()!=""     //serial_cit
				){
					return false;
				}else{
					return true;
				}
			}
			function cellFilter($value){
				$value = str_replace (" ", "", $value);

				return $value;
			}

			while(!isEmpty($objPHPExcel->getActiveSheet(),++$row)){
				$data['serial_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(A.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(A.$row)->getValue()) : NULL;
				$data['cus_serial_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue()) : NULL;
				$data['document_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(C.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(C.$row)->getValue()) : NULL;
				$data['first_name_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(D.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(D.$row)->getValue()) : NULL;
				$data['last_name_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(E.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(E.$row)->getValue()) : NULL;
				$data['phone1_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(F.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(F.$row)->getValue()) : NULL;
				$data['phone2_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(G.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(G.$row)->getValue()) : NULL;
				$data['cellphone_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(H.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(H.$row)->getValue()) : NULL;
				$data['email_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(I.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(I.$row)->getValue()) : NULL;
				$data['address_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(J.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(J.$row)->getValue()) : NULL;
				$data['relative_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(K.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(K.$row)->getValue()) : NULL;
				$data['relative_phone_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(L.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(L.$row)->getValue()) : NULL;
				$data['birthdate_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(M.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(M.$row)->getValue()) : NULL;
				$data['serial_cit'][$row] = $objPHPExcel->getActiveSheet()->getCell(N.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(N.$row)->getValue()) : NULL;
			}
			$customer = new Customer($db);
			$array=$customer->loadData($data,NULL);

			$_SESSION['exist']=$array['exist'];
			$_SESSION['serial_sal']=$_POST['serial_sal'];

			

			foreach($array['insert']['document_cus'] as $key=>$document_cus){
				$count++;
				$customer->setDocument_cus($array['insert']['document_cus'][$key]);
				$customer->setFirstname_cus($array['insert']['first_name_cus'][$key]);
				$customer->setLastname_cus($array['insert']['last_name_cus'][$key]);
				$customer->setPhone1_cus($array['insert']['phone1_cus'][$key]);
				$customer->setPhone2_cus($array['insert']['phone2_cus'][$key]);
				$customer->setCellphone_cus($array['insert']['cellphone_cus'][$key]);
				$customer->setEmail_cus($array['insert']['email_cus'][$key]);
				$customer->setAddress_cus($array['insert']['address_cus'][$key]);
				$customer->setRelative_cus($array['insert']['relative_cus'][$key]);
				$customer->setRelative_phone_cus($array['insert']['relative_phone_cus'][$key]);
				$customer->setBirthdate_cus($array['insert']['birthdate_cus'][$key]);
				$customer->setSerial_cit($array['insert']['serial_cit'][$key]);
				//Inserts customers that aren't in the DB yet.
				//$serial_cus=$customer->insert();
				
				//ERP_logActivity('new', $customer);//ERP ACTIVITY
				
			}
			//debug::print_r($array);
			unlink($path);
		}else{
			$error=4;//error al subir el archivo
		}
	}
}

?>