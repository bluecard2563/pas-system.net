<?php
/*
File: pNewSubMasiveSale.php
Author: Miguel Ponce
Creation Date:
Modified By: David Bergmann
Last Modified: 03/09/2010
*/

$serial_sal = $_POST['sal_serial_sal'];
/*LOADS CUSTOMERS*/
$path_info = pathinfo($_FILES['file_source']['name']);
$ext = $path_info['extension'];

/*FORMATING DATES*/
$aux=explode('/', $begin_date_sal);
$aux['3']=$aux['0'];
$aux['0']=$aux['2'];
$aux['2']=$aux['3'];
unset($aux['3']);
$begin_date_sal=implode('/', $aux);

$aux=explode('/', $end_date_sal);
$aux['3']=$aux['0'];
$aux['0']=$aux['2'];
$aux['2']=$aux['3'];
unset($aux['3']);
$end_date_sal=implode('/', $aux);
/*END FORMATING DATES*/

$beginDate=strtotime($begin_date_sal);
$endDate=strtotime($end_date_sal);
$today = strtotime(date("Y-m-d G:i:s"));

$sale = new Sales($db, $serial_sal);
$sale->getData();

$productByDealer=new ProductByDealer($db,$sale->getSerial_pbd());
$productByDealer->getData();

$productByCountry=new ProductByCountry($db, $productByDealer->getSerial_pxc());
$productByCountry->getData();

$product=new Product($db,$productByCountry->getSerial_pro());
$product->getData();

$param=new Parameter($db, 7);
$param->getData();
$childAge=$param->getValue_par();


$priceByProductByCountry=new PriceByProductByCountry($db);
$priceByProductByCountry->setSerial_pxc($productByDealer->getSerial_pxc());

if($priceByProductByCountry->pricesByProductExist($productByDealer->getSerial_pxc())){ //If there is a price for the product in the country of analisis.
	$priceByProductByCountry->getMasivePrice($productByCountry->getSerial_pro(),$productByCountry->getSerial_cou());
}else{ //If not, I take the prices for ALL COUNTRIES
	$priceByProductByCountry->getMasivePrice($productByCountry->getSerial_pro(),1);
}

$price=$priceByProductByCountry->getPrice_ppc();

if($_FILES['file_source']['name']){
	if($ext!='xls' && $ext!='xlsx'){ //verifies if it's an Excel file
		$error=2;

	} else {
		$prefix = substr(md5(uniqid(rand())),0,6);
		$fileName = $prefix.'_'.$_FILES["file_source"]['name'];
		$path=''.$fileName;
		if(copy($_FILES['file_source']['tmp_name'],$path)){

			require_once ("PHPExcel.php");
			require_once ("PHPExcel/Reader/Excel5.php");
			require_once ("PHPExcel/Reader/Excel2007.php");

			if($ext=='xls'){
				$objReader = new PHPExcel_Reader_Excel5();
			} else {
				$objReader = new PHPExcel_Reader_Excel2007();
			}

			$objReader->setReadDataOnly(true);

			ini_set('memory_limit', '512M');
			ini_set('max_execution_time','3600');
			if (file_exists($path)) {
				$objPHPExcel = @$objReader->load($path);
			}

			$row=1;//Empieza en 1 porque la cabecera de títulos
			$iteration=0;
			$ingresados = 0;
			$noIngresados = 0;
			$duplicadosCedula = array();
			$duplicadosEmail = array();
			$votantes=array();


			while(!isEmpty($objPHPExcel->getActiveSheet(),++$row)){
				if($objPHPExcel->getActiveSheet()->getCell(A.$row)->getValue() == ($row-1)){
					$data['serial_cus_xls'][$row] = especialCharFix($objPHPExcel->getActiveSheet()->getCell(A.$row)->getValue());
				}
				else{
					$error='6'; // secuence error
					break;
				}
				if($objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue()=="" || $objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue() < ($row-1)){
					if($objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue()!="") {
						if($product->getMax_extras_pro() >0) {
							$data['cus_serial_cus_xls'][$row] =	$objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue();
						} else {
							$error='7';// extras not acepted
							break;
						}
					} else {
						$data['cus_serial_cus_xls'][$row] =	$objPHPExcel->getActiveSheet()->getCell(B.$row)->getValue();
					}

				}
				else{
					$error='8'; // extras secuence error
					break;
				}
				$data['document_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(C.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(C.$row)->getValue()) : NULL;

				$first_name_cus=$objPHPExcel->getActiveSheet()->getCell(D.$row)->getValue();
				if ( eregi('^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$',$first_name_cus)){
					$data['first_name_cus'][$row]=$first_name_cus;
				}
				else{
					$error='9'; // first name error
					break;
				}
				$last_name_cus=$objPHPExcel->getActiveSheet()->getCell(E.$row)->getValue();
				if ( eregi('^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$',$first_name_cus)){
					$data['last_name_cus'][$row]=$last_name_cus;
				}
				else{
					$error='10'; // last name error
					break;
				}
				$phone1_cus=$objPHPExcel->getActiveSheet()->getCell(F.$row)->getValue();
//				if ( eregi('^\d+$',$phone1_cus)){
					$data['phone1_cus'][$row]=$phone1_cus;
//				}
//				else{
//					$err=5; // phone1 name error
//					break;
//				}
				$phone2_cus=$objPHPExcel->getActiveSheet()->getCell(G.$row)->getValue();
//				if ( eregi('^\d+$',$phone2_cus)){
					$data['phone2_cus'][$row]=$phone2_cus;
//				}
//				else{
//					$err=6; // phone2 name error
//					break;
//				}
				$cellphone_cus=$objPHPExcel->getActiveSheet()->getCell(H.$row)->getValue();
//				if ( eregi('^\d+$',$cellphone_cus)){
					$data['cellphone_cus'][$row]=$cellphone_cus;
//				}
//				else{
//					$err=7; // cellphone_cus name error
//					break;
//				}
				$email_cus=$objPHPExcel->getActiveSheet()->getCell(I.$row)->getValue();
//				if ( eregi("^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$",$email_cus)){
					$data['email_cus'][$row] =	$email_cus;
//				}
//				else{
//					$err=8; // email_cus email error
//					break;
//				}

				$data['address_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(J.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(J.$row)->getValue()) : NULL;
				$data['relative_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(K.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(K.$row)->getValue()) : NULL;
				$data['relative_phone_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(L.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(L.$row)->getValue()) : NULL;

				$excelWinDate	=	$objPHPExcel->getActiveSheet()->getCell(M.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(M.$row)->getValue()) : NULL;
				$timeStamp = mktime(0,0,0,1,$excelWinDate-1,1900);
				if($excelWinDate!=NULL){

					$age = calculateAge($timeStamp);
					$param = new Parameter($db,'1');
					$param->getData();
					$seniorAgeParam = $param->getValue_par();

					//echo $product->getSenior_pro()." && ".$age.">=".$seniorAgeParam;
					if($product->getSenior_pro() == 'NO' && $age>=$seniorAgeParam) {
						$error = '11'; //seniors not accepted
						break;
					} else {
						$childAgeParam = new Parameter($db,'7');
						$childAgeParam->getData();
						$childAge = $childAgeParam->getValue_par();
						if($product->getExtras_restricted_to_pro() == "CHILDREN") {
							if($age<$childAge) {
								$data['birthdate_cus'][$row] =	date("Y-m-d",$timeStamp);
							} else {
								$error = '12'; //only children accepted
								break;
							}
						} elseif($product->getExtras_restricted_to_pro() == "ADULT") {
							if($age>=$childAge) {
								$data['birthdate_cus'][$row] =	date("Y-m-d",$timeStamp);
							} else {
								$error = '13'; //only adults accepted
								break;
							}
						} else {
							$data['birthdate_cus'][$row] =	date("Y-m-d",$timeStamp);
						}
					}

				}
				else{
					$data['birthdate_cus'][$row] =	NULL;
				}

				$data['serial_cit'][$row] =	$objPHPExcel->getActiveSheet()->getCell(N.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(N.$row)->getValue()) : NULL;

				$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(O.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(O.$row)->getValue()) : NULL;
				$timeStamp = mktime(0,0,0,1,$excelWinDate-1,1900);
				$data['start_trl'][$row] =	date("Y-m-d",$timeStamp);

				$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(P.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(P.$row)->getValue()) : NULL;
				$timeStamp = mktime(0,0,0,1,$excelWinDate-1,1900);
				$data['end_trl'][$row] =	date("Y-m-d",$timeStamp);

				$data['relationship_ext'][$row] =	$objPHPExcel->getActiveSheet()->getCell(Q.$row)->getValue() !="" ? especialCharFix($objPHPExcel->getActiveSheet()->getCell(Q.$row)->getValue()) : NULL;
			}


			$customer = new Customer($db);
			$array=$customer->loadData($data,NULL);

			$toShow=array();
			if(is_array($array['exist'])){
				foreach($array['exist'] as $key=>$existCustomer){

					$toShow[$existCustomer['serial_cus_xls']]=
						array(
							'serial_cus'=>$existCustomer['serial_cus'],
							'document_usu'=>$existCustomer['document_cus'],
							'first_name_cus'=>$existCustomer['first_name_cus'],
							'last_name_cus'=>$existCustomer['last_name_cus'],
							'phone1_cus'=>$existCustomer['phone1_cus'],
							'email_cus'=>$existCustomer['email_cus'],
							'birthdate_cus'=>$existCustomer['birthdate_cus'],
							'serial_cus_xls'=>$existCustomer['serial_cus_xls'],
							'cus_serial_cus_xls'=>$existCustomer['cus_serial_cus_xls'],
							'start_trl'=>$existCustomer['start_trl'],
							'end_trl'=>$existCustomer['end_trl'],
							'relationship_ext'=>$existCustomer['relationship_ext']
							);

				}
			}

			if(is_array($array['insert']['document_cus'])){
				foreach($array['insert']['document_cus'] as $key=>$document_cus){
					$count++;
					$customer->setDocument_cus($array['insert']['document_cus'][$key]);
					$customer->setFirstname_cus($array['insert']['first_name_cus'][$key]);
					$customer->setLastname_cus($array['insert']['last_name_cus'][$key]);
					$customer->setPhone1_cus($array['insert']['phone1_cus'][$key]);
					$customer->setPhone2_cus($array['insert']['phone2_cus'][$key]);
					$customer->setCellphone_cus($array['insert']['cellphone_cus'][$key]);
					$customer->setEmail_cus($array['insert']['email_cus'][$key]);
					$customer->setAddress_cus($array['insert']['address_cus'][$key]);
					$customer->setRelative_cus($array['insert']['relative_cus'][$key]);
					$customer->setRelative_phone_cus($array['insert']['relative_phone_cus'][$key]);
					$customer->setBirthdate_cus(date('d/m/Y',strtotime($array['insert']['birthdate_cus'][$key])));
					$customer->setSerial_cit($array['insert']['serial_cit'][$key]);
					//Inserts customers that aren't in the DB yet.
					$serial_cus=$customer->insert();
					
					ERP_logActivity('new', $customer);//ERP ACTIVITY
					
					$toShow[$array['insert']['serial_cus_xls'][$key]]=
						array(
							'serial_cus'=>$serial_cus,
							'document_usu'=>$array['insert']['document_cus'][$key],
							'first_name_cus'=>$array['insert']['first_name_cus'][$key],
							'last_name_cus'=>$array['insert']['last_name_cus'][$key],
							'phone1_cus'=>$array['insert']['phone1_cus'][$key],
							'email_cus'=>$array['insert']['email_cus'][$key],
							'birthdate_cus'=>$array['insert']['birthdate_cus'][$key],
							'serial_cus_xls'=>$array['insert']['serial_cus_xls'][$key],
							'cus_serial_cus_xls'=>$array['insert']['cus_serial_cus_xls'][$key],
							'start_trl'=>$array['insert']['start_trl'][$key],
							'end_trl'=>$array['insert']['end_trl'][$key],
							'relationship_ext'=>$array['insert']['relationship_ext'][$key]
							);
				}
			}
			$sub_total=0;
			$total_customers=0;
			$total_spouse=0;
			$total_children=0;
			$total_children_free=0;
			$total_relatives=0;
			$total_relatives_free=0;

			foreach($toShow as $key=>&$item){
				$start_chl=date('Y-m-d',strtotime('-'.$childAge.' year', strtotime($item['start_trl'])));
				$birthdate=date('Y-m-d',strtotime($item['birthdate_cus']));
				$start_trl=strtotime($item['start_trl']);
				$end_trl=strtotime($item['end_trl']);
				$travel_days=($end_trl-$start_trl)/ (60 * 60 * 24);

				$spouse=0;
				$children=0;
				$adults=0;

				if($item['cus_serial_cus_xls']){
					$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['relationship_ext']=$item['relationship_ext'];
					if($item['relationship_ext']=='SPOUSE' && $spouse==0){
						if($product->getSpouse_pro()=='YES'){
							$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['type']='SPOUSE';
							$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage']=$productByCountry->getPercentage_spouse_pxc();
							$total_spouse++;
							$item['price']=round($price*$travel_days*(1-($productByCountry->getPercentage_spouse_pxc()/100)),2);
							$sub_total+=$item['price'];
						}
						else{
							if($product->getRelative_pro()=='YES'){
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage']=$productByCountry->getPercentage_extras_pxc();
								$total_relatives++;
								$item['price']=round($price*$travel_days*(1-($productByCountry->getPercentage_extras_pxc()/100)),2);
								$sub_total+=$item['price'];
							}
							else{
								$total_customers++;
								$item['price']=round($price*$travel_days,2);
								$sub_total+=$item['price'];
							}
						}
					}else {
						if($product->getRelative_pro()=='YES'){
							if($item['start_trl']!=""){

								if( $start_chl < $birthdate ){
									//echo $start_chl.' es mayor a '.$birthdate;
									$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['type']='RELATIVE';
									if($children>=$product->getChildren_pro()){
										$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage']=$productByCountry->getPercentage_children_pxc();
										$total_children++;
										$item['price']=round($price*$travel_days*(1-($productByCountry->getPercentage_children_pxc()/100)),2);
										$sub_total+=$item['price'];
									}
									else{
										$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage']=0;
										$total_children_free++;
										$item['price']=0;
									}
								}
								else{

									$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['type']='OTHER';
									if($adults>=$product->getAdults_pro()){
										$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage']=$productByCountry->getPercentage_extras_pxc();
										$total_relatives++;
										$item['price']=round($price*$travel_days*(1-($productByCountry->getPercentage_extras_pxc()/100)),2);
										$sub_total+=$item['price'];
									}
									else{
										$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage']=0;
										$total_relatives_free++;
										$item['price']=0;
									}

								}
							}
						}
						else{
							$total_customers++;
							$item['price']=$price*$travel_days;
							$sub_total+=$item['price'];
						}
					}

				}else{
					$total_customers++;
					$item['price']=$single_total=round($price*$travel_days,2);
					$sub_total+=$item['price'];
				}


				if($toShow[$item['cus_serial_cus_xls']]['extras']){
					foreach($toShow[$item['cus_serial_cus_xls']]['extras'] as $subKey=>&$extras){
						if($extras['type']=='SPOUSE'){
							$spouse++;
						}
						if($extras['type']=='RELATIVE'){
							$children++;
						}
						if($extras['type']=='OTHER'){
							$adults++;
						}
					}
				}
			}
			unlink($path);
		}else{
			$error=3; //File upload error
		}
	}
}

function especialCharFix($text){
	return html_entity_decode(htmlentities($text,ENT_COMPAT, 'UTF-8'),ENT_COMPAT, 'iso-8859-1');
}
function isEmpty($sheet, $row){
	if($sheet->getCell(A.$row)->getValue()!="" ||  //serial_cus_xls
	   $sheet->getCell(B.$row)->getValue()!="" ||  //cus_serial_cus_xls
	   $sheet->getCell(C.$row)->getValue()!="" ||  //document_cus
	   $sheet->getCell(D.$row)->getValue()!="" ||  //first_name_cus
	   $sheet->getCell(E.$row)->getValue()!="" ||  //last_name_cus
	   $sheet->getCell(F.$row)->getValue()!="" ||  //phone1_cus
	   $sheet->getCell(G.$row)->getValue()!="" ||  //phone2_cus
	   $sheet->getCell(H.$row)->getValue()!="" ||  //cellphone_cus
	   $sheet->getCell(I.$row)->getValue()!="" ||  //email_cus
	   $sheet->getCell(J.$row)->getValue()!="" ||  //address_cus
	   $sheet->getCell(K.$row)->getValue()!="" ||  //relative_cus
	   $sheet->getCell(L.$row)->getValue()!="" ||  //relative_phone_cus
	   $sheet->getCell(M.$row)->getValue()!="" ||  //birthdate_cus
	   $sheet->getCell(N.$row)->getValue()!="" ||  //serial_cit
	   $sheet->getCell(O.$row)->getValue()!="" ||  //start_trl
	   $sheet->getCell(P.$row)->getValue()!="" ||  //end_trl
	   $sheet->getCell(Q.$row)->getValue()!=""	   //relationship_ext
	){
		return false;
	} else {
		return true;
	}
}
function cellFilter($value){
	$value = str_replace (" ", "", $value);

	return $value;
}

function calculateAge($date) {
	//actual date
	$day=date(j);
	$month=date(n);
	$year=date(Y);

	//birthdate
	$bDay=date("d",$date);
	$bMonth=date("m",$date);
	$bYear=date("Y",$date);


	//If bMonth is the same as month but bDay is smaller than day, we substract one year
	if (($bMonth == $month) && ($bDay > $day)) {
	$year=($year-1); }

	//if bMonth is biger than month, we substract one year
	if ($bMonth > $month) {
	$year=($year-1);}

	//we calculate the diference between year and bYear
	$age=($year-$bYear);
	return $age;
}
/*END LOADS CUSTOMERS*/


/*CREATES SUB-SALE*/
//die(Debug::print_r($_POST));
if($_POST['rdCurrency']) {
	$serial_cur = $_POST['rdCurrency'];
} else {
	$serial_cur = 1;
}

if(!$error) {
	$cost_sal = $_POST['cost_sal'];
	$sale->setSal_Serial_sal($serial_sal);
	$sale->setCost_sal($cost_sal);
	$sale->setTotalCost_sal($cost_sal);
	$currency = new Currency($db,$serial_cur);
	$currency->getData();
	if($cost_sal) {
		if($currency->getExchange_fee_cur()) {
			$sale->setTotal_sal($cost_sal/$currency->getExchange_fee_cur());
		} else {
			$sale->setTotal_sal($cost_sal);
		}
	} else {
		$sale->setTotal_sal($sub_total);
	}
	$sale->setSerial_cur($serial_cur);
	$sale->setChange_fee_sal($currency->getExchange_fee_cur());
	$sale->setChange_fee_sal($currency->getExchange_fee_cur());
	$sale->setSerial_sal(NULL);
	$sale->setEmissionDate_sal(date('d/m/Y'));

	$sale->setInternationalFeeStatus_sal('TO_COLLECT');
	$sale->setInternationalFeeUsed_sal('NO');
	$sale->setInternationalFeeAmount_sal($cost_sal);

	$serial_sal=$sale->insert();
	if($serial_sal){
		$error='1';
		if(is_array($toShow)) {
			foreach ($toShow as $key=>$c) {
				$travelerLog = new TravelerLog($db);
				$travelerLog->setSerial_cnt($sale->getSerial_cnt());
				$travelerLog->setSerial_sal($serial_sal);
				$travelerLog->setSerial_cus($c['serial_cus']);
				$travelerLog->setSerial_cit($sale->getSerial_cit());
				$travelerLog->setCard_number_trl($sale->getCardNumber_sal());
				$travelerLog->setStart_trl($sale->getBeginDate_sal());
				$travelerLog->setEnd_trl($sale->getEndDate_sal());
				$travelerLog->setDays_trl($sale->getDays_sal());
				$travelerLog->setStatus_trl('ACTIVE');
				$travelerLog->setSerial_usr($_SESSION['serial_usr']);
				if(!$travelerLog->insert()) {
					$error='5';
					break;
				}
			}
		}
	}
	else{
		$error='4';
	}
}
http_redirect('main/masiveSales/'.$error.'/'.$serial_sal);
?>