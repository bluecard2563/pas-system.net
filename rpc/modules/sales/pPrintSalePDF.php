<?php
/*
    Document   : pPrintSalePDF.php
    Created on : May 19, 2010
    Author     : Gabriela Guerrero & Santiago Borja
    Generates a PDF file for new sale contract.
*/

	//THIS CODE IS PARTICULAR TO THIS IMPLEMENTATION OF pPrintSalePDF.php:
	Request::setString('0:serialSal');
	Request::setString('1:printType');
	
	if($serialSal){
		$serial_sal = $serialSal;
	}
	if($printType){
		$display = $printType;
	}
	$langCode = Language::getSerialLangByCode($db,$_SESSION['language']);
	//END OF PARTICULAR CODE, FOLLOWING CODE IS COMMON TO ALL IMPLEMENTATIONS:

	//********************************************   VARIABLE DECLARATION *********************************
	if($serial_generated_sale)$serial_sal = $serial_generated_sale;//Captured from invoking file
		
	global $date;
	global $global_weekMonths;
	global $global_extraRelationshipType;
	global $leftMostX;
	global $blue;
	global $lightBlue;
	global $gray;
	global $red;
	global $black;
	global $smallTextSize;
	global $medTextSize;
	global $stdTextSize;
	global $largeTextSize;
	
	include DOCUMENT_ROOT.'lib/globalPDFText.inc.php';//ALL THE FILE TEXTS
	$verticalPointer = 755;
	$bullet = '- ';
	//********************************************   END OF VARIABLE DECLARATION *********************************
	
	
	$sale = new Sales($db,$serial_sal);
	$sale -> getData();
	
	//Page title:
	include DOCUMENT_ROOT.'lib/PDFHeadersContracts.inc.php';
	
	//Gathering all the info:
	$dataInfo = $sale -> getSaleProduct($langCode);
	$dataBen = $sale -> getSaleBenefits($langCode);
	$dataServ = $sale -> getSaleServices($langCode);
	$serialCus = $sale -> getSerial_cus();

	$customer = new Customer($db, $serialCus);
	$customer -> getData();
	$customer = get_object_vars($customer);
	unset($customer['db']);

	$extra = new Extras($db,$serial_sal);
	$dataExt = Extras::getExtrasBySale($db, $serial_sal);
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext += $dE['fee_ext'];
		}
	}
	$date = html_entity_decode('Quito, '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );
	
	/*debug::print_r($dataInfo);die;
	debug::print_r($dataBen);
	debug::print_r($dataServ);
	debug::print_r($serialCus);
	debug::print_r($customer);
	debug::print_r($dataExt);
	debug::print_r($fee_ext);*/
	//Gathering - END
	
	//Header 1
	GlobalFunctions::header1($pdf, $dataInfo, $langCode);
	
	//HEADER 2:
	//DATE AND TIME
	$pdf -> setColor($gray[0], $gray[1], $gray[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer,200,$stdTextSize,''.$saleContract[$langCode]['header2']['location_date']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($leftMostX + 120,$verticalPointer,80,$stdTextSize,''.$dataInfo['name_cit']);//location

	$tempX = $leftMostX + 210;
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,160,$stdTextSize,'  '.date("d").'    '.date("m").'     '.date("y"));//emission date
	$pdf -> setColor($gray[0], $gray[1], $gray[2]);
	$pdf -> line($tempX,$verticalPointer - 2, $tempX,$verticalPointer - 2);
	$pdf -> line($tempX + 20,$verticalPointer + 10, $tempX + 20,$verticalPointer - 2);
	$pdf -> line($tempX + 40,$verticalPointer + 10, $tempX + 40,$verticalPointer - 2);
	$pdf -> line($tempX + 60,$verticalPointer + 10, $tempX + 60,$verticalPointer - 2);
	$pdf -> line($leftMostX + 100,$verticalPointer - 2, $tempX + 60,$verticalPointer - 2);
	$pdf -> addTextWrap($tempX + 5,$verticalPointer - 10,160,$smallTextSize,''.$saleContract[$langCode]['header2']['emission']);
	
	//COD SUCURSAL:
	$tempX += 70;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer - 2,160,$stdTextSize,''.$saleContract[$langCode]['header2']['codBranch']);
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> line($tempX + 45,$verticalPointer - 2,$tempX + 100,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX + 45,$verticalPointer,62,$stdTextSize,''.$dataInfo['code'],'center');//cod. branch
	
	//CONTRACT TYPE
	$tempX += 110;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer - 2,160,$stdTextSize,''.$saleContract[$langCode]['header2']['contract']);
	$pdf -> line($tempX + 100,$verticalPointer - 2,$tempX + 140,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	if($dataInfo['card_number_sal']!=''){
		$a=$pdf -> addTextWrap($tempX + 100,$verticalPointer,48,$stdTextSize,$dataInfo['card_number_sal'],'center');//contract
	}else{
		$pdf -> addTextWrap($tempX + 100,$verticalPointer,48,$stdTextSize,'N/A','center');//contract
	}

	//***********************************************SECTION 1: CLIENT DATA ********************************************************
	$parameter = new Parameter($db);
	$parameter -> setSerial_par('13');
	$parameter -> getData();
	
	//CLIENT DATA TITLE
	$verticalPointer -= 40;
	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract[$langCode]['client']['title'].'</b>');//Client data title

	//MAIN SECTION
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setStrokeColor($gray[0],$gray[1],$gray[2]);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Last name
	$tempX = $leftMostX;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['last_name_cus'], 150);
	
	//First name
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['first_name_cus'], 150);
	
	//Document
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['document_cus'],'center');//document
	
	//Birth Date
	$tempX += 110;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['birthdate_cus'],'center');
	$pdf -> line($leftMostX + 528,$verticalPointer + 2,$leftMostX + 528,$verticalPointer - 8);
	
	//Bottom labels:
	$pdf -> line($leftMostX,$verticalPointer-8,$leftMostX + 528,$verticalPointer-8);
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract[$langCode]['client']['0']);
	
	
	//SECOND LINE
	$verticalPointer -= 25;
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Phone
	$tempX = $leftMostX;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['phone1_cus'],'center');
	
	//Email
	$tempX += 110;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['email_cus'], 150);
	
	//Addesss
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['address_cus'], 268);
	$pdf -> line($leftMostX + 528,$verticalPointer + 2,$leftMostX + 528,$verticalPointer - 8);
	
	//Bottom labels:
	$pdf -> line($leftMostX,$verticalPointer-8,$leftMostX + 528,$verticalPointer-8);
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract[$langCode]['client']['1']);
	
	
	//THIRD LINE
	//Emergency contact Line:
	$verticalPointer -= 30;
	$tempX = $leftMostX;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,140,$stdTextSize,''.$saleContract[$langCode]['client']['3']);
	$pdf -> line($tempX + 140,$verticalPointer - 2,$tempX + 340,$verticalPointer - 2);
	$pdf -> line($tempX + 340,$verticalPointer + 8, $tempX + 340,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 140, $verticalPointer + 5, $stdTextSize, $customer['relative_cus'], 190);
	
	//Emergency Phone
	$tempX += 350;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,40,$stdTextSize,''.$saleContract[$langCode]['client']['4']);
	$pdf -> line($tempX + 40,$verticalPointer - 2, $tempX + 180,$verticalPointer - 2);
	$pdf -> line($tempX + 180, $verticalPointer + 8, $tempX + 180,$verticalPointer-2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 40, $verticalPointer + 5, $stdTextSize, $customer['relative_phone_cus'], 140);
	
	
	
	//********************************************* SECTION 1A: EXTRAS INFO **********************************************************
	//Extras section
	$verticalPointer -= 15;
	if(count($dataExt)>0){
	 $displayedLimit=$parameter->getValue_par();//Extras limit displayed.
	 //$displayedLimit=0;
		if(count($dataExt)<=$displayedLimit){
			$verticalPointer = GlobalFunctions::displayExtras($pdf,$dataExt,$verticalPointer,1,0, $langCode);
		}
	}

	//********************************************* SECTION 2: TRIP DATA **********************************************
	$verticalPointer -= 15;
	if($verticalPointer <= 100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$beginDate=''.$dataInfo['begin_date_sal'];
	$endDate=''.$dataInfo['end_date_sal'];
	if($dataInfo['flights_pro']==0){
		$beginDate = $tripBeginDate;
		$endDate = $tripEndDate;
		$dataInfo['country_sal'] = $tripCountry;
		$dataInfo['city_sal'] = $tripCity;
	}
	
	//Header line:
	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract[$langCode]['trip']['title1'].'</b>');
	
	$pdf -> setLineStyle(0.2,'square');
	$tempX = $leftMostX;
	$verticalPointer -= 5;
	$pdf -> setStrokeColor($black[0],$black[1],$black[2]);
	
	//Bottom Lines
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX + 2,$verticalPointer,528,$medTextSize,''.$saleContract[$langCode]['trip']['0']);
	$pdf -> addTextWrap($leftMostX,$verticalPointer-10,500,$smallTextSize,''.$saleContract[$langCode]['trip']['1']);
	
	//Departure Date
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX + 70, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', strtotime( $beginDate ) ) );
	$pdf -> line($tempX,$verticalPointer-2,$tempX + 130,$verticalPointer - 2);
	$pdf -> line($tempX,$verticalPointer + 10, $tempX,$verticalPointer - 2);
	$pdf -> line($tempX + 130,$verticalPointer + 10, $tempX + 130,$verticalPointer - 2);
	
	//End Date
	$tempX += 135;
	$pdf -> addTextWrap($tempX + 80, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', strtotime( $endDate ) ) );
	$pdf -> line($tempX -2,$verticalPointer - 2, $tempX  + 140,$verticalPointer-2);
	$pdf -> line($tempX + 140,$verticalPointer+10, $tempX + 140,$verticalPointer-2);
	
	//Duration:
	$tempX += 145;
	$dateDiff = strtotime( $endDate ) - strtotime( $beginDate );
	$fullDays = floor($dateDiff/(60*60*24));
	$pdf -> addTextWrap($tempX + 50, $verticalPointer, 30, $stdTextSize, $fullDays+1 );
	$pdf -> line($tempX -2,$verticalPointer - 2, $tempX + 80,$verticalPointer - 2);
	$pdf -> line($tempX + 80,$verticalPointer+10, $tempX + 80,$verticalPointer-2);
	
	//Destination
	$tempX += 85;
	$destinationString = $dataInfo['country_sal'];
	$destinationString .= $dataInfo['city_sal']== 'General'?'':' - ' . $dataInfo['city_sal'];
	GlobalFunctions::writeTextField($pdf, $tempX + 35, $verticalPointer + 5, $stdTextSize, $destinationString, 115);
	$pdf -> line($tempX,$verticalPointer-2, $tempX + 150,$verticalPointer-2);
	$pdf -> line($tempX + 150,$verticalPointer + 10, $tempX + 150,$verticalPointer - 2);
	
	//********************************************* SECTION 2.1: VALID DATES **********************************************
	if($dataInfo['flights_pro']==0){
		$verticalPointer -= 40;
		if($verticalPointer <= 100){
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
		
		//Header line:
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract[$langCode]['trip']['title2'].'</b>');
		
		$pdf -> setLineStyle(0.2,'square');
		$tempX = $leftMostX;
		$verticalPointer -= 5;
		$pdf -> setStrokeColor($black[0],$black[1],$black[2]);
		
		//Bottom Lines
		$pdf -> setColor($gray[0],$gray[1],$gray[2]);
		$pdf -> addTextWrap($tempX + 2,$verticalPointer,528,$medTextSize,''.$saleContract[$langCode]['trip']['3']);
		$pdf -> addTextWrap($leftMostX,$verticalPointer-10,500,$smallTextSize,''.$saleContract[$langCode]['trip']['1']);
		
		//Departure Date
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 70, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', strtotime( $dataInfo['begin_date_sal'] ) ) );
		$pdf -> line($tempX,$verticalPointer-2,$tempX + 130,$verticalPointer - 2);
		$pdf -> line($tempX,$verticalPointer + 10, $tempX,$verticalPointer - 2);
		$pdf -> line($tempX + 130,$verticalPointer + 10, $tempX + 130,$verticalPointer - 2);
		
		//End Date
		$tempX += 135;
		$pdf -> addTextWrap($tempX + 80, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', strtotime( $dataInfo['end_date_sal'] ) ) );
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX  + 140,$verticalPointer-2);
		$pdf -> line($tempX + 140,$verticalPointer+10, $tempX + 140,$verticalPointer-2);
		
		//Duration:
		$tempX += 145;
		$dateDiff = strtotime( $dataInfo['end_date_sal'] ) - strtotime( $dataInfo['begin_date_sal'] );
		$fullDays = floor($dateDiff/(60*60*24));
		$pdf -> addTextWrap($tempX + 50, $verticalPointer, 30, $stdTextSize, $fullDays+1 );
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX + 80,$verticalPointer - 2);
		$pdf -> line($tempX + 80,$verticalPointer+10, $tempX + 80,$verticalPointer-2);
	}
	
	
	
	//***************************************************** SECTION 3: PRODUCT DATA ******************************************************
	$verticalPointer -= 40;
	if($verticalPointer <= 100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract[$langCode]['product']['title'].'</b>');

	//Init:
	$verticalPointer -= 5;
	$tempX = $leftMostX;
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setStrokeColor($gray[0],$gray[1],$gray[2]);
	
	//Product Name:
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,90,$stdTextSize,''.$saleContract[$langCode]['product']['0']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 90, $verticalPointer + 5, $stdTextSize, ''.$dataInfo['name_pbl'], 130);
	$pdf -> line($tempX + 90 ,$verticalPointer - 2, $tempX + 220,$verticalPointer - 2);
	$pdf -> line($tempX + 220,$verticalPointer + 10, $tempX + 220,$verticalPointer - 2);
	
	//Fee
	$tempX += 230;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,50,$stdTextSize,''.$saleContract[$langCode]['product']['1']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX + 50,$verticalPointer,70,$stdTextSize,''.number_format(($dataInfo['fee_sal']+$fee_ext), 2, '.', ''),'center');
	$pdf -> line($tempX + 50,$verticalPointer -2, $tempX + 120,$verticalPointer - 2);
	$pdf -> line($tempX + 120,$verticalPointer + 10, $tempX + 120,$verticalPointer - 2);
	
	//Total Fee
	$tempX += 130;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract[$langCode]['product']['2']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.number_format($dataInfo['total_sal'], 2, '.', ''),'center');
	$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
	$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);

	

	//***************************************************** SECTION 3.1: SERVICES DATA ******************************************************
	if($dataServ){
		$verticalPointer -= 40;
		if($verticalPointer <= 100){
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract[$langCode]['service']['title'].'</b>');
	
		//Init:
		$verticalPointer -= 5;
		$tempX = $leftMostX;
		$pdf -> setLineStyle(0.2,'square');
		$pdf -> setStrokeColor($gray[0],$gray[1],$gray[2]);
		
		$totalServFee=0;
		foreach($dataServ as $dS){
			if($verticalPointer <= 100){
				$pdf -> ezNewPage();
				$verticalPointer = 755;
			}
			
			//Service Name
			$pdf -> setColor($gray[0],$gray[1],$gray[2]);
			$pdf -> addTextWrap($tempX,$verticalPointer,45,$stdTextSize,''.$saleContract[$langCode]['service']['0']);
			$pdf -> setColor($black[0], $black[1], $black[2]);
			GlobalFunctions::writeTextField($pdf, $tempX + 45, $verticalPointer + 5, $stdTextSize, ''.$dS['name_sbl'], 305);
			$pdf -> line($tempX + 45 ,$verticalPointer - 2, $tempX + 350,$verticalPointer - 2);
			$pdf -> line($tempX + 350,$verticalPointer + 10, $tempX + 350,$verticalPointer - 2);
			
			//Total Fee
			$tempX += 360;
			$pdf -> setColor($gray[0],$gray[1],$gray[2]);
			$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract[$langCode]['service']['1']);
			$pdf -> setColor($black[0], $black[1], $black[2]);
			$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.$dS['coverage_sbc'],'center');
			$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
			$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);
			
			if($dS['fee_type_ser']=='PERDAY'){
				$totalServFee += ($dS['price_sbc'] * $dataInfo['days_sal']);
			}else{
				$totalServFee += $dS['price_sbc'];
			}
			$verticalPointer -= 15;
			$tempX = $leftMostX;
		}
		
		//Total Fee
		$tempX += 360;
		$pdf -> setColor($gray[0],$gray[1],$gray[2]);
		$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract[$langCode]['service']['2']);
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.number_format($totalServFee, 2, '.', ''),'center');
		$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
		$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);
	 }

	 
	 
	//************************************************** SECTION 4: BENEFITS **********************************************
	$verticalPointer -= 35;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}
	
	$colWidth = 200;
	$valueWidth = 58;

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer + 12,546,$largeTextSize,'<b>'.$saleContract[$langCode]['benefits']['title'].'</b>');
	$pdf -> addTextWrap($leftMostX + $colWidth + $valueWidth + 10,$verticalPointer + 12,546,$largeTextSize,'<b>'.$saleContract[$langCode]['benefits']['title'].'</b>');

	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	if($dataBen){
		$count = 0;
		$verticalPointer -=5;
		$tempX = $leftMostX;
		$verticalSep = 10;
		$benefitsByColumn = 3;
		
		foreach($dataBen as $d){
			if($count ++ == $benefitsByColumn * 2)break;
			
			if($dataBen['price_bxp'] == 0){
				$displayValue = $d['alias_con'];
			}else{
				$displayValue = $d['price_bxp'].'  ('.$d['restriction_price_bxp'].' x '.$d['description_rbl'].')';
			}
			
			$descLen = $pdf -> getTextWidth($stdTextSize,$d['description_bbl']);
			$valueLen = $pdf -> getTextWidth($stdTextSize,$displayValue);
			
			if($count == $benefitsByColumn + 1){
				$tempX += $colWidth + $valueWidth + 10;
				$verticalPointer += $benefitsByColumn * $verticalSep;
			}
			
			$description = $bullet.$d['description_bbl'];
			while($pdf -> getTextWidth($stdTextSize,$description) < $colWidth - 5){
				$description .= '_';
			}

			$pdf -> addTextWrap($tempX,$verticalPointer,$colWidth,$stdTextSize,$description);
			$pdf -> addTextWrap($tempX + $colWidth,$verticalPointer, $valueWidth,$stdTextSize,$displayValue, 'right');
			
			if($verticalPointer<=100){
				$pdf -> ezNewPage();
				$verticalPointer = 755;
			}
			$verticalPointer -= $verticalSep;
		}
	}
	
	//Benefits last message:
	$verticalPointer -= 2;
	$pdf -> addTextWrap($leftMostX,$verticalPointer,550,$stdTextSize,''.$saleContract[$langCode]['benefits']['0']);
	$pdf -> ezSetMargins(50,30,60,30);
	
	
	
	
	//************************************************** SECTION 5: RECOMMENDATIONS **********************************************
	$verticalPointer -= 35;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract[$langCode]['recomen']['title'].'</b>');
	
	
	$verticalPointer -= 5;
	$verticalStep = 10;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$rows = count($saleContract[$langCode]['recomen']);
	for($i=0; $i < $rows - 1; $i++){
		$widthText = $pdf -> getTextWidth($stdTextSize,$saleContract[$langCode]['recomen'][$i]);
		if($widthText < 546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer, 538,$stdTextSize,$bullet.$saleContract[$langCode]['recomen'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract[$langCode]['recomen'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j <= ($widthText/546); $j++){
				$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer <= 50){
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer <= 50){
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}
	
	
	//************************************************** SECTION 6: EXCLUSIONS **********************************************
	$verticalPointer -= 25;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract[$langCode]['exclu']['title'].'</b>');
	
	
	$verticalPointer -= 5;
	$verticalStep = 10;
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$rows=count($saleContract[$langCode]['exclu']);
	for($i=0; $i<$rows-1; $i++){
		//$pdf -> ezText($saleContract[$langCode]['exclu'][$i],10);
		$widthText=$pdf -> getTextWidth($stdTextSize,$saleContract[$langCode]['exclu'][$i]);
		if($widthText<546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract[$langCode]['exclu'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract[$langCode]['exclu'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j<=($widthText/546); $j++){
				$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer<=50){
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer<=50){
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}
	if(count($dataExt)>0){
		if(count($dataExt)>$displayedLimit){
			$verticalPointer= GlobalFunctions::displayExtras($pdf,$dataExt,$verticalPointer,2,$dataInfo, $langCode);
		}
	}
	
	if($display=='1'){
		$pdf -> ezStream();
	}else{
		$pdfcode = $pdf -> ezOutput();
		$fp=fopen('contracts/contract_'.$serial_sal.'.pdf','wb');
		fwrite($fp,$pdfcode);
		fclose($fp);
	}
?>