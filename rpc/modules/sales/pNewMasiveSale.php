<?php
/*
File: pNewSale.php
Author: Esteban Angulo
Creation Date: 02/03/2010
*/

//Inserts New Customer Data
$customer=new Customer($db);
 $document_cus=$_POST['txtDocumentCustomer'];
 
 /*Checks if the Customer already is registered*/
 if($customer->existsCustomer($document_cus,NULL)){ //if the customer is registered. Proceed to sell
	$serial_cus=$customer->getCustomerSerialbyDocument($document_cus);
	$customer->setSerial_cit($_POST['selCityCustomer']);
	if($_POST['txtNameCustomer']){
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
	}

	if($_POST['txtNameCustomer1']){
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
	}

	$customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
	$customer->setPhone1_cus($_POST['txtPhone1Customer']);
	$customer->setPhone2_cus($_POST['txtPhone2Customer']);
	$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
	$customer->setEmail_cus($_POST['txtMailCustomer']);

   if($_POST['txtAddress']){
		$customer->setAddress_cus(specialchars($_POST['txtAddress']));
	}
	if($_POST['txtAddress1']){
		$customer->setAddress_cus(specialchars($_POST['txtAddress1']));
	}

	if($_POST['txtRelative']){
		$customer->setRelative_cus(specialchars($_POST['txtRelative']));
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
		$customer->setRelative_cus($_POST['txtManager']);
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}

	if($_POST['hdnVip']){
			$customer->setVip_cus($_POST['hdnVip']);
	}else{
			$customer->setVip_cus('0');
	}

	$customer->setStatus_cus('ACTIVE');
	$customer->updateInfoSale($serial_cus);
	$saleData['tit']['last_name_cus']=$_POST['txtLastnameCustomer'];
	if($_POST['txtNameCustomer']){
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer'];
	}

	if($_POST['txtNameCustomer1']){
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer1'];
	}
	$saleData['tit']['document_cus']=$_POST['txtDocumentCustomer'];
	$saleData['tit']['birthdate_cus']=$_POST['txtBirthdayCustomer'];
	$saleData['tit']['email_cus']=$_POST['txtMailCustomer'];
	 if($_POST['txtAddress']){
		$saleData['tit']['address_cus']=$_POST['txtAddress'];
	}
	if($_POST['txtAddress1']){
		$saleData['tit']['address_cus']=$_POST['txtAddress1'];
	}
	$saleData['tit']['phone_cus']=$_POST['txtPhone1Customer'];
	$saleData['tit']['relative_cus']=$_POST['txtRelative'];
	$saleData['tit']['relative_phone_cus']=$_POST['txtPhoneRelative'];
	
	
	/*Generating the info for sending the e-mail*/
    if($_POST['txtNameCustomer']){
    $misc['textForEmail']='Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';

	$misc['customer']=$_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
	}
    if($_POST['txtNameCustomer1']){
    $misc['customer']=$_POST['txtNameCustomer1'];
    }
	$customer->setserial_cus($serial_cus);
	$customer->getData();
	$password=$customer->getPassword_cus();
    $misc['username']=$customer->getEmail_cus();
    $misc['pswd']='Su clave es la misma de la &uacute;ltima vez.';
	$misc['rememberPass']='En caso de que no recuerde su clave ingrese a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y haga uso de nuestro servicio: "Olvide mi clave" en la parte superior derecha de la pantalla.';
    $misc['email']=$customer->getEmail_cus();
    /*END E-mail Info*/
	$ret=executeSale($_POST,$serial_cus,$saleData);
	if(is_array($ret)){
		$error=$ret['error'].'/2/'.$ret['saleID'];
		$misc['cardNumber']=$ret['cardNumber'];
		$misc['urlGeneralConditions']=$ret['urlGenCond'];
		$serial_sal=$ret['saleID'];
		$misc['contractFilePath']=$ret['pathContract'];
		$misc['urlContract']=$ret['urlContract'];
	}else{

		$error=$ret['error'];
	}
	$misc['subject']='REGISTRO DE NUEVA VENTA MASIVA';
	if($_POST['txtMailCustomer']!=''){
		//GlobalFunctions::sendMail($misc, 'newClient');
	}
	//echo 'error1: '.$error;
	http_redirect('main/masive_sales/'.$error);
 }else{//if the customer is not registered. Inserts the new client and the proceed to sell

    $customer->setSerial_cit($_POST['selCityCustomer']);
    $customer->setType_cus($_POST['selType']);
    $customer->setDocument_cus($_POST['txtDocumentCustomer']);

    if($_POST['txtNameCustomer']){
        $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
    }

    if($_POST['txtNameCustomer1']){
        $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
    }

    $customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
    $customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
    $customer->setPhone1_cus($_POST['txtPhone1Customer']);
    $customer->setPhone2_cus($_POST['txtPhone2Customer']);
    $customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
    $customer->setEmail_cus($_POST['txtMailCustomer']);
    
   if($_POST['txtAddress']){
        $customer->setAddress_cus(specialchars($_POST['txtAddress']));
    }
    if($_POST['txtAddress1']){
        $customer->setAddress_cus(specialchars($_POST['txtAddress1']));
    }

    if($_POST['txtRelative']){
		$customer->setRelative_cus($_POST['txtRelative']);
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
		$customer->setRelative_cus(specialchars($_POST['txtManager']));
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}

    if($_POST['hdnVip']){
            $customer->setVip_cus($_POST['hdnVip']);
    }else{
            $customer->setVip_cus('0');
    }

    $customer->setStatus_cus('ACTIVE');
    $password=GlobalFunctions::generatePassword(6, 8, false);
    $customer->setPassword_cus(md5($password));

    $serialNewCus=$customer->insert();
    if($serialNewCus){
    	
    	ERP_logActivity('new', $customer);//ERP ACTIVITY
    	
	/*Customer Data Array*/
	$saleData['tit']['last_name_cus']=$_POST['txtLastnameCustomer'];
	$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer'];
	$saleData['tit']['document_cus']=$_POST['txtDocumentCustomer'];
	$saleData['tit']['birthdate_cus']=$_POST['txtBirthdayCustomer'];
	$saleData['tit']['email_cus']=$_POST['txtLastnameCustomer'];
	$saleData['tit']['address_cus']=$_POST['txtLastnameCustomer'];
	$saleData['tit']['phone_cus']=$_POST['txtPhone1Customer'];
	$saleData['tit']['relative_cus']=$_POST['txtRelative'];
	$saleData['tit']['relative_phone_cus']=$_POST['txtPhoneRelative'];




    /*Generating the info for sending the e-mail*/
    if($_POST['txtNameCustomer']){
	$misc['customer']=$_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
	}
	

    if($_POST['txtNameCustomer1']){
    $misc['customer']=$_POST['txtNameCustomer1'];
    }
	$misc['textForEmail']='Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
    $misc['username']=$customer->getEmail_cus();
    $misc['pswd']=$password;
    $misc['email']=$customer->getEmail_cus();
    /*END E-mail Info*/

	
		$ret=executeSale($_POST,$serialNewCus,$saleData);
		if(is_array($ret)){
			$error=$ret['error'].'/2/'.$ret['saleID'];
			$misc['cardNumber']=$ret['cardNumber'];
			$misc['urlGeneralConditions']=$ret['urlGenCond'];
			$serial_sal=$ret['saleID'];
			$misc['contractFilePath']=$ret['pathContract'];
			$misc['urlContract']=$ret['urlContract'];
		}else{

			$error=$ret['error'];
		}
		if($_POST['txtMailCustomer']!=''){
			//echo 'mmm2';
			//GlobalFunctions::sendMail($misc, 'newClient');
		}
		//echo 'error2: '.$error;
		http_redirect('main/masive_sales/'.$error);
    }else{
		//echo 'error3: 6';
	http_redirect('main/masive_sales/6'); //Errors inserting a new Customer.
    }

    
 }

/***********************************************
* function executeSale
@Name: executeSale
@Description: executs the proccess of a sale
@Params:
 *       N/A
@Returns: true
 *        false
***********************************************/
 function executeSale($data,$serial_cus,$saleData){
	
    //Debug::print_r($data);
	
	//echo '---------------------------------------------';
	
	//Debug::print_r($saleData);
	

     global $db;
     $sale=new Sales($db);
    //Debug::print_r($data);

    $sale->setSerial_cus($serial_cus);
    $sale->setSerial_pbd($data['selProduct']);

    $counter=new Counter($db);
	if($data['hdnSerial_cntRPC']){
		$sale->setSerial_cnt($data['hdnSerial_cntRPC']);
	}else{
		$sale->setSerial_cnt($counter->counterExists($_SESSION['serial_usr']));
	}

	if($data['hdnCardNumRPC']){ //Evaluate if we have to register the card number sold, or just the sale.
		$sale->setCardNumber_sal($data['hdnCardNumRPC']);
	}
	else{
		$sale->setCardNumber_sal($data['txtCardNum']);
	}
	


    $sale->setEmissionDate_sal($data['txtEmissionDate']);
    $sale->setBeginDate_sal($data['txtDepartureDate']);
    $sale->setEndDate_sal($data['txtArrivalDate']);
    $sale->setInDate_sal(date("d/m/Y"));
    $sale->setDays_sal($data['hddDays']);
    $sale->setFee_sal($data['hdnPrice']);
    $sale->setObservations_sal('VENTA MASIVA '.specialchars($data['observations']));
    $sale->setCost_sal($data['hdnCost']);
	$sale->setFree_sal('NO');
	$sale->setStatus_sal('REGISTERED');

    //$sale->setIdErpSal_sal();
    $sale->setType_sal('SYSTEM');
    $sale->setStockType_sal('VIRTUAL');
	if($data['hdnDirectSale']){
		$sale->setDirectSale_sal('YES');
	}
	else{
		$sale->setDirectSale_sal('NO');
	}
    $sale->setTotal_sal($data['hdnTotal']);
    $sale->setTotalCost_sal($data['hdnTotalCost']);
    $sale->setChange_fee_sal($data['rdCurrency']);
	if($data['selCityDestination']){
		$sale->setSerial_cit($data['selCityDestination']);
	}
	else{
		$sale->setSerial_cit('NULL');
	}

	$sale->setInternationalFeeStatus_sal('PAID');
	$sale->setInternationalFeeUsed_sal('YES');
	$sale->setInternationalFeeAmount_sal($data['hdnTotalCost']);

    $saleID=$sale->insert();

if($saleID){
	$error=4;
		$arr=array();
		$arr['error']=$error;
		if($data['hdnCardNumRPC']){
			$arr['cardNumber']=$data['hdnCardNumRPC'];
		}
		else{
			$arr['cardNumber']=$data['txtCardNum'];
		}
		/*GENERAL CONDITIONS FILE*/
		$country_of_sale=Sales::getCountryofSale($db, $saleID);
		if($country_of_sale!=62){
			$country_of_sale=1;
		}
		$arr['urlGenCond']=$sale->getGeneralConditionsbySale($data['selProduct'], $saleID, $country_of_sale, $_SESSION['serial_lang']);
		/*END GENERAL CONDITIONS FILE*/
		$arr['saleID']=$saleID;

		if($error==4){
			$serial_generated_sale=$saleID;
			//include(DOCUMENT_ROOT.'modules/sales/pPrintSalePDF.php');
		}
		$arr['urlContract']='contract_'.$saleID.'.pdf';
		return $arr;
    }
    else{
	  $error=7;
	  return $error;
      http_redirect('main/sales/7');
    }
 }
?>
