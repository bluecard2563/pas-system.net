<?php
/*
File: pNewSale.php
Author: Esteban Angulo
Creation Date: 02/03/2010
*/

$customer=new Customer($db);
$document_cus=$_POST['txtDocumentCustomer'];

/*Checks if the Customer already is registered*/
 if($customer->existsCustomer($document_cus,NULL)){ //if the customer is registered. Proceed to sell
	$serial_cus=$customer->getCustomerSerialbyDocument($document_cus);
	$customer->setSerial_cit($_POST['selCityCustomer']);
	if($_POST['txtNameCustomer']){
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
	}

	if($_POST['txtNameCustomer1']){
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
	}

	$customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
	$customer->setPhone1_cus($_POST['txtPhone1Customer']);
	$customer->setPhone2_cus($_POST['txtPhone2Customer']);
	$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
	$customer->setEmail_cus($_POST['txtMailCustomer']);

   if($_POST['txtAddress']){
		$customer->setAddress_cus(specialchars($_POST['txtAddress']));
	}
	if($_POST['txtAddress1']){
		$customer->setAddress_cus(specialchars($_POST['txtAddress1']));
	}

	if($_POST['txtRelative']){
		$customer->setRelative_cus(specialchars($_POST['txtRelative']));
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
		$customer->setRelative_cus($_POST['txtManager']);
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}

	if($_POST['hdnVip']){
		$customer->setVip_cus($_POST['hdnVip']);
	}else{
		$customer->setVip_cus('0');
	}

	$customer->setStatus_cus('ACTIVE');
	$customer->updateInfoSale($serial_cus);
	$saleData['tit']['last_name_cus']=$_POST['txtLastnameCustomer'];
	if($_POST['txtNameCustomer']){
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer'];
	}

	if($_POST['txtNameCustomer1']){
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer1'];
	}
	$saleData['tit']['document_cus']=$_POST['txtDocumentCustomer'];
	$saleData['tit']['birthdate_cus']=$_POST['txtBirthdayCustomer'];
	$saleData['tit']['email_cus']=$_POST['txtMailCustomer'];
	 if($_POST['txtAddress']){
		$saleData['tit']['address_cus']=$_POST['txtAddress'];
	}
	if($_POST['txtAddress1']){
		$saleData['tit']['address_cus']=$_POST['txtAddress1'];
	}
	$saleData['tit']['phone_cus']=$_POST['txtPhone1Customer'];
	$saleData['tit']['relative_cus']=$_POST['txtRelative'];
	$saleData['tit']['relative_phone_cus']=$_POST['txtPhoneRelative'];
	     
    $sessionLanguage=$_SESSION['serial_lang'];
    $question=new Question($db);
    $maxSerial=$question->getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage,$serial_cus);

	foreach($questionList as $q){
		if($q['serial_ans']){
			$answer=new Answer($db,$q['serial_ans']);
		}else{
			$answer=new Answer($db);
		}
		$answer->setSerial_cus($serial_cus);
		$answer->setSerial_que($q['serial_que']);
		$ans=$_POST['question_'.$q['serial_qbl']];
		if($ans){
			if($q['type_que']=='YN'){
				$answer->setYesno_ans($ans);
			}else{
				$answer->setAnswer_ans($ans);
			}

			if($q['yesno_ans'] or $q['answer_ans']){
				if($answer->update()){
					$flag=0;
				}else{
					$flag=1;
					break;
				}
			}else{
				if($answer->insert()){
					$flag=0;
				}else{
					$flag=1;
					break;
				}
			}
		}
	}
	
	/*Generating the info for sending the e-mail*/
    if($_POST['txtNameCustomer']){
		$misc['textForEmail']='Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="'.URL.'">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
		$misc['customer']=$_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
	}
    if($_POST['txtNameCustomer1']){
		$misc['customer']=$_POST['txtNameCustomer1'];
    }
	$customer->setserial_cus($serial_cus);
	$customer->getData();

    $misc['username']=$customer->getEmail_cus();
    $misc['pswd']='Su clave es la misma de la &uacute;ltima vez.';
    $misc['email']=$customer->getEmail_cus();
	$misc['rememberPass']='En caso de que no recuerde su clave ingrese a nuestra p&aacute;gina web <a href="'.URL.'">www.planet-assist.net</a> y haga uso de nuestro servicio: "Olvid&eacute; mi clave" en la parte superior derecha de la pantalla.';
    /*END E-mail Info*/
	$ret=executeSale($_POST,$serial_cus,$saleData);
	if(is_array($ret)){
		if($_POST['rdIsFree']=='NO') {
			$error=$ret['error'].'/2/'.$ret['saleID'];
		} else {
			$error=$ret['error'].'/2';
		}
		$misc['cardNumber']=$ret['cardNumber'];
		$misc['urlGeneralConditions']=$ret['urlGenCond'];
		$serial_sal=$ret['saleID'];
		$misc['contractFilePath']=$ret['pathContract'];
		$misc['urlContract']=$ret['urlContract'];
	}else{
		$error=$ret['error'];
	}
	$misc['subject']='REGISTRO DE NUEVA VENTA';
	//If not free-sale shows contract
	if($error==4 && $_POST['hdnFlights']=='YES'){
		if($_POST['rdIsFree']=='NO' || !isset($_POST['rdIsFree'])) {
			$error='15/2/'.$serial_sal;
		} else {
			$error='15/2/'; 
		}
	}
	if($_POST['txtMailCustomer']!=''){
		GlobalFunctions::sendMail($misc, 'newClient');
	}
	http_redirect('main/sales/'.$error);
	
 }else{//if the customer is not registered. Inserts the new client and the proceed to sell
    $customer->setSerial_cit($_POST['selCityCustomer']);
    $customer->setType_cus($_POST['selType']);
    $customer->setDocument_cus($_POST['txtDocumentCustomer']);

    if($_POST['txtNameCustomer']){
        $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
    }

    if($_POST['txtNameCustomer1']){
        $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
    }

    $customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
    $customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
    $customer->setPhone1_cus($_POST['txtPhone1Customer']);
    $customer->setPhone2_cus($_POST['txtPhone2Customer']);
    $customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
    $customer->setEmail_cus($_POST['txtMailCustomer']);
    
   if($_POST['txtAddress']){
        $customer->setAddress_cus(specialchars($_POST['txtAddress']));
    }
    if($_POST['txtAddress1']){
        $customer->setAddress_cus(specialchars($_POST['txtAddress1']));
    }

    if($_POST['txtRelative']){
		$customer->setRelative_cus($_POST['txtRelative']);
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
		$customer->setRelative_cus(specialchars($_POST['txtManager']));
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}

    if($_POST['hdnVip']){
            $customer->setVip_cus($_POST['hdnVip']);
    }else{
            $customer->setVip_cus('0');
    }

    $customer->setStatus_cus('ACTIVE');
    $password=GlobalFunctions::generatePassword(6, 8, false);
    $customer->setPassword_cus(md5($password));

    $serialNewCus=$customer->insert();
    if($serialNewCus){
    	
    	ERP_logActivity('new', $customer);//ERP ACTIVITY
    	
		/*Customer Data Array*/
		$saleData['tit']['last_name_cus']=$_POST['txtLastnameCustomer'];
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer'];
		$saleData['tit']['document_cus']=$_POST['txtDocumentCustomer'];
		$saleData['tit']['birthdate_cus']=$_POST['txtBirthdayCustomer'];
		$saleData['tit']['email_cus']=$_POST['txtLastnameCustomer'];
		$saleData['tit']['address_cus']=$_POST['txtLastnameCustomer'];
		$saleData['tit']['phone_cus']=$_POST['txtPhone1Customer'];
		$saleData['tit']['relative_cus']=$_POST['txtRelative'];
		$saleData['tit']['relative_phone_cus']=$_POST['txtPhoneRelative'];

		/*Generating the info for sending the e-mail*/
		if($_POST['txtNameCustomer']){
			$misc['customer']=$_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
		}

		if($_POST['txtNameCustomer1']){
			$misc['customer']=$_POST['txtNameCustomer1'];
		}
		$misc['textForEmail']='Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="'.URL.'">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
		$misc['username']=$customer->getEmail_cus();
		$misc['pswd']=$password;
		$misc['email']=$customer->getEmail_cus();
		/*END E-mail Info*/

		if($_POST['hdnSaveQuestions']){
			$sessionLanguage=$_SESSION['serial_lang'];
			$question=new Question($db);
			$questionList = Question::getActiveQuestions($db, $sessionLanguage);
			$flag=0;
			foreach($questionList as $q){
				$answer=new Answer($db);
				$answer->setSerial_cus($customer->getLastSerial());
				$answer->setSerial_que($q['serial_que']);
				$ans=$_POST['question_'.$q['serial_qbl']];
				if($ans){
					if($q['type_que']=='YN'){
						$answer->setYesno_ans($ans);
					}else{
						$answer->setAnswer_ans($ans);
					}
					if($answer->insert()){
						$flag=1;
					}else{
						$flag=2;
						break;
					}
				}else{
					break;
				}
			}
			if($flag==1){
			   $ret= executeSale($_POST,$serialNewCus,$saleData);
			   if(is_array($ret)){
					$error=$ret['error'].'/2/'.$ret['saleID'];
					$misc['cardNumber']=$ret['cardNumber'];
					$misc['urlGeneralConditions']=$ret['urlGenCond'];
					$serial_sal=$ret['saleID'];
					$misc['contractFilePath']=$ret['pathContract'];
					$misc['urlContract']=$ret['urlContract'];
				}else{
					$error=$ret;
				}
				if($error==4 && $_POST['hdnFlights']=='YES'){
					$error==15;
				}
				if($_POST['txtMailCustomer']!=''){
					 if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
						http_redirect('main/sales/'.$error);//http_redirect('modules/customer/fNewCustomer/1'); // Success
					}else{
						executeSale($_POST,$serialNewCus,$saleData);
						http_redirect('main/sales/'.$error); //http_redirect('modules/customer/fNewCustomer/3'); //Errors by sending e-mail
					}
				}
				else{
					http_redirect('main/sales/'.$error);
				}
			}else{
				http_redirect('main/sales/6'); //Errors inserting a new Customer.
			}
		}else{//if the client is younger than 65 years old
			$ret= executeSale($_POST,$serialNewCus,$saleData);

			if(is_array($ret)){
				if($_POST['rdIsFree']=='NO') {
					$error=$ret['error'].'/2/'.$ret['saleID'];
				} else {
					$error=$ret['error'].'/2';
				}
				$misc['cardNumber']=$ret['cardNumber'];
				$misc['urlGeneralConditions']=$ret['urlGenCond'];
				$serial_sal=$ret['saleID'];
				$misc['contractFilePath']=$ret['pathContract'];
				$misc['urlContract']=$ret['urlContract'];
				}else{
				$error=$ret;
			}

			if($error==4 && $_POST['hdnFlights']=='YES'){
				$error==15;
			}

			if($_POST['txtMailCustomer']!=''){
				if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
					//If not free sale shows contract
					if($_POST['rdIsFree']=='NO') {
						http_redirect('main/sales/'.$error.'/'.$serial_sal);
					} else {
						http_redirect('main/sales/'.$error);
					}
				}else{
					$error=5;
					http_redirect('main/sales/'.$error);
				}
			}
			else{
				http_redirect('main/sales/'.$error);
			}
		}
    } else {
		http_redirect('main/sales/6'); //Errors inserting a new Customer.
    } 
 }

/***********************************************
* function executeSale
@Name: executeSale
@Description: executs the proccess of a sale
@Params:
 *       N/A
@Returns: true
 *        false
***********************************************/
 function executeSale($data,$serial_cus,$saleData){
    global $db;
    $sale=new Sales($db);

    $sale->setSerial_cus($serial_cus);
    $sale->setSerial_pbd($data['selProduct']);
	$sale->setSerial_cur($_POST['serial_cur']);
    $counter=new Counter($db);
	if($data['hdnSerial_cntRPC']){
		$sale->setSerial_cnt($data['hdnSerial_cntRPC']);
	}else{
		$sale->setSerial_cnt($counter->counterExists($_SESSION['serial_usr']));
	}

	if($data['hdnCardNumRPC']){ //Evaluate if we have to register the card number sold, or just the sale.
		$sale->setCardNumber_sal($data['hdnCardNumRPC']);
	}
	else{
		$sale->setCardNumber_sal($data['txtCardNum']);
	}
	
    $sale->setEmissionDate_sal($data['txtEmissionDate']);
    $sale->setBeginDate_sal($data['txtDepartureDate']);
    $sale->setEndDate_sal($data['txtArrivalDate']);
    $sale->setInDate_sal(date("d/m/Y"));
    $sale->setDays_sal($data['hddDays']);
    $sale->setFee_sal($data['hdnPrice']);
    $sale->setObservations_sal(specialchars($data['observations']));
    $sale->setCost_sal($data['hdnCost']);
	if($data['rdIsFree']){
		if($data['rdIsFree']=='NO'){
			$sale->setStatus_sal('REGISTERED');
		}else{
			$sale->setStatus_sal('REQUESTED');
		}
		$sale->setFree_sal($data['rdIsFree']);
	}else{
		$sale->setFree_sal('NO');
		$sale->setStatus_sal('REGISTERED');
	}
	
    //$sale->setIdErpSal_sal();
    $sale->setType_sal('SYSTEM');
    $sale->setStockType_sal($data['sale_type']);
	$sale->setDirectSale_sal($data['hdnDirectSale']);
    $sale->setTotal_sal($data['hdnTotal']);
    $sale->setTotalCost_sal($data['hdnTotalCost']);
    $sale->setChange_fee_sal($data['rdCurrency']);
	
	if($data['selCityDestination']){
		$sale->setSerial_cit($data['selCityDestination']);
	}
	else{
		$sale->setSerial_cit('NULL');
	}

	$sale->setInternationalFeeStatus_sal('TO_COLLECT');
	$sale->setInternationalFeeUsed_sal('NO');
	$sale->setInternationalFeeAmount_sal($data['hdnTotalCost']);
	
    $saleID=$sale->insert();

	if($saleID){
		$error=4;
		
		//added for register free appliance
		if($data['rdIsFree'] && $data['rdIsFree']!='NO'){
			$alert=new Alert($db);
			$alert->setMessage_alt('Solicitud de Free pendiente.');
			$alert->setType_alt('FREE');
			$alert->setStatus_alt('PENDING');
			$alert->setRemote_id($saleID);
			$alert->setTable_name('sales/fSearchFreeSales/');

			$parameter=new Parameter($db, '17'); //Parameter for the serial of the users who have this alert (sales modifications).
			$parameter->getData();
			$destinationUsers=unserialize($parameter->getValue_par());
			$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
			if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
				$usersCountry='62';
			}
			$destinationUsers[$usersCountry]=explode(',', $destinationUsers[$usersCountry]);
			foreach($destinationUsers[$usersCountry] as $d){
				$alert->setSerial_usr($d);
				if($alert->insert()){
					$error=4;
				}else{
					$error=16;
					break;
				}
			}
		}
		//end free appliance
		
		/* REGISTER THE SALES SERVICES */
		foreach($data as $key=>$arr){
			if(!(strpos($key,"chkService_")===false)){
				if(!$sale->insertServices($saleID,$arr)){
					 http_redirect('main/sales/8');
				}
			}
		}
		/* END SERVICES */

		/* REGISTER EXTRAS INFORMATION */
		if($data['extra']){
			$customer=new Customer($db);
			$extras=new Extras($db);
			foreach($data['extra'] as $key=>$extra ){
				if($customer->existsCustomer($extra['idExtra'],NULL)){
					$serial_cus=$customer->getCustomerSerialbyDocument($extra['idExtra']);
					$customer->setSerial_cus($serial_cus);
					$customer->getData();
					$customer->setType_cus('PERSON');
					$customer->setFirstname_cus(specialchars($extra['nameExtra']));
					$customer->setLastname_cus(specialchars($extra['lastnameExtra']));
					$customer->setEmail_cus($extra['emailExtra']);
					$customer->setSerial_cit($data['selCityCustomer']);
					$customer->setStatus_cus('ACTIVE');
					$customer->updateInfoSale($serial_cus);

					$extras->setSerial_sal($saleID);
					$extras->setSerial_cus($serial_cus);
					$extras->setRelationship_ext($extra['selRelationship']);
					$extras->setFee_ext($extra['hdnPriceExtra']);
					$extras->setCost_ext($extra['hdnCostExtra']);
					
					$sessionLanguage=$_SESSION['serial_lang'];
					$question=new Question($db);
					$questionList = Question::getActiveQuestions($db, $sessionLanguage,$serial_cus);
					foreach($questionList as $q){
						if($q['serial_ans']){
						$answer=new Answer($db,$q['serial_ans']);
						}else{
							$answer=new Answer($db);
						}
						$answer->setSerial_cus($serial_cus);
						$answer->setSerial_que($q['serial_que']);
						$ans=$_POST['question'.$key.'_'.$q['serial_qbl']];

						if($ans){
							if($q['type_que']=='YN'){
								$answer->setYesno_ans($ans);

							}else{
								$answer->setAnswer_ans($ans);
							}
							if($q['yesno_ans'] or $q['answer_ans']){
								if($answer->update()){
									$error=4;
								}else{
									$error=11;
									break;
								}
							}else{
								if($answer->insert()){
									$error=4;
								}else{
									$error=11;
									break;
								}
							}
						}else{
							break;
						}
					}
					if(!$extras->insert()){
						$error=9;
					}
					else{
						$error=4;
					}
				}
				else{
					$customer->setSerial_cit($data['selCityCustomer']);
					$customer->setDocument_cus($extra['idExtra']);
					$customer->setFirstname_cus(specialchars($extra['nameExtra']));
					$customer->setLastname_cus(specialchars($extra['lastnameExtra']));
					$customer->setBirthdate_cus($extra['birthdayExtra']);
					$customer->setType_cus('PERSON');
					$customer->setEmail_cus($extra['emailExtra']);
					$customer->setStatus_cus('ACTIVE');
					$extraID = $customer->insert();
					
					ERP_logActivity('new', $customer);//ERP ACTIVITY

					if($_POST['hdnSaveQuestions']){
						$sessionLanguage=$_SESSION['serial_lang'];
						$question=new Question($db);
						$questionList = Question::getActiveQuestions($db, $sessionLanguage);

						foreach($questionList as $q){
							$answer=new Answer($db);
							$answer->setSerial_cus($customer->getLastSerial());
							$answer->setSerial_que($q['serial_que']);
							$ans=$_POST['question'.$key.'_'.$q['serial_qbl']];

							if($ans){
								if($q['type_que']=='YN'){
									$answer->setYesno_ans($ans);
								}else{
									$answer->setAnswer_ans($ans);
								}
								if($q['yesno_ans'] or $q['answer_ans']){
									if($answer->update()){
										$error=4;
									}else{
										$error=11;
										break;
									}
								}else{
									if($answer->insert()){
										$error=4;
									}else{
										$error=11;
										break;
									}
								}
							}else{
								break;
							}
						}
					}

					if($extraID){
						$extras->setSerial_sal($saleID);
						$extras->setSerial_cus($extraID);
						$extras->setRelationship_ext($extra['selRelationship']);

						if($extra['hdnPriceExtra']==0){
							$extras->setFee_ext(0.00);
							$extras->setCost_ext(0.00);
						}
						else{
							$extras->setFee_ext($extra['hdnPriceExtra']);
							$extras->setCost_ext($extra['hdnCostExtra']);
						}

						if(!$extras->insert()){
							$error=9;
						}
						else{
							$error=4;
						}
					}
					else{
						$error=10;
					}
						
				}
			}
			$saleData['extras']= $_POST['extra'];
		}
		/* END EXTRAS */

		$arr=array();
		$arr['error']=$error;
		if($data['hdnCardNumRPC']){
			$arr['cardNumber']=$data['hdnCardNumRPC'];
		}
		else{
			$arr['cardNumber']=$data['txtCardNum'];
		}
		
		/*GENERAL CONDITIONS FILE*/
		$country_of_sale=Sales::getCountryofSale($db, $saleID);
		if($country_of_sale!=62){
			$country_of_sale=1;
		}
		$arr['urlGenCond']=$sale->getGeneralConditionsbySale($data['selProduct'], $saleID, $country_of_sale, $_SESSION['serial_lang']);
		/*END GENERAL CONDITIONS FILE*/

		$arr['saleID']=$saleID;

		if($error==4){
				$serial_generated_sale=$saleID;
			if($data['rdIsFree']=='NO'){
				include(DOCUMENT_ROOT.'modules/sales/pPrintSalePDF.php');
			}
		}
		$arr['pathContract']=DOCUMENT_ROOT.'modules/sales/contracts/contract_'.$saleID.'.pdf';
		$arr['urlContract']='contract_'.$saleID.'.pdf';
		return $arr;
    }
    else{
		$error=7;
		return $error;
    }
 }
?>