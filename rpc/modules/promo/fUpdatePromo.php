<?php
/*
File: fNewPromo.php
Author: Patricio Astudillo
Creation Date: 01/02/2010 10:36 am
LastModified: 01/02/2010
Modified By: Patricio Astudillo
*/
$hdnPromoID=($_POST['selPromo']);

$promo=new Promo($db,$hdnPromoID);
$zone=new Zone($db);
$country=new Country($db);
$city=new City($db);
$dealer=new Dealer($db);
$managerbc=new ManagerbyCountry($db);
$pbc=new ProductByCountry($db);

$curDate=date("d/m/Y");

if($promo->getData()){
    $data['serial_prm']=$hdnPromoID;
    $data['creationDate']=$promo->getCreationDate_prm();
    $data['beginDate']=$promo->getBeginDate_prm();
    $data['endDate']=$promo->getEndDate_prm();
    $data['type']=$promo->getType_prm();
    $data['periodType']=$promo->getPeriodType_prm();
    $data['periodDays']=$promo->getPeriodDays_prm();
    $data['repeat']=$promo->getRepeat_prm();
    $data['typeSale']=$promo->getTypeSale_prm();
    $data['notifications']=$promo->getNotifications_prm();
    $data['name']=$promo->getName_prm();
    $data['description']=$promo->getDescripction_prm();
    $data['weekends']=$promo->getWeekends_prm();
    $data['repeatType']=$promo->getRepeatType_prm();
    $data['appliedTo']=$promo->getAppliedTo_prm();
    $data['aux']=$promo->getAux_prm();
    $data['prizes']=$promo->getDataPrizes();
    $data['aux']['dealers']=$promo->getDataDealers();
    $i=0;

	if($data['prizes']){
		foreach($data['prizes'] as $p){
			$aux=$promo->getDataProductByCountryByPrice($p['serial_pbp']);
			if(is_array($aux)){
				foreach($aux as $ast){
					$data['productsByCountryByPrize'][$i]=$ast;
					$i++;
				}
			}
		}
	}
    
    switch($data['appliedTo']){
        case 'COUNTRY':
            $data['aux']=$data['aux']['country'];
            $geoData=$country->getGeoData($data['aux']);
            break;
        case 'CITY':
            $data['aux']=$data['aux']['city'];
            $geoData=$city->getGeoData($data['aux']);
            break;
        case 'MANAGER':
            $data['aux']=$data['aux']['manager'];
            $geoData=$managerbc->getGeoData($data['aux']);
            break;
        case 'DEALER':            
            $pbd=new PromoByDealer($db);
            $data['aux']=$promo->getDataDealers();
            $geoData=$dealer->getGeoData($data['aux'][0]['serial_dea']);
            break;
    }
    if($data['periodType']=='RANDOM'&&$data['repeatType']=='WEEKLY'){
        switch ($data['periodDays']) {
            case 1:
                $data['day']='Lunes';
                break;
            case 2:
                $data['day']='Martes';
                break;
            case 3:
                $data['day']='Mi&eacute;rcoles';
                break;
            case 4:
                $data['day']='Jueves';
                break;
            case 5:
                $data['day']='Viernes';
                break;
            case 6:
                $data['day']='S&aacute;bado';
                break;
            case 7:
                $data['day']='Domingo';
                break;
        }
    }

    if($data['repeat']==1 && $data['repeatType']=='WEEKLY' && $data['periodType']=='SEVERAL DAYS'){
        $data['periodDays']=explode(',', $data['periodDays']);
    }
    $selectedProducts=$promo->getProductsByPromo();

//    Debug::print_r($data);
//    die();
    $typeList=$promo->getTypeList();
    $typeSale=$promo->getTypeSale();
    $periodType=$promo->getPeriodType();
    $repeatType=$promo->getRepeatType();
    $apliedTo=$promo->getAppliedType();

    $prize=new Prize($db);
    $prizeList=$prize->getPrize('PROMO');

    $product=new Product($db);
    $productList=$product->getProducts($_SESSION['language']);

    $zoneList=$zone->getZones();
    $countryList=$country->getCountriesByZone($geoData['serial_zon'],$_SESSION['countryList']);
    $cityList=$city->getCitiesByCountry($geoData['serial_cou'],$_SESSION['cityList']);
    $managerList=$managerbc->getManagerbyCountry($geoData['serial_cou'], $_SESSION['serial_mbc']);
    //Debug::print_r($zonesList);
    //die();
    if($geoData['serial_cit']){
         foreach($selectedProducts as $k => $s){
            $sProducts[$k]=$s['serial_pro'];
         }
         $sProducts=implode(',',$sProducts);
//         Debug::print_r($sProducts);
//         die();
         $productByCountryList=$pbc->getProductByCountrySerials($geoData['serial_cou'], $sProducts);
        foreach($productByCountryList as $k => $p){
            $productByCountryList[$k]=$p['serial_pxc'];
        }
        $productByCountryList=implode(',', $productByCountryList);
        $dealerList=$dealer->getPromoDealers($geoData['serial_cit'], $productByCountryList, '');
    }

    $smarty->register('typeList,typeSale,periodType,repeatType,apliedTo,data,prizeList,productList,selectedProducts,geoData,zoneList,countryList,cityList,managerList,dealerList,curDate');
    $smarty->display();
}else{
    http_redirect('modules/promo/fSearchPromo/7');
}
?>
