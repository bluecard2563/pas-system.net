<?php
/*
 * File: fReclaimPromo.php
 * Author: Patricio Astudillo
 * Creation Date: 07/06/2010, 08:43:01 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 07/06/2010, 08:43:01 AM
 */

Request::setString('0:error');
$promo=new Promo($db);
$apliedTo=$promo->getAppliedType();

$smarty->register('apliedTo,error');
$smarty->display();
?>
