<?php
/*
 * File: pReclaimPromo.php
 * Author: Patricio Astudillo
 * Creation Date: 14/06/2010, 02:43:33 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 14/06/2010, 02:43:33 PM
 */


$serial_prm=$_POST['selPromo'];
$promo=new Promo($db, $serial_prm);
$promo->getData();
$days=$promo->getPeriodDays_prm();
$finalDays=array();
$weekendPromo=$promo->getWeekends_prm();

//We get all the available prizes.
$pbp=new PrizeByPromo($db);
$prizesByPromo=$pbp->getPrizesByPromoToReclaim($serial_prm, $promo->getType_prm());

/*GET PREVIOUS WINNERS*/
$previousWinners=PromoWinner::hasWinners($db, $serial_prm);
if($previousWinners){
	$aux=array();
	foreach($previousWinners as $pw){
		array_push($aux, $pw['serial_dea']);
	}
	$previousWinners=implode(',', $aux);
}
/*END GETTING PREVIOUS WINNERS*/

/*Making a string with all the days that apply in the promo*/
if($promo->getRepeat_prm()=='1' && $promo->getPeriodType()=='SEVERAL DAYS'){ //It repeats in a period of time.
	if($promo->getPeriodType()=='WEEKLY'){ //It repeats Weekly
		$finalDays=explode(',', $days);
	}else{ //It repeats Monthly
		if(stripos($days, ',')){ //If there is a string with a coma
			$days=explode(',', $days);

			if(is_array($days)){
				foreach($days as $a){
					if(stripos($a, '-')){
						$aux=explode('-', $a);
						for($i=$aux['0']; $i<=$aux['1']; $i++){
							if(!in_array($i, $finalDays)){
								array_push($finalDays,$i);
							}
						}
					}else{
						array_push($finalDays,$a);
					}
				}
			}
		}elseif(stripos($days, '-')){ //If there's only a '-' in the string.
			$days=explode('-', $days);

			for($i=$days['0']; $i<=$days['1']; $i++){
				array_push($finalDays,$i);
			}
		}else{ //It's only one day in the stack
			array_push($finalDays,$days);
		}

		$finalDays=implode(',', $finalDays);
	}
}else if($promo->getRepeat_prm()=='0'){ //It doesn't repeat.
	switch($promo->getPeriodType_prm()){
		case 'ONE DAY':
			$finalDays=$promo->getBeginDate_prm();
			break;
		case 'RANDOM':
			$finalDays=$days;
			break;
	}
}
/*END making array*/

//Obtain all the suitable sales for the dealers.
$salesList=Promo::getPromoWinners($db, $serial_prm, $promo->getAppliedTo_prm(), $promo->getRepeat_prm(), $promo->getPeriodType(), $finalDays, $promo->getType_prm(), $promo->getWeekends_prm(), $promo->getRepeatType_prm(), NULL, NULL, $previousWinners);
$winnerList=array();

if(is_array($salesList)){
	$count=-1;
	$current_dealer=0;

	/*MAKING THE WINNER'S ARRAY*/
	foreach($salesList as $sl){
		if($current_dealer!=$sl['serial_dea']){
			$current_dealer=$sl['serial_dea'];
			$count++;
			$winnerList[$count]['serial_dea']=$current_dealer;
			$winnerList[$count]['name_dea']=$sl['name_dea'];
			$winnerList[$count]['total_sold']=$sl['amount_available'];

			foreach($prizesByPromo as $p){
				$aux=Promo::getPromoWinners($db, $serial_prm, $promo->getAppliedTo_prm(), $promo->getRepeat_prm(), $promo->getPeriodType(), $finalDays, $promo->getType_prm(), $promo->getWeekends_prm(), $promo->getRepeatType_prm(), $sl['serial_dea'], $p['serials']);

				/*MAKE CALCULATIONS FOR NET PROMOS*/
				if($promo->getTypeSale_prm()=='NET'){
					$eTaxes=unserialize($aux['0']['applied_taxes_inv']);
					$eTaxesAmount=0;
					if(is_array($eTaxes)){
						foreach($eTaxes as $t){
							$eTaxesAmount+=$t['tax_percentage'];
						}
						$eTaxesAmount=$eTaxesAmount/100;
					}

					$eDiscounts=$aux['0']['discounts']/100;
					$total_product_sold=$aux['0']['amount_sold']-($aux['0']['amount_sold']*$discounts);
					$total_product_sold+=($total_product_sold*$eTaxesAmount);
				}else{
					$total_product_sold=$aux['0']['amount_sold'];
				}
				/*END CALCULATIONS*/

				$winnerList[$count]['amounts_available'][$p['serials']]=$total_product_sold;
			}
		}else{
			$winnerList[$count]['total_sold']+=$sl['amount_available'];
		}
	}
	//Debug::print_r($winnerList);
	/*END ARRAY*/

	//Transforming the promo object into an array
	$promo=get_object_vars($promo);
	unset($promo['db']);
	$neededPrizes=array();

	/*Sweeping the prize array*/
	if(is_array($prizesByPromo)){
		foreach($winnerList as $key=>&$w){
			$w['prizes']=array();
			foreach($prizesByPromo as &$item){
				/*GETTIGN THE AMOUNTS */
				$amountSoldPerProduct=$w['amounts_available'][$item['serials']];
				$item['reclaimed_amount']=$amountSoldPerProduct;
				/*END AMOUNTS*/

				if($promo['type_prm']=='PERCENTAGE'){ //PERCENTAGE of values.
					$item['base_amount']=($item['starting_value_pbp']/100)*$w['total_sold'];
					//echo 'Productos: '.$item['serials'].' Disponible: '.$amountSoldPerProduct.' - Desde: '.$item['base_amount'].'<br>';

					if($amountSoldPerProduct>=$item['base_amount'] && $item['base_amount']>0){
						$w['used']='YES';
						$item['quantity']=1;
						$item['amountSoldToReclaim']=$amountSoldPerProduct;
						array_push($w['prizes'], $item);
					}
				}else if($promo['type_prm']=='RANGE'){ //RANGE of values.
					$item['base_amount']=$item['starting_value_pbp'];
					//echo 'Disponible: '.$amountSoldPerProduct.' - Desde: '.$item['base_amount'].' Hasta: '.$item['end_value_pbp'].'<br>';
					if($amountSoldPerProduct>=$item['base_amount'] && $amountSoldPerProduct<$item['end_value_pbp']){
						$w['used']='YES';
						$item['quantity']=1;
						$item['amountSoldToReclaim']=$amountSoldPerProduct;
						array_push($w['prizes'], $item);
					}
				}else{//AMOUNT
					$item['base_amount']=$item['starting_value_pbp'];
					//echo 'Disponible: '.$amountSoldPerProduct.' - Desde: '.$item['starting_value_pbp'].'<br>';
					if($amountSoldPerProduct>=$item['starting_value_pbp']){
						$w['used']='YES';
						$item['amountSoldToReclaim']=$amountSoldPerProduct;
						$prizeQ=(int)($amountSoldPerProduct/$item['starting_value_pbp']);
						$item['quantity']=$prizeQ;

						if($prizeQ>1){
							$amountSoldPerProduct-=($amountSoldPerProduct*$prizeQ);
						}else{
							$amountSoldPerProduct-=$item['starting_value_pbp'];
						}

						$w['amounts_sold'][$item['serials']]=$amountSoldPerProduct;
						array_push($w['prizes'], $item);
					}
				}
			}
			/*END FOREACH PRIZES*/

			if($w['used']!='YES'){
				unset($winnerList[$key]);
			}
		}
		/*END FOREACH WINNERS*/

		/*MARKING ALL THE WINNERS IN DB*/
		$error=1; //All process is OK
		$pWinner=new PromoWinner($db);
		$pWinner->setSerial_usr($_SESSION['serial_usr']);
		$pWinner->setSerial_prm($serial_prm);

		foreach($winnerList as $w){
			$pWinner->setSerial_dea($w['serial_dea']);
			foreach($w['prizes'] as $p){
				$pWinner->setSerial_pbp($p['serial_pbp']);
				$pWinner->setAmount_prw($p['reclaimed_amount']);

				if(!$pWinner->insert()){
					$error=3;
					break;
				}else{
					Prize::substractStock($db, $p['serial_pri'], $p['quantity']);
				}
			}

			if($error==3){
				break;
			}
		}
		/*END MARKING WINNERS*/
	}else{//No enough prizes
		$error=4;
	}
	/*END IF_ARRAY*/
}else{//There isn't a winner list
	$error=2;
}

http_redirect('modules/promo/reclaimPromo/fReclaimPromo/'.$error);
?>
