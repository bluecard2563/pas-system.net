<?php
/*
File:pUpdatePromo
Author: Patricio Astudillo
Creation Date: 07/02/2010 15:24
LastModified: 07/02/2010
Modified By: Patricio Astudillo
*/

$apliedTo=$_POST['selApliedTo'];
$serial_prm=$_POST['hdnPromoID'];
$promo=new Promo($db,$serial_prm);

if($_POST['selPeriod']=='SEVERAL DAYS' and $_POST['selRepeatType']=='MONTHLY'){
    $days=split('[,-]', $_POST['weekDayInfo']);
    for($i=0;$i<count($days)-1;$i++){
        for($j=$i+1;$j<count($days);$j++){
            if($days[$j]<$days[$i]){
                http_redirect('modules/promo/fSearchPromo/8');
                break;
            }
        }
    }
}

if($promo->getData()){
    $oldApliedTo=$promo->getAppliedTo_prm();

    /*Updates the promo Basic Information*/
    $promo->setEndDate_prm($_POST['txtEndDate']);
    $promo->setBeginDate_prm($_POST['txtBeginDate']);
    $promo->setType_prm($_POST['selType']);
    $promo->setPeriodType_prm($_POST['selPeriod']);
    $promo->setRepeat_prm($_POST['rdRepeat']);
    $promo->setTypeSale_prm($_POST['selSaleType']);
    $promo->setNotifications_prm($_POST['rdNotification']);
    $promo->setName_prm($_POST['txtName']);
    $promo->setDescripction_prm($_POST['txtDescription']);
    if(!$_POST['weekDayInfo']){
        $promo->setPeriodDays_prm($_POST['hdnRandom']);
        //die($_POST['hdnRandom'].", ".$_POST['rdWeekends'].", ".$apliedTo);
    }else if(is_array($_POST['weekDayInfo'])){
        $promo->setPeriodDays_prm(implode(',', $_POST['weekDayInfo']));
    }else{
        $promo->setPeriodDays_prm($_POST['weekDayInfo']);
    }
    $promo->setRepeatType_prm($_POST['selRepeatType']);
    $promo->setAppliedTo_prm($apliedTo);
    $promo->setWeekends_prm($_POST['rdWeekends']);

    if($promo->update()){        
        $error=1;        
        switch($apliedTo){ /*WE insert the details for the promotion according to it's type*/
            case 'COUNTRY':
                switch($oldApliedTo){
                    case 'COUNTRY':
                        $pbcc=new PromoByCountry($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'CITY':
                        $pbcc=new PromoByCity($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'MANAGER':
                        $pbm=new PromoByManager($db);
                        $pbm->delete($serial_prm);
                        break;
                    case 'DEALER':
                        $pbd=new PromoByDealer($db);
                        $pbd->delete($serial_prm);
                        break;
                }
                $pbc=new PromoByCountry($db);
                $pbc->setSerial_cou($_POST['selCountry']);
                $pbc->setSerial_prm($serial_prm);

                if($pbc->insert()){
                    $error=1;
                }else{
                    $error=3;
                }
                break;
            case 'CITY':
                switch($oldApliedTo){
                    case 'COUNTRY':
                        $pbcc=new PromoByCountry($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'CITY':
                        $pbcc=new PromoByCity($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'MANAGER':
                        $pbm=new PromoByManager($db);
                        $pbm->delete($serial_prm);
                        break;
                    case 'DEALER':
                        $pbd=new PromoByDealer($db);
                        $pbd->delete($serial_prm);
                        break;
                }

                $pbc=new PromoByCity($db);
                $pbc->setSerial_cit($_POST['selCity']);
                $pbc->setSerial_prm($serial_prm);

                if($pbc->insert()){
                    $error=1;
                }else{
                    $error=4;
                }

                break;
            case 'MANAGER':
                switch($oldApliedTo){
                    case 'COUNTRY':
                        $pbcc=new PromoByCountry($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'CITY':
                        $pbcc=new PromoByCity($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'MANAGER':
                        $pbm=new PromoByManager($db);
                        $pbm->delete($serial_prm);
                        break;
                    case 'DEALER':
                        $pbd=new PromoByDealer($db);
                        $pbd->delete($serial_prm);
                        break;
                }

                $pbm=new PromoByManager($db);
                $pbm->setSerial_mbc($_POST['selManager']);
                $pbm->setSerial_prm($serial_prm);

                if($pbm->insert()){
                    $error=1;
                }else{
                    $error=5;
                }

                break;
            case 'DEALER':
                switch($oldApliedTo){
                    case 'COUNTRY':
                        $pbcc=new PromoByCountry($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'CITY':
                        $pbcc=new PromoByCity($db);
                        $pbcc->delete($serial_prm);
                        break;
                    case 'MANAGER':
                        $pbm=new PromoByManager($db);
                        $pbm->delete($serial_prm);
                        break;
                    case 'DEALER':
                        $pbd=new PromoByDealer($db);
                        $pbd->delete($serial_prm);
                        break;
                }

                $pbd=new PromoByDealer($db);
                foreach($_POST['dealersTo'] as $d){
                    $pbd->setSerial_dea($d);
                    $pbd->setSerial_prm($serial_prm);
                    $serial_pbd=$pbd->insert();
                    if(!$serial_pbd){
                        $error=6;
                        break;
                    }else{
                        $error=1;
                    }
                }
                break;
        }        
        if($error==1){
            $pbps=new PrizeByPromo($db);
            $pbcbp=new ProductByCountryByPrize($db);
            $pbpArray=$pbps->getPrizesByPromo($serial_prm);
            foreach($pbpArray as $pb){
                if(!$pbcbp->delete($pb['serial_pbp'])){
                    $error=2;
                    break;
                }
            }
            $pbps->delete($serial_prm);
            for($i=0;$i<sizeof($_POST['selPrize']);$i++){                
                $pbps=new PrizeByPromo($db);
                $pbps->setSerial_prm($serial_prm);
                $pbps->setSerial_pri($_POST['selPrize'][$i]);
                $pbps->setStarting_value_pbp($_POST['txtBeginP'][$i]);
                $pbps->setEnd_value_pbp($_POST['txtEndP'][$i]);
                $serial_pbp=$pbps->insert();
                if($serial_pbp /*&& $error!=2*/){
                    for($j=0;$j<sizeof($_POST['selProduct'][$i+1]);$j++){
                        $pbcbp=new ProductByCountryByPrize($db,$serial_pbp,$_POST['selProduct'][$i+1][$j]);
                        if(!$pbcbp->insert()){
                          $error=10;
                         /* break;*/
                        }
                    }
                }else{
                   $error=10;
                   /*break;*/
                }
                
            }

        }
        /*E-MAIL INFO*/
        $misc['textForEmail']="Se ha actualizado una promoci�n a clientes, los par&aacute;metros de la misma son los siguientes: ";
        $misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
        $misc['endDate']=$promo->getEndDate_prm();
        $misc['beginDate']=$promo->getBeginDate_prm();
        $misc['type']=$promo->getType_prm();
        $misc['periodType']=$promo->getPeriodType_prm();
        $misc['repeat']=$promo->getRepeat_prm();
        $misc['saleType']=$promo->getTypeSale_prm();
        $misc['notification']=$promo->getNotifications_prm();
        $misc['name']=$promo->getName_prm();
        $misc['description']=$promo->getDescripction_prm();
        $misc['periodDays']=$promo->getPeriodDays_prm();
        $misc['typeSale']=$promo->getTypeSale_prm();
        if(!$misc['periodDays']){
           $misc['periodDays']=$misc['beginDate'];
        }
        $misc['repeatType']=$promo->getRepeatType_prm();
        $misc['apliedTo']=$promo->getAppliedTo_prm();
        $misc['weekends']=$promo->getWeekends_prm();
        switch($apliedTo){
            case 'COUNTRY':
                $promobc=new PromoByCountry($db);
                $misc['participants']=$promobc->getCountriesByPromo($serial_prm);
                foreach($misc['participants'] as &$p){
                    $p=$p['name_cou'];
                }
                $emails=array();
                break;
            case 'CITY':
                $promobc=new PromoByCity($db);
                $misc['participants']=$promobc->getCitiesByPromo($serial_prm);
                foreach($misc['participants'] as &$p){
                    $p=$p['name_cit'];
                }
                break;
            case 'MANAGER':
                $promobm=new PromoByManager($db);
                $misc['participants']=$promobm->getManagersByPromo($serial_prm);
                foreach($misc['participants'] as &$p){
                    $p=$p['name_man'];
                }
                break;
            case 'DEALER':
                $promobd=new PromoByDealer($db);
                $misc['participants']=$promobd->getDealersByPromo($serial_prm);
                foreach($misc['participants'] as &$p){
                    $p=$p['name_dea'];
                }
                break;
        }
        if($apliedTo=='MANAGER'){
            $mbc=new ManagerbyCountry($db, $_POST['selManager']);
            $mbc->getData();
            $manager=new Manager($db,$mbc->getSerial_man());
            $emails[0]=$manager->getContact_email_man();
        }else{
            $mbc=new ManagerbyCountry($db);
            $managers=$mbc->getManagerByCountry($_POST['selCountry'],$_SESSION['serial_mbc']);
            foreach($managers as $k=>$man){
                $emails[$k]=$man['contact_email_man'];
            }
        }
        $misc['emails']=implode(',',$emails);
        $prizebp=new PrizeByPromo($db);
        $misc['conditions']=$prizebp->getPrizesByPromo($serial_prm);
        $productbcbp=new ProductByCountryByPrize($db);
        foreach($misc['conditions'] as &$p){
            $p['products']=$productbcbp->getProductsByPromo($p['serial_pbp'], $_SESSION['serial_lang']);
        }

        /*END E-MAIL INFO*/

        if(!GlobalFunctions::sendMail($misc, 'newPromo')){
            $error==9;
        }
        http_redirect('modules/promo/fSearchPromo/'.$error);
    }else{
        http_redirect('modules/promo/fSearchPromo/2');
    }
    /*END Updates*/
}
?>
