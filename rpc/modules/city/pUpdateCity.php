<?php
/*
File: pUpdateCity.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 15:48
Modified By: David Bergmann
*/

$city=new City($db);	

$data['serial_cit'] = $_POST['hdnSerial_cit'];	
$city -> setSerial_cit($_POST['hdnSerial_cit']);
$city -> getData();
$data['serial_cou'] = $city -> getSerial_cou();	
$data['name_cit'] = $_POST['txtNameCity'];
$city->setName_cit($_POST['txtNameCity']);
$data['code_cit'] = $_POST['txtCodeCity'];
$city->setCode_cit($_POST['txtCodeCity']);

if($city -> update()) {
	http_redirect('modules/city/fSearchCity/1');
}
else {
	$error=1;
}
$smarty->register('error,data');
$smarty->display('modules/city/fUpdateCity.'.$_SESSION['language'].'.tpl');

?>