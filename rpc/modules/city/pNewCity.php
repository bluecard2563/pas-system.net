<?php 
/*
File: pNewCity.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 
Modified By: 
*/
$city=new City($db);
$city->setSerial_cou($_POST['selCountry']);
$city->setName_cit($_POST['txtNameCity']);
$city->setCode_cit($_POST['txtCodeCity']);
if($city->insert()){
    $user=new User($db,$_SESSION['serial_usr']);
    $user->getData();
    if($user->isPlanetAssistUser()){
        $_SESSION['cityList']=$_SESSION['cityList'].",".$db->insert_ID();
    };
    http_redirect('modules/city/fNewCity/1');
}else{
    http_redirect('modules/city/fNewCity/2');
}
	
?>