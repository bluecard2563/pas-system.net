<?php 
/*
File: fNewManager.php
Author: Santiago Benítez
Creation Date: 01/02/2010 15:07
Last Modified: 01/02/2010 15:07
Modified By: Santiago Benítez 
*/

Request::setInteger('0:error');


$country=new Country($db);
$mbc= new ManagerbyCountry($db);
$manager= new Manager($db);
$zone= new  Zone($db);

/*Recover all available PaymentDeadline of managers from the system*/
$paymentDeadlineList=$mbc->getAllPaymentDeadline();

/*Recover all available type of managers from the system*/
$typeList=$manager->getAllTypes();

/*Recover all available type of percentage types from the system*/
$typePercentageList=$mbc->getAllPercentageTypes();

/*Recover all available status of managers from the system*/
$statusList=$mbc->getAllStatus();

/*Recover all available invoice number of managers from the system*/
$invoiceNumberList=$mbc->getAllInvoiceNumber();

/*Returns true if there already exists an exclusive manager, false if there doesn't*/
$exclusiveCountryList=$mbc->getExclusiveCountries();

$zonesList=$zone->getZones();

$smarty->register('error,userList,paymentDeadlineList,typeList,countryList,statusList,exclusiveCountryList,zonesList,typePercentageList,invoiceNumberList');
$smarty->display();
?>