<?php 
/*
File: fUpdateManager.php
Author: Santiago Benítez
Creation Date: 05/02/2010 15:23
Last Modified: 12/02/2010 14:50
Modified By: Santiago Benítez 
*/

Request::setInteger('0:error');

Request::setString('hdnManagerID');

$country=new Country($db);
$city=new City($db);
$mbc= new ManagerbyCountry($db);
$zone= new  Zone($db);
$manager= new Manager($db,$hdnManagerID);

if($manager->getData()){
	$data['serial_man'] = $hdnManagerID;
	$data['address_man'] = $manager -> getAddress_man();
	$data['contact_email_man'] = $manager ->getContact_email_man();
	$data['contact_man'] = $manager ->getContact_man();
	$data['contact_phone_man'] = $manager ->getContact_phone_man();
	$data['document_man'] = $manager ->getDocument_man();
	$data['contract_date_man'] = $manager ->getContract_date_man();
	$data['name_man'] = $manager ->getName_man();
	$data['phone_man'] = $manager ->getPhone_man();
	$data['serial_cit'] = $manager ->getSerial_cit();
	$data['type_man'] = $manager ->getType_man();
	$data['sales_only_man'] = $manager ->getSales_only_man();
	$data['aux'] = $manager ->getAuxData_man();
	$data['aux'] = array_merge($data['aux'], ManagerbyCountry::getManagerCommonData($db, $data['serial_man']));
	//Debug::print_r($data);
}else{
	http_redirect('modules/manager/fSearchManager/3');
}


/*Recover all available PaymentDeadline of managers from the system*/
$paymentDeadlineList=$mbc->getAllPaymentDeadline();

/*Recover all available type of managers from the system*/
$typeList=$manager->getAllTypes();

/*Recover all available type of percentage types from the system*/
$typePercentageList=$mbc->getAllPercentageTypes();

/*Recover all available status of managers from the system*/
$statusList=$mbc->getAllStatus();

/*Recover all available invoice number of managers from the system*/
$invoiceNumberList=$mbc->getAllInvoiceNumber();

/*Returns true if there already exists an exclusive manager, false if there doesn't*/
$exclusiveCountryList=$mbc->getExclusiveCountries();

$zonesList=$zone->getZones();

$countryList=$country->getCountriesByZone($data['aux']['serial_zon'],$_SESSION['countryList']);
//Debug::print_r($countryList);

$cityList=$city->getCitiesByCountry($data['aux']['serial_cou'],$_SESSION['cityList']);

$maxSerial=$mbc->getMaxSerial();


$smarty->register('error,userList,paymentDeadlineList,typeList,statusList,exclusiveCountryList,typePercentageList,data,cityList,countryList,zonesList,invoiceNumberList');
$smarty->display();
?>