<?php
/*
    Document   : pAssignAlerts.php
    Created on : Apr 29, 2010, 3:53:13 PM
    Author     : H3Dur4k
    Description:
       Registers the alerts reassign.
*/
Request::setInteger('selToGroup');
$error=1;
if($selToGroup){
	$alert=new Alert($db);
	if(is_array($_POST['chkAlerts'])){
		foreach($_POST['chkAlerts'] as $alerts){
			$alert->setSerial_alt($alerts);
			$alert->getData();
			$alert->setRemote_id($selToGroup);
			if(!$alert->update()){
				$error=3;
				break;
			}
		}
	}else{
		$error=2;
	}
}else{
	$error=2;
}
http_redirect("modules/alerts/fAssignAlerts/$error");
?>
