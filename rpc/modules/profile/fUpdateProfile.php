<?php session_start();
/*
File: fUpdateProfile.php
Author: Esteban Angulo
Creation Date: 30/12/2009 11:20
Last Modified: 03/02/2010
Modified By: David Bergmann
*/


$code_lang=$_SESSION['language'];
Request::setInteger('0:error');
$serial_lang=1;

$serial_pbc = $_POST['selProfile'];
$profile=new ProfileByCountry($db);
$allOptions=$profile->show_options($_SESSION['language']);
$profile->setSerial_pbc($serial_pbc);

//$profile_by_country=new ProfileByCountry($db);

$statusList=$profile->getAllStatus();

$country= new Country($db);
$countryList=$country->getAllCountry();

if(isset($_POST['selProfile'])){

$profile -> getData();

$name = $profile->getName_pbc();
$country =$profile->getSerial_cou();
$status = $profile->getStatus_pbc();

$profileOpt=$profile->show_options($_SESSION['language']);
$options=array();

$info=getOptions($allOptions,$profileOpt,$options);
$name=$name;
}

/***********************************************
@Name: getOptions
@Description: Runs over the hole allOptions array and calls the function search
@Params: $allOptions -> All Options array
 *		 $data-> Array with all profiles with its own options
 *		 $hasOptions-> Array that will be returned with all data processed
@Returns: $hasOptions -> Array with all the options of the system and which profile has each option
***********************************************/
function getOptions($allOptions,$data,&$hasOptions,$tab='',&$superParent=NULL) {
	foreach($allOptions as $all){
		$aux['serial']=$all['serial_opt'];
		$aux['name']=html_entity_decode($tab).utf8_decode(utf8_encode($all['name_obl']));
		$aux['chkName']=$all['name_obl'];
		$aux['parent_key']=$all['opt_serial_opt'];
		if($all['opt_serial_opt']==''){
			$superParent=$all['serial_opt'];
		}
		if(is_array($all['options'])){
			$aux['child']='YES';
		}
		else{
			$aux['child']='NO';
		}
		$aux['superParent']=$superParent;
		array_push($hasOptions,search($all,$data,$aux));
		if(is_array($all['options'])){
			getOptions($all['options'],$data,$hasOptions,$tab.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$superParent);
		}
	}
	return $hasOptions;
}


/***********************************************
@Name: search
@Description: Runs over profile options array and calls the function find Options
@Params: $all -> All Options array
 *		 $data-> Array with all profiles with its own options
 *		 $aux-> Array that will be returned with all data processed
@Returns: $aux -> Array with all the options of the a profile
***********************************************/
function search($all,$data,$aux){
	foreach($data as $prf){
		if(findOptions($all['serial_opt'],$prf['options'])){
			$aux['checked']='X';
			break;
		}
		else{
			$aux['checked']=' ';
		}
	}
	return $aux;
}


/***********************************************
@Name: findOptions
@Description: Compare the searched option with the options that the profile has
@Params: $serial_opt -> Serached Option
 *		 $optByPrf-> Array with all Options of the profile
@Returns: true-> if found
 *		  false-> if not found
***********************************************/
function findOptions($serial_opt, $optByPrf){
	$return = false;
	if(is_array($optByPrf)){
		foreach($optByPrf as $key=>$opt){
			if($serial_opt == $opt['serial_opt']){
				$return = true;
				break;
			} else {
				if(is_array($opt['options'])){
					if(findOptions($serial_opt, $opt['options'])){
						$return = true;
						break;
					}
				}
			}
		}
	}
	return $return;
}

$smarty->register('error,statusList,optionsList,optionsProfile,serial_pbc,name,status,country,countryList,info');

$smarty->display();

?>