<?php
/*
File: fSearchProfile.php
Author: Esteban Angulo
Creation Date: 30/12/2009 11:00
Last Modified: 03/02/2010
Modified By: David Bergmann
*/

Request::setInteger('0:error');
$profile = new ProfileByCountry($db);
$profiles = $profile -> getProfile_by_countryList($_SESSION['countryList']);
$country=new Country($db);

/*LOADING DEFAULT DATA*/
//Countries
$countryList=$country->getOwnCountries($_SESSION['countryList']);


foreach($profiles as &$p){
	$p['name_pbc']=$p['name_pbc'];
}
foreach($profiles as $key=>&$p){
	if($p['serial_pbc'] == 1){
		unset($profiles[$key]);
	}
}

$smarty->register('profiles,error,countryList');
$smarty->display();
?>