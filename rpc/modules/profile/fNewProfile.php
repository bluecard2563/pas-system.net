<?php session_start();
/*
File: fNewProfile.php
Author: Esteban Angulo
Creation Date: 28/12/2009 18:00
Last Modified: 03/02/2010
Modified By: David Bergmann
*/
Request::setInteger('0:error');
$code_lang=$_SESSION['language'];
$serial_lang=1;


$profile_by_country=new ProfileByCountry($db);
$country= new Country($db);
$statusList=$profile_by_country->getAllStatus();


$countryList=$country->getAllCountry();

$allOptions=$profile_by_country->show_options($_SESSION['language']);
$hasOptions=array();
$info=getOptions($allOptions,$hasOptions);

/***********************************************
@Name: getOptions
@Description: Runs over the hole allOptions array and idents each option acording to what correspond (parent or children)
@Params: $allOptions -> All Options array
 *		 $hasOptions-> Array that will be returned with all data processed
 *		 $tab-> Spaces used fot the identation process
@Returns: $hasOptions -> Array with all the options of the system and idented acording to what correspond (parent or children)
***********************************************/
function getOptions($allOptions,&$hasOptions,$tab='',&$superParent=NULL) {
	foreach($allOptions as $all){
		$aux['serial']=$all['serial_opt'];
		$aux['name']=html_entity_decode($tab).utf8_decode(utf8_encode($all['name_obl']));
		$aux['chkName']=$all['name_obl'];
		$aux['parent_key']=$all['opt_serial_opt'];
		if($all['opt_serial_opt']==''){
			$superParent=$all['serial_opt'];
		}
		if(is_array($all['options'])){
			$aux['child']='YES';
		}
		else{
			$aux['child']='NO';
		}
		$aux['superParent']=$superParent;
		array_push($hasOptions,$aux);
		if(is_array($all['options'])){
			getOptions($all['options'],$hasOptions,$tab.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$superParent);
		}
	}
	return $hasOptions;
}

$smarty->register('error,statusList,optionsList,countryList,info');
$smarty->display();
?>
