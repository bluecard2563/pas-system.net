<?php
/*
File: pUpdateSingleBonus
Author: David Bergmann
Creation Date: 22/03/2010
Last Modified:
Modified By:
*/

$bonus = new Bonus($db, $_POST['hdnSerialBon']);
$bonus->getData();
$serial_dea = $bonus->getSerial_dea();
$serial_pxc = $bonus->getSerial_pxc();
$bonus->setPercentage_bon($_POST['txtPercentage']);

if($bonus->deactivate()) {
    $newBonus = new Bonus($db);
    $newBonus->setSerial_dea($serial_dea);
    $newBonus->setSerial_pxc($serial_pxc);
    $newBonus->setPercentage_bon($_POST['txtPercentage']);
    if($newBonus->insert()) {
        http_redirect('modules/bonus/fSearchBonus/1');
    }
}
http_redirect('modules/bonus/fSearchBonus/2');
?>
