<?php
/*
    Document   : pAuthorizeOutDateBonus
    Created on : 21-jun-2010, 15:41:12
    Author     : Nicolas
    Description:
        Register the bonus which has been authorized or denied.
*/

Request::setString('selToAuthorize');
$to_authorize=$_POST['chkBonus'];
$error=1;
if($selToAuthorize && $to_authorize){
	$apBonus=new AppliedBonus($db);
	foreach($to_authorize as $bonus){
		$apBonus->setSerial_apb($bonus);
		if(!$apBonus->authorizeBonus($selToAuthorize, $_SESSION['serial_usr'])){
			$error=3;
			break;
		}
	}
}else{
	$error=2;
}
http_redirect('modules/bonus/fAuthorizeOutDateBonus/'.$error);
?>
