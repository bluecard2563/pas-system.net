<?php
/*
    Document   : fPayBonus
    Created on : 11-jun-2010, 10:04:59
    Author     : Nicolas
    Description:
       Creates an interface to choose the bonus to be paid.
*/
Request::setString('selBonusTo');
Request::setString('selBranch');
Request::setString('selToPay');
if($selBonusTo=='COUNTER'){
	Request::setInteger('selCounter');
	$serial_to=$selCounter;
}else{
	$serial_to=$selBranch;
}

//validate if there is enought data for the process
if(($selBonusTo && $selCounter && $selToPay)||($selBonusTo && $selBranch && $selToPay)){
	//initialize vars
	$apBonus=new AppliedBonus($db);
	$bonusToPay=array();
	$onDateBonus=array();
	$outDateBonus=array();
	$creditBonus=0;
	$againstBonus=0;
	$totalBonus=0;
	//check if is going to pay bonus ON date,bonus OUT date or both.
	//get On date date
	if($selToPay=='BOTH' || $selToPay=='ON'){
		if($selBonusTo=='DEALER'){
			$onDateBonus=$apBonus->getOnDateBonus($selBranch,$selBonusTo);
		}else{
			$onDateBonus=$apBonus->getOnDateBonus($selCounter,$selBonusTo);
		}
	}
	//get out date data
	if($selToPay=='BOTH' || $selToPay=='OUT'){
		if($selBonusTo=='DEALER'){
			$outDateBonus=$apBonus->getOutDateBonus($selBranch,$selBonusTo);
		}else{
			$outDateBonus=$apBonus->getOutDateBonus($selCounter,$selBonusTo);
		}
	}
//merge the bonus if neccesary
$bonusToPay=array_merge($onDateBonus, $outDateBonus);
//calculate the credit, against and total bonus
	foreach($bonusToPay as $bp){
		if($bp['status_apb']=='ACTIVE'){
			$creditBonus+=$bp['pending_amount_apb'];
		}elseif($bp['status_apb']=='REFUNDED'){
			$againstBonus+=$bp['pending_amount_apb'];
		}
	}
	$totalBonus=$creditBonus-$againstBonus;
	//get the available prizes
	$prizes=new Prize($db);
	//get the dealer contry to get the prizes
	$dealer=new Dealer($db,$selBranch);
	$dealer->getData();
	$aux=$dealer->getAuxData_dea();
	$availablePrizes=$prizes->getBonusPrizesByCountry($aux['serial_cou'],$totalBonus);
	$smarty->register('selBonusTo,serial_to,selToPay,availablePrizes,totalBonus,againstBonus,creditBonus');
}
$smarty->display();
?>
