<?php
/*
    Document   : pPayBonus
    Created on : 14-jun-2010, 14:44:25
    Author     : Nicolas
    Description:
        Register the bonus loquidation
*/
//Debug::print_r($_POST);
Request::setString('payTo');
Request::setString('serialTo');
Request::setString('selToPay');
Request::setFloat('liquidationTotal');
$error=1;
$prizes_applied=array();
$prizes_for_pdf=array();
$bonus_for_pdf=array();
$liquidationTotalForPdf=$liquidationTotal;


//validate if there is enought data for the process
if($payTo && $serialTo && $selToPay){
	//initialize vars
	$apBonus=new AppliedBonus($db);
	$bonusToPay=array();
	$onDateBonus=array();
	$outDateBonus=array();
	//check if is going to pay bonus ON date,bonus OUT date or both.
	//get On date date
	if($selToPay=='BOTH' || $selToPay=='ON'){
			$onDateBonus=$apBonus->getOnDateBonus($serialTo,$payTo);
	}
	//get out date data
	if($selToPay=='BOTH' || $selToPay=='OUT'){
			$outDateBonus=$apBonus->getOutDateBonus($serialTo,$payTo);
	}

//merge the bonus if neccesary
$bonusToPay=array_merge($onDateBonus, $outDateBonus);
//check if there are pending values, and bonus to pay from
	if($liquidationTotal>0 && is_array($bonusToPay)){
		//prizes data
		foreach($_POST['chkPrizes'] as $pa){
			$prize=new Prize($db,$pa);
			$prize->getData();
			//get data for updates
			array_push($prizes_applied,array('serial_pri'=>$prize->getSerial_pri(),
											 'name_pri'=>$prize->getName_pri(),
											 'coverage_pri'=>$prize->getCoverage_pri(),
											 'quantity'=>$_POST['quantity_'.$pa]));
			//get data for PDF
			array_push($prizes_for_pdf,array('name_pri'=>$prize->getName_pri(),
											 'quantity'=>($prize->getCoverage_pri()=='TOTAL')?'$'.$_POST['quantity_'.$pa]:$_POST['quantity_'.$pa].' u'));
		}
		//evaluate available bonus and store for PDF
		foreach($bonusToPay as $btp){
			if($btp['pending_amount_apb']>0){
				if($btp['pending_amount_apb']<$liquidationTotalForPdf){
					$liquidationTotalForPdf-=$btp['pending_amount_apb'];
					$apBonus->setSerial_apb($btp['serial_apb']);
					$apBonus->getData();
					$sale=new Sales($db,$apBonus->getSerial_sal());
					$sale->getData();
					$invoice=new Invoice($db,$sale->getSerial_inv());
					$invoice->getData();
					array_push($bonus_for_pdf,array('card_number'=>($sale->getCardNumber_sal()!='')?$sale->getCardNumber_sal():'N/A',
													'invoice_number'=>$invoice->getNumber_inv(),
													'bonus_value'=>$btp['pending_amount_apb']));
				}else{
					$apBonus->setSerial_apb($btp['serial_apb']);
					$apBonus->getData();
					$sale=new Sales($db,$apBonus->getSerial_sal());
					$sale->getData();
					$invoice=new Invoice($db,$sale->getSerial_inv());
					$invoice->getData();
					array_push($bonus_for_pdf,array('card_number'=>($sale->getCardNumber_sal()!='')?$sale->getCardNumber_sal():'N/A',
													'invoice_number'=>$invoice->getNumber_inv(),
													'bonus_value'=>number_format($liquidationTotalForPdf,2)));
					$liquidationTotalForPdf=0;
				
				}
			}
			//check if we have already paid all the bonus
			if($liquidationTotalForPdf==0){
				break;
			}
		}
		//generates PDF file
		include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
		$logUser=new User($db,$_SESSION['serial_usr']);
		$logUser->getData();
		$city=new City($db,$logUser->getSerial_cit());
		$city->getData();
		$cityName=utf8_decode(ucfirst($city->getName_cit()));
		$date = html_entity_decode("$cityName, ".$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);
		$pdf->ezStopPageNumbers();
		$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
		$pdf->ezSetDy(-30);
		//title
		$pdf->ezText($date);
		$pdf->ezSetDy(-20);
		//get counter or dealer data.
		if($payTo=='COUNTER'){
			$pdf->ezText(utf8_decode('Sr.(a):'));
			$counter=new Counter($db,$serialTo);
			$counter->getData();
			$counterUser=new User($db,$counter->getSerial_usr());
			$counterUser->getData();
			$dealer=new Dealer($db,$counter->getSerial_dea());
			$dealer->getData();
			$pdf->ezText("<b>{$counterUser->getFirstname_usr()} {$counterUser->getLastname_usr()}</b>");
			$pdf->ezText("<b>Comercializador {$dealer->getName_dea()}</b>");
		}else{
			$pdf->ezText(utf8_decode('Señores:'));
			$dealer=new Dealer($db,$serialTo);
			$dealer->getData();
			$pdf->ezText("<b>{$dealer->getName_dea()}</b>");
		}
		$pdf->ezText("Presente.-");
		$pdf->ezSetDy(-10);
		$pdf->ezText(utf8_decode("De mi consideración:"));
		$pdf->ezSetDy(-10);
		$pdf->ezText(utf8_decode("Por medio de la presente le expresamos nuestro agradecimiento por el apoyo y confianza brindada a $global_system_name.  A su vez, premiar su fidelidad, con la entrega de <b>US$ $liquidationTotal de incentivo en:</b>"));
		$pdf->ezSetDy(-20);
		$pdf->ezTable($prizes_for_pdf,
				  array('name_pri'=>'<b>Premio</b>',
					    'quantity'=>'<b>Cantidad/Valor</b>'),
				  '',
				  array('xPos'=>'center',
						'cols'=>array(
							'name_pri'=>array('justification'=>'center','width'=>80),
							'quantity'=>array('justification'=>'center','width'=>50))));
		$pdf->ezSetDy(-10);
		$pdf->ezText("Correspondiente a:");
		$pdf->ezSetDy(-10);
		$pdf->ezText("DETALLE DE TARJETAS:");
		$pdf->ezSetDy(-10);
		$pdf->ezTable($bonus_for_pdf,
				  array('card_number'=>'<b>Tarjeta</b>',
					    'invoice_number'=>'<b>Factura</b>',
					    'bonus_value'=>'<b>Valor Incentivo</b>'),
				  '',
				  array('xPos'=>'center',
						'cols'=>array(
							'card_number'=>array('justification'=>'center','width'=>80),
							'invoice_number'=>array('justification'=>'center','width'=>50),
							'bonus_value'=>array('justification'=>'center','width'=>50))));

		$pdf->ezSetDy(-10);
		$pdf->ezText(utf8_decode("Los resultados evidencian que el esfuerzo mancomunado ha tenido el éxito deseado, lo que conlleva nuestro compromiso para servirles cada vez mejor."));
		$pdf->ezSetDy(-20);
		$pdf->ezText("Atentamente,");
		$pdf->ezText(utf8_decode($global_system_name));
		$pdf->ezSetDy(-40);
		$pdf->ezText(utf8_decode($logUser->getFirstname_usr().' '.$logUser->getLastname_usr()));
		$pdf->ezText("Adjunto: Lo indicado");
		//create the liquidation register
		$bonusLiquidation=new BonusLiquidation($db);
		$bonusLiquidation->setSerial_usr($_SESSION['serial_usr']);
		$serialBonusLiquidation=$bonusLiquidation->insert();

		//register the bonus used
		if($serialBonusLiquidation){
			foreach($bonusToPay as $bp){
				$appliedBonusLiquidation=new AppliedBonusLiquidation($db);
				if($bp['pending_amount_apb']>0){
					if($bp['pending_amount_apb']<$liquidationTotal){
						$liquidationTotal-=$bp['pending_amount_apb'];
						$apBonus->setSerial_apb($bp['serial_apb']);
						$apBonus->getData();
						$apBonus->setPending_amount_apb('0');
						$apBonus->setStatus_apb('PAID');
						$apBonus->setPayment_date_apb(date('d/m/Y'));
						if(!$apBonus->update()){
							$error=2;
							break;
						}else{
							$appliedBonusLiquidation->setSerial_lqn($serialBonusLiquidation);
							$appliedBonusLiquidation->setSerial_apb($bp['serial_apb']);
							$appliedBonusLiquidation->setAmount_used_abl($bp['pending_amount_apb']);
							if(!$appliedBonusLiquidation->insert()){
								$error=2;
								break;
							}
						}
					}else{
						$apBonus->setSerial_apb($bp['serial_apb']);
						$apBonus->getData();
						$pending_amount=$bp['pending_amount_apb']-$liquidationTotal;
						$lastAmountUsed=$liquidationTotal;
						$liquidationTotal=0;
						$apBonus->setPending_amount_apb($pending_amount);
						if($pending_amount<=0){
							$apBonus->setPayment_date_apb(date('d/m/Y'));
							$apBonus->setStatus_apb('PAID');
						}
						if(!$apBonus->update()){
							$error=2;
							break;
						}else{
							$appliedBonusLiquidation->setSerial_lqn($serialBonusLiquidation);
							$appliedBonusLiquidation->setSerial_apb($bp['serial_apb']);
							$appliedBonusLiquidation->setAmount_used_abl($lastAmountUsed);
							if(!$appliedBonusLiquidation->insert()){
								$error=2;
								break;
							}
						}
					}
				}
				//check if we have already paid all the bonus
				if($liquidationTotal==0){
					break;
				}
			}
	
			//register the prize used
			$prizeToUpdate=new Prize($db);
			foreach($prizes_applied as $pa){
				$prizesLiquidation=new PrizesLiquidation($db);
				$prizeToUpdate->setSerial_pri($pa['serial_pri']);
				$prizeToUpdate->getData();
				if($prizeToUpdate->getCoverage_pri()=="AMOUNT"){
					$prizeToUpdate->setStock_pri($prizeToUpdate->getStock_pri()-$pa['quantity']);
					if(!$prizeToUpdate->update()){
						$error=3;
						break;
					}
				}
				$prizesLiquidation->setSerial_pri($prizeToUpdate->getSerial_pri());
				$prizesLiquidation->setSerial_lqn($serialBonusLiquidation);
				$prizesLiquidation->setQuantity_pli($pa['quantity']);
				$prizesLiquidation->setAmount_pli($prizeToUpdate->getAmount_pri());
				$prizesLiquidation->setCost_pli($prizeToUpdate->getCost_pri());
				if(!$prizesLiquidation->insert()){
					$error=4;
					break;
				}
			}
	}else{
		$error=4;
	}

		if($error==1){
			$pdf->ezStream();
		}else{
			http_redirect('main/bonusLiquidation/'.$error);
		}
	}
}else{
	$error=4;
	http_redirect('main/bonusLiquidation/'.$error);
}
?>
