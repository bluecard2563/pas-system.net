<?php
/*
File: fNewBonus.php
Author: Santiago Benitez
Creation Date: 16/03/2010 13:47
Last Modified: 16/03/2010 13:47
Modified By: Santiago Benitez
*/

Request::setInteger('0:error');
if($_SESSION['bonus']) {
    $nonInserted = $_SESSION['bonus'];
    unset($_SESSION['bonus']);
    $titles = array("Pa&iacute;s","Ciudad","Comercializador","Producto","Porcentaje");
}
$zone= new  Zone($db);
$zoneList=$zone->getZones();

$smarty->register('error,zoneList,nonInserted,titles');
$smarty->display();
?>