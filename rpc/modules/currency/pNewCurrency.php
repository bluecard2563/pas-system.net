<?php 
/*
File: pNewCurrency.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 12:25
Modified By: David Bergmann
*/

$currency=new Currency($db);
$currency->setName_cur($_POST['txtNameCurrency']);
$currency->setExchange_fee_cur($_POST['txtExchange_fee_cur']);
$currency->setSymbol_cur($_POST['txtSymbol_cur']);

if($currency->insert()){//Si la inserción es exitosa
	$error=1;
}else{//Si hubo algún error
	$error=2;
}
http_redirect('modules/currency/fNewCurrency/'.$error);
?>