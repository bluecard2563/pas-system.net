<?php
/*
File: fUpdateCurrency.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 12:47
Modified By: David Bergmann
*/

Request::setString('0:serial_cur');

$currency = new Currency($db);
$currency->setSerial_cur($serial_cur);

if($currency -> getData()){
    $data['serial_cur'] = $currency -> getSerial_cur();
    $data['name_cur'] = $currency -> getName_cur();
    $data['exchange_fee_cur'] = $currency -> getExchange_fee_cur();
    $data['symbol_cur'] = $currency -> getSymbol_cur();
    $data['status_cur'] = $currency -> getStatus_cur();
    //Debug::print_r($data);
}else{	
	http_redirect('modules/currency/fSearchCurrency/2');
}

$statusList=$currency->getAllStatus();

$smarty -> register('data,statusList,error');
$smarty -> display();
?>