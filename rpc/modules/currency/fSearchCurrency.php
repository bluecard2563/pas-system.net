<?php
/*
File: fSearchCurrency.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 12:26
Modified By: David Bergmann
*/
	Request::setInteger('0:error');
	$currency=new Currency($db);
	$currList=$currency->getCurrency();

	$smarty -> register('error,currList');
	$smarty -> display();
	
?>