<?php
/*
    Document   : pPrintComissionsPreview
    Created on : 12-may-2010, 12:07:05
    Author     : Nicolas Flores
    Description:
       PDF to print a preview of comision to pay.
 * Parameters:
 *	type: type of liquidation (RESPONSIBLE,MANAGER,DEALER)
 *  serial: serial of who the liquidation is for.
*/
//check if the file was included for comission paying
if(isset($serialToPay)){
	$serial=$serialToPay;
	$titleText='<b>LIQUIDACION DE COMISIONES</b>';
}else{
	Request::setInteger('1:serial');
	$titleText='<b>PRE LIQUIDACION DE COMISIONES</b>';
}
if(isset($typeToPay)){
	$type=$typeToPay;
}else{
	Request::setString('0:type');
}
//
if(!$type || !$serial)
	die('Insuficient arguments');
$error=1;
include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
$pdf->ezText($titleText,'', array('justification' =>'center'));
//get info for details header
$payTo='';
$today=date('d/m/Y');
$totalSold=0;
$totalRefunded=0;
$comissionToPay=0;
$payToId='';
$payToPhone='';
$payToAddress='';
$apComission= new AppliedComission($db);
//get info based on type for details
switch ($type){
		case 'DEALER':
				$dealer=new Dealer($db,$serial);
				$dealer->getData();
				$payTo=$dealer->getName_dea();
				$payToId=$dealer->getId_dea();
				$payToPhone=$dealer->getPhone1_dea();
				$payToAddress=$dealer->getAddress_dea();
				$salesInfo=$apComission->getDealersComissionableValues($serial, $dealer->getLast_commission_paid_dea());
				if($salesInfo){
					foreach($salesInfo as &$s){
						if($s['type_com']=='IN'){
							$totalSold+=$s['value_dea_com'];
							$s['comissionPercentage']=number_format(($s['value_dea_com']*100)/$s['total_sal'], 2, '.', '');
							$s['to_comission']=number_format($s['value_dea_com'], 2, '.', '');
							$s['toDiscount']=number_format(0, 2, '.', '');
						}else{
							$totalRefunded+=$s['value_dea_com'];
							$s['comissionPercentage']=number_format(($s['value_dea_com']*100)/$s['total_sal'], 2, '.', '');
							$s['to_comission']=number_format(0, 2, '.', '');
							$s['toDiscount']=number_format($s['value_dea_com'], 2, '.', '');
						}
						$s['name_cus']=utf8_decode($s['name_cus']);
						$s['name_pbl']=utf8_decode($s['name_pbl']);
						$s['name_dea']=utf8_decode($s['name_dea']);
						unset($s['serial_com']);
						unset($s['value_dea_com']);
						unset($s['type_com']);
					}
					$comissionToPay=$totalSold-$totalRefunded;
				}
			break;
		case 'MANAGER':
				$managerByCountry=new ManagerbyCountry($db,$serial);
				$managerByCountry->getData();
				$manager=new Manager($db,$managerByCountry->getSerial_man());
				$manager->getData();
				$payTo=$manager->getName_man();
				$payToId=$manager->getDocument_man();
				$payToPhone=$manager->getPhone_man();
				$payToAddress=$manager->getAddress_man();
				$salesInfo=$apComission->getManagersComissionableValues($serial, $managerByCountry->getLast_commission_paid_mbc());
				if($salesInfo){
					foreach($salesInfo as &$s){
							//fix to unset the duplicated data in the array on numeric Keys
							for($i=0;$i<12;$i++){
								unset($s[$i]);
							}
							//
						if($s['type_com']=='IN'){
							$totalSold+=$s['value_mbc_com'];
							$s['comissionPercentage']=number_format(($s['value_mbc_com']*100)/$s['total_sal'], 2, '.', '');
							$s['to_comission']=number_format($s['value_mbc_com'], 2, '.', '');
							$s['toDiscount']=number_format(0, 2, '.', '');
						}else{
							$totalRefunded+=$s['value_mbc_com'];
							$s['comissionPercentage']=number_format(($s['value_mbc_com']*100)/$s['total_sal'], 2, '.', '');
							$s['to_comission']=number_format(0, 2, '.', '');
							$s['toDiscount']=number_format($s['value_mbc_com'], 2, '.', '');
						}
						$s['name_cus']=utf8_decode($s['name_cus']);
						$s['name_pbl']=utf8_decode($s['name_pbl']);
						$s['name_dea']=utf8_decode($s['name_dea']);
						unset($s['serial_com']);
						unset($s['value_mbc_com']);
						unset($s['type_com']);
					}
					$comissionToPay=$totalSold-$totalRefunded;
				}
			break;
		case 'RESPONSIBLE':
				$user= new User($db,$serial);
				$user->getData();
				$payTo=$user->getFirstname_usr().' '.$user->getLastname_usr();
				$payToId=$user->getDocument_usr();
				$payToPhone=$user->getPhone_usr();
				$payToAddress=$user->getAddress_usr();
				$salesInfo=$apComission->getUsersComissionableSales($serial);
				if($salesInfo){
					foreach($salesInfo as &$s){
						if($s['type_com']=='IN'){
							$totalSold+=$s['value_ubd_com'];
							$s['comissionPercentage']=number_format(($s['value_ubd_com']*100)/$s['total_sal'], 2, '.', '');
							$s['to_comission']=number_format($s['value_ubd_com'], 2, '.', '');
							$s['toDiscount']=number_format(0, 2, '.', '');
						}else{
							$totalRefunded+=$s['value_ubd_com'];
							$s['comissionPercentage']=number_format(($s['value_ubd_com']*100)/$s['total_sal'], 2, '.', '');
							$s['to_comission']=number_format(0, 2, '.', '');
							$s['toDiscount']=number_format($s['value_ubd_com'], 2, '.', '');
						}
						$s['name_cus']=utf8_decode($s['name_cus']);
						$s['name_pbl']=utf8_decode($s['name_pbl']);
						$s['name_dea']=utf8_decode($s['name_dea']);
						unset($s['serial_ubd']);
						unset($s['value_ubd_com']);
						unset($s['type_com']);
					}
					$comissionToPay=$totalSold-$totalRefunded;
				}
			break;
}
//Debug::print_r($salesInfo);
$headerValues=array();
$headerLine1=array('col1'=>'<b>Pagar a:</b>',
				   'col2'=>$payTo,
				   'col3'=>'<b>RUC/CI:</b>',
				   'col4'=>$payToId);
array_push($headerValues,$headerLine1);
$headerLine2=array('col1'=>'<b>Fecha:</b>',
				   'col2'=>$today,
				   'col3'=>utf8_decode('<b>Teléfono:</b>'),
				   'col4'=>$payToPhone);
array_push($headerValues,$headerLine2);
$headerLine3=array('col1'=>utf8_decode('<b>Comisión a Favor:</b>'),
				   'col2'=>number_format($totalSold, 2, '.', ''),
				   'col3'=>utf8_decode('<b>Dirección:</b>'),
				   'col4'=>utf8_decode($payToAddress));
array_push($headerValues,$headerLine3);
$headerLine4=array('col1'=>utf8_decode('<b>Comisión a Descontar:</b>'),
				   'col2'=>number_format($totalRefunded, 2, '.', ''),
				   'col3'=>'',
				   'col4'=>'');
array_push($headerValues,$headerLine4);
$headerLine5=array('col1'=>utf8_decode('<b>Valor Comisión:</b>'),
				   'col2'=>number_format($comissionToPay, 2, '.', ''),
				   'col3'=>'',
				   'col4'=>'');
array_push($headerValues,$headerLine5);
//Debug::print_r($headerValues);
$pdf->ezSetDy(-10);
	$pdf->ezTable($headerValues,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>2,
						'xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>100),
							'col2'=>array('justification'=>'left','width'=>250),
							'col3'=>array('justification'=>'left','width'=>100),
							'col4'=>array('justification'=>'left','width'=>305))));

//end header
//start sales details
 $pdf->ezSetDy(-10);
	$pdf->ezTable($salesInfo,
				  array('card_number_sal'=>'<b># Tarjeta</b>',
						'number_inv'=>'<b># Factura</b>',
						'name_cus'=>'<b>Apellido y Nombre</b>',
						'name_pbl'=>'<b>Producto</b>',
						'begin_date_sal'=>'<b>Inicio</b>',
						'end_date_sal'=>'<b>Fin</b>',
						'days_sal'=>'<b>Tiempo</b>',
						'name_dea'=>'<b>Agencia</b>',
						'total_sal'=>'<b>Precio</b>',
						'comissionPercentage'=>utf8_decode('<b>% Comisión</b>'),
						'to_comission'=>utf8_decode('<b>Comisión</b>'),
						'toDiscount'=>'<b>Descuento</b>'),
						'',
				  array('xPos'=>'center',
						'innerLineThickness'=>0.8,
						'outerLineThickness' =>1.2,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'card_number_sal'=>array('justification'=>'center','width'=>48),
							'number_inv'=>array('justification'=>'center','width'=>48),
							'name_cus'=>array('justification'=>'center','width'=>100),
							'name_pbl'=>array('justification'=>'center','width'=>100),
							'begin_date_sal'=>array('justification'=>'center','width'=>55),
							'end_date_sal'=>array('justification'=>'center','width'=>55),
							'days_sal'=>array('justification'=>'center','width'=>50),
							'name_dea'=>array('justification'=>'center','width'=>100),
							'total_sal'=>array('justification'=>'center','width'=>50),
							'comissionPercentage'=>array('justification'=>'center','width'=>50),
							'to_comission'=>array('justification'=>'center','width'=>45),
							'toDiscount'=>array('justification'=>'center','width'=>52))));
//
//preparing totals array
	$totalValues=array();
	$totalLine1=array('col1'=>number_format($totalSold, 2, '.', ''),
					  'col2'=>number_format($totalRefunded, 2, '.', ''));
	array_push($totalValues,$totalLine1);
	$totalLine2=array('col1'=>'<b>TOTAL:</b>',
					  'col2'=>number_format($comissionToPay, 2, '.', ''));
	array_push($totalValues,$totalLine2);
	$pdf->ezTable($totalValues,
				  array('col1'=>'','col2'=>''),
				  '',
				  array('showHeadings'=>0,
						'innerLineThickness'=>0.8,
						'outerLineThickness' =>1.2,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>749,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'col1'=>array('justification'=>'center','width'=>45),
							'col2'=>array('justification'=>'center','width'=>52))));
//
//check if is paying 
if(isset($serialToPay)&& isset($typeToPay)){
	if($apComission->setPaidComission($typeToPay, $serialToPay, $_SESSION['serial_usr'])){
			//set the new last comission paid date
			switch($typeToPay){
			case 'MANAGER':
				$managerByCountry=new ManagerbyCountry($db,$serialToPay);
				$managerByCountry->getData();
				if($managerByCountry->updateLastComissionPaid()){
					$error=1;
				}else{
					$error=3;
				}
				break;
			case 'DEALER':
				$dealer=new Dealer($db,$serialToPay);
				$dealer->getData();
				if($dealer->updateLastComissionPaid()){
					$error=1;
				}else{
					$error=3;
				}
				break;
			case 'RESPONSIBLE':
				$responsible=new UserByDealer($db);
				$responsible->setSerial_usr($serialToPay);
				if($responsible->updateLastComissionPaid()){
					$error=1;
				}else{
					$error=3;
				}
				break;
		}
	}else{
			$error=2;
	}
}
if($error==1)
	$pdf->ezStream();
else
	http_redirect('main/comissions/'.$error);

?>
