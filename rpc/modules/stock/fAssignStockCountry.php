<?php
/*
File: fAssignStockCountry.php
Author: Nicolas Flores
Creation Date: 13/01/2010
Last Modified:
Modified By:
*/
Request::setInteger('0:error');
//get list of existing zones
$zone = new Zone($db);
$nameZoneList=$zone->getZones();
//get the list of stock types
$stock=new Stock($db);
$typeList=$stock->getTypeValues();
// check the number of cards per type available to assign
$availableStock=array();
foreach ($typeList as $tl){
    $availableStock[$tl]=$stock->getAvailableForCountry($tl);
}
if(isset($_SESSION['assigned_stock_country_ranges']))
        {
            $smarty->assign('assigned_ranges',$_SESSION['assigned_stock_country_ranges']);
            unset($_SESSION['assigned_stock_country_ranges']);
        }

$smarty -> register('nameZoneList,typeList,availableStock,error');
$smarty -> display();
?>