<?php
/*
File:
Author: Nicolas Flores
Creation Date:07/02/2010
Modified By: Nicolas Flores
Last Modified: 22/02/2010
*/
//flag to check if the process was correct
$flag=0;

// Get form Data
//$serial_cou = $_POST["selCountry"]; //Country iD
//$serial_dea = $_POST['selBranch']; // Dealer Serial
//$dea_serial_dea = $_POST['selBranch']; // Branch serial
$stock=new Stock($db);
$serial_cnt = $_POST["selSeller"]; //counter serial
$stockAmount = $_POST['txtAmountStock']; // stock amount
$type = $_POST['selType']; //type de Stock
$error=$stock->stockAssign($stockAmount,$type,'SELLER',$serial_cnt,true);
/*
//declare an Array to save the ranges inserted.
$insertedRanges=Array();


//Declaramos un stock
$stock = new Stock($db);
$available = $stock -> getAvailableForCounter($dea_serial_dea, $type);
//Verifica si el numero de Stocks ha asignar es menor al disponible
if($stockAmount<=$available){
//Fill stock data
	$stock -> setSerial_cou($serial_cou);
	$stock -> setType_sto($type);
	$stock -> setDea_serial_dea($dea_serial_dea);
	$stock -> setSerial_dea($serial_dea);
	$stock -> setSerial_usr($serial_usr);
        $stock->setUse_serial_usr($_SESSION['serial_usr']);

	 //get the list of ranges of total stock for the dealer by type
	$stockRanges = $stock -> getStockBranchType($dea_serial_dea, $type);
	for($i=0;$i<sizeof($stockRanges);$i++){
		if($stockAmount>0){
			//Saco el maximo stock asignado a los comercializadores del  Pais dentro de un rango
			$maxInRange = $stock ->getMaxStockSeller($dea_serial_dea, $type,$stockRanges[$i][1],$stockRanges[$i][2]);
			if($maxInRange<$stockRanges[$i][2]){
				//Saco si hay stock dentro del rango, sino le pongo el limite inferior
				if($maxInRange==0){
					$maxInRange=$stockRanges[$i][1]-1;
				}
				//Verifico si la cantidad que quiero meter, alcanza en el rango
				if(($stockRanges[$i][2]-$maxInRange)>=$stockAmount){
					$stock -> setFrom_sto($maxInRange+1);
					$stock -> setTo_sto($maxInRange+$stockAmount);
                                         array_push($insertedRanges, array('from'=>$maxInRange+1,'to'=>$maxInRange+$stockAmount));
					if($stock -> insert()){
						$stockAmount=0;
						$flag=1;
						}
				}else{
					$stock -> setFrom_sto($maxInRange+1);
					$stock -> setTo_sto($stockRanges[$i][2]);
                                        array_push($insertedRanges, array('from'=>$maxInRange+1,'to'=>$stockRanges[$i][2]));
					if($stock -> insert()){
						$stockAmount=$stockAmount-($stockRanges[$i][2]-$maxInRange);
						$flag=1;
						}
					}
			}
		}
	} //end for


	if($flag==1){
	    $error=1;
            //register the ranges created to be displayed as infromation for the user, the var should be unsset after used no fAssignStockSeller
            $_SESSION['assigned_stock_seller_ranges']=$insertedRanges;
            //TODO: mail function
            //then a mail should be sent, there is the required data to be send
            $mailType='Stock Assign Seller';
            $serial_usr=$stock->getSerial_usr();
            $misc=$insertedRanges;
            //with this data call to mail function.
	}else{
		 $error=2;
            //error while assigning
	}
}else{
	 $error=3;
	//insuficient stock
}
 */
 http_redirect('modules/stock/fAssignStockSeller/'.$error);
?>
