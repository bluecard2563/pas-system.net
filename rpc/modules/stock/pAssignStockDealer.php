<?php
/*
File: pAssignStockDealer.php
Author: Nicolas Flores
Creation Date: 14/01/2010
Modified By: Nicolas Flores
Last Modified: 19/02/2010
*/
//flag to check if the process was correct
$flag=0;
$stock= new Stock($db);
// Get form Data
//$serial_cou = $_POST["selCountry"]; //Country id
$serial_dea = $_POST['selDealer']; //dealer id
$stockAmount = $_POST['txtAmountStock']; // stock amount to be asigned
$type = $_POST['selType']; //Stock Type
$error=$stock->stockAssign($stockAmount,$type,'DEALER',$serial_dea,true);
/*

//declare an Array to save the ranges inserted.
$insertedRanges=Array();

//Declare an stock to validate availability
$stock = new Stock($db);
$available = $stock -> getAvailableForDealer($serial_cou, $type);
//verify if desired amount is less than the available
if($stockAmount<=$available){
	//Fill stock data
	$stock -> setSerial_cou($serial_cou);
	$stock -> setType_sto($type);
	$stock -> setSerial_dea($serial_dea);
        $stock->setUse_serial_usr($_SESSION['serial_usr']);
        //get the list of ranges of total stock for the country by type
	$stockRanges = $stock -> getStockCountryType($serial_cou, $type);
	for($i=0;$i<sizeof($stockRanges);$i++){
		if($stockAmount>0){
                        // get the max stock assigned to a dealer in the country in a  range
			$maxInRange = $stock ->getMaxStockDealer($serial_cou, $type,$stockRanges[$i][1],$stockRanges[$i][2]);
			if($maxInRange<$stockRanges[$i][2]){
                            //get if there is stock in the range, else use the floor
				if($maxInRange==0){
					$maxInRange=$stockRanges[$i][1]-1;
				}
                                //check if there is enought stock in the range for the desired amount
				if(($stockRanges[$i][2]-$maxInRange)>=$stockAmount){
					$stock -> setFrom_sto($maxInRange+1);
					$stock -> setTo_sto($maxInRange+$stockAmount);
                                        
                                        array_push($insertedRanges, array('from'=>$maxInRange+1,'to'=>$maxInRange+$stockAmount));
					if($stock -> insert()){
						$stockAmount=0;
						$flag=1;

						}
				}else{
					$stock -> setFrom_sto($maxInRange+1);
					$stock -> setTo_sto($stockRanges[$i][2]);
                                        
                                        array_push($insertedRanges, array('from'=>$maxInRange+1,'to'=>$stockRanges[$i][2]));
					if($stock -> insert()){
						$stockAmount=$stockAmount-($stockRanges[$i][2]-$maxInRange);
						$flag=1;
						}
					}
			}
		}
	} //end for
	if($flag==1){
            //no errors.
            $error=1;
            //register the ranges created to be displayed as infromation for the user, the var should be unsset after used no fAssignStockDealer
            $_SESSION['assigned_stock_dealer_ranges']=$insertedRanges;

            //TODO: mail function
            //then a mail should be sent, there is the required data to be send
            $mailType='Stock Assign Dealer';
            $serial_dea=$stock->getSerial_dea();
            $misc=$insertedRanges;
            //with this data call to mail function.

	}else{
		$error=2;
            //error while assigning
	}
}else{
	  $error=3;
	//insuficient stock
}
 */
http_redirect('modules/stock/fAssignStockDealer/'.$error);

?>
