<?php

/*
File: pAssignStockCountry.php
Author: Nicolas Flores
Creation Date: 14/01/2010
Modified By: Nicolas Flores
Last Modified: 19/02/2010
*/

//flag to check if the process was correct
$flag=0;
$stock= new Stock($db);
//get info from the form
$type=$_POST['selType'];
$country=$_POST['selCountry'];
$amount=$_POST['txtAmountStock'];
$error=$stock->stockAssign($amount,$type,'COUNTRY',$country,true);
/*
//declare an Array to save the ranges inserted.
$insertedRanges=Array();

$stock = new Stock($db);
$available = $stock -> getAvailableForCountry($type);
//validate if asigning amount is bigger than available amount of stock
if($amount<=$available){

	//Fill new stock info
	$stock -> setSerial_cou($country);
	$stock -> setType_sto($type);
        $stock->setUse_serial_usr($_SESSION['serial_usr']);
        // get a list of the ranges of the total stock for the type selected
	$stockRanges = $stock -> getStockType($type);

	for($i=0;$i<sizeof($stockRanges);$i++){
		if($amount>0){
			//get the maximun stock assigned to a country in the range
			$maxInRange = $stock ->getMaxStockCountry($type,$stockRanges[$i][1],$stockRanges[$i][2]);
			if($maxInRange<$stockRanges[$i][2]){

                                //Check if there is stock in the range, else use the floor
				if($maxInRange==0){
					$maxInRange=$stockRanges[$i][1]-1;
				}
				//check if there is enought stock in the range for the amount desired
				if(($stockRanges[$i][2]-$maxInRange)>=$amount){
					$stock -> setFrom_sto($maxInRange+1);
					$stock -> setTo_sto($maxInRange+$amount);
                                        array_push($insertedRanges, array('from'=>$maxInRange+1,'to'=>$maxInRange+$amount));
					if($stock -> insert()){
						$amount=0;
						$flag=1;
					}
				}else{
					$stock -> setFrom_sto($maxInRange+1);
					$stock -> setTo_sto($stockRanges[$i][2]);
                                        array_push($insertedRanges, array('from'=>$maxInRange+1,'to'=>$stockRanges[$i][2]));
					if($stock -> insert()){
						$amount=$amount-($stockRanges[$i][2]-$maxInRange);
						$flag=1;
					}
				}
			}
		}
	}
	if($flag==1){
            //no errors.
            $error=1;
            //register the ranges created to be displayed as infromation for the user, the var should be unsset after used no fAssignStockCountry
            $_SESSION['assigned_stock_country_ranges']=$insertedRanges;
              
	}else{
            $error=2;
            //error while assigning
	}
}else{
        $error=3;
	//insuficient stock
}
 */
http_redirect('modules/stock/fAssignStockCountry/'.$error);
?>