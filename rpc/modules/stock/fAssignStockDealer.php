<?php
/*
File: fAssignStockDealer.php
Author: Nicolas Flores
Creation Date: 14/01/2010
Modified by:Nicolas Flores
Last Modified:25/01/2010
*/

Request::setInteger('0:error');
//get the list of countries for the user logged in
$country=new Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);
//get the list of stock types
$stock=new Stock($db);
$typeList=$stock->getTypeValues();

if(isset($_SESSION['assigned_stock_dealer_ranges']))
        {
            $smarty->assign('assigned_ranges',$_SESSION['assigned_stock_dealer_ranges']);
            unset($_SESSION['assigned_stock_dealer_ranges']);
        }

$smarty -> register('countryList,typeList,error');
$smarty -> display();
?>
