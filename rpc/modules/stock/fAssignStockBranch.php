<?php
/*
File:fAssignStockBranch
Author: Nicolas Flores
Creation Date: 02/02/2010
Modified By:
Last Modified:
*/
Request::setInteger('0:error');
//get the list of countries for the user logged in
$country=new Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);
//get the list of stock types
$stock=new Stock($db);
$typeList=$stock->getTypeValues();

if(isset($_SESSION['assigned_stock_branch_ranges']))
        {
            $smarty->assign('assigned_ranges',$_SESSION['assigned_stock_branch_ranges']);
            unset($_SESSION['assigned_stock_branch_ranges']);
        }

$smarty -> register('countryList,typeList,error');
$smarty -> display();
?>
