<?php
/*
File: pDealerPendingPaymentsReportPDF
Author: David Bergmann
Creation Date: 22/07/2010
Last Modified:
Modified By:
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

//HEADER
$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);

$pdf->ezText('<b>'.utf8_decode("REPORTE DE CUENTAS POR PAGAR").'</b>', 12, array('justification'=>'center'));
$pdf->ezText(utf8_decode($date), 10, array('justification'=>'center'));
$pdf->ezSetDy(-10);


if($_SESSION['serial_dea']) {
	$branch = new Dealer($db,$_SESSION['serial_dea']);
	$branch->getData();
	$creditDay = new CreditDay($db, $branch->getSerial_cdd());
	$creditDay->getData();
	$creditDays = $creditDay->getDays_cdd();

	$invoicesList = $branch->getInvoicesByBranch();
	$report = array();

	$total_days = array();
	$total_days['total_30'] = '0.00';
	$total_days['total_60'] = '0.00';
	$total_days['total_90'] = '0.00';
	$total_days['total_more'] = '0.00';
	$total_days['total'] = 0;

	if(is_array($invoicesList)){

		foreach($invoicesList as &$i){
			if($i['days'] < 0 ){
				$i['days']=0;
			}
			if($i['days'] <= 30){
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_30'] += $i['total_to_pay'];
				}else{
					$total_days['total_30'] += $i['total_to_pay_fee'];
				}
			}elseif($i['days'] > 30 && $i['days'] <= 60){
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_60'] += $i['total_to_pay'];
				}else{
					$total_days['total_60'] += $i['total_to_pay_fee'];
				}
			}elseif($i['days'] > 60 && $i['days'] <= 90){
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_90'] += $i['total_to_pay'];
				}else{
					$total_days['total_90'] += $i['total_to_pay_fee'];
				}
			}else{
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_more'] += $i['total_to_pay'];
				}else{
					$total_days['total_more'] += $i['total_to_pay_fee'];
				}
			}
			if($i['status_inv']=='STAND-BY'){
				$total_days['total'] += $i['total_to_pay'];
			}else{
				$total_days['total'] += $i['total_to_pay_fee'];
			}
			if($i['status_pay'] == 'PARTIAL' || $i['status_pay'] == 'ONLINE_PENDING'){
				$i['number_inv'] = $i['number_inv'].' (A)';
			}elseif($i['status_pay'] == 'VOID'){
				$i['number_inv'] = $i['number_inv'].' (N)';
			}
			if($i['status_inv']=='VOID'){
				$i['number_inv'] = $i['number_inv'].' (CI)';
			}

			/*Data for PDF report*/
			$titles = array("col1"=>utf8_decode('<b>Fecha de Factura</b>'),
							"col2"=>utf8_decode('<b># de Factura</b>'),
							"col3"=>utf8_decode('<b>Facturado a</b>'),
							"col4"=>utf8_decode('<b>Días de Crédito</b>'),
							"col5"=>utf8_decode('<b>Impago</b>'),
							"col6"=>utf8_decode('<b>Hasta 30</b>'),
							"col7"=>utf8_decode('<b>Hasta 60</b>'),
							"col8"=>utf8_decode('<b>Hasta 90</b>'),
							"col9"=>utf8_decode('<b>Más de 90</b>'));

			if($i['status_inv'] == "STAND-BY") {
				$toPay = $i['total_to_pay'];
			} else {
				$toPay = $i['total_to_pay_fee'];
			}

			if($i['days']<31) {
				$col6 = $toPay;
				$col7 = "";
				$col8 = "";
				$col9 = "";
			} elseif($i['days']>30 && $i['days']<61) {
				$col6 = "";
				$col7 = $toPay;
				$col8 = "";
				$col9 = "";
			} elseif($i['days']>60 && $i['days']<91) {
				$col6 = "";
				$col7 = "";
				$col8 = $toPay;
				$col9 = "";
			} elseif($i['days']>90) {
				$col6 = "";
				$col7 = "";
				$col8 = "";
				$col9 = $toPay;
			}

			$aux = array("col1"=>$i['date_inv'],
						 "col2"=>$i['number_inv'],
						 "col3"=>$i['invoice_to'],
						 "col4"=>$creditDays,
						 "col5"=>$i['days'],
						 "col6"=>$col6,
						 "col7"=>$col7,
						 "col8"=>$col8,
						 "col9"=>$col9);
			array_push($report, $aux);
			/*End data for PDF report*/

		}
		$total_days['total_30'] = number_format($total_days['total_30'], 2 , '.', '');
		$total_days['total_60'] = number_format($total_days['total_60'], 2 , '.', '');
		$total_days['total_90'] = number_format($total_days['total_90'], 2 , '.', '');
		$total_days['total_more'] = number_format($total_days['total_more'], 2 , '.', '');
		$total_days['total'] = number_format($total_days['total'], 2 , '.', '');

		/*Data for PDF report*/
		$aux = array("col1"=>"",
					 "col2"=>"",
					 "col3"=>"",
					 "col4"=>"",
					 "col5"=>"Total:",
					 "col6"=>$total_days['total_30'],
					 "col7"=>$total_days['total_60'],
					 "col8"=>$total_days['total_90'],
					 "col9"=>$total_days['total_more']);
		array_push($report, $aux);
		/*End data for PDF report*/

		$pdf->ezTable($report,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>60),
                                        'col2'=>array('justification'=>'center','width'=>60),
										'col3'=>array('justification'=>'center','width'=>60),
                                        'col4'=>array('justification'=>'center','width'=>60),
                                        'col5'=>array('justification'=>'center','width'=>60),
                                        'col6'=>array('justification'=>'center','width'=>60),
										'col7'=>array('justification'=>'center','width'=>60),
										'col8'=>array('justification'=>'center','width'=>60),
										'col9'=>array('justification'=>'center','width'=>60))));
		//Debug::print_r($invoicesList);
		//die(Debug::print_r($report));

	} else {
		 $pdf->ezText(utf8_decode('La sucursal de comercializador no tiene cuentas por pagar pendientes.'), 10, array('justification' =>'center'));
	}
	$pdf->ezStream();
}

?>