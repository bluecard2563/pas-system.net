<?php
/*
File: fDealerSalesReport
Author: David Bergmann
Creation Date: 22/07/2010
Last Modified:
Modified By:
*/

$serial_dea = $_SESSION['serial_dea'];
if($serial_dea) {
	//List of all sales status
	$statusList = Sales::getSalesStatus($db);

	//List of all products from a dealer
	$productByDealer = new ProductByDealer($db);
	$productList = $productByDealer->getproductByDealer($_SESSION['dea_serial_dea'], $_SESSION['serial_lang']);
}
/*Debug::print_r($statusList);
die(Debug::print_r($productList));*/

$smarty->register('serial_dea,statusList,productList,global_salesStatus');
$smarty->display();
?>