<?php
/*
 * File: fDealerBonusReport.php
 * Author: Patricio Astudillo
 * Creation Date: 21-jul-2010, 10:32:58
 * Modified By: Patricio Astudillo
 */

if($_SESSION['serial_dea']){
	$statusList=AppliedBonus::getAppliedBonusStatus($db);
	$today=date('d/m/Y');
	$smarty->register('statusList,today');
}

$smarty->display();
?>
