<?php
/*
 * File: pDealerBonusReportPDF.php
 * Author: Patricio Astudillo
 * Creation Date: 21-jul-2010, 11:09:29
 * Modified By: Patricio Astudillo
 */

$loged_user=$_SESSION['serial_usr'];
$dealer=new Dealer($db, $_SESSION['serial_dea']);
$dealer->getData();
$misc=array();
$misc['type']=$dealer->getBonus_to_dea();

if($dealer->getBonus_to_dea()=='COUNTER'){ //BONUS TO COUNTER
	$counter=new Counter($db);
	$counter=$counter->getCounters($loged_user);

	$user=new User($db, $loged_user);
	$user->getData();
	$receiver_name=$user->getFirstname_usr().' '.$user->getLastname_usr();

	if(is_array($counter)){
		$misc['identifier']=array();
		foreach($counter as $c){
			array_push($misc['identifier'], $c['serial_cnt']);
		}

		$misc['identifier']=implode(',', $misc['identifier']);
	}
}else{ //BONUS TO DEALER
	$misc['identifier']=$dealer->getSerial_dea();
	$receiver_name=$dealer->getName_dea();
}

if($_POST['selStatus']){
	$status=$global_appliedBonusStatus[$_POST['selStatus']];
}else{
	$status='Todos';
}

if($_POST['txtCardNumber']){
	$card_number=$_POST['txtCardNumber'];
}else{
	$card_number='N/A';
}

$bonus_raw_list=AppliedBonus::getBonusForDealerReport($db, $misc, $_POST['selStatus'], $_POST['txtCardNumber'], $_POST['txtBeginDate'], $_POST['txtEndDate']);
//Debug::print_r($bonus_raw_list);

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

//Headers
$pdf->ezText('<b>REPORTE DE INCENTIVOS</b>', 12, array('justification'=>'center'));
$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);
$pdf->ezSetDy(-2);
$pdf->ezText(utf8_decode($date), 9, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$pdf->ezText('<b>FILTROS UTILIZADOS.-</b>', 11, array('justification'=>'left'));
$pdf->ezSetDy(-10);
$pdf->ezText('<b>Incentivos a: </b>'.$receiver_name, 10, array('justification'=>'left'));
$pdf->ezText('<b>Estado: </b>'.$status, 10, array('justification'=>'left'));
$pdf->ezText('<b>No. Tarjeta: </b>'.$card_number, 10, array('justification'=>'left'));

if(is_array($bonus_raw_list)){
	$final_array_data=array();
	$item=-1;
	$total_in_favor=0;
	$total_paid=0;
	$total_refunded=0;

	foreach($bonus_raw_list as &$b){ //FORMAT THE RAW ARRAY AND CALCULATE THE TOTALS.
		if($item!=$b['serial_dea']){
			$item=$b['serial_dea'];
			$final_array_data[$item]=array();
		}
		
		switch($b['status_apb']){
			case 'ACTIVE':
				$total_in_favor+=$b['total_amount_apb'];
				break;
			case 'PAID':
				$total_paid+=$b['total_amount_apb'];
				break;
			case 'REFUNDED':
				$total_refunded+=$b['total_amount_apb'];
				break;
		}

		$b['status_apb']=$global_appliedBonusStatus[$b['status_apb']];		
		array_push($final_array_data[$item], $b);
	}
	//Debug::print_r($final_array_data);

	foreach($final_array_data as $fd){ //WRITE THE TABLES ON THE PDF
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>Comercializador: </b>'.$fd['0']['name_dea'], 10, array('justification'=>'center'));
		$pdf->ezSetDy(-10);

		foreach($fd as &$array_item){
			unset($array_item['serial_dea']);
			unset($array_item['name_dea']);
		}
			
		$pdf->ezTable($fd,
					  array('card_number_sal'=>'<b>No. Tarjeta</b>',
							'name_pbl'=>'<b>Producto</b>',
							'customer_name'=>'<b>Cliente</b>',
							'total_amount_apb'=>'<b>Incentivo</b>',
							'status_apb'=>'<b>Estado</b>',
							'number_inv'=>'<b># Factura</b>'),
					  '',
					  array('showHeadings'=>1,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>'center',
							'fontSize' => 9,
							'titleFontSize' => 9,
							'cols'=>array(
								'card_number_sal'=>array('justification'=>'center','width'=>55),
								'name_pbl'=>array('justification'=>'center','width'=>80),
								'customer_name'=>array('justification'=>'center','width'=>120),
								'total_amount_apb'=>array('justification'=>'center','width'=>55),
								'status_apb'=>array('justification'=>'center','width'=>80),
								'number_inv'=>array('justification'=>'center','width'=>55)
								)));
	}
	
	$total_in_favor=number_format($total_in_favor, 2, '.', '');
	$total_paid=number_format($total_paid, 2, '.', '');
	$total_refunded=number_format($total_refunded, 2, '.', '');

	$pdf->ezSetDy(-10);
	$pdf->ezText('<b>Total a Favor: </b>$'.$total_in_favor, 11, array('justification'=>'left'));
	$pdf->ezText('<b>Total Pagado: </b>$'.$total_paid, 11, array('justification'=>'left'));
	$pdf->ezText('<b>Total Reembolsado: </b>$'.$total_refunded, 11, array('justification'=>'left'));
	$pdf->ezSetDy(-10);
}else{
	$pdf->ezSetDy(-10);
	$pdf->ezText(html_entity_decode('No se tienen incentivos registrados de acuerdo a los par&aacute;metros ingresados.'), 10, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
}

$pdf->ezStream();
?>
