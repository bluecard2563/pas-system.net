<?php
/*
 * File: fDealerComissionsReport.php
 * Author: Patricio Astudillo
 * Creation Date: 22-jul-2010, 10:06:39
 * Modified By: Patricio Astudillo
 */

if($_SESSION['serial_dea']){
	$statusList=AppliedComission::getAppliedComissionsStatus($db);
	$today=date('d/m/Y');
	$smarty->register('statusList,today');
}

$smarty->display();
?>
