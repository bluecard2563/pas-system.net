<?php 
/*
File: pNewTax.php
Author: David Bergmann
Creation Date:11/01/2010
Last Modified:
Modified By:
*/

$tax=new Tax($db);
$tax->setName_tax($_POST['txtNameTax']);
$tax-> setPercentage_tax($_POST['txtPercentageTax']);
$tax-> setSerial_cou($_POST['selCountry']);
$tax-> setStatus_tax('ACTIVE');

if($tax->insert()){
	$error=1;
}else{
	$error=2;
}

http_redirect('modules/tax/fNewTax/'.$error);
?>