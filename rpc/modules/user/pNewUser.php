<?php
/*
File: pNewUser.php
Author: Esteban Angulo
Creation Date: 24/12/2009 12:00
Last Modified:28/02/2010
Modified By: Patricio Astudillo
*/

$user=new User($db);
$ubc = new UserbyCity($db);
$parameter= new Parameter($db,3);

$parameter -> getData();
$textForEmail=$parameter -> getDescription_par();

/*Generates Username and Password*/
$misc['db']=$db;
$misc['class']='user';
$misc['fields']='username_usr';
$misc['firstname']=$_POST['txtFirstName'];
$misc['lastname']=$_POST['txtLastName'];

$username=GlobalFunctions::generateUser($misc);
//$username=$_POST['txtMail'];
$pass=GlobalFunctions::generatePassword(6, 8, false);
/*End Generates Username and Password*/

$user->setFirstname_usr($_POST['txtFirstName']);
$user->setLastname_usr($_POST['txtLastName']);
$user->setDocument_usr($_POST['txtDocument']);
$user->setAddress_usr($_POST['txtAddress']);
$user->setPhone_usr($_POST['txtPhone']);
$user->setCellphone_usr($_POST['txtCellphone']);
$user->setBirthdate_usr($_POST['txtBirthdate']);
$user->setEmail_usr($_POST['txtMail']);
$user->setPosition_usr($_POST['txtPosition']);
$user->setUsername_usr($username);
$user->setPassword_usr($pass);
$user->setSerial_pbc($_POST['selProfile']);
$user ->setSerial_dea($_POST['selBranch']);
$user->setSerial_cit($_POST['selCity']);
$user->setSellFree_usr($_POST['rdSellFree']);
$user->setIn_assistance_group_usr('NO');

if($_POST['belongsto_usr']!='PLANETASSIST'){
    $user->setBelongsto_usr($_POST['belongsto_usr']);
    $user->setSerial_mbc($_POST['selManager']);
}else{
    $user->setBelongsto_usr('MANAGER');
    $mbc=new ManagerbyCountry($db);
    $serial_mbc=$mbc->getMainManager();
    
    if($serial_mbc){
        $user->setSerial_mbc($serial_mbc);
    }else{//Not a PLANETASSIST valid ID
        http_redirect('main/user/2');
    }
}

if($_POST['txtVisitsNumber']!=''){
    $user->setVisitsNumber_usr($_POST['txtVisitsNumber']);
}else{
    $user->setVisitsNumber_usr(0);
}

$userID=$user->insert(); //Inserts a new User and returns serial_usr

if($userID){
    $error=1;
	if($_POST['cities']){
		foreach($_POST['cities'] as $serial_cit => $serial_cou){
				$ubc -> setSerial_usr($userID);
				$ubc -> setSerial_cit($serial_cit);
				if(!$ubc -> insert()){

					$error=2;
					break;
				}
		}
	}
	$user=$_POST['txtFirstName'].' '.$_POST['txtLastName'];
	$misc['user']=$user;
	$misc['textForEmail']=$textForEmail;
	$misc['username']=$username;
	$misc['pswd']=$pass;
	$misc['email']=$_POST['txtMail'];

	if(GlobalFunctions::sendMail($misc, 'new_user')){
		$error=1;
	}else{
		$error=2;
	}
}

if($error==1){//Insert was correct and the user neither a manager nor a counter
     http_redirect('main/user/1');
}
else{//There was an error
     http_redirect('main/user/2');
}
?>
