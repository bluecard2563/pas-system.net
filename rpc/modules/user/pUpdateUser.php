<?php
/*
File: pUpdateUser.php
Author: Esteban Angulo
Creation Date: 24/12/2009 12:00
Last Modified:06/01/2010 
*/
//Debug::print_r($_POST);
//die();
$user = new User($db);
$country = new Country($db);
$dea = new Dealer($db);
$city = new City($db);
$ubc = new UserbyCity($db);
$counter= new Counter($db);

$parameter = new Parameter($db,4);
$parameter -> getData();
$textForEmail = $parameter -> getDescription_par();

if($_POST['serial_usr']){//comes from users update
    $data['serial_usr'] = $_POST['serial_usr'];
    $user -> setSerial_usr($data['serial_usr']);
    $user -> getData();
	$username = $user -> getUsername_usr();

	if($_POST['hddNewPassword'] == 'NEW'){
		$pass = GlobalFunctions::generatePassword(6, 8, false);
	}

    $data['first_name_usr'] = $_POST['txtFirstName'];
    $data['last_name_usr'] = $_POST['txtLastName'];
    $data['document_usr'] = $_POST['txtDocument'];
    $data['address_usr'] = $_POST['txtAddress'];
    $data['phone_usr'] = $_POST['txtPhone'];
    $data['cellphone_usr'] = $_POST['txtCellphone'];
    $data['bithdate_usr'] = $_POST['txtBirthdate'];
    $data['email_usr'] = $_POST['txtMail'];
    $data['serial_pbc'] = $_POST['selProfile'];
    $data['position_usr'] = $_POST['txtPosition'];
    //$data['username_usr'] = $_POST['txtUsername'];
    $data['password_usr'] = $pass;
    $data['status_usr'] = $_POST['selStatus'];
	if($_POST['selBranch']){
		$data['serial_dea'] = $_POST['selBranch'];
	}else{
		$data['serial_dea'] = $user->getSerial_dea();
	}
    if($_POST['hddPAUser']=='PLANETASSIST'){
        $data['belongsto_usr'] = $_POST['hddPAUser'];
    }else{
        $data['belongsto_usr'] = $_POST['hddBelongsto'];
    }
    $data['visits_number_usr'] = $_POST['txtVisitsNumber'];
	if($_POST['selManager']){
		$data['serial_mbc'] = $_POST['selManager'];
	}else{
		$data['serial_mbc'] = $user->getSerial_mbc();
	}
	$data['sell_free_usr'] = $_POST['rdSellFree'];
    $user -> setFirstname_usr($data['first_name_usr']);
    $user -> setLastname_usr($data['last_name_usr']);
    $user -> setDocument_usr($data['document_usr']);
    $user -> setAddress_usr($data['address_usr']);
    $user -> setPhone_usr($data['phone_usr']);
    $user -> setCellphone_usr($data['cellphone_usr']);
    $user -> setBirthdate_usr($data['bithdate_usr']);
    $user -> setEmail_usr($data['email_usr']);
    $user -> setPosition_usr($data['position_usr']);
    //$user -> setUsername_usr($data['username_usr']);
    $user -> setSerial_pbc($data['serial_pbc']);
    $user -> setSerial_dea($data['serial_dea']);
    $user -> setVisitsNumber_usr($data['visits_number_usr']);
    $user -> setStatus_usr($data['status_usr']);
    $user -> setSerial_mbc($data['serial_mbc']);
	$user -> setSellFree_usr($data['sell_free_usr']);

    if($data['belongsto_usr'] != 'PLANETASSIST'){
        $user -> setBelongsto_usr($_POST['hddBelongsto']);
		if($_POST['selManager']){
			$user -> setSerial_mbc($_POST['selManager']);
		}
    }else{
        $user -> setBelongsto_usr('MANAGER');
        $mbc = new ManagerbyCountry($db);
        $serial_mbc = $mbc -> getMainManager();

        if($serial_mbc){
            $user -> setSerial_mbc($serial_mbc);
        }else{//Not a PLANETASSIST valid ID
            http_redirect('main/user/4');
        }
    }

    if($data['password_usr'] != ''){
        $user -> setPassword_usr(md5($pass));
    }

    if($user -> update()){//updates user info
		$error = 3;
        if($_POST['cities']){
            $ubc -> setSerial_usr($data['serial_usr']);
            $ubc -> delete();
            foreach($_POST['cities'] as $serial_cit => $serial_cou){
                    $ubc -> setSerial_usr($data['serial_usr']);
                    $ubc -> setSerial_cit($serial_cit);
                    if(!$ubc -> insert()){
                        $error = 4;
                        break;
                    }
            }
        }
        if($_POST['hddNewPassword']=='NEW'){
			$user = $_POST['txtFirstName'] . ' ' . $_POST['txtLastName'];
			$misc['user'] = $username;
			$misc['textForEmail'] = $textForEmail;
			$misc['username'] = $username;
			$misc['pswd'] = $pass;
			$misc['email'] = $_POST['txtMail'];
			if(GlobalFunctions::sendMail($misc, 'resetPassword')){
				$error = 5;
			}else{
				$error = 6;
			}
		}
        
        http_redirect('main/user/'.$error);//User Updated
    }else{ //if there is an error, the page displays the original information of the user
		http_redirect('modules/user/fSearchUser/2');
    }
}

http_redirect('main/user/4');
?>