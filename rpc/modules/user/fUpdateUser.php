<?php

/*
File: fUpdateUser.php
Author: Esteban Angulo
Creation Date: 24/12/2009 12:00
Last Modified:08/01/2010 
*/
//Request::setInteger('0:message');
Request::setString('serial_usr');
if($serial_usr==''){
	http_redirect('modules/user/fSearchUser/1');
}
$user= new User($db, $_SESSION['serial_usr']);
$profile_by_country=new ProfileByCountry($db);
$mbc=new ManagerbyCountry($db);
$dea=new Dealer($db);
$country=new Country($db);
$city= new City($db);
$counter= new Counter($db);


/*LOADING DEFAULT DATA*/
//Countries
$countryList=$country->getOwnCountries($_SESSION['countryList']);

/*Charges the profiles which status is active*/
$profileList=$profile_by_country->getActiveProfile_by_countryList($_SESSION['countryList']);
foreach($profileList as &$pl){
	$pl['name_prf'] = $pl['name_prf'];
}

/*Charges all kind of Status from Data Base*/
$statusList=$user->getAllStatus();


/*Recoves all available PaymentDeadline of managers form the system*/
$paymentDeadlineList=$mbc->getAllPaymentDeadline();
foreach($paymentDeadlineList as &$tl){
	$tl['payment_deadline_mbc'] = $tl['payment_deadline_mbc'];
}

/*Recoves all available type of users form the system*/
$userTypeList=$user->getAllTypes();
/*if($user->isPlanetAssistUser()){
    $planetAssistUser=1;
    $serial_cou=$country->countryExists('ecuador');
}*/


/*Charges the Country Access List  from Data Base*/
$countryAccessList=$city->getCityAccessList($_SESSION['countryList']);
$user->setSerial_usr($serial_usr);

if($user->getData()){
	$data['serial_usr'] = $serial_usr;
	$data['first_name_usr'] = $user -> getFirstname_usr();
	$data['last_name_usr'] = $user -> getLastname_usr();
	$data['document_usr'] = $user -> getDocument_usr();
	$data['address_usr'] = $user -> getAddress_usr();
	$data['phone_usr'] = $user -> getPhone_usr();
	$data['cellphone_usr'] = $user -> getCellphone_usr();
	$data['birthdate_usr'] = $user -> getBirthdate_usr();
	$data['email_usr'] = $user -> getEmail_usr();
	$data['position_usr'] = $user -> getPosition_usr();
	$data['serial_pbc'] = $user -> getSerial_pbc();
	$data['username_usr'] = $user -> getUsername_usr();
	$data['serial_cit'] = $user -> getSerial_cit();
	$data['status_usr'] = $user -> getStatus_usr();
	$data['serial_dea'] = $user -> getSerial_dea();
	$data['aux']=$user->getAuxData_usr();
	$data['countries']=$city->getCityAccessListbyUser($serial_usr);
	$data['belongsto_usr']=$user->getBelongsto_usr();
	$data['visits_number_usr']=$user->getVisitsNumber_usr();
	$data['serial_mbc'] = $user -> getSerial_mbc();
	$data['sell_free_usr'] = $user -> getSellFree_usr();

	if(is_array($data['countries'])){
		$data['hasCountries']=1;
	}else{
		$data['hasCountries']=0;
	}

	if($user->isPlanetAssistUser()){
		$planetAssistUser=1;
		$serial_cou=$country->countryExists('ecuador');
	}

	/* Gets information of a city in DB*/
	$city = new City($db);
	$cityList=$city->getCitiesByCountry($data['aux']['serial_cou']);

	/* Gets information of a country in DB*/
	$country=new Country($db);
	$countryList=$country->getCountriesByZone($data['aux']['serial_zon']);

	/* Gets a dealar list with all dealers fron an specific country*/
	$params=array();
	$aux=array('field'=>'cou.serial_cou',
				'value'=>$data['aux']['serial_cou'],
				'like'=> 1);
	array_push($params,$aux);

	$aux=array('field'=>'d.dea_serial_dea',
				'value'=>'NULL',
				'like'=> 1);
	array_push($params,$aux);

	$dealer=new Dealer($db);
	$dealerList=$dealer->getDealers($params);

	$params=array();
	$aux=array('field'=>'d.dea_serial_dea',
				'value'=>$data['aux']['serial_dea2'],
				'like'=> 1);
	array_push($params,$aux);

	$dealer2=new Dealer($db);
	$dealerList2=$dealer2->getDealers($params);
	$smarty -> register('user,data,profileList,statusList,countryList,cityList,dealerList,dealerList2,countryAccessList,userTypeList,planetAssistUser,serial_cou');
}

$smarty -> register('message,user,data,profileList,statusList,countryList,cityList,dealerList,dealerList2,countryAccessList,userTypeList,planetAssistUser,serial_cou');
$smarty -> display();
?>