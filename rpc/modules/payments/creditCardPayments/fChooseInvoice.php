<?php
/*
File: fChooseInvoice
Author: David Bergmann
Creation Date: 03/08/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
Request::setInteger('1:merchantError');
Request::setString('2:fileNumber');

if($_SESSION['serial_dea']) { //If it's a dealer's user
    $data['userType']="DEALER";
    $branch = new Dealer($db,$_SESSION['serial_dea']);
    $invoicesList = $branch->getInvoicesByBranch();

    if(is_array($invoicesList)){
        foreach($invoicesList as &$i){
            if($i['days'] < 0 ){
                $i['days']=0;
            }
            if($i['status_pay'] == 'PARTIAL' || $i['status_pay'] == 'ONLINE_PENDING'){
                $i['number_inv'] = $i['number_inv'].' (A)';
            }elseif($i['status_pay'] == 'VOID'){
                $i['number_inv'] = $i['number_inv'].' (N)';
            }
            if($i['status_inv']=='VOID'){
                $i['number_inv'] = $i['number_inv'].' (CI)';
            }
        }
    }

    $titles = array("Fecha de Factura","# de Factura","Facturado a","Impago","Hasta 30","Hasta 60","Hasta 90","M&aacute;s de 90");
} elseif($_SESSION['serial_mbc']) {
    $user = new User($db);
    if($user->isMainManagerUser($_SESSION['serial_usr'])) { //If it's PLANETASSIST'S user
        $data['userType']="PLANETASSIST";
        //get the list of countries for the user logged in
        $country=new Country($db);
        $data['countryList']=$country->getPaymentCountries($_SESSION['countryList'], $_SESSION['serial_mbc']);
    } else { //If it's a manager's user
        $data['userType']="MANAGER";
        $managerbc = new ManagerbyCountry($db, $_SESSION['serial_mbc']);
        $managerbc->getData();
        $country = new Country($db,$managerbc->getSerial_cou());
        $country->getData();
        $data['nameCountry'] = $country->getName_cou();
        $city=new City($db);
        $data['cityList']=$city->getPaymentCitiesByCountry($country->getSerial_cou(), $_SESSION['serial_mbc'], $_SESSION['cityList']);
        $manager = new Manager($db,$managerbc->getSerial_man());
        $manager->getData();
        $data['nameManager'] = $manager->getName_man();
        $data['serialMbc'] = $_SESSION['serial_mbc'];
    }
}

$smarty -> register('error,merchantError,invoicesList,titles,data,fileNumber');
$smarty->display();
?>