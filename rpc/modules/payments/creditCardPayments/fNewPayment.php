<?php
/*
File: fNewPayment
Author: David Bergmann
Creation Date: 05/08/2010
Last Modified:
Modified By:
*/

//die(Debug::print_r($_POST));
if(is_array($_POST['chkInvoice'])) {
    $_SESSION['current_sale_products'] = implode("_", $_POST['chkInvoice']);
    $totalInvoice = 0;
    foreach ($_POST['chkInvoice'] as $i) {
        $invoice = new Invoice($db, $i);
        $invoice->getData();
        if($invoice->getSerial_pay()) {
            $serial_pay = $invoice->getSerial_pay();
            $payment = new Payment($db, $serial_pay);
            $payment->getData();
            $totalInvoice += ($payment->getTotal_to_pay_pay() - $payment->getTotal_payed_pay());
        } else {
            $totalInvoice += $invoice->getTotal_inv();
        }
    }

    $totalInvoice = number_format($totalInvoice, 2,".","");

    $_SESSION['current_sale_total'] = $totalInvoice;
    $countryList = Country::getAllCountries($db,false);
    $yearlist = array();
    $year=date('Y');
    for($i=0; $i<11; $i++) {
        array_push($yearlist, $year+$i);
    }
} else {
    http_redirect('modules/payments/creditCardPayments/fChooseInvoice/2');
}

$smarty -> register('countryList,global_weekMonths,yearlist,totalInvoice,serial_pay');
$smarty->display();
?>