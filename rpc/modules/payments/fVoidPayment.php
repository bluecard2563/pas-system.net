<?php
/*
File: fVoidPayment.php
Author: Edwin Salvador
Creation Date:19/03/2010
Modified By:
Last Modified: 
*/
//Debug::print_r($_POST);//die;
Request::setInteger('txtInvoice');
$invoice = new Invoice($db);
$serial_pay = $invoice->getSerialPayByNumber($txtInvoice);

$invoiceData = $invoice->getInvoicesByPayment($serial_pay);

if($invoiceData){
	//$paymentData['invoices'] = $invoiceData;

	$payment = new Payment($db,$serial_pay);
	$payment->getData();
	$payment = get_object_vars($payment);
	if($payment){
		unset ($payment['db']);
		$payment['pending_to_pay'] = number_format($payment['total_to_pay_pay'] - $payment['total_payed_pay'], 2, '.', '');
		$paymentData = $payment;
		$paymentData['invoices'] = $invoiceData;
		$countInvoices = sizeof($paymentData['invoices']);
		$paymentDetail = new PaymentDetail($db,NULL,$paymentData['serial_pay']);
		$paymentData['details'] = $paymentDetail->getPaymentDetails();
		$paymentData['total_active'] = 0;
		$paymentData['total_void'] = 0;
		foreach ($paymentData['details'] as $d) {
			if($d['status_pyf'] == 'ACTIVE'){
				$paymentData['total_active'] += $d['amount_pyf'];
			}elseif($d['status_pyf'] == 'VOID'){
				$paymentData['total_void'] += $d['amount_pyf'];
			}
		}
		$paymentData['total_active'] = number_format($paymentData['total_active'], 2, '.', '');
		$paymentData['total_void'] = number_format($paymentData['total_void'], 2, '.', '');
		//Debug::print_r($paymentData);
	}else{//if there is not a required parameter the redirect to the choose invoice screen
		http_redirect('modules/payments/fChoosePaymentToVoid/2');
	}
}else{//if there is not a required parameter the redirect to the choose invoice screen
    http_redirect('modules/payments/fChoosePaymentToVoid/2');
}
//Debug::print_r($paymentData);
$smarty->register('paymentData,countInvoices');
$smarty->display();
?>