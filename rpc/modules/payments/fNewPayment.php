<?php
/*
File: fNewPayment.php
Author: Edwin Salvador
Creation Date:12/03/2010
Modified By:
Last Modified: 
*/
//Debug::print_r($_POST);die;
Request::setInteger('selDealer');
Request::setInteger('selBranch');
$invoicesIds = $_POST['chkInvoice'];
$invIds = $_POST['hdnSerialInv'];
$auxArray=array();
foreach($invIds as $k=>$inv){
    if(in_array($inv, $invoicesIds)){
        $auxArray[$k]=$inv;
    }
}
$invoicesIds=$auxArray;
$invoiceTo=$_POST['hdnInvoiceTo'];
$invoiceToSerial=$_POST['hdnInvoiceToSerial'];
$invoiceTo=updateArray($invoiceTo,$invoicesIds);
$invoiceToSerial=updateArray($invoiceToSerial,$invoicesIds);



if($selBranch && $invoicesIds){
    //get the invoices info
    $branch=new Dealer($db,$selBranch);
    $invoicesIds = implode(',', $invoicesIds);
    $invoicesList = $branch->getInvoicesBySerials($invoicesIds);
}

if($selDealer && $selBranch && is_array($invoicesList)){
        //get dealer info
        $dealer = new Dealer($db, $selDealer);
        $dealer->getData();
        $dealerData = get_object_vars($dealer);
        unset($dealerData['db']);

        //get branch info
        $branch=new Dealer($db,$selBranch);
        $branch->getData();
        $branchData = get_object_vars($branch);
        unset($branchData['db']);
        //get the credit days of the branch
        $creditD = new CreditDay($db, $branchData['serial_cdd']);
        $creditD->getData();
        $creditData = get_object_vars($creditD);
        $branchData['credit_day'] = $creditData['days_cdd'];
        //get the branch country code
        $country = new Country($db,$branchData['aux_data']['serial_cou']);
        $country->getData();
        $country = get_object_vars($country);
        $country_code = $country['code_cou'];
        //get the brach city code
        $city = new City($db,$branchData['aux_data']['serial_cit']);
        $city->getData();
        $city = get_object_vars($city);
        $city_code = $city['code_cit'];

        //the branch code
        $branchData['branch_code'] = $country_code.'-'.$city_code.'-'.$dealerData['code_dea'].'-'.$branchData['branch_number_dea'];

        $paymentDetail = new PaymentDetail($db);
        $formsList = $paymentDetail->getPaymentForms();

		foreach($formsList as $key=>$f){
			if($f == 'UNCOLLECTABLE' || $f == 'PAYPAL'){
				unset($formsList[$key]);
			}
		}

        //get total to pay for all the selected invoices
        $total = 0;
        if(is_array($invoicesList)){
            foreach($invoicesList as $il){
                //get total to pay for all the selected invoices
                if($il['status_inv']=='STAND-BY'){
                    $total_pending += number_format($il['total_to_pay'],2, '.', '');
                    $total += number_format($il['total_inv'],2, '.', '');
                }                
				//if is a partially payed invoice get the payment detail
                if($il['serial_pay']){
                    $details = array();
                    $paymentDetail->setSerial_pay($il['serial_pay']);
                    $details = $paymentDetail->getPaymentDetails(/*'ACTIVE'*/);
                }
            }
        }
		$total_pending = number_format($total_pending,2, '.', '');
        $total = number_format($total,2, '.', '');
        $countInvoices = sizeof($invoicesList);
        //get the banks parameter to fill the banks list
        $parameterBank = new Parameter($db,5);//BANKS
        $parameterBank->getData();
        $parameterBank = get_object_vars($parameterBank);
        $banks = explode(',',$parameterBank['value_par']);
		//get the credit cards parameter to fill the credit cards list
        $parameterCards = new Parameter($db,6);//CREDIT CARDS
        $parameterCards->getData();
        $parameterCards = get_object_vars($parameterCards);
        $creditCards = explode(',',$parameterCards['value_par']);

        if(checkInvoiceTo($invoiceTo,$invoiceToSerial)){
            $auxFlag=0;
            foreach($invoiceTo as $it){
                if($it=='dea'){
                    $auxFlag=0;
                }else{
                    $auxFlag=1;
                }
                break;
            }
            foreach($invoiceToSerial as $its){
                if($auxFlag==1){
                    $serial_cus=$its;
                    $serial_dea=null;
                }else{
                    $serial_cus=null;
                    $serial_dea=$its;
                }
                $serialTo=$its;
                break;
            }
        }else{
           $serial_cus=null;
           $serial_dea=null;
        }

        //verifies payment excess
        $payExcessList=$dealer->checkExcessPayment($serial_dea,$serial_cus);
        if(!$payExcessList){
            unset($formsList[5]);
        }
        //verifies if credit notes
        $cn=new CreditNote($db);
        $creditNoteList=$cn->getCreditNotesForPayment($serial_dea,$serial_cus);
        if(!$creditNoteList){
            unset($formsList[3]);
        }

		$count = sizeof($details)-1;
}else{//if there is not a required parameter the redirect to the choose invoice screen
    http_redirect('modules/payments/fChooseInvoice/2');
}
//Debug::print_r($invoicesList);
$smarty->register('dealerData,branchData,invoicesList,formsList,total,total_pending,countInvoices,banks,creditCards,invoicesIds,details,count,payExcessList,creditNoteList');
$smarty->display();

//function that helps to determine if the payment is going to be exec. just for one entity
function checkInvoiceTo($invTo,$invToSerial){
    $aux=$invTo[0];
    foreach($invTo as $i){
        if($i!=$aux){
            return false;
        }
        $aux=$i;
    }
    $aux=$invToSerial[0];
   foreach($invToSerial as $i){

        if($i!=$aux){
            return false;
        }
        $aux=$i;
    }
    return true;

}

//function that set array's original positions from the form
function updateArray($array, $baseArray){
    $returnArray = array();
    foreach($baseArray as $k=>$b){
        array_push($returnArray,$array[$k]);
    }
    return $returnArray;
}
?>
