<?php
/*
File:fPayedAccounts.php
Author: Mario L�pez
Creation Date:26/03/2010
Modified By: Gabriela Guerrero
Last Modified: 20/05/2010
*/
Request::setInteger('0:error');

//Contries list
$country = new Country($db);
$countryList = $country->getCountriesByInvoice($_SESSION['countryList']);

//Language to be used on data retrieve
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//DEALER CATEGORIES
$dealer=new Dealer($db);
$categoryList=$dealer->getCategories();

//PAYMENT STATES
$paymentstates = array();
$payment = new Payment($db);
$paymentstatesRaw = $payment->getPaymentStates();
foreach($paymentstatesRaw as $state){
    $paymentstates[$state]=$global_paymentsStatus[$state];
}

$smarty -> register('countryList,error,categoryList,paymentstates');
$smarty->display();
?>