<?php
if(isset($_POST['selCountry'])){
    $payments = new Payment($db);
	$invoice = new Invoice($db);
    $invoicesRaw = array();
    $invoices = array();
    $colsAux = array();
    $serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
    $serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
    $serial_usr = (isset($_POST['selResponsable'])) ? $_POST['selResponsable'] : '';
    $category_dea = (isset($_POST['selCategory'])) ? $_POST['selCategory'] : '';
    $serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
    $dea_serial_dea = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
    $payment_status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
    $reportTitle = '';
    $alignCols=array();

    //DETAILED REPORT
    if(isset($_POST['chkDetallado'])){
        $reportTitle = '<b>Reporte Detallado de Pagos de Cuentas</b>';
		$invoicesRaw = $payments->getDetailedPayedInvoices($serial_cou, $serial_cit, $serial_usr, $category_dea, $serial_dea, $dea_serial_dea,$payment_status);
		if($payment_status){
            $colsAux = array(
                'invoices'=>"<b>Documento</b>",
				'total_inv'=>"<b>Monto a Pagar</b>",
                'type_pyf'=>'<b>Forma de Pago</b>',
                'date_pyf'=>'<b>Fecha de Pago</b>',
                'amount_pyf'=>'<b>Monto</b>'
            );
            $alignCols= array('invoices'=>array('justification'=>'center','width'=>120),
						  'total_inv'=>array('justification'=>'center','width'=>85),
                          'type_pyf'=>array('justification'=>'center','width'=>85),
                          'date_pyf'=>array('justification'=>'center','width'=>85),
                          'amount_pyf'=>array('justification'=>'right','width'=>55)
                    );
        }else{
            $colsAux = array(
                'invoices'=>"<b>Documento</b>",
				'total_inv'=>"<b>Monto a Pagar</b>",
                'status_pay'=>"<b>Estado</b>",
                'type_pyf'=>'<b>Forma de Pago</b>',
                'date_pyf'=>'<b>Fecha de Pago</b>',
                'amount_pyf'=>'<b>Monto</b>'
            );
            $alignCols= array('invoices'=>array('justification'=>'center','width'=>120),
						  'total_inv'=>array('justification'=>'center','width'=>85),
                          'status_pay'=>array('justification'=>'center','width'=>65),
                          'type_pyf'=>array('justification'=>'center','width'=>85),
                          'date_pyf'=>array('justification'=>'center','width'=>85),
                          'amount_pyf'=>array('justification'=>'right','width'=>55)
                    );
        }
        if(!$invoicesRaw){
            http_redirect('modules/payments/reports/fReportAccounts');
        }
        $num=$invoicesRaw[0]['invoices'];
        $nom=$invoicesRaw[0]['serial_pay'];
        $f=TRUE;
        $total=0;
		$totalInvoice = 0;
        foreach ($invoicesRaw as $inv){
			$inv['date_pyf'] = date('Y-m-d',strtotime($inv['date_pyf']));
			//$totalInvoice = $inv['total_inv'];
            if($num == $inv['invoices'] && $nom == $inv['serial_pay']){
                $invoicesStr = '';
				$totalInv = '';
				$status = '';
                if($f){
                    $invoicesStr = $inv['invoices'];
					$totalInv = $inv['total_inv'];					
					$status = $global_paymentsStatus[$inv['status_pay']];
                }
                $f=FALSE;
                if($payment_status){
                    $invoices[] = array('invoices'=>$invoicesStr, 'total_inv'=>$totalInv, 'type_pyf'=>html_entity_decode($global_paymentForms[$inv['type_pyf']]), 'date_pyf'=>$inv['date_pyf'], 'amount_pyf'=>$inv['amount_pyf']);
                }else{
                    $invoices[] = array('invoices'=>$invoicesStr, 'total_inv'=>$totalInv, 'status_pay' => $status, 'type_pyf'=>html_entity_decode($global_paymentForms[$inv['type_pyf']]), 'date_pyf'=>$inv['date_pyf'], 'amount_pyf'=>$inv['amount_pyf']);
                }
                $total += $inv['amount_pyf'];
            }else{
                if($payment_status){
                    $invoices[]=array('invoices'=>'', 'total_inv'=>'', 'type_pyf'=>'', 'date_pyf'=>'<b>Total Pagado:</b>', 'amount_pyf'=>'<b>'.number_format($total,2).'</b>');
                }else{
                    $invoices[]=array('invoices'=>'', 'total_inv'=>'', 'status_pay' => '', 'type_pyf'=>'', 'date_pyf'=>'<b>Total Pagado:</b>', 'amount_pyf'=>'<b>'.number_format($total,2).'</b>');
                }
				if(number_format($total,2)<$totalInvoice){
					if($payment_status){
						$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'type_pyf'=>'', 'date_pyf'=>'<b>Monto en Exceso:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
					}else{
						$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'status_pay' => '', 'type_pyf'=>'', 'date_pyf'=>'<b>Monto en Exceso:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
					}
				}
				if(number_format($total,2)>$totalInvoice){
					if($payment_status){
						$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'type_pyf'=>'', 'date_pyf'=>'<b>Saldo:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
					}else{
						$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'status_pay' => '', 'type_pyf'=>'', 'date_pyf'=>'<b>Saldo:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
					}
				}
                $total=0;
                if($payment_status){
                    $invoices[] = array('invoices'=>$inv['invoices'], 'total_inv'=>$inv['total_inv'], 'type_pyf'=>html_entity_decode($global_paymentForms[$inv['type_pyf']]), 'date_pyf'=>$inv['date_pyf'], 'amount_pyf'=>$inv['amount_pyf']);;
                }else{
                    $invoices[] = array('invoices'=>$inv['invoices'], 'total_inv'=>$inv['total_inv'], 'status_pay' => $global_paymentsStatus[$inv['status_pay']], 'type_pyf'=>html_entity_decode($global_paymentForms[$inv['type_pyf']]), 'date_pyf'=>$inv['date_pyf'], 'amount_pyf'=>$inv['amount_pyf']);;
                }
                $total += $inv['amount_pyf'];
            }
			$totalInvoice = $inv['total_inv'];
            $num = $inv['invoices'];
            $nom = $inv['serial_pay'];
        }
        if($payment_status){
            $invoices[]=array('invoices'=>'', 'total_inv'=>'', 'type_pyf'=>'', 'date_pyf'=>'<b>Total Pagado:</b>', 'amount_pyf'=>'<b>'.number_format($total,2).'</b>');
        }else{
            $invoices[]=array('invoices'=>'', 'total_inv'=>'', 'status_pay' => '', 'type_pyf'=>'', 'date_pyf'=>'<b>Total Pagado:</b>', 'amount_pyf'=>'<b>'.number_format($total,2).'</b>');
        }
		if(number_format($total,2)<$totalInvoice){
			if($payment_status){
				$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'type_pyf'=>'', 'date_pyf'=>'<b>Monto en Exceso:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
			}else{
				$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'status_pay' => '', 'type_pyf'=>'', 'date_pyf'=>'<b>Monto en Exceso:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
			}
		}
		if(number_format($total,2)>$totalInvoice){
			if($payment_status){
				$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'type_pyf'=>'', 'date_pyf'=>'<b>Saldo:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
			}else{
				$invoices[]=array('invoices'=>'', 'total_inv'=>'', 'status_pay' => '', 'type_pyf'=>'', 'date_pyf'=>'<b>Saldo:</b>', 'amount_pyf'=>'<b>'.($totalInvoice-number_format($total,2)).'</b>');
			}
		}
    //NORMAL REPORT
    }else{
        $reportTitle = '<b>Reporte de Cuentas</b>';
		if($_POST['selStatus'] == 'CxC'){
			$invoicesRaw = $invoice->getNotPayedInvoices($serial_cou, $serial_cit, $serial_usr, $category_dea, $serial_dea, $dea_serial_dea);
		}
		else{
			$invoicesRaw = $payments->getPayedInvoices($serial_cou, $serial_cit, $serial_usr, $category_dea, $serial_dea, $dea_serial_dea, $payment_status);
		}
		
        //debug::print_r($invoicesRaw);die();
        if(!$invoicesRaw){
            http_redirect('modules/payments/reports/fPayedAccounts');
        }
        if($payment_status){
            $colsAux = array(
                'name_doc'=>'<b>Tipo de Documento</b>',
                'number_inv'=>"<b># Documento</b>",
                'total_to_pay_pay'=>'<b>Monto Total</b>',
                'total_payed_pay'=>'<b>Monto Pagado</b>',
                'date_pay'=>'<b>Fecha de Pago</b>',
                'date_inv'=>'<b>'.utf8_decode('Fecha Emisión Factura').'</b>',
                'due_date_inv'=>'<b>'.utf8_decode('Fecha Límite Pago').'</b>',
                'to_name'=>'<b>a Nombre de</b>',
                'inv_name'=>'<b>Nombre</b>'
            );
            $alignCols= array('name_doc'=>array('justification'=>'center','width'=>80),
                          'number_inv'=>array('justification'=>'center','width'=>65),
                          'total_to_pay_pay'=>array('justification'=>'right','width'=>55),
                          'total_payed_pay'=>array('justification'=>'right','width'=>55),
                          'date_pay'=>array('justification'=>'center','width'=>70),
                          'date_inv'=>array('justification'=>'center','width'=>80),
                          'due_date_inv'=>array('justification'=>'center','width'=>80),
                          'to_name'=>array('justification'=>'center','width'=>90),
                          'inv_name'=>array('justification'=>'center','width'=>80)
                    );
            $toInvoice='';
            $toName='';
            foreach ($invoicesRaw as $inv) {
				$inv['date_pay'] = date('Y-m-d',strtotime($inv['date_pay']));
				$inv['date_inv'] = date('Y-m-d',strtotime($inv['date_inv']));
				$inv['due_date_inv'] = date('Y-m-d',strtotime($inv['due_date_inv']));
                if($inv['dea_name']){
                    $toInvoice='Comercializador';
                    $toName=$inv['dea_name'];
                }elseif($inv['cus_name']){
                    $toInvoice='Cliente';
                    $toName=$inv['cus_name'];
                }
                $invoices[] = array('name_doc'=>$inv['name_doc'],
                                    'number_inv' => $inv['number_inv'],
                                    'total_to_pay_pay' => $inv['total_to_pay_pay'],
                                    'total_payed_pay' => $inv['total_payed_pay'],
                                    'date_pay' => $inv['date_pay'],
                                    'date_inv'=>$inv['date_inv'],
                                    'due_date_inv'=>$inv['due_date_inv'],
                                    'to_name'=>$toInvoice,
                                    'inv_name'=>$toName
                                   );
            }
        }else{
            $colsAux = array(
                'name_doc'=>'<b>Tipo de Documento</b>',
                'number_inv'=>"<b># Documento</b>",
                'status_pay'=>"<b>Estado</b>",
                'total_to_pay_pay'=>'<b>Monto Total</b>',
                'total_payed_pay'=>'<b>Monto Pagado</b>',
                'date_pay'=>'<b>Fecha de Pago</b>',
                'date_inv'=>'<b>'.utf8_decode('Fecha Emisión Factura').'</b>',
                'due_date_inv'=>'<b>'.utf8_decode('Fecha Límite Pago').'</b>',
                'to_name'=>'<b>a Nombre de</b>',
                'inv_name'=>'<b>Nombre</b>'
            );
            $alignCols= array(
                          'name_doc'=>array('justification'=>'center','width'=>80),
                          'number_inv'=>array('justification'=>'center','width'=>65),
                          'status_pay'=>array('justification'=>'center','width'=>65),
                          'total_to_pay_pay'=>array('justification'=>'right','width'=>55),
                          'total_payed_pay'=>array('justification'=>'right','width'=>55),
                          'date_pay'=>array('justification'=>'center','width'=>70),
                          'date_inv'=>array('justification'=>'center','width'=>80),
                          'due_date_inv'=>array('justification'=>'center','width'=>80),
                          'to_name'=>array('justification'=>'center','width'=>90),
                          'inv_name'=>array('justification'=>'center','width'=>80)
                    );
            $toInvoice='';
            $toName='';
			//debug::print_r($invoicesRaw);
            foreach ($invoicesRaw as $inv) {
				$inv['date_pay'] = date('Y-m-d',strtotime($inv['date_pay']));
				$inv['date_inv'] = date('Y-m-d',strtotime($inv['date_inv']));
				$inv['due_date_inv'] = date('Y-m-d',strtotime($inv['due_date_inv']));
				if($inv['serial_pay'] == NULL){
					$inv['date_pay'] = 'N/A';
					$inv['total_payed_pay'] = 'N/A';
					$inv['status_pay'] = 'CxC';
					$inv['total_to_pay_pay'] = $inv['total_inv'];
				}
                if($inv['dea_name']){
                    $toInvoice='Comercializador';
                    $toName=$inv['dea_name'];
                }elseif($inv['cus_name']){
                    $toInvoice='Cliente';
                    $toName=$inv['cus_name'];
                }
                $invoices[] = array(
                                    'name_doc'=>$inv['name_doc'],
                                    'number_inv' => $inv['number_inv'],
                                    'status_pay' => $global_paymentsStatus[$inv['status_pay']],
                                    'total_to_pay_pay' => $inv['total_to_pay_pay'],
                                    'total_payed_pay' => $inv['total_payed_pay'],
                                    'date_pay' => $inv['date_pay'],
                                    'date_inv'=>$inv['date_inv'],
                                    'due_date_inv'=>$inv['due_date_inv'],
                                    'to_name'=>$toInvoice,
                                    'inv_name'=>$toName
                                   );
            }
        }
    }

    $country = new Country($db, $serial_cou);
    $country->getData();
    $countryName = $country->getName_cou();
    $cityName = '';
    $managerName = '';
    $category = '';
    $dealerName = '';
    $branchName = '';

    if($serial_cit){
        $city = new City($db, $serial_cit);
        $city->getData();
        $cityName = $city->getName_cit();
    }
    if($serial_usr){
        $user = new User($db, $serial_usr);
        $user->getData();
        $managerName = $user->getFirstname_usr().' '.$user->getLastname_usr();
    }
    if($category_dea){
        $category = $category_dea;
    }
    if($serial_dea){
        $dealer = new Dealer($db, $serial_dea);
        $dealer->getData();
        $dealerName = $dealer->getName_dea();
    }
    if($dea_serial_dea){
        $branch = new Dealer($db, $dea_serial_dea);
        $branch->getData();
        $branchName = $branch->getName_dea();
    }

    //Debug::print_r($invoicesList);
    include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

    $cols = $colsAux;

    $options = array(
            'showLines'=>2,
            'showHeadings'=>1,
            'shaded'=>0,
            'fontSize' => 10,
            'textCol' =>array(0,0,0),
            'titleFontSize' => 12,
            'rowGap' => 5,
            'colGap' => 5,
            'lineCol'=>array(0.8,0.8,0.8),
            'xPos'=>'center',
            'xOrientation'=>'center',
            /*'width' => 700,*/
            'maxWidth' => 805,
            'cols' =>$alignCols,
            'innerLineThickness' => 0.8,
            'outerLineThickness' => 0.8/*,
            'protectRows' => 3*/
    );
	
    //BEGIN FILTERS
	$rowNum=0;
	$colNum=0;
	$headerRows[$rowNum][$colNum]=utf8_decode('<b>País: </b>').$countryName;
    if($cityName){
		$colNum=$colNum+1;
		$headerRows[$rowNum][$colNum]= '  <b>Ciudad:</b>'.$cityName;
    }
    if($managerName){
		$colNum=$colNum+1;
		$headerRows[$rowNum][$colNum]= '<b>Representante: </b>'.$managerName;
        if($category){
			$colNum=$colNum+1;
			$headerRows[$rowNum][$colNum]= utf8_decode('  <b>Categoría: </b>').$category;
        }
    }
    if($dealerName){
		if($colNum==3){
			$colNum=-1;
			$rowNum=$rowNum+1;
		}
		$colNum=$colNum+1;
		$headerRows[$rowNum][$colNum]= '<b>Comercializador: </b>'.$dealerName;
        if($branchName){
			if($colNum==3){
				$colNum=-1;
				$rowNum=$rowNum+1;
			}
			$colNum=$colNum+1;
			$headerRows[$rowNum][$colNum]= '<b>Sucursal de Comercializador: </b>'.$branchName;
        }

    }
    if($payment_status){
		if($colNum==3){
				$colNum=-1;
				$rowNum=$rowNum+1;
		}
		$colNum=$colNum+1;
		$headerRows[$rowNum][$colNum]= '<b>Estado: </b>'.$global_paymentsStatus[$payment_status];
    }
    //END FILTERS

	if($_POST['selStatus'] == 'CxC'){
		unset($cols['total_to_pay_pay']);
		unset($cols['total_payed_pay']);
		unset($cols['date_pay']);
		foreach($invoices as &$inv){
			unset($inv['total_to_pay_pay']);
			unset($inv['total_payed_pay']);
			unset($inv['date_pay']);
		}
	}

    $pdf->ezSetDy(-20);
	$pdf->ezText('<b>PARAMETROS APLICADOS </b>',11,array('justification'=>'center'));
	$pdf->ezSetDy(-20);
	$pdf->ezTable($headerRows, '', NULL, array('showHeadings'=>0,'showLines'=> 0, 'shaded'=>0));
	$pdf->ezSetDy(-20);
	$pdf->ezText('<b>DETALLE </b>',11,array('justification'=>'center'));
	$pdf->ezSetDy(-20);
    $pdf->ezTable($invoices, $cols, NULL,$options);
    $pdf->ezStream();
    //}
}else{
    http_redirect('modules/payments/reports/fPayedAccounts');
}
?>