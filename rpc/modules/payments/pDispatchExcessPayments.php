<?php
/*
 * File: pDispatchExcessPayments.php
 * Author: Patricio Astudillo
 * Creation Date: 18/05/2010, 09:06:09 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 18/05/2010, 09:06:09 AM
 */

//Debug::print_r($_POST);
$payment=new Payment($db);
if($_POST['chkPayment']){
	$paymentsSerials= implode(',' , $_POST['chkPayment']);
	$paymentData=$payment->getDispatchedPayments($paymentsSerials);

	if(is_array($paymentData)){
		$receiver=$paymentData['0']['receiver'];
		$branch=new Dealer($db, $_POST['selBranch']);
		$branch->getData();
		$total_to_pay=0;
		$details=array();
		$cont=1;
		foreach($paymentData as $pl){
			$total_to_pay+=$pl['excess_amount'];
			$descLine1=array('col1'=>$cont,
							 'col2'=>$pl['number_inv'],
							 'col3'=>$pl['date_pay'],
							 'col4'=>$pl['excess_amount']
			);
			array_push($details,$descLine1);
			$cont++;
		}


		include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

		//HEADER
		$pdf->ezText(utf8_decode('LIQUIDACIÓN DE PAGOS EN EXCESO'), 12, array('justification'=>'center'));//title of each page

		$pdf->ezSetDy(-10);
		$headerDesc=array();
		$descLine1=array('col1'=>utf8_decode('<b>Liquidación a nombre de:</b>'),
						 'col2'=>$receiver);
		array_push($headerDesc,$descLine1);
		$descLine2=array('col1'=>'<b>Fecha:</b>',
						 'col2'=>date('d/m/Y'));
		array_push($headerDesc,$descLine2);
		$descLine3=array('col1'=>'<b>Monto a Liquidar:</b>',
						 'col2'=>'$ '.$total_to_pay);
		array_push($headerDesc,$descLine3);
		$pdf->ezTable($headerDesc,
					  array('col1'=>'','col2'=>''),
					  '',
					  array('showHeadings'=>0,
							'shaded'=>0,
							'showLines'=>0,
							'xPos'=>'200',
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'col1'=>array('justification'=>'left','width'=>120),
								'col2'=>array('justification'=>'left','width'=>150))));
		$pdf->ezSetDy(-10);
		//END HEADERS

		/*EXCESS PAYMENTS DETAILS*/
		$pdf->ezText(utf8_decode('DETALLES DE LA LIQUIDACIÓN'), 12, array('justification'=>'center'));//title of each page
		$pdf->ezSetDy(-10);
		$pdf->ezTable($details,
					  array('col1'=>'<b>No.</b>',
							'col2'=>'<b>Factura No</b>',
							'col3'=>'<b>Fecha de Pago</b>',
							'col4'=>'<b>Valor</b>'),
							'',
					  array('showHeadings'=>1,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'col1'=>array('justification'=>'center','width'=>70),
								'col2'=>array('justification'=>'center','width'=>120),
								'col3'=>array('justification'=>'center','width'=>130),
								'col4'=>array('justification'=>'center','width'=>80))));

		$total=array();
		$totalLine1=array('col1'=>'<b>Total:</b>',
						  'col2'=>'$ '.$total_to_pay
		);
		array_push($total, $totalLine1);
		$pdf->ezTable($total,
					  array('col1'=>'',
							'col2'=>''),
							'',
					  array('showHeadings'=>0,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>'407',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'col1'=>array('justification'=>'center','width'=>130),
								'col2'=>array('justification'=>'center','width'=>80))));
		/*END DETAILS*/

		//$pdf->ezStopPageNumbers();
		/*UPDATE THE PAYMENTS WITHOUT THE EXCESS AMOUNT*/
		$error=1;
		foreach($_POST['chkPayment'] as $pl){
			$payment->setSerial_pay($pl);
			$payment->getData();
					$excessAmount = $payment->getExcess_amount_available_pay();
			$payment->setExcess_amount_available_pay('0');

			if($payment->update()){
				ERP_logActivity('update', $payment);//ERP ACTIVITY
				
						$excessPayment = new ExcessPaymentsLiquidation($db);
						$excessPayment -> setSerial_pay($pl);
						$excessPayment -> setSerial_usr($_SESSION['serial_usr']);
						$excessPayment -> setAmount_paid_xpl($excessAmount);
						if(!$excessPayment->insert()) {
							$error=2;
							break;
						}
			} else {
						$error=2;
						break;
					}
		}
		/*END UPDATE*/

		if($error==1){
			$pdf->ezStream();
		}else{
			http_redirect('modules/payments/fDispatchExcessPayments/'.$error);
		}
	}else{
		http_redirect('modules/payments/fDispatchExcessPayments/3');
	}
}else{
	http_redirect('modules/payments/fDispatchExcessPayments/3');
}
?>