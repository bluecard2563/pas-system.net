<?php
/*
 * File: pChooseUncollectableInvoices.php
 * Author: Patricio Astudillo
 * Creation Date: 23/06/2010, 11:19:33 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 23/06/2010, 11:19:33 AM
 */

$invoice=new Invoice($db);

$uncollectables=$_POST['check'];

foreach($uncollectables as $u){
	$payment=new Payment($db);
	$pDetail=new PaymentDetail($db);
	$invoice->setSerial_inv($u);
	$invoice->getData();

	if($invoice->getSerial_pay()!=''){ //The payment is PARTIAL
		$payment->setSerial_pay($invoice->getSerial_pay());
		$payment->getData();
		$uncAmount=$payment->getTotal_to_pay_pay()-$payment->getTotal_payed_pay();

		/*FILL THE PAYMENTS DETAIL*/
		$pDetail->setSerial_pay($invoice->getSerial_pay());
		$pDetail->setSerial_usr($_SESSION['serial_usr']);
		$pDetail->setAmount_pyf($uncAmount);
		$pDetail->setChecked_pyf('YES');
		$pDetail->setComments_pyf('Cuenta Incobrable');
		$pDetail->setType_pyf('UNCOLLECTABLE');
		$pDetail->setStatus_pyf('ACTIVE');
		$pDetail->insert();
		/*END PAYMENT DETAIL*/

		/*UPDATING THE PAYMENT*/
		$payment->setTotal_payed_pay($payment->getTotal_to_pay_pay());
		$payment->setStatus_pay('UNCOLLECTABLE');
		$payment->update();
		
		ERP_logActivity('update', $payment);//ERP ACTIVITY
		/*END PAYMENT*/
	}else{ //NO PAYMENT IS REGISTERED
		$payment->setStatus_pay('UNCOLLECTABLE');
		$payment->setExcess_amount_available_pay('0');
		$payment->setObservation_pay('Cuenta Incobrable');
		$payment->setTotal_payed_pay($invoice->getTotal_inv());
		$payment->setTotal_to_pay_pay($invoice->getTotal_inv());
		$serial_pay=$payment->insert();
		
		ERP_logActivity('new', $payment);//ERP ACTIVITY

		/*FILL THE PAYMENTS DETAIL*/
		$pDetail->setSerial_pay($serial_pay);
		$pDetail->setSerial_usr($_SESSION['serial_usr']);
		$pDetail->setAmount_pyf($invoice->getTotal_inv());
		$pDetail->setChecked_pyf('YES');
		$pDetail->setComments_pyf('Cuenta Incobrable');
		$pDetail->setType_pyf('UNCOLLECTABLE');
		$pDetail->setStatus_pyf('ACTIVE');
		$pDetail->insert();
		/*END PAYMENT DETAIL*/

		$invoice->setSerial_pay($serial_pay);
	}

	/*UPDATING THE INVOICE*/
	$invoice->setStatus_inv('UNCOLLECTABLE');
	if($invoice->update()){
		ERP_logActivity('update', $invoice);//ERP ACTIVITY
		
		$error=1;
	}else{
		$error=2;
		break;
	}
	/*END INVOICE*/
}

http_redirect('modules/payments/fChooseUncollectableInvoices/'.$error);
?>
