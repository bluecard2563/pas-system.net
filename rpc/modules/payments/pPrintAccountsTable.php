<?php
//error_reporting(E_ALL);

if(isset($_SESSION['invoicesToPrint'])){
	$serial_branch = $_SESSION['serial_dea'];
	$invoicesList = $_SESSION['invoicesToPrint'];
	$total_days[0] = $_SESSION['total_days'];
	unset($_SESSION['invoicesToPrint']);
	unset($_SESSION['total_days']);
	unset($_SESSION['serial_dea']);
	//Debug::print_r($invoicesList );
	foreach($invoicesList as &$il){
		$il['to_pay_30']='';
		$il['to_pay_60']='';
		$il['to_pay_90']='';
		$il['to_pay_more']='';
		if($il['days'] < 31 ){
			$il['to_pay_30'] = $il['total_to_pay'];
		}elseif($il['days'] > 30 && $il['days'] < 61 ){
			$il['to_pay_60'] = $il['total_to_pay'];
		}elseif($il['days'] > 60 && $il['days'] < 91 ){
			$il['to_pay_90'] = $il['total_to_pay'];
		}else{
			$il['to_pay_more'] = $il['total_to_pay'];
		}
	}

	$total_table[0]['days'] = 'Hasta 30';
	$total_table[0]['total'] = $total_days[0]['total_30'];
	$total_table[1]['days'] = 'Hasta 60';
	$total_table[1]['total'] = $total_days[0]['total_60'];
	$total_table[2]['days'] = 'Hasta 90';
	$total_table[2]['total'] = $total_days[0]['total_90'];
	$total_table[3]['days'] = htmlspecialchars(utf8_decode('Más de 90'));
	$total_table[3]['total'] = $total_days[0]['total_more'];
	$total_table[4]['days'] = 'Total';
	$total_table[4]['total'] = $total_days[0]['total'];

	//get branch info
	$branch=new Dealer($db,$serial_branch);
	$branch->getData();
	$branchData = get_object_vars($branch);
	unset($branchData['db']);
	//Debug::print_r($branchData);

	//get dealer info
	$serial_dea = $branchData['dea_serial_dea'];
	$dealer = new Dealer($db, $serial_dea);
	$dealer->getData();
	$dealerData = get_object_vars($dealer);
	unset($dealerData['db']);

	//get the credit days of the branch
	$creditD = new CreditDay($db, $branchData['serial_cdd']);
	$creditD->getData();
	$creditData = get_object_vars($creditD);
	$branchData['credit_day'] = $creditData['days_cdd'];
	//get the branch country code
	$country = new Country($db,$branchData['aux_data']['serial_cou']);
	$country->getData();
	$country = get_object_vars($country);
	$country_code = $country['code_cou'];
	//get the brach city code
	$city = new City($db,$branchData['aux_data']['serial_cit']);
	$city->getData();
	$city = get_object_vars($city);
	$city_code = $city['code_cit'];

	//the branch code
	$branchData['branch_code'] = $country_code.'-'.$city_code.'-'.$dealerData['code_dea'].'-'.$branchData['branch_number_dea'];


	//Debug::print_r($invoicesList);
	include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

	$cols = array(
		'date_inv'=>"<b>Fecha de Factura</b>",
		'number_inv'=>'<b># de Factura</b>',
		'invoice_to'=>'<b>Facturado a</b>',
		'days'=>'<b>Impago</b>',
		'to_pay_30'=>'<b>Hasta 30</b>',
		'to_pay_60'=>'<b>Hasta 60</b>',
		'to_pay_90'=>'<b>Hasta 90</b>',
		'to_pay_more'=>'<b>'.htmlspecialchars(utf8_decode('Más de 90')).'</b>'
	);

	$options = array(
		'showLines'=>2,
		'showHeadings'=>1,
		'shaded'=>0,
		//'shadeCol'=>array(0.9,0.9,0.9),
		//'shadeCol2'=>array(0.77,0.87,0.91),
		'fontSize' => 10,
		'textCol' =>array(0,0,0),
		'titleFontSize' => 12,
		'rowGap' => 5,
		'colGap' => 5,
		'lineCol'=>array(0.8,0.8,0.8),
		'xPos'=>'center',
		'xOrientation'=>'center',
		/*'width' => 700,*/
		'maxWidth' => 700,
		'cols' => array('date_inv'=>array('justification'=>'center','width'=>80),
			      'number_inv'=>array('justification'=>'left','width'=>80),
			      'invoice_to'=>array('justification'=>'left','width'=>150),
			      'days'=>array('justification'=>'center','width'=>70),
			      'to_pay_30'=>array('justification'=>'right','width'=>80),
			      'to_pay_60'=>array('justification'=>'right','width'=>80),
			      'to_pay_90'=>array('justification'=>'right','width'=>80),
			      'to_pay_more'=>array('justification'=>'right','width'=>80)
			),
		'innerLineThickness' => 0.8,
		'outerLineThickness' => 0.8/*,
		'protectRows' => 3*/
	);
	$pdf->ezText('<b>Lista de Cuentas por Cobrar</b>', 12, array('justification' =>'center'));
	$pdf->ezSetDy(-10);
	//BEGIN FILTERS
	//$pdf->addText(60,480,10,'<b>Comercializador: </b>');
	//$pdf->addText(160,480,10,$dealerData['name_dea']);
	$pdf->ezText(('<b>Comercializador: </b>'.$dealerData['name_dea'].'  <b>Sucursal de Comercializador:</b>'.$branchData['name_dea']),10,array('left'=>20/*,'justification'=>'center'*/));
	$pdf->ezSetDy(-10);
	$pdf->ezText((utf8_decode('<b>Código: </b>').$branchData['branch_code'].utf8_decode('  <b>Días de Crédito: </b>').$branchData['credit_day']),10,array('left'=>20/*,'justification'=>'center'*/));
	//END FILTERS
	$pdf->ezSetDy(-20);
	$pdf->ezTable($invoicesList, $cols, NULL,$options);

	$pdf->ezSetDy(-10);
	$cols2 = array(
		'days'=>utf8_decode('<b>Días</b>'),
		'total'=>'<b>Total</b>',
	);

	$options2 = array(
		'showLines'=>2,
		'showHeadings'=>1,
		'shaded'=>0,
		/*'shadeCol'=>array(0.9,0.9,0.9),
		'shadeCol2'=>array(0.77,0.87,0.91),*/
		'fontSize' => 10,
		'textCol' =>array(0,0,0),
		'titleFontSize' => 12,
		'rowGap' => 5,
		'colGap' => 5,
		'lineCol'=>array(0.8,0.8,0.8),
		'xPos'=>'center',
		'xOrientation'=>'center',
		/*'width' => 700,*/
		'maxWidth' => 700,
		'cols' => array('days'=>array('justification'=>'right','width'=>80),
			      'total'=>array('justification'=>'right','width'=>80)
			),
		'innerLineThickness' => 0.8,
		'outerLineThickness' => 0.8/*,
		'protectRows' => 3*/
	);
	$pdf->ezTable($total_table,$cols2,NULL,$options2);
	
	/*if (isset($_GET['d']) && $_GET['d']){
	  $pdfcode = $pdf->ezOutput(1);
	  $pdfcode = str_replace("\n","\n<br>",htmlspecialchars($pdfcode));
	  echo '<html><body>';
	  echo trim($pdfcode);
	  echo '</body></html>';
	} else {*/
	  $pdf->ezStream();
	//}

}else{
	http_redirect('modules/payments/fChooseInvoice');
}
?>
