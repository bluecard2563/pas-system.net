<?php
if(isset($_POST['selCountry']) && isset($_POST['selManager']) ){

   $selectedPayments = $_POST['chkPayment'];
   $dateFrom = $_POST['txtDateFrom'];
   $dateTo = $_POST['txtDateTo'];
   
   if($selectedPayments){
	   $band = 0;
	   foreach($selectedPayments as $s){
		   $payment = new PaymentDetail($db);
		   if(!$payment->updateChecked($s,'YES')){
			   $band = 1;
		   }
	   }
	   if($band == 1){
		   http_redirect('modules/payments/fRegisterPayment/3');
	   }
	   else{
		   $payment = new PaymentDetail($db);
		   $payments = array();
		   foreach($selectedPayments as $s){
				$payments[] = $payment->getPayment($s);
		   }
		   //debug::print_r($payments);
		   include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
		   $date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
			$pdf->setColor(0.57, 0.58, 0.60);
			$pdf->ezText($date,13,array('justification'=>'left'));
			$pdf->setColor(0, 0, 0);
			$pdf->ezSetDy(-20);
		   $header = array(
				'payment'=>'<b>Pago</b>',
				'invoice'=>'<b>Factura</b>',
				'type'=>'<b>Tipo</b>',
				'document'=>'<b>Documento</b>',
				'amount'=>'<b>Valor</b>',
				'branch'=>'<b>Comercializador</b>',
				'user'=>'<b>Usuario</b>'
			);
			$alignTable = array(
				'payment'=>array('justification'=>'center','width'=>50),
				'invoice'=>array('justification'=>'center','width'=>50),
				'type'=>array('justification'=>'center','width'=>80),
				'document'=>array('justification'=>'center','width'=>80),
				'amount'=>array('justification'=>'center','width'=>50),
				'branch'=>array('justification'=>'center','width'=>100),
				'user'=>array('justification'=>'center','width'=>100)
			);
			$optionsTable = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 10,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignTable,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);
			$requiredFields = array(
									array('country'=>'<b>Pa�s: </b>'.$payments[0]['NAME_COU'],'manager'=>'<b>Representante: </b>'.$payments[0]['NAME_MAN'])
									);
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>REGISTRO DE PAGOS</b>',14,array('justification'=>'center'));
			$pdf->ezSetDy(-20);
			$pdf->ezTable($requiredFields,array('country'=>'cou','manager'=>'man'),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13,
									'cols'=>array(
									'country'=>array('justification'=>'center','width'=>270),
									'manager'=>array('justification'=>'center','width'=>270)
            )));
			if($dateFrom && $dateTo){
				$dates[0]=array('0'=>'<b>Desde: </b>'.$dateFrom.'   <b>Hasta: </b>'.$dateTo);
				$pdf->ezSetDy(-20);
				$pdf->ezTable($dates,array('0'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13));
			}
			$table = array();
			if($payments){
				foreach($payments as $p){
					if($p['DOCUMENT_NUMBER_PYF']){
						$document = $p['COMMENTS_PYF'].' / '.$p['DOCUMENT_NUMBER_PYF'];
					}
					else{
						$document = $p['COMMENTS_PYF'];
					}
					$table[] = array('payment'=>$p['SERIAL_PYF'],
									'invoice'=>$p['INVOICE_GROUP'],
									'type'=> html_entity_decode ($global_paymentForms[$p['TYPE_PYF']]),
									'document'=>$document,
									'amount'=>number_format($p['AMOUNT_PYF'], 2, '.', ''),
									'branch'=>$p['NAME_DEA'],
									'user'=>$p['NAME_USR']
								);
				}
			}
			$pdf->ezSetDy(-20);
			$pdf->ezTable($table,$header, NULL,$optionsTable);
			$pdf->ezStream();

		   http_redirect('modules/payments/fRegisterPayment/1');
	   }
   }

}else{
    http_redirect('modules/payments/fRegisterPayment/2');
}
?>