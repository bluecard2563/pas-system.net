<?php 
/*
File: pNewServiceTranslation.php
Author: Edwin Salvador
Creation Date: 120201/2010
Last Modified:
Modified By:
*/

$sbl=new ServicesbyLanguage($db);
$sbl->setSerial_ser($_POST['hdnServiceID']);
$sbl->setSerial_lang($_POST['selLanguage']);
$sbl->setName_sbl($_POST['txtSblName']);
$sbl->setDescription_sbl($_POST['txtSblDesc']);

if($sbl->insert()){
    http_redirect('modules/services/fSearchService/1');
}else{
    http_redirect('modules/services/fSearchService/5');
}
?>