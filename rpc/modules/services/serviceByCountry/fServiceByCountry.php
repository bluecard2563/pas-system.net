<?php
/*
File: fProductByCountry.php
Author: Edwin Salvador
Creation Date: 17/02/2010
Last Modified:
Modified By:
*/
Request::setInteger('hdnSblID');
if($hdnSblID){
    $serviceByLanguage=new ServicesbyLanguage($db,$hdnSblID);
    $serviceByLanguage->getData();
    $serial_ser=$serviceByLanguage->getSerial_ser();
    
    $service = new Service($db,$serial_ser);
    $data = $service->getInfoService($_SESSION['language']);
    $data = $data[0];

    $countriesCount = 0;
    $sbc = new ServicesByCountry($db);
    $sbc->setSerial_ser($serial_ser);
    $assigned_countries = $sbc->getAssignedCountries();
    
    if(is_array($assigned_countries)){
        foreach($assigned_countries as &$ac){
            if($ac['assigned_taxes'] != ''){
                $ac['assigned_taxes'] = explode(',',$ac['assigned_taxes']);
            }else{
                unset($ac['assigned_taxes']);
            }
            if($ac['assigned_dealers'] != ''){
                $ac['assigned_dealers'] = explode(',',$ac['assigned_dealers']);
            }else{
                unset($ac['assigned_dealers']);
            }
        
            $ac['zone_countries'] = array();
            $country = new Country($db);
            $country_list = $country->getCountriesByZone($ac['serial_zon'],$_SESSION['countryList']);
        
            foreach($country_list as $key=>$cl){
                $ac['zone_countries'][$key]=array();
                $ac['zone_countries'][$key]['serial_cou'] = $cl['serial_cou'];
                $ac['zone_countries'][$key]['name_cou'] = $cl['name_cou'];
            }

            $tax = new Tax($db);
            $taxList = $tax->getTaxes($ac['serial_cou']);
            if(is_array($taxList)){
                foreach($taxList as $tax_key=>$tl){
                    $ac['country_taxes_serial'][$tax_key] = $tl['serial_tax'];
                    $ac['country_taxes_name'][$tax_key] = $tl['name_tax'];
                }
            }

            if(is_array($ac['assigned_dealers'])){
                foreach($ac['assigned_dealers'] as &$ad){
                    $serial_sbd = $ad;
                    $sbd = new ServicesByDealer($db, $ad);
                    $sbd->getData();
                    $dealer = new Dealer($db, $sbd->getSerial_dea());
                    $dealer->getData();
                    $ad = array();
                    $ad['serial_dea'] = $dealer->getSerial_dea();
                    $ad['serial_mbc'] = $dealer->getSerial_mbc();
                    $ad['name_dea'] = $dealer->getName_dea();

                    $used = $sbd->getIsUsed_dea();
                    $ad['used_dea'] = $used[0]['serial_pbd'];
                    /*if($used){
                        $ac['used_dealers'][] = $used[0];
                    }*/
                }
            }
            $mbc = new ManagerbyCountry($db);
            $managerList = $mbc->getManagerbyCountry($ac['serial_cou'], $_SESSION['serial_mbc']);
            if(is_array($managerList)){
                $ac['managers'] = array();
                $ac['managers'] = $managerList;
            }
        }
        $countriesCount = sizeof($assigned_countries)-1;
        $smarty->register('assigned_countries');
    }
//Debug::print_r($assigned_countries);
    $zone = new Zone($db);
    $zonesList = $zone->getZones();
}else{
    http_redirect('modules/product/productByCountry/fSearchProductByCountry/2');
}

$smarty->register('data,zonesList,countriesCount');
$smarty->display();

?>
