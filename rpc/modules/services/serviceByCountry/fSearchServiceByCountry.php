<?php
/*
File: fSearchService.php
Author: Edwin Salvador
Creation Date: 12/02/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$language = new Language($db);
$languageList = $language->getLanguages();

$smarty->register('error,languageList');
$smarty->display();
?>