<?php 
/*
File: pUpdateService.php
Author: Edwin Salvador
Creation Date: 12/02/2010
Last Modified:15/03/2010
Modified By:Miguel Ponce
*/

Request::setInteger('serial_ser');
$service=new Service($db,$serial_ser);
$service->setStatus_ser('INACTIVE');
$service->setFee_type_ser('TOTAL');
if($_POST['status_ser'])
    $service->setStatus_ser($_POST['status_ser']);
if($_POST['fee_type_ser'])
    $service->setFee_type_ser($_POST['fee_type_ser']);
if($service->update()){

    $sbc=new ServicesByCountry($db,$_POST['serial_sbc'] );
    $sbc->setSerial_cou('1');
    $sbc->setSerial_ser($serial_ser);
    $sbc->setStatus_sbc('ACTIVE');
    $sbc->setPrice_sbc($_POST['price_sbc']);
    $sbc->setCost_sbc($_POST['cost_sbc']);
    $sbc->setCoverage_sbc($_POST['coverage_sbc']);
    if($sbc->update()){
        $sbc=new ServicesbyLanguage($db,$_POST['serial_sbl']);
        $sbc->setSerial_lang($_POST['serial_lang']);
        $sbc->setSerial_ser($serial_ser);
        $sbc->setName_sbl($_POST['name_sbl']);
        $sbc->setDescription_sbl($_POST['description_sbl']);
        if($sbc->update()){
            http_redirect('modules/services/fSearchService/1');
        }
        http_redirect('modules/services/fSearchService/2');
    }
    http_redirect('modules/services/fSearchService/3');
}

?>