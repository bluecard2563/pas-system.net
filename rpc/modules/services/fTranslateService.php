<?php 
/*
File: fUpdateService.php
Author: Edwin Salvador
Creation Date: 12/02/2010
Last Modified:
Modified By:
*/

Request::setInteger('hdnSblID');

$sbl = new ServicesbyLanguage($db, $hdnSblID);

if($sbl->getData()){
    $service_id = $sbl->getSerial_ser();
    $service=new Service($db,$service_id);

    $language = new Language($db);
    $languageList=$language->getRemainingServiceLanguages($service_id);
    //STATUS
    $statusList=$service->getStatusList();
    if($service->getData()){
           // $data['name_ser'] = $service->getName_ser();
            $data['name_sbl'] = $sbl->getName_sbl();
            $data['description_sbl'] = $sbl->getDescription_sbl();
            $data['serial_ser'] = $service_id;
            $data['serial_sbl'] = $hdnSblID;
            $data['status_ser'] = $service->getStatus_ser();

            $translations = $sbl->getTranslations($service_id);

            $titles=array('#','Nombre','Descripci&oacute;n','Idioma','Actualizar');
    }else{
            http_redirect('modules/services/fSearchService/3');
    }
}else{
    http_redirect('modules/services/fSearchService/3');
}
$smarty->register('data,titles,translations,languageList,statusList');
$smarty->display();
?>