<?php 
/*
File: pNewService.php
Author: Edwin Salvador
Creation Date: 11/02/2010
Last Modified:04/03/2010
Modified By:Miguel Ponce
*/

$language=new Language($db);
$serial_lang=$language->getSerialByCode($_SESSION['language']);

$service=new Service($db);
$service->setStatus_ser("ACTIVE");
$service->setFee_type_ser($_POST['fee_type_ser']);

$serial_ser=$service->insert();
if($serial_ser){
    $sbl=new ServicesbyLanguage($db);
    $sbl->setSerial_ser($serial_ser);
    $sbl->setSerial_lang($serial_lang);
    $sbl->setName_sbl($_POST['name_sbl']);
    $sbl->setDescription_sbl($_POST['description_sbl']);
    
    if($sbl->insert()){
        $sbc=new ServicesByCountry($db);
        $sbc->setSerial_ser($serial_ser);
        $sbc->setStatus_sbc('ACTIVE');
        $sbc->setSerial_cou('1');
        $sbc->setPrice_sbc($_POST['price_sbc']);
        $sbc->setCost_sbc($_POST['cost_sbc']);
        $sbc->setCoverage_sbc($_POST['coverage_sbc']);
        $serial_sbc=$sbc->insert();
        http_redirect('modules/services/fNewService/1');
    }else{
        http_redirect('modules/services/fNewService/3');
    }
}else{
    http_redirect('modules/services/fNewService/2');
}
?>