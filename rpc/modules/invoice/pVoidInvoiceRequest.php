<?php
/*
File: pVoidInvoiceRequest.php
Author: Santiago Benitez
Creation Date: 08/04/2010
Creation Date: 08/04/2010
Modified By: Santiago Benitez
*/

$invLog = new InvoiceLog($db);
$sales=$_POST['sales'];
$checkedSales=$_POST['chkSales'];
$salesCost=$_POST['salesCost'];
$error=2;
foreach($sales as $k=>$serial_sal){
    $invLog->setSerial_inv($_POST['hdnSerialInv']);
    $invLog->setSerial_usr($_SESSION['serial_usr']);
    $invLog->setSerial_sal($serial_sal);
    $invLog->setObservation_inl($_POST['txtObservationsVoid']);
    $invLog->setPenalty_fee_inl($salesCost[$k]);
    $invLog->setVoid_sales_inl(checkSales($serial_sal,$checkedSales));
    if(!$invLog->insert()){
        $error=1;
        break;
    }
}
if($error==2){
	/*GENERATING THE ALERTS*/
	$parameter=new Parameter($db, '15'); //Parameters for INVOICE VOID Requests.
    if($parameter->getData()){
		$destinationUsers=unserialize($parameter->getValue_par());
		$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
		if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
			$usersCountry='62';
		}
		$destinationUsers[$usersCountry]=explode(',', $destinationUsers[$usersCountry]);

		$alert=new Alert($db);
		$alert->setMessage_alt('Usted tiene una solicitud pendiente para anulaci&oacute;n de factura');
                $alert->setRemote_id($_POST['hdnSerialInv']);
                        $alert->setStatus_alt('PENDING');
                $alert->setTable_name('invoice/fAuthorizeInvoiceLog/');
                $alert->setType_alt('INVOICE');
		foreach($destinationUsers[$usersCountry] as $d){
			$alert->setSerial_usr($d);
			if(!$alert->insert()){
				$error=4;
				break;
			}
		}
    }else{
        $error=4;
    }
    /*END ALERTS*/
}
function checkSales($serial_sal,$chkSales){
    if($chkSales){
       foreach($chkSales as $ch){
            if($ch==$serial_sal){
                return 'YES';
            }
        }
    }
    return 'NO';
}
http_redirect('modules/invoice/fSearchInvoice/'.$error);
?>