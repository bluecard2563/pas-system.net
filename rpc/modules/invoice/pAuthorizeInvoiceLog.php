<?php
/*
File: pAuthorizeInvoiceLog.php
Author: Santiago Benitez
Creation Date: 14/04/2010
Creation Date: 14/04/2010
Modified By: Mario López
*/

$invLog = new InvoiceLog($db);
$invLog->setSerial_inv($_POST['hdnSerialInv']);
$invLog->setUsr_serial_usr($_SESSION['serial_usr']);
$error=1;
$cNoteSerial=-1;
if($_POST['selVoidInvoice']=='DENIED'){
    if(!$invLog->changeStatus("DENIED")){
        $error=2;
    }
}else{
    if(!$invLog->changeStatus('AUTHORIZED')){
        $error=2;
    }

    if($error==1){
        $inv =new Invoice($db, $_POST['hdnSerialInv']);
        $serial_mbc=$inv->getSerialMbc();
		
        $mbc=new ManagerbyCountry($db, $serial_mbc['serial_mbc']);
        $mbc->getData();
        $inv->getData();
        $inv=get_object_vars($inv);
        $misc['db']=$db;
        $misc['type']='INVOICE';
        $misc['serial']=$_POST['hdnSerialInv'];
        $misc['serial_man']=$mbc->getSerial_man();
        $misc['person_type']=$_POST['hdnPersonType'];
        $misc['amount']=$_POST['txtTotal'];
        $misc['paymentStatus']='PAID';

        /*Prepare the information for the PDF of the Credit Note*/
        if($_POST['txtName']){
            $cNotePDF['name']=$_POST['txtName'];
        }else{
            $cNotePDF['name']=$_POST['txtFirstName']." ".$_POST['txtLastName'];
        }        
        $cNotePDF['address'] = $_POST['txtAddress'];
        $cNotePDF['document'] = $_POST['txtDocument'];
        $cNotePDF['phone'] = $_POST['txtPhone1'];
        $cNotePDF['type']='INVOICE';
        $cNotePDF['serial_inv']=$_POST['hdnSerialInv'];
        $cNotePDF['date_inv']=$inv['date_inv'];
        $cNotePDF['due_date_inv']=$inv['due_date_inv'];
        /*End PDF Info*/

        $cNote=GlobalFunctions::generateCreditNote($misc);

        $misc['cNoteNumber']=$cNote['number'];
		$misc['cNoteID']=$cNote['id'];
        switch($cNote['error']){
                case 'no_document': $error=7;
                        break;
                case 'insert_error': $error=8;
                        break;
                case 'update_error': $error=9;
                        break;
                case 'cNote_error': $error=10;
                        break;
                case 'success':
                        $error=1;
                        //Generate the PDF File for the Credit Note.
                        //GlobalFunctions::createCreditNotePDF($misc,$cNotePDF);
                        break;
        }
    }
    $invoice=new Invoice($db, $_POST['hdnSerialInv']);
    if(!$invoice->changeStatus('VOID')){
        $error=3;
    }    
    
    $checkedSales=$_POST['chkSales'];
	$sales_to_refund_internationalFee=$_POST['chkChargeInternationalFee'];
	
    $sale=new Sales($db);
    $sales=Array();

    $salesList=$sale->getSalesByInvoice($_POST['hdnSerialInv']);
    if($checkedSales){
        if($salesList){
            foreach($salesList as $sl){
                if(!in_array($sl['serial_sal'], $checkedSales)){
                    array_push($sales, $sl['serial_sal']);
                }
            }
        }        
        foreach($checkedSales as $s){
            $sale->setSerial_sal($s);            
            if(!$sale->changeStatus('VOID')){
                $error=4;
            }else{
                $slg=new SalesLog($db);
				$slg->setType_slg('VOID_INVOICE');
				$slg->setSerial_usr($_SESSION['serial_usr']);
				$slg->setUsr_serial_usr($_SESSION['serial_usr']);
				$slg->setSerial_sal($s);
				$misc=array('old_status_sal'=>'REGISTERED',
									'new_status_sall'=>'VOID',
									'Description'=>'Change sale status due to an invoice deny.',
									'old_serial_inv'=>$_POST['hdnSerialInv'],
									'new_serial_inv'=>'NULL',
									'Description'=>'Change serial invoice due to an invoice deny.');
				$slg->setDescription_slg(serialize($misc));
				if(!$slg->insert()){
					$error=5;
				}

				/* INTERNATIONAL FEE REFUND */
				if(is_array($sales_to_refund_internationalFee)){
					if(in_array($s, $sales_to_refund_internationalFee)){
						GlobalFunctions::refundInternationalFee($db, $s, TRUE);
					}else{
						GlobalFunctions::refundInternationalFee($db, $s, FALSE);
					}
				}else{
					GlobalFunctions::refundInternationalFee($db, $s, FALSE);
				}
				/* END INTERNATIONAL FEE */
            }
        }
    }else{
        $sales=$salesList;
    }
    if($error==1){
       if(!$sale->voidSerial_inv($_POST['hdnSerialInv'])){
            $error=4;
        } 
    }
    
    $slg=new SalesLog($db);
    $slg->setType_slg('VOID_INVOICE');
    $slg->setSerial_usr($_SESSION['serial_usr']);
	$slg->setUsr_serial_usr($_SESSION['serial_usr']);
	
    if($sales){
       foreach ($sales as $sal){
		   $sale->setSerial_sal($sal['serial_sal']);
		   if(!$sale->changeStatus('REGISTERED')){
                $error=4;
            }
            $slg->setSerial_sal($sal['serial_sal']);
            $misc=array('old_serial_inv'=>$_POST['hdnSerialInv'],
                                'new_serial_inv'=>'NULL',
                                'Description'=>'Change serial invoice due to an invoice deny.');
            $slg->setDescription_slg(serialize($misc));
            if(!$slg->insert()){
                $error=5;
            }
        }
    }
}

if($error==1){
    $alert=new Alert($db);
    if(!$alert->setServeAlert('INVOICE',$_POST['hdnSerialInv'])){
        $error=6;
    }
}

http_redirect("modules/invoice/fSearchInvoiceLog/$error/".$cNote['id']);
?>