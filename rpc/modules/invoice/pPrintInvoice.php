<?php
/*
    Document   : pPrintInvoice.php
    Created on : May 3, 2010, 2:05:09 PM
    Author     : H3Dur4k
    Description:
        Generates a PDF file for invoice printing.
*/
Request::setInteger('0:serial_inv');
if(!$serial_inv){
	if($_POST['txtInvoiceNumber'] && $_POST['selDocument'] && $_POST['selManager']){
		$number_inv=$_POST['txtInvoiceNumber'];
		$type_dbc=$_POST['selDocument'];
		$serial_mbc=$_POST['selManager'];
		$reprint=true;
	}
	$invoice=new Invoice($db);
	$serial_inv=$invoice->getInvoiceExistToPrint($number_inv, $serial_mbc, $type_dbc);
}
if($serial_inv){
	//parameters for number of sales an taxes to be shown
	$maxSales =5; //this will be dynamic based on number of taxes applied
	$maxTaxes =5;
	//initialize values
	$salesTotal=0;
	$taxesTotal=0;
	$subtotal=0;
	$discount=0;
	$otherDiscount=0;
	$discountPercentage=0;
	$otherDiscountPercentage=0;
	$taxesPercentage=0;
	$invoiceTotal=0;
	//get Invoice data
	$invoice=new Invoice($db,$serial_inv);
	$invoice->getData();
	if(!$reprint){
		if($invoice->getPrinted_inv()=='YES'){
			http_redirect('modules/invoice/fChooseSales/11/'.$serial_inv);
		}
	}
	$invoiceData=get_object_vars($invoice);
	unset($invoiceData['db']);
	//get invoice to information (user or dealer)
	$city=new City($db);
	$country=new Country($db);
	if($invoiceData['serial_cus']!=''){
		$customer=new Customer($db,$invoiceData['serial_cus']);
		$customer->getData();
		$auxData=$customer->getAuxData();
		$country->setSerial_cou($auxData['serial_cou']);
		$country->getData();
		$city->setSerial_cit($customer->getSerial_cit());
		$city->getData();
	}else{
		$dealer= new Dealer($db,$invoiceData['serial_dea']);
		$dealer->getData();
		$auxData=$dealer->getAuxData_dea();
		$country->setSerial_cou($auxData['serial_cou']);
		$country->getData();
		$city->setSerial_cit($auxData['serial_cit']);
		$city->getData();
	}


	//retrieve all the invoice related sales
	$invoiceCardsDetail=$invoice->getInvoiceSales($serial_inv);
	$numberOfSales=sizeof($invoiceCardsDetail);
	//calculate sales Total
	if($invoiceCardsDetail){
		$taxes=unserialize($invoiceCardsDetail[0]['applied_taxes_inv']);
		$discountPercentage+=$invoiceCardsDetail[0]['discount_prcg_inv'];
		$otherDiscountPercentage+=$invoiceCardsDetail[0]['other_dscnt_inv'];
		foreach($invoiceCardsDetail as &$icd){
			$icd['name'] .= " / ".$icd['number_inv'];
			$salesTotal+=$icd['total_sal'];
			unset($icd['discount_prcg_inv']);
			unset($icd['other_dscnt_inv']);
			unset($icd['applied_taxes_inv']);
			unset($icd['number_inv']);
		}
	}
	//calculate discounts and subtotal
	$discount=$salesTotal*($discountPercentage/100);
	$otherDiscount=$salesTotal*($otherDiscountPercentage/100);
	$subtotal=$salesTotal-($discount+$otherDiscount);
	//calculate taxes
	if($taxes){
		foreach($taxes as $tx){
		$taxesPercentage+=$tx['tax_percentage'];
		}
		$numberOfTaxes=sizeof($taxes);
		switch($numberOfTaxes){
			case 1:
				$maxSales =5;
				break;
			case 2:
				$maxSales =4;
				break;
			case 3:
				$maxSales =3;
				break;
			case 4:
				$maxSales =2;
				break;
			case 5:
				$maxSales =1;
				break;
			default:
				$maxSales =0;
				break;
		}
	}
	$taxesTotal=$subtotal*($taxesPercentage/100);
	$invoiceTotal=$subtotal+$taxesTotal;
	//converting total to text.
	$numalet= new CNumeroaLetra();
	$numalet->setNumero($invoiceTotal);
	$numalet->setMayusculas(0);
	$numalet->setGenero(1);
	$numalet->setMoneda("con");
	$numalet->setPrefijo("");
	$numalet->setSufijo(utf8_decode("dólares americanos"));
	$totalLetras = $numalet->letra();
	$totalInvoiceText="Son ".$totalLetras;

	//HEADER
	$headerDesc=array();
	$invoiceTo=($invoiceData['serial_cus'])?$customer->getFirstname_cus().' '.$customer->getLastname_cus():$dealer->getName_dea();
	$documentTo=($invoiceData['serial_cus'])?$customer->getDocument_cus():$dealer->getId_dea();
	$descLine1=array('col1'=>'<b>Facturar a:</b>',
					 'col2'=>$invoiceTo,
					 'col3'=>'<b>RUC/CI:</b>',
					 'col4'=>$documentTo);
	array_push($headerDesc,$descLine1);
	$phoneTo=($invoiceData['serial_cus'])?$customer->getPhone1_cus():$dealer->getPhone1_dea();
	$descLine2=array('col1'=>'<b>Fecha:</b>',
					 'col2'=>$invoiceData['date_inv'],
					 'col3'=>'<b>'.utf8_decode('Teléfono:').'</b>',
					 'col4'=>$phoneTo);
	array_push($headerDesc,$descLine2);
	$addressTo=($invoiceData['serial_cus'])?$customer->getAddress_cus():$dealer->getAddress_dea();
	$descLine3=array('col1'=>'<b>'.utf8_decode('Dirección:').'</b>',
					 'col2'=>$addressTo,
					 'col3'=>'<b>Vence:</b>',
					 'col4'=>$invoiceData['due_date_inv']);
	array_push($headerDesc,$descLine3);
	$descLine4=array('col1'=>'<b>'.utf8_decode('País:').'</b>',
					 'col2'=>$country->getName_cou(),
					 'col3'=>'<b>Ciudad:</b>',
					 'col4'=>$city->getName_cit());
	array_push($headerDesc,$descLine4);
		$pdf = new Cezpdf('A5','landscape');
		$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
		$pdf -> ezSetMargins(80,90,60,50);
	//END HEADERS
		$pdf->ezSetDy(-30);
		$pdf->ezTable($headerDesc,
					  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
					  '',
					  array('showHeadings'=>0,
							'shaded'=>0,
							'showLines'=>0,
							'xPos'=>'center',
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'col1'=>array('justification'=>'left','width'=>70),
								'col2'=>array('justification'=>'left','width'=>200),
								'col3'=>array('justification'=>'left','width'=>80),
								'col4'=>array('justification'=>'left','width'=>150))));
		$pdf->ezSetDy(-10);

	//values table
	$invoiceTable=array();
	if($numberOfSales>$maxSales){
	$tableLine1=array('card_number_sal'=>'',
					 'name'=>utf8_decode('Servicio de asistencia en viaje según detalle adjunto.'),
					 'name_pbl'=>'',
					 'total_sal'=>'');
	array_push($invoiceTable,$tableLine1);
	}else{
		$invoiceTable=$invoiceCardsDetail;
	}
	$pdf->ezTable($invoiceTable,
					  array('card_number_sal'=>'<b>Tarjeta No</b>','name'=>'<b>Nombre</b>','name_pbl'=>'<b>Tipo Tarjeta</b>','total_sal'=>'<b>Valor</b>'),
					  '',
					  array('showHeadings'=>1,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'card_number_sal'=>array('justification'=>'center','width'=>70),
								'name'=>array('justification'=>'left','width'=>220),
								'name_pbl'=>array('justification'=>'center','width'=>130),
								'total_sal'=>array('justification'=>'right','width'=>80))));
	//giving format for numbers
	$salesTotal=number_format($salesTotal, 2, '.', '');
	$discount=number_format($discount, 2, '.', '');
	$otherDiscount=number_format($otherDiscount, 2, '.', '');
	$subtotal=number_format($subtotal, 2, '.', '');
	$taxesTotal=number_format($taxesTotal, 2, '.', '');
	$invoiceTotal=number_format($invoiceTotal, 2, '.', '');
	$discountPercentage=number_format($discountPercentage, 2, '.', '');
	$otherDiscountPercentage=number_format($otherDiscountPercentage, 2, '.', '');
	$taxesPercentage=number_format($taxesPercentage, 2, '.', '');

	$invoiceValues=array();
	$valuesLine1=array('col1'=>'<b>Suman:</b>',
					 'col2'=>$salesTotal);
	array_push($invoiceValues,$valuesLine1);
	$valuesLine2=array('col1'=>'<b>Descuento('.$discountPercentage.'%):</b>',
					 'col2'=>$discount);
	array_push($invoiceValues,$valuesLine2);
	if($otherDiscount>0){
		$valuesLine3=array('col1'=>'<b>Otro Descuento('.$otherDiscountPercentage.'%):</b>',
					 'col2'=>$otherDiscount);
		array_push($invoiceValues,$valuesLine3);
	}
	$valuesLine4=array('col1'=>'<b>Subtotal:</b>',
					 'col2'=>$subtotal);
	array_push($invoiceValues,$valuesLine4);
	//check if there are taxes and if they are more than 2 taxes applied
	if($taxes){
		if(sizeof($taxes)>$maxTaxes){
			$valuesLine5=array('col1'=>'<b>Impuestos('.$taxesPercentage.'%):</b>',
							 'col2'=>$taxesTotal);
			array_push($invoiceValues,$valuesLine5);
			$taxesText='';
			foreach($taxes as $t){
				$taxesText.="{$t['tax_name']}:{$t['tax_percentage']}%, ";
			}
			$taxesText=substr($taxesText,0,strlen($taxesText)-2);
		}else{
			foreach($taxes as $t){
				$$t['tax_name']=array('col1'=>'<b>'.$t['tax_name'].'('.$t['tax_percentage'].'%):</b>',
							 'col2'=>number_format(($subtotal*($t['tax_percentage']/100)),2, '.',''));
				array_push($invoiceValues,$$t['tax_name']);
			}
		}
	}else{
		$valuesLine5=array('col1'=>'<b>Impuestos(0%):</b>',
							 'col2'=>'0.00');
			array_push($invoiceValues,$valuesLine5);
	}
	//Debug::print_r($taxes);
	$valuesLine6=array('col1'=>'<b>Valor Total:</b>',
					 'col2'=>$invoiceTotal);
	array_push($invoiceValues,$valuesLine6);

	$pdf->ezTable($invoiceValues,
					  array('col1'=>'','col2'=>''),
					  '',
					  array('showHeadings'=>0,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>342.5,
							'xOrientation'=>'right',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'col1'=>array('justification'=>'right','width'=>130),
								'col2'=>array('justification'=>'right','width'=>80))));
	$pdf->ezSetDy(-10);
		$pdf->ezText($totalInvoiceText, '', array('justification' =>'left'));
	if($taxesText){
		$pdf->ezSetDy(-10);
		$pdf->ezText('Impuestos Aplicados: '.$taxesText, '', array('justification' =>'left'));
	}
	if($numberOfSales>$maxSales){
	//invoice detail
		$pdf->ezNewPage();
		$pdf->ezStartPageNumbers(545,40,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
		$pdf->ezTable($invoiceCardsDetail,
					  array('card_number_sal'=>'<b>Tarjeta No</b>','name'=>'<b>Nombre</b>','name_pbl'=>'<b>Tipo Tarjeta</b>','total_sal'=>'<b>Valor</b>'),
					  '<b>DETALLE FACTURA No.'.$invoiceData['number_inv'].'</b>',
					  array('fontSize' => 8,
							'titleFontSize' => 8,
						  'cols'=>array(
								'card_number_sal'=>array('justification'=>'center','width'=>70),
								'name'=>array('justification'=>'left','width'=>200),
								'name_pbl'=>array('justification'=>'center','width'=>150),
								'total_sal'=>array('justification'=>'center','width'=>80)),
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'xPos'=>'center',
							'xOrientation'=>'center'));
		$total=array();
		$valuesTotal=array('col1'=>'<b>Suman:</b>',
					 'col2'=>$salesTotal);
		array_push($total,$valuesTotal);

		$pdf->ezTable($total,
					  array('col1'=>'','col2'=>''),
					  '',
					  array('showHeadings'=>0,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>322.5,
							'xOrientation'=>'right',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'col1'=>array('justification'=>'right','width'=>150),
								'col2'=>array('justification'=>'center','width'=>80))));
	}
		$invoice->setPrinted_inv('YES');
		$invoice->update();
		
		ERP_logActivity('update', $invoice);//ERP ACTIVITY
		//$pdf->ezStopPageNumbers();
		$pdf->ezStream();
}else{
	http_redirect('main/printInvoice/1');
}
?>
