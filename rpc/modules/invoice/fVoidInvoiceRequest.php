<?php
/*
File: fVoidInvoiceRequest.php
Author: Santiago Ben�tez
Creation Date: 19/02/2010 12:06
Last Modified: 19/02/2010
Modified By: Santiago Ben�tez
*/
Request::setInteger('0:error');
$inv=new Invoice($db);

$mbc=new ManagerbyCountry($db, $_POST['selManager']);
$mbc->getData();
$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $mbc->getSerial_man(), $_POST['selDocument'], 'INVOICE');

$serial_inv=$inv->getInvoice($_POST['txtInvoiceNumber'], $serial_dbm);
$serial_inv=$serial_inv['serial_inv'];

//zones
$zone= new  Zone($db);
$zoneList=$zone->getZones();

//invoice
$invoice=new Invoice($db,$serial_inv);
if($invoice->getData()){
    $invoiceData['serial_inv']=$invoice->getSerial_inv();
    $invoiceData['serial_cus']=$invoice->getSerial_cus();
    $invoiceData['serial_pay']=$invoice->getSerial_pay();
    $invoiceData['serial_dea']=$invoice->getSerial_dea();
    $invoiceData['serial_dbm']=$invoice->getSerial_dbm();
    $invoiceData['date_inv']=$invoice->getDate_inv();
    $invoiceData['due_date_inv']=$invoice->getDue_date_inv();
    $invoiceData['number_inv']=$invoice->getNumber_inv();
    $invoiceData['discount_prcg_inv']=$invoice->getDiscount_prcg_inv();
    $invoiceData['other_dscnt_inv']=$invoice->getOther_dscnt_inv();
    $invoiceData['comision_prcg_inv']=$invoice->getComision_prcg_inv();
    $invoiceData['comments_inv']=$invoice->getComments_inv();
    $invoiceData['printed_inv']=$invoice->getPrinted_inv();
    $invoiceData['status_inv']=$invoice->getStatus_inv();
    $invoiceData['subtotal_inv']=$invoice->getSubtotal_inv();
    $invoiceData['total_inv']=$invoice->getTotal_inv();

    if($invoiceData['serial_cus']){
        $customer=new Customer($db, $invoiceData['serial_cus']);
        $customer->getData();
        $extraData=get_object_vars($customer);

        //CITY
        $serial_cit=$customer->getSerial_cit();
        $city= new City($db,$serial_cit);
        $city->getData();
        $extraData['name_cit']=$city->getName_cit();

        //COUNTRY
        $serial_cou=$extraData['auxData']['serial_cou'];
        $country=new Country($db,$serial_cou);
        $country->getData();
        $extraData['name_cou']=$country->getName_cou();

    }else{
        $dealer=new Dealer($db, $invoiceData['serial_dea']);
        $dealer->getData();
        $extraData=get_object_vars($dealer);
    }
}

//sales
$sale=new Sales($db);
$salesList=$sale->getSalesByInvoice($serial_inv);
$smarty->register('error,zoneList,invoiceData,extraData,salesList,global_salesStatus');
$smarty->display();
?>