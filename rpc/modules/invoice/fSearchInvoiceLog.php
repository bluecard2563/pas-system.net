<?php
/*
File: fSearchInvoiceLog.php
Author: Santiago Ben�tez
Creation Date: 09/04/2010 15:06
Last Modified: 09/04/2010 15:06
Modified By: Santiago Ben�tez
*/

Request::setInteger('0:error');
Request::setString('1:serial_cn');

if($serial_cn){
	$number_cn=CreditNote::getCreditNoteNumber($db, $serial_cn);
}

$zone= new  Zone($db);
$zoneList=$zone->getInvoiceLogZones();


$smarty->register('error,zoneList,number_cn,serial_cn');
$smarty->display();
?>