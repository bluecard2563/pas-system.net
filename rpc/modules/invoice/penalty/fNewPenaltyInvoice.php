<?php
/*
 * File: fNewPenaltyInvoice.php
 * Author: Patricio Astudillo
 * Creation Date: 18/06/2010, 01:53:10 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 18/06/2010, 01:53:10 PM
 */

Request::setString('0:error');
Request::setString('1:serial_cn');

if($serial_cn){
	$number_cn=CreditNote::getCreditNoteNumber($db, $serial_cn);
}

$zone=new Zone($db);
$zoneList=$zone->getZones();
unset($_REQUEST);

$smarty->register('zoneList,error,serial_cn,number_cn');
$smarty->display();
?>
