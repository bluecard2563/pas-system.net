<?php
/*
 * File: pNewPenaltyInvoice.php
 * Author: Patricio Astudillo
 * Creation Date: 18/06/2010, 03:27:57 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 18/06/2010, 03:27:57 PM
 */

$txtCardNumber=$_POST['hdnCardNumber'];
$omit_international_fee=$_POST['chkInternationFee'];

if($_POST['txtPenalty']){
	$sale=new Sales($db);
	$sale->setCardNumber_sal($txtCardNumber);

	if($sale->getDataByCardNumber()){
		$invoice=new Invoice($db, $sale->getSerial_inv());

		if($invoice->getData()){
			/*CREATE CREDIT NOTE*/
				/*BILLING INFORMATION*/
				$counter=new Counter($db, $sale->getSerial_cnt());
				$counter->getData();
				$aux=$counter->getAux_cnt();
				/*END INFO*/

				/*GET APPLIED DISCOUNTS*/
				$discounts=number_format(($invoice->getDiscount_prcg_inv()+$invoice->getOther_dscnt_inv())/100, '2');
				/*END DISCOUNTS*/

				/*GET APPLIED TAXES*/
				$taxes=$invoice->getApplied_taxes_inv();
				$taxes=unserialize($taxes);
				$taxesValue=0;
				if(is_array($taxes)){
					foreach($taxes as $t){
						$taxesValue+=$t['tax_percentage'];
					}
				}
				$taxesValue=number_format($taxesValue/100, '2');
				/*END TAXES*/

				//Amount paid by the credit note.
				$paidAmount=$sale->getTotal_sal();
				$paidAmount-=($paidAmount*$discounts);
				$paidAmount+=($paidAmount*$taxesValue);
				$paidAmount-=number_format($_POST['txtPenalty'], '2');

				$mbc=new ManagerbyCountry($db, $aux['serial_mbc']);
				$mbc->getData();

				$misc['type']='PENALTY';
				$misc['serial']=$sale->getSerial_sal();
				$misc['db']=$db;
				$misc['serial_man']=$mbc->getSerial_man();
				$misc['amount']=$paidAmount;
				$misc['penalty']=number_format($_POST['txtPenalty'], '2');
				$cNote=GlobalFunctions::generateCreditNote($misc);
			/*END CREDIT NOTE*/

			if($cNote['error']=='success'){
				/*CREATING THE PAYMENT*/
				$payment=new Payment($db);
				$payment->setStatus_pay('PARTIAL');
				$payment->setTotal_to_pay_pay($invoice->getTotal_inv());
				$payment->setTotal_payed_pay($paidAmount);
				$payment->setExcess_amount_available_pay('0');
				$serial_pay=$payment->insert();
				
				ERP_logActivity('new', $payment);//ERP ACTIVITY
				/*END PAYMENT*/

				/*CREATING THE PAYMENT DETAIL*/
				$paymentDetail=new PaymentDetail($db);
				$paymentDetail->setSerial_pay($serial_pay);
				$paymentDetail->setSerial_usr($_SESSION['serial_usr']);
				$paymentDetail->setSerial_cn($cNote['id']);
				$paymentDetail->setAmount_pyf($paidAmount);
				$paymentDetail->setType_pyf('CREDIT_NOTE');
				$paymentDetail->setComments_pyf('N.C. por Penalidad');
				$paymentDetail->setDate_pyf(date('Y-m-d'));
				$paymentDetail->setDocumentNumber_pyf($cNote['number']);
				/*END PAYMENT DETAIL*/

				if($paymentDetail->insert()){
					/*UPDATING CREDIT NOTE INFO*/
					$cn=new CreditNote($db, $cNote['id']);
					$cn->getData();
					$cn->setPayment_status_cn('PAID');
					$cn->updatePayment_status_cn();
					/*END UPDATING*/

					/*UPDATING THE SALES AS VOID*/
					$sale->setStatus_sal('VOID');
					if($sale->updateStatus()){
						/* INTERNATIONAL FEE REFUND */
						if($omit_international_fee){
							GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal(), TRUE);
						}else{
							GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal(), FALSE);
						}
						/* END INTERNATIONAL FEE */

						$error=1;
					}else{
						$error=4;
					}
					/*END UPDATING*/

					/*UPDATING INVOICE*/
					$invoice->setSerial_pay($serial_pay);
					$invoice->update();
					
					ERP_logActivity('update', $invoice);//ERP ACTIVITY
					/*END INVOICE*/
				}else{
					$error = 5;//ERROR TO INSERT PAYMENT DETAIL
				}
			}else{
				switch($cNote['error']){
					case 'insert_error':
						$error=6;
						break;
					case 'update_error':
						$error=7;
						break;
					case 'cNote_error':
						$error=8;
						break;
					case 'no_document':
						$error=9;
						break;
				}
			}
		}else{//Invoice info didn't load.
			$error=3;
		}
	}else{ //Card doesn't exist.
		$error=2;
	}
	http_redirect('modules/invoice/penalty/fNewPenaltyInvoice/'.$error.'/'.$cNote['id']);
}else{
	http_redirect('modules/invoice/penalty/fNewPenaltyInvoice');
}
?>