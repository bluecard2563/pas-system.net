<?php
/*
File: pNewInvoice.php
Author: Santiago Benitez
Creation Date:22/03/2010
Modified By: Santiago Benitez
Last Modified: 22/03/2010
*/

$error=1;
$flag=0;
$invoice=new Invoice($db);
$customer=new Customer($db);
$mbc=new ManagerbyCountry($db, $_POST['hdnSerialMbc']);
$mbc->getData();

//customer
if($_POST['bill_to_radio']=='CUSTOMER'){
    $customer->setDocument_cus($_POST['txtCustomerDocument']); 
    $customer->setSerial_cit($_POST['selCity']);      
    $customer->setFirstname_cus($_POST['txtFirstName']);
    $customer->setLastname_cus($_POST['txtLastName']);    
    $customer->setPhone1_cus($_POST['txtPhone1']);
    $customer->setPhone2_cus($_POST['txtPhone2']);
    $customer->setAddress_cus($_POST['txtAddress']);
    if($_POST['selType']){
        $customer->setType_cus($_POST['selType']);
    }else{
        $customer->setType_cus($_POST['hdnTypeDbc']);
    }
    $customer->setStatus_cus('ACTIVE');
    //when a new customer is going to be inserted
    if($_POST['hdnCustomerAction']=='INSERT'){
        $flag=1;
        $customer->setVip_cus(0);
        $password=GlobalFunctions::generatePassword(6, 8, false);
        $customer->setPassword_cus(md5($password));
        $serial_cus=$customer->insert();
        if(!$serial_cus){
            $error=2;
        }else{
        	ERP_logActivity('new', $customer);//ERP ACTIVITY
        	
            /*Generating the info for sending the e-mail*/
            $misc['textForEmail']='Estimado(a) '.$_POST['txtFirstName'].' '.$_POST['txtLastName'].'<br><br> Si usted desea hacer uso de nuestro servicio de ventas
                                   on-line, ingrese a <a href="http://www.planet-assist.com/">www.planet-assist.com</a> y reg&iacute;strese con estos datos: ';
			$misc['customer']=$_POST['txtFirstName'].' '.$_POST['txtLastName'];
            $misc['username']=$customer->getDocument_cus();
            $misc['pswd']=$password;
            $misc['email']=$customer->getEmail_cus();
            /*END E-mail Info*/
			//Debug::print_r($misc);
        }
    }else{
        //CUSTOMER IS NOT UPDATED
    }
}

//credit day
$creditDay=new CreditDay($db,$_POST['hdnSerialCdd']);
if(!$creditDay->getData()){
    $error=5;
}

//document by country
if($error==1){
	$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $_POST['hdnSerialMan'], $_POST['hdnTypeDbc'], 'INVOICE');
    
    if(!$serial_dbm){
        $error=6;
    }
}

//invoice
if($error==1){
    $days=$creditDay->getDays_cdd();
    $invoice->setComments_inv($_POST['txtObservations']);
    $invoice->setDate_inv($_POST['txtDate']);
    if($_POST['hdnTypePercentage']=='COMISSION'){ //if percentage is aplied to comission
        $invoice->setComision_prcg_inv($_POST['hdnComissionValue']);
    }else{ //if percentage is aplied to discount
		//check if the invoice is for customer, in this case save as comission
		 if($_POST['bill_to_radio']=='CUSTOMER'){
			  $invoice->setComision_prcg_inv($_POST['hdnComissionValue']);
		 }else{
			  $invoice->setDiscount_prcg_inv($_POST['txtDiscount']);
		 }
    }
    $date=$_POST['txtDate'];
    $cd = strtotime(substr($date,3,3).substr($date,0,3).substr($date,6,4));
    $dueDate = date('d/m/Y', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
    $invoice->setDue_date_inv($dueDate);

	//document number
	if($mbc->getInvoice_number_mbc()=='AUTOMATIC'){
		$nextAvailableNumber=DocumentByManager::retrieveNextDocumentNumber($db, $_POST['hdnSerialMan'], $_POST['hdnTypeDbc'], 'INVOICE', TRUE);
		
		if(!$nextAvailableNumber){
			$error=8;
		}
	}else{
		$nextAvailableNumber=$_POST['txtInvoiceNumber'];
	}	
	
    $invoice->setNumber_inv(intval($nextAvailableNumber));
    $invoice->setOther_dscnt_inv($_POST['txtOtherDiscount']);
    if($_POST['bill_to_radio']=='DEALER'){
        $invoice->setSerial_dea($_POST['hdnSerialDea']);
    }else{
        if($_POST['hdnSerialCus']){
            $invoice->setSerial_cus($_POST['hdnSerialCus']);
        }else{
            $invoice->setSerial_cus($serial_cus);
        }
    }
    $invoice->setSerial_dbm($serial_dbm);
    $invoice->setSubtotal_inv($_POST['txtSubTotal']);
    $invoice->setTotal_inv($_POST['txtTotal']);

    $namesTax=$_POST['hdnNameTax'];
    $percentagesTax=$_POST['hdnPercentageTax'];
	if($namesTax){
		foreach ($namesTax as $k=>$n){
			$misc[$k]['tax_name']=$n;
			$misc[$k]['tax_percentage']=$percentagesTax[$k];
		}
		$invoice->setApplied_taxes_inv(serialize($misc));
	}
    
    $serial_inv=$invoice->insert();
    if(!$serial_inv){
        $error=7;
    }else{
    	ERP_logActivity('new', $invoice);//ERP ACTIVITY
    }
}

//update sales setting serial invoice 
if($error==1){
    $salesIds=$_POST['hdnSaleId'];
    foreach($salesIds as $s){
      $sale=New Sales($db,$s);
      if(!$sale->updateInvoice($serial_inv)){
          $error=9;
          break;
      }
    }
}

//sending mail to customer
if($error==1 && $flag==1){
	//echo $_POST['txtFirstName'].' '.$_POST['txtLastName'];
	//Debug::print_r($misc);
	//die();
	if($_POST['bill_to_radio']== 'CUSTOMER' && $_POST['txtMail']!=''){
		if(!GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
			$error=10;
		}
	}
}
http_redirect('modules/invoice/fChooseSales/'.$error.'/'.$serial_inv);
?>