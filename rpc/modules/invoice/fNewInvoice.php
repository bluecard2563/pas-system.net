<?php
/*
File:
Author: Nicolas Flores
Creation Date:08/03/2010
Modified By: Santiago Benitez
Last Modified: 18/03/2010
*/

Request::setInteger('selBranch');
//currency

//managerData
$serial_mbc=$_POST['selManager'];
$manager=new ManagerbyCountry($db,$serial_mbc);
$manager->getData();
$managerData['serial_mbc']=$serial_mbc;
$managerData['serial_cou']=$manager->getSerial_cou();
$managerData['invoice_number_mbc']=$manager->getInvoice_number_mbc();
$managerData['serial_man']=$manager->getSerial_man();
//get sales ids
$salesIds=$_POST['chkSales'];
$salesIdString= implode(",", $salesIds);
//get branch info
$branch=new Dealer($db,$selBranch);
$branch->getData();
$branchData=get_object_vars($branch);
unset($branchData['db']);
//get today date
$branchData['today']=date('d/m/Y');
//get sales total
$sales=new Sales($db);
$salesT=$sales->getSalesTotal($salesIdString);
$salesTotal=number_format($salesT, 2, '.', '');
//get the cost of all this sales
$salesCost=number_format($sales->getCostTotal($salesIdString), 2, '.', '');
if($branchData['type_percentage_dea']=='DISCOUNT'){
    $totalAfterD=$salesT-($salesT*$branchData['percentage_dea']/100);
}else{
   $totalAfterD=$salesT;
}
$totalAfterDiscount=number_format($totalAfterD,2, '.', '');

//get data form one of the sales to get the product info.
$sale=new Sales($db,$salesIds[0]);
$sale->getData();
if(sizeof($_POST['chkSales'])==1){
	$observations=$sale->getObservations_sal();
}
//create a product to get the taxes to be applied
$productByDealer=new ProductByDealer($db,$sale->getSerial_pbd());
$productByDealer->getData();
$taxes=$productByDealer->getTaxes();
$taxesToA=0;
if(is_array($taxes)){
    foreach ($taxes as $t){
        $taxesToA+=$t['percentage_tax'];
    }
}

$taxesToApply=number_format($taxesToA, 2, '.', '');
//apply the taxes
$total=number_format($totalAfterD*(1+($taxesToA/100)),2, '.', '');
//get data for who the invoice goes for
//get data for customer in case there is an invoice for only one sale.
//for customer case:
if(sizeof($salesIds)==1&& $sale->getSerial_cus()){
    $customer=new Customer($db,$sale->getSerial_cus());
    $customer->getData();
    $customerData=get_object_vars($customer);
    $typesList=$customer->getAllTypes();
    unset($customerData['db']);
    //retrieves a citylist by country
    $city=new City($db);
    $cityList=$city->getCitiesByCountry($customerData['auxData']['serial_cou']);
    //questions' answers
    $answer=new Answer($db);
    $answer->setSerial_cus($sale->getSerial_cus());
    $answerList=$answer->getAnswers();
}
//retrieves a countrylist
$country=new Country($db);
$countryList=$country->getCountry();
//retrieves a typelist where customer types are displayed
$customer=new Customer($db);
$typesList=$customer->getAllTypes();

/*QUESTIONS*/
$language=new Language($db);
$sessionLanguage=$language->getSerialByCode($_SESSION['language']);
$question=new Question($db);
$maxSerial=$question->getMaxSerial($sessionLanguage);
$questionList = Question::getActiveQuestions($db, $sessionLanguage,$sale->getSerial_cus());//list of active questions shown in the form
/*END QUESTIONS*/

/*DOCUMENT NUMBER*/
//$number=new Documentnumber($db);
//$number=getNumber
/*END DOCUMENT NUMBER*/

/*PARAMETERS*/
$parameter=new Parameter($db,1); //-->
$parameter->getData();
$parameterValue=$parameter->getValue_par(); //value of the paramater used to display or not the questions
$parameterCondition=$parameter->getCondition_par(); //condition of the paramater used to display or not the questions
/*END PARAMETERS*/

//for dealer case ,there is being used the branch where the sales where done
$dealer=new Dealer($db,$selBranch);
$dealer->getData();
$dealerData=get_object_vars($dealer);
unset($dealerData['db']);
$smarty->register('branchData,salesTotal,totalAfterDiscount,total,taxes,salesCost,taxesToApply,customerData,typesList,countryList,cityList,questionList,parameterValue,parameterCondition,maxSerial,answerList,managerData,salesIds,observations');
$smarty->display();
?>
