<?php 
/*
File: pNewRestriction.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 14:21 pm
Last Modified:
Modified By:
*/

$language=new Language($db);
$serial_lang=$language->getSerialByCode($_SESSION['language']);

$restriction=new Restriction($db);
$serial_rst=$restriction->insert();

if($serial_rst){
    $rbl=new RestrictionByLanguage($db);
    $rbl->setSerial_rst($serial_rst);
    $rbl->setSerial_lang($serial_lang);
    $rbl->setDescription_rbl($_POST['txtDescription']);

    if($rbl->insert()){
        http_redirect('modules/product/restrictions/fNewRestriction/1');
    }else{
        http_redirect('modules/product/restrictions/fNewRestriction/3');
    }
}else{
    http_redirect('modules/product/restrictions/fNewRestriction/2');
}
?>