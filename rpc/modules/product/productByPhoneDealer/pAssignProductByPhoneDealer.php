<?php
/*
File: pAssignProductByPhoneDealer
Author: David Bergmann
Creation Date: 20/07/2010
Last Modified:
Modified By:
*/
$dealer = new Dealer($db, $_POST['hdnSerial_dea']);
$dealer->getData();
$serial_dea = $dealer->getDea_serial_dea();

ProductByDealer::deactivateProductsByDealer($db, $serial_dea);

if(is_array($_POST['chkProduct'])) {
	$productByDealer = new ProductByDealer($db);
	$productByDealer->setSerial_dea($serial_dea);
	$productByDealer->setStatus_pbd('ACTIVE');
	foreach ($_POST['chkProduct'] as $p) {
		$productByDealer->setSerial_pxc($p);
		if($productByDealer->productByDealerExists()) {
			if(!$productByDealer->update()) {
				http_redirect('modules/product/productByPhoneDealer/fAssignProductByPhoneDealer/2');
			}
		} else {
			if(!$productByDealer->insert()) {
				http_redirect('modules/product/productByPhoneDealer/fAssignProductByPhoneDealer/2');
			}
		}
		
	}
	http_redirect('modules/product/productByPhoneDealer/fAssignProductByPhoneDealer/1');
} else {
	http_redirect('modules/product/productByPhoneDealer/fAssignProductByPhoneDealer/3');
}
?>