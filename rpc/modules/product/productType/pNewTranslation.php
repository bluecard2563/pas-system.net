<?php 
/*
File: pNewTranslation.php
Author: Patricio Astudillo M.
Creation Date: 26/01/2010 16:21 pm
Last Modified:
Modified By:
*/

$pTypeByLanguage=new PtypebyLanguage($db);


$pTypeByLanguage->setSerial_tpp($_POST['serial_tpp']);
$pTypeByLanguage->setSerial_lang($_POST['selLanguage']);
$pTypeByLanguage->setName_ptl($_POST['txtName_tpp']);
$pTypeByLanguage->setDescription_ptl($_POST['txtDescription_tpp']);




if($pTypeByLanguage->insert()){
    http_redirect('modules/product/productType/fSearchProductTypeTranslation/1');
}else{
    http_redirect('modules/product/productType/fSearchProductTypeTranslation/5');
}
?>
