<?php //session_start();

Request::setInteger('0:message');

$code_lang = $_SESSION['language'];

$language = new Language($db);
$languages = $language->getLanguages();

$productType = new ProductType($db);
$productTypeList = $productType->getProductTypes($code_lang);


$smarty -> register('productTypeList,message,code_lang,languages');
$smarty -> display();
?>