<?php
/*
File: fPricesByCountry.php
Author: Edwin Salvador
Creation Date: 08/03/2010
Last Modified:
Modified By:
*/
Request::setInteger('selCountry');
Request::setInteger('selProduct');
$serial_pro = $selProduct;
$serial_cou = $selCountry;
if($serial_pro && $serial_cou){
    $pbc = new ProductByCountry($db);
    $pbc->setSerial_cou($serial_cou);
    $pbc->setSerial_pro($serial_pro);
    $serial_pxc = $pbc->productByCountryExists();

    $product = new Product($db,$serial_pro);
    $data = $product->getInfoProduct($_SESSION['language']);
    $data = $data[0];
    $pbpbc = new PriceByProductByCountry($db);
    
    $ref_prices = $pbpbc->getPricesByProductByCountry($serial_pro, '1');
    if(is_array($ref_prices)){
        $data['ref_prices'] = $ref_prices;
    }

    $prices = $pbpbc->getPricesByProductByCountry($serial_pro, $serial_cou);
    $data['prices'] = array('');
    if(is_array($prices)){
        $data['prices'] = $prices;
    }
    $count = sizeof($data['prices'])-1;
    
    $country = new Country($db, $serial_cou);
    $country->getData();
    $name_cou = $country->getName_cou();
}else{
    http_redirect('modules/product/pricesByCountry/fSearchPricesByCountry/4');
}

$smarty->register('data,name_cou,serial_cou,serial_pxc,count');
$smarty->display();

?>
