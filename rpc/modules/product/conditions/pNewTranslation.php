<?php 
/*
File: pNewTranslation.php
Author: Patricio Astudillo M.
Creation Date: 25/01/2010 16:21 pm
Last Modified:
Modified By:
*/

$cbl=new ConditionByLanguage($db);
$cbl->setSerial_con($_POST['hdnConditionID']);
$cbl->setSerial_lang($_POST['selLanguage']);
$cbl->setDescription_cbl($_POST['txtDescription']);

if($cbl->insert()){
   http_redirect('modules/product/conditions/fSearchCondition/1');
}else{
   http_redirect('modules/product/conditions/fSearchCondition/5');
}

?>