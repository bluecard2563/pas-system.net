<?php session_start();
    $code_lang=$_SESSION['language'];
	Request::setInteger('0:message');
	$product = new Product($db);
	$productList = $product->getProducts($code_lang);
	//die(Debug::print_r($productList));
	$language = new Language($db);
	$languages = $language->getLanguages();

	$smarty -> register('productList,message,code_lang,languages');
	$smarty -> display();
?>
