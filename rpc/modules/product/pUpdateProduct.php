<?php session_start();
/*
File: pUpdateProduct
Author: Miguel Ponce
Creation date: 28/12/2009
Last modified: 30/07/2010
Modified By: David Bergmann
*/

	$code_lang=$_SESSION['language']='es';
	$product = new Product($db);
	$priceByProductByCountry = new PriceByProductByCountry($db);
	$productbyCountry = new ProductByCountry($db);
	$benefitsByProduct = new BenefitsByProduct($db);
	$conditionsByProduct = new ConditionsByProduct($db);
	$language = new Language($db);
	$serial_lang = $language -> getSerialByCode($code_lang);
	$productbyLanguage=new ProductbyLanguage($db);
	$serial_pro=$_POST['serial_pro'];
	$serial_tpp=$_POST['serial_tpp'];
	$product->setSerial_pro($serial_pro);
	$product->setSerial_tpp($serial_tpp);
	$product->setFlights_pro($_POST['flights_pro']);

	$product->setSpouse_pro('NO');
	$product->setHas_comision_pro('NO');
	$product->setSchengen_pro('NO');
	$product->setIn_web_pro('NO');
	$product->setRelative_pro('NO');
	$product->setThird_party_register_pro('NO');
	$product->setMasive_pro('NO');
	$product->setEmission_country_pro('NO');
	$product->setResidence_country_pro('NO');
	$product->setSenior_pro('NO');
	$product->setDestination_restricted_pro('NO');
	$product->setPrice_by_range_pro('NO');
	$product->setCalculate_pro('NO');
	$product->setStatus_pro('INACTIVE');
	$product->setPrice_by_day_pro('NO');
	$product->setGenerate_number_pro('NO');
	$product->setHas_services_pro('NO');
	$product->setLimit_pro('NO');
	$product->setShow_price_pro('NO');
	$product->setExtras_restricted_to_pro('');

	if($_POST['max_extras_pro']!=''){
	$product->setMax_extras_pro($_POST['max_extras_pro']);
	}

	if($_POST['children_pro']!=''){
	$product->setChildren_pro($_POST['children_pro']);
	}

	if($_POST['adults_pro']!=''){
	$product->setAdults_pro($_POST['adults_pro']);
	}

	if(isset($_POST['spouse_pro'])){
	$product->setSpouse_pro($_POST['spouse_pro']);
	}
	if(isset($_POST['has_comision_pro'])){
	$product->setHas_comision_pro($_POST['has_comision_pro']);
	}
	if(isset($_POST['schengen_pro'])){
	$product->setSchengen_pro($_POST['schengen_pro']);
	}
	if(isset($_POST['in_web_pro'])){
	$product->setIn_web_pro($_POST['in_web_pro']);
	}
	if(isset($_POST['relative_pro'])){
	$product->setRelative_pro($_POST['relative_pro']);
	}
	if(isset($_POST['third_party_register_pro'])){
	$product->setThird_party_register_pro($_POST['third_party_register_pro']);
	}
	if(isset($_POST['masive_pro'])){
	$product->setMasive_pro($_POST['masive_pro']);
	}
	if(isset($_POST['emission_country_pro'])){
		$product->setEmission_country_pro($_POST['emission_country_pro']);
	}
	if(isset($_POST['residence_country_pro'])){
	$product->setResidence_country_pro($_POST['residence_country_pro']);
	}
	if(isset($_POST['senior_pro'])){
	$product->setSenior_pro($_POST['senior_pro']);
	}
	if(isset($_POST['destination_restricted_pro'])){
	$product->setDestination_restricted_pro($_POST['destination_restricted_pro']);
	}
	if(isset($_POST['price_by_range'])){
	$product->setPrice_by_range_pro($_POST['price_by_range']);
	}
	if(isset($_POST['calculate_pro'])){
	$product->setCalculate_pro($_POST['calculate_pro']);
	}
	if(isset($_POST['status_pro'])){
	$product->setStatus_pro($_POST['status_pro']);
	}
	if(isset($_POST['price_by_day_pro'])){
	$product->setPrice_by_day_pro($_POST['price_by_day_pro']);
	}
	if(isset($_POST['generate_number_pro'])){
	$product->setGenerate_number_pro($_POST['generate_number_pro']);
	}
	if(isset($_POST['has_services_pro'])){
	$product->setHas_services_pro($_POST['has_services_pro']);
	}
	if(isset($_POST['limit_pro'])){
	$product->setLimit_pro($_POST['limit_pro']);
	}
	if(isset($_POST['show_price_pro'])){
	$product->setShow_price_pro($_POST['show_price_pro']);
	}
	if(isset($_POST['selExtrasRestricted'])){
	$product->setExtras_restricted_to_pro($_POST['selExtrasRestricted']);
	}

	$error=1;
	$product->setImage_pro($_POST['hdnImage_pro']);

	if($_FILES['image_pro']['name'] && (($_FILES["image_pro"]["type"] == "image/gif") || ($_FILES["image_pro"]["type"] == "image/jpeg") || ($_FILES["image_pro"]["type"] == "image/pjpeg"))){
	   //$prefijo = substr(md5(uniqid(rand())),0,6);
		$image_pro = $_FILES["image_pro"]['name'];
		if(copy($_FILES['image_pro']['tmp_name'],DOCUMENT_ROOT.'img/products/'.$image_pro)){
			$product->setImage_pro($image_pro);
			$copioArchivo=1;
		}
	}

	if($_POST['hdnSold']) {
	   $statusUpdated=$product->updateStatus();
	}else{
	   $updated=$product->update();
	}

	if($updated){ //SUCCESFULL UPDATE
		//************************* UPDATE PRODUCT'S NAME TRANSLATION ******************
		$productbyLanguage -> setSerial_lang($serial_lang);
		$productbyLanguage -> setSerial_pro($serial_pro);
		$productbyLanguage -> setName_pbl($_POST['txtName_pro']);
		$productbyLanguage -> setDescription_pbl($_POST['txtDescription_pro']);
		$languageUpdated=$productbyLanguage -> update();

		//***************** UPDATE REFERENTIAL DATA FOR PRODUCT BY COUNTRY *************
		$serial_pxc=$productbyCountry->getProductByCountry($serial_pro);
		$productbyCountry -> setSerial_pxc($serial_pxc);
		$productbyCountry -> setSerial_pro($serial_pro);
		$productbyCountry -> setSerial_cou('1');
		if($_POST['relative_pro']){
			$productbyCountry -> setPercentage_extras_pxc($_POST['percentage_extras_pxc']);
			$productbyCountry -> setPercentage_children_pxc($_POST['percentage_children_pxc']);
		}
		if($_POST['spouse_pro']){
			$productbyCountry -> setPercentage_spouse_pxc($_POST['percentage_spouse_pxc']);
		}
		$productbyCountry -> setAditional_day_pxc($_POST['aditional_day_pxc']);
		$productbyCountry -> setStatus_pxc('ACTIVE');
		$productbyCountry -> update();

		//************************** UPDATE REFERENTIAL PRICES **************************
		if($_POST['price_by_product']){
			$priceByProductByCountry -> deleteByProduct($serial_pro);
			foreach($_POST['price_by_product'] as $key=>$data){
				$priceByProductByCountry->setSerial_pxc($serial_pxc);
				$priceByProductByCountry->setDuration_ppc($data['duration_ppc']);
				if(isset($_POST['price_by_range'])){
					$priceByProductByCountry->setMin_ppc($data['min_ppc']);
					$priceByProductByCountry->setMax_ppc($data['max_ppc']);
				}else{
					$priceByProductByCountry->setPrice_ppc($data['price_ppc']);
				}
				$priceByProductByCountry->setCost_ppc($data['cost_ppc']);
				$priceByProductByCountry->insert();
			}
		}

		if($languageUpdated){
			$benefitsByProduct -> setSerial_pro($serial_pro);
			$benefitsByProduct -> delete();
			if(is_array($_POST['benefit'])){
				foreach($_POST['benefit'] as $key => $benefitItem){
					if($benefitItem['status_bpt']=="on"){
						$benefitsByProduct -> setSerial_ben($key);
						$benefitsByProduct -> setSerial_rst($benefitItem['serial_rst']);
						$benefitsByProduct -> setSerial_con($benefitItem['serial_con']);
						$benefitsByProduct -> setPrice_bxp($benefitItem['price_bpt']);
						$benefitsByProduct -> setStatus_bxp('ACTIVE');
						$benefitsByProduct -> setRestriction_price_bxp($benefitItem['restriction_price_bpt']);
						$benefitsByProduct -> setSerial_cur(1);
						$benefitsByProduct -> insert();
					}
				}
			}
			
			if($_POST['generalCondition']){
				$conditionsByProduct->setSerial_pro($serial_pro);
				$conditionsByProduct->setSerial_gcn($_POST['generalCondition']);
				$hasGeneralCondition=$conditionsByProduct->hasGeneralCondition($_SESSION['language']);

				if($hasGeneralCondition){
					$conditionsByProduct -> update();
				}else{
					$conditionsByProduct -> insert();
				}
				$error=1;
			}
		}
	} elseif($statusUpdated) { //SUCCESFULL UPDATE
		$serial_pxc=$productbyCountry->getProductByCountry($serial_pro);

		//************************** UPDATE REFERENTIAL PRICES **************************
		if($_POST['price_by_product']){
			$priceByProductByCountry -> deleteByProduct($serial_pro);
			foreach($_POST['price_by_product'] as $key=>$data){
				$priceByProductByCountry->setSerial_pxc($serial_pxc);
				$priceByProductByCountry->setDuration_ppc($data['duration_ppc']);
				if(isset($_POST['price_by_range'])){
					$priceByProductByCountry->setMin_ppc($data['min_ppc']);
					$priceByProductByCountry->setMax_ppc($data['max_ppc']);
				}else{
					$priceByProductByCountry->setPrice_ppc($data['price_ppc']);
				}
				$priceByProductByCountry->setCost_ppc($data['cost_ppc']);
				$priceByProductByCountry->insert();
			}
		}
		
		ProductByCountry::updateAditionalDays($db, $_POST['serial_pro'],$_POST['aditional_day_pxc']);
		$error=1;
	} else {
		$error=3;
	}

	http_redirect('modules/product/fSearchProduct/'.$error);
?>