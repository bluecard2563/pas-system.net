<?php
/*
 * File: fSearchCondition.php
 * Author: Patricio Astudillo
 * Creation Date: 24/06/2010, 09:01:44 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 24/06/2010, 09:01:44 AM
 */

Request::setInteger('0:error');
$gCondition=new GeneralCondition($db);
$gConditionList=$gCondition->getGeneralConditions($_SESSION['serial_lang']);

$smarty->register('gConditionList,error');
$smarty->display();
?>
