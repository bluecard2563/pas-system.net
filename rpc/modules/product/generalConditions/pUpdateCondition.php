<?php 
/*
File: pNewCondition.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 18:51 am
Last Modified: 31/08/2010
Modified By: Patricio Astudillo
*/


switch($_POST['hdnType']){
	case 'UPDATE_CONDITION':
		$serial_gcn=$_POST['hdnSerial_gcn'];
		if(GConditionByLanguageByCountry::updateNameByCondition_Language($db, $serial_gcn, $_POST['selLanguage'], htmlspecialchars($_POST['txtCondition']))){
			$condition=new GeneralCondition($db,$serial_gcn);
			if($condition->getData()){
				$condition->setStatus_gcn($_POST['selStatus']);
				if($condition->update()){
					$error=6;
				}else{ //STATUS UPDATE FAILED.
					$error=7;
				}
			}else{ //INFO WASN'T LOADED.
				$error=5;
			}
		}else{ //NAME TRANSLATION WASN'T REGISTERED.
			$error=9;
		}
		break;
	case 'ADD_FILE':
		$serial_lang=$_POST['selLanguageTranslation'];
		$serial_glc=$_POST['hdnAddSerial_glc'];
		
		$glc=new GConditionByLanguageByCountry($db,$serial_glc);
		if($glc->getData()){
			$glc->setSerial_lang($serial_lang);
			
			if($_FILES['general_condition_planet']['type']=='application/pdf'){
				$url_gcn = GConditionByLanguageByCountry::generateFileName($db);
				
				if(copy($_FILES['general_condition_planet']['tmp_name'],DOCUMENT_ROOT.'pdfs/generalConditions/'.$url_gcn)){
					$glc -> setUrl_glc($url_gcn);

					if($glc->insert()){
						$error=1;
					}else{ //INSERT FAILED.
						$error=2;
					}
				}else{ //FILE WASN'T UPLOADED CORRECTLY.
					$error=3;
				}
			}else{ //WRONG FILE EXTENSION.
				$error=4;
			}
		}else{ //INFO WASN'T LOADED.
			$error=5;
		}
		break;
	case 'REPLACE_FILE':
		$serial_glc=$_POST['hdnSerial_glc'];

		$glc=new GConditionByLanguageByCountry($db,$serial_glc);
		if($glc->getData()){
			$old_file=DOCUMENT_ROOT."pdfs/generalConditions/".$glc->getUrl_glc();
			
			if($_FILES['general_condition_planet']['type']=='application/pdf'){
				$url_gcn = GConditionByLanguageByCountry::generateFileName($db);
				
				if(copy($_FILES['general_condition_planet']['tmp_name'],DOCUMENT_ROOT.'pdfs/generalConditions/'.$url_gcn)){
					$glc -> setUrl_glc($url_gcn);

					if($glc->update()){
						$error=1;

						if(!unlink($old_file)){//PREVIOUS FILE WASN'T DELETED.
							$error=8;
						}
					}else{ //UPDATE FAILED
						$error=2;
					}
				}else{ //FILE WASN'T UPLOADED CORRECTLY.
					$error=3;
				}
			}else{ //WRONG FILE EXTENSION.
				$error=4;
			}
		}else{ //INFO WASN'T LOADED.
			$error=5;
		}
		break;
	default:
		//SUBMIT WAS PERFORMED FROM ANOTHER UNKOWN LOCATION.
		$error=10;
		break;
}

http_redirect('modules/product/generalConditions/fSearchCondition/'.$error);
?>