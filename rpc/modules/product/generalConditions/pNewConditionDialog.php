<?php
/*
File: pNewCondition.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 18:51 am
Last Modified: 31/08/2010
Modified By: David Bergmann
*/

Request::setString('txtCondition');
Request::setString('hdnLanguage');
Request::setString('planetFileName');
Request::setString('ecuadorFileName');

$condition = new GeneralCondition($db);
$condition->setStatus_gcn('ACTIVE');
$serial_gcn = $condition->insert();

if($serial_gcn) {
	$gCondLanguage = new GConditionByLanguageByCountry($db);
	$gCondLanguage->setName_glc(htmlspecialchars($txtCondition));
	$gCondLanguage->setSerial_gcn($serial_gcn);
	$gCondLanguage->setSerial_lang($hdnLanguage);
	
	//PLANETASSIST FILE
	$gCondLanguage->setUrl_glc($planetFileName);
	$gCondLanguage->setSerial_cou('1');

	if($gCondLanguage->insert()) {
		//ECUADOR FILE
		$gCondLanguage->setUrl_glc($ecuadorFileName);
		$gCondLanguage->setSerial_cou('62');
		if($gCondLanguage->insert()) {
			echo json_encode($serial_gcn);
		} else {
			echo json_encode(FALSE);
		}
	} else {
		echo json_encode(FALSE);
	}
} else {
	echo json_encode(FALSE);
}
?>