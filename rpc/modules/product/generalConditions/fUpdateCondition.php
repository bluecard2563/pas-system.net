<?php 
/*
File: fUpdateCondition.php
Author: Esteban Angulo
Creation Date: 24/06/2010
*/

$serial_gcn=$_POST['rdConditions'];
$gcn=new GeneralCondition($db,$serial_gcn);

if($gcn->getData()){
	$gconditions_list=GConditionByLanguageByCountry::getTranslationsInfo($db, $serial_gcn, $_SESSION['serial_lang']);

	$status_list=GeneralCondition::getGConditionStatus($db);
	$gcn=get_object_vars($gcn);
	unset($gcn['db']);

	$languageList=new Language($db);
	$languageList=$languageList->getLanguages();
	
	$smarty->register('gconditions_list,serial_gcn,status_list,gcn,languageList');
}

$smarty->display();
?>