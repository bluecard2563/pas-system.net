<?php 
/*
File: pNewCondition.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 18:51 am
Last Modified: 31/08/2010
Modified By: Patricio Astudillo
*/

if($_FILES['general_condition_ecuador']['type']=='application/pdf' &&
   $_FILES['general_condition_planet']['type']=='application/pdf'){
	$condition=new GeneralCondition($db);
	$condition->setStatus_gcn('ACTIVE');
	$serial_gcn=$condition->insert();

	if($serial_gcn){
		$gCondLanguage=new GConditionByLanguageByCountry($db);
		$gCondLanguage->setName_glc(htmlspecialchars($_POST['txtCondition']));
		$gCondLanguage->setSerial_gcn($serial_gcn);
		$gCondLanguage->setSerial_lang($_POST['hdnLanguage']);

		/*INSERTING THE FILE FOR PLANET ASSIST*/
		$url_pas_gcl = GConditionByLanguageByCountry::generateFileName($db);
		if(copy($_FILES['general_condition_planet']['tmp_name'],DOCUMENT_ROOT.'pdfs/generalConditions/'.$url_pas_gcl)){
			$gCondLanguage -> setUrl_glc(($url_pas_gcl));
			$gCondLanguage->setSerial_cou('1');
			$file_pas_copied=true;
		}else{ //PAS FILE WASN'T UPLOADED.
			$error=2;
		}

		if($file_pas_copied && $gCondLanguage->insert()){
			$continue=true;
		}else{ //PAS INFO WASN'T INSERTED.
			$error=3;
		}
		/*END*/

		/*INSERTING THE FILE FOR ECUADOR*/
		$url_ec_gcl = GConditionByLanguageByCountry::generateFileName($db);
		if($continue &&
		   copy($_FILES['general_condition_ecuador']['tmp_name'],DOCUMENT_ROOT.'pdfs/generalConditions/'.$url_ec_gcl)){
		   $gCondLanguage -> setUrl_glc(utf8_encode($url_ec_gcl));
		   $gCondLanguage->setSerial_cou('62');
		   $file_ec_copied=true;
		}else{
			if(!$error){
				$error=4;
			}
		}

		if($file_ec_copied && $gCondLanguage->insert()){
			$error=1;
		}else{ //PAS INFO WASN'T INSERTED.
			if(!$error){
				$error=5;
			}
		}
		/*END*/		
	}else{ //GENERAL CONDITION WASN'T INSERTED
		$error=6;
	}
}else{ //WRONG FILE FORMATS WHERE INSERTED.
	$error=7;
}

http_redirect('modules/product/generalConditions/fNewCondition/'.$error);
?>