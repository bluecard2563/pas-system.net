<?php session_start();
/*
File: pNewBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 11:59 am
Last Modified:
Modified By:
*/

$code_lang=$_SESSION['language'];
$language = new Language($db);

$serial_lang = $language -> getSerialByCode($code_lang);

$benefit=new Benefit($db);
$benefitbyLanguage = new BenefitbyLanguage($db);

$benefit->setStatus_ben(($_POST['selStatusBenefit']));

$serial_ben=$benefit->insert();
if($serial_ben){

	$benefitbyLanguage -> setSerial_lang($serial_lang);
	$benefitbyLanguage -> setSerial_ben($serial_ben);
	$benefitbyLanguage -> setDescription_bbl(utf8_decode($_POST['txtDescBenefit']));
	if($benefitbyLanguage -> insert()){
		echo $serial_ben;
	}
	else{
		echo false;	
	}
}else{
	echo false;
}
?>