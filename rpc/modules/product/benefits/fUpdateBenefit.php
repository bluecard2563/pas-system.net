<?php 
/*
File: fUpdateBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 16:33
Last Modified:
Modified By:
*/

Request::setInteger('hdnBenefitID');
$language=new Language($db);
$languageList=$language->getRemainingBenefitLanguages($hdnBenefitID);

$benefit=new Benefit($db,$hdnBenefitID);
$statusList=$benefit->getStatusValues();

if($benefit->getData()){
	$data['status_ben']=$benefit->getStatus_ben();
	$data['serial_ben']=$hdnBenefitID;

        $bbl=new BenefitbyLanguage($db);
        $translations=$bbl->getTranslations($hdnBenefitID);
        //Debug::print_r($translations);
        $titles=array('#','Traducci&oacute;n','Idioma','Actualizar');
}else{
	http_redirect('modules/product/benefits/fSearchBenefit/3');
}

$smarty->register('data,statusList,titles,translations,languageList');
$smarty->display();
?>