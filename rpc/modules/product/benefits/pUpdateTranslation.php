<?php
/*
File: pUpdateTranslation.php
Author: Patricio Astudillo
Creation Date: 26/01/2010 15:48pm
LastModified: 26/01/2010
Modified By: Patricio Astudillo
*/

$bbl=new BenefitbyLanguage($db,$_POST['serial_bbl']);

if($bbl->getData()){
    $bbl->setDescription_bbl($_POST['txtBenefit']);
    
    if($bbl->update()){
        http_redirect('modules/product/benefits/fSearchBenefit/2');
    }else{
        http_redirect('modules/product/benefits/fSearchBenefit/4');
    }
}else{
    http_redirect('modules/product/benefits/fSearchBenefit/4');
}
?>
