<?php session_start();
/*
File: fUpdateProduct
Author: Pablo Puente
Creation date: 22/12/2009
Last modified: 30/07/2010
Modified By: David Bergmann
*/

Request::setInteger('0:error');
$serial_pro = $_POST['hdnSerial_pro'];
$product = new Product($db);
$product_by_countries = new ProductByCountry($db);
$code_lang=$_SESSION['language'];

if($error==0){
	if($serial_pro){//If the product is found
		$product->setSerial_pro($serial_pro);
		$data=$product->getInfoProduct($code_lang);
				
		$benefit = new Benefit($db);
		$condition = new Condition($db);
		$restriction = new Restriction($db);
		$generalCondition = new GeneralCondition($db);
        $productType = new ProductType($db);

		$benefits = $benefit->getActiveBenefits($code_lang);
		$conditions = $condition->getConditions($code_lang);
		$restrictions = $restriction->getRestrictions($code_lang);
		$generalConditions = $generalCondition->getGeneralConditions($_SESSION['serial_lang']);
        $productTypes = $productType->getProductTypes($code_lang);

		$benefitsByProduct = new BenefitsByProduct($db);
		$benefitsByProduct->setSerial_pro($serial_pro);
		$benefitsByProduct = $benefitsByProduct->getBenefits();

        $conditionsByProduct = new ConditionsByProduct($db);
		$conditionsByProduct->setSerial_pro($serial_pro);
		$user = new User($db,$_SESSION['serial_usr']);
		$user->getData();
		$city = new City($db,$user->getSerial_cit());
		$city->getData();
		$conditionsByProduct = $conditionsByProduct -> getConditions();

        $priceByProductByCountry = new PriceByProductByCountry($db);
        $prices= $priceByProductByCountry->getPricesByProduct($serial_pro);

        $product_by_countries->setSerial_pro($serial_pro);
        $product_by_countries->setSerial_cou('1');
        $product_by_country = $product_by_countries->getDataByCountryAndProduct();

		$extraTypes = Product::getExtraTypes($db);

		/*Checks if the product has been alrady sold*/
		/*if(Product::alreadySold($db, $serial_pro)) {
			$sold = true;
			$smarty -> register('sold');
		}*/


        /** Dialog Bene*********fit ********************************/
		$benefit=new Benefit($db);
		$statusList=$benefit->getStatusValues();
		$smarty->register('statusList');
		/**************************************************/

	} else {
		http_redirect('modules/product/fSearchProduct/2');
	}
}

$smarty -> register('user,data,benefits,conditions,restrictions,generalConditions,benefitsByProduct,conditionsByProduct,productTypes,prices,priceslength,product_by_country,extraTypes,global_extraTypes');
$smarty -> display();

?>