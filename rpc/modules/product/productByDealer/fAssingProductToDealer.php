<?php
/*
 * File: fAssingProductToDealer.php
 * Author: Patricio Astudillo
 * Creation Date: 03-ago-2010, 11:53:04
 * Modified By: Patricio Astudillo
 */

Request::setString('0:error');

$user=new User($db, $_SESSION['serial_usr']);
$user->getData();

if($user->getBelongsto_usr()=='MANAGER'){ //ONLY MANAGER USER'S CAN ASSIGN PRODUCTS TO A DEALER
	if($user->isPlanetAssistUser()){
		$zone=new Zone($db);
		$zonesList=$zone->getAllowedZones($_SESSION['countryList']);

		$smarty->register('zonesList');
	}else{
		$countryID=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
		$dealer=new Dealer($db);
		$params=array();
		$aux=array('field'=>'d.status_dea',
				   'value'=> 'ACTIVE',
				   'like'=> 1);
		array_push($params, $aux);

		$aux=array('field'=>'d.serial_mbc',
				   'value'=> $user->getSerial_mbc(),
				   'like'=> 1);
		array_push($params, $aux);

		$aux=array('field'=>'d.dea_serial_dea',
				   'value'=> 'NULL',
				   'like'=> 1);
		array_push($params, $aux);

		$aux=array('field'=>'d.phone_sales_dea',
				   'value'=> 'NO',
				   'like'=> 1);
		array_push($params, $aux);

		$dealerResponsible=new UserByDealer($db);
		$ubdList=$dealerResponsible->getResponsibles($_SESSION['serial_usr']);

		if($ubdList){
			$ubd_serials=array();
			foreach($ubdList as $u){
				array_push($ubd_serials, $u['serial_ubd']);
			}

			$aux=array('field'=>'ubd.serial_ubd',
					   'value'=> $ubd_serials,
					   'like'=> 1);
			array_push($params, $aux);
		}

		$dealerList=$dealer->getDealers($params);

		if(count($dealerList)>0){
			$items=count($dealerList);
			$pages=(int)($items/7)+1;
		}
		$smarty->register('dealerList,pages,countryID');
	}	
}
$smarty->register('error');
$smarty->display();
?>