<?php
/*
File: pAssingProductToDealer
Author: David Bergmann
Creation Date: 06/08/2010
Last Modified:
Modified By:
*/
	$dealersList = $_POST['chkDealers'];
	$productList = $_POST['chkProduct'];
	
	if(is_array($dealersList)){
		foreach($dealersList as $dealer){
			if(count($dealersList)==1){
				ProductByDealer::deactivateProductsByDealer($db, $dealer);
			}

			if(is_array($productList)) {
				$productByDealer = new ProductByDealer($db);
				$productByDealer->setSerial_dea($dealer);
				$productByDealer->setStatus_pbd('ACTIVE');
		
				foreach ($productList as $product) {
					$productByDealer -> setSerial_pxc($product);	
					if($productByDealer -> productByDealerExists()) {
						if(!$productByDealer -> update()) {
							http_redirect('modules/product/productByDealer/fAssingProductToDealer/2');
						}
					}else{
						if(!$productByDealer->insert()) {
							http_redirect('modules/product/productByDealer/fAssingProductToDealer/2');
						}
					}
				}
			}
		}
	}
	http_redirect('modules/product/productByDealer/fAssingProductToDealer/1');
?>