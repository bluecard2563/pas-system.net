<?php
/*
File: pUpdateTranslation.php
Author: Patricio Astudillo
Creation Date: 26/01/2010 15:48pm
LastModified: 26/01/2010
Modified By: Patricio Astudillo
*/


$productByLanguage=new ProductbyLanguage($db);
$productByLanguage->setSerial_pbl($_POST['serial_pbl']);
$productByLanguage->setName_pbl($_POST['diaName_pro']);
$productByLanguage->setDescription_pbl($_POST['txtDescription_pro']);

   
    if($productByLanguage->updateBySerial_pbl()){
        http_redirect('modules/product/fSearchProductTranslation/1');
    }else{
        http_redirect('modules/product/fSearchProductTranslation/2');
    }

?>
