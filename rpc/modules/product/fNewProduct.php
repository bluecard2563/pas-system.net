<?php 
/*
Author:Miguel Ponce
Modified date:24/12/2009
Last modified:24/12/2009
*/

$productType = new ProductType($db);
$benefit = new Benefit($db);
$condition = new Condition($db);
$restriction = new Restriction($db);
$generalCondition = new GeneralCondition($db);

$code_lang=$_SESSION['language'];

$benefits = $benefit->getActiveBenefits($code_lang);
$conditions = $condition->getConditions($code_lang);
$restrictions = $restriction->getRestrictions($code_lang);

$generalConditions = $generalCondition->getGeneralConditions($_SESSION['serial_lang']);
$productTypes = $productType->getProductTypes($code_lang);

$extraTypes = Product::getExtraTypes($db);

/** Dialog Benefit ********************************/
$benefit=new Benefit($db);
$statusList=$benefit->getStatusValues();
$smarty->register('statusList');
/**************************************************/

Request::setInteger('0:error');
$smarty->register('error,benefits,conditions,restrictions,generalConditions,productTypes,extraTypes,global_extraTypes');
$smarty->display();
?>