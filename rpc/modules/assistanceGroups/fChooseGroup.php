<?php
/*
    Document   : fChooseGroup.php
    Created on : May 3, 2010, 8:52:52 AM
    Author     : H3Dur4k
    Description:
        retrieves info needed to choose a group to be edited.
*/

Request::setInteger('0:error');
$country=new Country($db);
$countryList=$country->getCountriesWithAssistanceGroups();
if($countryList)
		$smarty->register('countryList');
$smarty->register('error');
$smarty->display();
?>
