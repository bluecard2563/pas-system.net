<?php
/*
    Document   : pEditGroup.php
    Created on : May 3, 2010, 10:02:26 AM
    Author     : H3Dur4k
    Description:
        Edits a group record.
*/
Request::setInteger('hdnGroup');
Request::setString('txtGroupName');
$error=1;
$assistanceGroup=new AssistanceGroup($db,$hdnGroup);
$assistanceGroup->getData();
$assistanceGroup->setName_asg($txtGroupName);
if(!$assistanceGroup->update()){
	$error=2;
}
 http_redirect("modules/assistanceGroups/fChooseGroup/$error");
?>
