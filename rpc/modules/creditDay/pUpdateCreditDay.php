<?php 
/*
File: pUpdateCreditDay.php
Author: Patricio Astudillo
Creation Date: 07/01/2010 15:05 
Last Modified: 07/01/2010
Modified By: Patricio Astudillo
*/

$serialItem=$_POST['serialItem'];
$creditD=new CreditDay($db,$serialItem);

if($creditD->getData()){
	$creditD->setDays_cdd($_POST['txtUpCreditD']);
	$creditD->setStatus_cdd($_POST['selStatus']);
	
	if($creditD->update()){
		$error=3;
	}else{
		$error=4;
	}
}else{
	$error=4;
}



http_redirect('modules/creditDay/fNewCreditDay/'.$error);
?>