<?php 
/*
File: pNewCreditDay.php
Author: Patricio Astudillo
Creation Date: 06/01/2010 18:58 
Last Modified: 06/01/2010
Modified By: Patricio Astudillo
*/

$creditD=new CreditDay($db);
$creditD->setSerial_cou($_POST['selCountry']);
$creditD->setDays_cdd($_POST['txtCDay']);
$creditD->setStatus_cdd('ACTIVE');

if($creditD->insert()){
	$error=1;
}else{
	$error=2;
}

http_redirect('modules/creditDay/fNewCreditDay/'.$error);
?>