<?php 
/*
File: fNewCreditDay.php
Author: Patricio Astudillo
Creation Date: 06/01/2010 
Last Modified: 
Modified By: 
*/

Request::setInteger('0:error');
$country=new Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);

$creditD=new CreditDay($db);
$statusList=$creditD->getStatusList();

$smarty->register('countryList,error,statusList');
$smarty->display();
?>