<?php
/*
File: fUpdateManager.php
Author: Santiago Ben�tez
Creation Date: 19/02/2010 12:23
Last Modified: 19/02/2010 12:23
Modified By: Santiago Ben�tez
*/

Request::setInteger('0:error');

Request::setString('selContactByServiceProvider');

$country=new Country($db);
$provider=new ServiceProvider($db);
$city=new City($db);
$zone= new  Zone($db);
$contact= new ContactByServiceProvider($db,$selContactByServiceProvider);

if($contact->getData()){
	$data['serial_csp'] = $selContactByServiceProvider;
	$data['serial_spv'] = $contact -> getSerial_spv();
        $data['serial_cit'] = $contact ->getSerial_cit();
        $data['first_name_csp'] = $contact ->getFirst_name_csp();
        $data['last_name_csp'] = $contact ->getLast_name_csp();
        $data['phone_csp'] = $contact ->getPhone_csp();
        $data['email_csp'] = $contact ->getEmail_csp();
        $data['type_csp'] = $contact ->getType_csp();
        $data['status_csp'] = $contact ->getStatus_csp();
        $data['aux'] = $contact ->getAux_data();

	//Debug::print_r($data);
}else{
	http_redirect('modules/contactByServiceProvider/fSearchContactByServiceProvider/3');
}

/*Recover all available type of managers from the system*/
$typeList=$contact->getAllTypes();
$statusList=$contact->getAllStatus();

$zoneList=$zone->getZones();

$countryList=$country->getCountriesByZone($data['aux']['serial_zon'],$_SESSION['countryList']);
$countryProvList=$country->getCountriesByZone($data['aux']['prov_serial_zon'],$_SESSION['countryList']);
//Debug::print_r($countryList);

$cityList=$city->getCitiesByCountry($data['aux']['serial_cou'],$_SESSION['cityList']);
$providerList=$provider->getProviders($data['aux']['prov_serial_cou']);
//Debug::print_r($providerList);
$smarty->register('error,typeList,statusList,data,cityList,countryList,zoneList,countryProvList,providerList,statusList');
$smarty->display();
?>