<?php
/*
File: pNewContactByServiceProvider.php
Author: Santiago Ben�tez
Creation Date: 19/02/2010 11:07
Last Modified: 19/02/2010 11:07
Modified By: Santiago Ben�tez
*/

Request::setInteger('0:error');

$contact=new ContactByServiceProvider($db);
$contact->setSerial_cit($_POST['selCity']);
$contact->setSerial_spv($_POST['selProvider']);
$contact->setFirst_Name_csp($_POST['txtFirstName']);
$contact->setLast_Name_csp($_POST['txtLastName']);
$contact->setPhone_csp($_POST['txtPhone']);
$contact->setEmail_csp($_POST['txtEmail']);
$contact->setType_csp($_POST['selType']);
$contact->setEmail2_csp($_POST['txtEmail2']);

if($contact->insert()){
        http_redirect('modules/contactByServiceProvider/fNewContactByServiceProvider/1');
}else{
        http_redirect('modules/contactByServiceProvider/fNewContactByServiceProvider/2');
}

?>
