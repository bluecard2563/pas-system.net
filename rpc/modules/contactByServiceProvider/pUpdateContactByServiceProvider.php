<?php
/*
File: pUpdateContactByServiceProvider.php
Author: Santiago Ben�tez.
Creation Date: 19/02/2010 15:19
Last Modified: 19/02/2010 15:19
Modified By: Santiago Ben�tez
*/
$contact=new ContactByServiceProvider($db,$_POST['hdnContactID']);
$contact->setSerial_cit($_POST['selCity']);
$contact->setSerial_spv($_POST['selProvider']);
$contact->setFirst_Name_csp($_POST['txtFirstName']);
$contact->setLast_Name_csp($_POST['txtLastName']);
$contact->setPhone_csp($_POST['txtPhone']);
$contact->setEmail_csp($_POST['txtEmail']);
$contact->setType_csp($_POST['selType']);
$contact->setStatus_csp($_POST['selStatus']);
$contact->setEmail2_csp($_POST['txtEmail2']);

if($contact->update()){
        http_redirect('modules/contactByServiceProvider/fSearchContactByServiceProvider/2');
}else{
        http_redirect('modules/contactByServiceProvider/fUpdateContactByServiceProvider/2');
}
?>
