<?php
/*
 * File: fCheckRefundApplication.php
 * Author: Patricio Astudillo
 * Creation Date: 21/04/2010, 12:26:04 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 21/04/2010, 12:26:04 PM
 */

Request::setString('0:serial_ref');

$refund=new Refund($db, $serial_ref);
if($refund->getData()){
	$refund=get_object_vars($refund);
	$refund['document_ref']=unserialize($refund['document_ref']);
	unset($refund['db']);

	//Get the Information of the Customer and Invoice
	$sale=new Sales($db, $refund['serial_sal']);
	$onCoverageTime=$sale->onCoverageTime();
	$customer=new Customer($db,$refund['aux']['serial_cus']);
	$invoice=new Invoice($db,$refund['aux']['serial_inv']);

	if($customer->getData() and $invoice->getData() and $sale->getData()){
		$sale=get_object_vars($sale);
		unset($sale['db']);

		//Get the Payment Data
		$payment=new Payment($db,$invoice->getSerial_pay());
		$payment->getData();
		$payment=get_object_vars($payment);
		unset($payment['db']);			
		
		$dealer=Counter::getCountersDealer($db, $sale['serial_cnt']);
		$customer=get_object_vars($customer);
		$invoice=get_object_vars($invoice);
		unset($customer['db']);
		unset($invoice['db']);

		//Get the Refund Data for the aplication.
		$refundData=array();
		$refundData['reason']=Refund::getRefundReason($db);
		$refundData['retainedType']=Refund::getRetainedType($db);
		$parameter=new Parameter($db,'11'); //Parameter for the required documents to fill the refuns aplication.
		$parameter->getData();
		$documentList=explode(',',$parameter->getValue_par());

		//Get the taxes applied in the invoice
		if($invoice['applied_taxes_inv']!=""){
			$invoice['applied_taxes_inv']=unserialize($invoice['applied_taxes_inv']);
			foreach($invoice['applied_taxes_inv'] as $t){
				$taxes+=$t['tax_percentage'];
			}
			array_push($invoice['applied_taxes_inv'], array('tax_name'=>'Total','tax_percentage'=>$taxes));
		}

		/*Comissions Discount*/
		$comission=AppliedComission::hasPayedComissionsBySale($db, $sale['serial_sal'], Dealer::getLastComissionPaymentDate($db, $dealer['serial_dea']) );
		$bonus=AppliedBonus::hasPayedBonusBySale($db, $sale['serial_sal'], Dealer::getLastComissionPaymentDate($db, $dealer['serial_dea']) );

		//PreTotal Refund Calculation
		$totalRefund=$sale['total_sal'];
		$totalRefund-=($sale['total_sal']*($invoice['discount_prcg_inv']/100));
		$totalRefund-=($sale['total_sal']*($invoice['other_dscnt_inv']/100));
		$totalRefund*=(1+($taxes/100));
		$totalRefund=number_format($totalRefund, '2');

		if($comission or $bonus){
			$paiedAmounts=1;
		}

		if($bonus){
			$paidBonus=1;
		}
		
		$smarty->register('sale,customer,invoice,dealer,payment,documentList,refundData,taxes,totalRefund,refund,serial_ref,comission,bonus,paiedAmounts,paidAmounts,onCoverageTime');
	}

	$smarty->display();
}else{
	http_redirect('modules/refund/fSearchPendingAplications/7');
}
?>
