<?php
/*
 * File: fSearchPendingApplications.php
 * Author: Patricio Astudillo
 * Creation Date: 21/04/2010, 12:25:39 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 26/04/2010, 12:14:39 PM
 */

//Get the user's who can dispatch refund applications
/*GENERATING THE ALERTS*/
$parameter=new Parameter($db, '18'); //Parameters for Stand-by Requests.
$parameter->getData();
$destinationUsers=unserialize($parameter->getValue_par());
$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
	$usersCountry='62';
}
$destinationUsers[$usersCountry]=explode(',', $destinationUsers[$usersCountry]);

if(in_array($_SESSION['serial_usr'], $destinationUsers[$usersCountry])){
	//Get all the pending refund applications
	$refundApplications=Refund::getPendingApplications($db);

	Request::setString('0:error');
	Request::setString('1:serial_cn');

	if($serial_cn){
		$number_cn=CreditNote::getCreditNoteNumber($db, $serial_cn);
	}

	$smarty->register('refundApplications,error,number_cn,serial_cn');
}else{
	$not_allowed_user=1;
}

$smarty->register('not_allowed_user');
$smarty->display();
?>