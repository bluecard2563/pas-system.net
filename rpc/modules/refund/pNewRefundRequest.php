<?php
/*
 * File: pNewRefundRequest.php
 * Author: Patricio Astudillo
 * Creation Date: 15/04/2010, 06:05:09 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 15/04/2010, 06:05:09 PM
 */

$refund=new Refund($db);
$refund->setSerial_sal($_POST['hdnSerial_sal']);
$refund->setSerial_usr($_SESSION['serial_usr']);
$refund->setStatus_ref('STAND-BY');
$refund->setTotalToPay_ref($_POST['total_to_pay']);
$refund->setComments_ref(specialchars($_POST['txtObservations']));
$refund->setType_ref($_POST['hdnRefundType']);
$refund->setDiscount_ref($_POST['hdnDiscount']);
$refund->setOtherDiscount_ref($_POST['hdnOtherDiscount']);
$_POST['document']['other']=$_POST['OtherDocument'];
$refund->setDocument_ref(serialize($_POST['document']));
$refund->setReason_ref($_POST['selRefundReason']);
$refund->setRetainType_ref($_POST['selPenaltyType']);
$refund->setRetainValue_ref($_POST['txtPenaltyFee']);
$refundID=$refund->insert();

ERP_logActivity('new', $refund);//ERP ACTIVITY

if($refundID){
	/*GENERATING THE ALERTS*/
	$parameter=new Parameter($db, '18'); //Parameters for Stand-by Requests.
	$parameter->getData();
	$destinationUsers=unserialize($parameter->getValue_par());
	$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
	if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
		$usersCountry='62';
	}
	$destinationUsers[$usersCountry]=explode(',', $destinationUsers[$usersCountry]);

	$alert=new Alert($db);
	$alert->setMessage_alt('Solicitud de Reembolso pendiente.');
	$alert->setType_alt('REFUND');
	$alert->setStatus_alt('PENDING');
	$alert->setRemote_id($refundID);
	$alert->setTable_name('refund/fCheckRefundApplication/');
	
	foreach($destinationUsers[$usersCountry] as $d){
		$alert->setSerial_usr($d);
		if($alert->insert()){
			$error=1;
		}else{
			$error=3;
			break;
		}
	}
	/*END ALERTS*/
}else{ //The refund application wans't inserted.
	$error=2;
}

http_redirect('main/refund/'.$error);
?>
