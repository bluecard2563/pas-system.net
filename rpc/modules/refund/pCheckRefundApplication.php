<?php
/*
 * File: pCheckRefundApplication.php
 * Author: Patricio Astudillo
 * Creation Date: 26/04/2010, 04:51:55 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 26/04/2010, 04:51:55 PM
 */

if($_POST['hdnSerial_ref']){
	$refund=new Refund($db, $_POST['hdnSerial_ref']);
	$refund->getData();
	$refund->setAuthDate_ref(date('d/m/Y'));
	$user=new User($db,$refund->getSerial_usr());
	$user->getData();
	$misc['email']=$user->getEmail_usr();

	//Get the sale information so we can make the refund of the comissions/bonus/promo
	$sale=new Sales($db, $refund->getSerial_sal());
	$sale->getData();
	$misc['serial_sal']=$refund->getSerial_sal();

	//Set all the information for the credit note.
	$invoice=new Invoice($db, $sale->getSerial_inv());
	$invoice->getData();
	$invoice=get_object_vars($invoice);
	$counter=new Counter($db, $sale->getSerial_cnt());
	$counter->getData();
	$dealer=new Dealer($db, $counter->getSerial_dea());
	$dealer->getData();
	$mbc=new ManagerbyCountry($db, $dealer->getSerial_mbc());
	$mbc->getData();

	//Make the array of parameters for creating the Credit Note in DB
	$misc['db']=$db;
	$misc['type']='REFUND';
	$misc['serial']=$refund->getSerial_ref();
	$misc['serial_man']=$mbc->getSerial_man();
	$misc['observations']=$_POST['txtObservations'];
	$misc['card_number']=$sale->getCardNumber_sal();
	$misc['resolution']=$global_refundStatus[$_POST['selStatus']];
	$misc['dealer_name']=$dealer->getName_dea();
    $misc['paymentStatus']='PENDING';

	if($invoice['aux']['type_cus']!=''){
		$misc['person_type']=$invoice['aux']['type_cus'];
	}else{
		$misc['person_type']='LEGAL_ENTITY';
	}

	//Set the alert parameters so that we can marked it as done.
	$alert=new Alert($db);
	if($_POST['selStatus']=='APROVED'){
		$refund->setStatus_ref('APROVED');
		$refund->setTotalToPay_ref($_POST['total_to_pay']);
		$refund->setRetainType_ref($_POST['selPenaltyType']);
		$refund->setRetainValue_ref($_POST['txtPenaltyFee']);

		if($refund->update()){
			ERP_logActivity('update', $refund);//ERP ACTIVITY
			
			/*Put the Sale in REFUND status*/
			$sLog=new SalesLog($db);
			$sLog->setSerial_sal($sale->getSerial_sal());
			$sLog->setSerial_usr($_SESSION['serial_usr']);
			$sLog->setUsr_serial_usr($_SESSION['serial_usr']);
			$sLog->setType_slg('REFUNDED');
			$miscLog=array('old_status_sal'=>$sale->getStatus_sal(),
						   'new_status_sal'=>'REFUNDED',
						   'Description'=>'Refund of a sale.',
						   'Observations'=>$_POST['txtObservations']);
			$sLog->setDescription_slg(serialize($miscLog));
			$sLog->insert();
			$sale->setStatus_sal('REFUNDED');
			$sale->update();

			if(!isset($_POST['denyInternationalFeeRefund'])){
				GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal(), TRUE);
			}
			else{
				if($_POST['denyInternationalFeeRefund']=='APROVED'){
					GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal(), TRUE);
				}else{
					GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal(), FALSE);
				}
			}
			/*End Sale Modification*/

			/*Mark the invoice with "HAS CREDIT NOTE"*/
			$inv= new Invoice($db, $sale->getSerial_inv());
			$inv->getData();
			$inv->setHas_credit_note_inv('YES');
			$inv->update();
			
			ERP_logActivity('update', $inv);//ERP ACTIVITY
			/*End Invoice Modification*/

			$misc['amount']=$refund->getTotalToPay_ref();
			if($_POST['paidAmounts']){
				$paidAmounts=true;
			}else{
				$paidAmounts=false;
			}

			if($_POST['paidBonus']){
				$paidBonus=true;
			}else{
				$paidBonus=false;
			}
						
			$result=creditNoteProcess($misc,$alert, $_POST, $paidAmounts, $paidBonus);			
			$error=$result['error'];
		}else{
			$error=2;
		}
	}else{
		$refund->setStatus_ref('DENIED');

		if($refund->update()){
			ERP_logActivity('update', $refund);//ERP ACTIVITY
			
			$alert->setServeAlert('REFUND',$_POST['hdnSerial_ref']);
			GlobalFunctions::sendMail($misc, 'refund_application');
			$error=1;
		}else{
			$error=2;
		}
	}
	http_redirect('modules/refund/fSearchPendingAplications/'.$error.'/'.$result['cNoteID']);
}else{
	http_redirect('modules/refund/fSearchPendingAplications');
}


/*
 * @name: creditNoteProcess
 * @Description: The function for the entire process of creating the credit note.
 * @params: $misc, $alert object.
 * @returns: $error
 */
function creditNoteProcess($misc,$alert,$datosPost,$paidAmounts, $paidBonus){
	$cNote=GlobalFunctions::generateCreditNote($misc);

	$misc['cNoteNumber']=$cNote['number'];
	$misc['cNoteID']=$cNote['id'];

	switch($cNote['error']){
		case 'no_document': $error=3;
			break;
		case 'insert_error': $error=4;
			break;
		case 'update_error': $error=5;
			break;
		case 'cNote_error': $error=6;
			break;
		case 'success':
			$error=1;
			$alert->setServeAlert('REFUND',$datosPost['hdnSerial_ref']);
			$result['cNoteID']=$cNote['id'];

			//Get the comissions amounts back.
			$apComission=new AppliedComission($misc['db']);
			$apComission->setSerial_sal($misc['serial_sal']);
			if($paidAmounts){
				$apComission->refundSale('REMOVE');
			}else{
				$apComission->refundSale();
			}
			//End Comissions.

			//Agregar devoluciones de bonus AQUI
			if($paidBonus==false){
				GlobalFunctions::refundBonus($misc, $misc['serial_sal']);
			}					
			
			//Send Cotification E-mail
			GlobalFunctions::sendMail($misc, 'refund_application');
			break;
	}

	$result['error']=$error;
	return $result;
}
?>