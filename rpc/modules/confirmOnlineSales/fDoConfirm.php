<?php 
/*
File: fConfirmSale.php
Author: Santiago Borja 
Creation Date: 24/06/2010
*/

	$serialPay = $_POST['txtSerialInput'];
	
	$paym = new Payment($db, $serialPay);
	if(!$paym -> getData()){
		http_redirect('modules/confirmOnlineSales/fConfirmSale/2');
	}else{
		$errorCode = $paym -> confirmPaidSale();
		http_redirect('modules/confirmOnlineSales/fConfirmSale/'.$errorCode);
	}
	
	$smarty->register('error');
	$smarty->display();
?>