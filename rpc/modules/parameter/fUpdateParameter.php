<?php 
/*
File: fUpdateParameter.php
Author: Santiago Benítez
Creation Date: 13/01/2010 12:18
Last Modified: 13/01/2010 12:18
Modified By: Santiago Benítez
*/
Request::setInteger('hdnParameterID');

$parameter=new Parameter($db,$hdnParameterID);
$conditionList=$parameter->getConditionValues();
$typeList=$parameter->getTypeValues();
if($parameter->getData()){
	$data['name_par']=$parameter->getName_par();
	$data['value_par']=$parameter->getValue_par();
	$data['type_par']=$parameter->getType_par();
	$data['description_par']=$parameter->getDescription_par();
	$data['condition_par']=$parameter->getCondition_par();
	$data['serial_par']=$hdnParameterID;
}else{
	http_redirect('modules/parameter/fSearchParameter/3');
}$smarty->register('data,conditionList,typeList');
$smarty->display();
?>