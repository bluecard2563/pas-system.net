<?php 
/*
File: fEditAlertsParameter.php
Author: Gabriela Guerrero
Creation Date: 11/05/2010
Last Modified:
Modified By:
*/
Request::setInteger('0:error');

$parametersList= array('15','16','17','18','21','22','23','24','25','32');
foreach($parametersList as $p){
    $parameter=new Parameter($db, $p);
    $parameter->getData();
    $alertsList[$p]=array('serial_par'=>$p, 'name_par'=>$parameter->getName_par());
}
$country=new Country($db);
$countriesList=$country->getOwnCountries($_SESSION['countryList']);
$smarty->register('alertsList,countriesList,error');
$smarty->display();
?>