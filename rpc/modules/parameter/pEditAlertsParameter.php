<?php 
/*
File: pEditAlertsParamater.php
Author:Gabriela Guerrero
Creation Date: 11/05/2010
Last Modified: 
Modified By: 
*/
$error=1;

$alertId=$_POST['selAlerts'];
$users=$_POST['usersTo'];
$countries=$_POST['selectedCountries'];
$countries=explode(",", $countries);
$cont=0;
if(is_array($countries)){
	foreach($countries as $c){
		if($c=='0'){
			unset($countries[$cont]);
		}
		$cont=$cont+1;
	}
	$usersByCountry=array();
	if(is_array($users)){
		foreach($users as $key => $u){
			$usersByCountry[$countries[$key]]=$u;
		}
		if(is_array($usersByCountry)){
			foreach($usersByCountry as &$uBC){
				$uBC=implode(",",$uBC);
			}
			$parameter=new Parameter($db,$alertId);
			if($parameter){
				$parameter->getData();
				$parameter->setValue_par(serialize($usersByCountry));
				if(!$parameter->update()){
					$error=6;//couldn't update Parameter db
				}
			}else{
			$error=5;//error setting $parameter with $alertId
		}
		}else{
			$error=4;//$usersByCountry not an array
		}
	}else{
		$error=3;//$users not an array
	}
}else{
	$error=2;//$countries not an array
}

    http_redirect('modules/parameter/fEditAlertsParameter/'.$error);
?>