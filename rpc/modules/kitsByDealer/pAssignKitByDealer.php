<?php
/*
File: pAssignKit.php
Author: Santiago Ben�tez
Creation Date: 23/02/2010 15:01
Last Modified: 23/02/2010 15:01
Modified By: Santiago Ben�tez
*/
$flag=0;
$kit= new KitByDealer($db);
$kitDetails= new KitDetails($db);
$kit->setSerial_dea($_POST['selBranch']);
$kitsTotal=$_POST['hdnKitsTotal'];
$serial_kbd=$_POST['hdnKitByDealerID'];
if($_POST['selAction']=='GIVEN'){
    $kitsTotal+=$_POST['txtAmount'];
}else{
    $kitsTotal-=$_POST['txtAmount'];
}
$kit->setTotal_kbd($kitsTotal);
if($serial_kbd){
    $kit->setSerial_kbd($serial_kbd);
    if(!$kit->update()){
        $flag=1;
    }
}else{
    $serial_kbd=$kit->insert();
    if(!$serial_kbd){
        $flag=1;
    }
}
if($flag==0){
    $kitDetails->setAction_kdt($_POST['selAction']);
    $kitDetails->setAmount_kdt($_POST['txtAmount']);
    $kitDetails->setSerial_kbd($serial_kbd);
    if(!$kitDetails->insert()){
        $flag=1;
    }
}
if($flag==1){
    http_redirect('modules/kitsByDealer/fAssignKitByDealer/2');
}else{
    http_redirect('modules/kitsByDealer/fAssignKitByDealer/1');
}


/*Recover all available type of contacts from the system*/
$zoneList=$zone->getZones();

$smarty->register('error,zoneList');
$smarty->display();
?>