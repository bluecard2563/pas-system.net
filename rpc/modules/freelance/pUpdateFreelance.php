<?php 
/*
File: pUpdateFreelance.php
Author: Esteban Angulo
Creation Date: 31/12/2009 10:41
Last Modified:  06/01/2010
Modified By: 
*/

$freelance=new Freelance($db,$_POST['txtSerial_fre']);
if($freelance->getData()){
	$freelance->setName_fre($_POST['txtFirstname']);
	$freelance->setLastname_fre($_POST['txtLastname']);
	$freelance->setDocument_fre($_POST['txtDocument']);
	$freelance->setBirthdate_fre($_POST['txtBirthdate']);
	$freelance->setAddress_fre($_POST['txtAddress']);
	$freelance->setPhone1_fre($_POST['txtPhone1']);
	$freelance->setPhone2_fre($_POST['txtPhone2']);
	$freelance->setCellphone_fre($_POST['txtCellphone']);
	$freelance->setEmail_fre($_POST['txtMail']);

}

if($freelance->update()){
	http_redirect('main/freelance/3');
}
else{
	http_redirect('main/freelance/2');
}
/*END LOAD*/
?>