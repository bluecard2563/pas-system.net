<?php 
/*
File: fUpdateFreelance.php
Author: Esteban Angulo
Creation Date: 31/12/2009 10:15
Last Modified: 06/01/2010
Modified By:  
*/

Request::setString('serial_fre');

$freelance=new Freelance($db);
$freelance->setSerial_fre($serial_fre);

if($freelance->getData()){
	$data['serial_fre']=$serial_fre;
	$data['name_fre']=$freelance->getName_fre();
	$data['lastname_fre']=$freelance->getLastname_fre();
	$data['document_fre']=$freelance->getDocument_fre();
	$data['birthdate_fre']=$freelance->getBirthdate_fre();
	$data['address_fre']=$freelance->getAddress_fre();
	$data['phone1_fre']=$freelance->getPhone1_fre();
	$data['phone2_fre']=$freelance->getPhone2_fre();
	$data['cellphone_fre']=$freelance->getCellphone_fre();
	$data['email_fre']=$freelance->getEmail_fre();
	$smarty->register('error,data');
	$smarty->display();
}else{
	http_redirect('modules/freelance/fSearchFreelance/1');
}
?>