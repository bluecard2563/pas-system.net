<?php 
/*
File: fSearchFreelance.php
Author: Esteban Angulo
Creation Date: 31/12/2009 09:40
Last Modified: 31/12/2009
Modified By: 
*/

Request::setInteger('0:error');
$freelance=new Freelance($db);
$freelanceAux=$freelance->getFreelances();

if(is_array($freelanceAux)){
	foreach($freelanceAux as $key=>$f){
		$freelanceList [$key] = utf8_encode($f['first_name_usr'].' '.$f['last_name_usr'].' - '.$f['document_fre']);
	}
}

$smarty->register('error,freelanceList');
$smarty->display();
?>