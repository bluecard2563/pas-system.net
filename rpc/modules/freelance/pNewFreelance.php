<?php
/*
File: pNewFreelance.class.php
Author: Esteban Angulo
Creation Date: 30/12/2009 16:37
Last Modified:06/01/2010 
*/

$freelance=new Freelance($db);
	$freelance->setName_fre($_POST['txtFirstname']);
	$freelance->setLastname_fre($_POST['txtLastname']);
	$freelance->setDocument_fre($_POST['txtDocument']);
	$freelance->setBirthdate_fre($_POST['txtBirthdate']);
	$freelance->setAddress_fre($_POST['txtAddress']);
	$freelance->setPhone1_fre($_POST['txtPhone1']);
	$freelance->setPhone2_fre($_POST['txtPhone2']);
	$freelance->setCellphone_fre($_POST['txtCellphone']);
	$freelance->setEmail_fre($_POST['txtMail']);


	if($freelance->insert()){//Insert was correct
		$error=1;
	}else{//There was an error
		$error=2;
	}
	
http_redirect('main/freelance/'.$error);

?>