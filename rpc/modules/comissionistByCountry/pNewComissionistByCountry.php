<?php 
/*
File: pNewComissionistByCountry.php
Author: David Bergmann
Creation Date: 04/02/2010
Last Modified: 05/02/2010
Modified By: David Bergmann
*/

$commissionistByCountry = new ComissionistByCountry($db);
$commissionistByCountry->setSerial_cou($_POST['selCountryAssign']);
$commissionistByCountry->setSerial_usr($_POST['selUser']);
if($_POST['selComissionist']) {
    $commissionistByCountry->setCom_serial_cbc($_POST['selComissionist']);
}
$commissionistByCountry->setPercentage_cbc($_POST['txtComission']);
$commissionistByCountry->setStatus_cbc('ACTIVE');

if($commissionistByCountry->insert()) {
        $error=1;
}
else {
        $error=2;
}

http_redirect('modules/comissionistByCountry/fNewComissionistByCountry/'.$error);
?>