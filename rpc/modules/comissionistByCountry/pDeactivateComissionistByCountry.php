<?php
/*
File: pDeactivateComissionistByCountry
Author: David Bergmann
Creation Date: 18/03/2010
Last Modified:
Modified By:
*/

if($_POST['user']) {
    $users = implode(",",$_POST['user']);
}
$comissionistByCountry = new ComissionistByCountry($db);
if($comissionistByCountry->deactivateComissionistsByCountry($_POST['selCountry'],$users)){
   http_redirect('modules/comissionistByCountry/fDeactivateComissionistByCountry/1');
}
http_redirect('modules/comissionistByCountry/fDeactivateComissionistByCountry/2');
?>
