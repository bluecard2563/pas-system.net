<?php
/*
File: pUpdateServiceProvider.php
Author: Mario L�pez
Creation Date: 22/02/2010
Last Modified: 23/02/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('hdnServProvId');
Request::setInteger('selCity');
Request::setString('txtServProvName');
Request::setString('servProvType');
Request::setString('txtServProvEmail');
Request::setString('txtServProvAddress');
Request::setFloat('txtServProvCost');
Request::setFloat('txtServProvTechCost');
Request::setFloat('txtServProvMedSimCost');
Request::setFloat('txtServProvMedComCost');
Request::setString('servProvStatus');
$txtServProvPhone=serialize($_POST['txtServProvPhone']);
$txtServProvEmail=serialize($_POST['txtServProvEmail']);


$serviceProvider = new ServiceProvider($db,$hdnServProvId);
$serviceProvider->getData();
$serviceProvider->setSerial_cit($selCity);
$serviceProvider->setEmail_spv(specialchars($txtServProvEmail));
$serviceProvider->setAddress_spv(specialchars($txtServProvAddress));
$serviceProvider->setName_spv(specialchars($txtServProvName));
$serviceProvider->setPhone_spv($txtServProvPhone);
$serviceProvider->setService_spv($txtServProvCost);
$serviceProvider->setMedical_complex_spv($txtServProvMedComCost);
$serviceProvider->setMedical_simple_spv($txtServProvMedSimCost);
$serviceProvider->setTechnical_spv($txtServProvTechCost);
$serviceProvider->setType_spv($servProvType);

if($serviceProvider->update()){
	http_redirect('modules/serviceProvider/fSearchServiceProvider/1');
}else{
	http_redirect('modules/serviceProvider/fSearchServiceProvider/3');
}
?>