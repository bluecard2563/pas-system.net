<?php
/*
File: fNewServiceProvider.php
Author: Mario L�pez
Creation Date: 19/02/2010
Last Modified: 23/02/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('0:error');
$serviceProvider = new ServiceProvider($db);
$serviceTypes = $serviceProvider->getAllTypes();
$country= new  Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);

$smarty->register('error,serviceTypes,countryList');
$smarty->display();
?>
