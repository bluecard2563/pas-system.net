<?php
/*
File: fSearchServiceProvider.php
Author: Mario L�pez
Creation Date: 19/02/2010
Last Modified:24/06/2010
Modified By:Nicolas Flores (using table instead of autocompleter)
*/

Request::setInteger('0:error');
$smarty->register('error');
$serviceProvider=new ServiceProvider($db);
$providerList=$serviceProvider->getAllServiceProviders();
if($providerList){
	$smarty->register('providerList');
}
$smarty->display();
?>