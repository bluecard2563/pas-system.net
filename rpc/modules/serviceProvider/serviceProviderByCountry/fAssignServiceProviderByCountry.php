<?php 
/*
File: fAssignServiceProviderByCountry.php
Author: Mario Lopez
Creation Date: 22/02/2010
Modified By: Edwin Salvador
Last Modified: 01/03/2010
Last Modified:24/06/2010
Modified By:Nicolas Flores (using table instead of autocompleter)
*/

Request::setInteger('0:hdnServProvSerial');
if($hdnServProvSerial){
    $zone=new Zone($db);
    $zoneList=$zone->getZones();
    
    $spc = new ServiceProviderByCountry($db,NULL,$hdnServProvSerial);
    $assigned_countries = $spc->getServiceProviderByCountry('ACTIVE');
    if($assigned_countries){
        $smarty->register('assigned_countries');
    }
    //Debug::print_r($assigned_countries);
    $smarty->register('zoneList,hdnServProvSerial');
    $smarty->display();
}else{
    http_redirect('modules/serviceProvider/serviceProviderByCountry/fSearchServiceProviderByCountry/2');
}
?>