<?php 
/*
File: pUpdateCustomer.php
Author: Patricio Astudillo M.
Creation Date: 30/12/2009 11:10
Last Modified: 30/12/2009
Modified By: Santiago Ben�tez
*/

$customer=new Customer($db,$_POST['serial_cus']);
/*If we have only to reset the customer's password*/
if($_POST['hdnResetPassword']=='1'){
    $customer->getData();
    $password=GlobalFunctions::generatePassword(6, 8, false);
    $customer->setPassword_cus(md5($password));

    $misc['textForEmail']='Estimado(a) '.$_POST['txtFirstName'].' '.$_POST['txtLastName'].'<br><br> Su clave ha sido reestablecida.
                           Para poder ingresar nuevamente al sistema sus datos son: ';
    $misc['pswd']=$password;
    $misc['email']=$customer->getEmail_cus();
    $misc['username']=$customer->getEmail_cus();

    if(GlobalFunctions::sendMail($misc, 'resetPassword')){ //Sending the client his login info.
        http_redirect('modules/customer/fSearchCustomer/1');
    }else{
        http_redirect('modules/customer/fSearchCustomer/4');
    }
}
/*END reset Password*/



/*Update the Customer's Info*/
if($customer->getData()){
	$customer->setSerial_cit($_POST['selCity']);
	$customer->setType_cus($_POST['selType']);
	$customer->setDocument_cus($_POST['txtDocument']);

	if($_POST['txtFirstName']&& $_POST['selType']=='PERSON'){
		$customer->setFirstname_cus($_POST['txtFirstName']);
		$customer->setRelative_cus($_POST['txtRelative']);
	}else if($_POST['txtFirstName1'] && $_POST['selType']=='LEGAL_ENTITY'){
		$customer->setFirstname_cus($_POST['txtFirstName1']);
		$customer->setRelative_cus($_POST['txtManager']);
	}
	$customer->setLastname_cus($_POST['txtLastName']);
	$customer->setBirthdate_cus($_POST['txtBirthdate']);
	$customer->setPhone1_cus($_POST['txtPhone1']);
	$customer->setPhone2_cus($_POST['txtPhone2']);
	$customer->setCellphone_cus($_POST['txtCellphone']);
	$customer->setEmail_cus($_POST['txtMail']);
	if($_POST['txtAddress']){
        $customer->setAddress_cus($_POST['txtAddress']);
    }
    if($_POST['txtAddress1']){
    	$customer->setAddress_cus($_POST['txtAddress1']);
    }
    
    $customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	if($_POST['chkVip']){
		$customer->setVip_cus($_POST['chkVip']);
	}else{
		$customer->setVip_cus('0');
	}
	$customer->setStatus_cus($_POST['selStatus']);
	if($_POST['txtPassword']){
		$customer->setPassword_cus(md5($_POST['txtPassword']));
	}
	$language=new Language($db);
	$sessionLanguage=$language->getSerialByCode($_SESSION['language']);
	$question=new Question($db);
	$maxSerial=$question->getMaxSerial($sessionLanguage);
	$questionList = Question::getActiveQuestions($db, $sessionLanguage,$_POST['serial_cus']);
	$answer=new Answer($db);
	$answer->setSerial_cus($hdnCustomerID);
	$answerList=$answer->getAnswers();
	/*PARAMETERS*/
	$parameter=new Parameter($db,1); //-->
	$parameter->getData();
	$parameterValue=$parameter->getValue_par(); //value of the paramater used to display or not the questions
	$parameterCondition=$parameter->getCondition_par(); //condition of the paramater used to display or not the questions
	/*END PARAMETERS*/
	if($customer->update()){
		ERP_logActivity('update', $customer);//ERP ACTIVITY
		
		$flag=0;
		foreach($questionList as $q){
			if($q['serial_ans']){
				$answer=new Answer($db,$q['serial_ans']);
			}else{
				$answer=new Answer($db);
			}			
			$answer->setSerial_cus($_POST['serial_cus']);
			$answer->setSerial_que($q['serial_que']);
			$ans=$_POST['question_'.$q['serial_qbl']];
			if($ans){
				if($q['type_que']=='YN'){
					$answer->setYesno_ans($ans);
				}else{
					$answer->setAnswer_ans($ans);
				}
				if($q['yesno_ans'] or $q['answer_ans']){
					if($answer->update()){
						$flag=0;
					}else{
						$flag=1;
						break;
					}
				}else{
					if($answer->insert()){
						$flag=0;
					}else{
						$flag=1;
						break;
					}					
				}
			}
			
		}
		if($flag==0){
			http_redirect('modules/customer/fSearchCustomer/2');
		}else{
			http_redirect('modules/customer/fUpdateCustomer/1');
		}
	}
}
/*Finish Update*/


/*We retrieve the country and cities from the system*/
$country=new Country($db);
$city=new City($db);
$countryList=$country->getCountry();
$statusList=$customer->getAllStatus();
$typesList=$customer->getAllTypes();
/*END*/

/*LOAD of the previous info for the selected customer due to an error of the update*/
$customer->getData();
$city->setSerial_cit($customer->getSerial_cit());
$city->getData();
$cityList=$city->getCitiesByCountry($city->getSerial_cou());
$data['serial_cus']=$hdnCustomerID;
$data['serial_cit']=$customer->getSerial_cit();
$data['type_cus']=$customer->getType_cus();
$data['document_cus']=$customer->getDocument_cus();
$data['first_name_cus']=$customer->getFirstname_cus();
$data['last_name_cus']=$customer->getLastname_cus();
$data['address_cus']=$customer->getAddress_cus();
$data['phone1_cus']=$customer->getPhone1_cus();
$data['phone2_cus']=$customer->getPhone2_cus();
$data['cellphone_cus']=$customer->getCellphone_cus();
$data['serial_cou']=$city->getSerial_cou();
$data['serial_cit']=$customer->getSerial_cit();
$data['birthdate_cus']=$customer->getBirthdate_cus();
$data['email_cus']=$customer->getEmail_cus();
$data['relative_cus']=$customer->getRelative_cus();
$data['relative_phone_cus']=$customer->getRelative_phone_cus();
$data['vip_cus']=$customer->getVip_cus();
$data['status_cus']=$customer->getStatus_cus();
$data['password_cus']=$customer->getPassword_cus();	

$smarty->register('error,data,statusList,typesList,countryList,cityList,parameterValue,parameterCondition,questionList,maxSerial,answerList');
$smarty->display('modules/customer/fUpdateCustomer.'.$_SESSION['language'].'.tpl');
/*END LOAD*/
?>