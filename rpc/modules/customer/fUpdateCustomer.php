<?php 
/*
File: fUpdateCustomer.php
Author: Patricio Astudillo M.
Creation Date: 30/12/2009 16:50
Last Modified: 30/12/2009
Modified By: Patricio Astudillo 
*/

Request::setInteger('hdnCustomerID');
/*QUESTIONS*/
$language=new Language($db);
$sessionLanguage=$language->getSerialByCode($_SESSION['language']);
$question=new Question($db);
$maxSerial=$question->getMaxSerial($sessionLanguage);
$answer=new Answer($db);
$answer->setSerial_cus($hdnCustomerID);
$answerList=$answer->getAnswers();
$questionList = Question::getActiveQuestions($db, $sessionLanguage,$hdnCustomerID);//list of active questions shown in the form


/*END QUESTIONS*/
/*PARAMETERS*/
$parameter=new Parameter($db,1); //-->
$parameter->getData();
$parameterValue=$parameter->getValue_par(); //value of the paramater used to display or not the questions
$parameterCondition=$parameter->getCondition_par(); //condition of the paramater used to display or not the questions
/*END PARAMETERS*/

$customer=new Customer($db,$hdnCustomerID);
$country=new Country($db);
$city=new City($db);
$countryList=$country->getCountry();
$statusList=$customer->getAllStatus();
$typesList=$customer->getAllTypes();

if($customer->getData()){
	$city->setSerial_cit($customer->getSerial_cit());
	$city->getData();
	$cityList=$city->getCitiesByCountry($city->getSerial_cou(), $_SESSION['cityList']);
	
	$data['serial_cus']=$hdnCustomerID;
	$data['serial_cit']=$customer->getSerial_cit();
	$data['type_cus']=$customer->getType_cus();
	$data['document_cus']=$customer->getDocument_cus();
	$data['first_name_cus']=$customer->getFirstname_cus();
	$data['last_name_cus']=$customer->getLastname_cus();
	$data['address_cus']=$customer->getAddress_cus();
	$data['phone1_cus']=$customer->getPhone1_cus();
	$data['phone2_cus']=$customer->getPhone2_cus();
	$data['cellphone_cus']=$customer->getCellphone_cus();
	$data['serial_cou']=$city->getSerial_cou();
	$data['serial_cit']=$customer->getSerial_cit();
	$data['birthdate_cus']=$customer->getBirthdate_cus();
	$data['email_cus']=$customer->getEmail_cus();
	$data['relative_cus']=$customer->getRelative_cus();
	$data['relative_phone_cus']=$customer->getRelative_phone_cus();
	$data['vip_cus']=$customer->getVip_cus();
	$data['status_cus']=$customer->getStatus_cus();
	$data['password_cus']=$customer->getPassword_cus();	
}else{
	http_redirect('modules/customer/fSearchCustomer/3');
}
//Debug::print_r($data);
$smarty->register('error,data,statusList,typesList,countryList,cityList,parameterValue,parameterCondition,questionList,maxSerial,answerList');
$smarty->display();
?>