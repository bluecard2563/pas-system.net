<?php 
/*
File: pNewCustomer.php
Author: Patricio Astudillo M.
Creation Date: 30/12/2009 15:46
Last Modified: 19/02/2010
Modified By: Patricio Astudillo
*/

$customer=new Customer($db);
$customer->setSerial_cit($_POST['selCity']);
$customer->setType_cus($_POST['selType']);
$customer->setDocument_cus($_POST['txtDocument']);

 if($_POST['txtFirstName']){
        $customer->setFirstname_cus($_POST['txtFirstName']);
    }

    if($_POST['txtFirstName1']){
        $customer->setFirstname_cus($_POST['txtFirstName1']);
    }

$customer->setLastname_cus($_POST['txtLastName']);
$customer->setBirthdate_cus($_POST['txtBirthdate']);
$customer->setPhone1_cus($_POST['txtPhone1']);
$customer->setPhone2_cus($_POST['txtPhone2']);
$customer->setCellphone_cus($_POST['txtCellphone']);
$customer->setEmail_cus($_POST['txtMail']);

 if($_POST['txtAddress']){
        $customer->setAddress_cus($_POST['txtAddress']);
    }
    if($_POST['txtAddress1']){
        $customer->setAddress_cus($_POST['txtAddress1']);
    }

if($_POST['txtRelative']){
		$customer->setRelative_cus($_POST['txtRelative']);
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
if($_POST['txtManager']){
		$customer->setRelative_cus($_POST['txtManager']);
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}


if($_POST['chkVip']){
	$customer->setVip_cus($_POST['chkVip']);
}else{
	$customer->setVip_cus('0');
}

$customer->setStatus_cus('ACTIVE');
$password=GlobalFunctions::generatePassword(6, 8, false);
$customer->setPassword_cus(md5($password));

if($customer->insert()){
	ERP_logActivity('new', $customer);//ERP ACTIVITY
	
    /*Generating the info for sending the e-mail*/
    $misc['textForEmail']='<br><br> Si usted desea hacer uso de nuestro servicio de ventas
                           on-line, ingrese a <a href="www.planet-assist.com" target="_blank"> www.planet-assist.com</a>, ingresando los siguientes datos: ';
     if($_POST['txtFirstName']){
        $misc['user']=$_POST['txtFirstName'].' '.$_POST['txtLastName'];
    }
    if($_POST['txtFirstName1']){
        $misc['customer']=$_POST['txtFirstName1'];
    }
	$misc['username']=$customer->getEmail_cus();
    $misc['pswd']=$password;
    $misc['email']=$customer->getEmail_cus();
    /*END E-mail Info*/

	if($_POST['hdnQuestionsFlag']){
		$language=new Language($db);
		$sessionLanguage=$language->getSerialByCode($_SESSION['language']);
		$question=new Question($db);
		$questionList = Question::getActiveQuestions($db, $sessionLanguage);
		$flag=0;
		foreach($questionList as $q){
			$answer=new Answer($db);
			$answer->setSerial_cus($customer->getLastSerial());
			$answer->setSerial_que($q['serial_que']);
			$ans=$_POST['question_'.$q['serial_qbl']];
			if($ans){
				if($q['type_que']=='YN'){
					$answer->setYesno_ans($ans);
				}else{
					$answer->setAnswer_ans($ans);
				}
				if($answer->insert()){
					$flag=1;
				}else{
					$flag=2;
					break;
				}
			}else{
				break;
			}
			
		}
                
		if($flag==1){
			if($_POST['txtMail']!=''){
                    if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
                        http_redirect('modules/customer/fNewCustomer/1');
                    }else{
                        http_redirect('modules/customer/fNewCustomer/3');
                    }
			}
			else{
				http_redirect('modules/customer/fNewCustomer/1');
			}
		}else{
			http_redirect('modules/customer/fNewCustomer/2');
		}
	}else{
		if($_POST['txtMail']!=''){
            if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
                http_redirect('modules/customer/fNewCustomer/1');
            }else{
                http_redirect('modules/customer/fNewCustomer/3');
            }
		}
		else{
			http_redirect('modules/customer/fNewCustomer/1');
		}
	}
}else{
	http_redirect('modules/customer/fNewCustomer/2');
}
?>