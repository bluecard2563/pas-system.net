<?php 
/*
File: fNewCustomer.php
Author: Patricio Astudillo M.
Creation Date: 30/12/2009 12:07
Last Modified: 27/01/2009 11:07
Modified By: Santiago Ben�tez 
*/

Request::setInteger('0:error');

/*QUESTIONS*/
$language=new Language($db);
$sessionLanguage=$language->getSerialByCode($_SESSION['language']);
$question=new Question($db);
$maxSerial=$question->getMaxSerial($sessionLanguage);
$questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
/*END QUESTIONS*/

/*PARAMETERS*/
$parameter=new Parameter($db,1); //-->
$parameter->getData();
$parameterValue=$parameter->getValue_par(); //value of the paramater used to display or not the questions
$parameterCondition=$parameter->getCondition_par(); //condition of the paramater used to display or not the questions
/*END PARAMETERS*/

$customer=new Customer($db);
$country=new Country($db);
$countryList=$country->getCountry();

$typesList=$customer->getAllTypes();

$smarty->register('error,statusList,typesList,countryList,questionList,parameterValue,parameterCondition,maxSerial');
$smarty->display();
?>