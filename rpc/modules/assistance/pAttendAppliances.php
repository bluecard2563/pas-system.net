<?php
/*
File: pAttendAppliances
Author: David Bergmann
Creation Date: 10/06/2010
Last Modified: 12/07/2010
Modified By: David Bergmann
*/

if($_POST['btnApprove']) {
    $status_tcu = "AUTHORIZED";
} else if($_POST['btnDeny']){
    $status_tcu = "DENIED";
}

if(is_array($_POST['apliances'])) {
    foreach ($_POST['apliances'] as $a) {
        $temporary_customer = new TemporaryCustomer($db, $a);
        $temporary_customer->getData();
        $temporary_customer->setStatus_tcu($status_tcu);
        
        //File closed if exists
        if($temporary_customer->getSerial_fle() && $status_tcu == "DENIED") {
            TemporaryCustomer::denyAppliancisOnCascade($db, $temporary_customer->getSerial_fle());
            $file = new File($db,$temporary_customer->getSerial_fle());
            $file->getData();
            $file->setStatus_fle('closed');
            if(!$file->update()) {
                http_redirect('modules/assistance/fAttendAppliances/2');
            }
        }
        if(!$temporary_customer->update()) {
            http_redirect('modules/assistance/fAttendAppliances/2');
        }
    }
} else {
    http_redirect('modules/assistance/fAttendAppliances/3');
}

http_redirect('modules/assistance/fAttendAppliances/1');
?>