<?php
/*
File: pDispatchAssistanceClaims
Author: David Bergmann
Creation Date: 11/06/2010
Last Modified: 24/06/2010
Modified By: David Bergmann
*/

//VERIFIES IF ANY CLAIM IS SELECTED
if(is_array($_POST['chkClaims'])) {
    //Document Validation
    if($_FILES['flePaymentRequest']['name'] != ''){
        $file_name=$_FILES['flePaymentRequest']['name'];
        $ext=strtolower(end(explode(".", $_FILES['flePaymentRequest']['name'])));
        $allowedExtensions = array("txt", "doc", "docx", "xls", "xlsx", "pdf", "gif", "jpg", "jpeg", "png");

        //Verifies if the file has the allowed extensions
        if(in_array(end(explode(".", $file_name)), $allowedExtensions)){
            $path = DOCUMENT_ROOT.'assistanceFiles/assistancePayments/';
            $newFile = 'paymentRequest_'.date('U').'.'.$ext;
            $path .= $newFile;
            if(move_uploaded_file($_FILES['flePaymentRequest']['tmp_name'], $path)) {
                $_FILES['flePaymentRequest']['name'] = "paymentRequest".rand(0, 1000);
                $serial_usr = $_SESSION['serial_usr'];
                //LIQUIDATES EACH CLAIM
                foreach ($_POST['chkClaims'] as $c) {
                    $paymentRequest = new PaymentRequest($db, $c);
                    $paymentRequest->getData();
                    $paymentRequest->setAprovationUser($serial_usr);
                    $paymentRequest->setDispatched_prq("YES");
                    $paymentRequest->setDispatched_date_prq(date("d/m/y"));
                    $paymentRequest->setFile_url_prq($path);

                    if(!$paymentRequest->update()) {
                        http_redirect('modules/assistance/fDispatchAssistanceClaims/2');
                    }
                }
                http_redirect('modules/assistance/fDispatchAssistanceClaims/1');
            } else{
                http_redirect('modules/assistance/fDispatchAssistanceClaims/2');
            }
        }
        else{
            http_redirect('modules/assistance/fDispatchAssistanceClaims/4');
        }
    } else {
        http_redirect('modules/assistance/fDispatchAssistanceClaims/5');
    }
} else {
    http_redirect('modules/assistance/fDispatchAssistanceClaims/3');
}
?>