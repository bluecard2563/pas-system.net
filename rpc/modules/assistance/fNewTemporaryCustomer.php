<?php
/*
File: fNewTemporaryCustomer
Author: David Bergmann
Creation Date: 01/04/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$zone = new Zone($db);
$zoneList = $zone->getZones();
$card_number = $_SESSION['card_number'];
unset ($_SESSION['card_number']);
$sales = new Sales($db);
$sales->setCardNumber_sal($card_number);
$sales->getDataByCardNumber();
$serial_cit = $sales->getSerial_cit();
$city = new City($db, $serial_cit);
$city->getData();
$country = new Country($db, $city->getSerial_cou());
$country->getData();
$data['serial_cou'] = $country->getSerial_cou();
$data['serial_zon'] = $country->getSerial_zon();

if($data['serial_zon']) {
    $countryList = $country->getCountriesByZone($data['serial_zon']);
}

$serial_cus = $sales->getSerial_cus();

$customer = new Customer($db, $serial_cus);
$customer->getData();
$data['first_name'] = $customer->getFirstname_cus();
$data['last_name'] = $customer->getLastname_cus();
$data['document_cus']= $customer->getDocument_cus();
$data['birthdate_cus']= $customer->getBirthdate_cus();
$data['phone1_cus']= $customer->getPhone1_cus();

$smarty->register('error,zoneList,card_number,data,countryList');
$smarty -> display();
?>
