<?php
/*
File: pEditAssistance
Author: David Bergmann
Creation Date: 12/04/2010
Last Modified: 21/06/2010
Modified By: David Bergmann
*/

$_SESSION['card_number'] = $_POST['hdnCardNumber'];

$file = new File($db,$_POST['hdnSerialFle']);
$file->getData();

$file->setSerial_cit($_POST['selCity']);
$file->setSerial_cus($_POST['hdnSerialCus']);
$file->setDiagnosis_fle($_POST['txtDiagnosis']);
$file->setCause_fle($_POST['txtCause']);
$file->setSerial_sal($_POST['hdnSerialSale']);
$file->setType_fle($_POST['txtAssistanceType']);
if($_POST['hdnSerialTrl']) {
    $file->setSerial_trl($_POST['hdnSerialTrl']);
}

if($file->update()) {
	//Update services medical type
	if($_POST['selMedicalType']) {
		$file->updateFileMedicalType($_POST['selMedicalType']);
	}

    $customer = new Customer($db,$_POST['hdnSerialCus']);
    $customer->getData();
    $customer->setMedical_background_cus($_POST['txtMedicalBackground']);
    $customer->update();
    
    ERP_logActivity('update', $customer);//ERP ACTIVITY

    $blog = new Blog($db);
    $blog->setSerial_fle($_POST['hdnSerialFle']);
    $blog->setType_blg("CLIENT");
    $blog->setEntry_blg(nl2br($_POST['txtClientBlog']));
    if($blog->update()) {
        $blog->setType_blg("OPERATOR");
        $blog->setEntry_blg(nl2br($_POST['txtOperatorBlog']));
        if($blog->update()) {

            //IF THERE'S NO INVOICE OR IT ISN'T PAID
            //CREATES A NEW APPLIANCE
            /*INVOICE*/
            $sale = new Sales($db, $file->getSerial_sal());
            $sale->getData();
            //verifies if the sale has invoice
            $invoice = new Invoice($db, $sale->getSerial_inv());
            $invoice->getData();
            //verifies if the invoice is paid
            if($invoice->getStatus_inv() == "STAND-BY" && $invoice->daysToDueDate()<0) {
                $temporary_customer = new TemporaryCustomer($db);
                $customer = new Customer($db,$file->getSerial_cus());
                $customer->getData();
                $city = new City($db,$customer->getSerial_cit());
                $city->getData();
                $temporary_customer->setSerial_cou($city->getSerial_cou());
                $temporary_customer->setSerial_fle($file->getSerial_fle());
                $city = new City($db,$sale->getSerial_cit());
                $city->getData();
                $dealer = Dealer::getDealerByCardNumber($db,$sale->getCardNumber_sal());
                $temporary_customer->setCou_serial_cou($city->getSerial_cou());
                $temporary_customer->setSerial_usr($_SESSION['serial_usr']);
                $temporary_customer->setDocument_tcu($customer->getDocument_cus());
                $temporary_customer->setFirst_name_tcu($customer->getFirstname_cus());
                $temporary_customer->setLast_name_tcu($customer->getLastname_cus());
                $temporary_customer->setBirthdate_tcu($customer->getBirthdate_cus());
                $temporary_customer->setAgency_name_tcu($dealer[0]['name_dea']);
                $temporary_customer->setPhone_tcu($customer->getPhone1_cus());
                $temporary_customer->setContact_phone_tcu($customer->getCellphone_cus());
                $temporary_customer->setContact_address_tcu($customer->getAddress_cus());
                $temporary_customer->setCard_number_tcu($sale->getCardNumber_sal());
                $temporary_customer->setExtra_tcu(utf8_decode($blog->getEntry_blg()));
                $temporary_customer->setProblem_description_tcu($file->getDiagnosis_fle().utf8_decode(" (Venta no pagada y fuera de días de crédito)"));
                $temporary_customer->setStatus_tcu('REQUESTED');

                /*E-MAIL INFO*/
                $misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s o ingrese a <a href='".URL."' target='blank'>PAS</a>.";
                $country = new Country($db, $temporary_customer->getSerial_cou());
                $country->getData();
                $misc['country'] = $country->getName_cou();
                $misc['agency']=$temporary_customer->getAgency_name_tcu();
                $country = new Country($db, $temporary_customer->getCou_serial_cou());
                $country->getData();
                $misc['travelCountry']=$country->getName_cou();
                $misc['document']=$temporary_customer->getDocument_tcu();
                $misc['name']=$temporary_customer->getFirst_name_tcu()." ".$temporary_customer->getLast_name_tcu();
                $misc['birthDate']=$temporary_customer->getBirthdate_tcu();
                $misc['phone']=$temporary_customer->getPhone_tcu();
                $misc['contactAddress']=$temporary_customer->getContact_address_tcu();
                $misc['contactPhone']=$temporary_customer->getContact_phone_tcu();
                $misc['cardNumber']=$temporary_customer->getCard_number_tcu();
                $misc['extra']=$temporary_customer->getExtra_tcu();
                $misc['problem']=$temporary_customer->getProblem_description_tcu();

                $sale = new Sales($db);
                $sale->setCardNumber_sal($misc['cardNumber']);
                if($sale->getDataByCardNumber()) {
                    $misc['textForEmail']="Informaci&oacute;n de la tarjeta";
                    $misc['cardInfo']=true;
                    $misc['emission']=$sale->getEmissionDate_sal();
                    $misc['begin']=$sale->getBeginDate_sal();
                    $misc['end']=$sale->getEndDate_sal();
                    $misc['status']=$sale->getStatus_sal();


                    $counter = new Counter($db, $sale->getSerial_cnt());
                    $counter->getData();

                    $userByDealer = new UserByDealer($db);
                    $userByDealer->setSerial_dea($counter->getSerial_dea());
                    $userByDealer->getData();
                    $user = new User($db,$userByDealer->getSerial_usr());
                    $user->getData();
                    $misc['email'] = array($user->getEmail_usr());
                } else {
                    $managerByCountry = new ManagerbyCountry($db);
                    $managerInfo = $managerByCountry->getManagerByCountry($temporary_customer->getSerial_cou());
                    $misc['email'] = array();
                    foreach ($managerInfo as $m) {
                        array_push($misc['email'], $m['contact_email_man']);
                    }
                    $misc['textForEmail']="La tarjeta #".$misc['cardNumber']." no existe en el sistema.";
                }
                /*END E-MAIL INFO*/

                $temporary_customer->insert();
                // GlobalFunctions::sendMail($misc, 'newTemporaryCustomer');
            } else { /*INFORMATION E-MAIL INFO*/
                $counter = new Counter($db,$sale->getSerial_cnt());
                $counter->getData();
                $dealer = new Dealer($db,$counter->getSerial_dea());
                $dealer->getData();

                $misc['cardNumber']=$_POST['hdnCardNumber'];
                $misc['emission']=$sale->getEmissionDate_sal();
				$misc['status']=$sale->getStatus_sal();
                $invoice = new Invoice($db, $sale->getSerial_inv());
                $invoice->getData();
                $payment = new Payment($db, $invoice->getSerial_pay());
                $payment->getData();
                $misc['payment']=$payment->getDate_pay();
                $misc['serialFile']=$file->getSerial_fle();
                $misc['customer']=$customer->getFirstname_cus()." ".$customer->getLastname_cus();
                $productByDealer = new ProductByDealer($db, $sale->getSerial_pbd());
                $productByDealer->getData();
                $productByCountry = new ProductByCountry($db, $productByDealer->getSerial_pxc());
                $productByCountry->getData();
                $productByLanguage = new ProductbyLanguage($db);
                $productByLanguage->setSerial_pro($productByCountry->getSerial_pro());
                $productByLanguage->setSerial_lang($_SESSION['serial_lang']);
                $productByLanguage->getDataByProductAndLanguage();
                $misc['product'] = $productByLanguage->getName_pbl();
                $user = new User($db,$_SESSION['serial_usr']);
                $user->getData();
                $misc['user']= $user->getFirstname_usr()." ".$user->getLastname_usr();
                $misc['dealer']=$dealer->getName_dea();
                $misc['date']=$file->getCreation_date_fle();
                $misc['time']= date('H:i:s');


                $misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s o ingrese a <a href='".URL."' target='blank'>PAS</a>.";
                $misc['email'] = array();
                /*END INFORMATION E-MAIL INFO*/

                //E-MAIL LIST
                $parameter=new Parameter($db, '21'); //Parameters for Liquidation Permitions.
                $parameter->getData();
                $liquidationUsers=unserialize($parameter->getValue_par());

                $usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);

                if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
                        $usersCountry='62';
                }
                $liquidationUsers[$usersCountry]=explode(',', $liquidationUsers[$usersCountry]);

                //Assistance responsible contact
                if(is_array($liquidationUsers[$usersCountry])){
                    foreach ($liquidationUsers[$usersCountry] as $key=>$p) {
                        $user = new User($db,$p);
                        $user->getData();
                        $misc['email'][$key] = $user->getEmail_usr();
                    }
                }

                //Manager by country contact
                $sector = new Sector($db,$dealer->getSerial_sec());
                $sector->getData();
                $city = new City($db, $sector->getSerial_cit());
                $city->getData();
                $managerByCountry = new ManagerbyCountry($db, $dealer->getSerial_mbc());
				$managerByCountry->getData();
				$manager=new Manager($db, $managerByCountry->getSerial_man());
				$manager->getData();
				array_push($misc['email'], $manager->getContact_email_man());

                //Assistance contact
                if($dealer->getEmail_assistance_dea()) {
                    array_push($misc['email'], $dealer->getEmail_assistance_dea());
                }

                //Dealer responsible contact
                $userByDealer = new UserByDealer($db);
                $userByDealer->setSerial_dea($dealer->getSerial_dea());
                $userByDealer->getData();
                $contactUser = new User($db, $userByDealer->getSerial_usr());
                $contactUser->getData();
                if($contactUser->getEmail_usr()) {
                    array_push($misc['email'], $contactUser->getEmail_usr());
                }

                //Comissionist by country contact
                $comissionist = new ComissionistByCountry($db);
                $comissionistByCou = $comissionist->getActiveComissionistsByCountry($city->getSerial_cou());
                $contactUser = new User($db, $comissionistByCou['serial_usr']);
                $contactUser->getData();
                if($contactUser->getEmail_usr()) {
                    array_push($misc['email'], $contactUser->getEmail_usr());
                }
				
                if(!GlobalFunctions::sendMail($misc, 'newAssistance')){
                    http_redirect('modules/assistance/fEditAssistance/4');
                }
            }

            
            http_redirect('modules/assistance/fEditAssistance/1');
        }else {
            http_redirect('modules/assistance/fEditAssistance/2');
        }
    }else {
        http_redirect('modules/assistance/fEditAssistance/2');
    }
} else {
    http_redirect('modules/assistance/fEditAssistance/2');
}
?>