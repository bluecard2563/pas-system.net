<?php
/*
File: fDispatchAssistanceClaims
Author: David Bergmann
Creation Date: 11/06/2010
Last Modified: 16/06/2010
Modified By: David Bergmann
*/

Request::setInteger('0:error');

/*LIQUIDATION PERMISSIONS*/
$parameter=new Parameter($db, '21'); //Parameters for Stand-by Requests.
$parameter->getData();
$permitedUsers=unserialize($parameter->getValue_par());

$usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);

if($usersCountry=='1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
        $usersCountry='62';
}
$permitedUsers[$usersCountry]=explode(',', $permitedUsers[$usersCountry]);

foreach ($permitedUsers[$usersCountry] as $p) {
    if($_SESSION['serial_usr'] == $p) {
        $permission = $_SESSION['serial_usr'];
    }
}

if(!$permission) {
    http_redirect('main/dealers/1');
}
/*END LIQUIDATION PERMISSIONS*/

$documentToList = DocumentsByFile::getDocumentToList($db);
$providerList = ServiceProvider::getProvidersWithPendingClaims($db);


$zoneList=PaymentRequest::getZonesWithPendingClaims($db);

$smarty->register('error,documentToList,global_billTo,providerList,zoneList');
$smarty -> display();
?>