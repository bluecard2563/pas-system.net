<?php
/*
File: pLiquidateAssistance
Author: David Bergmann
Creation Date: 05/05/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:serial_fle');

$file = new File($db,$serial_fle);
$file->getData();
$file->setStatus_fle("closed");
$file->setClosing_usr($_SESSION['serial_usr']);

if($file->update()) {
    http_redirect('modules/assistance/fEditAssistance/3/'.$serial_fle);
}
?>