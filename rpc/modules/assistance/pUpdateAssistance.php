<?php
/*
File: pUpdateAssistance
Author: David Bergmann
Creation Date: 20/04/2010
Last Modified: 11/05/5010
Modified By: David Bergmann
*/

$_SESSION['card_number'] = $_POST['hdnCardNumberEdit'];

$file = new File($db,$_POST['hdnSerialFleEdit']);
$file->getData();
$file->setSerial_cit($_POST['selCityEdit']);
$file->setDiagnosis_fle($_POST['txtDiagnosisEdit']);
$file->setCause_fle($_POST['txtCauseEdit']);
$file->setType_fle($_POST['txtAssistanceTypeEdit']);

if($file->update()) {
	//Update services medical type
	if($_POST['selMedicalTypeEdit']) {
		$file->updateFileMedicalType($_POST['selMedicalTypeEdit']);
	}


    $customer = new Customer($db,$file->getSerial_cus());
    $customer->getData();
    $customer->setMedical_background_cus($_POST['txtMedicalBackgroundEdit']);
    $customer->update();
    
    ERP_logActivity('update', $customer);//ERP ACTIVITY
    
    $blog = new Blog($db);
    $blog->setSerial_fle($_POST['hdnSerialFleEdit']);
    $blog->setType_blg("CLIENT");
    $blog->setEntry_blg(nl2br($_POST['txtClientBlogEdit']));
    if($blog->update()) {
        $blog->setType_blg("OPERATOR");
        $blog->setEntry_blg(nl2br($_POST['txtOperatorBlogEdit']));
        if($blog->update()) {
            http_redirect('modules/assistance/fEditAssistance/3');
        }else {
            http_redirect('modules/assistance/fEditAssistance/2');
        }
    }else {
        http_redirect('modules/assistance/fEditAssistance/2');
    }
} else {
    http_redirect('modules/assistance/fEditAssistance/2');
}
?>