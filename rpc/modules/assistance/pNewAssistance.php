<?php
/*
File: pNewAssistance
Author: David Bergmann
Creation Date: 29/03/2010
Last Modified: 01/04/2010
Modified By: David Bergmann
*/
ini_set('memory_limit', '512M');
ini_set('max_execution_time','3600');

$_SESSION['card_number'] = $_POST['txtCard'];
if(Sales::cardNumberExists($db, $_POST['txtCard'])) {
    $sales = new Sales($db);
    $sales->setCardNumber_sal($_POST['txtCard']);
    $sales->getDataByCardNumber();
    if($sales->getStatus_sal() != "ACTIVE") {
        if($sales->getStatus_sal() == "VOID") {
            http_redirect('modules/assistance/fNewAssistance/5');
        } elseif($sales->getStatus_sal() == "DENIED") {
            http_redirect('modules/assistance/fNewAssistance/6');
        } elseif($sales->getStatus_sal() == "REFUNDED") {
            http_redirect('modules/assistance/fNewAssistance/7');
        } else {
            $invoice = new Invoice($db, $sales->getSerial_inv());
            $invoice->getData();
            if($sales->getSerial_inv()!= NULL){
                $invoice = new Invoice($db, $sales->getSerial_inv());
                $invoice->getData();
                //verifies if the invoice is paid
                if($invoice->status_inv == "VOID") {
                    http_redirect('modules/assistance/fNewAssistance/4');
                }
            } else {
                http_redirect('modules/assistance/fNewAssistance/2');
            }
        }
    }
} else {
    http_redirect('modules/assistance/fNewAssistance/1');
}

//Pasa
http_redirect('modules/assistance/fEditAssistance');


?>
