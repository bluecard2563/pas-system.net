<?php
/*
File: fAttendAppliances
Author: David Bergmann
Creation Date: 10/06/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$user = new User($db,$_SESSION['serial_usr']);
$user->getData();
$city = new City($db, $user->getSerial_cit());
$city->getData();

$apliances = TemporaryCustomer::getTemporaryCustomersByCountry($db, $city->getSerial_cou());
$titles = array('Tarjeta #','Solicitud #','Nombre de Agencia','Cliente','Documento','Tel&eacute;fono','Pa&iacute;s destino','Problema','Comentarios');

$smarty->register('error,apliances,titles');
$smarty -> display();
?>