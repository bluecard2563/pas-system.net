<?php
/*
File: fEditAssistance
Author: David Bergmann
Creation Date: 29/03/2010
Last Modified: 11/05/2010
Modified By: David Bergmann
*/
Request::setInteger('0:error');
Request::setInteger('1:serial_fle');
Request::setInteger('2:other_card');

if($_SESSION['card_number']) {
    $card_number = $_SESSION['card_number'];
    //unset($_SESSION['card_number']);
} elseif($serial_fle!="0"){
    $smarty->register('serial_fle');
    $file = new File($db, $serial_fle);
    $card_number = $file->getCardNumberByFile();
}

if($other_card) {
    $_SESSION['card_number'] = $other_card;
    $smarty->register('other_card');
    $card_number = $other_card;
}

if($card_number) {
    /*LIQUIDATION PERMISSIONS*/
    $parameter=new Parameter($db, '21');
    $parameter->getData();
    $permitedUsers=unserialize($parameter->getValue_par());

    $usersCountry=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
    if($usersCountry=='1'){ //If the user is a PLANETASSIST's user, we set the country to Ecuador 62.
            $usersCountry='62';
    }
    $permitedUsers[$usersCountry]=explode(',', $permitedUsers[$usersCountry]);
    /*USERS WITH LIQUIDATION PERMISSION*/
    foreach ($permitedUsers[$usersCountry] as $p) {
        if($_SESSION['serial_usr'] == $p) {
            $liquidation_permission = $_SESSION['serial_usr'];
            $smarty->register('liquidation_permission');
        }
    }

    $sale = new Sales($db);
    $sale->setCardNumber_sal($card_number);
    $sale->getDataByCardNumber();
    $serial_sal = $sale->getSerial_sal();

    if($serial_sal) {
        $smarty->register('serial_sal');
    } else {
        $travelerLog = new TravelerLog($db);
        $travelerLog->getDataByCardNumber($card_number);
        $serial_trl = $travelerLog->getSerial_trl();
        $serial_sal = $travelerLog->getSerial_sal();
        $smarty->register('serial_sal,serial_trl');
    }

    /*INVOICE*/
    $invoice = new Invoice($db, $sale->getSerial_inv());
    $invoice->getData();
    if($invoice->status_inv == "STAND-BY") {
        if($invoice->daysToDueDate()<0) {
            $status_inv = 1;
        } else {
            $status_inv = 2;
        }
        
    }

	/*PRODUCT TYPE*/
	$aux = $sale->getAux();
	$name_pbl = $aux['product'];
	$smarty->register('name_pbl');


    /*CARD BENEFICIARIES*/
    $customer = $sale->getCardBeneficiaries();
    $file = new File($db);
    if(is_array($customer)) {
        foreach ($customer as &$c) {
            $c['other_cards'] = $file->getOtherCardFilesByCustomer($serial_sal, $c['serial_cus']);
        }
    }

    /*CARD BENEFITS*/
    $titles = array("#","Beneficio","Condiciones","Monto","Restricci&oacute;n");
    $benefits = $sale->getCardBenefits();

    /*CARD SERVICES*/
    $serviceTitles = array("#","Servicio","Monto");
    $servicesByCountryBySale = new ServicesByCountryBySale($db);
    $counter = new Counter($db, $sale->getSerial_cnt());
    $counter->getData();
	$user = new User($db,$counter->getSerial_usr());
	$user->getData();
    $dealer = new Dealer($db, $counter->getSerial_dea());
    $dealer->getData();
    $services = $servicesByCountryBySale->getServicesBySale($serial_sal, $dealer->getDea_serial_dea());
	$aux_data = $dealer->getAuxData_dea();
	$country_dea = new Country($db, $aux_data['serial_cou']);
	$country_dea->getData();

	/*DEALER INFO*/
	$dealer_info = array(
		'name_cnt' => $user->getFirstname_usr().' '.$user->getLastname_usr(),
		'name_dea' => $dealer->getName_dea(),
		'name_cou' => $country_dea->getName_cou()
	);
	$smarty->register('dealer_info');

    /*SALES LOG*/
    $salesLog = new SalesLog($db);
    $salesHistory = $salesLog->getAuthorizadSalesLogBySale($sale->getSerial_sal());
    $sl_titles = array("#","Fecha","Tipo","Estado");
    if(is_array($salesHistory)) {
        $smarty->register('salesHistory,sl_titles,global_sales_log_status');
    }

	

    /*CARD FILES*/
    $files = new File($db);
    $fileList['allFiles'] = $files->getFilesByCard($card_number,"closed","!=");
    $fileList['openFiles'] = $files->getFilesByCard($card_number,"open","=");
    $fileList['closedFiles'] = $files->getFilesByCard($card_number,"closed","=");

    if(is_array($fileList)) {
        $fileTitles = array("#","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia","Abrir");
        $liquidationTitles = array("#","Pa&iacute;s","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia","Liquidar","Auditor&iacute;a M&eacute;dica");
        $smarty->register('fileTitles,liquidationTitles,fileList');
    }


    /*COUNTRIES*/
    $country = new Country($db);
    $countryList = $country->getCountry();

    /*SERVICE TYPES*/
    $servProvider = new ServiceProvider($db);
    $serviceTypes = $servProvider->getAllTypes();

    /*MEDICAL TYPES*/
    $serviceProviderByFile = new ServiceProviderByFile($db);
    $medicalTypes = $serviceProviderByFile->getMedicalTypes();

    /*48 HOURS*/
    if($sale->daysSinceEmission() < 24) {
        $emission = 1;
    }

	if($sale->getStatus_sal() == "EXPIRED") {
		$emission = 2;
	}

    /*CARD APPLIANCES*/
    $temporary_customer = new TemporaryCustomer($db);
    $applianceList = $temporary_customer->getTemporaryCustomersByCard($card_number);
    if(is_array($applianceList)){
        $applianceTitles = array("#","Pa&iacute;s","Cliente","Problema","Expediente","Estado");
        $smarty->register('applianceList,applianceTitles,global_ApplianceStatus');
    }

    /*MEDICAL CHECKUP*/
    $parameter = new Parameter($db,'25');
    $parameter->getData();
    $auditor_array = unserialize($parameter->getValue_par());

    if(is_array($auditor_array)) {
        $country_list=array();
        foreach($auditor_array as $key => $a) {
            $country = new Country($db,$key);
            $country->getData();
            array_push($country_list, array('serial_cou'=>$key, 'name_cou'=>$country->getName_cou()));
        }
        $smarty->register('country_list');
    }
    $smarty->register('error,emission,status_inv,card_number,customer,titles,benefits,serviceTitles,services,countryList,serial_sal,serviceTypes,global_serviceProviderType,medicalTypes,global_medicalType');
    $smarty -> display();
} else {
    http_redirect('modules/assistance/fNewAssistance');
}
?>