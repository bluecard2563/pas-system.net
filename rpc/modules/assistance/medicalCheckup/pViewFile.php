<?php
/*
 * File: pViewFile.php
 * Author: Patricio Astudillo
 * Creation Date: 21/05/2010, 12:26:16 PM
 * Modifies By: David Bergmann
 * Last Modified: 21/05/2010, 03:12:49 PM
 */

Request::setString('txtObservations');
Request::setString('hdnSerial_prq');

$pRequest=new PaymentRequest($db, $hdnSerial_prq);

if($pRequest->getData()){
	$pRequest->setAuditor_comments_prq(specialchars($txtObservations));
	$pRequest->setStatus_prq('AUDITED');
	$pRequest->setAudited_date_prq(date('d/m/Y'));

        /*E-MAIL INFO*/
        $user = new User($db,$pRequest->getSerial_usr());
        $user->getData();
        $misc['userName']=$user->getFirstname_usr()." ".$user->getLastname_usr();
        $misc['serial_fle']=$pRequest->getSerial_fle();
        $misc['email'] = $user->getEmail_usr();
        /*END E-MAIL INFO*/

	if($pRequest->update()){
            if(GlobalFunctions::sendMail($misc,'medicalCheckup')) {
				$error=1;
            } else {
                $error=4;
            }
	}else{
		$error=3;
	}
}else{
	$error=2;
}
http_redirect('modules/assistance/medicalCheckup/fSelectMedicalCheckup/'.$error);
?>