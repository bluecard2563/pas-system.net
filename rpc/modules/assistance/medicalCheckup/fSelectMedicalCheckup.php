<?php
/*
 * File: fSelectMedicalCheckup.php
 * Author: Patricio Astudillo
 * Creation Date: 19/05/2010, 02:32:55 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 19/05/2010, 02:32:55 PM
 */

Request::setString('0:error');
$pendingApplications=PaymentRequest::getPendingMedicalApplications($db, $_SESSION['serial_usr']);

$smarty->register('pendingApplications,error');
$smarty->display();
?>
