<?php
/*
File: pUpdateSector.php
Author: David Bergmann
Creation Date: 07/01/2010
Last Modified:
Modified By:
*/

$sector = new Sector($db);	
$sector -> setSerial_sec($_POST['hdnSerial_sec']);
$sector -> getData();
$data['serial_sec'] = $sector -> getSerial_sec();
$data['serial_cit'] = $sector -> getSerial_cit();
$data['code_sec'] = $_POST['txtCodeSector'];
$sector->setCode_sec($data['code_sec']);
$data['name_sec'] = $_POST['txtNameSector'];
$sector->setName_sec($data['name_sec']);

if($sector -> update()) {
	http_redirect('modules/sector/fSearchSector/1');
}else{
	$error = 1;
}

$smarty->register('error,data');
$smarty->display('modules/sector/fUpdateSector.'.$_SESSION['language'].'.tpl');
?>