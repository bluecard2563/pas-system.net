<?php 
/*
File: pUpdateQuestion.php
Author: Santiago Benítez
Creation Date: 25/01/2010 16:00
Last Modified: 25/01/2010 16:00
Modified By: Santiago Benítez
*/

Request::setInteger('serial_qbl');

$questionbl=new QuestionByLanguage($db,$serial_qbl);
if($questionbl->getData()){

	$questionbl->setText_qbl($_POST['txtTextQuestion']);
	if($questionbl->update()){
		http_redirect('modules/question/fSearchQuestion/2');
	}else{
		http_redirect('modules/question/fSearchQuestion/4');
	}
}


?>