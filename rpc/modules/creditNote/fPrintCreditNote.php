<?php
/*
 * File: fPrintCreditNote.php
 * Author: Patricio Astudillo
 * Creation Date: 30/04/2010, 08:34:12 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 30/04/2010, 08:34:12 AM
 */

Request::setString('0:error');
/*GET THE CREDIT NOTE DOCUMENTS IN A SPECIFIC COUNTRY*/
$country=new Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);

$smarty->register('countryList');
$smarty->register('error');
$smarty->display();
?>
