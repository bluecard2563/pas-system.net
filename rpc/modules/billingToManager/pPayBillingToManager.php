<?php
/*
File: pPayBillingToManager
Author: David Bergmann
Creation Date: 11/10/2010
Last Modified:
Modified By:
*/

set_time_limit(36000);
ini_set('memory_limit','512M');
$billingToManager = new BillingToManager($db, $_POST['hdnSerial_btm']);
$billingToManager->getData();

if(!Sales::payInternationaFee($db, $_POST['selManager'], $_POST['hdnSerial_btm'])){
	http_redirect('modules/billingToManager/fPayBillingToManager/2');
}

$billingToManager->setStatus_btm('PAID');

if($billingToManager->update()) {
	http_redirect('modules/billingToManager/fPayBillingToManager/1');
} else {
	http_redirect('modules/billingToManager/fPayBillingToManager/2');
}
?>