<?php 
/*
File: fSearchDealType.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 14:59
Last Modified: Patricio Astudillo M.
Modified By: 06/01/2010
*/

Request::setInteger('0:error');

$language = new Language($db);
$languageList = $language->getLanguages();

$smarty->register('error,languageList');
$smarty->display();
?>