<?php 
/*
File: fUpdateDealerType.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 15:00
Last Modified: 25/01/2010
Modified By: David Bergmann
*/

Request::setString('hdnDealTypeID');

if($hdnDealTypeID) {
	$language = new Language($db);
	$languageList = $language->getRemainingDealerTypeLanguages($hdnDealTypeID);
	
	$dealerType=new DealerType($db);
	$dealerType->setSerial_dlt($hdnDealTypeID);
	$dealerType->getData();
	$status_dlt=$dealerType->getStatus_dlt();
	$data=$dealerType->getDealerTypes($hdnDealTypeID);
	$titles=array('#','Alias','Nombre','Idioma','Actualizar');
	$text="Nueva traducci&oacute;n";
}
else {
	http_redirect('modules/dealer/dealerType/fSearchDealerType/3');
}

$smarty->register('data,titles,text,hdnDealTypeID,languageList,status_dlt');
$smarty->display();
?>