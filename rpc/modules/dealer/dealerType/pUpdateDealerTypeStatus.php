<?php 
/*
File: pUpdateDealerTypeStatus.php
Author: David Bergmann
Creation Date: 27/01/2010
Last Modified: 
Modified By: 
*/

$dealerT=new DealerType($db);
$dealerT->setSerial_dlt($_POST['hdnSerialDealerType']);

if($dealerT->getData()){
	$dealerT->setStatus_dlt($_POST['rdoStatus']);
	
	if($dealerT->updateDealerTypeStatus()){
		http_redirect('modules/dealer/dealerType/fSearchDealerType/1');
	}
}

http_redirect('modules/dealer/dealerType/fSearchDealerType/4');
?>