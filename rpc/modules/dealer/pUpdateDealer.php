<?php 
/*
File: pUpdateDealer.php
Author: Patricio Astudillo M.
Creation Date: 13/01/2009 08:50 am
Last Modified: 24/06/2010
Modified By: David Bergmann
*/

$dealer=new Dealer($db,$_POST['serial_dea']);

if($dealer->getData()){
        /*NOTIFICATION MAIL INFO*/
        $code = Dealer::getDealerCode($db, $dealer->getSerial_dea());
        $misc['textForEmail']="Se ha actualizado la siguiente informaci&oacute;n del comercializador <b>".$dealer->getName_dea()."</b> cod : <u>".$code."</u><br><br>";
        $misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
        $misc['titles'] = array();
        if($dealer->getSerial_sec() != $_POST['hdnSerialSec']) {
            $dealer->setSerial_sec($_POST['hdnSerialSec']);
            $sector = new Sector($db, $_POST['hdnSerialSec']);
            $sector->getData();
            $misc['data']['sector'] = $sector->getName_sec();
            array_push($misc['titles'], "Sector");
        }
        if($_POST['hdnSerialMbc']) {
            if($dealer->getSerial_mbc() != $_POST['hdnSerialMbc']) {
                $dealer->setSerial_mbc($_POST['hdnSerialMbc']);
                $managerByCountry = new ManagerbyCountry($db, $_POST['hdnSerialMbc']);
                $managerByCountry->getData();
                $manager = new Manager($db, $managerByCountry->getSerial_man());
                $manager->getData();
                $misc['data']['manager'] = $manager->getName_man();
                $misc['serial_mbc'] = $_POST['hdnSerialMbc'];
                array_push($misc['titles'], "Representante");
                $misc['managerMail'] = $manager->getContact_email_man();
            }
        } else {
            $dealer->setSerial_mbc(1);
        }
        if($dealer->getSerial_cdd() != $_POST['selCreditD']) {
            $dealer->setSerial_cdd($_POST['selCreditD']);
            $creditDay = new CreditDay($db, $_POST['selCreditD']);
            $creditDay->getData();
            $misc['data']['creditDay'] = $creditDay->getDays_cdd();
            array_push($misc['titles'], "D&iacute;as de cr&eacute;dito");
        }
        if($dealer->getSerial_dlt() != $_POST['selDealerT']) {
            $dealer->setSerial_dlt($_POST['selDealerT']);
            $dealerType = new DealerType($db, $_POST['selDealerT']);
            $dealerType->getData();
            $dealerType->getDealerTypeByLanguage($_SESSION['serial_lang']);
            $misc['data']['dealerType'] = $dealerType->getDescription_dtl();
            array_push($misc['titles'], "Tipo");
        }
	if($dealer->getId_dea() != $_POST['txtID']) {
            $dealer->setId_dea($_POST['txtID']);
            $misc['data']['id'] = $_POST['txtID'];
            array_push($misc['titles'], "Id");
        }
        if($dealer->getName_dea() != $_POST['txtName']) {
            $dealer->setName_dea($_POST['txtName']);
            $misc['data']['name'] = $_POST['txtName'];
            array_push($misc['titles'], "Nombre");
        }
        if($dealer->getCategory_dea() != $_POST['selCategory']) {
            $dealer->setCategory_dea($_POST['selCategory']);
            $misc['data']['category'] = $_POST['selCategory'];
            array_push($misc['titles'], "Categor&iacute;a");
        }
        if($dealer->getAddress_dea() != $_POST['txtAddress']) {
            $dealer->setAddress_dea($_POST['txtAddress']);
            $misc['data']['address'] = $_POST['txtAddress'];
            array_push($misc['titles'], "Direcci&oacute;n");
        }
        if($dealer->getPhone1_dea() != $_POST['txtPhone1']) {
            $dealer->setPhone1_dea($_POST['txtPhone1']);
            $misc['data']['phone1'] = $_POST['txtPhone1'];
            array_push($misc['titles'], "Tel&eacute;fono #1");
        }
	if($dealer->getPhone2_dea() != $_POST['txtPhone2']) {
            $dealer->setPhone2_dea($_POST['txtPhone2']);
            $misc['data']['phone2'] = $_POST['txtPhone2'];
            array_push($misc['titles'], "Tel&eacute;fono #2");
        }
        if($dealer->getContract_date_dea() != $_POST['txtContractDate']){
            $dealer->setContract_date_dea($_POST['txtContractDate']);
            $misc['data']['contractDate'] = $_POST['txtContractDate'];
            array_push($misc['titles'], "Fecha de contrato");
        }
        if($dealer->getFax_dea() != $_POST['txtFax']){
            $dealer->setFax_dea($_POST['txtFax']);
            $misc['data']['fax'] = $_POST['txtFax'];
            array_push($misc['titles'], "Fax");
        }
        if($dealer->getEmail1_dea() != $_POST['txtMail1']){
            $dealer->setEmail1_dea($_POST['txtMail1']);
            $misc['data']['email1'] = $_POST['txtMail1'];
            array_push($misc['titles'], "E-mail #1");
        }
        if($dealer->getEmail2_dea() != $_POST['txtMail2']){
            $dealer->setEmail2_dea($_POST['txtMail2']);
            $misc['data']['email2'] = $_POST['txtMail2'];
            array_push($misc['titles'], "E-mail #2");
        }
        if($dealer->getContact_dea() != $_POST['txtContact']){
            $dealer->setContact_dea($_POST['txtContact']);
            $misc['data']['contact'] = $_POST['txtContact'];
            array_push($misc['titles'], "Nombre del Contacto");
        }
        if($dealer->getPhone_contact_dea() != $_POST['txtContactPhone']){
            $dealer->setPhone_contact_dea($_POST['txtContactPhone']);
            $misc['data']['phoneContact'] = $_POST['txtContactPhone'];
            array_push($misc['titles'], "Tel&eacute;fono del Contacto");
        }
        if($dealer->getEmail_contact_dea() != $_POST['txtContactMail']){
            $dealer->setEmail_contact_dea($_POST['txtContactMail']);
            $misc['data']['emailContact'] = $_POST['txtContactMail'];
            array_push($misc['titles'], "E-mail del Contacto");
        }
        if($dealer->getPercentage_dea() != $_POST['txtPercentage']){
            $dealer->setPercentage_dea($_POST['txtPercentage']);
            $misc['data']['percentage'] = $_POST['txtPercentage'];
            array_push($misc['titles'], "Porcentaje");
        }
        if($dealer->getType_percentage_dea() != $_POST['selType']){
            $dealer->setType_percentage_dea($_POST['selType']);
            $misc['data']['percentageType'] = $global_pType[$_POST['selType']];
            array_push($misc['titles'], "Tipo de porcentaje");
        }
        if($dealer->getStatus_dea() != $_POST['selStatus']){
            $dealer->setStatus_dea($_POST['selStatus']);
            $misc['data']['status'] = $global_status[$_POST['selStatus']];
            array_push($misc['titles'], "Estado");
        }
        if($dealer->getPayment_deadline_dea() != $_POST['selPayment']){
            $dealer->setPayment_deadline_dea($_POST['selPayment']);
            $misc['data']['payDeadline'] = $global_weekDays[$_POST['selPayment']];
            array_push($misc['titles'], "D&iacute;a de Pago");
        }
        if($dealer->getBill_to_dea() != $_POST['chkBillTo']){
            $dealer->setBill_to_dea($_POST['chkBillTo']);
            $misc['data']['bill_to_dea'] = $global_billTo[$_POST['chkBillTo']];
            array_push($misc['titles'], "Factura a");
        }
        if($dealer->getVisits_number_dea() != $_POST['txtVisitAmount']){
            $dealer->setVisits_number_dea($_POST['txtVisitAmount']);
            $misc['data']['visitNumber'] = $_POST['txtVisitAmount'];
            array_push($misc['titles'], "Visitas mensuales");
        }
        if($dealer->getPay_contact_dea() != $_POST['txtContactPay']){
            $dealer->setPay_contact_dea($_POST['txtContactPay']);
            $misc['data']['payContact'] = $_POST['txtContactPay'];
            array_push($misc['titles'], "Contacto Pago");
        }
        if($dealer->getAssistance_contact_dea() != $_POST['txtAssistName']){
            $dealer->setAssistance_contact_dea($_POST['txtAssistName']);
            $misc['data']['assistanceContact'] = $_POST['txtAssistName'];
            array_push($misc['titles'], "Nombre de asistencias");
        }
        if($dealer->getEmail_assistance_dea() != $_POST['txtAssistEmail']){
            $dealer->setEmail_assistance_dea($_POST['txtAssistEmail']);
            $misc['data']['emailAssistance'] = $_POST['txtAssistEmail'];
            array_push($misc['titles'], "E-mail de asistencias");
        }
        if($dealer->getBonus_to_dea() != $_POST['chkBonusTo']){
            $dealer->setBonus_to_dea($_POST['chkBonusTo']);
            $misc['data']['bonus_to_dea'] = $global_bonusTo[$_POST['chkBonusTo']];
            array_push($misc['titles'], "Incentivo a");
        }

		if($dealer->getManager_name_dea() != $_POST['txtNameManager']){
            $dealer->setManager_name_dea($_POST['txtNameManager']);
            $misc['data']['manager_name'] = $_POST['txtNameManager'];
            array_push($misc['titles'], "Nombre del Gerente");
        }
		if($dealer->getManager_phone_dea() != $_POST['txtPhoneManager']){
            $dealer->setManager_phone_dea($_POST['txtPhoneManager']);
            $misc['data']['manager_phone'] = $_POST['txtPhoneManager'];
            array_push($misc['titles'], "Tel&eacute;fono del Gerente");
        }
		if($dealer->getManager_email_dea() != $_POST['txtMailManager']){
            $dealer->setManager_email_dea($_POST['txtMailManager']);
            $misc['data']['manager_email'] = $_POST['txtMailManager'];
            array_push($misc['titles'], "E-mail del Gerente");
        }
		if($dealer->getManager_birthday_dea() != $_POST['txtBirthdayManager']){
            $dealer->setManager_birthday_dea($_POST['txtBirthdayManager']);
            $misc['data']['manager_birthday'] = $_POST['txtBirthdayManager'];
            array_push($misc['titles'], "Fecha de Nacimiento del Gerente");
        }

		$user_by_dealer = new UserByDealer($db);
        $user_by_dealer->setSerial_dea($dealer->getSerial_dea());
        $user_by_dealer->getData();
        $user = new User($db, $user_by_dealer->getSerial_usr());
        $user->getData();
        $misc['comissionistMail'] = $user->getEmail_usr();

        /*END NOTIFICATION MAIL INFO*/
	
	if($dealer->update()){
		ERP_logActivity('update', $dealer);//ERP ACTIVITY
		
            /*SENDS MAIL*/
            if(is_array($misc['data'])) {
                if(!GlobalFunctions::sendMail($misc, 'updateDealer')){
                    http_redirect('modules/dealer/fSearchDealer/9');
                }
            }
            $error=2;
            $user_by_dealer = new UserByDealer($db);
            $user_by_dealer->setSerial_dea($_POST['serial_dea']);
			$user_by_dealer->setStatus_ubd("ACTIVE");
            $user_by_dealer->getData();

			$branches = new Dealer($db);
			$branchesByDealer = $branches -> getDealers($params);
			foreach($branchesByDealer as $bl) {
				$branches->setSerial_dea($bl[serial_dea]);
				$branches->getData();
				$branches->setBonus_to_dea($_POST['chkBonusTo']);
				$branches->update();
			}

            //If the user changes it`s deactivated and the new one is inserted
            if($user_by_dealer->getSerial_usr()!=$_POST['hdnSerialUsr']) {
				$user = new User($db, $_POST['hdnSerialUsr']);
				$user->getData();
                $user_by_dealer->deactivateUser();
                $user_by_dealer->setSerial_usr($_POST['hdnSerialUsr']);
                $user_by_dealer->setStatus_ubd("ACTIVE");
				$user_by_dealer->setPercentage_ubd($user->getCommission_percentage_usr());
                if(!$user_by_dealer->insert()) {
                    $error=4;
                }
                
                //Gets the branches from this dealer
                $branches = new Dealer($db);
                $params=array('0' => array('field'=>'d.dea_serial_dea',
                                  'value'=> $_POST['serial_dea'],
                                  'like'=> 1));
                $branchList = $branches -> getDealers($params);
				
                //Does the same for each branch
                foreach($branchList as $bl) {
					$user = new User($db, $_POST['hdnSerialUsr']);
					$user->getData();
                    $user_by_dealer = new UserByDealer($db);
                    $user_by_dealer->setSerial_dea($bl[serial_dea]);
                    $user_by_dealer->getData();
                    //If the user changes it`s deactivated and the new one is inserted
                    if($user_by_dealer->getSerial_usr()!=$_POST['hdnSerialUsr']) {
                        $user_by_dealer->deactivateUser();
                        $user_by_dealer->setSerial_usr($_POST['hdnSerialUsr']);
                        $user_by_dealer->setStatus_ubd("ACTIVE");
						$user_by_dealer->setPercentage_ubd($user->getCommission_percentage_usr());
                        if(!$user_by_dealer->insert()) {
                            $error=5;
                        }
                    }					
                }
            }
	}
        else {
            $error=1;
        }
        http_redirect('modules/dealer/fSearchDealer/'.$error);

}

/*RETRIEVING DEFAULT DATA*/
//Zones
$zone=new Zone($db);
$zoneList=$zone->getZones();

//Dealer Types
$dealerT=new DealerType($db);
$dTypesList=$dealerT->getDealerTypes();

//CATEGORY
$categoryList=$dealer->getCategories();

//STATUS
$statusList=$dealer->getStatusList();

//PAYMENT DEDLINE
$deadlineList=$dealer->getDedline();

//DISCOUNT/COMISSION
$typeList=$dealer->getPercentageType();
/*END DEFAULT DATA*/

$dealer->getData();
$data['serial_dea']=$serial_dea;
$data['serial_usr']=$dealer->getSerial_sec();
$data['serial_sec']=$dealer->getSerial_sec();
$data['serial_mbc']=$dealer->getSerial_mbc();
$data['serial_cdd']=$dealer->getSerial_cdd();
$data['serial_cdd']=$dealer->getSerial_cdd();
$data['serial_dlt']=$dealer->getSerial_dlt();
$data['status_dea']=$dealer->getStatus_dea();
$data['id_dea']=$dealer->getId_dea();
$data['code_dea']=$dealer->getCode_dea();
$data['name_dea']=$dealer->getName_dea();
$data['category_dea']=$dealer->getCategory_dea();
$data['address_dea']=$dealer->getAddress_dea();
$data['phone1_dea']=$dealer->getPhone1_dea();
$data['phone2_dea']=$dealer->getPhone2_dea();
$data['contract_date']=$dealer->getContract_date_dea();
$data['fax_dea']=$dealer->getFax_dea();
$data['email1_dea']=$dealer->getEmail1_dea();
$data['email2_dea']=$dealer->getEmail2_dea();
$data['contact_dea']=$dealer->getContact_dea();
$data['phone_contact']=$dealer->getPhone_contact_dea();
$data['email_contact']=$dealer->getEmail_contact_dea();
$data['percentage_dea']=$dealer->getPercentage_dea();
$data['type_dea']=$dealer->getType_percentage_dea();
$data['payment_dedline']=$dealer->getPayment_deadline_dea();
$data['bill_to']=$dealer->getBill_to_dea();
$data['dea_serial_dea']=$dealer->getDea_serial_dea();
$data['pay_contact']=$dealer->getPay_contact_dea();
$data['assistance_contact']=$dealer->getAssistance_contact_dea();
$data['email_assistance']=$dealer->getEmail_assistance_dea();
$data['branch_number']=$dealer->getBranch_number_dea();
$data['visits_number']=$dealer->getVisits_number_dea();
$data['bonus_to_dea']=$dealer->getBonus_to_dea();
$data['aux']=$dealer->getAuxData_dea();
$error=1;

$smarty->register('error,dTypesList,zoneList,statusList,deadlineList,categoryList,typeList,data');
$smarty->display('modules/dealer/fUpdateDealer.'.$_SESSION['language'].'.tpl');

?>