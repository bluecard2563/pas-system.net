<?php 
/*
File: fUpdateCountry.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified:10/02/2010
Modified By:  David Bergmann
*/
Request::setInteger('0:error');	
//Request::setString('hdnSerial_cou');
$serial_cou=$_POST['selCountry'];
$zone=new Zone($db);
$nameZoneList=$zone->getZones();

$country = new Country($db);
$country -> setSerial_cou($serial_cou);
$language=new Language($db);
$languageList=$language->getLanguages();
if($country -> getData()){
	$data['serial_cou'] = $country -> getSerial_cou();
        $data['serial_lang'] = $country -> getSerial_lang();
	$data['serial_zon'] = $country -> getSerial_zon();
	$data['code_cou'] = $country -> getCode_cou();
	$data['name_cou'] = $country -> getName_cou();
	$data['flag_cou'] = $country -> getFlag_cou();
	$data['card_cou'] = $country -> getCard_cou();
	$data['banner_cou'] = $country -> getBanner_cou();
        $data['phone_cou'] = $country -> getPhone_cou();
}else{	
	http_redirect('modules/country/fSearchCountry/2');
}
$smarty -> register('data,languageList,nameZoneList');
$smarty->display();
?>