<?php
/*
File: pUpdatePotentialCustomer
Author: Esteban Angulo
Creation Date: 14/07/2010
*/
//Debug::print_r($_POST);
//die();
$potentialCus=new PotentialCustomers($db,$_POST['serial_pcs']);
if($potentialCus->getData()){
	$potentialCus->setSerial_cit($_POST['selCity']);
	$potentialCus->setSerial_usr($_SESSION['serial_usr']);
	$potentialCus->setFirstname_pcs($_POST['txtFirstName']);
	$potentialCus->setLastname_pcs($_POST['txtLastName']);
	$potentialCus->setDocument_pcs($_POST['txtDocument']);
	$potentialCus->setPhone1_pcs($_POST['txtPhone']);
	$potentialCus->setEmail_pcs($_POST['txtMail']);
	$potentialCus->setBirthdate_pcs($_POST['txtBirthdate']);
	$potentialCus->setCellphone_pcs($_POST['txtCellphone']);
	$potentialCus->setComments_pcs(specialchars($_POST['comments']));
	$potentialCus->setStatus_pcs($_POST['selStatus']);
}

if($potentialCus->update()){
	$error=1;
}
else{
	$error=2;
}

http_redirect('modules/potentialCustomer/fSearchPotentialCustomer/'.$error);
?>
