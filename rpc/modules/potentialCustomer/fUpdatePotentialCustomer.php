<?php 
/*
File: fUpdatePotentialCustomer.php
Author: Esteban Angulo
Creation Date: 14/07/2010
*/

Request::setInteger('hdnCustomerID');

$potentialcustomer=new PotentialCustomers($db,$hdnCustomerID);
$country=new Country($db);
$city=new City($db);
$countryList=$country->getCountry();
$statusList=$potentialcustomer->getAllStatus();

if($potentialcustomer->getData()){
	$city->setSerial_cit($potentialcustomer->getSerial_cit());
	$city->getData();
	$cityList=$city->getCitiesByCountry($city->getSerial_cou());
	
	$data['serial_pcs']=$hdnCustomerID;
	$data['first_name_pcs']=$potentialcustomer->getFirstname_pcs();
	$data['last_name_pcs']=$potentialcustomer->getLastname_pcs();
	$data['document_pcs']=$potentialcustomer->getDocument_pcs();
	$data['phone1_pcs']=$potentialcustomer->getPhone1_pcs();
	$data['cellphone_pcs']=$potentialcustomer->getCellphone_pcs();
	$data['serial_cou']=$city->getSerial_cou();
	$data['serial_cit']=$potentialcustomer->getSerial_cit();
	$data['birthdate_pcs']=$potentialcustomer->getBirthdate_pcs();
	$data['email_pcs']=$potentialcustomer->getEmail_pcs();
	$data['status_pcs']=$potentialcustomer->getStatus_pcs();
	$data['comments_pcs']=$potentialcustomer->getComments_cus();
}else{
	http_redirect('modules/potentialCustomer/fSearchPotentialCustomer/3');
}
//Debug::print_r($data);
$smarty->register('error,data,statusList,countryList,cityList');
$smarty->display();
?>