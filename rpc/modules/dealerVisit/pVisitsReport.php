<?php
/*
File: pVisitsReport
Author: David Bergmann
Creation Date: 24/03/2010
Last Modified: 26/03/2010
Modified By: David Bergmann
*/
ini_set('memory_limit','256M');
ini_set('max_execution_time','3600');
set_include_path(get_include_path().PATH_SEPARATOR.DOCUMENT_ROOT.'lib/Excel/Classes/');
require_once("PHPExcel.php");
require_once ("PHPExcel/Writer/Excel5.php");
global $global_system_name;

//Objeto PHPExcel
$objPHPExcel = new PHPExcel();

$dealerVisit = new DealerVisit($db);
$user = new User($db,$_SESSION['serial_usr']);
$user->getData();

$params = array();

$params['u.serial_mbc'] = $user->getSerial_mbc();

if($_POST['selUser']){
    $params['u.serial_usr'] = $_POST['selUser'];
    if($_POST['selDealer']){
        $params['d.serial_dea'] = $_POST['selDealer'];
    }
}
if($_POST['selTypeVis']){
    $params['v.type_vis'] = $_POST['selTypeVis'];
}
if($_POST['selStatusVis']){
    $params['v.status_vis'] = $_POST['selStatusVis'];
}
if($_POST['txtBeginDate']){
    $params['v.start_vis'] = $_POST['txtBeginDate'];
}
if($_POST['txtEndDate']){
    $params['v.end_vis'] = $_POST['txtEndDate'];
}

$visitsList = $dealerVisit->getDealerVisitsReport($params);

if(sizeof($visitsList)>0){
    $titulos=array('Nombre del Responsable','Nombre del Comercializador','Fecha de la Visita','Estado de la Visita','Motivo de la Visita','Persona que recibió','Observaciones','Estrategia');

    $nombreArchivo='Listado de Visitas';
    $archivo='Visitas';

    //SET PROPERTIES
    $objPHPExcel->getProperties()->setCreator($global_system_name);
    $objPHPExcel->getProperties()->setTitle("Listado de Visitas");
    $objPHPExcel->getProperties()->setSubject("Visitas");
    $objPHPExcel->getProperties()->setDescription("Informaci&oacute;n General de Visitas Registrados");
    $objPHPExcel->getProperties()->setCategory("Reporte");

    $objPHPExcel->createSheet();
    $objPHPExcel->getActiveSheet()->setTitle('Listado de Visitas');

    $objPHPExcel->getActiveSheet()->mergeCells('F2:I2');
    $objPHPExcel->getActiveSheet()->setCellValue('F2',"Reporte de Visitas Registradas en ".$global_system_name);
    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->getColor()->setARGB('00000000');
    //$objPHPExcel->getActiveSheet()->getStyle('F2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

    $objPHPExcel->getActiveSheet()->mergeCells('G4:I4');
    $objPHPExcel->getActiveSheet()->setCellValue('G4',"Observaciones");
    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('G4')->getFont()->getColor()->setARGB('00000000');
    //$objPHPExcel->getActiveSheet()->getStyle('G4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('G4')->getFill()->getStartColor()->setARGB('FFFFFFFF');

    //DATA WRITE
    $col=1;
    $fila=5;

    foreach($titulos as $t){
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$fila,$t);
            //LETTER COLOR AND ALIGN
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFont()->getColor()->setARGB('00000000');
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //BORDER COLOR
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getTop()->getColor()->setARGB('00C0C0C0');
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getBottom()->getColor()->setARGB('00C0C0C0');
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getLeft()->getColor()->setARGB('00C0C0C0');
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getRight()->getColor()->setARGB('00C0C0C0');
            //BORDER STYLE
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            //CELL BACKGROUND COLOR
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFill()->getStartColor()->setARGB('FFFFFFFF');
            $col++;
    }

    $fila=6;
    $num=1;
    foreach($visitsList as $vl){
        $col=1;
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode($vl['name_usr']));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode($vl['name_dea']));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,$vl['date_vis']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode($global_visit_status[$vl['status_vis']]));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode(html_entity_decode($global_visit_type[$vl['type_vis']])));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode($vl['host_obv']));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode($vl['comments_obv']));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col++,$fila,utf8_encode($vl['strategy_obv']));

        $col=1;
        for($k=0;$k<sizeof($titulos);$k++){
                //Estilo de bordes
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                //Color de Bordes
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getTop()->getColor()->setARGB('00C0C0C0');
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getBottom()->getColor()->setARGB('00C0C0C0');
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getLeft()->getColor()->setARGB('00C0C0C0');
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getBorders()->getRight()->getColor()->setARGB('00C0C0C0');
           if(($fila+2)%2!=0){
                //Color de Fondo de la celda
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFill()->getStartColor()->setARGB('00FFFFFF');
            }else{
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col,$fila)->getFill()->getStartColor()->setARGB('00a5a5a5');
            }
            $col++;
        }
        $fila++;
        $num++;
    }
}


//SAVE FILE
$objWriter = new PHPExcel_Writer_Excel5 ($objPHPExcel);
$objWriter->save('Reporte_'.$archivo.'.xls');

//DOWNLOAD FILE
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=Reporte_".$archivo.".xls" );
readfile("Reporte_".$archivo.".xls");
unlink("Reporte_".$archivo.".xls");
?>