<?php
/*
File: fVisitsReport
Author: David Bergmann
Creation Date: 24/03/2010
Last Modified:
Modified By:
*/
Request::setInteger('0:error');

//BY USER
$user = new User($db, $_SESSION['serial_usr']);
$user->getData();
$comissionistList=$user->getUsersByManager($user->getSerial_mbc());
if(is_array($comissionistList)){
    foreach($comissionistList as &$cl){
            $cl['first_name_usr']=utf8_encode($cl['first_name_usr']);
            $cl['last_name_usr']=utf8_encode($cl['last_name_usr']);
    }
    $smarty->register('comissionistList');
}
//BY TYPE OF VISIT
$visit = new DealerVisit($db);
$typeList = $visit->getTypeList();
$smarty->register('typeList,global_visit_type');
//BY STATUS OF VISIT
$statusList = $visit->getStatusList();
$smarty->register('statusList,global_visit_status');

$smarty -> register('error');
$smarty->display();
?>
