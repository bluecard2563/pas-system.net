<?php
/*
File: fNewDealerVisit.php
Author: David Bergmann
Creation Date: 26/02/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$user = new User($db,$_SESSION['serial_usr']);
$user -> getData();
$dealerVisit = new DealerVisit($db);
//Only MANAGER's users with serial_mbc have acces here
if($user -> getBelongsto_usr() == 'MANAGER' && $user -> getSerial_mbc()!=NULL) {
    //SERIAL-USER
    $serial_user=$_SESSION['serial_usr'];
    //TYPE
    $typeList=$dealerVisit -> getTypeList();
    //STATUS
    $statusList=$dealerVisit -> getStatusList();
    //DEALRS
    $dealer = new Dealer($db);
    $dealerList = $dealer -> getDealersForCalendar($_SESSION['serial_usr']);
    //USERS BY MANAGER
    $user = new User($db,$_SESSION['serial_usr']);
    $user -> getData();
    $comissionistList=$user -> getUsersByManager($user -> getSerial_mbc(),$_SESSION['serial_usr']);

    if(is_array($comissionistList)){
            foreach($comissionistList as &$cl){
                    $cl['first_name_usr']=utf8_encode($cl['first_name_usr']);
                    $cl['last_name_usr']=utf8_encode($cl['last_name_usr']);
            }
    }

    $smarty -> register('error,typeList,global_visit_type,statusList,global_visit_status,comissionistList,serial_user,dealerList');
    $smarty  ->  display();
}
else {
    http_redirect('main/dealers/1');
}
?>