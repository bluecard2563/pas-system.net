<?php
/*
File: pUpdateOver.php
Author: Santiago Borja 
Creation Date: 17/03/2010
*/

	$apliedTo=$_POST['selApliedTo'];
	/*Sets the promo Basic Information*/
	$over=new Over($db);
	$over->setSerial_ove($_POST['serial_ove']);
	$over -> getData();
	$over->setEndDate_ove($_POST['txtEndDate']);
	$over->setBeginDate_ove($_POST['txtBeginDate']);
	$over->setPercentageIncrease_ove($_POST['increasePer']);
	$over->setPercentageComission_ove($_POST['comissionPer']);
	$over->setName_ove($_POST['txtName']);
	$over->setAppliedTo_ove($apliedTo);
	
	$over -> setAllAppliedOversInactive();
	switch($apliedTo){ /*WE insert the details for the over according to it's type*/
        case 'MANAGER':
        	$newManager = $_POST['selManager'];
        	$oldManager = $over -> getOverManager(true);
        	$isOldManager = false;
        	if($oldManager){
	        	foreach($oldManager as $oMan){
		        	if($oMan['serial_mbc'] == $newManager){
		        		$over -> setManagerOverActive($newManager);
		        		$isOldManager = true;
		        		break;
		        	}
	        	}
        	}
        	if(!$isOldManager){
        		$appOver = new AppliedOver($db,NULL,NULL,$newManager,'ACTIVE');
	            $serialApo = $appOver -> insert();
	            if(!$serialApo)http_redirect('modules/overs/fSearchOver/3'); 
	
	            $infoOver = new InfoOver($db,$serialApo,$_POST['serial_ove'],$_POST['txtBeginDate'], $_POST['txtEndDate'],'PENDING','0.0');
	            $serialInfo = $infoOver -> insert();
	            if(!$serialInfo)http_redirect('modules/overs/fSearchOver/4');
        	}
            break;
        case 'DEALER':
        	$oldDealers = $over -> getOverDealers(true);
        	$newDealers = $_POST['dealersTo'];
        	foreach($newDealers as $nDea){
        		$newDealerInserted = false;
        		foreach($oldDealers as $oDea){
        			if($oDea['serial_dea'] == $nDea){
        				$over -> setDealerOverActive($nDea);
        				$newDealerInserted = true;
        			}
        		}
        		if(!$newDealerInserted){//INSERT NEW DEALER
        			$appOver = new AppliedOver($db,NULL,$nDea,NULL,'ACTIVE');
		        	$serialApo = $appOver -> insert();
	            	if(!$serialApo)http_redirect('modules/overs/fSearchOver/3');
	            	
	            	$infoOver = new InfoOver($db,$serialApo,$_POST['serial_ove'],$_POST['txtBeginDate'], $_POST['txtEndDate'],'PENDING','0.0');
		            $serialInfo = $infoOver -> insert();
		            if(!$serialInfo)http_redirect('modules/overs/fSearchOver/4');
        		}
        	}
            break;
    }
    $serialOve = $over -> update();
    if(!$serialOve)http_redirect('modules/overs/fSearchOver/2');
    http_redirect('modules/overs/fSearchOver/1'); 
?>