<?php 
/*
File: fSearchOver.php
Author: Santiago Borja 
Creation Date: 17/03/2010
*/

	Request::setString('hidOverId');
	
	$country=new Country($db);
	$city=new City($db);
	$dealer=new Dealer($db);
	$managerbc=new ManagerbyCountry($db);
	
	$over = new Over($db,$hidOverId);
	$apliedTo=$over->getAppliedType();
	if($over->getData()){
	    $data['serial_ove']=$hidOverId;
	    $data['creationDate']=$over->getCreationDate_ove();
	    $data['beginDate']=$over->getBeginDate_ove();
	    $data['endDate']=$over->getEndDate_ove();
	    $data['status_ove']=$over->getStatus_ove();
	    $data['percentage_increase_ove']=$over->getPercentageIncrease_ove();
	    $data['percentage_comission_ove']=$over->getPercentageComission_ove();
	    $data['name']=$over->getName_ove();
	    $data['appliedTo']=$over->getAppliedTo_ove();
	    
		if($data['appliedTo'] == 'MANAGER'){
	        $managers = $over->getOverManager();
	        $manager = $managers[0];
	        $data['managerCountry'] = $manager['serial_cou'];
	        $data['managerId'] = $manager['serial_mbc'];
	    }//DEALERS DATA IS LOADED ON loadOverInfo.rpc after loading the dealers interface
	    
	    $smarty->register('appliedTo,data,apliedTo');
	    $smarty->display();
	}else{
	    http_redirect('modules/overs/fSearchOver/7');
	}
?>