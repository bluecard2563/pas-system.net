<?php 
/*
File: pAuthorizePrintOvers.php
Author: Santiago Borja 
Creation Date: 22/03/2010
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

$allInfoOvers = array();
foreach($_POST as $key => $postValue){
	if($key != 'btnAuthorize'){
		$tags = str_replace("overPay_", "", $key);
		$idS = split("_",$tags);
		$infoOver = new InfoOver($db,$idS[0],$idS[1]);
		$infoOver -> getData();
		$infoOver -> setStatus_ino('AUTHORIZED');

		if(!$infoOver -> update()){
			http_redirect('modules/overs/fPayOver/2');
		}
		$allInfoOvers[] = $infoOver;
	}
}

//HEADER
$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);
$pdf->ezText($date, 11, array('justification'=>'center'));
$pdf->ezSetDy(-10);
$pdf->ezText('<b>'.utf8_decode("AUTORIZACION DE PAGO DE OVERS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-20);

$allRecords = array();
$i = 1;
foreach($allInfoOvers as $key => $infoOver){
	$serial_apo = $infoOver -> getSerial_apo();
	$serial_ove = $infoOver -> getSerial_ove();
	$record = Over::getFilteredOversToPayResults($db,$serial_apo,$serial_ove);
	$allRecords[] = array('index'=>$i,
						  'name'=>$record['name_app'],
						  'type'=>$global_overAppliedTo[$record['applied_to_ove']],
						  'period'=>$record['begin_date_ino'].' al '.$record['end_date_ino'],
						  'growing'=>$record['percentage_increase_ove'].'%',
						  'total_to_pay'=>'$'.$record['amount_ino']);
	$i++;
}

$pdf->ezTable($allRecords,
		  array('index'=>'<b>No.</b>',
				'name'=>'<b>Sucursal de Comercializador</b>',
				'type'=>'<b>Tipo</b>',
				'period'=>html_entity_decode('<b>Per&iacute;odo</b>'),
				'growing'=>'<b>Porcentaje de Crecimiento</b>',
				'total_to_pay'=>'<b>A Pagar</b>'),
		  '<b>DETALLE DE OVERS LIQUIDADOS</b>',
		  array('showHeadings'=>1,
				'shaded'=>1,
				'showLines'=>2,
				'xPos'=>'center',
				'fontSize' => 9,
				'titleFontSize' => 11,
				'cols'=>array(
					'index'=>array('justification'=>'center','width'=>50),
					'name'=>array('justification'=>'center','width'=>100),
					'type'=>array('justification'=>'center','width'=>80),
					'period'=>array('justification'=>'center','width'=>80),
					'growing'=>array('justification'=>'center','width'=>80),
					'total_to_pay'=>array('justification'=>'center','width'=>60)
					)));

$pdf->ezStream();
?>