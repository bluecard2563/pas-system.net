<?php
/*
File: pNewOver.php
Author: Santiago Borja 
Creation Date: 17/03/2010
*/

	$apliedTo=$_POST['selApliedTo'];
	
	/*Sets the promo Basic Information*/
	$over=new Over($db);
	$over->setEndDate_ove($_POST['txtEndDate']);
	$over->setBeginDate_ove($_POST['txtBeginDate']);
	$over->setStatus_ove('PENDING');
	$over->setPercentageIncrease_ove($_POST['increasePer']);
	$over->setPercentageComission_ove($_POST['comissionPer']);
	$over->setName_ove($_POST['txtName']);
	
	switch($apliedTo){ /*WE insert the details for the over according to it's type*/
        case 'MANAGER':
        	$over->setAppliedTo_ove('MANAGER');
        	$serialOve = $over->insert();
        	if(!$serialOve)http_redirect('modules/overs/fNewOver/2');
        	
            $appOver = new AppliedOver($db,NULL,NULL,$_POST['selManager'],'ACTIVE');
            $serialApo = $appOver -> insert();
            if(!$serialApo)http_redirect('modules/overs/fNewOver/3'); 

            $infoOver = new InfoOver($db,$serialApo,$serialOve,$_POST['txtBeginDate'], $_POST['txtEndDate'],'ACTIVE','0.0');
            $serialInfo = $infoOver -> insert();
            if(!$serialInfo)http_redirect('modules/overs/fNewOver/5');
            break;
        case 'DEALER':
        	$over->setAppliedTo_ove('DEALER');
			$serialOve = $over->insert();
        	if(!$serialOve)http_redirect('modules/overs/fNewOver/2');
        	
        	
        	$dealers = $_POST['dealersTo'];
        	foreach($dealers as $dea){
        		$appOver = new AppliedOver($db,NULL,$dea,NULL,'ACTIVE');
	        	$serialApo = $appOver -> insert();
            	if(!$serialApo)http_redirect('modules/overs/fNewOver/4');
            	
            	$infoOver = new InfoOver($db,$serialApo,$serialOve,$_POST['txtBeginDate'], $_POST['txtEndDate'],'ACTIVE','0.0');
	            $serialInfo = $infoOver -> insert();
	            if(!$serialInfo)http_redirect('modules/overs/fNewOver/6');
        	}
            break;
    }
    http_redirect('modules/overs/fNewOver/1'); 
?>