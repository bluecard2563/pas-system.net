<?php 
/*
File: pActivateListOvers.php
Author: Santiago Borja 
Creation Date: 25/03/2010
*/
	foreach($_POST as $key => $postValue){
		
		if($key != 'btnActivate'){
			$overId = str_replace("overActivate_", "", $key);
			$over = new Over($db, $overId);
			if($over -> getData()){
				$over -> setStatus_ove('ACTIVE');
				if(!$over -> update()){
					http_redirect('modules/overs/fActivateOver/2');
				}
			}
		}
	}
	http_redirect('modules/overs/fActivateOver/1'); 
?>