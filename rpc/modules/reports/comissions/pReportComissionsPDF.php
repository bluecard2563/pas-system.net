<?php
/*
    Document   : pReportComissionsPDF
    Created on : 08-jun-2010, 15:16:31
    Author     : Nicolas
    Description:
       Generate a PDF report of comissions
*/

//Debug::print_r($_POST);
Request::setString('selComissionsTo');
if(!selComissionsTo){
	//die('insuficient Arguments');
	http_redirect('modules/reports/comissions/fSelectComissionsReport');
}
$appliedCom= new AppliedComission($db);
$serial = '';
	switch($selComissionsTo){
		case 'MANAGER':
				Request::setString('selManager');
				$serial=$selManager;
			break;
		case 'RESPONSIBLE':
				Request::setString('selResponsible');
				$serial=$selResponsible;
			break;
		case 'DEALER':
				Request::setString('selBranch');
				$serial=$selBranch;
			break;
	}
Request::setString('selType');
Request::setString('txtStartDate');
Request::setString('txtEndDate');

$apComission=new AppliedComission($db);
$comissionsData=$apComission->getComissionForReport($selComissionsTo,$serial,$selType,$txtStartDate,$txtEndDate);

if($comissionsData){
	$totalSold = 0;$totalRefunded = 0; $comissionToPay = 0;
	
	switch($selComissionsTo){
		case 'MANAGER':
				$manager=new Manager($db,$serial);
				$manager->getData();
				$payTo=$manager->getName_man();
				$payToId=$manager->getDocument_man();
				$payToPhone=$manager->getPhone_man();
				$payToAddress=$manager->getAddress_man();
				foreach($comissionsData as &$cData){
					$to_comission = 0;	$toDiscount = 0;
					$comissionPercentage = ($cData['value_mbc_com']*100)/$cData['total_sal'];
					if($cData['type_com']=='IN'){
						$totalSold+=$cData['value_mbc_com'];						
						$to_comission = $cData['value_mbc_com'];
					}
					else{
						$totalRefunded+=$cData['value_mbc_com'];
						$toDiscount = $cData['value_mbc_com'];
					}
					if(!$cData['card_number_sal']){
						$cData['card_number_sal'] = 'N/A';
					}
					array_push($cData, number_format($comissionPercentage, 2, '.', ''), number_format($to_comission, 2, '.', ''), number_format($toDiscount, 2, '.', '') );
				}
			break;
		case 'RESPONSIBLE':
				$user= new User($db,$serial);
				$user->getData();
				$payTo=$user->getFirstname_usr().' '.$user->getLastname_usr();
				$payToId=$user->getDocument_usr();
				$payToPhone=$user->getPhone_usr();
				$payToAddress=$user->getAddress_usr();
				foreach($comissionsData as &$cData){
					$to_comission = 0;	$toDiscount = 0;
					$comissionPercentage = ($cData['value_ubd_com']*100)/$cData['total_sal'];
					if($cData['type_com']=='IN'){
						$totalSold+=$cData['value_ubd_com'];
						$to_comission = $cData['value_ubd_com'];
					}
					else{
						$totalRefunded+=$cData['value_ubd_com'];
						$toDiscount = $cData['value_ubd_com'];
					}
					if(!$cData['card_number_sal']){
						$cData['card_number_sal'] = 'N/A';
					}
					array_push($cData, number_format($comissionPercentage, 2, '.', ''), number_format($to_comission, 2, '.', ''), number_format($toDiscount, 2, '.', '') );
				}
			break;
		case 'DEALER':
				$dealer=new Dealer($db,$serial);
				$dealer->getData();
				$payTo=$dealer->getName_dea();
				$payToId=$dealer->getId_dea();
				$payToPhone=$dealer->getPhone1_dea();
				$payToAddress=$dealer->getAddress_dea();
				foreach($comissionsData as &$cData){
					$to_comission = 0;	$toDiscount = 0;
					$comissionPercentage = ($cData['value_dea_com']*100)/$cData['total_sal'];
					if($cData['type_com']=='IN'){
						$totalSold+=$cData['value_dea_com'];
						$to_comission = $cData['value_dea_com'];
					}
					else{
						$totalRefunded+=$cData['value_dea_com'];
						$toDiscount = $cData['value_dea_com'];
					}
					if(!$cData['card_number_sal']){
						$cData['card_number_sal'] = 'N/A';
					}
					array_push($cData, number_format($comissionPercentage, 2, '.', ''), number_format($to_comission, 2, '.', ''), number_format($toDiscount, 2, '.', '') );
				}
			break;
	}
}

$comissionToPay=$totalSold-$totalRefunded;
$comissionToPay = number_format($comissionToPay, 2, '.', '');
$totalSold = number_format($totalSold, 2, '.', '');
$totalRefunded = number_format($totalRefunded, 2, '.', '');
//Debug::print_r($comissionsData);

$headerValues=array();
$headerLine1=array('col1'=>'<b>Pagar a:</b>',
				   'col2'=>$payTo,
				   'col3'=>'<b>RUC/CI:</b>',
				   'col4'=>$payToId);
array_push($headerValues,$headerLine1);
$headerLine2=array('col1'=>'<b>Fecha:</b>',
				   'col2'=>date('d/m/Y'),
				   'col3'=>utf8_decode('<b>Teléfono:</b>'),
				   'col4'=>$payToPhone);
array_push($headerValues,$headerLine2);
$headerLine3=array('col1'=>utf8_decode('<b>Comisión a Favor:</b>'),
				   'col2'=>number_format($totalSold, 2, '.', ''),
				   'col3'=>utf8_decode('<b>Dirección:</b>'),
				   'col4'=>utf8_decode($payToAddress));
array_push($headerValues,$headerLine3);
$headerLine4=array('col1'=>utf8_decode('<b>Comisión a Descontar:</b>'),
				   'col2'=>number_format($totalRefunded, 2, '.', ''),
				   'col3'=>'',
				   'col4'=>'');
array_push($headerValues,$headerLine4);
$headerLine5=array('col1'=>utf8_decode('<b>Valor Comisión:</b>'),
				   'col2'=>number_format($comissionToPay, 2, '.', ''),
				   'col3'=>'',
				   'col4'=>'');
array_push($headerValues,$headerLine5);
//Debug::print_r($headerValues);

include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
$pdf->ezText('<b>REPORTE DE COMISIONES</b>',12, array('justification' =>'center'));
$pdf->ezSetDy(-10);
	$pdf->ezTable($headerValues,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>2,
						'xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>100),
							'col2'=>array('justification'=>'left','width'=>250),
							'col3'=>array('justification'=>'left','width'=>100),
							'col4'=>array('justification'=>'left','width'=>305))));


//Debug::print_r($comissionsData);
$pdf->ezSetDy(-10);
$pdf->ezTable($comissionsData,
			  array('card_number_sal'=>'<b># Tarjeta</b>',
					'number_inv'=>'<b># Factura</b>',
					'name_cus'=>'<b>Apellido y Nombre</b>',
					'name_pbl'=>'<b>Producto</b>',
					'begin_date_sal'=>'<b>Inicio</b>',
					'end_date_sal'=>'<b>Fin</b>',
					'days_sal'=>'<b>Tiempo</b>',
					'name_dea'=>'<b>Agencia</b>',
					'total_sal'=>'<b>Precio</b>',
					'0'=>utf8_decode('<b>% Comisión</b>'),
					'1'=>utf8_decode('<b>Comisión</b>'),
					'2'=>'<b>Descuento</b>'),
					'',
			  array('xPos'=>'center',
					'innerLineThickness'=>0.8,
					'outerLineThickness' =>1.2,
					'fontSize' => 8,
					'titleFontSize' => 8,
					'cols'=>array(
						'card_number_sal'=>array('justification'=>'center','width'=>48),
						'number_inv'=>array('justification'=>'center','width'=>48),
						'name_cus'=>array('justification'=>'center','width'=>100),
						'name_pbl'=>array('justification'=>'center','width'=>100),
						'begin_date_sal'=>array('justification'=>'center','width'=>55),
						'end_date_sal'=>array('justification'=>'center','width'=>55),
						'days_sal'=>array('justification'=>'center','width'=>50),
						'name_dea'=>array('justification'=>'center','width'=>100),
						'total_sal'=>array('justification'=>'center','width'=>50),
						'0'=>array('justification'=>'center','width'=>50),
						'1'=>array('justification'=>'center','width'=>45),
						'2'=>array('justification'=>'center','width'=>52))));
//preparing totals array
	$totalValues=array();
	$totalLine1=array('col1'=>number_format($totalSold, 2, '.', ''),
					  'col2'=>number_format($totalRefunded, 2, '.', ''));
	array_push($totalValues,$totalLine1);
	$totalLine2=array('col1'=>'<b>TOTAL:</b>',
					  'col2'=>number_format($comissionToPay, 2, '.', ''));
	array_push($totalValues,$totalLine2);
	$pdf->ezTable($totalValues,
				  array('col1'=>'','col2'=>''),
				  '',
				  array('showHeadings'=>0,
						'innerLineThickness'=>0.8,
						'outerLineThickness' =>1.2,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>749,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'col1'=>array('justification'=>'center','width'=>45),
							'col2'=>array('justification'=>'center','width'=>52))));
	$pdf->ezStream();
?>
