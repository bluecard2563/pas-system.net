<?php
/*
    Document   : pReportComissionsXLS
    Created on : 08-jun-2010, 15:16:31
    Author     : Nicolas
    Description:
       Generate a XLS report of comissions
*/

//Debug::print_r($_POST);
Request::setString('selComissionsTo');
if(!selComissionsTo){
	//die('insuficient Arguments');
	http_redirect('modules/reports/comissions/fSelectComissionsReport');
}
$appliedCom= new AppliedComission($db);
$serial = '';
	switch($selComissionsTo){
		case 'MANAGER':
				Request::setString('selManager');
				$serial=$selManager;
			break;
		case 'RESPONSIBLE':
				Request::setString('selResponsible');
				$serial=$selResponsible;
			break;
		case 'DEALER':
				Request::setString('selBranch');
				$serial=$selBranch;
			break;
	}
Request::setString('selType');
Request::setString('txtStartDate');
Request::setString('txtEndDate');

$apComission=new AppliedComission($db);
$comissionsData=$apComission->getComissionForReport($selComissionsTo,$serial,$selType,$txtStartDate,$txtEndDate);

if($comissionsData){
	$totalSold = 0;$totalRefunded = 0; $comissionToPay = 0;

	switch($selComissionsTo){
		case 'MANAGER':
				$manager=new Manager($db,$serial);
				$manager->getData();
				$payTo=$manager->getName_man();
				$payToId=$manager->getDocument_man();
				$payToPhone=$manager->getPhone_man();
				$payToAddress=$manager->getAddress_man();
				foreach($comissionsData as &$cData){
					$to_comission = 0;	$toDiscount = 0;
					$comissionPercentage = ($cData['value_mbc_com']*100)/$cData['total_sal'];
					if($cData['type_com']=='IN'){
						$totalSold+=$cData['value_mbc_com'];
						$to_comission = $cData['value_mbc_com'];
					}
					else{
						$totalRefunded+=$cData['value_mbc_com'];
						$toDiscount = $cData['value_mbc_com'];
					}
					if(!$cData['card_number_sal']){
						$cData['card_number_sal'] = 'N/A';
					}
					array_push($cData, number_format($comissionPercentage, 2, '.', ''), number_format($to_comission, 2, '.', ''), number_format($toDiscount, 2, '.', '') );
				}
			break;
		case 'RESPONSIBLE':
				$user= new User($db,$serial);
				$user->getData();
				$payTo=$user->getFirstname_usr().' '.$user->getLastname_usr();
				$payToId=$user->getDocument_usr();
				$payToPhone=$user->getPhone_usr();
				$payToAddress=$user->getAddress_usr();
				foreach($comissionsData as &$cData){
					$to_comission = 0;	$toDiscount = 0;
					$comissionPercentage = ($cData['value_ubd_com']*100)/$cData['total_sal'];
					if($cData['type_com']=='IN'){
						$totalSold+=$cData['value_ubd_com'];
						$to_comission = $cData['value_ubd_com'];
					}
					else{
						$totalRefunded+=$cData['value_ubd_com'];
						$toDiscount = $cData['value_ubd_com'];
					}
					if(!$cData['card_number_sal']){
						$cData['card_number_sal'] = 'N/A';
					}
					array_push($cData, number_format($comissionPercentage, 2, '.', ''), number_format($to_comission, 2, '.', ''), number_format($toDiscount, 2, '.', '') );
				}
			break;
		case 'DEALER':
				$dealer=new Dealer($db,$serial);
				$dealer->getData();
				$payTo=$dealer->getName_dea();
				$payToId=$dealer->getId_dea();
				$payToPhone=$dealer->getPhone1_dea();
				$payToAddress=$dealer->getAddress_dea();
				foreach($comissionsData as &$cData){
					$to_comission = 0;	$toDiscount = 0;
					$comissionPercentage = ($cData['value_dea_com']*100)/$cData['total_sal'];
					if($cData['type_com']=='IN'){
						$totalSold+=$cData['value_dea_com'];
						$to_comission = $cData['value_dea_com'];
					}
					else{
						$totalRefunded+=$cData['value_dea_com'];
						$toDiscount = $cData['value_dea_com'];
					}
					if(!$cData['card_number_sal']){
						$cData['card_number_sal'] = 'N/A';
					}
					array_push($cData, number_format($comissionPercentage, 2, '.', ''), number_format($to_comission, 2, '.', ''), number_format($toDiscount, 2, '.', '') );
				}
			break;
	}
}

$comissionToPay=$totalSold-$totalRefunded;
$comissionToPay = number_format($comissionToPay, 2, '.', '');
$totalSold = number_format($totalSold, 2, '.', '');
$totalRefunded = number_format($totalRefunded, 2, '.', '');

//Debug::print_r($comissionsData);
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Reporte_de_Comisiones.xls");
header("Pragma: no-cache");
header("Expires: 0");

$title = "<table><tr><th></th><th><b>Reporte de Comisiones</b></th></tr></table>";
echo $title;

$headerValues = "<table border='1'>";
$headerValues .= "<tr>
					<th><b>Pagar a:</b></th>
					<th>".$payTo."</th>
					<th><b>RUC/CI:</b></th>
					<th>&nbsp;".$payToId."</th>
				</tr>";
$headerValues .= "<tr>
					<th><b>Fecha:</b></th>
					<th>".date('d/m/Y')."</th>
					<th><b>Tel�fono:</b></th>
					<th>&nbsp;".$payToPhone."</th>
				</tr>";
$headerValues .= "<tr>
					<th><b>Comisi�n a Favor:</b></th>
					<th>".$totalSold."</th>
					<th><b>Direcci�n:</b></th>
					<th>".$payToAddress."</th>
				</tr>";
$headerValues .= "<tr>
					<th><b>Comisi�n a Descontar:</b></th>
					<th>".$totalRefunded."</th>
					<th></th>
					<th></th>
				</tr>";
$headerValues .= "<tr>
					<th><b>Valor Comisi�n:</b></th>
					<th>".$comissionToPay."</th>
					<th></th>
					<th></th>
				</tr>";
echo $headerValues."</table>";


$data = "<table border='1'><tr>
			<th><b># Tarjeta</b></th>
			<th><b># Factura</b></th>
			<th><b>Apellido y Nombre</b></th>
			<th><b>Producto</b></th>
			<th><b>Inicio</b></th>
			<th><b>Fin</b></th>
			<th><b>Tiempo</b></th>
			<th><b>Agencia</b></th>
			<th><b>Precio</b></th>
			<th><b>% Comisi�n</b></th>
			<th><b>Comisi�n</b></th>
			<th><b>Descuento</b></th>
		</tr>";

foreach($comissionsData as $cd){
	$data .= "<tr>
				<th>".$cd['card_number_sal']."</th>
				<th>".$cd['number_inv']."</th>
				<th>".$cd['name_cus']."</th>
				<th>".$cd['name_pbl']."</th>
				<th>".$cd['begin_date_sal']."</th>
				<th>".$cd['end_date_sal']."</th>
				<th>".$cd['days_sal']."</th>
				<th>".$cd['name_dea']."</th>
				<th>".$cd['total_sal']."</th>
				<th>".$cd['0']."</th>
				<th>".$cd['1']."</th>
				<th>".$cd['2']."</th>
			  </tr>";
}
$data .= "</table>";
echo $data;

//preparing totals 
	$totalValue = "<table border='1'>
						<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
							<th>".$totalSold."</th>
							<th>".$totalRefunded."</th>
						</tr>
						<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
							<th>TOTAL</th>
							<th>".$comissionToPay."</th>
						</tr>
					</table>";
	echo $totalValue;
?>
