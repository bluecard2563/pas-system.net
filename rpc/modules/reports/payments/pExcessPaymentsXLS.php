<?php

if(isset($_POST['selCountry']) && isset($_POST['selManager'])){

	$payment = new Payment($db);
	$paymentsRaw = array();
    $payments = array();

	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
    $serial_usr = (isset($_POST['selUser'])) ? $_POST['selUser'] : '';

        $paymentsRaw = $payment->getExcessPayments($serial_cou,	$serial_mbc,$_SESSION['serial_mbc'],
													$_SESSION['dea_serial_dea'], $serial_dea,$serial_bra,$status,
													$serial_usr);
		$cont=0;
		foreach($paymentsRaw as $pay){
			$paymentsRaw[$cont]['invoice_group'] = explode(",", $pay['invoice_group']);
			$paymentsRaw[$cont]['card_group'] = explode(",", $pay['card_group']);
			$paymentsRaw[$cont]['inv_serial_dea'] = explode(",", $pay['inv_serial_dea']);
			$cont++;
		}
        //debug::print_r($paymentsRaw);die();
        if(!$paymentsRaw){
            http_redirect('modules/reports/payments/fExcessPayments');
        }

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=Reporte_Pagos_Exceso.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
		$mainHeader = "<table>
							<tr><td colspan=6>".$date."</td></tr>
							<tr></tr>
							<tr><th></th><th><b>Reporte de Pagos en Exceso</b></th></tr>
							<tr><th><b>Pa�s: </b>".$paymentsRaw[0]['name_cou']."</th>
							<th><b>Representante: </b>".$paymentsRaw[0]['name_man']."</th></tr></table>";
		echo $mainHeader;
		$parameters = "<table><tr>";
			
			if($serial_dea){
				$parameters.="<th><b>Comercializador: </b>".$paymentsRaw[0]['name_dea']."</th>";
			}
			if($serial_bra){
				$parameters.="<th><b>Sucursal: </b>".$paymentsRaw[0]['name_bra']."</th>";
			}
			if($status){
				if($status == 'Available'){
					$parameters.="<th><b>Estado: Disponibles</b></th>";
				}
				else{
					$parameters.="<th><b>Estado: Liquidados</b></th>";
				}
			}
			if($serial_usr){
				$parameters.="<th><b>Liquidado Por: </b>".$paymentsRaw[0]['first_name_usr']." ".$paymentsRaw[0]['last_name_usr']."</th>";
			}

			$parameters.="</tr><tr></tr></table>";
			echo $parameters;
			
			$dealer = '';
			//debug::print_r($paymentsRaw);die();
            foreach ($paymentsRaw as $key=> $pay) {
				if($pay['serial_dea'] != $dealer){
					$title = "<table><tr></tr><tr><th></th>
										<th><b>Pagos en Exceso del Comercializador: </b>".$pay['name_dea']."</th>
								</tr></table>";
					echo $title;
				}
				$rowsTable1 = "<table border='1'><tr>
									<th><b># Pago</b></th>
									<th><b>A Pagar</b></th>
									<th><b>Pagado</b></th>
									<th><b>En Exceso</b></th>
									<th><b>Fecha Pago</b></th>
									<th><b>Comercializador</b></th>
									</tr>
									<tr>
									<th>".$pay['serial_pay']."</th>
									<th>".number_format($pay['total_to_pay_pay'], 2, '.', '')."</th>
									<th>".number_format($pay['total_payed_pay'], 2, '.', '')."</th>
									<th>".number_format(($pay['total_payed_pay'] - $pay['total_to_pay_pay']), 2, '.', '')."</th>
									<th>".$pay['date_pay']."</th>
									<th>".$pay['name_bra']."</th>
									</tr>
								</table>";
				echo $rowsTable1;
				$rowsTable2 = "<table><tr></tr><tr><td></td><td>
									<table border='1'><tr>
									<th><b># Factura</b></th>
									<th><b># Tarjeta</b></th>		
							";
				if($status != 'Available'){
					$rowsTable2.= "<th><b>Fecha Liquidaci�n</b></th>
									<th><b>Usuario</b></th>
								";
				}
				$rowsTable2.= "</tr>";
				foreach($pay['inv_serial_dea'] as $key2=> $p){
					$rowsTable2.= "<tr>
										<th><b>".$pay['invoice_group'][$key2]."</th>
										<th><b>".$pay['card_group'][$key2]."</th>
									";
					if($status != 'Available'){
						$rowsTable2.="<th>".$pay['payment_date_xpl']."</th>
									 <th>".$pay['first_name_usr']." ".$pay['last_name_usr']."</th>";
					}
					$rowsTable2.= "</tr>";
				}
				$rowsTable2.= "</td></tr></table></table>";
				echo $rowsTable2;
				$dealer = $pay['serial_dea'];
            }
}else{
    http_redirect('modules/reports/payments/fExcessPayments');
}


?>