<?php
set_time_limit(36000);
ini_set('memory_limit','512M');
if( ( isset($_POST['txtDateFrom']) && isset($_POST['txtDateTo']) ) || ( isset($_POST['selCountry']) && isset($_POST['selManager']) && isset($_POST['txtInvoice']) ) ){
   
	$payment = new Payment($db);
	$paymentsRaw = array();
    $payments = array();

	if(isset($_POST['txtDateFrom'])){
		$dateFrom = $_POST['txtDateFrom'];
		$dateTo = $_POST['txtDateTo'];
		$serial_cus = (isset($_POST['txtCustomer'])) ? $_POST['txtCustomer'] : '';
		$card_number_sal = (isset($_POST['txtCard'])) ? $_POST['txtCard'] : '';
		$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
		$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
		$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
		$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
		$type = $_POST['selType'];

		$paymentsRaw = $payment->getPayments($dateFrom, $dateTo, $_SESSION['serial_mbc'], $_SESSION['dea_serial_dea'],
												$serial_cus, $card_number_sal, $serial_cou, $serial_mbc, $serial_dea,
												$serial_bra, $type);
		}
	else{
		$serial_cou = $_POST['selCountry'];
		$serial_mbc = $_POST['selManager'];
		$number_inv = $_POST['txtInvoice'];
		$type = $_POST['rdType'];
		$paymentsRaw = $payment->getPaymentsInvoice($serial_cou, $serial_mbc, $number_inv, $type, $_SESSION['serial_mbc'],
													$_SESSION['dea_serial_dea']);
	}
        //debug::print_r($paymentsRaw);
		$cont=0;
		foreach($paymentsRaw as $pay){
			$paymentsRaw[$cont]['invoice_group'] = explode(",", $pay['invoice_group']);
			$paymentsRaw[$cont]['card_group'] = explode(",", $pay['card_group']);
			$paymentsRaw[$cont]['name_customer_group'] = explode(",", $pay['name_customer_group']);
			$paymentsRaw[$cont]['last_customer_group'] = explode(",", $pay['last_customer_group']);
			$paymentsRaw[$cont]['inv_serial_dea'] = explode(",", $pay['inv_serial_dea']);
			$cont++;
		}
		
		//debug::print_r($paymentsRaw);die();
        if(!$paymentsRaw){
            http_redirect('modules/reports/payments/fPayments');
        }
			$table1 = array(
				'payment'=>'<b># Pago</b>',
				'date'=>'<b>Fecha</b>',
				'branch'=>'<b>Comercializador</b>',
				'amount'=>'<b>Valor</b>'
			);
			$alignTable1 = array(
				'payment'=>array('justification'=>'center','width'=>50),
				'date'=>array('justification'=>'center','width'=>130),
				'branch'=>array('justification'=>'center','width'=>100),
				'amount'=>array('justification'=>'center','width'=>50)
			);
			$optionsTable1 = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 10,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignTable1,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);
			$table2 = array(
				'invoice'=>'<b># Factura</b>',
				'card'=>'<b># Tarjeta</b>',
				'customer'=>'<b>Cliente</b>',
				'invoiceName'=>'<b>Facturado a</b>'
			);
			$alignTable2 = array(
				'invoice'=>array('justification'=>'center','width'=>60),
				'card'=>array('justification'=>'center','width'=>60),
				'customer'=>array('justification'=>'center','width'=>130),
				'invoiceName'=>array('justification'=>'center','width'=>130)
			);
			$optionsTable2 = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 10,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignTable2,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);

			if(isset($_POST['txtDateFrom'])){
				$requiredFields = array(
										array('dateFrom'=>'<b>Desde: </b>'.$dateFrom,'dateTo'=>'<b>Hasta: </b>'.$dateTo)
										);
				$parameters = array();
				$col=0; $row=0;
				if($serial_cus){
					$parameters[0][$col]='<b>Cliente: </b>'.$paymentsRaw[0]['name_customer_group'][0].' '.$paymentsRaw[0]['last_customer_group'][0];
					$col=$col+1;
				}
				if($card_number_sal){
					$parameters[0][$col]='<b>Tarjeta: </b>'.$paymentsRaw[0]['card_group'][0];
					$col=$col+1;
				}
				if($serial_cou){
					$parameters[0][$col]=html_entity_decode('<b>Pa&iacute;s: </b>').$paymentsRaw[0]['name_cou'];
					$col=$col+1;
				}
				if($serial_mbc){
					$parameters[0][$col]='<b>Representante: </b>'.$paymentsRaw[0]['name_man'];
					$col=$col+1;
				}
				if($col==4){$col=0; $row=1;}
				if($serial_dea){
					$parameters[$row][$col]='<b>Comercializador: </b>'.$paymentsRaw[0]['name_dea'];
					$col=$col+1;
				}
				if($col==4){$col=0; $row=1;}
				if($serial_bra){
					$parameters[$row][$col]='<b>Sucursal: </b>'.$paymentsRaw[0]['name_bra'];
					$col=$col+1;
				}
				if($col==4){$col=0; $row=1;}
				if($type){
					if($type == 1){
						$parameters[$row][$col]='<b>Tipo: </b>Total';
					}
					else{
						$parameters[$row][$col]='<b>Tipo: </b>Abono';
					}
				}
			}
			else{
				$requiredFields = array(
										array('country'=>html_entity_decode('<b>Pa&iacute;s: </b>').$paymentsRaw[0]['name_cou'],
												'manager'=>'<b>Representante: </b>'.$paymentsRaw[0]['name_dea'],
												'type'=>'<b>Tipo de Persona: </b>'.ucfirst($type),
												'invoice'=>html_entity_decode('<b>N&uacute;mero de Factura: </b>').$number_inv
											)
										);
			}
			include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

			$all = $pdf->openObject();
			$pdf->saveState();
			$pdf->addText(50,50,10,html_entity_decode('*Las tarjetas N/A son del tipo corporativo, no generan ning&uacute;n n&uacute;mero al momento de la venta.'));
			$pdf->restoreState();
			$pdf->closeObject();
			$pdf->addObject($all,'all');

			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>Reporte de Cobros</b>',14,array('justification'=>'center'));
			$pdf->ezSetDy(-20);
			if(isset($_POST['txtDateFrom'])){
				$pdf->ezTable($requiredFields,array('dateFrom'=>'dateFrom','dateTo'=>'dateTo'),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13,
										'cols'=>array(
										'dateFrom'=>array('justification'=>'center','width'=>270),
										'dateTo'=>array('justification'=>'center','width'=>270)
				)			));
				$pdf->ezSetDy(-20);
				$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13));
			}
			else{
				$pdf->ezTable($requiredFields,array('country'=>'country','manager'=>'manager','type'=>'type','invoice'=>'invoice'),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13,
									'cols'=>array(
									'country'=>array('justification'=>'center','width'=>180),
									'manager'=>array('justification'=>'center','width'=>180),
									'type'=>array('justification'=>'center','width'=>180),
									'invoice'=>array('justification'=>'center','width'=>180)
				)			));
			}
			$pdf->ezSetDy(-20);
			$dealer = '';
			$rowsTable1 = array();
			$rowsTable2 = array();//debug::print_r($paymentsRaw);die();
            foreach ($paymentsRaw as $pay) {				
				if($pay['serial_dea'] != $dealer){
					$pdf->ezText('<b>Cobros en el Comercializador: </b>'.$pay['name_dea'],12,array('justification'=>'center'));
					$pdf->ezSetDy(-20);
				}
				$rowsTable1[] = array(
					'payment'=>$pay['serial_pay'],
					'date'=>$pay['date_pay'],
					'branch'=>$pay['name_bra'],
					'amount'=>number_format($pay['total_payed_pay'], 2, '.', '')
				);
				$pdf->ezTable($rowsTable1, $table1, NULL,$optionsTable1);
				unset($rowsTable1);
				$pdf->ezSetDy(-20);
				
				foreach($pay['inv_serial_dea'] as $key2=> $p){
					$nameCustomer = '';$invoiceName = '';
					if($pay['inv_serial_dea'][$key2] == 'N/A'){
						$invoiceName = 'Cliente';
						$nameCustomer = $pay['name_customer_group'][$key2];
						if($pay['last_customer_group'][$key2] != 'N/A'){
							$nameCustomer.= ' '.$pay['last_customer_group'][$key2];
						}
					}
					else{
						$invoiceName = 'Comercializador';
						$nameCustomer = $pay['name_bra'];
					}
					$rowsTable2[] = array(
						'invoice'=>$pay['invoice_group'][$key2],
						'card'=>$pay['card_group'][$key2],
						'customer'=>$nameCustomer,
						'invoiceName'=>$invoiceName
					);
				}
				$pdf->ezTable($rowsTable2, $table2, NULL,$optionsTable2);
				unset($rowsTable2);
				$pdf->ezSetDy(-20);
				$dealer = $pay['serial_dea'];
            }			
			$pdf->ezStream();
}else{
    http_redirect('modules/reports/payments/fPayments');
}
?>