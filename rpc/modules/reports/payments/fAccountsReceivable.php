<?php
/*
File:fAccountsReceivable.php
Author: Gabriela Guerrero
Creation Date:31/05/2010
Modified By: 
Last Modified: 
*/
Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getZonesByInvoice($_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

//Language to be used on data retrieve
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//DEALER CATEGORIES
$dealer=new Dealer($db);
$categoryList=$dealer->getCategories();

$smarty -> register('zonesList,error,categoryList');
$smarty->display();
?>