<?php

if(isset($_POST['selZone']) && isset($_POST['selCountry']) && isset($_POST['selManager'])){
   
	$invoice = new Invoice($db);
	$invoicesRaw = array();
    $invoices = array();
    $colsAux = array();
	$alignCols=array();

	$serial_zon = (isset($_POST['selZone'])) ? $_POST['selZone'] : '';
	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$date_from = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
	$date_to = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';
	$days = (isset($_POST['selDays'])) ? $_POST['selDays'] : '';
	$category_dea = (isset($_POST['selCategory'])) ? $_POST['selCategory'] : '';
     
	$invoicesRaw = $invoice->getNotPayedInvoices($serial_zon, $serial_cou, $serial_man,
													$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'],
													$serial_cit,$serial_dea,$serial_bra,
													$date_from,$date_to,$days,$category_dea);
	
	//debug::print_r($invoicesRaw);die();

	if(!$invoicesRaw){
		http_redirect('modules/reports/payments/fAccountsReceivable');
	}
		$colsAux = array(
			'date'=>'<b>Fecha</b>',
			'numberInv'=>'<b># Factura / # Tarjeta</b>',
			'dealer'=>html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>'),
			'days'=>'<b>Impago</b>',
			'til30'=>'<b>Hasta 30</b>',
			'til60'=>'<b>Hasta 60</b>',
			'til90'=>'<b>Hasta 90</b>',
			'over90'=>html_entity_decode('<b>M&aacute;s de 90</b>')
		);
		$alignCols= array(
			'date'=>array('justification'=>'center','width'=>100),
			'numberInv'=>array('justification'=>'center','width'=>120),
			'dealer'=>array('justification'=>'center','width'=>150),
			'days'=>array('justification'=>'center','width'=>50),
			'til30'=>array('justification'=>'center','width'=>80),
			'til60'=>array('justification'=>'center','width'=>80),
			'til90'=>array('justification'=>'center','width'=>80),
			'over90'=>array('justification'=>'center','width'=>80)
		);
		$options = array(
			'showLines'=>2,
			'showHeadings'=>1,
			'shaded'=>0,
			'fontSize' => 10,
			'textCol' =>array(0,0,0),
			'titleFontSize' => 12,
			'rowGap' => 5,
			'colGap' => 5,
			'lineCol'=>array(0.8,0.8,0.8),
			'xPos'=>'center',
			'xOrientation'=>'center',
			'maxWidth' => 805,
			'cols' =>$alignCols,
			'innerLineThickness' => 0.8,
			'outerLineThickness' => 0.8
		);

		$requiredFields = array(
								array('zone'=>'<b>Zona: </b>'.$invoicesRaw[0]['name_zon'],
									  'country'=>html_entity_decode('<b>Pa&iacute;s: </b>').$invoicesRaw[0]['name_cou'],
									  'manager'=>'<b>Representante: </b>'.$invoicesRaw[0]['name_man']
								));
		$parameters = array();
		$row=0; $col=0;
		if($serial_cit){
			$parameters[0][$col]='<b>Ciudad: </b>'.$invoicesRaw[0]['name_cit'];
			$col++;
		}
		if($serial_dea){
			$parameters[0][$col]='<b>Comercializador: </b>'.$invoicesRaw[0]['name_dea'];
			$col++;
		}
		if($serial_bra){
			$parameters[0][$col]='<b>Sucursal: </b>'.$invoicesRaw[0]['name_bra'];
			$col++;
		}
		if($days){
			$days=$days+29;
			if($days==120){
				$days='90+';
			}
			$parameters[0][$col]=html_entity_decode('<b>D&iacute;as Hasta: </b>').$days;
			$col++;
		}
		if($col>2){
				$row=1;
				$col=0;
		}
		if($date_from && $date_to){
			$parameters[$row][$col]='<b>Desde: </b>'.$date_from;
			$col++;
			$parameters[$row][$col]='<b>Hasta: </b>'.$date_to;
			$col++;
		}
		if($col==4){
			$row=1;
		}
		if($category_dea){
			$parameters[$row][$col]=html_entity_decode('<b>Categor&iacute;a: </b>').$category_dea;
		}

		include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
		$all = $pdf->openObject();
		$pdf->saveState();
		$pdf->addText(50,50,10,html_entity_decode('*Las tarjetas N/A son del tipo corporativo, no generan ning&uacute;n n&uacute;mero al momento de la venta.'));
		$pdf->restoreState();
		$pdf->closeObject();
		$pdf->addObject($all,'all');
		$pdf->ezSetDy(-20);
		$pdf->ezText('<b>Reporte de Cuentas por Cobrar</b>',14,array('justification'=>'center'));
		$pdf->ezSetDy(-20);
		$pdf->ezTable($requiredFields,array('zone'=>'zon','country'=>'cou','manager'=>'man'),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13,
								'cols'=>array(
								'zone'=>array('justification'=>'center','width'=>270),
								'country'=>array('justification'=>'center','width'=>270),
								'manager'=>array('justification'=>'center','width'=>270)
		)			));
		$pdf->ezSetDy(-20);
		$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13));
		$pdf->ezSetDy(-30);
		$branch = '';
		
		//GRAND TOTAL CALCULATION
		$grandTotal = 0;
		foreach ($invoicesRaw as $key=> $inv) {
			$grandTotal += $inv['total_inv'];
		}
		$pdf->ezText('<b>Total a Cobrar: </b>'.number_format($grandTotal, 2, '.', ''),12,array('justification'=>'center'));
		$pdf->ezSetDy(-20);
		
		foreach ($invoicesRaw as $key=> $inv) {

			if($inv['serial_bra'] != $branch){
				$pdf->ezText('<b>Comercializador: </b>'.$inv['name_dea'],12,array('justification'=>'center'));
				$pdf->ezText('<b>Sucursal: </b>'.$inv['name_bra'],12,array('justification'=>'center'));
				$pdf->ezSetDy(-20);
			}

			$nameInv = ''; $phoneInv = '';
			if($inv['serial_dea']){
				$nameInv = $inv['name_dea'];
				$phoneInv = $inv['phone1_dea'];
			}
			else{
				$nameInv = $inv['first_name_cus'].' '.$inv['last_name_cus'];
				$phoneInv = $inv['phone1_cus'];
			}

			$daysNum = 0;
			if($inv['diff'] > 0){
				$daysNum = $inv['diff'];
			}

			$ammount30 = 0; $ammount60 = 0; $ammount90 = 0; $ammount91 = 0;
			if($inv['diff'] <= 30){
				$ammount30 = number_format($inv['total_inv'], 2, '.', '');
			}
			if($inv['diff'] > 30 && $inv['diff'] <= 60){
				$ammount60 = number_format($inv['total_inv'], 2, '.', '');
			}
			if($inv['diff'] > 60 && $inv['diff'] <= 90){
				$ammount90 = number_format($inv['total_inv'], 2, '.', '');
			}
			if($inv['diff'] > 90){
				$ammount91 = number_format($inv['total_inv'], 2, '.', '');
			}
			$card='';
			if($inv['card_number_sal']){
				$card=$inv['card_group'];
			}
			else{
				$card='N/A';
			}
			$invoices[] =array('date'=>$inv['date_inv'],
								'numberInv'=>$inv['number_inv'].' / '.$card,
								'dealer'=>$nameInv.' / '.$phoneInv,
								'days'=>$daysNum,
								'til30'=>$ammount30,
								'til60'=>$ammount60,
								'til90'=>$ammount90,
								'over90'=>$ammount91
							);
			if($inv['serial_bra']!=$invoicesRaw[$key+1]['serial_bra']){
				$amm30 = 0; $amm60 = 0; $amm90 = 0; $amm91 = 0;
				if($invoices){
					foreach($invoices as $i){
						$amm30 = $amm30+$i['til30']; $amm60 = $amm60+$i['til60']; $amm90 = $amm90+$i['til90']; $amm91 = $amm91+$i['over90'];
					}
				}
				$invoices[] =array('date'=>'',
									'numberInv'=>'',
									'dealer'=>'',
									'days'=>'TOTAL',
									'til30'=>number_format($amm30, 2, '.', ''),
									'til60'=>number_format($amm60, 2, '.', ''),
									'til90'=>number_format($amm90, 2, '.', ''),
									'over90'=>number_format($amm91, 2, '.', '')
								);
				if($days == '30'){
					$colsAux = array(
						'date'=>'<b>Fecha</b>',
						'numberInv'=>'<b># Factura / # Tarjeta</b>',
						'dealer'=>html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>'),
						'days'=>'<b>Impago</b>',
						'til30'=>'<b>Hasta 30</b>'
					);
				}
				if($days == '60'){
					$colsAux = array(
						'date'=>'<b>Fecha</b>',
						'numberInv'=>'<b># Factura / # Tarjeta</b>',
						'dealer'=>html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>'),
						'days'=>'<b>Impago</b>',
						'til60'=>'<b>Hasta 60</b>'
					);
				}
				if($days == '90'){
					$colsAux = array(
						'date'=>'<b>Fecha</b>',
						'numberInv'=>'<b># Factura / # Tarjeta</b>',
						'dealer'=>html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>'),
						'days'=>'<b>Impago</b>',
						'til90'=>'<b>Hasta 90</b>'
					);
				}
				if($days == '90+'){
					$colsAux = array(
						'date'=>'<b>Fecha</b>',
						'numberInv'=>'<b># Factura / # Tarjeta</b>',
						'dealer'=>html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>'),
						'days'=>'<b>Impago</b>',
						'over90'=>'<b>M�s de 90</b>'
					);
				}
				$pdf->ezTable($invoices, $colsAux, NULL,$options);
				$pdf->ezSetDy(-20);
				$invoices = array();
			}
			$branch = $inv['serial_bra'];
		}
		$pdf->ezStream();
}else{
    http_redirect('modules/reports/payments/fAccountsReceivable');
}
?>