<?php
set_time_limit(36000);
ini_set('memory_limit','512M');
if( ( isset($_POST['txtDateFrom']) && isset($_POST['txtDateTo']) ) || ( isset($_POST['selCountry']) && isset($_POST['selManager']) && isset($_POST['txtInvoice']) ) ){

	$payment = new Payment($db);
	$paymentsRaw = array();
    $payments = array();
	
	if(isset($_POST['txtDateFrom'])){
		$dateFrom = $_POST['txtDateFrom'];
		$dateTo = $_POST['txtDateTo'];
		$serial_cus = (isset($_POST['txtCustomer'])) ? $_POST['txtCustomer'] : '';
		$card_number_sal = (isset($_POST['txtCard'])) ? $_POST['txtCard'] : '';
		$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
		$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
		$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
		$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
		$type = $_POST['selType'];

		$paymentsRaw = $payment->getPayments($dateFrom, $dateTo,$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'],
												$serial_cus,$card_number_sal,$serial_cou,$serial_man,$serial_dea,
												$serial_bra,$type);
	} else{
		$serial_cou = $_POST['selCountry'];
		$serial_mbc = $_POST['selManager'];
		$number_inv = $_POST['txtInvoice'];
		$type = $_POST['rdType'];
		$paymentsRaw = $payment->getPaymentsInvoice($serial_cou,$serial_mbc,$number_inv,$type,$_SESSION['serial_mbc'],
													$_SESSION['dea_serial_dea']);
	}
        //debug::print_r($paymentsRaw);
		$cont=0;
		foreach($paymentsRaw as $pay){
			$paymentsRaw[$cont]['invoice_group'] = explode(",", $pay['invoice_group']);
			$paymentsRaw[$cont]['card_group'] = explode(",", $pay['card_group']);
			$paymentsRaw[$cont]['name_customer_group'] = explode(",", $pay['name_customer_group']);
			$paymentsRaw[$cont]['last_customer_group'] = explode(",", $pay['last_customer_group']);
			$paymentsRaw[$cont]['inv_serial_dea'] = explode(",", $pay['inv_serial_dea']);
			$cont++;
		}

		//debug::print_r($paymentsRaw);die();
        if(!$paymentsRaw){
            http_redirect('modules/reports/payments/fPayments');
        }

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=Reporte_de_Cobros.xls");
		header("Pragma: no-cache");
		header("Expires: 0");		
		$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
			if(isset($_POST['txtDateFrom'])){
				$mainHeader = "<table>
							<tr><td colspan=6>".$date."</td></tr>
							<tr></tr>
							<tr><td></td><td><b>Reporte de Cobros</b></td></tr><tr></tr>
							<tr><td><b>Desde: </b>".$dateFrom."</td>
							<td><b>Hasta: </b>".$dateTo."</td></tr><tr></tr></table>";
				echo $mainHeader;
				$parameters = array(
									'0'=>array('0'=>'<b>Cliente: </b>', '1'=>'Todos', '2'=>'<b>Tarjeta: </b>', '3'=>'Todos'),
									'1'=>array('0'=>'<b>Pa�s: </b>', '1'=>'Todos', '2'=>'<b>Representante: </b>', '3'=>'Todos'),
									'2'=>array('0'=>'<b>Comercializador: </b>', '1'=>'Todos', '2'=>'<b>Sucursal: </b>', '3'=>'Todos'),
									'3'=>array('0'=>'<b>Tipo: </b>', '1'=>'Todos', '2'=>'', '3'=>''),
									);
				if($serial_cus){
					$parameters['0']['1'] = $paymentsRaw[0]['name_customer_group'][0].' '.$paymentsRaw[0]['last_customer_group'][0];
				}
				if($card_number_sal){
					$parameters['0']['3'] = $paymentsRaw[0]['card_group'][0];
				}
				if($serial_cou){
					$parameters['1']['1'] = $paymentsRaw[0]['name_cou'];
				}
				if($serial_man){
					$parameters['1']['3'] = $paymentsRaw[0]['name_man'];
				}
				if($serial_dea){
					$parameters['2']['1'] = $paymentsRaw[0]['name_dea'];
				}
				if($serial_bra){
					$parameters['2']['3'] = $paymentsRaw[0]['name_bra'];
				}
				if($type){
					if($type == 1){
						$parameters['3']['1'] = 'Total';
					}
					else{
						$parameters['3']['1'] = 'Abono';
					}
				}
				$parametersTable = "<table>";
				foreach($parameters as $p){
					$parametersTable .= "<tr><td align='right'><b>".$p['0']."</b></td><td align='left'>".$p['1']."</td>
											<td align='right'><b>".$p['2']."</b></td><td align='left'>".$p['3']."</td></tr>";
				}
				$parametersTable .= "</table>";
				echo $parametersTable;
			}
			else{
				$mainHeader = "<table>
							<tr><td colspan=6>".$date."</td></tr>
							<tr></tr>
							<tr><th></th><th><b>Reporte de Cobros</b></th></tr>
							<tr><th><b>Pa&iacute;s: </b>".$paymentsRaw[0]['name_cou']."</th>
							<th><b>Representante: </b>".$paymentsRaw[0]['name_dea']."</th>
							<th><b>Tipo de Persona: </b>".ucfirst($type)."</th>
							<th><b>N&uacute;mero de Factura: </b>".$number_inv."</th></tr></table>";
				echo $mainHeader;
			}

			$dealer = '';
			//debug::print_r($paymentsRaw);die();
            foreach ($paymentsRaw as $pay) {
				if($pay['name_dea'] != $dealer){
					$title = "<table><tr></tr><tr><th></th>
										<th><b>Cobros en el Comercializador: </b>".$pay['name_dea']."</th>
								</tr></table>";
					echo $title;
				}
				$rowsTable1 = "<table border='1'><tr>
									<th><b># Pago: </b>".$pay['serial_pay']."</th>
									<th><b>Fecha: </b>".$pay['date_pay']."</th>
									<th><b>Comercializador: </b>".$pay['name_bra']."</th>
									<th><b>Valor: </b>".number_format($pay['total_payed_pay'], 2, '.', '')."</th>
								</tr></table>";
				echo $rowsTable1;
				$rowsTable2 = "<table border='1'><tr>
									<th><b># Factura</b></th>
									<th><b># Tarjeta</b></th>
									<th><b>Cliente</b></th>
									<th><b>Facturado a</b></th>
								</tr>
							";
				foreach($pay['inv_serial_dea'] as $key2=> $p){
					$nameCustomer = '';$invoiceName = '';
					if($pay['inv_serial_dea'][$key2] == 'N/A'){
						$invoiceName = 'Cliente';
						$nameCustomer = $pay['name_customer_group'][$key2];
						if($pay['last_customer_group'][$key2] != 'N/A'){
							$nameCustomer.= ' '.$pay['last_customer_group'][$key2];
						}
					}
					else{
						$invoiceName = 'Comercializador';
						$nameCustomer = $pay['name_bra'];
					}
					$rowsTable2.= "<tr><td align='center'>".$pay['invoice_group'][$key2]."</td>
									<td align='center'>".$pay['card_group'][$key2]."</td>
									<td align='center'>".$nameCustomer."</td>
									<td align='center'>".$invoiceName."</td></tr>";
				}
				$rowsTable2.= "</table>";
				echo $rowsTable2;
				$dealer = $pay['name_dea'];
            }
			
}else{
    http_redirect('modules/reports/payments/fPayments');
}


?>