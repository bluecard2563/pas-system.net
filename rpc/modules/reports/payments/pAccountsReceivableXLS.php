<?php

if(isset($_POST['selZone']) && isset($_POST['selCountry']) && isset($_POST['selManager'])){
   
	$invoice = new Invoice($db);
	$invoicesRaw = array();

	$serial_zon = (isset($_POST['selZone'])) ? $_POST['selZone'] : '';
	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$date_from = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
	$date_to = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';
	$days = (isset($_POST['selDays'])) ? $_POST['selDays'] : '';
	$category_dea = (isset($_POST['selCategory'])) ? $_POST['selCategory'] : '';
     
        $invoicesRaw = $invoice->getNotPayedInvoices($serial_zon, $serial_cou, $serial_man,
														$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'],
														$serial_cit,$serial_dea,$serial_bra,
														$date_from,$date_to,$days,$category_dea);
        //debug::print_r($invoicesRaw);die();
        if(!$invoicesRaw){
            http_redirect('modules/reports/payments/fAccountsReceivable');
        }
            $date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
			$requiredFields="<table>
							<tr><td colspan=6>".$date."</td></tr>
							<tr></tr>
							<tr>
								<th></th>
								<th><b>Reporte de Cuentas por Cobrar</b></th>
							</tr>
							<tr>
								<th><b>Zona: </b>".$invoicesRaw[0]['name_zon']."</th>
								".html_entity_decode("<th><b>Pa&iacute;s: </b>").$invoicesRaw[0]['name_cou']."</th>
								<th><b>Representante: </b>".$invoicesRaw[0]['name_man']."</th>
							</tr></table>";
			
			$parameters = "<table><tr>";
			$col=0;
			if($serial_cit){
				$parameters.= "<th><b>Ciudad: </b>".$invoicesRaw[0]['name_cit']."</th>";
				$col=$col+1;
			}
			if($serial_dea){
				$parameters.= "<th><b>Comercializador: </b>".$invoicesRaw[0]['name_dea']."</th>";
				$col=$col+1;
			}
			if($serial_bra){
				$parameters.= "<th><b>Sucursal: </b>".$invoicesRaw[0]['name_bra']."</th>";
				$col=$col+1;
			}
			if($days){
				$days=$days+29;
				if($days==120){
					$days='90+';
				}
				$parameters.= html_entity_decode("<th><b>D&iacute;as Hasta: </b>").$days."</th>";
				$col=$col+1;
			}
			if($col>2){
					$parameters.= "</tr><tr>";
					$col=0;
			}
			if($date_from && $date_to){
				$parameters.= "<th><b>Desde: </b>".$date_from."</th>";
				$col=$col+1;
				$parameters.= "<th><b>Hasta: </b>".$date_to."</th>";
				$col=$col+1;
			}
			if($col==4){
				$parameters.= "</tr><tr>";
			}
			if($category_dea){
				$parameters.= html_entity_decode("<th><b>Categor&iacute;a: </b>").$category_dea."</th>";
			}
			$parameters.= "</tr></table>";
			
			header('Content-type: application/vnd.ms-excel');
			header("Content-Disposition: attachment; filename=Cuentas_por_Cobrar.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			echo $requiredFields;
			echo $parameters;
			
			//GRAND TOTAL CALCULATION
			$grandTotal = 0;
			foreach ($invoicesRaw as $key=> $inv) {
				$grandTotal += $inv['total_inv'];
			}
			echo "<table><tr><th></th><th><b>Total a Cobrar: </b>".number_format($grandTotal, 2, '.', '')."</th></tr></table>";
					
			$branch = '';
			//debug::print_r($invoicesRaw);die();
            foreach ($invoicesRaw as $key=> $inv) {
				
				if($inv['serial_bra'] != $branch){
					$amm30 = 0; $amm60 = 0; $amm90 = 0; $amm91 = 0;
					$branchHeader = "<table><tr><th></th>";
					$branchHeader.="<th><b>Comercializador: </b>".$inv['name_dea']."</th></tr>";
					$branchHeader.="<tr><th></th><th><b>Sucursal: </b>".$inv['name_bra']."</th></tr></table>";
					echo $branchHeader;
					$invoices = "<table border='1'><tr>".
								"<th><b>Fecha</b></th>".
								"<th><b># Factura / # Tarjeta</b></th>".
								html_entity_decode("<th><b>Facturado a / Tel&eacute;fono</b></th>").
								"<th><b>Impago</b></th>"
								;
					if(!$days){
					$invoices.="<th><b>Hasta 30</b></th>".
								"<th><b>Hasta 60</b></th>".
								"<th><b>Hasta 90</b></th>".
								html_entity_decode("<th><b>M&aacute;s de 90</b></th></tr>")
							;

					}
					if($days == '30'){
						$invoices.="<th><b>Hasta 30</b></th></tr>";
					}
					if($days == '60'){
						$invoices.="<th><b>Hasta 60</b></th></tr>";
					}
					if($days == '90'){
						$invoices.="<th><b>Hasta 90</b></th></tr>";
					}
					if($days == '90+'){
						$invoices.="<th><b>M�s de 90</b></th></tr>";
					}
				}

				$nameInv = ''; $phoneInv = '';
				if($inv['serial_dea']){
					$nameInv = $inv['name_dea'];
					$phoneInv = $inv['phone1_dea'];
				}
				else{
					$nameInv = $inv['first_name_cus'].' '.$inv['last_name_cus'];
					$phoneInv = $inv['phone1_cus'];
				}

				$daysNum = 0;
				if($inv['diff'] > 0){
					$daysNum = $inv['diff'];
				}

				$ammount30 = 0; $ammount60 = 0; $ammount90 = 0; $ammount91 = 0;
				if($inv['diff'] <= 30){
					$ammount30 = number_format($inv['total_inv'], 2, '.', '');
				}
				if($inv['diff'] > 30 && $inv['diff'] <= 60){
					$ammount60 = number_format($inv['total_inv'], 2, '.', '');
				}
				if($inv['diff'] > 60 && $inv['diff'] <= 90){
					$ammount90 = number_format($inv['total_inv'], 2, '.', '');
				}
				if($inv['diff'] > 90){
					$ammount91 = number_format($inv['total_inv'], 2, '.', '');
				}
				$card='&nbsp;'.$inv['number_inv'].' / ';
				if($inv['card_number_sal']){
					$card.=$inv['card_group'];
				}
				else{
					$card.='N/A';
				}
				$amm30 += $ammount30; $amm60 += $ammount60; $amm90 += $ammount90; $amm91 += $ammount91;
				$invoices.="<tr><th>".$inv['date_inv']."</th>".
								"<th>".$card."</th>".
								"<th>".$nameInv." / ".'&nbsp;'.$phoneInv."</th>".
								"<th>".$daysNum."</th>";
				if(!$days){
					$invoices.="<th>".$ammount30."</th>".
								"<th>".$ammount60."</th>".
								"<th>".$ammount90."</th>".
								"<th>".$ammount91."</th></tr>"
								;					
				}
				if($days == '30'){
					$invoices.="<th>".$ammount30."</th></tr>";
				}
				if($days == '60'){
					$invoices.="<th>".$ammount60."</th></tr>";
				}
				if($days == '90'){
					$invoices.="<th>".$ammount90."</th></tr>";
				}
				if($days == '90+'){
					$invoices.="<th>".$ammount91."</th></tr>";
				}				
				
				if($inv['serial_bra']!=$invoicesRaw[$key+1]['serial_bra']){
					$invoices.="<tr><th></th>".
								"<th></th>".
								"<th></th>".
								"<th>TOTAL</th>";
					if(!$days){
						$invoices.="<th>".$amm30."</th>".
								"<th>".$amm60."</th>".
								"<th>".$amm90."</th>".
								"<th>".$amm91."</th></tr></table>";
					}
					if($days == '30'){
						$invoices.="<th>".$amm30."</th></tr></table>";
					}
					if($days == '60'){
						$invoices.="<th>".$amm60."</th></tr></table>";
					}
					if($days == '90'){
						$invoices.="<th>".$amm90."</th></tr></table>";
					}
					if($days == '90+'){
						$invoices.="<th>".$amm91."</th></tr></table>";
					}
					
					echo $invoices;
				}
				$branch = $inv['serial_bra'];
            }			
}else{
    http_redirect('modules/reports/payments/fAccountsReceivable');
}
?>