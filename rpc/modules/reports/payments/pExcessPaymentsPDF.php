<?php

if(isset($_POST['selCountry']) && isset($_POST['selManager'])){
   
	$payment = new Payment($db);
	$paymentsRaw = array();
    $payments = array();

	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
    $serial_usr = (isset($_POST['selUser'])) ? $_POST['selUser'] : '';
	
        $reportTitle = '<b>Reporte de Pagos en Exceso</b>';
		$paymentsRaw = $payment->getExcessPayments($serial_cou,	$serial_mbc,$_SESSION['serial_mbc'],
													$_SESSION['dea_serial_dea'], $serial_dea,$serial_bra,$status,
													$serial_usr);
		$cont=0;
		foreach($paymentsRaw as $pay){
			$paymentsRaw[$cont]['invoice_group'] = explode(",", $pay['invoice_group']);
			$paymentsRaw[$cont]['card_group'] = explode(",", $pay['card_group']);
			$paymentsRaw[$cont]['inv_serial_dea'] = explode(",", $pay['inv_serial_dea']);
			$cont++;
		}
        //debug::print_r($paymentsRaw);die();
        if(!$paymentsRaw){
            http_redirect('modules/reports/payments/fExcessPayments');
        }
            $table1 = array(
				'payment'=>'<b># Pago</b>',
				'toPay'=>'<b>A Pagar</b>',
				'payed'=>'<b>Pagado</b>',
				'excess'=>'<b>En Exceso</b>',
				'date'=>'<b>Fecha Pago</b>',
				'branch'=>'<b>Comercializador</b>'
			);
			$alignTable1 = array(
				'payment'=>array('justification'=>'center','width'=>50),
				'toPay'=>array('justification'=>'center','width'=>50),
				'payed'=>array('justification'=>'center','width'=>50),
				'excess'=>array('justification'=>'center','width'=>50),
				'date'=>array('justification'=>'center','width'=>130),
				'branch'=>array('justification'=>'center','width'=>130)
			);
			$optionsTable1 = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 10,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignTable1,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);
			$table2 = array(
				'invoice'=>'<b># Factura</b>',
				'card'=>'<b># Tarjeta</b>',
				'liquidationDate'=>'<b>Fecha Liquidaci�n</b>',
				'liquidationUser'=>'<b>Usuario</b>'
			);
			$alignTable2 = array(
				'invoice'=>array('justification'=>'center','width'=>60),
				'card'=>array('justification'=>'center','width'=>60),
				'liquidationDate'=>array('justification'=>'center','width'=>130),
				'liquidationUser'=>array('justification'=>'center','width'=>100),
			);
			$optionsTable2 = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 10,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignTable2,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);

			$requiredFields = array(
									array('country'=>'<b>Pa�s: </b>'.$paymentsRaw[0]['name_cou'],'manager'=>'<b>Representante: </b>'.$paymentsRaw[0]['name_man'])
									);
			$parameters = array();
			$col=0;
			if($serial_dea){
				$parameters[0][$col]='<b>Comercializador: </b>'.$paymentsRaw[0]['name_dea'];
				$col=$col+1;
			}
			if($serial_bra){
				$parameters[0][$col]='<b>Sucursal: </b>'.$paymentsRaw[0]['name_bra'];
				$col=$col+1;
			}
			if($status){
				if($status == 'Available'){
					$parameters[0][$col]='<b>Estado: Disponibles</b>';
				}
				else{
					$parameters[0][$col]='<b>Estado: Liquidados</b>';
				}
				$col=$col+1;
			}
			if($serial_usr){
				$parameters[0][$col]='<b>Liquidado Por: </b>'.($paymentsRaw[0]['first_name_usr'].' '.$paymentsRaw[0]['last_name_usr']);
			}

			include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

			$all = $pdf->openObject();
			$pdf->saveState();
			$pdf->addText(50,50,10,'*Las tarjetas N/A son del tipo corporativo, � no generan ning�n n�mero al momento de la venta.');
			$pdf->restoreState();
			$pdf->closeObject();
			$pdf->addObject($all,'all');

			$pdf->ezSetDy(-20);
			$pdf->ezText($reportTitle,14,array('justification'=>'center'));
			$pdf->ezSetDy(-20);
			$pdf->ezTable($requiredFields,array('country'=>'cou','manager'=>'man'),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13,
									'cols'=>array(
									'country'=>array('justification'=>'center','width'=>270),
									'manager'=>array('justification'=>'center','width'=>270)
            )			));
			$pdf->ezSetDy(-20);
			$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 13));
			$pdf->ezSetDy(-20);
			$dealer = '';
			$rowsTable1 = array();
			$rowsTable2 = array();
            foreach ($paymentsRaw as $key=> $pay) {				
				if($pay['serial_dea'] != $dealer){
					$pdf->ezText('<b>Pagos en Exceso del Comercializador: </b>'.$pay['name_dea'],12,array('justification'=>'center'));
					$pdf->ezSetDy(-20);
				}
				$rowsTable1[] = array(
					'payment'=>$pay['serial_pay'],
					'toPay'=>number_format($pay['total_to_pay_pay'], 2, '.', ''),
					'payed'=>number_format($pay['total_payed_pay'], 2, '.', ''),
					'excess'=>number_format(($pay['total_payed_pay'] - $pay['total_to_pay_pay']), 2, '.', ''),
					'date'=>$pay['date_pay'],
					'branch'=>$pay['name_bra']
				);
				$pdf->ezTable($rowsTable1, $table1, NULL,$optionsTable1);
				unset($rowsTable1);
				$pdf->ezSetDy(-20);
				
				foreach($pay['inv_serial_dea'] as $key2=> $p){
					$rowsTable2[] = array(
						'invoice'=>$pay['invoice_group'][$key2],
						'card'=>$pay['card_group'][$key2],
						'liquidationDate'=>$pay['payment_date_xpl'],
						'liquidationUser'=>$pay['first_name_usr'].' '.$pay['last_name_usr']
					);
				}
				$table2Copy = $table2;
				if($status == 'Available'){
					unset($table2Copy['liquidationDate']);
					unset($table2Copy['liquidationUser']);
				}
				$pdf->ezTable($rowsTable2, $table2Copy, NULL,$optionsTable2);
				unset($rowsTable2);
				$pdf->ezSetDy(-20);
				$dealer = $pay['serial_dea'];
            }			
			$pdf->ezStream();
}else{
    http_redirect('modules/reports/payments/fExcessPayments');
}


?>