<?php
/*
File:pViewPromoPDF
Author: Esteban Angulo
Creation Date: 02/07/2010
*/
Request::setInteger('0:serial_prm');
$promo=new Promo($db);
$promo->setSerial_prm($serial_prm);
$promo->getData();
global $global_promoTypes,$global_saleTypes,$global_periodTypes,$global_weekDaysNumb,$global_repeatTypes;

if ($promo->getNotifications_prm()=='0'){
	$notify='No';
}
elseif($promo->getNotifications_prm()=='1'){
	$notify='Si';
}

if ($promo->getWeekends_prm()=='NO'){
	$weekend='No';
}
elseif($promo->getWeekends_prm()=='YES'){
	$weekend='Si';
}

if ($promo->getRepeat_prm()=='0'){
	$repeat='No';
}
elseif($promo->getRepeat_prm()=='1'){
	$repeat='Si';
}

$promoDays=explode(',', $promo->getPeriodDays_prm());
$days=array();
foreach ($promoDays as $key=>$l){
	$days[$key]=html_entity_decode($global_weekDaysNumb[$l]);

}
$daysAll=implode(', ', $days);

include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
$pdf->ezText("<b>".utf8_decode("Reporte de la Promoción: ").$promo->getName_prm()."</b>", 12, array('justification' =>'center'));
$pdf->ezSetDy(-10);


$headerDesc=array();
	$descLine1=array('col1'=>utf8_decode('<b>Descripción:</b>'),
					 'col2'=>$promo->getDescripction_prm(),
					 'col3'=>utf8_decode('<b>Condiciones:</b>'),
					 'col4'=>$global_promoTypes[$promo->getType_prm()].'
'.utf8_decode('<b>Envío de Notitificaciones:</b>').' '.$notify);
	array_push($headerDesc,$descLine1);
	$pdf->ezTable($headerDesc,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>1,
						'xPos'=>'300',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>85),
							'col2'=>array('justification'=>'left','width'=>150),
							'col3'=>array('justification'=>'left','width'=>85),
							'col4'=>array('justification'=>'left','width'=>150))));

	$headerDesc=array();
	$descLine2=array('col1'=>utf8_decode('<b>Fecha de Inicio:</b>'),
					 'col2'=>$promo->getBeginDate_prm(),
					 'col3'=>utf8_decode('<b>Fecha de Fin:</b>'),
					 'col4'=>$promo->getEndDate_prm());
	array_push($headerDesc,$descLine2);

	$pdf->ezTable($headerDesc,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>1,
						'xPos'=>'300',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>85),
							'col2'=>array('justification'=>'left','width'=>150),
							'col3'=>array('justification'=>'left','width'=>85),
							'col4'=>array('justification'=>'left','width'=>150))));
	$headerDesc=array();
	$descLine3=array('col1'=>utf8_decode('<b>Valor de la Venta:</b>'),
					 'col2'=>$global_saleTypes[$promo->getTypeSale_prm()],
					 'col3'=>utf8_decode('<b>Días Aplicables:</b>'),
					 'col4'=>html_entity_decode($global_periodTypes[$promo->getPeriodType_prm()]));
	array_push($headerDesc,$descLine3);

	$pdf->ezTable($headerDesc,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>1,
						'xPos'=>'300',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>85),
							'col2'=>array('justification'=>'left','width'=>150),
							'col3'=>array('justification'=>'left','width'=>85),
							'col4'=>array('justification'=>'left','width'=>150))));

	$headerDesc=array();
	$descLine4=array('col1'=>utf8_decode('<b>Se repite la Promoción:</b>'),
					 'col2'=>$repeat.'
'.'<b>Periodicidad: </b>'.$global_repeatTypes[$promo->getRepeatType_prm()].'
'.utf8_decode('<b>Día de la Promoción: </b>').$daysAll,
					 'col3'=>utf8_decode('<b>Aplica Fin de Semana:</b>'),
					 'col4'=>$weekend);
	array_push($headerDesc,$descLine4);

	$pdf->ezTable($headerDesc,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>1,
						'xPos'=>'300',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>85),
							'col2'=>array('justification'=>'left','width'=>150),
							'col3'=>array('justification'=>'left','width'=>85),
							'col4'=>array('justification'=>'left','width'=>150))));
	$pdf->ezSetDy(-10);

$pdf->ezStream();
?>
