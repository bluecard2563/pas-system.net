<?php
/*
File:fInvoiceReport
Author: Esteban Angulo
Creation Date: 
*/
$zone=new Zone($db);
$nameZoneList=$zone->getZones();

$documentByCountry = new DocumentByCountry($db);
$purposeList = $documentByCountry->getDocumentTypeValues_dbc($db);

$smarty->register('nameZoneList,purposeList,global_typeManager');
$smarty->display();
?>
