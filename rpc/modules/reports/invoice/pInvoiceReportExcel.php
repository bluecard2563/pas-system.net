<?php
/*
	File: pInvoiceReportExcel
	Author: Santiago Borja
	Creation Date: 10/06/2010
	Description: Generates an Excel file with all invoices
*/

	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selCity');
	Request::setString('selManager');
	Request::setString('selResponsable');
	Request::setString('selDealer');
	Request::setString('selBranch');
	Request::setString('selType');
	Request::setString('selAmount');
	Request::setString('txtAmount');
	Request::setString('txtStartDate');
	Request::setString('txtEndDate');
	global $global_invoice_status;
	$invoices = Invoice::getInvoicesReport($db,$selCountry, $selManager,$selDealer,$selBranch,$selResponsable,$selType,$txtStartDate,$txtEndDate,$selAmount,$txtAmount);
	if(is_array($invoices)) {
		foreach($invoices as $key=>&$i){
			$i['status_inv']=$global_invoice_status[$i['status_inv']];
		}
	}
	$colspan = 7;
	
	$country = new Country($db, $selCountry);
	$country -> getData();


	$mbc=new ManagerbyCountry($db, $selManager);
	$mbc->getData();
	$manager = new Manager($db, $mbc->getSerial_man());
	$manager -> getData();
	
	if($selResponsable){
		$responsible = new User($db, $selResponsable);
		$responsible -> getData();
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Responsable:</b></td>
					<td colspan='2' align='left'>".utf8_decode($responsible -> getFirstname_usr().' '.$responsible -> getLastname_usr())."</td></tr>";
	}else{
		$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Responsable</b></td>";
		$colspan ++;
	}
	if($selDealer){
		$dealer = new Dealer($db, $selDealer);
		$dealer -> getData();
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Comercializador:</b></td>
					<td colspan='2' align='left'>".utf8_decode($dealer -> getName_dea())."</td></tr>";
	}else{
		$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Comercializador</b></td>";
		$colspan ++;
	}
	if($selBranch){
		$branch = new Dealer($db, $selBranch);
		$branch -> getData();
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Sucursal:</b></td>
					<td colspan='2' align='left'>".utf8_decode($branch -> getName_dea())."</td></tr>";
	}else{
		$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Sucursal</b></td>";
		$colspan ++;
	}
	if($selType){
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Estado:</b></td>
								<td colspan='2' align='left'>".$global_invoice_status[$selType]."</td></tr>";
	}else{
		$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Estado</b></td>";
		$colspan ++;
	}
	if($selAmount){
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Valor:</b></td>
					<td colspan='2' align='left'>".$selAmount.' '.$txtAmount."</td></tr>";
	}else{
		$colspan ++;
	}
	
	$total = 0;
	if(is_array($invoices)) {
		foreach($invoices as $t){
			$total += $t['total_inv'];
		}
	}

	
	$table.="<table border='0'>
				<tr><td colspan='".$colspan."' align='center'><b>Sistema Blue Card</b></td></tr>
				<tr><td colspan='".$colspan."' align='center' style='font-size: 10px; border-bottom: 1px solid #000000;'>Quito, ".date("F j, Y, g:i a")."</td></tr>
				<tr><td colspan='".$colspan."' align='center'><b>Reporte de Facturaci&oacute;n desde ".$txtStartDate." hasta ".$txtEndDate." </b></td></tr>
				<tr><td colspan='".$colspan."' align='center'>&nbsp;</td></tr>
				<tr><td colspan='".$colspan."' align='center'>&nbsp;</td></tr>
			</table>";
	
	$table.="<table border='0'>
				<tr><td colspan='2' align='left'><b>".utf8_decode('<b>País:</b>')."</b></td>
					<td colspan='2' align='left'>".utf8_decode($country -> getName_cou())."</td></tr>
				<tr><td colspan='2' align='left'><b>Representante:</b></td>
					<td colspan='2' align='left'>".utf8_decode($manager -> getName_man())."</td></tr>	
				".$variable_filters."
				<tr><td colspan='2' align='left'><b>Fecha de Inicio:</b></td>
					<td colspan='2' align='left'>".$txtStartDate."</td></tr>
				<tr><td colspan='2' align='left'><b>Fecha Fin:</b></td>
					<td colspan='2' align='left'>".$txtEndDate."</td></tr>
				<tr><td colspan='2' align='left'><b>Total:</b></td>
					<td colspan='2' align='left'>$".$total."</td></tr>
				<tr><td colspan='".$colspan."' align='left'>&nbsp;</td></tr>
			</table>";
	
	if($invoices){
		//TABLE HEADER:
		$table.="<table border='1'>
				<tr>
					<td style='background-color: #CCCCCC;' align='center'><b># Fac</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b># Tarjeta</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Cant.</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Fec. Emision</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Cliente</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Facturado a</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Valor</b></td>
					".$variable_rows."
				</tr>";
		foreach($invoices as $key=>$inv){
			$variable_results = '';
			if(!$selResponsable){
				$variable_results .= "<td  align='center'>".$inv['name_rep']."</td>";
			}
			if(!$selDealer){
				$variable_results .= "<td  align='center'>".$inv['name_dea']."</td>";
			}
			if(!$selBranch){
				$variable_results .= "<td  align='center'>".$inv['name_man']."</td>";
			}
			if(!$selType){
				$variable_results .= "<td  align='center'>".$inv['status_inv']."</td>";
			}
			
			$table.="<tr>
						<td  align='center'>".$inv['number_inv']."</td>
						<td  align='center'>".$inv['card_number_sal']."</td>
						<td  align='center'>".$inv['qty']."</td>
						<td  align='center'>".$inv['date_inv']."</td>
						<td  align='center'>".utf8_decode($inv['name_cus'])."</td>
						<td  align='center'>".$inv['facturado_a']."</td>
						<td  align='center'>".$inv['total_inv']."</td>
						".$variable_results."
					</tr>";
		}
		$table.="</table>";
		
	}else{
		$table.="<b>No existen clientes registrados</b>";
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=ReporteClientes.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $header;
	echo $table;
?>