<?php
/*
File:fInvoiceReport
Author: Esteban Angulo
Creation Date: 
*/
$zone=new Zone($db);
$nameZoneList=$zone->getZones();
$customer=new Customer($db);
$custStatusList=$customer->getAllStatus();
$invoice=new Invoice($db);
$invStatusList=$invoice->getAllStatus();

$smarty->register('nameZoneList,custStatusList,invStatusList');
$smarty->display();
?>
