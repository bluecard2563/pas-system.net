<?php
/*
File:pCustomerReportExcel
Author: Esteban Angulo
Creation Date: 28/05/2010
Description: Generates an Excel file with all customers by the filters applied

*/

set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('selCountry');
Request::setString('selCity');
Request::setString('selStatus');
$customer=new Customer($db);
$data=$customer->getCustomersForReport($selCountry, $selCity, $selStatus);
$country=new Country($db, $selCountry);
$country->getData();
$zone=new Zone($db, $country->getSerial_zon());
$zone->getData();
$allCities=0;
$allStatus=0;
if($selCity!=''){
	$city=new City($db, $selCity);
	$city->getData();
	$cityName=$city->getName_cit();
	$allCities=1;
}else{
	$cityName='Todos';
	$allCities=2;
}
if($selStatus!=''){
	if($selStatus=='ACTIVE'){
		$status='Regular';
	}
	elseif($selStatus=='INACTIVE'){
		$status='Lista Negra';
	}
	$allStatus=1;
}
else{
	$status='Todos';
	$allStatus=2;
}
if($allCities==2 && $allStatus==2){
	$colspan=11;
}
elseif($allCities==2 && $allStatus!=2){
	$colspan=10;
}
if($allCities!=2 && $allStatus==2){
	$colspan=10;
}
elseif($allCities==1 && $allStatus==1){
	$colspan=9;
}
	//$customers=array();
if(is_array($data)){
	$customers=array();
	foreach($data as $key=>$c){
		$customers[$key]['document_cus']=$c['document_cus'];
		$customers[$key]['name_cus']=$c['complete_name'];
		$customers[$key]['birthdate_cus']=$c['birthdate_cus'];
		if($c['phone1_cus']=='' ){
			$customers[$key]['phone1_cus']='-';
		}
		else{
			$customers[$key]['phone1_cus']=$c['phone1_cus'];
		}
		if($c['cellphone_cus']=='' ){
			$customers[$key]['cellphone_cus']='-';
		}
		else{
			$customers[$key]['cellphone_cus']=$c['cellphone_cus'];
		}
		$customers[$key]['email_cus']=$c['email_cus'];
		if($c['relative_cus']==''){
			$customers[$key]['relative_cus']='-';
		}
		else{
			$customers[$key]['relative_cus']=$c['relative_cus'];
		}
		if($c['phone_relative_cus']==''){
			$customers[$key]['phone_relative_cus']='-';
		}
		else{
			$customers[$key]['phone_relative_cus']=$c['relative_phone_cus'];
		}
		if($c['vip_cus']=='0' || $c['vip_cus']=='' ){
			$customers[$key]['vip_cus']='NO';
		}
		else{
			$customers[$key]['vip_cus']='SI';
		}
		if($allStatus==2){
			if($c['status_cus']=='ACTIVE'){
				$customers[$key]['status_cus']='Regular';
			}
			elseif($c['status_cus']=='INACTIVE'){
				$customers[$key]['status_cus']='Lista Negra';
			}
		}
		if($allCities==2){
			$customers[$key]['city_cus']=$c['name_cit'];
		}
	}
$header="<br><table border='1'>
			<tr>
				<td colspan='2' align='center'><b>Datos del Reporte de Clientes</b></td>
			</tr>
			<tr>
				<td  align='center'><b>Zona</b></td>
				<td	 align='center'>".$zone->getName_zon()."</td>
			</tr>
			<tr>
				<td  align='center'>".utf8_decode('<b>Pa&iacute;s</b>')."</td>
				<td	 align='center'>".$country->getName_cou()."</td>
			</tr>
			<tr>
				<td  align='center'><b>Ciudad</b></td>
				<td	 align='center'>".$cityName."</td>
			</tr>
			<tr>
				<td  align='center'><b>Estado</b></td>
				<td	 align='center'>".$status."</td>
			</tr>
		  </table>";
$table.="<br>";
$table.="<table border='1'>
		<tr>
			<td colspan='".$colspan."' align='center'><b>Reporte de Clientes</b></td>
		</tr>
		<tr>
			<td  align='center'><b>Documento</b></td>
			<td  align='center'><b>Nombre y Apellido</b></td>
			<td  align='center'><b>Fecha de Nacimiento</b></td>
			<td  align='center'>".utf8_decode('<b>Teléfono</b>')."</td>
			<td  align='center'><b>Celular</b></td>
			<td  align='center'><b>E-Mail</b></td>";
if($allCities==2){
	$table.="<td  align='center'><b>Ciudades</b></td>";
}
	$table.="<td  align='center'><b>Persona de Contacto</b></td>
			<td  align='center'>".utf8_decode('<b>Teléfono de Contacto</b>')."</td>
			<td  align='center'><b>Cliente VIP</b></td>";
if($allStatus==2){
	$table.="<td  align='center'><b>Estado</b></td>";
}
foreach($customers as $key=>$c){
$table.="   	<tr>
					<td  align='center'>&nbsp;".$customers[$key]['document_cus']."</td>
					<td  align='center'>".$customers[$key]['name_cus']."</td>
					<td  align='center'>".$customers[$key]['birthdate_cus']."</td>
					<td  align='center'>&nbsp;".$customers[$key]['phone1_cus']."</td>
					<td  align='center'>&nbsp;".$customers[$key]['cellphone_cus']."</td>
					<td  align='center'>".$customers[$key]['email_cus']."</td>";
if($allCities==2){
	$table.="<td  align='center'>".$customers[$key]['city_cus']."</td>";
}
	$table.="		<td  align='center'>".$customers[$key]['relative_cus']."</td>
					<td  align='center'>".$customers[$key]['phone_relative_cus']."</td>
					<td  align='center'>".$customers[$key]['vip_cus']."</td>";
if($allStatus==2){
	$table.="<td  align='center'>".$customers[$key]['status_cus']."</td>";
}

$table.="		</tr>";
}
$table.="	</tr>

		</table>";
}
else{
	$table.="<b>No existen clientes registrados</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteClientes.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $header;
echo $table;
?>
