<?php
/*
File:fRefunds.php
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Modified By: 
Last Modified: 
*/
Request::setInteger('0:error');

//Countries list
$county = new Country($db);
$countriesList = $county->getCountriesWithRefunds($_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty -> register('countriesList,error');
$smarty->display();
?>