<?php

if( ( isset($_POST['txtCustomerData']) ) || ( isset($_POST['selCountry']) && isset($_POST['selManager']) ) ){

	$refund = new Refund($db);
	$refundsRaw = array();
    $refunds = array();

	if($_POST['hdnFlag']==2){
		$serial_cou = $_POST['selCountry'];
		$serial_man = $_POST['selManager'];
		$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
		$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
		$serial_usr = (isset($_POST['selResponsible'])) ? $_POST['selResponsible'] : '';
		$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
		$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
		$dateFrom = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
		$dateTo = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';

		$refundsRaw = $refund->getRefundsByManager($serial_cou,$serial_man,
													$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'],
													$status,$serial_cit,$serial_usr,$serial_dea,$serial_bra,
													$dateFrom,$dateTo);
		}
	else{
		$serial_cus = $_POST['hdnCustomerID'];
		$refundsRaw = $refund->getRefundsByCustomer($serial_cus,$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
	}
        //debug::print_r($refundsRaw);die();

        if(!$refundsRaw){
            http_redirect('modules/reports/refund/fRefunds');
        }
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=Reporte_de_Reembolsos.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
		$title = "<table>
						<tr><td colspan=6>".$date."</td></tr><tr></tr>
						<tr><th></th>
							<th><b>Reporte de Reembolsos</b></th>
						 </tr>
						<tr></tr>
				 </table>";
		echo $title;
		$statusRefund = 'Todos'; $responsible = 'Todos'; $city = 'Todos'; $dealer = 'Todos'; $branch = 'Todos'; $date_from = 'Todos'; $date_to = 'Todos';
		$parameters = '';
		if($_POST['hdnFlag']==2){
			if($serial_cit){
				$city = $refundsRaw[0]['name_cit'];
			}
			if($serial_usr){
				$responsible = $refundsRaw[0]['name_usr'];
			}
			if($serial_dea){
				$dealer = $refundsRaw[0]['name_dea'];
			}
			if($serial_bra){
				$branch = $refundsRaw[0]['name_bra'];
			}
			if($dateFrom && $date_to){
				$date_from = $dateFrom;
				$date_to = $date_to;
			}
			if($status){
				$statusRefund = $global_refundStatus[$refundsRaw[0]['status_ref']];
			}
			$parameters = "<table>
								<tr>
									<td align='right'><b>".utf8_decode(País).": </b></td><td>".$refundsRaw[0]['name_cou']."</td>
									<td align='right'><b>Representante:</b></td><td>".$refundsRaw[0]['name_man']."</td>
								</tr>
								<tr>
									<td align='right'><b>Ciudad:</b></td><td>".$city."</td>
									<td align='right'><b>Responsable:</b></td><td>".$responsible."</td>
								</tr>
								<tr>
									<td align='right'><b>Comercializador:</b></td><td>".$dealer."</td>
									<td align='right'><b>Sucursal:</b></td><td>".$branch."</td>
								</tr>
								<tr>
									<td align='right'><b>Fecha Desde:</b></td><td>".$date_from."</td>
									<td align='right'><b>Fecha Hasta:</b></td><td>".$date_to."</td>
								</tr>
								<tr>
									<td align='right'><b>Estado:</b></td><td>".$statusRefund."</td>
								</tr>
							</table>";
			echo $parameters;
		}
		else{
			$parameter = "<table><tr><td align='right'><b>Cliente: </b></td><td>".$refundsRaw[0]['name_cus']."</td></tr></table>";
			echo $parameter;
		}
			

			$dealer = '';
			$rowsTable = "<table border='1'>";
            foreach ($refundsRaw as $key => $ref) {
				if($ref['serial_dea'] != $dealer){
					$header = "<table><tr></tr><tr></tr><tr><th></th><th><b>Reembolsos en el Comercializador: </b></th><th>".$ref['name_dea']."</th></tr></table>";
					echo $header;
					$rowsTable .= "<tr><th><b>Fecha Inicio</b></th>";
					if($_POST['hdnFlag']==2){
						$rowsTable .= "<th><b>Cliente</b></th>";
					}
					if(!$serial_bra){
						$rowsTable .= "<th><b>Sucursal</b></th>";
					}
					$rowsTable .= "<th><b># Tarjeta</b></th>
								<th><b># Nota ".utf8_decode(Crédito)."</b></th>";
					if(!$status){
						$rowsTable .= "<th><b>Estado</b></th>";
					}
					$rowsTable .= "<th><b>Valor Tarjeta</b></th>
								<th><b>Valor Retenido</b></th>
								<th><b>Valor a Pagar</b></th>
								<th><b>Fecha ".utf8_decode(Autorización)."</b></th>
								</tr>";
				}
				$retained = '';
				if($ref['retained_type_ref'] == 'PERCENTAGE'){
					$retained = number_format($ref['retained_value_ref'], 0, '.', '').'%';
				}
				else{
					$retained = number_format($ref['retained_value_ref'], 2, '.', '');
				}

				$rowsTable .= "<tr><td align='center'>".$ref['request_date_ref']."</td>";
				if($_POST['hdnFlag']==2){
					$rowsTable .= "<td align='center'>".$ref['name_cus']."</td>";
				}
				if(!$serial_bra){
					$rowsTable .= "<td align='center'>".$ref['name_bra']."</td>";
				}
				$rowsTable .= "<td align='center'>".$ref['card_number_sal']."</td>
							<td align='center'>".$ref['number_cn']."</td>";
				if(!$status){
					$rowsTable .= "<td align='center'>".$global_refundStatus[$ref['status_ref']]."</td>";
				}
				$rowsTable .= "<td align='center'>".number_format($ref['total_sal'], 2, '.', '')."</td>
							<td align='center'>".$retained."</td>
							<td align='center'>".number_format($ref['total_to_pay_ref'], 2, '.', '')."</td>
							<td align='center'>".$ref['auth_date_ref']."</td>
							</tr>";

				if($refundsRaw[$key]['serial_dea'] != $refundsRaw[$key+1]['serial_dea']){
					$rowsTable .= "</table>";
					echo $rowsTable;
					$rowsTable = "<table border='1'>";
				}
				$dealer = $ref['serial_dea'];
            }
}else{
    http_redirect('modules/reports/refund/fRefunds');
}


?>