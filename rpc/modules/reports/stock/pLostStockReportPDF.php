<?php
/*
    Document   : pLostStockReportPDF
    Created on : 24-jun-2010, 16:52:15
    Author     : Nicolas
    Description:
       Creates a PDF report of lost stock based on the parameters recieved
*/

Request::setInteger('selCountry,selManager,selResponsable,selDealer,selBranch,selCounter');
Request::setString('txtDateFrom,txtDateTo');
//check for filters applied
$filters_applied=array();
if($selCountry){
	$country=new Country($db,$selCountry);
	$country->getData();
	$filters_applied['country']=$country->getName_cou();
}else{
	$filters_applied['country']='Todos';
}
if($selManager){
	$managerByC=new ManagerbyCountry($db,$selManager);
	$managerByC->getData();
	$manager=new Manager($db,$managerByC->getSerial_man());
	$manager->getData();
	$filters_applied['manager']=$manager->getName_man();
}else{
	$filters_applied['manager']='Todos';
}
if($selResponsable){
	$user=new User($db,$selResponsable);
	$user->getData();
	$filters_applied['responsible']=$user->getFirstname_usr().' '.$user->getLastname_usr();
}else{
	$filters_applied['responsible']='Todos';
}
if($selDealer){
	$dealer=new Dealer($db,$selDealer);
	$dealer->getData();
	$filters_applied['dealer']=$dealer->getName_dea();
}else{
	$filters_applied['dealer']='Todos';
}
if($selBranch){
	$branch=new Dealer($db,$selBranch);
	$branch->getData();
	$filters_applied['branch']=$branch->getName_dea();
}else{
	$filters_applied['branch']='Todas';
}
if($selCounter){
	$counter=new Counter($db,$selCounter);
	$counter->getData();
	$user= new User($db,$counter->getSerial_usr());
	$user->getData();
	$filters_applied['counter']=$user->getFirstname_usr().' '.$user->getLastname_usr();
}else{
	$filters_applied['counter']='Todos';
}
if($txtDateFrom){
	$filters_applied['since']=$txtDateFrom;
}else{
	$filters_applied['since']='--';
}
if($txtDateTo){
	$filters_applied['until']=$txtDateTo;
}else{
	$filters_applied['until']='--';
}
//create the filters array to be used on a table.
$filtersData=array();
$line1=array('col1'=>'<b>'.utf8_decode('País:').'</b>','col2'=>utf8_decode($filters_applied['country']),'col3'=>'<b>Comercializador:</b>','col4'=>utf8_decode($filters_applied['dealer']));
array_push($filtersData, $line1);
$line2=array('col1'=>utf8_decode('<b>Representante:</b>'),'col2'=>utf8_decode($filters_applied['manager']),'col3'=>'<b>Sucursal:</b>','col4'=>utf8_decode($filters_applied['branch']));
array_push($filtersData, $line2);
$line3=array('col1'=>utf8_decode('<b>Responsable:</b>'),'col2'=>utf8_decode($filters_applied['responsible']),'col3'=>'<b>Counter:</b>','col4'=>utf8_decode($filters_applied['counter']));
array_push($filtersData, $line3);
$line4=array('col1'=>utf8_decode('<b>Fecha Desde:</b>'),'col2'=>utf8_decode($filters_applied['since']),'col3'=>'<b>Fecha Hasta:</b>','col4'=>utf8_decode($filters_applied['until']));
array_push($filtersData, $line4);
//var to save the unused(lost) stock
$lostStock=array();
$stock=new Stock($db);
//get the stock ranges based on the recieved parameters
$stockRanges=$stock->getManualStockRangesUsed($selManager,$selResponsable,$selDealer,$selBranch,$selCounter,$txtDateFrom,$txtDateTo);
if($stockRanges){
	//check each range to find the unused stock
	foreach($stockRanges as $sr){
		//get the sold cards by range.
		$soldCards=Sales::getSalesInRange($db,$sr['from_sto'],$sr['to_sto']);
		//evaluate each card in range tosee if it's sold or not
		for($i=$sr['from_sto'];$i<=$sr['to_sto'];$i++){
			if(!in_array($i, $soldCards)){
					$temp['card_number']=$i;
					$temp['city']=$sr['name_cit'];
					$temp['responsible']=$sr['responsible_name'];
					$temp['branch']=$sr['name_dea'];
					$temp['branch_code']=$sr['code_dea'];
					$temp['counter']=$sr['counter_name'];
					$temp['assigned_date']=$sr['date_sto'];
					$temp['assigned_by']=$sr['assigned_by'];
					array_push($lostStock, $temp);
			}
		}
	}
}
include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(800,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
$pdf->ezSetDy(-20);
//title
$pdf->ezText("<b>REPORTE DE STOCK SALTADO</b>", 12, array('justification' =>'center'));
$pdf->ezSetDy(-20);
$pdf->ezTable($filtersData,
				  array('col1'=>'',
					    'col2'=>'',
					    'col3'=>'',
					    'col4'=>''),
				  '<i><b>Filtros Aplicados</b></i>',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>150),
							'col2'=>array('justification'=>'left','width'=>150),
							'col3'=>array('justification'=>'left','width'=>150),
							'col4'=>array('justification'=>'left','width'=>150))));
$pdf->ezSetDy(-10);
if(sizeof($lostStock)>0){
	$pdf->ezTable($lostStock,
				  array('card_number'=>'<b>No. Tarjeta</b>',
					    'city'=>'<b>Ciudad</b>',
					    'responsible'=>'<b>Responsable</b>',
					    'branch'=>'<b>Sucursal</b>',
					    'branch_code'=>'<b>'.utf8_decode('Código').'</b>',
					    'counter'=>'<b>Counter</b>',
					    'assigned_date'=>'<b>'.utf8_decode('Fecha de Asignación').'</b>',
					    'assigned_by'=>'<b>Asignado por</b>'),
				  '',
				  array('xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'card_number'=>array('justification'=>'center','width'=>45),
							'city'=>array('justification'=>'center','width'=>60),
							'responsible'=>array('justification'=>'center','width'=>150),
							'branch'=>array('justification'=>'center','width'=>140),
							'branch_code'=>array('justification'=>'center','width'=>60),
							'counter'=>array('justification'=>'center','width'=>150),
							'assigned_date'=>array('justification'=>'center','width'=>60),
							'assigned_by'=>array('justification'=>'center','width'=>150))));
$pdf->ezSetDy(-10);
}else{
	$pdf->ezText(utf8_decode("No existe stock saltado dentro de los parámetros dados."), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>