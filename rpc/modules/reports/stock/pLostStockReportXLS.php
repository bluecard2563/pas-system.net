<?php
/*
    Document   : pLostStockReportPDF
    Created on : 24-jun-2010, 16:52:15
    Author     : Nicolas
    Description:
       Creates a XLS report of lost stock based on the parameters recieved
*/

Request::setInteger('selCountry,selManager,selResponsable,selDealer,selBranch,selCounter');
Request::setString('txtDateFrom,txtDateTo');
//check for filters applied
$filters_applied=array();
if($selCountry){
	$country=new Country($db,$selCountry);
	$country->getData();
	$filters_applied['country']=$country->getName_cou();
}else{
	$filters_applied['country']='Todos';
}
if($selManager){
	$managerByC=new ManagerbyCountry($db,$selManager);
	$managerByC->getData();
	$manager=new Manager($db,$managerByC->getSerial_man());
	$manager->getData();
	$filters_applied['manager']=$manager->getName_man();
}else{
	$filters_applied['manager']='Todos';
}
if($selResponsable){
	$user=new User($db,$selResponsable);
	$user->getData();
	$filters_applied['responsible']=$user->getFirstname_usr().' '.$user->getLastname_usr();
}else{
	$filters_applied['responsible']='Todos';
}
if($selDealer){
	$dealer=new Dealer($db,$selDealer);
	$dealer->getData();
	$filters_applied['dealer']=$dealer->getName_dea();
}else{
	$filters_applied['dealer']='Todos';
}
if($selBranch){
	$branch=new Dealer($db,$selBranch);
	$branch->getData();
	$filters_applied['branch']=$branch->getName_dea();
}else{
	$filters_applied['branch']='Todas';
}
if($selCounter){
	$counter=new Counter($db,$selCounter);
	$counter->getData();
	$user= new User($db,$counter->getSerial_usr());
	$user->getData();
	$filters_applied['counter']=$user->getFirstname_usr().' '.$user->getLastname_usr();
}else{
	$filters_applied['counter']='Todos';
}
if($txtDateFrom){
	$filters_applied['since']=$txtDateFrom;
}else{
	$filters_applied['since']='--';
}
if($txtDateTo){
	$filters_applied['until']=$txtDateTo;
}else{
	$filters_applied['until']='--';
}

//var to save the unused(lost) stock
$lostStock=array();
$stock=new Stock($db);
//get the stock ranges based on the recieved parameters
$stockRanges=$stock->getManualStockRangesUsed($selManager,$selResponsable,$selDealer,$selBranch,$selCounter,$txtDateFrom,$txtDateTo);
if($stockRanges){
	//check each range to find the unused stock
	foreach($stockRanges as $sr){
		//get the sold cards by range.
		$soldCards=Sales::getSalesInRange($db,$sr['from_sto'],$sr['to_sto']);
		//evaluate each card in range tosee if it's sold or not
		for($i=$sr['from_sto'];$i<=$sr['to_sto'];$i++){
			if(!in_array($i, $soldCards)){
					$temp['card_number']=$i;
					$temp['city']=$sr['name_cit'];
					$temp['responsible']=$sr['responsible_name'];
					$temp['branch']=$sr['name_dea'];
					$temp['branch_code']=$sr['code_dea'];
					$temp['counter']=$sr['counter_name'];
					$temp['assigned_date']=$sr['date_sto'];
					$temp['assigned_by']=$sr['assigned_by'];
					array_push($lostStock, $temp);
			}
		}
	}
}
$buffer.="<table border='1'>
		<tr>
			<td colspan='4' align='center'><b>REPORTE DE STOCK SALTADO</b></td>
		</tr>
		<tr>
			<td colspan='4' align='center'><b>Fitros Aplicados</b></td>
		</tr>
		<tr>
			<td  align='center'><b>".utf8_decode('País:')."</b></td>
			<td  align='center'>".utf8_decode($filters_applied['country'])."</td>
			<td  align='center'><b>Comercializador:</b></td>
			<td  align='center'>".utf8_decode($filters_applied['dealer'])."</td>
		</tr>
		<tr>
			<td  align='center'><b>Representante:</b></td>
			<td  align='center'>".utf8_decode($filters_applied['manager'])."</td>
			<td  align='center'><b>Sucursal:</b></td>
			<td  align='center'>".utf8_decode($filters_applied['branch'])."</td>
		</tr>
		<tr>
			<td  align='center'><b>Responsable:</b></td>
			<td  align='center'>".utf8_decode($filters_applied['responsible'])."</td>
			<td  align='center'><b>Counter:</b></td>
			<td  align='center'>".utf8_decode($filters_applied['counter'])."</td>
		</tr>
		<tr>
			<td  align='center'><b>Fecha Desde:</b></td>
			<td  align='center'>".$filters_applied['since']."</td>
			<td  align='center'><b>Fecha Hasta:</b></td>
			<td  align='center'>".$filters_applied['until']."</td>
		</tr>
		</table>
		<br/>";

if($lostStock){
	$buffer.="<table border='1'>
				<tr>
					<td  align='center'><b>No. Tarjeta</b></td>
					<td  align='center'><b>Ciudad</b></td>
					<td  align='center'><b>Responsable</b></td>
					<td  align='center'><b>Sucursal</b></td>
					<td  align='center'><b>".utf8_decode('Código')."</b></td>
					<td  align='center'><b>Counter</b></td>
					<td  align='center'><b>".utf8_decode('Fecha de Asignación')."</b></td>
					<td  align='center'><b>Asignado por</b></td>
				</tr>";
	foreach($lostStock as $ls){
		$buffer.="<tr>
					<td  align='center'>{$ls['card_number']}</td>
					<td  align='center'>".utf8_decode($ls['city'])."</td>
					<td  align='center'>".utf8_decode($ls['responsible'])."</td>
					<td  align='center'>".utf8_decode($ls['branch'])."</td>
					<td  align='center'>{$ls['branch_code']}</td>
					<td  align='center'>".utf8_decode($ls['counter'])."</td>
					<td  align='center'>{$ls['assigned_date']}</td>
					<td  align='center'>".utf8_decode($ls['assigned_by'])."</td>
				</tr>";

	}
	$buffer.="</table>";
	}else{
	$buffer.="<b>No existen stock saltado que cumpla con los filtros aplicados</b>";
}
header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=unusedStock.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $buffer;
?>
