<?php
/*
    Document   : fStockReport
    Created on : 27-may-2010, 9:54:58
    Author     : Nicolas
    Description:
       interface with filters to create an stock report
*/
//get list of existing zones
$zone = new Zone($db);
$nameZoneList=$zone->getZones();
//get the list of stock types
$stock=new Stock($db);
$typeList=$stock->getTypeValues();
// check the number of cards per type available to assign

$smarty -> register('nameZoneList,typeList');
$smarty -> display();
?>
