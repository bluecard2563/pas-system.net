<?php

if(  isset($_POST['selZone'])  &&  isset($_POST['selCountry']) && isset($_POST['selProduct']) ){

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$serial_pro = $_POST['selProduct'];
	$product = new ProductByCountry($db);
	$flag = true;
	$productsRaw = $product->getProductsPricesByCountry($serial_zon,$serial_cou,$serial_pro, $_SESSION['language']);
	$name_zon = $productsRaw[0]['name_zon'];//GETTING ZONE NAME
	$name_cou = $productsRaw[0]['name_cou'];//GETTING COUNTRY NAME
	$name_pro = $productsRaw[0]['name_pbl'];//GETTING PRODUCT NAME

	if(!$productsRaw){//IF THERE IS NO ACTIVE PRICES, GETTING REFERENCIAL PRICES TO DISPLAY
		$flag = false;
		$pbpbc = new PriceByProductByCountry($db);
		$productsRaw = $pbpbc->getPricesByProductByCountry($serial_pro, '1');
		$zone = new Zone($db, $serial_zon);
		$zone->getData();
		$name_zon = $zone->getName_zon();//GETTING ZONE NAME
		$country = new Country($db, $serial_cou);
		$country->getData();
		$name_cou = $country->getName_cou();//GETTING COUNTRY NAME
		$product = new ProductbyLanguage($db);
		$product->setSerial_pro($serial_pro);
		$product->setSerial_lang($_SESSION['serial_lang']);
		$product->getDataByProductAndLanguage();
		$name_pro = $product->getName_pbl();//GETTING PRODUCT NAME
	}
	//debug::print_r($productsRaw);
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Precios_Producto.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = "<table>
				<tr><td colspan=6>".$date."</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Precios por Producto</b></td></tr>
				<tr></tr>
			</table>";
	echo $title;
	//**PARAMETERS TABLE
	$parameters = array(
						array('0'=>'<b>Zona: </b>', '1'=>$name_zon),
						array('0'=>'<b>'.utf8_decode(País).':</b>', '1'=>$name_cou),
						array('0'=>'<b>Producto:</b>', '1'=>$name_pro)
						);
	$filters = "<table><tr>";
	foreach($parameters as $p){
		$filters .= "<td align='right'>".$p['0']."</td><td align='left'>".$p['1']."</td>";
	}
	$filters .= "</tr></table>";
	echo $filters;

	//**DISCOUNTS TABLE
	if($flag){//IF THERE ARE CURRENT ACTIVE PRICES
		if($productsRaw[0]['percentage_spouse_pxc'] != 'N/A'){
			$productsRaw[0]['percentage_spouse_pxc'] = number_format($productsRaw[0]['percentage_spouse_pxc'], 1, '.', '');
		}
		if($productsRaw[0]['percentage_extras_pxc'] != 'N/A'){
			$productsRaw[0]['percentage_extras_pxc'] = number_format($productsRaw[0]['percentage_extras_pxc'], 1, '.', '');
		}
		if($productsRaw[0]['percentage_children_pxc'] != 'N/A'){
			$productsRaw[0]['percentage_children_pxc'] = number_format($productsRaw[0]['percentage_children_pxc'], 1, '.', '');
		}
		$discounts = array(
							'0'=>array('0'=>'<b>Esposo/a</b>', '1'=>'<b>Extras</b>', '2'=>'<b>Hijos</b>', '3'=>'<b>'.utf8_decode(Días).' Adicionales</b>'),
							'1'=>array('0'=>$productsRaw[0]['percentage_spouse_pxc'],
										'1'=>$productsRaw[0]['percentage_extras_pxc'],
										'2'=>$productsRaw[0]['percentage_children_pxc'],
										'3'=>number_format($productsRaw[0]['aditional_day_pxc'], 1, '.', ''))
							);
		$discountsTable = "<table><tr></tr><tr><td colspan=6 align='center'><b>Descuentos (%)</b></td></tr>";
		foreach($discounts as $d){
			$discountsTable .= "<tr><td></td><td align='center'>".$d['0']."</td><td align='center'>".$d['1']."</td><td align='center'>".$d['2']."</td><td align='center'>".$d['3']."</td></tr>";
		}
		$discountsTable .= "</table>";
		echo $discountsTable;
	}
	$pricesTitle = "<table><tr></tr><tr align='center'><td colspan=6 align='center'>";
	if($flag){
		$pricesTitle .= "<b>Precios</b>";//ACTIVE PRICES TITLE
	}
	else{
		$pricesTitle .= "<b>Precios Referenciales";//REFERENCIAL PRICES TITLE
	}
	$pricesTitle .= "</td></tr><tr></tr></table>";
	echo $pricesTitle;

	$table = "<table><tr><td></td><td><table border=1><tr>
					<td align='center'><b>#</b></td><td align='center'><b>".utf8_decode(Días)."</b></td>
					<td align='center'><b>Precio</b></td><td align='center'><b>Costo</b></td>
				</tr>
			";
	foreach ($productsRaw as $key => $pro) {
		$price = number_format($pro['price_ppc'], 2, '.', '');
		if(!$pro['price_ppc']){
			$price = $pro['min_ppc'].' - '.$pro['max_ppc'];
		}
		$table .= "<tr><td align='center'>".($key+1)."</td>
						<td align='center'>".$pro['duration_ppc']."</td>
						<td align='center'>".$price."</td>
						<td align='center'>".number_format($pro['cost_ppc'], 2, '.', '')."</td>
					</tr>";
	}
	$table .= "</table></td></tr></table>";
	echo $table;
}else{
    http_redirect('modules/reports/product/pricesByProduct/fPricesByProduct');
}


?>