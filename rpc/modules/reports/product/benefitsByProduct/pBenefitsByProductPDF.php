<?php

if(  isset($_POST['selZone'])  &&  isset($_POST['selCountry']) && isset($_POST['selProduct']) ){

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$serial_pro = $_POST['selProduct'];

	$benefit = new BenefitsByProduct($db);
	$benefitRaw = $benefit->getBenefitsByProductReport($serial_zon,$serial_cou,$serial_pro,$_SESSION['serial_lang']);
	
	//debug::print_r($benefitRaw);
	$table = array(
		'num'=>'<b>#</b>',
		'name'=>'<b>Beneficio</b>',
		'coverage'=>'<b>Cobertura</b>',
		'restriction'=>'<b>'.utf8_decode(Restricción).'</b>'
	);
	$alignTable = array(
		'num'=>array('justification'=>'center','width'=>20),
		'name'=>array('justification'=>'center','width'=>250),
		'coverage'=>array('justification'=>'center','width'=>80),
		'restriction'=>array('justification'=>'center','width'=>80)
	);
	$optionsTable = array(
		'showLines'=>2,
		'showHeadings'=>1,
		'shaded'=>0,
		'fontSize' => 9,
		'textCol' =>array(0,0,0),
		'titleFontSize' => 12,
		'rowGap' => 5,
		'colGap' => 5,
		'lineCol'=>array(0.8,0.8,0.8),
		'xPos'=>'320',
		'xOrientation'=>'center',
		'maxWidth' => 805,
		'cols' =>$alignTable,
		'innerLineThickness' => 0.8,
		'outerLineThickness' => 0.8
	);
	include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$pdf->setColor(0.57, 0.58, 0.60);
	$pdf->ezText($date,13,array('justification'=>'left'));
	$pdf->setColor(0, 0, 0);
	$pdf->ezSetDy(-20);
	$pdf->ezText('<b>Reporte de Beneficios por Producto</b>',14,array('justification'=>'center'));
	$pdf->ezSetDy(-20);
	//**PARAMETERS TABLE
	$parameters = array(
						'0'=>array('0'=>'<b>Zona: </b>', '1'=>$benefitRaw[0]['name_zon'],
									'2'=>'<b>'.utf8_decode(País).':</b>', '3'=>$benefitRaw[0]['name_cou'],
									'4'=>'<b>Producto:</b>', '5'=>$benefitRaw[0]['name_pbl']
								)
						);
	$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>'','4'=>'','5'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'300',
						'cols'=>array(
						'0'=>array('justification'=>'right','width'=>100),
						'1'=>array('justification'=>'left','width'=>100),
						'2'=>array('justification'=>'right','width'=>100),
						'3'=>array('justification'=>'left','width'=>100),
						'4'=>array('justification'=>'right','width'=>100),
						'5'=>array('justification'=>'left','width'=>100)
						)
				));
	$pdf->ezSetDy(-40);	
	$pdf->ezText('<b>Beneficios</b>',12,array('justification'=>'center'));
	
	$pdf->ezSetDy(-10);
	$rowsTable = array();
	foreach ($benefitRaw as $key => $ben) {
		$coverage = ''; $restriction = 'N/A';
		if($ben['price_bxp']==0){
			$coverage = $ben['alias_con'];
		}
		else{
			$coverage = $ben['price_bxp'];
			if($ben['restriction_price_bxp']){
				$restriction = $ben['restriction_price_bxp'].'/'.$ben['description_rbl'];
			}
		}
		$rowsTable[] = array(
							'num'=>($key+1),
							'name'=>$ben['description_bbl'],
							'coverage'=>$coverage,
							'restriction'=>$restriction
							);
	}
	$pdf->ezTable($rowsTable, $table, NULL,$optionsTable);
	$pdf->ezStream();
}else{
    http_redirect('modules/reports/product/benefitsByProduct/fBenefitsByProduct');
}


?>