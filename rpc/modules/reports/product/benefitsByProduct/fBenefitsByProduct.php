<?php
/*
File: BenefitsByProduct.php
Author: Gabriela Guerrero
Creation Date: 12/07/2010
Modified By:
Last Modified:
*/
Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getZonesBenefitsByProduct($_SESSION['countryList'],$_SESSION['serial_lang']);

$smarty -> register('zonesList');
$smarty->display();
?>