<?php

if(  isset($_POST['selZone'])  &&  isset($_POST['selCountry']) && isset($_POST['selProduct']) ){

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$serial_pro = $_POST['selProduct'];

	$benefit = new BenefitsByProduct($db);
	$benefitRaw = $benefit->getBenefitsByProductReport($serial_zon,$serial_cou,$serial_pro,$_SESSION['serial_lang']);
	
	//debug::print_r($benefitRaw);
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Beneficios_Producto.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = "<table>
				<tr><td colspan=6>".$date."</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Beneficios por Producto</b></td></tr>
				<tr></tr>
			</table>";
	echo $title;
	//**PARAMETERS TABLE
	$parameters = array(
						array('0'=>'<b>Zona: </b>', '1'=>$benefitRaw[0]['name_zon']),
						array('0'=>'<b>'.utf8_decode(País).':</b>', '1'=>$benefitRaw[0]['name_cou']),
						array('0'=>'<b>Producto:</b>', '1'=>$benefitRaw[0]['name_pbl'])
						);
	$filters = "<table><tr>";
	foreach($parameters as $p){
		$filters .= "<td align='right'>".$p['0']."</td><td align='left'>".$p['1']."</td>";
	}
	$filters .= "</tr></table>";
	echo $filters;
	$pricesTitle = "<table><tr></tr><tr align='center'><td colspan=6 align='center'><b>Beneficios</b>
						</td></tr><tr></tr></table>";
	echo $pricesTitle;
	$table = "<table><tr><td></td><td><table border=1><tr>
					<td align='center'><b>#</b></td><td align='center'><b>Beneficio</b></td>
					<td align='center'><b>Cobertura</b></td><td align='center'><b>".utf8_decode(Restricción)."</b></td>
				</tr>
			";
	foreach ($benefitRaw as $key => $ben) {
		$coverage = ''; $restriction = 'N/A';
		if($ben['price_bxp']==0){
			$coverage = $ben['alias_con'];
		}
		else{
			$coverage = $ben['price_bxp'];
			if($ben['restriction_price_bxp']){
				$restriction = $ben['restriction_price_bxp'].'/'.$ben['description_rbl'];
			}
		}
		$table .= "<tr><td align='center'>".($key+1)."</td>
						<td align='center'>".$ben['description_bbl']."</td>
						<td align='center'>".$coverage."</td>
						<td align='center'>".$restriction."</td>
					</tr>";
	}
	$table .= "</table></td></tr></table>";
	echo $table;
}else{
    http_redirect('modules/reports/product/benefitsByProduct/fBenefitsByProduct');
}


?>