<?php
/*
File: pProductPDFReport
Author: David Bergmann
Creation Date: 27/05/2010
Last Modified:
Modified By:
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
$productByCountry = new ProductByCountry($db);
$product_list = $productByCountry->getProductsByCountry($_POST[selCountry], $_SESSION['serial_lang'],$_POST['selStatus']);

$country=new Country($db, $_POST['selCountry']);
$country->getData();

$zone=new Zone($db, $country->getSerial_zon());
$zone->getData();

//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE PRODUCTOS POR PAÍS").'</b>', 12, array('justification'=>'center'));

$pdf->ezSetDy(-10);
$headerDesc=array();
$descLine1=array('col1'=>utf8_decode('<b>Zona:</b>'),
				 'col2'=>$zone->getName_zon());
array_push($headerDesc,$descLine1);

$descLine2=array('col1'=>utf8_decode('<b>País:</b>'),
				 'col2'=>$country->getName_cou());
array_push($headerDesc,$descLine2);

$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 10,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>50),
						'col2'=>array('justification'=>'left','width'=>150))));
$pdf->ezSetDy(-10);
//END HEADERS

if(is_array($product_list)) {
    foreach ($product_list as &$pl) {
        $benefit_by_product = new BenefitsByProduct($db);
        $aux = $benefit_by_product->getBenefitsByProduct($pl['serial_pro'], $_SESSION['serial_lang']);
        $pl['benefits'] = $aux;
        //DATA
        $dataDesc=array();
        $descLine1=array('col1'=>utf8_decode('<b>Nombre:</b>'),
				 'col2'=>$pl['name_pbl']);
        array_push($dataDesc,$descLine1);

        $descLine2=array('col1'=>utf8_decode('<b>Descripción:</b>'),
				 'col2'=>$pl['description_pbl']);
        array_push($dataDesc,$descLine2);

        $pdf->ezTable($dataDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'285',
					'fontSize' => 8,
					'titleFontSize' => 8,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>75),
						'col2'=>array('justification'=>'left','width'=>400))));

        $pdf->ezSetDy(-10);

        $titles=array('col1'=>utf8_decode('<b>Máximo de Acompañantes</b>'),
                      'col2'=>utf8_decode('<b>Viajes por período</b>'),
                      'col3'=>utf8_decode('<b>Menores Gratis</b>'),
                      'col4'=>utf8_decode('<b>Mayores Gratis</b>'),
                      'col5'=>utf8_decode('<b>Precio para Familiares diferente</b>'),
                      'col6'=>utf8_decode('<b>Precio para Cónyuje diferente</b>'),
                      'col7'=>utf8_decode('<b>Válida dentro del país de emisión</b>'),
                      'col8'=>utf8_decode('<b>Válida dentro del país de residencia</b>'),
                      'col9'=>utf8_decode('<b>Masivo</b>'),
                      'col10'=>utf8_decode('<b>Países Schengen</b>'),
                      'col11'=>utf8_decode('<b>Adulto Mayor</b>'),
                      'col12'=>utf8_decode('<b>Tiene Servicios</b>'));

        $characteristics=array(array('col1'=>$pl['max_extras_pro'],
                                'col2'=>$pl['flights_pro'],
                                'col3'=>$pl['children_pro'],
                                'col4'=>$pl['adults_pro'],
                                'col5'=>$global_yes_no[$pl['relative_pro']],
                                'col6'=>$global_yes_no[$pl['spouse_pro']],
                                'col7'=>$global_yes_no[$pl['emission_country_pro']],
                                'col8'=>$global_yes_no[$pl['residence_country_pro']],
                                'col9'=>$global_yes_no[$pl['masive_pro']],
                                'col10'=>$global_yes_no[$pl['schengen_pro']],
                                'col11'=>$global_yes_no[$pl['senior_pro']],
                                'col12'=>$global_yes_no[$pl['has_services_pro']]));

        $pdf->ezTable($characteristics,$titles,utf8_decode('<b>Características</b>'),
                          array('showHeadings'=>1,
                                        'shaded'=>1,
                                        'showLines'=>2,
                                        'xPos'=>'center',
                                        'innerLineThickness' => 0.8,
                                        'outerLineThickness' => 0.8,
                                        'fontSize' => 8,
                                        'titleFontSize' => 8,
                                        'cols'=>array(
                                                'col1'=>array('justification'=>'center','width'=>60),
                                                'col2'=>array('justification'=>'center','width'=>60),
                                                'col3'=>array('justification'=>'center','width'=>50),
                                                'col4'=>array('justification'=>'center','width'=>50),
                                                'col5'=>array('justification'=>'center','width'=>80),
                                                'col6'=>array('justification'=>'center','width'=>80),
                                                'col7'=>array('justification'=>'center','width'=>80),
                                                'col8'=>array('justification'=>'center','width'=>80),
                                                'col9'=>array('justification'=>'center','width'=>50),
                                                'col10'=>array('justification'=>'center','width'=>50),
                                                'col11'=>array('justification'=>'center','width'=>50),
                                                'col12'=>array('justification'=>'center','width'=>50))));
        $pdf->ezSetDy(-10);
        //END DATA

        //BENEFITS
        $benefits = array();
        $benefit_titles=array('col1'=>utf8_decode('<b>Máximo de Acompañantes</b>'),
                              'col2'=>utf8_decode('<b>Monto de cobertura</b>'),
                              'col3'=>utf8_decode('<b>Restricción</b>'));
        foreach ($pl['benefits'] as $b){
            $price;
            if($b['price_bxp']>0) {
                $price=$b['price_bxp'];
            } else {
                $price="N/A";
            }
            $aux=array('col1'=>$b['description_bbl'],
                       'col2'=>$price,
                       'col3'=>$b['restriction_price_bxp']);
            array_push($benefits, $aux);
        }

        $pdf->ezTable($benefits,$benefit_titles,utf8_decode('<b>Características</b>'),
                          array('showHeadings'=>1,
                                        'shaded'=>1,
                                        'showLines'=>2,
                                        'xPos'=>'center',
                                        'innerLineThickness' => 0.8,
                                        'outerLineThickness' => 0.8,
                                        'fontSize' => 8,
                                        'titleFontSize' => 8,
                                        'cols'=>array(
                                                'col1'=>array('justification'=>'center','width'=>220),
                                                'col2'=>array('justification'=>'center','width'=>80),
                                                'col3'=>array('justification'=>'center','width'=>80))));
        $pdf->ezSetDy(-20);
        //END BENEFITS
    }
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}


$pdf->ezStream();
?>