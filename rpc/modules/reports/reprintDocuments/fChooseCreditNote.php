<?php
/*
 * File: fChooseCreditNote.php
 * Author: Nicolas Flores
 * Creation Date: 22/06/2010, 04:34:12 PM
 * Modifies By: 
 * Last Modified: 
 */

Request::setString('0:error');

$country=new Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);

$smarty->register('countryList');
$smarty->register('error');
$smarty->display();
?>
