<?php
/*
 * File: pChooseReportPDF.php
 * Author: Patricio Astudillo
 * Creation Date: 16/07/2010, 05:38:09 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 05:38:09 PM
 */

$parameters=array();

if($_POST['rdType']=='PARAMETER'){ //FILTERS BY PARAMETERS

	$zone=new Zone($db, $_POST['selZone']);
	$zone->getData();
	$zone=$zone->getName_zon();
	$country=new Country($db, $_POST['selCountry']);
	$country->getData();
	$country=$country->getName_cou();
	$aux=array('col1'=>'<b>Zona:</b>',
			   'col2'=>$zone,
			   'col3'=>html_entity_decode('<b>Pa&iacute;s:</b>'),
			   'col4'=>$country);
	array_push($parameters, $aux);

	$manager=ManagerbyCountry::getManagerByCountryName($db, $_POST['selManager']);
	$city=new City($db, $_POST['selCity']);
	$city->getData();
	$city=$city->getName_cit();
	$aux=array('col1'=>'<b>Representante:</b>',
			   'col2'=>$manager,
			   'col3'=>'<b>Ciudad:</b>',
			   'col4'=>$city);
	array_push($parameters, $aux);

	$branch=new Dealer($db, $_POST['selBranch']);
	$branch->getData();
	$branch=$branch->getName_dea();
	$dealer=new Dealer($db, $_POST['selDealer']);
	$dealer->getData();
	$dealer=$dealer->getName_dea();
	$aux=array('col1'=>'<b>Comercializador:</b>',
			   'col2'=>$dealer,
			   'col3'=>'<b>Sucursal de Comercializador:</b>',
			   'col4'=>$branch);
	array_push($parameters, $aux);

	if($_POST['selCounter']){
		$counter=new Counter($db, $_POST['selCounter']);
		$counter->getData();
		$counter=$counter->getAux_cnt();
		$counter=$counter['counter'];
	}else{
		$counter='Todos';
	}

	if($_POST['selStatus']){
		$status=$global_appliedBonusStatus[$_POST['selStatus']];
	}else{
		$status='Todos';
	}
	$aux=array('col1'=>'<b>Counter:</b>',
			   'col2'=>$counter,
			   'col3'=>'<b>Estado:</b>',
			   'col4'=>$status);
	array_push($parameters, $aux);
	
	if($_POST['txtBeginDate'] && $_POST['txtEndDate']){
		$aux=array('col1'=>'<b>Fecha Desde:</b>',
			   'col2'=>$_POST['txtBeginDate'],
			   'col3'=>'<b>Fecha Hasta:</b>',
			   'col4'=>$_POST['txtEndDate']);
		array_push($parameters, $aux);
	}

	if($_POST['selCounter']){ //Report for a specific counter
		$misc['type']='COUNTER';
		$misc['identifier']=$_POST['selCounter'];
	}else{ //Report for a whole branch
		$misc['type']='BRANCH';
		$misc['identifier']=$_POST['selBranch'];
	}
}else{ //FILTERS BY CARD NUMBER
	$aux=array('col1'=>'<b># de Tarjeta:</b>',
			   'col2'=>$_POST['txtCardNumber'],
			   'col3'=>'',
			   'col4'=>'');
	array_push($parameters, $aux);

	$misc['type']='CARD';
	$misc['identifier']=$_POST['txtCardNumber'];
}

$data_list=AppliedBonus::getBonusForReport($db, $misc, $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selStatus']);

if(is_array($data_list)){
	foreach($data_list as &$d){
		$d['status_apb']=$global_appliedBonusStatus[$d['status_apb']];
	}
}

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

//HEADER
$pdf->ezText('<b>REPORTE DE INCENTIVOS</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$pdf->ezText('<b>FILTROS UTILIZADOS.-</b>', 11, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$pdf->ezTable($parameters,
		  array('col1'=>'',
				'col2'=>'',
				'col3'=>'',
				'col4'=>''),
		  '',
		  array('showHeadings'=>0,
				'shaded'=>0,
				'showLines'=>0,
				'xPos'=>'350',
				'fontSize' => 9,
				'titleFontSize' => 11,
				'cols'=>array(
					'col1'=>array('justification'=>'left','width'=>100),
					'col2'=>array('justification'=>'left','width'=>200),
					'col3'=>array('justification'=>'left','width'=>100),
					'col4'=>array('justification'=>'left','width'=>100)
					)));
$pdf->ezSetDy(-10);

if(is_array($data_list) && sizeof($data_list)>0){
	$pdf->ezTable($data_list,
		  array('name_cit'=>'<b>Ciudad</b>',
				'name_dea'=>'<b>Sucursal de Comercializador</b>',
				'card_number_sal'=>'<b>Tarjeta</b>',
				'name_pbl'=>'<b>Producto</b>',
				'customer_name'=>'<b>Cliente</b>',
				'total_sal'=>'<b>Valor de la Tarjeta</b>',
				'total_amount_apb'=>'<b>Valor del Incentivo</b>',
			    'status_apb'=>'<b>Estado</b>'),
		  '<b>DETALLE DE INCENTIVOS POR VENTAS</b>',
		  array('showHeadings'=>1,
				'shaded'=>1,
				'showLines'=>2,
				'xPos'=>'center',
				'fontSize' => 9,
				'titleFontSize' => 11,
				'cols'=>array(
					'name_cit'=>array('justification'=>'center','width'=>100),
					'name_dea'=>array('justification'=>'center','width'=>100),
					'card_number_sal'=>array('justification'=>'center','width'=>60),
					'name_pbl'=>array('justification'=>'center','width'=>120),
					'customer_name'=>array('justification'=>'center','width'=>150),
					'total_sal'=>array('justification'=>'center','width'=>60),
					'total_amount_apb'=>array('justification'=>'center','width'=>60),
					'status_apb'=>array('justification'=>'center','width'=>80)
					)));
}else{
	$pdf->ezText(html_entity_decode('No se tienen incentivos registrados de acuerdo a los par&aacute;metros ingresados.'), 10, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
}

$pdf->ezStream();
?>
