<?php
/*
	File: pInvoiceReportExcel
	Author: Santiago Borja
	Creation Date: 10/06/2010
	Description: Generates an Excel file with all invoices
*/

	Request::setString('0:serialApo');
	Request::setString('1:serialOve');
	
	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selApliedTo');
	Request::setString('selManager');
	Request::setString('selResp');
	Request::setString('selDealerDea');
	Request::setString('selDealer');
	
	$apo = new AppliedOver($db,$serialApo);
	$apo -> getData();
	
	$ove = new Over($db, $serialOve);
	$ove -> getData();
	
	switch($selApliedTo){
		case 'MANAGER':
			$mbc = new ManagerbyCountry($db, $selManager);
			$mbc -> getData();
			$man = new Manager($db, $mbc -> getSerial_man());
			$man -> getData();
			$nameBranch = $man -> getName_man();
			$cit = new City($db, $man -> getSerial_cit());
			$cit -> getData();
			$city = $cit -> getName_cit();
			break;
		case 'DEALER':
			$branch = new Dealer($db, $apo -> getSerial_dea());
			$branch -> getData();
			$nameBranch = $branch -> getName_dea();
			$sec = new Sector($db, $branch -> getSerial_sec());
			$sec -> getData();
			$cit = new City($db, $sec -> getSerial_cit());
			$cit -> getData();
			$city = $cit -> getName_cit();
			break;
	}
	
	$apoSales = $apo -> getPresentSales();
	
	$table.="<table border='0'>
				<tr><td colspan=4 align='center'><b>Sistema Blue Card</b></td></tr>
				<tr><td colspan=4 align='center' style='font-size: 10px; border-bottom: 1px solid #000000;'>Quito, ".date("F j, Y, g:i a")."</td></tr>
				<tr><td colspan=4 align='center'><b>Reporte de Overs</b></td></tr>
				<tr><td colspan=4 align='center'>&nbsp;</td></tr>
				<tr><td colspan=4 align='center'>&nbsp;</td></tr>
			</table>";
	
	$table.="<table border='0'>
				<tr><td colspan='2' align='left'><b>Nombre del Over:</b></td>
					<td colspan='2' align='left'>".utf8_decode($ove -> getName_ove())."</td></tr>
				<tr><td colspan='2' align='left'><b>Estado del Over:</b></td>
					<td colspan='2' align='left'>".utf8_decode($apo -> getStatus_Apo())."</td></tr>
				<tr><td colspan='2' align='left'><b>Nombre de la Sucursal:</b></td>
					<td colspan='2' align='left'>".utf8_decode($nameBranch)."</td></tr>
				<tr><td colspan='2' align='left'><b>Ciudad:</b></td>
					<td colspan='2' align='left'>".utf8_decode($city)."</td></tr>
				<tr><td colspan=2 align='center'>&nbsp;</td></tr>
			</table>";
	
	if($apoSales){
		//TABLE HEADER:
		$table.="<table border='1'>
				<tr>
					<td style='background-color: #CCCCCC;' align='center'><b>No. de Tarjera</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Cliente</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Valor de la Venta Neta</b></td>
					<td style='background-color: #CCCCCC;' align='center'><b>Valor de la Venta Bruta</b></td>
					".$variable_rows."
				</tr>";
		foreach($apoSales as $key=>$inv){
			$table.="<tr>
						<td  align='center'>".$inv['card_number_sal']."</td>
						<td  align='center'>".$inv['name_cus']."</td>
						<td  align='center'>".$inv['net_sal']."</td>
						<td  align='center'>".$inv['total_sal']."</td>
					</tr>";
		}
		$table.="</table>";
		
	}else{
		$table.="<b>No existen overs registrados</b>";
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=ReporteClientes.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $header;
	echo $table;
?>
