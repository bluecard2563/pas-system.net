<?php
/*
File:pInvoiceReportPDF.php
Author: Santiago Borja
Creation Date: 10/06/2010
*/
	Request::setString('0:serialApo');
	Request::setString('1:serialOve');
	
	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selApliedTo');
	Request::setString('selManager');
	Request::setString('selResp');
	Request::setString('selDealerDea');
	Request::setString('selDealer');
	
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	
	$apo = new AppliedOver($db,$serialApo);
	$apo -> getData();
	
	$ove = new Over($db, $serialOve);
	$ove -> getData();
	
	
	switch($selApliedTo){
		case 'MANAGER':
			$mbc = new ManagerbyCountry($db, $selManager);
			$mbc -> getData();
			$man = new Manager($db, $mbc -> getSerial_man());
			$man -> getData();
			$nameBranch = $man -> getName_man();
			$cit = new City($db, $man -> getSerial_cit());
			$cit -> getData();
			$city = $cit -> getName_cit();
			break;
		case 'DEALER':
			$branch = new Dealer($db, $apo -> getSerial_dea());
			$branch -> getData();
			$nameBranch = $branch -> getName_dea();
			$sec = new Sector($db, $branch -> getSerial_sec());
			$sec -> getData();
			$cit = new City($db, $sec -> getSerial_cit());
			$cit -> getData();
			$city = $cit -> getName_cit();
			break;
	}
	
	$apoSales = $apo -> getPresentSales();
	
	//HEADER
	$pdf->ezText('<b>REPORTE DE OVERS</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	//HEADER FILTERS:
	$headerDesc=array();
	
	array_push($headerDesc,array('col1'=>utf8_decode('<b>Nombre del Over:</b>'),'col2'=> utf8_decode($ove -> getName_ove())));
	array_push($headerDesc,array('col1'=>utf8_decode('<b>Estado del Over:</b>'),'col2'=> utf8_decode($apo -> getStatus_Apo())));
	array_push($headerDesc,array('col1'=>utf8_decode('<b>Nombre de la Sucursal:</b>'),'col2'=> utf8_decode($nameBranch)));
	array_push($headerDesc,array('col1'=>utf8_decode('<b>Ciudad:</b>'),'col2'=> utf8_decode($city)));
	
	$header['card_number_sal'] = '<b>No. de Tarjera</b>';
	$params['card_number_sal'] = array('justification'=>'center','width'=>40);
	$header['name_cus'] = '<b>Cliente</b>';
	$params['name_cus'] = array('justification'=>'center','width'=>40);
	$header['net_sal'] = '<b>Valor de la Venta Neta</b>';
	$params['net_sal'] = array('justification'=>'center','width'=>40);
	$header['total_sal'] = '<b>Valor de la Venta Bruta</b>';
	$params['total_sal'] = array('justification'=>'center','width'=>40);
	
				
	
	//BODY:
	$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150))));
	$pdf->ezSetDy(-10);
	//END HEADERS
	
	if(is_array($apoSales)) {
		foreach($apoSales as $key=>&$sale){
			$i['status_inv']=$global_invoice_status[$i['status_inv']];
		}
		$pdf->ezTable($apoSales,$header,$tableTitle,$params);
		$pdf->ezSetDy(-10);
	}else{
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}
	
	$pdf->ezStream();
?>