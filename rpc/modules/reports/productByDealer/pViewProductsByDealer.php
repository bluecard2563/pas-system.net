<?php
/*
 * File: pViewProductsByDealer.php
 * Author: Patricio Astudillo
 * Creation Date: 27/05/2010, 11:14:16 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 27/05/2010, 11:14:16 AM
 */

Request::setString('selCountry');
Request::setString('selManager');
Request::setString('selDealer');

/* Getting the Product List in a Specific Country */
$pxc=new ProductByCountry($db);
$product_list=$pxc->getProductsByCountry($selCountry, $_SESSION['serial_lang']);
/* Getting the Product List in a Specific Country */

/* Getting the DEaler List According to the paramters */
$params=array();
$aux=array('field'=>'mbc.serial_cou',
		   'value'=> $selCountry,
		   'like'=> 1);
array_push($params,$aux);

$aux=array('field'=>'d.dea_serial_dea',
		   'value'=> 'NULL',
		   'like'=> 1);
array_push($params,$aux);

if($selManager){
	$aux=array('field'=>'mbc.serial_mbc',
			   'value'=> $selManager,
			   'like'=> 1);
	array_push($params,$aux);
}

if($selDealer){
	$aux=array('field'=>'d.serial_dea',
			   'value'=> $selDealer,
			   'like'=> 1);
	array_push($params,$aux);
}

$dealer=new Dealer($db);
$dealerList=$dealer->getDealers($params);
/* Getting the DEaler List According to the paramters */

if(is_array($dealerList) && is_array($product_list)){
	$total_product=sizeof($product_list);

	foreach($dealerList as $d){
		$table="<table border='1'>";
		$table.="<tr colspan=>";
		$table.="<td>hola";
		$table.="</td>";
		$table.="</tr>";
		$table.="</table>";
	}
}

?>