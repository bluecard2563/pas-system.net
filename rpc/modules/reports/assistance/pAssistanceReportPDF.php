<?php
/*
File:pAssistanceReportPDF
Author: Esteban Angulo
Creation Date: 11/06/2010
*/

Request::setInteger('0:serial_fle');
//Debug::print_r($_POST);
//echo $serial_fle;
$file=new File($db);
$file->setSerial_fle($serial_fle);
$file->getData();
$customer=new Customer($db);
$customer->setserial_cus($file->getSerial_cus());
$customer->getData();
$card_number=$file->getCardNumberByFile();
$sale=new Sales($db);
$sale->setSerial_sal($file->getSerial_sal());
$saleProduct=$sale->getSaleProduct($_SESSION['serial_lang']);
$blogCus=new Blog($db);
$blogCus->setSerial_fle($serial_fle);
$blogCus->getDataByFile('CLIENT');
$blogOp=new Blog($db);
$blogOp->setSerial_fle($serial_fle);
$blogOp->getDataByFile('OPERATOR');
$user=new User($db);
$user->setSerial_usr($blogOp->getSerial_usr());
$user->getData();
if($file->getStatus_fle()=='open'){
	$status='Abierto';
}
elseif($file->getStatus_fle()=='closed'){
	$status='Cerrado';
}
elseif($file->getStatus_fle()=='stand-by'){
	$status='Pendiente';
}
$contact=unserialize($file->getContanct_info_fle());
//Debug::print_r($file);
//Debug::print_r($contact);
//Debug::print_r($saleProdut);
//Debug::print_r($blogCus);
//Debug::print_r($blogOp);
//Debug::print_r($sale);
//Debug::print_r($file);

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
//HEADER
$pdf->ezText('<b>INFORME DE ASISTENCIAS</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);
$headerDesc=array();
$descLine1=array('col1'=>utf8_decode('<b># de Expediente:</b>'),
				 'col2'=>$file->getSerial_fle(),
				 'col3'=>utf8_decode('<b>Nombre del Cliente:</b>'),
				 'col4'=>$customer->getFirstname_cus().' '.$customer->getLastname_cus());
array_push($headerDesc,$descLine1);
$descLine2=array('col1'=>utf8_decode('<b># de Tarjeta:</b>'),
				 'col2'=>$card_number,
				 'col3'=>utf8_decode('<b>Nombre del Operador:</b>'),
				 'col4'=>$user->getFirstname_usr().' '.$user->getLastname_usr());
array_push($headerDesc,$descLine2);
$descLine3=array('col1'=>'<b>Tipo de Producto:</b>',
				 'col2'=>$saleProduct['name_pbl'],
				 'col3'=>utf8_decode('<b>Fecha de Inicio:</b>'),
				 'col4'=>$file->getCreation_date_fle());
array_push($headerDesc,$descLine3);
$descLine4=array('col1'=>'<b>Estado:</b>',
				 'col2'=>$status);
array_push($headerDesc,$descLine4);
$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>100),
						'col2'=>array('justification'=>'left','width'=>120),
						'col3'=>array('justification'=>'left','width'=>100),
						'col4'=>array('justification'=>'left','width'=>120),
						)));
$pdf->ezSetDy(-20);
$diagnosis=array();
if($file->getCause_fle()==''){
	$cause='N/A';
}
else{
	$cause=$file->getCause_fle();
}

if($file->getCause_fle()==''){
	$diag='N/A';
}
else{
	$diag=$file->getDiagnosis_fle();
}
$descLine5=array('col1'=>utf8_decode('<b>Causa:</b>'),
				 'col2'=>$cause);
array_push($diagnosis,$descLine5);
$descLine6=array('col1'=>utf8_decode('<b>Diagnóstico:</b>'),
				 'col2'=>$diag);
array_push($diagnosis,$descLine6);
$pdf->ezTable($diagnosis,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>100),
						'col2'=>array('justification'=>'left','width'=>340)
						)));
$pdf->ezSetDy(-20);

if($contact!=''){
$contanctHeader=array();
$descLine7=array('col1'=>utf8_decode('<b>Datos del Contactante</b>'));
array_push($contanctHeader,$descLine7);
$pdf->ezTable($contanctHeader,
			  array('col1'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>440)
						)));

foreach($contact as $key=>$c){
	$contact[$key]['num']=$key+1;
	$contact[$key]['name']=utf8_decode($contact[$key]['name']);
	$contact[$key]['address']=utf8_decode($contact[$key]['address']);
}

$header = array(		'num'=>'<b>#</b>',
						'name'=>'<b>Nombre y Apellido</b>',
						'phone'=>utf8_decode('<b>Teléfono</b>'),
						'address'=>utf8_decode('<b>Dirección</b>'));
$params = array('showHeadings'=>1,
				'shaded'=>1,
				'showLines'=>2,
				'xPos'=>'300',
				'fontSize' => 8,
				'titleFontSize' => 8,
				'cols'=>array(
					'num'=>array('justification'=>'center','width'=>40),
					'name'=>array('justification'=>'center','width'=>110),
					'phone'=>array('justification'=>'center','width'=>110),
					'address'=>array('justification'=>'center','width'=>180)));
$pdf->ezSetDy(-10);
$pdf->ezTable($contact,$header,
						$tableTitle,
						$params);
}
$pdf->ezSetDy(-10);

$blogCustomer=array();
$blog=str_replace("<br />"," ",$blogCus->getEntry_blg());
$descLine10=array('col1'=>utf8_decode('<b>Bitácora Cliente:</b>
'). htmlspecialchars($blog)
				);
array_push($blogCustomer,$descLine10);
		$pdf->ezTable($blogCustomer,
			  array('col1'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'splitRows' => 1,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>440)
						)));
		
$blogOperator=array();
$blog2=str_replace("<br />"," ",$blogOp->getEntry_blg());
$descLine11=array('col1'=>utf8_decode('<b>Bitácora Operador:</b>
') . htmlspecialchars($blog2)
				);
array_push($blogOperator,$descLine11);
		$pdf->ezTable($blogOperator,
			  array('col1'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'splitRows' => 1,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>440)
						)));
$pdf->ezStream();
//END HEADER
?>
