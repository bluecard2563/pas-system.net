<?php
/*
File: pSaleDetailReportPDF.php
Author: Santiago Borja
Creation Date: 10/06/2010
*/
	Request::setString('selCreditNote');
	Request::setString('txtNumCard');
	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selManager');
	Request::setString('selDocumentTo');
	Request::setString('txtNumInvoice');
	Request::setString('txtClient');
	Request::setString('hdnClientID');
	Request::setString('saleSerial');
	
	$tableInformation = array();
	
	include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
	
	$sale = new Sales($db, $saleSerial);
	$sale -> getData();
	
	//CUSTOMER:
	$serialCus = $sale -> getSerial_cus();
	$customer = new Customer($db, $serialCus);
	$customer -> getData();
	$customer=get_object_vars($customer);
	
	//TRIP INFO:
	$dataInfo = $sale->getSaleProduct($_SESSION['serial_lang']);
	$extra=new Extras($db,$sale -> getSerial_sal());
	$dataExt = Extras::getExtrasBySale($db, $sale -> getSerial_sal());
	$log = new SalesLog($db);
	$dataLog = $log -> getSalesLogBySale($sale -> getSerial_sal());
	$file = new File($db);
	$dataFiles = $file -> getFilesByCard($sale -> getCardNumber_sal());
	
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext = $fee_ext+$dE['fee_ext'];
		}
	}
	$dataServ = $sale -> getSaleServices($_SESSION['serial_lang']);
	$dataBen = $sale -> getSaleBenefits($_SESSION['serial_lang']);
	
	//TABLE DATA:
	$tableInformation['nameCustomer'] = $customer['first_name_cus'].' '.$customer['last_name_cus'];
	$tableInformation['documentCustomer'] = $customer['document_cus'];
	$tableInformation['birthdateCustomer'] = $customer['birthdate_cus'];
	$tableInformation['emailCustomer'] = $customer['email_cus'];
	$tableInformation['phoneCustomer'] = $customer['phone1_cus'];
	$tableInformation['cellphoneCustomer'] = $customer['cellphone_cus'];
	$tableInformation['addressCustomer'] = $customer['address_cus'];
	
	$tableInformation['emergencyContact'] = $customer['relative_cus'];
	$tableInformation['emergencyContactPhone'] = $customer['relative_phone_cus'];
	$tableInformation['beginDate'] = ''.$dataInfo['begin_date_sal'];
	$tableInformation['endDate'] = ''.$dataInfo['end_date_sal'];
	
	if($dataInfo['flights_pro'] == 1){
		$tableInformation['destination'] = $dataInfo['country_sal'].' - '.$dataInfo['city_sal'];
	}
	$tableInformation['productName'] = $dataInfo['name_pbl'];
	$tableInformation['productPrice'] = number_format($dataInfo['fee_sal'] + $fee_ext, 2, '.', '');
	
	if($dataServ){
		foreach($dataServ as $dS){
			$service = array();
			$service['name'] = $dS['name_sbl'];
			$service['coverage'] = $dS['coverage_sbc'];
			$tableInformation['productServices'][] = $service;
		}
	}
	
	if($dataBen){
		foreach($dataBen as $d){
			$benefit = array();
			$benefit['name'] = ''.$d['description_bbl'];
			
			if((int)$d['price_bxp'] == 0){
				$benefit['amount'] = ''.$d['alias_con'];
			}
			else{
				$benefit['amount'] = '$ '.$d['price_bxp'];
				if($d['restriction_price_bxp']){
					$benefit['amount'] .= '  ('.$d['restriction_price_bxp'].' x '.$d['description_rbl'].')';
				}
			}
			$tableInformation['productBenefits'][] = $benefit;
		}
	}
	
	if($dataExt){
		foreach($dataExt as $d){
			$extra = array();
			$extra['document'] = $d['document_cus'];
			$extra['name'] = $d['first_name_cus'].' '.$d['last_name_cus'];
			$extra['age'] = GlobalFunctions::getMyAge($d['birthdate_cus']);
			$extra['fee'] = '$'.number_format($d['fee_ext'], 2, '.', '');
			$tableInformation['productExtras'][] = $extra;
		}
	}
	
	if($dataLog){
		foreach($dataLog as $d){
			$extra = array();
			$myData = unserialize($d['description_slg']);
						
			$logX['user'] = $d['first_name_usr'].' '.$d['last_name_usr'];
			$logX['date'] = $d['date_slg'];
			$logX['description'] = $myData['Description'];
			
			switch($d['type_slg']){
				case 'VOID_INVOICE':
					$logX['old_val'] = $myData['old_serial_inv'];
					$logX['new_val'] = $sale -> getSerial_inv();
					break;
				case 'ADD_DAYS':
					$logX['old_val'] = $myData['old_days_sal'];
					$logX['new_val'] = $myData['new_days_sal'];
					break;
				case 'STAND_BY':
					$logX['old_val'] = $global_salesStatus[$myData['old_status_sal']];
					$logX['new_val'] = $global_salesStatus[$myData['new_status_sal']];
					break;
				case 'REFUNDED':
					$logX['old_val'] = $global_salesStatus[$myData['old_status_sal']];
					$logX['new_val'] = $global_salesStatus[$myData['new_status_sal']];
					break;
				case 'BLOCKED':
					$logX['old_val'] = $global_salesStatus[$myData['old_status_sal']];
					$logX['new_val'] = $global_salesStatus[$myData['new_status_sal']];
					break;
				case 'MODIFICATION':
					$old_sale_info=$myData['old_sale'];
					$new_sale_info=$myData['new_sale'];

					$oldData="<b>Inicio de covertura:</b> ".$old_sale_info['begin_date_sal']."\n";
					$oldData.="<b>Fin de Covertura:</b> ".$old_sale_info['end_date_sal']."\n";
					$oldData.="<b>Destino:</b> ".Country::getCountryNameByCityCode($db, $old_sale_info['serial_cit']);

					$newData="<b>Autorizado por:</b> ".$d['authorized_by']."\n";
					$newData.="<b>Inicio de covertura:</b> ".$new_sale_info['begin_date_sal']."\n";
					$newData.="<b>Fin de Covertura:</b> ".$new_sale_info['end_date_sal']."\n";
					$newData.="<b>Destino:</b> ".Country::getCountryNameByCityCode($db, $new_sale_info['serial_cit']);

					$logX['description'].=" \n\n<b>Estado:</b> ".$global_sales_log_status[$d['status_slg']];

					$logX['old_val'] =$oldData;
					$logX['new_val'] =$newData;
					break;
			}	
			
			$tableInformation['productLogs'][] = $logX;
		}
	}
	
	if($dataFiles){
		foreach($dataFiles as $d){
			$extra = array();
			$fileX['user'] = $d['name_cus'];
			$fileX['number'] = $d['serial_fle'];
			$fileX['date'] = $d['creation_date_fle'];
			$fileX['diagnosis'] = $d['diagnosis_fle'];
			$fileX['status'] = $d['status_fle'];
			$tableInformation['productFiles'][] = $fileX;
		}
	}	
	
	//*********************************************************** COUNTER ***************************************************
	$cnt = new Counter($db, $sale -> getSerial_cnt());
	$cnt -> getData();
	$usr = new User($db, $cnt -> getSerial_usr());
	$usr -> getData();
	$tableInformation['counterName'] = $usr -> getFirstname_usr() .' '. $usr -> getLastname_usr();
	
	$dealer = new Dealer($db, $cnt -> getSerial_dea());
	$dealer -> getData();
	$tableInformation['dealerName'] = $dealer -> getName_dea();
	
	$tableInformation['observations'] = $sale -> getObservations_sal();
	
	
	//*********************************************************** THE PDF REPORT ********************************************
	/*PARAMS FOR CREATING PDF FILE*/
	$header = array('number_inv' => '<b># Fac</b>',
					'card_number_sal' => '<b># Tarjeta</b>',
					'qty' => '<b>Cant.</b>',
					'date_inv' => '<b>Fec. Emision</b>',
					'name_cus' => '<b>Cliente</b>',
					'facturado_a' => '<b>Facturado a</b>',
					'total_inv' => '<b>Valor</b>');
	$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_inv'=>array('justification'=>'center','width'=>65),
							'card_number_sal'=>array('justification'=>'center','width'=>100),
							'qty'=>array('justification'=>'center','width'=>70),
							'date_inv'=>array('justification'=>'center','width'=>60),
							'name_cus'=>array('justification'=>'center','width'=>60),
							'facturado_a'=>array('justification'=>'center','width'=>120),
							'total_inv'=>array('justification'=>'center','width'=>40)));
	
	//HEADER
	$pdf->ezText('<b>REPORTE DE TARJETAS - DETALLE DE VENTA</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	//HEADER FILTERS:
	$headerDesc = array();
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Cliente:</b>'),
									'col2'=>$tableInformation['nameCustomer'],
									'col3'=>utf8_decode('<b>Documento:</b>'),
									'col4'=> utf8_decode($tableInformation['documentCustomer'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Fecha de Nacimiento:</b>'),
									'col2'=>$tableInformation['birthdateCustomer'],
									'col3'=>utf8_decode('<b>E-mail:</b>'),
									'col4'=> utf8_decode($tableInformation['emailCustomer'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Telefono:</b>'),
									'col2'=>$tableInformation['phoneCustomer'],
									'col3'=>utf8_decode('<b>Celular:</b>'),
									'col4'=> utf8_decode($tableInformation['cellphoneCustomer'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Direccion:</b>'),
									'col2'=>$tableInformation['addressCustomer'],
									'col3'=>utf8_decode('<b>Destino:</b>'),
									'col4'=> utf8_decode($tableInformation['destination'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Viaje Desde:</b>'),
									'col2'=> utf8_decode($tableInformation['beginDate']),
									'col3'=>utf8_decode('<b>Viaje Hasta:</b>'),
									'col4'=> utf8_decode($tableInformation['endDate'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Contacto de Emergencia:</b>'),
									'col2'=>$tableInformation['emergencyContact'],
									'col3'=>utf8_decode('<b>Telefono del contacto:</b>'),
									'col4'=> utf8_decode($tableInformation['emergencyContactPhone'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Producto:</b>'),
									'col2'=> utf8_decode($tableInformation['productName']),
									'col3'=>utf8_decode('<b>Valor Total:</b>'),
									'col4'=> utf8_decode($tableInformation['productPrice'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Counter:</b>'),
									'col2'=> utf8_decode($tableInformation['counterName']),
									'col3'=>utf8_decode('<b>Sucursal de Com:</b>'),
									'col4'=> utf8_decode($tableInformation['dealerName'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Observaciones:</b>'),
									'col2'=>$tableInformation['observations']
	));
	
	$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),'',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'325',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150),
					  	'col3'=>array('justification'=>'left','width'=>120),
					  	'col4'=>array('justification'=>'left','width'=>150)
			  )));
	$pdf->ezSetDy(-10);
	
	
	//******************************************************** BENEFITS TABLE ******************************************
	
	if(count($tableInformation['productServices']) > 0){
		$pdf->ezText('<b>SERVICIOS</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['name'] = '<b>Servicio</b>';
		$params['name'] = array('justification'=>'center');
		$header['coverage'] = '<b>Cobertura</b>';
		$params['coverage'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productServices'],$header,'',$params);
		$pdf->ezSetDy(-10);
	}
	
	if(count($tableInformation['productBenefits']) > 0){
		$pdf->ezText('<b>BENEFICIOS</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['name'] = '<b>Descripcion</b>';
		$params['name'] = array('justification'=>'center');
		$header['amount'] = '<b>Cobertura</b>';
		$params['amount'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productBenefits'],$header,'',$params);
		$pdf->ezSetDy(-10);
	}
	
	if(count($tableInformation['productExtras']) > 0){
		$pdf->ezText('<b>ADICIONALES</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['document'] = '<b>Documento</b>';
		$params['document'] = array('justification'=>'center');
		$header['name'] = '<b>Nombre</b>';
		$params['name'] = array('justification'=>'center');
		$header['age'] = '<b>Edad</b>';
		$params['age'] = array('justification'=>'center');
		$header['fee'] = '<b>Tarifa</b>';
		$params['fee'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productExtras'],$header,'',$params);
	}
	
	if(count($tableInformation['productLogs']) > 0){
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>MODIFICACIONES</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['user'] = '<b>Usuario</b>';
		$params['user'] = array('justification'=>'center');
		$header['description'] = '<b>Detalle</b>';
		$params['description'] = array('justification'=>'center');
		$header['old_val'] = '<b>Valor Anterior</b>';
		$params['old_val'] = array('justification'=>'center');
		$header['new_val'] = '<b>Valor Nuevo</b>';
		$params['new_val'] = array('justification'=>'center');
		$header['date'] = '<b>Fecha</b>';
		$params['date'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productLogs'],$header,'',$params);
	}
	
	if(count($tableInformation['productFiles']) > 0){
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>ASISTENCIAS</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['number'] = '<b>Expediente</b>';
		$params['number'] = array('justification'=>'center');
		$header['user'] = '<b>Cliente</b>';
		$params['user'] = array('justification'=>'center');
		$header['date'] = '<b>Fecha de Creacion</b>';
		$params['date'] = array('justification'=>'center');
		$header['diagnosis'] = '<b>Diagnostico</b>';
		$params['diagnosis'] = array('justification'=>'center');
		$header['status'] = '<b>Estado</b>';
		$params['status'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productFiles'],$header,'',$params);
	}
	
	$pdf->ezStream();
?>