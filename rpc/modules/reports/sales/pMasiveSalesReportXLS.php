<?php
/*
File: pMasiveSalesReportXLS
Author: David Bergmann
Creation Date: 19/07/2010
Last Modified:
Modified By:
*/

$customerList = Sales::getMasiveSalesCustomerReport($db,$_POST['rdoCardChild'][0]);
$sale = new Sales($db,$_POST['rdoCardChild'][0]);
$sale->getData();

$table = "<table border='1'>
		 <tr>
		 <td><b>Tarjeta No.:</b></td>
		 <td>".$sale->getCardNumber_sal()."</td>
		 </tr>
		 </table>";

if(is_array($customerList)) {
    $table.="<table border='1'><tr>
            <th><b>Documento</b></th>
            <th><b>Nombre Completo</b></th>
            <th><b>Fecha de Nacimiento</b></th>
            <th><b>Tel&eacute;fono</b></th>
            <th><b>Celular</b></th>
            <th><b>Direcci&oacute;n</b></th>
            <th><b>Correo Electr&oacute;nico</b></th>
            <th><b>Pa&iacute;s</b></th>
            <th><b>Ciudad</b></th>";

    foreach ($customerList as $cl) {
        $table.="<tr>
                 <td>'".$cl['document_cus']."</td>
                 <td>".$cl['name_cus']."</td>
                 <td>".$cl['birthdate_cus']."</td>
                 <td>".$cl['phone1_cus']."</td>
                 <td>".$cl['cellphone_cus']."</td>
                 <td>".$cl['address_cus']."</td>
                 <td>".$cl['email_cus']."</td>
                 <td>".$cl['name_cou']."</td>
				 <td>".$cl['name_cit']."</td></tr>";
    }
    $table.="</table>";
}


header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Tarjetas.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $table;
?>