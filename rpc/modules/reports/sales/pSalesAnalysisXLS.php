<?php

if( ( isset($_POST['selZone']) ) && ( isset($_POST['selCountry']) && isset($_POST['txtDateFrom']) && isset($_POST['txtDateTo']) ) ){
   
	$sale = new Sales($db);
	$salesRawActual = array();

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$dateFrom = $_POST['txtDateFrom'];
	$dateTo = $_POST['txtDateTo'];
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_usr = (isset($_POST['selResponsible'])) ? $_POST['selResponsible'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';

	$salesRawActual = $sale->getSalesAnalysis($serial_zon, $serial_cou, $dateFrom, $dateTo, $_SESSION['serial_mbc'],
												$_SESSION['serial_dea'], $serial_mbc, $serial_cit, $serial_usr, $serial_dea,
												$serial_bra);

	$dateFromBefore = ''.substr($dateFrom, 0, 6).(substr($dateFrom, 6, 4)-1);
	$dateToBefore = ''.substr($dateTo, 0, 6).(substr($dateTo, 6, 4)-1);
	$salesRawBefore = $sale->getSalesAnalysis($serial_zon, $serial_cou, $dateFromBefore, $dateToBefore, $_SESSION['serial_mbc'],
												$_SESSION['serial_dea'], $serial_mbc, $serial_cit, $serial_usr,$serial_dea,
												$serial_bra);
	//debug::print_r($salesRawActual);debug::print_r($salesRawBefore);die();	

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_de_Analisis_Ventas.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = '<table>
					<tr><td colspan=6>'.$date.'</td></tr>
					<tr></tr>
					<tr><th></th><th><b>Reporte de '.utf8_decode(Análisis).' de Ventas</b></th></tr>
					 <tr></tr>
			  </table>';
	echo $title;
	$manager = 'Todos'; $responsible = 'Todos'; $city = 'Todos'; $dealer = 'Todos'; $branch = 'Todos';

	if($serial_mbc){
		$manager = $salesRawActual[0]['name_man'];
	}
	if($serial_cit){
		$city = $salesRawActual[0]['name_cit'];
	}
	if($serial_usr){
		$responsible = $salesRawActual[0]['name_usr'];
	}
	if($serial_dea){
		$dealer = $salesRawActual[0]['name_dea'];
	}
	if($serial_bra){
		$branch = $salesRawActual[0]['name_bra'];
	}
	$type='';
	if($_POST['selType'] == 'netSale'){
		$type='Neta';
	}
	else{
		$type='Bruta';
	}
	$parameters = "<table>
					<tr>
						<td align='right'><b>Zona: </b></td><td align='left'>".$salesRawActual[0]['name_zon']."</td>
						<td align='right'><b>".utf8_decode(País).":</b></td><td align='left'>".$salesRawActual[0]['name_cou']."</td>
					</tr>
					<tr>
						<td align='right'><b>Fecha Desde:</b></td><td align='left'>".$dateFrom."</td>
						<td align='right'><b>Fecha Hasta:</b></td><td align='left'>".$dateTo."</td>
					</tr>
					<tr>
						<td align='right'><b>Representante:</b></td><td align='left'>".$manager."</td>
						<td align='right'><b>Ciudad:</b></td><td align='left'>".$city."</td>
					</tr>
					<tr>
						<td align='right'><b>Responsable:</b></td><td align='left'>".$responsible."</td>
						<td align='right'><b>Comercializador:</b></td><td align='left'>".$dealer."</td>
					</tr>
					<tr>
						<td align='right'><b>Sucursal:</b></td><td align='left'>".$branch."</td>
						<td align='right'><b>Tipo de Venta:</b></td><td align='left'>".$type."</td>
					</tr>
					<tr></tr>
				</table>";
	echo $parameters;
	$saleAmount = 0; $saleAmountBefore = 0;
	$dealer = ''; 
	foreach ($salesRawActual as $key => $sal) {
		if($sal['serial_dea'] != $dealer){
			$dealerTitle = '<table><tr></tr><tr><td></td><td><b>Comercializador:</b></td><td>'.$sal['name_dea'].'</td></tr>
					 </table>';
			echo $dealerTitle;
		}
		if($type == 'Bruta'){//GROSS SALE
			$saleAmount += $sal['total_sal'];
		}
		else{//NET SALE
			$eTaxes=unserialize($sal['applied_taxes_inv']);
			$eTaxesAmount=0;
			if(is_array($eTaxes)){
				foreach($eTaxes as $t){
					$eTaxesAmount+=$t['tax_percentage'];
				}
				$eTaxesAmount=$eTaxesAmount/100;
			}
			$eDiscounts=$sal['discounts']/100;
			$saleAmount=$sal['total_sal']-($sal['total_sal']*$eDiscounts);
			$saleAmount+=($saleAmount*$eTaxesAmount);
		}
		if($sal['serial_bra'] != $salesRawActual[$key+1]['serial_bra']){
			if($sal['type_percentage_dea'] == 'COMISSION'){
				$discount = ''.utf8_decode(Comisión);
			}
			else{
				$discount = 'Descuento';
			}
			foreach ($salesRawBefore as $keyBefore => $salBefore) {
				if($salBefore['serial_bra'] == $sal['serial_bra']){
					if($type == 'Bruta'){//GROSS SALE
						$saleAmountBefore += $salBefore['total_sal'];
					}
					else{//NET SALE
						$eTaxes=unserialize($salBefore['applied_taxes_inv']);
						$eTaxesAmount=0;
						if(is_array($eTaxes)){
							foreach($eTaxes as $t){
								$eTaxesAmount+=$t['tax_percentage'];
							}
							$eTaxesAmount=$eTaxesAmount/100;
						}
						$eDiscounts=$salBefore['discounts']/100;
						$saleAmountBefore=$salBefore['total_sal']-($salBefore['total_sal']*$eDiscounts);
						$saleAmountBefore+=($saleAmountBefore*$eTaxesAmount);
					}
					if($salBefore['serial_bra'] != $salesRawBefore[$keyBefore+1]['serial_bra']){
						break;
					}
				}			
			}
			if($saleAmountBefore){
				$variation = number_format((($saleAmount - $saleAmountBefore)/$saleAmountBefore)*100, 2, '.', '');
			}
			else{
				$variation = 'N/A';
			}
			$rowsTable[] = array(
									'branch'=>$sal['name_bra'],
									'city'=>$sal['name_cit'],
									'discount'=>$discount,
									'commission'=>number_format($sal['percentage_dea'], 2, '.', ''),
									'before'=>number_format($saleAmountBefore, 2, '.', ''),
									'actual'=>number_format($saleAmount, 2, '.', ''),
									'variation'=>$variation
								);
			if($sal['serial_dea'] != $salesRawActual[$key+1]['serial_dea']){
				$headerTable = "<table border='1'><tr><td align='center'><b>Sucursal</b></td>
											<td align='center'><b>Ciudad</b></td>
											<td align='center'><b>Descuento</b></td>
											<td align='center'><b>".utf8_decode(Comisión)."</b></td>
											<td align='center'><b>".(date('Y', strtotime($dateTo))-1)."</b></td>
											<td align='center'><b>".date('Y', strtotime($dateTo))."</b></td>
											<td align='center'><b>% ".utf8_decode(Variación)."</b></td>
								</tr></table>";
				echo $headerTable;
				$rows = '<table border="1">';
				foreach($rowsTable as $r){
					$rows .= "<tr><td align='center'>".$r['branch']."</td>
								<td align='center'>".$r['city']."</td>
								<td align='center'>".$r['discount']."</td>
								<td align='center'>".$r['commission']."</td>
								<td align='center'>".$r['before']."</td>
								<td align='center'>".$r['actual']."</td>
								<td align='center'>".$r['variation']."</td>
							  </tr>";
				}
				$rows .= '</table>';
				echo $rows;
				$rowsTable = array();
			}
			$saleAmount = 0; $saleAmountBefore = 0; 
		}		
		$dealer = $sal['serial_dea'];
	}
}else{
    http_redirect('modules/reports/sales/fSalesAnalysis');
}


?>