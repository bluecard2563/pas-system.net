<?php
/*
File: pMasiveSalesReportPDF
Author: David Bergmann
Creation Date: 19/07/2010
Last Modified:
Modified By:
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

$customerList = Sales::getMasiveSalesCustomerReport($db,$_POST['rdoCardChild'][0]);
$sale = new Sales($db,$_POST['rdoCardChild'][0]);
$sale->getData();

//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE TARJETAS MASIVAS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$pdf->ezText('<b>'.utf8_decode("Tarjeta No.: ").'</b>'.utf8_decode($sale->getCardNumber_sal()), 10, array('justification'=>'left'));

$pdf->ezSetDy(-20);

if(is_array($customerList)) {
    $titles=array('document_cus'=>utf8_decode('<b>Documento</b>'),
                  'name_cus'=>utf8_decode('<b>Nombre Completo</b>'),
                  'birthdate_cus'=>utf8_decode('<b>Fecha de Nacimiento</b>'),
                  'phone1_cus'=>utf8_decode('<b>Teléfono</b>'),
                  'cellphone_cus'=>utf8_decode('<b>Celular</b>'),
				  'address_cus'=>utf8_decode('<b>Dirección</b>'),
                  'email_cus'=>utf8_decode('<b>Correo Electrónico</b>'),
                  'name_cou'=>utf8_decode('<b>País</b>'),
                  'name_cit'=>utf8_decode('<b>Ciudad</b>'));

	$pdf->ezTable($customerList,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'document_cus'=>array('justification'=>'center','width'=>00),
                                        'name_cus'=>array('justification'=>'center','width'=>90),
                                        'birthdate_cus'=>array('justification'=>'center','width'=>60),
                                        'phone1_cus'=>array('justification'=>'center','width'=>60),
                                        'cellphone_cus'=>array('justification'=>'center','width'=>60),
                                        'address_cus'=>array('justification'=>'center','width'=>160),
                                        'email_cus'=>array('justification'=>'center','width'=>120),
                                        'name_cou'=>array('justification'=>'center','width'=>60),
                                        'name_cit'=>array('justification'=>'center','width'=>60))));
        $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();

?>