<?php

if( ( isset($_POST['selZone']) ) && ( isset($_POST['selCountry']) && isset($_POST['txtDateFrom']) && isset($_POST['txtDateTo']) ) ){
   
	$sale = new Sales($db);
	$salesRawActual = array();

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$dateFrom = $_POST['txtDateFrom'];
	$dateTo = $_POST['txtDateTo'];
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_usr = (isset($_POST['selResponsible'])) ? $_POST['selResponsible'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';

	$salesRawActual = $sale->getSalesAnalysis($serial_zon, $serial_cou, $dateFrom, $dateTo, $_SESSION['serial_mbc'],
												$_SESSION['serial_dea'], $serial_mbc, $serial_cit, $serial_usr, $serial_dea,
												$serial_bra);

	$dateFromBefore = ''.substr($dateFrom, 0, 6).(substr($dateFrom, 6, 4)-1);
	$dateToBefore = ''.substr($dateTo, 0, 6).(substr($dateTo, 6, 4)-1);
	$salesRawBefore = $sale->getSalesAnalysis($serial_zon, $serial_cou, $dateFromBefore, $dateToBefore, $_SESSION['serial_mbc'],
												$_SESSION['serial_dea'], $serial_mbc, $serial_cit, $serial_usr,$serial_dea,
												$serial_bra);
												
	//debug::print_r($salesRawActual);debug::print_r($salesRawBefore);die();
	$yearTo = substr($dateTo, 6, 4);
	$table = array(
		'branch'=>'<b>Sucursal</b>',
		'city'=>'<b>Ciudad</b>',
		'discount'=>'<b>Descuento</b>',
		'commission'=>'<b>'.utf8_decode(Comisión).'</b>',
		'before'=>'<b>'.($yearTo-1).'</b>',
		'actual'=>'<b>'.$yearTo.'</b>',
		'variation'=>'<b>% '.utf8_decode(Variación).'</b>'
	);
	$alignTable = array(
		'branch'=>array('justification'=>'center','width'=>100),
		'city'=>array('justification'=>'center','width'=>80),
		'discount'=>array('justification'=>'center','width'=>80),
		'commission'=>array('justification'=>'center','width'=>50),
		'before'=>array('justification'=>'center','width'=>50),
		'actual'=>array('justification'=>'center','width'=>50),
		'variation'=>array('justification'=>'center','width'=>50)
	);
	$optionsTable = array(
		'showLines'=>2,
		'showHeadings'=>1,
		'shaded'=>0,
		'fontSize' => 9,
		'textCol' =>array(0,0,0),
		'titleFontSize' => 12,
		'rowGap' => 5,
		'colGap' => 5,
		'lineCol'=>array(0.8,0.8,0.8),
		'xPos'=>'300',
		'xOrientation'=>'center',
		'maxWidth' => 805,
		'cols' =>$alignTable,
		'innerLineThickness' => 0.8,
		'outerLineThickness' => 0.8
	);
	include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$pdf->setColor(0.57, 0.58, 0.60);
	$pdf->ezText($date,13,array('justification'=>'left'));
	$pdf->setColor(0, 0, 0);
	$pdf->ezSetDy(-20);
	$pdf->ezText('<b>Reporte de '.utf8_decode(Análisis).' de Ventas</b>',14,array('justification'=>'center'));
	$manager = 'Todos'; $responsible = 'Todos'; $city = 'Todos'; $dealer = 'Todos'; $branch = 'Todos';
	$pdf->ezSetDy(-20);

	if($serial_mbc){
		$manager = $salesRawActual[0]['name_man'];
	}
	if($serial_cit){
		$city = $salesRawActual[0]['name_cit'];
	}
	if($serial_usr){
		$responsible = $salesRawActual[0]['name_usr'];
	}
	if($serial_dea){
		$dealer = $salesRawActual[0]['name_dea'];
	}
	if($serial_bra){
		$branch = $salesRawActual[0]['name_bra'];
	}
	$type='';
	if($_POST['selType'] == 'netSale'){
		$type='Neta';
	}
	else{
		$type='Bruta';
	}
	$parameters = array(
						'0'=>array('0'=>'<b>Zona: </b>', '1'=>$salesRawActual[0]['name_zon'], '2'=>'<b>'.utf8_decode(País).':</b>', '3'=>$salesRawActual[0]['name_cou']),
						'1'=>array('0'=>'<b>Desde:</b>','1'=>$dateFrom,'2'=>'<b>Hasta:</b>','3'=>$dateTo),
						'2'=>array('0'=>'<b>Representante:</b>','1'=>$manager,'2'=>'<b>Ciudad:</b>','3'=>$city),
						'3'=>array('0'=>'<b>Responsable:</b>','1'=>$responsible,'2'=>'<b>Comercializador:</b>','3'=>$dealer),
						'4'=>array('0'=>'<b>Sucursal:</b>','1'=>$branch,'2'=>'<b>Tipo de Venta:</b>','3'=>$type),
						);
	$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'330',
						'cols'=>array(
						'0'=>array('justification'=>'right','width'=>100),
						'1'=>array('justification'=>'left','width'=>130),
						'2'=>array('justification'=>'right','width'=>100),
						'3'=>array('justification'=>'left','width'=>130)
						)
				));

	$pdf->ezSetDy(-20);
	$rowsTable = array();
	$saleAmount = 0; $saleAmountBefore = 0;
	$dealer = ''; 
	foreach ($salesRawActual as $key => $sal) {
		if($sal['serial_dea'] != $dealer){
			$pdf->ezText('<b>Comercializador: </b>'.$sal['name_dea'],12,array('justification'=>'center'));
			$pdf->ezSetDy(-20);
		}
		if($type == 'Bruta'){//GROSS SALE
			$saleAmount += $sal['total_sal'];
		}
		else{//NET SALE
			$eTaxes=unserialize($sal['applied_taxes_inv']);
			$eTaxesAmount=0;
			if(is_array($eTaxes)){
				foreach($eTaxes as $t){
					$eTaxesAmount+=$t['tax_percentage'];
				}
				$eTaxesAmount=$eTaxesAmount/100;
			}
			$eDiscounts=$sal['discounts']/100;
			$saleAmount=$sal['total_sal']-($sal['total_sal']*$eDiscounts);
			$saleAmount+=($saleAmount*$eTaxesAmount);
		}
		if($sal['serial_bra'] != $salesRawActual[$key+1]['serial_bra']){
			if($sal['type_percentage_dea'] == 'COMISSION'){
				$discount = ''.utf8_decode(Comisión);
			}
			else{
				$discount = 'Descuento';
			}
			foreach ($salesRawBefore as $keyBefore => $salBefore) {
				if($salBefore['serial_bra'] == $sal['serial_bra']){
					if($type == 'Bruta'){//GROSS SALE
						$saleAmountBefore += $salBefore['total_sal'];
					}
					else{//NET SALE
						$eTaxes=unserialize($salBefore['applied_taxes_inv']);
						$eTaxesAmount=0;
						if(is_array($eTaxes)){
							foreach($eTaxes as $t){
								$eTaxesAmount+=$t['tax_percentage'];
							}
							$eTaxesAmount=$eTaxesAmount/100;
						}
						$eDiscounts=$salBefore['discounts']/100;
						$saleAmountBefore=$salBefore['total_sal']-($salBefore['total_sal']*$eDiscounts);
						$saleAmountBefore+=($saleAmountBefore*$eTaxesAmount);
					}
					if($salBefore['serial_bra'] != $salesRawBefore[$keyBefore+1]['serial_bra']){
						break;
					}
				}			
			}
			if($saleAmountBefore){
				$variation = number_format((($saleAmount - $saleAmountBefore)/$saleAmountBefore)*100, 2, '.', '');
			}
			else{
				$variation = 'N/A';
			}
			$rowsTable[] = array(
									'branch'=>$sal['name_bra'],
									'city'=>$sal['name_cit'],
									'discount'=>$discount,
									'commission'=>number_format($sal['percentage_dea'], 2, '.', ''),
									'before'=>number_format($saleAmountBefore, 2, '.', ''),
									'actual'=>number_format($saleAmount, 2, '.', ''),
									'variation'=>$variation
								);
			if($sal['serial_dea'] != $salesRawActual[$key+1]['serial_dea']){				
				$pdf->ezTable($rowsTable, $table, NULL,$optionsTable);
				$rowsTable = array();
				$pdf->ezSetDy(-20);
			}
			$saleAmount = 0; $saleAmountBefore = 0; 
		}		
		$dealer = $sal['serial_dea'];
	}
	$pdf->ezStream();
}else{
    http_redirect('modules/reports/sales/fSalesAnalysis');
}


?>