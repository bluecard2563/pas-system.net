<?php

if(isset($_POST['selZone']) && isset($_POST['selCountry'])){
   
	$sale = new Sales($db);
	$salesRaw = array();
    $sales = array();
    $colsAux = array();
	$alignCols=array();

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$date_from = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
	$date_to = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';
	$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
     
        $salesRaw = $sale->getFreeSalesByZone($serial_zon, $serial_cou, $_SESSION['serial_mbc'],$_SESSION['serial_dea'],
												$serial_man,$serial_cit,$serial_dea,$serial_bra,$date_from,$date_to,
												$status);
        //debug::print_r($salesRaw);die();
        if(!$salesRaw){
            http_redirect('modules/reports/sales/fFreeCardReport');
        }
            $colsAux = array(
                'date'=>'<b>Fecha</b>',
				'card'=>'<b># Tarjeta</b>',
				'responsible'=>'<b>Responsable</b>',
				'days'=>'<b>'.utf8_decode(Días).'</b>',
				'fee'=>'<b>Tarifa</b>',
				'destination'=>'<b>Destino</b>',
				'product'=>'<b>Producto</b>',
				'cost'=>'<b>Costo</b>',
            );
            $alignCols= array(
				'date'=>array('justification'=>'center','width'=>80),
				'card'=>array('justification'=>'center','width'=>50),
				'responsible'=>array('justification'=>'center','width'=>130),
				'days'=>array('justification'=>'center','width'=>30),
				'fee'=>array('justification'=>'center','width'=>50),
				'destination'=>array('justification'=>'center','width'=>90),
				'product'=>array('justification'=>'center','width'=>80),
				'cost'=>array('justification'=>'center','width'=>50)
            );
			$options = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 10,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignCols,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);
			include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
			$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
			$pdf->setColor(0.57, 0.58, 0.60);
			$pdf->ezText($date,13,array('justification'=>'left'));
			$pdf->setColor(0, 0, 0);
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>Reporte de Ventas Free</b>',14,array('justification'=>'center'));
			$pdf->ezSetDy(-20);
			$statusCard = 'Todos'; $manager = 'Todos'; $city = 'Todos'; $dealer = 'Todos'; $branch = 'Todos'; $dateFrom = 'Todos'; $dateTo = 'Todos';

			if($status){
				if($status == 'ACTIVE'){
					$statusCard = 'Autorizadas';
				}else{
					if($status == 'DENIED'){
						$statusCard = 'Negadas';
					}
					else{
						$statusCard = 'Pendientes';
					}
				}
			}

			if($serial_man){
				$manager = $salesRaw[0]['name_man'];
			}
			if($serial_cit){
				$city = $salesRaw[0]['name_cit'];
			}
			if($serial_dea){
				$dealer = $salesRaw[0]['name_dea'];
			}
			if($serial_bra){
				$branch = $salesRaw[0]['name_bra'];
			}
			if($date_from && $date_to){
				$dateFrom = $date_from;
				$dateTo = $date_to;
			}
			$parameters = array(
									'0'=>array('0'=>'<b>Zona: </b>', '1'=>$salesRaw[0]['name_zon'], '2'=>'<b>'.utf8_decode(País).': </b>', '3'=>$salesRaw[0]['name_cou']),
									'1'=>array('0'=>'<b>Representante:</b>','1'=>$manager,'2'=>'<b>Ciudad:</b>','3'=>$city),
									'2'=>array('0'=>'<b>Comercializador:</b>','1'=>$dealer,'2'=>'<b>Sucursal:</b>','3'=>$branch),
									'3'=>array('0'=>'<b>Fecha Desde:</b>','1'=>$dateFrom,'2'=>'<b>Fecha Hasta:</b>','3'=>$dateTo),
									'4'=>array('0'=>'<b>Estado:</b>','1'=>$statusCard,'2'=>'','3'=>'')
									);
			$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'330',
									'cols'=>array(
									'0'=>array('justification'=>'right','width'=>100),
									'1'=>array('justification'=>'left','width'=>130),
									'2'=>array('justification'=>'right','width'=>100),
									'3'=>array('justification'=>'left','width'=>130)
            )			));	

			$all = $pdf->openObject();
			$pdf->saveState();
			$pdf->addText(50,50,10,utf8_decode('*Las tarjetas N/A son del tipo corporativo, no generan ningún número al momento de la venta.'));
			$pdf->restoreState();
			$pdf->closeObject();
			$pdf->addObject($all,'all');
			$branch = '';
            foreach ($salesRaw as $key=> $sal) {
				
				if($sal['serial_bra'] != $branch){
					$pdf->ezSetDy(-20);
					$pdf->ezText('<b>Comercializador: </b>'.$sal['name_dea'],11,array('justification'=>'center'));
					$pdf->ezText('<b>Sucursal: </b>'.$sal['name_bra'],11,array('justification'=>'center'));
					$pdf->ezSetDy(-20);
				}
				
				$sales[] =array(
									'date'=>$sal['date_sal'],
									'card'=>$sal['card_number_sal'],
									'responsible'=>$sal['name_usr'],
									'days'=>$sal['days_sal'],
									'fee'=>number_format($sal['fee_sal'], 2, '.', ''),
									'destination'=>$sal['destination'],
									'product'=>$sal['name_pbl'],
									'cost'=>number_format($sal['total_cost_sal'], 2, '.', '')
								);

				if($sal['serial_bra']!=$salesRaw[$key+1]['serial_bra']){
					$pdf->ezTable($sales, $colsAux, NULL,$options);
					$pdf->ezSetDy(-20);
					$sales = array();
				}
				$branch = $sal['serial_bra'];
            }			
			$pdf->ezStream();
}else{
    http_redirect('modules/reports/sales/fFreeCardReport');
}


?>