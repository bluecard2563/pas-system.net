<?php
/*
    Document   : pTravelsByCardExcel
    Created on : 28-may-2010, 12:02:06
    Author     : Nicolas
    Description:
        Generates an Excel file with all travels registered belonging to a card .Receive the serial of the sale.
*/
Request::setInteger('0:serial_sal');
$sale=new Sales($db,$serial_sal);
$sale->getData();
$customer=new Customer($db,$sale->getSerial_cus());
$customer->getData();
$travelLog= new TravelerLog($db);
$travelLog->setSerial_sal($serial_sal);
$travelsRegistered=$travelLog->getTravelLogBySale(true);
$buffer='';
//title

//card details
$productByDealer=new ProductByDealer($db, $sale->getSerial_pbd());
$productByDealer->getData();
$productByCountry= new ProductByCountry($db,$productByDealer->getSerial_pxc());
$productByCountry->getData();
$product = new Product($db,$productByCountry->getSerial_pro());
$product->getData();
$product_by_language= new ProductbyLanguage($db);
$product_by_language->setSerial_lang($_SESSION['serial_lang']);
$product_by_language->setSerial_pro($product->getSerial_pro());
$product_by_language->getDataByProductAndLanguage();
$productName=$product_by_language->getName_pbl();
$days_available = $sale->getAvailableDays();
$buffer.="<table border='1'>
		<tr>
			<td colspan='5' align='center'><b>Reporte de Viajes registrados de la tarjeta de {$customer->getFirstname_cus()} {$customer->getLastname_cus()}</b></td>
		</tr>
		<tr>
			<td  align='center'>Producto</td>
			<td  align='center'>".utf8_decode('Días Contratados')."</td>
			<td  align='center'>".utf8_decode('Días Disponibles')."</td>
			<td  align='center'>Vigencia desde</td>
			<td  align='center'>Vigencia hasta</td>
		</tr>
		<tr>
			<td>".utf8_decode($productName)."</td>
			<td>{$sale->getDays_sal()}</td>
			<td>$days_available</td>
			<td>{$sale->getBeginDate_sal()}</td>
			<td>{$sale->getEndDate_sal()}</td>
		</tr>
		</table>";

$buffer.="<br/>";
//travels registered
if($travelsRegistered){
	$buffer.="<table border='1'>
				<tr>
					<td  align='center'>No. Tarjeta</td>
					<td  align='center'>Cliente</td>
					<td  align='center'>Destino</td>
					<td  align='center'>Desde</td>
					<td  align='center'>Hasta</td>
					<td  align='center'>".utf8_decode('No. de días')."</td>
					<td  align='center'>Estado</td>
				</tr>";
	foreach($travelsRegistered as $tr){
		$buffer.="<tr>
					<td  align='center'>{$tr['card_number']}</td>
					<td  align='center'>".utf8_decode($tr['customer_name'])."</td>
					<td  align='center'>".utf8_decode($tr['name_cit'])."</td>
					<td  align='center'>{$tr['start_trl']}</td>
					<td  align='center'>{$tr['end_trl']}</td>
					<td  align='center'>{$tr['days_trl']}</td>
					<td  align='center'>".utf8_decode($global_travelRegisterStatus[$tr['status_trl']])."</td>
				</tr>";

	}
	$buffer.="</table>";
	}else{
	$buffer.="<b>No existen viajes registrados</b>";
}


	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=TravelsByCard.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $buffer;
?>
