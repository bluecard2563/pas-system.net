<?php
/*
    Document   : pTravelsByCardPDF
    Created on : 28-may-2010, 12:01:39
    Author     : Nicolas
    Description:
        Generates an Excel file with all travels registered belonging to a card . Receive the serial of the sale.
*/

Request::setInteger('0:serial_sal');
$sale=new Sales($db,$serial_sal);
$sale->getData();
$customer=new Customer($db,$sale->getSerial_cus());
$customer->getData();
$travelLog= new TravelerLog($db);
$travelLog->setSerial_sal($serial_sal);
$travelsRegistered=$travelLog->getTravelLogBySale(true);
//Debug::print_r($travelsRegistered);
//starts pdf
include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
$pdf->ezSetDy(-20);
//title
$pdf->ezText("<b>Reporte de Viajes registrados de la tarjeta de {$customer->getFirstname_cus()} {$customer->getLastname_cus()}</b>", 12, array('justification' =>'center'));
$pdf->ezSetDy(-10);

//card details
$productByDealer=new ProductByDealer($db, $sale->getSerial_pbd());
$productByDealer->getData();
$productByCountry= new ProductByCountry($db,$productByDealer->getSerial_pxc());
$productByCountry->getData();
$product = new Product($db,$productByCountry->getSerial_pro());
$product->getData();
$product_by_language= new ProductbyLanguage($db);
$product_by_language->setSerial_lang($_SESSION['serial_lang']);
$product_by_language->setSerial_pro($product->getSerial_pro());
$product_by_language->getDataByProductAndLanguage();
$productName=$product_by_language->getName_pbl();
//Debug::print_r($product);
$days_available = $sale->getAvailableDays();
$product_description=array();
$descLine1=array('name'=>utf8_decode($productName),
				 'sale_days'=>$sale->getDays_sal(),
				 'days_available'=>$days_available,
				 'validity_from'=>$sale->getBeginDate_sal(),
				 'validity_to'=>$sale->getEndDate_sal());
array_push($product_description,$descLine1);
//Debug::Print_r($product_description);
$pdf->ezTable($product_description,
				  array('name'=>'<b>Producto</b>',
					    'sale_days'=>'<b>'.utf8_decode('Días Contratados').'</b>',
					    'days_available'=>'<b>'.utf8_decode('Días Disponibles').'</b>',
					    'validity_from'=>'<b>'.utf8_decode('Vigencia desde').'</b>',
					    'validity_to'=>'<b>'.utf8_decode('Vigencia hasta').'</b>'),
				  '',
				  array('xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'name'=>array('justification'=>'center','width'=>100),
							'sale_days'=>array('justification'=>'center','width'=>60),
							'days_available'=>array('justification'=>'center','width'=>60),
							'validity_from'=>array('justification'=>'center','width'=>60),
							'validity_to'=>array('justification'=>'center','width'=>60))));
$pdf->ezSetDy(-10);
//

//travels registered
if($travelsRegistered){
	foreach($travelsRegistered as &$tr){
		for($i=0;$i<8;$i++){
			unset($tr[$i]);
		}
		unset($tr['serial_trl']);
		$tr['status_trl']=$global_travelRegisterStatus[$tr['status_trl']];
	}
$pdf->ezTable($travelsRegistered,
				  array('card_number'=>'<b>No. Tarjeta</b>',
					    'customer_name'=>'<b>Cliente</b>',
					    'name_cit'=>'<b>Destino</b>',
					    'start_trl'=>'<b>Desde</b>',
						'end_trl'=>'<b>Hasta</b>',
						'days_trl'=>'<b>'.utf8_decode('No. de días').'</b>',
						'status_trl'=>'<b>Estado</b>'),
				  '',
				  array('xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'card_number'=>array('justification'=>'center','width'=>60),
							'customer_name'=>array('justification'=>'center','width'=>120),
							'name_cit'=>array('justification'=>'center','width'=>70),
							'start_trl'=>array('justification'=>'center','width'=>60),
							'end_trl'=>array('justification'=>'center','width'=>60),
							'days_trl'=>array('justification'=>'center','width'=>60),
							'status_trl'=>array('justification'=>'center','width'=>60))));
	}else{
	$pdf->ezText("No existen viajes registrados.", 12, array('justification' =>'center'));
}
$pdf->ezStream();
?>
