<?php
/*
    Document   : pCardsForRegisterByCustomerExcel
    Created on : 31-may-2010, 9:19:11
    Author     : Nicolas
    Description:
        Generates an XLS file with all cards available for travel registering
*/
Request::setInteger("0:serial_cus");
$customer=new Customer($db,$serial_cus);
$customer->getData();
$list=$customer->getRegisterCardByCustomer(TRUE);
$buffer='';
//cards
if($list){
	foreach($list as &$li){
		for($i=0;$i<7;$i++){
			unset($li[$i]);
		}
		if($li['card_number_sal']=='')
				$li['card_number_sal']='N/A';
	}
	$buffer.="<table border='1'>
				<tr>
					<td colspan='6' align='center'><b>Tarjetas que registran viajes de {$customer->getFirstname_cus()} {$customer->getLastname_cus()}</b></td>
				</tr>
				<tr>
					<td  align='center'><b>Producto</b></td>
					<td  align='center'><b>No. Tarjeta</b></td>
					<td  align='center'><b>Vigencia Desde</b></td>
					<td  align='center'><b>Vigencia Hasta</b></td>
					<td  align='center'><b>".utf8_decode('Días Contratados')."</b></td>
					<td  align='center'><b>".utf8_decode('Días Disponibles')."</b></td>
				</tr>";
	foreach($list as $lt){
		$buffer.="<tr>
					<td  align='center'>".utf8_decode($lt['name_pbl'])."</td>
					<td  align='center'>{$lt['card_number_sal']}</td>
					<td  align='center'>{$lt['begin_date_sal']}</td>
					<td  align='center'>{$lt['end_date_sal']}</td>
					<td  align='center'>{$lt['days_sal']}</td>
					<td  align='center'>{$lt['available_days']}</td>
				</tr>";

	}
	$buffer.="</table>";
	}else{
	$buffer.="No existen tarjetas que permitan registrar viajes.";
}


	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=CardsAllowTravelRegister.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $buffer;
?>
