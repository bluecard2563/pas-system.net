<?php
/*
File: pCardXLSReport
Author: David Bergmann
Creation Date: 31/05/2010
Last Modified: 24/06/2010
Modified By: David Bergmann
*/

set_time_limit(36000);
ini_set('memory_limit','512M');

$cardList = Sales::getCardReport($db, $_POST['selCountry'],$_POST['rdoDate'],$_POST['txtBeginDate'],$_POST['txtEndDate'],
                                 $_POST['selCity'],$_POST['selManager'], $_POST['selDealer'],$_POST['selBranch'],$_POST['selProduct'],
                                 $_POST['selType'], $_POST['selStatus'],$_POST['selOperator'],$_POST['txtAmount'],$_POST['selStockType'],$_POST['selOrderBy']);

$cardAmount = 0;
$totalAmount = 0;
if(is_array($cardList)) {
	$cardAmount = count($cardList);
	foreach ($cardList as $c) {
		$totalAmount +=$c['col8'];
	}
}

$table = "<table border='1'>
			<tr>
				<td>N&uacute;mero de tarjetas:</td>
				<td>$cardAmount</td>
			</tr>
			<tr>
				<td>Total de las ventas:</td>
				<td>$totalAmount</td>
			</tr>
		  </table>";

if(is_array($cardList)) {
    $table.="<table border='1'><tr>
            <th><b># Tarjeta</b></th>
            <th><b>Fec. Emisi&oacute;n</b></th>
            <th><b>Cliente</b></th>
            <th><b>Producto</b></th>
            <th><b>Desde</b></th>
            <th><b>Hasta</b></th>
            <th><b>Tiempo</b></th>
            <th><b>Precio</b></th>
            <th><b>Estado</b></th>
            <th><b>Comercializador</b></th>
            <th><b>Counter</b></th>
			<th><b>Venta</b></th></tr>";

    foreach ($cardList as $cl) {
        $table.="<tr>
                 <td>".$cl['col1']."</td>
                 <td>".$cl['col2']."</td>
                 <td>".$cl['col3']."</td>
                 <td>".$cl['col4']."</td>
                 <td>".$cl['col5']."</td>
                 <td>".$cl['col6']."</td>
                 <td>".$cl['col7']."</td>
                 <td>".$cl['col8']."</td>
                 <td>".$global_salesStatus[$cl['col9']]."</td>
                 <td>".$cl['col10']."</td>
                 <td>".$cl['col11']."</td>
				 <td>".$cl['col12']."</td></tr>";
    }
    $table.="</table>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Tarjetas.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $table;
?>
