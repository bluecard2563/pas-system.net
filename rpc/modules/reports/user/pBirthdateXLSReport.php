<?php
/*
File: pBirthdateXLSReport
Author: David Bergmann
Creation Date: 07/06/2010
Last Modified:
Modified By:
*/

$data = User::getBirthdateReport($db,$_POST['selCountry'],$_POST['selCity'],$_POST['selManager'],$_POST['selMonth']);

$report = "<table>
           <tr>
                <th>Documento #</th>
                <th>Usuario</th>
                <th>Fecha de nacimiento</th>
           </tr>";
foreach ($data as $d) {
    $report .= "<tr>
                    <td>".$d['col1']."</td>
                    <td>".$d['col2']."</td>
                    <td>".$d['col3']."</td>
               </tr>";
}
$report .= "</table>";

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=".utf8_decode("Cumpleaños").".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $report;
?>
