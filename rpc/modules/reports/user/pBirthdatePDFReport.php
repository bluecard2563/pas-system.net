<?php
/*
File: pBirthdatePDFReport
Author: David Bergmann
Creation Date: 07/06/2010
Last Modified:
Modified By:
*/
include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';



//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE CUMPLEAÑOS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$data = User::getBirthdateReport($db,$_POST['selCountry'],$_POST['selCity'],$_POST['selManager'],$_POST['selMonth']);

if(is_array($data)) {
    $titles=array('col1'=>utf8_decode('<b>Documento #</b>'),
                  'col2'=>utf8_decode('<b>Usuario</b>'),
                  'col3'=>utf8_decode('<b>Fecha de nacimiento</b>'));

    $pdf->ezTable($data,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>100),
                                        'col2'=>array('justification'=>'center','width'=>180),
                                        'col3'=>array('justification'=>'center','width'=>100))));
        $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>
