<?php 
/*
File: sendCartToSessionAjax.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('cart_prods');
	
	$preOrder = new PreOrder($db,NULL,$_SESSION['serial_customer_estimator'],'CUSTOMER',time());
	$cartProducts = $preOrder -> getAllMyCartProductItems(Language::getSerialLangByCode($db,$_SESSION['language']));
	
	if($cart_prods != 'false'){
		$productIdList = explode(',',$cart_prods);
		foreach ($productIdList as $prodId){
			$cartProducts[$prodId]['save'] = true;
		}
	}else{
		unset($_SESSION['system_estimator_shopping_cart_products']);
	}
	//DELETE ALL PODS NOT SELECTED ON THE 3RD TAB:
	foreach ($cartProducts as $prod){
		if(!$prod['save']){
			PreOrderDetail::deletePOD($db,$prod['serial_pod']);
		}
	}
	
	unset($_SESSION['web_estimator_session_products']);
	echo 'ok';
?>