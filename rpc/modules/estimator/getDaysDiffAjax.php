<?php 
/*
File: gwtProductEstimatorPropertiesAjax.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('beginDate');
	Request::setString('endDate');
	
	echo GlobalFunctions::getDaysDiffMmDdYyyy($beginDate, $endDate);
?>