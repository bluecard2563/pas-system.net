<?php 
/*
File: parseSessionCartToDB.php
Author: Santiago Borja
Creation Date: 01/04/2010
*/
	//EXTRA SECURITY CHECK
	if(!isset($_SESSION['serial_customer_estimator'])){
		http_redirect('modules/estimator/fEstimatorCustomerLogin');
	}
	
	unset($_SESSION['web_customer_redirect']);
	$_SESSION['web_customer_cart_direct'] = true;
	
	if(isset($_SESSION['system_estimator_shopping_cart_products'])){
		$sessionCart = $_SESSION['system_estimator_shopping_cart_products'];
		foreach($sessionCart as $sessProd){
			$error = GlobalFunctions::saveCartProductToDB($db,$sessProd,$_SESSION['serial_customer_estimator'],$webSalesCounterId);
			if($error != 5){
				http_redirect('modules/estimator/registered/estimatorCustomer/'.$error);
			}
		}
		unset($_SESSION['system_estimator_shopping_cart_products']);
		http_redirect('modules/estimator/registered/estimatorCustomer');
	}else{
		http_redirect('modules/estimator/registered/estimatorCustomer'); //EMPTY CART WARNING DISABLED, ADD PARAM /1 TO ENABLE
	}
?>