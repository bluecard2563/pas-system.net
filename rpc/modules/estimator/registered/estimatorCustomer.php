<?php 
/*
File: estimatorCustomer.php
Author: Santiago Borja
Creation Date: 01/04/2010
*/
	//EXTRA SECURITY CHECK
	if(!isset($_SESSION['serial_customer_estimator'])){
		http_redirect('modules/estimator/fEstimatorCustomerLogin');
	}

	Request::setString('0:error');
	Request::setString('1:goTo');
	
	$serial_cou = $_SESSION['web_selDeparture'];
	$product = new Product($db);
	if($serial_cou){
	    $products = $product->getProductsByCountryEstimator($serial_cou, $_SESSION['serial_lang']);
	}
	
	if(!$products){
		 $defaultDealer = new Parameter($db,'28');
	     $defaultDealer -> getData();
		 $products = $product->getProductsByCountryEstimator($defaultDealer -> getValue_par(), $_SESSION['serial_lang']);
	}
	
	if($products){
	    foreach($products as &$pl){
	        $pl['name_pbl'] = utf8_encode($pl['name_pbl']);
	    }
	}
	
	$countries = Country::getAllCountries($db,false);	
	
	$preOrder = new PreOrder($db,NULL,$_SESSION['serial_customer_estimator'],'CUSTOMER',time());
	
	$cartNotEmpty = !$preOrder -> isCartEmpty()?true:false;
	
	//DATA FROM SMALL ESTIMATOR:
	$estimatorData = '';
	$estimatorData .= $_SESSION['web_selProduct'].'%s%';
	$estimatorData .= $_SESSION['web_selDeparture'].'%s%';
	$estimatorData .= $_SESSION['web_selDestination'].'%s%';
	$estimatorData .= $_SESSION['web_checkPartner'].'%s%';
	$estimatorData .= $_SESSION['web_selTripDays'].'%s%';
	$estimatorData .= $_SESSION['web_txtBeginDate'].'%s%';
	$estimatorData .= $_SESSION['web_txtEndDate'].'%s%';
	$estimatorData .= $_SESSION['web_txtOverAge'].'%s%';
	$estimatorData .= $_SESSION['web_txtUnderAge'];
	
	// do unset vars, we wont use them again
	unset($_SESSION['web_selProduct']);
	unset($_SESSION['web_selDeparture']);
	unset($_SESSION['web_selDestination']);
	unset($_SESSION['web_checkPartner']);
	unset($_SESSION['web_selTripDays']);
	unset($_SESSION['web_txtBeginDate']);
	unset($_SESSION['web_txtEndDate']);
	unset($_SESSION['web_txtOverAge']);
	unset($_SESSION['web_txtUnderAge']);
	
	unset($_SESSION['data_already_added']);
	unset($_SESSION['cart_products_travelers_information']);
	
	if(isset($_SESSION['web_customer_cart_direct'])){
		$goTo = '3';
		unset($_SESSION['web_customer_cart_direct']);	
	}
	
	$smarty -> register('error,products,countries,estimatorData,cartNotEmpty,goTo');
	$smarty->assign('container','default_estimator');
	$smarty -> display();
?>