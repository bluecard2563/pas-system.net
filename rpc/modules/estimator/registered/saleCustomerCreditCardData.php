<?php 
/*
File: saleCustomerCreditCardData.php
Author: Santiago Borja
Creation Date: 10/04/2010
*/
	//EXTRA SECURITY CHECK
	if(!isset($_SESSION['serial_customer_estimator'])){
		http_redirect('modules/estimator/fEstimatorCustomerLogin');
	}
	
	Request::setString('0:error');
	
	$sessionProducts = $_SESSION['cart_products_travelers_information'];
	$finalPrice = 0;
	
	$productPricesTable = array();
	$num = 0;
	
	if(is_array($sessionProducts)){
		foreach($sessionProducts as $sessProd){
			if($sessProd['informationComplete']){
				$productPricesTable[] = array ('type' => 'product',
								   'num' => ++$num,
								   'name' => $sessProd['name_pbl'],
								   'price' => $sessProd['total_price_pod']);
				
				//THE TAXES:
				$tbc = new TaxesByProduct($db, $sessProd['serial_pxc']);
	    		$taxes = $tbc -> getTaxesByProduct();
	    		$taxesToApply = 0;
	
	    		if($taxes){
		    		foreach ($taxes as $k=>$n){
						$price = intval($n['percentage_tax']) * $sessProd['total_price_pod']/100;
						$productPricesTable[] = array ('type' => 'tax',
									   	   				'name' => $n['name_tax'],
										   				'percentage' => $n['percentage_tax'],
									   	   				'price' => $price);
						$taxesToApply += $price;
					}
	    		}
				
				$partialPrice = $sessProd['total_price_pod'] + $taxesToApply;
				$finalPrice += $partialPrice;
				$symbolCur = $sessProd['symbol_cur'];
				
				$productPricesTable[] = array (	'type' => 'total',
								   				'name' => 'Subtotal:',
								   				'price' => $partialPrice);
				$saleProductsArray[] = $sessProd['serial_pod'];
			}
		}
	}
	
	$productPricesTable[] = array (	'type' => 'total',
							   		'name' => 'Precio Final:',
							   		'price' => $finalPrice);
	$_SESSION['current_sale_total'] = $finalPrice;
	$_SESSION['current_sale_products'] = implode("_",$saleProductsArray);
	
	$countries = Country::getAllCountries($db,false);
	
	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$years = array();
	for($i = date('Y');$i <= date('Y') + 10; $i++){
		$years[] = $i;
	}
	
	$smarty -> register('error,countries,symbolCur,productPricesTable,months,years');
	$smarty -> display();
?>