<?php 
/*
File: pCheckCustomerLoggedAndCartEmptyAjax.php
Author: Santiago Borja
Creation Date: 30/04/2010
*/
	$userLogged = isset($_SESSION['serial_customer_estimator'])?'1':'0';
	if($userLogged){
		$preOrder = new PreOrder($db,NULL,$_SESSION['serial_customer_estimator'],'CUSTOMER',time());
		$cartNotEmpty = !$preOrder -> isCartEmpty()?'1':'0';
	}else{
		$cartNotEmpty = isset($_SESSION['system_estimator_shopping_cart_products'])?'1':'0';
	}
	
	echo $userLogged.'%S%'.$cartNotEmpty;
?>