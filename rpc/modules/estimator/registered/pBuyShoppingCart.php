<?php
/*
File: pBuyShoppingCart.php
Author: Santiago Borja Lopez
Creation Date: 16/04/2010 10:00
*/
	include(DOCUMENT_ROOT.'lib/merchantAccountConnection.inc.php');
	$_SESSION['serial_lang'] =  Language::getSerialLangByCode($db,$_SESSION['language']);
	
	Request::setString('0:error');
	Request::setString('1:extra_code');
	
	$error = $custom_error_code;
	$extra_code = 0;

	if($error != 40){
		escapeOnError($db,$error,'PHONE SALE DENIED',$error,$extra_code);
	}
	
	//We reached this point after a successful transaction!
	$cartProds = $_SESSION['cart_products_travelers_information'];
	
	$redirectToFlightsLog = false;
	$dbFail = 0;
	$todaysDate = date("d").'/'.date("m").'/'.date("Y");
	
	$validityParam = new Parameter($db,'14');
    $validityParam->getData();
    $validityPeriod = $validityParam -> getValue_par();
	
	foreach ($cartProds as $prod){
		if($prod['informationComplete']){
			$prodByCou = new ProductByCountry($db,$prod['serial_pxc']);
			$prodByCou -> getData();
			
			$departureCou = $prodByCou -> getSerial_cou();
			$userPhoneSalesCounter = Counter::getDeparturePhoneSalesCounter($db, $_SESSION['serial_usr'], $departureCou);
			if(!$userPhoneSalesCounter){
				$userPhoneSalesCounter = Counter::getDeparturePhoneSalesCounter($db, $_SESSION['serial_usr'], 1);
			}
			
			$serialDea = Counter::getCountersTopDealer($db,$userPhoneSalesCounter);
			$dealerPhone = new Dealer($db,$serialDea);
			$dealerPhone -> getData();
			$counterPhone = new Counter($db, $userPhoneSalesCounter);
			$counterPhone -> getData();
			
			$city = new City($db);
			$destinationCities = $city -> getCitiesByCountry($prod['serial_cou']);
			
			//TODO get from departure country and not prices country
			$serialPbd = ProductByDealer::getPBDbyPXC($db,$serialDea,$prod['serial_pxc']);
			
			if(!$serialPbd){
                $defaultDealer = new Parameter($db,'28');
                $defaultDealer -> getData();
				$serialPbd = ProductByDealer::getPBDbyPXC($db,$defaultDealer -> getValue_par(),$prod['serial_pxc']); //USES THE DEFAULT DEALER 1
			}
			
			//For several_flights = 0
			$validUntil = date("d/m/Y",strtotime($prod['begin_date_pod']. ' +'.$validityPeriod.' days'));
			$prod['begin_date_pod'] = date("d/m/Y",strtotime($prod['begin_date_pod']));
			$prod['end_date_pod'] = date("d/m/Y",strtotime($prod['end_date_pod']));
			
			//Begin for each card:
			foreach ($prod['cards'] as $card){
				//Generate the Card Number:
				$stock=new Stock($db);
				$cardNumber = $stock->getNextStockNumber($userPhoneSalesCounter,$migration_date_for_stock);
			    if(!$cardNumber){
			        $stock->autoAssign($userPhoneSalesCounter);
			        $cardNumber = $stock -> getNextStockNumber($userPhoneSalesCounter,$migration_date_for_stock);
			    }
			    
			    //Calculate cost:
				$totalCost = $prod['cost_pod'];
			    foreach ($card['extras'] as $extra){
			    	$totalCost += $extra['ownCost'];
			    }
			    
			    //Code fix for serveral flights products:
				if($prod['several_flights'] == 0){
					$redirectToFlightsLog = true;
			    	$destinationCities[0]['serial_cit'] = 'NULL';
			    	$prod['end_date_pod'] = $validUntil;
			    	$cardNumber = 'N/A';
			    }
			    
			    //********************************************** THE HOLDER *************************************:
				$holder = $card['holder'];
				$customerH = new Customer($db);
				$customerH -> setDocument_cus($holder['document_cus']);
				$customerH -> setSerial_cit($holder['serial_cit']);
				$customerH -> getData(true);
				
				//This info is always saved/updated:
				$customerH -> setPhone1_cus($holder['phone1_cus']);
				$customerH -> setCellphone_cus($holder['cellphone_cus']);
				$customerH -> setAddress_cus($holder['address_cus']);
				$customerH -> setRelative_cus($holder['relative_cus']);
				$customerH -> setRelative_phone_cus($holder['relative_phone_cus']);
				$customerH -> setStatus_cus('ACTIVE');
				
				if($customerH -> existsCustomer($holder['document_cus'], NULL)){
					$customerH -> update();//Update previous entered data
					
					ERP_logActivity('update', $customerH);//ERP ACTIVITY
				}else{
					$customerH -> setSerial_cit($holder['serial_cit']);
					$customerH -> setType_cus('PERSON');
					$customerH -> setFirstname_cus($holder['first_name_cus']);
					$customerH -> setLastname_cus($holder['last_name_cus']);
					$customerH -> setBirthdate_cus($holder['birthdate_cus']);
					$customerH -> setEmail_cus($holder['email_cus']);
					$customerH -> insert();
					
					ERP_logActivity('new', $customerH);//ERP ACTIVITY
				}
				
				$customerH -> getData(true);
				$serialHolder = $customerH -> getSerial_cus();
				if(!$serialHolder) escapeOnError($db,11,'customer save',$customerH);//DB FAIL LOG
				
				//THE ELDERLY QUESTIONS:
				if(is_array($holder['answers'])){
					$answersHolder = $holder['answers'];
					$questionList = Question::getActiveQuestions($db, $_SESSION['serial_lang']);
					
					Answer::deleteMyAnswers($db, $serialHolder);
					foreach($questionList as $q){
						$answer = new Answer($db);
						$answer -> setSerial_cus($serialHolder);
						$answer -> setSerial_que($q['serial_que']);
						$ans = $answersHolder[$q['serial_qbl']];
		
						if($ans){
							$answer -> setYesno_ans($ans);
							if(!$answer -> insert())
								escapeOnError($db,12,'answer insert failed',$answer);//DB FAIL LOG
						}
					}
				}
				
				//********************************************** THE PAYMENT *************************************:
				$payment = new Payment($db);
				$payment -> setTotal_to_pay_pay($card['card_price_pod']);
			    $payment -> setTotal_payed_pay($card['card_price_pod']);
			    $payment -> setStatus_pay('PAID');
			    $payment -> setDate_pay($todaysDate);
			    $payment -> setObservation_pay('Pago realizado con tarjeta de credito');
			    $payment -> setExcess_amount_available_pay(0);
			    $paymentId = $payment -> insert();
			    
				ERP_logActivity('new', $payment);//ERP ACTIVITY
			    
				if(!$paymentId)escapeOnError($db,12,'payment save',$payment);//DB FAIL LOG
				
			    //THE PAYMENT DETAIL:
				$paymentDetail = new PaymentDetail($db);
			    $paymentDetail -> setSerial_pay($paymentId);
				$paymentDetail -> setSerial_cn(NULL);
				$paymentDetail -> setSerial_usr($counterPhone -> getSerial_usr());
			    $paymentDetail -> setType_pyf('CREDIT_CARD');
			    $paymentDetail -> setAmount_pyf($card['card_price_pod']);
			    $paymentDetail -> setDate_pyf($todaysDate);
			    $paymentDetail -> setDocumentNumber_pyf($_SESSION['ccnum']);
			    $paymentDetail -> setComments_pyf('');
			    $paymentDetail -> setStatus_pyf('ACTIVE');
				$paymentDetail -> setObservation_pyf(NULL);
				$serialPaymentDetail = $paymentDetail -> insert();
				if(!$serialPaymentDetail)escapeOnError($db,13,'payment det save',$paymentDetail);//DB FAIL LOG
				
				//********************************************** THE INVOICE *************************************:
    			//taxes:
    			$tbc = new TaxesByProduct($db, $prod['serial_pxc']);
    			$taxes = $tbc -> getTaxesByProduct();
    			$taxesApplied = 0;
    			if($taxes){
	    			foreach ($taxes as $k=>$n){
						$misc[$k]['name_tax']=$n['name_tax'];
						$misc[$k]['tax_percentage']=$n['percentage_tax'];
						$taxesToApply += intval($n['percentage_tax']);
					}
    			}
    			
    			//credit days:
    			$creditDay = new CreditDay($db, $dealerPhone -> getSerial_cdd());
    			$creditDay -> getData();
    			$tempDate = str_replace("/", "-", $todaysDate);
    			
    			if($creditDay -> getDays_cdd() == ''){
    				$creditDay -> setDays_cdd(0);
    			}
				$dueDate = date("d/m/Y",strtotime($tempDate. ' +'.$creditDay -> getDays_cdd().' days'));
			
				//Get the Document Number:
				$tempMbc = new ManagerbyCountry($db, $dealerPhone -> getSerial_mbc());
				$tempMbc -> getData();
				
				$serialDbm = DocumentByManager::retrieveSelfDBMSerial($db, $tempMbc -> getSerial_man(), 'PERSON', 'INVOICE');
				$serialDbm = $serialDbm['serial_dbm'];
				$number = DocumentByManager::retrieveNextDocumentNumber($db, $tempMbc -> getSerial_man(), 'PERSON', 'INVOICE', true);
				
				$invoice = new Invoice($db);
				$invoice -> setSerial_cus($serialHolder);
				$invoice -> setSerial_pay($paymentId);
				$invoice -> setSerial_dea(NULL);
				$invoice -> setSerial_dbm($serialDbm);
				$invoice -> setDate_inv($todaysDate);
				$invoice -> setDue_date_inv($dueDate);
				$invoice -> setNumber_inv($number);
				if($dealerPhone -> getType_percentage_dea() == 'COMISSION'){
					$invoice -> setComision_prcg_inv($dealerPhone -> getPercentage_dea());
					$invoice -> setDiscount_prcg_inv(NULL);
				}else{
					$invoice -> setDiscount_prcg_inv($dealerPhone -> getPercentage_dea());
					$invoice -> setComision_prcg_inv(NULL);
				}
				$invoice -> setOther_dscnt_inv(0);
				$invoice -> setComments_inv("Factura Web");
				$invoice -> setPrinted_inv('NO');
				$invoice -> setStatus_inv('PAID');
				$invoice -> setSubtotal_inv($card['card_price_pod'] - $invoice -> getOther_dscnt_inv() - $invoice -> getDiscount_prcg_inv());
				$invoice -> setTotal_inv($invoice -> getSubtotal_inv() * (1 + $taxesToApply/100));
			    $invoice -> setHas_credit_note_inv('NO');
			    $invoice -> setApplied_taxes_inv(serialize($misc));
		        $serialInv = $invoice -> insert();
		        
		        ERP_logActivity('new', $invoice);//ERP ACTIVITY
		        
		        if(!$serialInv) escapeOnError($db,16, 'invoice save',$invoice);//DB FAIL LOG
		        
				//********************************************** THE SALE *************************************:
				$sale = new Sales($db);
			    $sale -> setSerial_cus($serialHolder);
			    $sale -> setSerial_pbd($serialPbd);
			    $sale -> setSerial_cit($destinationCities[0]['serial_cit']);
			    $sale -> setSerial_cnt($userPhoneSalesCounter);
			    $sale -> setSerial_inv($serialInv);
			    $sale -> setCardNumber_sal($cardNumber);
			    $sale -> setEmissionDate_sal($todaysDate);
			    $sale -> setBeginDate_sal($prod['begin_date_pod']);
				$sale -> setEndDate_sal($prod['end_date_pod']);
			    $sale -> setInDate_sal($todaysDate);
			    $sale -> setDays_sal($prod['days_pod']);
			    $sale -> setFee_sal($prod['price_pod']);
			    $sale -> setObservations_sal("Venta Telefónica");
			    $sale -> setCost_sal($prod['cost_pod']);
			    $sale -> setFree_sal('NO');
			    $sale -> setIdErpSal_sal(NULL);
			    $sale -> setStatus_sal('REGISTERED');
			    $sale -> setType_sal('PHONE');
			    $sale -> setTotal_sal($card['card_price_pod']);
			    $sale -> setStockType_sal('VIRTUAL');
			    $sale -> setDirectSale_sal($sale -> isDirectSale($userPhoneSalesCounter));
			    $sale -> setChange_fee_sal(1);
			    $sale -> setTotalCost_sal($totalCost);
			    $sale -> setSerial_cur($prod['serial_cur']);
			    $sale -> setInternationalFeeStatus_sal('PAID');
		        $sale -> setInternationalFeeUsed_sal('YES');
			    $sale -> setInternationalFeeAmount_sal($totalCost);
			    $serialSale = $sale -> insert();
			    
			    if(!$serialSale) escapeOnError($db,17, 'sale save', $sale);//DB FAIL LOG
			    
			    //********************************************** THE EXTRAS *************************************:
				foreach ($card['extras'] as $key => &$extra){
					$customerX = new Customer($db);
					$customerX -> setDocument_cus($extra['idExtra']);
					$customerX -> setSerial_cit($holder['serial_cit']);
					
					if(!$customerX -> existsCustomer($extra['idExtra'], NULL)){ //If exists we dont update
						$customerX -> setType_cus('PERSON');
						$customerX -> setFirstname_cus($extra['nameExtra']);
						$customerX -> setLastname_cus($extra['lastnameExtra']);
						$customerX -> setBirthdate_cus($extra['birthdayExtra']);
						//$customerX -> setPhone1_cus(NULL);
						//$customerX -> setPhone2_cus(NULL);
						//$customerX -> setCellphone_cus(NULL);
						$customerX -> setEmail_cus($extra['emailExtra']);
						//$customerX -> setAddress_cus($address_cus);
						//$customerX -> setRelative_cus($relative_cus);
						//$customerX -> setRelative_phone_cus($relative_phone_cus);
						//$customerX -> setVip_cus($vip_cus);
						$customerX -> setStatus_cus('ACTIVE');
						//$customerX -> setPassword_cus($password_cus);
						$serialCustomer = $customerX -> insert();
						
						ERP_logActivity('new', $customerX);//ERP ACTIVITY
					}else{
						$customerX -> getData(true);
						$serialCustomer = $customerX -> getSerial_cus();
					}
					if(!$serialCustomer) escapeOnError($db,18, 'customer extra save', $customerX);//DB FAIL LOG
					
					//THE ELDERLY QUESTIONS:
					if(is_array($extra['answers'])){
						$answersExtra = $extra['answers'];
						$questionList = Question::getActiveQuestions($db, $_SESSION['serial_lang']);
						
						Answer::deleteMyAnswers($db, $serialCustomer);
						foreach($questionList as $q){
							$answer = new Answer($db);
							$answer -> setSerial_cus($serialCustomer);
							$answer -> setSerial_que($q['serial_que']);
							$ans = $answersExtra[$q['serial_qbl']];
			
							if($ans){
								$answer -> setYesno_ans($ans);
								if(!$answer -> insert())
									escapeOnError($db,12,'answer extra insert failed',$answer);//DB FAIL LOG
							}
						}
					}
					
				    if($prod['third_party_register_pro'] == 'YES'){
					    $travelerLog = new TravelerLog($db);
						$travelerLog -> setSerial_cus($serialCustomer);
						$travelerLog -> setSerial_sal($serialSale);
						$travelerLog -> setCard_number_trl(NULL);
						$travelerLog -> setStart_trl($prod['begin_date_pod']);
						$travelerLog -> setEnd_trl($prod['end_date_pod']);
						$travelerLog -> setDays_trl($prod['days_pod']);
						$travelerLog -> setSerial_cnt($userPhoneSalesCounter);
						$travelerLog -> setSerial_cit($destinationCities[0]['serial_cit']);
						$travelerID = $travelerLog -> insert();
						if(!$travelerID) escapeOnError($db,19,'traveler log save', $travelerLog);//DB FAIL LOG
				    }else{
				    	$newExtra = new Extras($db);
					    $newExtra -> setSerial_sal($serialSale);
					    $newExtra -> setSerial_cus($serialCustomer);
					    $newExtra -> setSerial_trl(NULL);
					    $newExtra -> setRelationship_ext($extra['selRelationship']);
					    $newExtra -> setFee_ext($extra['ownPrice']);
					    $newExtra -> setCost_ext($extra['ownCost']);
					    $extraId = $newExtra -> insert();
					    if(!$extraId)escapeOnError($db,20, 'extra save', $newExtra);//DB FAIL LOG
				    }
				}
				
				//********************************************** THE PDF CONTRACT *************************************:
				$serial_generated_sale = $serialSale;
			    $display = 0;
				$urlContract = 'contract_'.$serialSale.'.pdf';
				include(DOCUMENT_ROOT.'modules/sales/pPrintSalePDF.php');
				
				//Send contract by mail:
				$misc['subject'] = 'REGISTRO DE NUEVA VENTA';
				$misc['customer'] = $customerH -> getFirstname_cus() . ' ' . $customerH -> getLastname_cus();
				$misc['cardNumber'] = $cardNumber;
				$misc['username'] = $customerH -> getEmail_cus();
				$misc['email'] = $customerH -> getEmail_cus();
				/*GENERAL CONDITIONS FILE*/
				$country_of_sale=Sales::getCountryofSale($db, $serialSale);
				if($country_of_sale!=62){
					$country_of_sale=1;
				}
				$misc['urlGeneralConditions']=$sale->getGeneralConditionsbySale($serialPbd, $serialSale, $country_of_sale, $_SESSION['serial_lang']);
				/*END GENERAL CONDITIONS FILE*/
				$misc['urlContract'] = $urlContract;
				$password = $customerH -> getPassword_cus();
				
				if($password){
					$password = 'Su clave es la misma de la &uacute;ltima vez.';
				}else{
					$password = GlobalFunctions::generatePassword(6, 8, false);
					$customerH -> setPassword_cus($password);
					$customerH -> update();
					
					ERP_logActivity('new', $customerH);//ERP ACTIVITY
				}
			    $misc['pswd'] = $password;
			    
			    if(!GlobalFunctions::sendMail($misc, 'newClient')){
			    	escapeOnError($db,23, 'mail not sent');//DB FAIL LOG
			    }
				
			}//End cards foreach
			//Finally We delete all the cart:
		    PreOrderDetail::deletePOD($db,$prod['serial_pod']);
		}
	}
	//Finally we delete the session information:
	unset($_SESSION['cart_products_travelers_information']);
	unset($_SESSION['ccnum']);
	
	$item = '1';
	if($redirectToFlightsLog){$item = '-1';}
	http_redirect($estimatorSuccessPage.$item);
	
	//************************** FUNCTION DEFINITION *********************************************
	
	function escapeOnError($db,$dbFail, $title = '-', $log = '-',$extra_code_error=NULL){
		global $estimatorErrorPage;
		$id = ErrorLog::log($db, $title, $log);
		//ErrorLog::retrieve($db,$id);
		http_redirect($estimatorErrorPage.$dbFail.'/'.$extra_code_error);
	}
?>