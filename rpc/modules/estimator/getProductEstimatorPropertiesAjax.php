<?php 
/*
File: getProductEstimatorPropertiesAjax.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('serial_pro');
	Request::setString('serial_cou');
	
	if($serial_pro == "-1"){
		$spouseTraveling = json_encode(true);
		
		$serial_cou = $serial_cou;
		if($serial_cou){
		    $product = new Product($db);
		    $products = $product->getProductsByCountryEstimator($serial_cou, $_SESSION['serial_lang']);
		}
		
		if(!$products){
			 $defaultDealer = new Parameter($db,'28');
		     $defaultDealer -> getData();
			 $products = $product->getProductsByCountryEstimator($defaultDealer -> getValue_par(), $_SESSION['serial_lang']);
		}
		
		if($products){
		    foreach($products as &$pl){
		        $pl['name_pbl'] = utf8_encode($pl['name_pbl']);
		    }
		}
		
		$showDays = json_encode(false);
		foreach ($products as $prod){
			$prod = new Product($db, $prod['serial_pro']);
			$prod -> getData();
			$prod = get_object_vars($prod);
			if($prod['show_price_pro']=='YES'){
				$showDays = json_encode(true);
				break;
			}
		}
		
		$maxOverAge = 0;
		$maxUnderAge = 0;
		$localFlightOnly = 'NO';
		$extrasRestricted = 'BOTH';
	}else{
		$productX = new Product($db,$serial_pro);
		$productX -> getData();
		$spouseTraveling = json_encode($productX -> getSpouse_pro()=='YES');
		$showDays = json_encode($productX -> getShow_price_pro()=='YES');
		$maxOverAge = $productX -> getAdults_pro();
		$maxUnderAge = $productX -> getChildren_pro();
		$localFlightOnly = $productX -> getDestination_restricted_pro();
		$extrasRestricted = $productX -> getExtras_restricted_to_pro();
	}
	echo 	$spouseTraveling.'%S%'.
			$showDays.'%S%'.
			($maxOverAge==NULL?'0':$maxOverAge).'%S%'.
			($maxUnderAge==NULL?'0':$maxUnderAge).'%S%'.
			($localFlightOnly=='YES'?'1':$localFlightOnly).'%S%'.
			($extrasRestricted==NULL?'BOTH':$extrasRestricted);
?>