<?php
/*
File: index.php
Author: Santiago Borja
Creation Date: 26/02/2010
LastModified: 01/03/2010
Modified By: Santiago Borja
*/
	
	if(!Counter::getDeparturePhoneSalesCounter($db, $_SESSION['serial_usr'], 1)){
		http_redirect('main/estimator/2');
	}
	
	unset($_SESSION['serial_customer_estimator']);
	
	Request::setString('0:goTo');
	
	//ESTIMATOR:
	$countries = Country::getAllCountries($db,false);
	$cartNotEmpty = isset($_SESSION['system_estimator_shopping_cart_products'])?true:false;
		
	$smarty -> register('countries,adultMinAge,cartNotEmpty,goTo');
	$smarty->assign('container','default_estimator');
	$smarty->display();
?>