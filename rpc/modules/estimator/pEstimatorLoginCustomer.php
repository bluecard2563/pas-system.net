<?php
	if(!$_POST['serialCus']){
		$customer=new Customer($db);
		
		$customer->setSerial_cit($_POST['selCityCustomer']);
		$customer->setType_cus('PERSON');
		$customer->setDocument_cus($_POST['txtDocumentCustomer']);
		$customer->setFirstname_cus($_POST['txtNameCustomer']);
		$customer->setLastname_cus($_POST['txtLastnameCustomer']);
		$customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
		$customer->setPhone1_cus($_POST['txtPhone1Customer']);
		$customer->setPhone2_cus($_POST['txtPhone2Customer']);
		$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
		$customer->setEmail_cus($_POST['txtMailCustomer']);
		$customer->setAddress_cus($_POST['txtAddress']);
		$customer->setRelative_cus($_POST['txtRelative']);
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
		$customer->setStatus_cus('ACTIVE');
		$password = GlobalFunctions::generatePassword(6, 8, false);
    	$customer->setPassword_cus(md5($password));
		
		if($cusId = $customer->insert()){
			ERP_logActivity('new', $customer);//ERP ACTIVITY
			
			$misc['customer']=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
			$misc['username']=$customer->getEmail_cus();
		    $misc['pswd'] = $password;
		    $misc['email']=$customer->getEmail_cus();
		    
		    if(GlobalFunctions::sendMail($misc, 'newClient')){
				//WE LOGIN THE CUSTOMER AND GO TO THE CUSTOMER HOME PAGE
				$_SESSION['email_customer_web'] = $customer -> getEmail_cus();
				$_SESSION['document_customer_web'] = $customer -> getDocument_cus();
				$_SESSION['serial_customer_web'] = $cusId;
				$_SESSION['name_customer_web'] = $customer -> getFirstname_cus().' '.$customer -> getLastname_cus();
			}else{
				http_redirect('modules/estimator/fEstimatorCustomerLogin/2'); //Created but no email sent
		    }
		}else{
			http_redirect('modules/estimator/fEstimatorCustomerLogin/3'); //Error while creating
		}
		$serialCus = $cusId;
	}else{
		$serialCus = $_POST['serialCus'];
	}
	
	$_SESSION['serial_customer_estimator'] = $serialCus;
	$tempCus = new Customer($db, $serialCus);
	$tempCus -> getData();
	$_SESSION['name_customer_estimator'] = $tempCus -> getFirstname_cus().' '.$tempCus -> getLastname_cus();
	http_redirect('modules/estimator/parseSessionCartToDB');
?>