<?php 
/*
File: sendCartToSessionAjax.php
Author: Santiago Borja
Creation Date: 28/03/2010
*/
	Request::setString('cart_prods');
	
	if($cart_prods != 'false'){
		$cartProducts = array();
		$rebuiltList = array();
		$productIdList = explode(',',$cart_prods);
		
		//WE FIRST REBUILD THE LIST WE HAD AT BENEFITS SINCE WE ARE DEALING WITH TWO ACTUAL LISTS,
		//AND ONLY AFTER THAT, WE CAN GET THE SELECTED PRODUCTS BASED ON THEIR INDEX (NOT SERIAL!)
		
		if(isset($_SESSION['system_estimator_shopping_cart_products'])){
			$cartProductsBefore = $_SESSION['system_estimator_shopping_cart_products'];
			foreach ($cartProductsBefore as $prod){
				$rebuiltList[] = $prod;
			}
		}
		
		if(isset($_SESSION['web_estimator_session_products'])){
			$sessionProducts = $_SESSION['web_estimator_session_products'];
			foreach ($sessionProducts as $prod){
				$rebuiltList[] = $prod;
			}
		}
		
		foreach ($productIdList as $prodId){
			$cartProducts[] = $rebuiltList[$prodId];
		}
		
		unset($_SESSION['system_estimator_shopping_cart_products']);
		$_SESSION['system_estimator_shopping_cart_products'] = $cartProducts;
	}else{
		unset($_SESSION['system_estimator_shopping_cart_products']);
	}
	
	unset($_SESSION['web_estimator_session_products']);
	
	echo '<pre>';print_r($_SESSION);echo '</pre>';
	
	echo 'ok';
?>