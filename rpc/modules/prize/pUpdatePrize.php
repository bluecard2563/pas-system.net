<?php 
/*
File: pUpdatePrize.php
Author: Nicolas Flores.
Creation Date: 05/01/2010 18:18
Last Modified:
Modified By:
*/

Request::setInteger('hdnPrizeID');

$prize=new Prize($db,$hdnPrizeID);
$statusList=$prize->getStatusValues();
$coverageList=$prize->getCoverageValues();

$zone= new  Zone($db);
$zoneList=$zone->getZones();

$country=new Country($db);
$countryList=$country->getCountry();

$product=new Product($db);
$productList=$product->getProducts($_SESSION['language']);

if($prize->getData()){
	$prize->setName_pri($_POST['txtName']);
	$prize->setCost_pri($_POST['txtValue']);
	$prize->setStock_pri($_POST['txtStock']);
	$prize->setStatus_pri($_POST['selStatus']);
	$prize->setType_pri($_POST['hdnTypePri']);
        if($_POST['hdnTypePri']=='BONUS'){
            $prize->setSerial_cou($_POST['hdnSerialCou']);
        }else{
            $prize->setSerial_cou(null);
        }
        if($_POST['hdnTypePri']=='BONUS'){
            $prize->setAmount_pri($_POST['hdnAmount']);
        }else{
            $prize->setAmount_pri(null);
        }
        if($_POST['hdnTypePri']=='BONUS'){
            $prize->setCoverage_pri($_POST['hdnCoverage']);
        }else{
            $prize->setCoverage_pri(null);
        }
        if($_POST['chkProduct']){
            $prize->setSerial_pro($_POST['selProduct']);
        }else{
            $prize->setSerial_pro(null);
        }
	
	if($prize->update()){
		http_redirect('modules/prize/fSearchPrize/2');
	}else{
		$error=1;
		$data['name_pri']=$prize->getName_pri();
                $data['value_pri']=$prize->getCost_pri();
                $data['stock_pri']=$prize->getStock_pri();
                $data['status_pri']=$prize->getStatus_pri();
                $data['type_pri']=$prize->getType_pri();
                $data['serial_cou']=$prize->getSerial_cou();
                $data['serial_pro']=$prize->getSerial_pro();
                $data['coverage_pri']=$prize->getCoverage_pri();
                $data['amount_pri']=$prize->getAmount_pri();
                $auxData=$prize->getAuxData();
                $data['serial_zon']=$auxData['serial_zon'];
                $data['serial_pri']=$hdnPrizeID;
		$smarty->register('error');
	}
}

$smarty->register('data,statusList');
$smarty->display('modules/prize/fUpdatePrize.'.$_SESSION['language'].'.tpl');
?>