<?php
/*
File: fNewPrize.tpl
Author: Nicolas Flores
Creation Date: 04/01/2010
Last Modified: 15/04/2010
Modified by: Santiago Benitez
*/
Request::setInteger('0:error');

$prize=new Prize($db);
$typeList=$prize->getTypeValues();
$coverageList=$prize->getCoverageValues();

$zone= new  Zone($db);
$zoneList=$zone->getZones();

$product=new Product($db);
$productList=$product->getProducts($_SESSION['language']);

$smarty->register('error,typeList,zoneList,productList,coverageList,global_prizeCoverages');
$smarty->display();
?>
