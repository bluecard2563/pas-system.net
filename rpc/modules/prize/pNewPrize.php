<?php
/*
File: fNewPrize.tpl
Author: Nicolas Flores
Creation Date: 05/01/2010
Last Modified: 12/01/2010 09:25
Modified by: Santiago Ben�tez
*/
	$prize=new Prize($db);
	$prize->setName_pri($_POST['txtName']);
	$prize->setCost_pri($_POST['txtValue']);
	$prize->setStock_pri($_POST['txtStock']);
	$prize->setType_pri($_POST['selType']);
        if($_POST['selCountry']){
            $prize->setSerial_cou($_POST['selCountry']);
        }
        if($_POST['txtAmount']){
            $prize->setAmount_pri($_POST['txtAmount']);
        }
        if($_POST['selCoverage']){
            $prize->setCoverage_pri($_POST['selCoverage']);
        }
        if($_POST['chkProduct']){
            $prize->setSerial_pro($_POST['selProduct']);
        }
//Debug::print_r($user);	

	if($prize->insert()){//Insert was correct
		$error=1;
	}else{//There was an error
		$error=2;
	}
	
http_redirect('modules/prize/fNewPrize/'.$error);


?>