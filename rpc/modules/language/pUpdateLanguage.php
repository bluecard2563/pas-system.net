<?php
/*
File: pUpdateLanguage.php
Author: Santiago Benítez
Creation Date: 20/01/2010 15:00
Last Modified: 20/01/2010 15:00
Modified By: Santiago Benítez
*/

$language = new Language($db);
$data['serial_lang'] = $_POST['hdnSerial_lang'];    
$language -> setSerial_lang($data['serial_lang']);
$data['name_lang']= $_POST['txtNameLanguage']; 
$data['code_lang']= $_POST['txtCodeLanguage']; 
if($language -> getData()){
	$language -> setName_lang($data['name_lang']);
	$language -> setCode_lang($data['code_lang']);
	if($language -> update()){
		http_redirect('modules/language/fSearchLanguage/1');//succes in updating language
	}else{
		$error=1;
		$smarty->register('error');
	}
}
$smarty->register('data');
$smarty->display('modules/language/fUpdateLanguage.'.$_SESSION['language'].'.tpl');

?>
