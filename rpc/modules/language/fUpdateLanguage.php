<?php
/*
File: fUpdateLanguage.php
Author: Santiago Benítez
Creation Date: 20/01/2010 14:43
Last Modified: 20/01/2010 14:43
Modified By: Santiago Benítez
*/

Request::setString('serial_lang');
$language = new Language($db);
$language->setSerial_lang($serial_lang);
	
if($language->getData()){	
	$data['serial_lang'] = $language -> getSerial_lang();
	$data['name_lang'] = $language -> getName_lang();
	$data['code_lang'] = $language -> getCode_lang();
	//Debug::print_r($data);
}else{
	http_redirect('modules/language/fSearchLanguage/2');
}

$smarty -> register('data');
$smarty -> display();

?>