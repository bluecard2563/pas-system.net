<?php 
/*
File: pAssignCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 9:20
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

$counter=new Counter($db);
$counter->setSerial_dea($_POST['selBranch']);
$counter->setSerial_usr($_POST['selUser']);
$counter->setStatus_cnt('ACTIVE');

if($counter->insert()){
	http_redirect('modules/counter/fAssignCounter/1');
}else{
	http_redirect('modules/counter/fAssignCounter/2');
}
?>