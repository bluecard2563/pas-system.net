<?php 
/*
File: fSearchCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 14:23
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('0:error');

$country=new Country($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);

$smarty->register('error,countryList');
$smarty->display();
?>