<?php 
/*
File: fUpdateCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 15:27
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('0:error');
Request::setInteger('hdnCounterID');
$counter=new Counter($db,$hdnCounterID);

if($counter->getData()){
    $auxData=$counter->getAux_cnt();
    $data['serial_cnt']=$hdnCounterID;
    $data['status_cnt']=$counter->getStatus_cnt();
    $data['dealer']=$auxData['dealer'];
    $data['branch']=$auxData['branch'];
    $data['counter']=$auxData['counter'];

    $statusList=$counter->getStatusTypes();
    $smarty->register('data,statusList');
}

$smarty->register('error');
$smarty->display();
?>