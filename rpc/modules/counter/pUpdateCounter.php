<?php 
/*
File: pNewCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 9:20
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

$counter=new Counter($db,$_POST['hdnCounterID']);

if($counter->getData()){
    $counter->setStatus_cnt($_POST['selStatus']);

    if($counter->update()){
            http_redirect('modules/counter/fSearchCounter/1');
    }
}

$counter->getData();
$auxData=$counter->getAux_cnt();
$data['serial_cnt']=$hdnCounterID;
$data['status_cnt']=$counter->getStatus_cnt();
$data['dealer']=$auxData['dealer'];
$data['branch']=$auxData['branch'];
$data['counter']=$auxData['counter'];
$error=1;

$statusList=$counter->getStatusTypes();
$smarty->register('data,statusList,error');
$smarty->display('modules/counter/fUpdateCounter.'.$_SESSION['language'].'.tpl');
?>