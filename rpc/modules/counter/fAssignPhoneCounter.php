<?php
/*
File: fAssignPhoneCounter.php
Author: David Bergmann
Creation Date: 15/07/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$countryList = Country::getPhoneSalesCountries($db);
$managerList = ManagerbyCountry::getOfficialManagersByCountry($db);

$smarty->register('error,countryList,managerList');
$smarty->display();
?>
