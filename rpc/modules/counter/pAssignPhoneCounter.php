<?php
/*
File: pAssignPhoneCounter
Author: David Bergmann
Creation Date: 15/07/2010
Last Modified:
Modified By:
*/

$counter=new Counter($db);
$counter->setSerial_dea($_POST['selCountryBranch']);
$counter->setSerial_usr($_POST['selUser']);
$counter->setStatus_cnt('ACTIVE');

if($counter->insert()){
	$error = 1;
	if(!Counter::userExistsInPhoneSalesMainCounter($db, $_POST['selUser'])) {
		$counter->setSerial_dea($phoneSalesBranch);
		if(!$counter->insert()){
			$error = 3;
		}
	}
} else {
	$error = 2;
	http_redirect('modules/counter/fAssignPhoneCounter/2');
}

http_redirect('modules/counter/fAssignPhoneCounter/'.$error);
?>