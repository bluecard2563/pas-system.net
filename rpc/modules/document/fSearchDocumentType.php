<?php
/*
File: fSearchDocumentType.php
Author: Santiago Benítez
Creation Date: 11/01/2010
Last Modified: 11/01/2010 15:13
Modified By: Santiago Benítez
*/

Request::setInteger('0:message');
$doc=new DocumentType($db);
$documentTypeList=$doc->getDocumentTypes();

$smarty -> register('message,documentTypeList');
$smarty -> display();				
?>
