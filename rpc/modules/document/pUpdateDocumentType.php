<?php
/*
File: pUpdateDocumentType.php
Author: Santiago Benítez
Creation Date: 11/01/2010 
Last Modified: 16/02/2010
Modified By: Edwin Salvador
*/

$documentType = new DocumentType($db);
$data['serial_doc'] = $_POST['hdnSerial_doc'];    
$documentType -> setSerial_doc($data['serial_doc']);
$data['name_doc'] = $_POST['txtNameDocumentType'];

$assigned_managers = $_POST['managersCount'];
if($documentType -> getData()){
    $documentType -> setName_doc($data['name_doc']);

    if($documentType -> update()){
        $dbm = new DocumentByManager($db);
        $dbm->setSerial_doc($data['serial_doc']);
        if($dbm->delete()){
			$band = 0;
            for($i=0; $i<=$assigned_managers; $i++){
                if($_POST['is_used'][$i] == 0){
				
                    if($_POST['selManager'.$i] != '' && $_POST['selType'.$i]!= ''){
                        $dbm->setSerial_man($_POST['selManager'.$i]);
                        $dbm->setSerial_doc($data['serial_doc']);
                        $dbm->setType_dbm($_POST['selType'.$i]);
						$dbm->setPurpose_dbm($_POST['selPurpose'.$i]);
						$dbm->setNumber_dbm(1);

                        if(!$dbm->insert()){
							$band = 1;
						}
                    }
                }
            }
			if( $band == 0 ){
				$error = 1;
			}else{
				$error = 2;
			}
        }else{
            $error = 2;
        }
    }else{
        $error=2;
    }
}else{
    $error =2;
}
http_redirect('modules/document/fSearchDocumentType/'.$error);//error in updating document type
/*
$smarty->register('data');
$smarty->display('modules/document/fUpdateDocumentType.'.$_SESSION['language'].'.tpl');
*/
?>
