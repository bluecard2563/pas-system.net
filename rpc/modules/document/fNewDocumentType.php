<?php 
/*
File: fNewDocumentType.php
Author: Santiago Benitez
Creation Date: 11/01/2010 
Last Modified: 12/02/2010
Modified By: Edwin Salvador
*/
Request::setInteger('0:error');
$zone= new  Zone($db);
$zonesList=$zone->getZones();

$purposeValues=DocumentByManager::getPurposeValues_dbm($db);

$smarty->register('error,zonesList,purposeValues');
$smarty->display();
?>