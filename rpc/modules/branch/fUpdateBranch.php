<?php 
/*
File: fUpdateBranch.php
Author: David Bergmann
Creation Date: 14/01/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$serial_dea=$_POST['selBranch'];
$dealer=new Dealer($db,$serial_dea);

/*RETRIEVING DEFAULT DATA*/
//Language
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//Dealer Types
$dealerT=new DealerType($db);
$dTypesList=$dealerT->getDealerTypes(NULL,$language->getSerial_lang());

//CATEGORY
$categoryList=$dealer->getCategories();

//STATUS
$statusList=$dealer->getStatusList();

//PAYMENT DEDLINE
$deadlineList=$dealer->getDedline();

//DISCOUNT/COMISSION
$typeList=$dealer->getPercentageType();
/*END DEFAULT DATA*/


if($dealer->getData()){
	//BRANCH DATA
	$data['serial_dea']=$serial_dea;
	$data['serial_sec']=$dealer->getSerial_sec();
	$data['serial_mbc']=$dealer->getSerial_mbc();
	$data['serial_cdd']=$dealer->getSerial_cdd();
	$data['serial_cdd']=$dealer->getSerial_cdd();
	$data['serial_dlt']=$dealer->getSerial_dlt();
	$data['status_dea']=$dealer->getStatus_dea();
	$data['id_dea']=$dealer->getId_dea();
	$data['code_dea']=$dealer->getCode_dea();
	$data['official_dea']= $dealer->getOfficial_seller_dea();
	$data['name_dea']=$dealer->getName_dea();
	$data['category_dea']=$dealer->getCategory_dea();
	$data['address_dea']=$dealer->getAddress_dea();
	$data['phone1_dea']=$dealer->getPhone1_dea();
	$data['phone2_dea']=$dealer->getPhone2_dea();
	$data['contract_date']=$dealer->getContract_date_dea();
	$data['fax_dea']=$dealer->getFax_dea();
	$data['email1_dea']=$dealer->getEmail1_dea();
	$data['email2_dea']=$dealer->getEmail2_dea();
	$data['contact_dea']=$dealer->getContact_dea();
	$data['phone_contact']=$dealer->getPhone_contact_dea();
	$data['email_contact']=$dealer->getEmail_contact_dea();
	$data['visits_number']=$dealer->getVisits_number_dea();
	$data['percentage_dea']=$dealer->getPercentage_dea();
	$data['type_dea']=$dealer->getType_percentage_dea();
	$data['payment_dedline']=$dealer->getPayment_deadline_dea();
	$data['bill_to']=$dealer->getBill_to_dea();
	$data['dea_serial_dea']=$dealer->getDea_serial_dea();
	$data['pay_contact']=$dealer->getPay_contact_dea();
	$data['assistance_contact']=$dealer->getAssistance_contact_dea();
	$data['email_assistance']=$dealer->getEmail_assistance_dea();
	$data['branch_number']=$dealer->getBranch_number_dea();
    $data['bonus_to_dea']=$dealer->getBonus_to_dea();
	$data['manager_name'] = $dealer->getManager_name_dea();
	$data['manager_phone'] = $dealer->getManager_phone_dea();
	$data['manager_email'] = $dealer->getManager_email_dea();
	$data['manager_birthday'] = $dealer->getManager_birthday_dea();
	$data['aux']=$dealer->getAuxData_dea();
	
	$creditD=new CreditDay($db);
	$creditDList=$creditD->getCreditDays($data['aux']['serial_cou']);

        //MANAGER DATA
        $user_by_dealer = new UserByDealer($db);
        $user_by_dealer->setSerial_dea($serial_dea);
        $user_by_dealer->getData();
        $data['serial_usr']=$user_by_dealer->getSerial_usr();


	//DEALER DATA
	$parentDealer = new Dealer($db,$dealer->getDea_serial_dea());
	$parentDealer->getData();

	$data['dea_serial_dea']=$parentDealer->getSerial_dea();
	$data['dea_name_dea']=$parentDealer->getName_dea();
	$data['dea_serial_sec']=$parentDealer->getSerial_sec();
        $data['dea_code_dea']=$parentDealer->getCode_dea();
	$data['dea_aux']=$parentDealer->getAuxData_dea();
	
	$zone=new Zone($db,$_POST['selZone']);
	$zone->getData();
	$data['name_zon']=$zone->getName_zon();

	$country=new Country($db,$data['dea_aux']['serial_cou']);
	$country->getData();
	$data['dea_name_cou']=$country->getName_cou();
	
	$city=new City($db,$data['dea_aux']['serial_cit']);
	$city->getData();
	$data['dea_name_cit']=$city->getName_cit();
	
	$sector=new Sector($db,$data['dea_serial_sec']);
	$sector->getData();
	$data['dea_name_sec']=$sector->getName_sec();

	$country=new Country($db,$data['aux']['serial_cou']);
	$country->getData();
	$data['name_cou']=$country->getName_cou();
        $data['code_cou']=$country->getCode_cou();

	$city=new City($db,$data['aux']['serial_cit']);
	$city->getData();
	$data['name_cit']=$city->getName_cit();
        $data['code_cit']=$city->getCode_cit();
	//SECTORS
	$sector=new Sector($db);
	$sectorList=$sector->getSectorsByCity($data['aux']['serial_cit']);

	//RESPONSIBLES
	$user = new User($db);
	if($_POST['selManager']) {
		$comissionistList=$user->getUsersByManager($data['serial_mbc']);
	}
	//PLANETASSIST USERS
	$mainComissionistList=$user->getMainManagerUsers();
	
	$user = new User($db, $_SESSION['serial_usr']);
	$paUser = !$user->isPlanetAssistUser();
	$serial_mbc=$_POST['selManager'];
	$smarty->register('data,sectorList,creditDList,comissionistList,mainComissionistList,serial_mbc,paUser');
}
else {
	http_redirect('modules/branch/fSearchBranch/2');
}

$today=date('d/m/Y');

$smarty->register('today');
$smarty->register('error,dTypesList,statusList,deadlineList,categoryList,typeList,sectorList');
$smarty->display();
?>