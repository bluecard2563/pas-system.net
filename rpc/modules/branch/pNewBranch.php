<?php 
/*
File: pNewBranch.php
Author: David Bergmann
Creation Date:14/01/2010
Last Modified: 16/02/2010
Modified By: David Bergmann
*/

$dealer=new Dealer($db);
$parentDealer=new Dealer($db,$_POST['hdnSerialDealer']);
$parentDealer->getData();
$user_by_dealer = new UserByDealer($db);
$user_by_dealer->setSerial_dea($_POST['hdnSerialDealer']);
$user_by_dealer->getData();
$serial_usr = $user_by_dealer->getSerial_usr();
$dealer->setSerial_sec($_POST['selSector']);
$dealer->setSerial_mbc($parentDealer->getSerial_mbc());
$dealer->setSerial_cdd($_POST['selCreditD']);
$dealer->setSerial_dlt($_POST['selDealerT']);
$dealer->setId_dea($_POST['txtID']);

//Dealer Code
$dealerNumber = new DealerNumberByCountry($db);
$dealerNumber->setSerial_cou($_POST['selCountry']);

if($dealerNumber->getLastNumberByCountry()) {
    $dealerNumber->setNumber_dnc($dealerNumber->getLastNumberByCountry()+1);
    if(!$dealerNumber->update()) {
        http_redirect('modules/dealer/fNewDealer/'.$error);
    }
} else {
    $dealerNumber->setNumber_dnc(1);
    if(!$dealerNumber->insert()) {
        http_redirect('modules/dealer/fNewDealer/'.$error);
    }
}

$dealer->setCode_dea($dealerNumber->getNumber_dnc());
$dealer->setName_dea($_POST['txtName']);
$dealer->setCategory_dea($_POST['selCategory']);
$dealer->setAddress_dea($_POST['txtAddress']);
$dealer->setPhone1_dea($_POST['txtPhone1']);
$dealer->setPhone2_dea($_POST['txtPhone2']);
$dealer->setContract_date_dea($_POST['txtContractDate']);
$dealer->setFax_dea($_POST['txtFax']);
$dealer->setEmail1_dea($_POST['txtMail1']);
$dealer->setEmail2_dea($_POST['txtMail2']);
$dealer->setContact_dea($_POST['txtContact']);
$dealer->setPhone_contact_dea($_POST['txtContactPhone']);
$dealer->setEmail_contact_dea($_POST['txtContactMail']);
$dealer->setVisits_number_dea($_POST['txtVisitAmount']);
$dealer->setPercentage_dea($_POST['txtPercentage']);
$dealer->setType_percentage_dea($_POST['selType']);
$dealer->setStatus_dea($_POST['selStatus']);
$dealer->setPayment_deadline_dea($_POST['selPayment']);
$dealer->setBill_to_dea($_POST['chkBillTo']);
$dealer->setDea_serial_dea($_POST['hdnSerialDealer']);
$dealer->setPay_contact_dea($_POST['txtContactPay']);
$dealer->setAssistance_contact_dea($_POST['txtAssistName']);
$dealer->setEmail_assistance_dea($_POST['txtAssistEmail']);
$dealer->setBranch_number_dea($_POST['hdnBranchNumber']);
$dealer->setBonus_to_dea($_POST['chkBonusTo']);
$dealer->setManager_name_dea($_POST['txtNameManager']);
$dealer->setManager_phone_dea($_POST['txtPhoneManager']);
$dealer->setManager_email_dea($_POST['txtMailManager']);
$dealer->setManager_birthday_dea($_POST['txtBirthdayManager']);
$dealer->setOfficial_seller_dea("NO");

/*E-MAIL INFO*/
$parentDealer=new Dealer($db,$_POST['hdnSerialDealer']);
$parentDealer->getData();
$misc['parent_dealer'] = $parentDealer->getName_dea();
$user_by_dealer = new UserByDealer($db);
$user_by_dealer->setSerial_dea($parentDealer->getSerial_dea());
$user_by_dealer->getData();
$user = new User($db, $user_by_dealer->getSerial_usr());
$user->getData();
$misc['comissionist'] = $user->getFirstname_usr().' '.$user->getLastname_usr();
$misc['sendToComissionist'] = $user->getEmail_usr();

$sector = new Sector($db,$dealer->getSerial_sec());
$sector->getData();
$misc['sector'] = $sector->getName_sec();
$city = new City($db, $sector->getSerial_cit());
$city->getData();
$misc['city'] = $city->getName_cit();
$country = new Country($db, $city->getSerial_cou());
$country->getData();
$misc['country'] = $country->getName_cou();

$mbc = new ManagerbyCountry($db,$dealer->getSerial_mbc());
$mbc->getData();
$manager = new Manager($db,$mbc->getSerial_man());
$manager->getData();
$misc['manager'] = $manager->getName_man();
$misc['sendToManager'] = $manager->getContact_email_man();
$misc['official'] = $dealer->getOfficial_seller_dea();
$misc['id'] = $dealer->getId_dea();
$misc['name'] = $dealer->getName_dea();
$dlt = new DealerType($db,$dealer->getSerial_dlt());
$dlt->getData();
$misc['type'] = $dlt->getName_dlt();
$misc['category'] = $dealer->getCategory_dea();
$misc['address'] = $dealer->getAddress_dea();
$misc['phone1'] = $dealer->getPhone1_dea();
$misc['phone2'] = $dealer->getPhone2_dea();
$misc['fax'] = $dealer->getFax_dea();
$misc['contract_date'] = $dealer->getContract_date_dea();
$misc['email1'] = $dealer->getEmail1_dea();
$misc['email2'] = $dealer->getEmail2_dea();
$misc['contact'] = $dealer->getContact_dea();
$misc['phone_contact'] = $dealer->getPhone_contact_dea();
$misc['email_contact'] = $dealer->getEmail_contact_dea();
$misc['pay_contact'] = $dealer->getPay_contact_dea();
if($dealer->getBill_to_dea()=='DEALER') {
    $misc['bill_to'] = 'Comercializador';
} else {
    $misc['bonus_to'] = 'Cliente';
}
$misc['payment_deadline'] = $dealer->getPayment_deadline_dea();
$creditDay = new CreditDay($db, $dealer->getSerial_cdd());
$creditDay->getData();
$misc['credit_day'] = $creditDay->getDays_cdd();
$misc['percentage'] = $dealer->getPercentage_dea();
$misc['percentage_type'] = $dealer->getType_percentage_dea();
if($dealer->getBonus_to_dea()=='DEALER') {
    $misc['bonus_to'] = 'Comercializador';
} else {
    $misc['bonus_to'] = 'Counter';
}
$misc['assistance'] = $dealer->getAssistance_contact_dea();
$misc['email_assistance'] = $dealer->getEmail_assistance_dea();

$misc['manager_name'] = $dealer->getManager_name_dea();
$misc['manager_phone'] = $dealer->getManager_phone_dea();
$misc['manager_email'] = $dealer->getManager_email_dea();
$misc['manager_birthday'] = $dealer->getManager_birthday_dea();

$misc['branch_code'] =$country->getCode_cou()."-".$city->getCode_cit()."-".$parentDealer->getCode_dea()."-".$dealer->getCode_dea();

$misc['textForEmail']="Se ha ingresado una nueva surcursal del comercializador <b>".$misc['parent_dealer']."</b> cod : <u>".$misc['branch_code']."</u><br><br>";
$misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
/*END E-MAIL INFO*/

if($serialBranch = $dealer->insert()){
	
	ERP_logActivity('new', $dealer);//ERP ACTIVITY
	$user = new User($db, $_POST['hdnSerialUsr']);
	$user->getData();
    $user_by_dealer->setSerial_usr($_POST['hdnSerialUsr']);
    $user_by_dealer->setSerial_dea($db->insert_ID());
    $user_by_dealer->setStatus_ubd("ACTIVE");
	$user_by_dealer->setPercentage_ubd($user->getCommission_percentage_usr());

    if($user_by_dealer->insert()){
        if(GlobalFunctions::sendMail($misc, 'newBranch')){
            http_redirect('modules/branch/fNewBranch/1/' . $serialBranch);
        } else {
            http_redirect('modules/branch/fSearchBranch/4');
        }

    } else {
        http_redirect('modules/branch/fNewBranch/3');
    }
    

}else{
    http_redirect('modules/branch/fNewBranch/2');
}
?>