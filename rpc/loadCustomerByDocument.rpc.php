<?php
/*
File: loadCustomerByDocument.rpc
Author: David Bergmann
Creation Date: 07/04/2010
Last Modified:
Modified By:
*/

$customer = new Customer($db);
$document_cus = $_POST['document_cus'];
$serial_cus = $customer->getCustomerSerialbyDocument($document_cus);

if($serial_cus){
    //Customer information
    $customer->setserial_cus($serial_cus);
    $customer->getData();
    $city = new City($db,$customer->getSerial_cit());
    $city->getData();
    $data['name_cit']=$city->getName_cit();
    $country = new Country($db, $city->getSerial_cou());
    $country->getData();
    $data['name_cou']=$country->getName_cou();
    $data['document_cus']=$customer->getDocument_cus();
    $data['first_name_cus']=$customer->getFirstname_cus();
    $data['last_name_cus']=$customer->getLastname_cus();
    $data['birthdate_cus']=$customer->getBirthdate_cus();
    $data['phone1_cus']=$customer->getPhone1_cus();
    $data['cellphone_cus']=$customer->getCellphone_cus();
    $data['email_cus']=$customer->getEmail_cus();
    $data['address_cus']=$customer->getAddress_cus();

    $titles = array("Pa&iacute;s","Ciudad","Documento","Nombre","Fecha de Nacimiento",
                "Tel&eacute;fono","Celular","Correo Electr&oacute;nico","Direcci&oacute;n", "Tarjetas");
    $titles_sal = array("Tarjeta","Fecha Emisi&oacute;n","Fecha Inicio","Fecha Fin","Estado de la tarjeta");
    
    //Cards information
    $sales = $customer->loadSalesByCustomer();    
    $smarty->register('titles,data,titles_sal,sales,global_salesStatus');
}

$smarty->register("document_cus");
$smarty->assign('container','none');
$smarty->display('helpers/loadCustomerByDocument.'.$_SESSION['language'].'.tpl');
?>
