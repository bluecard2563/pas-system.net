<?php
/*
File: loadPaymentCities.php
Author: Santiago Benitez
Creation Date: 19/04/2010 15:29
Last Modified: 19/04/2010
Modified By: Santiago Benitez
*/

$city=new City($db);
if($_POST['serial_cou']){
	$cityList=$city->getPaymentCitiesByCountry($_POST['serial_cou'],$_SESSION['serial_mbc'],$_SESSION['cityList']);
}

$smarty->register('cityList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadCities.'.$_SESSION['language'].'.tpl');
?>