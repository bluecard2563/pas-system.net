<?php
/*
File: loadPaymentBranches.rpc.php
Author: Santiago Benitez
Creation Date: 19/04/2010
LastModified:
Modified By:
*/
Request::setInteger('dea_serial_dea');
$dealer=new Dealer($db);
//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$branchList=$dealer->getInvoiceBranches($dea_serial_dea, $_SESSION['serial_dea']);
} else {
	$branchList=$dealer->getInvoiceBranches($dea_serial_dea);
}
$smarty->register('branchList');
$smarty->assign('container','none');
$smarty->display('helpers/loadBranches.'.$_SESSION['language'].'.tpl');
?>
