<?php
/*
File: loadPayPhoneInvoicesForApprByBranch.rpc
Author: David Rosales
Creation Date: 20/09/2016
Last Modified: 
Modified By:
*/

Request::setInteger('serial_dea');

$branch = new Dealer($db,$serial_dea);
$invoicesList = $branch->getInvoicesByBranch(true);

$transactionIDs = '';
if(is_array($invoicesList)){
	foreach($invoicesList as $invoice){
		$payphone_arr[] = $invoice['serial_inv'].'_'.$invoice['payphone_id']; 
	}
	$transactionIDs = implode('|', $payphone_arr);
}

$titles = array("Fecha de Factura","# de Factura","Facturado a","Total a Pagar");
	
$smarty -> register('invoicesList,titles,transactionIDs'); 
$smarty->assign('container','none');
$smarty->display('helpers/payments/payphonePayments/loadPayPhoneInvoicesForApprByBranch.es.tpl');
