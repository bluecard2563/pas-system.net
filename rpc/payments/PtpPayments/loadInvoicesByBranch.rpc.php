<?php
/*
File: loadInvoicesByBranch.rpc
Author: David Bergmann
Creation Date: 04/08/2010
Last Modified: 02/09/2016
Modified By: David Rosales
*/

Request::setInteger('serial_dea');

$payPhoneButton = false;
$convergeButton = false;
 $serial_usr=$_SESSION['serial_usr'];
$branch = new Dealer($db,$serial_dea);
$invoicesList = $branch->getInvoicesByBranch();
$user = new User($db, $_SESSION['serial_usr']);
$user->getData();
$user = get_object_vars($user);
$ptp_use=$user['ptp_use'];
//Debug::print_r($invoicesList);die();
if(is_array($invoicesList)){
    foreach($invoicesList as &$i){
        if($i['days'] < 0 ){
            $i['days']=0;
        }
        if($i['status_pay'] == 'PARTIAL' || $i['status_pay'] == 'ONLINE_PENDING'){
            $i['number_inv'] = $i['number_inv'].' (A)';
        }elseif($i['status_pay'] == 'VOID'){
            $i['number_inv'] = $i['number_inv'].' (N)';
        }
        if($i['status_inv']=='VOID'){
            $i['number_inv'] = $i['number_inv'].' (CI)';
        }
    }
}

if($ptp_use=="YES"){
    $titles = array("Fecha de Factura","# de Factura","Facturado a","Impago","Hasta 30","Hasta 60","Hasta 90","M&aacute;s de 90","Enviar mensaje");
}else{
    $titles = array("Fecha de Factura","# de Factura","Facturado a","Impago","Hasta 30","Hasta 60","Hasta 90","M&aacute;s de 90");
}

if($_SESSION['serial_dea']) { //If it's a dealer's user
	$payPhoneButton = true;
	$convergeButton = false;
} elseif($_SESSION['serial_mbc']) {
	$payPhoneButton = true;
	$convergeButton = true;
}

$smarty->register('invoicesList,titles,serial_dea,payPhoneButton,convergeButton,serial_usr,ptp_use');
$smarty->assign('container','none');
$smarty->display('helpers/payments/PtpPayments/loadInvoicesByBranch.'.$_SESSION['language'].'.tpl');
?>