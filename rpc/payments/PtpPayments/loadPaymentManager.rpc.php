<?php
/*
File: loadPaymentManager.rpc
Author: David Bergmann
Creation Date: 04/08/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');

if($serial_cou) {
    $managerByCountry=new ManagerbyCountry($db);
    $managerByCountryList=$managerByCountry->getPaymentManagerByCountry($serial_cou);
}

$smarty->register('managerByCountryList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/payments/PtpPayments/loadPaymentManager.'.$_SESSION['language'].'.tpl');
?>