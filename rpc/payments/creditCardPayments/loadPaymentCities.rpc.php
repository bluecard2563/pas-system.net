<?php
/*
File: loadPaymentCities.rpc
Author: David Bergmann
Creation Date: 04/08/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cou');
Request::setString('serial_mbc');

if($serial_cou && $serial_mbc) {
    $city = new City($db);
    $cityList = $city->getPaymentCitiesByCountry($serial_cou,$serial_mbc,$_SESSION['cityList']);
}
$smarty->register('cityList,serial_cou,serial_mbc');
$smarty->assign('container','none');
$smarty->display('helpers/payments/creditCardPayments/loadPaymentCities.'.$_SESSION['language'].'.tpl');
?>