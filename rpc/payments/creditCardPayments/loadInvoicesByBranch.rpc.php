<?php
/*
File:loadInvoicesByBranch.rpc.php
Author: Edwin Salvador
Creation Date:11/03/2010
Modified By:
Last Modified: 
*/
Request::setInteger('serial_dea');
if(!$serial_dea){
	Request::setInteger('0:serial_dea');
	$print = 1;
}
//Request::setInteger('0:print');


$branch = new Dealer($db,$serial_dea);
$invoicesList = $branch->getInvoicesByBranch();

$total_days = array();
$total_days['total_30'] = '0.00';
$total_days['total_60'] = '0.00';
$total_days['total_90'] = '0.00';
$total_days['total_more'] = '0.00';
$total_days['total'] = 0;
if(is_array($invoicesList)){
	$total_pages = (int)(count($invoicesList) / 10) + 1;
    foreach($invoicesList as &$i){
        if($i['days'] < 0 ){
            $i['days']=0;
        }
        if($i['days'] <= 30){
            if($i['status_inv']=='STAND-BY'){
                $total_days['total_30'] += $i['total_to_pay'];
            }else{
                $total_days['total_30'] += $i['total_to_pay_fee'];
            }            
        }elseif($i['days'] > 30 && $i['days'] <= 60){
            if($i['status_inv']=='STAND-BY'){
                $total_days['total_60'] += $i['total_to_pay'];
            }else{
                $total_days['total_60'] += $i['total_to_pay_fee'];
            }            
        }elseif($i['days'] > 60 && $i['days'] <= 90){
            if($i['status_inv']=='STAND-BY'){
                $total_days['total_90'] += $i['total_to_pay'];
            }else{
                $total_days['total_90'] += $i['total_to_pay_fee'];
            }            
        }else{
            if($i['status_inv']=='STAND-BY'){
                $total_days['total_more'] += $i['total_to_pay'];
            }else{
                $total_days['total_more'] += $i['total_to_pay_fee'];
            }            
        }
        if($i['status_inv']=='STAND-BY'){
            $total_days['total'] += $i['total_to_pay'];
        }else{
            $total_days['total'] += $i['total_to_pay_fee'];
        }
        if($i['status_pay'] == 'PARTIAL' || $i['status_pay'] == 'ONLINE_PENDING'){
            $i['number_inv'] = $i['number_inv'].' (A)';
        }elseif($i['status_pay'] == 'VOID'){
            $i['number_inv'] = $i['number_inv'].' (N)';
        }
        /*if($i['status_inv']=='VOID'){
            $i['number_inv'] = $i['number_inv'].' (CI)';
        }*/
    }
    $total_days['total_30'] = number_format($total_days['total_30'], 2 , '.', '');
    $total_days['total_60'] = number_format($total_days['total_60'], 2 , '.', '');
    $total_days['total_90'] = number_format($total_days['total_90'], 2 , '.', '');
    $total_days['total_more'] = number_format($total_days['total_more'], 2 , '.', '');
    $total_days['total'] = number_format($total_days['total'], 2 , '.', '');
}
//Debug::print_r($invoicesList);
if(!$print){
  $smarty->register('serial_dea,invoicesList,total_days,total_pages');
  $smarty->assign('container','none');
  $smarty->display('helpers/loadInvoicesByBranch.'.$_SESSION['language'].'.tpl');
}else{
  $_SESSION['serial_dea'] = $serial_dea;
  $_SESSION['invoicesToPrint'] = $invoicesList;
  $_SESSION['total_days'] = $total_days;

//http_redirect('modules/payments/pPrintAccountsTable');
 //header('Location:http://localhost/PLANETASSIST/modules/payments/pPrintAccountsTable');
	$url='http://localhost/PLANETASSIST/modules/payments/pPrintAccountsTable';
	echo '<script type="text/javascript">';
	echo 'window.location.href="'.$url.'";';
	echo '</script>';
}

?>