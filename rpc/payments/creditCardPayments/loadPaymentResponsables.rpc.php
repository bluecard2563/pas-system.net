<?php
/*
File: loadPaymentResponsables.rpc
Author: David Bergmann
Creation Date: 04/08/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
Request::setString('serial_mbc');

if($serial_cit && $serial_mbc) {
    $dealer = new Dealer($db);
    $responsablesList =  $dealer->getPaymentResponsablesByCity($serial_cit, $serial_mbc);
}

$smarty->register('responsablesList,serial_cit');
$smarty->assign('container','none');
$smarty->display('helpers/payments/creditCardPayments/loadPaymentResponsables.'.$_SESSION['language'].'.tpl');
?>