<?php
/*
File: loadPaymentDealers.rpc
Author: David Bergmann
Creation Date: 04/08/2010
Last Modified:
Modified By:
*/

Request::setInteger('serial_usr');
Request::setInteger('serial_cit');
Request::setInteger('serial_mbc');
Request::setInteger('serial_dea');
//die($serial_dea);
if($serial_usr && $serial_cit && $serial_mbc) {
    $dealer=new Dealer($db);
    $dealerList=$dealer->getPaymentDealersByManager($serial_cit,$serial_usr,$serial_mbc);
} elseif($serial_usr && $serial_dea) {
    $dealer=new Dealer($db);
    $dealerList=$dealer->getPaymentBranchesByManager($serial_dea,$serial_usr);
}

$smarty->register('dealerList,serial_usr,serial_cit,serial_mbc,serial_dea');
$smarty->assign('container','none');
$smarty->display('helpers/payments/creditCardPayments/loadPaymentDealers.'.$_SESSION['language'].'.tpl');
?>