<?php
/*
File: loadDealersByManager.rpc.php
Author: Edwin Salvador
Creation Date: 22/02/2010
LastModified:
Modified By:
*/

Request::setString('phone_dealers');
Request::setString('status_dea');

$dealer=new Dealer($db);
if($_POST['serial_mbc']){
    $serial_mbc = $_POST['serial_mbc'];
    $count = $_POST['count'];
    $params=array(
                '0'=>array('field'=> 'd.serial_mbc',
                           'value'=> $serial_mbc,
                           'like'=> 1),
                '1'=>array('field'=> 'd.dea_serial_dea',
                           'value'=> 'NULL',
                           'like'=> 1),
                '2'=>array('field'=> 'dtl.serial_lang',
                           'value'=> $_SESSION['serial_lang'],
                           'like'=> 1)
    );

	if(!$phone_dealers){
		array_push($params,array('field'=>'d.phone_sales_dea',
							   'value'=> 'NO',
							   'like'=> 1));
	}

	if($status_dea){
		array_push($params,array('field'=> 'd.status_dea',
							   'value'=> $status_dea,
							   'like'=> 1));
	}else{
		array_push($params,array('field'=> 'd.status_dea',
							   'value'=> 'ACTIVE',
							   'like'=> 1));
	}

    if($_POST['serial_dealers']){
        array_push($params,array('field'=> 'd.serial_dea',
                                      'value'=> explode(',', $_POST['serial_dealers']),
                                      'like'=> 0));
    }
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
    $dealerList=$dealer->getDealers($params);//Debug::print_r($dealerList);
}else{
    echo 'Seleccione un representante';
}

echo '<div align="center">Existentes</div>';
echo '<select multiple id="dealersFrom" name="countries[count][dealersFrom][]" class="selectMultiple span-4 last" manager="'.$_POST['serial_mbc'].'">';
    if(is_array($dealerList)){
        foreach($dealerList as &$dl){
            $dl['name_dea']=utf8_encode($dl['name_dea']);
            echo '<option value="'.$dl['serial_dea'].'" manager="'.$_POST['serial_mbc'].'" used="0">'.$dl['name_dea'].'</option>';
        }
    }
echo '</select>';
?>
