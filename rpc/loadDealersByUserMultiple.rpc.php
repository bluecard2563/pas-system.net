<?php
/*
File: loadDealersByUserMultiple
Author: David Bergmann
Creation Date: 23/12/2010
Last Modified:
Modified By:
*/

$dealer=new Dealer($db);
if($_POST['serial_usr']){
    $count = $_POST['count'];

    $params=array(
                '0'=>array('field'=> 'c.serial_cit',
						  'value'=> $_POST['serial_cit'],
						  'like'=> 1),
                '1'=>array('field'=> 'd.dea_serial_dea',
                           'value'=> 'NULL',
                           'like'=> 1),
                '2'=>array('field'=> 'd.status_dea',
                           'value'=> 'ACTIVE',
                           'like'=> 1),
                '3'=>array('field'=> 'dtl.serial_lang',
                           'value'=> $_SESSION['serial_lang'],
                           'like'=> 1),
				'4'=>array('field'=>'d.phone_sales_dea',
						   'value'=> 'NO',
						   'like'=> 1),
				'4'=>array('field'=>'ubd.serial_usr',
						   'value'=> $_POST['serial_usr'],
						   'like'=> 1)
    );

	if($_POST['selected_dea']){
		$selected_dea = explode(",", substr($_POST['selected_dea'], 0, strlen($_POST['selected_dea'])-1));
		array_push($params,array('field'=> 'd.serial_dea',
                                 'value'=> $selected_dea,
                                 'like'=> 0));
    }
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
	if($_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_mbc',
				  'value'=> $_SESSION['serial_mbc'],
				  'like'=> 1);
		array_push($params,$aux);
	}

    $dealerList=$dealer->getDealers($params);//Debug::print_r($dealerList);

	echo '<div align="center">Existentes</div><select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-12 last" user="'.$_POST['serial_usr'].'">';
	if(is_array($dealerList)){
		foreach($dealerList as &$dl){
			$dl['name_dea']=utf8_encode($dl['name_dea']);
			echo '<option value="'.$dl['serial_dea'].'" user="'.$_POST['serial_usr'].'">'.$dl['name_dea'] . ' - ' . $dl['code_dea'] .'</option>';
		}
		echo '</select>';
	}
}else{
    echo '<div align="center">Existentes</div><select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-12 last">';
}




?>