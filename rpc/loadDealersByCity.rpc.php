<?php 
/*
* File: loadDealersByCity.php
* Author: David Bergmann
* Creation Date: 01/02/2010
* Last Modified:
* Modified By: 
* Description: File which loads all the sectors from a specific city
	     in a select object.
*/

$dealer=new Dealer($db);
if($_POST['serial_cit']){
    $params=array('0'=>array('field'=> 'c.serial_cit',
                             'value'=> $_POST['serial_cit'],
                             'like'=> 1),
                  '1'=>array('field'=> 'd.dea_serial_dea',
                             'value'=> 'NULL',
                             'like'=> 1),
                  '2'=>array('field'=> 'd.status_dea',
                             'value'=> 'ACTIVE',
                             'like'=> 1),
                  '3'=>array('field'=> 'dtl.serial_lang',
                             'value'=> $_SESSION['serial_lang'],
                             'like'=> 1),
				  '4'=>array('field'=>'d.phone_sales_dea',
							 'value'=> 'NO',
							 'like'=> 1));

	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
}
else {
$noData=1;
}
$dealerList=$dealer->getDealers($params);

$smarty->register('dealerList,noData');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersByCity.'.$_SESSION['language'].'.tpl');
?>