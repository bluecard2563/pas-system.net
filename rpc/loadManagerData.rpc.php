<?php 
/*
File: loadManagerData.rpc.php
Author: Santiago Benítez
Creation Date: 05/02/2010 15:02
Last Modified: 05/02/2010 15:02
Modified By: Santiago Benítez
Description: File which loads all managers from an specific user
*/
Request::setString('serial_man');
$mbc=new ManagerbyCountry($db);
$mbc->setSerial_man($serial_man);
$managersList=$mbc->getManagersByManager();
$smarty->register('managersList');
$smarty->assign('container','none');
$smarty->display('helpers/loadManagerData.'.$_SESSION['language'].'.tpl');
?>