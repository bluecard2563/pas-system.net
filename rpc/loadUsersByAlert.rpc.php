<?php 
/*
File: loadUsersByAlert.rpc.php
Author: Gabriela Guerrero
Creation Date: 05/12/2010
Last Modified: 
Modified By: 
*/

	//TODO CHANGE THIS RPC NAME, THIS RPC DOES NOT LOAD USERS!!
	//THIS RPC LOADS MULTIPLE FORMS WHERE A MULTIPLE SELECT OF USERS IS LOADED EMPTY [AMONG OTHER STUFF] AND NEEDS ANTOHER RPC TO FILL IT UP

	Request::setString('serial_par');
	$error = 0;
	
	$country = new Country($db);
	$countriesList = $country -> getOwnCountries($_SESSION['countryList']);
	
	$parameter = new Parameter($db, $serial_par);
	$parameter -> getData();
	$parameterInfo = unserialize($parameter -> getValue_par());
	
	$alertMbcs = array();
	if($parameterInfo){
		foreach($parameterInfo as $key => $parameterMbc){
			$singleMbc = array();
			$mbcUsers = explode(",", $parameterMbc);
			$arrayUsers = array();
			foreach($mbcUsers as $singleUser){
				$user = new User($db, $singleUser);
				$user -> getData();
				$arrayUsers[] = array('serial_usr' => $singleUser, 'name_usr' => $user -> getFirstname_usr() . ' ' . $user -> getLastname_usr());
			}
			$singleMbc['users']  = $arrayUsers;
			
			$tempMbc = new ManagerByCountry($db, $key);
			$tempMbc -> getData();
			
			$singleMbc['country']  = $tempMbc -> getSerial_cou();
			$singleMbc['originalMbc']  = $key;
			$alertMbcs[] = $singleMbc;
		}
	}
	
	$parameterInfo = $alertMbcs;
	
	$smarty->register('parameterInfo,countriesList,error');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadUsersByAlert.'.$_SESSION['language'].'.tpl');

?>