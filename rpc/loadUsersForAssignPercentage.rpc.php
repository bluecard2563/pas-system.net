<?php
/*
File: loadUsersForAssignPercentage
Author: Esteban Angulo
Creation Date: 21/06/2010
*/

Request::setString('serial_mbc');
if($_POST['type']){
	$type=$_POST['type'];
}

if($_POST['general']){
	$general=$_POST['general'];
}
//Debug::print_r($_POST);
//die();

$user = new User($db);
if($type=='PLANET'){
	$mbc=new ManagerbyCountry($db);
    $serial_mbc=$mbc->getMainManager();
	$planetUserList=$user->getUsersForAssignPercentage($serial_mbc,$general);
	$size=count($planetUserList);
	$pages=round($size/10)+1;
}
if($serial_mbc && $type!='PLANET') {
	$userList=$user->getUsersForAssignPercentage($serial_mbc,$general);
}

	$smarty->register('userList,serial_mbc,planetUserList,type,pages');
	$smarty->assign('container','none');
	$smarty->display('helpers/loadUsersForAssignPercentage.'.$_SESSION['language'].'.tpl');


?>