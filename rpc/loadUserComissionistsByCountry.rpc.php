<?php 
/*
File: loadComissionistsByCountry.php
Author: David Bergmann
Creation Date: 03/02/2010
Last Modified:
Modified By:
Description: File which loads all the comissionists from a specific country
			 in a select objetc
*/
$user=new UserbyCity($db);

if($_POST['serial_cou']){
	$serial_cou = $_POST['serial_cou'];
	$userList=$user->getComissionistsByCountry($_POST['serial_cou'], $_SESSION['serial_mbc']);
}

if(is_array($userList)){
	foreach($userList as &$dl){
		$dl['first_name_usr']=utf8_encode($dl['first_name_usr']);
		$dl['last_name_usr']=utf8_encode($dl['last_name_usr']);
	}
}

$smarty->register('userList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadUserComissionistsByCountry.'.$_SESSION['language'].'.tpl');
?>