<?php
/* 
 *File: loadCountryAccess.rpc.php
 *Author: Esteban Angulo
 *Creation Date: 15/02/2010 
 *Last Modified:
 */
$country= new Country($db);
$city= new City($db);

if($_POST['serial_man']){
    $id=$_POST['serial_man'];
    $countryAccessList=$city->getAllCityAccessList();
    $cityAccessList=$city->getCityAccessListByManager($id);
}
if($_POST['serial_dea']){
    $id=$_POST['serial_dea'];
    $countryAccessList=$city->getAllCityAccessList();
    $cityAccessList=$city->getCityAccessListByDealer($id);
}

if($_POST['serial_usr']){
    $id=$_POST['serial_usr'];
    $countryAccessList=$city->getAllCityAccessList();
    $cityAccessList=$city->getCityAccessListbyUser($id);
}

$smarty->register('countryAccessList,cityAccessList,id');
$smarty->assign('container','none');
$smarty->display('helpers/loadCountryAccess.'.$_SESSION['language'].'.tpl');
?>
