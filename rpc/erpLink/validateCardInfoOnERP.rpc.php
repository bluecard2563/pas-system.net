<?php
/**
 * File: validateCardInfoOnERP
 * Author: Patricio Astudillo
 * Creation Date: 27-jun-2013
 * Last Modified: 27-jun-2013
 * Modified By: Patricio Astudillo
 */

	Request::setString('card_number');
	
	$sale = new Sales($db);
	$sale->setCardNumber_sal($card_number);
	if($sale->getDataByCardNumber()){
		$sale_data = get_object_vars($sale);
		unset($sale_data['db']);
		
		$customer = new Customer($db, $sale_data['serial_cus']);
		$customer->getData();
		
		$sale_data['customer'] = $customer->getFirstname_cus().' '.$customer->getLastname_cus();
		$sale_data['product'] = $sale_data['aux']['product'];
		
		//Debug::print_r($sale_data);
	}
	
	$smarty->register('sale_data');
	$smarty->assign('container','none');
	$smarty->display('helpers/erpLink/validateCardInfoOnERP.'.$_SESSION['language'].'.tpl');
?>
