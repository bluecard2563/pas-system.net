<?php
/*
File: loadCustomerByName.rpc
Author: David Bergmann
Creation Date: 07/04/2010
Last Modified:
Modified By:
*/

$customer = new Customer($db);
$name_cus = $_POST['name_cus'];
$data = $customer->getCustomerSerialbyName($name_cus);

if(is_array($data)){
    //Customer information
    $titles = array("Pa&iacute;s","Ciudad","Documento","Nombre","Fecha de Nacimiento",
                "Tel&eacute;fono","Celular","Correo Electr&oacute;nico","Direcci&oacute;n","Tarjetas");
    $titles_sal = array("Tarjeta","Fecha Emisi&oacute;n","Fecha Inicio","Fecha Fin","Estado de la tarjeta");
    //die(Debug::print_r($data));
    foreach($data as &$d) {     
        $d['name_cou']=utf8_encode($d['name_cou']);
        $d['name_cit']=utf8_encode($d['name_cit']);
        $d['first_name_cus']=utf8_encode($d['first_name_cus']);
        $d['last_name_cus']=utf8_encode($d['last_name_cus']);
        $d['email_cus']=utf8_encode($d['email_cus']);
        $d['address_cus']=utf8_encode($d['address_cus']);

        //Cards information
        $customer->setserial_cus($d['serial_cus']);
        $sales[$d['serial_cus']] = $customer->loadSalesByCustomer();
    }
    
    $smarty->register('titles,data,sales,titles_sal,global_salesStatus');
}
//die(Debug::print_r($sales));


$smarty->register("name_cus");
$smarty->assign('container','none');
$smarty->display('helpers/loadCustomerByName.'.$_SESSION['language'].'.tpl');
?>
