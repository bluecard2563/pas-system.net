<?php
/**
 * File: loadProductsByPOS
 * Author: Patricio Astudillo
 * Creation Date: 05-oct-2012
 * Last Modified: 05-oct-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('serial_dea');
	
	$product_list = ProductByDealer::getProductByDealerStatic($db, $serial_dea, $_SESSION['serial_lang']);
			
	$smarty->register('product_list,serial_dea');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/webPOS/loadProductsByPOS.'.$_SESSION['language'].'.tpl');
?>
