<?php
/*
File: loadResponsablesByManager.rpc.php
Author: Nicolas Flores
Creation Date: 03/03/2010
Last Modified:
Modified By:
Description: Load the manager users wich are responsables of at least one Branch
*/

Request::setString('serial_mbc');
Request::setString('opt_text');

if($serial_mbc) {
	$responsablesList = ManagerbyCountry::getResponsablesByManager($db, $serial_mbc);
}
if(is_array($responsablesList)) {
	$smarty->register('responsablesList');
}

$smarty->register('opt_text');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByManager.'.$_SESSION['language'].'.tpl');
?>