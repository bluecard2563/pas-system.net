<?php
/*
File: loadActiveComissionistsByCountry.rpc
Author: David Bergmann
Creation Date: 17/03/2010
Last Modified:
Modified By:
*/

$comissionist=new ComissionistByCountry($db);

if($_POST['serial_cou']){
	$serial_cou = $_POST['serial_cou'];
	$comissionistList=$comissionist->getActiveComissionistsByCountry($serial_cou);
}

if(is_array($userList)){
	foreach($comissionistList as &$cl){
		$cl['first_name_usr']=utf8_encode($cl['first_name_usr']);
		$cl['last_name_usr']=utf8_encode($cl['last_name_usr']);
	}
}

$smarty->register('comissionistList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadActiveComissionistsByCountry.'.$_SESSION['language'].'.tpl');
?>