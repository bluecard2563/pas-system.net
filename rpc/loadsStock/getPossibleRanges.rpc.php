<?php
/**
 * @author: Patricio Astudillo
 * @name: getPossibleRanges.rpc.php
 * @creationDate: 26/01/2011 09:50
 * @description: Retrieves the possible ranges for stock assign.
 */

Request::setString('stock_to');
Request::setString('amount');
Request::setString('stock_type');
Request::setString('remote_id');

$stock=new Stock($db);
$ranges=$stock->getStockRangesToBeAssigned($amount, $stock_type, $stock_to, $remote_id);

echo json_encode($ranges);
?>