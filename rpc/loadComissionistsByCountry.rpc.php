<?php 
/*
File: loadAssignedComissionistsByCountry.php
Author: David Bergmann
Creation Date: 04/02/2010
Last Modified:
Modified By:
Description: File which loads all the users responsible from a specific country
			 in a select objetc
*/

$comissionist=new ComissionistByCountry($db);

if($_POST['serial_usr']){
	$comissionistList=$comissionist->getComissionistsByCountry($_POST['serial_usr'], $_SESSION['serial_mbc']);
}

if(is_array($comissionistList)){
	foreach($comissionistList as &$cl){
		$dl['first_name_usr']=utf8_encode($dl['first_name_usr']);
		$dl['last_name_usr']=utf8_encode($dl['last_name_usr']);
	}
}

$smarty->register('comissionistList');
$smarty->assign('container','none');
$smarty->display('helpers/loadComissionistsByCountry.'.$_SESSION['language'].'.tpl');
?>