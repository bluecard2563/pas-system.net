<?php 
/*
File: loadManagersByCountry.rpc.php
Author: Patricio Astudillo M.
Creation Date: 11/01/2010 15:18
Last Modified: 19/03/2010
Modified By: Santiago Borja
Description: File which loads all the managers of a country
*/
$notRequired = $_POST['notRequired'];
Request::setString('serial_cou');
Request::setString('serial_sel');
//UPDATE OVER
Request::setString('manManager');
Request::setString('user');
//REPORTS
Request::setString('opt_text');
//MANAGERS
$paUser="1";
//DOCUMENTS
Request::setString('manager_only');

$managerBC = new ManagerbyCountry($db);
if($serial_cou){
	$managerList=$managerBC->getManagerByCountry($serial_cou, $_SESSION['serial_mbc']);
}
$smarty->register('managerList,serial_sel,serial_cou,manManager,user,opt_text,notRequired,manager_only');
$smarty->assign('container','none');
$smarty->display('helpers/loadManagersByCountry.'.$_SESSION['language'].'.tpl');
?>