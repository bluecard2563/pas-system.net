<?php
/**
 * File: loadDealersByComissionist
 * Author: Patricio Astudillo M.
 * Creation Date: 29/02/2012 
 * Last Modified: 29/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('serial_usr');
	
	$dealer_list = UserByDealer::getDealersByUser($db, $serial_usr);
	
	$smarty -> register('dealer_list');
	$smarty -> assign('container', 'none');
	$smarty -> display('helpers/loadComissionistByDealer/loadDealersByComissionist.'.$_SESSION['language'].'.tpl');
?>
