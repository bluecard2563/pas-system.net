<?php 
/*
File: loadServices.php
Author: Edwin Salvador
Creation Date: 12/02/2010
Last Modified: 23/06/2010
Modified By: Patricio Astudillo
Description: File which loads all the services acording a
			 specific language
*/

Request::setString('serial_lang');
$language=new Language($db, $serial_lang);
$language->getData();

$service=new Service($db);
$serviceList=$service->getServices($language->getCode_lang());

$smarty->assign('container','none');
$smarty->register('serviceList');
$smarty->display('helpers/loadServices/loadServices.'.$_SESSION['language'].'.tpl');
?>