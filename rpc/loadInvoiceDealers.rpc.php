<?php
/*
File: loadInvoiceDealers.rpc.php
Author: Santiago Benitez
Creation Date: 20/04/2010
LastModified:
Modified By:
*/
Request::setInteger('serial_usr');
Request::setString('dealer_cat');
Request::setInteger('serial_mbc');
Request::setInteger('serial_dlt');
if(!$dealer_cat||$dealer_cat=='false'){
    $dealer_cat=null;
}
$dealer=new Dealer($db);
//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList=$dealer->getInvoiceDealers($serial_mbc,$serial_usr,$dealer_cat,$serial_dlt,$_SESSION['dea_serial_dea']);
} else {
	$dealerList=$dealer->getInvoiceDealers($serial_mbc,$serial_usr,$dealer_cat,$serial_dlt);
}
$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersFiltered.'.$_SESSION['language'].'.tpl');
?>
