<?php
/*
File: loadDealersByManager.rpc.php
Author: Edwin Salvador
Creation Date: 22/02/2010
LastModified:
Modified By:
*/

$dealer=new Dealer($db);


if($_POST['serial_mbc']){
    $serial_mbc = $_POST['serial_mbc'];
    $serial_usr = $_POST['serial_usr'];
    $count = $_POST['count'];
    $params=array(
                '0'=>array('field'=> 'd.serial_mbc',
                           'value'=> $serial_mbc,
                           'like'=> 1),
                '1'=>array('field'=> 'd.dea_serial_dea',
                           'value'=> 'NULL',
                           'like'=> 1),
                '2'=>array('field'=> 'd.status_dea',
                           'value'=> 'ACTIVE',
                           'like'=> 1),
                '3'=>array('field'=> 'dtl.serial_lang',
                           'value'=> $_SESSION['serial_lang'],
                           'like'=> 1),
				'4'=>array('field'=>'d.phone_sales_dea',
						   'value'=> 'NO',
						   'like'=> 1),
    			'5'=>array('field'=>'ubd.serial_usr',
						   'value'=> $serial_usr,
						   'like'=> 1)
    );
    if($_POST['serial_dealers']){
        array_push($params,array('field'=> 'd.serial_dea',
                                      'value'=> explode(',', $_POST['serial_dealers']),
                                      'like'=> 0));
    }
	//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
	if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
		$aux=array('field'=>'d.serial_dea',
		  'value'=> $_SESSION['dea_serial_dea'],
		  'like'=> 1);
		array_push($params,$aux);
	}
    $dealerList=$dealer->getDealers($params);//Debug::print_r($dealerList);
}else{
    echo 'Seleccione un representante';
}

if(is_array($dealerList)){
    echo '<select id="selDealerDea" name="selDealerDea" class="select span-4 last" city="'.$_POST['serial_cit'].'">';
    echo '<option value="">- Todos -</option>';
    foreach($dealerList as &$dl){
		$dl['name_dea']=utf8_encode($dl['name_dea']);
        echo '<option value="'.$dl['serial_dea'].'">'.$dl['name_dea'].'</option>';
    }
    echo '</select>';
}else{
    echo '<br><select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" city="'.$_POST['serial_cit'].'" style="display:none;">';
    echo '</select>';
    echo 'No existen comercializadores para el representante seleccionado';
}
?>
