<?php
/*
File: loadKitDetails.rpc.php
Author: Santiago Ben�tez
Creation Date: 24/02/2010 14:02
Last Modified: 24/02/2010 14:02
Modified By: Santiago Ben�tez
*/
Request::setString('serial_kbd');
$kitDetails=new KitDetails($db);
$kitDetails->setSerial_kbd($serial_kbd);
$kitDetailsList=$kitDetails->getKitDetails($_POST['fromDate'],$_POST['toDate']);
$smarty->register('kitDetailsList');
$smarty->assign('container','none');
$smarty->display('helpers/loadKitDetails.'.$_SESSION['language'].'.tpl');
?>