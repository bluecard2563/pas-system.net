<?php
/*
File: loadResponsablesByCity.rpc.php
Author: Edwin Salvador
Creation Date: 11/03/2010
Last Modified:
Modified By:
*/

Request::setString('serial_cit');
$opt_text = $_POST['opt_text'];
if($serial_cit) {
    $dealer = new Dealer($db);
    $responsablesList =  $dealer->getResponsablesByCity($serial_cit, $_SESSION['serial_mbc']);

    if(is_array($responsablesList)){
        foreach($responsablesList as &$rl){
            $cl['name_usr']=($cl['name_usr']);
        }
    }
    $smarty->register('responsablesList,opt_text');
}
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>