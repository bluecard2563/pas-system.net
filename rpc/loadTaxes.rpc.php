<?php 
/*
File: loadTaxes.php
Author: David Bergmann
Creation Date: 28/01/2010
Last Modified:
Modified By:
Description: File which loads all the taxes from a specific country 
			 in a select objetc
*/

$tax=new Tax($db);
if($_POST['serial_cou']){
	$serial_cou=$_POST['serial_cou'];
	$taxList=$tax->getTaxes($_POST['serial_cou']);
}

if(is_array($taxList)){
	foreach($taxList as &$tl){
		$tl['name_tax']=utf8_encode($tl['name_tax']);
	}
}

$smarty->register('taxList,serial_cou');
$smarty->assign('container','none');
$smarty->display('helpers/loadTaxes.'.$_SESSION['language'].'.tpl');
?>