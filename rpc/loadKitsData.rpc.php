<?php
/*
File: loadKitsData.rpc.php
Author: Santiago Ben�tez
Creation Date: 23/02/2010 14:02
Last Modified: 23/02/2010 14:02
Modified By: Santiago Ben�tez
*/
Request::setString('serial_dea');
$kit=new KitByDealer($db);
$kitDetails=new KitDetails($db);
$kit->setSerial_dea($serial_dea);
if($kit->getDataByDealer()){
    $kitsTotal=$kit->getTotal_kbd();
    $kitByDealerID=$kit->getSerial_kbd();
}

$actionList=$kitDetails->getAllActions();
$smarty->register('actionList,kitsTotal,kitByDealerID');
$smarty->assign('container','none');
$smarty->display('helpers/loadKitsData.'.$_SESSION['language'].'.tpl');
?>