<?php
/**
 * File: loadBranchesAutocompleter
 * Author: Patricio Astudillo
 * Creation Date: 07/05/2012
 * Last Modified: 07/05/2012
 * Modified By: Patricio Astudillo
 */

	$pattern = utf8_decode($_GET['q']);
	
	$branches = Dealer::getBranchesAutocompleter($db, $pattern);
	
	if($branches){
		foreach($branches as $c){
			echo utf8_encode($c['serial_dea']."|".$c['code_dea'].' - '.$c['name_dea'])."\n";
		}
	}else{
		echo 'no_record|no_record';
	}
?>
