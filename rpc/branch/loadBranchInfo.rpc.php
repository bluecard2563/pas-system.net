<?php
/**
 * File: loadBranchInfo
 * Author: Patricio Astudillo
 * Creation Date: 07/05/2012
 * Last Modified: 07/05/2012
 * Modified By: Patricio Astudillo
 * 
 */

	Request::setInteger('serial_dea');
	
	$dealer = new Dealer($db, $serial_dea);
	
	if($dealer->getData()){
		$dealer_data = get_object_vars($dealer);
		unset($dealer_data['db']);
		
		/* CASH MANAGEMENT SECTION */
		$parameterBank = new Parameter($db, 5); //BANKS
		$parameterBank->getData();
		$parameterBank = get_object_vars($parameterBank);
		$banks = explode(',', $parameterBank['value_par']);
		$account_types = Dealer::getAccountTypes($db);
	}
	
	$smarty->register('dealer_data,banks,account_types');
	$smarty->assign('container', 'none');
	$smarty->display('helpers/branch/loadBranchInfo.'.$_SESSION['language'].'.tpl');
?>
