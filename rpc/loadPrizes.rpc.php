<?php
/*
File: loadPrizes.php
Author: Santiago Benitez
Creation Date: 16/04/2010 09:45
Last Modified: 16/04/2010
Modified By: Santiago Benitez
Description: File which loads all the sectors from a specific city
			 in a select objetc
*/

$prize=new Prize($db);
if($_POST['serial_cou']){
	$serial_cou=$_POST['serial_cou'];	
}else{
    $serial_cou=null;
}

if($_POST['type_pri']){
	$type_pri=$_POST['type_pri'];
}else{
    $type_pri=null;
}
if($type_pri!=null||$serial_cou!=null){
    $prizeList=$prize->getPrizes($serial_cou,$type_pri);
}
if(is_array($countryList)){
	foreach($countryList as &$cl){
		$cl['name_pri']=utf8_encode($cl['name_pri']);
	}
}
$smarty->register('prizeList,serial_cou,type_pri');
$smarty->assign('container','none');
$smarty->display('helpers/loadPrizes.'.$_SESSION['language'].'.tpl');
?>