<?php
/*
File: loadServiceProviderByCountry.rpc
Author: David Bergmann
Creation Date: 08/04/2010
Last Modified:
Modified By:
*/

$serviceProvider = new ServiceProviderByCountry($db);
$serial_cou = $_POST['serial_cou'];
$sel_id = $_POST['sel_id'];
$type_spv = $_POST['type_spv'];
$providerList = $serviceProvider->loadServiceProviderByCountry($serial_cou,$type_spv);

if(is_array($providerList)){
    foreach($providerList as &$pl) {
        $pl["name_spv"] = utf8_encode($pl["name_spv"]);
    }
}

$smarty->register('serial_cou,type_spv,providerList,sel_id');
$smarty->assign('container','none');
$smarty->display('helpers/loadServiceProviderByCountry.'.$_SESSION['language'].'.tpl');
?>