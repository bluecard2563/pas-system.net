<?php
/*
File: loadCountersByCountry.rpc
Author: David Bergmann
Creation Date: 15/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_dea');

if($serial_dea) {
	$counter = new Counter($db);
	$counterList = $counter->getCountersByBranch($serial_dea,true);
	if(is_array($counterList)) {
		foreach($counterList as &$c) {
			$c['cnt_name'] = utf8_encode($c['cnt_name']);
		}
	}
	$titles = array('#','Usuario','Estado','Cambiar Estado');
}
$smarty->register('counterList,titles,serial_dea,global_status');
$smarty->assign('container','none');
$smarty->display('helpers/counter/loadCountersByCountry.'.$_SESSION['language'].'.tpl');
?>