<?php
/*
File: loadUsersByManagerByCountry.rpc
Author: David Bergmann
Creation Date: 15/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_mbc');
Request::setString('serial_dea');

$userList = ManagerbyCountry::getUsersByManagerByCountry($db, $serial_mbc, $serial_dea);

$smarty->register('userList,serial_mbc,serial_dea');
$smarty->assign('container','none');
$smarty->display('helpers/counter/loadUsersByManagerByCountry.'.$_SESSION['language'].'.tpl');
?>
