<?php
/*
File: changePhoneCounterStatus.rpc
Author: David Bergmann
Creation Date: 15/07/2010
Last Modified:
Modified By:
*/

Request::setString('serial_usr');
Request::setString('status_cnt');

Counter::changePhoneCounterStatus($db, $serial_usr, $status_cnt);
$counter = new Counter($db, $serial_cnt);
$counter->getData();
$counter->setStatus_cnt($status_cnt);

if($counter->update()){
	echo json_encode(TRUE);
} else {
	echo json_encode(FALSE);
}

?>