<?php
/*
 * File: loadDealersWithFreeSales.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 19/05/2010, 09:12:56 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 19/05/2010, 09:12:56 AM
 */

Request::setString('serial_usr');
Request::setString('serial_cit');

//If $_SESSION['dea_serial_dea'] is set, loads only the user's dealer
if($_SESSION['dea_serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList=Dealer::getDealersWithFreeSales($db, $serial_usr, $serial_cit, $_SESSION['dea_serial_dea']);
} else {
	$dealerList=Dealer::getDealersWithFreeSales($db, $serial_usr, $serial_cit);
}

$smarty->register('dealerList');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealersFiltered.'.$_SESSION['language'].'.tpl');
?>