<?php
/*
 * File: loadBranchesWithFreeSales.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 19/05/2010, 09:27:26 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 19/05/2010, 09:27:26 AM
 */

Request::setString('dea_serial_dea');
$branch=1;

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dealerList=Dealer::getBranchesWithFreeSales($db, $dea_serial_dea, $_SESSION['serial_dea']);
} else{
	$dealerList=Dealer::getBranchesWithFreeSales($db, $dea_serial_dea);
}

$smarty->register('dealerList,branch');
$smarty->assign('container','none');
$smarty->display('helpers/loadDealers.'.$_SESSION['language'].'.tpl');
?>
