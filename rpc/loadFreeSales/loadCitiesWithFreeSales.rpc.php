<?php
/*
 * File: loadCitiesWithFreeSales.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 18/05/2010, 03:50:06 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 18/05/2010, 03:50:06 PM
 */

$cityList=City::getCitiesWithFreeSales($db, $_POST['serial_cou'], $_SESSION['serial_mbc']);
if(is_array($cityList)){
	foreach($cityList as &$cl){
		$cl['name_cit']=($cl['name_cit']);
	}
}

$smarty->register('cityList,serial_cou,serial_sel');
$smarty->assign('container','none');
$smarty->display('helpers/loadCities.'.$_SESSION['language'].'.tpl');
?>
