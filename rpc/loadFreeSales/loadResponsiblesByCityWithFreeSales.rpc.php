<?php
/*
 * File: loadResponsiblesByCityWithFreeSales.rpc.php
 * Author: Patricio Astudillo
 * Creation Date: 19/05/2010, 08:56:14 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 19/05/2010, 08:56:14 AM
 */

Request::setString('serial_cit');

$responsablesList =  UserByDealer::getResponsiblesWithFreeSales($db, $serial_cit, $_SESSION['serial_mbc']);

$smarty->register('responsablesList');
$smarty->assign('container','none');
$smarty->display('helpers/loadResponsablesByCity.'.$_SESSION['language'].'.tpl');
?>
