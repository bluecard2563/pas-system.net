<?php
/*
 * File: getRefundRequestForm.rpc
 * Author: Patricio Astudillo
 * Description: RPC that loads all the form to request the refund of a specific sale.
 * Creation Date: 13/04/2010
 * Modifies By:
 * Last Modified:
 */

Request::setString('card_number');
$sale=new Sales($db);
$sale->setCardNumber_sal($card_number);

if($sale->getDataByCardNumber()){
	$invoice=new Invoice($db, $sale->getSerial_inv());
	$invoice->getData();
	if(!Sales::isMassiveSale($db, $sale->getSerial_sal())){
		if($invoice->getStatus_inv()=='PAID'){
			if(!Sales::hasApplicationsPending($db, $sale->getSerial_sal())){
				if(!Sales::hasBeenReactivated($db, $sale->getSerial_sal())){
					if(!File::saleHasFilesAssociated($db, $sale->getSerial_sal())){
						if(!Sales::flightRegisterGeneratesCardNumber($db, $sale->getSerial_sal())){
							if(Sales::isRefundable($db, $sale->getSerial_sal())){
								if(!Refund::hasRefundApplications($db, $sale->getSerial_sal())){
									$parameter=new Parameter($db,'12'); //Parameter for the Direct Sale Fee.
									$parameter->getData();
									$directSaleFee=explode(',',$parameter->getValue_par());
									$sale=get_object_vars($sale);
									unset($sale['db']);
	
									//If the sale is a direct Sale, we have to charge the fee acording to the parameter 12.
									if($sale['direct_sale_sal']=='YES'){
										$directSaleFee['value']=$directSaleFee['0'];
										$directSaleFee['type']='PERCENTAGE';
									}else{
										$directSaleFee['value']=$directSaleFee['1'];
										$directSaleFee['type']='AMOUNT';
									}
	
									//Get the Information of the Customer and Invoice
									$customer=new Customer($db,$sale['serial_cus']);
	
									if($customer->getData()){
										//Get the Payment Data
										$payment=new Payment($db,$invoice->getSerial_pay());
										$payment->getData();
										$payment=get_object_vars($payment);
										$refundType='REGULAR';
										$invoice=get_object_vars($invoice);
	
										$dealer=Counter::getCountersDealer($db, $sale['serial_cnt']);
										$customer=get_object_vars($customer);
										unset($customer['db']);
										unset($invoice['db']);
	
										//Get the Refund Data for the aplication.
										$refundData=array();
										$refundData['reason']=Refund::getRefundReason($db);
										$refundData['retainedType']=Refund::getRetainedType($db);
										$parameter->setSerial_par('11');     //Parameter for the required documents to fill the refuns aplication.
										$parameter->getData();
										$documentList=explode(',',$parameter->getValue_par());
	
										//Get the taxes applied in the invoice
										if($invoice['applied_taxes_inv'] != ""){
											$invoice['applied_taxes_inv'] = unserialize($invoice['applied_taxes_inv']);
											
											if(is_array($invoice['applied_taxes_inv'])){
												foreach($invoice['applied_taxes_inv'] as $t){
													$taxes+=$t['tax_percentage'];
												}
												array_push($invoice['applied_taxes_inv'], array('tax_name'=>'Total','tax_percentage'=>$taxes));
											}											
										}
	
										//PreTotal Refund Calculation
										$totalRefund=$sale['total_sal'];
										$totalRefund-=($sale['total_sal']*($invoice['discount_prcg_inv']/100));
										$totalRefund-=($sale['total_sal']*($invoice['other_dscnt_inv']/100));
										$totalRefund*=(1+($taxes/100));
										$totalRefund=number_format($totalRefund, '2');
	
										$comission=AppliedComission::hasPayedComissionsBySale($db, $sale['serial_sal'], Dealer::getLastComissionPaymentDate($db, $dealer['serial_dea']));
										$bonus=AppliedBonus::hasPayedBonusBySale($db, $sale['serial_sal'], Dealer::getLastComissionPaymentDate($db, $dealer['serial_dea']));
										if($comission or $bonus){
											$paiedAmounts=1;
										}
										$smarty->register('sale,customer,invoice,dealer,payment,documentList,refundData,taxes,totalRefund,refundType,directSaleFee,comission,paiedAmounts,bonus');
									}else{//Error on loading the customer/invoice data
										$error=3;
									}
								}else{//There are some current applications pending or approved in the system for the sale.
									$error=5;
								}
							}else{//The card is not refundable.
								$error=2;
							}
						}else{ //Flights generate number.
							$error=6;
						}
					}else{ //Has Files Associated.
						$error=7;
					}
				}else{ //Has been Reactivated
					$error=8;
				}
			}else{ //Has any sales log application pending
				$error=9;
			}
		}else{ //The invoice is not Paid
			$error=4;
		}
	}else{ //Massive Sales can't be refunded.
		$error=10;
	}	
}else{//The card_number doesn't exist.
	$error=1;
}

$smarty->register('error');
$smarty->assign('container','none');
$smarty->display('helpers/loadRefunds/getRefundRequestForm.'.$_SESSION['language'].'.tpl');
?>