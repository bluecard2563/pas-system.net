<?php 
/*
File: loadCreditDays.rpc.php
Author: Patricio Astudillo M.
Creation Date: 11/01/2010 14:54
Last Modified: 11/01/2010
Modified By: Patricio Astudillo
Description: File which loads all the data for the management 
			 of credit days by country.
*/

Request::setString('serial_cou');
$creditD=new CreditDay($db);
if($serial_cou){
	$dataByCountry=$creditD->getCreditDays($serial_cou);
}

$smarty->register('dataByCountry');
$smarty->assign('container','none');
$smarty->display('helpers/loadCreditDays.'.$_SESSION['language'].'.tpl');
?>