<?php
/*
File: loadProductTypesByLanguage.rpc
Author: David Bergmann
Creation Date: 30/07/2010
Last Modified:
Modified By:
*/

Request::setString('code_lang');

$productType = new ProductType($db);
$productTypeList = $productType->getProductTypes($code_lang);

$smarty->register('productTypeList');
$smarty->assign('container','none');
$smarty->display('helpers/loadProductTypesByLanguage.'.$_SESSION['language'].'.tpl');
?>