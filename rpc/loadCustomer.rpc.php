<?php
/*
File: loadCustomer.rpc.php
Author: Esteban Angulo
Creation Date: 01/03/2010
*/
$customer=new Customer($db);
$typesList=$customer->getAllTypes();
$sessionLanguage = $_SESSION['serial_lang'];
$question=new Question($db);
$maxSerial=$question->getMaxSerial($sessionLanguage);


if($_POST['document_cus']){
        $document_cus=$_POST['document_cus'];
        if($customer->existsCustomer($document_cus,NULL)){
            $countryList = Country::getAllAvailableCountries($db);

            $city=new City($db);

            $customer->setserial_cus($customer->getCustomerSerialbyDocument($document_cus));
            $customer->getData();

            $data['serial_cus']=$customer->getSerial_cus();
            $data['document_cus']=$customer->getDocument_cus();
            $data['firstname_cus']=$customer->getFirstname_cus();
            $data['lastname_cus']=$customer->getLastname_cus();
            $data['serial_cit']=$customer->getSerial_cit();
            $data['birthdate_cus']=$customer->getBirthdate_cus();
            $data['address_cus']=$customer->getAddress_cus();
            $data['phone1_cus']=$customer->getPhone1_cus();
            $data['phone2_cus']=$customer->getPhone2_cus();
            $data['cellphone_cus']=$customer->getCellphone_cus();
            $data['email_cus']=$customer->getEmail_cus();
            $data['relative_cus']=$customer->getRelative_cus();
            $data['relative_phone_cus']=$customer->getRelative_phone_cus();
            $data['vip_cus']=$customer->getVip_cus();
            $data['type_cus']=$customer->getType_cus();



            $data['serial_cou']= Country::getCountryByCity($db, $data['serial_cit']);
            $cityList=$city->getCitiesByCountry($data['serial_cou'],NULL);

            if(is_array($countryList)){
                foreach($countryList as &$cl){
                        $cl['name_cou']=utf8_encode($cl['name_cou']);
                }
            }

            $questionList = Question::getActiveQuestions($db, $sessionLanguage,$data['serial_cus']);
            //Debug::print_r($questionList);
            $answer=new Answer($db);
            $answer->setSerial_cus($data['serial_cus']);
            $answerList=$answer->getAnswers();
            
        $smarty->register('data,countryList,cityList,typesList,answerList,questionList');
        }
        else{
            $countryList = Country::getAllAvailableCountries($db);

            if(is_array($countryList)){
                foreach($countryList as &$cl){
                        $cl['name_cou']=utf8_encode($cl['name_cou']);
                }
            }
            $questionList = Question::getActiveQuestions($db, $sessionLanguage);
            //echo $document_cus;
            $smarty->register('countryList,document_cus,typesList,questionList');
        }
        
    }

$smarty->assign('container','none');
$smarty->display('helpers/loadCustomer.'.$_SESSION['language'].'.tpl');
?>
