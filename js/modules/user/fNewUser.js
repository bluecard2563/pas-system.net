//Java Script
var type;
$(document).ready(function(){
        $("[name='belongsto_usr']").click(function(){
                $("#CountryAccess").html("");
                extraData($(this).val());
	});
	$(":checkbox.countries").click(function(){
            showCities($(this).val());
        });

         if($('#selCountry').find('option').size()==2){
                loadCities($('#selCountry').val());
                loadProfiles($('#selCountry').val());
            }
        
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmNewUser").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFirstName: {
				required: true,
				textOnly: true
			},
			txtLastName: {
				required: true,
				textOnly: true
			},
                        txtPosition:{
                            textOnly: true,
                            required: true
                        },
			txtDocument: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUserDocument.rpc",
					type:"POST"
				}
			},
			txtPhone: {
				required: true,
				digits: true
			},
			txtCellphone: {
				digits: true
			},
			txtBirthdate: {
				required: true,
				date: true
			},
			txtMail: {
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUserEmail.rpc",
					type: "POST"
				}
			},
			selProfile: "required",
			txtUsername: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUsername.rpc",
					type: "POST"
				}
			},
                        belongsto_usr:{
				required: true
			},
                        selManager:{
				required: true
			},
                        selCountryEx: {
				required: {
                                        depends: function(element) {
                                                var display = $("#extraInfo").css('display');
                                                if(display=='none')
                                                return false;
                                                else
                                                return true;
                                            }
                                        }
			},
			selDealer: {
				required: {
                                        depends: function(element) {
                                                var display = $("#extraInfo").css('display');
                                                if(display=='none')
                                                return false;
                                                else
                                                return true;
                                            }
                                        }
			},
			selBranch: {
				required: {
                                        depends: function(element) {
                                                var display = $("#extraInfo").css('display');
                                                if(display=='none')
                                                return false;
                                                else
                                                return true;
                                            }
                                        }
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtPassword: {
				required: true,
				minlength: 6
			},
			txtRePassword: {
				required: true,
				equalTo: "#txtPassword"
			},
                        txtVisitsNumber:{
                           max:6
                        },
                        hddValidateCities:{
                            required: {
                                        depends: function(element) {
                                                var display = $("#extraInfo").css('display');
                                                if(display=='none')
                                                return false;
                                                else
                                                return true;
                                            }
                                        }
                        },
                        hddValidateCountries:{
                            required: {
                                        depends: function(element) {
                                                var display = $("#extraInfo").css('display');
                                                if(display=='none')
                                                return false;
                                                else
                                                return true;
                                            }
                                        }
                        }
		},
		messages: {
			txtFirstName: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
     		},
			txtLastName: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
     		},
			txtDocument: {
				remote: "La identificaci&oacute;n ingresada ya existe."
			},
		    txtMail: {
       			email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe."
     		},
			txtPassword:{
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtRePassword: {
				equalTo: "La confirmaci&oacute;n ha fallado."
			},
			selProfile: {
				remote: "Por favor seleccione un Perfil."
			},
			txtUsername: {
				remote: "El usuario ingresado ya existe.",
				validarUsername: "El nombre de usuario no es v&aacute;lido."
			},
			 txtBirthdate: {
       			date:"El campo 'Fecha de Naciemiento' debe tener el formato mm/dd/YYYY"
                        },
                        txtVisitsNumber: {
                        max: "El m&aacute;ximo n&uacute;mero de visitas es 6"
                        },
                        txtCellphone: {
                            digits: "El campo 'Celular' admite solo d&iacute;gitos."
                        },
                        txtPhone: {
                            digits: "El campo 'Tel&eacute;fono' admite solo d&iacute;gitos."
                        },
                        txtPosition:{
                            textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Cargo'."
                        }
   		},
		submitHandler: function(form) {
			$('#btnRegistrar').attr('disabled', 'true');
			$('#btnRegistrar').val('Espere por favor...');
			form.submit();
	   }
	});	
        setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-20y');
	
	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
	      loadCities(this.value);
              loadProfiles(this.value);
        });
	/*END CHANGE*/
});

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}


function loadDealers(countryID){
	if(countryID==""){
		countryID="stop";
	}
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val(''); 
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{serial_cou: countryID, dea_serial_dea: 'NULL', phone_sales: 'ALL'},function(){
            $('#selDealer').bind('change',function(){
              loadBranches(this.value);
            });
			if($("#selDealer option").size() ==2 ){
				loadBranches($("#selDealer").val());
			}
        });
}

function loadBranches(dealerID){
		if(dealerID) {
            $("#branchContainer").load(document_root+"rpc/loadDealers.rpc",{dea_serial_dea: dealerID, phone_sales: 'ALL'},function(){
            $('#selBranch').change(function(){
                  loadCountryAccess(dealerID,type); 
            });
            
            if($('#selBranch').find('option').size()==2){
                loadCountryAccess(dealerID,type); 
            }
	  });
	}
}

function loadProfiles(countryID){
	if(countryID==""){
		$("#selProfile option[value='']").attr("selected",true);
	}

	$('#profileContainer').load(document_root+"rpc/loadProfiles.rpc",{serial_cou: countryID});
        
}


function loadCountryAccess(managerID){
    if(type=='MANAGER'){
        if(managerID!=""){
        $('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_man: managerID},function(){
            validateCheck("countries","hddValidateCountries")
            validateCheck("cities","hddValidateCities");
            $(":checkbox.countries").change(function(){
                showCities($(this).val());
                validateCheck("countries","hddValidateCountries");
                validateCheck("cities","hddValidateCities");

                
            });
            $(":checkbox.cities").change(function(){
                validateCheck("cities","hddValidateCities");
            });
        });
    }
    else{
         $('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_man: 'NULL'});
    }
  }
 if(type=='DEALER'){
      if(managerID!=""){
        $('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_dea: managerID},function(){
            validateCheck("countries","hddValidateCountries")
            validateCheck("cities","hddValidateCities");
            $(":checkbox.countries").change(function(){
                showCities($(this).val());
                validateCheck("countries","hddValidateCountries");
                validateCheck("cities","hddValidateCities");


            });
            $(":checkbox.cities").change(function(){
                validateCheck("cities","hddValidateCities");
            });
        });
    }
    else{
         $('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_man: 'NULL'});
    }
  }
}

function validateCheck(chkClass,hidden){
    var isAnySelected="";
        $(":checkbox."+chkClass).each(function(i){
            if($(this).attr("checked")){
               isAnySelected="1";
            }
        });
    $('#'+hidden).val(isAnySelected);
}

function loadManagersByCountry(countryID){
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID, user: '1'},function(){
           $('#selManager').change(function(){
                  loadCountryAccess($(this).val(),type);
            });
	});
}

function extraData(aux){
    if(aux!='PLANETASSIST'){
        $('#extraData').load(document_root+"rpc/loadExtraInfo.rpc",{param: aux},function(){
                $('#selCountryEx').bind('change',function(){
                  if(aux=='DEALER'){
                    loadDealers(this.value);
                    type='DEALER';
                  }else if(aux=='MANAGER'){
                     loadManagersByCountry(this.value);
                     type='MANAGER';
					 //loadCountryAccess($(this).val(),type);
                  }
                });
            },function(){
                $('#selDealer').bind('change',function(){
                  loadBranches(this.value);
                });
            }
        );
    }else{
        $('#extraData').html('');
        type='MANAGER';
        loadCountryAccess($('#ecuadorID').val());
    }
}

function checkAllCountries() {
	$('.countries').attr("checked",true);
	$('.country').show();
        $('.city').show();
	$('.city').attr("checked",true);
        $("#hddValidateCountries").val('1');
        $("#hddValidateCities").val('1');

	if(!($("#chkAllCountries").is(':checked'))){
		$('.countries').attr("checked",false);
		$('.country').hide();
		$('.city').attr("checked",false);
                $("#hddValidateCountries").val('');
                $("#hddValidateCities").val('')
	}
}

function checkAllCities(serial_cou) {
	if($("#chkAllCities"+serial_cou).is(':checked')){
		$(":checkbox.cities[country='"+serial_cou+"']").attr("checked",true);
		$("#hddValidateCities").val('1');

	}
	else{
		$(":checkbox.cities[value='"+serial_cou+"']").attr("checked",false);
		$("#hddValidateCities").val('');
	}

}

function showCities(serial_cou) {
	if($(":checkbox.countries[value='"+serial_cou+"']").is(':checked')){
		$("[country='"+serial_cou+"']").show();
	}
	else{
		$("[country='"+serial_cou+"']").hide();
		$(":checkbox.cities[value='"+serial_cou+"']").attr("checked",false);
		$("#chkAllCities"+serial_cou).attr("checked",false);
	}


}
