//JavaScript
	
  $(document).ready(function(){
	
	/*VALIDATION SECTION*/
	$("#frmSearchUser").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtSearch: {
				required: true
			}
		}
	});
	/*end Validation Section*/
	
	var ruta=document_root+"rpc/autocompleters/loadUsers.rpc";
	$("#txtSearch").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		//$("#serial_usr").val(row[1]);
                if(row[0]=="No existe un usuario con esos datos."){
                    $("#txtSearch").val('');
		}
		$("#serial_usr").val(row[1]);
	});
	
  });
