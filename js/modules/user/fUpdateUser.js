var type;
var pager;
$(document).ready(function(){
	if($('#city_table').length>0){
		pager = new Pager('city_table', 7 , 'Siguiente', 'Anterior');
		pager.init(200); //Max Pages
		pager.showPageNav('pager', 'table_paging'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	 }
	 
    $("[name='belongsto_usr']").click(function(){
		$("#CountryAccess").html("");
		extraData($(this).val());
	});
    $("#btnEdit").click(function(){
		$("#CountryAccess").html("");
		if($("#hddPAUser").val()=='PLANETASSIST'){
			type=$("#hddPAUser").val();
			extraData($("#hddPAUser").val());
		}
		else{
			type=$("#hddBelongsto").val();
			extraData($("#hddBelongsto").val());
		}
	});
        
	$(":checkbox.countries").click(function(){
		showCities($(this).val());
	});

	if($('#hddCountries').val()=='1'){
		$("#hddValidateCountries").val('1');
		$("#hddValidateCities").val('1');
	}

	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmUpdateUser").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFirstName: {
				required: true,
				textOnly: true
			},
			txtLastName: {
				required: true,
				textOnly: true
			},
			txtPhone: {
				required: true,
				digits: true
			},
			txtCellphone: {
				digits: true
			},
			txtBirthdate: {
				required: true,
				date: true
			},
			txtPosition:{
				textOnly: true,
				required: true
			},
			txtMail: {
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUserEmail.rpc",
					type: "POST",
					data: {
					  serial_usr: function() {
						return $("#serial_usr").val();
					  }
					}
				}
			},
			selProfile: {
				required: true
			},
                        belongsto_usr:{
				required: true
			},
                        selManager:{
				required: true
			},
			selCountryEx: {
				required: {
					depends: function(element) {
						var display = $("#extraInfo").css('display');
						if(display=='none')
						return false;
						else
						return true;
					}
				}
			},
			selDealer: {
				required: {
					depends: function(element) {
						var display = $("#extraInfo").css('display');
						if(display=='none')
						return false;
						else
						return true;
					}
				}
			},
			selBranch: {
				required: {
					depends: function(element) {
						var display = $("#extraInfo").css('display');
						if(display=='none')
						return false;
						else
						return true;
					}
				}
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selStatus:{	required: true,
				remote: {
						url: document_root+"rpc/remoteValidation/user/checkTypeUser.rpc",
						type: "post",
						data: {
						  serial_usr: function() {
							return $("#serial_usr").val();
						  }
						}
					}
			},
			txtPassword: {
				required:  {
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				minlength: {
					param:6,
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtRePassword: {
				required:  {
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				equalTo:  {
					param:"#txtPassword",
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtVisitsNumber:{
			   max:6
			},
			 hddValidateCities:{
				required: {
							depends: function(element) {
									var display = $("#extraInfo").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			},
			hddValidateCountries:{
				required: {
							depends: function(element) {
									var display = $("#extraInfo").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			}
		},
		messages: {
			txtFirstName: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
     		},
			txtLastName: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
     		},
			txtDocument: {
				remote: "La identificaci&oacute;n ingresada ya existe."
			},
		    txtMail: {
       			email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El correo ingresado ya existe."
     		},
			txtPassword:{
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtRePassword: {
				equalTo: "La confirmaci&oacute;n ha fallado."
			},
			selProfile: {
				remote: "Por favor seleccione un Perfil."
			},
			selStatus: {
				remote: "No Puede deshabilitar al Usuario porque tiene Comercializadores asociados."
			},
			txtBirthdate: {
                            date:"El campo 'Fecha de Naciemiento' debe tener el formato mm/dd/YYYY"
			},
			txtVisitsNumber: {
				max: "El m&aacute;ximo n&uacute;mero de visitas es 6"
			},
			txtPhone: {
				digits: "El m&aacute;ximo n&uacute;mero de visitas es 6"
			},
			txtCellphone: {
				digits: "El m&aacute;ximo n&uacute;mero de visitas es 6"
			}
   		},
		submitHandler: function(form) {
			$('#btnUpdate').attr('disabled', 'true');
			$('#btnUpdate').val('Espere por favor...');
			form.submit();
	   }
	});

    setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-20y');

	//Reset Password
	$("#changePassword").click(function () {
		$('#dialog').dialog('open');
	});

    $("#resetPassword").click(function () {
		if(confirm("\u00bfEst\u00e1 seguro que desea resetear la contrase\u00f1a?")){
			$('#hddNewPassword').val('NEW');
			$("#frmUpdateUser").submit();
		}
	});

	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
		loadCities(this.value);
		loadProfiles(this.value);
    });
	$('#selStatus').bind("change", function(e){
	   deactivateUser();
    });
	/*END CHANGE*/
});

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}


function loadDealers(countryID){
	if(countryID==""){
		countryID="stop";
	}
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val(''); 
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{serial_cou: countryID, dea_serial_dea: 'NULL', phone_sales: 'ALL'},function(){
            $('#selDealer').bind('change',function(){
              loadBranches(this.value);
            });
			if($('#selDealer').find('option').size()==2){
				loadBranches($('#selDealer').val());
			}
        });
}

function loadBranches(dealerID){
    var userID=$('#serial_usr').val();
		if(dealerID) {
            $("#branchContainer").load(document_root+"rpc/loadDealers.rpc",{dea_serial_dea: dealerID, phone_sales: 'ALL'},function(){
            $('#selBranch').change(function(){
                  loadCountryAccess(dealerID,type);
            });
            if($('#selBranch').find('option').size()==2){
                loadCountryAccess(dealerID,type);
            }
           });
		}
	}

function loadProfiles(countryID){
	if(countryID==""){
		$("#selProfile option[value='']").attr("selected",true);
	}
	$('#profileContainer').load(document_root+"rpc/loadProfiles.rpc",{serial_cou: countryID});
}


function loadCountryAccess(managerID){
	if(type=='MANAGER'){
		if(managerID!=""){
			$('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_man: managerID},function(){
				validateCheck("countries","hddValidateCountries");
				validateCheck("cities","hddValidateCities");
				$(":checkbox.countries").change(function(){
					showCities($(this).val());
					validateCheck("countries","hddValidateCountries");
					validateCheck("cities","hddValidateCities");
				});
				$(":checkbox.cities").change(function(){
					validateCheck("cities","hddValidateCities");
				});
			});
		}
		else{
			 $('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_man: 'NULL'});
		}
	}
	if(type=='DEALER'){
		  if(managerID!=""){
			$('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_dea: managerID},function(){
				$(":checkbox.countries").change(function(){
					validateCheck("countries","hddValidateCountries")
					validateCheck("cities","hddValidateCities");
					$(":checkbox.countries").change(function(){
						showCities($(this).val());
						validateCheck("countries","hddValidateCountries");
						validateCheck("cities","hddValidateCities");
					});
					$(":checkbox.cities").change(function(){
						validateCheck("cities","hddValidateCities");
					});
				});
			});
		}
		else{
			 $('#CountryAccess').load(document_root+"rpc/loadCountryAccess.rpc",{serial_man: 'NULL'});
		}
	}
}

function validateCheck(chkClass,hidden){
    var isAnySelected="";
        $(":checkbox."+chkClass).each(function(i){
            if($(this).attr("checked")){
               isAnySelected="1";
            }
        });
    $('#'+hidden).val(isAnySelected);
}

function loadManagersByCountry(countryID){
    var userID=$('#serial_usr').val();
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID, user: '1'},function(){
           $('#selManager').change(function(){
               userID=$('#selManager').val();
               loadCountryAccess(userID,type);
            });
            /*if($('#selManager').find('option').size()==2){
				alert($("#selProduct option:selected").val());
				alert(userID+"antes del countryAccess"+ type);
                loadCountryAccess(userID,type);
            }*/
	});
}

function extraData(aux){
    if(aux!='PLANETASSIST'){
        $('#extraData').load(document_root+"rpc/loadExtraInfo.rpc",{param: aux},function(){
                $('#selCountryEx').bind('change',function(){
                  if(aux=='DEALER'){
                    loadDealers(this.value);
                    type='DEALER';
                  }else if(aux=='MANAGER'){
                     loadManagersByCountry(this.value);
                     type='MANAGER';
                  }
                });
            },function(){
                $('#selDealer').bind('change',function(){
                  loadBranches(this.value);
                });
            }
        );
    }else{
        type='MANAGER';
        loadCountryAccess($('#ecuadorID').val());
    }
}

/*Country Access Functions*/
function checkAllCountries() {
	$('.countries').attr("checked",true);
	$('.country').show();
	$('.city').show();
	$('.city').attr("checked",true);
	$("#hddValidateCountries").val('1');
	$("#hddValidateCities").val('1');
	
	if(!($("#chkAllCountries").is(':checked'))){
		$('.countries').attr("checked",false);
		$('.country').hide();
		$('.city').attr("checked",false);
		$("#hddValidateCountries").val('');
		$("#hddValidateCities").val('');
	}
}

function checkAllCities(serial_cou) {
	if($("#chkAllCities"+serial_cou).is(':checked')){
		$(":checkbox.cities[country='"+serial_cou+"']").attr("checked",true);
		$("#hddValidateCities").val('1');
	}
	else{
		$(":checkbox.cities[value='"+serial_cou+"']").attr("checked",false);
		$("#hddValidateCities").val('');
	}
}

function showCities(serial_cou) {
	if($(":checkbox.countries[value='"+serial_cou+"']").is(':checked')){
		$("[country='"+serial_cou+"']").show();
	}
	else{
		$("[country='"+serial_cou+"']").hide();
		$(":checkbox.cities[value='"+serial_cou+"']").attr("checked",false);
		$("#chkAllCities"+serial_cou).attr("checked",false);
	}
}

function dateFormat(date){
	var formatDate=date.charAt(3)+'';
	for(var i=4;i<6;i++){
		formatDate+=date.charAt(i);
	}
	for(var i=0;i<3;i++){
		formatDate+=date.charAt(i);
	}
	for(var i=6;i<date.length;i++){
		formatDate+=date.charAt(i);
	}
	return formatDate;
}

function deactivateUser(){
	$('#deactivateUserDialog').load(document_root+"rpc/remoteValidation/user/deactivateUser.rpc",{serial_usr : $("#serial_usr").val()},function(){
		if($('#warningTitle').lengt>0){
			/*Dialog*/
			$("#deactivateUserDialog").dialog({
				bgiframe: true,
				autoOpen: false,
				height: 250,
				width: 550,
				modal: true,
				closeOnEscape: false,
				buttons: {
					'Aceptar': function() {
						/*VALIDATION*/
						$("#frmDeactivateUserDialog").validate({
								errorLabelContainer: "#alerts_DeactivateUserDialog",
								wrapper: "li",
								onfocusout: false,
								onkeyup: false
						});

						/*END VALIDATION*/
						$(this).dialog('close');
						$("#selStatus option[value='ACTIVE']").attr("selected",true);
					}
				  },
				  close: function() {
					  $("#selStatus option[value='ACTIVE']").attr("selected",true);
				  }
			});
			$("#deactivateUserDialog").dialog('open');
		}
	});
}

