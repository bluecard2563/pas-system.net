$(document).ready(function(){
    $("#frmUpdateAgreement").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,

        rules: {
            txtNameAgreement: {
                required: true,
                textOnly: true,
                maxlength: 100,

            },
            txtDiscountAgreement: {
                required: true,
                number: true,
                min: 1,
                max: 50
            },
            selStatusAgreement: {
                required: true
            }

        },
        messages: {
            txtNameAgreement: {
                textOnly: "En el campo Nombre de convenio solo se admite letras."
            },
            txtDiscountAgreement: {
                number: "En el campo Descuento solo se admiten números.",
                max: "El valor máximo permitido de descuento es de 50%.",
                min: "El valor mínimo permitido de descuento es de 1%."
            },
            selStatusAgreement: {
                textOnly: "El campo Estado es obligatorio."
            },

        }

    });
});