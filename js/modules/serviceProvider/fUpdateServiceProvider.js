// JavaScript Document
$(document).ready(function(){
    $("#frmUpdateServiceProvider").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtServProvName: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/serviceProvider/checkServiceNameAvailability.rpc",
					type: "post",
					data: {
						servProvCity: function() {return $("#servProvCity").val();},
						hdnServProvName: function() {return $("#hdnServProvName").val();}
					}
				}
			},
			servProvType: {
				required: true
			},
			txtServProvPhone_0: {
				required: true,
				digits: true
			},
			txtServProvEmail_0: {
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/serviceProvider/checkEmailAvailability.rpc",
					type: "post"
				}
			},
			txtServProvAddress: {
				required: true
			},
			txtServProvCost: {
				required: true,
				number:true
			},
			txtServProvTechCost: {
				required: true,
				number:true
			},
			txtServProvMedSimCost: {
				required: true,
				number:true
			},
			txtServProvMedComCost: {
				required: true,
				number:true
			}
        },
        messages: {
			selCountry: {
				required: 'El campo "Pa&iacute;s" es obligatorio.'
			},
			selCity: {
				required: 'El campo "Ciudad" es obligatorio.'
			},
			txtServProvName: {
				required: 'El campo "Nombre" es obligatorio.',
				remote: 'Ese nombre ya est&aacute; siendo usado por otro proveedor en el pa&iacute;s.'
			},
			servProvType: {
				required: 'El campo "Tipo de Servicio" es obligatorio.'
			},
			txtServProvPhone_0: {
				digits: 'El campo "Tel&eacute;fono Inicial" admite s\u00F3lo d\u00EDgitos'
			},
			txtServProvEmail: {
				required: 'El campo "Email" es obligatorio',
				email: 'Ingrese un email v&aacute;lido',
				remote: 'El email ya est&aacute; siendo usado por otro proveedor.'
			},
			txtServProvAddress: {
				required: 'El campo "Direcci&oacute;n" es obligatorio'
			},
			txtServProvCost: {
				required: 'El campo "Costo del Servicio" es obligatorio',
				number: 'Ingrese un n&uacute;mero v&aacute;lido'
			},
			txtServProvTechCost: {
				required: 'El campo "Costo de Asistencia T&eacute;cnica" es obligatorio',
				number: 'Ingrese un n&uacute;mero v&aacute;lido'
			},
			txtServProvMedSimCost: {
				required: 'El campo "Costo de Asistencia M&eacute;dica Simple" es obligatorio',
				number: 'Ingrese un n&uacute;mero v&aacute;lido'
			},
			txtServProvMedComCost: {
				required: 'El campo "Costo de Asistencia M&eacute;dica Compleja" es obligatorio',
				number: 'Ingrese un n&uacute;mero v&aacute;lido'
			}
        },
		submitHandler: function(form) {
			$('#btnAdd').attr('disabled', 'true');
			$('#btnAdd').val('Espere por favor...');
			form.submit();
	   }
    });

    $('#selCountry').bind('change',function(){
        loadCity(this.value);
    });

	$('#add_phone').css('cursor','pointer');
	$('#add_phone').bind('click', function(){
		var items=parseInt($('#phones_count').val());
		add_phone(items);
	});

	$('[id^="txtServProvPhone_"]').each(function(){
		var item=explode('_', this.id);
		item=item['1'];
		
		/* VALIDATION RULES */
		$(this).rules('add', {
			required: true,
			digits: true,
			messages: {
				digits: 'El campo "Tel&eacute;fono Adicional" admite s\u00F3lo d\u00EDgitos'
			}
		});

		/* VALIDATION RULES */

		/* REMOVE BUTTON */
		$('#remove_phone_'+item).css('cursor','pointer');
		$('#remove_phone_'+item).bind('click', function(){
			remove_phone(item);
		});
		/* REMOVE BUTTON */
	});

	$('#add_email').css('cursor','pointer');
	$('#add_email').bind('click', function(){
		var items=parseInt($('#email_count').val());
		add_email(items);
	});

	$('[id^="txtServProvEmail_"]').each(function(){
		var item=explode('_', this.id);
		item=item['1'];

		/* VALIDATION RULES */
		$(this).rules('add', {
			required: true,
			email: true,
			remote: {
				url: document_root+"rpc/remoteValidation/serviceProvider/checkEmailAvailability.rpc",
				type: "post"
			},
			messages: {
				required: 'El campo "Email Adicional" es obligatorio',
				email: 'Ingrese un email v&aacute;lido',
				remote: 'El email ya est&aacute; siendo usado por otro proveedor.'
			}
		});

		/* VALIDATION RULES */

		/* REMOVE BUTTON */
		$('#remove_email_'+item).css('cursor','pointer');
		$('#remove_email_'+item).bind('click', function(){
			remove_email(item);
		});
		/* REMOVE BUTTON */
	});

});

/*
 * @Name: loadCity
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCity(countryID){
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}

function showAssistanceCosts(type){
    var HTML='';
    if(type=='MEDICAL'){
        HTML= '<div class="span-24 last line">'
             +'<div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Simple:</div>'
             +'<div class="span-5 append-6 last">'
             +'<input type="text" name="txtServProvMedSimCost" id="txtServProvMedSimCost" title="El campo \"Costo de Asistencia M&eacute;dica Simple\" es obligatorio" />'
             +'</div>'
             +'</div>'
             +'<div class="span-24 last line">'
             +'<div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Compleja:</div>'
             +'<div class="span-5 append-6 last">'
             +'<input type="text" name="txtServProvMedComCost" id="txtServProvMedComCost" title="El campo \"Costo de Asistencia M&eacute;dica Compleja\" es obligatorio" />'
             +'</div>'
             +'</div>';
    }else if(type=='TECHNICAL'){
        HTML= '<div class="span-24 last line">'
             +'<div class="prepend-7 span-4 label">* Costo de Asistencia T&eacute;cnica:</div>'
             +'<div class="span-5 append-6 last">'
             +'<input type="text" name="txtServProvTechCost" id="txtServProvTechCost" title="El campo \"Costo de Asistencia T&eacute;cnica\" es obligatorio" />'
             +'</div>'
             +'</div>';
    }else if(type=='BOTH'){
        HTML= '<div class="span-24 last line">'
             +'<div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Simple:</div>'
             +'<div class="span-5 append-6 last">'
             +'<input type="text" name="txtServProvMedSimCost" id="txtServProvMedSimCost" title="El campo \"Costo de Asistencia M&eacute;dica Simple\" es obligatorio" />'
             +'</div>'
             +'</div>'
             +'<div class="span-24 last line">'
             +'<div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Compleja:</div>'
             +'<div class="span-5 append-6 last">'
             +'<input type="text" name="txtServProvMedComCost" id="txtServProvMedComCost" title="El campo \"Costo de Asistencia M&eacute;dica Compleja\" es obligatorio" />'
             +'</div>'
             +'</div>'
             +'<div class="span-24 last line">'
             +'<div class="prepend-7 span-4 label">* Costo de Asistencia T&eacute;cnica:</div>'
             +'<div class="span-5 append-6 last">'
             +'<input type="text" name="txtServProvTechCost" id="txtServProvTechCost" title="El campo \"Costo de Asistencia T&eacute;cnica\" es obligatorio" />'
             +'</div>'
             +'</div>';
    }
    $('#assistanceCosts').html(HTML);
}

function add_phone(current_items_count){
	$('#phones_count').val(++current_items_count);

	HTML= '<div class="span-7 last line" id="container_'+current_items_count+'">'
             +'<input type="text" name="txtServProvPhone[]" id="txtServProvPhone_'+current_items_count+'" title=\'El campo "Tel&eacute;fono Adicional" es obligatorio\' />'
			 +'&nbsp;<a id="remove_phone_'+current_items_count+'">Quitar</a>'
        +'</div>';

	$('#phones_container').append(HTML);

	/* VALIDATION RULES */
	$("#txtServProvPhone_"+current_items_count).rules('add', {
		required: true,
		digits: true,
		messages: {
			digits: 'El campo "Tel&eacute;fono Adicional" admite s\u00F3lo d\u00EDgitos'
		}
	});
	/* VALIDATION RULES */

	/* REMOVE BUTTON */
	$('#remove_phone_'+current_items_count).css('cursor','pointer');
	$('#remove_phone_'+current_items_count).bind('click', function(){
		remove_phone(current_items_count);
	});
	/* REMOVE BUTTON */
}

function add_email(current_items_count){
	$('#email_count').val(++current_items_count);

	HTML= '<div class="span-7 last line" id="email_container_'+current_items_count+'">'
             +'<input type="text" name="txtServProvEmail[]" id="txtServProvEmail_'+current_items_count+'" title=\'El campo "Email Adicional" es obligatorio\' />'
			 +'&nbsp;<a id="remove_email_'+current_items_count+'">Quitar</a>'
        +'</div>';

	$('#email_container').append(HTML);

	/* VALIDATION RULES */
	$("#txtServProvEmail_"+current_items_count).rules('add', {
		required: true,
			email: true,
			remote: {
				url: document_root+"rpc/remoteValidation/serviceProvider/checkEmailAvailability.rpc",
				type: "post"
			},
			messages: {
				required: 'El campo "Email Adicional" es obligatorio',
				email: 'Ingrese un email v&aacute;lido',
				remote: 'El email ya est&aacute; siendo usado por otro proveedor.'
			}
	});
	/* VALIDATION RULES */

	/* REMOVE BUTTON */
	$('#remove_email_'+current_items_count).css('cursor','pointer');
	$('#remove_email_'+current_items_count).bind('click', function(){
		remove_email(current_items_count);
	});
	/* REMOVE BUTTON */
}

function remove_phone(item_to_remove){
	$('#container_'+item_to_remove).remove();
}

function remove_email(item_to_remove){
	$('#email_container_'+item_to_remove).remove();
}