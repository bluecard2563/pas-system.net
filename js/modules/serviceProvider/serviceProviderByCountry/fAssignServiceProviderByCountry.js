// JavaScript Document
$(document).ready(function(){
    //SELECT ALL THE COUNTRIES IN THE dealersTo select before submit
    $("#frmAssignServiceProviderByCountry").bind('submit',function() {
        $('#btnInsert').attr('disabled', 'true');
		$('#btnInsert').val('Espere por favor...');
		$('#dealersTo *').attr('selected',true);
		

    });
    
    /*VALIDATION SECTION*/
    /*$("#frmAssignServiceProviderByCountry").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                    dealersTo:{
                            required: true
                    }
            }
    });*/
    /*END VALIDATION SECTION*/

    /*CHANGE OPTIONS*/
    $("#selZoneAssign").bind("change", function(e){
            loadCountry(this.value);
    });
    /*END CHANGE*/

    /*LIST SELECTOR*/
    $("#moveAll").click( function () {
        $("#dealersFrom option").each(function (i) {
            $("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" selected="selected" zone="'+$(this).attr('zone')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $("#removeAll").click( function () {
        $("#dealersTo option").each(function (i) {
            if($(this).attr('zone') == $('#dealersFrom').attr('zone')){
                $("#dealersFrom").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" selected="selected" zone="'+$(this).attr('zone')+'">'+$(this).text()+'</option>');
            }
            $(this).remove();
        });
    });

    $("#moveSelected").click( function () {
        $("#dealersFrom option:selected").each(function (i) {
            $("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" selected="selected" zone="'+$(this).attr('zone')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $("#removeSelected").click( function () {
        $("#dealersTo option:selected").each(function (i) {
            if($(this).attr('zone') == $('#dealersFrom').attr('zone')){
                $("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" selected="selected" zone="'+$(this).attr('zone')+'">'+$(this).text()+'</option>');
            }
            $(this).remove();
        });
    });
    /*END LIST SELECTOR*/
});

function loadCountry(zoneID){
    var countryList=new Array();
    if(zoneID != ""){
         $('#dealersTo option').each(function (i) {
            if($(this).attr('zone') == zoneID){
                countryList.push($(this).attr('value'));
            }
        });

        countryList=countryList.join(',');

        if(countryList!=''){
            $('#dealersFromContainer').load(document_root+"rpc/loadsServiceProviderCountries/loadServiceProviderCountries.rpc",{zone: zoneID, serial_countries: countryList});
        }else{
            $('#dealersFromContainer').load(document_root+"rpc/loadsServiceProviderCountries/loadServiceProviderCountries.rpc",{zone: zoneID});
        }
    }else{
        $('#dealersFromContainer').html('Seleccione una zona');
    }
}