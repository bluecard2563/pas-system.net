// JavaScript Document
$(document).ready(function(){
	$('#selZone').change(function(){
		loadCountries($('#selZone').val());
		loadManagers('');
		loadDealers('');
	});
	$('#selCountry').change(function(){loadManagers($('#selCountry').val());loadDealers(0);});
	$('#selManager').change(function(){loadDealers($('#selManager').val())});
	$('#selDealer').change(function(){loadBranches($('#selDealer').val())});
	$('#selBranch').change(function(){loadKitsData($('#selBranch').val())});
	loadKitsData($('#selBranch').val());

	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignKitByDealer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			selZone: {
				required: true
			},
                        selDealer: {
				required: true
			},
                        txtAmount: {
				required: true,
				digits: true,
                                min:1
			},
			selAction: {
				required: true
			}
		},
		messages: {
			txtAmount: {
				digits: 'El campo "Cantidad" admite s&oacute;lo n&uacute;meros.',
                min:"La cantidad debe ser al menos 1"
			}
   		},
		submitHandler: function(form) {
			$("#alerts").html('');
			$("#alerts").css('display','none');

			if($("#selAction").val()=='RETURNED' && parseInt($("#txtAmount").val())>parseInt($("#hdnKitsTotal").val()))
				{
				  $("#alerts").append("<li><label class='error_validation'>No se puede quitar una cantidad mayor a la de kits entregados al comercializador.</label></li>");
				  $("#alerts").css('display','block');
				}
			else{
					form.submit();
				}
		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/
});

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
		$('#kitsContainer').empty();
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
		$('#selCountry').change(function(){
			loadManagers($('#selCountry').val())
		});
		if($('#selCountry').find('option').size()==2){
			loadManagers($('#selCountry').val());
		}
	});
}

function loadManagers(countryID){
	if(countryID==""){
		$("#selManager option[value='']").attr("selected",true);
		$('#kitsContainer').empty();$('#kitsContainer').empty();
	}
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
		$('#selManager').change(function(){
			loadDealers($('#selManager').val())
		});
		if($('#selManager').find('option').size()==2){
			loadDealers($('#selManager').val());
		}
	});
}

function loadDealers(ManagerID){
	if(ManagerID==""){
		ManagerID='stop';
        $('#kitsContainer').empty();
	}
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{serial_mbc: ManagerID, dea_serial_dea: 'NULL'},function(){
		$('#selDealer').change(function(){
			loadBranches($('#selDealer').val())
		});
		if($('#selDealer').find('option').size()==2){
			loadBranches($('#selDealer').val());
		}
	});
}
function loadBranches(DealerID){
	if(DealerID==""){
		DealerID='stop';
		$('#kitsContainer').empty();
	}
	$('#branchContainer').load(document_root+"rpc/loadDealers.rpc",{dea_serial_dea: DealerID},function(){
		$('#selBranch').change(function(){
			loadKitsData($('#selBranch').val())
		});
		if($('#selBranch').find('option').size()==2){
			loadKitsData($('#selBranch').val());
		}
	});
}
function loadKitsData(DealerID){
	if(DealerID==""){
		$('#kitsContainer').empty();
	}else{
		$('#kitsContainer').load(document_root+"rpc/loadKitsData.rpc",{serial_dea: DealerID});
	}
}