// JavaScript Document
$(document).ready(function(){
	$('#selZone').bind("change",function(){
		loadCountries($('#selZone').val());
		loadManagers('');
		loadDealers('');
	});
        
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmConsultKitDetails").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			selZone: {
				required: true
			},
                        selDealer: {
				required: true
			}
		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/
});

function loadManagers(countryID){
	if(countryID==""){
		$("#selManager option[value='']").attr("selected",true);
		$('#kitsContainer').empty();$('#kitsContainer').empty();
	}
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
		$('#selManager').bind("change",function(){
			loadDealers($('#selManager').val())
		});
	});
}

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
		$('#kitsContainer').empty();
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
		$('#selCountry').bind("change",function(){
			loadManagers($('#selCountry').val())
		});
	});
}
function loadDealers(ManagerID){
	if(ManagerID==""){
		ManagerID='stop';
		$('#kitsContainer').empty();
	}
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{serial_mbc: ManagerID, dea_serial_dea: 'NULL'},function(){
		$('#selDealer').bind("change",function(){
			loadBranches($('#selDealer').val())
		});
	});
}
function loadBranches(DealerID){
	if(DealerID==""){
		DealerID='stop';
		$('#kitsContainer').empty();
	}
	$('#branchContainer').load(document_root+"rpc/loadDealers.rpc",{dea_serial_dea: DealerID},function(){
		$('#selBranch').bind("change",function(){
			loadKitTotal($('#selBranch').val())
		});
		if($('#selBranch').find('option').size()==2){
			loadKitTotal($('#selBranch').val());
		}
	});
}
function loadKitTotal(DealerID){
	if(DealerID==""){
		$('#kitsContainer').empty();
		$('#detailsContainer').empty();
	}else{
		$('#kitsContainer').load(document_root+"rpc/loadKitTotal.rpc",{serial_dea: DealerID},function(){
			setDefaultCalendar($("#txtFromDate"),'-100y','+0d','-0y');
			setDefaultCalendar($("#txtToDate"),'-100y','+0d','-0y');
			$('#btnDetails').bind("click",function(){
				loadKitDetails($('#hdnKitByDealerID').val());
			});
			$("#txtFromDate").bind("change", function() {
				if($("#txtToDate").val()){
					if(!validateDate(this.value,$("#txtToDate").val())) {
						alert('La fecha "desde" debe ser mayor a la fecha "hasta."')
						$(this).val('');
					}
				}
			});
			$("#txtToDate").bind("change", function() {
				if($("#txtFromDate").val()){
					if(!validateDate($("#txtFromDate").val(),this.value)) {
						alert('La fecha "hasta" debe ser menor a la fecha "desde."');
						$(this).val('');
					}
				}
			});
		});
	}
}

function validateDate(date_ini, date_end){
	if(date_ini>=date_end) {
		return false;
	}
	return true;
}

function loadKitDetails(KitID){
	if(KitID==""){
		$('#detailsContainer').empty();
	}else{
		$('#detailsContainer').load(document_root+"rpc/loadKitDetails.rpc",{serial_kbd: KitID, fromDate: $('#txtFromDate').val(), toDate: $('#txtToDate').val()});
	}
}