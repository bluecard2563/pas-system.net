//Java Script
$(document).ready(function(){
    $("#selZone").bind("change", function(e){
	      loadCountry(this.value);
    });

	$('#txtAmountStock').bind('change',function(){
		if($(this).val()%5!=0){
			$('#txtAmountStock').val('');
			alert('El stock a crearse debe ser un m\u00FAltiplo de 5.');
		}
	});

	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignStockCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selType: {
				required: true
			},
			txtAmountStock: {
				required: true,
				digits: true,
                                min:1
			},
			selZone: {
				required: true
			},
			selCountry:  {
				required: true
			},
			selManager:  {
				required: true
			}
		},
		messages: {
			txtAmountStock: {
       			digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Cantidad'.",
				min:"La cantidad debe ser almenos 1"
			}
   		},
		submitHandler: function(form) {
			$("#alerts").html('');
			$("#alerts").css('display','none');
			
			var amount = $('#txtAmountStock').val();
   			var stock_type = $('#selType').val();
   			var remote_id = $('#selManager').val();

			if(parseInt($("#"+$("#selType").val()).val())>=parseInt($("#txtAmountStock").val())){
			 	$.ajax({
	   				url: document_root+ 'rpc/loadsStock/getPossibleRanges.rpc.php',
	   				type: "POST",
	   				data: { 
	   					'stock_to': 'manager',
	   					'amount': amount,
	   					'stock_type': stock_type,
	   					'remote_id': remote_id
	   				},
	   				success: function(data){
	   					var format_data = json_parse(data);
	   					var ranges='Los rangos a asignar son: \n';
	   					
	   					if($.isArray(format_data)){
	   						for(item in format_data){
	   			   				ranges+=" Desde: "+format_data[item]['from']+" Hasta: "+format_data[item]['to']+" \n";
	   			   			}
	   						ranges+='Desea continuar?';
	   						
	   						if(confirm(ranges)){
	   							$('#btnRegistrar').attr('disabled', 'true');
	   	   						$('#btnRegistrar').val('Espere por favor...');
	   	   						form.submit();
	   						} 
	   					}else{
	   						alert('No existe suficiente stock para asignar.');
	   					}
	   				}
	   			});
			}else{
				$("#alerts").append("<li><label class='error_validation'>La cantidad a asignarse no puede ser mayor al stock disponible.</label></li>");
				$("#alerts").css('display','block');
			}
		}
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/
});

function loadCountry(zoneID){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
			serial_zon: zoneID
	},function(){
		$('#selCountry').bind('change',function(){
			loadManagerByCountry($(this).val());
		});
	});
}

function loadManagerByCountry(serial_cou){
	 $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou});
}
