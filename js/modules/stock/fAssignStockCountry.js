//Java Script

function loadCountry(zoneID){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
			serial_zon: zoneID
		});
}


$(document).ready(function(){
    $("#selZone").bind("change", function(e){
	      loadCountry(this.value);
    });

	$('#txtAmountStock').bind('change',function(){
		if($(this).val()%5!=0){
			$('#txtAmountStock').val('');
			alert('El stock a crearse debe ser un m\u00FAltiplo de 5.');
		}
	});

	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignStockCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selType: {
				required: true
			},
			txtAmountStock: {
				required: true,
				digits: true,
                                min:1
			},
			selZone: {
				required: true
			},
			selCountry:  {
				required: true
			}
		},
		messages: {
			txtAmountStock: {
       			digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Cantidad'.",
                        min:"La cantidad debe ser almenos 1"
                        }
   		},
                submitHandler: function(form) {
                    $("#alerts").html('');
                    $("#alerts").css('display','none');

                    if(parseInt($("#"+$("#selType").val()).val())>=parseInt($("#txtAmountStock").val()))
                        {
						$('#btnRegistrar').attr('disabled', 'true');
						$('#btnRegistrar').val('Espere por favor...');
						  form.submit();
                        }
                    else{
                        $("#alerts").append("<li><label class='error_validation'>La cantidad a asignarse no puede ser mayor al stock disponible.</label></li>");
                        $("#alerts").css('display','block');
                        }
                }
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/
});
