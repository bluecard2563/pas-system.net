//Java Script
$(document).ready(function(){
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmNewStock").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selType: {
				required: true
			},
			txtAmountStock: {
				required: true,
				digits: true
			}
		},
		messages: {
			txtAmountStock: {
				digits: "S&oacute;lo se aceptan d&iacute;gitos en el campo 'Cantidad'."
			}	
   		},
		submitHandler: function(form) {
   			var amount = $('#txtAmountStock').val();
   			var stock_type = $('#selType').val();
   			
   			$.ajax({
   				url: document_root+ 'rpc/loadsStock/getPossibleRanges.rpc.php',
   				type: "POST",
   				data: { 
   					'stock_to': 'general',
   					'amount': amount,
   					'stock_type': stock_type
   				},
   				success: function(data){
   					var format_data = json_parse(data);
   					var ranges='Los rangos a asignar son: \n';
   					
   					if($.isArray(format_data)){
   						for(item in format_data){
   			   				ranges+=" Desde: "+format_data[item]['from']+" Hasta: "+format_data[item]['to']+" \n";
   			   			}
   						ranges+='Desea continuar?';
   						
   						if(confirm(ranges)){
   							$('#btnRegistrar').attr('disabled', 'true');
   	   						$('#btnRegistrar').val('Espere por favor...');
   	   						form.submit();
   						} 
   					}else{
   						alert('No existe suficiente stock para asignar.');
   					}
   				}
   			});
	   }
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/

	$('#txtAmountStock').bind('change',function(){
		if($(this).val()%5!=0){
			$('#txtAmountStock').val('');
			alert('El stock a crearse debe ser un m\u00FAltiplo de 5.');
		}
	});
});
