//Java Script
$(document).ready(function(){
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignStockSeller").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selType: {
				required: true
			},
			txtAmountStock: {
				required: true,
				digits: true,
				min:1
			},
			selDealer: {
				required: true
			},
			selCountry:  {
				required: true
			},
			selManager:  {
				required: true
			},
			selBranch:  {
				required: true
			},
			selSeller:  {
				required: true
			}
		},
		messages: {
			txtAmountStock: {
				digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Cantidad'.",
				min:"La cantidad debe ser almenos 1"
			}
		},
		submitHandler: function(form) {
			$("#alerts").html('');
			$("#alerts").css('display','none');
			
			var amount = $('#txtAmountStock').val();
   			var stock_type = $('#selType').val();
   			var remote_id = $('#selSeller').val();

			if(parseInt($("#"+$("#selType").val()).val())>=parseInt($("#txtAmountStock").val())){
			 	$.ajax({
	   				url: document_root+ 'rpc/loadsStock/getPossibleRanges.rpc.php',
	   				type: "POST",
	   				data: { 
	   					'stock_to': 'seller',
	   					'amount': amount,
	   					'stock_type': stock_type,
	   					'remote_id': remote_id
	   				},
	   				success: function(data){
	   					var format_data = json_parse(data);
	   					var ranges='Los rangos a asignar son: \n';
	   					
	   					if($.isArray(format_data)){
	   						for(item in format_data){
	   			   				ranges+=" Desde: "+format_data[item]['from']+" Hasta: "+format_data[item]['to']+" \n";
	   			   			}
	   						ranges+='Desea continuar?';
	   						
	   						if(confirm(ranges)){
	   							$('#btnRegistrar').attr('disabled', 'true');
	   	   						$('#btnRegistrar').val('Espere por favor...');
	   	   						form.submit();
	   						} 
	   					}else{
	   						alert('No existe suficiente stock para asignar.');
	   					}
	   				}
	   			});
			}else{
				$("#alerts").append("<li><label class='error_validation'>La cantidad a asignarse no puede ser mayor al stock disponible.</label></li>");
				$("#alerts").css('display','block');
			}
		}
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/

	$("#selCountry").bind("change", function(e){
		loadManagerByCountry(this.value);
	});

	$('#txtAmountStock').bind('change',function(){
		if($(this).val()%5!=0){
			$('#txtAmountStock').val('');
			alert('El stock a crearse debe ser un m\u00FAltiplo de 5.');
		}
	});
});

function loadAvailable(branchID){
	$('#availableContainer').load(document_root+"rpc/loadAvailable.rpc",{
		id: branchID,
		type:'branch'
	});
}
function loadDealers(serial_mbc){
	$('#availableContainer').html('');
	if(serial_mbc=='') serial_mbc='stop';
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selSeller').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{
		serial_mbc: serial_mbc,
		dea_serial_dea: 'NULL'
	},function(){
		$("#selDealer").bind("change", function(e){
			loadBranches(this.value);
		});
		if($("#selDealer option").size() ==2){
			loadBranches($("#selDealer").val());
		}
	});
}
function loadBranches(dealerID){
	$('#availableContainer').html('');
	if(dealerID=='') dealerID='stop';
	$('#selSeller').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$("#branchContainer").load(document_root+"rpc/loadDealers.rpc",{
		dea_serial_dea: dealerID
	},function(){
		$("#selBranch").bind("change", function(e){
			loadSellers(this.value);
			loadAvailable(this.value);
		});
		if($("#selBranch option").size() ==2){
			loadSellers($("#selBranch").val());
			loadAvailable($("#selBranch").val());
		}

	});
}

function loadSellers(branchID){
	if(branchID==''){
		$('#selSeller').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}else{
		$("#sellerContainer").load(document_root+"rpc/loadSellers.rpc",{
			serial_dea: branchID
		});
	}
}

function loadManagerByCountry(serial_cou){
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",
	{
		serial_cou: serial_cou
	},
	function(){
		$('#selManager').bind('change',function(){
			loadDealers($(this).val());
		});

		if($('#selManager option').size()==2){
			loadDealers($('#selManager').val());
		}
	});
}