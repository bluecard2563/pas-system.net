// JavaScript Document
$(document).ready(function(){
    $("#frmNewServiceTranslation").validate({
        errorLabelContainer: "#alerts_t",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                selLanguage: {
                  required: true
                },
                txtSblName: {
                        required: true,
                        remote: {
                                url: document_root+"rpc/remoteValidation/services/checkTranslationName.rpc",
                                type: "post",
                                data: {
                                  serial_lang: function() {
                                    return $("#selLanguage").val();
                                  }
                                }
                        }
                }
        },
        messages: {
                txtSblName: {
                    remote: "La  nueva traducci&oacute;n ingresada ya existe en el sistema."
                }
        }
    });

     $("#dialog").dialog({
        bgiframe: true,
        autoOpen: false,
        height: 370,
        width: 450,
        modal: true,
        buttons: {
                'Actualizar': function() {
                      $("#frmUpdateServiceTranslation").validate({
                            errorLabelContainer: "#alerts_dialog",
                            wrapper: "li",
                            onfocusout: false,
                            onkeyup: false,
                            rules: {
								txtSblName: {
									required: true,
									remote: {
										url: document_root+"rpc/remoteValidation/services/checkService.rpc",
										type: "post",
										data: {
										  serial_lang: function() {
											return $("#dialog #serial_lang").val();
										  },
										  serial_ser: function() {
											  return $("#hdnServiceID").val();
										  }
										}
									}
								}
                            },
                            messages: {
								txtSblName: {
									remote: "La actualizar traducci&oacute;n ingresada ya existe en el sistema."
								}
                            }
                        });

                       if($('#frmUpdateServiceTranslation').valid()){
                            $('#frmUpdateServiceTranslation').submit();
                       }
                    },
                Cancelar: function() {
                    $('#alerts_dialog').css('display','none');
                    $(this).dialog('close');
                }
        },close: function() {
            $('select.select').show();
        }
    });

    $("#newInfo").css("display","none");

    $("#btnInsert").bind("click", function(e){
        createNew();
    });
});

function loadFormData(itemID){
	$('#dialog #language').html($('#name_lang_'+itemID).html());
	$('#dialog #txtSblName').val($('#name_sbl_'+itemID).html());
	$('#dialog #txtSblDesc').val($('#description_sbl_'+itemID).html());
    $('#dialog #serial_lang').val($('#serial_lang_'+itemID).val());
    $('#dialog #serial_sbl').val(itemID);
}

function createNew(){
    if($("#frmNewServiceTranslation").valid()){
      $("#frmNewServiceTranslation").submit();
    }
}