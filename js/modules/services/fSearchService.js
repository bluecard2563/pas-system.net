// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmSearchService").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                txtSblName: {
                    required: true
                },
                selLanguage: {
                    required: true
                }
        }
    });
    /*END VALIDATION*/
        
    /*AUTOCOMPLETER*/
    $("#txtSblName").autocomplete(document_root+"rpc/autocompleters/loadServices.rpc",{
        extraParams: {
            serial_lang: function(){ return $("#selLanguage").val()}
        },
        max: 10,
        scroll: false,
        matchContains:true,
        minChars: 3,
        formatItem: function(row) {
             if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
        }
    }).result(function(event, row) {
        $("#hdnSblID").val(row[1]);
    });
    /*END AUTOCOMPLETER*/

    $("#selLanguage").change (function(){
        $('#txtSblName').flushCache();
        $("#txtSblName").val('');
        $('#hdnSblID').val('');
    });
});