// JavaScript Document
$(document).ready(function(){
    $("#frmNewService").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                name_sbl:{
                    required: true,
                    remote: {
                            url:document_root+"rpc/remoteValidation/services/checkService.rpc",
                            type: "post"
                    }
                },
                fee_type_ser:{
                    required: true
                },
                price_sbc:{
                    required:true,
                    number:true,
                    greaterOrEqualTo:"#cost_sbc"
                },
                cost_sbc:{
                    required:true,
                    number:true,
                    lessOrEqualTo:"#price_sbc"
                }

        },
        messages: {
                name_sbl: {
                    required: 'El campo nombre es requerido.',
                    remote: 'El nombre del servicio ya existe en el sistema.'
                },
                fee_type_ser:{
                    required: 'Seleccion el tipo de precio a aplicarse.'
                },
                price_sbc:{
                    required:'El campo precio es requerido.',
                    number:'Ingrese un n&uacute;mero v&aacute;lido para el campo precio.',
                    greaterOrEqualTo: 'El campo precio debe ser mayor o igual al costo'
                },
                cost_sbc:{
                    required:'El campo costo es requerido.',
                    number:'Ingrese un n&uacute;mero v&aacute;lido para el campo costo.',
                    lessOrEqualTo: 'El campo costo debe ser menor o igual al precio'
                }
        }
    });
});