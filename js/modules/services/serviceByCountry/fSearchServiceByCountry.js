// JavaScript Document
var pager;

$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmSearchService").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                selLanguage: {
                    required: true
                }
        }
    });
    /*END VALIDATION*/

    $("#selLanguage").change (function(){
        $('#hdnSblID').val('');
		loadServices();
    });
});

function loadServices(){
	var language=$("#selLanguage").val();
	if(language!=''){
		$('#serviceContainer').load(document_root+'rpc/loadServices/loadServices.rpc', {serial_lang: language}, function(){
			if($('#servicesTable').length>0){
				$('[id^="rdService_"]').each(function(){
					$(this).bind('click',function(){
						if($(this).is(':checked')){
							$('#hdnSblID').val($(this).val());
						}
					});
				});

				pager = new Pager('servicesTable', 15 , 'Siguiente', 'Anterior');
				pager.init(10); //Max Pages
				pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); //Starting page
				
				$('#hdnSblID').rules('add',{
					required: true
				});
			}
		});
	}
}