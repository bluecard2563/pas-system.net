// JavaScript Document
var selectedCountries = new Array();
var deletedCountries = new Array();

$(document).ready(function(){
    //VALIDATION
    $("#frmPBC").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                'selZone0': {
                        required: true
                },
                'selCountry0': {
                        required: true
                },
                'coverage_sbc_0': {
                        number: true
                },
                'price_sbc_0': {
                        required:true,
                        number: true,
                        greaterOrEqualTo:"#cost_sbc_0"

                },
                'cost_sbc_0': {
                        required:true,
                        number: true,
                        lessOrEqualTo:"#price_sbc_0"
                }

        },
        messages:{
			'coverage_sbc_0': {
					number: 'El campo cobertura debe ser num&eacute;rico'
			},
			'price_sbc_0': {
					required:'El campo precio es requerido',
					number: 'El campo precio debe ser num&eacute;rico',
					greaterOrEqualTo: 'El campo precio debe ser mayor o igual al costo'
			},
			'cost_sbc_0': {
					required:'El campo costo es requerido',
					number: 'El campo costo debe ser num&eacute;rico',
					lessOrEqualTo: 'El campo costo debe ser menor o igual al precio'
                }
        }
    });
    //END VALIDATION
    //INITIALIZATION IF THERE ARE ASSIGNED COUNTRIES TO THE PRODUCT
    $('.cou_arr').each(function(){
        //ADD ASSIGNED COUNTRIES
        selectedCountries[this.id] = this.value;
        listSelectorButtons(this.id);
        //ADD THE ONCHANGE EVENT TO THE ASSIGNED COUNTRIES
        var count = this.id;
        $('#selZone'+this.id).bind('change',function(){loadCountries(this.value,count)});
        //ADD THE VALIDATION FOR EACH ASSIGNED COUNTRY
        $('#zoneContainer'+this.id+' #selZone'+this.id).rules("add", {
             required: true
        });
        $('#countryContainer'+this.id+' #selCountry'+this.id).rules("add", {
            required: true
        });


       /* $('#coverage_sbc_'+this.id).rules("add", {
            number: true,
            messages:{
                number: 'El campo covertura debe ser num&eacute;rico'
            }
        });
        $('#price_sbc_'+this.id).rules("add", {
            required: true,
            number: true,
            greaterOrEqualTo:"#cost_sbc_"+this.id,
            messages:{
                required: 'El campo precio es requerido',
                number: 'El campo precio debe ser num&eacute;rico',
                greaterOrEqualTo: 'El campo precio debe ser mayor o igual al costo'
            }
        });
        $('#cost_sbc_'+this.id).rules("add", {
            required: true,
            number: true,
            lessOrEqualTo:"#price_sbc_"+this.id,
            messages:{
                required:'El campo costo es requerido',
                number: 'El campo costo debe ser num&eacute;rico',
                lessOrEqualTo: 'El campo costo debe ser menor o igual al precio'

            }
        });*/
        


    });
    //ADD A NEW COUNTRY TO BE ASSIGNED
    var selZone = $('#zoneContainer0').html();
    $('#btnAddCountry').bind('click', function(){
        var count = parseInt($('#countriesCount').val())+1;
        var delete_country = '<div class="prepend-22 span-2 last"><a href="#" onclick="deleteCountry('+count+'); return false;" id="delete'+count+'">Eliminar</a></div>';
        var zone = '<div class="span-2 label">* Zona:</div><div class="span-4" id="zoneContainer'+count+'">'+selZone+'</div>';
        var country = '<div class="span-2 label">* Pa&iacute;s:</div><div class="span-5" id="countryContainer'+count+'"><select name="countries['+count+'][selCountry]" id="selCountry'+count+'" title=\'El campo "Pa&iacute;s" es obligatorio\'><option value="">- Seleccione -</option></select></div>';
        var tax = '<div class="span-3 label">* Impuestos:</div><div class="span-7 last line" id="taxContainer'+count+'">Seleccione un pa&iacute;s</div>';
        var price = '<div class="span-24 last" id="priceContainer'+count+'">Seleccione un pa&iacute;s</div>';
        var manager_dealers = '<div class="span-24 line label">Asignar comercializadores</div>';
        manager_dealers += '<div class="span-24 line" >';
        manager_dealers += '<div class="span-4 label">Representantes:</div>';
        manager_dealers += '<div class="span-5" id="managerContainer'+count+'">Seleccione un pa&iacute;s</div>';
        manager_dealers += '<div class="span-4 label">Comercializadores: </div>';
        manager_dealers += '<div class="span-10 append-1 last" id="dealersContainer'+count+'">';
        manager_dealers += '<div class="span-4"><select multiple id="dealersTo" name="countries['+count+'][dealersTo][]" class="selectMultiple span-4 last"></select></div>';
        manager_dealers += '<div class="span-2 last buttonPane">';
        manager_dealers += '<input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>';
        manager_dealers += '<input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>';
        manager_dealers += '<input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>';
        manager_dealers += '<input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>';
        manager_dealers += '</div>';
        manager_dealers += '<div class="span-4 last line" id="dealersFromContainer">Seleccione un representante</div>';
        manager_dealers += '</div>';
        manager_dealers += '</div>';
        
        if(count%2 == 0){
            row_class='even-row';
        }else{
            row_class='odd-row';
        }
        //WRITE THE NEW COUNTRY SECTION
        $('#countriesContainer').append('<div id="country'+count+'" class="span-24 last '+row_class+'"><hr>'+delete_country+'<div class="span-24 last line">'+zone+country+tax+price+'</div>'+manager_dealers+'</div>');

        //CHANGING THE ID NAMES AND EVENTS FOR THE NEW ELEMENTS
        $('#countriesContainer #country'+count+' #zoneContainer'+count).attr('class', 'span-5');
        $('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone0').attr('name','countries['+count+'][selZone]');
        $('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone0').attr('id','selZone'+count);
        $('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone'+count).removeAttr('onchange');
        //$('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone'+count).attr('onclick','loadCountries(this.value,'+count+')');
        $('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone'+count).bind('change',function(){loadCountries(this.value,count)});
        $('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone'+count+' option[value=""]').attr("selected",true);

        //VALIDATION SECTION  FOR THE NEW COUNTRY
        $('#countriesContainer #country'+count+' #zoneContainer'+count+' #selZone'+count).rules("add", {
             required: true
        });
        $('#countriesContainer #country'+count+' #countryContainer'+count+' #selCountry'+count).rules("add", {
            required: true
        });
        
       
        listSelectorButtons(count);
        //ADD THE COUNTRIES COUNTER
        $('#countriesCount').val(count);
    });

    $("#frmPBC").bind('submit',function() {
        $('#dealersTo *').attr('selected',true);
    });
});
//LOAD THE COUNTRY SELECT BASED ON THE ZONE SELECTED
function loadCountries(zoneId,count){
    $('#countryContainer'+count).load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
		$('#countryContainer'+count+' #selCountry').attr('name','countries['+count+'][selCountry]');
        $('#countryContainer'+count+' #selCountry').attr('id','selCountry'+count);
        $('#countryContainer'+count+' #selCountry'+count).bind('change',function(){addSelectedCountry(this.value,count);loadTaxes(this.value,count);loadManagersByCountry(this.value,count);loadPrices(this.value,$("#serial_ser").val(),count);});
        selectedCountries[count] = '';
		addSelectedCountry($('#countryContainer'+count+' #selCountry'+count).val(),count);
        loadTaxes($('#countryContainer'+count+' #selCountry'+count).val(),count);
        loadManagersByCountry($('#countryContainer'+count+' #selCountry'+count).val(),count);
		loadPrices(this.value,$('#serial_ser').val(),count);
    });
}
//LOAD THE TAXES CHECKS BASED ON THE COUNTRY SELECTED
function loadTaxes(countryId,count){
    if(countryId != ''){
        $('#taxContainer'+count).load(document_root+"rpc/loadTaxesChecks.rpc",{serial_cou: countryId, count: count});
    }else{
        $('#taxContainer'+count).html('Seleccione un pa\u00EDs');
        $('#dealersContainer'+count+' #dealersFromContainer').html('Seleccione un representante');
    }
}
function loadPrices(countryId,serial_ser,count){

    if(countryId != ''){
        $('#priceContainer'+count).load(document_root+"rpc/loadPricesServices.rpc",{serial_cou: countryId, count: count, serial_ser: serial_ser}, function() {
			$('#coverage_sbc_'+count).rules("add", {
				number: true,
				messages:{
					number: 'El campo cobertura debe ser num&eacute;rico'
				}
			});
			$('#price_sbc_'+count).rules("add", {
				required: true,
				number: true,
				greaterOrEqualTo:"#cost_sbc_"+count,
				messages:{
					required: 'El campo precio es requerido',
					number: 'El campo precio debe ser num&eacute;rico',
					greaterOrEqualTo: 'El campo precio debe ser mayor o igual al costo'
				}
			});
			$('#cost_sbc_'+count).rules("add", {
				required: true,
				number: true,
				lessOrEqualTo:"#price_sbc_"+count,
				messages:{
					required:'El campo costo es requerido',
					number: 'El campo costo debe ser num&eacute;rico',
					lessOrEqualTo: 'El campo costo debe ser menor o igual al precio'

				}
			});
		});
    }else{
        $('#priceContainer'+count).html('Seleccione un pa\u00EDs');
        $('#dealersContainer'+count+' #dealersFromContainer').html('Seleccione un representante');
    }
}

//LOAD THE MANAGERS SELECT BASED ON THE COUNTRY SELECTED
function loadManagersByCountry(countryId, count){
    if(countryId != ''){
        $('#managerContainer'+count).load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryId},function(){
            $('#managerContainer'+count+' #selManager').attr('name','countries['+count+'][selManager]');
            $('#managerContainer'+count+' #selManager').attr('id','selManager'+count);
            $('#selManager'+count).removeAttr('onchange');
            $('#selManager'+count).bind('change',function(){loadDealersByManager(this.value,count);});
            loadDealersByManager($('#selManager'+count).val(),count);
        });
    }else{
        $('#managerContainer'+count).html('Seleccione un pa\u00EDs');
    }
    $('#dealersContainer'+count+' #dealersTo option').each(function (i) {
        $(this).remove();
    });
    $('#dealersContainer'+count+' #dealersFromContainer').html('Seleccione un representante');
}
//LOAD THE DEALERS SELECT BASED ON THE MANAGER SELECTED
function loadDealersByManager(managerId,count){
     var dealerList=new Array();
     if(managerId != ""){
         $('#dealersContainer'+count+' #dealersTo option').each(function (i) {
            if($(this).attr('manager') == managerId){
                dealerList.push($(this).attr('value'));
            }
        });

        dealerList=dealerList.join(',');

        if(dealerList!=''){
            $('#dealersContainer'+count+' #dealersFromContainer').load(document_root+"rpc/loadDealersByManager.rpc",{serial_mbc: managerId, count: count, serial_dealers: dealerList});
        }else{
            $('#dealersContainer'+count+' #dealersFromContainer').load(document_root+"rpc/loadDealersByManager.rpc",{serial_mbc: managerId, count: count});
        }
        //listSelectorButtons(count);
        
     }else{
         $('#dealersContainer'+count+' #dealersFromContainer').html('Seleccione un representante');
     }
}
//ACTIVE OR INACTIVE THE COUNTRY
function statusCountry(count){
   if($('#countryStatus'+count).val() == 'ACTIVE'){
        $('.activeLink'+count).html('Inactivo').css('color','#FF0000');
        $('#countryStatus'+count).val('INACTIVE');

    }else{
        $('.activeLink'+count).html('Activo').css('color','#008000');
        $('#countryStatus'+count).val('ACTIVE');
    }
}
//DELETE A COUNTRY THAT WAS JUST ADDED BUT NOT STORED IN DB YET
function deleteCountry(count){
    $('#country'+count).remove();
    selectedCountries[count] = '';
}
//FUNCTION
function addSelectedCountry(countryId,count){
    var exist = 0;
    if(countryId != ''){
        for(i = 0; i<selectedCountries.length; i++){
            if(selectedCountries[i] == countryId){
                alert('Este pa\u00EDs ya ha sido asignado.');
                $('#countryContainer'+count+' #selCountry'+count+' option[value=""]').attr("selected",true);
                exist = 1;
                break;
            }
        }
        if(exist == 0){
            selectedCountries[count] = countryId;
        }else{
          selectedCountries[count] = '';
        }
    }else{
        selectedCountries[count] = '';
    }
}

function listSelectorButtons(count){
    /*LIST SELECTOR*/
    $('#dealersContainer'+count+' #moveAll').bind('click', function () {
        $('#dealersContainer'+count+' #dealersFrom option').each(function (i) {
            $('#dealersContainer'+count+' #dealersTo').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $('#dealersContainer'+count+' #removeAll').bind('click', function () {
        $('#dealersContainer'+count+' #dealersTo option').each(function (i) {
            //if($(this).attr('used') == 0){
                if($(this).attr('manager') == $('#dealersContainer'+count+' #dealersFrom').attr('manager')){
                    $('#dealersContainer'+count+' #dealersFrom').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
                }
                $(this).remove();
            //}
        });
    });

    $('#dealersContainer'+count+' #moveSelected').bind('click', function () {
        $('#dealersContainer'+count+' #dealersFrom option:selected').each(function (i) {
            $('#dealersContainer'+count+' #dealersTo').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $('#dealersContainer'+count+' #removeSelected').bind('click', function () {
        $('#dealersContainer'+count+' #dealersTo option:selected').each(function (i) {
            //if($(this).attr('used') == 0){
                if($(this).attr('manager') == $('#dealersContainer'+count+' #dealersFrom').attr('manager')){
                    $('#dealersContainer'+count+' #dealersFrom').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
                }
                $(this).remove();
            //}else{
            //    alert('Este comercializador ya ha realizado un venta.');
           // }
        });
    });
    /*END LIST SELECTOR*/
}