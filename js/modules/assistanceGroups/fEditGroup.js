$(document).ready(function(){
	$("#selCountry").bind("change", function(e){
		loadGroups(this.value);
	});
	/*VALIDATION SECTION*/
	$("#frmEditGroup").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtGroupName: {
				required: true,
				remote:{
					url: document_root+"rpc/remoteValidation/assistanceGroup/checkGroupName.rpc",
					type: "POST",
					data: {
					  serial_cou: function() {
						return $('#hdnCountry').val();
					  },
					  txtGroupName:function() {
						return $('#txtGroupName').val();
					  },
					  groupId:function(){
						  return $('#hdnGroup').val();
					  }
					}
				}
			}
		},
		messages: {
			txtGroupName: {
				required: 'El campo "Nombre" es obligatorio.',
				remote: "Ya existe un grupo con el nombre ingresado en el pa&iacute;s seleccionado'."
			}
   		}
	});
	/*END VALIDATION SECTION*/
});
