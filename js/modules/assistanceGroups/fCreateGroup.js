//Java Script
$(document).ready(function(){
	$("#selCountry").bind("change", function(e){
		$("#nameValidate").val($("#nameValidate").val()+1);
	});
	$("#txtGroupsName").bind("change", function(e){
		$("#nameValidate").val($("#nameValidate").val()+1);
	});
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmNewGroup").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			txtGroupsName: {
				required: true
			},
			nameValidate:{
				remote:{
					url: document_root+"rpc/remoteValidation/assistanceGroup/checkGroupName.rpc",
					type: "POST",
					data: {
					  serial_cou: function() {
						return $('#selCountry').val();
					  },
					  txtGroupName:function() {
						return $('#txtGroupsName').val();
					  }
					}
				}
			}
		},
		messages: {
			nameValidate: {
				remote: "Ya existe un grupo con el nombre ingresado en el pa&iacute;s seleccionado'."
			}	
   		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/
});
