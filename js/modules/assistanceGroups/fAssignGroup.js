$(document).ready(function(){
	$("#selCountry").bind("change", function(e){
		loadGroups(this.value);
	});
	$("#frmAssignGroup").bind('submit',function() {
        $('#usersTo *').attr('selected',true);
    });

	$("#radio_dealer").bind("click", function(e){
		$('#usersFrom').find('option').remove();
		$('#selZoneAssignManager').val('');
		$('#selCountryAssignManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		
		$("#dealerOptContainer").show('slow');
		$("#managerContainer").hide('slow');
	});

	$("#radio_manager").bind("click", function(e){
		$('#usersFrom').find('option').remove();
		$('#selZoneAssign').val('');
		$('#selCountryAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCityAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

		$("#managerContainer").show('slow');
		$("#dealerOptContainer").hide('slow');
	});

	$("#radio_planet").bind("click", function(e){
		$('#usersFrom').find('option').remove();
		$('#selZoneAssign').val('');
		$('#selCountryAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCityAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

		$('#selZoneAssignManager').val('');
		$('#selCountryAssignManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

		$("#dealerOptContainer").hide('slow');
		$("#managerContainer").hide('slow');
		loadUsers();
	});

	$("#selZoneAssign").bind("change", function(e){
		loadCountry(this.value,'DEALER');
	});

	$("#selZoneAssignManager").bind("change", function(e){
		loadCountry(this.value,'MANAGER');
	});
	//load users for first time
	loadUsers();
	//add actions for control buttons
	addControlListeners();
	/*VALIDATION SECTION*/
	$("#frmAssignGroup").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selGroup: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
});


//function to change a user to available.
function userToAvailable(serial_usr){
	 $.ajax({
       type: "POST",
       url: document_root+"rpc/loadsAssistanceGroups/userToAvailable.rpc",
       data: ({serial_usr : serial_usr})
     });
}

//function to load the groups based on a selected country
function loadGroups(serial_cou){
	$('#groupContainer').load(document_root+"rpc/loadsAssistanceGroups/loadAssistGroupByCountry.rpc",
							  {serial_cou: serial_cou},
		function(){
			$('#selGroup').bind("change",function(e){
				loadAlreadyAssign(this.value);
			});
			if($('#selGroup option').size() <=2) {
				loadAlreadyAssign($('#selGroup').val());
			}
		});
}

//function to load the user who are already assigned to a group
function loadAlreadyAssign(groupId){
	$('#assignedUsersContainer').load(document_root+"rpc/loadsAssistanceGroups/loadAlreadyAssignedUsers.rpc",
							  {serial_asg: groupId},
		function(){
			loadUsers();
		});
}

//function to load the user based on the ratio option, users from planetassist or users by branch
function loadUsers(){
	var selUsers="";
	var serial_mbc;
	
	for(var i=0; i < $("#usersTo *").length; i++) {
		if(i==0) {
			selUsers+=$("#usersTo").children()[i].value;
		}
		else{
			selUsers+=","+$("#usersTo").children()[i].value;
		}
	}
	if($("#radio_planet").is(':checked')){
		type='planet';
		serial_dea='';
		serial_mbc='';
	}else if($("#radio_manager").is(':checked')){
		type='manager';
		serial_dea='';
		serial_mbc=$('#selManager').val();
	}else{
		type='dealer';
		serial_mbc='';
		serial_dea=$('#selBranch').val();
	}
	//alert('tupo: '+type+' Serial_dea: '+serial_dea+' serial_mbc: '+serial_mbc);
	
	$("#usersFromContainer").load(document_root+"rpc/loadsAssistanceGroups/loadUsersForGroups.rpc",{type: type, selUsers: selUsers,serial_dea:serial_dea, serial_mbc: serial_mbc});
}

//add listeners for multiple select controls.
function addControlListeners(){
	/*LIST SELECTOR*/
	$("#moveAll").click( function () {
		$("#usersFrom option").each(function (i) {
			$("#usersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" selected="selected">'+$(this).text()+'</option>');
			$(this).remove();
		});
	});

	$("#removeAll").click( function () {
		$("#usersTo option").each(function (i) {
			userToAvailable(this.value);
			$(this).remove();
		});
		loadUsers();
	});

	$("#moveSelected").click( function () {
		$("#usersFrom option:selected").each(function (i) {
			$("#usersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" selected="selected">'+$(this).text()+'</option>');
			$(this).remove();
		});
	});

	$("#removeSelected").click( function () {
		$("#usersTo option:selected").each(function (i) {
			userToAvailable(this.value);
			$(this).remove();
		});
		loadUsers();
	});
}

//function to load de coutris by zone
function loadCountry(zoneID,type){
	var container;
	var selectID;
	if(type=='DEALER'){
		$('#selCityAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		container='countryAssignContainer';
		selectID='Assign';
	}else{
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		container='countryAssignContainerManager';
		selectID='AssignManager';
	}

	$('#'+container).load(
		document_root+"rpc/loadCountries.rpc",
		{serial_zon: zoneID,serial_sel:selectID},
		function(){
			if(type=='DEALER'){
				$('#selCountryAssign').bind("change",function(e){
					loadCity(this.value);
				});
				if($('#selCountryAssign option').size() <=2) {
					loadCity($('#selCountryAssign').val());
				}
			}else{
				$('#selCountryAssignManager').bind("change",function(e){
					loadManagers(this.value);
				});
				
				if($('#selCountryAssignManager option').size() <=2) {
					loadManagers($('#selCountryAssignManager').val());
				}
			}			
		});
}

//function to load cities by country
function loadCity(countryID){
	$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#cityAssignContainer').load(document_root+"rpc/loadCities.rpc",
								 {serial_cou: countryID,serial_sel:'Assign'},
								 function(){
									 $("#selCityAssign").bind("change", function(e){
										loadComissionist(this.value);
									});
									if($('#selCityAssign option').size() <=2){
										loadComissionist($("#selCityAssign").val());
									}
								});
}

//load the responsables (commisionist) by city
function loadComissionist(cityID){
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#comissionistAssignContainer').load(document_root+"rpc/loadComissionistByCity.rpc",
	 {serial_cit: cityID, serial_sel: 'Assign'},
	 function(){
		$("#selComissionistAssign").bind("change", function(e){
			loadDealer(this.value);
		});
			if($('#selComissionistAssign option').size() <=2){
				loadDealer($("#selComissionistAssign").val());
			}
	 });
}

//load dealers by responsable (commisionist)
function loadDealer(userID){
	cityID=$('#selCityAssign').val();
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#dealerContainer').load(document_root+"rpc/loadsAssistanceGroups/loadDealersByComissionistCity.rpc",
		{serial_cit: cityID, serial_usr: userID},
		 function(){
		$("#selDealer").bind("change", function(e){
			loadBranch(this.value);
		});
			if($('#selDealer option').size() <=2){
				loadBranch($("#selDealer").val());
			}
	 });
}

//load branches by dealer by comissionist and city
function loadBranch(dealerId){
	cityID=$('#selCityAssign').val();
	userID=$('#selComissionistAssign').val();
	$('#branchContainer').load(document_root+"rpc/loadsAssistanceGroups/loadBranchByDealerComissionistCity.rpc",
		{serial_cit: cityID, serial_usr: userID,serial_dea:dealerId},
		 function(){
		$("#selBranch").bind("change", function(e){
			loadUsers();
		});
			if($('#selBranch option').size() <=2){
				loadUsers();
			}
	 });

}

//Load all managers by country
function loadManagers(countryID){
	$('#managerByCountryContainer').load(document_root+"rpc/loadManagersByCountry.rpc",
	{
		serial_cou: countryID
	},function(){
		$('#selManager').bind('change',function(){
			loadUsers();
		});

		if($('#selManager option').size()){
			loadUsers();
		}
	});
}