//function to load the groups based on a selected country
function loadGroups(serial_cou){
	$('#groupContainer').load(document_root+"rpc/loadsAssistanceGroups/loadAssistGroupByCountry.rpc",
							  {serial_cou: serial_cou});
}

$(document).ready(function(){
	$("#selCountry").bind("change", function(e){
		loadGroups(this.value);
	});
	/*VALIDATION SECTION*/
	$("#frmChooseGroup").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selGroup: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
});

