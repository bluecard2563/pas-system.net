// JavaScript Document
$(document).ready(function(){
        /*VALIDATION SECTION*/
	$("#frmSearchBenefit").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameBenefit: {
                            required: true
			},
                        selLanguage: {
                            required: true
                        }
		}
	});
	/*END VALIDATION*/
        
	/*AUTOCOMPLETER*/
	$("#txtNameBenefit").autocomplete(document_root+"rpc/autocompleters/loadBenefits.rpc",{
		extraParams: {
			serial_lang: function(){ return $("#selLanguage").val()}
		},
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 3,
  		formatItem: function(row) {
			if(row[0]!='0'){
				return row[0];
			}else{
				return "No existe el beneficio ingresado.";
			}
  		}
	}).result(function(event, row) {
		if(row[0]!='0'){
			$("#hdnBenefitID").val(row[1]);
		}else{
			$("#hdnBenefitID").val('');
			$("#txtNameBenefit").val('');
		}
	});
	/*END AUTOCOMPLETER*/

        $("#selLanguage").change (function(){
            $('#txtNameBenefit').flushCache();
            $("#txtNameBenefit").val('');
            $('#hdnBenefitID').val('');
	});
});