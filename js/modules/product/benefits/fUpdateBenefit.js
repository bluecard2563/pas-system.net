// JavaScript Document
$(document).ready(function(){
	$("#frmUpdateBenefit").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			rdStatus: {
				required: true
                        }
		}
	});

         $("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 410,
		modal: true,
		buttons: {
			'Actualizar': function() {
				  $("#frmUpdateTranslation").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtBenefit: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
									type: "post",
									data: {
									  serial_lang: function() {
										return $("#serial_lang").val();
									  },
									  serial_ben: function() {
										return $("#hdnBenefitID").val();
									  }
									}
								}
							},
                                                        txtWeightBenefit: {
                                                            digits:true
                                                        }
						},
						messages: {
							txtBenefit: {
								remote: "La traducci&oacute;n ingresada ya existe en el sistema."
							},
                                                        txtWeightBenefit: {
                                                                digits: "La 'Priorizaci&oacute;n' debe contener valores num&eacute;ricos enteros"
                                                        }
						}
					});

				   if($('#frmUpdateTranslation').valid()){
					$('#frmUpdateTranslation').submit();
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});

        $("#newInfo").css("display","none");
        $("[name='rdStatus']").bind("click", function(e){
            changeStatus(this.value);
        });

        $("#btnInsert").bind("click", function(e){
            createNew();
        });
});

function loadFormData(itemID){
	$('#language').html($('#name_lang_'+itemID).html());
        $('#txtWeightBenefit').val($('#weight_ben_'+itemID).html());
	$('#txtBenefit').val($('#description_bbl_'+itemID).html());
        $('#serial_lang').val($('#serial_lang_'+itemID).val());
        $('#serial_bbl').val(itemID);
        $('#serial_ben').val($('#hdnBenefitID').val());
}

function changeStatus(status){
    if(status=='INACTIVE'){
        if(confirm('Est\u00E1 seguro que desea deshabilitar este beneficio?')){
          $("#frmUpdateBenefit").submit();
        }
    }else{
        $("#frmUpdateBenefit").submit();
    }
}

function createNew(){
    if($("#frmNewTranslation").valid()){
      $("#frmNewTranslation").submit();
    }
}