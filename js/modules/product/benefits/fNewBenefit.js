// JavaScript Document
$(document).ready(function(){
	$("#frmNewBenefit").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
                        selCategories: {
				required: true
			},
			txtDescBenefit: {
				required: true,
				remote: {
					url:document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
					type: "post"
				}
			},
                        txtWeightBenefit: {
                            digits:true
                        }
		},
		messages: {
			txtDescBenefit: {
				remote: 'El beneficio ya existe en el sistema'
                                
			},
                        txtWeightBenefit: {
                                digits: "La 'Priorizaci&oacute;n' debe contener valores num&eacute;ricos enteros"
                        }
		}
	});
});