var pager;
$(document).ready(function(){
	 if($('#conditionsTable').length>0){
		pager = new Pager('conditionsTable', 10 , 'Siguiente', 'Anterior');
		pager.init(10); //Max Pages
		pager.showPageNav('pager', 'navPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page

		$("#frmGConditions").validate({
			errorLabelContainer: "#alerts",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				hdnSelectOne: {
					required: true
				}
			}
		});
		$("[name='rdConditions']").click(function(){
			$('#hdnSelectOne').val($(this).val());
		});
	 }
});