$(document).ready(function(){
	$("#frmNewCondition").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCondition: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/product/generalCondition/checkGeneralCondition.rpc",
					type: "post",
					data: {
						serial_lang:function(){
							return $("#hdnLanguage").val()
						}
					}
				}
			},
			general_condition_planet: {
				required: true,
				accept: "pdf",
				notEqualTo: '#general_condition_ecuador'
			},
			general_condition_ecuador: {
				required: true,
				accept: "pdf",
				notEqualTo: '#general_condition_planet'
			}

		},
		messages: {
			txtCondition: {
				remote: 'La condici&oacute;n ya existe en el sistema.'
			},
			general_condition_planet: {
				required: "El campo 'Archivo Planet' es obligatorio.",
				accept: "Seleccione un archivo PDF para el campo 'Archivo Planet'",
				notEqualTo: "Ingrese un archivo diferente para el campo 'Archivo Planet'"
			},
			general_condition_ecuador: {
				required: "El campo 'Archivo Ecuador' es obligatorio.",
				accept: "Seleccione un archivo PDF 'Archivo Ecuador'",
				notEqualTo: "Ingrese un archivo diferente para el campo 'Archivo Ecuador'"
			}
		}
	});
});