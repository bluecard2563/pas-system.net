$(document).ready(function(){
	$("#frmNewCondition").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCondition: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/product/generalCondition/checkGeneralCondition.rpc",
					type: "post",
					data: {
						serial_lang:function(){
							return $("#selLanguage").val()
						},
						serial_gcn: function(){
							return $("#hdnSerial_gcn").val()
						}
					}
				}
			},
			selStatus: {
				required: true
			},
			selLanguage:{
				required: true
			}

		},
		messages: {
			txtCondition: {
				remote: 'La condici&oacute;n ingresada ya existe en el sistema.'
			}
		}
	});

	$("#updateFile").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 530,
		modal: true,
		buttons: {
			'Guardar': function() {
				$("#frmReplaceFile").validate({
					errorLabelContainer: "#alerts_replacement",
					wrapper: "li",
					onfocusout: false,
					onkeyup: false,
					rules: {
						general_condition_planet: {
							required: true,
							accept: 'pdf'
						}
					},
					messages: {
						general_condition_planet: {
							accept: 'S&oacute;lo se admiten archivos PDF.'
						}
					}
				});

				if($('#frmReplaceFile').valid()){
					$('#frmReplaceFile').submit();
				}
			},
			Cancelar: function() {
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('#hdnSerial_glc').val("");
			$('#general_condition_planet').val("");
			$('#alerts_replacement').css("display","none");
		}
	});

	$("#addFile").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 530,
		modal: true,
		buttons: {
			'Guardar': function() {
				$("#frmAddFile").validate({
					errorLabelContainer: "#alerts_add",
					wrapper: "li",
					onfocusout: false,
					onkeyup: false,
					rules: {
						general_condition_planet: {
							required: true,
							accept: 'pdf'
						},
						selLanguageTranslation:{
							required: true
						}
					},
					messages: {
						general_condition_planet: {
							accept: 'S&oacute;lo se admiten archivos PDF.'
						}
					}
				});

				if($('#frmAddFile').valid()){
					$('#frmAddFile').submit();
				}
			},
			Cancelar: function() {
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('#hdnSerial_glc').val("");
			$('#selLanguageTranslation').val("");
			$('#general_condition_planet').val("");
			$('#alerts_add').css("display","none");
		}
	});

	$('#selLanguage').bind('change',function(){
		$('#alerts').css("display","none");
		if($(this).val()!=''){
			$('#file_translations').load(document_root+'rpc/loadProduct/generalConditions/loadTranslationsInfo.rpc',
				{
					serial_gcn: $('#hdnSerial_gcn').val(),
					serial_lang: $(this).val()
				},function(){
					if($("#txtCondition").length>0){
						$("#txtCondition").rules("add", {
							required: true,
							remote: {
								url: document_root+"rpc/remoteValidation/product/generalCondition/checkGeneralCondition.rpc",
								type: "post",
								data: {
									serial_lang:function(){
										return $("#selLanguage").val()
									},
									serial_gcn: function(){
										return $("#hdnSerial_gcn").val()
									}
								}
							},
							messages: {
								remote: "La condici&oacute;n ingresada ya existe en el sistema para el idioma seleccionado."
							}
						});
					}

					$('[id^="update_"]').each(function(){
						$(this).bind('click',function(){
							$('#hdnSerial_glc').val($(this).attr('element_id'));
							load_remainingLanguages($(this).attr('element_id'),'updateFile');
						});
					})

					$('[id^="translate_"]').each(function(){
						$(this).bind('click',function(){
							$('#hdnAddSerial_glc').val($(this).attr('element_id'));
							load_remainingLanguages($(this).attr('element_id'),'addFile');
						});
					});
				});
		}else{
			$('#file_translations').html('');
		}
	});

	$('[id^="update_"]').each(function(){
		$(this).bind('click',function(){
			$('#hdnSerial_glc').val($(this).attr('element_id'));
			load_remainingLanguages($(this).attr('element_id'),'updateFile');
		});
	})

	$('[id^="translate_"]').each(function(){
		$(this).bind('click',function(){
			$('#hdnAddSerial_glc').val($(this).attr('element_id'));
			load_remainingLanguages($(this).attr('element_id'),'addFile');
		});
	});
});

function load_remainingLanguages(serial_glc, dialog_name){
	var serial_gcn=$('#hdnSerial_gcn').val();
	
	$('#remaining_languages').load(document_root+'rpc/loadProduct/generalConditions/loadRemainingLanguages.rpc',
		{ serial_glc: serial_glc,
		  serial_gcn: serial_gcn
	    },
		function(){
			if($('#selLanguageTranslation option').size() > 1){
				$("#"+dialog_name).dialog('open');
			}else{
				alert('Ya han sido ingresadas todas las traducciones para este archivo.');
			}
		});
}