// JavaScript Document
	$(document).ready(function(){

	$("#flights_pro").change(function(){
			flights($("#flights_pro").val());
	});


	function flights(flightNumber){
		if(!$("#masive_pro").is(":checked")){
			if(flightNumber!='1' && flightNumber!=""){
				showRelative();
				showSpouse();
				$(".multipleFlights").attr("disabled",true);
				$(".multipleFlights").css("color","#aaa");
				$(":text.multipleFlights").val("");
				$(":checkbox:checked.multipleFlights").attr("checked",false);
				$(".singleFlight").attr("disabled",false);
				$(".singleFlight").css("color","#000");
			} else {
				showRelative();
				showSpouse();
				$(".multipleFlights").attr("disabled",false);
				$(".multipleFlights").css("color","#000");
				$("#calculate_pro").attr("checked",true);
				$(".singleFlight").attr("disabled",true);
				$(".singleFlight").css("color","#aaa");
				$(":text.singleFlight").val("");
				$(":checkbox:checked.singleFlight").attr("checked",false);

			}
		} else {
			enableMasive(true);
		}
	}


$("#masive_pro").bind("click",function(){enableMasive($("#masive_pro").is(":checked"))});
	function enableMasive(status){
		$(".multipleFlights").attr("disabled",false);
		$(".multipleFlights").css("color","#000");
		$(".singleFlight").attr("disabled",false);
		$(".singleFlight").css("color","#000");

		if(status){
			$(".masive").css("color","#aaa");
			$(".masive").attr("disabled",true);
			$(".masiveBtn").css("color","#aaa");
			$(".masiveBtn").attr("disabled",true);
			$(":checkbox:checked.masive").attr("checked",false);
			$(":text.masive").val("");
			$(".masiveOn").attr("disabled",true);
			$(":checkbox.masiveOn").attr("checked",true);
			$(".divItem:not(:first)").remove();
		}
		else{
			$(".masive").css("color","#000");
			$(".masive").attr("disabled",false);
			$(".masiveBtn").css("color","#2E6E9E");
			$(".masiveBtn").attr("disabled",false);
			flights($("#flights_pro").val());
			$(".masiveOn").attr("disabled",false);
			$(":checkbox.masiveOn").attr("checked",false);
			$(":text.masive").val("");
		}
	}

	$("#frmNewProduct").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			serial_tpp: {
				required: true
			},
			price_pro: {
				required: true,
				number:true
			},
			txtName_pro: {
				required: true,				
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/product/checkProduct.rpc",
					type: "post"
				}
			},
			image_pro:{
				accept: "jpeg|jpg|gif"
			},
			max_extras_pro:{
				digits:true,
				greaterOrEqualTo: "#min_extras_pro",
				min: 0
			},
			children_pro:{
				digits:true,
				lessOrEqualTo: "#max_extras_pro",
				min: 0

			},
			adults_pro:{
				digits:true,
				lessOrEqualTo: "#max_extras_pro",
				min: 0

			},
			flights_pro:{
				required : true
			},
			generalCondition: {
				required :true
			},
			percentage_spouse_pxc: {
				required : {
					depends: "!#spouse_pro:checked"
				},
				number: true
			},
			percentage_extras_pxc: {
				required : {
					depends: "!#relative_pro:checked"
				},
				number: true
			},
			percentage_children_pxc: {
				required : {
					depends: "!#relative_pro:checked"
				},
				number: true
			},
			aditional_day_pxc: {
				required : {
					depends: "!#calculate_pro:checked"
				},
				number: true
			},
			repeated: {
				equal: 0
			}
		},

		messages: {
			serial_tpp: {
				required: "Seleccione un Tipo de Producto"
			},
							price_pro: {
				required: "Ingrese un valor en el campo precio",
									number: "Ingrese un n&uacute;mero v&aacute;lido en el campo precio"
			},
			txtName_pro: {
				required: "El campo Nombre es obligatorio.",				
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: 'Ese Nombre ya fue asignado a otro Producto.'
			},
			max_extras_pro:{
				digits: 'El campo M&aacute;ximo Acompa&ntilde;antes debe ser un n&uacute;mero',
									greaterOrEqualTo: 'El campo M&aacute;ximo Acompa&ntilde;antes debe ser mayor a M&iacute;nimo Acompa&ntilde;antes',
									min: "El campo M&aacute;ximo Acompa&ntilde;antes debe ser mayor a 0"
			},
			children_pro:{
				digits: 'El campo Menores gratis debe ser un n&uacute;mero',
									lessOrEqualTo: 'El campo Menores gratis debe ser menor o igual a M&aacute;ximo Acompa&ntilde;antes',
									min: "El campo Menores gratis debe ser mayor a 0"

			},
			adults_pro:{
				digits: 'El campo Adultos gratis debe ser un n&uacute;mero',
									lessOrEqualTo: 'El campo Adultos gratis debe ser menor o igual a M&aacute;ximo Acompa&ntilde;antes',
									min: "El campo Adultos gratis debe ser mayor a 0"

			},
			flights_pro:{
				required : "Seleccione el n&uacute;mero de Viajes por Per&iacute;odo"
			},
			generalCondition: {
				required : "Debe Seleccionar un archivo de Condiciones Generales"
			},
							image_pro:{
								accept: "Debe seleccionar un archivo JPG, JPEG o GIF"
							},
							percentage_spouse_pxc: {
								required : "Ingrese un valor en el campo %C&oacute;nyuge",
								number: "Ingrese un n&uacute;mero v&aacute;lido en el campo %C&oacute;nyuge"
							},
							percentage_extras_pxc: {
								required : "Ingrese un valor en el campo %Extras",
								number: "Ingrese un n&uacute;mero v&aacute;lido en el campo %Extras"
							},
							percentage_children_pxc: {
								required : "Ingrese un valor en el campo %Hijos",
								number: "Ingrese un n&uacute;mero v&aacute;lido en el campo %Hijos"
							},
							aditional_day_pxc:{
								required : "Ingrese un valor en el campo Precio d&iacute;a adicional",
								number: "Ingrese un n&uacute;mero v&aacute;lido en el campo Precio d&iacute;a adicional"
							},
							repeated: {
								equal: "Existen d&iacute;as de duraci&oacute;n repetidos "
							}
		},
		submitHandler:function(form){
			$("#show_price_pro").attr("disabled",false);
			form.submit();
		}
	});

$("[id^='status_bpt_']").each(function (i) {
		var id=this.id.replace('status_bpt_','');
		$("#serial_ben_"+id).rules("add", {
			required:{
				depends:"!#"+this.id+":checked"
			},
			messages: {
 			  	required: "El campo Condiciones es obligatorio para el beneficio "+$("#description_ben_"+id).html()+"."
 			}
		});

		$("#price_bpt_"+id).rules("add", {
			required:{
				depends:"!#"+this.id+":checked"
			},
			number:{
				depends:"!#"+this.id+":checked"
			},
			messages: {
 			  	required: "El campo Cobertura es obligatorio para el beneficio "+$("#description_ben_"+id).html()+".",
				number: "El campo Cobertura para el beneficio "+$("#description_ben_"+id).html()+" solo acepta n&uacute;meros."
 			}
		});
		$("#restriction_price_bpt_"+id).rules("add", {
			required:function(element) {
                                    if($("#status_bpt_"+id).is(":checked") && $("#serial_rst_"+id).val()!="")
                                        return true;
                                    else
                                        return false;
                        },
			number:{
				depends:"!#"+this.id+":checked"
			},
			messages: {
 			  	required: "El campo Valor Restr. es obligatorio para el beneficio "+$("#description_ben_"+id).html()+".",
				number: "El campo Valor Restr. para el beneficio "+$("#description_ben_"+id).html()+" solo acepta n&uacute;meros."
 			}
		});
		$("#serial_rst_"+id).rules("add", {
			required:function(element) {
                                    if($("#status_bpt_"+id).is(":checked") && $("#restriction_price_bpt_"+id).val()!="")
                                        return true;
                                    else
                                        return false;
                        },
			messages: {
 			  	required: "El campo Restricci&oacute;n es obligatorio para el beneficio "+$("#description_ben_"+id).html()+"."
 			}
		});
	});

	 $("#serial_tpp").change(function(){

		$("#min_duration_pro").val("");
		$("#max_duration_pro").val("");
		$("#min_extras_pro").val("");
		$("#max_extras_pro").val("");
		$("#children_pro ").val("");

		$("#has_comision_pro").attr("checked",false);
		$("#relative_pro").attr("checked",false);
		$("#emission_country_pro").attr("checked",false);
		$("#third_party_register_pro").attr("checked",false);
		$("#schengen_pro").attr("checked",false);
		$("#in_web_pro").attr("checked",false);
		$("#spouse_pro").attr("checked",false);
		$("#residence_country_pro").attr("checked",false);
		$("#masive_pro").attr("checked",false);

		$("[id^='status_bpt_']").each(function (i) {
					var id=this.id.replace('status_bpt_','');
					$("#status_bpt_"+id).attr("checked",false);
					$("#serial_rst_"+id).val("");
					$("#price_bpt_"+id).val("");
					$("#restriction_price_bpt_"+id).val("");
					$("#serial_ben_"+id).val("");
		});

		$.getJSON(document_root+"rpc/loadProductsPage1.rpc", {serial_tpp:$("#serial_tpp").val()}, function(data){

			$("#txtName_pro").val(data['page1'][0].name_ptl);
			$("#txtDescription_pro").val(data['page1'][0].description_ptl);
			$("#hdnImage_pro").val(data['page1'][0].image_tpp);
			$("#img_pro").attr("src",document_root+"img/products/"+data['page1'][0].image_tpp);

			if(data['page1'][0].max_extras_tpp!="0"){
				$("#max_extras_pro").val(data['page1'][0].max_extras_tpp);
			}
			if(data['page1'][0].children_tpp!="0"){
				$("#children_pro").val(data['page1'][0].children_tpp);
			}
			if(data['page1'][0].adults_tpp!="0"){
				$("#adults_pro").val(data['page1'][0].adults_tpp);
			}

			$("#flights_pro").val(data['page1'][0].flights_tpp);
			/******************************************************************/
			/*if($("#flights_pro").val()!='1' && $("#flights_pro").val()!="" ){
				$(".multipleFlights").attr("disabled",true);
				$(".multipleFlights").css("color","#aaa");
				$(":text.multipleFlights").val("");
				$(":checkbox:checked.multipleFlights").attr("checked",false);

				$(".singleFlight").attr("disabled",false);
				$(".singleFlight").css("color","#000");

				showRelative();
				showSpouse();
			}
			else{
				$(".multipleFlights").attr("disabled",false);
				$(".multipleFlights").css("color","#000");

				$(".singleFlight").attr("disabled",true);
				$(".singleFlight").css("color","#aaa");
				$(":text.singleFlight").val("");
				$(":checkbox:checked.singleFlight").attr("checked",false);

				showRelative();
				showSpouse();

			}*/

			/******************************************************************/
				if(data['page1'][0].has_comision_tpp=="YES")
					$("#has_comision_pro").attr("checked",data['page1'][0].has_comision_tpp);
				if(data['page1'][0].relative_tpp=="YES")
					$("#relative_pro").attr("checked",data['page1'][0].relative_tpp);
				showRelative();
				if(data['page1'][0].emission_country_tpp=="YES")
					$("#emission_country_pro").attr("checked",data['page1'][0].emission_country_tpp);
				if(data['page1'][0].third_party_register_tpp=="YES")
					$("#third_party_register_pro").attr("checked",data['page1'][0].third_party_register_tpp);
				if(data['page1'][0].schengen_tpp=="YES")
					$("#schengen_pro").attr("checked",data['page1'][0].schengen_tpp);
				if(data['page1'][0].in_web_tpp=="YES")
					$("#in_web_pro").attr("checked",data['page1'][0].in_web_tpp);
				if(data['page1'][0].spouse_tpp=="YES")
					$("#spouse_pro").attr("checked",data['page1'][0].spouse_tpp);
				showSpouse();
				if(data['page1'][0].residence_country_tpp=="YES")
					$("#residence_country_pro").attr("checked",data['page1'][0].residence_country_tpp);
				if(data['page1'][0].masive_tpp=="YES")
					$("#masive_pro").attr("checked",data['page1'][0].masive_tpp);
				if(data['page1'][0].senior_tpp=="YES")
					$("#senior_pro").attr("checked",data['page1'][0].senior_tpp);
				if(data['page1'][0].destination_restricted_tpp=="YES")
					$("#destination_restricted_pro").attr("checked",data['page1'][0].destination_restricted_tpp);
				if(data['page1'][0].extras_restricted_to_tpp != "") {
					$("#selExtrasRestricted").attr("disabled", false);
					$("#selExtrasRestricted").val(data['page1'][0].extras_restricted_to_tpp);
				}
				$("[id^='status_bpt_']").each(function (i) {
					var id=this.id.replace('status_bpt_','');

					if(data['page2']!=null){
						if(data['page2'][id]!=null){
							$("#status_bpt_"+id).attr("checked",true);
							if(data['page2'][id].serial_rst!=null){
								$("#serial_rst_"+id).val(data['page2'][id].serial_rst);
							}
							$("#price_bpt_"+id).val(data['page2'][id].price_bpt);
							$("#restriction_price_bpt_"+id).val(data['page2'][id].restriction_price_bpt);
							$("#serial_ben_"+id).val(data['page2'][id].serial_con);
						}
					}
				});

				if(data['page3']!=null){
					$(":radio[name='generalCondition']").each(function (i) {
						if($(this).val()==data['page3'][0].serial_gcn){
							$(this).attr("checked",true);
						}
					});
				}

				enableMasive($("#masive_pro").is(":checked"));
				disableExtraOptions();
		});
	});

	$("#frmNewProduct").wizard({
		show: function(element) {
			},
			next:"Siguiente >",
			prev:"< Anterior",
			action:"Insertar",
			extra:"ui-state-default ui-corner-all"
	});

	/***********************************************************************/
	/************************  General Conditions  *************************/
	/***********************************************************************/
		$('#create-user').click(function() {
			$('#dialog').dialog('open');
		});
		
		$("#frmNewCondition").validate({
			errorLabelContainer: "#alertsDialog",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				txtCondition: {
					required: true
				},
				hdnPlanetFileName: {
					required: true
				},
				hdnEcuadorFileName: {
					required: true
				}

			}
		});

		new AjaxUpload('general_condition_planet', {
			action: document_root+'rpc/remoteValidation/product/generalCondition/uploadFile.rpc',
			name: 'uploadFile',
			onSubmit : function(file, ext){
				if (ext && /^(pdf)$/.test(ext)){
					if(file != $('#hdnEcuadorFileRealName').val()) {
						$('#general_condition_planet_file').html("Cargando "+file+" <img src='"+document_root+"img/loading_small.gif'>");
					} else {
						alert("Los archivos deben ser diferentes.");
						return false;
					}
				} else {
					alert("El archivo debe ser de formato PDF.");
					return false;
				}
			},
			onComplete : function(file,response){
				if(response) {
					$('#general_condition_planet_file').html('');
					$('#general_condition_planet').css('display','none');
					$('#general_condition_planet_msj').html(file);
					$('#hdnPlanetFileRealName').val(file);
					$('#hdnPlanetFileName').val(response);
				} else {
					$('#general_condition_planet_file').html('Error al subir '+file);
				}
			}
		});

		new AjaxUpload('general_condition_ecuador', {
			action: document_root+'rpc/remoteValidation/product/generalCondition/uploadFile.rpc',
			name: 'uploadFile',
			onSubmit : function(file, ext){
				if (ext && /^(pdf)$/.test(ext)){
					if(file != $('#hdnPlanetFileRealName').val()) {
						$('#general_condition_ecuador_file').html("Cargando "+file+" <img src='"+document_root+"img/loading_small.gif'>");
					} else {
						alert("Los archivos deben ser diferentes.");
						return false;
					}
				} else {
					alert("El archivo debe ser de formato PDF.");
					return false;
				}
			},
			onComplete : function(file,response){
				if(response) {
					$('#general_condition_ecuador_file').html('');
					$('#general_condition_ecuador').css('display','none');
					$('#general_condition_ecuador_msj').html(file);
					$('#hdnEcuadorFileRealName').val(file);
					$('#hdnEcuadorFileName').val(response);
				} else {
					$('#general_condition_ecuador_file').html('Error al subir '+file);
				}
			}
		});

		$("#dialog").dialog({
			bgiframe: true,
			autoOpen: false,
			draggable:false,
			resizable: false,
			height: 320,
			width: 520,
			closeOnEscape: false,
			open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
			modal: false,
			buttons: {
				'Guardar': function() {
					if($('#frmNewCondition').valid()){
						$.getJSON(document_root+"rpc/remoteValidation/product/generalCondition/checkGeneralCondition.rpc", {txtCondition: $('#txtCondition').val(), serial_lang:$('#hdnSerial_lang').val()}, function(data){
							if(data) {
								$.getJSON(document_root+"modules/product/generalConditions/pNewConditionDialog",
								{
									txtCondition: $('#txtCondition').val(),
									hdnLanguage: $('#hdnSerial_lang').val(),
									planetFileName: $('#hdnPlanetFileName').val(),
									ecuadorFileName: $('#hdnEcuadorFileName').val()
								},
								function(data) {
									if(data) {
										$('#innerConfirmMsj').html("Condici&oacute;n General creada exitosamente.");
										$('#generalConditionsTable').append("<div class='span-24 last line'><div class='prepend-4 span-1'><input type='radio' id='generalCondition' name='generalCondition' value='"+data+"' /></div><div class='span-10'>"+$('#txtCondition').val()+"</div><div class='span-4'><a href='"+document_root+"pdfs/generalConditions/"+$('#hdnPlanetFileName').val()+"' target='_blank'><img src='"+document_root+"img/ico-pdf.gif' border='0' /></a></div><div class='span-4'><a href='"+document_root+"pdfs/generalConditions/"+$('#hdnEcuadorFileName').val()+"' target='_blank'><img src='"+document_root+"img/ico-pdf.gif' border='0' /></a></div></div>");
										$('#innerConfirmMsj').addClass('success');
										$('#confirmMsj').css("display","block");
										$('#txtCondition').val('');
										$('#general_condition_planet').css('display','block');
										$('#general_condition_ecuador').css('display','block');
										$('#general_condition_planet_msj').html('');
										$('#general_condition_ecuador_msj').html('');
										$('#hdnPlanetFileName').val('');
										$('#hdnEcuadorFileName').val('');
										$('#hdnPlanetFileRealName').val('');
										$('#hdnEcuadorFileRealName').val('');
									} else {
										$('#innerConfirmMsj').html("Error al crear Condici&oacute;n General.");
										$('#innerConfirmMsj').addClass('error');
										$('#confirmMsj').css("display","block");
									}

									setTimeout(function(){
										$('#confirmMsj').hide("slide", {direction: "up"}, 1000);
										$('#innerConfirmMsj').removeClass('success');
										$('#innerConfirmMsj').removeClass('error');
									},3000);
								});
							} else {
								$('#alertsDialog').html("<li>El nombre de la Condici&oacute;n General ya existe.</li>");
								$('#alertsDialog').css('display','block');
							}
						});
					}
				},
				'Salir': function() {
					$('#txtCondition').val('');
					$('#general_condition_planet').css('display','block');
					$('#general_condition_ecuador').css('display','block');
					$('#general_condition_planet_msj').html('');
					$('#general_condition_ecuador_msj').html('');
					if($('#hdnPlanetFileName').val() != '') {
						unlink($('#hdnPlanetFileName').val());
					}
					if($('#hdnEcuadorFileName').val() != '') {
						unlink($('#hdnEcuadorFileName').val());
					}
					$('#hdnPlanetFileName').val('');
					$('#hdnEcuadorFileName').val('');
					$('#hdnPlanetFileRealName').val('');
					$('#hdnEcuadorFileRealName').val('');
					$('#alertsDialog').css('display','none');
					$(this).dialog('close');
				}
              }
		});

		function unlink(fileName) {
			$.getJSON(document_root+"rpc/remoteValidation/product/generalCondition/unlinkFile.rpc", {fileName: fileName}, function(data){
				if(!data) {
					alert("Error al borrar "+fileName+".");
				}
			});
		}
	/***********************************************************************/
	/*****************************  Benefits  ******************************/
	/***********************************************************************/


	$('#create-Benefit').click(function() {
		$('#dialogBenefit').dialog('open');
	})

	$("#dialogBenefit").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 420,
		modal: true,
		buttons: {
			'Guardar': function() {
				  $('#frmNewBenefit').submit();
			},
			Cancelar: function() {
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('#selStatusBenefit').val("");
			$('#txtDescBenefit').val("");
                        $('#txtWeightBenefit').val("");
			$('#alertsDialogBenefit').css("display","none");
			$('#messageDialogBenefit').remove();
		}
	});



	$("#frmNewBenefit").validate({
		errorLabelContainer: "#alertsDialogBenefit",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDescBenefit: {
				required: true,
				remote: {
					url:document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
					type: "post"
				}
			},
			selStatusBenefit: {
				required: true
			},
			txtTextBenefit: {
				required: true
			},
                        txtWeightBenefit: {
                            digits:true
                        }
		},
		messages: {
			txtDescBenefit: {
				remote: 'El beneficio ya existe en el sistema'
			},
                        txtWeightBenefit: {
                                digits: "La 'Priorizaci&oacute;n' debe contener valores num&eacute;ricos enteros"
                        }
		},
		submitHandler: function(form) {
			$(form).ajaxSubmit(
			{
				success: function(responseText) {
                                        var conditions='<option value="">Ninguna</option>';
                                        var restrictions='<option value="">Ninguna</option>';
                                        var descBenefit = $('#txtDescBenefit').val();
                                        if(responseText){
                                            $.getJSON(document_root+"rpc/loadBenefits.rpc", {}, function(data){
                                               for (condition in data['conditions']){
                                                    conditions=conditions+'<option value="'+data['conditions'][condition].serial_con+'">'+data['conditions'][condition].description_cbl+'</option>';
                                               }
                                               for (restriction in data['restrictions']){
                                                    restrictions=restrictions+'<option value="'+data['restrictions'][restriction].serial_rst+'">'+data['restrictions'][restriction].description_rbl+'</option>';
                                               }
                                              
                                               $('#benefitsTable').append('<div class="span-24 last line"><div class="prepend-5  span-1"><input type="checkbox" id="status_bpt_'+responseText+'" name="benefit['+responseText+'][status_bpt]" /></div><div id="description_ben_'+responseText+'" class="span-4">'+descBenefit+'</div><div class="span-3"><select id="serial_ben_'+responseText+'" name="benefit['+responseText+'][serial_con]" class="span-3">'+conditions+'</select></div><div class="span-2"><input type="text" id="price_bpt_'+responseText+'" name="benefit[{'+responseText+'][price_bpt]" class="span-2"/></div><div class="span-2"><input type="text" id="restriction_price_bpt_'+responseText+'" name="benefit['+responseText+'][restriction_price_bpt]"  class="span-2"/></div><div class="span-3 append-3 last"><select id="serial_rst_'+responseText+'" name="benefit['+responseText+'][serial_rst]" class="span-3">'+restrictions+'</select></div></div>');
                                            });
                                                
                                            $("#dialogBenefit").dialog('close');

					}
					else{
                                            $("#messageDialogBenefit").remove();
                                            $("#frmNewBenefit").prepend('<div id="messageDialogBenefit" class="append-8 span-8 prepend-1 last"><div class="span-8 error" align="center">La condici&oacute;n general se ha registrado exitosamente!</div></div>');
					}
				}
			});
		}
	});

	/***********************************************************************/
	/*****************************  Price  *********************************/
	/***********************************************************************/
	$("#spouse_pro").click(function(){
		showSpouse();
	});


	function showSpouse(){
		if($("#spouse_pro").is(':checked')){
			$(".spouseContainer").show();
			$("#percentage_spouse_pxc").val("");
		}
		else{
			$(".spouseContainer").hide();
			$("#percentage_spouse_pxc").val("");
		}
	}

	$("#relative_pro").click(function(){
		showRelative();
	});

	/*
	 *@Name: showRelative
	 *@Description: Shows or Hides percentage_extras_pxc and percentage_children_pxc field if spouse_pro checkbox is checked.
	 *@Params:
	 **/
	function showRelative(){
		if($("#relative_pro").is(':checked')){
			$(".childrenContainer").show();
			$(".extrasContainer").show();
			$("#percentage_extras_pxc").val("");
			$("#percentage_children_pxc").val("");
		}
		else{
			$(".childrenContainer").hide();
			$(".extrasContainer").hide();
			$("#percentage_extras_pxc").val("");
			$("#percentage_children_pxc").val("");
		}
	}

	var percentage=1;
	$("#percentage").keyup(function(){
		if($("#percentage").val()!=""){
			percentage = parseFloat($("#percentage").val().replace(",", "."));
			if(percentage > 1){
				percentage=percentage/100;
			}
		}
		else{
			percentage=1;
		}
		if(!$("price_by_range").is(":checked")){
			$('.divItem > .subtitle >  [name$="[price_ppc]"]" ').each(function(){
				if($(this).val()!=""){
					$("[name$='["+$(this).attr("pricenumber")+"][cost_ppc]']").val((parseFloat($(this).val().replace(",", "."))*percentage).toFixed(2));
				}
			});
		}else{
			$('.divItem > .subtitle >  [name$="[min_ppc]"]" ').each(function(){
				if($(this).val()!=""){
					$("[name$='["+$(this).attr("minnumber")+"][cost_ppc]']").val((parseFloat($(this).val().replace(",", "."))*percentage).toFixed(2));
				}
			});
		}
	});


	$("#price_by_range").bind("click",function(){
		showPrice();
		if($("#price_by_range").is(":checked")){
			$("#calculate_pro").attr("checked",false);
			$("#price_by_day_pro").attr("checked",true);
			$(".relatedToRange").attr("disabled",true);
			$(".relatedToRange").css("color","#aaa");
			$("#show_price_pro").attr("checked",true);
			$(".calculateDisable").css("color","#000");
			if($("#masive_pro").is(":checked")){
				enableMasive($("#masive_pro").is(":checked"));
			}
		}
		else{
			$("#price_by_day_pro").attr("checked",false);
			$("#show_price_pro").attr("checked",false);
			$(".relatedToRange").attr("disabled",false);
			$(".relatedToRange").css("color","#000");
			if($("#masive_pro").is(":checked")){
				enableMasive($("#masive_pro").is(":checked"));
			}
		}

	});
	$("#calculate_pro").click(function(){

		if($("#calculate_pro").is(":checked")){
			$("#price_by_range").attr("checked",false);
			$("#price_by_day_pro").attr("checked",false);
			$("#price_by_day_pro").attr("disabled",true);
			$("#show_price_pro").attr("checked",false);
			$("#show_price_pro").attr("disabled",false);
			$(".calculateDisable").css("color","#aaa");
		}
		else{

			$("#price_by_day_pro").attr("disabled",false);
			$(".calculateDisable").css("color","#000");
		}
		 showPrice();
	});

	/*
	 *@Name: showPrice
	 *@Description: Shows fields price.
	 *@Params:
	 **/
	function showPrice(){
		if($("#price_by_range").is(':checked')){
			$(".min").show();
			$(".max").show();
			$(".price").hide();
		}
		else{
			$(".min").hide();
			$(".max").hide();
			$(".price").show();

		}
	}


	$('[name$="[1][price_ppc]"]').keyup(function(){
		var value=$(this).val().replace(",", ".");
		if(value !='' ){
			$('[name$="[1][cost_ppc]"]').val((parseFloat(value)*percentage).toFixed(2));
			$('[name$="[1][min_ppc]"]').val((parseFloat(value)).toFixed(2));

		}
		else{
			$('[name$="[1][cost_ppc]"]').val('');
			$('[name$="[1][min_ppc]"]').val('');

		}
		$('[name$="[1][max_ppc]"]').val("");
	});

	$('[name$="[1][min_ppc]"]').keyup(function(){
		var value=$(this).val().replace(",", ".");
		if(value !='' ){
			$('[name$="[1][cost_ppc]"]').val((parseFloat(value)*percentage).toFixed(2));
			$('[name$="[1][price_ppc]"]').val((parseFloat(value)).toFixed(2));
		}
		else{
			$('[name$="[1][cost_ppc]"]').val('');
			$('[name$="[1][price_ppc]"]').val('');
		}
	});

	var equal;

	$('[name$="[1][duration_ppc]"]').change(function(){
		equal=0;
		$("#repeated").val(equal);
		validateDays();
	});

		/*
		 *@Name: validateDays
		 *@Description: Verifies if there are any number of days repeated in the price assignment
		 *@Params:
		 **/
	function validateDays(){
		$('[name$="[duration_ppc]"]').each(function(){
			durationnumber = $(this).attr('durationnumber');
			$('[divnumber="'+durationnumber+'"]').css('background','#FFFFFF none repeat scroll 0 0');
			$('[divnumber="'+durationnumber+'"]').css('border-color','#FFFFFF');
			$('[divnumber="'+durationnumber+'"]').css('color','#000000');
			$('[divnumber="'+durationnumber+'"]').css('border','none');
			$('[divnumber="'+durationnumber+'"]').css('margin-bottom','10px');
			$('[divnumber="'+durationnumber+'"]').css('padding','0');

			value=$(this);
			$('[name$="[duration_ppc]"]').each(function(){
				if($(this).attr('durationnumber')!=durationnumber){
					if((parseFloat(value.val().replace(",", "."))*percentage).toFixed(2)==(parseFloat($(this).val().replace(",", "."))*percentage).toFixed(2)){
						if($(this).val()!=""){
							$('[divnumber="'+durationnumber+'"]').css('background','#FBE3E4 none repeat scroll 0 0');
							$('[divnumber="'+durationnumber+'"]').css('color','#8A1F11');
							$('[divnumber="'+durationnumber+'"]').css('border','2px solid #FBC2C4');
							$('[divnumber="'+durationnumber+'"]').css('margin-bottom','1em');
							$('[divnumber="'+durationnumber+'"]').css('padding','0.8em');
							equal=1;
							$("#repeated").val(equal);
						}
					}
				}
			});
		});
	}


	$('#newPrice').click(function() {
		var n = parseInt($(".divItem:last").attr('divnumber'))+1;
		$("#deletePriceButton_"+(n-1)).remove();
		$("#price_by_product").append(' <div class="span-19 last line divItem" divNumber="'+n+'">\n\
											<div class=" span-1 number">'+n+'</div>\n\
											<div class=" span-3 subtitle">\n\
												<input type="text" id="price_by_product_'+n+'_duration_ppc" name="price_by_product['+n+'][duration_ppc]" class="price_by_product span-2" durationnumber="'+n+'" />\n\
											</div>\n\
											<div class="span-3 subtitle price">\n\
												<input type="text" id="price_by_product_'+n+'_price_ppc" name="price_by_product['+n+'][price_ppc]"  priceNumber="'+n+'" class="span-2"/>\n\
											</div>\n\
											<div class="span-3 subtitle min">\n\
												<input type="text" id="price_by_product_'+n+'_min_ppc" name="price_by_product['+n+'][min_ppc]"  minnumber="'+n+'" class="span-2"/>\n\
											</div>\n\
											<div class="span-3 subtitle max">\n\
												<input type="text" id="price_by_product_'+n+'_max_ppc" name="price_by_product['+n+'][max_ppc]"  maxnumber="'+n+'" class="span-2"/>\n\
											</div>\n\
											<div class="span-3 subtitle">\n\
												<input type="text" id="price_by_product_'+n+'_cost_ppc" name="price_by_product['+n+'][cost_ppc]" class="span-2"/>\n\
											</div>\n\
											<div class="span-3 subtitle last" id="deletePriceDiv_'+n+'">\n\
												<input type="button" id="deletePriceButton_'+n+'" class="deletePrice ui-state-default ui-corner-all" value="eliminar" itemNumber="'+n+'"/>\n\
											</div>\n\
										</div>');

		showPrice();

		$('[name$="['+n+'][price_ppc]"]').rules("add", {
			required:{
				depends: function(element) {
						if($("#price_by_range").is(':checked')){
						return false;
					} else {
						return true;
					}

				}
			},
			number:true,
			greaterOrEqualTo: "#price_by_product_"+n+"_cost_ppc",
			min: 0.01,
			messages: {
				required: "El precio  "+n+"  es obligatorio",
				number: "El precio  "+n+"  debe ser num&eacute;rico",
				greaterOrEqualTo: "El precio  "+n+"  debe ser mayor al costo",
				min: "El precio  "+n+ " debe ser mayor a 0"
			}
		});

		$('[name$="['+n+'][min_ppc]"]').rules("add", {
			required:{
				depends: function(element) {
					if($("#price_by_range").is(':checked')){
						return true;
					} else {
						return false;
					}
				}
			},
			number:true,
			lessOrEqualTo: "#price_by_product_"+n+"_max_ppc",
			min: 0.01,
			messages: {
				required: "El m&iacute;nimo  "+n+"  es obligatorio",
				number: "El m&iacute;nimo  "+n+"  debe ser num&eacute;rico",
				lessOrEqualTo: "El m&iacute;nimo  "+n+"  debe ser menor al m&aacute;ximo",
				min: "El m&iacute;nimo  "+n+ " debe ser mayor a 0"
			}
		});

		$('[name$="['+n+'][max_ppc]"]').rules("add", {
			required:{
				depends: function(element) {
						if($("#price_by_range").is(':checked')){
							return true;
						} else {
							return false;
						}
					}
				},
				number:true,
				greaterOrEqualTo: "#price_by_product_"+n+"_max_ppc",
				min: 0.01,
				messages: {
					required: "El m&aacute;ximo "+n+"  es obligatorio",
					number: "El m&aacute;ximo  "+n+"  debe ser num&eacute;rico",
					greaterOrEqualTo: "El m&aacute;ximo  "+n+"  debe ser mayor al m&iacute;nimo",
					min: "El m&aacute;ximo  "+n+ " debe ser mayor a 0"
				}
		});

		$('[name$="['+n+'][cost_ppc]"]').rules("add", {
				required:true,
				number:true,
				lessOrEqualTo: "#price_by_product_"+n+"_price_ppc",
				min: 0.01,
				messages: {
					required: "El costo  "+n+"  es obligatorio",
					number: "El costo  "+n+"  debe ser num&eacute;rico",
					lessOrEqualTo: "El costo  "+n+"  debe ser menor al precio",
					min: "El costo  "+n+ " debe ser mayor a 0"
				}
		});

		$('[name$="['+n+'][duration_ppc]"]').rules("add", {
				required:true,
				digits:true,
				min: 1,
				messages: {
					required: "La duraci&oacute;n  "+n+"  es obligatorio",
					digits: "La duraci&oacute;n  "+n+"  debe ser num&eacute;rico",
					min: "La duraci&oacute;n  "+n+ " debe ser mayor a 0"
				}
		});


		$('.[itemnumber="'+n+'"]').click(function() {
		 deleteItem(n);
		});


		$('[name$="['+n+'][price_ppc]"]').keyup(function(){
			var value=$(this).val().replace(",", ".");
			if(value !='' ){
				$('[name$="['+n+'][cost_ppc]"]').val((parseFloat(value)*percentage).toFixed(2));
				$('[name$="['+n+'][min_ppc]"]').val(parseFloat(value).toFixed(2));
			}
			else{
				$('[name$="['+n+'][cost_ppc]"]').val('');
				$('[name$="['+n+'][min_ppc]"]').val('');
			}
			$('[name$="['+n+'][max_ppc]"]').val("");
		});


		$('[name$="['+n+'][min_ppc]"]').keyup(function(){
			var value=$(this).val().replace(",", ".");
			if(value !='' ){
				$('[name$="['+n+'][cost_ppc]"]').val((parseFloat(value)*percentage).toFixed(2));
				$('[name$="['+n+'][price_ppc]"]').val(parseFloat(value).toFixed(2));
			}
			else{
				$('[name$="['+n+'][cost_ppc]"]').val('');
				$('[name$="['+n+'][price_ppc]"]').val('');
			}

		});

		var value;
		$('[name$="['+n+'][duration_ppc]"]').change(function(){
				equal=0;
				$("#repeated").val(equal);
				validateDays();
		});
	});

	function deleteItem(n){
		$("#deletePriceDiv_"+(n-1)).html('<input type="button" id="deletePriceButton_'+(n-1)+'" class="deletePrice ui-state-default ui-corner-all" value="eliminar" itemNumber="'+(n-1)+'"/>')
		$('[divnumber="'+n+'"]').remove();
		$("#deletePriceButton_"+(n-1)).click(function(){deleteItem(n-1)});
		validateDays();
	}

	$('[name$="[1][price_ppc]"]').rules("add", {
		required:{
			depends: function(element) {
				if($("#price_by_range").is(':checked')){
					return false;
				}
				else{
					return true;
				}

			}
		},
		number:true,
		greaterOrEqualTo: "#price_by_product_1_cost_ppc",
		min: 0.01,
		messages: {
			required: "El precio  1  es obligatorio",
			number: "El precio  1  debe ser num&eacute;rico",
			greaterOrEqualTo: "El precio  1  debe ser mayor al costo",
			min: "El precio  1 debe ser mayor a 0"
		}
	});


	$('[name$="[1][min_ppc]"]').rules("add", {
		required:{
			depends: function(element) {
				if($("#price_by_range").is(':checked')){
					return true;
				} else {
					return false;
				}
			}
		},
		number:true,
		lessOrEqualTo: "#price_by_product_1_max_ppc",
		min: 0.01,
		messages: {
			required: "El m&iacute;nimo  1  es obligatorio",
			number: "El m&iacute;nimo  1  debe ser num&eacute;rico",
			lessOrEqualTo: "El m&iacute;nimo  1  debe ser menor al m&aacute;ximo",
			min: "El m&iacute;nimo  1 debe ser mayor a 0"
		}
	});

	$('[name$="[1][max_ppc]"]').rules("add", {
		required:{
			depends: function(element) {
				if($("#price_by_range").is(':checked')){
					return true;
				} else {
					return false;
				}
			}
		},
		number:true,
		greaterOrEqualTo: "#price_by_product_1_max_ppc",
		min: 0.01,
		messages: {
			required: "El m&aacute;ximo 1  es obligatorio",
			number: "El m&aacute;ximo  1  debe ser num&eacute;rico",
			greaterOrEqualTo: "El m&aacute;ximo  1  debe ser mayor al m&iacute;nimo",
			min: "El m&aacute;ximo  1 debe ser mayor a 0"
		}
	});

	$('[name$="[1][cost_ppc]"]').rules("add", {
			required:true,
			number:true,
			lessOrEqualTo: "#price_by_product_1_price_ppc",
			min: 0.01,
			messages: {
				required: "El costo  1  es obligatorio",
				number: "El costo  1  debe ser num&eacute;rico",
				lessOrEqualTo: "El costo  1  debe ser menor al precio",
				min: "El costo  1 debe ser mayor a 0"
			}
	});

	$('[name$="[1][duration_ppc]"]').rules("add", {
			required:true,
			digits:true,
			min: 1,
			messages: {
				required: "La duraci&oacute;n  1  es obligatorio",
				digits: "La duraci&oacute;n  1  debe ser num&eacute;rico",
				min: "La duraci&oacute;n  1 debe ser mayor a 0"
			}
	});

	disableExtraOptions();
	$('#max_extras_pro').bind("focusout", function(){
		disableExtraOptions();
	});
	/*Verifies that the sum of the free extras is less or equal than the amount of paid extras*/
	$('#children_pro').bind("focusout", function() {
		var amount = 0;
		if($('#children_pro').val() != '') {
			amount+=parseInt($('#children_pro').val());
		}
		if($('#adults_pro').val() != '') {
			amount+=parseInt($('#adults_pro').val());
		}
		if(amount > $('#max_extras_pro').val()) {
			alert("La cantidad de menores gratis mas la cantidad de mayores gratis debe ser menor a la cantidad m\u00E1xima de acompa\u00F1antes.");
			$('#children_pro').val('');
		}
	});
	$('#adults_pro').bind("focusout", function() {
		var amount = 0;
		if($('#children_pro').val() != '') {
			amount+=parseInt($('#children_pro').val());
		}
		if($('#adults_pro').val() != '') {
			amount+=parseInt($('#adults_pro').val());
		}
		if(amount > $('#max_extras_pro').val()) {
			alert("La cantidad de menores gratis mas la cantidad de mayores gratis debe ser menor a la cantidad m\u00E1xima de acompa\u00F1antes.");
			$('#adults_pro').val('');
		}
	});

});

function disableExtraOptions() {
	if($('#max_extras_pro').val() != "" && parseInt($('#max_extras_pro').val()) > 0) {
		$('#selExtrasRestricted').attr("disabled", false);
		$('#children_pro').attr("disabled", false);
		$('#adults_pro').attr("disabled", false);
		$('#relative_pro').attr("disabled", false);
		$('#spouse_pro').attr("disabled", false);
	} else {
		$('#selExtrasRestricted').val('');
		$('#selExtrasRestricted').attr("disabled", true);
		$('#children_pro').val('');
		$('#children_pro').attr("disabled", true);
		$('#adults_pro').val('');
		$('#adults_pro').attr("disabled", true);
		$('#relative_pro').attr('checked',false);
		$('#relative_pro').attr("disabled", true);
		$('#spouse_pro').attr('checked',false);
		$('#spouse_pro').attr("disabled", true);
	}
}