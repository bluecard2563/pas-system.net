// JavaScript Document

$(document).ready(function(){
	/*AUTOCOMPLETER*/
	$("#txtName_tpp").autocomplete(document_root+"rpc/autocompleters/loadProductTypes.rpc",{
		extraParams: {
			serial_lang: function(){ return $("#selLanguage").val()}
        },
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnSerial_tpp").val(row[1]);

	});
	/*END AUTOCOMPLETER*/

});