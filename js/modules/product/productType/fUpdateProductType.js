/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){



	flights($("#flights_tpp").val());

	$("#flights_tpp").change(function(){
		flights($("#flights_tpp").val());
	});

	function flights(flightNumber){
		if(!$("#masive_tpp").is(":checked")){
			if(flightNumber!='1' && flightNumber!=""){

				$(".multipleFlights").attr("disabled",true);
				$(".multipleFlights").css("color","#aaa");
				$(":text.multipleFlights").val("");
				$(":checkbox:checked.multipleFlights").attr("checked",false);
				$(".singleFlight").attr("disabled",false);
				$(".singleFlight").css("color","#000");
			}
			else{

				$(".multipleFlights").attr("disabled",false);
				$(".multipleFlights").css("color","#000");
				$("#calculate_pro").attr("checked",true);
				$(".singleFlight").attr("disabled",true);
				$(".singleFlight").css("color","#aaa");
				$(":text.singleFlight").val("");
				$(":checkbox:checked.singleFlight").attr("checked",false);
			}
		}
		else{
		enableMasive(true);
		}
	}

$("#masive_tpp").bind("click",function(){enableMasive($("#masive_tpp").is(":checked"))});

	function enableMasive(status){
		$(".multipleFlights").attr("disabled",false);
		$(".multipleFlights").css("color","#000");
		$(".singleFlight").attr("disabled",false);
		$(".singleFlight").css("color","#000");

		if(status){
			$(".masive").css("color","#aaa");
			$(".masive").attr("disabled",true);
			$(".masiveBtn").css("color","#aaa");
			$(".masiveBtn").attr("disabled",true);
			$(":checkbox:checked.masive").attr("checked",false);
			$(":text.masive").val("");
			$(".masiveOn").attr("disabled",true);
			$(":checkbox.masiveOn").attr("checked",true);
			$(".divItem:not(:first)").remove();
		}
		else{
			$(".masive").css("color","#000");
			$(".masive").attr("disabled",false);
			$(".masiveBtn").css("color","#2E6E9E");
			$(".masiveBtn").attr("disabled",false);
			flights($("#flights_pro").val());
			$(".masiveOn").attr("disabled",false);
			$(":checkbox.masiveOn").attr("checked",false);
		}
	}

	$("#frmUpdateProductType").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName_tpp: {
				required: true,
				textOnly: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/product/checkProductType.rpc",
					type: "post",
					data:{hdnSerial_tpp:$("#serial_tpp").val(),txtName_tpp:$("#name_tpp").val()}
				}
			},
			image_tpp:{
				accept: "jpeg|jpg"
			},
			max_extras_tpp:{
				digits:true,
				greaterOrEqualTo: "#min_extras_tpp",
				min: 1
			},
			children_tpp:{
				digits:true,
				lessOrEqualTo: "#max_extras_tpp",
				min: 1
			},
			adults_tpp:{
				digits:true,
				lessOrEqualTo: "#max_extras_tpp",
				min: 1
			},
			flights_tpp:{
				required : true
			},
			generalCondition: {
				//required :true
			}
		},
		messages: {
			txtName_tpp: {
				required: "El campo Nombre es obligatorio.",
				soloTexto: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: 'Ya existe ese tipo de Producto.'
			},
			max_extras_tpp:{
				digits: 'El campo M&aacute;ximo Acompa&ntilde;antes debe ser un n&uacute;mero',
				greaterOrEqualTo: 'El campo M&aacute;ximo Acompa&ntilde;antes debe ser mayor a M&aacute;nimo Acompa&ntilde;antes',
				min: "El campo M&aacute;ximo Acompa&ntilde;antes debe ser mayor a 0"
			},
			children_tpp:{
				digits: 'El campo Menores gratis debe ser un n&uacute;mero',
				lessOrEqualTo: 'El campo Menores gratis debe ser menor o igual a M&aacute;ximo Acompa&ntilde;antes',
				min: "El campo Menores gratis debe ser mayor a 0"

			},
			flights_tpp:{
				required : "Seleccione el n&uacute;mero de Viajes por Per&iacute;odo"
			},
			generalCondition: {
				//required : "Debe Seleccionar un archivo de Condiciones Generales"
			},
			image_tpp:{
				accept: "Debe seleccionar un archivo JPG o JPEG",
				required : "Seleccione una imagen"
			}
		}
	});

	$("[id^='status_bpt_']").each(function (i) {
		var id=this.id.replace('status_bpt_','');
		$("#serial_ben_"+id).rules("add", {
			required:{
				depends:"!#"+this.id+":checked"
			},
			messages: {
 			  	required: "El campo Condiciones es obligatorio para el beneficio "+$("#description_"+id).html()+"."
 			}
		});

		$("#price_bpt_"+id).rules("add", {
			required:{
				depends:"!#"+this.id+":checked"
			},
			number:{
				depends:"!#"+this.id+":checked"
			},
			messages: {
 			  	required: 'El campo Cobertura es obligatorio para el beneficio "'+$("#description_"+id).html()+'".',
				number: 'El campo Cobertura para el beneficio "'+$("#description_"+id).html()+'" solo acepta n&uacute;meros.'
 			}
		});
		$("#restriction_price_bpt_"+id).rules("add", {
                        required:function(element) {
                            if($("#status_bpt_"+id).is(":checked") && $("#serial_rst_"+id).val()!="")
                                return true;
                            else
                                return false;
                        },
			number:{
				depends:"!#"+this.id+":checked"
			},
			messages: {
 			  	required: "El campo Valor Restr. es obligatorio para el beneficio "+$("#description_"+id).html()+".",
				number: "El campo Valor Restr. para el beneficio "+$("#description_"+id).html()+" solo acepta n&uacute;meros."
 			}
		});
		$("#serial_rst_"+id).rules("add", {
                        required:function(element) {
                            if($("#status_bpt_"+id).is(":checked") && $("#restriction_price_bpt_"+id).val()!="")
                                return true;
                            else
                                return false;
                        },
			messages: {
 			  	required: "El campo Restricci&oacute;n es obligatorio para el beneficio "+$("#description_"+id).html()+"."
 			}
		});
	});

        $("#frmUpdateProductType").wizard({
		show: function(element) {
			},
			next:"Siguiente >",
			prev:"< Anterior",
			action:"Actualizar",
			extra:"ui-state-default ui-corner-all"
        });



        /***********************************************************************/
        /************************  General Conditions  *************************/
        /***********************************************************************/

		$('#create-user').click(function() {
			$('#dialog').dialog('open');
		});

		$("#frmNewCondition").validate({
			errorLabelContainer: "#alertsDialog",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				txtCondition: {
					required: true
				},
				hdnPlanetFileName: {
					required: true
				},
				hdnEcuadorFileName: {
					required: true
				}

			}
		});

		new AjaxUpload('general_condition_planet', {
			action: document_root+'rpc/remoteValidation/product/generalCondition/uploadFile.rpc',
			name: 'uploadFile',
			onSubmit : function(file, ext){
				if (ext && /^(pdf)$/.test(ext)){
					if(file != $('#hdnEcuadorFileRealName').val()) {
						$('#general_condition_planet_file').html("Cargando "+file+" <img src='"+document_root+"img/loading_small.gif'>");
					} else {
						alert("Los archivos deben ser diferentes.");
						return false;
					}
				} else {
					alert("El archivo debe ser de formato PDF.");
					return false;
				}
			},
			onComplete : function(file,response){
				if(response) {
					$('#general_condition_planet_file').html('');
					$('#general_condition_planet').css('display','none');
					$('#general_condition_planet_msj').html(file);
					$('#hdnPlanetFileRealName').val(file);
					$('#hdnPlanetFileName').val(response);
				} else {
					$('#general_condition_planet_file').html('Error al subir '+file);
				}
			}
		});

		new AjaxUpload('general_condition_ecuador', {
			action: document_root+'rpc/remoteValidation/product/generalCondition/uploadFile.rpc',
			name: 'uploadFile',
			onSubmit : function(file, ext){
				if (ext && /^(pdf)$/.test(ext)){
					if(file != $('#hdnPlanetFileRealName').val()) {
						$('#general_condition_ecuador_file').html("Cargando "+file+" <img src='"+document_root+"img/loading_small.gif'>");
					} else {
						alert("Los archivos deben ser diferentes.");
						return false;
					}
				} else {
					alert("El archivo debe ser de formato PDF.");
					return false;
				}
			},
			onComplete : function(file,response){
				if(response) {
					$('#general_condition_ecuador_file').html('');
					$('#general_condition_ecuador').css('display','none');
					$('#general_condition_ecuador_msj').html(file);
					$('#hdnEcuadorFileRealName').val(file);
					$('#hdnEcuadorFileName').val(response);
				} else {
					$('#general_condition_ecuador_file').html('Error al subir '+file);
				}
			}
		});

		$("#dialog").dialog({
			bgiframe: true,
			autoOpen: false,
			draggable:false,
			resizable: false,
			height: 320,
			width: 520,
			closeOnEscape: false,
			open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
			modal: false,
			buttons: {
				'Guardar': function() {
					if($('#frmNewCondition').valid()){
						$.getJSON(document_root+"rpc/remoteValidation/product/generalCondition/checkGeneralCondition.rpc", {txtCondition: $('#txtCondition').val(), serial_lang:$('#hdnSerial_lang').val()}, function(data){
							if(data) {
								$.getJSON(document_root+"modules/product/generalConditions/pNewConditionDialog",
								{
									txtCondition: $('#txtCondition').val(),
									hdnLanguage: $('#hdnSerial_lang').val(),
									planetFileName: $('#hdnPlanetFileName').val(),
									ecuadorFileName: $('#hdnEcuadorFileName').val()
								},
								function(data) {
									if(data) {
										$('#innerConfirmMsj').html("Condici&oacute;n General creada exitosamente.");
										$('#generalConditionsTable').append("<div class='span-24 last line'><div class='prepend-4 span-1'><input type='radio' id='generalCondition' name='generalCondition' value='"+data+"' /></div><div class='span-10'>"+$('#txtCondition').val()+"</div><div class='span-4'><a href='"+document_root+"pdfs/generalConditions/"+$('#hdnPlanetFileName').val()+"' target='_blank'><img src='"+document_root+"img/ico-pdf.gif' border='0' /></a></div><div class='span-4'><a href='"+document_root+"pdfs/generalConditions/"+$('#hdnEcuadorFileName').val()+"' target='_blank'><img src='"+document_root+"img/ico-pdf.gif' border='0' /></a></div></div>");
										$('#innerConfirmMsj').addClass('success');
										$('#confirmMsj').css("display","block");
										$('#txtCondition').val('');
										$('#general_condition_planet').css('display','block');
										$('#general_condition_ecuador').css('display','block');
										$('#general_condition_planet_msj').html('');
										$('#general_condition_ecuador_msj').html('');
										$('#hdnPlanetFileName').val('');
										$('#hdnEcuadorFileName').val('');
										$('#hdnPlanetFileRealName').val('');
										$('#hdnEcuadorFileRealName').val('');
									} else {
										$('#innerConfirmMsj').html("Error al crear Condici&oacute;n General.");
										$('#innerConfirmMsj').addClass('error');
										$('#confirmMsj').css("display","block");
									}

									setTimeout(function(){
										$('#confirmMsj').hide("slide", {direction: "up"}, 1000);
										$('#innerConfirmMsj').removeClass('success');
										$('#innerConfirmMsj').removeClass('error');
									},3000);
								});
							} else {
								$('#alertsDialog').html("<li>El nombre de la Condici&oacute;n General ya existe.</li>");
								$('#alertsDialog').css('display','block');
							}
						});
					}
				},
				'Salir': function() {
					$('#txtCondition').val('');
					$('#general_condition_planet').css('display','block');
					$('#general_condition_ecuador').css('display','block');
					$('#general_condition_planet_msj').html('');
					$('#general_condition_ecuador_msj').html('');
					if($('#hdnPlanetFileName').val() != '') {
						unlink($('#hdnPlanetFileName').val());
					}
					if($('#hdnEcuadorFileName').val() != '') {
						unlink($('#hdnEcuadorFileName').val());
					}
					$('#hdnPlanetFileName').val('');
					$('#hdnEcuadorFileName').val('');
					$('#hdnPlanetFileRealName').val('');
					$('#hdnEcuadorFileRealName').val('');
					$('#alertsDialog').css('display','none');
					$(this).dialog('close');
				}
              }
		});

		function unlink(fileName) {
			$.getJSON(document_root+"rpc/remoteValidation/product/generalCondition/unlinkFile.rpc", {fileName: fileName}, function(data){
				if(!data) {
					alert("Error al borrar "+fileName+".");
				}
			});
		}

        /***********************************************************************/
        /*****************************  Benefits  ******************************/
        /***********************************************************************/

        $('#create-Benefit').click(function() {
			$('#dialogBenefit').dialog('open');
		})

		$("#dialogBenefit").dialog({
			bgiframe: true,
			autoOpen: false,
			height: 300,
			width: 420,
			modal: true,
			buttons: {
				'Guardar': function() {
					  $('#frmNewBenefit').submit();
				},
				Cancelar: function() {
					$(this).dialog('close');
				}
              },
			close: function() {
				$('#selStatusBenefit').val("");
				$('#txtDescBenefit').val("");
				$('#alertsDialogBenefit').css("display","none");
				$('#messageDialogBenefit').remove();
			}
		});



		$("#frmNewBenefit").validate({
		errorLabelContainer: "#alertsDialogBenefit",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDescBenefit: {
				required: true,
				remote: {
					url:document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
					type: "post"
				}
			},
			selStatusBenefit: {
				required: true
			},
			txtTextBenefit: {
				required: true
			},
                        txtWeightBenefit: {
                            digits:true
                        }
		},
		messages: {
			txtDescBenefit: {
				remote: 'El beneficio ya existe en el sistema'
			},
                        txtWeightBenefit: {
                                digits: "La 'Priorizaci&oacute;n' debe contener valores num&eacute;ricos enteros"
                        }
		},
		submitHandler: function(form) {
						$(form).ajaxSubmit({
				success: function(responseText) {
					var conditions='<option value="">Ninguna</option>';
					var restrictions='<option value="">Ninguna</option>';
					var descBenefit = $('#txtDescBenefit').val();
					if(responseText){
						$.getJSON(document_root+"rpc/loadBenefits.rpc", {}, function(data){
						   for (condition in data['conditions']){
								conditions=conditions+'<option value="'+data['conditions'][condition].serial_con+'">'+data['conditions'][condition].description_cbl+'</option>';
						   }
						   for (restriction in data['restrictions']){
								restrictions=restrictions+'<option value="'+data['restrictions'][restriction].serial_rst+'">'+data['restrictions'][restriction].description_rbl+'</option>';
						   }

						   $('#benefitsTable').append('<div class="span-24 last line"><div class="prepend-5  span-1"><input type="checkbox" id="status_bpt_'+responseText+'" name="benefit['+responseText+'][status_bpt]" /></div><div id="description_ben_'+responseText+'" class="span-4">'+descBenefit+'</div><div class="span-3"><select id="serial_ben_'+responseText+'" name="benefit['+responseText+'][serial_con]" class="span-3">'+conditions+'</select></div><div class="span-2"><input type="text" id="price_bpt_'+responseText+'" name="benefit[{'+responseText+'][price_bpt]" class="span-2"/></div><div class="span-2"><input type="text" id="restriction_price_bpt_'+responseText+'" name="benefit['+responseText+'][restriction_price_bpt]"  class="span-2"/></div><div class="span-3 append-4 last"><select id="serial_rst_'+responseText+'" name="benefit['+responseText+'][serial_rst]" class="span-3">'+restrictions+'</select></div></div>');

						   $("#serial_ben_"+responseText).rules("add", {
								required:{
									depends:"!#status_bpt_"+responseText+":checked"
								},
								messages: {
									required: "El campo Condiciones es obligatorio para el beneficio "+$("#description_ben_"+responseText).html()+"."
								}
							});

							$("#price_bpt_"+responseText).rules("add", {
								required:{
									depends:"!#status_bpt_"+responseText+":checked"
								},
								number:{
									depends:"!#status_bpt_"+responseText+":checked"
								},
								messages: {
									required: "El campo Cobertura es obligatorio para el beneficio "+$("#description_ben_"+responseText).html()+".",
									number: "El campo Cobertura para el beneficio "+$("#description_ben_"+responseText).html()+" solo acepta n&uacute;meros."
								}
							});
							$("#restriction_price_bpt_"+responseText).rules("add", {
								required:function(element) {
																if($("#status_bpt_"+responseText).is(":checked") && $("#serial_rst_"+responseText).val()!="")
																	return true;
																else
																	return false;
															},
								number:{
									depends:"!#status_bpt_"+responseText+":checked"
								},
								messages: {
									required: "El campo Valor Restr. es obligatorio para el beneficio "+$("#description_ben_"+responseText).html()+".",
									number: "El campo Valor Restr. para el beneficio "+$("#description_ben_"+responseText).html()+" solo acepta n&uacute;meros."
								}
							});
							$("#serial_rst_"+responseText).rules("add", {
								required:function(element) {
																if($("#status_bpt_"+responseText).is(":checked") && $("#restriction_price_bpt_"+responseText).val()!="")
																	return true;
																else
																	return false;
															},
								messages: {
									required: "El campo Restricci&oacute;n es obligatorio para el beneficio "+$("#description_ben_"+responseText).html()+"."
								}
							});

						});

						$('#selStatusBenefit').val("");
						$('#txtDescBenefit').val("");
						$("#dialogBenefit").dialog('close');
					}
					else{
						$("#messageDialogBenefit").remove();
						$("#frmNewBenefit").prepend('<div id="messageDialogBenefit" class="append-8 span-8 prepend-1 last"><div class="span-8 error" align="center">La condici&oacute;n general se ha registrado exitosamente!</div></div>');
					}
				}
			});
		}
	});

	disableExtraOptions();
	$('#max_extras_tpp').bind("focusout", function(){
		disableExtraOptions();
	});

	/*Verifies that the sum of the free extras is less or equal than the amount of paid extras*/
	$('#children_tpp').bind("focusout", function() {
		var amount = 0;
		if($('#children_tpp').val() != '') {
			amount+=parseInt($('#children_tpp').val());
		}
		if($('#adults_tpp').val() != '') {
			amount+=parseInt($('#adults_tpp').val());
		}
		if(amount > $('#max_extras_tpp').val()) {
			alert("La cantidad de menores gratis mas la cantidad de mayores gratis debe ser menor a la cantidad m\u00E1xima de acompa\u00F1antes.");
			$('#children_tpp').val('');
		}
	});
	$('#adults_tpp').bind("focusout", function() {
		var amount = 0;
		if($('#children_tpp').val() != '') {
			amount+=parseInt($('#children_tpp').val());
		}
		if($('#adults_tpp').val() != '') {
			amount+=parseInt($('#adults_tpp').val());
		}
		if(amount > $('#max_extras_tpp').val()) {
			alert("La cantidad de menores gratis mas la cantidad de mayores gratis debe ser menor a la cantidad m\u00E1xima de acompa\u00F1antes.");
			$('#adults_tpp').val('');
		}
	});
});

function disableExtraOptions() {
	if($('#max_extras_tpp').val() != "" && parseInt($('#max_extras_tpp').val()) > 0) {
		$('#selExtrasRestricted').attr("disabled", false);
		$('#children_tpp').attr("disabled", false);
		$('#adults_tpp').attr("disabled", false);
		$('#relative_tpp').attr("disabled", false);
		$('#spouse_tpp').attr("disabled", false);
	} else {
		$('#selExtrasRestricted').val('');
		$('#selExtrasRestricted').attr("disabled", true);
		$('#children_tpp').val('');
		$('#children_tpp').attr("disabled", true);
		$('#adults_tpp').val('');
		$('#adults_tpp').attr("disabled", true);
		$('#relative_tpp').attr('checked',false);
		$('#relative_tpp').attr("disabled", true);
		$('#spouse_tpp').attr('checked',false);
		$('#spouse_tpp').attr("disabled", true);
	}
}