// JavaScript Document

    $(document).ready(function(){
	$("#frmUpdateBenefit").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			rdStatus: {
				required: true
                        }
		}
	});

       
        $("#newInfo").css("display","none");
        $("[name='rdStatus']").bind("click", function(e){
            changeStatus(this.value);
        });

        $("#btnInsert").bind("click", function(e){
            createNew();
        });


          $("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 400,
		width: 410,
		modal: true,
		buttons: {
			'Actualizar': function() {
				  $("#frmUpdateTranslation").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtBenefit: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
									type: "post",
									data: {
									  serial_lang: function() {
										return $("#serial_lang").val();
									  },
									  serial_ben: function() {
										return $("#hdnBenefitID").val();
									  }
									}
								}
							}
						},
						messages: {
							txtBenefit: {
								remote: "La traducci&oacute;n ingresada ya existe en el sistema."
							}
						}
					});

				   if($('#frmUpdateTranslation').valid()){
					$('#frmUpdateTranslation').submit();
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});





    });

function loadFormData(itemID){
	$('#language').html($('#name_lang_'+itemID).html());
	$('#diaName_tpp').val($('#name_ptl_'+itemID).html());
        $('#serial_lang').val($('#serial_lang_'+itemID).val());
        $('#serial_ptl').val(itemID);
		
}

function changeStatus(status){
    if(status=='INACTIVE'){
        if(confirm('Est\u00E1 seguro que desea deshabilitar este beneficio?')){
          $("#frmUpdateBenefit").submit();
        }
    }else{
        $("#frmUpdateBenefit").submit();
    }
}

function createNew(){
    if($("#frmNewTranslation").valid()){
      $("#frmNewTranslation").submit();
    }
}