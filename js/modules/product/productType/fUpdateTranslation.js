// JavaScript Document
$(document).ready(function(){
	
  $("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 400,
		width: 410,
		modal: true,
		buttons: {
			'Actualizar': function() {
				  $("#frmUpdateTranslation").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtBenefit: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
									type: "post",
									data: {
									  serial_lang: function() {
										return $("#serial_lang").val();
									  },
									  serial_ben: function() {
										return $("#hdnBenefitID").val();
									  }
									}
								}
							}
						},
						messages: {
							txtBenefit: {
								remote: "La traducci&oacute;n ingresada ya existe en el sistema."
							}
						}
					});

				   if($('#frmUpdateTranslation').valid()){
					$('#frmUpdateTranslation').submit();
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});
});