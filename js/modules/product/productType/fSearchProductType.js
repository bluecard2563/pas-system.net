// JavaScript Document
$(document).ready(function(){
	if($("#product_table").length>0) {
        pager = new Pager('product_table', 8 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	/*EVENT OPTIONS*/
	$('#selLanguage').bind("change", function(){
		$('#productContainer').load(document_root+"rpc/loadProductTypesByLanguage.rpc", {code_lang: this.value}, function(){
			if($("#product_table").length>0) {
				pager = new Pager('product_table', 8 , 'Siguiente', 'Anterior');
				pager.init(7); //Max Pages
				pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); //Starting page
			}
			$("a[id^='btnUpdate']").bind("click", function(){
				$('#hdnSerial_tpp').val($(this).attr("serial_tpp"));
				$('#frmSearchProductType').submit();
			});
		});
	});

	$("a[id^='btnUpdate']").bind("click", function(){
		$('#hdnSerial_tpp').val($(this).attr("serial_tpp"));
		$('#frmSearchProductType').submit();
	});
	/*END EVENT OPTIONS*/
});