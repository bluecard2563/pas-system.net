// JavaScript Document
var currentTable;
$(document).ready(function(){
	/*Style dataTable*/
	currentTable = $('#destinationTable').dataTable( {
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"oPaginate": {
				"sFirst": "Primera",
				"sLast": "&Uacute;ltima",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
			"sZeroRecords": "No se encontraron resultados",
			"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
			"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
			"sSearch": "Filtro:",
			"sProcessing": "Filtrando.."
		},
		"sDom": 'T<"clear">lfrtip',
		"oTableTools": {
			"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
			"aButtons": ["pdf","xls" ]
		}

	} );

	$("input[id^='chkDestination']", currentTable.fnGetNodes()).click( function () {
		if(!$(this).attr("checked")) {
			$('#selAll').attr("checked", false);
		}
	});
	
	$('#selAll').bind("click",function(){
		if($('#selAll').attr("checked")) {
			$("input[id^='chkDestination']", currentTable.fnGetFilteredNodes()).attr("checked", true);
		} else {
			$("input[id^='chkDestination']", currentTable.fnGetNodes()).attr("checked", false);
		}
	});
	
	$('#btnSubmit').bind("click",function(){
		var one_required=false;
		$('[id^="chkDestination"]', currentTable.fnGetNodes()).each(function(){
			if($(this).is(':checked')){
				one_required=true;
				return;
			}
		});

		if(one_required){
			var claims="";
			$("input:checked[id^='chkDestination']", currentTable.fnGetNodes()).each(function(){
				claims+=this.value+",";
			});
			
			$('#txtValue').text(claims);
			$('#frmDestinations').submit();
		}else{
			alert('Seleccione al menos un Destino.');
		}
	});
	
	$('#btnBack').bind("click",function(){
		location.href = document_root + 'modules/product/productByCountry/fManageAllowedDestinations'
	});
});



