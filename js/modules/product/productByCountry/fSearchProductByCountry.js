$(document).ready(function(){
    $("#frmSearchProductBC").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,

        rules: {
                'hdnSerial_pro': {
                        required: true
                }
        }
    });

    /*AUTOCOMPLETER*/
    $("#txtName_pro").autocomplete(document_root+"rpc/autocompleters/loadProduct.rpc",{
            extraParams: {
                    serial_lang: function(){return $("#selLanguage").val()}
    },
            cacheLength: 0,
            max: 10,
            scroll: false,
            matchContains:true,
            minChars: 1,
            formatItem: function(row) {
                     return row[0] ;
            }
    }).result(function(event, row) {
            $("#hdnSerial_pro").val(row[1]);

    });
    /*END AUTOCOMPLETER*/

});