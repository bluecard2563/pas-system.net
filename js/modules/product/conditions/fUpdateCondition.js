// JavaScript Document
var pager;
$(document).ready(function(){
	$("#frmUpdateRestriction").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			rdStatus: {
				required: true
			}				
		},
		messages: {
			
		}
	});

    $("#frmNewTranslation").validate({
		errorLabelContainer: "#alerts_t",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selLanguage: {
				required: true
			},
            txtDescription: {
				required: true
			}
		}
	});

    $("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 410,
		modal: true,
		buttons: {
			'Actualizar': function() {
				  $("#frmUpdateTranslation").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtDescription_up: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/product/checkCndDescription.rpc",
									type: "post",
									data: {
									  serial_lang: function() {
										return $("#serial_lang").val();
									  },
									  serial_cbl: function() {
										return $("#serial_cbl").val();
									  }
									}
								}
							}
						},
						messages: {
							txtDescription_up: {
								remote: "La traducci&oacute;n ingresada ya existe en el sistema."
							}
						}
					});

				   if($('#frmUpdateTranslation').valid()){
						$('#frmUpdateTranslation').submit();
				   }
			},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});

	$("#newInfo").css("display","none");
	$("[name='rdStatus']").bind("click", function(e){
		changeStatus(this.value);
	});

	$("#btnInsert").bind("click", function(e){
		createNew();
	});

	$('#show').bind("click",function(e){
		$("#newInfo").css("display","inline");
	});

	if($('#adminTable').length>0){
		pager = new Pager('translationsTable', 10 , 'Siguiente', 'Anterior');
		pager.init(25); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page

		$("img.image").bind("click", function(e){
			loadFormData($(this).attr("id"));
			$('select.select').hide();
			$('#dialog').dialog('open');
		});
	}
});

function loadFormData(itemID){
	$('#language').html($('#name_lang_'+itemID).val());
	$('#txtDescription_up').val($('#description_cbl_'+itemID).val());
	$('#serial_lang').val($('#serial_lang_'+itemID).val());
	$('#serial_cbl').val(itemID);
}

function changeStatus(status){
    if(status=='INACTIVE'){
        if(confirm('Est\u00E1 seguro que desea deshabilitar esta restricci\u00F3n?')){
          $("#frmUpdateRestriction").submit();
        }
    }else{
        $("#frmUpdateRestriction").submit();
    }
}

function createNew(){
    if($("#frmNewTranslation").valid()){
      $("#frmNewTranslation").submit();
    }
}