// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchCondition").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCondition: {
				required: true
                        },
                        selLanguage:{
                            required: true
                        }
		}
	});
	/*END VALIDATION*/
	
	/*AUTOCOMPLETER*/
	$("#txtCondition").autocomplete(document_root+"rpc/autocompleters/loadConditions.rpc",{
		extraParams: {
                    serial_lang: function(){ return $("#selLanguage").val()}
                },
                max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnConditionID").val(row[1]);
	});
	/*END AUTOCOMPLETER*/

        $("#selLanguage").change (function(){
            $('#txtCondition').flushCache();
            $("#txtCondition").val('');
            $('#hdnConditionID').val('');
	});
});