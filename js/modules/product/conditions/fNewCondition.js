// JavaScript Document
$(document).ready(function(){
	$("#frmNewCondition").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDescription: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/product/checkCndDescription.rpc",
					type: "post"
				}
			},
			txtAlias: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/product/checkCndAlias.rpc",
					type: "post"
				}
			}
		},
		messages: {
			txtDescription: {
				remote: 'Ya existe una condici&oacute;n con esa descripci&oacute;n.'
			},
			txtAlias: {
				remote: 'Ya existe una condici&oacute;n con el alias ingresado.'
			}
		}
	});
});