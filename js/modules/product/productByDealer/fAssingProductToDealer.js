// JavaScript Document
var pager;
var pager_dealers;
var pager_product;
$(document).ready(function(){
	 //VALIDATION
    $("#frmAssignProduct").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                'selZone': {
					required: true
                },
                'selCountry': {
					required: true
                },
                'selManager': {
					required: true,
					number: true
                },
                'selCity': {
					required: true,
					number: true
                },
                'selDealer': {
					required: true,
					number: true
                },
				'hdnSelectOne': {
					required: true
				}
        },
		messages: {
			'selCity': {
				required: "El campo 'Ciudad' es obligatorio"
			}
		}
    });
	
	$('#selZone').bind("change", function(){
		$('#dealerContainer').html('');
		$('#productContainer').html('');
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

		$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",
			{
				serial_zon: $('#selZone').val()
			},function(){
				$('#selCountry').bind('change',function(){
					$('#dealerContainer').html('');
					$('#productContainer').html('');
					
					loadCities($(this).val());
					loadManagersByCountry($(this).val());
				});

				if($('#selCountry option').size()<=2){
					loadCities($('#selCountry').val());
					loadManagersByCountry($('#selCountry').val());
				}
			});
	});

	if($('#dealer_table').length>0){
		pager_dealers = new Pager('dealer_table', 10 , 'Siguiente', 'Anterior');
		pager_dealers.init($('#pages_dealer').val()); //Max Pages
		pager_dealers.showPageNav('pager_dealers', 'paging_dealers'); //Div where the navigation buttons will be placed.
		pager_dealers.showPage(1); //Starting page

		$('#chkAll').bind('click',function(){
			if($(this).is(':checked')){
				$('[name^="chkDealers"]').each(function(){
					$(this).attr('checked',true);
				});
			}else{
				$('[name^="chkDealers"]').each(function(){
					$(this).attr('checked',false);
				});
			}
			evaluateProducts();
		});

		$('[name^="chkDealers"]').each(function(){
			$(this).bind('click',function(){
				evaluateProducts();
			});
		});
	}
});

function loadManagersByCountry(serial_cou){
	if(serial_cou!=''){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",
			{
				serial_cou: serial_cou
			},function(){
				$('#selManager').bind('change',function(){
					loadDealers();
				});

				if($('#selManager option').size()){
					loadDealers();
				}
			});
	}else{
		$('#dealerContainer').html('');
		$('#productContainer').html('');
	}
}

function loadCities(serial_cou){
	if(serial_cou!=''){
		$('#cityContainer').load(document_root+"rpc/loadCities.rpc",
			{
				serial_cou: serial_cou
			},function(){
				$('#selCity').bind('change',function(){
					loadComissionists();
					loadDealers();
				});

				if($('#selCity option').size()){
					loadComissionists();
					loadDealers();
				}
			});
	}else{
		$('#dealerContainer').html('');
		$('#productContainer').html('');
	}
}

function loadDealers(){
	var serial_mbc=$('#selManager').val();
	var serial_cit=$('#selCity').val();
	var serial_usr=$('#selComissionist').val();
	$('#productContainer').html('');
	
	if(serial_mbc!='' && serial_cit!=''){
		$('#dealerContainer').load(document_root+"rpc/loadProduct/loadDealersForAssingProducts.rpc", {
				serial_mbc: serial_mbc,
				serial_cit: serial_cit,
				serial_usr: serial_usr
			},function(){
				if($('#dealer_table').length>0){
					pager = new Pager('dealer_table', 10 , 'Siguiente', 'Anterior');
					pager.init($('#pages').val()); //Max Pages
					pager.showPageNav('pager', 'table_paging'); //Div where the navigation buttons will be placed.
					pager.showPage(1); //Starting page

					$('#chkAll').bind('click',function(){
						if($(this).is(':checked')){
							$('[name^="chkDealers"]').each(function(){
								$(this).attr('checked',true);
							});
						}else{
							$('[name^="chkDealers"]').each(function(){
								$(this).attr('checked',false);
							});
						}
						evaluateProducts();
					});

					$('[name^="chkDealers"]').each(function(){
						$(this).bind('click',function(){
							evaluateProducts();
						});
					});
				 }
			});
	}
}

function loadComissionists() {
	$('#productContainer').html('');
	$('#comissionistContainer').load(document_root+"rpc/loadComissionistByCityByManager.rpc",{serial_mbc:$('#selManager').val(), serial_cit:$('#selCity').val(), all:true, text: "Todos"},function(){
		$('#selComissionist').bind("change", function(){
			loadDealers();
		});
	});
}

function evaluateProducts(){
	var serials_array=new Array();
	var serials;
	var items_checked=0;
	var aux;

	$('[name^="chkDealers"]').each(function(){
		if($(this).is(':checked')){
			items_checked++;
			aux=$(this).attr('id');
			aux=aux.split('_');
			array_push(serials_array, aux['1']);
		}
	});
	serials=implode(',', serials_array);

	if(items_checked>0){
		$('#productContainer').load(document_root+"rpc/loadProduct/loadProductsToAssign.rpc",
			{
				dealer_number: items_checked,
				serials: serials,
				serial_cou: $('#selCountry').val()
			},function(){
				if($('#product_table').length>0){
					pager_product = new Pager('product_table', 7 , 'Siguiente', 'Anterior');
					pager_product.init($('#pages_product').val()); //Max Pages
					pager_product.showPageNav('pager_product', 'table_paging_product'); //Div where the navigation buttons will be placed.
					pager_product.showPage(1); //Starting page

					$('#chkAll_products').bind('click',function(){
						if($(this).is(':checked')){
							$('[name^="chkProduct"]').each(function(){
								$(this).attr('checked',true);
							});
						}else{
							$('[name^="chkProduct"]').each(function(){
								$(this).attr('checked',false);
							});
						}
						validateOneRequired();
					});

					$('[name^="chkProduct"]').each(function(){
						$(this).bind('click',function(){
							validateOneRequired();
						});
					});

					if(items_checked==1){
						validateOneRequired();
					}
				}
			});		
	}else{
		$('#productContainer').html('');
	}
}

function validateOneRequired(){
	var selected=false;

	$('[name^="chkProduct"]').each(function(){
		if($(this).is(':checked')){
			selected=true;
			return;
		}
	});

	if(selected){
		$('#hdnSelectOne').val($('#hdnSelectOne').val()+1);
	}else{
		$('#hdnSelectOne').val('');
	}
}