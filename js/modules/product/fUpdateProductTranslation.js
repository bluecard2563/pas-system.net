	$(document).ready(function(){
	$("#frmUpdateProductTranslation").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			rdStatus: {
				required: true
                        }
		}
	});


        $("#newInfo").css("display","none");
        $("[name='rdStatus']").bind("click", function(e){
            changeStatus(this.value);
        });

        $("#btnInsert").bind("click", function(e){
            createNew();
        });
});

function loadFormData(itemID){
	$('#language').html($('#name_lang_'+itemID).html());
	$('#diaName_pro').val($('#name_pbl_'+itemID).html());
        $('#serial_lang').val($('#serial_lang_'+itemID).val());
        $('#serial_pbl').val(itemID);

}

function changeStatus(status){
    if(status=='INACTIVE'){
        if(confirm('Est\u00E1 seguro que desea deshabilitar este beneficio?')){
          $("#frmUpdateProductTranslation").submit();
        }
    }else{
        $("#frmUpdateProductTranslation").submit();
    }
}

function createNew(){
    if($("#frmNewTranslation").valid()){
      $("#frmNewTranslation").submit();
    }
}
