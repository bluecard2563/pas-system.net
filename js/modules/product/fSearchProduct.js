$(document).ready(function(){
	if($("#product_table").length>0) {
        pager = new Pager('product_table', 10 , 'Siguiente', 'Anterior');
        pager.init($('#hdnPages').val()); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	/*EVENT OPTIONS*/
	$('#selLanguage').bind("change", function(){
		$('#productContainer').load(document_root+"rpc/loadProductsByLanguage.rpc", {code_lang: this.value}, function(){
			if($("#product_table").length>0) {
				pager = new Pager('product_table', 8 , 'Siguiente', 'Anterior');
				pager.init(7); //Max Pages
				pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); //Starting page
			}
			$("a[id^='btnUpdate']").bind("click", function(){
				$('#hdnSerial_pro').val($(this).attr("serial_pro"));
				$('#frmSearchProduct').submit();
			});
		});
	});
	
	$("a[id^='btnUpdate']").bind("click", function(){
		$('#hdnSerial_pro').val($(this).attr("serial_pro"));
		$('#frmSearchProduct').submit();
	});
	/*END EVENT OPTIONS*/
});