	function loadChecks(serial_tpp){
		$('#checkContainer').load(document_root+"rpc/loadProductsPage1.rpc",{serial_tpp: serial_tpp});
	}

	$(document).ready(function(){

		$("#frmNewProduct").validate({

			errorLabelContainer: "#alerts",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,

			rules: {
				txtName_pro: {
					required: true,
					maxlength: 30,
					 remote: {
						url: document_root+"rpc/remoteValidation/product/checkProduct.rpc",
						type: "post"
					}
				}
			},

			messages: {
				txtName_pro: {
					required: "El campo Nombre es obligatorio.",
					maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
					remote: 'Ya existe ese tipo de Producto.'
				}
			}
		});

	});