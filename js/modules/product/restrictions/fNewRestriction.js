// JavaScript Document
$(document).ready(function(){
	$("#frmNewRestriction").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDescription: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/product/checkRstDescription.rpc",
					type: "post"
				}
			}
		},
		messages: {
			txtDescription: {
                            remote: 'Ya existe una traducci&oacute;n para el presente idioma.'
                        }
		}
	});
});