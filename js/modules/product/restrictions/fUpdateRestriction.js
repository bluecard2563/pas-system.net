// JavaScript Document
$(document).ready(function(){
	$("#frmUpdateRestriction").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			rdStatus: {
				required: true
			}				
		},
		messages: {
			
		}
	});

        $("#frmNewTranslation").validate({
		errorLabelContainer: "#alerts_t",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selLanguage: {
				required: true
			},
                        txtDescription: {
				required: true
			}
		}
	});

        $("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 410,
		modal: true,
		buttons: {
			'Actualizar': function() {
				  $("#frmUpdateTranslation").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtRestriction: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/product/checkRstDescription.rpc",
									type: "post",
									data: {
									  serial_lang: function() {
										return $("#serial_lang").val();
									  },
									  serial_rst: function() {
										return $("#hdnRestrictionID").val();
									  }
									}
								}
							}
						},
						messages: {
							txtRestriction: {
								remote: "La traducci&oacute;n ingresada ya existe en el sistema."
							}
						}
					});

				   if($('#frmUpdateTranslation').valid()){
					$('#frmUpdateTranslation').submit();
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});

        $("#newInfo").css("display","none");
        $("[name='rdStatus']").bind("click", function(e){
            changeStatus(this.value);
        });
        
        $("#btnInsert").bind("click", function(e){
            createNew();
        });
});

function loadFormData(itemID){
	$('#language').html($('#name_lang_'+itemID).html());
	$('#txtRestriction').val($('#description_rbl_'+itemID).html());
        $('#serial_lang').val($('#serial_lang_'+itemID).val());
        $('#serial_rbl').val(itemID);
}

function changeStatus(status){
    if(status=='INACTIVE'){
        if(confirm('Est\u00E1 seguro que desea deshabilitar esta restricci\u00F3n?')){
          $("#frmUpdateRestriction").submit();
        }
    }else{
        $("#frmUpdateRestriction").submit();
    }
}

function createNew(){
    if($("#frmNewTranslation").valid()){
      $("#frmNewTranslation").submit();
    }
}