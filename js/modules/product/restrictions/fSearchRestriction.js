// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchRestriction").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtRestriction: {
				required: true
                        },
                        selLanguage:{
                            required: true
                        }
		}
	});
	/*END VALIDATION*/
	
	/*AUTOCOMPLETER*/
	$("#txtRestriction").autocomplete(document_root+"rpc/autocompleters/loadRestrictions.rpc",{
		extraParams: {
                    serial_lang: function(){ return $("#selLanguage").val()}
                },
                max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnRestrictionID").val(row[1]);
	});
	/*END AUTOCOMPLETER*/

        $("#selLanguage").change (function(){
            $('#txtRestriction').flushCache();
            $("#txtRestriction").val('');
            $('#hdnRestrictionID').val('');
	});
});