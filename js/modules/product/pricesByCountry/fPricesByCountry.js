// JavaScript Document
var pager;
$(document).ready(function(){
    //VALIDATION
    $("#frmPBC").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                duration_ppc_0: {
                        required: true,
                        digits: true
                },
                price_ppc_0: {
                        required: true,
                        number: true,
                        greaterOrEqualTo: '#cost_ppc_0'
                },
                min_ppc_0: {
                        required: true,
                        number: true
                },
                max_ppc_0: {
                        required: true,
                        number: true,
                        greaterOrEqualTo: '#min_ppc_0'
                },
                cost_ppc_0: {
                        required: true,
                        number: true
                },
                percentage: {
                        number: true
                },
                repeated: {
                        equal: 0
                }
        },
         messages:{
                duration_ppc_0: {
                        digits: 'El campo "Dias" admite s&oacute;lo enteros.'
                },
                price_ppc_0: {
                        number: 'El campo "Precio" admite s&oacute;lo n&uacute;meros.',
                        greaterOrEqualTo: 'El costo debe ser menor al precio.'
                },
                min_ppc_0: {
                        number: 'El campo "M&iacute;nimo" admite s&oacute;lo n&uacute;meros.'
                },
                max_ppc_0: {
                        number: 'El campo "M&aacute;ximo" admite s&oacute;lo n&uacute;meros.',
                        greaterOrEqualTo: 'El m&aacute;ximo debe ser mayor que el m&iacute;nimo.'
                },
                cost_ppc_0: {
                        number: 'El campo "Costo" admite s&oacute;lo n&uacute;meros.'
                },
                percentage: {
                        number: 'El campo "Porcentaje" admite s&oacute;lo n&uacute;meros.'
                },
                repeated: {
                        equal: 'Existen d&iacute;as de duraci&oacute;n repetidos.'
                }
        }
    });
    //END VALIDATION    

    //ADD A NEW PRICE
    var div_price = $('#price_by_product #price_0').html();
    var range = $('#range').val();
    $('#addPrice').bind('click', function(){
        var count = parseInt($('#count').val())+1;
        var new_price = '';
        var span = 'span-12 append-6';
        if(range == 'YES'){
            span = 'span-15 append-5';
        }
        //WRITE THE NEW PRICE SECTION
        new_price = '<div id="price_'+count+'" class="'+span+' last line">'+div_price+'</div>';
        $('#new_prices').append(new_price);
        //$('#new_prices #price_0').attr('id', 'price_'+count);

        //$('#new_prices #price_'+count+' #number').html(count+1);
        
        $('#new_prices #price_'+count+' #duration_ppc_0').val('').attr({name: 'price_by_product['+count+'][duration_ppc]',id: 'duration_ppc_'+count}).bind('change', function(){
            validateDays();
        }).rules("add", {
            required: true,
            number: true,
            messages:{
                number: 'El campo "Dias" admite s&oacute;lo enteros.'
            }
        });
        
        if($('#new_prices #price_'+count+' #price_ppc_0').length > 0){
            $('#new_prices #price_'+count+' #price_ppc_0').val('').attr({name: 'price_by_product['+count+'][price_ppc]',id: 'price_ppc_'+count}).bind('change', function(){
                getCost($(this).val(), count);
            }).rules("add", {
                required: true,
                number: true,
                greaterOrEqualTo: '#cost_ppc_'+count,
                messages:{
                    number: 'El campo "Precio" admite s&oacute;lo n&uacute;meros.',
                    greaterOrEqualTo: 'El costo debe ser menor al precio.'
                }
            });
        }
        if($('#new_prices #price_'+count+' #min_ppc_0').length > 0){
            $('#new_prices #price_'+count+' #min_ppc_0').val('').attr({name: 'price_by_product['+count+'][min_ppc]',id: 'min_ppc_'+count}).bind('change', function(){
                getCost($(this).val(), count);
            }).rules("add", {
                required: true,
                number: true,
                messages:{
                    number: 'El campo "M&iacute;nimo" admite s&oacute;lo n&uacute;meros.'
                }
            });
        }
        if($('#new_prices #price_'+count+' #max_ppc_0').length > 0){
            $('#new_prices #price_'+count+' #max_ppc_0').val('').attr({name: 'price_by_product['+count+'][max_ppc]',id: 'max_ppc_'+count}).rules("add", {
                required: true,
                number: true,
                greaterOrEqualTo: '#min_ppc_'+count,
                messages:{
                    number: 'El campo "M&aacute;ximo" admite s&oacute;lo n&uacute;meros.',
                    greaterOrEqualTo: 'El m&aacute;ximo debe ser mayor que el m&iacute;nimo.'
                }
            });
        }

        $('#new_prices #price_'+count+' #cost_ppc_0').val('').attr({name: 'price_by_product['+count+'][cost_ppc]',id: 'cost_ppc_'+count}).rules("add", {
            required: true,
            number: true,
            messages:{
                number: 'El campo "Costo" admite s&oacute;lo n&uacute;meros.'
            }
        });

        $('#new_prices #price_'+count+' #delete_0').attr({id: 'delete_'+count}).bind('click',function(){
            delete_price(count);
        });


        $('#count').val(count);
    });

    //ADD THE ONCLICK EVENT TO EVERY DELETE BUTTON IN THE FORM
    $.each($('.deletePrice'),function (key){
        $(this).bind('click', function(){
            delete_price(key);
        });
    });
    //ADD THE VALIDATE DAYS FUNCTION TO EVERY DURECION FIELD AND VALIDATION
    $.each($('.duration'),function(){
        $(this).bind('change', function(){
            validateDays();
        }).rules("add", {
            required: true,
            number: true,
            messages:{
                number: 'El campo "Dias" admite s&oacute;lo enteros.'
            }
        });
    });
    //ADD THE CALCULATE COST FUNCTION TO EVERY PRICE REGISTERED AND VALIDATION TO THE PRICE AND MIN FIELD
    $.each($('.getcost'),function(key){
        $(this).bind('change',function(){
            getCost($(this).val(), key);
        }).rules("add", {
                required: true,
                number: true,
                greaterOrEqualTo: '#cost_ppc_'+key,
                messages:{
                    number: 'El campo "Precio" admite s&oacute;lo n&uacute;meros.',
                    greaterOrEqualTo: 'El costo debe ser menor al precio.'
                }
            });
    });
    //ADD VALIDATION TO THE MAX FIELD
    $.each($('.maxi'),function(key){
        $(this).rules("add", {
                required: true,
                number: true,
                greaterOrEqualTo: '#min_ppc_'+key,
                messages:{
                    number: 'El campo "M&aacute;ximo" admite s&oacute;lo n&uacute;meros.',
                    greaterOrEqualTo: 'El m&aacute;ximo debe ser mayor que el m&iacute;nimo.'
                }
            });
    });

    //ADD THE CALCULATE COST FUNCTION TO THE PRECENTAGE FIELD
    $('#percentage').bind('change', function(){
        $.each($('.getcost'),function(){
            var id = $(this).attr('id');
            var aux_arr = [];
            aux_arr = id.split('_');
            getCost($(this).val(), aux_arr[2]);
        });
    });

	pager = new Pager('referentialPrices', 10 , 'Siguiente', 'Anterior');
	pager.init(7); //Max Pages
	pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
	pager.showPage(1); //Starting page
});

/*
 *@Name: delete_price
 *@Description: Deletes the selected price from the form
 *@Params: count: the item number that has to be deleted
 **/
function delete_price(count){
    $('#price_'+count).remove();
    validateDays();
}

/*
 *@Name: validateDays
 *@Description: Verifies if there are any number of days repeated in the price assignment
 *@Params:
 **/
function validateDays(){
    var equal=0;
    $("#repeated").val(equal);
    $.each($('.duration'),function(){
        var duration = $(this).val();
        var id = $(this).attr('id');
        var aux_arr = [];
        aux_arr = id.split('_');
        var key = aux_arr[2];

        $('#price_'+key).css({
            background: '#FFFFFF none repeat scroll 0 0',
            color: '#000000',
            border: 'none',
            'margin-bottom': '10px',
            padding: '0'
        });
        $.each($('.duration'),function(){
            var duration_1 = $(this).val();
            var id_1 = $(this).attr('id');
            var aux_arr_1 = [];
            aux_arr_1 = id_1.split('_');
            var key_1 = aux_arr_1[2];

            if(id_1 != id ){
                if(duration != '' && duration_1 == duration){
                    $('#price_'+key_1).css({
                        background: '#FBE3E4 none repeat scroll 0 0',
                        color: '#8A1F11',
                        border: '2px solid #FBC2C4',
                        'margin-bottom': '1em',
                        padding: '0.8em'
                    });
                    $('#price_'+key).css({
                        background: '#FBE3E4 none repeat scroll 0 0',
                        color: '#8A1F11',
                        border: '2px solid #FBC2C4',
                        'margin-bottom': '1em',
                        padding: '0.8em'
                    });
                    equal=1;
                    $("#repeated").val(equal);
                }
            }
        });
    });
}

/*
 *@Name: getCost
 *@Description: Calculate the cost based on the price and cost percentage
 *@Params: price, count: the item number that has to be calculated
 **/
function getCost(value, count){
    var cost = '';
    var price = '';
    if($('#percentage').val() != '' && !isNaN($('#percentage').val())){
        var percentage = parseFloat($('#percentage').val().replace(",", ".")).toFixed(2);
        if(value != '' && !isNaN(value)){
            price = parseFloat(value.replace(",", ".")).toFixed(2);
            cost = ((percentage*price)/100).toFixed(2);
        }
    }else{
        if(value != '' && !isNaN(value)){
            price = parseFloat(value.replace(",", ".")).toFixed(2);
            cost = price;
        }
    }
    $('#cost_ppc_'+count).val(cost);
}

