$(document).ready(function(){
    $("#frmSearchPricesBC").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,

        rules: {
                selZone: {
                        required: true
                },
                selCountry: {
                        required: true
                },
                selProduct: {
                        required: true
                }
        }
    });

    $('#selZone').bind('change',function(){
        loadCountries(this.value)
    });
    $('#selCountry').bind('change',function(){
        loadProducts(this.value);
    });

    //LOAD THE COUNTRY SELECT BASED ON THE SELECTED ZONE
    function loadCountries(zoneId){
        if(zoneId != ''){
            $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
                $('#selCountry').bind('change',function(){loadProducts(this.value);});
                $('#selCountry').attr('title','Por favor seleccione un pa\u00EDs.');
                loadProducts($('#selCountry').val());
            });
        }else{
            $('#countryContainer').html('Seleccione una zona');
            loadProducts('');
        }
    }

    //LOAD THE PRODUCTS SELECT BASED ON THE SELECTED COUNTRY
    function loadProducts(countryId){
        if(countryId != ''){
            $('#productContainer').load(document_root+"rpc/loadProducts.rpc",{serial_cou: countryId});
        }else{
            $('#productContainer').html('Seleccione un pa\u00EDs');
        }
    }
});