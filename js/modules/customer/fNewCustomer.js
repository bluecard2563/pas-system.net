// JavaScript Document
function loadCities(){
	cityID=this.value;
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
		serial_cou: cityID,
		all: 'all'
	});
}

function clearTxtBirthdate(){
	$("[id^='question_']").each(function (i) {
		if(!this.value){
			$("#txtBirthdate").val("");
		}		
	});
	
}

function addQuestions(){
	var x;
	x=$("#hiddenAnswers");
	x.html("");
	$("[id^='question_']").each(function (i) {
		x.append('<input type="hidden" name="'+this.id+'" id="'+this.id+'" value="'+this.value+'" />');
	});
}

function loadDialog(){	
	var curr = new Date();
	curr.setFullYear(curr.getFullYear() - $('#hdnParameterValue').val());
	var dob =new Date(Date.parse(dateFormat($('#txtBirthdate').val())));
	var dialogOpen=0;
	
	if($('#hdnParameterCondition').val()=='<'){
		if((curr-dob)<0){
			dialogOpen=1;
		}
	}
	else if($('#hdnParameterCondition').val()=='<='){
		if((curr-dob)<=0){
			dialogOpen=1;
		}
	}
	else if($('#hdnParameterCondition').val()=='>'){
		if((curr-dob)>0){
			dialogOpen=1;
		}
	}
	else if($('#hdnParameterCondition').val()=='>='){
		if((curr-dob)>=0){
			dialogOpen=1;
		}
	}
	else if($('#hdnParameterCondition').val()=='='){
		if((curr-dob)==0){
			dialogOpen=1;
		}
	}
	if(dialogOpen==1){
		$('#hdnQuestionsFlag').val('1');
		if($('#hdnQuestionsExists').val()==1){
			$('#dialog').dialog('open');
		}
		else if($('#hdnQuestionsExists').val()==0){
			alert("No existen preguntas registradas en el sistema");
		}
	}else{
		$('#hdnQuestionsFlag').val('0');
	}
}

$(document).ready(function(){
	$('#divOpenDialog').hide();
	$('#openDialog').click(function(){
		$('#dialog').dialog('open');
	});
	$('#selCountry').change(loadCities);
	$("#txtBirthdate").blur(loadDialog);
	$("#txtBirthdate").change(loadDialog);
	setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-20y');
        
	/*VALIDATION SECTION*/
	$("#frmNewCustomer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDocument: {
				required: true,
				alphaNumeric: true,
				remote: {
					url: document_root+"rpc/remoteValidation/customer/customerDocumentIsUnique.rpc",
					type: "post"
				}
			},
			selType: {
				required: true
			},
                        rdGender: {
				required: {
                                    depends: function(element) {
                                            var display = $("#divName").css('display');
                                            if(display=='none')
                                                    return false;
                                            else
                                                    return true;
                                    }
                                }
			},
			txtFirstName: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
				
			},
			txtFirstName1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtLastName: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhone1: {
                number: true,
                minlength: 9,
                maxlength: 9
			},
			txtPhone2: {
                number: true,
                minlength: 9,
                maxlength: 9
			},
			txtCellphone: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtBirthdate: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				date: true
			},
			txtAddress: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtAddress1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtMail: {
				//required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/customer/checkCustomerEmail.rpc",
					type: "post"
				}
			},
			txtRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
			txtManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
			selStatus: {
				required: true
			},
			txtPassword: {
				required: true,
				minlength: 6
			},
			txtRePassword: {
				required: true,
				equalTo: "#txtPassword"
			},
			question_2:{}
		},
		messages: {
			txtDocument: {
				alphaNumeric: 'El campo "Documento" debe ser caracter alfanum&eacute;rico. ',
				remote: 'El documento ingresado ya existe en el sistema.'
			},
			txtFirstName: {
				textOnly: 'El campo "Nombre" debe tener s&oacute;lo caracteres.'
			},
                        rdGender: {
				required: 'El campo "G&eacute;nero" es obligatorio.'
			},
			txtLastName: {
				textOnly: 'El campo "Apellido" debe tener s&oacute;lo caracteres'
			},
			txtPhone1: {
                number: 'El campo "Tel&eacute;fono Principal" debe ser num&eacute;rico.',
                minlength: "El campo 'Tel&eacute;fono Principal' debe tener como mínimo 9 dígitos",
                maxlength: "El campo 'Tel&eacute;fono Principal' debe tener como máximo 9 dígitos"
			},
			txtPhone2: {
                number: 'El campo "Tel&eacute;fono Secundario" debe ser num&eacute;rico.',
                minlength: "El campo 'Tel&eacute;fono Secundario' debe tener como mínimo 9 dígitos",
                maxlength: "El campo 'Tel&eacute;fono Secundario' debe tener como máximo 9 dígitos"
			},
			txtCellphone: {
                number: 'El campo "Celular" debe ser num&eacute;rico.',
                required: 'El campo "Celular" es obligatorio.',
                minlength: "El campo 'Celular' debe tener como mínimo 10 dígitos",
                maxlength: "El campo 'Celular' debe tener como máximo 10 dígitos"
			},
			txtBirthdate: {
				date: 'El campo "Fecha de Nacimiento"  debe tener formato de fecha.'
			},
			txtMail: {
				email: "Por favor ingrese un e-mail con el formato nombre@dominio.com",
				remote: "El e-mail ingresado ya existe en el sistema."
			},
			txtRelative: {
				textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneRelative: {
				number: 'El campo "Tel&eacute;fono de un familiar" debe ser num&eacute;rico.'
			},
			txtManager: {
				textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneManager: {
				number: 'El campo "Tel&eacute;fono del Gerente " debe ser num&eacute;rico.'
			},
			txtPassword: {
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtRePassword: {
				equalTo: "La confirmaci&oacute;n ha fallado."
			}
		}
	});	
	/*END VALIDATION SECTION*/
	
	/*Dialog*/
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 500,
		width: 700,
		modal: true,
		buttons: {
			'Guardar': function() {
				/*VALIDATION*/
				
				$("#frmCustomerDialog").validate({
					errorLabelContainer: "#alerts_dialog",
					wrapper: "li",
					onfocusout: false,
					onkeyup: false
				});
				$("[name^=question]").each(function(){
					$(this).rules("add", {
						required: true
					});
				});
				if($('#frmCustomerDialog').valid()){
					
					addQuestions();
					$('#divOpenDialog').show();
					$(this).dialog('close');
				}
			/*END VALIDATION*/
				
				 
			},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		},
		close: function() {
			clearTxtBirthdate();		
		}
	});
	/*End Dialog*/

	/*CHANGE OPTIONS*/
	$("#selType").bind("change", function(e){
		loadCustomerForms(this.value);
	});
/*END CHANGE*/
});

        

/*
 *@Name: loadCustomerForms
 *@Description: loads a new form if the customer is a Legal Entity
 **/
function loadCustomerForms(typeCus){
	if(typeCus=='LEGAL_ENTITY'){
		$("#divName").hide();
		$("#divName2").show();
		$("#divAddress").hide();
		$("#divAddress2").show();
		$("#divRelative").hide();
		$("#divManager").show();
                $("#genderDiv").hide();
                $('[id^="rdGender"]').attr('checked', false);
	}

	if(typeCus=='PERSON'){
		$("#divName").show();
		$("#divName2").hide();
		$("#divAddress").show();
		$("#divAddress2").hide();
		$("#divRelative").show();
		$("#divManager").hide();
                $("#genderDiv").show();
	}
}