// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmEditCustomersDocument").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNewDocument: {
				required: true,
				remote: {
                        url: document_root+"rpc/remoteValidation/customer/customerDocumentIsUnique.rpc",
                        type: "post",
                        data: {
                            serialCus: function() {
                                return $("#hdnCustomerID").val();
                            },
                            txtDocument: function() {
                                return $("#txtNewDocument").val();
                            }
                        }
                    }
			},
			txtNewDocumentConf: {
				required: true,
				equalTo: '#txtNewDocument'
			}
		},
		messages: {
			txtNewDocument: {
				remote: "El documento ingresado ya existe en el sistema."
			},
			txtNewDocumentConf: {
				equalTo: "La confirmaci&oacute;n del documento no coincide."
			}
		}
	});
	/*END VALIDATION SECTION*/

	var ruta=document_root+"rpc/autocompleters/loadCustomers.rpc";
	$("#txtCustomerData").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnCustomerID").val('');
			$("#txtCustomerData").val('');
		}else{
			$("#hdnCustomerID").val(row[1]);
		}
	});


	$('#btnSearch').bind('click',function(){
		if($('#hdnCustomerID').val() != "") {
			$('#alerts').html('');
			$('#alerts').hide();
			$('#customerInfoContainer').load(document_root+'rpc/customer/loadCustomerInfo.rpc',{serial_cus:$('#hdnCustomerID').val()});
		} else {
			$('#alerts').html('<li><b>Seleccione un cliente de la lista.</b></li>');
			$('#alerts').show();
		}
	});
});