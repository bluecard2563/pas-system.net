// JavaScript Document
var pager;
$(document).ready(function(){
	$('#btnSearch').bind('click',function(){
		loadAllCustomers();
	});
	$('#txtPattern').bind('keypress',function(){
		$('#customerContainer').html('');
	});
});

/*
 * loadAllCustomers
 * @param: pattern in the HTML element 'txtPattern'
 * @return: A table of all customers that have their names like the pattern in the textfield.
 */
function loadAllCustomers(){
	var pattern=$('#txtPattern').val();

	if(pattern){
		$('#customerContainer').load(document_root+'rpc/loadCustomers/loadCustomersInfo',{pattern: pattern},function(){
			//Paginator
			if($('#customersTable').length>0){
				/*pager = new Pager('customersTable', 20 , 'Siguiente', 'Anterior');
				pager.init(20); //Max Pages
				pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); *///Starting page
					currentTable = $('#customersTable').dataTable( {
									"bJQueryUI": true,
									"sPaginationType": "full_numbers",
									"oLanguage": {
										"oPaginate": {
											"sFirst": "Primera",
											"sLast": "&Uacute;ltima",
											"sNext": "Siguiente",
											"sPrevious": "Anterior"
										},
										"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
										"sZeroRecords": "No se encontraron resultados",
										"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
										"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
										"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
										"sSearch": "Filtro:",
										"sProcessing": "Filtrando.."
									},
								   "sDom": 'T<"clear">lfrtip',
								   "oTableTools": {
									  "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
									  "aButtons": ["pdf" ]
								   }

				} );
			}
		});
	}else{
		$('#customerContainer').html('');
		alert('Debe ingresar un patr\u00F3n primero.');
	}
}
