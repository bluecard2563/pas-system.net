// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/	
	$("#frmSearchCustomer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCustomerData: {
				required: true
			}		
		}
	});
	/*END VALIDATION SECTION*/
	
	var ruta=document_root+"rpc/autocompleters/loadCustomers.rpc";
	$("#txtCustomerData").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnCustomerID").val('');
			$("#txtCustomerData").val('');
		}else{
			$("#hdnCustomerID").val(row[1]);
		}
	});
});