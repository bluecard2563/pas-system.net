// JavaScript Document
$(document).ready(function(){
	testQuestions();
	loadDialog();
	$('#selCountry').change(loadCities);
	$('#openDialog').click(function(){
		$('#dialog').dialog('open');
	});
	$("#txtBirthdate").change(loadDialog);
	setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-20y');
	$('#resetPassword').click(resetPassword);

	if($('#selType').val()=='LEGAL_ENTITY'){
		$('#hdnConfirm').val('NO');
		$('#txtBirthdate').val('');
		$("#divOpenDialog").hide();
	}

	/*VALIDATION SECTION*/
	$("#frmUpdateCustomer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDocument: {
				required: true,
				alphaNumeric: true,
				/*remote: {
					url: document_root+"rpc/remoteValidation/customer/customerDocumentIsUnique.rpc",
					type: "post",
					data: {
						serialCus: function() {
							return $("#serial_cus").val();
						}
					}
				}*/
			},
			selType: {
				required: true
			},
			txtFirstName: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}

			},
			txtFirstName1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtLastName: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhone1: {
                number: true,
                minlength: 9,
                maxlength: 9
			},
			txtPhone2: {
                number: true,
                minlength: 9,
                maxlength: 9
			},
			txtCellphone: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtBirthdate: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				date: true
			},
			txtAddress: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtAddress1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtMail: {
				//required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/customer/checkCustomerEmail.rpc",
					type: "post",
					data: {
						serialCus: function() {
							return $("#serial_cus").val();
						}
					}
				}
			},
			txtRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:{
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhoneRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:{
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhoneManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			selStatus: {
				required: true
			},
			hdnConfirm: {
				required: true
			},
			txtPassword: {
				required: {
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				minlength: {
					param:6,
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtRePassword: {
				required:  {
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				equalTo:  {
					param:"#txtPassword",
					depends: function(element) {
						var display = $("#changePassword").css('display');
				
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			}
		},
		messages: {
			txtDocument: {
				alphaNumeric: 'El campo "Documento" debe ser caracter alfanum&eacute;rico. ',
				remote: 'El documento ingresado ya existe en el sistema.'
			},
			txtFirstName: {
				textOnly: 'El campo "Nombre" debe tener s&oacute;lo caracteres.'
			},
			txtLastName: {
				textOnly: 'El campo "Apellido" debe tener s&oacute;lo caracteres'
			},
			txtPhone1: {
                number: 'El campo "Tel&eacute;fono Principal" debe ser num&eacute;rico.',
                minlength: "El campo 'Tel&eacute;fono Principal' debe tener como mínimo 9 dígitos",
                maxlength: "El campo 'Tel&eacute;fono Principal' debe tener como máximo 9 dígitos"
			},
			txtPhone2: {
                number: 'El campo "Tel&eacute;fono Secundario" debe ser num&eacute;rico.',
                minlength: "El campo 'Tel&eacute;fono Secundario' debe tener como mínimo 9 dígitos",
                maxlength: "El campo 'Tel&eacute;fono Secundario' debe tener como máximo 9 dígitos"
			},
			txtCellphone: {
                number: 'El campo "Celular" debe ser num&eacute;rico.',
                minlength: "El campo 'Celular' debe tener como mínimo 10 dígitos",
                maxlength: "El campo 'Celular' debe tener como máximo 10 dígitos"
			},
			txtBirthdate: {
				date: 'El campo "Fecha de Nacimiento"  de nacimiento debe tener formato de fecha.'
			},
			txtMail: {
				email: "Por favor ingrese un e-mail con el formato nombre@dominio.com",
				remote: "El e-mail ingresado ya existe en el sistema."
			},
			txtRelative: {
				textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneRelative: {
				digits: 'El campo "Tel&eacute;fono del Contacto" debe ser num&eacute;rico.'
			},
			txtManager: {
				textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneManager: {
				digits: 'El campo "Tel&eacute;fono del Gerente " debe ser num&eacute;rico.'
			},
			txtPassword: {
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtRePassword: {
				equalTo: "La confirmaci&oacute;n de la contrase&ntilde;a ha fallado."
			}
		},
		submitHandler: function(form){
			show_ajax_loader();
			
			form.submit();
		}
	});
	/*END VALIDATION SECTION*/
        
	//************* ANSWERS DIALOG CONFIGURATION ************
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 500,
		width: 700,
		modal: true,
		buttons: {
			'Guardar': function() {
				
				$("#frmCustomerDialog").validate({
					errorLabelContainer: "#alerts_dialog",
					wrapper: "li",
					onfocusout: false,
					onkeyup: false
				});
				
				$("[name^=question]").each(function(){
					$(this).rules("add", {
						required: true
					});
				});	
				
				if($('#frmCustomerDialog').valid()){
					addQuestions();
					$('#divOpenDialog').show();
					$(this).dialog('close'); 
				}
			},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		},
		close: function() {
			$('#hdnConfirm').val('1');
			clearTxtBirthdate();		
		}
	});

	$("#selType").bind("change", function(e){
		loadCustomerForms(this.value);
	});	
	
	/*RESET PASSWORD LINK*/
	if($('#txtMail').val()==''){
		$('#resetPassword').hide();
	}
});

function loadCities(){
	cityID=this.value;
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
		serial_cou: cityID, 
		all: 'all'
	});	
}

function dateFormat(date){
	var formatDate=date.charAt(3)+'';
	for(var i=4;i<6;i++){
		formatDate+=date.charAt(i);
	}
	for(var i=0;i<3;i++){
		formatDate+=date.charAt(i);
	}
	for(var i=6;i<date.length;i++){
		formatDate+=date.charAt(i);
	}
	return formatDate;
}

function resetPassword(){
	if(confirm('Est\u00E1 seguro que desea reestablecer la contrase\u00F1a? Se le enviar\u00E1 un e-mail al cliente con su nueva clave.')){
		$('#hdnResetPassword').val('1');
		$('#frmUpdateCustomer').submit();
	}
}

function clearTxtBirthdate(){
	$("[id^='question_']").each(function (i) {
		if(!this.value){
			$("#txtBirthdate").val("");
		}		
	});
	
}

function testQuestions(){
	var flag=0;
	$("[id^='question_']").each(function (i) {
		if(!this.value){
			flag=1;
		}		
	});
	if(flag==1){
		$('#hdnConfirm').val('');
	}
	
}

function addQuestions(){
	var x;
	x=$("#hiddenAnswers");
	x.html("");
	$("[id^='question_']").each(function (i) {
		x.append('<input type="hidden" name="'+this.id+'" id="'+this.id+'" value="'+this.value+'" />');
	});
	
	$("[id^='answer_']").each(function (i) {
		x.append('<input type="hidden" name="'+this.id+'" id="'+this.id+'" value="'+this.value+'" />');
	});
}

function loadDialog(){	
	var curr = new Date();
	curr.setFullYear(curr.getFullYear() - $('#hdnParameterValue').val());
	var dob =new Date(Date.parse(dateFormat($('#txtBirthdate').val())));
	var dialogOpen=0;

	
	if($('#hdnParameterCondition').val()=='<'){
		if((curr-dob)<0){
			dialogOpen=1;
		}
	}else if($('#hdnParameterCondition').val()=='<='){
		if((curr-dob)<=0)
		{
			dialogOpen=1;
		}
	}else if($('#hdnParameterCondition').val()=='>'){
		if((curr-dob)>0)
		{
			dialogOpen=1;
		}
	}else if($('#hdnParameterCondition').val()=='>='){
		if((curr-dob)>=0)
		{
			dialogOpen=1;
		}
	}else if($('#hdnParameterCondition').val()=='='){
		if((curr-dob)==0)
		{
			dialogOpen=1;
		}
	}
	
	
	if(dialogOpen==1){
		$('#dialog').dialog('open');                
                
		
	}else{
		$('#hdnConfirm').val('1');
		$('#divOpenDialog').hide();
	}
}

/*
 *@Name: loadCustomerForms
 *@Description: loads a new form if the customer is a Legal Entity
 **/
function loadCustomerForms(typeCus){
	if(typeCus=='LEGAL_ENTITY'){
		$("#divName").hide();
		$("#divName2").show();
		$("#divAddress").hide();
		$("#divAddress2").show();
		$("#divRelative").hide();
		$("#divManager").show();
	}

	if(typeCus=='PERSON'){
		$("#divName").show();
		$("#divName2").hide();
		$("#divAddress").show();
		$("#divAddress2").hide();
		$("#divRelative").show();
		$("#divManager").hide();
	}
}