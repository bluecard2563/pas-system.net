// JavaScript Document
var pager;
$(document).ready(function(){
/*VALIDATION SECTION*/
	$("#frmViewPotentialCustomer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone:{
					required: true
				},
			selCountry:{
					required: true
				}
		}
	});
/*END VALIDATION SECTION*/

/*CHANGE OPTIONS*/
	$("#selZone").bind("change", function(e){
	      loadCountry(this.value);
    });

	$('#btnSearch').bind('click',function(){
		if($('#frmViewPotentialCustomer').valid()){
			loadPotentialCustomers($('#selCountry').val(),$('#selCity').val());
		}
	});
});

/*
 *@Name: loadCountry
 *@Description: loads the countries of the selected zone
 **/
function loadCountry(zoneID){
	if(zoneID==""){
		$("#selCountry option[value='']").attr("selected",true);
	}

	$('#countryContainer').load(
		document_root+"rpc/loadCountries.rpc",
		{serial_zon: zoneID}, function(){
			$('#selCountry').bind('change',function(){
				loadCities(this.value);
			});
			if($("#selCountry").find('option').size()==2){
				loadCities($('#selCountry').val());
			}
		});
}

/*
 *@Name: loadCities
 *@Description: loads the cities for the selected country
 **/
function loadCities(countryID){
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID, opt_text:'Todas'});
}

/*
 *@Name: loadPotentialCustomers
 *@Description: loads the cities for the selected country
 **/
function loadPotentialCustomers(countryID,cityID){
	$('#potentialCustomers').load(
	document_root+"rpc/loadPotentialCustomers/loadPotentialCustomerList.rpc",
	{
		serial_cou: countryID, serial_cit: cityID
	},function(){
		if($('#tblPotentialCustomers').length>0){
		pager = new Pager('tblPotentialCustomers', 5 , 'Siguiente', 'Anterior');
		pager.init($('#hdnPages').val()); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}
	});
}