// JavaScript Document

$(document).ready(function(){
/*VALIDATION SECTION*/
	$("#frmUpdatePotentialCustomer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFirstName:{
				required: true,
				textOnly:true
			},
			txtLastName:{
				required: true,
				textOnly:true
			},
			txtDocument:{
				required: true,
				alphaNumeric: true,
				remote: {
					url: document_root+"rpc/remoteValidation/potentialCustomer/checkPotentialCustomerDocument.rpc",
					type: "post",
					data: {
						serialPcs: function() {
							return $("#serial_pcs").val();
						}
					}
				}
			},
			txtBirthdate:{
				date: true
			},
			selCountry:{
				required: true
			},
			selCity:{
				required: true
			},
			txtPhone:{
				required: true,
				number: true
			},
			txtCellphone:{
				number:true
			},
			txtMail:{
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/potentialCustomer/checkPotentialCustomerEmail.rpc",
					type: "post",
					data: {
						serialPcs: function() {
							return $("#serial_pcs").val();
						}
					}
				}
			},
			selStatus:{
				required:true
			}
		},
		messages: {
			txtFirstName: {
				textOnly: 'El campo "Nombre" debe tener s&oacute;lo caracteres'
			},
			txtLastName: {
				textOnly: 'El campo "Apellido" debe tener s&oacute;lo caracteres'
			},
			txtDocument: {
				alphaNumeric: 'El campo "Documento" debe ser caracter alfanum&eacute;rico. ',
				remote: 'El documento ingresado ya existe en el sistema.'
			},
			txtPhone: {
				number: 'El campo "Tel&eacute;fono Principal" debe ser num&eacute;rico.'
			},
			txtCellphone: {
				number: 'El campo "Celular" debe ser num&eacute;rico.'
			},
			txtBirthdate: {
				date: 'El campo "Fecha de Nacimiento"  debe tener formato de fecha.'
			},
			txtMail: {
				email: "Por favor ingrese un e-mail con el formato nombre@dominio.com",
				remote: "El e-mail ingresado ya existe en el sistema."
			}
		}
	});
/*END VALIDATION SECTION*/

	//CALENDARS
	setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-20y');
	//END CALENDARS

	$('#selCountry').bind('change',function(){
		loadCities(this.value);
	});
});

/*
 *@Name: loadCities
 *@Description: loads the cities for the selected country
 **/
function loadCities(countryID){
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}