// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewBillingToManagerByCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true,
				remote: {
                        url: document_root+"rpc/remoteValidation/billingToManager/hasPendingBillsByCountry.rpc",
                        type: "post"
                    }
			},
			txtDocNumber: {
				required: true,
				digits: true
			},
			hdnRemoteValidation:{
				remote: {
                        url: document_root+"rpc/remoteValidation/billingToManager/checkBillingNumber.rpc",
                        type: "post",
                        data: {
                            billing_number: function() {
                                return $('#txtDocNumber').val();
                            }
                        }
                    }
			}
		},
		messages: {
			selCountry: {
				remote: 'Almenos un representante del pa&iacute;s seleccionado tiene facturas pendientes.'
			},
			txtDocNumber: {
				digits: 'El campo "N&uacute;mero de factura" admite &uacute;nicamente n&uacute;meros.'
			},
			hdnRemoteValidation: {
				remote: 'El n&uacute;mero de factura ingresado ya existe en el sistema.'
			}
		}
	});
	/*END VALIDATION SECTION*/

	$('#selCountry').bind('change',function(){
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
	});

	$('#txtDocNumber').bind('change',function(){
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
	});
	$('#btnReport').bind('click',function(){
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
		if($("#frmNewBillingToManagerByCountry").valid){
			$("#frmNewBillingToManagerByCountry").submit();
		}
			
	});

	setDefaultCalendar($('#txtToDate'), '-1y', '-1d', '-1d');

	$('#txtToDate').bind('change', function(){
		validateDates();
	});
});

function validateDates(){
	var max_day = dayDiff($("#txtToDate").val(),$("#hdnToday").val());

	if(max_day <= 1){
		alert("La Fecha Hasta no puede ser igual a la fecha actual");
		$("#txtToDate").val('');
	}
}
