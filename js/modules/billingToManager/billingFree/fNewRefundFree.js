// JavaScript Document
var currentTable;
$(document).ready(function(){
	$('#selCountry').bind("change",function(){
				clearSales();
				loadManagersByCountry(this.value);
	});
	$('#btnSearch').bind("click",function(){
				if($("#frmNewRefundFree").valid()){
					loadSelectableSales();
				}
	});
    /*VALIDATION SECTION*/
	$("#frmNewRefundFree").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selCountry: {
                    required: true
                },
				selManager: {
                    required: true
                },
                txtBeginDate: {
                    required: true,
                    date: true
                },
                txtEndDate: {
                    required: true,
                    date: true
                }
            },
            messages:{
                txtBeginDate: {
                    date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
                }
            },
			submitHandler: function(form) {
				if($("#frmNewRefundFree").valid()){
				$('#btnSave').val('Espere por favor..');
				$('#btnSave').attr('disabled',true);	
				var sData = $('input', currentTable.fnGetNodes()).serialize();
				$('#selectedBoxes').val(sData);
				form.submit();
				}else{
					$('#btnSave').val('Guardar');
					$('#btnSave').attr('disabled',false);
				}

			}
    });
    /*END VALIDATION SECTION*/
    
    /*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
        setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
    /*END CALENDAR*/

	/* CHANGE BINDS */
	$("#txtBeginDate").bind('change', function(){
		clearSales();
		evaluateDates();
	});

	$("#txtEndDate").bind('change', function(){
		clearSales();
		evaluateDates();
	});
	/* CHANGE BINDS */
});


function loadManagersByCountry(serial_cou){
		$('#selManagerContainer').load(document_root+"rpc/billingToManager/billingFree/loadManagersByCountryWithFree.rpc",{serial_cou: serial_cou},function(){
				$('#selManager').bind("change",function(){
					clearSales();
				});
		});
}

function evaluateDates(){
	var beginDate=$('#txtBeginDate').val();
	var endDate=$('#txtEndDate').val();

	if(beginDate && endDate){
		var new_days=parseFloat(dayDiff(beginDate,endDate));

		if(new_days < 0){
			alert('La fecha fin debe ser mayor a la fecha inicio.');
			$('#txtEndDate').val('');
		}else if(new_days > 365){
			alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
		}
	}
}

function loadSelectableSales(){
	if( $('#selManager').val() != '' && $('#txtBeginDate').val()!= '' && $('#txtEndDate').val()!= ''){
		clearSales();
		serial_mbc = $('#selManager').val();
		dateFrom = $('#txtBeginDate').val();
		dateTo = $('#txtEndDate').val();
		show_ajax_loader();
		$('#managerSales').load(document_root+"rpc/billingToManager/billingFree/loadFreeSalesByManager.rpc",{serial_mbc: serial_mbc,dateFrom : dateFrom,dateTo:dateTo},function(){
				currentTable = $('#salesTable').dataTable( {
								"bJQueryUI": true,
								"sPaginationType": "full_numbers",
								"oLanguage": {
									"oPaginate": {
										"sFirst": "Primera",
										"sLast": "&Uacute;ltima",
										"sNext": "Siguiente",
										"sPrevious": "Anterior"
									},
									"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
									"sZeroRecords": "No se encontraron resultados",
									"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
									"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
									"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
									"sSearch": "Filtro:",
									"sProcessing": "Filtrando.."
								},
							   "sDom": 'T<"clear">lfrtip',
							   "oTableTools": {
								  "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
								  "aButtons": ["pdf","xls"]
							   }

			} );
			//Set table size
			$('#salesTable_wrapper').attr('class',$('#salesTable_wrapper').attr('class')+' span-24');
			hide_ajax_loader();
		});
	}else{
		alert('Llene todos los campos requeridos');
	}
}

function clearSales(){
	$('#managerSales').html('');
}