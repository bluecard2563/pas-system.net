// JavaScript Document
$(document).ready(function(){
	if($("#viewInvoiceLink").length > 0){
		$('#viewInvoiceLink').bind('click',function() {
			$.ajax({
					url: document_root+"rpc/billingToManager/deletePrintedInvoice.rpc",
					type: "post",
					dataType: "json",
					data: ({fileName: $("#hdnFileName").val()
					})
			});
			$('#errorContainer').html('');
			$('#errorContainer').hide();
		});
	}
	
	/*VALIDATION SECTION*/
	$("#frmPayBillingToManagerByCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			}
		},
		submitHandler: function(form){
			$('#btnPayBillingToManagerByCountry').val('Espere por favor..');
			$('#btnPayBillingToManagerByCountry').attr('disabled',true);
			form.submit();
		}
	});
	/*END VALIDATION SECTION*/

	$('#selCountry').bind('change',function() {
		$('#billingToManagerByCountryContainer').html('');
		
		$('#billingToManagerByCountryContainer').load(document_root+'rpc/billingToManager/loadBillingCountry.rpc',{serial_cou:this.value}, function() {
			$('#reprintBillingCountryReport').bind('click', function() {
				show_ajax_loader();
				$.getJSON(document_root+'rpc/billingToManager/reprintBillingCountryReport.rpc', {invoice_number: $('#hdnDocumentNumber').val(), serial_cou: $('#selCountry').val()}, function(data) {
					hide_ajax_loader();
					if(data) {
						window.open(document_root+"modules/billingToManager/reports/"+data, "FACTURACI&Oacute;N A REPRESENTANTES POR PAIS");
					} else {
						alert("Hubieron errores al abrir el documento.");
					}

				});
			});
		});
	});
});