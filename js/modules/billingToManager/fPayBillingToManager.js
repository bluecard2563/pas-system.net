// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmPayBillingToManager").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			}
		},
		submitHandler: function(form){
			if($("#frmPayBillingToManager").valid()){
				$('#btnPayBillingToManager').val('Espere por favor..');
				$('#btnPayBillingToManager').attr('disabled',true);
				$("#frmPayBillingToManager").submit();
			}else{
				$('#btnPayBillingToManager').val('Liquidar Factura');
				$('#btnPayBillingToManager').attr('disabled',false);
			}
		}
	});
	/*END VALIDATION SECTION*/

	$('#selCountry').bind('change',function() {
		$('#selManagerContainer').load(document_root+'rpc/billingToManager/loadManager.rpc',{serial_cou:this.value}, function() {
			$('#selManager').bind('change',function() {
				$('#billingToManagerContainer').load(document_root+'rpc/billingToManager/loadBillingToManager.rpc',{serial_man:this.value});
			});
		});
	});
});
