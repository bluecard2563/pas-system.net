// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewBillingToManager").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true,
				remote: {
                        url: document_root+"rpc/remoteValidation/billingToManager/hasPendingBills.rpc",
                        type: "post",
                        data: {
                            serial_man: function() {
                                return $('#selManager').val();
                            }
                        }
                    }
			},
			txtDocNumber: {
				required: true,
				digits: true/*,
				remote: {
                        url: document_root+"rpc/remoteValidation/billingToManager/checkBillingNumber.rpc",
                        type: "post",
                        data: {
                            billing_number: function() {
                                return $('#txtDocNumber').val();
                            }
                        }
                    }*/
			},
			hdnRemoteValidation:{
				remote: {
                        url: document_root+"rpc/remoteValidation/billingToManager/checkBillingNumber.rpc",
                        type: "post",
                        data: {
                            billing_number: function() {
                                return $('#txtDocNumber').val();
                            }
                        }
                    }
			}
		},
		messages: {
			selManager: {
				remote: 'El representante seleccionado tiene facturas pendientes.'
			},
			txtDocNumber: {
				digits: 'El campo "N&uacute;mero de factura" admite &uacute;nicamente n&uacute;meros.'
			},
			hdnRemoteValidation: {
				remote: 'El n&uacute;mero de factura ingresado ya existe en el sistema.'
			}
		}
	});
	/*END VALIDATION SECTION*/

	$('#selCountry').bind('change',function(){
		$('#selManagerContainer').load(document_root+'rpc/billingToManager/loadManager.rpc',{serial_cou:this.value}, function(){
			$('#selManager').bind('change',function(){
				$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
			});
		});
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
	});

	$('#selManager').bind('change',function(){
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
	});

	$('#txtDocNumber').bind('change',function(){
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
	});
	$('#btnReport').bind('click',function(){
		$('#hdnRemoteValidation').val(parseInt($('#hdnRemoteValidation').val())+1);
		if($("#frmNewBillingToManager").valid){
			$("#frmNewBillingToManager").submit();
		}
			
	});
	setDefaultCalendar($('#txtFromDate'), '-1y', '-1d', '-1d');
	setDefaultCalendar($('#txtToDate'), '-1y', '-1d', '-1d');

	$('#txtToDate').bind('change', function(){
		validateDates();
	});
});

function validateDates(){
	var max_day = dayDiff($("#txtToDate").val(),$("#hdnToday").val());

	if(max_day <= 1){
		alert("La Fecha Hasta no puede ser igual a la fecha actual");
		$("#txtToDate").val('');
	}
}
