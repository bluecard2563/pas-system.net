// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmReprintBill").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			txtInvoiceNumber: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/

	$('#selCountry').bind('change',function() {
		$('#selManagerContainer').load(document_root+'rpc/billingToManager/loadManager.rpc',{serial_cou:this.value});
		clear_submit_button();
	});
	
	$('#selManager').bind('change',function() {
		clear_submit_button();
	});
	
	$('#txtInvoiceNumber').bind('keypress',function() {
		clear_submit_button();
	});
	
	$('#btnSubmit').bind('click', function(){
		if($("#frmReprintBill").valid()){
			$('#btnSubmit').val('Espere por favor..');
			$('#btnSubmit').attr('disabled',true);
			$("#frmReprintBill").submit();
		}else{
			clear_submit_button();
		}
	});
});

function clear_submit_button(){
	$('#btnSubmit').val('Reimprimir Factura');
	$('#btnSubmit').attr('disabled',false);
}
