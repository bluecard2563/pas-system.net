// JavaScript Document
$(document).ready(function() {
    $("#frmUploadReceipts").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtReceipts: {
                required: true
            },
            txtFrom: {
                required: true
            },
            txtTo: {
                required: true
            }
        }
    });
    
    $('#btnRegister').bind('click', function(){
       if($("#frmUploadReceipts").valid()) {
           if(validateDates()){
               $('#frmUploadReceipts').submit(); 
           }
       } 
    });

    setDefaultCalendar($("#txtFrom"), '-5y', '+0d');
    setDefaultCalendar($("#txtTo"), '-5y', '+0d');
});

function validateDates() {
    var days = dayDiff($("#txtFrom").val(), $("#txtTo").val());
    var date = new Date();
    var today = (date.getDate()) + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    if (days < 0) {
        alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
        return false;
    }
    if (days > 30) {
        alert("El rango de fechas no debe ser mayor a 30 d\u00EDas.");
        return false;
    }
    if (dayDiff($("#txtFrom").val(), today) <= 0 || dayDiff($("#txtTo").val(), today) <= 0) {
        alert("La fecha ingresada no puede ser mayor a la fecha actual.");
        return false;
    }
    return true;
}