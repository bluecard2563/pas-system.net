// JavaScript Document
$(document).ready(function(){
	$("#frmSelectSale").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCardNumber:{
				required: true,
				digits:true
			}
		},
		messages: {
			txtCardNumber:{
				required:"Ingrese un n&uacute;mero de tarjeta.",
				digits:"El campo 'N&uacute;mero de Tarjeta' acepta solo d&iacute;gitos.",
			}
		},
		submitHandler: function(form) {
			show_ajax_loader();
			form.submit();
		}
	});
	
	$('#btnRegister').bind('click', function(){
		if($("#frmSelectSale").valid()){
			validateCardWithERP();
		}
	});
	
	$('#txtCardNumber').bind('keypress', function(){
		$('#OODataContainer').html('');
	});
});

function validateCardWithERP(){
	show_ajax_loader();
	$('#OODataContainer').load(document_root + 'rpc/erpLink/validateCardInfoOnERP.rpc', 
		{
			card_number: function(){
				return $('#txtCardNumber').val();
			}
		}, function(){
			hide_ajax_loader();
		});
}

