$(document).ready(function(){
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmUpdateFreelance").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFirstname: {
				required: true
			},
			txtDocument: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/freelance/checkFreelanceDocument.rpc",
					type: "POST",
					data: {
						  txtSerial_fre: function() {
							return $("#txtSerial_fre").val();
						  }
					}
				}
			},
			txtPhone1: {
				required: true,
				digits: true
			},
			txtPhone2: {
				digits: true
			},
			txtMail: {
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/freelance/checkFreelanceEmail.rpc",
					type: "post",
					data: {
						  serial_fre: function() {
							return $("#txtSerial_fre").val();
						  }
					}
				}
			},
			txtComission: {
				required: true,
				number: true,
				max: 100
			},
			txtUrl:{
				required: true,
				url: true
			},
			selStatus: {
				required: true
			}
		},
		messages: {
			txtDocument: {
				remote: "El 'RUC' ingresada ya existe."
			},
			txtPhone1: {
				digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'."
			},
			txtPhone2: {
				digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'."
			},
		    txtMail: {
       			email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe."
     		},
			txtComission: {
				number: 'El campo "% Comisi&oacute;n" admite s&oacute;lo n&uacute;meros',
				max: 'El campo "% Comisi&oacute;n" no puede ser mayor a 100'
			},
			txtUrl: {
				url: 'Ingrese una direcci&oacute;n v&aacute;lida en el campo "URL".'
			}
   		}
	});
});