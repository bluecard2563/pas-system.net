// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmPayPhoneInvoice").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                txtPhoneNum: {
                    required: true,
                    digits: true
                },
                txtRegionCode: {
                    required: true,
                    digits: true,
                    maxlength: 3
                }
            },
            messages: {
                txtPhoneNum: {
                    digits: 'El campo "N&uacute;mero de Tel&eacute;fono" admite solo n&uacute;meros.'
                },
                txtRegionCode: {
                    digits: 'El campo "C&oacute;digo de &Aacute;rea" admite solo n&uacute;meros.',
                    maxlength: 'El campo "C&oacute;digo de &Aacute;rea" admite hasta 3 n&uacute;meros.'
                }
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#invoicesResumeTable').dataTable( {
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "oLanguage": {
                "oPaginate": {
                        "sFirst": "Primera",
                        "sLast": "&Uacute;ltima",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                },
                "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                "sSearch": "Filtro:",
                "sProcessing": "Filtrando.."
        },
        "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
                "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
                "aButtons": ["pdf" ]
        }
    });


    $('#payInvoices').bind("click", function() {
        if($("#frmPayPhoneInvoice").valid()) {
            $('#payInvoices').attr('disabled', true);
            show_ajax_loader();
            $("#frmPayPhoneInvoice").submit();
        }
    });
    /*END JQUERY EVENTS*/
});
