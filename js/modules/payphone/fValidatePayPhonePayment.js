// JavaScript Document
var currentTable;
$(document).ready(function () {
    /*VALIDATION SECTION*/
    $("#frmValidatePPhonePayment").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        submitHandler: function (form) {
            form.submit();
        }
    });
    /*END VALIDATION SECTION*/

    if ($('#selCountry option').size() <= 2 && $('#selCountry option').size() > 1) {
        loadManagers($('#selCountry').val());
    } else {
        if ($('#selCity option').size() <= 2) {
            loadResponsables($('#selCity').val(), $('#hdnSerialMbc').val());
        }
        $('#selCity').bind("change", function () {
            loadResponsables(this.value, $('#hdnSerialMbc').val())
        });
    }
    
    $('#selCountry').bind("change", function () {
        loadManagers(this.value);
    });

    if ($('#selectedDealer').val()){
        actionButtons();
    }
});

function loadManagers(countryID) {
    $('#managerContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentManager.rpc", {
        serial_cou: countryID
    }, function () {
        if ($('#selManager option').size() <= 2 && $('#selCountry option').size() > 1) {
            loadCities(countryID, $('#selManager').val());
        }
        $('#selManager').bind("change", function () {
            loadCities(countryID, this.value);
        });
    });
}

function loadCities(countryID, managerID) {
    $('#cityContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentCities.rpc", {
        serial_cou: countryID,
        serial_mbc: managerID
    }, function () {
        if ($('#selCity option').size() <= 2 && $('#selCountry option').size() > 1) {
            loadResponsables($('#selCity').val(), managerID);
        }
        $('#selCity').bind("change", function () {
            loadResponsables(this.value, managerID)
        });
    });
}

function loadResponsables(cityID, managerID) {
    $('#responsibleContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentResponsables.rpc", {
        serial_cit: cityID,
        serial_mbc: managerID
    }, function () {
        if ($('#selResponsible option').size() <= 2 && $('#selResponsible option').size() > 1) {
            loadDealers(cityID, $('#selResponsible').val(), managerID);
        }
        $('#selResponsible').bind("change", function () {
            loadDealers(cityID, this.value, managerID);
            $('#selBranch').html('<option value="">- Seleccione -</option>');
            $('#invoiceContainer').html('');
        });
    });
}

function loadDealers(serial_cit, userID, managerID) {
    $('#dealerContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentDealers.rpc", {
        serial_cit: serial_cit,
        serial_usr: userID,
        serial_mbc: managerID
    }, function () {
        if ($('#selDealer option').size() <= 2 && $('#selDealer option').size() > 1) {
            loadBranches($('#selDealer').val(), userID);
        }
        $('#selDealer').bind("change", function () {
            loadBranches(this.value, userID);
        });
    });
}

function loadBranches(dealerID, userID) {
    $("#branchContainer").load(document_root + "rpc/payments/creditCardPayments/loadPaymentDealers.rpc", {
        serial_dea: dealerID,
        serial_usr: userID
    }, function () {
        if ($('#selBranch option').size() <= 2 && $('#selBranch option').size() > 1) {
            loadInvoices($('#selBranch').val());
        }
        $('#selBranch').bind("change", function () {
            loadInvoices(this.value);
        });
    });
}

function loadInvoices(branchID) {
    $('#invoiceContainer').load(document_root + "rpc/payments/payphonePayments/loadPayPhoneInvoicesForApprByBranch.rpc", {
        serial_dea: branchID,
        is_payphone: true
    }, function () {
        actionButtons();
    });
}


function actionButtons(){
    if ($("#payedInvoices").length > 0) {
        /*Style dataTable*/
        currentTable = $('#payedInvoices').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "Primera",
                    "sLast": "&Uacute;ltima",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                "sSearch": "Filtro:",
                "sProcessing": "Filtrando.."
            },
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sSwfPath": document_root + "img/dataTables/copy_cvs_xls_pdf.swf",
                "aButtons": ["pdf"]
            }
        });
    }
        
    $('#vldInvoices').bind("click", function () {
        if ($("#frmValidatePPhonePayment").valid()) {
            $('#vldInvoices').attr('disabled', true);
            show_ajax_loader();
            $("#frmValidatePPhonePayment").submit();
        }
    });
}
