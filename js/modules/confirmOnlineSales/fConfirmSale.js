$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	$("#frmConfirmSale").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtSerialInput: {
				required: true,
				number: true
			}
		}
	});
	/*END VALIDATION*/

	$('#txtSerialInput').bind('keypress',function(){
		$('#messageDiv').html('');
		$('#messageDiv').removeClass('error');
		$('#messageDiv').removeClass('success');
	});
});