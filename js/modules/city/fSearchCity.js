// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchCity").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true									
			}		
		}
	});
	/*END VALIDATION SECTION*/
        $('#selZone').change(function(){
            loadCountries($('#selZone').val());
            loadCities('');
        });
});

function loadCountries(zoneId){
    if(zoneId==""){
            $("#selCountry option[value='']").attr("selected",true);
    }
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
        $('#selCountry').change(function(){loadCities($('#selCountry').val())});

		if($('#selCountry option').size()<=2){
			loadCities($('#selCountry').val());
		}
    });
}
function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}
