// JavaScript Document
function loadCountry(){
	zoneID=this.value;
	if(zoneID!=''){
		$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneID, charge_country:0, view_all: 'YES'});	
	}
}

function changeFlag(){
	$('#hdnFlagCity').val($('#hdnFlagCity').val()+1);
}

$(document).ready(function(){
	$('#selZone').bind("change",loadCountry);
	$("#txtNameCity").bind("change",changeFlag);
	
	/*VALIDATION SECTION*/
	$("#frmNewCity").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                hdnFlagCity: {
                    remote: {
                        url: document_root+"rpc/remoteValidation/city/checkNameCity.rpc",
                        type: "post",
                        data: {
                            serial_cou: function() {
                                return $("#selCountry").val();
                            },
                            txtNameCity: function() {
                                return $("#txtNameCity").val();
                            }
                        }
                    }
                },
                txtCodeCity: {
                   required: true,
                   maxlength: 3,
                   remote: {
                        url: document_root+"rpc/remoteValidation/city/checkCodeCity.rpc",
                        type: "post"
                    }
                },
                txtNameCity: {
                        required: true,
                        textOnly: true,
                        maxlength: 30
                }
            },
            messages: {
                txtCodeCity: {
                    maxlength: 'El campo "C&oacute;digo ciudad" debe tener m&aacute;ximo 3 caracteres.',
                    remote:'El c&oacute;digo de ciudad ingresado ya existe para otra ciudad.'
                },
                txtNameCity: {
                    textOnly: 'S&oacute;lo se aceptan caracteres en el campo "Nombre" Ciudad.',
                    maxlength: 'El campo "Nombre" debe tener m&aacute;ximo 30 caracteres.'
                },
                hdnFlagCity: {
                    remote:'La ciudad ingresada ya existe en el pa&iacute;s seleccionado.'
                }
            }
    });
    /*END VALIDATION SECTION*/
}
);