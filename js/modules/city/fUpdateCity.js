// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmUpdateCity").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtCodeCity: {
               required: true,
               maxlength: 3,
               remote: {
                    url: document_root+"rpc/remoteValidation/city/checkCodeCity.rpc",
                    type: "post",
                    data: {
                        serial_cit: function() {
                                return $("#hdnSerial_cit").val();
                        }
                    }
                }
            },
            txtNameCity:{
                required: true,
                textOnly: true,
                maxlength: 30,
                remote: {
                    url: document_root+"rpc/remoteValidation/city/checkNameCity.rpc",
                    type: "post",
                    data: {
                        serial_cit: function() {
                                return $("#hdnSerial_cit").val();
                        },
                        serial_cou: function() {
                                return $("#selCountry").val();
                        }
                    }
                }
            }
        },
        messages: {
            txtCodeCity: {
                maxlength: 'El campo "C&oacute;digo ciudad" debe tener m&aacute;ximo 3 caracteres.',
                remote:'El c&oacute;digo de ciudad ingresado ya existe para otra ciudad.'
            },
            txtNameCity: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre Ciudad'.",
                maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
                remote: "Ya existe una ciudad con ese nombre"
            }
        }
    });
    /*END VALIDATION SECTION*/
});