//Java Script
var pager;
$(document).ready(function(){
	$("#frmMedicalCheckup").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtObservations: {
				required: true
			}
		},submitHandler: function(form){

					$('#btnSubmit').val('Espere por favor..');
					$('#btnSubmit').attr('disabled','true');

					form.submit();
				
			

		}
	});
	
	$("#btnSubmit").bind("click",function(){

		/*valid if the extension is correct*/
		$("#alerts").empty();
		$("#alerts").hide();

		var valor=$("#medical_checkup").val().split(".");
		var	aext=["pdf"];
		var band='';

			for(var i=0;i<=aext.length-1;i++){
				if(valor[valor.length-1] == aext[i] ){
					 band="ok";
					 break;
				}
			}
			if(band!="ok"){
				$("#alerts").append('<li id="errorExt"><label>La extensi&oacute;n del archivo no es v&aacute;lida </label> </li>');
				$("#alerts").show();
				
			}else{
				$("#frmMedicalCheckup").submit();
			}
	})

	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 500,
		width: 680,
		resizable: false,
		title: 'Preguntas de Adulto Mayor',
		modal: true,
		buttons: {
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});

	$('#seniorQuestions').bind('click',function(){
		$("#dialog").dialog('open');
	});
	
	 $("#blog_tabs").tabs({ 
		 heightStyle: "auto" 
	 });

	if($('#documents').length>0){
		pager = new Pager('documents', 5 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}

	if($('#filesTable').length>0){
		pager = new Pager('filesTable', 5 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPositionFiles'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}
	
	if($('#servicesTable').length>0){
		pager = new Pager('servicesTable', 5 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPositionServices'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}
});