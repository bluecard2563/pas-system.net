// JavaScript Document
//var pager;
//var pager2;
var amountSum=0;
var currentTable;

$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmDispatchClaims").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selBillTo: {
				required: true
			},
			selProvider:{
				required: {
					depends: function(element) {
						return $('#providerContainer').css("display") == "block";
					}
				}
			},
			selZone: {
				required: {
					depends: function(element) {
						return $('#billToClientContainer').css("display") == "block";
					}
				}
			},
			selCountry: {
				required: {
					depends: function(element) {
						return $('#billToClientContainer').css("display") == "block";
					}
				}
			},
			selCity: {
				required: {
					depends: function(element) {
						return $('#billToClientContainer').css("display") == "block";
					}
				}
			},
			selCustomer: {
				required: {
					depends: function(element) {
						return $('#billToClientContainer').css("display") == "block";
					}
				}
			},
			selCard: {
				required: {
					depends: function(element) {
						return $('#billToClientContainer').css("display") == "block";
					}
				}
			},
			flePaymentRequest: {
				required: {
					depends: function(element) {
						return $('#documentsContainer').css("display") == "block";
					}
				}
			}
		},
		submitHandler: function(form) {
			var sData = $("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).serialize();
			$('#selectedBoxes').val(sData);
			form.submit();

		}
	});
	/*END VALIDATION SECTION*/

	$('#selBillTo').bind("change",function(){
		/*RESET DATA*/
		$('#documentsContainer').hide();
		$('#customerPreLiquidation').hide();
		$('#preLiquidationContainer').hide();
		$('#divAmountSum').hide();
		$('#btnLiquidateContainer').hide();
		$('#directPreLiquidation').hide();
		$('#paymentRequestContainer').html("");
		$('#providerContainer').hide();
		$("#selProvider option[value='']").attr("selected",true);
		$('#billToClientContainer').hide();
		$("#selZone option[value='']").attr("selected",true);
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCustomer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCard').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#filesContainer').html('');
        
		/*END RESET DATA*/

		if(this.value == "DIRECT") { //IF THE LIQUIDATION PAYMENT IS DIRECT
			show_ajax_loader();
			$('#paymentRequestContainer').load(document_root+"rpc/assistance/loadAssistanceClaims.rpc",{
				document_to_dbf:this.value,
                processAS: 'PAYABLE'
			},function(){

				if($('#claims_table').length>0) {
					$('#divAmountSum').show();
					$('#btnLiquidateContainer').show();
					$('#preLiquidationContainer').show();

					/*Style dataTable*/
					currentTable = $('#claims_table').dataTable( {
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"oLanguage": {
							"oPaginate": {
								"sFirst": "Primera",
								"sLast": "&Uacute;ltima",
								"sNext": "Siguiente",
								"sPrevious": "Anterior"
							},
							"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
							"sZeroRecords": "No se encontraron resultados",
							"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
							"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
							"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
							"sSearch": "Filtro:",
							"sProcessing": "Filtrando.."
						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
							"aButtons": ["pdf","xls" ]
						}

					} );

					$("input[id^='chkClaims']", currentTable.fnGetNodes()).click( function () {
						if(!$(this).attr("checked")) {
							$('#selAll').attr("checked", false);
						}
						amountSum=0;
						$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
							amountSum+= parseFloat( $(this).attr('amount'));
						});
						$("#amountSum").text(amountSum.toFixed(2));

					});

					$('#documentsContainer').css("display","block");


				} else {
					$('#divAmountSum').hide();
					$('#btnLiquidateContainer').hide();
					$('#preLiquidationContainer').hide();
				}
				$("a[id^='viewDocument']").bind("click", function(){
					$("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{
						serial_dbf:$(this).attr("serial")
					});
					$("#fileViewBenefitsDialog").dialog("open");
				});
				hide_ajax_loader();
			});
	
		} else if(this.value == "PROVIDER") { //IF THE LIQUIDATION PAYMENT GOES TO THE SERVICE PROVIDER
			$('#paymentRequestContainer').html("");
			$('#providerContainer').css("display","block");
			
		} else if(this.value == "CLIENT") { //IF THE LIQUIDATION PAYMENT GOES TO THE CUSTOMER
			$('#billToClientContainer').css("display","block");
		}
	});

	$('#selProvider').bind("change",function(){
		if(this.value == "") {
			$('#paymentRequestContainer').html("");
			$('#preLiquidationContainer').hide();
			$('#divAmountSum').hide();
			$('#btnLiquidateContainer').hide();
			$('#documentsContainer').hide();
		} else {
                        show_ajax_loader();
			$('#paymentRequestContainer').load(document_root+"rpc/assistance/loadAssistanceClaims.rpc",{
				document_to_dbf:$('#selBillTo').val(),
				serial_spv:this.value,
                processAS: 'PAYABLE'
			},function(){
				if($('#claims_table').length>0) {

					/*Style dataTable*/
					currentTable = $('#claims_table').dataTable( {
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"oLanguage": {
							"oPaginate": {
								"sFirst": "Primera",
								"sLast": "&Uacute;ltima",
								"sNext": "Siguiente",
								"sPrevious": "Anterior"
							},
							"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
							"sZeroRecords": "No se encontraron resultados",
							"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
							"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
							"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
							"sSearch": "Filtro:",
							"sProcessing": "Filtrando.."
						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
							"aButtons": ["pdf","xls" ]
						}

					} );
				}
				$('#btnLiquidateContainer').show();
				$('#divAmountSum').show();
				
				$('#selAll').bind("click",function(){
					if($('#selAll').attr("checked")) {
						//OLD $("input[id^='chkClaims']", currentTable.fnGetNodes()).attr("checked", true);
						$("input[id^='chkClaims']", currentTable.fnGetFilteredNodes()).attr("checked", true);
						amountSum=0;
						//OLD $("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
						$("input:checked[id^='chkClaims']", currentTable.fnGetFilteredNodes()).each(function(){
							amountSum+= parseFloat( $(this).attr('amount'));
						});
						$("#amountSum").text(amountSum.toFixed(2));
					} else {
						$("input[id^='chkClaims']", currentTable.fnGetNodes()).attr("checked", false);
						amountSum=0;
						$("#amountSum").text(amountSum.toFixed(2));
					}
				});

				$("input[id^='chkClaims']", currentTable.fnGetNodes()).click( function () {
					if(!$(this).attr("checked")) {
						$('#selAll').attr("checked", false);
					}
					amountSum=0;
					$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
						amountSum+= parseFloat( $(this).attr('amount'));
					});
					$("#amountSum").text(amountSum.toFixed(2));

				});


				$("a[id^='viewDocument']").bind("click", function(){
					$("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{
						serial_dbf:$(this).attr("serial")
					});
					$("#fileViewBenefitsDialog").dialog("open");
				});
				$('#preLiquidationContainer').show();
				$('#documentsContainer').show();

				hide_ajax_loader();
			});			
		}
		
	});

	$('#selZone').bind("change",function(){
		loadCountries(this.value);
		//Reset Data
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCustomer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCard').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#filesContainer').html('');
		$('#paymentRequestContainer').html('');
	});
        
        $('#btnPreLiquidatePDF').bind("click",function(){
		var one_required=false;
		$('[id^="chkClaims"]', currentTable.fnGetNodes()).each(function(){
			if($(this).is(':checked')){
				one_required=true;
				return;
			}
		});

		if(one_required){
			$('#messageText').html('Abriendo archivo');
			$('#progressDialog').dialog('open');
			var claims="";
			$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
				claims+=this.value+",";
			});
			
			$.ajax({
				url: document_root + "rpc/assistance/createLiquidationDocument.rpc",
				type: "POST",
				data: {
					selProvider: $('#selProvider').val(),
					liquidationType: 'preliquidation',
					chkClaims: claims.substr(0, claims.length-1)
				},
				success: function(data){
					$('#progressDialog').dialog('close');
					if(data) {
						data = str_replace("\"", "", data);
						//Show the pre-liquidation document
						if(window.open(document_root+"assistanceFiles/assistanceLiquidations/"+data+".pdf")) {
						//Delete the pre-liquidation document from the server
						/*$.ajax({
								url:document_root+"rpc/unlinkFile.rpc",
								data: "file_name=assistanceFiles/assistanceLiquidations/"+data+".pdf"
							});*/
						}
					}else{
						alert('Hubo errores en el proceso. Por favor vuelva a intentarlo.');
					}
				}
			});
		}else{
			alert('Seleccione al menos un documento.');
		}
	});
        
        $('#btnPreLiquidateXLS').bind("click",function(){
		var one_required=false;
		$('[id^="chkClaims"]', currentTable.fnGetNodes()).each(function(){
                    if($(this).is(':checked')){
                        one_required=true;
                        return;
                    }
		});

		if(one_required){
                        var claims="";
                        $("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
                                claims+=this.value+",";
                        });
                        
                        $('#liquidationType').val('preliquidation');
                        $('#chkClaims').html(claims.substr(0, claims.length-1));
                        $( "#flePaymentRequest" ).rules( "remove" );

                        if($("#frmDispatchClaims").valid()){
                            $("#frmDispatchClaims").attr("action", document_root + "modules/assistance/pLiquidationDocumentXLS.php");
                            $("#frmDispatchClaims").submit();
                        }
                        
		}else{
			alert('Seleccione al menos un documento.');
		}
	});

	$('#btnCustomerPreLiquidation').bind('click',function(){
		var one_required=false;
		$('[id^="chkClaims"]', currentTable.fnGetNodes()).each(function(){
			if($(this).is(':checked')){
				one_required=true;
				return;
			}
		});

		if(one_required){
			$('#messageText').html('Abriendo archivo');
			$('#progressDialog').dialog('open');
			var claims="";
			$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
				claims+=this.value+",";
			});
			
			$.ajax({
				url: document_root+ 'rpc/assistance/createCustomerPayOffDocument.rpc',
				type: "POST",
				data: { 
					chkClaims: claims.substr(0, claims.length-1),
					pre_liquidation: 'yes'
				},
				success: function(data){
					data = str_replace('"', '', data);
					$('#progressDialog').dialog('close');
					//Show the pay-off document
					if(window.open(document_root+"assistanceFiles/assistanceLiquidations/"+data+".pdf","mywindow")) {
						//Delete the pay-off document from the server
						$.ajax({
							url:document_root+"rpc/unlinkFile.rpc",
							data: "file_name=assistanceFiles/assistanceLiquidations/"+data+".pdf", 
							success: function(data){
							//$('#frmDispatchClaims').submit();
							}
						});
					}
				}
			});
		}else{
			alert('Seleccione al menos un documento.');
		}
	});
    
	$('#btnLiquidate').bind("click",function(){
		if($('#frmDispatchClaims').valid()) {
			$('#btnLiquidate').attr('disabled','true');
			$('#btnLiquidate').val('Espere por favor');
			$('#messageText').html('Enviando archivo');
			$('#progressDialog').dialog('open');
			var claims="";
			$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
				claims+=this.value+",";
			});

			//Creates liquidation document
			$.ajax({
				url: document_root+ 'rpc/assistance/createLiquidationDocument.rpc',
				type: "POST",
				data: { 
					selProvider: $('#selProvider').val(),
					liquidationType: 'liquidation',
					chkClaims: claims.substr(0, claims.length-1)
				},
				success: function(data){
					$('#progressDialog').dialog('close');
					if(data) {
						//Creates customer pay-off document
						if($('#selBillTo').val()=='CLIENT') {
							$.ajax({
								url: document_root+ 'rpc/assistance/createCustomerPayOffDocument.rpc',
								type: "POST",
								data: { 
									chkClaims: claims.substr(0, claims.length-1)
								},
								success: function(data){
									data = str_replace('"', '', data);
									//Show the pay-off document
									if(window.open(document_root+"assistanceFiles/assistanceLiquidations/"+data+".pdf","mywindow")) {
										//Delete the pay-off document from the server
										$.ajax({
											url:document_root+"rpc/unlinkFile.rpc",
											data: "file_name=assistanceFiles/assistanceLiquidations/"+data+".pdf", 
											success: function(data){
												$('#frmDispatchClaims').submit();
											}
										});
									}
								}
							});
						} else {
							$('#frmDispatchClaims').submit();
						}
					} else {
						$('#innerOtherMessages').html('Hubo errores al enviar liquidaci&oacute;n.');
						$('#innerOtherMessages').addClass('error');
						$('#otherMessages').css("display", "block");
					}
				}
			});
		}
	});

	/*FILE BENEFITS DIALOG*/
	$('#fileViewBenefitsDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 350,
		width: 710,
		modal: true,
		stack: true,
		resizable: false,
		title:"Beneficios por archivo",
		buttons: {
			'Salir': function() {
				$(this).dialog('close');
			}
		}
	});
	/*END FILE BENEFITS DIALOG*/


	/*PROGRESS DIALOG*/
	$("#progressDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		modal: true,
		stack: true,
		resizable: false,
		closeOnEscape: false,
		open: function(event, ui) {
			$(".ui-dialog-titlebar-close").hide();
		},
		title:"Espere por favor"
	});
/*END PROGRESS DIALOG*/
});

function loadCountries(serial_zon) {
	$('#countryContainer').load(document_root+"rpc/assistance/loadCountriesWithPendingClaims.rpc",{
		serial_zon: serial_zon
	},function(){
		$('#selCountry').bind("change",function(){
			loadCities(this.value);
			$('#selCustomer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selCard').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#filesContainer').html('');
			$('#paymentRequestContainer').html('');
		});

		if($('#selCountry option').length==2){
			loadCities($('#selCountry').val());
		}
	});
}

function loadCities(serial_cou) {
	$('#cityContainer').load(document_root+"rpc/assistance/loadCitiesWithPendingClaims.rpc",{
		serial_cou: serial_cou
	}, function() {
		$('#selCity').bind("change",function(){
			loadCustomers(this.value);
            
			$('#selCard').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#filesContainer').html('');
			$('#paymentRequestContainer').html('');
		});

		if($('#selCity option').length==2){
			loadCustomers($('#selCity').val());
		}
	});
}

function loadCustomers(serial_cit) {
	$('#customerContainer').load(document_root+"rpc/assistance/loadCustomersWithPendingClaims.rpc",{
		serial_cit: serial_cit
	}, function() {
		$('#selCustomer').bind("change",function(){
			loadCards(this.value);
			$('#filesContainer').html('');
			$('#paymentRequestContainer').html('');
		});

		if($('#selCustomer option').length==2){
			loadCards($('#selCustomer').val());
		}
	});
}

function loadCards(serial_cus) {
	$('#cardContainer').load(document_root+"rpc/assistance/loadCardsWithPendingClaims.rpc",{
		serial_cus: serial_cus
	}, function() {
		$('#selCard').bind("change",function(){
			loadFiles(this.value);
			$('#paymentRequestContainer').html('');
		});

		if($('#selCard option').length==2){
			loadFiles($('#selCard').val());
		}
	});
}

function loadFiles(card_number) {
	$('#filesContainer').load(document_root+"rpc/assistance/loadFilesByCard.rpc",{
		card_number: card_number
	}, function() {
		if($('#files_table').length>0) {
			pager2 = new Pager('files_table', 10 , 'Siguiente', 'Anterior');
			pager2.init(Math.floor($("#hdnFileCount").val()/10)+1); //Max Pages
			pager2.showPageNav('pager2', 'pageNavPositionFiles'); //Div where the navigation buttons will be placed.
			pager2.showPage(1); //Starting page
		}
		$("input[id^='rdoFiles']").bind("click", function() {
			show_ajax_loader();
			$('#paymentRequestContainer').load(document_root+"rpc/assistance/loadAssistanceClaims.rpc",{
				document_to_dbf:$('#selBillTo').val(),
				serial_fle:this.value,
                processAS: 'PAYABLE'
			},function(){
				if($('#claims_table').length>0) {
					/*Style dataTable*/
					currentTable = $('#claims_table').dataTable( {
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"oLanguage": {
							"oPaginate": {
								"sFirst": "Primera",
								"sLast": "&Uacute;ltima",
								"sNext": "Siguiente",
								"sPrevious": "Anterior"
							},
							"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
							"sZeroRecords": "No se encontraron resultados",
							"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
							"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
							"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
							"sSearch": "Filtro:",
							"sProcessing": "Filtrando.."
						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
							"aButtons": ["pdf","xls" ]
						}

					} );

				} else {
					$('#customerPreLiquidation').hide();
					$('#btnLiquidateContainer').hide();
					$('#divAmountSum').hide();
					$('#documentsContainer').hide();
				}

				$('#selAll').bind("click",function(){
					if($('#selAll').attr("checked")) {
						$("input[id^='chkClaims']", currentTable.fnGetNodes()).attr("checked", true);
						amountSum=0;
						$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
							amountSum+= parseFloat( $(this).attr('amount'));
						});
						$("#amountSum").text(amountSum.toFixed(2));
					} else {
						$("input[id^='chkClaims']", currentTable.fnGetNodes()).attr("checked", false);
						amountSum=0;
						$("#amountSum").text(amountSum.toFixed(2));
					}
				});

				$("input[id^='chkClaims']", currentTable.fnGetNodes()).click( function () {
					if(!$(this).attr("checked")) {
						$('#selAll').attr("checked", false);
					}
					amountSum=0;
					$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
						amountSum+= parseFloat( $(this).attr('amount'));
					});
					$("#amountSum").text(amountSum.toFixed(2));

				});

				$("a[id^='viewDocument']").bind("click", function(){
					$("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{
						serial_dbf:$(this).attr("serial")
					});
					$("#fileViewBenefitsDialog").dialog("open");
				});

				$('#customerPreLiquidation').show();
				$('#divAmountSum').show();
				$('#btnLiquidateContainer').show();
				$('#documentsContainer').show();
				hide_ajax_loader();
			});

		});
	});
}