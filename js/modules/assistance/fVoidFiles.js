// JavaScript Document
var currentTable;
$(document).ready(function(){
	var card_number;
	
	//Validation for RPC process
	$('#btnSearch').bind('click', function(){
		card_number = $('#txtCard').val();
		if(card_number == ""){
			alert('Por favor ingrese un n\u00FAmero de tarjeta.');
		}else{
			loadFileTable(card_number);
		}
	});
	
	//Clear work area for every card number
	$('#txtCard').bind('click', function(){
		$('#fileTableContainer').html('');
	});
	$('#txtCard').bind('keypress', function(){
		$('#fileTableContainer').html('');
	});
});

function loadFileTable(card_number){
	show_ajax_loader();
	$('#fileTableContainer').load(document_root + 'rpc/assistance/simpleLoadFilesByCard.rpc', 
		{
			card_number: card_number
		}, function(){			
			currentTable = $('#files_table').dataTable( {
							"bJQueryUI": true,
							"sPaginationType": "full_numbers",
							"oLanguage": {
								"oPaginate": {
									"sFirst": "Primera",
									"sLast": "&Uacute;ltima",
									"sNext": "Siguiente",
									"sPrevious": "Anterior"
								},
								"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
								"sZeroRecords": "No se encontraron resultados",
								"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
								"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
								"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
								"sSearch": "Filtro:",
								"sProcessing": "Filtrando.."
							},
							"sDom": 'T<"clear">lfrtip',
							"oTableTools": {
								"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
								"aButtons": []
							}
			});
			
			$("#frmVoidFiles").validate({
				errorLabelContainer: "#alerts",
				wrapper: "li",
				onfocusout: false,
				onkeyup: false,
				rules: {
					hdnOneRequired: {
						required: function(){
							var size = $("input:checked[id^='chkFile_']", currentTable.fnGetNodes()).length;

							if(size > 0){
								return false;
							}else{
								return true;
							}
						}
					},
					txtObservations: {
						required: true
					}
				},
				submitHandler: function(form){
					show_ajax_loader();
					var selected_ids = new Array();
					
					$("input:checked[id^='chkFile_']", currentTable.fnGetNodes()).each(function(){
						array_push(selected_ids, $(this).val());
					});
					$('#hdnOneRequired').val(implode(',', selected_ids));
					
					form.submit();
				}
			});
			
			hide_ajax_loader();		
	});
}

