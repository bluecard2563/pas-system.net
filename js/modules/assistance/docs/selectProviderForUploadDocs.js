// JavaScript Document
var currentTable;
$(document).ready(function() {
	//Validation for RPC process
	$('#btnSearch').bind('click', function() {
		loadFileTable($('#provider').val());
	});

	//Clear work area for every card number
	$('#txtCard').bind('change', function() {
		$('#fileTableContainer').html('');
	});
});

function loadFileTable(serial_spv) {
	if (serial_spv != '') {
		show_ajax_loader();
		$('#fileTableContainer').load(document_root + 'rpc/assistance/docs/loadLiquidationFilesForProvider.rpc',
				{
					provider_id: serial_spv
				}, function() {
					currentTable = $('#files_table').dataTable({
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"oLanguage": {
							"oPaginate": {
								"sFirst": "Primera",
								"sLast": "&Uacute;ltima",
								"sNext": "Siguiente",
								"sPrevious": "Anterior"
							},
							"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
							"sZeroRecords": "No se encontraron resultados",
							"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
							"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
							"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
							"sSearch": "Filtro:",
							"sProcessing": "Filtrando.."
						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": document_root + "img/dataTables/copy_cvs_xls_pdf.swf",
							"aButtons": []
						}
					});

					hide_ajax_loader();
		});
	} else {
		alert('Seleccione un proveedor.');
	}
}