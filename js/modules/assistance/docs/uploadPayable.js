// JavaScript Document
var currentTable;
$(document).ready(function() {
	activateBenefitsChooser();
	enableFilesTableStatusSelectionJs();
	
	$("#frmUploadPayable").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFileName: {
				required: true
			},
			txtFileDescription: {
				required: true
			},
			txtTotalPresentedAmount: {
				required: true,
				number: true
			},
			txtTotalAuthorizadAmount: {
				required: true,
				number: true
			},
			selBillTo: {
				required: true
			},
			txtDocumentDate: {
				required: true,
				date: true
			},
			txtObservations: {
				required: true,
				text: true
			},
			selTypePrq: {
				required: true
			},
			fileAddress: {
				required: true
			}
		},
		messages: {
			txtObservations: {
				text: 'Se debe ingresar solo texto.'
			},
			txtDocumentDate: {
				date: "Debe ingresar una fecha en formato dd/mm/YYYY"
			},
			txtTotalAuthorizadAmount: {
				number: "El campo 'Monto Aprobado' admite solo n&uacute;meros."
			},
			txtTotalPresentedAmount: {
				number: "El campo 'Monto Presentado' admite solo n&uacute;meros."
			}
		}
	});
	
	setDefaultCalendar($('#txtDocumentDate'),'-1y','+0d', $('#hdnToday').val());
	
	if($('#files_table').length > 0){
		currentTable = $('#files_table').dataTable({
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"oLanguage": {
							"oPaginate": {
								"sFirst": "Primera",
								"sLast": "&Uacute;ltima",
								"sNext": "Siguiente",
								"sPrevious": "Anterior"
							},
							"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
							"sZeroRecords": "No se encontraron resultados",
							"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
							"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
							"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
							"sSearch": "Filtro:",
							"sProcessing": "Filtrando.."
						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": document_root + "img/dataTables/copy_cvs_xls_pdf.swf",
							"aButtons": []
						}
					});
	}
	
	/*FILE BENEFITS DIALOG*/
	$('#fileViewBenefitsDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 465,
		width: 710,
		modal: true,
		stack: true,
		resizable: false,
		title:"Beneficios por archivo",
		buttons: {
			'Salir': function() {
				$(this).dialog('close');
			}
		}
	});
	
	$("a[id^='viewDocument']", currentTable.fnGetNodes()).each(function(){
		$(this).bind("click", function(){
			$("#viewBenefitsUploadContainer").html("<center><img src = '"+document_root+"img/loading_small.gif'>Cargando Beneficios</center>");
			$("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{serial_dbf:$(this).attr("serial_dbf")});
			$("#fileViewBenefitsDialog").dialog("open");
		});
	});
	
	$("a[id^='editDocument']", currentTable.fnGetNodes()).each(function(){
		$(this).bind("click", function(){
			$("#viewBenefitsUploadContainer").html("<center><img src = '"+document_root+"img/loading_small.gif'>Cargando Beneficios.</center>");
			$("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{serial_dbf:$(this).attr("serial_dbf"), serial_prq:$(this).attr("serial_prq"),edit:"edit"});
			$("#fileViewBenefitsDialog").dialog("open");
		});
	});
	
	$("#updateDiagnosis").bind('click', function(){
		remoteUpdateFileDiagnosis($('#hdnSerial_fle').val(), $('#txtDiagnosis').val());
	});
});