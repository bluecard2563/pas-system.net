//JavaScript Document
$(document).ready(function(){
	if($('#filesTable').length > 0){
		currentTable = $('#filesTable').dataTable( {
			"aoColumns": [
				null,
				null,
				{ "sType": "uk_date" },
				null,
				null,
				null,
				null,
				null,
				null
			],
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"oLanguage": {
					"oPaginate": {
						"sFirst": "Primera",
						"sLast": "&Uacute;ltima",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
					"sZeroRecords": "No se encontraron resultados",
					"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
					"sSearch": "Filtro:",
					"sProcessing": "Filtrando.."
				},
			   "sDom": 'T<"clear">lfrtip',
			   "oTableTools": {
				  "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
				  "aButtons": ["pdf" ]
			   }
		});
	}
}); 