// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmNewTempDealer").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,

            rules: {
                selZone: {
                    required:true
                },
                selCountry: {
                    required:true
                },
                selTravelZone: {
                    required:true
                },
                selCountrybirth: {
                    required:true
                },
                txtAgencyName: {
                    required:true
                },
                txtDocument: {
                    required:true
                },
                txtFirstName: {
                    required:true,
                    textOnly: true
                },
                txtLastName: {
                    required:true,
                    textOnly: true
                },
		txtBirthDate: {
                    required:true,
                    date: true
                },
		txtPhone: {
                    required:true,
                    digits: true
                },
		txtContactPhone: {
                    required:true,
                    digits: true
                },
		txtContactAddress: {
                    required:true
                },
		txtProblem: {
                    required:true
                }
            },
            messages: {
                txtFirstName: {
                    textOnly: 'El campo "Nombre" admite solo letras.'
                },
                txtLastName: {
                    textOnly: 'El campo "Apellido" admite solo letras.'
                },
                txtBirthDate: {
                    date: 'El campo "Fecha de nacimiento" debe tener formato de fecha.'
                },
                txtPhone: {
                    digits: 'El campo "Tel&eacute;fono" admite solo n&uacute;meros.'
                },
                txtContactPhone: {
                    digits: 'El campo "Tel&eacute;fono de contacto" admite solo n&uacute;meros.'
                }
            },
            submitHandler: function(form) {
                $('#btnRegistrar').attr('disabled', 'true');
                $('#btnRegistrar').val('Espere por favor...');
                form.submit();
            }
    });
    /*END VALIDATION SECTION*/

    /*CALENDAR*/
	setDefaultCalendar($("#txtBirthDate"),'-100y','+0d','-20y');
    /*END CALENDAR*/

    $("#selZone").bind("change",function(){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0});
    });
    $("#selTravelZone").bind("change",function(){
        $('#travelCountryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, serial_sel:"Travel"});
    });



});