// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewAssistance").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,

		rules: {
			txtCard: {
				required: true
			}
		}
	});

	$('#btnSubmit').bind('click', function(){
		show_ajax_loader();
		var band=0;
		if($("#frmNewAssistance").valid()){
			for(var i=0;i<$("#txtCard").val().length;i++)
			{				
				if($("#txtCard").val().charAt(i)=="_"){					
					$("#frmNewAssistance").attr('action',$("#frmNewAssistance").attr('action')+"/1");
					band=1;
					break;
				}
			}
			if(band==0){
				$("#frmNewAssistance").attr('action',$("#frmNewAssistance").attr('action')+"/0");
			}
			$("#frmNewAssistance").submit();
		}else{
	hide_ajax_loader();
	}
	});

	$("#frmSearchCustomer").validate({
		errorLabelContainer: "#searchAlerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,

		rules: {
			txtCustomerName: {
				minlength: 7,
				textOnly: true
			},
			hdnValidate:{
				required: true
			}
		},
		messages: {
			txtCustomerName: {
				minlength: 'El campo "Nombre" debe tener al menos 7 caracteres.',
				textOnly: 'El campo "Nombre" solo admite letras.'
			}
		}
	});

	/*END VALIDATION SECTION*/

	$("#srchCustomer").bind("click",function(){
		$("#searchCustomerContainer").show();
		$("#BtnSrchCustomer").hide();
	});
	$("#btnTempCustomer").bind("click",function(){
		window.location = document_root+"modules/assistance/fNewTemporaryCustomer";
	});

	$("#txtCustomerID").bind("focusout",function(){
		$("#txtCustomerName").val('');
		$('#hdnValidate').val('');
		if(this.value != ''){
			$('#hdnValidate').val('1');
		}
	});
	$("#txtCustomerName").bind("focusout",function(){
		$("#txtCustomerID").val('');
		$('#hdnValidate').val('');
		if(this.value != ''){
			$('#hdnValidate').val('1');
		}
	});


	$("#btnSearch").bind("click",function(){
		if($("#frmSearchCustomer").valid()) {
			show_ajax_loader();
			if($("#txtCustomerID").val()!=""){
				$("#customerInfoContainer").load(document_root+"rpc/loadCustomerByDocument.rpc",{
					document_cus:$("#txtCustomerID").val()
				},function(){
					hide_ajax_loader();
				});
			} else{
				if($("#txtCustomerName").val()!="") {
					$("#customerInfoContainer").load(document_root+"rpc/loadCustomerByName.rpc",{
						name_cus:$("#txtCustomerName").val()
					},function(){
						hide_ajax_loader();
					});
				}
			}
		}
	});
});