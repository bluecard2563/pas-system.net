// JavaScript Document
var pager;
var pager2;
var pager3;
var pager4;
var pager5;
var pager6;
var pager7;
var pager8;

$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmNewFile").validate({
        errorLabelContainer: "#alertsDialog",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountry: {
                required: true
            },
            selCity: {
                required: true
            },
            txtDiagnosis: {
                required: true
            },
            txtAssistanceType: {
                required: true
            },
			selMedicalType: {
				required: function() {
					if($('#medicalType').css("display") == 'none')
						return false;
					else
						return true;
				}
			}
        }
    });
    /*END VALIDATION SECTION*/

    if($('#hdnSelFilesTab').val()>0) {
        $("#filesContainer").tabs({selected: 2});
    }else {
        $("#filesContainer").tabs();
    }

    var browserCheck = (document.all) ? 1 : 0; // ternary operator statements to check if IE. set 1 if is, set 0 if not.

	/*Sales Log*/
	if($("#sales_log_table").length>0){
        pager = new Pager('sales_log_table', 4 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPositionSalesLog'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	$("#displaySalesLog").bind("click", function(){
        $("#displaySalesLog").hide();
        $("#salesLogContainer").show();
    });
	/*End Sales Log*/

	/*Load Beneficiaries*/
	$("#beneficiariesContainer").load(document_root+"rpc/assistance/file/loadBeneficiaries.rpc", {card_number: $('#hdnCardNumber').val(),serial_sal: $('#hdnSerialSale').val()}, function(){
		$("#customers").accordion({autoHeight: false, collapsible: true, active:false});
		$("input[id^='createFile']").bind("click",function(){
			show_ajax_loader();
			var serial_cus = $(this).attr("serial");
			var serial_trl = $(this).attr("serial_trl");
			/*Verifies if exists any Service Provider*/
			$.getJSON(document_root+"rpc/assistance/checkServiceProviderByCountry.rpc",function(data){
			   if(data) {
					/*FILE CREATION*/
					$.getJSON(document_root+"rpc/assistance/createNewFile.rpc",
					{
						serial_sal: $("#hdnSerialSale").val(),
						serial_trl: serial_trl,
						serial_cus: serial_cus
					},
						function(data){
							if(data){
								$('#editeFilesDialog').load(document_root+"rpc/assistance/loadFile.rpc",
								{
									serial_fle: data.serial_fle,
									card_number: $('#hdnCardNumber').val(),
									status: "first_open"
								},function(){
									//values for blog validation
									$('#hdnClientCharacters').val($('#txtClientBlog').val().length);
									$('#hdnOperatorCharacters').val($('#txtOperatorBlog').val().length);
									hide_ajax_loader();
								});
							} else {
								hide_ajax_loader();
								alert("Existieron problemas al insertar el expediente.");
							}
					});
					/*END FILE CREATION*/
			   } else {
				   hide_ajax_loader();
				   alert("No se puede crear un nuevo expediente.\nNo existen coordinadores asignados a ning\u00FAn pa\u00EDs.");
			   }
			});

		});
	})
	/*End Load Beneficiaries*/

	/*Load Benefits*/
	$("#loadBenefits").bind('click',function(){
		$("#benefitsContainer").html("<center><img src = '"+document_root+"img/loading_small.gif'>Cargando Beneficios.</center>");
		$("#benefitsContainer").load(document_root+"rpc/assistance/file/loadBenefits.rpc", {card_number: $('#hdnCardNumber').val(),serial_sal: $('#hdnSerialSale').val(),serial_dea: $('#hdnSerialDea').val()}, function(){
			//Pager benefits table
			if($("#benefits_table").length>0) {
				pager4 = new Pager('benefits_table', 6 , 'Siguiente', 'Anterior');
				pager4.init(7); //Max Pages
				pager4.showPageNav('pager4', 'pageNavPositionBenefits'); //Div where the navigation buttons will be placed.
				pager4.showPage(1); //Starting page
			}
			//Pager services table
			if($("#services_table").length>0){
				pager7 = new Pager('services_table', 5 , 'Siguiente', 'Anterior');
				pager7.init(7); //Max Pages
				pager7.showPageNav('pager7', 'pageNavPositionServices'); //Div where the navigation buttons will be placed.
				pager7.showPage(1); //Starting page
			}
		})
	});
	/*End Load Benefits*/
    
    if($("#files_table").length>0){
        pager3 = new Pager('files_table', 7 , 'Siguiente', 'Anterior');
        pager3.init(7); //Max Pages
        pager3.showPageNav('pager3', 'pageNavPositionFiles'); //Div where the navigation buttons will be placed.
        pager3.showPage(1); //Starting page
    }
    
    if($("#liquidations_table").length>0){
        pager5 = new Pager('liquidations_table', 7 , 'Siguiente', 'Anterior');
        pager5.init(7); //Max Pages
        pager5.showPageNav('pager5', 'pageNavPositionLiquidations'); //Div where the navigation buttons will be placed.
        pager5.showPage(1); //Starting page
    }
    if($("#apliances_table").length>0){
        pager6 = new Pager('apliances_table', 4 , 'Siguiente', 'Anterior');
        pager6.init(7); //Max Pages
        pager6.showPageNav('pager6', 'pageNavPositionAppliances'); //Div where the navigation buttons will be placed.
        pager6.showPage(1); //Starting page
    }
    
	if($("#files_closed_table").length>0){
        pager7 = new Pager('files_closed_table', 5 , 'Siguiente', 'Anterior');
        pager7.init(7); //Max Pages
        pager7.showPageNav('pager7', 'pageNavPositionClosedFiles'); //Div where the navigation buttons will be placed.
        pager7.showPage(1); //Starting page
    }

    $("#btnExit").bind("click",function(){
         window.location = document_root+"modules/assistance/fNewAssistance";
    });
    
    /*OPEN FILE*/
    $("a[id^='openFile']").bind("click",function(){
		show_ajax_loader();
        $("#alertsDialogContainer").load(document_root+"rpc/assistance/loadAlertsDialog.rpc",{serial_fle: $(this).attr("serial")},function(){
				$("#newAlert").bind("click", function(){
					 $("#createAlertsDialog").dialog('open');

				});
				$("#createAlertsDialog").dialog({
					bgiframe: true,
					autoOpen: false,
					autoResize:true,
					height: 450,
					width: 450,
					modal: true,
					stack: true,
					resizable: false,
					title:"Crear Alerta",
					buttons: {
						'Salir': function() {
							$(this).dialog('close');
						}
					}
			});

		});
		$('#editeFilesDialog').load(document_root+"rpc/assistance/loadFile.rpc",
			{
				serial_fle: $(this).attr("serial"),
				card_number: $('#hdnCardNumber').val()
			},function(){
				//values for blog validation
				$('#hdnClientCharacters').val($('#txtClientBlog').val().length);
				$('#hdnOperatorCharacters').val($('#txtOperatorBlog').val().length);
			});
    });
    /*END OPEN FILE*/

    /*OPEN CLOSED FILE*/
    $("a[id^='viewFile']").bind("click",function(){
		show_ajax_loader();
        $('#editeFilesDialog').load(document_root+"rpc/assistance/loadFile.rpc",
            {
                serial_fle: $(this).attr("serial"),
                card_number: $('#hdnCardNumber').val(),
                status: "closed"
            });
    });
    /*END OPEN CLOSED FILE*/

    /*OPEN LIQUIDATION FILE*/
    $("a[id^='openLiquidationFile']").bind("click",function(){
        location.href=document_root+"modules/assistance/fLiquidateAssistance/"+$(this).attr("serial");
    });
    /*END OPEN LIQUIDATION FILE*/

    /*SEND FILE TO MEDICAL CHECKUP*/
    $("a[id^='medicalCheckup']").bind("click",function(){
		if($(this).attr('status') == "AUDITED"){
			location.href=document_root+"modules/assistance/medicalCheckup/fViewFile/"+$(this).attr("serial_prq")+"/read_only";
		} else {
			$('#hdnSerialFleCheckup').val($(this).attr("serial"));
			$("#paymentRequestDialog").dialog('open');
		}
    });
	
	/*SEND OPEN FILE TO LIQUIDATION*/
    $("a[id^='toLiquidation']").bind("click",function(){
		if(confirm('Est\u00E1 seguro de desea enviar este expediente a liquidaci\u00F3n')){
			serial_fle = $(this).attr("serial");
			show_ajax_loader();
			$.ajax({
					url: document_root+"rpc/assistance/sendToLiquidation.rpc",
					type: "post",
					data: ({serial_fle : serial_fle}),
					success: function(data){
						if(data=='true'){
							$('#row_'+serial_fle).html('En liquidaci&oacute;n');
							window.location = document_root+"modules/assistance/fEditAssistance";
						}else{
							hide_ajax_loader();
							alert('No se ha podido registrar el cambio');
						}
					}
			});
		}
    });
    /*END OPEN CLOSED FILE*/
	
	
    /*END SEND FILE TO MEDICAL CHECKUP*/
    
    /*ASSISTANCE DIALOG OPTIONS*/
    $("#filesDialog").dialog({
        bgiframe: true,
        autoOpen: false,
        autoResize:true,
        height: 675,
        width: 1000,
        modal: true,
        resizable: false,
        closeOnEscape: false,
        open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
        title:"Nuevo Expediente",
        buttons: {
            'Guardar y Salir': function() {
				if($("#frmNewFile").valid()) {
					$('#messageText').html('Enviando e-mail');
					$('#uploading_file').dialog('open');
					$("#frmNewFile").submit();
				}
            },
			'Enviar e-mail': function() {
				if($('#txtDiagnosis').val()){
					$('#messageText').html('Enviando e-mail');
					$('#uploading_file').dialog('open');
					$.getJSON(document_root+"rpc/assistance/sendInformationEmail.rpc",
					{
					  card_number: $('#hdnCardNumber').val(),
					  serial_fle: $('#hdnSerialFle').val(),
					  diagnosis: $('#txtDiagnosis').val()
					}, function(data){
						$('#uploading_file').dialog('close');
						if(data) {
							alert("E-mail enviado al supervisor de liquidaciones exitosamente.");
						} else {
							alert("Error al enviar e-mail al supervisor de liquidaciones.");
						}
					});
				} else {
					alert("Para poder enviar el e-mail informativo debe ingresar el diagn\u00F3stico de la asistencia.");
				}
			}
        }
    });

    /*BLOGS*/
    $("#btnOperator").bind("click",function(){
        $("#clientBlog").hide("slow");
        $("#operatorBlog").show("slow");  
    });
    $("#btnClient").bind("click",function(){
        $("#operatorBlog").hide("slow");
        $("#clientBlog").show("slow");
    });

	var bandHeaderInsCli=0;
    $("#btnClientBlog").bind("click",function(){
        if($("#txtInputClientBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsCli==0 && $("#blogHeader").val() != ""){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n");
                $("#txtClientBlog").append(specialchars($("#txtInputClientBlog").val()));
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n\n");
				bandHeaderInsCli=1;
            } else {//other browsers
				if(bandHeaderInsCli==0 && $("#blogHeader").val() != ""){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").append(specialchars("\n"+$("#txtInputClientBlog").val())+"\n\n");
				bandHeaderInsCli=1;
            }
            $("#txtInputClientBlog").val("");
            $("#txtClientBlog").scrollTop(50000);
        }
    });

    $("#txtInputClientBlog").bind("keyup",function(event){
        if(event.keyCode == '13' && $("#txtInputClientBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsCli==0 && $("#blogHeader").val()!=''){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n");
                $("#txtClientBlog").append($("#txtInputClientBlog").val());
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n");
				bandHeaderInsCli=1;
            } else {//other browsers
				if(bandHeaderInsCli==0 && $("#blogHeader").val()!=''){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").append("\n"+$("#txtInputClientBlog").val()+"\n");
				bandHeaderInsCli=1;
            }
            $("#txtInputClientBlog").val("");
            $("#txtClientBlog").scrollTop(50000);
        }
    });
	var bandHeaderInsOp=0;
    $("#btnOperatorBlog").bind("click",function(){
        if($("#txtInputOperatorBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("<br />"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
                $("#txtOperatorBlog").append("<br />"+specialchars($("#txtInputOperatorBlog").val()));
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
				bandHeaderInsOp=1;
            }else{//other browsers
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("\n"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").append("\n"+specialchars($("#txtInputOperatorBlog").val())+"\n");
				bandHeaderInsOp=1;
            }
            $("#txtInputOperatorBlog").val("");
            $("#txtOperatorBlog").scrollTop(50000);
        }
    });

    $("#txtInputOperatorBlog").bind("keyup",function(event){
        if(event.keyCode == '13' && $("#txtInputOperatorBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("<br />"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
                $("#txtOperatorBlog").append("<br />"+$("#txtInputOperatorBlog").val());
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
				bandHeaderInsOp=1;
            }else{//other browsers
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("\n"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").append("\n"+$("#txtInputOperatorBlog").val());
				bandHeaderInsOp=1;
            }
            $("#txtInputOperatorBlog").val("");
            $("#txtOperatorBlog").scrollTop(50000);
        }
    });
    /*END BLOG*/

    /*DIALOG TABS*/
    $("#nextTab").bind("click",function(){
        $("#locationData").hide();
        $("#benefitsData").show();
    });
    $("#prevTab").bind("click",function(){
        $("#benefitsData").hide();
        $("#locationData").show();
    });
    /*END DIALOG TABS*/


    /*LOCATION CONTAINER*/
    $('#displayContactForm').bind("click",function(){
        $('#displayContactForm').toggle("fast");
        $('#contactFormContainer').toggle("fast");
    });

    $('#hideContactForm').bind("click",function(){
        $('#contactName').val('');
        $('#contactPhone').val('');
        $('#contactMobilePhone').val('');
        $('#contactOtherPhone').val('');
        $('#txtContactReference').val('');
        $('#displayContactForm').toggle("fast");
        $('#contactFormContainer').toggle("fast");
    });

    $('#btnAddLocation').bind("click",function(){
        //Validates the required fields in locaition container
        var valid = true;
        //RULES
        var digits = /^\d+$/;
        var textOnly = /^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$/;
        
        //MESSAGES
        $('#alertsLocation').html("");
        if($('#contactName').val() == "") { //Validates if the contact name field is not empty
            $('#alertsLocation').append('<li><b>El campo "Nombre del contactante" es obligatorio.</b></li>');
            valid=false;
        } else if(!textOnly.test($('#contactName').val())) { //Validates if the contact name field contains only text
            $('#alertsLocation').append('<li><b>El campo "Nombre del contactante" admite solo letras.</b></li>');
            valid=false;
        }
        if($('#contactPhone').val() == "") { //Validates if the phone field is not empty
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono fijo" es obligatorio.</b></li>');
            valid=false;
        }else if(!digits.test($('#contactPhone').val())) { //Validates if the phone field contains only digits
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono fijo" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if(!digits.test($('#contactMobilePhone').val()) && $('#contactMobilePhone').val() != "") { //Validates if the mobile phone field contains only digits
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono movil" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if(!digits.test($('#contactOtherPhone').val()) && $('#contactOtherPhone').val() != "") { //Validates if the other phone field contains only digits
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono (otro)" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if($('#txtContactReference').val() == "") {
            $('#alertsLocation').append('<li><b>El campo "Direcci&oacute;n y referencias" es obligatorio.</b></li>');
            valid=false;
        }

        if(valid) {
            $('#alertsLocation').css("display", "none");

            var phone = $('#contactPhone').val();
            if($('#contactMobilePhone').val() != ""){
                phone+=" / "+$('#contactMobilePhone').val();
            }
            if($('#contactOtherPhone').val() != ""){
                phone+=" / "+$('#contactOtherPhone').val();
            }

            $('#locationContainer').load(document_root+"rpc/assistance/addAssistanceLocations.rpc",
            {
                serial_fle: $('#hdnSerialFle').val(),
                contact_name: $('#contactName').val(),
                contact_phone: phone,
                contact_address: $('#txtContactReference').val()
            },function(){
                $('#contactName').val("");
                $('#contactPhone').val("");
                $('#contactMobilePhone').val("");
                $('#contactOtherPhone').val("");
                $('#txtContactReference').val("");
                $('#displayContactForm').toggle("fast");
                $('#contactFormContainer').toggle("fast");
            });
        } else {
            $('#alertsLocation').css("display", "block");
        }
    });
    /*END LOCATION CONTAINER*/

    /*LOAD PROVIDERS*/
    $("#selCountry").bind("change", function(){
		$("#fileMessageContainer").css('display','none');
		$("#fileMessage").html('');
        if(this.value==""){
			$("#selCity option[value='']").attr("selected",true);
			$("#selProvider option[value='']").attr("selected",true);
			show_ajax_loader();
			$("#benefitsContainer").load(document_root+"rpc/assistance/loadBenefitsByServiceProvider.rpc",
			{
				serial_spv: $('#selProvider').val(),
				card_number: $('#hdnCardNumber').val(),
				serial_fle:$('#hdnSerialFle').val()
			},function(){
				hide_ajax_loader();
			});
		}

		$.getJSON(document_root+"rpc/assistance/checkProductRestrictions.rpc", {serial_sal: $("#hdnSerialSale").val(),  serial_cou: this.value, serial_cus: $("#hdnSerialCus").val()},function(data){
			if(data) {
				if(data == 1) {
					$("#fileMessageContainer").css('display','block');
					$('#fileMessage').html("La tarjeta no permite asistencias en el pa&iacute;s de emisi&oacute;n de la misma, ni del pa&iacute;s de residencia del cliente.");
				} else if(data ==2) {
					$("#fileMessageContainer").css('display','block');
					$('#fileMessage').html("La tarjeta no permite asistencias en el pa&iacute;s de emisi&oacute;n de la misma.");
				} else if(data ==3) {
					$("#fileMessageContainer").css('display','block');
					$('#fileMessage').html("La tarjeta no permite asistencias en el pa&iacute;s de residencia del cliente.");
				}

			}
		});
		$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: this.value, all: true});
        $('#providerContainer').load(document_root+"rpc/loadServiceProviderByCountry.rpc",
        {serial_cou: this.value, type_spv: $('#selServiceType').val()});
    });

    $('#selServiceType').bind("change", function(){

        if(this.value==""){
            $("#selProvider option[value='']").attr("selected",true);
            $("#medicalType option[value='']").attr("selected",true);
            $('#medicalType').css("display", "none");
			$("#benefitsContainer").html('');
        }

        $('#providerContainer').load(document_root+"rpc/loadServiceProviderByCountry.rpc",
        {serial_cou: $('#selCountry').val(), type_spv: this.value},function(){
			$('#selProvider').bind("change", function(){
				if(this.value != '') {
				$('#benefitsContainer').html("<center><img src = '"+document_root+"img/loading_small.gif'><b>Cargando Servicios</b></center>");
				$('#benefitsContainer').load(	document_root+"rpc/assistance/loadBenefitsByServiceProvider.rpc",
												{
													serial_spv: $('#selProvider').val(),
													card_number: $('#hdnCardNumber').val(),
													serial_fle:$('#hdnSerialFle').val()
												});
				} else {
					$('#benefitsContainer').html('<center>Seleccione un coordinador.</center>');
				}
			});

            if($('#selServiceType').val() == "MEDICAL") {
                $('#medicalType').css("display", "block");
            } else {
                $('#medicalType').css("display", "none");
            }
        });
    });
    /*LOAD PROVIDERS*/

    /*END ASSISTANCE DIALOG OPTIONS*/
	
	/*GRADE SERVICE PROVIDER DIALOG*/
	$("#gradeServiceProviderDialog").dialog({
            title: "Calificar Coordinadores",
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            height: 360,
            width: 600,
            modal: true,
            stack: true,
            resizable: false,
            buttons: {
                'Cancelar': function() {
                    $(this).dialog('close');
                },
				'Guardar': function() {
					if($('#newRateServiceProvider').valid()){
						if($('#selGrade').length>0 && $('#txtRateObservation').length>0){
							$.ajax({
									url: document_root+"rpc/assistance/registerProviderGrade.rpc",
									type: "post",
									data: ({serial_fle : $('#serial_fle').val(),
											serial_spv: $('#selServiceProvider').val(),
											grade: $('#selGrade').val(),
											comment:$('#txtRateObservation').val()
											}),
									success: function(data){
										$.ajax({
											url: document_root+"rpc/assistance/file/validateProvidersGraded.rpc",
											type: "post",
											data: ({serial_fle : $('#serial_fle').val()}),
											success: function(grade_status){
												if(grade_status == 'true'){
													$('#gradeProviders'+$('#serial_fle').val()).html('Ver Calificaciones');
												}
												
												if(data=='true'){
													alert('Registro Exitoso');
													$('#gradeServiceProviderDialog').dialog('close');
												}else{
													alert('No se ha podido registrar su comentario');
												}
											}
										});
									}
							});
						}else{
							alert('Este Coordinador ya ha sido calificado');
						}
					}
				}
            }
    });
	/*END GRADE SERVICE PROVIDER DIALOG*/
	
	/*ADD DOCUMENT DIALOG*/
    $("#addDocDialog").dialog({
            title: "Documentos Esperados",
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            height: 360,
            width: 600,
            modal: true,
            stack: true,
            resizable: false,
            buttons: {
                'Cancelar': function() {
                    $(this).dialog('close');
                },
				'Guardar': function() {
					if($('#newPendingDocument').valid()){
						$.ajax({
								url: document_root+"rpc/assistance/registerPendingDocument.rpc",
								type: "post",
								data: ({serial_fle : $('#serial_fle').val(),
										document: $('#pendingDocument').val(),
										serial_cus: $('#serial_cus').val(),
										receiveFrom:$('#receiveFrom').val(),
										serial_spv:$('#docServiceProvider').val()}),
								success: function(data){
									if(data=='true'){
										$('#addDocDialog').dialog('close');
									}else{
										alert('No se ha podido registrar el cambio');
									}
								}
						});
					}
				}
            }
    });
    /*END ADD DOCUMENT DIALOG*/
	

    /*VIEW ALERTS DIALOG*/
    $("#viewAlertsDialog").dialog({
            title: "Alertas",
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            height: 350,
            width: 615,
            modal: true,
            stack: true,
            resizable: false,
            buttons: {
                'Salir': function() {
                    $(this).dialog('close');
                }
            }
    });
    /*END VIEW ALERTS DIALOG*/

    /*PROVIDERS INFO DIALOG*/
    $("#providersInfoDialog").dialog({
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            height: 400,
            width: 750,
            modal: true,
            stack: true,
            resizable: false,
            buttons: {
                'Salir': function() {
                    $(this).dialog('close');
                }
            }
    });
    /*END PROVIDERS INFO DIALOG*/

	/*USED PROVIDERS DIALOG*/
    $("#usedProvidersDialog").dialog({
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            height: 400,
            width: 400,
            modal: true,
            stack: true,
            resizable: false,
			title: "Coordinadores utilizados",
            buttons: {
                'Salir': function() {
                    $(this).dialog('close');
                }
            }
    });
    /*END USED PROVIDERS DIALOG*/

    /*PAYMENT REQUEST DIALOG*/
    $("#paymentRequestDialog").dialog({
        bgiframe: true,
        autoOpen: false,
        autoResize:true,
        height: 350,
        width: 600,
        modal: true,
        resizable: false,
        closeOnEscape: false,
        open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
        title:"Enviar a Auditor\u00EDa M\u00E9dica",
        buttons: {
            'Salir': function() {
                $("#medicalCheckupCountry option[value='']").attr("selected",true);
                $("#paymentRequestContainer").html('');
                $('#paymentRequestMessages').hide();
                $(this).dialog('close');
            }
        }
    });

    $('#medicalCheckupCountry').bind("change", function(){
        $('#paymentRequestContainer').load(document_root+"rpc/assistance/loadMedicalAuditors.rpc",{serial_cou: $('#medicalCheckupCountry option:selected').val()});
    });
    /*END PAYMENT REQUEST DIALOG*/

    /*UPLOAD FILE DIALOG*/
    $("#uploading_file").dialog({
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            modal: true,
            stack: true,
            resizable: false,
            closeOnEscape: false,
            open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
            title:"Espere por favor"
    });
    /*END UPLOAD FILE DIALOG*/
    
    setTimeout(function(){
        $('#successMessage').hide("slide", {direction: "up"}, 500);
    },2000);

    $('#displayProviderInfo').bind("click", function(){
        if($('#selProvider').val() != '') {
            $('#providersInfoContainer').load(document_root+"rpc/assistance/loadContactsByServiceProvider.rpc",{serial_spv:$('#selProvider').val()});
            $("#providersInfoDialog").dialog( "option", "title", $('#selProvider option:selected').text() );
            $("#providersInfoDialog").dialog('open');
        } else {
            alert("Debe seleccionar un coordinador.");
        }
    });
	
	 /*OPEN ADD DOCUMENT FORM*/
    $("a[id^='registerDocument']").bind("click",function(){
		$('#addDocDialog').load(document_root+"rpc/assistance/loadDocumentForm.rpc",
								{serial_fle:$(this).attr('serial')},
								function(){
									if($('#showRegisteredPendingDocs').length>0){
										$('#showRegisteredPendingDocs').bind('click',function(){
											$('#alreadyPendingDocuments').toggle();
										})
									}
									$('#receiveFrom').bind('change',function(){
										if($(this).val()=='provider'){
											$('#docProviderContainer').show(250);
										}else{
											$('#docProviderContainer').hide(250);
										}
									});
									$("#newPendingDocument").validate({
										errorLabelContainer: "#docPendingAlerts",
										wrapper: "li",
										onfocusout: false,
										onkeyup: false,
										rules: {
											pendingDocument: {
												required: true
											},
											docServiceProvider: {
												required:{depends: function() {
															if($('#receiveFrom').val() == 'provider')
																return true;
															else
																return false;
															}
												}		
											}
										}
									});
									$('#addDocDialog').dialog('open');
								});
	});
	
	
	/*OPEN GRADE SERVICE PROVIDER FORM*/
	$("a[id^='gradeProviders']").bind("click",function(){
		var fileID = $(this).attr('serial');
		$('#gradeServiceProviderDialog').load(document_root+"rpc/assistance/loadGradeProviderForm.rpc",
				{serial_fle:fileID},
				function(){
					$('#selServiceProvider').bind('change',function(){
						$('#serviceProviderGradeComment').load(document_root+"rpc/assistance/loadProviderFileCurrentGrade.rpc",{serial_spv:$(this).val(),serial_fle:fileID});
					});
					
					//form validation
					$("#newRateServiceProvider").validate({
										errorLabelContainer: "#docGradeAlerts",
										wrapper: "li",
										onfocusout: false,
										onkeyup: false,
										rules: {
											selServiceProvider: {
												required: true
											},
											selGrade: {
												required:{depends: function() {
															if($('#toValidate').length>0 && $('#toValidate').val() == '1')
																return true;
															else
																return false;
															}
												}		
											},
											txtRateObservation: {
												required:{depends: function() {
															if($('#toValidate').length>0 && $('#toValidate').val() == '1')
																return true;
															else
																return false;
															}
												},
												maxlength:140
											}
										},
										messages: {
											txtRateObservation:{
												maxlength:'El campo Observaciones permite solo hasta 140 caracteres.'
											}
										}
									});
					
					$('#gradeServiceProviderDialog').dialog('open');
				});
	});
	
	
	//*************************** FILE HISTORY TAB ADDITIONS
	$('#documents_by_file_container').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 500,
		width: 1170,
		modal: true,
		stack: true,
		resizable: false,
		title: "Documentaci&oacute;n Relacionada",
		buttons: {
			'Salir': function() {
				$(this).dialog('close');
			}
		}
	});
	
	$('[id^="view_documents_file"]').each(function(){
		$(this).bind('click', function(){
			show_ajax_loader();
			var serial_fle_history = $(this).attr('serial');
			
			$('#documents_by_file_container').load(document_root + 'rpc/assistance/file/loadDocumentsHistoryInfo.rpc',
				{
					serial_fle: serial_fle_history
				},function(){
					$('#selProviderHistory').bind('change', function(){
						$('#documents_history_container').html('');
						
						if($(this).val() != ''){
							loadHistoryDocumentsTable($(this).val(), serial_fle_history);
						}
					});
					
					hide_ajax_loader();
					$('#documents_by_file_container').dialog('open');
				});			
		})
	});
});

// Function that replaces all occurrences of search in haystack with replace
function str_replace (search, replace, subject, count) {
            f = [].concat(search),
            r = [].concat(replace),
            s = subject,
            ra = r instanceof Array, sa = s instanceof Array;s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }
     for (i=0, sl=s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j=0, fl=f.length; j < fl; j++) {temp = s[i]+'';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length-s[i].length)/f[j].length;}}
    }
    return sa ? s : s[0];
}

function moneyFormat(amount) {
    var val = parseFloat(amount);
    if (isNaN(val)) {return "0.00";}
    if (val <= 0) {return "0.00";}
    val += "";
    // Next two lines remove anything beyond 2 decimal places
    if (val.indexOf('.') == -1) {return val+".00";}
    else {val = val.substring(0,val.indexOf('.')+3);}
    val = (val == Math.floor(val)) ? val + '.00' : ((val*10 ==
    Math.floor(val*10)) ? val + '0' : val);
    return val;
}

function loadHistoryDocumentsTable(serial_spv, serial_fle){
	show_ajax_loader();
	$('#documents_history_container').load(document_root + 'rpc/assistance/loadUploadedFiles.rpc', 
	{
		serial_spv: serial_spv,
		serial_fle: serial_fle,
		acces_permition: 'view',
		extended: 'yes'
	}, function(){
		hide_ajax_loader();
	});
}