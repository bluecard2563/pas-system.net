// JavaScript Document
var currentTable;

$(document).ready(function() {
    if ($('#casesTable').length > 0) {
        /*Style dataTable*/
        currentTable = $('#casesTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "Primera",
                    "sLast": "&Uacute;ltima",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                "sSearch": "Filtro:",
                "sProcessing": "Filtrando.."
            },
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sSwfPath": document_root + "img/dataTables/copy_cvs_xls_pdf.swf",
                "aButtons": ["pdf", "xls"]
            }

        });
        
        $("a[id^='case_']", currentTable.fnGetNodes()).each( function () {
            $(this).css('cursor', 'pointer');
            
            $(this).bind('click', function(){
                displayLiquidationForm($(this).attr('case_no'));
            });
        });
        
        
        $('#liquidation_container').dialog({
               bgiframe: true,
               autoOpen: false,
               autoResize:true,
               height: 450,
               width: 800,
               modal: true,
               stack: true,
               resizable: false,
               title: "Seleccione Facturas para el Acta de Finiquito",
               buttons: {
                    
               }
        });
    }
});

function displayLiquidationForm(case_id){
    show_ajax_loader();
    
    $('#liquidation_container').load(document_root + 'rpc/assistance/liquidation/invoiceSelection.rpc', 
    {
        serial_fle: case_id
    }, function(){
        hide_ajax_loader();
        var myButtons = {
            'Salir': function() {
                $(this).dialog('close');
            }
        };

        if ($('#frmGenerateSettlement').length > 0) {
            myButtons["Generar Acta"] = function() {
                var one_selected = false;
                
                $('[id^="chkInvoice_"]').each(function(){
                    if($(this).is(':checked')){
                        one_selected = true;
                        return;
                    }
                });
                
                if(one_selected){
                    show_ajax_loader();
                    $('#frmGenerateSettlement').submit();
                }else{
                    alert('Por favor seleccione al menos una factura');
                }
            }
        }
        
        $('#liquidation_container').dialog('option', 'buttons', myButtons);
        $('#liquidation_container').dialog('open');
    });
}
