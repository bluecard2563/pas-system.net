// JavaScript Document
$(document).ready(function(){
	 $("#frmEditLiquidations").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,

            rules: {
                txtCard: {
                    required: true,
                    digits: true
                }
            },
            messages: {
                txtCard: {
                    digits:"El campo 'N&uacute;mero de Tarjeta' solo admite n&uacute;meros."
                }
            }
    });

	/*UPLOAD FILE DIALOG*/
    $("#deleting_file").dialog({
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            modal: true,
            stack: true,
            resizable: false,
            closeOnEscape: false,
            open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
            title:"Espere por favor"
    });
    /*END UPLOAD FILE DIALOG*/

	/*FILE BENEFITS DIALOG*/
    $('#fileViewBenefitsDialog').dialog({
            bgiframe: true,
            autoOpen: false,
            autoResize:true,
            height: 350,
            width: 710,
            modal: true,
            stack: true,
            resizable: false,
            title:"Beneficios por archivo",
            buttons: {
                'Salir': function() {
                    $(this).dialog('close');
                }
            }
    });
    /*END FILE BENEFITS DIALOG*/

	$("#btnSearch").bind("click", function(){
		clearMessages();
		$('#fileContainer').html("<center><img src = '"+document_root+"img/loading_small.gif'><b>Cargando...</b></center>");
		if($("#frmEditLiquidations").valid()){
			$.getJSON(document_root+"rpc/assistance/checkCardNumber.rpc", {card_num:$("#txtCard").val()}, function(data){
				if(data) {
					$("#fileContainer").load(document_root+"rpc/assistance/loadFilesLiquidation.rpc", {card_num:$("#txtCard").val()}, function(){
						$("#files").accordion({autoHeight: false, collapsible: true, active:true});
					});
				} else {
					showMessages("error","El n&uacute;mero de tarjeta ingresado no est&aacute; registrado en el sistema.");
					$("#fileContainer").html("<center>Ingrese un n&uacute;mero de tarjeta.</center>");
				}
			});
		} else {
			$("#fileContainer").html("<center>Ingrese un n&uacute;mero de tarjeta.</center>");
		}
	});
});

function showMessages(type, message) {
	$("#confirmationMessage").html(message);
	$("#confirmationMessage").addClass(type);
	$("#confirmationMessage").css("display", "block");
}

function clearMessages() {
	$("#confirmationMessage").css("display", "none");
	$("#confirmationMessage").removeClass("succes");
	$("#confirmationMessage").removeClass("error");
	$("#confirmationMessage").html('');
}