// JavaScript Document
var pager;
var currentTable
$(document).ready(function(){
	/*SECCION DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAttendAppliances").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {

		},
		submitHandler: function(form) {
			var sData = $("input[id^='chkApliance']",currentTable.fnGetNodes()).serialize();
			$('#selectedBoxes').val(sData);

			form.submit();
		}
	});
	/*FIN DE LA SECCION DE VALIDACIONES*/

	if($('#appliancesTable').length > 0){
		currentTable = $('#appliancesTable').dataTable( {
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"oLanguage": {
				"oPaginate": {
					"sFirst": "Primera",
					"sLast": "&Uacute;ltima",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				},
				"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
				"sZeroRecords": "No se encontraron resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
				"sSearch": "Filtro:",
				"sProcessing": "Filtrando.."
			},
			"sDom": 'T<"clear">lfrtip',
			"oTableTools": {
				"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
				"aButtons": ["pdf" ]
			}

		} );



		$('#selAll').bind("click",function(){
			if($('#selAll').attr("checked")) {
				$("input[id^='chkApliance']",currentTable.fnGetNodes()).attr("checked", true);
			} else {
				$("input[id^='chkApliance']",currentTable.fnGetNodes()).attr("checked", false);
			}
		});

		$("input[id^='chkApliance']",currentTable.fnGetNodes()).bind("click",function(){
			if(!$(this).attr("checked")) {
				$('#selAll').attr("checked", false);
			}
		});

		/* FILE INFORMATION DIALOG */
		$('[id^="description_"]').each(function(){
			$(this).dialog({
				bgiframe: true,
				autoOpen: false,
				autoResize:true,
				height: 250,
				width: 450,
				modal: true,
				stack: true,
				resizable: false,
				title: "Descripci&oacute;n del Problema",
				buttons: {
					'Salir': function() {
						$(this).dialog('close');
					}
				}
			});
		});

		$('[id^="show_description_"]',currentTable.fnGetNodes()).each(function(){
			var id=$(this).attr('id');
			id=explode('_', id);
			id=id['2'];

			$(this).css('cursor','pointer');
			$(this).bind('click',function(){
				$('#description_'+id).dialog('open');
			});
		});

		$('[id^="extra_info_"]').each(function(){
			$(this).dialog({
				bgiframe: true,
				autoOpen: false,
				autoResize:true,
				height: 250,
				width: 450,
				modal: true,
				stack: true,
				resizable: false,
				title: "Comentarios Adicionales",
				buttons: {
					'Salir': function() {
						$(this).dialog('close');
					}
				}
			});
		});

		$('[id^="show_extra_info_"]',currentTable.fnGetNodes()).each(function(){
			var id=$(this).attr('id');
			id=explode('_', id);
			id=id['3'];

			$(this).css('cursor','pointer');
			$(this).bind('click',function(){	
				$('#extra_info_'+id).dialog('open');
			});
		});
	/* FILE INFORMATION DIALOG */
	}
});