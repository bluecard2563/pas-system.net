var pager;
var pager2;
var currentTable;
$(document).ready(function(){

	if($("#medical_checkup").length>0) {
		pager = new Pager('medical_checkup', 4 , 'Siguiente', 'Anterior');
		pager.init($('#countMedicalCheckup').val()); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}
	//Pending Documents table
	if($('#docsTable').length>0){
		currentTable = $('#docsTable').dataTable( {
										"bJQueryUI": true,
										"sPaginationType": "full_numbers",
										"oLanguage": {
											"oPaginate": {
												"sFirst": "Primera",
												"sLast": "&Uacute;ltima",
												"sNext": "Siguiente",
												"sPrevious": "Anterior"
											},
											"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
											"sZeroRecords": "No se encontraron resultados",
											"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
											"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
											"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
											"sSearch": "Filtro:",
											"sProcessing": "Filtrando.."
										},
									   "sDom": 'T<"clear">lfrtip',
									   "oTableTools": {
										  "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
										  "aButtons": [ ]
									   }

						} );
		//Listener for register document				
		if($('#registerPendingDocs').length > 0){
			$('#registerPendingDocs').bind('click',function(){
				var sData = new Array();
				$("input:checked[id^='chkPending'][disabled = false]", currentTable.fnGetNodes()).each(function(){
					array_push(sData,$(this).val());
				});
				if(sData.length > 0){
					show_ajax_loader();
					$.ajax({
							url: document_root+"rpc/assistance/receivePendingDocument.rpc",
							type: "post",
							data: ({serial_pdfs : implode(',',sData)}),
							success: function(data){
								if(data=='true'){
									$("input:checked[id^='chkPending'][disabled = false]", currentTable.fnGetNodes()).each(function(){
										$(this).attr('disabled',true);
									});
									hide_ajax_loader();
									alert('Los Documentos se han Registrado con Exito');
								}else{
									hide_ajax_loader();
									alert('No se ha podido registrar el cambio');
								}
							}
					});
				}else{
					alert('Seleccione al menos un documento.');
				}
			});
		}					
	}
	
	/*PROVIDERS INFO DIALOG*/
	$('#providersInfoDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 400,
		width: 750,
		modal: true,
		stack: true,
		resizable: false,
		buttons: {
			'Salir': function() {
				$(this).dialog('close');
			}
		}
	});
	/*END PROVIDERS INFO DIALOG*/
    
	/*LOAD PROVIDERS*/
	$('#selProviders').bind("change", function(){
		$("#medicalType option[value='']").attr("selected",true);
		$('#medicalType').css("display", "none");
		$('#assistanceCost').css("display", "none");
		if($('#selProviders').val()!="") {
			show_ajax_loader();
			
			$("#benefitsContainer").load(document_root+"rpc/assistance/loadBenefitsByServiceProvider.rpc",{
				serial_spv: this.value,
				card_number: $('#txtCardNumber').val(),
				serial_fle:$('#txtSerialFle').val(),
				edit_dialog: "edit",
				liquidation: "liquidation",
				acces_request: $('#accesPermition').val()
				},function(){				
					if($("#benefitsTable").length>0) {
						pager2 = new Pager('benefitsTable', 5 , 'Siguiente', 'Anterior');
						pager2.init(7); //Max Pages
						pager2.showPageNav('pager2', 'pageNavPositionBenefits'); //Div where the navigation buttons will be placed.
						pager2.showPage(1); //Starting page
					}

					if($('#selProviders option:selected').attr('service_type') == 'MEDICAL') {
						$("#selMedicalType option[value='"+$('#hdnMedicalType').val()+"']").attr("selected",true);
						$('#medicalType').css("display", "block");
					}

					if($('#hdnAsistanceCost').val()) {
						$('#txtAssistanceCost').html("$"+$('#hdnAsistanceCost').val());
						$('#assistanceCost').css("display", "block");
					}
					
					$('#benefitsUploadContainer').load(document_root+"rpc/assistance/loadBenefitChooser.rpc",{
						serial_fle:$("#txtSerialFle").val(),
						serial_spv:$("#selProviders").val()
					}, function(){
						$('#uploadFiles').load(document_root+"rpc/assistance/loadUploadedFiles.rpc", {
							serial_fle: $('#txtSerialFle').val(),
							acces_permition:$('#accesPermition').val(),
							serial_spv:$("#selProviders").val()
						}, function(){
							hide_ajax_loader();
						});
					});		
			});
			
				
		} else {
			$("#benefitsContainer").html('');
			$('#benefitsUploadContainer').html('');
			$('#uploadFiles').html('<center>Seleccione un coordinador.</center>');
		}
	});

	$('#displayProviderInfo').bind("click", function(){
		if($('#selProviders').val() != '') {
			$('#providersInfoContainer').load(document_root+"rpc/assistance/loadContactsByServiceProvider.rpc",{
				serial_spv:$('#selProviders').val()
				});
			$("#providersInfoDialog").dialog( "option", "title", $('#selProviders option:selected').text() );
			$("#providersInfoDialog").dialog('open');
		} else {
			alert("Debe seleccionar un coordinador.");
		}
	});
	/*END LOAD PROVIDERS*/

	/*FILE BENEFITS DIALOG*/
	$('#fileBenefitsDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 465,
		width: 710,
		modal: true,
		stack: true,
		resizable: false,
		title:"Asignaci&oacute;n de beneficios a archivo",
		buttons: {
			'Aceptar': function() {
				var files="";
				var authorized_amounts="";
				var presented_amounts="";
				var sum_authorized = 0;
				var sum_presented = 0;

				$("input:checked[id^='chkBenUp']").each(function(){
					files+=this.value+",";
					if($('#txtAuthorizadAmount'+this.value).val()=='') {
						authorized_amounts+="0,";
					} else {
						authorized_amounts+=$('#txtAuthorizadAmount'+this.value).val()+",";
						sum_authorized += parseFloat($('#txtAuthorizadAmount'+this.value).val());
					}
					if($('#txtPresentedAmount'+this.value).val()=='') {
						presented_amounts+="0,";
					} else {
						presented_amounts+=$('#txtPresentedAmount'+this.value).val()+",";
						sum_presented += parseFloat($('#txtPresentedAmount'+this.value).val());
					}
				});
				files = files.substr(0, files.length-1);
				authorized_amounts = authorized_amounts.substr(0, authorized_amounts.length-1);
				presented_amounts = presented_amounts.substr(0, presented_amounts.length-1);
				$('#filesToUpload').val(files);
				$('#authorized_amounts').val(authorized_amounts);
				$('#presented_amounts').val(presented_amounts);
				$('#txtTotalPresentedAmount').val(sum_presented);
				$('#txtTotalAuthorizadAmount').val(sum_authorized);

				$(this).dialog('close');
			}
		}
	});
	/*END FILE BENEFITS DIALOG*/

	/*FILE BENEFITS DIALOG*/
	$('#fileViewBenefitsDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 465,
		width: 710,
		modal: true,
		stack: true,
		resizable: false,
		title:"Beneficios por archivo",
		buttons: {
			'Salir': function() {
				$(this).dialog('close');
			}
		}
	});
	/*END FILE BENEFITS DIALOG*/

	/*MEDICAL CHECKUP DIALOG*/
	$('#showMedicalCheckup').bind('click', function() {
		$('#medicalCheckupDialog').dialog('open');
	})

	$('#medicalCheckupDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 570,
		width: 710,
		modal: true,
		stack: true,
		resizable: false,
		title:"Datos de Auditor&iacute;a M&eacute;dica",
		buttons: {
			'Salir': function() {
				$(this).dialog('close');
			}
		}
	});
	/*END MEDICAL CHECKUP DIALOG*/

	$("#btnCloseAssistance").bind("click", function(){
		//comprobar q no tenga medical checkups pendientes
		$.getJSON(document_root+"rpc/assistance/checkPendingRequests.rpc", {
			serial_fle: $('#txtSerialFle').val()
			}, function(data){
			if(data) {
				if(confirm("Desea cerrar el expediente")) {
					if(confirm("El expediente tiene aud\u00EDtarias pendientes. Lo desea cerrar de todos modos")) {
						show_ajax_loader();
						$.getJSON(document_root+"rpc/assistance/cancelMedicalCheckups.rpc", {
							serial_fle: $('#txtSerialFle').val()
							}, function(data){
							if(data) {
								window.location = document_root+"modules/assistance/pLiquidateAssistance/"+$('#txtSerialFle').val();
							} else {
								hide_ajax_loader();
								alert("Ocurrio un error");
							}
						});
						
					} else {
						show_ajax_loader();
						window.location = document_root+"modules/assistance/fEditAssistance/0/"+$('#txtSerialFle').val();
					}
				} else {
					show_ajax_loader();
					window.location = document_root+"modules/assistance/fEditAssistance/0/"+$('#txtSerialFle').val();
				}
			} else {
				if(confirm("Desea cerrar el expediente")) {
					show_ajax_loader();
					window.location = document_root+"modules/assistance/pLiquidateAssistance/"+$('#txtSerialFle').val();
				} else {
					show_ajax_loader();
					window.location = document_root+"modules/assistance/fEditAssistance/0/"+$('#txtSerialFle').val();
				}
			}
		});
        
	});

	/*UPLOAD FILE DIALOG*/
	$("#uploading_file").dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		modal: true,
		stack: true,
		resizable: false,
		closeOnEscape: false,
		open: function(event, ui) {
			$(".ui-dialog-titlebar-close").hide();
		},
		title:"Espere por favor"
	});
/*END UPLOAD FILE DIALOG*/

	$("#blog_tabs").tabs({ 
		 heightStyle: "fill" 
	 });
});