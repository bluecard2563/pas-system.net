// JavaScript Document
function activateBenefitsChooser(){
	/*FILE BENEFITS DIALOG*/
	$('#fileBenefitsDialog').dialog({
		bgiframe: true,
		autoOpen: false,
		autoResize:true,
		height: 465,
		width: 710,
		modal: true,
		stack: true,
		resizable: false,
		title:"Asignaci&oacute;n de beneficios a archivo",
		buttons: {
			'Aceptar': function() {
				var files="";
				var authorized_amounts="";
				var presented_amounts="";
				var sum_authorized = 0;
				var sum_presented = 0;

				$("input:checked[id^='chkBenUp']").each(function(){
					files+=this.value+",";
					if($('#txtAuthorizadAmount'+this.value).val()=='') {
						authorized_amounts+="0,";
					} else {
						authorized_amounts+=$('#txtAuthorizadAmount'+this.value).val()+",";
						sum_authorized += parseFloat($('#txtAuthorizadAmount'+this.value).val());
					}
					if($('#txtPresentedAmount'+this.value).val()=='') {
						presented_amounts+="0,";
					} else {
						presented_amounts+=$('#txtPresentedAmount'+this.value).val()+",";
						sum_presented += parseFloat($('#txtPresentedAmount'+this.value).val());
					}
				});
				files = files.substr(0, files.length-1);
				authorized_amounts = authorized_amounts.substr(0, authorized_amounts.length-1);
				presented_amounts = presented_amounts.substr(0, presented_amounts.length-1);
				$('#filesToUpload').val(files);
				$('#authorized_amounts').val(authorized_amounts);
				$('#presented_amounts').val(presented_amounts);
				$('#txtTotalPresentedAmount').val(sum_presented);
				$('#txtTotalAuthorizadAmount').val(sum_authorized);

				$(this).dialog('close');
			}
		}
	});
	/*END FILE BENEFITS DIALOG*/

	if($("#benefits_upload_table").length>0) {
        pager = new Pager('benefits_upload_table', 5 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    $("#selAll").bind("click",function(){
        if($('#selAll').attr("checked")) {
            $("input[id^='chkBenUp']").attr("checked", true);
            $("input[id^='txtAuthorizadAmount']").attr("disabled",false);
			$("input[id^='txtPresentedAmount']").attr("disabled",false);
        } else {
            $("input[id^='chkBenUp']").attr("checked", false);
            $("input[id^='txtAuthorizadAmount']").attr("disabled",true);
            $("input[id^='txtAuthorizadAmount']").val('');
			$("input[id^='txtPresentedAmount']").attr("disabled",true);
            $("input[id^='txtPresentedAmount']").val('');
        }
    });

    $("input[id^='chkBenUp']").bind("click",function(){
        if(!$(this).attr("checked")) {
            $('#selAll').attr("checked", false);
            $('#txtAuthorizadAmount'+this.value).val('');
            $('#txtAuthorizadAmount'+this.value).attr("disabled",true);
			$('#txtPresentedAmount'+this.value).val('');
            $('#txtPresentedAmount'+this.value).attr("disabled",true);
        } else {
            $('#txtAuthorizadAmount'+this.value).attr("disabled",false);
			$('#txtPresentedAmount'+this.value).attr("disabled",false);
        }
    });

    $("input[id^='txtAuthorizadAmount']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }

		//Max AMount to be paid for a benefit.
		if($('#selStatusPrq option:selected').val() == "APROVED") {
			var max_amount=parseFloat($('#maxAmount'+$(this).attr('serial')).val());
			if(max_amount>0){ //If the max amount is grater than 0 (no limit)
				//Preventing of pay a grater value than the available for a specific benefit
				if(parseFloat(this.value) > max_amount) {
					alert("El valor sobrepasa el monto m\u00E1ximo disponible. Verifique los montos previamente cancelados.");
					this.value = '';
				}
			}
		}
    });

	$("input[id^='txtPresentedAmount']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
    });
}

function enableFilesTableStatusSelectionJs(){
	$('#selStatusPrq').bind('change',function(){
		$("input:checked[id^='chkBenUp']").each(function(){
			$('#txtPresentedAmount'+this.value).val('');
			$('#txtAuthorizadAmount'+this.value).val('');
			$('#txtPresentedAmount'+this.value).attr("disabled",true);
			$('#txtAuthorizadAmount'+this.value).attr("disabled",true);
			$(this).attr("checked",false);
			$('#selAll').attr("checked",false);
		});
		$('#txtTotalAuthorizadAmount').val('0.00');
		$('#txtTotalPresentedAmount').val('0.00');
		$('#authorized_amounts').val('');
		$('#presented_amounts').val('');
	});
	
	$("select[id^='changeStatusPrq']").bind("change", function(){
        if(this.value=="APROVED" || this.value=="DENIED") {
			show_ajax_loader();
            $.getJSON(document_root+"rpc/assistance/updatePaymentRequestStatus.rpc", 
				{
					serial_prq:$(this).attr('serial_prq'), 
					status_prq:this.value
				}, function(data){
					if(data) {
						location.reload();
					} else {
						hide_ajax_loader();
						alert("Ha ocurrido un error.");
					}
            });
        }
    });
	
	$('#benefitsAssign ').bind("click", function(){
		if($('#selStatusPrq option:selected').val() == "") {
			alert("Debe seleccionar el estado primero.");
		} else {
			$('#fileBenefitsDialog').dialog('open');
		}
	});
}

function remoteUpdateFileDiagnosis(file_id, diagnosis){
	show_ajax_loader();
	$.ajax({
		url: document_root + "rpc/assistance/docs/customDiagnosisUpdate.rpc",
		type: "POST",
		data: {
			file_id: file_id,
			diagnosis: diagnosis
		},
		success: function(data){
			hide_ajax_loader();
			if(data == 'ok') {
				alert('Actualizaci\u00F3n exitosa');
			}else{
				alert('Hubo errores en el proceso. Por favor vuelva a intentarlo.');
			}
		}
	});
	
}