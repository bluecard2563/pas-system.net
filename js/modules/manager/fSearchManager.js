// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchManager").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtManagerData: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/

	var ruta=document_root+"rpc/autocompleters/loadManagers.rpc";
	$("#txtManagerData").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			if(row['0']==0){
				return 'El representante ingresado no existe.';
			}else{
				return row[0];
			}
  		}
	}).result(function(event, row) {
		if(row[0]==0){
			$("#hdnManagerID").val('');
			$("#txtManagerData").val('');
		}else{
			$("#hdnManagerID").val(row[1]);
		}
	});
});