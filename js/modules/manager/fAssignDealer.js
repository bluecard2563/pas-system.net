// JavaScript Document
$(document).ready(function(){
	$('#selZone').change(function(){loadCountries($('#selZone').val())});
        $('#selManagerA').change(function(){loadDealers($('#selManagerA').val())});
	$('#dealers').hide();
	$('#noDealers').hide();
        $('#oneDealers').hide();

	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignDealer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
                        selManagerA: {
				required: true
			},
                        selCountry: {
				required: true
			}
		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/


});

function loadManagers(countryID){
	if(countryID==""){
		$("#selManagerA option[value='']").attr("selected",true);
	}
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID, serial_sel: 'A'},function(){	$('#selManagerA').change(function(){loadDealers($('#selManagerA').val())});});
        $('#newManagerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID, serial_sel: 'B'});
        $('#dealers').hide();
        $('#noDealers').hide();
        $("#selManagerA").rules("add", {
            required: true
        });
}
function loadDealers(managerID){
        var cont=0;
        $('#selManagerB option').each(function (i) {
		$('#selManagerB').find('option').show();
                cont++;
        });
        $('#selManagerB').find('[value="'+managerID+'"]').hide();
	if(managerID==''){
                $('#dealers').hide();
		$("#selDealers option[value='']").attr("selected",true);                
	}        
        else{
            $('#dealersContainer').load(document_root+"rpc/loadDealers.rpc",{serial_mbc: managerID, dea_serial_dea: 'NULL'});
            if(cont>2){
                $('#dealers').show();
                $('#noDealers').hide();
                $("#selDealer").rules("add", {
                    required: true
                });
                $("#selManagerB").rules("add", {
                    required: true
                });
            }else{
                $('#noDealers').show();
                $('#dealers').hide();
            }

        }
}

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){	$('#selCountry').change(function(){loadManagers($('#selCountry').val())});});
        $('#dealers').hide();
        $('#noDealers').hide();
        $("#selCountry").rules("add", {
            required: true
        });
}