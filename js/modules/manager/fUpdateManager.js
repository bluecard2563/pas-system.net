// JavaScript Document
var cont=1; //helps counting the managers added
var exclusiveCont; //helps counting the countries with exclusive managers
var officialCont; //helps counting the countries with official managers
var usedCountries=new Array();  //Used Countries list

/***********************************************
* funcion setCont
* set var cont with the correct value
***********************************************/
function setCont(){
    $('.manager').each(function (i) {
		if($(this).attr('manager')>cont){
		  cont=$(this).attr('manager');
		}
    });
    cont++;
}

/***********************************************
* funcion addManagerData
* prints all the manager's data from the dialog
***********************************************/
function addManagerData(updateFlag){
	var x; //helps print the data in the specific div
	var str; //data going to be printed
	var y; //helps print the countries' hiddens in the specific div
    var z; //helps print the countries' official hiddens in the specific div
	var aux; //in case of update it is declared with the attribute (updateFlag), else it's declared with (cont)
	var aux2;//in case of update it is declared with "Update", else it's declared empty
	var auxFlag;
	
	if(updateFlag){
		aux=updateFlag;
		aux2="Update";
		stateText=$("#selStatus"+aux2+" option:selected").text();
		stateVal=$("#selStatus"+aux2).val();
	}else{
		aux=cont;
		aux2="";
		stateText='Activo';
		stateVal='ACTIVE';
	}

	if($('[manager="'+aux+'"]').attr('id')){
		auxFlag=$('[manager="'+aux+'"]').attr('id');
	}else{
		auxFlag='insert';
	}

  	x=$("#managerData");
	y=$("#exclusiveHiddens");
    z=$("#officialHiddens");
	$('[hdnManager="'+aux+'"]').remove();

	if($("#chkExclusive"+aux2).is(':checked') ){		
		/*here is where data is printed in the specific div*/
		y.append('<input type="hidden" id="hdnExclusiveCountry_'+(exclusiveCont)+'" name="hdnExclusiveCountry_'+(exclusiveCont++)+'" hdnManager="'+aux+'" value="'+$("#selCountries"+aux2+" option:selected").text()+'"  class="hiddenExclusiveCountry" />');
		str='<div manager="'+aux+'" id="'+auxFlag+'" class="manager"><input type="hidden" id="hdnDbType'+aux+'" name="hdnDbType'+aux+'" value="'+auxFlag+'" /><div class="span-19 last line">&nbsp;</div><hr /><input type="hidden" id="hdnExclusive'+aux+'" name="hdnExclusive'+aux+'" value="YES" /><div class="span-24 last line title">Representante Exclusivo '
		if($("#chkOfficial"+aux2).is(':checked') ){
			str+='y Oficial: '+$("#selName option:selected").text()+'</div>';
		}else{
			str+=': '+$("#selName option:selected").text()+'</div>';
		}
	}else{
		str='<div manager="'+aux+'" id="'+auxFlag+'" class="manager"><input type="hidden" id="hdnDbType'+aux+'" name="hdnDbType'+aux+'" value="'+auxFlag+'" /><div class="span-19 last line">&nbsp;</div><hr /><input type="hidden" id="hdnExclusive'+aux+'" name="hdnExclusive'+aux+'" value="NO" /><div class="span-24 last line title">Sub-representante '
		if($("#chkOfficial"+aux2).is(':checked') ){
			str+='Oficial: '+$("#selName option:selected").text()+'</div>';
		}else{
			str+=': '+$("#selName option:selected").text()+'</div>';
		}
	}

	if($("#chkOfficial"+aux2).is(':checked') ){
		/*here is where data is printed in the specific div*/
		z.append('<input type="hidden" id="hdnOfficialCountry_'+(officialCont)+'" name="hdnOfficialCountry_'+(officialCont++)+'" hdnManager="'+aux+'" value="'+$("#selCountries"+aux2+" option:selected").text()+'"  class="hiddenOfficialCountry" />');
		str+='<input type="hidden" id="hdnOfficial'+aux+'" name="hdnOfficial'+aux+'" value="YES" />';
	}else{
		str+='<input type="hidden" id="hdnOfficial'+aux+'" name="hdnOfficial'+aux+'" value="NO" />';
	}

	str+='<div class="span-24 last line">&nbsp;</div><div class="span-24 last line"><div class="prepend-9 span-3 label">Pa&iacute;s:</div><div class="span-4">'+$("#selCountries"+aux2+" option:selected").text()+'</div><input type="hidden" id="hdnCountry'+aux+'" name="hdnCountry'+aux+'" value="'+$("#selCountries"+aux2).val()+'" /><div class="span-24 last  line"><div class="prepend-9 span-3 label">Estado:</div><div class="span-4">'+stateText+'</div><input type="hidden" id="hdnStatus'+aux+'" name="hdnStatus'+aux+'" value="'+stateVal+'" /></div><div class="span-24 last  line"><div class="span-4 prepend-10 buttons">';

	if(auxFlag=='insert'){
		str+='<input type="button" name="btnRemove" manager="'+aux+'" id="btnRemove" value="Quitar" class="ui-state-default ui-corner-all">&nbsp;<input type="button" name="btnEdit"  edit="'+aux+'" id="btnEdit" value="Editar" class="ui-state-default ui-corner-all"></div></div></div>';
	}else{
		str+='<input type="button" name="btnEdit"  edit="'+aux+'" id="btnEdit" value="Editar" class="ui-state-default ui-corner-all"></div></div></div>';
	}
	
	if(updateFlag){//in case of update the data is replaced
		$('[manager="'+aux+'"]').replaceWith(str);
	}
	else{//in case of insert the data is printed
		x.append(str);

	}

	$('#lastButtons').show();
	$(':button[manager="'+aux+'"]').click(function(){//used for remove's button
		if(confirm("�Est\u00E1 seguro que desea quitar este representante?")){
			for(i in usedCountries){
				if(usedCountries[i]==$('#hdnCountry'+aux).val()){
					delete usedCountries[i];
					break;
				}
			}
			
			$('[manager="'+$(this).attr("manager")+'"]').remove();
			$('[hdnManager="'+$(this).attr("manager")+'"]').remove();
		}		
	});
	
	$(':button[edit="'+aux+'"]').click(function(){//used for edit's button
		loadUpdateManagerDialog($(this).attr("edit"));
	});	
	
	if(!updateFlag){//in case of insert var: cont increases by one
		$('#hdnCont').val(cont);
		/*Saving the Country ID's Used in a JS array*/
		
		if($('#selCountries').length>0){ //ID from the newManagerDialog
			array_push(usedCountries, $('#selCountries').val());
		}else{ //ID from the newUpdateManagerDialog
			array_push(usedCountries, $('#selCountriesUpdate').val());
		}
	}
}

function loadNewManagerDialog(){//loads insertion dialog
    setCont();
	var usedCountriesCount=0;
	$('#newManagerDialog').dialog('open');

	if(usedCountries.length>0){
		for(i in usedCountries){
			$("#selCountries option[value='"+usedCountries[i]+"']").hide();
			usedCountriesCount++;
		}
	}

	if(usedCountriesCount==$('#selCountries option').size()){
		$('#newManagerDialog').dialog('close');
		alert("Este representante ya est\u00E1 asignado a todos los pa\u00EDses");
	}
}

function loadManagerData(){
	var managerID=$('#hdnManagerId').val();
	$('#hdnSerialMbc').val(managerID);
	if(managerID!=""){
		$('#managerData').load(document_root+"rpc/loadManagerData.rpc",{serial_man: managerID},function(){
			$('[id^="hdnCountry"]').each(function(){
				array_push(usedCountries, $(this).val());
			});
		});
		$('#lastButtons').show();
	}else{
		alert("Seleccione un usuario");
	}
}

function loadUpdateManagerDialog(cont){//loads update dialog and data
	$('#updateManagerDialog').dialog('open');
	$("#selCountriesUpdate option[value='"+$('[manager="'+cont+'"] > .line > #hdnCountry'+cont).val()+"']").attr("selected",true);
	$("#selPercentageTypeUpdate option[value='"+$('[manager="'+cont+'"] > .line > #hdnPercentageType'+cont).val()+"']").attr("selected",true);
	$("#selPaymentDeadlineUpdate option[value='"+$('[manager="'+cont+'"] > .line > #hdnPaymentDeadline'+cont).val()+"']").attr("selected",true);
	$("#txtPercentageUpdate").val($('[manager="'+cont+'"] > .line > #hdnPercentage'+cont).val());
        $("#selInvoiceUpdate option[value='"+$('[manager="'+cont+'"] > .line > #hdnInvoice'+cont).val()+"']").attr("selected",true);
	if($('[manager="'+cont+'"] > .line > #hdnExclusive'+cont).val()==1){
		exclusiveExistsUpdate(cont);
	}
	else{
		exclusiveExistsUpdate(cont);
	}
        if($('[manager="'+cont+'"] > .line > #hdnOfficial'+cont).val()=='YES'){
		officialExistsUpdate(cont);
	}
	else{
		officialExistsUpdate(cont);
	}
	$("#hdnSelectedDiv").val(cont);
	$("#selStatusUpdate option[value='"+$('[manager="'+cont+'"] > .line > #hdnStatus'+cont).val()+"']").attr("selected",true);

	$('.manager').each(function (i) {
		if($(this).attr('manager')!=cont){
			for(i in usedCountries){
				$("#selCountriesUpdate option[value='"+usedCountries[i]+"']").hide();
			}
		}
	});
    checkStatus();
}

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){$('#selCountry').change(function(){loadCities($('#selCountry').val())});});
}


function exclusiveExists(){//check if the selected country already has an exclusive manager
	var flag;
	flag=0;
	$(".hiddenExclusiveCountry").each(function (i) {
			if($(this).val()==$('#selCountries option:selected').text()){
				flag=1;
				return false;
			}				
		}
	);
	if(flag==1){
		$('#chkExclusive').attr('checked', false);
		$('#subManager').show();
		$('#exclusiveManager').hide();
	}else{
		$('#subManager').hide();
		$('#exclusiveManager').show();
	}
}

function officialExists(){//check if the selected country already has an official manager
	var flag;
	flag=0;
	$(".hiddenOfficialCountry").each(function (i) {
			if($(this).val()==$('#selCountries option:selected').text()){
				flag=1;
				return false;
			}
		}
	);
	if(flag==1){
		$('#chkOfficial').attr('checked', false);
		$('#subOfficial').show();
		$('#officialManager').hide();
	}else{
		$('#subOfficial').hide();
		$('#officialManager').show();
	}
}

function exclusiveExistsUpdate(selected){//check if the selected country already has an exclusive manager but in case of an update
	var flag;
	flag=0;
	$(".hiddenExclusiveCountry").each(function (i) {
			if($(this).val()==$('#selCountriesUpdate option:selected').text() && $(this).attr('hdnManager')!=selected){
				flag=1;
			}
			else if($(this).attr('hdnManager')==selected){			
				flag=2;
			}
		}
	);
	if(flag==1){
		$('#chkExclusiveUpdate').attr('checked', false);
		$('#subManagerUpdate').show();
		$('#exclusiveManagerUpdate').hide();
	}else if(flag==2){
		$('#chkExclusiveUpdate').attr('checked', true);                
		$('#subManagerUpdate').hide();
		$('#exclusiveManagerUpdate').show();
	}
	else{
		$('#subManagerUpdate').hide();
		$('#exclusiveManagerUpdate').show();
	}
}

function officialExistsUpdate(selected){//check if the selected country already has an exclusive manager but in case of an update
	var flag;
	flag=0;
	$(".hiddenOfficialCountry").each(function (i) {
			if($(this).val()==$('#selCountriesUpdate option:selected').text() && $(this).attr('hdnManager')!=selected){
				flag=1;
			}
			else if($(this).val()==$('#selCountriesUpdate option:selected').text() && $(this).attr('hdnManager')==selected){

				flag=2;
			}
		}
	);
	if(flag==1){
		$('#chkOfficialUpdate').attr('checked', false);
		$('#subOfficialUpdate').show();
		$('#officialManagerUpdate').hide();
	}else if(flag==2){
		$('#chkOfficialUpdate').attr('checked', true);
                $('#chkOfficialUpdate').attr('disabled', true);
		$('#subOfficialUpdate').hide();
		$('#officialManagerUpdate').show();
	}
	else{
		$('#subOfficialUpdate').hide();
		$('#officialManagerUpdate').show();
	}
}

function checkStatus(){
	if($('#selStatusUpdate').val()=='INACTIVE'){
		$('#chkExclusiveUpdate').attr('checked', false);
		$('#chkExclusiveUpdate').attr('disabled', true);
	}else{
		$('#chkExclusiveUpdate').attr('disabled', false);
	}
}


$(document).ready(function(){
    setDefaultCalendar($("#txtContractDate"),'-100y','+0d','-0y');
	$('#selZone').change(function(){
            loadCities(0)
            loadCountries($('#selZone').val())            
        });
        $('#hdnCont').val(cont);
        $("#frmUpdateManager").wizard({
		show: function(element) {
			},
		next:"Siguiente >",
		prev:"< Anterior",
		action:"Actualizar",
		extra:"ui-state-default ui-corner-all"
	});
	loadManagerData();
	$('#btnAssignNew').click(loadNewManagerDialog);
	$('#selCountries').change(exclusiveExists);
    $('#selCountries').change(officialExists);
	$('#selStatusUpdate').change(checkStatus);
	$('#selCountriesUpdate').change(function(){
		exclusiveExistsUpdate($('#hdnSelectedDiv').val());
		officialExistsUpdate($('#hdnSelectedDiv').val());
	});
	$('#lastButtons').hide();
	$('#subManager').hide();
	$('#exclusiveManager').hide();
	$('#subManagerUpdate').hide();
	$('#exclusiveManagerUpdate').hide();
	$('#subOfficial').hide();
	$('#officialManager').hide();
	$('#subOfficialUpdate').hide();
	$('#officialManagerUpdate').hide();
	exclusiveCont=$('#hdnExclusiveSize').val();
    officialCont=$('#hdnExclusiveSize').val();
	
    /*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmUpdateManager").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selZone: {
				required: true
			},
			txtDocument: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/manager/checkManagerDocumentByCountry.rpc",
					type: "POST",
					data: ({serial_cou:function(){return $('#selCountry').val()},
							document:function(){return $('#txtDocument').val()},
							serial_man: function (){return $('#hdnManagerId').val()}
							})
				}
			},
			txtName: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/manager/checkManagerName.rpc",
					type: "POST",
					data: {
						serial_man: function() {
						return $("#hdnManagerId").val();
					  }
					}
				}
			},
			txtAddress: {
				required: true
			},
			txtPhone: {
				required: true,
				number: true
			},
			txtContractDate: {
				required: true,
				date: true
			},
			txtContact: {
				required: true,
				textOnly: true
			},
			txtContactPhone: {
				required: true,
				number: true
			},
			txtContactEmail: {
				required: true,
				email: true
			},
			selType: {
				required: true
			},
			selPercentageType: {
				required: true
			},
			selPaymentDeadline: {
				required: true
			},
			selInvoice: {
				required: true
			},
			txtPercentage: {
				required: true,
				number: true,
				percentage: true,
				max_value: 100
			}
		},
		messages: {
			txtName: {
				remote: "El nombre ingresado ya existe."
			},
			txtDocument: {
				remote: "El documento ingresado ya existe."
			},
			txtPhone: {
				number: 'El campo "Tel&eacute;fono #1" admite s&oacute;lo n&uacute;meros.'
			},
			txtContractDate: {
				date: 'El campo "Fecha de contrato" debe tener formato fecha.'
			},
			txtFax: {
				number: 'El campo "Fax" admite s&oacute;lo n&uacute;meros.'
			},
			txtContact: {
				textOnly: 'El campo "Nombre del Contacto" admite s&oacute;lo texto.'
			},
			txtContactPhone: {
				number: 'El campo "Tel&eacute;fono del Contacto" admite s&oacute;lo n&uacute;meros.'
			},
			txtContactEmail: {
				email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Contacto"'
			},
			txtPercentage: {
				number: "El campo 'Valor Comisi&oacute;n' admite s&oacute;lo n&uacute;meros.",
				percentage: "La comisi&oacute;n solo puede tener valores de 0 a 100",
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			}
   		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/

	/*Dialog*/
	$("#newManagerDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 450,
		modal: true,
		buttons: {
			'Guardar': function() {
				/*VALIDATION*/
				$("#frmNewManagerDialog").validate({
						errorLabelContainer: "#alerts_NewManagerDialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							selCountries: {
								required: true
							}
						}
				});

				if($('#frmNewManagerDialog').valid()){
					addManagerData();
					$('#managerData').show();
					 $(this).dialog('close');
				}
				/*END VALIDATION*/

			},
			Cancelar: function() {
				$('#alerts_NewManagerDialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('#selCountries').val("");
			$("#chkExclusive").attr('checked', false);
			$('#subManager').hide();
			$('#exclusiveManager').hide();
            $("#chkOfficial").attr('checked', false);
			$('#subOfficial').hide();
			$('#officialManager').hide();
		}
	});
	$("#updateManagerDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 450,
		modal: true,
		buttons: {
			'Guardar': function() {
				/*VALIDATION*/
				$('#frmUpdateManagerDialog').submit();
				/*END VALIDATION*/
			},
			Cancelar: function() {
				$('#alerts_UpdateManagerDialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('#selCountriesUpdate').val("");
			$("#chkExclusiveUpdate").attr('checked', false);
            $('#chkExclusiveUpdate').attr('disabled', false);
			$('#subManagerUpdate').hide();
			$('#exclusiveManagerUpdate').hide();
            $("#chkOfficialUpdate").attr('checked', false);
            $('#chkOfficialUpdate').attr('disabled', false);
			$('#subOfficialUpdate').hide();
			$('#officialManagerUpdate').hide();
			$('#selStatus').val("");                        
		}
	});

	$("#frmUpdateManagerDialog").validate({
			errorLabelContainer: "#alerts_UpdateManagerDialog",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				selCountriesUpdate: {
					required: true
				},
				selStatusUpdate: {
					required: true,
					remote: {
							url: document_root+"rpc/remoteValidation/manager/checkTypeManager.rpc",
							type: "POST",
							data: {
								  serial_mbc: function() {
										return $("#hdnManagerId").val();
								  }
							}
					}
				},
				txtPercentageUpdate: {
					required: true,
					number: true,
					percentage: true,
					max_value: 100
				}
			},
			messages: {
				selStatusUpdate: {
					remote: "Este Representante no puede ser desactivado ya que tiene comercializadores asignados, si desea desactivarlos reasigne estos comercializadores a otro representante."
				}

			},
			submitHandler: function(form) {
					addManagerData($('#hdnSelectedDiv').val());
					$('#managerData').show();
					$("#updateManagerDialog").dialog('close');
					//form.submit();
					//$('#iframeUpload').css("display","inline");
			}
	});
	/*End Dialog*/
});

