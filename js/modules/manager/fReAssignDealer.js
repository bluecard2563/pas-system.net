// JavaScript Document
$(document).ready(function(){

	/*VALIDATION SECTION*/
	$("#frmReAssignDealer").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selCountry: {
                    required: true
                },
                selManagerSource: {
                        required: true
                },
                selManagerDestiny: {
                        required: true
                },
                selZone: {
                        required: true
                }
            },
            submitHandler: function(form) {
                if($("#frmReAssignDealer").valid()) {
                    $("#btnRegister").attr('disabled','true');
                    $("#btnRegister").val('Espere por favor...');
                    form.submit();
                }
            }
	});
	/*END VALIDATION SECTION*/

    /*CHANGE OPTIONS*/
    $('#selZone').change(function(){
        loadCountries($('#selZone').val());
    });

	$('#selStatus').change(function(){
        loadDealersByManager();
    });
    //If #selCountry is preloaded with only 1 element
    if($("#selZone option").size() <=2 ){
        loadCountries($("#selCountry").val());
    }
    listSelectorButtons();
    /*END CHANGE*/
});
function loadManagersByCountry(countryID){
	$('#managerContainerSource').load(document_root+"rpc/loadManagersByCountry.rpc",
        {serial_cou: countryID, serial_sel: 'Source'},
        function(){
            $('#selManagerSource').change(function(){
                loadDealersByManager();
            });            
            
            loadDealersByManager();
        });
        $('#managerContainerDestiny').load(document_root+"rpc/loadManagersByCountry.rpc",
        {serial_cou: countryID, serial_sel: 'Destiny'});
}

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){	
            $('#selCountry').change(function(){
                loadManagersByCountry($('#selCountry').val());
            });
            loadManagersByCountry($("#selCountry").val());
        });
}
function loadDealersByManager(){
	var managerId = $('#selManagerSource').val();
	var status = $('#selStatus').val();

     if(managerId != "" && status != ''){
            $('#dealersFromContainer').load(document_root+"rpc/loadDealersByManager.rpc",
			{
				serial_mbc: managerId,
				phone_dealers: 'YES',
				status_dea: status
			});
     }else{
         $('#dealersFromContainer').html('Seleccione un representante y estado');
     }
}
function listSelectorButtons(){
    /*LIST SELECTOR*/
    $('#moveAll').bind('click', function () {
        $('#dealersFrom option').each(function (i) {
            $('#dealersTo').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $('#removeAll').bind('click', function () {
        $('#dealersTo option').each(function (i) {
            //if($(this).attr('used') == 0){
                if($(this).attr('manager') == $('#dealersFrom').attr('manager')){
                    $('#dealersFrom').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
                }
                $(this).remove();
            //}
        });
    });

    $('#moveSelected').bind('click', function () {
        $('#dealersFrom option:selected').each(function (i) {
            $('#dealersTo').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $('#removeSelected').bind('click', function () {
        $('#dealersTo option:selected').each(function (i) {
            //if($(this).attr('used') == 0){
                if($(this).attr('manager') == $('#dealersFrom').attr('manager')){
                    $('#dealersFrom').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
                }
                $(this).remove();
            //}else{
            //    alert('Este comercializador ya ha realizado un venta.');
           // }
        });
    });
    /*END LIST SELECTOR*/
}