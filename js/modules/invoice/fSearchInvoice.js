// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchInvoice").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
                        selZone: {
				required: true
			},
                        selCountry: {
				required: true
			},
                        selManager: {
				required: true
			},
                        selDocument: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/

        $('#selZone').change(function(){
			loadCountries($('#selZone').val());
			$('#invoiceContainer').hide();
			$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selDocument').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		});  
});

function loadCountries(zoneId){
	$('#selCountry').rules("add", {
		 required: true
	});

	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
		$('#selCountry').change(function(){
			loadManagersByCountry($('#selCountry').val());
			$('#invoiceContainer').hide();
			$("#txtInvoiceNumber").val('');
		});

		if($("#selCountry option").size() <=2 ){
			loadManagersByCountry($("#selCountry").val());
		}
	});
}
function loadManagersByCountry(countryID){
	$('#selDocument').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

    $('#selManager').rules("add", {
             required: true
        });
    
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
		if($('#selManager option').size()==2){
			loadDocumentsByManager($('#selManager').val());
		}
		
        $('#selManager').change(function(){
			loadDocumentsByManager($('#selManager').val());
            if($('#selDocument').val()){
                $('#invoiceContainer').show();
                $('#txtInvoiceNumber').rules("add", {
                     required: true,
                     remote: {
                            url: document_root+"rpc/remoteValidation/invoice/checkInvoice.rpc",
                            type: "post",
                            data: {
                                number_inv: $("#txtInvoiceNumber").val(),
                                serial_mbc: $("#selManager").val(),
                                type_dbm: $("#selDocument").val()
                            }

                     },
                     messages:{
                            remote: "El n&uacute;mero de factura ingresado no existe para las opciones seleccionadas, o la factura ya est&aacute; pagada."
                    }
                });
            }
            if(!$('#selManager').val()){
                $('#invoiceContainer').hide();
            }
            $("#txtInvoiceNumber").val('');

        });
    });    
    
}
function loadDocumentsByManager(managerID){
    $('#selDocument').rules("add", {
             required: true
        });

    $('#documentContainer').load(document_root+"rpc/loadDocumentsByManager.rpc",{serial_mbc: managerID},function(){
        if($('#selManager').val() && $('#selDocument').val()){
            $('#invoiceContainer').show();
            $('#txtInvoiceNumber').rules("add", {
                     required: true,
                     remote: {
                            url: document_root+"rpc/remoteValidation/invoice/checkInvoice.rpc",
                            type: "post",
                            data: {
                                number_inv: $("#txtInvoiceNumber").val(),
                                serial_mbc: $("#selManager").val(),
                                type_dbc: $("#selDocument").val()
                            }
                     },
                     messages:{
                            remote: "El n&uacute;mero de factura ingresado no existe para las opciones seleccionadas, o la factura ya est&aacute; pagada, o ya existe una solicitud de anulaci&oacute;n para esa factura."
                    }
                });
        }

        $('#selDocument').change(function(){
            if($('#selManager').val()){
                $('#invoiceContainer').show();
                $('#txtInvoiceNumber').rules("add", {
                     required: true,
                     remote: {
                            url: document_root+"rpc/remoteValidation/invoice/checkInvoice.rpc",
                            type: "post",
                            data: {
                                number_inv: $("#txtInvoiceNumber").val(),
                                serial_mbc: $("#selManager").val(),
                                type_dbc: $("#selDocument").val()
                            }
                     },
                     messages:{
                            remote: "El n&uacute;mero de factura ingresado no existe para las opciones seleccionadas, o la factura ya est&aacute; pagada, o ya existe una solicitud de anulaci&oacute; para esa factura."
                    }
                });
            }
			
            if(!$('#selDocument').val()){
                $('#invoiceContainer').hide();
            }
            $("#txtInvoiceNumber").val('');
        });
    });
   }