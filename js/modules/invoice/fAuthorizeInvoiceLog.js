// JavaScript Document
var pager;
$(document).ready(function(){
        $("#selVoidInvoice").change(function(){
           if($("#selVoidInvoice").val()=='AUTHORIZED'){
                $("#voidSalesContainer").show();
                $("#btnAuthorize").val('Autorizar');
                $("#buttonContainer").show();
           }else if($("#selVoidInvoice").val()=='DENIED'){
                $("#voidSalesContainer").hide();
                $("#btnAuthorize").val('No Autorizar');
                $("#buttonContainer").show();
           }else{
                $("#voidSalesContainer").hide();
                $("#buttonContainer").hide();
           }
        });
	/*VALIDATION SECTION*/
	$("#frmAuthorizeInvoiceLog").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selVoidInvoice: {
                    required: true
                },
				txtObservations: {
					required: true,
					textOnly: true
				}
            }, messages: {
				txtObservations: {
					textOnly: "El campo 'Observaciones' admite s&oacute;lo texto."
				}
			}
    });
    /*END VALIDATION SECTION*/
    if($("#selAll")){
		$("#selAll").bind("click",checkAll);
    }
    //Paginator
    if($('#salesTable').size()>0){
        pager = new Pager('salesTable', 5 , 'Siguiente', 'Anterior');
        pager.init(100); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	$('[name^="chkSales"]').each(function(){
		$(this).bind('click',function(){
			if($(this).is(':checked')){
				$('#chkInternationalFee_'+$(this).val()).attr('disabled',false);
			}else{
				$('#chkInternationalFee_'+$(this).val()).attr('disabled', true);
				$('#chkInternationalFee_'+$(this).val()).attr('checked',false);
			}
		});
	});
}
);

//checkAll - select\deselect all the sales
function checkAll() {
	if($("#selAll").is(':checked')){
		$('[name^="chkSales"]').each(function(){
			$(this).attr('checked',true);
			$('#chkInternationalFee_'+$(this).val()).attr('disabled',false);
		});
	}else{
		$('[name^="chkSales"]').each(function(){
			$(this).attr('checked',false);
			$('#chkInternationalFee_'+$(this).val()).attr('disabled', true);
			$('#chkInternationalFee_'+$(this).val()).attr('checked',false);
		});
	}
}