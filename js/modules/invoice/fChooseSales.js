/*
 File:fChooseSales.js
 Author: Nicolas Flores
 Creation Date:02/03/2010
 Modified By:
 Last Modified: 
 */
$(document).ready(function() {
//

	var pager;
	if ($("#selCountry").val()) {
		loadManagersByCountry($("#selCountry").val());
	}
	$("#selCountry").bind("change", function(e) {
		loadManagersByCountry(this.value);
		clearDealerContainer();
	});
	$("#selType").bind("change", function(e) {
		clearDealerContainer();
	});
	$("#selCategory").bind("change", function(e) {
		clearDealerContainer();
	});
	$("#selResponsable").bind("change", function(e) {
		clearDealerContainer();
	});
	$("#selManager").bind("change", function(e) {
		clearDealerContainer();
	});
	$("#searchBranches").bind("click", function(e) {
		$("#alerts").html('');
		$("#alerts").css('display', 'none');
		evaluateFilters();
	});
 function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmChooseSales").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selectedSales: {
				required: true
			}
		},
		submitHandler: function(form) {
			$("#alerts").html('');
			$("#alerts").css('display', 'none');
			var equalto = true;
			var taxes = '';
			var arrayUnique=[];
			var unique=0;
			$(".salesChk").each(function(i) {
				if ($(this).is(':checked')) {
					 var s_pxc = $(this).parent().find('input[type=hidden]').val();
                    arrayUnique.push(s_pxc);
                    unique = arrayUnique.filter( onlyUnique );
					if (taxes == '') {
						taxes = $(this).attr('taxes');
					} else {
						if ($(this).attr('taxes') != taxes) {
							equalto = false;
							return;
						}
					}
				}
			});
			if (unique.length>1){
                alert('Para facturar en conjuntos las ventas los productos deben ser los mismos.');
                return;
            }
			if (equalto == true) {
				form.submit();
			} else {
				$("#alerts").append("<li><label class='error_validation'>Para facturar en conjunto las ventas deben tener los mismos impuestos aplicados.</label></li>");
				$("#alerts").css('display', 'block');
			}
		}
	});

    //Message confirm send mail
    $('#paymentPtoPMail').bind('click',function(){
        setTimeout ("alert('El correo electrónico ha sido enviado al cliente');", 3000);
    });



    $('#term').click(function(){
        // $("#customPopUp2").show();
        var customPopUp2=$('#customPopUp2').val();
        $('<div id="dialog">'+customPopUp2+'</div>').appendTo('body');
        event.preventDefault();

        if($('#dialog').size() > 0){
            $("#dialog").dialog({
                modal: true,
                draggable: false,
                maxWidth: 820,
                minWidth: 820,
                width: 820,
                maxHeight: 555,
                minHeight: 555,
                height: 555,
                title: $('#popTitle').val(),
                close: function(event, ui) {
                    $("#dialog").remove();
                }
            });
        }
    });


    $('#pre').click(function(){
        // $("#customPopUp2").show();
        var customPopUp3=$('#customPopUp3').val();
        $('<div id="dialog3">'+customPopUp3+'</div>').appendTo('body');
        event.preventDefault();

        if($('#dialog3').size() > 0){
            $("#dialog3").dialog({
                modal: true,
                draggable: false,
                maxWidth: 820,
                minWidth: 820,
                width: 820,
                maxHeight: 555,
                minHeight: 555,
                height: 555,
                title: $('#popTitle').val(),
                close: function(event, ui) {
                    $("#dialog3").remove();
                }
            });
        }
    });


    var checker = document.getElementById('checkme');
    var btnPtPS = document.getElementById('btnPtPS');
    var btnPtPM = document.getElementById('btnPtPM');
    checker.onchange = function() {

        btnPtPS.disabled = !this.checked;
        btnPtPM.disabled = !this.checked;

    };

    $( "#customPopUp2" ).dialog( "option", "buttons", buttons);
    $( "#customPopUp3" ).dialog( "option", "buttons", buttons);







    /*FIN DE LA SECCI�N DE VALIDACIONES*/
});
function loadManagersByCountry(countryID) {
	$('#responsableContainer').html("<select name='selResponsable' id='selResponsable'><option value=''>- Seleccione -</option></select>");
	$('#managerContainer').load(document_root + "rpc/loadInvoiceManagersByCountry.rpc",
			{serial_cou: countryID},
	function() {
		$("#selManager").bind("change", function(e) {
			loadResponsablesByManager(this.value);
		});
		if ($("#selManager option").size() <= 2) {
			loadResponsablesByManager($("#selManager").val());
		}
	});

}
function loadResponsablesByManager(managerID) {
	$('#responsableContainer').load(document_root + "rpc/loadsInvoices/loadInvResponsablesByManager.rpc", {serial_mbc: managerID},
	function() {
		$("#selResponsable").bind("change", function(e) {
			clearDealerContainer();
		});
	});
}
function evaluateFilters() {
	var next = true;
	if ($("#selCountry").val() == '') {
		$("#alerts").append("<li><label class='error_validation'>Seleccione Almenos un Pa&iacute;s</label></li>");
		$("#alerts").css('display', 'block');
		next = false;
	}
	if ($("#selManager").val() == '') {
		$("#alerts").append("<li><label class='error_validation'>Seleccione Almenos un Representante</label></li>");
		$("#alerts").css('display', 'block');
		next = false;
	}
	if (next) {
		var serial_mbc = $("#selManager").val();
		var serial_usr = ($("#selResponsable").val() == '') ? false : $("#selResponsable").val();
		var dealer_cat = ($("#selCategory").val() == '') ? false : $("#selCategory").val();
		var serial_dlt = ($("#selType").val() == '') ? false : $("#selType").val();
		loadDealers(serial_mbc, serial_usr, dealer_cat, serial_dlt);
		$("#selManager").bind("change", function(e) {
			clearDealerContainer();
		});
	}
}
function loadDealers(serial_mbc, serial_usr, dealer_cat, serial_dlt) {
	$('#dealerContainer').load(document_root + "rpc/loadInvoiceDealers.rpc",
			{serial_mbc: serial_mbc,
				serial_usr: serial_usr,
				dealer_cat: dealer_cat,
				serial_dlt: serial_dlt},
	function() {
		$("#selDealer").bind("change", function(e) {
			loadBranches(this.value);
		});
		if ($("#selDealer option").size() <= 2) {
			loadBranches($("#selDealer").val());
		}
	});
}
function clearDealerContainer() {
	$('#dealerContainer').html('');
	clearSalesContainer();
}
function clearSalesContainer() {
	$('#salesContainer').html('');
}
function loadBranches(dealerID) {
	clearSalesContainer();
	if (dealerID == '') {
		dealerID = 'stop';
	}
	$("#branchContainer").load(document_root + "rpc/loadInvoiceBranches.rpc", {dea_serial_dea: dealerID},
	function() {
		$("#selBranch").bind("change", function(e) {
			clearSalesContainer();
			loadSales(this.value);
		});
	});
}
function loadSales(branchID) {
	//get sales status registered and serial_inv NULL
	$('#salesContainer').load(document_root + "rpc/loadsInvoices/loadSales.rpc", {serial_dea: branchID, serial_cou: $("#selCountry").val()},
	function() {
		if ($("#selAll")) {
			$("#selAll").bind("click", checkAll);
			$(".salesChk").each(function(i) {
				$(this).bind('click', function() {
					salesChecked();
				});
			});
		}
		//Paginator
		if ($('#salesTable').length > 0) {
			pager = new Pager('salesTable', 10, 'Siguiente', 'Anterior');
			pager.init($('#total_pages').val()); //Max Pages
			pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
			pager.showPage(1); //Starting page
		}
	});
}

//checkAll
//select\deselect all the sales
function checkAll() {
	var nodoCheck = document.getElementsByTagName("input");
	var varCheck = document.getElementById("selAll").checked;
	for (i = 0; i < nodoCheck.length; i++) {
		if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "selAll" && nodoCheck[i].disabled == false) {
			nodoCheck[i].checked = varCheck;
		}
	}
	salesChecked();
}
//salesChecked
//checks if theres is at least one sale choosen
function salesChecked() {
	var checked = 0;
	$(".salesChk").each(function(i) {
		if ($(this).is(':checked')) {
			checked = 1;
			return;
		}
	});
	if (checked == 1) {
		$('#selectedSales').val('1');
	} else {
		$('#selectedSales').val('');
	}
}

