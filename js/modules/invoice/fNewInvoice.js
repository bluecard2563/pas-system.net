/*
File:fNewInvoice.js
Author: Nicolas Flores
Creation Date:09/03/2010
Modified By:
Last Modified:11/03/2010
*/
$(document).ready(function(){
    /*VALIDATION*/
    $("#frmInvoice").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtTotal: {
                required: true
            },
            txtSubTotal: {
                required: true
            },
            txtSalesTotal: {
                required: true
            },
            txtInvoiceNumber: {
                required: true,				
                digits: true,
                remote: {
                    url: document_root+"rpc/remoteValidation/invoice/checkDocumentNumber.rpc",
                    type: "post",
                    data: {
                        number_inv: function() {
                            return $('#txtInvoiceNumber').val();
                        },
                        serial_man: function() {
                            return $('#hdnSerialMan').val();
                        },
                        type_dbm: function(){
                            return $('#hdnTypeDbc').val();
                        }
                    }
                }
            },
            txtDiscount: {
                required: true
            },
            txtDate:{
                required: true
            },
            txtDealer: {
                required: true
            },
            txtOtherDiscount:{
                percentage: true,
                max_value: 100
            }
        },
        messages: {
            txtOtherDiscount:{
                percentage: "El campo 'Otro descuento' solo admite valores de 0 a 100 con dos decimales.",
                max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
            },
            txtInvoiceNumber:{
                digits: "El campo facturas solo admite n&uacute;meros enteros.",
                remote: "El n&uacute;mero de factura ingresado ya existe."
            },
            hdnCheckInvoiceNumber:{
                remote: "El n&uacute;mero de factura ingresado ya existe para este representante."
            }
        },
        submitHandler: function(form) {		
            show_ajax_loader();
			if(validate_cost()){
				document_type = 'RUC';

				if($('#customerData').css('display') != 'none'){
					$('[id^="rdDocumentType_"]').each(function(){
						if($(this).is(':checked')){
							document_type = $(this).val();
						}
					});
				}            

				$.ajax({
					url: document_root+"rpc/remoteValidation/invoice/checkHolderDocumentNumber.rpc",
					type: "post",
					data: ({
						validate_document: $('#hdnValidateHolderDocument').val(),
						document_type: document_type,
						branch_id: function(){
							return $('#hdnDealerDocument').val();
						},
						customer_id: function(){
							return $('#txtCustomerDocument').val();
						},					
						invoice_to: function(){
							if($('#radio_customer').is(':checked')){
								return 'CUSTOMER';
							}else{
								return 'DEALER';
							}
						}
					}),
					dataType: 'json',
					success: function(valid_document) {
						hide_ajax_loader();

						if(valid_document){
							$('#btnRegister').attr('disabled', 'true');
							$('#btnRegister').val('Espere por favor...');

							form.submit();
						} else {
							alert('El documento ingresado no es v\u00E1lido. Por favor ingrese una CI o RUC v\u00E1lido.');
						}						
					}
				});
			}	
        }
    });
    /*END VALIDATION*/
	
    checkType();
    $('#selCountry').change(function(){
        loadCities($('#selCountry').val())
    });

    if($('#hdnTypeDbc').val()){
        loadInvoiceNumber($('#hdnTypeDbc').val());
    }

    $("#selType").change(function(){
        personTypeBindings();
    });
	
    $('#txtInvoiceNumber').change(function(){
        $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);
    });
	
    $('input[name=bill_to_radio]').each(function(){
        $(this).bind('click',function(){
            if($(this).val()=='DEALER'){
                $('#dealerData').show();
                $('#customerData').hide();
            }else{
                $('#dealerData').hide();
                $('#customerData').show();
            }
			
            $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);
			
            if($(this).is(':checked')){				
                evaluate_bill_to_radio($(this));
            }
        });

        if($(this).is(':checked')){			
            evaluate_bill_to_radio($(this));
        }
    });

    $('#txtCustomerDocument').bind('change',function(){
        var docCust= $(this).val();
        loadCustomer(docCust);
    });

    validate_cost();

    $("#txtOtherDiscount").bind("keyup", function(e){
        if(e.keyCode!=13){
            calculateTotal();
        }
    });
	
    $('[id^="rdDocumentType"]').each(function(){
        $(this).bind('click', function(){
            $('#selType').val($(this).attr('person_type'));
            personTypeBindings();
        });
    });
});

function validate_cost(){
	var alternSubTotal = parseFloat( $("#txtSubTotal").val() ) - ( parseFloat($("#txtSubTotal").val() ) * parseFloat($("#hdnComission").val()));
	
    if(parseFloat($("#txtSubTotal").val()) < parseFloat($("#h_TotalCost").val()) || alternSubTotal < parseFloat($("#h_TotalCost").val())){
        alert('El costo de la venta no puede ser Mayor al SubTotal.');
        $("#txtTotal").val('');
        $("#txtSubTotal").val('');
		return false;
    }
	
	return true;
}

function loadCities(countryID){
    if(countryID==""){
        $("#selCity option[value='']").attr("selected",true);
    }
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
        serial_cou: countryID,
        all: 'YES'
    });
}

function loadInvoiceNumber(typeDbc){
    if($('#txtInvoiceNumber').attr('readonly')){
        $('#invoiceNumberContainer').load(document_root+"rpc/loadsInvoices/loadInvoiceNumber.rpc",{
            serial_man: $('#hdnSerialMan').val(),
            type_dbm: typeDbc
        });
    }
}

function addCustomerRules(){
    $('#txtCustomerDocument').rules("add", {
        required: true,
        alphaNumeric: true,
        messages:{
            alphaNumeric: "El campo 'Documento' solo acepta valores alfan&uacute;mericos."
        }
    });
    if($('#selType').val()){
        $('#selType').rules("add", {
            required: true
        });
    }
    $('#txtFirstName').rules("add", {
        required: true,
        invoice_holder: true,
        messages:{
            invoice_holder: "El campo 'Nombre' solo acepta valores alfan&uacute;mericos y el (.)"
        }
    });
    
    $('#txtPhone1').rules("add", {
        required: true,
        number: true,
        messages:{
            number: "El campo 'Tel&eacute;fono' solo acepta n&uacute;meros."
        }
    });
    $('#txtPhone2').rules("add", {
        number: true,
        messages:{
            number: "El campo 'Tel&eacute;fono Secundario' solo acepta n&uacute;meros."
        }
    });
    $('#selCountry').rules("add", {
        required: true
    });
    $('#selCity').rules("add", {
        required: true
    });
    if($('#selType').val()!='LEGAL_ENTITY'&&$('#hdnTypeDbc').val()!='LEGAL_ENTITY'){
        $('#txtLastName').rules("add", {
            required: {
				depends: function(element) {
					if($('#customerData').css('display') == 'none')
						return false;
					else
						return true;
				}
			},
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
    }
}

function removeCustomerRules(){
    $('#txtCustomerDocument').rules('remove');
    if($('#selType').val()){
        $('#selType').rules("remove");
    }
    $('#txtFirstName').rules('remove');
    $('#txtLastName').rules('remove');
    $('#txtPhone1').rules('remove');
    $('#txtPhone2').rules('remove');
    $('#selCountry').rules('remove');
    $('#selCity').rules('remove');
}

function checkType(){
    if($('#selType').val() == 'LEGAL_ENTITY' || $('#hdnTypeDbc').val() == 'LEGAL_ENTITY'){
        $('#lastNameContainer').hide();
        $('#txtLastName').rules('remove');
        $('#labelName').html('* Raz&oacute;n Social:');
    }else{
        $('#lastNameContainer').show();
        $('#txtLastName').rules("add", {
            required: {
				depends: function(element) {
					if($('#customerData').css('display') == 'none')
						return false;
					else
						return true;
				}
			},
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
		
        $('#labelName').html('* Nombre:');
    }
}

function checkDealerChangeDiscountToComission(type){
    if($('#hdnTypePercentage').val()=='DISCOUNT'){
        if(type=='CUSTOMER'){
            $('#hdnComissionValue').val($('#txtDiscount').val());
            $('#txtDiscount').val('0');
            calculateTotal();
        }else{
            if($('#hdnComissionValue').val()>0){
                $('#txtDiscount').val($('#hdnComissionValue').val());
                $('#hdnComissionValue').val('0');
                calculateTotal();
            }
        }
    }
}

function calculateTotal(){
    var subTotal,otherDiscount;
    var discount = parseFloat($("#txtDiscount").val());
    var managerRightsComission = parseFloat($("#hdnManagerRightsComission").val());
    var hasRightsComission = $("#hdnManagerRights").val();
    otherDiscount = parseFloat(jQuery.trim(($("#txtOtherDiscount").val())));	
	
    if(otherDiscount>0){
        subTotal=parseFloat($("#txtSalesTotal").val())-(parseFloat($("#txtSalesTotal").val())*((otherDiscount+parseFloat($("#txtDiscount").val()))/100));
        $("#txtSubTotal").val(roundNumber(subTotal, 2));
    }else{
        subTotal=parseFloat($("#txtSalesTotal").val())-(parseFloat($("#txtSalesTotal").val())*(parseFloat($("#txtDiscount").val())/100));
        $("#txtSubTotal").val(roundNumber(subTotal, 2));
    }
//    $("#txtTotal").val(roundNumber((subTotal*(1+(parseFloat($("#h_taxesToApply").val())/100))), 2));
    $("#txtTotal").val(roundNumber((subTotal), 2));
    if($("#txtOtherDiscount").val().length>4){
        if($("#txtOtherDiscount").val().indexOf('.')>2){
            alert('Porcentaje mayor a 100. Ingrese un valor menor por favor.');
            $("#txtTotal").val('');
            $("#txtSubTotal").val('');

        }
        $("#txtOtherDiscount").val(otherDiscount.toFixed(2));
    }
	
    if(hasRightsComission == 'YES'){
        if((discount + otherDiscount) > managerRightsComission){
            alert('Los porcentajes de descuentos superan el permitido para el comercializador. Por favor notifique al Administrador');
            $("#txtTotal").val('');
            $("#txtSubTotal").val('');
        }
    }
		
    validate_cost();
}

/*
 *@Name: loadCustomer
 *@Description: Verifies if the document entered is from a customer already registered and displays his/her information
 *@Params: customerID = Identification number of the customer
 **/
function loadCustomer(customerID){
    var docCust= customerID;
    show_ajax_loader();
    $.ajax({
        url: document_root+"rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({
            txtDocument : docCust
        }),
        success: function(customerExists) {
            $('#customerData').load(
                document_root+"rpc/loadsInvoices/loadCustomer.rpc",
                {
                    document_cus: docCust,
                    document_validation: function(){
                        return $('#hdnValidateHolderDocument').val();
                    }
                },function(){
                    $('#selCountry').bind('change',function(){
                        loadCities(this.value,'Customer','ALL');
                    });
                    $('#txtCustomerDocument').bind('blur',function(){
                        loadCustomer($(this).val());
                    });
                    $('#hdnCustomerAction').val($('#hdnCustomerActionRPC').val());
                    $('#hdnSerialCus').val(($('#hdnSerialCusRPC').val()));
                    $('#hdnTypeDbc').val($('#hdnTypeDbcRPC').val());

                    if(customerExists == 'true'){
                        var invoice_to=$("#selType").val();
                        $('#hdnTypeDbc').val(invoice_to);
                        $('#hdnTypeDbc2').val(invoice_to);
                        $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);
                        loadInvoiceNumber(invoice_to);
                        checkType();
                    }else{
                        $("#selType").bind("change", function(){
                            personTypeBindings();
                        });
                    }
					
                    $('[id^="rdDocumentType"]').each(function(){
                        $(this).bind('click', function(){
                            $('#selType').val($(this).attr('person_type'));
                            personTypeBindings();
                        });
                    });
					
                    hide_ajax_loader();
                }
                );
        }
    });
}

function evaluate_bill_to_radio(element){
    checkDealerChangeDiscountToComission(element.val());
	
    if(element.val()=='DEALER'){
        removeCustomerRules();
        checkType();
        loadInvoiceNumber('LEGAL_ENTITY');
    }else{
        addCustomerRules();
        checkType();
        if($('#hdnTypeDbc').val()){
            loadInvoiceNumber($('#hdnTypeDbc').val());
        }
    }
}

function personTypeBindings(){
    $('#hdnTypeDbc').val($("#selType").val());
    $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);

    if($("#selType").val()){
        loadInvoiceNumber($("#selType").val());
        checkType();
    }
}

function setAgreementDiscountToInvoice(element){
    var agreementDiscount = element.value;
    var agreementId  = $('#selAgreement').find(':selected').attr('data-id');
    $('#hdnAgreementId').val(agreementId);
    $('#txtOtherDiscount').val('');
    $('#txtOtherDiscount').val(agreementDiscount);
    calculateTotal();
}
