// JavaScript Document
var pager;
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmVoidInvoiceRequest").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                txtObservationsVoid: {
                    required: true
                }
            }
    });
    /*END VALIDATION SECTION*/
    if($("#selAll")){
            $("#selAll").bind("change",checkAll);
    }
    //Paginator
    if($('#salesTable').lenght>0){
        pager = new Pager('salesTable', 5 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }
}
);

//checkAll
//select\deselect all the sales
function checkAll() {
        var nodoCheck = document.getElementsByTagName("input");
        var varCheck = document.getElementById("selAll").checked;
        for (i=0; i<nodoCheck.length; i++){
                if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "selAll" && nodoCheck[i].disabled == false) {
                        nodoCheck[i].checked = varCheck;
                }
        }
}