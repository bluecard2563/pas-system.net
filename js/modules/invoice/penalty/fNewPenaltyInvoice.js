// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmPenaltyInvoice").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false
	});
	/*END VALIDATION SECTION*/

	$('#btnSearch').bind('click',function(){
		loadPenaltyForm();
	});
	$('#txtCardNumber').bind('keypress',function(){
		$('#invoiceInfo').html('');
		$('#confirmDiv').html('');
		$('#confirmDiv').removeClass('success');
		$('#confirmDiv').removeClass('error');
	});
});

function loadPenaltyForm(){
	$('#invoiceInfo').html("");
	var number_card=$('#txtCardNumber').val();

	if(number_card!=''){
		show_ajax_loader();
		$('#invoiceInfo').load(document_root+'rpc/loadsInvoices/penalty/loadPenaltyInvoiceInfo.rpc',
		{
			number_card: number_card
		}, function(){
			if($('#salesTable').length>0){
				$('#txtPenalty').rules('add',{
					required: true,
					number: true,
					lessThan: '#hdnTotal',
					min: 0,
					messages: {
						number: "El campo 'Penalidad' admite s&oacute;lo n&uacute;meros.",
						lessThan: 'La penalidad no puede ser mayor al valor de la(s) tarjeta(s) escogida(s).',
						min: "La penalidad debe ser mayor o igual que cero."
					}
				});
			}
			hide_ajax_loader();
		});
	}else{
		alert('Ingrese un n\u00FAmero de tarjeta.');
	}
}