// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchInvoiceLog").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
                        selZone: {
				required: true
			},
                        selCountry: {
				required: true
			},
                        selManager: {
				required: true
			},
                        selDocument: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/

        $('#selZone').change(function(){loadCountries(
			$('#selZone').val());
			loadManagersByCountry('');
			loadDocumentsByManager('');
			$('#invoiceLogContainer').html('');
		});
        $('#btnSearch').click(function(){
            if($('#frmSearchInvoiceLog').valid()){
               loadInvoiceLogs();
            }            
        });

});

function loadCountries(zoneId){
	$('#invoiceLogContainer').html('');
	$('#selCountry').rules("add", {
		 required: true
	});

	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
		loadDocumentsByManager('');
	}
	$('#countryContainer').load(document_root+"rpc/loadInvoiceLogCountries.rpc",{serial_zon: zoneId},function(){
		if($('#selCountry ').find('option').size()==2){
			loadManagersByCountry($('#selCountry').val());
		}
		$('#selCountry').change(function(){
			loadDocumentsByManager('');
			loadManagersByCountry($('#selCountry').val());
		});
	});
}
function loadManagersByCountry(countryID){
	$('#invoiceLogContainer').html('');
    $('#selManager').rules("add", {
             required: true
    });

    $('#managerContainer').load(document_root+"rpc/loadInvoiceLogManagersByCountry.rpc",{serial_cou: countryID},function(){
        $('#selManager').change(function(){
            loadDocumentsByManager($(this).val());
        });

		if($('#selManager ').find('option').size()==2){
			loadDocumentsByManager($('#selManager').val());
        }
    });
    

}
function loadDocumentsByManager(managerID){
	$('#invoiceLogContainer').html('');
    $('#selDocument').rules("add", {
             required: true
    });
	
    $('#documentContainer').load(document_root+"rpc/loadDocumentsByManager.rpc",{serial_mbc: managerID},function(){
		$('#selDocument').bind('change',function(){
			$('#invoiceLogContainer').html('');
		});
	});
}

function loadInvoiceLogs(){
    $('#invoiceLogContainer').load(document_root+"rpc/loadsInvoices/loadInvoiceLogs.rpc",{
		serial_mbc: $('#selManager').val(),
		type_dbc: $('#selDocument').val()
	});
}