// JavaScript Document

$(document).ready(function(){
	$("#frmTranslateQuestion").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtText: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/question/checkQuestionByLanguage.rpc",
					type: "post",
					data: {
					  serial_que: function() {
						return $("#hdnQuestionID").val();
					  }
					  code_lang: function() {
						return $("#selLanguage").val();
					  }
					}
				}
			},
			selLanguage: {
				required: true
			}			
		},
		messages: {
			txtText: {
				remote: 'La Pregunta ya existe en el sistema'
			}
		}

	});
});// JavaScript Document