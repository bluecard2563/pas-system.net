// JavaScript Document

$(document).ready(function(){
	$("#frmUpdateQuestion").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selType: {
				required: true
			},
			selStatus: {
				required: true
			}				
		},
		messages: {
		}/*
		submitHandler: function(form) {
			if(confirm("Los cambios efectuados no se guardar\u00E1n en los otros idiomas, �Est\u00E1 seguro que desea actualizar la pregunta?")){
				form.submit();
			}
		}*/

	});
	$("#newInfo").css("display","none");
	$("#frmNewQuestionTranslation").validate({
		errorLabelContainer: "#alerts_dialog",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtText: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/question/checkQuestion.rpc",
					type: "post",
					data: {
					  serial_que: function() {
						$('#serial_qbl').val();
					  }
					}
				}
			}
		},
		messages:{
			txtText: {
				remote: "La pregunta ingresada ya consta para este idioma."
			}
		}
	});
	$("#btnOk").click(function(){
		$('#newInfo').hide();		  
	});
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 410,
		modal: true,
		buttons: {
			'Guardar': function() {
				  $("#frmUpdateQuestionTranslationDialog").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtTextQuestion: {
								required: true,
								remote: {
									url:document_root+"rpc/remoteValidation/question/checkQuestion.rpc",
									type: "post",
									data: {
									  serial_que: function() {
										return $('#serial_qbl').val();
									  }
									}
								}
							}
						},
						messages:{
							txtTextQuestion: {
								remote: "La pregunta ingresada ya consta para este idioma."
							}
						}
					});
				  
				   if($('#frmUpdateQuestionTranslationDialog').valid()){
						$('#frmUpdateQuestionTranslationDialog').submit();  
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});
	
});

function loadFormData(itemID){
	$('#serial_qbl').val(itemID);
	$('#txtTextQuestion').val($('#text_'+itemID).html());	
}

// JavaScript Document