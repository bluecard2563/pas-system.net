// JavaScript Document
$(document).ready(function(){					   
	$("#selLanguage").change (function(){						   
		$("#txtText").val('');
                loadQuestions($(this).val());
	});	
	/*AUTOCOMPLETER*/
	$("#txtText").autocomplete(document_root+"rpc/autocompleters/loadQuestions.rpc",{
		extraParams: {
			serial_lang: function(){ return $("#selLanguage").val()}
			//
        },
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnQuestionID").val(row[1]);
	});
	/*END AUTOCOMPLETER*/
	
	/*VALIDATION SECTION*/
	$("#frmSearchQuestion").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtText: {
				required: true
			},	
			selLanguage: {
				required: true
			}	
		}
	});
	/*END VALIDATION*/

        //Paginator
            pager = new Pager('questionsTable', 5 , 'Siguiente', 'Anterior');
            pager.init(30); //Max Pages
            pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
            pager.showPage(1); //Starting page
});// JavaScript Document

function loadQuestions(languageID){
    //get sales status registered and serial_inv NULL
         $('#questionsContainer').load(document_root+"rpc/loadQuestionsByLanguage.rpc",{serial_lang: languageID},
        function(){
            //Paginator
            pager = new Pager('questionsTable', 5 , 'Siguiente', 'Anterior');
            pager.init(30); //Max Pages
            pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
            pager.showPage(1); //Starting page

        });

}