// JavaScript Document
$(document).ready(function(){
	$("#frmNewQuestion").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtText: {
				required: true,
				remote: {
					url:document_root+"rpc/remoteValidation/question/checkQuestion.rpc",
					type: "post"
				}
			},
			selType: {
				required: true
			}					
		},
		messages: {
			txtText: {
				remote: 'La pregunta ya existe en el sistema'
			}
		}
	});
});