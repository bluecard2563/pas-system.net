// JavaScript Document
var pager;
var config = {
	toolbar:
	[
		['Source','Bold', 'Italic', 'Underline', '-', 'BulletedList', '-','Cut','Copy','Paste','-','Find','Replace','SelectAll','common'],
		['UIColor']
	]
};

$(document).ready(function(){
	$('#arDescription').ckeditor(config);
	setDefaultCalendar($("#txtPublishDate"),'-0d','+1y','+0d');
	setDefaultCalendar($("#txtExpirationDate"),'-0d','+1y','+0d');
	
	/*VALIDATION SECTION*/
	$("#frmNewPopUp").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                txtTitle: {
                    required: true
                },
                txtArt: {
                    required: true
                },
				txtPublishDate: {
					required: true
				},
				txtExpirationDate: {
					required: true
				},
				txtLink: {
					url: true
				},
				hdnScopeSelected: {
					required: true
				}
            },
			messages: {
				txtLink: {
					url: "El formato del Link Web es incorrecto."
				},
				hdnScopeSelected: {
					required: "Seleccione al menos un Representante/Responsable"
				}
			}
    });
	
	$('[id^="rdScope_"]').each(function(){
		$(this).bind('click', function(){
			processScope($(this).val())
		});
	});
	
	$('[id^="chManager_"]').each(function(){
		$(this).bind('click', function(){
			oneScopeAtLeast();
		});
	});
	
	$('[id^="chResponsible_"]').each(function(){
		$(this).bind('click', function(){
			oneScopeAtLeast();
		});
	});
	
	$('#btnSubmit').bind('click', function(){
		if($("#frmNewPopUp").valid() && validateDates()){
			$("#frmNewPopUp").submit();
		}
	});
	
	if ($('#hdnID').val()){
		oneScopeAtLeast();
	}
});

function oneScopeAtLeast(){
	var oneResponsible = false;
	var oneManager = false;
	
	$('[id^="chManager_"]').each(function(){
		if($(this).is(":checked")){
			oneManager = true;
			return;
		}
	});
	
	$('[id^="chResponsible_"]').each(function(){
		if($(this).is(":checked")){
			oneResponsible = true;
			return;
		}
	});
	
	if($('#rdScope_manager').is(':checked') == true){
		if(oneManager){
			$('#hdnScopeSelected').val('yes');
		}else{
			$('#hdnScopeSelected').val('');
		}
	}else{
		if(oneResponsible){
			$('#hdnScopeSelected').val('yes');
		}else{
			$('#hdnScopeSelected').val('');
		}
	}
}

function processScope(scope){
	$('#hdnScopeSelected').val('');
	
	if(scope == 'MANAGER'){
		$('#scopeManager').show();
		$('#scopeResponsible').hide();
		
		$('[id^="chResponsible_"]').each(function(){
			$(this).attr('checked',false);
			
		});
	}else{
		$('#scopeManager').hide();
		$('#scopeResponsible').show();
		
		$('[id^="chManager_"]').each(function(){
			$(this).attr('checked',false);
		});
	}
}

function validateDates(){
		var days=dayDiff($("#txtPublishDate").val(),$("#txtExpirationDate").val());
		var date = new Date();
		var today = (date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
		if(days<0){
			alert("La Fecha de Expiraci\u00F3n no puede ser menor a la Fecha de Publicaci\u00F3n");
			return false;
		}
		if(days>365){
			alert("El rango de fechas no debe ser mayor a un a\u00F1o.");
			return false;
		}
		
		return true;
}
