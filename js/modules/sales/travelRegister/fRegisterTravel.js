var pager;
$(document).ready(function(){

console.log("Third Party Register Pro: " + $('#isThirdPartyRegisterPro').val());


	$("#frmRegisterTravel").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
            selCustomerDocumentTypeTravelRecord: {
                required: true
            },
			txtDocumentCustomer: {
				required: true,
				alphaNumeric: true
			},
			selType: {
				required: true
			},
			txtNameCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			txtNameCustomer1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			txtLastnameCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
            rdGender: {
                required: true
            },
			selCountry: {
				required: true
			},
            selCityCustomer: {
				required: true
			},
			selCity: {
				required: true
			},
			txtBirthdayCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				date: true
			},
			txtAddress: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtAddress1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhone1Customer: {
				required: true,
				digits: true
			},
			txtPhone2Customer: {
				digits: true
			},
			txtCellphoneCustomer: {
				digits: true
			},
			txtMailCustomer: {
				//required: true,
				email: true,
				/* remote: {
					url: document_root+"rpc/remoteValidation/customer/checkCustomerEmail.rpc",
					type: "POST",
					data: {
						serialCus: function() {
							return $("#serialCus").val();
						},
						txtMail: function(){
							return $('#txtMailCustomer').val();
						}
					}
				} */
			},
			txtRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
			txtManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
			txtDepartureDate: {
				required: true,
				date: true
			},
			txtArrivalDate: {
				required: true,
				date: true
			},
			selCountryDestination: {
				required: true
			},
			selCityDestination: {
				required: true
			},
			all_answered: {
				required: {
					depends: function(element) {
						if($('#hdnQuestionsExists').val()=='1' && $('#should_check_questions').val()=='1')
							return true;
						else
							return false;
					}
				}
			}
		},
		messages: {
			txtNameCustomer: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
			},
			txtNameCustomer1: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
			},
			txtLastnameCustomer: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
			},
			txtDocumentCustomer: {
				remote: "La identificaci&oacute;n ingresada ya existe.",
				alphaNumeric: "S&oacute;lo se aceptan caracteres alpha - num&eacute;ricos en el campo 'Identificaci&oacute;n'"
			},
			txtMailCustomer: {
				email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe en el sistema."
			},
			txtBirthdayCustomer: {
				date:"El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
			},
			txtDepartureDate: {
				date:"El campo 'Fecha de Salida' debe tener el formato dd/mm/YYYY",
				lessOrEqualTo: 'La Fecha de Salida debe ser menor o igual que la Fecha de Llegada'
			},
			txtArrivalDate: {
				date:"El campo 'Fecha de Llegada' debe tener el formato dd/mm/YYYY",
				greaterOrEqualTo: 'La Fecha de Llegada debe ser mayor o igual que la Fecha de Salida'
			},
			txtPhone1Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'"
			},
			txtPhone2Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono 2'"
			},
			txtCellphoneCustomer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Celular'"
			},
			txtRelative: {
				textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneRelative: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Familiar'"
			},
			txtManager: {
				textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneManager: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Gerente'"
			},
			all_answered: {
				required:"Debe contestar todas la preguntas de adulto mayor para poder continuar'"
			}
		},
		submitHandler: function(form) {
			var serial_cus;
			if($('#serialCus').length > 0){
				serial_cus = $('#serialCus').val();
			}else if($('#serial_cus').length > 0){
				serial_cus = $('#serial_cus').val();
			}else{
				serial_cus = '';
			}


			//Health Statement: Validate if customer has a statement complete before submit.
			//if (!statementSubmitValidation()) {
			//	return false;
			//}



			if(serial_cus != ''){
				$.ajax({
					url: document_root+"rpc/remoteValidation/sales/travelRegister/checkCrossingTravels.rpc",
					type: "post",
					data: ({
						serial_cus : serial_cus,
						start_dt:$('#txtDepartureDate').val(),
						end_dt:$('#txtArrivalDate').val()
					}),
					success: function(data) {
console.log(data);
						if(data=='true'){
							$('#btnRegisterTravel').attr('disabled', 'true');
							$('#btnRegisterTravel').val('Espere por favor...');
							form.submit();
						}
						else
							alert('El cliente ya tiene un viaje registrado entre las fechas dadas.');
					}
				});
			}else{
				$('#btnRegisterTravel').attr('disabled', 'true');
				$('#btnRegisterTravel').val('Espere por favor...');
				form.submit();
			}
		}
	});
	
	/*CALENDARS*/
	setDefaultCalendar($("#txtBirthdayCustomer"),'-100y','+0d','-20y');
	setDefaultCalendar($("#txtDepartureDate"),'+0d','+100y');
	setDefaultCalendar($("#txtArrivalDate"),'+0d','+100y');
	/*END CALENDARS*/
	
	if($('#oldRegistersTable').length>0){
		pager = new Pager('oldRegistersTable', 10 , 'Siguiente', 'Anterior');
		pager.init($('#total_travels').val()); //Max Pages
		pager.showPageNav('pager', 'travelPageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}

	$('#txtDocumentCustomer').bind('blur',function(e){
		//loadCustomer($(this).val());
        customerDocumentValidation($(this).val());
	});
	$('#txtBirthdayCustomer').bind('change',function(){
		evaluateElderQuestions();
	});
	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
		loadCities(this.value,'Customer','ALL');
	});
	$("#selCountryDestination").bind("change", function(e){
		loadCities(this.value,'Destination','ALL');
		validateDestination($(this).val());
	});
	$("#txtArrivalDate").bind("change", function(e){
		evaluateDates();
	});
	$("#txtDepartureDate").bind("change", function(e){
		evaluateDates();
		evaluateElderQuestions();
	});

	/*Load cities if destination restricted*/
	if($('#selCountryDestination option:selected').val()!='') {
		loadCities($('#selCountryDestination option:selected').val(),'Destination','ALL');
		validateDestination($('#selCountryDestination option:selected').val());
	}
	/*End load cities if destination restricted*/

    //Customer Document Validation:
	// Numbers or letters customer validation.
    $("#customerContainer").bind("change", "selCustomerDocumentTypeTravelRecord", function(e) {
        addDocumentValidationClass();
    });

    //Store in hidden field travel record system origin
	$('#hdnTravelStatementOrigin').val(travelRecordStatementOrigin);

});

/*
 *@Name: loadCustomer
 *@Description: Verifies if the document entered is from a customer already registered and displays his/her information
 *@Params: customerID = Identification number of the customer
 **/
function loadCustomer(customerID){
    var docCust= customerID;
	var cardNumber = $("#hdnCardNumber").val();
    var documentType = $("#selCustomerDocumentTypeTravelRecord").val();
    var systemOrigin = travelRecordStatementOrigin;

	show_ajax_loader();
	clearQuestionsDiv();
	$.ajax({
		url: document_root+"rpc/remoteValidation/customer/checkCustomerDocument.rpc",
		type: "post",
		data: ({
			txtDocument : docCust
		}),
		success: function(data) {			
			$.ajax({
				url: document_root+"rpc/remoteValidation/customer/checkCustomerType.rpc",
				type: "post",
				data: ({
					txtDocument : docCust
				}),
				success: function(customerType) {
					if(customerType != 'PERSON' && customerType != 'NOCUSTOMER'){
						alert('El cliente seleccionado no es una persona natural y por lo tanto no puede constar como viajero.');
						docCust = '';
					}

					$('#customerContainer').load(document_root+"rpc/loadsSales/loadCustomer.rpc", {
						document_cus: docCust,
						card_number: cardNumber,
						document_type: documentType,
						system_origin: systemOrigin
					},
						function(){
						hide_ajax_loader();
						setDefaultCalendar($("#txtBirthdayCustomer"),'-100y','+0d','-20y');
						$('#txtBirthdayCustomer').rules('add',{
							required: {
								depends: function(element) {
									var display = $("#divName").css('display');
									if(display=='none')
										return false;
									else
										return true;
								}
							},
							date: true
						});

						$('#txtBirthdayCustomer').bind('change',function(){
							evaluateElderQuestions();
						});
						
						$('#selCountry').bind('change',function(){
							loadCities(this.value,'Customer','ALL');
						});

						$('#txtNameCustomer').removeAttr('readonly');
						$('#txtLastnameCustomer').removeAttr('readonly');
						
						if(docCust != ''){
							evaluateElderQuestions();
						}

						//Customer Document Validation:
						//Set only number or letters function and execute validation function on document number change.
                        addDocumentValidationClass();
                        $('#txtDocumentCustomer').bind('blur',function(){
                            //loadCustomer($(this).val());
                            if ($('#txtDocumentCustomer').val()) {
                                customerDocumentValidation($(this).val());
                            }

                        });

                        $('#selCustomerDocumentTypeTravelRecord').unbind('change').change(function () {
                            var documentTypeChanged = $("#selCustomerDocumentTypeTravelRecord").val();
                            clearCustomerContainer(documentTypeChanged);
                        });

						// Health Statement
						checkCustomerHasStatement();

					});
				}
			});
		}
	});
}

function loadCities(countryID,selectID, ALL){
	var container
	if(selectID == "Destination") {
		container = $('#cityDestinationContainer');
	}else{
		container = $('#cityContainerCustomer');
	}
	container.load(
		document_root+"rpc/loadCities.rpc",
		{
			serial_cou: countryID,
			serial_sel: selectID,
			all: ALL
		},function(){
			if(selectID == "Destination") {
				$("#selCityDestination option[name='General']").attr("selected", true);
			}
		});
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var expirationDate =new Date(Date.parse(dateFormat($('#end_dt').html())));
	var startDate =new Date(Date.parse(dateFormat($('#start_dt').html())));
	var today = new Date(Date.parse(dateFormat($('#today').val())));
	if($("#txtArrivalDate").val()!=''){
		var arrivalDate =new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
		if(arrivalDate>expirationDate){
			alert("La Fecha de Llegada no puede superar la fecha de expiraci\u00F3n de la tarjeta.");
			$("#txtArrivalDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}

	}
	if($("#txtDepartureDate").val()!=''){
		var departureDate =new Date(Date.parse(dateFormat($('#txtDepartureDate').val())));
		if(departureDate>expirationDate){
			alert("La Fecha de Partida no puede superar la fecha de expiraci\u00F3n de la tarjeta.");
			$("#txtArrivalDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}else{
			if(departureDate<startDate){
				alert("La Fecha de Partida no puede ser inferior a la fecha de inicio de cobertura de la tarjeta.");
				$("#txtDepartureDate").val('');
				$("#txtDaysBetween").html('');
				$("#hddDays").val('');
			}else{
				if(departureDate<today){
					alert('La Fecha de partida no puede ser menor al d\u00EDa de hoy.');
					$("#txtDepartureDate").val('');
					$("#txtDaysBetween").html('');
					$("#hddDays").val('');
				}
			}
		}
	}
	if($("#txtArrivalDate").val()!='' && $("#txtDepartureDate").val()!=''){
		if(departureDate>arrivalDate){
			alert("La Fecha de Partida no puede superar la fecha de Retorno.");
			$("#txtDepartureDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}else{
			if(parseInt(dayDiff($("#txtDepartureDate").val(),$("#txtArrivalDate").val()))>parseInt($('#days_available').html())){
				alert("Los d\u00EDas de viaje no pueden superar el n\u00FAmero de d\u00EDas disponibles.");
				$("#txtArrivalDate").val('');
				$("#txtDaysBetween").html('');
				$("#hddDays").val('');
			}else{
				updateTravelDays();
			}
		}
	}else{
		$("#hddDays").val('');
	}
}

function updateTravelDays(){
	var days=dayDiff($("#txtDepartureDate").val(),$("#txtArrivalDate").val());
	$('#txtDaysBetween').html(days);
	$("#hddDays").val(days);
}

function evaluateElderQuestions(){
	var departureDate = new Date(Date.parse(dateFormat($('#txtDepartureDate').val())));
	departureDate.setFullYear(departureDate.getFullYear() - $('#hdnParameterValue').val());
	if($('#txtBirthdayCustomer').length > 0)
	var birthDate =new Date(Date.parse(dateFormat($('#txtBirthdayCustomer').val())));
	
	if((departureDate - birthDate) >= 0){
		if($("#hdnSeniorPro").val()=='YES'){
			$('#customerQuestionsContainer').load(document_root+"rpc/loadsSales/loadElderQuestionsCustomer.rpc",{
				customer: $('#serialCus').val(),
				editionAllowed: 'YES'},function(){
					$(":select[id^='cust_answer']").each(function () {
						$(this).rules("add", {
							 required: true
						});
					});
					
					$('#has_questions').val('YES');
				}
			);
		}else{
			alert("Este producto no acepta Adultos Mayores.");
			loadCustomer('');
		}
	}else{
		clearQuestionsDiv();
	}
}

function clearQuestionsDiv(){
	$('#has_questions').val('NO');
	$('#customerQuestionsContainer').html('');
}

function validateDestination(serial_cou){
	var serial_pxc = $("#hdnSerial_pxc").val();
	
	if(serial_pxc && serial_cou){
		$.ajax({
			url: document_root+"rpc/remoteValidation/sales/checkDestination.rpc",
			type: "post",
			data: ({serial_pxc : serial_pxc, serial_cou: serial_cou}),
			dataType: "json",
			success: function(valid_destination) {
				if(!valid_destination){
					$('#selCountryDestination').val('');
					$('#selCityDestination').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					alert('El producto seleccionado no puede utilizarse para el destino actual.');
				}
			}
		});
	}	
}

function customerDocumentValidation(customerDocument) {
    var data;
    var customerDocument = customerDocument.trim();
    var documentType = $("#selCustomerDocumentTypeTravelRecord").val();

    if (documentType && customerDocument) {
        if (documentType === "c") {
            //Validate Cedula number
            var action = "validateCustomerCardId";
        }
        else {
            var action = "validateCustomerPassport";
        }

        show_ajax_loader();
        $.ajax({
            //async: false,
            url: document_root + "rpc/remoteValidation/customer/validateIdentification.rpc",
            type: "POST",
            data: {action: action, documentType: documentType,  customerDocument: customerDocument},
            success: function (response) {
                //hide_ajax_loader();
                data = JSON.parse(response);
                if (data) {
                    validIdentification(data);
                }
            },
        });

    }
}

function validIdentification(data){
    var customerDocument = $('#txtDocumentCustomer').val();
    //If the customer identification is invalid alert error.
    if (!data.success) {
        alert(data.message);
        hide_ajax_loader();
        clearCustomerContainer();
    }
    //else if the customer identification is valid execute origin function.
    else {
        loadCustomer(customerDocument);
    }

}

function clearCustomerContainer(documentTypeChanged) {
	$('#customerContainer :input').val('');
    $("#customerContainer :radio").attr("checked", false);

	var customerTypeDropdown = $("<select></select>").attr("id", "selType").attr("name", "selType").attr("title", 'El campo "Tipo de Cliente" es obligatorio.');
    customerTypeDropdown.append($("<option></option>").attr("value", "").text("- Seleccione -"));
    customerTypeDropdown.append($("<option></option>").attr("value", "PERSON").text("Persona Natural"));
	$("#customerType").html(customerTypeDropdown);

	//After bringing customer data when the customer exists. If the user changes the document type, the information on the screen is deleted.
	if (documentTypeChanged) {
        var documentTypeDropdown = $("<select></select>").attr("id", "selCustomerDocumentTypeTravelRecord").attr("name", "selCustomerDocumentTypeTravelRecord").attr("title", 'El campo "Tipo de Documento" es obligatorio.');
        documentTypeDropdown.append($("<option></option>").attr("value", "").text("- Seleccione -"));

		if (documentTypeChanged === 'c') {
            documentTypeDropdown.append($("<option></option>").attr("value", "c").attr("selected", "selected").text("Cédula"));
			documentTypeDropdown.append($("<option></option>").attr("value", "p").text("Pasaporte"));
		} else {
            documentTypeDropdown.append($("<option></option>").attr("value", "c").text("Cédula"));
            documentTypeDropdown.append($("<option></option>").attr("value", "p").attr("selected", "selected").text("Pasaporte"));
		}
        $("#customerDocumentTypeContainer").html(documentTypeDropdown);
	}

}

function addDocumentValidationClass() {
    //Based on document type allow only numbers or letters/numbers.
    var customerDocument = $("#txtDocumentCustomer");
    var documentType = $("#selCustomerDocumentTypeTravelRecord").val();

    $('.number-customer').unbind("keypress");
    $('.alphanumeric-customer').unbind("keypress");

    if (documentType === 'c') {
        $("#txtDocumentCustomer").attr("class", "number-customer");
        numbersOnly(customerDocument);

    } else {
        $("#txtDocumentCustomer").attr("class", "alphanumeric-customer");
        alphanumericOnly(customerDocument);
    }
}

function numbersOnly(htmlElement) {
    htmlElement.bind("keypress", function(e) {
        var regex = new RegExp("^[0-9\b]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });
}

function alphanumericOnly(htmlElement) {
    htmlElement.bind("keypress", function(e) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });
}

// Health Statement:
// if the customer already have a statement, the button is not shown else the button will shown.
// We need to fill in the customer email and select the person type need to be PERSON to show the statement button.
function checkCustomerHasStatement() {
	if ($("#txtDocumentCustomer").val() != "") {
		if ($('#hdnCustomerTravelHasStatement').val()) {
			//Hide button because customer has a statement complete
			$('#healthStatementContainer').hide();
		}
		else {
			$('#healthStatementContainer').show();
		}
	}
}

//Health Statement: This process checks if a customer has complete the statement before submit.
function statementSubmitValidation() {
	var selCustomerDocumentTypeTravelRecord = $("#selCustomerDocumentTypeTravelRecord").val();
	// Only customer id and passport will alert the user that the statement must be completed.
	if (selCustomerDocumentTypeTravelRecord === 'c' || selCustomerDocumentTypeTravelRecord === 'p') {
		if (!$("#hdnCustomerTravelHasStatement").val() || $("#hdnCustomerTravelHasStatement").val() == "false") {
			alert("Por favor complete el formulario de declaración de salud del cliente y asegúrese de realizar el enlace de declaración para continuar.");
			return false;
		} else {
			return true;
		}
	}
}

//Health Statement: This function allows to execute the process of opening the angular link to fill out the health declaration.
function openHealthStatement() {
	var cardNumber = $("#hdnCardNumber").val();
	var basSessionUser = $("#hdnSessionUser").val();
	var customerDocument = $("#txtDocumentCustomer").val();
	var customerName = $("#txtNameCustomer").val();
	var customerLastName = $("#txtLastnameCustomer").val();
	var customerEmail = $("#txtMailCustomer").val();
	var customerFullName = (customerName + " " + customerLastName).toUpperCase();
	var isExtraSent = false;

	if (customerName === "" || customerLastName === "") {
		alert("Se requiere el nombre y apellido del cliente para completar el formulario de declaración de salud.");
		return;
	}

	if ($("#txtMailCustomer").val() == "") {
		alert("Se requiere el email del cliente para completar el formulario de declaración de salud.");
		return;
	}

	//Object to send to Angular Statement.
	var data = {
		sessionUser: basSessionUser,
		cardNumber: cardNumber,
		customerDocument: customerDocument,
		customerEmail: customerEmail,
		customerName: customerFullName,
		isExtra: isExtraSent,
		systemOrigin: travelRecordStatementOrigin
	};

	var dataString = JSON.stringify(data);
	//console.log(dataString);
	//Base64 data encoded using local function called utoa().
	var dataEncoded = utoa(dataString);
	//console.log(dataEncoded);

	var dataDecoded = atou(dataEncoded);
	//console.log(dataDecoded);

	//var url = 'http://localhost:4200/survey?data=' + customerDocument;
	//var url = 'http://localhost:4200/statement?data=' + dataEncoded;
	//var url = 'http://23.239.12.79:3030/statement?data=' + dataEncoded;
	var url = statementBaseUrl + dataEncoded;
	window.open(url, '_blank');

}

function checkStatementRefreshButton() {
	var cardNumber = $("#hdnCardNumber").val();
	var customerDocument = $("#txtDocumentCustomer").val();
	var systemOrigin = travelRecordStatementOrigin;

	if (customerDocument && cardNumber) {
		show_ajax_loader();
		$.ajax({
			url: document_root + "rpc/remoteValidation/statements/statement.rpc",
			type: "POST",
			data: {action: 'customerHasStatement', customerDocument: customerDocument, cardNumber: cardNumber, systemOrigin: systemOrigin},
			//dataType: 'json',
			success: function (response) {
				var data = JSON.parse(response);
				console.log(data);
				$('#hdnCustomerTravelHasStatement').val(data.exist);
				if (data.exist) {
					$('#healthStatementContainer').hide();
				}
				else {
					alert("El cliente no dispone de declaración de salud. Por favor complete la declaración de salud para proceder con el enlace.");
					$('#healthStatementContainer').show();
				}
				hide_ajax_loader();
			}
		});
	}

}

// ucs-2 string to base64 encoded ascii
// https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/btoa
function utoa(str) {
	return btoa(unescape(encodeURIComponent(str)));
}

// base64 encoded ascii to ucs-2 string
function atou(str) {
	return decodeURIComponent(escape(window.atob(str)));
}
