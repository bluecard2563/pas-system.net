/*
File:fChooseCard.js
Author: Nicolas Flores
Creation Date:26/03/2010
Modified By:Nicolas Flores (paginate)
Last Modified:19/04/2010
*/
var pager;
//get the  cards where client could register travels, by client
function searchRegisterCards(){
	serial_cus=$('#hdnSerial_cus').val();
	front_type=$('#frontType').val();
	$('#cardsContainer').load(document_root+"rpc/loadTravelRegister/loadRegisterCardByCustomer.rpc",{
		serial_cus: serial_cus, 
		front_type: front_type
	},
	function(){
		//Paginator
		if($('#travelsTable').length>0){
			pager = new Pager('travelsTable', 10 , 'Siguiente', 'Anterior');
			pager.init(7); //Max Pages
			pager.showPageNav('pager', 'travelPageNavPosition'); //Div where the navigation buttons will be placed.
			pager.showPage(1); //Starting page
		}

	});
}

//get the  the description of the card given in txtCardNumber
function searchCardDescription(){
		show_ajax_loader();
                card_number=$('#txtCardNumber').val();
		front_type=$('#frontType').val();
		$('#cardsContainer').load(document_root+"rpc/loadTravelRegister/loadCardDescription.rpc",{
		card_number: card_number,front_type:front_type
                
	}, function(){
            hide_ajax_loader();
        });
        
}

$(document).ready(function(){
	/*AUTOCOMPLETER*/
	$("#txtCustomerName").autocomplete(document_root+"rpc/autocompleters/loadRegisterCustomers.rpc",{
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
		loadingClass: 'process_loading',
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnSerial_cus").val('');
			$("#txtCustomerName").val('');
		}else{
			$("#hdnSerial_cus").val(row[1]);
		}

	});
	/*END AUTOCOMPLETER*/
	//clear the card number textfield in case the autocompleter is used
	$("#txtCustomerName").bind("keypress", function(e){
		$("#txtCardNumber").val('');
		$("#hdnSerial_cus").val('');
		$("#cardsContainer").html('');
	});
	//clear the txtCustomerName textfield in case a card number is typed
	$("#txtCardNumber").bind("keypress", function(e){
		$("#txtCustomerName").val('');
		$("#hdnSerial_cus").val('');
		$("#cardsContainer").html('');
	});
	//validation
	$("#frmTravelRegister").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCardNumber:{
				required: {
					depends: function(element) {
						var display = $("#executiveContainer").css('display');
						if(display=='none' || $("#hdnSerial_cus").val()!='')
							return false;
						else
							return true;
					}
				},
				digits:true,
				remote: {
					url: document_root+"rpc/remoteValidation/sales/travelRegister/checkRegisterCard.rpc",
					type: "post"
				}
			}
		},
		messages: {
			txtCardNumber:{
				required:"Ingrese un n&uacute;mero de tarjeta o seleccione un cliente.",
				digits:"El campo 'N&uacute;mero de Tarjeta' acepta solo d&iacute;gitos.",
				remote: "El n&uacute;mero de tarjeta ingresado no pertenece a una Ejecutiva."
			}
		},
		submitHandler: function(form) {
			//TODO funcion para cargar las tarjetas del tipo o la unica tarjeta.
			if($("#hdnSerial_cus").val()){
				searchRegisterCards();
			}
			else{
				searchCardDescription();
			}
			
		}
	});
});