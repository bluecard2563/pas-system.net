// JavaScript Document
/*
File:fSearchFreeSales.js
Author: Mario López
Creation Date:31/03/2010
Modified By:
Last Modified:
*/
$(document).ready(function(){
    $("#selCountry").bind("change", function(){
        loadCitiesByCountry($(this).val());
		$('#resultSet').html('');
    });
	loadCitiesByCountry($('#selCountry').val());
});

/*EVALUATE IF THE USER HAS SELECTED
 *THE REQUIRED FIELDS TO SEARCH*/
function evaluateFilters(){
    var next=true;
    $("#alerts").hide();
    $("#alerts").html('');
    $('#resultSet').html('');
    if(!$("#selCountry").val()){
        $("#alerts").append("<li><label class='error_validation'>Seleccione un Pa&iacute;s.</label></li>");
        $("#alerts").css('display','block');
        next = false;
    }
    if(next){
        loadFreeSales();
    }
}

/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#cityContainer').load(document_root+"rpc/loadFreeSales/loadCitiesWithFreeSales.rpc", {serial_cou: countryId}, function(){
            $('#selCity').bind('change', function(){
               loadResponsablesByCity($(this).val());
			   $('#resultSet').html('');
            });
            loadResponsablesByCity($("#selCity").val());//
        });
    }else{
        $('#cityContainer').html('Seleccione un pa\u00EDs');
        loadResponsablesByCity('');
    }
}

/*CALL TO THE RPC TO LOAD THE RESPONSABLES
 *BASED ON THE CITY SELECTED*/
function loadResponsablesByCity(cityId){
    if(cityId){//IF A CITY HAS BEEN SELECTED
        $('#responsableContainer').load(document_root+"rpc/loadFreeSales/loadResponsiblesByCityWithFreeSales.rpc",{serial_cit: cityId},function (){
            $("#selResponsable").bind("change", function(){
                var serial_usr=($("#selResponsable").val()=='')?false:$("#selResponsable").val();
                loadDealers(serial_usr);
				$('#resultSet').html('');
            });
			loadDealers($("#selResponsable").val());
        });
    }else{
       $('#responsableContainer').html('Seleccione una ciudad');
    }
    $('#dealerContainer').html('');
}

/*CALL TO THE RPC TO LOAD THE DEALERS
 *BASED ON THE RESPONSABLE
 *AND THE CATEGORY (OPTIONAL)*/
function loadDealers(serial_usr){
	var serial_cit=$('#selCity').val();
    if(serial_usr){
        $('#dealerContainer').load(document_root+"rpc/loadFreeSales/loadDealersWithFreeSales.rpc",{serial_usr: serial_usr, serial_cit:serial_cit},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
				$('#resultSet').html('');
            });
            loadBranches($("#selDealer").val());
        });
    }else{
       $('#dealerContainer').html('');
       loadBranches('');
    }
}

/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#branchContainer").load(document_root+"rpc/loadFreeSales/loadBranchesWithFreeSales.rpc",{dea_serial_dea: dealerID},function (){
			if($('#selBranch').length>0){
				$('#selBranch').bind('change',function(){
					$('#resultSet').html('');
				});
			}
		});
    }else{
        $("#branchContainer").html('Seleccione un Dealer');
    }
}

/*CALL TO THE RPC TO LOAD THE SALES*/
function loadFreeSales(){
    if($('#selCountry').val()){
        var city = '', user = '', dealer = '', branch = '';
        $("#messageContainer").hide();
        if($('#selCity').val()){
            city = $('#selCity').val();
        }
        if($('#selResponsable').val()){
            user = $('#selResponsable').val();
        }
        if($('#selDealer').val()){
            dealer = $('#selDealer').val();
        }
        if($('#selBranch').val()){
            branch = $('#selBranch').val();
        }

		if(branch!=''){
			//get free sales
			$('#resultSet').load(document_root+"rpc/authorizeFreeSales.rpc",{serial_cou : $('#selCountry').val(),serial_cit : city,serial_usr : user,serial_dea : dealer,dea_serial_dea : branch}, function(){
					$("#frmAuthorizeFree").validate({
						errorLabelContainer: "#alerts",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
								selAction:{
									required: true
								},
								hdnOneSelection: {
									required: true
								},
								txtObservations: {
									required: true,
									minlength : 15
								}
						},
						messages: {
								hdnOneSelection:{
								  required:'Seleccione al menos una solicitud de aprobaci&oacute;n.'
								}
						},
						submitHandler: function(form) {
								$("#alerts").html('');
								$("#alerts").css('display','none');

								$('#show').attr('disabled', 'true');
								$('#show').val('Espere por favor...');
								form.submit();
				   }
				});

				$('[name^="serial_sal"]').each(function (){
					$(this).bind('click',function(){
						validateSelection();
					});
				});

				$('#checkAll').bind('click',function(){
					checkAll();
					validateSelection();
				});
			});
		}else{
			$("#alerts").append("<li><label class='error_validation'>Debe escoger todos los par&aacute;metros antes de proceder a la b&uacute;squeda!</label></li>");
			$("#alerts").css('display','block');
		}
    }else{
        $('#resultSet').html('');
    }
}
//select\deselect all the sales
function checkAll() {
        var nodoCheck = document.getElementsByTagName("input");
        var varCheck = document.getElementById("checkAll").checked;
        for (i=0; i<nodoCheck.length; i++){
                if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "checkAll" && nodoCheck[i].disabled == false) {
                        nodoCheck[i].checked = varCheck;
                }
        }
}
/*
 * @Name: validateSelection
 * @Description: Changes the hidden value if one element
 *               is selected or not.
 */
function validateSelection(){
	var checked=0;
    
    $('[name^="serial_sal"]').each(function(i){
        if($(this).is(':checked')){
            checked=1;
            return;
        }else{
			$('#checkAll').attr('checked', false);
		}
    });

	
	if($('#checkAll').is(':checked')){
		checked=1;
	}else{
		if(checked!=1){
			checked=0;
		}
	}

    if(checked==1){
        $('#hdnOneSelection').val('1');
    }else{
        $('#hdnOneSelection').val('');
    }
}