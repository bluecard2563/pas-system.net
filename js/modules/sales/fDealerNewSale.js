$(document).ready(function(){
	show_ajax_loader();

        loadCustomer('');


        loadOtherCustomer('');
        
        $('input[name=bill_to_radio]').each(function(){
        $(this).bind('click',function(){
            if($(this).val()=='DEALER'){
                $('#dealerData').show();
                $('#customerData').hide();
                $('#otherData').hide();
            }else if($(this).val()=='OTHER') {
                $('#dealerData').hide();
                $('#customerData').hide();
                $('#otherData').show();
                //Call numbersOnly method when user change radioBill on new sale invoice section.
                $("#OtherrdDocumentType_CI").attr('checked', 'checked');
                $("#selOtherDocumentType").val('c');
                $("#selTypeOther").val('PERSON');
                numbersOnly($('#txtOtherDocument'));
            }else{
                $('#dealerData').hide();
                $('#customerData').show();
                $('#otherData').hide();
            }
			
            $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);
			

        });

    });
    
    $('#selectInv').change(function(){
            if($('#selectInv').val() == 'autoInv'){
                $('#radioBill').show();
                $('#customerData').show();
                $('#invLabel').hide();
            }else{
                $('#radioBill').hide();
                $('#customerData').hide();
                $('#otherData').hide();
                $('#dealerData').hide();
                $('#invLabel').show();
                
            }
	});  
        
        $('[id^="OtherrdDocumentType"]').each(function(){
            console.log($(this).attr('person_type'));
        $(this).bind('click', function(){
            $('#selTypeOther').val($(this).attr('person_type'));
            personTypeBindingsOther();
        });
    });

	$('#customerContainer').load(
		document_root+"rpc/loadsSales/loadCustomerComplete.rpc",
		{document_cus: ''},function(){
			setupCustomerSection();
			evaluateCardDates();
			enableQuestionsDiv();
			hide_ajax_loader();
			
			$("#txtBirthdayCustomer").change(enableQuestionsDiv);
		}
	);

	setupExtrasSection();

	initializeMainSalesForm('frmNewSale');

	addProductSelectAction();

	$('[name="rdCurrency"]').bind('click',function(){
		executeProductPriceCalculation();
		$('#serial_cur').val($(this).attr('serial_cur'));
	});

	$("#txtArrivalDate").bind("change", function(e){
		doTravelDatesVerifications();
		enableQuestionsDiv();
		displayQuestionsForAllExtras();
	});
	$("#txtDepartureDate").bind("change", function(e){
		checkAndSetProductMaxCoverageTime();
		doTravelDatesVerifications();
	});
	$("#selCountryDestination").bind("change", function(e){
		loadCities(this.value,'Destination','ALL');
		validateDestination();
	});
	addEmissionDateBoxAction();

	/*** CUSTOMER PROMO SECTION ***/
	enablePromoValidationButton();

	$('[id^="rdCouponApply"]').each(function(){
		$(this).bind('click', function(){
			evaluateSelection($(this).val());
		})
	});
	
	$("#txtBirthdayCustomer").change(enableQuestionsDiv);
        
        $('[id^="rdInvoicing_"]').each(function(){
            $(this).bind('click', function(){
                if($(this).val() === 'PAX'){
                    processInvoicingDataValidations('DISABLE');
                    $("#invoiceDataContainer").empty();
                }else{
                    displayInvoicingForm();
                }
            })
	});
});
function personTypeBindingsOther() {
        console.log('function');
        if ($("#selTypeOther").val()) {
            if ($('#selTypeOther').val() == 'LEGAL_ENTITY') {
                $('#lastNameOtherContainer').hide();
                $('#genderOtherDiv').hide();
                //$('#txtLastOtherName').rules('remove');
                $('#labelOtherName').html('* Raz&oacute;n Social:');
            } else {
                $('#lastNameOtherContainer').show();
                $('#genderOtherDiv').show();

                /*
                $('#txtLastOtherName').rules("add", {
                    required: {
                        depends: function (element) {
                            if ($('#customerData').css('display') == 'none')
                                return false;
                            else
                                return true;
                        }
                    },
                    invoice_holder: true,
                    messages: {
                        invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
                    }
                });
                */

                $('#labelOtherName').html('* Nombre:');
            }
        }
    }
