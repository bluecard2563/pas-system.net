//Java Script
//THIS FILE CONTAINS ALL THE FUNCTIONS USED IN:
//			fNewSale.js
//			fEditSale.js
//			fNewMasiveSale.js
checkProductValidatorURL = 'modules/estimator/registered/checkProductAdultsValidator.php';

//FILTERS LOADING
function loadCities(countryID, selectID, ALL) {
    if (selectID == "Destination") {
        var container = $('#cityDestinationContainer');
    } else if(selectID == "Representative"){
        var container = $('#repCityContainerCustomer');
    } else if (selectID == "Reassign") {
        var container = $('#cityContainerReassign');
    } else {
        var container = $('#cityContainerCustomer');
    }

    container.load(document_root + "rpc/loadCities.rpc", {
        serial_cou: countryID,
        serial_sel: selectID,
        all: ALL
    }, function () {
        $("#selCityReassign").bind("change", function (e) {
            loadDealer(this.value);
        });
        if (selectID == 'Reassign') {
            if ($("#selCityReassign").find('option').size() == 2) {
                loadDealer($('#selCityReassign').val());
            } else {
                loadDealer('');
            }
        }
        if (selectID == "Destination") {
            $("#selCityDestination option[name='General']").attr("selected", true);
        }

        // Santi 24 Sep 2020: Cities validation. Set hidden origin city value on origin city option change.
        $("#selCityCustomer").bind("change", function () {
            $('#hdnSelectedOriginCity').val($('#selCityCustomer').val());
            destinationCityValidation(false);
        });

        // Santi 24 Sep 2020: Cities validation. Set hidden destination city value on destination city option change.
        $("#selCityDestination").bind("change", function () {
            $('#hdnSelectedDestinationCity').val($('#selCityDestination').val());
            destinationCityValidation(true);
        });
    });
}

function loadDealer(cityID) {
    if (cityID) {
        $('#dealerContainerReassign').load(
                document_root + "rpc/loadDealers.rpc",
                {//serial_cou: countryID,
                    serial_cit: cityID,
                    dea_serial_dea: 'NULL'},
                function () {
                    $("#selDealer").bind("change", function (e) {
                        loadBranch(this.value);
                    });
                    if ($("#selDealer").find('option').size() == 2) {
                        loadBranch($('#selDealer').val());
                    } else {
                        loadBranch('');
                    }
                }
        );
    } else {
        $('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
        $('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
        $(':enabled#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
    }
}

function loadBranch(dealerID) {
    $(":enabled#selCounter").find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
    if (dealerID == "") {
        $("#selBranch option[value='']").attr("selected", true);
        $(":enabled#selCounter option[value='']").attr("selected", true);
        dealerID = 'stop';
    }

    $('#dealerData').load(
            document_root + "rpc/loadsSales/loadInvoiceDealer.rpc",
            {dea_serial_dea: dealerID}
    );
    $('#branchContainerReassign').load(
            document_root + "rpc/loadDealers.rpc",
            {dea_serial_dea: dealerID},
            function () {
                $("#selBranch").bind("change", function (e) {
                    loadBranchInvoice(this.value);
                    loadCounter(this.value);
                });
                loadCounter($('#selBranch').val());
            }
    );
}
//END OF FILTERS LOADING

function loadReassignSale(isChecked) {
    if (isChecked) {
        $("#selCounter option[value='']").attr("selected", true);
        $('#selCounter').attr('disabled', 'disabled');
        $('#saleInfoPACounter').hide();
        $('#reassignSaleContainer').load(document_root + "rpc/loadsSales/loadReassignSale.rpc", function () {
            $("#selCountryReassign").bind("change", function (e) {
                loadCities(this.value, 'Reassign', 'ALL');
                $('#hdnCntSerial_cou').val(this.value);
            });
        });
    } else {
        $('#reassignSaleContainer').html('');
        $('#selCounter').removeAttr('disabled');
        if ($('#selCounter').length == 1) {
            loadSaleInfo($(':enabled#selCounter').val(), 'YES', 'REGULAR');
        } else {
            loadSaleInfo($('#hdnOriginal_cnt').val(), 'NO', 'REGULAR');
        }
        
                
    }
}

function addSaleTypeClickAction() {
    $('[name="sale_type"]').click(function () {
        if ($(this).val() == 'VIRTUAL') {
            if ($(':enabled#selCounter').length == 1) {
                loadSaleInfo($(':enabled#selCounter').val(), 'YES', 'REGULAR');
            } else {
                loadSaleInfo($('#hdnOriginal_cnt').val(), 'NO', 'REGULAR');
            }
        }
        initializeSaleBox($(this).val());
    });
    initializeSaleBox('VIRTUAL');
}

function initializeSaleBox(saleType) {
    $('#txtCardNum').attr("readonly", true);
    $('#txtEmissionPlace').attr("readonly", true);
    $('#txtNameCounter').attr("readonly", true);
    $('#txtCodCounter').attr("readonly", true);
    $('#txtEmissionDate').attr("readonly", true);
    $('#txtDealer').attr("readonly", true);

    setCardBoxBehaviour(saleType, true);
    addEmissionDateBoxAction();
}

function setCardBoxBehaviour(saleType, erase_date) {
    if ($('#isEdition').val() != '1') {
        if (saleType == 'MANUAL') {
            $('#txtCardNum').attr("readonly", false);
            $('#txtEmissionDate').attr("readonly", false);
            setDefaultCalendar($("#txtEmissionDate"), '-1Y', '+0d');
            if (erase_date)
                $('#txtEmissionDate').val('');
            $('#txtCardNum').val('');
            $('#txtCardNum').change(function () {
                if ($('#txtCardNum').val() != '') {
                    var cardNum = $(this).val();
                    var serialCounter = $('#hdnSerial_cnt').val();
                    $.ajax({
                        url: document_root + "rpc/remoteValidation/sales/checkCardNumber.rpc",
                        type: "post",
                        data: ({txtCardNum: cardNum, serial_cnt: serialCounter, typeSale: saleType}),
                        success: function (data) {
                            var response = json_parse(data);
                            //hdnOriginal_cnt
                            switch (response['error']) {
                                case 'with_owner':
                                    $('#txtCodCounter').val(response['sellerCode']);
                                    $('#txtNameCounter').val(response['counterName']);
                                    $('#txtEmissionPlace').val(response['emissionPlace']);
                                    $('#txtDealer').val(response['name_dea']);
                                    if ($('#hdnSerial_cntRPC').length) {
                                        $('#hdnSerial_cntRPC').val(response['counter_id']);
                                        $('#hdnSerial_cnt').val(response['counter_id']);
                                    } else {
                                        $('#hdnSerial_cnt').val(response['counter_id']);
                                    }
                                    loadProductsBasedOnCounter(response['counter_id']);
                                    break;
                                case 'no_owner':
                                    break;
                                case 'was_sold':
                                    $('#txtCardNum').val('');
                                    alert('La tarjeta ingresada ya fue vendida.');
                                    break;
                                case 'out_of_range':
                                    $('#txtCardNum').val('');
                                    alert('La tarjeta no se encuentra asignada');
                                    break;
                            }
                        }
                    });
                }
            });
        } else { //VIRTUAL SALE
            $('#txtCardNum').unbind('change');
            $('#txtEmissionDate').val($('#hdnEmissionDate').val());
            $('#txtCardNum').val('AUTOMATICO');
            setDefaultCalendar($("#txtEmissionDate"), '+0d', '+0d');
        }

        if ($("#selProduct option:selected").attr("generate_number_pro") == 'NO') {
            $('#txtCardNum').unbind('change');
            $('#txtCardNum').val('N/A');
            $('#txtCardNum').attr("readonly", true);
        }
    }
}

function addEmissionDateBoxAction() {
    $('#txtEmissionDate').change(function () {
        validateDates();
        executeProductPriceCalculation();
    });
}

function addCounterSelectAction() {
    $(":enabled#selCounter").change(function (e) {
        loadSaleInfo(this.value, 'YES', 'REGULAR');
    });
}

function evaluateEmissionDate() {
    var emDays = dayDiff($("#txtDepartureDate").val(), $('#txtEmissionDate').val());
    if (emDays > 1) { //Checks if the departure day is at least 1 day more than the emission date
        alert("La Fecha de Salida debe ser mayor o igual a la Fecha de Emisi\u00f3n");
        $("#txtDepartureDate").val('');
        $("#txtDaysBetween").html('');
    }
    type = $('#selType').val();
    if (type == 'MANUAL') {
        var emDate = dayDiff($("#hdnEmissionDate").val(), $('#txtEmissionDate').val());
        if (emDate > 1) {
            alert("La Fecha de Emisi\u00f3n no puede ser mayor a hoy");
            $('#txtEmissionDate').val('');
        }
    }
}

function loadProductsBasedOnCounter(serialCounter) {
    $('#productsContainer').load(document_root + "rpc/loadsSales/loadProducts.rpc", {counter: serialCounter}, function () {
        addProductSelectAction();
    });
}

function addProductSelectAction() {
    $("#selProduct").bind("change", function (e) {

        $('#btnCustomerPromoButon').attr('disabled', true);

        if ($('#isEdition').val() != '1') {
            saleType = $('[name="sale_type"]:checked').val();
            previousValue = $('#txtCardNum').val();
            setCardBoxBehaviour(saleType, false);

            //IF the result of the cardBoxValue change is empty field, means that we should put the previous manually entered value
            if (saleType == 'MANUAL' && $('#txtCardNum').val() == '' && previousValue != 'N/A') {
                $('#txtCardNum').val(previousValue)
            }
        }

        if ($("#selProduct option:selected").attr("flights_pro") == '0') {//Many flights: HIDE COUNTRY SELECTION FIELDS
            var today = new Date();
            $("#txtDepartureDate").val(("0" + today.getDate()).slice(-2) + "/" + ("0" + (today.getMonth() + 1)).slice(-2) + "/" + today.getFullYear());
            $('#divDestination').hide();
            $("#selCountryDestination").rules("remove");
            $("#selCityDestination").rules("remove");
            $("#selCountryDestination option[value='']").attr("selected", true);
            $("#selCityDestination option[value='']").attr("selected", true);
            $("#depDate").hide();
            $("#arrDate").hide();
            $("#startCoverage").show();
            $("#endCoverage").show();
            $("#hdnFlights").val('YES');

        } else { //Only one flight: SHOW COUNTRY SELECTION FIELDS

            $("#txtDepartureDate").attr('readonly', false);
            $("#txtArrivalDate").attr('readonly', false);
            $("#txtDepartureDate").val("");
            setDefaultCalendar($("#txtDepartureDate"), '+0d', '+100y');
            setDefaultCalendar($("#txtArrivalDate"), '+0d', '+100y');

            $("#depDate").show();
            $("#arrDate").show();
            $("#startCoverage").hide();
            $("#endCoverage").hide();
            $("#hdnFlights").val('NO');
            if ($('hddDays').val() != '' && $("#divDestination").css('display') == 'none') {
                $("#txtArrivalDate").val('');
                $("#txtDaysBetween").html('');
                $('hddDays').val('');
            }
            $('#divDestination').show();
            $("#selCountryDestination").rules('add', {
                required: true
            });
            $("#selCityDestination").rules('add', {
                required: true
            });

            $('#selCountryDestination').removeAttr('disabled');
            //destination_restricted_pro: NATIONAL PRODUCTS SET COUNTRY TO THE COUNTER'S AND BLOCK IT
            if ($("#selProduct option:selected").attr("destination_restricted_pro") == 'YES') {
                $("#selCountryDestination option[value='" + $('#hdnSerial_cou').val() + "']").attr("selected", true);
                loadCities($('#hdnSerial_cou').val(), 'Destination', 'ALL');
                $("#selCountryDestination").attr("disabled", true);
            }
        }

        setupExtrasSection();
        loadServices($('#product_' + $(this).val()).attr('services'));
        checkAndSetProductMaxCoverageTime();
        enableQuestionsDiv();
        executeProductPriceCalculation();
        validateDestination();

        // Santi 24 Sep 2020: Cities validation Set hidden serial pro and destination_restricted_pro value on product option change.
        $('#hdnSelectedSerialProduct').val($("#selProduct option:selected").attr("serial_pro"));
        $('#hdnHasDestinationRestricted').val($("#selProduct option:selected").attr("destination_restricted_pro"));

    });
}

function loadCustomerForms(typeCus) {
    //Always load as PERSON type
    $("#divName").show();
    $("#divName2").hide();
    $("#divAddress").show();
    $("#divAddress2").hide();
    $("#divRelative").show();
    $("#divManager").hide();
}

function calculateAllServicesPrice() {
    clearCustomerPromoContainer();
    var travelDays = parseInt($('#txtDaysBetween').html());
    var currency = new Array();
    var productID = $('#selProduct').val();
    var range_price = $('#product_' + productID).attr('range_price');
    var people_in_card = countPeopleInCard();

    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');

    if (travelDays > 0) {
        var price = 0;
        var cost = 0;
        var type_fee;

        $('[id^="chkService_"]').each(function () {
            if ($(this).is(':checked')) {
                type_fee = $(this).attr('type_fee');

                if (type_fee == 'PERDAY') {
                    if (range_price == 'YES') {
                        travelDays = $('#pPrice_' + $('#selDayNumber').val()).text();
                        if (travelDays > 0) {
                            price += parseFloat($(this).attr('price')) * travelDays;
                            cost += parseFloat($(this).attr('cost')) * travelDays;
                        }
                    } else {
                        price += parseFloat($(this).attr('price')) * travelDays;
                        cost += parseFloat($(this).attr('cost')) * travelDays;
                    }
                } else if (type_fee == 'TOTAL') {
                    price += parseFloat($(this).attr('price'));
                    cost += parseFloat($(this).attr('cost'));
                }
            }
        });

        if (price > 0) {
            /* FIX PRICE WITH ALL CARD MEMBERS */
            price *= people_in_card;
            cost *= people_in_card;
            /* FIX PRICE WITH ALL CARD MEMBERS */

            $('#hdnServicePrice').val(price.toFixed(2));
            price = (price * currency['0']).toFixed(2);
            $('#servicePriceContainer').html(currency['1'] + ' ' + price);
            $('#hdnServiceCost').val(cost.toFixed(2));
        } else {
            $('#servicePriceContainer').html('');
            $('#hdnServicePrice').val('0');
            $('#hdnServiceCost').val('0');
        }
    }
}

function logManualInputPrice() {//WHEN PRICE IS INPUT MANUALLY, HERE ITS VALUES ARE LOGGED TO COMPLY WITH NORMAL CALCULATIONS
    formatManualPriceInputBox();
    var currency = new Array();
    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');
    var productID = $('#selProduct').val();
    var price = 0;
    var cost = parseFloat($('#pPrice_' + $('#selDayNumber').val()).attr('cost'));
    var insertedPrice = parseFloat($('#userPriceFee').val());
    var minValue = parseFloat($('#hdnMin').val());
    var maxValue = parseFloat($('#hdnMax').val());
    var day_price = $('#product_' + productID).attr('price_by_day_pro');

    if (insertedPrice <= maxValue && minValue <= insertedPrice) {
        var days = $('#pPrice_' + $('#selDayNumber').val()).text();
        if (day_price == 'YES') {
            price = (parseFloat(days) * insertedPrice).toFixed(2);
            cost = (cost * parseFloat(days)).toFixed(2);
        } else {
            price = insertedPrice.toFixed(2);
        }

        price = roundNumber(price, 2).toFixed(2);
        $('#priceContainer').html(currency['1'] + ' ' + price);
        price = roundNumber(parseFloat(price) / currency['0'], 2);
        $('#hdnPrice').val(price);
        $('#hdnCost').val(cost);
        calculateAllExtrasPrices();
    } else {
        alert('Ingrese un valor entre los precios establecidos.');
        $('#hdnPrice').val('');
        $('#hdnCost').val('');
        $('#priceContainer').html('');
        $('#userPriceFee').val('');
    }
}

function sumAllPricesAndDisplay() {
    var currency = new Array();
    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');
    var totalPriceProducts = 0;
    var totalCostProducts = 0;
    var totalPriceServices = 0;
    var totalCostServices = 0;
    var totalPriceExtras = 0;
    var totalCostExtras = 0;
    var birthDate = ((dateFormat($('#txtBirthdayCustomer').val())));
    var today = new Date();
    var birthdayFormat = new Date(birthDate);
    var age = today.getFullYear() - birthdayFormat.getFullYear();
    var m = today.getMonth() - birthdayFormat.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthdayFormat.getDate())) {
        age--;
    }
    console.log(age);
    // if(age>=$('#hdnElderlyAgeLimit').val()){
        var yearsBetween = parseInt(dayDiff($('#txtBirthdayCustomer').val(), $('#txtArrivalDate').val()) / 365);
        var ageReal = yearsBetween;
        
    if(ageReal>=70){
        var serial_pro= $('#product_' + $('#selProduct').val()).attr('serial_pro');
        console.log(serial_pro);
        if (serial_pro == 188 || serial_pro == 222) {
            totalPriceProducts = parseFloat($('#hdnPrice').val());
            totalCostProducts = parseFloat($('#hdnCost').val());
            $('#priceContainer').val(totalPriceProducts);
            console.log('Entro aqui');
        }
        if ($('#hdnPriceAdultSenior').length > 0 && $('#hdnPriceAdultSenior').val() != '') {
            totalPriceProducts = parseFloat($('#hdnPriceAdultSenior').val());
            totalCostProducts = parseFloat($('#hdnCost').val());
            $('#priceContainer').val(totalPriceProducts);
            console.log("hdnPriceAdultSenior",$('#hdnPriceAdultSenior').val());
            console.log("hdnPiceAdult",totalPriceProducts);
        }
    }else {
        if ($('#hdnPrice').length > 0 && $('#hdnPrice').val() != '') {
            totalPriceProducts = parseFloat($('#hdnPrice').val());
            totalCostProducts = parseFloat($('#hdnCost').val());
            $('#priceContainer').val(totalPriceProducts);
            console.log("hdnPice",totalPriceProducts);
        }

    }
    if ($('#hdnServicePrice').length > 0 && $('#hdnServicePrice').val() != '') {
        totalPriceServices = parseFloat($('#hdnServicePrice').val());
        totalCostServices = parseFloat($('#hdnServiceCost').val());
        $('#servicePriceContainer').val(totalPriceServices);
    }

    if ($('#hdnTotalPriceExtra').val() != '') {
        totalPriceExtras = parseFloat($('#hdnTotalPriceExtra').val());
        totalCostExtras = parseFloat($('#hdnTotalCostExtra').val());
        $('#totalPriceExtras').val(totalPriceExtras);
    }

    totalPriceProductsFixed = parseFloat(totalPriceProducts * currency['0']);
    var services_and_extras_price = parseFloat((totalPriceServices + totalPriceExtras) * currency['0']);
    var totalFixed = roundNumber(services_and_extras_price + totalPriceProductsFixed, 2).toFixed(2);
    var total = roundNumber((totalPriceServices + totalPriceProducts + totalPriceExtras), 2).toFixed(2);
    var cost = roundNumber(totalCostServices + totalCostProducts + totalCostExtras, 2).toFixed(2);
    //alert('Productos: '+totalPriceProducts+' Servicios: '+totalPriceServices+' Total: '+ total);

    $('#totalContainer').html(currency['1'] + ' ' + totalFixed);
    if ($('#totalGrossContainer').length > 0) {
        $('#totalGrossContainer').html(currency['1'] + ' ' + roundNumber(totalFixed * peasant_tax, 2).toFixed(2));
    }
    $('#hdnTotalCost').val(cost);
    $('#hdnTotal').val(total);
    $('#btnRegistrar').removeAttr('disabled');
    $('#btnCustomerPromoButon').attr('disabled', false);
}

function formatManualPriceInputBox() {
    var insertedPrice = $('#userPriceFee').val().split('.');
    if (insertedPrice[1]) {
        if (insertedPrice[1].length > 2) {
            insertedPrice[1] = insertedPrice[1].substring(0, 2);
            $('#userPriceFee').val(insertedPrice[0] + '.' + insertedPrice[1]);
        }
    }
}

function logPricesForDaySelectProducts() {//WHEN DAYS SELECT BOX IS DISPLAYED, HERE ITS PRICES ARE LOGGED TO COMPLY WITH NORMAL CALCULATIONS
    var priceID = $('#selDayNumber').val();
    var productID = $('#selProduct').val();
    var day_price = $('#product_' + productID).attr('price_by_day_pro');
    var calculate_price = $('#product_' + productID).attr('calculate_price');

    $('#priceContainer').html('');
    $('#hdnPrice').val('');
    $('#hdnCost').val('');

    var currency = new Array();
    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');

    if (priceID != '') {
        var price;
        var cost;
        if (calculate_price == 'YES') {
            if (day_price == 'YES') {
                price = roundNumber(parseFloat($('#selDayNumber option:selected').text()) * parseFloat($('#pPrice_' + priceID).attr('price')), 2).toFixed(2);
                cost = roundNumber(parseFloat($('#selDayNumber option:selected').text()) * parseFloat($('#pPrice_' + priceID).attr('cost')), 2).toFixed(2);
            } else {
                price = $('#pPrice_' + priceID).attr('price');
                cost = $('#pPrice_' + priceID).attr('cost');
            }
            $('#hdnPrice').val(price);
            price = roundNumber(price * currency['0'], 2).toFixed(2);
            $('#priceContainer').html(currency['1'] + ' ' + price);
            $('#hdnCost').val(cost);

            calculateAllExtrasPrices();
        } else {
            $('#rangePricesContainer').load(document_root + "rpc/loadsSales/displayRangeInfo.rpc", {
                serial_ppc: priceID,
                change_fee: currency['0']}, function () {
                $('#userPriceFee').change(logManualInputPrice);
            }
            );
        }
    } else {
        $('#rangePricesContainer').html('');
        $('#priceContainer').html('');
        $('#hdnPrice').val('');
    }
}

function loadDaysSelectBox() {
    var travelDays = $('#txtDaysBetween').html();
    var serial_pxc = $('#product_' + $('#selProduct').val()).attr('serial_pxc');
    var serial_cou = $('#hdnSerial_cou').val();
    var serial_pro = $('#product_' + $('#selProduct').val()).attr('serial_pro');

    $('#rangeSelect').load(document_root + "rpc/loadsSales/calculateFixPrice.rpc", {
        days: travelDays,
        serial_pxc: serial_pxc,
        serial_cou: serial_cou,
        serial_pro: serial_pro}, function () {
        $('#selDayNumber').change(function () {
            logPricesForDaySelectProducts();
        });
        $("#selDayNumber").rules("add", {
            required: true
        });
        $("#hdnPrice").rules("add", {
            required: true,
            messages: {
                required: "No existe un 'Precio' para el n&uacute;mero de d&iacute;as seleccionado. <br>\n\
	                          Por favor disminuya el n&uacute;mero de d&iacute;as del viaje."
            }
        });

        if ($("#hdnModify").length > 0 &&
                parseInt($('#selProduct :selected').attr('flights_pro')) == 0 &&
                $("#hdnModify").val() == 'YES') {
            $('#selDayNumber :option').each(function () {
                if ($(this).text() == $('#hdnBackupDays').val()) {
                    $(this).attr('selected', true);
                    return;
                }
            });
        }
    }
    );
}

function loadServices(has_services) {
    var serialCounter = $('#hdnSerial_cnt').val();

    $('#servicesContainer').load(document_root + "rpc/loadsSales/loadServices.rpc",
            {
                counter: serialCounter,
                has_services: has_services
            }, function () {
        $('[id^="chkService_"]').each(function () {
            $(this).bind('click', function () {
                calculateAllServicesPrice();
                sumAllPricesAndDisplay();
            });
        });
    })
}

function validateDates() {
    if ($("#txtArrivalDate").val() != '' && $("#txtDepartureDate").val() != '' && $('#txtEmissionDate').length > 0) {
        var days = dayDiff($("#txtDepartureDate").val(), $("#txtArrivalDate").val());
        console.log(days);
        if (days > 0) { //if departure date is smaller than arrival date
            // if (days > 364 && $("#selProduct option:selected").attr("individual_pro") != 'YES' && $("#selProduct option:selected").attr("flights_pro") != '0') {
                // alert("La Fecha de Llegada no puede ser mayor a 364 d\u00edas de la Fecha de Salida");
                // $("#txtArrivalDate").val('');
                // $("#txtDaysBetween").html('');
            // }
            if (days > 365) { //the departure date can't be over than 365 days of departure date
                alert("La Fecha de Llegada no puede ser mayor a 365 d\u00edas de la Fecha de Salida");
                $("#txtArrivalDate").val('');
                $("#txtDaysBetween").html('');
            } else {
                var emDays = dayDiff($("#txtDepartureDate").val(), $('#txtEmissionDate').val());
                if (emDays > 1) { //Checks if the departure day is at least 1 day more the the emission date
                    alert("La Fecha de Salida debe ser mayor o igual a la Fecha de Emisi\u00f3n");
                    $("#txtDepartureDate").val('');
                    $("#txtDaysBetween").html('')
                } else {
                    $("#txtDaysBetween").html(days);
                    $("#hddDays").val(days);
                }
            }
        } else {
            alert("La Fecha de Llegada no puede ser mayor a la Fecha de Salida");
            $("#txtDaysBetween").html('');
            $("#txtArrivalDate").val('');
        }
    }
    if ($("#txtArrivalDate").val() != '' && $("#txtDepartureDate").val() != '' && $('#txtEmissionDate').length <= 0) {
        $("#txtDepartureDate").val('');
        $("#txtArrivalDate").val('');
        $("#txtDaysBetween").html('');
        alert("Primero debe seleccionar un Counter");
    }
}

function clearAllDisplayedPrices() {
    $('#priceContainer').html('0.00');
    $('#servicePriceContainer').html('0.00');
    $('#totalPriceExtras').html('0.00');
    $('#totalContainer').html('');
}

function loadSaleInfo(counterID, show_line, type_sale) {
    $('#saleInfoPACounter').load(document_root + "rpc/loadsSales/loadSaleInfo.rpc", {serial_counter: counterID, show_line: show_line}, function () {
        $('#hdnDirectSale').val($('#hdnOfCounterRPC').val());
        $('[name="rdCurrency"]').bind('click', function () {
            calculateAllServicesPrice();
            sumAllPricesAndDisplay();
        });

        if ($('#rdIsFree').length > 0) {
            $('#rdIsFree').rules('add', {
                required: true
            });
        }

        $('#hdnEmissionDate').val($('#hdnEmissionDateRPC').val());
        $('#hdnCardNum').val($('#hdnCardNumRPC').val());
        $('#hdnSerial_cou').val($('#hdnSerial_couRPC').val());

        addSaleTypeClickAction();
        evaluateEmissionDate();
        $('#hdnSerial_cnt').val(counterID);

        if (type_sale == 'REGULAR') {
            if ($(":enabled#selCounter option:selected").val()) {
                loadProductsBasedOnCounter($(":enabled#selCounter option:selected").val());
            } else {
                loadProductsBasedOnCounter($('#hdnSerial_cnt').val());
            }
        } else {
            loadMassiveProductsBasedOnCounter($(":enabled#selCounter option:selected").val());
        }
    });
    $('#saleInfoPACounter').show();
}

function executeProductPriceCalculation() {
    clearCustomerPromoContainer();
    if ($("#hdnModify").val() == 'NO') {//IF MODIFICATION IS NOT PERMITTED, PRICE RECALCULATION IS DISABLED!
        totalSalePrice = $('#hdnTotal').val();
        totalSaleCost = $('#hdnTotalCost').val();

        //PERSIST ORIGINAL PRODUCT PRICE
        $('#priceContainer').val($('#hdnOriginalPrice').val());
        $('#hdnPrice').val($('#hdnOriginalPrice').val());
        $('#hdnCost').val($('#hdnOriginalCost').val());

        //PERSIST ORIGINAL TOTAL PRICE
        $('#totalContainer').html($('[name="rdCurrency"]:checked').attr('symbol') + ' ' + totalSalePrice);
        $('#hdnTotalCost').val(totalSaleCost);
        $('#hdnTotal').val(totalSalePrice);
        return false;
    }

    clearAllDisplayedPrices();
    if ($('#selProduct').val() != '') {
        var display_prices = $('#product_' + $('#selProduct').val()).attr('show_price_pro');
        var calculate_price = $('#product_' + $('#selProduct').val()).attr('calculate_price');

        if (display_prices == "NO" && calculate_price == "YES") {//PRICE AUTOCALCULATED
            $('#rangeSelect').html('');
            $('#rangePricesContainer').html('');
            executeDirectPriceCalculation();
        } else {//MANUAL PRICE
            loadDaysSelectBox();
        }
    }
}

function executeDirectPriceCalculation() {
    if ($('#selProduct').val() != '') {
        var travelDays = $('#txtDaysBetween').html();
        var serial_pxc = $('#product_' + $('#selProduct').val()).attr('serial_pxc');
        var serial_cou = $('#hdnSerial_cou').val();
        var serial_pro = $('#product_' + $('#selProduct').val()).attr('serial_pro');
        var display_prices = $('#product_' + $('#selProduct').val()).attr('show_price_pro');
        var birthday= $('#txtBirthdayCustomer').val();
        var change_fee = parseFloat($('[name="rdCurrency"]:checked').val());
        var symbol = $('[name="rdCurrency"]:checked').attr('symbol');

        if (travelDays != '' && serial_pxc != '') {
            // console.log("edad",birthday);
            $('#priceContainer').load(document_root + "rpc/loadsSales/calculatePrice.rpc", {
                days: travelDays,
                serial_pxc: serial_pxc,
                serial_cou: serial_cou,
                serial_pro: serial_pro,
                range_price: display_prices,
                change_fee: change_fee,
                birthday:birthday,
                beginDate: $("#txtDepartureDate").val(),
                endDate: $("#txtArrivalDate").val(),
                symbol: symbol}, function () {
                $("#hdnPrice").rules("add", {
                    required: true,
                    messages: {
                        required: "No existe un 'Precio' para el n&uacute;mero de d&iacute;as seleccionado. <br>\n\
									  Por favor disminuya el n&uacute;mero de d&iacute;as del viaje."
                    }
                });
                calculateAllExtrasPrices();
            }
            );
        }
    }
}

function evaluateCardDates() {
    if ($("#txtArrivalDate").val() != '' && $("#txtDepartureDate").val() != '' && $("#serialCus").length > 0) {
        $.ajax({
            url: document_root + "rpc/remoteValidation/sales/evaluateCardDates.rpc",
            type: "post",
            data: ({
                beginDate: $("#txtDepartureDate").val(),
                endDate: $("#txtArrivalDate").val(),
                serialCus: $("#serialCus").val(),
                serialSal: $("#hdnSerial_sal").val()
            }),
            success: function (data) {
                if (data == 'true') {
                    $("#txtArrivalDate").val('');
                    $('#txtDaysBetween').html('');
                    clearAllDisplayedPrices();

                    alert("El cliente ya tiene una tarjeta activa en el mismo per\u00EDodo de tiempo. No se puede emitir otro contrato.");
                } else {
                    executeProductPriceCalculation();
                }
            }
        });
    }
}

function loadCounter(branchID) {
    if (branchID == "") {
        $(":enabled#selCounter option[value='']").attr("selected", true);
        branchID = 'stop';
    }
    $('#counterContainerReassign').load(document_root + "rpc/loadCounter.rpc", {serial_dea: branchID}, function () {
        addCounterSelectAction();
    });
}

function loadBranchInvoice(branchID) {
    if (branchID == "") {
        $(":enabled#selCounter option[value='']").attr("selected", true);
        branchID = 'stop';
    }
}

function checkAndSetProductMaxCoverageTime() {
    if ($("#selProduct option:selected").attr("individual_pro") == 'YES' && $("#txtDepartureDate").val() != '') {
        var departureDate = $("#txtDepartureDate").val();
        var departureDate_array = departureDate.split('/');
        var arrivalDate = new Date(departureDate_array[2], departureDate_array[1] - 1, departureDate_array[0]);
        var mili = parseInt($("#hdnMaxCoverTime").val()) * 24 * 60 * 60 * 1000;
        var time = arrivalDate.getTime() + mili;
        arrivalDate.setTime(parseInt(time));
        var newArrivalDate;
        if (arrivalDate.getDate() < 10) {
            newArrivalDate = "0" + arrivalDate.getDate() + "/";
        } else {
            newArrivalDate = arrivalDate.getDate() + "/";
        }

        if (arrivalDate.getMonth() + 1 < 10) {
            newArrivalDate = newArrivalDate + "0" + (arrivalDate.getMonth() + 1) + "/";
        } else {
            newArrivalDate = newArrivalDate + (arrivalDate.getMonth() + 1) + "/";
        }

        newArrivalDate = newArrivalDate + arrivalDate.getFullYear();
        $("#txtArrivalDate").val(newArrivalDate);
        $("#txtArrivalDate").attr("readonly", "readonly");
        $("#txtArrivalDate").datepicker("destroy");


    }else if ($("#selProduct option:selected").attr("flights_pro") == '0' && $("#txtDepartureDate").val() != '') {
        var departureDate = $("#txtDepartureDate").val();
        var departureDate_array = departureDate.split('/');
        var arrivalDate = new Date(departureDate_array[2], departureDate_array[1] - 1, departureDate_array[0]);
        var mili = parseInt($("#hdnMaxCoverTime").val()) * 24 * 60 * 60 * 1000;
        var time = arrivalDate.getTime() + mili;
        arrivalDate.setTime(parseInt(time));
        var newArrivalDate;
        if (arrivalDate.getDate() < 10) {
            newArrivalDate = "0" + arrivalDate.getDate() + "/";
        } else {
            newArrivalDate = arrivalDate.getDate() + "/";
        }

        if (arrivalDate.getMonth() + 1 < 10) {
            newArrivalDate = newArrivalDate + "0" + (arrivalDate.getMonth() + 1) + "/";
        } else {
            newArrivalDate = newArrivalDate + (arrivalDate.getMonth() + 1) + "/";
        }

        newArrivalDate = newArrivalDate + arrivalDate.getFullYear();
        $("#txtArrivalDate").val(newArrivalDate);
        $("#depDate").hide();
        $("#arrDate").hide();
        $("#startCoverage").show();
        $("#endCoverage").show();

        validateDates();
        $("#txtDepartureDate").attr("readonly", "readonly");
        $("#txtArrivalDate").attr("readonly", "readonly");
        $("#txtDepartureDate").datepicker("destroy");
        $("#txtArrivalDate").datepicker("destroy");
    } else {
        $("#depDate").show();
        $("#arrDate").show();
        $("#startCoverage").hide();
        $("#endCoverage").hide();
        if ($("#selProduct option:selected").attr("flights_pro") == '0') {
            $("#txtArrivalDate").val('');
        }
    }
}

function isPACounter(serialCounter) {
    $.ajax({
        url: document_root + "rpc/remoteValidation/sales/isPACounter.rpc",
        type: "post",
        data: ({serialCounter: serialCounter}),
        success: function (data) {
            if (data == 'false') {
                $("#divSellFree").hide();
            } else {
                $("#divSellFree").show();
            }
        }
    });
}

//FUNCTIONS FOR EXTRAS
function hasSpouse(count) {
    var hasSpouse = 0;
    $("[id^='selRelationship_']").each(function () {
        if ($(this).attr('elementID') != count) {
            if ($(this).val() == 'SPOUSE') {
                hasSpouse = 1;
                return;
            }
        }
    });

    if ($("#selRelationship_" + count).val() == 'SPOUSE' && hasSpouse == 1) {
        alert("Ya est\u00e1 seleccionada la opci\u00f3n acompañante 1");
        $("#selRelationship_" + count).find('option[value=""]').attr('selected', true);
    }
}

function checkEmailExtraIsDuplicated(extraPosition) {
    //VALUES:
    // 0 - NO DUPLICATED EMAILS
    // 1 - DUPLICATED EMAIL
    // 2 - DUPLICATED WITH HOLDER EMAIL
    var duplicityChecker = 0;
    if ($("#txtMailCustomer").val() != $('#emailExtra_' + extraPosition).val()) {
        $("[id^='emailExtra_']").each(function () {
            if ($(this).attr("id") != 'emailExtra_' + extraPosition) {
                if ($(this).val() == $('#emailExtra_' + extraPosition).val()) {
                    alert('El "Email" ya ha sido ingresado en otro Extra.\n Por favor ingrese un "Email" diferente');
                    duplicityChecker = 1;

                }
            }
        });
    } else {
        alert('El "Email" ingresado es igual a "Email" del titular.\n Ingrese un "Email" distinto');
        duplicityChecker = 2;
    }
    return duplicityChecker;
}

function checkEmailExtra(count) {
    //VALUES:
    // 0 - NO DUPLICATED EMAILS
    // 1 - DUPLICATED EMAIL
    // 2 - DUPLICATED WITH HOLDER EMAIL
    var duplicityChecker = 0;
    if ($("#txtMailCustomer").val() != $('#emailExtra_' + count).val()) {
        $("[id^='emailExtra_']").each(function () {
            if ($(this).attr("id") != 'emailExtra_' + count) {
                if ($(this).val() == $('#emailExtra_' + count).val()) {
                    alert('El "Email" ya ha sido ingresado en otro Extra.\n Por favor ingrese un "Email" diferente');
                    duplicityChecker = 1;

                }
            }
        });
    } else {
        alert('El "Email" ingresado es igual a "Email" del titular.\n Ingrese un "Email" distinto');
        duplicityChecker = 2;
    }
    return duplicityChecker;
}

function displayQuestionsForAllExtras() {
    var extraPosition = parseInt($('#count').val());
    if (extraPosition > 0) {
        extraPosition--;
        for (extraPosition; extraPosition >= 0; extraPosition--) {
            displayQuestionsForExtra(extraPosition);
        }
    }
}

function displayQuestionsForExtra(count) {
    var serial_cus = $('#hdnSerial_cusExtra_' + count).val();
    var arrivalDate = new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
    arrivalDate.setFullYear(arrivalDate.getFullYear() - $('#hdnElderlyAgeLimit').val()); //ELDERLY
    var birthDate = new Date(Date.parse(dateFormat($('#birthdayExtra_' + count).val())));

    if ($('#txtArrivalDate').val() != '' && $('#birthdayExtra_' + count).val() != '' && $('#birthdayExtra_' + count).val() != undefined) {
        if ($('#hdnQuestionsExists').val() == 1) {

            //************** FOR SENIOR ALLOWED VALIDATION
            if((arrivalDate - birthDate) >= 0){
                if ($("#selProduct option:selected").attr("senior_pro") == 'NO') {
                    alert("Este producto no acepta Adultos Mayores.");
                    $("#selRelationship_" + count + " option[value='']").attr("selected", true);
                    $("#birthdayExtra_" + count).val('');
                    $("#idExtra_" + count).val('');
                    $("#serialCus_" + count).val('');
                    $("#selRelationship_" + count).val('');
                    $("#nameExtra_" + count).val('');
                    $("#lastnameExtra_" + count).val('');
                    $("#emailExtra_" + count).val('');
                    $("#hdnPriceExtra_" + count).val('');
                    $("#hdnCostExtra_" + count).val('');
                }
            }

            //**************** FOR DISPLAY QUESTIONS ********
            var yearsBetween = parseInt(dayDiff($('#birthdayExtra_'+count).val(), $('#txtArrivalDate').val()) / 365);
            if(yearsBetween >=  $('#hdnElderlyAgeLimit').val() && $("#selProduct option:selected").attr("senior_pro") == 'YES'){
                $('#questionsContainer_' + count).load(document_root + "rpc/loadsSales/loadElderQuestionsExtras.rpc", {
                    customer: serial_cus,
                    count: count,
                    editionAllowed: $("#hdnModify").val()}, function () {
                    var ext = '[groupid="' + count + '"]';
                    $(":select[id^='answer" + count + "_']" + ext).each(function () {
                        // Santi 15 Jun 2020: Remove rules function added to disable customer answers validation in heath statement.
                        // $(this).rules("add", {
                        //     required: true
                        // });
                        $(this).rules("remove");
                    });
                });
            }else{
                $('#questionsContainer_' + count).html('');
            }
        } else {
            alert("No existen preguntas registradas en el sistema");
        }
    } else {
        var ext = '[groupid="' + count + '"]';
        $(":select[id^='answer" + count + "_']" + ext).each(function () {
            $(this).rules("remove", "required");
        });
        $('#questionsContainer_' + count).html('');
    }
}

function setExtrasTotalPriceToZero() {
    highestExtraPosition = parseInt($('#count').val());
    for (extraPosition = highestExtraPosition - 1; extraPosition > -1; extraPosition--) {
        $('#extraPriceContainer_' + extraPosition).html('');
    }
    $('#hdnTotalPriceExtra').val('0');
    $('#hdnTotalCostExtra').val('0');
    $('#hdnChildrenFree').val($("#selProduct option:selected").attr("children_pro"));
    $('#hdnAdultsFree').val($("#selProduct option:selected").attr("adults_pro"));
    sumAllPricesAndDisplay();
}

function calculateAllExtrasPrices() {
    //Validate children restriction

     if(typeof extraPosition!=='undefined'){
        var yearsBetween = parseInt(dayDiff($('#birthdayExtra_' + extraPosition).val(), $('#txtArrivalDate').val()) / 365);
        if(yearsBetween <= $('#hdnAge').val()){
            if($("#selProduct option:selected").attr("restricted_children_pro")=='YES'){
                alert("Este producto no acepta niños.");
                $('#birthdayExtra_' + extraPosition).val('');
            }
        }
        console.log('edad',yearsBetween);
        console.log('edadTope',$('#hdnAge').val());
     }
    //GATHER INFORMATION
    var extrasJSONObject = new Object;

    extrasJSONObject.base_price = $('#hdnPrice').val();
    extrasJSONObject.base_cost = $('#hdnCost').val();
    extrasJSONObject.adults_free = $("#selProduct option:selected").attr("adults_pro");

    extrasJSONObject.percentage_adult = '' + $("#selProduct option:selected").attr("percentage_extras");
    extrasJSONObject.percentage_children = '' + $("#selProduct option:selected").attr("percentage_children");
    extrasJSONObject.percentage_spouse = '' + $("#selProduct option:selected").attr("percentage_spouse");

    extrasJSONObject.max_extras_pro = $("#selProduct option:selected").attr("max_extras_pro");
    extrasJSONObject.children_free = $("#selProduct option:selected").attr("children_pro");
    extrasJSONObject.extras_restriction = $("#selProduct option:selected").attr("extras_restricted_to_pro");

    extrasJSONObject.arrivalDate = $("#txtArrivalDate").val(); //TODO Change Age ValidationToEndDate
    extrasJSONObject.serial_pxc = $('#product_' + $('#selProduct').val()).attr('serial_pxc');

    extrasJSONObject.extras = new Array;
    numberOfExtras = parseInt($('#count').val());
    for (var i = 0; i < numberOfExtras; i++) {
        extrasJSONObject.extras[i] = new Object;
        extrasJSONObject.extras[i].age = $('#birthdayExtra_' + i).val();
        extrasJSONObject.extras[i].relationship = $('#selRelationship_' + i).val();
    }
    jsonString = json_encode(extrasJSONObject);

    currencyValue = parseFloat($('[name="rdCurrency"]:checked').val());
    currencySymbol = $('[name="rdCurrency"]:checked').attr('symbol');

    //SEND AND PROCESS
    var totalPriceExtras = 0;
    var totalCostExtras = 0;
    $.ajax({
        url: document_root + "modules/sales/ajaxGetExtrasPrices",
        type: "post",
        data: ({jsonData: jsonString}),
        success: function (jsonResponse) {
            //RECIBIR Y DESPLEGAR

            response = json_parse(jsonResponse);
            for (var i = 0; i < numberOfExtras; i++) {
                extraPrice = response[i]['price'];
                extraCost = response[i]['cost'];
                console.log(extraPrice);
                console.log(extraCost);

                totalPriceExtras += extraPrice;
                totalCostExtras += extraCost;

                $('#hdnPriceExtra_' + i).val(extraPrice);
                $('#hdnCostExtra_' + i).val(extraCost);

                priceExtraFixed = roundNumber((extraPrice) * currencyValue, 2).toFixed(2);
                $('#extraPriceContainer_' + i).html(currencySymbol + ' ' + priceExtraFixed);
            }

            $('#hdnTotalPriceExtra').val(totalPriceExtras);
            $('#hdnTotalCostExtra').val(totalCostExtras);

            totalPriceExtrasFixed = roundNumber((totalPriceExtras) * currencyValue, 2).toFixed(2);
            $('#totalPriceExtras').html(totalPriceExtrasFixed);

            calculateAllServicesPrice();
            sumAllPricesAndDisplay();
        }
    });
}

function rejectAgeOfExtra(extraPosition) {
    $('#birthdayExtra_' + extraPosition).val('');
    $('#extraPriceContainer_' + extraPosition).html('');
    var totalPrice = parseFloat($('#hdnTotalPriceExtra').val());
    var priceExtra = parseFloat($('#hdnPriceExtra_' + extraPosition).val());
    var totalCost = parseFloat($('#hdnTotalCostExtra').val());
    var costExtra = parseFloat($('#hdnCostExtra_' + extraPosition).val());

    totalPrice = totalPrice - priceExtra;
    totalCost = totalCost - costExtra;

    $('#hdnTotalPriceExtra').val(totalPrice);
    $('#hdnTotalCostExtra').val(totalCost);
    sumAllPricesAndDisplay();
}

function setExtraPriceToZero(extraPosition) {
    currencySymbol = $('[name="rdCurrency"]:checked').attr('symbol');
    priceExtraFixed = roundNumber(0, 2).toFixed(2);
    $('#hdnPriceExtra_' + extraPosition).val(0);
    $('#hdnCostExtra_' + extraPosition).val(0);
    $('#extraPriceContainer_' + extraPosition).html(currencySymbol + ' ' + priceExtraFixed);
}

function getAgeOfExtra(extraPosition) {
    var curr = new Date(Date.parse(dateFormat($('#birthdayExtra_' + extraPosition).val())));
    var year = curr.getFullYear();
    var month = curr.getMonth() + 1;
    var day = curr.getDate();
    var today = new Date(Date.parse(dateFormat($('#txtDepartureDate').val())));

    var age = today.getFullYear() - year - 1;

    if (today.getMonth() + 1 - month < 0) {
        return age;
    }
    if (today.getMonth() + 1 - month > 0) {
        return age + 1;
    }
    if (today.getUTCDate() - day >= 0) {
        return age + 1;
    }

    return age;
}

function enableQuestionsDiv() {
    if ($("#selType").val() == 'PERSON') {
        birthdateValue = $('#txtBirthdayCustomer').val();
        if ($('#txtArrivalDate').val() != '' && birthdateValue != '' && $("#selProduct option:selected").val() != '') {
            var arrivalDate = new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
            arrivalDate.setFullYear(arrivalDate.getFullYear() - $('#hdnDisplayQuestionsAge').val()); //QUESTIONS
            var birthDate = new Date(Date.parse(dateFormat(birthdateValue)));

            var arrivalDateElderly = new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
            arrivalDateElderly.setFullYear(arrivalDateElderly.getFullYear() - $('#hdnElderlyAgeLimit').val()); //ELDERLY

            //****** FOR CONTROL OF CHILDREN ALLOWED
            var yearsBetween = parseInt(dayDiff($('#txtBirthdayCustomer').val(), $('#txtArrivalDate').val()) / 365);
            if(yearsBetween <= $('#hdnAge').val()){
                if($("#selProduct option:selected").attr("restricted_children_pro")=='YES'){
                    alert("Este producto no acepta niños.");
                    $("#selProduct option[value='']").attr("selected",true);
                }
            }

            //******** FOR CONTROL OF SENIOR ALLOWED
            if ((arrivalDateElderly - birthDate) >= 0) {
                if ($("#selProduct option:selected").attr("senior_pro") == 'NO') {
                    alert("Este producto no acepta Adultos Mayores.");
                    $("#selProduct option[value='']").attr("selected", true);
                }
            }

            //******************** FOR CONTROL OF QUESTION ************
            var yearsBetween = parseInt(dayDiff($('#txtBirthdayCustomer').val(), $('#txtArrivalDate').val()) / 365);
            if (yearsBetween >=  $('#hdnElderlyAgeLimit').val() && $("#selProduct option:selected").attr("senior_pro") == 'YES') {
                show_ajax_loader();
                $('#customerQuestionsContainer').load(document_root + "rpc/loadsSales/loadElderQuestionsCustomer.rpc", {
                    customer: $('#serialCus').val(),
                    editionAllowed: $("#hdnModify").val()
                }, function () {
                    $(":select[id^='cust_answer']").each(function () {
                        // Santi 15 Jun 2020: Remove rules function added to disable customer answers validation in heath statement.
                        // $(this).rules("add", {
                        //     required: true
                        // });
                        $(this).rules("remove");
                    });

                    hide_ajax_loader();
                });
            } else {
                $(":select[id^='cust_answer").each(function () {
                    $(this).rules("remove");
                });

                $('#customerQuestionsContainer').html('');
            }
        } else {
            $('#customerQuestionsContainer').html('');
        }
    } else {
        $('#customerQuestionsContainer').html('');
    }

    showRepContainerForUnderAgeHolder();
}

function showRepContainerForUnderAgeHolder() {
    if ($("#selType").val() == 'PERSON') {
        if ($('#txtArrivalDate').val() != '' && $('#txtBirthdayCustomer').val() != '') {
            var yearsBetween = parseInt(dayDiff($('#txtBirthdayCustomer').val(), $('#txtArrivalDate').val()) / 365);

            if (yearsBetween <= underAge) {
                $('#repContainer').show();
            } else {
                $('#repContainer').hide();
                clearRepForm();
            }
        }
    }
}

function setupCustomerSection() {
    $('#txtNameCustom er').attr('readonly', false);
    $('#txtLastnameCustomer').attr('readonly', false);

    $('#txtDocumentCustomer').unbind('change').change(function () {
        customerDocumentValidation($(this).val(), "loadCustomer");
        //loadCustomer($(this).val());
    });
    
    $('#selOtherCountry').change(function(){
        console.log('country');
        loadOtherCities($('#selOtherCountry').val())
    });
    
    $('#txtOtherDocument').change(function(e) {
        //e.stopPropagation();
        console.log('Cambio');
        customerDocumentValidation($(this).val(), "loadOtherCustomer");
        //loadOtherCustomer($(this).val());
    });
    
    $('#txtNameCustomer').change(function () {
        document.getElementById("txtFirstNameInvoice").value = document.getElementById('txtNameCustomer').value;
    });
    $('#txtLastnameCustomer').change(function () {
        document.getElementById("txtLastNameInvoice").value = document.getElementById('txtLastnameCustomer').value;
    });
    $('#selCountry').change(function () {
        document.getElementById("selCountryInvoice").value = document.getElementById('selCountry').value;
    });
    $('#selCityCustomer').change(function () {
        document.getElementById("selCityInvoice").value = document.getElementById('selCityCustomer').value;
        // Santi 24 Sep 2020: Cities validation Set hidden origin city value on origin city option change.
        $('#hdnSelectedOriginCity').val($('#selCityCustomer').val());
        destinationCityValidation(false);
    });
    $('#txtAddress').change(function () {
        document.getElementById("txtAddressInvoice").value = document.getElementById('txtAddress').value;
    });
    $('#txtPhone1Customer').change(function () {
        document.getElementById("txtPhone1Invoice").value = document.getElementById('txtPhone1Customer').value;
    });
    $('#txtPhone2Customer').change(function () {
        document.getElementById("txtPhone2Invoice").value = document.getElementById('txtPhone2Customer').value;
    });
    $('#txtMailCustomer').change(function () {
        document.getElementById("txtEmailInvoice").value = document.getElementById('txtMailCustomer').value;
    });

//    $("#txtBirthdayCustomer").change(enableQuestionsDiv);
//    $("#txtBirthdayCustomer").change(executeProductPriceCalculation);
    
    
    $('#repDocument').change(function () {
        loadRepCustomer($(this).val());
    });

    $('#selType').change(function () {
        enableQuestionsDiv();
        switchCustomerType($(this).val());

        //
        $("#hdnTypeCustomer").val($(this).val());
    });
    $("#selCountry").bind("change", function (e) {
        loadCities(this.value, 'Customer', 'ALL');
    });
    enableQuestionsDiv();

    if ($("#hdnModify").length == 1) {
        if ($("#hdnModify").val() == 'YES') {
            setDefaultCalendar($("#txtBirthdayCustomer"), '-100y', '+0d', '-20y');
            setDefaultCalendar($("#txtBirthdayOtherCustomer"), '-100y', '+0d', '-20y');
            setDefaultCalendar($("#repBirthday"), '-100y', '-18y', '-20y');
        }
    } else {
        setDefaultCalendar($("#txtBirthdayCustomer"), '-100y', '+0d', '-20y');
        setDefaultCalendar($("#txtBirthdayOtherCustomer"), '-100y', '+0d', '-20y');
        setDefaultCalendar($("#repBirthday"), '-100y', '-18y', '-20y');
    }
    $("#txtBirthdayCustomer").change(enableQuestionsDiv);
    $("#txtBirthdayCustomer").change(executeProductPriceCalculation);


    //numbers or letters customer validation.
    $('#selCustomerDocumentType').unbind('change').change(function () {
        //Based on document type allow only numbers or letters/numbers.
        $('.number-customer').unbind("keypress");
        $('.alphanumeric-customer').unbind("keypress");
        var element = $("#txtDocumentCustomer");
        if ($(this).val() === 'c' || $(this).val() === 'r') {
            $("#txtDocumentCustomer").attr("class", "number-customer");
            numbersOnly(element);

        } else {
            $("#txtDocumentCustomer").attr("class", "alphanumeric-customer");
            alphanumericOnly(element);
        }

        //Initiate customer type document and document number validation.
        if ($('#txtDocumentCustomer').val()) {
            customerDocumentValidation($('#txtDocumentCustomer').val(), "loadCustomer");
        }
    });



    //numbers or letters other customer invoice validation.
    $('[id^="OtherrdDocumentType"]').each(function(){
        $(this).bind('change', function(event){

            //unbind the keypress event on the txtOtherDocument text box to allow only numbers or letters/numbers.
            $('.number-other').unbind("keypress");
            $('.alphanumeric-other').unbind("keypress");

            //the value of the document type is assigned to the other invoice document hidden.
            if ($(this).val() === 'CI') {
                $('#selOtherDocumentType').val('c');
            }
            else if ($(this).val() === 'RUC') {
                $('#selOtherDocumentType').val('r');
            }
            else {
                $('#selOtherDocumentType').val('p');
            }

            //Add new class to the other invoice document input to restrict numbers/letters digits.
            var element = $("#txtOtherDocument");
            if ($(this).val() === 'CI' || $(this).val() === 'RUC') {
                $("#txtOtherDocument").attr("class", "number-other");
                numbersOnly(element);

            } else {
                $("#txtOtherDocument").attr("class", "alphanumeric-other");
                alphanumericOnly(element);
            }
        });


    });

    /*
    //If other customer document type change then validate customer document number.
    $('[id^="OtherrdDocumentType"]').each(function(){
        $(this).bind('change', function(){
            if ($('#txtOtherDocument').val()) {
                customerDocumentValidation($('#txtOtherDocument').val(), "loadOtherCustomer");
            }
            //clearOtherCustomerContainer();
        });
    });
    */
}

function loadCustomer(customerID) {
    var docCust = customerID;
    var cardNumber = $("#hdnCardNum").val();
    var documentType = $("#selCustomerDocumentType").val();
    var systemOrigin = newSaleStatementOrigin;

    show_ajax_loader();
    $.ajax({
        url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({txtDocument: docCust}),
        success: function (customerExists) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                type: "post",
                data: ({txtDocument: docCust}),
                success: function (customerType) {
                    $('#customerContainer').load(document_root + "rpc/loadsSales/loadCustomerComplete.rpc", {
                            document_cus: docCust,
                            card_number: cardNumber,
                            document_type: documentType,
                            system_origin: systemOrigin
                    }, function () {
                            hide_ajax_loader();
                            setupCustomerSection();
                            evaluateCardDates();
                            enableQuestionsDiv();

                            $("#txtBirthdayCustomer").change(enableQuestionsDiv);

                            if ($('#nmBlackListed').size() > 0) {
                                $('#btnRegistrar').hide();
                            } else {
                                $('#btnRegistrar').show();
                            }

                        // Health Statement
                        checkCustomerHasStatement();

                        // Santi 24 Sep 2020: Cities validation. Set hidden origin city value on customer information change.
                        $('#hdnSelectedOriginCity').val($('#selCityCustomer').val());
                        destinationCityValidation(false);

                    });

                    $('#customerData').load(document_root + "rpc/loadsSales/loadInvoiceCustomer.rpc", {document_cus: docCust}, function () {
                        //hide_ajax_loader();
                    });

                    //hide_ajax_loader();

                }
            });

            $("#txtBirthdayCustomer").change(enableQuestionsDiv);
        }
        
    });

    // Health Statement: This code allows to set Customer Document Number in hidden input.
    if (customerID != null) {
        $("#hdnCustomerDocument").val(customerID);
    }

}

function loadOtherCities(countryID){
    console.log(countryID);
    if(countryID==""){
        $("#selOtherCity option[value='']").attr("selected",true);
    }
    $('#cityOtherContainer').load(document_root+"rpc/loadCities.rpc",{
        serial_cou: countryID,
        all: 'YES'
    });
}

function loadOtherCustomer(customerID) {
    var docCust = customerID;
    var documentType = $("#selOtherDocumentType").val();
    show_ajax_loader();
    $.ajax({
        url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({txtDocument: docCust}),
        success: function (customerExists) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                type: "post",
                data: ({txtDocument: docCust}),
                success: function (customerType) {
                        $('#otherData').load(
                            document_root + "rpc/loadsSales/loadInvoiceOtherCustomer.rpc",
                            {document_cus: docCust, document_type: documentType},
                            function () {
                                setupCustomerSection();
                                personTypeBindingsOther();
                                hide_ajax_loader();
                                $('[id^="OtherrdDocumentType"]').each(function(){
                                    $(this).bind('click', function(e){
                                        $('#selTypeOther').val($(this).attr('person_type'));

                                        if ($(this).val() === 'CI') {
                                            $("#OtherrdDocumentType_CI").attr('checked', 'checked');
                                        }
                                        else if ($(this).val() === 'RUC') {
                                            $("#OtherrdDocumentType_RUC").attr('checked', 'checked');
                                        }
                                        else {
                                            $("#OtherrdDocumentType_PASSPORT").attr('checked', 'checked');
                                        }
                                        personTypeBindingsOther();
                                    });
                                });
                            }
                    );
                }
            });
        }        
    });
}

function checkDocumentAndLoadExtra(extraPosition, documentExtra) {
    if ($("#txtDocumentCustomer").val() != documentExtra) {
        var documentDuplicated = false;

        //CHECK FOR DUPLICITY IN OTHER EXTRAS:
        $("[id^='idExtra_']").each(function () {
            if ($(this).attr("id") != 'idExtra_' + extraPosition) {
                if ($(this).val() == $('#idExtra_' + extraPosition).val()) {
                    alert('La identificaci\u00f3n ya ha sido ingresada.\n Por favor ingrese una diferente');
                    documentDuplicated = true;
                }
            }
        });

        if (!documentDuplicated) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
                type: "post",
                data: ({txtDocument: documentExtra}),
                success: function (customerExists) {
                    $.ajax({
                        url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                        type: "post",
                        data: ({txtDocument: documentExtra}),
                        success: function (customerType) {
                            if (customerType != 'PERSON' && customerType != 'NOCUSTOMER') {
                                alert('El cliente seleccionado no es una persona natural y por lo tanto no puede constar como viajero.');
                                clearExtraInfo(extraPosition);
                                $("#idExtra_" + extraPosition).val('');
                                setDefaultCalendar($("#birthdayExtra_" + extraPosition), '-100y', '+0d', '-20y');
                                displayQuestionsForExtra(extraPosition);
                            } else {
                                reloadExtraInfo(documentExtra, extraPosition);
                            }
                        }
                    });
                }
            });
        } else {
            clearExtraInfo(extraPosition);
        }
    } else {
        clearExtraInfo(extraPosition);
        alert("La identificaci\u00f3n ingresada es igual a la del titular.\n Ingrese una identificaci\u00f3n distinta");
    }
}

function clearExtraInfo(extraPosition) {
    $('#idExtra_' + extraPosition).val('');
    $("#nameExtra_" + extraPosition).val('');
    $("#lastnameExtra_" + extraPosition).val('');
    $("#birthdayExtra_" + extraPosition).val('');
    $("#emailExtra_" + extraPosition).val('');
}

function reloadExtraInfo(documentExtra, extraPosition) {
    loadExtraIntoPosition(documentExtra, extraPosition);
}

function addOneExtra(documentExtra) {
    extraPosition = parseInt($('#count').val());

    if (extraPosition >= $("#selProduct option:selected").attr("max_extras_pro")) {
        alert('El producto seleccionado no admite m\u00e1s extras.');
        return false;
    }
    $('#extrasContainer').append('<div id="extra_' + extraPosition + '" class="span-24 last line"></div>');
    loadExtraIntoPosition(documentExtra, extraPosition);
    $('#count').val(extraPosition + 1);
}

function loadExtraIntoPosition(documentExtra, extraPosition) {
    var cardNumber = $("#hdnCardNum").val();
    show_ajax_loader();
    $('#extra_' + extraPosition).load(
            document_root + "rpc/loadsSales/loadExtrasInfo.rpc", {
                document_cus: documentExtra,
                card_number: cardNumber,
                extraPosition: extraPosition,
                serialSale: $("#hdnSerial_sal").val(),
                serial_pro: $('#selProduct option:selected').attr('serial_pro'),
                serial_cou: $('#hdnSerial_cou').val(),
                serial_pxc: $('#selProduct option:selected').attr('serial_pxc'),
                editionAllowed: $("#hdnModify").val()},
            function () {
                hide_ajax_loader();
                displayQuestionsForExtra(extraPosition);
                setDefaultCalendar($("#birthdayExtra_" + extraPosition), '-100y', '+0d', '-20y');
                $('#birthdayExtra_' + extraPosition).attr("readonly", false);
                $("#birthdayExtra_" + extraPosition).change(function () {
                    displayQuestionsForExtra(extraPosition);
                    calculateAllExtrasPrices();
                });

                //ADD LISTENERS:
                $("#selRelationship_" + extraPosition).change(function () {
                    hasSpouse(extraPosition);
                    calculateAllExtrasPrices();
                });
                // $("#emailExtra_" + extraPosition).change(function () {
                //     if (checkEmailExtraIsDuplicated(extraPosition) != 0) {
                //         $("#emailExtra_" + extraPosition).val('');
                //     }
                // });
                $("#idExtra_" + extraPosition).change(function () {
                    checkDocumentAndLoadExtra(extraPosition, $(this).val());
                });
                calculateAllExtrasPrices();
                addExtraValidators(extraPosition);

                //BUTTONS:
                $('#btnDeleteExtra').css('display', 'block');
                $('#btnResetExtra').css('display', 'block');

                // Health Statement
                checkExtraCustomerHasStatement(extraPosition);
            });
}

function deleteOneExtra() {
    var extraPosition = parseInt($('#count').val()) - 1;
    removeExtraValidators(extraPosition);
    $('#extra_' + extraPosition).remove();
    $('#count').val(extraPosition);
    if ($('#count').val() == 0) {
        $('#btnDeleteExtra').css('display', 'none');
    }
    calculateAllExtrasPrices();
}

function displayMyExtras() {
    $.ajax({
        url: document_root + "modules/sales/modifications/ajaxGetExtrasIds.php",
        type: "post",
        data: ({serialSale: $("#hdnSerial_sal").val()}),
        success: function (extrasIds) {
            if (extrasIds != 'false') {
                extrasIds = jQuery.trim(extrasIds);
                singleIds = extrasIds.split("%%%");
                for (posId = 0; posId < singleIds.length; posId++) {
                    addOneExtra(singleIds[posId]);
                }
            }
        }
    });
}

function setupExtrasSection() {
    $.ajax({
        url: document_root + "rpc/remoteValidation/sales/checkExtras.rpc",
        type: "post",
        data: ({serial_pro: $("#selProduct option:selected").attr("serial_pro")}),
        success: function (extrasAdmitted) {
            if (extrasAdmitted == 'true') {
                $('#extrasGlobalSection').show();
                setupExtrasLayout();
            } else {
                $('#extrasGlobalSection').hide();
                $('#addNew').hide();
                resetExtras();
                $('#count').val('0');
            }
        }
    });
}

function setupExtrasLayout() {
    $('#btnAdd').attr('disabled', false);
    $('#btnAdd').unbind('click');
    $('#btnDeleteExtra').unbind('click');
    $('#btnResetExtra').unbind('click');

    $('#addNew').show();
    $('#btnAdd').bind("click", function () {
        addOneExtra();
    });
    $('#extrasContainer').show();
    $('#btnDeleteExtra').bind("click", function () {
        deleteOneExtra();
    });
    $('#btnResetExtra').bind("click", function () {
        resetExtras();
    });
    setExtrasTotalPriceToZero();
    displayMyExtras();
}

function resetExtras() {
    $('#btnAdd').attr('disabled', false);
    var extraPosition = parseInt($('#count').val());
    extraPosition--;
    for (extraPosition; extraPosition >= 0; extraPosition--) {
        removeExtraValidators(extraPosition);
        $('#extra_' + extraPosition).remove();
        $('#count').val(extraPosition);
        if (extraPosition == 0) {
            $('#btnDeleteExtra').css('display', 'none');
            $('#btnResetExtra').css('display', 'none');
        }
    }
    calculateAllExtrasPrices();
}

function addExtraValidators(extraPosition) {
    $("#idExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#selRelationship_" + extraPosition).rules('add', {
        required: true
    });
    $("#nameExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#lastnameExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#birthdayExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#emailExtra_" + extraPosition).rules('add', {
        required: true
    });

    // Health statement: This code allows to show or hide the link button to angular in extras section.
    $(".extra_" + extraPosition).change(function () {
        if ( $("#nameExtra_" + extraPosition).val() && $("#lastnameExtra_" + extraPosition).val()) {
            $("#extraContainerBtn_" + extraPosition).show();
        }
        else {
            $("#extraContainerBtn_" + extraPosition).hide();
        }
    });
}

function removeExtraValidators(extraPosition) {
    $("#idExtra_" + extraPosition).rules("remove");
    $("#selRelationship_" + extraPosition).rules("remove");
    $("#nameExtra_" + extraPosition).rules("remove");
    $("#lastnameExtra_" + extraPosition).rules("remove");
    $("#birthdayExtra_" + extraPosition).rules("remove");
}

function doTravelDatesVerifications(extraPosition) {
    validateDates();
    evaluateCardDates();
}

function initializeMainSalesForm(formName) {
    $("#hdnPrice").attr('editable', 'YES');

    $.validator.addMethod("adultsOnlyValidator", function (customerId, element) {
        return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"), 'ADULTS', 'ADULTS');
    }, "Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.");

    $.validator.addMethod("childrenOnlyValidator", function (customerId, element) {
        return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"), 'CHILDREN', 'CHILDREN');
    }, "Los acompa&ntilde;antes s&oacute;lo pueden ser menores.");

    $.validator.addMethod("seniorExtrasValidator", function (customerId, element) {
        return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"), 'NO', 'SENIOR');
    }, "Este producto no admite adultos mayores.");

    $("#" + formName).validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            sale_type: {
                required: true
            },
            txtCardNum: {
                required: true
            },
            txtCodCounter: {
                required: true
            },
            txtEmissionPlace: {
                required: true
            },
            txtEmissionDate: {
                required: {
                    depends: function (element) {
                        var manual = $("#txtEmissionDate").attr("readonly");
                        if (manual == true)
                            return false;
                        else
                            return true;
                    }
                }//,
                //dateLessThan: function(){return $('#hdnEmissionDate').val()}
            },
            txtNameCounter: {
                required: true
            },
            txtDealer: {
                required: true
            },
            selCustomerDocumentType: {
                required: true
            },
            txtDocumentCustomer: {
                required: true,
                alphaNumeric: true,
                adultsOnlyValidator: true,
                childrenOnlyValidator: true,
                seniorExtrasValidator: true,
                notEqualTo: "#repDocument"
            },
            selType: {
                required: true
            },
            txtNameCustomer: {
                required: true,
                textOnly: true
            },
            txtLastnameCustomer: {
                required: {
                    depends: function (element) {
                        return $("#selType").val() == 'PERSON';
                    }
                },
//                textOnly: true
            },
            rdGender: {
                required: {
                    depends: function(element) {
                            return $("#selType").val() == 'PERSON';
                    }
                }
            },
            selCountry: {
                required: true
            },
            selCityCustomer: {
                required: true
            },
            txtBirthdayCustomer: {
                required: {
                    depends: function (element) {
                        return $("#selType").val() == 'PERSON';
                    }
                },
                date: true
            },
            txtAddress: {
                required: true
            },
            txtPhone1Customer: {
                digits: true,
                minlength: 9,
                maxlength: 15
            },
            txtPhone2Customer: {
                digits: true,
                minlength: 9,
                maxlength: 15
            },
            txtCellphoneCustomer: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 15
            },
            txtEmailInvoice: {
                required: false,
                email: false
            },
            txtMailCustomer: {
                required: true,
                email: true
//                remote: {
//                    url: document_root + "rpc/remoteValidation/customer/checkCustomerEmail.rpc",
//                    type: "POST",
//                    data: {
//                        serialCus: function () {
//                            return $("#serialCus").val();
//                        },
//                        txtMail: function () {
//                            return $('#txtMailCustomer').val();
//                        }
//                    }
//                }
            },
            txtRelative: {
                required: {
                    depends: function (element) {
                        return $("#divRelative").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            txtPhoneRelative: {
                required: {
                    depends: function (element) {
                        return $("#divRelative").css('display') != 'none';
                    }
                },
                digits: true
            },
            txtManager: {
                required: {
                    depends: function (element) {
                        return $("#divManager").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            txtPhoneManager: {
                required: {
                    depends: function (element) {
                        return $("#divManager").css('display') != 'none';
                    }
                },
                digits: true
            },
            txtDepartureDate: {
                required: true,
                date: true
            },
            txtArrivalDate: {
                required: true,
                date: true
            },
            selCountryDestination: {
                required: {
                    depends: function (element) {
                        return $("#divDestination").css('display') != 'none';
                    }
                }
            },
            selCityDestination: {
                required: {
                    depends: function (element) {
                        return $("#divDestination").css('display') != 'none';
                    }
                }
            },
            selProduct: {
                required: true
            },
            selCounter: {
                required: true
            },
            repDocument: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                alphaNumeric: true,
                notEqualTo: "#txtDocumentCustomer"
            },
            repName: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            repLastname: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            repBirthday: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                date: true
            },
            repEmail: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                email: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/customer/checkCustomerEmail.rpc",
                    type: "POST",
                    data: {
                        serialCus: function () {
                            return $("#repSerialCus").val();
                        },
                        txtMail: function () {
                            return $('#repEmail').val();
                        }
                    }
                }
            },
            repCountry: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                }
            },
            repCityCustomer: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                }
            },
            txtOtherDocument: {
                //required: "#radio_dealer:checked"
                //required: $("#radio_dealer:checked").val() == 'OTHER'
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }

                }
            },
            selOtherDocumentType: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            rdOtherGender: {
                required: {
                    depends: function (element) {
                        return (
                            //$("#radio_dealer:checked").length &&
                            $("#radio_dealer:checked").val() == 'OTHER' &&
                            $("#genderOtherDiv").css('display') != 'none'

                        );
                        //return ($("#genderOtherDiv").css('display') != 'none')
                    }
                }
            },
            txtBirthdayOtherCustomer: {
                required: {
                    depends: function (element) {
                        return (
                            //$("#radio_dealer:checked").length &&
                            $("#radio_dealer:checked").val() == 'OTHER' &&
                            $("#genderOtherDiv").css('display') != 'none'

                        );
                        //return ($("#genderOtherDiv").css('display') != 'none')
                    }
                },
                date: true,
            },
            txtOtherFirstNameInvoice: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            txtOtherLastNameInvoice: {
                //required: "#radio_dealer:checked"
                required: {
                    depends: function (element) {
                        return (
                            //$("#radio_dealer:checked").length &&
                            $("#radio_dealer:checked").val() == 'OTHER' &&
                            $("#lastNameOtherContainer").css('display') != 'none'
                        );
                        //return ($("#genderOtherDiv").css('display') != 'none')
                    }
                },
            },
            txtOtherAddress: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            txtOtherPhone1: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            txtOtherEmail: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            selOtherCountry: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            selOtherCity: {
                //required: "#radio_dealer:checked"
                required: function(){
                    var radioValue = $("#radio_dealer:checked").val() == 'OTHER';
                    if(radioValue){
                        return true;
                    }else{
                        return false;
                    }
                }
            },
        },
        messages: {
            txtCardNum: {
                remote: 'El n&uacute;mero de tarjeta ingresado ya esta vendido o no le corresponde.'
            },
            txtEmissionDate: {
                dateLessThan: 'La fecha de emisi&oacute;n no puede ser mayor a la fecha actual.'
            },
            txtNameCustomer: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
            },
            repName: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre - Representante'."
            },
            txtLastnameCustomer: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
            },
            repLastname: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido - Representante'."
            },
            txtDocumentCustomer: {
                remote: "La identificaci&oacute;n ingresada ya existe.",
                alphaNumeric: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Identificaci&oacute;n'",
                adultsOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.",
                childrenOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser menores.",
                seniorExtrasValidator: "Este producto no admite adultos mayores.",
                notEqualTo: "La identificaci&oacute;n del Titular no puede ser la misma que la del Representante Legal"
            },
            rdGender: {
                    required: 'El campo "G&eacute;nero" es obligatorio.'
            },
            repDocument: {
                alphaNumeric: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Identificaci&oacute;n - Representante'",
                notEqualTo: "La identificaci&oacute;n del Representante Legal no puede ser la misma que la del Titular"
            },
            txtMailCustomer: {
                email: "Por favor ingrese un correo con el formato nombre@dominio.com",
                remote: "El 'E-mail - Representante' ingresado ya existe en el sistema."
            },
            repEmail: {
                email: "Por favor ingrese un correo con el formato nombre@dominio.com",
                remote: "El E-mail ingresado ya existe en el sistema."
            },
            txtBirthdayCustomer: {
                date: "El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
            },
            repBirthday: {
                date: "El campo 'Fecha de Naciemiento - Representante' debe tener el formato dd/mm/YYYY"
            },
            txtDepartureDate: {
                date: "El campo 'Fecha de Salida' debe tener el formato dd/mm/YYYY",
                lessOrEqualTo: 'La Fecha de Salida debe ser menor o igual que la Fecha de Llegada'
            },
            txtArrivalDate: {
                date: "El campo 'Fecha de Llegada' debe tener el formato dd/mm/YYYY",
                greaterOrEqualTo: 'La Fecha de Llegada debe ser mayor o igual que la Fecha de Salida'
            },
            txtPhone1Customer: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'",
                minlength: "El campo 'Tel&eacute;fono Fijo' debe tener como mínimo 9 dígitos",
                maxlength: "El campo 'Tel&eacute;fono Fijo' debe tener como máximo 9 dígitos"
            },
            txtPhone2Customer: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono 2'",
                minlength: "El campo 'Tel&eacute;fono Secundario' debe tener como mínimo 9 dígitos",
                maxlength: "El campo 'Tel&eacute;fono Secundario' debe tener como máximo 9 dígitos"
            },
            txtCellphoneCustomer: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Celular'",
                minlength: 'El campo "Celular" debe tener como mínimo 10 dígitos',
                maxlength: 'El campo "Celular" debe tener como máximo 10 dígitos'
            },
            txtRelative: {
                textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
            },
            txtPhoneRelative: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Familiar'"
            },
            txtManager: {
                textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
            },
            txtPhoneManager: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Gerente'"
            }
        },
        submitHandler: function (form) {
            //DOUBLE CONTROL TO AVOID SUBMITTING SALES WITH ZERO VALUES DUE TO AJAX-PRICE-UPDATE EXTENSIVE TIME
            if ($('#hdnTotal').val() == '' ||
                    $('#hdnTotal').val() == 'NaN' ||
                    parseInt($('#hdnTotal').val()) <= 0 ||
                    parseInt($('#hddDays').val()) <= 0) {
                alert('La tarifa del producto aun no ha sido calculada.\nPor favor revise el valor de la misma antes de registrar la venta');
                return false;
            }

            if ($('#hdnStatus').val() == 'INACTIVE') {
                alert('La venta de cualquier producto est\u00E1 prohibida a clientes en Lista Negra.');
                return false;
            }

            //CUSTOMER PROMOTION VALIDATION
            var partial_total = 0;
            if ($('#hdnPromoDiscount').length > 0 && $('#hdnPromoDiscount').val() != '') {
                partial_total = parseFloat($('#hdnTotal').val());
                partial_total -= parseFloat($('#hdnPromoDiscount').val());
            } else {
                partial_total = $('#hdnTotal').val();
            }

            //Health Statement: Validate if customres has a statement complete before submit sale.
            if (!statementSubmitValidation()) {
                return false;
            }

            // Santi 24 Sep 2020: Cities validation. Show alert on submit form.
            if ($('#hdnIsValidCity').val() == "false") {
                alert('La ciudad de destino no puede ser igual a la ciudad de residencia. Por favor seleccione una ciudad de destino diferente para continuar.');
                return false;
            }

            if (!confirm('Esta seguro que desea registrar la venta?\nTarifa Final: ' + partial_total)) {
                return false;
            }

            //IF SALE HAS AN INVOICE, DAYS SHOULD NOT BE LESS THAN ORIGINAL!
            if ($("#hdnModify").val() == 'NO') {
                var travelDays = parseInt($('#txtDaysBetween').html());
                var originalTravelDays = parseInt($('#hdnBackupDays').val());
                if (travelDays > originalTravelDays) {
                    alert('No puede seleccionar mas de ' + $("#hdnBackupDays").val() + 'dias de viaje.');
                    return false;
                }
            }

            /*
             * ONLY ALLOW LEGAL ENTITIES IF PRODUCT IS:
             flights_pro = 0
             generate_number_pro = 'NO'
             */
            if ($("#selProduct option:selected").attr("generate_number_pro") != 'NO'
                    || parseInt($("#selProduct option:selected").attr("flights_pro")) > 0) {
                //NO LEGAL ENTITIES ALLOWED!!
                if ($("#selType").val() != 'PERSON') {
//                    alert('El cliente no puede ser una persona juridica.');
//                    return false;
                }
            }

            $('#btnRegistrar').attr('disabled', 'true');
            $('#btnRegistrar').val('Espere por favor...');
            if ($("#selProduct option:selected").attr("flights_pro") == '0') {
                $('#hddDays').val($("#selDayNumber option:selected").attr("duration_ppc"));
            }
            form.submit();
        }
    });

    if ($("#hdnModify").length == 1) {
        if (parseInt($('#selProduct :selected').attr('flights_pro')) == 0 && $("#hdnModify").val() == 'NO') {
            $("#txtDepartureDate").attr('readonly', 'true');
            $("#txtArrivalDate").attr('readonly', 'true');
        } else {
            setDefaultCalendar($("#txtDepartureDate"), '+0d', '+100y');
            setDefaultCalendar($("#txtArrivalDate"), '+0d', '+100y');
        }
    } else {
        setDefaultCalendar($("#txtDepartureDate"), '+0d', '+100y');
        setDefaultCalendar($("#txtArrivalDate"), '+0d', '+100y');
    }
}

function switchCustomerType(newType) {
    switch (newType) {
        case 'LEGAL_ENTITY':
            $("#customerNameLabel").html('* Raz&oacute;n Social:');
            $("#customerLastNameLabelContainer").css("display", 'none');
            $("#customerBirthdateContainer").css("display", 'none');
            $("#customerContactLabel").html('* Gerente:');
            $("#customerContactPhoneLabel").html('* Tel&eacute;fono del gerente:');
            $("#genderDiv").hide();
            $('[id^="rdGender"]').attr('checked', false);
            break;
        default: //PERSON TYPE OR UNDEFINED VALUE
            $("#customerNameLabel").html('* Nombre:');
            $("#customerLastNameLabelContainer").css("display", 'block');
            $("#customerBirthdateContainer").css("display", 'block');
            $("#customerContactLabel").html('* Contacto de emergencia:');
            $("#customerContactPhoneLabel").html('* Tel&eacute;fono del contacto:');
            $("#genderDiv").show();
    }
}

/*********************************************************************************************
 ************************************** CUSTOMER PROMO SECTION ********************************
 **********************************************************************************************/

function bindingsForCustomerPromo(button_selector, text_selector, container_selector) {
    $(button_selector).bind('click', function () {
        validateCuponCode($(text_selector).val(), container_selector);
    });

    $(text_selector).bind('keypress', function () {
        clearCustomerPromoContainer();
    });
}

function validateCuponCode(promo_code, container_selector) {
    var serial_pbd = $('#selProduct').val();
    if (promo_code != '' && serial_pbd != '') {
        $(container_selector).load(document_root + 'rpc/loadClientPromo/validateCupon.rpc', {
            cupon: promo_code,
            serial_pbd: serial_pbd
        }, function () {

        });
    } else {
        $(container_selector).html('<i>Ingrese un c&oacute;digo y seleccione un producto antes de continuar</i>');
    }
}

function clearCustomerPromoContainer() {
    $('#customerPromo_container').html('');
    $('#txtCoupon').val('');
}

function enablePromoValidationButton() {
    $('#txtCoupon').bind('keypress', function () {
        $('#customerPromo_container').html('');
    });

    $('#btnCustomerPromoButon').bind('click', function () {
        $('#promo_loader').css('display', 'inline');
        var promo_code = $('#txtCoupon').val();
        var serial_pxc = $('#selProduct :selected').attr('serial_pxc');
        var total_sal = $('#hdnTotal').val();
        var symbol = $('[name="rdCurrency"]:checked').attr('symbol');

        if (promo_code != '' && serial_pxc != '' && serial_pxc) {
            if (total_sal != '' && total_sal > 0) {
                $('#customerPromo_container').load(document_root + 'rpc/loadsSales/validateCoupon.rpc',
                        {
                            promo_code: promo_code,
                            serial_pxc: serial_pxc,
                            total_sal: total_sal,
                            symbol: symbol
                        }, function () {
                    $('#promo_loader').css('display', 'none');
                });
            } else {
                $('#promo_loader').css('display', 'none');
                alert('Por favor cotice completamente un producto antes de aplicar un cup\u00F3n.');
            }
        } else {
            $('#promo_loader').css('display', 'none');
            alert('Se debe seleccionar un producto e ingresar un c\u00F3digo para continuar.');
        }
    });
}

function evaluateSelection(selection) {
    if (selection == 'YES') {
        $('#generalCustomerPromoContainer').show();
    } else {
        $('#generalCustomerPromoContainer').hide();
        clearCustomerPromoContainer();
    }
}

function loadMassiveProductsBasedOnCounter(serialCounter) {
    $('#productsContainer').load(document_root + "rpc/loadsSales/loadProducts.rpc", {counter: serialCounter, masive: 'YES'});
}

function countPeopleInCard() {
    return parseInt($('#count').val()) + 1;
}

function validateDestination() {
    var serial_pxc = $("#selProduct option:selected").attr("serial_pxc");
    var serial_cou = $('#selCountryDestination').val();

    if (serial_pxc && serial_cou) {
        show_ajax_loader();
        $.ajax({
            url: document_root + "rpc/remoteValidation/sales/checkDestination.rpc",
            type: "post",
            data: ({serial_pxc: serial_pxc, serial_cou: serial_cou}),
            dataType: "json",
            success: function (valid_destination) {
                if (!valid_destination) {
                    $('#selCountryDestination').val('');
                    $('#selCityDestination').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
                    alert('El producto seleccionado no puede utilizarse para el destino actual.');
                }

                hide_ajax_loader();
            }
        });
    }
}

function loadRepCustomer(docCust) {
    show_ajax_loader();
    $.ajax({
        url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({txtDocument: docCust}),
        success: function (customerExists) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                type: "post",
                data: ({txtDocument: docCust}),
                success: function (customerType) {
                    $('#repContainer').load(document_root + "rpc/loadsSales/loadCustomerComplete.rpc",
                            {
                                document_cus: docCust,
                                repContainer: 'TRUE'
                            }, function () {
                            if (customerType == 'LEGAL_ENTITY') {
                                clearRepForm();

                                alert('No se puede ingresar a una Persona Juridica como Representante Legal');
                            }

                            setDefaultCalendar($("#repBirthday"), '-100y', '-18y', '-20y');

                            $('#repDocument').change(function () {
                                loadRepCustomer($(this).val());
                            });

                            $('#repCountry').bind('change', function(){
                                loadCities($(this).val(), 'Representative', 'ALL');
                            });

                            //hide_ajax_loader();
                    });
                }
            });

            $("#txtBirthdayCustomer").change(enableQuestionsDiv);
        }
    });
    hide_ajax_loader();
}

function clearRepForm() {
    $('#repDocument').val("");
    $('#repSerialCus').val("");
    $('#repName').val("");
    $('#repLastname').val("");
    $('#repBirthday').val("");
    $('#repEmail').val("");
    $('#repCountry').val("");
    $('#repCityCustomer').val("");
}

function checkStatementRefreshButton(event, extraPosition, isExtra) {
    var cardNumber = $("#hdnCardNum").val();
    var systemOrigin = newSaleStatementOrigin;

    if (isExtra) {
        var customerDocument = $("#idExtra_" + extraPosition).val();
    }
    else {
        var customerDocument = $("#txtDocumentCustomer").val();
    }

    if (customerDocument && cardNumber) {
        show_ajax_loader();
        $.ajax({
            url: document_root + "rpc/remoteValidation/statements/statement.rpc",
            type: "POST",
            data: {action: 'customerHasStatement', customerDocument: customerDocument, cardNumber: cardNumber, systemOrigin: systemOrigin},
            //dataType: 'json',
            success: function (response) {
                var data = JSON.parse(response);
                // console.log(data);
                if (isExtra) {
                    $('#hdnExtraCustomerHasStatement_' + extraPosition).val(data.exist);
                    if (data.exist) {
                        $('#extraContainerBtn_' + extraPosition).hide();
                    }
                    else {
                        alert("El cliente no dispone de declaración de salud en la presente venta. Por favor complete la declaración de salud para proceder con el enlace.");
                        $('#extraContainerBtn_' + extraPosition).show();
                    }
                }
                else {
                    $('#hdnMainCustomerHasStatement').val(data.exist);
                    if (data.exist) {
                        $('#healthStatementContainer').hide();
                    }
                    else {
                        alert("El cliente no dispone de declaración de salud en la presente venta. Por favor complete la declaración de salud para proceder con el enlace.");
                        $('#healthStatementContainer').show();
                    }
                }
                hide_ajax_loader();
            }
        });
    }

}

//Health Statement: This function allows to show or hide the link button to angular.
function showHealthStatementButton() {
    var customerDocument = $("#hdnCustomerDocument").val();
    var customerType = $("#hdnTypeCustomer").val();
    var customerEmail = $("#txtMailCustomer").val();

    if ($("#txtMailCustomer").val() && customerType == "PERSON") {
        $("#healthStatementContainer").show();
    }
    else {
        $("#healthStatementContainer").hide();
    }
}


// Health Statement: // Each time the customer document number changes, we check if the customer has a statement.
// if the customer already have a statement, the button is not shown else the button will shown.
// We need to fill in the customer email and select the person type need to be PERSON to show the statement button.
function checkCustomerHasStatement() {
    if ($("#txtDocumentCustomer").val() != "") {

        if ($('#hdnMainCustomerHasStatement').val()) {
            //Hide button because customer has a statement complete
            $('#healthStatementContainer').hide();
        }
        else {
            //$('#healthStatementContainer').show();
            //if customer exist in our db on load check type person to show the statement button.
            if ($("#selCustomerDocumentType").val() == "c" || $("#selCustomerDocumentType").val() == "p") {

                if ($("#txtMailCustomer").val() != "") {
                    //Show button when customer not have a statement.
                    $('#healthStatementContainer').show();
                }
                else {
                    $('#healthStatementContainer').hide();
                }

                //on email change check type person to show the statement button.
                $('#txtMailCustomer').change(function () {
                    if ($("#txtMailCustomer").val() != "") {
                        //Show button when customer not have a statement.
                        $('#healthStatementContainer').show();
                    }
                    else {
                        $('#healthStatementContainer').hide();
                    }
                });
            }
        }
    }
    else {
        // Health Statement: The first time that the page load, we hide the statement button for main customer.
        $('#healthStatementContainer').hide();
    }
}


// Health Statement: Each time the extra customer document number changes, we check if the extra has a complete statement.
// if the extra already have a statement, the button is not shown else the button will shown.
// Hide health statement for extra customer if edit sale has been requested.
function checkExtraCustomerHasStatement(extraPosition) {
    if ($('#hdnIsEditSaleForm').val() === "1"){
        $("#extraContainerBtn_" + extraPosition).hide();
    }
    else {
        if ($('#hdnExtraCustomerHasStatement_' + extraPosition).val()){
            $("#extraContainerBtn_" + extraPosition).hide();
        }
        else {
            if ( $("#nameExtra_" + extraPosition).val() && $("#lastnameExtra_" + extraPosition).val()) {
                $("#extraContainerBtn_" + extraPosition).show();
            }
        }
    }
}

//Health Statement: This process checks if a customer has complete the statement before submit.
function statementSubmitValidation() {
    var extraPosition = parseInt($('#count').val());
    var hdnTypeCustomer = $("#hdnTypeCustomer").val();
    var hdnSelType = $("#selType").val();
    var selCustomerDocumentType = $("#selCustomerDocumentType").val();

    // Only customer id and passport will alert the user that the statement must be completed.
    if (selCustomerDocumentType === 'c' || selCustomerDocumentType === 'p') {
        if (extraPosition > 0) {
            extraPosition--;

            if (!$("#hdnMainCustomerHasStatement").val() || $("#hdnMainCustomerHasStatement").val() == "false") {
                alert("Por favor complete el formulario de declaración de salud del cliente principal y asegúrese de realizar el enlace de declaración para continuar.");
                return false;
            }

            for (extraPosition; extraPosition >= 0; extraPosition--) {
                //alert($('#idExtra_' + extraPosition).val());
                if (!$("#hdnExtraCustomerHasStatement_" + extraPosition).val() || $("#hdnExtraCustomerHasStatement_" + extraPosition).val() == "false") {
                    alert("Por favor complete el formulario de declaración de salud del acompañante y asegúrese de realizar el enlace de declaración para continuar.");
                    return false;
                }
            }
        }
        else {
            if (!$("#hdnMainCustomerHasStatement").val() || $("#hdnMainCustomerHasStatement").val() == "false") {
                 alert("Por favor complete el formulario de declaración de salud del cliente y asegúrese de realizar el enlace de declaración para continuar.");
                return false;
            }
        }
    }

    return true;
}

//Health Statement.class: This function allows to execute the process of opening the angular link to fill out the health declaration.
function openHealthStatement(event, extraPosition, isExtra) {
    var cardNumber = $("#hdnCardNum").val();
    var basSessionUser = $("#hdnSessionUser").val();

    if (!isExtra) {
        console.log('no es extra');
        var customerDocument = $("#hdnCustomerDocument").val();
        var customerName = document.getElementById('txtNameCustomer').value;
        var customerLastName = document.getElementById('txtLastnameCustomer').value;
        var customerEmail = document.getElementById('txtMailCustomer').value;
        var customerFullName = (customerName + " " + customerLastName).toUpperCase();
        var isExtraSent = false;

        if (customerName === "" || customerLastName === "") {
            alert("Se requiere el nombre y apellido del cliente para completar el formulario de declaración de salud.");
            return;
        }

        if (customerEmail == "") {
            alert("Se requiere el email del cliente para completar el formulario de declaración de salud.");
            return;
        }
    }
    else {
        console.log('es extra');
        var customerDocument = ($("#idExtra_" + extraPosition).val() ? $("#idExtra_" + extraPosition).val() : "");
        var customerName = ($("#nameExtra_" + extraPosition).val() ? $("#nameExtra_" + extraPosition).val() : "");
        var customerLastName = ($("#lastnameExtra_" + extraPosition).val() ? $("#lastnameExtra_" + extraPosition).val() : "");
        var customerFullName = (customerName + " " +customerLastName).toUpperCase();
        var customerEmail = ($("#emailExtra_" + extraPosition).val() ? $("#emailExtra_" + extraPosition).val() : "");
        var isExtraSent = true;

        if (customerName === "" || customerLastName === "") {
            alert("Se requiere el nombre y apellido del acompañante para completar el formulario de declaración de salud.");
            return;
        }

        if (customerEmail == "") {
            alert("Se requiere el email del acompañante para completar el formulario de declaración de salud.");
            return;
        }

    }

    //Object to send to Angular Statement.
    var data = {
        sessionUser: basSessionUser,
        cardNumber: cardNumber,
        customerDocument: customerDocument,
        customerEmail: customerEmail,
        customerName: customerFullName,
        isExtra: isExtraSent,
        systemOrigin: newSaleStatementOrigin
    };
    // console.log(data);

    var dataString = JSON.stringify(data);
    //console.log(dataString);
    //Base64 data encoded using local function called utoa().
    var dataEncoded = utoa(dataString);
    //console.log(dataEncoded);

    var dataDecoded = atou(dataEncoded);
    //console.log(dataDecoded);

    var url = statementBaseUrl + dataEncoded;
    window.open(url, '_blank');

}

// ucs-2 string to base64 encoded ascii
// https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/btoa
function utoa(str) {
    return btoa(unescape(encodeURIComponent(str)));
}
// base64 encoded ascii to ucs-2 string
function atou(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

function getCustomerDocumentType(origin) {
    var documentType;
    if (origin) {
        switch(origin) {
            case 'loadCustomer':
                documentType = $("#selCustomerDocumentType").val();
                break;
            case 'loadOtherCustomer':
                console.log($("input[name='OtherrdDocumentType']:checked").val());
                var radioValue = $("input[name='OtherrdDocumentType']:checked").val();
                if (radioValue === 'CI') {
                    documentType = 'c'
                }
                else if (radioValue === 'RUC') {
                    documentType = 'r'
                }
                else if (radioValue === 'PASSPORT')  {
                    documentType = 'p'
                }
                break;
        }

    }
    return documentType;
}

function customerDocumentValidation(customerDocument, origin) {
    console.log(customerDocument);
    console.log(origin);
    var data;
    var customerDocument = customerDocument.trim();
    var documentType = getCustomerDocumentType(origin);
    console.log(documentType);


    if (documentType && customerDocument) {
        if (documentType === "c") {
            //Validate Cedula number
            var action = "validateCustomerCardId";
        }
        else if (documentType === "r") {
            //Validate Ruc number
            var action = "validateCustomerRuc";
        }
        else {
            var action = "validateCustomerPassport";
        }

        show_ajax_loader();
        $.ajax({
            //async: false,
            url: document_root + "rpc/remoteValidation/customer/validateIdentification.rpc",
            type: "POST",
            data: {action: action, documentType: documentType,  customerDocument: customerDocument},
            success: function (response) {
                //hide_ajax_loader();
                data = JSON.parse(response);
                //console.log(data);
                if (data) {
                    validIdentification(data, origin);
                }
            },
        });

    }
}

function validIdentification(data, origin){
    console.log(data);
    console.log(origin);
    //If the customer identification is invalid alert error.
    if (!data.success) {
        alert(data.message);
        hide_ajax_loader();
        switch(origin) {
            case 'loadCustomer':
                clearCustomerContainer();
                break;
            case 'loadOtherCustomer':
                clearOtherCustomerContainer();
                break;
        }
    }
    //else if the customer identification is valid execute origin function.
    else {
        switch(origin) {
            case 'loadCustomer':
                var customerDocument = $('#txtDocumentCustomer').val();
                loadCustomer(customerDocument);
                break;
            case 'loadOtherCustomer':
                var customerOtherDocument = $('#txtOtherDocument').val();
                loadOtherCustomer(customerOtherDocument);
                break;
        }
    }

}

/*
function validIdentification(response, message) {
    var customerDocument = ($('#txtDocumentCustomer').val());
    $("#hdnValidIdentification").val(response);
    if (response) {
        if (customerDocument) {
            loadCustomer(customerDocument);
        }
    }
    else {
        alert(message);
        clearCustomerContainer();
        hide_ajax_loader();
    }
}
*/

function clearCustomerContainer() {
    $('#customerContainer :input').val('');
    $("#genderDiv").hide();
    $("#rdGender").attr("checked", false);

    var select = $("<select></select>").attr("id", "selType").attr("name", "selType").attr("title", 'El campo "Tipo de Cliente" es obligatorio.');
    select.append($("<option></option>").attr("value", "").text("- Seleccione -"));
    select.append($("<option></option>").attr("value", "PERSON").text("Persona Natural"));
    select.append($("<option></option>").attr("value", "LEGAL_ENTITY").text("Persona Jurídica"));
    $("#customerType").html(select);

    $('#selType').change(function () {
        enableQuestionsDiv();
        switchCustomerType($(this).val());
        $("#hdnTypeCustomer").val($(this).val());
    });

    $("#healthStatementContainer").hide();
    $("#hdnMainCustomerHasStatement").val(false);

}

function clearOtherCustomerContainer() {
    //$('#otherData :input').val('');
    $('#txtOtherDocument').val('');

    /*
    var radioValue = $("input[name='OtherrdDocumentType']:checked").val();
    console.log(radioValue);

    if (radioValue === 'CI') {
        $('#selTypeOther').val('PERSON');
        $('#OtherrdDocumentType_CI').attr("checked", true);
        $('#genderOtherDiv').show();
        $('#lastNameOtherContainer').show();
    }
    else if (radioValue === 'RUC') {
        $('#selTypeOther').val('PERSON');
        $('#OtherrdDocumentType_RUC').attr("checked", true);
        $('#genderOtherDiv').hide();
        $('#lastNameOtherContainer').hide();
    }
    else if (radioValue === 'PASSPORT')  {
        $('#selTypeOther').val('PERSON');
        $('#OtherrdDocumentType_PASSPORT').attr("checked", true);
        $('#genderOtherDiv').show();
        $('#lastNameOtherContainer').show();
    }
    */

    $('#txtBirthdayOtherCustomer').val('');
    $('#txtOtherFirstNameInvoice').val('');
    $('#txtOtherLastNameInvoice').val('');
    $('#txtOtherAddress').val('');
    $('#txtOtherPhone1').val('');
    $('#txtOtherPhone2').val('');
    $('#txtOtherEmail').val('');
    $('#selOtherCountry').val('');
    $('#selOtherCity').val('');

}

function numbersOnly(htmlElement) {
    htmlElement.bind("keypress", function(e) {
        var regex = new RegExp("^[0-9\b]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });
}

function alphanumericOnly(htmlElement) {
    htmlElement.bind("keypress", function(e) {
        var regex = new RegExp("^[a-zA-Z0-9\b]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });
}

function cleanOtherCustomerTextBox() {
    $('#txtOtherDocument').val('');
    $('#txtBirthdayOtherCustomer').val('');
    $('#txtOtherFirstNameInvoice').val('');
    $('#txtOtherLastNameInvoice').val('');
    $('#txtOtherAddress').val('');
    $('#txtOtherPhone1').val('');
    $('#txtOtherPhone2').val('');
    $('#txtOtherEmail').val('');
    $('#selOtherCountry').val('');
    $('#selOtherCity').val('');
}

// Santi 24 Sep 2020: Cities validation This function allows to validate if the selected cities (residence and destination) are different for products with destination_restricted_pro true.
function destinationCityValidation(showAlert) {
    // if the product has been set as destination_restricted_pro YES in product config page.
    if ($('#hdnHasDestinationRestricted').val() == 'YES') {
        if ($('#hdnSelectedOriginCity').val() == $('#hdnSelectedDestinationCity').val()) {
            $('#hdnIsValidCity').val(false);
            if (showAlert) {
                alert('La ciudad de destino no puede ser igual a la ciudad de residencia. Por favor seleccione una ciudad de destino diferente para continuar.');
                $('#selCityDestination').find('option[value=""]').attr('selected', 'selected');
                $('#hdnSelectedDestinationCity').val('');
            }
        } else {
            $('#hdnIsValidCity').val(true);
        }
    } else {
        $('#hdnIsValidCity').val(true);
    }
}
