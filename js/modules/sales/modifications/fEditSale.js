//Java Script
$(document).ready(function () {
    $('[name="sale_type"]').each(function () {
        $(this).attr('disabled', 'disabled');
    });
    $('#txtCardNum').attr("readonly", true);
    $('#txtEmissionPlace').attr("readonly", true);
    $('#txtNameCounter').attr("readonly", true);
    $('#txtCodCounter').attr("readonly", true);
    $('#txtEmissionDate').attr("readonly", true);
    $('#txtDealer').attr("readonly", true);

    $("#txtBirthdayCustomer").change(enableQuestionsDiv);
    $("#selProduct").change(enableQuestionsDiv);
	executeDirectPriceCalculation();

    $("#txtArrivalDate").bind("change", function (e) {
        doTravelDatesVerifications();
        displayQuestionsForAllExtras();
        enableQuestionsDiv();
		executeDirectPriceCalculation();
    });
    $("#txtDepartureDate").bind("change", function (e) {
        checkAndSetProductMaxCoverageTime();
        doTravelDatesVerifications();
		executeDirectPriceCalculation();
    });
      
    setupCustomerSection();

    setupExtrasSection();

    initializeMainSalesForm('frmEditSale');

    addProductSelectAction();

    $('[name="rdCurrency"]').bind('click', function () {
        executeProductPriceCalculation();
        $('#serial_cur').val($(this).attr('serial_cur'));
    });

    $("#selCountryDestination").bind("change", function (e) {
        loadCities(this.value, 'Destination', 'ALL');
        validateDestination();
    });
    addEmissionDateBoxAction();

    $("#observations").rules("add", {
        required: true
    });

    if (parseInt($('#selProduct :selected').attr('flights_pro')) == 0 && $("#hdnModify").val() == 'YES') {
        loadDaysSelectBox();
    }
	
	 

    $('[id^="chkService_"]').each(function () {
        $(this).bind('click', function () {
            calculateAllServicesPrice();
            sumAllPricesAndDisplay();
        });
    });

    if ($('#hasRepData').val() == 'YES') {
        $('#repContainer').show();
    }
});