//Java Script
$(document).ready(function(){
	/*VALIDATION SECTION*/
		$("#frmDispatchApplications").validate({
			errorLabelContainer: "#alerts",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				selStatus: {
					required: true
				},
				txtComments: {
					required: true
				}
			}
		});
		/*END VALIDATION SECTION*/
});