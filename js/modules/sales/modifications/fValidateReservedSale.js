//Java Script
var pager;
$(document).ready(function(){
	$('#btnSearch').click(function(){
		var card_number=$('#txtCardNumber').val();
		var maxMonths=$('#limitTime').val();

		if(card_number!=''){
			$('#cardContainer').load(document_root+"rpc/loadsSales/modifications/getCardChangesHistoryReserved.rpc", 
				{card_number: card_number,
				  time_limit: maxMonths,
                                    type: 'RESERVED'
				},
				function(){
					/*VALIDATION SECTION*/
					$("#frmValidateReservedSale").validate({
							errorLabelContainer: "#alerts",
							wrapper: "li",
							onfocusout: false,
							onkeyup: false,
							
					});
					/*END VALIDATION SECTION*/

					//Paginator
					if($('#salesLogTable').length>0){
						pager = new Pager('salesLogTable', 5 , 'Siguiente', 'Anterior');
						pager.init(7); //Max Pages
						pager.showPageNav('pager', 'salesLogNav'); //Div where the navigation buttons will be placed.
						pager.showPage(1); //Starting page
					}				
				}
			);
		}else{
			alert('Ingrese un n\u00FAmero de tarjeta por favor.');
		}
    });    
});

function disableBtn(){
	if($("#frmValidateReservedSale").valid()) {
		$('#btnSubmit').attr('disabled', 'true');
		$('#btnSubmit').val('Espere por favor...');
		$('#frmValidateReservedSale').submit();
	}
}
