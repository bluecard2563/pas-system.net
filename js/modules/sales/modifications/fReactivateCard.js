//Java Script
$(document).ready(function(){

	$('#btnSearch').bind("click",function(){
		/*SEARCH FIELD VALIDATION*/
		$('#alerts').hide();
		$('#alerts').html('');
		if($('#txtCardNo').val() == '') {
			$('#alerts').append('<li><b>'+$('#txtCardNo').attr('title')+'</b></li>');
			$('#alerts').show();
		} else if(!/^\d+$/.test($('#txtCardNo').val())) {
			$('#alerts').append('<li><b>El campo de b&uacute;squeda solo admite n&uacute;meros.</b></li>');
			$('#alerts').show();
		} else {
			searchCard();
		}
		/*SEARCH FIELD VALIDATION*/
	});
});

function searchCard() {
	$('#cardInformation').load(document_root+'rpc/loadsSales/modifications/getExpiredCards.rpc',{card_number: $('#txtCardNo').val()},function(){
		/*VALIDATION SECTION*/
		$("#frmReactivateCard").validate({
				errorLabelContainer: "#alerts",
				wrapper: "li",
				onfocusout: false,
				onkeyup: false,
				rules: {
					txtBeginDateSal: {
						required: true
					},
					txtEndDateSal: {
						required: true
					},
					txtDescription: {
						required: true
					}
				}
		});
		/*END VALIDATION SECTION*/
		$("#txtBeginDateSal").bind("change",function(){
			if(!evaluateDates()) {
				$(this).val('');
			}
		});
		$("#txtEndDateSal").bind("change",function(){
			if(!evaluateDates()) {
				$(this).val('');
			}
		});
	});
}

function evaluateDates(){
	if($("#txtBeginDateSal").val()!='') {
		var days=dayDiff($("#old_end_date").val(),$("#txtBeginDateSal").val());
		if(days>60){ //if departure date is smaller than arrival date
			alert("La nueva fecha de inicio debe estar dentro de los siguientes 60 d\u00EDas de la fecha fin inicial.");
			return false;
		}
	}
    if($("#txtBeginDateSal").val()!='' && $("#txtEndDateSal").val()!='') {
		var days=dayDiff($("#txtBeginDateSal").val(),$("#txtEndDateSal").val());
		if(days<0){ //if departure date is smaller than arrival date
			alert("La Fecha de Llegada no puede ser mayor a la Fecha de Salida.");
			return false;
		} else if(days>$("#days_sal").val()) {
			alert("El rango de fechas no puede ser mayor al inicial.");
			return false;
		}
	}
	return true;
}