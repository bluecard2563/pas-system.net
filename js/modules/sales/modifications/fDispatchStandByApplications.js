//Java Script
$(document).ready(function(){
	$("#frmDispatchApplications").validate({
			errorLabelContainer: "#alerts",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,

			rules: {
				selStatus: {
					required: true
				},
				txtObservations: {
					required: true,
					textOnly: true
				}
			},
			messages: {
				txtObservations: {
					textOnly: "El campo 'Observaciones' admite s&oacute;lo texto."
				}
			},
			submitHandler: function(form) {
				$('#btnSubmit').attr('disabled', 'true');
				$('#btnSubmit').val('Espere por favor...');
				form.submit();
		   }
		});
});