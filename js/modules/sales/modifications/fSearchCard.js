//Java Script
$(document).ready(function(){
	$("#frmSearchCard").validate({
						errorLabelContainer: "#alerts",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,

						rules: {
							txtCardNumber: {
								required: true
							}

						},
						submitHandler: function(form) {
							$('#btnSearch').attr('disabled', 'true');
							$('#btnSearch').val('Espere por favor...');
							form.submit();
					   }
		});

});