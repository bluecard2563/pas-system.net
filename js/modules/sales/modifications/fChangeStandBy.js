//Java Script
var pager;
$(document).ready(function(){
	$('#btnSearch').click(function(){
		var card_number=$('#txtCardNumber').val();
		var maxMonths=$('#limitTime').val();

		if(card_number!=''){
			$('#cardContainer').load(document_root+"rpc/loadsSales/modifications/getCardChangesHistory.rpc", 
				{card_number: card_number,
				  time_limit: maxMonths,
                  type: 'STAND_BY'
				},
				function(){
					/*VALIDATION SECTION*/
					$("#frmChangeStatus").validate({
							errorLabelContainer: "#alerts",
							wrapper: "li",
							onfocusout: false,
							onkeyup: false,
							rules: {
								txtBeginDate: {
									required: true
								},
								txtEndDate: {
									required: true
								},
								txtComments: {
									required: true
								}
							}
					});
					/*END VALIDATION SECTION*/

					//Paginator
					if($('#salesLogTable').length>0){
						pager = new Pager('salesLogTable', 5 , 'Siguiente', 'Anterior');
						pager.init(7); //Max Pages
						pager.showPageNav('pager', 'salesLogNav'); //Div where the navigation buttons will be placed.
						pager.showPage(1); //Starting page
					}

					/*CALENDAR*/
					if($("#txtBeginDate").length > 0 && $("#txtEndDate").length > 0){
						setDefaultCalendar($("#txtBeginDate"),'-1y','+1y');
						setDefaultCalendar($("#txtEndDate"),'-1y','+1y');
						evaluateEmissionDate();
					}

					$('#txtBeginDate').change(function(e){
						evaluateEmissionDate();
					});
					$('#txtEndDate').change(function(e){
						evaluateEmissionDate();
					});
					/*END CALENDAR*/
				}
			);
		}else{
			alert('Ingrese un n\u00FAmero de tarjeta por favor.');
		}
    });    
});

function displayTripDays(){
	$("#txtDays").html(dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val()));
}

function disableBtn(){
	if($("#frmChangeStatus").valid()) {
		$('#btnSubmit').attr('disabled', 'true');
		$('#btnSubmit').val('Espere por favor...');
		$('#frmChangeStatus').submit();
	}
}

function evaluateEmissionDate(){
	var emDays = dayDiff($("#txtEndDate").val(),$('#txtBeginDate').val());
	
    if(emDays > 1){ //Checks if the departure day is at least 1 day more than the emission date
		alert("La Fecha de Salida debe ser mayor o igual a la Fecha de Emisi\u00f3n");
		$("#txtEndDate").val('');
		$("#txtDays").html('');
    }else{
		displayTripDays();
	}
}