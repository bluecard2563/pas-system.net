//Java Script
$(document).ready(function(){
	$('#btnSearch').click(function(){
		var card_number=$('#txtCardNumber').val();
        if(card_number!=''){
			$('#saleContainer').load(document_root+"rpc/loadsSales/modifications/getSaleInformation.rpc", {card_number: card_number}, function(){
				setDefaultCalendar($("#txtArrivalDate"),'-0d','+10y','-20y');
				$("#txtArrivalDate").bind('change',function(){
					evaluateChagedDate();
				});

				$("#frmAddDays").validate({
						errorLabelContainer: "#alerts",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,

						rules: {
							txtArrivalDate: {
								required: true,
								date: true
							}
						},
						messages: {
							txtArrivalDate:{
								date: "El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
							}
						},
						submitHandler: function(form) {
							$('#btnSubmit').attr('disabled', 'true');
							$('#btnSubmit').val('Espere por favor...');
							form.submit();
					   }
					});
			});
		}else{
			alert('Ingrese un n\u00FAmero de tarjeta por favor.');
		}
    });
});

function evaluateChagedDate(){
	var depDate=$('#lblDepartureDate').html();
	var arrDate=$('#txtArrivalDate').val();
	
	if(arrDate){
		var new_days=parseFloat(dayDiff(depDate,arrDate));
		var max_days=parseFloat($('#hddOldDays').val())+parseFloat($('#maxAddedDays').val());

		if(new_days){
			if(new_days>max_days){
				$('#txtArrivalDate').val($('#oldArrivalDate').val());
				$('#txtDaysBetween').html($('#hddOldDays').val());
				$('#hddDays').val($('#hddOldDays').val());
				alert('Es permitido aumentar hasta '+$('#maxAddedDays').val()+' d\u00EDas!');
			}else if(parseDate(arrDate) < parseDate($('#oldArrivalDate').val())){
				$('#txtArrivalDate').val($('#oldArrivalDate').val());
				$('#txtDaysBetween').html($('#hddOldDays').val());
				$('#hddDays').val($('#hddOldDays').val());
				alert('No se puede retirar d\u00EDas del viaje!');
			}else{
				$('#txtDaysBetween').html(new_days);
				$('#hddDays').val(new_days);
			}
		}
	}
}