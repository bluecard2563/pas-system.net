//Java Script
var pager;
$(document).ready(function(){
	if($('#tblModifyObservations').length>0){
		pager = new Pager('tblModifyApplications', 10 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}
	
	$("#frmCheckSalesApplication").wizard({
		show: function(element) {
			},
		next:"Siguiente >",
		prev:"< Anterior",
		action:"Aceptar",
		extra:"ui-state-default ui-corner-all"
	});

	$("#frmCheckSalesApplication").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selAuthorize:{
				required: true
			},
			authorizeObs:{
				required: true
			}
		},
		messages: {
   		}
	});
});