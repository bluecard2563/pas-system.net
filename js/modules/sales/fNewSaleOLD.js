//Java Script
$(document).ready(function(){
	show_ajax_loader();
	
	if($(':enabled#selCounter').length==0){
		loadSaleInfo($('#hdnSerial_cnt').val(), 'NO', 'REGULAR');
	}
	
	$('#chkReassign').click(function(){
		loadReassignSale($(this).is(':checked'));
	});
	
	$('#selectInv').change(function(){
            if($('#selectInv').val() == 'autoInv'){
                $('#radioBill').show();
                $('#customerData').show();
                $('#invLabel').hide();
            }else{
                $('#radioBill').hide();
                $('#customerData').hide();
                $('#otherData').hide();
                $('#dealerData').hide();
                $('#invLabel').show();
                $('#txtEmailInvoice').rules("add", {
                    required: false
                });
            }
	});     
        
	addCounterSelectAction();
	addSaleTypeClickAction();
	
	$("#txtBirthdayCustomer").change(enableQuestionsDiv);
        $("#txtDepartureDate").change(enableQuestionsDiv);
	$("#selProduct").change(enableQuestionsDiv);
	
	loadCustomer('');
	
	loadOtherCustomer('');
	
        setupExtrasSection();
    
	initializeMainSalesForm('frmNewSale');	
		
	addProductSelectAction();

	$('[name="rdCurrency"]').bind('click',function(){
		executeProductPriceCalculation();
		$('#serial_cur').val($(this).attr('serial_cur'));
	});

	$("#txtArrivalDate").bind("change", function(e){
		doTravelDatesVerifications();
		displayQuestionsForAllExtras();
		enableQuestionsDiv();
	});
	$("#txtDepartureDate").bind("change", function(e){
		checkAndSetProductMaxCoverageTime();
		doTravelDatesVerifications();
	});
	$("#selCountryDestination").bind("change", function(e){
		loadCities(this.value,'Destination','ALL');
		validateDestination();
	});
	addEmissionDateBoxAction();

	/*** CUSTOMER PROMO SECTION ***/
	enablePromoValidationButton();
	
	$('[id^="rdCouponApply"]').each(function(){
		$(this).bind('click', function(){
			evaluateSelection($(this).val());
		})
	});	
	/*** CUSTOMER PROMO SECTION ***/
        
        $('[id^="rdInvoicing_"]').each(function(){
            $(this).bind('click', function(){
                if($(this).val() === 'PAX'){
                    processInvoicingDataValidations('DISABLE');
                    $("#invoiceDataContainer").empty();
                }else{
                    displayInvoicingForm();
                }
            })
	});
            /*VALIDATION*/
    $("#frmInvoice").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtTotal: {
                required: true
            },
            txtSubTotal: {
                required: true
            },
            txtSalesTotal: {
                required: true
            },
            txtInvoiceNumber: {
                required: true,				
                digits: true,
                remote: {
                    url: document_root+"rpc/remoteValidation/invoice/checkDocumentNumber.rpc",
                    type: "post",
                    data: {
                        number_inv: function() {
                            return $('#txtInvoiceNumber').val();
                        },
                        serial_man: function() {
                            return $('#hdnSerialMan').val();
                        },
                        type_dbm: function(){
                            return $('#hdnTypeDbc').val();
                        }
                    }
                }
            },
            txtDiscount: {
                required: true
            },
            txtDate:{
                required: true
            },
            txtDealer: {
                required: true
            },
            txtOtherDiscount:{
                percentage: true,
                max_value: 100
            }
        },
        messages: {
            txtOtherDiscount:{
                percentage: "El campo 'Otro descuento' solo admite valores de 0 a 100 con dos decimales.",
                max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
            },
            txtInvoiceNumber:{
                digits: "El campo facturas solo admite n&uacute;meros enteros.",
                remote: "El n&uacute;mero de factura ingresado ya existe."
            },
            hdnCheckInvoiceNumber:{
                remote: "El n&uacute;mero de factura ingresado ya existe para este representante."
            }
        },
        submitHandler: function(form) {		
            show_ajax_loader();
			if(validate_cost()){
				document_type = 'RUC';
          	
				if($('#customerData').css('display') != 'none'){
					$('[id^="rdDocumentType_"]').each(function(){
						if($(this).is(':checked')){
							document_type = $(this).val();
						}
});
				}            

				$.ajax({
					url: document_root+"rpc/remoteValidation/invoice/checkHolderDocumentNumber.rpc",
					type: "post",
					data: ({
						validate_document: $('#hdnValidateHolderDocument').val(),
						document_type: document_type,
						branch_id: function(){
							return $('#hdnDealerDocument').val();
						},
						customer_id: function(){
							return $('#txtCustomerDocument').val();
						},					
						invoice_to: function(){
							if($('#radio_customer').is(':checked')){
								return 'CUSTOMER';
							}else{
								return 'DEALER';
							}
						}
					}),
					dataType: 'json',
					success: function(valid_document) {
						hide_ajax_loader();

						if(valid_document){
							$('#btnRegister').attr('disabled', 'true');
							$('#btnRegister').val('Espere por favor...');

							form.submit();
						} else {
							alert('El documento ingresado no es v\u00E1lido. Por favor ingrese una CI o RUC v\u00E1lido.');
						}						
					}
				});
			}	
        }
    });
    /*END VALIDATION*/
	
    checkType();
    $('#selCountry').change(function(){
        loadCities($('#selCountry').val())
    });
    $('#selOtherCountry').change(function(){
        loadOtherCities($('#selOtherCountry').val())
    });

    if($('#hdnTypeDbc').val()){
        loadInvoiceNumber($('#hdnTypeDbc').val());
    }

    $("#selType").change(function(){
        personTypeBindings();
    });
	
    $('#txtInvoiceNumber').change(function(){
        $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);
    });
	
    $('input[name=bill_to_radio]').each(function(){
        $(this).bind('click',function(){
            if($(this).val()=='DEALER'){
                $('#dealerData').show();
                $('#customerData').hide();
                $('#otherData').hide();
            }else if($(this).val()=='OTHER') {
                $('#dealerData').hide();
                $('#customerData').hide();
                $('#otherData').show();
            }else{
                $('#dealerData').hide();
                $('#customerData').show();
                $('#otherData').hide();
            }
			
            $('#hdnCheckInvoiceNumber').val($('#hdnCheckInvoiceNumber').val()*-1);
			

        });

    });

    $('#txtCustomerDocument').bind('change',function(){
        var docCust= $(this).val();
        loadCustomer(docCust);
    });

    validate_cost();

    $("#txtOtherDiscount").bind("keyup", function(e){
        if(e.keyCode!=13){
            calculateTotal();
        }
    });
    
    $('[id^="OtherrdDocumentType"]').each(function(){
        $(this).bind('click', function(){
            $('#selTypeOther').val($(this).attr('person_type'));
            personTypeBindingsOther();
        });
    });
    $('[id^="rdDocumentType"]').each(function(){
        $(this).bind('click', function(){
            $('#selType').val($(this).attr('person_type'));
            personTypeBindings();
        });
    });
});

function validate_cost(){
	var alternSubTotal = parseFloat( $("#txtSubTotal").val() ) - ( parseFloat($("#txtSubTotal").val() ) * parseFloat($("#hdnComission").val()));
	
    if(parseFloat($("#txtSubTotal").val()) < parseFloat($("#h_TotalCost").val()) || alternSubTotal < parseFloat($("#h_TotalCost").val())){
        alert('El costo de la venta no puede ser Mayor al SubTotal.');
        $("#txtTotal").val('');
        $("#txtSubTotal").val('');
		return false;
    }
	
	return true;
}

function loadCities(countryID){
    if(countryID==""){
        $("#selCity option[value='']").attr("selected",true);
    }
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
        serial_cou: countryID,
        all: 'YES'
    });
}
function loadOtherCities(countryID){
    if(countryID==""){
        $("#selOtherCity option[value='']").attr("selected",true);
    }
    $('#cityOtherContainer').load(document_root+"rpc/loadCities.rpc",{
        serial_cou: countryID,
        all: 'YES'
    });
}

function loadInvoiceNumber(typeDbc){
    if($('#txtInvoiceNumber').attr('readonly')){
        $('#invoiceNumberContainer').load(document_root+"rpc/loadsInvoices/loadInvoiceNumber.rpc",{
            serial_man: $('#hdnSerialMan').val(),
            type_dbm: typeDbc
        });
    }
}

function addCustomerRules(){
    $('#txtCustomerDocument').rules("add", {
        required: true,
        alphaNumeric: true,
        messages:{
            alphaNumeric: "El campo 'Documento' solo acepta valores alfan&uacute;mericos."
        }
    });
    if($('#selType').val()){
        $('#selType').rules("add", {
            required: true
        });
    }
    $('#txtFirstName').rules("add", {
        required: true,
        invoice_holder: true,
        messages:{
            invoice_holder: "El campo 'Nombre' solo acepta valores alfan&uacute;mericos y el (.)"
        }
    });
    
    $('#txtPhone1').rules("add", {
        required: true,
        number: true,
        messages:{
            number: "El campo 'Tel&eacute;fono' solo acepta n&uacute;meros."
        }
    });
    $('#txtPhone2').rules("add", {
        number: true,
        messages:{
            number: "El campo 'Tel&eacute;fono Secundario' solo acepta n&uacute;meros."
        }
    });
    $('#selCountry').rules("add", {
        required: true
    });
    $('#selCity').rules("add", {
        required: true
    });
    if($('#selType').val()!='LEGAL_ENTITY'&&$('#hdnTypeDbc').val()!='LEGAL_ENTITY'){
        $('#txtLastName').rules("add", {
            required: {
				depends: function(element) {
					if($('#customerData').css('display') == 'none')
						return false;
					else
						return true;
				}
			},
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
    }
}

function removeCustomerRules(){
    $('#txtCustomerDocument').rules('remove');
    if($('#selType').val()){
        $('#selType').rules("remove");
    }
    $('#txtFirstName').rules('remove');
    $('#txtLastName').rules('remove');
    $('#txtPhone1').rules('remove');
    $('#txtPhone2').rules('remove');
    $('#selCountry').rules('remove');
    $('#selCity').rules('remove');
}

function checkType(){
    if($('#selType').val() == 'LEGAL_ENTITY' || $('#hdnTypeDbc').val() == 'LEGAL_ENTITY'){
        $('#lastNameContainer').hide();
        $('#txtLastName').rules('remove');
        $('#labelName').html('* Raz&oacute;n Social:');
    }else{
        $('#lastNameContainer').show();
        $('#txtLastName').rules("add", {
            required: {
				depends: function(element) {
					if($('#customerData').css('display') == 'none')
						return false;
					else
						return true;
				}
			},
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
		
        $('#labelName').html('* Nombre:');
    }
}



function evaluate_bill_to_radio(element){
    checkDealerChangeDiscountToComission(element.val());
	
    if(element.val()=='DEALER'){
        removeCustomerRules();
        checkType();
        loadInvoiceNumber('LEGAL_ENTITY');
    }else{
        addCustomerRules();
        checkType();
        if($('#hdnTypeDbc').val()){
            loadInvoiceNumber($('#hdnTypeDbc').val());
        }
    }
}

function personTypeBindings(){
    $('#hdnTypeDbc').val($("#selType").val());

    if($("#selType").val()){
        checkType();
    }
}
function personTypeBindingsOther(){
    if($("#selTypeOther").val()){
        if($('#selTypeOther').val() == 'LEGAL_ENTITY'){
        $('#lastNameOtherContainer').hide();
        $('#genderOtherDiv').hide();
        $('#txtLastOtherName').rules('remove');
        $('#labelOtherName').html('* Raz&oacute;n Social:');
    }else{
        $('#lastNameOtherContainer').show();
        $('#genderOtherDiv').show();
        $('#txtLastOtherName').rules("add", {
            required: {
				depends: function(element) {
					if($('#customerData').css('display') == 'none')
						return false;
					else
						return true;
				}
			},
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
		
        $('#labelOtherName').html('* Nombre:');
    }
    }
}


