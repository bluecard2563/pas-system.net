// JavaScript Document
function displayInvoicingForm(){
    $('#invoiceDataContainer').load(document_root + "rpc/loadsSales/displayInvoicingInfo.rpc", {
        countryID: '62'
    },function(){
        $('#txtCustomerDocument').bind('change',function(){
            var docCust= $(this).val();
            loadInvoiceCustomer(docCust);
        });
        
        processInvoicingDataValidations('ENABLE');
        personTypeBindings();
    })
}

function loadInvoiceCustomer(docCust){
    show_ajax_loader();
    
    $.ajax({
        url: document_root+"rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({
            txtDocument : docCust
        }),
        success: function(customerExists) {
            $('#customerInvoicingData').load(
                document_root+"rpc/loadsInvoices/loadCustomer.rpc",
                {
                    document_cus: docCust,
                    document_validation: function(){
                        return $('#hdnValidateHolderDocument').val();
                    }
                },function(){	
                    hide_ajax_loader();
                    
                    $('#selCountry').bind('change',function(){
                        loadCities(this.value,'Customer','ALL');
                    });
                    
                    $('#txtCustomerDocument').bind('blur',function(){
                        loadInvoiceCustomer($(this).val());
                    });
                    
                    personTypeBindings();
                    
                    if(customerExists == 'true'){
                        checkType();
                    }
                });
        }
    });
}

function personTypeBindings(){
    $('[id^="rdDocumentType_"]').each(function(){
        $(this).bind("click", function(){
            $('#hdnTypeDbc').val($(this).attr('person_type'));
            checkType();
        });
    });
}

function checkType(){
    if($('#hdnTypeDbc').val() == 'LEGAL_ENTITY'){
        $('#lastNameContainer').hide();
        $('#txtLastName').rules('remove');
        $('#labelName').html('* Raz&oacute;n Social:');
    }else{
        $('#lastNameContainer').show();
        $('#txtLastName').rules("add", {
            required: {
                        depends: function(element) {
                                if($('#customerData').css('display') == 'none')
                                        return false;
                                else
                                        return true;
                        }
            },
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
		
        $('#labelName').html('* Nombre:');
    }
}

function processInvoicingDataValidations(action){
    if(action === 'ENABLE'){
        $('#txtCustomerDocument').rules("add", {
            required: true,
            alphaNumeric: true,
            remote: {
                url: document_root+"rpc/remoteValidation/invoice/checkHolderDocumentNumber.rpc",
                type: "post",
                data: {
                    validate_document: $('#hdnValidateHolderDocument').val(),
                    document_type: function(){
                        var temp_document_type;
    
                        $('[id^="rdDocumentType_"]').each(function(){
                            if($(this).is(':checked')){
                                    temp_document_type = $(this).val();
                            }
                        });
                        
                        return temp_document_type;
                    },
                    customer_id: function(){
                            return $('#txtCustomerDocument').val();
                    }
                }
            },
            messages:{
                alphaNumeric: "El campo 'Documento Facturaci&oacute;n' solo acepta valores alfan&uacute;mericos.",
                remote: "El nro. de documento de facturacion es incorrecto."
            }
        });
        $('#txtFirstName').rules("add", {
            required: true,
            invoice_holder: true,
            messages:{
                invoice_holder: "El campo 'Nombre - Facturaci&oacute;n' solo acepta valores alfan&uacute;mericos y el (.)"
            }
        });
        $( "#txtAddress" ).rules( "add", {required: true, alphaNumeric: true});

        $('#txtPhone1').rules("add", {
            required: true,
            number: true,
            messages:{
                number: "El campo 'Tel&eacute;fono - Facturaci&oacute;n' solo acepta n&uacute;meros."
            }
        });
        $('#txtPhone2').rules("add", {
            number: true,
            messages:{
                number: "El campo 'Tel&eacute;fono Secundario - Facturaci&oacute;n' solo acepta n&uacute;meros."
            }
        });
        $( "#txtEmail" ).rules( "add", { 
            required: true, email:true
        });
        $('#selCountry').rules("add", {
            required: true
        });
        
        $('#selCity').rules("add", {
            required: true
        });
        
        if($('#hdnTypeDbc').val()!='LEGAL_ENTITY'){
            $('#txtLastName').rules("add", {
                required: {
                    depends: function(element) {
                        if($('#customerData').css('display') == 'none')
                                return false;
                        else
                                return true;
                    }
                },
                invoice_holder: true,
                messages:{
                    invoice_holder: "El campo 'Apellido - Facturaci&oacute;n' solo acepta valores alfan&uacute;mericos y el (.)"
                }
            });
        }
    }else{
        $('#txtCustomerDocument').rules('remove');
        if($('#selType').val()){
            $('#selType').rules("remove");
        }
        $('#txtFirstName').rules('remove');
        $('#txtLastName').rules('remove');
        $('#txtPhone1').rules('remove');
        $('#txtPhone2').rules('remove');
        $('#selCountry').rules('remove');
        $('#selCity').rules('remove');
    }
}