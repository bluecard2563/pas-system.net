//javascript

$(document).ready(function() {

    function RefreshTable() {
        show_ajax_loader();
        $( "#theTable" ).load( "https://www.pas-system.net/modules/sales/fPaymentRecord #theTable", function() {
            hide_ajax_loader();
        } );

    }

    $("#refresh-table").bind("click", RefreshTable);
    // hide_ajax_loader();
    // OR CAN THIS WAY
    //
    // $("#refresh-btn").on("click", function() {
    //    $( "#mytable" ).load( "your-current-page.html #mytable" );
    // });
});

