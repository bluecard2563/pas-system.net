//Javascript Document
var pager = Array();
$(document).ready(function() {
    if ($("#selCountry").val()) {
        loadManagersByCountry($("#selCountry").val());
    }
    $("#selCountry").bind("change", function(e) {
        loadManagersByCountry(this.value);
        clearDealerContainer();
    });
    $("#selType").bind("change", function(e) {
        clearDealerContainer();
    });
    $("#selCategory").bind("change", function(e) {
        clearDealerContainer();
    });
    $("#selResponsable").bind("change", function(e) {
        clearDealerContainer();
    });
    $("#selManager").bind("change", function(e) {
        clearDealerContainer();
    });
    $("#searchBranches").bind("click", function(e) {
        $("#alerts").html('');
        $("#alerts").css('display', 'none');
        evaluateFilters();
    });

    /*UPLOAD FILE DIALOG*/
    $("#uploading_file").dialog({
        bgiframe: true,
        autoOpen: false,
        autoResize: true,
        modal: true,
        stack: true,
        resizable: false,
        closeOnEscape: false,
        open: function(event, ui) {
            $(".ui-dialog-titlebar-close").hide();
        },
        title: "Espere por favor"
    });
    /*END UPLOAD FILE DIALOG*/
});
function loadManagersByCountry(countryID) {
    $('#responsableContainer').html("<select name='selResponsable' id='selResponsable'><option value=''>- Seleccione -</option></select>");
    $('#managerContainer').load(document_root + "rpc/loadMasiveManagersByCountry.rpc",
            {serial_cou: countryID},
    function() {
        $("#selManager").bind("change", function(e) {
            loadResponsablesByManager(this.value);
        });
        if ($("#selManager option").size() <= 2) {
            loadResponsablesByManager($("#selManager").val());
        }
    });
}

function loadResponsablesByManager(managerID) {
    $('#responsableContainer').load(document_root + "rpc/loadsSales/loadMasiveResponsablesByManager.rpc", {serial_mbc: managerID},
    function() {
        $("#selResponsable").bind("change", function(e) {
            clearDealerContainer();
        });
    });
}

function evaluateFilters() {
    var next = true;
    if ($("#selCountry").val() == '') {
        $("#alerts").append("<li><label class='error_validation'>Seleccione Almenos un Pa&iacute;s</label></li>");
        $("#alerts").css('display', 'block');
        next = false;
    }
    if ($("#selManager").val() == '') {
        $("#alerts").append("<li><label class='error_validation'>Seleccione Almenos un Representante</label></li>");
        $("#alerts").css('display', 'block');
        next = false;
    }
    if (next) {
        var serial_mbc = $("#selManager").val();
        var serial_usr = ($("#selResponsable").val() == '') ? false : $("#selResponsable").val();
        var dealer_cat = ($("#selCategory").val() == '') ? false : $("#selCategory").val();
        var serial_dlt = ($("#selType").val() == '') ? false : $("#selType").val();
        loadDealers(serial_mbc, serial_usr, dealer_cat, serial_dlt);
        $("#selManager").bind("change", function(e) {
            clearDealerContainer();
        });
    }
}

function loadDealers(serial_mbc, serial_usr, dealer_cat, serial_dlt) {
    $('#dealerContainer').load(document_root + "rpc/loadMasiveDealers.rpc",
            {
                serial_mbc: serial_mbc,
                serial_usr: serial_usr,
                dealer_cat: dealer_cat,
                serial_dlt: serial_dlt
            }, function() {
        $("#selDealer").bind("change", function(e) {
            loadBranches(this.value);
        });
        if ($("#selDealer option").size() <= 2) {
            loadBranches($("#selDealer").val());
        }
    });
}

function clearDealerContainer() {
    $('#dealerContainer').html('');
    clearSalesContainer();
}

function clearSalesContainer() {
    $('#salesContainer').html('');
}

function loadBranches(dealerID) {
    clearSalesContainer();
    if (dealerID == '') {
        dealerID = 'stop';
    }
    $("#branchContainer").load(document_root + "rpc/loadMasiveBranches.rpc", {dea_serial_dea: dealerID},
    function() {
        $("#selBranch").bind("change", function(e) {
            clearSalesContainer();
            loadSales(this.value);
        });
    });
}

function loadSales(branchID) {
    //get sales status registered and serial_inv NULL
    $('#salesContainer').load(document_root + "rpc/loadsSales/loadMasiveSales.rpc", {serial_dea: branchID, serial_cou: $("#selCountry").val()},
    function() {
        if ($('#accordion_container_1').length > 0) {

            /*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
            $("#frmChooseSales").validate({
                errorLabelContainer: "#alerts_travelers",
                wrapper: "li",
                onfocusout: false,
                onkeyup: false,
                rules: {
                    file_source: {
                        required: function() {
                            if ($('#fileContainer').css('display') == 'none')
                                return false;
                        }
                    },
                    cost_sal: {
                        required: function() {
                            if ($('#fixedContainer').css('display') == 'none')
                                return false;
                        }
                    }
                },
                submitHandler: function(form) {
                    $("#alerts").html('');
                    $("#alerts").css('display', 'none');
                    var equalto = true;
                    var taxes = '';
                    $(".salesChk").each(function(i) {
                        if ($(this).is(':checked')) {
                            if (taxes == '') {
                                taxes = $(this).attr('taxes');
                            } else {
                                if ($(this).attr('taxes') != taxes) {
                                    equalto = false;
                                    return;
                                }
                            }
                        }
                    });
                    if (equalto == true) {
                        form.submit();
                    } else {
                        $("#alerts").append("<li><label class='error_validation'>Para facturar en conjunto las ventas deben tener los mismos impuestos aplicados.</label></li>");
                        $("#alerts").css('display', 'block');
                    }
                }
            });
            /*FIN DE LA SECCI�N DE VALIDACIONES*/

            $('#uploadContainer').dialog({
                title: "Nueva Carga de Cliente",
                height: 300,
                width: 500,
                modal: true,
                stack: false,
                autoOpen: false,
                draggable: false,
                resizable: false,
                open: function() {
                    $('#alerts_travelers').hide();
                },
                buttons: {
                    'Cancelar': function() {
                        $('#file_source').val('');
                        $('#cost_sal').val('');
                        $('#customersMasive').html('');
                        $(this).dialog('close');
                    },
                    'Crear': function() {
                        if ($('#frmChooseSales').valid()) {
                            $('#frmChooseSales').submit();
                        }
                    }
                }
            });

            $('[id^="accordion_container_"]').each(function() {
                $(this).accordion({
                    autoHeight: false,
                    fillSpace: true,
                    navigation: true
                });
            });

            var total_pages = $('#hdnTotal_sales').val();
            for (var i = 1; i <= total_pages; i++) {
                pager[i] = new Pager('salesTable_' + i, 5, 'Siguiente', 'Anterior');
                pager[i].init(20); //Max Pages
                pager[i].showPageNav('pager[' + i + ']', 'pagingTable_' + i); //Div where the navigation buttons will be placed.
                pager[i].showPage(1); //Starting page
            }


            /* triggered when new subsale is clicked */
            $(".loadNewCustomers").bind("click", function() {
                $("#sal_serial_sal").val($(this).attr('sal_serial_sal'));
                $('#uploadContainer').dialog('open');
            });

            /* triggered when an existent subsale is clicked */
            $("[id^='loadCustomers_']").click(function() {
                //$('#uploadContainer').css("display", "none");
                $('#saleInfo').load(document_root + "rpc/loadSale.rpc", {
                    serial_sal: $(this).attr("sal_serial_sal")
                }, function() {
                    $("#saleInfo").dialog({
                        height: 350,
                        width: 1000,
                        modal: true,
                        draggable: false,
                        resizable: false,
                        buttons: {
                            'Cancelar': function() {
                                $(this).dialog('close');
                            }
                        }
                    });
                    $("#saleInfo").dialog('open');
                });

                $(".selectedRow").removeClass("selectedRow");
                $(this).addClass("selectedRow");

                $("#serial_sal").val($(this).attr('sal_serial_sal'));
            });

            /* triggered when an existent subsale is clicked */
            $("[id^='loadTravelers_']").click(function() {
                loadUpdateForm($(this).attr('sal_serial_sal'));
            });

            $("#loadPeopleContainer").dialog({
                height: 300,
                width: 550,
                modal: true,
                autoOpen: false,
                draggable: false,
                resizable: false,
                buttons: {
                    'Cancelar': function() {
                        $('#hdnSubSaleID').val('');
                        $(this).dialog('close');
                    },
                    'Registrar': function() {
                        $("#frmRegisterTravelers").validate({
                            errorLabelContainer: "#alerts_dialog",
                            wrapper: "li",
                            onfocusout: false,
                            onkeyup: false,
                            rules: {
                                travelers_list: {
                                    required: true
                                },
                                hdnSubSaleID: {
                                    required: true
                                }
                            }
                        });

                        if ($('#frmRegisterTravelers').valid()) {
                            $('#frmRegisterTravelers').submit();
                        }
                    }
                }
            });

            $('#showTravelersContainer').dialog({
                height: 500,
                width: 580,
                modal: true,
                autoOpen: false,
                draggable: false,
                resizable: false,
                buttons: {
                    'Salir': function() {
                        $(this).dialog('close');
                    }
                }
            });

            /*Styles*/
            $('#fixedContainer').css('display', 'none');

            /*Types of generate a new register.*/
            $('[id^="rdUploadType_"]').each(function(){
               $(this).bind('click', function(){
                   enableSelection($(this).val());
                   $('#customersMasive').html("");
               });
            });
        }
    });


}

function enableSelection(type) {
    if (type == 'FIXED') {
        $('#fixedContainer').show();
        $('#fileContainer').hide();
        $('#file_source').val('');
    } else { //UploadedFile
        $('#fixedContainer').hide();
        $('#fileContainer').show();
        $('#cost_sal').val('');
    }
}

function display_pages(pages) {
    var total_pages = $('#hdnTotalPages').val();

    if (pages == 'first') {
        $('.current').removeClass('current');
        $('#accordion_container_1').addClass('current');
    } else if (pages == 'last') {
        $('.current').removeClass('current');
        $('#accordion_container_' + total_pages).addClass('current');
    } else {
        $('.current').removeClass('current');
        $('#accordion_container_' + pages).addClass('current');
    }

    //PAGING STYLES
    $('.current_link').removeClass('current_link');
    $('#link_' + pages).addClass('current_link');
}

function loadUpdateForm(id) {
    var uploaded_people = $('[sal_serial_sal="' + id + '"]').attr('has_travelers');

    if (uploaded_people == 'NO') {
        $('#hdnSubSaleID').val(id);
        $("#loadPeopleContainer").dialog('open');
    } else {
        $('#showTravelersList').load(document_root + "rpc/loadsSales/loadTravelersMasive.rpc", {serial_sal: id}, function() {
            if ($("#customers_tale").length > 0) {
                pager = new Pager('customers_tale', 10, 'Siguiente', 'Anterior');
                pager.init(7); //Max Pages
                pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
                pager.showPage(1); //Starting page
            }
        });
        $('#showTravelersContainer').dialog('open');
    }
}