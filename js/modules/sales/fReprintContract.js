//Java Script
var pager;
var currentTable;
$(document).ready(function(){
	$("#frmReprintContract").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCardNumber:{
				required: {
					depends: function(element) {
						var display = $("#executiveContainer").css('display');
						if(display=='none' || $("#hdnSerial_cus").val()!='')
							return false;
						else
							return true;
					}
				},
				//digits:true
			}
		},
		messages: {
			txtCardNumber:{
				required:"Ingrese un n&uacute;mero de tarjeta o seleccione un cliente.",
				digits:"El campo 'N&uacute;mero de Tarjeta' acepta solo d&iacute;gitos.",
				remote: "El n&uacute;mero de tarjeta ingresado no pertenece a una Ejecutiva."
			}
		},
		submitHandler: function(form) {
			if($("#hdnSerial_cus").val()){
				searchRegisterCards();
			}
			else{
				searchCardDescription();
			}
			
		}
	});
	
	/*AUTOCOMPLETER*/
	$("#txtCustomerName").autocomplete(document_root+"rpc/autocompleters/loadCustomers.rpc",{
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnSerial_cus").val('');
			$("#txtCustomerName").val('');
		}else{
			$("#hdnSerial_cus").val(row[1]);
		}

	});
	/*END AUTOCOMPLETER*/
	
	//clear the card number textfield in case the autocompleter is used
	$("#txtCustomerName").bind("click",function(){
		$("#txtCardNumber").val('');
		$("#hdnSerial_cus").val('');
		$("#cardsContainer").html('');
		$("#masiveCardsContainer").html('');
	});

	//clear the txtCustomerName textfield in case a card number is typed
	$("#txtCardNumber").bind("click", function(){
		$("#txtCustomerName").val('');
		$("#hdnSerial_cus").val('');
		$("#cardsContainer").html('');
		$("#masiveCardsContainer").html('');
	});

//validation
});

//get the  cards where client could register travels, by client
function searchRegisterCards(){
	serial_cus=$('#hdnSerial_cus').val();
	$('#cardsContainer').load(document_root+"rpc/loadsSales/loadCardsByCustomer.rpc",{
		serial_cus: serial_cus
	},
	function(){
		//Paginator
		if($('#travelsTable').length>0){
			pager = new Pager('travelsTable', 10 , 'Siguiente', 'Anterior');
			pager.init(20); //Max Pages
			pager.showPageNav('pager', 'travelPageNavPosition'); //Div where the navigation buttons will be placed.
			pager.showPage(1); //Starting page
		}

		$("[id^='showCards']").each(function(){
			$(this).bind("click",function(){
				//alert($(this).attr('product'));
				$("#masiveCardsContainer").load(document_root+"rpc/loadsSales/loadMasiveCards.rpc",{
					serial_sal: $(this).attr('serialSal'),
					product: $(this).attr('product')
				},function(){
					/*Style dataTable*/
					if($('#cards_table').length>0) {
						currentTable = $('#cards_table').dataTable( {
							"bJQueryUI": true,
							"sPaginationType": "full_numbers",
							"oLanguage": {
								"oPaginate": {
									"sFirst": "Primera",
									"sLast": "&Uacute;ltima",
									"sNext": "Siguiente",
									"sPrevious": "Anterior"
								},
								"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
								"sZeroRecords": "No se encontraron resultados",
								"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
								"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
								"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
								"sSearch": "Filtro:",
								"sProcessing": "Filtrando.."
							},
							"sDom": 'T<"clear">lfrtip',
							"oTableTools": {
								"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
								"aButtons": []
							}

						} );
						$("[id^='sendEmail']", currentTable.fnGetNodes()).each(function(){
							$(this).bind("click",function(){
								var email=$(this).attr('email');
								var pdf=$(this).attr('pdf');
								if(email){

									if(confirm("Va a enviar el contrato al mail: "+email+"\nDesea continuar? ")){
										show_ajax_loader();
										$.ajax({
											url: document_root+"modules/sales/travelRegister/pPrintRegister/"+$(this).attr('serial')+"/2",
											success: function(data){
												$.ajax({
													url: document_root+ 'rpc/loadsSales/sendContractMail.rpc.php',
													type: "POST",
													data: {
														'email': email,
														'pdf': pdf
													},
													success: function(data){
														hide_ajax_loader();
														alert("El mail se ha enviado correctamente");
													}
												});
											}
										});
									}
								}else
								{
									alert("No se ha asignado un mail para el Cliente");
								}
							})
						})
					}
				});

			})
		})

	});	
}
//get the  the description of the card given in txtCardNumber
function searchCardDescription(){
	card_number=$('#txtCardNumber').val();
	$('#cardsContainer').load(document_root+"rpc/loadsSales/loadCardsByCustomer.rpc",{
		card_number: card_number
	},function(){
		$("[id^='showCards']").each(function(){
			$(this).bind("click",function(){
				//alert($(this).attr('product'));
				$("#masiveCardsContainer").load(document_root+"rpc/loadsSales/loadMasiveCards.rpc",{
					serial_sal: $(this).attr('serialSal'),
					product: $(this).attr('product')
				},function(){
					/*Style dataTable*/
					if($('#cards_table').length>0) {
						currentTable = $('#cards_table').dataTable( {
							"bJQueryUI": true,
							"sPaginationType": "full_numbers",
							"oLanguage": {
								"oPaginate": {
									"sFirst": "Primera",
									"sLast": "&Uacute;ltima",
									"sNext": "Siguiente",
									"sPrevious": "Anterior"
								},
								"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
								"sZeroRecords": "No se encontraron resultados",
								"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
								"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
								"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
								"sSearch": "Filtro:",
								"sProcessing": "Filtrando.."
							},
							"sDom": 'T<"clear">lfrtip',
							"oTableTools": {
								"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
								"aButtons": []
							}

						} );
						
						$("[id^='sendEmail']", currentTable.fnGetNodes()).each(function(){
							$(this).bind("click",function(){
								var email=$(this).attr('email');
								var pdf=$(this).attr('pdf');
								if(email){

									if(confirm("Va a enviar el contrato al mail: "+email+"\nDesea continuar? ")){
										show_ajax_loader();
										$.ajax({
											url: document_root+"modules/sales/travelRegister/pPrintRegister/"+$(this).attr('serial')+"/2",
											success: function(data){
												$.ajax({
													url: document_root+ 'rpc/loadsSales/sendContractMail.rpc.php',
													type: "POST",
													data: {
														'email': email,
														'pdf': pdf
													},
													success: function(data){
														hide_ajax_loader();
														alert("El mail se ha enviado correctamente");
													}
												});
											}
										});
									}
								}else
								{
									alert("No se ha asignado un mail para el Cliente");
								}
							})
						})
					}
				});

			})			
		})
	});
}