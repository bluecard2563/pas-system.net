//Java Script
$(document).ready(function(){
	show_ajax_loader();
	
	$("#txtBirthdayCustomer").change(enableQuestionsDiv);
    $("#txtDepartureDate").change(enableQuestionsDiv);
	$("#selProduct").change(enableQuestionsDiv);
	
	loadCustomer('');
	
	//INITIALIZE FORM
	$.validator.addMethod("adultsOnlyValidator", function(customerId, element) {
		return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"),'ADULTS', 'ADULTS');
	},"Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.");
	
	$.validator.addMethod("childrenOnlyValidator", function(customerId, element) {
		return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"),'CHILDREN', 'CHILDREN');
	},"Los acompa&ntilde;antes s&oacute;lo pueden ser menores.");
	
	$.validator.addMethod("seniorExtrasValidator", function(customerId, element) {
		return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"),'NO','SENIOR');
	},"Este producto no admite adultos mayores.");
	
	$("#frmNewMasiveSale").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			sale_type:{
				required: true
			},
			txtCardNum: {
				required: true
			},
			txtCodCounter: {
				required: true
			},
			txtEmissionPlace: {
				required: true
			},
			txtEmissionDate: {
				required: {
					depends: function(element) {
						var manual= $("#txtEmissionDate").attr("readonly");
						if(manual==true)
						return false;
						else
						return true;
					}
				}//,
				//dateLessThan: function(){return $('#hdnEmissionDate').val()}
			},
			txtNameCounter: {
				required: true
			},
            txtDealer: {
				required: true
			},
            txtDocumentCustomer: {
				required: true,
				alphaNumeric: true,
				adultsOnlyValidator: true,
		        childrenOnlyValidator: true,
		        seniorExtrasValidator: true
			},
            selType: {
                     required: true
            },
            txtNameCustomer: {
				required: true,
				textOnly: true
			},
            txtLastnameCustomer: {
				required: {
					depends: function(element) {
						return $("#selType") == 'PERSON';
					}
				},
				textOnly: true
			},
			selCountry: {
				required: true
			},
			selCityCustomer: {
				required: true
			},
			txtBirthdayCustomer: {
				required: {
					depends: function(element) {
						return $("#selType") == 'PERSON';
					}
				},
				date: true
			},
			 txtAddress: {
				required: true
			},
			txtPhone1Customer: {
				required: true,
				digits: true
			},
			txtPhone2Customer: {
				digits: true
			},
			txtCellphoneCustomer: {
				digits: true
			},
			txtMailCustomer: {
				//required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/customer/checkCustomerEmail.rpc",
					type: "POST",
					data: {
						serialCus: function() {
							return $("#serialCus").val();
						},
						txtMail: function(){
							 return $('#txtMailCustomer').val();
						}
					}
				}
			},
			txtRelative: {
				required: {
					depends: function(element) {
							return $("#divRelative").css('display') != 'none';
						}
				},
				textOnly:true
			},
            txtPhoneRelative: {
				required: {
					depends: function(element) {
							return $("#divRelative").css('display') != 'none';
						}
				},
                digits: true
			},
			txtManager: {
				required: {
					depends: function(element) {
							return $("#divManager").css('display') != 'none';
						}
				},
				textOnly:true
			},
            txtPhoneManager: {
				required: {
					depends: function(element) {
							return $("#divManager").css('display') != 'none';
						}
				},
                digits: true
			},
            txtDepartureDate: {
				required: true,
				date: true
			},
            txtArrivalDate: {
				required: true,
				date: true
			},
            selCountryDestination: {
				required: {
						depends: function(element) {
								return $("#divDestination").css('display') != 'none';
							}
					}
			},
			selCityDestination: {
				required: {
						depends: function(element) {
								return $("#divDestination").css('display') != 'none';
							}
					}
			},
            selProduct: {
				required: true
			},
			selCounter: {
				required: true
			}
		},
		messages: {
			txtCardNum:{
			  remote:'El n&uacute;mero de tarjeta ingresado ya esta vendido o no le corresponde.'
			},
			txtEmissionDate: {
				dateLessThan:'La fecha de emisi&oacute;n no puede ser mayor a la fecha actual.'
			},
			txtNameCustomer: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
     		},
			txtLastnameCustomer: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
     		},
			txtDocumentCustomer: {
				remote: "La identificaci&oacute;n ingresada ya existe.",
				alphaNumeric: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Identificaci&oacute;n'",
				adultsOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.",
	        	childrenOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser menores.",
	        	seniorExtrasValidator: "Este producto no admite adultos mayores."
			},
			txtMailCustomer: {
       			email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe en el sistema."
     		},
			txtBirthdayCustomer: {
       			date:"El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
			},
			txtDepartureDate: {
       			date:"El campo 'Fecha de Salida' debe tener el formato dd/mm/YYYY",
				lessOrEqualTo: 'La Fecha de Salida debe ser menor o igual que la Fecha de Llegada'
			},
			txtArrivalDate: {
       			date:"El campo 'Fecha de Llegada' debe tener el formato dd/mm/YYYY",
				greaterOrEqualTo: 'La Fecha de Llegada debe ser mayor o igual que la Fecha de Salida'
			},
			txtPhone1Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'"
			},
			txtPhone2Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono 2'"
			},
			txtCellphoneCustomer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Celular'"
			},
			txtRelative: {
				textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneRelative: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Familiar'"
			},
			txtManager: {
				textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneManager: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Gerente'"
			}
   		},
		submitHandler: function(form) {
			$('#btnRegistrar').attr('disabled', 'true');
			$('#btnRegistrar').val('Espere por favor...');
			if($("#selProduct option:selected").attr("flights_pro")=='0'){
				$('#hddDays').val($("#selDayNumber option:selected").attr("duration_ppc"));
			}
			form.submit();
	   }
	});
	
	setDefaultCalendar($("#txtDepartureDate"),'+0d','+100y');
	setDefaultCalendar($("#txtArrivalDate"),'+0d','+100y');
	//END INITIALIZE FORM

	$("#txtArrivalDate").bind("change", function(e){
		validateDates();
	});
	
	$("#selProduct").bind("change", function(e){
		checkAndSetProductMaxCoverageTime();
		validateDestination();
	});
	
	$("#txtDepartureDate").bind("change", function(e){
		checkAndSetProductMaxCoverageTime();
		validateDates();
	});
	$("#selCountryDestination").bind("change", function(e){
		loadCities(this.value,'Destination','ALL');
		validateDestination();
	});
	
	$('#selCounter').bind('change', function(){
		loadSaleInfo($(this).val(), 'YES', 'MASIVE');
	});
});