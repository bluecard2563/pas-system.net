// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmKitsReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                txtBeginDate: {
                    required: true,
                    date: true
                },
                txtEndDate: {
                    required: true,
                    date: true
                }
            },
            messages:{
                txtBeginDate: {
                    date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
                }
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
    });

    $('#btnGeneratePDF').bind("click", function(){
        if($("#frmKitsReport").valid()){
            $("#frmKitsReport").attr("action", document_root+"modules/sales/pKitsReportPDF.php");
            $("#frmKitsReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmKitsReport").valid()){
            $("#frmKitsReport").attr("action", document_root+"modules/sales/pKitsReportXLS.php");
            $("#frmKitsReport").submit();
        }
    });

    /*CALENDAR*/
    setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
    setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
    /*END CALENDAR*/

    /* CHANGE BINDS */
    $("#txtBeginDate").bind('change', function(){
            evaluateDates();
    });

    $("#txtEndDate").bind('change', function(){
            evaluateDates();
    });
    /* CHANGE BINDS */
});

function loadCountry(serial_zon){
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
                            loadCity(this.value);
                            loadManagersByCountry(this.value);
                            //loadProductByCountry(this.value);
            });

            if($('#selCountry option').size()<=2){
                    loadCity($('#selCountry').val());
                    loadManagersByCountry($('#selCountry').val());
                    //loadProductByCountry($('#selCountry').val());
            }
    });
}
function loadCity(serial_cou){
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: serial_cou, charge_country:0, opt_text: 'Todos'},function(){
            if($("#selCity option").size() <=2 ){
                    loadResponsibleByCity(this.value);
            }
            $('#selCity').bind("change", function(){
                    loadResponsibleByCity(this.value);
                    $('#selResponsible').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
            });
    });
}
function loadManagersByCountry(serial_cou){
    if(serial_cou){
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
                    $('#selManager').bind('change', function(){
            loadComissionists($(this).val());
        });
            });
    }else{
        loadComissionists('');
    }
}

function loadComissionists(managerId){
    if(managerId){
        $('#comissionistsContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: managerId},function(){
            $("#selComissionist").bind('change', function(){
            	loadDealers();
            });
        });
    }else{
    	$('#comissionistsContainer').html('Seleccione un representante');
    	loadDealers();
    }
}

function loadDealers(){
    if($('#selManager').val() && $('#selComissionist').val()){
        $('#comercializadorContainer').load(document_root+"rpc/loadDealersByManagerAndComissionistAndCity.rpc",{
        	city: $("#selCity").val(),
        	serial_mbc: $('#selManager').val(), 
        	serialComissionist: $('#selComissionist').val()},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
                        loadCounters('');
	            });
	        });
    }else{
    	$('#comercializadorContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{
                    dea_serial_dea: serial_dea,
                    opt_text: 'Todos'},
                function(){
                    $("#selBranch").bind('change', function(){
	                loadCounters($(this).val());
	            });
                });
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}

function loadResponsibleByCity(serial_cit){
    if(serial_cit!=''){
        $('#responsibleContainer').load(document_root+"rpc/reports/sales/loadResponsibleByCity.rpc",{serial_cit: serial_cit, reports: 'true'});
    }
}

function loadCounters(branchId){
    $('#countersContainer').load(document_root+"rpc/reports/sales/loadCountersByDealer.rpc", {serial_dea: branchId});
}

function evaluateDates(){
    var beginDate=$('#txtBeginDate').val();
    var endDate=$('#txtEndDate').val();

    if(beginDate && endDate){
        var new_days=parseFloat(dayDiff(beginDate,endDate));

        if(new_days < 0){
                alert('La fecha fin debe ser mayor a la fecha inicio.');
                $('#txtEndDate').val('');
        }else if(new_days > 365){
                alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
        }
    }
}


