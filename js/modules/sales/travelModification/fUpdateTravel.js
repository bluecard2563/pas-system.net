var aux;
var pager;
$(document).ready(function(){
	evaluateDates();
	if($('#customerContainer').length>0){
		loadCustomer($('#txtDocumentCustomer').val());
	}
	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
		loadCities(this.value,'Customer','ALL');
	});
	$("#selCountryDestination").bind("change", function(e){
		loadCities(this.value,'Destination','ALL');
		validateDestination($(this).val());
	});
	$("#txtArrivalDate").bind("change", function(e){
		evaluateDates();
	});
	$("#txtArrivalDate").bind("blur", function(e){
		evaluateDates();
	});
	$("#txtDepartureDate").bind("change", function(e){
		evaluateDates();
	});
	$("#txtDepartureDate").bind("blur", function(e){
		evaluateDates();
	});
	$("#selType").bind("change", function(e){
		loadCustomerForms(this.value);
		loadDialog();
	});
	/*CALENDARS*/
	setDefaultCalendar($("#txtBirthdayCustomer"),'-100y','+0d','-20y');
	setDefaultCalendar($("#txtDepartureDate"),'+0d','+100y');
	setDefaultCalendar($("#txtArrivalDate"),'+0d','+100y');
	/*END CALENDARS*/
	$("#frmModifyRegisterTravel").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDocumentCustomer: {
				required: true,
				alphaNumeric: true
			},
			selType: {
				required: true
			},
			txtNameCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			txtNameCustomer1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			txtLastnameCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtBirthdayCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				date: true
			},
			txtAddress: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtAddress1: {
				required: {
					depends: function(element) {
						var display = $("#divName2").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhone1Customer: {
				required: true,
				digits: true
			},
			txtPhone2Customer: {
				digits: true
			},
			txtCellphoneCustomer: {
				digits: true
			},
			txtMailCustomer: {
				//required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/customer/checkCustomerEmail.rpc",
					type: "POST",
					data: {
						serialCus: function() {
							return $("#serialCus").val();
						},
						txtMail: function(){
							return $('#txtMailCustomer').val();
						}
					}
				}
			},
			txtRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
			txtManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneManager: {
				required: {
					depends: function(element) {
						var display = $("#divManager").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
			txtDepartureDate: {
				required: true,
				date: true
			},
			txtArrivalDate: {
				required: true,
				date: true
			},
			selCountryDestination: {
				required: true
			},
			selCityDestination: {
				required: true
			},
			all_answered: {
				required: {
					depends: function(element) {
						if($('#hdnQuestionsExists').val()=='1' && $('#should_check_questions').val()=='1')
							return true;
						else
							return false;
					}
				}
			}
		},
		messages: {
			txtNameCustomer: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
			},
			txtNameCustomer1: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
			},
			txtLastnameCustomer: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
			},
			txtDocumentCustomer: {
				remote: "La identificaci&oacute;n ingresada ya existe.",
				alphaNumeric: "S&oacute;lo se aceptan caracteres alpha - num&eacute;ricos en el campo 'Identificaci&oacute;n'"
			},
			txtMailCustomer: {
				email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe en el sistema."
			},
			txtBirthdayCustomer: {
				date:"El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
			},
			txtDepartureDate: {
				date:"El campo 'Fecha de Salida' debe tener el formato dd/mm/YYYY",
				lessOrEqualTo: 'La Fecha de Salida debe ser menor o igual que la Fecha de Llegada'
			},
			txtArrivalDate: {
				date:"El campo 'Fecha de Llegada' debe tener el formato dd/mm/YYYY",
				greaterOrEqualTo: 'La Fecha de Llegada debe ser mayor o igual que la Fecha de Salida'
			},
			txtPhone1Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'"
			},
			txtPhone2Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono 2'"
			},
			txtCellphoneCustomer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Celular'"
			},
			txtRelative: {
				textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneRelative: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Familiar'"
			},
			txtManager: {
				textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneManager: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Gerente'"
			},
			all_answered: {
				required:"Debe contestar todas la preguntas de adulto mayor para poder continuar'"
			}
		},
		submitHandler: function(form) {
			if($('#serialCus').length>0){
				$.ajax({
					url: document_root+"rpc/remoteValidation/sales/travelRegister/checkCrossingTravels.rpc",
					type: "post",
					data: ({
						serial_cus : $('#serialCus').val(),
						start_dt:$('#txtDepartureDate').val(),
						end_dt:$('#txtArrivalDate').val(),
						exclude_ids:$('#current_trl').val()
						}),
					success: function(data) {
						if(data=='true'){
							$('#btnRegisterTravel').attr('disabled', 'true');
							$('#btnRegisterTravel').val('Espere por favor...');
							form.submit();
						}
						else
							alert('El cliente ya tiene un viaje registrado entre las fechas dadas.');
					}
				});
			}else{

				$('#btnRegisterTravel').attr('disabled', 'true');
				$('#btnRegisterTravel').val('Espere por favor...');
				form.submit();
			}
		}
	});
	/*ELDER QUESTION's*/
	$('#divOpenDialog').hide();
	$('#openDialog').click(function(){
		$('#dialog').dialog('open');
	});
	if($("#txtBirthdayCustomer").length>0){
		$("#txtBirthdayCustomer").blur(loadDialog);
		$("#txtBirthdayCustomer").change(loadDialog);
		$("#txtDepartureDate").blur(loadDialog);
		$("#txtDepartureDate").change(loadDialog);
	}

	/*Dialog*/
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 500,
		width: 700,
		modal: true,
		buttons: {
			'Guardar': function() {
				var formValid=true;
				$('[id^="question_"]').each(function(){
					if($(this).val()==''){
						formValid=false;
						return;
					}
				});

				if(formValid){
					addQuestions();
					$('#divOpenDialog').show();
					$(this).dialog('close');
					$('#all_answered').val('1');
					aux=1;
				}else{
					alert('Por favor responda todas las preguntas!');
					$('#all_answered').val('');
				}
			/*END VALIDATION*/
			},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		}
	});
/*End Dialog*/
});



/*
 *@Name: loadCustomerForms
 *@Description: loads a new form if the customer is a Legal Entity
 **/
function loadCustomerForms(typeCus){
	if(typeCus=='LEGAL_ENTITY'){
		$("#divName").hide();
		$("#divName2").show();
		$("#divAddress").hide();
		$("#divAddress2").show();
		$("#divRelative").hide();
		$("#divManager").show();
		$("#txtBirthdayCustomer").val('');
	}

	if(typeCus=='PERSON'){
		$("#divName").show();
		$("#divName2").hide();
		$("#divAddress").show();
		$("#divAddress2").hide();
		$("#divRelative").show();
		$("#divManager").hide();
	}
}

/*
 *@Name: loadDialog
 *@Description: Evaluates if the customer is older than a specific age according
 *              to a QUESTION GLOBAL PARAMETER.
 **/
function loadDialog(){
	if($("#divName").css('display')!='none'){
		var curr =new Date(Date.parse(dateFormat($('#txtDepartureDate').val())));
		curr.setFullYear(curr.getFullYear() - $('#hdnParameterValue').val());
		var dob =new Date(Date.parse(dateFormat($('#txtBirthdayCustomer').val())));
		var dialogOpen=0;
		if($('#hdnParameterCondition').val()=='<'){
			if((curr-dob)<0){
				dialogOpen=1;
			}
		}
		else if($('#hdnParameterCondition').val()=='<='){
			if((curr-dob)<=0){
				dialogOpen=1;
			}
		}
		else if($('#hdnParameterCondition').val()=='>'){
			if((curr-dob)>0){
				dialogOpen=1;
			}
		}
		else if($('#hdnParameterCondition').val()=='>='){
			if((curr-dob)>=0){
				dialogOpen=1;
			}
		}
		else if($('#hdnParameterCondition').val()=='='){
			if((curr-dob)==0){
				dialogOpen=1;
			}
		}
	}
	if(dialogOpen==1){
		if($('#allow_senior').val()=='1'){
			$('#should_check_questions').val('1');
			$('#hdnQuestionsFlag').val('1');
			if($('#hdnQuestionsExists').val()==1){
				$('#dialog').dialog('open');
			}
			else if($('#hdnQuestionsExists').val()==0){
				alert("No existen preguntas registradas en el sistema");
			}

			if($("#txtDepartureDate").val()!=''){
				$('#divOpenDialog').show();
				if(aux==1){
					loadAnswers();
				}
				else{
					unloadAnswers();
				}
			}
		}else{
			alert('Este producto no permite viajar a adultos mayores.');
			$("#txtDepartureDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}


	}else{
		$('#should_check_questions').val('');
		$('#divOpenDialog').hide();
		$('#hdnQuestionsFlag').val('0');

	}

}

/*
 *@Name: addQuestions
 *@Description: Adds an answer for each question displayed.
 **/
function addQuestions(){
	var x;
	x=$("#hiddenAnswers");
	x.html("");
	$("[id^='question_']").each(function (i) {
		x.append('<input type="hidden" name="'+this.id+'" id="'+this.id+'" value="'+this.value+'" />');
	});
}

/*
 *@Name: loadAnswers
 *@Description: loads answers from DB.
 **/
function loadAnswers(){
	$("select[id^='question_']").each(function (i) {
		$(this).val($(":hidden[id^='question_"+(i+1)+"']").val());
	});
}
/*
 **@Name: unloadAnswers
 *@Description: unloads answers from DB.
 **/
function unloadAnswers(){
	$("select[id^='question_']").each(function (i) {
		$(this).val('');
	});
}
/*
 *@Name: loadCustomer
 *@Description: Verifies if the document entered is from a customer already registered and displays his/her information
 *@Params: customerID = Identification number of the customer
 **/
function loadCustomer(customerID){
	var docCust= customerID;
	$.ajax({
		url: document_root+"rpc/remoteValidation/customer/checkCustomerDocument.rpc",
		type: "post",
		data: ({
			txtDocument : docCust
		}),
		success: function(data) {
			if(data=='false'){
				aux=1;
				$('#customerContainer').load(
					document_root+"rpc/loadsSales/loadCustomer.rpc",
					{
						document_cus: docCust
					},function(){
						$('#selCountry').bind('change',function(){
							loadCities(this.value,'Customer','ALL');
						});
						$('#txtBirthdayCustomer').rules("remove");
						$('#txtDocumentCustomer').bind('blur',function(){
							loadCustomer($(this).val());
						});
					});
			}
			if(data=='true'){
				aux=0;
				$('#customerContainer').load(
					document_root+"rpc/loadsSales/loadCustomer.rpc",
					{
						document_cus: docCust
					},function(){
						$('#selCountry').bind('change',function(){
							loadCities(this.value,'Customer','ALL');
						});
						setDefaultCalendar($("#txtBirthdayCustomer"),'-100y','+0d','-20y');
						$('#txtBirthdayCustomer').rules('add',{
							required: {
								depends: function(element) {
									var display = $("#divName").css('display');
									if(display=='none')
										return false;
									else
										return true;
								}
							},
							date: true
						});
						$('#txtDocumentCustomer').bind('blur',function(){
							loadCustomer($(this).val());
						});
						if($("#txtBirthdayCustomer").length>0){
							$("#txtBirthdayCustomer").blur(loadDialog);
							$("#txtBirthdayCustomer").change(loadDialog);
						}
						$("#selType").bind("change", function(e){
							loadCustomerForms(this.value);
							loadDialog();
						});
					});
			}
		}
	});
}
function loadCities(countryID,selectID, ALL){
	var container;
	if(selectID == "Destination") {
		container = $('#cityDestinationContainer');
	}
	else{
		container = $('#cityContainerCustomer');
	}
	container.load(
		document_root+"rpc/loadCities.rpc",
		{
			serial_cou: countryID, 
			serial_sel: selectID, 
			all: ALL
		});

}
/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var expirationDate =new Date(Date.parse(dateFormat($('#end_dt').html())));
	var startDate =new Date(Date.parse(dateFormat($('#start_dt').html())));
	var today = new Date(Date.parse(dateFormat($('#today').val())));
	if($("#txtArrivalDate").val()!=''){
		var arrivalDate =new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
		if(arrivalDate>expirationDate){
			alert("La Fecha de Llegada no puede superar la fecha de expiraci\u00F3n de la tarjeta.");
			$("#txtArrivalDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}

	}
	if($("#txtDepartureDate").val()!=''){
		var departureDate =new Date(Date.parse(dateFormat($('#txtDepartureDate').val())));
		if(departureDate>expirationDate){
			alert("La Fecha de Partida no puede superar la fecha de expiraci\u00F3n de la tarjeta.");
			$("#txtArrivalDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}else{
			if(departureDate<startDate){
				alert("La Fecha de Partida no puede ser inferior a la fecha de inicio de cobertura de la tarjeta.");
				$("#txtDepartureDate").val('');
				$("#txtDaysBetween").html('');
				$("#hddDays").val('');
			}else{
				if(departureDate<today){
					alert('La Fecha de partida no puede ser menor al d\u00EDa de hoy.');
					$("#txtDepartureDate").val('');
					$("#txtDaysBetween").html('');
					$("#hddDays").val('');
				}
			}
		}
	}
	if($("#txtArrivalDate").val()!='' && $("#txtDepartureDate").val()!=''){
		if(departureDate>arrivalDate){
			alert("La Fecha de Partida no puede superar la fecha de Retorno.");
			$("#txtDepartureDate").val('');
			$("#txtDaysBetween").html('');
			$("#hddDays").val('');
		}else{
			if(parseInt(dayDiff($("#txtDepartureDate").val(),$("#txtArrivalDate").val()))>parseInt($('#days_available').html())){
				alert("Los d\u00EDas de viaje no pueden superar el n\u00FAmero de d\u00EDas disponibles.");
				$("#txtArrivalDate").val('');
				$("#txtDaysBetween").html('');
				$("#hddDays").val('');
			}else{
				updateTravelDays();
			}
		}
	}else{
		$("#hddDays").val('');
	}
}
function updateTravelDays(){
	var days=dayDiff($("#txtDepartureDate").val(),$("#txtArrivalDate").val());
	$('#txtDaysBetween').html(days);
	$("#hddDays").val(days);
}

function validateDestination(serial_cou){
	var serial_pxc = $("#hdnSerial_pxc").val();
	
	if(serial_pxc && serial_cou){
		$.ajax({
			url: document_root+"rpc/remoteValidation/sales/checkDestination.rpc",
			type: "post",
			data: ({serial_pxc : serial_pxc, serial_cou: serial_cou}),
			dataType: "json",
			success: function(valid_destination) {
				if(!valid_destination){
					$('#selCountryDestination').val('');
					$('#selCityDestination').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					alert('El producto seleccionado no puede utilizarse para el destino actual.');
				}
			}
		});
	}	
}