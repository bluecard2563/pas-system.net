/*
File:fChooseCard.js
Author: Nicolas Flores
Creation Date:19/05/2010
*/
var pager;
//function to void a travel row
function voidTravel(serial_trl){
	if(serial_trl){
		if(confirm('Confirma que desea anular este registro?')){
			$.ajax({
					url: document_root+"rpc/loadTravelModification/voidRegister.rpc",
					type: "post",
					data: ({serial_trl : serial_trl}),
					success: function(data){
						if(data=='true'){
							$('#success_message').html('Anulaci&oacute;n exitosa!');
							$('#success_message').css('display','block');
							$('#error_message').html('');
							$('#error_message').css('display','none');
							searchCardDescription();
						}else{
							$('#success_message').html('');
							$('#success_message').css('display','none');
							$('#error_message').html('Error al anular el Registro.');
							$('#error_message').css('display','block');
						}
					}
			});
	  }
	}
}

//get the  the description of the card given in txtCardNumber
function searchCardDescription(){
		card_number=$('#txtCardNumber').val();
		$('#registersContainer').load(document_root+"rpc/loadTravelModification/loadTravelsDetails.rpc",{
		card_number: card_number
	},function(){
		 //Paginator
			if($('#travelsTable').length>0){
				pager = new Pager('travelsTable', 10 , 'Siguiente', 'Anterior');
				pager.init(7); //Max Pages
				pager.showPageNav('pager', 'pageTravelPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); //Starting page
				$('.void_travel').each(function(e){
					$(this).bind("click",function(e){
						voidTravel($(this).attr('serial_trl'));
				});
			})
		}
	});
}

$(document).ready(function(){
	//clear the txtCustomerName textfield in case a card number is typed
	$("#txtCardNumber").bind("keypress", function(e){
		$("#registersContainer").html('');
	});
	//validation
	$("#frmChooseRegister").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCardNumber:{
				required: true,
				digits:true,
				remote: {
					url: document_root+"rpc/remoteValidation/sales/travelModification/checkCardNumberRegister.rpc",
					type: "post"
				}
			}
		},
		messages: {
			txtCardNumber:{
				required:"Ingrese un n&uacute;mero de tarjeta.",
				digits:"El campo 'N&uacute;mero de Tarjeta' acepta solo d&iacute;gitos.",
				remote: "El n&uacute;mero de tarjeta ingresado no posee viajes registrados que se puedan modificar."
			}
		},
		submitHandler: function(form) {
				$('#error_message').html('');
				$('#error_message').css('display','none');
				$('#success_message').html('');
				$('#success_message').css('display','none');
				searchCardDescription();
		}
	});
});