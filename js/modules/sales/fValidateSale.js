//Java Script
$(document).ready(function(){
    var card_number;
    var verification_code;
    
    $('#btnRegister').bind('click', function(){
       card_number = $('#txtCardNumber').val();
       verification_code = $('#txtVerificationCode').val();
       
       show_ajax_loader();
       
       if(card_number != '' && verification_code != ''){
           $('#verificationContainer').load(document_root + 'rpc/remoteValidation/sales/validContract.rpc', 
            {
                card_number_sal: card_number,
                code: verification_code
            }, function(){
                hide_ajax_loader();
            });
       }else{
		   hide_ajax_loader();
		   alert('Ingrese los datos requeridos antes de continuar.');
	   }
    });
});