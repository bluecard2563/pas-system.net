// JavaScript Document
var pager;
$(document).ready(function(){
	/* MAIN FORM VALIDATION */
	$("#frmMultipleSales").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCounter: {
				required: true
			},
			selProduct:{
				required: true
			},
			hdnLoadedClients: {
				required: true
			}
		},
		messages: {
			flMassiveList:{
				accept: "El campo 'Archivo' acepta s\u00F3lo extensiones 'xls' y 'xlsx'"
			}
		},
		submitHandler: function(form){
			$('#btnConfirm').attr('disabled', 'true');
			$('#btnConfirm').val('Espere por favor...');
			form.submit();
		}
	});
	/* END VALIDATION */

	/* REASIGN SALE SECTION */
	$('#chkReasignSale').bind('click',function(){
		loadReassignSale();
	});
	/* END SECTION */

	/* BINDING FOR MULTI-COUNTER SELECT */
	$('#selMultipleCounter').bind('change',function(){
		if($('#selMultipleCounter').val()==''){
			$('#hdnCounter_id_toUse').val('');
		}else{
			$('#hdnCounter_id_toUse').val($('#selMultipleCounter').val());
			loadProducts($('#hdnCounter_id_toUse').val());
			loadSaleInfo($('#hdnCounter_id_toUse').val());
		}
	});
	/* END MULTI-COUNTER */
	
	enableProductsSelect();
});

/**
 *@Name: loadReassignSale
 *@Description: Choose another counter to reassign the sale. s
 */
function loadReassignSale(){
	if($('#chkReasignSale').is(':checked')){
		$('#selProduct').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		
		if($('#ownCounterInfo').length>0){
			$('#ownCounterInfo').hide();
		}

		$('#counterInfoContainer').html('');
		$('#saleInfoContainer').html('');
		
		if($('#selMultipleCounter').length>0){
			$("#selMultipleCounter option[value='']").attr("selected",true);
			$('#selMultipleCounter').attr('disabled','disabled');
		}		
		
		$('#counterInfoContainer').load(document_root+"rpc/loadsSales/loadReassignSale.rpc",function(){
			$("#selCountryReassign").bind("change", function(e){
				  loadCities(this.value,'Reassign','ALL');
				  $('#hdnCntSerial_cou').val(this.value);
			});
		});
		$('#hdnCounter_id_toUse').val('');
	}else{
		$('#counterInfoContainer').html('');
		$('#saleInfoContainer').html('');
		
		if($('#selMultipleCounter').length>0){
			$('#selMultipleCounter').removeAttr('disabled');
		}

		if($('#ownCounterInfo').length>0){
			$('#ownCounterInfo').show();
		}
		
		if($('#hdnCurrent_counter_id').val()){
			$('#hdnCounter_id_toUse').val($('#hdnCurrent_counter_id').val());
		}else{
			$('#hdnCounter_id_toUse').val($('#selMultipleCounter').val());
		}

		loadProducts($('#hdnCounter_id_toUse').val());
	}
}

/*
 * @name: loadCitites
 * @Description: Load all the cities for a specific country.
 */
function loadCities(countryID,selectID, ALL){
	if(selectID == "Destination") {
		var container = $('#cityDestinationContainer');
	} else {
		if(selectID == "Reassign"){
			var container = $('#cityContainerReassign');
		} else {
			var container = $('#cityContainerCustomer');
		}
	}
	container.load(
	document_root+"rpc/loadCities.rpc",
	{serial_cou: countryID, serial_sel: selectID, all: ALL},
	function(){
			$("#selCityReassign").bind("change", function(e){loadDealer(this.value);});
			if(selectID=='Reassign'){
				if($("#selCityReassign").find('option').size()==2){
					loadDealer($('#selCityReassign').val());
				}
			}
			if(selectID == "Destination") {
				$("#selCityDestination option[name='General']").attr("selected", true);
			}
		});
}

/*
 *@Name: loadDealer
 *@Description: Charges all dealers
 **/
function loadDealer(cityID){
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$(":enabled#selCounter").find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

	if(cityID==""){
		$("#selDealer option[value='']").attr("selected",true);
		$("#selBranch option[value='']").attr("selected",true);
		$(":enabled#selCounter option[value='']").attr("selected",true);
		cityID='stop';
	}

	$('#dealerContainerReassign').load(document_root+"rpc/loadDealers.rpc",
		{
			serial_cit: cityID,
			dea_serial_dea: 'NULL'
		},function(){
			$("#selDealer").bind("change", function(e){loadBranch(this.value);});
			if($("#selDealer").find('option').size()==2){
				loadBranch($('#selDealer').val());
			}
		}
	);
}

/*
 *@Name: loadBranch
 *@Description: Charges branches
 **/
function loadBranch(dealerID){
	$(":enabled#selCounter").find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

	if(dealerID==""){
		$("#selBranch option[value='']").attr("selected",true);
		$(":enabled#selCounter option[value='']").attr("selected",true);
		dealerID='stop';
	}

	$('#branchContainerReassign').load(document_root+"rpc/loadDealers.rpc",
		{dea_serial_dea: dealerID},
		function(){
			$("#selBranch").bind("change", function(e){loadCounter(this.value);});
			if($('#selBranch').find('option').size()==2){
				loadCounter($('#selBranch').val());
			}
	});

}

/*
 *@Name: loadCounter
 *@Description: Charges counters
 **/
function loadCounter(branchID){
	if(branchID==""){
		$(":enabled#selCounter option[value='']").attr("selected",true);
		branchID='stop';
	}
	
	$('#counterContainerReassign').load(document_root+"rpc/loadCounter.rpc",{serial_dea: branchID},function(){
		$("#selCounter").bind('change',function(e){
			var counterID=this.value;
			
			if(counterID==''){
				$('#hdnCounter_id_toUse').val('');
			}else{
				$('#hdnCounter_id_toUse').val(counterID);
				loadSaleInfo(counterID);
				loadProducts(counterID);
			}

			$('#productsContainer').load(document_root+"rpc/loadsSales/loadProducts.rpc",{counter: counterID});
		});
	});
}

/*
 *@Name: loadSaleInfo
 *@Description: Load info for a Sell
 **/
function loadSaleInfo(counterID){
    $('#saleInfoContainer').load(document_root+"rpc/loadsSales/loadMultipleSaleInfo.rpc",{serial_counter: counterID},function(){
		$('#hdnCounter_id_toUse').val($('#hdn_counter_remote_id').val());
	});
}

/*
 *@Name: loadSaleInfo
 *@Description: Load info for a Sell
 **/
function loadProducts(counterID){
    $('#productContainer').load(document_root+"rpc/loadsSales/loadProducts.rpc",{
		counter: counterID,
		multiple_sales: 'YES'
	}, function(){
		enableProductsSelect()
	});
}

function enableProductsSelect(){
	$('#selProduct').bind('change', function(){
		clearCustomerList();	
		loadServices($('#product_' + $(this).val()).attr('has_services'));
		
		if($(this).value!=''){
			$('#chooseFileDiv').css('display', 'block');
			$('#messageFileDiv').css('display', 'none');
		}else{
			$('#chooseFileDiv').css('display', 'none');
			$('#messageFileDiv').css('display', 'block');
		}
		
		/* UPLOADING FILE */
		new AjaxUpload('flMassiveList', {
			action: document_root+'rpc/loadsSales/multiSales/uploadMultiSalesFile.rpc',
			name: 'uploadFile',
			data: {
				serial_pbd: $(this).val()
			},
			onSubmit : function(file, ext){
				show_ajax_loader();
				if($('#selProduct').val()==''){
					hide_ajax_loader();
					alert('Seleccione un producto por favor.');
					return false;
				}
			},
			onComplete : function(file, response){
				hide_ajax_loader();
				var parsed_response = json_parse(response);
                console.log(parsed_response);

				switch(parsed_response[0]){
					case 'success':
						$('#cards_container').load(document_root+'rpc/loadsSales/multiSales/generateCustomersTable.rpc',
						{
							customers_list: response,
							services: function(){
								var services = new Array();
	
								$('[id^="chkService_"]').each(function(){
									if($(this).is(':checked')){
										array_push(services, $(this).val());
									}		
								});
								
								return serialize(services);
							}
						},
						function(){
								var items=parseInt($('#hdnTotalItems').val());
								var pages=parseInt((items/10)+1);
		
								if(items>0){
									pager = new Pager('customers_table', 10 , 'Siguiente', 'Anterior');
									pager.init(pages); //Max Pages
									pager.showPageNav('pager', 'paging_controls'); //Div where the navigation buttons will be placed.
									pager.showPage(1); //Starting page
		
									$('#hdnLoadedClients').val('1');
								}
						});
						break;
					case 'wrong_extension':
						$('#cards_container').html('');
						alert('Se admite s\u00F3lo archivos con extensiones .xls o .xlsx.');
						break;
					case 'upload_error':
						$('#cards_container').html('');
						alert('Hubo un error al subir el archivo. Lo sentimos.');
						break;
					case 'customer_secuence_error':
						$('#cards_container').html('');
						alert('Existe un error en la secuencia de clientes del archivo. Por favor rev\u00EDselo.');
						break;
					case 'extras_not_allowed':
						$('#cards_container').html('');
						alert('El producto seleccionado no admite adicionales.');
						break;
					case 'customer_extras_secuence_error':
						$('#cards_container').html('');
						alert('Existe un error en la secuencia de los adicionales en el archivo. Por favor rev\u00EDselo.');
						break;
					case 'wrong_name_format':
						$('#cards_container').html('');
						alert('El formato para el nombre/apellido de los clientes es incorrecto. Por favor rev\u00EDselo.');
						break;
					case 'wrong_numbers_format':
						$('#cards_container').html('');
						alert('El formato para los numeros de tel\u00E9fono de los clientes es incorrecto. Por favor rev\u00EDselo.');
						break;
					case 'seniors_not_allowed':
						$('#cards_container').html('');
						alert('No se admiten adultos mayores como adicionales en esta tarjeta. Por favor revise el archivo.');
						break;
					case 'only_children_allowed_for_extras':
						$('#cards_container').html('');
						alert('Solamente se admiten menores como adicionales en esta tarjeta. Por favor revise el archivo.');
						break;
					case 'only_adults_allowed_for_extras':
						$('#cards_container').html('');
						alert('Solamente se admiten adultos como adicionales en esta tarjeta. Por favor revise el archivo.');
						break;
					case 'no_file_found':
						$('#cards_container').html('');
						alert('No se pudo encontrar el archivo a cargar. Lo sentimos.');
						break;
					case 'no_price_found':
						$('#cards_container').html('');
						alert('No se pudo encontrar un precio para uno de los pasajeros. El viaje es muy extenso.');
						break;
					case 'duplicated_spouse':
						$('#cards_container').html('');
						alert('Existe un registor duplicado de "Conyuge" para un cliente. Revise el archivo.');
						break;
					case 'extras_not_allowed':
						$('#cards_container').html('');
						alert('Este producto no admite m\u00E1s adicionales. Revise el archivo.');
						break;
					case 'unknown_relationship':
						$('#cards_container').html('');
						alert('Existe una relaci\u00F3n con el titular desconocida. Revise el archivo.');
						break;
					case 'wrong_travel_dates':
						$('#cards_container').html('');
						alert('Las fechas de viaje son incorrectas. Revise el archivo.');
						break;
					case 'document_missing':
						$('#cards_container').html('');
						alert('Existe un pasajero sin su documento.');
						break;
					case 'birthdate_missing':
						$('#cards_container').html('');
						alert('Existe un pasajero sin su fecha de nacimiento.');
						break;
					case 'customer_city_unknown':
						$('#cards_container').html('');
						alert('Existe un pasajero sin su ciudad de origen. Revise el archivo.');
						break;
					default:
						$('#cards_container').html('');
						alert('Hubo errores en la subida del archivo.');
						break;
				}
			}
		});
		/* END UPLOAD */
	});
}

function loadServices(has_services){
	var serialCounter = $('#hdnCounter_id_toUse').val();	
	
	$('#servicesContainer').load(document_root+"rpc/loadsSales/loadServices.rpc",
		{
			counter: serialCounter, 
			has_services: has_services,
			multisales: 'YES'
		}, function(){
			$('[id^="chkService_"]').each(function(){
				$(this).bind('click', function(){
					clearCustomerList()
				});
			});
	});
}

function clearCustomerList(){
	$('#cards_container').html('');
	$('#hdnLoadedClients').val('');
}