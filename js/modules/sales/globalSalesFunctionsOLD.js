//Java Script
//THIS FILE CONTAINS ALL THE FUNCTIONS USED IN:
//			fNewSale.js
//			fEditSale.js
//			fNewMasiveSale.js
checkProductValidatorURL = 'modules/estimator/registered/checkProductAdultsValidator.php';

//FILTERS LOADING
function loadCities(countryID, selectID, ALL) {
    if (selectID == "Destination") {
        var container = $('#cityDestinationContainer');
    } else {
        if (selectID == "Reassign") {
            var container = $('#cityContainerReassign');
        } else {
            var container = $('#cityContainerCustomer');
        }
    }
    container.load(document_root + "rpc/loadCities.rpc", {
        serial_cou: countryID, serial_sel: selectID, all: ALL}, function () {
        $("#selCityReassign").bind("change", function (e) {
            loadDealer(this.value);
        });
        if (selectID == 'Reassign') {
            if ($("#selCityReassign").find('option').size() == 2) {
                loadDealer($('#selCityReassign').val());
            } else {
                loadDealer('');
            }
        }
        if (selectID == "Destination") {
            $("#selCityDestination option[name='General']").attr("selected", true);
        }
    }
    );
}

function loadDealer(cityID) {
    if (cityID) {
        $('#dealerContainerReassign').load(
                document_root + "rpc/loadDealers.rpc",
                {//serial_cou: countryID,
                    serial_cit: cityID,
                    dea_serial_dea: 'NULL'},
                function () {
                    $("#selDealer").bind("change", function (e) {
                        loadBranch(this.value);
                    });
                    if ($("#selDealer").find('option').size() == 2) {
                        loadBranch($('#selDealer').val());
                    } else {
                        loadBranch('');
                    }
                }
        );
    } else {
        $('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
        $('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
        $(':enabled#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
    }
}

function loadBranch(dealerID) {
    $(":enabled#selCounter").find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
    if (dealerID == "") {
        $("#selBranch option[value='']").attr("selected", true);
        $(":enabled#selCounter option[value='']").attr("selected", true);
        dealerID = 'stop';
    }

    $('#dealerData').load(
            document_root + "rpc/loadsSales/loadInvoiceDealer.rpc",
            {dea_serial_dea: dealerID}
    );
    $('#branchContainerReassign').load(
            document_root + "rpc/loadDealers.rpc",
            {dea_serial_dea: dealerID},
            function () {
                $("#selBranch").bind("change", function (e) {
                    loadBranchInvoice(this.value);
                    loadCounter(this.value);
                });
                loadCounter($('#selBranch').val());
            }
    );
}
//END OF FILTERS LOADING

function loadReassignSale(isChecked) {
    if (isChecked) {
        $("#selCounter option[value='']").attr("selected", true);
        $('#selCounter').attr('disabled', 'disabled');
        $('#saleInfoPACounter').hide();
        $('#reassignSaleContainer').load(document_root + "rpc/loadsSales/loadReassignSale.rpc", function () {
            $("#selCountryReassign").bind("change", function (e) {
                loadCities(this.value, 'Reassign', 'ALL');
                $('#hdnCntSerial_cou').val(this.value);
            });
        });
    } else {
        $('#reassignSaleContainer').html('');
        $('#selCounter').removeAttr('disabled');
        if ($('#selCounter').length == 1) {
            loadSaleInfo($(':enabled#selCounter').val(), 'YES', 'REGULAR');
        } else {
            loadSaleInfo($('#hdnOriginal_cnt').val(), 'NO', 'REGULAR');
        }
        
                
    }
}

function addSaleTypeClickAction() {
    $('[name="sale_type"]').click(function () {
        if ($(this).val() == 'VIRTUAL') {
            if ($(':enabled#selCounter').length == 1) {
                loadSaleInfo($(':enabled#selCounter').val(), 'YES', 'REGULAR');
            } else {
                loadSaleInfo($('#hdnOriginal_cnt').val(), 'NO', 'REGULAR');
            }
        }
        initializeSaleBox($(this).val());
    });
    initializeSaleBox('VIRTUAL');
}

function initializeSaleBox(saleType) {
    $('#txtCardNum').attr("readonly", true);
    $('#txtEmissionPlace').attr("readonly", true);
    $('#txtNameCounter').attr("readonly", true);
    $('#txtCodCounter').attr("readonly", true);
    $('#txtEmissionDate').attr("readonly", true);
    $('#txtDealer').attr("readonly", true);

    setCardBoxBehaviour(saleType, true);
    addEmissionDateBoxAction();
}

function setCardBoxBehaviour(saleType, erase_date) {
    if ($('#isEdition').val() != '1') {
        if (saleType == 'MANUAL') {
            $('#txtCardNum').attr("readonly", false);
            $('#txtEmissionDate').attr("readonly", false);
            setDefaultCalendar($("#txtEmissionDate"), '-1Y', '+0d');
            if (erase_date)
                $('#txtEmissionDate').val('');
            $('#txtCardNum').val('');
            $('#txtCardNum').change(function () {
                if ($('#txtCardNum').val() != '') {
                    var cardNum = $(this).val();
                    var serialCounter = $('#hdnSerial_cnt').val();
                    $.ajax({
                        url: document_root + "rpc/remoteValidation/sales/checkCardNumber.rpc",
                        type: "post",
                        data: ({txtCardNum: cardNum, serial_cnt: serialCounter, typeSale: saleType}),
                        success: function (data) {
                            var response = json_parse(data);
                            //hdnOriginal_cnt
                            switch (response['error']) {
                                case 'with_owner':
                                    $('#txtCodCounter').val(response['sellerCode']);
                                    $('#txtNameCounter').val(response['counterName']);
                                    $('#txtEmissionPlace').val(response['emissionPlace']);
                                    $('#txtDealer').val(response['name_dea']);
                                    if ($('#hdnSerial_cntRPC').length) {
                                        $('#hdnSerial_cntRPC').val(response['counter_id']);
                                        $('#hdnSerial_cnt').val(response['counter_id']);
                                    } else {
                                        $('#hdnSerial_cnt').val(response['counter_id']);
                                    }
                                    loadProductsBasedOnCounter(response['counter_id']);
                                    break;
                                case 'no_owner':
                                    break;
                                case 'was_sold':
                                    $('#txtCardNum').val('');
                                    alert('La tarjeta ingresada ya fue vendida.');
                                    break;
                                case 'out_of_range':
                                    $('#txtCardNum').val('');
                                    alert('La tarjeta no se encuentra asignada');
                                    break;
                            }
                        }
                    });
                }
            });
        } else { //VIRTUAL SALE 
            $('#txtCardNum').unbind('change');
            $('#txtEmissionDate').val($('#hdnEmissionDate').val());
            $('#txtCardNum').val('AUTOMATICO');
            setDefaultCalendar($("#txtEmissionDate"), '+0d', '+0d');
        }

        if ($("#selProduct option:selected").attr("generate_number_pro") == 'NO') {
            $('#txtCardNum').unbind('change');
            $('#txtCardNum').val('N/A');
            $('#txtCardNum').attr("readonly", true);
        }
    }
}

function addEmissionDateBoxAction() {
    $('#txtEmissionDate').change(function () {
        validateDates();
        executeProductPriceCalculation();
    });
}

function addCounterSelectAction() {
    $(":enabled#selCounter").change(function (e) {
        loadSaleInfo(this.value, 'YES', 'REGULAR');
    });
}

function evaluateEmissionDate() {
    var emDays = dayDiff($("#txtDepartureDate").val(), $('#txtEmissionDate').val());
    if (emDays > 1) { //Checks if the departure day is at least 1 day more than the emission date
        alert("La Fecha de Salida debe ser mayor o igual a la Fecha de Emisi\u00f3n");
        $("#txtDepartureDate").val('');
        $("#txtDaysBetween").html('');
    }
    type = $('#selType').val();
    if (type == 'MANUAL') {
        var emDate = dayDiff($("#hdnEmissionDate").val(), $('#txtEmissionDate').val());
        if (emDate > 1) {
            alert("La Fecha de Emisi\u00f3n no puede ser mayor a hoy");
            $('#txtEmissionDate').val('');
        }
    }
}

function loadProductsBasedOnCounter(serialCounter) {
    $('#productsContainer').load(document_root + "rpc/loadsSales/loadProducts.rpc", {counter: serialCounter}, function () {
        addProductSelectAction();
    });
}

function addProductSelectAction() {
    $("#selProduct").bind("change", function (e) {

        $('#btnCustomerPromoButon').attr('disabled', true);

        if ($('#isEdition').val() != '1') {
            saleType = $('[name="sale_type"]:checked').val();
            previousValue = $('#txtCardNum').val();
            setCardBoxBehaviour(saleType, false);

            //IF the result of the cardBoxValue change is empty field, means that we should put the previous manually entered value
            if (saleType == 'MANUAL' && $('#txtCardNum').val() == '' && previousValue != 'N/A') {
                $('#txtCardNum').val(previousValue)
            }
        }

        if ($("#selProduct option:selected").attr("flights_pro") == '0') {//Many flights: HIDE COUNTRY SELECTION FIELDS
            var today = new Date();
            $("#txtDepartureDate").val(("0" + today.getDate()).slice(-2) + "/" + ("0" + (today.getMonth() + 1)).slice(-2) + "/" + today.getFullYear());
            $('#divDestination').hide();
            $("#selCountryDestination").rules("remove");
            $("#selCityDestination").rules("remove");
            $("#selCountryDestination option[value='']").attr("selected", true);
            $("#selCityDestination option[value='']").attr("selected", true);
            $("#depDate").hide();
            $("#arrDate").hide();
            $("#startCoverage").show();
            $("#endCoverage").show();
            $("#hdnFlights").val('YES');

        } else { //Only one flight: SHOW COUNTRY SELECTION FIELDS

            $("#txtDepartureDate").attr('readonly', false);
            $("#txtArrivalDate").attr('readonly', false);
            $("#txtDepartureDate").val("");
            setDefaultCalendar($("#txtDepartureDate"), '+0d', '+100y');
            setDefaultCalendar($("#txtArrivalDate"), '+0d', '+100y');

            $("#depDate").show();
            $("#arrDate").show();
            $("#startCoverage").hide();
            $("#endCoverage").hide();
            $("#hdnFlights").val('NO');
            if ($('hddDays').val() != '' && $("#divDestination").css('display') == 'none') {
                $("#txtArrivalDate").val('');
                $("#txtDaysBetween").html('');
                $('hddDays').val('');
            }
            $('#divDestination').show();
            $("#selCountryDestination").rules('add', {
                required: true
            });
            $("#selCityDestination").rules('add', {
                required: true
            });

            $('#selCountryDestination').removeAttr('disabled');
            //destination_restricted_pro: NATIONAL PRODUCTS SET COUNTRY TO THE COUNTER'S AND BLOCK IT
            if ($("#selProduct option:selected").attr("destination_restricted_pro") == 'YES') {
                $("#selCountryDestination option[value='" + $('#hdnSerial_cou').val() + "']").attr("selected", true);
                loadCities($('#hdnSerial_cou').val(), 'Destination', 'ALL');
                $("#selCountryDestination").attr("disabled", true);
            }
        }

        setupExtrasSection();
        loadServices($('#product_' + $(this).val()).attr('services'));
        checkAndSetProductMaxCoverageTime();
        enableQuestionsDiv();
        executeProductPriceCalculation();
        validateDestination();
    });
}

function loadCustomerForms(typeCus) {
    //Always load as PERSON type
    $("#divName").show();
    $("#divName2").hide();
    $("#divAddress").show();
    $("#divAddress2").hide();
    $("#divRelative").show();
    $("#divManager").hide();
}

function calculateAllServicesPrice() {
    clearCustomerPromoContainer();
    var travelDays = parseInt($('#txtDaysBetween').html());
    var currency = new Array();
    var productID = $('#selProduct').val();
    var range_price = $('#product_' + productID).attr('range_price');
    var people_in_card = countPeopleInCard();

    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');

    if (travelDays > 0) {
        var price = 0;
        var cost = 0;
        var type_fee;

        $('[id^="chkService_"]').each(function () {
            if ($(this).is(':checked')) {
                type_fee = $(this).attr('type_fee');

                if (type_fee == 'PERDAY') {
                    if (range_price == 'YES') {
                        travelDays = $('#pPrice_' + $('#selDayNumber').val()).text();
                        if (travelDays > 0) {
                            price += parseFloat($(this).attr('price')) * travelDays;
                            cost += parseFloat($(this).attr('cost')) * travelDays;
                        }
                    } else {
                        price += parseFloat($(this).attr('price')) * travelDays;
                        cost += parseFloat($(this).attr('cost')) * travelDays;
                    }
                } else if (type_fee == 'TOTAL') {
                    price += parseFloat($(this).attr('price'));
                    cost += parseFloat($(this).attr('cost'));
                }
            }
        });

        if (price > 0) {
            /* FIX PRICE WITH ALL CARD MEMBERS */
            price *= people_in_card;
            cost *= people_in_card;
            /* FIX PRICE WITH ALL CARD MEMBERS */

            $('#hdnServicePrice').val(price.toFixed(2));
            price = (price * currency['0']).toFixed(2);
            $('#servicePriceContainer').html(currency['1'] + ' ' + price);
            $('#hdnServiceCost').val(cost.toFixed(2));
        } else {
            $('#servicePriceContainer').html('');
            $('#hdnServicePrice').val('0');
            $('#hdnServiceCost').val('0');
        }
    }
}

function logManualInputPrice() {//WHEN PRICE IS INPUT MANUALLY, HERE ITS VALUES ARE LOGGED TO COMPLY WITH NORMAL CALCULATIONS
    formatManualPriceInputBox();
    var currency = new Array();
    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');
    var productID = $('#selProduct').val();
    var price = 0;
    var cost = parseFloat($('#pPrice_' + $('#selDayNumber').val()).attr('cost'));
    var insertedPrice = parseFloat($('#userPriceFee').val());
    var minValue = parseFloat($('#hdnMin').val());
    var maxValue = parseFloat($('#hdnMax').val());
    var day_price = $('#product_' + productID).attr('price_by_day_pro');

    if (insertedPrice <= maxValue && minValue <= insertedPrice) {
        var days = $('#pPrice_' + $('#selDayNumber').val()).text();
        if (day_price == 'YES') {
            price = (parseFloat(days) * insertedPrice).toFixed(2);
            cost = (cost * parseFloat(days)).toFixed(2);
        } else {
            price = insertedPrice.toFixed(2);
        }

        price = roundNumber(price, 2).toFixed(2);
        $('#priceContainer').html(currency['1'] + ' ' + price);
        price = roundNumber(parseFloat(price) / currency['0'], 2);
        $('#hdnPrice').val(price);
        $('#hdnCost').val(cost);
        calculateAllExtrasPrices();
    } else {
        alert('Ingrese un valor entre los precios establecidos.');
        $('#hdnPrice').val('');
        $('#hdnCost').val('');
        $('#priceContainer').html('');
        $('#userPriceFee').val('');
    }
}

function sumAllPricesAndDisplay() {
    var currency = new Array();
    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');
    var totalPriceProducts = 0;
    var totalCostProducts = 0;
    var totalPriceServices = 0;
    var totalCostServices = 0;
    var totalPriceExtras = 0;
    var totalCostExtras = 0;

    if ($('#hdnPrice').length > 0 && $('#hdnPrice').val() != '') {
        totalPriceProducts = parseFloat($('#hdnPrice').val());
        totalCostProducts = parseFloat($('#hdnCost').val());
        $('#priceContainer').val(totalPriceProducts);
    }

    if ($('#hdnServicePrice').length > 0 && $('#hdnServicePrice').val() != '') {
        totalPriceServices = parseFloat($('#hdnServicePrice').val());
        totalCostServices = parseFloat($('#hdnServiceCost').val());
        $('#servicePriceContainer').val(totalPriceServices);
    }

    if ($('#hdnTotalPriceExtra').val() != '') {
        totalPriceExtras = parseFloat($('#hdnTotalPriceExtra').val());
        totalCostExtras = parseFloat($('#hdnTotalCostExtra').val());
        $('#totalPriceExtras').val(totalPriceExtras);
    }

    totalPriceProductsFixed = parseFloat(totalPriceProducts * currency['0']);
    var services_and_extras_price = parseFloat((totalPriceServices + totalPriceExtras) * currency['0']);
    var totalFixed = roundNumber(services_and_extras_price + totalPriceProductsFixed, 2).toFixed(2);
    var total = roundNumber((totalPriceServices + totalPriceProducts + totalPriceExtras), 2).toFixed(2);
    var cost = roundNumber(totalCostServices + totalCostProducts + totalCostExtras, 2).toFixed(2);
    //alert('Productos: '+totalPriceProducts+' Servicios: '+totalPriceServices+' Total: '+ total);

    $('#totalContainer').html(currency['1'] + ' ' + totalFixed);
    if ($('#totalGrossContainer').length > 0) {
        $('#totalGrossContainer').html(currency['1'] + ' ' + roundNumber(totalFixed * peasant_tax, 2).toFixed(2));
    }
    $('#hdnTotalCost').val(cost);
    $('#hdnTotal').val(total);
    $('#btnRegistrar').removeAttr('disabled');
    $('#btnCustomerPromoButon').attr('disabled', false);
}

function formatManualPriceInputBox() {
    var insertedPrice = $('#userPriceFee').val().split('.');
    if (insertedPrice[1]) {
        if (insertedPrice[1].length > 2) {
            insertedPrice[1] = insertedPrice[1].substring(0, 2);
            $('#userPriceFee').val(insertedPrice[0] + '.' + insertedPrice[1]);
        }
    }
}

function logPricesForDaySelectProducts() {//WHEN DAYS SELECT BOX IS DISPLAYED, HERE ITS PRICES ARE LOGGED TO COMPLY WITH NORMAL CALCULATIONS
    var priceID = $('#selDayNumber').val();
    var productID = $('#selProduct').val();
    var day_price = $('#product_' + productID).attr('price_by_day_pro');
    var calculate_price = $('#product_' + productID).attr('calculate_price');

    $('#priceContainer').html('');
    $('#hdnPrice').val('');
    $('#hdnCost').val('');

    var currency = new Array();
    currency['0'] = parseFloat($('[name="rdCurrency"]:checked').val());
    currency['1'] = $('[name="rdCurrency"]:checked').attr('symbol');

    if (priceID != '') {
        var price;
        var cost;
        if (calculate_price == 'YES') {
            if (day_price == 'YES') {
                price = roundNumber(parseFloat($('#selDayNumber option:selected').text()) * parseFloat($('#pPrice_' + priceID).attr('price')), 2).toFixed(2);
                cost = roundNumber(parseFloat($('#selDayNumber option:selected').text()) * parseFloat($('#pPrice_' + priceID).attr('cost')), 2).toFixed(2);
            } else {
                price = $('#pPrice_' + priceID).attr('price');
                cost = $('#pPrice_' + priceID).attr('cost');
            }
            $('#hdnPrice').val(price);
            price = roundNumber(price * currency['0'], 2).toFixed(2);
            $('#priceContainer').html(currency['1'] + ' ' + price);
            $('#hdnCost').val(cost);

            calculateAllExtrasPrices();
        } else {
            $('#rangePricesContainer').load(document_root + "rpc/loadsSales/displayRangeInfo.rpc", {
                serial_ppc: priceID,
                change_fee: currency['0']}, function () {
                $('#userPriceFee').change(logManualInputPrice);
            }
            );
        }
    } else {
        $('#rangePricesContainer').html('');
        $('#priceContainer').html('');
        $('#hdnPrice').val('');
    }
}

function loadDaysSelectBox() {
    var travelDays = $('#txtDaysBetween').html();
    var serial_pxc = $('#product_' + $('#selProduct').val()).attr('serial_pxc');
    var serial_cou = $('#hdnSerial_cou').val();
    var serial_pro = $('#product_' + $('#selProduct').val()).attr('serial_pro');

    $('#rangeSelect').load(document_root + "rpc/loadsSales/calculateFixPrice.rpc", {
        days: travelDays,
        serial_pxc: serial_pxc,
        serial_cou: serial_cou,
        serial_pro: serial_pro}, function () {
        $('#selDayNumber').change(function () {
            logPricesForDaySelectProducts();
        });
        $("#selDayNumber").rules("add", {
            required: true
        });
        $("#hdnPrice").rules("add", {
            required: true,
            messages: {
                required: "No existe un 'Precio' para el n&uacute;mero de d&iacute;as seleccionado. <br>\n\
	                          Por favor disminuya el n&uacute;mero de d&iacute;as del viaje."
            }
        });

        if ($("#hdnModify").length > 0 &&
                parseInt($('#selProduct :selected').attr('flights_pro')) == 0 &&
                $("#hdnModify").val() == 'YES') {
            $('#selDayNumber :option').each(function () {
                if ($(this).text() == $('#hdnBackupDays').val()) {
                    $(this).attr('selected', true);
                    return;
                }
            });
        }
    }
    );
}

function loadServices(has_services) {
    var serialCounter = $('#hdnSerial_cnt').val();

    $('#servicesContainer').load(document_root + "rpc/loadsSales/loadServices.rpc",
            {
                counter: serialCounter,
                has_services: has_services
            }, function () {
        $('[id^="chkService_"]').each(function () {
            $(this).bind('click', function () {
                calculateAllServicesPrice();
                sumAllPricesAndDisplay();
            });
        });
    })
}

function validateDates() {
    if ($("#txtArrivalDate").val() != '' && $("#txtDepartureDate").val() != '' && $('#txtEmissionDate').length > 0) {
        var days = dayDiff($("#txtDepartureDate").val(), $("#txtArrivalDate").val());
        if (days > 0) { //if departure date is smaller than arrival date
            if (days > 365) { //the departure date can't be over than 365 days of departure date
                alert("La Fecha de Llegada no puede ser mayor a 365 d\u00edas de la Fecha de Salida");
                $("#txtArrivalDate").val('');
                $("#txtDaysBetween").html('');
            } else {
                var emDays = dayDiff($("#txtDepartureDate").val(), $('#txtEmissionDate').val());
                if (emDays > 1) { //Checks if the departure day is at least 1 day more the the emission date
                    alert("La Fecha de Salida debe ser mayor o igual a la Fecha de Emisi\u00f3n");
                    $("#txtDepartureDate").val('');
                    $("#txtDaysBetween").html('')
                } else {
                    $("#txtDaysBetween").html(days);
                    $("#hddDays").val(days);
                }
            }
        } else {
            alert("La Fecha de Llegada no puede ser mayor a la Fecha de Salida");
            $("#txtDaysBetween").html('');
            $("#txtArrivalDate").val('');
        }
    }
    if ($("#txtArrivalDate").val() != '' && $("#txtDepartureDate").val() != '' && $('#txtEmissionDate').length <= 0) {
        $("#txtDepartureDate").val('');
        $("#txtArrivalDate").val('');
        $("#txtDaysBetween").html('');
        alert("Primero debe seleccionar un Counter");
    }
}

function clearAllDisplayedPrices() {
    $('#priceContainer').html('0.00');
    $('#servicePriceContainer').html('0.00');
    $('#totalPriceExtras').html('0.00');
    $('#totalContainer').html('');
}

function loadSaleInfo(counterID, show_line, type_sale) {
    $('#saleInfoPACounter').load(document_root + "rpc/loadsSales/loadSaleInfo.rpc", {serial_counter: counterID, show_line: show_line}, function () {
        $('#hdnDirectSale').val($('#hdnOfCounterRPC').val());
        $('[name="rdCurrency"]').bind('click', function () {
            calculateAllServicesPrice();
            sumAllPricesAndDisplay();
        });

        if ($('#rdIsFree').length > 0) {
            $('#rdIsFree').rules('add', {
                required: true
            });
        }

        $('#hdnEmissionDate').val($('#hdnEmissionDateRPC').val());
        $('#hdnCardNum').val($('#hdnCardNumRPC').val());
        $('#hdnSerial_cou').val($('#hdnSerial_couRPC').val());

        addSaleTypeClickAction();
        evaluateEmissionDate();
        $('#hdnSerial_cnt').val(counterID);

        if (type_sale == 'REGULAR') {
            if ($(":enabled#selCounter option:selected").val()) {
                loadProductsBasedOnCounter($(":enabled#selCounter option:selected").val());
            } else {
                loadProductsBasedOnCounter($('#hdnSerial_cnt').val());
            }
        } else {
            loadMassiveProductsBasedOnCounter($(":enabled#selCounter option:selected").val());
        }
    });
    $('#saleInfoPACounter').show();
}

function executeProductPriceCalculation() {
    clearCustomerPromoContainer();
    if ($("#hdnModify").val() == 'NO') {//IF MODIFICATION IS NOT PERMITTED, PRICE RECALCULATION IS DISABLED!
        totalSalePrice = $('#hdnTotal').val();
        totalSaleCost = $('#hdnTotalCost').val();

        //PERSIST ORIGINAL PRODUCT PRICE
        $('#priceContainer').val($('#hdnOriginalPrice').val());
        $('#hdnPrice').val($('#hdnOriginalPrice').val());
        $('#hdnCost').val($('#hdnOriginalCost').val());

        //PERSIST ORIGINAL TOTAL PRICE
        $('#totalContainer').html($('[name="rdCurrency"]:checked').attr('symbol') + ' ' + totalSalePrice);
        $('#hdnTotalCost').val(totalSaleCost);
        $('#hdnTotal').val(totalSalePrice);
        return false;
    }

    clearAllDisplayedPrices();
    if ($('#selProduct').val() != '') {
        var display_prices = $('#product_' + $('#selProduct').val()).attr('show_price_pro');
        var calculate_price = $('#product_' + $('#selProduct').val()).attr('calculate_price');

        if (display_prices == "NO" && calculate_price == "YES") {//PRICE AUTOCALCULATED
            $('#rangeSelect').html('');
            $('#rangePricesContainer').html('');
            executeDirectPriceCalculation();
        } else {//MANUAL PRICE
            loadDaysSelectBox();
        }
    }
}

function executeDirectPriceCalculation() {
    if ($('#selProduct').val() != '') {
        var travelDays = $('#txtDaysBetween').html();
        var serial_pxc = $('#product_' + $('#selProduct').val()).attr('serial_pxc');
        var serial_cou = $('#hdnSerial_cou').val();
        var serial_pro = $('#product_' + $('#selProduct').val()).attr('serial_pro');
        var display_prices = $('#product_' + $('#selProduct').val()).attr('show_price_pro');

        var change_fee = parseFloat($('[name="rdCurrency"]:checked').val());
        var symbol = $('[name="rdCurrency"]:checked').attr('symbol');

        if (travelDays != '' && serial_pxc != '') {
            $('#priceContainer').load(document_root + "rpc/loadsSales/calculatePrice.rpc", {
                days: travelDays,
                serial_pxc: serial_pxc,
                serial_cou: serial_cou,
                serial_pro: serial_pro,
                range_price: display_prices,
                change_fee: change_fee,
                symbol: symbol}, function () {
                $("#hdnPrice").rules("add", {
                    required: true,
                    messages: {
                        required: "No existe un 'Precio' para el n&uacute;mero de d&iacute;as seleccionado. <br>\n\
									  Por favor disminuya el n&uacute;mero de d&iacute;as del viaje."
                    }
                });
                calculateAllExtrasPrices();
            }
            );
        }
    }
}

function evaluateCardDates() {
    if ($("#txtArrivalDate").val() != '' && $("#txtDepartureDate").val() != '' && $("#serialCus").length > 0) {
        $.ajax({
            url: document_root + "rpc/remoteValidation/sales/evaluateCardDates.rpc",
            type: "post",
            data: ({
                beginDate: $("#txtDepartureDate").val(),
                endDate: $("#txtArrivalDate").val(),
                serialCus: $("#serialCus").val(),
                serialSal: $("#hdnSerial_sal").val()
            }),
            success: function (data) {
                if (data == 'true') {
                    $("#txtArrivalDate").val('');
                    $('#txtDaysBetween').html('');
                    clearAllDisplayedPrices();

                    alert("El cliente ya tiene una tarjeta activa en el mismo per\u00EDodo de tiempo. No se puede emitir otro contrato.");
                } else {
                    executeProductPriceCalculation();
                }
            }
        });
    }
}

function loadCounter(branchID) {
    if (branchID == "") {
        $(":enabled#selCounter option[value='']").attr("selected", true);
        branchID = 'stop';
    }
    $('#counterContainerReassign').load(document_root + "rpc/loadCounter.rpc", {serial_dea: branchID}, function () {
        addCounterSelectAction();
    });
}

function loadBranchInvoice(branchID) {
    if (branchID == "") {
        $(":enabled#selCounter option[value='']").attr("selected", true);
        branchID = 'stop';
    }
}

function checkAndSetProductMaxCoverageTime() {
    if ($("#selProduct option:selected").attr("flights_pro") == '0' && $("#txtDepartureDate").val() != '') {
        var departureDate = $("#txtDepartureDate").val();
        var departureDate_array = departureDate.split('/');
        var arrivalDate = new Date(departureDate_array[2], departureDate_array[1] - 1, departureDate_array[0]);
        var mili = parseInt($("#hdnMaxCoverTime").val()) * 24 * 60 * 60 * 1000;
        var time = arrivalDate.getTime() + mili;
        arrivalDate.setTime(parseInt(time));
        var newArrivalDate;
        if (arrivalDate.getDate() < 10) {
            newArrivalDate = "0" + arrivalDate.getDate() + "/";
        } else {
            newArrivalDate = arrivalDate.getDate() + "/";
        }

        if (arrivalDate.getMonth() + 1 < 10) {
            newArrivalDate = newArrivalDate + "0" + (arrivalDate.getMonth() + 1) + "/";
        } else {
            newArrivalDate = newArrivalDate + (arrivalDate.getMonth() + 1) + "/";
        }

        newArrivalDate = newArrivalDate + arrivalDate.getFullYear();
        $("#txtArrivalDate").val(newArrivalDate);
        $("#depDate").hide();
        $("#arrDate").hide();
        $("#startCoverage").show();
        $("#endCoverage").show();

        validateDates();
        $("#txtDepartureDate").attr("readonly", "readonly");
        $("#txtArrivalDate").attr("readonly", "readonly");
        $("#txtDepartureDate").datepicker("destroy");
        $("#txtArrivalDate").datepicker("destroy");
    } else {
        $("#depDate").show();
        $("#arrDate").show();
        $("#startCoverage").hide();
        $("#endCoverage").hide();
        if ($("#selProduct option:selected").attr("flights_pro") == '0') {
            $("#txtArrivalDate").val('');
        }
    }
}

function isPACounter(serialCounter) {
    $.ajax({
        url: document_root + "rpc/remoteValidation/sales/isPACounter.rpc",
        type: "post",
        data: ({serialCounter: serialCounter}),
        success: function (data) {
            if (data == 'false') {
                $("#divSellFree").hide();
            } else {
                $("#divSellFree").show();
            }
        }
    });
}

//FUNCTIONS FOR EXTRAS
function hasSpouse(count) {
    var hasSpouse = 0;
    $("[id^='selRelationship_']").each(function () {
        if ($(this).attr('elementID') != count) {
            if ($(this).val() == 'SPOUSE') {
                hasSpouse = 1;
                return;
            }
        }
    });

    if ($("#selRelationship_" + count).val() == 'SPOUSE' && hasSpouse == 1) {
        alert("Ya est\u00e1 seleccionada la opci\u00f3n Esposa");
        $("#selRelationship_" + count).find('option[value=""]').attr('selected', true);
    }
}

function checkEmailExtraIsDuplicated(extraPosition) {
    //VALUES:
    // 0 - NO DUPLICATED EMAILS
    // 1 - DUPLICATED EMAIL
    // 2 - DUPLICATED WITH HOLDER EMAIL
    var duplicityChecker = 0;
    if ($("#txtMailCustomer").val() != $('#emailExtra_' + extraPosition).val()) {
        $("[id^='emailExtra_']").each(function () {
            if ($(this).attr("id") != 'emailExtra_' + extraPosition) {
                if ($(this).val() == $('#emailExtra_' + extraPosition).val()) {
                    alert('El "Email" ya ha sido ingresado en otro Extra.\n Por favor ingrese un "Email" diferente');
                    duplicityChecker = 1;

                }
            }
        });
    } else {
        alert('El "Email" ingresado es igual a "Email" del titular.\n Ingrese un "Email" distinto');
        duplicityChecker = 2;
    }
    return duplicityChecker;
}

function checkEmailExtra(count) {
    //VALUES:
    // 0 - NO DUPLICATED EMAILS
    // 1 - DUPLICATED EMAIL
    // 2 - DUPLICATED WITH HOLDER EMAIL
    var duplicityChecker = 0;
    if ($("#txtMailCustomer").val() != $('#emailExtra_' + count).val()) {
        $("[id^='emailExtra_']").each(function () {
            if ($(this).attr("id") != 'emailExtra_' + count) {
                if ($(this).val() == $('#emailExtra_' + count).val()) {
                    alert('El "Email" ya ha sido ingresado en otro Extra.\n Por favor ingrese un "Email" diferente');
                    duplicityChecker = 1;

                }
            }
        });
    } else {
        alert('El "Email" ingresado es igual a "Email" del titular.\n Ingrese un "Email" distinto');
        duplicityChecker = 2;
    }
    return duplicityChecker;
}

function displayQuestionsForAllExtras() {
    var extraPosition = parseInt($('#count').val());
    if (extraPosition > 0) {
        extraPosition--;
        for (extraPosition; extraPosition >= 0; extraPosition--) {
            displayQuestionsForExtra(extraPosition);
        }
    }
}

function displayQuestionsForExtra(count) {
    var serial_cus = $('#hdnSerial_cusExtra_' + count).val();
    var arrivalDate = new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
    arrivalDate.setFullYear(arrivalDate.getFullYear() - $('#hdnDisplayQuestionsAge').val()); //ELDERLY
    var birthDate = new Date(Date.parse(dateFormat($('#birthdayExtra_' + count).val())));

    if ($('#txtArrivalDate').val() != '' && $('#birthdayExtra_' + count).val() != '' && $('#birthdayExtra_' + count).val() != undefined) {
        if ($('#hdnQuestionsExists').val() == 1) {
        
            //************** FOR SENIOR ALLOWED VALIDATION
            if((arrivalDate - birthDate) >= 0){
                if ($("#selProduct option:selected").attr("senior_pro") == 'NO') {
                    alert("Este producto no acepta Adultos Mayores.");
                    $("#selRelationship_" + count + " option[value='']").attr("selected", true);
                    $("#birthdayExtra_" + count).val('');
                    $("#idExtra_" + count).val('');
                    $("#serialCus_" + count).val('');
                    $("#selRelationship_" + count).val('');
                    $("#nameExtra_" + count).val('');
                    $("#lastnameExtra_" + count).val('');
                    $("#emailExtra_" + count).val('');
                    $("#hdnPriceExtra_" + count).val('');
                    $("#hdnCostExtra_" + count).val('');
                }
            }
            
            //**************** FOR DISPLAY QUESTIONS ********
            if((arrivalDate - birthDate) >= 0){
                $('#questionsContainer_' + count).load(document_root + "rpc/loadsSales/loadElderQuestionsExtras.rpc", {
                    customer: serial_cus,
                    count: count,
                    editionAllowed: $("#hdnModify").val()}, function () {
                    var ext = '[groupid="' + count + '"]';
                    $(":select[id^='answer" + count + "_']" + ext).each(function () {
                        $(this).rules("add", {
                            required: true
                        });
                    });
                });
            }
        } else {
            alert("No existen preguntas registradas en el sistema");
        }
    } else {
        var ext = '[groupid="' + count + '"]';
        $(":select[id^='answer" + count + "_']" + ext).each(function () {
            $(this).rules("remove", "required");
        });
        $('#questionsContainer_' + count).html('');
    }
}

function setExtrasTotalPriceToZero() {
    highestExtraPosition = parseInt($('#count').val());
    for (extraPosition = highestExtraPosition - 1; extraPosition > -1; extraPosition--) {
        $('#extraPriceContainer_' + extraPosition).html('');
    }
    $('#hdnTotalPriceExtra').val('0');
    $('#hdnTotalCostExtra').val('0');
    $('#hdnChildrenFree').val($("#selProduct option:selected").attr("children_pro"));
    $('#hdnAdultsFree').val($("#selProduct option:selected").attr("adults_pro"));
    sumAllPricesAndDisplay();
}

function calculateAllExtrasPrices() {
    //GATHER INFORMATION
    var extrasJSONObject = new Object;

    extrasJSONObject.base_price = $('#hdnPrice').val();
    extrasJSONObject.base_cost = $('#hdnCost').val();
    extrasJSONObject.adults_free = $("#selProduct option:selected").attr("adults_pro");

    extrasJSONObject.percentage_adult = '' + $("#selProduct option:selected").attr("percentage_extras");
    extrasJSONObject.percentage_children = '' + $("#selProduct option:selected").attr("percentage_children");
    extrasJSONObject.percentage_spouse = '' + $("#selProduct option:selected").attr("percentage_spouse");

    extrasJSONObject.max_extras_pro = $("#selProduct option:selected").attr("max_extras_pro");
    extrasJSONObject.children_free = $("#selProduct option:selected").attr("children_pro");
    extrasJSONObject.extras_restriction = $("#selProduct option:selected").attr("extras_restricted_to_pro");

    extrasJSONObject.arrivalDate = $("#txtArrivalDate").val(); //TODO Change Age ValidationToEndDate

    extrasJSONObject.extras = new Array;
    numberOfExtras = parseInt($('#count').val());
    for (var i = 0; i < numberOfExtras; i++) {
        extrasJSONObject.extras[i] = new Object;
        extrasJSONObject.extras[i].age = $('#birthdayExtra_' + i).val();
        extrasJSONObject.extras[i].relationship = $('#selRelationship_' + i).val();
    }
    jsonString = json_encode(extrasJSONObject);

    currencyValue = parseFloat($('[name="rdCurrency"]:checked').val());
    currencySymbol = $('[name="rdCurrency"]:checked').attr('symbol');

    //SEND AND PROCESS
    var totalPriceExtras = 0;
    var totalCostExtras = 0;
    $.ajax({
        url: document_root + "modules/sales/ajaxGetExtrasPrices",
        type: "post",
        data: ({jsonData: jsonString}),
        success: function (jsonResponse) {
            //RECIBIR Y DESPLEGAR
            response = json_parse(jsonResponse);
            for (var i = 0; i < numberOfExtras; i++) {
                extraPrice = response[i]['price'];
                extraCost = response[i]['cost'];

                totalPriceExtras += extraPrice;
                totalCostExtras += extraCost;

                $('#hdnPriceExtra_' + i).val(extraPrice);
                $('#hdnCostExtra_' + i).val(extraCost);

                priceExtraFixed = roundNumber((extraPrice) * currencyValue, 2).toFixed(2);
                $('#extraPriceContainer_' + i).html(currencySymbol + ' ' + priceExtraFixed);
            }

            $('#hdnTotalPriceExtra').val(totalPriceExtras);
            $('#hdnTotalCostExtra').val(totalCostExtras);

            totalPriceExtrasFixed = roundNumber((totalPriceExtras) * currencyValue, 2).toFixed(2);
            $('#totalPriceExtras').html(totalPriceExtrasFixed);

            calculateAllServicesPrice();
            sumAllPricesAndDisplay();
        }
    });
}

function rejectAgeOfExtra(extraPosition) {
    $('#birthdayExtra_' + extraPosition).val('');
    $('#extraPriceContainer_' + extraPosition).html('');
    var totalPrice = parseFloat($('#hdnTotalPriceExtra').val());
    var priceExtra = parseFloat($('#hdnPriceExtra_' + extraPosition).val());
    var totalCost = parseFloat($('#hdnTotalCostExtra').val());
    var costExtra = parseFloat($('#hdnCostExtra_' + extraPosition).val());

    totalPrice = totalPrice - priceExtra;
    totalCost = totalCost - costExtra;

    $('#hdnTotalPriceExtra').val(totalPrice);
    $('#hdnTotalCostExtra').val(totalCost);
    sumAllPricesAndDisplay();
}

function setExtraPriceToZero(extraPosition) {
    currencySymbol = $('[name="rdCurrency"]:checked').attr('symbol');
    priceExtraFixed = roundNumber(0, 2).toFixed(2);
    $('#hdnPriceExtra_' + extraPosition).val(0);
    $('#hdnCostExtra_' + extraPosition).val(0);
    $('#extraPriceContainer_' + extraPosition).html(currencySymbol + ' ' + priceExtraFixed);
}

function getAgeOfExtra(extraPosition) {
    var curr = new Date(Date.parse(dateFormat($('#birthdayExtra_' + extraPosition).val())));
    var year = curr.getFullYear();
    var month = curr.getMonth() + 1;
    var day = curr.getDate();
    var today = new Date(Date.parse(dateFormat($('#txtDepartureDate').val())));

    var age = today.getFullYear() - year - 1;

    if (today.getMonth() + 1 - month < 0) {
        return age;
    }
    if (today.getMonth() + 1 - month > 0) {
        return age + 1;
    }
    if (today.getUTCDate() - day >= 0) {
        return age + 1;
    }

    return age;
}

function enableQuestionsDiv() {
    if ($("#selType").val() == 'PERSON') {
        birthdateValue = $('#txtBirthdayCustomer').val();
        if ($('#txtArrivalDate').val() != '' && birthdateValue != '' && $("#selProduct option:selected").val() != '') {
            var arrivalDate = new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
            arrivalDate.setFullYear(arrivalDate.getFullYear() - $('#hdnDisplayQuestionsAge').val()); //QUESTIONS
            var birthDate = new Date(Date.parse(dateFormat(birthdateValue)));
            
            var arrivalDateElderly = new Date(Date.parse(dateFormat($('#txtArrivalDate').val())));
            arrivalDateElderly.setFullYear(arrivalDateElderly.getFullYear() - $('#hdnElderlyAgeLimit').val()); //ELDERLY
            
            
            //******** FOR CONTROL OF SENIOR ALLOWED
            if ((arrivalDateElderly - birthDate) >= 0) {
                if ($("#selProduct option:selected").attr("senior_pro") == 'NO') {
                    alert("Este producto no acepta Adultos Mayores.");
                    $("#selProduct option[value='']").attr("selected", true);
                }
            }
            
            //******************** FOR CONTROL OF QUESTION ************
            if ((arrivalDate - birthDate) >= 0) {
                show_ajax_loader();
                $('#customerQuestionsContainer').load(document_root + "rpc/loadsSales/loadElderQuestionsCustomer.rpc", {
                    customer: $('#serialCus').val(),
                    editionAllowed: $("#hdnModify").val()
                }, function () {
                    $(":select[id^='cust_answer']").each(function () {
                        $(this).rules("add", {
                            required: true
                        });
                    });

                    hide_ajax_loader();
                });
            } else {
                $(":select[id^='cust_answer").each(function () {
                    $(this).rules("remove");
                });

                $('#customerQuestionsContainer').html('');
            }
        } else {
            $('#customerQuestionsContainer').html('');
        }
    } else {
        $('#customerQuestionsContainer').html('');
    }

//    showRepContainerForUnderAgeHolder();
}

function showRepContainerForUnderAgeHolder() {
    if ($("#selType").val() == 'PERSON') {
        if ($('#txtArrivalDate').val() != '' && $('#txtBirthdayCustomer').val() != '') {
            var yearsBetween = parseInt(dayDiff($('#txtBirthdayCustomer').val(), $('#txtArrivalDate').val()) / 365);

            if (yearsBetween <= underAge) {
                $('#repContainer').show();
            } else {
                $('#repContainer').hide();
                clearRepForm();
            }
        }
    }
}

function setupCustomerSection() {
    $('#txtNameCustomer').attr('readonly', false);
    $('#txtLastnameCustomer').attr('readonly', false);

    $('#txtDocumentCustomer').change(function () {
        loadCustomer($(this).val());
    });

    $('#selOtherCountry').change(function(){
        console.log('country');
        loadOtherCities($('#selOtherCountry').val())
    });
    
    $('#txtOtherDocument').bind('change', function() {
        console.log('Cambio');
        loadOtherCustomer($(this).val());
    });
    
    $('#txtNameCustomer').change(function () {
        document.getElementById("txtFirstNameInvoice").value = document.getElementById('txtNameCustomer').value;
    });
    $('#txtLastnameCustomer').change(function () {
        document.getElementById("txtLastNameInvoice").value = document.getElementById('txtLastnameCustomer').value;
    });
    $('#selCountry').change(function () {
        document.getElementById("selCountryInvoice").value = document.getElementById('selCountry').value;
    });
    $('#selCityCustomer').change(function () {
        document.getElementById("selCityInvoice").value = document.getElementById('selCityCustomer').value;
    });
    $('#txtAddress').change(function () {
        document.getElementById("txtAddressInvoice").value = document.getElementById('txtAddress').value;
    });
    $('#txtPhone1Customer').change(function () {
        document.getElementById("txtPhone1Invoice").value = document.getElementById('txtPhone1Customer').value;
    });
    $('#txtPhone2Customer').change(function () {
        document.getElementById("txtPhone2Invoice").value = document.getElementById('txtPhone2Customer').value;
    });
    $('#txtMailCustomer').change(function () {
        document.getElementById("txtEmailInvoice").value = document.getElementById('txtMailCustomer').value;
    });
    
    
    

    $('#repDocument').change(function () {
        loadRepCustomer($(this).val());
    });

    $('#selType').change(function () {
        enableQuestionsDiv();
        switchCustomerType($(this).val());
    });
    $("#selCountry").bind("change", function (e) {
        loadCities(this.value, 'Customer', 'ALL');
    });
    enableQuestionsDiv();

    if ($("#hdnModify").length == 1) {
        if ($("#hdnModify").val() == 'YES') {
            setDefaultCalendar($("#txtBirthdayCustomer"), '-100y', '+0d', '-20y');
            setDefaultCalendar($("#txtBirthdayOtherCustomer"), '-100y', '+0d', '-20y');
            setDefaultCalendar($("#repBirthday"), '-100y', '-18y', '-20y');
        }
    } else {
        setDefaultCalendar($("#txtBirthdayCustomer"), '-100y', '+0d', '-20y');
        setDefaultCalendar($("#txtBirthdayOtherCustomer"), '-100y', '+0d', '-20y');
        setDefaultCalendar($("#repBirthday"), '-100y', '-18y', '-20y');
    }
    $("#txtBirthdayCustomer").change(enableQuestionsDiv);
}

function loadCustomer(customerID) {
    var docCust = customerID;
    show_ajax_loader();
    $.ajax({
        url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({txtDocument: docCust}),
        success: function (customerExists) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                type: "post",
                data: ({txtDocument: docCust}),
                success: function (customerType) {
                    $('#customerContainer').load(
                            document_root + "rpc/loadsSales/loadCustomerComplete.rpc",
                            {document_cus: docCust}, function () {
                        setupCustomerSection();
                        evaluateCardDates();
                        enableQuestionsDiv();

                        $("#txtBirthdayCustomer").change(enableQuestionsDiv);
                        hide_ajax_loader();

                        if ($('#nmBlackListed').size() > 0) {
                            $('#btnRegistrar').hide();
                        } else {
                            $('#btnRegistrar').show();
                        }
                    }
                    );
            
                        $('#customerData').load(
                            document_root + "rpc/loadsSales/loadInvoiceCustomer.rpc",
                            {document_cus: docCust}, function () {
                            hide_ajax_loader();
                }
                    );
                }
            });

            $("#txtBirthdayCustomer").change(enableQuestionsDiv);
        }
        
    });
}

function loadOtherCities(countryID){
    console.log(countryID);
    if(countryID==""){
        $("#selOtherCity option[value='']").attr("selected",true);
    }
    $('#cityOtherContainer').load(document_root+"rpc/loadCities.rpc",{
        serial_cou: countryID,
        all: 'YES'
    });
}

function loadOtherCustomer(customerID) {
    var docCust = customerID;
    show_ajax_loader();
    $.ajax({
        url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({txtDocument: docCust}),
        success: function (customerExists) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                type: "post",
                data: ({txtDocument: docCust}),
                success: function (customerType) {
                        $('#otherData').load(
                            document_root + "rpc/loadsSales/loadInvoiceOtherCustomer.rpc",
                            {document_cus: docCust}, function () {
                            setupCustomerSection();
                            hide_ajax_loader();
                            $('[id^="OtherrdDocumentType"]').each(function(){
                                $(this).bind('click', function(){
                                    $('#selTypeOther').val($(this).attr('person_type'));
                                    personTypeBindingsOther();
                                });
                            });
                    }
                    );
                }
            });
        }        
    });
}

function checkDocumentAndLoadExtra(extraPosition, documentExtra) {
    if ($("#txtDocumentCustomer").val() != documentExtra) {
        var documentDuplicated = false;

        //CHECK FOR DUPLICITY IN OTHER EXTRAS:
        $("[id^='idExtra_']").each(function () {
            if ($(this).attr("id") != 'idExtra_' + extraPosition) {
                if ($(this).val() == $('#idExtra_' + extraPosition).val()) {
                    alert('La identificaci\u00f3n ya ha sido ingresada.\n Por favor ingrese una diferente');
                    documentDuplicated = true;
                }
            }
        });

        if (!documentDuplicated) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
                type: "post",
                data: ({txtDocument: documentExtra}),
                success: function (customerExists) {
                    $.ajax({
                        url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                        type: "post",
                        data: ({txtDocument: documentExtra}),
                        success: function (customerType) {
                            if (customerType != 'PERSON' && customerType != 'NOCUSTOMER') {
                                alert('El cliente seleccionado no es una persona natural y por lo tanto no puede constar como viajero.');
                                clearExtraInfo(extraPosition);
                                $("#idExtra_" + extraPosition).val('');
                                setDefaultCalendar($("#birthdayExtra_" + extraPosition), '-100y', '+0d', '-20y');
                                displayQuestionsForExtra(extraPosition);
                            } else {
                                reloadExtraInfo(documentExtra, extraPosition);
                            }
                        }
                    });
                }
            });
        } else {
            clearExtraInfo(extraPosition);
        }
    } else {
        clearExtraInfo(extraPosition);
        alert("La identificaci\u00f3n ingresada es igual a la del titular.\n Ingrese una identificaci\u00f3n distinta");
    }
}

function clearExtraInfo(extraPosition) {
    $('#idExtra_' + extraPosition).val('');
    $("#nameExtra_" + extraPosition).val('');
    $("#lastnameExtra_" + extraPosition).val('');
    $("#birthdayExtra_" + extraPosition).val('');
    $("#emailExtra_" + extraPosition).val('');
}

function reloadExtraInfo(documentExtra, extraPosition) {
    loadExtraIntoPosition(documentExtra, extraPosition);
}

function addOneExtra(documentExtra) {
    extraPosition = parseInt($('#count').val());

    if (extraPosition >= $("#selProduct option:selected").attr("max_extras_pro")) {
        alert('El producto seleccionado no admite m\u00e1s extras.');
        return false;
    }

    $('#extrasContainer').append('<div id="extra_' + extraPosition + '" class="span-24 last line"></div>');
    loadExtraIntoPosition(documentExtra, extraPosition);
    $('#count').val(extraPosition + 1);
}

function loadExtraIntoPosition(documentExtra, extraPosition) {
    $('#extra_' + extraPosition).load(
            document_root + "rpc/loadsSales/loadExtrasInfo.rpc", {
                document_cus: documentExtra,
                extraPosition: extraPosition,
                serialSale: $("#hdnSerial_sal").val(),
                serial_pro: $('#selProduct option:selected').attr('serial_pro'),
                serial_cou: $('#hdnSerial_cou').val(),
                serial_pxc: $('#selProduct option:selected').attr('serial_pxc'),
                editionAllowed: $("#hdnModify").val()},
            function () {
                displayQuestionsForExtra(extraPosition);
                setDefaultCalendar($("#birthdayExtra_" + extraPosition), '-100y', '+0d', '-20y');
                $('#birthdayExtra_' + extraPosition).attr("readonly", false);
                $("#birthdayExtra_" + extraPosition).change(function () {
                    displayQuestionsForExtra(extraPosition);
                    calculateAllExtrasPrices();
                });

                //ADD LISTENERS:
                $("#selRelationship_" + extraPosition).change(function () {
                    hasSpouse(extraPosition);
                    calculateAllExtrasPrices();
                });
                $("#emailExtra_" + extraPosition).change(function () {
                    if (checkEmailExtraIsDuplicated(extraPosition) != 0) {
                        $("#emailExtra_" + extraPosition).val('');
                    }
                });
                $("#idExtra_" + extraPosition).change(function () {
                    checkDocumentAndLoadExtra(extraPosition, $(this).val());
                });
                calculateAllExtrasPrices();
                addExtraValidators(extraPosition);

                //BUTTONS:
                $('#btnDeleteExtra').css('display', 'block');
                $('#btnResetExtra').css('display', 'block');
            }
    );
}

function deleteOneExtra() {
    var extraPosition = parseInt($('#count').val()) - 1;
    removeExtraValidators(extraPosition);
    $('#extra_' + extraPosition).remove();
    $('#count').val(extraPosition);
    if ($('#count').val() == 0) {
        $('#btnDeleteExtra').css('display', 'none');
    }
    calculateAllExtrasPrices();
}

function displayMyExtras() {
    $.ajax({
        url: document_root + "modules/sales/modifications/ajaxGetExtrasIds.php",
        type: "post",
        data: ({serialSale: $("#hdnSerial_sal").val()}),
        success: function (extrasIds) {
            if (extrasIds != 'false') {
                extrasIds = jQuery.trim(extrasIds);
                singleIds = extrasIds.split("%%%");
                for (posId = 0; posId < singleIds.length; posId++) {
                    addOneExtra(singleIds[posId]);
                }
            }
        }
    });
}

function setupExtrasSection() {
    $.ajax({
        url: document_root + "rpc/remoteValidation/sales/checkExtras.rpc",
        type: "post",
        data: ({serial_pro: $("#selProduct option:selected").attr("serial_pro")}),
        success: function (extrasAdmitted) {
            if (extrasAdmitted == 'true') {
                $('#extrasGlobalSection').show();
                setupExtrasLayout();
            } else {
                $('#extrasGlobalSection').hide();
                $('#addNew').hide();
                resetExtras();
                $('#count').val('0');
            }
        }
    });
}

function setupExtrasLayout() {
    $('#btnAdd').attr('disabled', false);
    $('#btnAdd').unbind('click');
    $('#btnDeleteExtra').unbind('click');
    $('#btnResetExtra').unbind('click');

    $('#addNew').show();
    $('#btnAdd').bind("click", function () {
        addOneExtra();
    });
    $('#extrasContainer').show();
    $('#btnDeleteExtra').bind("click", function () {
        deleteOneExtra();
    });
    $('#btnResetExtra').bind("click", function () {
        resetExtras();
    });
    setExtrasTotalPriceToZero();
    displayMyExtras();
}

function resetExtras() {
    $('#btnAdd').attr('disabled', false);
    var extraPosition = parseInt($('#count').val());
    extraPosition--;
    for (extraPosition; extraPosition >= 0; extraPosition--) {
        removeExtraValidators(extraPosition);
        $('#extra_' + extraPosition).remove();
        $('#count').val(extraPosition);
        if (extraPosition == 0) {
            $('#btnDeleteExtra').css('display', 'none');
            $('#btnResetExtra').css('display', 'none');
        }
    }
    calculateAllExtrasPrices();
}

function addExtraValidators(extraPosition) {
    $("#idExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#selRelationship_" + extraPosition).rules('add', {
        required: true
    });
    $("#nameExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#lastnameExtra_" + extraPosition).rules('add', {
        required: true
    });
    $("#birthdayExtra_" + extraPosition).rules('add', {
        required: true
    });
}

function removeExtraValidators(extraPosition) {
    $("#idExtra_" + extraPosition).rules("remove");
    $("#selRelationship_" + extraPosition).rules("remove");
    $("#nameExtra_" + extraPosition).rules("remove");
    $("#lastnameExtra_" + extraPosition).rules("remove");
    $("#birthdayExtra_" + extraPosition).rules("remove");
}

function doTravelDatesVerifications(extraPosition) {
    validateDates();
    evaluateCardDates();
}

function initializeMainSalesForm(formName) {
    $("#hdnPrice").attr('editable', 'YES');

    $.validator.addMethod("adultsOnlyValidator", function (customerId, element) {
        return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"), 'ADULTS', 'ADULTS');
    }, "Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.");

    $.validator.addMethod("childrenOnlyValidator", function (customerId, element) {
        return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"), 'CHILDREN', 'CHILDREN');
    }, "Los acompa&ntilde;antes s&oacute;lo pueden ser menores.");

    $.validator.addMethod("seniorExtrasValidator", function (customerId, element) {
        return checkAgeValidationsSales($("#selProduct option:selected").attr("extras_restricted_to_pro"), 'NO', 'SENIOR');
    }, "Este producto no admite adultos mayores.");

    $("#" + formName).validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            sale_type: {
                required: true
            },
            txtCardNum: {
                required: true
            },
            txtCodCounter: {
                required: true
            },
            txtEmissionPlace: {
                required: true
            },
            txtEmissionDate: {
                required: {
                    depends: function (element) {
                        var manual = $("#txtEmissionDate").attr("readonly");
                        if (manual == true)
                            return false;
                        else
                            return true;
                    }
                }//,
                //dateLessThan: function(){return $('#hdnEmissionDate').val()}
            },
            txtNameCounter: {
                required: true
            },
            txtDealer: {
                required: true
            },
            txtDocumentCustomer: {
                required: true,
                alphaNumeric: true,
                adultsOnlyValidator: true,
                childrenOnlyValidator: true,
                seniorExtrasValidator: true,
                notEqualTo: "#repDocument"
            },
            selType: {
                required: true
            },
            txtNameCustomer: {
                required: true,
                textOnly: true
            },
            txtLastnameCustomer: {
                required: {
                    depends: function (element) {
                        return $("#selType").val() == 'PERSON';
                    }
                },
                textOnly: true
            },
            rdGender: {
                required: {
                    depends: function(element) {
                            return $("#selType").val() == 'PERSON';
                    }
                }
            },
            selCountry: {
                required: true
            },
            selCityCustomer: {
                required: true
            },
            txtBirthdayCustomer: {
                required: {
                    depends: function (element) {
                        return $("#selType").val() == 'PERSON';
                    }
                },
                date: true
            },
            txtAddress: {
                required: true
            },
            txtPhone1Customer: {
                required: true,
                digits: true
            },
            txtPhone2Customer: {
                digits: true
            },
            txtCellphoneCustomer: {
                digits: true
            },
            txtEmailInvoice: {
                required: false,
                email: false
            },
            txtMailCustomer: {
               required: false,
                email: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/customer/checkCustomerEmail.rpc",
                    type: "POST",
                    data: {
                        serialCus: function () {
                            return $("#serialCus").val();
                        },
                        txtMail: function () {
                            return $('#txtMailCustomer').val();
                        }
                    }
                }
            },
            txtRelative: {
                required: {
                    depends: function (element) {
                        return $("#divRelative").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            txtPhoneRelative: {
                required: {
                    depends: function (element) {
                        return $("#divRelative").css('display') != 'none';
                    }
                },
                digits: true
            },
            txtManager: {
                required: {
                    depends: function (element) {
                        return $("#divManager").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            txtPhoneManager: {
                required: {
                    depends: function (element) {
                        return $("#divManager").css('display') != 'none';
                    }
                },
                digits: true
            },
            txtDepartureDate: {
                required: true,
                date: true
            },
            txtArrivalDate: {
                required: true,
                date: true
            },
            selCountryDestination: {
                required: {
                    depends: function (element) {
                        return $("#divDestination").css('display') != 'none';
                    }
                }
            },
            selCityDestination: {
                required: {
                    depends: function (element) {
                        return $("#divDestination").css('display') != 'none';
                    }
                }
            },
            selProduct: {
                required: true
            },
            selCounter: {
                required: true
            },
            repDocument: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                alphaNumeric: true,
                notEqualTo: "#txtDocumentCustomer"
            },
            repName: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            repLastname: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                textOnly: true
            },
            repBirthday: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                },
                date: true
            },
            repEmail: {
//                required: {
//                    depends: function (element) {
//                        return $("#repContainer").css('display') != 'none';
//                    }
//                },
                email: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/customer/checkCustomerEmail.rpc",
                    type: "POST",
                    data: {
                        serialCus: function () {
                            return $("#repSerialCus").val();
                        },
                        txtMail: function () {
                            return $('#repEmail').val();
                        }
                    }
                }
            },
            repCountry: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                }
            },
            repCityCustomer: {
                required: {
                    depends: function (element) {
                        return $("#repContainer").css('display') != 'none';
                    }
                }
            }
        },
        messages: {
            txtCardNum: {
                remote: 'El n&uacute;mero de tarjeta ingresado ya esta vendido o no le corresponde.'
            },
            txtEmissionDate: {
                dateLessThan: 'La fecha de emisi&oacute;n no puede ser mayor a la fecha actual.'
            },
            txtNameCustomer: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
            },
            repName: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre - Representante'."
            },
            txtLastnameCustomer: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
            },
            repLastname: {
                textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido - Representante'."
            },
            txtDocumentCustomer: {
                remote: "La identificaci&oacute;n ingresada ya existe.",
                alphaNumeric: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Identificaci&oacute;n'",
                adultsOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.",
                childrenOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser menores.",
                seniorExtrasValidator: "Este producto no admite adultos mayores.",
                notEqualTo: "La identificaci&oacute;n del Titular no puede ser la misma que la del Representante Legal"
            },
            rdGender: {
                    required: 'El campo "G&eacute;nero" es obligatorio.'
            },
            repDocument: {
                alphaNumeric: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Identificaci&oacute;n - Representante'",
                notEqualTo: "La identificaci&oacute;n del Representante Legal no puede ser la misma que la del Titular"
            },
            txtMailCustomer: {
                email: "Por favor ingrese un correo con el formato nombre@dominio.com",
                remote: "El 'E-mail - Representante' ingresado ya existe en el sistema."
            },
            repEmail: {
                email: "Por favor ingrese un correo con el formato nombre@dominio.com",
                remote: "El E-mail ingresado ya existe en el sistema."
            },
            txtBirthdayCustomer: {
                date: "El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
            },
            repBirthday: {
                date: "El campo 'Fecha de Naciemiento - Representante' debe tener el formato dd/mm/YYYY"
            },
            txtDepartureDate: {
                date: "El campo 'Fecha de Salida' debe tener el formato dd/mm/YYYY",
                lessOrEqualTo: 'La Fecha de Salida debe ser menor o igual que la Fecha de Llegada'
            },
            txtArrivalDate: {
                date: "El campo 'Fecha de Llegada' debe tener el formato dd/mm/YYYY",
                greaterOrEqualTo: 'La Fecha de Llegada debe ser mayor o igual que la Fecha de Salida'
            },
            txtPhone1Customer: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'"
            },
            txtPhone2Customer: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono 2'"
            },
            txtCellphoneCustomer: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Celular'"
            },
            txtRelative: {
                textOnly: 'El campo "Familiar" debe tener s&oacute;lo caracteres.'
            },
            txtPhoneRelative: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Familiar'"
            },
            txtManager: {
                textOnly: 'El campo "Gerente" debe tener s&oacute;lo caracteres.'
            },
            txtPhoneManager: {
                digits: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Gerente'"
            }
        },
        submitHandler: function (form) {
            //DOUBLE CONTROL TO AVOID SUBMITTING SALES WITH ZERO VALUES DUE TO AJAX-PRICE-UPDATE EXTENSIVE TIME
            if ($('#hdnTotal').val() == '' ||
                    $('#hdnTotal').val() == 'NaN' ||
                    parseInt($('#hdnTotal').val()) <= 0 ||
                    parseInt($('#hddDays').val()) <= 0) {
                alert('La tarifa del producto aun no ha sido calculada.\nPor favor revise el valor de la misma antes de registrar la venta');
                return false;
            }

            if ($('#hdnStatus').val() == 'INACTIVE') {
                alert('La venta de cualquier producto est\u00E1 prohibida a clientes en Lista Negra.');
                return false;
            }

            //CUSTOMER PROMOTION VALIDATION
            var partial_total = 0;
            if ($('#hdnPromoDiscount').length > 0 && $('#hdnPromoDiscount').val() != '') {
                partial_total = parseFloat($('#hdnTotal').val());
                partial_total -= parseFloat($('#hdnPromoDiscount').val());
            } else {
                partial_total = $('#hdnTotal').val();
            }

            if (!confirm('Esta seguro que desea registrar la venta?\nTarifa Final: ' + partial_total)) {
                return false;
            }

            //IF SALE HAS AN INVOICE, DAYS SHOULD NOT BE LESS THAN ORIGINAL!
            if ($("#hdnModify").val() == 'NO') {
                var travelDays = parseInt($('#txtDaysBetween').html());
                var originalTravelDays = parseInt($('#hdnBackupDays').val());
                if (travelDays > originalTravelDays) {
                    alert('No puede seleccionar mas de ' + $("#hdnBackupDays").val() + 'dias de viaje.');
                    return false;
                }
            }

            /*
             * ONLY ALLOW LEGAL ENTITIES IF PRODUCT IS:
             flights_pro = 0 
             generate_number_pro = 'NO'
             */
            if ($("#selProduct option:selected").attr("generate_number_pro") != 'NO'
                    || parseInt($("#selProduct option:selected").attr("flights_pro")) > 0) {
                //NO LEGAL ENTITIES ALLOWED!!
                if ($("#selType").val() != 'PERSON') {
                    alert('El cliente no puede ser una persona juridica.');
                    return false;
                }
            }

            $('#btnRegistrar').attr('disabled', 'true');
            $('#btnRegistrar').val('Espere por favor...');
            if ($("#selProduct option:selected").attr("flights_pro") == '0') {
                $('#hddDays').val($("#selDayNumber option:selected").attr("duration_ppc"));
            }
            form.submit();
        }
    });

    if ($("#hdnModify").length == 1) {
        if (parseInt($('#selProduct :selected').attr('flights_pro')) == 0 && $("#hdnModify").val() == 'NO') {
            $("#txtDepartureDate").attr('readonly', 'true');
            $("#txtArrivalDate").attr('readonly', 'true');
        } else {
            setDefaultCalendar($("#txtDepartureDate"), '+0d', '+100y');
            setDefaultCalendar($("#txtArrivalDate"), '+0d', '+100y');
        }
    } else {
        setDefaultCalendar($("#txtDepartureDate"), '+0d', '+100y');
        setDefaultCalendar($("#txtArrivalDate"), '+0d', '+100y');
    }
}

function switchCustomerType(newType) {
    switch (newType) {
        case 'LEGAL_ENTITY':
            $("#customerNameLabel").html('* Raz&oacute;n Social:');
            $("#customerLastNameLabelContainer").css("display", 'none');
            $("#customerBirthdateContainer").css("display", 'none');
            $("#customerContactLabel").html('* Gerente:');
            $("#customerContactPhoneLabel").html('* Tel&eacute;fono del gerente:');
            $("#genderDiv").hide();
            $('[id^="rdGender"]').attr('checked', false);
            break;
        default: //PERSON TYPE OR UNDEFINED VALUE
            $("#customerNameLabel").html('* Nombre:');
            $("#customerLastNameLabelContainer").css("display", 'block');
            $("#customerBirthdateContainer").css("display", 'block');
            $("#customerContactLabel").html('* Contacto de emergencia:');
            $("#customerContactPhoneLabel").html('* Tel&eacute;fono del contacto:');
            $("#genderDiv").show();
    }
}

/*********************************************************************************************
 ************************************** CUSTOMER PROMO SECTION ********************************
 **********************************************************************************************/

function bindingsForCustomerPromo(button_selector, text_selector, container_selector) {
    $(button_selector).bind('click', function () {
        validateCuponCode($(text_selector).val(), container_selector);
    });

    $(text_selector).bind('keypress', function () {
        clearCustomerPromoContainer();
    });
}

function validateCuponCode(promo_code, container_selector) {
    var serial_pbd = $('#selProduct').val();
    if (promo_code != '' && serial_pbd != '') {
        $(container_selector).load(document_root + 'rpc/loadClientPromo/validateCupon.rpc', {
            cupon: promo_code,
            serial_pbd: serial_pbd
        }, function () {

        });
    } else {
        $(container_selector).html('<i>Ingrese un c&oacute;digo y seleccione un producto antes de continuar</i>');
    }
}

function clearCustomerPromoContainer() {
    $('#customerPromo_container').html('');
    $('#txtCoupon').val('');
}

function enablePromoValidationButton() {
    $('#txtCoupon').bind('keypress', function () {
        $('#customerPromo_container').html('');
    });

    $('#btnCustomerPromoButon').bind('click', function () {
        $('#promo_loader').css('display', 'inline');
        var promo_code = $('#txtCoupon').val();
        var serial_pxc = $('#selProduct :selected').attr('serial_pxc');
        var total_sal = $('#hdnTotal').val();
        var symbol = $('[name="rdCurrency"]:checked').attr('symbol');

        if (promo_code != '' && serial_pxc != '' && serial_pxc) {
            if (total_sal != '' && total_sal > 0) {
                $('#customerPromo_container').load(document_root + 'rpc/loadsSales/validateCoupon.rpc',
                        {
                            promo_code: promo_code,
                            serial_pxc: serial_pxc,
                            total_sal: total_sal,
                            symbol: symbol
                        }, function () {
                    $('#promo_loader').css('display', 'none');
                });
            } else {
                $('#promo_loader').css('display', 'none');
                alert('Por favor cotice completamente un producto antes de aplicar un cup\u00F3n.');
            }
        } else {
            $('#promo_loader').css('display', 'none');
            alert('Se debe seleccionar un producto e ingresar un c\u00F3digo para continuar.');
        }
    });
}

function evaluateSelection(selection) {
    if (selection == 'YES') {
        $('#generalCustomerPromoContainer').show();
    } else {
        $('#generalCustomerPromoContainer').hide();
        clearCustomerPromoContainer();
    }
}

function loadMassiveProductsBasedOnCounter(serialCounter) {
    $('#productsContainer').load(document_root + "rpc/loadsSales/loadProducts.rpc", {counter: serialCounter, masive: 'YES'});
}

function countPeopleInCard() {
    return parseInt($('#count').val()) + 1;
}

function validateDestination() {
    var serial_pxc = $("#selProduct option:selected").attr("serial_pxc");
    var serial_cou = $('#selCountryDestination').val();

    if (serial_pxc && serial_cou) {
        show_ajax_loader();
        $.ajax({
            url: document_root + "rpc/remoteValidation/sales/checkDestination.rpc",
            type: "post",
            data: ({serial_pxc: serial_pxc, serial_cou: serial_cou}),
            dataType: "json",
            success: function (valid_destination) {
                if (!valid_destination) {
                    $('#selCountryDestination').val('');
                    $('#selCityDestination').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
                    alert('El producto seleccionado no puede utilizarse para el destino actual.');
                }

                hide_ajax_loader();
            }
        });
    }
}

function loadRepCustomer(docCust) {
    show_ajax_loader();
    $.ajax({
        url: document_root + "rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({txtDocument: docCust}),
        success: function (customerExists) {
            $.ajax({
                url: document_root + "rpc/remoteValidation/customer/checkCustomerType.rpc",
                type: "post",
                data: ({txtDocument: docCust}),
                success: function (customerType) {
                    $('#repContainer').load(document_root + "rpc/loadsSales/loadCustomerComplete.rpc",
                            {
                                document_cus: docCust,
                                repContainer: 'TRUE'
                            }, function () {
                        if (customerType == 'LEGAL_ENTITY') {
                            clearRepForm();

                            alert('No se puede ingresar a una Persona Juridica como Representante Legal');
                        }

                        $('#repDocument').change(function () {
                            loadRepCustomer($(this).val());
                        });

                        hide_ajax_loader();
                    });
                }
            });

            $("#txtBirthdayCustomer").change(enableQuestionsDiv);
        }
    });
}

function clearRepForm() {
    $('#repDocument').val("");
    $('#repSerialCus').val("");
    $('#repName').val("");
    $('#repLastname').val("");
    $('#repBirthday').val("");
    $('#repEmail').val("");
    $('#repCountry').val("");
    $('#repCityCustomer').val("");
}