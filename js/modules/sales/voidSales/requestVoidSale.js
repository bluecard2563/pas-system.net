// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmVoidRequest").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
				txtCardNumber: {
                    required: true,
                    number: true
                },
                txtObservation: {
                	required: true
                },
                selCountry: {
                	required:{depends: function(element) {return $('input[name="checkVoidType"]:checked').attr('id') != 'radioSingleCard';}}
                },
                selManager: {
                	required:{depends: function(element) {return $('input[name="checkVoidType"]:checked').attr('id') != 'radioSingleCard';}}
                },
                selComissionist: {
                	required:{depends: function(element) {return $('input[name="checkVoidType"]:checked').attr('id') != 'radioSingleCard';}}
                },
                selDealerDea: {
                	required:{depends: function(element) {return $('input[name="checkVoidType"]:checked').attr('id') != 'radioSingleCard';}}
                },
                selBranch: {
                	required:{depends: function(element) {return $('input[name="checkVoidType"]:checked').attr('id') != 'radioSingleCard';}}
                }
            },
            messages:{
            	txtCardNumber: {
                    number: 'El campo "N&uacute;mero de tarjeta" debe ser num&eacute;rico.'
                }
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
	});
    
    $("#radioSingleCard").click(function(){
    	$("#txtCardNumber").removeAttr('disabled');
    	$('#requestResults').html('');
    });
    
    $("#radioMultipleCards").click(function(){
    	$("#txtCardNumber").val('');
    	$("#txtCardNumber").attr('disabled', 'disabled');
    });
    
    //SUBMIT MAIN FORM:
    $("#btnRequestVoid").click(function(){
    	if($("#frmVoidRequest").valid()){
    		if($('input[name="checkVoidType"]:checked').attr('id') == 'radioSingleCard'){ //VOID ONLY ONE CARD
        		$.ajax({
                    url: document_root+"modules/sales/voidSales/pRequestSingleVoidSale",
                    type: "post",
                    data: ({txtCardNumber : $('#txtCardNumber').val(),
                    		isChecked : 'FALSE',
                    		observation : $('#txtObservation').val()}),
                    success: function(transactionResponse) {
        				if(transactionResponse.length < 20){
        					myUrl = window.location + '';
        					myPos = myUrl.indexOf("requestVoidSale");
        					
        					cleanUrl = myUrl.substr(0, myPos + 15);
        					window.location = cleanUrl + '/' + transactionResponse
        				}else{
        					alert('Hubo un error al realizar la solicitud, por favor contacte al administrador');
        				}
                    }
        		});
        	}else{//VOID MULTIPLE CHECKED CARDS
        		$('#requestResults').html('Cargando Resultados...');
       			$('#requestResults').load(document_root+"rpc/loadsSales/retrieveCandidateVoidSales.rpc",{
       				selBranch: $('#selBranch').val()},
       				function(){
       					if($("#stockTable").length > 0) {
       						var itemsPerPAge = 10;
       						var items = parseInt($('#hdItemsCount').val());
       						var pages_number=parseInt(items/itemsPerPAge) + 1;
       	                    pager = new Pager('stockTable', itemsPerPAge , 'Siguiente', 'Anterior');
       	                    pager.init(pages_number); //Max Pages
       	                    pager.showPageNav('pager', 'pageNavPositionLocation'); //Div where the navigation buttons will be placed.
       	                    pager.showPage(1); //Starting page
       	                }
       				}
       			);
        	}
    	}
    });
});

function loadCountry(serial_zon){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
		$('#selCountry').bind("change",function(){
				loadManagersByCountry(this.value);
		});

		if($('#selCountry option').size()<=2){
			loadManagersByCountry($('#selCountry').val());
		}
	});
}

function loadManagersByCountry(serial_cou){
	if(serial_cou){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
			$('#selManager').bind('change', function(){
            	loadComissionists($(this).val());
            });
		});
	}else{
    	loadComissionists('');
    }
}

/* LOAD COMISSIONISTS FROM MANAGER
 */
function loadComissionists(managerId){
    if(managerId){
        $('#comissionistsContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: managerId},function(){
            $("#selComissionist").bind('change', function(){
            	loadDealers();
            });
        });
    }else{
    	$('#comissionistsContainer').html('Seleccione un representante');
    	loadDealers();
    }
}


/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val() && $('#selComissionist').val()){
		$('#dealerContainer').load(document_root+"rpc/loadDealersByManagerAndRepresentante.rpc",{
				serial_mbc: $('#selManager').val(), 
				serial_usr: $('#selComissionist').val() },
				function(){
					$('#selDealerDea').attr('title','El campo "Comercializador" es obligatorio.');
					$('#selDealerDea').bind("change",function(){
						loadBranches($(this).val());
				    });
					loadBranches('');
				}
		);
    }else{
    	$('#dealerContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}