// JavaScript Document
$(document).ready(function(){				 	
	/*VALIDATION SECTION*/
	$("#frmSearchLanguage").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameLanguage: {
				required: true
			}		
		}
	});
	/*END VALIDATION SECTION*/
	
	var ruta=document_root+"rpc/autocompleters/loadLanguages.rpc";
	$("#txtNameLanguage").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#serial_lang").val(row[1]);
	});
	
});