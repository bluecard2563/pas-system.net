// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewLanguage").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameLanguage: {
				required: true,
				textOnly: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/language/checkNameLanguage.rpc",
					type: "post"
				}
			},
			txtCodeLanguage: {
				required: true,
				textOnly: true,
				maxlength: 4,
				remote: {
					url: document_root+"rpc/remoteValidation/language/checkCodeLanguage.rpc",
					type: "post"
				}
			}			
		},
		messages: {
			txtNameLanguage: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Idioma'.",
				maxlength: 'El idioma debe tener m&aacute;ximo 30 caracteres.',
				remote: 'El idioma ingresado ya existe.'
			},
			txtCodeLanguage: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'C&oacute;digo'.",
				maxlength: 'El c&oacute;digo debe tener m&aacute;ximo 4 caracteres.',
				remote: 'El c&oacute;digo ingresado ya existe.'
			}
		}
	});
	/*END VALIDATION SECTION*/
});