// JavaScript Document
$(document).ready(function(){
	/*AUTOCOMPLETER*/
	$("#txtPattern").autocomplete(document_root+"rpc/autocompleters/loadDealerTypes.rpc",{
		extraParams: {
			serial_lang: function(){ return $("#selLanguage").val()}
                },
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnDealTypeID").val(row[1]);
		
	});
	/*END AUTOCOMPLETER*/
	
	/*VALIDATION SECTION*/
	$("#frmSearchDealerType").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selLanguage: {
				required: true
			},
			txtPattern: {
				required: true
			}		
		}
	});
	/*END VALIDATION*/
});