// JavaScript Document
$(document).ready(function(){
	$("#frmNewDealerType").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtAlias: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/dealer/checkDealerType.rpc",
					type: "post"
				}
			},
			txtDesc: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/dealer/dealerType/checkDescriptionDealerType.rpc",
					type: "post",
					data: {
						serial_lang: function() {
							return $('#serialLanguage').val();
						}
					}
				}
			}
		},
		messages: {
			txtAlias: {
				remote: 'El tipo de comercializador ingresado ya existe en el sistema.'
			},
			txtDesc: {
				remote: "El nombre que ingreso ya existe para otro tipo de comercializador."
			}
		}
	});
});