// JavaScript Document
$(document).ready(function(){
	$("#newInfo").css("display","none");
	$("input[name='rdoStatus']").bind('click',function(){changeStatus();});
	/*VALIDATION SECTION*/
	$("#frmTranslateDealerType").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selLanguage: {
				required: true
			},
			txtDesc: {
				required: true,
				textOnly: true,
				remote: {
					url: document_root+"rpc/remoteValidation/dealer/dealerType/checkDescriptionDealerType.rpc",
					type: "post",
					data: {
						serial_lang: function() {
							return $('#selLanguage').val();
						}
					}
				}
			}					
		},
		messages: {
			txtDesc: {				
				textOnly: "S&oacute;lo se aceptan caracteres en el campo Nombre.",
				remote: "La traducci&oacute;n que ingreso ya existe para otro tipo de comercializador."
			}
		}
	});

	/*END VALIDATION SECTION*/	   
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 410,
		modal: true,
		buttons: {
			'Guardar': function() {
				  $("#frmUpdateDealerType").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtDesc: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/dealer/dealerType/checkDescriptionDealerType.rpc",
									type: "post",
									data: {
										serial_lang: function() {
											return $('#serialLanguage').val();
										},
										serial_delaerTyByLang: function() {
											return $('#serialDealerTyByLang').val();
										}
									}
								}
							}
						},
						messages: {
							txtDesc: {
								remote: "La traducci&oacute;n que ingreso ya existe para otro tipo de comercializador."
							}
						}
					});
				  
				   if($('#frmUpdateDealerType').valid()){
						$('#frmUpdateDealerType').submit();  
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});
});

function changeStatus(){
	idDealerType=$('#hdnSerialDealerType').val();
	$.ajax({
		   url: document_root+"rpc/remoteValidation/dealer/dealerType/checkUnusedDealerType.rpc",
		   data:{serial_delaerTyByLang:idDealerType},
		   success: function(data){
			   			if(data){
							if(confirm("�Est\u00e1 seguro de que quiere cambiar el estado del tipo de comercializador?")){
								$('#frmUpdateDealerTypeStatus').submit();
							}
						}
						else {
							alert("Este tipo de comercializador esta siendo usado. Es imposible deshabilitarlo.");
							$('#rdoStatusAct').attr("checked", true);
						}
					},
		   error: function(){
			   			alert("Ocurrio un error.");
					}
	});
}

function loadFormData(itemID){
	$('#serialDealerTyByLang').val(itemID);
	$('#dealName').html($('#alias_'+itemID).html());
	$('#language').html($('#language_'+itemID).html());
	$('#txtDesc').val($('#name_'+itemID).html());
	$('#serialLanguage').val($('#serial_lang_'+itemID).val());
}