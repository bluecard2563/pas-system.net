// JavaScript Document
$(document).ready(function() {
    $("#selCountry").bind("change", changeFlag);
    $("#txtCode").bind("change", changeFlag);
    var today = $('#today').val();

    /*VALIDATION SECTION*/
    $("#frmNewDealer").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountry: {
                required: true
            },
            selCity: {
                required: true
            },
            selSector: {
                required: true
            },
            selManager: {
                required: true
            },
            txtID: {
                required: true,
                alphaNumeric: true,
                // remote: {
                    // url: document_root + "rpc/remoteValidation/dealer/checkValidDocument.rpc",
                    // type: "post",
                    // data: {
                        // country: function() {
                            // return $('#selCountry').val();
                        // }
                    // }
                // }
            },
            txtCode: {
                required: true,
                digits: true,
                minlength: 4,
                maxlength: 4
            },
            hdnFlagCode: {
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerCode.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        },
                        txtCode: function() {
                            return $("#txtCode").val();
                        }
                    }
                }
            },
            selDealerT: {
                required: true
            },
            selCategory: {
                required: true
            },
            txtName: {
                required: true
            },
            txtAddress: {
                required: true
            },
            txtPhone1: {
                required: true,
                number: true
            },
            txtPhone2: {
                number: true,
                notEqualTo: "#txtPhone1"
            },
            txtContractDate: {
                required: true,
                date: true,
                dateLessThan: today

            },
            txtFax: {
                number: true
            },
            txtMail1: {
                required: true,
                email: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerEmail.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        }
                    }
                }
            },
            txtMail2: {
                required: true,
                email: true,
                //notEqualTo: "#txtMail1",
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerEmail.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        }
                    }
                }
            },
            txtContact: {
                required: true,
                textOnly: true
            },
            txtContactPhone: {
                required: true,
                number: true
            },
            txtContactMail: {
                required: true,
                email: true
            },
            txtVisitAmount: {
                required: true,
                number: true,
                min: 1,
                max: 20
            },
            selStatus: {
                required: true
            },
            selCreditD: {
                required: true
            },
            txtContactPay: {
                required: true,
                textOnly: true
            },
            chkBillTo: {
                required: true
            },
            selPayment: {
                required: true
            },
            txtPercentage: {
                required: true,
                max_value: 100,
                percentage: true

            },
            selType: {
                required: true
            },
            txtAssistName: {
                required: true,
                textOnly: true
            },
            txtAssistEmail: {
                required: true,
                email: true
            },
            chkBonusTo: {
                required: true
            },
            hdnSerialUsr: {
                required: true
            },
            txtNameManager: {
                required: true,
                textOnly: true
            },
            txtPhoneManager: {
                required: true,
                number: true
            },
            txtMailManager: {
                required: true,
                email: true
            },
            txtBirthdayManager: {
                date: true
                        // dateLessThan: today
            },
            txtAccountNumber: {
                digits: true
            },
            txtMinimumKits: {
                max_value: 50
            }
        },
        messages: {
            txtName: {
                alphaNumeric: 'El campo "Nombre" admite caracteres alfa-num&eacute;ricos.'
            },
            txtCode: {
                digits: 'El campo "C&oacute;digo" admite s&oacute;lo d&iacute;gitos.',
                minlength: 'El campo "C&oacute;digo" debe tener 4 d&iacute;gitos.',
                maxlength: 'El campo "C&oacute;digo" debe tener 4 d&iacute;gitos.'
            },
            hdnFlagCode: {
                remote: 'Existe otro comercializador con el mismo c&oacute;digo en el sistema para este pa&iacute;s.'
            },
            txtID: {
                alphaNumeric: 'El campo "Id" admite s&oacute;lo caracteres alfa-num&eacute;ricos.',
                remote: 'El campo "Id" es incorrecto. Por favor ingrese un RUC v&aacute;lido.'
            },
            txtPhone1: {
                number: 'El campo "Tel&eacute;fono #1" admite s&oacute;lo n&uacute;meros.'
            },
            txtPhone2: {
                number: 'El campo "Tel&eacute;fono #2" admite s&oacute;lo n&uacute;meros.',
                notEqualTo: 'Tel&eacute;fono 2: Ingrese un tel&eacute;fono diferente que el anterior.'
            },
            txtContractDate: {
                date: 'El campo "Fecha de contrato" debe tener formato fecha.',
                dateLessThan: 'La fecha de contrato no puede ser mayor a la fecha actual.'
            },
            txtFax: {
                number: 'El campo "Fax" admite s&oacute;lo n&uacute;meros.'
            },
            txtMail1: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #1"',
                remote: 'E-mail #1: El correo ingresado ya consta en el sistema.'
            },
            txtMail2: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #2"',
                notEqualTo: 'E-mail #2: Ingrese un e-mail diferente que el anterior.',
                remote: 'E-mail #2: El correo ingresado ya consta en el sistema.'
            },
            txtContact: {
                textOnly: 'El campo "Nombre del Contacto" admite s&oacute;lo texto.'
            },
            txtContactPhone: {
                number: 'El campo "Tel&eacute;fono del Contacto" admite s&oacute;lo n&uacute;meros.'
            },
            txtContactMail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Contacto"'
            },
            txtVisitAmount: {
                number: 'El campo "N&uacute;mero de visitas al mes" admite s&oacute;lo n&uacute;meros.',
                min: 'El campo "N&uacute;mero de visitas al mes" debe ser m&iacute;nimo 1.',
                max: 'El campo "N&uacute;mero de visitas al mes" admite solo hasta 20.'
            },
            txtContactPay: {
                textOnly: 'El campo "Contacto Pago" admite s&oacute;lo texto.'
            },
            txtPercentage: {
                percentage: 'El campo "Porcentaje" admite s&oacute;lo n&uacute;meros con dos decimales.',
                max_value: 'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
            },
            txtAssistName: {
                textOnly: 'El campo "Nombre (Asistencias)" admite s&oacute;lo texto.'
            },
            txtAssistEmail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail (Asistencias)"'
            },
            txtNameManager: {
                required: 'El campo de "Nombre del Gerente" es obligatorio.',
                textOnly: 'Ingrese solamente texto en el campo "Nombre del Gerente".'
            },
            txtPhoneManager: {
                required: 'El campo "Tel&eacute;fono del Gerente" es obligatorio.',
                number: 'Ingrese solamente n&uacute;meros en el campo "Tel&eacute;fono del Gerente".'
            },
            txtMailManager: {
                required: 'El campo de "E-mail del Gerente" es obligatorio.',
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Gerente".'
            },
            txtBirthdayManager: {
                date: 'El campo "Fecha de Nacimiento" debe tener formato fecha.'
                        //dateLessThan:'La Fecha de Nacimiento no puede ser mayor a la fecha actual.'
            },
            txtAccountNumber: {
                digits: 'El campo "N&uacute;mero de cuenta" acepta solo d&iacute;gitos.'
            },
            txtMinimumKits: {
                max_value: 'El campo "M&iacute;nimo de Stock" admite valores hasta 50%'
            }
        },
        submitHandler: function(form) {
            if ($("#frmNewDealer").valid()) {
                $("#btnRegister").attr('disabled', 'true');
                $("#btnRegister").val('Espere por favor...');
                form.submit();
            }
        }
    });
    /*END VALIDATION SECTION*/

    /*CALENDAR*/
    setDefaultCalendar($("#txtContractDate"), '-100y', '+0d', '-18');
    setDefaultCalendar($("#txtBirthdayManager"), '-100y', '+0d', '-18y');
    /*END CALENDAR*/

    /*CHANGE OPTIONS*/
    $("#selCountry").bind("change", function(e) {
        loadCities(this.value);
        loadCreditDays(this.value);
        loadManagersByCountry(this.value);
        checkPhoneSales(this.value);
    });

    $("#selCity").bind("change", function(e) {
        loadSector(this.value);
    });
    $("#selComissionist").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionist").val());
    });
    $("#selComissionistPa").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionistPa").val());
    });
    //If #selCountry is preloaded with only 1 element
    if ($("#selCountry option").size() <= 2) {
        loadCities($("#selCountry").val());
        loadCreditDays($("#selCountry").val());
        loadManagersByCountry($("#selCountry").val());
        checkPhoneSales($("#selCountry").val());
    }
    $("#chkOfficialSeller").bind("click", function(e) {
        if ($("#chkOfficialSeller").attr("checked")) {
            //if($("#hdnOfficialMbc").attr("value")=="YES") {
            $("#visitsContainer").css("display", "none");
            $("#hideSection").css("display", "none");
            $("#txtVisitAmount").val(0);
            $("#txtPercentage").val(0);
            $("#selType").val("COMISSION");
            $("#txtVisitAmount").rules("remove", "min");
            $("#txtPercentage").rules("remove", "percentage");
            //}
        } else {
            $("#visitsContainer").css("display", "block");
            $("#hideSection").css("display", "block");
            $("#txtVisitAmount").val("");
            $("#txtPercentage").val("");
            $("#selType").val("");
            $("#txtVisitAmount").rules("add", "min");
            $("#txtPercentage").rules("add", "percentage");
        }
    });
    $('#chkPhoneSales').bind("click", function() {

        if ($('#chkPhoneSales').attr("checked")) {
            $('#chkOfficialSeller').attr("checked", false);
            $('#officialSellerContainer').css("display", "none");
            $("#visitsContainer").css("display", "none");
            $("#hideSection").css("display", "none");
            $("#txtVisitAmount").val(0);
            $("#txtPercentage").val(0);
            $("#selType").val("COMISSION");
            $("#txtVisitAmount").rules("remove");
            $("#txtPercentage").rules("remove");
            $("#selType").rules("remove");
        } else {
            $('#officialSellerContainer').css("display", "block");
            $("#visitsContainer").css("display", "block");
            $("#hideSection").css("display", "block");
            $("#txtVisitAmount").val("");
            $("#txtPercentage").val("");
            $("#selType").val("");
            $("#txtVisitAmount").rules("add", {
                required: true,
                number: true,
                min: 1,
                max: 20,
                messages: {
                    number: 'El campo "N&uacute;mero de visitas al mes" admite s&oacute;lo n&uacute;meros.',
                    min: 'El campo "N&uacute;mero de visitas al mes" debe ser m&iacute;nimo 1.',
                    max: 'El campo "N&uacute;mero de visitas al mes" admite solo hasta 20.'
                }
            });
            $("#txtPercentage").rules("add", {
                required: true,
                percentage: true,
                max_value: 100,
                messages: {
                    percentage: 'El campo "Porcentaje" admite s&oacute;lo n&uacute;meros con dos decimales.',
                    max_value: 'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
                }
            });
            $("#selType").rules("add", {
                required: true
            });
        }
    });
    /*END CHANGE*/
});

function checkPhoneSales(countryID) {
    $.getJSON(document_root + "rpc/remoteValidation/dealer/checkPhoneSales.rpc", {serial_cou: countryID}, function(data) {
        if (data) {
            $('#phoneSalesContainer').css("display", "block");
        } else {
            $('#phoneSalesContainer').css("display", "none");
            $('#officialSellerContainer').css("display", "block");
            $("#visitsContainer").css("display", "block");
            $("#hideSection").css("display", "block");
            $("#txtVisitAmount").val("");
            $("#txtPercentage").val("");
            $("#selType").val("");
            $("#txtVisitAmount").rules("add", {
                required: true,
                number: true,
                min: 1,
                max: 20,
                messages: {
                    number: 'El campo "N&uacute;mero de visitas al mes" admite s&oacute;lo n&uacute;meros.',
                    min: 'El campo "N&uacute;mero de visitas al mes" debe ser m&iacute;nimo 1.',
                    max: 'El campo "N&uacute;mero de visitas al mes" admite solo hasta 20.'
                }
            });
            $("#txtPercentage").rules("add", {
                required: true,
                percentage: true,
                max_value: 100,
                messages: {
                    percentage: 'El campo "Porcentaje" admite s&oacute;lo n&uacute;meros con dos decimales.',
                    max_value: 'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
                }
            });
            $("#selType").rules("add", {
                required: true
            });
        }
    });
}

function changeFlag() {
    $('#hdnFlagCode').val($('#hdnFlagCode').val() + 1);
}

function loadCities(countryID) {
    $('#sectorContainer').load(document_root + "rpc/loadSectors.rpc");
    $('#cityContainer').load(document_root + "rpc/loadCities.rpc", {serial_cou: countryID},
    function() {
        $("#selCity").bind("change", function(e) {
            loadSector(this.value);
        });
        if ($("#selCity option").size() <= 2) {
            loadSector($("#selCity").val());
        }
    });
}

function loadSector(cityID) {
    $('#sectorContainer').load(document_root + "rpc/loadSectors.rpc", {serial_cit: cityID});
}

function loadCreditDays(countryID) {
    $('#creditDContainer').load(document_root + "rpc/loadCreditDays.rpc", {serial_cou: countryID});
}

function loadManagersByCountry(countryID) {
    $('#managerContainer').load(document_root + "rpc/loadManagersByCountry.rpc",
            {serial_cou: countryID},
    function() {
        $('#selManager').bind('change', function() {
            loadUsersByManager(this.value);
            $("#hdnSerialMbc").val($("#selManager").val());
        });
        if ($("#selManager option").size() <= 2) {
            loadUsersByManager($("#selManager").val());
            $("#hdnSerialMbc").val($("#selManager").val());
        }
    });
}
function loadUsersByManager(mbcID) {
    $('#comissionistContainer').load(document_root + "rpc/loadUsersByManager.rpc", {serial_mbc: mbcID});
    $('#officialSellerContainer').load(document_root + "rpc/hasOfficialSeller.rpc",
            {serial_mbc: $("#selManager").val()},
    function() {
        $("#chkOfficialSeller").bind("click", function(e) {
            if ($("#chkOfficialSeller").attr("checked")) {
                //if($("#hdnOfficialMbc").attr("value")=="YES") {
                $("#visitsContainer").css("display", "none");
                $("#hideSection").css("display", "none");
                $("#txtVisitAmount").val(0);
                $("#txtPercentage").val(0);
                $("#selType").val("COMISSION");
                $("#txtVisitAmount").rules("remove", "min");
                $("#txtPercentage").rules("remove", "percentage");
                //}
            } else {
                $("#visitsContainer").css("display", "block");
                $("#hideSection").css("display", "block");
                $("#txtVisitAmount").val("");
                $("#txtPercentage").val("");
                $("#selType").val("");
                $("#txtVisitAmount").rules("add", "min");
                $("#txtPercentage").rules("add", "percentage");
            }
        });
    });
}
