// JavaScript Document
$(document).ready(function() {
    var today = $('#today').val();

    if ($("#selComissionistPa").val() == '') {
        $("#selComissionistPa").attr('disabled', true);
        $("#rdoSelComissionistMa").attr('checked', true);
    }
    else {
        $("#selComissionistMa").attr('disabled', true);
        $("#rdoSelComissionistPa").attr('checked', true);
    }

    $("#selStatus").bind("change", function() {
        if ($("#selStatus").val() == "INACTIVE") {
            if (!confirm("Esto afectara a todo el sistema. Esta seguro de querer cambiar?")) {
                $("#selStatus").val("ACTIVE");
            }
        }
    });
    /*VALIDATION SECTION*/
    $("#frmUpdateDealer").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountry: {
                required: true
            },
            selCity: {
                required: true
            },
            selSector: {
                required: true
            },
            txtID: {
                required: true,
                alphaNumeric: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkValidDocument.rpc",
                    type: "post"
                }
            },
            txtCode: {
                required: true,
                digits: true,
                minlength: 4,
                maxlength: 4,
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerCode.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        },
                        serial_dea: function() {
                            return $("#serial_dea").val();
                        }
                    }
                }
            },
            selCategory: {
                required: true
            },
            txtName: {
                required: true
            },
            selDealerT: {
                required: true
            },
            txtAddress: {
                required: true
            },
            txtPhone1: {
                required: true,
                number: true
            },
            txtPhone2: {
                number: true
            },
            txtContractDate: {
                required: true,
                date: true,
                dateLessThan: today
            },
            txtFax: {
                number: true
            },
            txtMail1: {
                required: true,
                email: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerEmail.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        },
                        serial_dea: function() {
                            return $("#serial_dea").val();
                        },
                        serial_father: function() {
                            return $("#serial_dea").val();
                        }
                    }
                }
            },
            txtMail2: {
                required: true,
                email: true,
                //notEqualTo: "#txtMail1",
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerEmail.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        },
                        serial_dea: function() {
                            return $("#serial_dea").val();
                        },
                        serial_father: function() {
                            return $("#serial_dea").val();
                        }
                    }
                }
            },
            txtContact: {
                required: true,
                textOnly: true
            },
            txtContactPhone: {
                required: true,
                number: true
            },
            txtContactMail: {
                required: true,
                email: true
            },
            txtVisitAmount: {
                required: true,
                number: true,
                max: 20
            },
            selStatus: {
                required: true
            },
            selCreditD: {
                required: true
            },
            txtContactPay: {
                required: true,
                textOnly: true
            },
            chkBillTo: {
                required: true
            },
            selPayment: {
                required: true
            },
            txtPercentage: {
                required: true,
                max_value: 100,
                percentage: true
            },
            selType: {
                required: true
            },
            txtAssistName: {
                required: true,
                textOnly: true
            },
            txtAssistEmail: {
                required: true,
                email: true
            },
            hdnSerialUsr: {
                required: true
            },
            txtNameManager: {
                required: true,
                textOnly: true
            },
            txtPhoneManager: {
                required: true,
                number: true
            },
            txtMailManager: {
                required: true,
                email: true
            },
            txtBirthdayManager: {
                date: true
                        //dateLessThan: today
            },
            selComissionist: {
                required: true
            },
            txtAccountNumber: {
                digits: true
            },
            txtMinimumKits: {
                max_value: 50
            }
        },
        messages: {
            txtName: {
                alphaNumeric: 'El campo "Nombre" admite caracteres alfa-num&eacute;ricos.'
            },
            txtCode: {
                remote: 'Existe otro comercializador con el mismo c&oacute;digo en el sistema para este pa&iacute;s.',
                digits: 'El campo "C&oacute;digo" admite s&oacute;lo d&iacute;gitos.',
                minlength: 'El campo "C&oacute;digo" debe tener 4 d&iacute;gitos.',
                maxlength: 'El campo "C&oacute;digo" debe tener 4 d&iacute;gitos.'
            },
            txtID: {
                alphaNumeric: 'El campo "Id" admite s&oacute;lo n&uacute;meros y/o letras.',
                remote: 'El campo "Id" es incorrecto. Por favor ingrese un RUC v&aacute;lido.'
            },
            txtPhone1: {
                number: 'El campo "Tel&eacute;fono #1" admite s&oacute;lo n&uacute;meros.'
            },
            txtPhone2: {
                number: 'El campo "Tel&eacute;fono #2" admite s&oacute;lo n&uacute;meros.'
            },
            txtContractDate: {
                date: 'El campo "Fecha de contrato" debe tener formato fecha.',
                dateLessThan: 'La "Fecha del Contrato" no puede ser mayor a hoy.'
            },
            txtFax: {
                number: 'El campo "Fax" admite s&oacute;lo n&uacute;meros.'
            },
            txtMail1: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #1"',
                remote: 'E-mail #1: El correo ingresado ya consta en el sistema.'
            },
            txtMail2: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #2"',
                notEqualTo: 'E-mail #2: Ingrese un e-mail diferente que el anterior.',
                remote: 'E-mail #2: El correo ingresado ya consta en el sistema.'
            },
            txtContact: {
                required: true,
                textOnly: 'El campo "Nombre del Contacto" admite s&oacute;lo texto.'
            },
            txtContactPhone: {
                number: 'El campo "Tel&eacute;fono del Contacto" admite s&oacute;lo n&uacute;meros.'
            },
            txtContactMail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Contacto"'
            },
            txtVisitAmount: {
                number: 'El campo "N&uacute;mero de visitas al mes" admite s&oacute;lo n&uacute;meros.',
                max: 'El campo "N&uacute;mero de visitas al mes" tomar solo hasta 20 d&iacute;as.'
            },
            txtContactPay: {
                textOnly: 'El campo "Contacto Pago" admite s&oacute;lo texto.'
            },
            txtPercentage: {
                percentage: 'El campo "% de Descuento" admite s&oacute;lo n&uacute;meros con dos decimales.',
                max_value: 'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
            },
            txtAssistName: {
                textOnly: 'El campo "Nombre (Asistencias)" admite s&oacute;lo texto.'
            },
            txtAssistEmail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail (Asistencias)"'
            },
            txtNameManager: {
                required: 'El campo de "Nombre del Gerente" es obligatorio.',
                textOnly: 'Ingrese solamente texto en el campo "Nombre del Gerente".'
            },
            txtPhoneManager: {
                required: 'El campo "Tel&eacute;fono del Gerente" es obligatorio.',
                number: 'Ingrese solamente n&uacute;meros en el campo "Tel&eacute;fono del Gerente".'
            },
            txtMailManager: {
                required: 'El campo de "E-mail del Gerente" es obligatorio.',
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Gerente".'
            },
            txtBirthdayManager: {
                date: 'El campo "Fecha de Nacimiento" debe tener formato fecha.'
                        //dateLessThan:'La Fecha de Nacimiento no puede ser mayor a la fecha actual.'
            },
            txtAccountNumber: {
                digits: 'El campo "N&uacute;mero de cuenta" acepta solo d&iacute;gitos.'
            },
            txtMinimumKits: {
                max_value: 'El campo "M&iacute;nimo de Stock" admite valores hasta 50%'
            }
        },
        submitHandler: function(form) {
            if ($("#frmUpdateDealer").valid()) {
                input_box = confirm("Los cambios realizados en incentivos y responsables se efectuar\u00E1n en todas las sucursales de este comercializador.");
                if (input_box == true) {
                    $("#btnRegistrar").attr('disabled', 'true');
                    $("#btnRegistrar").val('Espere por favor...');
                    form.submit();
                }
            }
        }
    });
    /*END VALIDATION SECTION*/

    /*CALENDAR*/
    setDefaultCalendar($("#txtBirthdayManager"), '-100y', '+0d', '-18y');
    /*END CALENDAR*/

    /*CHANGE OPTIONS*/
    $("#selCountry").bind("change", function(e) {
        loadCities(this.value);
        loadCreditDays(this.value);
        loadManagersByCountry(this.value);
    });
    $('#selManager').bind('change', function(e) {
        loadUsersByManager(this.value);
        $("#hdnSerialMbc").val($("#selManager").val());
    })
    $('#rdoSelComissionistMa').bind('click', function(e) {
        $('#selComissionistPa').attr('disabled', true);
        $('#selComissionistPa').val('');
        $('#selComissionistMa').attr('disabled', false);
        $('#rdoSelComissionistPa').attr('checked', false);
    });
    $('#rdoSelComissionistPa').bind('click', function(e) {
        $('#selComissionistMa').attr('disabled', true);
        $('#selComissionistMa').val('');
        $('#selComissionistPa').attr('disabled', false);
        $('#rdoSelComissionistMa').attr('checked', false);
    });
    $("#selComissionistMa").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionistMa").val());
    });
    $("#selComissionistPa").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionistPa").val());
    });
    $("#selComissionist").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionist").val());
    });
    /*END CHANGE*/
});

function loadCities(countryID) {
    if (countryID == "") {
        $("#selCity option[value='']").attr("selected", true);
        $("#selSector option[value='']").attr("selected", true);
    }

    $('#cityContainer').load(document_root + "rpc/loadCities.rpc", {
        serial_cou: countryID
    }, function() {
        $("#selCity").bind("change", function(e) {
            loadSector(this.value);
        });
    });
}

function loadSector(cityID) {
    if (cityID == "") {
        $("#selSector option[value='']").attr("selected", true);
    }

    $('#sectorContainer').load(document_root + "rpc/loadSectors.rpc", {
        serial_cit: cityID
    });
}

function loadCreditDays(countryID) {
    $('#creditDContainer').load(document_root + "rpc/loadCreditDays.rpc", {
        serial_cou: countryID
    });
}

function loadManagersByCountry(countryID) {
    $('#managerContainer').load(document_root + "rpc/loadManagersByCountry.rpc", {
        serial_cou: countryID
    },
    function() {
        $('#selManager').bind('change', function() {
            loadUsersByManager(this.value);
            $("#hdnSerialMbc").val($("#selManager").val());
        });
    });
}

function loadUsersByManager(mbcID) {
    $('#comissionistContainer').load(document_root + "rpc/loadUsersByManager.rpc", {
        serial_mbc: mbcID
    });
}
