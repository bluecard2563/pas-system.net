// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/	
	$("#frmSearchDealer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selManager: {
				required: true
			},
			selDealer: {
				required: true
			},
			selBranch: {
				required: true
			}
		},
		messages: {
			selManager: {
				required: 'El campo "Representante" es obligatorio.'
			},
			selDealer: {
				required: 'El campo "Comercializador" es obligatorio.'
			},
			selCity: {
				required: 'El campo "Ciudad" es obligatorio.'
			},
			selBranch: {
				required: 'El campo "Sucursal" es obligatorio.'
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	/*var ruta=document_root+"rpc/autocompleters/loadDealers.rpc";
	$("#txtDealerData").autocomplete(ruta,{
        extraParams: {
			dea_serial_dea: 'NULL'
        },
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnDealerID").val(row[1]);
	});*/

	$("#selZone").bind("change", function(e){
		loadCountriesByZone($(this).val());
     });


    $("#btnSearch").bind("click", function(e) {
        $("#alerts").html('');
        $("#alerts").css('display', 'none');

        evaluateFilters();

    });
});

function evaluateFilters() {
    var next = true;
    if ($("#selCountry").val() == '') {
        $("#alerts").append("<li><label class='error_validation'>Seleccione Almenos un Pa&iacute;s</label></li>");
        $("#alerts").css('display', 'block');
        next = false;
    }
    if ($("#selManager").val() == '') {
        $("#alerts").append("<li><label class='error_validation'>Seleccione Almenos un Representante</label></li>");
        $("#alerts").css('display', 'block');
        next = false;
    }
    if (next) {
          loadLogDealer($("#selDealer").val());

    }
}

function loadLogDealer(branchID) {
    //get sales status registered and serial_inv NULL

    $('#logDealerContainer').load(document_root + "rpc/dealerVisit/loadDealersLogs.rpc", {selDealer: branchID},
        function() {

        });

}




/*CALL TO THE RPC TO LOAD THE COUNTRIES
 *BASED ON THE ZONE SELECTED*/
function loadCountriesByZone(zoneId){
    if(zoneId){//IF A ZONE HAS BEEN SELECTED
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc", {serial_zon: zoneId}, function(){
            $('#selCountry').bind('change', function(){
               loadManagersByCountry($(this).val());
			   loadCitiesByCountry($(this).val());
            });

			if($('#selCountry option').size()<=2){
               loadManagersByCountry($('#selCountry').val());
			   loadCitiesByCountry($('#selCountry').val());
            }
        });
    }else{
        $('#countryContainer').html('<select name="selCountry" id="selCountry" title=El campo "Pa&iacute;s" es obligatorio.><option value="">- Seleccione -</option></select>');
       loadManagersByCountry('');
	   loadCitiesByCountry('');
    }
}

/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc", {serial_cou: countryId}, function(){
            $('#selManager').bind('change', function(){
               loadDealers($(this).val(),$("#selCity").val());
            });

			if($('#selManager option').size()<=2){
				loadDealers($('#selManager').val(),$("#selCity").val());
			}
        });
    }else{
        $('#managerContainer').html('<select name="selManager" id="selManager" title=El campo "Representante" es obligatorio.><option value="">- Seleccione -</option></select>');
       loadDealers('','');
    }
}


/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#cityContainer').load(document_root+"rpc/loadCities.rpc", {serial_cou: countryId}, function(){
			$("#selCity").bind('change', function(){
				loadDealers($("#selManager").val(),$(this).val());
			});

			if($('#selCity option').size()<=2){
				loadDealers($('#selManager').val(),$("#selCity").val());
			}
        });
    }else{
        $('#cityContainer').html('<select name="selCity" id="selCity" title=El campo "Ciudad" es obligatorio.><option value="">- Seleccione -</option></select>');
		loadDealers('','');
    }
}

/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE MANAGER
 */
function loadDealers(managerId, cityId){
    if(managerId && cityId){
        $('#dealerContainer').load(document_root+"rpc/loadDealer.rpc",{
        		serial_man: managerId, 
        		serial_cit: cityId,
        		dea_serial_dea: 'NULL',
        		phone_sales: 'ALL'},function(){
        });
    }else{
       $('#dealerContainer').html('<select name="selDealer" id="selDealer" title=El campo "Comercializador" es obligatorio.><option value="">- Seleccione -</option></select>');
    }
}