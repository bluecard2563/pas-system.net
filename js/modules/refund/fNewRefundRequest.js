//Java Script
$(document).ready(function(){
	$('#btnSearch').click(function(){
		var card_number=$('#txtCardNumber').val();
        if(card_number!=''){
			show_ajax_loader();
			
			$('#refundContainer').load(document_root+"rpc/loadRefunds/getRefundRequestForm.rpc", {card_number: card_number}, function(){
					$("#frmRefundRequest").validate({
						errorLabelContainer: "#alerts",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							selRefundType: {
								required: true
							},
							hdnDocument:{
								required: true
							},
							rdPenaltyType:{
								required: true
							},
							txtPenaltyFee:{
								required: true,
								number: true
							},
							observations:{
								required: true
							},
							selRefundReason: {
								required: true
							},
							total_to_pay: {
								required: true
							}
						},
						messages: {
							txtPenaltyFee: {
								txtPenaltyFee: "El campo 'Valor Retenido' admite s&oacute;lo n&uacute;meros."
							},
							total_to_pay: {
								required: "El valor a reembolsar no se ha calculado."
							}
						},
						submitHandler: function(form) {
							$('#btnSubmit').attr('disabled', 'true');
							$('#btnSubmit').val('Espere por favor...');
							form.submit();
					   }
					});

					$('[id^="document_"]').each(function(){
						$(this).bind('change', function(){
							evaluateDocuments();
						});
					});

					$('#selPenaltyType').bind('change', function(){
						changeFeeValidation();
						calculateTotalToRefund();
					});
					
					$('#txtPenaltyFee').bind('change',function(){
						evaluatePenalty();
					});

					calculateTotalToRefund();
					
					$('#selRefundReason').bind('change', function(){
						evaluateRefundReason($(this).val());
					});
					
					hide_ajax_loader();
			});
			
		}else{
			alert('Ingrese un n\u00FAmero de tarjeta por favor.');
		}
    });
});

/*
 * @Function: evaluateDocuments
 * @Description: Evaluates if one document is selected, so that
 *               they can be validated.
 **/
function evaluateDocuments(){
	var oneFilled=false;
	$('[id^="document_"]').each(function(){
		if($(this).attr('id')!='document_0'){
			if($(this).is(':checked')){
				oneFilled=true;
				return;
			}
		}else{
			if($(this).val()!=""){
				oneFilled=true;
				return;
			}
		}
	});

	if(oneFilled){
		$('#hdnDocument').val('1');
	}else{
		$('#hdnDocument').val('');
	}
}

/*
 * @Function: changeFeeValidation
 * @Description: Adds or Removes a validation to the 'txtPenaltyFee' textfield
 *               for validation the user's input.
 **/
function changeFeeValidation(){
	if($('#selPenaltyType').val()=='PERCENTAGE'){
		$('#txtPenaltyFee').rules("add", {
			 percentage: true,
			 max_value: 100,
			 messages: {
			   percentage: "Por favor ingrese un porcentaje entre 0 y 100",
			   max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			 }
		});
	}else if($('#selPenaltyType').val()=='AMOUNT'){
		$("#txtPenaltyFee").rules("remove", "percentage");
	}
}

/*
 * @Function: calculateTotalToRefund
 * @Description: Calculates a first value to de refunded. This is prior to the bonus
 *               and commisions discount.
 **/
function calculateTotalToRefund(){
	var retainValue=parseFloat($('#txtPenaltyFee').val());
	var refundType=$('#selPenaltyType').val();
	var refundReason = $('#selRefundReason').val();

	var saleValue=parseFloat($('#hdnTotalSale').val());
	var discount=parseFloat($('#hdnDiscount').val())/100;
	var otherDiscount=parseFloat($('#hdnOtherDiscount').val())/100;
	var aproxRefund=0;
	var taxes=0;
	if($('#hdnTaxes').val()!=''){
		taxes=parseFloat($('#hdnTaxes').val())/100;
	}
	var paidAmounts=0;
	$('[id^="discount_"]').each(function(i){
		paidAmounts+=parseFloat($(this).val());
	});
	
	if((refundReason == 'SPECIAL') || (retainValue && refundType)){
		//Get the retained value for the refund.
		if(refundType == 'PERCENTAGE'){
			retainValue=saleValue*(retainValue/100);
		}

		/******* The formula of a refund is: (Total_sale-retainValue-discount-othreDiscount-paidAmounts)+taxes *******/
		/******* The NEW formula of a refund is: (Total_sale-paidAmounts-discount-othreDiscount-retainValue)+taxes *******/
		//Minus paidAmounts
		aproxRefund=saleValue-paidAmounts;

		//Minus Discount and Other Discount
		var refundDiscount=saleValue*discount;
		var refundOtherDiscount=saleValue*otherDiscount;
		aproxRefund=aproxRefund-(refundOtherDiscount+refundDiscount);

		//Minus retainValue
		if(retainValue>aproxRefund){
			$('#lblTotalToPay').html('');
			$('#total_to_pay').val('');
			$('#txtPenaltyFee').val('');

			alert('No se puede retener un monto mayor al remanente de la venta.');
		}else{
			aproxRefund=aproxRefund-retainValue;

			//Plus Taxes
			aproxRefund=aproxRefund+(aproxRefund*taxes)
			aproxRefund=aproxRefund.toFixed(2);

			$('#lblTotalToPay').html(aproxRefund);
			$('#total_to_pay').val(aproxRefund);
		}
	}else{
		$('#lblTotalToPay').html('');
		$('#total_to_pay').val('');
	}
}

function evaluatePenalty(){
	var remainingPercentage=parseFloat(100-parseFloat($('#hdnDiscount').val())-parseFloat($('#hdnOtherDiscount').val()));
	var retainValue=parseFloat($('#txtPenaltyFee').val());
	var refundType=$('#selPenaltyType').val();
	var beginCalculation='true';


	if(refundType=='PERCENTAGE'){
		if(retainValue>remainingPercentage){
			beginCalculation='false';
		}
	}

	if(beginCalculation=='true'){
		calculateTotalToRefund();
	}else{
		$('#txtPenaltyFee').val('');
		alert('No es posible realizar un a descuento mayor al 100%');
	}
}

function evaluateRefundReason(type){
	switch(type){
		case 'SPECIAL':
			$('#txtPenaltyFee').val('0');
			$('#txtPenaltyFee').attr('readonly', 'true');
			
			break;
		default:
			$('#lblTotalToPay').html('');
			$('#total_to_pay').val('');
			
			$('#txtPenaltyFee').removeAttr('readonly');
			break;
	}
	
	calculateTotalToRefund();
}