//Java Script
var pager;
$(document).ready(function(){
	if($('#tblRefundApplications').length>0){
		pager = new Pager('tblRefundApplications', 10 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}
});

/*
 * @Name: openReport
 * @Description: Displays the pdf file.
 **/
function openReport(creditNoteNumber){
    window.open(document_root+'modules/refund/pdfReports/credit_note_'+creditNoteNumber+'.pdf', 'reportCreditNote');
    $.ajax({
       type: "POST",
       url: document_root+"rpc/creditNote/deleteCreditNoteReport.rpc",
       data: "url=modules/refund/pdfReports/credit_note_"+creditNoteNumber+".pdf",
       success: function(msg){
         if(msg=='true'){
            window.location = document_root+"modules/refund/fSearchPendingAplications";
         }
       }
     });
}
