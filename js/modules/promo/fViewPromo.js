//Java Script

$(document).ready(function(){       

        /*Binding and Style Functions*/
        var i=0;
        for(i=1;i<=$('#hdnNumberPrize').val();i++){
            $("#selPrize_"+i).change(function(){validateProducts($(this),$("#selProduct_"+$(this).attr('id').charAt($(this).attr('id').length-1)),0)});
            $("#selProduct_"+i).change(function(){validateProducts($("#selPrize_"+$(this).attr('id').charAt($(this).attr('id').length-1)),$(this),1)});//both used to check if there isn't the same combination: prize-products among all the conditions
        }       
        var auxRepeat=-1;
        var weekends=-1;
        $("input[name='rdRepeat']").each(function(i){
            if($(this).is(':checked')){
                auxRepeat=this.value;
                return;
            }
        });
        $("input[name='rdWeekends']").each(function(i){
            if($(this).is(':checked')){
                weekends=this.value;
                return;
            }
        });
        if(auxRepeat==0){
            $('#repeatCriteria').css('display','none');
        }
        if(weekends=='NO'){
            $('#weekendsInfo').css('display','none');
        }
        //$('#weekDayContainer').css('display','none');
        if($('#hdnNumberPrize').val()==1){
            $('#btnRemove_1').css('display','none');
        }   
        $("input[name='rdRepeat']").each(function(i){
           $(this).bind('click',function(){
               $('#selRepeatType').val('');
               repeatCriteria(this.value);
               evaluateOneDayPromo();
               evaluateRepeatPromo();
               if($('#selRepeatType').val()!=''){
                   weekDayInfo($('#selRepeatType').val());
               }
               resetRandom();
           });
        });
        $("input[name='rdWeekends']").each(function(i){
           $(this).bind('click',function(){
               checkWeekends(this.value);
               resetRandom();
           });
        });
        $('#randomDay').bind('click',function(){
            generateRandom();
            var auxDay=$('#hdnRandom').val();
            if($('#selRepeatType').val()=='WEEKLY'){
                switch(parseInt($('#hdnRandom').val())){
                    case 1:auxDay='Lunes';
                            break;

                    case 2 :auxDay='Martes';
                            break;

                    case 3:auxDay='Miercoles';
                            break;

                    case 4:auxDay='Jueves';
                            break;

                    case 5:auxDay='Viernes';
                            break;

                    case 6:auxDay='Sabado';
                            break;

                    case 7:auxDay='Domingo';
                            break;

                }
            }
            
            $('#randomDay').html(auxDay+'&nbsp;&nbsp;<a href="#" name="generateRand" id="generateRand">Generar Otro</a>');
        });        
        $('#selRepeatType').bind('change',function(){
            resetRandom();
            $("#hddRepeatType").val($("#selRepeatType").val());
            weekDayInfo(this.value);
            $("input[name='rdWeekends']").each(function(i){
                if($(this).is(':checked')){
                    checkWeekends(this.value);
                    return;
                }
            });
        });
        $('#selPeriod').bind('change',function(){
            resetRandom();
            evaluateOneDayPromo();
            $("#hddPeriod").val($("#selPeriod").val());
            evaluateRepeatPromo();
            weekDayInfo($("#hddRepeatType").val());
            $("input[name='rdWeekends']").each(function(i){
                if($(this).is(':checked')){
                    checkWeekends(this.value);
                    return;
                }
            });
        });
        $('#txtBeginDate').change(function(){
            evaluateOneDayPromo();
            resetRandom();
        });
        $('#txtEndDate').change(function(){
            resetRandom();
        });
        $('#txtBeginDate').blur(function(){
            checkDate($("#txtBeginDate").val());
        });
        $('#selApliedTo').bind('change',function(){
            showApliedInfo(this.value);
        });

        $('#selType').bind('change',function(){
            loadPrizeInfo(this.value);
        });

        $('#btnAdd_1').bind('click',function(){
            addNew();
        });

        $('#btnRemove_1').bind('click',function(){
            removeLast();
        });
        $('#selProducts').bind('change',function(){
            loadAppliedProducts();
            $('#selApliedTo').val('');
            showApliedInfo(null);
        });
        $("#moveAll").click( function () {
                $("#dealersFrom option").each(function (i) {
                        $("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
                        $(this).remove();
                });
        });

        $("#removeAll").click( function () {
                $("#dealersTo option").each(function (i) {
                    if($(this).attr('city')==$("#dealersFrom").attr('city')){
                        $("#dealersFrom").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
                        $(this).remove();
                    }
                });
        });

        $("#moveSelected").click( function () {
                $("#dealersFrom option:selected").each(function (i) {
                        $("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
                        $(this).remove();
                });
        });

        $("#removeSelected").click( function () {
                $("#dealersTo option:selected").each(function (i) {
                    if($(this).attr('city')==$("#dealersFrom").attr('city')){
                        $("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
                        $(this).remove();
                    }
                });
        });
        /*END LIST SELECTOR*/

        $('#selZone').bind('change',function(){
           loadCountry(this.value);
        });
        /*End Bind and Style Function*/

        /*Calendars*/
        setDefaultCalendar($("#txtBeginDate"),'+0d','+10y','+0d');
        setDefaultCalendar($("#txtEndDate"),'+0d','+10y','+1d');
        /*End Calendars*/

        $("#frmViewPromo").wizard({
            show: function(element) {
				if($(element).is("#assignPrizes")){
					$('[type="submit"]').each(function(){
						$(this).hide();
					});
				}
			},
			next:"Siguiente >",
			prev:"< Anterior",
			extra:"ui-state-default ui-corner-all promoButton"
        });
});


/*
 * @Name: loadCountry
 * @Params: zoneID
 * @Description: Load all the countries for a specific zone.
 */
function loadCountry(zoneID){
    $('#countryContainer').load(document_root+"rpc/loadsPromo/loadPromoCountries.rpc",{
        serial_zon: zoneID,
        serial_pro: function (){
            var serial_products=new Array();
            $('#selProducts').find('option').each(function(){
                if($(this).attr('selected')==true){
                    array_push(serial_products,$(this).val());
                }
            });

            serial_products=implode(',', serial_products);
            return serial_products;
        }
    },function(){
        if($('#selCountry').val()){
            loadCity($('#selCountry').val());
        }
        $('#selCountry').change(function(){loadCity($('#selCountry').val())});
        $('#selCountry').bind('change',function(){
           loadManagersByCountry(this.value);
        });

        if($('#selCountry').find('option').size()==2){
            loadManagersByCountry($('#selCountry').val());
        }
    });
}

/*
 * @Name: loadCity
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCity(countryID){
    var appliedTo=$('#selApliedTo').val();
    if(appliedTo=="CITY" || appliedTo=="DEALER"){

	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},function(){
            if($('#selCity').val()){
                loadDealers($('#selCity').val());
            }
            $('#selCity').change(function(){loadDealers($('#selCity').val())});
        });
    }
}

/*
 * @Name: loadDealers
 * @Params: countryID
 * @Description: Load all the dealers for a specific country.
 */
function loadDealers(cityID){
     var countryID=$('#selCountry').val();
     var dealerList=new Array();
     var serial_products=new Array();
     if(cityID==""){
         cityID="stop";
     }


     //Retrieve the list of dealers that are shown already.
     $("#dealersTo option").each(function (i) {
        if($(this).attr('city')==cityID){
            dealerList.push($(this).attr('value'));
        }
    });
    //Retrieve the list of selected products
    $('#selProducts').find('option').each(function(){
        if($(this).attr('selected')==true){
            array_push(serial_products,$(this).val());
        }
    });

    serial_products=implode(',', serial_products);
    dealerList=implode(',', dealerList);

    if(dealerList!=''){
        $('#dealersFromContainer').load(document_root+"rpc/loadsPromo/loadPromoDealers.rpc",{serial_cit: cityID, serial_cou: countryID,serial_dealers: dealerList, products: serial_products});
    }else{
        $('#dealersFromContainer').load(document_root+"rpc/loadsPromo/loadPromoDealers.rpc",{serial_cit: cityID, serial_cou: countryID, products: serial_products});
    }
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
    var appliedTo=$('#selApliedTo').val();

    if(appliedTo=='MANAGER'){
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID});
    }
}

/*
 * @Name: testArray
 * @Params: Array tArray, element tElement
 * @Description: verifies if an element is in an array
 */
function testArray(tArray, tElement){
    for(var i = 0; i<tArray.length; i++){
        if(tElement==tArray[i]){
            return false
        }
    }
    return true;
}

/*
 * @Name: checkWeekends
 * @Params: radio value
 * @Description: verifies if the user has selected YES or NO in weekends option
 */
function checkWeekends(val){
    if(val=='YES'){
        $('#weekendsInfo').show();
    }else{
        $('#weekendsInfo').hide();
        $("#weekDayInfo_6").attr('checked', false);
        $("#weekDayInfo_7").attr('checked', false);
    }
}

/*
 * @Name: resetRandom
 * @Description: sets the hidden hdnRandom with ''
 */
function resetRandom(){
    $('#hdnRandom').val('');
    if($('#selPeriod').val()=='RANDOM'){
       $('#weekDayContainer').css('display','none');
    } 
}

/*
 * @Name: generateRandom
 * @Returns: String returnValue
 * @Description: generate a random day depending on user's selection
 */
function generateRandom(){
    var repeat=-1; //used to set the value of the selected radio in repeat option
    var weekends=''; //used to set the value of the selected radio in weekends option
    var returnValue=0; //value that is going to be returned (it is displayed in a message)
    var numRand; //var that helps generating the random number
    var beginDate; // beginDate's value captured from txtBeginDate
    var beginDay; // beginDate's day value
    var beginMonth; // beginDate's month value
    var beginYear; // beginDate's year value
    var endDate; // endDate's value captured from txtEndDate
    var endDay; // endDate's day value
    var endMonth; // endDate's month value
    var endYear; // endDate's year value
    var resultingDay; // day generated randomly
    var resultingMonth; // month generated randomly
    var resultingYear; // year generated randomly
    var resultingDate; // date generated with the combination off resultingDay, resultingMonth and resultingYear
    var monthDays; //number of days each month has

    $("input[name='rdRepeat']").each(function(i){
           if($(this).is(':checked')){
               repeat=$(this).val();
           }
    });

    $("input[name='rdWeekends']").each(function(i){
           if($(this).is(':checked')){
               weekends=$(this).val();
           }
    });

    //if the promo repeats
    if(repeat==1){
        //if the promo repeats weekly
        if($('#selRepeatType').val()=='WEEKLY'){
            //if the promo repeats weekly without weekends
            if(weekends=='NO'){
                numRand = Math.floor(Math.random()*5) + 1;
            }
            //if the promo repeats weekly with weekends
            else{
                numRand = Math.floor(Math.random()*7) + 1;
            }
            //sets the resulting day depending on which random day has been generated
            switch(numRand){
                case 1:resultingDay='Lunes';
                        break;

                case 2:resultingDay='Martes';
                        break;

                case 3:resultingDay='Miercoles';
                        break;

                case 4:resultingDay='Jueves';
                        break;

                case 5:resultingDay='Viernes';
                        break;

                case 6:resultingDay='Sabado';
                        break;

                case 7:resultingDay='Domingo';
                        break;

            }
            returnValue='El dia aleatorio generado es el "'+resultingDay+'" de cada semana';
            //if the promo repeats monthly
        }else{
            numRand = Math.floor(Math.random()*30) + 1;
            returnValue='El dia aleatorio generado es el "'+numRand+'" de cada mes';
        }
        $('#hdnRandom').val(numRand);
    }
    //if the promo doesn't repeat
    else{
        beginDate = new Date(Date.parse(dateFormat($('#txtBeginDate').val())));//to transform this txtBeginDate's value into a date.
        beginDay=beginDate.getDate();//getDate returns the day of the month from a date.
        beginMonth=beginDate.getMonth()+1;//getMonth returns the month from a date but with values from 0 to 11.
        beginYear=beginDate.getFullYear();//getYear returns the year from a date.

        endDate = new Date(Date.parse(dateFormat($('#txtEndDate').val())));//to transform this txtEndDate's value into a date.
        endDay=endDate.getDate();
        endMonth=endDate.getMonth()+1;
        endYear=endDate.getFullYear();

        resultingYear = Math.floor(Math.random()*(endYear-beginYear+1)) + beginYear;//first we generate the random year using beginYear and endYear

        if(resultingYear==beginYear&&resultingYear!=endYear){//if the resulting year is the same than beginYear but different than endYear
            resultingMonth = Math.floor(Math.random()*(12-beginMonth+1)) + beginMonth;//we generate the random month depending on if clauses
        }else if(resultingYear==endYear&&resultingYear!=beginYear){//if the resulting year is the same than endYear but different than beginYear
            resultingMonth = Math.floor(Math.random()*(endMonth)) + 1;//we generate the random month depending on if clauses
        }
        else if(resultingYear==endYear&&resultingYear==beginYear){//if the resulting year is the same than beginYear and endYear
            resultingMonth = Math.floor(Math.random()*(endMonth-beginMonth+1)) + beginMonth;
        }else{
            resultingMonth = Math.floor(Math.random()*(12)) + 1;
        }

        if(resultingMonth == 1 || resultingMonth == 3 || resultingMonth == 5 || resultingMonth == 7 || resultingMonth == 8 || resultingMonth == 10 || resultingMonth == 12){
            monthDays=31; //sets the number of days for months that have 31 days
        }else if(resultingMonth == 2){
            if(resultingYear % 4 == 0){//
                monthDays=29; //sets the number of days for february in a leap year
            }else{
                monthDays=28; //sets the number of days for february in a normal year
            }
        }else{
            monthDays=30; //sets the number of days for months that have 30 days
        }

        if(resultingYear==beginYear&&resultingYear!=endYear){//almost the same than months but with days
            if(resultingMonth==beginMonth){
                resultingDay = Math.floor(Math.random()*(monthDays-beginDay+1)) + beginDay;
            }else{
                resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
            }
        }else if(resultingYear==endYear&&resultingYear!=beginYear){
            if(resultingMonth==endMonth){
                resultingDay = Math.floor(Math.random()*(endDay)) + 1;
            }else{
                resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
            }
        }
        else if(resultingYear==endYear&&resultingYear==beginYear){
            if(resultingMonth==beginMonth&&resultingMonth==endMonth){
                resultingDay = Math.floor(Math.random()*(endDay-beginDay+1)) + beginDay;
            }else if(resultingMonth!=beginMonth && resultingMonth==endMonth){
                resultingDay = Math.floor(Math.random()*(endDay)) + 1;
            }else if(resultingMonth==beginMonth && resultingMonth!=endMonth){
                resultingDay = Math.floor(Math.random()*(monthDays-beginDay+1)) + beginDay;
            }else{
                resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
            }
        }else{
            resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
        }

        if(resultingDay<10){
            resultingDay="0"+resultingDay;//we add a "0" in days smaller than 10 in order to conserve the date format
        }
        if(resultingMonth<10){
            resultingMonth="0"+resultingMonth;//the same with months
        }
        resultingDate=resultingDay+"/"+resultingMonth+"/"+resultingYear;//here we combine the random values to set the resultingDate
        if(weekends=='NO'){
            resultingDate=evaluateWeekends(resultingDate);//if it is set to not acept any weekend, this function evaluates if the resulting date is a weekend, if it isn't it returns the same date, otherwise it generates another date until it isn't a weekend
        }
        $('#hdnRandom').val(resultingDate);//hidden use to save the value of the generated value.
        returnValue='El dia aleatorio generado entre el intervalo de fechas es el "'+resultingDate+'"';


    }
    return returnValue;
}


/*
 * @Name: repeatCriteria
 * @Params: value
 * @Description: Hides or show the Repeat Data according to
 *              user's choice.
 */
function repeatCriteria(value){
    if(value==0){
        $('#repeatCriteria').css('display','none');
    }else{
        $('#repeatCriteria').css('display','inline');
    }
}

/*
 * @Name: evaluateWeekends
 * @Params: date
 * @Description: this function evaluates if the resulting date is a weekend,
 * if it isn't it returns the same date, otherwise it generates another date until it isn't a weekend
 */
function evaluateWeekends(date){
    var evaDate= new Date(Date.parse(dateFormat(date)));
    if(evaDate.getDay()==5||evaDate.getDay()==6){
        generateRandom();
    }
    return date;
}

/*
 * @Name: showApliedInfo
 * @Params: N/A
 * @Description: Displays the section where we select
 *               the country/city etc. to apply the promo.
 *               If no products are selecte, an alert is
 *               displayed and the selection is reseted.
 */
function showApliedInfo(value){
    if($('#selProducts').val()!=null){
        $('#appliedToContainer').load(document_root+"rpc/loadsPromo/loadPromoInfo.rpc",{appliedTo: value});
    }else{
        $('#selApliedTo').find('option').each(function(){
            if($(this).val()==""){
                $(this).attr('selected', true);
                return;
            }
        });
        alert('Debe escoger al menos un producto de la lista anterior!');
    }
}

/*
 * @Name: evaluateOneDayPromo
 * @Params: N/A
 * @Description: Evaluates if the user chose 'ONE DAY' in the
 *               selPeriod item and 'NO' in the rdRepeat item.
 *               If that was the selection, the End Date Textfield,
 *               will be disabled.
 */
function evaluateOneDayPromo(){
    var period=$('#selPeriod').val();
    var repeat;
    $("input[name='rdRepeat']").each(function(i){
        if($(this).is(':checked')){
            repeat=this.value;
            return false;
        }
    });

    if(period=="ONE DAY" && repeat=="0"){
        setDefaultCalendar($("#txtEndDate"),'+0d','-1d','+1d');
        $('#txtEndDate').val($('#txtBeginDate').val());
        $('#txtEndDate').attr('readonly',true);
        $('#txtBeginDate').bind('keyup',function(){
           $('#txtEndDate').val($('#txtBeginDate').val());
        });
    }else{
        $('#txtEndDate').removeAttr('readonly');
        $('#txtBeginDate').unbind('keyup');
        setDefaultCalendar($("#txtEndDate"),'+0d','+10y','+1d');
    }
}

/*
 * @Name: evaluateRepeatPromo
 * @Params: N/A
 * @Description: Evaluates if the user chose 'RANDOM' in the
 *               selPeriod so the weekDayContainer won't be
 *               displayed.
 */
function evaluateRepeatPromo(){
    var period=$('#selPeriod').val();
    var repeat=-1;    
    $("input[name='rdRepeat']").each(function(i){
           if($(this).is(':checked')){
               repeat=$(this).val();
           }
    });
    if(repeat==0){
        $('#selPeriod').find('[value="SEVERAL DAYS"]').hide();
        if($('#selPeriod').val()=="SEVERAL DAYS"){
           $('#selPeriod').val('');
        }
    }else{
        $('#selPeriod option').each(function (i) {
                        $('#selPeriod').find('option').show();
        });
    }
    
    if(period=='RANDOM' || repeat==-1 || repeat==0 || !period || !$('#selRepeatType').val()){
        $('#weekDayContainer').css('display','none');
    }else{
        $('#weekDayContainer').css('display','inline');
    }
}

/*
 * @Name: weekDayInfo
 * @Params: value
 * @Description: Loads info according if the periodicity is
 *               weekly or monthly
 */
function weekDayInfo(value){

    if(value=="" || !$('#selPeriod').val()){
         $('#weekDayContainer').css('display','none');
    }else{
         if(value=="WEEKLY"){
             
             var weekDays='<input type="hidden" name="hdnWeekDayInfo" id="hdnWeekDayInfo" title="El campo \'D&iacute;a de la Promoci&oacute;n\' es obligatorio."/>';


             /*Loading Weekly Data*/
            if($('#hddPeriod').val()=="ONE DAY" && $('#hddRepeatType').val()=="WEEKLY"){
             weekDays+=" <input type='radio' name='weekDayInfo[]' id='weekDayInfo_1' value='1' class='weekDayInfo' /> Lunes<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_2' value='2' class='weekDayInfo' /> Martes<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_3' value='3' class='weekDayInfo' /> Mi&eacute;rcoles<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_4' value='4' class='weekDayInfo' /> Jueves<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_5' value='5' class='weekDayInfo'/> Viernes<br/>\n\
                        <div id=weekendsInfo>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_6' value='6' class='weekDayInfo'/> S&aacute;bado<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_7' value='7' class='weekDayInfo'/> Domingo<br/>\n\
                        </div>";
            }else{
                weekDays+=" <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_1' value='1' class='weekDayInfo' /> Lunes<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_2' value='2' class='weekDayInfo' /> Martes<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_3' value='3' class='weekDayInfo' /> Mi&eacute;rcoles<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_4' value='4' class='weekDayInfo' /> Jueves<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_5' value='5' class='weekDayInfo' /> Viernes<br/>\n\
                        <div id=weekendsInfo>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_6' value='6' class='weekDayInfo' /> S&aacute;bado<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_7' value='7' class='weekDayInfo' /> Domingo<br/>\n\
                        </div>";
	    }
            $('#weekDay').html(weekDays);            
            /*END Loading*/

            $(".weekDayInfo").each(function(i){
                $(this).bind('click',function(){
                   validateSelection();
                });
            });
            if($('#selPeriod').val()!='RANDOM'){
               $("#hdnWeekDayInfo").rules("add", {
                     required: true
                });
            }else{
                $("#hdnWeekDayInfo").rules("remove");
            }
         }else if(value=="MONTHLY"){
             /*Loading Monthly Data*/
            $('#weekDay').html('<input type="text" name="weekDayInfo" id="weekDayInfo_Calendar" title="El campo \'D&iacute;a de la Promoci&oacute;n\' es obligatorio."/>');

            $('#weekDayInfo_Calendar').bind('change',function(){
               weekDayWarning(this.value);
            });
            /*End Loading*/
             /*Adding Validation Rules*/
            if($('#selPeriod').val()!='RANDOM'){
                if($('#selPeriod').val()=='ONE DAY'){
                    $("#weekDayInfo_Calendar").rules("remove");
                    $("#weekDayInfo_Calendar").rules("add", {
                         required: true,
                         digits: true,
                         messages: {
                           digits: "El campo 'D&iacute;a de la Promoci&oacute;n' acepta solo d&iacute;gitos (1 d&iacute;a ya que esa es la opci&oacute;n que est&aacute; seleccionada)."
                         }
                    });
                }else{
                    $("#weekDayInfo_Calendar").rules("remove");
                    $("#weekDayInfo_Calendar").rules("add", {
                         required: true,
                         rangeType: true,
                         messages: {
                           rangeType: "El campo 'D&iacute;a de la Promoci&oacute;n' acepta d&iacute;gitos o intervalos separados por comas y sin espacios en blanco como por ejemplo: '1,3,5,8-12,14,15-23'."
                         }
                    });
                }

            }else{
                $("#weekDayInfo_Calendar").rules("remove");
                $("#hdnWeekDayInfo").rules("remove");
            }
            /*END Validation*/
         }
        
         if($('#selPeriod').val()!='RANDOM' && $('#selRepeatType').val()){
             
            $('#weekDayContainer').css('display','inline');
         }
         else{
             $("input[name='rdWeekends']").each(function(i){
                 if($(this).attr('checked')==true){
                    checkWeekends(this.value);
                 }
            });
         }
         $("input[name='rdWeekends']").each(function(i){
           $(this).bind('click',function(){
               checkWeekends(this.value);
           });
        });
    }
}

/*
 * @Name: validateSelection
 * @Description: Changes the hidden value if one element
 *               is selected or not.
 */
function validateSelection(){
    var checked=0;

    $(".weekDayInfo").each(function(i){
        if($(this).is(':checked')){
            checked=1;
            return;
        }
    });

    if(checked==1){
        $('#hdnWeekDayInfo').val('1');
    }else{
        $('#hdnWeekDayInfo').val('');
    }
}

/*
 * @Name: weekDayWarning
 * @Params: value
 * @Description: Shows a warning if the day is bigger than 28, because no
 *               all of the months have 30 or 31 days
 */
function weekDayWarning(value){
    if(value>28){
        alert('Tenga en cuenta que aquellos meses que no tengan '+value+' d\u00EDas, se aplicar\u00E1 la promoci\u00F3n el \u00FAltimo d\u00EDa del mes.');
    }
}

/*
 * @Name: addNew
 * @Params: N/A
 * @Description: Adds the description of a new prize for the promotion.
 */
function addNew(){
    $('#btnRemove_1').css('display','inline');
    var newID = parseInt($('#hdnNumberPrize').val());
    newID++;
    $('#hdnNumberPrize').val(newID);

    var newPrize = "<div class='span-24 last line' id='prize_"+newID+"'>";

    newPrize +=    "<div class='span-24 last title line'><label>Condici&oacute;n No. "+newID+"</label></div>";
    newPrize +=    "<div class='prepend-4 span-4 label'>* Productos que Aplican:</div>";
    newPrize +=    "<div class='span-5'>";
    newPrize +=    "<select name='selProduct["+newID+"][]' id='selProduct_"+newID+"' case='"+newID+"' title=\"El campo 'Productos ("+newID+")' es obligatorio.\" multiple class='appliedProducts'>";
    $('#selProduct_1').find('option').each(function(i){
        newPrize +="<option value='"+$(this).val()+"'>"+$(this).text()+"</option>";
    });
    newPrize +=    "</select>";
    newPrize +=    "</div>";

    newPrize +=    "<div class='span-9 line'>";
    newPrize +=    "<div class='span-3 label'>* Monto Base:</div>";
    newPrize +=    "<div class='span-5'>";
    newPrize +=    "<input type='text' name='txtBeginP[]' id='txtBeginP_"+newID+"' title=\"El campo 'Monto Base ("+newID+")' es obligatorio.\" position='"+newID+"'/>";
    newPrize +=    "</div>";
    newPrize +=    "</div>";

    newPrize +=    "<div class='span-9 line'>";
    newPrize +=    "<div class='span-3 label'>Monto L&iacute;mite:</div>";
    newPrize +=    "<div class='span-5'>";
    newPrize +=    "<input type='text' name='txtEndP[]' id='txtEndP_"+newID+"' title=\"El campo 'Monto L&iacute;mite ("+newID+")' es obligatorio.\" position='"+newID+"'/>";
    newPrize +=    "</div>";
    newPrize +=    "</div>";


    newPrize +=    "<div class='span-9 line'>";
    newPrize +=    "<div class='span-3 label'>* Premio:</div>";
    newPrize +=    "<div class='span-5'>";
    newPrize +=    "<select name='selPrize[]' id='selPrize_"+newID+"' case='"+newID+"' title=\"El campo 'Premio ("+newID+")' es obligatorio.\">";
    $('#selPrize_1').find('option').each(function(i){
        newPrize +="<option value='"+$(this).val()+"'>"+$(this).text()+"</option>";
    });
    newPrize +=    "</select>";
    newPrize +=    "</div>";
    newPrize +=    "</div>";

    newPrize +=    "</div>";

    $('#morePrizes').append(newPrize);

    /*Adding Validation Rules*/
    $("#selProduct_"+newID).rules("add", {
         required: true
    });
    $("#txtBeginP_"+newID).rules("add", {
         required: true,
         number: true,
         message:{
             number: "El campo 'Monto Base ("+newID+")' admite s&oacute;lo n&uacute;meros."
         }
    });
    $("#selPrize_"+newID).rules("add", {
         required: true
    });
    /*END Validation*/

    $("#selPrize_"+newID).change(function(){validateProducts($("#selPrize_"+newID),$("#selProduct_"+newID),0)});
    $("#selProduct_"+newID).change(function(){validateProducts($("#selPrize_"+newID),$("#selProduct_"+newID),1)});

    $('#btnRemove_'+newID).bind('click',function(){
        $('#prize_'+newID).remove();
    });
}

/*
 * @Name: validateProducts
 * @Params: N/A
 * @Description: validate products and prices don't repeat.
 */
function validateProducts(prizeObject,productObject,type){
    var controlPrize;
    var controlProduct;
    controlPrize=0;
    controlProduct=0;
    $("select[id^='selPrize_']").each(function(i){
       if(prizeObject.val()==$(this).val() && prizeObject.attr('id')!=$(this).attr('id') && $(this).val()){
           controlPrize=$(this).attr('case');
       }
       if(productObject.val()!= null && productObject.attr('case')!=$(this).attr('case')){
           if(controlEndP(productObject,$("#selProduct_"+$(this).attr('case')))){
                controlProduct=$(this).attr('case');
                if(controlProduct==controlPrize){
                    return false;
                }
           }
       }
    });
    if(controlPrize==controlProduct && controlProduct*controlPrize!=0){
        if(type){
            alert("En la 'Condici\u00F3n No."+controlProduct+"' ya se asign\u00F3 uno o m\u00E1s productos iguales con el mismo premio.");
            productObject.val("");
        }else{
            alert("En la 'Condici\u00F3n No."+controlProduct+"' ya se asign\u00F3 el mismo premio con uno o m\u00E1s productos iguales.");
            prizeObject.val("");
        }
    }

}

function checkDate(txtDate){
    var emDays=dayDiff(txtDate,$('#hdnCurDate').val());
    if(emDays>0){ //Checks if the departure day is at least 1 day more than the emission date
        alert("La Fecha de Salida no puede ser anterior a la fecha actual");
        $("#txtBeginDate").val('');
    }
}

function controlEndP(object1,object2){
    var arr1 = new Array();
    arr1=explode(',',object1.val()+"");
    var arr2 = new Array
    arr2=explode(',',object2.val()+"");
    for(var i=0;i<arr1.length;i++){
        for(var j=0;j<arr2.length;j++){
            if(arr1[i]==arr2[j]){
                return true;
            }
        }
    }
    return false;
}


/*
 * @Name: removeLast
 * @Params: N/A
 * @Description: Removes the last prize for the promotion.
 */
function removeLast(){
    var newID = parseInt($('#hdnNumberPrize').val());
    $('#prize_'+newID).remove();

    if(newID>=2){
        newID--;
        $('#hdnNumberPrize').val(newID);

        if(newID==1){
            $('#btnRemove_1').css('display','none');
        }
    }
}

/*
 * @Name: removeLast
 * @Params: N/A
 * @Description: Removes the last prize for the promotion.
 */
function loadPrizeInfo(value){
    switch(value){
        case 'PERCENTAGE':
            $("input[id^='txtBeginP_']").each(function(i){
               $(this).rules("add", {
                     percentage: true,
					 max_value: 100,
                     messages:{
                         percentage: "El campo 'Monto Base ("+$(this).attr('position')+")' admite s&oacute;lo porcentajes.",
						 max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
                     }
                });
            });

            $("input[id^='txtEndP_']").each(function(i){
               $(this).rules("add", {
                     percentage: true,
					 max_value: 100,
                     messages:{
                         percentage: "El campo 'Monto L&iacute;mite ("+$(this).attr('position')+")' admite s&oacute;lo porcentajes.",
						 max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
                     }
                });
                $(this).attr('disabled',true);
                $(this).val('');
            });
            break;
        case 'AMOUNT':
			$("input[id^='txtBeginP_']").each(function(i){
				$(this).rules("remove", 'percentage');
				$(this).rules("remove", 'max_value');
            });

            $("input[id^='txtEndP_']").each(function(i){
               $(this).rules("remove", 'required');
               $(this).attr('disabled',true);
               $(this).val('');
            });

            break;
        case 'RANGE':
			$("input[id^='txtBeginP_']").each(function(i){
				$(this).rules("remove", 'percentage');
				$(this).rules("remove", 'max_value');
            });
			
            $("input[id^='txtEndP_']").each(function(i){
               $(this).rules("add", {
                     required: true
                });
                $(this).attr('disabled',false);
            });
            break;
    }
}

/*
 * @Name: removeLast
 * @Params: N/A
 * @Description: Removes the last prize for the promotion.
 */
function loadAppliedProducts(){
    $("select[id^='selProduct_']").each(function(i){
        $(this).find('option').remove();
    });

    $('#selProducts').find('option').each(function(i){
        var option = $(this);
        $("select[id^='selProduct_']").each(function(j){
           if(option.attr('selected')==true){
                $(this).append("<option value='"+option.val()+"'>"+option.text()+"</option>");
           }
        });
    });
}