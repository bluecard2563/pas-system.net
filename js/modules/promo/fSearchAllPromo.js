var serial;
var pager;
$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	$("#frmSearchAllPromo").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {
				required: true
			},
                        selApliedTo: {
                            required: true
                        },
                        selCountry: {
                            required: {
                                depends: function(element) {
                                        var appliedTo=$('#selApliedTo').val();

                                        if(appliedTo=='COUNTRY')
                                            return false;
                                        else
                                            return true;
                                }
                            }
                        },
                        selCity: {
                            required: {
                                depends: function(element) {
                                        var appliedTo=$('#selApliedTo').val();

                                        if(appliedTo=='CITY')
                                            return true;
                                        else
                                            return false;
                                }
                            }
                        },
                        selManager: {
                            required: {
                                depends: function(element) {
                                         var appliedTo=$('#selApliedTo').val();

                                        if(appliedTo=='MANAGER')
                                            return true;
                                        else
                                            return false;
                                }
                            }
                        },
                        selDealer: {
                            required: {
                                depends: function(element) {
                                        var appliedTo=$('#selApliedTo').val();

                                        if(appliedTo=='DEALER')
                                            return true;
                                        else
                                            return false;
                                }
                            }
                        }
		},
		messages: {
			
   		}
	});
	/*END VALIDATION*/

        /*Binding and Style Functions*/
        $('#selApliedTo').bind('change',function(){
           showApliedInfo(this.value);
        });
        /*End Bind and Style Function*/
});

/*
 * @Name: showApliedInfo
 * @Params: N/A
 * @Description: Displays the section where we select
 *               the country/city etc. to apply the promo.
 */
function showApliedInfo(value){
    $('#appliedToContainer').load(document_root+"rpc/loadsPromo/loadPromoInfoSearch.rpc",{appliedTo: value},function(){
            switch($('#selApliedTo').val()){
                case 'MANAGER':
                    serial='#selManager';
                   break;
                case 'COUNTRY':
                    serial='#selCountry';
                   break;
                case 'CITY':
                    serial='#selCity';
                    break;
                case 'DEALER':
                    serial='#selDealer';
                   break;
            }

            /*var ruta=document_root+"rpc/autocompleters/loadPromo.rpc";
            $("#txtName").autocomplete(ruta,{
                    extraParams: {
                        appliedTo: function(){ return $('#selApliedTo').val()},
                        serial_info: function(){ return $(serial).val()},
						all: function(){return 'YES'}
                    },
                    max: 10,
                    scroll: false,
                    matchContains:true,
                    minChars: 2,
                    formatItem: function(row) {
                             return row[0] ;
                    }
            }).result(function(event, row) {
                if(row[0]=="No existen promociones con ese nombre."){
                    $("#txtName").val('');
		}
                $("#hdnPromoID").val(row[1]);		
            });*/
     });
}

/*
 * @Name: loadCountry
 * @Params: zoneID
 * @Description: Load all the countries for a specific zone.
 */
function loadCountry(zoneID){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneID, charge_country:0},function(){
            $('#selCountry').change(loadCity);
            $('#selCountry').bind('change',function(){
               loadManagersByCountry(this.value);
			   if($('#selApliedTo').val()=='COUNTRY'){
				   loadPromo();
			   }
               cleanHistory();
            });
        });
}

/*
 * @Name: loadCity
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCity(){
    var appliedTo=$('#selApliedTo').val();
    if(appliedTo=="CITY" || appliedTo=="DEALER"){
        var countryID=this.value;

	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},function(){
            $('#selCity').bind('change',function(){
				if($('#selApliedTo').val()=='CITY'){
				   loadPromo();
			   }
			   if($('#selApliedTo').val()=='DEALER'){
				   loadDealers($('#selCity').val());
			   }
               cleanHistory();
            });
        });
    }
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
    var appliedTo=$('#selApliedTo').val();

    if(appliedTo=='MANAGER'){
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
			$('#selManager').bind('change',function(){
			   if($('#selApliedTo').val()=='MANAGER'){
				   loadPromo();
			   }
               cleanHistory();
            });
		});
    }
}

/*
 *@Name: loadDealer
 *@Description: Charges all dealers
 **/
function loadDealers(cityID){
	if(cityID==""){
		$("#selDealer option[value='']").attr("selected",true);
		cityID='stop';
	}

	$('#dealerContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{//serial_cou: countryID,
		serial_cit: cityID,
		dea_serial_dea: 'NULL'},
		function(){
			$("#selDealer").bind("change", function(e){
			  if($('#selApliedTo').val()=='DEALER'){
				loadPromo();
			  }
			});
		}
	);
}

function cleanHistory(){
    $('#txtName').val('');
    $('#txtName').flushCache();
    $('#hdnPromoID').val('');
}

/*
 * @Name: loadPromo
 * @Params: countryID
 * @Description: Load all promos for the specified criteria.
 */
function loadPromo(){
	$('#promoContainer').load(document_root+"rpc/loadPromo.rpc",{appliedTo: $('#selApliedTo').val(), serial_info: $(serial).val(), all: 'YES' },function(){
		if($('#tblPromos').length>0){
			pager = new Pager('tblPromos', 10 , 'Siguiente', 'Anterior');
			pager.init($('#hdnPages').val()); //Max Pages
			pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
			pager.showPage(1); //Starting page
		}
	});
}