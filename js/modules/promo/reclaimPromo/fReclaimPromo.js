//Java Script
var pagerWinners;
var pagerNeededPrizes;
var pagerPrizes;

$(document).ready(function(){
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmReclaimPromo").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone:{
				required: true
			},
            selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selManager: {
				required: true
			},
			selDealer: {
				required: true
			},
			selApliedTo: {
				required: true
			}
		},
		message:{

		},
		submitHandler: function(form){
			if(confirm('Recuerde que los montos no reclamados no se tomar\u00E1n en cuenta para futuros canjes. Desea continuar?')){
				form.submit();
			}
		}
	});
	
	$('#selApliedTo').bind('change',function(){
		loadAvailablePromo();
		cleanContainers();
	});	
});

/*
 * @Name: loadPromosAvailable
 * @Description: Load all the promos available for be reclaimed according to the type.n
 */
function loadAvailablePromo(){
    var appliedTo=$('#selApliedTo').val();
	show_ajax_loader();
	
	$('#promoContainer').load(document_root+"rpc/loadsPromo/loadAvailablePromos.rpc",{applied_to_prm: appliedTo},function (){
		hide_ajax_loader();
		$('#selPromo').bind('change',function(){
			displayPromoWinners();
		});

		if($('#selPromo').find('option').size()==2){
			displayPromoWinners();
		}
	});
}

/*
 * @Name: displayPromoAwardsInfo
 * @Description: loads the amount available for trading and the availble prizes in the selected promo.
 */
function displayPromoWinners(){
	var serial_dea=$('#selDealer').val();
	var serial_prm=$('#selPromo').val();

	if(serial_prm!=''){		
		$('#prizeInfoContainer').load(document_root+"rpc/loadsPromo/getAvailablePrizesByPromo.rpc",
		{
			serial_prm: serial_prm,
			serial_dea: serial_dea
		},function (){			
			if($('#winnerTable').length>0){
				pagerWinners = new Pager('winnerTable', 15 , 'Siguiente', 'Anterior');
				pagerWinners.init(50); //Max Pages
				pagerWinners.showPageNav('pagerWinners', 'navWinner'); //Div where the navigation buttons will be placed.
				pagerWinners.showPage(1); //Starting page

				pagerNeededPrizes = new Pager('neededPrizes', 3 , 'Siguiente', 'Anterior');
				pagerNeededPrizes.init(7); //Max Pages
				pagerNeededPrizes.showPageNav('pagerNeededPrizes', 'navNeededPrizes'); //Div where the navigation buttons will be placed.
				pagerNeededPrizes.showPage(1); //Starting page
			}

			pagerPrizes = new Pager('prizeTable', 3 , 'Siguiente', 'Anterior');
			pagerPrizes.init(7); //Max Pages
			pagerPrizes.showPageNav('pagerPrizes', 'navPrizes'); //Div where the navigation buttons will be placed.
			pagerPrizes.showPage(1); //Starting page
			
			if($("#hdnStock").length>0){
				$("#hdnStock").rules("add", {
					 required: true
				});
			}

			if($('#btnIncreaseStock').length>0){
				$('#btnIncreaseStock').bind('click',function(){
					reasignStock();
				});
			}
		});
	}else{		
		$('#prizeInfoContainer').html('');
	}
}


/*
 * @Name: cleanContainers
 * @Description: Cleans both of the containers that display the promo info.
 */
function cleanContainers(){
	$('#promoContainer').html('');
	$('#prizeInfoContainer').html('');
}


/*
 * @Name: reasignStock
 * @Description: Creates a new amount of stock for a specific set of prizes.
 */
function reasignStock(){
	var serials=new Array();
	var quantity=new Array();
	var aux= new Array();
	show_ajax_loader();
	

	$('[id^="hdnPrizeNeeded_"]').each(function(){
		array_push(serials, $(this).val());
		array_push(quantity, $(this).attr('quantity'));
	});

	$.ajax({
		url: document_root+"rpc/loadsPromo/reasignPrizeStock.rpc",
		type: "post",
		data: ({serials : implode(',',serials), quantity: implode(',',quantity)}),
		success: function(data) {
			if(data=='true'){
				$('#hdnStock').val('1');
				var q=0;

				$('[id^="hdnPrizeNeeded_"]').each(function(){
					q=parseFloat($(this).attr('quantity'))+1;
					$('#text_'+$(this).attr('serial_pbp')).html(q);
				});

				$('#btnIncreaseStock').css('display', 'none');
				hide_ajax_loader();
				alert('Se ha actualizado exitosamente el stock!');
			}else{
				hide_ajax_loader();
				alert('No se ha podido actualizar los registros de stock. Lo sentimos');
			}
		}
	});
}