//Java Script
$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	var good=0;
	$("#frmNewPromo").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {                                
				required: true,
				alphaNumeric: true,
				remote: {
					url: document_root+"rpc/remoteValidation/promo/checkName.rpc",
					type: "post",
					data: {
						beginDate: function() {
							return $("#txtBeginDate").val();
						},
						endDate: function() {
							return $("#txtEndDate").val();
						}
					}
				}
			},
			txtDescription: {
				required: true
			},
			txtBeginDate:{
				required: true,
				date: true,
				dateLessOrEqualToBySelector: '#txtEndDate'
			},
			txtEndDate:{
				required: true,
				date: true,
				dateGraterOrEqualToBySelector: '#txtBeginDate'
			},
			selType: {
				required: true
			},
			selPeriod: {
				required: true
			},
			txtNDays: {
				required: true
			},
			selSaleType: {
				required: true
			},
			rdNotification: {
				required: true
			},
			rdRepeat:{
				required: true
			},
			selPromoFor: {
				required: true
			},
			selRepeatType: {
				required: {
					depends: function(element) {
						var display = $("#repeatCriteria").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			selApliedTo: {
				required: true
			},
			selCountry: {
				required: {
					depends: function(element) {
						var display = $("#appliedToContainer").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			selCity: {
				required: {
					depends: function(element) {
						var appliedTo=$('#selApliedTo').val();
                                        
						if(appliedTo=='CITY')
							return true;
						else
							return false;
					}
				}
			},
			selManager: {
				required: {
					depends: function(element) {
						var appliedTo=$('#selApliedTo').val();

						if(appliedTo=='MANAGER')
							return true;
						else
							return false;
					}
				}
			},
			selDealer: {
				required: {
					depends: function(element) {
						var appliedTo=$('#selApliedTo').val();

						if(appliedTo=='DEALER')
							return true;
						else
							return false;
					}
				}
			},
			selProduct_1: {
				required: true
			},
			txtBeginP_1: {
				required: true,
				number: true
			},
			txtEndP_1: {
				number: true,
				required:true
			},
			selPrize_1: {
				required: true
			},
			selProducts: {
				allselected: function() {
					var arr=new Array();
					var aux;
					$("select[id^='selProduct_']").each(function(i){
						aux=new Array();
						aux=explode(',',$(this).val()+"");
						for(var i=0;i< aux.length;i++){
							if(testArray(arr,aux[i])){
								array_push(arr,aux[i]);
							}
						}
					});
					return arr;
				}
                            
			}
		},
		messages: {
			txtName: {
				alphaNumeric: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
				remote: "Ya existe otra promoci&oacute;n con el mismo nombre para el per&iacute;odo elegido."
			},
			hdnNamePrize:{
				remote: "El nombre ingresado ya existe en el sistema para el tipo seleccionado."
			},
			txtValue: {
				number: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Valor'."
			},
			txtStock: {
				digits: "S&oacute;lo se aceptan d&iacute;gitos en el campo 'Stock'."
			},	
			selType: {
				remote: "Por favor seleccione un Tipo."
			},
			txtBeginDate:{
				date:"El campo 'Fecha Inicio' debe tener el formato mm/dd/YYYY",
				dateLessOrEqualToBySelector: 'La Fecha Inicio debe ser menor o igual que la Fecha Fin'
			},
			txtEndDate:{
				date:"El campo 'Fecha Fin' debe tener el formato mm/dd/YYYY",
				dateGraterOrEqualToBySelector: 'La Fecha Fin debe ser mayor o igual que la Fecha Inicio'
			},
			txtBeginP_1: {
				number: "El campo 'Monto Base (1)' admite s&oacute;lo n&uacute;meros"
			},
			txtEndP_1: {
				number: "El campo 'Monto L&iacute;mite (1)' admite s&oacute;lo n&uacute;meros"
			},
			selProducts: {
				allselected: "Todos los productos seleccionados deben constar en al menos una promoci&oacute;n"
			}
		}
		,
		submitHandler: function(form) {
			$("#dialog").dialog({
				bgiframe: true,
				autoOpen: false,
				height: 200,
				width: 410,
				modal: true,
				buttons: {
					'Aceptar': function() {

						good=1;
						form.submit();
						$(this).dialog('close');
					},
					'Generar Otro': function(){
						var day;
						day=generateRandom();
						$('#randomDialog').html("<b>"+day+" presione 'Aceptar' para continuar, 'Generar Otro' para generar otro dia aleatorio.</b>");

					},
					Cancelar: function() {
						$(this).dialog('close');
					}
				}
			});
			/*End Dialog*/

			good=1;

			if($('#selPeriod').val()=='RANDOM'&&$('#hdnRandom').val()==''){
				good=0;
				var day;
				day=generateRandom();
				$("#dialog").dialog('open');
				/*Dialog*/

				$('#randomDialog').html("<b>"+day+" presione 'Aceptar' para continuar, 'Generar Otro' para generar otro dia aleatorio.</b>");
			}else{
				good=1;
			}


			if($('#selType').val()=='RANGE'){
				var item='-1';
				var max_base_value=0;
				var noAnotherLimit=true;
				var anotherLimit;
				var coherentValues=true;

				$('[id^="txtEndP_"]').each(function(i){
					if(parseFloat($(this).val())=='0'){
						if(item=='-1'){
							item=i+1;
						}else{
							noAnotherLimit=false;
							return;
						}
					}
				});

				if(noAnotherLimit){
					var iteration=0;
					$('[id^="txtBeginP_"]').each(function(i){
						iteration=i+1;
						if(parseFloat($('#txtEndP_'+iteration).val())>0 && (parseFloat($(this).val())>=parseFloat($('#txtEndP_'+iteration).val()))){
							coherentValues=false;
							return;
						}
					});

					if(coherentValues){
						max_base_value=parseFloat($('#txtBeginP_'+item).val());
						$('[id^="txtEndP_"]').each(function(i){
							if(i!=(parseFloat(item)-1)){
								if(parseFloat($(this).val())>max_base_value){
									anotherLimit=i+1;
									good=2;
									return;
								}
							}
						});
					}else{
						good=4;
					}
				}else{
					good=3;
				}
			}

			switch(good){
				case 1:
					form.submit();
					break;
				case 2:
					alert('El monto base de la condici\u00F3n final, es menor al monto l\u00EDmite de la contici\u00F3n ('+anotherLimit+')');
					break;
				case 3:
					alert('S\u00F3lo puede existir una condici\u00F3n con "Monto L\u00EDmite" igual a "0"');
					break;
				case 4:
					alert('Existen rangos ingresados incorrectamente. EL "Monto L\u00EDmite" debe ser mayor al "Monto Base".');
					break;
			}
		}
	});
	/*END VALIDATION*/

	/*Binding and Style Functions*/
        
	$("#selPrize_1").change(function(){
		validateProducts($("#selPrize_1"),$("#selProduct_1"),0)
		});
	$("#selProduct_1").change(function(){
		validateProducts($("#selPrize_1"),$("#selProduct_1"),1)
		});//both used to check if there isn't the same combination: prize-products among all the conditions
	$('#repeatCriteria').css('display','none');
	$('#weekDayContainer').css('display','none');
	$('#btnRemove_1').css('display','none');

	$("input[name='rdRepeat']").each(function(i){
		$(this).bind('click',function(){
			$('#selRepeatType').val('');
			repeatCriteria(this.value);
			evaluateOneDayPromo();
			evaluateRepeatPromo();               
		});
	});
	$("input[name='rdWeekends']").each(function(i){
		$(this).bind('click',function(){
			checkWeekends(this.value);               
		});
	});
	$('#selRepeatType').bind('change',function(){
		$("#hddRepeatType").val($("#selRepeatType").val());
		weekDayInfo(this.value);
		$("input[name='rdWeekends']").each(function(i){
			if($(this).is(':checked')){
				checkWeekends(this.value);
				return;
			}
		});            
	});
	$('#selPeriod').bind('change',function(){
		evaluateOneDayPromo();
		$("#hddPeriod").val($("#selPeriod").val());
		evaluateRepeatPromo();
		weekDayInfo($("#hddRepeatType").val());
		$("input[name='rdWeekends']").each(function(i){
			if($(this).is(':checked')){
				checkWeekends(this.value);
				return;
			}
		});
	});
	$('#txtBeginDate').change(function(){
		evaluateOneDayPromo();            
	});
	$('#txtBeginDate').bind('change',function(){
		checkDate($("#txtBeginDate").val());
	});
	$('#selApliedTo').bind('change',function(){
		showApliedInfo(this.value);
	});

	$('#selType').bind('change',function(){
		loadPrizeInfo(this.value);
		if(this.value == "RANGE") {
			$('#endPriceContainer').css("display", "block");
		} else {
			$('#endPriceContainer').css("display", "none");
		}
	});

	$('#btnAdd_1').bind('click',function(){
		addNew();
	});

	$('#btnRemove_1').bind('click',function(){
		removeLast();
	});
	$('#selProducts').bind('change',function(){
		loadAppliedProducts();
		$('#selApliedTo').val('');
		showApliedInfo(null);
	});
	/*End Bind and Style Function*/

	/*Calendars*/
	setDefaultCalendar($("#txtBeginDate"),'+0d','+10y','+0d');
	setDefaultCalendar($("#txtEndDate"),'+0d','+10y','+1d');
	/*End Calendars*/

	$("#frmNewPromo").wizard({
		show: function(element) {
		},
		next:"Siguiente >",
		prev:"< Anterior",
		action:"Insertar",
		extra:"ui-state-default ui-corner-all promoButton"/*,
                    extraFunction: "eraseAlerts"*/
	});
});


/*
 * @Name: loadCountry
 * @Params: zoneID
 * @Description: Load all the countries for a specific zone.
 */
function loadCountry(zoneID){
	$('#countryContainer').load(document_root+"rpc/loadsPromo/loadPromoCountries.rpc",{
		serial_zon: zoneID,
		serial_pro: function (){
			var serial_products=new Array();
			$('#selProducts').find('option').each(function(){
				if($(this).attr('selected')==true){
					array_push(serial_products,$(this).val());
				}
			});

			serial_products=implode(',', serial_products);
			return serial_products;
		}
	},function(){
		if($('#selCountry').val()){
			loadCity($('#selCountry').val());
		}
		$('#selCountry').change(function(){
			loadCity($('#selCountry').val())
			});
		$('#selCountry').bind('change',function(){
			loadManagersByCountry(this.value);
		});
        
		if($('#selCountry').find('option').size()==2){
			loadManagersByCountry($('#selCountry').val());
		}
	});
}

/*
 * @Name: loadCity
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCity(countryID){
	var appliedTo=$('#selApliedTo').val();
	if(appliedTo=="CITY" || appliedTo=="DEALER"){
              
		$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
			serial_cou: countryID
		},function(){
			if($('#selCity').val()){
				loadDealers($('#selCity').val());
			}  
			$('#selCity').change(function(){
				loadDealers($('#selCity').val())
				});
		});
	}
}

function checkDate(txtDate){
	var emDays=dayDiff(txtDate,$('#hdnCurDate').val());
	if(emDays>0){ //Checks if the departure day is at least 1 day more than the emission date
		alert("La Fecha de Inicio debe ser mayor a la fecha actual");
		$("#txtBeginDate").val('');
	}
}
/*
 * @Name: loadDealers
 * @Params: countryID
 * @Description: Load all the dealers for a specific country.
 */
function loadDealers(cityID){
	var countryID=$('#selCountry').val();
	var dealerList=new Array();
	var serial_products=new Array();
	if(cityID==""){
		cityID="stop";
	}


	//Retrieve the list of dealers that are shown already.
	$("#dealersTo option").each(function (i) {
		if($(this).attr('city')==cityID){
			dealerList.push($(this).attr('value'));
		}
	});
	//Retrieve the list of selected products
	$('#selProducts').find('option').each(function(){
		if($(this).attr('selected')==true){
			array_push(serial_products,$(this).val());
		}
	});

	serial_products=implode(',', serial_products);
	dealerList=implode(',', dealerList);

	if(dealerList!=''){
		$('#dealersFromContainer').load(document_root+"rpc/loadsPromo/loadPromoDealers.rpc",{
			serial_cit: cityID, 
			serial_cou: countryID,
			serial_dealers: dealerList, 
			products: serial_products
		});
	}else{
		$('#dealersFromContainer').load(document_root+"rpc/loadsPromo/loadPromoDealers.rpc",{
			serial_cit: cityID, 
			serial_cou: countryID, 
			products: serial_products
		});
	}
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
	var appliedTo=$('#selApliedTo').val();

	if(appliedTo=='MANAGER'){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{
			serial_cou: countryID
		});
	}
}

/*
 * @Name: testArray
 * @Params: Array tArray, element tElement
 * @Description: verifies if an element is in an array
 */
function testArray(tArray, tElement){
	for(var i = 0; i<tArray.length; i++){
		if(tElement==tArray[i]){
			return false
		}
	}
	return true;
}

/*
 * @Name: checkWeekends
 * @Params: radio value
 * @Description: verifies if the user has selected YES or NO in weekends option
 */
function checkWeekends(val){
	if(val=='YES'){
		$('#weekendsInfo').show();
	}else{
		$('#weekendsInfo').hide();
		$("#weekDayInfo_6").attr('checked', false);
		$("#weekDayInfo_7").attr('checked', false);
	}
}

/*
 * @Name: generateRandom
 * @Returns: String returnValue
 * @Description: generate a random day depending on user's selection
 */
function generateRandom(){
	var repeat=-1; //used to set the value of the selected radio in repeat option
	var weekends=''; //used to set the value of the selected radio in weekends option
	var returnValue=0; //value that is going to be returned (it is displayed in a message)
	var numRand; //var that helps generating the random number
	var beginDate; // beginDate's value captured from txtBeginDate
	var beginDay; // beginDate's day value
	var beginMonth; // beginDate's month value
	var beginYear; // beginDate's year value
	var endDate; // endDate's value captured from txtEndDate
	var endDay; // endDate's day value
	var endMonth; // endDate's month value
	var endYear; // endDate's year value
	var resultingDay; // day generated randomly
	var resultingMonth; // month generated randomly
	var resultingYear; // year generated randomly
	var resultingDate; // date generated with the combination off resultingDay, resultingMonth and resultingYear
	var monthDays; //number of days each month has

	$("input[name='rdRepeat']").each(function(i){
		if($(this).is(':checked')){
			repeat=$(this).val();
		}
	});

	$("input[name='rdWeekends']").each(function(i){
		if($(this).is(':checked')){
			weekends=$(this).val();
		}
	});

	//if the promo repeats
	if(repeat==1){
		//if the promo repeats weekly
		if($('#selRepeatType').val()=='WEEKLY'){
			//if the promo repeats weekly without weekends
			if(weekends=='NO'){
				numRand = Math.floor(Math.random()*5) + 1;
			}
			//if the promo repeats weekly with weekends
			else{
				numRand = Math.floor(Math.random()*7) + 1;
			}
			//sets the resulting day depending on which random day has been generated
			switch(numRand){
				case 2:
					resultingDay='Lunes';
					break;

				case 3:
					resultingDay='Martes';
					break;

				case 4:
					resultingDay='Miercoles';
					break;

				case 5:
					resultingDay='Jueves';
					break;

				case 6:
					resultingDay='Viernes';
					break;

				case 7:
					resultingDay='Sabado';
					break;

				case 1:
					resultingDay='Domingo';
					break;
                        
			}            
			returnValue='El dia aleatorio generado es el "'+resultingDay+'" de cada semana';
		//if the promo repeats monthly
		}else{
			numRand = Math.floor(Math.random()*30) + 1;
			returnValue='El dia aleatorio generado es el "'+numRand+'" de cada mes';
		}
		$('#hdnRandom').val(numRand);
	}
	//if the promo doesn't repeat
	else{
		beginDate = new Date(Date.parse(dateFormat($('#txtBeginDate').val())));//to transform this txtBeginDate's value into a date.
		beginDay=beginDate.getDate();//getDate returns the day of the month from a date.
		beginMonth=beginDate.getMonth()+1;//getMonth returns the month from a date but with values from 0 to 11.
		beginYear=beginDate.getFullYear();//getYear returns the year from a date.

		endDate = new Date(Date.parse(dateFormat($('#txtEndDate').val())));//to transform this txtEndDate's value into a date.
		endDay=endDate.getDate();
		endMonth=endDate.getMonth()+1;
		endYear=endDate.getFullYear();

		resultingYear = Math.floor(Math.random()*(endYear-beginYear+1)) + beginYear;//first we generate the random year using beginYear and endYear

		if(resultingYear==beginYear&&resultingYear!=endYear){//if the resulting year is the same than beginYear but different than endYear
			resultingMonth = Math.floor(Math.random()*(12-beginMonth+1)) + beginMonth;//we generate the random month depending on if clauses
		}else if(resultingYear==endYear&&resultingYear!=beginYear){//if the resulting year is the same than endYear but different than beginYear
			resultingMonth = Math.floor(Math.random()*(endMonth)) + 1;//we generate the random month depending on if clauses
		}
		else if(resultingYear==endYear&&resultingYear==beginYear){//if the resulting year is the same than beginYear and endYear
			resultingMonth = Math.floor(Math.random()*(endMonth-beginMonth+1)) + beginMonth;
		}else{
			resultingMonth = Math.floor(Math.random()*(12)) + 1;
		}

		if(resultingMonth == 1 || resultingMonth == 3 || resultingMonth == 5 || resultingMonth == 7 || resultingMonth == 8 || resultingMonth == 10 || resultingMonth == 12){
			monthDays=31; //sets the number of days for months that have 31 days
		}else if(resultingMonth == 2){
			if(resultingYear % 4 == 0){//
				monthDays=29; //sets the number of days for february in a leap year
			}else{
				monthDays=28; //sets the number of days for february in a normal year
			}
		}else{
			monthDays=30; //sets the number of days for months that have 30 days
		}

		if(resultingYear==beginYear&&resultingYear!=endYear){//almost the same than months but with days
			if(resultingMonth==beginMonth){
				resultingDay = Math.floor(Math.random()*(monthDays-beginDay+1)) + beginDay;
			}else{
				resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
			}
		}else if(resultingYear==endYear&&resultingYear!=beginYear){
			if(resultingMonth==endMonth){
				resultingDay = Math.floor(Math.random()*(endDay)) + 1;
			}else{
				resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
			}
		}
		else if(resultingYear==endYear&&resultingYear==beginYear){
			if(resultingMonth==beginMonth&&resultingMonth==endMonth){
				resultingDay = Math.floor(Math.random()*(endDay-beginDay+1)) + beginDay;
			}else if(resultingMonth!=beginMonth && resultingMonth==endMonth){
				resultingDay = Math.floor(Math.random()*(endDay)) + 1;
			}else if(resultingMonth==beginMonth && resultingMonth!=endMonth){
				resultingDay = Math.floor(Math.random()*(monthDays-beginDay+1)) + beginDay;
			}else{
				resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
			}
		}else{
			resultingDay = Math.floor(Math.random()*(monthDays)) + 1;
		}

		if(resultingDay<10){
			resultingDay="0"+resultingDay;//we add a "0" in days smaller than 10 in order to conserve the date format
		}
		if(resultingMonth<10){
			resultingMonth="0"+resultingMonth;//the same with months
		}
		resultingDate=resultingDay+"/"+resultingMonth+"/"+resultingYear;//here we combine the random values to set the resultingDate
		if(weekends=='NO'){
			resultingDate=evaluateWeekends(resultingDate);//if it is set to not acept any weekend, this function evaluates if the resulting date is a weekend, if it isn't it returns the same date, otherwise it generates another date until it isn't a weekend
		}
		$('#hdnRandom').val(resultingDate);//hidden use to save the value of the generated value.
		returnValue='El dia aleatorio generado entre el intervalo de fechas es el "'+resultingDate+'"';
        

	}
	return returnValue;
}


/*
 * @Name: repeatCriteria
 * @Params: value
 * @Description: Hides or show the Repeat Data according to
 *              user's choice.
 */
function repeatCriteria(value){
	if(value==0){
		$('#repeatCriteria').css('display','none');
	}else{
		$('#repeatCriteria').css('display','inline');
	}
}

/*
 * @Name: evaluateWeekends
 * @Params: date
 * @Description: this function evaluates if the resulting date is a weekend,
 * if it isn't it returns the same date, otherwise it generates another date until it isn't a weekend
 */
function evaluateWeekends(date){    
	var evaDate= new Date(Date.parse(dateFormat(date)));
	if(evaDate.getDay()==5||evaDate.getDay()==6){
		generateRandom();
	}
	return date;
}

/*
 * @Name: showApliedInfo
 * @Params: N/A
 * @Description: Displays the section where we select
 *               the country/city etc. to apply the promo.
 *               If no products are selecte, an alert is
 *               displayed and the selection is reseted.
 */
function showApliedInfo(value){
	if($('#selProducts').val()!=null){
		$('#appliedToContainer').load(document_root+"rpc/loadsPromo/loadPromoInfo.rpc",{
			appliedTo: value
		});
	}else{
		$('#selApliedTo').find('option').each(function(){
			if($(this).val()==""){
				$(this).attr('selected', true);
				return;
			}
		});
		alert('Debe escoger al menos un producto de la lista anterior!');
	} 
}

/*
 * @Name: evaluateOneDayPromo
 * @Params: N/A
 * @Description: Evaluates if the user chose 'ONE DAY' in the
 *               selPeriod item and 'NO' in the rdRepeat item.
 *               If that was the selection, the End Date Textfield,
 *               will be disabled.
 */
function evaluateOneDayPromo(){
	var period=$('#selPeriod').val();
	var repeat;
	$("input[name='rdRepeat']").each(function(i){
		if($(this).is(':checked')){
			repeat=this.value;
			return false;
		}
	});

	if(period=="ONE DAY" && repeat=="0"){
		setDefaultCalendar($("#txtEndDate"),'+0d','-1d','+1d');
		$('#txtEndDate').val($('#txtBeginDate').val());
		$('#txtEndDate').attr('readonly',true);
		$('#txtBeginDate').bind('keyup',function(){
			$('#txtEndDate').val($('#txtBeginDate').val());
		});
	}else{
		$('#txtEndDate').removeAttr('readonly');
		$('#txtBeginDate').unbind('keyup');
		setDefaultCalendar($("#txtEndDate"),'+0d','+10y','+1d');
	}
}

/*
 * @Name: evaluateRepeatPromo
 * @Params: N/A
 * @Description: Evaluates if the user chose 'RANDOM' in the
 *               selPeriod so the weekDayContainer won't be
 *               displayed.
 */
function evaluateRepeatPromo(){
	var period=$('#selPeriod').val();
	var repeat=-1;
	$("input[name='rdRepeat']").each(function(i){
		if($(this).is(':checked')){
			repeat=$(this).val();
		}
	});
	if(repeat==0){
		$('#selPeriod').find('[value="SEVERAL DAYS"]').hide();
		if($('#selPeriod').val()=="SEVERAL DAYS"){
			$('#selPeriod').val('');
		}
	}else{
		$('#selPeriod option').each(function (i) {
			$('#selPeriod').find('option').show();
		});
	}
	if(period=='RANDOM' || repeat==-1 || repeat==0 || !period || !$('#selRepeatType').val()){
		$('#weekDayContainer').css('display','none');
	}else{
		$('#weekDayContainer').css('display','inline');
	}    
}

/*
 * @Name: weekDayInfo
 * @Params: value
 * @Description: Loads info according if the periodicity is
 *               weekly or monthly
 */
function weekDayInfo(value){
    
	if(value=="" || !$('#selPeriod').val()){
		$('#weekDayContainer').css('display','none');
	}else{
		if(value=="WEEKLY"){
			var weekDays='<input type="hidden" name="hdnWeekDayInfo" id="hdnWeekDayInfo" title="El campo \'D&iacute;a de la Promoci&oacute;n\' es obligatorio."/>';
             

			/*Loading Weekly Data*/
			if($('#hddPeriod').val()=="ONE DAY" && $('#hddRepeatType').val()=="WEEKLY"){
				weekDays+=" <input type='radio' name='weekDayInfo[]' id='weekDayInfo_2' value='2' class='weekDayInfo' /> Lunes<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_3' value='3' class='weekDayInfo' /> Martes<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_4' value='4' class='weekDayInfo' /> Mi&eacute;rcoles<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_5' value='5' class='weekDayInfo' /> Jueves<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_6' value='6' class='weekDayInfo'/> Viernes<br/>\n\
                        <div id=weekendsInfo>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_7' value='7' class='weekDayInfo'/> S&aacute;bado<br/>\n\
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_1' value='1' class='weekDayInfo'/> Domingo<br/>\n\
                        </div>";
			}else{
				weekDays+=" <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_2' value='2' class='weekDayInfo' /> Lunes<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_3' value='3' class='weekDayInfo' /> Martes<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_4' value='4' class='weekDayInfo' /> Mi&eacute;rcoles<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_5' value='5' class='weekDayInfo' /> Jueves<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_6' value='6' class='weekDayInfo' /> Viernes<br/>\n\
                        <div id=weekendsInfo>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_7' value='7' class='weekDayInfo' /> S&aacute;bado<br/>\n\
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_1' value='1' class='weekDayInfo' /> Domingo<br/>\n\
                        </div>";
			}
			$('#weekDay').html(weekDays);
			/*END Loading*/

			$(".weekDayInfo").each(function(i){
				$(this).bind('click',function(){
					validateSelection();
				});
			});
			if($('#selPeriod').val()!='RANDOM'){
				$("#hdnWeekDayInfo").rules("add", {
					required: true
				});
			}else{
				$("#hdnWeekDayInfo").rules("remove");
			}
            
            
		}else if(value=="MONTHLY"){
			/*Loading Monthly Data*/
			$('#weekDay').html('<input type="text" name="weekDayInfo" id="weekDayInfo_Calendar" title="El campo \'D&iacute;a de la Promoci&oacute;n\' es obligatorio."/>');

			$('#weekDayInfo_Calendar').bind('change',function(){
				weekDayWarning(this.value);
			});
			/*End Loading*/
			/*Adding Validation Rules*/
			if($('#selPeriod').val()!='RANDOM'){
				if($('#selPeriod').val()=='ONE DAY'){
					$("#weekDayInfo_Calendar").rules("remove");
					$("#weekDayInfo_Calendar").rules("add", {
						required: true,
						digits: true,
						messages: {
							digits: "El campo 'D&iacute;a de la Promoci&oacute;n' acepta solo d&iacute;gitos (1 d&iacute;a ya que esa es la opci&oacute;n que est&aacute; seleccionada)."
						}
					});
				}else{
					$("#weekDayInfo_Calendar").rules("remove");
					$("#weekDayInfo_Calendar").rules("add", {
						required: true,
						rangeType: true,
						messages: {
							rangeType: "El campo 'D&iacute;a de la Promoci&oacute;n' acepta d&iacute;gitos o intervalos separados por comas y sin espacios en blanco como por ejemplo: '1,3,5,8-12,14,15-23'."
						}
					}); 
				}
                
			}else{				
				$("#weekDayInfo_Calendar").rules("remove");
				
				if($("#hdnWeekDayInfo").length>0){
					$("#hdnWeekDayInfo").rules("remove");
				}
			}
		/*END Validation*/
		}         

		if($('#selPeriod').val()!='RANDOM' && $('#selRepeatType').val()){
			$('#weekDayContainer').css('display','inline');
		}
		else{
			$("input[name='rdWeekends']").each(function(i){
				if($(this).attr('checked')==true){
					checkWeekends(this.value);
				}
			});
		}
		$("input[name='rdWeekends']").each(function(i){
			$(this).bind('click',function(){
				checkWeekends(this.value);
			});
		});
	}
}

/*
 * @Name: validateSelection
 * @Description: Changes the hidden value if one element
 *               is selected or not.
 */
function validateSelection(){
	var checked=0;

	$(".weekDayInfo").each(function(i){
		if($(this).is(':checked')){
			checked=1;
			return;
		}
	});
    
	if(checked==1){
		$('#hdnWeekDayInfo').val('1');
	}else{
		$('#hdnWeekDayInfo').val('');
	}
}

/*
 * @Name: weekDayWarning
 * @Params: value
 * @Description: Shows a warning if the day is bigger than 28, because no
 *               all of the months have 30 or 31 days
 */
function weekDayWarning(value){
	if(value>28){
		alert('Tenga en cuenta que aquellos meses que no tengan '+value+' d\u00EDas, se aplicar\u00E1 la promoci\u00F3n el \u00FAltimo d\u00EDa del mes.');
	}
}

/*
 * @Name: addNew
 * @Params: N/A
 * @Description: Adds the description of a new prize for the promotion.
 */
function addNew(){
	$('#btnRemove_1').css('display','inline');
	var newID = parseInt($('#hdnNumberPrize').val());
	newID++;
	$('#hdnNumberPrize').val(newID);

	var newPrize = "<div class='span-24 last line' id='prize_"+newID+"'>";

	newPrize +=    "<div class='span-24 last title line'><label>Condici&oacute;n No. "+newID+"</label></div>";
	newPrize +=    "<div class='prepend-4 span-4 label'>* Productos que Aplican:</div>";
	newPrize +=    "<div class='span-5'>";
	newPrize +=    "<select name='selProduct["+newID+"][]' id='selProduct_"+newID+"' case='"+newID+"' title=\"El campo 'Productos ("+newID+")' es obligatorio.\" multiple class='appliedProducts'>";
	$('#selProduct_1').find('option').each(function(i){
		newPrize +="<option value='"+$(this).val()+"'>"+$(this).text()+"</option>";
	});
	newPrize +=    "</select>";
	newPrize +=    "</div>";

	newPrize +=    "<div class='span-9 line'>";
	newPrize +=    "<div class='span-3 label'>* Monto Base:</div>";
	newPrize +=    "<div class='span-5'>";
	newPrize +=    "<input type='text' name='txtBeginP[]' id='txtBeginP_"+newID+"' title=\"El campo 'Monto Base ("+newID+")' es obligatorio.\" position='"+newID+"'/>";
	newPrize +=    "</div>";
	newPrize +=    "</div>";

	newPrize +=    "<div class='span-9 line'>";
	newPrize +=    "<div class='span-3 label'>Monto L&iacute;mite:</div>";
	newPrize +=    "<div class='span-5'>";
	newPrize +=    "<input type='text' name='txtEndP[]' id='txtEndP_"+newID+"' title=\"El campo 'Monto L&iacute;mite ("+newID+")' es obligatorio.\" position='"+newID+"'/>";
	newPrize +=    "</div>";
	newPrize +=    "</div>";


	newPrize +=    "<div class='span-9 line'>";
	newPrize +=    "<div class='span-3 label'>* Premio:</div>";
	newPrize +=    "<div class='span-5'>";
	newPrize +=    "<select name='selPrize[]' id='selPrize_"+newID+"' case='"+newID+"' title=\"El campo 'Premio ("+newID+")' es obligatorio.\">";
	$('#selPrize_1').find('option').each(function(i){
		newPrize +="<option value='"+$(this).val()+"'>"+$(this).text()+"</option>";
	});
	newPrize +=    "</select>";
	newPrize +=    "</div>";
	newPrize +=    "</div>";
   
	newPrize +=    "</div>";

	$('#morePrizes').append(newPrize);
    
	/*Adding Validation Rules*/
	$("#selProduct_"+newID).rules("add", {
		required: true
	});
	$("#txtBeginP_"+newID).rules("add", {
		required: true,
		number: true,
		messages:{
			number: "El campo 'Monto Base ("+newID+")' admite s&oacute;lo n&uacute;meros."
		}
	});
	$("#selPrize_"+newID).rules("add", {
		required: true
	});
	/*END Validation*/

	$("#selPrize_"+newID).change(function(){
		validateProducts($("#selPrize_"+newID),$("#selProduct_"+newID),0)
		});
	$("#selProduct_"+newID).change(function(){
		validateProducts($("#selPrize_"+newID),$("#selProduct_"+newID),1)
		});

	$('#btnRemove_'+newID).bind('click',function(){
		$('#prize_'+newID).remove();
	});
	loadPrizeInfo($('#selType').val());
}

/*
 * @Name: validateProducts
 * @Params: N/A
 * @Description: validate products and prices don't repeat.
 */
function validateProducts(prizeObject,productObject,type){
	var controlPrize;
	var controlProduct;
	controlPrize=0;
	controlProduct=0;
	$("select[id^='selPrize_']").each(function(i){
		if(prizeObject.val()==$(this).val() && prizeObject.attr('id')!=$(this).attr('id') && $(this).val()){
			controlPrize=$(this).attr('case');
		}
		if(productObject.val()!= null && productObject.attr('case')!=$(this).attr('case')){
			if(controlEndP(productObject,$("#selProduct_"+$(this).attr('case')))){
				controlProduct=$(this).attr('case');
				if(controlProduct==controlPrize){
					return false;
				}
			}
		}
	});
	if(controlPrize==controlProduct && controlProduct*controlPrize!=0){
		if(type){
			alert("En la 'Condici\u00F3n No."+controlProduct+"' ya se asign\u00F3 uno o m\u00E1s productos iguales con el mismo premio.");
			productObject.val("");
		}else{
			alert("En la 'Condici\u00F3n No."+controlProduct+"' ya se asign\u00F3 el mismo premio con uno o m\u00E1s productos iguales.");
			prizeObject.val("");
		}
	}
}

function controlEndP(object1,object2){
	var arr1 = new Array();
	arr1=explode(',',object1.val()+"");
	var arr2 = new Array
	arr2=explode(',',object2.val()+"");
	for(var i=0;i<arr1.length;i++){
		for(var j=0;j<arr2.length;j++){
			if(arr1[i]==arr2[j]){
				return true;
			}
		}
	}
	return false;
}


/*
 * @Name: removeLast
 * @Params: N/A
 * @Description: Removes the last prize for the promotion.
 */
function removeLast(){
	var newID = parseInt($('#hdnNumberPrize').val());
	$('#prize_'+newID).remove();

	if(newID>=2){
		newID--;
		$('#hdnNumberPrize').val(newID);

		if(newID==1){
			$('#btnRemove_1').css('display','none');
		}
	}
}

/*
 * @Name: removeLast
 * @Params: N/A
 * @Description: Removes the last prize for the promotion.
 */
function loadPrizeInfo(value){
	switch(value){
		case 'PERCENTAGE':
			$('#rangeWarning').css('display', 'none');
			$('#typeWarning').css('display', 'inline');
			
			$("input[id^='txtBeginP_']").each(function(i){
				$(this).rules("add", {
					percentage: true,
					max_value: 100,
					messages:{
						percentage: "El campo 'Monto Base ("+$(this).attr('position')+")' admite s&oacute;lo porcentajes.",
						max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
					}
				});                
			});

			$("input[id^='txtEndP_']").each(function(i){
				$(this).rules("add", {
					percentage: true,
					max_value: 100,
					messages:{
						percentage: "El campo 'Monto L&iacute;mite ("+$(this).attr('position')+")' admite s&oacute;lo porcentajes.",
						max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
					}
				});
				$(this).attr('disabled',true);
				$(this).val('');
			});
			break;
		case 'AMOUNT':
			$('#rangeWarning').css('display', 'none');
			$('#typeWarning').css('display', 'inline');

			$("input[id^='txtBeginP_']").each(function(i){
				$(this).rules("remove", 'percentage');
				$(this).rules("remove", 'max_value');
			});

			$("input[id^='txtEndP_']").each(function(i){
				$(this).rules("remove", 'required');
				$(this).attr('disabled',true);
				$(this).val('');
			});        
			break;
		case 'RANGE':
			$('#rangeWarning').css('display', 'inline');
			$('#typeWarning').css('display', 'none');

			$("input[id^='txtBeginP_']").each(function(i){
				$(this).rules("remove", 'percentage');
				$(this).rules("remove", 'max_value');
			});
			$("input[id^='txtEndP_']").each(function(i){
				$(this).rules("remove", 'percentage');
				$(this).rules("remove", 'max_value');
			});

			$("input[id^='txtEndP_']").each(function(i){
				$(this).rules("add", {
					required: true
				});
				$(this).attr('disabled',false);
			});            
			break;
	}
}

/*
 * @Name: removeLast
 * @Params: N/A
 * @Description: Removes the last prize for the promotion.
 */
function loadAppliedProducts(){
	$("select[id^='selProduct_']").each(function(i){
		$(this).find('option').remove();
	});

	$('#selProducts').find('option').each(function(i){
		var option = $(this);
		$("select[id^='selProduct_']").each(function(j){
			if(option.attr('selected')==true){
				$(this).append("<option value='"+option.val()+"'>"+option.text()+"</option>");
			}
		});
	});
}

