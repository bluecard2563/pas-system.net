var pager;

$(document).ready(function(){
	if($("#invoicesTable").length>0) {
		
		var pages_count = (parseInt($('#hdnCount').val()) / 10) + 1;
		
        pager = new Pager('invoicesTable', 10 , 'Siguiente', 'Anterior');
        pager.init(pages_count); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	$('#btnGeneratePDF').bind('click',function(){
		$('#frmPendingPaymentsReport').submit();
	});
});