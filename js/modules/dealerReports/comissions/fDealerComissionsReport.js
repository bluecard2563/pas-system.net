// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmDealerComissionsReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
				 selStatus: {
                    required: {
								depends: function(element) {
											var display = $("#statusContainer").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
                txtCardNumber: {
                    required: {
								depends: function(element) {
											var display = $("#cardContainer").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  },
					digits: true
                },
				txtBeginDate: {
					required: {
								depends: function(element) {
											var display = $("#statusContainer").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  },
					date: true
				},
				txtEndDate: {
					required: {
								depends: function(element) {
											var display = $("#statusContainer").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  },
					date: true
				}
			},
			messages: {
				txtCardNumber: {
					digits: "El campo 'No. de Tarjeta' admite s&oacute;lo d&iacute;gitos."
				},
				txtBeginDate: {
					date: "Ingrese una fecha con formato dd/mm/YYYY para el campo 'Fecha Inicio'"
				},
				txtEndDate: {
					date: "Ingrese una fecha con formato dd/mm/YYYY para el campo 'Fecha Fin'"
				}
			}
	});
	/*END VALIDATION*/

	/*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-10y','+0d');
        setDefaultCalendar($("#txtEndDate"),'-10y','+0d');

		$("#txtBeginDate").bind("change", function(e){
			evaluateDates();
		});
		$("#txtEndDate").bind("change", function(e){
			 evaluateDates();
		});
    /*END CALENDAR*/

	$('#statusContainer').css('display','none');

	$('[name="rdFilterType"]').each(function(){
		$(this).bind('click',function(){
			switchFilters($(this).attr('id'));
		});
	});
});

function switchFilters(type){
	$('#alerts').hide();

	if(type=='CARD'){
		$('#statusContainer').hide();
		$('#cardContainer').show();

		$('#selStatus').val('');
	}else{//Status
		$('#cardContainer').hide();
		$('#statusContainer').show();

		$('#txtCardNumber').val('');
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>today){
			alert("La Fecha Hasta debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>today){
			alert("La Fecha Desde debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>endDate){
			 alert("La Fecha Hasta debe ser superior a la Fecha Desde.");
			 $("#txtBeginDate").val('');
		 }
	 }
}