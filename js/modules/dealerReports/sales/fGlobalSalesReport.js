$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmGlobalSales").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			dealersTo: {
				required: true
			},
			txtBeginDate: {
				required: true,
				date: true
			},
			txtEndDate: {
				required: true,
				date: true
			}
		}, messages: {
			txtBeginDate: {
				date: "El campo 'Fecha Inicio' debe tener el formato dd/mm/YYYY."
			},
			txtEndDate: {
				date: "El campo 'Fecha Fin' debe tener el formato dd/mm/YYYY."
			}
		}
	});
	/*END VALIDATION SECTION*/

	/*CALENDARS*/
	setDefaultCalendar($("#txtBeginDate"),'-100y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-100y','+0d');

	$("#txtBeginDate").bind('change', function(){
		validateDates();
	});

	$("#txtEndDate").bind('change', function(){
		validateDates();
	});
	/*END CALENDARS*/


	/* BINDING FUNCTIONS */	
	$('#btnGenerateXLS').bind('click', function(){
		submitFunction('XLS');
	});

	$('#btnGeneratePDF').bind('click', function(){
		submitFunction('PDF');
	});
	/* BINDING FUNCTIONS */

	listSelectorButtons();
});

function validateDates(){
	if($("#txtBeginDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtBeginDate").val());

		if(greater_than_today > 1){
			alert("La Fecha Inicio debe ser menor a la fecha actual");
			$("#txtBeginDate").val('');
		}
	}

	if($("#txtEndDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtEndDate").val());

		if(greater_than_today > 1){
			alert("La Fecha Fin debe ser menor a la fecha actual");
			$("#txtEndDate").val('');
		}
	}

    if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		var days = dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val());
		if(days <= 0){ //if departure date is smaller than arrival date
			alert("La Fecha Fin no puede ser menor a la Fecha Inicio");
			$("#txtEndDate").val('');
		}
	}
}

function submitFunction(type){
	$('#dealersTo :option').each(function (i) {
		$(this).attr('selected','selected');
	});

	if($("#frmGlobalSales").valid()){
		if(type == 'XLS'){
			$("#frmGlobalSales").attr('action', document_root + 'modules/dealerReports/sales/pGlobalSalesXLS');
		}else{
			$("#frmGlobalSales").attr('action', document_root + 'modules/dealerReports/sales/pGlobalSalesPDF');
		}

		$("#frmGlobalSales").submit();
	}
}

function listSelectorButtons(){
    /*LIST SELECTOR*/
    $('#moveAll').bind('click', function () {
        $('#dealersFrom option').each(function (i) {
            $('#dealersTo').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $('#removeAll').bind('click', function () {
        $('#dealersTo option').each(function (i) {
			$('#dealersFrom').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
			$(this).remove();
        });
    });

    $('#moveSelected').bind('click', function () {
        $('#dealersFrom option:selected').each(function (i) {
            $('#dealersTo').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

    $('#removeSelected').bind('click', function () {
        $('#dealersTo option:selected').each(function (i) {
			$('#dealersFrom').append('<option value="'+$(this).attr('value')+'" manager="'+$(this).attr('manager')+'" used="'+$(this).attr('used')+'" selected="selected">'+$(this).text()+'</option>');
			$(this).remove();
        });
    });
    /*END LIST SELECTOR*/
}