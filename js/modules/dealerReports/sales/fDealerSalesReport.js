$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmDealerSalesReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			hdnSerialCus: {
				required: function() {
							return $("#rdoCustomerSearch").attr("checked");
						  }
			},
			txtBeginDate: {
				required: function() {
							return $("#rdoParameterSearch").attr("checked");
						  }
			},
			txtEndDate: {
				required: function() {
							return $("#rdoParameterSearch").attr("checked");
						  }
			}
		}
	});
	/*END VALIDATION SECTION*/

	/*AUTOCOMPLETER*/
	$("#txtCustomerData").autocomplete(document_root+"rpc/autocompleters/loadCustomers.rpc",{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnCustomerID").val('');
			$("#txtCustomerData").val('');
		}else{
			$("#hdnSerialCus").val(row[1]);
		}
	});
	/*END AUTOCOMPLETER*/

	/*CALENDARS*/
	setDefaultCalendar($("#txtBeginDate"),'-100y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
	/*END CALENDARS*/

	/*EVENTS*/
	$('#rdoCustomerSearch').bind("click", function() {
		$('#txtCustomerData').attr("disabled", false);
		$('#selStatus').attr("disabled", true);
		$('#selStatus').val("");
		$('#selProduct').attr("disabled", true);
		$('#selProduct').val("");
		$('#txtBeginDate').attr("disabled", true);
		$('#txtBeginDate').val("");
		$('#txtEndDate').attr("disabled", true);
		$('#txtEndDate').val("");
	});
	$('#rdoParameterSearch').bind("click", function() {
		$('#selStatus').attr("disabled", false);
		$('#selProduct').attr("disabled", false);
		$('#txtBeginDate').attr("disabled", false);
		$('#txtEndDate').attr("disabled", false);
		$('#txtCustomerData').attr("disabled", true);
		$('#txtCustomerData').val("");
		$('#hdnSerialCus').val("");
	});
	$('#btnGeneratePDF').bind("click", function() {
		$("#frmDealerSalesReport").submit();
	})
	/*END EVENTS*/
});