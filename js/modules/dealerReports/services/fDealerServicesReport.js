// JavaScript Document
/*
File: fDealerBRBReport.js
Author: Valeria Andino C�lleri
Creation Date: 03/08/2016
Modified By: 
Last Modified:
*/
$(document).ready(function () {
    $("#selZone").bind("change", function () {
        loadCountry($(this).val());
    });
   
    $("#frmDealerServicesReport").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selZone: {
                required: true
            },
            selCountry: {
                required: true
            },            
            from: {
                required: true
            },
            to: {
                required: true
            }

        }       
    });
    $('#btnGenerateXLS').bind("click", function () {
        if ($("#frmDealerServicesReport").valid()) {
            $("#frmDealerServicesReport").attr("action", document_root + "modules/dealerReports/services/pDealerServicesReportXLS.php");
            $("#frmDealerServicesReport").submit();
        }
    });
    /* DATES */
    var dates = $("#from, #to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        showOn: 'button',
        buttonImage: document_root + 'img/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
        onSelect: function (selectedDate) {
            var option = this.id == "from" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                            $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings);
            dates.not(this).datepicker("option", option, date);
        }
    });
    
            function loadCountry(serial_zon){
            $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
                $('#selManager option[value=""]').attr("selected", true);
                $('#selCountry').bind("change", function(){
                    loadManager(this.value);
                });

                        if($('#selCountry option').size()<=2){
                                loadManager($('#selCountry').val());
                        }
            });
        }

            /*
         * loadManager
         * @param: serial_cou - Country ID
         * @return: A list of managers located in a specific country
         **/
        function loadManager(serial_cou){
            $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'}, function(){
                        $('#selManager').bind("change", function(){
                    loadDealer(this.value);
                });

                        if($('#selManager option').size()<=2){
                                loadDealer($('#selManager').val());
                        }
                });
        }

        /*
         * loadManager
         * @param: serial_cou - Country ID
         * @return: A list of managers located in a specific country
         **/
        function loadDealer(serial_mbc){
            $('#dealerContainer').load(document_root+"rpc/reports/productByDealer/loadDealersWithProducts.rpc",
                        {
                                serial_mbc: serial_mbc,
                                opt_text: 'Todos'
                        }
                        , function () {
               $("#selDealer").bind('change', function () {
                    loadServicesByDealer($('#selDealer').val(), $('#selCountry').val());
                });
                });
        }

        /*CALL TO THE RPC TO LOAD THE SERVICES
     *BASED ON THE COUNTRY SELECTED*/
    function loadServicesByDealer(dealerId, serial_cou) {
        
        $('#servicesContainer').load(document_root + "rpc/reports/services/loadServicesByDealer.rpc", {serial_dea: dealerId, serial_cou: serial_cou}, function () {});
        
    }
    
});
