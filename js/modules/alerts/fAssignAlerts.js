//Java Script
//var for paginator
var pager;
//loads all the groups available to assign alerts from the group(serial_asg) selected
function loadGroupsToAssign(serial_asg){
	$('#groupToContainer').load(document_root+"rpc/alerts/loadGroupsToReassign.rpc",
		  {serial_asg: serial_asg},
			function(){
				$('#selectedAlerts').val('');
				loadPendingAlerts(serial_asg);
			});
}
//load all pending alerts from the group selected
function loadPendingAlerts(serial_asg){
    //check if there is a valid serial_asg
	if(serial_asg!=''){
         $('#alertsContainer').load(document_root+"rpc/alerts/loadGroupAlerts.rpc",{serial_asg: serial_asg},
                                function(){
                                    if($("#selAll")){
                                            $("#selAll").bind("change",checkAll);
                                             $(".alertsChk").each(function(i){
                                                $(this).bind('click',function(){
                                                   alertsChecked();
                                                });
                                            });
                                    }
                                    //Paginator
                                    if($('#alertsTable').length>0){
                                        pager = new Pager('alertsTable', 10 , 'Siguiente', 'Anterior');
                                        pager.init(7); //Max Pages
                                        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
                                        pager.showPage(1); //Starting page
                                    }

                                });
	}else{
		$('#alertsContainer').html('');
	}

}

//checkAll
//select\deselect all alerts
function checkAll() {
        var nodoCheck = document.getElementsByTagName("input");
        var varCheck = document.getElementById("selAll").checked;
        for (i=0; i<nodoCheck.length; i++){
                if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "selAll" && nodoCheck[i].disabled == false) {
                        nodoCheck[i].checked = varCheck;
                }
        }
        alertsChecked();
}
//alertsChecked
//checks if theres is at least one alert choosen
function alertsChecked(){
     var checked=0;
    $(".alertsChk").each(function(i){
            if($(this).is(':checked')){
                checked=1;
                return;
            }
    });
    if(checked==1){
        $('#selectedAlerts').val('1');
    }else{
        $('#selectedAlerts').val('');
    }
}

$(document).ready(function(){
	$("#selFromGroup").bind("change", function(e){
			loadGroupsToAssign(this.value);
	});
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignAlert").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selFromGroup: {
				required: true
			},
			selToGroup: {
				required: true
			},
			selectedAlerts: {
				required: true
			}
		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/
});
