// JavaScript Document
$(document).ready(function(){
	$("#frmNewCreditDays").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			txtCDay: {
				required: {
					depends: function(element) {
						var display = $("#newInfo").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				number: true,
				min: true,
				remote: {
					url: document_root+"rpc/remoteValidation/creditDay/checkCreditDay.rpc",
					type: "post",
					data: {
					  serial_cou: function() {
						return $("#selCountry").val();
					  }
					}
				}
			}
		},
		messages: {
			txtCDay: {
				number: "Este campo admite s&oacute;lo n&uacute;meros.",
				remote: "El d&iacute;a ingresado ya consta para este pa&iacute;s.",
				min: "El d&iacute;a de cr&eacute;dito debe ser mayor a cero."
			}
		},
		submitHandler: function(form) {
			$('#btnSubmit').attr('disabled', 'true');
			$('#btnSubmit').val('Espere por favor...');
			form.submit();
	   }
	});
	
	$("#selCountry").change(loadCreditDayInfo);
	$("#newInfo").css("display","none");
	
	$("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 300,
		width: 410,
		modal: true,
		buttons: {
			'Guardar': function() {
				  $("#frmUpdateCreditDays").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							selStatus: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/creditDay/checkCreditDayStatus.rpc",
									type: "post",
									data: {
									  serial_cdd: function() {
										return $("#serialItem").val();
									  }
									}
								}
							},
							txtUpCreditD: {
								required: true,
								number: true,
								//max: 31,
								remote: {
									url: document_root+"rpc/remoteValidation/creditDay/checkCreditDay.rpc",
									type: "post",
									data: {
									  serial_cou: function() {
										return $("#serial_cou").val();
									  },
									  serial_cdd: function() {
										return $("#serialItem").val();
									  }
									}
								}
							}
						},
						messages: {
							txtUpCreditD: {
								number: "Este campo admite s&oacute;lo n&uacute;meros.",
								remote: "El d&iacute;a ingresado ya consta para este pa&iacute;s.",
								max: "El d&iacute;a de cr&eacute;dito m&aacute;ximo es el 31 de cada mes."
							},
							selStatus: {
								remote: "No se puede cambiar de Status porque esta asociado a un comercializador."
							}							
						}
					});
				  
				   if($('#frmUpdateCreditDays').valid()){
					$('#btnSubmit').attr('disabled', 'true');
					$('#btnSubmit').val('Espere por favor...');
					$('#frmUpdateCreditDays').submit();  
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});
});

function loadCreditDayInfo(){
	var countryID=$('#selCountry').val();
	if(countryID!=""){
		$('#alerts').addClass("alerts");
		$('#creditDayInfo').load(document_root+"rpc/loadCreditDayInfo.rpc",{serial_cou: countryID});
		
	}else{
		$('#creditDayInfo').html('');
		$('#newInfo').hide();
		$('#alerts').html('');
		$('#alerts').removeClass("alerts");
	}
}

function loadFormData(itemID){
	$('#serialItem').val(itemID);
	$('#serial_cou').val($('#selCountry').val());
	$('#txtUpCreditD').val($('#days_cdd_'+itemID).html());
	$("#selStatus option[text='"+$('#status_'+itemID).html()+"']").attr("selected",true);
}