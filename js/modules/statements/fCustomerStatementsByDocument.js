var pager;

$(document).ready(function () {
    $("#customerStatementContainer").hide();
    $("#notFoundMessage").hide();
});

// Get health statement results by customer document number using api
function getCustomerStatementsByDocument() {
    var customerDocument = $('#txtCustomerDocument').val();
    if (customerDocument) {
        show_ajax_loader();
        $.ajax({
            url: document_root + "rpc/remoteValidation/statements/statement.rpc",
            type: "POST",
            data: {action: 'getCustomerStatementsByDocument', customerDocument: customerDocument},
            // async:false,
            // dataType: 'json',
            success: function (response) {
                var data = JSON.parse(response);
                if (data.statement.length > 0) {
                    $("#notFoundMessage").hide();
                    getCustomerStatementDataTable(data.statement);
                } else {
                    hide_ajax_loader();
                    cleanForm();
                    $("#notFoundMessage").show();
                    // Hide not found message after 5 sec and fade out
                    setTimeout(function () {
                        $("#notFoundMessage").fadeOut(1000);
                    }, 4000);
                }
            }
        });
    }
}

// Show the results obtained in an html table with pagination and search filter
function getCustomerStatementDataTable(statements) {
    if (statements) {
        show_ajax_loader();
        $("#customerStatementContainer").load(document_root + "rpc/remoteValidation/statements/statement.rpc", {
            action: 'getCustomerStatementDataTable',
            statements: JSON.stringify(statements),
        }, function () {

            if ($('#customerStatementsTable').length > 0) {
                $("#customerStatementContainer").show();

                // Set Pagination settings in the results table
                setPagination();

                // Set Search filter configuration in the results table
                setSearchFilter();

            }

            hide_ajax_loader();

        });

    }
}

// Pagination settings in the results table
function setPagination() {
    pager = new Pager('customerStatementsTable', 10, 'Siguiente', 'Anterior');
    pager.init(20); // Max Pages
    pager.showPageNav('pager', 'pageNavPosition'); // Div where the navigation buttons will be placed.
    pager.showPage(1); // Starting page
}

// Search filter configuration in the results table
function setSearchFilter() {
    var rows = $('#customerStatementsTable tbody tr');
    $('#search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        if (val === "") {
            pager.showPage(1); // Max Pages
        } else {
            rows.show().filter(function () {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();

        }
    });
}

function sendStatementByEmail(element) {
    var statementId = element.id;
    if (statementId) {
        show_ajax_loader();
        $.ajax({
            url: document_root + "rpc/remoteValidation/statements/statement.rpc",
            type: "POST",
            data: {action: 'sendCustomerStatementEmail', statementId: statementId},
            //dataType: 'json',
            success: function (response) {
                //console.log(response);
                //var data = JSON.parse(response);
                hide_ajax_loader();
            }
        });
    }
}

function changeCustomerStatementStatus(element) {
    if (element) {
        var statementId = element.value;
        var statementStatus = element.checked ? 'ACTIVE' : 'INACTIVE';
        if (statementId) {
            show_ajax_loader();
            $.ajax({
                url: document_root + "rpc/remoteValidation/statements/statement.rpc",
                type: "POST",
                data: {action: 'changeStatementStatus', statementId: statementId, statementStatus: statementStatus},
                //dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response);
                    // location.reload();
                    hide_ajax_loader();
                }
            });
        }
    }
}

function cleanForm() {
    $("#customerStatementsTable tr").remove();
    $("#pageNavPosition").remove();
    $("#filterContainer").remove();
    $("#txtCustomerDocument").val('');
}

// Accept only alpha numerics, no special characters
function onlyAlphanumericDigits(e) {
    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
}