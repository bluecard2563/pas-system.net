//var pager;
$(document).ready(function(){

    if($('#customerStatementsTable').length > 0){
        var currentTable = $('#customerStatementsTable').dataTable( {
            "bJQueryUI": true,
            "aaSorting": [[ 0, "desc" ]],
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "Primera",
                    "sLast": "&Uacute;ltima",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                "sSearch": "Filtro:",
                "sProcessing": "Filtrando.."
            },
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
                "aButtons": ""
            }
        });
    }
});

function changeStatementStatus(element) {
    if (element) {
        var statementId = element.value;
        var statementStatus = element.checked ? 'ACTIVE' : 'INACTIVE';
        if (statementId) {
            show_ajax_loader();
            $.ajax({
                url: document_root + "rpc/remoteValidation/statements/statement.rpc",
                type: "POST",
                data: {action: 'changeStatementStatus', statementId: statementId, statementStatus: statementStatus},
                //dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response);
                    location.reload();
                    hide_ajax_loader();
                }
            });
        }
    }
}

function sendCustomerStatementEmail(element) {
    //console.log(element.id);
    var statementId = element.id;
    if (statementId) {
        show_ajax_loader();
        $.ajax({
            url: document_root + "rpc/remoteValidation/statements/statement.rpc",
            type: "POST",
            data: {action: 'sendCustomerStatementEmail', statementId: statementId},
            //dataType: 'json',
            success: function (response) {
                //console.log(response);
                //var data = JSON.parse(response);
                hide_ajax_loader();
            }
        });
    }
}