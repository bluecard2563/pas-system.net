$(document).ready(function () {
    $("#frmIncentiveDeliveryReport").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {},
        messages: {},
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#imgSubmitXLS').bind('click', function () {
        sendInfo('Excel');
    });
});

function sendInfo(exportTo) {
    $('#frmIncentiveDeliveryReport').attr('action', document_root + 'modules/reports/incentives/pIncentiveDeliveryReport' + exportTo);
    $('#frmIncentiveDeliveryReport').submit();
}