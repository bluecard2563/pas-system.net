// JavaScript Document
function loadCountry(serial_zon){
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
        $('#selCountry').bind("change",function(){
            loadCity(this.value);
            loadManager(this.value);
        });

		if($('#selCountry option').size()<=2){
			loadCity($('#selCountry').val());
            loadManager($('#selCountry').val());
		}
    });
}
function loadCity($serial_cou){
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: $serial_cou, charge_country:0, opt_text: 'Todos'});
}
function loadManager(serial_cou){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
		if($('#selManager option').size()<=2){
			loadResponsibles(this.value);
		}

		$('#selManager').bind('change',function(){
			loadResponsibles(this.value);
		});
	});
}
function loadResponsibles(serial_mbc){
    $('#responsibleContainer').load(document_root+"rpc/loadResponsablesByManager.rpc",{serial_mbc: serial_mbc, opt_text: 'Todos'});
}


$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmBirthdateReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                }
            }
    });
    /*END VALIDATION SECTION*/

    $('#selZone').bind("change",function(){
        loadCountry(this.value);
    });

    $('#btnGeneratePDF').bind("click", function(){
        if($("#frmBirthdateReport").valid()){
            $("#frmBirthdateReport").attr("action", document_root+"modules/reports/user/pBirthdatePDFReport.php");
            $("#frmBirthdateReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmBirthdateReport").valid()){
            $("#frmBirthdateReport").attr("action", document_root+"modules/reports/user/pBirthdateXLSReport.php");
            $("#frmBirthdateReport").submit();
        }
    });
});