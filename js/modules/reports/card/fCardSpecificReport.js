// JavaScript Document
var pager;
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmCardSpecificReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCreditNote: {
				required: true
			},
			txtNumCard: {
				required: {
					depends: function(element) {
						var display = $("#numCardContainer").css('display');
						return display=='block';
					}
				}
			},
			txtNumInvoice:{
				required: {
					depends: function(element) {
						var display = $("#numInvoiceContainer").css('display');
						return display=='block';
					}
				}
			},
			selCountry:{
				required: {
					depends: function(element) {
						var display = $("#numInvoiceContainer").css('display');
						return display=='block';
					}
				}
			},
			selManager:{
				required: {
					depends: function(element) {
						var display = $("#numInvoiceContainer").css('display');
						return display=='block';
					}
				}
			},
			selDocument:{
				required: {
					depends: function(element) {
						var display = $("#numInvoiceContainer").css('display');
						return display=='block';
					}
				}
			},
			hdnClientID: {
				required: {
					depends: function(element) {
						var display = $("#customerContainer").css('display');
						return display=='block';
					}
				}
			}
		}, 
		messages:{
			selCreditNote: {
				required: "Debe seleccionar una opci&oacute;n."
			},
			txtNumCard: {
				required: "El campo 'N&uacute;mero de la Tarjeta' es obligatorio"
			},
			txtNumInvoice:{
				required: "El campo 'N&uacute;mero de la Factura' es obligatorio"
			},
			hdnClientID: {
				required: "El campo 'Cliente' es obligatorio"
			}
		}
	});
	/*END VALIDATION SECTION*/
    
	$('#selCreditNote').bind("change", function(){
		/*RESET DATA*/
		$('#numCardContainer').css("display", "none");
		$('#numInvoiceContainer').css("display", "none");
		$('#customerContainer').css("display", "none");
		$('#txtNumCard').val('');
		$('#txtNumInvoice').val('');
		$('#txtClient').val('');
		$('#hdnClientID').val('');
		/*END RESET DATA*/

		if(this.value == "CARD") {
			$('#numCardContainer').css("display", "block");
		} else if(this.value == "INVOICE") {
			$('#numInvoiceContainer').css("display", "block");
		} else if(this.value == "CUSTOMER") {
			$('#customerContainer').css("display", "block");
		}

		$('#resultsContainer').html('');
	});

	/*CUSTOMER AUTO COMPLETER*/
	var ruta=document_root+"rpc/autocompleters/loadCustomers.rpc";
	$("#txtClient").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnClientID").val('');
			$("#txtClient").val('');
			$('#resultsContainer').html('');
		}else{
			$("#txtClient").val(row[0]);
			$("#hdnClientID").val(row[1]);
		}
	});
	/*END CUSTOMER AUTO COMPLETER*/

	$('#txtNumCard').bind('keyup',function(){
		$('#resultsContainer').html('');
	})

	$('#txtNumInvoice').bind('keyup',function(){
		$('#resultsContainer').html('');
	});

	$('txtClient').bind('keyup',function(){
		$('#resultsContainer').html('');
	});

	$('#btnGenerate').bind("click", function(){
		if($("#frmCardSpecificReport").valid()){
			show_ajax_loader();
			$('#resultsContainer').load(document_root+"rpc/reports/card/loadCardSpecificInfo.rpc", {
				selCreditNote: $('#selCreditNote').val(),
				txtNumCard: $('#txtNumCard').val(),
				txtNumInvoice: $('#txtNumInvoice').val(),
				hdnClientID: $('#hdnClientID').val(),
				selManager: $('#selManager').val(),
				selDocument: $('#selDocument').val()
			}, function(){
				/*BUTTONS*/
				$("input[id^='btnGenerateSaleReportPDF_']").each( function(e){
					$(this).bind('click',function(){
						salNum = $(this).attr('id').replace("btnGenerateSaleReportPDF_","");
						sendInfo('Sale', salNum);
					});
				});
				$("input[id^='btnGenerateInvoiceReportPDF']").each( function(e){
					$(this).bind('click',function(){
						salNum = $(this).attr('id').replace("btnGenerateInvoiceReportPDF_","");
						sendInfo('Invoice', salNum);
					});
				});

				if($('#card_table').length>0){
					pager = new Pager('card_table', 10 , 'Siguiente', 'Anterior');
					pager.init(20); //Max Pages
					pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
					pager.showPage(1); //Starting page
				}
				 
				hide_ajax_loader();
			});
		}
	});

	$('#selZone').bind("change",function(){
		loadCountries(this.value);
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	});
});

function loadCountries(serial_zon) {
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
		serial_zon: serial_zon
	},function(){
		$('#selCountry').bind("change",function(){
			loadManagersByCountry(this.value);
		});
		if($("#selCountry option").size() <=2 ){
			loadManagersByCountry($('#selCountry').val());
		}
	});
}

function loadManagersByCountry(countryID){
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc", {
		serial_cou: countryID
	}, function(e){
		$('#selManager').change(function(e){
			loadDocumentsByManager();
		});
		loadDocumentsByManager();
	});
}

function sendInfo(exportTo, salNum){
	if($('#frmCardSpecificReport').valid()){
		$('#saleSerial').val(salNum);
		$('#frmCardSpecificReport').attr('action',document_root+'modules/reports/card/p'+exportTo+'DetailReportPDF');
		$('#frmCardSpecificReport').submit();
	}
}

function loadDocumentsByManager(){
	managerID = $('#selManager').val();
	$('#documentToContainer').load(document_root+"rpc/loadDocumentsByManager.rpc",{
		serial_mbc: managerID
	});
}