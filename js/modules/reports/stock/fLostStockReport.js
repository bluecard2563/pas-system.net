/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//Java Script

function loadCountry(zoneID){
	 $('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	 $('#selResponsable').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
			serial_zon: zoneID
		},function(){
			$("#selCountry").bind("change", function(e){
			 loadManager(this.value);
			 });
			 if($("#selCountry option").size() ==2){
						loadManager($("#selCountry").val());
			 }
		});
}
function loadManager(countryID){
	 $('#selResponsable').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{
			serial_cou: countryID
		},function(){
			$("#selManager").bind("change", function(e){
			 loadResponsible(this.value);
			 });
			  if($("#selManager option").size() ==2){
						loadResponsible($("#selManager").val());
			  }
		});
}
function loadResponsible(managerID){
	 $('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#responsableContainer').load(document_root+"rpc/loadResponsablesByManager.rpc",{
			serial_mbc: managerID,opt_text:'Todos',use_serial_ubd:true
		},function(){
			$("#selResponsable").bind("change", function(e){
			 loadDealer(this.value);
			 });
			  if($("#selResponsable option").size() == 2){
						loadDealer($("#selResponsable").val());
			  }
		});
}
function loadDealer(responsibleID){
	if(responsibleID=='')
            {
                responsibleID='stop';
            }
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{
			serial_cou: $('#selCountry').val(),serial_usr:responsibleID,dea_serial_dea:'NULL',opt_text:'Todos'
		},function(){
			$("#selDealer").bind("change", function(e){
			  loadBranch(this.value);
			 });
			  if($("#selDealer option").size() ==2){
						loadBranch($("#selDealer").val());
			  }
		});
}
function loadBranch(dealerID){
	if(dealerID=='')
            {
                dealerID='stop';
            }
	$('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#branchContainer').load(document_root+"rpc/loadDealers.rpc",{
			dea_serial_dea:dealerID,opt_text:'Todas'
		},function(){
			$("#selBranch").bind("change", function(e){
			  loadCounter(this.value);
			 });
			 if($("#selBranch option").size() ==2){
						loadCounter($("#selBranch").val());
			  }
		});
}
function loadCounter(branchID){
	if(branchID==''){
		branchID='stop';
	}
	$('#counterContainer').load(document_root+"rpc/loadCounter.rpc",{
			serial_dea:branchID,opt_text:'Todos',all:true
		});
}

/*VALIDATE DATES RANGE*/
function validateDates(){
		var days=dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
		if(days<0){
			alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
			return false;
		}
		return true;
}

$(document).ready(function(){
	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
    $("#selZone").bind("change", function(e){
	      loadCountry(this.value);
    });
	$("#btnGenerateXLS").bind("click", function(e){
			$('#report_type').val('xls');
	});
	$("#btnGeneratePDF").bind("click", function(e){
		$('#report_type').val('pdf');
	});
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmLostStockReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			}
		},
		messages: {
                    selCountry: {
                        required: "El campo 'Pa&iacute;s' es obligatorio."
                    },
					selManager: {
                        required: "El campo 'Representante' es obligatorio."
                    }
   		},
		submitHandler: function(form) {
			if($("#txtDateFrom").length>0 && $("#txtDateTo").length>0){
				if(validateDates()){
					if($('#report_type').val()!=''){
						if($('#report_type').val()=='xls'){
							form.action=document_root+"modules/reports/stock/pLostStockReportXLS";
						}
						if($('#report_type').val()=='pdf'){
							form.action=document_root+"modules/reports/stock/pLostStockReportPDF";
						}
						form.submit();
					}
				}
			}else{
				if($('#report_type').val()!=''){
						if($('#report_type').val()=='xls'){
							form.action=document_root+"modules/reports/stock/pLostStockReportXLS";
						}
						if($('#report_type').val()=='pdf'){
							form.action=document_root+"modules/reports/stock/pLostStockReportPDF";
						}
					form.submit();
				}
			}
		}
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/
});



