//Java Script
$(document).ready(function(){
    $("#selZone").bind("change", function(e){
	      loadCountry(this.value);
    });
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmStockReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			txtBeginDate: {
				required: true
			},
			txtEndDate: {
				required: true
			}
		}
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/
	
	/*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-50y','+0d');
        setDefaultCalendar($("#txtEndDate"),'-50y','+0d');
    /*END CALENDAR*/
	
	/* CHANGE BINDS */
	$("#txtBeginDate").bind('change', function(){
		evaluateDates();
	});

	$("#txtEndDate").bind('change', function(){
		evaluateDates();
	});
	/* CHANGE BINDS */
	
	$('#btnGeneratePDF').bind("click", function(){
        if($("#frmStockReport").valid()){
            $("#frmStockReport").attr("action", document_root+"modules/reports/stock/pReportStockPDF");
            $("#frmStockReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmStockReport").valid()){
            $("#frmStockReport").attr("action", document_root+"modules/reports/stock/pReportStockXLS");
            $("#frmStockReport").submit();
        }
    });
});

function loadCountry(zoneID){
	 $('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selResponsable').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
			serial_zon: zoneID
		},function(){
			$("#selCountry").bind("change", function(e){
			 loadManager(this.value);
			 });
			 if($("#selCountry option").size() ==2){
						loadManager($("#selCountry").val());
			 }
		});
}

function loadManager(countryID){
	 $('#selResponsable').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{
			serial_cou: countryID,opt_text:'Todos'
		},function(){
			$("#selManager").bind("change", function(e){
			 loadResponsible(this.value);
			 });
			  if($("#selManager option").size() ==2){
						loadResponsible($("#selManager").val());
			  }
		});
}

function loadResponsible(managerID){
	 $('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#responsableContainer').load(document_root+"rpc/loadResponsablesByManager.rpc",{
			serial_mbc: managerID,opt_text:'Todos',use_serial_ubd:true
		},function(){
			$("#selResponsable").bind("change", function(e){
			 loadDealer(this.value);
			 });
			  if($("#selResponsable option").size() == 2){
						loadDealer($("#selResponsable").val());
			  }
		});
}

function loadDealer(responsibleID){
	if(responsibleID=='')
            {
                responsibleID='stop';
            }
	 $('#selBranch').find('option').remove().end().append('<option value="">- Todas -</option>').val('');
	 $('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{
			serial_cou: $('#selCountry').val(),serial_usr:responsibleID,dea_serial_dea:'NULL',opt_text:'Todos'
		},function(){
			$("#selDealer").bind("change", function(e){
			  loadBranch(this.value);
			 });
			  if($("#selDealer option").size() ==2){
						loadBranch($("#selDealer").val());
			  }
		});
}

function loadBranch(dealerID){
	if(dealerID=='')
            {
                dealerID='stop';
            }
	$('#selCounter').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#branchContainer').load(document_root+"rpc/loadDealers.rpc",{
			dea_serial_dea:dealerID,opt_text:'Todas'
		},function(){
			$("#selBranch").bind("change", function(e){
			  loadCounter(this.value);
			 });
			 if($("#selBranch option").size() ==2){
						loadCounter($("#selBranch").val());
			  }
		});
}

function loadCounter(branchID){
	if(branchID==''){
		branchID='stop';
	}
	$('#counterContainer').load(document_root+"rpc/loadCounter.rpc",{
			serial_dea:branchID,opt_text:'Todos',all:true
		});
}

function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>today){
			alert("La Fecha Hasta debe ser menor o igual a la fecha actual.");
			$("#txtEndDate").val('');
		}
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>today){
			alert("La Fecha Desde debe ser menor o igual a la fecha actual.");
			$("#txtBeginDate").val('');
		}
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>endDate){
			 alert("La Fecha Hasta debe ser igual o superior a la Fecha Desde.");
			 $("#txtEndDate").val('');
		 }
	 }
}