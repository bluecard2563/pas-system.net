// JavaScript Document
$(document).ready(function(){
	$("#frmProfileReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			}
		}
	});
    $('#selZone').bind("change",function(){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0});
    });

	$('#imgSubmitPDF').bind('click',function(){
		sendInfo('PDF');
	});
});

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmProfileReport').valid()){
		$('#frmProfileReport').attr('action',document_root+'modules/reports/profile/pProfileReport'+exportTo);
		$('#frmProfileReport').submit();
	}
}