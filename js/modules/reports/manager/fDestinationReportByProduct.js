// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmCardReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                txtBeginDate: {
                    required: true,
                    date: true
                },
                txtEndDate: {
                    required: true,
                    date: true
                }
            },
            messages:{
                txtAmount: {
                    number: 'El campo "Monto" solo admite valores num\u00E9ricos.'
                },
                txtBeginDate: {
                    date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
                }
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
	});

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmCardReport").valid()){
            $("#frmCardReport").attr("action", document_root+"modules/reports/manager/pDestinationReportByProductXLS.php");
            $("#frmCardReport").submit();
        }
    });

    /*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
        setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
    /*END CALENDAR*/

	/* CHANGE BINDS */
	$("#txtBeginDate").bind('change', function(){
		evaluateDates();
	});

	$("#txtEndDate").bind('change', function(){
		evaluateDates();
	});
	/* CHANGE BINDS */
});

function loadCountry(serial_zon){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
		$('#selCountry').bind("change",function(){
				loadManagersByCountry(this.value);
                                loadProductByCountry(this.value);
                                loadDealerType($('#selCountry').val())
		});

		if($('#selCountry option').size()<=2){
			loadManagersByCountry($('#selCountry').val());
                        loadProductByCountry($('#selCountry').val());
                        loadDealerType($('#selCountry').val());
		}
	});
}

function loadManagersByCountry(serial_cou){
	if(serial_cou){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
			$('#selManager').bind('change', function(){
            	loadComissionists($(this).val());
            });
		});
	}else{
    	loadComissionists('');
    }
}

function loadDealerType(serial_cou){
	if(serial_cou){
		$('#typeContainer').load(document_root+"rpc/loadDealerType.rpc",{opt_text: 'Todos'},function(){
			$('#selType').bind('change', function(){
            	loadDealers();
            });
		});
	}else{
    	
    }
}

/* LOAD COMISSIONISTS FROM MANAGER
 */
function loadComissionists(managerId){
    if(managerId){
        $('#comissionistsContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: managerId},function(){
            $("#selComissionist").bind('change', function(){
            	loadDealers();
            });
        });
    }else{
    	$('#comissionistsContainer').html('Seleccione un representante');
    	loadDealers();
    }
}


/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val() && $('#selComissionist').val()){
        $('#comercializadorContainer').load(document_root+"rpc/loadDealersByUbd.rpc",{
        	city: $("#selCity").val(),
        	serial_mbc: $('#selManager').val(), 
                serial_type : $('#selType').val(), 
        	serialComissionist: $('#selComissionist').val()},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				loadBranches('');
	        });
    }else{
    	$('#comercializadorContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}
function loadProductByCountry(serial_cou){
	if(serial_cou){
		$('#productContainer').load(document_root+"rpc/loadProducts.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
			$('#selManager').bind('change', function(){
            	
            });
		});
	}else{
    	
    }
}



function evaluateDates(){
	var beginDate=$('#txtBeginDate').val();
	var endDate=$('#txtEndDate').val();

	if(beginDate && endDate){
		var new_days=parseFloat(dayDiff(beginDate,endDate));

		if(new_days < 0){
			alert('La fecha fin debe ser mayor a la fecha inicio.');
			$('#txtEndDate').val('');
		}else if(new_days > 365){
			alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
		}
	}
}