// JavaScript Document
$(document).ready(function(){
    $('#selZone').bind("change",function(){
        $('#taxContainer').html("");
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
                    $('#taxContainer').html("");
            });
        });
    });

    $('#btnGenerate').bind("click", function(){
        if(validate()){
            $('#taxContainer').load(document_root+"rpc/reports/loadTaxByCountry.rpc", {serial_cou: $('#selCountry').val()}, function(){
                if($("#tax_table").length>0) {
                    pager = new Pager('tax_table', 10 , 'Siguiente', 'Anterior');
                    pager.init(7); //Max Pages
                    pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
                    pager.showPage(1); //Starting page
                }
            });
        }
    });
});

function validate(){
    var valid = true;
    $('#alerts').html("");
    $('#alerts').css("display", "none");
    if($('#selZone').val() == ""){
        $('#alerts').append("<li><b>"+$('#selZone').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    } else if($('#selCountry').val() == "") {
        $('#alerts').append("<li><b>"+$('#selCountry').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    }
    return valid;
}