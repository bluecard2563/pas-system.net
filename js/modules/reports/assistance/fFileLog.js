// JavaScript Document
$(document).ready(function(){
	$("#frmFileLog").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFileNro: {
				digits: true
			},
			txtBeginDate: {
				required: true
			},
			txtEndDate: {
				required: true
			}
		},
		messages: {
			txtFileNro: {
				digits: "El campo 'Nro. Expediente' admite solo n&uacute;meros."
			}
		}
	});
	
	$('#imgSubmitXLS').bind('click',function(){
		if($('#frmFileLog').valid()){
			$('#frmFileLog').submit();
		}
	});
	
	/*CALENDAR*/
	var days_passed=dayDiff($('#first_day').val(),$('#today').val());

	setDefaultCalendar($("#txtBeginDate"),'-5y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-5y','+0d');

	$("#txtBeginDate").bind("change", function(e){
		 evaluateDates();
	});
	$("#txtEndDate").bind("change", function(e){
		 evaluateDates();
	});
    /*END CALENDAR*/	
});

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>=today){
			alert("La Fecha Fin debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>=today){
			alert("La Fecha Inicio debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>=endDate){
			 alert("La Fecha Fin debe ser superior a la Fecha Inicio.");
			 $("#txtEndDate").val('');
		 }
	 }
}