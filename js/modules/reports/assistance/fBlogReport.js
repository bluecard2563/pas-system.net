// JavaScript Document

$(document).ready(function(){
	$("#frmBlogReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCustomer: {
				minlength: 7,
				textOnly: true
			},
			txtCard: {
				digits: true
			},
			txtFile: {
				digits: true
			},
			hdnControl: {
				required: true
			}
		},
		messages: {
			txtCustomer: {
				minlength: "El campo 'Nombre del cliente' debe tener al menos 7 caracteres."
			}
		}
	});

	$('#txtCustomer').bind('focusout', function(){
		$('#txtFile').val('');
		$('#txtCard').val('');
		if(this.value != '') {
			$('#hdnControl').val('1');
		} else {
			$('#hdnControl').val('');
		}
	});

	$('#txtCard').bind('focusout', function(){
		$('#txtCustomer').val('');
		$('#txtFile').val('');
		if(this.value != '') {
			$('#hdnControl').val('1');
		} else {
			$('#hdnControl').val('');
		}
	});
	
	$('#txtFile').bind('focusout', function(){
		$('#txtCustomer').val('');
		$('#txtCard').val('');
		if(this.value != '') {
			$('#hdnControl').val('1');
		} else {
			$('#hdnControl').val('');
		}
	});

	

	$('#btnSearch').bind('click',function(){
		$("#filesInfoContainer").html('');
		if($("#frmBlogReport").valid()){
			show_ajax_loader();
			if($("#txtCustomer").val()!=""){
                $("#filesInfoContainer").load(document_root+"rpc/reports/assistance/loadCustomerByName.rpc",{name_cus:$("#txtCustomer").val()},function(){
					hide_ajax_loader();
				});
				
            } else if($("#txtCard").val()!=""){
                 $("#filesInfoContainer").load(document_root+"rpc/reports/assistance/loadFilesByCardNumber.rpc",{card_number:$("#txtCard").val()},function(){
					$("img[id^='imgSubmitPDF']").bind("click", function(){
						$('#frmBlogReport').attr('action',document_root+'modules/reports/assistance/pAssistanceReport/'+$(this).attr('serial_fle'));
						$('#frmBlogReport').submit();
					});
					hide_ajax_loader();
				});
            } else if($("#txtFile").val()!=""){
				$("#filesInfoContainer").load(document_root+"rpc/reports/assistance/loadFileByNumber.rpc",{file_num:$("#txtFile").val()},function(){
					$("#imgSubmitPDF").bind("click", function(){
						$('#frmBlogReport').attr('action',document_root+'modules/reports/assistance/pAssistanceReport/'+$(this).attr('serial_fle'));
						$('#frmBlogReport').submit();
					});
					hide_ajax_loader();
				});
			} else {
				$("#filesInfoContainer").html("Ocurrio un error. Por favor comunicarse con un asistente t&eacutecnico.",function(){					
					hide_ajax_loader();
				});
			}
		}
	});
});