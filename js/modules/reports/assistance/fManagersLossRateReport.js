// JavaScript Document
$(document).ready(function(){
	$("#frmAssistanceLossRate").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			txtBeginDate: {
				required: true
			},
			txtEndDate: {
				required: true
			}
		}
	});
    $('#selZone').bind("change",function(){
		clearfields('zone');
        loadCountries($(this).val());
    });
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('XLS');
	});

	/*CALENDAR*/
		var days_passed=dayDiff($('#first_day').val(),$('#today').val());

        setDefaultCalendar($("#txtBeginDate"),'-'+(days_passed-1)+'d','-1d');
        setDefaultCalendar($("#txtEndDate"),'-'+(days_passed-1)+'d','-1d');
		
		$("#txtBeginDate").bind("change", function(e){
			 evaluateDates();
		});
		$("#txtEndDate").bind("change", function(e){
			 evaluateDates();
		});
    /*END CALENDAR*/	
});

function loadCountries(serial_zon){
	$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	
	$('#countryContainer').load(document_root + "rpc/reports/dealer/loadDealerCountries.rpc",{serial_zon: serial_zon},function(){
		$('#selCountry').bind("change",function(){
			clearfields('country');
			loadManagerByCountry(this.value);
			loadCities(this.value);
		});

		if($('#selCountry option').size()<=2){
			loadManagerByCountry($('#selCountry').val());
			loadCities($('#selCountry').val());
		}
	});
}

function loadManagerByCountry(serial_cou){
	$('#managerContiner').load(document_root + "rpc/reports/dealer/loadManagersWithDealers.rpc",{serial_cou: serial_cou,opt_text:'Todos'},function(){
		if($('#selManager :option').length == 2){
			loadDealers();
		}		
		
		$('#selManager').bind('change', function(){
			clearfields('manager');
			loadDealers();
		});
	});
}

/* LOAD COMISSIONISTS FROM MANAGER
 */
function loadCities(serial_cou){
	if(serial_cou){//IF A COUNTRY HAS BEEN SELECTED
        $('#cityContainer').load(document_root+"rpc/reports/payments/loadCitiesByCountry.rpc", 
			{
				serial_cou: serial_cou, 
				type: 'analysis'
			}, function(){		
				if($('#selCity :option').length == 2){
					clearfields('city');
					loadDealers();
				}
				
				$('#selCity').bind('change', function(){
					clearfields('city');
					loadDealers();
				});				
        });
    }else{
       // $('#cityContainer').html('Seleccione un Pa\u00EDs');
    }
}

/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val() || $('#selCity').val()){
        $('#dealerContiner').load(document_root+"rpc/loadDealers.rpc",{
        	serial_mbc: $('#selManager').val(), 
        	serial_cit: $('#selCity').val(),
			dea_serial_dea: 'NULL'
		},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				loadBranches('');
	        });
    }else{
    	$('#dealerContiner').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmAssistanceLossRate').valid()){
		$('#frmAssistanceLossRate').submit();
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>=today){
			alert("La Fecha Fin debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>=today){
			alert("La Fecha Inicio debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>=endDate){
			 alert("La Fecha Fin debe ser superior a la Fecha Inicio.");
			 $("#txtEndDate").val('');
		 }
	 }
}

function clearfields(changing_element){
	switch(changing_element){
		case 'zone':
			$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selCity').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			break;
		case 'country':
			$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selCity').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			break;
		case 'city':
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');			
			break;
	}
}