// JavaScript Document
$(document).ready(function(){
	/*CALENDARS*/
	setDefaultCalendar($("#txtBeginDate"),'-100y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-100y','+0d');

	$("#txtBeginDate").bind('change', function(){
		validateDates();
	});

	$("#txtEndDate").bind('change', function(){
		validateDates();
	});
	/*END CALENDARS*/
});

function validateDates()
{
	if($("#txtBeginDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtBeginDate").val());

		if(greater_than_today > 1){
			alert("La Fecha Inicio debe ser menor a la fecha actual");
			$("#txtBeginDate").val('');
		}
	}

	if($("#txtEndDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtEndDate").val());

		if(greater_than_today > 1){
			alert("La Fecha Fin debe ser menor a la fecha actual");
			$("#txtEndDate").val('');
		}
	}

    if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		var days = dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val());
		if(days <= 0){ //if departure date is smaller than arrival date
			alert("La Fecha Fin no puede ser menor a la Fecha Inicio");
			$("#txtEndDate").val('');
		}
	}
}
