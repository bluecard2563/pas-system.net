// JavaScript Document

$(document).ready(function(){
	$("#frmMedicalCheckupReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCard: {
				required: true,
				digits: true
			}
		},
		messages: {
			txtCard: {
				digits: "El campo 'N&uacute;mero de tarjeta' solo admite n&uacute;meros."
			}
		}
	});

	$('#btnSearch').bind('click',function(){
		$("#filesInfoContainer").html('');

		if($("#frmMedicalCheckupReport").valid()){
			show_ajax_loader();

			$("#filesInfoContainer").load(document_root+"rpc/reports/assistance/loadFilesWithMedicalCheckup.rpc",{card_number:$("#txtCard").val()},function(){
				$("img[id^='imgSubmitPDF']").bind("click", function(){
					$('#frmMedicalCheckupReport').attr('action',document_root+'modules/reports/assistance/pMedicalCheckupReportPDF/'+$(this).attr('serial_prq'));
					$('#frmMedicalCheckupReport').submit();
				});
				hide_ajax_loader();
			});

		}

	});
});