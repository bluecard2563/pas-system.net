// JavaScript Document
$(document).ready(function(){
	/*AUTOCOMPLETER*/
	$("#txtCustomer").autocomplete(document_root+"rpc/autocompleters/loadRegisterCustomers.rpc",{
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
		loadingClass: 'process_loading',
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnSerial_cus").val('');
			$("#txtCustomer").val('');
		}else{
			$("#hdnSerial_cus").val(row[1]);
		}

	});
	/*END AUTOCOMPLETER*/
	
	$("#txtCustomer").bind("keypress", function(e){
		$("#hdnSerial_cus").val('');
		$("#cardsContainer").html('');
	});
	
	$('#btnSearch').bind('click', function(){
		if($('#hdnSerial_cus').val()){
			searchRegisterCards();
		}else{
			alert('Seleccione un cliente para continuar. Recuerde que debe tomar un cliente de la lista.');
		}
	});
});

function searchRegisterCards(){
	var serial_cus=$('#hdnSerial_cus').val();
	var front_type= 'LOSS_RATE';
	
	$('#cardsContainer').load(document_root+"rpc/loadTravelRegister/loadRegisterCardByCustomer.rpc",{
		serial_cus: serial_cus, 
		front_type: front_type
	},
	function(){
		//Paginator
		if($('#travelsTable').length>0){
			pager = new Pager('travelsTable', 10 , 'Siguiente', 'Anterior');
			pager.init(7); //Max Pages
			pager.showPageNav('pager', 'travelPageNavPosition'); //Div where the navigation buttons will be placed.
			pager.showPage(1); //Starting page
		}

	});
}


