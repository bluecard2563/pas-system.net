// JavaScript Document
$(document).ready(function(){
	$("#frmOpenedFilesReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selGroup: {
				required: true
			}
		}
	});
    

	/*CALENDAR*/
	var days_passed=dayDiff($('#first_day').val(),$('#today').val());
	setDefaultCalendar($("#txtDateFilter"),'-'+(days_passed-1)+'d','-1d');

	$("#txtDateFilter").bind("change", function(e){
		 evaluateDates();
	});
    /*END CALENDAR*/	
	
	$('#imgSubmitXLS').bind('click', function(){
		sendInfo('XLS');
	})
	
	$('#imgSubmitPDF').bind('click', function(){
		sendInfo('PDF');
	})
});


/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
	if(exportTo == 'XLS'){
		$('#frmOpenedFilesReport').attr('action', document_root + 'modules/reports/assistance/pOpenedFilesReportXLS');
	}else{
		$('#frmOpenedFilesReport').attr('action', document_root + 'modules/reports/assistance/pOpenedFilesReportPDF');
	}
	
    if($('#frmOpenedFilesReport').valid()){
		$('#frmOpenedFilesReport').submit();
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtDateFilter").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtDateFilter').val())));
		if(endDate>=today){
			alert("La Fecha debe ser menor o igual a la fecha actual.");
			$("#txtDateFilter").val('');
		}
	}
}