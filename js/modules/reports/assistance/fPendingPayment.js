//Java Script
$(document).ready(function(){
	$('#btnGeneratePDF').bind('click',function(){
		submitReport('PDF');
	});
	$('#btnGenerateXLS').bind('click',function(){
		submitReport('XLS');
	});
/*Calendars*/
    setDefaultCalendar($("#txtBeginDate"),'-2y','+1y','+0d');
    setDefaultCalendar($("#txtEndDate"),'-2y','+1y','+0d');
    /*End Calendars*/
	//FORM VALIDATION
$.validator.addMethod("enddate", function(value, element) {
		var startdatevalue = $('#txtBeginDate').val();
		//Date.parse IS NOT WORKING PROPERLY
		//return Date.parse(startdatevalue) < Date.parse(value);
		
		//manual comparison:
		var dateBefore = startdatevalue.split("/");
		var dateAfter = value.split("/");
		
		//compare year
		if(dateBefore[2]<dateAfter[2]){
			return true;
		}else if(dateBefore[2]=dateAfter[2]){
			if(dateBefore[1]<dateAfter[1]){
					return true;
			}else if(dateBefore[1]==dateAfter[1]){
				if(dateBefore[0]<dateAfter[0]){
					return true;
				}
			}
		}
		return false;
	});	
$("#frmPendingPayment").validate({
	errorLabelContainer: "#alerts",
	wrapper: "li",
	onfocusout: false,
	onkeyup: false,
	rules: {
		selPayTo: {                            
			required: true
		},
		selType: {                            
			required: true
		},
		txtBeginDate:{
			required: true,
			date: true
		},
		txtEndDate:{
			enddate: true,
			required: true,
			date: true
		}
	},
	messages: {
			txtBeginDate: {
                date: "El campo 'Fecha Inicio' debe tener el formato mm/dd/YYYY"
            },
			txtEndDate: {
                date: "El campo 'Fecha Inicio' debe tener el formato mm/dd/YYYY",
                enddate: "La 'Fecha Inicio' debe ser menor que Fecha Fin."
            }
	 }
});
//END VALIDATION

function submitReport(type){
	alert(type);
	if($("#frmPendingPayment").valid()){
		if(type == 'PDF'){
				$("#frmPendingPayment").attr("action", document_root+"modules/reports/assistance/pPendingPaymentPDF.php");
			 }
			 else{
				$("#frmPendingPayment").attr("action", document_root+"modules/reports/assistance/pPendingPaymentXLS.php");
			 }
		$("#frmPendingPayment").submit();
	}
}
});	
	