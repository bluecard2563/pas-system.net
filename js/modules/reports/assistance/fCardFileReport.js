// JavaScript Document
$(document).ready(function(){
	$("#frmCardFileReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			txtBeginDate: {
				required: true
			},
			txtEndDate: {
				required: true
			}
		}
	});
    $('#selZone').bind("change",function(){
		clearfields('zone');
        loadCountries($(this).val());
    });
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('XLS');
	});

	/*CALENDAR*/
		var days_passed=dayDiff($('#first_day').val(),$('#today').val());

        setDefaultCalendar($("#txtBeginDate"),'-'+(days_passed-1)+'d','-1d');
        setDefaultCalendar($("#txtEndDate"),'-'+(days_passed-1)+'d','-1d');
		
		$("#txtBeginDate").bind("change", function(e){
			 evaluateDates();
		});
		$("#txtEndDate").bind("change", function(e){
			 evaluateDates();
		});
    /*END CALENDAR*/	
});

function loadCountries(serial_zon){
	$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	
	$('#countryContainer').load(document_root + "rpc/reports/dealer/loadDealerCountries.rpc",{serial_zon: serial_zon},function(){
		$('#selCountry').bind("change",function(){
			clearfields('country');
			loadManagerByCountry(this.value);
			loadProductByCountry(this.value);
		});

		if($('#selCountry option').size()<=2){
			
			loadManagerByCountry($('#selCountry').val());
			loadProductByCountry($('#selCountry').val());
		}
	});
}

function loadManagerByCountry(serial_cou){
	$('#managerContiner').load(document_root + "rpc/reports/dealer/loadManagersWithDealers.rpc",{serial_cou: serial_cou},function(){
		if($('#selManager :option').length == 2){
			loadComissionists($('#selManager').val());
		}		
		
		$('#selManager').bind('change', function(){
			clearfields('manager');
			loadComissionists($(this).val());
		});
	});
}

/* LOAD COMISSIONISTS FROM MANAGER
 */
function loadComissionists(managerId){
    if(managerId){
        $('#comissionistsContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: managerId},function(){
            $("#selComissionist").bind('change', function(){
            	loadDealers();
            });
        });
    }else{
    	$('#comissionistsContainer').html('Seleccione un representante');
    	loadDealers();
    }
}

/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val() && $('#selComissionist').val()){
        $('#dealerContiner').load(document_root+"rpc/loadDealers.rpc",{
        	serial_mbc: $('#selManager').val(), 
        	serial_usr: $('#selComissionist').val(),
			dea_serial_dea: 'NULL'
		},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				loadBranches('');
	        });
    }else{
    	$('#dealerContiner').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}

function loadProductByCountry(serial_cou){
	$('#selProduct').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	
	if(serial_cou!=''){
		$('#productContainer').load(document_root+"rpc/reports/sales/loadProductsByCountry.rpc",{serial_cou: serial_cou, reports: 'true'});
	}
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmCardFileReport').valid()){
		$('#frmCardFileReport').attr('action',document_root+'modules/reports/assistance/pCardFileReport'+exportTo);
		$('#frmCardFileReport').submit();
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>=today){
			alert("La Fecha Fin debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>=today){
			alert("La Fecha Inicio debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>=endDate){
			 alert("La Fecha Fin debe ser superior a la Fecha Inicio.");
			 $("#txtEndDate").val('');
		 }
	 }
}

function clearfields(changing_element){
	switch(changing_element){
		case 'zone':
			$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selProduct').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			break;
		case 'country':
			$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			break;
		case 'manager':
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');			
			break;
	}
}