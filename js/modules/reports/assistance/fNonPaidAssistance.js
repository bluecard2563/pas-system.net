// JavaScript Document
var currentTable;
$(document).ready(function(){	
	$('#cardNumberFilter').hide();
	
	$('[id^="filter_"]').each(function(){
		$(this).bind('click', function(){
			clear_container($(this).val());
			
			if($(this).val() == 'CARD'){
				$('#geographicFilter').hide();
				$('#cardNumberFilter').show();
			}else{
				$('#geographicFilter').show();
				$('#cardNumberFilter').hide();
			}			
		});		
	});	
	
	//**************** VALIDATE BUTTON ACTIONS
	$('#btnValidate').bind('click', function(){
		clear_container('container');
		var valid_search = true;
		
		//VERIFY IF THE CARD NUMBER FIELD IS ENTERED TO PROCEED
		if($('#cardNumberFilter').css('display') == 'block' && $('#txtCardNumber').val() == ''){
			valid_search = false;
		}
		
		if(valid_search){
			getOnLineRegisters();
		}else{
			alert('Ingrese el n\u00FAmero de tarjeta primero.');
		}
	});
	
	//***************** BIND ACTIONS TO GEOGRAPHIC SELECTS
	$('#selCountry').bind('change', function(){
		show_ajax_loader();
		clear_container('manager');
		$('#managerContainer').load(document_root + 'rpc/reports/assistance/loadManagersNonPaidAssistance.rpc', {
			serial_cou: $('#selCountry').val()
		}, function(){
			$('#selManager').bind('change', loadResponsiblesByManager);
			
			hide_ajax_loader();
			if($('#selManager :option').length == 2){
				loadResponsiblesByManager();
			}
		});
	});
});

/**
 * @name loadResponsiblesByManager
 */
function loadResponsiblesByManager()
{
	clear_container('responsible');
	show_ajax_loader();
	$('#responsibleContainer').load(document_root + 'rpc/reports/assistance/loadResponsiblesNonPaidAssistance.rpc',
	{
		serial_mbc: $('#selManager').val()
	},
	function(){
		$('#selResponsable').bind('change', function(){
			loadDealersByResponsibles();
		});
		
		hide_ajax_loader();
		if($('#selResponsable :option').length == 2){
			loadDealersByResponsibles();
		}
	});
}

function loadDealersByResponsibles()
{
	clear_container('container');
	show_ajax_loader();
	$('#dealerContainer').load(document_root + 'rpc/reports/assistance/loadDealersNonPaidAssistance.rpc',
	{
		serial_ubd: $('#selResponsable').val()
	},
	function(){		
		hide_ajax_loader();
	});
}

/**
 * @name getOnLineRegisters
 * @description Display on-line registers and enable XLS export functions
 */
function getOnLineRegisters()
{
	show_ajax_loader();
	$('#filesContainer').load(document_root + 'rpc/reports/assistance/loadAssistancesWithNonPaidInvoice.rpc',
		{
			serial_cou: $('#selCountry').val(),
			serial_mbc: $('#selManager').val(),
			serial_usr: $('#selResponsable').val(),
			serial_dea: $('#selDealer').val(),
			card_number: $('#txtCardNumber').val()
		},
		function(){
			/*Style dataTable*/
			if($('#files_table').length > 0){
				currentTable = $('#files_table').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": {
						"oPaginate": {
							"sFirst": "Primera",
							"sLast": "&Uacute;ltima",
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						},
						"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
						"sZeroRecords": "No se encontraron resultados",
						"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
						"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
						"sSearch": "Filtro:",
						"sProcessing": "Filtrando.."
					},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
						"aButtons": []
					}
				});
				
				$('#btnGenerateXLS').bind('click', function(){
					$('#frmNonPaidAssistanceReport').submit();
				});
			}
			
			hide_ajax_loader();
		});
}

/**
 * @name clear_container
 * @description Clean container area
 */
function clear_container(section)
{
	switch(section){
		case 'container':
			$('#filesContainer').html('');
			break;
		case 'responsible':
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			clear_container('container');
			break;
		case 'manager':
			$('#selResponsable').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			clear_container('responsible');
			break;
		case 'CARD':
			$('#selCountry').val('');
			$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			clear_container('manager');
			break;
		case 'GEOGRAPHIC':
			$('#txtCardNumber').val('');
			clear_container('container');
			break;
	}
}