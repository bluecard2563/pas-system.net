// JavaScript Document
$(document).ready(function(){
	/*CALENDARS*/
	setDefaultCalendar($("#txtBeginDate"),'-100y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
	
	$('#selProvider').bind('change', function(){
		clearContainer();
	});

	$("#txtBeginDate").bind('change', function(){
		validateDates();
		clearContainer();
	});

	$("#txtEndDate").bind('change', function(){
		validateDates();
		clearContainer();
	});
	/*END CALENDARS*/
	
	$('#btnValidate').bind('click', function(){
		loadCases();
	});
});

function loadCases()
{
	show_ajax_loader();
	var serial_spv = $('#selProvider').val();
	var date_from = $('#txtBeginDate').val();
	var date_to = $('#txtEndDate').val();
	clearContainer();
		
	$('#detailsContainer').load(document_root + 'rpc/reports/assistance/loadFilesBySProvider.rpc', 
		{
			serial_spv: serial_spv,
			date_from: date_from,
			date_to: date_to
		}, function(){
			
			
			
			hide_ajax_loader();
		});
}

function validateDates()
{
	if($("#txtBeginDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtBeginDate").val());

		if(greater_than_today > 1){
			alert("La Fecha Inicio debe ser menor a la fecha actual");
			$("#txtBeginDate").val('');
		}
	}

	if($("#txtEndDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtEndDate").val());

		if(greater_than_today > 1){
			alert("La Fecha Fin debe ser menor a la fecha actual");
			$("#txtEndDate").val('');
		}
	}

    if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		var days = dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val());
		if(days <= 0){ //if departure date is smaller than arrival date
			alert("La Fecha Fin no puede ser menor a la Fecha Inicio");
			$("#txtEndDate").val('');
		}
	}
}

function clearContainer()
{
	$('#detailsContainer').html('');
}
