//Java Script
var currentTable;
$(document).ready(function(){
	$("#frmAssistancePayment").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selPayTo: {
				required: true
			},
			selProvider:{
				//required: {
				//	depends: function(element) {
				//		return $('#providerContainer').css("display") == "block";
					//}
				//}
			},
			selZone: {
				required: {
					depends: function(element) {
						return $('#clientContainer').css("display") == "block";
					}
				}
			},
			selCountry: {
				required: {
					depends: function(element) {
						return $('#clientContainer').css("display") == "block";
					}
				}
			},
			txtBeginDate:{
				required: true,
				date: true
			},
			txtEndDate:{
				required: true,
				date: true
			}
		},
		messages: {
			txtBeginDate: {
				date: "El campo 'Fecha Inicio' debe tener el formato mm/dd/YYYY"
			},
			txtEndDate: {
				date: "El campo 'Fecha Inicio' debe tener el formato mm/dd/YYYY"
			}
		}
	});

	/*Calendars*/
	setDefaultCalendar($("#txtBeginDate"),'-100y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
	
	$('#txtBeginDate').bind("change",function(){
		$('#data_container').html("");
		validateDates();
	});

	$('#txtEndDate').bind("change",function(){
		$('#data_container').html("");
		validateDates();
	});   
        
        $('#selProcessAs').bind("change", function() {
            $('#data_container').html("");
        });
	/*End Calendars*/

	
	//****************************** BIND SECTION - PAY_TO SELECTION BOX
	$('#selPayTo').bind("change",function(){
		$('#data_container').html("");
		
		if(this.value == "PROVIDER") {
			$('#providerContainer').show();
			$('#clientContainer').hide();
			
			$("#selZone option[value='']").attr("selected",true);
			$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		}else if(this.value == "CLIENT"){			
			$('#clientContainer').show();
			$('#providerContainer').hide();
			
			$("#selProvider option[value='']").attr("selected", true);
		}else{
			$('#clientContainer').hide();
			$('#providerContainer').hide();
			
			$("#selProvider option[value='']").attr("selected", true);
			$("#selZone option[value='']").attr("selected",true);
			$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		}
	});

	//****************************** BIND SECTION - PROVIDER
	$('#selProvider').bind("change", function(){
		$('#data_container').html("");
	});

	//****************************** BIND SECTION - ZONE
	$('#selZone').bind("change",function(){
		$('#data_container').html("");
		loadCountries(this.value);
		
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	});

	//****************************** BIND SECTION - REGISTER BUTTON
	$('#btnRegistrar').bind("click",function(){		
		if($("#frmAssistancePayment").valid()){
			validateData();
		}
	});
});	

function validateData()
{
	show_ajax_loader();
	
	$('#data_container').load(document_root+"rpc/reports/assistance/loadAssistanceClaims.rpc",{
		document_to_dbf: $('#selPayTo').val(),
		date_from: $('#txtBeginDate').val(),
		date_end: $('#txtEndDate').val(),
		serial_cou: $('#selCountry').val(),
		serial_cit: $('#selCity').val(),
		serial_spv: $('#selProvider').val(),
                processAs: $('#selProcessAs').val()
	}, function(){
		if($('#claims_table').length>0) {
			currentTable = $('#claims_table').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"oLanguage": {
					"oPaginate": {
						"sFirst": "Primera",
						"sLast": "&Uacute;ltima",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
					"sZeroRecords": "No se encontraron resultados",
					"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
					"sSearch": "Filtro:",
					"sProcessing": "Filtrando.."
				},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
					"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
					"aButtons": []
				}

			} );
			
			if($("#selAll")){
				$("#selAll").bind("click", function(){
					/*var checked = $(this).is(':checked');
					var registers = currentTable.fnGetDisplayNodes();
					var last_item;
					
					//************ CHECK/UNCHECK THE NODE
					$.each(registers, function(index, item){
						//***** GET A SPECIFIC NODE
						var node_data = String(currentTable.fnGetData( item ));
						last_item = item;
						
						//****** RETRIEVE ITS INFORMATION AS A STRING AND MANIPULATE IT TO GET THE ID
						var id_section = node_data.substring(node_data.indexOf('id="'), node_data.indexOf('" value="'));
						id_section = str_replace('id="', '', id_section);
						
						//****** AFECT THE NODE WITH THE CHECKED STATE
						$("#"+id_section).attr("checked", checked);
					});
					
					currentTable.fnDisplayRow( last_item );*/
					
					if($('#selAll').attr("checked")) {
						//OLD $("input[id^='chkClaims']", currentTable.fnGetNodes()).attr("checked", true);
						$("input[id^='chkClaims']", currentTable.fnGetFilteredNodes()).attr("checked", true);
					} else {
						$("input[id^='chkClaims']", currentTable.fnGetNodes()).attr("checked", false);
					}
					
					
				});
			}

			$('#btnGeneratePDF').bind('click', function(){		
				getSelected_SubmitReport('PDF');
			});

			$('#btnGenerateXLS').bind('click', function(){
				getSelected_SubmitReport('XLS');
			});
		}

		hide_ajax_loader();
	});
}

// Function loadCountries and on change load cities
function loadCountries(serial_zon) 
{
	$('#countryContainer').load(document_root+"rpc/assistance/loadCountriesWithClaims.rpc",{
		serial_zon: serial_zon, 
		document_to:$('#selPayTo').val()
		},function(){
		$('#selCountry').bind("change",function(){
			$('#data_container').html("");
			$('#buttonContainer').hide();
			loadCities(this.value);
		});

		if($('#selCountry option').length==2){
			loadCities($('#selCountry').val());
		}
	});
}

// Function loadCities and on change load cities
function loadCities(serial_cou) 
{
	$('#cityContainer').load(document_root+"rpc/reports/assistance/loadCityWithClaims.rpc",{
		serial_cou: serial_cou, 
		document_to:$('#selPayTo').val()
		}, function() {
		$('#selCity').bind("change",function(){
			$('#data_container').html("");
			$('#buttonContainer').hide();
			
		});

	});
}

/*VALIDATE DATES RANGE*/
function validateDates(){
	var greater_than_today;
	
	if($("#txtBeginDate").val()!=''){
		greater_than_today = dayDiff($("#hdnToday").val(),$("#txtBeginDate").val());
		if(greater_than_today > 1){
			alert("La Fecha Inicio debe ser menor a la fecha actual");
			$("#txtBeginDate").val('');
		}
	}
	
	if($("#txtEndDate").val()!=''){
		greater_than_today = dayDiff($("#hdnToday").val(),$("#txtEndDate").val());
		if(greater_than_today > 1){
			alert("La Fecha Fin debe ser menor a la fecha actual");
			$("#txtEndDate").val('');
		}
	}


    if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		var days = dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val());
		if(days < 1){ //if departure date is smaller than arrival date
			alert("La Fecha Fin no puede ser menor a la Fecha Inicio");
			$("#txtEndDate").val('');
		}
	}
}

//VALIDATE AT LEAST ONE SELECTED
function getSelected_SubmitReport(type_file)
{
	var payto = $('#selPayTo').val();
	var selected_ids = new Array();
	
	//if(payto == "CLIENT" || payto == "DIRECT" ){
	$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
		array_push(selected_ids, $(this).val());
	});
	$('#selectedBoxes').val(implode(',', selected_ids));
	//}

	$("#frmAssistancePayment").attr("action", document_root+"modules/reports/assistance/pAssistancePayment"+type_file);
	$("#frmAssistancePayment").submit();
}
