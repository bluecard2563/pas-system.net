
$(document).ready(function(){
	$("#frmAssistanceAlertReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			from:{
				required:true
			},
			to:{
				required:true
			}
		},
		messages: {
	}
	});

	$('#btnGenerate_PDF').bind("click", function(){
		if($("#frmAssistanceAlertReport").valid()){
			$("#frmAssistanceAlertReport").attr("action", document_root+"modules/reports/assistance/pAssistanceAlertReportPDF.php");
			$("#frmAssistanceAlertReport").submit();
		}
	});

	$("#assistanceGroup").rules("add",{
		required:true
	});
	$("#alertOwner").rules("remove");

	$("#radioGroupAssistance").bind("click",function(){
		$("#assistanceGroupDiv").show();
		$("#alertOwnerDiv").hide();

		$("#assistanceGroup").rules("add",{
			required:true
		});
		$("#alertOwner").rules("remove");
		$("#alertOwner").val("");

	});

	$("#radioOwnerAlert").bind("click",function(){
		$("#assistanceGroupDiv").hide();
		$("#alertOwnerDiv").show();

		$("#alertOwner").rules("add",{
			required:true
		});
		$("#assistanceGroup").rules("remove");
		$("#assistanceGroup").val("");
	});

	var dates = $( "#from, #to" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
        showOn: 'button',
        buttonImage: document_root+'img/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
		
		onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" ),
			date = $.datepicker.parseDate(
				instance.settings.dateFormat ||
				$.datepicker._defaults.dateFormat,
				selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});



});

