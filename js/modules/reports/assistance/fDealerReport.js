// JavaScript Document

$(document).ready(function(){
	$("#frmDealerReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selManager: {
				required: true
			},
			selComissionist: {
				required: true
			}
			,
			selDealer: {
				required: true
			},
			selBranch: {
				required: true
			}
		}
	});

	$("#selCountry").bind("change", function(){
		loadCities(this.value);
		loadManagersByCountry(this.value);
	});

	function loadCities(countryID){
		$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},
			function(){
				$("#selCity").bind("change",function(e){
					if($('#selComissionist').val() != ''){
						loadDealer(this.value,$('#selComissionist').val());
					}
				});
			});
	}


	function loadManagersByCountry(countryID){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},
			function(){
				$('#selManager').bind('change',function(){loadUsersByManager(this.value);});
				if($("#selManager option").size() <=2 ){
					loadUsersByManager($("#selManager").val());
					$("#hdnSerialMbc").val($("#selManager").val());
				}
			});
	}

	function loadUsersByManager(mbcID){
        $('#comissionistContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: mbcID},
			function(){
				$("#selComissionist").bind("change",function(e){
					if($('#selCity').val() != ''){
						loadDealer($('#selCity').val(),this.value);
					}
				});
			});
	}

	function loadDealer(cityID, userID){
		$('#dealerContainer').load(document_root+"rpc/reports/assistance/loadDealerByCityByResponsible.rpc",{serial_cit: cityID, serial_usr:userID, serial_mbc:$('#selManager').val()},function(){
			$('#selDealer').bind('change', function(){loadBranch(cityID, userID, this.value);});
			if($("#selDealer option").size() <=2 ){
				loadBranch(cityID, userID, $("#selDealer").val());
			}
		});
	}

	function loadBranch(cityID, userID, dealerID){
		$('#branchContainer').load(document_root+"rpc/reports/assistance/loadBranchByDealerByCityByResponsible.rpc",{serial_cit: cityID, serial_usr:userID, serial_mbc:$('#selManager').val(), serial_dea:dealerID});
	}
});