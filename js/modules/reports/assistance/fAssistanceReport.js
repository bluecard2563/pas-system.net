// JavaScript Document
var pager;
$(document).ready(function(){
	$("#frmAssistanceReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			},
			selCity:{
				required: true
			},
			selDealer:{
				required: true
			},
			selBranch:{
				required: true
			}
		},
		messages: {
		}
	});
    $('#selZone').bind("change",function(){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
				loadCities(this.value);
            });
			if($("#selCountry").find('option').size()==2){
				loadCities($('#selCountry').val());
			}
        });
    });

	if($('#hdnUser').length>0 && $('#hdnUser').val()!=''){
		$("#selZone").rules("remove");
		$("#selCountry").rules("remove");
		$("#selCity").rules("remove");
		$("#selDealer").rules("remove");
		$("#selBranch").rules("remove");
		loadAssistances($('#hdnUser').val());
	}

});

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmAssistanceReport').valid()){
		$('#frmAssistanceReport').attr('action',document_root+'modules/reports/assistance/pAssistanceReport'+exportTo);
		$('#frmAssistanceReport').submit();
	}
}

/*
 * loadCities
 * @description: Loads all cities from the selected Country
 * @params: countryID-> Country's serial
 **/
function loadCities(countryID){
 $('#cityContainer').load(
	document_root+"rpc/loadCities.rpc",
	{serial_cou: countryID},
	function(){
              $('#selCity').bind('change', function(){
			   loadDealer($(this).val());
            });
			if($("#selCity").find('option').size()==2){
				loadDealer($('#selCity').val());
			}
		});

}

/*
 *@Name: loadDealer
 *@Description: Charges all dealers
 **/
function loadDealer(cityID){
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	if(cityID==""){
		$("#selDealer option[value='']").attr("selected",true);
		$("#selBranch option[value='']").attr("selected",true);
		cityID='stop';
	}
	$('#dealerContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{//serial_cou: countryID,
		serial_cit: cityID,
		dea_serial_dea: 'NULL', phone_sales: 'ALL'},
		function(){
			$("#selDealer").bind("change", function(e){loadBranch(this.value);});
			if($("#selDealer").find('option').size()==2){
				loadBranch($('#selDealer').val());
			}
		});
}

/*
 *@Name: loadBranch
 *@Description: Loads branches
 **/
function loadBranch(dealerID){
	if(dealerID==""){
		$("#selBranch option[value='']").attr("selected",true);
		dealerID='stop';
	}

	$('#branchContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{dea_serial_dea: dealerID, phone_sales: 'ALL'},
		function(){
			$("#selBranch").bind("change", function(e){loadAssistances(this.value);});
			if($('#selBranch').find('option').size()==2){
				loadAssistances($('#selBranch').val());
			}
	});

}

/*
 *@Name: loadAssistances
 *@Description: Calls a RPC file which loads all assistences of the selected branch
 **/
function loadAssistances(dealerID){
$('#assistancesContainer').load(
	document_root+"rpc/reports/assistance/loadAssistancesbyDealer.rpc",
	{serial_dea: dealerID},
	function(){

		if($('#files_table').length>0){
			//alert('entro');
					pager = new Pager('files_table', 10 , 'Siguiente', 'Anterior');
					//pager.init(10); //Max Pages
					pager.init($('#hdnPages').val()); //Max Pages
					pager.showPageNav('pager', 'pageNavPositionFiles'); //Div where the navigation buttons will be placed.
					pager.showPage(1); //Starting page
		}
		//PDF BUTTON
		$("[name^='imgSubmitPDF_']").each(function (){
			$(this).bind('click',function(){
				sendInfo('PDF/'+$(this).attr('fileID'));
			});
		});
	});
}