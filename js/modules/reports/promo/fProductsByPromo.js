//Java Script
$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	$("#frmReportProductsByPromo").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
				selApliedTo: {
					required: true
				},
				selZone: {
					required: {
						depends: function(element) {
								var appliedTo=$('#selApliedTo').val();
								if(appliedTo=='COUNTRY' || appliedTo=='CITY' || appliedTo=='MANAGER' || appliedTo=='MANAGER')
									return true;
								else
									return false;
						}
					}
				},
				selCountry: {
					required: {
						depends: function(element) {
								var appliedTo=$('#selApliedTo').val();

								if(appliedTo=='COUNTRY')
									return true;
								else
									return false;
						}
					}
				},
				selCity: {
					required: {
						depends: function(element) {
								var appliedTo=$('#selApliedTo').val();

								if(appliedTo=='CITY')
									return true;
								else
									return false;
						}
					}
				},
				selManager: {
					required: {
						depends: function(element) {
								 var appliedTo=$('#selApliedTo').val();

								if(appliedTo=='MANAGER')
									return true;
								else
									return false;
						}
					}
				},
				selDealer: {
					required: {
						depends: function(element) {
								var appliedTo=$('#selApliedTo').val();

								if(appliedTo=='DEALER')
									return true;
								else
									return false;
						}
					}
				},
				selPromo:{
					required: true
				}
		},
		messages: {

   		}
	});
	/*END VALIDATION*/

        /*Binding and Style Functions*/
        $('#selApliedTo').bind('change',function(){
		   $('#promoContainer').html('');
           showApliedInfo(this.value);
        });
        /*End Bind and Style Function*/
});

/*
 * @Name: showApliedInfo
 * @Params: N/A
 * @Description: Displays the section where we select
 *               the country/city etc. to apply the promo.
 */
function showApliedInfo(value){
    $('#appliedToContainer').load(document_root+"rpc/loadsPromo/loadPromoInfoSearch.rpc",{appliedTo: value},function(){
            switch($('#selApliedTo').val()){
                case 'MANAGER':
                    serial='#selManager';
                   break;
                case 'COUNTRY':
                    serial='#selCountry';
                   break;
                case 'CITY':
                    serial='#selCity';
                    break;
                case 'DEALER':
                    serial='#selDealer';
                   break;
				default: serial='';

            }
			if(serial==''){
				$('#appliedToContainer').html('');
				$('#buttonsContainer').hide();
			}
     });
	 
}

/*
 * @Name: loadCountry
 * @Params: zoneID
 * @Description: Load all the countries for a specific zone.
 */
function loadCountry(zoneID){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneID, charge_country:0},function(){
            $('#selCountry').change(loadCity);
            $('#selCountry').bind('change',function(){
               loadManagersByCountry(this.value);
			   if($('#selApliedTo').val()=='COUNTRY'){
				   loadOptions();
			   }
            });
        });
}

/*
 * @Name: loadCity
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCity(){
    var appliedTo=$('#selApliedTo').val();
    if(appliedTo=="CITY" || appliedTo=="DEALER"){
        var countryID=this.value;

	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},function(){
            $('#selCity').bind('change',function(){
				if($('#selApliedTo').val()=='CITY'){
				   loadOptions();
			   }
			   if($('#selApliedTo').val()=='DEALER'){
				   loadDealers($('#selCity').val());
			   }
            });
        });
    }
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
    var appliedTo=$('#selApliedTo').val();

    if(appliedTo=='MANAGER'){
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
			$('#selManager').bind('change',function(){
			   if($('#selApliedTo').val()=='MANAGER'){
				   loadOptions();
			   }
            });
		});
    }
}

/*
 *@Name: loadDealer
 *@Description: Charges all dealers
 **/
function loadDealers(cityID){
	if(cityID==""){
		$("#selDealer option[value='']").attr("selected",true);
		cityID='stop';
	}

	$('#dealerContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{//serial_cou: countryID,
		serial_cit: cityID,
		dea_serial_dea: 'NULL'},
		function(){
			$("#selDealer").bind("change", function(e){
			  if($('#selApliedTo').val()=='DEALER'){
				loadOptions();
			  }
			});
		}
	);
}
/*
 *@Name: cleanHistory
 *@Description: Function Empty because in the other place where the RPC file is used, we need to call this function but here we don't
 **/
function cleanHistory(){}

/*
 *@Name: loadOptions
 *@Description: show the div with the options to generate the report
 **/
function loadOptions(){

	$('#buttonsContainer').show();
	$('#imgSubmitPDF').unbind('click');
	$('#imgSubmitXLS').unbind('click');
	$('#imgSubmitPDF').bind('click',function(){
		sendInfo('PDF');
	});
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('Excel');
	});
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmReportProductsByPromo').valid()){
		$('#frmReportProductsByPromo').attr('action',document_root+'modules/reports/promo/pProductsByPromo'+exportTo);
		$('#frmReportProductsByPromo').submit();
	}
}