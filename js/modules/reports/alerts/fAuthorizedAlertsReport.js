// JavaScript Document
$(document).ready(function(){
	$("#frmAlertsReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,

            rules: {
                selZone: {
                    required: true
                },
				selCountry: {
                    required: true
                },
				selManager: {
                    required: true
                },
				txtBeginDate: {
                    required: true,
					date: true
                },
				txtEndDate: {
                    required: true,
					date: true
                },
				selType: {
                    required: true
                }
            },
            messages: {
                txtBeginDate: {
					date: 'El campo "Fecha desde" debe tener formato dd/mm/yyyy.'
                },
				txtEndDate: {
					date: 'El campo "Fecha hasta" debe tener formato dd/mm/yyyy.'
                }
            }
    });

	$('#selZone').bind("change",function(){
		$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
			$('#selCountry').bind("change",function(){
				loadManagers(this.value);
			});
			if($("#selCountry").find('option').size()==2){
				loadManagers($('#selCountry').val());
			}
		});
	});

	/*CALENDAR*/
	setDefaultCalendar($("#txtBeginDate"),'-5y','+0d','-0');
	setDefaultCalendar($("#txtEndDate"),'-5y','+0d','-0');
    /*END CALENDAR*/

	$("#btnGeneratePDF").bind('click', function(){
		if($('#frmAlertsReport').valid()){
			$('#frmAlertsReport').submit();
		}
	});
});

function loadManagers(serial_cou) {
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou});
}