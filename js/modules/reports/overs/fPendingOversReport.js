// JavaScript Document
$(document).ready(function(){
	$("#frmOversReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			},
			selApliedTo:{
				required: true
			},
			selManager:{
				required: true
			},
		}
	});
	
    $('#selZone').bind("change",function(){
    	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
			$('#selCountry').attr('title','El campo "Pa&iacute;s" es obligatorio.');
    		$('#selCountry').bind('change',function(){
    			loadManagersByCountry($('#selCountry').val());
    			loadManagersByCountry(this.value);
    		});

			if($('#selCountry option').size()<=2){
				loadManagersByCountry($('#selCountry').val());
    			loadManagersByCountry($('#selCountry').val());
			}
        });
    });
    
    $('#selResp').bind("change",function(){
    	loadDealersByManagerAndRepresentante();
    });
    
    $('#selManager').bind("change",function(){
    	loadResponsablesByManager($(this).val());
    	loadDealersByManagerAndRepresentante();
    });
    
	$('#selApliedTo').bind('change',function(){
		switchAppliedToValues($('#selApliedTo').val());
    });
	
	/*CALENDARS*/
    setDefaultCalendar($("#txtStartDate"),'-100y','+0d');
    setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
    
    $('#selDealerDea').bind("change",function(){
    	loadDealersByManagerAndRepresentante();
    });
    
    $('#searchButton').bind("click",function(){
    	generateOversReport();
    });
});

function switchAppliedToValues(value){
	$('#managerContainerTop').css('display','none');
	$('#dealerContainerTop').css('display','none');
	$('#selManager').val('');
	$('#selDealer').val('');
	switch(value){
		case 'MANAGER':
			$('#managerContainerTop').css('display','block');
			break;
		case 'DEALER':
			$('#managerContainerTop').css('display','block');
			$('#dealerContainerTop').css('display','block');
			break;
	}
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
		$('#selManager').attr('title','El campo "Representante" es obligatorio.');
		$('#selManager').bind("change",function(){
	    	loadResponsablesByManager($(this).val());
	    	loadDealersByManagerAndRepresentante();
	    });
	});
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadDealersByManagerAndRepresentante(){
	if($('#selManager').val() != '' && $('#selResp').val() != ''){
		$('#dealerContainer').load(document_root+"rpc/loadDealersByManagerAndRepresentante.rpc",{serial_mbc: $('#selManager').val(), serial_usr: $('#selResp').val() },function(){
			$('#selDealerDea').attr('title','El campo "Comercializador" es obligatorio.');
			$('#selDealerDea').bind("change",function(){
				$('#branchContainer').load(document_root+"rpc/loadBranchesByDealer.rpc",{dea_serial_dea: $('#selDealerDea').val(), reports: true});
		    });
			$('#branchContainer').load(document_root+"rpc/loadBranchesByDealer.rpc",{dea_serial_dea: $('#selDealerDea').val(), reports: true});
		});
	}
}

function generateOversReport(){
	if($('#frmOversReport').valid()){
		$('#reportResults').load(document_root+"rpc/reports/overs/loadPendingOversReportResult.rpc",{
			serialZone: $('#selZone').val(),
			serialCountry: $('#selCountry').val(),
			selAppTo: $('#selApliedTo').val(),
			serialMan: $('#selManager').val(),
			serialResp: $('#selResp').val(),
			serialDealer: $('#selDealerDea').val(),
			serialBranch: $('#selDealer').val(),
		});
		return false;
    }
}

/*
 * loadResponsablesByCity
 * @description: Call to the RPC which loads the responsables based in the selected city
 * @params: cityID-> City's serial
 **/
function loadResponsablesByManager(managerID){
	$('#responsableContainer').load(document_root+"rpc/loadResponsables.rpc",{serial_mbc: managerID, opt_text: 'Todos'}, function(e){
		$('#selResp').bind("change",function(){
	    	loadDealersByManagerAndRepresentante();
	    });
	});
}

function sendInfo(exportTo,apoSerial, serialOve){
    $('#frmOversReport').attr('action',document_root+'modules/reports/overs/pendingOversReport'+exportTo+'/'+apoSerial+'/'+serialOve);
    $('#frmOversReport').submit();
}