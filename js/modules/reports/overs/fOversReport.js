// JavaScript Document
$(document).ready(function(){
	$("#frmOversReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			},
			selApliedTo:{
				required: true
			},
			selManager:{
				required: {
	                depends: function(element) {
	                        var appliedTo=$('#selApliedTo').val();
	                        return appliedTo=='MANAGER';
	                	}
	            }
			},
			selDealer:{
				required: {
	                depends: function(element) {
	                        var appliedTo=$('#selApliedTo').val();
	                        return appliedTo=='DEALER';
	                	}
	            }
			}
		},
		submitHandler: function(form){
			generateOversReport(form);
			return false;
		}

	});
	
    $('#selZone').bind("change",function(){
    	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
			$('#selCountry').attr('title','El campo "Pa&iacute;s" es obligatorio.');
    		$('#selCountry').bind('change',function(){
    			loadManagersByCountry($('#selCountry').val());
    			loadDealersByCountry($('#selCountry').val());
    		});

			if($('#selCountry option').size()<=2){
				loadManagersByCountry($('#selCountry').val());
    			loadDealersByCountry($('#selCountry').val());
			}
        });
    });
	$('#imgSubmitPDF').bind('click',function(){
		sendInfo('PDF');
	});
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('Excel');
	});

	$('#selApliedTo').bind('change',function(){
		switchAppliedToValues($('#selApliedTo').val());
    });
	
});

function switchAppliedToValues(value){
	$('#managerContainerTop').css('display','none');
	$('#dealerContainerTop').css('display','none');
	$('#selManager').val('');
	$('#selDealer').val('');
	switch(value){
		case 'MANAGER':
			$('#managerContainerTop').css('display','block');
			break;
		case 'DEALER':
			$('#dealerContainerTop').css('display','block');
			break;
	}
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
		$('#selManager').attr('title','El campo "Representante" es obligatorio.');
	});
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadDealersByCountry(countryID){
    $('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",{serial_cou: countryID, dea_serial_dea: 'NULL'},function(){
		$('#selDealer').attr('title','El campo "Comercializador" es obligatorio.');
	});
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmOversReport').valid()){
		$('#frmOversReport').attr('action',document_root+'modules/reports/overs/pOversReport'+exportTo);
		$('#frmOversReport').submit();
	}
}

function generateOversReport(form){
	$('#reportResults').load(document_root+"rpc/reports/overs/loadOversReportResult.rpc",{
		serialZone: form.selZone.value,
		serialCountry: form.selCountry.value,
		serialMan: $('#selManager').val(),
		serialDea: $('#selDealer').val()});
	return false;
}