// JavaScript Document
$(document).ready(function(){
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
    });

	$('#imgSubmit').bind('click',function(){
		sendInfo();
	});

	$("#frmReportProductByDealer").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			}
		}
	});
});

/*
 * loadCountry
 * @param: serial_zon - Zone ID
 * @return: A list of countries in a specific zone.
 **/
function loadCountry(serial_zon){
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
        $('#selManager option[value=""]').attr("selected", true);
        $('#selCountry').bind("change", function(){
            loadManager(this.value);
        });

		if($('#selCountry option').size()<=2){
			loadManager($('#selCountry').val());
		}
    });
}

/*
 * loadManager
 * @param: serial_cou - Country ID
 * @return: A list of managers located in a specific country
 **/
function loadManager(serial_cou){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'}, function(){
		$('#selManager').bind("change", function(){
            loadDealer(this.value);
        });

		if($('#selManager option').size()<=2){
			loadDealer($('#selManager').val());
		}
	});
}

/*
 * loadManager
 * @param: serial_cou - Country ID
 * @return: A list of managers located in a specific country
 **/
function loadDealer(serial_mbc){
    $('#dealerContainer').load(document_root+"rpc/reports/productByDealer/loadDealersWithProducts.rpc",
		{
			serial_mbc: serial_mbc,
			opt_text: 'Todos'
		});
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(){
    if($('#frmReportProductByDealer').valid()){
		$('#frmReportProductByDealer').submit();
	}
}