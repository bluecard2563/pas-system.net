// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmCreditNoteReport").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCreditNote: {
                required: true
            },
            selZone:{
                required: {
                    depends: function() {
                        return $('#selCountryContainer').css("display") == "block";
                    }
                }
            },
            selCountry: {
                required: {
                    depends: function() {
                        return $('#selCountryContainer').css("display") == "block";
                    }
                }
            },
            selManager: {
                required: false
//                        {
//                    depends: function() {
//                        return $('#selManagerContainer').css("display") == "block";
//                    }
//                }
            },
            selDocument: {
                required: {
                    depends: function() {
                        return $('#selDocumentToContainer').css("display") == "block";
                    }
                }
            },
            hdnClientID: {
                required: {
                    depends: function() {
                        return $('#customerContainer').css("display") == "block";
                    }
                }
            },
            txtNumInvoice: {
                required: {
                    depends: function() {
                        return $('#invoiceContainer').css("display") == "block";
                    }
                }
            },
            txtNumCreditNote: {
                required: {
                    depends: function() {
                        return $('#creditNoteContainer').css("display") == "block";
                    }
                }
            }
        }
    });
    /*END VALIDATION SECTION*/

    /*CUSTOMER AUTO COMPLETER*/
    var ruta=document_root+"rpc/autocompleters/loadCustomers.rpc";
    $("#txtClient").autocomplete(ruta,{
        max: 10,
        scroll: false,
        matchContains:true,
        minChars: 2,
        formatItem: function(row) {
            if(row[0]==0){
                return 'No existen resultados';
            }else{
                return row[0] ;
            }
        }
    }).result(function(event, row) {
        if(row[1]==0){
            $("#hdnClientID").val('');
            $("#txtClient").val('');
        }else{
            $("#txtClient").val(row[0]);
            $("#hdnClientID").val(row[1]);
        }
    });
    /*END CUSTOMER AUTO COMPLETER*/

    $('#selCreditNote').bind("change", function(){
        /*RESET DATA*/
        $('#selCountryContainer').css("display", "none");
        $('#selManagerContainer').css("display", "none");
        $('#selCityContainer').css("display", "none");
        $('#extraParamsContainer').css("display", "none");
        $('#selDocumentToContainer').css("display", "none");
        $('#customerContainer').css("display", "none");
        $('#invoiceContainer').css("display", "none");
        $('#creditNoteContainer').css("display", "none");
        $("#selZone option[value='']").attr("selected",true);
        loadDocumentsByManager('');
        $("#selType option[value='']").attr("selected",true);
        $('#txtClient').val('');
        $('#hdnClientID').val('');
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc");
        $('#cityContainer').load(document_root+"rpc/loadCities.rpc");
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc");
        $('#dealerContainer').load(document_root+"rpc/reports/credit_note/loadDealersByCity.rpc");
        /*END RESET DATA*/

        if(this.value == "CUSTOMER") {
            $('#customerContainer').css("display", "block");
        } else if(this.value == "INVOICE") {
            $('#selCountryContainer').css("display", "block");
            $('#selManagerContainer').css("display", "block");
            $('#invoiceContainer').css("display", "block");
            $('#selDocumentToContainer').css("display", "block");
        } else if(this.value == "CREDIT_NOTE") {
            $('#selCountryContainer').css("display", "block");
            $('#selManagerContainer').css("display", "block");
            $('#creditNoteContainer').css("display", "block");
            $('#selDocumentToContainer').css("display", "block");
        } else if(this.value == "GENERAL") {
            $('#selCountryContainer').css("display", "block");
            $('#selManagerContainer').css("display", "block");
            $('#selCityContainer').css("display", "block");
            $('#extraParamsContainer').css("display", "block");
        }
    });

    /*CALENDARS*/
    setDefaultCalendar($("#txtStartDate"),'-100y','+0d');
    setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
    /*END CALENDARS*/

    $('#selZone').bind("change",function(){
        loadCountries(this.value);
        //Reset Data
        $('#cityContainer').load(document_root+"rpc/loadCities.rpc");
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc");
        $('#dealerContainer').load(document_root+"rpc/reports/credit_note/loadDealersByCity.rpc");
    });

    /*BUTTONS*/
    $('#imgSubmitPDF').bind('click',function(){
            sendInfo('PDF');
    });
    $('#imgSubmitXLS').bind('click',function(){
            sendInfo('Excel');
    });
});

function sendInfo(exportTo){
    if($('#frmCreditNoteReport').valid()){
        $('#frmCreditNoteReport').attr('action',document_root+'modules/reports/credit_note/pCreditNoteReport'+exportTo);
        $('#frmCreditNoteReport').submit();
    }
}

function loadCountries(serial_zon) {
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon},function(){
        $('#selCountry').bind("change",function(){
            loadCities(this.value);
            loadManagersByCountry(this.value);
        });
        if($("#selCountry option").size() <=2 ){
            loadCities($('#selCountry').val());
            loadManagersByCountry($('#selCountry').val());
        }
    });
}

function loadCities(serial_cou) {
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: serial_cou}, function() {
        $('#selCity').bind("change",function(){
            loadDealersByCity(this.value);
        });
        if($("#selCity option").size() <=2 ){
            loadDealersByCity(this.value);
        }
    });
}

function loadManagersByCountry(countryID){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc", {serial_cou: countryID}, function(e){
    	$('#selManager').change(function(e){
    		loadDocumentsByManager($('#selManager').val());
    	});
    });
}

function loadDealersByCity(cityID){
    $('#dealerContainer').load(document_root+"rpc/reports/credit_note/loadDealersByCity.rpc", {serial_cit: cityID});
}

function loadDocumentsByManager(managerID){
    $('#documentToContainer').load(document_root+"rpc/loadDocumentsByManager.rpc",{serial_mbc: managerID});
   }