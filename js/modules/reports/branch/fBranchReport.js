// JavaScript Document
var currentTable;

$(document).ready(function(){
    $('#selZone').bind("change",function(){
		clearRegisters();
        $('#branchContainer').html("");
        loadCountry(this.value);
    });

    $('#btnGenerate').bind("click", function(){
        if(validate()){
			show_ajax_loader();
            $('#branchContainer').load(document_root+"rpc/reports/loadBranch.rpc",
            {
                serial_cou: $('#selCountry').val(),
                serial_mbc: $('#selManager').val(),
                serial_dea: $('#selDealer').val(),
				serial_usr: $('#selResponsible').val(),
				status_dea: $('#selStatus').val()
            },
            function(){
                if($("#branch_table").length>0) {
					currentTable = $('#branch_table').dataTable( {
										"bJQueryUI": true,
										"sPaginationType": "full_numbers",
										"oLanguage": {
											"oPaginate": {
												"sFirst": "Primera",
												"sLast": "&Uacute;ltima",
												"sNext": "Siguiente",
												"sPrevious": "Anterior"
											},
											"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
											"sZeroRecords": "No se encontraron resultados",
											"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
											"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
											"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
											"sSearch": "Filtro:",
											"sProcessing": "Filtrando.."
										},
									"sDom": 'T<"clear">lfrtip',
									"oTableTools": {
										"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
										"aButtons": []
									}

					} );
                }
				
				hide_ajax_loader();
            });
        }
    });

	$('#btnGeneratePDF').bind("click", function(){
        if(validate()){
            $("#frmBranchReport").attr("action", document_root+"modules/reports/branch/pBranchPDFReport.php");
            $("#frmBranchReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if(validate()){
            $("#frmBranchReport").attr("action", document_root+"modules/reports/branch/pBranchXLSReport.php");
            $("#frmBranchReport").submit();
        }
    });
});

function loadCountry(zoneID){
	$('#selManager').html('<option value ="">- Todos -</option>');
	$('#selDealer').html('<option value ="">- Todos -</option>');
	$('#selResponsible').html('<option value ="">- Todos -</option>');
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneID, charge_country:0},function(){
		if($("#selCountry option").size() ==2 ){
			loadManager($("#selCountry").val());
		}
		$('#selCountry').bind("change",function(){
			clearRegisters();
			$('#branchContainer').html("");
			loadManager(this.value);
		});
	});
}

function loadManager(countryID){
	$('#selManager').html('<option value ="">- Todos -</option>');
	$('#selDealer').html('<option value ="">- Todos -</option>');
	$('#selResponsible').html('<option value ="">- Todos -</option>');
	if(countryID!=''){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID, notRequired: true},function(){
			if($("#selManager option").size() ==2 ){
				loadDealer($("#selManager").val());
				loadResponsibles($("#selManager").val());
			}
			$('#selManager').bind("change",function(){
				clearRegisters();
				$('#branchContainer').html('');
				loadDealer(this.value);
				loadResponsibles(this.value);
			});
		});
	}
}
function loadDealer(serial_mbc){
	$('#selDealer').html('<option value ="">- Seleccione -</option>');
	if(serial_mbc!=''){
		$('#dealerContainer').load(document_root+"rpc/reports/loadDealerByManager.rpc",{serial_mbc: serial_mbc, notRequired: true},function(){
			$('#selDealer').bind('change', function(){
				clearRegisters();
				$('#selResponsible option[value=""]').selected();
			});
		});
	}
}

function loadResponsibles(serial_mbc){
	$('#selResponsible').html('<option value ="">- Seleccione -</option>');
	if(serial_mbc!=''){
		$('#responsibleContainer').load(document_root+"rpc/reports/loadResponsiblesByDealer.rpc",{serial_mbc: serial_mbc},function(){
			$('#selResponsible').bind('change', function(){
				clearRegisters();
				$('#selDealer option[value=""]').selected();
			});
		});
	}
}

function validate(){
    var valid = true;
    $('#alerts').html("");
    $('#alerts').css("display", "none");
    if($('#selZone').val() == ""){
        $('#alerts').append("<li><b>"+$('#selZone').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    } else if($('#selCountry').val() == "") {
        $('#alerts').append("<li><b>"+$('#selCountry').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    }
    return valid;
}

function clearRegisters(){
	$('#branchContainer').html("");
}