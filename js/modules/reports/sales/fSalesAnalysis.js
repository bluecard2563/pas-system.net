// JavaScript Document
/*
File: fSalesAnalysis.js
Author: Gabriela Guerrero
Creation Date: 06/07/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
    $("#selZone").bind("change", function(){
        loadCountries($(this).val());
    });
	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');

	$("#frmSalesAnalysis").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                selZone: {
					required: true
                },
				selCountry: {
					required: true
                },
				txtDateFrom: {
					required: true
                },
				txtDateTo: {
					required: true
                }
        }
	});

	$('#btnGeneratePDF').bind('click',function(){
		if(validateDates() && $('#frmSalesAnalysis').valid()){
			checkSales('PDF');
		}
	});
	$('#btnGenerateXLS').bind('click',function(){
		if(validateDates() && $('#frmSalesAnalysis').valid()){
			checkSales('XLS');
		}
	});
});

/*FUNCTIONS*/
/*VALIDATE DATES RANGE*/
function validateDates(){
		var days=dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
		var date = new Date();
		var today = (date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
		if(days<0){
			alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
			return false;
		}
		if(days>365){
			alert("El rango de fechas no debe ser mayor a un a�o.");
			return false;
		}
		if( dayDiff($("#txtDateFrom").val(),today)<=0 || dayDiff($("#txtDateTo").val(),today)<=0 ){
			alert("La fecha ingresada no puede ser mayor a la fecha actual.");
			return false;
		}
		return true;
}
/*CALL TO THE RPC TO LOAD THE COUNTRIES
 *BASED ON THE ZONE SELECTED*/
function loadCountries(zoneId){
    if(zoneId){//IF A ZONE HAS BEEN SELECTED
			$('#countryContainer').load(document_root+"rpc/reports/sales/loadCountriesByZone.rpc", {serial_zon: zoneId, type: 'analysis'},function(){
			$("#selCountry").bind('change', function(){
                loadManagers($(this).val());
				loadCities($(this).val());
            });
			if($("#selCountry").find('option').size()==2){
				loadCities($('#selCountry').val());
				loadManagers($('#selCountry').val());
			}
			loadManagers('');
			loadCities('');
			});
    }else{
        $('#countryContainer').html('<select name="selCountry" id="selCountry"><option value="">- Seleccione -</option></select>');
		loadManagers('');
		loadCities('');
    }
}
/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagers(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
			$('#managerContainer').load(document_root+"rpc/reports/sales/loadManagersByCountry.rpc", {serial_cou: countryId, type: 'analysis'},function(){});
    }else{
        $('#managerContainer').html('<select name="selManager" id="selManager"><option value="">Todos</option></select>');
    }
}
/*CALL TO THE RPC TO LOAD THE CITIES
	 *BASED ON THE COUNTRY
 */
function loadCities(countryId){
    if(countryId){
        $('#cityContainer').load(document_root+"rpc/reports/sales/loadCitiesByCountry.rpc",{serial_cou: countryId, type: 'analysis'},function(){
            $("#selCity").bind('change', function(){
                loadResponsibles($(this).val());
            });
			loadResponsibles('');
        });
    }else{
       $('#cityContainer').html('<select name="selCity" id="selCity"><option value="">Todos</option></select>');
       loadResponsibles('');
    }
}
/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE MANAGER
 */
function loadResponsibles(cityId){
    if(cityId){
        $('#responsibleContainer').load(document_root+"rpc/reports/sales/loadResponsablesByCity.rpc",{serial_cit: cityId},function(){
            $("#selResponsible").bind('change', function(){
                loadDealers($(this).val());
            });
			loadDealers('');
        });
    }else{
       $('#responsibleContainer').html('<select name="selResponsible" id="selResponsible"><option value="">Todos</option></select>');
       loadDealers('');
    }
}
/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE RESPONSIBLE
 */
function loadDealers(responsibleId){
    if(responsibleId){
        $('#dealerContainer').load(document_root+"rpc/reports/sales/loadDealersByResponsible.rpc",{serial_usr: responsibleId},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
            });
			loadBranches('');
        });
    }else{
       $('#dealerContainer').html('<select name="selDealer" id="selDealer"><option value="">Todos</option></select>');
       loadBranches('');
    }
}

/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#branchContainer").load(document_root+"rpc/reports/sales/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID, type: 'analysis'});
    }else{
        $("#branchContainer").html('<select name="selBranch" id="selBranch"><option value="">Todos</option></select>');
    }
}

function checkSales(reportType){
	$('#emptyResult').html('');
	var zone = '', country = '', from = '', to = '', manager = '',  city = '', responsible = '', dealer = '', branch = '', type = '';

	zone = $('#selZone').val(); country = $('#selCountry').val(); from = $('#txtDateFrom').val(); to = $('#txtDateTo').val();

	if($('#selManager').val()){
		manager = $('#selManager').val();
	}
	if($('#selCity').val()){
		city = $('#selCity').val();
	}
	if($('#selResponsible').val()){
		responsible = $('#selResponsible').val();
	}
	if($('#selDealer').val()){
		dealer = $('#selDealer').val();
	}
	if($('#selBranch').val()){
		branch = $('#selBranch').val();
	}
	/*if($('#selType').val()){
		type = $('#selType').val();
	}*/
		$.ajax({
		   type: "POST",
		   url: document_root+"rpc/reports/sales/checkSalesAnalysis.rpc",
		   data: ({
					serial_zon : zone,
					serial_cou : country,
					dateFrom : from,
					dateTo : to,
					serial_mbc : manager,
					serial_cit : city,
					serial_usr : responsible,
					serial_dea : dealer,
					serial_bra : branch
					//type : type
		   }),
		   success: function(msg){
			 if(msg == 1){
				 $('#emptyResult').html('');
				 $("#frmSalesAnalysis").attr("action", document_root+"modules/reports/sales/pSalesAnalysis"+reportType+".php");
				 $("#frmSalesAnalysis").submit();
			 }else{
				 if(msg == 0){
					 $('#emptyResult').html('No existen resultados que coincidan con el criterio de b&uacute;squeda');
				 }
				 else{
					 $('#emptyResult').html('No existen ventas anteriores.');
				 }
			 }
		   }
		 });
}