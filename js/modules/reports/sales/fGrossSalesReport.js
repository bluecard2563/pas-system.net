// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmGrossSalesReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                selMonth: {
                    required: true
                },
                selYear: {
                    required: true
                },
                txtAmount: {
                    number: true
                }
            },
            messages:{
                txtAmount: {
                    number: 'El campo "Monto" solo admite valores num\u00E9ricos.'
                }
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
	});

    $('#btnGeneratePDF').bind("click", function(){
        if($("#frmGrossSalesReport").valid()){
            $("#frmGrossSalesReport").attr("action", document_root+"modules/reports/sales/pGrossSalesReportPDF.php");
            $("#frmGrossSalesReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmGrossSalesReport").valid()){
            $("#frmGrossSalesReport").attr("action", document_root+"modules/reports/sales/pGrossSalesReportXLS.php");
            $("#frmGrossSalesReport").submit();
        }
    });
});

function loadCountry(serial_zon){
	/* REMOVE SELECTED OPTIONS */
	$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selCity').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#selProduct').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	/* REMOVE SELECTED OPTIONS */

	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
		$('#selCountry').bind("change",function(){
			/* REMOVE SELECTED OPTIONS */
			$('#selCity').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			$('#selProduct').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			/* REMOVE SELECTED OPTIONS */

			loadCity(this.value);
			loadManagersByCountry(this.value);
			loadProductByCountry(this.value);
		});

		if($('#selCountry option').size()<=2){
			loadCity($('#selCountry').val());
			loadManagersByCountry($('#selCountry').val());
			loadProductByCountry($('#selCountry').val());
		}
	});
}
function loadCity(serial_cou){	
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: serial_cou, charge_country:0, opt_text: 'Todos'},function(){
		if($("#selCity option").size() <=2 ){
			loadResponsibleByCity(this.value);
			loadDealers();
		}
		$('#selCity').bind("change", function(){
			loadResponsibleByCity(this.value);
			loadDealers();
			$('#selResponsible').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
		});
	});
}
function loadManagersByCountry(serial_cou){
	if(serial_cou){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
			$('#selManager').bind('change', function(){
            	loadComissionists($(this).val());
				loadDealers();
            });
		});
	}else{
    	loadComissionists('');
    }
}

/* LOAD COMISSIONISTS FROM MANAGER
 */
function loadComissionists(managerId){
    if(managerId){
        $('#comissionistsContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: managerId},function(){
            $("#selComissionist").bind('change', function(){
            	loadDealers();
            });
        });
    }else{
    	$('#comissionistsContainer').html('Seleccione un representante');
    	loadDealers();
    }
}


/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val() && $('#selComissionist').val()){
        $('#comercializadorContainer').load(document_root+"rpc/loadDealersByManagerAndComissionistAndCity.rpc",{
        	city: $("#selCity").val(),
        	serial_mbc: $('#selManager').val(), 
        	serialComissionist: $('#selComissionist').val()},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				loadBranches('');
	        });
    }else{
    	$('#comercializadorContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}

function loadProductByCountry(serial_cou){
	if(serial_cou!=''){
		$('#productContainer').load(document_root+"rpc/reports/sales/loadProductsByCountry.rpc",{serial_cou: serial_cou, reports: 'true', multiple: 'true'});
	}
}

function loadResponsibleByCity(serial_cit){
	if(serial_cit!=''){
		$('#responsibleContainer').load(document_root+"rpc/reports/sales/loadResponsibleByCity.rpc",{serial_cit: serial_cit, reports: 'true'});
	}
}