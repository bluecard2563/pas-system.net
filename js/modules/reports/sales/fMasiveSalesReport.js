// JavaScript Document
$(document).ready(function(){
    /*AUTOCOMPLETER*/
	$("#txtCustomer").autocomplete(document_root+"rpc/autocompleters/loadCustomers.rpc",{
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnSerial_cus").val(row[1]);

	});
	/*END AUTOCOMPLETER*/


	/*EVENTS*/
	$('#btnSearch').bind('click',function(){
		$('#parentCardsContainer').html('');
		$('#childrenCardsContainer').html('');
		if($("#hdnSerial_cus").val()) {
			$('#parentCardsContainer').load(document_root+"rpc/reports/sales/loadMasiveSalesByCustomer.rpc",{serial_cus:$("#hdnSerial_cus").val()},function(){
				$("input[id^='rdoCard']:first").attr("checked", true);
				$('#btnChildrenCards').bind('click',function(){
					$('#childrenCardsContainer').load(document_root+"rpc/reports/sales/loadMasiveSalesByCustomer.rpc",{serial_cus:$("#hdnSerial_cus").val(),serial_sal:$("input[id^='rdoCardParent']:checked").val()},function(){
						$("input[id^='rdoCardChild']:first").attr("checked", true);

						$('#btnGeneratePDF').bind("click", function(){
							if($("#frmCardReport").valid()){
								$("#frmCardReport").attr("action", document_root+"modules/reports/sales/pMasiveSalesReportPDF.php");
								$("#frmCardReport").submit();
							}
						});

						$('#btnGenerateXLS').bind("click", function(){
							if($("#frmCardReport").valid()){
								$("#frmCardReport").attr("action", document_root+"modules/reports/sales/pMasiveSalesReportXLS.php");
								$("#frmCardReport").submit();
							}
						});
						
					});
				});
			});
		} else {
			alert("Seleccione un cliente para realizar la b\u00FAsqueda.")
		}
	});
	/*END EVENTS*/
});