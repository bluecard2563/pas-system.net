//Javascript
var pager;
$(document).ready(function(){
	/*AUTOCOMPLETER*/
	$("#txtNameCustomer").autocomplete(document_root+"rpc/autocompleters/loadCustomers.rpc",{
		cacheLength: 0,
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnSerial_cus").val('');
			$("#txtNameCustomer").val('');
		}else{
			$("#hdnSerial_cus").val(row[1]);
		}

	});
	/*END AUTOCOMPLETER*/

	$("#txtNameCustomer").bind('keypress',function(){
		$('#cardsContainer').html('');
	});

	$('#btnSearch').bind('click',function(){
		if($("#txtNameCustomer").val()!=''){
			 $('#cardsContainer').load(document_root+"rpc/reports/sales/loadCardsByCustomer.rpc", {serial_cus: $("#hdnSerial_cus").val()}, function(){
				 if($('#cardsTable').length>0){
					pager = new Pager('cardsTable', 10 , 'Siguiente', 'Anterior');
					pager.init(10); //Max Pages
					pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
					pager.showPage(1); //Starting page
				 }
			 });
		}else{
			alert('Ingrese alg\u00FAn par\u00E1metro de b\u00FAsqueda por favor.');
		}
	});
});