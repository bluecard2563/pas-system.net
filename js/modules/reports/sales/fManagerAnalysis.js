// JavaScript Document
$(document).ready(function(){
	 /*VALIDATION SECTION*/
	$("#frmManagerAnalysis").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			txtBeginDate: {
				required: true,
				date: true
			},
			txtEndDate: {
				required: true,
				date: true
			}
		},
		messages:{
			txtBeginDate: {
				date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
			},
			txtEndDate: {
				date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
			}
		}
	});
	/*BINDING SECTION*/
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
		$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	});

	$("#txtBeginDate").bind("change", function(e){
		 evaluateDates();
	});
	$("#txtEndDate").bind("change", function(e){
		 evaluateDates();
	});
	/*END BINDING*/

	/*CALENDAR*/
		var days_passed=dayDiff($('#first_day').val(),$('#today').val());

        setDefaultCalendar($("#txtBeginDate"),'-'+(days_passed-1)+'d','0d');
        setDefaultCalendar($("#txtEndDate"),'-'+(days_passed-1)+'d','0d');
    /*END CALENDAR*/

	/*SUBMIT BUTTONS
	$('#btnGeneratePDF').bind("click", function(){
        if($("#frmManagerAnalysis").valid()){
            $("#frmManagerAnalysis").attr("action", document_root+"modules/reports/sales/pManagerAnalysisPDF.php");
            $("#frmManagerAnalysis").submit();
        }
    });*/

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmManagerAnalysis").valid()){
            $("#frmManagerAnalysis").attr("action", document_root+"modules/reports/sales/pManagerAnalysisXLS.php");
            $("#frmManagerAnalysis").submit();
        }
	 });
	/*END SUBMIT BUTTONS*/
});

/*
 * @name: loadCountry
 * @Description: Load all the countries that have dealers in them and belongs to a specific zone.
 */
function loadCountry(serial_zon){
	if(serial_zon!=""){
		$('#countryContainer').load(document_root+"rpc/reports/dealer/loadDealerCountries.rpc",{serial_zon: serial_zon},function(){
			$('#selCountry').bind("change",function(){
				loadManager(this.value);
				$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
			});

			if($("#selCountry option").size() <=2 ){
				loadManager($("#selCountry").val());
			}
		});
	}else{
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadManager
 * @Description: Load all the managers that have dealers in them and belongs to a specific country.
 */
function loadManager(countryID){
	if(countryID!=""){
		$('#managerContainer').load(document_root+"rpc/reports/dealer/loadManagersWithDealers.rpc",{serial_cou: countryID},function(){
			$("#selManager").find('option[value=""]').remove().end().prepend('<option value="">- Todos -</option>').val('');
		});
	}else{
		$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that date from and date to are valid for the report
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));
	var first_day = new Date(Date.parse(dateFormat($('#first_day').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>today){
			alert("La Fecha Fin debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}/*else{
			if(first_day>endDate){
				alert("La Fecha Fin no puede ser menor al 1ero de enero del a\u00F1o en curso.");
				$("#txtEndDate").val('');
			}
		}*/
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		/*if(beginDate>=today){
			alert("La Fecha Inicio debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}else{
			if(first_day>beginDate){
				alert("La Fecha Inicio no puede ser menor al 1ero de enero del a\u00F1o en curso.");
				$("#txtBeginDate").val('');
			}
		}*/
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>endDate){
			 alert("La Fecha Fin debe ser superior a la Fecha Inicio.");
			 $("#txtBeginDate").val('');
		 }

	 }
}