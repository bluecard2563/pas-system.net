// JavaScript Document
/*
File:fFreeCardReport.js
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
	$("#frmFreeCard").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			}
		}
	});
	
    $("#selZone").bind("change", function(){
        loadCountriesByZone($(this).val());
    });

	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
});

/*FUNCTIONS*/

/*VALIDATE DATES RANGE*/
function validateDates(){
	var date = new Date();
	var today = (date.getDate()+1)+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
	
		var days=dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
		if(days<0){
			alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
			return false;
		}
		days=dayDiff($("#txtDateTo").val(),today);
		if(days<0){
			alert("La fecha hasta no puede ser mayor a la fecha actual.");
			return false;
		}
		return true;
}

/*CALL TO THE RPC TO LOAD THE COUNTRIES
 *BASED ON THE ZONE SELECTED*/
function loadCountriesByZone(zoneId){
    if(zoneId){//IF A ZONE HAS BEEN SELECTED
        $('#paisContainer').load(document_root+"rpc/reports/sales/loadCountriesByZone.rpc", {serial_zon: zoneId}, function(){
            $('#selCountry').bind('change', function(){
               loadManagersByCountry($(this).val());
			   loadCitiesByCountry($(this).val());
            });
			if($("#selCountry").find('option').size()==2){
				loadCitiesByCountry($('#selCountry').val());
				loadManagersByCountry($('#selCountry').val());
			}
        });
    }else{
		$('#paisContainer').html('<select name="selCountry" id="selCountry"><option value="">Todos</option></select>');
       loadManagersByCountry('');
	   loadCitiesByCountry('');
    }
}

/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#representanteContainer').load(document_root+"rpc/reports/sales/loadManagersByCountry.rpc", {serial_cou: countryId}, function(){
            $('#selManager').bind('change', function(){
               loadDealers($(this).val());
            });
        });
    }else{
        $('#representanteContainer').html('<select name="selManager" id="selManager"><option value="">Todos</option></select>');
       loadDealers('');
    }
}

/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#cityContainer').load(document_root+"rpc/reports/sales/loadCitiesByCountry.rpc", {serial_cou: countryId}, function(){
         
        });
    }else{
        $('#cityContainer').html('<select name="selCity" id="selCity"><option value="">Todos</option></select>');
    }
}

/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE MANAGER
 */
function loadDealers(managerId){
    if(managerId){
        $('#comercializadorContainer').load(document_root+"rpc/reports/sales/loadDealersByManager.rpc",{serial_mbc: managerId},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
            });
			loadBranches('');
        });
    }else{
       $('#comercializadorContainer').html('<select name="selDealer" id="selDealer"><option value="">Todos</option></select>');
       loadBranches('');
    }
}
/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#sucursalContainer").load(document_root+"rpc/reports/sales/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID});
    }else{
        $("#sucursalContainer").html('<select name="selBranch" id="selBranch"><option value="">Todos</option></select>');
    }
}

function checkSales(val){
	$("#messageDates").hide();
	if($('#frmFreeCard').valid()){
		$datesStatus = validateDates();
		if( (($("#txtDateFrom").val()!='' && $("#txtDateTo").val()!='') || ($("#txtDateFrom").val()=='' && $("#txtDateTo").val()=='')) && $datesStatus){
			$datesStatus = true;
		}else{
			$datesStatus = false;
		}

		if($('#selZone').val() && $('#selCountry').val() && $datesStatus){
			var manager = '', city = '', dealer = '', branch = '', from = '', to = '', status = '';

			if($('#selManager').val()){
				manager = $('#selManager').val();
			}
			if($('#selCity').val()){
				city = $('#selCity').val();
			}
			if($('#selDealer').val()){
				dealer = $('#selDealer').val();
			}
			if($('#selBranch').val()){
				branch = $('#selBranch').val()
			}
			if($('#txtDateFrom').val()){
				from = $('#txtDateFrom').val();
			}
			if($('#txtDateTo').val()){
				to = $('#txtDateTo').val();
			}
			if($('#selStatus').val()){
				status = $('#selStatus').val();
			}

			$.ajax({
			   type: "POST",
			   url: document_root+"rpc/reports/sales/checkFreeSales.rpc",
			   data: ({
						serial_zon : $('#selZone').val(),
						serial_cou : $('#selCountry').val(),
						serial_mbc : manager,
						serial_cit : city,
						serial_dea : dealer,
						serial_bra : branch,
						date_from : from,
						date_to : to,
						status : status
			   }),
			   success: function(msg){
				 if(msg == 'true'){
					 $('#emptyResult').html('');
					 if(val == 1){
						$("#frmFreeCard").attr("action", document_root+"modules/reports/sales/pFreeCardReportPDF.php");
					 }
					 else{
						$("#frmFreeCard").attr("action", document_root+"modules/reports/sales/pFreeCardReportXLS.php");
					 }
					 $("#frmFreeCard").submit();
				 }else{
					 $('#emptyResult').html('No existen resultados que coincidan con el criterio de b&uacute;squeda');
				 }
			   }
			 });
		}else{
			$("#messageDates").show();
			$("#txtDateFrom").val('');
		}
	}
}