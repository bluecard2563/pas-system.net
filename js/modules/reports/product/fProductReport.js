// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmProductReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                }
            }
    });
    /*END VALIDATION SECTION*/

    $('#selZone').bind("change",function(){
        $('#managerContainer').html("");
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
                    $('#managerContainer').html("");
            });
        });
    });

    $('#btnGeneratePDF').bind("click", function(){
        if($("#frmProductReport").valid()){
            $("#frmProductReport").attr("action", document_root+"modules/reports/product/pProductPDFReport.php");
            $("#frmProductReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmProductReport").valid()){
            $("#frmProductReport").attr("action", document_root+"modules/reports/product/pProductXLSReport.php");
            $("#frmProductReport").submit();
        }
    });
});