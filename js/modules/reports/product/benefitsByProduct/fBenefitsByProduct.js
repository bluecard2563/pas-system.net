// JavaScript Document
/*
File: fBenefitsByProduct.js
Author: Gabriela Guerrero
Creation Date: 12/07/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
    $("#selZone").bind("change", function(){
        loadCountriesByZone($(this).val());
    });

	$("#frmBenefitsByProduct").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                selZone: {
					required: true
                },
				selCountry: {
					required: true
                },
				selProduct: {
					required: true
                }
        },
		messages: {
			selCountry: {
				required: 'El campo "Pa&iacute;s" es obligatorio'
			},
			selProduct: {
				required: 'El campo "Producto" es obligatorio'
			}
		}
	});

	$('#btnGeneratePDF').bind('click',function(){
		if($('#frmBenefitsByProduct').valid()){
			$("#frmBenefitsByProduct").attr("action", document_root+"modules/reports/product/benefitsByProduct/pBenefitsByProductPDF.php");
			$("#frmBenefitsByProduct").submit();
		}
	});
	$('#btnGenerateXLS').bind('click',function(){
		if($('#frmBenefitsByProduct').valid()){
			$("#frmBenefitsByProduct").attr("action", document_root+"modules/reports/product/benefitsByProduct/pBenefitsByProductXLS.php");
			$("#frmBenefitsByProduct").submit();
		}
	});
});
/*CALL TO THE RPC TO LOAD THE COUNTRIES
 *BASED ON THE ZONE SELECTED*/
function loadCountriesByZone(zoneId){
    if(zoneId){//IF A ZONE HAS BEEN SELECTED
			$('#countryContainer').load(document_root+"rpc/reports/product/benefitsByProduct/loadCountriesByZone.rpc", {serial_zon: zoneId},function(){
			$("#selCountry").bind('change', function(){
                loadProductsByCountry($(this).val());
            });
			if($("#selCountry").find('option').size()==2){
				loadProductsByCountry($('#selCountry').val());
			}
			loadProductsByCountry('');
			});
    }else{
        $('#countryContainer').html('<select name="selCountry" id="selCountry"><option value="">- Seleccione -</option></select>');
		loadProductsByCountry('');
    }
}
/*CALL TO THE RPC TO LOAD THE PRODUCTS
 *BASED ON THE COUNTRY SELECTED*/
function loadProductsByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
			$('#productContainer').load(document_root+"rpc/reports/product/benefitsByProduct/loadProductsByCountry.rpc", {serial_cou: countryId},function(){});
    }else{
        $('#productContainer').html('<select name="selProduct" id="selProduct"><option value="">- Seleccione -</option></select>');
    }
}