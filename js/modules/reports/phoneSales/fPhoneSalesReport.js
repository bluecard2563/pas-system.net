// JavaScript Document
$(document).ready(function(){
	$("#frmPhoneSalesReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry:{
				required: true
			},
			selCounter: {
				required: true
			},
			beginDate: {
				required: true
			},
			endDate: {
				required: true
			}
		}
	});

	/*EVENTS*/
	$('#btnGeneratePDF').bind('click',function(){
		$("#frmPhoneSalesReport").submit();
	});

	$('#selCountry').bind('change',function(){
		$('#counterContainer').load(document_root+"rpc/reports/phoneSales/loadPhoneCounter.rpc",{serial_dea:this.value});
	});
	/*END EVENTS*/

	/*CALENDAR*/
	setDefaultCalendar($("#beginDate"),'-100y','+0d');
	setDefaultCalendar($("#endDate"),'-100y','+0d');
    /*END CALENDAR*/
});