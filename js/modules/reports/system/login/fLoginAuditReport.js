// JavaScript Document
$(document).ready(function(){
	$("#frmLoginAuditReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDateFrom: {
				required: true,
				date: true
			},
			txtDateTo: {
				required: true,
				date: true
			},
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			}
		},
		messages : {
			txtDateFrom: {
				required: 'El campo "Fecha Desde" es obligatorio.',
				date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Desde"'
			},
			txtDateTo: {
				required: 'El campo "Fecha Hasta" es obligatorio.',
				date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Hasta"'
			}
		},
		submitHandler: function(form){
			form.submit();
		}
	});

	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');

	$('#txtDateFrom').bind('change', function(){
		validateDates($('#txtDateFrom').val());
	});

	$('#txtDateTo').bind('change', function(){
		validateDates($('#txtDateTo').val());
	});

	$('#selCountry').bind('change', function(){
		loadManagersByCountry($(this).val());
	});
});

/*FUNCTIONS*/
/*VALIDATE DATES RANGE*/
function validateDates(){
	var greater_than_today;

	if($("#txtDateFrom").val()!=''){
		greater_than_today = dayDiff($("#hdnToday").val(),$("#txtDateFrom").val());
		if(greater_than_today > 1){
			alert("La Fecha desde debe ser menor a la fecha actual");
			$("#txtDateFrom").val('');
		}
	}

	if($("#txtDateTo").val()!=''){
		greater_than_today = dayDiff($("#hdnToday").val(),$("#txtDateTo").val());
		if(greater_than_today > 1){
			alert("La Fecha hasta debe ser menor a la fecha actual");
			$("#txtDateTo").val('');
		}
	}


    if($("#txtDateFrom").val()!='' && $("#txtDateTo").val()!=''){
		var days = dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
		if(days < 1){ //if departure date is smaller than arrival date
			alert("La Fecha hasta no puede ser menor a la Fecha desde");
			$("#txtDateTo").val('');
		}
	}
}

/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
    	$('#managerContainer').load(document_root+"rpc/reports/system/login/loadManagersByCountry.rpc", {serial_cou: countryId, type:'payments'}, function(){
			$('#selManager').bind('change', function(){
				loadDealers($(this).val());
			});

			if($('#selManager :option').length == 2){
				loadDealers($('#selManager').val());
			}
		});
    }else{
		$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	}
}

function loadDealers(managerId){
    if(managerId){
        $('#dealerContainer').load(document_root+"rpc/reports/system/login/loadDealersByManager.rpc",{serial_mbc: managerId, type:'payments'},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
            });

			if($('#selDealer :option').length == 2){
				loadDealers($('#selDealer').val());
			}
        });
    }else{
		$('#selDealer').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	}
}

/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#branchContainer").load(document_root+"rpc/reports/system/login/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID, type:'payments'});
    }else{
		$('#selBranch').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	}
}

