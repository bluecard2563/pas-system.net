// JavaScript Document
$(document).ready(function(){
	$('[name^="rdType"]').each(function(){
		$(this).bind('click', function(){
			loadBonusPreForm($(this).val());
		});
	});
});


function loadBonusPreForm(bonus_to){
	$('#bonusContainer').html('');
	$('#bonusContainer').load(document_root+'rpc/reports/bonus/loadReportForm.rpc', {bonus_to: bonus_to, at_least_one_paid: 'true'}, function(){
                /*CALENDAR*/
                setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
                setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
                /*END CALENDAR*/

                /* CHANGE BINDS */
                $("#txtBeginDate").bind('change', function(){
                        evaluateDates();
                });

                $("#txtEndDate").bind('change', function(){
                        evaluateDates();
                });
                /* CHANGE BINDS */
		/* VALIDATION RULES */
		$('#imgSubmit_XLS').bind('click', function(){
			if($('#selCountry').val() != ''){
				$('#frmBonusToPay').attr('action', document_root + 'modules/reports/bonus/pPaidBonusXLS');
				$('#frmBonusToPay').submit();
			}else{
				alert('Debe seleccionar al menos el Pa\u00EDs para continuar.');
			}
		});

		$('#imgSubmit_PDF').bind('click', function(){			
			if($('#selCountry').val() != ''){
				$('#frmBonusToPay').attr('action', document_root + 'modules/reports/bonus/pPaidBonusPDF');
				$('#frmBonusToPay').submit();
			}else{
				alert('Debe seleccionar al menos el Pa\u00EDs para continuar.');
			}
		});
		/* VALIDATION RULES */

		$('#selCountry').bind('change', function(){
			$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			
			loadCitiesWithBonus($(this).val(), bonus_to);
			loadMbcWithBonus($(this).val(), bonus_to);
		});

		if($('#selCountry').val() != ''){
			loadCitiesWithBonus($('#selCountry').val(), bonus_to);
			loadMbcWithBonus($('#selCountry').val(), bonus_to);			
		}		
	});
}

function loadMbcWithBonus(serial_cou, bonus_to){
	$('#managerContainer').load(document_root+'rpc/reports/bonus/loadManagersWithBonus.rpc', {serial_cou: serial_cou, bonus_to: bonus_to, at_least_one_paid: 'true'}, function(){
		$('#selManager').bind('change', function(){
			loadDealersWithBonus(bonus_to);
		});

		if($('#selManager').val() != ''){
			loadDealersWithBonus(bonus_to);
		}
	});
}

function loadCitiesWithBonus(serial_cou, bonus_to){
	$('#cityContainer').load(document_root+'rpc/reports/bonus/loadCitiesWithBonus.rpc', {serial_cou: serial_cou, bonus_to: bonus_to, at_least_one_paid: 'true'}, function(){
		$('#selCity').bind('change', function(){
			loadDealersWithBonus(bonus_to);
		});

		if($('#selCity').val() != ''){
			loadDealersWithBonus(bonus_to);
		}
	});
}

function loadDealersWithBonus(bonus_to){
	var serial_cit = $('#selCity').val();
	var serial_mbc = $('#selManager').val();

	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

	
	$('#dealerContainer').load(document_root+'rpc/reports/bonus/loadDealersWithBonus.rpc', {serial_mbc: serial_mbc, serial_cit: serial_cit, bonus_to:bonus_to, at_least_one_paid: 'true'}, function(){
		$('#selDealer').bind('change', function(){
			loadBranchesWithBonus($(this).val(), bonus_to);
		});

		if($('#selDealer').val() != ''){
			loadBranchesWithBonus($('#selDealer').val(), bonus_to);
		}
	});
}

function loadBranchesWithBonus(serial_dea, bonus_to){
	$('#branchContainer').load(document_root+'rpc/reports/bonus/loadBranchesWithBonus.rpc', {serial_dea: serial_dea, bonus_to: bonus_to, at_least_one_paid: 'true'}, function(){
		$('#selBranch').bind('change', function(){
			loadCountersWithBonus($(this).val(), bonus_to);
		});

		if($('#selBranch').val() != ''){
			loadCountersWithBonus($('#selBranch').val(), bonus_to);
		}
	});
}

function loadCountersWithBonus(serial_dea, bonus_to){
	$('#counterContainer').load(document_root+'rpc/reports/bonus/loadCountersWithBonus.rpc', {serial_dea: serial_dea, bonus_to: bonus_to, at_least_one_paid: 'true'});
}