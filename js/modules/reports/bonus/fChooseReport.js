// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmBonusReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: {
								depends: function(element) {
											var display = $("#parameters").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
                selCountry: {
                    required: {
								depends: function(element) {
											var display = $("#parameters").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
                selManager: {
                    required: {
								depends: function(element) {
											var display = $("#parameters").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
				selCity: {
                    required: {
								depends: function(element) {
											var display = $("#parameters").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
				selDealer: {
                    required: {
								depends: function(element) {
											var display = $("#parameters").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
				selBranch: {
                    required: {
								depends: function(element) {
											var display = $("#parameters").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  }
                },
				txtCardNumber: {
					required: {
								depends: function(element) {
											var display = $("#cardNumber").css('display');
											if(display=='none')
												return false;
											else
												return true;
									}
							  },
					digits: true
				}
            },
			messages:{
				txtCardNumber: {
					digits: "El campo '# de Tarjeta' admite s&oacute;lo n&uacute;meros."
				}
			}
    });
    /*END VALIDATION SECTION*/

	/*BINDING SECTION*/
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	});

	$('#btnGenerateXLS').bind('click',function(){
		submitForm('XLS');
	});

	$('#btnGeneratePDF').bind('click',function(){
		submitForm('PDF');
	});

	$("#txtBeginDate").bind("change", function(e){
		 evaluateDates();
	});
	$("#txtEndDate").bind("change", function(e){
		 evaluateDates();
	});
	
	$('[name="rdType"]').each(function(){
		$(this).bind('click',function(){
			switchFilters($(this).attr('id'));
		});
	});

	$('#cardNumber').css('display','none');
	/*END BINDING*/

	/*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-10y','+0d');
        setDefaultCalendar($("#txtEndDate"),'-10y','+0d');
    /*END CALENDAR*/
});

/*
 * @name: loadCountry
 * @Description: Load all the countries that have dealers in them and belongs to a specific zone.
 */
function loadCountry(serial_zon){
	if(serial_zon!=""){
		$('#countryContainer').load(document_root+"rpc/reports/bonus/loadCountriesWithBonus.rpc",{serial_zon: serial_zon},function(){
			$('#selCountry').bind("change",function(){
					loadManager(this.value);
					loadCity(this.value);
					$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});

			if($("#selCountry option").size() <=2 ){
				loadManager($("#selCountry").val());
				loadCity($("#selCountry").val());
			}
		});
	}else{
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadManager
 * @Description: Load all the managers that have dealers in them and belongs to a specific country.
 */
function loadManager(countryID){
	if(countryID!=""){
		$('#managerContainer').load(document_root+"rpc/reports/bonus/loadManagersWithBonus.rpc",{serial_cou: countryID},function(){
			if($("#selManager option").size() <=2 ){
				loadDealer();
			}
			$('#selManager').bind("change", function(){
				loadDealer();
				$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});
		});
	}else{
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadResponsible
 * @Description: Load all the responsibles that have dealers in them and belongs to a specific Manager.
 */
function loadCity(serial_cou){
	if(serial_cou!=''){
		$('#cityContainer').load(document_root+"rpc/reports/bonus/loadCitiesWithBonus.rpc",{serial_cou: serial_cou},function(){
			if($("#selCity option").size() <=2 ){
				loadDealer();
			}
			$('#selCity').bind("change", function(){
				loadDealer();
				$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});
		});
	}else{
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadDealer
 * @Description: Load all the dealers that belong to a specific responsible and manager
 */
function loadDealer(){
	var serial_mbc=$('#selManager').val();
	var serial_cit=$('#selCity').val();

	if(serial_mbc!='' && serial_cit!=''){
		$('#dealerContainer').load(document_root+"rpc/reports/bonus/loadDealersWithBonus.rpc",{serial_cit: serial_cit, serial_mbc: serial_mbc},function(){
			$('#selDealer').bind("change", function(){
					loadBranch(this.value);
					$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});
			if($("#selDealer option").size() <=2 ){
				loadBranch($("#selDealer").val());
			}
		});
	}else{
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadBranch
 * @Description: Load all the branches of a specific dealer that belong to a specific responsible and manager
 */
function loadBranch(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/reports/bonus/loadBranchesWithBonus.rpc",{
			serial_dea: serial_dea
		},function (){
			if($('#selBranch option').size()<=2){
				loadCounters($('#selBranch').val());
			}
			
			$('#selBranch').bind('change',function(){
				loadCounters(this.value);
			});
			
		});
	}else{
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadCounters
 * @Description: Load all the counters with applied bonus in a specific branch.
 */
function loadCounters(serial_dea){
	if(serial_dea!=''){
		$('#counterContainer').load(document_root+"rpc/reports/bonus/loadCountersWithBonus.rpc",{
			serial_dea: serial_dea
		});
	}else{
		$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: submitForm
 * @Description: Submits the form according to a TYPE
 *               XLS: to an EXCEL format
 *               PDF: to a PDF format
 */
function submitForm(type){
	var action=document_root+"modules/reports/bonus/pChooseReport"+type;

	if($('#frmBonusReport').valid()){
		$('#frmBonusReport').attr('action',action);
		$('#frmBonusReport').submit();
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));

	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>today){
			alert("La Fecha Hasta debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>today){
			alert("La Fecha Desde debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>endDate){
			 alert("La Fecha Hasta debe ser superior a la Fecha Desde.");
			 $("#txtBeginDate").val('');
		 }
	 }
}


/*
 * @name: switchFilters
 * @Description: Changes the behavior of the form in terms of the TYPE parameter
 * @param: type - PARAMETER: filters by parameters
 *              - CARD: filters by card number
 */
function switchFilters(type){
	$('#alerts').css('display','none');

	if(type=='PARAMETER'){ //Filters by Parameters
		$('#txtCardNumber').val('');
		
		$('#parameters').show();
		$('#cardNumber').hide();
	}else{ //Filters by Card Number
		$('#selZone').val('');
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selStatus').val('');
		$('#txtBeginDate').val('');
		$('#txtEndDate').val('');

		$('#parameters').hide();
		$('#cardNumber').show();
	}
}