// JavaScript Document
$(document).ready(function(){
	$("#frmCustomerReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			},
            txtBeginDate: {
                required: true,
                date: true
            },
            txtEndDate: {
                required: true,
                date: true
            }
		},
		messages:{
            txtBeginDate: {
                date: 'El campo "Fecha inicio" es obligatorio'
            },
            txtEndDate: {
                date: 'El campo "Fecha fin" es obligatorio'
            }
		}
	});
    $('#selZone').bind("change",function(){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
				loadCities(this.value);
            });

			if($('#selCountry option').size()<=2){
				loadCities($('#selCountry').val());
			}
        });
    });
	$('#imgSubmitPDF').bind('click',function(){
		sendInfo('PDF');
	});
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('Excel');
	});

    /*CALENDAR*/
    setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
    setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
    /*END CALENDAR*/

    /* CHANGE BINDS */
    $("#txtBeginDate").bind('change', function(){
        evaluateDates();
    });

    $("#txtEndDate").bind('change', function(){
        evaluateDates();
    });
    /* CHANGE BINDS */

});

/*
 *@Name: loadCities
 *@Description: Loads all cities from the selected country
 **/
function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID, opt_text:'Todos'});
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmCustomerReport').valid()){
		$('#frmCustomerReport').attr('action',document_root+'modules/reports/customer/pCustomerReport'+exportTo);
		$('#frmCustomerReport').submit();
	}
}

function evaluateDates(){
    var beginDate=$('#txtBeginDate').val();
    var endDate=$('#txtEndDate').val();

    if(beginDate && endDate){
        var new_days=parseFloat(dayDiff(beginDate,endDate));

        if(new_days < 0){
            alert('La fecha fin debe ser mayor a la fecha inicio.');
            $('#txtEndDate').val('');
        }else if(new_days > 365){
            alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
        }
    }
}