// JavaScript Document
$(document).ready(function(){
	$("#frmMailList").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			}
		}
	});
    $('#selZone').bind("change",function(){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
				loadCities(this.value);
            });

			if($('#selCountry option').size()<=2){
				loadCities($('#selCountry').val());
			}
        });
    });
	$('#imgSubmitPDF').bind('click',function(){
		sendInfo('PDF');
	});
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('Excel');
	});

});

/*
 *@Name: loadCities
 *@Description: Loads all cities from the selected country
 **/
function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID, opt_text:'Todos'});
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmMailList').valid()){
		$('#frmMailList').attr('action',document_root+'modules/reports/customer/pMailList'+exportTo);
		$('#frmMailList').submit();
	}
}