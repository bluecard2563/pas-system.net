// JavaScript Document
function loadCountry(serial_zon){
	$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	
	if(serial_zon!=''){
		$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
			$('#selCountry').bind("change", function(){
				$('#counterContainer').html("");
				loadManager(this.value);
			});
			if($("#selCountry option").size() <=2 ){
				loadManager($("#selCountry").val());
			}
		});
	}else{
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

function loadManager(serial_cou){
	if(serial_cou!=''){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou},function(){
			if($("#selManager option").size() <=2 ){
				loadDealer($("#selManager").val());
			}
			$('#selManager').bind("change", function(){
				$('#counterContainer').html("");
				loadDealer(this.value);
			});
		});
	}else{
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

function loadDealer(serial_mbc){
	if(serial_mbc!=''){
		$('#dealerContainer').load(document_root+"rpc/reports/loadDealerByManager.rpc",{serial_mbc: serial_mbc},function(){
			$('#selDealer').bind("change", function(){
				$('#counterContainer').html("");
				if(this.value != ""){
					loadBranch(this.value);
				}else{
					$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				}
			});
			if($("#selDealer option").size() <=2 ){
				loadBranch($("#selDealer").val());
			}
		});
	}else{
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

function loadBranch(serial_dea){
	$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea},function(){
		$('#selBranch').bind("change", function(){
			$('#counterContainer').html("");
		});
	});
}

$(document).ready(function(){
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
    });

    $('#btnGenerate').bind("click", function(){
        if(validate()){
            $('#counterContainer').load(document_root+"rpc/reports/loadCounter.rpc",
            {
                serial_dea: $('#selBranch').val()
            },
            function(){
                if($("#counter_table").length>0) {
                    pager = new Pager('counter_table', 10 , 'Siguiente', 'Anterior');
                    pager.init(7); //Max Pages
                    pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
                    pager.showPage(1); //Starting page
                }
            });
        }
    });
});

function validate(){
    var valid = true;
    $('#alerts').html("");
    $('#alerts').css("display", "none");
    if($('#selZone').val() == ""){
        $('#alerts').append("<li><b>"+$('#selZone').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    } else if($('#selCountry').val() == "") {
        $('#alerts').append("<li><b>"+$('#selCountry').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    } else if($('#selManager').val() == "") {
        $('#alerts').append("<li><b>"+$('#selManager').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    } else if($('#selDealer').val() == "") {
        $('#alerts').append("<li><b>"+$('#selDealer').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    } else if($('#selBranch').val() == "") {
        $('#alerts').append("<li><b>"+$('#selBranch').attr('title')+"</b></li>");
        $('#alerts').css("display", "block");
        valid = false;
    }
    return valid;
}