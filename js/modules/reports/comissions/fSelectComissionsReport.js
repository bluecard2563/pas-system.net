// JavaScript Document
$(document).ready(function(){
	$("#frmSelectComissions").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selComissionsTo: {
				required: true
			},
			selType:{
				required: true
			},
			txtStartDate:{
				date: true
			},
			txtEndDate:{
				date: true
			}
		},
		submitHandler: function(form) {
			if($('#report_type').val()!=''){
				if($('#report_type').val()=='xls'){
					form.action=document_root+"modules/reports/comissions/pReportComissionsXLS";
				}
				if($('#report_type').val()=='pdf'){
					form.action=document_root+"modules/reports/comissions/pReportComissionsPDF";
				}
				form.submit();
			}
		}
	});

	/*CHANGE OPTIONS*/
	$("#selComissionsTo").bind("change", function(e){
		loadComissionsForm(this.value);
	});

/*END CHANGE*/
});

/*
 * @Name: loadComissionsForm
 * @Description: Loads the information to pay the comissions to dealer/responsible
 * @Params: type
 **/
function loadComissionsForm(type){
	$('#comissionsContainer').html('');
	if(type!=""){
		$('#formContainer').load(document_root+'rpc/loadComissions/loadComissionsForm.rpc',{
			comissions_to: type,
			is_report:true
		},function(){
			if($('#selCountry').size()>0){
				switch(type){
					case 'DEALER':
						$('#selCountry').bind('change',function(){
							$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
							$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
							loadManagers(this.value, type);
						});
						
						if($("#selCountry option").size() ==2 ){
							loadManagers($("#selCountry").val(), type);
						}
						
						$("#selManager").rules("add", {
							required: true
						});
						$("#selResponsible").rules("remove");
						$("#selDealer").rules("remove");
						$("#selBranch").rules("remove");
						break;
					case 'RESPONSIBLE':
						$('#selCountry').bind('change',function(){
							$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
							loadCities(this.value,type);
						});
						if($("#selCountry option").size() ==2 ){
							loadCities($("#selCountry").val(),type);
						}
						$("#selResponsible").rules("add", {
							required: true
						});
						$("#selCity").rules("add", {
							required: true
						});
						break;
					case 'MANAGER':
						$("#selManager").rules("add", {
							required: true
						});
						$('#selCountry').bind('change',function(){
							loadManagers(this.value, type);
						});
						if($("#selCountry option").size() ==2 ){
							loadManagers($("#selCountry").val(), type);
						}
						break;
					case 'SUBMANAGER':
						$("#selDealer").rules("add", {
							required: true
						});
					
						$('#selCountry').bind('change',function(){
							$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
							loadCities(this.value,type);
						});
						if($("#selCountry option").size() ==2 ){
							loadCities($("#selCountry").val(),type);
						}
						$("#selCity").rules("add", {
							required: true
						});
						break;
					default:
						alert('Lo sentimos. Hubo un error en la carga de datos');
						break;
				}

				$("#selCountry").rules("add", {
					required: true
				});


				$('#btnFindComissions').bind('click',function(){
					if($("#frmSelectComissions").valid()){
						loadComissions();
					}
				});
				setDefaultCalendar($("#txtStartDate"),'-100y','+100y');
				setDefaultCalendar($("#txtEndDate"),'-100y','+100y');
				$('#selType').bind('change',function(){
					$('#comissionsContainer').html('');
				});
				$('#txtStartDate').bind('change',function(){
					$('#comissionsContainer').html('');
				});
				$('#txtEndDate').bind('change',function(){
					$('#comissionsContainer').html('');
				});
			}
		});
	}else{
		$('#formContainer').html("");
	}
}

/*
 * @Name: loadCities
 * @Description: Loads the cities of a specific country.
 * @Params: countryID
 **/
function loadCities(countryID,type){
	$('#comissionsContainer').html('');
	$('#cityContainer').load(document_root+"rpc/loadComissions/loadCitiesForComissions.rpc",
	{
		serial_cou: countryID,
		comission_to:type,
		is_report:true
	}, function(){
		switch(type){
			case 'DEALER':
			case 'SUBMANAGER':
				$('#selCity').bind('change',function(){
					loadDealer(this.value, type);
				});

				if($("#selCity option").size() ==2 ){
					loadDealer($("#selCity").val(), type);
				}
				break;
			case 'RESPONSIBLE':
				$('#selCity').bind('change',function(){
					loadResponsiblesByCity(this.value);
				});

				if($("#selCity option").size() ==2 ){
					loadResponsiblesByCity($("#selCity").val());
				}
				break;
			default:
				alert('Lo sentimos. Hubo un error en la carga de datos');
				break;
		}

		$("#selCity").rules("add", {
			required: true
		});
	});
}

/*
 * @Name: loadResponsiblesByCity
 * @Description: Loads the list of responsibles in a specific city.
 * @Params: cityID
 **/
function loadResponsiblesByCity(cityID){
	$('#comissionsContainer').html('');
	$('#reponsiblesContainer').load(document_root+"rpc/loadComissions/loadResponsablesByCityWithComissions.rpc",
	{
		serial_cit: cityID,
		is_report:true
	},
	function(){
		$("#selResponsible").rules("add", {
			required: true
		});
		$("#selResponsible").bind("change", function(e){
			getLastPayDate($('#selComissionsTo').val(),this.value);
		});
		if($("#selResponsible option").size() ==2 ){
			getLastPayDate($('#selComissionsTo').val(),$("#selResponsible").val());
		}

	});
}

/*
 * @Name: loadDealer
 * @Description: Loads the list of dealers in a specific city.
 * @Params: cityID
 **/
function loadDealer(){
	$('#comissionsContainer').html('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	
	var commissionsTo = $('#selComissionsTo').val();
	var serial_cit = $('#selCity').val();
	var serial_mbc = $('#selManager').val();
	var serial_usr = $('#selResponsible').val();

	$('#dealerContainer').load(document_root+"rpc/loadComissions/loadDealersWithComissionsReport.rpc",
	{
		serial_mbc: serial_mbc,
		serial_usr: serial_usr,
		commissionsTo: commissionsTo,
		serial_cit: serial_cit,
		is_report:true
	},
	function(){
		$("#selDealer").bind("change", function(e){
			loadBranch(this.value);
		});
		if($("#selDealer option").size() ==2 ){
			loadBranch($("#selDealer").val());
		}
	});
}

/*
 * @Name: loadBranch
 * @Description: Loads the list of branches of a specific dealer.
 * @Params: dealerID
 **/
function loadBranch(dealerID){
	$('#comissionsContainer').html('');
	$('#branchContainer').load(
		document_root+"rpc/loadComissions/loadBranchesWithComissions.rpc",
		{
			serial_dea: dealerID,
			is_report:true
		},
		function(){
			$("#selBranch").bind("change", function(e){
				getLastPayDate($('#selComissionsTo').val(),this.value);
			});
			if($("#selBranch option").size() ==2 ){
				getLastPayDate($('#selComissionsTo').val(),$("#selBranch").val());
			}
		});
}

/*
 * @Name: loadComissions
 * @Description: Loads all comission based on the given parameters.
 * @Params: dealerID
 **/
function loadComissions(){
	var comission_to=$('#selComissionsTo').val();
	var serial=0;
	switch(comission_to){
		case 'DEALER':
			serial=$('#selBranch').val();
			break;
		case 'RESPONSIBLE':
			serial=$('#selResponsible').val();
			break;
		case 'MANAGER':
			serial=$('#selManager').val();
			break;
		case 'SUBMANAGER':
			serial=$('#selDealer').val();
			break;
		default:
			alert('Lo sentimos. Hubo errores en la carga de datos');
			break;
	}

	$('#comissionsContainer').load(document_root+"rpc/reports/comissions/loadComissionsForReport.rpc",
	{
		comission_to: comission_to,
		serial: serial,
		type:$('#selType').val(),
		startDate:$('#txtStartDate').val(),
		endDate:$('#txtEndDate').val(),
		serial_mbc: $('#selManager').val(),
		serial_usr: $('#selResponsible').val(),
		serial_dea: $('#selDealer').val()
	},function(){
		if($('#xlsButton').length>0){
			$("#xlsButton").bind("click", function(e){
				$('#report_type').val('xls');
			});
		}
		if($('#pdfButton').length>0){
			$("#pdfButton").bind("click", function(e){
				$('#report_type').val('pdf');
			});
		}
	});
}
/*
 * @Name: loadManagers
 * @Description: Loads the list of managers of a specific country.
 * @Params: countryId
 **/
function loadManagers(countryId, type){
	$('#comissionsContainer').html('');
	
	if(type == 'MANAGER'){
		$('#managerContainer').load(document_root+"rpc/loadComissions/loadManagerWithComissionByCountry.rpc",
		{
			serial_cou: countryId,
			is_report:true
		},function(){
			$("#selManager").bind("change", function(e){
				getLastPayDate($('#selComissionsTo').val(),this.value);
			});
			if($("#selManager option").size() ==2 ){
				getLastPayDate($('#selComissionsTo').val(),$("#selManager").val());
			}
		});
	}else{	//DEALER
		$('#managerContainer').load(document_root+"rpc/loadComissions/loadManagersForDealerComissions.rpc",
		{
			serial_cou: countryId,
			is_report:true
		},function(){
			$("#selManager").bind("change", function(e){
				loadResponsiblesForDealer(this.value);
				loadDealer();
			});
			
			if($("#selManager option").size() == 2 ){
				loadResponsiblesForDealer($("#selManager").val());
				loadDealer();
			}
		});
	}
}

function loadResponsiblesForDealer(serial_mbc){
	$('#comissionsContainer').html('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	
	$('#reponsiblesContainer').load(document_root+"rpc/loadComissions/loadResponsibleForDealerComissions.rpc",
	{
		serial_mbc: serial_mbc,
		is_report:true
	},function(){
		$("#selResponsible").bind("change", function(e){
			loadDealer();
		});

		if($("#selResponsible option").size() ==2 ){
			loadDealer();
		}
	});
}

function getLastPayDate(type,id){
	$.ajax({
		url: document_root+"rpc/reports/comissions/getLastPaidDay.rpc",
		type: "post",
		data: ({
			type : type,
			id:id
		}),
		success: function(data) {
			if(data!='false'){
				$('#last_paid_date').html(data);
			}
		}
	});
}