$(document).ready(function(){
	$("#frmAverageComissions").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			},
			txtStartDate:{
				required: true,
				date: true
			},
			txtEndDate:{
				required: true,
				date: true
			}
		},
		messages:{
			txtStartDate:{
				date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Desde"'
			},
			txtEndDate:{
				date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Hasta"'
			}
		}
	});

	/*CHANGE OPTIONS*/
	$("#selZone").bind("change", function(e){
		clearInfoArea();
	    loadCountries(this.value);
    });

	$("#btnValidate").bind("click", function(e){
	   loadAverageInfo();
    });

	$("#txtStartDate").bind("change", function(e){
	    validateDates();
    });
	$("#txtEndDate").bind("change", function(e){
	    validateDates();
    });
	/*END CHANGE*/

	setDefaultCalendar($('#txtStartDate'), '-100y', '+0d');
	setDefaultCalendar($('#txtEndDate'), '-100y', '+0d');
});

function loadCountries(serial_zon){
	$('#selManager').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	
	$('#countryContainer').load(document_root + 'rpc/reports/dealer/loadDealerCountries.rpc', {serial_zon: serial_zon}, function(){
		if($('#selCountry :option').size() == 2){
			loadManagerByCountry($('#selCountry').val());
		}

		$("#selCountry").bind("change", function(e){
			clearInfoArea();
			loadManagerByCountry(this.value);
		});
	});
}

function loadManagerByCountry(serial_cou){
	$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');

	$('#managerContainer').load(document_root + 'rpc/loadManagersByCountry.rpc', {serial_cou: serial_cou, opt_text: 'Todos'}, function(){
		if($('#selManager :option').size() == 2){
			loadComissionsistByManager($('#selManager').val());
		}

		$("#selManager").bind("change", function(e){
			clearInfoArea();
			loadComissionsistByManager(this.value);
		});
	});
}

function loadComissionsistByManager(serial_mbc){
	clearInfoArea();

	if(serial_mbc!=''){
		$('#comissionistContainer').load(document_root + 'rpc/loadComissionistsByManager.rpc', {serial_mbc: serial_mbc}, function(){
			$('#selComissionist').bind('change', function(){
				clearInfoArea();
			});
		});
	}else{
		$('#selComissionist').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
	}
}

function validateDates(){
	clearInfoArea();
	
	if($("#txtStartDate").val()!=''){
		var less_than_today = dayDiff($("#hdnToday").val(),$("#txtStartDate").val());
		if(less_than_today > 1){
			alert("La Fecha Desde debe ser menor o igual a la fecha actual");
			$("#txtStartDate").val('');
		}
	}

	if($("#txtEndDate").val()!=''){
		var less_than_today = dayDiff($("#hdnToday").val(),$("#txtEndDate").val());
		if(less_than_today > 1){
			alert("La Fecha Hasta debe ser menor o igual a la fecha actual");
			$("#txtEndDate").val('');
		}
	}


    if($("#txtStartDate").val()!='' && $("#txtEndDate").val()!=''){
		var days = dayDiff($("#txtStartDate").val(),$("#txtEndDate").val());
		if(days <= 0){ //if departure date is smaller than arrival date
			alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
			$("#txtEndDate").val('');
		}
	}
}

function loadAverageInfo(){
	 if($("#frmAverageComissions").valid()){
		var serial_cou = $("#selCountry").val();
		var serial_mbc = $("#selManager").val();
		var serial_ubd = $("#selComissionist").val();
		var begin_date = $("#txtStartDate").val();
		var end_date = $("#txtEndDate").val();
		show_ajax_loader();

		$('#infoContainer').load(document_root + 'rpc/reports/comissions/loadAverageComissionsInfo.rpc',
		{
			serial_cou: serial_cou,
			serial_mbc: serial_mbc,
			serial_ubd: serial_ubd,
			begin_date: begin_date,
			end_date: end_date
		}, function(){
			hide_ajax_loader();
		});
	}else{
		clearInfoArea();
	}
}

function clearInfoArea(){
	$('#infoContainer').html('<i>Seleccione los par&aacute;metros para la b&uacute;squeda.</i>');
}