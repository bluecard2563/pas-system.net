// JavaScript Document
/*
File: fRefunds.js
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
    $("#selCountry").bind("change", function(){
        loadManagersByCountry($(this).val());
		loadCitiesByCountry($(this).val());
    });
	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');

	$('#radioParams').click(function(){
			$('#paramsContainer').show();
			$('#customerContainer').hide();
			$("#messageCustomer").hide();
			$('#emptyResult').html('');
			$("#hdnFlag").attr('value','2');
	});
	$('#radioCustomer').click(function(){
			$('#paramsContainer').hide();
			$('#customerContainer').show();
			$("#messageCountry").hide();
			$("#messageManager").hide();
			$("#messageDates").hide();
			$('#emptyResult').html('');
			$("#hdnFlag").attr('value','1');
	});

	$('#btnGeneratePDF').bind('click',function(){
		checkRefunds('PDF');
	});
	$('#btnGenerateXLS').bind('click',function(){
		checkRefunds('XLS');
	});

	var ruta=document_root+"rpc/autocompleters/loadCustomers.rpc";
	$("#txtCustomerData").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnCustomerID").val('');
		}else{
			$("#hdnCustomerID").val(row[1]);
		}
	});
	
});

/*FUNCTIONS*/
/*VALIDATE DATES RANGE*/
function validateDates(){
		var days=dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
		if(days<0){
			alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
			return false;
		}
		return true;
}
/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
			$('#representanteContainer').load(document_root+"rpc/reports/refund/loadManagersByCountry.rpc", {serial_cou: countryId},function(){
				$('#selManager').bind('change', function(){
					loadResponsables($(this).val());
				});
			});
    }else{
        $('#representanteContainer').html('Seleccione un Pa&iacute;s');
		loadResponsables('');
    }
}
/*CALL TO THE RPC TO LOAD THE CITIES
	 *BASED ON THE COUNTRY
 */
function loadCitiesByCountry(countryId){
    if(countryId){
        $('#ciudadContainer').load(document_root+"rpc/reports/refund/loadCitiesByCountry.rpc",{serial_cou: countryId},function(){
            $("#selCity").bind('change', function(){
                loadDealers();
            });
        });
    }else{
       $('#ciudadContainer').html('Seleccione un Pa&iacute;s');
       loadDealers();
    }
}
/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE MANAGER
 */
function loadResponsables(manager_id){
    if(manager_id){
        $('#responsableContainer').load(document_root+"rpc/reports/refund/loadResponsablesByCity.rpc",{serial_mbc: manager_id},function(){
            $("#selResponsible").bind('change', function(){
                loadDealers();
            });
			loadDealers();
        });
    }else{
       $('#responsableContainer').html('Seleccione una Ciudad');
       loadDealers();
    }
}
/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE RESPONSIBLE
 */
function loadDealers(responsibleId){
	if($('#selManager').val() && $('#selResponsible').val()){
        $('#comercializadorContainer').load(document_root+"rpc/loadDealersByManagerAndComissionistAndCity.rpc",{
        	city: $("#selCity").val(),
        	serial_mbc: $('#selManager').val(), 
        	serialComissionist: $('#selResponsible').val()
		},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				loadBranches('');
	        });
    }else{
    	$('#comercializadorContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#sucursalContainer").load(document_root+"rpc/reports/refund/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID});
    }else{
        $("#sucursalContainer").html('Seleccione un Comercializador');
    }
}

function checkRefunds(reportType){
	$('#emptyResult').html('');
	var flag = $('#hdnFlag').val(), valid = false;
	var country = '', manager = '', dateStatus = '';
	var city = '', responsible = '', dealer = '', branch = '', status = '', dateFrom = '', dateTo = '';
	var customer = '';

	if(flag == 2){
		$("#messageCountry").hide();
		$("#messageManager").hide();

		country = $('#selCountry').val(); manager = $('#selManager').val(); dateStatus = false;

		if( ($('#txtDateFrom').val() && $('#txtDateTo').val()) || (!$('#txtDateFrom').val() && !$('#txtDateTo').val()) ){
			if(validateDates()){
				dateStatus = true;
			}
		}
		if(country && manager && dateStatus){
			valid = true;

			if($('#selCity').val()){
				city = $('#selCity').val();
			}
			if($('#selResponsible').val()){
				responsible = $('#selResponsible').val();
			}
			if($('#selDealer').val()){
				dealer = $('#selDealer').val();
			}
			if($('#selBranch').val()){
				branch = $('#selBranch').val();
			}
			if($('#txtDateFrom').val() && $('#txtDateTo').val()){
				dateFrom = $('#txtDateFrom').val();
				dateTo = $('#txtDateTo').val();
			}
			if($('#selStatus').val()){
				status = $('#selStatus').val();
			}
		}
		else{
			if(!country){
				$("#messageCountry").show();
			}
			if(!manager){
				$("#messageManager").show();
			}
			if(!dateStatus){
				$("#messageDates").show();
			}
		}
	}
	else{
		$("#messageCustomer").hide();

		customer = $('#hdnCustomerID').val();

		if(customer){
			valid = true;
		}
		else{
			$("#messageCustomer").show();
		}
	}
	if(valid){
		$.ajax({
		   type: "POST",
		   url: document_root+"rpc/reports/refund/checkRefunds.rpc",
		   data: ({
					serial_cou : country,
					serial_mbc : manager,
					serial_cit : city,
					serial_usr : responsible,
					serial_dea : dealer,
					serial_bra : branch,
					status_ref : status,
					dateFrom : dateFrom,
					dateTo : dateTo,
					serial_cus : customer,
					flag : flag
		   }),
		   success: function(msg){
			 if(msg == 'true'){
				 $('#emptyResult').html('');
				 $("#frmRefunds").attr("action", document_root+"modules/reports/refund/pRefunds"+reportType+".php");
				 $("#frmRefunds").submit();
			 }else{
				 $('#emptyResult').html('No existen resultados que coincidan con el criterio de b&uacute;squeda');
			 }
		   }
		 });
	}
}