// JavaScript Document
$(document).ready(function(){
	$("#frmInvoiceReport").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry:{
				required: true
			},
			txtStartDate:{
				required: true,
				date: true
			},
			txtEndDate:{
				required: true,
				date: true
			}
		},
		messages: {
			txtStartDate:{
				date:"El campo 'Fecha de Inicio' debe tener el formato dd/mm/YYYY"
			},
			txtEndDate:{
				date:"El campo 'Fecha de Fin' debe tener el formato dd/mm/YYYY"
			}
		}
	});
    $('#selZone').bind("change",function(){
        $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: this.value, charge_country:0},function(){
            $('#selCountry').bind("change",function(){
				loadCities(this.value);
				loadManagersByCountry(this.value);
            });
			if($('#selCountry option').size()<=2){
				loadCities($('#selCountry').val());
				loadManagersByCountry($('#selCountry').val());
			}
        });
    });
	$('#selAmount').bind('change',function(){
		$('#divAmountVal').show();
		if($('#selAmount').val()==''){
			$('#divAmountVal').hide();
		}
	});

	/*BUTTONS*/
	$('#imgSubmitPDF').bind('click',function(){
		sendInfo('PDF');
	});
	$('#imgSubmitXLS').bind('click',function(){
		sendInfo('Excel');
	});
	/*END BUTTONS*/

	/*CALENDARS*/
	setDefaultCalendar($("#txtStartDate"),'-100y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
	/*END CALENDARS*/

});



/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    if($('#frmInvoiceReport').valid()){
		$('#frmInvoiceReport').attr('action',document_root+'modules/reports/invoice/pInvoiceReport'+exportTo);
		$('#frmInvoiceReport').submit();
	}
}

/*
 * loadCities
 * @description: Loads all cities from the selected Country
 * @params: countryID-> Country's serial
 **/
function loadCities(countryID){
 $('#cityContainer').load(
	document_root+"rpc/loadCities.rpc",
	{serial_cou: countryID},
	function(){
              $('#selCity').bind('change', function(){
               loadResponsablesByCity($(this).val());
			   loadDealer($(this).val());
            });

			if($('#selCity option').size()<=2){
				loadResponsablesByCity($('#selCity').val());
				loadDealer($('#selCity').val());
			}
		});

}

/*
 * loadResponsablesByCity
 * @description: Call to the RPC which loads the responsables based in the selected city
 * @params: cityID-> City's serial
 **/
function loadResponsablesByCity(cityId){
    if(cityId){//IF A CITY HAS BEEN SELECTED
        $('#responsableContainer').load(document_root+"rpc/loadResponsablesByCity.rpc",
			{
				serial_cit: cityId, 
				opt_text:'Todos'
			},function (){
				if($('#selResponsable :option').length == 2){
					loadDealer();
				}			

				$('#selResponsable').bind('change', function(){
					loadDealer();
				});		
        });
    }else{
       $('#responsableContainer').html('Seleccione una ciudad');
    }
}

/*
 *@Name: loadDealer
 *@Description: Charges all dealers
 **/
function loadDealer(){
	var serial_usr = $('#selResponsable').val();
	var serial_mbc = $('#selManager').val();
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	
	if(serial_mbc != ''){
		$('#dealerContainer').load(document_root+"rpc/loadDealers.rpc",
			{//serial_cou: countryID,
				serial_mbc: serial_mbc,
				serial_usr: serial_usr,
				dea_serial_dea: 'NULL', phone_sales: 'ALL',
				opt_text: 'Todos'
			},
			function(){
				$("#selDealer").bind("change", function(e){loadBranch(this.value);});

				if($("#selDealer option").size()<=2){
					loadBranch($("#selDealer").val());
				}
			}
		);
	}else{
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}	
}

/*
 *@Name: loadBranch
 *@Description: Loads branches
 **/
function loadBranch(dealerID){
	if(dealerID==""){
		$("#selBranch option[value='']").attr("selected",true);
		dealerID='stop';
	}
	var serial_usr = $('#selResponsable').val();

	$('#branchContainer').load(document_root+"rpc/loadDealers.rpc",
		{
			dea_serial_dea: dealerID,
			opt_text:'Todos', 
			serial_usr: serial_usr,
			phone_sales: 'ALL'
		},
		function(){
	});

}
/*
 *@Name: loadManagersByCountry
 *@Description: Loads all Managers from a selected Country
 **/
function loadManagersByCountry(countryID){
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",
		{
			serial_cou: countryID, 
			opt_text:'Seleccione'
		}, function(){
			if($('#selManager :option').length == 2){
				loadDealer();
			}			
			
			$('#selManager').bind('change', function(){
				loadDealer();
			});		
	});
}