// JavaScript Document
$(document).ready(function(){
    $("#frmRedromReport").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtDateFrom: {
                required: true,
                date: true
            },
            txtDateTo: {
                required: true,
                date: true
            },

        },
        messages : {
            txtDateFrom: {
                required: 'El campo "Fecha Desde" es obligatorio.',
                date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Desde"'
            },
            txtDateTo: {
                required: 'El campo "Fecha Hasta" es obligatorio.',
                date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Hasta"'
            }

        },
        submitHandler: function(form){
            form.submit();
        }
    });

    setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');

    $('#txtDateFrom').bind('change', function(){
        validateDates($('#txtDateFrom').val());
    });

    $('#txtDateTo').bind('change', function(){
        validateDates($('#txtDateTo').val());
    });

    $('#imgSubmitXLS').bind('click',function(){
        sendInfo('Excel');
    });
});

function validateDates(){
    var greater_than_today;

    if($("#txtDateFrom").val()!=''){
        greater_than_today = dayDiff($("#hdnToday").val(),$("#txtDateFrom").val());
        if(greater_than_today > 1){
            alert("La Fecha desde debe ser menor a la fecha actual");
            $("#txtDateFrom").val('');
        }
    }

    if($("#txtDateTo").val()!=''){
        greater_than_today = dayDiff($("#hdnToday").val(),$("#txtDateTo").val());
        if(greater_than_today > 1){
            alert("La Fecha hasta debe ser menor a la fecha actual");
            $("#txtDateTo").val('');
        }
    }


    if($("#txtDateFrom").val()!='' && $("#txtDateTo").val()!=''){
        var days = dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
        if(days < 1){ //if departure date is smaller than arrival date
            alert("La Fecha hasta no puede ser menor a la Fecha desde");
            $("#txtDateTo").val('');
        }
    }
}

/*
 * sendInfo
 * @description: Validates the form and send it to make the PDF report.
 **/
function sendInfo(exportTo){
    $('#frmRedromReport').attr('action',document_root+'modules/reports/invoice/pRedromReport'+exportTo);
    $('#frmRedromReport').submit();

}