var managerId = "";
var countryId = "";
var dateFrom = "";
var dateTo = "";

$(document).ready(function () {
    countryId = $('#selCountry').val();
    managerId = $('#selManager').val();

    $("#frmDetailedIncentiveReport").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountry: {
                required: true
            },
            txtDateFrom: {
                required: true,
                date: true
            },
            txtDateTo: {
                required: true,
                date: true
            },
        },
        messages: {
            txtDateFrom: {
                required: 'El campo "Fecha Desde" es obligatorio.',
                date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Desde"'
            },
            txtDateTo: {
                required: 'El campo "Fecha Hasta" es obligatorio.',
                date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Hasta"'
            },
            selCountry: {
                textOnly: "El campo País es obligatorio."
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    setDefaultCalendar($("#txtDateFrom"), '-100y', '+0d');
    setDefaultCalendar($("#txtDateTo"), '-100y', '+0d');

    $('#txtDateFrom').bind('change', function () {
        validateDates($('#txtDateFrom').val());
    });

    $('#txtDateTo').bind('change', function () {
        validateDates($('#txtDateTo').val());
    });

    $('#imgSubmitXLS').bind('click', function () {
        sendInfo('Excel');
    });

    $('#selManager').bind('change', function (event) {
        managerId = event.target.value;
    });

    $('#txtDateFrom').bind('change paste', function (event) {
        dateFrom = event.target.value;
    });

    $('#txtDateTo').bind('change paste', function (event) {
        dateTo = event.target.value;
    });

    $('#btnSaveIncentives').bind('click', function (event) {
        if (dateFrom === "" || dateTo === "") {
            alert("Porfavor seleccione un rango de fechas para continuar.");
            return;
        }
        saveDetailedIncentiveData();
    });


});

function validateDates() {
    var greater_than_today;

    if ($("#txtDateFrom").val() != '') {
        greater_than_today = dayDiff($("#hdnToday").val(), $("#txtDateFrom").val());
        if (greater_than_today > 1) {
            alert("La Fecha desde debe ser menor a la fecha actual");
            $("#txtDateFrom").val('');
        }
    }

    if ($("#txtDateTo").val() != '') {
        greater_than_today = dayDiff($("#hdnToday").val(), $("#txtDateTo").val());
        if (greater_than_today > 1) {
            alert("La Fecha hasta debe ser menor a la fecha actual");
            $("#txtDateTo").val('');
        }
    }


    if ($("#txtDateFrom").val() != '' && $("#txtDateTo").val() != '') {
        var days = dayDiff($("#txtDateFrom").val(), $("#txtDateTo").val());
        if (days < 1) { //if departure date is smaller than arrival date
            alert("La Fecha hasta no puede ser menor a la Fecha desde");
            $("#txtDateTo").val('');
        }
    }
}

function sendInfo(exportTo) {
    $('#frmDetailedIncentiveReport').attr('action', document_root + 'modules/reports/invoice/pDetailedIncentiveReport' + exportTo);
    $('#frmDetailedIncentiveReport').submit();
}

function saveDetailedIncentiveData() {
    show_ajax_loader();
    $.ajax({
        url: document_root + "modules/reports/invoice/pSaveDetailedIncentiveData.php",
        type: "POST",
        data: {
            action: 'saveDetailedIncentiveData',
            countryId: countryId,
            managerId: managerId,
            txtDateFrom: dateFrom,
            txtDateTo: dateTo
        },
        success: function (response) {
            console.log("REGISTROS AGREGADOS:" + response);
            hide_ajax_loader();
        }

    });
}