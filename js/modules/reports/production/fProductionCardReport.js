// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmCardReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                txtBeginDate: {
                    required: true,
                    date: true
                },
                txtEndDate: {
                    required: true,
                    date: true
                }
            },
            messages:{
                txtAmount: {
                    number: 'El campo "Monto" solo admite valores num\u00E9ricos.'
                },
                txtBeginDate: {
                    date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
                },
				selManager: {
					required: 'El campo "Representante" es obligatorio.'
				},
				selDealer: {
					required: 'El campo "Comercializador" es obligatorio.'
				}
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
	});


    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmCardReport").valid()){
            $("#frmCardReport").attr("action", document_root+"modules/reports/production/pProductionCardReportXLS.php");
            $("#frmCardReport").submit();
        }
    });

    /*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
        setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
    /*END CALENDAR*/

	/* CHANGE BINDS */
	$("#txtBeginDate").bind('change', function(){
		evaluateDates();
	});

	$("#txtEndDate").bind('change', function(){
		evaluateDates();
	});
	/* CHANGE BINDS */
});

function loadCountry(serial_zon){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
		$('#selCountry').bind("change",function(){
				loadCity(this.value);
				loadManagersByCountry(this.value);
				loadProductByCountry(this.value);
		});

		if($('#selCountry option').size()<=2){
			loadCity($('#selCountry').val());
			loadManagersByCountry($('#selCountry').val());
			loadProductByCountry($('#selCountry').val());
		}
	});
}


function evaluateDates(){
	var beginDate=$('#txtBeginDate').val();
	var endDate=$('#txtEndDate').val();

	if(beginDate && endDate){
		var new_days=parseFloat(dayDiff(beginDate,endDate));

		if(new_days < 0){
			alert('La fecha fin debe ser mayor a la fecha inicio.');
			$('#txtEndDate').val('');
		}else if(new_days > 365){
			alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
		}
	}
}