// JavaScript Document
/*
File:fPayments.js
Author: Gabriela Guerrero
Creation Date:08/06/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
	$("#frmPayments").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDateFrom: {
				required: true,
				date: true
			},
			txtDateTo: {
				required: true,
				date: true
			},
			txtCustomer: {
				digits: true
			},
			txtCard: {
				digits: true
			}
		},
		messages : {
			txtDateFrom: {
				required: 'El campo "Fecha Desde" es obligatorio.',
				date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Desde"'
			},
			txtDateTo: {
				required: 'El campo "Fecha Hasta" es obligatorio.',
				date: 'Ingrese una fecha con el formato "dd/mm/YYYY" en el campo "Fecha Hasta"'
			},
			txtCustomer: {
				digits: 'El campo "Identificaci&oacute;n del Cliente" admite solo n&uacute;meros.'
			},
			txtCard: {
				digits: 'El campo "N&uacute;mero de Tarjeta" admite solo n&uacute;meros.'
			}
		}
	});

	$("#frmPaymentsInvoice").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			txtInvoice: {
				required: true,
				digits: true
			}
		},
		messages : {
			txtInvoice: {
				digits: 'Ingrese s&oacute;lo d&iacute;gitos en el campo "Num. Factura".'
			}
		}
	});
	
    $("#generalContainer #selCountry").bind("change", function(){
        loadManagersByCountry($(this).val(),1);
		loadCitiesByCountry($(this).val());
    });
	$("#invoiceContainer #selCountry").bind("change", function(){
        loadManagersByCountry($(this).val(),2);
    });
	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
    setDefaultCalendar($("#txtDateTo"),'-100y','+0d');

	$('#radioGeneral').click(function(){
			$('#generalContainer').show();
			$('#invoiceContainer').hide();
			$("#messageCountry").hide();
			$("#messageManager").hide();
			$("#messageInvoice").hide();
			$("#emptyResult").hide();
			loadManagersByCountry('',1);

	});
	$('#radioInvoice').click(function(){
		$('#generalContainer').hide();
		$('#invoiceContainer').show();
		$("#messageDates").hide();
		$("#emptyResult").hide();
		loadManagersByCountry('',2);
	});
});

/*FUNCTIONS*/
/*VALIDATE DATES RANGE*/
function validateDates(){
		var days=dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
		if(days<0){
			alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
			$("#txtDateFrom").val('');
			return false;
		}
		return true;
}
/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId,val){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
    	var container;
    	if(val == 1){
    		container = '#generalContainer';
    	}else{
    		container = '#invoiceContainer';
    	}
    	
    	$(container+' #representanteContainer').load(document_root+"rpc/reports/payments/loadManagersByCountry.rpc", {serial_cou: countryId, type:'payments', required: (val != 1)}, function(){
			$('#generalContainer #selManager').bind('change', function(){
				if(val == 1){
					loadComissionists($(this).val());
				}else{
					loadDealers($(this).val());
				}
			});
		});
    }else{
        $('#generalContainer #representanteContainer').html('Seleccione un Pa&iacute;s');
		$('#invoiceContainer #representanteContainer #selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
       loadDealers('');
    }
}

function loadComissionists(managerId){
    if(managerId){
        $('#comissionistContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{serial_mbc: managerId, type:'payments', paymentsFilter: 'TRUE'},function(){
            $("#selComissionist").bind('change', function(){
            	loadDealersByComissionistsCity();
            });
			
            loadDealersByComissionistsCity();
        });
    }else{
       $('#comissionistContainer').html('Seleccione un Representante');
       loadDealersByComissionistsCity();
    }
}

function loadDealersByComissionistsCity(comissionistId){
	var comissionistId = $("#selComissionist").val();
	var city_id = $("#selCity").val();
	
    if(comissionistId || city_id){
        $('#comercializadorContainer').load(document_root+"rpc/reports/payments/loadDealersWithPaymentsByComissionist.rpc",
		{
			serial_usr: comissionistId,
			serial_cit: city_id
		},function(){
			$("#selDealer").bind('change', function(){
				loadBranches($(this).val());
			});
			loadBranches($('#selDealer').val());
        });
    }else{
       $('#comercializadorContainer').html('Seleccione un Responsable o una Ciudad.');
       loadBranches('');
    }
}

function loadDealers(managerId){
    if(managerId){
        $('#comercializadorContainer').load(document_root+"rpc/reports/payments/loadDealersByManager.rpc",{serial_mbc: managerId, type:'payments'},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
            });
			loadBranches('');
        });
    }else{
       $('#comercializadorContainer').html('Seleccione un Representante');
       loadBranches('');
    }
}
/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#sucursalContainer").load(document_root+"rpc/reports/payments/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID, type:'payments'});
    }else{
        $("#sucursalContainer").html('Seleccione un Comercializador');
    }
}


function checkPayments(val){
	$("#messageDates").hide();
	if($("#frmPayments").valid()){
		if(validateDates()){
			var  customer= '',  card = '', country = '', manager = '', dealer = '', branch = '',
				dateFrom = $('#txtDateFrom').val(), dateTo = $('#txtDateTo').val(), type = '', selComissionist = '';

			if($('#txtCustomer').val()){
				customer = $('#txtCustomer').val();
			}
			if($('#txtCard').val()){
				card = $('#txtCard').val();
			}
			if($('#selCountry').val()){
				country = $('#generalContainer #selCountry').val();
			}
			if($('#selManager').val()){
				manager = $('#generalContainer #selManager').val();
			}
			if($('#selDealer').val()){
				dealer = $('#selDealer').val();
			}
			if($('#selBranch').val()){
				branch = $('#selBranch').val();
			}
			if($('#selType').val()){
				type = $('#selType').val();
			}
			if($('#selComissionist').val()){
				selComissionist = $('#selComissionist').val();
			}

			$('#emptyResult').html('');
			 if(val == 1){
				$("#frmPayments").attr("action", document_root+"modules/reports/payments/pPaymentsPDF.php");
			 }
			 else{
				$("#frmPayments").attr("action", document_root+"modules/reports/payments/pPaymentsXLS.php");
			 }
			 $("#frmPayments").submit();
		}else{
			$("#messageDates").show();
		}
	}
}

function checkPaymentsInvoice(val){
	if($('#frmPaymentsInvoice').valid()){
		if($('#invoiceContainer #selCountry').val() && $('#invoiceContainer #selManager').val() && $('#txtInvoice').val()){
			var country = '', manager = '', invoice = '', type = '';

			country = $('#invoiceContainer #selCountry').val();
			manager = $('#invoiceContainer #selManager').val();
			invoice = $('#txtInvoice').val();
			if($('#rdType_1').is(':checked')){
				type = $('#rdType_1').val();
			}
			else{
				type = $('#rdType_2').val();
			}

			$('#emptyResult').html('');
			 if(val == 1){
				$("#frmPaymentsInvoice").attr("action", document_root+"modules/reports/payments/pPaymentsPDF.php");
			 }
			 else{
				$("#frmPaymentsInvoice").attr("action", document_root+"modules/reports/payments/pPaymentsXLS.php");
			 }
			 $("#frmPaymentsInvoice").submit();
		}
	}
}

function loadCitiesByCountry(country_id)
{
	if(country_id){
		$('#cityContainer').load(document_root+"rpc/loadCities.rpc",
			{
				serial_cou: country_id, 
				opt_text: 'Todos'
			}, function(){
				$("#selCity").bind('change', function(){
					loadDealersByComissionistsCity();
				});

				loadDealersByComissionistsCity();
			});
	}else{
		$('#cityContainer').html('Seleccione un pa&iacute;s.');
	}
}