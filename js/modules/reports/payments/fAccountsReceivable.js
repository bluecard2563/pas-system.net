// JavaScript Document
/*
File:fAccountsReceivable.js
Author: Gabriela Guerrero
Creation Date:31/05/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
	$("#frmAccountsReceivable").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone:{
				required: true
			},
			selCountry:{
				required: true
			},
			selManager:{
				required: true
			},
			txtDateFrom: {
				date: true
			},
			txtDateTo: {
				date: true
			}
		},
		messages: {
			selZone:{
				required: 'El campo "Zona" es obligatorio.'
			},
			selCountry:{
				required: 'El campo "Pa&iacute;s" es obligatorio.'
			},
			selManager:{
				required: 'El campo "Representante" es obligatorio.'
			},
			txtDateFrom: {
				date: 'Por favor ingrese una fecha v&aacute;lida.'
			},
			txtDateTo: {
				date: 'Por favor ingrese una fecha v&aacute;lida.'
			}
		}       
	});

	$("#selZone").bind("change", function(){
		clearRegisters();
		loadCountriesByZone($(this).val());
	});
	$("#btnGeneratePDF").bind("click", function(){
		show_ajax_loader();
		checkInvoices(1);
	});
	$("#btnGenerateXLS").bind("click", function(){
		show_ajax_loader();
		checkInvoices(2);
	});

	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
	setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
	
	$("#selCategory").bind("change", function(e){
		loadDealers();
	});
});

/*FUNCTIONS*/

/*VALIDATE DATES RANGE*/
function validateDates(){
	clearRegisters();
	var days = dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
	if(days < 0){
		alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
		return false;
	}
	return true;
}

/*CALL TO THE RPC TO LOAD THE COUNTRIES
 *BASED ON THE ZONE SELECTED*/
function loadCountriesByZone(zoneId){
	clearRegisters();
	if(zoneId){//IF A ZONE HAS BEEN SELECTED
		$('#paisContainer').load(document_root+"rpc/reports/payments/loadCountriesByZone.rpc", {
			serial_zon: zoneId, 
			required: true
		}, function(){
			$('#selCountry').bind('change', function(){
				loadCitiesByCountry($(this).val());
				loadManagersByCountry($(this).val());
			});
			if($("#selCountry").find('option').size()==2){
				loadCitiesByCountry($('#selCountry').val());
				loadManagersByCountry($('#selCountry').val());
			}
		});
	}else{
		//$('#paisContainer').html('Seleccione una Zona');
		$('#representanteContainer').html('Seleccione un pa&iacute;s.');
		$('#cityContainer').html('Seleccione un pa&iacute;s.');
	}
}

/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
	clearRegisters();
	if(countryId != ''){//IF A COUNTRY HAS BEEN SELECTED
		$('#representanteContainer').load(document_root+"rpc/reports/payments/loadManagersByCountry.rpc", {
			serial_cou: countryId, 
			required: true
		}, function(){
			$('#selManager').bind('change', function(){
				loadComissionists($(this).val());
			});
		});
	}else{
		$('#representanteContainer').html('Seleccione un pa&iacute;s.');
	}
}

/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(countryId){
	clearRegisters();
	if(countryId != ''){//IF A COUNTRY HAS BEEN SELECTED
		$('#cityContainer').load(document_root+"rpc/reports/payments/loadCitiesByCountry.rpc", {
			serial_cou: countryId
		}, function(){
			$('#selCity').change(function(e){
				loadDealers();
			});
		});
	}else{
		$('#cityContainer').html('Seleccione un pa&iacute;s.');
	}
}

/* LOAD COMISSIONISTS FROM MANAGER
 */
function loadComissionists(managerId){
	clearRegisters();
	if(managerId){
		$('#comissionistsContainer').load(document_root+"rpc/loadComissionistsByManager.rpc",{
			serial_mbc: managerId
		},function(){
			$("#selComissionist").bind('change', function(){
				loadDealers();
			});
		});
	}else{
		$('#comissionistsContainer').html('Seleccione un representante');
		loadDealers();
	}
}

/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	clearRegisters();
	if($('#selManager').val() && $('#selComissionist').val()){
		$('#comercializadorContainer').load(document_root+"rpc/loadDealersByManagerAndComissionistAndCity.rpc",{
			category: $("#selCategory").val(),
			city: $("#selCity").val(),
			serial_mbc: $('#selManager').val(), 
			serialComissionist: $('#selComissionist').val()
			},function(){
			$("#selDealer").bind('change', function(){
				loadBranches($(this).val());
			});
			loadBranches('');
		});
	}else{
		$('#comercializadorContainer').html('Seleccione un responsable');
		loadBranches('');
	}
}
/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
	clearRegisters();
	if(dealerID){
		$("#sucursalContainer").load(document_root+"rpc/reports/payments/loadBranchesByDealer.rpc",{
			dea_serial_dea: dealerID
		});
	}else{
		$("#sucursalContainer").html('Seleccione un Comercializador');
	}
}

function checkInvoices(val){	
	var datesStatus = false;
	if( ($("#txtDateFrom").val()!='' && $("#txtDateTo").val()!='') || ($("#txtDateFrom").val()=='' && $("#txtDateTo").val()=='')){
		datesStatus = true;
	}
	
	if($('#selZone').val() && $('#selCountry').val() && datesStatus && validateDates()){
		$('#emptyResult').html('Ejecutando la consulta...');
		var city = '', dealer = '', branch = '', from = '', to = '', days = '', category = '',selComissionist = '';
		if($('#selCity').val()){
			city = $('#selCity').val();
		}
		if($('#selDealer').val()){
			dealer = $('#selDealer').val();
		}
		if($('#selBranch').val()){
			branch = $('#selBranch').val()
		}
		if($('#txtDateFrom').val()){
			from = $('#txtDateFrom').val();
		}
		if($('#txtDateTo').val()){
			to = $('#txtDateTo').val();
		}
		if($('#selDays').val()){
			days = $('#selDays').val();
		}
		if($('#selCategory').val()){
			category = $('#selCategory').val();
		}
		if($('#selComissionist').val()){
			selComissionist = $('#selComissionist').val();
		}
		$.ajax({
			type: "POST",
			url: document_root+"rpc/reports/payments/checkNotPayedAccounts.rpc",
			dataType:'json',
			async:false,
			data: {
				serial_zon : $('#selZone').val(),
				serial_cou : $('#selCountry').val(),
				serial_mbc : $('#selManager').val(),
				serial_cit : city,
				serial_dea : dealer,
				serial_bra : branch,
				date_from : from,
				date_to : to,
				days : days,
				category_dea : category,
				selComissionist: selComissionist
			},
			success: function(msg){
				if(msg == true){
					if(val == 1){
						$("#frmAccountsReceivable").attr("action", document_root+"modules/reports/payments/pAccountsReceivablePDF.php");
					}
					else{
						$("#frmAccountsReceivable").attr("action", document_root+"modules/reports/payments/pAccountsReceivableXLS.php");
					}
					hide_ajax_loader();
					$("#frmAccountsReceivable").submit();
				}else{
					hide_ajax_loader();
					$('#emptyResult').html('No existen resultados que coincidan con el criterio de b&uacute;squeda');
				}
			}
		});
	}else{
		hide_ajax_loader();
		$("#frmAccountsReceivable").submit();
	}
}

function clearRegisters(){
	$('#emptyResult').html('');
}