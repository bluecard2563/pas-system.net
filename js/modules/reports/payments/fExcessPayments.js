// JavaScript Document
/*
File:fExcessPayments.js
Author: Gabriela Guerrero
Creation Date:04/06/2010
Modified By: 
Last Modified:
*/
$(document).ready(function(){
    $("#selCountry").bind("change", function(){
        loadManagersByCountry($(this).val());
    });
});

/*FUNCTIONS*/

/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#representanteContainer').load(document_root+"rpc/reports/payments/loadManagersByCountry.rpc", {serial_cou: countryId, type:'excess', required: true}, function(){
            $('#selManager').bind('change', function(){
               loadDealers($(this).val());
            });
        });
    }else{
        $('#representanteContainer').html('Seleccione un Pa&iacute;s');
       loadDealers('','');
    }
}

/*CALL TO THE RPC TO LOAD THE DEALERS
	 *BASED ON THE MANAGER
 */
function loadDealers(managerId){
    if(managerId){
        $('#comercializadorContainer').load(document_root+"rpc/reports/payments/loadDealersByManager.rpc",{serial_mbc: managerId, type:'excess'},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
            });
			loadBranches('');
        });
    }else{
       $('#comercializadorContainer').html('Seleccione un Representante');
       loadBranches('');
    }
}
/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID){
		$("#sucursalContainer").load(document_root+"rpc/reports/payments/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID, type:'excess'});
    }else{
        $("#sucursalContainer").html('Seleccione un Comercializador');
    }
}

function checkInvoices(val){
	var dealer = $('#selDealer').val();
	var branch = $('#selBranch').val()
	var status = $('#selStatus').val();
	
	$.ajax({
		type: "POST",
		url: document_root+"rpc/reports/payments/checkExcessPayments.rpc",
		data: ({
			serial_cou : $('#selCountry').val(),
			serial_mbc : $('#selManager').val(),
			serial_dea : dealer,
			serial_bra : branch,
			status : status,
		}),
		success: function(msg){
			if(msg == 'true'){
				$('#emptyResult').html('');
				if(val == 1){
					$("#frmExcessPayments").attr("action", document_root+"modules/reports/payments/pExcessPaymentsPDF.php");
				}
				else{
					$("#frmExcessPayments").attr("action", document_root+"modules/reports/payments/pExcessPaymentsXLS.php");
				}
				
				$("#frmExcessPayments").submit();
			}else{
				$('#emptyResult').html('No existen resultados que coincidan con el criterio de b&uacute;squeda');
			}
		}
	});
}