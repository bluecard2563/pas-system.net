// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchInvoice").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			selDocument: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/

    $('#selZone').change(function(){
		loadCountries($('#selZone').val());
		$('#invoiceContainer').hide();
		loadManagersByCountry('');
		loadDocumentsByManager('');
	});

});

function loadCountries(zoneId){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
            $('#selCountry').change(function(){
                loadManagersByCountry($('#selCountry').val());
                $('#invoiceContainer').hide();
                $("#txtInvoiceNumber").val('');
            });
			if($("#selCountry").find('option').size()==2){
				loadManagersByCountry($('#selCountry').val());
                $('#invoiceContainer').hide();
                $("#txtInvoiceNumber").val('');
			}
        });
}

function loadManagersByCountry(countryID){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
        if($('#selManager').val()&&$('#selDocument').val()){
            $('#invoiceContainer').show();
			addInvoiceNumberRule();
        }
        $('#selManager').bind('change',function(){
            if($('#selDocument').val()){
                $('#invoiceContainer').show();
				addInvoiceNumberRule();
            }
            if(!$('#selManager').val()){
                $('#invoiceContainer').hide();
            }
            $("#txtInvoiceNumber").val('');
            loadDocumentsByManager($('#selManager').val());
        });
		if($('#selManager option').size() <=2){
			loadDocumentsByManager($('#selManager').val());
		}
    });

}

function loadDocumentsByManager(managerID){
	$('#documentContainer').load(document_root+"rpc/loadDocumentsByManager.rpc",{serial_mbc: managerID},function(){
		if($('#selManager').val()&&$('#selDocument').val()){
			$('#invoiceContainer').show();
			addInvoiceNumberRule();
		}
		$('#selDocument').change(function(){
			if($('#selManager').val()){
				$('#invoiceContainer').show();
				addInvoiceNumberRule();
			}
			if(!$('#selDocument').val()){
				$('#invoiceContainer').hide();
			}
			$("#txtInvoiceNumber").val('');
		});
	});
}

function addInvoiceNumberRule(){
	$('#txtInvoiceNumber').rules("add", {
		 required: true,
		 remote: {
				url: document_root+"rpc/remoteValidation/invoice/checkInvoiceExist.rpc",
				type: "post",
				data: {
					number_inv: $("#txtInvoiceNumber").val(),
					serial_mbc: $("#selManager").val(),
					type_dbc: $("#selDocument").val()
				}
		 },
		 messages:{
				remote: "El n&uacute;mero de factura ingresado no existe para el representante seleccionado o no puede ser reimpreso."
		}
	});
}