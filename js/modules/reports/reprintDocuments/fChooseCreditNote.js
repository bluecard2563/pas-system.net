//JavaScript
$(document).ready(function(){
	$('#btnSearch').click(function()
	{
		var credit_note=$('#txtCreditNote').val();
		var serial_mbc=$('#selManager').val();
		var type=$('#hdnType').val();

		if(credit_note!='' && serial_mbc!='' && type!=''){
			$('#creditNoteContainer').load(document_root+"rpc/loadCNotes/getPrintCNoteForm.rpc",
			{
				cNoteNumber: credit_note,
				serial_mbc: serial_mbc,
				type: type
			}, function(){
				var optID=$('#selCNoteInformation').val();
				$('#type_dbc').val($('#'+optID).attr('type_dbc'));
				$('#serial_dbc').val($('#'+optID).attr('id'));
				$('#serial_cou').val($('#'+optID).attr('serial_cou'));
			});
				
		}else{
			alert('Ingrese todos los datos obligatorios por favor');
		}
	});

	$('#selCountry').bind('change',function(){
		loadManagersByCountry($(this).val());
	});

	$('[id^="rdOrigin_"]').each(function(i)
	{
		$(this).bind('click',function(){
			$('[id^="rdOrigin_"]').each(function(i){
				if($(this).is(':checked')){
					$('#hdnType').val($(this).val());
				}
			});

			clearContainer(true);
		});
	});
	
	$('#txtCreditNote').bind('click',function(){
		clearContainer(false);
	});
	
	$('#txtCreditNote').bind('keypress',function(){
		clearContainer(false);
	});
});

/*
 * @Name: loadManagersByCountry
 * @Description: Loads all the managers in a specific country
 * @Params: Country ID
 **/
function loadManagersByCountry(countryID)
{
	clearContainer(true);
	
	if(countryID!=''){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{
			serial_cou: countryID
		}, function(){
			$('#selManager').bind('change',function(){
				clearContainer(true);
			});
		});
	}else{
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}	
}

function clearContainer(clear_cn_nomber)
{
	$('#creditNoteContainer').html('');
	
	if(clear_cn_nomber){
		$('#txtCreditNote').html('');
	}
}