// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	var date = new Date();
	var today = (date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
	$("#frmCardsRelation").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selCountry: {
                    required: true
                },
                txtBeginDate: {
                    required: true,
					date: true,
					dateLessThan: function(){
						return today;
					}

                },
                txtEndDate: {
                    required: true,
					date: true,
					dateLessThan: function(){
						return today;
					}
                }
            },
            messages:{
				txtBeginDate: {
					date: "Ingrese una fecha v&aacute;lida en el campo 'Fecha Inicio'.",
					dateLessThan: "La fecha de inicio no debe ser mayor a la actual."
                },
                txtEndDate: {
					date: "Ingrese una fecha v&aacute;lida en el campo 'Fecha Fin'",
					dateLessThan: "La fecha de fin no debe ser mayor a la actual."
                }
            }
    });
    /*END VALIDATION SECTION*/

	/*BINDING SECTION*/
    $('#selCountry').bind("change",function(){
		loadCitiesByCountry($(this).val());
		loadManagersByCountry($(this).val());
	});
	$('#btnSearch').bind("click",function(){
		if(validateDates()){
			if($("#frmCardsRelation").valid()){
				loadCardsRelationTable($("#selCountry").val(), $("#selCity").val(), $("#selManager").val(), $("#selResponsible").val(),
									$("#txtBeginDate").val(), $("#txtEndDate").val());
			}
		}
	});
	$('#txtBeginDate').bind("change",function(){
		$('#cardsRelationContainer').html('');
	});
	$('#txtEndDate').bind("change",function(){
		$('#cardsRelationContainer').html('');
	});
	/*END BINDING*/

    /*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-10y','+0d');
        setDefaultCalendar($("#txtEndDate"),'-10y','+0d');
    /*END CALENDAR*/
});

/*VALIDATE DATES RANGE*/
function validateDates(){
		var days = dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val());
		if(days<=0){
			alert("La Fecha de Inicio no puede ser mayor a la Fecha Fin");
			return false;
		}
		return true;
}

function submitForm(type)
{
	$("#frmCardsRelation").attr("action", document_root+"modules/reports/dealer/pManualVirtualCardsRelation"+type+".php");
	$("#frmCardsRelation").submit();
}

/*LOADS THE CARDS MANUAL/VIRTUAL RELATION ON A TABLE*/
function loadCardsRelationTable(serial_cou, serial_cit, serial_mbc, serial_usr, begin_date, end_date)
{
	$('#cardsRelationContainer').load(document_root+"rpc/reports/dealer/loadCardsRelationTable.rpc",
                                    {serial_cou: serial_cou, serial_cit: serial_cit, serial_mbc: serial_mbc,
									 serial_usr: serial_usr, begin_date: begin_date, end_date: end_date},function(){
			
			$('#btnGeneratePDF').bind("click",function(){
				submitForm('PDF');
			});
			$('#btnGenerateXLS').bind("click",function(){
				submitForm('XLS');
			});
	});
}

/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(serial_cou)
{
    if(serial_cou){
        $('#cityContainer').load(document_root+"rpc/reports/dealer/loadCitiesByCountry.rpc",
                                    {serial_cou: serial_cou},function(){
            $("#selCity").bind("change", function(){
                loadResponsiblesByCityAndManager($(this).val(), $("#selManager").val());
			});
        });
    }else{
		$('#cityContainer').html('<select name="selCity" id="selCity" class="span-5"> <option value="">- Seleccione -</option></select>');
    }
	$('#responsibleContainer').html('<select name="selResponsible" id="selResponsible" class="span-5"> <option value="">- Seleccione -</option></select>');
	$('#cardsRelationContainer').html('');
}

/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(serial_cou)
{
    if(serial_cou){
        $('#managerContainer').load(document_root+"rpc/reports/dealer/loadManagersByCountry.rpc",
                                    {serial_cou: serial_cou},function(){
			$("#selManager").bind("change", function(){
                loadResponsiblesByCityAndManager($("#selCity").val(), $(this).val());
			});
        });
    }else{
		$('#managerContainer').html('<select name="selManager" id="selManager" class="span-5"> <option value="">- Seleccione -</option></select>');
    }
	$('#responsibleContainer').html('<select name="selResponsible" id="selResponsible" class="span-5"> <option value="">- Seleccione -</option></select>');
	$('#cardsRelationContainer').html('');
}

/*CALL TO THE RPC TO LOAD THE RESPONSIBLES
 *BASED ON THE CITY AND MANAGER SELECTED*/
function loadResponsiblesByCityAndManager(serial_cit, serial_mbc)
{
    if(serial_cit && serial_mbc){
        $('#responsibleContainer').load(document_root+"rpc/reports/dealer/loadResponsiblesByCityAndManager.rpc",
                                    {serial_cit: serial_cit, serial_mbc: serial_mbc},function(){
						$("#selResponsible").bind("change", function(){
							$('#cardsRelationContainer').html('');
						});
		});
    }else{
		$('#responsibleContainer').html('<select name="selResponsible" id="selResponsible" class="span-5"> <option value="">- Seleccione -</option></select>');
    }
	$('#cardsRelationContainer').html('');
}
