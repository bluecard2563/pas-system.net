// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmDealerChart").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                selManager: {
                    required: true
                },
				selResponsible: {
                    required: true
                },
				selDealer: {
                    required: true
                },
				selBranch: {
                    required: true
                },
                txtBeginDate: {
                    date: true
                },
                txtEndDate: {
                    date: true
                }
            },
            messages:{
                txtBeginDate: {
                    date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
                }
            }
    });
    /*END VALIDATION SECTION*/

	/*BINDING SECTION*/
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	});
	
	$("#txtBeginDate").bind("change", function(e){
		 evaluateDates();
	});
	$("#txtEndDate").bind("change", function(e){
		 evaluateDates();
	});
	/*END BINDING*/

    /*CALENDAR*/
		var days_passed=dayDiff($('#first_day').val(),$('#today').val());
		
        setDefaultCalendar($("#txtBeginDate"),'-'+(days_passed-1)+'d','-1d');
        setDefaultCalendar($("#txtEndDate"),'-'+(days_passed-1)+'d','-1d');
    /*END CALENDAR*/
});

/*
 * @name: loadCountry
 * @Description: Load all the countries that have dealers in them and belongs to a specific zone.
 */
function loadCountry(serial_zon){
	if(serial_zon!=""){
		$('#countryContainer').load(document_root+"rpc/reports/dealer/loadDealerCountries.rpc",{serial_zon: serial_zon},function(){
			$('#selCountry').bind("change",function(){
					loadManager(this.value);
					$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
					$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});

			if($("#selCountry option").size() <=2 ){
				loadManager($("#selCountry").val());
			}
		});
	}else{
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadManager
 * @Description: Load all the managers that have dealers in them and belongs to a specific country.
 */
function loadManager(countryID){
	if(countryID!=""){
		$('#managerContainer').load(document_root+"rpc/reports/dealer/loadManagersWithDealers.rpc",{serial_cou: countryID},function(){
			if($("#selManager option").size() <=2 ){
				loadResponsible($("#selManager").val());
			}
			$('#selManager').bind("change", function(){
				loadResponsible(this.value);
				$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});
		});
	}else{
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadResponsible
 * @Description: Load all the responsibles that have dealers in them and belongs to a specific Manager.
 */
function loadResponsible(serial_mbc){
	if(serial_mbc!=''){
		$('#responsibleContainer').load(document_root+"rpc/reports/dealer/loadResponsiblesByManager.rpc",{serial_mbc: serial_mbc},function(){
			if($("#selResponsible option").size() <=2 ){
				loadDealer($("#selResponsible").val());
			}
			$('#selResponsible').bind("change", function(){
				loadDealer(this.value);
				$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
			});
		});
	}else{
		$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadDealer
 * @Description: Load all the dealers that belong to a specific responsible and manager
 */
function loadDealer(serial_usr){
	var serial_mbc=$('#selManager').val();

	if(serial_mbc!='' && serial_usr!=''){
		$('#dealerContainer').load(document_root+"rpc/reports/dealer/loadDealerByResponsible.rpc",{serial_usr: serial_usr, serial_mbc: serial_mbc},function(){
			$('#selDealer').bind("change", function(){
					loadBranch(this.value);
			});
			if($("#selDealer option").size() <=2 ){
				loadBranch($("#selDealer").val());
			}
		});
	}else{
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @name: loadBranch
 * @Description: Load all the branches of a specific dealer that belong to a specific responsible and manager
 */
function loadBranch(serial_dea){
	var serial_mbc=$('#selManager').val();
	var serial_usr=$('#selResponsible').val();

	if(serial_dea!='' && serial_mbc!='' && serial_usr!=''){
		$('#branchContainer').load(document_root+"rpc/reports/dealer/loadBranches.rpc",{dea_serial_dea: serial_dea, serial_mbc: serial_mbc, serial_usr: serial_usr});
	}else{
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 *@Name: evaluateDates
 *@Description: Evaluates that departure date and arrival day are valid
 **/
function evaluateDates(){
	var today = new Date(Date.parse(dateFormat($('#today').val())));
	var first_day = new Date(Date.parse(dateFormat($('#first_day').val())));
	
	if($("#txtEndDate").val()!=''){
		var endDate =new Date(Date.parse(dateFormat($('#txtEndDate').val())));
		if(endDate>=today){
			alert("La Fecha Fin debe ser menor a la fecha actual.");
			$("#txtEndDate").val('');
		}/*else{
			if(first_day>endDate){
				alert("La Fecha Fin no puede ser menor al 1ero de enero del a\u00F1o en curso.");
				$("#txtEndDate").val('');
			}
		}*/
	}

	if($("#txtBeginDate").val()!=''){
		var beginDate=new Date(Date.parse(dateFormat($('#txtBeginDate').val())));
		if(beginDate>=today){
			alert("La Fecha Inicio debe ser menor a la fecha actual.");
			$("#txtBeginDate").val('');
		}/*else{
			if(first_day>beginDate){
				alert("La Fecha Inicio no puede ser menor al 1ero de enero del a\u00F1o en curso.");
				$("#txtBeginDate").val('');
			}
		}*/
	}

	 if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		 if(beginDate>=endDate){
			 alert("La Fecha Fin debe ser superior a la Fecha Inicio.");
			 $("#txtBeginDate").val('');
		 }

		 //Both dates must be on the same year
		 beginDate = $("#txtBeginDate").val();
		 endDate = $("#txtEndDate").val();
		 if(beginDate.substr(6, 4) != endDate.substr(6, 4)){
			 alert("La Fecha Fin debe tener el mismo a\u00F1o que la Fecha Inicio.");
			 $("#txtBeginDate").val('');
			 $("#txtEndDate").val('');
		 }
	 }
}