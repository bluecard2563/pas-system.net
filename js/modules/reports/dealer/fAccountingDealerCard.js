// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmAccountingAnalysis").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
				selManager: {
					required: false
				},
                txtBeginDate: {
                    required: true,
                    date: true
                },
                txtEndDate: {
                    required: true,
                    date: true
                },
                txtCxCFrom: {
                    required: true,
                    date: true
                }
            },
            messages:{
                txtBeginDate: {
                    date: 'El campo "Transacciones desde" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Transacciones hasta" debe tener el formato dd/mm/YYYY'
                },
                txtCxCFrom: {
                    date: 'El campo "CxC hasta" debe tener el formato dd/mm/YYYY'
                }
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
	});

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmAccountingAnalysis").valid()){
            $("#frmAccountingAnalysis").attr("action", document_root+"modules/reports/dealer/pAccountingDealerCardXLS.php");
            $("#frmAccountingAnalysis").submit();
        }
    });

    /*CALENDAR*/
	setDefaultCalendar($("#txtCxCFrom"),'-50y','+0d');
	setDefaultCalendar($("#txtEndDate"),'-50y','+0d');
    /*END CALENDAR*/

	/* CHANGE BINDS */
	$("#txtCxCFrom").bind('change', function(){
		evaluateDates();
		$('#txtBeginDate').val($("#txtCxCFrom").val());
	});

	$("#txtEndDate").bind('change', function(){
		evaluateDates();
	});
	/* CHANGE BINDS */
});

function loadCountry(serial_zon){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
		$('#selCountry').bind("change",function(){
			loadManagersByCountry(this.value);
		});

		if($('#selCountry option').size()<=2){
			loadManagersByCountry($('#selCountry').val());
		}
	});
}

function loadManagersByCountry(serial_cou){
	if(serial_cou){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
			$('#selManager').bind('change', function(){
            	loadDealers();
            });
			
			if($('#selManager option').size()<=2){
				loadDealers();
			}
		});
	}else{
    	loadDealers('');
    }
}


/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val()){
        $('#comercializadorContainer').load(document_root+"rpc/loadDealers.rpc",{
        	serial_mbc: $('#selManager').val(),
			dea_serial_dea: 'NULL'
		},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				
				if($('#selDealer option').size()<=2){
					loadBranches($("#selDealer").val());
				}				
	        });
    }else{
    	$('#comercializadorContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}

function evaluateDates(){
	var beginDate=$('#txtBeginDate').val();
	var endDate=$('#txtEndDate').val();

	if(beginDate && endDate){
		var new_days=parseFloat(dayDiff(beginDate,endDate));

		if(new_days < 0){
			alert('La fecha fin debe ser mayor a la fecha inicio.');
			$('#txtEndDate').val('');
		}else if(new_days > 365){
			alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
		}
	}
}