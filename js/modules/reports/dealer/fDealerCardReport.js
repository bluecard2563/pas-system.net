// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmCardReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                },
				selCity: {
					required: true
				},
				selManager: {
					required: true
				},
				selDealer: {
					required: true
				},
                txtBeginDate: {
                    required: true,
                    date: true
                },
                txtEndDate: {
                    required: true,
                    date: true
                },
                txtAmount: {
                    number: true
                }
            },
            messages:{
                txtAmount: {
                    number: 'El campo "Monto" solo admite valores num\u00E9ricos.'
                },
                txtBeginDate: {
                    date: 'El campo "Fecha inicio" debe tener el formato dd/mm/YYYY'
                },
                txtEndDate: {
                    date: 'El campo "Fecha fin" debe tener el formato dd/mm/YYYY'
                },
				selManager: {
					required: 'El campo "Representante" es obligatorio.'
				},
				selDealer: {
					required: 'El campo "Comercializador" es obligatorio.'
				}
            }
    });
    /*END VALIDATION SECTION*/
    
    $('#selZone').bind("change",function(){
        loadCountry(this.value);
	});

    $('#btnGeneratePDF').bind("click", function(){
        if($("#frmCardReport").valid()){
            $("#frmCardReport").attr("action", document_root+"modules/reports/dealer/pDealerCardReportPDF.php");
            $("#frmCardReport").submit();
        }
    });

    $('#btnGenerateXLS').bind("click", function(){
        if($("#frmCardReport").valid()){
            $("#frmCardReport").attr("action", document_root+"modules/reports/dealer/pDealerCardReportXLS.php");
            $("#frmCardReport").submit();
        }
    });

    /*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-50y','+50y');
        setDefaultCalendar($("#txtEndDate"),'-50y','+50y');
    /*END CALENDAR*/

	/* CHANGE BINDS */
	$("#txtBeginDate").bind('change', function(){
		evaluateDates();
	});

	$("#txtEndDate").bind('change', function(){
		evaluateDates();
	});
	/* CHANGE BINDS */
});

function loadCountry(serial_zon){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
		$('#selCountry').bind("change",function(){
				loadCity(this.value);
				loadManagersByCountry(this.value);
				loadProductByCountry(this.value);
		});

		if($('#selCountry option').size()<=2){
			loadCity($('#selCountry').val());
			loadManagersByCountry($('#selCountry').val());
			loadProductByCountry($('#selCountry').val());
		}
	});
}
function loadCity(serial_cou){
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: serial_cou, charge_country:0, opt_text: 'Todos'},function(){
		if($("#selCity option").size() <=2 ){
			loadResponsibleByCity(this.value);
		}
		$('#selCity').bind("change", function(){
			loadResponsibleByCity(this.value);
			$('#selResponsible').find('option').remove().end().append('<option value="">- Todos -</option>').val('');
		});
	});
}
function loadManagersByCountry(serial_cou){
	if(serial_cou){
		$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou, opt_text: 'Todos'},function(){
			$('#selManager').bind('change', function(){
            	loadDealers();
            });
			
			if($('#selManager option').size()<=2){
				loadDealers();
			}
		});
	}else{
    	loadDealers('');
    }
}


/* LOAD DEALERS FROM COMISSIONIST */
function loadDealers(){
	if($('#selManager').val()){
        $('#comercializadorContainer').load(document_root+"rpc/loadDealers.rpc",{
        	serial_cit: $("#selCity").val(),
        	serial_mbc: $('#selManager').val(),
			dea_serial_dea: 'NULL'
		},function(){
	            $("#selDealer").bind('change', function(){
	                loadBranches($(this).val());
	            });
				
				if($('#selDealer option').size()<=2){
					loadBranches($("#selDealer").val());
				}				
	        });
    }else{
    	$('#comercializadorContainer').html('Seleccione un responsable');
    	loadBranches('');
    }
}

function loadBranches(serial_dea){
	if(serial_dea!=''){
		$('#branchContainer').load(document_root+"rpc/loadBranches.rpc",{dea_serial_dea: serial_dea, opt_text: 'Todos'});
	}else{
		$('#branchContainer').html('Seleccione un comercializador');
	}
}

function loadProductByCountry(serial_cou){
	if(serial_cou!=''){
		$('#productContainer').load(document_root+"rpc/reports/sales/loadProductsByCountry.rpc",{serial_cou: serial_cou, reports: 'true'});
	}
}

function loadResponsibleByCity(serial_cit){
	if(serial_cit!=''){
		$('#responsibleContainer').load(document_root+"rpc/reports/sales/loadResponsibleByCity.rpc",{serial_cit: serial_cit, reports: 'true'});
	}
}

function evaluateDates(){
	var beginDate=$('#txtBeginDate').val();
	var endDate=$('#txtEndDate').val();

	if(beginDate && endDate){
		var new_days=parseFloat(dayDiff(beginDate,endDate));

		if(new_days < 0){
			alert('La fecha fin debe ser mayor a la fecha inicio.');
			$('#txtEndDate').val('');
		}else if(new_days > 365){
			alert('El intervalo de fechas no puede ser mayor a un a\u00F1o.');
		}
	}
}