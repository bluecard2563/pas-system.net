// JavaScript Document
var currentTable
$(document).ready(function(){
	$("#frmAssignPCondition").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selProduct: {
				required: true
			},
			selPCondition: {
				required: true
			},
			txtWeight: {
				required: true,
				digits: true
			},
			hdnRemoteValidation: {
				remote: {
					url: document_root+"rpc/remoteValidation/product/particularCondition/checkAssignedConditions.rpc",
					type: "post",
					data: {
						serial_pro: function() {
							return $("#selProduct").val();
						},
						serial_cou: function() {
							return $("#selCountry").val();
						},
						serial_prc: function() {
							return $("#selPCondition").val();
						}
					}
				}
			}
		},
		messages: {
			txtWeight: {
				digits: "El campo 'Orden' admite solo d&iacute;gitos."
			},
			hdnRemoteValidation: {
				remote: "La Condici&oacute;n Particular ya fue asociada a este producto."
			}
		}
	});

	$('#selZone').bind('change',function(){
		cleanContainer();
		loadCountries(this.value);
	});
	$('#selCountry').bind('change',function(){
		cleanContainer();
		loadProducts(this.value);
	});

	$('#selPCondition').bind('change',function(){
		remoteValidationFix();
	});
	
	$('#btnValidate').bind('click', function(){
		loadPreviousInformation();
	});
});


//LOAD THE COUNTRY SELECT BASED ON THE SELECTED ZONE
function loadCountries(zoneId){
	if(zoneId != ''){
		$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
			serial_zon: zoneId
		},function(){
			$('#selCountry').bind('change',function(){
				remoteValidationFix();
				loadProducts(this.value);
			});
			$('#selCountry').attr('title','Por favor seleccione un pa\u00EDs.');
			loadProducts($('#selCountry').val());
		});
	}else{
		$('#countryContainer').html('Seleccione una zona');
		loadProducts('');
	}
}

//LOAD THE PRODUCTS SELECT BASED ON THE SELECTED COUNTRY
function loadProducts(countryId){
	if(countryId != ''){
		$('#productContainer').load(document_root+"rpc/loadProducts.rpc",{
			serial_cou: countryId
		}, function(){
			$('#selProduct').bind('change', function(){
				remoteValidationFix();
			});
		});
	}else{
		$('#productContainer').html('Seleccione un pa\u00EDs');
	}
}

function remoteValidationFix(){
	$('#hdnRemoteValidation').val($('#hdnRemoteValidation').val() + 1);
	cleanContainer();
}

function cleanContainer(){
	$('#currentConditionsContainer').html("");
}

function loadPreviousInformation(){
	show_ajax_loader();
	$('#currentConditionsContainer').load(document_root + 'rpc/loadProduct/particularConditions/loadExistingPConditions.rpc', 
	{
		serial_cou: function(){
			return $('#selCountry').val();
		},
		serial_pro: function(){
			return $('#selProduct').val();
		},
		serial_prc: function(){
			return $('#selPCondition').val();
		}
	},
	function(){
		if($('#tblPConditions').length > 0){
			currentTable = $('#tblPConditions').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"oLanguage": {
					"oPaginate": {
						"sFirst": "Primera",
						"sLast": "&Uacute;ltima",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
					"sZeroRecords": "No se encontraron resultados",
					"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
					"sSearch": "Filtro:",
					"sProcessing": "Filtrando.."
				},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
					"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
					"aButtons": ["pdf"]
				}
			});
			
			$('[id^="pCondition_"]').each(function(){
				$(this).bind('click', function(){
					removeItemAndUpdateTable($(this).attr('serial_pcp'), $(this).attr('status_to'));
				});
			});
		}
		
		hide_ajax_loader();
	});
}

function removeItemAndUpdateTable(serial_pcp, status_to){
	$.ajax({
		url: document_root + "rpc/loadProduct/particularConditions/modifyPCondition.rpc",
		type: "POST",
		data: {
			serial_pcp: serial_pcp,
			status_to: status_to
		},
		success: function(msg){
			msg = str_replace("\"", "", msg);
			
			if(msg == 'success'){
				alert('El cambio se realiz\u00F3 exitosamente! El sistema va a actualizar la informaci\u00F3n.')
				loadPreviousInformation();
			}else if(msg == 'error_update'){
				alert('No se pudo actualizar el registro. Si el problema persiste, comun\u00EDquese con el Administrador.');
			}else if(msg == 'error_load'){
				alert('Hubo errores en la carga de informaci\u00F3n. Vuelva a intentarlo.')
			}
		}
	});
}