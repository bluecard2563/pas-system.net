// JavaScript Document
var config = {
		toolbar:
		[
			['Source','Bold', 'Italic', 'Underline', '-', 'BulletedList', '-','Cut','Copy','Paste','-','Find','Replace','SelectAll',],
			['UIColor']
		]
	};
	
$(document).ready(function(){
	if($('#txtDescription').length > 0){
		$('#txtDescription').ckeditor(config);
		$('#txtDescription').val($('#hdnDescription').val());
		
		$('#btnRegistrar').bind('click', function(){
			$('#hdnDescription').val($('#txtDescription').val());

			if($('#hdnDescription').val() != '' && $('#selStatus').val() != ''){
				$('#frmNewPCondition').submit();
			}else{
				alert('Ingrese toda la informaci\u00F3n antes de continuar.');
			}
		});
	}else{
		
	}
});


