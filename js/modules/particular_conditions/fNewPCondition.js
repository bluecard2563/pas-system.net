// JavaScript Document
var config = {
		toolbar:
		[
			['Source','Bold', 'Italic', 'Underline', '-', 'BulletedList', '-','Cut','Copy','Paste','-','Find','Replace','SelectAll',],
			['UIColor']
		]
	};
	
$(document).ready(function(){
	$('#txtDescription').ckeditor(config);
	
	$('#btnRegistrar').bind('click', function(){
		$('#hdnDescription').val($('#txtDescription').val());
		
		if($('#hdnDescription').val() != ''){
			$('#frmNewPCondition').submit();
		}else{
			alert('Ingrese el texto de la condici\u00F3n particular para continuar.');
		}
	});
});
