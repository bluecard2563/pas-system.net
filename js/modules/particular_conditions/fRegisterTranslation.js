// JavaScript Document
$(document).ready(function(){
	$('#selLanguage').bind('change', function(){
		$('#translationContainer').html('');
		
		if($(this).val()){
			loadTransaltionSection($(this).val());
		}
	});
});


function loadTransaltionSection(serial_lang){
	var serial_prc = $('#hdnSerial_prc').val();
	
	show_ajax_loader();
	$('#translationContainer').load(document_root + 'rpc/loadProduct/particularConditions/translationSection.rpc', {
		serial_lang: serial_lang,
		serial_prc: serial_prc
	}, function(){
		$('#btnRegistrar').bind('click', function(){
			$('#hdnDescription').val($('#txtDescription').val());

			if($('#hdnDescription').val() != ''){
				$('#frmNewPCondition').submit();
			}else{
				alert('Ingrese la traducci\u00F3n para continuar.');
			}
		});
		
		
		hide_ajax_loader();
	});
}

