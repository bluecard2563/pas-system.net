// JavaScript Document
$(document).ready(function(){
	$('#selAlerts').val('');
	$('#selAlerts').change(function(){
        loadUsers($('#selAlerts').val())
    });   
	
    /*VALIDATION SECTION*/
    $("#frmEditAlertsParameter").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                selAlerts: {
                    required: true
                }
        },

        messages: {
        }

	});
    /*END VALIDATION SECTION*/
});

function loadUsers(alertId){
		//THIS RPC DOES NOT FILL THE USERS MULTIPLE SELECT!
		$('#selectsContainer').load(document_root+"rpc/loadUsersByAlert.rpc",{serial_par: alertId},function(){

			$('#btnAddCountry').bind('click', function(){
				var count = parseInt($('#countriesCount').val())+1;
				var country = $('#countryWrapper').html();
				var manager = '<div class="span-8"> <div class="span-3 label">* Representante:</div><div class="span-5 last" id="managerContainer'+count+'"><select name="selManager'+count+'" id="selManager'+count+'"><option value="">- Seleccione -</option></select></div></div>';
				var city = '<div class="span-8"> <div class="span-3 label">* Ciudad:</div><div class="span-5 last" id="cityContainer'+count+'"><select name="selCity'+count+'" id="selCity'+count+'"><option value="">- Seleccione -</option></select></div></div>';
				var users = $('#usersContainer').html();

				$('#countriesContainer').append('<div id="country'+count+'" class="span-24 last upperLine"><div class="span-24 last line" ><div class="prepend-5 span-7">'+country+'</div><div class="prepend-2 span-8 last">'+manager+'</div></div><div class="span-24 last line" ><div class="prepend-4 span-20 last">'+city+'</div></div><div class="prepend-8 span-16 last" >'+users+'</div><div class="prepend-16 span-2 last"><a href="#" onclick="deleteManager('+count+'); return false;" id="delete'+count+'">Eliminar</a></div></div>');

				$('#countriesContainer #country'+count+' #countryContainer').attr('id','countryContainer'+count);
				$('#countriesContainer #country'+count+' #countryContainer'+count).attr('class', 'span-3 last');
				$('#countriesContainer #country'+count+' #countryContainer'+count+' #countriesList').attr('name','selCountry'+count);
				$('#countriesContainer #country'+count+' #countryContainer'+count+' #countriesList').attr('id','selCountry'+count);

				$('#countriesContainer #country'+count+' #usersToContainer').attr('id','usersToContainer'+count);
				$('#countriesContainer #country'+count+' #usersFromContainer').attr('id','usersFromContainer'+count);
				//changing selects attrs
				$('#countriesContainer #country'+count+' #0usersTo').attr('name','usersTo['+count+'][]');
				$('#countriesContainer #country'+count+' #0usersTo').attr('count',count);
				$('#countriesContainer #country'+count+' #0usersTo').attr('id','usersTo'+count);
				$('#countriesContainer #country'+count+' #usersFrom').attr('name','usersFrom'+count);
				$('#countriesContainer #country'+count+' #usersFrom').attr('count',count);
				$('#countriesContainer #country'+count+' #usersFrom').attr('id','usersFrom'+count);

				$('#countriesCount').val(count);
				listSelectorButtons(count);

				$('#countriesContainer #country'+count+' #countryContainer'+count+' #selCountry'+count).bind('change', function(){
					loadManagers($('#selCountry'+count).val(),count, false);
					loadCities($('#selCountry'+count).val(),count);
				});
				$('#selCountry'+count).rules("add", {
					required: true
				});
				$('#usersTo'+count).rules("add", {
					required: true
				});
			});
			
			$('[id^="usersToContainer"] [id^="usersTo"]').each(function(){
				$(this).rules("add", {
					required: true
				});
			});
			
			$('[id^="selCountry"]').each(function(){
				listSelectorButtons($(this).attr('count'));
				loadManagers($(this).val(),$(this).attr('count'), true);
				loadCities($(this).val(),$(this).attr('count'));
				$(this).bind('change', function(){
					loadManagers($(this).val(),$(this).attr('count'), false);
					loadCities($(this).val(),$(this).attr('count'));
					$('#usersFrom'+$(this).attr('count')).find('option').remove().end();
				});
			});			
    });
}

function managerCityUsers(count){
	var manager = $('#managerContainer'+count+' #selManager'+count).val();
	var city = $('#cityContainer'+count+' #selCity'+count).val();
	if(manager!='' && city!=''){
		$('#usersFromContainer'+count).load(document_root+"rpc/loadUsersByCity.rpc",{serial_cit: city, count: count, planet: 'YES'}, 
				function(e){
					//remove already selected users
					$('#country'+count+' #usersTo'+count+' option').each(function () {
						valTo = $(this).val();
			            $('#country'+count+' #usersFrom'+count+' option').each(function () {
			            	valfrom = $(this).val();
			            	if(valTo == valfrom){
			            		$(this).remove();
			            	}
				        });
			        });
				}
		);
	}
}

function loadManagers(countryId,count, loadDefault){
    if(countryId==''){
        $('#selManager'+count).find('option').remove().end().append('<option value="">- Seleccione -</option>');
    }
    $('#managerContainer'+count).load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryId, serial_sel: count},function(){
    	$('#selManager'+count).bind('change', function(){
    		addSelectedManager($('#selManager' + count).val(), count);
    		managerCityUsers(count);
		});
    	if(loadDefault){
			$('#selManager'+count).val($('#originalMbc'+count).val());
		}
    });
}

function loadCities(countryId,count){
    if(countryId==''){
        $('#selCity'+count).find('option').remove().end().append('<option value="">- Seleccione -</option>');
    }
    $('#cityContainer'+count).load(document_root+"rpc/loadCities.rpc",{serial_cou: countryId, serial_sel: count},function(){
		$('#selCity'+count).bind('change', function(){
			managerCityUsers(count);
		});
    });
}

function deleteManager(count){
    $('#country'+count).remove();
}

function addSelectedManager(managerId, count){
    if(managerId != ''){
    	$('[id^="selManager"]').each(function(e){
    		countId = $(this).attr('id').replace("selManager","");
    		if(countId != count){ //NOT THE SAME SELECT
    			if($(this).val() == managerId){
                    alert('Este representante ya ha sido asignado.');
                    $('#managerContainer'+count+' #selManager'+count+' option[value=""]').attr("selected",true);
                    $('#usersFromContainer'+count).html('');
                    return;
                }
    		}
    	});
    }
}

function listSelectorButtons(count){
    /*LIST SELECTOR*/
    $('#country'+count+' #moveAll').bind('click', function () {
        $('#country'+count+' #usersFrom'+count+' option').each(function () {
            $('#country'+count+' #usersTo'+count).append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

	$('#country'+count+' #removeAll').bind('click', function () {
        $('#country'+count+' #usersTo'+count+' option').each(function () {
            $('#country'+count+' #usersFrom'+count).append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

	$('#country'+count+' #moveSelected').bind('click', function () {
        $('#country'+count+' #usersFrom'+count+' option:selected').each(function () {
            $('#country'+count+' #usersTo'+count).append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

	$('#country'+count+' #removeSelected').bind('click', function () {
        $('#country'+count+' #usersTo'+count+' option:selected').each(function () {
            $('#country'+count+' #usersFrom'+count).append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });
    /*END LIST SELECTOR*/
}

function selectAll(){
	$('[id^="country"] [id^="usersToContainer"] [id^="usersTo"]').each(function(){
			$("#usersTo"+$(this).attr('count')+" option").attr("selected","selected");
	});
}