// JavaScript Document
$(document).ready(function(){
	$("#frmNewParameter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules:{
			txtName:{
				required:true,
				remote:{
					url:document_root+"rpc/remoteValidation/parameter/checkParameter.rpc",
					type: "post"
				}
			},
			txtValue:{
				required:true
			},
			selType:{
				required:true
			},
			txaDescription:{
				required:true
			}
		},
		messages:{
			txtName:{
				remote:'Ese par\xe1metro ya fue ingresado en el sistema'
			},
			txtValue:{
				number:'El campo Valor solo puede ser num\xe9rico'
			}
		}
	});
});
		