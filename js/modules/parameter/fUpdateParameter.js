// JavaScript Document
$(document).ready(function(){
	$("#frmUpdateParameter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/parameter/checkParameter.rpc",
					type: "post",
					data: {
					  serial_par: function() {
						return $("#hdnParameterID").val();
					  }
					}
				}
			},
			txtValue: {
				required: true
			},
			selType: {
				required: true
			},
			txaDescription: {
				required: true
			}
		},
		messages: {
			txtName: {
				remote: 'Ese par\xe1metro ya existe en el sistema'
			}
		}
	});
});// JavaScript Document