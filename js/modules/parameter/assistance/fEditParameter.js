//Java Script
$(document).ready(function(){
	$('#selType').bind("change", function(){
		restartSelects();
		if(this.value != '') {
			$('#selectionContainer').css('display', 'block'); //loadAssistanceMails
			$.getJSON(document_root+"rpc/parameter/assistance/loadAssistanceMails.rpc", {type:this.value}, function(data){
				if(data) {
					for(var i=0;i<data.length;i++){
						if(data[i][0]) {
							$('#usersTo').append("<option value='"+data[i][0]+"' selected>"+data[i][1]+"</option>");
						}
					}
				}
			});
		} else {
			$('#selectionContainer').css('display', 'none');
		}
	});

	$('#selZone').bind("change",function(){
		loadCountry(this.value);
	});

	/* SELECTOR BUTTONS */
	$('#moveAll').bind('click', function () {
        $('#usersFrom option').each(function () {
            $('#usersTo').append('<option value="'+$(this).attr('value')+'" selected>'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

	$('#removeAll').bind('click', function () {
        $('#usersTo option').each(function () {
            $('#usersFrom').append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

	$('#moveSelected').bind('click', function () {
        $('#usersFrom option:selected').each(function () {
            $('#usersTo').append('<option value="'+$(this).attr('value')+'" selected>'+$(this).text()+'</option>');
            $(this).remove();
        });
    });

	$('#removeSelected').bind('click', function () {
        $('#usersTo option:selected').each(function () {
            $('#usersFrom').append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>');
            $(this).remove();
        });
    });
	/* END SELECTOR BUTTONS */

	$('#btnSave').bind("click", function(){
		$('#usersTo option').each(function () {
            $(this).attr("selected", true);
			$('#frmEditParameter').submit();
        });
	});
});

function loadCountry(serial_zon){
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: serial_zon, charge_country:0},function(){
        $('#selCountry').bind("change",function(){
            loadManager(this.value);
        });
		if($('#selCountry option').size()<=2){
            loadManager($('#selCountry').val());
		}
    });
}

function loadManager(serial_cou){
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: serial_cou},function(){
		$('#selManager').bind("change",function(){
			loadUsersByManager($('#selManager').val());
        });
		if($('#selManager option').size()<=2){
            loadUsersByManager($('#selManager').val());
		}
	});
}

function loadUsersByManager(serial_mbc){
	$('#usersFrom').html('');
	var not_in="";
	$('#usersTo option').each(function(){
		not_in+="'"+this.value+"',"
	});
	
	$.getJSON(document_root+"rpc/parameter/assistance/loadUsersByManager.rpc", {serial_mbc:serial_mbc, not_in:not_in}, function(data){
		if(data) {
				for(var i=0;i<data.length;i++){
					if(data[i][0]) {
						$('#usersFrom').append("<option value='"+data[i][0]+"'>"+data[i][1]+"</option>");
					}
				}
			}
	});
}

function restartSelects(){
	$('#selZone option[value=""]').attr('selected',true);
	$('#selCountry').html("<option value=''>- Seleccione -</option>");
	$('#selManager').html("<option value=''>- Seleccione -</option>");
	$('#usersTo').html('');
	$('#usersFrom').html('');
}