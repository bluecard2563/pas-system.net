// JavaScript Document
$(document).ready(function(){
	/*AUTOCOMPLETER*/
	
	$("#txtText").autocomplete(document_root+"rpc/autocompleters/loadParameters.rpc",{
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#hdnParameterID").val(row[1]);
	});
	/*END AUTOCOMPLETER*/
	
	/*VALIDATION SECTION*/
	$("#frmSearchParameter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtText: {
				required: true
			}		
		}
	});
	/*END VALIDATION*/
});// JavaScript Document