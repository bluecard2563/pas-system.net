// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmDealerVisitReport").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                    txtEndDate: {
                        greaterOrEqualTo: '#txtBeginDate'
                    }

            },
            messages: {
                    txtEndDate: {
                        greaterOrEqualTo: 'La "Fecha hasta" debe ser mayor que la "Fecha desde".'
                    }
            }
    });
    /*END VALIDATION SECTION*/

    /*CALENDAR*/
        setDefaultCalendar($("#txtBeginDate"),'-100y','+0d');
        setDefaultCalendar($("#txtEndDate"),'-100y','+0d');
    /*END CALENDAR*/

    $('#selUser').bind("change",function(){
        $('#dealerContainer').load(document_root+"rpc/dealerVisit/loadDealersByUser.rpc", {serial_usr:  this.value, report: true});
    });
});