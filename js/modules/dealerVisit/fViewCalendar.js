// JavaScript Document
var eventId = 1;
var myEvent;

$(document).ready(function() {

    /*CALENDAR*/
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
            theme: false,
            height: 450,
            header: {
                    left: 'title prev,next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
            },
            weekends:false,
            allDaySlot:true,
            allDayText:'Visitados',
            slotMinutes:15,
            defaultEventMinutes: 60,
            minTime: 8,
            maxTime: 19,
            editable: true,
            buttonText: {
                today:  'hoy',
                month:  'mes',
                agendaWeek: 'semana',
                agendaDay:  'd\u00eda'
            },
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNames: ['Domingo','Lunes','Martes','Mi\u00e9rcoles','Jueves','Viernes','Sabado'],
            dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
			events: document_root+"rpc/dealerVisit/loadDealerVisits.rpc?serial_usr="+$('#hdnUser').val(),
            /*Events*/
            dayClick: function(date, allDay, jsEvent, view ) {
                if(view.name == "agendaDay" || view.name == "agendaWeek") {
                    if(!hasEvent(1,date,0)){
                        $('#eventTime').html( $.fullCalendar.formatDate(date,"hh:mm"));
                        $('#hdnEventTime').val(date);
                        $("#selStatus [value='PENDING']").attr('selected', true);
                        $("#selType [value='']").attr('selected', true);
                        $("#newEventDialog").dialog('open');
                    } else {
                        alert("Podr\u00eda existir sobreposici\u00f3n de eventos en la hora seleccionada.");
                    }
                } else{
                    $('#calendar').fullCalendar( 'gotoDate', date );
                    $('#calendar').fullCalendar( 'changeView', "agendaDay" );
                }
            },
            eventClick: function(callEvent, jsEvent, view ) {
                if(view.name == "agendaDay" || view.name == "agendaWeek") {
                    if(callEvent.status == "VISITED"){
						$('#eventTitleVis').html(callEvent.title);
						$('#selTypeVis').val(callEvent.type);
                        $('#selTypeVis').attr('disabled', true);
						
						$.ajax({
							url: document_root+"rpc/dealerVisit/loadObservationsByVisit.rpc",
							data: {
								serial_vis: callEvent.id
							},
							dataType: "json",
							success: function(data) {
								$('#eventHostVis').html(data[0].host_obv);
								$('#txtCommentsVis').html(data[0].comments_obv);
								$('#txtStrategyVis').html(data[0].strategy_obv);
								$("#visitedDialog").dialog('open');
							}
						});                      
                    }
                    else {
						$('#eventTimeEd').html( $.fullCalendar.formatDate(callEvent.start,"hh:mm"));
						$('#eventTitleEd').html(callEvent.title);
						$('#selTypeEd').val(callEvent.type);
						$('#selStatusEd').val(callEvent.status);
						myEvent=callEvent;
                    	$("#editEventDialog").dialog('open');
                    }
                    
                } else{
                    $('#calendar').fullCalendar( 'gotoDate', callEvent.start );
                    $('#calendar').fullCalendar( 'changeView', "agendaDay" );
                }
            },
            eventDrop: function(callEvent,dayDelta,minuteDelta,allDay,revertFunc) {
                if(hasEvent(2,callEvent.start,callEvent.id,callEvent.end)){
                    revertFunc();
                } else if(callEvent.status=="VISITED") {
                    revertFunc();
                } else {
                    if(callEvent.allDay) {
                        if(callEvent.type == "PRODUCT" || callEvent.type == "BONUS" || callEvent.type == "PROMO") {
                            callEvent.status = "VISITED";
                            myEvent=callEvent;
                            editDate(callEvent);
                        } else {
                            revertFunc();
                            alert("Seleccione un tipo de visita.");
                        }
                    } else {
                        editDate(callEvent);
                    }
                }

            },
            eventResize: function(callEvent,dayDelta,minuteDelta,revertFunc) {
                if(hasEvent(3,callEvent.start,callEvent.id,callEvent.end)){
                    revertFunc();
                }
                editDate(callEvent);
            }

    });
    /*END CALENDAR*/

    /*NEW EVENT DIALOG*/
    $("#newEventDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 280,
		width: 480,
		modal: true,
                resizable: false,
                title:"Nuevo Evento",
		buttons:  {
			'Guardar': function() {
                                  if(validateNewEventDialog()){
                                      addDate();
                                      $('#selDealer').val("");
									  $('#selBranch').val("");
                                      $('#selStatus').val("");
                                      $(this).dialog('close');
                                  }
				},
			'Cancelar': function() {
				$('#alerts_dialog').css('display','none');
                                $('#selDealer').val("");
                                $("#validateNewEventDialog").css("display","none");
                                $("#validateDealer").css("display","none");
                                $("#validateType").css("display","none");
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});
    /*END NEW EVENT DIALOG*/

    /*EDIT EVENT DIALOG*/
    $("#editEventDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 230,
		width: 420,
		modal: true,
                resizable: false,
                title:"Editar Evento",
		buttons:  {
			'Guardar': function() {
                               if(validateEditEventDialog()){
                                  myEvent.type=$('#selTypeEd').val();
                                  myEvent.status=$('#selStatusEd').val();
                                  editDate(myEvent);
                                  $('#selStatusEd').val("");
                                  $(this).dialog('close');
                               }
                            },
                        'Eliminar': function() {
                                $.getJSON(document_root+"rpc/dealerVisit/checkMonthVisitsPerDealer.rpc",
                                {
                                    serial_vis: myEvent.id
                                },
                                    function(data){
                                        if(data){
                                           if(confirm("Est\u00e1 seguro de querer eliminar el evento?")) {
                                              deleteDate(myEvent);
                                              $("#editEventDialog").dialog('close');

                                            }
                                        } else {
                                            alert("No se pueden eliminar m\u00e1s visitas de las requeridas por el comercializador.");
                                        }
                                });
                            },
			'Cancelar': function() {
				$('#alerts_dialog').css('display','none');
                                $("#validateEditEventDialog").css("display","none");
                                $("#validateTypeEd").css("display","none");
				$(this).dialog('close');
			}
		  },
		close: function() {
                        $('#alerts_dialog').css('display','none');
                        $("#validateEditEventDialog").css("display","none");
                        $("#validateTypeEd").css("display","none");
			$('select.select').show();
		}
	});
    /*EDIT EVENT DIALOG*/

    /*OBSERVATIONS DIALOG*/
    $("#observationsDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 345,
		width: 450,
		modal: true,
                resizable: false,
                closeOnEscape: false,
                open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
                title:"Observaciones de la visita",
		buttons:  {
                    'Guardar': function() {
                        if(validateObservationsDialog()) {
                            insertObservationsByDealer(myEvent);
                            $('#txtHost').val("");
                            $('#txtComments').val("");
                            $('#txtStrategy').val("");
                            $(this).dialog('close');
                        }
                    }
               }
	});
    /*OBSERVATIONS DIALOG*/
	
	/*VISITED DIALOG*/
    $("#visitedDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 410,
		width: 450,
		modal: true,
                resizable: false,
                closeOnEscape: false,
                open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
                title:"Observaciones de la visita",
		buttons:  {
                    'Aceptar': function() {
                            $(this).dialog('close');
                    }
               }
	});
    /*VISITED DIALOG*/

	$('#selDealer').bind('change',function(){
		loadBranches(this.value);
	});
});

//Loads all the branches from a dealer
function loadBranches(serial_dea){
	if(serial_dea) {
		$('#BranchContainer').load(document_root+"rpc/loadBranches.rpc", {dea_serial_dea:serial_dea, calendar: 1});
	} else {
		$("#selBranch option[value='']").attr("selected", true);
	}
}

//Adds an Event
function addDate() {
    var date=$.fullCalendar.parseDate($('#hdnEventTime').val());
    //Date String Controll
    if($.fullCalendar.formatDate(date, "H")<9) {
        var endTime = $.fullCalendar.parseDate($.fullCalendar.formatDate(date, "yyyy-MM-dd")+" 09"+$.fullCalendar.formatDate(date, ":mm"));
    }else{
        endTime = $.fullCalendar.parseDate($.fullCalendar.formatDate(date, "yyyy-MM-dd")+" "+(parseInt($.fullCalendar.formatDate(date, "H"))+parseInt(1))+$.fullCalendar.formatDate(date, ":mm"));
    }

    //Creates event in Database
    var event = new Array();
    event[0] = new Date().getTime()+eventId;
    event[1] = $('#selBranch').val();
    event[2] = $('#hdnUser').val();
    event[3] = $('#selBranch option:selected').text();
    event[4] = $.fullCalendar.formatDate(date, "yyyy-MM-dd H:mm");
    event[5] = $.fullCalendar.formatDate(endTime, "yyyy-MM-dd H:mm");
    event[6] = $('#selType').val();
    event[7] = $('#selStatus').val();
    event[8] = false;
    event[9] = "event";

    if($('#selStatus').val()=="SKIPPED") {
        event[9] = "skipped";
    }

    if($('#selStatus').val()=="VISITED") {
        event[4] = $.fullCalendar.formatDate(date, "yyyy-MM-dd");
        event[5] = $.fullCalendar.formatDate(endTime, "yyyy-MM-dd");
        event[8] = true;
        event[9] = "visited";
    }

    $.getJSON(document_root+"rpc/dealerVisit/insertDealerVisit.rpc", {newEvent: event.toString()},function(data){
        if(data) {
            //Creates event in calendar
            $('#calendar').fullCalendar('renderEvent', {
                        id:      event[0],
                        serial:  event[1],
                        user:    event[2],
                        title:   event[3],
                        start:   date,
                        end:     endTime,
                        type:    event[6],
                        status:  event[7],
                        allDay : event[8],
                        className: event[9]
            },true);
            eventId++;
            if(callEvent.status=="VISITED"){
                $("#observationsDialog").dialog('open');
            }
            showMessages("success");
        } else {
            showMessages("error");
        }
    });
}

//Edits an Event
function editDate(callEvent) {
    if(callEvent.status == "VISITED"){
        if(callEvent.className != "visited"){
            callEvent.className = ["visited"];
        }
        callEvent.allDay = true;
        callEvent.start = $.fullCalendar.parseDate($.fullCalendar.formatDate(callEvent.start, "yyyy-MM-dd"));
        callEvent.end = $.fullCalendar.parseDate($.fullCalendar.formatDate(callEvent.end, "yyyy-MM-dd"));
    } else if(callEvent.status=="SKIPPED"){
        if(callEvent.className != "skipped"){
            callEvent.className = ["skipped"];
        }
    } else {
        if(callEvent.className != "event"){
            callEvent.className = ["event"];
        }
    }
    $.getJSON(document_root+"rpc/dealerVisit/updateDealerVisit.rpc",
    {
        id: callEvent.id,
        type:callEvent.type,
        status:callEvent.status,
        start:$.fullCalendar.formatDate(callEvent.start, "yyyy-MM-dd H:mm"),
        end:$.fullCalendar.formatDate(callEvent.end, "yyyy-MM-dd H:mm"),
        allDay:callEvent.allDay
    },
        function(data){
            if(data){
                $('#calendar').fullCalendar('updateEvent', callEvent);
                if(callEvent.status == "VISITED"){
                    $("#observationsDialog").dialog('open');
                }
                showMessages("success");
            } else {
                showMessages("error");
            }
    });

}

//Deletes an Event
function deleteDate(callEvent) {
    $.getJSON(document_root+"rpc/dealerVisit/deleteDealerVisit.rpc",{serial_vis: callEvent.id},function(data){
        if(data){
            $('#calendar').fullCalendar('removeEvents',callEvent.id);
            showMessages("success");
        } else {
            showMessages("error");
        }
    });
}

//Deletes events at selected month
function deleteMonthEvents(month, year){
    var events = new Array();
    events = $('#calendar').fullCalendar('clientEvents');
    for(var i=0;i<events.length;i++){
        if($.fullCalendar.formatDate(events[i].start, "MM")==month && $.fullCalendar.formatDate(events[i].start, "yyyy")==year) {
            if(!events[i].allDay) {
                deleteDate(events[i]);
            }
        }
    }
}

//Inserts the observations from a visit into Database
//when the status of the visit is changed to "VISITED"
function insertObservationsByDealer(callEvent){
    $.getJSON(document_root+"rpc/dealerVisit/insertObservationsByDealer.rpc",
    {
        serial_vis: callEvent.id,
        host_obv:$('#txtHost').val(),
        comments_obv:$('#txtComments').val(),
        strategy_obv:$('#txtStrategy').val()
    },
        function(data){
            if(data){
                $('#calendar').fullCalendar('updateEvent', callEvent);
                showMessages("success");
            } else {
                showMessages("error");
            }
    });
}

//Verifies if an event already exists at the selected hour
function hasEvent(opt,date,dateId,dateEnd){
    var events = $('#calendar').fullCalendar('clientEvents');
    //Verifies at the same hour
    for(var i=0;i<events.length;i++) {
        if($.fullCalendar.formatDate(events[i].start, "MM")==$.fullCalendar.formatDate(date, "MM")
            && $.fullCalendar.formatDate(events[i].start, "yyyy")==$.fullCalendar.formatDate(date, "yyyy")
            && $.fullCalendar.formatDate(events[i].start, "dd")==$.fullCalendar.formatDate(date, "dd")
            && dateId!=events[i].id) {
                switch(opt) {
                    case 1://clicking
                        if((parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-parseInt($.fullCalendar.formatDate(date, "Hmm")) <=0
                            && parseInt($.fullCalendar.formatDate(events[i].end, "Hmm"))-parseInt($.fullCalendar.formatDate(date, "Hmm")) >0)
                            || (parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-(parseInt($.fullCalendar.formatDate(date, "Hmm"))+parseInt(100)) <0)
                            && parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-parseInt($.fullCalendar.formatDate(date, "Hmm")) >=0) {
                                return true;
                        }
                        break;
                    case 2://dragging
                        if((parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-parseInt($.fullCalendar.formatDate(date, "Hmm")) <=0
                            && parseInt($.fullCalendar.formatDate(events[i].end, "Hmm"))-parseInt($.fullCalendar.formatDate(date, "Hmm")) >0)
                            ||(parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-parseInt($.fullCalendar.formatDate(dateEnd, "Hmm")) <0
                            && parseInt($.fullCalendar.formatDate(events[i].end, "Hmm"))-parseInt($.fullCalendar.formatDate(dateEnd, "Hmm")) >=0)) {
                                return true;
                        }
                        break;
                    case 3://rezising
                        if(parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-parseInt($.fullCalendar.formatDate(date, "Hmm")) >=0
                            && parseInt($.fullCalendar.formatDate(events[i].start, "Hmm"))-parseInt($.fullCalendar.formatDate(dateEnd, "Hmm")) <0) {
                                return true;
                        }
                        break;
                }
        }
    }
    return false;
}

function validateNewEventDialog(){
    var validate = true;
    $("#validateNewEventDialog").css("display","none");
    $("#validateDealer").css("display","none");
    $("#validateType").css("display","none");
    if($("#selDealer").val()== '') {
        $("#validateNewEventDialog").css("display","block");
        $("#validateDealer").css("display","block");
        validate = false;
    }
    if($("#selType").val()== '') {
        $("#validateNewEventDialog").css("display","block");
        $("#validateType").css("display","block");
        validate = false;
    }
    return validate;
}

function validateEditEventDialog(){
    var validate = true;
    $("#validateEditEventDialog").css("display","none");
    $("#validateTypeEd").css("display","none");
    if($("#selTypeEd").val()== '') {
        $("#validateEditEventDialog").css("display","block");
        $("#validateTypeEd").css("display","block");
        validate = false;
    }
    return validate;
}

function validateObservationsDialog(){
    var validate = true;
    $("#validateObservationsDialog").css("display","none");
    $("#validateHost").css("display","none");
    $("#validateComments").css("display","none");
    $("#validateStrategy").css("display","none");
    if($("#txtHost").val()== '') {
        $("#validateObservationsDialog").css("display","block");
        $("#validateHost").css("display","block");
        validate = false;
    }
    if($("#txtComments").val()== '') {
        $("#validateObservationsDialog").css("display","block");
        $("#validateComments").css("display","block");
        validate = false;
    }
    if($("#txtStrategy").val()== '') {
        $("#validateObservationsDialog").css("display","block");
        $("#validateStrategy").css("display","block");
        validate = false;
    }
    return validate;
}

function showMessages(type){
    if(type=="success") {
        $('#successMsg').show("slide", { direction: "up" }, 1000);
        setTimeout(function(){
            $('#successMsg').hide("slide", { direction: "up" }, 1000);

        },3000);
    } else {
        $('#errorMsg').show("slide", { direction: "up" }, 1000);
        setTimeout(function(){
            $('#successMsg').hide("slide", { direction: "up" }, 1000);
        },3000);
    }
}

function show_ajax_loader(){
    $("#loader").css('display', 'block');
}

function hide_ajax_loader(){
	$("#loader").css( 'display', 'none' );
}