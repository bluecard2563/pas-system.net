// JavaScript Document
var selectedManagers = new Array();
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmUpdateDocumentType").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameDocumentType: {
				required: true,
				textOnly: true,
				maxlength: 30,
				remote: {
						url: document_root+"rpc/remoteValidation/document/checkNameDocumentType.rpc",
						type: "post",
						data: {
							serial_doc: function() {
							return $("#hdnSerial_doc").val();
						  }
						}
					}
				},
				selZone0: {
					required: true
				},
				selCountry0: {
					required: true
				},
				selType0: {
					required: true
				},
				selPurpose0: {
					required: true
				}
		},
		messages: {
			txtNameDocumentType: {
				required: "El campo 'Tipo de Documento' es obligatorio.",
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Tipo de Documento'.",
				maxlength: 'El tipo de documento debe tener m&aacute;ximo 30 caracteres.',
				remote: 'El tipo de documento ingresado ya existe'
			}
		}
	});
	/*END Validation*/

    $('#selZone0').change(function(){
        loadCountries($('#selZone0').val(),0);
		//validateGroup('0');
    });
    $('.cou_arr').each(function(){
        selectedManagers[this.id] = this.value;
    });
	$('#selManager0').bind('change', function(){
		validateGroup('0');
	});
	$('#selType0').bind('change', function(){
		validateGroup('0');
	});
	$('#selPurpose0').bind('change', function(){
		validateGroup('0');
	});
	
    $('#btnAddCountry').bind('click', function(){
        var count = parseInt($('#managersCount').val())+1;
        var zone = $('#zoneWrapper').html();
        var type = $('#typeWrapper').html();
        var country = '<div class="span-3 label">* Pa&iacute;s:</div>\n\
						<div class="span-5" id="countryContainer'+count+'">\n\
							<select name="selCountry'+count+'" id="selCountry'+count+'" title=\'El campo "Pa&iacute;s" es obligatorio\'>\n\
								<option value="">- Seleccione -</option>\n\
							</select>\n\
						</div>';
		var purpose=$('#purposeWraper').html();
		
        $('#countriesContainer').append('\n\
				<div id="manager'+count+'" class="span-24 last line upperLine">\n\
					<div class="span-24 last line" id="hola" >\n\
						<div class="prepend-5 span-6">'+zone+'</div>\n\
						<div class="span-9 append-4 last">'+country+'</div>\n\
					</div>\n\
				<div class="span-24 last line">\n\
					<div class="prepend-4 span-3 label">* Representante:</div>\n\
					<div class="span-5" id="managerContainer'+count+'">\n\
						<select name="selManager'+count+'" id="selManager'+count+'" title="El campo Representante es obligatorio.">\n\
							<option value="">- Seleccione -</option>\n\
						</select>\n\
					</div>\n\
				</div>\n\
					<div class="span-24 last line" >\n\
						<div class="prepend-5 span-6">'+type+'</div>\n\
						<div class="stand-9 append-3">'+purpose+'</div>\n\
						<div class="span-2 last">\n\
							<a href="#" onclick="deleteManager('+count+'); return false;" id="delete'+count+'">Eliminar</a>\n\
						</div>\n\
					</div>\n\
				</div>');
		$('#zoneContainer0', $('#manager'+count)).attr('id','zoneContainer'+count);
		$('#zoneContainer'+count).attr('class', 'span-3 last');
		$('#selZone0', $('#zoneContainer'+count)).attr('name','selZone'+count);
		$('#selZone0', $('#zoneContainer'+count)).attr('id','selZone'+count);
        $('#selZone'+count).removeAttr("disabled");
        $('#selZone'+count+' option[value=""]').attr("selected",true);
		$('#selZone'+count).bind('change',function(){
			loadCountries($('#selZone'+count).val(),count);
		})
		
		$('#typeContainer0', $('#manager'+count)).attr('id','typeContainer'+count);
		$('#typeContainer'+count).attr('class','span-3 last');
		$('#selType0', $('#typeContainer'+count)).attr('name','selType'+count);
		$('#selType0', $('#typeContainer'+count)).attr('id','selType'+count);
        $('#selType'+count).removeAttr("disabled");
        $('#selType'+count+' option[value=""]').attr("selected",true);

		$('#purposeContainer0', $('#manager'+count)).attr('id','purposeContainer'+count);
		$('#selPurpose0', $('#purposeContainer'+count)).attr('name','selPurpose'+count);
		$('#selPurpose0', $('#purposeContainer'+count)).attr('id','selPurpose'+count);
		$('#selPurpose'+count).removeAttr("disabled");
		$('#selPurpose'+count+' option[value=""]').attr("selected",true);
		$('#manager'+count).append('<input type="hidden" name="hdnUnique'+count+'" id="hdnUnique'+count+'" value="" title="Por favor llene todos los datos. ('+(count+1)+')"');
        $('#managersCount').val(count);

		$("#hdnUnique"+count).rules("add", {
			 remote: {
						url: document_root+"rpc/remoteValidation/document/checkRepeatedDocument.rpc",
						type: "post",
						data: {
							serial_man: function(){
								return $('#selManager'+count).val();
							},
							type_dbm: function (){
								return $('#selType'+count).val();
							},
							purpose_dbm: function(){
								return $('#selPurpose'+count).val();
							}
						}
					},
			messages:{
					remote: 'Ya existe un documento para el tipo de persona seleccionada.'
			}
		});
		
		$("#hdnUnique"+count).rules("add", {
			 required: true
		});
		
		$('#selManager'+count).bind('change', function(){
			validateGroup(count);
		});
		$('#selType'+count).bind('change', function(){
			validateGroup(count);
		});
		$('#selPurpose'+count).bind('change', function(){
			validateGroup(count);
		});
    });

	$('[id^="hdnUnique"]').each(function(){
		var id=$(this).attr('serial');
		$(this).rules("add", {
			 remote: {
						url: document_root+"rpc/remoteValidation/document/checkRepeatedDocument.rpc",
						type: "post",
						data: {
							serial_man: function(){
								return $('#selManager'+id).val();
							},
							type_dbm: function (){
								return $('#selType'+id).val();
							},
							serial_dbm:function(){
								return $('#hdnSerial_dbm'+id).val();
							},
							purpose_dbm: function(){
								return $('#selPurpose'+id).val();
							}
						}
					},
			required: true,
			messages:{
					remote: 'Ya existe un documento para el tipo de persona seleccionada.'
			}
	});
		
		$('#selManager'+id).bind('change', function(){
			validateGroup(id);
		});
		$('#selType'+id).bind('change', function(){
			validateGroup(id);
		});
		$('#selPurpose'+id).bind('change', function(){
			validateGroup(id);
		});
	});
	/*END VALIDATION SECTION*/
});

function loadCountries(zoneId,count){
    if(zoneId==""){
        $("#selCountry option[value='']").attr("selected",true);
    }
    $('#countryContainer'+count).load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
		$('#countryContainer'+count+' #selCountry').attr('name','selCountry'+count);
        $('#countryContainer'+count+' #selCountry').attr('id','selCountry'+count);
		$('#countryContainer'+count+' #selCountry'+count).bind('change', function(){
			loadManagers($('#selCountry'+count).val(),count)
		});
		$('#managerContainer'+count).html('\n\
			<select name="selManager'+count+'" id="selManager'+count+'" title="El campo Representante es obligatorio.">\n\
				<option value="">- Seleccione -</option>\n\
			</select>');
    });
}

function loadManagers(countryId,count){
    if(countryId==""){
        $("#selManager option[value='']").attr("selected",true);
    }
    $('#managerContainer'+count).load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryId, manager_only:'YES'},function(){
        $('#managerContainer'+count+' #selManager').attr('name','selManager'+count);
        $('#managerContainer'+count+' #selManager').attr('id','selManager'+count);
        $('#managerContainer'+count+' #selManager'+count).attr('onchange','addSelectedManager(this.value,'+count+')');
		$('#managerContainer'+count+' #selManager'+count).bind('change', function(){
			if($('#hdnUnique'+count).val() != ''){
				$('#hdnUnique'+count).val(parseInt($('#hdnUnique'+count).val())+1);
			}else{
				$('#hdnUnique'+count).val('0');
			}
		});
		addSelectedManager($('#selManager'+count).val(),count);
    });
}


function deleteManager(count){
    $('#manager'+count).remove();
    selectedManagers[count] = '';
}

function addSelectedManager(managerId,count){
    var exist = 0;
    if(managerId != ''){
        for(i = 0; i<selectedManagers.length; i++){
			if(selectedManagers[i] == managerId){
                alert('Este representante ya ha sido asignado.');
                $('#managerContainer'+count+' #selManager'+count+' option[value=""]').attr("selected",true);
                exist = 1;
                break;
            }
        }
        if(exist == 0){
            selectedManagers[count] = managerId;
        }else{
          selectedManagers[count] = '';
        }
    }else{
        selectedManagers[count] = '';
    }
}

function validateGroup(id){
	var manager=$('#selManager'+id).val();
	var type=$('#selType'+id).val();
	var purpose=$('#selPurpose'+id).val();

	if(manager!='' && type!='' && purpose!=''){
		$('#hdnUnique'+id).val($('#hdnUnique'+id).val()+1);
	}else{
		$('#hdnUnique'+id).val('');
	}
}