// JavaScript Document
$(document).ready(function(){				 	
	/*VALIDATION SECTION*/
	$("#frmSearchDocumentType").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameDocumentType: {
				required: true
			}		
		}
	});
	/*END VALIDATION SECTION*/
	
	var ruta=document_root+"rpc/autocompleters/loadDocumentTypes.rpc.php";
	$("#txtNameDocumentType").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		$("#serial_doc").val(row[1]);
	});
	
});