$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	$("#frmDispatchCreditNotes").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
                        selCountry: {
                            required: true
                        },
                        selCity: {
                            required: true                            
                        },
                        selNotesFor: {
                            required: true                            
                        },
                        selZone: {
                            required: true                            
                        },
                        selectedCreditNotes: {
				required: true
			}

		},
		messages: {

   		}
	});
	/*END VALIDATION*/
        $('#selZone').bind("change",function(){
            loadCountries($('#selZone').val());
            loadCities('');
            loadDealers('');
            $('#selNotesFor').val('');
            $('#creditNotesContainer').html('');
        });
        $('#selNotesFor').bind("change",function(){
            notesFor($('#selNotesFor').val());
            $('#creditNotesContainer').html('');
            $('#selCustomer').val('');
            $('#selDealer').val('');
        });
});


/*
 * @Name: loadCountry
 * @Params: zoneID
 * @Description: Load all the countries for a specific zone.
 */
function loadCountries(zoneID){
        $('#countryContainer').load(document_root+"rpc/loadCNotes/loadCreditNoteCountries.rpc",{serial_zon: zoneID, charge_country:0},function(){
            $('#selCountry').change(function(){
              loadCities($(this).val());
              loadDealers('');
              $('#selNotesFor').val('');
              $('#creditNotesContainer').html('');
            });
            if($("#selCountry option").size() <=2 ){
                    loadCities($('#selCountry').val());
            }
        });
}

/*
 * @Name: loadCity
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCities(countryID){
    $('#cityContainer').load(document_root+"rpc/loadCNotes/loadCreditNoteCities.rpc",{serial_cou: countryID},function(){
        $('#selCity').bind('change',function(){
           loadDealers($(this).val());
           $('#selNotesFor').val('');
           $('#creditNotesContainer').html('');
        });
        if($("#selCity option").size() <=2 ){
                loadDealers($("#selCity").val());
        }
    });
}

function loadDealers(cityID) {
    $('#dealerContainer').load(document_root+"rpc/loadCNotes/loadDealersByCity.rpc",{serial_cit: cityID},function(){
        $('#selDealer').bind('change',function(){
           loadCreditNotes($(this).val(),'dea');
        });
    });
    $('#customerContainer').load(document_root+"rpc/loadCNotes/loadCustomersByCity.rpc",{serial_cit: cityID},function(){
        $('#selCustomer').bind('change',function(){
           loadCreditNotes($(this).val(),'cus');
        });
    });
}

function loadCreditNotes(serial,cnfor) {
    $('#creditNotesContainer').load(document_root+"rpc/loadCNotes/loadCreditNotes.rpc",{serial: serial, cnfor: cnfor},
    function(){
        if($("#selAll")){
                $("#selAll").bind("change",checkAll);
                 $(".creditNotesChk").each(function(i){
                    $(this).bind('click',function(){
                       cChecked();
                    });
                });
        }
        if($('#creditNotesTable').html()){
           //Paginator
            pager = new Pager('creditNotesTable', 5 , 'Siguiente', 'Anterior');
            pager.init(7); //Max Pages
            pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
            pager.showPage(1); //Starting page
        }
    });
}

function notesFor(value) {
    if(value=='DEALER'){
       $('#dealerContainer').show();
       $('#customerContainer').hide();
    }else if(value=='CUSTOMER'){
       $('#dealerContainer').hide();
       $('#customerContainer').show();
    }else{
       $('#dealerContainer').hide();
       $('#customerContainer').hide();
    }
}

//checkAll
//select\deselect all the sales
function checkAll() {
        var nodoCheck = document.getElementsByTagName("input");
        var varCheck = document.getElementById("selAll").checked;
        for (i=0; i<nodoCheck.length; i++){
                if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "selAll" && nodoCheck[i].disabled == false) {
                        nodoCheck[i].checked = varCheck;
                }
        }
        cChecked();
}
//cChecked
//checks if theres is at least one option choosen
function cChecked(){
     var checked=0;
    $(".creditNotesChk").each(function(i){
            if($(this).is(':checked')){
                checked=1;
                return;
            }
    });
    if(checked==1){
        $('#selectedCreditNotes').val('1');
    }else{
        $('#selectedCreditNotes').val('');
    }
}