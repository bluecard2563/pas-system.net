// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmUpdateSector").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {				
			txtCodeSector: {
				required: true,
				maxlength: 10,
				remote: {
					url: document_root+"rpc/remoteValidation/sector/checkCodeSector.rpc",
					type: "post",
					data: {
						serial_sec: function() {
							return $("#hdnSerial_sec").val();
					  	},
						serial_cit: function() {
							return $("#hdnSerial_cit").val();	
						}
					}
				}
			},
			txtNameSector: {
				required: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/sector/checkNameSector.rpc",
					type: "post",
					data: {
						serial_sec: function() {
							return $("#hdnSerial_sec").val();
					  	},
						serial_cit: function() {
							return $("#hdnSerial_cit").val();	
						}
					}
				}
			}	
		},
		messages: {
			txtCodeSector: {
				required: "El campo 'Codigo del sector' es obligatorio",
				maxlength: 'El nombre debe tener m&aacute;ximo 10 caracteres.',
				remote: 'El C&oacute;digo del sector ingresada ya existe en la ciudad seleccionada.'
			},
			txtNameSector: {
				required: "El campo 'Nombre del sector' es obligatorio",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: 'El Sector ingresado ya existe en la ciudad seleccionada.'
			}
		}
	});
	/*END VALIDATION SECTION*/
});