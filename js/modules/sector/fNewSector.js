// JavaScript Document
$(document).ready(function(){
	$('#selZone').bind("change",function(){
            loadCountry(this.value);
            loadCity('');
        });
	$("#txtCodeSector").bind("change",changeFlagCode);
	$("#txtNameSector").bind("change",changeFlagName);
	
	/*VALIDATION SECTION*/
	$("#frmNewSector").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtCodeSector: {
				required: true,
				maxlength: 10
			},
			txtNameSector: {
				required: true,
				maxlength: 30
			},
			hdnFlagCodeSector: {
				remote: {
					url: document_root+"rpc/remoteValidation/sector/checkCodeSector.rpc",
					type: "post",
					data: {
						serial_cit: function() {
							return $("#selCity").val();	
						},
						txtCodeSector: function() {
							return $("#txtCodeSector").val();	
						}
					}
				}
			},
			hdnFlagNameSector: {
				remote: {
					url: document_root+"rpc/remoteValidation/sector/checkNameSector.rpc",
					type: "post",
					data: {
						serial_cit: function() {
							return $("#selCity").val();	
						},
						txtNameSector: function() {
							return $("#txtNameSector").val();	
						}
					}
				}
			}
			
		},
		messages: {
			selZone: {
				required: "El campo 'Zona' es obligatorio"
			},
			selCountry: {
				required: "El campo 'Pa&iacute;s' es obligatorio"
			},
			selCity: {
				required: "El campo 'Ciudad' es obligatorio"
			},
			txtCodeSector: {
				required: "El campo 'Codigo del sector' es obligatorio",
				maxlength: 'El nombre debe tener m&aacute;ximo 10 caracteres.'
			},
			txtNameSector: {
				required: "El campo 'Nombre del sector' es obligatorio",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.'
			},
			hdnFlagCodeSector: {
				remote: 'El C&oacute;digo del sector ingresado ya existe en la ciudad seleccionada.'
			},
			hdnFlagNameSector: {
				remote: 'El Sector ingresado ya existe en la ciudad seleccionada.'
			}
		}
	});
	/*END VALIDATION SECTION*/
});

function loadCountry(zoneID){
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",
								{serial_zon: zoneID, charge_country:0},
								function(){
									$('#selCountry').bind("change",function(){
                                                                            loadCity($('#selCountry').val());
                                                                        });
                                                                        if($("#selCountry option").size() <=2 ){
                                                                            loadCity($("#selCountry").val());
                                                                        }
								});
}

function loadCity(countryID){
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}

function changeFlagCode(){
	$('#hdnFlagCodeSector').val($('#hdnFlagCodeSector').val()+1);
}
function changeFlagName(){
	$('#hdnFlagNameSector').val($('#hdnFlagNameSector').val()+1);
}