// JavaScript Document
$(document).ready(function(){				 	
	/*VALIDATION SECTION*/
	$("#frmSearchSector").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selSector: {
				required: true
			}		
		}
	});
	/*END VALIDATION SECTION*/
        $('#selZone').change(function(){
            loadCountries($('#selZone').val());
            loadCities('');
            loadSector('');
        });
	
});

function loadSector(cityID){ 
    if(cityID==""){
            $("#selSector option[value='']").attr("selected",true);
    }

    $('#sectorContainer').load(document_root+"rpc/loadSectors.rpc",{serial_cit: cityID});
}

function loadCountries(zoneId){
    if(zoneId==""){
            $("#selCountry option[value='']").attr("selected",true);
    }
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
        $('#selCountry').change(function(){loadCities($('#selCountry').val())});

		if($("#selCountry option").size() <=2 ){
			loadCities($("#selCountry").val());
		}
    });
}

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},function(){
            $('#selCity').change(function(){loadSector($('#selCity').val())});
            if($('#selCity options').size()<=2){
                loadSector($('#selCity').val());
            }
        });
}