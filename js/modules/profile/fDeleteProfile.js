$(document).ready(function(){
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#formDeleteProfile").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selProfile: {
				required: true
			},
			selCountry: {
				required: true
			}
		}
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/
	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
		  loadProfiles(this.value);
    });
	/*END CHANGE*/
});
function loadProfiles(countryID){
	if(countryID==""){
		$("#selProfile option[value='']").attr("selected",true);
	}
	$('#profileContainer').load(document_root+"rpc/loadProfiles.rpc",{serial_cou: countryID});
}