function checkAll(optSelected) {
	//check if the parent is checked or not
	var parentIsChecked=$('[parent_check="'+optSelected+'"]').attr('checked');
	//get each child and check or uncheck depending on parentIsChecked
	$('[parent_key="'+optSelected+'"]').each(function(){
		if(parentIsChecked){
			$(this).attr('checked', true);
		}else{
			$(this).attr('checked', false);
		}
	});
}

$(document).ready(function(){
	$('.checkAllOpt').each(function(){
		$(this).bind("click", function(e){
			 checkAll($(this).attr('parent_check'));
        });
	});
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmUpdateProfile").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtProfileName: {
				required: true,
				textOnly: true,
				remote: {
					url: document_root+"rpc/remoteValidation/profile/checkProfileName.rpc",
					type: "POST",
					data: {
					  serial_pbc: function() {
						return $("#hddSerialProfile").val();
					  },
                                          serial_cou: function() {
							return $('#hdnSerial_cou').val();
						}
					}
				}
			},
			selStatus: {
				required: true
			}
		},
			messages: {
				txtProfileName: {
					textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
					remote: "El Nombre de Perfil ingresado ya existe en el sistema."
				}
			},
			submitHandler: function(form) {
				show_ajax_loader();
				$('#btnInsert').attr('disabled', 'true');
				$('#btnInsert').val('Espere por favor...');
				form.submit();
			}
		});
		/*FIN DE LA SECCIÓN DE VALIDACIONES*/
});

