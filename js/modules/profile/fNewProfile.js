//JavaScript Document

function checkAll(optSelected) {
	//check if the parent is checked or not
	var parentIsChecked=$('[parent_check="'+optSelected+'"]').attr('checked');
	//get each child and check or uncheck depending on parentIsChecked
	$('[parent_key="'+optSelected+'"]').each(function(){
		if(parentIsChecked){
			$(this).attr('checked', true);
		}else{
			$(this).attr('checked', false);
		}
	});
}

$(document).ready(function(){
	$('.checkAllOpt').each(function(){
		$(this).bind("click", function(e){
			 checkAll($(this).attr('parent_check'));
        });
	});
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmNewProfile").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtProfileName: {
				required: true,
				textOnly: true
			},
                        hidenName: {
				remote: {
					url: document_root+"rpc/remoteValidation/profile/checkProfileName.rpc",
					type: "POST",
                                        data: {
						serial_cou: function() {
							return $('#selCountry').val();
						},
                                                txtProfileName: function() {
							return $('#txtProfileName').val();
						}
					}
				}
                        },
			selCountry: {
				required: true
				},
			selStatus: {
				required: true
				}
			},
			messages: {
				txtProfileName: { 
					textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
				},
                                hidenName: {
					remote: "El perfil ingresado ya existe para el pa&iacute;s seleccionado."
				}
			},
			submitHandler: function(form) {
				show_ajax_loader();
				$('#btnInsert').attr('disabled', 'true');
				$('#btnInsert').val('Espere por favor...');
				form.submit();
			}
		});
		/*FIN DE LA SECCIÓN DE VALIDACIONES*/
        $("#selCountry").bind("change", function(e){
              changeValue();
        });
        $("#txtProfileName").bind("change", function(e){
              changeValue();
        });
});

function changeValue(){
    $('#hidenName').val($('#hidenName').val()+1);
}