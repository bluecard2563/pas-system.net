//Java Script
var type;
var pager;
$(document).ready(function(){
	$("[name='user_type']").click(function(){
			type=$(this).val();
			if(type=='PLANET'){
				loadPlanetUsers();
			}
			else{
				$("#selZone option[value='']").attr("selected",true);
				$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#txtPercentage').val('');
				$('#planetUsersContainer').hide();
				$('#dataContainer').show();
				$('#percentageData').show();
				$("#selZone").rules('add',{
					required: true
				});
				$("#selCountry").rules('add',{
					required: true
				});
				$("#selManager").rules('add',{
					required: true
				});
				if($("#selResponsible").length>0){
					$("#selResponsible").rules('add',{
						required: true
					});
				}
				//$("#userTBL").rules("remove");
				$("#txtPercentage").rules('add',{
					required: true,
					percentage: true,
					max_value: 100,
						 messages: {
						   percentage: "Solo debe ingresar un valor tipo Porcentaje",
						   max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
						 }
				});
			}
			
	});
	
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignCommission").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules:{
			user_type:{
				required: {
							depends: function(element) {
									var display = $("#userType").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			},
			selZone:{
				required: {
							depends: function(element) {
									var display = $("#dataContainer").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			},
			selCountry:{
				required: {
							depends: function(element) {
									var display = $("#dataContainer").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			},
			selManager:{
				required: {
							depends: function(element) {
									var display = $("#dataContainer").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			},
			selResponsible:{
				required: {
							depends: function(element) {
									var display = $("#dataContainer").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}
			},
			txtPercentage:{
				required: {
							depends: function(element) {
									var display = $("#dataContainer").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							},
				percentage: {
							depends: function(element) {
									var display = $("#dataContainer").css('display');
									if(display=='none')
									return false;
									else
									return true;
								}
							}

			}

		},
		 messages: {
				txtPercentage:{
					percentage: "Solo debe ingresar un valor tipo Porcentaje"
				}
		 }
	});

	//LOADS
	$("#selZone").bind("change",function(){loadCountry(this.value);});

});

function loadCountry(zoneID){
	if(zoneID==""){
				$("#selCountry option[value='']").attr("selected",true);
				$("#selManager option[value='']").attr("selected",true);
				$("#selResponsible option[value='']").attr("selected",true);
				loadManagersByCountry($('#selCountry').val());
		}
		$("#selManager option[value='']").attr("selected",true);
		$("#selResponsible option[value='']").attr("selected",true);
		$('#countryContainer').load(
				document_root+"rpc/loadCountries.rpc",
				{serial_zon: zoneID},
				function(){
					if($('#selCountry ').find('option').size()==2){
						loadManagersByCountry($('#selCountry').val());
					}
				   $("#selCountry").bind("change", function(e){loadManagersByCountry(this.value);});

		});
}

function loadManagersByCountry(countryID){
    if(countryID==""){
            $("#selManager option[value='']").attr("selected",true);
			$("#selResponsible option[value='']").attr("selected",true);
			loadUsers($('#selManager').val());
    }
	$("#selResponsible option[value='']").attr("selected",true);
    $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
		if($('#selManager').find('option').size()==2){
			 loadUsers($('#selManager').val());
		}
        $('#selManager').change(function(){
            loadUsers(this.value);
        });
    });


}

function loadUsers(mbcID){
	//var paUser=NULL;
	if(mbcID==""){
            $("#selResponsible option[value='']").attr("selected",true);
    }

	$('#responsibleContainer').load(document_root+"rpc/loadUsersForAssignPercentage.rpc",{serial_mbc: mbcID, general:'YES'},function(){
        $('#selResponsible').change(function(){

        });
    });
}

function loadPlanetUsers(){
	$("#selZone option[value='']").attr("selected",true);
	$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	if($('#selResponsible').length>0){
		$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
	$('#txtPercentage').val('');
	$('#planetUsersContainer').show();
	$('#dataContainer').hide();
	//cargar el RPC
	$('#planetUsersContainer').load(document_root+"rpc/loadUsersForAssignPercentage.rpc",{type: type, general:'YES'},function(){
		if($('#tblUserForAssignPercentage').length>0){
			pager = new Pager('tblUserForAssignPercentage', 10 , 'Siguiente', 'Anterior');
			pager.init($('#hdnPages').val()); //Max Pages
			pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
			pager.showPage(1); //Starting page
		}
		$("#selZone").rules("remove");
		$("#selCountry").rules("remove");
		$("#selManager").rules("remove");
		$("#selResponsible").rules("remove");
		$('#planetUsersContainer').show();
		$('#percentageData').show();
		$("#userTBL").rules('add',{
					required: true
				});
		$("#txtPercentage").rules('add',{
					required: true,
					percentage: true,
					max_value: 100,
						 messages: {
						   percentage: "Solo debe ingresar un valor tipo Porcentaje",
						   max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
						 }
				});
    });

}