var currentTable;
$(document).ready(function(){
	$("#frmWebSales").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			action: {                            
				required: true
			}
		}
	});
	
	if($('#salesTable').length>0){
		/*pager = new Pager('salesTable', 10 , 'Siguiente', 'Anterior');
		pager.init($('#hdnTotalPages').val()); //Max Pages
		pager.showPageNav('pager', 'alertPageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page*/
		currentTable = $('#salesTable').dataTable( {
									"bJQueryUI": true,
									"sPaginationType": "full_numbers",
									"oLanguage": {
										"oPaginate": {
											"sFirst": "Primera",
											"sLast": "&Uacute;ltima",
											"sNext": "Siguiente",
											"sPrevious": "Anterior"
										},
										"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
										"sZeroRecords": "No se encontraron resultados",
										"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
										"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
										"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
										"sSearch": "Filtro:",
										"sProcessing": "Filtrando.."
									},
								   "sDom": 'T<"clear">lfrtip',
								   "oTableTools": {
									  "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
									  "aButtons": ["pdf" ]
								   }

				} );
	}

	$('#btnResale').bind('click', function(){
		registerForm('RESALE');
	});

	$('#btnRemove').bind('click', function(){
		registerForm('DELETE');
	});
});

function registerForm(action){
	if(one_required()){
		$('#action').val(action);
	}else{
		$('#action').val('');
	}	
	
	if($("#frmWebSales").valid()){
		$('#btnResale').attr('disabled', 'true');
		$('#btnRemove').attr('disabled', 'true');
		var sData = $('input', currentTable.fnGetNodes()).serialize();
		$('#selectedRows').val(sData);
		$("#frmWebSales").submit();
	}
}

function one_required(){
	var one_required = false;
	
	$('input', currentTable.fnGetNodes()).each(function(){
		if($(this).is(':checked')){
			one_required=true;
			return;
		}
	});
	
	return one_required;
}