// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewCupon").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selCountry: {
                    required: true
                },
                selCustomerPromo: {
                    required: true
                },
				txtNumberToCreate: {
					required: true,
					digits: true,
					min: 1
				}
            },
			messages: {
				txtNumberToCreate: {
					digits: "El campo 'N&uacute;mero de Cupones a Crear' admite s&oacute;lo d&iacute;gitos.",
					min: "El campo 'N&uacute;mero de Cupones a Crear' debe ser mayor a 1 "
				}
			},
			submitHandler: function(form){
				$('#btnInsert').val('Espere por favor...');
				$('#btnInsert').attr('disabled','true');

				form.submit();
			}
    });
    /*END VALIDATION SECTION*/

    $('#selCountry').bind("change",function(){
       loadCustomerPromoByCountry($(this).val());
    });
});

function loadCustomerPromoByCountry(serial_cou){
	$('#promoContainer').load(document_root + 'rpc/loadCupon/loadCountriesWithPromo.rpc', {serial_cou: serial_cou}, function(){
		$('#selCustomerPromo').bind('change', function(){
			loadPromoInfo($(this).val());
		});

		if($('#selCustomerPromo :option').length == 2){
			loadPromoInfo($('#selCustomerPromo').val());
		}
	});
}

function loadPromoInfo(serial_ccp){
	$('#infoContainer').load(document_root + 'rpc/loadCupon/loadPromoInfo.rpc', {serial_ccp: serial_ccp});
}