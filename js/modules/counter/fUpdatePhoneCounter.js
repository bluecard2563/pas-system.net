// JavaScript Document
$(document).ready(function(){
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignPhoneCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountryBranch: {
				required: true
			}
		}
	});

	/*CHANGE OPTIONS*/
	$('#selCountryBranch').bind("change", function(){
		$('#userContainer').load(document_root+"rpc/counter/loadCountersByCountry.rpc", {serial_dea: this.value},function(){
			$("a[id^='changeStatus']").bind("click", function(){
				$.getJSON(document_root+"rpc/counter/changePhoneCounterStatus.rpc", {serial_usr: $(this).attr("serial_usr"), status_cnt:$(this).attr("value")},function(data){
					if(data){
						window.location = document_root+"modules/counter/fUpdatePhoneCounter/1";
					} else {
						window.location = document_root+"modules/counter/fUpdatePhoneCounter/2";
					}
				});
			});
		});
	});
	/*END CHANGE*/
});