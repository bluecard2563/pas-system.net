// JavaScript Document
$(document).ready(function(){
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/	
	$("#frmAssignCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selDealer: {
				required: true
			},
			selBranch: {
				required: true
			},
                        selUser: {
				required: true
			},
			txtCounterData:{
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/counter/checkCounterExists.rpc",
					type: "post",
					data: {
					  serial_usr: function() {
						return $("#hdnUserID").val();
					  }
					}
				}
			}
		},
		messages: {
			txtCounterData: {
				remote: "El usuario seleccionado ya consta como counter."
			}
		}
	});
	
	/*DYNAMIC AUTOCOMPLETE*/
	var ruta=document_root+"rpc/autocompleters/loadUserForCounter.rpc";
	$("#txtCounterData").autocomplete(ruta,{
		extraParams: {
			serial_dea: function() { return $("#selBranch").val(); }
                },
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 3,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		if(row[0]=="Counter no encontrado"){
			$("#txtCounterData").val('');
		}
		$("#hdnUserID").val(row[1]);
	});
	/*END AUTOCOMPLETE*/
	
	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
           // $('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
            $('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
            $('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
            cleanHistory();
			loadCities(this.value);
        });


	/*END CHANGE*/
});

/*function loadCountry(zoneID){
        if(zoneID!=""){
            $('#countryContainer').load(
                    document_root+"rpc/loadCountries.rpc",
                    {serial_zon: zoneID},
                    function(){
                            $("#selCountry").bind("change", function(e){loadDealer(this.value);}
                    );
            });
        }else{
            $('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
            $('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
            cleanHistory();
        }
}*/

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},function(){
		$("#selCity").bind("change", function(e){loadDealer(this.value);})
		if($('#selCity').find('option').size()==2){
			loadDealer($('#selCity').val());
		}
	});
}

function loadDealer(cityID){
    $('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
    $('#selUser').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
    cleanHistory();
    
    if(cityID==""){
        cityID='stop';
    }

    $('#dealerContainer').load(
            document_root+"rpc/loadDealers.rpc",
            {serial_cit: cityID,
            dea_serial_dea: 'NULL'},
            function(){
                    $("#selDealer").bind("change", function(e){loadBranch(this.value);});
                    
                    if($('#selDealer').find('option').size()==2){
                        loadBranch($('#selDealer').val());
                    }
            
            }
    );
}

function loadBranch(dealerID){
    if(dealerID==""){
        dealerID='stop';
        loadUsers(dealerID);
    }
    cleanHistory();
	
    $('#branchContainer').load(
            document_root+"rpc/loadDealers.rpc",
            {dea_serial_dea: dealerID},
            function(){
                    $("#selBranch").bind("change", function(e){loadUsers(this.value);});
                    if($('#selBranch').find('option').size()==2){
                        loadUsers($('#selBranch').val());
                    }
            }
    );
            
}

function loadUsers(dealerID){
    if(dealerID==""){
        dealerID='stop';
    }
    $('#userContainer').load(
            document_root+"rpc/loadUserForCounter.rpc",
            {serial_dea: dealerID}
    );
}

function cleanHistory(){
    $("#hdnUserID").val('');
    $("#txtCounterData").val('');
    $("#txtCounterData").flushCache();
}