// JavaScript Document
function initializeCounterSearchForm(){
	/*VALIDATION SECTION*/	
	$("#frmSearchCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCounterData: {
                            required: true
			},
                        selCountry: {
                            required: true
			},
                        selDealer: {
                            required: true
			},
                        selBranch: {
                            required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	var ruta=document_root+"rpc/autocompleters/loadCounter.rpc";
	$("#txtCounterData").autocomplete(ruta,{
                extraParams: {
                    serial_dea: function(){
                        return $('#selBranch').val();
                    }
                },
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		//$("#hdnCounterID").val(row[1]);
                if(row[0]=="No existe un counter con esos datos." || row[0]=="Defina un filtro a aplicarse."){
                    $("#txtCounterData").val('');
		}
		$("#hdnCounterID").val(row[1]);
	});

    $("#selCountry").bind("change", function(e){
        $('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
        cleanHistory();
        loadCities(this.value);
		//loadDealer(this.value);
    });
	
}

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},
	function(){
		$('#selCity').bind("change", function(e){loadDealer(this.value);});
		if($('#selCity option').size()<=2){
			loadDealer($('#selCity').val());
		}
	});
}

function loadDealer(cityID){
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	cleanHistory();

	if(cityID!=''){
		$('#dealerContainer').load(
				document_root+"rpc/loadDealers.rpc",
					{serial_cit: cityID,
					dea_serial_dea: 'NULL'},
						function(){
							$("#selDealer").bind("change", function(e){loadBranch(this.value);});
							if($('#selDealer option').size()<=2){
								loadBranch($('#selDealer').val());
							}
						}
        );
	}
}

function loadBranch(dealerID){
	cleanHistory();
    if(dealerID==''){
        dealerID='stop';
    }
	$('#branchContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{dea_serial_dea: dealerID}, function(e){
			$('#selBranch').change(function(e){
				loadCounters($('#selBranch').val());
			});
			if($('#selBranch').val() != ''){
				loadCounters($('#selBranch').val());
			}
		}
	);
}

function loadCounters(branchId){
	$('#countersContainer').html('Cargando resultados...');
	
	$('#countersContainer').load(document_root+"rpc/counter/loadCountersByBranch.rpc",
            {serial_dea: branchId},
             	function(){
            		$("[id^='goEdit']").each(function(e){
            			$(this).click(function(e){
            				serial = $(this).attr("my_serial");
                			$('#hdnCounterID').val(serial);
                			$('#frmSearchCounter').submit();
            			});
            		});
            		
	                if($("#counters_table").length>0) {
						var itemsPerPAge = 10;
						var items = parseInt($('#hdItemsCount').val());
						var pages_number = parseInt(items / itemsPerPAge) + 1;
						
	                    pager = new Pager('counters_table', itemsPerPAge , 'Siguiente', 'Anterior');
	                    pager.init(pages_number); //Max Pages
	                    pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
	                    pager.showPage(1); //Starting page
	                }
            }
	);
}

function cleanHistory(){
    $("#hdnUserID").val('');
    $("#txtCounterData").val('');
    $("#txtCounterData").flushCache();
}