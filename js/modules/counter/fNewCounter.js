// JavaScript Document
$(document).ready(function(){
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/	
	$("#frmNewCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtFirstName: {
				required: true,
				textOnly: true
			},
			txtLastName: {
				required: true,
				textOnly: true
			},
			txtDocument: {
				required: true,
                                alphaNumeric: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUserDocument.rpc",
					type:"POST"
				}
			},
			txtPhone: {
				required: true,
				number: true
			},
			txtCellphone: {
				number: true
			},
			txtBirthdate: {
				required: true,
                                date: true
			},
			txtMail: {
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUserEmail.rpc",
					type: "POST"
				}
			},
			selDealer: {
				required: true
			},
			selBranch: {
				required: true
			},
			selProfile: {
				required: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
                        txtPosition: {
				alphaNumeric: true
			},
			txtUsername: {
				required: true,
                                alphaNumeric: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUsername.rpc",
					type: "POST"
				}
			},
			txtPassword: {
				required: true,
				minlength: 6
			},
			txtRePassword: {
				required: true,
				equalTo: "#txtPassword"
			}
		},
		messages: {
			txtFirstName: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
                        },
			txtLastName: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Apellido'."
                        },
			txtDocument: {
				remote: "La identificaci&oacute;n ingresada ya existe.",
				alphaNumeric: "La 'Identificaci&oacute;n' admite s&oacute;lo caracteres alfa-num&eacute;ricos."
			},
			txtPhone: {
				number: "El campo 'Tel&eacute;fono' admite s&oacute;lo texto."
                        },
			txtCellphone: {
				number: "El campo 'Celular' admite s&oacute;lo texto."
			},
			txtBirthdate: {
				date:"El campo 'Fecha de Nacimiento' debe tener el formato mm/dd/YYYY"
			},
			txtMail: {
				email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe."
                        },
			txtPassword:{
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtRePassword: {
				equalTo: "La confirmaci&oacute;n ha fallado."
			},
			txtUsername: {
				remote: "El usuario ingresado ya existe.",
                                alphaNumeric: "El campo 'Usuario' admite s&oacute;lo caracteres alfa-num&eacute;ricos."
			},
                        txtPosition: {
				alphaNumeric: "El campo 'Cargo' admite s&oacute;lo admite caracteres alfa-num&eacute;ricos."
			},
			txtCounterData: {
				remote: "El usuario seleccionado ya consta como counter."
			}
		}
	});	
	/*FIN DE LA SECCI�N DE VALIDACIONES*/

     setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-20y');
	
	/*DYNAMIC AUTOCOMPLETE*/
	var ruta=document_root+"rpc/autocompleters/loadUserForCounter.rpc";
	$("#txtCounterData").autocomplete(ruta,{
		extraParams: {
			serial_dea: function() { return $("#selBranch").val(); }
	    },
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 3,
  		formatItem: function(row) {
			 return row[0] ;
  		}
	}).result(function(event, row) {
		if(row[0]=="Usuario no encontrado"){
			$("#txtCounterData").val('');
		}
		$("#hdnUserID").val(row[1]);
	});
	/*END AUTOCOMPLETE*/
	
	$("#selCountry").bind("change", function(e){
	      loadCities(this.value);
		  loadProfilesForCounter(this.value);
    });
	/*END CHANGE*/
});

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}

	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},
	function(){
		$('#selCity').bind("change", function(e){
			loadDealer(this.value);
		});

		if($('#selCity option').size()<=2){
			loadDealer($('#selCity').val());
		}
	});
}

function loadDealer(cityID){
	if(cityID==""){
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$("#selDealer option[value='']").attr("selected",true);
		$("#selBranch option[value='']").attr("selected",true);
		$("#hdnUserID").val('');
		$("#txtCounterData").val('');
		cityID='stop';
	}
	
	$('#dealerContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{serial_cit: cityID,
		dea_serial_dea: 'NULL'},
		function(){
			$("#selDealer").bind("change", function(e){loadBranch(this.value);});

			if($('#selDealer option').size()<=2){
				loadBranch($('#selDealer').val());
			}
		}
	);
}

function loadBranch(dealerID){
	if(dealerID==""){
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$("#selBranch option[value='']").attr("selected",true);
		$("#txtCounterData").val('');
		$("#hdnUserID").val('');
		dealerID='stop';
	}
	
	$('#branchContainer').load(
		document_root+"rpc/loadDealers.rpc",
		{dea_serial_dea: dealerID}
	);	
}

function loadProfilesForCounter(countryID){
	if(countryID==""){
		$("#selProfile option[value='']").attr("selected",true);
	}

	$('#profileContainer').load(document_root+"rpc/loadProfiles.rpc",{serial_cou: countryID, counter:'YES'});

}