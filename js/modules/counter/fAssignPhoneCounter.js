// JavaScript Document
$(document).ready(function(){
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmAssignPhoneCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountryBranch: {
				required: true
			},
			selManager: {
				required: true
			},
			selUser: {
				required: true
			}
		}
	});

	/*CHANGE OPTIONS*/
	$('#selManager').bind("change", function(){
		$('#userContainer').load(document_root+"rpc/counter/loadUsersByManagerByCountry.rpc", {serial_mbc: this.value, serial_dea: $('#selCountryBranch').val()});
	});
	$('#selCountryBranch').bind("change", function(){
		$('#userContainer').load(document_root+"rpc/counter/loadUsersByManagerByCountry.rpc", {serial_mbc: $('#selManager').val(), serial_dea: this.value});
	});
	/*END CHANGE*/
});