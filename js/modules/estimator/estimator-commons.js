//JavaScript Document
//WARNING: These functions are common to ALL places where the estimator function is called

function switchSelectedTab(tab){
	for(i=1;i<4;i++){
		if(i==tab){
			$("#tab_page_"+i).removeClass("unselectedTab");
		}else{
			$("#tab_page_"+i).addClass("unselectedTab");
		}
	}
}

//***********************************************
//*************** ESTIMATOR FIRST TAB ***********
//***********************************************

function enableEstimatorTabJavascript(useDatabase){
	//TODO FIX CORNERS FAIL
	//$('.dark-record').corner("10px");
	$('#selProduct').sSelect({ddMaxHeight: 100});
	$('#selDeparture').sSelect({ddMaxHeight: 100});
	$('#selDestination').sSelect({ddMaxHeight: 100});
	
	$('#txtBeginDate').bind('change',function(){
		calcDaysBetween($(this).val(),$('#txtEndDate').val(),1);
		loadProductProperties($('#selProduct').val());
    });
	$('#txtEndDate').bind('change',function(){
		calcDaysBetween($('#txtBeginDate').val(),$(this).val(),2);
		loadProductProperties($('#selProduct').val());
    });
	
	setDefaultCalendar($("#txtBeginDate"),'+0d','+10y','+0d');
    setDefaultCalendar($("#txtEndDate"),'+0d','+10y','+1d');
	
	$('#selProduct').bind('change',function(){
        loadProductProperties($('#selProduct').val());
    });
	
	$('#goToCartLink').bind('click',function(){
		estimatorMasterRedirect();
    });
	
	$('#selDeparture').bind('change',function(){
		$('#selProductContainer').load(document_root+"rpc/loadProductsEstimator.rpc",{serial_cou: $('#selDeparture').val()}, function(e){
			if($('#selProduct').css('display') != 'none'){
				$('#selProduct').sSelect({ddMaxHeight: 100}).change(function(){
					loadProductProperties($('#selProduct').val());
					validateDestination();
				});
			}
		});
    });
	
	$('#selDestination').bind('change',function(){
		validateDestination();
	});
}

function activateWizard(formName,submitButtonText,useDatabase){
    $("#"+formName).wizard({
        show: function(element) {
			pageNumber = $(element).attr("id").replace("page_","");
			switchSelectedTab(pageNumber);
    		switch(pageNumber){
    			case '1': //estimator page
    				break;
    			case '2': //benefits page
    				processQuotesToBenefits(useDatabase);
    				break;
    			case '3': //Shopping Cart
    				addToShoppingCart(useDatabase);
    				break;
    		}
    	},
        next:"Siguiente >",
        prev:"< Anterior",
        action:submitButtonText,
        extra:"ui-state-default ui-corner-all"
    });
}

function addValidatorsEstimatorTab(formName,onSubmit){
    $("#"+formName).validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		onsubmit : onSubmit,
    	rules: {
	    	selProduct: {
		        required: true
		    },
		    selDestination: {
		    	required: { 
		        	depends: function(element) {
	                    var boxStatus = $('#destinationHideBox').css('display');
	                    return (boxStatus == 'block');
		        	}
		    	}
		    },
		    selDeparture: {
		        required: true
		    },
		    selTripDays: {
		    	required:{ 
		        	depends: function(element) {
	                    var boxStatus = $('#executiveBoxField').css('display');
	                    return (boxStatus == 'block');
		        	}
		    	}
		    },
		    txtBeginDate: {                            
				required: true,
				dynamicDateLessThan: '#txtEndDate',
				dynamicDateWithinOneYear: '#txtEndDate'
			},
			txtEndDate: {                            
				required: true
			},
			txtOverAge: {                            
				required: { 
		        	depends: function(element) {
	                    var boxStatus = $('#adultsInput').css('display');
	                    return (boxStatus == 'block');
		        	}
		    	},
				number: true
			},
			txtUnderAge: {                            
				required:  { 
		        	depends: function(element) {
	                    var boxStatus = $('#childrenInput').css('display');
	                    return (boxStatus == 'block');
		        	}
		    	},
				number: true
			}
		},
		messages: {
			txtOverAge: {
				number: "El campo 'Viajeros mayores a 12 a&ntilde;os:' debe ser num&eacute;rico"
			},
			txtUnderAge: {
				number: "El campo 'Viajeros menores a 12 a&ntilde;os:' debe ser num&eacute;rico"
			},
			txtBeginDate: {
				dynamicDateLessThan: "La Fecha de Salida debe ser menor o igual que la Fecha de Llegada",
				dynamicDateWithinOneYear: "El rango de fechas debe ser menor a un a&ntilde;o"
			}
		}
	});
}

function addCustomSubmission(submitFunctionName){
    //SINCE WE SET VALIDATION@SUBMISSION = FALSE, WE HAVE TO DEFINE A CUSTOM SUBMISSION FUCTION:
    jQuery('form#estimatorFullForm').unbind('submit'); //REMOVE DEFAULT SUBMISSION
    jQuery('form#estimatorFullForm').bind("submit", function() {
    	selectedProds = getSelectedProducts();
    	if(!selectedProds){
    		alert('Debe seleccionar al menos un Producto para Comprar');
    		return false;
    	}else{
    		if(!confirm('Los Productos no Seleccionados no seran Guardados en el carrito, \u00BFdesea continuar?')){
    			return false;
    		}
    	}
        jQuery('form#estimatorFullForm #btnInsert').val("Espere por favor...");
        jQuery('form#estimatorFullForm #btnInsert').attr("disabled","true");
    	saveMyCart(selectedProds);
    	var ret = eval(submitFunctionName+"();");
    	return false;
    });
    
    //ASSIGN VALIDATORS TO EACH PAGE  ( REQUIRES FILE "jquery.wizard.js" MODIFCATION )
	$('#nav_next_page_1').bind('mousedown',validateEstimatorForm);
	
	//ALERT WHEN ONLY ONE ADULT [BUT ALSO A SPOUSE] IS CHECKED
	$('#nav_next_page_1').bind('mouseup',function(e){
		if($('#txtOverAge').val() < 2 && $('#checkPartner').attr('checked') == true){
			alert('Ha ingresado \u00FAnicamente un adulto, el precio no incluir\u00E1 el c\u00F3nyugue seleccionado.');
			return true;
		}
	});
}

function loadProductProperties(prodVal, selectedVal){
	$('#spouseTravelingField').css('display','none');
	$('#executiveBoxField').css('display','none');
	$('#maxOverSpan').css('display','none');
	$('#maxUnderSpan').css('display','none');
	$('#destinationHideBox').css('display','block');
	$('#destinationFixed').css('display','none');
	$('#adultsInput').css('display','block');
	$('#childrenInput').css('display','block');
	$('#adultsInputAltn').css('display','none');
	$('#childrenInputAltn').css('display','none');
	$.ajax({
		url: document_root+ 'modules/estimator/getProductEstimatorPropertiesAjax.php',
		type: "POST",
		data: { 'serial_pxc': prodVal, 'serial_cou': $('#selDeparture').val()},
		success: function(values){
			parts = values.split("%S%");
			if(parts[0] == 'true'){
				$('#spouseTravelingField').css('display','block');
			}
			parts[1]
			if(parts[1] == 'true'){
				tripDaysCalc = $('#daysDiff').html();
				isSmall = $('#isSmall').val();
				$('#estimatorTripDays').load(document_root+"rpc/estimator/loadEstimatorTripDays.rpc",{
						serial_pxc: prodVal, 
						serial_cou: $('#selDeparture').val(),
						selectedVal: selectedVal, 
						tripDaysCalc: tripDaysCalc, 
						isSmall: isSmall});
				$('#executiveBoxField').css('display','block');
			}else{
				$('#estimatorTripDays').html('<input type="text" value="" name="selTripDays" id="selTripDays"/>');
			}
			//TODO load all other constraints, not working
			if(parts[2] > 0){
				$('#maxOverSpan').html('('+parts[2]+' gratis)');
				$('#maxOverSpan').css('display','block');
			}
			
			if(parts[3] > 0){
				$('#maxUnderSpan').html('('+parts[3]+' gratis)');
				$('#maxUnderSpan').css('display','block');
			}
			
			if(parts[4] > 0){
				$('#destinationHideBox').css('display','none');
				$('#destinationFixed').css('display','block');
				$('#destinationRestricted').val('1');
			}else{
				$('#destinationRestricted').val('0');
			}
			
			switch(parts[5]){
				case 'ADULTS':
					$('#childrenInput').css('display','none');
					$('#childrenInputAltn').css('display','block');
					$('#extrasRestricted').val('ADULTS');
					break;
				case 'CHILDREN':
					$('#extrasRestricted').val('CHILDREN');
					break;
				default://BOTH
					//LEAVE THEM OK
					$('#extrasRestricted').val('BOTH');
					break;
			}
		}
	});
}

function loadEstimatorInfo(smallEstimatorData){
	parts = smallEstimatorData.split("%s%");
	$('#selProduct').val(parts[0]);
	$('#selDeparture').val(parts[1]);
	$('#selDestination').val(parts[2]);
	if(parts[3]){
		$('#checkPartner').attr('checked','checked');
	}
	$('#txtBeginDate').val(parts[5]);
	$('#txtEndDate').val(parts[6]);
	$('#txtOverAge').val(parts[7]);
	$('#txtUnderAge').val(parts[8]);
	calcDaysBetween($('#txtBeginDate').val(),$('#txtEndDate').val(),1);
	loadProductProperties($('#selProduct').val(),parts[4]);
}

function validateEstimatorForm(){
	//ASSIGN CHILDREN ZERO IF NONE:
	if($('#txtUnderAge').val() == ''){
		$('#txtUnderAge').val(0);
	}
	validationPassed = true;
	var formValid = true;
	formValid = formValid & $('#selProduct').valid();
	formValid = formValid & $('#selDeparture').valid();
	formValid = formValid & $('#selDestination').valid();
	if($('#executiveBoxField').css('display') == 'block'){formValid = formValid && $('#selTripDays').valid();}
	formValid = formValid & $('#txtBeginDate').valid();
	formValid = formValid & $('#txtEndDate').valid();
	formValid = formValid & ($('#adultsInput').css('display')=='none' || $('#txtOverAge').valid());
	formValid = formValid & ($('#childrenInput').css('display')=='none' || $('#txtUnderAge').valid());
	
	if(!formValid) {
		validationPassed = false;
	}else{
		validationPassed = true;
	}
}
    
//***********************************************
//*************** BENEFITS TAB ******************
//***********************************************

function processQuotesToBenefits(useDatabase){
	var selProduct = $('#selProduct').val();
	var selDeparture = $('#selDeparture').val();
	var selDestination = $('#selDestination').val();
	var checkPartner = $('#checkPartner').attr('checked');
	var selTripDays = $('#selTripDays').val();
	var txtBeginDate = $('#txtBeginDate').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtOverAge = $('#txtOverAge').val();
	var txtUnderAge = $('#txtUnderAge').val();
	var destinationRestricted = $('#destinationRestricted').val();
	var extrasRestricted = $('#extrasRestricted').val();
	
	$('#benefitsPageContent').html('Loading...');
	$('#benefitsPageContent').load(document_root+"rpc/estimator/loadEstimatorBenefits.rpc",{
		selProduct: selProduct,
		selDeparture: selDeparture,
		selDestination: selDestination,
		checkPartner: checkPartner,
		selTripDays: selTripDays,
		txtBeginDate: txtBeginDate,
		txtEndDate: txtEndDate,
		txtOverAge: txtOverAge,
		txtUnderAge: txtUnderAge,
		useDatabase: useDatabase,
		destinationRestricted: destinationRestricted,
		extrasRestricted: extrasRestricted}, function(e){
			$errorMessage = $("#errorMessage").val();
			if($errorMessage){
				alert("Lo sentimos, el producto no admite el rango de d\u00EDas seleccionado\nPor favor modifique los valores de la cotizaci\u00F3n.");
				window.location = document_root + 'modules/estimator/estimator';
			}else{
				loadBenefitsJavascript(useDatabase);
			}
		});
	
	validationPassed = true;
}

function loadBenefitsJavascript(useDatabase){
	//TODO FIX CORNERS FAIL
	//$('.dark-record').corner("10px");
	$("span[id^='dialogAllBenefits_']").each(function(e){
			$(this).bind("click", function(e){
				showProductBenefitsDialog($(this).attr("id"),useDatabase);
	        });
	 });
}

function bindBenefitsDialog(){
	$("#dialogBenefits").dialog({
		bgiframe: true,
		autoOpen: false,
		width: 700,
		height: 310,
		modal: true,
		buttons: {
			'Cerrar': function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  }
	});
	
	$("#dialogGeneralConditions").dialog({
		bgiframe: true,
		autoOpen: false,
		width: 900,
		height: 300,
		modal: true,
		buttons: {
			'Cancelar': function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  }
	});
}

function showProductBenefitsDialog(prodId, useDatabase){
	product = prodId.replace("dialogAllBenefits_","");
	show_ajax_loader();
	$('#productBenefitsDialogInfoContainer').load(document_root+"rpc/estimator/loadProductBenefitsDialogData.rpc",{selProduct: product, useDatabase: useDatabase});
	$("#dialogBenefits").dialog('open');
	hide_ajax_loader();
}

//***********************************************
//*************** SHOPPING CART TAB *************
//***********************************************


function addToShoppingCart(useDatabase){
	$('#shoppingCartPageContent').load(document_root+"rpc/estimator/loadShoppingCart.rpc",{useDatabase: useDatabase});
}

function loadShoppingCartScript(estimatorUrl){
	//$('.dark-record').corner("10px");
	
	 $("input[id^='cartProductSelected_']").each( function(e){
		 $(this).attr('checked','checked');
	  });
	
	$("#addAnotherProduct").bind("click", function(e){
		selectedProds = getSelectedProducts();
		if(!confirm('\u00BF Los Productos no Seleccionados no seran Guardados en el carrito, desea continuar?')){
			return;
		}
		saveMyCart(selectedProds);
		window.location = document_root + estimatorUrl;
    });
}

function getSelectedProducts(){
	selectedIds = "";
	noneSelected = true;
	$("input[id^='cartProductSelected_']").each( function(e){
		  productId = $(this).attr('id').replace("cartProductSelected_","");
		  if($(this).attr('checked')){
			  selectedIds = selectedIds+","+productId;
			  noneSelected = false;
		  }
	});
	selectedIds = selectedIds.substr(1,selectedIds.length);
	if(selectedIds){
		return selectedIds;
	}
	return false;
}

function listenToRedirection(useDatabase){
	if(window.goTo){
		switch(goTo){
		case '2':
			$('#nav_next_page_1').click();
			break;
		case '3':
			goToCartLink(useDatabase);
			break;
		}
		goTo = '';
	}
}

function goToCartLink(useDatabase){
	addToShoppingCart(useDatabase);
	$('#page_1').css('display','none');
	$('#page_2').css('display','none');
	$('#page_3').css('display','block');
	$('#btnInsert').css('display','none');
	switchSelectedTab(3);
}

function validateDestination(){
	var serial_pxc = $('#selProduct').find('option[text="'+$("#selProduct").getSetSSValue()+'"]').val();
	var serial_cou = $('#selDestination').find('option[text="'+$("#selDestination").getSetSSValue()+'"]').val();
	
	if(serial_pxc && serial_cou){
		$.ajax({
			url: document_root+"rpc/remoteValidation/sales/checkDestination.rpc",
			type: "post",
			data: ({serial_pxc : serial_pxc, serial_cou: serial_cou}),
			dataType: "json",
			success: function(valid_destination) {
				if(!valid_destination){
					$('#selDestination').val('');
					$("#selDestination").getSetSSValue('- Seleccione -');
					alert('El producto seleccionado no puede utilizarse para el destino actual.');
				}
			}
		});
	}	
}

//FUNCTION estimatorMasterRedirect IS $global_functions.js SINCE IT'S CALLED FROM THE HEADER ANYWHERE 