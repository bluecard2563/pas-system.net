//Java Script
var type; /* Sale type-> VIRTUAL or MANUAL*/
$(document).ready(function(){
	$('#txtCardNum').attr("readonly",true);
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmCustomerLogin").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDocumentCustomer: {
				required: true
			},
			txtNameCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			txtLastnameCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly: true
			},
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			txtBirthdayCustomer: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				date: true
			},
			txtAddress: {
				required: {
					depends: function(element) {
						var display = $("#divName").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				}
			},
			txtPhone1Customer: {
				required: true,
				digits: true
			},
			txtPhone2Customer: {
				digits: true
			},
			txtCellphoneCustomer: {
				digits: true
			},
			txtMailCustomer: {
				required: true,//is required further in the process
				email: true, 
				remote: {
					url: document_root+"rpc/remoteValidation/customer/checkCustomerEmail.rpc",
					type: "POST",
					data: {
						serialCus: function() {
							return $("#serialCus").val();
						},
						txtMail: function(){
							return $('#txtMailCustomer').val();
						}
					}
				}
			},
			txtRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				textOnly:true
			},
			txtPhoneRelative: {
				required: {
					depends: function(element) {
						var display = $("#divRelative").css('display');
						if(display=='none')
							return false;
						else
							return true;
					}
				},
				digits: true
			},
		},
		messages: {
			txtDocumentCustomer: {
				remote: "La identificaci&oacute;n ingresada ya existe."
			},
			txtMailCustomer: {
				email: "Por favor ingrese un correo con el formato nombre@dominio.com",
				remote: "El E-mail ingresado ya existe en el sistema."
			},
			txtBirthdayCustomer: {
				date:"El campo 'Fecha de Naciemiento' debe tener el formato dd/mm/YYYY"
			},
			txtPhone1Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono'"
			},
			txtPhone2Customer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono 2'"
			},
			txtCellphoneCustomer: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Celular'"
			},
			txtRelative: {
				textOnly: 'El campo "Contacto en caso de Emergencia" debe tener s&oacute;lo caracteres.'
			},
			txtPhoneRelative: {
				digits:"S&oacute;lo se aceptan n&uacute;meros en el campo 'Tel&eacute;fono del Contacto'"
			},
		},
		submitHandler: function(form) {
			if($('#hdnStatus').val() == 'INACTIVE'){
				alert('La venta de cualquier producto est\u00E1 prohibida a clientes en Lista Negra.');
				return false;
			}
			
			$('#btnRegistrar').attr('disabled', 'true');
			$('#btnRegistrar').val('Espere por favor...');
			form.submit();
		}
	});
	
	$('#txtDocumentCustomer').bind('blur',function(){        
		loadCustomer($(this).val());
	});
	
	/*CHANGE OPTIONS*/
	$("#selCountry").bind("change", function(e){
		loadCities(this.value,'Customer','ALL');
	});

	/*CALENDAR*/
	setDefaultCalendar($("#txtBirthdayCustomer"),'-100y','+0d','-20y');
});

function loadCities(countryID,selectID, ALL){
	if(selectID == "Destination") {
		var container = $('#cityDestinationContainer');
	}
	else{
		if(selectID == "Reassign"){
			var container = $('#cityContainerReassign');
		}
		else{
			var container = $('#cityContainerCustomer');
		}
	}
	container.load(
		document_root+"rpc/loadCities.rpc",
		{
			serial_cou: countryID, 
			serial_sel: selectID, 
			all: ALL
		},
		function(){
			$("#selCityReassign").bind("change", function(e){
				loadDealer(this.value);
			});
		});
}

/*
*@Name: loadCustomer
*@Description: Verifies if the document entered is from a customer already registered and displays his/her information
*@Params: customerID = Identification number of the customer
**/
function loadCustomer(customerID){
	show_ajax_loader();
	var docCust= customerID;
	$.ajax({
		url: document_root+"rpc/remoteValidation/customer/checkCustomerDocument.rpc",
		type: "post",
		data: ({
			txtDocument : docCust
		}),
		success: function(customerExists) {
			$('#customerContainer').load(
				document_root+"rpc/loadsSales/loadCustomer.rpc",
				{
					document_cus: docCust
				},function(){
					$('#selCountry').bind('change',function(){
						loadCities(this.value,'Customer','ALL');
					});
					$('#txtDocumentCustomer').bind('blur',function(){
						loadCustomer($(this).val());
					});
					if(customerExists == 'true'){
						$('#txtNameCustomer').attr('readonly', 'true');
						$('#txtLastnameCustomer').attr('readonly', 'true');
						$('#txtMailCustomer').attr('readonly', 'true');
					}else{
						setDefaultCalendar($("#txtBirthdayCustomer"),'-100y','+0d','-20y');
						$('#txtNameCustomer').removeAttr('readonly');
						$('#txtLastnameCustomer').removeAttr('readonly');
						$('#txtEmail').removeAttr('readonly');
					}
						   
					hide_ajax_loader();
				});
		}
	});
}

function loadDialog(){
//DO NOT DELETE, OVERRIDES DEFAULT RPC CALL
}