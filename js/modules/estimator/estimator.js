// JavaScript Document
$(document).ready(function(){
	validationPassed = true; //used in the wizard.js file
	
	enableEstimatorTabJavascript(false);
	
	activateWizard("estimatorFullForm","Comprar",false);
    
	//NOTE THAT onsubmit : false, CALLS FOR MANUAL VALIDATION OF EACH FIELD WHENEVER IT'S NEEDED
	addValidatorsEstimatorTab("estimatorFullForm",false);
	
	addCustomSubmission("submitEstimatorFull");

	bindBenefitsDialog();
	
	listenToRedirection(false);
});

function submitEstimatorFull(){
	//AFTER LOGIN GO DIRECTLY TO THE ESTIMATOR PAGE:
	saveSessionVariable('web_customer_redirect','parseSessionCartToDB');
	window.location = document_root+ "modules/estimator/parseSessionCartToDB";
}

function saveMyCart(selectedIds){
	$.ajax({
		url: document_root+ 'modules/estimator/sendCartToSessionAjax.php',
		type: "POST",
		data: { 'cart_prods': selectedIds},
		async: false,
		success: function(values){
			//alert(values);
		}
	});
}