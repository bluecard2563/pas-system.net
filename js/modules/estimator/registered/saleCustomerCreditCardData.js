// JavaScript Document
$(document).ready(function(){
	$('#selCity').sSelect({ddMaxHeight: 100});
	$('#selMonth').sSelect({ddMaxHeight: 100});
	$('#selYear').sSelect({ddMaxHeight: 100});
	$('#selCountry').sSelect({ddMaxHeight: 100});
		
	$("#selCountry").bind("change", function(e){
	    loadCities(this.value,'Customer','ALL',null);
	});
	
	$.validator.addMethod("mod10Valid", function(valueA, element) {
		isValid = Mod10(valueA);
		return isValid;
	},"El N&uacute;mero de Tarjeta no es valido");
	
	$("#creditCardForm").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCardNum: {
		        required: true,
		        number: true,
		        mod10Valid: true
		    },
		    txtSecCode: {
		    	required: true,
		        number: true
		    },
		    txtNameCard: {
		        required: true,
		        textOnly: true
		    },
		    selMonth: {
		        required: true
		    },
		    selYear: {
		        required: true
		    },
		    txtAddress: {
		        required: true
		    },
		    selCountry: {
		        required: true
		    },
		    selCity: {
		        required: true
		    },
		    txtState: {
		        required: true,
		        textOnly: true
		    },
		    txtZip: {
		        required: true
		    },
		    txtPhone: {
		        required: true,
		        number: true
		    },
		    txtMail: {
		        required: true,
		        email: true
		    }
		},
		messages: {
			txtCardNum: {
		        number: "El campo 'N&uacute;mero de Tarjeta' acepta solo n&uacute;meros",
		        mod10Valid: "El N&uacute;mero de Tarjeta no es valido"
		    },
		    txtSecCode: {
		    	number: "El campo 'C&oacute;digo de Seguridad' acepta solo n&uacute;meros"
		    },
		    txtNameCard: {
		        textOnly: "El campo 'Nombre en la Tarjeta' acepta solo caracteres"
		    },
		    txtExpirationDate: {
		        date: "El el formato ingresado en 'Fecha de Vencimiento' no es v&aacute;lido"
		    },
		    txtState: {
		    	textOnly: "El campo 'Estado/Provincia' acepta solo caracteres"
		    },		    
		    txtPhone: {
		        number: "El campo 'Tel&eacute;fono' acepta solo n&uacute;meros"
		    },
		    txtMail: {
		        email: "El formato del campo 'Email' no es v&aacute;lido"
		    }
		}
	});
	
	$("#btnSend").bind("click", function(e){
        $('#messages').css('display','none');
        if($("#creditCardForm").valid()){
      	  show_ajax_loader();
            $("#creditCardForm").submit();
        }else{
        	window.scroll(0,0);
        }
	});
});

function loadCities(countryID,selectID, ALL, cityId){
	$('#cityContainerCustomer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID, serial_sel: selectID, all: ALL, cityId: cityId}, function(){
		$('#selCityCustomer').sSelect({ddMaxHeight: 100});
	});
}