// JavaScript Document
$(document).ready(function(){
	validationPassed = true; //used in the wizard.js file
	
	enableEstimatorTabJavascript(true); //use database
	
	activateWizard("estimatorFullForm","Proceder a Ingreso de Datos de Viajeros",true);//use database
    
	//NOTE THAT onsubmit : false, CALLS FOR MANUAL VALIDATION OF EACH FIELD WHENEVER IT'S NEEDED
	addValidatorsEstimatorTab("estimatorFullForm",false);
	
	addCustomSubmission("submitEstimatorCustomer");

	bindBenefitsDialog();
	
	listenToRedirection(true);
	
	$('#quitCustomer').bind('click',function(){
		window.location = document_root+ "modules/estimator/estimator";
    });
});

function submitEstimatorCustomer(){
	window.location = document_root+ "modules/estimator/registered/saleCustomerTravelersData";
}

function saveMyCart(selectedIds){
	$.ajax({
		url: document_root+ 'modules/estimator/sendCartToDatabaseAjax.php',
		type: "POST",
		data: { 'cart_prods': selectedIds},
		async: false,
		success: function(values){
			//alert(values);
		}
	});
}