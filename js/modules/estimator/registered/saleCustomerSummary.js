// JavaScript Document
$(document).ready(function(){
	$("#saleTypeForm").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		submitHandler: function(form){
			show_ajax_loader();

			if($("#saleTypeForm").attr('action')){
				form.submit();
			}else{
				window.location = document_root+ "modules/estimator/registered/saleCustomerCreditCardData/";
			}
			return false;
		}
	});
	
	$("#continueButton").bind("click", function(e){
		$("#saleTypeForm").submit();
    });

	$('#btnCoupon').bind('click', function(){
		if($('#txtCoupon').val() != ''){
			$("#saleTypeForm").attr('action', '#');
			$("#saleTypeForm").submit();
		}else{
			alert('Debe ingresar un c\u00F3digo para continuar.');
		}
	});
	
	//NEW INVOICE HOLDER SETTINGS
	$('#selCountry').bind('change',function(){
		loadCities(this.value,'Customer','ALL');
	});
	
	$('#txtCustomerDocument').bind('change',function(){
        var docCust= $(this).val();
        loadCustomer(docCust);
    });
	
	$("#newHolderInfo").dialog({
		bgiframe: true,
		autoOpen: false,
		width: 770,
		height: 430,
		modal: true,
		resizable: false,
		title: "Nuevo Titular de Factura(s)",
		buttons: {
			'Cerrar': function() {
				$('#alertsNewHolder').css('display','none');
				$(this).dialog('close');
			}
		  }
	});
	
	$('#btnNewHolder').bind('click', function(){
		addNewHolderValidations();
		$("#newHolderInfo").dialog('open');
	});
});


function loadCustomer(customerID){
    var docCust= customerID;
    show_ajax_loader();
    $.ajax({
        url: document_root+"rpc/remoteValidation/customer/checkCustomerDocument.rpc",
        type: "post",
        data: ({
            txtDocument : docCust
        }),
        success: function(customerExists) {
            $('#customerData').load(
                document_root+"rpc/loadsInvoices/loadCustomer.rpc",
                {
                    document_cus: docCust,
                    document_validation: 'YES',
					small_container: 'YES'
                },function(){
                    $('#selCountry').bind('change',function(){
                        loadCities(this.value,'Customer','ALL');
                    });
                    
					$('#txtCustomerDocument').bind('blur',function(){
                        loadCustomer($(this).val());
                    });
					
					addNewHolderValidations();
					
					$('#btnChangeHolder').bind('click', function(){
						checkAndCommitNewHolderForm();
					});
					
                    hide_ajax_loader();
                });
        }
    });
}

function loadCities(countryID){
    if(countryID==""){
        $("#selCity option[value='']").attr("selected",true);
    }
	
    $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
        serial_cou: countryID,
        all: 'YES'
    });
}

function addNewHolderValidations(){
    $("#invoiceHolder").validate({
        errorLabelContainer: "#alertsNewHolder",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtCustomerDocument: {
                required: true,
				alphaNumeric: true
            },
            txtFirstName: {
				required: true,
				invoice_holder: true
            },
            txtLastName: {
                required: {
					depends: function(element) {
						if($('#lastNameContainer').css('display') == 'none')
							return false;
						else
							return true;
					}
				},
				invoice_holder: true
            },
            txtPhone1: {
                required: true,
				number: true
            },
            txtPhone2: {
                number: true
            },
            selCountry:{
                required: true
            },
            selCity: {
                required: true
            }
        },
        messages: {
            txtCustomerDocument:{
                alphaNumeric: "El campo 'Documento' solo acepta valores alfan&uacute;mericos."
            },
            txtFirstName:{
                invoice_holder: "El campo 'Nombre' solo acepta valores alfan&uacute;mericos y el (.)"
            },
            txtLastName:{
                invoice_holder: "El campo 'Apellido' solo acepta valores alfan&uacute;mericos y el (.)"
            },
			txtPhone1: {
				number: "El campo 'Tel&eacute;fono' solo acepta n&uacute;meros."
			},
			txtPhone2: {
				number: "El campo 'Tel&eacute;fono Secundario' solo acepta n&uacute;meros."
			}
        }
    });
}

function checkAndCommitNewHolderForm(){
	if($('#invoiceHolder').valid()){
		show_ajax_loader();
		$('[id^="rdDocumentType_"]').each(function(){
			if($(this).is(':checked')){
				document_type = $(this).val();
			}
		});          

		$.ajax({
			url: document_root+"rpc/remoteValidation/invoice/checkHolderDocumentNumber.rpc",
			type: "post",
			data: ({
				validate_document: 'YES',
				document_type: document_type,
				invoice_to: 'CUSTOMER',
				customer_id: function(){
					return $('#txtCustomerDocument').val();
				}
			}),
			dataType: 'json',
			success: function(valid_document) {
				if(valid_document){
					logNewHolderInfo();
				} else {
					alert('El documento ingresado no es v\u00E1lido. Por favor ingrese una CI o RUC v\u00E1lido.');
				}
				hide_ajax_loader();
			}
		});
	}
}

function logNewHolderInfo(){
	var person_type;
	
	$('[id^="rdDocumentType"]').each(function(){
		if($(this).is(':checked')){
			person_type = $(this).attr('person_type');
			return;
		}
	});
	
	show_ajax_loader();	
	$.ajax({
		url: document_root+"rpc/estimator/logNewHolderInfo.rpc",
		type: "post",
		data: ({
			customer_type: person_type,
			customer_id: function(){  
				return $('#hdnSerialCusRPC').val();   
			},
			document_number: function(){  
				return $('#txtCustomerDocument').val();   
			},
			customer_name: function(){ 
				return $('#txtFirstName').val();  
			},
			customer_lastname: function(){ 
				return $('#txtLastName').val();   
			},
			customer_address: function (){  
				return $('#txtAddress').val();  
			},
			customer_phone1: function(){ 
				return $('#txtPhone1').val(); 
			},
			customer_phone2: function(){  
				return $('#txtPhone2').val();   
			},
			customer_city: function(){  
				return $('#selCity').val();   
			}
		}),
		dataType: 'html',
		success: function(registration_data) {
			hide_ajax_loader();
			registration_data = registration_data.split('%%');

			if(registration_data[0]){
				$('#lblDocument').html(registration_data[1]);
				$('#lblFullName').html(registration_data[2]);
				$('#newHolderInfoContainer').show();

				$('#btnRemoveInvoiceHolder').bind('click', function(){
					removeInvoiceHolder();
				});

				$("#newHolderInfo").dialog('close');
			} else {
				alert('Hubo errores en la operaci\u00F3n. Vuelva a Intentarlo.');
			}
		}
	});
}

function removeInvoiceHolder(){
	show_ajax_loader();
	
	$.ajax({
		url: document_root+"rpc/estimator/removeNewHolderInfo.rpc",
		type: "post",
		dataType: 'json',
		success: function(response) {
			if(!response){
				alert('Hubo errores al remover el registro.');
			}
			
			$('#newHolderInfoContainer').hide();
			$('#lblDocument').html('');
			$('#lblFullName').html('');
			
			cleanNewHolderForm();
			
			hide_ajax_loader();		
		}
	});
}

function cleanNewHolderForm(){
	$('#txtCustomerDocument').val('');
	$('#txtFirstName').val('');
	$('#txtLastName').val('');
	$('#txtAddress').val('');
	$('#txtPhone1').val('');
	$('#txtPhone2').val('');
	$('#selCountry').val('');
	$('#selCity').val('');
	
	$('#newHolderActionContainer').hide();
}