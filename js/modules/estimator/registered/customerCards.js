// JavaScript Document
var pager;

function loadCustomerCards(serial_cus){
	$('#cardsContainer').load(document_root+"rpc/customer/loadRegisterCardByCustomer.rpc",{serial_cus: serial_cus},
        function(){
            //Paginator
            if($('#travelsTable').length>0){
                pager = new Pager('travelsTable', 10 , 'Siguiente', 'Anterior');
                pager.init(7); //Max Pages
                pager.showPageNav('pager', 'travelPageNavPosition'); //Div where the navigation buttons will be placed.
                pager.showPage(1); //Starting page
            }

	});
}