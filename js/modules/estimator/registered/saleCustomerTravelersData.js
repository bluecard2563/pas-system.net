// JavaScript Document

//This variables are particular to this site's implementation:
estimatorModuleMainURL = 'modules/estimator/registered/';
travelersDataUrl = estimatorModuleMainURL + 'saleCustomerTravelersData/';
summaryPage = estimatorModuleMainURL + 'saleCustomerSummary';
estimatorTab3 = estimatorModuleMainURL + 'estimatorCustomer/0/3';
removeCardURL = estimatorModuleMainURL + 'removeTravelerCardAjax.php';
checkProductValuesURL = estimatorModuleMainURL + 'checkProductCardsValues.php';
getCustomerDataURL = estimatorModuleMainURL + 'getCustomerDataAjax.php';
checkProductValidatorURL = estimatorModuleMainURL + 'checkProductAdultsValidator.php';

productTravelersRPC = 'rpc/estimator/loadProductTravelersInfo.rpc';
loadHolderInfoRPC = 'rpc/estimator/loadProductTravelersInfoHolder.rpc';
loadExtraInfoRPC = 'rpc/estimator/loadProductTravelersInfoExtra.rpc';
loadCitiesRPC = 'rpc/loadCities.rpc';

$(document).ready(function(){
	$("div[id^='productBox_']").each( function(e){
		$(this).bind('click',function(){
			activeProduct = $("#travelersInfoForm").css('display') == 'block';
			if(activeProduct){
				if(!confirm('Si cambia de tab perder\u00E1 la informaci\u00F3n ingresada, \u00BFdesea continuar?')){
	    			return false;
	    		}
			}
			show_ajax_loader();
			productIndex = $(this).attr('id').replace("productBox_","");
			window.location = document_root + travelersDataUrl + productIndex;
	    });
	});
	
	$("div[id^='cardBox_']").each( function(e){
		$(this).bind('click',function(){
			show_ajax_loader();
			activeProduct = $("#prodIndex").val();
			$("#cards_content").css('display', 'none');
			
			cardIndex = $(this).attr('id').replace("cardBox_","");
			productIndex = $("#prodIndex").val();
			$('#productTravelersInfoContainer').load(document_root+ productTravelersRPC,{productIndex: productIndex, cardIndex: cardIndex}, function(){
				$("#travelersInfoForm").css('display', 'block');
				loadHolderJS();
				
				$("[id^='idExtra_']").each(function(e){
					extraId = $(this).attr('id').replace("idExtra_","");
					loadExtraJS(extraId);
				});
				
				enableDisableAllTravelers();
				$('#scrollable-field').jScrollPane({showArrows:true,scrollbarWidth:13});
				
				//QUESTIONS DIALOGS:
				$("#questionsDialog").dialog({
					bgiframe: true,
					autoOpen: false,
					height: 500,
					width: 700,
					modal: true,
					buttons: {
						'Guardar': function() {
							var formValid = true;
							$('[id^="question_"]').each(function(){
								if($(this).val() == ''){
									formValid=false;
									return;
								}
							});
							if(formValid){
								saveQuestionsToInputs();
								 $(this).dialog('close');
							}
							else{
								alert('Por favor responda todas las preguntas!');
							}
						}
					  },
					close: function() {
						//mark questions as not answered
					}
				});
				hide_ajax_loader();
			});
	    });
	});
	
	$("#continueBox").bind('click',function(){
		if(!confirm('Se agregar\u00E1n solamente los productos cuyos datos han sido ingresados,\n (Solamente aquellos'+
					' que no tienen el simbolo de alerta ) \u00BFdesea continuar?')){
			return false;
		}
		show_ajax_loader();
		window.location = document_root+ summaryPage;
    });
	
	$("#returnBox").bind('click',function(){
		show_ajax_loader();
		window.location = document_root + estimatorTab3;
    });
	
	$("img[id^='closeCross_']").click(function(){
		if(confirm('Est\u00E1 seguro que desea eliminar esta tarjeta?, no podr\u00E1 deshacer esta operaci\u00F3n')){
			productIndex = $("#prodIndex").val();
			cardIndex = $(this).attr('id').replace("closeCross_","");
			$.ajax({
				  url: document_root+ removeCardURL,
				  type: "POST",
				  data: { 'productIndex': productIndex, 'cardIndex': cardIndex},
				  async: false,
				  success: function(response){
					  window.location = document_root+ travelersDataUrl +response;
				  }
			});
		}
		return false;
    });
	
	$.validator.addMethod("uniqueDocumentValidator", function(customerId, element) {
		if(customerId == ''){
			return true;//If no value, we display no alert, this is another validator
		}
		var uniqueIds = checkOtherCardsValues(customerId,$("#prodIndex").val(),$('#cardIndex').val(), 'id');
		if(!uniqueIds){ return false;}
		$("[id^='idExtra_']").each( function(e){
			myId = $(this).attr('id');
			myVal = $(this).val();
			if(myVal == ''){
				return true;//If this extra's value is empty, display no alert, this is another validator
			}
			if(myVal == customerId){
				uniqueIds = false; return false;
			}else{
				$("[id^='idExtra_']").each( function(e){
					secId = $(this).attr('id');
					secVal = $(this).val();
					if(myId != secId && myVal == secVal){
						uniqueIds = false; return false;
					}
				});
				if(!uniqueIds){ return false;}
			}
			//Also validate with the emails for other cards:
			uniqueIds = checkOtherCardsValues(myVal,$("#prodIndex").val(),$('#cardIndex').val(), 'id');
			if(!uniqueIds){ 
				return false;
			}
		});
		return uniqueIds;
	},"Los N&uacute;meros de documento deben ser &uacute;nicos para cada viajero");
	
	$.validator.addMethod(
	    "australianDate",
	    function(value, element) {
	        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
	    },
	    "Please enter a date in the format dd/mm/yyyy"
	);
	
	$.validator.addMethod("uniqueEmailValidator", function(customerId, element) {
		customerEmail = $('#txtMailCustomer').val();
		if(customerEmail == ''){
			return true;//If no value, we display no alert, this is another validator
		}
		var uniqueEmails = checkOtherCardsValues(customerEmail,$("#prodIndex").val(),$('#cardIndex').val(), 'email');
		if(!uniqueEmails){ return false;}
		$("[id^='emailExtra_']").each( function(e){
			myId = $(this).attr('id');
			myVal = $(this).val();
			if(myVal == ''){
				return true;//If this extra's value is empty, display no alert, this is another validator
			}
			if(myVal == customerEmail){
				uniqueEmails = false;return false;
			}else{
				$("[id^='emailExtra_']").each( function(e){
					secId = $(this).attr('id');
					secVal = $(this).val();
					if(myId != secId && myVal == secVal){
						uniqueEmails = false;return false;
					}
				});
				if(!uniqueEmails){ return false;}
			}
			//Also validate with the emails for other cards:
			uniqueEmails = checkOtherCardsValues(myVal,$("#prodIndex").val(),$('#cardIndex').val(), 'email');
			if(!uniqueEmails){ 
				return false;
			}
		});
		return uniqueEmails;
	},"Los Email deben ser &uacute;nicos para cada viajero");

	$.validator.addMethod("adultsOnlyValidator", function(customerId, element) {
		return checkAgeValidationsSales($("#extrasRestriction").html(),'ADULTS', 'ADULTS');
	},"Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.");
	
	$.validator.addMethod("childrenOnlyValidator", function(customerId, element) {
		return checkAgeValidationsSales($("#extrasRestriction").html(),'CHILDREN', 'CHILDREN');
	},"Los acompa&ntilde;antes s&oacute;lo pueden ser menores.");
	
	$.validator.addMethod("seniorExtrasValidator", function(customerId, element) {
		return checkAgeValidationsSales($("#extrasRestriction").html(),'NO','SENIOR');
	},"Este producto no admite adultos mayores.");
	
	$("#travelersInfoForm").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtDocumentCustomer: {
		        required: true,
		        uniqueDocumentValidator: true,
		        uniqueEmailValidator: true,
		        adultsOnlyValidator: true,
		        childrenOnlyValidator: true,
		        seniorExtrasValidator: true
		    },
		    selType: {
		        required: true
		    },
		    txtNameCustomer: {
		        required: true,
		        textOnly: true
		    },
		    txtLastnameCustomer: {
		        required: true,
		        textOnly: true
		    },
		    txtBirthdayCustomer: {
		        required: true,
		        australianDate: true
		    },
		    txtMailCustomer: {
		        required: true,
		        email: true
		    },
		    selCountry: {
		        required: true
		    },
		    selCityCustomer: {
		        required: true
		    },
		    txtAddress: {
		        required: true
		    },
		    txtPhone1Customer: {
		        required: true,
		        number: true
		    },
		    txtPhone2Customer: {
		        number: true
		    },
		    txtCellphoneCustomer: {
		        number: true
		    },
		    txtRelative: {
		        required: true,
		        textOnly: true
		    },
		    txtPhoneRelative: {
		        required: true,
		        number: true
		    }
		},
		messages: {
			txtDocumentCustomer: {
		        uniqueDocumentValidator: "Los N&uacute;meros de documento deben ser &uacute;nicos para cada viajero",
		        uniqueEmailValidator: "Los Email deben ser &uacute;nicos para cada viajero", //This is the only enabled field, this validation must be here
	        	adultsOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser adultos.",
	        	childrenOnlyValidator: "Los acompa&ntilde;antes s&oacute;lo pueden ser menores.",
	        	seniorExtrasValidator: "Este producto no admite adultos mayores."
		    },
		    txtNameCustomer: {
		        textOnly: "El campo 'Nombre' acepta solo caracteres"
		    },
		     txtLastnameCustomer: {
		        textOnly: "El campo 'Apellido' acepta solo caracteres"
		    },
		    txtBirthdayCustomer: {
		        australianDate: "El el formato ingresado en 'Fecha de Nacimiento' no es v&aacute;lido"
		    },
		    txtMailCustomer: {
		        email: "El formato del campo 'Email' no es v&aacute;lido"
		    },
		    txtPhone1Customer: {
		        number: "El campo 'Tel&eacute;fono 1' acepta solo n&uacute;meros"
		    },
		    txtPhone2Customer: {
		        number: "El campo 'Tel&eacute;fono 2' acepta solo n&uacute;meros"
		    },
		    txtCellphoneCustomer: {
		        number: "El campo 'Celular' acepta solo n&uacute;meros"
		    },
		    txtRelative: {
		        textOnly: "El campo 'Contacto en caso de emergencia' acepta solo letras"
		    },
		    txtPhoneRelative: {
		        number: "El campo 'Tel&eacute;fono del contacto' acepta solo n&uacute;meros"
			}
		}
	});
	
	$("#btnSend").bind("click", function(e){
		show_ajax_loader();
        $('#messages').css('display','none');
        enableAllElements();
        if($("#travelersInfoForm").valid()){
            if(validateAllElderlyQuestionsForms()){
            	$("#travelersInfoForm").submit();return;
            }else{
            	alert('Por favor complete las preguntas asociadas al Adulto Mayor');
            }
        }
        enableDisableAllTravelers();
    	window.scroll(0,0);
    	hide_ajax_loader();
	});
	
	$("#btnExit").bind("click", function(e){
        $('#messages').css('display','none');
        productIndex = $(this).attr('id').replace("productBox_","");
        if(confirm('Esta seguro que desea salir? No se guardara ningun cambio en la tarjeta.')){
        	window.location = window.location;
		}
	});
	
	$.validator.addMethod("dateRange", function(birthDate, element) {
	    var today = new Date();
	    var mdy = birthDate.split('/')
	    birthDate = new Date(mdy[2], mdy[1]-1, mdy[0]);
	    if( birthDate >= today ){
	    	return false;
	    }
	    return true;
	}, "La fecha de Nacimiento no puede ser mayor a la de Hoy");
	
});

function validateAllElderlyQuestionsForms(){
	allFormsAreValid = true;
	if($('#questionsLinkHolder').css('display') == 'block'){
		if($('#hiddenAnswers_0').html() != ""){ 
			$("#hiddenAnswers_0 [id^='answer_']").each(function (i) {
				if(this.value == ""){
					allFormsAreValid = false;
				}
			});
		}else{
			allFormsAreValid = false;
		}
	}
	
	$("[id^='idExtra_']").each(function(e){
		idExtra = parseInt($(this).attr('id').replace("idExtra_",""));
		if($('#questionsExtra_' + idExtra).css('display') == 'block'){
			if($('#hiddenAnswers_' + (idExtra + 1)).html() != ""){ 
				$("#hiddenAnswers_" + (idExtra + 1) + " [id^='answer_']").each(function (i) {
					if(this.value == ""){
						allFormsAreValid = false;
					}
				});
			}else{
				allFormsAreValid = false;
			}
		}
	});
	return allFormsAreValid;
}

function openQuestionsDialog(customerLocation){
	if(customerLocation == 0){//Holder
		customerDocument = $('#txtDocumentCustomer').val();
	}else{//Extras
		customerDocument = $('#idExtra_' + (customerLocation - 1)).val();
	}
	
	productIndex = $("#prodIndex").val();
	cardIndex = $("#cardIndex").val();
	
	$("#customerLocation").val(customerLocation);
	$('#dialogQuestionsContainer').load(document_root + "rpc/estimator/loadCustomerQuestionsIntoDialog.rpc",{
			customerLocation: customerLocation, 
			customerDocument: customerDocument},
			function(){
				//Load questions from Inputs:
				$("#hiddenAnswers_" + customerLocation + " [id^='answer_" + customerLocation + "_']").each(function (i) {
					questionId = $(this).attr('id').replace("answer_" + customerLocation + "_", "");
					$("#question_" + customerLocation + "_" + questionId).val(this.value);
				});
				$("#questionsDialog").dialog('open');
			});
}

function displayHideQuestionsLink(customerLocation){
	if(customerLocation == 0){//Holder
		birthDate = $('#txtBirthdayCustomer').val();
	}else{//Extras
		birthDate = $('#birthdayExtra_' + (customerLocation - 1)).val();
	}
	
	visibilityToSet = 'none';
	if(birthDate && birthDate != ''){
		customerAge = getAgeOfCustomer(birthDate);
		if(customerAge > $('#hdnElderlyAge').val()){
			visibilityToSet = 'block';
		}
	}
	
	if(customerLocation == 0){//Holder
		$('#questionsLinkHolder').css('display', visibilityToSet);
	}else{//Extras
		$('#questionsExtra_' + (customerLocation - 1)).css('display', visibilityToSet);
	}
}

/*
 *@Name: getAgeOfCustomer
 *@Description: Calculates the exact age of an Extra
 *@Params: count=Number of Extra form
 **/
function getAgeOfCustomer(birthDate){
	var curr = new Date(Date.parse(dateFormat(birthDate)));
	var year = curr.getFullYear();
	var month = curr.getMonth()+1;
	var day = curr.getDate();
	
	var today = new Date();
	
	var age = today.getFullYear()-year-1;

	if(today.getMonth() + 1 - month < 0){
		return age;
	}
	if(today.getMonth() + 1 - month > 0){
		return age+1;
	}
	if (today.getUTCDate() - day >=0){
       return age+1;
	}
    return age;
}

function saveQuestionsToInputs(){
	customerLocation = $("#customerLocation").val(); //0 for Holder, else for extra
  	answersDiv = $("#hiddenAnswers_" + customerLocation);
  	answersDiv.html("");
	$("[id^='question_']").each(function (i) {
		questionId = $(this).attr('id').replace("question_" + customerLocation + "_","");
		answersDiv.append('<input type="hidden" name="answer_' + customerLocation + '_' + questionId + '" id="answer_' + customerLocation + '_' + questionId + '" value="'+this.value+'" />');
	});
	$('#dialogQuestionsContainer').html("");
}

function loadHolderJS(){
	$('#selCountry').sSelect({ddMaxHeight: 100});
	$('#selCityCustomer').sSelect({ddMaxHeight: 100});
	
	$('#txtDocumentCustomer').bind('change',function(){
		loadCustomerDataIfExists($(this).val(),-1);
	});
	$("#selCountry").bind("change", function(e){
		loadCities(this.value,'Customer','ALL',null,false);
	});
	$("#questionsLinkHolder").click(function(){
		openQuestionsDialog(0);
	});
	
	if(!customerAlreadyExists($('#txtDocumentCustomer').val(),-1)){
		setDefaultCalendar($('#txtBirthdayCustomer'),'-100y','+0d','-20y');
	}
	
	$("#txtBirthdayCustomer").change(function(e){
		displayHideQuestionsLink(0);
	});
	displayHideQuestionsLink(0);
}

function loadExtraJS(extraId){
	$("#idExtra_" + extraId).bind('change',function(){
		loadCustomerDataIfExists($(this).val(),extraId);
	});
	
	$("#questionsExtra_" + extraId).click(function(){
		openQuestionsDialog(parseInt(extraId) + 1);
	});
	
	$("#selRelationship_" + extraId).sSelect({ddMaxHeight: 100});
	if(!customerAlreadyExists($('#idExtra_'+extraId).val(),-1)){
		setDefaultCalendar($("#birthdayExtra_" + extraId),'-100y','+0d','-20y');
	}
	
	//VALIDATIONS:
	$("#idExtra_"+extraId).rules("add", {
		required: true,
		messages: {
			required: "El campo 'Identificaci&oacute;n' es obligatorio (Acompa&ntilde;ante "+(extraId*1+1)+")"
		}
	});
	$("#selRelationship_"+extraId).rules("add", {
		required: true,
		messages: {
			required: "El campo 'Tipo de Relaci&oacute;n con el Titular' es obligatorio (Acompa&ntilde;ante "+(extraId*1+1)+")"
		}
	});
	$("#selRelationship_" + extraId).bind("change", function(e){
		if($(this).val() == 'SPOUSE'){
			var spouseAlreadyExists = false;
			var myId = $(this).attr('id');
			$("[id^='selRelationship_']").each( function(e){
				if($(this).val() == 'SPOUSE' && $(this).attr('id')!=myId){
					spouseAlreadyExists = true;
					$(this).getSetSSValue('OTHER');
				}
			});
			if(spouseAlreadyExists){
				alert('Ya ha seleccionado a otro acompa\u00F1ante como Esposa');
			}
		}
	});
	$("#nameExtra_" + extraId).rules("add", {
		required: true,
		textOnly: true,
		messages: {
			required: "El campo 'Nombre' es obligatorio (Acompa&ntilde;ante "+(extraId*1+1)+")",
			textOnly: "El campo 'Nombre' acepta solo caracteres (Acompa&ntilde;ante "+(extraId*1+1)+")"
		}
	});
	$("#emailExtra_" + extraId).rules("add", {
		email: true,
		messages: {
			email: "El formato del campo 'Email' no es v&aacute;lido (Acompa&ntilde;ante "+(extraId*1+1)+")"
		}
	});
	$("#lastnameExtra_" + extraId).rules("add", {
		required: true,
		textOnly: true,
		messages: {
			required: "El campo 'Apellido' es obligatorio (Acompa&ntilde;ante "+(extraId*1+1)+")",
			textOnly: "El campo 'Apellido' acepta solo caracteres (Acompa&ntilde;ante "+(extraId*1+1)+")"
		}
	});
	$("#birthdayExtra_" + extraId).rules("add", {
		required: true,
		australianDate: true,
		dateRange: true,
		messages: {
			required: "El campo 'Fecha de Nacimiento' es obligatorio (Acompa&ntilde;ante "+(extraId*1+1)+")",
			australianDate: "El campo 'Fecha de Nacimiento' debe ser una fecha v&aacute;lida (Acompa&ntilde;ante "+(extraId*1+1)+")",
			dateRange: "El campo 'Fecha de Nacimiento' debe ser una fecha anterior a hoy (Acompa&ntilde;ante "+(extraId*1+1)+")"
		}
	});
	
	$("#birthdayExtra_" + extraId).change(function(e){
		displayHideQuestionsLink(parseInt(extraId) + 1);
	});
	displayHideQuestionsLink(parseInt(extraId) + 1);
}

function checkOtherCardsValues(theValue, prodValue, cardValue, type){
	uniqueValues = false;
	$.ajax({
		  url: document_root+ checkProductValuesURL,
		  type: "POST",
		  data: { 'txtValue': theValue, 'prodIndex': prodValue, 'cardIndex': cardValue, 'type': type},
		  async: false,
		  success: function(response){
			  if(response == 'true'){
				  uniqueValues = true;
			  }else{
				  uniqueValues = false;
			  }
		  }
	});
	return uniqueValues;
}

function loadCustomerDataIfExists(userDocument, extraId){
	show_ajax_loader();
	customerExists = customerAlreadyExists(userDocument, extraId);
	if(extraId < 0){
		$('#holderContainer').load(document_root + loadHolderInfoRPC,{userDocument: userDocument}, function(){
			loadHolderJS();
			enableDisableFormElements(extraId,customerExists);
			hide_ajax_loader();
		});
	}else{
		$('#extra_'+extraId).load(document_root + loadExtraInfoRPC,{extraIndex: extraId, prodIndex: $("#prodIndex").val(), userDocument: userDocument}, function(){
			loadExtraJS(extraId);
			enableDisableFormElements(extraId,customerExists);
			hide_ajax_loader();
		});
	}
}

function customerAlreadyExists(userDocument, extraId){
	customerExists = false;
	$.ajax({
		url: document_root + getCustomerDataURL,
		type: "POST",
		data: { 'userDocument': userDocument, 'extraId': extraId},
		async: false,
		success: function(values){
			if(values == 'false'){
				customerExists=  false;
			}else if(values == 'inactive'){
				alert('No se puede emitir la p\u00F3liza al cliente ingresado. \nPor favor cont\u00E1ctese con su representante local m\u00E1s cercano y \nentr\u00E9guele el siguiente c\u00F3digo: Ex00300');
				customerExists=  false;
			}else{
				customerExists = values;
			}
		}
	});
	return customerExists;
}

function enableDisableFormElements(extraId, disabledStatus){
	if(extraId >= 0){
		$("#nameExtra_" + extraId).attr('disabled', disabledStatus);
		$("#lastnameExtra_" + extraId).attr('disabled', disabledStatus);
		$("#birthdayExtra_" + extraId).attr('disabled', disabledStatus);
		$("#emailExtra_" + extraId).attr('disabled', disabledStatus);
	}else{
		$("#txtNameCustomer").attr('disabled', disabledStatus);
		$("#txtLastnameCustomer").attr('disabled', disabledStatus);
		$("#txtBirthdayCustomer").attr('disabled', disabledStatus);
		$("#txtMailCustomer").attr('disabled', disabledStatus);
	}
}

function enableAllElements(){
	enableDisableFormElements(-1,false);
	$("[id^='idExtra_']").each(function(e){
		userId = $(this).attr('id').replace("idExtra_","");
		enableDisableFormElements(userId,false);
	});
}

function enableDisableAllTravelers(){
	if(customerAlreadyExists($('#txtDocumentCustomer').val(),-1)){
		enableDisableFormElements(-1,true);
	}
	$("[id^='idExtra_']").each(function(e){
		userId = $(this).attr('id').replace("idExtra_","");
		if(customerAlreadyExists($('#idExtra_'+userId).val(),-1)){
			enableDisableFormElements(userId, true);
		}
	});
}

function loadCities(countryID,selectID, ALL, cityId, disabled){
	$('#cityContainerCustomer').load(document_root + loadCitiesRPC,{serial_cou: countryID, serial_sel: selectID, all: ALL, cityId: cityId, disabled: disabled}, function(){
		$('#selCityCustomer').sSelect({ddMaxHeight: 100});
	});
}