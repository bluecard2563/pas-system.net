// JavaScript Document
$(document).ready(function(){
	$('#selCountry').bind('change',function loadTaxes(){ $('#taxContainer').load(document_root+"rpc/loadTaxes.rpc",{serial_cou: this.value}); });			   

	/*VALIDATION SECTION*/
	$("#frmSearchTax").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selTax: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/	
});