// JavaScript Document
$(document).ready(function(){
	$("#selCountry").bind("change",changeFlag);
	$("#txtNameTax").bind("change",changeFlag);
	/*VALIDATION SECTION*/
	$("#frmNewTax").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			txtNameTax: {
				required: true,
				alphaNumeric: true,
				maxlength: 30
			},
			hdnFlagTax:{
				remote: {
					url: document_root+"rpc/remoteValidation/tax/checkNameTax.rpc",
					type: "post",
					data: {
						serial_cou: function() {
							return $("#selCountry").val();	
						},
						txtNameTax: function() {
							return $("#txtNameTax").val();	
						}
					}
				}
			},
			txtPercentageTax: {
				required: true,
				percentage: true,
				max_value: 100
			}
		},
		messages: {
			txtNameTax: {
				alphaNumeric: "S&oacute;lo se aceptan caracteres alfanum&eacute;ricos en el campo 'Nombre'.",
				maxlength: "El campo 'Nombre' debe tener m&aacute;ximo 30 caracteres."
			},
			hdnFlagTax:{
				remote: 'El Impuesto Ingresado ya existe en el pa&iacute;s seleccionado.'
			},
			txtPercentageTax: {
				percentage: "El campo 'Porcentaje' debe tener formato de porcentaje.",
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			}
		},
		submitHandler: function(form) {
			$('#btnInsert').attr('disabled', 'true');
			$('#btnInsert').val('Espere por favor...');
			form.submit();			
	   }
	});
	/*END VALIDATION SECTION*/
});

function changeFlag(){
	$('#hdnFlagTax').val($('#hdnFlagTax').val()+1);
}