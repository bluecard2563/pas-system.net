// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmUpdateTax").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameTax: {
				required: true,		
				alphaNumeric: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/tax/checkNameTax.rpc",
					type: "post",
					data: {
						serial_tax: function() {
							return $("#hdnSerial_tax").val();
					  	},
					  	serial_cou: function() {
							return $("#selCountry").val();	
						}
					}
				}
			},
			txtPercentageTax: {
				required: true,
				percentage: true,
				max_value: 100
			},
			selStatus: {
				required: true
			}
		},
		messages: {
			txtNameTax: {
				alphaNumeric: "S&oacute;lo se aceptan caracteres alfanum&eacute;ricos en el campo 'Nombre'.",
				maxlength: "El campo 'Nombre' debe tener m&aacute;ximo 30 caracteres.",
				remote: 'El Impuesto Ingresado ya existe en el pa&iacute;s seleccionado.'
			},
			txtPercentageTax: {
				percentage: "El campo 'Porcentaje' debe tener formato de porcentaje.",
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			}
		}
	});
	/*END VALIDATION SECTION*/
});