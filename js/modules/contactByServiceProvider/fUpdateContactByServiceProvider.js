// JavaScript Document
$(document).ready(function(){
	$('#selZone').change(function(){loadCountries($('#selZone').val());loadCities(0);});
        $('#selZoneProv').change(function(){loadCountriesProv($('#selZoneProv').val());loadProviders(0);});
        $('#selCountryProv').change(function(){loadProviders($('#selCountryProv').val())});
        $('#selCountry').change(function(){loadCities($('#selCountry').val())});

	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
	$("#frmUpdateContactByServiceProvider").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selZone: {
				required: true
			},
            selCountryProv: {
				required: true
			},
			selProvider: {
				required: true
			},
			selZoneProv: {
				required: true
			},
			txtFirstName: {
				required: true,
				textOnly: true
			},
			txtLastName: {
				required: true,
				textOnly: true
			},
			txtPhone: {
				required: true,
				number: true
			},
			txtEmail: {
				required: true,
				email: true
			},
			txtEmail2: {
				email: true,
				notEqualTo: '#txtEmail'
			},
			selType: {
				required: true
			},
			selStatus: {
				required: true
			},
			hdnEmail: {
				remote: {
					url: document_root+"rpc/remoteValidation/contactByServiceProvider/checkContactByServiceProviderEmail.rpc",
					type: "POST",
					data: {
						email: function() {
							return $("#txtEmail").val();
						},
						service_provider: function (){
							return $("#selProvider").val();
						},
						serial_spc: function(){
							return $('#hdnContactID').val();
						},
						origin: '1'
				    }
				}
			},
			hdnEmail2: {
				remote: {
					url: document_root+"rpc/remoteValidation/contactByServiceProvider/checkContactByServiceProviderEmail.rpc",
					type: "POST",
					data: {
						email: function() {
							return $("#txtEmail2").val();
						},
						service_provider: function (){
							return $("#selProvider").val();
						},
						serial_spc: function(){
							return $('#hdnContactID').val();
						},
						origin: '2'
				    }
				}
			}
		},
		messages: {
			txtFirstName: {
				textOnly: 'El campo "Nombre" admite s&oacute;lo texto.'
			},
			txtLastName: {
				textOnly: 'El campo "Apellido" admite s&oacute;lo texto.'
			},
			txtPhone: {
				number: 'El campo "Tel&eacute;fono" admite s&oacute;lo n&uacute;meros.'
			},
			txtEmail: {
				email: 'Ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Contacto"',
				remote: 'El e-mail ingresado ya existe'
			},
			txtEmail2: {
				email: 'Ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail 2"',
				notEqualTo: 'Ingrese otro valor diferente en el campo "E-mail 2"'
			},
			hdnEmail: {
				remote: 'El e-mail ingresado ya existe'
			},
			hdnEmail2: {
				remote: 'El e-mail 2 ingresado ya existe'
			}
   		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/

	/* E-MAIL CHANGES */
	$('#txtEmail').bind('change',function(){
		$('#hdnEmail').val(parseInt($('#hdnEmail').val())+1);
	});
	$('#txtEmail2').bind('change',function(){
		$('#hdnEmail2').val(parseInt($('#hdnEmail2').val())+1);
	});
	$('#selProvider').bind('change',function(){
		$('#hdnEmail').val(parseInt($('#hdnEmail').val())+1);
		$('#hdnEmail2').val(parseInt($('#hdnEmail2').val())+1);
	});
	/* E-MAIL CHANGES */
});

function loadCities(countryID){
	if(countryID==""){
		$("#selCity option[value='']").attr("selected",true);
	}
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID});
}

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){	$('#selCountry').change(function(){loadCities($('#selCountry').val())});});
}

function loadProviders(countryID){
	if(countryID==""){
		$("#selProvider option[value='']").attr("selected",true);
	}
	$('#providerContainer').load(document_root+"rpc/loadProviders.rpc",{serial_cou: countryID});
}

function loadCountriesProv(zoneId){
	if(zoneId==""){
		$("#selCountryProv option[value='']").attr("selected",true);
	}
	$('#countryProvContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId, serial_sel: 'Prov'},function(){	$('#selCountryProv').change(function(){loadProviders($('#selCountryProv').val())});});
}