// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchContactByServiceProvider").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZoneProv: {
				required: true
			},
                        selCountryProv:{
                                required: true
                        },
                        selProvider:{
                                required: true
                        },
                        selContactByServiceProvider:{
                                required: true
                        }
		}
	});
	/*END VALIDATION SECTION*/

        $('#selZoneProv').change(function(){loadCountriesProv($('#selZoneProv').val());loadProviders(0);loadContactsByServiceProvider(0);});
//	var ruta=document_root+"rpc/autocompleters/loadContactsByServiceProvider.rpc";
//	$("#txtContactData").autocomplete(ruta,{
//		max: 10,
//		scroll: false,
//		matchContains:true,
//		minChars: 2,
//  		formatItem: function(row) {
//			 return row[0] ;
//  		}
//	}).result(function(event, row) {
//		$("#hdnContactID").val(row[1]);
//	});
});

function loadContactsByServiceProvider(providerID){
	if(providerID==""){
		$("#selProvider option[value='']").attr("selected",true);
	}
	$('#contactByServiceProviderContainer').load(document_root+"rpc/loadContactsByServiceProvider.rpc",{serial_spv: providerID});
}

function loadProviders(countryID){
	if(countryID==""){
		$("#selProvider option[value='']").attr("selected",true);
	}
	$('#providerContainer').load(document_root+"rpc/loadProviders.rpc",{serial_cou: countryID},function(){
            $('#selProvider').change(function(){
                loadContactsByServiceProvider($('#selProvider').val());
            });
        });
}

function loadCountriesProv(zoneId){
	if(zoneId==""){
		$("#selCountryProv option[value='']").attr("selected",true);
	}
	$('#countryProvContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId, serial_sel: 'Prov'},function(){
            $('#selCountryProv').change(function(){
                loadProviders($('#selCountryProv').val());
                loadContactsByServiceProvider(0);
            });
        });
}