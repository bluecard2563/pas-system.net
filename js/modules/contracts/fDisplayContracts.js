// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/	
	$("#frmNewContract").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtCustomer: {
				required: true
			},
			selProduct: {
				required: true
			},
			txtContractName: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	var ruta = document_root+"rpc/autocompleters/loadCustomers.rpc";
	$("#txtCustomer").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]==0){
				return 'No existen resultados';
			}else{
				return row[0] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]==0){
			$("#hdnCustomerID").val('');
			$("#txtCustomer").val('');
		}else{
			$("#hdnCustomerID").val(row[1]);
		}
	});
});