//Java Script
$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	$.validator.addMethod("enddate", function(value, element) {
		var startdatevalue = $('#txtBeginDate').val();
		//Date.parse IS NOT WORKING PROPERLY
		//return Date.parse(startdatevalue) < Date.parse(value);
		
		//manual comparison:
		var dateBefore = startdatevalue.split("/");
		var dateAfter = value.split("/");
		
		//compare year
		if(dateBefore[2]<dateAfter[2]){
			return true;
		}else if(dateBefore[2]=dateAfter[2]){
			if(dateBefore[1]<dateAfter[1]){
					return true;
			}else if(dateBefore[1]==dateAfter[1]){
				if(dateBefore[0]<dateAfter[0]){
					return true;
				}
			}
		}
		return false;
	});
	
	$("#frmNewOver").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {                            
				required: true,
				alphaNumeric: true,
                remote: {
					url: document_root+"rpc/remoteValidation/overs/checkName.rpc",
					type: "post"
				}
			},
			txtBeginDate:{
				required: true,
                date: true
			},
            txtEndDate:{
				enddate: true,
				required: true,
                date: true
			},
			txtInfoBeginDate:{
				required: true,
                date: true
			},
            txtInfoEndDate:{
				required: true,
                date: true
			},
			increasePer: {
				required: true,
				percentage: true,
				max_value: 100
			},
			comissionPer: {
				required: true,
				percentage: true,
				max_value: 100
			},
			selApliedTo: {
                required: true
            },
            selManager: {
                required: {
                    depends: function(element) {
                             var appliedTo=$('#selApliedTo').val();
                            if(appliedTo=='MANAGER')
                                return true;
                            else
                                return false;
                    }
                }
            },
            dealersTo: {
                required: {
                    depends: function(element) {
                            var appliedTo=$('#selApliedTo').val();

                            if(appliedTo=='DEALER')
                                return true;
                            else
                                return false;
                    	}
                }
            }
		},
		messages: {
			txtName: {
                alphaNumeric: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
                remote: "Ya existe otro over con el mismo nombre."
            },
            txtBeginDate:{
                date:"El campo 'Inicio de Vigencia' debe tener el formato mm/dd/YYYY"
			},
            txtEndDate:{
				enddate:"El 'Inicio de Vigencia' debe ser menor que su fin.",
                date:"El campo 'Fin de Vigencia' debe tener el formato mm/dd/YYYY"
			},
			txtInfoBeginDate:{
                date:"El campo 'Inicio de An&aacute;lisis' debe tener el formato mm/dd/YYYY"
			},
            txtInfoEndDate:{
                date:"El campo 'Fin de An&aacute;lisis' debe tener el formato mm/dd/YYYY",
				enddateInfo: "El 'Fin de An&aacute;lisis' debe ser mayor al Inicio."
			},
			increasePer: {
				percentage: "El campo 'Porcentaje de Incremento' solo admite valores del 0 al 100 y con dos decimales.",
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			},
			comissionPer: {
				percentage: "El campo 'Comisi&oacute;n del Over' solo admite valores del 0 al 100 y con dos decimales.",
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			},
			dealersTo: {
                required: "Es necesario que seleccione al menos un comercializador"
			}
   		},
   		submitHandler: function(form) {
			show_ajax_loader();
            form.submit();
   		}
	});
	
	 $('#selApliedTo').bind('change',function(){
         showApliedInfo(this.value);
     });
	

   /*Calendars*/
    setDefaultCalendar($("#txtBeginDate"),'-2y','+5y','+0d');
    setDefaultCalendar($("#txtEndDate"),'-2y','+5y','+0d');
	/*End Calendars*/
	
	$("#txtInfoBeginDate").bind('change', function(){
		validateDates('#txtBeginDate', '#txtEndDate', '#'+$(this).attr('id'));
	});
	
	$("#txtInfoEndDate").bind('change', function(){
		validateDates('#txtBeginDate', '#txtEndDate', '#'+$(this).attr('id'));
	});
	
	$("#txtBeginDate").bind('change', function(){
		evaluateChanges();
	});
	
	$("#txtEndDate").bind('change', function(){
		evaluateChanges();
	});

    $("#frmNewOver").wizard({
        show: function(element) {},
        next:"Siguiente >",
        prev:"< Anterior",
        action:"Actualizar",
        extra:"ui-state-default ui-corner-all promoButton"
    });
	
});

/*
 * @Name: showApliedInfo
 * @Params: N/A
 * @Description: Displays the section where we select
 *               the country/city etc. to apply the over.
 */
function showApliedInfo(value){
	$('#appliedToContainer').load(document_root+"rpc/loadsOvers/loadOverInfo.rpc",{appliedTo: value});
}

/*
 * @Name: loadCountry
 * @Description: Load all the countries.
 */
function loadCountry(){
    $('#selCountry').change(function(){
    	loadManagersByCountry($('#selCountry').val());
    });
    $('#selCountry').bind('change',function(){
       loadManagersByCountry(this.value);
    });
    
    if($('#selCountry').find('option').size()==2){
        loadManagersByCountry($('#selCountry').val());
    }
}

/*
 * @Name: loadManagersByCountry
 * @Params: countryID
 * @Description: Load all the managers for a specific country.
 */
function loadManagersByCountry(countryID){
   var appliedTo=$('#selApliedTo').val();
 
        $('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: countryID},function(){
			if(appliedTo=='DEALER'){
					$('#selManager').bind('change', function(){
						loadResponsablesByManager($(this).val());
						loadDealers();
					})
			}
		});
		
}

/*
 * @Name: loadResponsablesByManager
 * @Params: serial_mbc
 * @Description: Load all the responsibles for a specific country.
 */
function loadResponsablesByManager(serial_mbc){
     $('#responsibleContainer').load(document_root+"rpc/loadResponsablesByManager.rpc",{serial_mbc: serial_mbc}, function(){
		 $('#selResponsable').bind('change', function(){
			 loadDealers();
			});
     });
}



/*
 * @Name: loadDealers
 * @Params: countryID
 * @Description: Load all the dealers for a specific country.
 */
function loadDealers(){
	var serial_mbc = $('#selManager').val();
	var serial_usr = $('#selResponsable').val();
     var dealerList=new Array();

     //Retrieve the list of dealers that are shown already.
     $("#dealersTo option").each(function (i) {
            dealerList.push($(this).attr('value'));
    });
    dealerList=implode(',', dealerList);
    $('#dealersFromContainer').load(document_root+"rpc/loadBranchByManagerResponsible.rpc",{serial_mbc: serial_mbc,serial_usr:serial_usr,serial_branches: dealerList});
    
}

function loadAppliedTo(appTo,hidOverId,manCountry,manManager){
	//LOADS THE APPLIED TO SELECT AND CALLS FOR CORRESPONDING MANAGER/DEALER LOAD
	$('#appliedToContainer').load(document_root+"rpc/loadsOvers/loadOverInfo.rpc",{appliedTo: appTo, serial_ove: hidOverId, myCountry: manCountry, manManager: manManager},function(){
		if(appTo == 'DEALER'){
			$('#selManager').bind('change', function(){
						loadResponsablesByManager($(this).val());
						loadDealers();
					});
			}
	});
	$("#selApliedTo").val(appTo);
}

function loadManagerData(manCountry,manManager){
	//LOADS COUNTRY AND CALLS LOAD MANAGER SELECT
	$('#managerContainer').load(document_root+"rpc/loadManagersByCountry.rpc",{serial_cou: manCountry, manManager: manManager},function(){
		if($('#selApliedTo').val()=='DEALER'){
			$('#selManager').bind('change', function(){
						loadResponsablesByManager($(this).val());
						loadDealers();
					});
			}
		});
}
function loadManagerSelect(manManager){
	$("#selManager").val(manManager);
}



function validateDates(lower_range, higher_range, value){
	var lower_validation = dayDiff($(lower_range).val(), $(value).val());
	var higher_validation = dayDiff($(higher_range).val(), $(value).val());

	if(lower_validation <= 0 || higher_validation > 1){
		alert('El valor ingresado debe estar dentro de los rangos de vigencia');
		$(value).val('');
	}
}

function evaluateChanges(){
	var begin_date = $('#txtBeginDate').val();
	var end_date = $('#txtEndDate').val();
	
	$("#txtInfoBeginDate").val('');
	$("#txtInfoEndDate").val('');
	
	if(begin_date != '' && end_date != ''){
		$("#txtInfoBeginDate").attr('disabled', false);
		$("#txtInfoEndDate").attr('disabled', false);
		
		$("#txtInfoBeginDate").datepicker('enable');
		$("#txtInfoEndDate").datepicker('enable');
	}else{		
		$("#txtInfoBeginDate").attr('disabled', true);
		$("#txtInfoEndDate").attr('disabled', true);
		
		$("#txtInfoBeginDate").datepicker('disable');
		$("#txtInfoEndDate").datepicker('disable');
	}
}