//Java Script
$(document).ready(function(){
	
	/*FORM VALIDATION SECTION*/
	$("#frmPayOver").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selApliedTo: {
				required: true
			},
			selCountry:{
				required: true
			},
			selOver:{
				required: true
			}
		},
		submitHandler: function(form) {
			submitFilterSearch(form);
			return false;
		}
	});
	
	$("#frmAuthorizeOver").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		submitHandler: function(form) {
			var overSelected = false;
			$(":input[id^='overPay_']").each( function(e){
				if($(this).attr('checked')){
					overSelected = true;
				}
			});
			if(!overSelected){
				alert('Seleccione al menos un Over para Autorizar');
			}else{
				form.submit();
				temporal = window.location;
				window.location = temporal;
			}
		}
	});
	
	$("#frmPayOverName").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {
				required: true
			}
		},
		submitHandler: function(form) {
			submitFilterOverName();
			return false;
		}
	});
	
	var ruta=document_root+"rpc/autocompleters/loadOvers.rpc";
	$("#txtName").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
		formatItem: function(row) {
			return row[0] ;
		},
		extraParams: {
			for_payment: true
		//
		}
	}).result(function(event, row) {
		if(row[0]=="No existen overs con ese nombre."){
			$("#txtName").val('');
		}
		$("#hidOverId").val(row[1]);
	});

	//LOAD COUNTRY WHEN CHANGING APLICA A
	$('#selApliedTo').change(function(){
		loadCountry($('#selApliedTo').val());
	});

	//LOAD OVERS WHEN CHANGING COUNTRY
	$('#selCountry').change(function(){
		loadOver($('#selCountry').val());
	});
});
	
/*
 * @Name: loadOver
 * @Params: countryID
 * @Description: Load all the overs for a specific country.
 */
function loadOver(countryID){
	var appliedTo=$('#selApliedTo').val();
	if(countryID == ""){
		$('#selOver').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}else{
		$('#overContainer').load(document_root+"rpc/loadOvers.rpc",{
			serial_cou: countryID,
			appliedTo: appliedTo
		});
	}
}
function loadCountry(appliedTo){
	if(appliedTo=="DEALER" || appliedTo=="MANAGER"){
		$('#selOver').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#countryContainer').load(document_root+"rpc/loadsOvers/loadCountryOver.rpc",
		{
			appliedTo: appliedTo
		},function(){
			$('#selCountry').change(function(){
				loadOver($('#selCountry').val());
			});
			if($('#selCountry option').size()==2){
				loadOver($('#selCountry').val());
			}
		});
		
	}
	else{
		$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selOver').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
	
}
function submitFilterSearch(form){
	show_ajax_loader();
	var appliedTo = form.selApliedTo.value;
	var countrySel = form.selCountry.value;
	var overId = form.selOver.value;
	
	$('#oversPayResults').load(document_root+"rpc/loadsOvers/loadOversPayResults.rpc",{
		appliedTo: appliedTo,
		countrySel: countrySel,
		overId: overId
	},function(){
		hide_ajax_loader();
	});
	
}

function submitFilterOverName(){
	var overId = $('#hidOverId').val();
	$('#oversPayResults').load(document_root+"rpc/loadsOvers/loadOversPayResults.rpc",{
		overId: overId
	});
}

