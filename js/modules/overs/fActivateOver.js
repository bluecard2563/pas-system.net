//Java Script
$(document).ready(function(){
	/*FORM VALIDATION SECTION*/
	$("#frmActivateOver").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
   		submitHandler: function(form) {
			var overSelected = false;
			$(":input[id^='overActivate_']").each( function(e){
				if($(this).attr('checked')){
					overSelected = true;
				}
			});
			if(!overSelected){
				alert('Seleccione al menos un Over para Activar');
			}else{
				form.submit();
			}
   		}
	});
});