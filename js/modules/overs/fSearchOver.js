var serial_ove;

$(document).ready(function(){

	if($('#overs_table').length>0) {
		/*Style dataTable*/
		currentTable = $('#overs_table').dataTable( {
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"oLanguage": {
				"oPaginate": {
					"sFirst": "Primera",
					"sLast": "&Uacute;ltima",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				},
				"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
				"sZeroRecords": "No se encontraron resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
				"sSearch": "Filtro:",
				"sProcessing": "Filtrando.."
			},
			"sDom": 'T<"clear">lfrtip',
			"oTableTools": {
				"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
				"aButtons": ["pdf","xls" ]
			}
		});
		/*Style dataTable*/

		$('#btnRegistrar').bind('click', function(){
			if(selectedRadio()){
                $('#selectedOver').val(serial_ove);
				form.submit();
			}else{
				//alert('Escoja al menos un Over para actualizar');
			}
		});

	}

function selectedRadio(){
	var selected = false;
	$("input:checked[id^='radOver']", currentTable.fnGetNodes()).each(function(){
		if($(this).is(':checked')){
			selected = true;
			serial_ove = $(this).val();
			return;
		}
	});
	
	return selected;
}

	/*FORM VALIDATION SECTION*/
	$("#frmSearchOver").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selectedOver: {
				required: true
			}
		}
	});
	
});