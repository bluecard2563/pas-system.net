// JavaScript Document
$(document).ready(function(){
        $("#hdnValidateFlag").val(1);
	/*VALIDATION SECTION*/
	$("#frmNewComissionistByCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountryUser: {
				required: true
			},
			selUser: {
				required: true
			},
			txtComission: {
				required: true,
				percentage: true,
				max_value: 100
			},
			selZoneAssign: {
				required: true
			},
			selCountryAssign: {
				required: true
			},
                        hdnValidateFlag:{
                            remote: {
					url: document_root+"rpc/remoteValidation/comissionistByCountry/checkComissionistCountry.rpc",
					type: "post",
					data: {
					  selUser: function() {
						return $("#selUser").val();
					  },
                                          selCountryAssign:function() {
                                              return $("#selCountryAssign").val();
                                          }
					}
				}
                        }
		},
		messages: {
			txtComission: {
				percentage: 'El campo "Comisi&oacute;n" debe tener formato de porcentaje.',
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			},
			hdnValidateFlag: {
				remote: "El usuario seleccionado ya es responsable del pa&iacute;s seleccionado."
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	/*CHANGE OPTIONS*/
	$("#selZone").bind("change", function(e){
	      loadCountry(this.value,"User");
		  loadUsers('');
        });
	$("#selZoneAssign").bind("change", function(e){
	  loadCountry(this.value,"Assign");
	});
	/*END CHANGE*/
});

function loadCountry(zoneID,selectID){
	if(selectID == "User") {
		var container = $('#countryContainer');
	}
	else{
		var container = $('#countryAssignContainer');
	}
	container.load(
	document_root+"rpc/loadCountries.rpc",
	{serial_zon: zoneID, serial_sel: selectID},
	function(){
		$("#selCountryUser").bind("change", function(e){
			loadUsers(this.value);
		})
		if($("#selCountryUser option").size() <= 2){
			loadUsers($("#selCountryUser").val());
		}
                $("#selCountryAssign").bind("change", function(e){
                  $("#hdnValidateFlag").val($("#hdnValidateFlag").val()+1);
                });
	});
	
}

function loadUsers(countryID){
    $('#userContainer').load(document_root+"rpc/loadUserComissionistsByCountry.rpc",
     {serial_cou: countryID},
     function(){$("#selUser").change(function(){
            $("#hdnValidateFlag").val($("#hdnValidateFlag").val()+1);
            if($("#selUser option#"+$("#selUser").val()).attr('isComissionist')=="true"){
                    alert("El usuario seleccionado es responsable de una o m\u00e1s sucursales de comercializador en su pa\u00eds.");
            }
            $('#supervisorContainer').load(document_root+"rpc/loadComissionistsByCountry.rpc",
                                    {serial_usr: this.value})
        });
     });
}