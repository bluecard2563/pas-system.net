// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmDeactivateComissionistByCountry").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selZone: {
                required: true
            },
            selCountry: {
                required: true
            }
        }
    });
    /*END VALIDATION SECTION*/

    /*CHANGE OPTIONS*/
    $("#selZone").bind("change", function(e){
        loadCountry(this.value);
        loadComissionists('');
    });
    /*END CHANGE*/
});

function loadCountry(zoneID){
    $('#countryContainer').load(
    document_root+"rpc/loadCountries.rpc",
    {serial_zon: zoneID},
    function(){
        $("#selCountry").bind("change", function(e){
            loadComissionists(this.value);
        })
        if($("#selCountry option").size() <= 2){
            loadComissionists($("#selCountryUser").val());
        }
    });
}

function loadComissionists(countryID) {
    $('#comissionistList').load(document_root+"rpc/loadActiveComissionistsByCountry.rpc", {serial_cou: countryID});
}