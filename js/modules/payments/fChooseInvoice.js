// JavaScript Document
/*
File:fChooseInvoice.js
Author: Edwin Salvador
Creation Date:10/03/2010
Modified By:
Last Modified: 
*/
var currentTable;
$(document).ready(function(){
	//
	if($('#printCreditNote').length>0){
		$('#printCreditNote').bind('click',function(){
			$('#printCreditNote').html('');
		});
	}

	$("#selCountry").bind("change", function(){
		loadCitiesByCountry($(this).val());
	});
	if($("#selCountry").val()){
		loadCitiesByCountry($("#selCountry").val());
	}
    
	$("#selCategory").bind("change", function(e){
		$('#dealerContainer').html('');
		$('#invoiceContainer').html('');
	});

	$("#searchBranches").bind("click", function(){
		$("#alerts").html('');
		$("#alerts").css('display','none');
		evaluateFilters();
	});

	/*SECCION DE VALIDACIONES DEL FORMULARIO*/
	$("#frmChooseInvoice").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selectedInvoices: {
				required: true
			}
		},
		submitHandler: function(form) {
			var sData = $(".invoiceChk", currentTable.fnGetNodes()).serialize();
			$('#selectedBoxes').val(sData);
			var sData = $(".hdnInvoiceToSerial", currentTable.fnGetNodes()).serialize();
			$('#hdnInvoiceToSerialAux').val(sData);
			var sData = $(".hdnInvoiceTo", currentTable.fnGetNodes()).serialize();
			$('#hdnInvoiceToAux').val(sData);
			var sData = $(".hdnSerialInv", currentTable.fnGetNodes()).serialize();
			$('#hdnSerialInvAux').val(sData);

			form.submit();
		}
	});
/*FIN DE LA SECCION DE VALIDACIONES*/
});
/*EVALUATE IF THE USER HAS SELECTED
 *THE REQUIRED FIELDS TO SEARCH*/
function evaluateFilters(){
	var next=true;
	if(!$("#selCountry").val()){
		$("#alerts").append("<li><label class='error_validation'>Seleccione un Pa&iacute;s.</label></li>");
		$("#alerts").css('display','block');
		next = false;
	}
	if(!$("#selCity").val()){
		$("#alerts").append("<li><label class='error_validation'>Seleccione una Ciudad.</label></li>");
		$("#alerts").css('display','block');
		next = false;
	}
	if(next){
		var serial_cit=($("#selCity").val()=='')?false:$("#selCity").val();
		var serial_usr=($("#selResponsable").val()=='')?false:$("#selResponsable").val();
		var dealer_cat=($("#selCategory").val()=='')?false:$("#selCategory").val();
		loadDealers(serial_cit,serial_usr,dealer_cat);
	}
}
/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(countryId){
	if(countryId){//IF A COUNTRY HAS BEEN SELECTED
		$('#cityContainer').load(document_root+"rpc/loadPaymentCities.rpc", {
			serial_cou: countryId
		}, function(){
			$('#selCity').bind('change', function(){
				loadResponsablesByCity($(this).val());
			});
			loadResponsablesByCity($("#selCity").val());//
		});
	}else{
		$('#cityContainer').html('Seleccione un pa\u00EDs');
		loadResponsablesByCity('');
	}
}
/*CALL TO THE RPC TO LOAD THE RESPONSABLES
 *BASED ON THE CITY SELECTED*/
function loadResponsablesByCity(cityId){
	if(cityId){//IF A CITY HAS BEEN SELECTED
		$('#responsableContainer').load(document_root+"rpc/loadPaymentResponsablesByCity.rpc",{
			serial_cit: cityId
		},function (){
			$("#selResponsable").bind("change", function(){
				$('#dealerContainer').html('');
				$('#invoiceContainer').html('');
			});
		});
	}else{
		$('#responsableContainer').html('Seleccione una ciudad');
	}
	$('#dealerContainer').html('');
	$('#invoiceContainer').html('');
}
/*CALL TO THE RPC TO LOAD THE DEALERS
 *BASED ON THE RESPONSABLE, CITY
 *AND THE CATEGORY (OPTIONAL)*/
function loadDealers(serial_cit,serial_usr,dealer_cat){
	$('#dealerContainer').load(document_root+"rpc/loadPaymentDealers.rpc",{
		serial_cit:serial_cit,
		serial_usr: serial_usr,
		dealer_cat: dealer_cat
	},function(){
		$("#selDealer").bind('change', function(){
			loadBranches($(this).val());
		});
		loadBranches($("#selDealer").val());
	});
}
/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
	if(dealerID){
		$("#branchContainer").load(document_root+"rpc/loadPaymentBranches.rpc",{
			dea_serial_dea: dealerID
		},function(){
			$("#selBranch").bind("change", function(){
				loadInvoices($(this).val());
			});
			loadInvoices($("#selBranch").val());
		});
	}else{
		$("#branchContainer").html('Seleccione un Dealer');
		loadInvoices('');
	}
}
/*CALL TO THE RPC TO LOAD THE INVOICES
 *WITH THE STAND-BY STATUS
 *BASED ON THE BRANCH SELECTED */
function loadInvoices(branchID){
	if(branchID){
		//get invoices status stand-by
		$('#invoiceContainer').load(document_root+"rpc/loadInvoicesByBranch.rpc",{
			serial_dea: branchID
		}, function(){

			if($('#invoicesTable').length>0){

				/*Style dataTable*/
				currentTable = $('#invoicesTable').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": {
						"oPaginate": {
							"sFirst": "Primera",
							"sLast": "&Uacute;ltima",
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						},
						"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
						"sZeroRecords": "No se encontraron resultados",
						"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
						"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
						"sSearch": "Filtro:",
						"sProcessing": "Filtrando.."
					},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
						"aButtons": ["pdf" ]
					}

				} );

			}

			if($("#selAll")){
				$("#selAll").bind("click",checkAll);
				$(".invoiceChk",currentTable.fnGetNodes()).each(function(){
					$(this).bind('click',function(){
						invoiceChecked($(this));
					});
				});
			}
		});
	}else{
		$('#invoiceContainer').html('');
	}
}

/*CALL TO THE RPC TO LOAD THE INVOICES
 *WITH THE STAND-BY STATUS
 *BASED ON THE BRANCH SELECTED */
function printInvoices(branchID){
	if(branchID){
		//get invoices status stand-by
		window.open(document_root+"rpc/loadInvoicesByBranch.rpc/"+branchID , "PDF" , "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no");
	}
}

//checkAll
//select\deselect all the invoices
function checkAll() {
	$(".invoiceChk",currentTable.fnGetNodes()).each(function(){
		if($('#selAll').is(':checked')){
			if($(this).attr('payment') == ''){
				$(this).attr('checked',true);
			}else{
				$(this).attr('checked',false);
			}
		}else{
			$(this).attr('checked',false);
		}
	});
	invoiceChecked();
}
/*invoiceChecked
 * CHECKS IF THERE IS AN INVOICE
 * THAT HAD BEEN PARTIALLY PAYED
 * TO CONTROL THAT HAS TO BE PAYED INDIVUDUALY
 * AND CONTROL THAT AT LEAT ONE INVOICE HAS BEEN SELECTED
 */
function invoiceChecked(check){
	var somechecked = 0;
	var payed = 0;
	var checked = 0;
	if($(check).is(':checked')){//IF THE CLICKED CHECKBOX IS SELECTED
		if($(check).attr('payment') != ''){//IF THE SELECTED INVOICE HAS THE PAYMENT ATTRIBUTE
			$('#payedSelected').val('1');//TO CONTROL THAT A PARTIALLY PAYED INVOICE HAS BEEN SELECTED
		}
		$(".invoiceChk",currentTable.fnGetNodes()).each(function(){//FOREACH INVOICE
			if($(this).attr('id') != $(check).attr('id')){//EXCLUDE THE CURRENT SELECTED INVOICE
				if($(this).is(':checked')){//IF THE INVOICE IS SELECTED
					somechecked = 1;//CONTROL THAT THERE IS A SELECTED INVOICE NOT THE CURRENT SELECTED
					if($(this).attr('payment') != ''){
						payed = 1;//TO CONTROL THAT THERE IS A SELECTED INVOICE AND IS A PARTIALLY PAYED INVOICE
					}
					if(payed == 1){
						$('#payedSelected').val('1');//TO CONTROL THAT A PARTIALLY PAYED INVOICE HAS BEEN SELECTED
					}else{
						$('#payedSelected').val('0');
					}
				}
			}
		});
		//IF THERE IS A SELECTED INVOICE AND IS A PAYED INVOICE OR IF THERE IS A SELECTED INVOICE AND THE CURRENT SELECTED INVOICE IS A PARTIALLY PAYED INVOICE
		if((somechecked == 1 && $('#payedSelected').val() == '1') || (somechecked == 1 && $(check).attr('payment') != '')){
			$(check).attr('checked',false);
			alert('Las facturas que tienen abonos realizados deben pagarse individualmente.');
		}
	}
	$(".invoiceChk").each(function(){
		if($(this).is(':checked')){
			checked = 1;
		}
	});
	if(checked == 1){
		$('#selectedInvoices').val('1');
	}else{
		$('#selectedInvoices').val('');
	}
}
/*
 * @Name: openReport
 * @Description: Displays the pdf file.
 **/
function openReport(paymentNoteNumber){
	window.open(document_root+'modules/payments/pdfReports/payment_note_'+paymentNoteNumber+'.pdf', 'reportPaymentNote');
	$.ajax({
		type: "POST",
		url: document_root+"rpc/deletePaymentNoteReport.rpc",
		data: "url=modules/payments/pdfReports/payment_note_"+paymentNoteNumber+".pdf",
		success: function(msg){
			if(msg=='true'){
				window.location = document_root+"modules/payments/fChooseInvoice";
			}
		}
	});
}


