// JavaScript Document
var currentTable;
$(document).ready(function(){
	if($("#frmUncollectableInvoices").length>0){
		/*SECCION DE VALIDACIONES DEL FORMULARIO*/
		$("#frmUncollectableInvoices").validate({
			errorLabelContainer: "#alerts",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				selManager: {
					required: true
				}
			},
			submitHandler: function(form) {
				show_ajax_loader();
				
				var sData = $("input:checked[id^='check_']", currentTable.fnGetNodes()).serialize();
				$('#selectedBoxes').val(sData);
				form.submit();

			}
		});
		/*FIN DE LA SECCION DE VALIDACIONES*/

		$('#selManager').bind('change',function(){
			loadUncollectableInvoices();
		});
		
		if($('#selManager :option').size() == 2){
			loadUncollectableInvoices();
		}
	}	
});

/*
 * @name: loadUncollectableInvoices
 * @description: Function that loads all the uncollectable invoices
 *               for a specific manager in a table, so that they can be
 *				 dispatched.
 */
function loadUncollectableInvoices(){
	var manager=$('#selManager').val();
	var param=$('#hdnParameter').val();
	
	if(manager!=''){
		show_ajax_loader();
		
		$('#invoiceContainer').load(document_root+'rpc/loadPayments/loadUncollectableInvoicesByManager.rpc',{
			serial_mbc: manager, 
			parameter: param
		}, function(){
			if($('#uncollectableTable').length>0){
				currentTable = $('#uncollectableTable').dataTable( {
					"aoColumns": [
					{
						"bSortable": false
					},
					null,
					null,
					{
						"sType": "uk_date"
					},

					{
						"sType": "uk_date"
					},
					null,
					null,
					null
					],
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": {
						"oPaginate": {
							"sFirst": "Primera",
							"sLast": "&Uacute;ltima",
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						},
						"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
						"sZeroRecords": "No se encontraron resultados",
						"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
						"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
						"sSearch": "Filtro:",
						"sProcessing": "Filtrando.."
					},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
						"aButtons": ["pdf","xls" ]
					}

				} );

				$('#hdnValidateOne').rules('add', {
					required: true
				})
	
				$("input[id^='check_']", currentTable.fnGetNodes()).each(function(){
					$(this).bind('change',function(){
						validate_one();
					});
				});

				$('#checkAll').bind('click',function(){
					checkAll();
					validate_one();
				});
			}
			
			hide_ajax_loader();
		});
	}else{
		$('#invoiceContainer').html('');
	}
}

/*
 * @name: validate_one
 * @description: Validates if at least one register is check so the form can be submited.
 */
function validate_one(){
	var one_selected=false;

	$("input[id^='check_']", currentTable.fnGetNodes()).each(function(){
		if($(this).is(':checked')){
			one_selected=true;
			return;
		}
	});

	if(one_selected){
		$('#hdnValidateOne').val('1');
	}else{
		$('#hdnValidateOne').val('');
	}
}

/*
 * @name: checkAll
 * @description: checks or unchecks all the registers in the uncollectable table.
 */
function checkAll(){
	if($('#checkAll').is(':checked')){
		$("input[id^='check_']", currentTable.fnGetNodes()).each(function(){
			$(this).attr('checked','true');
		});
	}else{
		$("input[id^='check_']", currentTable.fnGetNodes()).each(function(){
			$(this).removeAttr('checked');
		});
	}
}