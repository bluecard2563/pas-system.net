// JavaScript Document
/*
File:fChoosePaymentToVoid.js
Author: Edwin Salvador
Creation Date:19/03/2010
Modified By:
Last Modified: 
*/
$(document).ready(function(){

	/*SECCION DE VALIDACIONES DEL FORMULARIO*/
	$("#frmChoosePaymentToVoid").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                txtInvoice: {
                    required: true,
					remote: {
						url:document_root+"rpc/remoteValidation/payments/checkPayment.rpc",
						type: "post"
					}
                }
        },
		messages: {
				txtInvoice: {
					remote: "No se puede anular los pagos realizados a esta factura. Los motivos pueden ser:<br />- El n&uacute;mero de factura no existe o no tiene pagos realizados.<br />- Una de las ventas que corresponden a esta factura ha sido reembolsada."
				}
		}
    });
    /*FIN DE LA SECCION DE VALIDACIONES*/
});

