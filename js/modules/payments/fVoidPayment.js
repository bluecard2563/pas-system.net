// JavaScript Document
/*
File:fNewPayment.js
Author: Edwin Salvador
Creation Date:19/03/2010
Modified By:
Last Modified:
*/
$(document).ready(function(){
    //VALIDATION
    $("#frmVoidPayment").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                paymentType_0: {
                        required: true
                }
        },
        submitHandler: function(form){
			$('#btnSave').attr('disabled','true');
			$('#btnSave').val('Espere por favor...');
			var pending = parseFloat($('#total_pending').val());
			if(pending == 0){
				if(confirm("Se cubre la totalidad de las factura.\n Se registrar\u00E1 como pagado. Desea continuar?")){
					$('#status_pay').val('PAID');
					form.submit();
				}
			}else{
				if(pending < 0){
					if(confirm("El pago excede el total a pagar.\n Se registrar\u00E1 como pago en exceso. Desea continuar?")){
                        $('#status_pay').val('EXCESS');
                        form.submit();
                    }
				}else{
					if(pending >0){
						if(pending == parseFloat($('#total_inv').val())){
							if(confirm("Est\u00E1 anulando todos los pagos. Desea continuar?")){
								$('#status_pay').val('VOID');

								form.submit();
							}
						}else{
							if(confirm("El pago no cubre el total a pagar.\n Se registrar\u00E1 como un abono. Desea continuar?")){
								$('#status_pay').val('PARTIAL');
								form.submit();
							}
						}
					}
				}
			}
			$('#status_pay').val('');
			$('#btnSave').removeAttr('disabled');
			$('#btnSave').val('Guardar Cambios');
        }
    });
    //END VALIDATION
	//events
    /*$('#btnAddForm').bind('click', function(){
        addPaymentForm();
    });*/
	//end events

});

//delete a payment form
function changeStatus(a, hidden, amount, observation){
	var sumActive;
	var sumVoid;
	var pending;

	if($(a).html() == 'Activo'){
		sumActive = parseFloat($('#sumActive').html()) - parseFloat(amount);
		sumVoid = parseFloat($('#sumVoid').html()) + parseFloat(amount);
		
		$(a).html('Anulado');
		$(a).css('color','#FF0000');
		$('#'+hidden).val('VOID');
		$(':text#'+observation).removeAttr('disabled');
		$(':text#'+observation).rules('add',{
            required: true
        });
	}else{
		sumActive = parseFloat($('#sumActive').html()) + parseFloat(amount);
		sumVoid = parseFloat($('#sumVoid').html()) - parseFloat(amount);

		$(a).html('Activo');
		$(a).css('color','#008000');
		$('#'+hidden).val('ACTIVE');
		$(':text#'+observation).attr('disabled', 'disabled');
		$(':text#'+observation).val('');
		$(':text#'+observation).rules('remove');

		$('#observation_all').attr('disabled','disabled').rules('remove');
		$.each($('.observation'),function(){
			if($(this).attr('disabled') == ''){
				$(this).rules('add',{
					required: true
				});
			}
		});
	}

	$('#sumActive').html(sumActive.toFixed(2));
	$('#sumVoid').html(sumVoid.toFixed(2));

	$('#total_payed').val(sumActive.toFixed(2));

	pending = parseFloat($('#total_inv').val()) - parseFloat(sumActive);
	$('#pending').html(pending.toFixed(2));
	$('#total_pending').val($('#pending').html());
}

function voidAll(){
		$('a.status').html('Anulado').css('color','#FF0000');
		$('.status_hidden').val('VOID');

		$('#observation_all').removeAttr('disabled').rules('add',{
            required: true
        });

		$.each($('.observation'),function(){$(this).removeAttr('disabled').rules('remove')});
				
		sumActive = parseFloat('0.00');
		sumVoid = parseFloat($('#sumVoid').html()) + parseFloat($('#sumActive').html());

		$('#sumActive').html(sumActive.toFixed(2));
		$('#sumVoid').html(sumVoid.toFixed(2));

		$('#total_payed').val(sumActive.toFixed(2));

		pending = parseFloat($('#total_inv').val()) - parseFloat(sumActive);
		$('#pending').html(pending.toFixed(2));
		$('#total_pending').val($('#pending').html());
}
