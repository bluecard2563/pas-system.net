// JavaScript Document
$(document).ready(function(){
	$("#selCountry").bind("change", function(){
		loadCitiesByCountry($(this).val());
	});
	if($("#selCountry").val()){
		loadCitiesByCountry($("#selCountry").val());
	}

	$("#selCategory").bind("change", function(e){
		$('#dealerContainer').html('');
		$('#paymentsContainer').html('');
	});

	$("#searchBranches").bind("click", function(){
		$("#alerts").html('');
		$("#alerts").css('display','none');
		evaluateFilters();
	});

	/*SECCION DE VALIDACIONES DEL FORMULARIO*/
	$("#frmExcessPayments").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selectedPayments: {
				required: true
			}
		}
	});
/*FIN DE LA SECCION DE VALIDACIONES*/
});

/*EVALUATE IF THE USER HAS SELECTED
 *THE REQUIRED FIELDS TO SEARCH*/
function evaluateFilters(){
	var next=true;
	if(!$("#selCountry").val()){
		$("#alerts").append("<li><label class='error_validation'>Seleccione un Pa&iacute;s.</label></li>");
		$("#alerts").css('display','block');
		next = false;
	}
	if(!$("#selCity").val()){
		$("#alerts").append("<li><label class='error_validation'>Seleccione una Ciudad.</label></li>");
		$("#alerts").css('display','block');
		next = false;
	}
	if(!$("#selResponsable").val()){
		$("#alerts").append("<li><label class='error_validation'>Seleccione un Representante.</label></li>");
		$("#alerts").css('display','block');
		next = false;
	}
	if(next){
		var serial_cit=($("#selCity").val()=='')?false:$("#selCity").val();
		var serial_usr=($("#selResponsable").val()=='')?false:$("#selResponsable").val();
		var dealer_cat=($("#selCategory").val()=='')?false:$("#selCategory").val();
		loadDealers(serial_cit,serial_usr,dealer_cat);
	}
}

/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED AND
 *HAVE EXCESS PAYMENTS*/
function loadCitiesByCountry(countryId){
	if(countryId){//IF A COUNTRY HAS BEEN SELECTED
		$('#cityContainer').load(document_root+"rpc/loadPayments/fLoadCitiesWithExcessPayments.rpc", {
			serial_cou: countryId
		}, function(){
			$('#selCity').bind('change', function(){
				loadResponsablesByCity($(this).val());
			});
			loadResponsablesByCity($("#selCity").val());//
		});
	}else{
		$('#cityContainer').html('Seleccione un pa\u00EDs');
		loadResponsablesByCity('');
	}
}

/*CALL TO THE RPC TO LOAD THE RESPONSABLES
 *BASED ON THE CITY SELECTED AND
 *HAVE EXCESS PAYMENTS*/
function loadResponsablesByCity(cityId){
	if(cityId){//IF A CITY HAS BEEN SELECTED
		$('#responsableContainer').load(document_root+"rpc/loadPayments/fLoadResponsablesWithExcessPayments.rpc",{
			serial_cit: cityId
		},function (){
			$("#selResponsable").bind("change", function(){
				$('#dealerContainer').html('');
				$('#paymentsContainer').html('');
			});
		});
	}else{
		$('#responsableContainer').html('Seleccione una ciudad');
	}
	$('#dealerContainer').html('');
	$('#paymentsContainer').html('');
}

/*CALL TO THE RPC TO LOAD THE DEALERS
 *BASED ON THE RESPONSABLE, CITY
 *AND THE CATEGORY (OPTIONAL) AND HAVE
 *EXCESS PAYMENTS*/
function loadDealers(serial_cit,serial_usr,dealer_cat){
	if(serial_usr){
		$('#dealerContainer').load(document_root+"rpc/loadPayments/loadDealersWithExcessPayments.rpc",
		{
			serial_cit: serial_cit,
			serial_usr: serial_usr,
			dealer_cat: dealer_cat
		},function(){
			$("#selDealer").bind('change', function(){
				loadBranches($(this).val());
			});
			loadBranches($("#selDealer").val());
		});
	}else{
		$('#dealerContainer').html('Seleccione un responsable');
		loadBranches('');
	}
}

/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
	if(dealerID){
		$("#branchContainer").load(document_root+"rpc/loadPayments/loadBranchesWithExcessPayments.rpc",{
			dea_serial_dea: dealerID
		},function(){
			$("#selBranch").bind("change", function(){
				loadExcessPayments($(this).val());
			});
			loadExcessPayments($("#selBranch").val());
		});
	}else{
		$("#branchContainer").html('Seleccione un Dealer');
		loadExcessPayments('');
	}
}

/*CALL TO THE RPC TO LOAD THE INVOICES
 *WITH THE STAND-BY STATUS
 *BASED ON THE BRANCH SELECTED */
function loadExcessPayments(branchID){
	if(branchID){
		var countryID=$('#selCountry').val();
		$('#paymentsContainer').load(document_root+"rpc/loadPayments/loadExcessPaymentsTable.rpc",
		{
			serial_dea: branchID,
			serial_cou: countryID
		}, function(){
				
			if($("#selAll")){
				$("#selAll").bind("change",checkAll);
				$(".paymentsChk").each(function(){
					$(this).bind('click',function(){
						invoiceChecked();
					});
				});
				
				$("#selectedPayments").rules("add", {
					required: true
				});
			}
			
			if($('#paymentsTable').length>0){
				//Paginator
				pager = new Pager('paymentsTable', 5 , 'Siguiente', 'Anterior');
				pager.init(7); //Max Pages
				pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); //Starting page
			}
			
			$('#btnSubmit').bind('click', function(){
				if($("#frmExcessPayments").valid()){
					$("#alerts").html('');
					$("#alerts").css('display','none');
					var equalto=true;
					var in_favor_to='';
					$(".paymentsChk").each(function(i){
						if($(this).is(':checked')){
							if(in_favor_to==''){
								in_favor_to=$(this).attr('in_favor_to');
							}else{
								if($(this).attr('in_favor_to')!=in_favor_to){
									equalto=false;
									return;
								}
							}
						}
					});
					
					if(equalto==true){
						$("#frmExcessPayments").submit();
						$('#dealerContainer').html('');
						$('#paymentsContainer').html('');
					}else{
						$("#alerts").append("<li><label class='error_validation'>Se puede liquidar pagos en exceso unicamente del mismo beneficiario.</label></li>");
						$("#alerts").css('display','block');
					}
				}
			});
		});
	}else{
		$('#paymentsContainer').html('');
	}
}

//checkAll
//select\deselect all the invoices
function checkAll() {
	$(".paymentsChk").each(function(){
		if($('#selAll').is(':checked')){
			$(this).attr('checked',true);
		}else{
			$(this).attr('checked',false);
		}
	});
	invoiceChecked();
}

/*invoiceChecked
 * CHECKS IF THERE IS AN INVOICE
 * THAT HAD BEEN PARTIALLY PAYED
 * TO CONTROL THAT HAS TO BE PAYED INDIVUDUALY
 * AND CONTROL THAT AT LEAT ONE INVOICE HAS BEEN SELECTED
 */
function invoiceChecked(){
	var items_checked = new Array();
	
	$(".paymentsChk").each(function(){
		if($(this).is(':checked')){
			array_push(items_checked, $(this).val());
		}
	});
	
	if(count(items_checked) > 0){
		$('#selectedPayments').val(implode(',', items_checked));
	}else{
		$('#selectedPayments').val('');
	}
}
