// JavaScript Document
/*
File:fPayedAccounts.js
Author: Mario L�pez
Creation Date:26/03/2010
Modified By: Gabriela Guerrero
Last Modified:
*/
$(document).ready(function(){
//
    $("#selCountry").bind("change", function(){
        loadCitiesByCountry($(this).val());
    });

	$("#selStatus").bind("change", function(){
        if($(this).val() == 'CxC'){
			$("#chkDetallado").attr('checked', false);
			$("#chkDetallado").attr('disabled', true);
		}
		else{
			$("#chkDetallado").attr('disabled', false);
		}
    });

    $("#selCategory").bind("change", function(e){
        var serial_usr=($("#selResponsable").val()=='')?false:$("#selResponsable").val();
        //var dealer_cat=($("#selCategory").val()=='')?false:$("#selCategory").val();
        if(serial_usr!=''){
            loadDealers(serial_usr);
        }
    });
});

/*CALL TO THE RPC TO LOAD THE CITIES
 *BASED ON THE COUNTRY SELECTED*/
function loadCitiesByCountry(countryId){
    if(countryId){//IF A COUNTRY HAS BEEN SELECTED
        $('#cityContainer').load(document_root+"rpc/loadPayments/reports/loadCitiesByCountry.rpc", {serial_cou: countryId}, function(){
            $('#selCity').bind('change', function(){
               loadResponsablesByCity($(this).val());
            });
            loadResponsablesByCity($("#selCity").val());//
        });
    }else{
        $('#cityContainer').html('Seleccione un pa\u00EDs');
        loadResponsablesByCity('');
    }
}
/*CALL TO THE RPC TO LOAD THE RESPONSABLES
 *BASED ON THE CITY SELECTED*/
function loadResponsablesByCity(cityId){
    if(cityId){//IF A CITY HAS BEEN SELECTED
        $('#responsableContainer').load(document_root+"rpc/loadPayments/reports/loadResponsablesByCity.rpc",{serial_cit: cityId},function (){
            $("#selResponsable").bind("change", function(){
                //var serial_usr = ( $("#selResponsable").val()=='' ) ? false : $("#selResponsable").val();
                //var dealer_cat=($("#selCategory").val()=='')?false:$("#selCategory").val();
                loadDealers($("#selResponsable").val());
            });
        });
    }else{
       $('#responsableContainer').html('Seleccione una ciudad');
    }
    $('#dealerContainer').html('');
}
/*CALL TO THE RPC TO LOAD THE DEALERS
 *BASED ON THE RESPONSABLE
 *AND THE CATEGORY (OPTIONAL)*/
function loadDealers(responsableId){
    if(responsableId && $("#selCountry").val()){
        $('#dealerContainer').load(document_root+"rpc/loadPayments/reports/loadDealersByResponsable.rpc",{serial_usr: responsableId, serial_cou: $("#selCountry").val()},function(){
            $("#selDealer").bind('change', function(){
                loadBranches($(this).val());
            });
            loadBranches($("#selDealer").val());
        });
    }else{
       $('#dealerContainer').html('');
       loadBranches('');
    }
}
/*CALL TO THE RPC TO LOAD THE BRANCHES
 *BASED ON THE DEALER SELECTED */
function loadBranches(dealerID){
    if(dealerID && $("#selCity").val()){
	$("#branchContainer").load(document_root+"rpc/loadPayments/reports/loadBranchesByDealer.rpc",{dea_serial_dea: dealerID, serial_cit: $("#selCity").val()});
    }else{
        $("#branchContainer").html('Seleccione un Dealer');
    }
}

function checkInvoices(){
    if($('#selCountry').val()){
        var city = '', user = '', category = '', dealer = '', branch = '', status = '';
        $("#messageContainer").hide();
        if($('#selCity').val()){
            city = $('#selCity').val();
        }
        if($('#selResponsable').val()){
            user = $('#selResponsable').val();
        }
        if($('#selCategory').val()){
            category = $('#selCategory').val()
        }
        if($('#selDealer').val()){
            dealer = $('#selDealer').val();
        }
        if($('#selBranch').val()){
            branch = $('#selBranch').val();
        }
        if($('#selStatus').val()){
            status = $('#selStatus').val();
        }
        $.ajax({
           type: "POST",
           url: document_root+"rpc/checkPayedAccounts.rpc",
           data: ({serial_cou : $('#selCountry').val(),
                    serial_cit : city,
                    serial_usr : user,
                    category_dea : category,
                    serial_dea : dealer,
                    dea_serial_dea : branch,
                    status_pay : status
           }),
           success: function(msg){
             if(msg == 'true'){
                 $('#emptyResult').html('');
                 $('#frmPayedAccounts').submit();
             }else{
                 $('#emptyResult').html('No existen resultados que coincidan con el criterio de b&uacute;squeda');
             }
           }
         });
    }else{
        $("#messageContainer").show();
    }
}