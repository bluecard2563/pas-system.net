// JavaScript Document
/*
File: fRegisterPayment.js
Author: Gabriela Guerrero
Creation Date:16/06/2010
Modified By: Santiago Arellano
Last Modified: Date:11/07/2011
*/

var currentTable
$(document).ready(function(){

	$("#selCountry").bind("change", function(){
		loadManagersByCountry($(this).val());
		$("#tableContainer").html('');
	});

	$("#txtDateFrom").bind("change", function(){
		$("#tableContainer").html('');
	});
	$("#txtDateTo").bind("change", function(){
		$("#tableContainer").html('');
	});
	setDefaultCalendar($("#txtDateFrom"),'-100y','+0d');
	setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
	
});

/*FUNCTIONS*/
/*VALIDATE DATES RANGE*/
function validateDates(){
	var date = new Date();
	var today = (date.getDate()+1)+"/"+(date.getMonth()+1)+"/"+date.getFullYear();

	var days=dayDiff($("#txtDateFrom").val(),$("#txtDateTo").val());
	if(days<0){
		alert("La Fecha Hasta no puede ser menor a la Fecha Desde");
		return false;
	}
	days=dayDiff($("#txtDateTo").val(),today);
	if(days<0){
		alert("La fecha hasta no puede ser mayor a la fecha actual.");
		return false;
	}
	return true;
}
/*CALL TO THE RPC TO LOAD THE MANAGERS
 *BASED ON THE COUNTRY SELECTED*/
function loadManagersByCountry(countryId){
	if(countryId){//IF A COUNTRY HAS BEEN SELECTED
		$('#managerContainer').load(document_root+"rpc/loadPayments/loadManagersByCountry.rpc", {
			serial_cou: countryId
		});
	}else{
		$('#managerContainer').html('Seleccione un Pa&iacute;s');
		$("#tableContainer").html('');
	}
}

function submitForm(){
	var num = 0;
	$(".paymentsChk", currentTable.fnGetNodes()).each(function(){
		if($(this).is(':checked')){
			num = 1;
		}
	});
	if(num){
		return true;
	}
	else{
		$("#leastOne").show();
		return false;
	}
}

function checkPayments(){	
	$("#messageCountry").hide();
	$("#messageManager").hide();
	$("#messageDates").hide();

	var datesStatus=false;
	if( ($("#txtDateFrom").val()!='' && $("#txtDateTo").val()!='') || ($("#txtDateFrom").val()=='' && $("#txtDateTo").val()=='')){
		datesStatus = true;
	}

	if( $('#selCountry').val() && $('#selManager').val() && validateDates() && datesStatus){

		var  dateFrom = '', dateTo = '', country = $('#selCountry').val(), manager = $('#selManager').val();

		if($('#txtDateFrom').val() && $('#txtDateTo').val()){
			dateFrom = $('#txtDateFrom').val();
			dateTo = $('#txtDateTo').val();
		}
		show_ajax_loader();
		$('#tableContainer').load(document_root+"rpc/loadPayments/loadPaymentDetailTable.rpc",
		{
			serial_cou: country,
			serial_mbc: manager,
			dateFrom: dateFrom,
			dateTo: dateTo
		},
		function(){
			
			if($("#selAll")) $("#selAll").bind("click",checkAll);

			if($('#paymentsTable').html()){
				/*Style dataTable*/
				currentTable = $('#paymentsTable').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": {
						"oPaginate": {
							"sFirst": "Primera",
							"sLast": "&Uacute;ltima",
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						},
						"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
						"sZeroRecords": "No se encontraron resultados",
						"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
						"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
						"sSearch": "Filtro:",
						"sProcessing": "Filtrando.."
					},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
						"aButtons": ["pdf" ]
					}

				} );
			}
			hide_ajax_loader();

			$("#submitBtn").bind("click",function(){

				var sData = $(".paymentsChk", currentTable.fnGetNodes()).serialize();
				$('#selectedBoxes').val(sData);

				show_ajax_loader();
				$.getJSON(document_root+"rpc/payments/createRegisterPaymentDocument.rpc",
				{
					selectedBoxes : $('#selectedBoxes').val(),
					dateFrom : $('#txtDateFrom').val(),
					dateTo : $('#txtDateTo').val(),
					selCountry : $('#selCountry').val(),
					selManager : $('#selManager').val()
				},
				function(data){
					
					if(data=='3'){
						location.href=document_root+"modules/payments/fRegisterPayment/3";
					}else
					if(data=='2'){
						location.href=document_root+"modules/payments/fRegisterPayment/2";
					}else
					if(data){
						if(window.open(document_root+"system_temp_files/"+data+".pdf","mywindow")){
							location.href=document_root+"modules/payments/fRegisterPayment/1";
							//Delete the pay-off document from the server
							$.ajax({
								url:document_root+"rpc/unlinkFile.rpc",
								data: "file_name=system_temp_files/"+data+".pdf"
							});
						}
					}
					hide_ajax_loader();
				}
				);


			})

		});
	}else{
		if(!$('#selCountry').val()){
			$("#messageCountry").show();
		}
		else{
			if(!$('#selManager').val()){
				$("#messageManager").show();
			}
			else{
				$("#messageDates").show();
			}
		}
		
	}
}

//checkAll
//select/deselect all payments
function checkAll() {
	$(".paymentsChk", currentTable.fnGetNodes()).each(function(){
		if($('#selAll').is(':checked')){
			$(this).attr('checked',true);
		}else{
			$(this).attr('checked',false);
		}
	});
}
