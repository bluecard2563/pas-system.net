// JavaScript Document
var currentTable;
$(document).ready(function () {

    /*VALIDATION SECTION*/
    $("#frmChooseInvoice").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selectedInvoices:{
                required: true
            }
        },
        messages: {
            selectedInvoices:{
                required: "Debe seleccionar al menos una factura a pagar"
            }
        },
        submitHandler: function (form) {
            var sData = $("input[id^='chkInvoice']", currentTable.fnGetNodes()).serialize();
            $('#selectedBoxes').val(sData);
            form.submit();
        }
    });
    /*END VALIDATION SECTION*/


    if ($('#selCountry option').size() <= 2 && $('#selCountry option').size() > 1) {
        loadManagers($('#selCountry').val());
    } else {
        if ($('#selCity option').size() <= 2) {
            loadResponsables($('#selCity').val(), $('#hdnSerialMbc').val());
        }
        $('#selCity').bind("change", function () {
            loadResponsables(this.value, $('#hdnSerialMbc').val())
        });
    }
    $('#selCountry').bind("change", function () {
        loadManagers(this.value);
    });
    
    if ($('#selectedDealer').val()){
        actionButtons();
    }
    
    /*END JQUERY EVENTS*/
});

function loadManagers(countryID) {
    $('#managerContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentManager.rpc", {
        serial_cou: countryID
    }, function () {
        if ($('#selManager option').size() <= 2 && $('#selCountry option').size() > 1) {
            loadCities(countryID, $('#selManager').val());
        }
        $('#selManager').bind("change", function () {
            loadCities(countryID, this.value);
        });
    });
}

function loadCities(countryID, managerID) {
    $('#cityContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentCities.rpc", {
        serial_cou: countryID,
        serial_mbc: managerID
    }, function () {
        if ($('#selCity option').size() <= 2 && $('#selCountry option').size() > 1) {
            loadResponsables($('#selCity').val(), managerID);
        }
        $('#selCity').bind("change", function () {
            loadResponsables(this.value, managerID)
        });
    });
}

function loadResponsables(cityID, managerID) {
    $('#responsibleContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentResponsables.rpc", {
        serial_cit: cityID,
        serial_mbc: managerID
    }, function () {
        if ($('#selResponsible option').size() <= 2 && $('#selResponsible option').size() > 1) {
            loadDealers(cityID, $('#selResponsible').val(), managerID);
        }
        $('#selResponsible').bind("change", function () {
            loadDealers(cityID, this.value, managerID);
            $('#selBranch').html('<option value="">- Seleccione -</option>');
        });
    });
}

function loadDealers(serial_cit, userID, managerID) {
    $('#dealerContainer').load(document_root + "rpc/payments/creditCardPayments/loadPaymentDealers.rpc", {
        serial_cit: serial_cit,
        serial_usr: userID,
        serial_mbc: managerID
    }, function () {
        if ($('#selDealer option').size() <= 2 && $('#selDealer option').size() > 1) {
            loadBranches($('#selDealer').val(), userID);
        }
        $('#selDealer').bind("change", function () {
            loadBranches(this.value, userID);
        });
    });
}

function loadBranches(dealerID, userID) {
    $("#branchContainer").load(document_root + "rpc/payments/creditCardPayments/loadPaymentDealers.rpc", {
        serial_dea: dealerID,
        serial_usr: userID
    }, function () {
        if ($('#selBranch option').size() <= 2 && $('#selBranch option').size() > 1) {
            loadInvoices($('#selBranch').val());
        }
        $('#selBranch').bind("change", function () {
            loadInvoices(this.value);
        });
    });
}

function loadInvoices(branchID) {
    $('#invoiceContainer').load(document_root + "rpc/payments/creditCardPayments/loadInvoicesByBranch.rpc", {
        serial_dea: branchID
    }, function () {
        actionButtons();
    });
}

/*invoiceChecked
 * CHECKS IF THERE IS AN INVOICE
 * THAT HAD BEEN PARTIALLY PAYED
 * TO CONTROL THAT HAS TO BE PAYED INDIVUDUALY
 * AND CONTROL THAT AT LEAT ONE INVOICE HAS BEEN SELECTED
 */
function invoiceChecked(check) {
    var somechecked = 0;
    var payed = 0;
    var checked = 0;
    if ($(check).is(':checked')) {//IF THE CLICKED CHECKBOX IS SELECTED
        if ($(check).attr('payment') != '') {//IF THE SELECTED INVOICE HAS THE PAYMENT ATTRIBUTE
            $('#payedSelected').val('1');//TO CONTROL THAT A PARTIALLY PAYED INVOICE HAS BEEN SELECTED
        }
        $(".invoiceChk", currentTable.fnGetNodes()).each(function () {//FOREACH INVOICE
            if ($(this).attr('id') != $(check).attr('id')) {//EXCLUDE THE CURRENT SELECTED INVOICE
                if ($(this).is(':checked')) {//IF THE INVOICE IS SELECTED
                    somechecked = 1;//CONTROL THAT THERE IS A SELECTED INVOICE NOT THE CURRENT SELECTED
                    if ($(this).attr('payment') != '') {
                        payed = 1;//TO CONTROL THAT THERE IS A SELECTED INVOICE AND IS A PARTIALLY PAYED INVOICE
                    }
                    if (payed == 1) {
                        $('#payedSelected').val('1');//TO CONTROL THAT A PARTIALLY PAYED INVOICE HAS BEEN SELECTED
                    } else {
                        $('#payedSelected').val('0');
                    }
                }
            }
        });
        //IF THERE IS A SELECTED INVOICE AND IS A PAYED INVOICE OR IF THERE IS A SELECTED INVOICE AND THE CURRENT SELECTED INVOICE IS A PARTIALLY PAYED INVOICE
        if ((somechecked == 1 && $('#payedSelected').val() == '1') || (somechecked == 1 && $(check).attr('payment') != '')) {
            $(check).attr('checked', false);
            alert('Las facturas que tienen abonos realizados deben pagarse individualmente.');
        }
    }
    $(".invoiceChk").each(function () {
        if ($(this).is(':checked')) {
            checked = 1;
        }
    });
    if (checked == 1) {
        $('#selectedInvoices').val('1');
    } else {
        $('#selectedInvoices').val('');
    }
}

/*actionButtons
 * Button controls in data table
 */
function actionButtons(){
    if ($("#invoicesTable").length > 0) {
            /*Style dataTable*/
            currentTable = $('#invoicesTable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "oLanguage": {
                    "oPaginate": {
                        "sFirst": "Primera",
                        "sLast": "&Uacute;ltima",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                    "sZeroRecords": "No se encontraron resultados",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                    "sSearch": "Filtro:",
                    "sProcessing": "Filtrando.."
                },
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": document_root + "img/dataTables/copy_cvs_xls_pdf.swf",
                    "aButtons": ["pdf"]
                }
            });

        
            $('#selAll').bind("click", function () {
                if ($('#selAll').attr("checked")) {
                    //check if there is at least one paid invoice selected
                    checkedPaid = 0;
                    $(".invoiceChk", currentTable.fnGetNodes()).each(function () {
                        if ($(this).is(':checked') && $(this).attr('payment') != '') {
                            checkedPaid = 1;
                        }
                    });
                    if (checkedPaid == 1) {
                        alert('Las facturas que tienen abonos realizados deben pagarse individualmente.');
                        $('#selAll').attr('checked', false);
                    } else {
                        $("input[id^='chkInvoice'][payment='']", currentTable.fnGetNodes()).attr("checked", true);
                    }
                } else {
                    $("input[id^='chkInvoice']", currentTable.fnGetNodes()).attr("checked", false);
                }
            });

            $("input[id^='chkInvoice']", currentTable.fnGetNodes()).bind("click", function () {
                if (!$(this).attr("checked")) {
                    $('#selAll').attr("checked", false);
                }
            });
            $(".invoiceChk", currentTable.fnGetNodes()).each(function () {
                $(this).bind('click', function () {
                    invoiceChecked($(this));
                });
            });
        }

        $('#pay_cd').bind('click', function () {
            $('#frmChooseInvoice').attr('action', document_root + 'modules/payments/creditCardPayments/fNewPayment');
        });

        $('#pay_pp').bind('click', function () {
            $('#frmChooseInvoice').attr('action', document_root + 'modules/payphone/fNewPayPhonePayment');
        });
}