// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
	$("#frmGlobalPayment").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                txtCardNum: {
                    required: true,
                    creditcard: true
                },
                txtSecCode: {
                    required: true,
                    digits: true,
                    maxlength: 4
                },
                txtNameCard: {
                    required: true,
                    textOnly: true
                },
                selMonth: {
                    required: true
                },
                selYear: {
                    required: true
                },
                selCountry: {
                    required: true
                },
                selCityCustomer: {
                    required: true
                },
                txtState: {
                    required: true,
                    textOnly: true
                },
                txtZip: {
                    required: true,
                    alphaNumeric: true
                },
                txtPhone: {
                    required: true,
                    digits: true
                },
                txtMail: {
                    required: true,
                    email: true
                },
                txtAddress: {
                    required: true,
                    alphaNumeric: true
                },
				txtTotalAmount: {
					required: true,
					number: true
				},
				txtObservations: {
					required: true,
					textOnly: true
				}
            },
            messages: {
                txtCardNum: {
                    creditcard: 'Ingrese un n&uacute;mero de tarjeta v&aacute;lido.'
                },
                txtSecCode: {
                    digits: 'El campo "C&oacute;digo de seguridad" admite solo n&uacute;meros.',
                    maxlength: 'El campo "C&oacute;digo de seguridad" admite hasta 4 n&uacute;meros.'
                },
                txtNameCard: {
                    textOnly: 'El campo "Nombre en la tarjeta" admite solo caracteres.'
                },
                txtState: {
                    textOnly: 'El campo "Estado/Provincia" admite solo caracteres.'
                },
                txtZip: {
                    alphaNumeric: 'El campo "C&oacute;digo Postal/Zip" admite solo n&uacute;meros y letras.'
                },
                txtPhone: {
                    digits: 'El campo "Tel&eacute;fono" admite solo n&uacute;meros.'
                },
                txtMail: {
                    email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com.'
                },
                txtAddress: {
                    alphaNumeric: 'El campo "Direcci&oacute;n" admite solo n&uacute;meros y letras.'
                },
				txtTotalAmount: {
					number: 'El campo "Total a Pagar" admite s&oacute;lo n&uacute;meros.'
				},
				txtObservations: {
					textOnly: 'El campo "Concepto" admite s&oacute;lo texto.'
				}
            }
    });
    /*END VALIDATION SECTION*/

   
    $('#selCountry').bind("change", function(){
        $('#cityContainer').load(document_root+"rpc/loadCities.rpc", {serial_cou: this.value,all:true, serial_sel: "Customer"});
    });
});