// JavaScript Document
/*
File:fNewPayment.js
Author: Edwin Salvador
Creation Date:12/03/2010
Modified By:
Last Modified:
*/
$(document).ready(function(){
    //VALIDATION
    $("#frmPayment").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
                paymentType_0: {
                        required: true
                },
                paymentDesc_0: {
                        required: function(element) {
                            return $(element).css('display') != 'none';
                        }
                },
                paymentCard_0: {
                        required: function(element) {
                            return $(element).css('display') != 'none';
                        }
                },
                paymentExcess_0: {
                        required: function(element) {
                            return $(element).css('display') != 'none';
                        }
                },
                paymentBank_0: {
                        required: function(element) {
                            return $(element).css('display') != 'none';
                        }
                },
                paymentOther_0: {
                        required: function(element) {
                            if($(element).val() == 'Especifique'){
                                $(element).val('');
                            }
                            return $(element).css('display') != 'none';
                        }
                },
                paymentDocument_0: {
                        required: true,
                        digits: true
                },
                paymentVal_0: {
                        required: true,
                        number: true
                }
        },
         messages:{
                paymentDocument_0: {
                        digits: 'El campo "# Documento" admite s&oacute;lo n&uacute;meros.'
                },
                paymentVal_0: {
                        number: 'El campo "Valor" admite s&oacute;lo n&uacute;meros.'
                }
        },
        submitHandler: function(form){
			$('#btnPay').attr('disabled','true');
			$('#btnPay').val('Espere por favor...');
			//set the status_pay
            if($('#count_invoices').val() == '1'){
                if(parseFloat($('#total_to_pay').val()) < parseFloat($('#curPayment').html())){
                    if(confirm("El pago excede el total a pagar.\n Se registrar\u00E1 como pago en exceso. Desea continuar?")){
                        if(confirm("Errores de pagos en exceso no est\u00E1n sujetos a cambios. Est\u00E1 seguro que desea continuar?")){
                            $('#status_pay').val('EXCESS');
                            form.submit();
                        }
                    }
					
					$('#btnPay').removeAttr('disabled');
                    $('#btnPay').val('Pagar');
                }else {
                    if(parseFloat($('#total_to_pay').val()) > parseFloat($('#curPayment').html())){
                        if(confirm("El pago no cubre el total a pagar.\n Se registrar\u00E1 como un abono. Desea continuar?")){
                            $('#status_pay').val('PARTIAL');
                            form.submit();
                        }else{
							$('#btnPay').removeAttr('disabled');
							$('#btnPay').val('Pagar');
						}
                    }else{
                        if(parseFloat($('#total_to_pay').val()) == parseFloat($('#curPayment').html())){
							$('#status_pay').val('PAID');
							form.submit();
                        }
                    }
                }
            }else{
                if(parseFloat($('#total_to_pay').val()) > parseFloat($('#curPayment').html())){
                    alert("El pago debe cubrir el total a pagar.\n No se pueden realizar abonos para m\u00E1s de una factura al mismo tiempo.");
                    $('#status_pay').val('');
					$('#btnPay').removeAttr('disabled');
					$('#btnPay').val('Pagar');
                }else{
                    if(parseFloat($('#total_to_pay').val()) < parseFloat($('#curPayment').html())){
						if(confirm("El pago excede el total a pagar.\n Se registrar\u00E1 como pago en exceso. Desea continuar?")){
							if(confirm("Errores de pagos en exceso no est\u00E1n sujetos a cambios. Est\u00E1 seguro que desea continuar?")){
								$('#status_pay').val('EXCESS');
								form.submit();
							}
						}

						$('#btnPay').removeAttr('disabled');
						$('#btnPay').val('Pagar');
                    }else{
                        if(parseFloat($('#total_to_pay').val()) == parseFloat($('#curPayment').html())){
							$('#status_pay').val('PAID');
							form.submit();
                        }
                    }
                }
            }
        }
    });
    //END VALIDATION
	//events
    $('#btnAddForm').bind('click', function(){
        if($("#frmPayment").valid()){
           addPaymentForm();           
        }else{
            alert('Debe llenar todos los campos obligatorios correctamente antes de a\u00f1adir otra forma de pago.')
        }
        
    });
    
    $('#btnDeleteForm').bind('click', function(){
        deletePaymentForm();
    });
    
    $('#paymentVal_0').bind('keyup change', function(){
        sumPaymentForms();
    });

    $('#paymentType_0').bind('change', function(){
        changeDescriptionField($(this).val(),0);

        if($(this).val()=='CASH' || $(this).val()=='EXCESS' || $(this).val()=='RETENTION' || $(this).val()=='COMMISSION'){
            $('#paymentDocument_0').rules('remove');
            $('#paymentDocument_0').hide();
            $('#paymentDocument_0').val('');
            $('#paymentVal_0').attr("readonly", false);
            if($(this).val()=='CASH'){
                $('#paymentDesc_0').val('Efectivo');
                //$('#paymentDesc_0').attr("readonly", true);
                $('#paymentDocument_0').show();
            }else{
				if($(this).val()=='RETENTION' || $(this).val()=='COMMISSION'){
					$('#paymentDesc_0').val('');
					$('#paymentDesc_0').attr("readonly", false);
					$('#paymentDocument_0').show();
				}else{
					$('#paymentDesc_0').val('');
					$('#paymentDesc_0').attr("readonly", false);
				}
            }
        }else{			
            $('#paymentDocument_0').rules("add", {
                required: true,
                digits: true,
                messages:{
                    digits: 'El campo "# Documento" admite s&oacute;lo n&uacute;meros.'
                }
            });

			$('#paymentDocument_0').show();

			if($(this).val()=='CREDIT_NOTE'){
                $('#paymentVal_0').attr("readonly", true);
                $('#paymentDocument_0').attr("readonly", true);
                $('#paymentDocument_0').rules('remove');
            }else{
                $('#paymentVal_0').attr("readonly", false);
                $('#paymentDocument_0').attr("readonly", false);
            }
        }
    });

    $('#paymentOther_0').bind('click', function(){
        if($(this).val() == 'Especifique'){
            $(this).val('');
        }
    });
	//end events

});
//add a new payment form
function addPaymentForm(){
    var formDiv = $('#paymentsContainer #form_0').html();
    var count = parseInt($('#count').val())+1;
    var newForm = '<div id="form_'+count+'" class="span-24 last line">'+formDiv+'</div>';

    

    $('#paymentsContainer').append(newForm);
    //TYPE FIELD
  //  alert($('#paymentsContainer #form_'+count+' > .label > [id=paymentType_0]').attr('id'));alert(count);
    //		$('#selType0', $('#typeContainer'+count)).attr('name','selType'+count); *****MUESTRA****
    //$('#paymentsContainer #form_'+count+' [id=paymentType_0]').attr({name: 'payments['+count+'][paymentType]', id: 'paymentType_'+count}).bind('change', function(){

	   $('#paymentType_0',$('#paymentsContainer #form_'+count)).attr('name','payments['+count+'][paymentType]');
	   $('#paymentType_0',$('#paymentsContainer #form_'+count)).attr('id','paymentType_'+count);
	   $("#paymentType_"+count+" option[value='']").attr("selected",true);
	   $('#paymentType_'+count).bind('change', function(){
			changeDescriptionField($(this).val(),count);
	   
			if($(this).val()=='CASH'){
				$('#paymentDesc_'+count).val('Efectivo');
				$('#paymentDesc_'+count).attr("readonly", true);
			}
			if($(this).val()=='EXCESS' || $(this).val()=='RETENTION' || $(this).val()=='COMMISSION'){
				$('#paymentDocument_'+count).rules('remove');
				$('#paymentDocument_'+count).hide();
				$('#paymentDocument_'+count).val('');
				$('#paymentVal_'+count).attr("readonly", false);
				if($(this).val()=='RETENTION' || $(this).val()=='COMMISSION'){
					$('#paymentDesc_'+count).val('');
					$('#paymentDesc_'+count).attr("readonly", false);
					$('#paymentDocument_'+count).show();
				}else{
					$('#paymentDesc_'+count).val('');
					$('#paymentDesc_'+count).attr("readonly", false);
				}
			}else{
				$('#paymentDocument_'+count).rules("add", {
					required: true,
					digits: true,
					messages:{
						digits: 'El campo "# Documento" admite s&oacute;lo n&uacute;meros.'
					}
				});
				$('#paymentDocument_'+count).show();
				if($(this).val()=='CREDIT_NOTE'){
					$('#paymentVal_'+count).attr("readonly", true);
					$('#paymentDocument_'+count).attr("readonly", true);
				}else{
					$('#paymentVal_'+count).attr("readonly", false);
					$('#paymentDocument_'+count).attr("readonly", false);
				}

			}
    }).rules("add", {
        required: true
    });
    //DESCRIPTION FIELD
    $('#paymentsContainer #form_'+count+' [id=paymentDesc_0]').attr({name: 'payments['+count+'][paymentDesc]', id: 'paymentDesc_'+count}).css('display','inline').rules("add", {
        required: function(element) {
            return $(element).css('display') != 'none';
        }
    });
    $('#paymentsContainer #form_'+count+' [id=paymentCard_0]').attr({name: 'payments['+count+'][paymentCard]', id: 'paymentCard_'+count}).css('display','none').rules("add", {
        required: function(element) {
            return $(element).css('display') != 'none';
        }
    });
    $('#paymentsContainer #form_'+count+' [id=paymentExcess_0]').attr({name: 'payments['+count+'][paymentExcess]', id: 'paymentExcess_'+count}).css('display','none').rules("add", {
        required: function(element) {
            return $(element).css('display') != 'none';
        }
    });
    $('#paymentsContainer #form_'+count+' [id=creditNotes_0]').attr({name: 'payments['+count+'][creditNotes]', id: 'creditNotes_'+count}).css('display','none').rules("add", {
        required: function(element) {
            return $(element).css('display') != 'none';
        }
    });
    $('#paymentsContainer #form_'+count+' [id=paymentBank_0]').attr({name: 'payments['+count+'][paymentBank]', id: 'paymentBank_'+count}).css('display','none').rules("add", {
        required: function(element) {
            return $(element).css('display') != 'none';
        }
    });
    $('#paymentsContainer #form_'+count+' [id=paymentOther_0]').attr({name: 'payments['+count+'][paymentOther]', id: 'paymentOther_'+count, value: 'Especifique'}).css('display','none').bind('click', function(){
        if($(this).val() == 'Especifique'){
            $(this).val('');
        }
    }).rules("add", {
        required: function(element) {
            if($(element).val() == 'Especifique'){
                $(element).val('');
            }
            return $(element).css('display') != 'none';
        }
    });
	//VALUES

	$('#paymentDesc_'+count).val('');
	$('#paymentDocument_'+count).val('');
	$('#paymentVal_'+count).val('');
    
	//#DOCUMENT FIELD
    $('#paymentsContainer #form_'+count+' [id=paymentDocument_0]').attr({name: 'payments['+count+'][paymentDocument]', id: 'paymentDocument_'+count}).rules("add", {
        required: true,
        digits: true,
        messages:{
            digits: 'El campo "# Documento" admite s&oacute;lo n&uacute;meros.'
        }
    });

	//DOCUMENT FIELD ->VALUE
	$('#paymentDocument_'+count).val('');

    //VAL FIELD
    $('#paymentsContainer #form_'+count+' [id=paymentVal_0]').attr({name: 'payments['+count+'][paymentVal]', id: 'paymentVal_'+count}).bind('keyup change', function(){
        sumPaymentForms();
    }).rules("add", {
        required: true,
        number: true,
        messages:{
            number: 'El campo "Valor" admite s&oacute;lo n&uacute;meros.'
        }
    });
	//VAL FIELD ->VALUE
	$('#paymentVal_'+count).val('');

    $('#count').val(count);
    if(count == 1){
        $('#btnDeleteForm').css('display', 'block');
    }
    $('[id^=paymentExcess_]').each(function (i) {
        if($(this).val()){
          $('#paymentExcess_'+count).find('[value="'+$(this).val()+'"]').remove();
        }
          
    });
    $('[id^=creditNotes_]').each(function (i) {
        if($(this).val()){
          $('#creditNotes_'+count).find('[value="'+$(this).val()+'"]').remove();
        }

    });
}
//delete a payment form
function deletePaymentForm(){
    var count = parseInt($('#count').val());

    $('#form_'+count).remove();
    $('#count').val(count-1);

    sumPaymentForms();

    if(count == 1){
        $('#btnDeleteForm').css('display', 'none');
    }
}
//get the total of the payment forms
function sumPaymentForms(){
    var sum = 0.00.toFixed(2);
    var val = 0;
    var pend = $('#total_to_pay').val();
    $.each($('.formVal'), function(){
        val = (Math.round(parseFloat($(this).val())*100)/100);
        if(!isNaN(val)){
            sum = (Math.round((parseFloat(sum) + val)*100)/100).toFixed(2);
            pend = (Math.round((parseFloat(pend) - val)*100)/100).toFixed(2);
        }
    });
    if(pend<=0){
        $('#btnAddForm').css('display', 'none');
        if(pend<0){
            $('#pending').attr('class','span-2 last warning');
            $('#textPending').attr('class','span-3 warning');
        }else{
            $('#pending').attr('class','span-2 last');
            $('#textPending').attr('class','span-3');
        }
    }else{
        $('#btnAddForm').css('display', 'inline');
        $('#pending').attr('class','span-2 last');
        $('#textPending').attr('class','span-3');
    }
    $('#curPayment').html(sum);
    $('#pending').html(pend);
}
//chege the description field based on the payment form
function changeDescriptionField(option,count){
    $('#paymentOther_'+count).css('display','none');
    $('#paymentOther_'+count).val('Especifique');
    $('#paymentVal_'+count).val('');
    $('#paymentDocument_'+count).val('');

    if(option == 'CREDIT_CARD' || option == 'CHECK' || option == 'TRANSFER' || option == 'EXCESS' || option == 'CREDIT_NOTE'){
        $('#paymentDesc_'+count).css('display','none');
        $('#paymentDesc_'+count).val('');
        if(option == 'CREDIT_CARD'){
            $('#paymentCard_'+count).css('display','inline');
            $('#paymentCard_'+count).bind('change',function(){
                showOther(this.value,count);
            });
        }else{
            $('#paymentCard_'+count).css('display','none');
            $('#paymentCard_'+count).val('');
            $('#paymentCard_'+count).unbind('change');
        }
        if(option == 'CHECK' || option == 'TRANSFER'){
            $('#paymentBank_'+count).css('display','inline');
            $('#paymentBank_'+count).bind('change',function(){
                showOther(this.value,count);
            });
        }else{
            $('#paymentBank_'+count).css('display','none');
            $('#paymentBank_'+count).val('');
            $('#paymentBank_'+count).unbind('change');
        }
        if(option == 'EXCESS'){             
            $('#paymentExcess_'+count).css('display','inline');
            $('#paymentVal_'+count).blur(function(){
                checkExcessValue($('#paymentExcess_'+count+' option:selected'),$(this),$('#paymentType_'+count).val());
            });

        }else{
            $('#paymentExcess_'+count).css('display','none');
            $('#paymentExcess_'+count).val('');
            $('#paymentExcess_'+count).unbind('change');
        }
        if(option == 'CREDIT_NOTE'){
            $('#creditNotes_'+count).css('display','inline');
            $('#creditNotes_'+count).change(function(){
                if($(this).val()!=''){
                    $('#paymentVal_'+count).val($("#creditNotes_"+count+" option:selected").text().substr(4));
                    $('#paymentDocument_'+count).val($("#creditNotes_"+count+" option:selected").attr('number'));
                }else{
                    $('#paymentVal_'+count).val('');
                    $('#paymentDocument_'+count).val('');
                }
                sumPaymentForms();
            });
        }else{
            $('#creditNotes_'+count).css('display','none');
            $('#creditNotes_'+count).val('');
            $('#creditNotes_'+count).unbind('change');
        }
    }else{
        $('#paymentDesc_'+count).css('display','inline');
        $('#paymentCard_'+count).css('display','none');
        $('#paymentCard_'+count).val('');
        $('#paymentCard_'+count).unbind('change');
        $('#paymentBank_'+count).css('display','none');
        $('#paymentBank_'+count).val('');
        $('#paymentBank_'+count).unbind('change');
        $('#paymentExcess_'+count).css('display','none');
        $('#paymentExcess_'+count).val('');
        $('#paymentExcess_'+count).unbind('change');
        $('#creditNotes_'+count).css('display','none');
        $('#creditNotes_'+count).val('');
        $('#creditNotes_'+count).unbind('change');
    }
}
//if the "Other" option has been selected the show the field to write the name of the bank or credit card
function showOther(option,count){
    if(option == 'OTHER'){
        $('#paymentOther_'+count).css('display','inline');
    }else{
        $('#paymentOther_'+count).css('display','none');
        $('#paymentOther_'+count).val('Especifique');
    }

}

function checkExcessValue(available, valueObject,selectedType){
    if(selectedType=='EXCESS'){
        if(available.val()){
            if(parseFloat(available.text().substring(4))<valueObject.val()){
                alert('El valor ingresado no puede ser mayor al valor disponible seleccionado.');
                valueObject.val('');
                sumPaymentForms();
            }
        }else{
             alert('Seleccione primero un pago en exceso.');
             valueObject.val('');
             sumPaymentForms();
        }
    } 
}