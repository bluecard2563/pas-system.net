// JavaScript Document
var currentTable;
var current_id;
$(document).ready(function(){
	if($('#pos_table').length > 0 ){
		/*Style dataTable*/
		currentTable = $('#pos_table').dataTable( {
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"oLanguage": {
				"oPaginate": {
					"sFirst": "Primera",
					"sLast": "&Uacute;ltima",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				},
				"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
				"sZeroRecords": "No se encontraron resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
				"sSearch": "Filtro:",
				"sProcessing": "Filtrando.."
			},
			"sDom": 'T<"clear">lfrtip',
			"oTableTools": {
				"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
				"aButtons": []
			}

		} );
		
		
		$('[id^="chkPOS_"]').each(function(){	
			current_id = $(this).val();
			$(this).bind('click', function(){
				show_ajax_loader();
				$('#productContainer').load(document_root + 'rpc/webPOS/loadProductsByPOS.rpc', 
					{
						serial_dea: current_id
					}, function(){
						$("#frmAsignProductPOS").validate({
							errorLabelContainer: "#alerts",
							wrapper: "li",
							onfocusout: false,
							onkeyup: false,
							rules: {
								selProduct: {
									required: true
								}
							}
						});
		
						hide_ajax_loader();
					});
			});
		});	
	}
});


