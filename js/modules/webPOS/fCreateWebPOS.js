// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/	
	$("#frmCreateWebPOS").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtIP: {
				required: true,
				ipValidator: true,
				remote: {
					url: document_root+"rpc/remoteValidation/webPOS/checkIPExists.rpc",
					type: "post",
					data: {
					  serial_dea: function() {
						return $("#hdnSerial_dea").val();
					  }
					}
				}
			},
			selStatus: {
				required: true
			},
			txtUsername: {
				required: true,
				alphaNumeric: true,
				remote: {
					url: document_root+"rpc/remoteValidation/webPOS/checkUsernameExists.rpc",
					type: "post",
					data: {
					  serial_dea: function() {
						return $("#hdnSerial_dea").val();
					  }
					}
				}
			},
			txtPassword: {
				required: function(){
					if($('#password_container').css('display') == 'block'){
						return true;
					}else{
						return false;
					}
				}
			},
			txtConfirmPassword: {
				equalTo: '#txtPassword'
			}
		},
		messages: {
			txtIP: {
				ipValidator: "Ingrese una direcci&oacute;n IP v&aacute;lida.",
				remote: "La direcci&oacute;n IP ingresada ya existe en el sistema."
			},
			txtUsername: {
				alphaNumeric: "El campo 'Nombre de usuario' admite s&oacute;lo caracteres alfa - num&eacute;ricos.'",
				remote: "El 'Nombre de usuario' ya consta en el sistema. Por favor ingrese uno diferente."
			},
			txtConfirmPassword: {
				equalTo: "La confirmaci&oacute;n de la contrase&ntilde;a no concuerda."
			}
		}
	});
	
	
	$('#changePAssword').bind('click', function(){
		$('#password_container').toggle('slow');
	});
});


