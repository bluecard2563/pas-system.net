// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountryUser: {
				required: true
			},
			selCityUser: {
				required: true
			},
			hdnSerial_usu:{
				required: true
			},
			txtPercentage:{
				required: true,
				percentage: true,
				max_value: 100
			},
			selZoneAssign: {
				required: true
			},
			selCountryAssign: {
				required: true
			},
			selCityAssign: {
				required: true
			},
			selComissionistAssign: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	/*DYNAMIC AUTOCOMPLETE*/
	var ruta=document_root+"rpc/autocompleters/loadComissionists.rpc";
	$("#txtUser").autocomplete(ruta,{
		extraParams: {
			serial_cit: function() {
				return $("#selCityUser").val();
			}
		},
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 3,
		formatItem: function(row) {
			if(row[0]!='none'){
				return row[0];
			}else{
				return 'No existen responsables registrados.';
			}			
		}
	}).result(function(event, row) {
		if(row[0]!='none'){
			$("#hdnSerial_usu").val(row[1]);
			if(row[3]=="true"){
				alert("El usuario seleccionado es responsable de uno o m\u00e1s pa\u00edses.");
			}
		}else{
			$("#hdnSerial_usu").val('');
			$("#txtUser").val('');
		}
	});
	/*END AUTOCOMPLETE*/
	
	/*CHANGE OPTIONS*/
	$("#selZone").bind("change", function(e){
		clearFields('zone', "User");
		loadCountry(this.value, "User");		
	});
	
	$("#selZoneAssign").bind("change", function(e){
		clearFields('zone', "Assign");
		loadCountry(this.value, "Assign");
	});
	/*END CHANGE*/
});

function loadCountry(zoneID, selectID)
{
	if(selectID == "User") {
		var container = $('#countryContainer');
	}else{
		var container = $('#countryAssignContainer');
	}
	
	container.load(document_root+"rpc/loadCountries.rpc",
	{
		serial_zon: zoneID, 
		serial_sel: selectID
	},
	function(){
		$('#selCountry'+selectID).change(function(){
			clearFields('country', selectID);
			loadCity(this.value,selectID);
		});
		
		if($('#selCountry'+selectID+' option').size() <=2) {
			loadCity($('#selCountry'+selectID).val(),selectID);
		}
	});
}

function loadCity(countryID, selectID)
{
	if(selectID == "User") {
		var container = $('#cityContainer');
		$("#txtUser").val("");
	} else {
		var container = $('#cityAssignContainer');
	}
	
	container.load(document_root+"rpc/loadCities.rpc",
	{
		serial_cou: countryID, 
		serial_sel: selectID
	},function(){
		if(selectID == "User") {
			$("#selCityUser").bind("change", function(e){
				clearFields('city', selectID);
			});
		}else{
			$("#selCityAssign").bind("change", function(e){
				clearFields('city', selectID);
				loadComissionist(this.value,selectID);
			});
		}		
	});
}

function loadComissionist(cityID, selectID)
{	
	$('#comissionistAssignContainer').load(document_root+"rpc/loadComissionist.rpc",
	{
		serial_cit: cityID, 
		serial_sel: selectID
	},
	function(){
		$("#selComissionistAssign").bind("change", function(e){
			clearFields('responsible', selectID);
			loadDealers($(this).val());
		});
	});
}

function loadDealers(serial_usr)
{
	show_ajax_loader();
	$('#dealersContainer').load(document_root+"rpc/loadComissionistByDealer/loadDealersByComissionist.rpc",{
		serial_usr: serial_usr
	}, function(){
		if($('#dealer_table').length>0) {
			currentTable = $('#dealer_table').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"oLanguage": {
					"oPaginate": {
						"sFirst": "Primera",
						"sLast": "&Uacute;ltima",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
					"sZeroRecords": "No se encontraron resultados",
					"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
					"sSearch": "Filtro:",
					"sProcessing": "Filtrando.."
				},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
					"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
					"aButtons": []
				}

			} );
			
			if($("#selAll")){
				$("#selAll").bind("click", function(){
					var checked = $(this).is(':checked');
					var registers = currentTable.fnGetDisplayNodes();
					var last_item;
					
					//************ CHECK/UNCHECK THE NODE
					$.each(registers, function(index, item){
						//***** GET A SPECIFIC NODE
						var node_data = String(currentTable.fnGetData( item ));
						last_item = item;
						
						//****** RETRIEVE ITS INFORMATION AS A STRING AND MANIPULATE IT TO GET THE ID
						var id_section = node_data.substring(node_data.indexOf('id="'), node_data.indexOf('" value="'));
						id_section = str_replace('id="', '', id_section);
						
						//****** AFECT THE NODE WITH THE CHECKED STATE
						$("#"+id_section).attr("checked", checked);
					});
					
					currentTable.fnDisplayRow( last_item );
				});
			}
			
			$("#hdnDealersTo").rules("add", {
				required: true
			});
			
			$('#btnInsert').bind('click', function(){
				var selected_ids = new Array();

				$("input:checked[id^='chkClaims']", currentTable.fnGetNodes()).each(function(){
					array_push(selected_ids, $(this).val());
				});
				
				if(selected_ids.length > 0){
					$('#hdnDealersTo').val(implode(',', selected_ids));
				}else{
					$('#hdnDealersTo').val('');
				}
				
				if($("#frmNewCounter").valid()){
					$("#frmNewCounter").submit();
				}				
			});
		}
		
		hide_ajax_loader();
	});
}

function clearFields(origin, section)
{
	switch(origin){
		case 'zone':
			if(section == 'User'){
				$('#selCountryUser').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selCityUser').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#txtUser').val("");
				$('#txtPercentage').val("");
				$('#hdnSerial_usu').val("");					
			}else{
				$('#selCountryAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selCityAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#dealersContainer').html('');
			}
			break;
		case 'country':
			if(section == 'User'){
				$('#selCityUser').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#txtUser').val("");
				$('#txtPercentage').val("");
				$('#hdnSerial_usu').val("");					
			}else{
				$('#selCityAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#dealersContainer').html('');
			}
			break;
		case 'city':
			if(section == 'User'){
				$('#txtUser').val("");
				$('#txtPercentage').val("");
				$('#hdnSerial_usu').val("");					
			}else{
				$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
				$('#dealersContainer').html('');
			}
			break;
		case 'responsible':
			$('#dealersContainer').html('');
			break;
	}
}