// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewCounter").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountryUser: {
				required: true
			},
			selCityUser: {
				required: true
			},
			txtUser:{
				required: true
			},
			dealersTo:{
				required: true
			},
			txtPercentage:{
				required: true,
				percentage: true,
				max_value: 100
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	/*DYNAMIC AUTOCOMPLETE*/
	var ruta=document_root+"rpc/autocompleters/loadComissionists.rpc";
	$("#txtUser").autocomplete(ruta,{
		extraParams: {
			serial_cit: function() { return $("#selCityUser").val(); }
	    },
		max: 10,
		scroll: false,
		matchContains:true, 
		minChars: 3,
		formatItem: function(row) {
			if(row[0]!='none'){
				return row[0];
			}else{
				return 'No existen responsables registrados.';
			}			
		}
	}).result(function(event, row) {
		if(row[0]!='none'){
			$("#hdnSerial_usu").val(row[1]);
			if(row[3]=="true"){
				alert("El usuario seleccionado es responsable de uno o m\u00e1s pa\u00edses.");
			}
		}else{
			$("#hdnSerial_usu").val('');
			$("#txtUser").val('');
		}
	});
	/*END AUTOCOMPLETE*/
	
	/*CHANGE OPTIONS*/
	$("#selZone").bind("change", function(e){
		loadCountry(this.value,"User");
    });
	$("#selZoneAssign").bind("change", function(e){
		$('#selComissionistAssign').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		loadCountry(this.value,"Assign");
	});
	/*END CHANGE*/
});

function loadCountry(zoneID,selectID){
	if(selectID == "User") {
		var container = $('#countryContainer');
		$('#cityContainer').load(document_root+"rpc/loadCities.rpc");
		$("#txtUser").val("");
	}
	else{
		var container = $('#countryAssignContainer');
		$('#cityAssignContainer').load(document_root+"rpc/loadCities.rpc");
		$('#dealerContainer').load(document_root+"rpc/loadDealersByCity.rpc");
	}
	container.load(
		document_root+"rpc/loadCountries.rpc",
		{serial_zon: zoneID, serial_sel: selectID},
		function(){
			$('#selCountry'+selectID).change(function(){loadCity(this.value,selectID);});
			if($('#selCountry'+selectID+' option').size() <=2) {
				loadCity($('#selCountry'+selectID).val(),selectID);
			}
		});
}

function loadCity(countryID,selectID){
	if(selectID == "User") {
		var container = $('#cityContainer');
		$("#txtUser").val("");
	}
	else{
		var container = $('#cityAssignContainer');
		$('#dealerContainer').load(document_root+"rpc/loadDealersByCity.rpc");
	}
	container.load(document_root+"rpc/loadCities.rpc",
							 {serial_cou: countryID, serial_sel: selectID},
							 function(){
								 $("#selCityUser").bind("change", function(e){
	      								$("#txtUser").val("");
    							});
								 $("#selCityAssign").bind("change", function(e){
	      								//$('#dealerContainer').load(document_root+"rpc/loadDealersByCity.rpc",{serial_cit: this.value});
										loadComissionist(this.value,selectID);
    							});
								if($('#selCityAssign option').size() <=2){
									//$('#dealerContainer').load(document_root+"rpc/loadDealersByCity.rpc",{serial_cit: $('#selCityAssign').val()});
								}
							});
}

function loadComissionist(cityID,selectID){

		var container = $('#comissionistAssignContainer');
		//$('#dealerContainer').load(document_root+"rpc/loadDealersByCity.rpc");
	
	container.load(document_root+"rpc/loadComissionist.rpc",
	 {serial_cit: cityID, serial_sel: selectID},
	 function(){
		$("#selComissionistAssign").bind("change", function(e){
			$('#dealerContainer').load(document_root+"rpc/loadDealersByComissionist.rpc",{serial_cit: cityID, serial_usr: this.value});
		});
		if($('#selCityAssign option').size() <=2) {
			$('#dealerContainer').load(document_root+"rpc/loadDealersByCity.rpc",{serial_cit: $('#selCityAssign').val()});
		}
	 });
}