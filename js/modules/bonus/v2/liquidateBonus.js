// JavaScript Document
var currentTable;
$(document).ready(function(){
    if($('#liquidationTable').length > 0){
        currentTable = $('#liquidationTable').dataTable( {
                        "bJQueryUI": true,
                        "sPaginationType": "full_numbers",
                        "oLanguage": {
                                "oPaginate": {
                                        "sFirst": "Primera",
                                        "sLast": "&Uacute;ltima",
                                        "sNext": "Siguiente",
                                        "sPrevious": "Anterior"
                                },
                                "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                                "sZeroRecords": "No se encontraron resultados",
                                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                                "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                                "sSearch": "Filtro:",
                                "sProcessing": "Filtrando.."
                        },
                   "sDom": 'T<"clear">lfrtip',
                   "oTableTools": {
                          "sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
                          "aButtons": []
                   }
        });
        
        $("[id^='voidRegistry_']").each(function(){
           $(this).bind('click', function(){
               if(confirm('Esta seguro que desea invalidar la liquidacion?')){
                   voidBonusLiquidation($(this).attr("registryID"));
               }
           }) 
        });
        
        $('#chkAll').bind('click', checkAll);
        
        enableSingleCheckValidation();
        
        $('#btnExportPurchaseList').bind('click', function(){
            exportToXLSList();
        });
        
        $('#btnLiquidate').bind('click', function(){
            liquidateBonus();
        });
    }
}); 

function liquidateBonus(){
    if(atLeastOneChecked()){
        $('#frmPayBonus').attr('action', document_root + "modules/bonus/v2/pLiquidateBonus");
        $('#frmPayBonus').removeAttr('target');
        
        $('#frmPayBonus').submit();
    }
}

function exportToXLSList(){
    if(atLeastOneChecked()){
        $('#frmPayBonus').attr('action', document_root + "modules/bonus/v2/printLiquidationPurchaseList");
        $('#frmPayBonus').attr('target', "_blank");
        
        $('#frmPayBonus').submit();
    }
}

function atLeastOneChecked(){
    var checkedCount = $("[id^='rdBonusLiquidation_']:checked").size();
    
    if(checkedCount > 0){
        return true;
    }else{
        alert('Seleccione al menos un registro para continuar.');
        return false;
    }
}

function enableSingleCheckValidation(){
    $("[id^='rdBonusLiquidation_']").each(function(){
        $(this).bind('click', function(){
            calculateTotal();
        });        
    });
}

function checkAll(){
    $("[id^='rdBonusLiquidation_']").each(function(){
        if($('#chkAll').is(":checked")){
            $(this).attr('checked', true); 
        }else{
            $(this).attr('checked', false); 
        }
    });
    
    calculateTotal();
}

function calculateTotal(){
    var liquidationID;
    var totalLiquidated = 0;
    
    $("[id^='rdBonusLiquidation_']:checked").each(function(){
        liquidationID = $(this).val();
        totalLiquidated += parseFloat($('#hdnTotal_'+liquidationID).val());
    });
    
    $('#lblTotal').html(totalLiquidated+" USD");
}

function voidBonusLiquidation(liquidationIDString){
    show_ajax_loader();
    
    $.ajax({
        url: document_root+"modules/bonus/v2/voidLiquidation",
        data: {
            liquidationID: liquidationIDString
        },
        method: "POST",
        success: function(data){
            hide_ajax_loader();
            if(data == "true"){
                alert('El registro fue invalidado exitosamente!');
                show_ajax_loader();
                location.reload();
            }else{
                alert('Hubo errores al invalidar el registro');
            }
        }
    });
}
