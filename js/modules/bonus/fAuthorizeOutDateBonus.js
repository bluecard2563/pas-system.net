// JavaScript Document
var pager;//for paginator
var currentTable;
$(document).ready(function(){
	$("#frmSelectBonus").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selBonusTo: {
				required: true
			}
		},
		submitHandler: function(form) {
			var sData = $(".bonusChk", currentTable.fnGetNodes()).serialize();
			$('#selectedBoxes').val(sData);
			form.submit();
		}
	});

	/*CHANGE OPTIONS*/
	$("#selBonusTo").bind("change", function(e){
		loadBonusForm(this.value);
	});
/*END CHANGE*/
});

function loadBonusForm(type){
	if(type!=""){
		$('#formContainer').load(document_root+'rpc/loadBonus/loadAuthorizeBonusForm.rpc',{
			bonus_to: type
		},function(){
			if($("#selCountry").length){
				$('#selCountry').bind('change',function(){
					loadCities(this.value,type);
				});
				if($("#selCountry option").size() ==2 ){
					loadCities($("#selCountry").val(),type);
				}
				$("#selCountry").rules("add", {
					required: true
				});
			}

			$('#btnFindBonus').bind('click',function(){
				$('#bonusContainer').html("");
				if($("#frmSelectBonus").valid()){
					loadBonus();
				}
			});
		});
	}else{
		$('#formContainer').html("");
	}
}

/*
 * @Name: loadCities
 * @Description: Loads the list of cities in a specific country.
 * @Params: countryID,type
 **/
function loadCities(countryID,type){
	$('#bonusContainer').html("");	
	$('#cityContainer').load(document_root+"rpc/loadBonus/loadCitiesForAuthorizeBonus.rpc",
	{
		serial_cou: countryID,
		bonus_to:type
	}, function(){
		$("#selCity").rules("add", {
			required: true
		});
	});
}

function loadBonus(){
	show_html_loader('#bonusContainer');
	
	type=$('#selBonusTo').val();
	serial=$('#selCity').val();
	$('#bonusContainer').load(document_root+"rpc/loadBonus/loadBonusToAuthorize.rpc",{
		serial: serial,
		bonus_to:type
	},
	function(){
		if($('#tblBonus').length>0){
			/*Style dataTable*/
			currentTable = $('#tblBonus').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"oLanguage": {
					"oPaginate": {
						"sFirst": "Primera",
						"sLast": "&Uacute;ltima",
						"sNext": "Siguiente",
						"sPrevious": "Anterior"
					},
					"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
					"sZeroRecords": "No se encontraron resultados",
					"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
					"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
					"sSearch": "Filtro:",
					"sProcessing": "Filtrando.."
				},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
					"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
					"aButtons": ["pdf" ]
				}

			} );
			
			if($("#selAll")){
				$("#selAll").bind("click",checkAll);
				$(".bonusChk", currentTable.fnGetNodes()).each(function(i){
					$(this).bind('click',function(){
						bonusChecked();
					});
				});
			}
		}

		if($("#selToAuthorize").length>0){
			$("#selToAuthorize").rules("add", {
				required: true
			});
		}
		if($("#selectedBonus").length){
			$("#selectedBonus").rules("add", {
				required: true
			});
		}
		if($("#txtObservations").length){
			$("#txtObservations").rules("add", {
				required: true,
				alphaNumeric: true,
				messages: {
					alphaNumeric: 'El campo "Observaciones" admite s&oacute;lo caracteres alfa - num&eacute;ricos.'
				}
			});
		}
	});
}

//checkAll
//select\deselect all the sales
function checkAll() {
	var nodoCheck = document.getElementsByTagName("input");
	var varCheck = document.getElementById("selAll").checked;
	for (i=0; i<nodoCheck.length; i++){
		if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "selAll" && nodoCheck[i].disabled == false) {
			nodoCheck[i].checked = varCheck;
		}
	}
	bonusChecked();
}
//salesChecked
//checks if theres is at least one sale choosen
function bonusChecked(){
	var checked=0;
	$(".bonusChk", currentTable.fnGetNodes()).each(function(i){
		if($(this).is(':checked')){
			checked=1;
			return;
		}
	});
	if(checked==1){
		$('#selectedBonus').val('1');
	}else{
		$('#selectedBonus').val('');
	}
}