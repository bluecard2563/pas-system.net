// JavaScript Document
var pager;

$(document).ready(function(){
	$('#selZone').change(function(){loadCountries($('#selZone').val())});
	if($('#theTable').length>0){
		pager = new Pager('theTable', 7 , 'Siguiente', 'Anterior');
		pager.init(100); //Max Pages
		pager.showPageNav('pager', 'failedPageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}

	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmNewBonus").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
                    selCountry: {
                        required: true
                    },
                    selCity: {
                        required: true
                    },
                    selZone: {
                        required: true
                    },
					selResponsible:{
						required: true
					},
                    txtPercentage: {
                        required: true,
                        percentage: true,
						max_value: 100,
                    },
                    dealersTo: {
                        required: true
                    },
                    productsTo: {
                        required: true
                    }
		},
		messages: {
                    txtPercentage: {
                        percentage: "El campo 'Porcentaje' solo admite valores del 0 al 100 y con dos decimales.",
						max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
                    }
   		}
	});
	/*FIN DE LA SECCIÓN DE VALIDACIONES*/
        $('#btnInsert').bind("click", function(){
        	show_ajax_loader();
            $("#dealersTo option").each(function () {
                $(this).attr('selected','selected');
            });
            $("#productsTo option").each(function(){
                $(this).attr('selected','selected');
            });
            $("#frmNewBonus").submit();
        });
});


/*
 * @Name: loadCountries
 * @Params: zoneId
 * @Description: Load all the countries for a specific zone.
 */


function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){	
			$('#selCountry').change(function(){
                $("#dealersTo option").each(function (i) {
                        $(this).remove();
                });
                $("#dealersFrom option").each(function (i) {
                        $(this).remove();
                });

                loadCities($('#selCountry').val());

                $("#productsTo option").each(function (i) {
                        $(this).remove();
                });
                loadProducts($('#selCountry').val());
            });

			if($('#selCountry option').size()<=2){
				loadCities($('#selCountry').val());
				loadProducts($('#selCountry').val());

				$("#dealersTo option").each(function (i) {
                        $(this).remove();
                });
                $("#dealersFrom option").each(function (i) {
                        $(this).remove();
                });
				$("#productsTo option").each(function (i) {
                        $(this).remove();
                });
			}
        });
}

/*
 * @Name: loadProducts
 * @Params: countryId
 * @Description: Load all the products for a specific country.
 */
function loadProducts(countryId){
	$('#productsFromContainer').load(document_root+"rpc/loadProductsMultiple.rpc",{serial_cou: countryId});
	$("#moveAllProducts").click( function () {
			$("#productsFrom option").each(function (i) {
					$("#productsTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
					$(this).remove();
			});
	});

	$("#removeAllProducts").click( function () {
			$("#productsTo option").each(function (i) {
					$("#productsFrom").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
					$(this).remove();
			});
	});

	$("#moveSelectedProducts").click( function () {
			$("#productsFrom option:selected").each(function (i) {
					$("#productsTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
					$(this).remove();
			});
	});

	$("#removeSelectedProducts").click( function () {
			$("#productsTo option:selected").each(function (i) {
					$("#productsFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
					$(this).remove();
			});
	});
}

/*
 * @Name: loadCities
 * @Params: countryID
 * @Description: Load all the cities for a specific country.
 */
function loadCities(countryID){
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: countryID},function(){
		if($('#selCity').val()){
			loadResponsibles($('#selCity').val());
		}
		$('#selCity').change(function(){
			loadResponsibles($('#selCity').val())
		});
	});
}

/*
 * @Name: loadResponsibles
 * @Params: cityID
 * @Description: Load all the responsibles for a specific city.
 */
function loadResponsibles(cityID){
	if(cityID == '') {
		$('dealersFrom').html('');
	}
	$('#responsibleContainer').load(document_root+"rpc/loadBonus/loadResponsiblesByCity.rpc",{serial_cit: cityID},function(){
		if($('#selResponsible').val()){
			loadDealers($('#selResponsible').val());
		}
		$('#selResponsible').change(function(){
			loadDealers($('#selResponsible').val())
		});
	});
}

/*
 * @Name: loadDealers
 * @Params: countryID
 * @Description: Load all the dealers for a specific country.
 */

function loadDealers(userID){
		var selectedDealers = "";
		$("#dealersTo option").each(function () {
			selectedDealers += $(this).val()+",";
		});
		
        $('#dealersFromContainer').load(document_root+"rpc/loadDealersByUserMultiple.rpc",{serial_cit: $('#selCity').val(), serial_usr:userID, selected_dea:selectedDealers});

        $("#moveAll").click( function () {
                $("#dealersFrom option").each(function (i) {
                        $("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" user="'+$(this).attr('user')+'" selected="selected">'+$(this).text()+'</option>');
                        $(this).remove();
                });
        });

        $("#removeAll").click( function () {
                $("#dealersTo option").each(function (i) {
						if($("#dealersFrom").attr("user")==$(this).attr('user')){
							$("#dealersFrom").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" user="'+$(this).attr('user')+'" selected="selected">'+$(this).text()+'</option>');
						}
						$(this).remove();
                });
        });

        $("#moveSelected").click( function () {
                $("#dealersFrom option:selected").each(function (i) {
                        $("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" user="'+$(this).attr('user')+'" selected="selected">'+$(this).text()+'</option>');
                        $(this).remove();
                });
        });

        $("#removeSelected").click( function () {
                $("#dealersTo option:selected").each(function (i) {
						if($("#dealersFrom").attr("user")==$(this).attr('user')){
							$("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" user="'+$(this).attr('user')+'" selected="selected">'+$(this).text()+'</option>');
						}
						$(this).remove();
                });
        });
        /*END LIST SELECTOR*/
}