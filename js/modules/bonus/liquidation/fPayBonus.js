$(document).ready(function(){
	//validation for prize stock and base
	jQuery.validator.addMethod("validBonus", function(value, element) {
		base=$(element).attr('base');
		type=$(element).attr('p_type');
		stock=$(element).attr('stock');
		if(type=='AMOUNT'){
			return this.optional(element)|| (parseInt(value)<=parseInt(stock) && /^\d+$/.test(value) && parseInt(value)>0);
		}else{
			return this.optional(element) || (parseFloat(value)>=parseFloat(base) && /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value));
		}
	}, "La cantidad debe ser un valor que cumpla con las condiciones del tipo de premio.");

	$("#frmSelectPrizes").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selectedPrize: {
				required: true
			},
			liquidationTotal:{
				max:function(){return ($('#maxBonus').val())}
			}
		}
	});
	
	
	if($('#prizesTable').length>0){
		//prizes table pagination
		pager = new Pager('prizesTable', 10 , 'Siguiente', 'Anterior');
		pager.init(15); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page

		//select at least one prize validation
		 $(".prizesChk").each(function(i){
			$(this).bind('click',function(){
			   prizesChecked();
			   showQuantityField(this);
			});
		 });
		//add validate for quantity inputs
		$(".inputQuantity").each(function(i){
			$(this).rules("add", {
						 required: {
							depends: function(element) {
									 var id=this.id;
									 var prizeId=id.substr(9);
									if($('#chkPrize_'+prizeId).is(':checked'))
										return true;
									else
										return false;
						}
					},
						validBonus:true,
						messages: {
						  validBonus: "La cantidad debe ser un valor que cumpla con las condiciones del tipo de premio."
						}

			});
			$(this).bind('change',function(){
				updateAppliedBonus();
			});
		});
	}
});

//prizesChecked
//checks if theres is at least one sale choosen
function prizesChecked(){
     var checked=0;
    $(".prizesChk").each(function(i){
            if($(this).is(':checked')){
                checked=1;
                return;
            }
    });
    if(checked==1){
        $('#selectedPrize').val('1');
    }else{
        $('#selectedPrize').val('');
    }
}
//show/hide the field for quantity
function showQuantityField(element){
	 var id=element.id;
	var prizeId=id.substr(9);
	if($(element).is(':checked')){
		$('#quantity_'+prizeId).val('0');
		$('#quantity_'+prizeId).css('display', 'inherit');
	}else{
		$('#quantity_'+prizeId).val('');
		$('#quantity_'+prizeId).css('display', 'none');
		updateAppliedBonus();
	}
	
}

//add or sbstract values to the liquidation Total
function updateAppliedBonus(){
	$('#liquidationTotal').val('0.00');
	$(".inputQuantity").each(function(i){
		if($(this).css('display')!='none'){
			base=$(this).attr('base');
			p_type=$(this).attr('p_type');
			stock=$(this).attr('stock');
			if(p_type=='AMOUNT'){
				quantityValue=parseInt($(this).val())*parseFloat(base);
			}else{
				quantityValue=parseFloat($(this).val());
			}
			$('#liquidationTotal').val(parseFloat($('#liquidationTotal').val())+quantityValue);
		}
	});
}

function submit_redirect(){
	if($("#frmSelectPrizes").valid())
		location.href=document_root+'modules/bonus/liquidation/fSelectBonus';
}