// JavaScript Document
$(document).ready(function(){
		$("#frmSelectBonus").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selBonusTo: {
				required: true
			}
		},
		submitHandler: function(form) {	
			form.submit();
		}
	});

	/*CHANGE OPTIONS*/
	$("#selBonusTo").bind("change", function(e){
	    loadBonusForm(this.value);
    });
	/*END CHANGE*/
});

function loadBonusForm(type){
	if(type!=""){
		$('#formContainer').load(document_root+'rpc/loadBonus/loadBonusForm.rpc',{bonus_to: type},function(){
			switch(type){
				case 'DEALER':
					if($("#selDealer").length){
						$("#selDealer").rules("add", {
							 required: true
						});
					}
					if($("#selBranch").length){
						$("#selBranch").rules("add", {
							 required: true
						});
					}
					if($("#selCountry").length){
						$('#selCountry').bind('change',function(){
							loadCities(this.value,type);
						});
						if($("#selCountry option").size() ==2 ){
							loadCities($("#selCountry").val(),type);
						}
					}
					if($("#selBranch").length){
						$("#selCity").rules("add", {
							 required: true
						});
					}
					break;
				case 'COUNTER':
					if($("#selDealer").length){
						$("#selDealer").rules("add", {
							 required: true
						});
					}
					if($("#selBranch").length){
						$("#selBranch").rules("add", {
							 required: true
						});
					}
					if($("#selCountry").length){
						$('#selCountry').bind('change',function(){
							loadCities(this.value,type);
						});
						if($("#selCountry option").size() ==2 ){
							loadCities($("#selCountry").val(),type);
						}
					}
					if($("#selCity").length){
						$("#selCity").rules("add", {
							 required: true
						});
					}
					if($("#selCounter").length){
						$("#selCounter").rules("add", {
							 required: true
						});
					}
					break;
				default:
					alert('Lo sentimos. Hubo un error en la carga de datos');
					break;
			}
				if($("#selCountry").length){
					$("#selCountry").rules("add", {
						 required: true
					});
				}
			$('#btnFindBonus').bind('click',function(){
				$('#bonusContainer').html("");
				if($("#frmSelectBonus").valid()){
					loadBonus();
				}
			});
		});
	}else{
		$('#formContainer').html("");
	}
}

/*
 * @Name: loadCities
 * @Description: Loads the list of cities in a specific country.
 * @Params: countryID,type
 **/
function loadCities(countryID,type){
	$('#bonusContainer').html("");
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#cityContainer').load(document_root+"rpc/loadBonus/loadCitiesForBonus.rpc",
		{serial_cou: countryID,bonus_to:type
		}, function(){
			$('#selCity').bind('change',function(){
				loadDealer(this.value,type);
			});
			if($("#selCity option").size() ==2 ){
				loadDealer($("#selCity").val(),type);
			}

			$("#selCity").rules("add", {
				 required: true
			});
		});
}

/*
 * @Name: loadDealer
 * @Description: Loads the list of dealers in a specific city.
 * @Params: cityID
 **/
function loadDealer(cityID,type){
	$('#bonusContainer').html("");
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	if(cityID!=''){
		$('#dealerContainer').load(document_root+"rpc/loadBonus/loadDealersWithBonus.rpc",
			{serial_cit: cityID,bonus_to:type
			},
			function(){
				$("#selDealer").bind("change", function(e){loadBranch(this.value,type);});
				if($("#selDealer option").size() ==2 ){
						loadBranch($("#selDealer").val(),type);
					}
				$("#selDealer").rules("add", {
					 required: true
				});
			});
	}else{
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
	$("#selDealer").rules("add", {
				 required: true
			});
}

/*
 * @Name: loadBranch
 * @Description: Loads the list of branches of a specific dealer.
 * @Params: dealerID
 **/
function loadBranch(dealerID,type){
	$('#bonusContainer').html("");
	if(dealerID!=''){
	$('#branchContainer').load(
		document_root+"rpc/loadBonus/loadBranchesWithBonus.rpc",
		{serial_dea: dealerID,bonus_to:type},
		function(){
			$("#selBranch").rules("add", {
				 required: true
			});
			if(type=='COUNTER'){
				$("#selBranch").bind("change", function(e){loadCounter(this.value);});
				if($("#selBranch option").size() ==2 ){
						loadCounter($("#selBranch").val());
					}
			}else{
				$("#selBranch").bind("change", function(e){$('#bonusContainer').html("");});
			}
	});
	}else{
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}

/*
 * @Name: loadCounter
 * @Description: Loads the list of counters of a specific branch.
 * @Params: dealerID
 **/
function loadCounter(branchID){
	$('#bonusContainer').html("");
	if(branchID!=''){
	$('#counterContainer').load(
		document_root+"rpc/loadBonus/loadCountersWithBonus.rpc",
		{serial_dea: branchID},function(){
			$("#selCounter").rules("add", {
				 required: true
			});
				$("#selCounter").bind("change", function(e){$('#bonusContainer').html("");});
		});
	}else{
		$('#selCounter').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	}
}



function loadBonus(){
	type=$('#selBonusTo').val();
	if(type=='COUNTER'){
		serial=$('#selCounter').val();
	}else{
		serial=$('#selBranch').val();
	}
	
	show_html_loader('#bonusContainer');
	
	$('#bonusContainer').load(
		document_root+"rpc/loadBonus/loadBonus.rpc",
		{serial: serial,bonus_to:type},function(){
			if($("#selToPay").length>0){
				$("#selToPay").rules("add", {
					 required: true
				});
			}
		});

}