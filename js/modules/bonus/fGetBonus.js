var pager;

$(document).ready(function(){
       
$("#frmSelectPrizes").validate({
                errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selectedPrize: {
				required: true
			},
			liquidationTotal:{
                                min: 1,
                                max_value:$('#availablePoints').val()
			}
		}
	});
        
if($('#prizesTable').length>0){
		//prizes table pagination
		pager = new Pager('prizesTable', 10 , 'Siguiente', 'Anterior');
		pager.init(15); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page

		//select at least one prize validation
		 $(".prizesChk").each(function(i){
			$(this).bind('click',function(){
			   prizesChecked();
			   showQuantityField(this);
			});
		 });
		//add validate for quantity inputs
		$(".inputQuantity").each(function(i){
			$(this).rules("add", {
						 required: {
							depends: function(element) {
									 var id=this.id;
									 var prizeId=id.substr(9);
									if($('#chkPrize_'+prizeId).is(':checked'))
										return true;
									else
										return false;
						}
					},
						digits:true,
						
			});
			$(this).bind('change',function(){
				updateAppliedBonus();
			});
		});
	}

	
});

//prizesChecked
//checks if theres is at least one sale choosen
function prizesChecked(){
     var checked=0;
    $(".prizesChk").each(function(i){
            if($(this).is(':checked')){
                checked=1;
                return;
            }
    });
    if(checked==1){
        $('#selectedPrize').val('1');
    }else{
        $('#selectedPrize').val('');
    }
    
}

//show/hide the field for quantity
function showQuantityField(element){
	 var id=element.id;
	var prizeId=id.substr(9);
	if($(element).is(':checked')){
		$('#quantity_'+prizeId).val('0');
		$('#quantity_'+prizeId).css('display', 'inherit');
	}else{
		$('#quantity_'+prizeId).val('');
		$('#quantity_'+prizeId).css('display', 'none');
                updateAppliedBonus();
	}
	
}

//add or sbstract values to the liquidation Total
function updateAppliedBonus(){
	$('#liquidationTotal').val('0.00');
	$(".inputQuantity").each(function(i){
          	if($(this).css('display')!='none'){
			base=$(this).attr('base');
                        p_type=$(this).attr('p_type');
			if(p_type=='AMOUNT'){
				quantityValue=parseInt($(this).val())*parseFloat(base);
			}else{
				quantityValue=parseFloat($(this).val())*parseFloat(base);
			}
                       $('#liquidationTotal').val(parseFloat($('#liquidationTotal').val())+quantityValue);
                       
		}
	});
}

function submit_redirect(){
        alert('La cantidad debe ser mayor a Cero')
        if(parseFloat($('#liquidationTotal').val())>0){
	if($("#frmSelectPrizes").valid())
               location.href=document_root+'modules/bonus/fGetBonus';
       }else{
           alert('La cantidad debe ser mayor a Cero');
       }
}