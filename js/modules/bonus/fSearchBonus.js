// JavaScript Document
var currentTable;
$(document).ready(function(){
	$('#selZone').bind("change",function(){
		loadCountries($('#selZone').val());
		$("#checkContainer").html("");
	});

	/*VALIDATION SECTION*/
	$("#frmSearchBonus").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			},
			selResponsible:{
				required: true
			},
			selCity: {
				required: true
			},
			selDealer: {
				required: true
			},
			txtNewPercentage: {
				percentage: true,
				max_value: 100
			},
			messages: {
				txtNewPercentage:{
					percentage: 'El campo "Porcentaje" solo admite valores entre 1 y 100.',
					max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'

				}
			}
		},
		submitHandler: function(form) {
			var sData = $("input[id^='chk']", currentTable.fnGetNodes()).serialize();
			$('#selectedBoxes').val(sData);
			form.submit();
		}
	});
    
	$("#frmUpdateBonus").validate({
		errorLabelContainer: "#alertsDialog",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtPercentage: {
				required: true,
				percentage: true,
				max_value: 100
			}
		},
		messages: {
			txtPercentage:{
				percentage: 'El campo "Porcentaje" solo admite valores entre 1 y 100.',
				max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
			}
		}
	});
	/*END VALIDATION SECTION*/


	/*NEW EVENT DIALOG*/
	$("#editBonusDialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 250,
		width: 450,
		modal: true,
		resizable: false,
		title:"Nuevo Evento",
		buttons:  {
			'Guardar': function() {
				$("#frmUpdateBonus").submit();
				if($("#frmUpdateBonus").valid()){
					$(this).dialog('close');
				}
			},
			'Cancelar': function() {
				$(this).dialog('close');
			}
		},
		close: function() {
			$('select.select').show();
		}
	});
	/*END NEW EVENT DIALOG*/


	$('#btnSearch').bind("click",function(){
		displayRegisters();
	});
});

function loadCountries(zoneId)
{
	$('#selCountry').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{
		serial_zon: zoneId
	},function(){
		$('#selCountry').bind("change",function(){
			loadCities($('#selCountry').val());
			$("#checkContainer").html("");
		});
		if($("#selCountry").find('option').size()==2){
			loadCities($('#selCountry').val());
		}
	});
}

function loadCities(countryID)
{
	$('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
	
	$('#cityContainer').load(document_root+"rpc/loadCities.rpc",{
		serial_cou: countryID
	},function(){
		if($('#selCity').val()){
			loadResponsibles($('#selCity').val());
		}
		$('#selCity').bind("change",function(){
			loadResponsibles($('#selCity').val());
			$("#checkContainer").html("");
		});
	});
}

function loadResponsibles(cityID) 
{
	$('#responsibleContainer').load(document_root+"rpc/loadBonus/loadResponsiblesByCity.rpc",{
		serial_cit: cityID
	},
	function(){
		if($('#selResponsible').val()){
			loadDealers($('#selResponsible').val());
		}
		$('#selResponsible').bind("change",function(){
			loadDealers($('#selResponsible').val());
			$("#checkContainer").html("");
		});
	});
}

function loadDealers(userID) 
{
	$('#dealerContainer').load(document_root+"rpc/loadBonus/loadDealersByResponsible.rpc",{
		serial_usr: userID,
		serial_cit:$('#selCity').val()
	},
	function(){
		$("#selDealer").bind("change",function(){
			$("#checkContainer").html("");
		});
	});
}

function validateOneRequired()
{
	var checked=false;

	$("[id^='chk']").each(function (){
		if($(this).is(':checked')){
			checked=true;
			return;
		}
	});

	if(checked){
		$('#hdnchecks').val('1');
	}else{
		$('#hdnchecks').val('');
	}
}

function displayRegisters()
{
	if($('#frmSearchBonus').valid()){
		show_ajax_loader();
		$('#checkContainer').load(document_root+"rpc/loadBonusList.rpc",
		{
			serial_cou:$('#selCountry').val(),
			serial_cit:$('#selCity').val(),
			serial_dea: $('#selDealer').val()
		}, function (){
			//adding new table
			if($('#theTable').length>0){
				currentTable = $('#theTable').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": {
						"oPaginate": {
							"sFirst": "Primera",
							"sLast": "&Uacute;ltima",
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						},
						"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
						"sZeroRecords": "No se encontraron resultados",
						"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
						"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
						"sSearch": "Filtro:",
						"sProcessing": "Filtrando.."
					},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
						"aButtons": ["pdf" ]
					}

				} );
				
				if($('#selAll').length>0){
					/*CheckAll functionality*/
					$('#selAll').bind("click",function(){
						if($('#selAll').attr("checked")) {
							$("input[id^='chk']", currentTable.fnGetNodes()).attr("checked", true);
							$("#hdnchecks").val('1');
						}else {
							$("input[id^='chk']", currentTable.fnGetNodes()).attr("checked", false);
							$("#hdnchecks").val('');
						}
					});
					/*End checkall*/

					$('#txtNewPercentage').rules('add',{
						required: true,
						percentage: true,
						max_value: 100,
						messages: {
							percentage: 'El campo "Porcentaje" solo admite valores entre 1 y 100.',
							max_value:	'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
						}
					});

					$('#hdnchecks').rules('add',{
						required: true
					});

					$("[id^='chk']", currentTable.fnGetNodes()).each(function (){
						$(this).bind('change',function (){
							validateOneRequired();
						})
					});
				}

				if($("img[id^='update']", currentTable.fnGetNodes()).length>0){
					$("img[id^='update']", currentTable.fnGetNodes()).bind("click",function(){
						$("#dealerName").html($(this).attr("dealer"));
						$("#productName").html($(this).attr("product"));
						$("#txtPercentage").val($(this).attr("percentage"));
						$("#hdnSerialDea").val($(this).attr("serialDea"));
						$("#hdnSerialPxc").val($(this).attr("serialPxc"));
						$("#hdnSerialBon").val($(this).attr("value"));
						$("#editBonusDialog").dialog('open');
					});
				}

				if($("img[id^='deactivate']", currentTable.fnGetNodes()).length>0){
					$("img[id^='deactivate']", currentTable.fnGetNodes()).bind("click",function(){
						if(confirm("Est\u00e1 seguro de que quiere desactivar el incentivo?")) {
							show_ajax_loader();
							$.ajax({
								type: "POST",
								url: document_root+"rpc/deactivateBonus.rpc",
								data: ({
									serial_bon : $(this).attr('value')
								}),
								dataType: "jason",
								success: function(data) {
									hide_ajax_loader();
									if(data) {
										alert('Incentivo desactivado exitosamente.');

										$('#txtNewPercentage').rules('remove');
										$('#hdnchecks').rules('remove');

										displayRegisters();
									} else {
										alert('Hubo Errores al desactivar el incentivo.');
									}
								}
							});
						}
					});
				}				
			}
			
			hide_ajax_loader();
		});
	}
}