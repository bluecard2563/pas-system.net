// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmUpdateCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		
		rules: {
			txtCodeCountry:{
				required: true,
				textOnly: true,
				maxlength: 2,
				remote: {
					url: document_root+"rpc/remoteValidation/country/checkCodeCountry.rpc",
					type: "post",
					data: {
					  	serial_cou: function() {
							return $("#hdnSerial_cou").val();	
						}
					}
				}
			},
			txtNameCountry: {
				required: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/country/checkNameCountry.rpc",
					type: "post",
					data: {
					  	serial_cou: function() {
							return $("#hdnSerial_cou").val();	
						}
					}
				}
			},
			txtPhoneCountry:{
				phone: true
			},
			selZone:{
				required: true
			}
		},
		messages: {
			txtCodeCountry: {
				textOnly: 'S&oacute;lo se aceptan caracteres en el campo "C&oacute;digo".',
				maxlength: 'El campo "C&oacute;digo" debe tener m&aacute;ximo 2 caracteres.',
				remote: "Ya existe un pa&iacute;s con el c&oacute;digo ingresado."
				
			},
			txtNameCountry: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: "Ya existe un pa&iacute;s con ese nombre"
			},
                        txtPhoneCountry:{
                            phone: 'Ingrese un n&uacute;mero de tel&eacute;fono v&aacute;lido (ej: +022222222)'
                        }
		}
	});
	/*END VALIDATION SECTION*/
});