// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/

	$("#frmNewCountry").validate({
		
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		
		rules: {
			selZone: {
				required: true
			},
			txtCodeCountry:{
				required: true,
				textOnly: true,
				maxlength: 2,
				remote: {
					url: document_root+"rpc/remoteValidation/country/checkCodeCountry.rpc",
					type: "post"
				}
			},
			txtNameCountry: {
				required: true,
				textOnly: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/country/checkNameCountry.rpc",
					type: "post"
				}
			},
                        txtPhoneCountry:{
                            phone: true
                        },
			fileFlagCou: {
				required: true
			},
			fileCardCou: {
				required: true
			},
			fileBannerCou: {
				required: true
			}
			
		},
		messages: {
			txtCodeCountry: {
				textOnly: 'S&oacute;lo se aceptan caracteres en el campo "C&oacute;digo".',
				maxlength: 'El campo "C&oacute;digo" debe tener m&aacute;ximo 2 caracteres.',
				remote: "Ya existe un pa&iacute;s con el c&oacute;digo ingresado."
				
			},
			txtNameCountry: {
				textOnly: 'S&oacute;lo se aceptan caracteres en el campo "Nombre".',
				maxlength: 'El campo "Nombre" debe tener m&aacute;ximo 30 caracteres.',
				remote: "Ya existe un pa&iacute;s con el nombre ingresado."
				
			},
                        txtPhoneCountry:{
                            phone: 'Ingrese un n&uacute;mero de tel&eacute;fono v&aacute;lido (ej: +022343434)'
                        }
		}
	});
	/*END VALIDATION SECTION*/
});