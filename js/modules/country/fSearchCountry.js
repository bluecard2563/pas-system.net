// JavaScript Document
$(document).ready(function(){	
	/*VALIDATION SECTION*/
	$("#frmSearchCountry").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone: {
				required: true
			},
			selCountry: {
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
	
	/*CHANGE OPTIONS*/
	$("#selZone").bind("change", function(e){
	      loadCountry(this.value);
    });

});




function loadCountry(zoneID){
	if(zoneID==""){
		$("#selCountry option[value='']").attr("selected",true);
	}

	$('#countryContainer').load(
		document_root+"rpc/loadCountries.rpc",
		{serial_zon: zoneID});
}