// JavaScript Document
var amountSum=0;
var currentTable;
var serial_fre;

$(document).ready(function(){
	if($('#comissions_table').length>0) {
		/*Style dataTable*/
		currentTable = $('#comissions_table').dataTable( {
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"oLanguage": {
				"oPaginate": {
					"sFirst": "Primera",
					"sLast": "&Uacute;ltima",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				},
				"sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
				"sZeroRecords": "No se encontraron resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
				"sSearch": "Filtro:",
				"sProcessing": "Filtrando.."
			},
			"sDom": 'T<"clear">lfrtip',
			"oTableTools": {
				"sSwfPath": document_root+"img/dataTables/copy_cvs_xls_pdf.swf",
				"aButtons": ["pdf","xls" ]
			}
		});
		/*Style dataTable*/
		
		setDefaultCalendar($('#txtDateTo'), '-2y', '+0d', '+0d');
		$('#txtDateTo').bind('change', function(){
			clear_preview();
		})
		
		$('#btnFindComissions').bind('click', function(){
			clear_preview();
			var date_filter = $('#txtDateTo').val();
			
			if(displayPreview()){
				$('#preview_container').load(document_root + 'rpc/loadComissions/loadAvailableComissions.rpc', {
					type: 'FREELANCE',
					serial: serial_fre,
					dateTo: date_filter
				}, function(){
					
				});
			}else{
				alert('Escoja al menos un Agente para liquidar');
			}
		});
	} 
});

function displayPreview(){
	var one_selected = false;
	$("input[id^='chkFreelance_']", currentTable.fnGetNodes()).each(function(){
		if($(this).is(':checked')){
			one_selected = true;
			serial_fre = $(this).val();
			return;
		}
	});
	
	return one_selected;
}

function clear_preview(){
	$('#preview_container').html('');
}

function submit_form() {
	$('#frmSelectComissions').attr("action", 'pPayComission');
	document.frmSelectComissions.submit();
	document.frmSelectComissions.reset();
	$('#preview_container').html("");
}

function submit_preview_form() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreview');
	$('#frmSelectComissions').submit();
}
function submit_preview_form_excel() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreviewExcel');
	$('#frmSelectComissions').submit();
}