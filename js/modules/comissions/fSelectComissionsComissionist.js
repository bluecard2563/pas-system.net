// JavaScript Document
$(document).ready(function(){
	$("#frmSelectComissions").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selResponsible: {
				required: true
			},
			txtDateTo: {
				required: true
			}
		}
	});
	
	$('#selCountry').bind('change',function(){
		$('#selResponsible').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		loadCities(this.value);
	});
	if($("#selCountry option").size() ==2 ){
		loadCities($("#selCountry").val());
	}
	
	$('#btnFindComissions').bind('click',function(){
		if($("#frmSelectComissions").valid()){
			loadComissions();
		}
	});

	setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
	/*END CHANGE*/
});

function submit_form() {
	$('#frmSelectComissions').attr("action", 'pPayComission');
	document.frmSelectComissions.submit();
	document.frmSelectComissions.reset();
	$('#comissionsContainer').html("");
}

function submit_preview_form() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreview');
	$('#frmSelectComissions').submit();
}
function submit_preview_form_excel() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreviewExcel');
	$('#frmSelectComissions').submit();
}

/*
 * @Name: loadCities
 * @Description: Loads the cities of a specific country.
 * @Params: countryID
 **/
function loadCities(countryID){
	$('#comissionsContainer').html('');
	$('#cityContainer').load(document_root+"rpc/loadComissions/loadCitiesForComissions.rpc",
		{serial_cou: countryID,comission_to:'RESPONSIBLE'}, function(){
			$('#selCity').bind('change',function(){
				loadResponsiblesByCity(this.value);
			});
			
			if($("#selCity option").size() ==2 ){
				loadResponsiblesByCity($("#selCity").val());
			}

			$("#selCity").rules("add", {
				 required: true
			});
		});
}

/*
 * @Name: loadResponsiblesByCity
 * @Description: Loads the list of responsibles in a specific city.
 * @Params: cityID
 **/
function loadResponsiblesByCity(cityID){
	$('#comissionsContainer').html('');
	$('#reponsiblesContainer').load(document_root+"rpc/loadComissions/loadResponsablesByCityWithComissions.rpc",
		{serial_cit: cityID},
		function(){
			$("#selResponsible").rules("add", {
				 required: true
			});
	});
}

/*
 * @Name: loadComissions
 * @Description: Loads all comission based on the given parameters.
 * @Params: dealerID
 **/
function loadComissions(){
	$('#comissionsContainer').html('Cargando resultados...');
	$('#comissionsContainer').load(document_root+"rpc/loadComissions/loadAvailableComissions.rpc", {
			type: 'RESPONSIBLE', 
			serial: $('#selResponsible').val(),
			dateTo: $('#txtDateTo').val()});
}