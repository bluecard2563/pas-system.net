// JavaScript Document
$(document).ready(function(){
	$("#frmSelectComissions").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			selSubManager: {
				required: true
			},
			txtDateTo: {
				required: true
			}
		}
	});
	
	setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
	
	$('#selCountry').bind('change',function(){
		$('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selSubManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		loadManagers(this.value);
	});
	
	if($("#selCountry option").size() ==2 ){
		loadManagers($("#selCountry").val());
	}

	$('#btnFindComissions').bind('click',function(){
		if($("#frmSelectComissions").valid()){
			loadComissions();
		}
	});

});

function submit_form() {
	$('#frmSelectComissions').attr("action", 'pPayComission');
	document.frmSelectComissions.submit();
	document.frmSelectComissions.reset();
	$('#comissionsContainer').html("");
}

function submit_preview_form() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreview');
	$('#frmSelectComissions').submit();
}
function submit_preview_form_excel() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreviewExcel');
	$('#frmSelectComissions').submit();
}


/*
 * @Name: loadCities
 * @Description: Loads the cities of a specific country.
 * @Params: countryID
 **/
function loadManagers(countryID){
	$('#comissionsContainer').html('');
	
	$('#managerContainer').load(document_root+"rpc/loadComissions/loadManagersForSubManagers.rpc",
		{
			serial_cou: countryID
		}, function(){
			$('#selManager').bind('change',function(){
				loadSubManagers($(this).val());
			});

			if($("#selManager option").size() == 2 ){
				loadSubManagers($("#selManager").val());
			}
		});
}

function loadSubManagers(serial_mbc){
	$('#comissionsContainer').html('');
	
	$('#subManagerContainer').load(document_root+"rpc/loadComissions/loadSubManagersWithComissions.rpc",
		{ serial_mbc: serial_mbc });
}

/*
 * @Name: loadComissions
 * @Description: Loads all comission based on the given parameters.
 * @Params: dealerID
 **/
function loadComissions(){
	$('#comissionsContainer').html('Cargando resultados...');
	$('#comissionsContainer').load(document_root + "rpc/loadComissions/loadAvailableComissions.rpc", {
			type: 'SUBMANAGER', 
			serial: $('#selSubManager').val(),
			dateTo: $('#txtDateTo').val()});
}