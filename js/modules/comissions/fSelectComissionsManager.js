// JavaScript Document
$(document).ready(function(){
	$("#frmSelectComissions").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selManager: {
				required: true
			},
			txtDateTo: {
				required: true
			}
		}
	});
	
	setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
	
	$('#selCountry').bind('change',function(){
		loadManagers(this.value);
	});
	if($("#selCountry option").size() ==2 ){
		loadManagers($("#selCountry").val());
	}

	$('#btnFindComissions').bind('click',function(){
		if($("#frmSelectComissions").valid()){
			loadComissions();
		}
	});
});

function submit_form() {
	$('#frmSelectComissions').attr("action", 'pPayComission');
	document.frmSelectComissions.submit();
	document.frmSelectComissions.reset();
	$('#comissionsContainer').html("");
}

function submit_preview_form() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreview');
	$('#frmSelectComissions').submit();
}
function submit_preview_form_excel() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreviewExcel');
	$('#frmSelectComissions').submit();
}

/*
 * @Name: loadComissions
 * @Description: Loads all comission based on the given parameters.
 * @Params: dealerID
 **/
function loadComissions(){
	$('#comissionsContainer').html('Cargando resultados...');
	$('#comissionsContainer').load(document_root+"rpc/loadComissions/loadAvailableComissions.rpc",{	
			type: 'MANAGER', 
			serial: $('#selManager').val(),
			dateTo: $('#txtDateTo').val()});
}
/*
 * @Name: loadManagers
 * @Description: Loads the list of managers of a specific country.
 * @Params: countryId
 **/
function loadManagers(countryId){
	$('#comissionsContainer').html('');
	$('#managerContainer').load(document_root+"rpc/loadComissions/loadManagerWithComissionByCountry.rpc", {	serial_cou: countryId});
}