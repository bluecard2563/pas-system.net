// JavaScript Document
$(document).ready(function(){
	$("#frmSelectComissions").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selCountry: {
				required: true
			},
			selCity: {
				required: true
			},
			selDealer: {
				required: true
			},
			selBranch: {
				required: true
			},
			txtDateTo: {
				required: true
			}
		}
	});
	
	setDefaultCalendar($("#txtDateTo"),'-100y','+0d');
	
	$('#selCountry').bind('change',function(){
		$('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
		loadCities(this.value);
	});
	if($("#selCountry option").size() ==2 ){
		loadCities($("#selCountry").val());
	}

	$('#btnFindComissions').bind('click',function(){
		if($("#frmSelectComissions").valid()){
			loadComissions();
		}
	});

});

function submit_form() {
	$('#frmSelectComissions').attr("action", 'pPayComission');
	document.frmSelectComissions.submit();
	document.frmSelectComissions.reset();
	$('#comissionsContainer').html("");
}

function submit_preview_form() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreview');
	$('#frmSelectComissions').submit();
}
function submit_preview_form_excel() {
	$('#frmSelectComissions').attr("action", 'pPrintComissionsPreviewExcel');
	$('#frmSelectComissions').submit();
}


/*
 * @Name: loadCities
 * @Description: Loads the cities of a specific country.
 * @Params: countryID
 **/
function loadCities(countryID){
	$('#comissionsContainer').html('');
	$('#cityContainer').load(document_root+"rpc/loadComissions/loadCitiesForComissions.rpc",
		{serial_cou: countryID,comission_to: 'DEALER'}, function(){
			$('#selCity').bind('change',function(){
				loadDealer(this.value);
			});

			if($("#selCity option").size() ==2 ){
				loadDealer($("#selCity").val());
			}

			$("#selCity").rules("add", {
				 required: true
			});
		});
}

/*
 * @Name: loadDealer
 * @Description: Loads the list of dealers in a specific city.
 * @Params: cityID
 **/
function loadDealer(cityID){
	$('#comissionsContainer').html('');
	$('#selBranch').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');

	if(cityID!=''){
		$('#dealerContainer').load(document_root+"rpc/loadComissions/loadDealersWithComissions.rpc",
			{	serial_cit: cityID
			},
			function(){
				$("#selDealer").bind("change", function(e){loadBranch(this.value);});
				if($("#selDealer option").size() ==2 ){
						loadBranch($("#selDealer").val());
					}
				$("#selDealer").rules("add", {
					 required: true
				});
			});
	}
}

/*
 * @Name: loadBranch
 * @Description: Loads the list of branches of a specific dealer.
 * @Params: dealerID
 **/
function loadBranch(dealerID){
	$('#comissionsContainer').html('');
	$('#branchContainer').load(
		document_root+"rpc/loadComissions/loadBranchesWithComissions.rpc",
		{serial_dea: dealerID},
		function(){
			$("#selBranch").rules("add", {
				 required: true
			});
	});
}

/*
 * @Name: loadComissions
 * @Description: Loads all comission based on the given parameters.
 * @Params: dealerID
 **/
function loadComissions(){
	$('#comissionsContainer').html('Cargando resultados...');
	$('#comissionsContainer').load(document_root + "rpc/loadComissions/loadAvailableComissions.rpc", {
			type: 'DEALER', 
			serial: $('#selBranch').val(),
			dateTo: $('#txtDateTo').val()});
}