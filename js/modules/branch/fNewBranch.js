// JavaScript Document
$(document).ready(function() {
    $("#selCountry").bind("change", changeFlag);
    var today = $('#today').val();

    /*VALIDATION SECTION*/
    $("#frmNewBranch").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selZone: {
                required: true
            },
            selCountry: {
                required: true
            },
            selCity: {
                required: true
            },
            selSector: {
                required: true
            },
            selManager: {
                required: true
            },
            txtID: {
                required: true,
                alphaNumeric: true,
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkValidDocument.rpc",
                    type: "post",
                    data: {
                        country: function() {
                            return $('#selCountry').val();
                        }
                    }
                }
            },
            txtCode: {
                required: true,
                digits: true,
                minlength: 4,
                maxlength: 4
            },
            hdnFlagCode: {
                remote: {
                    url: document_root + "rpc/remoteValidation/dealer/checkDealerCode.rpc",
                    type: "post",
                    data: {
                        serial_cou: function() {
                            return $("#selCountry").val();
                        },
                        txtCode: function() {
                            return $("#txtCode").val();
                        }
                    }
                }
            },
            selCategory: {
                required: true
            },
            txtName: {
                required: true
            },
            txtBranchNumber: {
                required: true,
                digits: true,
                maxlength: 11,
                remote: {
                    url: document_root + "rpc/remoteValidation/branch/checkBranchNumber.rpc",
                    type: "post",
                    data: {
                        dea_serial_dea: function() {
                            return $("#hdnSerialDealer").val();
                        }
                    }
                }
            },
            selDealer: {
                required: true
            },
            txtAddress: {
                required: true
            },
            txtPhone1: {
                required: true,
                number: true
            },
            txtPhone2: {
                number: true
            },
            txtContractDate: {
                required: true,
                date: true,
                dateLessThan: today
            },
            txtFax: {
                number: true
            },
            txtMail1: {
                required: true,
                email: true
            },
            txtMail2: {
                required: true,
                email: true,
                //notEqualTo: "#txtMail1"
            },
            txtContact: {
                required: true,
                textOnly: true
            },
            txtContactPhone: {
                required: true,
                number: true
            },
            txtContactMail: {
                required: true,
                email: true
            },
            txtVisitAmount: {
                required: true,
                number: true,
                min: 1,
                max: 20
            },
            selStatus: {
                required: true
            },
            selCreditD: {
                required: true
            },
            txtContactPay: {
                required: true,
                textOnly: true
            },
            chkBillTo: {
                required: true
            },
            selPayment: {
                required: true
            },
            txtDiscount: {
                required: true,
                max_value: 100,
                percentage: true

            },
            txtPercentage: {
                required: true,
                percentage: true,
                max_value: 100
            },
            selType: {
                required: true
            },
            txtAssistName: {
                required: true,
                textOnly: true
            },
            txtAssistEmail: {
                required: true,
                email: true
            },
            chkBonusTo: {
                required: true
            },
            txtNameManager: {
                required: true,
                textOnly: true
            },
            txtPhoneManager: {
                required: true,
                number: true
            },
            txtMailManager: {
                required: true,
                email: true
            },
            txtBirthdayManager: {
                date: true
                        //dateLessThan: today
            },
            txtAccountNumber: {
                digits: true
            },
            txtMinimumKits: {
                max_value: 50
            }
        },
        messages: {
            txtName: {
                textOnly: 'El campo "Nombre" admite s&oacute;lo texto.'
            },
            txtBranchNumber: {
                digits: 'El campo "N&uacute;mero de Sucursal" admite s&oacute;lo d&iacute;gitos.',
                maxlength: 'El campo "N&uacute;mero de Sucursal de Comercializador" debe tener m&aacute;ximo 11 d&iacute;gitos.',
                remote: 'Existe otra sucursal de comercializador con el mismo "N&uacute;mero de Sucursal de Comercializador" para el comercializador seleccionado.'
            },
            txtCode: {
                digits: 'El campo "C&oacute;digo" admite s&oacute;lo d&iacute;gitos.',
                minlength: 'El campo "C&oacute;digo" debe tener 4 d&iacute;gitos.',
                maxlength: 'El campo "C&oacute;digo" debe tener 4 d&iacute;gitos.'
            },
            hdnFlagCode: {
                remote: 'Existe otro comercializador con el mismo c&oacute;digo en el sistema para este pa&iacute;s.'
            },
            txtID: {
                alphaNumeric: 'El campo "Id" admite s&oacute;lo caracteres alfa-num&eacute;ricos.',
                remote: 'El campo "Id" es incorrecto. Por favor ingrese un RUC v&aacute;lido.'
            },
            txtPhone1: {
                number: 'El campo "Tel&eacute;fono #1" admite s&oacute;lo n&uacute;meros.'
            },
            txtPhone2: {
                number: 'El campo "Tel&eacute;fono #2" admite s&oacute;lo n&uacute;meros.'
            },
            txtContractDate: {
                date: 'El campo "Fecha de contrato" debe tener formato fecha.',
                dateLessThan: 'La fecha de contrato no puede ser mayor a la fecha actual.'
            },
            txtFax: {
                number: 'El campo "Fax" admite s&oacute;lo n&uacute;meros.'
            },
            txtMail1: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #1"'
            },
            txtMail2: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #2"',
                notEqualTo: 'E-mail #2: Ingrese un e-mail diferente que el anterior.'
            },
            txtContact: {
                textOnly: 'El campo "Nombre del Contacto" admite s&oacute;lo texto.'
            },
            txtContactPhone: {
                number: 'El campo "Tel&eacute;fono del Contacto" admite s&oacute;lo n&uacute;meros.'
            },
            txtContactMail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Contacto"'
            },
            txtVisitAmount: {
                number: 'El campo "N&uacute;mero de visitas al mes" admite s&oacute;lo n&uacute;meros.',
                min: 'El campo "N&uacute;mero de visitas al mes" debe ser m&iacute;nimo 1.',
                max: 'El campo "N&uacute;mero de visitas al mes" admite solo hasta 20.'
            },
            txtContactPay: {
                textOnly: 'El campo "Contacto Pago" admite s&oacute;lo texto.'
            },
            txtPercentage: {
                percentage: 'El campo "Porcentaje" debe tener formato de porcentaje.',
                max_value: 'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
            },
            txtAssistName: {
                textOnly: 'El campo "Nombre (Asistencias)" admite s&oacute;lo texto.'
            },
            txtAssistEmail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail (Asistencias)"'
            },
            txtNameManager: {
                required: 'El campo de "Nombre del Gerente" es obligatorio.',
                textOnly: 'Ingrese solamente texto en el campo "Nombre del Gerente".'
            },
            txtPhoneManager: {
                required: 'El campo "Tel&eacute;fono del Gerente" es obligatorio.',
                number: 'Ingrese solamente n&uacute;meros en el campo "Tel&eacute;fono del Gerente".'
            },
            txtMailManager: {
                required: 'El campo de "E-mail del Gerente" es obligatorio.',
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Gerente".'
            },
            txtBirthdayManager: {
                date: 'El campo "Fecha de Nacimiento" debe tener formato fecha.'
                        //dateLessThan:'La Fecha de Nacimiento no puede ser mayor a la fecha actual.'
            },
            txtAccountNumber: {
                digits: 'El campo "N&uacute;mero de cuenta" acepta solo d&iacute;gitos.'
            },
            txtMinimumKits: {
                max_value: 'El campo "M&iacute;nimo de Stock" admite valores hasta 50%'
            }
        },
        submitHandler: function(form) {
            var responsible = false;
            if ($("#selComissionistPa").val() || $("#selComissionist").val()) {
                responsible = true;
            }
            else {
                alert('Seleccione un responsable.');
            }
            if ($("#frmNewBranch").valid() && responsible) {
                $("#btnRegister").attr('disabled', 'true');
                $("#btnRegister").val('Espere por favor...');
                form.submit();
            }
        }
    });
    /*END VALIDATION SECTION*/

    /*CALENDAR*/
    setDefaultCalendar($("#txtContractDate"), '-100y', '+0d', '-18');
    setDefaultCalendar($("#txtBirthdayManager"), '-100y', '+0d', '-18y');
    /*END CALENDAR*/

    /*CHANGE OPTIONS*/
    $("#selCountry").bind("change", function(e) {
        loadManagersByCountry(this.value);
        loadCities(this.value, 'Dealer');
        loadCities(this.value, '');
        loadCreditDays(this.value);
    });
    if ($("#selCountry option").size() == 2) {
        loadCities($("#selCountry").val(), 'Dealer');
        loadCities($("#selCountry").val(), '');
        loadManagersByCountry($("#selCountry").val());
        loadCreditDays($("#selCountry").val());
    }
    $("#selComissionist").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionist").val());
    });
    $("#selComissionistPa").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionistPa").val());
    });

    $("#selCityDealer").bind("change", function(e) {
        loadSector(this.value, "Dealer");
    });
    $("#selCity").bind("change", function(e) {
        loadSector(this.value, "");
    });
    /*END CHANGE*/
});

function changeFlag() {
    $('#hdnFlagCode').val($('#hdnFlagCode').val() + 1);
}

function loadCities(countryID, selectID) {
    $("#dealerContainer option[value='']").attr("selected", true);
    $('#generalInfo').html("Seleccione un comercializador.");
    loadDealersBySector('');
    loadSector('', "Dealer");
    loadSector('');

    if (selectID == 'Dealer') {
        $('#cityDealerContainer').load(document_root + "rpc/loadCities.rpc", {
            serial_cou: countryID,
            serial_sel: "Dealer"
        },
        function() {
            $("#selCityDealer").bind("change", function(e) {
                loadSector(this.value, "Dealer");
            });
            if ($("#selCityDealer option").size() == 2) {
                loadSector($("#selCityDealer").val(), 'Dealer');
            }
        });
    } else {
        $('#cityContainer').load(document_root + "rpc/loadCities.rpc", {
            serial_cou: countryID
        },
        function() {
            $("#selCity").bind("change", function(e) {
                loadSector(this.value, "");
            });
            if ($("#selCity option").size() == 2) {
                loadSector($("#selCity").val(), '');
            }
        });
    }
}

function loadSector(cityID, selectID) {
    if (selectID == "Dealer") {
        $('#generalInfo').html("Seleccione un comercializador.");
        loadDealersBySector('');
        $('#sectorDealerContainer').load(document_root + "rpc/loadSectors.rpc",
                {
                    serial_cit: cityID,
                    serial_sel: selectID
                },
        function() {
            $("#selSectorDealer").bind("change", function(e) {
                loadDealersBySector(this.value);
            });
            if ($("#selSectorDealer option").size() == 2) {
                loadDealersBySector($("#selSectorDealer").val());
            }
        });
    }
    else {
        $('#sectorContainer').load(document_root + "rpc/loadSectors.rpc",
                {
                    serial_cit: cityID,
                    serial_sel: selectID
                });
    }
}

function loadCreditDays(countryID) {
    $('#creditDContainer').load(document_root + "rpc/loadCreditDays.rpc", {
        serial_cou: countryID
    });
}

function loadManagersByCountry(countryID) {
    if (countryID) {
        $('#managerDealerContainer').load(document_root + "rpc/loadManagersByCountry.rpc", {
            serial_cou: countryID
        }, function() {
            $("#selManager").bind("change",
                    function() {
                        loadUsersByManager(this.value);
                    })
            loadUsersByManager($("#selManager").val());
        });
    }
    else {
        $('#managerDealerContainer').html('');
    }
}
function loadUsersByManager(mbcID) {
    if (mbcID) {
        $('#comissionistContainer').load(document_root + "rpc/loadUsersByManager.rpc", {
            serial_mbc: mbcID
        });
    }
    else {
        $('#comissionistContainer').html('Seleccione un representante.');
    }
}
function loadDealersBySector(sectorID) {
    $('#generalInfo').html("Seleccione un comercializador.");
    if (sectorID == '') {
        sectorID = 'stop';
    }
    $('#dealerContainer').load(document_root + "rpc/loadDealers.rpc",
            {
                serial_sec: sectorID,
                dea_serial_dea: 'NULL',
                phone_sales: 'ALL'
            },
    function() {
        $("#selDealer").bind("change",
                function() {
                    loadDealerData(this.value);
                })
        if ($("#selDealer option").size() == 2) {
            loadDealerData($("#selDealer").val());
        }
    });
}

function loadDealerData(dealerID) {
    if (dealerID) {
        if (confirm("Desea cargar los datos del comercializador seleccionado?")) {
            var confirmation = 1;
        }

        $('#generalInfo').load(document_root + "rpc/loadDealerInfo.rpc", {
            confirmation: confirmation,
            id_dea: dealerID
        },
        function() {
            $("#txtContractDate").datepicker();
            $('#txtContractDate').datepicker('option', {
                dateFormat: 'dd/mm/yy',
                maxDate: '+0d',
                changeMonth: true,
                changeYear: true
            })
            $("#branchNumber").load(document_root + "rpc/loadBranchNumber.rpc", {
                serialDea: dealerID
            })

            $('#txtPercentage').bind('change', function() {
                if ($('#hdnManagerRights').val() == 'YES') {
                    var parent_comission = parseFloat($('#hdnParentComission').val());

                    if (parseFloat($(this).val()) > parent_comission) {
                        alert('El porcentage no puede ser mayor al del comercializador.');
                        $('#txtPercentage').val('');
                    }
                }
            });
        });

        $('#managerInfo').load(document_root + "rpc/loadManagerInfo.rpc", {
            confirmation: confirmation,
            id_dea: dealerID
        });

        $.ajax({
            url: document_root + "rpc/getUserByDealer.rpc",
            type: "post",
            data: ({
                serial_dea: dealerID,
                type: 'new'
            }),
            success: function(data) {
                var aux = String(eval('(' + data + ')'));
                var user_data = aux.split(',');
                if (user_data[1] == 1) {
                    $("#selComissionistPa").val(user_data[0]).attr("selected", "selected");
                    $("#rdoSelComissionistPa").attr("checked", "checked");
                    $("#selComissionist").attr("disabled", "disabled");
                    $("#selComissionistPa").attr("disabled", "");
                    $("#hdnSerialUsr").val($("#selComissionistPa").val());
                }
                else {
                    $("#selComissionist").val(user_data[0]).attr("selected", "selected");
                    $("#rdoSelComissionistMa").attr("checked", "checked");
                    $("#selComissionistPa").attr("disabled", "disabled");
                    $("#selComissionist").attr("disabled", "");
                    $("#hdnSerialUsr").val($("#selComissionist").val());
                }
            }
        });
    } else {
        $('#generalInfo').html('');
        $('#managerInfo').html('');
    }
}
