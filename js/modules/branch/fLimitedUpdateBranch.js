// JavaScript Document
$(document).ready(function() {
    var today = $('#today').val();

    if ($("#selComissionistPa").val() == '') {
        $("#selComissionistPa").attr('disabled', true);
        $("#rdoSelComissionistMa").attr('checked', true);
    }
    else {
        $("#selComissionistMa").attr('disabled', true);
        $("#rdoSelComissionistPa").attr('checked', true);
    }
    $('#rdoSelComissionistMa').bind('click', function(e) {
        $('#selComissionistPa').attr('disabled', true);
        $('#selComissionistPa').val('');
        $('#selComissionistMa').attr('disabled', false);
        $('#rdoSelComissionistPa').attr('checked', false);
    });
    $('#rdoSelComissionistPa').bind('click', function(e) {
        $('#selComissionistMa').attr('disabled', true);
        $('#selComissionistMa').val('');
        $('#selComissionistPa').attr('disabled', false);
        $('#rdoSelComissionistMa').attr('checked', false);
    });

    $("#selStatus").bind("change", function() {
        if ($("#selStatus").val() == "INACTIVE") {
            if (!confirm("Esto afectara a todo el sistema. Esta seguro de querer cambiar?")) {
                $("#selStatus").val("ACTIVE");
            }
        }
    });
    /*VALIDATION SECTION*/
    $("#frmUpdateBranch").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountry: {
                required: true
            },
            selCity: {
                required: true
            },
            selSector: {
                required: true
            },
            selCategory: {
                required: true
            },
            txtName: {
                required: true
            },
            txtBranchNumber: {
                required: true,
                digits: true,
                maxlength: 11,
                remote: {
                    url: document_root + "rpc/remoteValidation/branch/checkBranchNumber.rpc",
                    type: "post",
                    data: {
                        dea_serial_dea: function() {
                            return $("#hdnDeaSerialDealer").val();
                        },
                        serial_dea: function() {
                            return $("#hdnSerialDealer").val();
                        }
                    }
                }
            },
            selDealerT: {
                required: true
            },
            txtAddress: {
                required: true
            },
            txtPhone1: {
                required: true,
                number: true
            },
            txtPhone2: {
                number: true
            },
            txtContractDate: {
                required: true,
                date: true,
                dateLessThan: today
            },
            txtFax: {
                number: true
            },
            txtMail1: {
                required: true,
                email: true
            },
            txtMail2: {
                required: true,
                email: true,
                //notEqualTo: "#txtMail1"
            },
            txtContact: {
                required: true,
                textOnly: true
            },
            txtContactPhone: {
                required: true,
                number: true
            },
            txtContactMail: {
                required: true,
                email: true
            },
            txtVisitAmount: {
                required: true,
                number: true,
                min: 1,
                max: 20
            },
            selStatus: {
                required: true
            },
            selCreditD: {
                required: true
            },
            txtContactPay: {
                required: true,
                textOnly: true
            },
            chkBillTo: {
                required: true
            },
            selPayment: {
                required: true
            },
            txtDiscount: {
                required: true,
                percentage: true,
                max_value: 100
            },
            txtPercentage: {
                required: true,
                max_value: 100,
                percentage: true

            },
            selType: {
                required: true
            },
            txtAssistName: {
                required: true,
                textOnly: true
            },
            txtAssistEmail: {
                required: true,
                email: true
            },
            txtNameManager: {
                required: true,
                textOnly: true
            },
            txtPhoneManager: {
                required: true,
                number: true
            },
            txtMailManager: {
                required: true,
                email: true
            },
            txtBirthdayManager: {
                date: true
                        //dateLessThan: today
            },
            txtMinimumKits: {
                max_value: 50
            }
        },
        messages: {
            txtName: {
                textOnly: 'El campo "Nombre" admite s&oacute;lo texto.'
            },
            txtBranchNumber: {
                digits: 'El campo "N&uacute;mero de Sucursal de Comercializador" admite s&oacute;lo d&iacute;gitos.',
                maxlength: 'El campo "N&uacute;mero de Sucursal de Comercializador" debe tener m&aacute;ximo 11 d&iacute;gitos.',
                remote: 'Existe otra sucursal de Comercializador con el mismo "N&uacute;mero de Sucursal de Comercializador" para el comercializador seleccionado.'
            },
            txtPhone1: {
                number: 'El campo "Tel&eacute;fono #1" admite s&oacute;lo n&uacute;meros.'
            },
            txtPhone2: {
                number: 'El campo "Tel&eacute;fono #2" admite s&oacute;lo n&uacute;meros.'
            },
            txtContractDate: {
                date: 'El campo "Fecha de contrato" debe tener formato fecha.',
                dateLessThan: 'La fecha de contrato no puede ser mayor a la fecha actual.'
            },
            txtFax: {
                number: 'El campo "Fax" admite s&oacute;lo n&uacute;meros.'
            },
            txtMail1: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #1"'
            },
            txtMail2: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail #2"',
                notEqualTo: 'E-mail #2: Ingrese un e-mail diferente que el anterior.'
            },
            txtContact: {
                required: true,
                textOnly: 'El campo "Nombre del Contacto" admite s&oacute;lo texto.'
            },
            txtContactPhone: {
                number: 'El campo "Tel&eacute;fono del Contacto" admite s&oacute;lo n&uacute;meros.'
            },
            txtContactMail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Contacto"'
            },
            txtVisitAmount: {
                number: 'El campo "N&uacute;mero de visitas al mes" admite s&oacute;lo n&uacute;meros.',
                min: 'El campo "N&uacute;mero de visitas al mes" debe ser m&iacute;nimo 1.',
                max: 'El campo "N&uacute;mero de visitas al mes" admite solo hasta 20.'
            },
            txtContactPay: {
                textOnly: 'El campo "Contacto Pago" admite s&oacute;lo texto.'
            },
            txtPercentage: {
                percentage: 'El campo "Porcentaje" debe tener formato de porcentaje.',
                max_value: 'El campo "Porcentaje" solo admite valores menores o iguales a 100.'
            },
            txtAssistName: {
                textOnly: 'El campo "Nombre (Asistencias)" admite s&oacute;lo texto.'
            },
            txtAssistEmail: {
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail (Asistencias)"'
            },
            txtNameManager: {
                required: 'El campo de "Nombre del Gerente" es obligatorio.',
                textOnly: 'Ingrese solamente texto en el campo "Nombre del Gerente".'
            },
            txtPhoneManager: {
                required: 'El campo "Tel&eacute;fono del Gerente" es obligatorio.',
                number: 'Ingrese solamente n&uacute;meros en el campo "Tel&eacute;fono del Gerente".'
            },
            txtMailManager: {
                required: 'El campo de "E-mail del Gerente" es obligatorio.',
                email: 'Por favor ingrese un e-mail con el formato nombre@dominio.com para el campo "E-mail del Gerente".'
            },
            txtBirthdayManager: {
                date: 'El campo "Fecha de Nacimiento" debe tener formato fecha.'
                        //dateLessThan:'La Fecha de Nacimiento no puede ser mayor a la fecha actual.'
            },
            txtMinimumKits: {
                max_value: 'El campo "M&iacute;nimo de Stock" admite valores hasta 50%'
            }
        },
        submitHandler: function(form) {
            var responsible = false;
            if ($("#selComissionistPa").length > 0 && $("#selComissionistMa").length > 0) {
                if ($("#selComissionistPa").val() || $("#selComissionistMa").val()) {
                    responsible = true;
                } else {
                    alert('Seleccione un responsable.');
                }
            } else {
                responsible = true;
            }

            if ($("#frmUpdateBranch").valid() && responsible) {
                $("#btnRegistrar").attr('disabled', 'true');
                $("#btnRegistrar").val('Espere por favor...');
                form.submit();
            }
        }
    });
    /*END VALIDATION SECTION*/

    /*CALENDAR*/
    setDefaultCalendar($("#txtBirthdayManager"), '-100y', '+0d', '-18y');
    /*END CALENDAR*/

    /*CHANGE OPTIONS*/
    $("#selCountry").bind("change", function(e) {
        loadCities(this.value);
        loadCreditDays(this.value);
    });
    $("#selComissionistMa").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionistMa").val());
    });
    $("#selComissionistPa").bind("change", function(e) {
        $("#hdnSerialUsr").val($("#selComissionistPa").val());
    });
    /*END CHANGE*/
    /*SELECT RESPONSIBLE*/
    var dealerID = $("#hdnSerialDealer").val();
    $.ajax({
        url: document_root + "rpc/getUserByDealer.rpc",
        type: "post",
        data: ({
            serial_dea: dealerID,
            type: 'update'
        }),
        success: function(data) {
            var aux = String(eval('(' + data + ')'));
            var user_data = aux.split(',');
            if (user_data[1] == 1) {
                $("#selComissionistPa").val(user_data[0]).attr("selected", "selected");
                $("#rdoSelComissionistPa").attr("checked", "checked");
                $("#selComissionistMa").attr("disabled", "disabled");
                $("#selComissionistPa").attr("disabled", "");
                $("#hdnSerialUsr").val($("#selComissionistPa").val());
            }
            else {
                $("#selComissionistMa").val(user_data[0]).attr("selected", "selected");
                $("#rdoSelComissionistMa").attr("checked", "checked");
                $("#selComissionistPa").attr("disabled", "disabled");
                $("#selComissionistMa").attr("disabled", "");
                $("#hdnSerialUsr").val($("#selComissionistMa").val());
            }
        }
    });
    /*END SELECT RESPONSIBLE*/

    $('#txtPercentage').bind('change', function() {
        if ($('#hdnManagerRights').val() == 'YES') {
            var parent_comission = parseFloat($('#hdnParentComission').val());

            if (parseFloat($(this).val()) > parent_comission) {
                alert('El porcentage no puede ser mayor al del comercializador.');
                $('#txtPercentage').val('');
            }
        }
    });
});

function loadCities(countryID) {
    $('#sectorContainer').load(document_root + "rpc/loadSectors.rpc");
    $('#cityContainer').load(document_root + "rpc/loadCities.rpc", {
        serial_cou: countryID
    }, function() {
        $("#selCity").bind("change", function(e) {
            loadSector(this.value);
        });
    });
}

function loadSector(cityID) {
    $('#sectorContainer').load(document_root + "rpc/loadSectors.rpc", {
        serial_cit: cityID
    });
}

function loadCreditDays(countryID) {
    $('#creditDContainer').load(document_root + "rpc/loadCreditDays.rpc", {
        serial_cou: countryID
    });
}
