// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmEditBranchDocument").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNewDocument: {
				required: true,
				remote: {
					url: document_root+"rpc/remoteValidation/dealer/checkValidDocument.rpc",
					type: "post",
					data: {
						txtID: function(){
							return $('#txtNewDocument').val();
						}
					}
				}
			},
			txtNewDocumentConf: {
				required: true,
				equalTo: '#txtNewDocument'
			},
			txtAccountNumber: {
				digits: true
			}
		},
		messages: {
			txtNewDocument: {
				remote: 'El campo "Id" es incorrecto. Por favor ingrese un RUC v&aacute;lido.'
			},
			txtNewDocumentConf: {
				equalTo: "La confirmaci&oacute;n del documento no coincide."
			},
			txtAccountNumber: {
				digits: "El campo 'N&uacute;mero de Cuenta' admite solo d&iacute;gitos."
			}
		}
	});
	/*END VALIDATION SECTION*/

	var ruta=document_root+"rpc/branch/loadBranchesAutocompleter.rpc";
	$("#txtBranch").autocomplete(ruta,{
		max: 10,
		scroll: false,
		matchContains:true,
		minChars: 2,
  		formatItem: function(row) {
			if(row[0]=='no_record'){
				return 'No existen resultados';
			}else{
				return row[1] ;
			}
  		}
	}).result(function(event, row) {
		if(row[1]=='no_record'){
			$("#hdnBranchID").val('');
			$("#txtBranch").val('');
		}else{
			$("#txtBranch").val(row[1]);
			$("#hdnBranchID").val(row[0]);
		}
	});
	
	$("#txtBranch").bind('keypress', function(){
		$("#hdnBranchID").val('');
		$('#branchInfoContainer').html("");
	});


	$('#btnSearch').bind('click',function(){
		if($('#hdnBranchID').val() != "") {
			$('#alerts').html('');
			$('#alerts').hide();
			show_ajax_loader();
			$('#branchInfoContainer').load(document_root+'rpc/branch/loadBranchInfo.rpc',{serial_dea:$('#hdnBranchID').val()},
				function(){
					hide_ajax_loader();
				});
		} else {
			$('#alerts').html('<li><b>Seleccione un cliente de la lista.</b></li>');
			$('#alerts').show();
		}
	});
});


