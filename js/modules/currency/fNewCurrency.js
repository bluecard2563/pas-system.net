// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewCurrency").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		
		rules: {
			txtNameCurrency: {
				required: true,
				textOnly: true,
				maxlength: 30, 
				remote: {
					url: document_root+"rpc/remoteValidation/currency/checkNameCurrency.rpc",
					type: "post"
				}
			},
			txtExchange_fee_cur: {
				required: true,
				number: true									
			},
			txtSymbol_cur: {
				required: true,
				maxlength: 4,
				remote: {
					url: document_root+"rpc/remoteValidation/currency/checkSymbolCurrency.rpc",
					type: "post"
				}
			},
			selStatus: {
				required: true
			}
		},
		messages: {
			txtNameCurrency: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: 'Ya existe la moneda ingresada.'
			},
			txtExchange_fee_cur: {
				number: "En el campo *Cambio en relaci�n al d�lar: solo se aceptan valores num�ricos."
			},
			txtSymbol_cur: {			
				maxlength: "El s&iacute;mbolo debe tener m&aacute;ximo 2 caracteres",
				remote:"El s&iacute;mbolo ingresado ya existe"
			}
		}
	});
	/*END VALIDATION SECTION*/
});