// JavaScript Document
var pager;
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchCurrency").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameCurrency: {
				required: true
			}		
		}
	});
	/*END VALIDATION SECTION*/


	if($('#tblCurrency').length>0){
		pager = new Pager('tblCurrency', 10 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}	
});
