// JavaScript Document
$(document).ready(function(){
	$('#selZone').bind("change",loadCountry);

	/*VALIDATION SECTION*/
	$("#frmSearchCurrencyByCountry").validate({
            errorLabelContainer: "#alerts",
            wrapper: "li",
            onfocusout: false,
            onkeyup: false,
            rules: {
                selZone: {
                    required: true
                },
                selCountry: {
                    required: true
                }
            }
    });
    /*END VALIDATION SECTION*/

    /*CHANGE SECTION*/
    $('#btnDisplay').bind("click",function(){
        $('#currencyContainer').load(document_root+"rpc/loadCurrenciesByCountry.rpc",
        {serial_cou:$('#selCountry').val()});
    });
    /*END CHANGE SECTION*/
});

function loadCountry(){
	zoneID=this.value;
        $('#currencyContainer').html('');
	if(zoneID!=''){
		$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",
                            {serial_zon: zoneID, charge_country:0},
                            function(){
                                $('#selCountry').bind("change",function(){
                                    $('#currencyContainer').html('');
                                })
                            });
	}
}