// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmSearchPrize").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selType: {
				required: true
			},
                        selPrize: {
				required: true
			}
		}
	});

        $('#selZone').change(function(){loadCountries($('#selZone').val())});
        $("#selType").change(function(){
            
            if($("#selType").val()=='BONUS'){
                $("#bonusContainer").show();
                addBonusRules();
            }else{
                $("#bonusContainer").hide();
                removeBonusRules();
                loadPrizesByType($("#selType").val());
            }
        });


	/*END VALIDATION*/
        
});// JavaScript Document
function loadCountries(zoneId){
    if(zoneId==""){
            $("#selCountry option[value='']").attr("selected",true);
    }
    $('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId},function(){
        $('#selCountry').change(function(){loadPrizesByCountry($('#selCountry').val())});
		if($("#selCountry").find('option').size()==2){
				loadPrizesByCountry($('#selCountry').val());
			}
    });
}
function loadPrizesByCountry(countryId){
    if(countryId==""){
            $("#selPrize option[value='']").attr("selected",true);
    }
    $('#prizeContainer').load(document_root+"rpc/loadPrizes.rpc",{serial_cou: countryId});
}
function loadPrizesByType(type){
    if(type==""){
            $("#selPrize option[value='']").attr("selected",true);
    }
    $('#prizeContainer').load(document_root+"rpc/loadPrizes.rpc",{type_pri: type});
}
function addBonusRules(){
    $("#selZone").rules("add", {
         required: true
    });
    $("#selCountry").rules("add", {
         required: true
    });
}

function removeBonusRules(){
    $("#selZone").rules("remove");
    $("#selCountry").rules("remove");
}