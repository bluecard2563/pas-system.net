//Java Script
function changeFlag(){
	$('#hdnNamePrize').val($('#hdnNamePrize').val()+1);
}

$(document).ready(function(){
	$("#txtName").bind("change",changeFlag);
	$("#selType").bind("change",changeFlag);
        $("#selType").change(function(){
            if($("#selType").val()=='BONUS'){
                $("#bonusContainer").show();
                addBonusRules();              
            }else{
                $("#bonusContainer").hide();
                removeBonusRules();
            }
        });
        $("#chkProduct").click(function(){
            if($("#chkProduct").is(':checked')){
                $("#productContainer").show();
                $("#selProduct").rules("add", {
                     required: true
                });
            }else{
                $("#productContainer").hide();
                $("#selProduct").rules("remove");
            }
        });
        $('#selZone').change(function(){loadCountries($('#selZone').val())});
	/*SECCIÓN DE VALIDACIONES DEL FORMULARIO*/
	$("#frmNewPrize").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {
				required: true,
				textOnly: true
			},
			hdnNamePrize:{
				remote: {
					url: document_root+"rpc/remoteValidation/prize/checkName.rpc",
					type: "post",
					data: {
							type: function() {
									return $("#selType").val();
							},
							txtName: function() {
									return $("#txtName").val();
							}
					}
				}
			},
			txtValue: {
				required: true,
				number: true
			},
			txtStock: {
				required: true,
				digits: true
			},
			selType: {
				required: true
			}
		},
		messages: {
			txtName: {
       			textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
     		},
			hdnNamePrize:{
				 remote: "El nombre ingresado ya existe en el sistema para el tipo seleccionado."
			},
			txtValue: {
       			number: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Valor'."
     		},
			txtStock: {
				digits: "S&oacute;lo se aceptan d&iacute;gitos en el campo 'Stock'."
			},	
			selType: {
				remote: "Por favor seleccione un Tipo."
			}
   		}
	});
	/*FIN DE LA SECCION DE VALIDACIONES*/

        $("#selType").change (function(){
            $('#txtName').removeAttr('class');
	});
});

function loadCountries(zoneId){
	if(zoneId==""){
		$("#selCountry option[value='']").attr("selected",true);
	}
	$('#countryContainer').load(document_root+"rpc/loadCountries.rpc",{serial_zon: zoneId});
}

function addBonusRules(){   
    $("#selZone").rules("add", {
         required: true
    });
    $("#selCountry").rules("add", {
         required: true
    });
    $("#txtAmount").rules("add", {
         required: true,
         number: true,
         messages:{
            number: "El campo 'Monto' solo acepta valores n&uacute;mericos."
         }
    });
    $("#selCoverage").rules("add", {
         required: true
    });
}

function removeBonusRules(){
    $("#selZone").rules("remove");
    $("#selCountry").rules("remove");
    $("#txtAmount").rules("remove");
    $("#selCoverage").rules("remove");
}
