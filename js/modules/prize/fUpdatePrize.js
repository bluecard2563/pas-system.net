//Java Script

$(document).ready(function(){
	/*SECCI�N DE VALIDACIONES DEL FORMULARIO*/
        $("#chkProduct").click(function(){
            if($("#chkProduct").is(':checked')){
                $("#productContainer").show();
                $("#selProduct").rules("add", {
                     required: true
                });
            }else{
                $("#productContainer").hide();
                $("#selProduct").rules("remove");
            }
        });
	$("#frmUpdatePrize").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtName: {
				required: true,
				alphaNumeric: true
			},
			txtValue: {
				required: true,
				number: true
			},
			txtStock: {
				required: true,
				digits: true
			},
			selStatus: "required"
		},
		messages: {
			txtName: {
       			alphaNumeric: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'."
     		},
			txtValue: {
       			number: "S&oacute;lo se aceptan n&uacute;meros en el campo 'Valor'."
     		},
			txtStock: {
				digits: "S&oacute;lo se aceptan d&iacute;gitos en el campo 'Stock'."
			}
   		}
	});
	/*FIN DE LA SECCI�N DE VALIDACIONES*/
});