// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmUpdateZone").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameZone: {
				required: true,		
				textOnly: true,
				maxlength: 30,
				remote: {
					url: document_root+"rpc/remoteValidation/zone/checkNameZone.rpc",
					type: "post",
					data: {
						serial_zon: function() {
						return $("#hdnSerial_zon").val();
					  }
					}
				}
			}				
		},
		messages: {
			txtNameZone: {
				required: "El campo Nombre es obligatorio.",
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: 'La Zona ingresada ya existe'
			}
		}
	});
	/*END VALIDATION SECTION*/
});