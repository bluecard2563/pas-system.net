// JavaScript Document
$(document).ready(function(){				 	
	/*VALIDATION SECTION*/
	$("#frmSearchZone").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			selZone:{
				required: true
			}
		}
	});
	/*END VALIDATION SECTION*/
});