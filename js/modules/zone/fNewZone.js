// JavaScript Document
$(document).ready(function(){
	/*VALIDATION SECTION*/
	$("#frmNewZone").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtNameZone: {
				required: true,
				textOnly: true,
				maxlength: 30,
				 remote: {
					url: document_root+"rpc/remoteValidation/zone/checkNameZone.rpc",
					type: "post"
				}
			}				
			
		},
		messages: {
			txtNameZone: {
				textOnly: "S&oacute;lo se aceptan caracteres en el campo 'Nombre'.",
				maxlength: 'El nombre debe tener m&aacute;ximo 30 caracteres.',
				remote: 'La zona ingresada ya existe.'
			}
		}
	});
	/*END VALIDATION SECTION*/
});