$(document).ready(function () {
    if ($('#incentiveDeliveryTable').length > 0) {
        var currentTable = $('#incentiveDeliveryTable').dataTable({
            "bJQueryUI": true,
            "aaSorting": [[2, "asc"]],
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "oPaginate": {
                    "sFirst": "Primera",
                    "sLast": "&Uacute;ltima",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sLengthMenu": "Mostrar _MENU_ registros por P&aacute;gina",
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                "sSearch": "Filtro:",
                "sProcessing": "Filtrando.."
            },
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sSwfPath": document_root + "img/dataTables/copy_cvs_xls_pdf.swf",
                "aButtons": ""
            }
        });
    }
});

function updateDelivery(element) {
    if (element) {
        let incentiveDeliveryId = element.value;
        let delivered = element.checked ? '1' : '0';
        if (incentiveDeliveryId) {
            show_ajax_loader();
            $.ajax({
                url: document_root + "modules/incentives/pIncentiveDelivery.php",
                type: "POST",
                data: {action: 'updateDelivery', incentiveDeliveryId: incentiveDeliveryId, delivered: delivered},
                success: function (response) {
                    if (response === '1') {
                        console.log("REGISTRO ACTUALIZADO");
                        // location.reload();
                    }
                    hide_ajax_loader();
                }
            });
        }
    }
}