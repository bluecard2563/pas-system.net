// JavaScript Document
function applyPromoTypeRestrictions(type, initial_call){
	if(!initial_call){
		$('#txtDiscount').val('');
		$('#txtOtherDiscount').val('');
	}
	
	switch(type){
		case 'CURRENT_SALE':
			$('#txtDiscount').attr("disabled", false);
			$('#txtOtherDiscount').attr("disabled", true);
			$('#otherDiscountContainer').css('display','none');
			
			break;
		case 'NEXT_SALE':
			$('#txtDiscount').attr("disabled", true);
			$('#txtOtherDiscount').removeAttr("disabled");
			$('#otherDiscountContainer').css('display','inline');

			break;
		case 'BOTH':
			$('#txtDiscount').removeAttr("disabled");
			$('#txtOtherDiscount').removeAttr("disabled");
			$('#otherDiscountContainer').css('display','inline');
			
			break;
		default:
			$('#otherDiscountContainer').css('display','none');
			$('#txtDiscount').removeAttr("disabled");
			break;
	}
}

function applyPromoScopeRestrictions(){
	var selected_array = new Array();
	$('#selScope :selected').each(function(){
		array_push(selected_array, $(this).val());
	});
	var selected_scope = implode(',', selected_array);

	switch(selected_scope){
		case 'WEB':
			$('#CASH').attr('disabled',true);
			$('#CASH').attr('selected',false);
			$('#PAYPAL').removeAttr('disabled');
			$('#CREDIT_CARD').removeAttr('disabled');
			break;
		case 'SYSTEM':
			$('#CASH').removeAttr('disabled');
			$('#PAYPAL').attr('disabled', true);
			$('#PAYPAL').attr('selected',false);
			$('#CREDIT_CARD').attr('disabled', true);
			$('#CREDIT_CARD').attr('selected',false);
			break;
		case 'PHONE':
			$('#CASH').attr('disabled', true);
			$('#CASH').attr('selected',false);
			$('#PAYPAL').attr('disabled', true);
			$('#PAYPAL').attr('selected',false);
			$('#CREDIT_CARD').removeAttr('disabled');
			break;
		case 'WEB,SYSTEM':
			$('#CASH').removeAttr('disabled');
			$('#PAYPAL').removeAttr('disabled');
			$('#CREDIT_CARD').removeAttr('disabled');
			break;
		case 'SYSTEM,PHONE':
			$('#CASH').removeAttr('disabled');
			$('#PAYPAL').attr('disabled',true);
			$('#PAYPAL').attr('selected',false);
			$('#CREDIT_CARD').removeAttr('disabled');
			break;
		case 'WEB,PHONE':
			$('#CASH').attr('disabled',true);
			$('#CASH').attr('selected',false);
			$('#PAYPAL').removeAttr('disabled');
			$('#CREDIT_CARD').removeAttr('disabled');
			break;
		case 'WEB,SYSTEM,PHONE':
			$('#CASH').removeAttr('disabled');
			$('#PAYPAL').removeAttr('disabled');
			$('#CREDIT_CARD').removeAttr('disabled');
			break;
	}
}

function loadProductsByCountry(countryId){
	$('#appliedProductContainer').load(document_root+"rpc/loadClientPromo/loadAppliedProducts.rpc",
		{
			serial_cou: countryId,
			multiple: 'YES'
		});    
}

function validateDates(){
	if($("#txtBeginDate").val()!=''){
		var greater_than_today = dayDiff($("#hdnToday").val(),$("#txtBeginDate").val());
		if(greater_than_today <= 1){
			alert("La Fecha Inicio debe ser mayor a la fecha actual");
			$("#txtBeginDate").val('');
		}
	}


    if($("#txtBeginDate").val()!='' && $("#txtEndDate").val()!=''){
		var days = dayDiff($("#txtBeginDate").val(),$("#txtEndDate").val());
		if(days <= 0){ //if departure date is smaller than arrival date
			alert("La Fecha Fin no puede ser menor a la Fecha Inicio");
			$("#txtEndDate").val('');
		}
	}
}

/*
 * TRICKS THE VALIDATOR TO EXECUTE THE REMOTE RPC IF ANY OF THE PARAMS HAS CHANGED
 * VALUE OF hdnRemote IS NOT IMPORTANT AS LONG AS IT CHANGES.
 */
function remoteValidationFunction(){
	var begin_date = $('#txtBeginDate').val();
	var end_date = $('#txtEndDate').val();
	var promo_name = $('#txtName').val();

	if(begin_date != '' || end_date != '' || promo_name != ''){
		$('#hdnRemote').val($('#hdnRemote').val() + '1');
	}else{
		$('#hdnRemote').val('');
	}
}

function loadCustomerPromos(serial_cou, status_ccp){
	$('#customerPromo_container').load(document_root+'rpc/loadClientPromo/loadPromosForUpdate.rpc',
		{
			serial_cou: serial_cou,
			status_ccp: status_ccp
		}, function(){
			 if($('#promoTable').length>0){
				pager = new Pager('promoTable', 10 , 'Siguiente', 'Anterior');
				pager.init($('#hdnTotalPages').val()); //Max Pages
				pager.showPageNav('pager', 'alertPageNavPosition'); //Div where the navigation buttons will be placed.
				pager.showPage(1); //Starting page
			 }
	});
}
