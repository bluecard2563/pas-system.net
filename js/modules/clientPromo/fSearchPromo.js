// JavaScript Document
var pager;
$(document).ready(function(){
	$('#btnSearch').bind('click', function(){
		var country = $('#selCountry').val();
		var status = $('#selStatus').val();

		if(country != '' && status != ''){
			loadCustomerPromos(country, status);
		}else{
			alert('Debe escoger los campos \'Pa\u00EDs\' y \'Estado\' antes de continuar.');
		}
	});

	$('#selCountry').bind('change', function(){
		$('#customerPromo_container').html('');
	});

	$('#selStatus').bind('change', function(){
		$('#customerPromo_container').html('');
	});
});