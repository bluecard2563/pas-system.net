// JavaScript Document
$(document).ready(function(){
    /*VALIDATION SECTION*/
    $("#frmCustomerPromo").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtName: {
                required: true,
				alphaNumeric: true				
            },
			hdnRemote: {
				remote: {
					url: document_root+"rpc/remoteValidation/clientPromo/checkName.rpc",
					type: "post",
					data: {
						txtName: function(){
							return $('#txtName').val();
						},
						begin_date: function(){
							return $('#txtBeginDate').val();
						},
						end_date: function(){
							return $('#txtEndDate').val();
						},
						serial_ccp: function(){
							return $('#hdnSerial_ccp').val();
						}
					}
				}
			},
			txtCode: {
                required: true,
				alphaNumeric: true,
				maxlength: 3,
				minlength: 3,
				remote: {
					url: document_root+"rpc/remoteValidation/clientPromo/checkCode.rpc",
					type: "post",
					data: {
						serial_ccp: function(){
							return $('#hdnSerial_ccp').val();
						}
					}
				}
            },
			selCountry: {
                required: true
            },
			selProduct: {
				required: true
			},
			txtBeginDate: {
				required: true
			},
			txtEndDate: {
				required: true
			},
			selScope: {
				required: true
			},
			selPaymentForm: {
				required: true
			},
			selType: {
				required: true
			},
			txtDiscount: {
				required: function(element) {
					if($('#selType').val == '' || $('#selType').val() != 'NEXT_SALE'){
						return true;
					}else{
						return false;
					}
				},
				number: true,
				max: 100
			},
			txtOtherDiscount: {
				required: function(element) {
					if($('#selType').val == '' || $('#selType').val() != 'CURRENT_SALE'){
						return true;
					}else{
						return false;
					}
				},
				number: true,
				max: 100,
				greaterThanNumber: 0
			},
			txtDescription: {
				required: true,
				alphaNumeric: true
			},
			selAppliedProduct: {
				required: true
			},
			selStatus: {
				required: true
			}
        },
		messages: {
			txtName: {
				alphaNumeric: "El campo 'Nombre' acepta caracteres alfa-num&eacute;ricos."
            },
			hdnRemote: {
				remote: "El nombre especificado ya existe para otra promoci&oacute;n en curso."
			},
			txtCode: {
				maxlength: "El campo 'C&oacute;digo' debe ser de 3 caracteres alfa-num&eacute;ricos",
				minlength: "El campo 'C&oacute;digo' debe ser de 3 caracteres alfa-num&eacute;ricos",
				alphaNumeric: "El campo 'C&oacute;digo' acepta hasta 4 caracteres alfa-num&eacute;ricos",
				remote: "El c&oacute;digo especificado ya fue usado anteriormente."
            },
			txtDiscount: {
				number: "El campo 'Descuento' admite s&oacute;lo n&uacute;meros.",
				max: "El valor m&aacute;ximo del campo 'Descuento' es 100."
			},
			txtOtherDiscount: {
				number: "El campo 'Pr&oacute;ximo Descuento' admite s&oacute;lo n&uacute;meros.",
				max: "El valor m&aacute;ximo del campo 'Pr&oacute;ximo Descuento' es 100.",
				greaterThanNumber: "El valor del campo 'Pr&oacute;ximo Descuento' debe ser mayor a 0."
			},
			txtDescription: {
				alphaNumeric: "El campo 'Descripci&oacute;n' acepta caracteres alfa-num&eacute;ricos."
			}
		},
		submitHandler: function(form){
			$('#btnSubmit').val('Expere por favor...');
			$('#btnSubmit').attr('disabled', true);

			form.submit();
		}
    });
    /*END VALIDATION SECTION*/

    /*CHANGE OPTIONS*/
    $("#selCountry").bind("change", function(e){
        loadProductsByCountry(this.value)
    });

	$('#selType').bind('change',function(){
		applyPromoTypeRestrictions($(this).val(), false);
	});

	$('#selScope').bind('click',function(){
		applyPromoScopeRestrictions();
	});

	$('#txtBeginDate').bind('change', function(){
		validateDates();
		remoteValidationFunction();
	});

	$('#txtEndDate').bind('change', function(){
		validateDates();
		remoteValidationFunction();
	});

	$('#txtName').bind('keyup', function(){
		remoteValidationFunction();
	});
    /*END CHANGE*/

	if($('#hdnCurrentStatus').val() == 'REGISTERED'){
		setDefaultCalendar($("#txtBeginDate"),'+1d','+1y');
		setDefaultCalendar($("#txtEndDate"),'+1d','+1y');
	}

	applyPromoScopeRestrictions();
	applyPromoTypeRestrictions($('#selType').val(), true);
});