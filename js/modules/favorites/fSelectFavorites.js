$(document).ready(function(){	
	$( "#sortable1" ).sortable({
		items: "li:not(.ui-state-disabled)"
	});
	
	$( "#sortable2" ).sortable({
		items: "li:not(.ui-state-disabled)"
	});
	
	
	$( "#sortable1" ).sortable("option", "connectWith", '#sortable2');
	$( "#sortable2" ).sortable("option", "connectWith", '#sortable1');
	
	$("#btnSubmit").bind('click', function(){
		var items = $("#sortable2").sortable("toArray");
		var selected = $("#sortable2").sortable("serialize");
		
		if(selected != ''){
			if(count(items) <= 15){
				$('#txtSelected').html(selected);
				$('#frmFavorites').submit();
			}else{
				alert('Puede elegirse solo 15 favoritos al mismo tiempo.');
			}
		}else{
			alert('Seleccione al menos un favorito.');
		}
	});
});


