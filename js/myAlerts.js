var pager;
$(document).ready(function(){
	 if($('#alertsTable').length>0){
		pager = new Pager('alertsTable', 10 , 'Siguiente', 'Anterior');
											pager.init(7); //Max Pages
											pager.showPageNav('pager', 'alertPageNavPosition'); //Div where the navigation buttons will be placed.
											pager.showPage(1); //Starting page
	 }
	 
	 $("#freeSalesDetailsDialog").dialog({
			bgiframe: false,
			autoOpen: false,
			height: 500,
			width: 700,
			modal: true,
			buttons: {
				'Cerrar': function() {
					$(this).dialog('close');
				}
			  }
		});
	 
	 $(".[id^='alertDetails_']").each(function(e){
		 $(this).click(function(e){
			 alertId = $(this).attr('alert_id');
			 $.ajax({
			        url: document_root+"getFreeSaleObsAjax",
			        type: "post",
			        data: ({alertId : alertId}),
			        success: function(dialogContentData) {
				 		$('#dialogContentContainer').html(dialogContentData);
				 		$('#freeSalesDetailsDialog').dialog('open');
			 		}
			 });
		 });
	 });
});

