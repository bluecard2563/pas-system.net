//Java Script
$(document).ready(function(){
	//Delete adendum
	if($('#printContract').length>0){
		$('#printContract').bind('click',function(){
			$('#printContract').html('');
			$.ajax({
				url:document_root+"deleteFile",
				type: "post",
				data:({url: "/modules/sales/contracts/contract_" + $('#printContract').attr("serial_sal")+".pdf"}),
				 success: function(data) {
                                if(data=='false'){
                                    alert("No puede vender una tarjeta con ese n\u00famero");
                                 }
                            }
			});
		});
	}
    //Delete Contract
    if($('#printNewContract').length>0){
		$('#printNewContract').bind('click',function(){
			$('#printNewContract').html('');
			$.ajax({
				url:document_root+"deleteFile",
				type: "post",
				data:({url: "/modules/sales/contracts/nwcontract_" + $('#printNewContract').attr("serial_sal")+".pdf"}),
				 success: function(data) {
                                if(data=='false'){
                                    alert("No puede vender una tarjeta con ese n\u00famero");
                                 }
                            }
			});
		});
	}

    //Message confirm send mail
        $('#paymentPtoPMail').bind('click',function(){
            setTimeout ("alert('El correo electrónico ha sido enviado al cliente');", 3000);
        });



    $('#term').click(function(){
        // $("#customPopUp2").show();
		var customPopUp2=$('#customPopUp2').val();
        $('<div id="dialog">'+customPopUp2+'</div>').appendTo('body');
        event.preventDefault();

        if($('#dialog').size() > 0){
            $("#dialog").dialog({
                modal: true,
                draggable: false,
                maxWidth: 820,
                minWidth: 820,
                width: 820,
                maxHeight: 555,
                minHeight: 555,
                height: 555,
                title: $('#popTitle').val(),
                close: function(event, ui) {
                    $("#dialog").remove();
                }
            });
        }
    });


    $('#pre').click(function(){
        // $("#customPopUp2").show();
        var customPopUp3=$('#customPopUp3').val();
        $('<div id="dialog3">'+customPopUp3+'</div>').appendTo('body');
        event.preventDefault();

        if($('#dialog3').size() > 0){
            $("#dialog3").dialog({
                modal: true,
                draggable: false,
                maxWidth: 820,
                minWidth: 820,
                width: 820,
                maxHeight: 555,
                minHeight: 555,
                height: 555,
                title: $('#popTitle').val(),
                close: function(event, ui) {
                    $("#dialog3").remove();
                }
            });
        }
    });


    var checker = document.getElementById('checkme');
    var btnPtPS = document.getElementById('btnPtPS');
    var btnPtPM = document.getElementById('btnPtPM');

    if (checker){
        checker.onchange = function() {
            btnPtPS.disabled = !this.checked;
            btnPtPM.disabled = !this.checked;
        };
    }


	if($('#printRegister').length>0){
		$('#printRegister').bind('click',function(){
			$('#printRegister').html('');
		});
	}


	//customPopUp Is the Popup created by Cintya
	if($('#customPopUp').size() > 0){
		$("#customPopUp").dialog({
			modal: true,
			draggable: false,
			maxWidth: 820,
			minWidth: 820,
			width: 820,
			maxHeight: 555,
			minHeight: 555,
			height: 555,
			title: $('#popTitle').val()
		}).addClass('');
	}

    var buttons = {};
        
    if ($('#popWebLink').val() && $('#pdfFile').val()) {

        buttons = {
            'Salir': function() {
                    $(this).dialog('close');
                },
            'Mas Informacion': function() {
                    window.open($('#pdfFile').val(), '_blank');
                },
            'Sitio Web' : function(){
					window.open($('#popWebLink').val(), '_blank');
				}
        };
    } else if ($('#popWebLink').val()) {

        buttons = {
            'Salir': function() {
                    $(this).dialog('close');
                },
            'Sitio Web' : function(){
					window.open($('#popWebLink').val(), '_blank');
				}
        };
    } else if ($('#pdfFile').val()) {

        buttons = {
            'Salir': function() {
                    $(this).dialog('close');
                },
            'Mas Informacion': function() {
                    window.open($('#pdfFile').val(), '_blank');
                }
        };
    }

    $( "#customPopUp" ).dialog( "option", "buttons", buttons);
    $( "#customPopUp2" ).dialog( "option", "buttons", buttons);
    $( "#customPopUp3" ).dialog( "option", "buttons", buttons);

});

//Download Zone
function openFilesPopup(optionId) {
    //event.preventDefault();
    //if($('#digitalFilesPopUp').size() > 0){
    $("#digitalFilesPopUp" + optionId).dialog({
        modal: true,
        draggable: false,
        closeOnEscape: true,
        resizable: true,
        maxWidth: 580,
        minWidth: 580,
        width: 580,
        maxHeight: 350,
        minHeight: 350,
        height: 350,
        title: 'Archivos Disponibles',
        close: function(event, ui) {
            $(this).dialog("destroy");
        }
    });
    //}
}

