// JavaScript Document
$(document).ready(function(){
	$("#frmLogin").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtSystemUser: {
				required: true
			},
			txtSystemPassword: {
				required: true
			},
			selLanguage: {
				required: true
			}
		}
	});
	
	$("#btnSubmit").bind("click", function(e){
		$('#messages').css('display','none');
	$	("#frmLogin").submit();
	});

	$('#txtSystemUser').bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			$('#messages').css('display','none');
			$("#frmLogin").submit();
		}
	});

	$('.text-background').bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			$('#messages').css('display','none');
			$("#frmLogin").submit();
		}
	});
	
	$("#dialogChangePass").dialog({
		bgiframe: true,
		autoOpen: false,
		width: 600,
		height: 250,
		modal: true,
		buttons: {
			'Cancelar': function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  }
	});
	
	$("#goChangePass").bind("click", function(e){
		$("#goChangePass").css( 'display', 'none' );
		$("#goChangeStdBy").css( 'display', 'block' );
		var email = $("#cpMail").val();
		$.ajax({
			url: document_root+ 'pForgotPasswordAjax.php',
			type: "POST",
			data: { 'email': email},
			success: function(success){
				if(success == 'ok'){
					alert('Se ha enviado un email a '+email+' con su usuario y contrase\xF1a.\n Por favor si usted no recibe un correo pronto, revise su carpeta de correo no deseado (junk) o asegurese que el correo electronico ingresado haya sido el correcto.');

					$("#dialogChangePass").dialog('close');
				}else{
					$("#registerText").css( 'display', 'block' );
				}
				$("#goChangePass").css( 'display', 'block' );
				$("#goChangeStdBy").css( 'display', 'none' );
			}
		});
	});
	
	$("#lostPass").click(lostPass);
});

function lostPass(){
	show_ajax_loader();
	$("#cpMail").val('');
	$("#registerText").css( 'display', 'none');
	$("#dialogChangePass").dialog('open');
	hide_ajax_loader();
}