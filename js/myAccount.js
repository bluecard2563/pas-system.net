// JavaScript Document
$(document).ready(function(){
    $("#frmMyAccount").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            txtOldPassword: {
                required: {
                    depends: function(element) {
                        var display = $("#passwordContainer").css('display');
                        if(display=='none')
                            return false;
                        else
                            return true;
                    }
                },
                minlength: {
                    param:6
                },
                remote: {
                    url: document_root+"rpc/remoteValidation/checkOldPassword.rpc",
                    type: "POST",
                    data: {
                        password_usr: function() {
                            return $("#txtOldPassword").val();
                        }
                    }
                }
            },
            txtPassword: {
                required: {
                    depends: function(element) {
                        var display = $("#passwordContainer").css('display');
                        if(display=='none')
                            return false;
                        else
                            return true;
                    }
                },
                minlength: {
                    param:6
                }
            },
            txtRePassword: {
                required: {
                    depends: function(element) {
                        var display = $("#passwordContainer").css('display');
                        if(display=='none')
                            return false;
                        else
                            return true;
                    }
                },
                equalTo:  {
                    param:"#txtPassword"
                }
            },
            txtFirstName: {
                required: true,
                textOnly: true
            },
            txtLastName: {
                required: true,
                textOnly: true
            },
            txtAddress: {
                required: true
            },
            txtPhone: {
                required: true,
                digits: true
            },
            txtCellphone: {
                digits: true
            },
            txtMail: {
                required: true,
                email: true,
                remote: {
                    url: document_root+"rpc/remoteValidation/user/checkUserEmail.rpc",
                    type: "POST",
                    data: {
                        serial_usr: function() {
                            return $("#serial_usr").val();
                        }
                    }
                }
            }
        },
        messages: {
            txtOldPassword: {
                remote: "La contrase&ntilde;a anterior es incorrecta.",
                minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
            },
            txtPassword: {
                minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
            },
            txtRePassword: {
                equalTo: "La confirmaci&oacute;n ha fallado."
            },
            txtFirstName: {
                textOnly: "El campo 'Nombre' admite s&oacute;lo texto."
            },
            txtLastName: {
                textOnly: "El campo 'Apellido' admite s&oacute;lo texto."
            },
            txtPhone: {
                digits: "El campo 'Tel&eacute;fono' admite s&oacute;lo d&iacute;gitos."
            },
            txtCellphone: {
                digits: "El campo 'Celular' admite s&oacute;lo d&iacute;gitos."
            },
            txtMail: {
                email: "El campo 'E-mail' debe tener el formato nombre@dominio.com",
                remote: "El 'E-mail' ingresado ya existe en el sistema."
            }
        }
    });

    $('#passwordContainer').css('display','none');
    $('#qrContainer').css('display', 'none');

    $('[name="rdPassword"]').each(function(){
        $(this).bind('click', function(){
            change_password($(this).val());
        });
    });
    
    $('#displayQR').bind('click', function(){
        $('#qrContainer').toggle('slow');
    });
});

function change_password(type){
    if(type=='CHANGE'){
        $('#passwordContainer').show();
    }else{
        $('#passwordContainer').hide();
        $('#txtOldPassword').val('');
        $('#txtPassword').val('');
        $('#txtRePassword').val('');
    }
}