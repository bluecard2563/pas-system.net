//var document_root='/';
var peasant_tax = 1.005;
var underAge = 17;

/*Santi 04-Jan-2019: Automatic site url detected.
build automatic url from javascript and server current configurations
*/
var pathArray = window.location.pathname.split('/');
var hostname = window.location.hostname;
var protocol = window.location.protocol;
//var document_root = protocol + "//" + hostname + "/" + pathArray[1] + "/";
var document_root = protocol + "//" + hostname + "/";
console.log(document_root);

// Santi 26-May-2020: Global string variables for health statement system origin.
var newSaleStatementOrigin = 'BAS_NEW_SALE';
var travelRecordStatementOrigin = 'BAS_TRAVEL_RECORD';

// Santi 04-Jun-2020: Global statement base url to open the health statement survey statementBaseUrl is used in globalSalesFunctions.js and fRegisterTravel.js
var statementBaseUrlParameter = '?data=';
// for production
var statementBaseUrl = 'http://23.239.12.79:80/statement' + statementBaseUrlParameter;