// JavaScript Document
$.fn.dataTableExt.oApi.fnGetHiddenNodes = function ( oSettings )
{
	/* Note the use of a DataTables 'private' function thought the 'oApi' object */
	var anNodes = this.oApi._fnGetTrNodes( oSettings );
	var anDisplay = $('tbody tr', oSettings.nTable);
	
	/* Remove nodes which are being displayed */
	for ( var i=0 ; i<anDisplay.length ; i++ )
	{
		var iIndex = jQuery.inArray( anDisplay[i], anNodes );
		if ( iIndex != -1 )
		{
			anNodes.splice( iIndex, 1 );
		}
	}
	
	/* Fire back the array to the caller */
	return anNodes;
}
$.fn.dataTableExt.oApi.fnGetFilteredNodes = function ( oSettings )
{
	var anRows = [];
	for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
	{
		var nRow = oSettings.aoData[ oSettings.aiDisplay[i] ].nTr;
		anRows.push( nRow );
	}
	return anRows;
}

/*
 * Function: fnGetDisplayNodes
 * Purpose:  Return an array with the TR nodes used for displaying the table
 * Returns:  array node: TR elements
 *           or
 *           node (if iRow specified)
 * Inputs:   object:oSettings - automatically added by DataTables
 *           int:iRow - optional - if present then the array returned will be the node for
 *             the row with the index 'iRow'
 */
$.fn.dataTableExt.oApi.fnGetDisplayNodes = function ( oSettings, iRow )
{
    var anRows = [];
    if ( oSettings.aiDisplay.length !== 0 )
    {
        if ( typeof iRow != 'undefined' )
        {
            return oSettings.aoData[ oSettings.aiDisplay[iRow] ].nTr;
        }
        else
        {
            for ( var j=oSettings._iDisplayStart ; j<oSettings._iDisplayEnd ; j++ )
            {
                var nRow = oSettings.aoData[ oSettings.aiDisplay[j] ].nTr;
                anRows.push( nRow );
            }
        }
    }
    return anRows;
};

$.fn.dataTableExt.oApi.fnDisplayRow = function ( oSettings, nRow )
{
    var iPos = -1;
    for( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
    {
        if( oSettings.aoData[ oSettings.aiDisplay[i] ].nTr == nRow )
        {
            iPos = i;
            break;
        }
    }
     
    if( iPos >= 0 )
    {
        oSettings._iDisplayStart = ( Math.floor(i / oSettings._iDisplayLength) ) * oSettings._iDisplayLength;
        this.oApi._fnCalculateEnd( oSettings );
    }
     
    this.oApi._fnDraw( oSettings );
}
