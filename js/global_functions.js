// JavaScript Document
$(document).ready(function(){
	evaluateAlerts();
	setInterval("evaluateAlerts()", 120000);

	//Check if the JS is active
	$("#JSWarning").css('display','none');
});

function explode (delimiter, string, limit) {
    // Splits a string on string separator and return array of components. If limit is positive only limit number of components is returned. If limit is negative all components except the last abs(limit) are returned.
    //
    // version: 909.322
    // discuss at: http://phpjs.org/functions/explode    // +     original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: kenneth
    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: d3x
    // +     bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)    // *     example 1: explode(' ', 'Kevin van Zonneveld');
    // *     returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}
    // *     example 2: explode('=', 'a=bc=d', 2);
    // *     returns 2: ['a', 'bc=d']
     var emptyArray = {0: ''};

    // third argument is not required
    if ( arguments.length < 2 ||
        typeof arguments[0] == 'undefined' ||        typeof arguments[1] == 'undefined' )
    {
        return null;
    }
     if ( delimiter === '' ||
        delimiter === false ||
        delimiter === null )
    {
        return false;}

    if ( typeof delimiter == 'function' ||
        typeof delimiter == 'object' ||
        typeof string == 'function' ||        typeof string == 'object' )
    {
        return emptyArray;
    }
     if ( delimiter === true ) {
        delimiter = '1';
    }

    if (!limit) {return string.toString().split(delimiter.toString());
    } else {
        // support for limit argument
        var splitted = string.toString().split(delimiter.toString());
        var partA = splitted.splice(0, limit - 1);var partB = splitted.join(delimiter.toString());
        partA.push(partB);
        return partA;
    }
}
function implode (glue, pieces) {
    // Joins array elements placing glue string between items and return one string
    //
    // version: 911.718
    // discuss at: http://phpjs.org/functions/implode    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Waldo Malqui Silva
    // +   improved by: Itsacon (http://www.itsacon.net/)
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);    // *     returns 1: 'Kevin van Zonneveld'
    // *     example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
    // *     returns 2: 'Kevin van Zonneveld'

    var i = '', retVal='', tGlue='';

    if (arguments.length === 1) {
        pieces = glue;
        glue = '';
    }
    if (typeof(pieces) === 'object') {
        if (pieces instanceof Array) {
            return pieces.join(glue);
        }
        else {
            for (i in pieces) {
                retVal += tGlue + pieces[i];
                tGlue = glue;
            }
            return retVal;
        }
    } else {
        return pieces;
    }
}

/***********************************************
* @Name: array_push
* @Description: Adds an element to an specific array at the end of it.
* @Params: arrayGroup: array stack
*          element: the new element to be added
* @Returns: The new Array
***********************************************/
function array_push ( arrayGroup, element ) {
    var size=arrayGroup.length;
    arrayGroup[size]=element;
    return arrayGroup;
}

/***********************************************
* @Name: setDefaultCalendar
* @Description: sets a datepicker for an specific date textBox, it also calls
*               formatDate() function that is used to control the insertion
*               of a correct date in the textBox.
* @Params:  object: textBox that is going to aply the datepicker and formatDate()
*           min: the min date that is going to accept the calendar
*           max: the max date that is going to accept the calendar
*           defDate: the default date used to initialize the calendar
***********************************************/
function setDefaultCalendar(object,min,max,defDate){
    object.datepicker({
        showOn: 'button',
        buttonImage: document_root+'img/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
		date: object.val(),
		current: object.val(),
		starts: 1,
		position: 'r',
		defaultDate: defDate,
		maxDate: max,
		minDate: min,
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+20',
		onBeforeShow: function(){
				object.DatePickerSetDate(object.val(), true);
		},
		onChange: function(formated, dates){
				object.val(formated);
		}
    });

    object.change(function(){$(this).val(formatDate($(this).val()))});
}

/***********************************************
* @Name: setDefaultCalendar
* @Description: checks if a value is numeric
* @Params:  value
* @Returns: false if it isn't numeric
*           true if it is numeric
***********************************************/
function IsNumeric(value){
    var log=value.length;
    var sw="S";
    for (x=0; x<log; x++) {
        v1=value.substr(x,1);
        v2 = parseInt(v1);
        if (isNaN(v2)) {
            sw= "N";
        }
    }
    if (sw=="S") {
        return true;
    } else {
        return false;
    }
}

/***********************************************
* @Name: setDefaultCalendar
* @Description: function used to control the insertion
*               of a correct date in the textBox in an specific format 'dd/mm/yyyy'.
* @Params:  date: String going to be checked and modified if necesary
* @Returns: date: The modified String
***********************************************/
function formatDate(date){
    var firstlap=false;
    var secondlap=false;
    var date_lng = date.length;
    var day;
    var month;
    var year;
    if ((date_lng>=2) && (firstlap==false)) {
        day=date.substr(0,2);
        if ((IsNumeric(day)==true) && (day<=31) && (day!="00")) {
            date=date.substr(0,2)+"/"+date.substr(3,7);firstlap=true;
        }
        else {
            date="";firstlap=false;
        }
    }else{
        day=date.substr(0,1);
        if (!IsNumeric(day)){
            date="";
        }
        if ((date_lng<=2) && (firstlap=true)) {
            date=date.substr(0,1);firstlap=false;
        }
    }
    if ((date_lng>=5) && (secondlap==false)){
        month=date.substr(3,2);
        if ((IsNumeric(month)==true) &&(month<=12) && (month!="00")) {
            date=date.substr(0,5)+"/"+date.substr(6,4);secondlap=true;
        }else {
            date=date.substr(0,3);;secondlap=false;
        }
    }
    else {
        if ((date_lng<=5) && (secondlap=true)) {
            date=date.substr(0,4);secondlap=false;
        }
    }
    if (date_lng>=7){
        year=date.substr(6,4);
        if (IsNumeric(year)==false) {
            date=date.substr(0,6);
        }
        else {
            if (date_lng==10){
                if ((year==0) || (year<1900) || (year>2100)) {
                    date=date.substr(0,6);
                }
            }
        }
    }
    if (date_lng>=10){
        date=date.substr(0,10);
        day=date.substr(0,2);
        month=date.substr(3,2);
        year=date.substr(6,4);
        if ( (year%4 != 0) && (month ==02) && (day > 28) ) {
            date=date.substr(0,2)+"/";
        }
        if ((month ==02) && (day > 29) ) {
            date=date.substr(0,2)+"/";
        }
    }
    return (date);
}

/***********************************************
	* function parseDate
	@Name: parseDate
	@Description: Parses a string in format date dd/mm/YYYY to a date
	@Params:  str:string to be parsed
	@Returns: Date object.
			  used mostly on dayDiff function
***********************************************/
function parseDate(str) {
    var mdy = String(str).split('/')
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}
/***********************************************
	* function dayDiff
	@Name: dayDiff
	@Description: get the days between to dates
	@Params:
         *       first:lower date string format dd/mm/YYYY
         *       second: higher date string format dd/mm/YYYY
	@Returns: int: days between
	***********************************************/
function dayDiff(first, second) {
    var date1 = parseDate(first);
    var date2 = parseDate(second);

    return parseInt(((date2-date1)/(1000*60*60*24))+1);
}

/***********************************************
@Name: dateFormat
@Description: Sets date format from mm/dd/YYYY to dd/mm/YYYY
@Params: Date String
@Returns: Formated Date
***********************************************/
function dateFormat(date){
	var formatDate=date.charAt(3)+'';
	for(var i=4;i<6;i++){
		formatDate+=date.charAt(i);
	}
	for(var i=0;i<3;i++){
		formatDate+=date.charAt(i);
	}
	for(var i=6;i<date.length;i++){
		formatDate+=date.charAt(i);
	}
	return formatDate;
}

function strtolower (str) {
    // Makes a string lowercase
    //
    // version: 909.322
    // discuss at: http://phpjs.org/functions/strtolower    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Onno Marsman
    // *     example 1: strtolower('Kevin van Zonneveld');
    // *     returns 1: 'kevin van zonneveld'
    return (str+'').toLowerCase();
}

function ucwords(str) {
    // Uppercase the first character of every word in a string
    //
    // version: 1001.2911
    // discuss at: http://phpjs.org/functions/ucwords    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Waldo Malqui Silva
    // +   bugfixed by: Onno Marsman
    // +   improved by: Robin
    // *     example 1: ucwords('kevin van zonneveld');    // *     returns 1: 'Kevin Van Zonneveld'
    // *     example 2: ucwords('HELLO WORLD');
    // *     returns 2: 'HELLO WORLD'
    return (str + '').replace(/^(.)|\s(.)/g, function ($1) {
        return $1.toUpperCase();});
}

function evaluateAlerts(){
		$.ajax({
				url: document_root+"rpc/alerts/checkUserAlerts.rpc",
				type: "post",
				data: ({}),
				success: function(data) {
					if(data=='false'){
						$('#user_alerts_container').css('display','none');
					}else{
						text ='('+data+')';
						if(data>1)
							text+=' Alertas';
						else
							text+=' Alerta';
						$('#user_alerts_number').html(text);
						$('#user_alerts_container').css('display','block');
					}

				}
		});
}

function calcDaysBetween($startDate, $endDate,changeSource){
	$.ajax({
		url: document_root+ 'modules/estimator/getDaysDiffAjax.php',
		type: "POST",
		async: false,
		data: {'beginDate': $startDate,'endDate': $endDate},
		success: function(days){
			if(days<0){
				alert('La fecha de regreso debe ser mayor que la de salida');
				$('#daysDiff').html('0');
				if(changeSource == 1){
					$('#txtBeginDate').val('');
				}else{
					$('#txtEndDate').val('');
				}
			}else{
				$('#daysDiff').html(days);
			}
		}
	});
}

function saveSessionVariable($name, $value){
	$.ajax({
		url: document_root+ 'saveSessionVariableAjax.php',
		type: "POST",
		async: false,
		data: {'name': $name,'value': $value},
		success: function(okString){
			return okString;
		}
	});
}

function estimatorMasterRedirect(){
	$.ajax({
		url: document_root+ 'modules/estimator/registered/pCheckCustomerLoggedAndCartEmptyAjax.php',
		type: "POST",
		success: function(information){
			information = jQuery.trim(information);
			parts = information.split("%S%");
			if(parts[1] == '1'){
				pageNum = 3;
			}else{
				pageNum = '';
			}
			if(parts[0] == '1'){
				window.location = document_root + 'modules/estimator/registered/estimatorCustomer/0/'+pageNum;
			}else{
				window.location = document_root + 'modules/estimator/estimator/'+pageNum;
			}
		}
	});
}

function serialize (mixed_value) {
    // Returns a string representation of variable (which can later be unserialized)
    //
    // version: 1004.2314
    // discuss at: http://phpjs.org/functions/serialize    // +   original by: Arpad Ray (mailto:arpad@php.net)
    // +   improved by: Dino
    // +   bugfixed by: Andrej Pavlovic
    // +   bugfixed by: Garagoth
    // +      input by: DtTvB (http://dt.in.th/2008-09-16.string-length-in-bytes.html)    // +   bugfixed by: Russell Walker (http://www.nbill.co.uk/)
    // +   bugfixed by: Jamie Beck (http://www.terabit.ca/)
    // +      input by: Martin (http://www.erlenwiese.de/)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // -    depends on: utf8_encode    // %          note: We feel the main purpose of this function should be to ease the transport of data between php & js
    // %          note: Aiming for PHP-compatibility, we have to translate objects to arrays
    // *     example 1: serialize(['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: 'a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}'
    // *     example 2: serialize({firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'});    // *     returns 2: 'a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}'
    var _getType = function (inp) {
        var type = typeof inp, match;
        var key;
        if (type == 'object' && !inp) {return 'null';
        }
        if (type == "object") {
            if (!inp.constructor) {
                return 'object';}
            var cons = inp.constructor.toString();
            match = cons.match(/(\w+)\(/);
            if (match) {
                cons = match[1].toLowerCase();}
            var types = ["boolean", "number", "string", "array"];
            for (key in types) {
                if (cons == types[key]) {
                    type = types[key];break;
                }
            }
        }
        return type;};
    var type = _getType(mixed_value);
    var val, ktype = '';

    switch (type) {case "function":
            val = "";
            break;
        case "boolean":
            val = "b:" + (mixed_value ? "1" : "0");break;
        case "number":
            val = (Math.round(mixed_value) == mixed_value ? "i" : "d") + ":" + mixed_value;
            break;
        case "string":mixed_value = this.utf8_encode(mixed_value);
            val = "s:" + encodeURIComponent(mixed_value).replace(/%../g, 'x').length + ":\"" + mixed_value + "\"";
            break;
        case "array":
        case "object":val = "a";
            /*
            if (type == "object") {
                var objname = mixed_value.constructor.toString().match(/(\w+)\(\)/);
                if (objname == undefined) {                    return;
                }
                objname[1] = this.serialize(objname[1]);
                val = "O" + objname[1].substring(1, objname[1].length - 1);
            }            */
            var count = 0;
            var vals = "";
            var okey;
            var key;for (key in mixed_value) {
                ktype = _getType(mixed_value[key]);
                if (ktype == "function") {
                    continue;
                }
                okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
                vals += this.serialize(okey) +
                        this.serialize(mixed_value[key]);
                count++;}
            val += ":" + count + ":{" + vals + "}";
            break;
        case "undefined": // Fall-through
        default: // if the JS object has a property which contains a null value, the string cannot be unserialized by PHP            val = "N";
            break;
    }
    if (type != "object" && type != "array") {
        val += ";";}
    return val;
}

function utf8_encode ( argString ) {
    // Encodes an ISO-8859-1 string to UTF-8
    //
    // version: 1004.2314
    // discuss at: http://phpjs.org/functions/utf8_encode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: sowberry
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman    // +   improved by: Yves Sucaet
    // +   bugfixed by: Onno Marsman
    // +   bugfixed by: Ulrich
    // *     example 1: utf8_encode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'    var string = (argString+''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");

    var utftext = "";
    var start, end;
    var stringl = 0;
    start = end = 0;
    stringl = argString.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = argString.charCodeAt(n);var enc = null;

        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
        } else {
            enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
        }
        if (enc !== null) {if (end > start) {
                utftext += argString.substring(start, end);
            }
            utftext += enc;
            start = end = n+1;}
    }

    if (end > start) {
        utftext += argString.substring(start, argString.length);}

    return utftext;
}

function show_ajax_loader(){
	$("#loader").css( 'display', 'block' );
}

function hide_ajax_loader(){
	$("#loader").css( 'display', 'none' );
}

/*
 * MOD10 'LUTH' ALGORITHM JAVASCRIPT VALIDATION FUNCTION
 * TAKEN AND MODIFIED FROM THE FOLLOWING URL:
 * https://www.internetsecure.com/merchants/Mod10.html
 */
function Mod10(ccNumb) { //v2.0
	var valid = "0123456789"
	var len = ccNumb.length;
	var bNum = true;
	var iCCN = ccNumb;
	var sCCN = ccNumb.toString();
	var iCCN;
	var iTotal = 0;
	var bResult = false;
	var digit;
	var temp;
	
	iCCN = sCCN.replace (/^\s+|\s+$/g,'');	// strip spaces
	for (var j=0; j<len; j++) {
		temp = "" + iCCN.substring(j, j+1);
	
	if (valid.indexOf(temp) == "-1") bNum = false;}
	if(!bNum){alert("Not a Number");}
	   
	iCCN = parseInt(iCCN);
		
	if(len == 0){ /* nothing, field is blank */ 
		bResult = true;
	}else{
		if(len >= 15){		//15 or 16 for Amex or V/MC
			for(var i=len;i>0;i--){
				digit = "digit" + i;
				calc = parseInt(iCCN) % 10;	//right most digit
				calc = parseInt(calc);
				iTotal += calc;		
				//parseInt(cardnum.charAt(count))i:\t" + calc.toString() + " x 2 = " + (calc *2) +" : " + calc2 + "\n";
				i--;
				digit = "digit" + i;
				iCCN = iCCN / 10; 	// subtracts right most digit from ccNum
				calc = parseInt(iCCN) % 10 ;	// step 1 double every other digit
				calc2 = calc *2;
				
				switch(calc2){
					case 10:calc2 = 1;break;	//5*2=10 & 1+0 = 1
					case 12:calc2 = 3;break;	//6*2=12 & 1+2 = 3
					case 14:calc2 = 5;break;	//7*2=14 & 1+4 = 5
					case 16:calc2 = 7;break;	//8*2=16 & 1+6 = 7
					case 18:calc2 = 9;break;	//9*2=18 & 1+8 = 9
					default:calc2 = calc2; 		//4*2= 8 &   8 = 8  -same for all lower numbers
				}
				iCCN = iCCN / 10; 	// subtracts right most digit from ccNum
				iTotal += calc2;
			}
			if ((iTotal%10)==0){
				//document.calculator.results.value = "Yes"; 
				bResult = true;
	 		}else{
				//document.calculator.results.value = "No"; 
				bResult = false;
			}
		}
	}
	return bResult;
}

/*
 * @description: Returns a number formatated with n-decimal numbers
 * @params: number to be formatated
 * @params: decimals to be displayed.
 */
function roundNumber(number, decimals) {
	var result = Math.round(number*Math.pow(10,decimals))/Math.pow(10,decimals);
	return result;
}

function loadProductGNCFiles(prodId){
	redirectMe = false;
	redirectTo = '';
	$.ajax({
		url: document_root+ 'loadProductGNCFiles.php',
		type: "POST",
		data: {'product_id': prodId},
		async: false,
		success: function(messageContent){
			information = jQuery.trim(messageContent);
			parts = information.split("%%%");
			if(parts[0] == '1'){
				redirectMe = true;
				redirectTo = parts[1];
			}else if(parts[0] > '1'){
				$("#generalConditionsListed").html(parts[1]);
				$("#dialogGeneralConditions").dialog('open');
				hide_ajax_loader();
			}else{
				alert('Archivo no encontrado');
			}
		}
	});
	if(redirectMe){
		window.open(redirectTo, 'open_window');
	}
}

function json_encode(mixed_val) {
    // Returns the JSON representation of a value  
    // 
    // version: 1009.2513
    // discuss at: http://phpjs.org/functions/json_encode
    // +      original by: Public Domain (http://www.json.org/json2.js)
    // + reimplemented by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      improved by: Michael White
    // +      input by: felix
    // +      bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *        example 1: json_encode(['e', {pluribus: 'unum'}]);
    // *        returns 1: '[\n    "e",\n    {\n    "pluribus": "unum"\n}\n]'
    /*
        http://www.JSON.org/json2.js
        2008-11-19
        Public Domain.
        NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
        See http://www.JSON.org/js.html
    */
    var retVal, json = this.window.JSON;
    try {
        if (typeof json === 'object' && typeof json.stringify === 'function') {
            retVal = json.stringify(mixed_val); // Errors will not be caught here if our own equivalent to resource
                                                                            //  (an instance of PHPJS_Resource) is used
            if (retVal === undefined) {
                throw new SyntaxError('json_encode');
            }
            return retVal;
        }
 
        var value = mixed_val;
 
        var quote = function (string) {
            var escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
            var meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            };
 
            escapable.lastIndex = 0;
            return escapable.test(string) ?
                            '"' + string.replace(escapable, function (a) {
                                var c = meta[a];
                                return typeof c === 'string' ? c :
                                '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                            }) + '"' :
                            '"' + string + '"';
        };
 
        var str = function (key, holder) {
            var gap = '';
            var indent = '    ';
            var i = 0;          // The loop counter.
            var k = '';          // The member key.
            var v = '';          // The member value.
            var length = 0;
            var mind = gap;
            var partial = [];
            var value = holder[key];
 
            // If the value has a toJSON method, call it to obtain a replacement value.
            if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }
 
            // What happens next depends on the value's type.
            switch (typeof value) {
                case 'string':
                    return quote(value);
 
                case 'number':
                    // JSON numbers must be finite. Encode non-finite numbers as null.
                    return isFinite(value) ? String(value) : 'null';
 
                case 'boolean':
                case 'null':
                    // If the value is a boolean or null, convert it to a string. Note:
                    // typeof null does not produce 'null'. The case is included here in
                    // the remote chance that this gets fixed someday.
 
                    return String(value);
 
                case 'object':
                    // If the type is 'object', we might be dealing with an object or an array or
                    // null.
                    // Due to a specification blunder in ECMAScript, typeof null is 'object',
                    // so watch out for that case.
                    if (!value) {
                        return 'null';
                    }
                    if ((this.PHPJS_Resource && value instanceof this.PHPJS_Resource) ||
                        (window.PHPJS_Resource && value instanceof window.PHPJS_Resource)) {
                        throw new SyntaxError('json_encode');
                    }
 
                    // Make an array to hold the partial results of stringifying this object value.
                    gap += indent;
                    partial = [];
 
                    // Is the value an array?
                    if (Object.prototype.toString.apply(value) === '[object Array]') {
                        // The value is an array. Stringify every element. Use null as a placeholder
                        // for non-JSON values.
 
                        length = value.length;
                        for (i = 0; i < length; i += 1) {
                            partial[i] = str(i, value) || 'null';
                        }
 
                        // Join all of the elements together, separated with commas, and wrap them in
                        // brackets.
                        v = partial.length === 0 ? '[]' :
                                gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                                '[' + partial.join(',') + ']';
                        gap = mind;
                        return v;
                    }
 
                    // Iterate through all of the keys in the object.
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
 
                    // Join all of the member texts together, separated with commas,
                    // and wrap them in braces.
                    v = partial.length === 0 ? '{}' :
                            gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                    gap = mind;
                    return v;
                case 'undefined': // Fall-through
                case 'function': // Fall-through
                default:
                    throw new SyntaxError('json_encode');
            }
        };
 
        // Make a fake root object containing our value under the key of ''.
        // Return the result of stringifying the value.
        return str('', {
            '': value
        });
    
    } catch(err) { // Todo: ensure error handling above throws a SyntaxError in all cases where it could
                            // (i.e., when the JSON global is not available and there is an error)
        if (!(err instanceof SyntaxError)) {
            throw new Error('Unexpected error type in json_encode()');
        }
        this.php_js = this.php_js || {};
        this.php_js.last_error_json = 4; // usable by json_last_error()
        return null;
    }
}

function json_decode (str_json) {
	// http://kevin.vanzonneveld.net
	// +      original by: Public Domain (http://www.json.org/json2.js)
	// + reimplemented by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +      improved by: T.J. Leahy
	// +      improved by: Michael White
	// *        example 1: json_decode('[\n    "e",\n    {\n    "pluribus": "unum"\n}\n]');
	// *        returns 1: ['e', {pluribus: 'unum'}]
	/*
    http://www.JSON.org/json2.js
    2008-11-19
    Public Domain.
    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
    See http://www.JSON.org/js.html
  */

	var json = this.window.JSON;
	if (typeof json === 'object' && typeof json.parse === 'function') {
		try {
			return json.parse(str_json);
		} catch (err) {
			if (!(err instanceof SyntaxError)) {
				throw new Error('Unexpected error type in json_decode()');
			}
			this.php_js = this.php_js || {};
			this.php_js.last_error_json = 4; // usable by json_last_error()
			return null;
		}
	}

	var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
	var j;
	var text = str_json;

	// Parsing happens in four stages. In the first stage, we replace certain
	// Unicode characters with escape sequences. JavaScript handles many characters
	// incorrectly, either silently deleting them, or treating them as line endings.
	cx.lastIndex = 0;
	if (cx.test(text)) {
		text = text.replace(cx, function (a) {
			return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
		});
	}

	// In the second stage, we run the text against regular expressions that look
	// for non-JSON patterns. We are especially concerned with '()' and 'new'
	// because they can cause invocation, and '=' because it can cause mutation.
	// But just to be safe, we want to reject all unexpected forms.
	// We split the second stage into 4 regexp operations in order to work around
	// crippling inefficiencies in IE's and Safari's regexp engines. First we
	// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
	// replace all simple value tokens with ']' characters. Third, we delete all
	// open brackets that follow a colon or comma or that begin the text. Finally,
	// we look to see that the remaining characters are only whitespace or ']' or
	// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
	if ((/^[\],:{}\s]*$/).
		test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
			replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
			replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

		// In the third stage we use the eval function to compile the text into a
		// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
		// in JavaScript: it can begin a block or an object literal. We wrap the text
		// in parens to eliminate the ambiguity.
		j = eval('(' + text + ')');

		return j;
	}

	this.php_js = this.php_js || {};
	this.php_js.last_error_json = 4; // usable by json_last_error()
	return null;
}

//USED ON VALIDATIONS FOR EXTRAS ON SALES AND PHONE SALES
function checkAgeValidationsSales(valueId, valueComparator, type){
	checkThis = valueId;
	if(checkThis != valueComparator){
		return true;
	}
	isOld = true;
	isOld = checkAgeValidationsSub($('#txtBirthdayCustomer').val(), type);
	if(!isOld && valueComparator != 'ADULTS' && valueComparator != 'CHILDREN'){
		return false;
	}else{
		isOld = true;
	}
	$("[id^='birthdayExtra_']").each( function(e){
		myVal = $(this).val();
		isOld = checkAgeValidationsSub(myVal, type);
		if(!isOld){ 
			return false;
		}
	});
	return isOld;
}

function checkAgeValidationsSub(myVal, type){
	if(myVal == ''){
		return true;
	}
	isOld = true;
	$.ajax({
		  url: document_root+ checkProductValidatorURL,
		  type: "POST",
		  data: {'birthDate': myVal, type: type},
		  async: false,
		  success: function(response){
			  if(response == 'true'){
				  isOld = true;
			  }else{
				  isOld = false;
			  }
		  }
	});
	return isOld;
}

function str_replace (search, replace, subject, count) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Gabriel Paderni
    // +   improved by: Philip Peterson
    // +   improved by: Simon Willison (http://simonwillison.net)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   bugfixed by: Anton Ongson
    // +      input by: Onno Marsman
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +    tweaked by: Onno Marsman
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   input by: Oleg Eremeev
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Oleg Eremeev
    // %          note 1: The count parameter must be passed as a string in order
    // %          note 1:  to find a global variable in which the result will be given
    // *     example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
    // *     returns 1: 'Kevin.van.Zonneveld'
    // *     example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
    // *     returns 2: 'hemmo, mars'

    var i = 0, j = 0, temp = '', repl = '', sl = 0, fl = 0,
            f = [].concat(search),
            r = [].concat(replace),
            s = subject,
            ra = r instanceof Array, sa = s instanceof Array;
    s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }

    for (i=0, sl=s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j=0, fl=f.length; j < fl; j++) {
            temp = s[i]+'';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length-s[i].length)/f[j].length;}
        }
    }
    return sa ? s : s[0];
}

function specialchars(text){
	if(substr(text, 0, 2) == "a:"){
			return addslashes(text);
	}else{
			text = str_replace("<", "&lt;", text);
			text = str_replace(">", "&gt;", text);
			text = str_replace("'", "&#039;", text);
			text = str_replace("\"", "&quot;", text);
			text = str_replace("\\\\", "\\", text); // to catch any double encoding
			text = str_replace("&amp;", "&", text); // this will reverse any double html encoding issues we have had

			return addslashes(text); // this verifies that we have a safe query
	}
}

function addslashes (str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +   improved by: marrtins
    // +   improved by: Nate
    // +   improved by: Onno Marsman
    // +   input by: Denny Wardhana
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
    // *     example 1: addslashes("kevin's birthday");
    // *     returns 1: 'kevin\'s birthday'

    return (str+'').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function substr (str, start, len) {
    // Returns part of a string
    //
    // version: 909.322
    // discuss at: http://phpjs.org/functions/substr
    // +     original by: Martijn Wieringa
    // +     bugfixed by: T.Wild
    // +      tweaked by: Onno Marsman
    // +      revised by: Theriault
    // +      improved by: Brett Zamir (http://brett-zamir.me)
    // %    note 1: Handles rare Unicode characters if 'unicode.semantics' ini (PHP6) is set to 'on'
    // *       example 1: substr('abcdef', 0, -1);
    // *       returns 1: 'abcde'
    // *       example 2: substr(2, 0, -6);
    // *       returns 2: false
    // *       example 3: ini_set('unicode.semantics',  'on');
    // *       example 3: substr('a\uD801\uDC00', 0, -1);
    // *       returns 3: 'a'
    // *       example 4: ini_set('unicode.semantics',  'on');
    // *       example 4: substr('a\uD801\uDC00', 0, 2);
    // *       returns 4: 'a\uD801\uDC00'
    // *       example 5: ini_set('unicode.semantics',  'on');
    // *       example 5: substr('a\uD801\uDC00', -1, 1);
    // *       returns 5: '\uD801\uDC00'
    // *       example 6: ini_set('unicode.semantics',  'on');
    // *       example 6: substr('a\uD801\uDC00z\uD801\uDC00', -3, 2);
    // *       returns 6: '\uD801\uDC00z'
    // *       example 7: ini_set('unicode.semantics',  'on');
    // *       example 7: substr('a\uD801\uDC00z\uD801\uDC00', -3, -1)
    // *       returns 7: '\uD801\uDC00z'

// Add: (?) Use unicode.runtime_encoding (e.g., with string wrapped in "binary" or "Binary" class) to
// allow access of binary (see file_get_contents()) by: charCodeAt(x) & 0xFF (see https://developer.mozilla.org/En/Using_XMLHttpRequest ) or require conversion first?

    var i = 0, allBMP = true, es = 0, el = 0, se = 0, ret = '';
    str += '';
    var end = str.length;

    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    switch(
        (this.php_js.ini['unicode.semantics'] &&
            this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
        case 'on': // Full-blown Unicode including non-Basic-Multilingual-Plane characters
            // strlen()
            for (i=0; i < str.length; i++) {
                if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i+1))) {
                    allBMP = false;
                    break;
                }
            }

            if (!allBMP) {
                if (start < 0) {
                    for (i = end - 1, es = (start += end); i >= es; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i-1))) {
                            start--;
                            es--;
                        }
                    }
                }
                else {
                    var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
                    while ((surrogatePairs.exec(str)) != null) {
                        var li = surrogatePairs.lastIndex;
                        if (li - 2 < start) {
                            start++;
                        }
                        else {
                            break;
                        }
                    }
                }

                if (start >= end || start < 0) {
                    return false;
                }
                if (len < 0) {
                    for (i = end - 1, el = (end += len); i >= el; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i-1))) {
                            end--;
                            el--;
                        }
                    }
                    if (start > end) {
                        return false;
                    }
                    return str.slice(start, end);
                }
                else {
                    se = start + len;
                    for (i = start; i < se; i++) {
                        ret += str.charAt(i);
                        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i+1))) {
                            se++; // Go one further, since one of the "characters" is part of a surrogate pair
                        }
                    }
                    return ret;
                }
                break;
            }
            // Fall-through
        case 'off': // assumes there are no non-BMP characters;
                           //    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
        default:
            if (start < 0) {
                start += end;
            }
            end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
            // PHP returns false if start does not fall within the string.
            // PHP returns false if the calculated end comes before the calculated start.
            // PHP returns an empty string if start and end are the same.
            // Otherwise, PHP returns the portion of the string from start to end.
            return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
    }
    return undefined; // Please Netbeans
}

function show_html_loader(element_selector){
	var loader_element = "<div class='span-24 last line center'> \n\
								<div class='span-24 last line subtitle line'>CARGANDO</div>\n\
								<img src='"+document_root+"img/loading_bar.gif' />\n\
						</div>";
	
	$(element_selector).html(loader_element);
}

function count (mixed_var, mode) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Waldo Malqui Silva
    // +   bugfixed by: Soren Hansen
    // +      input by: merabi
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Olivier Louvignes (http://mg-crea.com/)
    // *     example 1: count([[0,0],[0,-4]], 'COUNT_RECURSIVE');
    // *     returns 1: 6
    // *     example 2: count({'one' : [1,2,3,4,5]}, 'COUNT_RECURSIVE');
    // *     returns 2: 6
    var key, cnt = 0;

    if (mixed_var === null || typeof mixed_var === 'undefined') {
        return 0;
    } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
        return 1;
    }

    if (mode === 'COUNT_RECURSIVE') {
        mode = 1;
    }
    if (mode != 1) {
        mode = 0;
    }

    for (key in mixed_var) {
        if (mixed_var.hasOwnProperty(key)) {
            cnt++;
            if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
                cnt += this.count(mixed_var[key], 1);
            }
        }
    }

    return cnt;
}

function onlyNumbersKeyPress(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function onlyDecimalKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

//thanks: http://javascript.nwbox.com/cursor_position/
//This works with the onlyDecimalKeyPress function to validate only 2 decimals.
function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}