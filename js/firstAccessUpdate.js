// JavaScript Document
$(document).ready(function(){
	var today = $('#today').val();
	setDefaultCalendar($("#txtBirthdate"),'-100y','+0d','-18');
	
	$("#frmFirstAccess").validate({
		errorLabelContainer: "#alerts",
		wrapper: "li",
		onfocusout: false,
		onkeyup: false,
		rules: {
			txtOldPassword: {
				required: true,
                minlength: {
					param:6
				},
				remote: {
					url: document_root+"rpc/remoteValidation/checkOldPassword.rpc",
					type: "POST",
					data: {
					  password_usr: function() {
						return $("#txtOldPassword").val();
					  }
					}
				}
			},
			txtPassword: {
				required: true,
				minlength: {
					param:6
				}
			},
			txtRePassword: {
				required: true,
				equalTo:  {
					param:"#txtPassword"
				}
			},
			txtFirstName: {
				required: true,
				textOnly: true
			},
			txtLastName: {
				required: true,
				textOnly: true
			},
			txtAddress: {
				required: true,
				alphaNumeric: true
			},
			txtPhone: {
				required: true,
				digits: true
			},
			txtCellphone: {
				digits: true
			},
			txtMail: {
				required: true,
				email: true,
				remote: {
					url: document_root+"rpc/remoteValidation/user/checkUserEmail.rpc",
					type: "POST",
					data: {
					  serial_usr: function() {
						return $("#serial_usr").val();
					  }
					}
				}
			},
			txtBirthdate:{
				required: true,
				date: true,
				dateLessThan: today
			}
		},
		messages: {
			txtOldPassword: {
				remote: "La contrase&ntilde;a anterior es incorrecta.",
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtPassword: {
				minlength: "La contrase&ntilde;a debe tener m&iacute;nimo 6 caracteres."
			},
			txtRePassword: {
				equalTo: "La confirmaci&oacute;n de contrase&ntilde;a ha fallado."
			},
			txtFirstName: {
				textOnly: "El campo 'Nombre' admite s&oacute;lo texto."
			},
			txtLastName: {
				textOnly: "El campo 'Apellido' admite s&oacute;lo texto."
			},
			txtAddress: {
				alphaNumeric: "El campo 'Direcci&oacute;n' admite s&oacute;lo letras y n&uacute;meros."
			},
			txtPhone: {
				digits: "El campo 'Tel&eacute;fono' admite s&oacute;lo d&iacute;gitos."
			},
			txtCellphone: {
				digits: "El campo 'Celular' admite s&oacute;lo d&iacute;gitos."
			},
			txtMail: {
				email: "El campo 'E-mail' debe tener el formato nombre@dominio.com",
				remote: "El 'E-mail' ingresado ya existe en el sistema."
			},
			txtBirthdate:{
				date: "El campo 'Fecha de Nacimiento' debe tener el formato dd/mm/YYYY.",
				dateLessThan: "La 'Fecha de Nacimiento' debe ser menor o igual a la fecha actual."
			}
		}
	});
});