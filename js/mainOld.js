//Java Script
$(document).ready(function(){
	//Delete adendum
	if($('#printContract').length>0){
		$('#printContract').bind('click',function(){
			$('#printContract').html('');
			$.ajax({
				url:document_root+"deleteFile",
				type: "post",
				data:({url: "/modules/sales/contracts/contract_" + $('#printContract').attr("serial_sal")+".pdf"}),
				 success: function(data) {
                                if(data=='false'){
                                    alert("No puede vender una tarjeta con ese n\u00famero");
                                 }
                            }
			});
		});
	}
    //Delete Contract
    if($('#printNewContract').length>0){
		$('#printNewContract').bind('click',function(){
			$('#printNewContract').html('');
			$.ajax({
				url:document_root+"deleteFile",
				type: "post",
				data:({url: "/modules/sales/contracts/nwcontract_" + $('#printNewContract').attr("serial_sal")+".pdf"}),
				 success: function(data) {
                                if(data=='false'){
                                    alert("No puede vender una tarjeta con ese n\u00famero");
                                 }
                            }
			});
		});
	}
    
	if($('#printRegister').length>0){
		$('#printRegister').bind('click',function(){
			$('#printRegister').html('');
		});
	}
    
	
	if($('#customPopUp').size() > 0){
		$("#customPopUp").dialog({
			modal: true,
			draggable: false,
			maxWidth: 820,
			minWidth: 820,
			width: 820,
			maxHeight: 555,
			minHeight: 555,
			height: 555,
			title: $('#popTitle').val()
		});
	}
    
    
    var buttons = {};
        
    if ($('#popWebLink').val() && $('#pdfFile').val()) {
        buttons = { 
            'Salir': function() {
                    $(this).dialog('close');
                },
            'Mas Informacion': function() {
                    window.open($('#pdfFile').val(), '_blank');
                },
            'Sitio Web' : function(){
					window.open($('#popWebLink').val(), '_blank');
				}
        };
    } else if ($('#popWebLink').val()) {
        buttons = { 
            'Salir': function() {
                    $(this).dialog('close');
                },
            'Sitio Web' : function(){
					window.open($('#popWebLink').val(), '_blank');
				}
        };
    } else if ($('#pdfFile').val()) {
        buttons = { 
            'Salir': function() {
                    $(this).dialog('close');
                },
            'Mas Informacion': function() {
                    window.open($('#pdfFile').val(), '_blank');
                }
        };
    }

    $( "#customPopUp" ).dialog( "option", "buttons", buttons);
    
});
