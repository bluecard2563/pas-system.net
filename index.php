<?php 
if(isset($_SESSION['serial_usr'])){
	/*REGISTER LOGOUT*/
	if($_SESSION['serial_lgn'])
		$closedSession = LoginControl::registerLogout($db, $_SESSION['serial_lgn']);
	/*END REGISTER LOGOUT*/
    unset($_SESSION['serial_usr']);
    unset($_SESSION['serial_pbc']);
    unset($_SESSION['user_name']);
    unset($_SESSION['cityList']);
    unset($_SESSION['countryList']);
	unset($_SESSION['user_type']);
	unset($_SESSION['sellFree']);
	unset($_SESSION['cityForReports']);
	unset($_SESSION['serial_mbc']);
	unset($_SESSION['serial_dea']);
	unset($_SESSION['dea_serial_dea']);
	unset($_SESSION['suitable_pages']);
	unset($_SESSION['serial_customer_estimator']);
	unset($_SESSION['system_estimator_shopping_cart_products']);
	unset($_SESSION['firstAccess']);
	unset($_SESSION['serial_cup']);
	unset($_SESSION['current_sale_products']);
	unset($_SESSION['current_sale_total']);
	unset($_SESSION['productPricesTable']);
	unset($_SESSION['cart_products_travelers_information']);
	unset($_SESSION['name_customer_estimator']);
	unset($_SESSION['card_number']);
	unset($_SESSION['serial_lgn']);
}

//KILL ALL WEBSITE VARS TO AVOID CONFLICTS
unset($_SESSION['email_customer_web']);
unset($_SESSION['document_customer_web']);
unset($_SESSION['serial_customer_web']);
unset($_SESSION['name_customer_web']);
unset($_SESSION['web_customer_redirect']);

Request::setInteger('0:error');
global $systemToken;

$_SESSION['serial_lang'] = Language::getSerialLangByCode($db, $_SESSION['language']);;
if($_POST['txtSystemUser']){
	//Security check
	if(!$_POST['systemToken'] || $_POST['systemToken'] != $systemToken){
		http_redirect('index/9');
	}
	
	if(isset($_POST['autoLogin']) && $_POST['autoLogin'] == '1'){
		$_POST['txtSystemPassword'] = md5(mysql_real_escape_string($_POST['txtSystemPassword']));
	}
	
	//HOST NAME CHECK:
	$host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
	$host = str_replace("www.", "", $host);

	$allowedReferers = array();
	//array_push($allowedReferers, 'localhost'); //TODO REMOVE ON AIR!!!
	//array_push($allowedReferers, '192.168.0.154'); //TODO REMOVE ON AIR!!!
	array_push($allowedReferers, 'pas-system.net');
	array_push($allowedReferers, 'planet-assist.net');
	
	if(!in_array($host, $allowedReferers)){
		//http_redirect('index/9');
	}
	
	$user=new User($db);
	$user -> setUsername_usr(mysql_real_escape_string($_POST['txtSystemUser']));
	if($user->getDataLogin()){
		require_once('pLoginValidationProcess.php');
	}else{
		$error = 2;
	}
}

$smarty->register('error,systemToken');
$smarty->assign('container','index');
$smarty->display('index.'.$_SESSION['language'].'.tpl');
?>
