<?php
/**
 * File: promo_BlueCar3
 * Author: Patricio Astudillo
 * Creation Date: 21-jul-2013
 * Last Modified: 21-jul-2013
 * Modified By: Patricio Astudillo
 */
	
	//************** CODE TO GET COUNTER WINNER LIST BY TERM ************************
	$begin_date = '01/09/2015';
	$end_date = '31/12/2015';
	$excluded_dealers = '4312,4062,4205,2336,292,4050,953,2438,1917,17,223';
	$base_amount = 3000;
	$second_coupon = 1000;
	$winners_array= array();
	$net_sales = false;
	$total_coupons_needed = 0;
	/* POR COUNTER
	$sql = "SELECT cnt.serial_cnt, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'counter', 
					b.name_dea, cit.name_cit, SUM(s.total_sal) AS 'total_sold', discounts,
					applied_taxes_inv, u.document_usr, b.code_dea
			FROM sales s
			JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.status_cnt = 'ACTIVE'
			JOIN dealer b ON b.serial_dea = cnt.serial_dea AND b.serial_dea NOT IN ($excluded_dealers)
			JOIN sector sec ON sec.serial_sec = b.serial_sec
			JOIN city cit ON cit.serial_cit = sec.serial_cit
			JOIN user u ON u.serial_usr = cnt.serial_usr
			JOIN view_promoBC3 vp ON vp.serial_inv = s.serial_inv AND vp.in_credit_days = 'YES'
			WHERE STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
			AND STR_TO_DATE(DATE_FORMAT(vp.date_inv, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
			GROUP BY cnt.serial_cnt
			HAVING total_sold >= $base_amount";*/
			
	//pOR COMERCIALIZADOR
	$sql = "SELECT b.serial_dea, 
					b.name_dea, cit.name_cit, SUM(s.total_sal) AS 'total_sold', discounts,
					applied_taxes_inv, u.document_usr, b.code_dea
			FROM sales s
			JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.status_cnt = 'ACTIVE'
			JOIN dealer b ON b.serial_dea = cnt.serial_dea AND b.serial_dea NOT IN (4312,4062,4205,2336,292,4050,953,2438,1917,17,223) AND b.status_dea ='ACTIVE'
			JOIN sector sec ON sec.serial_sec = b.serial_sec
			JOIN city cit ON cit.serial_cit = sec.serial_cit
			JOIN user u ON u.serial_usr = cnt.serial_usr
			JOIN view_promoBC3 vp ON vp.serial_inv = s.serial_inv AND vp.in_credit_days = 'YES'
			WHERE STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('01/09/2015', '%d/%m/%Y') AND STR_TO_DATE('31/12/2015', '%d/%m/%Y')
			AND STR_TO_DATE(DATE_FORMAT(vp.date_inv, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('01/09/2015', '%d/%m/%Y') AND STR_TO_DATE('31/12/2015', '%d/%m/%Y')
			GROUP BY b.serial_dea
			HAVING total_sold >= 3000";
	//Debug::print_r($sql); die;
	$result = $db ->getAll($sql);
	
	if($result){
		foreach($result as $counter){
			$temp = array();
			$temp['serial_cnt'] = $counter['serial_cnt'];
			$temp['counter'] = $counter['counter'];
			$temp['document_usr'] = $counter['document_usr'];
			$temp['code_dea'] = $counter['code_dea'];
			$temp['name_dea'] = $counter['name_dea'];
			$temp['name_cit'] = $counter['name_cit'];
			$temp['total_sold'] = $counter['total_sold'];
			
			if($net_sales):
				//******** DISCOUNTS ********
				$discounts = $temp['total_sold'] * ($counter['discounts'] / 100);
				$temp['total_sold'] -= $discounts;
				
				//******** TAXES ********
				$taxes = unserialize($counter['applied_taxes_inv']);
				if($taxes){
					$taxesPercentage = 0;
					foreach($taxes as $tx){
						$taxesPercentage += $tx['tax_percentage'];
					}

					$taxes_amount = round($temp['total_sold'] * ($taxesPercentage / 100), 2);
					$temp['total_sold'] += $taxes_amount;
				}
			endif;
			
			$temp['coupons'] = 0;
			$temp['coupons'] = ($temp['total_sold'] / $base_amount);
			
			if($temp['coupons'] >= 1){
				$temp['coupons'] = 1;
				
				$excessAmount = $counter['total_sold'] - $base_amount;
				$temp['coupons'] += (int)($excessAmount / $second_coupon);
				
				$total_coupons_needed += $temp['coupons'];
				array_push($winners_array, $temp);
			}
			
			
		}
		
		//Debug::print_r($winners_array);
		
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=BlueCarCumpleTuSuenios_Counters.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		echo '<h2>LISTADO DE GANADORES</h2>';
		echo "<h4>$begin_date - $end_date</h4>";
		echo '<b>Fecha del Listado:</b> '.date('d/m/Y').'';
		
		echo '<h4>Total de Cupones: '.$total_coupons_needed.'</h4>';

		echo '<table>
				<tr>
					<td><b>Counter</b></td>
					<td><b>No. de Documento</b></td>
					<td><b>Cod. Comercializador</b></td>
					<td><b>Comercializador</b></td>
					<td><b>Ciudad</b></td>
					<td><b>Total de Ventas</b></td>
					<td><b>No. Cupones Premio</b></td>
				</tr>';
		
		foreach($winners_array as $w){
			$line = "<tr>
						<td>{$w['counter']}</td>
						<td>'{$w['document_usr']}</td>
						<td>{$w['code_dea']}</td>
						<td>{$w['name_dea']}</td>
						<td>{$w['name_cit']}</td>
						<td>{$w['total_sold']}</td>
						<td>{$w['coupons']}</td>
					</tr>";
						
			echo $line;
		}
		
		echo '</table>';
		
	}else{
		die('NO WINNERS');
	}
?>
