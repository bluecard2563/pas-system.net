<?php

//Debug::print_r($_GET['referencia']);die();
$cod=$_GET['referencia'];
$cod=base64_decode($cod);
//Debug::print_r($cod);die();
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "http://52.87.250.190/placeToPay/public/api/getPaymentRecords/".$cod,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Postman-Token: b41586c2-4c62-4f16-8d48-2e17d354d2e6"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);


if ($err) {
    echo "cURL Error #:" . $err;
} else {
    // echo $response.",";
    // $responseEdit = json_decode($response,true);
//        foreach ($responseEdit as $rs){
    //Create data for authentication in place to pay

    //@param $secretKey is trankey provided by place to pay
    $secretKey = TRANKEY;
    //@param $login is provided by place to pay
    $login = LOGIN;
    $seed = date('c');
    if (function_exists('random_bytes')) {
        $nonce = bin2hex(random_bytes(16));
    } elseif (function_exists('openssl_random_pseudo_bytes')) {
        $nonce = bin2hex(openssl_random_pseudo_bytes(16));
    } else {
        $nonce = mt_rand();
    }
    $nonceBase64 = base64_encode($nonce);
    $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

    $jsonAuth['auth'] = [
        "login" => $login,
        "seed" => $seed,
        "nonce" => $nonceBase64,
        "tranKey" => $tranKey
    ];

    $editedJson = json_encode($jsonAuth);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://secure.placetopay.ec/redirection/api/session/" . $response,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $editedJson,
        CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 7dcbb9bf-a0de-4099-8d32-48cd4d91c9b0"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $responseEdit = json_decode($response,true);
   $reference= $responseEdit['request']['payment']['reference'];
    $status= $responseEdit['status']['status'];



}


$smarty->register('reference,status');
$smarty->display();
?>