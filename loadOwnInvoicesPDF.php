<?php
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_PORT => "8082",
    CURLOPT_URL => "http://52.22.250.24:8082/report/1391722907001/01/" . $_GET['number'] . "/PDF",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Authorization: Basic UHJvZENsaWVudFJlc3Q6VGFuZCFDMHJw",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Length: 0",
        "Host: 52.22.250.24:8082",
        "Postman-Token: 00bcbed0-3814-4cbb-9394-4e9d7d3c07af,012393a1-4ff2-4dc4-b3c9-5d0d97fef27a",
        "User-Agent: PostmanRuntime/7.20.1",
        "cache-control: no-cache"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
}
else {
    $obj = json_decode($response);
    header('Location:' . $obj->link);
    echo $obj->link;
}