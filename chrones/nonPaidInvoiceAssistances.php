<?php
/*
File: nonPaidInvoiceAssistances
Author: David Bergmann
Creation Date: 09/06/2010
Last Modified: 14/06/2010
Modified By: David Bergmann
*/

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

$fileList = File::nonPaidInvoiceAssistances($db);

if(is_array($fileList)) {
	foreach ($fileList as $fl) {
		$dealer = Dealer::getDealerByCardNumber($db,$fl['card_number']);

		$temporary_customer = new TemporaryCustomer($db);
		$temporary_customer->setSerial_cou($fl['serial_cou']);
		$temporary_customer->setSerial_fle($fl['serial_fle']);
		$temporary_customer->setCou_serial_cou($fl['cou_serial_cou']);
		$temporary_customer->setSerial_usr('4');
		$temporary_customer->setDocument_tcu($fl['document_cus']);
		$temporary_customer->setFirst_name_tcu($fl['first_name_cus']);
		$temporary_customer->setLast_name_tcu($fl['last_name_cus']);
		$temporary_customer->setBirthdate_tcu($fl['birthdate_cus']);
		$temporary_customer->setAgency_name_tcu('PAS');
		$temporary_customer->setPhone_tcu($fl['phone1_cus']);
		$temporary_customer->setContact_phone_tcu($fl['cellphone_cus']);
		$temporary_customer->setContact_address_tcu($fl['address_cus']);
		$temporary_customer->setCard_number_tcu($fl['card_number']);
		$temporary_customer->setStatus_tcu('REQUESTED');

		$user = new User($db,'4');
		$user->getData();

		$extras = "Tarjeta no pagada.<br />Requested by: ".$user->getFirstname_usr()." ".$user->getLastname_usr()."<br />Date:".date("d/m/Y H:i:s")."<br />";
		$temporary_customer->setExtra_tcu($extras);
		$problem = "Tarjeta no pagada.<br />Requested by: ".$user->getFirstname_usr()." ".$user->getLastname_usr()."<br />Date:".date("d/m/Y H:i:s")."<br />";
		$temporary_customer->setProblem_description_tcu($problem);

		/*E-MAIL INFO*/
		$misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s o ingrese a <a href='".URL."' target='blank'>PAS</a>.";
		$country = new Country($db, $temporary_customer->getSerial_cou());
		$country->getData();
		$misc['country'] = $country->getName_cou();
		$misc['agency']=$temporary_customer->getAgency_name_tcu();
		$country = new Country($db, $temporary_customer->getCou_serial_cou());
		$country->getData();
		$misc['travelCountry']=$country->getName_cou();
		$misc['document']=$temporary_customer->getDocument_tcu();
		$misc['name']=$temporary_customer->getFirst_name_tcu()." ".$temporary_customer->getLast_name_tcu();
		$misc['birthDate']=$temporary_customer->getBirthdate_tcu();
		$misc['phone']=$temporary_customer->getPhone_tcu();
		$misc['contactAddress']=$temporary_customer->getContact_address_tcu();
		$misc['contactPhone']=$temporary_customer->getContact_phone_tcu();
		$misc['cardNumber']=$temporary_customer->getCard_number_tcu();
		$misc['extra']=$temporary_customer->getExtra_tcu();
		$misc['problem']=$temporary_customer->getProblem_description_tcu();
		$misc['user'] = 'AUTOMATIC PAS REQUEST - CRONJOB';

		$sale = new Sales($db,$fl['serial_sal']);
		if($sale->getData()) {
			$misc['textForEmail']="Informaci&oacute;n de la tarjeta";
			$misc['cardInfo']=true;
			$misc['emission']=$sale->getEmissionDate_sal();
			$misc['begin']=$sale->getBeginDate_sal();
			$misc['end']=$sale->getEndDate_sal();
			$misc['status']=$sale->getStatus_sal();
			$counter = new Counter($db, $sale->getSerial_cnt());
			$counter->getData();
			$userByDealer = new UserByDealer($db);
			$userByDealer->setSerial_dea($counter->getSerial_dea());
			$userByDealer->getData();
			$user = new User($db,$userByDealer->getSerial_usr());
			$user->getData();
			$misc['email'] = array($user->getEmail_usr());
		} else {
			$misc['textForEmail']="Error al obtener informaci&oacute;n de la tarjeta #".$misc['cardNumber'].".";
		}
		/*END E-MAIL INFO*/

		if($temporary_customer->insert()) {
			// GlobalFunctions::sendMail($misc, 'newTemporaryCustomer');
		}
	}
}
?>
