<?php
/**
 * File: closePendingInvoices
 * Author: Patricio Astudillo
 * Creation Date: 22/09/2014
 * Last Modified: 22/09/2014
 * Modified By: Patricio Astudillo


	ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
	define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
	require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	
	$sql = "SELECT p.serial_pay, i.serial_inv
			FROM payments p 
			JOIN invoice i on i.serial_pay = p.serial_pay 
				AND i.status_inv = 'STAND-BY'
				AND p.total_to_pay_pay = p.total_payed_pay
				AND p.status_pay = 'PARTIAL'
			GROUP BY p.serial_pay";
	
	$result = $db->getAll($sql);
	
	if($result){
		$sale = new Sales($db);
		
		foreach($result as $payItem){	
			$payment = new Payment($db, $payItem['serial_pay']);
			$payment->getData();
			$payment->setStatus_pay('PAID');
			
			if($payment->update()){
				$invoice_list = Payment::getInvoicesInPayment($db, $payItem['serial_pay']);
				
				if($invoice_list){
					foreach($invoice_list as $invItem){
						$invoice = new Invoice($db, $invItem['serial_inv']);
						$invoice->getData();
						$invoice->setStatus_inv('PAID');//update the invoice status
						$invoice->update();
						
						Payment::registerEfectivePaymentDate($db, $payItem['serial_pay']);

						//CHANGE ALL REGISTERED SALES TO 'ACTIVE' STATUS
						$saleList = $sale -> getSalesByInvoice($invItem['serial_inv']);

						if(sizeof($saleList)>0){
							foreach($saleList as $s){
								if($s['status_sal'] == 'REGISTERED'){
									$sale->setSerial_sal($s['serial_sal']);
									$sale->changeStatus('ACTIVE');
								}
							}
						}						

						$misc['db'] = $db;
						$misc['type_com'] = 'IN';
						$misc['serial_fre'] = NULL;
						$misc['percentage_fre'] = NULL;
						GlobalFunctions::calculateCommissions($misc, $invItem['serial_inv']);
						GlobalFunctions::calculateBonus($misc, $invItem['serial_inv']);
					}
				}
			}
		}
	}

	*/