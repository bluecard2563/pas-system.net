<?php
/**
 * File: expireDueBonuses
 * Author: Patricio Astudillo
 * Creation Date: 27-ago-2013
 * Last Modified: 27-ago-2013
 * Modified By: Patricio Astudillo
 */
	
    ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
    define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
    require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

    $param = new Parameter($db, 45); //Max Time for Due Bonusses
    $param->getData();
    $days_due = $param->getValue_par();

    $due_bonusses = AppliedBonus::getOutOfDateBonusesByPeriodInDays($db, $days_due);

    if($due_bonusses){
            foreach($due_bonusses as $bonus){
                    AppliedBonus::authorizeBonus($db, $bonus['serial_apb'], 'NO', '1', 'OUT OF DATE BONUS AUTOMATIC DENIAL DUE TO '.$days_due.' DAYS POLICY');
            }
    }

?>
