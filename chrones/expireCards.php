<?php
/*
 * File: expireCards.php
 * Author: Patricio Astudillo
 * Creation Date: 01/07/2010, 12:25:07 PM
 * Modifies By: Patricio Astudillo
 * Description: Updates all active card to EXPIRED status after they completed it's life time.
 * Last Modified: 01/07/2010, 12:25:07 PM
 */

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

Sales::expireCards($db);
TravelerLog::expireTravelerLogRegisters($db);

?>
