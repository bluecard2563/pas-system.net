<?php
/*
File: expireStandByCards
Author: David Bergmann
Creation Date: 20/08/2010
Last Modified:
Modified By:
*/

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

$sales = Sales::getSalesWithStandByChanges($db);

if(is_array($sales)){
	foreach($sales as $s) {
		$sale = new Sales($db,$s['serial_sal']);
		$sale->getData();
		if($sale->getStatus_sal() == 'STANDBY' && $s['expire'] == 'YES') {
			$sale->setStatus_sal('EXPIRED');
			$sale->update();
		}
	}
}
?>
