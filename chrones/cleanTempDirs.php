<?php 
/*
File: cleanTempDirs.php
Author: Santiago Borja 
Creation Date: 27/10/2010

REMOVE all temp files non used
*/

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

$dirList[0] = 'system_temp_files';
$dirList[1] = 'modules/sales/contracts';
$dirList[2] = 'modules/sales/multiSaleContracts';
$dirList[3] = 'modules/billingToManager/reports';


foreach ($dirList as $singleDir){
	emptyDir(DOCUMENT_ROOT.$singleDir);
}

function emptyDir($dir){
	$current_dir = opendir($dir);
	while($entryname = readdir($current_dir)){
		if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!=".." and $entryname!=".svn")){
			emptyDir("{$dir}/{$entryname}");
		}elseif($entryname != "." and $entryname!=".." and $entryname!=".svn"){
			unlink("{$dir}/{$entryname}");
		}
	}
}
?>
