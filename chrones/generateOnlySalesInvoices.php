<?php
/*
File: generateOnlySalesInvoices
Author: David Bergmann
Creation Date: 06/10/2010
Last Modified:
Modified By:
*/

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

$sales = Sales::getDaylyOnlySaleManagerSales($db);

if(is_array($sales)) {
	$invoice = new Invoice($db);
	foreach($sales as $s) {
		$dealer = new Dealer($db,$s['serial_dea']);
		$dealer->getData();

		$documentByManager = new DocumentByManager($db);
		$serial_dbm = $documentByManager->getDocumentByManagerByType($s['serial_man'], 'INVOICE','BOTH');

		$invoice->setSerial_dea($dealer->getSerial_dea());
		$invoice->setSerial_dbm($serial_dbm);
		$date = date('d/m/Y');
		$invoice->setDate_inv($date);
		//Due Date
		$creditDay = new CreditDay($db,$s['serial_cdd']);
		$creditDay->getData();
		$days = $creditDay->getDays_cdd();
		$cd = strtotime(substr($date,3,3).substr($date,0,3).substr($date,6,4));
		$due_date_inv = date('d/m/Y', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
		$invoice->setDue_date_inv($due_date_inv);
		$number_inv = DocumentByManager::retrieveNextDocumentNumber($db, $s['serial_man'], 'LEGAL_ENTITY', 'INVOICE', TRUE);
		$invoice->setNumber_inv($number_inv);

		//Discount or Comission
		if($dealer->getType_percentage_dea() == 'DISCOUNT'){
			$invoice->setDiscount_prcg_inv($dealer->getPercentage_dea());
		}elseif($dealer->getType_percentage_dea() == 'COMISSION'){
			$invoice->setComision_prcg_inv($comision_prcg_inv);
		}

		//Sale subtotal
		$sales = new Sales($db,$s['serial_sal']);
		$sales->getData();
		$subtotal_inv = $sales->getTotal_sal();
		if($dealer->getPercentageType() == 'DISCOUNT'){
			$subtotal_inv -= ($subtotal_inv * $dealer->getPercentage_dea()/100);
		}
		$invoice->setSubtotal_inv($subtotal_inv);
		
		//Aplied Taxes
		$productByDealer = new ProductByDealer($db,$s['serial_pbd']);
		$productByDealer->getData();
		$taxes = $productByDealer->getTaxes();
		$taxesToAply = 0;
		if(is_array($taxes)){
			foreach ($taxes as $k=>$t){
				$misc[$k]['tax_name'] = $t['name_tax'];
				$misc[$k]['tax_percentage'] = $t['percentage_tax'];
				$taxesToAply += $t['percentage_tax'];
			}
			$invoice->setApplied_taxes_inv(serialize($misc));
		}

		//Sale total
		$total_inv = number_format($subtotal_inv*(1+($taxesToAply/100)),2, '.', '');
		$invoice->setTotal_inv($total_inv);

		if($invoice->insert()) {
			$sales->updateInvoice($db->insert_ID());
		}
	}
}
?>
