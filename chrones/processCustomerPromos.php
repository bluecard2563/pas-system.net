<?php
/*
 * File: processCustomerPromos
 * Author: Patricio Astudillo
 * Creation Date: Feb 24, 2011, 10:51:21 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

$sql_activate_promo = " UPDATE customer_promo
						SET status_ccp = 'ACTIVE'
						WHERE status_ccp = 'REGISTERED'
						AND NOW() BETWEEN begin_date_ccp AND DATE_ADD(end_date_ccp, INTERVAL 1 DAY)";

$db -> Execute($sql_activate_promo);

$sql_expire_promo = " UPDATE customer_promo
						SET status_ccp = 'EXPIRED'
						WHERE status_ccp = 'ACTIVE'
						AND NOW() > DATE_ADD(end_date_ccp, INTERVAL 1 DAY)";

$db -> Execute($sql_expire_promo);
?>
