<?php

/**
 * Name: createMissedCommissionsBonus
 * Created by: pastudillo
 * Created on: 10-may-2017
 * Description: 
 * Modified by: pastudillo
 * Modified on: 10-may-2017


ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT_CR', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT_CR.'lib/config.inc.php');

$sql = "SELECT i.serial_inv, i.number_inv, m.name_man, s.serial_sal, com.serial_com, s.card_number_sal
        FROM invoice i
        JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
        JOIN manager m ON m.serial_man = dbm.serial_man
            AND m.serial_man not in (20,5,6)
        JOIN payments p ON p.serial_pay = i.serial_pay
            AND i.status_inv = 'PAID'
            AND p.status_pay in ('PAID','EXCESS')
        JOIN sales s ON s.serial_inv = i.serial_inv
            AND s.emissiON_date_sal >= STR_TO_DATE('01/01/2017', '%d/%m/%Y')
            AND s.status_sal in ('ACTIVE','STANDBY','EXPIRED')
        JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
        JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
            AND pxc.serial_cou = 62
        LEFT JOIN applied_comissions com ON com.serial_sal = s.serial_sal
        WHERE com.serial_com IS NULL
        GROUP BY i.serial_inv";

$result = $db->getAll($sql);

if($result){
    foreach($result as $invoice){
        $misc['db']=$db;
        $misc['type_com']='IN';
        $misc['serial_fre']=NULL;
        $misc['percentage_fre']=NULL;
        GlobalFunctions::calculateCommissions($misc, $invoice['serial_inv']);
        GlobalFunctions::calculateBonus($misc, $invoice['serial_inv']);
        
        ErrorLog::log($db, 'FORCED COMMISSION CREATION - CARD NUMBER: '.$invoice['card_number_sal'], $invoice);
    }
}
 *
 * */