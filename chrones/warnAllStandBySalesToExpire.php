<?php
/*
 * File: warnAllStandBySalesToExpire.php
 * Author: Patricio Astudillo
 * Creation Date: 19/09/2016, 12:25:07 PM
 * Modifies By: Patricio Astudillo
 * Description: Fetchs all stand by sales near to expire and sends them an email to warn the dealers about it.
 * Last Modified: 19/09/2016, 12:25:07 PM
 */

ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');

define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT.'lib/config.inc.php');

$parameter = new Parameter($db, 9); //MAX ALLOWED TIME IN STAND-BY IN MONTHS
$parameter->getData();
$maxDays = $parameter->getValue_par() * 30; //MULTIPLY TO 30 TO GET VALUE IN DAYS

$salesList = Sales::getAllStandBySalesToExpire($db, 2, 160, $maxDays);

$parameter = new Parameter($db, 52); //DAYS BEFORE EXPIRATION TO SEND EMAIL
$parameter->getData();
$alertDays = split(',', $parameter->getValue_par());

if($salesList){
	foreach($salesList as $sale){
		$daysUntilExpiration = $maxDays - $sale['daysPassed'];
		
		if(in_array($daysUntilExpiration, $alertDays)){
			//$misc['email'] = 'pastudillo@planet-assist.net';
			$misc['email'] = $sale['email1_dea'];
			$misc['subject'] = "Recordatorio Blue Card_Contrato en Stand by";
			$misc['cardNumber'] = $sale['cardNumber'];
			$misc['activationDate'] = $sale['dueDate'];

			if($misc['email'] != ''){
				GlobalFunctions::sendMail($misc, 'standby_warning');
			}
			break;
		}
	}
}
