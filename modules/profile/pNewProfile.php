<?php
/*
File: pNewProfile.php
Author: Esteban Angulo
Creation Date: 28/12/2009 18:00
Last Modified: 03/02/2010
Modified By: David Bergmann
*/
//Debug::print_r($_POST);
//die;
	$profile=new ProfileByCountry($db);
	$profile->setName_pbc($_POST['txtProfileName']);
	$profile->setSerial_cou($_POST['selCountry']);
	$profile->setStatus_pbc($_POST['selStatus']);
	

	$selectedOptions=$_POST['option'];
	if(!$profile->verifyName()){
		if(sizeof($selectedOptions)>0){
			$serial_pbc=$profile->insert();
			if($serial_pbc){

				foreach($selectedOptions as $p){
					//Obteins the  place of - in order to catch the part after that as the process to insert
					$num = strpos($p,'-');
					//It obteins the serial of the Process
					$option = substr($p,$num+1);
					//Inserts the process related to the profile
					$insobyp = $profile->insertOpt_by_profcountry($serial_pbc,$option);
				}
				
				if($insobyp){
					http_redirect('modules/profile/fNewProfile/1');// Success
				}else{
					http_redirect('modules/profile/fNewProfile/4');// There is an error and the option by profile was not updated
				}
			}else{
				http_redirect('modules/profile/fNewProfile/3');//Profile not updated. There is an error
			}
		}else{
			http_redirect('modules/profile/fNewProfile/5');//At least one option must be chosen 
		}
	}else{
		http_redirect('modules/profile/fNewProfile/2');//A profile with the same name already exists. Profile not Updated
	}
?>