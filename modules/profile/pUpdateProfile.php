<?php session_start();
/*
File: pUpdateProfile.php
Author: Esteban Angulo
Creation Date: 30/12/2009 11:20
Last Modified: 11/01/2011
Modified By: David Bergmann
*/
//die(Debug::print_r($_POST));


$code_lang=$_SESSION['language'];
$serial_pbc = $_POST['hddSerialProfile'];
$profile=new ProfileByCountry($db,$serial_pbc);
$statusList=$profile->getAllStatus();

$options=new Options($db);
$optionsList=$options->getOptionsRelationship($code_lang);

if($_POST['update'] == '1'){
	//creates a profile whit the name previously entered
	$name=$_POST['txtProfileName'];
	$country=$_POST['hdnSerial_cou'];
	$profile->setName_pbc($name);
	$profile->setSerial_cou($country);
	//$profile->getData();
	if($_POST['selStatus']!=""){
		$profile->setStatus_pbc($_POST['selStatus']);
	}else{
		$profile->setStatus_pbc('INACTIVE');
	}	
	
	$selectedOption=$_POST['option'];
	if(!$profile->verifyNameUpdate()){
		if(sizeof($selectedOption)>0){
			if($profile->update()){
					$profile->deleteOptions_by_Profile();

					foreach($selectedOption as $p){
						// Obteins the place of  -  to get the part after that as the option which is going to update
						$num = strpos($p,'-');
						//Obteins the serial of the option
						$uptOptions = substr($p,$num+1);
						//Updates the option related to the profile
						$upobyxp = $profile->insertOpt_by_profcountry($serial_pbc,$uptOptions);
					}
					if($upobyxp){
						http_redirect('modules/profile/fSearchProfile/1');//success
					}else{
						 // There is an error and the option by profile was not updated
						$error=4;
						$profile -> getData();
						
						$name = $profile->getName_pbc();
						$status = $profile->getStatus_pbc();
						
						$optionsProfile = $profile -> getOptionsbyProfile($serial_pbc);
						
						if(is_array($optionsProfile)){
							foreach($optionsProfile as $key=>$value){
								$optionsProfile[$key]=$value['serial_opt'];
					
								foreach($optionsList as &$p){
									if(is_array($p['child'])){
										foreach($p['child'] as &$c){
											if(is_array($c['child2'])){
												foreach($c['child2'] as &$ch){
													if(in_array($ch['serial_opt'],$optionsProfile)){
														$ch['checked']=1;
													}else{
														$ch['checked']=0;
													}
												}
											}
										
											if(in_array($c['serial_opt'],$optionsProfile)){
												$c['checked']=1;
											}else{
												$c['checked']=0;
											}
										}
								  }
								}
							}
						}
					
						//$smarty->register('error');
						http_redirect('modules/profile/fSearchProfile/'.$error);
					}
			}else{
				//Profile not updated. There is an error
				$error=3;
				$profile -> getData();
				
				$name = $profile->getName_pbc();
				$country=$profile->getSerial_cou();
				$status = $profile->getStatus_pbc();
				
				$optionsProfile = $profile -> getOptionsbyProfile($serial_pbc);
				
				if(is_array($optionsProfile)){
					foreach($optionsProfile as $key=>$value){
						$optionsProfile[$key]=$value['serial_opt'];
			
						foreach($optionsList as &$p){
							if(is_array($p['child'])){
								foreach($p['child'] as &$c){
									if(is_array($c['child2'])){
										foreach($c['child2'] as &$ch){
											if(in_array($ch['serial_opt'],$optionsProfile)){
												$ch['checked']=1;
											}else{
												$ch['checked']=0;
											}
										}
									}
								
									if(in_array($c['serial_opt'],$optionsProfile)){
										$c['checked']=1;
									}else{
										$c['checked']=0;
									}
								}
						  }
						}
					}
				}
			
				//$smarty->register('error');
				http_redirect('modules/profile/fSearchProfile/'.$error);
			}
		}else{
			$error=5;
			$profile -> getData();
			
			$name = $profile->getName_pbc();
			$status = $profile->getStatus_pbc();
			
			$optionsProfile = $profile -> getOptionsbyProfile($serial_pbc);
			
			if(is_array($optionsProfile)){
				foreach($optionsProfile as $key=>$value){
					$optionsProfile[$key]=$value['serial_opt'];
		
					foreach($optionsList as &$p){
						if(is_array($p['child'])){
							foreach($p['child'] as &$c){
								if(is_array($c['child2'])){
									foreach($c['child2'] as &$ch){
										if(in_array($ch['serial_opt'],$optionsProfile)){
											$ch['checked']=1;
										}else{
											$ch['checked']=0;
										}
									}
								}
							
								if(in_array($c['serial_opt'],$optionsProfile)){
									$c['checked']=1;
								}else{
									$c['checked']=0;
								}
							}
					  }
					}
				}
			}
		
			//$smarty->register('error');
			http_redirect('modules/profile/fSearchProfile/'.$error);
		}
	}else{
 		 //A profile with the same name already exists. Profile not Updated
			$error=2;
			$profile -> getData();
			
			$name = $profile->getName_pbc();
			$status = $profile->getStatus_pbc();
				
			$optionsProfile = $profile -> getOptionsbyProfile($serial_pbc);
				
			if(is_array($optionsProfile)){
				foreach($optionsProfile as $key=>$value){
					$optionsProfile[$key]=$value['serial_opt'];
			
					foreach($optionsList as &$p){
						if(is_array($p['child'])){
							foreach($p['child'] as &$c){
								if(is_array($c['child2'])){
									foreach($c['child2'] as &$ch){
										if(in_array($ch['serial_opt'],$optionsProfile)){
											$ch['checked']=1;
										}else{
											$ch['checked']=0;
										}
									}
								}
								
								if(in_array($c['serial_opt'],$optionsProfile)){
									$c['checked']=1;
								}else{
									$c['checked']=0;
								}
							}
					  }
					}
				}
			}
			
			//$smarty->register('error');
			http_redirect('modules/profile/fSearchProfile/'.$error);
	}
}

//$smarty->register('error,statusList,optionsList,optionsProfile,serial_pbc,name,status');
//$smarty->display('modules/profile/fSearchProfile.'.$_SESSION['language'].'.tpl');
?>