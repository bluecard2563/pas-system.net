<?php 
/*
File: processEstimatorSmall.php
Author: Santiago Borja
Creation Date: 01/04/2010
*/

	Request::setString('selProduct');
	Request::setString('selDeparture');
	Request::setString('selDestination');
	Request::setString('checkPartner');
	Request::setString('selTripDays');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('txtOverAge');
	Request::setString('txtUnderAge');
	
	//Save all values to session
	$_SESSION['web_selProduct'] = $selProduct;
	$_SESSION['web_selDeparture'] = $selDeparture;
	$_SESSION['web_selDestination'] = $selDestination;
	$_SESSION['web_checkPartner'] = $checkPartner;
	$_SESSION['web_selTripDays'] = $selTripDays;
	$_SESSION['web_txtBeginDate'] = $txtBeginDate;
	$_SESSION['web_txtEndDate'] = $txtEndDate;
	$_SESSION['web_txtOverAge'] = $txtOverAge;
	$_SESSION['web_txtUnderAge'] = $txtUnderAge;
	
	$userLogged = isset($_SESSION['serial_customer_estimator'])?'1':'0';
	if($userLogged){
		http_redirect('modules/estimator/estimatorCustomer/0/2');
	}
	http_redirect('estimatorFull/2');
?>