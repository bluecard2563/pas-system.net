<?php
	/*
	File: fEstimatorCustomerLogin.php
	Author: Santiago Borja
	Creation Date: 12/07/2010
	*/
	Request::setString('0:error');

	$countryList = Country::getAllAvailableCountries($db);

    $customer=new Customer($db);
    $typesList=$customer->getAllTypes();

	$smarty->register('error,countryList,typesList');
	$smarty->display();
?>