<?php 
/*
File: saleCustomerCreditCardData.php
Author: Santiago Borja
Creation Date: 10/04/2010
*/
	//EXTRA SECURITY CHECK
	if(!isset($_SESSION['serial_customer_estimator'])){
		http_redirect('modules/estimator/fEstimatorCustomerLogin');
	}
	
	Request::setString('0:error');
	
	$sessionProducts = $_SESSION['cart_products_travelers_information'];
	$serial_cup = $_SESSION['serial_cup'];
	$finalPrice = 0;
	
	$productPricesTable = array();
	$num = 0;
	
	if(is_array($sessionProducts)){
		foreach($sessionProducts as $sessProd){
			if($sessProd['informationComplete']){
				$total_amount_for_card = $sessProd['total_price_pod'];
				
				if($serial_cup){
					$data = Cupon::getCuponData($db, $serial_cup);
					$promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);

					if($data['limit_amount_cup'] > 0){ //IF THE COUPON HAS A LIMIT AMOUNT
						$limit_amount = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_amount_for_card , 2, '.', '');

						if($limit_amount < $data['limit_amount_cup']){ //IF THE NEW DISCOUNT IS LESS THAN THE PREVIOUS ONE. WE USE IT.
							$discount_of_product = $limit_amount;
						}else{ //OTHERWISE, WE USE THE LIMIT OF THE COUPON
							$discount_of_product = $data['limit_amount_cup'];
						}
					}else{ //IF NO LIMIT IS SPECIFIED
						$discount_of_product = ($promo_info['discount_percentage_ccp'] / 100) * $total_amount_for_card;
					}
				}

				$total_amount_for_card = $total_amount_for_card - $discount_of_product;

				$productPricesTable[] = array ('type' => 'product',
											   'num' => ++$num,
											   'name' => $sessProd['name_pbl'],
											   'cupon' => number_format($discount_of_product, 2, '.', ''),
											   'price' =>  number_format($total_amount_for_card, 2, '.', ''));
				
				//THE TAXES:
				$tbc = new TaxesByProduct($db, $sessProd['serial_pxc']);
	    		$taxes = $tbc -> getTaxesByProduct();
	    		$taxesToApply = 0;
	
	    		if($taxes){
		    		foreach ($taxes as $k=>$n){
						$price = intval($n['percentage_tax']) * $total_amount_for_card / 100;
						$productPricesTable[] = array ('type' => 'tax',
									   	   				'name' => $n['name_tax'],
										   				'percentage' => $n['percentage_tax'],
									   	   				'price' => $price);
						$taxesToApply += $price;
					}
	    		}
				
				$partialPrice = $total_amount_for_card + $taxesToApply;
				$finalPrice += $partialPrice;
				$symbolCur = $sessProd['symbol_cur'];
				
				$productPricesTable[] = array (	'type' => 'total',
								   				'name' => 'Subtotal:',
								   				'price' =>  number_format($partialPrice, 2, '.', ''));
				$saleProductsArray[] = $sessProd['serial_pxc'];
			}
		}
	}
	
	$productPricesTable[] = array (	'type' => 'total',
							   		'name' => 'Precio Final:',
							   		'price' =>  number_format($finalPrice, 2, '.', ''));
	$_SESSION['current_sale_total'] =  number_format($finalPrice, 2, '.', '');
	$_SESSION['current_sale_products'] = implode("_",$saleProductsArray);
	
	$countries = Country::getAllCountries($db,false);
	
	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$years = array();
	for($i = date('Y');$i <= date('Y') + 10; $i++){
		$years[] = $i;
	}
	
	$smarty -> register('error,countries,symbolCur,productPricesTable,months,years');
	$smarty -> display();
?>