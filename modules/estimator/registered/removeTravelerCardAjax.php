<?php
/*
File: removeTravelerCardAjax.php
Author: Santiago Borja Lopez
Creation Date: 30/05/2010 10:00
*/
	Request::setString('productIndex');
	Request::setString('cardIndex');
	
	//Check if it is the only card for this product
	$sessionProductTravelersData = &$_SESSION['cart_products_travelers_information'];
	$cards = &$sessionProductTravelersData[$productIndex]['cards'];
	
	if(count($cards) == 1){
		unset($sessionProductTravelersData[$productIndex]);
		$sessionProductTravelersData = array_values($sessionProductTravelersData);
		echo '0';return;
	}
	unset($cards[$cardIndex]);
	$cards = array_values($cards);
	echo $productIndex;return;	
?>