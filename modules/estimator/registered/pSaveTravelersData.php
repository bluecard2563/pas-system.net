<?php
/*
File: pSaveTravelersData.php
Author: Santiago Borja Lopez
Creation Date: 16/04/2010 10:00
*/
	//EXTRA SECURITY CHECK
	if(!isset($_SESSION['serial_customer_estimator'])){
		http_redirect('modules/estimator/fEstimatorCustomerLogin');
	}
 
	$questionList = Question::getActiveQuestions($db, Language::getSerialLangByCode($db, $_SESSION['language']));
	$sessionProductTravelersData = $_SESSION['cart_products_travelers_information'];
	$productData = &$sessionProductTravelersData[$_POST['prodIndex']];
	$cardIndex = $_POST['cardIndex'];
	
	//Save holder data:
	$answersHolder = array(); 
	foreach($questionList as $q){
		$serialQ = $q['serial_qbl'];
		
		$answer = $_POST['answer_0_' . $serialQ];
		$answersHolder[$serialQ] = $answer;
	}
	
	$productData['cards'][$cardIndex]['holder'] = array(
								'serial_cus' => $_POST['hidCustomerId'],
								'serial_cit' => $_POST['selCityCustomer'],
								'type_cus' => $_POST['selType'],
								'document_cus' => $_POST['txtDocumentCustomer'],
								'first_name_cus' => $_POST['txtNameCustomer'],
								'last_name_cus' => $_POST['txtLastnameCustomer'],
								'birthdate_cus' => $_POST['txtBirthdayCustomer'],
								'phone1_cus' => $_POST['txtPhone1Customer'],
								'phone2_cus' => $_POST['txtPhone2Customer'],
								'cellphone_cus' => $_POST['txtCellphoneCustomer'],
								'email_cus' => $_POST['txtMailCustomer'],
								'address_cus' => $_POST['txtAddress'],
								'relative_cus' => $_POST['txtRelative'],
								'relative_phone_cus' => $_POST['txtPhoneRelative'],
								'answers' => $answersHolder
							);
	
	//Initialize Data:
	$adultsTraveling = 1;
	$childrenTraveling = 0;
	$spouseTraveling = false;
	
	//Get third party register pro, adults and children for free:
	$prodByCou = new ProductByCountry($db,$productData['serial_pxc']);
	$prodByCou -> getData();
	$product = new Product($db,$prodByCou -> getSerial_pro());
	$product -> getData();
	$productData['third_party_register_pro'] = $product -> getThird_party_register_pro();
	$adultsFree = $product -> getAdults_pro();
	$childrenFree = $product -> getChildren_pro();
	
	$cardPrice = $productData['price_pod'];
	$count = 0;
	foreach ($productData['cards'][$cardIndex]['extras'] as $key => &$extra){
		//Save the Extra's info:
		$extra['idExtra'] = $_POST['extra'][$key]['idExtra'];
		$extra['serialCus'] = $_POST['extra'][$key]['serialCus'];
		$extra['selRelationship'] = $_POST['extra'][$key]['selRelationship'];
		$extra['nameExtra'] = $_POST['extra'][$key]['nameExtra'];
		$extra['lastnameExtra'] = $_POST['extra'][$key]['lastnameExtra'];
		$extra['birthdayExtra'] = $_POST['extra'][$key]['birthdayExtra'];
		$extra['emailExtra'] = $_POST['extra'][$key]['emailExtra'];
		$extra['ownPrice'] =  0;
		$extra['ownCost'] =  0;
		
		//Answers:
		$answersExtra = array(); 
		foreach($questionList as $q){
			$serialQ = $q['serial_qbl'];
			
			$answer = $_POST['answer_' . ($key + 1) . '_' . $serialQ];
			$answersExtra[$serialQ] = $answer;
		}
		$extra['answers'] =  $answersExtra;
		
		//Extras Price Calculation:
		global $adultMinAge;
		$extraAge = GlobalFunctions::getMyAge($extra['birthdayExtra']); 
		if($extraAge >= $adultMinAge){
			if($extra['selRelationship'] == 'SPOUSE'){//We just avoided underage spouses
				$spouseTraveling = true;
				if($adultsFree == 0){
					$extra['ownPrice'] =  $productData['price_spouse_pod'];
					$extra['ownCost'] =  $productData['cost_spouse_pod'];
				}else{
					$adultsFree --;//If at least one adult goes for free, it's the spouse
				}
			}else{
				$adultsTraveling ++;
				if($adultsFree == 0){
					$extra['ownPrice'] = $productData['price_extra_pod'];
					$extra['ownCost'] =  $productData['cost_extra_pod'];
				}else{
					$adultsFree --;
				}
			}
		}else{
			$childrenTraveling ++;
			if($childrenFree == 0){
				$extra['ownPrice'] = $productData['price_children_pod'];
				$extra['ownCost'] =  $productData['cost_children_pod'];
			}else{
				$childrenFree --;
			}
		}
		$count ++;
		$cardPrice += $extra['ownPrice'];
	}
	$productData['cards'][$cardIndex]['card_price_pod'] = $cardPrice;
	$productData['cards'][$cardIndex]['informationComplete'] = true;
	$productData['cards'][$cardIndex]['adults_pod'] = $adultsTraveling; //Adults apart from the spouse
    $productData['cards'][$cardIndex]['children_pod'] = $childrenTraveling;
    $productData['cards'][$cardIndex]['spouse_pod'] = $spouseTraveling?'1':'0';
    
	$_SESSION['cart_products_travelers_information'] = $sessionProductTravelersData;
	http_redirect('modules/estimator/registered/saleCustomerTravelersData/'.$_POST['prodIndex']);
?>