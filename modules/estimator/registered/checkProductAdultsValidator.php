<?php 
/*
File: checkProductCardsEmail.rpc.php
Author: Santiago Borja
Creation Date: 24/05/2010
Description: Checks uniqueness of an email address with respect to the email 
addresses entered to other cards over the same product.
FOR ESTIMATOR SALES CUSTOMER TRAVELERS DATA PAGE ONLY 
*/

	Request::setString('birthDate');
	Request::setString('type');

	$myAge = GlobalFunctions::getMyAge($birthDate);
	
	//JOINT VALIDATION FOR ADULTS AND CHILDREN
	$returnValue = true;
	if($myAge <= 12){
		$returnValue = false;
	}
	if($type == 'CHILDREN'){
		$returnValue = !$returnValue;
	}
	
	if($type == 'SENIOR'){
		if($myAge > 65){
			$returnValue = false;//Here false means he is old
		}else{
			$returnValue = true;
		}
	}
	
	echo json_encode($returnValue);return;
?>