<?php
/**
 * File: pBuyShoppingCart.php
 * Author: Santiago Borja Lopez
 * Creation Date: 16/04/2010 10:00
 * Modified By: Patricio Astudillo
 * Modification Date: 09/07/2012
 */

if(!USE_CONVERGE){
    include(DOCUMENT_ROOT . 'lib/merchantAccountConnection.inc.php');

    Request::setString('0:error');
    Request::setString('1:extra_code');

    $error = $custom_error_code;
    $extra_code = $transaction_response;
}

$_SESSION['serial_lang'] = Language::getSerialLangByCode($db, $_SESSION['language']);

//We reached this point after a successful transaction!
$cartProds = $_SESSION['cart_products_travelers_information'];

ErrorLog::log($db, 'PHONE SALES INIT ' . $error, $cartProds); //SAFETY LOG

if ($error != '30' && $error != '40') {
    ErrorLog::log($db, 'PHONE SALE DENIED' . $error, $extra_code);
    http_redirect('customer/saleCustomerSummary/' . $error . '/' . $extra_code);
}

//GET THE CUPON'S ID FOR THE INFORMATION.
$serial_cup = $_SESSION['serial_cup'];
$cupon_applied = 0;
unset($_SESSION['serial_cup']);

global $phoneSalesDealer;
$dealerPhone = new Dealer($db, $phoneSalesDealer);
$dealerPhone->getData();
$counterPhone = new Counter($db, Dealer::getDealerUniqueCounter($db, $phoneSalesDealer));
$counterPhone->getData();

$dealersCountry = Dealer::getDealersCountrySerial($db, $phoneSalesDealer);

$redirectToFlightsLog = false;
$dbFail = 0;
$todaysDate = date("d") . '/' . date("m") . '/' . date("Y");

$validityParam = new Parameter($db, '14');
$validityParam->getData();
$validityPeriod = $validityParam->getValue_par();

foreach ($cartProds as $prod) {
    if ($prod['informationComplete']) {
        $prodByCou = new ProductByCountry($db, $prod['serial_pxc']);
        $prodByCou->getData();

        $departureCou = $prodByCou->getSerial_cou();
        $userPhoneSalesCounter = Counter::getCountryPhoneSalesCounter($db, $departureCou, $_SESSION['serial_usr']);
        global $phoneSalesDealer;
        if (!$userPhoneSalesCounter) {
            $phoneCountry = Dealer::getDealersCountrySerial($db, $phoneSalesDealer);
            $userPhoneSalesCounter = Counter::getCountryPhoneSalesCounter($db, $phoneCountry);
        }

        $serialDea = Counter::getCountersTopDealer($db, $userPhoneSalesCounter); //WARNING: DEPENDS ON WHICH COUNTER WE USED BEFORE
        $dealerPhone = new Dealer($db, $serialDea);
        $dealerPhone->getData();
        $counterPhone = new Counter($db, $userPhoneSalesCounter);
        $counterPhone->getData();

        $city = new City($db);
        $destination_city = City::getGeneralCityByCountry($db, $prod['serial_cou']);
        $destination_city = $destination_city['serial_cit'];

        $productInTransit = $prodByCou->getSerial_pro();
        $serialPxc = ProductByCountry::getPxCSerial($db, $dealersCountry, $productInTransit);
        $serialPbd = ProductByDealer::getPBDbyPXC($db, $serialDea, $serialPxc);
        if (!$serialPbd) {
            ErrorLog::log($db, $productInTransit . ' NO PBD ' . $serialPbd, $prod);
            escapeOnError($db, 24, 'SYS-NO PBD, CHECK CART PRODUCTS!!' . $serialPxc); //DB FAIL LOG
        }

        //For several_flights = 0
        $validUntil = date("d/m/Y", strtotime($prod['begin_date_pod'] . ' +' . $validityPeriod . ' days'));
        $prod['begin_date_pod'] = date("d/m/Y", strtotime($prod['begin_date_pod']));
        $prod['end_date_pod'] = date("d/m/Y", strtotime($prod['end_date_pod']));
        
        if ($prod['departure_cou'] == '62'):
            include DOCUMENT_ROOT . 'modules/estimator/registered/customCheckOutWithERPConnectivity.php';
        else:
            include DOCUMENT_ROOT . 'modules/estimator/registered/globalCheckOutWithERPConnectivity.php';
        endif;

        //********************************************** THE PDF CONTRACT *************************************:
        $serial_generated_sale = $serialSale;
        $display = 0;
        $urlContract = 'contract_' . $serialSale . '.pdf';
        include(DOCUMENT_ROOT . 'modules/sales/pPrintSalePDF.php');

        //Send contract by mail:
        $misc['subject'] = 'REGISTRO DE NUEVA VENTA';
        $misc['customer'] = $customerH->getFirstname_cus() . ' ' . $customerH->getLastname_cus();
        $misc['cardNumber'] = $cardNumber;
        $misc['username'] = $customerH->getEmail_cus();
        $misc['email'] = $customerH->getEmail_cus();
        /* GENERAL CONDITIONS FILE */
        $country_of_sale = Sales::getCountryofSale($db, $serialSale);
        if ($country_of_sale != 62) {
            $country_of_sale = 1;
        }
        $misc['urlGeneralConditions'] = $sale->getGeneralConditionsbySale($serialPbd, $serialSale, $country_of_sale, $_SESSION['serial_lang']);
        /* END GENERAL CONDITIONS FILE */
        $misc['urlContract'] = $urlContract;
        $password = $customerH->getPassword_cus();

        if ($password) {
            $password = 'Su clave es la misma de la &uacute;ltima vez.';
        } else {
            $password = GlobalFunctions::generatePassword(6, 8, false);
            $customerH->setPassword_cus($password);
            $customerH->update();
        }
        $misc['pswd'] = $password;

        ErrorLog::log($db, ' sending mail ', $misc);
        if (!GlobalFunctions::sendMail($misc, 'newClient')) {
            escapeOnError($db, 23, 'mail not sent'); //DB FAIL LOG
        }
    }//End cards foreach
    //Finally We delete all the cart:
        
    $misc['db']=$db;
    $misc['type_com']='IN';
    $misc['serial_fre']=NULL;
    $misc['percentage_fre']=NULL;
    //GlobalFunctions::calculateCommissions($misc,$serialInv);
    //GlobalFunctions::calculateBonus($misc,$serialInv);

    PreOrderDetail::deletePOD($db, $prod['serial_pod']);
}

//Finally we delete the session information:
unset($_SESSION['cart_products_travelers_information']);
unset($_SESSION['ccnum']);
unset($_SESSION['web_estimator_altern_invoice_holder']);

$item = '1';
if ($redirectToFlightsLog) {
    $item = '-1';
}
http_redirect($estimatorSuccessPage . $item);

//************************** FUNCTION DEFINITION *********************************************

function escapeOnError($db, $dbFail, $title = '-', $log = '-', $extra_code_error = NULL) {
    global $estimatorErrorPage;
    $id = ErrorLog::log($db, $title, $log);
    //ErrorLog::retrieve($db,$id);
    http_redirect($estimatorErrorPage . $dbFail . '/' . $extra_code_error);
}

?>