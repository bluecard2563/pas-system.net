<?php 
/*
File: saleCustomerCreditCardData.php
Author: Santiago Borja
Creation Date: 10/04/2010
*/
	Request::setString('0:error');
	Request::setString('1:extra_error_code');
	//10 - 20 ERRORS ARE FROM pBuyShoppingCart 
	//30 ERRORS ARE FROM PAYPAL
	//40 ERRORS FROM MERCHANT ACCOUNT

	/* COUPON INFORMATION */
	if($_POST['txtCoupon']){
		$code_parts = split('-', $_POST['txtCoupon']);
		$cupon_applied = 0;
	}else{
		unset($_SESSION['serial_cup']);
	}
	/* COUPON INFORMATION */
	
	$sessionProducts = $_SESSION['cart_products_travelers_information'];
	$finalPrice = 0;
	$ecuadorian_departure =false;
	
	$productPricesTable = array();
	$num = 0;
	$saleProductsArray = array();
	foreach($sessionProducts as $sessProd){
		$discount_of_product = 0;
		if($sessProd['informationComplete']){
			$total_amount_for_card = $sessProd['total_price_pod'];
			
			if($code_parts){
				$serial_cup = Cupon::validateCupon($db, trim($code_parts['1']), strtoupper(trim($code_parts['0'])), $sessProd['serial_pxc'], 'PHONE', 'CREDIT_CARD');
				if($serial_cup && $cupon_applied == 0){
					$_SESSION['serial_cup'] = $serial_cup;
					$cupon_applied = 1;
					$data = Cupon::getCuponData($db, $serial_cup);
					$promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);
					$previous_limit = $data['limit_amount_cup'];
					
					if($data['generating_sal'] != ''){ //COUPON CAME FROM A PREVIOUS ONE
						$promo_info['discount_percentage_ccp'] = $promo_info['other_discount_ccp'];
						$promo_info['other_discount_ccp'] = 0;			
						
						//IF THE LIMIT AMOUNT IS GREATER THAN THE SALE, WE USE THE PERCENTAGE OF THE COUPON
						if($previous_limit > $total_amount_for_card){
							$discount_of_product = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_amount_for_card, 2, '.', '');
						}else{ //IF NOT, WE USE THE LIMIT AMOUNT
							$discount_of_product = $previous_limit;
						}
					}else{ //IF NOT
						$discount_of_product = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_amount_for_card, 2, '.', '');
					}				
				}
			}
			
			$total_amount_for_card = $total_amount_for_card - $discount_of_product;

			$productPricesTable[] = array ('type' => 'product',
										   'num' => ++$num,
										   'name' => $sessProd['name_pbl'],
										   'cupon' => number_format($discount_of_product, 2, '.', ''),
										   'price' => number_format($total_amount_for_card, 2, '.', ''));
			
			//THE TAXES:
			$tbc = new TaxesByProduct($db, $sessProd['serial_pxc']);
    		$taxes = $tbc -> getTaxesByProduct();
    		$taxesToApply = 0;

    		if($taxes){
	    		foreach ($taxes as $k=>$n){
					$price = intval($n['percentage_tax']) * $total_amount_for_card / 100;
					$productPricesTable[] = array ('type' => 'tax',
								   	   				'name' => $n['name_tax'],
									   				'percentage' => $n['percentage_tax'],
								   	   				'price' => $price);
					$taxesToApply += $price;
				}
    		}
			
			$partialPrice = $total_amount_for_card + $taxesToApply;
			$finalPrice += $partialPrice;
			
			$productPricesTable[] = array (	'type' => 'total',
							   				'name' => 'Subtotal:',
							   				'price' =>  number_format($partialPrice, 2, '.', ''));
			
			$saleProductsArray[] = $sessProd['serial_pxc'];
			
			if($sessProd['departure_cou'] == 62){
				$ecuadorian_departure = true;
			}
		}
	}
	$productPricesTable[] = array (	'type' => 'total',
							   		'name' => 'Precio Final:',
							   		'price' =>  number_format($finalPrice, 2, '.', ''));
	
	//SAVE SESSION VALUES:
	$_SESSION['productPricesTable'] = $productPricesTable;
	$_SESSION['current_sale_total'] =  number_format($finalPrice, 2);
	$_SESSION['current_sale_products'] = implode("_",$saleProductsArray);
	
	$countries = Country::getAllCountries($db,false);
	$months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$years = array();
	for($i = date('Y');$i <= date('Y') + 10; $i++){
		$years[] = $i;
	}
	
	/**************** RETRIEVE COMMON DATA FOR CUSTOMER FORM ****************/
	$countryList = Country::getAllAvailableCountries($db);
	
	$smarty -> register('ecuadorian_departure,countryList');
	$smarty -> register('error,countries,productPricesTable,months,years,extra_error_code,cupon_applied,code_parts');
	$smarty -> display();
?>