<?php 
/*
File: checkProductCardsEmail.rpc.php
Author: Santiago Borja
Creation Date: 24/05/2010
Description: Checks uniqueness of an email address with respect to the email 
addresses entered to other cards over the same product.
FOR ESTIMATOR SALES CUSTOMER TRAVELERS DATA PAGE ONLY 
*/

	Request::setString('txtValue');
	Request::setString('prodIndex');
	Request::setString('cardIndex');
	Request::setString('type');
	
	switch($type){
		case 'email':
			$holderField = 'email_cus';
			$extrasField = 'emailExtra';
			break;
		case 'id':
			$holderField = 'document_cus';
			$extrasField = 'idExtra';
			break;
	}
	
	$txtMailCustomer = trim(strtolower(utf8_decode($txtValue)));
	
	$productData =  $_SESSION['cart_products_travelers_information'][$prodIndex];
	
	unset($productData['cards'][$cardIndex]);//Make sure we dont compare with own emails, that is another validator
	$cardsData =  $productData['cards'];
	
	//Gather all email addresses for analisys:
	if(is_array($cardsData) && count($cardsData) > 0){
		foreach ($cardsData as $card){
			$_SESSION['debugger']['enter'] = 'entro';
			if($card['holder'][$holderField] == $txtMailCustomer){
				$_SESSION['debugger'][date('ms')]['product_index'] = $prodIndex;
				$_SESSION['debugger'][date('ms')]['validation_case'] = $type;
				$_SESSION['debugger'][date('ms')]['holder_key'] = $holderField;
				$_SESSION['debugger'][date('ms')]['holder_value'] = $card['holder'][$holderField];
				$_SESSION['debugger'][date('ms')]['current_value'] = $txtMailCustomer;

				echo json_encode(false);return;
			}
			
			foreach ($card['extras'] as $extra){
				if($extra[$extrasField] == $txtMailCustomer){
					$_SESSION['debugger'][date('ms')]['product_index'] = $prodIndex;
					$_SESSION['debugger'][date('ms')]['validation_case'] = $type;
					$_SESSION['debugger'][date('ms')]['extra_key'] = $extra;
					$_SESSION['debugger'][date('ms')]['extra_value'] = $extra[$extrasField];
					$_SESSION['debugger'][date('ms')]['current_value'] = $txtMailCustomer;

					echo json_encode(false);return;
				}
			}
		}
	}
	
	echo json_encode(true);
?>