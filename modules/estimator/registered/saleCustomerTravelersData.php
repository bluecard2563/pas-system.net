<?php 
/*
File: saleCustomerTravelersData.php
Author: Santiago Borja
Creation Date: 10/04/2010
*/
	//EXTRA SECURITY CHECK
	if(!isset($_SESSION['serial_customer_estimator'])){
		http_redirect('modules/estimator/fEstimatorCustomerLogin');
	}
	
	Request::setString('0:productSelected');
	if(!$productSelected){$productSelected = 0;}
	
	$preOrder = new PreOrder($db, NULL, $_SESSION['serial_customer_estimator'], 'CUSTOMER', time());
	$cartProducts = $preOrder -> getAllMyCartProductItems(Language::getSerialLangByCode($db, $_SESSION['language']));
	
	$holder = new Customer($db, $_SESSION['serial_customer_estimator']);
	$holder -> getData();
	
	//IF NOT SET, THEN INITIALIZE - ONLY FIRST TIME:
	if(!isset($_SESSION['cart_products_travelers_information'])){
		$sessionProductTravelersData = array();
		foreach($cartProducts as &$cartP){
			$productData = $cartP;
			$productData['informationComplete'] = false;
			
			$prodByCou = new ProductByCountry($db, $productData['serial_pxc']);
			$prodByCou -> getData();
			$product = new Product($db, $prodByCou -> getSerial_pro());
			$product -> getData();
			
			//GET THE MAX EXTRAS PER CARD AND SET CARD VACANCY, IF UNLIMITED, SET CARD VACANCY TO THE CURRENT AMOUNT OF PEOPLE:
			$travelersNum = $productData['adults_pod'] + $productData['children_pod'];
			$maxExtras = $product -> getMax_extras_pro();
			$adultsFree = $product -> getAdults_pro();
			$childrenFree = $product -> getChildren_pro();
			$extrasRestriction = $product -> getExtras_restricted_to_pro();
			$productData['extrasRestriction'] = $extrasRestriction;
			$seniorRestriction = $product -> getExtras_restricted_to_pro();
			$productData['seniorRestriction'] = $product -> getSenior_pro();
			$productData['extrasRestriction'] = $extrasRestriction;
			$seniorRestriction = $product -> getExtras_restricted_to_pro();
			$productData['seniorRestriction'] = $product -> getSenior_pro();
			$numCards = GlobalFunctions::getNumberOfCards($maxExtras, $productData['adults_pod'], $productData['children_pod'], $extrasRestriction);
			$cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);
			$productData['several_flights'] = $product -> getFlights_pro();
			
		$cardsDistribution = GlobalFunctions::getCardsTravelersDistribution($maxExtras, $productData['adults_pod'], $productData['children_pod'], $productData['spouse_pod'], 
														 $extrasRestriction, $adultsFree, $childrenFree);
			
			foreach($cardsDistribution as $cardD){
				$cardInfo = array();
				$extras = array();
				foreach($cardD as $traveler){
					switch($traveler){
						case 'H':
							$cardInfo['holder'] = array(
										'serial_cus' => $holder -> getSerial_cus(),
										'serial_cit' => $holder -> getSerial_cit(),
										'type_cus' => $holder -> getType_cus(),
										'document_cus' => $holder -> getDocument_cus(),
										'first_name_cus' => $holder -> getFirstname_cus(),
										'last_name_cus' => $holder -> getLastname_cus(),
										'birthdate_cus' => $holder -> getBirthdate_cus(),
										'phone1_cus' => $holder -> getPhone1_cus(),
										'phone2_cus' => $holder -> getPhone2_cus(),
										'cellphone_cus' => $holder -> getCellphone_cus(),
										'email_cus' => $holder -> getEmail_cus(),
										'address_cus' => $holder -> getAddress_cus(),
										'relative_cus' => $holder -> getRelative_cus(),
										'relative_phone_cus' => $holder -> getRelative_phone_cus(),
									);
							$holder = new Customer($db);
							break;
						case 'C':
						case 'A':
						case 'S':
						case 'F':
							$extras[] = array(
								'idExtra' => '',
								'serialCus' => '',
								'selRelationship' => '',
								'nameExtra' => '',
								'lastnameExtra' => '',
								'birthdayExtra' => '',
								'emailExtra' => '',
								);
							break;
					}
				}
				$cardInfo['extras'] = $extras;
				$productData['cards'][] = $cardInfo;
			}
			$sessionProductTravelersData[] = $productData;
		}
		$_SESSION['cart_products_travelers_information'] = $sessionProductTravelersData;
	}
	$sessionProducts = &$_SESSION['cart_products_travelers_information'];
	
	
	//Verify if all cards have their information complete:
    foreach($sessionProducts as &$productData){
    	$allInformationCompleted = true;
	    foreach ($productData['cards'] as $card){
	    	if(!$card['informationComplete']){
	    		$allInformationCompleted = false;
	    		break;
	    	}
	    }
	    if($allInformationCompleted){
	    	//If complete, then calculate the final price here:
	    	$productPrice = 0;
	    	foreach ($productData['cards'] as $card){
	    		$productPrice += $card['card_price_pod']; 
	    	}
	    	$productData['total_price_pod'] = $productPrice;
	    	$productData['informationComplete'] = true;
	    }else{
	    	$productData['informationComplete'] = false;
	    }
    }
	
	$goodToGo = false;
	foreach($sessionProducts as $prodSess){
		if($prodSess['informationComplete']){
			$goodToGo = true;
			break;
		}
	}
	unset($_SESSION['good_to_go']);
	
	
	//Selected Product Data:
	$productX = new Product($db, $sessionProducts[$productSelected]['serial_pro']);
	$productX -> getData();
	
	$adultsExtra = $productX -> getAdults_pro();
	$childrenExtra =  $productX -> getChildren_pro();
	$maxExtras = $productX -> getMax_extras_pro();
	$extrasRestriction = $productX -> getExtras_restricted_to_pro();
	$seniorRestriction = $productX -> getSenior_pro();
	
	$elderlyAgeParameter = new Parameter($db,1);
    $elderlyAgeParameter -> getData();
    $elderlyAge = $elderlyAgeParameter -> getValue_par(); 
    
    $relationshipTypeList = Extras::getAllRelationships($db);
    
	$smarty -> register('sessionProducts,goodToGo,productSelected,adultsExtra,childrenExtra,maxExtras,extrasRestriction,seniorRestriction,elderlyAge,relationshipTypeList');
	$smarty -> display();
?>