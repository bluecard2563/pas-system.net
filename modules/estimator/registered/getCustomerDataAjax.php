<?php 
/*
File: getCustomerDataAjax.php
Author: Santiago Borja
Creation Date: 18/04/2010
*/
	Request::setString('userDocument');
	Request::setString('extraId');
	
	$customerX = new Customer($db,NULL,NULL,NULL,$userDocument);
	$exists = $customerX -> getData();
	
	if(!$exists){
		echo 'false';return;
	}
	
	if($customerX->getStatus_cus() == 'INACTIVE'){
		echo 'inactive';return;
	}
	
	$txtDocumentCustomer = $customerX -> getDocument_cus();
	$hidCustomerId = $customerX -> getSerial_cus();
	$txtNameCustomer = $customerX -> getFirstname_cus();
	$txtLastnameCustomer = $customerX -> getLastname_cus();
	$txtBirthdayCustomer = $customerX -> getBirthdate_cus();
	$txtMailCustomer = $customerX -> getEmail_cus();
	
	$selCityCustomer = $customerX -> getSerial_cit();
	$selType = $customerX -> getType_cus();
	$txtPhone1Customer = $customerX -> getPhone1_cus();
	$txtPhone2Customer = $customerX -> getPhone2_cus();
	$txtCellphoneCustomer = $customerX -> getCellphone_cus();
	$txtAddress = $customerX -> getAddress_cus();
	$txtRelative = $customerX -> getRelative_cus();
	$txtPhoneRelative = $customerX -> getRelative_phone_cus();
	
	$city = new City($db,$selCityCustomer);
	$city -> getData();
	$selCountry = $city -> getSerial_cou();
	
	$selRelationship = '';
	
	$returnString = $txtDocumentCustomer.'%S%'.
					$hidCustomerId.'%S%'.
					$txtNameCustomer.'%S%'.
					$txtLastnameCustomer.'%S%'.
					$txtBirthdayCustomer.'%S%'.
					$txtMailCustomer.'%S%';
	
	if($extraId >=0){
		$returnString .= $selRelationship;
	}else{
		$returnString.= $selCityCustomer.'%S%'.
						$selType.'%S%'.
						$txtPhone1Customer.'%S%'.
						$txtPhone2Customer.'%S%'.
						$txtCellphoneCustomer.'%S%'.
						$txtAddress.'%S%'.
						$txtRelative.'%S%'.
						$txtPhoneRelative.'%S%'.
						$selCountry;
	}
	
	echo utf8_encode($returnString);
?>