<?php

    $html_message .= '<html xmlns="http://www.w3.org/1999/xhtml">';
    $html_message .= '<head>';
    $html_message .= '<meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1" />';
    $html_message .= '<link rel="shortcut icon" href="'.URL.'img/favicon.ico" />';

    $html_message .= '<link rel="stylesheet" href="'.URL.'css/screen.css" type="text/css" media="screen, projection" />';
    $html_message .= '<link rel="stylesheet" href="'.URL.'css/system_styles.css" type="text/css"/>';
    $html_message .= '<link rel="stylesheet" href="'.URL.'css/epayment.css" type="text/css"/>';
    $html_message .= '<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="'.URL.'css/ipad.css" type="text/css" />';

    $html_message .= '<title>'.$global_system_name.'</title>';
    $html_message .= '</head>';

    $html_message .= '<body class="inner_body container">';
    $html_message .= '<div class="span-24 center last contents epayment_container">';

    $html_message .= '<div class="span-24 epayment_header center last">';
    $html_message .= '<img src="'.URL.'img/epayment/logo_pas.jpg" alt="PLANETASSIST" />';
    $html_message .= '</div>';

    $html_message .= '<div class="span-24 center last line epayment_loading">';
    $html_message .= '<img src="'.URL.'img/epayment/payment_loading_150x150.gif" alt="Cargando"/>';
    $html_message .= '</div>';

    $html_message .= '<div class="span-24 center last line epayment_message">';
    $html_message .= '<label class="epayment_blue">Un momento por favor,</label><br>';
    $html_message .= '<label class="epayment_gray">Su pago est&aacute; siendo procesado</label>';

    $html_message .= '</div>';
    $html_message .= '</body>';
    $html_message .= '</html>';

    echo $html_message;

