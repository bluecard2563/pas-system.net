<?php
    
    Request::setInteger("ssl_result");
    Request::setString("ssl_result_message");
    Request::setString("ssl_txn_id");
    Request::setString("ssl_card_number");
    
    Debug::print_r($_POST); die;
    
    $report_information = array();
    $report_information['serial_usr'] = $_SESSION['serial_usr'];
    $report_information['amount_paid'] = round($_POST['txtTotalAmount'], 2);
    $report_information['payment_document'] = $_SESSION['ccnum'];
    $report_information['payment_date'] = date('d/m/Y H:i');
    $report_information['observations'] = $ssl_result_message;
    $report_information['error_code'] = $ssl_result_message;
    $report_information['txn_id'] = $ssl_txn_id;
    $report_information['card_number'] = $ssl_card_number;
    
    $error = 0;
    ErrorLog::log($db, 'MERCHANT_ACCOUNT_SERVICE_PAYMENT_ERROR', $report_information);
    
    http_redirect('modules/payments/creditCardPayments/fGlobalPayment/'.$error.'/'.$transaction_response);