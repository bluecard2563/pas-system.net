<?php
    /**
     * File: executeCustomPayment.php
     * Author: Patricio Astudillo
     * Creation Date: 08/06/2016
     * LastModified: 08/06/2016
     * Modified By: Patricio Astudillo
    **/

    Request::setString('0:type');
    Request::setInteger('1:serial_pay');
    
    include(DOCUMENT_ROOT . 'modules/estimator/epayment/transitionMessage.php');

    $merchant_params = array();
    
    if(!isset($_POST['txtTotalAmount'])):
         $_POST['txtTotalAmount'] = $_SESSION['current_sale_total'];
    endif;
    
    $exp_year = substr($_POST['selYear'], -2);
    
    $merchant_params['card_num'] = $_POST['txtCardNum'];
    $merchant_params['exp_date'] = $_POST['selMonth'].$exp_year;
    $merchant_params['sec_code'] = $_POST['txtSecCode'];
    $merchant_params['address'] = substr(utf8_decode($_POST['txtAddress']), 0, 30);
    $merchant_params['zip'] = $_POST['txtZip'];
    $merchant_params['email'] = $_POST['txtMail'];
    $merchant_params['total_fee'] = $_POST['txtTotalAmount'];
    $merchant_params['description'] = 'INDIVIDUAL CHARGE';

    $country = new Country($db, $_POST['selCountry']);
    $country->getData();

    $merchant_params['country'] = $country->getCode_cou();
    $merchant_params['company'] = $global_system_name;
    $merchant_params['first_name'] = utf8_encode($_POST['txtNameCard']);
    //$merchant_params['last_name'] = "LAST NAME";
    
    if(PROCESS_CREDIT_CARDS){
        $merchant_url = CONVERGE_PRODUCTION_URL;
    }else{
        $merchant_url = CONVERGE_TEST_URL;
    }
    
//    Debug::print_r($_REQUEST);
//    Debug::print_r($merchant_params); die;
    
    $queryString = "ssl_merchant_id=".CONVERGE_MERCHANT_ID;
    $queryString .= "&ssl_user_id=".CONVERGE_USER_ID;
    $queryString .= "&ssl_pin=".CONVERGE_PIN_ID;
    $queryString .= "&ssl_show_form=false"; //SET UP FOR OWN PAGE ERROR
    $queryString .= "&ssl_transaction_type=".CONVERGE_TRANSACTION_TYPE;
    
    $queryString .= "&ssl_card_number=".$merchant_params['card_num'];
    $queryString .= "&ssl_exp_date=".$merchant_params['exp_date'];
    $queryString .= "&ssl_card_present=N"; //CREDIT CARD WAS NOT PRESENT AT MOMENT OF CHARGE
    $queryString .= "&ssl_cvv2cvc2_indicator=1";
    $queryString .= "&ssl_cvv2cvc2=".$merchant_params['sec_code'];
    
    $queryString .= "&ssl_first_name=".$merchant_params['first_name'];
    $queryString .= "&ssl_last_name=".$merchant_params['last_name'];
    $queryString .= "&ssl_avs_zip=".$merchant_params['zip'];
    $queryString .= "&ssl_avs_address=".$merchant_params['address'];
    $queryString .= "&ssl_country=".$merchant_params['country'];
    $queryString .= "&ssl_company=".$merchant_params['company'];
    $queryString .= "&ssl_email=".$merchant_params['email'];
    
    $queryString .= "&ssl_description=".$merchant_params['description'];
    $queryString .= "&ssl_salestax=".$merchant_params['tax'];
    $queryString .= "&ssl_amount=".$merchant_params['total_fee'];
    
    //REDIRECTION VARS
    $queryString .= "&sssl_receipt_link_method=REDG";
    
    if($type == 'GLOBAL'){
        $queryString .= "&ssl_receipt_link_url=".FIXED_AMOUNT_PAYMENT;
    }else{
        $queryString .= "&ssl_receipt_link_url=".SELECT_INVOICE_PAYMENT."/$serial_pay";        
    }
    //echo $queryString; die;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $merchant_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    $result = curl_exec($ch);
    
    curl_close($ch);