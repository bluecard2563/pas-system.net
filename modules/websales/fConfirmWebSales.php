<?php
/**
 * File:  fConfirmWebSales
 * Author: Patricio Astudillo
 * Creation Date: 12-may-2011, 14:49:32
 * Description:
 * Last Modified: 12-may-2011, 14:49:32
 * Modified by:
 */

	Request::setString('0:error');
	Request::setString('1:extra_error_code');
	$paypal_sales = ErrorLog::retrieveByTitle($db, 'WEB SALES INIT 30');
	$merchant_sales = ErrorLog::retrieveByTitle($db, 'WEB SALES INIT 40');
	
	if(is_array($paypal_sales) && is_array($merchant_sales)){
		$sales_group = array_merge($paypal_sales, $merchant_sales);
	}elseif(is_array($paypal_sales)){
		$sales_group = $paypal_sales;
	}elseif(is_array($merchant_sales)){
		$sales_group = $merchant_sales;
	}
	
	if($sales_group){
		$total_pages = (int)(count($sales_group) / 10) + 1;
		$general_data = array();
		foreach($sales_group as $g){
			$web_sales_info['serial'] = $g['id'];
			$web_sales_info['date'] = $g['date'];
			if($g['title'] == 'WEB SALES INIT 30'){
				$web_sales_info['origin'] = 'Pay Pal';
			}else{
				$web_sales_info['origin'] = 'Evalon';
			}			
			
			$log = unserialize($g['log']);
			$web_sales_info['product'] = '';
			$web_sales_info['amount_of_cards'] = '';
			$web_sales_info['total_sold'] = '';
			foreach($log as $product){ //SWEEPING EACH PRODUCT
				$web_sales_info['product'] .= $product['name_pbl'].'<br />';
				$web_sales_info['amount_of_cards'] .= $product['cards_pod'].'<br />';
				$web_sales_info['total_sold'] .= number_format($product['total_price_pod'], '2').'<br />';
				
				
				$web_sales_info['holder'] = '';
				foreach($product['cards'] as $cards){ //GETTING EACH CARD INFO
					if($cards['informationComplete'] == 1){
						$web_sales_info['holder'] .= $cards['holder']['first_name_cus'].' '.$cards['holder']['last_name_cus'].'<br />';
					}					
				}
			}
			
			array_push($general_data, $web_sales_info);			
		}
	}
	
	$smarty -> register('general_data,total_pages,error');
	$smarty -> display();
?>
