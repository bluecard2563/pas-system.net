<?php
/**
 * File:  pConfirmWebSales
 * Author: Patricio Astudillo
 * Creation Date: 12-may-2011, 16:44:06
 * Description:
 * Last Modified: 12-may-2011, 16:44:06
 * Modified by:
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	
	Request::setString('action,selectedRows');
	$selectedRows = str_replace('&',',',str_replace('=on', '', $selectedRows));
	$sales_array = explode(',', $selectedRows);
	$error = 1;
	if($action == 'DELETE'){
		foreach($sales_array as $s){
			if(!ErrorLog::deleteLog($db, $s)){
				$error = 2;
			}
		}
	}else{
		//Getting the name of the logged user to log.
		$username = $_SESSION['user_name']." (Id - {$_SESSION['serial_usr']})";		
		
		foreach($sales_array as $s){
			$cart_item = ErrorLog::load($db, $s);
			if($cart_item['title'] == 'WEB SALES INIT 40'){
				$merchant_sale = true;
				$dealer_for_sale = $webSalesDealer;
			}else{
				$merchant_sale = false;
				$dealer_for_sale = $phoneSalesDealer;
			}
			
			$cart_item = unserialize($cart_item['log']);
			
			if(is_array($cart_item)){
				include(DOCUMENT_ROOT.'modules/websales/pReMakeSale.php');
			}			
		}
		
	}
	
	http_redirect('modules/websales/fConfirmWebSales/'.$error);
	
	//************************** FUNCTION DEFINITION *********************************************
	
	function escapeOnError($db, $dbFail, $title = '-', $log = '-', $extra_code_error = NULL){
		$id = ErrorLog::log($db, $title, $log);
		http_redirect('modules/websales/fConfirmWebSales/' . $dbFail . '/' . $extra_code_error);
	}
?>
