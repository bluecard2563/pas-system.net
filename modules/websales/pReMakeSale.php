<?php
/*
 * File: pReMakeSale.php
 * Author: Patricio Astudillo
 * Creation Date: 13/05/2011 11:00
 * Descripton: Modified copy from pBuyShopingCart.php in PLANETASSISTWEB repository. 
 *			   All the logic is the same, but translated into the PAS.
*/
	
	$dealerWeb = new Dealer($db, $dealer_for_sale);
	$dealerWeb -> getData();
	$webSalesCounter = new Counter($db, Dealer::getDealerUniqueCounter($db, $dealer_for_sale));
	$webSalesCounter -> getData();
	$dealersCountry = Dealer::getDealersCountrySerial($db, $dealer_for_sale);
	
	$redirectToFlightsLog = false;
	$dbFail = 0;
	$todaysDate = date("d").'/'.date("m").'/'.date("Y");
	
	$validityParam = new Parameter($db,'14');
    $validityParam -> getData();
    $validityPeriod = $validityParam -> getValue_par();
	
	foreach ($cart_item as $prod){
		if($prod['informationComplete']){
			$prodByCou = new ProductByCountry($db, $prod['serial_pxc']);
			$prodByCou -> getData();
			
			$city = new City($db);
			$destination_city = City::getGeneralCityByCountry($db, $prod['serial_cou']);
			$destination_city = $destination_city['serial_cit'];
			
			$productInTransit = $prodByCou -> getSerial_pro();
			$serialPxc = ProductByCountry::getPxCSerial($db, $dealersCountry, $productInTransit);
			$serialPbd = ProductByDealer::getPBDbyPXC($db, $dealer_for_sale, $serialPxc);
			if(!$serialPbd){
				ErrorLog::log($db, $productInTransit . ' NO PBD ' . $serialPbd, $prod);
		        escapeOnError($db, 24, 'WEB-NO PBD, CHECK CART PRODUCTS!! ' . $serialPxc);//DB FAIL LOG
		    }
			
			//For several_flights = 0
			$validUntil = date("d/m/Y", strtotime($prod['begin_date_pod'] . ' +' . $validityPeriod . ' days'));
			$prod['begin_date_pod'] = date("d/m/Y", strtotime($prod['begin_date_pod']));
			$prod['end_date_pod'] = date("d/m/Y", strtotime($prod['end_date_pod']));
			
			//Begin for each card:
			foreach ($prod['cards'] as $card){
				//Generate the Card Number:
				//FIX FOR STOCK LOOKING FOR serial_usr_web
				$_SESSION['serial_usr_web'] = $webSalesCounter -> getSerial_usr();
				$cardNumber = Stock::getNextStockNumber($db, true);
				
				if(!$cardNumber){
			        escapeOnError($db, 24, 'STOCK NOT CREATED');//DB FAIL LOG
			    }
			    
				if(!$cardNumber || $cardNumber < 1){
			    	ErrorLog::log($db, 'WEB SALES NO CARD NUMBER ' . $cardNumber, $cart_item);
			    }
			    unset($_SESSION['serial_usr_web']);
			    
			    //Calculate cost:
				$totalCost = $prod['cost_pod'];
			    foreach ($card['extras'] as $extra){
			    	$totalCost += $extra['ownCost'];
			    }
			    
			    //Code fix for serveral flights products:
				if($prod['several_flights'] == 0){
					$redirectToFlightsLog = true;
			    	$destination_city = 'NULL';
			    	$prod['end_date_pod'] = $validUntil;
			    	$cardNumber = 'N/A';
			    }
			    
			    //********************************************** THE HOLDER *************************************:
				$holder = $card['holder'];
				$customerH = new Customer($db);
				$customerH -> setDocument_cus($holder['document_cus']);
				$customerH -> setSerial_cit($holder['serial_cit']);
				$customerH -> getData(true);
				
				//This info is always saved/updated:
				$customerH -> setPhone1_cus($holder['phone1_cus']);
				$customerH -> setCellphone_cus($holder['cellphone_cus']);
				$customerH -> setAddress_cus($holder['address_cus']);
				$customerH -> setRelative_cus($holder['relative_cus']);
				$customerH -> setRelative_phone_cus($holder['relative_phone_cus']);
				$customerH -> setStatus_cus('ACTIVE');
				
				if($customerH -> existsCustomer($holder['document_cus'], NULL)){
					$customerH -> update();//Update previous entered data
				}else{
					$customerH -> setSerial_cit($holder['serial_cit']);
					$customerH -> setType_cus('PERSON');
					$customerH -> setFirstname_cus($holder['first_name_cus']);
					$customerH -> setLastname_cus($holder['last_name_cus']);
					$customerH -> setBirthdate_cus($holder['birthdate_cus']);
					$customerH -> setEmail_cus($holder['email_cus']);
					$customerH -> insert();
				}
				$customerH -> getData(true);
				$serialHolder = $customerH -> getSerial_cus();
				if(!$serialHolder) escapeOnError($db, 11, 'customer save', $customerH);//DB FAIL LOG
				
				//THE ELDERLY QUESTIONS:
				if(is_array($holder['answers'])){
					$answersHolder = $holder['answers'];
					$questionList = Question::getActiveQuestions($db, $_SESSION['serial_lang']);
					
					Answer::deleteMyAnswers($db, $serialHolder);
					foreach($questionList as $q){
						$answer = new Answer($db);
						$answer -> setSerial_cus($serialHolder);
						$answer -> setSerial_que($q['serial_que']);
						$ans = $answersHolder[$q['serial_qbl']];
		
						if($ans){
							$answer -> setYesno_ans($ans);
							if(!$answer -> insert())
								escapeOnError($db, 12, 'answer insert failed', $answer);//DB FAIL LOG
						}
					}
				}
				
				//********************************************** THE PAYMENT *************************************:
				$payment = new Payment($db);
				$payment -> setTotal_to_pay_pay($card['card_price_pod']);
			    $payment -> setTotal_payed_pay($card['card_price_pod']);
			    $payment -> setStatus_pay('PAID');
			    $payment -> setDate_pay($todaysDate);
			    
			    if($merchant_sale){
			    	$payment -> setObservation_pay('Pago realizado con tarjeta de credito');
			    }else{
			    	$payment -> setObservation_pay('Pago realizado por PayPal');
			    }
			    
			    $payment -> setExcess_amount_available_pay(0);
			    $paymentId = $payment -> insert();
			    if(!$paymentId) escapeOnError($db, 12, 'payment save', $payment);//DB FAIL LOG
				
			    //THE PAYMENT DETAIL:
				$paymentDetail = new PaymentDetail($db);
			    $paymentDetail -> setSerial_pay($paymentId);
				$paymentDetail -> setSerial_cn(NULL);
				$paymentDetail -> setSerial_usr($webSalesCounter -> getSerial_usr());
				if($merchant_sale){ //CREDIT CARD (MERCHANT ACCOUNT)
					$paymentDetail -> setType_pyf('CREDIT_CARD');
				}else{ //PAYPAL
					$paymentDetail -> setType_pyf('PAYPAL');
				}
			    $paymentDetail -> setAmount_pyf($card['card_price_pod']);
			    $paymentDetail -> setDate_pyf($todaysDate);
			    
			    if($merchant_sale){
			    	 $paymentDetail -> setDocumentNumber_pyf('***********0000');
			    }
			   
			    $paymentDetail -> setComments_pyf('');
			    $paymentDetail -> setStatus_pyf('ACTIVE');
				$paymentDetail -> setObservation_pyf(NULL);
				$serialPaymentDetail = $paymentDetail -> insert();
				if(!$serialPaymentDetail) escapeOnError($db, 13, 'payment detail save', $paymentDetail);//DB FAIL LOG
				
				//********************************************** THE INVOICE *************************************:
    			//taxes:
    			$tbc = new TaxesByProduct($db, $prod['serial_pxc']);
    			$taxes = $tbc -> getTaxesByProduct();
    			$taxesApplied = 0;
    			if($taxes){
	    			foreach ($taxes as $k => $n){
						$misc[$k]['name_tax'] = $n['name_tax'];
						$misc[$k]['tax_percentage'] = $n['percentage_tax'];
						$taxesToApply += intval($n['percentage_tax']);
					}
    			}
    			
    			//credit days:
    			$creditDay = new CreditDay($db, $dealerWeb -> getSerial_cdd());
    			$creditDay -> getData();
    			$tempDate = str_replace("/", "-", $todaysDate);
    			
    			if($creditDay -> getDays_cdd() == ''){
    				$creditDay -> setDays_cdd(0);
    			}
				$dueDate = date("d/m/Y",strtotime($tempDate . ' +' . $creditDay -> getDays_cdd() . ' days'));
			
				//Get the Document Number:
				$tempMbc = new ManagerbyCountry($db, $dealerWeb -> getSerial_mbc());
				$tempMbc -> getData();
				
				$serialDbm = DocumentByManager::retrieveSelfDBMSerial($db, $tempMbc -> getSerial_man(), 'PERSON', 'INVOICE');
				$serialDbm = $serialDbm['serial_dbm'];
				$number = DocumentByManager::retrieveNextDocumentNumber($db, $tempMbc -> getSerial_man(), 'PERSON', 'INVOICE', true);

				$invoice = new Invoice($db);
				$invoice -> setSerial_cus($serialHolder);
				$invoice -> setSerial_pay($paymentId);
				$invoice -> setSerial_dea(NULL);
				$invoice -> setSerial_dbm($serialDbm);
				$invoice -> setDate_inv($todaysDate);
				$invoice -> setDue_date_inv($dueDate);
				$invoice -> setNumber_inv($number);
				if($dealerWeb -> getType_percentage_dea() == 'COMISSION'){
					$invoice -> setComision_prcg_inv($dealerWeb -> getPercentage_dea());
					$invoice -> setDiscount_prcg_inv(NULL);
				}else{
					$invoice -> setDiscount_prcg_inv($dealerWeb -> getPercentage_dea());
					$invoice -> setComision_prcg_inv(NULL);
				}
				$invoice -> setOther_dscnt_inv(0);
				$invoice -> setComments_inv("Factura Web");
				$invoice -> setPrinted_inv('NO');
				$invoice -> setStatus_inv('PAID');
				$invoice -> setSubtotal_inv($card['card_price_pod'] - $invoice -> getOther_dscnt_inv() - $invoice -> getDiscount_prcg_inv());
				$invoice -> setTotal_inv($invoice -> getSubtotal_inv() * (1 + $taxesToApply/100));
			    $invoice -> setHas_credit_note_inv('NO');
			    $invoice -> setApplied_taxes_inv(serialize($misc));
		        $serialInv = $invoice -> insert();
		        
		        if(!$serialInv) escapeOnError($db, 16, 'invoice save', $invoice);//DB FAIL LOG
		        
				//********************************************** THE SALE *************************************:
				$sale = new Sales($db);
			    $sale -> setSerial_cus($serialHolder);
			    $sale -> setSerial_pbd($serialPbd);
			    $sale -> setSerial_cit($destination_city);
			    $sale -> setSerial_cnt($webSalesCounter -> getSerial_cnt());
			    $sale -> setSerial_inv($serialInv);
			    $sale -> setCardNumber_sal($cardNumber);
			    $sale -> setEmissionDate_sal($todaysDate);
			    $sale -> setBeginDate_sal($prod['begin_date_pod']);
				$sale -> setEndDate_sal($prod['end_date_pod']);
			    $sale -> setInDate_sal($todaysDate);
			    $sale -> setDays_sal($prod['days_pod']);
			    $sale -> setFee_sal($prod['price_pod']);
				if($merchant_sale){
					$observations = "Remake of Evalon Sale by $username";
				}else{
					$observations = "Remake of PayPal Sale by $username";
				}
			    $sale -> setObservations_sal($observations);
			    $sale -> setCost_sal($prod['cost_pod']);
			    $sale -> setFree_sal('NO');
			    $sale -> setIdErpSal_sal(NULL);
			    $sale -> setStatus_sal('REGISTERED');
			    $sale -> setType_sal('WEB');
			    $sale -> setTotal_sal($card['card_price_pod']);
			    $sale -> setStockType_sal('VIRTUAL');
			    $sale -> setDirectSale_sal($sale -> isDirectSale($webSalesCounter -> getSerial_cnt()));
			    $sale -> setChange_fee_sal(1);
			    $sale -> setTotalCost_sal($totalCost);
			    $sale -> setSerial_cur($prod['serial_cur']);
			    $sale -> setInternationalFeeStatus_sal('PAID');
		        $sale -> setInternationalFeeUsed_sal('YES');
		        $sale -> setInternationalFeeAmount_sal($totalCost);
			    $serialSale = $sale -> insert();
			    if(!$serialSale) escapeOnError($db,17, 'sale save', $sale);//DB FAIL LOG
			    
			    //********************************************** THE EXTRAS *************************************:
				foreach ($card['extras'] as $key => &$extra){
					$customerX = new Customer($db);
					$customerX -> setDocument_cus($extra['idExtra']);
					$customerX -> setSerial_cit($holder['serial_cit']);
					
					if(!$customerX -> existsCustomer($extra['idExtra'], NULL)){ //If exists we dont update
						$customerX -> setType_cus('PERSON');
						$customerX -> setFirstname_cus($extra['nameExtra']);
						$customerX -> setLastname_cus($extra['lastnameExtra']);
						$customerX -> setBirthdate_cus($extra['birthdayExtra']);
						//$customerX -> setPhone1_cus(NULL);
						//$customerX -> setPhone2_cus(NULL);
						//$customerX -> setCellphone_cus(NULL);
						$customerX -> setEmail_cus($extra['emailExtra']);
						//$customerX -> setAddress_cus($address_cus);
						//$customerX -> setRelative_cus($relative_cus);
						//$customerX -> setRelative_phone_cus($relative_phone_cus);
						//$customerX -> setVip_cus($vip_cus);
						$customerX -> setStatus_cus('ACTIVE');
						//$customerX -> setPassword_cus($password_cus);
						$serialCustomer = $customerX -> insert();
					}else{
						$customerX -> getData(true);
						$serialCustomer = $customerX -> getSerial_cus();
					}
					if(!$serialCustomer) escapeOnError($db,18, 'customer extra save', $customerX);//DB FAIL LOG
					
					//THE ELDERLY QUESTIONS:
					if(is_array($extra['answers'])){
						$answersExtra = $extra['answers'];
						$questionList = Question::getActiveQuestions($db, $_SESSION['serial_lang']);
						
						Answer::deleteMyAnswers($db, $serialCustomer);
						foreach($questionList as $q){
							$answer = new Answer($db);
							$answer -> setSerial_cus($serialCustomer);
							$answer -> setSerial_que($q['serial_que']);
							$ans = $answersExtra[$q['serial_qbl']];
			
							if($ans){
								$answer -> setYesno_ans($ans);
								if(!$answer -> insert())
									escapeOnError($db, 12, 'answer extra insert failed', $answer);//DB FAIL LOG
							}
						}
					}
					
				    if($prod['third_party_register_pro'] == 'YES'){
					    $travelerLog = new TravelerLog($db);
						$travelerLog -> setSerial_cus($serialCustomer);
						$travelerLog -> setSerial_sal($serialSale);
						$travelerLog -> setCard_number_trl(NULL);
						$travelerLog -> setStart_trl($prod['begin_date_pod']);
						$travelerLog -> setEnd_trl($prod['end_date_pod']);
						$travelerLog -> setDays_trl($prod['days_pod']);
						$travelerLog -> setSerial_cnt($webSalesCounter -> getSerial_cnt());
						$travelerLog -> setSerial_cit($destination_city);
						$travelerID = $travelerLog -> insert();
						if(!$travelerID) escapeOnError($db,19,'traveler log save', $travelerLog);//DB FAIL LOG
				    }else{
				    	$newExtra = new Extras($db);
					    $newExtra -> setSerial_sal($serialSale);
					    $newExtra -> setSerial_cus($serialCustomer);
					    $newExtra -> setSerial_trl(NULL);
					    $newExtra -> setRelationship_ext($extra['selRelationship']);
					    $newExtra -> setFee_ext($extra['ownPrice']);
					    $newExtra -> setCost_ext($extra['ownCost']);
					    $extraId = $newExtra -> insert();
					    if(!$extraId)escapeOnError($db,20, 'extra save', $newExtra);//DB FAIL LOG
				    }
				}
				
				//********************************************** THE PDF CONTRACT *************************************:
				$serial_generated_sale = $serialSale;
				
			    $display = 0;
				$urlContract = 'contract_'.$serialSale.'.pdf';
				include(DOCUMENT_ROOT.'modules/sales/pPrintSalePDF.php');
				
				//Send contract by mail:
				$misc['subject'] = 'REGISTRO DE NUEVA VENTA';
				$misc['customer'] = $customerH -> getFirstname_cus() . ' ' . $customerH -> getLastname_cus();
				$misc['cardNumber'] = $cardNumber;
				$misc['username'] = $customerH -> getEmail_cus();
				$misc['email'] = $customerH -> getEmail_cus();
				$misc['urlGeneralConditions'] = $sale -> getGeneralConditionsbySale($serialPbd, $serialSale, 1, $_SESSION['serial_lang']);
				$misc['urlContract'] = $urlContract;
				$password = $customerH -> getPassword_cus();
				
				if($password){
					$password = 'Su clave es la misma de la &uacute;ltima vez.';
				}else{
					$password = GlobalFunctions::generatePassword(6, 8, false);
					$customerH -> setPassword_cus(md5($password));
					$customerH -> update();
				}
			    $misc['pswd'] = $password;
			    
			    ErrorLog::log($db, ' sending mail ', $misc);
			    if(!GlobalFunctions::sendMail($misc, 'newClient')){
			    	escapeOnError($db, 23, 'mail not sent');//DB FAIL LOG
			    }
			}//End cards foreach
			//Finally We delete all the cart:
		    PreOrderDetail::deletePOD($db, $prod['serial_pod']);
		}
	}
?>