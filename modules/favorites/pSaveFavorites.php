<?php
/**
 * File: pSaveFavorites
 * Author: Patricio Astudillo M.
 * Creation Date: 28/02/2012 
 * Last Modified: 28/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('txtSelected');
	
	$txtSelected = str_replace('option[]=', '', $txtSelected);
	$txtSelected = explode('&', $txtSelected);
	$error_message = '1';				//success
	
	if(is_array($txtSelected)){
		Favorites::removeFavorites($db, $_SESSION['serial_usr']);
		foreach($txtSelected as $opt){
			if(!Favorites::saveFavorite($db, $_SESSION['serial_usr'], $opt)){
				$error_message = '2';	//success
			}
		}
	}else{
		$error_message = '2';			//error;
	}
	
	http_redirect('modules/favorites/fSelectFavorites/'.$error_message);
?>
