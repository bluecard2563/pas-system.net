<?php
/**
 * File: fSelectFavorites
 * Author: Patricio Astudillo M.
 * Creation Date: 27/02/2012 
 * Last Modified: 27/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('0:error');
	
	$myFavorites = Favorites::getMyFavorites($db, $_SESSION['serial_usr']);
	$exception_vals = array();
	
	if($myFavorites){
		foreach($myFavorites as $fav){
			array_push($exception_vals, $fav['serial_opt']);
		}
		$exception_vals = implode(',', $exception_vals);
	}
	
	$options = Options::getUsersAvailableFavoriteOptions($db, $_SESSION['serial_usr'], $exception_vals);
	
	
	
	$smarty -> register('options,error,myFavorites');
	$smarty -> display();
?>
