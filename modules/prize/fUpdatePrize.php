<?php 
/*
File: fUpdatePRize.php
Author: Nicolas Flores
Creation Date: 05/01/2010 18:02
Last Modified:
Modified By:
*/

Request::setInteger('selPrize');
$prize=new Prize($db,$selPrize);
$statusList=$prize->getStatusValues();
$product=new Product($db);
$productList=$product->getProducts($_SESSION['language']);

if($prize->getData()){
	$data['name_pri']=$prize->getName_pri();
	$data['value_pri']=$prize->getCost_pri();
	$data['stock_pri']=$prize->getStock_pri();
	$data['status_pri']=$prize->getStatus_pri();
	$data['type_pri']=$prize->getType_pri();
        $data['serial_cou']=$prize->getSerial_cou();
	$data['serial_pro']=$prize->getSerial_pro();
	$data['coverage_pri']=$prize->getCoverage_pri();
	$data['amount_pri']=$prize->getAmount_pri();
        $auxData=$prize->getAuxData();
        $data['serial_zon']=$auxData['serial_zon'];
	$data['serial_pri']=$selPrize;
}else{
	http_redirect('modules/prize/fSearchPrize/3');
}
$smarty->register('data,statusList,productList');
$smarty->display();
?>