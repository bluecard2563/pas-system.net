<?php 
/*
File: fSearchPrize.php
Author: Nicolas Flores.
Creation Date: 05/01/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$prize=new Prize($db);
$typeList=$prize->getTypeValues();
$zone= new  Zone($db);
$zoneList=$zone->getZones();

$smarty->register('error,typeList,zoneList');
$smarty->display();
?>