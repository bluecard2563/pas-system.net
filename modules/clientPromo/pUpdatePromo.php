<?php
/*
 * File: pUpdatePromo
 * Author: Santiago Borja
 * Creation Date: Feb 15, 2011, 1:29:15 PM
 * Description:
 * Modified By:
 * Last Modified:
 */


$cPromo = new CustomerPromo($db);
$serial_ccp = $_POST['hdnSerial_ccp'];
$cPromo -> setSerial_ccp($serial_ccp);
$cPromo -> getData();

if($_POST['selStatus'] == 'REGISTERED'){
	$cPromo -> setName_ccp(mysql_real_escape_string(addslashes($_POST['txtName'])));
	$cPromo -> setDescription_ccp(mysql_real_escape_string(addslashes($_POST['txtDescription'])));
	$cPromo -> setCupon_prefix_ccp(mysql_real_escape_string(addslashes($_POST['txtCode'])));
	$cPromo -> setBegin_date_ccp($_POST['txtBeginDate']);
	$cPromo -> setEnd_date_ccp($_POST['txtEndDate']);
	$cPromo -> setPayment_form_ccp(implode(',', $_POST['selPaymentForm']));
	$cPromo -> setType_ccp($_POST['selType']);
	$cPromo -> setScope_ccp(implode(',', $_POST['selScope']));


	if($_POST['txtDiscount']){
		$cPromo ->setDiscount_percentage_ccp($_POST['txtDiscount']);
	}else{
		$cPromo ->setDiscount_percentage_ccp('0');
	}

	if($_POST['txtOtherDiscount']){
		$cPromo ->setOther_discount_ccp($_POST['txtOtherDiscount']);
	}else{
		$cPromo ->setOther_discount_ccp('0');
	}
}

$cPromo ->setStatus_ccp($_POST['selStatus']);

if($cPromo -> update()){
	$error = '1';

	if($_POST['selAppliedProduct'] && $_POST['selCountry']){
		AppliedProductsForCustomerPromo::deleteAllMyProducts($db, $serial_ccp);

		$apProduct = new AppliedProductsForCustomerPromo($db);
		$apProduct -> setSerial_ccp($serial_ccp);
		$apProduct -> setSerial_cou($_POST['selCountry']);
		$error = '1';

		foreach($_POST['selAppliedProduct'] as $item){
			if($item){
				$apProduct -> setSerial_pxc($item);

				if(!$apProduct -> insert()){ //NO APPLIED PRODUCTS FOR CUSTOMER INSERT
					$error = '3';
					break;
				}
			}
		}
	}
}else{ //NO PROMO INSERTED
	$error = '2';
}

http_redirect('modules/clientPromo/fSearchPromo/'.$error);
?>