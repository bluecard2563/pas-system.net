<?php
/*
 * File: fNewPromo
 * Author: Patricio Astudillo
 * Creation Date: Feb 14, 2011, 10:37:41 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setString('0:error');

$countryList = Country::getOwnCountriesWithProducts($db, $_SESSION['countryList'], false);

if(is_array($countryList)){
	foreach($countryList as &$c){
		if($c['name_cou'] == 'Todos'){
			$c['name_cou'] = 'Ventas Web / Telef&oacute;nicas';
		}
	}
}

$customer_promo_types = CustomerPromo::getCustomerPromoTypes($db);
$customer_promo_payment_forms = CustomerPromo::getCustomerPromoPaymentForms($db);
$customer_promo_scopes = CustomerPromo::getCustomerPromoScope($db);

$today = date('d/m/Y');

$smarty -> register('countryList,customer_promo_types,customer_promo_payment_forms,customer_promo_scopes,today,error');
$smarty -> display();
?>