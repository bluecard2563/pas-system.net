<?php
/*
 * File: fUpdatePromo
 * Author: Patricio Astudillo
 * Creation Date: Feb 15, 2011, 11:22:07 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

$serial_ccp = $_POST['rdClientPromo'];
$cPromo = new CustomerPromo($db, $serial_ccp);

if($cPromo ->getData()){
	$cPromo = get_object_vars($cPromo);
	unset($cPromo['db']);
	$cPromo['payment_form_ccp'] = explode(',', $cPromo['payment_form_ccp']);
	$cPromo['scope_ccp'] = explode(',', $cPromo['scope_ccp']);
	$applied_products = AppliedProductsForCustomerPromo::getAllMyProducts($db, $cPromo['serial_ccp']);

	/* COUNTRY AND PRODUCT INFO */
	$pxc = new ProductByCountry($db);
	$product_list = $pxc -> getProductsByCountry($cPromo['aux']['serial_cou'], $_SESSION['serial_lang'], 'ACTIVE', 'NO');
	if(is_array($applied_products)){
		foreach($product_list as &$p){
			foreach($applied_products as $owned){
				if($p['serial_pxc'] == $owned['serial_pxc']){
					$p['checked'] = 'YES';
					break;
				}else{
					$p['checked'] = 'NO';
				}
			}
		}
	}
	$countryList = Country::getOwnCountriesWithProducts($db, $_SESSION['countryList']);
	/* COUNTRY AND PRODUCT INFO */

	/* DEFAULT INFO */
	$customer_promo_types = CustomerPromo::getCustomerPromoTypes($db);
	$customer_promo_payment_forms = CustomerPromo::getCustomerPromoPaymentForms($db);
	$customer_promo_scopes = CustomerPromo::getCustomerPromoScope($db);
	$customer_promo_status = CustomerPromo::getCustomerPromoStatus($db);
	
	switch($cPromo['status_ccp']){
		case 'REGISTERED':
			foreach($customer_promo_status as $key=>&$item){
				if($item == 'ACTIVE' || $item == 'EXPIRED'){
					unset($customer_promo_status[$key]);
				}
			}
			break;
		case 'ACTIVE':
			foreach($customer_promo_status as $key=>&$item){
				if($item == 'REGISTERED' || $item == 'EXPIRED'){
					unset($customer_promo_status[$key]);
				}
			}
			break;
		case 'EXPIRED':
			foreach($customer_promo_status as $key=>&$item){
				if($item == 'ACTIVE' || $item == 'CANCELED' || $item == 'REGISTERED'){
					unset($customer_promo_status[$key]);
				}
			}
			break;
		case 'CANCELED':
			foreach($customer_promo_status as $key=>&$item){
				if($item == 'ACTIVE' || $item == 'EXPIRED' || $item == 'REGISTERED'){
					unset($customer_promo_status[$key]);
				}
			}
			break;
	}
	
	$today = date('d/m/Y');
	/* DEFAULT INFO */

	$smarty -> register('countryList,customer_promo_types,customer_promo_payment_forms,customer_promo_scopes,today,error');
	$smarty -> register('product_list,cPromo,customer_promo_status');
	$smarty -> display();
}else{
	http_redirect('modules/clientPromo/fSearchPromo/no_log');
}
?>
