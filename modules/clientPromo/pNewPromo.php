<?php
/*
 * File: pNewPromo
 * Author: Patricio Astudillo
 * Creation Date: Feb 14, 2011, 2:50:44 PM
 * Description:
 * Modified By:
 * Last Modified:
 */

$cPromo = new CustomerPromo($db);
$cPromo -> setName_ccp(mysql_real_escape_string(addslashes($_POST['txtName'])));
$cPromo -> setDescription_ccp(mysql_real_escape_string(addslashes($_POST['txtDescription'])));
$cPromo -> setCupon_prefix_ccp(mysql_real_escape_string(addslashes($_POST['txtCode'])));
$cPromo -> setBegin_date_ccp($_POST['txtBeginDate']);
$cPromo -> setEnd_date_ccp($_POST['txtEndDate']);
$cPromo ->setPayment_form_ccp(implode(',', $_POST['selPaymentForm']));
$cPromo ->setType_ccp($_POST['selType']);
$cPromo ->setScope_ccp(implode(',', $_POST['selScope']));

if($_POST['txtDiscount']){
	$cPromo ->setDiscount_percentage_ccp($_POST['txtDiscount']);
}else{
	$cPromo ->setDiscount_percentage_ccp('0');
}
if($_POST['txtOtherDiscount']){
	$cPromo ->setOther_discount_ccp($_POST['txtOtherDiscount']);
}else{
	$cPromo ->setOther_discount_ccp('0');
}

$serial_ccp = $cPromo -> insert();

if($serial_ccp){
	$apProduct = new AppliedProductsForCustomerPromo($db);
	$apProduct ->setSerial_ccp($serial_ccp);
	$apProduct ->setSerial_cou($_POST['selCountry']);		$error = '1';

	foreach($_POST['selAppliedProduct'] as $item){
		if($item){
			$apProduct ->setSerial_pxc($item);

			if($apProduct ->insert()){//SUCCESS
				$error = '1';
			}else{ //NO APPLIED PRODUCTS FOR CUSTOMER INSERT
				$error = '3';
				break;
			}
		}
	}	
}else{ //NO PROMO INSERTED
	$error = '2';
}

http_redirect('modules/clientPromo/fNewPromo/'.$error);
?>