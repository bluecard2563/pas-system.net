<?php
/*
 * File: fSearchPromo
 * Author: Patricio Astudillo
 * Creation Date: Feb 15, 2011, 10:29:20 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setString('0:error');

$countryList = Country::getOwnCountriesWithProducts($db, $_SESSION['countryList']);
$customer_promo_status = CustomerPromo::getCustomerPromoStatus($db);

$smarty -> register('countryList,customer_promo_status,error');
$smarty -> display();
?>
