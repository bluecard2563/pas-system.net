<?php 
/*
File: fUpdateBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 16:33
Last Modified:
Modified By:
*/

Request::setInteger('hdnBenefitID');
$language=new Language($db);
$languageList=$language->getRemainingBenefitLanguages($hdnBenefitID);

//Categories
$benefits_categories=new benefitsCategories($db);
$benefitsCategoriesList=$benefits_categories->getBenefitsCategoriesList();

$benefit=new Benefit($db,$hdnBenefitID);
//$benefitCategory = $benefit ->getSerial_bcat($hdnBenefitID);
$statusList=$benefit->getStatusValues();

if($benefit->getData()){
	$data['status_ben']=$benefit->getStatus_ben();
        $data['serial_bcat']=$benefit->getSerial_bcat();
	$data['serial_ben']=$hdnBenefitID;

        $bbl=new BenefitbyLanguage($db);
        $translations=$bbl->getTranslations($hdnBenefitID);                
       // Debug::print_r($data); die();
        $titles=array('#','Traducci&oacute;n','Idioma','Prioridad','Actualizar');
}else{
	http_redirect('modules/product/benefits/fSearchBenefit/3');
}

$smarty->register('data,statusList,titles,translations,languageList,benefitsCategoriesList');
$smarty->display();
?>