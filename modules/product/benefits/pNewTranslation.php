<?php 
/*
File: pNewTranslation.php
Author: Patricio Astudillo M.
Creation Date: 26/01/2010 16:21 pm
Last Modified:
Modified By:
*/

$bbl=new BenefitbyLanguage($db);
$bbl->setSerial_ben($_POST['hdnBenefitID']);
$bbl->setSerial_lang($_POST['selLanguage']);
$bbl->setDescription_bbl($_POST['txtDescription']);

if($bbl->insert()){
    http_redirect('modules/product/benefits/fSearchBenefit/1');
}else{
    http_redirect('modules/product/benefits/fSearchBenefit/5');
}
?>