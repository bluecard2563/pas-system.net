<?php 
/*
File: fNewBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 11:43
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$benefit=new Benefit($db);
$statusList=$benefit->getStatusValues();

//Categories
$benefits_categories=new benefitsCategories($db);
$benefitsCategoriesList=$benefits_categories->getBenefitsCategoriesList();

$smarty->register('error,statusList,benefitsCategoriesList');
$smarty->display();
?>