<?php 
/*
File: pUpdateBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 16:48
Last Modified:
Modified By:
*/

Request::setInteger('hdnBenefitID');

$benefit=new Benefit($db,$hdnBenefitID);

if($benefit->getData()){
    $benefit->setStatus_ben($_POST['rdStatus']);
    $benefit->setSerial_bcat($_POST['selCategories']);
    
    if($benefit->update()){
        http_redirect('modules/product/benefits/fSearchBenefit/2');
    }else{
        http_redirect('modules/product/benefits/fSearchBenefit/3');
    }
}
?>