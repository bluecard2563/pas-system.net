<?php 
/*
File: pNewBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 11:59 am
Last Modified:
Modified By:
*/

$serial_lang = $_SESSION['serial_lang'];

$benefit=new Benefit($db);
$benefit->setStatus_ben('ACTIVE');
$benefit->setSerial_bcat($_POST['selCategories']);
$benefit->setWeight_ben($_POST['txtWeightBenefit']);
$serial_ben=$benefit->insert();

if($serial_ben){
    $bbl=new BenefitbyLanguage($db);
    $bbl->setSerial_ben($serial_ben);
    $bbl->setSerial_lang($serial_lang);
    $bbl->setDescription_bbl($_POST['txtDescBenefit']);

    if($bbl->insert()){
        http_redirect('modules/product/benefits/fNewBenefit/1');
    }else{
        http_redirect('modules/product/benefits/fNewBenefit/3');
    }
}else{
    http_redirect('modules/product/benefits/fNewBenefit/2');
}
?>