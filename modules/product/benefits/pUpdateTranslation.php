<?php
/*
File: pUpdateTranslation.php
Author: Patricio Astudillo
Creation Date: 26/01/2010 15:48pm
LastModified: 05/05/2010
Modified By: Santiago Arellano
*/

$bbl=new BenefitbyLanguage($db,$_POST['serial_bbl']);
$ben=new Benefit($db,$_POST['serial_ben']);

if($bbl->getData() && $ben->getData()){
    $bbl->setDescription_bbl($_POST['txtBenefit']);
    $ben->setWeight_ben($_POST['txtWeightBenefit']);

    if($bbl->update() && $ben->updateWeight()){
        http_redirect('modules/product/benefits/fSearchBenefit/2');
    }else{
        http_redirect('modules/product/benefits/fSearchBenefit/4');
    }

}else{
    http_redirect('modules/product/benefits/fSearchBenefit/4');
}





?>
