<?php
/*
File: pUpdateTranslation.php
Author: Patricio Astudillo
Creation Date: 26/01/2010 15:48pm
LastModified: 26/01/2010
Modified By: Patricio Astudillo
*/

$pTypeByLanguage=new PtypebyLanguage($db);
$pTypeByLanguage->setSerial_ptl($_POST['serial_ptl']);
$pTypeByLanguage->setName_ptl($_POST['diaName_tpp']);
$pTypeByLanguage->setDescription_ptl($_POST['txtDescription_tpp']);

   
    if($pTypeByLanguage->update()){
        http_redirect('modules/product/productType/fSearchProductTypeTranslation/1');
    }else{
        http_redirect('modules/product/productType/fSearchProductTypeTranslation/2');
    }

?>
