<?php session_start();
/*
File: fUpdateProductType
Author: Pablo Puente
Creation date: 22/12/2009
Last modified: 22/12/2009
Modified By: David Bergmann
*/

Request::setInteger('0:error');

$serial_tpp = $_POST['hdnSerial_tpp'];
$productType = new ProductType($db);
$code_lang=$_SESSION['language'];

if($error==0) {//viene del buscar la moneda y coge el user que se ingreso a buscar	
	if($serial_tpp){	
		$productType -> setSerial_tpp($serial_tpp);
		$data=$productType -> getInfoProductType($code_lang);

		$benefit = new Benefit($db);
		$condition = new Condition($db);
		$currency = new Currency($db);
		$restriction = new Restriction($db);
		$generalCondition = new GeneralCondition($db);
		
		$benefits = $benefit -> getActiveBenefits($code_lang);
		$conditions = $condition -> getConditions($code_lang);
		$restrictions = $restriction -> getRestrictions($code_lang);
		$currencies = $currency -> getCurrency();
		$generalConditions = $generalCondition -> getGeneralConditions($_SESSION['serial_lang']);

		$benefitsByProductType = new BenefitsByProductType($db); 
		$benefitsByProductType -> setSerial_tpp($serial_tpp);
		$benefitsByType = $benefitsByProductType -> getBenefits();

		$conditionsByProductType = new ConditionsByProductType($db);
		$conditionsByProductType -> setSerial_tpp($serial_tpp);
		$conditionsByType = $conditionsByProductType -> getConditions();

		$extraTypes = ProductType::getExtraTypes($db);

		/** Dialog Benefit ********************************/
		$benefit=new Benefit($db);
		$statusList=$benefit->getStatusValues();
		$smarty->register('statusList');
		/**************************************************/
	}else{
		http_redirect('modules/product/productType/fSearchProductType/2');
	}
}

//Debug::print_r($data);
$smarty -> register('user,data,benefits,conditions,currencies,restrictions,generalConditions,benefitsByType,conditionsByType,extraTypes,global_extraTypes');
$smarty -> display();
?>