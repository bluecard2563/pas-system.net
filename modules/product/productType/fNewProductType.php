<?php
/*
File: fNewProductType.php
Author:Miguel Ponce
Modified date:24/12/2009
Last Modified: 31/08/2010
Modified By:
*/

$productType = new ProductType($db);
$benefit = new Benefit($db);
$condition = new Condition($db);
$currency =	new Currency($db);
$restriction = new Restriction($db);
$generalCondition = new GeneralCondition($db);

$code_lang=$_SESSION['language'];

$benefits = $benefit->getActiveBenefits($code_lang);
$conditions = $condition->getConditions($code_lang);
$restrictions = $restriction->getRestrictions($code_lang);

$currencies = $currency->getCurrency();
$generalConditions = $generalCondition->getGeneralConditions($_SESSION['serial_lang']);
$extraTypes = ProductType::getExtraTypes($db);

/** Dialog Benefit ********************************/
$benefit=new Benefit($db);
$statusList=$benefit->getStatusValues();
$smarty->register('statusList');
/**************************************************/

$serial_lang = $_SESSION['serial_lang'];

Request::setInteger('0:error');
$smarty->register('error,benefits,conditions,currencies,restrictions,generalConditions,extraTypes,global_extraTypes,serial_lang');
$smarty->display();
?>