<?php //session_start();
/*
File: pNewProductType.class
Author:Miguel Ponce
Modified date:22/12/2009
Last modified:02/08/2010
Modified By: David Bergmann
*/

$productType = new ProductType($db);
$benefitsByProductType = new BenefitsByProductType($db);
$conditionsByProductType = new ConditionsByProductType($db);
$serial_lang = $_SESSION['serial_lang'];
$ptypebyLanguage=new PtypebyLanguage($db);

$productType->setFlights_tpp($_POST['flights_tpp']);
$productType->setSpouse_tpp('NO');
$productType->setHas_comision_tpp('NO');
$productType->setSchengen_tpp('NO');
$productType->setIn_web_tpp('NO');
$productType->setRelative_tpp('NO');
$productType->setThird_party_register_tpp('NO');
$productType->setMasive_tpp('NO');
$productType->setEmission_country_tpp('NO');
$productType->setResidence_country_tpp('NO');
$productType->setSenior_tpp('NO');
$productType->setDestination_restricted_tpp('NO');
$productType->setExtras_restricted_to_tpp('');


if($_POST['max_extras_tpp']!=''){
	$productType->setMax_extras_tpp($_POST['max_extras_tpp']);
}
if($_POST['children_tpp']!=''){
	$productType->setChildren_tpp($_POST['children_tpp']);
}
if($_POST['adults_tpp']!=''){
	$productType->setAdults_tpp($_POST['adults_tpp']);
}
if(isset($_POST['spouse_tpp'])) {
	$productType->setSpouse_tpp($_POST['spouse_tpp']);
}
if(isset($_POST['has_comision_tpp'])){
	$productType->setHas_comision_tpp($_POST['has_comision_tpp']);
}
if(isset($_POST['schengen_tpp'])) {
	$productType->setSchengen_tpp($_POST['schengen_tpp']);
}
if(isset($_POST['in_web_tpp'])) {
	$productType->setIn_web_tpp($_POST['in_web_tpp']);
}
if(isset($_POST['relative_tpp'])) {
	$productType->setRelative_tpp($_POST['relative_tpp']);
}
if(isset($_POST['third_party_register_tpp'])) {
	$productType->setThird_party_register_tpp($_POST['third_party_register_tpp']);
}
if(isset($_POST['masive_tpp'])) {
	$productType->setMasive_tpp($_POST['masive_tpp']);
}
if(isset($_POST['emission_country_tpp'])) {
	$productType->setEmission_country_tpp($_POST['emission_country_tpp']);
}
if(isset($_POST['residence_country_tpp'])) {
	$productType->setResidence_country_tpp($_POST['residence_country_tpp']);
}
if(isset($_POST['senior_tpp'])) {
	$productType->setSenior_tpp($_POST['senior_tpp']);
}
if(isset($_POST['destination_restricted_tpp'])) {
	$productType->setDestination_restricted_tpp($_POST['destination_restricted_tpp']);
}
if(isset($_POST['selExtrasRestricted'])) {
	$productType->setExtras_restricted_to_tpp($_POST['selExtrasRestricted']);
}

$error=1;
if($_FILES['image_tpp']['name']){
	if(($_FILES["image_tpp"]["type"] == "image/gif") || ($_FILES["image_tpp"]["type"] == "image/jpeg") || ($_FILES["image_tpp"]["type"] == "image/pjpeg")){
		//$prefijo = substr(md5(uniqid(rand())),0,6);
		$image_tpp = $_FILES["image_tpp"]['name'];
		if(copy($_FILES['image_tpp']['tmp_name'], DOCUMENT_ROOT.'img/products/'.$image_tpp)){
			$productType->setImage_tpp($image_tpp);
			$serial_tpp=$productType->insert();
			$copioArchivo=1;
		} else {
			$copioArchivo=0;
		}
	} else {
		$copioArchivo=2;
	}
} else {
	$copioArchivo=1;
}

if($serial_tpp){//Si la inserci�n es exitosa
	$ptypebyLanguage -> setSerial_lang($serial_lang);
	$ptypebyLanguage -> setSerial_tpp($serial_tpp);
	$ptypebyLanguage -> setName_ptl($_POST['txtName_tpp']);
	$ptypebyLanguage -> setDescription_ptl($_POST['txtDescription_tpp']);
	$serial_ptl=$ptypebyLanguage -> insert();

	if($serial_ptl){
		if(is_array($_POST['benefit'])){
			foreach($_POST['benefit'] as $key => $benefitItem){
				if($benefitItem['status_bpt']=="on"){
					$benefitsByProductType -> setSerial_ben($key);
					$benefitsByProductType -> setSerial_rst($benefitItem['serial_rst']);
					$benefitsByProductType -> setSerial_tpp($serial_tpp);
					$benefitsByProductType -> setSerial_con($benefitItem['serial_con']);
					$benefitsByProductType -> setPrice_bpt($benefitItem['price_bpt']);
					$benefitsByProductType -> setStatus_bpt('ACTIVE');
					$benefitsByProductType -> setRestriction_price_bpt($benefitItem['restriction_price_bpt']);
					$benefitsByProductType -> setSerial_cur(1);
					$benefitsByProductType -> insert();
				}
			}
		}
		if($_POST['generalCondition']){
			$conditionsByProductType->setSerial_tpp($serial_tpp);
			$conditionsByProductType->setSerial_gcn($_POST['generalCondition']);
			$conditionsByProductType -> insert();
			$error=1;
		}
	}
} else {
	$error=2;
}
http_redirect('modules/product/productType/fNewProductType/'.$error);
?>