<?php session_start();
    $code_lang=$_SESSION['language'];
	Request::setInteger('0:message');
	$product = new Product($db);
	$productList = $product->getProducts($code_lang);
	//die(Debug::print_r($productList));
	$languages = Language::getAllLanguages($db);
	$pro_count = count($productList);
	
	if($pro_count > 0){
		$pages = (int)($pro_count / 10 ) + 1;
	}

	$smarty -> register('productList,message,code_lang,languages,pages');
	$smarty -> display();
?>
