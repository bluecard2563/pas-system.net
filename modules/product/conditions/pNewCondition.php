<?php 
/*
File: pNewRestriction.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 14:21 pm
Last Modified:
Modified By:
*/

$serial_lang = $_SESSION['serial_lang'];

$condition=new Condition($db);
$condition->setAlias_con($_POST['txtAlias']);
$serial_con=$condition->insert();

if($serial_con){
    $cbl=new ConditionByLanguage($db);
    $cbl->setSerial_con($serial_con);
    $cbl->setSerial_lang($serial_lang);
    $cbl->setDescription_cbl($_POST['txtDescription']);

    if($cbl->insert()){
        http_redirect('modules/product/conditions/fNewCondition/1');
    }else{
        http_redirect('modules/product/conditions/fNewCondition/3');
    }
}else{
    http_redirect('modules/product/conditions/fNewCondition/2');
}
?>