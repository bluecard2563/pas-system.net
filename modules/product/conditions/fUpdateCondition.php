<?php 
/*
File: fUpdateBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 16:33
Last Modified:
Modified By:
*/
Request::setInteger('hdnConditionID');
$language=new Language($db);
$languageList=$language->getRemainingConditionLanguages($hdnConditionID);
$condition=new Condition($db,$hdnConditionID);

if($condition->getData()){
	$dataO['serial_con']=$hdnConditionID;
	$data=$condition->getTranslations($hdnConditionID);
	$itemCount=sizeof($data);
	$titles=array('#','Traducci&oacute;n','Idioma','Actualizar');
}else{
	http_redirect('modules/product/conditions/fSearchCondition/3');
}

$smarty->register('data,dataO,titles,languageList,itemCount');
$smarty->display();
?>