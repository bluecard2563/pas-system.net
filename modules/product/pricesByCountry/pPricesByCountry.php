<?php
/*
File: pPricesByCountry.php
Author: Edwin Salvador
Creation Date: 09/03/2010
Last Modified:
Modified By:
*/
//Debug::print_r($_POST);
$error = 2;
$band = 0;
Request::setInteger('serial_pro');
Request::setInteger('serial_cou');
Request::setInteger('serial_pxc');
$price_by_product = $_POST['price_by_product'];

if($serial_pxc){
    $pricebyProductbyCountry = new PriceByProductByCountry($db);

    $pricebyProductbyCountry->setSerial_pxc($serial_pxc);
    $pricebyProductbyCountry->delete();
    if(is_array($price_by_product)){
        foreach($price_by_product as $data){
            $pricebyProductbyCountry->setDuration_ppc($data['duration_ppc']);
            if(isset($data['price_ppc'])){
                $pricebyProductbyCountry->setPrice_ppc($data['price_ppc']);
            }else{
                $pricebyProductbyCountry->setMin_ppc($data['min_ppc']);
                $pricebyProductbyCountry->setMax_ppc($data['max_ppc']);
            }
            $pricebyProductbyCountry->setCost_ppc($data['cost_ppc']);

            if($pricebyProductbyCountry->insert()){
                $error = 2;
            }else{
                $error =3;
            }

        }
    }
}else{
    $error = 3;
}

http_redirect('modules/product/pricesByCountry/fSearchPricesByCountry/'.$error);

?>
