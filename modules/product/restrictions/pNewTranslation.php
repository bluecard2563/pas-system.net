<?php 
/*
File: pNewTranslation.php
Author: Patricio Astudillo M.
Creation Date: 25/01/2010 16:21 pm
Last Modified:
Modified By:
*/

$rbl=new RestrictionByLanguage($db);
$rbl->setSerial_rst($_POST['hdnRestrictionID']);
$rbl->setSerial_lang($_POST['selLanguage']);
$rbl->setDescription_rbl($_POST['txtDescription']);

if($rbl->insert()){
    http_redirect('modules/product/restrictions/fSearchRestriction/1');
}else{
    http_redirect('modules/product/restrictions/fSearchRestriction/5');
}

?>