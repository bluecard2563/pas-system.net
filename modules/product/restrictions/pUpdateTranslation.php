<?php
/*
File: pUpdateTranslation.php
Author: Patricio Astudillo
Creation Date: 26/01/2010 15:48pm
LastModified: 26/01/2010
Modified By: Patricio Astudillo
*/

$rbl=new RestrictionByLanguage($db,$_POST['serial_rbl']);
if($rbl->getData()){
    $rbl->setDescription_rbl($_POST['txtRestriction']);
    if($rbl->update()){
        http_redirect('modules/product/restrictions/fSearchRestriction/2');
    }else{
        http_redirect('modules/product/restrictions/fSearchRestriction/4');
    }
}else{
    http_redirect('modules/product/restrictions/fSearchRestriction/4');
}


?>
