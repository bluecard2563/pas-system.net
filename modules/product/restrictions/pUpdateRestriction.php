<?php 
/*
File: pUpdateBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 16:48
Last Modified:
Modified By:
*/

Request::setInteger('hdnRestrictionID');
$restriction=new Restriction($db,$hdnRestrictionID);

if($restriction->getData()){
	$restriction->setStatus_rst($_POST['rdStatus']);
	
	if($restriction->update()){
            http_redirect('modules/product/restrictions/fSearchRestriction/2');
	}else{
            http_redirect('modules/product/restrictions/fSearchRestriction/4');
	}
}
?>