<?php 
/*
File: fUpdateBenefit.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 16:33
Last Modified:
Modified By:
*/
Request::setInteger('hdnRestrictionID');
$language=new Language($db);
$languageList=$language->getRemainingRestrictionLanguages($hdnRestrictionID);

$restriction=new Restriction($db,$hdnRestrictionID);
$statusList=$restriction->getStatusList();

if($restriction->getData()){
	$dataO['status_rst']=$restriction->getStatus_rst();
	$dataO['serial_rst']=$hdnRestrictionID;

        $data=$restriction->getTranslations($hdnRestrictionID);
        $itemCount=sizeof($data);
        $titles=array('#','Traducci&oacute;n','Idioma','Actualizar');
}else{
	http_redirect('modules/product/restrictions/fSearchRestriction/3');
}

$smarty->register('data,dataO,statusList,titles,languageList,itemCount');
$smarty->display();
?>