<?php
/**
 * File: pRegisterDestinations
 * Author: Patricio Astudillo
 * Creation Date: 19-feb-2013
 * Last Modified: 19-feb-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('hdnSerial_pxc');
	Request::setString('txtValues');
	
	
	$new_destinations = split(',', $txtValues);
	$error = 2;
	
	if(is_array($new_destinations) && count($new_destinations) > 0){
		AllowedDestinations::removeAllAccess($db, $hdnSerial_pxc);
		//REMOVE LAST ELEMENT DUE TO BLANK ALWAYS ARRIVES TO ARRAY
		if(AllowedDestinations::enableAccess($db, $hdnSerial_pxc, FALSE, array_slice($new_destinations, 0, count($new_destinations) - 1))){
			$error = 1;
		}else{
			$error = 3;
		}
	}
	
	http_redirect('modules/product/productByCountry/fManageAllowedDestinations/'.$error);
?>
