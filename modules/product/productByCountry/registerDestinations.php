<?php
/**
 * File: registerDestinations
 * Author: Patricio Astudillo
 * Creation Date: 19-feb-2013
 * Last Modified: 19-feb-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('selCountry');
	Request::setInteger('selProduct');
	
	$serial_pxc = ProductByCountry::getPxCSerial($db, $selCountry, $selProduct);
	$allowed_destinations = AllowedDestinations::getCurrentDestinations($db, $serial_pxc);
	$completeDestinations = Country::getAllCountries($db, true);

	$smarty->register('allowed_destinations,completeDestinations,serial_pxc');
	$smarty->display();
?>
