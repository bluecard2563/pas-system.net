<?php
/*
  File: fProductByCountry.php
  Author: Edwin Salvador
  Creation Date: 17/02/2010
  Last Modified:
  Modified By:
 */

set_time_limit(36000);
ini_set('memory_limit', '512M');

$error = '';
$band = 0;

if ($_POST['serial_pro'] != '') {
	$pbc = new ProductByCountry($db);
	$serial_pro = $_POST['serial_pro'];
	$pbc->setSerial_pro($serial_pro);

	$countries = $_POST['countries'];

	foreach ($countries as $c) {
		$assigned_dealers = '';
		//PRODUCT BY COUNTRY SECTION
		$serial_cou = $c['selCountry'];
		$pbc->setSerial_cou($c['selCountry']);
		$pbc->setAditional_day_pxc($c['txtAditional']);
		if ($c['txtSpouse'] != '-1') {
			$pbc->setPercentage_spouse_pxc($c['txtSpouse']);
		} else {
			$pbc->setPercentage_spouse_pxc(NULL);
		}
		if ($c['txtExtras'] != '-1') {
			$pbc->setPercentage_extras_pxc($c['txtExtras']);
		} else {
			$pbc->setPercentage_extras_pxc(NULL);
		}
		if ($c['txtChildren'] != '-1') {
			$pbc->setPercentage_children_pxc($c['txtChildren']);
		} else {
			$pbc->setPercentage_children_pxc(NULL);
		}
		$pbc->setStatus_pxc($c['countryStatus']);

		if ($c['serial_pbc'] == '' || $c['serial_pbc'] == NULL) {//IS A NEW ASSIGNED COUNTRY
			if ($pbc->productByCountryExists() == 0) {//VERIFY IF THE ASSIGNED COUNTRY EXISTS IN THE DB
				$serial_pbc = $pbc->insert();
			}
		} else {//UPDATE THE DATA FOR THE ASSIGNED COUNTRY
			$serial_pbc = $c['serial_pbc'];
			$pbc->setSerial_pxc($serial_pbc);
			if (!$pbc->update()) {
				$band = 1;
			}
			$pbd = new ProductByDealer($db);
			$assigned_dealers = $pbd->getDealerProductByCountry($serial_pbc); //GET ALL THE ASSIGNED DEALERS TO THIS COUNTRY TO ACTIVATE OR DEACTIVATE THE DEALER
			$pbc->setSerial_pxc(NULL);
		}
		$taxes = $c['taxes'];
		$to_assign_dealers = $c['dealersTo'];
		//TAXES BY PRODUCT BY COUNTRY SECTION
		if ($serial_pbc != '' && $serial_pbc !== false) {//IF THE COUNTRY WAS INSERTED OR IS AN UPDATED COUNTRY INSERT THE TAXES
			$tbp = new TaxesByProduct($db);
			$tbp->setSerial_pxc($serial_pbc);
			$tbp->delete(); //DELETE ALL THE TAXES ASSIGNED TO THIS COUNTRY TO UPDATE THEM
			if (is_array($taxes)) {
				foreach ($taxes as $t) {
					$tbp->setSerial_tax($t);
					if ($tbp->taxByProductExists() == 0) {//VERIFY IF THE TAX TO THIS COUNTRY ALREADY EXISTS
						if (!$tbp->insert()) {
							$band = 1;
						}
					}
				}
			}
			//DEALER BY PRODUCT BY COUNTRY SECTION
			if (is_array($to_assign_dealers)) {//IF THERE ARE TO ASSIGN DEALERS
				$pbd = new ProductByDealer($db);
				foreach ($to_assign_dealers as $d) {//INSERT THE ASSIGNED DEALERS
					$pbd->setSerial_dea($d);
					$pbd->setSerial_pxc($serial_pbc);
					if (isset($c['countryStatus'])) {//IF THE COUNTRY WAS UPDATED SET THE STATUS
						$pbd->setStatus_pbd($c['countryStatus']);
					} else {//IF IS A NEW COUNTRY ALL THE ASSIGNED COUNTRIES ARE ACTIVE
						$pbd->setStatus_pbd('ACTIVE');
					}
					$serial_pbd = $pbd->productByDealerExists(); //VERIFY IF THE PRODUCT BY DEALER EXISTS

					if ($serial_pbd == 0) {//IF NOT EXISTS THE INSERT
						if (!$pbd->insert()) {
							$band = 1; //BAND TO CHECK IF THERE WAS AN ERROR
						}
					} else {//IF THE PRODUCT BY DEALER EXISTS UPDATE IT
						if ($c['countryStatus'] == 'INACTIVE') {//CHECK IF THE COUNTRY WAS DISABLED TO DISABLED ALL THE ASSIGNED DEALERS TO THIS COUNTRY
							$pbd->setSerial_pxc($serial_pbc); //SET THE SERIAL OF THE COUNTRY
							$pbd->setSerial_pbd(NULL);
							$pbd->setStatus_pbd('INACTIVE');
						} else {//IF THE COUNTRY IS ACTIVE THEN ACTIVATE THE DEALERS THAT WAS SELECTED
							$pbd->setSerial_pbd($serial_pbd);
							$pbd->setStatus_pbd('ACTIVE');
						}

						if (!$pbd->update()) {
							$band = 1;
						}
					}
				}
				if (is_array($assigned_dealers)) {//IF THERE ARE ASSIGNED DEALERS IN THE DB
					foreach ($assigned_dealers as $ad) {
						if (!in_array($ad, $to_assign_dealers)) {//CHECK IF AN ASSGNED DEALER IS NOT IN THE TO ASSIGN DEALERS LIST THEN DISABLE THE DEALER BY PRODUCT
							$pbd->setSerial_dea($ad);
							$pbd->setSerial_pxc($serial_pbc);
							$serial_pbd = $pbd->productByDealerExists();
							//$pbd->setSerial_pxc(NULL);
							$pbd->setSerial_pbd($serial_pbd);
							$pbd->setStatus_pbd('INACTIVE');
							if (!$pbd->updateStatus()) {//echo 'error dea up';die;
								$band = 1;
							}
						}
					}
				}
			} else {//IF THERE ARE NO TO ASSIGN DEALERS THE DEACTIVE ALL THE DEALERS BY PRODUCT BY COUNTRY
				$pbd = new ProductByDealer($db);
				$pbd->setSerial_pxc($serial_pbc);
				$pbd->setSerial_pbd(NULL);
				$pbd->setStatus_pbd('INACTIVE');
				if (!$pbd->update()) {
					$band = 1;
				}
			}
		}
		if ($band == 1) {//THERE WAS AN ERROR IN THE PROCESS
			$error = 3;
		} else {
			$error = 1; //EVERYthing WORKED OK

			AllowedDestinations::enableAccess($db, $serial_pbc);
		}
	}
} else {
	$error = 3;
}


if ($_POST['btnGuardarSalir'] || $error != 1) {
	http_redirect('modules/product/productByCountry/fSearchProductByCountry/' . $error);
} elseif ($_POST['btnInsertar']) {
	http_redirect('modules/product/pricesByCountry/fSearchPricesByCountry/' . $error . '/' . $serial_cou . '/' . $serial_pro);
}
?>
