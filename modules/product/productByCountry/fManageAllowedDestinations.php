<?php
/**
 * File: fManageAllowedDestinations
 * Author: Patricio Astudillo
 * Creation Date: 19-feb-2013
 * Last Modified: 19-feb-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');
    Request::setInteger('1:serial_cou');//THE LAST COUNTRY ASSIGNED
    Request::setInteger('2:serial_pro');//THE PRODUCT

    $code_lang=$_SESSION['language'];
    $zone = new Zone($db);
    $zoneList = $zone->getZones();
    
    //WHEN THE USER JUST ASSIGNED THE COUNTRIES
    if($serial_cou && $serial_pro){
        $countryOb = new Country($db,$serial_cou);
        $countryOb->getData();

        $country = get_object_vars($countryOb);
        $serial_zon = $country['serial_zon'];
        $countryList = $countryOb->getCountriesByZone($serial_zon, $_SESSION['countryList']);

        $product = new Product($db);
        $productList = $product->getProductsByCountry($serial_cou, $_SESSION['serial_lang']);

        $smarty -> register('serial_zon,countryList,serial_cou,productList,serial_pro');
    }
    
    $smarty -> register('error,zoneList');
    $smarty -> display();
?>
