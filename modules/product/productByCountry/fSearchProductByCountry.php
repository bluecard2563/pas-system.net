<?php
    $code_lang=$_SESSION['language'];
    Request::setInteger('0:error');
    $productType = new ProductType($db);
    $productTypes = $productType -> getProductTypes($code_lang);

    $languages = Language::getAllLanguages($db);
    if(is_array($productTypes)){
        foreach($productTypes as $key=>$c){
            $list_productType[$c['serial_tpp']] = utf8_encode($c['name_ptl']);
        }
    }

    $smarty -> register('list_productType,error,code_lang,languages');
    $smarty -> display();
?>
