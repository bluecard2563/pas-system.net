<?php
/*
File: fProductByCountry.php
Author: Edwin Salvador
Creation Date: 17/02/2010
Last Modified:
Modified By:
*/
Request::setInteger('hdnSerial_pro');

if($hdnSerial_pro){
    $serial_pro=$hdnSerial_pro;
    $product = new Product($db,$serial_pro);
    $data = $product->getInfoProduct($_SESSION['language']);
    $data = $data[0];

	$aditional_day_fee=(float)Product::getReferentialAdditionalDayFee($db, $serial_pro);
	//echo $aditional_day_fee;

    $countriesCount = 0;
    $pbc = new ProductByCountry($db);
    $assigned_countries = $pbc->getAssignedCountries($serial_pro);
    if(is_array($assigned_countries)){
        foreach($assigned_countries as &$ac){
            if($ac['assigned_taxes'] != ''){
                $ac['assigned_taxes'] = explode(',',$ac['assigned_taxes']);
            }else{
                unset($ac['assigned_taxes']);
            }
            if($ac['assigned_dealers'] != ''){
                $ac['assigned_dealers'] = explode(',',$ac['assigned_dealers']);
            }else{
                unset($ac['assigned_dealers']);
            }
//Debug::print_r($assigned_countries);

        
            $ac['zone_countries'] = array();
            $country = new Country($db);
            $country_list = $country->getCountriesByZone($ac['serial_zon'],$_SESSION['countryList']);
        
            foreach($country_list as $key=>$cl){
                $ac['zone_countries'][$key]=array();
                $ac['zone_countries'][$key]['serial_cou'] = $cl['serial_cou'];
                $ac['zone_countries'][$key]['name_cou'] = $cl['name_cou'];
				if($ac['serial_cou'] == $cl['serial_cou']){
					$ac['display'] = '1';
				}
            }
//Debug::print_r($country_list);
            $tax = new Tax($db);
            $taxList = $tax->getTaxes($ac['serial_cou']);
            if(is_array($taxList)){
                foreach($taxList as $tax_key=>$tl){
                    $ac['country_taxes_serial'][$tax_key] = $tl['serial_tax'];
                    $ac['country_taxes_name'][$tax_key] = $tl['name_tax'];
                }
            }
            
            if(is_array($ac['assigned_dealers'])){
                foreach($ac['assigned_dealers'] as &$ad){
                    $serial_pbd = $ad;
                    $pbd = new ProductByDealer($db, $ad);
                    $pbd->getData();
                    $dealer = new Dealer($db, $pbd->getSerial_dea());
                    $dealer->getData();
                    $ad = array();
                    $ad['serial_dea'] = $dealer->getSerial_dea();
                    $ad['serial_mbc'] = $dealer->getSerial_mbc();
                    $ad['name_dea'] = $dealer->getName_dea();

                    $used = $pbd->getIsUsed_dea();
                    $ad['used_dea'] = $used[0]['serial_pbd'];
                    /*if($used){
                        $ac['used_dealers'][] = $used[0];
                    }*/
                }
            }
            $mbc = new ManagerbyCountry($db);
            $managerList = $mbc->getManagerbyCountry($ac['serial_cou'],$_SESSION['serial_mbc']);
            if(is_array($managerList)){
                $ac['managers'] = array();
                $ac['managers'] = $managerList;
            }
        }
        $countriesCount = sizeof($assigned_countries)-1;
        $smarty->register('assigned_countries');
    }

    $zone = new Zone($db);
    $zonesList = $zone->getZones();
}else{
    http_redirect('modules/product/productByCountry/fSearchProductByCountry/2');
}
//Debug::print_r($assigned_countries);
$smarty->register('data,zonesList,countriesCount,aditional_day_fee');
$smarty->display();
?>