<?php 
/*
File: pNewDealerType.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2009 14:29 am
Last Modified: 21/01/2010
Modified By: David Bergmann
*/
$dealerT=new DealerType($db);
$dealerT->setDescription_dtl($_POST['txtDesc']);
$dealerT->setName_dlt($_POST['txtAlias']);
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);
$dealerT->setSerial_lang($language->getSerial_lang());
$dealerT->setStatus_dlt('ACTIVE');

if($dealerT->insertDealerType()){
	$dealerT->setSerial_dlt($dealerT->getDealerTypeSerial($dealerT->getName_dlt()));
	if($dealerT->insertDealerTypeByLanguage()){
		http_redirect('modules/dealer/dealerType/fNewDealerType/1');
	}else{
		http_redirect('modules/dealer/dealerType/fNewDealerType/3');
	}	
}else{
	http_redirect('modules/dealer/dealerType/fNewDealerType/2');
}
?>