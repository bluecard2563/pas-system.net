<?php 
/*
File: fNewDealerType.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 12:51
Last Modified: 21/01/2010
Modified By: David Bergmann
*/

Request::setInteger('0:error');
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);
$language = $language->getSerial_lang();
$smarty->register('error,language');
$smarty->display();
?>