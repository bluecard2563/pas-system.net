<?php
/*
File: pTranslateDealerType.php
Author: David Bergmann
Creation Date: 26/01/2010
Last Modified:
Modified By:
*/

$dealerT = new DealerType($db);

$dealerT->setSerial_lang($_POST['selLanguage']);
$dealerT->setSerial_dlt($_POST['hdnSerialDealerType']);
$dealerT->setDescription_dtl($_POST['txtDesc']);

if($dealerT->insertDealerTypeByLanguage()){
	http_redirect('modules/dealer/dealerType/fSearchDealerType/2');
}

http_redirect('modules/dealer/dealerType/fSearchDealerType/5');
?>