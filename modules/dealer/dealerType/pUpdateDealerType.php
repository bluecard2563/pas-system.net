<?php 
/*
File: pUpdateDealerType.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 15:30
Last Modified: 22/01/2010
Modified By: David Bergmann
*/

$dealerT=new DealerType($db);

if($dealerT->getDataByLanguage($_POST['serialDealerTyByLang'])){
	$dealerT->setDescription_dtl($_POST['txtDesc']);
	
	if($dealerT->update()){
		http_redirect('modules/dealer/dealerType/fSearchDealerType/1');
	}
}

http_redirect('modules/dealer/dealerType/fSearchDealerType/4');
?>