<?php

/*
  File: fUpdateDealer.php
  Author: Patricio Astudillo M.
  Creation Date: 11/01/2010 14:16
  Last Modified: Patricio Astudillo
  Modified By: 18/07/2012
 */

Request::setInteger('0:error');
//$serial_dea=$_POST['hdnDealerID'];
$serial_dea = $_POST['selDealer'];

if (!$serial_dea) {
    http_redirect('modules/dealer/fSearchDealer/3');
}

$user = new User($db, $_SESSION['serial_usr']);
$user->getData();
if ($user->getBelongsto_usr() == 'MANAGER' && $user->getSerial_mbc() != NULL) {
    $dealer = new Dealer($db, $serial_dea);

    /* RETRIEVING DEFAULT DATA */
    //Country
    $country = new Country($db);
    $countryList = $country->getOwnCountries($_SESSION['countryList']);

    //Language
    $language = new Language($db);
    $language->getDataByCode($_SESSION['language']);

    //Dealer Types
    $dealerT = new DealerType($db);
    $dTypesList = $dealerT->getDealerTypes(NULL, $language->getSerial_lang());

    //CATEGORY
    $categoryList = $dealer->getCategories();

    //STATUS
    $statusList = $dealer->getStatusList();

    //PAYMENT DEDLINE
    $deadlineList = $dealer->getDedline();

    //DISCOUNT/COMISSION
    $typeList = $dealer->getPercentageType();
    /* END DEFAULT DATA */

    if ($dealer->getData()) {
        $data['serial_dea'] = $serial_dea;
        $data['serial_sec'] = $dealer->getSerial_sec();
        $data['serial_mbc'] = $dealer->getSerial_mbc();
        $data['serial_cdd'] = $dealer->getSerial_cdd();
        $data['serial_dlt'] = $dealer->getSerial_dlt();
        $data['status_dea'] = $dealer->getStatus_dea();
        $data['official_dea'] = $dealer->getOfficial_seller_dea();
        $data['id_dea'] = $dealer->getId_dea();
        $data['code_dea'] = $dealer->getCode_dea();
        $data['name_dea'] = $dealer->getName_dea();
        $data['category_dea'] = $dealer->getCategory_dea();
        $data['address_dea'] = $dealer->getAddress_dea();
        $data['phone1_dea'] = $dealer->getPhone1_dea();
        $data['phone2_dea'] = $dealer->getPhone2_dea();
        $data['contract_date'] = $dealer->getContract_date_dea();
        $data['fax_dea'] = $dealer->getFax_dea();
        $data['email1_dea'] = $dealer->getEmail1_dea();
        $data['email2_dea'] = $dealer->getEmail2_dea();
        $data['contact_dea'] = $dealer->getContact_dea();
        $data['phone_contact'] = $dealer->getPhone_contact_dea();
        $data['email_contact'] = $dealer->getEmail_contact_dea();
        $data['percentage_dea'] = $dealer->getPercentage_dea();
        $data['type_dea'] = $dealer->getType_percentage_dea();
        $data['payment_dedline'] = $dealer->getPayment_deadline_dea();
        $data['bill_to'] = $dealer->getBill_to_dea();
        $data['dea_serial_dea'] = $dealer->getDea_serial_dea();
        $data['pay_contact'] = $dealer->getPay_contact_dea();
        $data['assistance_contact'] = $dealer->getAssistance_contact_dea();
        $data['email_assistance'] = $dealer->getEmail_assistance_dea();
        $data['branch_number'] = $dealer->getBranch_number_dea();
        $data['visits_number'] = $dealer->getVisits_number_dea();
        $data['bonus_to_dea'] = $dealer->getBonus_to_dea();
        $data['manager_name'] = $dealer->getManager_name_dea();
        $data['manager_phone'] = $dealer->getManager_phone_dea();
        $data['manager_email'] = $dealer->getManager_email_dea();
        $data['manager_birthday'] = $dealer->getManager_birthday_dea();
        $data['created_on'] = $dealer->getCreated_on_dea();
        $data['manager_rights'] = $dealer->getManager_rights_dea();
        $data['aux'] = $dealer->getAuxData_dea();
        $data['creator_name'] = $data['aux']['creator_name'];
        $data['bank'] = $dealer->getBank_dea();
        $data['account_type'] = $dealer->getAccount_type_dea();
        $data['account_number'] = $dealer->getAccount_number_dea();
		$data['minimumKits'] = $dealer->getMinimum_kits_dea();
		$data['notifications'] = $dealer->getNotificacions_dea();
        $data['responsible_name'] = $data['aux']['responsible_name'];
        		$data['logo_dea']= $dealer ->getLogo_dea();

        $country = new Country($db, $data['aux']['serial_cou']);
        $country->getData();
        $data['name_cou'] = $country->getName_cou();
        $data['code_cou'] = $country->getCode_cou();

        $city = new City($db, $data['aux']['serial_cit']);
        $city->getData();
        $data['name_cit'] = $city->getName_cit();
        $data['code_cit'] = $city->getCode_cit();

        $sector = new Sector($db);
        $sectorList = $sector->getSectorsByCity($data['aux']['serial_cit']);
        $sector = new Sector($db, $data['serial_sec']);
        $sector->getData();
        $data['serial_sec'] = $sector->getSerial_sec();

        $creditD = new CreditDay($db);
        $creditDList = $creditD->getCreditDays($data['aux']['serial_cou']);

        //Manager Data
        $user_by_dealer = new UserByDealer($db);
        $user_by_dealer->setSerial_dea($serial_dea);
        $user_by_dealer->getData();
        $data['serial_usr'] = $user_by_dealer->getSerial_usr();

        $comissionistUser = new User($db, $user_by_dealer->getSerial_usr());
        $comissionistUser->getData();
        $data['comissionist_serial_mbc'] = $comissionistUser->getSerial_mbc();
        $data['comissionist_name'] = $comissionistUser->getFirstname_usr() . ' ' . $comissionistUser->getLastname_usr();

        $mbc = new ManagerByCountry($db, $data['serial_mbc']);
        $mbc->getData();
        
        $manager = new Manager($db, $mbc->getSerial_man());
        $manager->getData();
        
        $data['name_man'] = $manager->getName_man();
        
        $user = new User($db, $_SESSION['serial_usr']);
        $paUser = !$user->isPlanetAssistUser();
        
        $smarty->register('data,cityList,sectorList,creditDList,paUser');
    }

    /* CASH MANAGEMENT SECTION */
    $parameterBank = new Parameter($db, 5); //BANKS
    $parameterBank->getData();
    $parameterBank = get_object_vars($parameterBank);
    $banks = explode(',', $parameterBank['value_par']);
    $account_types = Dealer::getAccountTypes($db);

    $today = date('d/m/Y');

    $smarty->register('today,banks,account_types');
    $smarty->register('error,dTypesList,countryList,statusList,deadlineList,categoryList,typeList,sectorList');
    $smarty->display();
} else {
    http_redirect('main/dealers/1');
}
?>