<?php 
/*
File: fSearchDealer.php
Author: Patricio Astudillo M.
Creation Date: 12/01/2010 14:06
Last Modified: 12/01/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getAllowedZones($_SESSION['countryList']);

$smarty->register('zonesList,error');
$smarty->display();
?>