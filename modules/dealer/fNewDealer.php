<?php 
/*
File: fNewDealer.php
Author: Patricio Astudillo M.
Creation Date: 08/01/2010 17:22
Last Modified: 08/01/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('0:error');
$user = new User($db,$_SESSION['serial_usr']);
$user->getData();

$subManagerRights = User::isSubManagerUser($db, $_SESSION['serial_usr']);
if($subManagerRights){
	$serialMbc = new Dealer($db, $_SESSION['serial_dea']);
	$serialMbc ->getData();
	$serialMbc = $serialMbc->getSerial_mbc();
}

//Only MANAGER's users with serial_mbc have access here
if(($user->getBelongsto_usr() == 'MANAGER' && $user->getSerial_mbc()!=NULL) || $subManagerRights) {
    $dealer=new Dealer($db);
    /*RETRIEVING DEFAULT DATA*/
    //Language
    $language = new Language($db);
    $language->getDataByCode($_SESSION['language']);
    //Dealer Types
    $dealerT=new DealerType($db);
    $dTypesList=$dealerT->getDealerTypes(NULL,$language->getSerial_lang());
    //CATEGORY
    $categoryList=$dealer->getCategories();
    //STATUS
    $statusList=$dealer->getStatusList();
    //PAYMENT DEDLINE
    $deadlineList=$dealer->getDedline();
    //DISCOUNT/COMISSION
    $typeList=$dealer->getPercentageType();
    //MANAGERS
    $managerbc = new ManagerbyCountry($db, $_SESSION['serial_mbc']);
    $managerbc->getData();

    if($user->isMainManagerUser($_SESSION['serial_usr'])) { //It's a PLANETASSIST's user
        //Countries
        $country=new Country($db);
        $countryList=$country->getOwnCountries($_SESSION['countryList']);
        $isMainManagerUser=1;
		$serialMbc=$_SESSION['serial_mbc'];
    } else { //It's a MANAGER's user
		if(!$subManagerRights):
			$serialMbc = $_SESSION['serial_mbc'];
		endif;
        
        $isOfficialManager=$managerbc->getOfficial_mbc();
		$hasOfficialSeller=$managerbc->hasOfficialSeller();
        $country = new Country($db,$managerbc->getSerial_cou());
        $country->getData();
        $nameCountry = $country->getName_cou();
		$idCountry=$country->getSerial_cou();
        $manager = new Manager($db,$managerbc->getSerial_man());
        $manager->getData();
        $nameManager = $manager->getName_man();
        //Cities
        $city=new City($db);
        $cityList=$city->getCitiesByCountry($managerbc->getSerial_cou(),$_SESSION['cityList']);
        //Comissionists
        $comissionistList=$user->getUsersByManager($serialMbc);
		//CreditDays
        $creditD=new CreditDay($db);
        $dataByCountry=$creditD->getCreditDays($managerbc->getSerial_cou());

		$user = new User($db, $_SESSION['serial_usr']);
    }
    /*END DEFAULT DATA*/
    
    //NEW: If we display the success message (1) we add the dealer's code:
    $recentlyCreatedDealerCode = 0;
    if($error == 1){
    	Request::setInteger('1:recentlyCreatedDealerSerial');
    	$dealer = new Dealer($db,$recentlyCreatedDealerSerial);
    	if($dealer -> getData()){
            $auxData = $dealer -> getAuxData_dea();
            $country = new Country($db,$auxData['serial_cou']);
            $country -> getData();
            $city = new City($db,$auxData['serial_cit']);
            $city -> getData();
            $recentlyCreatedDealerCode = $country -> getCode_cou() . '-' . $city -> getCode_cit() . '-' . $dealer -> getCode_dea();
    	}
    }
	
	/* CASH MANAGEMENT SECTION */
	$parameterBank = new Parameter($db,5);//BANKS
	$parameterBank -> getData();
	$parameterBank = get_object_vars($parameterBank);
	$banks = explode(',',$parameterBank['value_par']);
	$account_types = Dealer::getAccountTypes($db);

	$today=date('d/m/Y');
	$smarty->register('today,banks,account_types');
    $smarty->register('error,recentlyCreatedDealerCode,dTypesList,countryList,statusList,deadlineList,categoryList,typeList,serialMbc,nameCountry,nameManager,cityList,comissionistList,isMainManagerUser,dataByCountry,hasOfficialSeller,isOfficialManager,idCountry');
    $smarty->display();
}else{
    http_redirect('main/dealers/1');
}
?>