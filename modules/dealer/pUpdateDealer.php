<?php 
/*
 * File: pUpdateDealer.php
 * Author: Patricio Astudillo M.
 * Creation Date: 13/01/2009 08:50 am
 * Modified By: Santiago Borja
 * Last Modified: 21/02/2011
*/
	$dealer = new Dealer($db, $_POST['serial_dea']);
	global $global_yes_no;
	
	if($dealer -> getData()){
		//Json previous before update
        $jsonPrevious = json_decode(null);
        $jsonPrevious['selSector'] = $dealer -> getSerial_sec();
        $jsonPrevious['rdoSelComissionist'] = $_POST['hdnSerialUsr'];
        $jsonPrevious['rdManagerRights'] = $dealer ->getManager_rights_dea();
        $jsonPrevious['txtID'] = $dealer -> getId_dea();
        $jsonPrevious['txtName'] = $dealer -> getName_dea();
        $jsonPrevious['selDealerT'] = $dealer -> getSerial_dlt();
        $jsonPrevious['selCategory'] = $dealer -> getCategory_dea();
        $jsonPrevious['txtAddress'] = $dealer -> getAddress_dea();
        $jsonPrevious['selStatus'] = $dealer -> getStatus_dea();
        $jsonPrevious['txtPhone1'] = $dealer -> getPhone1_dea();
        $jsonPrevious['txtPhone2'] = $dealer -> getPhone2_dea();
        $jsonPrevious['txtFax'] = $dealer -> getFax_dea();
        $jsonPrevious['txtContractDate'] = $dealer -> getContract_date_dea();
        $jsonPrevious['txtMail1'] = $dealer -> getEmail1_dea();
        $jsonPrevious['txtMail2'] = $dealer -> getEmail2_dea();
        $jsonPrevious['txtContact'] = $dealer -> getContact_dea();
        $jsonPrevious['txtContactPhone'] = $dealer -> getPhone_contact_dea();
        $jsonPrevious['txtContactMail'] = $dealer -> getEmail_contact_dea();
        $jsonPrevious['txtVisitAmount'] = $dealer -> getVisits_number_dea();
        $jsonPrevious['txtLogo_dea'] = $dealer->getLogo_dea();
        $jsonPrevious['txtMinimumKits'] = $dealer ->getMinimum_kits_dea();
        $jsonPrevious['txtNameManager'] = $dealer -> getManager_name_dea();
        $jsonPrevious['txtPhoneManager'] = $dealer -> getManager_phone_dea();
        $jsonPrevious['txtMailManager'] = $dealer -> getManager_email_dea();
        $jsonPrevious['txtBirthdayManager'] = $dealer -> getManager_birthday_dea();
        $jsonPrevious['txtContactPay'] = $dealer -> getPay_contact_dea();
        $jsonPrevious['chkBillTo'] = $dealer -> getBill_to_dea();
        $jsonPrevious['selPayment'] = $dealer -> getPayment_deadline_dea();
        $jsonPrevious['selCreditD'] = $dealer -> getSerial_cdd();
        $jsonPrevious['txtPercentage'] = $dealer -> getPercentage_dea();
        $jsonPrevious['selType'] = $dealer -> getType_percentage_dea();
        $jsonPrevious['chkBonusTo'] = $dealer -> getBonus_to_dea();
        $jsonPrevious['selBank'] = $dealer -> getBank_dea();
        $jsonPrevious['selAccountType'] = $dealer ->getAccount_type_dea();
        $jsonPrevious['txtAccountNumber'] = $dealer ->getAccount_number_dea();
        $jsonPrevious['rdNotifications'] = $dealer ->getNotificacions_dea();
        $jsonPrevious['txtAssistName'] = $dealer -> getAssistance_contact_dea();
        $jsonPrevious['txtAssistEmail'] = $dealer -> getEmail_assistance_dea();
        $jsonPrevious['serial_dea'] = $dealer -> getSerial_dea();
        $jsonPrevious['hdnSerialMbc'] = $dealer -> getSerial_mbc();
        $jsonPrevious['hdnSerialUsr'] = $_POST['hdnSerialUsr'];
        $jsonPrevious['today'] = date('d/m/Y');
		$modifier_user_info = User::getBasicInformationForUser($db, $_SESSION['serial_usr']);
		
        /*NOTIFICATION MAIL INFO*/
        $code = Dealer::getDealerCode($db, $dealer -> getSerial_dea());
        $misc['textForEmail'] = "Se ha actualizado la siguiente informaci&oacute;n del comercializador <b>" . 
        						$dealer -> getName_dea() . "</b> cod : <u>$code</u><br><br>
								La persona que actualiz&oacute; el registro fue: ".$modifier_user_info['first_name_usr'].' '.$modifier_user_info['last_name_usr']; 
        $misc['textInfo'] = "Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
        $misc['titles'] = array();
        $jsonUpdate = json_decode(null);
        if($dealer -> getSerial_sec() != $_POST['selSector']) {
            $dealer -> setSerial_sec($_POST['selSector']);
            $sector = new Sector($db, $_POST['selSector']);
            $sector -> getData();
            $misc['data']['sector'] = $sector -> getName_sec();
            array_push($misc['titles'], "Sector");
			 $jsonUpdate['selSector'] = $_POST['selSector'];
        }
        
        if($_POST['hdnSerialMbc']) {
            if($dealer -> getSerial_mbc() != $_POST['hdnSerialMbc']) {
                $dealer -> setSerial_mbc($_POST['hdnSerialMbc']);
                $managerByCountry = new ManagerbyCountry($db, $_POST['hdnSerialMbc']);
                $managerByCountry -> getData();
                $manager = new Manager($db, $managerByCountry -> getSerial_man());
                $manager -> getData();
                $misc['data']['manager'] = $manager -> getName_man();
                $misc['serial_mbc'] = $_POST['hdnSerialMbc'];
                array_push($misc['titles'], "Representante");
                $misc['managerMail'] = $manager -> getContact_email_man();
				$jsonUpdate['hdnSerialMbc'] = $_POST['hdnSerialMbc'];
            }
        }else{
            $dealer -> setSerial_mbc(1); //TODO HARDCODED
        }
        
        if($dealer -> getSerial_cdd() != $_POST['selCreditD']) {
            $dealer -> setSerial_cdd($_POST['selCreditD']);
            $creditDay = new CreditDay($db, $_POST['selCreditD']);
            $creditDay -> getData();
            $misc['data']['creditDay'] = $creditDay -> getDays_cdd();
            array_push($misc['titles'], "D&iacute;as de cr&eacute;dito");
			 $jsonUpdate['selCreditD'] = $_POST['selCreditD'];
        }
        
        if($dealer -> getSerial_dlt() != $_POST['selDealerT']) {
            $dealer -> setSerial_dlt($_POST['selDealerT']);
            $dealerType = new DealerType($db, $_POST['selDealerT']);
            $dealerType -> getData();
            $dealerType -> getDealerTypeByLanguage($_SESSION['serial_lang']);
            $misc['data']['dealerType'] = $dealerType -> getDescription_dtl();
            array_push($misc['titles'], "Tipo");
			 $jsonUpdate['selDealerT'] = $_POST['selDealerT'];
        }
        
		if($dealer ->getManager_rights_dea() != $_POST['rdManagerRights'][0]) {
            $dealer -> setManager_rights_dea($_POST['rdManagerRights'][0]);
            $misc['data']['manager_rights'] = $global_yes_no[$_POST['rdManagerRights'][0]];
            array_push($misc['titles'], "Es sub-representante?");
			$jsonUpdate['rdManagerRights'] = $_POST['rdManagerRights'][0];
        }
		
		if($dealer -> getId_dea() != $_POST['txtID']) {
            $dealer -> setId_dea($_POST['txtID']);
            $misc['data']['id'] = $_POST['txtID'];
            array_push($misc['titles'], "Id");
			$jsonUpdate['txtID'] = $_POST['txtID'];
        }		
        
        if($dealer -> getName_dea() != $_POST['txtName']) {
            $dealer -> setName_dea(specialchars($_POST['txtName']));
            $misc['data']['name'] = specialchars($_POST['txtName']);
            array_push($misc['titles'], "Nombre");
			$jsonUpdate['txtName'] = $_POST['txtName'];
        }
        
        if($dealer -> getCategory_dea() != $_POST['selCategory']) {
            $dealer -> setCategory_dea($_POST['selCategory']);
            $misc['data']['category'] = $_POST['selCategory'];
            array_push($misc['titles'], "Categor&iacute;a");
			$jsonUpdate['selCategory'] = $_POST['selCategory'];
        }
        
        if($dealer -> getAddress_dea() != $_POST['txtAddress']) {
            $dealer -> setAddress_dea($_POST['txtAddress']);
            $misc['data']['address'] = $_POST['txtAddress'];
            array_push($misc['titles'], "Direcci&oacute;n");
			$jsonUpdate['txtAddress'] = $_POST['txtAddress'];
        }
        
        if($dealer -> getPhone1_dea() != $_POST['txtPhone1']) {
            $dealer -> setPhone1_dea($_POST['txtPhone1']);
            $misc['data']['phone1'] = $_POST['txtPhone1'];
            array_push($misc['titles'], "Tel&eacute;fono #1");
			$jsonUpdate['txtPhone1'] = $_POST['txtPhone1'];
        }
        
		if($dealer -> getPhone2_dea() != $_POST['txtPhone2']) {
            $dealer -> setPhone2_dea($_POST['txtPhone2']);
            $misc['data']['phone2'] = $_POST['txtPhone2'];
            array_push($misc['titles'], "Tel&eacute;fono #2");
			$jsonUpdate['txtPhone2'] = $_POST['txtPhone2'];
        }
        
        if($dealer -> getContract_date_dea() != $_POST['txtContractDate']){
            $dealer -> setContract_date_dea($_POST['txtContractDate']);
            $misc['data']['contractDate'] = $_POST['txtContractDate'];
            array_push($misc['titles'], "Fecha de contrato");
			 $jsonUpdate['txtContractDate'] = $_POST['txtContractDate'];
        }
        
        if($dealer -> getFax_dea() != $_POST['txtFax']){
            $dealer -> setFax_dea($_POST['txtFax']);
            $misc['data']['fax'] = $_POST['txtFax'];
            array_push($misc['titles'], "Fax");
			$jsonUpdate['txtFax'] = $_POST['txtFax'];
        }
        
        if($dealer -> getEmail1_dea() != $_POST['txtMail1']){
            $dealer -> setEmail1_dea($_POST['txtMail1']);
            $misc['data']['email1'] = $_POST['txtMail1'];
            array_push($misc['titles'], "E-mail #1");
			$jsonUpdate['txtMail1'] = $_POST['txtMail1'];
        }
        
        if($dealer -> getEmail2_dea() != $_POST['txtMail2']){
            $dealer -> setEmail2_dea($_POST['txtMail2']);
            $misc['data']['email2'] = $_POST['txtMail2'];
            array_push($misc['titles'], "E-mail #2");
			$jsonUpdate['txtMail2'] = $_POST['txtMail2'];
        }
        
        if($dealer -> getContact_dea() != $_POST['txtContact']){
            $dealer -> setContact_dea($_POST['txtContact']);
            $misc['data']['contact'] = $_POST['txtContact'];
            array_push($misc['titles'], "Nombre del Contacto");
			$jsonUpdate['txtContact'] = $_POST['txtContact'];
        }
        
        if($dealer -> getPhone_contact_dea() != $_POST['txtContactPhone']){
            $dealer -> setPhone_contact_dea($_POST['txtContactPhone']);
            $misc['data']['phoneContact'] = $_POST['txtContactPhone'];
            array_push($misc['titles'], "Tel&eacute;fono del Contacto");
			$jsonUpdate['txtContactPhone'] = $_POST['txtContactPhone'];
        }
        
        if($dealer -> getEmail_contact_dea() != $_POST['txtContactMail']){
            $dealer -> setEmail_contact_dea($_POST['txtContactMail']);
            $misc['data']['emailContact'] = $_POST['txtContactMail'];
            array_push($misc['titles'], "E-mail del Contacto");
			$jsonUpdate['txtContactMail'] = $_POST['txtContactMail'];
        }
        
        if($dealer -> getPercentage_dea() != $_POST['txtPercentage']){
            $dealer -> setPercentage_dea($_POST['txtPercentage']);
            $misc['data']['percentage'] = $_POST['txtPercentage'];
            array_push($misc['titles'], "Porcentaje");
			$jsonUpdate['txtPercentage'] = $_POST['txtPercentage'];
        }
        
        if($dealer -> getType_percentage_dea() != $_POST['selType']){
            $dealer -> setType_percentage_dea($_POST['selType']);
            $misc['data']['percentageType'] = $global_pType[$_POST['selType']];
            array_push($misc['titles'], "Tipo de porcentaje");
			$jsonUpdate['selType'] = $_POST['selType'];
        }
        
        if($dealer -> getStatus_dea() != $_POST['selStatus']){
            $dealer -> setStatus_dea($_POST['selStatus']);
            $misc['data']['status'] = $global_status[$_POST['selStatus']];
            array_push($misc['titles'], "Estado");
			
			$status_value = $_POST['selStatus'];
			$jsonUpdate['selStatus'] = $_POST['selStatus'];
        }
        
        if($dealer -> getPayment_deadline_dea() != $_POST['selPayment']){
            $dealer -> setPayment_deadline_dea($_POST['selPayment']);
            $misc['data']['payDeadline'] = $global_weekDays[$_POST['selPayment']];
            array_push($misc['titles'], "D&iacute;a de Pago");
			$jsonUpdate['selPayment'] = $_POST['selPayment'];
        }
        
        if($dealer -> getBill_to_dea() != $_POST['chkBillTo']){
            $dealer -> setBill_to_dea($_POST['chkBillTo']);
            $misc['data']['bill_to_dea'] = $global_billTo[$_POST['chkBillTo']];
            array_push($misc['titles'], "Factura a");
			$jsonUpdate['chkBillTo'] = $_POST['chkBillTo'];
        }
        
        if($dealer -> getVisits_number_dea() != $_POST['txtVisitAmount']){
            $dealer -> setVisits_number_dea($_POST['txtVisitAmount']);
            $misc['data']['visitNumber'] = $_POST['txtVisitAmount'];
            array_push($misc['titles'], "Visitas mensuales");
			$jsonUpdate['txtVisitAmount'] = $_POST['txtVisitAmount'];
        }
        
        if($dealer -> getPay_contact_dea() != $_POST['txtContactPay']){
            $dealer -> setPay_contact_dea($_POST['txtContactPay']);
            $misc['data']['payContact'] = $_POST['txtContactPay'];
            array_push($misc['titles'], "Contacto Pago");
			$jsonUpdate['txtContactPay'] = $_POST['txtContactPay'];
        }
        
        if($dealer -> getAssistance_contact_dea() != $_POST['txtAssistName']){
            $dealer -> setAssistance_contact_dea($_POST['txtAssistName']);
            $misc['data']['assistanceContact'] = $_POST['txtAssistName'];
            array_push($misc['titles'], "Nombre de asistencias");
			$jsonUpdate['txtAssistName'] = $_POST['txtAssistName'];
        }
        
        if($dealer -> getEmail_assistance_dea() != $_POST['txtAssistEmail']){
            $dealer -> setEmail_assistance_dea($_POST['txtAssistEmail']);
            $misc['data']['emailAssistance'] = $_POST['txtAssistEmail'];
            array_push($misc['titles'], "E-mail de asistencias");
			$jsonUpdate['txtAssistEmail'] = $_POST['txtAssistEmail'];
        }
        
        if($dealer -> getBonus_to_dea() != $_POST['chkBonusTo']){
            $dealer -> setBonus_to_dea($_POST['chkBonusTo']);
            $misc['data']['bonus_to_dea'] = $global_bonusTo[$_POST['chkBonusTo']];
            array_push($misc['titles'], "Incentivo a");
			$jsonUpdate['chkBonusTo'] = $_POST['chkBonusTo'];
        }

		if($dealer -> getManager_name_dea() != $_POST['txtNameManager']){
            $dealer -> setManager_name_dea($_POST['txtNameManager']);
            $misc['data']['manager_name'] = $_POST['txtNameManager'];
            array_push($misc['titles'], "Nombre del Gerente");
			 $jsonUpdate['txtNameManager'] = $_POST['txtNameManager'];
        }
        
		if($dealer -> getManager_phone_dea() != $_POST['txtPhoneManager']){
            $dealer -> setManager_phone_dea($_POST['txtPhoneManager']);
            $misc['data']['manager_phone'] = $_POST['txtPhoneManager'];
            array_push($misc['titles'], "Tel&eacute;fono del Gerente");
			$jsonUpdate['txtPhoneManager'] = $_POST['txtPhoneManager'];
        }
        
		if($dealer -> getManager_email_dea() != $_POST['txtMailManager']){
            $dealer -> setManager_email_dea($_POST['txtMailManager']);
            $misc['data']['manager_email'] = $_POST['txtMailManager'];
            array_push($misc['titles'], "E-mail del Gerente");
			$jsonUpdate['txtMailManager'] = $_POST['txtMailManager'];
        }
        
		if($dealer -> getManager_birthday_dea() != $_POST['txtBirthdayManager']){
            $dealer -> setManager_birthday_dea($_POST['txtBirthdayManager']);
            $misc['data']['manager_birthday'] = $_POST['txtBirthdayManager'];
            array_push($misc['titles'], "Fecha de Nacimiento del Gerente");
			$jsonUpdate['txtBirthdayManager'] = $_POST['txtBirthdayManager'];
        }
		
		if($dealer -> getBank_dea() != $_POST['selBank']){
            $dealer -> setBank_dea($_POST['selBank']);
            $misc['data']['bank'] = $_POST['selBank'];
            array_push($misc['titles'], "Banco");
			$jsonUpdate['selBank'] = $_POST['selBank'];
        }
		
		if($dealer ->getAccount_number_dea() != $_POST['txtAccountNumber']){
            $dealer -> setAccount_number_dea($_POST['txtAccountNumber']);
            $misc['data']['account_number'] = $_POST['txtAccountNumber'];
            array_push($misc['titles'], "N&uacute;mero de Cuenta");
			 $jsonUpdate['txtAccountNumber'] = $_POST['txtAccountNumber'];
        }
		
		if($dealer ->getAccount_type_dea() != $_POST['selAccountType']){
            $dealer -> setAccount_type_dea($_POST['selAccountType']);
            $misc['data']['account_type'] = $global_account_types[$_POST['selAccountType']];
            array_push($misc['titles'], "Tipo de Cuenta");
			$jsonUpdate['selAccountType'] = $_POST['selAccountType'];
        }
		
		if($dealer ->getMinimum_kits_dea() != $_POST['txtMinimumKits']){
            $dealer -> setMinimum_kits_dea($_POST['txtMinimumKits']);
            $misc['data']['txtMinimumKits'] = $_POST['txtMinimumKits'];
            array_push($misc['titles'], "Stock M&iacute;nimo");
			$jsonUpdate['txtMinimumKits'] = $_POST['txtMinimumKits'];
        }
		
		if($dealer ->getNotificacions_dea() != $_POST['rdNotifications']){
            $dealer ->setNotificacions_dea($_POST['rdNotifications']);
            $misc['data']['rdNotifications'] = $_POST['rdNotifications'];
            array_push($misc['titles'], "Recibe Notificaciones?");
			 $jsonUpdate['rdNotifications'] = $_POST['rdNotifications'];
        }
$artFileName= ' ';
		
		if (($dealer->getLogo_dea() != $_FILES['txtLogo_dea']['name'])&&( $_FILES['txtLogo_dea']['name'])!= NULL)
		{		
			//UPLOADING LOGO FILE
			$allowedArtExtensions = array("image/jpeg","image/pjpeg");
			$artFileUploaded = false;

			if(isset($_FILES['txtLogo_dea']['name']))
			{
				if(in_array($_FILES["txtLogo_dea"]["type"], $allowedArtExtensions))
				{
					if($_FILES["txtLogo_dea"]["size"]<2097152)
					{
					
                        //CHANGE LOGO NAME
						$artFileName = $code.'.'.$_FILES["txtLogo_dea"]['name'];
//						$artFileName = $code.'.'.date("Y/m/d");
//						$artFileName = $code;
                        
                        //CHECK IF FILE EXIST
                        //if (file_exists(DOCUMENT_ROOT.'img/dea_logos/'.$artFileName)) {
                          //  unlink(DOCUMENT_ROOT.'img/dea_logos/'.$artFileName);
                        //} else {
//                            nada
                        //}
						if(copy($_FILES['txtLogo_dea']['tmp_name'], DOCUMENT_ROOT.'img/dea_logos/'.$artFileName))
						{
							$artFileUploaded = true;
						}
					}else {
						http_redirect('modules/dealer/fSearchDealer/9');
					}
				}else{
					http_redirect('modules/dealer/fSearchDealer/8');
				}
			}
			$dealer->setLogo_dea($artFileName);
			$jsonUpdate['txtLogo_dea'] = $artFileName;
			//$misc['data']['logo'] = $artFileName;
            //array_push($misc['titles'], "Logo");
		}


		$user_by_dealer = new UserByDealer($db);
        $user_by_dealer -> setSerial_dea($dealer -> getSerial_dea());
        $user_by_dealer -> getData();
        
        $newUser = new User($db, $_POST['hdnSerialUsr']);
		$newUser -> getData();

        //UPDATE UBD IF CHANGED:
        if($user_by_dealer -> getSerial_usr() != $_POST['hdnSerialUsr']) {
		
        	$user_by_dealer -> deactivateUser();
            $user_by_dealer -> setSerial_usr($_POST['hdnSerialUsr']);
            $user_by_dealer -> setStatus_ubd("ACTIVE");
            $user_by_dealer -> setPercentage_ubd($newUser -> getCommission_percentage_usr());
            
        	if(!$user_by_dealer -> insert()) {
            	ErrorLog::log($db, 'UPDATE DEALER: FAILED TO INSERT NEW UBD', $user_by_dealer);
        		http_redirect('modules/dealer/fSearchDealer/4');
           	}
			 $jsonUpdate['hdnSerialUsr'] = $_POST['hdnSerialUsr'];
        }
        
        //UPDATE UBD AND SOME OTHER INFO ON ALL BRANCHES:
		$branch = new Dealer($db);
		$params = array('0' => array(	'field' => 'd.dea_serial_dea',
                                  		'value' => $dealer -> getSerial_dea(),
                                  		'like' => 1));
		$branchesByDealer = $branch -> getDealers($params); //TODO STATIC
		
		if($dealer->getManager_rights_dea() == 'NO'){
			if($branchesByDealer){
				foreach($branchesByDealer as $bl) {
					$branch -> setSerial_dea($bl['serial_dea']);
					$branch -> getData();
					$branch -> setBonus_to_dea($_POST['chkBonusTo']);
					$branch -> setType_percentage_dea($_POST['selType']);
					$branch -> setPercentage_dea($_POST['txtPercentage']);
					$branch -> setStatus_dea($_POST['selStatus']);
					$branch -> setBank_dea($_POST['selBank']);
					$branch -> setAccount_number_dea($_POST['txtAccountNumber']);
					$branch -> setAccount_type_dea($_POST['selAccountType']);
					$branch ->setLogo_dea($artFileName);

					if(!$branch -> update()){
						ErrorLog::log($db, 'UPDATE DEALER: FAILED TO UDPATE BRANCH INHERITED DATA', $branch);
						http_redirect('modules/dealer/fSearchDealer/4');
					}

					$branchUbd = new UserByDealer($db);
					$branchUbd -> setSerial_dea($bl[serial_dea]);
					$branchUbd -> getData();

					//CHANGE ONLY IF NOT THE SAME ALREADY:
					if($branchUbd -> getSerial_usr() != $_POST['hdnSerialUsr']) {
						$branchUbd -> deactivateUser();
						$branchUbd -> setSerial_usr($_POST['hdnSerialUsr']);
						$branchUbd -> setStatus_ubd("ACTIVE");
						$branchUbd -> setPercentage_ubd($newUser -> getCommission_percentage_usr());

						if(!$branchUbd -> insert()) {
							ErrorLog::log($db, 'UPDATE DEALER: FAILED TO INSERT NEW UBD FOR BRANCH', $user_by_dealer);
							http_redirect('modules/dealer/fSearchDealer/4');
						}
					}
				}
			}else{
				ErrorLog::log($db, 'UPDATE DEALER: FAILED TO FETCH BRANCHES', $params);
				http_redirect('modules/dealer/fSearchDealer/4');
			}
		}		
        
		//Could be the newUser or the previous user if nothing changed
        $finalUser = new User($db, $user_by_dealer -> getSerial_usr());
        $finalUser -> getData();
        $misc['comissionistMail'] = $finalUser -> getEmail_usr();
		
		if($dealer -> update()){
			//DEACTIVE USER'S IF APPLIES
			if($status_value){
				User::changeUserStatus($db, $status_value, 'DEALER', $dealer->getSerial_dea());
			}
			
            if(is_array($misc['data'])) {
                if(!GlobalFunctions::sendMail($misc, 'updateDealer')){
                    http_redirect('modules/dealer/fSearchDealer/7');
                }
            }
			
			$registerLog= Dealer::insertDealerLog($db, $_SESSION['serial_usr'],serialize($jsonPrevious),serialize($jsonUpdate), date('d/m/Y H:i:s'),$_POST['serial_dea']);
            
            http_redirect('modules/dealer/fSearchDealer/2');
		}else{
			ErrorLog::log($db, 'UPDATE DEALER: FAILED TO UPDATE', $dealer);
			http_redirect('modules/dealer/fSearchDealer/1');
        }
	}
?>