<?php
/**
 * Created by: Santi Albuja
 * Date: 23/10/2019
 * Time: 13:24
 */

Request::setInteger('0:error');
$statement = new Statement();
$statements = $statement->getCustomerStatementByMonthLimit();
//$statements = json_decode($statements, true);

$customers = array();
$i = 0;
if (is_array($statements)) {
    foreach ($statements as $statement) {
        if (is_array($statement)) {
            foreach ($statement as $customerStatement) {
                //Get customer section from full response and rebuild in a new array to template.
                $customers[$i] = $customerStatement['customer'];
                $customers[$i]['system_origin_es'] = getSystemOrigin($customers[$i]['system_origin']);
                $customers[$i]['disabled'] = $customers[$i]['status'] === 'INACTIVE' || $customers[$i]['statement_link'] === '' ? 'disabled' : '';
                $customers[$i]['hidden'] = $customers[$i]['status'] === 'INACTIVE' || $customers[$i]['statement_link'] === '' ? 'hidden' : '';
                $i++;
            }
        }
    }
}

function getSystemOrigin($systemOrigin)
{
    switch ($systemOrigin) {
        case 'BAS_NEW_SALE':
            $origin = 'NUEVA VENTA';
            break;
        case 'BAS_TRAVEL_RECORD':
            $origin = 'REGISTRO DE VIAJES';
            break;
    }
    return $origin;
}

$smarty->register('error,customers');
$smarty->display();