<?php
/*
File: fReAssignDealer.php
Author: Santiago Benitez
Creation Date: 12/05/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$zone=new Zone($db);
$zoneList=$zone->getZones();
$dealer = new Dealer($db);
$dealer_status = $dealer -> getStatusList();

$smarty->register('error,zoneList,dealer_status');
$smarty->display();
?>