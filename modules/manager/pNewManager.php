<?php 
/*
File: pNewManager.php
Author: Santiago Benitez.
Creation Date: 05/02/2010 11:42
Last Modified: 04/10/2010
Modified By: David Bergmann
*/

if($_POST['hdnCont']>0){
	$manager=new Manager($db);
	$mbc=new ManagerbyCountry($db);
	$flag=0;
	$manager->setSerial_cit($_POST['selCity']);
	$manager->setDocument_man($_POST['txtDocument']);
	$manager->setName_man($_POST['txtName']);
	$manager->setAddress_man($_POST['txtAddress']);
	$manager->setPhone_man($_POST['txtPhone']);
	$manager->setContract_date_man($_POST['txtContractDate']);
	$manager->setContact_man($_POST['txtContact']);
	$manager->setContact_phone_man($_POST['txtContactPhone']);
	$manager->setContact_email_man($_POST['txtContactEmail']);
	$manager->setType_man($_POST['selType']);
	if($_POST['txtSalesOnlyMan']) {
		$sales_only_man = 'YES';
	} else {
		$sales_only_man = 'NO';
	}
	$manager->setSales_only_man($sales_only_man);
	$manager->setMan_serial_man(1);
	$serial_man=$manager->insert();

	/*E-MAIL INFO*/
	$misc['textForEmail']="Se ha ingresado un nuevo Representante: <br><br>";
	$misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
	$misc['document']=$manager->getDocument_man();
	$misc['name']=$manager->getName_man();
	$misc['address']=$manager->getAddress_man();
	$misc['phone']=$manager->getPhone_man();
	$misc['contractDate']=$manager->getContract_date_man();
	$misc['contact']=$manager->getContact_man();
	$misc['contactPhone']=$manager->getContact_phone_man();
	$misc['contactEmail']=$manager->getContact_email_man();
	$misc['type']=$manager->getType_man();
	$misc['textCountryData']="Informaci&oacute;n de los pa&iacute;ses asignados:";
	$misc['countryInfo'] = array();
	/*END E-MAIL INFO*/

	if($serial_man){
		for($i=1;$i<$_POST['hdnCont'];$i++){
				if($_POST['hdnCountry'.$i]){

					$country = new Country($db,$_POST['hdnCountry'.$i]);
					$country->getData();
					$name_cou = $country->getName_cou();

					$mbc->setSerial_cou($_POST['hdnCountry'.$i]);
					$mbc->setSerial_man($serial_man);
					$mbc->setPercentage_mbc($_POST['txtPercentage']);
					$mbc->setType_percentage_mbc($_POST['selPercentageType']);
					$mbc->setStatus_mbc($_POST['hdnStatus'.$i]);
					$mbc->setPayment_deadline_mbc($_POST['selPaymentDeadline']);
					$mbc->setInvoice_number_mbc($_POST['selInvoice']);
					$mbc->setExclusive_mbc($_POST['hdnExclusive'.$i]);
					$mbc->setOfficial_mbc($_POST['hdnOfficial'.$i]);

					/*E-MAIL INFO*/
					$misc['country_table_titles']= array('Pa&iacute;s','Porcentaje','Tipo de Porcentaje',' Fecha L&iacute;mite de Pago','Facturaci&oacute;n','Exclusivo','Oficial');
					$misc['countryInfo'][$i] = array('countryName'=>$name_cou,
													 'percentage'=>$mbc->getPercentage_mbc(),
													 'percentageType'=>$mbc->getType_percentage_mbc(),
													 'paymentDeadline'=>$mbc->getPayment_deadline_mbc(),
													 'invoice'=>$mbc->getInvoice_number_mbc(),
													 'exclusive'=>$mbc->getExclusive_mbc(),
													 'official'=>$mbc->getOfficial_mbc());
					/*END E-MAIL INFO*/

					if(!$mbc->insert()){
							$flag=1;
							break;
					}
				}
		}
	}else{
		$flag=1;
	}
	if($flag==0){
		if(GlobalFunctions::sendMail($misc, 'newManager')){
			http_redirect('modules/manager/fNewManager/1');
		} else {
			http_redirect('modules/manager/fNewManager/3');
		}
	}else{
			http_redirect('modules/manager/fNewManager/2');
	}
}else{
	http_redirect('modules/manager/fNewManager/4');
}
?>
