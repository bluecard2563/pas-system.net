<?php
/*
File: fNewManager.php
Author: Santiago Ben�tez
Creation Date: 15/02/2010 15:07
Last Modified: 15/02/2010 15:07
Modified By: Santiago Ben�tez
*/

Request::setInteger('0:error');

$mbc= new ManagerbyCountry($db);
$manager= new Manager($db);
$zone= new  Zone($db);

$zonesList=$zone->getZones();

$smarty->register('error,userList,paymentDeadlineList,typeList,countryList,statusList,exclusiveCountryList,zonesList,typePercentageList');
$smarty->display();
?>