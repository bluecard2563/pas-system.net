<?php
/*
 * File: fSelectComissions.php
 * Author: Patricio Astudillo
 * Creation Date: 04/05/2010, 11:09:55 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 04/05/2010, 11:09:55 AM
 */
	$country = new Country($db);	
	$countryList = $country -> getOwnCountriesWithComission($_SESSION['countryList'], 'MANAGER', $_SESSION['serial_mbc']);
	
	$smarty -> register('error,countryList');
	$smarty -> display();
?>