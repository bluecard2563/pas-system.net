<?php
/*
 * File: fSelectComissionsComissionist.php
 * Author: Patricio Astudillo
 * Creation Date: 04/05/2010, 11:09:55 AM
 * Modifies By: Santiago Borja
 * Last Modified: 07/01/2011, 11:09:55 AM
 */
	$country = new Country($db);	
	$countryList = $country -> getOwnCountriesWithComission($_SESSION['countryList'], 'RESPONSIBLE', $_SESSION['serial_mbc']);//TODO MAKE STATIC FUNCTION
	
	$smarty->register('error,countryList');
	$smarty->display();
?>