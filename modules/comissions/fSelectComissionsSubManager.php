<?php
/**
 * file: fSelectComissionsSubManager
 * Author: Patricio Astudillo
 * Creation Date: 09/05/2012
 * Modification Date:  09/05/2012
 * Modified By: Patricio Astudillo
 */

	$country = new Country($db);	
	$countryList = $country -> getOwnCountriesWithComission($_SESSION['countryList'], 'SUBMANAGER', $_SESSION['serial_mbc']);
	
	$smarty -> register('error,countryList');
	$smarty -> display();
?>
