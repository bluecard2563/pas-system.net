<?php
/**
 * File: pSubManagersPreLiquidation
 * Author: Ing. Patricio Astudillo
 * Creation Date: 04-jun-2012
 * Last Modified: 04-jun-2012
 * Modified By: Ing. Patricio Astudillo
 */

	if (isset($typeToPay)) {
		$type = $typeToPay;
	} else {
		$type = $_POST['selComissionsTo'];
	}
	
	if (!$type)
		die('Insuficient arguments');

	if (isset($serialToPay)) {
		$serial = $serialToPay;
		$titleText = '<b>LIQUIDACION DE COMISIONES</b>';
	} else {
		switch ($type) {
			case 'DEALER':
				$serial = $_POST['selBranch'];
				break;
			case 'MANAGER':
				$serial = $_POST['selManager'];
				break;
			case 'RESPONSIBLE':
				$serial = $_POST['selResponsible'];
				break;
			case 'FREELANCE':
				$serial = $_POST['chkFreelance'];
			case 'SUBMANAGER':
				$serial = $_POST['selSubManager'];
				break;
		}
		$titleText = '<b>PRE LIQUIDACION DE COMISIONES</b>';
	}

	$dateTo = $_POST['txtDateTo'];

	$error = 1;

	//get info for details header
	$payTo = '';
	$today = date('d/m/Y');
	$totalSold = 0;
	$totalRefunded = 0;
	$comissionToPay = 0;
	$payToId = '';
	$payToPhone = '';
	$payToAddress = '';
	$apComission = new AppliedComission($db);
	//get info based on type for details

	$reportDateTo = $dateTo;

	$dealer = new Dealer($db, $serial);
	$dealer->getData();
	$payTo = $dealer->getName_dea();
	$payToId = $dealer->getId_dea();
	$payToPhone = $dealer->getPhone1_dea();
	$payToAddress = $dealer->getAddress_dea();

	$salesInfo = $apComission->getSubManagerComissionableValues($serial, $dateTo);
	if ($salesInfo) {
		foreach ($salesInfo as &$s) {
			
			if ($s['comision_prcg_inv'] > 0) { //COMISSION CASE
				/*** FORMULA ***
				 * COM = TOTAL_SAL * (1 - %OTHER_DISC) * (%DEALER - %BRANCH)
				 */
				
				$comission = $s['total_sal'] * (1 - ($s['other_dscnt_inv'] / 100)) * (($s['fixed_comission_percentage'] - $s['comision_prcg_inv']) / 100);
			} else { //DISCOUNT CASE
				/*** FORMULA ***
				 * COM = TOTAL_SAL * (%DEALER - %DISCOUNT - %OTHER_DISCOUNT)
				 */
				
				
				$comission = $s['total_sal'] * (($s['fixed_comission_percentage'] - $s['discount_prcg_inv'] - $s['other_dscnt_inv']) / 100);
			}

			$s['base_value'] = number_format($s['total_sal'], 2, '.', '');
			$s['comissionValue'] = number_format($comission , 2, '.', '');
		}
	}
	

	//CALCULATE THE TOTAL SOLD AND TOTAL REFUNDED
	$totalSold = 0;
	$totalRefunded = 0;
	$salesTotalEffective = 0;
	$zeroValue = number_format(0, 2, '.', '');
	$sales_group = array();
	
	if ($salesInfo) {
		foreach ($salesInfo as &$s) {
			array_push($sales_group, $s['serial_com']);
			if ($s['type_com'] == 'IN') {
				$totalSold += $s['comissionValue'];
				$s['to_comission'] = $s['comissionValue'];
				$s['toDiscount'] = $zeroValue;

				$salesTotalEffective += $s['net_total_sal'];
			} else {
				$totalRefunded += $s['comissionValue'];
				$s['to_comission'] = $zeroValue;
				$s['toDiscount'] = $s['comissionValue'];

				$salesTotalEffective -= $s['net_total_sal'];
			}

			//style some stuff:
			$s['net_total_sal'] = number_format($s['net_total_sal'], 2, '.', '');
			$s['fixed_comission_percentage'] = number_format($s['fixed_comission_percentage'], 2, '.', '');
			$s['comissionValue'] = number_format($s['comissionValue'], 2, '.', '');

			$s['name_cus'] = utf8_decode($s['name_cus']);
			$s['name_pbl'] = utf8_decode($s['name_pbl']);
			$s['name_dea'] = utf8_decode($s['name_dea']);
			unset($s['serial_com']);
			unset($s['value_dea_com']);
			unset($s['type_com']);
		}

		$comissionToPay = $totalSold - $totalRefunded;
		
		$data = "<table border='1'>
					<tr>
						<td><b># Tarjeta</b></td>
						<td><b>Fecha de Emisi&oacute;n</b></td>
						<td><b># Factura</b></td>
						<td><b>Apellido y Nombre</b></td>
						<td><b>Comercializador</b></td>
						<td><b>Fecha de Cobranza</b></td>						
						<td><b>% Sub-representnate</b></td>						
						<td><b>% Comisi&oacute;n Comercializador</b></td>
						<td><b>% Descuento en Factura</b></td>
						<td><b>% Otro Descuento en Factura</b></td>						
						<td><b>Monto Venta</b></td>
						<td><b>Monto Comisi&oacute;n</b></td>
						<td><b>Monto Descuento</b></td>
					</tr>";

		foreach($salesInfo as $cd){
			$data .= "<tr>
						<td>".$cd['card_number_sal']."</td>
						<td>".$cd['in_date_sal']."</td>
						<td>".$cd['number_inv']."</td>
						<td>".$cd['name_cus']."</td>
						<td>".$cd['name_dea']."</td>
						<td>".$cd['creation_date_com']."</td>
						<td>".$cd['fixed_comission_percentage']."</td>
						<td>".$cd['comision_prcg_inv']."</td>
						<td>".$cd['discount_prcg_inv']."</td>
						<td>".$cd['other_dscnt_inv']."</td>
						<td>".$cd['total_sal']."</td>
						<td>".$cd['to_comission']."</td>
						<td>".$cd['toDiscount']."</td>
					</tr>";
		}

		$data .= "	<tr>
						<td colspan='11'></td>
						<td>".$totalSold."</td>
						<td>".$totalRefunded."</td>
					</tr>
					<tr>
						<td colspan='11'><b>TOTAL:</b></td>
						<td colspan='2'>".number_format($totalSold - $totalRefunded, 2, '.', '')."</td>
					</tr>
		</table>";
	}
	
	
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = "<table>
				<tr>
					<td><b>PRE-LIQUIDACI&Oacute;N DE COMISIONES - SUBREPRESENTANTE</b></td>
				</tr>
				<tr>
					<td>$date</td>
				</tr>
				<tr><td></td></tr>
			</table>";
	
	$headerValues = "<table border='1'>";
	$headerValues .= "<tr>
						<td><b>Pagar a:</b></td>
						<td>".$payTo."</td>
						<td><b>RUC/CI:</b></td>
						<td>&nbsp;".$payToId."</td>
					</tr>";
	$headerValues .= "<tr>
						<td><b>Fecha:</b></td>
						<td>".$today."</td>
						<td><b>Tel&eacute;fono:</b></td>
						<td>&nbsp;".$payToPhone."</td>
					</tr>";
	$headerValues .= "<tr>
						<td><b>Comisi&oacute;n a Favor:</b></td>
						<td>".$totalSold."</td>
						<td><b>Direcci&oacute;n:</b></td>
						<td>".$payToAddress."</td>
					</tr>";
	$headerValues .= "<tr>
						<td><b>Comisi&oacute;n a Descontar:</b></td>
						<td>".$totalRefunded."</td>
						<td><b>Total Ventas Efectivas:</b></td>
						<td>".$salesTotalEffective."</td>
					</tr>";
	$headerValues .= "<tr>
						<td><b>Valor Comisi&oacute;n:</b></td>
						<td>".number_format($comissionToPay, 2, '.', '')."</td>
						<td><b>Liquidaci&oacute;n Hasta:</b></td>
						<td>$reportDateTo</td>
					</tr>";	
	$headerValues .= "</table><br />";

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=PreliquidacionComisiones.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $headerValues;
	echo $data;
?>
