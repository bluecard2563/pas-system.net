<?php
/*
  Document   : pPrintComissionsPreview
  Created on : 12-may-2010, 12:07:05
  Author     : Nicolas Flores
  Description:
  PDF to print a preview of comision to pay.
 * Parameters:
 * 	type: type of liquidation (RESPONSIBLE,MANAGER,DEALER,FREELANCE,SUBMANAGER)
 *  serial: serial of who the liquidation is for.
 *  dateTo: the ending date of the report
 */

	if (isset($typeToPay)) {
		$type = $typeToPay;
	} else {
		$type = $_POST['selComissionsTo'];
	}
        
	if (!$type)
		die('Insuficient arguments');

	if (isset($serialToPay)) {
		$serial = $serialToPay;
		$titleText = '<b>LIQUIDACION DE COMISIONES</b>';
	} else {
		switch ($type) {
			case 'DEALER':
				$serial = $_POST['selBranch'];
				break;
			case 'MANAGER':
				$serial = $_POST['selManager'];
				break;
			case 'RESPONSIBLE':
				$serial = $_POST['selResponsible'];
				break;
			case 'FREELANCE':
				$serial = $_POST['chkFreelance'];
			case 'SUBMANAGER':
				$serial = $_POST['selSubManager'];
				break;
		}
                $date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y'));
                $col_num = 12;
                $table ='<table>
                            <tr><td colspan='.$col_num.'>'.$date.'</td></tr>
                            <tr></tr>
                            <tr>
                                    <th></th>
                                    <th>     ------ PRE LIQUIDACION DE COMISIONES ----- DOCUMENTO NO VALIDO PARA EL PAGO ----------- </th>
                                    <th></th>
                            </tr>
                            <tr></tr>
                        </table>';
//		$titleText = '<b>----- PRE LIQUIDACION DE COMISIONES ----- DOCUMENTO NO VALIDO PARA EL PAGO ------</b>';
//		$titleText2 = '<b> -------------------------------------------------------------------------------------------------- </b>';
//		$titleText3 = '<b> ---------- DOCUMENTO NO VALIDO PARA EL PAGO ------------ </b>';
//		$titleText4 = '<b> -------------------------------------------------------------------------------------------------- </b>';
	}

	$dateTo = $_POST['txtDateTo'];

	$error = 1;
//get info for details header
	$payTo = '';
	$today = date('d/m/Y');
	$totalSold = 0;
	$totalRefunded = 0;
	$comissionToPay = 0;
	$payToId = '';
	$payToPhone = '';
	$payToAddress = '';
	$apComission = new AppliedComission($db);
	//get info based on type for details

	$reportDateTo = $dateTo;
	switch ($type) {
		case 'DEALER':
			$dealer = new Dealer($db, $serial);
			$dealer->getData();
			$payTo = $dealer->getName_dea();
			$payToId = $dealer->getId_dea();
			$payToPhone = $dealer->getPhone1_dea();
			$payToAddress = $dealer->getAddress_dea();

			$salesInfo = $apComission->getDealersComissionableValues($serial, $dealer->getLast_commission_paid_dea(), $dateTo);

			if ($salesInfo) {
				foreach ($salesInfo as &$s) {
                                    //Calculate new base value - Discount Farmers Tax
                                     //Check Taxes
                                        if($s['applied_taxes_inv'] != NULL){
                                        $taxes = unserialize($s['applied_taxes_inv']);
                                            if($taxes){
                                                $tx = 0;
                                                foreach($taxes as $tax){
                                                    $tx += (($s['base_value'] * $tax['tax_percentage']) /100);
                                                }
                                            }
                                        }
                                        if($s['report_date'] > '2018-09-09 00:00:00'){
                                            $s['net_total_sal'] = $s['base_value'] - $tx;
                                        }else{
                                            $s['net_total_sal'] = $s['base_value'];
                                        }
                                    
					$s['comissionPercentage'] = ($s['value_dea_com'] * 100) / $s['net_total_sal'];
					$s['comissionValue'] = $s['value_dea_com'];
				}
			}
			break;

		case 'MANAGER':
			$managerByCountry = new ManagerbyCountry($db, $serial);
			$managerByCountry->getData();
			$manager = new Manager($db, $managerByCountry->getSerial_man());
			$manager->getData();
			$payTo = $manager->getName_man();
			$payToId = $manager->getDocument_man();
			$payToPhone = $manager->getPhone_man();
			$payToAddress = $manager->getAddress_man();

			$salesInfo = $apComission->getManagersComissionableValues($serial, $managerByCountry->getLast_commission_paid_mbc(), $dateTo);

			if ($salesInfo) {
				foreach ($salesInfo as &$s) {
                                        //Calculate new base value - Discount Farmers Tax
                                        //Check Taxes
                                            if($s['applied_taxes_inv'] != NULL){
                                            $taxes = unserialize($s['applied_taxes_inv']);
                                                if($taxes){
                                                    $tx = 0;
                                                    foreach($taxes as $tax){
                                                        $tx += (($s['base_value'] * $tax['tax_percentage']) /100);
                                                    }
                                                }
                                            }
                                            if($s['report_date'] > '2018-09-09 00:00:00'){
                                                $s['net_total_sal'] = $s['base_value'] - $tx - $s['value_dea_com'];
                                            }else{
                                                $s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
                                            }
					$s['comissionPercentage'] = ($s['value_mbc_com'] * 100) / $s['net_total_sal'];
					$s['comissionValue'] = $s['value_mbc_com'];
				}
			}
			break;

		case 'RESPONSIBLE':
			$user = new User($db, $serial);
			$user->getData();
			$payTo = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
			$payToId = $user->getDocument_usr();
			$payToPhone = $user->getPhone_usr();
			$payToAddress = $user->getAddress_usr();

			$salesInfo = $apComission->getUsersComissionableSales($serial, $dateTo);

			if ($salesInfo) {
				foreach ($salesInfo as &$s) {
                                        //Calculate new base value - Discount Farmers Tax
                                        //Check Taxes
                                            if($s['applied_taxes_inv'] != NULL){
                                            $taxes = unserialize($s['applied_taxes_inv']);
                                                if($taxes){
                                                    $tx = 0;
                                                    foreach($taxes as $tax){
                                                        $tx += (($s['base_value'] * $tax['tax_percentage']) /100);
                                                    }
                                                }
                                            }
                                            if($s['report_date'] > '2018-09-09 00:00:00'){
                                                $tax = $s['base_value'] * 0.5/100;
                                                $s['net_total_sal'] = $s['base_value'] - $tx - $s['value_dea_com'];
                                            }else{
                                                $s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
                                            }
					$s['comissionPercentage'] = ($s['value_ubd_com'] * 100) / $s['net_total_sal'];
					$s['comissionValue'] = $s['value_ubd_com'];
				}
			}
			break;

		case 'FREELANCE':
			$freelance = new Freelance($db, $serial);
			$freelance->getData();
			$payTo = $freelance->getName_fre();
			$payToId = $freelance->getDocument_fre();
			$payToPhone = $freelance->getPhone1_fre();
			$payToAddress = $freelance->getAddress_fre();

			$salesInfo = AppliedComission::getFreelanceComissionableValues($db, $freelance->getSerial_fre(), $freelance->getLast_commission_paid_fre(), $dateTo);

			if ($salesInfo) {
				foreach ($salesInfo as &$s) {
                                        //Calculate new base value - Discount Farmers Tax
                                        //Check Taxes
                                            if($s['applied_taxes_inv'] != NULL){
                                            $taxes = unserialize($s['applied_taxes_inv']);
                                                if($taxes){
                                                    $tx = 0;
                                                    foreach($taxes as $tax){
                                                        $tx += (($s['base_value'] * $tax['tax_percentage']) /100);
                                                    }
                                                }
                                            }
                                        //Discount Taxes
                                        if($s['report_date'] > '2018-09-09 00:00:00'){
                                            $s['net_total_sal'] = $s['base_value'] - $tx - $s['value_dea_com'];
                                        }else{
                                            $s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
                                        }
					$s['comissionPercentage'] = ($s['value_fre_com'] * 100) / $s['net_total_sal'];
					$s['comissionValue'] = $s['value_fre_com'];
				}
			}
			break;
		case 'SUBMANAGER';
			$dealer = new Dealer($db, $serial);
			$dealer->getData();
			$payTo = $dealer->getName_dea();
			$payToId = $dealer->getId_dea();
			$payToPhone = $dealer->getPhone1_dea();
			$payToAddress = $dealer->getAddress_dea();

			$salesInfo = $apComission->getSubManagerComissionableValues($serial, $dateTo);
			if ($salesInfo) {
				foreach ($salesInfo as &$s) {
					if ($s['comision_prcg_inv'] > 0) { //COMISSION CASE
						$s['percentage_branch'] = $s['comision_prcg_inv'];
						$s['base_value'] = $s['total_sal'] - ($s['total_sal'] * (($s['discount_prcg_inv'] + $s['other_dscnt_inv']) / 100));
					} else { //DISCOUNT CASE
						$s['base_value'] = $s['total_sal'];
						$s['percentage_branch'] = $s['discount_prcg_inv'];
					}
					
					$s['base_value'] = number_format($s['base_value'], 2, '.', '');
					$s['comissionValue'] = $s['value_dea_com'];
					$s['net_total_sal'] = $s['base_value'];
				}
			}
			break;
	}

	//CALCULATE THE TOTAL SOLD AND TOTAL REFUNDED
	$totalSold = 0;
	$totalRefunded = 0;
	$salesTotalEffective = 0;
	$zeroValue = number_format(0, 2, '.', '');
	$sales_group = array();
	if ($salesInfo) {
		foreach ($salesInfo as &$s) {
            //********** FULL INVOICE NUMBER
            $serial_mbc = $s['serial_mbc'];                                          
            $erp_bodega = ERPConnectionFunctions::getStoreID($serial_mbc); 	    
            $erp_pde = ERPConnectionFunctions::getDepartmentID($serial_mbc); 	                                         
            
            if($erp_bodega){
                if ((int)$erp_bodega > 10){
                    $number_inv = substr ($s['number_inv'], 1);
                    $invoice_number =$erp_bodega.'-'.$erp_pde.'-'.str_pad($number_inv, 9, "0", STR_PAD_LEFT);                                                                      
                }else{
                    $invoice_number =$erp_bodega.'-'.$erp_pde.'-'.str_pad($s['number_inv'], 9, "0", STR_PAD_LEFT)."";                                                                
                }   
                $s['number_inv'] = $invoice_number;
            }
            
			array_push($sales_group, $s['serial_com']);
			if ($s['type_com'] == 'IN') {
				$totalSold += $s['comissionValue'];
				$s['to_comission'] = $s['comissionValue'];
				$s['toDiscount'] = $zeroValue;

				$salesTotalEffective += $s['net_total_sal'];
			} else {
				$totalRefunded += $s['comissionValue'];
				$s['to_comission'] = $zeroValue;
				$s['toDiscount'] = $s['comissionValue'];

				$salesTotalEffective -= $s['net_total_sal'];
			}

			//style some stuff:
			$s['net_total_sal'] = number_format($s['net_total_sal'], 2, '.', '');
			$s['fixed_comission_percentage'] = number_format($s['fixed_comission_percentage'], 2, '.', '');
			$s['comissionValue'] = number_format($s['comissionValue'], 2, '.', '');

			$s['name_cus'] = utf8_decode($s['name_cus']);
			$s['name_pbl'] = utf8_decode($s['name_pbl']);
			$s['name_dea'] = utf8_decode($s['name_dea']);
			unset($s['serial_com']);
			unset($s['value_dea_com']);
			unset($s['type_com']);
		}

		$comissionToPay = $totalSold - $totalRefunded;
	}

        //BUILD THE EXCEL HEADER FILE
        $table .= "<table border='1'>
                    <tr>
                            <th><b>Pagar a:</b></th>
                            <td>$payTo</td>
                            <th><b>RUC/CI</b></th>
                            <td>$payToId</td>
                    </tr>
                    <tr>
                            <th><b>Fecha</b></th>
                            <td>$today</td>
                            <th>Telefono</th>
                            <td>$payToPhone</td>
                    </tr>
                    <tr>
                            <th><b>Comision a favor</b></th>
                            <td>$totalSold</td>
                            <th><b>Direccion</b></th>
                            <td>$payToAddress</td>
                    </tr>
                    <tr>
                            <th><b>Comision a Descontar</b></th>
                            <td>$totalRefunded</td>
                            <th><b>Total Ventas efectivas</b></th>
                            <td>$salesTotalEffective</td>
                    </tr>
                    <tr>
                            <th><b>Valor Comisión</b></th>
                            <td>$comissionToPay</td>
                            <th><b>Liquidación Hasta:</b></th>
                            <td>$reportDateTo</td>
                    </tr>";
        
        //SEPARATE DATA
        $table .= "<table border='0'>"
                . "</table>";
        
        //BUILD THE EXCEL BODY FILE
        $table .= "<table border='1'>
                    <tr>
                            <th><b>N&uacute;mero de Tarjeta</b></th>
                            <th><b># Factura</b></th>
                            <th><b>Fecha Emision</b></th>
                            <th><b>Apellido y Nombre</b></th>
                            <th><b>Producto</b></th>
                            <th><b>Tiempo</b></th>
                            <th><b>Agencia</b></th>
                            <th><b>Valor Neto</b></th>
                            <th><b>% Comision</b></th>
                            <th><b>Fecha</b></th>
                            <th><b>Comision</b></th>
                            <th><b>Descuento</b></th>
                    </tr>";
        
        foreach ($salesInfo as $cl) {
        $table .= "<tr>
                <td>".$cl['card_number_sal']."</td>
                <td>".$cl['number_inv']."</td>
                <td>".$cl['date_inv']."</td>
                <td>".$cl['name_cus']."</td>
                <td>".$cl['name_pbl']."</td>
                <td>".$cl['days_sal']."</td>
                <td>".$cl['name_dea']."</td>
                <td>".$cl['net_total_sal']."</td>
                <td>".$cl['fixed_comission_percentage']."</td>
                <td>".$cl['report_date']."</td>
                <td>".$cl['to_comission']."</td>
                <td>".$cl['toDiscount']."</td>";
        
                $table .=  "</tr>";
    }
    $table.="</table>";
        
	//BUILD THE PDF FILE:
//	$headerValues = array();
//	$headerLine1 = array('col1' => '<b>Pagar a:</b>',
//		'col2' => $payTo,
//		'col3' => '<b>RUC/CI:</b>',
//		'col4' => $payToId);
//	array_push($headerValues, $headerLine1);
//	$headerLine2 = array('col1' => '<b>Fecha:</b>',
//		'col2' => $today,
//		'col3' => html_entity_decode('<b>Tel&eacute;fono:</b>'),
//		'col4' => $payToPhone);
//	array_push($headerValues, $headerLine2);
//	$headerLine3 = array('col1' => html_entity_decode('<b>Comisi&oacute;n a Favor:</b>'),
//		'col2' => number_format($totalSold, 2, '.', ''),
//		'col3' => html_entity_decode('<b>Direcci&oacute;n:</b>'),
//		'col4' => utf8_decode($payToAddress));
//	array_push($headerValues, $headerLine3);
//	$headerLine4 = array('col1' => html_entity_decode('<b>Comisi&oacute;n a Descontar:</b>'),
//		'col2' => number_format($totalRefunded, 2, '.', ''),
//		'col3' => utf8_decode('<b>Total ventas efectivas:</b>'),
//		'col4' => number_format($salesTotalEffective, 2, '.', ''));
//	array_push($headerValues, $headerLine4);
//	$headerLine5 = array('col1' => html_entity_decode('<b>Valor Comisi&oacute;n:</b>'),
//		'col2' => number_format($comissionToPay, 2, '.', ''),
//		
//		'col3' => html_entity_decode('<b>Liquidaci&oacute;n Hasta:</b>'),
//		'col4' => $reportDateTo);
//	array_push($headerValues, $headerLine5);
//
//	$pdf->ezSetDy(-10);
//	
//	$pdf->ezTable($headerValues, array('col1' => '', 'col2' => '', 'col3' => '', 'col4' => ''), '', array('showHeadings' => 0,
//		'shaded' => 0,
//		'showLines' => 2,
//		'xPos' => 'center',
//		'fontSize' => 8,
//		'titleFontSize' => 8,
//		'cols' => array(
//			'col1' => array('justification' => 'left', 'width' => 100),
//			'col2' => array('justification' => 'left', 'width' => 250),
//			'col3' => array('justification' => 'left', 'width' => 100),
//			'col4' => array('justification' => 'left', 'width' => 305))));
//
//	$pdf->ezSetDy(-10);
//
//	if ($type != 'SUBMANAGER') {
//		$pdf->ezTable($salesInfo, array('card_number_sal' => '<b># Tarjeta</b>',
//			'number_inv' => '<b># Factura</b>',
//            'date_inv' => '<b>Fecha Emision</b>',
//			'name_cus' => '<b>Apellido y Nombre</b>',
//			'name_pbl' => '<b>Producto</b>',
//			'days_sal' => '<b>Tiempo</b>',
//			'name_dea' => '<b>Agencia</b>',
//			'net_total_sal' => '<b>Valor Neto</b>',
//			'fixed_comission_percentage' => html_entity_decode('<b>% Comisi&oacute;n</b>'),
//			'report_date' => '<b>Fecha</b>',
//			'to_comission' => html_entity_decode('<b>Comisi&oacute;n</b>'),
//			'toDiscount' => '<b>Descuento</b>'), '', array('xPos' => 'center',
//			'innerLineThickness' => 0.8,
//			'outerLineThickness' => 1.2,
//			'fontSize' => 8,
//			'titleFontSize' => 8,
//			'cols' => array(
//				'card_number_sal' => array('justification' => 'center', 'width' => 48),
//				'number_inv' => array('justification' => 'center', 'width' => 48),
//                'date_inv' => array('justification' => 'center', 'width' => 55),
//				'name_cus' => array('justification' => 'center', 'width' => 100),
//				'name_pbl' => array('justification' => 'center', 'width' => 90),
//				'days_sal' => array('justification' => 'center', 'width' => 50),
//				'name_dea' => array('justification' => 'center', 'width' => 100),
//				'total_sal' => array('justification' => 'center', 'width' => 50),
//				'comissionPercentage' => array('justification' => 'center', 'width' => 50),
//				'report_date' => array('justification' => 'center', 'width' => 55),
//				'to_comission' => array('justification' => 'center', 'width' => 50),
//				'toDiscount' => array('justification' => 'center', 'width' => 55))));
//		$total_table_xpos = 748;
//	} else {
//		$pdf->ezTable($salesInfo, 
//				array(	'card_number_sal' => '<b># Tarjeta</b>',
//						'in_date_sal' => html_entity_decode('<b>Fecha de Emisi&oacute;n</b>'),
//						'number_inv' => '<b># Factura</b>',
//						'name_cus' => '<b>Apellido y Nombre</b>',						
//						'fixed_comission_percentage' => '<b>% Sub-representnate</b>',
//						'name_dea' => '<b>Comercializador</b>',
//						'percentage_branch' => '<b>% Comercializador</b>',
//						'total_sal' => '<b>Monto Venta</b>',
//						'base_value' => '<b>Monto Base</b>',
//						'creation_date_com' => '<b>Fecha de Cobranza</b>',
//						'to_comission' => html_entity_decode('<b>Comisi&oacute;n</b>'),
//						'toDiscount' => '<b>Descuento</b>'), 
//				'', 
//				array(	'xPos' => 'center',
//						'innerLineThickness' => 0.8,
//						'outerLineThickness' => 1.2,
//						'fontSize' => 8,
//						'titleFontSize' => 8,
//						'cols' => array(
//									'card_number_sal' => array('justification' => 'center', 'width' => 48),
//									'in_date_sal' => array('justification' => 'center', 'width' => 55),
//									'number_inv' => array('justification' => 'center', 'width' => 48),
//									'name_cus' => array('justification' => 'center', 'width' => 100),
//									'fixed_comission_percentage' => array('justification' => 'center', 'width' => 50),
//									'name_dea' => array('justification' => 'center', 'width' => 100),
//									'percentage_branch' => array('justification' => 'center', 'width' => 50),
//									'total_sal' => array('justification' => 'center', 'width' => 50),
//									'base_value' => array('justification' => 'center', 'width' => 50),
//									'creation_date_com' => array('justification' => 'center', 'width' => 55),
//									'to_comission' => array('justification' => 'center', 'width' => 50),
//									'toDiscount' => array('justification' => 'center', 'width' => 55))));
//		$total_table_xpos = 719;
//	}
//
//	//preparing totals array
//	$totalValues = array();
//	$totalLine1 = array('col1' => number_format($totalSold, 2, '.', ''),
//		'col2' => number_format($totalRefunded, 2, '.', ''));
//	array_push($totalValues, $totalLine1);
//	$totalLine2 = array('col1' => '<b>TOTAL:</b>',
//		'col2' => number_format($totalSold - $totalRefunded, 2, '.', ''));
//	array_push($totalValues, $totalLine2);
//	$pdf->ezTable($totalValues, array('col1' => '', 'col2' => ''), '', array('showHeadings' => 0,
//		'innerLineThickness' => 0.8,
//		'outerLineThickness' => 1.2,
//		'shaded' => 1,
//		'showLines' => 2,
//		'xPos' => $total_table_xpos,
//		'fontSize' => 8,
//		'titleFontSize' => 8,
//		'cols' => array(
//			'col1' => array('justification' => 'center', 'width' => 50),
//			'col2' => array('justification' => 'center', 'width' => 55))));
//
//	//check if is paying 
//	if (isset($serialToPay) && isset($typeToPay)) {
//		$mark_as_paid = implode(',', $sales_group);
//		if ($apComission->setPaidComission($typeToPay, $serialToPay, $_SESSION['serial_usr'], $mark_as_paid)) {
//			//set the new last comission paid date	
//			switch ($typeToPay) {
//				case 'MANAGER':
//					$managerByCountry = new ManagerbyCountry($db, $serialToPay);
//					$managerByCountry->getData();
//					if ($managerByCountry->updateLastComissionPaid()) {
//						$error = 1;
//					} else {
//						$error = 3;
//					}
//					break;
//				case 'DEALER':
//				case 'SUBMANAGER':
//					$dealer = new Dealer($db, $serialToPay);
//					$dealer->getData();
//					if ($dealer->updateLastComissionPaid()) {
//						$error = 1;
//					} else {
//						$error = 3;
//					}
//					break;
//				case 'RESPONSIBLE':
//					$responsible = new UserByDealer($db);
//					$responsible->setSerial_usr($serialToPay);
//					if ($responsible->updateLastComissionPaid()) {
//						$error = 1;
//					} else {
//						$error = 3;
//					}
//					break;
//				case 'FREELANCE':
//					if (Freelance::updateLastComissionPaid($db, $serialToPay)) {
//						$error = 1;
//					} else {
//						$error = 3;
//					}
//					break;
//			}
//		} else {
//			$error = 2;
//		}
//	}
//	if ($error == 1)
//		$pdf->ezStream();
//	else
//		http_redirect('main/comissions/' . $error);
    
    header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=pPrintPreviewExcel.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $table;
?>
