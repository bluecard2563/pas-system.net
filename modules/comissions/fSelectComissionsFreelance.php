<?php
/**
 * File:  fSelectComissionsFreelance
 * Author: Patricio Astudillo
 * Creation Date: 23-ago-2011, 9:00:05
 * Description:
 * Last Modified: 23-ago-2011, 9:00:05
 * Modified by:
 */
	
	$freelance_with_comissions = Freelance::getFreelanceWithComissions($db);
	
	$smarty -> register('freelance_with_comissions');
	$smarty -> display();
?>
