<?php 
/*
File: fNewContactByServiceProvider.php
Author: Santiago Ben�tez
Creation Date: 01/02/2010 14:37
Last Modified: 01/02/2010 14:37
Modified By: Santiago Ben�tez
*/

Request::setInteger('0:error');


$zone= new  Zone($db);
$contact= new ContactByServiceProvider($db);

/*Recover all available type of contacts from the system*/
$typeList=$contact->getAllTypes();
$zoneList=$zone->getZones();

$smarty->register('error,typeList,zoneList');
$smarty->display();
?>