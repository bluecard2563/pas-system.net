<?php
/**
 * File: fRetrievePaymentsFromERP
 * Author: Patricio Astudillo
 * Creation Date: 02-may-2013
 * Last Modified: 02-may-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');
	
	$param = new Parameter($db, 43); //Sync Param
	$param ->getData();
	$last_sync_date = $param->getValue_par();
	
	$smarty->register('error,last_sync_date');
	$smarty->display();
?>
