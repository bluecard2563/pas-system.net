<?php
/**
 * File: pRetrievePaymentsFromERP
 * Author: Patricio Astudillo
 * Creation Date: 02-may-2013
 * Last Modified: 02-may-2013
 * Modified By: Patricio Astudillo
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	
	ERPConnectionFunctions::fillPASWithERPPayments($db);
	
	http_redirect('modules/erpLink/fRetrievePaymentsFromERP/1');
?>
