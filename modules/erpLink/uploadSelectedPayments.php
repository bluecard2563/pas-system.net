<?php
/**
 * File: uploadSelectedPayments
 * Author: Patricio Astudillo
 * Creation Date: 22/09/2014
 * Last Modified: 22/09/2014
 * Modified By: Patricio Astudillo
*/
	
	if($_POST){
		Request::setString('txtFrom');
		Request::setString('txtTo');
		Request::setString('txtReceipts');
		
		$receiptArray = explode(',', $txtReceipts);
		ERPConnectionFunctions::fillPASWithERPPayments($db, $txtFrom, $txtTo, $receiptArray);
		$success = 1;
	}
	
	$today = date('d/m/Y');

	$smarty->register('success,today');
	$smarty->display();