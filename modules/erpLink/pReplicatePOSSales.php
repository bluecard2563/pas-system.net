<?php
/**
 * File: pReplicatePOSSales
 * Author: Patricio Astudillo
 * Creation Date: 31-may-2013
 * Last Modified: 31-may-2013
 * Modified By: Patricio Astudillo
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	
	$invoicesToReplicate = ERPConnectionFunctions::retrieveMissedInvoicesToReplicateOnERP($db, '19');
	
	//Debug::print_r($invoicesToReplicate); die;
	if($invoicesToReplicate){
		foreach($invoicesToReplicate as $invoice){
			$error_inv = ERPConnectionFunctions::migrateWebInvoiceToERP($db, $invoice['serial_inv']);
			
			if($error_inv != 'success'){
				$last_sync_invoice = $invoice['serial_inv'];
				break;
			}
		}	
	}
	
	if($error_inv == 'success'){
		$paymentsToReplicate = ERPConnectionFunctions::retrieveMissedPaymentsToReplicateOnERP($db, '19');
		
		//Debug::print_r($paymentsToReplicate); die;
		if($paymentsToReplicate){
			foreach($paymentsToReplicate as $payment){
				$error_pay = ERPConnectionFunctions::migrateWebPaymentToERP($db, $payment['serial_pay'], $payment['serial_man']);

				if($error_pay != 'success'){
					$last_sync_payment = $payment['serial_pay'];
					break;
				}
			}	
		}
	}
	
	if($error_inv == 'success' && $error_pay == 'success'){
		//****************** UPDATE PARAMETER *****************
		$param = new Parameter($db, 44);
		$param->getData();
		$param->setValue_par(date('d/m/Y'));
		$param->update();
	}else{
		if($last_sync_invoice){
			$error_pay .= "/$last_sync_invoice";
		}
		
		if($last_sync_payment){
			$error_pay .= "/$last_sync_payment";
		}
	}
	
	http_redirect("modules/erpLink/replicatePOSSales/$error_inv/$error_pay");
?>
