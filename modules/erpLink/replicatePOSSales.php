<?php
/**
 * File: replicatePOSSales
 * Author: Patricio Astudillo
 * Creation Date: 20-may-2013
 * Last Modified: 20-may-2013
 * Modified By: Patricio Astudillo
 */
	Request::setString('0:errorInvoice');
	Request::setString('1:errorPayment');
	
	$param = new Parameter($db, 44);
	$param->getData();
	$last_sync_date = $param->getValue_par();
	
	if($errorInvoice && $errorPayment){
		if($errorInvoice == 'success' && $errorPayment == 'success'){
			$error = 'success';
		}else{
			$error = 'error';
		}
	}
	
	$smarty->register('last_sync_date,error');
	$smarty->display();
?>
