<?php
/**
 * File: pReplicateSale
 * Author: Patricio Astudillo
 * Creation Date: 27-jun-2013
 * Last Modified: 27-jun-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('hdnSerialSal');
	
	$error = 1;
	if(!ERP_logActivity('SALES', $hdnSerialSal)){
		$error = 2;
	}
	
	http_redirect('modules/erpLink/replicateSale/'.$error);
?>
