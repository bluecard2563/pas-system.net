<?php
/**
 * File: pRegisterTranslation
 * Author: Patricio Astudillo
 * Creation Date: 05-sep-2012
 * Last Modified: 05-sep-2012
 * Modified By: Patricio Astudillo
 */

Request::setInteger('hdnSerial_lang');
Request::setString('hdnDescription');
Request::setInteger('hdnSerial_prc');
Request::setInteger('hdnPrc_Serial_prc');

$hdnDescription = mysql_escape_string(strip_tags($hdnDescription, '<p><b><i><u>'));

$pc = new ParticularConditions($db, $hdnSerial_prc);
$pc->getData();

$pc->setSerial_lang($hdnSerial_lang);
$pc->setName_prc($hdnDescription);
$pc->setPrc_serial_prc($hdnPrc_Serial_prc);

/*Debug::print_r($hdnSerial_prc);
Debug::print_r($hdnPrc_Serial_prc);
Debug::print_r($hdnDescription);
Debug::print_r($hdnSerial_lang);
Debug::print_r($pc->getSerial_prc());
die;
$serialPXC= PConditionsByProductCountry::getPxcBySerialPrc($db,$hdnSerial_prc);
/*Debug::print_r($serialPXC);
Debug::print_r($hdnSerial_prc);
Debug::print_r("serial_prc: " . $pc->getSerial_prc());*/


$exist = PConditionsByProductCountry::existTranslationAssociation($db,$serialPXC[0], $hdnSerial_prc);
if($pc->getSerial_prc() === 0){
    $serial_prc = $pc->insert();
    if($serial_prc){
        $error = 1;
    }else{
        $error = 2;
    }
}
else{
    if($pc->update()){
        //If no exist translation association in pconditions_pxc with serial_pxc and serial_prc then insert.
        if (!$exist){
            PConditionsByProductCountry::associateTranslation($db,$serialPXC[0], $hdnSerial_prc);
        }
        $error = 1;
    }else{
        $error = 2;
    }
}
http_redirect('modules/particular_conditions/fTranslatePCondition/'.$error);
?>