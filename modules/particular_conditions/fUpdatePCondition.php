<?php
/**
 * File: fUpdatePCondition
 * Author: Patricio Astudillo
 * Creation Date: 27-ago-2012
 * Last Modified: 27-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');
	Request::setInteger('1:serial_prc');
	
	
	if($serial_prc){
		$prc = new ParticularConditions($db, $serial_prc);
		if($prc ->getData()){
			$prc = get_object_vars($prc);
			unset($prc['db']);
			
			$language = new Language($db, $prc['serial_lang']);
			$language->getData();
			$language = get_object_vars($language);
			unset($language['db']);
			
			$status_list = ParticularConditions::getStatusValues($db);
			$smarty->register('prc,language,status_list');
		}
	}else{
		$pconditions_list = ParticularConditions::getAllConditionsByLanguage($db, $_SESSION['serial_lang']);
		$smarty->register('pconditions_list');
	}
	
	
	$smarty->register('error');
	$smarty->display();
?>
