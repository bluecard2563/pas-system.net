<?php
/**
 * File: pUpdatePCondition
 * Author: Patricio Astudillo
 * Creation Date: 27-ago-2012
 * Last Modified: 27-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('hdnLanguage');
	Request::setString('hdnDescription');
	Request::setString('selStatus');
	Request::setString('hdnSerial_prc');
	
	
	$hdnDescription = mysql_escape_string(strip_tags($hdnDescription, '<p><b><i><u><li>'));
	
	$pc = new ParticularConditions($db, $hdnSerial_prc);
	$pc->getData();
	$pc ->setSerial_lang($hdnLanguage);
	$pc ->setName_prc($hdnDescription);
	$pc ->setStatus_prc($selStatus);
	
	if($pc ->update()){
		$error = 1;
	}else{
		$error = 2;
	}
	
	http_redirect('modules/particular_conditions/fUpdatePCondition/'.$error);
?>
