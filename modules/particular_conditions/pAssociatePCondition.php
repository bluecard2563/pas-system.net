<?php
/**
 * File: pAssociatePCondition
 * Author: Patricio Astudillo
 * Creation Date: 27-ago-2012
 * Last Modified: 27-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('selPCondition');
	Request::setInteger('txtWeight');
	Request::setInteger('selProduct');
	Request::setInteger('selCountry');
	
	$serial_pxc = ProductByCountry::getPxCSerial($db, $selCountry, $selProduct);
	$serial_pcp = PConditionsByProductCountry::relationshipExists($db, $selPCondition, $serial_pxc);
	
	$pcp = new PConditionsByProductCountry($db, $serial_pcp);
	$pcp ->getData();
	$pcp ->setSerial_pxc($serial_pxc);
	$pcp->setSerial_prc($selPCondition);
	$pcp->setWeight_pcp($txtWeight);
	
	if(!$serial_pcp){
		$serial_pcp = $pcp->insert();
	}else{
		$pcp ->setStatus_pcp('ACTIVE');
		$serial_pcp = $pcp->update();
	}
	
	if($serial_pcp){
		$error = 1;
	}else{
		$error = 2;
	}
	
	http_redirect('modules/particular_conditions/fAssociatePCondition/'.$error);
?>
