<?php
/**
 * File: fRegisterTranslation
 * Author: Patricio Astudillo
 * Creation Date: 05-sep-2012
 * Last Modified: 05-sep-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:serial_prc');
	
	$pcp = new ParticularConditions($db, $serial_prc);
	if(!$pcp ->getData()){		//CAN'T LOAD INFO, SO REDIRECTION
		http_redirect('modules/particular_conditions/fTranslatePCondition/3');
	}
	
	$pcp_data = get_object_vars($pcp);
	unset($pcp_data['db']);
	
	$languages = Language::getAllLanguages($db, $_SESSION['serial_lang']);

	$smarty->register('pcp_data,languages');
	$smarty->display();
?>
