<?php
/**
 * File: pNewPCondition
 * Author: Patricio Astudillo
 * Creation Date: 24-ago-2012
 * Last Modified: 24-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('hdnLanguage');
	Request::setString('hdnDescription');
	
	$hdnDescription = mysql_escape_string(strip_tags($hdnDescription, '<p><b><i><u><li>'));
	
	$pc = new ParticularConditions($db);
	$pc ->setSerial_lang($hdnLanguage);
	$pc ->setName_prc($hdnDescription);
	$serial_prc = $pc ->insert();
	
	if($serial_prc){
		$error = 1;
	}else{
		$error = 2;
	}
	
	http_redirect('modules/particular_conditions/fNewPCondition/'.$error);
?>
