<?php
/**
 * File: fAssociatePCondition
 * Author: Patricio Astudillo
 * Creation Date: 24-ago-2012
 * Last Modified: 24-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');
	
	$code_lang=$_SESSION['language'];
    $zone = new Zone($db);
    $zoneList = $zone->getZones();
	
	$pc_list = ParticularConditions::getAllConditionsByLanguage($db, $_SESSION['serial_lang']);
	
	$smarty -> register('error,zoneList,pc_list');
    $smarty -> display();
?>
