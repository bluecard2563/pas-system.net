<?php
/**
 * File: fTranslatePCondition
 * Author: Patricio Astudillo
 * Creation Date: 05-sep-2012
 * Last Modified: 05-sep-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');

	$pconditions_list = ParticularConditions::getAllConditionsByLanguage($db, $_SESSION['serial_lang']);

	$smarty->register('pconditions_list,error');
	$smarty->display();
?>
