<?php 
/*
 * File: fDisplayContracts.php
 * Author: Santiago Borja
 * Creation Date: 21/04/2011
*/

	Request::setInteger('0:error');
	
	$products = Product::getMassiveProducts($db, $_SESSION['language']);
	$corporative_pro = Product::getCorporativeProducts($db, $_SESSION['serial_lang']);
	$products = array_merge($products, $corporative_pro);
	
	$smarty->register('error,products');
	$smarty->display();
?>