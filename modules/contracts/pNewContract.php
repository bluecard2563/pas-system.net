<?php 
/*
 * File: fNewContract.php
 * Author: Santiago Borja
 * Creation Date: 21/04/2011
*/
	extract($_POST);
	
	
	$cbc = new ContractsByCustomer($db);
	
	$cbc -> setContract_url_cbc($txtContractName);
	$cbc -> setSerial_cus($hdnCustomerID);
	$cbc -> setSerial_pro($selProduct);
	$cbc -> setStatus_cbc('ACTIVE');
	
	$cbc -> insert();
	
	http_redirect("modules/contracts/fDisplayContracts/1")
	
?>