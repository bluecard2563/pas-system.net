<?php 
/*
File: pNewCountry.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 10/02/2010 17:41
Modified By: David Bergmann
*/
	
$country=new Country($db);
$country->setSerial_lang($_POST['selLang']);
$country->setSerial_zon($_POST['selZone']);
$country->setCode_cou($_POST['txtCodeCountry']);
$country->setName_cou($_POST['txtNameCountry']);
$country->setPhone_cou($_POST['txtPhoneCountry']);
$lastSerial = $country -> getLastCountry()+1;

//Flag Image Validation
if($_FILES['fileFlagCou']['name'] != ''){
	$ext=strtolower(substr(basename($_FILES['fileFlagCou']['name']),-3));
	
	//Verifies if the file has the followin extensions: JPEG, GIF, PNG
	if($ext=='jpg' || $ext=='gif' || $ext=='png'){
		$pathFlag = DOCUMENT_ROOT.'img/country/flag/';
		$pathRealImageFlag = $pathFlag;
		$newImageFlag = 'flag_'.$lastSerial.'.'.$ext;
		$tempImage = 'tmp.'.$ext;
		$pathRealImageFlag .= $newImageFlag;
		$pathFlag .= $tempImage;
		if(move_uploaded_file($_FILES['fileFlagCou']['tmp_name'], $pathFlag)) {
			///verifies if the image has the followin dimentions: 150x150
			list($width, $heigh) = getimagesize($pathFlag);
	
			if($width <= 150 && $heigh <= 150){
				$tam = filesize($pathFlag);
				///verifies if the image has the followin size: 307200 bytes (300 KB)
				if($tam <= 307200){
					$flagImgFlag = 1;
				}
				else{
					http_redirect('modules/country/fNewCountry/3');
				}
			}
			else{
				http_redirect('modules/country/fNewCountry/4');
			}
		}
		else{
			http_redirect('modules/country/fNewCountry/5');
		}
	}
	else{
		http_redirect('modules/country/fNewCountry/6');
	}
}

//Card Image Validation
if($_FILES['fileCardCou']['name'] != ''){
	$ext=strtolower(substr(basename($_FILES['fileCardCou']['name']),-3));
	
	//Verifies if the file has the followin extensions: JPEG, GIF, PNG
	if($ext=='jpg' || $ext=='gif' || $ext=='png'){
		$pathCard = DOCUMENT_ROOT.'img/country/card/';
		$pathRealImageCard = $pathCard;
		$newImageCard = 'card_'.$lastSerial.'.'.$ext;
		$tempImage = 'tmp.'.$ext;
		$pathRealImageCard .= $newImageCard;
		$pathCard .= $tempImage;
		if(move_uploaded_file($_FILES['fileCardCou']['tmp_name'], $pathCard)) {
			///verifies if the image has the followin dimentions: 150x150
			list($width, $heigh) = getimagesize($pathCard);
	
			if($width <= 150 && $heigh <= 150){
				$tam = filesize($pathCard);
				///verifies if the image has the followin size: 307200 bytes (300 KB)
				if($tam <= 307200){
					$flagImgCard = 1;
				}
				else{
					http_redirect('modules/country/fNewCountry/7');
				}
			}
			else{
				http_redirect('modules/country/fNewCountry/8');
			}
		}
		else{
			http_redirect('modules/country/fNewCountry/9');
		}
	}
	else{
		http_redirect('modules/country/fNewCountry/10');
	}
}

//Banner Image Validation
if($_FILES['fileBannerCou']['name'] != ''){
	$ext=strtolower(substr(basename($_FILES['fileBannerCou']['name']),-3));
	
	//Verifies if the file has the followin extensions: JPEG, GIF, PNG
	if($ext=='jpg' || $ext=='gif' || $ext=='png'){
		$pathBanner = DOCUMENT_ROOT.'img/country/banner/';
		$pathRealImageBanner = $pathBanner;
		$newImageBanner = 'banner_'.$lastSerial.'.'.$ext;
		$tempImage = 'tmp.'.$ext;
		$pathRealImageBanner .= $newImageBanner;
		$pathBanner .= $tempImage;
		if(move_uploaded_file($_FILES['fileBannerCou']['tmp_name'], $pathBanner)) {
			///verifies if the image has the followin dimentions: 150x150
			list($width, $heigh) = getimagesize($pathBanner);
	
			if($width <= 150 && $heigh <= 150){
				$tam = filesize($pathBanner);
				///verifies if the image has the followin size: 307200 bytes (300 KB)
				if($tam <= 307200){
					$flagImgBanner = 1;
				}
				else{
					http_redirect('modules/country/fNewCountry/11');
				}
			}
			else{
				http_redirect('modules/country/fNewCountry/12');
			}
		}
		else{
			http_redirect('modules/country/fNewCountry/13');
		}
	}
	else{
		http_redirect('modules/country/fNewCountry/14');
	}
}


if($flagImgFlag){
	if( copy( $pathFlag, $pathRealImageFlag ) ){
		$country -> setFlag_cou($newImageFlag);
	}
}
if($flagImgCard){
	if( copy( $pathCard, $pathRealImageCard ) ){
		$country -> setCard_cou($newImageCard);
	}
}
if($flagImgBanner){
	if( copy( $pathBanner, $pathRealImageBanner ) ){
		$country -> setBanner_cou($newImageBanner);
	}
}



if($country->insert()){
    $user=new User($db,$_SESSION['serial_usr']);
    $user->getData();
    if($user->isPlanetAssistUser()){
        $_SESSION['countryList']=$_SESSION['countryList'].",".$db->insert_ID();
    }
    $currency = new CurrenciesByCountry($db, 1, $db->insert_ID(),"YES","ACTIVE");

    if($currency->insert()) {
       http_redirect('modules/country/fNewCountry/1');
    } else {
        http_redirect('modules/country/fNewCountry/15');
    }
    
}else{
    http_redirect('modules/country/fNewCountry/2');
}
?>