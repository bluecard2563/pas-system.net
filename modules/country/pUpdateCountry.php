<?php 
/*
File: pUpdateCountry.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 
Modified By: 
*/
$country = new Country($db);
$data['serial_cou'] = $_POST['hdnSerial_cou'];
$country -> setSerial_cou($data['serial_cou']);
$country -> getData();
$data['code_cou'] = $country->getCode_cou();
$data['selLang'] = $country->getSerial_lang();
$data['serial_zon'] = $_POST['selZone'];
$data['name_cou'] = $_POST['txtNameCountry'];
$data['flag_cou'] = $country -> getFlag_cou();
$data['card_cou'] = $country -> getCard_cou();
$data['banner_cou'] = $country -> getBanner_cou();
$data['phone_cou'] = $country -> getPhone_cou();

//Flag Image Validation
if($_FILES['fileFlagCou']['name'] != ''){
	$ext=strtolower(substr(basename($_FILES['fileFlagCou']['name']),-3));
	
	//Verifies if the file has the followin extensions: JPEG, GIF, PNG
	if($ext=='jpg' || $ext=='gif' || $ext=='png'){
		$pathFlag = DOCUMENT_ROOT.'img/country/flag/';
		$pathRealImageFlag = $pathFlag;
		$newImageFlag = 'flag_'.$data['serial_cou'].'.'.$ext;
		$tempImage = 'tmp.'.$ext;
		$pathRealImageFlag .= $newImageFlag;
		$pathFlag .= $tempImage;
		if(move_uploaded_file($_FILES['fileFlagCou']['tmp_name'], $pathFlag)) {
			///verifies if the image has the followin dimentions: 150x150
			list($width, $heigh) = getimagesize($pathFlag);
			if($width <= 150 && $heigh <= 150){
				$tam = filesize($pathFlag);
				///verifies if the image has the followin size: 307200 bytes (300 KB)
				if($tam <= 307200){
					$flagImgFlag = 1;
				}
				else{
					$error = 3;
				}
			}
			else{
				$error = 4;
			}
		}
		else{
			$error = 5;
		}
	}
	else{
		$error = 6;
	}
}

//Card Image Validation
if($_FILES['fileCardCou']['name'] != ''){
	$ext=strtolower(substr(basename($_FILES['fileCardCou']['name']),-3));
	
	//Verifies if the file has the followin extensions: JPEG, GIF, PNG
	if($ext=='jpg' || $ext=='gif' || $ext=='png'){
		$pathCard = DOCUMENT_ROOT.'img/country/card/';
		$pathRealImageCard = $pathCard;
		$newImageCard = 'card_'.$data['serial_cou'].'.'.$ext;
		$tempImage = 'tmp.'.$ext;
		$pathRealImageCard .= $newImageCard;
		$pathCard .= $tempImage;
		if(move_uploaded_file($_FILES['fileCardCou']['tmp_name'], $pathCard)) {
			///verifies if the image has the followin dimentions: 150x150
			list($width, $heigh) = getimagesize($pathCard);
	
			if($width <= 150 && $heigh <= 150){
				$tam = filesize($pathCard);
				///verifies if the image has the followin size: 307200 bytes (300 KB)
				if($tam <= 307200){
					$flagImgCard = 1;
				}
				else{
					$error = 7;
				}
			}
			else{
				$error = 8;
			}
		}
		else{
			$error = 9;
		}
	}
	else{
		$error = 10;
	}
}

//Banner Image Validation
if($_FILES['fileBannerCou']['name'] != ''){
	$ext=strtolower(substr(basename($_FILES['fileBannerCou']['name']),-3));
	
	//Verifies if the file has the followin extensions: JPEG, GIF, PNG
	if($ext=='jpg' || $ext=='gif' || $ext=='png'){
		$pathBanner = DOCUMENT_ROOT.'img/country/banner/';
		$pathRealImageBanner = $pathBanner;
		$newImageBanner = 'banner_'.$data['serial_cou'].'.'.$ext;
		$tempImage = 'tmp.'.$ext;
		$pathRealImageBanner .= $newImageBanner;
		$pathBanner .= $tempImage;
		if(move_uploaded_file($_FILES['fileBannerCou']['tmp_name'], $pathBanner)) {
			///verifies if the image has the followin dimentions: 150x150
			list($width, $heigh) = getimagesize($pathBanner);
	
			if($width <= 150 && $heigh <= 150){
				$tam = filesize($pathBanner);
				///verifies if the image has the followin size: 307200 bytes (300 KB)
				if($tam <= 307200){
					$flagImgBanner = 1;
				}
				else{
					$error = 11;
				}
			}
			else{
				$error = 12;
			}
		}
		else{
			$error = 13;
		}
	}
	else{
		$error = 14;
	}
}


if($flagImgFlag){
	if( copy( $pathFlag, $pathRealImageFlag ) ){
		$country -> setFlag_cou($newImageFlag);
	}
}
if($flagImgCard){
	if( copy( $pathCard, $pathRealImageCard ) ){
		$country -> setCard_cou($newImageCard);
	}
}
if($flagImgBanner){
	if( copy( $pathBanner, $pathRealImageBanner ) ){
		$country -> setBanner_cou($newImageBanner);
	}
}

$country->setSerial_lang($_POST['selLang']);
$country->setSerial_zon($_POST['selZone']);
$country->setName_cou($_POST['txtNameCountry']);
$country->setPhone_cou($_POST['txtPhoneCountry']);
$country->setCode_cou($_POST['txtCodeCountry']);

if(!$error) {
	if($country -> update() ){
		http_redirect("modules/country/fSearchCountry/1");	
	}
	else {
		$error = 2;
	}
}
$smarty->register('error,data');
$smarty->display('modules/country/fUpdateCountry.'.$_SESSION['language'].'.tpl')
	
?>