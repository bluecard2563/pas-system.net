<?php
/**
 * Name: pExportAvailableBonus
 * Created by: pastudillo
 * Created on: 17-ene-2017
 * Description: PDF File for export a detailed list of available bonus to liquidate.
 * Modified by: pastudillo
 * Modified on: 17-ene-2017
 */

$amountAvailable = AppliedBonus::getAvailableBonusForUser1($db, $_SESSION['serial_usr']);

//**************** CONVERT MONEY INTO POINTS
$param = new Parameter($db, 53);
$param->getData();
$convertionFactor = $param->getValue_par();

//**************** MAKING THE PDF
include DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);

//TITLE
$pdf->ezText(html_entity_decode('<b>LISTADO DE PUNTOS DISPONIBLES</b>'), 14, array('justification' => 'center'));
$pdf->ezSetDy(-20);

$date = html_entity_decode($global_weekDaysNumb[$date['weekday']] . ', ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);

$pdf->ezText(html_entity_decode('<b>Fecha:</b> ') . $date, 11, array('justification' => 'left'));
$pdf->ezText('<b>Incentivos para:</b> ' . $bonus_to_subject, 11, array('justification' => 'left'));
$pdf->ezText('<b>Beneficiario:</b> ' . $bonus_to_name, 11, array('justification' => 'left'));
$pdf->ezSetDy(-20);

if ($amountAvailable) {
	foreach ($amountAvailable as &$b) {
		($b['ondate_apb'] == 'YES') ? $b['ondate_apb'] = 'Si' : $b['ondate_apb'] = 'No';
		
		$total_to_pay += $b['amount_available'];
		
		$b['total_amount_apb'] *= $convertionFactor;
		$b['amount_available'] *= $convertionFactor;
	}
	
	//Debug::print_r($amountAvailable); die;

	$pdf->ezText('<b>Total por Liquidar:</b> ' . (int)($total_to_pay * $convertionFactor).' puntos', 11, array('justification' => 'center'));
	$pdf->ezSetDy(-10);

	$pdf->ezTable($amountAvailable,
			array('card_number_sal' => '<b>Tarjeta</b>',
					'name_cus' => '<b>Cliente</b>',
					'number_inv' => '<b>Factura</b>',
					'total_amount_apb' => '<b>Puntos Generados</b>',
					'amount_available' => '<b>Puntos Disponibles</b>'),
				'<b>DETALLE DE TARJETAS</b>',
			array(	'xPos' => 'center',
					'cols' => array(
						'card_number_sal' => array('justification' => 'center', 'width' => 80),
						'name_cus' => array('justification' => 'center', 'width' => 100),
						'number_inv' => array('justification' => 'center', 'width' => 80),
						'total_amount_apb' => array('justification' => 'center', 'width' => 80),
						'pending_amount_apb' => array('justification' => 'center', 'width' => 90))));
} else {
	$pdf->ezText(html_entity_decode('No existen incentivos por cancelar.'), 10, array('justification' => 'center'));
}

$pdf->ezStream();
