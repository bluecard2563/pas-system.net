<?php
/*
    Document   : pAuthorizeOutDateBonus
    Created on : 21-jun-2010, 15:41:12
    Author     : Nicolas
    Description:
        Register the bonus which has been authorized or denied.
*/

Request::setString('selToAuthorize');
Request::setString('txtObservations');
Request::setString('selectedBoxes');

$selectedBoxes = str_replace('&', ',', str_replace('chkBonus=', '', $selectedBoxes));
$chkBonus = explode(",", $selectedBoxes);

$to_authorize=$chkBonus;
$error=1;
if($selToAuthorize && $to_authorize){
	foreach($to_authorize as $serial_apb){
		//echo "<h1>Begin Iteration app: $serial_apb</h1>";
		if(!AppliedBonus::authorizeBonus($db, $serial_apb, $selToAuthorize, $_SESSION['serial_usr'], specialchars($txtObservations))){
			$error=3;
			break;
		}
		//echo "<h1>End iteration</h1>";
	}
}else{
	$error=2;
}

http_redirect('modules/bonus/fAuthorizeOutDateBonus/'.$error);
?>