<?php

/*
 * File: liquidationsPreview
 * Author: Patricio Astudillo
 * Creation Date: Jan 31, 2011, 4:52:29 PM
 * Description:
 * Modified By:
 * Last Modified:
 */

Request::setString('0:payTo');
Request::setString('1:serialTo');

$apBonus = new AppliedBonus($db);
$onDateBonus = $apBonus->getOnDateBonus($serialTo, $payTo);
if (!$onDateBonus) {
	$onDateBonus = array();
}
$outDateBonus = $apBonus->getOutDateBonus($serialTo, $payTo);
if (!$outDateBonus) {
	$outDateBonus = array();
}
$bonusToPay = array_merge($onDateBonus, $outDateBonus);

if ($payTo == 'DEALER') {
	$dealer = new Dealer($db, $serialTo);
	$dealer->getData();

	$bonus_to_name = $dealer->getName_dea() . ' - ' . $dealer->getCode_dea();
	$bonus_to_subject = 'Comercializador';
} else {
	$counter = new Counter($db, $serialTo);
	$counter->getData();

	$bonus_to_name = $counter->getAux_cnt();
	$bonus_to_name = $bonus_to_name['counter'];
	$bonus_to_subject = 'Counter';
}


include DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);


//TITLE
$pdf->ezText(html_entity_decode('<b>ACTA DE PRE - LIQUIDACI&Oacute;N</b>'), 14, array('justification' => 'center'));
$pdf->ezSetDy(-20);

// DOCUMENT DATE
$logUser = new User($db, $_SESSION['serial_usr']);
$date = html_entity_decode($global_weekDaysNumb[$date['weekday']] . ', ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);


$pdf->ezText(html_entity_decode('<b>Fecha:</b> ') . $date, 11, array('justification' => 'left'));
$pdf->ezText('<b>Incentivos para:</b> ' . $bonus_to_subject, 11, array('justification' => 'left'));
$pdf->ezText('<b>Beneficiario:</b> ' . $bonus_to_name, 11, array('justification' => 'left'));
$pdf->ezSetDy(-20);

if (count($bonusToPay) > 0) {
	foreach ($bonusToPay as &$b) {
		($b['ondate_apb'] == 'YES') ? $b['ondate_apb'] = 'Si' : $b['ondate_apb'] = 'No';
		if ($b['status_apb'] == 'REFUNDED') {
			$b['pending_amount_apb'] = "-" . $b['pending_amount_apb'];
			$total_to_pay += $b['pending_amount_apb'];
			$b['pending_amount_apb'] =$b['pending_amount_apb'] . " (R)";
		} else {
			$total_to_pay += $b['pending_amount_apb'];
		}
	}

	$pdf->ezText('<b>Total por Liquidar:</b> $' . $total_to_pay, 11, array('justification' => 'center'));
	$pdf->ezSetDy(-10);

	$pdf->ezTable($bonusToPay,
			array('card_number_sal' => '<b>Tarjeta</b>',
				'name_cus' => '<b>Cliente</b>',
				'number_inv' => '<b>Factura</b>',
				'total_amount_apb' => '<b>Valor Incentivo</b>',
				'pending_amount_apb' => '<b>Valor Pendiente a la fecha</b>',
				'ondate_apb' => html_entity_decode('<b>En D&iacute;as de Cr&eacute;dito</b>')),
			'<b>DETALLE DE TARJETAS</b>',
			array('xPos' => 'center',
				'cols' => array(
					'card_number_sal' => array('justification' => 'center', 'width' => 80),
					'name_cus' => array('justification' => 'center', 'width' => 100),
					'number_inv' => array('justification' => 'center', 'width' => 80),
					'total_amount_apb' => array('justification' => 'center', 'width' => 80),
					'pending_amount_apb' => array('justification' => 'center', 'width' => 90),
					'ondate_apb' => array('justification' => 'center', 'width' => 70))));
} else {
	$pdf->ezText(html_entity_decode('No existen incentivos por cancelar.'), 10, array('justification' => 'center'));
}

$pdf->ezStream();
?>