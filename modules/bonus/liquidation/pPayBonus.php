<?php

/*
  Document   : pPayBonus
  Created on : 14-jun-2010, 14:44:25
  Author     : Nicolas
  Description:
  Register the bonus loquidation
 */
//Debug::print_r($_POST);
Request::setString('payTo');
Request::setString('maxBonus');
Request::setString('serialTo');
Request::setString('selToPay');
Request::setFloat('liquidationTotal');
$error = 1;
$prizes_applied = array();
$prizes_for_pdf = array();
$bonus_for_pdf = array();
$liquidationTotalForPdf = $liquidationTotal;
$total = $liquidationTotal;

//validate if there is enought data for the process
if ($payTo && $serialTo && $selToPay) {
	//initialize vars
	$apBonus = new AppliedBonus($db);
	$bonusToPay = array();
	$onDateBonus = array();
	$outDateBonus = array();
	//check if is going to pay bonus ON date,bonus OUT date or both.
	//get On date date
	if ($selToPay == 'BOTH' || $selToPay == 'ON') {
		$onDateBonus = $apBonus->getOnDateBonus($serialTo, $payTo);
	}
	//get out date data
	if ($selToPay == 'BOTH' || $selToPay == 'OUT') {
		$outDateBonus = $apBonus->getOutDateBonus($serialTo, $payTo);
	}

//merge the bonus if neccesary
	$bonusToPay = array_merge($onDateBonus, $outDateBonus);

//change status of refunded bonus
	foreach ($bonusToPay as &$b1) {
		foreach ($bonusToPay as $b2) {
			if ($b1['card_number_sal'] == $b2['card_number_sal'] && $b1['number_inv'] == $b2['number_inv'] && $b2['status_apb'] == 'REFUNDED' && $b1['status_apb'] == 'ACTIVE') {
				$b1['status_apb'] = 'REFUNDED';
			}
		}
	}

//check if there are pending values, and bonus to pay from
	if ($liquidationTotal > 0 && is_array($bonusToPay)) {
		//prizes data
		foreach ($_POST['chkPrizes'] as $pa) {
			$prize = new Prize($db, $pa);
			$prize->getData();
			//get data for updates
			array_push($prizes_applied, array(	'serial_pri' => $prize->getSerial_pri(),
												'name_pri' => $prize->getName_pri(),
												'coverage_pri' => $prize->getCoverage_pri(),
												'quantity' => $_POST['quantity_' . $pa]));
			//get data for PDF
			array_push($prizes_for_pdf, array(	'name_pri' => $prize->getName_pri(),
												'quantity' => ($prize->getCoverage_pri() == 'TOTAL') ? '$' . $_POST['quantity_' . $pa] : $_POST['quantity_' . $pa] . ' u'));
		}
		
		//evaluate available bonus and store for PDF
		foreach ($bonusToPay as $btp) {
			if ($btp['status_apb'] == 'ACTIVE') {
				if ($btp['pending_amount_apb'] > 0) {
					if ($btp['pending_amount_apb'] < $liquidationTotalForPdf) {
						$liquidationTotalForPdf-=$btp['pending_amount_apb'];
						$apBonus->setSerial_apb($btp['serial_apb']);
						$apBonus->getData();
						$sale = new Sales($db, $apBonus->getSerial_sal());
						$sale->getData();
						$invoice = new Invoice($db, $sale->getSerial_inv());
						$invoice->getData();
						array_push($bonus_for_pdf, array('card_number' => ($sale->getCardNumber_sal() != '') ? $sale->getCardNumber_sal() : 'N/A',
							'name_cus' => $btp['name_cus'],
							'invoice_number' => $invoice->getNumber_inv(),
							'bonus_value' => $btp['pending_amount_apb'],
							'left_value' => number_format(0, 2)));
					} else {
						$apBonus->setSerial_apb($btp['serial_apb']);
						$apBonus->getData();
						$sale = new Sales($db, $apBonus->getSerial_sal());
						$sale->getData();
						$invoice = new Invoice($db, $sale->getSerial_inv());
						$invoice->getData();
						array_push($bonus_for_pdf, array('card_number' => ($sale->getCardNumber_sal() != '') ? $sale->getCardNumber_sal() : 'N/A',
							'name_cus' => $btp['name_cus'],
							'invoice_number' => $invoice->getNumber_inv(),
							'bonus_value' => number_format($liquidationTotalForPdf, 2),
							'left_value' => number_format(($btp['pending_amount_apb'] - $liquidationTotalForPdf), 2)));
						$liquidationTotalForPdf = 0;
					}
				}
				//check if we have already paid all the bonus
				if ($liquidationTotalForPdf == 0) {
					break;
				}
			}
		}

		//create the liquidation register
		$bonusLiquidation = new BonusLiquidation($db);
		$bonusLiquidation->setSerial_usr($_SESSION['serial_usr']);
		$serialBonusLiquidation = $bonusLiquidation->insert();

		//register the bonus used
		if ($serialBonusLiquidation) {
			foreach ($bonusToPay as $bp) {
				if ($bp['status_apb'] == 'ACTIVE') {
					$appliedBonusLiquidation = new AppliedBonusLiquidation($db);
					if ($bp['pending_amount_apb'] > 0) {
						if ($bp['pending_amount_apb'] < $liquidationTotal) {
							$liquidationTotal-=$bp['pending_amount_apb'];
							$apBonus->setSerial_apb($bp['serial_apb']);
							$apBonus->getData();
							$apBonus->setPending_amount_apb('0');
							$apBonus->setStatus_apb('PAID');
							$apBonus->setPayment_date_apb(date('d/m/Y'));
							if (!$apBonus->update()) {
								$error = 2;
								break;
							} else {
								$appliedBonusLiquidation->setSerial_lqn($serialBonusLiquidation);
								$appliedBonusLiquidation->setSerial_apb($bp['serial_apb']);
								$appliedBonusLiquidation->setAmount_used_abl($bp['pending_amount_apb']);
								$serial_lqn = $appliedBonusLiquidation->insert();
								if (!$serial_lqn) {
									$error = 2;
									break;
								}
							}
						} else {
							$apBonus->setSerial_apb($bp['serial_apb']);
							$apBonus->getData();
							$pending_amount = $bp['pending_amount_apb'] - $liquidationTotal;
							$lastAmountUsed = $liquidationTotal;
							$liquidationTotal = 0;
							$apBonus->setPending_amount_apb($pending_amount);
							if ($pending_amount <= 0) {
								$apBonus->setPayment_date_apb(date('d/m/Y'));
								$apBonus->setStatus_apb('PAID');
							}
							if (!$apBonus->update()) {
								$error = 2;
								break;
							} else {
								$appliedBonusLiquidation->setSerial_lqn($serialBonusLiquidation);
								$appliedBonusLiquidation->setSerial_apb($bp['serial_apb']);
								$appliedBonusLiquidation->setAmount_used_abl($lastAmountUsed);
								$serial_lqn = $appliedBonusLiquidation->insert();
								if (!$serial_lqn) {
									$error = 2;
									break;
								}
							}
						}
					}
					//check if we have already paid all the bonus
					if ($liquidationTotal == 0) {
						break;
					}
				}else{ //ALL DUPLICATED ITEMS IN APPLIED_BONUS TABLE FOR REFUND & ACTIVE
					AppliedBonus::disableAppliedBonusInLiquidation($db, $bp['serial_apb'], $_SESSION['serial_usr']);
				}
			}

			//register the prize used
			$prizeToUpdate = new Prize($db);
			foreach ($prizes_applied as $pa) {
				$prizesLiquidation = new PrizesLiquidation($db);
				$prizeToUpdate->setSerial_pri($pa['serial_pri']);
				$prizeToUpdate->getData();
				if ($prizeToUpdate->getCoverage_pri() == "AMOUNT") {
					$prizeToUpdate->setStock_pri($prizeToUpdate->getStock_pri() - $pa['quantity']);
					if (!$prizeToUpdate->update()) {
						$error = 3;
						break;
					}
				}
				$prizesLiquidation->setSerial_pri($prizeToUpdate->getSerial_pri());
				$prizesLiquidation->setSerial_lqn($serialBonusLiquidation);
				$prizesLiquidation->setQuantity_pli($pa['quantity']);
				$prizesLiquidation->setAmount_pli($prizeToUpdate->getAmount_pri());
				$prizesLiquidation->setCost_pli($prizeToUpdate->getCost_pri());
				if (!$prizesLiquidation->insert()) {
					$error = 4;
					break;
				}
			}
		} else {
			$error = 4;
		}

		if ($error == 1) {
			//generates PDF file
			include DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';
			//get counter or dealer data.
			if ($payTo == 'COUNTER') {
				$counter = new Counter($db, $serialTo);
				$counter->getData();
				$counterUser = new User($db, $counter->getSerial_usr());
				$counterUser->getData();
				$dealer = new Dealer($db, $counter->getSerial_dea());
				$dealer->getData();
			} else {
				$dealer = new Dealer($db, $serialTo);
				$dealer->getData();
			}

			$aux = $dealer->getAuxData_dea();
			$city = new City($db, $aux['serial_cit']);
			$city->getData();
			$country = new Country($db, $aux['serial_cou']);
			$country->getData();
			//Date
			$logUser = new User($db, $_SESSION['serial_usr']);
			$logUser->getData();
			$city = new City($db, $logUser->getSerial_cit());
			$city->getData();
			$cityName = utf8_decode(ucfirst($city->getName_cit()));
			$date = html_entity_decode("$cityName, " . $global_weekDaysNumb[$date['weekday']] . ' ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);
			$pdf->ezStopPageNumbers();
			$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);
			$pdf->ezSetDy(-30);
			//Code
			$digits = 6 - strlen($serial_lqn);
			for ($i = 0; $i < $digits; $i++) {
				$code .= "0";
			}
			$pdf->ezText($country->getCode_cou() . "-" . $city->getCode_cit() . "-INC-" . $code . $serial_lqn, 12, array('justification' => 'right'));
			//date
			$pdf->ezText($date);
			$pdf->ezSetDy(-20);


			if ($payTo == 'COUNTER') {
				$pdf->ezText(utf8_decode('Sr.(a):'));
				$pdf->ezText("<b>{$counterUser->getFirstname_usr()} {$counterUser->getLastname_usr()}</b>");
				$pdf->ezText("<b>Comercializador {$dealer->getName_dea()}</b>");
			} else {
				$pdf->ezText(utf8_decode('Señores:'));
				$pdf->ezText("<b>{$dealer->getName_dea()}</b>");
			}
			$pdf->ezText("Presente.-");
			$pdf->ezSetDy(-10);
			$pdf->ezText(utf8_decode("De mi consideración:"));
			$pdf->ezSetDy(-10);
			$pdf->ezText(utf8_decode("Por medio de la presente le expresamos nuestro agradecimiento por el apoyo y confianza brindada a $global_system_name.  A su vez, premiar su fidelidad, con la entrega de <b>US$ $total de incentivo en:</b>"));
			$pdf->ezSetDy(-20);
			$pdf->ezTable($prizes_for_pdf,
					array('name_pri' => '<b>Premio</b>',
						'quantity' => '<b>Cantidad/Valor</b>'),
					'',
					array('xPos' => 'center',
						'cols' => array(
							'name_pri' => array('justification' => 'center', 'width' => 80),
							'quantity' => array('justification' => 'center', 'width' => 50))));
			$pdf->ezSetDy(-10);
			$pdf->ezText("Correspondiente a:");
			$pdf->ezSetDy(-10);
			$pdf->ezText("DETALLE DE TARJETAS:");
			$pdf->ezSetDy(-10);
			$pdf->ezTable($bonus_for_pdf,
					array('card_number' => '<b>Tarjeta</b>',
						'name_cus' => '<b>Cliente</b>',
						'invoice_number' => '<b>Factura</b>',
						'bonus_value' => '<b>Valor Incentivo</b>',
						'left_value' => '<b>Valor Pendiente a la fecha</b>'),
					'',
					array('xPos' => 'center',
						'cols' => array(
							'card_number' => array('justification' => 'center', 'width' => 80),
							'name_cus' => array('justification' => 'center', 'width' => 100),
							'invoice_number' => array('justification' => 'center', 'width' => 60),
							'bonus_value' => array('justification' => 'center', 'width' => 60),
							'left_value' => array('justification' => 'center', 'width' => 60))));

			$pdf->ezSetDy(-10);
			$pdf->ezText(utf8_decode("Los resultados evidencian que el esfuerzo mancomunado ha tenido el éxito deseado, lo que conlleva nuestro compromiso para servirles cada vez mejor."));
			$pdf->ezSetDy(-20);
			$pdf->ezText("Atentamente,");
			//CODE FOR SIGN AS PLANET ASSIST OR BLUECARD
			if($payTo == 'DEALER'){
				$dealerToPay = new Dealer($db,$serialTo);
				$dealerToPay ->getData();
				$managerByCountry = new ManagerbyCountry($db,$dealerToPay->getSerial_mbc());
				$managerByCountry->getData();
				if($managerByCountry->getSerial_cou() == '62'){
					$sign_as='BLUECARD';
				}else{
					$sign_as='PLANETASSIST';
				}
			}else{
				$counterToPay = new Counter($db,$serialTo);
				$counterToPay->getData();
				$dealerToPay = new Dealer($db,$counterToPay->getSerial_dea());
				$dealerToPay ->getData();
				$managerByCountry = new ManagerbyCountry($db,$dealerToPay->getSerial_mbc());
				$managerByCountry->getData();
				if($managerByCountry->getSerial_cou() == '62'){
					$sign_as='BLUECARD';
				}else{
					$sign_as='PLANETASSIST';
				}
			}
			//
			$pdf->ezText(utf8_decode($sign_as));
			$pdf->ezSetDy(-40);
			$pdf->ezText(utf8_decode($logUser->getFirstname_usr() . ' ' . $logUser->getLastname_usr()));
			$pdf->ezText("Adjunto: Lo indicado");





			$pdf->ezStream();
		} else {
			http_redirect('main/bonusLiquidation/' . $error);
		}
	}
} else {
	$error = 4;
	http_redirect('main/bonusLiquidation/' . $error);
}
?>
