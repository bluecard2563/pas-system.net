<?php
/**
 * @author Patricio Astudillo
 */

Request::setInteger('0:error');

$pendingLiquidation = BonusLiquidation::getAllPendingBonusLiquidation($db, $_SESSION['serial_mbc']);

$smarty->register('pendingLiquidation,error');
$smarty->display();
