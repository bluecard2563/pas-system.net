<?php
/**
 * Name: printLiquidationPurchaseList
 * Created by: pastudillo
 * Created on: 17-ene-2017
 * Description: PDF File for purchase List
 * Modified by: pastudillo
 * Modified on: 17-ene-2017
 */

$checkedLiquidation = $_POST['chkBonusLiquidation'];
$liquidationIds = implode(',', $checkedLiquidation);

$purchaseListDetails = BonusLiquidation::getPrizeResumeForLiquidations($db, $liquidationIds);

$detailCards = array();
$detailCards = BonusLiquidation::getAllSalesByLqn($db, $liquidationIds);
//Debug::print_r($detailCards);die();

    
//**************** MAKING THE PDF
include DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);

//TITLE
$pdf->ezText(html_entity_decode('<b>LISTADO DE COMPRAS DE INCENTIVOS</b>'), 14, array('justification' => 'center'));
$pdf->ezSetDy(-20);

$date = html_entity_decode($global_weekDaysNumb[$date['weekday']] . ', ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);

$pdf->ezText(html_entity_decode('<b>Fecha:</b> ') . $date, 11, array('justification' => 'left'));
//$pdf->ezText('<b>Incentivos para:</b> Comercializador', 11, array('justification' => 'left'));
//$pdf->ezText('<b>Beneficiario:</b> ' . $detailCards->, 11, array('justification' => 'left'));
$pdf->ezSetDy(-20);

if ($purchaseListDetails) {
    $prizeResume = array();
    
	foreach ($purchaseListDetails as $detail) {
		if(!isset($prizeResume[$detail['serial_pri']])){
            $prizeResume[$detail['serial_pri']]['qty'] = 0;
            $prizeResume[$detail['serial_pri']]['name'] = $detail['name_pri'];
            $prizeResume[$detail['serial_pri']]['cost'] = 0;
        }
        
        $prizeResume[$detail['serial_pri']]['qty'] += $detail['qty'];
        $prizeResume[$detail['serial_pri']]['cost'] += $detail['cost'];
	}
	
	//Debug::print_r($prizeResume); die;
    
    $pdf->ezTable($prizeResume,
			array(  'name' => '<b>Premio</b>',
					'qty' => '<b>Cantidad Unidades</b>',
					'cost' => '<b>Precio de Compra</b>'),
				'<b>DETALLE DE PREMIOS</b>',
			array(	'xPos' => 'center',
					'cols' => array(
						'name' => array('justification' => 'center', 'width' => 150),
						'qty' => array('justification' => 'center', 'width' => 80),
						'cost' => array('justification' => 'center', 'width' => 80))));

    $pdf->ezSetDy(-10);
    
	$pdf->ezTable($purchaseListDetails,
			array('serial_lqn' => '<b>Nro. Liquidacion</b>',
					'name_pri' => '<b>Premio</b>',
					'qty' => '<b>Cantidad Unidades</b>',
					'cost' => '<b>Precio de Compra</b>'),
				'<b>DETALLE DE LIQUIDACIONES</b>',
			array(	'xPos' => 'center',
					'cols' => array(
						'serial_lqn' => array('justification' => 'center', 'width' => 80),
						'name_pri' => array('justification' => 'center', 'width' => 150),
						'qty' => array('justification' => 'center', 'width' => 80),
						'cost' => array('justification' => 'center', 'width' => 80))));
    
    $pdf->ezSetDy(-10);
    
//    $pdf->ezTable($detailCards,
//			array('tarjeta' => '<b>Tarjeta</b>',
//				'customer' => '<b>Cliente</b>',
//				'invoice' => '<b>Factura</b>',
//				'amount' => '<b>Valor Incentivo</b>'),
//			'<b>DETALLE DE TARJETAS</b>',
//			array('xPos' => 'center',
//				'cols' => array(
//					'tarjeta' => array('justification' => 'center', 'width' => 80),
//					'customer' => array('justification' => 'center', 'width' => 150),
//					'invoice' => array('justification' => 'center', 'width' => 80),
//					'amount' => array('justification' => 'center', 'width' => 80))));
} else {
	$pdf->ezText(html_entity_decode('No existen incentivos por cancelar.'), 10, array('justification' => 'center'));
}

$pdf->ezStream();