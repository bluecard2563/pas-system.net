<?php
/**

 */

$checkedLiquidation = $_POST['chkBonusLiquidation'];
$liquidationIds = implode(',', $checkedLiquidation);
$appliedbonus_liquidation = new AppliedBonusLiquidation($db);
$bonusLiquidation = new BonusLiquidation($db,$checkedLiquidation[0]);
$bonusLiquidation->getData();
$bondata = $bonusLiquidation->getStatus_lqn();

$detailCards = array();
$detailCards = BonusLiquidation::getAllSalesByLqn($db, $liquidationIds);
//if($bondata != 'PAID'){
foreach ($checkedLiquidation as &$checks) {
    
$dataabl = $appliedbonus_liquidation ->getInfo($db,$checks);

$apBonus = new AppliedBonus($db);
if ($dataabl){
    foreach ($dataabl as &$bonus) {
        $apBonus->setSerial_apb($bonus['serial_apb']);
	$apBonus->getData();
        $pending_amount = $apBonus->getPending_amount_apb()- $bonus['amount_used_abl'];
        
        $apBonus->setPending_amount_apb($pending_amount);
        $apBonus->setPayment_date_apb(date('Y-m-d H:i:s'));
	$apBonus->setStatus_apb('PAID');
        if (!$apBonus->update()) {
                $error = 2;
                http_redirect('main/bonusLiquidation/' . $error);
		break;
	} 
    }
$bonusLiquidation = new BonusLiquidation($db);
$date = date('Y/m/d H:i:s');
    $bonusLiquidation->updateliquidation($checks, $_SESSION['serial_usr'],$date);
        }
else{
    $error = 5;
    http_redirect('main/bonusLiquidation/' . $error);
}
    $bonusLiquidation = new BonusLiquidation($db);
    $bondata = $bonusLiquidation->getDataLiq($db,$checks);
    $total_bon = $bondata['total_lqn'];
    $request_usr = $bondata['request_user'];
    $request_dea = $bondata['dealer'];
    $prizes_for_pdf = array();
    $prize = new PrizesLiquidation($db);
    $prizes = $prize->getPrizesbyLiquidation($checks);

    //generates PDF file
        $global_system_name='BLUECARD';
    $global_system_logo=DOCUMENT_ROOT."img/contract_logos/logoblue_contracts.jpg";
    $pdf =& new Cezpdf('A4','portrait');
//WIDTH: 600
//HEIGHT: 800
set_time_limit(36000);
ini_set('memory_limit','1024M');

$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
$pdf -> ezSetCmMargins(4,3,2,1);
$all = $pdf->openObject();
$pdf->saveState();

if(!$clean_pdf){
	$pdf->addJpegFromFile($global_system_logo,375,750,170);
	$pdf->setColor(0, 0.33, 0.64);
	$pdf->addText(60, 765, 16, '<b>SISTEMA '.$global_system_name.'</b>');
	$pdf->setColor(0.57, 0.58, 0.60);
}
$pdf->setColor(0, 0, 0);

$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
$pdf->restoreState();
$pdf->closeObject();
$pdf->addObject($all,'all');

    //$date = html_entity_decode($global_weekDaysNumb[$date['weekday']] . ' ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);
			$pdf->ezStopPageNumbers();
			$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);
			$pdf->ezSetDy(-30);
			//Code
                        $counter = new Counter($db, $_SESSION['serial_usr']);
			$counter->getData();
			$counterUser = new User($db, $counter->getSerial_usr());
			$counterUser->getData();
			$dealer = new Dealer($db, $counter->getSerial_dea());
			$dealer->getData();
                        $aux = $dealer->getAuxData_dea();
                        $dealer1 = new Dealer($db,$request_dea);
			$dealer1->getData();
                        $name_dea = $dealer1 ->getName_dea();
			$city = new City($db, $aux['serial_cit']);
			$city->getData();
			$country = new Country($db, $aux['serial_cou']);
			$country->getData();
			$pdf->ezText($country->getCode_cou() . "-" . $city->getCode_cit() . "-INC-" . $code . $bonus['serial_lqn'], 12, array('justification' => 'right'));
			//date
			$pdf->ezText($date);
			$pdf->ezSetDy(-20);
                        $pdf->ezText(utf8_decode('Sr.(a):'));
                        $pdf->ezText("<b>$request_usr</b>");
                        $pdf->ezSetDy(-10);
                        $pdf->ezText("<b>$name_dea</b>");
			$pdf->ezSetDy(-10);
                        $pdf->ezText("Presente.-");
			$pdf->ezSetDy(-10);
			$pdf->ezText(utf8_decode("De mi consideración:"));
			$pdf->ezSetDy(-10);
			$pdf->ezText(utf8_decode("Por medio de la presente le expresamos nuestro agradecimiento por el apoyo y confianza brindada a BLUE CARD.  "
                                . "A su vez, premiar su fidelidad, con la entrega de <b>US$ $total_bon </b> de incentivos de la siguiente manera:"));
			$pdf->ezSetDy(-20);
                        
			$pdf->ezTable($prizes,
					array('name_pri' => '<b>Premio</b>',
						'quantity_pli' => '<b>Cantidad</b>'),
					'',
					array('xPos' => 'center',
						'cols' => array(
							'name_pri' => array('justification' => 'center', 'width' => 180),
							'quantity_pli' => array('justification' => 'center', 'width' => 90))));
//			$pdf->ezSetDy(-10);
//			$pdf->ezText("DETALLE DE TARJETAS:");
			$pdf->ezSetDy(-10);
            
                $pdf->ezTable($detailCards,
			array('tarjeta' => '<b>Tarjeta</b>',
				'customer' => '<b>Cliente</b>',
				'invoice' => '<b>Factura</b>',
				'amount' => '<b>Valor Incentivo</b>'),
			'<b>DETALLE DE TARJETAS</b>',
			array('xPos' => 'center',
				'cols' => array(
					'tarjeta' => array('justification' => 'center', 'width' => 80),
					'customer' => array('justification' => 'center', 'width' => 180),
					'invoice' => array('justification' => 'center', 'width' => 80),
					'amount' => array('justification' => 'center', 'width' => 80))));
//			$pdf->ezTable($bonus_for_pdf,
//					array('card_number' => '<b>Tarjeta</b>',
//						'name_cus' => '<b>Cliente</b>',
//						'invoice_number' => '<b>Factura</b>',
//						'bonus_value' => '<b>Valor Incentivo</b>',
//						'left_value' => '<b>Valor Pendiente a la fecha</b>'),
//					'',
//					array('xPos' => 'center',
//						'cols' => array(
//							'card_number' => array('justification' => 'center', 'width' => 80),
//							'name_cus' => array('justification' => 'center', 'width' => 100),
//							'invoice_number' => array('justification' => 'center', 'width' => 60),
//							'bonus_value' => array('justification' => 'center', 'width' => 60),
//							'left_value' => array('justification' => 'center', 'width' => 60))));

			$pdf->ezSetDy(-10);
			$pdf->ezText(utf8_decode("Los resultados evidencian que el esfuerzo mancomunado ha tenido el éxito deseado, lo que conlleva nuestro compromiso para servirles cada vez mejor."));
			$pdf->ezSetDy(-20);
			$pdf->ezText("Atentamente,");
			
			$logUser = new User($db, $_SESSION['serial_usr']);
			$logUser->getData();
			$sign_as='BLUECARD';
                        $pdf->ezText(utf8_decode($sign_as));
			$pdf->ezSetDy(-40);
			$pdf->ezText(utf8_decode($logUser->getFirstname_usr() . ' ' . $logUser->getLastname_usr()));
			$pdf->ezText("Adjunto: Lo indicado");
                        $pdf->ezStream();                    
    
}//end foreach
//}else{
//
//$error = 7;
//http_redirect('main/bonusLiquidation/' . $error);
//
//}

