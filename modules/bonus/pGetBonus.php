<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Request::setFloat('liquidationTotal');
Request::setString('serialTo');
Request::setString('selToPay');
$bonus_liquidation = new BonusLiquidation($db);
$hasPendingLiquidation = $bonus_liquidation->hasPendingLiquidation($db, $_SESSION['serial_dea']);

//**************** CONVERT POINTS TO MONEY INTO POINTS
$param = new Parameter($db, 53);
$param->getData();
$total_lqn = $liquidationTotal;
$liquidationTotal = $liquidationTotal / $param->getValue_par();


if($hasPendingLiquidation){
    $error = 5;
    http_redirect('main/bonusLiquidation/' . $error);
    
}
 else {
//initialize vars
$apBonus = new AppliedBonus($db);
$bonusToPay = array();
$prizes_applied = array();
        
//create the preliquidation register
$bonusLiquidation = new BonusLiquidation($db);
$bonusLiquidation->setSerial_usr(NULL);
$bonusLiquidation->setStatus_lqn('PENDING');
$bonusLiquidation->setRequest_date_lqn(date('d/m/Y'));
$bonusLiquidation->setRequest_usr($_SESSION['serial_usr']);
$bonusLiquidation->setRequest_dea($_SESSION['serial_dea']);
$bonusLiquidation->setSerial_mbc($_SESSION['serial_mbc']);
$bonusLiquidation->setTotal_lqn($liquidationTotal);

//*********** GET AVAILABLE POINTS FOR USER
$bonusToPay = AppliedBonus::getAvailableBonusForUser1($db, $_SESSION['serial_usr']);
//Debug::print_r($bonusToPay);DIE();
//change status of refunded bonus
	foreach ($bonusToPay as &$b1) {
		foreach ($bonusToPay as $b2) {
			if ($b1['card_number_sal'] == $b2['card_number_sal'] && $b1['number_inv'] == $b2['number_inv'] && $b2['status_apb'] == 'REFUNDED' && $b1['status_apb'] == 'ACTIVE') {
				$b1['status_apb'] = 'REFUNDED';
			}
		}
	}
        
//prizes data
	foreach ($_POST['chkPrizes'] as $pa) {
		$prize = new Prize($db, $pa);
		$prize->getData();
		//get data for updates
		array_push($prizes_applied, array(	'serial_pri' => $prize->getSerial_pri(),
					'name_pri' => $prize->getName_pri(),
					'coverage_pri' => $prize->getCoverage_pri(),
					'quantity' => $_POST['quantity_' . $pa]));
	}    
$error = 1;        
//Insert Bonus Preliquidation
$serialBonusLiquidation = $bonusLiquidation->insert();

//register the bonus used
if ($serialBonusLiquidation) {
    foreach ($bonusToPay as $bp) {
	if ($bp['status_apb'] == 'ACTIVE') {
		$appliedBonusLiquidation = new AppliedBonusLiquidation($db);
		if ($bp['pending_amount_apb'] > 0) {
			if ($bp['pending_amount_apb'] < $liquidationTotal) {
				$liquidationTotal-=$bp['pending_amount_apb'];
				$apBonus->setSerial_apb($bp['serial_apb']);
				$apBonus->getData();
				//$apBonus->setPending_amount_apb('0');
				//$apBonus->setStatus_apb('PAID');
				//$apBonus->setPayment_date_apb(date('Y-m-d H:i:s'));
					if (!$apBonus->update()) {
                                            
						$error = 2;
                                                break;
					} else {
						$appliedBonusLiquidation->setSerial_lqn($serialBonusLiquidation);
						$appliedBonusLiquidation->setSerial_apb($bp['serial_apb']);
						$appliedBonusLiquidation->setAmount_used_abl($bp['pending_amount_apb']);
						$serial_lqn = $appliedBonusLiquidation->insert();
						if (!$serial_lqn) {
                                                    
							$error = 2;
							break;
							}
						}
			} else {
				$apBonus->setSerial_apb($bp['serial_apb']);
				$apBonus->getData();
				$pending_amount = $bp['pending_amount_apb'] - $liquidationTotal;
				$lastAmountUsed = $liquidationTotal;
				$liquidationTotal = 0;
				//$apBonus->setPending_amount_apb($pending_amount);
				if ($pending_amount <= 0) {
					//$apBonus->setPayment_date_apb(date('Y-m-d H:i:s'));
					//$apBonus->setStatus_apb('PAID');
                                        //$apBonus->setStatus_apb('PAID');
				}
				if (!$apBonus->update()) {
                                    
					$error = 2;
					break;
				} else {
					$appliedBonusLiquidation->setSerial_lqn($serialBonusLiquidation);
					$appliedBonusLiquidation->setSerial_apb($bp['serial_apb']);
					$appliedBonusLiquidation->setAmount_used_abl($lastAmountUsed);
					$serial_lqn = $appliedBonusLiquidation->insert();
                                        if (!$serial_lqn) {
                                            
                                            $error = 2;
                                            break;
                                        }
					}
				}
		}
		//check if we have already paid all the bonus
		if ($liquidationTotal == 0) {
			break;
		}
		}else{ //ALL DUPLICATED ITEMS IN APPLIED_BONUS TABLE FOR REFUND & ACTIVE
			AppliedBonus::disableAppliedBonusInLiquidation($db, $bp['serial_apb'], $_SESSION['serial_usr']);
		}
    }

//register the prize used
$prizeToUpdate = new Prize($db);
    foreach ($prizes_applied as $pa) {
	$prizesLiquidation = new PrizesLiquidation($db);
	$prizeToUpdate->setSerial_pri($pa['serial_pri']);
	$prizeToUpdate->getData();
	if ($prizeToUpdate->getCoverage_pri() == "AMOUNT") {
		$prizeToUpdate->setStock_pri($prizeToUpdate->getStock_pri() - $pa['quantity']);
		if (!$prizeToUpdate->update()) {
			$error = 3;
			break;
		}
	}
	$prizesLiquidation->setSerial_pri($prizeToUpdate->getSerial_pri());
	$prizesLiquidation->setSerial_lqn($serialBonusLiquidation);
	$prizesLiquidation->setQuantity_pli($pa['quantity']);
	$prizesLiquidation->setAmount_pli($prizeToUpdate->getAmount_pri());
	$prizesLiquidation->setCost_pli($prizeToUpdate->getCost_pri());
	if (!$prizesLiquidation->insert()) {
		$error = 4;
		break;
	}
    }
} else {
	$error = 4;
}

$user = new User($db, $_SESSION['serial_usr']);
$user -> getData(true);
//SEND THE CORRESPONDING EMAIL
    $misc['serial_lqn'] = $serialBonusLiquidation;
    $misc['points'] = $total_lqn;
    $misc['date'] = date('d/m/Y');
    $misc['email'] = $user -> getEmail_usr();
    
    if(!GlobalFunctions::sendMail($misc, 'bonuspreliquidation')){
	    	$error = 2;
	    }

http_redirect('main/bonusLiquidation/' . $error.'/'.$serialBonusLiquidation);
}