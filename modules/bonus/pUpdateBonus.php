<?php
/*
File: pUpdateBonus.php
Author: David Bergmann
Creation Date: 19/03/2010
Last Modified: 12/07/2011
Modified By: Santiago Arellano
*/
Request::setString('selectedBoxes');

$selectedBoxes = str_replace('&', ',', str_replace('serial=', '', $selectedBoxes));
$chkBonus = explode(",", $selectedBoxes);

foreach($chkBonus as $s){
    $bonus = new Bonus($db,$s);
    $bonus->getData();
    $serial_dea = $bonus->getSerial_dea();
    $serial_pxc = $bonus->getSerial_pxc();


    if($bonus->deactivate()) {
        $newBonus = new Bonus($db);
        $newBonus->setSerial_dea($serial_dea);
        $newBonus->setSerial_pxc($serial_pxc);
        $newBonus->setPercentage_bon($_POST['txtNewPercentage']);
        if(!$newBonus->insert()) {
            http_redirect('modules/bonus/fSearchBonus/2');
        }
    }
}
http_redirect('modules/bonus/fSearchBonus/1');
?>
