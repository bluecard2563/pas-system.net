<?php
/*

*/

$bonus = new Bonus($db);
$bonus->setPercentage_bon($_POST['txtPercentage']);

$dealerList = $_POST['dealersTo'];
$productList = $_POST['productsTo'];
$i=0;
//Debug::print_r($dealerList);
//Debug::print_r($productList);
set_time_limit(36000);
foreach($dealerList as $dl){
    foreach($productList as $pl){
        $bonus = new Bonus($db);
        $bonus->setPercentage_bon($_POST['txtPercentage']);
        $bonus->setSerial_dea($dl);
        $bonus->setSerial_pxc($pl);
        if($bonus->bonusExists()) {
            //Adds existent bonuses into an array
            $existentBonus = new Bonus($db, $bonus->bonusExists());
            $existentBonus->getData();

            //Dealer name
            $dealer = new Dealer($db,$existentBonus->getSerial_dea());
            $dealer->getData();
            $name_dea = $dealer->getName_dea();

            $productByCountry = new ProductByCountry($db,$existentBonus->getSerial_pxc());
            $productByCountry->getData();

            //Product name
            $product = new Product($db, $productByCountry->getSerial_pro());
            $productData = $product->getInfoProduct($_SESSION['language']);
            $name_pro = $productData[0]['name_pbl'];

            //City name
            $sector = new Sector($db,$dealer->getSerial_sec());
            $sector->getData();
            $city = new City($db,$sector->getSerial_cit());
            $city->getData();
            $name_cit = $city->getName_cit();

            //Country name
            $country = new Country($db,$city->getSerial_cou());
            $country->getData();
            $name_cou = $country->getName_cou();

            //Percentage
            $percentage = $existentBonus->getPercentage_bon();

            $_SESSION['bonus'][$i] = array('name_cou'=>$name_cou,
                                           'name_cit'=>$name_cit,
                                           'name_dea'=>$name_dea,
                                           'name_pxc'=>$name_pro,
                                           'percentage'=>$percentage);
            $i++;
        } else { // If bonus doesn't exist
            if(!$bonus->insert()){
                http_redirect('modules/bonus/fNewBonus/2');
            }
            else {
                $error = 1;
            }
        }
    }
}
if($error) {
    http_redirect('modules/bonus/fNewBonus/1');
} else {
    http_redirect('modules/bonus/fNewBonus/3');
}

?>