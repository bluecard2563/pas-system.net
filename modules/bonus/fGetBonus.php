<?php 

if(isset($_SESSION['serial_usr'])){
    $user = new User($db , $_SESSION['serial_usr']);
    $user->getData();
    $isLeader = $user ->getIsLeader_usr();
    
}
$serial_dea = $_SESSION['serial_dea'];
if($serial_dea) {
    $dealer = new Dealer($db, $_SESSION['serial_dea']);
    $dealer -> getData();
    $bonusTo = $dealer ->getBonus_to_dea();
    $aux=$dealer->getAuxData_dea();
}

Request::setInteger('0:error');

//*********** GET AVAILABLE POINTS FOR USER
$amountAvailable = AppliedBonus::getAvailableBonusForUser1($db, $_SESSION['serial_usr'], true);
//Debug::print_r($amountAvailable);die();

//**************** CONVERT MONEY INTO POINTS
$param = new Parameter($db, 53);
$param->getData();

//QUICK FIX FOR ALWAYS ROUND DOWN THE AVAILABLE POINTS. EJ. 30.9 -> 30
$totalForBonus = $amountAvailable['amount_available'];
$availablePoints = (int)($totalForBonus * $param->getValue_par());

//Get Prizes
$prizes=new Prize($db);
$availablePrizes=$prizes->getBonusPrizesByCountry($aux['serial_cou'],$totalForBonus, $param->getValue_par());


$smarty->register('error,serial_dea,isLeader,bonusTo,availablePoints,availablePrizes');
$smarty->display();
?>
