<?php
/*
File: fUpdateZone.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 05/01/2010 16:59
Modified By: David Bergmann
*/
$serial_zon=$_POST['selZone'];
$zone = new Zone($db);
$zone->setSerial_zon($serial_zon);
	
if($zone->getData()){	
	$data['serial_zon'] = $zone -> getSerial_zon();
	$data['name_zon'] = $zone -> getName_zon();
	//Debug::print_r($data);
}else{
	http_redirect('modules/zone/fSearchZone/2');
}

$smarty -> register('data');
$smarty -> display();

?>