<?php
/*
File: fSearchZone.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 04/01/2010
Modified By: Patricio Astudillo
*/

Request::setInteger('0:error');
$zone=new Zone($db);
$zoneList=$zone->getZones();
$smarty -> register('error,zoneList');
$smarty -> display();				
?>
