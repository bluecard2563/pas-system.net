<?php
/*
File: fUpdateZone.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 
Modified By:
*/

$zone = new Zone($db);
     
$data['serial_zon'] = $_POST['hdnSerial_zon'];
$zone -> setSerial_zon($data['serial_zon']);
$zone -> getData();
$data['name_zon'] = $_POST['txtNameZone'];
$zone -> setName_zon($data['name_zon']);

if($zone -> update()){
	http_redirect("modules/zone/fSearchZone/1");
}else{
	$error = 1;
}

$smarty->register('error,data');
$smarty->display('modules/zone/fUpdateZone.'.$_SESSION['language'].'.tpl')
?>