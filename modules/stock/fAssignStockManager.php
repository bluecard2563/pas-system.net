<?php
/*
File: fAssignStockCountry.php
Author: Nicolas Flores
Creation Date: 13/01/2010
Last Modified:
Modified By:
*/
Request::setInteger('0:error');

$zone = new Zone($db);
$stock=new Stock($db);

$nameZoneList=$zone->getZones();
$typeList=$stock->getTypeValues();
$availableStock=array();

foreach ($typeList as $tl){
    $availableStock[$tl]=$stock->getAvailableForManager($tl);
}

if(isset($_SESSION['assigned_stock_ranges'])){
	$smarty->assign('assigned_ranges',$_SESSION['assigned_stock_ranges']);
	unset($_SESSION['assigned_stock_ranges']);
}

$smarty -> register('nameZoneList,typeList,availableStock,error');
$smarty -> display();
?>