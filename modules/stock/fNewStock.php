<?php
/*
File: fNewStock.tpl
Author: Nicolas Flores
Creation Date: 06/01/2010
Last Modified: 06/01/2010
*/
Request::setInteger('0:error');

$stock=new Stock($db);
$typeList=$stock->getTypeValues();

if(isset($_SESSION['assigned_stock_general_ranges'])){
	$from=$_SESSION['assigned_stock_general_ranges']['from'];
	$to=$_SESSION['assigned_stock_general_ranges']['to'];
	unset($_SESSION['assigned_stock_general_ranges']);
}

$smarty->register('error,typeList,from,to');
$smarty->display();
?>
