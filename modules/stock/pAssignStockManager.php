<?php

/*
File: pAssignStockCountry.php
Author: Nicolas Flores
Creation Date: 14/01/2010
Modified By: Nicolas Flores
Last Modified: 19/02/2010
*/

//flag to check if the process was correct
$flag=0;
$stock= new Stock($db);
//get info from the form
$type=$_POST['selType'];
$serial_mbc=$_POST['selManager'];
$amount=$_POST['txtAmountStock'];
$error=$stock->stockAssign($amount,$type,'MANAGER',$serial_mbc,true);

http_redirect('modules/stock/fAssignStockManager/'.$error);
?>