<?php
/*
File: fAssignStockDealer.php
Author: Nicolas Flores
Creation Date: 14/01/2010
Modified by:Nicolas Flores
Last Modified:25/01/2010
*/

Request::setInteger('0:error');
$country=new Country($db);
$stock=new Stock($db);
$countryList=$country->getOwnCountries($_SESSION['countryList']);
$typeList=$stock->getTypeValues();

if(isset($_SESSION['assigned_stock_ranges'])) {
	$smarty->assign('assigned_ranges',$_SESSION['assigned_stock_ranges']);
	unset($_SESSION['assigned_stock_ranges']);
}

if(isset($_SESSION['documentURL'])) {
	$smarty->assign('documentURL',$_SESSION['documentURL']);
	unset($_SESSION['documentURL']);
}

$smarty -> register('countryList,typeList,error');
$smarty -> display();
?>
