<?php
/*
File: pNewStock.tpl
Author: Nicolas Flores
Creation Date: 06/01/2010
Modified By: Nicolas Flores
Last Modified: 19/02/2010
*/

$stock= new Stock($db);
$type= $_POST['selType'];
$amount=$_POST['txtAmountStock'];
$error=$stock->stockAssign($amount,$type,'GENERAL');

http_redirect('modules/stock/fNewStock/'.$error);
?>