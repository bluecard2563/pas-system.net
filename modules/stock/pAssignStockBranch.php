<?php
/*
File:pAssignStockBranch.php
Author: Nicolas Flores
Creation Date: 02/02/2010
Modified By: David Bergmann
Last Modified: 13/01/2011
*/
//flag to check if the process was correct
$flag=0;
$stock= new Stock($db);
// Get form Data
$dea_serial_dea = $_POST['selBranch']; // Branch serial
$stockAmount = $_POST['txtAmountStock']; // stock Amount
$type = $_POST['selType']; //stock type
$error=$stock->stockAssign($stockAmount,$type,'BRANCH',$dea_serial_dea,true);

//If the stock is 'MANUAL' the same amount will be assigned to the branch as kits
if($_POST['selType'] == "MANUAL" && $error == 1) {
	$kit = new KitByDealer($db);
	$kit->setSerial_dea($dea_serial_dea);
	if($kit->getDataByDealer()){
		$serial_kbd=$kit->getSerial_kbd();
	}else{
		$kit->setTotal_kbd(0);
		$serial_kbd=$kit->insert();

		if(!$serial_kbd){ //FAILED CREATING THE KITS
			http_redirect('modules/stock/fAssignStockBranch/2');
		}
	}

	if(GlobalFunctions::assignStockKits($db, $_POST['selBranch'], $serial_kbd, 'GIVEN', $_POST['txtAmountStock'])){
		$error=1;
	}else{ //FAILED ASSIGN THE NEW KITS
		$error=3;
	}
}

include('modules/stock/pCreateManualStockDocumentPDF.php');

if(!$direct_asignation){
	http_redirect('modules/stock/fAssignStockBranch/'.$error);
}
?>