<?php
/*
File: pCreateManualStockDocumentPDF
Author: David Bergmann
Creation Date: 25/01/2011
Last Modified:
Modified By:
*/

$clean_pdf=TRUE;
include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

$dealer = new Dealer($db, $dea_serial_dea);
$dealer->getData();
$companyName = $dealer->getName_dea();

foreach ($_SESSION['assigned_stock_ranges'] as $k=>$a) {
	$ranges .= "N&ordm; {$a['from']} al {$a['to']}, ";
}

$user = new User($db,$_SESSION['serial_usr']);
$user->getData();

$param_name = $user->getFirstname_usr()." ".$user->getLastname_usr();
$param_charge = $user->getPosition_usr();


$pdf->ezSetCmMargins(4,3,2.5,2.5);
$date = $global_weekDaysNumb[date('N')].", ".date('d')." de ".$global_weekMonths[date('n')]." de ".date('Y');
$pdf->ezText(html_entity_decode($date), 11, array('justification'=>'left'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("Se&ntilde;ores:"), 11, array('justification'=>'left'));


$pdf->ezText("<b>".html_entity_decode($companyName)."</b>", 11, array('justification'=>'left'));
$pdf->ezText(html_entity_decode("Presente.-"), 11, array('justification'=>'left'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("De mi consideraci&oacute;n"), 11, array('justification'=>'left'));
$pdf->ezSetDy(-40);
$pdf->ezText(html_entity_decode("Adjunto a la presente hago entrega de {$stockAmount} contratos de adhesi&oacute;n manuales, con la siguiente numeraci&oacute;n: {$ranges}mismos que est&aacute;n bajo responsabilidad de {$companyName}."), 11, array('justification'=>'full'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("Cada uno de los contratos de adhesi&oacute;n debe ser llenado en su totalidad, y, en caso de anulaci&oacute;n, deber&aacute;n ser devuelto a BLUE CARD, incluyendo el contrato original para su respectivo proceso, previa justificaci&oacute;n."), 11, array('justification'=>'full'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("El Comercializador deber&aacute; enviar, cada vez que realice una venta, v&iacute;a fax o por cualquier medio electr&oacute;nico, para la respectiva activaci&oacute;n y facturaci&oacute;n.  Posteriormente verificar&aacute; la recepci&oacute;n por parte de Blue Card."), 11, array('justification'=>'full'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("Cabe reiterar que cada uno de los contratos de adhesi&oacute;n constituye una especie valorada que queda a entera responsabilidad de {$companyName}, y que, en caso de existir faltantes de los mismos, BLUE CARD se reserva el derecho de realizar los cobros respectivos por el extrav&iacute;o o mala utilizaci&oacute;n de los mismos."), 11, array('justification'=>'full'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("Sin otro particular y agradeciendo su confianza, me suscribo."), 11, array('justification'=>'full'));
$pdf->ezSetDy(-20);
$pdf->ezText(html_entity_decode("Atentamente,"), 11, array('justification'=>'full'));
$pdf->ezText("<b>".html_entity_decode("BLUE CARD.")."</b>", 11, array('justification'=>'full'));
$pdf->ezSetDy(-40);
$pdf->ezText(html_entity_decode($param_name), 11, array('justification'=>'full'));
$pdf->ezText("<b>".html_entity_decode($param_charge)."</b>", 11, array('justification'=>'full'));



$pdfcode = $pdf->ezOutput();
$pdfname = 'acta_stock_'.date('U');
$fp=fopen(DOCUMENT_ROOT.'system_temp_files/'.$pdfname.'.pdf','wb');
$_SESSION['documentURL'] = $pdfname.'.pdf';
fwrite($fp,$pdfcode);
fclose($fp);
?>