<?php
/*
File: pAssignStockDealer.php
Author: Nicolas Flores
Creation Date: 14/01/2010
Modified By: Patricio Astudillo
Last Modified: 20/01/2011
*/

$stock= new Stock($db);
$serial_dea = $_POST['selDealer']; //dealer id
$stockAmount = $_POST['txtAmountStock']; // stock amount to be asigned
$type = $_POST['selType']; //Stock Type
$error=$stock->stockAssign($stockAmount,$type,'DEALER',$serial_dea,true);

//*********** RETRIEVE THE BRANCHES FOR THE DEALER TO EXECUTE DIRECT ASIGNATION.
$dealer=new Dealer($db);
$params=array();
$aux=array('field'=>'d.status_dea',
		   'value'=> 'ACTIVE',
		   'like'=> 1);
array_push($params,$aux);
$aux=array('field'=>'d.dea_serial_dea',
		   'value'=> $_POST['selDealer'],
		   'like'=> 1);
array_push($params,$aux);
$branchList=$dealer->getDealers($params);

if($error==1 && count($branchList)==1){
	$_POST['selBranch']=$branchList['0']['serial_dea'];
	$direct_asignation=TRUE;

	include('pAssignStockBranch.php');
}

http_redirect('modules/stock/fAssignStockDealer/'.$error);
?>