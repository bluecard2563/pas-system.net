<?php 
/*
File: pUpdateParameter.php
Author: Santiago Benítez
Creation Date: 13/01/2010 11:39
Last Modified: 13/01/2010 11:39
Modified By: Santiago Benítez
*/

Request::setInteger('hdnParameterID');
$parameter=new Parameter($db,$hdnParameterID);
$typeList=$parameter->getTypeValues();
$conditionList=$parameter->getConditionValues();

if($parameter->getData()){
	$parameter->setName_par($_POST['txtName']);
	$parameter->setValue_par($_POST['txtValue']);
	$parameter->setType_par($_POST['selType']);
	$parameter->setDescription_par($_POST['txaDescription']);
	$parameter->setCondition_par($_POST['selCondition']);
	
	if($parameter->update()){
		http_redirect('modules/parameter/fSearchParameter/2');
	}else{
		$error=1;
		$parameter->getData();
		$data['type_par']=$parameter->getType_par();
		$data['name_par']=$parameter->getName_par();
		$data['value_par']=$parameter->getValue_par();
		$data['description_par']=$parameter->getDescription_par();
		$data['condition_par']=$parameter->getCondition_par();
		$data['serial_par']=$hdnQuestionID;
		$smarty->register('error');
		
	}
}
$smarty->register('data,conditionList,typeList');
$smarty->display('modules/parameter/fUpdateParameter.'.$_SESSION['language'].'.tpl');

?>
