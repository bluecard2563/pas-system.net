<?php
/*
File: pEditParameter
Author: David Bergmann
Creation Date: 29/12/2010
Last Modified:
Modified By:
*/

$parameter = new Parameter($db, $_POST['hdnSerial']);
$parameter->getData();

$value_par = unserialize($parameter->getValue_par());
$value_par[$_POST['selType']] = $_POST['usersTo'];

$parameter->setValue_par(serialize($value_par));

if($parameter->update()){
	http_redirect("modules/parameter/assistance/fEditParameter/1");
} else {
	http_redirect("modules/parameter/assistance/fEditParameter/2");
}

?>