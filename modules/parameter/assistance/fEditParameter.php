<?php
/*
File: fEditParameter
Author: David Bergmann
Creation Date: 29/12/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$parameter = new Parameter($db, '34'); //Parameters for Liquidation Permitions.
$parameter->getData();

$data['serial_par'] = $parameter->getSerial_par();
$data['name_par'] = $parameter->getName_par();
$data['value_par'] = unserialize($parameter->getValue_par());
$data['description_par'] = $parameter->getDescription_par();

$zone = new Zone($db);
$nameZoneList=$zone->getZones();

$smarty->register('error,data,global_assistanceEmailType,nameZoneList');
$smarty->display();
?>