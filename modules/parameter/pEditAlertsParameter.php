<?php 
/*
File: pEditAlertsParamater.php
Author:Gabriela Guerrero
Creation Date: 11/05/2010
Last Modified: 
Modified By: 
*/
	
	$alertId = $_POST['selAlerts'];
	$usersArray = $_POST['usersTo'];
	
	$parameter = new Parameter($db, $alertId);
	$parameter -> getData();
		
	$parameterMbcs = array();
	if(is_array($usersArray)){
		foreach($usersArray as $key => $usersByMbc){
			$usersList = array();
			foreach($usersByMbc as $singleUser){
				$usersList[] = $singleUser;
			}
			
			if($alertId == 25){
				$parameterMbcs[$_POST['selCountry' . $key]] = implode(",", $usersList);
			}else{
				$parameterMbcs[$_POST['selManager' . $key]] = implode(",", $usersList);
			}
		}
	}
	
	$parameter -> setValue_par(serialize($parameterMbcs));
	
	if(!$parameter->update()){
		http_redirect('modules/parameter/fEditAlertsParameter/2');
	}

    http_redirect('modules/parameter/fEditAlertsParameter/1');
?>