<?php
/*
File: pNewParameter.php
Author: Santiago Benítez
Creation Date: 13/01/2010 09:43
Last Modified: 13/01/2010 09:43
Modified By: Santiago Benítez
*/
$parameter=new Parameter($db);
$parameter->setName_par(specialchars($_POST['txtName']));
$parameter->setValue_par(specialchars($_POST['txtValue']));
$parameter->setType_par($_POST['selType']);
$parameter->setDescription_par(specialchars($_POST['txaDescription']));
$parameter->setCondition_par($_POST['selCondition']);

if($parameter->insert()){
	http_redirect('modules/parameter/fNewParameter/1');
}
else{
	http_redirect('modules/parameter/fNewParameter/2');
}


