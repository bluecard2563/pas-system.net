<?php

/*
  File:fChooseMasiveSales.php
  Author: Miguel Ponce
  Creation Date:
  Modified By:
  Last Modified:
 */
	Request::setString('0:error');
	//get the list of countries for the user logged in
	$country = new Country($db);
	$countryList = $country -> getMasiveCountries($_SESSION['countryList']);
	//Language to be used on data retrieve
	$language = new Language($db);
	$language -> getDataByCode($_SESSION['language']);

	//Dealer Types
	$dealerT = new DealerType($db);
	$typeList = $dealerT -> getDealerTypes(NULL, $language -> getSerial_lang());

	//CATEGORY
	$dealer = new Dealer($db);
	$categoryList = $dealer -> getCategories();
	$smarty -> register('countryList,error,typeList,categoryList');
	$smarty -> display();
?>
