<?php

/*
  File: fNewSale.php
  Author: Esteban Angulo
  Creation Date: 22/02/2010
  Modified By: Patricio Astudillo
  Last Modified: 24/03/2010
 */

if ($_SESSION['serial_dea'] && !User::isSubManagerUser($db, $_SESSION['serial_usr']))
    http_redirect('modules/sales/fDealerNewSale');

//check if who is trying to sell is a counter.
$counter = new Counter($db);
$serial_cnt = $counter->getCounters($_SESSION['serial_usr']);
$sellFree = $_SESSION['sellFree'];
$parameter = new Parameter($db);
$parameter->setSerial_par('7');
$parameter->getData();
$age = $parameter->getValue_par();
$planetUser = 0;
$user = new User($db);
$user->setSerial_usr($_SESSION['serial_usr']);
$user->getData();
//$planetUser=$user->isPlanetAssistUser();
$planetUser = $user->getBelongsto_usr();
global $ageForDisplayQuestions;
global $custom_payments;
$sessionUser = $_SESSION['serial_usr'];

if ($planetUser != 'MANAGER' && User::isSubManagerUser($db, $_SESSION['serial_usr'])):
    $planetUser = 'MANAGER';
endif;

if (count($serial_cnt) == 1) {
    $serial_cnt = $serial_cnt['0']['serial_cnt'];
    //die($serial_cnt);
    $sale = new Sales($db);
    $ofCounter = $sale->isDirectSale($serial_cnt);
    $counter = new Counter($db, $serial_cnt);
    $counter->getData();

    $nextAvailableNumber = 'AUTOMATICO'; //DO NOT GET A NUMBER AT THIS POINT, THIS AVOIDS USER CONFUSION

    $city = new City($db);
    $dealer = new Dealer($db);

    $countryList = Country::getAllAvailableCountries($db);

    //PRE-LOAD INFORMATION
    //CounterName
    $data['counterName'] = $_SESSION['user_name'];

    //Dealer Data
    //$deaData=Dealer::getDealerCity($db, $_SESSION['serial_usr']);
    $deaData = Dealer::getDealerCity($db, $serial_cnt);
    $data['dealerOfUser'] = $counter->getSerial_dea();
    //Country currencies
    $curBycountry = new CurrenciesByCountry($db);
    $data['currencies'] = $curBycountry->getCurrenciesDealersCountry($counter->getSerial_dea());
    $data['currenciesNumber'] = sizeof($data['currencies']);

    $dealer->setSerial_dea($data['dealerOfUser']);
    if ($dealer->getData()) {
        $data['name_dea'] = $dealer->getName_dea();
        $data['serialDealer'] = $dealer->getDea_serial_dea();

        /* products by Dealer */
        $productByDealer = new ProductByDealer($db);
        $productListByDealer = $productByDealer->getproductByDealer($data['serialDealer'], $_SESSION['serial_lang'], 'NO', 'ACTIVE');

        if (!is_array($productListByDealer)) {
            http_redirect('main/sales/20');
        }

        //ADD THE REQUIRED PERCENTAGES FOR EACH PRODUCT
        foreach ($productListByDealer as &$singleProListed) {
            $pxc = new ProductByCountry($db, $singleProListed['serial_pxc']);
            $pxc->getData();
            if (PriceByProductByCountry::pricesByProductExist($db, $singleProListed['serial_pxc'])) {
                //If there is a price for the product in the country of analisis.
                $percentages = ProductByCountry::getPercentages_by_Product($db, $singleProListed['serial_pro'], $pxc->getSerial_cou());
            } else {
                //If not, I take the prices for ALL COUNTRIES
                $percentages = ProductByCountry::getPercentages_by_Product($db, $singleProListed['serial_pro'], '1');
            }
            $singleProListed['percentage_spouse'] = $percentages['percentage_spouse_pxc'];
            $singleProListed['percentage_extras'] = $percentages['percentage_extras_pxc'];
            $singleProListed['percentage_children'] = $percentages['percentage_children_pxc'];
            $singleProListed['percentage_senior_adults_pxc'] = $percentages['percentage_senior_adults_pxc'];
        }

        //Services by dealer
        $servicesByDealer = new ServicesByDealer($db);
        if (ServicesByDealer::serviceByCountryExist($db, $deaData['serial_cou'])) {
            $servicesListByDealer = $servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $deaData['serial_cou']);
        } else {
            $servicesListByDealer = $servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
        }
    } else {
        http_redirect('main/sales/13');
    }


    //If the dealer doesn't have any products to sale,
    //he can't see the sales interface.
    if (!is_array($productListByDealer)) {
        http_redirect('main/sales/3');
    }

    //Makes Seller Code
    $sellerCode = $deaData['code_cou'] . '-' . $deaData['code_cit'] . '-' . $dealer->getCode_dea() . '-' . $data['serialDealer'] . '-' . $serial_cnt;

    //EmissionCity
    $data['emissionPlace'] = $deaData['name_cit'];

    //EmissionDate
    $data['emissionDate'] = date("d/m/Y");


    //Customer type
    $customer = new Customer($db);
    $typesList = $customer->getAllTypes();

    /* QUESTIONS */
    $sessionLanguage = $_SESSION['serial_lang'];
    $question = new Question($db);
    $maxSerial = $question->getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage); //list of active questions shown in the form
    /* END QUESTIONS */

    /* PARAMETERS */
    $parameter = new Parameter($db, 1); // --> PARAMETER FOR ELDER QUESTIONS
    $parameter->getData();
    $elderlyAgeLimit = $parameter->getValue_par(); //value of the paramater used to display or not the questions
    /* END PARAMETERS */

    //Get the Country Serial for calculate the price
    //of the product to be sold.
    $serial_cou = $deaData['serial_cou'];
	if($serial_cou == '62'): //BURNED VALUE FOR ECUADOR
		$validate_holder_document = 'YES';
	endif;
	
	$smarty->register('validate_holder_document');
    $smarty->register('countryList,data,nextAvailableNumber,productListByDealer,servicesListByDealer,sellerCode,typesList,questionList,elderlyAgeLimit,serial_cou,serial_cnt,maxSerial,hasDealers,ofCounter');
} else {
    if (count($serial_cnt) == 0) {
        //Only a Counter can sell, other users go to main page
        http_redirect('main/sales/2');
    } else {
        //Customer type
        $customer = new Customer($db);
        $typesList = $customer->getAllTypes();

        /* QUESTIONS */
        $sessionLanguage = $_SESSION['serial_lang'];
        $question = new Question($db);
        $maxSerial = $question->getMaxSerial($sessionLanguage);
        $questionList = Question::getActiveQuestions($db, $sessionLanguage); //list of active questions shown in the form
        /* END QUESTIONS */

        /* PARAMETERS */
        $parameter = new Parameter($db, 1); // --> PARAMETER FOR ELDER QUESTIONS
        $parameter->getData();
        $elderlyAgeLimit = $parameter->getValue_par(); //value of the paramater used to display or not the questions
        /* END PARAMETERS */

        $hasDealers = count($serial_cnt);
        $countryList = Country::getAllAvailableCountries($db);

        $smarty->register('questionList,elderlyAgeLimit,serial_cou,serial_cnt,hasDealers,countryList,typesList');
    }
}
$parameter = new Parameter($db);
$parameter->setSerial_par('14');
$parameter->getData();
$maxCoverTime = $parameter->getValue_par();

/**************** BRANCH INFO FOR INVOICE HEADER *********************/
	$branch = new Dealer($db, $data['dealerOfUser']);
	$branch->getData();
	$branchData = get_object_vars($branch);
	unset($branchData['db']);
	$branchData['today'] = date('d/m/Y');
	
	/* RETRIEVE SALES'S TOTAL PRICE */
	$sales = new Sales($db);
	$salesT = $sales->getSalesTotal($salesIdString);
	$salesTotal = number_format($salesT, 2, '.', '');
	
	/* RETRIEVE SALES'S TOTAL COST */
	$salesCost = number_format($sales->getCostTotal($salesIdString), 2, '.', '');
	if ($branchData['type_percentage_dea'] == 'DISCOUNT') {
		$totalAfterD = $salesT - ($salesT * $branchData['percentage_dea'] / 100);
	} else {
		if($branchData['percentage_dea'] > 0){
			$branchData['commission'] = $branchData['percentage_dea'] / 100;
		}else{
			$branchData['commission'] = 0;
		}
		
		$totalAfterD = $salesT;
	}
//Debug::print_r($branchData);die();
$smarty->register('sellFree,age,planetUser,maxCoverTime,ageForDisplayQuestions,custom_payments,branchData,sessionUser');
$smarty->display();
?>
