<?php
/*
 * File: pAuthorizeFreeSales
 * Author: Patricio Astudillo
 * Creation Date:
 * Modifies By:
 * Last Modified:
 */

$sales_IDs=$_POST['serial_sal'];
$error=11;
$alert=new Alert($db);
$sale=new Sales($db);
$customer=new Customer($db);

foreach($sales_IDs as $s){
    $sale->setSerial_sal($s);
	$sale->getData();
    switch($_POST['selAction']){
        case 'DENIED':  $sale->setStatus_sal('DENIED');
                        $sale->setFree_sal('YES');
						$misc['resolution']='NEGADA';
						$sales_log_status = 'DENIED';
                        break;

        case 'APROVED': $sale->setStatus_sal('ACTIVE');
                        $sale->setFree_sal('YES');
						$misc['resolution']='APROBADA';
						$sales_log_status = 'AUTHORIZED';
                        break;

        case 'INVOICE': $sale->setStatus_sal('REGISTERED');
                        $sale->setFree_sal('NO');
						$misc['resolution']='POR FACTURAR';
						$sales_log_status = 'DENIED';
                        break;
    }
	
    if(!$sale->updateStatus()){
        $error=12;
        break;
    }else{
		$alert->setServeAlert('FREE', $s);
		$aplicantsData=Counter::getCountersMailInfo($db, $sale->getSerial_cnt());
		$customer->setserial_cus($sale->getSerial_cus());

		if(is_array($aplicantsData) and $customer->getData()){
			/*E-mail info*/
			$misc['product']=ProductByDealer::getProductsByDealerByLanguage($db, $sale->getSerial_pbd(), $_SESSION['serial_lang']);
			$misc['email']=$aplicantsData['email_usr'];
			$misc['user']=$aplicantsData['name'];
			$misc['customer_name']=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
			$misc['card_number']=$sale->getCardNumber_sal();
			$misc['observations']=$_POST['txtObservations'];
			if($_POST['selAction'] == 'DENIED'){
				$misc['textForMail'] = "La solicitud de venta de una tarjeta free de tipo ".$misc['product']." fue <u>".$misc['resolution']."</u>.";
			} elseif($_POST['selAction'] == 'APROVED') {
				$misc['textForMail'] = "La tarjeta free #".$misc['card_number']." fue <u>".$misc['resolution']."</u>.";
			} elseif($_POST['selAction'] == 'INVOICE') {
				$misc['textForMail'] = "La tarjeta free #".$misc['card_number']." fue cambiada a estado <u>".$misc['resolution']."</u>.";
			}
			/*End E-mail info*/

			/* SALES_LOG ACTIVITY */
			$sLog = new SalesLog($db);
			$sLog -> getPendingSalesLogBySale($s, 'FREE');
			$log_misc = $sLog ->getAux_slg();
			$log_misc = $log_misc['0'];
			$serial_log = $log_misc['serial_slg'];
			$log_misc = unserialize($log_misc['description_slg']);

			$log_misc['new_sale'] = $sale;
			$log_misc['new_sale'] = get_object_vars($log_misc['new_sale']);
			unset($log_misc['new_sale']['db']);

			$log_misc['global_info']['authorizing_usr'] = $_SESSION['serial_usr'];
			$log_misc['global_info']['authorizing_date'] = date('d/m/Y');
			$log_misc['global_info']['authorizing_obs'] = $_POST['txtObservations'];

			SalesLog::serveLog($db, $_SESSION['serial_usr'], $sales_log_status, $log_misc, $serial_log);
			/* SALES_LOG ACTIVITY */

			if(GlobalFunctions::sendMail($misc, 'free_application')){
				$error=11;
			}else{
				$error=14;
			}
		}else{
			$error=14;
		}
	}
}

http_redirect('main/sales/'.$error);
?>