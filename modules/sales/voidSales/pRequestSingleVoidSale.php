<?php
/*
File: pRequestSingleVoidSale
Author: Santiago Borja
Creation Date: 30/12/2010
*/
	Request::setString('txtCardNumber');
	Request::setString('isChecked');
	Request::setString('observation');
	
	$transactionLog = new SalesLog($db);
	$transactionLog -> setType_slg('VOID_SALE');
	$transactionLog -> setStatus_slg('PENDING');
	$transactionLog -> setSerial_usr($_SESSION['serial_usr']);
	
	$misc = array(	'old_sale' => 'NO SALE',
					'new_sale' => 'NEW SALE',
					'Description' => 'Sales Void Request.');
	
	if($txtCardNumber){
		$originalSale = new Sales($db);
		$originalSale -> setCardNumber_sal($txtCardNumber);
		$originalSale -> getDataByCardNumber();
		
		$newSale = $originalSale;
		$newSale -> setStatus_sal('VOID');
		$newSale -> setChange_fee_sal(1);
		$newSale -> setInternationalFeeStatus_sal('PAID');
		$newSale -> setInternationalFeeUsed_sal('YES');
		$newSale -> setInternationalFeeAmount_sal(0);
		
		if($originalSale -> getSerial_sal()){ //THE SALE EXISTS
			//TODO CHECK SALE IS YOURS!!
			if($transactionLog -> getPendingSalesLogBySale($originalSale -> getSerial_sal())){ //THIS SALE HAS ANOTHER REQUEST PENDING
				echo 'REQUESTED';return;
			}
			
			if($originalSale -> getSerial_inv()){//INVOICED SALE!! CANNOT VOID
				echo 'INVOICED';return;
			}
			
			$originalSale = get_object_vars($originalSale);
			$newSale = get_object_vars($newSale);
			unset($originalSale['db']);
			unset($originalSale['aux']);
			unset($newSale['db']);
			unset($newSale['aux']);
			
			$misc['old_sale'] = $originalSale;
			$misc['new_sale'] = $newSale;
			$transactionLog -> setSerial_sal($newSale['serial_sal']);
		}else{//THE SALE DOES NOT EXIST
			if($isChecked != 'TRUE'){
				//HERE IT MUST BE WITHIN THE SPECIAL RANGE, OTHERWISE, IT IS NOT A VALID CARD NUMBER
				global $global_salesRanges;
				$withinRange = false;
				foreach($global_salesRanges as $range){
					if($txtCardNumber > $range['from'] &&
					   $txtCardNumber < $range['to']){
						$withinRange = true;
						break;
					}
				}
				if(!$withinRange){
					echo 'INVALID';return;
				}
			}//ELSE, COMES FROM A CHECKED RECORD, MEANING IT IS WITHIN THE MANUAL STOCK AND THEREFORE IS A VALID SALE TO VOID
			global $globalCEOCustomer;
			global $globalCEOCounter;
			
		    $newSale -> setSerial_cus($globalCEOCustomer); //JFPonce
		    $newSale -> setSerial_pbd(1); //TODO FIX A SUITABLE PBD
			$newSale -> setSerial_cur(1); //USD
		    $newSale -> setSerial_cnt($globalCEOCounter);
		    $newSale -> setCardNumber_sal($txtCardNumber);
		    $newSale -> setSerial_cit(1); //GENERAL CITY FOR COUNTRY 'ALL'
		    $newSale -> setEmissionDate_sal(date('d/m/Y'));
		    $newSale -> setBeginDate_sal(date('d/m/Y'));
		    $newSale -> setEndDate_sal(date('d/m/Y'));
		    $newSale -> setInDate_sal(date('d/m/Y'));
		    $newSale -> setDays_sal(0);
		    $newSale -> setObservations_sal('VENTA MANUAL ANULADA');
		    $newSale -> setType_sal('SYSTEM');
		    $newSale -> setStockType_sal('MANUAL');
			$newSale -> setFree_sal('NO');
			$newSale ->setDirectSale_sal('NO');
			
			$serialSale = $newSale -> insert();
			if(!$serialSale){
				ErrorLog::log($db, 'VOID SALES REQUEST FAIL' . 'Sales insert Fault', $newSale);
				echo 'SAVE_FAULT';return;
			}
			
			$newSale -> setSerial_sal($serialSale);
			$newSale = get_object_vars($newSale);
			unset($newSale['db']);
			unset($newSale['aux']);
			
			$transactionLog -> setSerial_sal($serialSale);
			$misc['old_sale'] = $newSale;
			$misc['new_sale'] = $newSale;
		}
		
		$misc['modifyObs'] = $observation;
		$misc['global_info']['requester_usr'] = $_SESSION['serial_usr'];
		$misc['global_info']['request_date'] = date('d/m/Y');
		$misc['global_info']['request_obs'] = addslashes($observation);
		
		$transactionLog -> setDescription_slg(serialize($misc));
		$serialLog = $transactionLog -> insert();
		if(!$serialLog){
			ErrorLog::log($db, 'VOID SALES REQUEST FAIL' . 'Sales Log insert Fault', $transactionLog);
			echo 'SAVE_FAULT';return;
		}
		
		$usersCountry = GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
		if($usersCountry == '1'){ //If the user is a PLANETASSIST user, we set the country to Ecuador 62.
			$usersCountry = '62';
		}

		$alert = new Alert($db);
		$alert -> setMessage_alt('Solicitud de anulacion de Venta pendiente.');
		$alert -> setType_alt('VOID_SALE');
		$alert -> setStatus_alt('PENDING');
		$alert -> setRemote_id($serialLog);
		$alert -> setTable_name('sales/modifications/fCheckSalesVoidApplication/');
		
		if(!$alert -> autoSend($_SESSION['serial_usr'])){
			echo 'ALERT_FAULT';return;
		}
		echo 'OK';
	}else{
		ErrorLog::log($db, 'VOID SALES REQUEST FAULT' . 'No Card Number Specified', $txtCardNumber);
		echo 'NO_CARD';
	}
?>