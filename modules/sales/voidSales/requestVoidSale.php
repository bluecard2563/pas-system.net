<?php
/*
File: fCardReport
Author: David Bergmann
Creation Date: 28/05/2010
Last Modified:
Modified By:
*/

	Request::setString('0:error');
	
	$user = new User($db, $_SESSION['serial_usr']);
	$user -> getData();
	$unable = 'FALSE';
	if($user -> getSerial_dea()){
		$unable = 'TRUE';
	}
	
	$zone = new Zone($db);
	$nameZoneList = $zone -> getZones();
	
	$stockTypeList = Sales::getSalesStock($db);
	$order_by = array('card_number'=>'N&uacute;mero de tarjeta','in_date'=>'Fecha de ingreso');
	
	$smarty->register('error,nameZoneList,unable');
	$smarty->display();
?>