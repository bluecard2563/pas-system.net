<?php

	/*
	Document		:	pVoidCheckedSales.php
	Author			:	Santiago Borja
	Modified date	:	30/12/2010
	*/
	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	
	$allPosts = $_POST;
	
	foreach($allPosts as $key => $singleCheck){
		$cardNumber = str_replace('check_', '', $key);
		if(is_numeric($cardNumber)){ //ONLY PROCESS CHECKS, WE SKIP POST VALUES SUCH AS THE SUBMIT BUTTON
				$url = URL."modules/sales/voidSales/pRequestSingleVoidSale";
				if (!function_exists('curl_init')) die('CURL is not installed!');

				// create a new curl resource
				$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
				session_write_close();

				$txtObservation = htmlentities($_POST['txtObservation']);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "txtCardNumber=$cardNumber&isChecked=TRUE&observation=$txtObservation");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_COOKIE, $strCookie );

				// download the given URL, and return output
				$output = curl_exec($ch);
				
				if($output != 'OK'){
					http_redirect('modules/sales/voidSales/requestVoidSale/' . $output);
				}
				// close the curl resource, and free system resources
				curl_close($ch);
		}
	}
	http_redirect('modules/sales/voidSales/requestVoidSale/OK');
?>