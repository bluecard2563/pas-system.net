<?php
/*
File:fSearchFreeSales.php
Author: Mario López
Creation Date:31/03/2010
Modified By:
Last Modified:
*/
Request::setInteger('0:error');
//get the list of countries for the user logged in
$country=new Country($db);
$countryList=$country->getOwnCountriesWithPendingFreeSales($_SESSION['countryList']);

$smarty -> register('countryList,error');
$smarty->display();
?>