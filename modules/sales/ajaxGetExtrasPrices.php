<?php 
/*
File: ajaxGetExtrasPrices.php
Author: Santiago Borja
Creation Date: 18/04/2010
*/
	$jsonData = get_object_vars(json_decode($_POST['jsonData']));
	global $adultMinAge;
$elderlyAgeParameter = new Parameter($db,1);
$elderlyAgeParameter -> getData();
$elderlyAge = $elderlyAgeParameter -> getValue_par();

// Get the value of the parameter where the 65 years old of eldery is setted
$elderlyMinAgeHandler = new Parameter($db, 55);
$elderlyMinAgeHandler->getData();
$elderlyMinAge = $elderlyMinAgeHandler->getValue_par();

// Get the value of the parameter where the 86 years old of eldery is setted
$elderlyMaxAgeHandler = new Parameter($db, 56);
$elderlyMaxAgeHandler->getData();
$elderlyMaxAge = $elderlyMaxAgeHandler->getValue_par();

// Get the value of the parameter where the 80 years old of eldery is setted
$elderlyMiddleAgeHandler = new Parameter($db, 61);
$elderlyMiddleAgeHandler->getData();
$elderlyMiddleAge = $elderlyMiddleAgeHandler->getValue_par();
// Get value percentage senior
$percentageSeniorOneParameter = new Parameter($db, 57);
$percentageSeniorOneParameter->getData();
$percentageSeniorOne = $percentageSeniorOneParameter->getValue_par();

$percentageSeniorTwoParameter = new Parameter($db, 58);
$percentageSeniorTwoParameter->getData();
$percentageSeniorTwo = $percentageSeniorTwoParameter->getValue_par();

$percentageSeniorThreeParameter = new Parameter($db, 59);
$percentageSeniorThreeParameter->getData();
$percentageSeniorThree = $percentageSeniorThreeParameter->getValue_par();

$percentageSeniorFourParameter = new Parameter($db, 60);
$percentageSeniorFourParameter->getData();
$percentageSeniorFour = $percentageSeniorFourParameter->getValue_par();

//Debug::print_r($jsonData);die();
	$allExtras = $jsonData['extras'];
	$adults = 0;
	$children = 0;
	$spouse = false;
	$other=false;
	$outputExtras = array();
	foreach($allExtras as $key => $extra){
		$extraAge = GlobalFunctions::getMyAge($extra -> age, $jsonData['arrivalDate']); 
		//$outputExtras[$key] = 'ADULT'; //DEFAULT
		if($extraAge >= $adultMinAge){
			$adults ++;
		}else{
			$children ++;
			$outputExtras[$key][] = 'CHILD';
		}
		if($extra -> relationship == 'SPOUSE'){
			$spouse = true;
			$outputExtras[$key] []= 'SPOUSE';
            $outputExtras[$key][]=$extraAge;
		}elseif($extra->relationship == 'OTHER'){
            $other=true;
		    $outputExtras[$key][] = 'OTHER';
		    $outputExtras[$key][]=$extraAge;
        }elseif($extra->relationship == 'RELATIVE'){
            $outputExtras[$key][] = 'RELATIVE';
            $outputExtras[$key][]=$extraAge;
        }
	}
	$prices = GlobalFunctions::productCalculatePrice($db,	$jsonData['base_price'],
															$jsonData['base_cost'],
															$jsonData['adults_free'],// #
															$jsonData['percentage_adult'],// int
															$adults,// # - Including the Holder
															$jsonData['children_free'],// #
															$jsonData['percentage_children'],// int
															$children,//CHILDREN TRAVELING
															$spouse,//SPOUSE TRAVELING
															$jsonData['percentage_spouse'],
															$jsonData['max_extras_pro'],
															$jsonData['extras_restriction'],
                                                            $jsonData['serial_pxc']);
	//Debug::print_r($prices);die();
	
	//CALCULATIONS:
	$adultsFree = $jsonData['adults_free'];
	$childrenFree = $jsonData['children_free'];
	//Debug::print_r($outputExtras);die();
	foreach($outputExtras as &$oExtra){
	   //Debug::print_r($outputExtras);die();
		$priceExtra = 0;
		$costExtra = 0;
		switch (true) {
            case ($oExtra[1] == 'SPOUSE'):
                if (--$adultsFree >= 0) {
                    $priceExtra = 0;
                    $costExtra = 0;
                } else {
                    $priceExtra = round($prices['spousePrice']);
                    $costExtra = $prices['spouseCost'];
                }
                BREAK;
            case ($oExtra[0] == 'CHILD'):
                if (--$childrenFree >= 0) {
                    $priceExtra = 0;
                    $costExtra = 0;
                } else {
                    $priceExtra = round($prices['childrenPrice']);
                    $costExtra = $prices['childrenCost'];
                }
                BREAK;
            // Santi 20 May 2021: Calculate the price for each type of extra based on age by the percentage of surcharge of the product (50% or 100%).
            case ($oExtra[0] === 'SPOUSE'):
                if (--$adultsFree >= 0) {
                    $priceExtra = 0;
                    $costExtra = 0;
                } else {
                    if ($oExtra[1] >= $elderlyMinAge && $oExtra[1] < $elderlyAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorOne) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['spouseCost'];

                     } else if ($oExtra[1] >= $elderlyAge && $oExtra[1] < $elderlyMiddleAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorTwo) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['spouseCost'];
                        
                    } else if ($oExtra[1] >= $elderlyMiddleAge && $oExtra[1] < $elderlyMaxAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorThree) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['spouseCost'];

                    }elseif ($oExtra[1] >= $elderlyMaxAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorFour) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['spouseCost'];
                    } else {
                        $priceExtra = round($prices['spousePrice']);
                        $costExtra = $prices['spouseCost'];
                    }
                }
                break;
            case ($oExtra[0] === 'RELATIVE'):
                if (--$adultsFree >= 0) {
                    $priceExtra = 0;
                    $costExtra = 0;
                } else {
                    if ($oExtra[1] >= $elderlyMinAge && $oExtra[1] < $elderlyAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorOne) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];

                    } elseif ($oExtra[1] >= $elderlyMinAge && $oExtra[1] < $elderlyMiddleAge ) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorTwo) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];

                    } elseif ($oExtra[1] >= $elderlyMiddleAge && $oExtra[1] < $elderlyMaxAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorThree) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];
                        
                    }elseif ($oExtra[1] >= $elderlyMaxAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorFour) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];
                    } else {
                        $priceExtra = round($prices['extrasPrice']);
                        $costExtra = $prices['extrasCost'];
                    }
                }
                break;
            case ($oExtra[0] === 'OTHER'):
                if (--$adultsFree >= 0) {
                    $priceExtra = 0;
                    $costExtra = 0;
                } else {
                    if ($oExtra[1] >= $elderlyMinAge && $oExtra[1] < $elderlyAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorOne) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];

                    } elseif ($oExtra[1] >= $elderlyMinAge && $oExtra[1] < $elderlyMiddleAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorTwo) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];

                    }  elseif ($oExtra[1] >= $elderlyMiddleAge && $oExtra[1] < $elderlyMaxAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorThree) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];

                    }elseif ($oExtra[1] >= $elderlyMaxAge) {
                        $priceExtra = $prices['holderPrice'] + round(number_format(round((($prices['holderPrice'] * $percentageSeniorFour) / 100), 2), 2, '.', ''));
                        $costExtra = $prices['extrasCost'];
                    } else {
                        $priceExtra = round($prices['extrasPrice']);
                        $costExtra = $prices['extrasCost'];
                    }
                }
                break;
        }
		$oExtra = array();
		$oExtra['price'] = $priceExtra;
		$oExtra['cost'] = $costExtra;
	}
	echo json_encode($outputExtras);
	
	//echo '<pre>'; print_r($jsonData); echo '</pre>';
	//echo '<pre>'; print_r($prices); echo '</pre>';
	//echo '<pre>'; print_r($outputExtras); echo '</pre>';
?>