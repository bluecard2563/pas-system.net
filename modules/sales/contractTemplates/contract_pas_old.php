<?php
/*
 * Document   : contract_pas.php
 * Created on : May 19, 2010
 * Author     : Gabriela Guerrero & Santiago Borja
 * 
*/
	
	//GET LANGUAGE CODE FOR LABEL'S TEXT
	$lang_string = new Language($db, $langCode);
	$lang_string ->getData();
	$lang_string = $lang_string ->getCode_lang();

	$sale = new Sales($db,$serial_sal);
	$sale -> getData();
	
	if($sale){
		$planetassist_sale = Sales::isPlanetAssistSale($db, $sale -> getSerial_sal());

		if($planetassist_sale){
			$pdf -> addJpegFromFile(DOCUMENT_ROOT."img/contract_logos/logopas_contracts.jpg", 375, 784, 170);
			$web_url = PASWEB_URL;
		}else{
			$pdf -> addJpegFromFile(DOCUMENT_ROOT."img/contract_logos/logoblue_contracts.jpg", 410, 784, 140);
			$web_url = BLUE_URL;
		}
	}else{
		$pdf -> addJpegFromFile(DOCUMENT_ROOT."img/contract_logos/logopas_contracts.jpg", 375, 784, 170);
		$web_url = PASWEB_URL;
	}
	
	include("modules/sales/contractTemplates/contractTexts/text_pas.php");
	
	/* GETTING NATIONAL/INTERNATIONAL KIND OF TRAVEL */
	if($serial_trl):
		$international_flight = Sales::internationalFlight($db, $serial_sal, $serial_trl);
	else:
		$international_flight = Sales::internationalFlight($db, $serial_sal);
	endif;
	
	//Gathering all the info:
	$dataInfo = $sale -> getSaleProduct($langCode);
	$dataBen = $sale -> getSaleBenefits($langCode);
	$dataServ = $sale -> getSaleServices($langCode);
	$serialCus = $sale -> getSerial_cus();
	$pConditions = PConditionsByProductCountry::getPConditionsBySale($db, $serial_sal, $langCode);
	
	/*********** FORMAT PRICE NUMBERS *************/
	$dataInfo['total_sal'] = str_replace(",", "", $dataInfo['total_sal']);
    $dataInfo['fee_sal'] = str_replace(",", "", $dataInfo['fee_sal']);
	
	/*********** ASISTANCE PHONES *****************/
	$xCountry = new Country($db);
	$assistance_list = $xCountry -> getPhoneCountries(false);
	$assistance_phones = array();
	$name_array = array();
	$phone_array = array();
	foreach($assistance_list as $a){
		if(count($name_array) < 4){
			array_push($name_array, $a['name_cou']);
			array_push($phone_array, $a['phone_cou']);
		}else{
			array_push($assistance_phones, $name_array);
			array_push($assistance_phones, $phone_array);
			$name_array = array();
			$phone_array = array();
			array_push($name_array, $a['name_cou']);
			array_push($phone_array, $a['phone_cou']);
		}
	}
	array_push($assistance_phones, $name_array);
	array_push($assistance_phones, $phone_array);
	/*********** ASISTANCE PHONES *****************/

	if(!$traveler_log_customer){
		$customer = new Customer($db, $serialCus);
	}else{
		$customer = new Customer($db, $traveler_log_customer);
	}
	$customer -> getData();
	$customer = get_object_vars($customer);
	unset($customer['db']);
	$customer['full_address']=Customer::getGeographicalCustomerAddress($db, $customer['serial_cus']);
	$customer['full_address']=$customer['full_address']['name_cou'].' / '.$customer['full_address']['name_cit'].' / '.$customer['full_address']['address_cus'];
	
	$extra = new Extras($db,$serial_sal);
	$dataExt = Extras::getExtrasBySale($db, $serial_sal);
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext += $dE['fee_ext'];
		}
	}
	
	$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$global_contractDate['weekday']].' '.$global_contractDate['day'].' de '.$global_weekMonths[$global_contractDate['month']].' de '.$global_contractDate['year'].' a las '.date("h:i:s a", time()) );
	
	/********************************** QR CODE ***************************************/
	//TODO: text by language
	$qr_image = $serial_sal;
	$qr_contract_url = DOCUMENT_ROOT.'system_temp_files/'.$qr_image.'.png';
        if($international_flight):
            $qr_string = "PLANET ASSIST - ";
        else:
            $qr_string = "BLUE CARD - ";
        endif;
	
	$qr_string .= $saleContract['qr']['contract'].$sale->getCardNumber_sal();
        $qr_string .= $saleContract['qr']['verification_label'].substr( (string)($sale->getSerial_sal().$sale->getSerial_cus().$sale->getSerial_pbd()), 3, 10);
	QRcode::png($qr_string, $qr_contract_url, 'Q', 3, 2);
	
	/*debug::print_r($dataInfo);die;
	debug::print_r($dataBen);
	debug::print_r($dataServ);
	debug::print_r($serialCus);
	debug::print_r($customer);
	debug::print_r($dataExt);
	debug::print_r($fee_ext);*/
	//Gathering - END
	
	//Title
	$pdf->setColor($blue[0], $blue[1], $blue[2]);
	if ($international_flight):
		$pdf->addTextWrap($leftMostX + 100, $verticalPointer, 200, $titleTextSize, '' . $saleContract['title']['international']);
	else:
		$pdf->addTextWrap($leftMostX + 100, $verticalPointer, 200, $titleTextSize, '' . $saleContract['title']['national']);
	endif;
	$verticalPointer -= 25;

	//Header 1
	GlobalFunctions::header1($pdf, $dataInfo, $langCode, $verticalPointer);
	$verticalPointer -= 55;
	
	//HEADER 2:
	//DATE AND TIME
	$pdf -> setColor($refText[0], $refText[1], $refText[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer,200,$stdTextSize,''.$saleContract['header2']['location_date']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($leftMostX + 120,$verticalPointer,80,$stdTextSize,''.$dataInfo['name_cit']);//location

	$emissionDate = GlobalFunctions::strToTimeDdMmYyyy($dataInfo['emission_date_sal']);
	$tempX = $leftMostX + 190;
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,160,$stdTextSize,'  '.date("d", $emissionDate).'    '.date("m", $emissionDate).'     '.date("y", $emissionDate));//emission date
	$pdf -> setColor($refText[0], $refText[1], $refText[2]);
	$pdf -> line($tempX,$verticalPointer - 2, $tempX,$verticalPointer - 2);
	$pdf -> line($tempX + 20,$verticalPointer + 10, $tempX + 20,$verticalPointer - 2);
	$pdf -> line($tempX + 40,$verticalPointer + 10, $tempX + 40,$verticalPointer - 2);
	$pdf -> line($tempX + 60,$verticalPointer + 10, $tempX + 60,$verticalPointer - 2);
	$pdf -> line($leftMostX + 100,$verticalPointer - 2, $tempX + 60,$verticalPointer - 2);
	$pdf -> addTextWrap($tempX + 5,$verticalPointer - 10,160,$smallTextSize,''.$saleContract['header2']['emission']);
	
	//COD SUCURSAL:
	$tempX += 70;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer - 2,160,$stdTextSize,''.$saleContract['header2']['codBranch']);
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> line($tempX + 45,$verticalPointer - 2,$tempX + 120,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX + 35,$verticalPointer,95,$stdTextSize,'' . $dataInfo['code'],'center');//cod. branch
	
	//CONTRACT TYPE
	$tempX += 130;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer - 2,160,$stdTextSize,''.$saleContract['header2']['contract']);
	$pdf -> line($tempX + 100,$verticalPointer - 2,$tempX + 140,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);

	if($traveler_log_cardnumber){ //CARD_NUMBER FROM A TRAVELER_LOG
		$pdf -> addTextWrap($tempX + 100,$verticalPointer,55,$stdTextSize,$traveler_log_cardnumber,'left');
	}else{
		if($dataInfo['card_number_sal']!=''){ //REGULAR SALE
			$a=$pdf -> addTextWrap($tempX + 100,$verticalPointer,48,$stdTextSize,$dataInfo['card_number_sal'],'center');
		}else{ //CORPORATIVE SALE
			$pdf -> addTextWrap($tempX + 100,$verticalPointer,48,$stdTextSize,'N/A','center');
		}
	}	

	//***********************************************SECTION 1: CLIENT DATA ********************************************************
	$parameter = new Parameter($db);
	$parameter -> setSerial_par('13');
	$parameter -> getData();
	
	//CLIENT DATA TITLE
	$verticalPointer -= 40;
	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['client']['title'].'</b>');//Client data title

	//MAIN SECTION
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setStrokeColor($refText[0],$refText[1],$refText[2]);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Last name
	$tempX = $leftMostX;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['last_name_cus'], 150);
	
	//First name
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['first_name_cus'], 150);
	
	//Document
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['document_cus'],'center');//document
	
	//Birth Date
	$tempX += 110;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['birthdate_cus'],'center');
	$pdf -> line($leftMostX + 528,$verticalPointer + 2,$leftMostX + 528,$verticalPointer - 8);
	
	//Bottom labels:
	$pdf -> line($leftMostX,$verticalPointer-8,$leftMostX + 528,$verticalPointer-8);
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract['client']['0']);
	
	
	//SECOND LINE
	$verticalPointer -= 25;
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Phone
	$tempX = $leftMostX;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['phone1_cus'],'center');
	
	//Email
	$tempX += 110;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['email_cus'], 150);
	
	//Addesss
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['full_address'], 268);
	$pdf -> line($leftMostX + 528,$verticalPointer + 2,$leftMostX + 528,$verticalPointer - 8);
	
	//Bottom labels:
	$pdf -> line($leftMostX,$verticalPointer-8,$leftMostX + 528,$verticalPointer-8);
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract['client']['1']);
	
	
	//THIRD LINE
	//Emergency contact Line:
	$verticalPointer -= 30;
	$tempX = $leftMostX;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,140,$stdTextSize,''.$saleContract['client']['3']);
	$pdf -> line($tempX + 140,$verticalPointer - 2,$tempX + 340,$verticalPointer - 2);
	$pdf -> line($tempX + 340,$verticalPointer + 8, $tempX + 340,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 140, $verticalPointer + 5, $stdTextSize, $customer['relative_cus'], 190);
	
	//Emergency Phone
	$tempX += 350;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,40,$stdTextSize,''.$saleContract['client']['4']);
	$pdf -> line($tempX + 40,$verticalPointer - 2, $tempX + 180,$verticalPointer - 2);
	$pdf -> line($tempX + 180, $verticalPointer + 8, $tempX + 180,$verticalPointer-2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 40, $verticalPointer + 5, $stdTextSize, $customer['relative_phone_cus'], 140);
	
	
	
	//********************************************* SECTION 1A: EXTRAS INFO **********************************************************
	//Extras section
	$verticalPointer -= 15;
	if(count($dataExt)>0){
	 $displayedLimit=$parameter->getValue_par();//Extras limit displayed.
	 //$displayedLimit=0;
		if(count($dataExt)<=$displayedLimit){
			$verticalPointer = GlobalFunctions::displayExtras($pdf,$dataExt,$verticalPointer,1,0, $langCode);
		}
	}

	//********************************************* SECTION 2: TRIP DATA **********************************************
	$verticalPointer -= 15;
	if($verticalPointer <= 100){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$beginDate=''.$dataInfo['begin_date_sal'];
	$endDate=''.$dataInfo['end_date_sal'];
	if($dataInfo['flights_pro']==0){
		$beginDate = $tripBeginDate;
		$endDate = $tripEndDate;
		$dataInfo['country_sal'] = $tripCountry;
		$dataInfo['city_sal'] = $tripCity;
	}

	if($beginDate && $endDate && $dataInfo['country_sal'] && $dataInfo['city_sal']){
		//Header line:
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['trip']['title1'].'</b>');

		$pdf -> setLineStyle(0.2,'square');
		$tempX = $leftMostX;
		$verticalPointer -= 5;
		$pdf -> setStrokeColor($black[0],$black[1],$black[2]);

		//Bottom Lines
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX + 2,$verticalPointer,528,$medTextSize,''.$saleContract['trip']['0']);
		$pdf -> addTextWrap($leftMostX,$verticalPointer-10,500,$smallTextSize,''.$saleContract['trip']['1']);

		//Departure Date
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 70, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $beginDate ) ) );
		$pdf -> line($tempX,$verticalPointer-2,$tempX + 130,$verticalPointer - 2);
		$pdf -> line($tempX,$verticalPointer + 10, $tempX,$verticalPointer - 2);
		$pdf -> line($tempX + 130,$verticalPointer + 10, $tempX + 130,$verticalPointer - 2);

		//End Date
		$tempX += 135;
		$pdf -> addTextWrap($tempX + 80, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $endDate ) ) );
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX  + 140,$verticalPointer-2);
		$pdf -> line($tempX + 140,$verticalPointer+10, $tempX + 140,$verticalPointer-2);

		//Duration:
		$tempX += 145;
		$fullDays = GlobalFunctions::getDaysDiffDdMmYyyy($beginDate, $endDate);
		$pdf -> addTextWrap($tempX + 50, $verticalPointer, 30, $stdTextSize, $fullDays);
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX + 80,$verticalPointer - 2);
		$pdf -> line($tempX + 80,$verticalPointer+10, $tempX + 80,$verticalPointer-2);

		//Destination
		$tempX += 85;
		$destinationString = $dataInfo['country_sal'];
		$destinationString .= $dataInfo['city_sal']== 'General'?'':' - ' . $dataInfo['city_sal'];
		GlobalFunctions::writeTextField($pdf, $tempX + 35, $verticalPointer + 5, $stdTextSize, $destinationString, 115);
		$pdf -> line($tempX,$verticalPointer-2, $tempX + 150,$verticalPointer-2);
		$pdf -> line($tempX + 150,$verticalPointer + 10, $tempX + 150,$verticalPointer - 2);
	}else{
		$verticalPointer += 35;
	}
	
	//********************************************* SECTION 2.1: VALID DATES **********************************************
	if($dataInfo['flights_pro']==0){
		$verticalPointer -= 40;
		if($verticalPointer <= 100){
			$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
			
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
		
		//Header line:
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['trip']['title2'].'</b>');
		
		$pdf -> setLineStyle(0.2,'square');
		$tempX = $leftMostX;
		$verticalPointer -= 5;
		$pdf -> setStrokeColor($black[0],$black[1],$black[2]);
		
		//Bottom Lines
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX + 2,$verticalPointer,528,$medTextSize,''.$saleContract['trip']['3']);
		$pdf -> addTextWrap($leftMostX,$verticalPointer-10,500,$smallTextSize,''.$saleContract['trip']['1']);
		
		//Departure Date
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 70, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $dataInfo['begin_date_sal'] ) ) );
		$pdf -> line($tempX,$verticalPointer-2,$tempX + 130,$verticalPointer - 2);
		$pdf -> line($tempX,$verticalPointer + 10, $tempX,$verticalPointer - 2);
		$pdf -> line($tempX + 130,$verticalPointer + 10, $tempX + 130,$verticalPointer - 2);
		
		//End Date
		$tempX += 135;
		$pdf -> addTextWrap($tempX + 80, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $dataInfo['end_date_sal'] ) ) );
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX  + 140,$verticalPointer-2);
		$pdf -> line($tempX + 140,$verticalPointer+10, $tempX + 140,$verticalPointer-2);
		
		//Duration:
		$tempX += 145;
		$fullDays = GlobalFunctions::getDaysDiffDdMmYyyy($dataInfo['begin_date_sal'], $dataInfo['end_date_sal']);
		$pdf -> addTextWrap($tempX + 50, $verticalPointer, 30, $stdTextSize, $fullDays);
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX + 80,$verticalPointer - 2);
		$pdf -> line($tempX + 80,$verticalPointer+10, $tempX + 80,$verticalPointer-2);
	}
	
	
	
	//***************************************************** SECTION 3: PRODUCT DATA ******************************************************
	$verticalPointer -= 40;
	if($verticalPointer <= 100){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['product']['title'].'</b>');

	//Init:
	$verticalPointer -= 5;
	$tempX = $leftMostX;
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setStrokeColor($refText[0],$refText[1],$refText[2]);
	
	//Product Name:
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,90,$stdTextSize,''.$saleContract['product']['0']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 90, $verticalPointer + 5, $stdTextSize, ''.$dataInfo['name_pbl'], 130);
	$pdf -> line($tempX + 90 ,$verticalPointer - 2, $tempX + 220,$verticalPointer - 2);
	$pdf -> line($tempX + 220,$verticalPointer + 10, $tempX + 220,$verticalPointer - 2);
	
	//Fee
	if(!$sale->flightRegisterGeneratesCardNumber($db, $sale->getSerial_sal()) || !isset($traveler_log_cardnumber)) {
		$tempX += 230;
		$width_measure = 50;
		$start_point = 70;
		$vertical_ctart_point = 120;
		if($dataServ){ 
			$tempX += 130;
			$width_measure += 30;
			$start_point += 18;
			$vertical_ctart_point += 48;
		}
		
		
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX,$verticalPointer,$width_measure,$stdTextSize,''.$saleContract['product']['1']);
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + $width_measure, $verticalPointer, $start_point, $stdTextSize,''.number_format(($dataInfo['fee_sal']), 2, '.', ''),'center');//only product fee
		$pdf -> line($tempX + $width_measure,$verticalPointer -2, $tempX + $vertical_ctart_point,$verticalPointer - 2);
		$pdf -> line($tempX + $vertical_ctart_point,$verticalPointer + 10, $tempX + $vertical_ctart_point,$verticalPointer - 2);
	}

	//Total Fee
	$display_total_price = true;
	if((!$sale->flightRegisterGeneratesCardNumber($db, $sale->getSerial_sal()) || !isset($traveler_log_cardnumber)) && !$dataServ) {
		$tempX += 130;
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract['product']['2']);
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.number_format($dataInfo['total_sal'], 2, '.', ''),'center');
		$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
		$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);
		$display_total_price = false;
	}
	
	if($sale->flightRegisterGeneratesCardNumber($db, $sale->getSerial_sal())) $display_total_price = false;	

	//***************************************************** SECTION 3.1: SERVICES DATA ******************************************************
	if($dataServ){
		$verticalPointer -= 40;
		if($verticalPointer <= 100){
			$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
			
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['service']['title'].'</b>');
	
		//Init:
		$verticalPointer -= 5;
		$tempX = $leftMostX;
		$pdf -> setLineStyle(0.2,'square');
		$pdf -> setStrokeColor($refText[0],$refText[1],$refText[2]);
		
		$totalServFee=0;
		foreach($dataServ as $dS){
			if($verticalPointer <= 100){
				$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
				
				$pdf -> ezNewPage();
				$verticalPointer = 755;
			}
			
			if($dS['fee_type_ser']=='PERDAY'){
				$service_price = ($dS['price_sbc'] * $dataInfo['days_sal']);
			}else{
				$service_price = $dS['price_sbc'];
			}
			
			//Service Name:
			$pdf -> setColor($refText[0],$refText[1],$refText[2]);
			$pdf -> addTextWrap($tempX,$verticalPointer,60,$stdTextSize,''.$saleContract['service']['0']);
			$pdf -> setColor($black[0], $black[1], $black[2]);
			GlobalFunctions::writeTextField($pdf, $tempX + 60, $verticalPointer + 5, $stdTextSize, ''.$dS['name_sbl'], 150);
			$pdf -> line($tempX + 60 ,$verticalPointer - 2, $tempX + 220,$verticalPointer - 2);
			$pdf -> line($tempX + 220,$verticalPointer + 10, $tempX + 220,$verticalPointer - 2);

			//Service Coverage
			$tempX += 230;
			$pdf -> setColor($refText[0],$refText[1],$refText[2]);
			$pdf -> addTextWrap($tempX,$verticalPointer,50,$stdTextSize,''.$saleContract['service']['1']);
			$pdf -> setColor($black[0], $black[1], $black[2]);
			$pdf -> addTextWrap($tempX + 50,$verticalPointer,70,$stdTextSize,''.number_format(($dS['coverage_sbc']), 2, '.', ''),'center');//only product fee
			$pdf -> line($tempX + 50,$verticalPointer -2, $tempX + 120,$verticalPointer - 2);
			$pdf -> line($tempX + 120,$verticalPointer + 10, $tempX + 120,$verticalPointer - 2);
			

			//Service Fee
			$tempX += 130;
			$pdf -> setColor($refText[0],$refText[1],$refText[2]);
			$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract['product']['1']);
			$pdf -> setColor($black[0], $black[1], $black[2]);
			$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.number_format($service_price, 2, '.', ''),'center');
			$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
			$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);
			
			
			$totalServFee += $service_price;
			$verticalPointer -= 15;
			$tempX = $leftMostX;
			
			/* ADD SERVICES TO BENEFITS LIST */
			$temp_ben_array['price_bxp'] = 0;
			$temp_ben_array['description_bbl'] = $dS['name_sbl'];
			$temp_ben_array['alias_con'] = number_format(($dS['coverage_sbc']), 2, '.', '');
			array_push($dataBen, $temp_ben_array);
			/* ADD SERVICES TO BENEFITS LIST */
		}
		
		/*//Total Fee
		$tempX += 360;
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract['service']['2']);
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.number_format($totalServFee, 2, '.', ''),'center');
		$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
		$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);*/
	 }
	 
	 if($display_total_price){
		$verticalPointer -= 5;
		$tempX = $leftMostX;
		if($verticalPointer <= 100){
			$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
			
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
		
		$tempX += 360;
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX,$verticalPointer,80,$stdTextSize,''.$saleContract['product']['2']);
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 80,$verticalPointer,88,$stdTextSize,''.number_format($dataInfo['total_sal'], 2, '.', ''),'center');
		$pdf -> line($tempX + 80,$verticalPointer - 2, $tempX + 168,$verticalPointer - 2);
		$pdf -> line($tempX + 168,$verticalPointer + 10, $tempX + 168,$verticalPointer - 2);
	 }
	 
	 
	//************************************************** SECTION 4: BENEFITS **********************************************
	$verticalPointer -= 35;
	if($verticalPointer<=100){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}
	
	$colWidth = 200;
	$valueWidth = 58;

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer + 12,546,$largeTextSize,'<b>'.$saleContract['benefits']['title'].'</b>');
	$pdf -> addTextWrap($leftMostX + $colWidth + $valueWidth + 10,$verticalPointer + 12,546,$largeTextSize,'<b>'.$saleContract['benefits']['title'].'</b>');

	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	if($dataBen){
		$count = 0;
		$verticalSep = 21;
		//Make the calculations for sorting the benefits in a final array
		$benefits_count = count($dataBen);
		($benefits_count % 2 != 0) ? $middle = (int)($benefits_count / 2) + 1 : $middle = $benefits_count / 2;
		//Getting the table's height
		$benefits_height = (int)(($benefits_count * $verticalSep) / 1.70);
		
		$benefits_final_array = array();
		$each_row = array();		
		foreach($dataBen as $key => $d){
			if($key == $middle) break;
			
			if($dataBen['price_bxp'] == 0){
				$displayValue = $d['alias_con'];
			}else{
				$displayValue = $d['price_bxp'].'  ('.$d['restriction_price_bxp'].' x '.$d['description_rbl'].')';
			}

			$each_row['0'] = $bullet.$d['description_bbl'];
			$each_row['1'] = $displayValue;
			if(array_key_exists($key+$middle, $dataBen)){
				$each_row['2'] = $bullet.$dataBen[$key+$middle]['description_bbl'];

				if($dataBen['price_bxp'] == 0){
					$each_row['3'] = $dataBen[$key+$middle]['alias_con'];
				}else{
					$each_row['3'] = $dataBen[$key+$middle]['price_bxp'].'  ('.$dataBen[$key+$middle]['restriction_price_bxp'].' x '.$dataBen[$key+$middle]['description_rbl'].')';
				}
			}			
			
			array_push($benefits_final_array, $each_row);
			$each_row = array();
		}

		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf->ezSetDy(-($max_height - $verticalPointer));
		$pdf->ezTable($benefits_final_array,
							'',
							'',
							array('showHeadings'=>0,
								'colGap' => 2,
								'shaded'=>2,
								'shadeCol' => array(0.95,0.97,0.98),
								'shadeCol2' => array(1,1,1),
								'showLines'=>0,
								'xPos'=>'310',
								'xOrientation'=>'center',
								'innerLineThickness' => 0.8,
								'outerLineThickness' => 0.8,
								'fontSize' => $stdTextSize,
								'titleFontSize' => $stdTextSize,
								'cols'=>array(
										'0'=>array('justification'=>'left','width'=>210),
										'1'=>array('justification'=>'center','width'=>60),
										'2'=>array('justification'=>'left','width'=>210),
										'3'=>array('justification'=>'center','width'=>60)
										)
							)
		);
	}

	//************************************************** SECTION 5: RECOMMENDATIONS **********************************************
	$verticalPointer -= $benefits_height;
	if($verticalPointer<=100){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract['recomen']['title'].'</b>');
	
	
	$verticalPointer -= 5;
	$verticalStep = 10;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$rows = count($saleContract['recomen']);
	for($i=0; $i < $rows - 1; $i++){
		$widthText = $pdf -> getTextWidth($stdTextSize,$saleContract['recomen'][$i]);
		if($widthText < 546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer, 538,$stdTextSize,$bullet.$saleContract['recomen'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract['recomen'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j <= ($widthText/546); $j++){
				$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer <= 50){
					$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
					
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer <= 50){
			$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
			
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}
	
	//********************************* SECTION 6: PARTICULAR CONDITIONS **********************************************
	if($pConditions){
		$verticalPointer -= 25;
		if ($verticalPointer <= 100) {
			$pdf->addPngFromFile($qr_contract_url, 500, 35, 70);

			$pdf->ezNewPage();
			$verticalPointer = 755;
		}

		$pdf->setStrokeColor($lightBlue[0], $lightBlue[1], $lightBlue[2]);
		$pdf->setLineStyle(16);
		$pdf->line($leftMostX + 2, $verticalPointer + 16, $leftMostX + 519, $verticalPointer + 16);
		$pdf->setColor($red[0], $red[1], $red[2]);
		$pdf->addTextWrap($leftMostX, $verticalPointer + 12, 546, $largeTextSize, '<b>' . $saleContract['pcon']['title'] . '</b>');


		$verticalPointer -= 5;
		$verticalStep = 10;
		$pdf->setColor($refText[0], $refText[1], $refText[2]);
		$rows = count($pConditions);
		
		foreach($pConditions as $pcon){
			$pcondition_formated = utf8_decode(html_entity_decode(trim(strip_tags($pcon['name_prc']))));
			$widthText = $pdf->getTextWidth($stdTextSize, $pcondition_formated);
			
			if ($widthText < 540) {
				$pdf->addTextWrap($leftMostX, $verticalPointer, 520, $stdTextSize, $bullet . $pcondition_formated, 'full');
				$verticalPointer -= $verticalStep;
			} else {
				$a = $pdf->addTextWrap($leftMostX, $verticalPointer, 520, $stdTextSize, $bullet . $pcondition_formated, 'full');
				$verticalPointer -= $verticalStep;
				for ($j = 1; $j <= (int)($widthText / 500); $j++) {
					$a = $pdf->addTextWrap($leftMostX, $verticalPointer, 520, $stdTextSize, '' . $a, 'full');
					$verticalPointer -= $verticalStep;
					if ($verticalPointer <= 50) {
						$pdf->addPngFromFile($qr_contract_url, 500, 35, 70);

						$pdf->ezNewPage();
						$verticalPointer = 755;
					}
				}
			}
			
			if ($verticalPointer <= 50) {
				$pdf->addPngFromFile($qr_contract_url, 500, 35, 70);

				$pdf->ezNewPage();
				$verticalPointer = 755;
			}
		}
	}
	
	//************************************************** SECTION 6: EXCLUSIONS **********************************************
	$verticalPointer -= 25;
	if($verticalPointer<=100){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract['exclu']['title'].'</b>');
	
	
	$verticalPointer -= 5;
	$verticalStep = 10;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$rows=count($saleContract['exclu']);
	for($i=0; $i<$rows-1; $i++){
		//$pdf -> ezText($saleContract['exclu'][$i],10);
		$widthText=$pdf -> getTextWidth($stdTextSize,$saleContract['exclu'][$i]);
		if($widthText<546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract['exclu'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract['exclu'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j<=($widthText/546); $j++){
				$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer<=50){
					$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
					
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer<=50){
			$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
			
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}

	//************************************************** SECTION 7: ASSISTANCE PHONES **********************************************
	$verticalPointer -= 25;
	if($verticalPointer<=100){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract['phones']['title'].'</b>');
	
	//LEYEND
	$verticalPointer -= 15;
	$verticalStep = 10;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$widthText = $pdf -> getTextWidth($stdTextSize,$saleContract['phones']['leyend']);
	if($widthText<538){
		$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$saleContract['phones']['leyend']);
		$verticalPointer -= $verticalStep;
	}else{
		$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$saleContract['phones']['leyend']);
		$verticalPointer -= $verticalStep;
		for($j=1; $j<=($widthText/538); $j++){
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
			$verticalPointer -= $verticalStep;
			if($verticalPointer <= 50){
				$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);

				$pdf -> ezNewPage();
				$verticalPointer = 755;
			}
		}
	}
	
	//FIRST INDICATION
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$widthText = $pdf -> getTextWidth($stdTextSize,$saleContract['phones']['phones']);
	if($widthText<546){
		$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$spaced_bullet.$saleContract['phones']['phones']);
		$verticalPointer -= $verticalStep;
	}else{
		$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$spaced_bullet.$saleContract['phones']['phones']);
		$verticalPointer -= $verticalStep;
		for($j=1; $j<=($widthText/546); $j++){
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
			$verticalPointer -= $verticalStep;
			if($verticalPointer <= 50){
				$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);

				$pdf -> ezNewPage();
				$verticalPointer = 755;
			}
		}
	}

	$verticalSep = 21;
	$assistantce_phones_height = (int)((count($assistance_phones) * $verticalSep) / 1.20);
	$verticalPointer -= 5;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf->ezSetDy(-($max_height - $verticalPointer));
	$pdf->ezTable($assistance_phones,
						'',
						'',
						array('showHeadings'=>0,
							'shaded'=>1,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
									'0'=>array('justification'=>'center','width'=>80),
									'1'=>array('justification'=>'center','width'=>80),
									'2'=>array('justification'=>'center','width'=>80),
									'3'=>array('justification'=>'center','width'=>80)
									)
						)
	);
	
	if($pdf->ezGetDy() >= $verticalPointer): //FIX FOR MULTI-PAGE ASSITANCE PHONE TABLE
		$verticalPointer = $pdf->ezGetDy();
		$assistantce_phones_height = 15;
	endif;
	
	$verticalPointer -= $assistantce_phones_height;
	$rows=count($saleContract['phones']['other_marks']);
	for($i=0; $i<$rows-1; $i++){
		//$pdf -> ezText($saleContract['exclu'][$i],10);
		$widthText=$pdf -> getTextWidth($stdTextSize,$saleContract['phones']['other_marks'][$i]);
		if($widthText<546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$spaced_bullet.$saleContract['phones']['other_marks'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$spaced_bullet.$saleContract['phones']['other_marks'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j<=($widthText/546); $j++){
				$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer<=50){
					$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
					
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer<=50){
			$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
			
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}
	
	//************************************************** SECTION 8: SIGN **********************************************
	$verticalPointer -= 15;
	
	if($verticalPointer<=50){
		$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
		
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}
	
	$widthText_sign=$pdf -> getTextWidth($stdTextSize,$saleContract['signature']);
	if($widthText_sign<450){
		$pdf -> addTextWrap($leftMostX,$verticalPointer,450,$stdTextSize,$saleContract['signature']);
		$verticalPointer -= $verticalStep;
	}
	else{
		$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,450,$stdTextSize,$saleContract['signature']);
		$verticalPointer -= $verticalStep;
		for($j=1; $j<=($widthText_sign/450); $j++){
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,450,$stdTextSize,''.$a);
			$verticalPointer -= $verticalStep;
			
			if($verticalPointer<=50 && $a){
				$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);

				$pdf -> ezNewPage();
				$verticalPointer = 755;
			}
		}
	}
	
	//$pdf -> addTextWrap($leftMostX,$verticalPointer, 450, $stdTextSize,$saleContract['signature']);
	$verticalPointer -= 25;
	
	if(count($dataExt)>0){
		if(count($dataExt)>$displayedLimit){
			$verticalPointer= GlobalFunctions::displayExtras($pdf,$dataExt,$verticalPointer,2,$dataInfo, $langCode);
		}
	}
	
	$pdf->setEncryption('','',array('print'));
	
	$pdf -> addPngFromFile ($qr_contract_url, 500, 35, 70);
	
	if($display=='1'){
		$pdf -> ezStream();
	}else{
		$pdfcode = $pdf -> ezOutput();
		if($multi_sale){
			$fp=fopen(DOCUMENT_ROOT .'modules/sales/multiSaleContracts/contract_'.$serial_sal.'.pdf','wb');
		}else{
			$fp=fopen(DOCUMENT_ROOT .'modules/sales/contracts/contract_'.$serial_sal.'.pdf','wb');
		}		
		fwrite($fp,$pdfcode);
		fclose($fp);
	}
?>