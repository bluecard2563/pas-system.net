<?php

/*****************************************  SPANISH TEXTS *********************************************/
$textES = array();
$textES['header1'] = array(
	'title' => 'Detalle de venta',
	'branch' => 'Sucursal de Comercializador: ',
	'counter' => 'Counter: ','title2' => 'Registro de Viaje'
);
$textES['header2'] = array(
	'codBranch' => 'COD. SUC: ',
	'location_date' => html_entity_decode('Lugar y Fecha de emisi&oacute;n:'),
	'emission' => 'dd      mm       aa',
	'contract' => html_entity_decode('Contrato de adhesi&oacute;n N:')
);
$textES['client'] = array(
	'title' => 'DATOS DEL CLIENTE',
	'extras' => 'ADICIONALES',
	'0' => 					'          Apellidos                                            Nombres                                    C.C. o Pasaporte                   Fec. de Nacimiento',
	'1' => html_entity_decode('Tel&eacute;fono(s)                                          E-mail                                                          Direcci&oacute;n/Ciudad/Pa&iacute;s'),
	'2' =>  NULL,
	'3' => 'En caso de emergencia notificar a:',
	'4' => html_entity_decode('Tel&eacute;fono:'),
	'5' => html_entity_decode('      Fec. de Nacimiento                                    Relaci&oacute;n                                               E-mail'),
	'table' => array(
		'0' => 'C.I / Pasaporte',
		'1' => 'Nombre',
		'2' => 'Edad',
		'3' => 'Tarifa USD:'
	)
);
$textES['trip'] = array(
	'title1' => 'DATOS DEL VIAJE',  
	'title2' => 'VIGENCIA',
	'0' => html_entity_decode('Fecha de Salida                                Fecha de Regreso                                Duraci&oacute;n                      Destino'),
	'1' => 					  '                                 dd    mm     aa                                                           dd    mm     aa                                    dd',
	'2' => NULL,
	'3' => html_entity_decode('Fecha de Inicio                                Fecha de Finalizaci&oacute;n                           Duraci&oacute;n'),
	'4' => html_entity_decode('Fecha de Inicio                                      Fecha de Finalizaci&oacute;n                                  D&iacute;as')
);
$textES['product'] = array(
	'title' => 'DATOS DEL PRODUCTO',
	'0' => 'Nombre del Producto:',
	'1' => 'Tarifa USD:',
	'2' => 'TOTAL TARIFA USD:'
);
$textES['service'] = array(
	'title' => 'SERVICIOS',
	'0' =>  'Servicio:',
	'1' =>  'Cobertura:',
	'2' =>  'Tarifa Servicios:'
);
$textES['benefits'] = array(
	'title' => 'BENEFICIOS                                                                                                                                                                         COBERTURA',
	'0' => 'El Resto de Beneficios se encuentran Detallados en Condiciones Generales'
);
$textES['recomen'] = array(
	'title' => 'RECOMENDACIONES:',
	'0' => html_entity_decode('Comunicarse previamente y de manera inmediata con la Central de Servicios, antes de comprometer cualquier tipo de gasto. Recuerde que la central de Servicios determinar&aacute; la forma de manejar el caso.'),
	'1' => html_entity_decode('Si la condici&oacute;n de emergencia presentada, le impide la comunicaci&oacute;n inmediata por causa de fuerza mayor podr&aacute; contratar los servicios requeridos, debiendo comunicarse con la Central de Servicios dentro de las 24 horas siguientes a la ocurrencia del hecho.'),
	'2' => html_entity_decode('Presentar la documentaci&oacute;n original con sus respaldos respectivos.'),
	'3' => html_entity_decode('En caso de negaci&oacute;n de Visa se debe notificar con 72 horas antes del inicio de cobertura.'),
	'4' => html_entity_decode('Aseg&uacute;rese de leer el documento de Condiciones generales que se adjuntan, y las condiciones particulares que se detallan a continuaci&oacute;n.'),
);
$textES['exclu'] = array(
	'title' => 'EXCLUSIONES:', 
	'0' => html_entity_decode('Iniciar, recibir, continuar o finalizar un tratamiento m&eacute;dico.'),
	'1' => html_entity_decode('Tratamientos cosm&eacute;ticos, de cirug&iacute;a est&eacute;tica, reconstrucci&oacute;n, dermatolog&iacute;a en caso de tratamiento de acn&eacute;, dermatitis.'),
	'2' => html_entity_decode('Enfermedades cr&oacute;nicas y preexistentes, cong&eacute;nitas o recurrentes, conocidas o no por el Beneficiario.'),
	'3' => html_entity_decode('Tratamientos homeop&aacute;ticos y quiropr&aacute;cticos; acupuntura entre otras.'),
	'4' => html_entity_decode('Lesiones o eventos originados en hechos auto infringidos, suicidio y su tentativa.'),
	'5' => html_entity_decode('Tratamientos o enfermedades mentales, nerviosas, trastornos psicol&oacute;gicos, psiqui&aacute;tricos, estr&eacute;s'),
	'6' => html_entity_decode('Afecciones, enfermedades, accidentes derivados de Ingesti&oacute;n o administraci&oacute;n de t&oacute;xicos (drogas), narc&oacute;ticos y/o  bebidas alcoh&oacute;licas.'),
	'7' => html_entity_decode('Tratamientos hechos por especialistas que no pertenezcan al equipo profesional m&eacute;dico indicados por la central.'),
	'8' => html_entity_decode('Eventos ocasionados por la manipulaci&oacute;n o uso de qu&iacute;micos o productos nocivos.'),
	'9' => html_entity_decode('Diagn&oacute;sticos, ex&aacute;menes, y  tratamientos del embarazo o interrupci&oacute;n voluntaria del mismo. '),
	'10' => html_entity_decode('Chequeos m&eacute;dicos y toda clase de tratamientos prolongados, ni aquellos que requieran el control de una patolog&iacute;a.'),
	'11' => html_entity_decode('Cualquier tipo de tratamiento de ginecolog&iacute;a, odontolog&iacute;a, otorrinolaringolog&iacute;a,  oftalmol&oacute;gica'),
	'12' => html_entity_decode('Tratamientos o cirug&iacute;as no emergentes de &uacute;tero, anexos y mamas. Tampoco la implementaci&oacute;n, seguimiento y control de la fertilidad, o m&eacute;todo anticonceptivo de cualquier tipo.'),
	'13' => html_entity_decode('Infecciones Urinarias, se atender&aacute; la emergencia hasta su confirmaci&oacute;n diagnostica.'),
	'14' => html_entity_decode('Tratamientos urol&oacute;gicos, tratamiento quir&uacute;rgico, c&aacute;lculos renales, biliares, arenillas, nefrolitiasis o cualquier tipo de complicaci&oacute;n derivado de este tipo. Se atender&aacute; &uacute;nicamente hasta su confirmaci&oacute;n diagn&oacute;stica.'),
	'15' => html_entity_decode('Incidentes durante viajes realizados en contra de recomendaci&oacute;n o prescripci&oacute;n m&eacute;dica.'),
	'16' => html_entity_decode('Gastos presentados  para su reintegro pasados los180 d&iacute;as de ocurrido el hecho.'),
	'17' => html_entity_decode('Todos los casos de asistencia que no sean documentados dentro de los 180 d&iacute;as plazo.'),
	'18' => html_entity_decode('Enfermedades end&eacute;micas o epid&eacute;micas.'),
	'19' => html_entity_decode('Trasplante de &oacute;rganos y tejidos.'),
	'20' => html_entity_decode('Eventos consecuencia de actos de guerra insurrecci&oacute;n, terrorismo, motines, huelgas.'),
	'21' => html_entity_decode('Intervenciones quir&uacute;rgicas que requieran la implantaci&oacute;n o reparaci&oacute;n de pr&oacute;tesis, etc.'),
	'22' => html_entity_decode('Enfermedades de transmisi&oacute;n sexual, VIH (SIDA)'),
	'23' => html_entity_decode('Controles de tensi&oacute;n arterial o enfermedades  hipertensi&oacute;n o hipotensi&oacute;n, s&iacute;ncopes.'),
	'24' => html_entity_decode('Enfermedades   Vasculares tales como varices, trombocitopenia, trombosis, hemorroides, etc., sin que la enumeraci&oacute;n tenga car&aacute;cter taxativo.'),
	'25' => html_entity_decode('Enfermedades o condiciones oncol&oacute;gicas, diabetes, desordenes cardiovasculares, hepatitis, infecciones renales, enfermedades respiratorias cr&oacute;nicas, alergias estacionarias, Enfermedades gastrointestinales cr&oacute;nicas tales como gastritis, &uacute;lceras p&eacute;pticas, pancreatitis, colitis.'),
	'26' => html_entity_decode('Pr&aacute;ctica profesional, y no profesional  de deportes peligrosos. (Salvo se pague la tarifa adicional o se contrate el producto que los cubra).'),
	'27' => html_entity_decode('Accidentes ocurridos en vuelos ilegales o sin licencia.'),
	'28' => html_entity_decode('Accidentes o enfermedades relacionadas con la participaci&oacute;n del Beneficiario como parte de cuerpos armados, de polic&iacute;a o bomberos.'),
	'29' => html_entity_decode('Accidentes o enfermedades consecuencia del trabajo fuera del pa&iacute;s de residencia permanente o habitual del Beneficiario.'),
);
$textES['phones']['title'] = html_entity_decode('TEL&Eacute;FONOS DE ASISTENCIA:');







/*****************************************  ENGLISH TEXTS *********************************************/
$textEN = array();
$textEN['header1'] = array(
	'title' => 'Sale Detail',
	'branch' => 'Marketer Branch: ',
	'counter' => 'Contract: ',
	'title2' => 'Trip Log'
);
$textEN['header2'] = array(
	'codBranch' => 'COD. SUC: ',
	'location_date' => html_entity_decode('Place and Date of emission:'),
	'emission' => 'dd      mm       yy',
	'contract' => html_entity_decode('Accession Contract')
);
$textEN['client'] = array(
	'title' => 'CLIENT INFORMATION',
	'extras' => 'ADDITIONALS',
	'0' => '          Last Names                                           Names                                    ID Or Passport                   Date of Birth',
	'1' => html_entity_decode('Phone(s)                                             E-mail                                                          Address/City/Country'),
	'2' => NULL,
	'3' => 'In case of emergency notify to:',
	'4' => html_entity_decode('Tel&eacute;fono:'),
	'5' => html_entity_decode('      Birth Date                                    Relationship                                               E-mail'),
	'table' => array(
		'0' => 'ID / Passport',
		'1' => 'Names',
		'2' => 'Age',
		'3' => 'Fare USD:'
	)
);
$textEN['trip'] = array(
	'title1' => 'TRIP INFORMATION',  
	'title2' => 'VALIDITY',
	'0' => html_entity_decode('Departure Date                                Return Date                                      Duration                      Destination'),
	'1' => 					  '                                 dd    mm     yy                                                           dd    mm     yy                                    dd',
	'2' => NULL,
	'3' => html_entity_decode('Begin Date                                       End Date                                      Duration'),
	'4' => html_entity_decode('Begin Date                                            End Date                                             Duration')
);
$textEN['product'] = array(
	'title' => 'PRODUCT DATA',
	'0' => 'Product Name:',
	'1' => 'Fare USD:',
	'2' => 'Total Fare USD:'
);
$textEN['service'] = array(
	'title' => 'SERVICES',
	'0' =>  'Service:',
	'1' =>  'Coverage:',
	'2' =>  'Service Fare:'
);
$textEN['benefits'] = array(
	'title' => 'BENEFITS                                                                                                                                                                             COVERAGE',
	'0' => 'The rest of the benefits are detailed with the General Conditions'
);
$textEN['recomen'] = array(
	'title' => 'RECOMENDATIONS:',
	'0' => html_entity_decode('Communicate previously in an immediate way to the Central of Services'),
	'1' => html_entity_decode('If the emergency situation does not allow to contact us immediately, you may engage the required services and you should contact the Central of Services within 24 hours of occurrence'),
	'2' => html_entity_decode('Services available within 24 hours from the occurrence.'),
	'3' => html_entity_decode('Present all the original documentation with the corresponding backup.'),
	'4' => html_entity_decode('In case of Visa denial, it must be notified 72 hours before the coverage start.')
);
$textEN['exclu'] = array(
	'title' => 'EXCLUSIONS:',
	'0' => 'To initiate, receive, continue or finish medical treatment.',
	'1' => 'Chronic and preexisting diseases, whether congenital or recurrent, whether known or unknown by the Insured.',
	'2' => 'Homoeopathic and chiropractic treatments, acupuncture, etc.',
	'3' => 'Self-inflicted injuries or incidents caused by the Insured.',
	'4' => 'Treatment of mental or nervous conditions or illnesses.',
	'5' => 'Treatments done by professionals outside the medical equipment specified by our service center.',
	'6' => 'Events caused by the manipulation and/or use of chemicals and/or products that are hazardous to health',
	'7' => 'Diagnosis, monitoring, exams, complications and treatment for pregnancy or the voluntary interruption of the same.',
	'8' => 'Medical attention services for control, even if prescribed by the treating physician for the follow-up of an illness diagnosed',
	'9' => 'Medical check-ups and all kinds of prolonged treatment.',
	'10' => 'Gynecological Treatments, Urological, Dental care, otorhinolaryngological, Ophthalmology',
	'11' => 'Any expense presented for reimbursement more than 180 days after the fact.',
	'12' => 'All cases of medical assistance that are not documented.',
	'13' => 'Endemic or epidemic illnesses',
	'14' => 'Organ and tissue transplants',
	'15' => 'Events caused by acts of war declared or not declared, insurgency, invasion by foreign enemy, civil war, terrorism and disturbance of the peace, riots, uprisings, strikes and administrative acts.',
	'16' => 'Surgical interventions that require inserting and/or repairing prosthesis, etc',
	'17' => 'Sexually transmitted diseases, (HIV) positive virus.',
	'18' => 'Controls of blood pressure or illness related to hypertension – hypotension',
	'19' => 'Oncological diseases or conditions, diabetes, chronic respiratory illnesses, chronic renal infections, hepatitis',
	'20' => 'Consequences derived from professional activities, especially dangerous sports',
	'21' => 'Accidents that may happen in illegal or unlicensed flights, as well as for those in which being part of the crew plays a part.',
	'22' => 'Accidents or illnesses related to the Cardholder and/or Client’s participation in the armed forces, police force or fire brigade.'
);
$textEN['phones']['title'] = html_entity_decode('ASSISTANCE PHONES:');

$saleContract = array('es' => $textES, 'en' => $textEN);
$saleContract = $saleContract[$lang_string];

?>