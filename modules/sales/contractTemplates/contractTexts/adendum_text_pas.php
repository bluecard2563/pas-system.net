<?php


/*****************************************  SPANISH TEXTS *********************************************/
$textES = array();

$textES['qr'] = array(
	'contract' => 'Contrato: ',
	'verification_label' => ' Cod. Verificador: '
);
$textES['header1'] = array(
//	'title' => 'Detalle de venta',
	'branch' => 'Sucursal: ',
	'counter' => 'Counter: ',
//    'title2' => 'Registro de Viaje'
);
$textES['header2'] = array(
	'codBranch' => 'COD. SUC: ',
	'location_date' => utf8_decode(html_entity_decode('Lugar y Fecha de emisi&oacute;n:')),
	'emission' => 'dd      mm       aa',
	'contract' => utf8_decode(html_entity_decode('Contrato de adhesi&oacute;n N:'))
);
$textES['client'] = array(
	'title' => 'DATOS DEL CLIENTE',
	'extras' => 'ADICIONALES',
	'0' => 					'          Apellidos                                            Nombres                                    C.C. o Pasaporte                   Fec. de Nacimiento',
	'1' => utf8_decode(html_entity_decode('Tel&eacute;fono(s)                                          E-mail                                                          Direcci&oacute;n/Ciudad/Pa&iacute;s')),
	'2' =>  NULL,
	'3' => 'En caso de emergencia notificar a:',
	'4' => utf8_decode(html_entity_decode('Tel&eacute;fono:')),
	'5' => utf8_decode(html_entity_decode('      Fec. de Nacimiento                                    Relaci&oacute;n                                               E-mail')),
	'table' => array(
		'0' => 'C.I / Pasaporte',
		'1' => 'Nombre',
		'2' => 'Edad',
		'3' => 'Tarifa USD:'
	)
);
$textES['trip'] = array(
	'title1' => 'DATOS DEL VIAJE',  
	'title2' => 'VIGENCIA',
	'0' => utf8_decode(html_entity_decode('Fecha de Salida                                Fecha de Regreso                                Duraci&oacute;n                      Destino')),
	'1' => 					  '                                 dd    mm     aa                                                           dd    mm     aa                                    dd',
	'2' => NULL,
	'3' => utf8_decode(html_entity_decode('Fecha de Inicio                                Fecha de Finalizaci&oacute;n                           Duraci&oacute;n')),
	'4' => utf8_decode(html_entity_decode('Fecha de Inicio                                      Fecha de Finalizaci&oacute;n                                  D&iacute;as'))
);
$textES['product'] = array(
	'title' => 'DATOS DEL PRODUCTO',
	'0' => 'Nombre del Producto:',
	'1' => 'Tarifa USD:',
	'2' => 'TOTAL TARIFA USD:'
);
$textES['service'] = array(
	'title' => 'SERVICIOS ADICIONALES',
	'0' =>  'Servicio:',
	'1' =>  'Cobertura USD:',
	'2' =>  'Tarifa Servicios USD:'
);
$textES['benefits'] = array(
	'title' => 'BENEFICIOS                                                                      COBERTURA',
	'0' => 'El Resto de Beneficios se encuentran Detallados en Condiciones Generales'
);
$textES['recomen'] = array(
	'title' => 'RECOMENDACIONES:',
	'0' => utf8_decode(html_entity_decode('Comunicarse previamente y de manera inmediata con la Central de Servicios, antes de comprometer cualquier tipo de gasto. Recuerde que la central de Servicios determinar&aacute; la forma de manejar el caso.')),
	'1' => utf8_decode(html_entity_decode('Si la condici&oacute;n de emergencia presentada, le impide la comunicaci&oacute;n inmediata por causa de fuerza mayor podr&aacute; contratar los servicios requeridos, debiendo comunicarse con la Central de Servicios dentro de las 24 horas siguientes a la ocurrencia del hecho.')),
	'2' => utf8_decode(html_entity_decode('Presentar la documentaci&oacute;n original con sus respaldos respectivos.')),
	'3' => utf8_decode(html_entity_decode('En caso de negaci&oacute;n de Visa se debe notificar con 72 horas antes del inicio de cobertura.')),
	'4' => utf8_decode(html_entity_decode('Aseg&uacute;rese de leer el documento de Condiciones generales que se adjuntan, y las condiciones particulares que se detallan a continuaci&oacute;n.')),
);
$textES['exclu'] = array(
	'title' => 'EXCLUSIONES:', 
	'0' => utf8_decode(html_entity_decode('Iniciar, recibir, continuar o finalizar un tratamiento m&eacute;dico.')),
	'1' => utf8_decode(html_entity_decode('Tratamientos cosm&eacute;ticos, de cirug&iacute;a est&eacute;tica, reconstrucci&oacute;n, dermatolog&iacute;a en caso de tratamiento de acn&eacute;, dermatitis.')),
	'2' => utf8_decode(html_entity_decode('Enfermedades cr&oacute;nicas y preexistentes, cong&eacute;nitas o recurrentes, conocidas o no por el Beneficiario.')),
	'3' => utf8_decode(html_entity_decode('Tratamientos homeop&aacute;ticos y quiropr&aacute;cticos; acupuntura entre otras.')),
	'4' => utf8_decode(html_entity_decode('Lesiones o eventos originados en hechos auto infringidos, suicidio y su tentativa.')),
	'5' => utf8_decode(html_entity_decode('Tratamientos o enfermedades mentales, nerviosas, trastornos psicol&oacute;gicos, psiqui&aacute;tricos, estr&eacute;s')),
	'6' => utf8_decode(html_entity_decode('Afecciones, enfermedades, accidentes derivados de Ingesti&oacute;n o administraci&oacute;n de t&oacute;xicos (drogas)), narc&oacute;ticos y/o  bebidas alcoh&oacute;licas.')),
	'7' => utf8_decode(html_entity_decode('Tratamientos hechos por especialistas que no pertenezcan al equipo profesional m&eacute;dico indicados por la central.')),
	'8' => utf8_decode(html_entity_decode('Eventos ocasionados por la manipulaci&oacute;n o uso de qu&iacute;micos o productos nocivos.')),
	'9' => utf8_decode(html_entity_decode('Diagn&oacute;sticos, ex&aacute;menes, y  tratamientos del embarazo o interrupci&oacute;n voluntaria del mismo. ')),
	'10' => utf8_decode(html_entity_decode('Chequeos m&eacute;dicos y toda clase de tratamientos prolongados, ni aquellos que requieran el control de una patolog&iacute;a.')),
	'11' => utf8_decode(html_entity_decode('Cualquier tipo de tratamiento de ginecolog&iacute;a, odontolog&iacute;a, otorrinolaringolog&iacute;a,  oftalmol&oacute;gica')),
	'12' => utf8_decode(html_entity_decode('Tratamientos o cirug&iacute;as no emergentes de &uacute;tero, anexos y mamas. Tampoco la implementaci&oacute;n, seguimiento y control de la fertilidad, o m&eacute;todo anticonceptivo de cualquier tipo.')),
	'13' => utf8_decode(html_entity_decode('Infecciones Urinarias, se atender&aacute; la emergencia hasta su confirmaci&oacute;n diagnostica.')),
	'14' => utf8_decode(html_entity_decode('Tratamientos urol&oacute;gicos, tratamiento quir&uacute;rgico, c&aacute;lculos renales, biliares, arenillas, nefrolitiasis o cualquier tipo de complicaci&oacute;n derivado de este tipo. Se atender&aacute; &uacute;nicamente hasta su confirmaci&oacute;n diagn&oacute;stica.')),
	'15' => utf8_decode(html_entity_decode('Incidentes durante viajes realizados en contra de recomendaci&oacute;n o prescripci&oacute;n m&eacute;dica.')),
	'16' => utf8_decode(html_entity_decode('Gastos presentados  para su reintegro pasados los180 d&iacute;as de ocurrido el hecho.')),
	'17' => utf8_decode(html_entity_decode('Todos los casos de asistencia que no sean documentados dentro de los 180 d&iacute;as plazo.')),
	'18' => utf8_decode(html_entity_decode('Enfermedades end&eacute;micas o epid&eacute;micas.')),
	'19' => utf8_decode(html_entity_decode('Trasplante de &oacute;rganos y tejidos.')),
	'20' => utf8_decode(html_entity_decode('Eventos consecuencia de actos de guerra insurrecci&oacute;n, terrorismo, motines, huelgas.')),
	'21' => utf8_decode(html_entity_decode('Intervenciones quir&uacute;rgicas que requieran la implantaci&oacute;n o reparaci&oacute;n de pr&oacute;tesis, etc.')),
	'22' => utf8_decode(html_entity_decode('Enfermedades de transmisi&oacute;n sexual, VIH (SIDA)')),
	'23' => utf8_decode(html_entity_decode('Controles de tensi&oacute;n arterial o enfermedades  hipertensi&oacute;n o hipotensi&oacute;n, s&iacute;ncopes.')),
	'24' => utf8_decode(html_entity_decode('Enfermedades   Vasculares tales como varices, trombocitopenia, trombosis, hemorroides, etc., sin que la enumeraci&oacute;n tenga car&aacute;cter taxativo.')),
	'25' => utf8_decode(html_entity_decode('Enfermedades o condiciones oncol&oacute;gicas, diabetes, desordenes cardiovasculares, hepatitis, infecciones renales, enfermedades respiratorias cr&oacute;nicas, alergias estacionarias, Enfermedades gastrointestinales cr&oacute;nicas tales como gastritis, &uacute;lceras p&eacute;pticas, pancreatitis, colitis.')),
	'26' => utf8_decode(html_entity_decode('Pr&aacute;ctica profesional, y no profesional  de deportes peligrosos. (Salvo se pague la tarifa adicional o se contrate el producto que los cubra).')),
	'27' => utf8_decode(html_entity_decode('Accidentes ocurridos en vuelos ilegales o sin licencia.')),
	'28' => utf8_decode(html_entity_decode('Accidentes o enfermedades relacionadas con la participaci&oacute;n del Beneficiario como parte de cuerpos armados, de polic&iacute;a o bomberos.')),
	'29' => utf8_decode(html_entity_decode('Accidentes o enfermedades consecuencia del trabajo fuera del pa&iacute;s de residencia permanente o habitual del Beneficiario.')),
);
$textES['pcon'] = array(
	'title' => 'CONDICIONES PARTICULARES:'
);
$textES['phones'] = array(
	'title' => html_entity_decode('TEL&Eacute;FONOS DE ASISTENCIA:'),
	'local_title' => html_entity_decode('L&iacute;neas Locales'),
        'international_title' => html_entity_decode('L&iacute;neas Internacionales'),
        'local_label' => html_entity_decode('L&iacute;neas Gratuitas &uacute;nicamente para llamada local.'),
        'international_label' => html_entity_decode('L&iacute;neas que puede marcar desde cualquier lugar del mundo.'),
        'phones' => array(
		'0' => array (
                    '0' => utf8_decode(html_entity_decode('Argentina')),
                    '1' => utf8_decode(html_entity_decode('0800-666-1599')),
                    '2' => utf8_decode(html_entity_decode('M&eacute;xico')),
                    '3' => utf8_decode(html_entity_decode('001866-600-8936')),
                    '4' => utf8_decode(html_entity_decode('Alemania')),
                    '5' => utf8_decode(html_entity_decode('(+)49-69-2222-7544')),
                    '6' => utf8_decode(html_entity_decode('Italia')),
                    '7' => utf8_decode(html_entity_decode('(+)39-06-983-54343'))
                ),
                '1' => array (
                    '0' => utf8_decode(html_entity_decode('Brasil')),
                    '1' => utf8_decode(html_entity_decode('0800-666-1599')),
                    '2' => utf8_decode(html_entity_decode('Per&uacute;')),
                    '3' => utf8_decode(html_entity_decode('001866-600-8936')),
                    '4' => utf8_decode(html_entity_decode('Argentina')),
                    '5' => utf8_decode(html_entity_decode('(+)49-69-2222-7544')),
                    '6' => utf8_decode(html_entity_decode('Japón')),
                    '7' => utf8_decode(html_entity_decode('(+)39-06-983-54343'))
                ),
                '2' => array (
                    '0' => utf8_decode(html_entity_decode('Chile')),
                    '1' => utf8_decode(html_entity_decode('1230-020-2396')),
                    '2' => utf8_decode(html_entity_decode('R. Dominicana')),
                    '3' => utf8_decode(html_entity_decode('1888-751-2280')),
                    '4' => utf8_decode(html_entity_decode('Australia')),
                    '5' => utf8_decode(html_entity_decode('(+)61-29-119-2941')),
                    '6' => utf8_decode(html_entity_decode('M&eacute;xico')),
                    '7' => utf8_decode(html_entity_decode('(+)52-55-132-81928'))
                ),
                '3' => array (
                    '0' => utf8_decode(html_entity_decode('Colombia')),
                    '1' => utf8_decode(html_entity_decode('01800-915-5913')),
                    '2' => utf8_decode(html_entity_decode('USA-Canada')),
                    '3' => utf8_decode(html_entity_decode('1800-504-0027 ')),
                    '4' => utf8_decode(html_entity_decode('Brasil')),
                    '5' => utf8_decode(html_entity_decode('(+)55-21-352-19188')),
                    '6' => utf8_decode(html_entity_decode('Per&uacute;')),
                    '7' => utf8_decode(html_entity_decode('(+)51-1-718-5156'))
                ),
                '4' => array (
                    '0' => utf8_decode(html_entity_decode('España')),
                    '1' => utf8_decode(html_entity_decode('900-941-988')),
                    '2' => utf8_decode(html_entity_decode('Venezuela')),
                    '3' => utf8_decode(html_entity_decode('0800-100-2453')),
                    '4' => utf8_decode(html_entity_decode('Chile')),
                    '5' => utf8_decode(html_entity_decode('(+)59-2-2760-9099')),
                    '6' => utf8_decode(html_entity_decode('Puerto Rico')),
                    '7' => utf8_decode(html_entity_decode('(+)1787-304-3043'))
                ),
                '5' => array (
                    
                    '4' => utf8_decode(html_entity_decode('China')),
                    '5' => utf8_decode(html_entity_decode('(+)86-10-840-53610')),
                    '6' => utf8_decode(html_entity_decode('Reino Unido')),
                    '7' => utf8_decode(html_entity_decode('(+)44-20-302-62071'))
                ),
                '6' => array (
                    
                    '4' => utf8_decode(html_entity_decode('España')),
                    '5' => utf8_decode(html_entity_decode('(+)34-91414-2281')),
                    '6' => utf8_decode(html_entity_decode('Suiza')),
                    '7' => utf8_decode(html_entity_decode('(+)41-22-533-0959'))
                ),
                '7' => array (
                    
                    '4' => utf8_decode(html_entity_decode('Ecuador')),
                    '5' => utf8_decode(html_entity_decode('(+)593-2-3814-790')),
                    '6' => utf8_decode(html_entity_decode('USA')),
                    '7' => utf8_decode(html_entity_decode('(+)1-305-644-8435'))
                ),
                '8' => array (
                    
                    '4' => utf8_decode(html_entity_decode('Francia')),
                    '5' => utf8_decode(html_entity_decode('(+)33-1-706-13000')),
                    '6' => utf8_decode(html_entity_decode('Holanda')),
                    '7' => utf8_decode(html_entity_decode('(+)31-20-708-4091'))
                ),
	),
        
        'other' => array(
                '6' => utf8_decode(html_entity_decode('Skype: asistenciasplanet-assist1 (unicamente para mensajes escritos) ')),
                '7' => utf8_decode(html_entity_decode('Skype: asistencias-planet (unicamente para llamadas)')),	
                '8' => utf8_decode(html_entity_decode('Correo: asistencias@planet-assist.net')),
        ),
);
$textES['signature'] = utf8_decode(html_entity_decode('b'));