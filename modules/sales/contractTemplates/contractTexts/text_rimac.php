<?php

/*****************************************  SPANISH TEXTS *********************************************/
$textES = array();
$textES['title'] = '<b><i>Salud Viajeros</i></b>';

$textES['certificate'] = array(
	'title' => 'Certificado',
	'policy' => 'P&oacute;liza No:',
	'code' => 'C&oacute;digo SBS:',
	'product' => 'Producto',
	'certificate' => 'Certificado / Voucher No.',
	'begins' => 'Inicia desde las 12 Hrs de',
	'ends' => 'Hasta las 12 Hrs de'
);

$textES['customer'] = array(
	'title' => 'Informaci&oacute;n Contratante',
	'customer' => 'Contratante:',
	'address' => 'Direcci&oacute;n Contratante:',
	'ruc' => 'RUC Contratante:'
);

$textES['insured'] = array(
	'title' => 'Informaci&oacute;n del Asegurado',
	'names' => 'Nombres y Apellidos:',
	'document' => 'Documento de Identidad:',
	'sex' => 'Sexo:',
	'relationship' => 'Relaci&oacute;n con el contratante:',
	'beneficiaries' => 'Beneficiarios:',
	'beneficiaries_text' => 'En caso de fallecimiento del Asegurado, los beneficiarios ser&aacute;n los herederos legales',
	'birthdate' => 'Fecha de Nacimiento:',
	'phone' => 'Tel&eacute;fono:',
	'citizenship' => 'Pa&iacute;s de Residencia:',
);

$textES['limits'] = array(
	'title' => 'L&iacute;mite de Ingreso y Permanencia',
	'min_age' => 'Edad m&iacute;nima de ingreso a la p&oacute;liza: 18 a&ntilde;os',
	'max_age' => 'Edad L&iacute;mite de ingreso a la p&oacute;liza: 75 a&ntilde;os',
	'max_perm' => 'Edad L&iacute;mite de permanencia en la p&oacute;liza: 75 a&ntilde;os'
);

$textES['product'] = array(
	'title' => 'Informaci&oacute;n del Producto',
	'services' => 'SERVICIOS',
	'assistance' => 'ASISTENCIA EN VIAJES INTERNACIONALES',
	'max_amount' => 'MONTO M&Aacute;XIMO EN D&Oacute;LARES'
);

$textES['benefits'] = array(
	'0' => array("text" => html_entity_decode("ASISTENCIA M&Eacute;DICA POR ACCIDENTE"), "limit"=>"US&#36; 40.000"),
	'1' => array("text" => html_entity_decode("ASISTENCIA M&Eacute;DICA POR ENFERMEDAD"), "limit"=>"US&#36; 20.000"),
	'2' => array("text" => html_entity_decode("MEDICAMENTOS RECETADOS"), "limit"=>"US&#36; 20.000"),
	'3' => array("text" => html_entity_decode("ODONTOLOG&Iacute;A DE URGENCIA"), "limit"=>"US&#36; 300"),
	'4' => array("text" => html_entity_decode("TRASLADO SANITARIO Y REPATRIACI&Oacute;N SANITARIA"), "limit"=>"US&#36; 20.000"),
	'5' => array("text" => html_entity_decode("TRASLADO DE FAMILIA POR HOSPITALIZACI&Oacute;N"), "limit"=>"SI"),
	'6' => array("text" => html_entity_decode("HOTEL POR CONVALECENCIA"), "limit"=>"US&#36; 800"),
	'7' => array("text" => html_entity_decode("ACOMPA&Ntilde;AMIENTO DE MENORES"), "limit"=>"SI"),
	'8' => array("text" => html_entity_decode("REPATRIACI&Oacute;N SANITARIA"), "limit"=>"US&#36; 20.000"),
	'9' => array("text" => html_entity_decode("REGRESO POR FALLECIMIENTO DEL FAMILIAR"), "limit"=>"SI"),
	'10' => array("text" => html_entity_decode("INTERRUPCI&Oacute;N DE VIAJE POR ACCIDENTE, MUERTE O ENFERMEDAD"), "limit"=>"NO"),
	'11' => array("text" => html_entity_decode("GASTO POR VUELO DEMORADO O CANCELADO"), "limit"=>"NO"),
	'12' => array("text" => html_entity_decode("ANULACI&Oacute;N DE VIAJE"), "limit"=>"US&#36; 700"),
	'13' => array("text" => html_entity_decode("SUBSTITUCIÓN DE EJECUTIVO"), "limit"=>"NO"),
	'14' => array("text" => html_entity_decode("TRANSMISI&Oacute;N DE MENSAJES URGENTES"), "limit"=>"SI"),
	'15' => array("text" => html_entity_decode("ASISTENCIA EN CASO DE P&Eacute;RDIDA O ROBO DE DOCUMENTOS"), "limit"=>"SI"),
	'16' => array("text" => html_entity_decode("L&Iacute;NEA DE CONSULTAS &#40;24 HORAS&#41;"), "limit"=>"SI"),
	'17' => array("text" => html_entity_decode("INDEMNIZACI&Oacute;N POR P&Eacute;RDIDA DEL EQUIPAJE"), "limit"=>"US&#36; 1.200"),
	'18' => array("text" => html_entity_decode("COMPENSACI&Oacute;N POR DEMORA DEL EQUIPAJE"), "limit"=>"NO"),
	'19' => array("text" => html_entity_decode("TRANSFERENCIA DE FONDOS"), "limit"=>"US&#36; 4.000"),
	'20' => array("text" => html_entity_decode("TRANSFERENCIA DE FONDOS PARA FIANZA LEGAL"), "limit"=>"US&#36; 15.000"),
	'21' => array("text" => html_entity_decode("ASISTENCIA LEGAL POR ACCIDENTE DE TR&Aacute;NSITO"), "limit"=>"US&#36; 2.000"),
	'22' => array("text" => html_entity_decode("ASISTENCIA M&Eacute;DICA EN CASO DE ENFERMEDAD PRE &#45; EXISTENTE O CR&Oacute;NICA"), "limit"=>"US&#36; 500")
);




$textES['important'] = array(
	'title' => 'Informaci&oacute;n Importante',
	'text' => 	"Para cualquier consulta o reclamo sobre el presente seguro, o ante la ocurrencia de un siniestro, el Asegurado podr&aacute; comunicarse a los tel&eacute;fonos: Alemania. " . 
				"0800-185-9976 Argentina. 0800-666-29-84 Brasil. 0800-891-4530 China Sur. 10800-130-1130 China Norte. 10800-713-1166 Colombia. 257-1922 / 257-1945 Costa Rica. " .
				"0800-013-1097 Espa&ntilde;a. 911-881-617 / 911-815-905 USA. 1-877-822-7386 Reino Unido. 0808-234-1766 Francia. 800-905-030 Italia. 800-839-070 M&eacute;xico. 001866-261- " .
				"1935 Rep&uacute;blica Dominicana. 1888-751-8475 Venezuela. 0800-100-9032 Otras pa&iacute;ses llamadas por  " .
				"cobrar a Estados Unidos. 1-954-472-1895."
);

$textES['general'] = array(
	'title' => 'Informaci&oacute;n General',
	'text' => html_entity_decode("El presente certificado se emite como constancia de que usted se encuentra asegurado en Rimac Seguros, hasta el l&iacute;mite del beneficio m&aacute;ximo, bajo los t&eacute;rminos y condiciones " .
				"generales, particulares, especiales, anexos y endosos que en forma conjunta e indivisible constituyen la p&oacute;liza de seguro de Salud Viajero. Las comunicaciones cursadas por el Asegurado al " .
				"Contratante, por aspectos relacionados con el contrato se seguros, tienen el mismo efecto que si se hubieren dirigido a la Compa&ntilde;&iacute;a. Asimismo, los pagos realizados por el Asegurado al Contratante" .
				" se consideran abonados a la Compa&ntilde;&iacute;a. La Compa&ntilde;&iacute;a declara expresamente que ha aceptado la solicitud de seguro presentada por el Asegurado. La firma del presente documento significa que la Compa&ntilde;&iacute;a" .
				" por medio del Contratante ha hecho entrega del presente certificado dentro de los plazos establecidos por la normativa vigente. El Asegurado tendr&aacute; derecho a solicitar copia de la p&oacute;liza a la Compa&ntilde;&iacute;a" .
				" y de recibirla dentro de los 15 d&iacute;as calendario desde la fecha de recepci&oacute;n de la solicitud presentada por el Asegurado. Al recibir nuestro voucher/certificado usted acepta t&aacute;ticamente las condiciones" .
				" generales que rigen nuestros productos y se acoge en su totalidad a ellas. "),
	'bottom' => 'El Asegurado firma el presente certificado que consta de dos (3) p&aacute;ginas, en se&ntilde;al de haber tomado conocimiento de las condiciones aqu&iacute; detalladas.',
);

$textES['acceptance'] = array(
	'title' => 'Aceptaci&oacute;n de las Condiciones Planteadas',
	'law' => 	'De Acuerdo al art&iacute;culo 341 de la Ley 26702 agradecemos devolver a la Compa&ntilde;&iacute;a una copia del presente certificado debidamente firmado por el Asegurado.',
	'company' => 'Empresa de Seguros: Rimac Internacional Compa&ntilde;&iacute;a de Seguros y Reaseguros',
	'ruc' => 'RUC: 20100041953 Domicilio: Av. Begonias 475 San Isidro Tel&eacute;fonos: 411-3000 Fax: 421-0555',
	'name_left' => 'Marcelo Escobar Garc&iacute;a',
	'position_left' => 'Gerente de Divisi&oacute;n',
	'unit_left' => 'Unidad de Negocios',
	'name_right' => 'Firma Asegurado',
);

$textES['conditions'] = array(
	'title' => 'Exclusiones',
	'text' => html_entity_decode('a) Enfermedades cr&oacute;nicas o preexistentes, cong&eacute;nitas y recurrentes,  padecidas con anterioridad al inicio de la vigencia del Plan y/o del viaje, conocidas o no por el ASEGURADO, as&iacute; como sus agudizaciones y consecuencias, incluso cuando las mismas aparezcan durante el viaje'.
								'b) Enfermedades, lesiones, afecciones o complicaciones resultantes de tratamientos m&eacute;dicos y farmac&eacute;uticos efectuados por personas o profesionales no autorizados por el Departamento M&eacute;dico de la Central de Asistencia'.
								'c) Tratamientos homeop&aacute;ticos, acupuntura, quinesioterapia, curas termales, podolog&iacute;a, quiropr&aacute;cticos, tratamiento por medio de medicina no convencional, tratamientos experimentales o investigativos '.
								'd) Enfermedades o lesiones resultantes de intento de suicidio o causadas intencionalmente por el ASEGURADO a si mismo.'.
								'e) Afecciones, enfermedades o lesiones derivadas de accidentes de trabajo, intento o acci&oacute;n criminal o penal del ASEGURADO, directa o indirectamente.'.
								'f) Afecciones o lesiones consecuentes a la exposici&oacute;n al sol.'.
								'g) Tratamiento de enfermedades o estados patol&oacute;gicos producidos por ingesti&oacute;n o administraci&oacute;n intencional de t&oacute;xicos (drogas), narc&oacute;ticos, o por la utilizaci&oacute;n de medicamentos, sin orden m&eacute;dica.'.
								'h) Gastos incurridos en cualquier tipo de pr&oacute;tesis, incluidas las dentales, lentes, aud&iacute;fonos, anteojos, ortesis, s&iacute;ntesis o ayudas mec&aacute;nicas o artificiales o el alquiler y/o adquisici&oacute;n de elementos afines a las mismas.'.
								'i) Eventos ocurridos como consecuencia de entrenamiento, pr&aacute;ctica o participaci&oacute;n activa en competencias deportivas (profesionales o amateurs). Adem&aacute;s quedan expresamente excluidas las ocurrencias consecuentes a la pr&aacute;ctica de deportes peligrosos, incluyendo pero no limitado a: motociclismo, automovilismo, boxeo, polo, ski acu&aacute;tico, buceo, aladeltismo, alpinismo, ski y otros deportes invernales fuera de pistas reglamentarias y autorizadas. '.
								'j) Cobertura de maternidad '.
								'k) Todo tipo de enfermedad mental.'.
								'l) Afecciones, enfermedades o lesiones derivadas de la ingesti&oacute;n de bebidas alcoh&oacute;licas de cualquier tipo.'.
								'm) Controles de tensi&oacute;n arterial o enfermedades relacionadas con la hipertensi&oacute;n o hipotensi&oacute;n arterial, sincopes o secuelas glandulares y sus consecuencias, salvo en cuanto a atenci&oacute;n inicial de urgencia con compromiso vital hasta la estabilizaci&oacute;n de los signos vitales del Beneficiario'.
								'n) Tratamientos cosm&eacute;ticos, de cirug&iacute;a est&eacute;tica y reconstrucci&oacute;n.'.
								'o) S&iacute;ndrome de inmune deficiencia adquirida, SIDA y HIV en todas sus formas, secuelas y consecuencias. Enfermedades ven&eacute;reas y/o en general todo tipo de prestaci&oacute;n, examen y/o tratamiento que no haya recibido la autorizaci&oacute;n previa de la Central de Asistencia. En caso de constatarse que el motivo del viaje fuera el tratamiento en el extranjero de una enfermedad de base, y que el tratamiento actual tiene alguna vinculaci&oacute;n directa o indirecta con la dolencia previa motivo del viaje, la COMPA&Ntilde;IA quedar&aacute; relevada de otorgar cobertura. A tal fin la COMPA&Ntilde;IA se reserva el derecho de investigar la conexi&oacute;n del hecho actual con la dolencia previa. '.
								'p) Eventos, consecuencia de desencadenamiento de fuerzas naturales, radiaci&oacute;n nuclear y radioactividad, as&iacute; como cualquier otro fen&oacute;meno con car&aacute;cter extraordinario o evento que debido a sus proporciones o gravedad, sea considerado como desastre nacional regional o local o cat&aacute;strofe, as&iacute; como eventos ocasionados por la manipulaci&oacute;n o uso de qu&iacute;micos o productos nocivos para la salud as&iacute; como tambi&eacute;n efectos producidos por exposici&oacute;n a altos niveles de radiaci&oacute;n. '.
								'q) El suicidio, intento de suicidio o las lesiones infringidas a s&iacute; mismo por parte del ASEGURADO, as&iacute; como cualquier acto de manifiesta irresponsabilidad o imprudencia grave por parte del ASEGURADO. '.
								'r) Eventos como consecuencia de actos de guerra, invasi&oacute;n, actos cometidos por enemigos extranjeros, hostilidades u operaciones de guerra (sea que haya sido declarada o no la guerra) guerra civil, rebeli&oacute;n, insurrecci&oacute;n o poder militar, naval o usurpado, la intervenci&oacute;n del ASEGURADO en motines, tumultos que tengan o no car&aacute;cter de guerra civil, o sea que la intervenci&oacute;n sea personal o como miembro de una organizaci&oacute;n civil o militar; terrorismo u otra alteraci&oacute;n grave del orden p&uacute;blico.'.
								's) Los actos intencionados y de mala fe del ASEGURADO.'.
								't) Alistarse en el servicio militar con cualquier fuerza armada de pa&iacute;s u organismo internacional.'.
								'u) Uso de aeronaves, a menos que sea como pasajero que paga boleto en un avi&oacute;n de l&iacute;neas a&eacute;reas regulares o compa&ntilde;&iacute;a de vuelos fletados, con la debida autorizaci&oacute;n para el transporte frecuente de pasajeros que pagan boleto y que tengan una vinculaci&oacute;n directa en la ocurrencia de la muerte accidental del ASEGURADO.'.
								'v) Actos il&iacute;citos del ASEGURADO o de los albaceas o administradores, herederos legales o representantes personales del ASEGURADO.'.
								'w) Que el ASEGURADO tenga un nivel de alcohol en la sangre, m&aacute;s alto que el permitido por la ley. '.
								'x) Intoxicaci&oacute;n, infecci&oacute;n bacteriana o virulenta, aunque se haya contra&iacute;do accidentalmente. Esto no incluye la infecci&oacute;n bacteriana, que sea el resultado directo de una cortadura o herida accidental o intoxicaci&oacute;n alimentar&iacute;a accidental.'.
								'y) De accidentalidad sufrida dentro de Transportes privados, autos de alquiler sin chofer, o, inclusive como peat&oacute;n.'.
								'z) Accidentes dentro del pa&iacute;s en que es emitida la P&oacute;liza.')
);

$textES['documentation'] = array(
	'title' => 'Documentaci&oacute;n para Presentar en Caso de un Siniestro',
	'text' => 	'Para cualquier consulta o reclamo sobre el presente seguro, o ante la ocurrencia de un siniestro, el Asegurado podr&aacute; comunicarse a los tel&eacute;fonos: Alemania. 0800-185-9976 Argentina. 0800-666-29-84 Brasil. 0800-891-4530 China Sur. 10800-130-1130 China Norte. 10800-713-1166 Colombia. 257-1922 / 257-1945 Costa Rica. 0800-013-1097 Espa&ntilde;a. 911-881-617 / 911-815-905 USA. 1-877-822.7386 Reino Unido. 0808-234-1766 Francia. 800-905-300 Italia. 800-839-070 M&eacute;xico. 001866-261-1935 Rep&uacute;blica Dominicana. 1888-751-8475 Venezuela. 0800-9032 Otros pa&iacute;ses llamada por cobrar a Estados Unidos. 1-954-472-1895',
	'body' => 	html_entity_decode('1. En caso de GASTOS M&Eacute;DICOS Y HOSPITALARIOS, GASTOS ODONTOL&Oacute;GICOS Y EVACUACI&Oacute;N M&Eacute;DICA: En el caso de reembolso de estos gastos, el ASEGURADO tendr&aacute; que presentar los siguientes documentos:'.
									'a) Diagn&oacute;stico m&eacute;dico, '.
									'b) Carta con breve relato de lo sucedido con los datos de la P&oacute;liza, '.
									'c) Comprobante de pago originales. '.
									'2. En caso de RETRASO DEL VIAJE: '.
									'a) Copia del pasaje a&eacute;reo y tarjeta de embarque; '.
									'b) Comprobantes originales de los gastos con  alimentaci&oacute;n y alojamiento; '.
									'c) Declaraci&oacute;n de la compa&ntilde;&iacute;a a&eacute;rea del atraso. '.
									'3. En caso de MEDICAMENTOS: '.
									'a) Receta m&eacute;dica; '.
									'b) Boleta original de la compra del medicamento en el extranjero. El ASEGURADO o su beneficiario tendr&aacute;n que presentar todos los documentos descritos con anterioridad en un plazo m&aacute;ximo de 30 (treinta) d&iacute;as despu&eacute;s de ocurrido el accidente, dejando a salvo, el derecho del ASEGURADO de acudir a la v&iacute;a judicial. '.
									'4. En caso de P&Eacute;RDIDA DEL EQUIPAJE: '.
									'a) Informe comprobante de la p&eacute;rdida emitida por la Empresa A&eacute;rea responsable (PIR-Property Irregularity  Report), que se&ntilde;ale el peso, en kilogramos del Equipaje extraviado. Ticket original del Equipaje; '.
									'b) recibo de indemnizaci&oacute;n emitido por el TRANSPORTE P&Uacute;BLICO AUTORIZADO. '.
									'5. En caso de SEGURO POR MUERTE ACCIDENTAL: Los beneficiarios del asegurados (Herederos legales), producido un accidente indemnizable en virtud de esta P&oacute;liza de Seguro, est&aacute;n en la obligaci&oacute;n de comunicarlo a la COMPA&Ntilde;IA, mediante correo certificado, dentro de los TREINTA (30) d&iacute;as h&aacute;biles de ocurrido el siniestro en el extranjero. La COMPA&Ntilde;IA dar&aacute; por recibido el aviso respectivo a&uacute;n con posteridad a los plazos indicados anteriormente, cuando se pruebe fehacientemente la imposibilidad de haber efectuado dicho aviso dentro de tales plazos. Asimismo deber&aacute;n presentarse a la COMPA&Ntilde;&iacute;A, a trav&eacute;s de su plataforma de servicio al cliente, ubicada en la Agencias San Borja (Av. De Las Artes Norte 389),  los siguientes documentos en original o copia legalizada: '.
									'a) Acta o Partida de Defunci&oacute;n del ASEGURADO, '.
									'b) Certificado de Defunci&oacute;n del ASEGURADO, '.
									'c) Atestado policial, '.
									'd) Informe sobre las circunstancias del accidente emitido por el empleador (de ocurrido el accidente en horas de trabajo), '.
									'e) Protocolo de Necropsia, '.
									'f) An&aacute;lisis toxicol&oacute;gico con resultados de alcoholemia y toxinas, '.
									'g) Dosaje et&iacute;lico (en caso de tratarse de accidente de tr&aacute;nsito), '.
									'h) Documento de identidad del ASEGURADO fallecido y beneficiarios (Partida de Nacimiento en caso de ser menores de edad), '.
									'i) Certificado domiciliario de los beneficiarios menores de edad, '.
									'j) Cualquier otro documento que la COMPA&Ntilde;&iacute;A estime conveniente, En caso de muerte presunta, &eacute;sta deber&aacute; acreditarse conforme a la Ley vigente. La COMPA&Ntilde;&Iacute;A se reserva el derecho de requerir al o los Beneficiario(s) cualquier documento o informaci&oacute;n adicional en original o copia legalizada, en caso las circunstancias espec&iacute;ficas as&iacute; lo ameriten. Una vez que el o los beneficiario(s) haya(n) cumplido con presentar todos los documentos referidos en este art&iacute;culo, la COMPA&Ntilde;&iacute;A, de encontrarlos conformes, completos y sin observaci&oacute;n, tendr&aacute; un plazo de 30 d&iacute;as calendarios para proceder al pago de la(s) suma(s) asegurada(s) descrita(s) en las Condiciones Particulares del presente Seguro. '.
									'La falta de cumplimiento de alguna o de todas las obligaciones y formalidades se&ntilde;aladas en el presente art&iacute;culo, salvo que se demostrare la imposibilidad de cumplir, hace perder todo derecho a la indemnizaci&oacute;n, pues queda entendido y convenido que su estricto y total cumplimiento es esencial a los efectos de este Seguro, no pudiendo en caso alguno los beneficiarios, alegar como excusa del incumplimiento, la ignorancia, olvido o error de la existencia de la P&oacute;liza de Seguro o del desconocimiento de las obligaciones que imponen sus condiciones. '.
									'6. En caso de INVALIDEZ PERMANENTE TOTAL Y PARCIAL POR ACCIDENTE: El CONTRANTANTE y/o ASEGURADO, deber&aacute;n presentar a la COMPA&Ntilde;IA los siguientes documentos: '.
									'a) Copia legalizada del Documento de Identidad del ASEGURADO; b)Original del Dictamen de Invalidez de la Comisi&oacute;n Medica Evaluadora de Incapacidades del MINSA O ESSALUD; '.
									'c) Informe sobre las circunstancias del accidente emitido por el empleador (de ser el accidente en horas de trabajo); '.
									'd) Copia certificada del Atestado Policial o Informe sobre las circunstancias del accidente (proporcionado por el empleador en caso sea en horas de trabajo); o Declaraci&oacute;n Jurada del Accidente (en caso sea un Accidente Com&uacute;n); e) Original o copia legalizada del Dosaje Et&iacute;lico en caso la Invalidez sea consecuencia de un  accidente de tr&aacute;nsito;  f) Informe M&eacute;dico completo e Historia Cl&iacute;nica del ASEGURADO (foliada y fedateada).')
);

$textES['suggestions'] = array(
	'title' => 'Consultas, Sugerencias y Reclamos (Lima y Provincia)',
	'text' => 	html_entity_decode('Para cualquier consulta y/o reclamo el asegurado deber&aacute; comunicarse a los n&uacute;meros detallados en el certificado de seguro o al 411-3000. Tambi&eacute;n nos podr&aacute; ubicar en nuestra Oficina Principal en la Av. Las Begonias 475 piso 2, San Isidro, Lima o en nuestra Oficina de Seguros Personales  en la Av. Comandante Espinar 689, Miraflores, Lima. '.
				'La COMPA&Ntilde;&Iacute;A atender&aacute; las quejas y reclamos en un plazo m&aacute;ximo de treinta (30) d&iacute;as calendario de presentado por parte del CONTRATANTE/ASEGURADO. ')
);

$textES['defense'] = array(
	'title' => 'Defensor&iacute;a del Asegurado',
	'address' => html_entity_decode('Direcci&oacute;n: Arias Araguez 146, San Antonio Miraflores, Lima-Per&uacute;'),
	'fax' => 'Telefax: 446-9158',
	'email' => 'E-mail: info@defaseg.com.pe',
	'web' => 'www.defaseg.com.pe'
);


/*****************************************  ENGLISH TEXTS *********************************************/
$textEN = array();


$contractTexts = array('es' => $textES, 'en' => $textEN);
$contractTexts = $contractTexts[$lang_string];
?>