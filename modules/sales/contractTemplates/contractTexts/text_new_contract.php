<?php

/*****************************************  SPANISH TEXTS *********************************************/
$textES = array();
$textES['title'] = array(
	'header' => 'Planet Assist',
	'main' => 'Contrato de Servicios'
);
$textES['article_0'] = array(
	'title' => null,
	'content' => html_entity_decode('<p align="justify">A los %con_days_beg% d&iacute;as del mes de %con_month_beg% de %con_year_beg%, comparecen a la celebraci&oacute;n del presente contrato de Prestaci&oacute;n de Servicios de Salud y medicina prepagada, por una parte, '
            . 'la compa&ntilde;&iacute;a %legal_name_com%, legalmente representada por su Gerente General Juan Carlos Prieto, parte a la cual de ahora en adelante y para los efectos jur&iacute;dicos '
            . 'del presente contrato se le denominar&aacute; %legal_name_com%, de conformidad con el nombramiento que se anexa como documento habilitante; y, por otra, el se&ntilde;or(a) %customer_name% '
            . 'parte a la cual de ahora en adelante se denominara %contracting_party% quienes libre y voluntariamente en las calidades que representan, convienen en celebrar el presente '
            . 'contrato  al tenor de las siguientes cl&aacute;usulas</p><br>')
);
$textES['article_1'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA PRIMERA. ANTECEDENTES</b><br>'),
	'content' => html_entity_decode('<ol type="1" align="justify">'
            . '<li>%legal_name_com% es una persona jur&iacute;dica legalmente constituida bajo las leyes de la Rep&uacute;blica del Ecuador, aprobada mediante Resoluci&oacute;n No. 03-P-DIC de la Intendencia de Compa&ntilde;&iacute;as de Portoviejo e inscrita el diecisiete (17) de marzo del  dos mil tres (2003), en el Registro Mercantil del cant&oacute;n Portoviejo, bajo el n&uacute;mero cincuenta y uno (51) y anotado en repertorio general tomo n&uacute;mero veinte y uno (21); </li>'
            . '<li>Mediante escritura p&uacute;blica celebrada el siete (07) de septiembre del dos mil seis (2006), se aument&oacute; el Capital, se fij&oacute; capital autorizado, se reform&oacute; los Estatutos Sociales y el Objeto Social de la Compa&ntilde;&iacute;a, aprobado mediante resoluci&oacute;n No. 06.Q.IJ.005077 de la Intendencia de Compa&ntilde;&iacute;as de Quito e inscrita veinte y seis (26) de diciembre del dos mil seis (2006) en el Registro Mercantil del cant&oacute;n Quito, estableciendo como objeto principal de la compa&ntilde;&iacute;a el otorgar a sus afiliados el servicio de Medicina Pre-pagada, a trav&eacute;s de un sistema de servicios de prestaciones y beneficios de salud, con m&eacute;dicos y en centros de atenci&oacute;n  m&eacute;dica y laboratorios habilitados para ello, por el pago de cuotas convenidas con dichos afiliados. </li>'
            . '<li>%contracting_party%, con pleno conocimiento del servicio que otorga %legal_name_com%, solicita su afiliaci&oacute;n con el prop&oacute;sito de recibir los beneficios de este sistema, los mismos que se extender&aacute;n a sus empleados y/o accionistas. </li>'
            . '<li>%contracting_party% de manera expresa reconoce y acepta que por la naturaleza del presente Contrato de medicina prepagada, las aportaciones que se cancelen o cancelaren durante la vigencia del Contrato o sus renovaciones a favor de %legal_name_com%, corresponden a la cobertura de servicios de salud, y no a la calidad sobre la prestaci&oacute;n misma de servicios m&eacute;dicos relacionados a eventos que quebranten su salud o la de sus beneficiarios.</li>'
            . '<ol></p><br>')
);

$textES['article_2'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA SEGUNDA.- DEFINICIONES.</b><br>'),
	'content' => html_entity_decode('Para la interpretaci&oacute;n y los efectos del presente contrato se considerar&aacute; lo establecido en la ley que regula el funcionamiento de las Empresas Privadas de Salud y Medicina Prepagada, Ley Org&aacute;nica de la Salud, Ley Org&aacute;nica de Defensa al Consumidor y de forma supletoria a lo dispuesto en el C&oacute;digo Civil, adicionalmente se tomar&aacute; en cuenta las siguientes reglas: </p>'
            . '<ol type="a" align="justify">'
            . ' <li>Que los t&iacute;tulos de las cl&aacute;usulas se han puesto simplemente para facilidad de lectura y referencia, mas no como medio de interpretaci&oacute;n;</li>'
            . ' <li>Que la referencia al singular de un t&eacute;rmino incluye el plural de los mismos, si fuere aplicables;</li>'
            . ' <li>Que las palabras utilizadas en la redacci&oacute;n del presente contrato ser&aacute;n interpretadas de conformidad con su tenor literal, salvo aquellas cuyas definiciones se encuentran establecidas dentro de la presente cl&aacute;usula.</li>'
            . '</ol><br>'
            . '<p align="justify"><b>DEFINICI&Oacute;N DE T&Eacute;RMINOS</b><br><br>'
            . '<b>ACCIDENTE:</b> D&iacute;cese del accidente, al hecho: externo, s&uacute;bito, imprevisto, violento, fortuito y ajeno a la voluntad de una persona, donde la causa sea externa al cuerpo propio de la v&iacute;ctima y que produzca una lesi&oacute;n corporal durante la vigencia del Contrato. Todo accidente debe ser notificado a %legal_name_com% dentro de un plazo m&aacute;ximo de 24 horas h&aacute;biles. Este evento para ser cubierto requiere la presentaci&oacute;n de un examen de alcoholemia en todos los casos, realizado al momento del ingreso a la casa de salud.<br>'
            . '<b>ACOMPA&Ntilde;ANTE:</b> Significa el pariente cercano, o cualquier otra persona adulta, que acompa&ntilde;a al ASEGURADO durante su per&iacute;odo de hospitalizaci&oacute;n en una Cl&iacute;nica, Hospital o Centro de Trasplantes.<br>'
            . '<b>AFILIADO (BENEFICIARIO):</b> Toda persona que tenga derecho a las prestaciones y beneficios m&eacute;dicos determinados en el plan contratado.<br>'
            . '<b>ANEXO:</b> Documentos en los que se definen t&eacute;rminos y condiciones particulares del plan contratado, as&iacute; como cualquier condici&oacute;n adicional que deba formar parte integral del Contrato.<br>'
            . '<b>APOPLEG&Iacute;A/DERRAME:</b> Es un da&ntilde;o cerebral provocado por la interrupci&oacute;n del suministro de sangre al cerebro. Sus causas son: infarto de tejido cerebral y hemorragia, embolia cerebral, trombosis cerebral.'
            . '<b>ARANCEL:</b> Es un listado de todas las prestaciones m&eacute;dicas, codificadas y valoradas que se utiliza para calcular el monto m&aacute;ximo a restituir en un reembolso.  El arancel utilizado para la liquidaci&oacute;n de los reembolsos del plan contratado consta en el Anexo B-Condiciones Particulares del Plan. <br>'
            . '<b>ATENCIONES AMBULATORIAS:</b> Son aquellas prestaciones de salud que, de conformidad con la pr&aacute;ctica y las normas m&eacute;dicas, no requieren internamiento en casa de salud.<br>'
            . '<b>ATENCIONES HOSPITALARIAS:</b> Son aquellas prestaciones de salud que, de conformidad con la pr&aacute;ctica m&eacute;dica requieren el internamiento en casa de salud independientemente del tiempo.<br>'
            . '<b>BENEFICIARIOS:</b> Incluye a todas las personas que tienen derecho a la cobertura del plan contratado de acuerdo al Anexo B-Condiciones Particulares del Plan.<br>'
            . '<b>BENEFICIO MAXIMO VITALICIO:</b> Es el l&iacute;mite de por vida indicado en la tabla de beneficios. La suma de todos los pagos y reembolsos efectuados bajo este contrato para todas las patolog&iacute;as cubiertas, no pueden exceder este l&iacute;mite.<br>'
            . '<b>C&Aacute;NCER:</b> Significa una enfermedad caracterizada por la presencia de uno o varios tumores malignos cuyas c&eacute;lulas exhiben propiedades de invasi&oacute;n y met&aacute;stasis, la cual ha sido diagnosticada como c&aacute;ncer por un m&eacute;dico. El t&eacute;rmino “C&aacute;ncer” incluye Leucemia.<br>'
            . '<b>CENTRO DE TRASPLANTE:</b> Significa una instituci&oacute;n que:
               <ol type="1" align="justify">
                <li>Est&aacute; licenciada y funciona como un Centro de Trasplantes de conformidad con las leyes de la jurisdicci&oacute;n donde tiene si domicilio.</li>
                <li>Est&aacute; especialmente equipada para efectuar Trasplantes de &Oacute;rganos y Trasplantes de Tejidos, y es reconocida y aceptada como un centro especializado para efectuar dicho trasplante.</li>
                <li>Se ocupa en primer t&eacute;rmino, y por remuneraci&oacute;n de proporcionar a los pacientes ingresados en la instituci&oacute;n, y bajo la direcci&oacute;n de un grupo de M&eacute;dicos, tratamiento y cuidado a enfermos, incluyendo procedimientos especiales requeridos por concepto de Trasplante de &Oacute;rganos y Trasplante de Tejidos, y cuidados en unidades de cuidado intensivo.</li>
                <li>Proporciona en forma continua servicios de enfermer&iacute;a 24 horas al d&iacute;a, prestados por enfermeros graduados registrados.</li>
                <li>Posee los equipos e instalaciones necesarios para efectuar intervenciones de cirug&iacute;a mayor, incluyendo Trasplantes de &Oacute;rganos y de Tejidos.</li>
               </ol><br>
              El t&eacute;rmino Centro de Trasplantes no incluye una instituci&oacute;n que es usada principalmente como un centro para descanso; cuidados de custodia; cuidados de enfermer&iacute;a; cuidados de ancianos; cuidados de convalecencia; o tratamientos de alcoholismo, adici&oacute;n a las drogas, condiciones nerviosas o mentales.<br>'
            . '<b>CENTRAL DE ASISTENCIAS:</b> Es la central de comunicaciones y servicios a trav&eacute;s del cual se canaliza la coordinaci&oacute;n de los servicios de asistencias, la misma que opera las 24 horas todos los d&iacute;as del a&ntilde;o.<br>'
            . '<b>CONTRATANTE (CLIENTE):</b> Es toda persona natural que celebra un Contrato de Prestaci&oacute;n de Servicios de ASISTENCIA MEDICA EMERGENTE INTERNACIONAL MASIVO INTERNACIONALES  para  beneficio  suyo, pudi&eacute;ndolo hacer extensivo a sus beneficiarios.<br>'
            . '<b>COPAGO:</b> Es el porcentaje de los gastos m&eacute;dicos que no ser&aacute;n cubiertos por %legal_name_com%, es decir que deben ser cubiertos por el afiliado de acuerdo a las condiciones del plan contratado.<br>'
            . '<b>CUARTO Y ALIMENTO DIARIO:</b> Es la cobertura que %legal_name_com% otorga por los gastos de habitaci&oacute;n y alimentaci&oacute;n del afiliado durante una hospitalizaci&oacute;n de acuerdo al Anexo B-Descripci&oacute;n del Plan Contratado.<br>'
            . '<b>CL&Iacute;NICA U HOSPITAL:</b> Instituci&oacute;n legalmente autorizada para funcionar como centro m&eacute;dico, cl&iacute;nico y/o quir&uacute;rgico, donde puedan realizarse tratamientos o procedimientos m&eacute;dicos frente a enfermedades, dolencias agudas o emergencias y cuya actividad principal no sea la de hidrocl&iacute;nica, sanatorio, instituci&oacute;n de rehabilitaci&oacute;n, centro de convalecientes o asilo de ancianos.<br>'
            . '<b>CUADRO CERRADO:</b>  Es una modalidad en la prestaci&oacute;n del servicio mediante la cual el afiliado o beneficiario opta por la atenci&oacute;n o servicio por parte de profesionales de la salud o instituciones de salud adscritas a %legal_name_com% <br>'
            . '<b>DECLARACI&Oacute;N JURAMENTADA DE SALUD:</b> Documento en el cual se declaran los antecedentes m&eacute;dicos y el estado actual de salud de los beneficiarios de un Contrato de Prestaci&oacute;n de Servicios de Salud y Medicina Prepagada y que forma parte integrante de &eacute;l.<br>'
            . '<b>DEDUCIBLE:</b> Valor de Gastos M&eacute;dicos Cubiertos asumidos por el Afiliado de acuerdo a lo estipulado en el anexo.<br>'
            . '<b>DEDUCIBLE POR INCAPACIDAD:</b> Valor de Gastos M&eacute;dicos Cubiertos asumido por el Afiliado, por cada Incapacidad.  Este deducible puede aplicarse tanto para incapacidades ambulatorias como hospitalarias, y se reestablece una vez que termin&oacute; el periodo de incapacidad.<br>'
            . '<b>DEPENDIENTE CONTRACTUAL:</b> Se entiende como dependiente contractual a: la o el c&oacute;nyuge, conviviente legal, hijos, padres u otros familiares, que tienen el car&aacute;cter de cargas legales, que fueron declarados como beneficiarios dentro de la solicitud de la prestaci&oacute;n del presente servicio.<br>'
            . '<b>DIAGN&Oacute;STICO (ENFERMEDAD):</b> Es toda alteraci&oacute;n, desorden funcional o estructural, accidental o no, que padezca un beneficiario y requiera de tratamiento autorizado y conducido por un m&eacute;dico.  Estar&aacute;n cubiertos los diagn&oacute;sticos cuyas primeras manifestaciones se presenten durante la vigencia del Contrato de Prestaci&oacute;n de Servicios de ASISTENCIA MEDICA EMERGENTE INTERNACIONAL.<br>'
            . '<b>DONANTE:</b> Significa una persona viva o fallecida a la cual se le han extra&iacute;do uno, o m&aacute;s de uno, de los &Oacute;rganos o Tejidos de su cuerpo con la finalidad de insertarlos (en total o en parte) al cuerpo de otra persona, mediante un procedimiento quir&uacute;rgico considerado M&eacute;dicamente Necesario.<br>'
            . '<b>EMERGENCIA:</b> Es la aparici&oacute;n s&uacute;bita o inesperada de una condici&oacute;n que amenaza la vida y que requiere de atenci&oacute;n inmediata sea m&eacute;dica o quir&uacute;rgica,  del beneficiario.<br>'
            . '<b>EMERGENCIA VITAL:</b> Es toda condici&oacute;n que amenaza en forma inmediata la vida del afiliado y no est&aacute; relacionada con una preexistencia.  %legal_name_com% cubrir&aacute; las siguientes, dentro del per&iacute;odo de carencia definido en el Anexo B-Condiciones Particulares del Plan: apendicitis aguda, intoxicaci&oacute;n alimentaria, retenci&oacute;n urinaria aguda y oclusi&oacute;n intestinal.<br>'
            . '<b>EMERGENCIA POR ACCIDENTE:</b> Son los servicios proporcionados al afiliado por el departamento de Emergencia de una cl&iacute;nica u hospital, como consecuencia de un accidente o lesi&oacute;n corporal que no requiera hospitalizaci&oacute;n.  Este ser&aacute; restituido de acuerdo al Anexo B- Condiciones Particulares del Plan.<br>'
            . '<b>EMPLEADOS:</b> Toda persona que trabaja bajo relaci&oacute;n de dependencia o que prestan servicios profesionales al contratante.<br>'
            . '<b>ENFERMEDAD CR&Oacute;NICA:</b> Es toda patolog&iacute;a, sus secuelas y/o complicaciones que afecta en forma permanente la salud del afiliado.<br>'
            . '<b>ENFERMEDAD CONG&Eacute;NITA:</b> Es toda patolog&iacute;a, sus secuelas y/o complicaciones, iniciada durante la vida intrauterina, que se haya manifestado en el nacimiento o con posterioridad a &eacute;l.<br>'
            . '<b>ENFERMEDAD INFECCIOSA:</b> Proceso patol&oacute;gico com&uacute;n transmisible, causado por virus, bacteria, hongos o par&aacute;sitos.<br>'
            . '<b>EPICRISIS:</b> Documento m&eacute;dico preparado el momento del Alta M&eacute;dica del paciente, el cual contiene un resumen del estado actual, diagn&oacute;sticos m&eacute;dicos finales, ex&aacute;menes realizados, tratamiento y plan posterior al alta.<br>'
            . '<b>ENFERMEDADES HEREDITARIAS:</b> son un conjunto de enfermedades gen&eacute;ticas caracterizadas por transmitirse de generaci&oacute;n en generaci&oacute;n, es decir de padres a hijos, en la descendencia y que se pueden manifestar en alg&uacute;n momento de sus vidas.<br>'
            . '<b>ENFERMEDAD PREEXISTENTE:</b> Es aquella enfermedad, lesi&oacute;n o desorden, sus causas, secuelas, complicaciones y/o efectos tard&iacute;os, conocidos por el afiliado y/o diagnosticadas o no m&eacute;dicamente con anterioridad a la fecha de suscripci&oacute;n del Contrato de Prestaci&oacute;n de Servicios de ASISTENCIA MEDICA EMERGENTE MASIVO INTERNACIONALES , declaradas o no en la Declaraci&oacute;n Juramentada de Salud.  Contralor&iacute;a M&eacute;dica de %legal_name_com% podr&aacute; definir la exclusi&oacute;n  de la enfermedad.<br>'
            . '<b>EXCLUSIONES (PRESTACIONES NO CUBIERTAS):</b> Son aquellas prestaciones que %legal_name_com%, no reconoce al beneficiario en funci&oacute;n del Contrato o de las preexistencias declaradas o no del beneficiario.<br>'
            . '<b>EXPERIMENTAL:</b> Significa que un tratamiento, cirug&iacute;a, procedimiento, suministro, tecnolog&iacute;a o medicamento relacionado con las patolog&iacute;as cubiertas, no ha sido ampliamente aceptado como seguro, efectivo y apropiado para efectuar un Tratamiento de cualquiera de ellas, por el consenso de las organizaciones profesionales que est&aacute;n reconocidas por la comunidad m&eacute;dica internacional, y se encuentra bajo estudio, investigaci&oacute;n, per&iacute;odo de prueba, o en cualquier fase de un experimento cl&iacute;nico.<br>'
            . '<b>FECHA DE VIGENCIA:</b> Significa la fecha en la que entra en vigor la Cobertura y seg&uacute;n est&aacute; indicado en las Condiciones Particulares.  Para cada ASEGURADO es la fecha en la que ha sido aceptado como asegurado por %legal_name_com% e incluido como tal en la presente P&oacute;liza.<br>'
            . '<b>FECHA DE INCURRENCIA:</b> Lapso durante el cual el beneficiario ha recibido prestaciones m&eacute;dicas.<br>'
            . '<b>INCAPACIDAD:</b> Es todo diagn&oacute;stico (enfermedad o accidente) por el cual el afiliado tiene la cobertura de %legal_name_com%<br>'
            . '<b>INFARTO:</b> Se produce por una obstrucci&oacute;n en una de las arterias coronarias, frecuentemente por ruptura de una placa de ateroma vulnerable. Es la muerte de una parte del m&uacute;sculo coronario. El infarto de Miocardio es la causa principal de muerte, tanto en hombre como en mujeres, en todo el mundo.<br>'
            . '<b>INSUFICIENCIA RENAL CR&Oacute;NICA:</b> Es la obstrucci&oacute;n irreversible de ambos ri&ntilde;ones resultando en la necesidad de di&aacute;lisis renal permanente o trasplante de ri&ntilde;ones.<br>'
            . '<b>LIBRE ELECCI&Oacute;N:</b> Es una modalidad de prestaci&oacute;n de servicios por medio de la cual el usuario puede elegir la cl&iacute;nica, hospital o profesional m&eacute;dico que le preste el servicio, fuera de los afiliados a %legal_name_com% <br>'
            . '<b>L&Iacute;MITE DE COPARTICIPACI&Oacute;N:</b> Es el monto hasta el cual se aplica el porcentaje de cobertura con copago, y a partir de este monto se aplica el 100% de cobertura.<br>'
            . '<b>M&Aacute;XIMO POR INCAPACIDAD:</b> Monto m&aacute;ximo a restituirse por cada enfermedad o accidente.<br>'
            . '<b>M&Eacute;DICAMENTE NECESARIO O NECESIDAD M&Eacute;DICA:</b> Significa una operaci&oacute;n, procedimiento o tratamiento de cualquiera de los m&oacute;dulos cubiertos:
                <ul style="list-style-type:circle">
                    <li>Es apropiado y esencial.</li>
                    <li>No excede en alcance, duraci&oacute;n o intensidad el nivel de cuidado necesario para proporcionar un tratamiento seguro, adecuado y apropiado.</li>
                    <li>Ha sido prescrito por un M&eacute;dico Especialista en la patolog&iacute;a cubierta.</li>
                    <li>Es consistente con las normas profesionales ampliamente aceptadas en la pr&aacute;ctica de la medicina en la Rep&uacute;blica del Ecuador y en los Estados Unidos de Am&eacute;rica; y</li>
                    <li>En el caso de un paciente internado en un Hospital, no puede ser administrado fuera del Hospital, sin riesgo para el paciente.</li>
                </ul><br>
                La Necesidad M&eacute;dica ser&aacute; determinada bas&aacute;ndose en la definici&oacute;n anterior.  El hecho de que un tratamiento, operaci&oacute;n, servicio o suministro haya sido prescrito, recomendado, aprobado o suministrado por un M&eacute;dico no es necesariamente suficiente para considerarlo M&eacute;dicamente Necesario.<br>'
            . '<b>M&Eacute;DICO:</b> Toda persona autorizada para el ejercicio profesional de la medicina, conforme a las Leyes de la Rep&uacute;blica del Ecuador.<br>'
            . '<b>MEDICINAS:</b> Medicamentos prescritos por un m&eacute;dico, para el tratamiento y restablecimiento de la salud del afiliado, siempre y cuando se encuentren en el Vademecum Farmac&eacute;utico, tengan registro sanitario, hayan sido adquiridos en una farmacia o laboratorio autorizado y &eacute;ste haya emitido la factura respectiva con todos los requisitos del Servicio de Rentas Internas SRI.<br>'
            . '<b>M&Eacute;DULA &Oacute;SEA:</b> Significa el tejido que se encuentra en las cavidades de los huesos, presentado fibras reticulares y c&eacute;lulas.<br>'
            . '<b>&Oacute;RGANO:</b> Una parte diferenciada y vital del cuerpo humano formada por diferentes tejidos, que mantiene su estructura, vascularizaci&oacute;n y capacidad para desarrollar funciones fisiol&oacute;gicas con un nivel de autonom&iacute;a importante.<br>'
            . '<b>PA&Iacute;S DE RESIDENCIA:</b> Significa Rep&uacute;blica del Ecuador.<br>'
            . '<b>PER&Iacute;ODO DE CARENCIA:</b> Es el per&iacute;odo posterior al inicio de vigencia del Contrato, durante el cual los afiliados no tienen derecho a cobertura por parte de %legal_name_com% sobre los servicios m&eacute;dicos incurridos, de acuerdo al Anexo B-Condiciones Particulares del Plan Contratado.<br>'
            . '<b>PER&Iacute;ODO DE INCAPACIDAD:</b> Tiempo m&aacute;ximo durante el cual, por una misma incapacidad, el usuario puede incurrir en gastos m&eacute;dicos y solicitud reembolso a %legal_name_com%, definido en el Anexo B-Descripci&oacute;n del Plan.<br>'
            . '<b>PER&Iacute;ODO DE PRESENTACI&Oacute;N DE RECLAMOS:</b> Tiempo m&aacute;ximo hasta el cual cada factura por gastos m&eacute;dicos amparados incurridos puede ser presentada a %legal_name_com% para su restituci&oacute;n, definido en el Anexo B-Condiciones Particulares del Plan Contratado.<br>'
            . '<b>PLAN:</b> Conjunto de servicios, prestaciones, beneficios, limitaciones y condiciones a las que se someten los afiliados del Contrato de Prestaciones de Servicios de Salud y Medicina Prepagada.<br>'
            . '<b>PORCENTAJE DE COBERTURA:</b> Es el porcentaje que se utilizar&aacute; para calcular el monto a restituir por la prestaci&oacute;n m&eacute;dica, una vez aplicado el valor de franquicia.  Este porcentaje est&aacute; definido en cada plan de salud en el Anexo B-Descripci&oacute;n del Plan.<br>'
            . '<b>PRESTACIONES M&Eacute;DICAS:</b> Son los servicios m&eacute;dicamente necesarios, solicitados, evaluados y/o conducidos por un m&eacute;dico para atender los requerimientos de salud de los afiliados de un Contrato. Las prestaciones m&eacute;dicas se reembolsan de acuerdo a los planes contratados, y considerando las gastos razonables y acostumbrados de acuerdo a la pr&aacute;ctica m&eacute;dica.<br>'
            . '<b>PRESTADORES DE SERVICIOS M&Eacute;DICOS:</b> Son todas las personas naturales o jur&iacute;dicas que cuentan con la autorizaci&oacute;n legal para el ejercicio profesional de la medicina o para funcionar como casas de salud, laboratorios, centros de imagen o similares.<br>'
            . '<b>QUIMIOTERAPIA:</b> Significa el uso de agentes qu&iacute;micos prescritos por un m&eacute;dico para el tratamiento y control del c&aacute;ncer.<br>'
            . '<b>RADIOTERAPIA:</b> Significa el uso de Rayos X o de otras sustancias radioactivas recetadas por un M&eacute;dico para el tratamiento y control del c&aacute;ncer.<br>'
            . '<b>RED DE PROVEEDORES DE SERVICIOS M&Eacute;DICOS:</b> Significa el grupo de Hospitales, M&eacute;dicos y otros proveedores de servicios m&eacute;dicos incluidos en la lista que forma parte de las Condiciones Particulares de la P&oacute;liza y que han sido autorizados con el objeto de proporcionar a los ASEGURADOS, los servicios m&eacute;dicos cubiertos en cualquier parte del Mundo, y que cuentan con la conformidad de %legal_name_com% <br>'
            . '<b>SEGUNDA OPONION M&Eacute;DICA:</b> Este servicio permite a %legal_name_com% y a los profesionales m&eacute;dicos tratantes de los beneficiarios, realizar intercambio de opiniones con profesionales de destacadas Instituciones para confirmar y evaluar su diagn&oacute;stico o tratamiento en los casos que considere conveniente, debido a la complejidad que el caso presente. Permite tambi&eacute;n coordinar las intervenciones de alta complejidad m&eacute;dica, derivadas de estas consultas.<br>'
            . '<b>SIDA:</b> Es la sigla del s&iacute;ndrome de inmunodeficiencia adquirida. Es el estado m&aacute;s avanzado de infecci&oacute;n por el virus de inmunodeficiencia humana (VIH). El VIH es un virus que mata o da&ntilde;a las c&eacute;lulas del sistema inmunol&oacute;gico del organismo.<br>'
            . '<b>SISTEMA DE MEDICINA PREPAGADA:</b> Es un sistema organizado en el que un afiliado paga por adelantado las aportaciones del plan contratado, para tener acceso a solicitar se le restituyan los gastos en los que hubiere incurrido a consecuencia de una enfermedad, de acuerdo a los t&eacute;rminos del Contrato y los t&eacute;rminos de la Ley de las Empresas Privadas de Salud y Medicina Prepagada.<br>'
            . '<b>TEJIDO:</b> Toda parte constituyente del cuerpo humano formada por c&eacute;lulas unidas por alg&uacute;n tipo de tejido conectivo.<br>'
            . '<b>TOPE M&Aacute;XIMO DE COBERTURA:</b> Es el monto m&aacute;ximo de reembolso que se reconocer&aacute; por cada incapacidad seg&uacute;n el plan contratado.<br>'
            . '<b>TRASPLANTE DE &Oacute;RGANOS:</b> Es la sustituci&oacute;n con fines terap&eacute;uticos de componentes anat&oacute;micos en una persona, por otros iguales y funcionales provenientes del mismo receptor o de un donante vivo o muerto.<br>'
            . '<b>UNIDAD DE CUIDADOS INTENSIVOS:</b> Gastos m&eacute;dicos incurridos por el afiliado por los cargos necesarios, razonables y acostumbrados, suministrados por la Cl&iacute;nica u Hospital por concepto de cama, equipos, instalaciones, alimentaci&oacute;n y dem&aacute;s servicios de hospital durante su permanencia en el &aacute;rea de cuidados intensivos, por cada d&iacute;a de internaci&oacute;n, por persona, por incapacidad, hasta el l&iacute;mite de d&iacute;as y valor estipulados en el anexo.'
            . '</p><br>')
);

$textES['article_3'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA TERCERA. OBJETO</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">%legal_name_com% a trav&eacute;s del presente contrato tiene por objeto establecer las condiciones para la prestaci&oacute;n de servicios de medicina prepagada con cobertura espec&iacute;fica para la asistencia m&eacute;dica emergente Internacional , de forma directa o indirecta, a trav&eacute;s de las diferentes modalidades de financiaci&oacute;n.</p>'
            . '<p align="justify">El plan regulado en el presente contrato es mixto, al dar la posibilidad al BENEFICIARIO/AFILIADO a la atenci&oacute;n  por parte de m&eacute;dicos, cl&iacute;nicas u hospitales de su preferencia o a trav&eacute;s de las cl&iacute;nicas, hospitales o m&eacute;dicos adscritos a nuestro sistema, de red de proveedores a nivel internacional y nacional.</p><br>')
);

$textES['article_4'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA CUARTA. CONDICIONES DEL SERVICIO.</b><br>'),
	'content' => html_entity_decode('El Titular y/o Cliente aceptan la prestaci&oacute;n materia del presente contrato, dentro de las condiciones establecidas a continuaci&oacute;n:'
            . '<ol type="1" align="justify">'
            . ' <li>Los beneficios de este Contrato, una vez suscrito por las dos partes, empezar&aacute;n a partir de la fecha de inicio de la vigencia asignada por %legal_name_com%, la cual consta al inicio del presente Contrato, y una vez cancelada la primera cuota o la totalidad de la prima establecida y de acuerdo a los tiempos de carencias determinados en el anexo B Condiciones Particulares.</li>'
            . ' <li>Los t&eacute;rminos y condiciones de este Contrato est&aacute;n expresados en este texto, as&iacute; como en los anexos que forman parte integrante del mismo.</li>'
            . ' <li>%legal_name_com% se compromete a reembolsar en favor de %contracting_party% y/o Titular los gastos por las prestaciones m&eacute;dicas amparadas en este Contrato, bajo las condiciones del plan contratado, y de acuerdo a los procedimientos y requisitos determinados y que est&aacute;n en conocimiento de %contracting_party%. Los montos m&aacute;ximos a restituir por cada prestaci&oacute;n son los razonables y acostumbrados de acuerdo al arancel del plan contratado indicado en el Anexo B-Condiciones Particulares del Plan, que forma parte integrante del presente contrato una vez suscrito por las partes.</li>'
            . ' <li>%contracting_party% se compromete y obliga a pagar en favor de %legal_name_com%, de forma mensual, trimestral, semestral o anual, seg&uacute;n consta en el Anexo B-Condiciones Particulares del Plan, y  anticipadamente al per&iacute;odo de cobertura, las  aportaciones convenidas.  Si el pago ocurre posteriormente, se considerar&aacute; que la cuota est&aacute; vencida y en estado de morosidad, por lo tanto todos los gastos incurridos durante ese per&iacute;odo carecer&aacute;n de cobertura.</li>'
            . ' <li>La no utilizaci&oacute;n de los servicios por parte de %contracting_party% y/o  beneficiarios, no exime al Contratante del pago de los valores acordados.</li>'
            . ' <li>La cobertura de todos los Afiliados amparados bajo este Contrato tendr&aacute;n de manera independiente su fecha de inicio y de t&eacute;rmino, independientemente de la fecha en la cual  integren a &eacute;l.</li>'
            . ' <li>La atenci&oacute;n m&eacute;dica, cl&iacute;nica, farmac&eacute;utica, quir&uacute;rgica o cualquier otra amparada por el uso y aplicaci&oacute;n del presente Contrato , es de exclusiva responsabilidad de la persona y/o Instituci&oacute;n que presta la misma. En consecuencia, %legal_name_com% no asume responsabilidad alguna derivada de la calidad y prestaci&oacute;n de los servicios m&eacute;dicos otorgados a nuestros Afiliados.</li>'
            . ' <li>Los enunciados de Cuadro Cerrado y Libre Elecci&oacute;n son simplemente esquemas econ&oacute;micos de mejor restituci&oacute;n o pago sobre los gastos incurridos sin construir restricci&oacute;n alguna y menos obligatoriedad de uso por parte de los Afiliados de este Contrato.</li>'
            . ' <li>Frente a una urgencia o emergencia m&eacute;dica del usuario o sus dependientes contractuales, estos podr&aacute;n elegir el centro de atenci&oacute;n m&eacute;dica al cual acudir. M&aacute;s, si no est&aacute; en condiciones de ser trasladado a una unidad de salud, podr&aacute; a su arbitrio solicitar en el lugar en donde se encuentre, la presencia de un servicio de atenci&oacute;n de urgencias m&eacute;dicas ambulatorias.</li>'
            . ' <li>En ambos casos, la empresa de salud y medicina prepagada privada, deber&aacute; reembolsar los valores respectivos, de acuerdo al plan de salud contratado.</li>'
            . ' <li>En caso de fallecimiento del titular del contrato, sus dependientes contractuales quedar&aacute;n afiliados por un a&ntilde;o a partir de la fecha de deceso, sin tener que pagar cuota alguna.</li>'
            . ' <li>Si un Afiliado,  por su decisi&oacute;n, cambiar durante la vigencia del Contrato a un  plan superior al contratado, las incapacidades en curso que est&aacute;n teniendo la cobertura de %commercial_name% mantendr&aacute;n la cobertura del plan anterior y deber&aacute; llenar una nueva declaraci&oacute;n juramentada de salud.</li>'
            . '</ol></p><br>')
);

$textES['article_5'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA QUINTA. PRESTACIONES Y BENEFICIOS M&Eacute;DICOS CUBIERTOS</b><br>'),
	'content' => html_entity_decode('El Titular y/o Cliente aceptan la prestaci&oacute;n materia del presente contrato, dentro de las condiciones establecidas a continuaci&oacute;n:</p>'
            . '<p align="justify"><ol type="a" align="justify">'
            . ' <li>Atenci&oacute;n m&eacute;dica y profesional en las diversas especialidades, terapia intensiva, insumos m&eacute;dicos y quir&uacute;rgicos, y, medicamentos.</li>'
            . ' <li>Atenci&oacute;n ambulatoria en todas las especialidades y sub especialidades practicadas en las diferentes unidades de salud, en los consultorios y en los domicilios cuando el delicado estado de salud e imposibilidad de movilizar al paciente as&iacute; lo justifique; La atenci&oacute;n ambulatoria est&aacute; definida por todas aquellas prestaciones de salud que, de acuerdo a la pr&aacute;ctica m&eacute;dica com&uacute;n, no requieren de hospitalizaci&oacute;n para ser atendidas;</li>'
            . ' <li>Visita m&eacute;dica hospitalaria, cuando el beneficiario se encuentra internado en una Unidad de salud.</li>'
            . ' <li>Procedimiento de emergencia ambulatorio o medico quir&uacute;rgicas. La emergencia calificada por un m&eacute;dico, debe ser atendida como una situaci&oacute;n grave que se presenta s&uacute;bitamente y amenaza la vida o la salud de una persona o de un grupo de individuos, como las cat&aacute;strofes naturales o una enfermedad aguda.</li>'
            . ' <li>Ambulancia terrestre, a&eacute;rea, y fluvial, debida y legalmente autorizada como tal, dentro del territorio nacional e internacional, excepto en los casos expresados en el respectivo Reglamento;</li>'
            . ' <li>Todas las interconsultas profesionales, ex&aacute;menes auxiliares y de diagnostico, existentes a la fecha de promulgaci&oacute;n de la Ley o que pudieran crearse, incorporarse o estar disponibles la fecha de requerimiento de servicio.</li>'
            . ' <li>Atenci&oacute;n hospitalaria, a criterio del m&eacute;dico o profesional que atiende al beneficiario. En este derecho se incluye transporte, alimentaci&oacute;n, habitaci&oacute;n, terapia intensiva, insumos m&eacute;dicos y quir&uacute;rgicos, medicamentos, honorarios profesionales y todos los ex&aacute;menes, procedimientos auxiliares de diagnostico y terap&eacute;uticos y terap&eacute;uticos mencionados en el art&iacute;culo anterior o a criterio del m&eacute;dico o profesional bajo cuya responsabilidad est&aacute; %contracting_party%, siempre que las condiciones del plan lo establezcan y no lo limiten.</li>'
            . ' <li>Todos los procedimientos diagn&oacute;sticos y terap&eacute;uticos, curaciones y atenci&oacute;n ambulatoria en atenci&oacute;n al tratamiento de continuaci&oacute;n posterior de cada enfermedad o accidente atendido.</li>'
            . ' <li>Todos los procedimientos, diagn&oacute;sticos y terap&eacute;uticos, curaciones, atenci&oacute;n ambulatoria y rehabilitaci&oacute;n para dar cobertura a los contratantes dentro de las especialidades y subespecialidades medicas y profesionales actuales o que pudieran crearse.</li>'
            . ' <li>Abortos no provocados, embarazos normales o complicados, controles prenatales, partos vaginales o por ces&aacute;rea de productos a termino o pre termino, complicaciones que se presentaren antes, durante y despu&eacute;s del parto y alumbramiento, atenci&oacute;n de reci&eacute;n nacido a termino o prematuro, atenci&oacute;n perinatal, atenci&oacute;n en la unidad de cuidados intensivos, tanto para la madre como para el producto a termino o pre termino, para lo cual se emplearan todos los procedimiento m&eacute;dicos necesarios, a fin de preservar la salud de la unidad madre-ni&ntilde;o. Se atender&aacute; de igual manera al reci&eacute;n nacido que padeciera de estigmas o patolog&iacute;as cong&eacute;nitas, gen&eacute;ticas o hereditarias.</li>'
            . ' <li>Reconocimiento de pagos totales o parciales, seg&uacute;n el plan contratado, por la compra de medicamentos en cualquier farmacia calificada, siempre que aquellos hayan sido prescritos por un m&eacute;dico. Ser&aacute; obligaci&oacute;n de la empresa cubrir todos los gastos que demande el transplante de &oacute;rganos necesarios para la supervivencia del beneficiario sus dependientes, en cuyos casos, la empresa tambi&eacute;n cubrir&aacute; los gastos en que para tal fin incurriera el donante, de acuerdo al plan contratado.</li>'
            . ' <li>Cobertura total de estudios anatomopatol&oacute;gicos cuando estos sean solicitados por un m&eacute;dico o profesional para configurar apropiadamente el diagnostico o evoluci&oacute;n de la patolog&iacute;a de un paciente.</li>'
            . ' <li>Cobertura dental total o parcial de conformidad con el plan contratado, y,</li>'
            . ' <li>Cobertura total de todas las enfermedades cong&eacute;nitas y hereditarias.</li>'
            . '</ol></p>'
            . '<p align="justify">%legal_name_com% asume la responsabilidad del pago de la prestaci&oacute;n de los servicios m&eacute;dicos necesarios para restablecer la salud del asegurado en cualquier enfermedad grave determinada y aceptada por el departamento m&eacute;dico de %commercial_name%, la misma que conlleve a una intervenci&oacute;n quir&uacute;rgica. Las prestaciones y beneficios amparados por %legal_name_com% son las m&eacute;dicamente necesarias, en eventos hospitalarios de acuerdo a las condiciones, t&eacute;rminos, l&iacute;mites y carencias del Plan contratado, mismas que se encuentran especificadas en el Anexo B y en la Tabla de beneficios.</p>'
            . '<p align="justify">Las prestaciones m&eacute;dicas cubiertas de acuerdo al plan contratado ser&aacute;n liquidadas considerando:  1) El presente Contrato, 2) El Anexo A-Descripci&oacute;n del Plan Contratado, 3) Anexo B-Condiciones Particulares del Plan y 4) El monto de acuerdo al Plan Contratado, el mismo que consta en el Anexo B-Condiciones Particulares del Plan:'
            . '<ol type="a" align="justify">'
            . ' <li>Atenci&oacute;n M&eacute;dica y profesional en las diversas especialidades, terapia intensiva, insumos m&eacute;dicos quir&uacute;rgicos y medicamentos.</li>'
            . ' <li>Visita m&eacute;dica hospitalaria, cuando el beneficiario se encuentra internado en una unidad de salud;</li>'
            . ' <li>Ex&aacute;menes auxiliares y de diagn&oacute;stico: siempre que est&eacute;n relacionados con el  diagn&oacute;stico y amparados en la respectiva solicitud del m&eacute;dico tratante.</li>'
            . ' <li>Radiograf&iacute;as simples, Endoscop&iacute;a Digestiva y Endoscop&iacute;a Respiratoria, relacionadas a una enfermedad cubierta por la p&oacute;liza.</li>'
            . ' <li>Ex&aacute;menes de Laboratorio Cl&iacute;nico razonables y acostumbrados relacionados con una enfermedad cubierta por la p&oacute;liza.</li>'
            . ' <li>Atenci&oacute;n hospitalaria que incluye lo siguiente:'
            . '     <ul style="list-style-type:circle">'
            . '         <li>Cirug&iacute;as programadas en las especialidades m&eacute;dicas cubiertas en la p&oacute;liza;</li>'
            . '         <li>Uso de quir&oacute;fano</li>'
            . '         <li>Uso de salas de cuidado intensivo Habitaci&oacute;n y Alimentaci&oacute;n en cl&iacute;nicas y hospitales</li>'
            . '         <li>Suministros Quir&uacute;rgicos, Derechos de sala, Anestesia. En concepto de Honorarios del Anestesista se reconocer&aacute; el 35% del valor establecido como Honorario del Cirujano. Medicamentos empleados o consumidos por el Usuario durante su estad&iacute;a en el establecimiento hospitalario.</li>'
            . '         <li>Procedimiento Auxiliar de T&eacute;cnicas Quir&uacute;rgicas</li>'
            . '         <li>Medicamentos;</li>'
            . '         <li>Ex&aacute;menes, los establecidos dentro de la presente cl&aacute;usula</li>'
            . '         <li>Honorarios profesionales</li>'
            . '     </ul><br>'
            . '     Se debe considerar que todas estas coberturas tienen como l&iacute;mites los establecidos en el Anexo B Condiciones Particulares que forma parte integral del contrato, como as&iacute; tambi&eacute;n de acuerdo al Plan Contratado.'
            . ' </li>'
            . ' <li>Trasplante de &oacute;rganos necesarios para la supervivencia del  beneficiario o sus dependientes, en cuyos casos tambi&eacute;n cubrir&aacute; los gastos en que para tal fin incurriere el donante, de acuerdo al plan contratado.</li>'
            . '</ol>'
            . 'Todas las coberturas tendr&aacute;n como l&iacute;mite el Plan Contratado y lo establecido en el Anexo B de las Condiciones Particulares que forma parte integral de este contrato.'
            . '</p><br>')
);

$textES['article_6'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA SEXTA. PRESTACIONES Y BENEFICIOS M&Eacute;DICOS NO CUBIERTOS</b><br>'),
	'content' => html_entity_decode('Los servicios m&eacute;dicos complementarios no dar&aacute;n lugar a cobertura o reembolso por parte de %legal_name_com%, as&iacute; como tampoco liquidar&aacute; los costos y gastos de tales servicios cuando ellos sean o est&eacute;n originados por las siguientes causas:</p><br>'
            . '<p align="justify"><ol type="1" align="justify">'
            . ' <li>Lesiones o afecciones causadas directa o indirectamente por acci&oacute;n de la naturaleza, erupci&oacute;n, energ&iacute;a at&oacute;mica, guerra declarada o no, conmoci&oacute;n civil, revoluci&oacute;n  y participaci&oacute;n en huelgas y motines; as&iacute; como toda enfermedad que hubiere sido declarada como epidemia por el Ministerio de Salud P&uacute;blica de la Rep&uacute;blica del Ecuador y toda enfermedad end&eacute;mica.</li>'
            . ' <li>Ex&aacute;menes no inherentes a, o no necesarios para, el diagn&oacute;stico de una enfermedad, as&iacute; como chequeos m&eacute;dicos, ex&aacute;menes y perfiles generales de salud.</li>'
            . ' <li>Transportes que no sean prestados por servicio de ambulancia terrestre debida y legalmente autorizados como tales.</li>'
            . ' <li>Consultas y tratamientos para curaciones psiqui&aacute;tricas o psicol&oacute;gicas; cuidado o per&iacute;odos de cuarentena o aislamiento.</li>'
            . ' <li>Cualquiera que fuera su uso en la atenci&oacute;n ambulatoria de: vitaminas, minerales, al&eacute;rgenos, tranquilizantes, sedantes,  neurol&eacute;pticos, estimulantes del apetito, anor&eacute;xicos, anticonceptivos, preservativos, jabones, filtros solares, shampoos, alimentos para beb&eacute;s, medicamentos homeop&aacute;ticos, medicamentos bioenerg&eacute;ticos, medicamentos sin registro sanitario y otros similares.</li>'
            . ' <li>Cualquiera que fuera su uso en la atenci&oacute;n hospitalaria de: tranquilizantes mayores, neurol&eacute;pticos, estimulantes del apetito, anor&eacute;xicos, anticonceptivos, jabones, filtros solares, shampoos, alimentos para beb&eacute;s, medicamentos homeop&aacute;ticos, medicamentos bioenerg&eacute;ticos, medicamentos sin registro sanitario y otros similares.</li>'
            . ' <li>Vacunas, pruebas de sensibilidad y tratamientos inmunol&oacute;gicos en general por condiciones preexistentes, excepto la vacuna de la rabia y antitet&aacute;nica aplicadas por mordeduras de animales o por accidentes reportados a %legal_name_com% en forma inmediata (en las siguientes 72 horas h&aacute;biles).</li>'
            . ' <li>Consulta y tratamiento dermatol&oacute;gicos relacionados con aspectos cosmetol&oacute;gicos.</li>'
            . ' <li>Consulta y tratamiento de enfermedades mentales, nerviosas y/o estr&eacute;s.  Terapia ambiental de descanso y/o para observaci&oacute;n; servicios o tratamientos en instituciones asistenciales, hidrocl&iacute;nicas, ba&ntilde;os termales, sanatorios, cl&iacute;nicas de reposo o cl&iacute;nicas de ancianos que no sean hospitales.</li>'
            . ' <li>Consultas y tratamientos de o por infertilidad en general, procedimientos para esterilizaci&oacute;n masculina o femenina, otros procedimientos anticonceptivos y sus consecuencias, al igual que prestaciones realizadas en centros especializados de infertilidad.  Abortos y sus consecuencias que se hayan realizado por razones psicol&oacute;gicas o sociales. Cualquier cr&iacute;o-preservaci&oacute;n y la implantaci&oacute;n o re-implantaci&oacute;n de c&eacute;lulas vivas.</li>'
            . ' <li>Enfermedades, accidentes y toda prestaci&oacute;n m&eacute;dica requerida o causada por haber estado bajo el efecto de estupefacientes, substancias psicotr&oacute;picas, alcohol y/o drogas siempre y cuando hayan sido la causa de los hechos  y calificados como faltas a la Ley, o sufridos como producto de la participaci&oacute;n en actos calificados como delitos. Da&ntilde;os causados a s&iacute; mismo estando o no en uso de sus facultades mentales.</li>'
            . ' <li>Consulta y tratamiento de rehabilitaci&oacute;n por alcoholismo y/o drogadicci&oacute;n.</li>'
            . ' <li>Ex&aacute;menes rutinarios de la vista y de los o&iacute;dos, as&iacute; como los marcos, cristales &oacute;pticos, lentes de contacto y aud&iacute;fonos.</li>'
            . ' <li>Suministros de muletas, aparatos ortop&eacute;dicos, bast&oacute;n, andador y &oacute;rtesis, plantillas, suspensorios, cabestrillo, zapatos ortop&eacute;dicos; como tambi&eacute;n de equipos m&eacute;dicos duraderos como sillas de ruedas, camas de hospitales, equipos requeridos para terapia f&iacute;sica; as&iacute; tambi&eacute;n, costos de adaptaci&oacute;n de veh&iacute;culos, cuartos de ba&ntilde;o y residencias.  Se excluye cualquier equipo m&eacute;dico duradero.</li>'
            . ' <li>Pr&oacute;tesis, &oacute;rtesis y/o s&iacute;ntesis y/o dispositivos de correcci&oacute;n.  En el Anexo B correspondiente, se establecen beneficios adicionales para pr&oacute;tesis no dentales.</li>'
            . ' <li>Cirug&iacute;a pl&aacute;stica, salvo para corregir lesiones por accidentes que deben ser reportados a %legal_name_com% y siempre que del accidente resulten otras incapacidades que sean cubiertas por %legal_name_com%</li>'
            . ' <li>Cirug&iacute;a correctiva y/o l&aacute;ser y terapia de defectos visuales, tratamientos de miop&iacute;a, astigmatismo, hipermetrop&iacute;a.  Terapia l&aacute;ser en problemas dermatol&oacute;gicos.</li>'
            . ' <li>Gastos adicionales de acompa&ntilde;antes en cl&iacute;nicas y hospitales y atenci&oacute;n particular de enfermer&iacute;a.</li>'
            . ' <li>Tratamiento facturado por parte de un miembro de la familia, y cualquier autoterapia, incluyendo autoprescripci&oacute;n de medicamentos, sus secuelas y complicaciones derivadas del mismo.</li>'
            . ' <li>Gastos m&eacute;dicos causados por Accidentes de trabajo en afiliados miembros de las Fuerzas Armadas, Polic&iacute;as, Guardias de Seguridad;  y  lesiones en la pr&aacute;ctica profesional o en competencia de deportes de alto riesgo como: Acrobacia, Alpinismo/andinismo, Boxeo, Equitaci&oacute;n, Bungee Jumping, Motociclismo, Hockey (c&eacute;sped, hielo, ruedas), Carreras de Autom&oacute;viles, Artes Marciales, Levantamiento de Pesas, Esqu&iacute; (acu&aacute;tico y en nieve), Moton&aacute;utica, Paracaidismo y Parapente, Karting, Rappel, Surf, Lucha, Vela, Rafting y Ciclismo de Competencia.</li>'
            . ' <li>Enfermedades preexistentes con anterioridad a la afiliaci&oacute;n del Usuario, hayan sido o no diagnosticadas por un M&eacute;dico y/o conocidas por el Usuarios, , as&iacute; como sus agudizaciones, consecuencias y/o complicaciones, a&uacute;n cuando estas consecuencias y/o complicaciones aparezcan por primera vez durante el contrato. En tales casos %commercial_name% asistir&aacute; &uacute;nicamente la urgencia inicial cuando la misma implique atenci&oacute;n vital, representado un riesgo inminente para la vida del Usuario y solamente hasta la estabilizaci&oacute;n de sus signos vitales.</li>'
            . ' <li>Ataque de isquemia cerebral transitorio, da&ntilde;os cerebrales causados por accidentes o lesiones, vasculitis y enfermedades inflamatorias, enfermedades vasculares afectando el ojo o el nervio &oacute;ptico, des&oacute;rdenes isquemias del sistema vestibular.</li>'
            . ' <li>Medicamentos de tratamiento ambulatorio en caso de apoplej&iacute;a e Infarto.</li>'
            . ' <li>Carcinoma Basal.</li>'
            . ' <li>Tumores en presencia del Virus de inmunodeficiencia Adquirida, SIDA. Enfermedades de transmisi&oacute;n sexual, s&iacute;ndormoe inmunol&oacute;gico de deficiencia adquirida.</li>'
            . ' <li>Tumores benignos o malformaciones vasculares o afecci&oacute;n originada en tumores benignos de las c&eacute;lulas nerviosas o en malformaciones aneurismas o hemangiomas que en ambos casos se presenten en la parte central del cerebro, en la base del enc&eacute;falo o vecinos a estructuras vitales.</li>'
            . ' <li>Todos los tratamientos paliativos, no relacionados con una etapa curativa del c&aacute;ncer o los presentados en una fase terminal de la enfermedad.</li>'
            . ' <li>Cuando se trate de lesiones o eventos originados en hechos auto infringidos, suicidio y su tentativa o a los cuales haya dado lugar el Beneficiario.</li>'
            . ' <li>Cuando se trate de controles de tensi&oacute;n arterial o enfermedades relacionadas con la hipertensi&oacute;n o hipotensi&oacute;n arterial, s&Iacute;ncopes o secuelas glandulares y sus consecuencias, salvo en cuanto a atenci&oacute;n inicial de urgencia con compromiso vital hasta la estabilizaci&oacute;n de los signos vitales del Beneficiario.</li>'
            . ' <li>Cuando se trate de tratamientos de enfermedades o condiciones oncol&oacute;gicas, diabetes, desordenes cardiovasculares incluyendo hipertensi&oacute;n, enfermedades respiratorias cr&oacute;nicas, infecciones renales cr&oacute;nicas, hepatitis, y aquellas que presenten un compromiso inmunol&oacute;gico, ya sea &eacute;ste consecuencia de la misma enfermedad o de las drogas utilizadas para su tratamiento, sin que esta enumeraci&oacute;n tenga car&aacute;cter taxativo.</li>'
            . ' <li>Cuando se trate de accidentes ocurridos en vuelos ilegales o sin licencia, as&iacute; como aquellos en los cuales tenga incidencia el hecho de ser parte de la tripulaci&oacute;n.</li>'
            . ' <li>Cuando se trate de accidentes o enfermedades consecuencia del trabajo fuera del ciudad de residencia permanente o habitual del Beneficiario.</li>'
            . ' <li>Tratamientos homeop&aacute;ticos y quiropr&aacute;cticos; acupuntura; fsio-kinesioterapia; curas termales, podolog&iacute;a, tratamiento por medio de medicina no convencional, o tratamientos considerados experimentales o investigativos.</li>'
            . ' <li>Diagn&oacute;sticos, seguimientos, ex&aacute;menes, complicaciones y tratamientos del embarazo o interrupci&oacute;n voluntaria del mismo.</li>'
            . ' <li>Tratamientos ginecol&oacute;gicos. El servicio no contempla enfermedades o s&iacute;ntomas cr&oacute;nicos que est&eacute;n siendo tratados por el m&eacute;dico del Beneficiario, cirug&iacute;a no emergente de &uacute;tero, anexos y mamas. Tampoco la implementaci&oacute;n, seguimiento y  control de la fertilidad, o m&eacute;todo anticonceptivo de cualquier tipo. No habr&aacute; cobertura para la hormonoterapia.</li>'
            . ' <li>Tratamientos urol&oacute;gicos, tratamiento quir&uacute;rgico o litotripsia  por nefrolitiasis. Se atender&aacute; la emergencia, confirmaci&oacute;n diagn&oacute;stica, estabilizaci&oacute;n y alivio del dolor. En caso de hiperplasia prost&aacute;tica que se complique y llegue a provocar la retenci&oacute;n urinaria, se cubre exclusivamente los procedimientos de vaciamiento vesical, sondaje permanente y re direccionamiento al especialista del paciente en su pa&iacute;s de residencia permanente o habitual.</li>'
            . ' <li>Tratamientos de odontolog&iacute;a general, endodoncia, exodoncia, enfermedad periodontal, ortodoncia, fisioterapia dental, terceros molares en cualquier circunstancia. Se atender&aacute; la emergencia dental con la consulta, cuidados b&aacute;sicos, alivio del dolor, control de la inflamaci&oacute;n y/o infecci&oacute;n, estabilizaci&oacute;n del cuadro m&eacute;dico y re direccionamiento al odont&oacute;logo del paciente en su pa&iacute;s de residencia permanente o habitual.</li>'
            . ' <li>Tratamientos de otorrinolaringolog&iacute;a, por enfermedades cr&oacute;nicas, salvo que tengan un episodio de reagudizaci&oacute;n, ni en caso de cirug&iacute;a est&eacute;tica, cirug&iacute;a funcional no emergente.</li>'
            . ' <li>Tratamientos de oftalmolog&iacute;a por enfermedades cr&oacute;nicas, ni cirug&iacute;a de excimer l&aacute;ser, pterigium, blefaroplastias, cirug&iacute;a de estrabismo, ambliop&iacute;a, lentes correctivos, lentes de sol, tratamientos de conducto lacrimonasal, biopsias ni ex&eacute;resis de lesiones palpebrales antiguas. Tampoco se prestar&aacute; el servicio de asistencia cuando se trate de servicios de atenci&oacute;n m&eacute;dica de control oftalmol&oacute;gico, as&iacute; hayan sido prescritos por el m&eacute;dico tratante para la evaluaci&oacute;n del seguimiento de una enfermedad diagnosticada durante el viaje y/o aquellas que tengan por objeto la reposici&oacute;n de medicamentos, lentes, lentes de contacto etc. por p&eacute;rdida, robo u olvido de los mismos.</li>'
            . ' <li>Controles de tensi&oacute;n arterial o enfermedades relacionadas con la hipertensi&oacute;n o hipotensi&oacute;n arterial, sincopes o secuelas glandulares y sus consecuencias, salvo en cuanto a atenci&oacute;n inicial de urgencia con compromiso vital hasta la estabilizaci&oacute;n de los signos vitales del Beneficiario.</li>'
            . ' <li>Tratamientos de enfermedades o condiciones oncol&oacute;gicas, diabetes, desordenes cardiovasculares incluyendo hipertensi&oacute;n, enfermedades respiratorias cr&oacute;nicas, infecciones renales cr&oacute;nicas incluyendo todo tipo de c&aacute;lculos o arenillas renales y biliares, hepatitis, y aquellas que presenten un compromiso inmunol&oacute;gico, ya sea &eacute;ste consecuencia de la misma enfermedad o de las drogas utilizadas para su tratamiento, sin que esta enumeraci&oacute;n tenga car&aacute;cter taxativo.</li>'
            . '</ol></p>')
);

$textES['article_7'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA S&Eacute;PTIMA. PER&Iacute;ODOS DE CARENCIA</b><br>'),
	'content' => html_entity_decode('El  per&iacute;odo de carencia se cuenta desde la fecha de inicio de la vigencia del Contrato o desde la fecha de incorporaci&oacute;n de un beneficiario en &eacute;l, hasta que pueda hacer uso de los diferentes beneficios de su plan. Las carencias para cada evento se encuentran detalladas en el Anexo B y de acuerdo a los planes contratados.'
            . '</p>')
            
);

$textES['article_8'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA OCTAVA. OBLIGACIONES de %contracting_party% / CONTRATANTES</b></p>'),
	'content' => html_entity_decode('<p align="justify"><ol type="1" align="justify">'
            . '<li>Responder por el correcto uso que hagan  los afiliados y/o beneficiarios de los servicios y beneficios previstos en este Contrato y/o en los  Planes de Salud.  Responsabilizarse personalmente de sus actos y de aquellos que tengan relaci&oacute;n con los afilados y/o beneficiarios incorporados por &eacute;l.</li>'
            . '<li>Proporcionar tanto la informaci&oacute;n veraz y oportuna, como los antecedentes personales y familiares de salud que %legal_name_com% solicite, con el fin de acceder a los beneficios acordados. As&iacute; mismo, autoriza expresamente a %legal_name_com% para que &eacute;sta solicite a los prestadores de servicios m&eacute;dicos que considere necesario, los antecedentes cl&iacute;nico-quir&uacute;rgicos, historia cl&iacute;nica, radiograf&iacute;as y resultados de ex&aacute;menes realizados al afiliado.</li>'
            . '<li>Entregar a %legal_name_com%, dentro del per&iacute;odo m&aacute;ximo de reclamo del gasto incurrido definido en el Anexo B-Condiciones Particulares del Plan, las facturas, planillas o documentos originales de los m&eacute;dicos y entidades, que soporten las liquidaciones que %legal_name_com% deba realizar. </li>'
            . '<li>Si como resultado de un accidente, acci&oacute;n m&eacute;dica o cualquier otro evento similar, resultare afectada la salud de un beneficiario, y el responsable en forma comprobada fuese un tercero, %legal_name_com% se reserva el derecho de buscar, por los medios que prev&eacute; la Ley, las indemnizaciones a que hubiere lugar.  Para este efecto, los beneficiarios se obligan a conferir a %legal_name_com% las atribuciones necesarias para intervenir a su nombre en las acciones legales que fueren del caso, cediendo a %legal_name_com% la totalidad  de las indemnizaciones o compensaciones que correspondan. </li>'
            . '<li>En caso de un accidente, %contracting_party% est&aacute; obligado a reportarlo a %legal_name_com% dentro de las siguientes 24 horas h&aacute;biles de ocurrido el hecho.  Adicionalmente, %legal_name_com% podr&aacute; exigir una prueba de alcoholemia, en caso de accidente de tr&aacute;nsito, que debe ser realizada en el centro hospitalario al que ingrese.  Si no se reporta el accidente en el plazo establecido, %legal_name_com% liquidar&aacute; el evento bajo los t&eacute;rminos de Libre Elecci&oacute;n aunque haya utilizado el Cuadro Cerrado.</li>'
            . '<li>Concurrir a %legal_name_com% dentro de los 180 d&iacute;as posteriores a la presentaci&oacute;n de facturas a ser restituidas, a cobrar los valores que por este efecto  %legal_name_com% deber&aacute; rembolsar al Contratante.  En caso de no hacerlo, la prestaci&oacute;n se considerar&aacute; cancelada y %legal_name_com% queda liberada de toda responsabilidad.</li>'
            . '<li>Presentar su tarjeta de afiliaci&oacute;n al acceder a la prestaci&oacute;n de un servicio m&eacute;dico en cualquiera de los prestadores de la Red de Convenios en Cuadro Cerrado.</li>'
            . '<li>Informar a %legal_name_com% por escrito sobre cambio de domicilio, direcci&oacute;n y tel&eacute;fono el momento que esto ocurra.</li>'
            . '<li>Pagar cumplida e incondicionalmente las cuotas de afiliaci&oacute;n aplicables, dentro del plazo definido en el Anexo B-Condiciones Particulares del Plan.</li>'
            . '</p><br>')
);

$textES['article_9'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA NOVENA. REQUISITOS PARA AFILIACI&Oacute;N DE CONTRATANTES</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">%contracting_party% llenar&aacute; los Formularios de Ingreso de Beneficiarios, considerando:</p>'
            . '<p align="justify">'
            . ' <ol type="1" align="justify">'
            . '     <li>Son elegibles como contratantes todos aquellos que se enmarquen en la definici&oacute;n dada en la cl&aacute;usula segunda Definiciones. La cobertura por la edad del afiliado est&aacute; definida en el Anexo B-Condiciones Particulares del Plan</li>'
            . '     <li>Para el caso de beneficiarios, se aplicar&aacute; lo dispuesto en la definici&oacute;n de BENEFICIARIOS de la cl&aacute;usula segunda Definiciones.</li>'
            . '     <li>Terminaci&oacute;n del Contrato de acuerdo a la edad del afiliado y/o beneficiarios, de acuerdo al Anexo B-Condiciones Particulares del Plan.</li>'
            . ' </ol>'
            . '</p>'
            . '<p align="justify">'
            . 'Solicitud de Ingreso: %contracting_party%, previamente a la celebraci&oacute;n de este Contrato, deber&aacute; presentar a %commercial_name% el correspondiente Formulario de Enrolamiento y/o Contrato de adhesi&oacute;n, la Declaraci&oacute;n Juramentada de Salud, en caso que %commercial_name% as&iacute; la solicite, que como tales formar&aacute;n parte del presente contrato una vez que %commercial_name% acepte su enrolamiento o aceptaci&oacute;n.'
            . '</p><br>')
);

$textES['article_10'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMA. INCLUSI&Oacute;N DE BENEFICIARIOS</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">Para el caso de inclusiones, %legal_name_com% podr&aacute; solicitar y verificar&aacute; el cumplimiento de los siguientes requisitos de afiliaci&oacute;n:</p>'
            . '<p align="justify">'
            . ' <ol type="a" align="justify">'
            . '     <li>Declaraci&oacute;n juramentada  de Salud o namnesis, hasta el l&iacute;mite de su conocimiento, firmada por el Afiliado o Contratante, la cual contendr&aacute; lo siguiente:'
            . '         <ul style="list-style-type:circle">'
            . '             <li>Estado Actual de Salud;</li>'
            . '             <li>Expresi&oacute;n de enfermedades preexistentes que conoce adolecer y su historia cl&iacute;nica pasada; y,</li>'
            . '             <li>Enfermedades padecidas, operaciones y accidentes sufridos</li>'
            . '         </ul>'
            . '     </li>'
            . '     <li>Solicitud de inclusi&oacute;n, firmada por %contracting_party%.</li>'
            . '     <li>Ex&aacute;menes m&eacute;dicos en caso que as&iacute; se lo requiera.</li>'
            . '     <li>Nuevas inclusiones s&oacute;lo hasta la edad definida en el Anexo B-Condiciones Particulares del Plan.</li>'
            . ' </ol>'
            . '</p>'
            . '<p align="justify">%contracting_party% y/o usuario puede en cualquier tiempo durante la vigencia del contrato solicitar a %legal_name_com%, la inclusi&oacute;n de nuevos beneficiarios. Deber&aacute; presentar pruebas de salud a satisfacci&oacute;n de %legal_name_com% si as&iacute; se solicitare, y en este caso la inclusi&oacute;n se har&aacute; a la fecha en que %legal_name_com% a su sola conveniencia lo decida. Ninguna inclusi&oacute;n ser&aacute; aceptada con car&aacute;cter retroactivo. El valor de la cuota adicional por la incorporaci&oacute;n de nuevos beneficiarios, se adicionar&aacute; al monto total de las cuotas en la siguiente factura.</p>'
            . '<p align="justify">Nuevas inclusiones de acuerdo a la edad definida en Anexo B-Condiciones Particulares del Plan, y podr&aacute;n tener exclusi&oacute;n de preexistencias en curso definida por %legal_name_com% en base a la Declaraci&oacute;n de Salud, y cumplimiento del per&iacute;odo de carencia  definido en el Anexo B-Condiciones Particulares del Plan.  El valor de las inclusiones ser&aacute; el  valor mensual que se encuentre cancelando  %contracting_party%.</p><br>')
);

$textES['article_11'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO PRIMERA. EXCLUSI&Oacute;N BENEFICIARIOS</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'Cuando por cualquier causa, %contracting_party% deseare excluir a un beneficiario del amparo de este Contrato, %contracting_party% deber&aacute; comunicarlo por escrito a %legal_name_com% hasta ocho d&iacute;as antes del inicio de la siguiente vigencia mensual. <br> La solicitud de exclusi&oacute;n, la tramitar&aacute; %legal_name_com% ajustando las cuotas por el retiro, mediante el d&eacute;bito al monto total de cuotas en la siguiente factura. Cuando %legal_name_com% haya aceptado la  exclusi&oacute;n de uno o varios beneficiarios y %contracting_party% tuviere pagadas las cuotas por un lapso mayor, %legal_name_com% devolver&aacute; al Contratante la parte proporcional no devengada.  No se podr&aacute;n tramitar exclusiones retroactivas al per&iacute;odo de vigencia en el cual se report&oacute; la exclusi&oacute;n. <br>La exclusi&oacute;n de un Afiliado, y por tanto la disminuci&oacute;n del valor en las aportaciones, se har&aacute; efectiva a partir del mes de cobertura siguiente al de la notificaci&oacute;n. %contracting_party% no puede disminuir el valor de sus aportaciones de manera unilateral; tampoco pueden procesarse exclusiones de manera retroactiva. '
            . '</p>'
            . '<p align="justify">'
            . 'En caso de contratos prepagados, el valor que se restituir&aacute; por una exclusi&oacute;n de beneficiario  o por la terminaci&oacute;n del contrato, ser&aacute; de acuerdo al porcentaje establecido por la tabla de corto plazo vigente, definida por la Superintendencia de Bancos y Seguros  el valor de las aportaciones pendientes a devengar hasta la finalizaci&oacute;n del Contrato, por gastos administrativos y de manejo del contrato.'
            . '</p><br>')
);

$textES['article_12'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO SEGUNDA. REEMBOLSO POR PRESTACIONES</b></p>'),
	'content' => html_entity_decode('<p align="justify">'
            . ' <ol type="1" align="justify">'
            . '     <li>Para la restituci&oacute;n de prestaciones, el afiliado deber&aacute; presentar los siguientes documentos:'
            . '         <ul style="list-style-type:circle">'
            . '             <li>Formulario de reclamaci&oacute;n de asistencia m&eacute;dica</li>'
            . '             <li>Pedidos  de ex&aacute;menes y recetas emitidos por el m&eacute;dico y/o el prestador; y Facturas y/o planillas originales que cumplan con los requisitos de ley y los solicitados por %commercial_name%, donde se detallen los honorarios pagados, as&iacute; como el (los) diagn&oacute;stico(s) y las prestaciones recibidas por el beneficiario. Incluyendo la factura original de la cl&iacute;nica u hospital con su respectivo desglose, factura de honorarios m&eacute;dicos en el caso de que no est&eacute;n incluida en la factura de la cl&iacute;nica u hospital, y copia completa de la historia cl&iacute;nica.</li>'
            . '             <li>En el caso de emergencias, se deber&aacute; incluir la Hoja de Emergencia que emite la cl&iacute;nica/hospital.</li>'
            . '         </ul>'
            . '     </li>'
            . '     <li>La liquidaci&oacute;n del reembolso se realizar&aacute; de acuerdo a los t&eacute;rminos y condiciones del plan contratado especificados en el Anexo A-Descripci&oacute;n del Plan y Anexo B-Condiciones Particulares del Plan.</li>'
            . '     <li>Si el plan contratado tiene deducible, este valor ser&aacute; aplicado de acuerdo a lo definido en el Anexo A-Descripci&oacute;n del Plan. </li>'
            . ' </ol>'
            . '</p>'
            . '<p align="justify">CAUSALES DE NEGACI&Oacute;N'
            . ' <ol type="1" align="justify">'
            . '     <li>La falta de cancelaci&oacute;n oportuna de una o m&aacute;s aportaciones por parte de %contracting_party%, faculta a %legal_name_com% para negar los reembolsos por gastos incurridos durante el per&iacute;odo de falta de pago.  Un reembolso negado  por esta raz&oacute;n no podr&aacute; volver a presentarse y tendr&aacute; el sello de negaci&oacute;n de %legal_name_com%</li>'
            . '     <li>El incumplimiento de las condiciones del Contrato o sus Anexos, faculta a %legal_name_com% para negar los reembolsos correspondientes.</li>'
            . '     <li>En caso de que las facturas presentadas para liquidar no fueren originales o no cumplieren los requisitos dispuestos en la Ley, %legal_name_com% no los reembolsar&aacute;.</li>'
            . '     <li>Si los documentos para el reembolso son presentados en un plazo superior al definido en el Anexo B-Condiciones Particulares del Plan para la presentaci&oacute;n de reclamos, las facturas ser&aacute;n devueltas por extempor&aacute;neas.</li>'
            . ' </ol>'
            . '</p>'
            . '<p align="justify">AUTORIZACI&Oacute;N PARA ATENCI&Oacute;N HOSPITALARIA'
            . ' <ol type="1" align="justify">'
            . '     <li>Si el beneficiario va a ser sometido a un procedimiento quir&uacute;rgico o cl&iacute;nico, deber&aacute; solicitar la autorizaci&oacute;n respectiva. </li>'
            . '     <li>La Solicitud deber&aacute; ser llenada por el Afiliado y el m&eacute;dico tratante, y deber&aacute; incluir los documentos de sustento especificados para el procedimiento.  %legal_name_com% aprobar&aacute; dicha solicitud siempre y cuando la prestaci&oacute;n est&eacute; cubierta.   En caso de que el afiliado no cumpla este requisito, la prestaci&oacute;n ser&aacute; de acuerdo a lo establecido en el Anexo B-Condiciones Particulares del Plan que forma parte integral del presente contrato.</li>'
            . '     <li>La Solicitud mencionada deber&aacute; presentarse por lo menos 72 horas h&aacute;biles antes de la fecha de hospitalizaci&oacute;n la cirug&iacute;a programada.</li>'
            . '     <li>El evento de emergencia y/o accidente que requiera internaci&oacute;n hospitalaria quir&uacute;rgica tendr&aacute; cobertura una vez autorizado por %legal_name_com%, y mientras esto ocurre, mantendr&aacute; su garant&iacute;a personal de acuerdo a las pol&iacute;ticas de la Instituci&oacute;n de Cuadro Cerrado.</li>'
            . ' </ol>'
            . '</p>')
);

$textES['article_13'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO TERCERA. REVISI&Oacute;N DE PRECIOS</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . '%legal_name_com% podr&aacute; revisar el precio de un plan contratado anualmente de acuerdo al nivel de inflaci&oacute;n en los costos de los servicios de salud registrado por el Instituto Nacional de Estad&iacute;sticas y Censos y el comportamiento de la morbilidad registrada por cada plan; en base a lo que establece el art. 10 de la Ley que regula el funcionamiento de las empresas privadas de salud y medicina prepagada.'
            . '</p><br>')
);

$textES['article_14'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO CUARTA. PAGO DE CUOTAS</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">%contracting_party% se obliga para con %legal_name_com%, a pagar como contraprestaci&oacute;n a los servicios de prestaciones y beneficios de salud previsto en este contrato cada una de las cuotas establecidas para el efecto en el Anexo A-Descripci&oacute;n del Plan, en las condiciones all&iacute; determinadas, mismas que son aceptadas por el usuario. </p>'
            . '<p align="justify">El pago deber&aacute; hacerse de acuerdo a lo establecido en el Anexo B-Condiciones Particulares del Plan.</p>'
            . '<p align="justify">Las cuotas podr&aacute;n ser revisadas o modificadas anualmente, previa notificaci&oacute;n al usuario con 30 d&iacute;as de anticipaci&oacute;n. No obstante dicha modificaci&oacute;n no podr&aacute; exceder los precios vigentes para nuevos contratos.</p><br>')
);

$textES['article_15'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO QUINTA. PLAZO DEL CONTRATO Y RENOVACI&Oacute;N</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'El plazo del Contrato es de un a&ntilde;o; y se renovara por igual per&iacute;odo autom&aacute;ticamente a su vencimiento por periodos iguales, mientras ninguna de las partes manifieste su intenci&oacute;n de no renovarla o de cancelarla.'
            . '</p><br>')
);

$textES['article_16'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO SEXTA. DE LOS DOCUMENTOS</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'Los documentos que forman parte del presente Contrato son:'
            . '</p>'
            . '<p align="justify">'
            . ' <ul style="list-style-type:circle">'
            . '     <li>El Contrato de Prestaci&oacute;n de Servicios de Salud y Medicina Prepagada</li>'
            . '     <li>Anexos del Plan Contratado: Anexo A-Descripci&oacute;n del Plan Contratado, Anexo B-Condiciones Particulares del Plan (las condiciones particulares que se detallan en este anexo, tienen prelaci&oacute;n sobre las generales) y dem&aacute;s anexos que formaran parte integrante del presente contrato una vez suscrito por las partes. Tales anexos tienen id&eacute;ntica validez en derecho y surten plenos efectos jur&iacute;dicos, en los mismo t&eacute;rminos  en lo que lo hacen todas y cada una de las cl&aacute;usulas que forman el cuerpo principal del contrato. Los anexos pueden modificarse en cualquier momento por las partes. Tales anexos tienen id&eacute;ntica validez en derecho y surten plenos efectos jur&iacute;dicos, en los mismo t&eacute;rminos  en lo que lo hacen todas y cada una de las cl&aacute;usulas que forman el cuerpo principal del contrato. Los anexos pueden modificarse en cualquier momento por las partes lo mismo que debe ser aprobado por la Autoridad Sanitaria Nacional.  debiendo al efecto: suscribirse un nuevo anexo, o acordarse la modificaci&oacute;n por cruce de notas escritas entre las partes. Las partes quedan en libertad de suscribir anexos adicionales a los citados en este contrato, si as&iacute; lo estiman necesario y conveniente para acordar condiciones especiales, particulares, distintas o modificaciones de aquellas previsiones de este contrato. A estos otros anexos les son tambi&eacute;n aplicables lo anteriormente se&ntilde;alado en esta cl&aacute;usula.</li>'
            . '     <li>La(s) Declaraci&oacute;n(es) Juramentada(s) de Salud (de los afiliados desde el inicio de vigencia y en el caso de nuevas inclusiones)</li>'
            . '     <li>Los documentos de soporte solicitados por %legal_name_com% de acuerdo al plan contratado</li>'
            . ' </ul>'
            . '</p>'
            . '<p align="justify">%legal_name_com% se reserva el derecho de revisar la Declaraci&oacute;n Juramentada de Salud de cada uno de los aspirantes a Afiliados de este Contrato, determinando condiciones especiales de cobertura cuando &eacute;ste sea el caso acerca de exclusiones permanentes definidas por Contralor&iacute;a M&eacute;dica.</p><br>')
);

$textES['article_17'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO S&Eacute;PTIMA. TERMINACI&Oacute;N DEL CONTRATO</b></p>'),
	'content' => html_entity_decode('<p align="justify">'
            . '  <ol type="1" align="justify">'
            . '     <li>Por decisi&oacute;n unilateral de %contracting_party% en cualquier momento, para lo cual &eacute;ste comunicar&aacute; su decisi&oacute;n por escrito con por lo menos 15 d&iacute;as de anticipaci&oacute;n a la fecha de terminaci&oacute;n deseada. Para ser v&aacute;lida, la comunicaci&oacute;n deber&aacute; tener la fe de recepci&oacute;n por parte de %legal_name_com%</li>'
            . '     <li>Por  decisi&oacute;n unilateral de %legal_name_com%, por incumplimientos de %contracting_party% tales como: falta de pago de una o m&aacute;s aportaciones caso en el cual se cancelar&aacute; la prestaci&oacute;n de los servicios aqu&iacute; previstos; a estos efectos %contracting_party% y/o beneficiarios, expresamente renuncian a los requerimientos para constituirlo en mora previstos en la legislaci&oacute;n aplicable en la materia, falta de verdad en Declaraciones de Salud, irregularidades en la suscripci&oacute;n del Contrato, entrega de informaci&oacute;n falsa, intento de fraude en la presentaci&oacute;n de facturas a ser liquidadas.</li>'
            . '  </ol>'
            . '</p>'
            . '<p align="justify">'
            . '%commercial_name% solo podr&aacute; hacer uso de esta posibilidad dentro de los 2 primeros a&ntilde;os de vigencia del Contrato. <br>'
            . 'En el caso previsto en el Numeral Uno anterior, %commercial_name% reembolsar&aacute; al CONTRATANTE el 80% del valor de la cuota del per&iacute;odo no transcurrido del Contrato. El 20% restante quedar&aacute; a disposici&oacute;n de %commercial_name% por concepto de gastos generales y de administraci&oacute;n, lo mismo se aplicar&aacute; para la exclusi&oacute;n de Usuarios y/o Dependientes del Contrato. <br>'
            . 'Para el caso previsto en el Numeral Dos anterior, %commercial_name% se obliga a devolver la parte proporcional no devengada.'
            . '</p><br>')
);

$textES['article_18'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO OCTAVA. COORDINACI&Oacute;N DE BENEFICIOS</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'En caso de que los beneficiarios de este Contrato cuenten con un seguro de asistencia m&eacute;dica o de accidentes personales, o con un Contrato de medicina prepagada, adem&aacute;s del contratado para ASISTENCIA MEDICA EMERGENTE INTERNACIONAL , %legal_name_com% reembolsar&aacute; al beneficiario sobre la diferencia entre el gasto efectivamente realizado y el monto pagado por la otra compa&ntilde;&iacute;a, seg&uacute;n los l&iacute;mites de su plan, y una vez cubierto el deducible. En ning&uacute;n caso el reembolso total ser&aacute; mayor al l&iacute;mite establecido en el plan contratado y al monto total de gastos m&eacute;dicos. Para que el beneficiario pueda ejercer su derecho a solicitar el reembolso, debe previamente presentar a %legal_name_com% la liquidaci&oacute;n original de la otra compa&ntilde;&iacute;a o copia certificada de la misma, y copias de todos los documentos habilitantes. Cabe indicar que el afiliado elige la Compa&ntilde;&iacute;a que procesar&aacute; el reembolso de la coordinaci&oacute;n de beneficios.'
            . '</p><br><br>')
);

$textES['article_19'] = array(
	'title' => html_entity_decode('<br><p align="justify"><b>CL&Aacute;USULA D&Eacute;CIMO NOVENA. NULIDAD PARCIAL</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'Si una o m&aacute;s de las disposiciones de este Contrato se llegase a declarar inv&aacute;lida, ilegal o inejecutable en cualquier jurisdicci&oacute;n o con  respecto a cualquiera de las partes, dicha nulidad, ilegalidad o inejecutabilidad, no deber&aacute; ser refutada por las partes como nulita, o torna ilegal o inejecutable al resto del Contrato.'
            . '</p>'
            . '<p align="justify">'
            . 'Si una o m&aacute;s disposiciones de este Contrato no posibilitaren su aplicaci&oacute;n o tuvieren el car&aacute;cter de inejecutables no podr&aacute;n ser refutadas como causas de nulidad del resto del Contrato.'
            . '</p><br>')
);

$textES['article_20'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CLAUSULA VIG&Eacute;SIMA. JURISDICCI&Oacute;N Y DOMICILIO</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'Para el caso de suscitarse controversias relacionadas con este Contrato, las partes se someten al procedimiento prescrito en los  art&iacute;culos 17 y 18 de la Ley que Regula el Funcionamiento de las Empresas Privadas de Salud y Medicina Prepagada en los que se menciona que en primera instancia se acudir&aacute; a la Direcci&oacute;n Provincial de Salud para resolver la controversia y en segunda instancia se recurrir&aacute; a tr&aacute;mite verbal sumario ante los jueces y tribunales competentes seg&uacute;n sea su voluntad, sino tambi&eacute;n , las partes de conformidad con la Ley de Arbitraje y Mediaci&oacute;n, someten la resoluci&oacute;n de todas las controversias originadas en la interpretaci&oacute;n, aplicaci&oacute;n y cumplimiento de este Contrato al arbitraje administrado por el Centro de Arbitraje de la C&aacute;mara de Comercio de Quito, pudiendo libremente demandar y reconvenir exclusivamente sobre la misma materia y debiendo efectuarse un arbitraje de derecho por los &aacute;rbitros de dicho Centro.  El tribunal arbitral  estar&aacute; conformado por un &aacute;rbitro elegido por acuerdo de las partes, en caso de no llegar a un acuerdo lo determinar&aacute; el Director del Centro.  Los &aacute;rbitros est&aacute;n facultados para dictar medidas cautelares y solicitar el auxilio de funcionarios p&uacute;blicos para su ejecuci&oacute;n.  Las partes se&ntilde;alan como sus domicilios, para todas las citaciones y notificaciones del arbitraje,  los que constan en este Contrato.'
            . '</p><br>')
);

$textES['article_21'] = array(
	'title' => html_entity_decode('<p align="justify"><b>CLAUSULA VIG&Eacute;SIMA PRIMERA. NOTIFICACIONES</b></p><br>'),
	'content' => html_entity_decode('<p align="justify">'
            . 'Toda notificaci&oacute;n que requieran realizar los contratantes entre s&iacute;, en relaci&oacute;n con el presente Convenio se har&aacute; por escrito a las siguientes direcciones:'
            . '</p>'
            . '<p align="justify">'
            . '<table cellspacing="0" cellpadding="1" border="0">
                <tr>
                    <td>%legal_name_com%:</td>
                    <td colspan="2">%legal_name_com_address%</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Quito - Ecuador</td>
                </tr>
                <tr>
                    <td>%contracting_party%:</td>
                    <td colspan="2">%customer_address%</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Quito - Ecuador</td>
                </tr>
               </table>'
            . '</p>'
            . '<p align="justify">'
            . 'De presentarse cambio en las direcciones descritas, la parte respectiva dar&aacute; aviso por escrito de tal hecho a la otra, dentro de las 24 horas de producido el cambio.'
            . '</p>'
            . '<p align="justify">'
            . 'En constancia de aceptaci&oacute;n de todo lo antes expuesto, %contracting_party% y %legal_name_com% suscriben el presente instrumento por duplicado, en el lugar y fecha se&ntilde;alados al inicio del mismo.'
            . '</p>'
            . '<p align="justify">'
            . 'ACEPTACI&Oacute;N: <br>Las partes se someten al procedimiento prescrito en los  art&iacute;culos 17 y 18 de la Ley que Regula el Funcionamiento de las Empresas Privadas de Salud y Medicina Prepagada, pero de ser el caso las partes tambi&eacute;n aceptan someterse al Arbitraje como medio de resoluci&oacute;n de conflictos que se presenten por el contrato, ejecuci&oacute;n, liquidaci&oacute;n e interpretaci&oacute;n del mismo.'
            . '</p><br>')
);



/*****************************************  ENGLISH TEXTS *********************************************/
$textEN = array();


$saleContract = array('es' => $textES, 'en' => $textES);
$saleContract = $saleContract[$lang_string];

?>