<?php
/**
 * Created by PhpStorm.
 * User: sistemas5
 * Date: 25/4/2019
 * Time: 9:30
 */


/*****************************************  SPANISH TEXTS *********************************************/
$textES = array();
$textES['title'] = array(
    'international' => 'Asistencia Internacional en Viajes',
    'national' => 'Asistencia Nacional en Viajes'
);
$textES['qr'] = array(
    'contract' => 'Contrato: ',
    'verification_label' => ' Cod. Verificador: '
);
$textES['header1'] = array(
    'title' => 'Detalle de venta',
    'branch' => 'Sucursal de Comercializador: ',
    'counter' => 'Counter: ','title2' => 'Registro de Viaje'
);
$textES['header2'] = array(
    'codBranch' => 'COD. SUC: ',
    'location_date' => utf8_decode(html_entity_decode('Lugar y Fecha de emisi&oacute;n:')),
    'emission' => 'dd      mm       aa',
    'contract' => utf8_decode(html_entity_decode('Contrato de adhesi&oacute;n N:'))
);
$textES['client'] = array(
    'title' => 'DATOS DEL CLIENTE',
    'extras' => 'ADICIONALES',
    '0' => 					'          Apellidos                                            Nombres                                    C.C. o Pasaporte                   Fec. de Nacimiento',
    '1' => utf8_decode(html_entity_decode('Tel&eacute;fono(s)                                          E-mail                                                          Direcci&oacute;n/Ciudad/Pa&iacute;s')),
    '2' =>  NULL,
    '3' => 'En caso de emergencia notificar a:',
    '4' => utf8_decode(html_entity_decode('Tel&eacute;fono:')),
    '5' => utf8_decode(html_entity_decode('      Fec. de Nacimiento                                    Relaci&oacute;n                                               E-mail')),
    'table' => array(
        '0' => 'C.I / Pasaporte',
        '1' => 'Nombre',
        '2' => 'Edad',
        '3' => 'Tarifa USD:'
    )
);
$textES['trip'] = array(
    'title1' => 'DATOS DEL VIAJE',
    'title2' => 'VIGENCIA',
    '0' => utf8_decode(html_entity_decode('Fecha de Salida                                Fecha de Regreso                                Duraci&oacute;n                      Destino')),
    '1' => 					  '                                 dd    mm     aa                                                           dd    mm     aa                                    dd',
    '2' => NULL,
    '3' => utf8_decode(html_entity_decode('Fecha de Inicio                                Fecha de Finalizaci&oacute;n                           Duraci&oacute;n')),
    '4' => utf8_decode(html_entity_decode('Fecha de Inicio                                      Fecha de Finalizaci&oacute;n                                  D&iacute;as'))
);
$textES['product'] = array(
    'title' => 'DATOS DEL PRODUCTO',
    '0' => 'Nombre del Producto:',
    '1' => 'Tarifa USD:',
    '2' => 'TOTAL TARIFA USD:'
);
$textES['service'] = array(
    'title' => 'SERVICIOS ADICIONALES',
    '0' =>  'Servicio:',
    '1' =>  'Cobertura USD:',
    '2' =>  'Tarifa Servicios USD:'
);
$textES['benefits'] = array(
    'title' => 'BENEFICIOS                                                                      COBERTURA',
    '0' => 'El Resto de Beneficios se encuentran Detallados en Condiciones Generales'
);
$textES['recomen'] = array(
    'title' => 'RECOMENDACIONES:',
    '0' => utf8_decode(html_entity_decode('Comunicarse previamente y de manera inmediata con la Central de Servicios, antes de comprometer cualquier tipo de gasto. Recuerde que la central de Servicios determinar&aacute; la forma de manejar el caso.')),
    '1' => utf8_decode(html_entity_decode('Si la condici&oacute;n de emergencia presentada, le impide la comunicaci&oacute;n inmediata por causa de fuerza mayor podr&aacute; contratar los servicios requeridos, debiendo comunicarse con la Central de Servicios dentro de las 24 horas siguientes a la ocurrencia del hecho.')),
    '2' => utf8_decode(html_entity_decode('Presentar la documentaci&oacute;n original con sus respaldos respectivos.')),
    '3' => utf8_decode(html_entity_decode('En caso de negaci&oacute;n de Visa se debe notificar con 72 horas antes del inicio de cobertura.')),
    '4' => utf8_decode(html_entity_decode('Aseg&uacute;rese de leer el documento de Condiciones generales que se adjuntan, y las condiciones particulares que se detallan a continuaci&oacute;n.')),
);
$textES['con_general'] = array(
    'title' => 'CONDICIONES GENERALES:',
    '0' => utf8_decode(html_entity_decode('El Cliente reconoce y acepta darse por enterado de todas las clausulas detalladas en las condiciones Generales Planet Assist, las mismas que han sido entregadas f&iacute;sicamente, enviadas mediante correo electr&oacute;nico o tambi&eacute;n puede ser descargada desde la p&aacute;gina web www.planet-assist.net El presente Contrato, se regir&aacute; bajo las Condiciones Generales y Particulares de Planet Assist vigentes al momento de su emisi&oacute;n, independientemente de que sean anuladas, modificadas o agregadas.')),
    '1' => utf8_decode(html_entity_decode('Puede descargarse las condiciones generales en los siguientes Links:')),
    '2' => utf8_decode(html_entity_decode('https://www.planet-assist.net/useConditions')),
    '3' => utf8_decode(html_entity_decode('Exclusiones seg&uacute;n condiciones particulares')),
);
$textES['exclu'] = array(
    'title' => 'EXCLUSIONES:',
    '0' => utf8_decode(html_entity_decode('Iniciar, recibir, continuar o finalizar un tratamiento m&eacute;dico.')),
    '1' => utf8_decode(html_entity_decode('Tratamientos cosm&eacute;ticos, de cirug&iacute;a est&eacute;tica, reconstrucci&oacute;n, dermatolog&iacute;a en caso de tratamiento de acn&eacute;, dermatitis.')),
    '2' => utf8_decode(html_entity_decode('Enfermedades cr&oacute;nicas y preexistentes, cong&eacute;nitas o recurrentes, conocidas o no por el Beneficiario.')),
    '3' => utf8_decode(html_entity_decode('Tratamientos homeop&aacute;ticos y quiropr&aacute;cticos; acupuntura entre otras.')),
    '4' => utf8_decode(html_entity_decode('Lesiones o eventos originados en hechos auto infringidos, suicidio y su tentativa.')),
    '5' => utf8_decode(html_entity_decode('Tratamientos o enfermedades mentales, nerviosas, trastornos psicol&oacute;gicos, psiqui&aacute;tricos, estr&eacute;s')),
    '6' => utf8_decode(html_entity_decode('Afecciones, enfermedades, accidentes derivados de Ingesti&oacute;n o administraci&oacute;n de t&oacute;xicos (drogas)), narc&oacute;ticos y/o  bebidas alcoh&oacute;licas.')),
    '7' => utf8_decode(html_entity_decode('Tratamientos hechos por especialistas que no pertenezcan al equipo profesional m&eacute;dico indicados por la central.')),
    '8' => utf8_decode(html_entity_decode('Eventos ocasionados por la manipulaci&oacute;n o uso de qu&iacute;micos o productos nocivos.')),
    '9' => utf8_decode(html_entity_decode('Diagn&oacute;sticos, ex&aacute;menes, y  tratamientos del embarazo o interrupci&oacute;n voluntaria del mismo. ')),
    '10' => utf8_decode(html_entity_decode('Chequeos m&eacute;dicos y toda clase de tratamientos prolongados, ni aquellos que requieran el control de una patolog&iacute;a.')),
    '11' => utf8_decode(html_entity_decode('Cualquier tipo de tratamiento de ginecolog&iacute;a, odontolog&iacute;a, otorrinolaringolog&iacute;a,  oftalmol&oacute;gica')),
    '12' => utf8_decode(html_entity_decode('Tratamientos o cirug&iacute;as no emergentes de &uacute;tero, anexos y mamas. Tampoco la implementaci&oacute;n, seguimiento y control de la fertilidad, o m&eacute;todo anticonceptivo de cualquier tipo.')),
    '13' => utf8_decode(html_entity_decode('Infecciones Urinarias, se atender&aacute; la emergencia hasta su confirmaci&oacute;n diagnostica.')),
    '14' => utf8_decode(html_entity_decode('Tratamientos urol&oacute;gicos, tratamiento quir&uacute;rgico, c&aacute;lculos renales, biliares, arenillas, nefrolitiasis o cualquier tipo de complicaci&oacute;n derivado de este tipo. Se atender&aacute; &uacute;nicamente hasta su confirmaci&oacute;n diagn&oacute;stica.')),
    '15' => utf8_decode(html_entity_decode('Incidentes durante viajes realizados en contra de recomendaci&oacute;n o prescripci&oacute;n m&eacute;dica.')),
    '16' => utf8_decode(html_entity_decode('Gastos presentados  para su reintegro pasados los180 d&iacute;as de ocurrido el hecho.')),
    '17' => utf8_decode(html_entity_decode('Todos los casos de asistencia que no sean documentados dentro de los 180 d&iacute;as plazo.')),
    '18' => utf8_decode(html_entity_decode('Enfermedades end&eacute;micas o epid&eacute;micas.')),
    '19' => utf8_decode(html_entity_decode('Trasplante de &oacute;rganos y tejidos.')),
    '20' => utf8_decode(html_entity_decode('Eventos consecuencia de actos de guerra insurrecci&oacute;n, terrorismo, motines, huelgas.')),
    '21' => utf8_decode(html_entity_decode('Intervenciones quir&uacute;rgicas que requieran la implantaci&oacute;n o reparaci&oacute;n de pr&oacute;tesis, etc.')),
    '22' => utf8_decode(html_entity_decode('Enfermedades de transmisi&oacute;n sexual, VIH (SIDA)')),
    '23' => utf8_decode(html_entity_decode('Controles de tensi&oacute;n arterial o enfermedades  hipertensi&oacute;n o hipotensi&oacute;n, s&iacute;ncopes.')),
    '24' => utf8_decode(html_entity_decode('Enfermedades   Vasculares tales como varices, trombocitopenia, trombosis, hemorroides, etc., sin que la enumeraci&oacute;n tenga car&aacute;cter taxativo.')),
    '25' => utf8_decode(html_entity_decode('Enfermedades o condiciones oncol&oacute;gicas, diabetes, desordenes cardiovasculares, hepatitis, infecciones renales, enfermedades respiratorias cr&oacute;nicas, alergias estacionarias, Enfermedades gastrointestinales cr&oacute;nicas tales como gastritis, &uacute;lceras p&eacute;pticas, pancreatitis, colitis.')),
    '26' => utf8_decode(html_entity_decode('Pr&aacute;ctica profesional, y no profesional  de deportes peligrosos. (Salvo se pague la tarifa adicional o se contrate el producto que los cubra).')),
    '27' => utf8_decode(html_entity_decode('Accidentes ocurridos en vuelos ilegales o sin licencia.')),
    '28' => utf8_decode(html_entity_decode('Accidentes o enfermedades relacionadas con la participaci&oacute;n del Beneficiario como parte de cuerpos armados, de polic&iacute;a o bomberos.')),
    '29' => utf8_decode(html_entity_decode('Accidentes o enfermedades consecuencia del trabajo fuera del pa&iacute;s de residencia permanente o habitual del Beneficiario.')),
);
$textES['pcon'] = array(
    'title' => 'CONDICIONES PARTICULARES:'
);
$textES['phones'] = array(
    'title' => utf8_decode(html_entity_decode('CENTRAL DE ASISTENCIA:')),
    'leyend' => 'En caso de cualquier tipo de siniestro, comunicarse  de manera inmediata a nuestra central de Servicios, por cualquiera de los siguientes medios:',
    'phones' => utf8_decode(html_entity_decode('N&uacute;meros Internacionales. (puede marcar cualquier n&uacute;mero, he indicar al operador que le devuelva la llamada)')),

    'other_marks' => array(
        '0' => utf8_decode(html_entity_decode('')),
        '1' => utf8_decode(html_entity_decode('Alerta mediante nuestra WEB-SITE '.$web_url.' en la opci&oacute;n "Ayuda en el Exterior".')),
        '2' => utf8_decode(html_entity_decode('Servicio SKYPE. (asistencias-planet / asistenciasplanet-assist1)')),
        '3' => utf8_decode(html_entity_decode('Correo electr&oacute;nico. (asistencias@planet-assist.net)')),
        '4' => utf8_decode(html_entity_decode('Correo electr&oacute;nico. (asistencias@planet-assist.net)')),
        '5' => utf8_decode(html_entity_decode('Blue App (Aplicaci&oacute;n para iPhone)'))
    )
);
$textES['signature'] = utf8_decode(html_entity_decode('Al usar los servicios de la compa&ntilde;&iacute;a, certifico que he le&iacute;do y acepto las condiciones generales adjuntas y sus condiciones particulares.'));







/*****************************************  ENGLISH TEXTS *********************************************/
$textEN = array();
$textEN['title'] = array(
    'international' => 'International Travel Assistance',
    'national' => 'Domestic Travel Assistance'
);
$textEN['qr'] = array(
    'contract' => 'Contract: ',
    'verification_label' => ' Verification Code: '
);
$textEN['header1'] = array(
    'title' => 'Sale Detail',
    'branch' => 'Marketer Branch: ',
    'counter' => 'Contract: ',
    'title2' => 'Trip Log'
);
$textEN['header2'] = array(
    'codBranch' => 'COD. SUC: ',
    'location_date' => html_entity_decode('Place and Date of emission:'),
    'emission' => 'dd      mm       yy',
    'contract' => html_entity_decode('Accession Contract')
);
$textEN['client'] = array(
    'title' => 'CLIENT INFORMATION',
    'extras' => 'ADDITIONALS',
    '0' => '          Last Names                                           Names                                    ID Or Passport                   Date of Birth',
    '1' => html_entity_decode('Phone(s)                                             E-mail                                                          Address/City/Country'),
    '2' => NULL,
    '3' => 'In case of emergency notify to:',
    '4' => html_entity_decode('Phone:'),
    '5' => html_entity_decode('      Birth Date                                    Relationship                                               E-mail'),
    'table' => array(
        '0' => 'ID / Passport',
        '1' => 'Names',
        '2' => 'Age',
        '3' => 'Fare USD:'
    )
);
$textEN['trip'] = array(
    'title1' => 'TRIP INFORMATION',
    'title2' => 'VALIDITY',
    '0' => html_entity_decode('Departure Date                                Return Date                                      Duration                      Destination'),
    '1' => 					  '                                 dd    mm     yy                                                           dd    mm     yy                                    dd',
    '2' => NULL,
    '3' => html_entity_decode('Begin Date                                       End Date                                      Duration'),
    '4' => html_entity_decode('Begin Date                                            End Date                                             Duration')
);
$textEN['product'] = array(
    'title' => 'PRODUCT DATA',
    '0' => 'Product Name:',
    '1' => 'Fare USD:',
    '2' => 'Total Fare USD:'
);
$textEN['service'] = array(
    'title' => 'SERVICES',
    '0' =>  'Service:',
    '1' =>  'Coverage:',
    '2' =>  'Service Fare:'
);
$textEN['benefits'] = array(
    'title' => 'BENEFITS                                                                      COVERAGE',
    '0' => 'The rest of the benefits are detailed with the General Conditions'
);
$textEN['recomen'] = array(
    'title' => 'RECOMENDATIONS:',
    '0' => html_entity_decode('Communicate previously in an immediate way to the Central of Services'),
    '1' => html_entity_decode('If the emergency situation does not allow to contact us immediately, you may engage the required services and you should contact the Central of Services within 24 hours of occurrence'),
    '2' => html_entity_decode('Services available within 24 hours from the occurrence.'),
    '3' => html_entity_decode('Present all the original documentation with the corresponding backup.'),
    '4' => html_entity_decode('In case of Visa denial, it must be notified 72 hours before the coverage start.'),
    '5' => html_entity_decode('Make sure you read the General Conditions attached to this contract.')
);
$textEN['exclu'] = array(
    'title' => 'EXCLUSIONS:',
    '0' => 'Commence, receive, continue or finalize medical treatment.',
    '1' => 'Cosmetic treatments, cosmetic surgery and reconstruction.',
    '2' => 'Chronic and pre-existing illnesses, congenital or recurrent, known or unknown by the Cardholder',
    '3' => 'Homoeopathic and chiropractic treatments; acupuncture etc.',
    '4' => 'Injuries or events resulting from self-inflicted harm, suicide or attempted suicide.',
    '5' => 'Treatment or mental illness, nervous, psychological, psychiatric',
    '6' => 'Diseases resulting from ingestion or administration of intoxicants (drugs), narcotics and / or alcohol.',
    '7' => 'Medical check-ups and all types of extended treatment to medical teams indicated by the central of assistance.',
    '8' => 'Events caused by handling and/or using chemicals and/or products harmful to health.',
    '9' => 'Diagnosis, follow-up, exams, complications and treatment for pregnancy or voluntary interruption thereof.',
    '10' => 'Check-ups and all kinds of long-term treatment, or those requiring a disease control.',
    '11' => 'Any type of therapy gynecology, dentistry, Otorhinolaryngology, ophthalmology.',
    '12' => 'Treatment for non-emergency surgery of the uterus, attachments and breasts. There shall be no coverage for fertility consultations or treatments, control, or contraceptive methods of any kind.&nbsp;',
    '13' => 'Urology treatments, surgical treatment or lithotripsy, gallstones, urinary tract infections.',
    '14' => 'Incidents occurring during trips made against medical advice and/or the Service Center.',
    '15' => 'Any expense submitted for reimbursement 180 days after the incident occurred.',
    '16' => 'Cases that are not documented or that the documentation required has not been submitted within 180 days after service',
    '17' => 'Endemic or epidemic illnesses',
    '18' => 'Organ and tissue transplant.',
    '19' => 'Events caused by acts of war, insurgency, war, terrorism and disturbance of the peace, riots, uprising.',
    '20' => 'Surgical interventions that require inserting and/or repairing prosthesis, etc.',
    '21' => 'Sexual transmitted diseases, (AIDS).',
    '22' => 'Blood pressure control or illnesses related to hypertension or hypotension, syncope.',
    '23' => 'Varicose Diseases, thrombocytopenia, thrombosis, and hemorrhoids.',
    '24' => 'Oncological diseases or conditions, diabetes, cardiovascular disorders, hepatitis, chronic renal infections, chronic respiratory illnesses.',
    '25' => 'Professional practice, and not professional of dangerous sports. (Unless you pay the additional fee or contract that covers the product).',
    '26' => 'Accidents occurring on illegal or unlicensed flights',
    '27' => 'Accidents or illnesses related to the Cardholder and/or Client&rsquo;s participation in the armed forces, police force or fire brigade.',
    '28' => 'Accidents or illnesses resulting from work outside the country of permanent residence of the beneficiary',
);
$textEN['pcon'] = array(
    'title' => 'PARTICULAR CONDITIONS:'
);
$textEN['phones'] = array(
    'title' => html_entity_decode('ASSISTANCE PHONES:'),
    'leyend' => 'In the event of a casualty or incident, communicate immediately to the Central of Services, by any of the following means:',
    'phones' => html_entity_decode('International Numbers (you can dial any number, and request the operator to call you back)'),
    'other_marks' => array(
        '0' => html_entity_decode('Alert Notifications by our WEB-SITE '.$web_url.' in the option "Help Abroad".'),
        '1' => html_entity_decode('SKYPE Free Call (asistencias-planet / asistenciasplanet-assist1)'),
        '2' => html_entity_decode('E-mail (asistencias@planet-assist.net)'),
        '3' => html_entity_decode('BlueApp (iPhone Application)')
    )
);
$textEN['signature'] = html_entity_decode('When using the company`s service, I agree and accept the general conditions attached to this document and its particular conditions.');

$saleContract = array('es' => $textES, 'en' => $textEN);
$saleContract = $saleContract[$lang_string];