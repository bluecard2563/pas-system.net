<?php

/**
 * File: text_diners
 * Author: Patricio Astudillo
 * Creation Date: 22-feb-2013
 * Last Modified: 22-feb-2013
 * Modified By: Patricio Astudillo
 */
/* * ***************************************  SPANISH TEXTS ******************************************** */
$textES = array();
$textES['date'] = array(
    'legend' => utf8_decode(html_entity_decode('Lugar y Fecha de Emisi&oacute;n:')),
    'labels' => ' dd     mm       aa'
);

$textES['client'] = array('title' => 'DATOS DEL CLIENTE',
    'extras' => 'ADICIONALES',
    'line_0' => '        Apellidos                                                            Nombres                                                C.C. o Pasaporte',
    'line_1' => 'Empresa',
    'emergency_label1' => 'En caso de emergencia notificar a:',
    'emergency_data1' => 'Central de Emergencias',
    'emergency_label2' => utf8_decode(html_entity_decode('Tel&eacute;fono:')),
    'emergency_data2' => '(01) 213 66 55 op. 1'
);

$textES['trip'] = array('title1' => 'DATOS DEL VIAJE',
    '0' => utf8_decode(html_entity_decode('        Fecha de Salida                              Fecha de Regreso                               Duraci&oacute;n                                    Destino')),
    '1' => '           dd    mm     aa                                             dd    mm     aa                                                      dd',
);

$textES['benefits'] = array(
    'titles' => array('col_1' => '<b>SERVICIOS</b>',
        'col_2' => '<b>COBERTURAS</b>'),
    'data' => array(
        array('col_1' => html_entity_decode('<b>ASISTENCIA EN VIAJES</b>'),
            'col_2' => utf8_decode(html_entity_decode('<b>MONTO M&Aacute;XIMO (EN D&Oacute;LARES)</b>'))),
        array('col_1' => utf8_decode(html_entity_decode('Asistencia m&eacute;dica por accidente')),
            'col_2' => 'US$ 100,000.00'),
        array('col_1' => utf8_decode(html_entity_decode('Asistencia m&eacute;dica por enfermedad (incluido medicamentos)')),
            'col_2' => 'US$ 50,000.00'),
        array('col_1' => utf8_decode(html_entity_decode('Odontolog&iacute;a por emergencia')),
            'col_2' => 'US$ 2,000.00'),
        array('col_1' => html_entity_decode('Traslado Sanitario'),
            'col_2' => 'US$ 50,000.00'),
        array('col_1' => utf8_decode(html_entity_decode('Traslado de familiar por hospitalizaci&oacute;n')),
            'col_2' => utf8_decode(html_entity_decode('Tkt a&eacute;reo + Hotel'))),
        array('col_1' => html_entity_decode('Hotel por convalecencia'),
            'col_2' => 'US$ 850.00'),
        array('col_1' => utf8_decode(html_entity_decode('Acompa&ntilde;amiento de menores')),
            'col_2' => 'SI'),
        array('col_1' => utf8_decode(html_entity_decode('Repatriaci&oacute;n funeraria')),
            'col_2' => 'US$ 50,000.00'),
        array('col_1' => html_entity_decode('Regreso por fallecimiento de familiar'),
            'col_2' => 'SI'),
        array('col_1' => html_entity_decode('Regreso anticipado, siniestro en domicilio'),
            'col_2' => 'SI'),
        array('col_1' => utf8_decode(html_entity_decode('Indemnizaci&oacute;n vuelo demorado o cancelado')),
            'col_2' => 'US$ 150.00'),
        array('col_1' => utf8_decode(html_entity_decode('Anulaci&oacute;n de viaje')),
            'col_2' => 'US$ 1,500.00'),
        array('col_1' => utf8_decode(html_entity_decode('Traslado de ejecutivo en substituci&oacute;n')),
            'col_2' => 'SI'),
        array('col_1' => html_entity_decode('Mensajes urgentes'),
            'col_2' => 'SI'),
        array('col_1' => utf8_decode(html_entity_decode('Apoyo por extrav&iacute;o de documentos o equipajes')),
            'col_2' => 'SI'),
        array('col_1' => utf8_decode(html_entity_decode('L&iacute;nea de consultas (24 horas)')),
            'col_2' => 'SI'),
        array('col_1' => utf8_decode(html_entity_decode('Indemnizaci&oacute;n por p&eacute;rdida de equipaje')),
            'col_2' => 'US$ 1,500.00'),
        array('col_1' => utf8_decode(html_entity_decode('Compensaci&oacute;n por demora de equipaje (Primeras 6 horas)')),
            'col_2' => 'US$ 250.00'),
        array('col_1' => utf8_decode(html_entity_decode('Compensaci&oacute;n por demora de equipaje (Luego de 6 horas y hasta 36 hrs)')),
            'col_2' => 'US$ 400.00'),
        array('col_1' => html_entity_decode('Transferencia de fondos'),
            'col_2' => 'US$ 5,000.00'),
        array('col_1' => html_entity_decode('Transferencia de fondos para fianza legal'),
            'col_2' => 'US$ 15,000.00'),
        array('col_1' => utf8_decode(html_entity_decode('Asistencia legal por accidente de tr&aacute;nsito')),
            'col_2' => 'US$ 2,500.00'),
        array('col_1' => utf8_decode(html_entity_decode('Enfermedad cr&oacute;nica o preexistente')),
            'col_2' => 'US$ 700.00'),
    )
);

$textES['assistance_phones'] = array(
    array('col_1' => utf8_decode(html_entity_decode('USA (L&iacute;nea gratuita)')),
        'col_2' => '1-877-216-7219'),
    array('col_1' => utf8_decode(html_entity_decode('Espa&ntilde;a')),
        'col_2' => '9-111-815905'),
    array('col_1' => html_entity_decode('USA (Por cobrar)'),
        'col_2' => '1-954-472-1895'),
);

$saleContract = array('es' => $textES);
$saleContract = $saleContract[$lang_string];
?>
