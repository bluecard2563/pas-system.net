<?php
/*
File: pPrintAdendumPDF.php
Author: David Rosales / Luis Salvador
Creation Date: 29/10/2015
Last modified:
Modified By: 
*/

/************************************ COMMON VARIABLE INITIALIZATION ********************************/
Request::setString('0:serial_sal');
Request::setString('1:printType');
Request::setString('2:printLanguage');
//if travel register get serial_sal
if($serial_generated_sale){
		$serial_sal = $serial_generated_sale;
}

$sale = new Sales($db,$serial_sal);
$sale -> getData();
$langCode = 2;

//GET LANGUAGE CODE FOR LABEL'S TEXT

if($printLanguage) $langCode = $printLanguage;
$lang_string = new Language($db, $langCode);
$lang_string ->getData();
$lang_string = $lang_string ->getCode_lang();

if($sale){
    $planetassist_sale = Sales::isPlanetAssistSale($db, $sale -> getSerial_sal());

    if($planetassist_sale){
        $header_logo = 'logopas_contracts.jpg';
    }else{
        $header_logo = 'logoblue_contracts.jpg';
    }
}else{
    $header_logo = 'logopas_contracts.jpg';
}

//Include Adendum texts
include("modules/sales/contractTemplates/contractTexts/adendum_text_pas.php");

//Gathering all the info:
global $date;
$dataInfo = $sale -> getSaleProduct($langCode);
$serialCus = $sale -> getSerial_cus();
$dataBen = $sale -> getSaleBenefits($langCode);
$dataServ = $sale -> getSaleServices($langCode);
$serialRep = $sale -> getSerial_rep();
//debug::print_r($sale); die();	

$pConditions = PConditionsByProductCountry::getPConditionsBySale($db, $serial_sal, $langCode);

//include_once DOCUMENT_ROOT.'lib/qrLibrary/qrlib.php';
if($dataInfo['flights_pro']==0){
		$serial_sal = $serial_generated_sale;
                $tripDays = GlobalFunctions::getDaysDiffDdMmYyyy($tripBeginDate, $tripEndDate);
                //trip_date
                $tripBeginDate = GlobalFunctions::strToTimeDdMmYyyy($tripBeginDate);
                $begintripDay = date("d", $tripBeginDate);
                $begintripMonth = date("m", $tripBeginDate);
                $begintripYear = date("y", $tripBeginDate);
                //end trip date
                $tripEndDate = GlobalFunctions::strToTimeDdMmYyyy($tripEndDate);
                $endtripDay = date("d", $tripEndDate);
                $endtripMonth = date("m", $tripEndDate);
                $endtripYear = date("y", $tripEndDate);
                
		$dataInfo['country_sal'] = $tripCountry;
		$dataInfo['city_sal'] = $tripCity;
	}

/*********** FORMAT PRICE NUMBERS *************/
$dataInfo['total_sal'] = str_replace(",", "", $dataInfo['total_sal']);
$dataInfo['fee_sal'] = str_replace(",", "", $dataInfo['fee_sal']);
$emissionDate = GlobalFunctions::strToTimeDdMmYyyy($dataInfo['emission_date_sal']);
$emissionDay = date("d", $emissionDate);
$emissionMonth = date("m", $emissionDate);
$emissionYear = date("y", $emissionDate);
$beginDate = GlobalFunctions::strToTimeDdMmYyyy($dataInfo['begin_date_sal']);
$endDate = GlobalFunctions::strToTimeDdMmYyyy($dataInfo['end_date_sal']);

//begin_date
$beginDay = date("d", $beginDate);
$beginMonth = date("m", $beginDate);
$beginYear = date("y", $beginDate);
//end date
$endDay = date("d", $endDate);
$endMonth = date("m", $endDate);
$endYear = date("y", $endDate);

$CoverageDays = GlobalFunctions::getDaysDiffDdMmYyyy($dataInfo['begin_date_sal'], $dataInfo['end_date_sal']);

$date_today=$dataInfo[name_cit].", ".$date[day]." / ".$date[month]." / ".$date[year];

if($serialSal) $serial_sal = $serialSal;
if($serial_generated_sale) $serial_sal = $serial_generated_sale;//Captured from invoking file

if($printType) $display = $printType;

if(!$traveler_log_customer){
		$customer = new Customer($db, $serialCus);
	}else{
		$customer = new Customer($db, $traveler_log_customer);
	}
	$customer -> getData();
        $customer = get_object_vars($customer);
	unset($customer['db']);
	$customer['full_address']=Customer::getGeographicalCustomerAddress($db, $customer['serial_cus']);
	$customer['full_address']=$customer['full_address']['name_cou'].' / '.$customer['full_address']['name_cit'].' / '.$customer['full_address']['address_cus'];
	
	$extra = new Extras($db,$serial_sal);
	$dataExt = Extras::getExtrasBySale($db, $serial_sal);
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext += $dE['fee_ext'];
		}
	}
//Debug::print_r($dataInfo);    die();
$card_number_sale='';
//Ejecutivas
        if($traveler_log_cardnumber){ //CARD_NUMBER FROM A TRAVELER_LOG
		$card_number_sale= $traveler_log_cardnumber;
        }else{
            $card_number_sale= $dataInfo['card_number_sal'];
        }
        
//Representative Information
if($serialRep){
	$representative = new Customer($db, $serialRep);
        $representative-> getData();
        $serial_rep = $representative ->getSerial_cus();
        $representative= get_object_vars($representative);
        
}else{
	$representative = NULL;
        
}

/************************************ CONTRACT CREATION PDF *****************************************/
set_time_limit(36000);
ini_set('memory_limit','512M');

//Global Variables
global $smallTextSize;
global $medTextSize;
global $stdTextSize;
global $largeTextSize;
global $titleTextSize;

//Colors:
global $blue;
global $lightBlue;
global $refText;
global $red;
global $black;
global $white;

global $refLines;
global $rimacBckGnd;
global $lightGray;

class MYPDFADENDUM extends TCPDF {

    //Page header
    public function Header() {
        
    }

    // Page footer
    public function Footer() {
        // Position at 20 mm from bottom
        $this->SetY(-20);
        // Page number
        $this->Cell(0, 15, $this->getAliasNumPage().' de '.$this->getAliasNbPages().'     ', 0, 0, 'R', 0, '', 0, false, 'T', 'M');
    }
}


// create new PDF document
$pdf = new MYPDFADENDUM(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set default header data
//$pdf->SetHeaderData($header_logo, PDF_HEADER_LOGO_WIDTH, '', '', array(0,84,163));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 10, 10);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);

//CONTENIDO DESDE BASE

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BLUE CARD S.A.');
$pdf->SetTitle('ADENDUM DE PRESTACION DE SERVICIOS DE SALUD Y MEDICINA PREPAGADA');
$pdf->SetSubject('ASISTENCIA MEDICA EMERGENTE INDIVIDUAL');
$pdf->SetKeywords('PDF, CONTRATO, ASISTENCIA');


//ADENDUM BODY

/* GETTING NATIONAL/INTERNATIONAL KIND OF TRAVEL */
if($serial_trl):
    $international_flight = Sales::internationalFlight($db, $serial_sal, $serial_trl);
else:
    $international_flight = Sales::internationalFlight($db, $serial_sal);
endif;


//HEADING
$pdf->Image(DOCUMENT_ROOT.'img/contract_logos/logoblue_contracts.jpg', 140, 7, 50, 0, 'JPG', '', '', true, 150, 'right', false, false, 0, false, false, false);


//$html = file_get_contents(DOCUMENT_ROOT.'/smarty/templates/modules/sales/table.xhtml');
// output the HTML content
//$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->Ln();

$html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
        <style>
           
            table.first {
                font-family: helvetica;
                font-size: 11px;
				line-height: 13px;
            }
			.blue_lbl{
				color: #00438A;	
			}
            .border_bot{
                border-bottom-width: 0.1px
			}
            .border_lft{
                border-left-width: 0.1px;
            }
			.border_rgt{
                border-right-width: 0.1px; 
			}
			.row_title{
				background-color: #00438A;	
				color: #FFFFFF;	
			}
             .reduced_text{
                    font-family: helvetica;
                    font-size: 9px;
                    
			}
             .benefit_text{
                    font-family: helvetica;
                    font-size: 10px;
                    
			}           
        </style>
        <table class="first" cellpadding="2" cellspacing="0">
           <tr>
                <td colspan="11" align="left">{$date_today}</td>
            </tr>
            <tr>
                <td align="left" class="blue_lbl"><b>Sucursal:</b></td>
                <td colspan="10" align="left" class="second">{$dataInfo['name_dea']}</td>
            </tr>
            <tr>
                <td align="left" class="blue_lbl"><b>Counter:</b></td>
                <td colspan="10" align="left" class="second">{$dataInfo['counter']}</td>
            </tr>
            <tr>
                <td width="145" align="left"><b>Lugar y Fecha de emisión</b></td>
                <td width="70" align="center" class="border_bot">{$dataInfo['name_cit']}</td>
                <td width="25" align="center" class="border_bot border_rgt">$emissionDay</td>
                <td width="25" align="center" class="border_bot border_rgt">$emissionMonth</td>
                <td width="25" align="center" class="border_bot border_rgt">$emissionYear</td>
                <td colspan="2" align="right"><b>COD. SUC:</b></td>
                <td width="105" align="center" class="border_bot border_rgt">{$dataInfo['code']}</td>
                <td width="20"></td>
                <td width="80" align="right"><b>Adendum N:</b></td>
                <td width="60" align="center" class="border_bot border_rgt">$card_number_sale</td>
            </tr>
             <tr>
                <td width="145" align="left"></td>
                <td width="70" align="center"></td>
                <td width="25" align="center"><b>dd</b></td>
                <td width="25" align="center"><b>mm</b></td>
                <td width="25" align="center"><b>yy</b></td>
                <td colspan="2" align="right"></td>
                <td width="90" align="center"></td>
                <td width="20"></td>
                <td width="80" align="right"></td>
                <td width="60" align="center"></td>
            </tr>
         
EOF;
             
if($serial_rep){
$html .= <<<EOF
            <tr>                       
            
                <td colspan="11" class="row_title">   <b>DATOS DEL REPRESENTANTE</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td align="center" class="border_lft border_bot border_rgt">{$representative['document_cus']}</td>
                <td colspan="4" align="center" class="border_bot border_rgt">{$representative['first_name_cus']}</td>
                <td colspan="3" align="center" class="border_bot border_rgt">{$representative['last_name_cus']}</td>
                <td colspan="3" align="center" class="border_bot border_rgt">{$representative['birthdate_cus']}</td>
            </tr>
            <tr>
                <td align="center"><b>C.I / Pasaporte</b></td>
                <td colspan="4" align="center"><b>Apellidos</b></td>
                <td colspan="3" align="center"><b>Nombres</b></td>
                <td colspan="3" align="center"><b>Fecha de Nacimiento</b></td>
            </tr>
            <tr>
            
                <td colspan="11" align="center" class="border_bot border_rgt">{$representative['email_cus']}</td>
            </tr>
            <tr>
                <td colspan="11" align="center"><b>Email</b></td>
            </tr>'
EOF;
                
}

$html .= <<<EOF
         <tr>
           <td colspan="11" class="row_title">   <b>DATOS DEL BENEFICIARIO</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td align="center" class="border_lft border_bot border_rgt">{$customer['document_cus']}</td>
                <td colspan="4" align="center" class="border_bot border_rgt">{$customer['first_name_cus']}</td>
                <td colspan="3" align="center" class="border_bot border_rgt">{$customer['last_name_cus']}</td>
                <td colspan="3" align="center" class="border_bot border_rgt">{$customer['birthdate_cus']}</td>
            </tr>
            <tr>
                <td align="center"><b>C.I / Pasaporte</b></td>
                <td colspan="4" align="center"><b>Apellidos</b></td>
                <td colspan="3" align="center"><b>Nombres</b></td>
                <td colspan="3" align="center"><b>Fecha de Nacimiento</b></td>
            </tr>
            <tr>
                <td align="center" class="border_lft border_bot border_rgt">{$customer['phone1_cus']}</td>
                <td colspan="5" align="center" class="border_bot border_rgt">{$customer['email_cus']}</td>
                <td colspan="5" align="center" class="border_bot border_rgt">{$customer['full_address']}</td>
            </tr>
            <tr>
                <td align="center"><b>Teléfono</b></td>
                <td colspan="5" align="center"><b>Email</b></td>
                <td colspan="5" align="center"><b>País / Ciudad / Dirección</b></td>
            </tr>
            <tr>
                <td colspan="2" width="200" align="left"><b>En caso de emergencia notificar a:</b></td>
                <td colspan="4" width="200" align="center" class="border_bot border_rgt">{$customer['relative_cus']}</td>
                <td colspan="3" width="130" align="right"><b>Teléfono:</b></td>
                <td colspan="2" width="130" align="center" class="border_bot border_rgt">{$customer['relative_phone_cus']}</td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>'
                
EOF;
//***************** SECTION: EXTRAS ***************************************
	
//Extras section
if(count($dataExt)>0){
    $extras_html = '';
    $extras_html= '<tr>
                        <td colspan="11" class="row_title">   <b>DATOS DE ADICIONALES</b></td>
                    </tr>
                    <tr>
                        <td colspan="11"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><b>C.I / Pasaporte</b></td>
                        <td colspan="6" align="center"><b>Nombres</b></td>
                        <td colspan="1" align="center"><b>Edad</b></td>
                        <td colspan="2" align="center"><b>Tarifa USD</b></td>
                    </tr>';
    foreach($dataExt as $keyex => $dataex ){   
        $fullNameext = $dataex['first_name_cus'] . ' ' . $dataex['last_name_cus'];
        list($day, $month, $year) = explode("/", $dataex['birthdate_cus']);
        $dataex['birthdate_cus'] = $year . '/' . $month . '/' . $day;
        $ageext = GlobalFunctions::calculateAge($dataex['birthdate_cus']);
        $extras_html.= '<tr>
                            <td colspan="2" align="center" class="border_lft border_bot border_rgt">'.utf8_encode($dataex['document_cus']).'</td>
                            <td colspan="6" align="center" class="border_bot border_rgt">'.utf8_encode($fullNameext).'</td>
                            <td colspan="1" align="center" class="border_bot border_rgt">'.$ageext.'</td>
                            <td colspan="2" align="center" class="border_bot border_rgt">'.utf8_encode($dataex['fee_ext']).'</td>
                        </tr>';
        }
        $extras_html.= '<tr>
                            <td colspan="11"></td>
                        </tr>';
$html .= $extras_html;     
}
//debug::print_r($html); die();
if($dataInfo['flights_pro']==0){
$html .= <<<EOF
            <tr>
                <td colspan="11" class="row_title">   <b>DATOS DEL VIAJE</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td width="170" align="center" class="border_lft border_bot border_rgt">{$dataInfo['country_sal']}</td>
                <td width="100" align="right"><b>Fecha de Salida:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$begintripDay</td>
                <td width="30" align="center" class="border_bot border_rgt">$begintripMonth</td>
                <td width="30" align="center" class="border_bot border_rgt">$begintripYear</td>
                <td width="110" align="right"><b>Fecha de Regreso:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$endtripDay</td>
                <td width="30" align="center" class="border_bot border_rgt">$endtripMonth</td>
                <td width="30" align="center" class="border_bot border_rgt">$endtripYear</td>
                <td width="70" align="right"><b>Duración:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$tripDays</td>
            </tr>
            <tr>
                <td width="170" align="center"><b>Destino</b></td>
                <td width="100" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
                <td width="30" align="center"><b>mm</b></td>
                <td width="30" align="center"><b>yy</b></td>
                <td width="110" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
                <td width="30" align="center"><b>mm</b></td>
                <td width="30" align="center"><b>yy</b></td>
                <td width="70" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" class="row_title">   <b>COBERTURA DEL PLAN</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                 <td width="10" align="center"></td>
                <td width="180" align="right"><b>Fecha de Inicio de Cobertura:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$beginDay</td>
                <td width="30" align="center" class="border_bot border_rgt">$beginMonth</td>
                <td width="30" align="center" class="border_bot border_rgt">$beginYear</td>
                <td width="180" align="right"><b>Fecha de Fin de Cobertura:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$endDay</td>
                <td width="30" align="center" class="border_bot border_rgt">$endMonth</td>
                <td width="30" align="center" class="border_bot border_rgt">$endYear</td>
                <td width="80" align="right"><b>Duración:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$CoverageDays</td>
            </tr>
            <tr>
                <td width="10" align="center"><b></b></td>
                <td width="180" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
                <td width="30" align="center"><b>mm</b></td>
                <td width="30" align="center"><b>yy</b></td>
                <td width="180" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
                <td width="30" align="center"><b>mm</b></td>
                <td width="30" align="center"><b>yy</b></td>
                <td width="80" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
EOF;
                
}
else{
$html .= <<<EOF
            <tr>
                <td colspan="11" class="row_title">   <b>DATOS DEL VIAJE</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td width="170" align="center" class="border_lft border_bot border_rgt">{$dataInfo['country_sal']}</td>
                <td width="100" align="right"><b>Fecha de Salida:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$beginDay</td>
                <td width="30" align="center" class="border_bot border_rgt">$beginMonth</td>
                <td width="30" align="center" class="border_bot border_rgt">$beginYear</td>
                <td width="110" align="right"><b>Fecha de Regreso:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$endDay</td>
                <td width="30" align="center" class="border_bot border_rgt">$endMonth</td>
                <td width="30" align="center" class="border_bot border_rgt">$endYear</td>
                <td width="70" align="right"><b>Duración:</b></td>
                <td width="30" align="center" class="border_bot border_rgt">$CoverageDays</td>
            </tr>
            <tr>
                <td width="170" align="center"><b>Destino</b></td>
                <td width="100" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
                <td width="30" align="center"><b>mm</b></td>
                <td width="30" align="center"><b>yy</b></td>
                <td width="110" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
                <td width="30" align="center"><b>mm</b></td>
                <td width="30" align="center"><b>yy</b></td>
                <td width="70" align="right"></td>
                <td width="30" align="center"><b>dd</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>    
   
EOF;
}                
$html .= <<<EOF
            <tr>
                <td colspan="11" class="row_title">   <b>DATOS DEL PLAN CONTRATADO</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td width="130" align="left"><b>Nombre del Producto:</b></td>
                <td width="130" align="center" class="border_bot border_rgt">{$dataInfo['name_pbl']}</td>
                <td width="150" colspan="4" align="right"><b>TARIFA TITULAR USD:</b></td>
                <td width="50" align="center" class="border_bot border_rgt">{$dataInfo['fee_sal']}</td>
                <td width="150" colspan="3" align="right"><b>TOTAL TARIFA USD:</b></td>
                <td width="50" align="right" class="border_bot border_rgt">{$dataInfo['total_sal']}</td>
            </tr>
            <!--SECCION BENEFICIOS -->
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" class="row_title">   <b>BENEFICIOS</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <!--Beneficios Médicos -->
            <tr>
                <td width="40"></td>
                <td colspan="7" width="450" align="Left"><b>Beneficios Médicos</b></td>
                <td width="110" align="center" ><b>Cobertura</b></td>
               <td width="30"></td>
            </tr>
                
EOF;
                

//ARREGLO DE BENEFICIOS POR CATEGORIA                
$benefits = '';
$dataBenMed = array();
$dataBenTec = array();
$dataBenInfo = array();
//debug::print_r($dataBen);
foreach($dataBen as $key => $datab ){
    if ($datab['serial_bcat'] == 1){
        array_push($dataBenMed, $datab);
    }else{
        if ($datab['serial_bcat'] == 2){
          array_push($dataBenTec, $datab);
        }else{
            if ($datab['serial_bcat'] == 3){
            array_push($dataBenInfo, $datab);
        }else{
            //nada
        }
        }
    }
    
}
//MEDICOS

foreach($dataBenMed as $keym => $databm ){    
       if($dataBenMed['price_bxp'] == 0){
	      $coverageValuem = $databm['alias_con'];
        }else{
              $coverageValuem = $databm['price_bxp'].'  ('.$databm['restriction_price_bxp'].' x '.$databm['description_rbl'].')';
	}
$benefitsm .= '<tr class="benefit_text">
                <td width="40"></td>
                 <td width="30"></td>
                <td colspan="6" width="420" align="left">'.utf8_encode($databm['description_bbl']).'</td>
                <td width="110" align="center" >'.utf8_encode($coverageValuem).'</td>
               <td width="30"></td>
            </tr>';
    }
$html .= $benefitsm; 
                
$html .= <<<EOF

            <!--Beneficios Técnicos -->
            <tr>
                <td width="40"></td>
                <td colspan="7" width="450" align="Left"><b>Beneficios Técnicos</b></td>
                <td width="110" align="center" ><b>Cobertura</b></td>
                <td width="30"></td>
            </tr>
EOF;

//TECNICOS
foreach($dataBenTec as $keyt => $databt ){    
       
        if($dataBenTec['price_bxp'] == 0){
	      $coverageValuet = $databt['alias_con'];
        }else{
              $coverageValuet = $databt['price_bxp'].'  ('.$databt['restriction_price_bxp'].' x '.$databt['description_rbl'].')';
	}
$benefitst .= '<tr class="benefit_text">
                <td width="40"></td>
                 <td width="30"></td>
                <td colspan="6" width="420" align="left">'.utf8_encode($databt['description_bbl']).'</td>
                <td width="110" align="center" >'.utf8_encode($coverageValuet).'</td>
               <td width="30"></td>
            </tr>';
       
       
   }
  
$html .= $benefitst; 
//debug::print_r($databt); die ();
$html .= <<<EOF

            <!--Beneficios Informativos -->
            <tr>
                <td width="40"></td>
                <td colspan="7" width="450" align="Left"><b>Beneficios Informativos</b></td>
                <td width="110" align="center" ><b>Cobertura</b></td>
                <td width="30"></td>
            </tr>
EOF;
//INFORMATIVOS
foreach($dataBenInfo as $keyi => $databi ){    
       
        if($dataBenInfo['price_bxp'] == 0){
	      $coverageValuei = $databi['alias_con'];
        }else{
              $coverageValuei = $databi['price_bxp'].'  ('.$databi['restriction_price_bxp'].' x '.$databi['description_rbl'].')';
	}
$benefitsi .= '<tr class="benefit_text">
                <td width="40"></td>
                 <td width="30"></td>
                <td colspan="6" width="420" align="left">'.utf8_encode($databi['description_bbl']).'</td>
                <td width="110" align="center" >'.utf8_encode($coverageValuei).'</td>
               <td width="30"></td>
            </tr>';
       
       
   }
$html .= $benefitsi; 
           
$html .= <<<EOF
            
            <!--SECCION INSTRUCCIONES DE USO -->
            <tr>
                <td colspan="11" width="660"></td>
            </tr>
            <tr>
                <td colspan="11" class="row_title">   <b>INSTRUCCIONES DE USO</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" align="justify">Para facilitar el uso del servicio de Asistencia en viajes, usted deberá comunicarse SIEMPRE, con nuestra Central
                    de Asistencias, a través de los números telefónicos, correo electrónico o skype.<br/><br/>
                    Si la urgencia de la asistencia, le impide comunicarse de forma inmediata y previo al uso de
                    cualquier tipo de servicio, usted deberáar aviso a nuestra Central de Asistencias dentro de las 24 horas
                    siguientes a la ocurrencia del evento.<br/><br/>
                    Nuestros operadores coordinarán la presentación adecuada de todos los servicios de asistencia
                    que requiera en el país en el que se encuentre.<br/>
                </td>
            </tr>
            
            <!--SECCION TELÉFONOS DE ASISTENCIA -->
            
            <tr>
                <td colspan="11" class="row_title"><b>{$textES['phones']['title']}</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <!--SECCION Lineas -->
            <tr><!--Cabecera -->
                <td colspan="5" class="row_title">   <b>Líneas Locales</b></td>
                <td colspan="1" >   </td>
                <td colspan="5" class="row_title">   <b>Líneas Internacionales</b></td>
            </tr>
            
            <tr> <!--Frase Cabecera -->
                <td colspan="5" align="center" class="reduced_text"><b>Líneas Gratuitas únicamente para llamada Local</b></td>
                <td colspan="1" >   </td>
                <td colspan="5" align="center" class="reduced_text"><b>Líneas que puede marcar desde cualquier lugar del mundo</b></td>
            </tr>
            <!--NUMEROS TELEFONICOS -->
            <tr class="reduced_text"> <!--Fila1 -->
                <td width="70" align="left">Argentina</td>
                <td width="80" align="right">0800-666-1599</td>
                <td width="5" >   </td>
                <td width="70" align="left">México</td>
                <td width="80" align="right">001866-600-8936</td>
                <td width="50" >   </td>
                <td width="60" align="left">Alemania</td>
                <td width="90" align="right">(+)49-69-2222-7544</td>
                <td width="5" >   </td>
                <td width="60" align="left">Italia</td>
                <td width="90" align="right">(+)39-06-983-54343</td>
            </tr><!--FIN Fila 1 -->
            <tr class="reduced_text"> <!--Fila2 -->
                <td width="70" align="left">Brasil</td>
                <td width="80" align="right">0800-891-0199</td>
                <td width="5" >   </td>
                <td width="70" align="left">Perú</td>
                <td width="80" align="right">0800-52173</td>
                <td width="50" >   </td>
                <td width="60" align="left">Argentina</td>
                <td width="90" align="right">(+)54-11-684-1909</td>
                <td width="5" >   </td>
                <td width="60" align="left">Japón</td>
                <td width="90" align="right">(+)81-3-452-08917</td>
            </tr><!--FIN Fila 2 -->
            <tr class="reduced_text"> <!--Fila3 -->
                <td width="70" align="left">Chile</td>
                <td width="80" align="right">1230-020-2396</td>
                <td width="5" >   </td>
                <td width="80" align="left">Rep. Dominicana</td>
                <td width="70" align="right">1888-751-2280</td>
                <td width="50" >   </td>
                <td width="60" align="left">Australia</td>
                <td width="90" align="right">(+)61-29-119-2941</td>
                <td width="5" >   </td>
                <td width="60" align="left">México</td>
                <td width="90" align="right">(+)52-55-132-81928</td>
            </tr><!--FIN Fila 3 -->
            <tr class="reduced_text"> <!--Fila4 -->
                <td width="70" align="left">Colombia</td>
                <td width="80" align="right">01800-915-5913</td>
                <td width="5" >   </td>
                <td width="70" align="left">USA-Canada</td>
                <td width="80" align="right">1800-504-0027</td>
                <td width="50" >   </td>
                <td width="60" align="left">Brasil</td>
                <td width="90" align="right">(+)55-21-352-19188</td>
                <td width="5" >   </td>
                <td width="60" align="left">Perú</td>
                <td width="90" align="right">(+)51-1-718-5156</td>
            </tr><!--FIN Fila 4 -->
            <tr class="reduced_text"> <!--Fila5 -->
                <td width="70" align="left">España</td>
                <td width="80" align="right">900-941-988</td>
                <td width="5" >   </td>
                <td width="70" align="left">Venezuela</td>
                <td width="80" align="right">0800-100-2453</td>
                <td width="50" >   </td>
                <td width="60" align="left">Chile</td>
                <td width="90" align="right">(+)59-2-2760-9099</td>
                <td width="5" >   </td>
                <td width="60" align="left">Puerto Rico</td>
                <td width="90" align="right">(+)1787-304-3043</td>
            </tr><!--FIN Fila 5 -->
            <tr class="reduced_text"> <!--Fila6 -->
                <td width="70" align="left"></td>
                <td width="80" align="right"></td>
                <td width="5" >   </td>
                <td width="70" align="left"></td>
                <td width="80" align="right"></td>
                <td width="50" >   </td>
                <td width="60" align="left">China</td>
                <td width="90" align="right">(+)86-10-840-53610</td>
                <td width="5" >   </td>
                <td width="60" align="left">Reino Unido</td>
                <td width="90" align="right">(+)44-20-302-62071</td>
            </tr><!--FIN Fila 6 -->
            <tr class="reduced_text"> <!--Fila7 -->
                <td width="305" align="left"><b>Skype:</b> asistenciasplanet-assist1 (unicamente para mensajes escritos)</td>
                <td width="50" >   </td>
                <td width="60" align="left">España</td>
                <td width="90" align="right">(+)34-91414-2281</td>
                <td width="5" >   </td>
                <td width="60" align="left">Suiza</td>
                <td width="90" align="right">(+)41-22-533-0959</td>
            </tr><!--FIN Fila 7 -->
            <tr class="reduced_text"> <!--Fila8 -->
                <td width="305" align="left"><b>Skype:</b> asistencias-planet (unicamente para llamadas)</td>
                <td width="50" >   </td>
                <td width="60" align="left">Ecuador</td>
                <td width="90" align="right">(+)593-2-3814-790</td>
                <td width="5" >   </td>
                <td width="60" align="left">USA</td>
                <td width="90" align="right">(+)1-305-644-8435</td>
            </tr><!--FIN Fila 8 -->
            <tr class="reduced_text"> <!--Fila9 -->
                <td width="305" align="left"><b>Correo Electrónico:</b> asistencias@planet-assist.net</td>
                <td width="50" >   </td>
                <td width="60" align="left">Francia</td>
                <td width="90" align="right">(+)33-1-706-13000</td>
                <td width="5" >   </td>
                <td width="60" align="left">Holanda</td>
                <td width="90" align="right">(+)31-20-708-4091</td>
            </tr><!--FIN Fila 9 -->
            
            <!--SECCION CONDICIONES GENERALES -->
            <tr>
                <td colspan="11" width="660"></td>
            </tr>
            <tr>
                <td colspan="11" class="row_title">   <b>CONDICIONES GENERALES</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" align="justify">El cliente reconoce y acepta darse por enterado de todas las cláusulas detalladas en las Condiciones Generales
                        de BLUE CARD, las mismas que han sido entregadas físicamente, enviadas mediante correo electrónico o
                        también pueden ser descargadas desde la página web www.bluecard.com.ec<br/><br/>
                        El presente Adendum al Contrato de Medicina Prepagada, se regirá bajo las Condiciones Generales y Particulares
                        de BLUE CARD vigentes al momento de su emisión, independientemente de que estas sean anuladas,
                        modificadas o añadidas.<br/>
                </td>
            </tr>
            
            <!--SECCION EXCLUSIONES -->
            
            <tr>
                <td colspan="11" class="row_title">   <b>EXCLUSIONES</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" align="justify">El cliente reconoce y acepta darse por enterado de todas las exclusiones detalladas en las Condiciones Generales
                    de BLUE CARD, las mismas que han sido enviadas mediante correo electrónico y/o entregadas fisicamente,
                    conforme lo señala la clausula Décima Primera.<br/>
                </td>
            </tr>
            
            <!--SECCION DESCRIPCION DEL PRODUCTO -->
            
            <tr>
                <td colspan="11" class="row_title">   <b>DESCRIPCION DEL PRODUCTO</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" align="justify">Aplica según producto<br/>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum
                        sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
                        pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                        vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
                        mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper.<br/>
                </td>
            </tr>
            
            <!--SECCION CONDICIONES PARTICULARES -->
            
            <tr>
                <td colspan="11" class="row_title">   <b>CONDICIONES PARTICULARES</b></td>
            </tr>
            <tr>
                <td colspan="11"></td>
            </tr>
            <tr>
                <td colspan="11" align="justify">Aplica según producto<br/>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum
                        sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
                        pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                        vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
                        mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper.<br/>
                </td>
            </tr>
        </table>
EOF;


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln();


if($display=='1'){
    //Close and output PDF document
    $pdf->Output('adendum_'.$serial_con.'.pdf', 'I');
}else{
//    $pdf->Output(DOCUMENT_ROOT .'modules/sales/contracts/adendum_'.$serial_sal.'.pdf', 'F');
}