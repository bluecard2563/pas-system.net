<?php
/**
 * File: contract_diners
 * Author: Patricio Astudillo
 * Creation Date: 22-feb-2013
 * Last Modified: 22-feb-2013
 * Modified By: Patricio Astudillo
 */

	//GET LANGUAGE CODE FOR LABEL'S TEXT
	$lang_string = new Language($db, $langCode);
	$lang_string ->getData();
	$lang_string = $lang_string ->getCode_lang();

	$sale = new Sales($db,$serial_sal);
	$sale -> getData();
	
	include("modules/sales/contractTemplates/contractTexts/text_diners.php");
	
	$pdf -> addPngFromFile(DOCUMENT_ROOT."img/contract_logos/logo_diners.png", 375, 774, 170);
	$pdf -> addPngFromFile(DOCUMENT_ROOT."img/contract_logos/logo_amsiss.png", 30, 760, 100);
	
	
	//Gathering all the info:
	$dataInfo = $sale -> getSaleProduct($langCode);
	$dataBen = $saleContract['benefits']['data'];
	$serialCus = $sale -> getSerial_cus();
	
	/*********** FORMAT PRICE NUMBERS *************/
	$dataInfo['total_sal'] = str_replace(",", "", $dataInfo['total_sal']);
    $dataInfo['fee_sal'] = str_replace(",", "", $dataInfo['fee_sal']);
	if(!$emissionDate):
		$emissionDate = GlobalFunctions::strToTimeDdMmYyyy($dataInfo['emission_date_sal']);
	endif;
	
	/*********** ASISTANCE PHONES *****************/
	$assistance_phones = $saleContract['assistance_phones'];
	/*********** ASISTANCE PHONES *****************/

	if(!$traveler_log_customer){
		$customer = new Customer($db, $serialCus);
	}else{
		$customer = new Customer($db, $traveler_log_customer);
	}
	$customer -> getData();
	$customer = get_object_vars($customer);
	unset($customer['db']);
	$customer['full_address']=Customer::getGeographicalCustomerAddress($db, $customer['serial_cus']);
	$customer['full_address']=$customer['full_address']['name_cou'].' / '.$customer['full_address']['name_cit'].' / '.$customer['full_address']['address_cus'];
	
	$extra = new Extras($db,$serial_sal);
	$dataExt = Extras::getExtrasBySale($db, $serial_sal);
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext += $dE['fee_ext'];
		}
	}
	$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );
	
//	Debug::print_r($dataInfo); die;
//	Debug::print_r($dataBen);
//	Debug::print_r($assistance_phones);
//	Debug::print_r($serialCus);
//	Debug::print_r($customer);
//	Debug::print_r($dataExt);
//	Debug::print_r($fee_ext); die;
	
	//Header 1
	$verticalPointer -= 60;
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf->addTextWrap($leftMostX, $verticalPointer, 350, $largeTextSize, $saleContract['date']['legend']);
	$pdf->setColor($black[0], $black[1], $black[2]);
	$pdf->addTextWrap($leftMostX + 108, $verticalPointer, 350, $largeTextSize, 'Lima'); //Changed by Request of RIMAC
	$pdf->addTextWrap($leftMostX + 153, $verticalPointer, 100, $largeTextSize, date("d", $emissionDate).'    '.date("m", $emissionDate).'   '.date("Y", $emissionDate));
	
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> line($leftMostX + 105, $verticalPointer-4, $leftMostX + 145, $verticalPointer-4);
	$pdf -> line($leftMostX + 153, $verticalPointer-4, $leftMostX + 163, $verticalPointer-4);
	$pdf -> line($leftMostX + 170, $verticalPointer-4, $leftMostX + 180, $verticalPointer-4);
	$pdf -> line($leftMostX + 187, $verticalPointer-4, $leftMostX + 210, $verticalPointer-4);
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap($leftMostX + 153, $verticalPointer-12, 500, $smallTextSize,$saleContract['date']['labels']);
	
	//***********************************************SECTION 1: CLIENT DATA ********************************************************
	$parameter = new Parameter($db);
	$parameter -> setSerial_par('13');
	$parameter -> getData();
	
	//CLIENT DATA TITLE
	$verticalPointer -= 50;
	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['client']['title'].'</b>');//Client data title

	//MAIN SECTION
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setStrokeColor($refText[0],$refText[1],$refText[2]);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Last name
	$tempX = $leftMostX;
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['last_name_cus'], 150);
	$pdf -> line($tempX,$verticalPointer-8,$tempX + 150,$verticalPointer-8);
	
	//First name
	$tempX += 180;
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['first_name_cus'], 150);
	$pdf -> line($tempX, $verticalPointer-8, $tempX + 150, $verticalPointer-8);
	
	//Document
	$tempX += 180;
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['document_cus'],'center');//document
	$pdf -> line($tempX, $verticalPointer-8, $tempX + 150, $verticalPointer-8);
	
	//Bottom labels:
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract['client']['line_0']);
	
	
	//SECOND LINE
	$verticalPointer -= 35;
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Company
	$tempX = $leftMostX + 180;
	$pdf -> addTextWrap($tempX, $verticalPointer-5, 150, $stdTextSize, '', 'center');
	$pdf -> line($tempX, $verticalPointer-8, $tempX + 150, $verticalPointer-8);
	
	//Bottom labels:
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap($tempX, $verticalPointer-17, 150, $stdTextSize, ''.$saleContract['client']['line_1'], 'center');
	
	
	//THIRD LINE
	$verticalPointer -= 35;
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf->addTextWrap($leftMostX, $verticalPointer, 150, $largeTextSize, $saleContract['client']['emergency_label1']);
	$pdf->addTextWrap($leftMostX + 275, $verticalPointer, 100, $largeTextSize, $saleContract['client']['emergency_label2']);
	
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf->addTextWrap($leftMostX + 138, $verticalPointer, 100, $largeTextSize, $saleContract['client']['emergency_data1']);
	$pdf->addTextWrap($leftMostX + 318, $verticalPointer, 100, $largeTextSize, $saleContract['client']['emergency_data2']);
	$pdf -> line($leftMostX + 135, $verticalPointer-5, $leftMostX + 235, $verticalPointer-5);
	$pdf -> line($leftMostX + 318, $verticalPointer-5, $leftMostX + 400, $verticalPointer-5);
	
	
	//********************************************* SECTION 2: TRIP DATA **********************************************
	$verticalPointer -= 45;
	if($verticalPointer <= 100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$beginDate=''.$dataInfo['begin_date_sal'];
	$endDate=''.$dataInfo['end_date_sal'];
	if($dataInfo['flights_pro']==0){
		$beginDate = $tripBeginDate;
		$endDate = $tripEndDate;
		$dataInfo['country_sal'] = $tripCountry;
		$dataInfo['city_sal'] = $tripCity;
	}
	
	//Header line:
	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['trip']['title1'].'</b>');

	$pdf -> setLineStyle(0.2,'square');
	$tempX = $leftMostX;
	$verticalPointer -= 5;
	$pdf -> setStrokeColor($black[0],$black[1],$black[2]);

	//Top Legends
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer,528,$medTextSize,''.$saleContract['trip']['0']);
	$verticalPointer -= 10;

	//Departure Date
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX + 20, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $beginDate ) ) );
	$pdf -> line($tempX+15, $verticalPointer-2, $tempX + 80, $verticalPointer - 2);
	
	//End Date
	$tempX += 150;
	$pdf -> addTextWrap($tempX, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $endDate ) ) );
	$pdf -> line($tempX -2,$verticalPointer - 2, $tempX  + 70,$verticalPointer-2);

	//Duration:
	$tempX += 150;
	$fullDays = GlobalFunctions::getDaysDiffDdMmYyyy($beginDate, $endDate);
	$pdf -> addTextWrap($tempX, $verticalPointer, 30, $stdTextSize, $fullDays);
	$pdf -> line($tempX -5, $verticalPointer - 2, $tempX + 10,$verticalPointer - 2);

	//Destination
	$tempX += 70;
	$destinationString = $dataInfo['country_sal'];
	$destinationString .= $dataInfo['city_sal']== 'General'?'':' - ' . $dataInfo['city_sal'];
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer + 5, $stdTextSize, $destinationString, 150);
	$pdf -> line($tempX + 10 ,$verticalPointer-2, $tempX + 110,$verticalPointer-2);
		
	//Bottom Lines
	$pdf -> setColor($blue[0],$blue[1],$blue[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer-10,500,$smallTextSize,''.$saleContract['trip']['1']);
		
	//************************************************** SECTION 4: BENEFITS **********************************************
	$verticalPointer -= 35;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}
	
	//Getting the table's height
	$verticalSep = 10;
	$benefits_count = count($dataBen);
	$benefits_height = (int)(($benefits_count * $verticalSep) / 1.70);
	$pdf->setColor($refText[0],$refText[1],$refText[2]);
	$pdf->ezSetDy(-($max_height - $verticalPointer));
	$pdf->ezTable($dataBen,
				$saleContract['benefits']['titles'],
				'',
				array('showHeadings'=>1,
						'showLines'=> 2,
						'colGap' => 2,
						'shaded'=>2,
						'shadeCol' => array(0.95,0.97,0.98),
						'shadeCol2' => array(1,1,1),
						'xPos'=>'300',
						'xOrientation'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => $stdTextSize,
						'cols'=>array(
								'col_1'=>array('justification'=>'center','width'=>310),
								'col_2'=>array('justification'=>'center','width'=>150)
								)
				)
	);
	
	//************************************************** SECTION 7: ASSISTANCE PHONES **********************************************
	$verticalPointer -= $benefits_height;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf->ezSetDy(-20);
	$pdf->ezTable($saleContract['assistance_phones'],
				'',
				'',
				array('showHeadings'=>0,
						'showLines'=> 2,
						'colGap' => 2,
						'shaded'=>2,
						'shadeCol' => array(0.95,0.97,0.98),
						'shadeCol2' => array(1,1,1),
						'xPos'=>'300',
						'xOrientation'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => $stdTextSize,
						'titleFontSize' => $stdTextSize,
						'cols'=>array(
								'col_1'=>array('justification'=>'center','width'=>100),
								'col_2'=>array('justification'=>'center','width'=>100)
								)
				)
	);
	
	if($display=='1'){
		$pdf -> ezStream();
	}else{
		$pdfcode = $pdf -> ezOutput();
		if($multi_sale){
			$fp=fopen(DOCUMENT_ROOT .'modules/sales/multiSaleContracts/contract_'.$serial_sal.'.pdf','wb');
		}else{
			$fp=fopen(DOCUMENT_ROOT .'modules/sales/contracts/contract_'.$serial_sal.'.pdf','wb');
		}		
		fwrite($fp,$pdfcode);
		fclose($fp);
	}
?>
