<?php
/*
 * Document   : contract_rimac.php
 * Created on : May 19, 2010
 * Author     : Gabriela Guerrero & Santiago Borja
 * 
*/

	//GET LANGUAGE CODE FOR LABEL'S TEXT
	$lang_string = new Language($db, $langCode);
	$lang_string -> getData();
	$lang_string = $lang_string ->getCode_lang();
	
	include("modules/sales/contractTemplates/contractTexts/text_rimac.php");
	$gpf = new  GraphicPDFFunctions($pdf);
	
	
	//Gathering all the info:
	$sale = new Sales($db,$serial_sal);
	$sale -> getData();
	$dataInfo = $sale -> getSaleProduct($langCode);
	$dataBen = $sale -> getSaleBenefits($langCode);
	$dataServ = $sale -> getSaleServices($langCode);
	$serialCus = $sale -> getSerial_cus();
	

	$pdf -> addJpegFromFile(DOCUMENT_ROOT."img/contract_logos/logo_rimac.jpg", $leftMostX, $max_height - 40, 80);
	
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setColor($refLines[0], $refLines[1], $refLines[2]);
	$pdf -> line($leftMostX + 100, $max_height, $leftMostX + 100, $max_height - 30);
	
	
	//************************* CERTIFICATE *************************************//
	$verticalPointer = $max_height - 10;
	$gpf -> write_red_box($leftMostX + 120, $verticalPointer);
	$gpf -> write_title_box($contractTexts['title'], $leftMostX + 150, $verticalPointer, false);
	
	if($traveler_log_cardnumber){
		$number = $traveler_log_cardnumber;
	}else{
		if($dataInfo['card_number_sal'] != ''){ //REGULAR SALE
			$number = $dataInfo['card_number_sal'];
		}else{ //CORPORATIVE SALE
			$number = 'N/A';
		}
	}
	
	$beginDate = ''.$dataInfo['begin_date_sal'];
	$endDate = ''.$dataInfo['end_date_sal'];
	if($dataInfo['flights_pro']==0 && $tripBeginDate && $tripEndDate){
		$beginDate = $tripBeginDate;
		$endDate = $tripEndDate;
		$dataInfo['country_sal'] = $tripCountry;
		$dataInfo['city_sal'] = $tripCity;
	}
	$beginDate =  date('d/m/Y', GlobalFunctions::strToTimeDdMmYyyy($beginDate));
	$endDate = date('d/m/Y', GlobalFunctions::strToTimeDdMmYyyy($endDate));
	
	$verticalPointer -= 40;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['certificate']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);	
	$verticalPointer -= 15;
	$gpf -> write_single_column_left($contractTexts['certificate']['policy'], '', $verticalPointer);
	$gpf -> write_single_column_right($contractTexts['certificate']['certificate'], $number, $verticalPointer);
	$verticalPointer -= 10;
	$gpf -> write_single_column_left($contractTexts['certificate']['code'], 'AE0506400303', $verticalPointer);
	$gpf -> write_single_column_right($contractTexts['certificate']['begins'], $beginDate, $verticalPointer);
	$verticalPointer -= 10;
	$gpf -> write_single_column_left($contractTexts['certificate']['product'], $dataInfo['name_pbl'], $verticalPointer);
	$gpf -> write_single_column_right($contractTexts['certificate']['ends'], $endDate, $verticalPointer);
	
	$gpf -> write_box($verticalPointer);
	
	
	//************************* CUSTOMER INFO *************************************//
	$contractor = new Customer($db, $serialCus);
	$contractor -> getData();
	$contractor = get_object_vars($contractor);
	unset($contractor['db']);
	$contractor['full_address'] = Customer::getGeographicalCustomerAddress($db, $contractor['serial_cus']);
	$contractor['full_address'] = $contractor['full_address']['name_cou'].' / '.$contractor['full_address']['name_cit'].' / '.$contractor['full_address']['address_cus'];
	
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['customer']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 15;
	$gpf -> write_single_column_left($contractTexts['customer']['customer'], "{$contractor['first_name_cus']} {$contractor['last_name_cus']}", $verticalPointer);
	$gpf -> write_single_column_right($contractTexts['customer']['ruc'], $contractor['document_cus'], $verticalPointer);
	$verticalPointer -= 10;
	$gpf -> write_single_column_left($contractTexts['customer']['address'], $contractor['full_address'], $verticalPointer);
	
	$gpf -> write_box($verticalPointer);
	
	
	
	//************************* INSURED INFO *************************************//
	if($traveler_log_customer){
		$customer = new Customer($db, $traveler_log_customer);
		$customer -> getData();
		$customer = get_object_vars($customer);
		unset($customer['db']);
		$customer['full_address'] = Customer::getGeographicalCustomerAddress($db, $customer['serial_cus']);
		$customer['full_address'] = $customer['full_address']['name_cou'].' / '.$customer['full_address']['name_cit'].' / '.$customer['full_address']['address_cus'];
		$xc = new Country($db, $customer['auxData']['serial_cou']);
		$xc -> getData();
		$customer['auxData']['serial_cou'] = $xc -> getName_cou();
	
		$verticalPointer -= 20;
		$gpf -> write_red_box($leftMostX, $verticalPointer);
		$gpf -> write_title_box($contractTexts['insured']['title'], $leftMostX + 20, $verticalPointer);
		$gpf -> setBoxTop($verticalPointer);
		
		$verticalPointer -= 15;
		$gpf -> write_single_column_left($contractTexts['insured']['names'], "{$customer['first_name_cus']} {$customer['last_name_cus']}", $verticalPointer);
		$gpf -> write_single_column_right($contractTexts['insured']['birthdate'], $customer['birthdate_cus'], $verticalPointer);
		$verticalPointer -= 10;
		$gpf -> write_single_column_left($contractTexts['insured']['document'], " {$customer['document_cus']}", $verticalPointer);
		$gpf -> write_single_column_right($contractTexts['insured']['phone'], ''.$customer['phone1_cus'], $verticalPointer);
		$verticalPointer -= 10;
		$gpf -> write_single_column_left($contractTexts['insured']['citizenship'], $customer['auxData']['serial_cou'], $verticalPointer);
		$verticalPointer -= 10;
		$gpf -> write_single_column_left($contractTexts['insured']['beneficiaries'], $contractTexts['insured']['beneficiaries_text'], $verticalPointer);
		
		$gpf -> write_box($verticalPointer);
	}
	
	//************************* ENTRY LIMITS INFO *************************************//
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['limits']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 15;
	$gpf -> write_single_column($contractTexts['limits']['min_age'], '', $verticalPointer);
	$verticalPointer -= 10;
	$gpf -> write_single_column($contractTexts['limits']['max_age'], '', $verticalPointer);
	$verticalPointer -= 10;
	$gpf -> write_single_column($contractTexts['limits']['max_perm'], '', $verticalPointer);
	
	$gpf -> write_box($verticalPointer);
	
	
	//************************* PRODUCT INFORMATION *************************************//
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['product']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 15;
	$gpf -> write_wide_title_left($contractTexts['product']['services'], $verticalPointer, true, true);
	$gpf -> write_short_title_right($dataInfo['name_pbl'], $verticalPointer, true, true);
	$verticalPointer -= 15;
	$gpf -> write_wide_title_left($contractTexts['product']['assistance'], $verticalPointer, false, true);
	$gpf -> write_short_title_right($contractTexts['product']['max_amount'], $verticalPointer, false, true);
	
	$gpf -> write_box($verticalPointer - 5);
	
	$verticalPointer -= 5;
	foreach($dataBen as $d){
		$gpf -> setBoxTop($verticalPointer);

		if($dataBen['price_bxp'] == 0){
			$displayValue = $d['alias_con'];
		}else{
			$displayValue = $d['price_bxp'].'  ('.$d['restriction_price_bxp'].' x '.$d['description_rbl'].')';
		}
		
		$verticalPointer -= 10;	
		$gpf -> write_wide_text_left($d['description_bbl'], $verticalPointer, false);
		$gpf -> write_short_text_right($displayValue, $verticalPointer, false);
	
		$gpf -> write_box($verticalPointer);
		
		if($verticalPointer <= 100){
			$pdf -> ezNewPage();
			$verticalPointer = $max_height;
		}
	}
	
	
	
	
	
	//************************* IMPORTANT INFORMATION *************************************//
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['important']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['important']['text'], $verticalPointer, 'full');
	
	$gpf -> write_box($verticalPointer + 5);
	
	
	
	//************************* GENERAL INFORMATION *************************************//
	$verticalPointer -= 20;
	$gpf -> write_gray_title_box($contractTexts['general']['title'], $leftMostX, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['general']['text'], $verticalPointer, 'full');
	$verticalPointer -= 15;
	$verticalPointer = $gpf -> write_text_block($contractTexts['general']['bottom'], $verticalPointer, 'full');
	
	$gpf -> write_box($verticalPointer + 5);
	
	
	
	
	//************************* ACCEPTANCE OF CONDITIONS *************************************//
	$verticalPointer -= 10;
	$gpf -> write_gray_title_box($contractTexts['acceptance']['title'], $leftMostX, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['acceptance']['law'], $verticalPointer, 'full');
	
	$verticalPointer -= 50;
	$pdf -> addJpegFromFile(DOCUMENT_ROOT."img/signature.jpg", $leftMostX + 90, $verticalPointer, 80);
	$gpf -> write_signature_lines($verticalPointer);
	$verticalPointer -= 15;
	$gpf -> write_left_column($contractTexts['acceptance']['name_left'], $verticalPointer);
	$gpf -> write_right_column($contractTexts['acceptance']['name_right'], $verticalPointer);
	$verticalPointer -= 15;
	$gpf -> write_left_column($contractTexts['acceptance']['position_left'], $verticalPointer);
	$verticalPointer -= 15;
	$gpf -> write_left_column($contractTexts['acceptance']['unit_left'], $verticalPointer);
	
	$verticalPointer -= 30;
	$verticalPointer = $gpf -> write_text_block($contractTexts['acceptance']['company'], $verticalPointer, 'center');
	$verticalPointer = $gpf -> write_text_block($contractTexts['acceptance']['ruc'], $verticalPointer, 'center');
	
	$gpf -> write_box($verticalPointer);
	
	
	
	//************************* GENERAL CONDITIONS *************************************//
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['conditions']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['conditions']['text'], $verticalPointer, 'full');
	
	$gpf -> write_box($verticalPointer);
	
	
	
	
	//************************* DOCUMENTATION IN THE EVENT OF *************************************//
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['documentation']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['documentation']['text'], $verticalPointer, 'full');
	$verticalPointer -= 10;
	$verticalPointer = $gpf -> write_text_block($contractTexts['documentation']['body'], $verticalPointer, 'full');
	
	$gpf -> write_box($verticalPointer + 5);
	
	
	
	//************************* SUGGESTIONS *************************************//
	$verticalPointer -= 20;
	$gpf -> write_red_box($leftMostX, $verticalPointer);
	$gpf -> write_title_box($contractTexts['suggestions']['title'], $leftMostX + 20, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['suggestions']['text'], $verticalPointer, 'full');
	
	$gpf -> write_box($verticalPointer + 5);
	
	
	
	
	//************************* USER DEFENSE *************************************//
	$verticalPointer -= 10;
	$gpf -> write_gray_title_box($contractTexts['defense']['title'], $leftMostX, $verticalPointer);
	$gpf -> setBoxTop($verticalPointer);
	
	$verticalPointer -= 20;
	$verticalPointer = $gpf -> write_text_block($contractTexts['defense']['address'], $verticalPointer, 'full');
	$verticalPointer = $gpf -> write_text_block($contractTexts['defense']['fax'], $verticalPointer, 'full');
	$verticalPointer = $gpf -> write_text_block($contractTexts['defense']['email'], $verticalPointer, 'full');
	$verticalPointer = $gpf -> write_text_block($contractTexts['defense']['web'], $verticalPointer, 'full');
	
	$gpf -> write_box($verticalPointer + 5);
	$pdf->setEncryption('','',array('print'));
	
	
	

	//************************* DISPLAY PDF FILE *************************************//
	if($display == '1'){
		$pdf -> ezStream();
	}else{
		$pdfcode = $pdf -> ezOutput();
		$fp=fopen(DOCUMENT_ROOT .'modules/sales/contracts/contract_'.$serial_sal.'.pdf','wb');
		fwrite($fp,$pdfcode);
		fclose($fp);
	}
	
?>