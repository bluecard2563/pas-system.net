<?php
/*
 * Document   : contract_pas.php
 * Created on : May 19, 2010
 * Author     : Gabriela Guerrero & Santiago Borja
 * 
*/

	//GET LANGUAGE CODE FOR LABEL'S TEXT
	$lang_string = new Language($db, $langCode);
	$lang_string ->getData();
	$lang_string = $lang_string ->getCode_lang();
	
	include("modules/sales/contractTemplates/contractTexts/text_interbank.php");

	$sale = new Sales($db,$serial_sal);
	$sale -> getData();
	
	$pdf -> addJpegFromFile(DOCUMENT_ROOT."img/contract_logos/logo_interbank.jpg", 375, 784, 170);
	
	//Gathering all the info:
	$dataInfo = $sale -> getSaleProduct($langCode);
	$dataBen = $sale -> getSaleBenefits($langCode);
	$dataServ = $sale -> getSaleServices($langCode);
	$serialCus = $sale -> getSerial_cus();
	
	/*********** ASISTANCE PHONES *****************/
	$xCountry = new Country($db);
	$assistance_list = $xCountry -> getPhoneCountries(false);
	$assistance_phones = array();
	$name_array = array();
	$phone_array = array();
	foreach($assistance_list as $a){
		if(count($name_array) < 4){
			array_push($name_array, $a['name_cou']);
			array_push($phone_array, $a['phone_cou']);
		}else{
			array_push($assistance_phones, $name_array);
			array_push($assistance_phones, $phone_array);
			$name_array = array();
			$phone_array = array();
			array_push($name_array, $a['name_cou']);
			array_push($phone_array, $a['phone_cou']);
		}
	}
	array_push($assistance_phones, $name_array);
	array_push($assistance_phones, $phone_array);
	/*********** ASISTANCE PHONES *****************/

	if(!$traveler_log_customer){
		$customer = new Customer($db, $serialCus);
	}else{
		$customer = new Customer($db, $traveler_log_customer);
	}
	$customer -> getData();
	$customer = get_object_vars($customer);
	unset($customer['db']);
	$customer['full_address']=Customer::getGeographicalCustomerAddress($db, $customer['serial_cus']);
	$customer['full_address']=$customer['full_address']['name_cou'].' / '.$customer['full_address']['name_cit'].' / '.$customer['full_address']['address_cus'];
	
	$extra = new Extras($db,$serial_sal);
	$dataExt = Extras::getExtrasBySale($db, $serial_sal);
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext += $dE['fee_ext'];
		}
	}
	$date = html_entity_decode('Quito, '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );
	
	/*debug::print_r($dataInfo);die;
	debug::print_r($dataBen);
	debug::print_r($dataServ);
	debug::print_r($serialCus);
	debug::print_r($customer);
	debug::print_r($dataExt);
	debug::print_r($fee_ext);*/
	//Gathering - END
	
	//Header 1
	$pdf -> setColor($gray[0],$gray[1],$gray[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer,350,$largeTextSize,''.$date);
	$verticalPointer -= 39;
	
	$widthBranch=$pdf -> getTextWidth($stdTextSize,$saleContract['header1']['branch']);
	$widthCounter=$pdf -> getTextWidth($stdTextSize,$saleContract['header1']['counter']);

	$pdf -> setLineStyle(1.4,'square');
	$pdf -> setStrokeColor($gray[0],$gray[1],$gray[2]);
	$pdf -> line($leftMostX - 5,$verticalPointer,$leftMostX + 528,$verticalPointer);
	$verticalPointer -= 15;
	
	//HEADER 2:
	//DATE AND TIME
	$pdf -> setColor($refText[0], $refText[1], $refText[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer,200,$stdTextSize,''.$saleContract['header2']['location_date']);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($leftMostX + 120,$verticalPointer,80,$stdTextSize,''.$dataInfo['name_cit']);//location

	$emissionDate = GlobalFunctions::strToTimeDdMmYyyy($dataInfo['emission_date_sal']);
	$tempX = $leftMostX + 190;
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setColor($black[0], $black[1], $black[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,160,$stdTextSize,'  '.date("d", $emissionDate).'    '.date("m", $emissionDate).'     '.date("y", $emissionDate));//emission date
	$pdf -> setColor($refText[0], $refText[1], $refText[2]);
	$pdf -> line($tempX,$verticalPointer - 2, $tempX,$verticalPointer - 2);
	$pdf -> line($tempX + 20,$verticalPointer + 10, $tempX + 20,$verticalPointer - 2);
	$pdf -> line($tempX + 40,$verticalPointer + 10, $tempX + 40,$verticalPointer - 2);
	$pdf -> line($tempX + 60,$verticalPointer + 10, $tempX + 60,$verticalPointer - 2);
	$pdf -> line($leftMostX + 100,$verticalPointer - 2, $tempX + 60,$verticalPointer - 2);
	$pdf -> addTextWrap($tempX + 5,$verticalPointer - 10,160,$smallTextSize,''.$saleContract['header2']['emission']);

	//***********************************************SECTION 1: CLIENT DATA ********************************************************
	$parameter = new Parameter($db);
	$parameter -> setSerial_par('13');
	$parameter -> getData();
	
	//CLIENT DATA TITLE
	$verticalPointer -= 40;
	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['client']['title'].'</b>');//Client data title

	//MAIN SECTION
	$pdf -> setLineStyle(0.2,'square');
	$pdf -> setStrokeColor($refText[0],$refText[1],$refText[2]);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Last name
	$tempX = $leftMostX;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['last_name_cus'], 150);
	
	//First name
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['first_name_cus'], 150);
	
	//Document
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['document_cus'],'center');//document
	
	//Birth Date
	$tempX += 110;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['birthdate_cus'],'center');
	$pdf -> line($leftMostX + 528,$verticalPointer + 2,$leftMostX + 528,$verticalPointer - 8);
	
	//Bottom labels:
	$pdf -> line($leftMostX,$verticalPointer-8,$leftMostX + 528,$verticalPointer-8);
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract['client']['0']);
	
	
	//SECOND LINE
	$verticalPointer -= 25;
	$pdf -> setColor($black[0], $black[1], $black[2]);
	
	//Phone
	$tempX = $leftMostX;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	$pdf -> addTextWrap($tempX,$verticalPointer-5,110,$stdTextSize,''.$customer['phone1_cus'],'center');
	
	//Email
	$tempX += 110;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['email_cus'], 150);
	
	//Addesss
	$tempX += 150;
	$pdf -> line($tempX,$verticalPointer + 2,$tempX,$verticalPointer - 8);
	GlobalFunctions::writeTextField($pdf, $tempX, $verticalPointer, $stdTextSize, $customer['full_address'], 268);
	$pdf -> line($leftMostX + 528,$verticalPointer + 2,$leftMostX + 528,$verticalPointer - 8);
	
	//Bottom labels:
	$pdf -> line($leftMostX,$verticalPointer-8,$leftMostX + 528,$verticalPointer-8);
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap(80,$verticalPointer-17,500,$stdTextSize,''.$saleContract['client']['1']);
	
	
	//THIRD LINE
	//Emergency contact Line:
	$verticalPointer -= 30;
	$tempX = $leftMostX;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,140,$stdTextSize,''.$saleContract['client']['3']);
	$pdf -> line($tempX + 140,$verticalPointer - 2,$tempX + 340,$verticalPointer - 2);
	$pdf -> line($tempX + 340,$verticalPointer + 8, $tempX + 340,$verticalPointer - 2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 140, $verticalPointer + 5, $stdTextSize, $customer['relative_cus'], 190);
	
	//Emergency Phone
	$tempX += 350;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf -> addTextWrap($tempX,$verticalPointer,40,$stdTextSize,''.$saleContract['client']['4']);
	$pdf -> line($tempX + 40,$verticalPointer - 2, $tempX + 180,$verticalPointer - 2);
	$pdf -> line($tempX + 180, $verticalPointer + 8, $tempX + 180,$verticalPointer-2);
	$pdf -> setColor($black[0], $black[1], $black[2]);
	GlobalFunctions::writeTextField($pdf, $tempX + 40, $verticalPointer + 5, $stdTextSize, $customer['relative_phone_cus'], 140);
	
	
	
	//********************************************* SECTION 1A: EXTRAS INFO **********************************************************
	//Extras section
	$verticalPointer -= 15;
	if(count($dataExt)>0){
	 $displayedLimit=$parameter->getValue_par();//Extras limit displayed.
	 //$displayedLimit=0;
		if(count($dataExt)<=$displayedLimit){
			$verticalPointer = GlobalFunctions::displayExtras($pdf,$dataExt,$verticalPointer,1,0, $langCode);
		}
	}

	//********************************************* SECTION 2: TRIP DATA **********************************************
	$verticalPointer -= 15;
	if($verticalPointer <= 100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$beginDate=''.$dataInfo['begin_date_sal'];
	$endDate=''.$dataInfo['end_date_sal'];
	if($dataInfo['flights_pro']==0){
		$beginDate = $tripBeginDate;
		$endDate = $tripEndDate;
		$dataInfo['country_sal'] = $tripCountry;
		$dataInfo['city_sal'] = $tripCity;
	}

	if($beginDate && $endDate && $dataInfo['country_sal'] && $dataInfo['city_sal']){
		//Header line:
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer+12,200,$largeTextSize,'<b>'.$saleContract['trip']['title1'].'</b>');

		$pdf -> setLineStyle(0.2,'square');
		$tempX = $leftMostX;
		$verticalPointer -= 5;
		$pdf -> setStrokeColor($black[0],$black[1],$black[2]);

		//Bottom Lines
		$pdf -> setColor($refText[0],$refText[1],$refText[2]);
		$pdf -> addTextWrap($tempX + 2,$verticalPointer,528,$medTextSize,''.$saleContract['trip']['0']);
		$pdf -> addTextWrap($leftMostX,$verticalPointer-10,500,$smallTextSize,''.$saleContract['trip']['1']);

		//Departure Date
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($tempX + 70, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $beginDate ) ) );
		$pdf -> line($tempX,$verticalPointer-2,$tempX + 130,$verticalPointer - 2);
		$pdf -> line($tempX,$verticalPointer + 10, $tempX,$verticalPointer - 2);
		$pdf -> line($tempX + 130,$verticalPointer + 10, $tempX + 130,$verticalPointer - 2);

		//End Date
		$tempX += 135;
		$pdf -> addTextWrap($tempX + 80, $verticalPointer, 60, $stdTextSize, date( 'd - m - Y', GlobalFunctions::strToTimeDdMmYyyy( $endDate ) ) );
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX  + 140,$verticalPointer-2);
		$pdf -> line($tempX + 140,$verticalPointer+10, $tempX + 140,$verticalPointer-2);

		//Duration:
		$tempX += 145;
		$fullDays = GlobalFunctions::getDaysDiffDdMmYyyy($beginDate, $endDate);
		$pdf -> addTextWrap($tempX + 50, $verticalPointer, 30, $stdTextSize, $fullDays);
		$pdf -> line($tempX -2,$verticalPointer - 2, $tempX + 80,$verticalPointer - 2);
		$pdf -> line($tempX + 80,$verticalPointer+10, $tempX + 80,$verticalPointer-2);

		//Destination
		$tempX += 85;
		$destinationString = $dataInfo['country_sal'];
		$destinationString .= $dataInfo['city_sal']== 'General'?'':' - ' . $dataInfo['city_sal'];
		GlobalFunctions::writeTextField($pdf, $tempX + 35, $verticalPointer + 5, $stdTextSize, $destinationString, 115);
		$pdf -> line($tempX,$verticalPointer-2, $tempX + 150,$verticalPointer-2);
		$pdf -> line($tempX + 150,$verticalPointer + 10, $tempX + 150,$verticalPointer - 2);
	}else{
		$verticalPointer += 35;
	}
	 
	//************************************************** SECTION 4: BENEFITS **********************************************
	$verticalPointer -= 40;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$colWidth = 200;
	$valueWidth = 58;

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer + 12,546,$largeTextSize,'<b>'.$saleContract['benefits']['title'].'</b>');

	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	if($dataBen){
		$count = 0;
		$verticalSep = 20;
		//Make the calculations for sorting the benefits in a final array
		$benefits_count = count($dataBen);
		($benefits_count % 2 != 0) ? $middle = (int)($benefits_count / 2) + 1 : $middle = $benefits_count / 2;
		//Getting the table's height
		$benefits_height = ($benefits_count * $verticalSep);
		$benefits_final_array = array();
		
		foreach($dataBen as $key => $d){
			if($dataBen['price_bxp'] == 0){
				$displayValue = $d['alias_con'];
			}else{
				$displayValue = $d['price_bxp'].'  ('.$d['restriction_price_bxp'].' x '.$d['description_rbl'].')';
			}

			$each_row['0'] = $bullet.$d['description_bbl'];
			$each_row['1'] = $displayValue;

			array_push($benefits_final_array, $each_row);
			$each_row = array();
		}

		$pdf->ezSetDy(-($max_height - $verticalPointer));
		$pdf->ezTable($benefits_final_array,'','',
							array('showHeadings'=>0,
								'colGap' => 2,
								'shaded'=>2,
								'shadeCol' => array(0.95,0.97,0.98),
								'shadeCol2' => array(1,1,1),
								'showLines'=>0,
								'xPos'=>'302',
								'xOrientation'=>'center',
								'innerLineThickness' => 0.8,
								'outerLineThickness' => 0.8,
								'fontSize' => $stdTextSize,
								'titleFontSize' => $stdTextSize,
								'cols'=>array(
										'0'=>array('justification'=>'left','width'=>420),
										'1'=>array('justification'=>'center','width'=>113)
										)
							)
		);
	}
	
	//************************************************** SECTION 5: RECOMMENDATIONS **********************************************
	$verticalPointer -= $benefits_height;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract['recomen']['title'].'</b>');
	
	
	$verticalPointer -= 5;
	$verticalStep = 10;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$rows = count($saleContract['recomen']);
	for($i=0; $i < $rows - 1; $i++){
		$widthText = $pdf -> getTextWidth($stdTextSize,$saleContract['recomen'][$i]);
		if($widthText < 546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer, 538,$stdTextSize,$bullet.$saleContract['recomen'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract['recomen'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j <= ($widthText/546); $j++){
				$a = $pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer <= 50){
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer <= 50){
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}
	
	
	//************************************************** SECTION 6: EXCLUSIONS **********************************************
	$verticalPointer -= 25;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract['exclu']['title'].'</b>');
	
	
	$verticalPointer -= 5;
	$verticalStep = 10;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$rows=count($saleContract['exclu']);
	for($i=0; $i<$rows-1; $i++){
		//$pdf -> ezText($saleContract['exclu'][$i],10);
		$widthText=$pdf -> getTextWidth($stdTextSize,$saleContract['exclu'][$i]);
		if($widthText<546){
			$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract['exclu'][$i]);
			$verticalPointer -= $verticalStep;
		}
		else{
			$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,$bullet.$saleContract['exclu'][$i]);
			$verticalPointer -= $verticalStep;
			for($j=1; $j<=($widthText/546); $j++){
				$a=$pdf -> addTextWrap($leftMostX,$verticalPointer,538,$stdTextSize,''.$a);
				$verticalPointer -= $verticalStep;
				if($verticalPointer<=50){
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		if($verticalPointer<=50){
			$pdf -> ezNewPage();
			$verticalPointer = 755;
		}
	}

	//************************************************** SECTION 7: ASSISTANCE PHONES **********************************************
	$verticalPointer -= 25;
	if($verticalPointer<=100){
		$pdf -> ezNewPage();
		$verticalPointer = 755;
	}

	$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
	$pdf -> setLineStyle(16);
	$pdf -> line($leftMostX + 2,$verticalPointer+16,$leftMostX + 519,$verticalPointer+16);
	$pdf -> setColor($red[0],$red[1],$red[2]);
	$pdf -> addTextWrap($leftMostX,$verticalPointer+12,546,$largeTextSize,'<b>'.$saleContract['phones']['title'].'</b>');


	$verticalPointer -= 5;
	$pdf -> setColor($refText[0],$refText[1],$refText[2]);
	$pdf->ezSetDy(-($max_height - $verticalPointer));

	//INTERBANK CUSTOM PHONES
	$additional_phones = array(
		0 => array( 0 => 'Lima',
					1 => html_entity_decode('00 &#45; 511 &#45; 708 &#45; 62 &#45; 67')),
		1 => array( 0 => 'USA',
					1 => html_entity_decode('1 &#45; 877 &#45; 830 &#45; 6858'))

	);

	$pdf -> ezTable($additional_phones,
						'',
						'',
						array('showHeadings'=>0,
							'shaded'=>1,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
									'0'=>array('justification'=>'center','width'=>120),
									'1'=>array('justification'=>'center','width'=>120)
									)
						)
	);

	if(count($dataExt)>0){
		if(count($dataExt)>$displayedLimit){
			$verticalPointer= GlobalFunctions::displayExtras($pdf,$dataExt,$verticalPointer,2,$dataInfo, $langCode);
		}
	}
	
	$pdf->setEncryption('','',array('print'));
	
	if($display=='1'){
		$pdf -> ezStream();
	}else{
		$pdfcode = $pdf -> ezOutput();
		if($multi_sale){
			$fp=fopen(DOCUMENT_ROOT .'modules/sales/multiSaleContracts/contract_'.$serial_sal.'.pdf','wb');
		}else{
			$fp=fopen(DOCUMENT_ROOT .'modules/sales/contracts/contract_'.$serial_sal.'.pdf','wb');
		}
		fwrite($fp,$pdfcode);
		fclose($fp);
	}
?>