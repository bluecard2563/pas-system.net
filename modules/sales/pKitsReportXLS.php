<?php
/*
File: pKitsReportXLS
Author: David Rosales
Creation Date: 09/03/2015
Last Modified: 
Modified By:
*/

set_time_limit(36000);
ini_set('memory_limit','512M');

$cardList = Sales::getKitsReport($db,$_POST['selManager'],$_POST['selComissionist'],$_POST['selDealer'],$_POST['selBranch'],$_POST['selCounter'],$_POST['txtBeginDate'],$_POST['txtEndDate']);

$totalKitsDelivered = 0;
$totalKitsNOTDelivered = 0;
if(is_array($cardList)) {
    foreach ($cardList as $c) {
        if ($c['deliveredKit'] == 'Si')
            $totalKitsDelivered++;
        else
            $totalKitsNOTDelivered++;
    }
}

if ($_POST['selCounter'] && $_POST['selManager'] && $_POST['selComissionist']) {
    $col_num = 4;
} else {
    $col_num = 8;
}

/*********************************** DISPLAYING THE XLS *****************************************/
$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y'));
$table = '<table>
            <tr><td colspan='.$col_num.'>'.$date.'</td></tr>
            <tr></tr>
            <tr>
                    <th></th>
                    <th><b>REPORTE DE KITS ENTREGADOS</b></th>
            </tr>
            <tr></tr>
        </table>';

if(is_array($cardList)) {
    if ($_POST['selCounter'] && $_POST['selManager'] && $_POST['selComissionist']) {
        $table .= "<table>
                    <tr>
                        <td><b>Comercializador:</b></td>
                        <td>".$cardList[0]['dealer']."</td>
                    </tr>
                    <tr>
                        <td><b>Sucursal:</b></td>
                        <td>".$cardList[0]['branch']."</td>
                    </tr>
                    <tr>
                        <td><b>Counter:</b></td>
                        <td>".$cardList[0]['counter']."</td>
                    </tr>
                    <tr>
                        <td><b>Ventas realizadas:</b></td>
                        <td>".($totalKitsDelivered+$totalKitsNOTDelivered)."</td>
                    </tr>
                    <tr>
                        <td><b>Total de Kits entregados:</b></td>
                        <td>".$totalKitsDelivered."</td>
                    </tr>
                    <tr>
                        <td><b>Total de Kits No entregados:</b></td>
                        <td>".$totalKitsNOTDelivered."</td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                </table>
                <table border='1'>
                    <tr>
                        <th><b>N&uacute;mero de Tarjeta</b></th>
                        <th><b>Cliente</b></th>
                        <th><b>Producto</b></th>
                        <th><b>Kit Entregado</b></th>
                    </tr>";        
    } else {
        $table .= "<table>
                    <tr>
                        <td><b>Ventas realizadas:</b></td>
                        <td>".($totalKitsDelivered+$totalKitsNOTDelivered)."</td>
                    </tr>
                    <tr>
                        <td><b>Total de Kits entregados:</b></td>
                        <td>".$totalKitsDelivered."</td>
                    </tr>
                    <tr>
                        <td><b>Total de Kits No entregados:</b></td>
                        <td>".$totalKitsNOTDelivered."</td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                </table>
                <table border='1'>
                    <tr>
                            <th><b>N&uacute;mero de Tarjeta</b></th>
                            <th><b>Cliente</b></th>
                            <th><b>Responsable</b></th>
                            <th><b>Comercializador</b></th>
                            <th><b>Sucursal</b></th>
                            <th><b>Counter</b></th>
                            <th><b>Producto</b></th>
                            <th><b>Kit Entregado</b></th>
                    </tr>";
    }
    
    foreach ($cardList as $cl) {
        $table .= "<tr>
                <td>".$cl['card_number']."</td>
                <td>".$cl['customer']."</td>";
                if (!$_POST['selDealer'] && !$_POST['selCounter']) {
                    $table .= "<td>".$cl['responsible']."</td>
                        <td>'".$cl['dealer']."</td>
                        <td>".$cl['branch']."</td>
                        <td>".$cl['counter']."</td>";
                }
                $table .= "<td>".html_entity_decode($cl['product'])."</td>
                <td>".html_entity_decode($cl['deliveredKit'])."</td>
            </tr>";
    }
    $table.="</table>";
    
} else {
    $table = "<table border='1'>
                <tr>
                    <td><b>No existen registros para los parámetros seleccionados.</b></td>
                </tr>
                <tr></tr>
                <tr></tr>
            </table>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=KitsEntregados.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $table;
?>
