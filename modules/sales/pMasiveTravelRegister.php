<?php
/*
File: pMasiveTravelRegister.php
Author: Esteban Angulo
Creation Date: 02/03/2010
*/

//Debug::print_r($_POST);
//die();
//Inserts New Customer Data
$customer=new Customer($db);
 $document_cus=$_POST['txtDocumentCustomer'];
 
 /*Checks if the Customer already is registered*/
 if($customer->existsCustomer($document_cus,NULL)){ //if the customer is registered. Proceed to sell
	$serial_cus=$customer->getCustomerSerialbyDocument($document_cus);
	$customer->setSerial_cit($_POST['selCityCustomer']);
	if($_POST['txtNameCustomer']){
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
	}

	if($_POST['txtNameCustomer1']){
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
	}

	$customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
	$customer->setPhone1_cus($_POST['txtPhone1Customer']);
	$customer->setPhone2_cus($_POST['txtPhone2Customer']);
	$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
	$customer->setEmail_cus($_POST['txtMailCustomer']);

   if($_POST['txtAddress']){
		$customer->setAddress_cus(specialchars($_POST['txtAddress']));
	}
	if($_POST['txtAddress1']){
		$customer->setAddress_cus(specialchars($_POST['txtAddress1']));
	}

	if($_POST['txtRelative']){
		$customer->setRelative_cus(specialchars($_POST['txtRelative']));
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
		$customer->setRelative_cus($_POST['txtManager']);
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}

	if($_POST['hdnVip']){
			$customer->setVip_cus($_POST['hdnVip']);
	}else{
			$customer->setVip_cus('0');
	}

	$customer->setStatus_cus('ACTIVE');
	$customer->updateInfoSale($serial_cus);
	$saleData['tit']['last_name_cus']=$_POST['txtLastnameCustomer'];
	if($_POST['txtNameCustomer']){
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer'];
	}

	if($_POST['txtNameCustomer1']){
		$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer1'];
	}
	$saleData['tit']['document_cus']=$_POST['txtDocumentCustomer'];
	$saleData['tit']['birthdate_cus']=$_POST['txtBirthdayCustomer'];
	$saleData['tit']['email_cus']=$_POST['txtMailCustomer'];
	 if($_POST['txtAddress']){
		$saleData['tit']['address_cus']=$_POST['txtAddress'];
	}
	if($_POST['txtAddress1']){
		$saleData['tit']['address_cus']=$_POST['txtAddress1'];
	}
	$saleData['tit']['phone_cus']=$_POST['txtPhone1Customer'];
	$saleData['tit']['relative_cus']=$_POST['txtRelative'];
	$saleData['tit']['relative_phone_cus']=$_POST['txtPhoneRelative'];
	
    $sessionLanguage = $_SESSION['serial_lang'];
    $question=new Question($db);
    $maxSerial=$question->getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage,$serial_cus);

	foreach($questionList as $q){
		if($q['serial_ans']){
			$answer=new Answer($db,$q['serial_ans']);
		}else{
			$answer=new Answer($db);
		}
		$answer->setSerial_cus($serial_cus);
		$answer->setSerial_que($q['serial_que']);
		$ans=$_POST['question_'.$q['serial_qbl']];
		if($ans){
			if($q['type_que']=='YN'){
				$answer->setYesno_ans($ans);
			}else{
				$answer->setAnswer_ans($ans);
			}

			if($q['yesno_ans'] or $q['answer_ans']){
				if($answer->update()){
					$flag=0;
				}else{
					$flag=1;
					break;
				}
			}else{
				if($answer->insert()){
					$flag=0;
				}else{
					$flag=1;
					break;
				}
			}
		}
	}
	
	/*Generating the info for sending the e-mail*/
    if($_POST['txtNameCustomer']){
    $misc['textForEmail']='Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';

	$misc['customer']=$_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
	}
    if($_POST['txtNameCustomer1']){
    $misc['customer']=$_POST['txtNameCustomer1'];
    }
	$customer->setserial_cus($serial_cus);
	$customer->getData();
	$password=$customer->getPassword_cus();
    $misc['username']=$customer->getEmail_cus();
    $misc['pswd']=$password;
    $misc['email']=$customer->getEmail_cus();
    /*END E-mail Info*/
	$ret=executeSale($_POST,$serial_cus,$saleData);
	if(is_array($ret)){
		$error=$ret['error'];
		$misc['cardNumber']=$ret['cardNumber'];
	}else{

		$error=$ret['error'];
	}
	GlobalFunctions::sendMail($misc, 'newClient');
	http_redirect('main/sales/'.$error);
 }else{//if the customer is not registered. Inserts the new client and the proceed to sell

    $customer->setSerial_cit($_POST['selCityCustomer']);
    $customer->setType_cus($_POST['selType']);
    $customer->setDocument_cus($_POST['txtDocumentCustomer']);

    if($_POST['txtNameCustomer']){
        $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
    }

    if($_POST['txtNameCustomer1']){
        $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
    }

    $customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
    $customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
    $customer->setPhone1_cus($_POST['txtPhone1Customer']);
    $customer->setPhone2_cus($_POST['txtPhone2Customer']);
    $customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
    $customer->setEmail_cus($_POST['txtMailCustomer']);
    
   if($_POST['txtAddress']){
        $customer->setAddress_cus(specialchars($_POST['txtAddress']));
    }
    if($_POST['txtAddress1']){
        $customer->setAddress_cus(specialchars($_POST['txtAddress1']));
    }

    if($_POST['txtRelative']){
		$customer->setRelative_cus($_POST['txtRelative']);
		$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	}
	if($_POST['txtManager']){
		$customer->setRelative_cus(specialchars($_POST['txtManager']));
		$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	}

    if($_POST['hdnVip']){
            $customer->setVip_cus($_POST['hdnVip']);
    }else{
            $customer->setVip_cus('0');
    }

    $customer->setStatus_cus('ACTIVE');
    $password=GlobalFunctions::generatePassword(6, 8, false);
    $customer->setPassword_cus(md5($password));

    $serialNewCus=$customer->insert();
    if($serialNewCus){
	/*Customer Data Array*/
	$saleData['tit']['last_name_cus']=$_POST['txtLastnameCustomer'];
	$saleData['tit']['first_name_cus']=$_POST['txtNameCustomer'];
	$saleData['tit']['document_cus']=$_POST['txtDocumentCustomer'];
	$saleData['tit']['birthdate_cus']=$_POST['txtBirthdayCustomer'];
	$saleData['tit']['email_cus']=$_POST['txtLastnameCustomer'];
	$saleData['tit']['address_cus']=$_POST['txtLastnameCustomer'];
	$saleData['tit']['phone_cus']=$_POST['txtPhone1Customer'];
	$saleData['tit']['relative_cus']=$_POST['txtRelative'];
	$saleData['tit']['relative_phone_cus']=$_POST['txtPhoneRelative'];




    /*Generating the info for sending the e-mail*/
    if($_POST['txtNameCustomer']){
	$misc['customer']=$_POST['txtNameCustomer'].' '.$_POST['txtLastnameCustomer'];
	}
	

    if($_POST['txtNameCustomer1']){
    $misc['customer']=$_POST['txtNameCustomer1'];
    }
	$misc['textForEmail']='Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
    $misc['username']=$customer->getEmail_cus();
    $misc['pswd']=$password;
    $misc['email']=$customer->getEmail_cus();
    /*END E-mail Info*/

	if($_POST['hdnQuestionsFlag']){
		$sessionLanguage = $_SESSION['serial_lang'];
		$question=new Question($db);
		$questionList = Question::getActiveQuestions($db, $sessionLanguage);
		$flag=0;
		foreach($questionList as $q){
			$answer=new Answer($db);
			$answer->setSerial_cus($customer->getLastSerial());
			$answer->setSerial_que($q['serial_que']);
			$ans=$_POST['question_'.$q['serial_qbl']];
			if($ans){
				if($q['type_que']=='YN'){
					$answer->setYesno_ans($ans);
				}else{
					$answer->setAnswer_ans($ans);
				}
				if($answer->insert()){
					$flag=1;
				}else{
					$flag=2;
					break;
				}
			}else{
				break;
			}

		}
		if($flag==1){
                   $ret= executeSale($_POST,$serialNewCus,$saleData);
				   if(is_array($ret)){
						$error=$ret['error'];
						$misc['cardNumber']=$ret['cardNumber'];
					}else{
						$error=$ret;
					}
					 if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
                        http_redirect('main/sales/'.$error);//http_redirect('modules/customer/fNewCustomer/1'); // Success
                    }else{
                        executeSale($_POST,$serialNewCus,$saleData);
                        http_redirect('main/sales/'.$error); //http_redirect('modules/customer/fNewCustomer/3'); //Errors by sending e-mail
                    }
		}else{
			http_redirect('main/sales/6'); //Errors inserting a new Customer.
                        //http_redirect('modules/customer/fNewCustomer/2');
		}
	}else{//if the client is younger than 65 years old
		$ret= executeSale($_POST,$serialNewCus,$saleData);
	   if(is_array($ret)){
			$error=$ret['error'];
			$misc['cardNumber']=$ret['cardNumber'];
		}else{
			$error=$ret;
		}
            if(GlobalFunctions::sendMail($misc, 'newClient')){ //Sending the client his login info.
                  http_redirect('main/sales/'.$error);
               // http_redirect('modules/customer/fNewCustomer/1'); //Success
            }else{
                $error=5;
                http_redirect('main/sales/'.$error);
               // http_redirect('modules/customer/fNewCustomer/3'); //Errors by sending e-mail
            }
	}
    }else{
	http_redirect('main/sales/6'); //Errors inserting a new Customer.
    }

    
 }

/***********************************************
* function executeSale
@Name: executeSale
@Description: executs the proccess of a sale
@Params:
 *       N/A
@Returns: true
 *        false
***********************************************/
 function executeSale($data,$serial_cus,$saleData){
	

     global $db;
     $sale=new Sales($db);
    //Debug::print_r($data);

    $sale->setSerial_cus($serial_cus);
    $sale->setSerial_pbd($data['selProduct']);

    $counter=new Counter($db);
	if($data['hdnSerial_cntRPC']){
		$sale->setSerial_cnt($data['hdnSerial_cntRPC']);
	}else{
		$sale->setSerial_cnt($counter->counterExists($_SESSION['serial_usr']));
	}

	if(true){ //Evaluate if we have to register the card number sold, or just the sale.
		$sale->setCardNumber_sal($data['txtCardNum']);
	}
	


    $sale->setEmissionDate_sal($data['txtEmissionDate']);
    $sale->setBeginDate_sal($data['txtDepartureDate']);
    $sale->setEndDate_sal($data['txtArrivalDate']);
    $sale->setInDate_sal(date("d/m/Y"));
    $sale->setDays_sal($data['hddDays']);
    $sale->setFee_sal($data['hdnPrice']);
    $sale->setObservations_sal(specialchars($data['observations']));
    $sale->setCost_sal($data['hdnCost']);
	if($data['rdIsFree']){
            if($data['rdIsFree']=='NO'){
                $sale->setStatus_sal('REGISTERED');
            }else{
                $sale->setStatus_sal('REQUESTED');
            }
		$sale->setFree_sal($data['rdIsFree']);
		
	}else{
		$sale->setFree_sal('NO');
		$sale->setStatus_sal('REGISTERED');
	}
    //$sale->setIdErpSal_sal();
    $sale->setType_sal('SYSTEM');
    $sale->setStockType_sal($data['sale_type']);
	if($data['hdnDirectSale']){
		$sale->setDirectSale_sal('YES');
	}
	else{
		$sale->setDirectSale_sal('NO');
	}
    $sale->setTotal_sal($data['hdnTotal']);
    $sale->setTotalCost_sal($data['hdnTotalCost']);
    $sale->setChange_fee_sal($data['rdCurrency']);
	if($data['selCityDestination']){
		$sale->setSerial_cit($data['selCityDestination']);
	}
	else{
		$sale->setSerial_cit('NULL');
	}
    //Debug::print_r($data);
	//Debug::print_r($sale);
	//die();


    $saleID=$sale->insert();


if($saleID){
	$error=4;
	//added for register free appliance
	if($data['rdIsFree'] && $data['rdIsFree']!='NO'){
		$alert = new Alert($db);
		$alert -> setMessage_alt('Solicitud de Free pendiente.');
		$alert -> setType_alt('FREE');
		$alert -> setStatus_alt('PENDING');
		$alert -> setRemote_id($saleID);
		$alert -> setTable_name('sales/fSearchFreeSales/');
		
		if(!$alert -> autoSend($_SESSION['serial_usr'])){
			$error = 3;
		}else{
			$error = 1;
		}
	}
		
	//end free appliance
		foreach($data as $key=>$arr){
			if(!(strpos($key,"chkService_")===false)){
				if(!$sale->insertServices($saleID,$arr)){
					 http_redirect('main/sales/8');
				}
			}
		}

		if($data['extra']){

			$customer=new Customer($db);
			$extras=new Extras($db);
			foreach($data['extra'] as $key=>$extra ){
				if($customer->existsCustomer($extra['idExtra'],NULL)){

					$serial_cus=$customer->getCustomerSerialbyDocument($extra['idExtra']);
					$customer->setSerial_cus($serial_cus);
					$customer->getData();
					$customer->setType_cus('PERSON');
					$customer->setFirstname_cus(specialchars($extra['nameExtra']));
					$customer->setLastname_cus(specialchars($extra['lastnameExtra']));
					$customer->setEmail_cus($extra['emailExtra']);
					$customer->setSerial_cit($data['selCityCustomer']);
					$customer->updateInfoSale($serial_cus);



					$extras->setSerial_sal($saleID);
					$extras->setSerial_cus($serial_cus);
					$extras->setRelationship_ext($extra['selRelationship']);
					$extras->setFee_ext($extra['hdnPriceExtra']);
					$extras->setCost_ext($extra['hdnCostExtra']);
					
						$sessionLanguage = $_SESSION['serial_lang'];
						$question=new Question($db);
						$questionList = Question::getActiveQuestions($db, $sessionLanguage,$serial_cus);
						foreach($questionList as $q){
							if($q['serial_ans']){
							$answer=new Answer($db,$q['serial_ans']);
							}else{
								$answer=new Answer($db);
							}
							$answer->setSerial_cus($serial_cus);
							$answer->setSerial_que($q['serial_que']);
							$ans=$_POST['question'.$key.'_'.$q['serial_qbl']];

							if($ans){
								if($q['type_que']=='YN'){
									$answer->setYesno_ans($ans);

								}else{
									$answer->setAnswer_ans($ans);
								}
								if($q['yesno_ans'] or $q['answer_ans']){
									if($answer->update()){
										$error=4;										
									}else{
										$error=11;
										//return $error;
										break;
									}
								}else{
									if($answer->insert()){
										$error=4;
									}else{
										$error=11;
										//return $error;
										break;
									}
								}
							}else{
								//return $error;
								break;
							}
						  }
//Debug::print_r($extras);
//die();
					if(!$extras->insert()){
							$error=9;
						}
						else{
							$error=4;
						}
//die();
				}
				else{
					$customer->setSerial_cit($data['selCityCustomer']);
					$customer->setDocument_cus($extra['idExtra']);
					$customer->setFirstname_cus(specialchars($extra['nameExtra']));
					$customer->setLastname_cus(specialchars($extra['lastnameExtra']));
					$customer->setBirthdate_cus($extra['birthdayExtra']);
					$customer->setType_cus('PERSON');
					$customer->setEmail_cus($extra['emailExtra']);

					$extraID = $customer->insert();

						if($_POST['hdnQuestionsFlag']){
						$sessionLanguage = $_SESSION['serial_lang'];
						$question=new Question($db);
						$questionList = Question::getActiveQuestions($db, $sessionLanguage);
						foreach($questionList as $q){
							$answer=new Answer($db);
							$answer->setSerial_cus($customer->getLastSerial());
							$answer->setSerial_que($q['serial_que']);
							$ans=$_POST['question_'.$q['serial_qbl']];
							if($ans){
								if($q['type_que']=='YN'){
									$answer->setYesno_ans($ans);
								}else{
									$answer->setAnswer_ans($ans);
								}
								if($q['yesno_ans'] or $q['answer_ans']){
									if($answer->update()){
										$error=4;
									}else{
										$error=11;
										//return $error;
										break;
									}
								}else{
									if($answer->insert()){
										$error=4;
									}else{
										$error=11;
										//return $error;
										break;
									}
								}
							}else{
								
								break;
							}
						  }
						}

					if($extraID){
						$extras->setSerial_sal($saleID);
						$extras->setSerial_cus($extraID);
						$extras->setRelationship_ext($extra['selRelationship']);

						if($extra['hdnPriceExtra']==0){
							$extras->setFee_ext(0.00);
							$extras->setCost_ext(0.00);
						}
						else{
							$extras->setFee_ext($extra['hdnPriceExtra']);
							$extras->setCost_ext($extra['hdnCostExtra']);
						}
						//Debug::print_r($extras);
						if(!$extras->insert()){
							$error=9;
						}
						else{
							$error=4;
						}
					}
					else{
						$error=10;
					}
						
				}
				
			}
			$saleData['extras']= $_POST['extra'];

			
			//http_redirect('main/sales/'.$error);
		}
		$arr=array();
		$arr['error']=$error;
		$arr['cardNumber']=$data['txtCardNum'];
		return $arr;
    }
    else{
		echo "no mismo";
	  $error=7;
	  return $error;
      //http_redirect('main/sales/7');
    }
 }
?>
