<?php
/*
    Document   : pPaymentPtoP
    Created on : 20/08/2018
    Author     : Lenin Padilla
    Description: Get information user
*/
$array =unserialize($_GET['serial_inv']);

$user = new User($db, $_SESSION['serial_usr']);
$serial_usr = $_SESSION['serial_usr'];
$user->getData();
$user = get_object_vars($user);
$country = new Country($db,$user['aux_data']['serial_cou']);
$country->getData();
$country=get_object_vars($country);
$city= new City($db,$user['serial_cit']);
$city->getData();
$city=get_object_vars($city);



$userInfoPtoP = Sales::getDataUserPaymentPtoP($db,$array[0]);

$taxes = unserialize($userInfoPtoP[0][taxes]);


$jsonSent = json_decode(null);
$ad= array();
foreach ($array as $ar){

    $userInfoPtoP_ = Sales::getDataUserPaymentPtoP($db,$ar);

    $taxes_ = unserialize($userInfoPtoP_[0][taxes]);

    $taxesModified_ = round((($userInfoPtoP_[0][total] * $taxes_[0][tax_percentage]) / 100), 2);
    $base_ = ($userInfoPtoP_[0][total] - $taxesModified_);
    $total_ = $taxesModified_ + $base_;



    $jsonItem=[
        "sku"=> $userInfoPtoP_[0][reference],
        "name"=> $userInfoPtoP_[0][name_product],
        "category"=> "viajero",
        "qty"=> 1,
        "price"=> $base_,
        "tax"=>$taxesModified_
    ];
     array_push($ad,$jsonItem);
} 



$taxesModified = round((($_GET['total_inv'] * $taxes[0][tax_percentage]) / 100), 2);
$base = ($_GET['total_inv'] - $taxesModified);
$total = $taxesModified + $base;
$iva=0;


$jsonSent['locale'] = "es_EC";
$jsonSent['buyer'] = [
    "document" => $user['document_usr'],
    "documentType" => "CI",
    "name" => $user['first_name_usr'],
    "surname" => $user['last_name_usr'],
    "email" => $user['email_usr'],
    "address" => [
        "street" => $user['address_usr'],
        "city" => $city['name_cit'],
        "country" => $country['name_cou']
    ]
];
$jsonSent['payment'] = [
    "reference" => $array[0],
    "description" => "Viajero",
    "amount" => [
        "taxes" => [[
            "kind" => $taxes[0][tax_name],
            "amount" => $taxesModified,
            "base" => $base
        ],
            [
                "kind" => "valueAddedTax",
                "amount" =>$iva,
                "base" => $base
            ]
        ],
        "currency" => "USD",
        "total" => $_GET['total_inv']
    ],
    "items" => $ad,
    "allowPartial" => false
];
$reference=$userInfoPtoP[0][reference];
$reference=base64_encode($reference);
$jsonSent['expiration'] = date('c', time() + 60 * 10);
$jsonSent['returnUrl'] = "http://www.pas-system.net/returnUrl.php?referencia=".$reference;
$jsonSent['userAgent']=$_SERVER['HTTP_USER_AGENT'];
$jsonSent['ipAddress']=$_SERVER['REMOTE_ADDR'];
$jsonSent['bandera']="viajero";

$saveRecord=Sales::insertRecordPayment($db,$array[0],$user['first_name_usr'],$user['last_name_usr'], $user['document_usr'],$user['email_usr'],
$user['address_usr'],$array[0],$taxes[0][tax_name],$_GET['total_inv'],"PENDING",$userInfoPtoP[0][erp_id],$_SESSION['serial_usr']);

//consume ws placetopay
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "http://52.87.250.190/placeToPay/public/api/infoUser",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($jsonSent),
    CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: ad44d741-7b34-486a-993a-4dbf458a7980"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    $obj = json_decode($response);
    header('Location:'.$obj->processUrl);
    echo $obj;
}


?>
