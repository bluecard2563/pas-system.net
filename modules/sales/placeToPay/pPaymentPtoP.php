<?php
/*
    Document   : pPaymentPtoP
    Created on : 20/08/2018
    Author     : Lenin Padilla
    Description: Get information user
*/

$userInfoPtoP = Sales::getDataUserPaymentPtoP($db, $_GET['serial_inv']);


$jsonSent = json_decode(null);
$taxes = unserialize($userInfoPtoP[0][taxes]);

$taxesModified = round((($userInfoPtoP[0][total] * $taxes[0][tax_percentage]) / 100), 2);
$base = ($userInfoPtoP[0][total] - $taxesModified);
$total = $taxesModified + $base;
$iva=0;
$street1=$userInfoPtoP[0][address]; 
$street2=str_replace(',','',$street1);
$jsonSent['locale'] = "es_EC";
$jsonSent['buyer'] = [
    "document" => $userInfoPtoP[0][document],
    "documentType" => "CI",
    "name" => utf8_encode($userInfoPtoP[0][name]),
    "surname" => utf8_encode($userInfoPtoP[0][surname]),
    // "surname" => null,
    "email" => $userInfoPtoP[0][email],
    //"email" => "hbk.lenin5@gmail.com",
    "address" => [
        "street" =>utf8_encode($street2),
        "city" => utf8_encode($userInfoPtoP[0][city]),
        "country" => utf8_encode($userInfoPtoP[0][country])
    ]
];
$jsonSent['payment'] = [
    "reference" => $userInfoPtoP[0][reference],
    "description" => "Viajero",
    "amount" => [
        "taxes" => [[
            "kind" => $taxes[0][tax_name],
            "amount" => $taxesModified,
            "base" => $base
        ],
            [
                "kind" => "valueAddedTax",
                "amount" => $iva,
                "base" => $base
            ]
        ],
        "currency" => "USD",
        "total" => $total
    ],
    "items" => [[
        "sku" => $userInfoPtoP[0][sku],
        "name" => utf8_encode($userInfoPtoP[0][name_product]),
        "category" => "viajero",
        "qty" => 1,
        "price" => $base,
        "tax" => $taxesModified
    ]],
    "allowPartial" => false
];
$reference=$userInfoPtoP[0][reference];
$reference=base64_encode($reference);
$jsonSent['expiration'] = date('c', time() + 60 * 10);
$jsonSent['returnUrl'] = "http://www.pas-system.net/returnUrl.php?referencia=".$reference;
$jsonSent['userAgent']=$_SERVER['HTTP_USER_AGENT'];
$jsonSent['ipAddress']=$_SERVER['REMOTE_ADDR'];
$jsonSent['bandera']="viajero";
 
if(empty($_GET['serial_usr'])){
    $saveRecord=Sales::insertRecordPayment($db,$userInfoPtoP[0][reference],$userInfoPtoP[0][name],$userInfoPtoP[0][surname], $userInfoPtoP[0][document],$userInfoPtoP[0][email],
        $userInfoPtoP[0][address],$userInfoPtoP[0][reference],$taxes[0][tax_name],$total,"PENDING",$userInfoPtoP[0][erp_id],$_SESSION['serial_usr']);
}else{
    $saveRecord=Sales::insertRecordPayment($db,$userInfoPtoP[0][reference],$userInfoPtoP[0][name],$userInfoPtoP[0][surname], $userInfoPtoP[0][document],$userInfoPtoP[0][email],
        $userInfoPtoP[0][address],$userInfoPtoP[0][reference],$taxes[0][tax_name],$total,"PENDING",$userInfoPtoP[0][erp_id],$_GET['serial_usr']);
}

//consume ws placetopay
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "http://52.87.250.190/placeToPay/public/api/infoUser",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($jsonSent),
    CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: ad44d741-7b34-486a-993a-4dbf458a7980"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    $obj = json_decode($response);
    header('Location:'.$obj->processUrl);
    echo $obj;
}
