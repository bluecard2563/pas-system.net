<?php 
/*
File: ajaxGetExtrasPrices.php
Author: Santiago Borja
Creation Date: 18/04/2010
*/
	$jsonData = get_object_vars(json_decode($_POST['jsonData']));
	global $adultMinAge;
	
	$allExtras = $jsonData['extras'];
	$adults = 0;
	$children = 0;
	$spouse = false;
	$outputExtras = array();
	foreach($allExtras as $key => $extra){
		$extraAge = GlobalFunctions::getMyAge($extra -> age, $jsonData['arrivalDate']); 
		$outputExtras[$key] = 'ADULT'; //DEFAULT
		if($extraAge >= $adultMinAge){
			$adults ++;
		}else{
			$children ++;
			$outputExtras[$key] = 'CHILD';
		}
		if($extra -> relationship == 'SPOUSE'){
			$spouse = true;
			$outputExtras[$key] = 'SPOUSE';
		}
	}
	
	$prices = GlobalFunctions::productCalculatePrice($db,	$jsonData['base_price'],
															$jsonData['base_cost'],
															$jsonData['adults_free'],// #
															$jsonData['percentage_adult'],// int
															$adults,// # - Including the Holder
															$jsonData['children_free'],// #
															$jsonData['percentage_children'],// int
															$children,//CHILDREN TRAVELING
															$spouse,//SPOUSE TRAVELING
															$jsonData['percentage_spouse'],
															$jsonData['max_extras_pro'],
															$jsonData['extras_restriction']);
	
	
	//CALCULATIONS:
	$adultsFree = $jsonData['adults_free'];
	$childrenFree = $jsonData['children_free'];
	foreach($outputExtras as &$oExtra){
		$priceExtra = 0;
		$costExtra = 0;
		switch($oExtra){
			case 'CHILD':
				if(--$childrenFree >= 0){
					$priceExtra = 0;
					$costExtra = 0;
				}else{
					$priceExtra = $prices['childrenPrice'];
					$costExtra = $prices['childrenCost'];
				}
				BREAK;
			case 'SPOUSE':
				if(--$adultsFree >= 0){
					$priceExtra = 0;
					$costExtra = 0;
				}else{
					$priceExtra = $prices['spousePrice'];
					$costExtra = $prices['spouseCost'];
				}
				break;
			default:
			case 'ADULT':
				if(--$adultsFree >= 0){
					$priceExtra = 0;
					$costExtra = 0;
				}else{
					$priceExtra = $prices['extrasPrice'];
					$costExtra = $prices['extrasCost'];
				}
				break;
		}
		$oExtra = array();
		$oExtra['price'] = $priceExtra;
		$oExtra['cost'] = $costExtra;
	}
	echo json_encode($outputExtras);
	
	//echo '<pre>'; print_r($jsonData); echo '</pre>';
	//echo '<pre>'; print_r($prices); echo '</pre>';
	//echo '<pre>'; print_r($outputExtras); echo '</pre>';
?>