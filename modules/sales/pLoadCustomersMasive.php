<?php
	/*
	Document		:	pLoadCustomersMasive.php
	Author			:	Miguel Ponce
	Modified date	:	24/12/2009
	Last modified	:	24/12/2009
	Description		:	Uploads a xls or xlsx file with a list of customers
	*					reads it and insert them into the database if they don�t exist.
	*					Then it links the customers with a travelers log.
	*/

	$serial_sal = $_POST['hdnSubSaleID'];
	$sale = new Sales($db, $serial_sal);
	$sale -> getData();
	$travelerLog = new TravelerLog($db);
	$travelerLog -> setSerial_sal($serial_sal);
	$travelerLog -> setSerial_cnt($sale -> getSerial_cnt());
	$travelerLog -> setStatus_trl('ACTIVE');
	$travelerLog -> setSerial_usr($_SESSION['serial_usr']);
	
	$response = GlobalFunctions::masiveSaleProcess($db, $_FILES, $sale -> getSerial_pbd());
	
	$sub_card_number = TravelerLog::getMaxCardNumberForSale($db, $sale -> getSal_serial_sal()) + 1;
	
	if($response[0] == 'success'){
		foreach($response[2] as $client){
			$travelerLog -> setSerial_cus($client['serial_cus']);
			$travelerLog -> setStart_trl(GlobalFunctions::changeDateFormat($client['start_trl']));
			$travelerLog -> setEnd_trl(GlobalFunctions::changeDateFormat($client['end_trl']));
			$travelerLog -> setDays_trl($client['travel_days']);
			$travelerLog -> setSerial_cit($client['destination_cit']);			
			$travelerLog -> setCard_number_trl($sale -> getCardNumber_sal().'_'.$sub_card_number++);

			if(!$travelerLog->insert()) {
				$response[0] = 'insert_error';
				break;
			}
		}
	}
	
	$error = $response[0];
	
	http_redirect('modules/sales/fChooseMasiveSales/'.$error);
?>