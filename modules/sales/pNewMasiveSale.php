<?php
/*
 * File: pNewSale.php
 * Author: Esteban Angulo
 * Creation Date: 02/03/2010
 * Modified By: Patricio Astudillo
 * Modification Date: 30/05/2011
 */

//Inserts New Customer Data
$customer = new Customer($db);
$document_cus = $_POST['txtDocumentCustomer'];

/* Checks if the Customer already is registered */
if ($customer -> existsCustomer($document_cus, NULL)) { //if the customer is registered. Proceed to sell
	$serial_cus = $customer -> getCustomerSerialbyDocument($document_cus);
	
	//********************************* UPDATING THE CUSTOMER'S INFO **********************************
	$customer -> setSerial_cit($_POST['selCityCustomer']);
	$customer -> setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
	$customer -> setPhone1_cus($_POST['txtPhone1Customer']);
	$customer -> setPhone2_cus($_POST['txtPhone2Customer']);
	$customer -> setCellphone_cus($_POST['txtCellphoneCustomer']);
	$customer -> setEmail_cus($_POST['txtMailCustomer']);
	$customer -> setStatus_cus('ACTIVE');	
	
	if ($_POST['txtNameCustomer']) {
		$customer -> setFirstname_cus(specialchars($_POST['txtNameCustomer']));
	}else if ($_POST['txtNameCustomer1']) {
		$customer -> setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
	}

	if ($_POST['txtAddress']) {
		$customer -> setAddress_cus(specialchars($_POST['txtAddress']));
	}else if ($_POST['txtAddress1']) {
		$customer -> setAddress_cus(specialchars($_POST['txtAddress1']));
	}

	if ($_POST['txtRelative']) {
		$customer -> setRelative_cus(specialchars($_POST['txtRelative']));
		$customer -> setRelative_phone_cus($_POST['txtPhoneRelative']);
	}else if ($_POST['txtManager']) {
		$customer -> setRelative_cus($_POST['txtManager']);
		$customer -> setRelative_phone_cus($_POST['txtPhoneManager']);
	}

	if ($_POST['hdnVip']) {
		$customer -> setVip_cus($_POST['hdnVip']);
	} else {
		$customer -> setVip_cus('0');
	}
	$customer -> updateInfoSale($serial_cus);

	//******************************************** GATTERING THE INFO FOR THE E-MAIL **********************
	if ($_POST['txtNameCustomer']) {
		$misc['textForEmail'] = 'Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';

		$misc['customer'] = $_POST['txtNameCustomer'] . ' ' . $_POST['txtLastnameCustomer'];
	}else if ($_POST['txtNameCustomer1']) {
		$misc['customer'] = $_POST['txtNameCustomer1'];
	}
	
	$customer -> setserial_cus($serial_cus);
	$customer -> getData();
	$password = $customer -> getPassword_cus();
	
	$misc['username'] = $customer -> getEmail_cus();
	$misc['pswd'] = 'Su clave es la misma de la &uacute;ltima vez.';
	$misc['rememberPass'] = 'En caso de que no recuerde su clave ingrese a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y haga uso de nuestro servicio: "Olvide mi clave" en la parte superior derecha de la pantalla.';
	$misc['email'] = $customer -> getEmail_cus();
	
	//********************************************** EXCECUTING THE SALE *********************************
	$sale_result = executeSale($_POST, $serial_cus);
	if (is_array($sale_result)) {
		$misc['subject'] = 'REGISTRO DE NUEVA VENTA MASIVA';
		$error = $sale_result['error'] . '/2/' . $sale_result['saleID'];
		$misc['cardNumber'] = $sale_result['cardNumber'];
		$misc['urlGeneralConditions'] = $sale_result['urlGenCond'];
		$serial_sal = $sale_result['saleID'];
		$misc['contractFilePath'] = $sale_result['pathContract'];
		
		
		
	} else {
		$error = $sale_result['error'];
	}
	
	http_redirect('main/masive_sales/' . $error);
} else {//if the customer is not registered. Inserts the new client and the proceed to sell
	$customer -> setSerial_cit($_POST['selCityCustomer']);
	$customer -> setType_cus($_POST['selType']);
	$customer -> setDocument_cus($_POST['txtDocumentCustomer']);

	if ($_POST['txtNameCustomer']) {
		$customer -> setFirstname_cus(specialchars($_POST['txtNameCustomer']));
	}

	if ($_POST['txtNameCustomer1']) {
		$customer -> setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
	}

	$customer -> setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
	$customer -> setBirthdate_cus($_POST['txtBirthdayCustomer']);
	$customer -> setPhone1_cus($_POST['txtPhone1Customer']);
	$customer -> setPhone2_cus($_POST['txtPhone2Customer']);
	$customer -> setCellphone_cus($_POST['txtCellphoneCustomer']);
	$customer -> setEmail_cus($_POST['txtMailCustomer']);

	if ($_POST['txtAddress']) {
		$customer -> setAddress_cus(specialchars($_POST['txtAddress']));
	}else if ($_POST['txtAddress1']) {
		$customer -> setAddress_cus(specialchars($_POST['txtAddress1']));
	}

	if ($_POST['txtRelative']) {
		$customer -> setRelative_cus($_POST['txtRelative']);
		$customer -> setRelative_phone_cus($_POST['txtPhoneRelative']);
	}else if ($_POST['txtManager']) {
		$customer -> setRelative_cus(specialchars($_POST['txtManager']));
		$customer -> setRelative_phone_cus($_POST['txtPhoneManager']);
	}

	if ($_POST['hdnVip']) {
		$customer -> setVip_cus($_POST['hdnVip']);
	} else {
		$customer -> setVip_cus('0');
	}

	$customer -> setStatus_cus('ACTIVE');
	$password = GlobalFunctions::generatePassword(6, 8, false);
	$customer -> setPassword_cus(md5($password));

	$serialNewCus = $customer -> insert();
	
	if ($serialNewCus) {
		/* Generating the info for sending the e-mail */
		if ($_POST['txtNameCustomer']) {
			$misc['customer'] = $_POST['txtNameCustomer'] . ' ' . $_POST['txtLastnameCustomer'];
		}else if ($_POST['txtNameCustomer1']) {
			$misc['customer'] = $_POST['txtNameCustomer1'];
		}
		$misc['textForEmail'] = 'Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
		$misc['username'] = $customer -> getEmail_cus();
		$misc['pswd'] = $password;
		$misc['email'] = $customer -> getEmail_cus();
		/* END E-mail Info */

		$sale_result = executeSale($_POST, $serialNewCus);
		
		if (is_array($sale_result)) {
			$error = $sale_result['error'] . '/2/' . $sale_result['saleID'];
		} else {
			$error = $sale_result['error'];
		}
	} else { //Errors inserting a new Customer.
		$error = 6;		
	}
}

http_redirect('main/masive_sales/' . $error);

/**
 *
 * @global type $db
 * @param type $data
 * @param type $serial_cus
 * @return string 
 */
function executeSale($data, $serial_cus) {
	global $db;
	$sale = new Sales($db);
	//Debug::print_r($data);

	$sale -> setSerial_cus($serial_cus);
	$sale -> setSerial_pbd($data['selProduct']);

	$counter = new Counter($db);
	if ($data['hdnSerial_cntRPC']) {
		$sale -> setSerial_cnt($data['hdnSerial_cntRPC']);
	} else {
		$sale -> setSerial_cnt($counter -> counterExists($_SESSION['serial_usr']));
	}

	$nextAvailableNumber = Stock::getNextStockNumber($db, true);
	if (!$nextAvailableNumber) {
		http_redirect('main/sales/19');
	}

	$sale -> setCardNumber_sal($nextAvailableNumber);
	$sale -> setEmissionDate_sal($data['txtEmissionDate']);
	$sale -> setBeginDate_sal($data['txtDepartureDate']);
	$sale -> setEndDate_sal($data['txtArrivalDate']);
	$sale -> setInDate_sal(date("d/m/Y"));
	$sale -> setDays_sal($data['hddDays']);
	$sale -> setFee_sal($data['hdnPrice']);
	$sale -> setObservations_sal('VENTA MASIVA ' . specialchars($data['observations']));
	$sale -> setCost_sal($data['hdnCost']);
	$sale -> setFree_sal('NO');
	$sale -> setStatus_sal('REGISTERED');

	//$sale -> setIdErpSal_sal();
	$sale -> setType_sal('SYSTEM');
	$sale -> setStockType_sal('VIRTUAL');
	if ($data['hdnDirectSale']) {
		$sale -> setDirectSale_sal('YES');
	} else {
		$sale -> setDirectSale_sal('NO');
	}
	$sale -> setTotal_sal($data['hdnTotal']);
	$sale -> setTotalCost_sal($data['hdnTotalCost']);
	$sale -> setChange_fee_sal($data['rdCurrency']);
	if ($data['selCityDestination']) {
		$sale -> setSerial_cit($data['selCityDestination']);
	} else {
		$sale -> setSerial_cit('NULL');
	}

	$sale -> setInternationalFeeStatus_sal('PAID');
	$sale -> setInternationalFeeUsed_sal('YES');
	$sale -> setInternationalFeeAmount_sal($data['hdnTotalCost']);

	$saleID = $sale -> insert();
	//INITIALIZE ERROR MESSAGE
	$sale_result['error'] = 7;

	if ($saleID) {		
		/* GENERAL CONDITIONS FILE */
		$country_of_sale = Sales::getCountryofSale($db, $saleID);
		if ($country_of_sale != 62) {
			$country_of_sale = 1;
		}
		$sale_result['urlGenCond'] = $sale -> getGeneralConditionsbySale($data['selProduct'], $saleID, $country_of_sale, $_SESSION['serial_lang']);
		/* END GENERAL CONDITIONS FILE */
		
		$sale_result['error'] = 4;
		$sale_result['cardNumber'] = $nextAvailableNumber;
		$sale_result['saleID'] = $saleID;
		$sale_result['urlContract'] = 'contract_' . $saleID . '.pdf';
	}
	
	return $sale_result;
}

?>
