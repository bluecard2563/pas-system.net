<?php
/**
 * Created by PhpStorm.
 * User: lpadilla
 * Date: 4/10/2018
 * Time: 9:35
 */
$recordPayment=Sales::getRecordPaymentPending($db,$_SESSION['serial_usr']);
//http://sandbox.pas-system.net/modules/sales/pPaymentRecordTandi.php
foreach ($recordPayment as $rp){
    if($rp['estado_de_pago']=="APPROVED" && ($rp['shipping_status']=="PENDING" || $rp['shipping_status']=="NOT_SEND" || $rp['shipping_status']==null)){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://54.159.232.149/placeToPay/public/api/getPaymentRecords/".$rp['referencia'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Postman-Token: b41586c2-4c62-4f16-8d48-2e17d354d2e6"
            ),
        ));
        $responsePaymentRecords = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            //Create data for authentication in place to pay
            //@param $secretKey is trankey provided by place to pay
            $secretKey = "1KwOcMNbX0r7np17";
            //@param $login is provided by place to pay
            $login = "0d48f5702980f31c9fa830eaac53bcc6";
            $seed = date('c');
            if (function_exists('random_bytes')) {
                $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
                $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
                $nonce = mt_rand();
            }
            $nonceBase64 = base64_encode($nonce);
            $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

            $jsonAuth['auth'] = [
                "login" => $login,
                "seed" => $seed,
                "nonce" => $nonceBase64,
                "tranKey" => $tranKey
            ];

            $editedJson = json_encode($jsonAuth);
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://test.placetopay.ec/redirection/api/session/" . $responsePaymentRecords,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $editedJson,
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "Postman-Token: 7dcbb9bf-a0de-4099-8d32-48cd4d91c9b0"
                ),
            ));
            $responseRedirection = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            }else{
                $responseEdit = json_decode($responseRedirection,true);
                $responseEdit['request']['payment']['reference'];
                $responseEdit['status']['status'];
                $responseEdit['requestId'];
                // Debug::print_r($responseEdit['payment'][0]['processorFields'][5]['value']);die();
                // Debug::print_r($responseEdit);die();
                $bodyTandi = json_decode(null);
                $bodyTandi['cajaId'] = "10749382675405";
                $bodyTandi['cobradorId']="1";
                $bodyTandi['concepto']="Pago placeTopay";
                $bodyTandi['conceptoDebito']=null;
                $bodyTandi['conceptoTrans']="Pago placeTopay";
                $bodyTandi['cuentaCheque']=null;
                $bodyTandi['host']=null;
                $bodyTandi['idBanco']=null;
                $bodyTandi['idComisionTarjeta']="10729573779117";
                $bodyTandi['idConvenioDebito']=null;
                $bodyTandi['idCuentaBancariaDebito']=null;
                $bodyTandi['idCuentaBancariaTrans']=null;
                $bodyTandi['idTarjetaCredito']="10729573778932";
                $bodyTandi['importePagoTotal']=$rp['total'];
                //$bodyTandi['numeroAutorizacion']=$responseEdit['payment']['authorization']$rt['authorization'];
                $bodyTandi['numeroAutorizacion']=$responseEdit['payment'][0]['authorization'];
                $bodyTandi['numeroCheque']=null;
                $bodyTandi['numeroCuentaTarjetaDebito']=null;
                $bodyTandi['numeroDocumentoDebito']=null;
                //$bodyTandi['numeroDocumentoTrans']=$rp['requestID'];
                $bodyTandi['numeroDocumentoTrans']=$responseEdit['requestId'];
                //$bodyTandi['numeroTarjeta']=$rt['card_number'];
                $card_number="XXXX-XXXX-XXXX-".$responseEdit['payment'][0]['processorFields'][5]['value'];
                $bodyTandi['numeroTarjeta']=$card_number;
                //$bodyTandi['numeroVoucher']=$rt['receipt'];
                $bodyTandi['numeroVoucher']=$responseEdit['payment'][0]['receipt'];
                $bodyTandi['pass']=null;
                $bodyTandi['puerto']=null;
                $bodyTandi['recibimosDe']=null;
                $bodyTandi['tipoCuentaDebito']=null;
                $bodyTandi['user']=null;
                $bodyTandi['valorCheque']=0;
                $bodyTandi['valorDebito']=0;
                $bodyTandi['valorTarjeta']=$rp['total'];
                $bodyTandi['valorTrans']=0;
                $bodyTandi['valorEfectivo']=0;
                $bodyTandi['valorRetencion']=0;
                $bodyTandi['tipoRetencionId']=null;
                $bodyTandi['numeroRetencion']=null;
                $bodyTandi['numeroAutorizacionRetencion']=null;
                $bodyTandi['numeroImprentaRetencion']=null;
                $bodyTandi['fechaEmisionRetencion']=null;
                $bodyTandi['fechaRecepcionRetencion']=null;
                $bodyTandi['fechaValidaHastaRetencion']=null;
                $bodyTandi['facturaId']=$rp['erp_id'];

//              Consumo tandi
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_PORT => "8087",
                    CURLOPT_URL => "http://18.234.9.61:8087/pago/put",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 3000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($bodyTandi),
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Basic cm9zczpyb3Nz",
                        "cache-control: no-cache",
                        "content-type: application/json"
                    ),
                ));
                $responseTandiPay = curl_exec($curl);
                $responseTandi = json_decode($responseTandiPay,true);
                $err = curl_error($curl);
                curl_close($curl);


                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {

                    if($responseTandi['codigo']==0){
                        $serial_mbc=ManagerbyCountry::getSerialMbc($db,$rp['referencia']);
                        $tandi_Id=ManagerbyCountry::getTandiId($serial_mbc);
                        $user = new User($db, $_SESSION['serial_usr']);
                        $serial_usr = $_SESSION['serial_usr'];
                        $user->getData();
                        $user = get_object_vars($user);
                        $user['document_usr'];
                        $payment_date = date("Y-m-d");
                        $id_factura=$rp['erp_id'];
                        $userInfoPtoP = Sales::getDataUserPaymentPtoP($db, $rp['referencia']);
                        $numero_factura=$userInfoPtoP[0][number_inv];
                        $payment_number=$responseEdit['payment'][0]['receipt'];
                        $obs_payment="PAGO PLACETOPAY DE LA FACTURA ".$numero_factura;
                        $type_payment="CREDIT_CARD";
                        $payment_value=$rp['total'];
                        $estado_payment="PAID";
                        $bodyUpdateBas = json_decode(null);
                        $bodyUpdateBas['payment_date']=$payment_date;
                        $bodyUpdateBas['obs_payment']=$obs_payment;
                        $bodyUpdateBas['cedu_user']=$user['document_usr'];
                        $bodyUpdateBas['facturas']=[[
                            "id_factura"=>$id_factura,
                            "numero_factura"=>$numero_factura,
                            "tandi_id"=>$tandi_Id,
                            "pagos"=>[[
                                "type_payment"=>$type_payment,
                                "payment_number"=>$payment_number,
                                "payment_value"=>$payment_value
                            ]],
                            "estado_payment"=>$estado_payment
                        ]];

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://sandbox.pas-system.net/wsdl/tandi/paymentTandi",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($bodyUpdateBas),
                            CURLOPT_HTTPHEADER => array(
                                "Authorization: Basic Zjc2OTAxZjMzYjE1YzI4MWNjZWYxYmNkMjdkOGMxZWU6ZDM0ZjE4MDA3YTczN2NmODY0MmEwZGRiYTAzNDlmZTg=",
                                "Content-Type: application/json",
                                "cache-control: no-cache"
                            ),
                        ));

                        $responseUpdateWSBAS = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);

                        if ($err) {
                            echo "cURL Error #:" . $err;
                        } else {
                            echo $responseUpdateWSBAS;
                        }
                        $json_Log_send=serialize($bodyTandi);
                        $json_Log_receive=serialize($responseTandi);
                        $json_Log_update_bas=serialize($bodyUpdateBas);
                        $resUpdateWSBAS=serialize($responseUpdateWSBAS);
                        $registerToUpdate=Sales::getRegisterUpdate($db,$responseEdit['request']['payment']['reference']);
                        $registerUpdate=Sales::updateRecordPaymentTandi($db,$responseEdit['status']['status'],$registerToUpdate[0]['serial_rp'],"SENT",$json_Log_send,$json_Log_receive,"SUCCESS",$json_Log_update_bas,$resUpdateWSBAS);
                    }
                    elseif ($responseTandi['codigo']==1){
                        $json_Log_send=serialize($bodyTandi);
                        $json_Log_receive=serialize($responseTandi);
                        $registerToUpdate=Sales::getRegisterUpdate($db,$responseEdit['request']['payment']['reference']);
                        $registerUpdate=Sales::updateRecordPaymentTandi($db,$responseEdit['status']['status'],$registerToUpdate[0]['serial_rp'],"SENT",$json_Log_send,$json_Log_receive,"ERROR");

                    }

                }
            }
        }
    }
}
http_redirect('modules/sales/fPaymentRecordTandi');
$smarty->register('recordPayment');
$smarty->display();