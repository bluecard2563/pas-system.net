<?php

/*
  File: pNewSale.php
  Author: Santiago Borja
  Completeley redefined @19/01/2011
 */

if (!$_POST['selProduct']) {
    ErrorLog::log($db, 'SALE FAULT - NO POST', $_POST);
    http_redirect('main/sales/7');
}

$customer = new Customer($db);
$document_cus = $_POST['txtDocumentCustomer'];
$sessionLanguage = $_SESSION['serial_lang'];
$questionList = Question::getActiveQuestions($db, $sessionLanguage);

//FIX NON-EXISTING VAR
if (!isset($_POST['rdIsFree'])) {
    $_POST['rdIsFree'] = 'NO';
}

//INSERT DATA INTO CUSTOMER OBJECT
$customer->setSerial_cit($_POST['selCityCustomer']);
if ($_POST['txtNameCustomer']) {
    $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
} else {
    $customer->setFirstname_cus(specialchars($_POST['txtNameCustomer1']));
}

if ($_POST['selType'] == 'LEGAL_ENTITY') {
    $_POST['txtLastnameCustomer'] = "";
    $_POST['txtBirthdayCustomer'] = "";
}

$customer->setPhone1_cus($_POST['txtPhone1Customer']);
$customer->setPhone2_cus($_POST['txtPhone2Customer']);
$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
$customer->setEmail_cus($_POST['txtMailCustomer']);
$customer->setDocument_cus($document_cus);
$customer->setAddress_cus(specialchars($_POST['txtAddress']));
$customer->setRelative_cus(specialchars($_POST['txtRelative']));
$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
$customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
$customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
$customer->setGender_cus($_POST['rdGender']);

if ($_POST['hdnVip']) {
    $customer->setVip_cus($_POST['hdnVip']);
} else {
    $customer->setVip_cus('0');
}

$customer->setStatus_cus('ACTIVE');
$customer->setType_cus($_POST['selType']);


//SAVE OR UPDATE THE CUSTOMER INFO
if ($customer->existsCustomer($document_cus, NULL)) {
    $serial_cus = $customer->getCustomerSerialbyDocument($document_cus);
    $customer->updateInfoSale($serial_cus);

    $customer->setserial_cus($serial_cus);
    $customer->getData();
} else {
    $password = GlobalFunctions::generatePassword(6, 8, false);
    $customer->setPassword_cus(md5($password));
    $serial_cus = $customer->insert();

    if (!$serial_cus) {
        $error = 6; //Customer not inserted
        ErrorLog::log($db, 'FAILED TO INSERT NEW HOLDER CUSTOMER:', $customer);
        http_redirect('main/sales/6'); //Errors inserting a new Customer.
    }
}
//Other Customer for Invoice
if($_POST['bill_to_radio'] == 'OTHER'){
    $customerOther = new Customer($db);
    $document_cusOther = $_POST['txtOtherDocument'];
    $sessionLanguage = $_SESSION['serial_lang'];
    $questionList = Question::getActiveQuestions($db, $sessionLanguage);

    //FIX NON-EXISTING VAR
    if (!isset($_POST['rdIsFree'])) {
        $_POST['rdIsFree'] = 'NO';
    }

    //INSERT DATA INTO CUSTOMER OBJECT
    $customerOther->setSerial_cit($_POST['selOtherCity']);
    $customerOther->setFirstname_cus(specialchars($_POST['txtOtherFirstName']));

    if ($_POST['selTypeOther'] == 'LEGAL_ENTITY') {
        $_POST['txtLastnameCustomer'] = "";
        $_POST['txtBirthdayCustomer'] = "";
    }

    $customerOther->setPhone1_cus($_POST['txtOtherPhone1']);
    $customerOther->setPhone2_cus($_POST['txtOtherPhone2']);
    $customerOther->setEmail_cus($_POST['txtOtherEmail']);
    $customerOther->setDocument_cus($document_cusOther);
    $customerOther->setAddress_cus(specialchars($_POST['txtOtherAddress']));
    $customerOther->setLastname_cus(specialchars($_POST['txtOtherLastName']));
    $customerOther->setBirthdate_cus($_POST['txtBirthdayOtherCustomer']);
    $customerOther->setGender_cus($_POST['rdOtherGender']);

    if ($_POST['hdnVip']) {
        $customerOther->setVip_cus($_POST['hdnVip']);
    } else {
        $customerOther->setVip_cus('0');
    }

    $customerOther->setStatus_cus('ACTIVE');
    $customerOther->setType_cus($_POST['selType']);


    //SAVE OR UPDATE THE CUSTOMER INFO
    if ($customerOther->existsCustomer($document_cusOther, NULL)) {
        $serial_cusOther = $customerOther->getCustomerSerialbyDocument($document_cusOther);
        $customerOther->updateInfoSale($serial_cusOther);

        $customerOther->setserial_cus($serial_cusOther);
        $customerOther->getData();
    } else {
        $password = GlobalFunctions::generatePassword(6, 8, false);
        $customerOther->setPassword_cus(md5($password));

        $serial_cusOther = $customerOther->insert();
        $customerOther->getData();

        if (!$serial_cusOther) {
            $error = 6; //Customer not inserted
            ErrorLog::log($db, 'FAILED TO INSERT NEW HOLDER CUSTOMER:', $customer);
            http_redirect('main/sales/6'); //Errors inserting a new Customer.
        }
    }
    
}

//REGISTER THE USER'S QUESTIONS
Answer::deleteMyAnswers($db, $serial_cus);

foreach ($questionList as $q) {
    $answer = new Answer($db);
    $answer->setSerial_cus($serial_cus);
    $answer->setSerial_que($q['serial_que']);
    $ans = $_POST['cust_answer_' . $q['serial_qbl']];

    if ($ans) {
        $answer->setYesno_ans($ans);
        if (!$answer->insert()) {
            ErrorLog::log($db, 'FAILED TO INSERT ANSWER, HOLDER CUSTOMER:' . $serial_cus, $answer);
            break;
        }
    } else {
        break;
    }
}

//BUILD UP THE SALEDATA ARRAY
$saleData['tit']['last_name_cus'] = $_POST['txtLastnameCustomer'];
$saleData['tit']['first_name_cus'] = $_POST['txtNameCustomer'];
$saleData['tit']['document_cus'] = $_POST['txtDocumentCustomer'];
$saleData['tit']['birthdate_cus'] = $_POST['txtBirthdayCustomer'];
$saleData['tit']['email_cus'] = $_POST['txtLastnameCustomer'];
$saleData['tit']['address_cus'] = $_POST['txtLastnameCustomer'];
$saleData['tit']['phone_cus'] = $_POST['txtPhone1Customer'];
$saleData['tit']['relative_cus'] = $_POST['txtRelative'];
$saleData['tit']['relative_phone_cus'] = $_POST['txtPhoneRelative'];


//*************************	UNDER AGE REPRESENTANTIVE SECTION ************************
if(isset($_POST['repDocument']) && $_POST['repDocument'] != ""){
	$repCus = new Customer($db, $_POST['repSerialCus']);
	if($repCus->getData()){
		$repSerial = $repCus->getSerial_cus();
	}else{
		$repCus->setDocument_cus($_POST['repDocument']);
		$repCus->setFirstname_cus($_POST['repName']);
		$repCus->setLastname_cus($_POST['repLastname']);
		$repCus->setBirthdate_cus($_POST['repBirthday']);
		$repCus->setEmail_cus($_POST['repEmail']);
		$repCus->setSerial_cit($_POST['selCityRepresentative']);
		$repCus->setAddress_cus($customer->getAddress_cus());
		$repCus->setPhone1_cus($customer->getPhone1_cus());
		
		$repSerial = $repCus->insert();
	}
	
	$saleData['tit']['rep_cus'] = $repSerial;
}
//EXECUTE THE SALE
$ret = executeSale($_POST, $serial_cus, $saleData);

if (is_array($ret)) { //IF IT IS A SUCCESSFUL SALE
    $serial_sal = $ret['saleID'];

//    ERP_logActivity('SALES', $serial_sal); //ERP ACTIVITY

    /* CUSTOMER PROMO SECTION */
    if ($_POST['txtCoupon'] != '') {
        $product_information = Sales::getSoldProductInformation($db, $serial_sal);
        $code_parts = split('-', $_POST['txtCoupon']);
        $serial_cup = Cupon::validateCupon($db, trim($code_parts['1']), strtoupper(trim($code_parts['0'])), $product_information['serial_pxc']);

        if ($serial_cup) {
            $data = Cupon::getCuponData($db, $serial_cup);
            $promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);

            $result = Cupon::reclaimCupon($db, $serial_cup, $serial_sal, $_SESSION['serial_usr']);
            if ($result['process_code'] == 'success' && $result['serial_cup'] != '') {
                $_SESSION['serial_cup'] = $result['serial_cup'];
            }
        }
    }
    $finalArray = array();
    $array['serial_gcn'] = '7';
    $array['serial_pro'] = '102';
    $array['serial_pbd'] = '42778';
    $array['url_glc'] = 'general_condition_477.pdf';
    $finalArray[0] = $array;
//    Debug::print_r($finalArray);
//    Debug::print_r($ret);die();
    /* CUSTOMER PROMO SECTION */
	
    //SEND THE CORRESPONDING EMAIL
    $misc['textForEmail'] = 'Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href="' . URL . '">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
    $misc['customer'] = $_POST['txtNameCustomer'] . ' ' . $_POST['txtLastnameCustomer'];
    $misc['username'] = $customer->getEmail_cus();
    $misc['pswd'] = 'Su clave es la misma de la &uacute;ltima vez.';
    $misc['email'] = $customer->getEmail_cus();
    $misc['rememberPass'] = 'En caso de que no recuerde su clave ingrese a nuestra p&aacute;gina web <a href="' . URL . '">www.planet-assist.net</a> y haga uso de nuestro servicio: "Olvid&eacute; mi clave" en la parte superior derecha de la pantalla.';
    $misc['subject'] = 'NOTIFICACION DE COMPRA';

    $misc['cardNumber'] = $ret['cardNumber'];
    $misc['urlGeneralConditions'] = $ret['urlGenCond'];
    $misc['contractFilePath'] = $ret['pathContract'];
    $misc['urlContract'] = $ret['urlContract'];
    $misc['urlYearlyContract'] = $ret['urlYearlyContract'];
//    debug::print_r($misc);die();
    if ($_POST['txtMailCustomer'] != '' && $_POST['rdIsFree'] == 'NO') {
		if(Sales::isPlanetAssistSale($db, $serial_sal)){
			GlobalFunctions::sendMail($misc, 'newClient');
		}else{
			GlobalFunctions::sendMail($misc, 'new_sale');
//                        Debug::print_r($misc);die();
		}
        
        $error = 4; //Complete success
    } else {
        $error = 4; //Success but email not sent
    }
} else {
    ErrorLog::log($db, 'FAILED TO INSERT SALE!!', $ret);
    $error = 7; //Sale not executed
}

//****************** CUSTOM ON LINE INVOICING ****************
$customInvoiceData = array();
if($_POST['rdInvoicing'] == 'OTHER'){
	$customInvoiceData['ID'] = $_POST['hdnSerialCusRPC'];
	$customInvoiceData['document'] = $_POST['txtCustomerDocument'];
	$customInvoiceData['first_name'] = $_POST['txtFirstName'];
	$customInvoiceData['last_name'] = $_POST['txtLastName'];
	$customInvoiceData['address'] = $_POST['txtAddress'];
	$customInvoiceData['phone1'] = $_POST['txtPhone1'];
	$customInvoiceData['phone2'] = $_POST['txtPhone2'];
	$customInvoiceData['email'] = $_POST['txtEmail'];
	$customInvoiceData['countryID'] = $_POST['selCountry'];
	$customInvoiceData['cityID'] = $_POST['selCity'];
}

$invoiceData = ERPConnectionFunctions::getInvoiceBasicDataForSale($db, $serial_sal);

$sendingData = array();
$sendingData['txtCustomerDocument'] = $_POST['txtCustomerDocument'];
$sendingData['hdnSaleId'] = array();
array_push($sendingData['hdnSaleId'], $serial_sal);
if($invoiceData['type_percentage_dea'] == 'DISCOUNT'){
    if ($_POST['bill_to_radio'] == 'CUSTOMER' || $_POST['bill_to_radio'] == 'OTHER') {
            $sendingData['txtDiscount'] = 0;
    } else {
    $sendingData['txtDiscount'] = $invoiceData['percentage_dea'];
}
}else{
    $sendingData['txtDiscount'] = 0;
}
$sendingData['txtOtherDiscount'] = 0;
$sendingData['extras'] = array();
if($_POST['extra']){
    foreach($_POST['extra'] as $key => $extra) {
        $extInv = TandiGlobalFunctions::getExtrasByDocument($db, $extra['idExtra']);
        $tempExtra['serial_cus'] = $extInv['serial_cus'];
        $tempExtra['fee_ext'] = $extInv['fee_ext'];
        array_push($sendingData['extras'], $tempExtra);
    }
}
$sendingData['hdnSerialMan'] = $invoiceData['serial_man'];
if($_POST['bill_to_radio'] == 'CUSTOMER'){
    $sendingData['hdnSerialCus'] = $serial_cus;
    $sendingData['invHolder'] = $serial_cus;
}else if($_POST['bill_to_radio'] == 'OTHER'){
    $sendingData['hdnSerialCus'] =  $serial_cusOther;
    $sendingData['invHolder'] =  $serial_cusOther;
}
$sendingData['hdnDealerDocument'] = $_POST['hdnDealerDocument'];
if($_POST['bill_to_radio'] == 'DEALER'){
    $sendingData['bill_to_radio'] = $_POST['bill_to_radio'];
    $dealer = TandiGlobalFunctions::dealerIDExists($db, $_POST['hdnDealerDocument']);
    $Dealer = new Dealer($db, $dealer[0]['serial_dea']);
    $Dealer->getData();
    $sendingData['invHolder'] = $Dealer->getSerial_dea();
}else{
    $sendingData['bill_to_radio'] = 'CUSTOMER';
} 
//TAXES
$sale = new Sales($db, $serial_sal);
$sale->getData();
$productByDealer = new ProductByDealer($db, $sale->getSerial_pbd());
$productByDealer->getData();
$taxes = $productByDealer->getTaxes();
$taxesToA = 0;

$_POST['hdnNameTax'] = array();
$_POST['hdnPercentageTax'] = array();
if (is_array($taxes)) {
        foreach ($taxes as $t) {
                $tempTaxName = $t['name_tax'];
                $tempTaxValue = $t['percentage_tax'];
                array_push($_POST['hdnNameTax'], $tempTaxName);
                array_push($_POST['hdnPercentageTax'], $tempTaxValue);
        }
}
//Debug::print_r($_POST);
//Debug::print_r($customer);
//Debug::print_r($sendingData);die();
if($_POST['selectInv'] == 'autoInv'){
    //Invoice TANDI
    $invoice = Invoice::replicateAutomaticInvoiceForSale($db, $serial_sal, $_POST, $sendingData);
    if($invoice == NULL){
        $invoice = 99;
    }


if ($error == 4) {
    if ($_POST['hdnFlights'] == 'YES') {
        $base_error = 15;
    } else {
        $base_error = 4;
    }
    if ($_POST['rdIsFree'] == 'YES') {
            http_redirect("main/sales/$base_error/2". "/".$invoice);
        } else {
        http_redirect("main/sales/$base_error/2/" . $ret['saleID'] . "/".$invoice);
        }
    }else{
        http_redirect('main/sales/' . $error.'/2/'.$ret['saleID']);
    }    
}else{
    if ($error == 4) {
        if ($_POST['hdnFlights'] == 'YES') {
            $base_error = 15;
        } else {
            $base_error = 4;
        }
        if ($_POST['rdIsFree'] == 'YES') {
        http_redirect("main/sales/$base_error/2");
    } else {
        http_redirect("main/sales/$base_error/2/" . $ret['saleID']);
    }
    }else{
        http_redirect('main/sales/' . $error.'/2/'.$ret['saleID']);
}
}


/* * *********************************************
 * function executeSale
  @Name: executeSale
  @Description: executs the proccess of a sale
  @Params:
 *       N/A
  @Returns: true
 *        false
 * ********************************************* */

function executeSale($data, $serial_cus, $saleData) {
    global $db;
    
    if(CONTRACT_LOGIC){
        //Contract
        $serial_con = null;
        $begin_date = new DateTime();
        $end_date = new DateTime();
		
		//IF THE SALE IS FOR AN UNDERAGE CUSTOMER, TAKE REP ID FOR CONTRACT
		if(isset($saleData['tit']['rep_cus'])){ 
			$contractHolder = $saleData['tit']['rep_cus'];
		}else{
			$contractHolder = $serial_cus;
		}

        $contracts_cus = Contracts::getContractByCustomer($db, $contractHolder, 'ACTIVE');
        if ($contracts_cus && $contracts_num = count($contracts_cus)>0) {
            $departure_date = DateTime::createFromFormat('d/m/Y', $data['txtDepartureDate'])->getTimestamp();
            $arrival_date = DateTime::createFromFormat('d/m/Y', $data['txtArrivalDate'])->getTimestamp();
            $contracts_con = 0;

            foreach ($contracts_cus as $key => $con){
                $contracts_con++;
                $contract_beg = DateTime::createFromFormat('d/m/Y', $con['begin_date'])->getTimestamp();
                $contract_end = DateTime::createFromFormat('d/m/Y', $con['end_date'])->getTimestamp();

                if ($contract_beg <= $departure_date && $contract_end >= $arrival_date){
                    $serial_con = $con['serial_con'];
                    break;
                } elseif ($contracts_con == $contracts_num && $departure_date < $contract_end){
                    $begin_date->setTimestamp($departure_date);
                    $end_date->setTimestamp($departure_date);
                }
            }
        }

        //Create Contract
        $contract = new Contracts($db, $serial_con);
        if (!$contract->getBegin_date_con() && !$contract->getEnd_date_con() && !$contract->getNumber_con()){
            $contract->setSerial_cof(1);
            $contract->setSerial_cus($contractHolder);

            $nextAvailableContract = Stock::getNextContractNumber($db);
            $end_date->add(new DateInterval('P1Y'));

            $contract->setNumber_con($nextAvailableContract);
            $contract->setBegin_date_con($begin_date->format('d/m/Y'));
            $contract->setEnd_date_con($end_date->format('d/m/Y'));
            $contract->setStatus_con('ACTIVE');
            $serial_con = $contract->insert();
        }
    }
    
    //Sale
    $sale = new Sales($db);
    $sessionLanguage = $_SESSION['serial_lang'];

    $questionList = Question::getActiveQuestions($db, $sessionLanguage);
    
    $sale->setSerial_con($serial_con);
    $sale->setSerial_cus($serial_cus);
    $sale->setSerial_pbd($data['selProduct']);
    $sale->setSerial_cur($_POST['serial_cur']);
    $counter = new Counter($db);
    if ($data['hdnSerial_cntRPC']) {
        $sale->setSerial_cnt($data['hdnSerial_cntRPC']);
    } else {
        $sale->setSerial_cnt($counter->counterExists($_SESSION['serial_usr']));
    }

    if ($data['sale_type'] == 'VIRTUAL') {
        //REGEN THE NUMBER:
        $myPbd = new ProductByDealer($db, $data['selProduct']);
        $myPbd->getData();
        $myPxc = new ProductByCountry($db, $myPbd->getSerial_pxc());
        $myPxc->getData();
        $myPro = new Product($db, $myPxc->getSerial_pro());
        $myPro->getData();

        if ($myPro->getGenerate_number_pro() == 'YES') {
            $nextAvailableNumber = Stock::getNextStockNumber($db, true);
            if (!$nextAvailableNumber) {
                http_redirect('main/sales/19');
            }
            $sale->setCardNumber_sal($nextAvailableNumber);
        } else {
            $sale->setCardNumber_sal('N/A');
        }
    } else {//MANUAL SALE
        //RECHECK NUMBER IS STILL VALID:
        $stock = new Stock($db);
        if (!$validate = $stock->validateManualNumber($sale->getSerial_cnt(), $data['txtCardNum'])) {
            http_redirect('main/sales/18');
        }
        $sale->setCardNumber_sal($data['txtCardNum']);
    }

    $sale->setEmissionDate_sal($data['txtEmissionDate']);
    $sale->setBeginDate_sal($data['txtDepartureDate']);
    $sale->setEndDate_sal($data['txtArrivalDate']);
    $sale->setInDate_sal(date("d/m/Y"));
    $sale->setDays_sal($data['hddDays']);
    $sale->setFee_sal($data['hdnPrice']);
    $sale->setObservations_sal(specialchars($data['observations']));
    $sale->setCost_sal($data['hdnCost']);
    if ($data['rdIsFree']) {
        if ($data['rdIsFree'] == 'YES') {
            $sale->setStatus_sal('REQUESTED');
        } else {
            $sale->setStatus_sal('REGISTERED');
        }
        $sale->setFree_sal($data['rdIsFree']);
    } else {
        $sale->setFree_sal('NO');
        $sale->setStatus_sal('REGISTERED');
    }

    //$sale -> setIdErpSal_sal();
    $sale->setType_sal('SYSTEM');
    $sale->setStockType_sal($data['sale_type']);
    $sale->setDirectSale_sal($data['hdnDirectSale']);
    $sale->setTotal_sal($data['hdnTotal']);
    $sale->setTotalCost_sal($data['hdnTotalCost']);
    $sale->setChange_fee_sal($data['rdCurrency']);

    if ($data['selCityDestination']) {
        $sale->setSerial_cit($data['selCityDestination']);
    } else {
        $sale->setSerial_cit('NULL');
    }

    $sale->setInternationalFeeStatus_sal('TO_COLLECT');
    $sale->setInternationalFeeUsed_sal('NO');
    $sale->setInternationalFeeAmount_sal($data['hdnTotalCost']);
    $sale->setDeliveredKit($data['rdDeliveredKit']);
	$sale->setFuture_payment_sal($data['rdPaymentForm']);
	
	if(isset($saleData['tit']['rep_cus'])){
		$sale->setSerial_rep($saleData['tit']['rep_cus']);
	}
    $saleID = $sale->insert();

    if ($saleID) {
        $error = 4;

        //REMOVE KIT FROM STOCK
        $serial_bra = Counter::getCountersDealer($db, $sale->getSerial_cnt());
        KitsMovement::kitsStockMinusOne($db, $serial_bra);

        //added for register free appliance
        if ($data['rdIsFree'] && $data['rdIsFree'] == 'YES') {
            $alert = new Alert($db);
            $alert->setMessage_alt('Solicitud de Free pendiente.');
            $alert->setType_alt('FREE');
            $alert->setStatus_alt('PENDING');
            $alert->setRemote_id($saleID);
            $alert->setTable_name('sales/fSearchFreeSales/');

            if (!$alert->autoSend($_SESSION['serial_usr'])) {
                $error = 16;
            } else {
                $error = 4;
            }

            $log_misc = array();
            $log_misc['old_sale'] = $sale;
            $log_misc['old_sale'] = get_object_vars($log_misc['old_sale']);
            unset($log_misc['old_sale']['db']);

            $log_misc['global_info']['requester_usr'] = $_SESSION['serial_usr'];
            $log_misc['global_info']['request_date'] = date('d/m/Y');
            $log_misc['global_info']['request_obs'] = $sale->getObservations_sal();

            SalesLog::registerLog($db, 'FREE', $log_misc, $saleID, $_SESSION['serial_usr']);
        }
        //end free appliance

        /* REGISTER THE SALES SERVICES */
        if ($data['chkService']) {
            foreach ($data['chkService'] as $serial_sbd) {
                if (!$sale->insertServices($saleID, $serial_sbd)) {
                    http_redirect('main/sales/8');
                }
            }
        }
        /* END SERVICES */

        /* REGISTER EXTRAS INFORMATION */
        if ($data['extra']) {
            $customer = new Customer($db);
            $extras = new Extras($db);
            $sessionLanguage = $_SESSION['serial_lang'];

            foreach ($data['extra'] as $key => $extra) {
                if ($customer->existsCustomer($extra['idExtra'], NULL)) {
                    $serial_cus = $customer->getCustomerSerialbyDocument($extra['idExtra']);
                    $customer->setSerial_cus($serial_cus);
                    $customer->getData();
                }


                //INSERT THE DATA
                $customer->setSerial_cit($data['selCityCustomer']);
                $customer->setDocument_cus($extra['idExtra']);
                $customer->setFirstname_cus(specialchars($extra['nameExtra']));
                $customer->setLastname_cus(specialchars($extra['lastnameExtra']));
                $customer->setBirthdate_cus($extra['birthdayExtra']);
                $customer->setType_cus('PERSON');
                $customer->setEmail_cus($extra['emailExtra']);
                $customer->setStatus_cus('ACTIVE');


                //SAVE OR UPDATE THE CUSTOMER
                if ($customer->existsCustomer($extra['idExtra'], NULL)) {
                    $customer->updateInfoSale($serial_cus);
                } else {
                    $serial_cus = $customer->insert();

                    if (!$serial_cus) {
                        ErrorLog::log($db, 'FAILED TO INSERT CUSTOMER -EXTRA-', $customer);
                    }
                }


                //REGISTER EXTRA - ANSWERS
                Answer::deleteMyAnswers($db, $serial_cus);

                foreach ($questionList as $q) {
                    if ($q['serial_ans']) {
                        $answer = new Answer($db, $q['serial_ans']);
                    } else {
                        $answer = new Answer($db);
                    }

                    $answer->setSerial_cus($serial_cus);
                    $answer->setSerial_que($q['serial_que']);
                    $ans = $_POST['answer' . $key . '_' . $q['serial_qbl']];

                    if ($ans) {
                        $answer->setYesno_ans($ans);
                        if ($answer->insert()) {
                            $error = 4;
                        } else {
                            ErrorLog::log($db, 'FAILED TO INSERT ANSWER, EXTRA CUSTOMER:' . $serial_cus, $answer);
                            break;
                        }
                    } else {
                        break;
                    }
                }

                //INSERT THE EXTRA
                $extras->setSerial_sal($saleID);
                $extras->setSerial_cus($serial_cus);
                $extras->setRelationship_ext($extra['selRelationship']);
                if ($extra['hdnPriceExtra'] == 0) {
                    $extras->setFee_ext(0.00);
                    $extras->setCost_ext(0.00);
                } else {
                    $extras->setFee_ext($extra['hdnPriceExtra']);
                    $extras->setCost_ext($extra['hdnCostExtra']);
                }

                if (!$extras->insert()) {
                    ErrorLog::log($db, 'FAILED TO INSERT EXTRA', $extras);
                } else {
                    $error = 4;
                }
            }//END FOREACH
        }
        /* END EXTRAS */

        $arr = array();
        $arr['error'] = $error;
        $arr['cardNumber'] = $sale->getCardNumber_sal();

        /* GENERAL CONDITIONS FILE */
        $country_of_sale = Sales::getCountryofSale($db, $saleID);
        if ($country_of_sale != 62) {
            $country_of_sale = 1;
        }
        $arr['urlGenCond'] = $sale->getGeneralConditionsbySale($data['selProduct'], $saleID, $country_of_sale, $_SESSION['serial_lang']);
        /* END GENERAL CONDITIONS FILE */

        $arr['saleID'] = $saleID;

        if ($error == 4) {
            $serial_generated_sale = $saleID;
            //Create contract
            if ($data['rdIsFree'] != 'YES') {
                //Printing Adendum
                include(DOCUMENT_ROOT . 'modules/sales/pPrintSalePDF.php');
                
                if(CONTRACT_LOGIC):
                    //Printing Contract
                    $arr['urlYearlyContract'] = 'yearlycontract_'.$serial_con.'.pdf';
                    include(DOCUMENT_ROOT . 'modules/sales/pPrintContractPDF.php');
                endif;
            }
        }
        $arr['pathContract'] = DOCUMENT_ROOT . 'modules/sales/contracts/contract_' . $saleID . '.pdf';
        $arr['urlContract'] = 'contract_' . $saleID . '.pdf';
        return $arr;
    } else {
        $error = 7;
        return $error;
    }
}

?>