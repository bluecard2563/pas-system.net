<?php
/*
 * Document   : pPrintSalePDF.php
 * Created on : Apr 29, 2011
 * Author     : Santiago Borja
 * Redirects navigation to use the correct PDF template to generate a contract
*/


	/************************************ COMMON VARIABLE INITIALIZATION ********************************/
	Request::setString('0:serialSal');
	Request::setString('1:printType');
	Request::setString('2:printLanguage');
	
	include_once DOCUMENT_ROOT.'lib/qrLibrary/qrlib.php';
	
	if($serialSal) $serial_sal = $serialSal;
	if($serial_generated_sale) $serial_sal = $serial_generated_sale;//Captured from invoking file
	
	if($printType) $display = $printType;

	$langCode = 2;
    if ($printLanguage) {
        $langCode = $printLanguage;
    }

	$verticalPointer = 807;
	$bullet = '- ';
	$spaced_bullet = '      - ';
	$max_height = 800;

	//Global Variables
	global $smallTextSize;
	global $medTextSize;
	global $stdTextSize;
	global $largeTextSize;
	global $titleTextSize;

	//Colors:
	global $blue;
	global $lightBlue;
	global $refText;
	global $red;
	global $black;
	global $white;

	global $refLines;
	global $rimacBckGnd;
	global $lightGray;

	//Positioning:
	$leftMostX = 40;	
	
	/************************************ CONTRACT CREATION PDF *****************************************/
	set_time_limit(36000);
	ini_set('memory_limit','512M');
	
	$pdf =& new Cezpdf('A4','portrait');
	$pdf -> selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/HELNLTSC.afm');
	$pdf -> ezSetCmMargins(1.19, 1.5, 1.15, 1.15);
	$all = $pdf -> openObject();
	$pdf -> saveState();
	
	$pdf -> setColor(0, 0, 0);
	$pdf -> ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
	$pdf -> restoreState();
	$pdf -> closeObject();
	$pdf -> addObject($all, 'all');
	
	
	
	
	/************************************ CONTRACT TEMPLATE TO USE *****************************************/
	$contract = ContractsByCustomer::verify_contract($db, $serial_sal);
	//******COMPLETE CONTRACTS LIST
		// DEFAULT: contract_pas.php
	//RIMAC: contract_rimac.php
	//INTERBANK: contract_interbank.php
	//DECAMERON: contract_decameron.php
	//DINERS PERU: contract_diners.php
	//PACIFICARD: contract_pacificard.php
        if(USE_NEW_CONTRACT){
            $contract = 'contract_pas2016.php';
	    $verticalPointer = 807;
            
        }else{
	if(!$contract):
		$contract = 'contract_pas.php';
		$verticalPointer = 807;
	endif;
        }
	include("modules/sales/contractTemplates/$contract");
?>