<?php

/*
  File: pNewSubMasiveSale.php
  Author: Miguel Ponce
  Creation Date:
  Modified By: David Bergmann
  Last Modified: 03/09/2010
 */

	Request::setString('rdType');
	$master_sale_serial = $_POST['sal_serial_sal'];
	
	//*********************************** CURRENCY SECTION ****************************************************
	($_POST['rdCurrency'])? $serial_cur = $_POST['rdCurrency']:	$serial_cur = 1;	
	$currency = new Currency($db, $serial_cur);
	$currency -> getData();
	
	//********************** GET THE SALE BASIC INFORMATION ***************************************************
	$sale = new Sales($db, $master_sale_serial);
	$sale -> getData();
	$productByDealer = new ProductByDealer($db, $sale -> getSerial_pbd());
	$productByDealer -> getData();
	$productByCountry = new ProductByCountry($db, $productByDealer -> getSerial_pxc());
	$productByCountry -> getData();
	$product = new Product($db, $productByCountry -> getSerial_pro());
	$product -> getData();
	
	//*************************** GIVING FORMAT TO THE SUBSALE INFO *******************************************
	$sale -> setSerial_sal(NULL);
	$sale -> setSerial_inv(NULL);
	$sale -> setSerial_cur($serial_cur);
	$sale -> setChange_fee_sal($currency -> getExchange_fee_cur());
	$sale -> setEmissionDate_sal(date('d/m/Y'));
	$sale -> setInternationalFeeStatus_sal('TO_COLLECT');
	$sale -> setInternationalFeeUsed_sal('NO');
	$sale -> setSal_Serial_sal($master_sale_serial);

	//********************* IF THE SUB-SALE MUST BE PERFORMED BY A FILE ***************************************
	if ($rdType != 'FIXED') {
		$priceByProductByCountry = new PriceByProductByCountry($db);
		$priceByProductByCountry -> setSerial_pxc($productByDealer -> getSerial_pxc());

		if (PriceByProductByCountry::pricesByProductExist($db, $productByDealer->getSerial_pxc())) { //If there is a price for the product in the country of analisis.
			$priceByProductByCountry -> getMasivePrice($productByCountry -> getSerial_pro(), $productByCountry -> getSerial_cou());
		} else { //If not, I take the prices for ALL COUNTRIES
			$priceByProductByCountry -> getMasivePrice($productByCountry -> getSerial_pro(), 1);
		}
		$price = $priceByProductByCountry -> getPrice_ppc();
		$cost = $priceByProductByCountry -> getCost_ppc() / $price;
		
		//PROCESING THE FILE
		$response = GlobalFunctions::masiveSaleProcess($db, $_FILES, $sale->getSerial_pbd());
		$error = $response[0];
		
		//IF THE FILE WAS OK, CALCULATE THE PRICES
		if ($error == 'success') {
			$toShow = $response[2];
			
			//SETTING THE FEE_SAL AND COST_SAL WHEN LOADING THE FILE			
			foreach ($toShow as $key => &$item) {
				if(AllowedDestinations::canProceedMultiSaleWithDestinationRestriction($db, $sale->getSerial_pbd(), $item['destination_cit'])){
					$start_chl = date('Y-m-d', strtotime('-' . $childAge . ' year', strtotime($item['start_trl'])));
					$birthdate = date('Y-m-d', strtotime($item['birthdate_cus']));
					$travel_days = $item['travel_days'];

					$spouse = 0;
					$children = 0;
					$adults = 0;

					if ($item['cus_serial_cus_xls']) {
						$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['relationship_ext'] = $item['relationship_ext'];
						if ($item['relationship_ext'] == 'SPOUSE' && $spouse == 0) {
							if ($product -> getSpouse_pro() == 'YES') {
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['type'] = 'SPOUSE';
								$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage'] = $productByCountry -> getPercentage_spouse_pxc();
								$total_spouse++;
								$item['price'] = round($price * $travel_days * (1 - ($productByCountry->getPercentage_spouse_pxc() / 100)), 2);
								$sub_total += $item['price'];
							} else {
								if ($product->getRelative_pro() == 'YES') {
									$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage'] = $productByCountry -> getPercentage_extras_pxc();
									$total_relatives++;
									$item['price'] = round($price * $travel_days * (1 - ($productByCountry -> getPercentage_extras_pxc() / 100)), 2);
									$sub_total += $item['price'];
								} else {
									$total_customers++;
									$item['price'] = round($price * $travel_days, 2);
									$sub_total += $item['price'];
								}
							}
						} else {
							if ($product->getRelative_pro() == 'YES') {
								if ($item['start_trl'] != "") {

									if ($start_chl < $birthdate) {
										//echo $start_chl.' es mayor a '.$birthdate;
										$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['type'] = 'RELATIVE';
										if ($children >= $product -> getChildren_pro()) {
											$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage'] = $productByCountry -> getPercentage_children_pxc();
											$total_children++;
											$item['price'] = round($price * $travel_days * (1 - ($productByCountry -> getPercentage_children_pxc() / 100)), 2);
											$sub_total += $item['price'];
										} else {
											$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage'] = 0;
											$total_children_free++;
											$item['price'] = 0;
										}
									} else {

										$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['type'] = 'OTHER';
										if ($adults >= $product->getAdults_pro()) {
											$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage'] = $productByCountry -> getPercentage_extras_pxc();
											$total_relatives++;
											$item['price'] = round($price * $travel_days * (1 - ($productByCountry -> getPercentage_extras_pxc() / 100)), 2);
											$sub_total += $item['price'];
										} else {
											$toShow[$item['cus_serial_cus_xls']]['extras'][$item['serial_cus']]['percentage'] = 0;
											$total_relatives_free++;
											$item['price'] = 0;
										}
									}
								}
							} else {
								$total_customers++;
								$item['price'] = $price * $travel_days;
								$sub_total += $item['price'];
							}
						}
					} else {
						$total_customers++;
						$item['price'] = $single_total = round($price * $travel_days, 2);
						$sub_total += $item['price'];
					}

					if ($toShow[$item['cus_serial_cus_xls']]['extras']) {
						foreach ($toShow[$item['cus_serial_cus_xls']]['extras'] as $subKey => &$extras) {
							if ($extras['type'] == 'SPOUSE') {
								$spouse++;
							}
							if ($extras['type'] == 'RELATIVE') {
								$children++;
							}
							if ($extras['type'] == 'OTHER') {
								$adults++;
							}
						}
					}
				}else{
					ErrorLog::log($db, 'MASSIVE_SALE_REMOVE_PAX_DUE_DESTINATION', array('PAX' => $item, 'serial_pbd' => $sale->getSerial_pbd()));
					unset($toShow[$key]); //REMOVE PASSEGENR DUE TO DESTINATION RESTRICTED.
					$destination_restricted = 'YES';
				}
			}
		}
	} else { //IF THE SUBSALE IS JUST FOR 
		$error = 'success';
		
		//Fixing the price
		$sub_total = $_POST['cost_sal'];
		if ($currency -> getExchange_fee_cur()) {
			$sub_total = $sub_total / $currency->getExchange_fee_cur();
		}
	}

	if ($error == 'success') {
		//CALCULATING THE TOTAL COST OF THE SALE AND SETTING IT
		$cost_sal = $cost * $sub_total;
		$sale -> setTotalCost_sal($cost_sal);
		$sale -> setInternationalFeeAmount_sal($cost_sal);
		
		//SETTING THE TOTAL_SAL
		$sale -> setFee_sal($sub_total);
		$sale -> setTotal_sal($sub_total);
		$serial_sal = $sale -> insert();
		
		if ($serial_sal) {
			ERP_logActivity('SALES', $serial_sal); //ERP ACTIVITY
			
			if (is_array($toShow)) {
				$travelerLog = new TravelerLog($db);
				$travelerLog -> setSerial_cnt($sale -> getSerial_cnt());
				$travelerLog -> setSerial_sal($serial_sal);
				$travelerLog -> setStatus_trl('ACTIVE');
				$travelerLog -> setSerial_usr($_SESSION['serial_usr']);
				
				$sub_card_number = TravelerLog::getMaxCardNumberForSale($db, $master_sale_serial) + 1;
				
				foreach ($toShow as $key => $c) {
					$travelerLog -> setSerial_cus($c['serial_cus']);
					$travelerLog -> setStart_trl(GlobalFunctions::changeDateFormat($c['start_trl']));
					$travelerLog -> setEnd_trl(GlobalFunctions::changeDateFormat($c['end_trl']));
					$travelerLog -> setDays_trl($c['travel_days']);
					$travelerLog -> setSerial_cit($c['destination_cit']);
					$travelerLog -> setCard_number_trl($sale -> getCardNumber_sal() . '_' . $sub_card_number++);

					if (!$travelerLog -> insert()) {
						$error = 'travelerlog_error';
						break;
					}
				}
			}
		} else {
			$error = 'main_subsale_insert_error';
		}
	}

	http_redirect('main/masiveSales/' . $error . '/' . $serial_sal . '/'. $destination_restricted);
?>