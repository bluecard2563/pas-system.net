<?php
/*
 * File: fvalidateReservedSale.php
 * Author: Valeria Andino
 * Creation Date: 27/07/2016
 * Modifies By:
 * Last Modified:
 */

$parameter=new Parameter($db); //It's the parameter for the max number of added days.
$parameter->getData();
$parameter=get_object_vars($parameter);
unset($parameter['db']);
        
$smarty->register('parameter');
$smarty->display();
?>
