<?php
/*
File:fCheckSalesVoidApplication.php
Author: Santiago Borja
Creation Date: 04/05/2010
*/

Request::setInteger('0:serial_slg');

$sLog = new SalesLog($db, $serial_slg);
if($sLog -> getData()){
	$sLog = get_object_vars($sLog);
	$sLogStatus = SalesLog::getSalesLogStatus($db);
	$description = unserialize($sLog['description_slg']);
	unset($sLogStatus[0]);

/***
 * SALE MODIFIED
 */
	$serial_sal = $description['new_sale']['serial_sal'];
	$sale=new Sales($db,$serial_sal);
	$sale->getData();
	$sale=get_object_vars($sale);
	unset($sale['db']);
	$sale['begin_date_sal']=$description['new_sale']['begin_date_sal'];
	$sale['end_date_sal']=$description['new_sale']['end_date_sal'];
	$sale['observations_sal']=$description['modifyObs'];
	$sale['days_sal']=$description['new_sale']['days_sal'];
	$sale['fee_sal']=$description['new_sale']['fee_sal'];
	$sale['cost_sal']=$description['new_sale']['cost_sal'];
	$sale['total_sal']=$description['new_sale']['total_sal'];
	$sale['total_cost_sal']=$description['new_sale']['total_cost_sal'];
	$sale['modifier']=$description['new_sale']['modifier'];
	$sale['Services']=$description['new_sale']['Services'];
	$sale['serial_pbd']=$description['new_sale']['serial_pbd'];
	//PRE-LOAD INFORMATION
    //CounterName
    $counter=new Counter($db);
	$counter->setSerial_cnt($sale['serial_cnt']);
	$counter->getData();
	$counter=get_object_vars($counter);
	unset($counter['db']);

	$dealer=new Dealer($db);
	$dealer->getData();

	$serial_cnt=$sale['serial_cnt'];
	$data['counterName']=$counter['aux_cnt']['counter'];
	$deaData=Dealer::getDealerCity($db, $serial_cnt);
    $data['dealerOfUser']=$counter['serial_dea'];
	$dealer->setSerial_dea($data['dealerOfUser']);
    $dealer->getData();
	//Debug::print_r($dealer);
    $data['name_dea']=$dealer->getName_dea();
    $data['serialDealer']=$dealer->getDea_serial_dea();

	//Makes Seller Code
    $sellerCode=$deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer->getCode_dea().'-'.$data['serialDealer'].'-'.$serial_cnt;
	//EmissionCity
    $data['emissionPlace']=$deaData['name_cit'];
    //EmissionDate
	$emissionDate=date("d/m/Y", strtotime(str_replace("/", "-", $sale['emission_date_sal'])));
	$sale['emission_date_sal']=$emissionDate;
	//Country currencies
	$curBycountry=new CurrenciesByCountry($db);
	$data['currencies']=$curBycountry->getCurrenciesDealersCountry($counter['serial_dea']);
	$data['currenciesNumber']=sizeof($data['currencies']);

	//Country and City Lists
    $city= new City($db);
	$countryList = Country::getAllAvailableCountries($db);


	//MAIN CUSTOMER INFO
	$customer=new Customer($db);
	$customer->setserial_cus($description['new_sale']['serial_cus']);
	$customer->getData();
	$customer=get_object_vars($customer);
	unset($customer['db']);

	$cityListCus=$city->getCitiesByCountry($customer['auxData']['serial_cou']);
	//Destination Country

	$sale['serial_cou']= Country::getCountryByCity($db, $description['new_sale']['serial_cit']);
	$sale['serial_cit']=$description['new_sale']['serial_cit'];
	$cityList=$city->getCitiesByCountry($sale['serial_cou']);

	    /*products by Dealer*/
    $productByDealer= new ProductByDealer($db);
    $productListByDealer=$productByDealer->getproductByDealer($data['serialDealer'], $_SESSION['serial_lang']);

    //Services by dealer
    $servicesByDealer= new ServicesByDealer($db);
    if(ServicesByDealer::serviceByCountryExist($db, $deaData['serial_cou'])){
        $servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $deaData['serial_cou']);
    }else{
        $servicesListByDealer=$servicesByDealer->getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
    }

	$sbcbs= new ServicesByCountryBySale($db);
	$sbcbs=$sbcbs->getServicesBySale($sale['serial_sal'], $data['serialDealer']);


	$extra=new Extras($db);
	$extraArray=($description['new_extras']);
    if(is_array($extraArray)){
		$relationshipTypeList = Extras::getAllRelationships($db);
		$smarty->register('relationshipTypeList,percentages');
		$totalFeeExtras=0;
		$totalCostExtras=0;
		foreach($extraArray as $key=>$ext){

			$totalFeeExtras=$totalFeeExtras+(float)$ext['fee_ext'];
			$totalCostExtras=$totalCostExtras+(float)$ext['cost_ext'];
		}
		$sale['totalFeeExtras']=$totalFeeExtras;
		$sale['totalCostExtras']=$totalCostExtras;
	}
	/*****
	 * END MODIFIED SALE
	 *
	 */

	/***
	* ORIGINAL SALE
	*/
	$serial_sal=$description['old_sale']['serial_sal'];
	$saleOld=new Sales($db,$serial_sal);
	$saleOld->getData();
	$saleBackedUp = $saleOld;
	$saleOld=get_object_vars($saleOld);
	unset($saleOld['db']);
	$emissionDate=date("d/m/Y", strtotime(str_replace("/", "-", $saleOld['emission_date_sal'])));
	$saleOld['emission_date_sal']=$emissionDate;

	//MAIN CUSTOMER INFO
	$customer_old=new Customer($db);
	$customer_old->setserial_cus($saleOld['serial_cus']);
	$customer_old->getData();
	$customer_old=get_object_vars($customer_old);
	unset($customer_old['db']);
	$cityListCus_old=$city->getCitiesByCountry($customer_old['auxData']['serial_cou']);
	//Destination Country
	$saleOld['serial_cou']= Country::getCountryByCity($db, $saleOld['serial_cit']);
	
	$cityListOld=$city->getCitiesByCountry($saleOld['serial_cou']);

	$sbcbs_old= new ServicesByCountryBySale($db);
	$sbcbs_old=$sbcbs_old->getServicesBySale($saleOld['serial_sal'], $data['serialDealer']);


	$extra_old=new Extras($db);
	$extraArray_old = Extras::getExtrasBySale($db, $saleOld['serial_sal']);
    if(is_array($extraArray_old)){
		$relationshipTypeList = Extras::getAllRelationships($db);
		$smarty->register('relationshipTypeList,percentages');
		$totalFeeExtras=0;
		$totalCostExtras=0;
		foreach($extraArray_old as $key=>$ext){

			$totalFeeExtras=$totalFeeExtras+(float)$ext['fee_ext'];
			$totalCostExtras=$totalCostExtras+(float)$ext['cost_ext'];
		}
		$saleOld['totalFeeExtras']=$totalFeeExtras;
		$saleOld['totalCostExtras']=$totalCostExtras;
	}
	/*
	 * END ORIGINAL SALE
	 */
	
	/*HISTROY OF CHANGES*/
	$sLog2 = new SalesLog($db);
	$sLog2 -> getSalesLogBySale($serial_sal, $sLog['type_slg']);
	$auxSlog = $sLog2 -> getAux_slg();
	//Debug::print_r($auxSlog);
	$history= array();
	foreach($auxSlog as $key => $slg){
		$descrip=unserialize($slg['description_slg']);
		//Debug::print_r($descrip);
		$history[$key]['obs']=$descrip['modifyObs'];
		$history[$key]['status']=$slg['status_slg'];
		$history[$key]['applicant']=$slg['first_name_usr']." ".$slg['last_name_usr'];
		$history[$key]['modDate']=$slg['date_slg'];
	}
	/*END HISTORY OF CHANGES*/

	//Debug::print_r($history);
	$smarty->register('sale,serial_slg,sLog,sLogStatus,sellerCode,data,customer,countryList,cityList,cityListCus,productListByDealer,servicesListByDealer,extraArray,sbcbs,history');
	$smarty->register('saleOld,customer_old,cityListOld,cityListCus_old,extraArray_old,sbcbs_old');
}else{//Can`t load all the info.
	$error=1;
}
$smarty->display();
?>
