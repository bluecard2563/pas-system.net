<?php
/*
 * File: fAddDays.php
 * Author: Patricio Astudillo
 * Creation Date: 08/04/2010
 * Modifies By:
 * Last Modified:
 */

$parameter=new Parameter($db, '8'); //It's the parameter for the max number of added days.
$parameter->getData();
$parameter=get_object_vars($parameter);
unset($parameter['db']);

$smarty->register('parameter');
$smarty->display();
?>
