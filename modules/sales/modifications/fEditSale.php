<?php
/*
File:editSale.php
Author: Esteban Angulo
Creation Date: 14/04/2010
*/
	$parameter = new Parameter($db, '7');
	$parameter -> getData();
	$age = $parameter -> getValue_par();
	$sale = new Sales($db);
	$sale -> setCardNumber_sal($_POST['txtCardNumber']);
	$FINAL_ERROR_ESCAPE_URL = "modules/sales/modifications/fSearchCard/";
	
	//MODIFICATION CONSTRAINTS CHECKS:
	if(!$sale -> getDataByCardNumber()){
		http_redirect($FINAL_ERROR_ESCAPE_URL . '1');//CARD NUMBER NOT FOUND
	}else{//NO CORPORATE OR EXECUTIVE SALES CAN BE MODIFIED:
		if(TravelerLog::getDataByCardNumber($db, $_POST['txtCardNumber'])){
			http_redirect($FINAL_ERROR_ESCAPE_URL . '2');
		}else{
			$saleProduct = $sale -> getSaleProduct($_SESSION['serial_lang']);
			if($saleProduct['masive_pro'] == 'YES'){
				http_redirect($FINAL_ERROR_ESCAPE_URL . '8');
			}
			if(Sales::hasTravelsRegistered($db, $sale->getSerial_sal())){
				http_redirect($FINAL_ERROR_ESCAPE_URL . '2');
			}
		}
		
		//************************ STAND-BY VERIFICATION ***********************
		if($sale->getStatus_sal() == 'STANDBY'){
			http_redirect($FINAL_ERROR_ESCAPE_URL . '9');
		}
	}
	
	if(!Sales::isMySale($db, $sale->getCardNumber_sal(), $_SESSION['serial_mbc'], $_SESSION['serial_dea'])){
		http_redirect($FINAL_ERROR_ESCAPE_URL . '10');
	}

	$nameUser = $_SESSION['user_name'];
	$sale = get_object_vars($sale);
	unset($sale['db']);
	
	$sLog2 = new SalesLog($db);
	if(	$sLog2 -> getPendingSalesLogBySale($sale['serial_sal'], 'MODIFICATION') != false || 
		$sLog2 -> getPendingSalesLogBySale($sale['serial_sal'], 'SPECIAL_MODIFICATION') != false){
		http_redirect($FINAL_ERROR_ESCAPE_URL . '4');
	}

	if($sale['free_sal']=='YES'){
		if(	$sale['status_sal'] == 'REQUESTED' || 
			$sale['status_sal'] == 'DENIED' ){
			http_redirect($FINAL_ERROR_ESCAPE_URL . '5');
		}
	}

	if(	$sale['status_sal'] == 'REQUESTED' || 
		$sale['status_sal'] == 'DENIED' || 
		$sale['status_sal'] == 'VOID' ||
                $sale['status_sal'] == 'REFUNDED' ||
		$sale['status_sal'] == 'EXPIRED' || 
		$sale['status_sal'] == 'BLOCKED'){
		http_redirect($FINAL_ERROR_ESCAPE_URL . '6');
	}
	$date = date("d/m/Y");
	// if(	(!User::isSubManagerUser($db, $_SESSION['serial_usr']) && $_SESSION['serial_dea'] != '') && 
	// 	$date > $sale['begin_date_sal']){
	// 	http_redirect($FINAL_ERROR_ESCAPE_URL . '7');
	// }
	
	//PRE-LOAD INFORMATION
    //CounterName
    $counter = new Counter($db);
	$counter -> setSerial_cnt($sale['serial_cnt']);
	$counter -> getData();
	$counter = get_object_vars($counter);
	unset($counter['db']);

	$serial_cnt = $sale['serial_cnt'];
	$data['counterName'] = $counter['aux_cnt']['counter'];
	$deaData = Dealer::getDealerCity($db, $serial_cnt);
    $data['dealerOfUser'] = $counter['serial_dea'];
    
    $dealer = new Dealer($db);
	$dealer -> setSerial_dea($data['dealerOfUser']);
    $dealer -> getData();
	//Debug::print_r($dealer);
    $data['name_dea'] = $dealer -> getName_dea();
    $data['serialDealer'] = $dealer -> getDea_serial_dea();
    
	//Makes Seller Code
    $sellerCode = $deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer -> getCode_dea().'-'.$data['serialDealer'].'-'.$serial_cnt;
	//EmissionCity
    $data['emissionPlace'] = $deaData['name_cit'];
    //EmissionDate
	$emissionDate = date("d/m/Y", strtotime(str_replace("/", "-", $sale['emission_date_sal'])));
	//Country currencies
	$curBycountry = new CurrenciesByCountry($db);
	$data['currencies'] = $curBycountry -> getCurrenciesDealersCountry($counter['serial_dea'],$sale['serial_sal']);
	$data['currenciesNumber']=sizeof($data['currencies']);

	//Country and City Lists
    $city = new City($db);
	$countryList = Country::getAllAvailableCountries($db);

	//MAIN CUSTOMER INFO
	$customer = new Customer($db);
	$customer -> setserial_cus($sale['serial_cus']);
	$customer -> getData();
	$typesList = $customer -> getAllTypes();
	
	$customer = get_object_vars($customer);
	unset($customer['db']);
	$customer['serial_cou'] = Country::getCountryByCity($db, $customer['serial_cit']);
	$customerCityList = $city -> getCitiesByCountry($customer['serial_cou']);
	
	//*********** REP CUSTOMER DATA
	$repCustomer = new Customer($db, $sale['serial_rep']);
	$repCustomer->getData();
	$repCustomer = get_object_vars($repCustomer);
	unset($repCustomer['db']);
	$repCustomer['serial_cou'] = Country::getCountryByCity($db, $repCustomer['serial_cit']);
	$repCustomerCityList = $city -> getCitiesByCountry($repCustomer['serial_cou']);
	
	//Destination Country
	$sale['serial_cou'] = Country::getCountryByCity($db, $sale['serial_cit']);
	$cityList = $city -> getCitiesByCountry($sale['serial_cou']);

	//COUNTRY OF SALE
	$product_of_sale_info = ProductByCountry::getPxCDataByPbDSerial($db, $sale['serial_pbd']);

	/*products by Dealer*/
    $productByDealer = new ProductByDealer($db);
    $productListByDealer = $productByDealer -> getproductByDealer($data['serialDealer'], $_SESSION['serial_lang']);
	$fligths = $productByDealer -> getFligths($sale['serial_pbd']);
	
	//ADD THE REQUIRED PERCENTAGES FOR EACH PRODUCT
    foreach($productListByDealer as &$singleProListed){
		$pxc = new ProductByCountry($db, $singleProListed['serial_pxc']);
		$pxc -> getData();
		if(PriceByProductByCountry::pricesByProductExist($db, $singleProListed['serial_pxc'])){
			//If there is a price for the product in the country of analisis.
			$percentages = ProductByCountry::getPercentages_by_Product($db, $singleProListed['serial_pro'], $pxc -> getSerial_cou());
		}else{ 
			//If not, I take the prices for ALL COUNTRIES
			$percentages = ProductByCountry::getPercentages_by_Product($db, $singleProListed['serial_pro'],'1');
		}
		$singleProListed['percentage_spouse'] = $percentages['percentage_spouse_pxc'];
		$singleProListed['percentage_extras'] = $percentages['percentage_extras_pxc'];
		$singleProListed['percentage_children'] = $percentages['percentage_children_pxc'];
	}

    //Services by dealer
    $servicesByDealer = new ServicesByDealer($db);
    if(ServicesByDealer::serviceByCountryExist($db, $deaData['serial_cou'])){
        $servicesListByDealer = $servicesByDealer -> getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], $deaData['serial_cou']);
    }else{
        $servicesListByDealer = $servicesByDealer -> getServicesByDealer($data['serialDealer'], $_SESSION['serial_lang'], '1');
    }

	$sbcbs = new ServicesByCountryBySale($db);
	$sbcbs = $sbcbs -> getServicesBySale($sale['serial_sal'], $data['serialDealer']);

	$extra = new Extras($db);
	$extraArray = Extras::getExtrasBySale($db, $sale['serial_sal']);
	$people_in_card = sizeof($extraArray);
    if(is_array($extraArray)){
		$pbc=new ProductByCountry($db);
		$percentages = ProductByCountry::getPercentages_by_Product($db, $sale['serial_pbd'],$deaData['serial_cou']);
		$relationshipTypeList = Extras::getAllRelationships($db);		
		$smarty -> register('relationshipTypeList,percentages');
		$totalFeeExtras = 0;
		$totalCostExtras = 0;
		foreach($extraArray as $key => $ext){
			$totalFeeExtras = $totalFeeExtras+(float)$ext['fee_ext'];
			$totalCostExtras = $totalCostExtras+(float)$ext['cost_ext'];
		}
		$sale['totalFeeExtras'] = $totalFeeExtras;
		$sale['totalCostExtras'] = $totalCostExtras;
	}
	
    //If the dealer has no products to sale,
    //he can't see the sales interface.
    if(!is_array($productListByDealer)){
        http_redirect($FINAL_ERROR_ESCAPE_URL . '3');
    }

	$sizeExtras = count($extraArray)-1;
	$parameter = new Parameter($db);
	$parameter -> setSerial_par('14');
	$parameter -> getData();
	$maxCoverTime = $parameter -> getValue_par();
	
	$parameter2 = new Parameter($db, 1); // - ->  PARAMETER FOR ELDER QUESTIONS
    $parameter2 -> getData();
    $elderlyAgeLimit = $parameter2 -> getValue_par(); //value of the paramater used to display or not the questions

	/*QUESTIONS*/
    $sessionLanguage = $_SESSION['serial_lang'];
    $question=new Question($db);
    $maxSerial = $question -> getMaxSerial($sessionLanguage);
    $questionList = Question::getActiveQuestions($db, $sessionLanguage);//list of active questions shown in the form
    /*END QUESTIONS*/

	/*HISTROY OF CHANGES*/
	$sLog2= new SalesLog($db);
	$sLog2 -> getSalesLogBySale($sale['serial_sal'], 'MODIFICATION');
	$modifications_array = $sLog2 -> getAux_slg();
	if(!is_array($modifications_array))	$modifications_array = array();

	$sLog2 -> getSalesLogBySale($sale['serial_sal'], 'SPECIAL_MODIFICATION');
	$special_modifications_array= $sLog2 -> getAux_slg();
	if(!is_array($special_modifications_array))	$special_modifications_array = array();
	$auxSlog = array_merge($modifications_array, $special_modifications_array);
	
	if(sizeof($auxSlog) > 0){
		$history= array();
		foreach($auxSlog as $key=>$slg){
			$descrip=unserialize($slg['description_slg']);
			//Debug::print_r($descrip);
			$history[$key]['obs'] = $descrip['modifyObs'];
			$history[$key]['status'] = $slg['status_slg'];
			$history[$key]['applicant'] = $slg['first_name_usr']." ".$slg['last_name_usr'];
			$history[$key]['modDate'] = $slg['date_slg'];
		}
		$smarty -> register('history');
	}
	/*END HISTORY OF CHANGES*/

	
	/***********************************************************************************************************/
	/***************************************   MODIFICATION RESTRICTIONS  **************************************/
	/***********************************************************************************************************/
	
	//IN NO CASE, A PRODUCT CAN BE CHOSEN WITH A DIFFERENT generate_number_pro, SO WE REMOVE ALL THOSE PRODUCTS
	$pxc = new ProductByCountry($db, $product_of_sale_info['serial_pxc']);
	$pxc -> getData();
	$pro = new Product($db, $pxc -> getSerial_pro());
	$pro -> getData();
	$generateNumberCondition = $pro -> getGenerate_number_pro();
	foreach ($productListByDealer as $key => &$single){
		if($single['generate_number_pro'] != $generateNumberCondition){
			unset($productListByDealer[$key]);
		}
	}
	
	//IF THE SALE HAS AN INVOICE, THE FOLLOWING CANNOT BE MODIFIED:
	//				PRODUCT
	//				INCREASE DAYS, USER CAN ONLY DECREASE
	//				EXTRAS
	//				NAME, LASTNAME, DOCUMENT & BIRTHDATE
	if($sale['serial_inv'] != '' || InvoiceLog::didSaleHaveInvoice($db, $sale['serial_sal'])){
		$sale['modify'] = 'NO';
	}else{
		$sale['modify'] = 'YES';
	}
	
	//echo '<pre>';print_r($pro);echo '</pre>';
	//echo '<pre>';print_r($productListByDealer);echo '</pre>';
	//Debug::print_r($history);
	//Debug::print_r($sale);
	
	/************************************** FIX SERVICES PRICE ********************************************/
	$people_in_card++; //Add Holder in count
	if($sbcbs){
		$service_total_price = 0;
		$service_total_cost = 0;
		foreach($sbcbs as $service){
			if($service['fee_type_ser'] == 'TOTAL'){
				$service_total_price += $service['price_sbc'];
				$service_total_cost += $service['cost_sbc'];
			}else{
				$service_total_price += ($service['price_sbc'] * $sale['days_sal']);
				$service_total_cost += ($service['cost_sbc'] * $sale['days_sal']);
			}
		}
		
		$service_total_price = number_format($service_total_price, 2);
		$service_total_cost = number_format($service_total_cost, 2);
	}
	
	$smarty -> register('typesList,sale,sellerCode,data,customer,countryList,cityList,customerCityList,productListByDealer,servicesListByDealer');
	$smarty -> register('sbcbs,emissionDate,maxCoverTime,elderlyAgeLimit,extraArray,sizeExtras,nameUser,questionList');
	$smarty -> register('fligths,age,product_of_sale_info,service_total_cost,service_total_price');
	$smarty -> register('repCustomer,repCustomerCityList');
	$smarty -> display();
?>