<?php
/*
File: pReactivateCard
Author: David Bergmann
Creation Date: 23/08/2010
Last Modified:
Modified By:
*/

//die(Debug::print_r($_POST));

$sale = new Sales($db,$_POST['serial_sal']);
$sale->getData();

$sales_log = new SalesLog($db);
$sales_log->setSerial_usr($_SESSION['serial_usr']);
$sales_log->setSerial_sal($sale->getSerial_sal());
$sales_log->setType_slg('REACTIVATED');
$sales_log->setStatus_slg('PENDING');
$description = array(
	'old_status_sal'=>$sale->getStatus_sal(),
	'new_status_sal'=>'ACTIVE',
	'Description'=>'Change of state to ACTIVE',
	'old_begin_date'=>$sale->getBeginDate_sal(),
	'new_begin_date'=>$_POST['txtBeginDateSal'],
	'old_end_date'=>$sale->getEndDate_sal(),
	'new_end_date'=>$_POST['txtEndDateSal'],
	'comments_request'=>$_POST['txtDescription']
);
$sales_log->setDescription_slg(serialize($description));

$serial_slg = $sales_log->insert();

if($serial_slg) {
	$alert = new Alert($db);
	$alert->setMessage_alt(utf8_decode("Solicitud de reactivación de tarjeta expirada."));
	$alert->setType_alt('REACTIVATED');
	$alert -> setStatus_alt('PENDING');
	$alert -> setRemote_id($serial_slg);
	$alert->setTable_name("sales/modifications/fDispatchReactivationApplications/");
	
	if(!$alert -> autoSend($_SESSION['serial_usr'])){
		$error = 3;
	}else{
		//Email Buildup:
		$user = new User($db, $_SESSION['serial_usr']);
		$user -> getData();
		
		$customer = new Customer($db, $sale -> getSerial_cus());
		$customer -> getData();
		
		$misc['card_number'] = $sale -> getCardNumber_sal();
		$misc['user_name'] = $user -> getFirstname_usr() . " " . $user -> getLastname_usr();
		$misc['customer_name'] = $customer->getFirstname_cus()." ".$customer->getLastname_cus();
	
		$destinationUsers = Alert::getAlertedUsers($db, 'REACTIVATED', $user -> getSerial_usr());
		foreach($destinationUsers as $key => $singleUser){
			$misc['email'][$key] = $singleUser['email_usr'];
		}
		if(GlobalFunctions::sendMail($misc, 'card_reactivation')){
			$error = 1;
		}else{
			$error = 4;
		}
	}
}else{
	$error = 2;
}

http_redirect("modules/sales/modifications/fReactivateCard/".$error);
?>