<?php
/*
File: fSearchReactivateCard
Author: David Bergmann
Creation Date: 25/08/2010
Last Modified:
Modified By:
*/

$data['cards'] = Sales::getPendingReactivatedCards($db);
$data['titles'] = array('Tarjeta No.','Titular','Atender');

$smarty->register('data');
$smarty->display();
?>