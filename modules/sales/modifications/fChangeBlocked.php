<?php
/*
 * File: fChangeBlocked.php
 * Author: Gabriela Guerrero
 * Creation Date: 10/05/2010
 * Modifies By:
 * Last Modified:
 */

$parameter23=new Parameter($db, '23'); //Parameter for the list of mails to receive a notification
$parameter23->getData();
$parameter23=get_object_vars($parameter23);
unset($parameter23['db']);

$smarty->register('parameter23');
$smarty->display();
?>
