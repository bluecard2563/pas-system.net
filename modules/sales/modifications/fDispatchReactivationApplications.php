<?php
/*
File: fDispatchReactivationApplications
Author: David Bergmann
Creation Date: 24/08/2010
Last Modified:
Modified By:
*/

Request::setString('0:serial_slg');

$sales_log = new SalesLog($db,$serial_slg);
$sales_log->getData();

$sale = new Sales($db, $sales_log->getSerial_sal());
$sale->getData();
$data['serial_slg'] = $serial_slg;
$data['all_status'] = SalesLog::getSalesLogStatus($db);
$data['card_number'] = $sale->getCardNumber_sal();
$customer = new Customer($db, $sale->getSerial_cus());
$customer->getData();
$data['customer_name'] = $customer->getFirstname_cus()." ".$customer->getLastname_cus();
$data['product'] = ProductByDealer::getProductsByDealerByLanguage($db, $sale->getSerial_pbd(), $_SESSION['serial_lang']);
$data['description'] = unserialize($sales_log->getDescription_slg());
//die(Debug::print_r($data));

$smarty->register('data');
$smarty->display();
?>