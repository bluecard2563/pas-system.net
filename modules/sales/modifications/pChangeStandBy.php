<?php
/*
 * File: pChangeStandBy.php
 * Author: Patricio Astudillo
 * Creation Date: 13/04/2010
 * Modifies By: David Bergmann
 * Last Modified: 28/07/2010
 */

	$sale = new Sales($db, $_POST['serial_sal']);
	$sale->getData();
	$needsAproval = $_POST['hdnNeedAproval'];

	$country = new Country($db);
	$serialCountry = Country::getCountryByCity($db, $sale->getSerial_cit());

	//********************************** MAILING GETTING MAILING LIST ****************************
	$parameter = new Parameter($db, 22); //List of mails that will receive a notification
	$parameter->getData();
	$usersByCountry = unserialize($parameter->getValue_par());
	$arrayUsers = explode(",", $usersByCountry[$serialCountry]);
	$cont = 0;
	$arrayEmails = array();
	if ($arrayUsers) {
		foreach ($arrayUsers as $aUser) {
			$user = new User($db, $aUser);
			$user->getData();
			$arrayEmails[$cont] = $user->getEmail_usr();
			$cont = $cont + 1;
		}
	}
	$sale->getSaleUserMail($_POST['serial_sal']);
	$arrayEmails[count($arrayEmails)] = $sale->getAux();
	$user = new User($db, $_SESSION['serial_usr']);
	$user->getData();
	$infoMail['userName'] = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
	$infoMail['cardNumber'] = $_POST['cardNumber'];
	$infoMail['customerName'] = $_POST['customerName'] . ' ' . $_POST['customerSurname'];

	//******************************* GET CARD INFORMATION *****************************************
	if ($sale->getData()) {
		
		//******************* GET NEW TRAVEL DATES FOR CARD ACTIVATION *****************************
		if ($_POST['txtBeginDate'] && $_POST['txtEndDate']) {

			$begindate = date("Y-m-d", mktime(0, 0, 0, substr($_POST['txtBeginDate'], 3, 2), substr($_POST['txtBeginDate'], 0, 2), substr($_POST['txtBeginDate'], 6, 4)));
			$endDate = date("Y-m-d", mktime(0, 0, 0, substr($_POST['txtEndDate'], 3, 2), substr($_POST['txtEndDate'], 0, 2), substr($_POST['txtEndDate'], 6, 4)));
			$beginDateSal = date("Y-m-d", mktime(0, 0, 0, substr($sale->getBeginDate_sal(), 3, 2), substr($sale->getBeginDate_sal(), 0, 2), substr($sale->getBeginDate_sal(), 6, 4)));
			$today = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));

			if (strtotime($begindate) >= strtotime($beginDateSal) &&
					strtotime($begindate) > strtotime($today) &&
					strtotime($endDate) > strtotime($today)) {

				//Calculates date difference
				$day = substr($_POST['txtBeginDate'], 0, 2);
				$month = substr($_POST['txtBeginDate'], 3, 2);
				$year = substr($_POST['txtBeginDate'], 6, 4);
				$beginDate = $year . "-" . $month . "-" . $day;

				$day = substr($_POST['txtEndDate'], 0, 2);
				$month = substr($_POST['txtEndDate'], 3, 2);
				$year = substr($_POST['txtEndDate'], 6, 4);
				$endDate = $year . "-" . $month . "-" . $day;

				$days = floor((strtotime($endDate) - strtotime($beginDate)) / (60 * 60 * 24)) + 1;

				if ($days <= $sale->getDays_sal()) {
					//*********************** LOG TRAVEL DATE AT EACH CHANGE *********************
					$misc_dates_log = array('old_begin_date' => $sale ->getBeginDate_sal(),
											'old_end_date' => $sale ->getEndDate_sal(),
											'new_begin_date' => $_POST['txtBeginDate'],
											'new_end_date' => $_POST['txtEndDate']);
					
					$sale->setBeginDate_sal($_POST['txtBeginDate']);
					$sale->setEndDate_sal($_POST['txtEndDate']);
				} else {
					http_redirect('main/sales_modifications/8');
				}
			} else {
				http_redirect('main/sales_modifications/7');
			}
		}

		//Save the LOG for this sale modification.
		$slg = new SalesLog($db);
		$slg->setType_slg('STAND_BY');
		$slg->setSerial_usr($_SESSION['serial_usr']);
		$slg->setSerial_sal($_POST['serial_sal']);
		$misc = array(	'old_status_sal' => $_POST['status'],
						'new_status_sal' => $_POST['rdStatus'],
						'Description' => 'Change of state to STAND-BY.',
						'observations' => $_POST['txtComments']);
		if(is_array($misc_dates_log)){
			$misc['travel_dates_log'] = $misc_dates_log;
		}

		$misc['global_info']['requester_usr'] = $_SESSION['serial_usr'];
		$misc['global_info']['request_date'] = date('d/m/Y');
		$misc['global_info']['request_obs'] = $_POST['txtComments'];

		$slg->setDescription_slg(serialize($misc));

		if ($_POST['rdStatus'] != $sale->getStatus_sal()) {
			if ($_POST['rdStatus'] == 'STANDBY') {
				if ($needsAproval == '1') {//Needs Aproval
					$slg->setStatus_slg('PENDING');
					$serial_slg = $slg->insert();

					if ($serial_slg) {
						$alert = new Alert($db);
						$alert->setMessage_alt('Solicitud de cambio a Stand-By');
						$alert->setType_alt('STANDBY');
						$alert->setStatus_alt('PENDING');
						$alert->setRemote_id($serial_slg);
						$alert->setTable_name('sales/modifications/fDispatchStandByApplications/');

						if (!$alert->autoSend($_SESSION['serial_usr'])) {
							$error = 5;
						} else {
							$error = 1;
						}
						/* END ALERTS */
					} else {//The sale_log wasn't inserted.
						$error = 2;
					}
				} else {//Doesn't Need Aproval
					$sale->setStatus_sal($_POST['rdStatus']);

					if ($sale->updateStatus()) {
						$misc['global_info']['authorizing_usr'] = $_SESSION['serial_usr'];
						$misc['global_info']['authorizing_date'] = date('d/m/Y');
						$misc['global_info']['authorizing_obs'] = 'Cambio directo del sistema';

						$slg->setDescription_slg(serialize($misc));
						$slg->setStatus_slg('AUTHORIZED');
						$slg->setUsr_serial_usr($_SESSION['serial_usr']);

						if ($slg->insert()) {
							$error = 1;
						} else {//The sale_log wasn't inserted.
							$error = 2;
						}
					} else {//Updtae Status Failed.
						$error = 3;
					}
				}

				foreach ($arrayEmails as $email) {
					$infoMail['email'] = $email;
					GlobalFunctions::sendMail($infoMail, 'change_standBy');
				}
			} else {
				$sale->setStatus_sal($_POST['rdStatus']);
				if ($sale->updateStatus()) {
					$slg->setStatus_slg('AUTHORIZED');
					$misc['global_info']['authorizing_usr'] = $_SESSION['serial_usr'];
					$misc['global_info']['authorizing_date'] = date('d/m/Y');
					$misc['global_info']['authorizing_obs'] = html_entity_decode('Cambio Autom&aacute;tico del Sistema');
					$slg->setDescription_slg(serialize($misc));

					if ($slg->insert()) {
						$error = 1;
					} else {//The sale_log wasn't inserted.
						$error = 2;
					}
				} else {//Updtae Status Failed.
					$error = 3;
				}
			}
		} else {
			$error = 6;
		}
	} else {//The card number doesn't exist.
		$error = 4;
	}

	http_redirect('main/sales_modifications/' . $error);
?>