<?php
/*
File: pCheckSalesAplication
Author: Esteban Angulo
Creation Date: 05/05/2010
Modifies By: David Bergmann
Last Modified: 13/07/2010
*/
//Debug::print_r($_POST);
//die();

//ErrorLog::log($db, 'AUTHORIZING SALE ' . $_POST['hdnSerial_sal'], $_POST);

$sLog=new SalesLog($db,$_POST['hdnSerial_slog']);
$sLog->getData();
$sLog->setUsr_serial_usr($_SESSION['serial_usr']);
$description=unserialize($sLog->getDescription_slg());
$description['authorize']=$_POST['authorizeObs'];

$description['global_info']['authorizing_usr'] = $_SESSION['serial_usr'];
$description['global_info']['authorizing_date'] = date('d/m/Y');
$description['global_info']['authorizing_obs'] = addslashes($_POST['authorizeObs']);
$sLog->setDescription_slg(serialize($description));

if($_POST['hdnSerial_sal'] && $_POST['selAuthorize']=='AUTHORIZED'){
	$autoAuthorized = $_POST['autoAuthorized'];
	if(!$autoAuthorized){
		$autoAuthorized = 0;
	}
	
	 $sLog->setStatus_slg('AUTHORIZED');

	 if($sLog->update()){
		$serial_sal=$description['new_sale']['serial_sal'];
        
        //Contract
        $serial_con = null;
        $serial_cus = $description['new_sale']['serial_cus'];
        $begin_date = new DateTime();
        $end_date = new DateTime();

        $contracts_cus = Contracts::getContractByCustomer($db, $serial_cus, 'ACTIVE');
        if ($contracts_cus && $contracts_num = count($contracts_cus)>0) {
            $departure_date = DateTime::createFromFormat('d/m/Y', $description['new_sale']['begin_date_sal'])->getTimestamp();
            $arrival_date = DateTime::createFromFormat('d/m/Y', $description['new_sale']['end_date_sal'])->getTimestamp();
            $contracts_con = 0;

            foreach ($contracts_cus as $key => $con){
                $contracts_con++;
                $contract_beg = DateTime::createFromFormat('d/m/Y', $con['begin_date'])->getTimestamp();
                $contract_end = DateTime::createFromFormat('d/m/Y', $con['end_date'])->getTimestamp();

                if ($contract_beg <= $departure_date && $contract_end >= $arrival_date){
                    $serial_con = $con['serial_con'];
                    break;
                } elseif ($contracts_con == $contracts_num && $departure_date < $contract_end){
                    $begin_date->setTimestamp($departure_date);
                    $end_date->setTimestamp($departure_date);
                }
            }
        }

        //Create Contract
        $contract = new Contracts($db, $serial_con);
        if (!$contract->getBegin_date_con() && !$contract->getEnd_date_con() && !$contract->getNumber_con()){
            $contract->setSerial_cof(1);
            $contract->setSerial_cus($serial_cus);

            $nextAvailableContract = Stock::getNextContractNumber($db);
            $end_date->add(new DateInterval('P1Y'));

            $contract->setNumber_con($nextAvailableContract);
            $contract->setBegin_date_con($begin_date->format('d/m/Y'));
            $contract->setEnd_date_con($end_date->format('d/m/Y'));
            $contract->setStatus_con('ACTIVE');
            $serial_con = $contract->insert();
        }
        
		$sale=new Sales($db,$serial_sal);
		$sale->getData();
		$sale->setSerial_pbd($description['new_sale']['serial_pbd']);
		$sale->setSerial_cus($description['new_sale']['serial_cus']);
        $sale->setSerial_con($serial_con);
		$sale->setSerial_cit($description['new_sale']['serial_cit']);
		$sale->setCardNumber_sal($description['new_sale']['card_number_sal']);
		$sale->setBeginDate_sal($description['new_sale']['begin_date_sal']);
		$sale->setEndDate_sal($description['new_sale']['end_date_sal']);
		$sale->setDays_sal($description['new_sale']['days_sal']);
		$sale->setFee_sal($description['new_sale']['fee_sal']);
		$sale->setObservations_sal(specialchars($description['new_sale']['observations_sal']));
		$sale->setCost_sal($description['new_sale']['cost_sal']);
		$sale->setFree_sal($description['new_sale']['free_sal']);
		$sale->setStatus_sal($description['new_sale']['status_sal']);
		$sale->setTotal_sal($description['new_sale']['total_sal']);
		$sale->setTotalCost_sal($description['new_sale']['total_cost_sal']);
		$sale->setChange_fee_sal($description['new_sale']['change_fee_sal']);
		$sale->setInternationalFeeAmount_sal($description['new_sale']['international_fee_amount']);

		if($sale -> update()){
			if($description['new_extras']){
				$extras = new Extras($db);
				$extras -> delete($serial_sal);
				
				foreach($description['new_extras'] as $extra){
					$extras -> setSerial_sal($serial_sal);
					$extras -> setSerial_cus($extra['serialCus']);
					$extras -> setRelationship_ext($extra['selRelationship']);
					$extras -> setFee_ext($extra['hdnPriceExtra']);
					$extras -> setCost_ext($extra['hdnCostExtra']);
					if(!$extras -> insert()){
						$error = 5;
						ErrorLog::log($db, 'AUTHORIZED SALES MOD ADD EXTRA FAULT', $extras);
						http_redirect('modules/sales/modifications/fSearchPendingModifyApplications/5');
					}else{
						$error = 1;
					}
				}
			}
			
			/* REGISTER THE SALES SERVICES */
			if($description['old_services'] && !$description['new_services']):
				ServicesByCountryBySale::deleteServicesBySale($db, $serial_sal);
			endif;			
			
			if($description['new_services']){
				ServicesByCountryBySale::deleteServicesBySale($db, $serial_sal);
				
				foreach($description['new_services'] as $serial_sbd){
					$sale -> insertServices($serial_sal, $serial_sbd);
				}
			}
			/* END SERVICES */

			$alert = new Alert($db);
			
			$alertType = 'SALE';
			if($sLog->getType_slg()=='MODIFICATION'){
				$alertType = 'SALE';
				ERP_logActivity('UPDATE_SALES', $serial_sal, 'PUT'); //ERP ACTIVITY
			}elseif($sLog->getType_slg()=='VOID_SALE'){
				$alertType = 'VOID_SALE';
				
				//*********************** DELETE ALL TRAVELS REGISTERED FOR VOID SALES
				TravelerLog::voidAllTravelsRegistered($db, $serial_sal);
				
				//***************** INTERNATIONAL FEE REFUND ****************
				//NO VOID IS CHARGED ON MANAGERS BILL. DECISION MADE BY JFPONCE ON DEC-2 2013
				GlobalFunctions::refundInternationalFee($db, $serial_sal);
			}else{
				$alertType = 'SPECIAL_SALE_MODIFICATION';
				ERP_logActivity('UPDATE_SALES', $serial_sal, 'PUT'); //ERP ACTIVITY
			}

			if($autoAuthorized != 1){
				if($alert->setServeAlert($alertType,$_POST['hdnSerial_slog'])){
					$error=1;
				}else{
					$error=$autoAuthorized;
				}
			}else{
				$error=1;
			}
		}else{
			ErrorLog::log($db, 'AUTHORIZED SALES MOD CHANGE FAULT', $sLog);
			$error=5;
		}
	 }else{
	 	ErrorLog::log($db, 'MOD AUTH FAULT 1', $sLog);
		$error = 4;
	 }
}else{
	$sLog->setStatus_slg('DENIED');
	
	if($sLog->update()){
		$alert=new Alert($db);
		
		$alertType = 'SALE';
		if($sLog->getType_slg()=='MODIFICATION'){
			$alertType = 'SALE';
		}elseif($sLog->getType_slg()=='VOID_SALE'){
			$alertType = 'VOID_SALE';
		}else{
			$alertType = 'SPECIAL_SALE_MODIFICATION';
		}
		
		if($autoAuthorized != 1){
			if($alert->setServeAlert($alertType,$_POST['hdnSerial_slog'])){
				$error=2;
			}else{
				$error=3;
			}
		}else{
			$error=2;
		}
		
	}else{
		ErrorLog::log($db, 'MOD AUTH FAULT 2', $sLog);
		$error=4;
	}
}
//ErrorLog::log($db, 'SALES AUTH STD LOG ' . $_POST['hdnSerial_sal'], $error);

http_redirect('modules/sales/modifications/fSearchPendingModifyApplications/'.$error);
?>