<?php
/*
 * File: fChooseStandBySales.php
 * Author: Patricio Astudillo
 * Creation Date: 03/05/2010, 03:49:34 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 03/05/2010, 03:49:34 PM
 */
Request::setString('0:error');

$parameter=new Parameter($db,'16');
$parameter->getData();
$userSerials=unserialize($parameter->getValue_par());
$hasAuthorization=0;
foreach($userSerials as &$u){
	$u=explode(',', $u);
	if(in_array($_SESSION['serial_usr'], $u)){
		$hasAuthorization=1;
	}
}

if($hasAuthorization==1){
	$standByRequest=Alert::getStandByRequest($db, $_SESSION['serial_usr']);
	$smarty->register('standByRequest');
}

$smarty->register('error');
$smarty->display();
?>
