<?php
	/**
	 * File:pEditSale.php
	 * Author: Esteban Angulo
	 * Modified By: Patricio Astudillo
	 * Modification Date: 09-04-2013
	 */

	$slg = new SalesLog($db);
	$saleNew = new Sales($db, $_POST['hdnSerial_sal']);
	$saleNew->getData();
	$saleOld = new Sales($db, $_POST['hdnSerial_sal']);
	$customer = new Customer($db);

	/* * ***************************************************************************************************************************** */
	/* * ***************************************************  New Sale Info START  *************************************************** */
	/* * ***************************************************************************************************************************** */

	/* * ******************** THE CUSTOMER ************************ */
	if ($_POST['serialCus'] != '') { //IF THE CUSTOMER EXISTS
		$customer->setserial_cus($_POST['serialCus']);
		$customer->getData();
	}

	$customer->setSerial_cit($_POST['selCityCustomer']);

	if (!$saleNew->getSerial_inv()) {
		$customer->setFirstname_cus(specialchars($_POST['txtNameCustomer']));
		$customer->setLastname_cus(specialchars($_POST['txtLastnameCustomer']));
		$customer->setDocument_cus($_POST['txtDocumentCustomer']);
		$customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
	}

	$customer->setPhone1_cus($_POST['txtPhone1Customer']);
	$customer->setPhone2_cus($_POST['txtPhone2Customer']);
	$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
	$customer->setEmail_cus($_POST['txtMailCustomer']);
	$customer->setAddress_cus(specialchars($_POST['txtAddress']));
	$customer->setRelative_cus($_POST['txtRelative']);
	$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
	$customer->setStatus_cus('ACTIVE');
	$customer->setType_cus($_POST['selType']);

	if ($_POST['hdnVip']) {
		$customer->setVip_cus($_POST['hdnVip']);
	} else {
		$customer->setVip_cus('0');
	}

	if ($_POST['serialCus'] == '') { //IF THE CUSTOMER DOES NOT EXIST
		$password = GlobalFunctions::generatePassword(6, 8, false);
		$customer->setPassword_cus(md5($password));

		$serialNewCus = $customer->insert();

		if ($serialNewCus) {
			/* Generating the info for sending the e-mail */
			if ($_POST['txtNameCustomer']) {
				$misc['customer'] = $_POST['txtNameCustomer'] . ' ' . $_POST['txtLastnameCustomer'];
			}
			if ($_POST['txtNameCustomer1']) {
				$misc['customer'] = $_POST['txtNameCustomer1'];
			}
			$misc['textForEmail'] = 'Si desea mayor informaci&oacute;n puede ingresar a nuestra p&aacute;gina web <a href = "http://www.planet-assist.net/">www.planet-assist.net</a> y reg&iacute;strese con estos datos: ';
			$misc['username'] = $customer->getEmail_cus();
			$misc['pswd'] = $password;
			$misc['email'] = $customer->getEmail_cus();
			/* END E-mail Info */

			if (GlobalFunctions::sendMail($misc, 'newClient')) { //Sending the client his login info.
				$error = 1;
			} else {
				$error = 6;
			}
			$saleNew->setSerial_cus($serialNewCus);
		} else {
			$error = 5;
			http_redirect('main/editSale/' . $error);
		}
	} else {
		if ($customer->update()) {
			$error = 1;
		} else {
			$error = 7;
		}
		$saleNew->setSerial_cus($_POST['serialCus']);
	}


	/* * ******************** THE SALE ************************ */
	if (!$saleNew->getSerial_inv()) {
		$saleNew->setSerial_pbd($_POST['selProduct']);
	}
	$saleNew->setSerial_cnt($_POST['hdnSerial_cnt']);
	$saleNew->setSerial_inv($_POST['hdnSerial_inv']);
	$saleNew->setEmissionDate_sal($_POST['txtEmissionDate']);
	$saleNew->setBeginDate_sal($_POST['txtDepartureDate']);
	$saleNew->setEndDate_sal($_POST['txtArrivalDate']);
	$saleNew->setInDate_sal($_POST['hdnInDateSal']);
	$saleNew->setDays_sal($_POST['hddDays']);
	$saleNew->setFee_sal($_POST['hdnPrice']);
	//$saleNew -> setObservations_sal(specialchars($_POST['observations']));
	$saleNew->setCost_sal($_POST['hdnCost']);
	if ($_POST['hdnDirectSale']) {
		$saleNew->setDirectSale_sal('YES');
	} else {
		$saleNew->setDirectSale_sal('NO');
	}

	$saleNew->setTotal_sal($_POST['hdnTotal']);
	$saleNew->setTotalCost_sal($_POST['hdnTotalCost']);
	$saleNew->setChange_fee_sal($_POST['rdCurrency']);
	if ($_POST['selCityDestination']) {
		$saleNew->setSerial_cit($_POST['selCityDestination']);
	} else {
		$saleNew->setSerial_cit('NULL');
	}
	$saleNew->setType_sal('SYSTEM');
	$saleNew->setStockType_sal($_POST['hdnStockType']);
	//$saleNew -> setFree_sal($_POST['rdIsFree']);

	if (!$saleNew->onCoverageTime()) {
		$saleNew->setInternationalFeeAmount_sal($_POST['hdnTotalCost']);
	}

	/* * ******************** THE EXTRAS ************************ */
	$extra = new Extras($db);
	$extrasOld = Extras::getExtrasBySale($db, $_POST['hdnSerial_sal']);
	$extrasNew = &$_POST['extra'];

	if (is_array($extrasNew) && !$saleNew->getSerial_inv()) {
		foreach ($extrasNew as $key => &$extra) {
			$custExt = new Customer($db);
			if ($custExt->existsCustomer($extra['idExtra'], NULL)) {
				$customerID = $custExt->getCustomerSerialbyDocument($extra['idExtra']);
				$custExt->setSerial_cus($customerID);
				$custExt->getData();
				$custExt->setType_cus('PERSON');
				$custExt->setFirstname_cus(specialchars($extra['nameExtra']));
				$custExt->setLastname_cus(specialchars($extra['lastnameExtra']));
				$custExt->setEmail_cus($extra['emailExtra']);
				$custExt->setSerial_cit($_POST['selCityCustomer']);
				$custExt->setBirthdate_cus($extra['birthdayExtra']);
				$custExt->setStatus_cus('ACTIVE');
				$custExt->update();
			} else {
				$custExt->setSerial_cit($_POST['selCityCustomer']);
				$custExt->setDocument_cus($extra['idExtra']);
				$custExt->setFirstname_cus(specialchars($extra['nameExtra']));
				$custExt->setLastname_cus(specialchars($extra['lastnameExtra']));
				$custExt->setBirthdate_cus($extra['birthdayExtra']);
				$custExt->setType_cus('PERSON');
				$custExt->setStatus_cus('ACTIVE');
				$custExt->setEmail_cus($extra['emailExtra']);
				$passwordExt = GlobalFunctions::generatePassword(6, 8, false);
				$custExt->setPassword_cus(md5($passwordExt));
				$customerID = $custExt->insert();
				$extra['serialCus'] = $customerID;
			}

			//SAVE QUESTIONS:
			$sessionLanguage = Language::getSerialLangByCode($db, $_SESSION['language']);
			$questionList = Question::getActiveQuestions($db, $sessionLanguage);

			Answer::deleteMyAnswers($db, $customerID);
			foreach ($questionList as $q) {
				$answer = new Answer($db);
				$answer->setSerial_cus($customerID);
				$answer->setSerial_que($q['serial_que']);
				$ans = $_POST['question' . $key . '_' . $q['serial_qbl']];
				if ($ans) {
					$answer->setYesno_ans($ans);
					$answer->insert();
				} else {
					break;
				}
			}
		}//END FOREACH
	}

	/* * ******************** LOG THE MODIFICATION ************************ */
	/* New Sale Info END  */

	if ($saleOld->getData()) {
		$saleOld = get_object_vars($saleOld);
		unset($saleOld['db']);
		$saleNew = get_object_vars($saleNew);
		unset($saleNew['db']);
		$saleNew['modifier'] = $_POST['hdnModifier']; //saves who modified the sale.

		//************************** NEW SERVICES SECTION ********************
		$temp_new_services_array = array();
		
		if (isset($_POST['chkService'])) { //IF NEW SERVICES SELECTED, TAKE THEM
			$temp_new_services_array = $_POST['chkService'];
		} elseif (isset($_POST['hdnFixedServices']) && $saleNew['serial_inv'] != '') { //IF NO NEW SERVICES SELECTED AND SALE HAS INVOICE, TAKE PREVIOUS SERVICES.
			$temp_new_services_array = $_POST['hdnFixedServices'];
		}

		if (count($temp_new_services_array) > 0) {
			foreach ($temp_new_services_array as $arr) {
				$saleNew['Services'][$arr] = $arr;
			}
		}

		/* VERIFY IF THE CARD IS ALREADY IN COVERGARE TIME */
		$begin_coverage = split('/', $saleOld['begin_date_sal']);
		$begin_coverage = strtotime($begin_coverage['2'] . '/' . $begin_coverage['1'] . '/' . $begin_coverage['0']);
		$today_time = strtotime(date('Y/m/d'));

		if ($begin_coverage <= $today_time) {
			$began_coverage = true;
		} else {
			$began_coverage = false;
		}
		/* END COVERAGE */

		//Save the LOG for this sale modification.
		$slg = new SalesLog($db);
		$slg->setType_slg('MODIFICATION');
		if ($began_coverage && $saleOld['status_sal'] != 'STANDBY') {
			$slg->setType_slg('SPECIAL_MODIFICATION');
		}

		/* SERVICES */
		$serv_info = ServicesByCountryBySale::getFullServiceSalesData($db, $saleOld['serial_sal']);
		$sbcbs = new ServicesByCountryBySale($db);
		$sbcbs = $sbcbs->getServicesBySale($saleOld['serial_sal'], $serv_info['serial_dea']);

		$slg->setSerial_usr($_SESSION['serial_usr']);
		$slg->setSerial_sal($_POST['hdnSerial_sal']);
		$misc = array('old_sale' => $saleOld,
			'old_extras' => $extrasOld,
			'old_services' => $sbcbs,
			'new_sale' => $saleNew,
			'new_extras' => $_POST['extra'],
			'new_services' => $saleNew['Services'],
			'modifyObs' => specialchars($_POST['observations']),
			'Description' => 'Sales Modification Request.');

		$misc['global_info']['requester_usr'] = $_SESSION['serial_usr'];
		$misc['global_info']['request_date'] = date('d/m/Y');
		$misc['global_info']['request_obs'] = specialchars($_POST['observations']);

		$slg->setDescription_slg(serialize($misc));
		$slg->setStatus_slg('PENDING');
		$serial_slg = $slg->insert();

		if ($serial_slg) {
			//ErrorLog::log($db, 'REGISTERING SALE ' . $_POST['hdnSerial_sal'], $slg);
			/*		 * ****************************** SALES LOG ALERTS POLICY ***************************************
			 * IF THE CARD BEGAN ITS COVERAGE AND IS NOT STDBY
			 * 		REQUEST FOR MANAGER AUTHORIZATION (ALERT->SPECIAL_SALE_MODIFICATION, PARAMETER->33)
			 * ELSE IF THE CARD DID NOT BEGAN ITS COVERAGE AND IS NOT INVOICED
			 * 		THE MODIFICATION TAKES PLACE.
			 * 	ELSE
			 * 		REQUEST FOR STANDAR AUTHORIZATION. (ALERT-> SALE, PARAMETER->17)
			 */

			$alert = new Alert($db);
			$alert->setStatus_alt('PENDING');
			$alert->setRemote_id($serial_slg);
			$alert->setTable_name('sales/modifications/fCheckSalesApplication/');
			$alert->setMessage_alt('Solicitud de Modificacion de Venta pendiente.');
			$alert->setType_alt('SALE');

			$sendAlert = true;
			if ($began_coverage) {//DO NOT JOIN THESE TWO IFS
				if ($saleOld['status_sal'] != 'STANDBY') {
					$alert->setMessage_alt('Solicitud Especial de Modificacion de Venta pendiente.');
					$alert->setType_alt('SPECIAL_SALE_MODIFICATION');
				}
			} elseif ($_POST['hdnSerial_inv'] == "") { //IF NOT BEGAN COVERAGE AND IS NOT INVOICED
				//ErrorLog::log($db, 'AUTO MODIFICATION ' . $_POST['hdnSerial_sal'], $slg);
				$url = URL . "modules/sales/modifications/pCheckSalesApplication";
				if (!function_exists('curl_init'))
					die('CURL is not installed!');

				// create a new curl resource
				$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
				session_write_close();

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "hdnSerial_sal=" . $_POST['hdnSerial_sal'] . "&selAuthorize=AUTHORIZED&hdnSerial_slog=" . $serial_slg . "&autoAuthorized=1&authorizeObs=Venta no facturada modificacion automatica");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_COOKIE, $strCookie);

				// download the given URL, and return output
				$output = curl_exec($ch);
				if (!$output && $output != 0) {
					ErrorLog::log($db, 'CURL ERROR SALES!! ', curl_error($ch));
				}
				// close the curl resource, and free system resources
				curl_close($ch);
				$error = 9;
				$sendAlert = false;

				ERP_logActivity('UPDATE_SALES', $_POST['hdnSerial_sal'], 'PUT'); //ERP ACTIVITY
			} else { //IF NOT BEGAN COVERAGE AND IS ALREADY INVOICED
				//SEND NORMAL ALERT (ALREADY SET)
				//ErrorLog::log($db, 'NORMAL MODIFICATION ' . $_POST['hdnSerial_sal'], $alert);
			}

			if ($sendAlert) {
				if (!$alert->autoSend($_SESSION['serial_usr'])) {
					$error = 3;
				} else {
					$error = 1;
				}
			}
		} else {//The sale_log wasn't inserted.
			$error = 2;
		}
	}

	http_redirect('main/editSale/' . $error);
?>