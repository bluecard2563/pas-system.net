<?php
/*
 * File: pChangeBlocked.php
 * Author: Gabriela Guerrero
 * Creation Date: 10/05/2010
 * Modifies By: David Bergmann
 * Last Modified: 13/07/2010
 */

$sale=new Sales($db, $_POST['serial_sal']);
$needsAproval=$_POST['hdnNeedAproval'];
/*$sale->getSaleUserMail($_POST['serial_sal']);
$arrayEmails=unserialize( $parameter->getValue_par() );
print_r($arrayEmails);die;
$arrayEmails[count($arrayEmails)]=$sale->getAux();
$user = new User($db,$_SESSION['serial_usr']);
$user->getData();
$infoMail['userName']=$user->getFirstname_usr().' '.$user->getLastname_usr();
$infoMail['cardNumber']=$_POST['cardNumber'];
$infoMail['customerName']=$_POST['customerName'].' '.$_POST['customerSurname'];*/
if($sale->getData()){
	//Save the LOG for this sale modification.
	$slg=new SalesLog($db);
	$slg->setType_slg('BLOCKED');
	$slg->setSerial_usr($_SESSION['serial_usr']);
	$slg->setSerial_sal($_POST['serial_sal']);
	$misc=array('old_status_sal'=>$_POST['status'],
				'new_status_sal'=>$_POST['rdStatus'],
				'Description'=>'Change of state to BLOCKED.',
				'observations'=>$_POST['txtComments']);
	$slg->setDescription_slg(serialize($misc));
	if($_POST['rdStatus']=='BLOCKED'){
		if($needsAproval=='1'){//Needs Aproval
			$slg->setStatus_slg('PENDING');
			$serial_slg=$slg->insert();
			
			if($serial_slg){
				$alert = new Alert($db);
				$alert -> setMessage_alt('Solicitud de cambio a Bloqueada');
				$alert -> setType_alt('BLOCKED');
				$alert -> setStatus_alt('PENDING');
				$alert -> setRemote_id($serial_slg);
				$alert -> setTable_name('sales/modifications/fDispatchBlockedApplications/');
				
				if(!$alert -> autoSend($_SESSION['serial_usr'])){
					$error = 5;
				}else{
					$error = 1;
				}
				/*END ALERTS*/
			}else{//The sale_log wasn't inserted.
				$error=2;
			}
		}else{//Doesn't Need Aproval
			$sale->setStatus_sal($_POST['rdStatus']);

			if($sale->updateStatus()){
                            $slg->setStatus_slg('AUTHORIZED');
							$slg->setUsr_serial_usr($_SESSION['serial_usr']);
                            
                            if($slg->insert()){
                                    $error=1;
                            }else{//The sale_log wasn't inserted.
                                    $error=2;
                            }
			}else{//Updtae Status Failed.
				$error=3;
			}
		}
                /*foreach($arrayEmails as $email){
                    $infoMail['email']=$email;
                    GlobalFunctions::sendMail($infoMail,'change_standBy');
                }*/
	}else{
		$sale->setStatus_sal($_POST['rdStatus']);
		if($sale->updateStatus()){
			$slg->setStatus_slg('AUTHORIZED');

			if($slg->insert()){
				$error=1;
			}else{//The sale_log wasn't inserted.
				$error=2;
			}
		}else{//Updtae Status Failed.
			$error=3;
		}
	}
}else{//The card number doesn't exist.
	$error=4;
}

http_redirect('main/sales_modifications/'.$error);
?>
