<?php
/*
 * File: pvalidateReservedSale.php
 * Author: Valeria Andino
 * Creation Date: 27/07/2016
 * Modifies By:
 * Last Modified:
 */
    
	$sale = new Sales($db, $_POST['serial_sal']);
	$sale->getData();
	//******************************* GET CARD INFORMATION *****************************************
	if ($sale->getData()) {	

		//Save the LOG for this sale modification.
		$slg = new SalesLog($db);
		$slg->setType_slg('MODIFICATION');
		$slg->setSerial_usr($_SESSION['serial_usr']);
		$slg->setSerial_sal($_POST['serial_sal']);
		$misc = array(	'old_status_sal' => $sale->getStatus_sal(),
						'new_status_sal' => $_POST['rdStatus'],
						'Description' => 'Change of state to RESERVED.',
						'observations' => $_POST['txtComments']);
		if(is_array($misc_dates_log)){
			$misc['travel_dates_log'] = $misc_dates_log;
		}

		$misc['global_info']['requester_usr'] = $_SESSION['serial_usr'];
		$misc['global_info']['request_date'] = date('d/m/Y');
		$misc['global_info']['request_obs'] = $_POST['txtComments'];

		$slg->setDescription_slg(serialize($misc));

		if ($_POST['rdStatus'] != $sale->getStatus_sal()) {		
				$sale->setStatus_sal('REGISTERED');
				if ($sale->updateStatus()) {
					$slg->setStatus_slg('AUTHORIZED');
					$misc['global_info']['authorizing_usr'] = $_SESSION['serial_usr'];
					$misc['global_info']['authorizing_date'] = date('d/m/Y');
					$misc['global_info']['authorizing_obs'] = html_entity_decode('Cambio Autom&aacute;tico del Sistema');
					$slg->setDescription_slg(serialize($misc));

					if ($slg->insert()) {
						$error = 1;
					} else {//The sale_log wasn't inserted.
						$error = 2;
					}
				} else {//Updtae Status Failed.
					$error = 3;
				}
		} else {
			$error = 6;
		}
	} else {//The card number doesn't exist.
		$error = 4;
	}

	http_redirect('main/sales_modifications/' . $error);
?>