<?php
/**
 * Created by PhpStorm.
 * User: lpadilla
 * Date: 4/10/2018
 * Time: 9:35
 */
if (empty($_GET['serial_inv'])) {
    $recordPayment = Sales::getRecordPaymentDealer($db, $_SESSION['serial_usr']);
} else {
    $recordPayment = Sales::getRecordPaymentDealerId($db, $_GET['serial_inv']);
    $onlyRecord = "true";

}
$smarty->register('recordPayment,serial_dm,onlyRecord');
$smarty->display();
