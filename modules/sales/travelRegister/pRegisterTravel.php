<?php
	/*
	  File:pRegisterTravel.php
	  Author: Nicolas Flores
	  Creation Date:13.04.2010
	  Modified By:15.04.2010
	  Last Modified:
	 */
	Request::setInteger('serialCus');
	if ($serialCus != '')
		$serial_cus = $serialCus;
	else
		Request::setInteger('serial_cus');

	$error = 0;
	//define a customer object to insert or update the customer who is being registered.
	$customer = new Customer($db);
	//check if the customer already exists
	if ($serial_cus) {//if user exist , update his info
		$customer->setserial_cus($serial_cus);
		$customer->getData();
		$shouldUpdate = 0;
		if ($_POST['txtNameCustomer']) {
			if ($_POST['txtNameCustomer'] != $customer->getFirstname_cus()) {
				$customer->setFirstname_cus($_POST['txtNameCustomer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtLastnameCustomer']) {
			if ($_POST['txtLastnameCustomer'] != $customer->getLastname_cus()) {
				$customer->setLastname_cus($_POST['txtLastnameCustomer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['selCityCustomer']) {
			if ($_POST['selCityCustomer'] != $customer->getSerial_cit()) {
				$customer->setSerial_cit($_POST['selCityCustomer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtAddress']) {
			if ($_POST['txtAddress'] != $customer->getAddress_cus()) {
				$customer->setAddress_cus($_POST['txtAddress']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtPhone1Customer']) {
			if ($_POST['txtPhone1Customer'] != $customer->getPhone1_cus()) {
				$customer->setPhone1_cus($_POST['txtPhone1Customer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtPhone2Customer']) {
			if ($_POST['txtPhone2Customer'] != $customer->getPhone2_cus()) {
				$customer->setPhone2_cus($_POST['txtPhone2Customer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtCellphoneCustomer']) {
			if ($_POST['txtCellphoneCustomer'] != $customer->getCellphone_cus()) {
				$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtMailCustomer']) {
			if ($_POST['txtMailCustomer'] != $customer->getEmail_cus()) {
				$customer->setEmail_cus($_POST['txtMailCustomer']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtRelative']) {
			if ($_POST['txtRelative'] != $customer->getRelative_cus()) {
				$customer->setRelative_cus($_POST['txtRelative']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtPhoneRelative']) {
			if ($_POST['txtPhoneRelative'] != $customer->getRelative_phone_cus()) {
				$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtManager']) {
			if ($_POST['txtManager'] != $customer->getRelative_cus()) {
				$customer->setRelative_cus($_POST['txtManager']);
				$shouldUpdate = 1;
			}
		}
		if ($_POST['txtPhoneManager']) {
			if ($_POST['txtPhoneManager'] != $customer->getRelative_phone_cus()) {
				$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
				$shouldUpdate = 1;
			}
		}
		if ($customer->getStatus_cus() == 'INACTIVE') {
			$customer->setStatus_cus('ACTIVE');
			$shouldUpdate = 1;
		}
		if ($_POST['selCustomerDocumentTypeTravelRecord']) {
			if ($_POST['selCustomerDocumentTypeTravelRecord'] != $customer->getDocumentType()) {
				$customer->setDocumentType($_POST['selCustomerDocumentTypeTravelRecord']);
                $shouldUpdate = 1;
			}
		}
		if ($_POST['rdGender']) {
			if ($_POST['rdGender'] != $customer->getGender_cus()) {
				$customer->setGender_cus($_POST['rdGender']);
                $shouldUpdate = 1;
			}
		}
		//update the register
		if ($shouldUpdate == 1) {
			if ($customer->update()) {
				$error = 0;
			} else {
				$error = 1; //error while updating
			}
		}
	} else {//if customer doesn't exist , insert it
		$customer->setType_cus('PERSON');
		$customer->setStatus_cus('ACTIVE');

		if ($_POST['txtDocumentCustomer'])
			$customer->setDocument_cus($_POST['txtDocumentCustomer']);
		if ($_POST['txtNameCustomer'])
			$customer->setFirstname_cus($_POST['txtNameCustomer']);
		if ($_POST['txtLastnameCustomer'])
			$customer->setLastname_cus($_POST['txtLastnameCustomer']);
		if ($_POST['selCityCustomer'])
			$customer->setSerial_cit($_POST['selCityCustomer']);
		if ($_POST['txtBirthdayCustomer'])
			$customer->setBirthdate_cus($_POST['txtBirthdayCustomer']);
		if ($_POST['txtAddress'])
			$customer->setAddress_cus($_POST['txtAddress']);
		if ($_POST['txtPhone1Customer'])
			$customer->setPhone1_cus($_POST['txtPhone1Customer']);
		if ($_POST['txtPhone2Customer'])
			$customer->setPhone2_cus($_POST['txtPhone2Customer']);
		if ($_POST['txtCellphoneCustomer'])
			$customer->setCellphone_cus($_POST['txtCellphoneCustomer']);
		if ($_POST['txtMailCustomer'])
			$customer->setEmail_cus($_POST['txtMailCustomer']);
		if ($_POST['txtRelative'])
			$customer->setRelative_cus($_POST['txtRelative']);
		if ($_POST['txtPhoneRelative'])
			$customer->setRelative_phone_cus($_POST['txtPhoneRelative']);
		if ($_POST['txtManager'])
			$customer->setRelative_cus($_POST['txtManager']);
		if ($_POST['txtPhoneManager'])
			$customer->setRelative_phone_cus($_POST['txtPhoneManager']);
		if ($_POST['selCustomerDocumentTypeTravelRecord']) {
			$customer->setDocumentType($_POST['selCustomerDocumentTypeTravelRecord']);
		}
		if ($_POST['rdGender']) {
			$customer->setGender_cus($_POST['rdGender']);
		}

		$serial_cus = $customer->insert();
	}

	if ($serial_cus) {
		//REGISTER THE USER'S QUESTIONS
		$answers_loaded = true;
		if ($_POST['has_questions'] == 'YES') {
			$sessionLanguage = $_SESSION['serial_lang'];
			$question = new Question($db);
			$questionList = Question::getActiveQuestions($db, $sessionLanguage, $serial_cus);			
						
			Answer::deleteMyAnswers($db, $serial_cus);
			foreach($questionList as $q){
				$answer = new Answer($db);
				$answer -> setSerial_cus($serial_cus);
				$answer -> setSerial_que($q['serial_que']);
				$ans = $_POST['cust_answer_'.$q['serial_qbl']];
				
				if($ans){
					if ($q['type_que'] == 'YN'){
						$answer -> setYesno_ans($ans);
					}else{
						$answer -> setAnswer_ans($ans);
					}
					
					if(!$answer -> insert()){
						$answers_loaded = false;
						ErrorLog::log($db, 'FAILED TO INSERT ANSWER, TRAVEL REGISTER:' . $serial_cus, $answer);
						break;
					}
				}else{
					$answers_loaded = false;
					break;
				}
			}
		}

		if ($answers_loaded) {
			//GET MAIN SALE INFORMATION
			$sale = new Sales($db, $_POST['hdnSale']);
			$sale->getData();
			
			//PUTTING ALL THE INFORMATION FOR THE REGISTER.
			$travelLog = new TravelerLog($db);
			$travelLog->setSerial_sal($sale->getSerial_sal());
			$travelLog->setSerial_usr($_SESSION['serial_usr']);
			$travelLog->setSerial_cus($serial_cus);
			$travelLog->setSerial_cit($_POST['selCityDestination']);
			
			//check if a card number is required
			if ($_POST['generates_card_number'] == 1) {
				$counter = new Counter($db, $sale->getSerial_cnt());
				$counter->getData();
				if ($counter->getStatus_cnt() == 'ACTIVE') {
					$serial_cnt = $counter->getSerial_cnt();
				} else {
					$serial_cnt = $counter->getCounterForCorpStock();
				}

				$nextAvailableNumber = Stock::getNextStockNumber($db, true);
				if (!$nextAvailableNumber) {
					http_redirect('main/sales/19');
				}
				
				$travelLog->setCard_number_trl($nextAvailableNumber);
				$travelLog->setSerial_cnt($serial_cnt);
			}
			
			$travelLog->setStart_trl($_POST['txtDepartureDate']);
			$travelLog->setEnd_trl($_POST['txtArrivalDate']);
			$travelLog->setDays_trl($_POST['hddDays']);
			$serial_trl = $travelLog->insert();
			
			if ($serial_trl) {
				$error = 5;
				// Health Statement: After travel register has been saved send customer heath statement by email.
				$cardNumber = trim($_POST['hdnCardNumber']);
				$customerDocument = trim($_POST['txtDocumentCustomer']);
				$systemOrigin = trim($_POST['hdnTravelStatementOrigin']);
				$customerFullName =  ($_POST['txtNameCustomer'] . " " .  $_POST['txtLastnameCustomer']);
				$customerEmail = trim($_POST['txtMailCustomer']);

				$statement = new Statement();
				$customerStatement = $statement->existCustomerStatement($cardNumber, $customerDocument, $systemOrigin);
				if (!empty($customerStatement)) {
					if ($customerStatement['exist']) {
						$misc['card_number'] = $cardNumber;
						$misc['customer_name'] = $customerFullName;
						$misc['customer_email'] = $customerEmail;
						$misc['statement_file'] = $customerStatement['statement']['statement_file_name'];
						if(!empty($customerStatement['statement']['statement_file_name'])) {
							$mailSend = GlobalFunctions::sendMail($misc, 'healthStatementNotification');
						}
					}
				}
			}
		}else{
			$error = 3; //register error in case answers where not inserted
		}
	} else {
		$error = 2; //error while creating the client
	}

	http_redirect("main/travelerRegister/$error/0/$serial_trl");
?>
