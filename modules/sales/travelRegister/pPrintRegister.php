<?php
/*
    Document   : pPrintRegister
    Created on : 12-sep-2010, 14:32:20
    Author     : Santiago Borja
    Description:
    Generates a PDF file for new travel registration.
*/
	Request::setInteger('0:serial_trl');
	Request::setInteger('1:sendEmail');
	$travelerLog = new TravelerLog($db,$serial_trl);
	$travelerLog -> getData();
	$serial_generated_sale = $travelerLog -> getSerial_sal();
	unset($serial_sal);
	
	$city = new City($db, $travelerLog -> getSerial_cit());
	$city -> getData();
	$country = new Country($db,$city -> getSerial_cou());
	$country -> getData();
	
	//Override with specific travel values:
	$tripCountry = $country -> getName_cou();
	$tripCity = $city -> getName_cit();
	$tripBeginDate = ''.str_replace('/', '-', $travelerLog -> getStart_trl());
	$tripEndDate = ''.str_replace('/', '-', $travelerLog -> getEnd_trl());
	$emissionDate = GlobalFunctions::strToTimeDdMmYyyy($travelerLog->getInDate_trl());

	$traveler_log_customer=$travelerLog->getSerial_cus();
	$traveler_log_cardnumber=$travelerLog->getCard_number_trl();
	if($sendMail){
		if($sendMail==1)
			$display = 1;
		else
			$display = 2;
	}else
		$display = 1;

	include('modules/sales/pPrintSalePDF.php');
?>