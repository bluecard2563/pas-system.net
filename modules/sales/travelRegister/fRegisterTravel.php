<?php
/*
  File:fRegisterTravel.php
  Author: Nicolas Flores
  Creation Date:09.04.2010
  Modified By:
  Last Modified:
 */
	Request::setInteger('0:serial_sal');
	Request::setString('1:error');
	Request::setString('2:success');
	
	$sale = new Sales($db, $serial_sal);
	$sale->getData();
	$sessionUser = $_SESSION['serial_usr'];

	if($sale->getStatus_sal() == 'ACTIVE' || $sale->getStatus_sal() == 'REGISTERED'){
		$customer = new Customer($db, $sale->getSerial_cus());
		$customer->getData();

		$serial_cus = $sale->getSerial_cus();
		$typesList = $customer->getAllTypes();
		$countryList = Country::getAllAvailableCountries($db);

		$travelLog = new TravelerLog($db);
		$travelLog->setSerial_sal($serial_sal);
		$travelsRegistered = $travelLog->getTravelLogBySale();
		if (count($travelsRegistered) > 0) {
			$total_travels = ((int) (count($travelsRegistered) / 10)) + 1;
		}

		$saleData = get_object_vars($sale);
		unset($saleData['db']);
		$productByDealer = new ProductByDealer($db, $sale->getSerial_pbd());
		$productByDealer->getData();
		$productByCountry = new ProductByCountry($db, $productByDealer->getSerial_pxc());
		$productByCountry->getData();
		$serial_pxc = $productByCountry->getSerial_pxc();
		$product = new Product($db, $productByCountry->getSerial_pro());
		$product->getData();

		if ($product->getDestination_restricted_pro() == "YES") {
			$destinationRestricted = "YES";
			$country = new Country($db, $productByCountry->getSerial_cou());
			$country->getData();
			$serial_cou = $country->getSerial_cou();
			$smarty->register('destinationRestricted,serial_cou');
		}

		$productData = get_object_vars($product);
		unset($productData['db']);
		if ($product->getGenerate_number_pro() == 'NO') {
			$counter = new Counter($db, $sale->getSerial_cnt());
			$counter->getData();
			if ($counter->getStatus_cnt() == 'ACTIVE') {
				$serial_cnt = $counter->getSerial_cnt();
			} else {
				$serial_cnt = $counter->getCounterForCorpStock();
			}
			$nextAvailableNumber = Stock::getNextStockNumber($db, false);

			if (!$nextAvailableNumber) {
				http_redirect('main/sales/19');
			}
		}
                //Validate sale date
                $days = $sale->validateDates();
                
                if($days == 'true'){
                    $days_available = 90;
                }else{
		$days_available = $sale->getAvailableDays();
                }

		/* PARAMETERS */
		$parameter = new Parameter($db, 1); // --> PARAMETER FOR ELDER QUESTIONS
		$parameter->getData();
		$parameterValue = $parameter->getValue_par(); //value of the paramater used to display or not the questions
		$parameterCondition = $parameter->getCondition_par(); //condition of the paramater used to display or not the questions
		//check if the card should validate the travel dates to be higher than the last travel end_date_sal
		if ($productData['third_party_register_pro'] == 'NO') {
			if ($travelsRegistered) {
				$date_last_travel = $travelsRegistered[0]['end_trl'];
			} else {
				$date_last_travel = date('d/m/Y');
			}
		}

		// Set values to health statement button
		$cardNumber = $sale->getCardNumber_sal();
		$today = date('d/m/Y');
		$smarty->register('serial_cus,error,success,today,date_last_travel,serial_cnt,serial_sal,maxSerial,cardNumber,sessionUser');
		$smarty->register('questionList,parameterValue,parameterCondition,saleData,days_available,travelsRegistered');
		$smarty->register('productData,typesList,countryList,nextAvailableNumber,total_travels,serial_pxc');
		$smarty->display();
	}else{
		http_redirect('main/travelerRegister/4');
	}
?>
