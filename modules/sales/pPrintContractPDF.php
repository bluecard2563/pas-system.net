<?php
/*
File: pPrintContractPDF.php
Author: David Rosales
Creation Date: 10/09/2015
Last modified:
Modified By: 
*/

Request::setString('0:serialSal');
Request::setString('1:printType');
Request::setString('2:printLanguage');

if($serialSal) $serial_sal = $serialSal;
if($serial_generated_sale) $serial_sal = $serial_generated_sale;//Captured from invoking file

if($printType) $display = $printType;

$langCode = Language::getSerialLangByCode($db,$_SESSION['language']);
//GET LANGUAGE CODE FOR LABEL'S TEXT
$lang_string = new Language($db, $langCode);
$lang_string ->getData();
$lang_string = $lang_string ->getCode_lang();

if($printLanguage) $langCode = $printLanguage;


/************************************ CONTRACT CREATION PDF *****************************************/
set_time_limit(36000);
ini_set('memory_limit','512M');



class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        if ($this->header_xobjid === false) {
			// start a new XObject Template
			$this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);
			$headerfont = $this->getHeaderFont();
			$headerdata = $this->getHeaderData();
			$this->y = $this->header_margin;
			if ($this->rtl) {
				$this->x = $this->w - $this->original_rMargin;
			} else {
				$this->x = $this->original_lMargin;
			}
			if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
                $this->SetAlpha(0.5);
				$imgtype = TCPDF_IMAGES::getImageFileType(LOCAL_PATH_IMAGES.$headerdata['logo']);
                $img_x = A4_PAGE_WIDTH - $this->original_rMargin - ($headerdata['logo_width']);
				if (($imgtype == 'eps') OR ($imgtype == 'ai')) {
					$this->ImageEps(LOCAL_PATH_IMAGES.$headerdata['logo'], $img_x, '', $headerdata['logo_width']);
				} elseif ($imgtype == 'svg') {
					$this->ImageSVG(LOCAL_PATH_IMAGES.$headerdata['logo'], $img_x, '', $headerdata['logo_width']);
				} else {
					$this->Image(LOCAL_PATH_IMAGES.$headerdata['logo'], $img_x, '', $headerdata['logo_width']);
				}
				$imgy = $this->getImageRBY();
			} else {
				$imgy = $this->y;
			}
			$cell_height = $this->getCellHeight($headerfont[2] / $this->k);
			// set starting margin for text data cell
			if ($this->getRTL()) {
				//$header_x = $this->original_rMargin + ($headerdata['logo_width'] * 1.1);
                $header_x = $this->original_rMargin;
			} else {
				//$header_x = $this->original_lMargin + ($headerdata['logo_width'] * 1.1);
                $header_x = $this->original_lMargin;
			}
			$cw = $this->w - $this->original_lMargin - $this->original_rMargin - ($headerdata['logo_width'] * 1.1);
			$this->SetTextColorArray($this->header_text_color);
			// header title
			$this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
			$this->SetX($header_x);
			$this->Cell($cw, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
			// header string
			//$this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
			$this->SetX($header_x);
			$this->MultiCell($cw, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false, true, 0, 'T', false);
			// print an ending header line
//			$this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $headerdata['line_color']));
//			$this->SetY((2.835 / $this->k) + max($imgy, $this->y));
//			if ($this->rtl) {
//				$this->SetX($this->original_rMargin);
//			} else {
//				$this->SetX($this->original_lMargin);
//			}
//			$this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
			$this->endTemplate();
		}
		// print header template
		$x = 0;
		$dx = 0;
		if (!$this->header_xobj_autoreset AND $this->booklet AND (($this->page % 2) == 0)) {
			// adjust margins for booklet mode
			$dx = ($this->original_lMargin - $this->original_rMargin);
		}
		if ($this->rtl) {
			$x = $this->w + $dx;
		} else {
			$x = 0 + $dx;
		}
		$this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
		if ($this->header_xobj_autoreset) {
			// reset header xobject template at each page
			$this->header_xobjid = false;
		}
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-20);
        
        $this->SetAlpha(0.5);
        $img_x = A4_PAGE_WIDTH - $this->original_rMargin - 15;
        $this->Image(LOCAL_PATH_IMAGES.'logoblue_thumb.jpg', $img_x, '', '15');
        
        $cw = $this->w - $this->original_lMargin - $this->original_rMargin - (15 * 1.1);
        $this->Cell($cw, 15, 'Aprobado según Resolución No. 0000000338 del 21 de Septiembre del 2012', 0, 0, '', 0, '', 0);
        
        // Page number
        $this->Cell(0, 15, $this->getAliasNumPage().' de '.$this->getAliasNbPages().'     ', 0, 0, 'R', 0, '', 0, false, 'T', 'M');
    }
}

include("modules/sales/contractTemplates/contractTexts/text_new_contract.php");


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0,84,163));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 12);
// print a block of text using Write()
$pdf->Write(0, html_entity_decode('CONTRATO COLECTIVO DE PRESTACI&Oacute;N DE SERVICIOS DE SALUD Y MEDICINA PREPAGADA'), '', 0, 'C', true, 0, false, false, 0);
$pdf->Ln();
$pdf->Write(0, html_entity_decode('“ASISTENCIA MEDICA EMERGENTE INDIVIDUAL”'), '', 0, 'C', true, 0, false, false, 0);
//$pdf->Ln();

// set font
$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 12);

//Required Contract Data
$var = 0;
$serial_con = 0;
global $global_weekMonths;
$customer_name = $customer_address = $begin_date_con = $end_date_con = $con_days_beg = $con_month_beg = $con_year_beg = "";

$sale = new Sales($db,$serial_sal);
if ($sale->getData()) {
    $contract = new Contracts($db, $sale->getSerial_con());
    if($contract->getBegin_date_con()){
    $begin_date_con = DateTime::createFromFormat('Y-m-d h:i:s', $contract->getBegin_date_con());
    $con_days_beg = $begin_date_con->format('j');
    $con_month_beg = $global_weekMonths[$begin_date_con->format('n')];
    $con_year_beg = $begin_date_con->format('Y');
    }
    if ($contract->getSerial_cus()){
        $serial_con = $contract->getSerial_con();
        
        $customer = new Customer($db, $contract->getSerial_cus());
        $customer->getData();
        $customer_name = utf8_encode($customer->getFirstname_cus() . ' '  . $customer->getLastname_cus());
        $customer_address = utf8_encode($customer->getAddress_cus());
    }
}

$keys = array("%con_days_beg%", "%con_month_beg%", "%con_year_beg%", "%company_name%", "%legal_name_com%", "%legal_name_com_address%", "%commercial_name%", "%company%", "%legal_manager%", "%customer_name%", "%customer_address%", "%contracting_party%");
$values   = array($con_days_beg, $con_month_beg, $con_year_beg,"BLUECARD ECUADOR S.A.", "BLUE CARD S.A.", "Avenida Gonzalo Serrano N37-13 y Jos&eacute; Correa", "BLUE CARD", "LA COMPA&Ntilde;&Iacute;A", "Juan Carlos Prieto Castro", $customer_name, $customer_address, "EL CONTRATANTE");

$html = "";
foreach ($saleContract as $key => &$cont) {
    if ($key == 'article_'.$var){
        $cont = str_replace($keys, $values, $cont);
        // output the HTML content
        if (isset($cont['title']))
            $pdf->writeHTML($cont['title'], true, 0, true, 0);
        
        $pdf->writeHTML($cont['content'], true, 0, true, 0);
        $pdf->Ln();
        $var++;
    }
}

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BLUE CARD S.A.');
$pdf->SetTitle('CONTRATO COLECTIVO DE PRESTACION DE SERVICIOS DE SALUD Y MEDICINA PREPAGADA');
$pdf->SetSubject('ASISTENCIA MEDICA EMERGENTE INDIVIDUAL');
$pdf->SetKeywords('PDF, CONTRATO, ASISTENCIA');

// set additional information
$info = array(
    'Name' => 'BLUE CARD S.A.',
    'Location' => 'Avenida Gonzalo Serrano N37-13 y Jose Correa',
    'Reason' => 'CONTRATO COLECTIVO DE PRESTACION DE SERVICIOS DE SALUD Y MEDICINA PREPAGADA',
    'ContactInfo' => 'http://bluecard.com.ec/',
    );


/* set certificate file
/*$certs = array(); 
//$pkcs12 = file_get_contents( DOCUMENT_ROOT."lib/esigns/bluecard_ecuador_sa.p12" );
$pkcs12 = file_get_contents( K_PATH_MAIN."examples/data/cert/tcpdf.p12" );
if(openssl_pkcs12_read($pkcs12, $certs, "Bluecard2003")){
	$privateKey = $certs['pkey'];
	$certificate = $certs['cert'];
}

// set document signature
$pdf->setSignature($certificate, $privateKey, 'tcpdfdemo', '', 2, $info);*/

// *** set signature appearance ***
// create content for signature (image and/or text)
$pdf->Image(LOCAL_PATH_IMAGES.'jcp_sign.jpg', $pdf->GetX()+15, $pdf->GetY(), 70, 40, '');

// define active area for signature appearance
$pdf->setSignatureAppearance($pdf->GetX()+15, $pdf->GetY(), 70, 40);

$sign_resp = '<table cellspacing="0" cellpadding="1" border="0">
                <tr>
                    <td align="center">p) %legal_name_com%</td>
                    <td align="center">p) %contracting_party%</td>
                </tr>
               </table>';
$txt = str_replace($keys, $values, $sign_resp);
// Print text using writeHTMLCell()
$pdf->writeHTMLCell($w=0, $h=0, $pdf->GetX(), ($pdf->GetY()+38), $txt, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
$pdf->Ln();

if($display=='1'){
    //Close and output PDF document
    $pdf->Output('contract_'.$serial_con.'.pdf', 'I');
}else{
    global $global_yearly_contracts;
    $pdf->Output(DOCUMENT_ROOT . $global_yearly_contracts . 'yearlycontract_'.$serial_con.'.pdf', 'F');
}
