<?php

/*
 * File: pNewMultipleSales.php
 * Author: Patricio Astudillo
 * Creation Date: 18-oct-2010, 15:52:50
 * Modified By: Patricio Astudillo
 */

set_time_limit(0);
ini_set('memory_limit', '1024M');

if (isset($_SESSION['customer_list'])) {
    $customer_list = $_SESSION['customer_list'];

    $serial_cnt = $_POST['hdnCounter_id_toUse'];
    if (!$serial_cnt)
        $serial_cnt = $_POST['hdn_counter_remote_id'];

    $serial_pbd = $_POST['selProduct'];

    $extra = new Extras($db);
    $counter = new Counter($db, $serial_cnt);
    $counter->getData();
    $branch = new Dealer($db, $counter->getSerial_dea());
    $branch->getData();
    $geo_information = $branch->getAuxData_dea();
    $currency_data = CurrenciesByCountry::getOfficialCurrencyData($db, $geo_information['serial_cou']);
    
    $sale = new Sales($db);
    $sale->setSerial_cnt($serial_cnt);
    $sale->setSerial_pbd($serial_pbd);
    $sale->setFree_sal('NO');
    $sale->setObservations_sal('Multiple Sale');
    $sale->setStatus_sal('REGISTERED');
    $sale->setStockType_sal('VIRTUAL');
    $sale->setEmissionDate_sal(date('d/m/Y'));
    $sale->setType_sal('SYSTEM');
    $sale->setInternationalFeeStatus_sal('TO_COLLECT');
    $sale->setInternationalFeeUsed_sal('NO');
    $sale->setDirectSale_sal($sale->isDirectSale($serial_cnt));
    $sale->setSerial_cur($currency_data['serial_cur']);
    $sale->setChange_fee_sal($currency_data['exchange_fee_cur']);

    foreach ($_POST['customer_list'] as $c) {
        if (Customer::customerOnBlackList($db, $c['serial'])):
            $black_list_element = 'yes';
            continue;
        endif;

        /* DATES */
        $start_trl = strtotime($c['start_trl']);
        $end_trl = strtotime($c['end_trl']);

        $star_date = $c['start_trl'];
        $star_date = split('-', $star_date);
        $star_date = $star_date['2'] . '/' . $star_date['1'] . '/' . $star_date['0'];

        $end_date = $c['end_trl'];
        $end_date = split('-', $end_date);
        $end_date = $end_date['2'] . '/' . $end_date['1'] . '/' . $end_date['0'];
        /* DATES */

        /* CARD NUMBER */
        $card_number = Stock::getNextStockNumber($db, true);
        if (!$card_number) {
            http_redirect('main/sales/19');
        }
        /* CARD NUMBER */

        //************* DESTINATION VALIDATION **************
        if (AllowedDestinations::canProceedMultiSaleWithDestinationRestriction($db, $serial_pbd, $c['destination_cit'])) {

            if (CONTRACT_LOGIC) {
                //Contract
                $serial_con = null;
                $serial_cus = $c['serial'];
                $begin_date_con = new DateTime();
                $end_date_con = new DateTime();

                $contracts_cus = Contracts::getContractByCustomer($db, $serial_cus, 'ACTIVE');
                if ($contracts_cus && $contracts_num = count($contracts_cus) > 0) {
                    $departure_date = DateTime::createFromFormat('d/m/Y', $star_date)->getTimestamp();
                    $arrival_date = DateTime::createFromFormat('d/m/Y', $end_date)->getTimestamp();
                    $contracts_con = 0;

                    foreach ($contracts_cus as $key => $con) {
                        $contracts_con++;
                        $contract_beg = DateTime::createFromFormat('d/m/Y', $con['begin_date'])->getTimestamp();
                        $contract_end = DateTime::createFromFormat('d/m/Y', $con['end_date'])->getTimestamp();

                        if ($contract_beg <= $departure_date && $contract_end >= $arrival_date) {
                            $serial_con = $con['serial_con'];
                            break;
                        } elseif ($contracts_con == $contracts_num && $departure_date < $contract_end) {
                            $begin_date_con->setTimestamp($departure_date);
                            $end_date_con->setTimestamp($departure_date);
                        }
                    }
                }

                //Create Contract
                $contract = new Contracts($db, $serial_con);
                if (!$contract->getBegin_date_con() && !$contract->getEnd_date_con() && !$contract->getNumber_con()) {
                    $contract->setSerial_cof(1);
                    $contract->setSerial_cus($serial_cus);

                    $nextAvailableContract = Stock::getNextContractNumber($db);
                    $end_date_con->add(new DateInterval('P1Y'));

                    $contract->setNumber_con($nextAvailableContract);
                    $contract->setBegin_date_con($begin_date_con->format('d/m/Y'));
                    $contract->setEnd_date_con($end_date_con->format('d/m/Y'));
                    $contract->setStatus_con('ACTIVE');
                    $serial_con = $contract->insert();
                }
            }

            $sale->setSerial_cit($c['destination_cit']);
            $sale->setSerial_cus($c['serial']);
            $sale->setSerial_con($serial_con);
            $sale->setBeginDate_sal($star_date);
            $sale->setEndDate_sal($end_date);
            $sale->setEmissionDate_sal(date('d/m/Y'));
            $sale->setCardNumber_sal($card_number);
            $sale->setDays_sal($c['travel_days']);
            $sale->setFee_sal($c['price']);
            $sale->setCost_sal($c['cost']);
            $sale->setTotal_sal($c['total_price']);
            $sale->setTotalCost_sal($c['total_cost']);
            $sale->setInternationalFeeAmount_sal($c['total_cost']);
            $sales_id = $sale->insert();
                        
            if ($sales_id) {
                $error = 1;

                //REMOVE KIT FROM STOCK
                $serial_bra = Counter::getCountersDealer($db, $sale->getSerial_cnt());
                KitsMovement::kitsStockMinusOne($db, $serial_bra);

                if (is_array($c['extras'])) {
                    $extra->setSerial_sal($sales_id);

                    foreach ($c['extras'] as $e) {
                        $extra->setSerial_cus($e['serial']);
                        $extra->setRelationship_ext($e['relationship']);
                        $extra->setCost_ext($e['cost']);
                        $extra->setFee_ext($e['price']);

                        if (!$extra->insert()) { //EXTRAS INSERT FAILED
                            ErrorLog::log($db, 'MULTISALE EXTRAS INSERT FAILED ', $extra);
                            $error = 4;
                            break;
                        }
                    }
                }

                /* SERVICES SECTION IF ANY */
                if (is_array($_POST['chkService']) && sizeof($_POST['chkService']) > 0) {
                    foreach ($_POST['chkService'] as $s) {
                        $sbd = new ServicesByCountryBySale($db, $s, $sales_id);
                        $sbd->insert();
                    }
                }
                /* SERVICES SECTION IF ANY */

                ERP_logActivity('SALES', $sales_id); //ERP ACTIVITY
                //GENERATING THE CONTRACTS
                $serial_generated_sale = $sales_id;
                $multi_sale = true;
                include("modules/sales/pPrintSalePDF.php");

                //Printing Contract
                if (CONTRACT_LOGIC):
                    include(DOCUMENT_ROOT . 'modules/sales/pPrintContractPDF.php');
                endif;
            }else { //SALE INSERT FAILED
                ErrorLog::log($db, 'MULTISALE INSERT FAILED ', $sale);
                $error = 3;
                break;
            }
        } else {
            ErrorLog::log($db, 'MULTISALE DESTINATION VALIDATION FAILED ', $sale);
            $error = 5;
            break;
        }
    }

    if ($black_list_element) {
        $error.= "/$black_list_element";
    }

    http_redirect('main/multi_sales/' . $error);
} else {
    http_redirect('main/multi_sales/2');
}
?>
