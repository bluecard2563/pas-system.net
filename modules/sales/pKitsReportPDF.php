<?php
/*
File: pKitsReportPDF
Author: David Rosales
Creation Date: 09/03/2015
Last Modified: 
Modified By: 
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE KITS ENTREGADOS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);


$cardList = Sales::getKitsReport($db,$_POST['selManager'],$_POST['selComissionist'],$_POST['selDealer'],$_POST['selBranch'],$_POST['selCounter'],$_POST['txtBeginDate'],$_POST['txtEndDate']);

$totalKitsDelivered = 0;
$totalKitsNOTDelivered = 0;
if(is_array($cardList)) {
    foreach ($cardList as $c) {
        if ($c['deliveredKit'] == 'Si')
            $totalKitsDelivered++;
        else
            $totalKitsNOTDelivered++;
    }
}

if(is_array($cardList)) {
    if ($_POST['selCounter'] && $_POST['selManager'] && $_POST['selComissionist']) {
        $titles = array('card_number'=>utf8_decode(html_entity_decode('<b>N&uacute;mero de Tarjeta</b>')),
                      'customer'=>utf8_decode('<b>Cliente</b>'),
                      'product'=>utf8_decode('<b>Producto</b>'),
                      'deliveredKit'=>utf8_decode('<b>Kit Entregado</b>'));

        $cols = array('card_number'=>array('justification'=>'center','width'=>100),
                'customer'=>array('justification'=>'center','width'=>180),
                'product'=>array('justification'=>'center','width'=>120),
                'deliveredKit'=>array('justification'=>'center','width'=>90));
        
        $pdf->ezText('<b>'.utf8_decode("Comercializador: ").'</b>'.$cardList[0]['dealer'], 10, array('justification'=>'left'));
        $pdf->ezSetDy(-7);
        $pdf->ezText('<b>'.utf8_decode("Sucursal: ").'</b>'.$cardList[0]['branch'], 10, array('justification'=>'left'));
        $pdf->ezSetDy(-7);
        $pdf->ezText('<b>'.utf8_decode("Counter: ").'</b>'.$cardList[0]['counter'], 10, array('justification'=>'left'));
        $pdf->ezSetDy(-7);
    } else {
        $titles = array('card_number'=>utf8_decode(html_entity_decode('<b>N&uacute;mero de Tarjeta</b>')),
                      'customer'=>utf8_decode('<b>Cliente</b>'),
                      'responsible'=>utf8_decode('<b>Responsable</b>'),
                      'dealer'=>utf8_decode('<b>Comercializador</b>'),
                      'branch'=>utf8_decode('<b>Sucursal</b>'),
                      'counter'=>utf8_decode('<b>Counter</b>'),
                      'product'=>utf8_decode('<b>Producto</b>'),
                      'deliveredKit'=>utf8_decode('<b>Kit Entregado</b>'));

        $cols = array('card_number'=>array('justification'=>'center','width'=>70),
                'customer'=>array('justification'=>'center','width'=>110),
                'responsible'=>array('justification'=>'center','width'=>100),
                'dealer'=>array('justification'=>'center','width'=>110),
                'branch'=>array('justification'=>'center','width'=>110),
                'counter'=>array('justification'=>'center','width'=>100),
                'product'=>array('justification'=>'center','width'=>90),
                'deliveredKit'=>array('justification'=>'center','width'=>70));
    }
    
    $pdf->ezText('<b>Ventas realizadas: </b>'.($totalKitsDelivered+$totalKitsNOTDelivered), 10, array('justification'=>'left'));
    $pdf->ezSetDy(-7);
    $pdf->ezText('<b>Total de Kits entregados:</b> '.$totalKitsDelivered, 10, array('justification'=>'left'));
    $pdf->ezSetDy(-7);
    $pdf->ezText('<b>Total de Kits No entregados:</b> '.$totalKitsNOTDelivered, 10, array('justification'=>'left'));
    $pdf->ezSetDy(-25);
    
    $pdf->ezTable($cardList,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>$cols));
    $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();

?>