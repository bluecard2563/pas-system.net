<?php
/*
 * File: fNewMultipleSales.php
 * Author: Patricio Astudillo
 * Creation Date: 18-oct-2010, 14:35:02
 * Modified By: Patricio Astudillo
 */

$counter=new Counter($db);
$counter_information=$counter->getCounters($_SESSION['serial_usr']);

if(sizeof($counter_information)>0){
	$country=new Country($db);
	$countryList=$country->getOwnCountries($_SESSION['countryList']);

	$user=new User($db, $_SESSION['serial_usr']);
	$user->getData();
	if($user->getSerial_mbc() !='' || User::isSubManagerUser($db, $_SESSION['serial_usr'])){
		$reasign_sale=TRUE;
	}else{
		$reasign_sale=FALSE;
	}

	/* IF THE USER HAS ONLY ONE COUNTER */
	if(sizeof($counter_information)==1){
		$counter=new Counter($db, $counter_information['0']['serial_cnt']);
		$counter->getData();
		$sale_data['counter']=$counter->getAux_cnt();
		$sale_data['counter']=$sale_data['counter']['counter'];
		
		$dealer=new Dealer($db, $counter->getSerial_dea());
		$dealer->getData();
		$deaData=Dealer::getDealerCity($db, $counter_information['0']['serial_cnt']);

		//Counter Code
		$sale_data['sellerCode']=$deaData['code_cou'].'-'.$deaData['code_cit'].'-'.$dealer->getCode_dea().'-'.$counter->getSerial_dea().'-'.$counter->getSerial_cnt();
		//EmissionCity
		$sale_data['emissionPlace']=$deaData['name_cit'];
		//EmissionDate
		$sale_data['emissionDate']=date("d/m/Y");
		//Product List
		$pbd=new ProductByDealer($db);
		$product_list=$pbd->getproductByDealer($dealer->getDea_serial_dea(), $_SESSION['serial_lang'], 'NO', 'ACTIVE', 'YES');

		$smarty->register('sale_data,product_list');
	}
	/* END ONE COUNTER */
	

	$smarty->register('countryList,counter_information,reasign_sale');
}

$smarty->display();
?>
