<?php
/**
 * File: pChangeComissionist
 * Author: Patricio Astudillo M.
 * Creation Date: 29/02/2012 
 * Last Modified: 29/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('hdnDealersTo');
	Request::setString('txtPercentage');
	$user_id_from = $_POST['selComissionistAssign'];
	$user_id_to = $_POST['hdnSerial_usu'];
	
	$dealerGroupToReasign = explode(',', $hdnDealersTo);
	$globalGroupToReassign = array();					//ARRAY WHERE DEALERS AND BRANCHES TO BE REASIGNED WILL BE PUT
	
	//GETTING THE INFORMATION
	foreach($dealerGroupToReasign as $dea){
		array_push($globalGroupToReassign, $dea);
		
		$branches_list = Dealer::getAllBranchesByDealer($db, $dea);
		
		if($branches_list){
			foreach($branches_list as $bra){
				array_push($globalGroupToReassign, $bra['serial_dea']);
			}
		}
	}
	
	//********************* REASIGNATION ITSELF
	if(sizeof($globalGroupToReassign) > 0) {
		$dealer=new Dealer($db);
		$error=1;
		
		foreach($globalGroupToReassign as $dT) {
			$user_by_dealer = new UserByDealer($db);			
			$user_by_dealer->setSerial_dea($dT);
			$user_by_dealer->getData();
			
			//If the user changes it`s deactivated and the new one is inserted
			if($user_by_dealer->getSerial_usr() != $user_id_to) {
				$user_by_dealer->deactivateUser();
				$user_by_dealer->setSerial_usr($user_id_to);
				$user_by_dealer->setStatus_ubd("ACTIVE");
				$user_by_dealer->setPercentage_ubd($txtPercentage);
				
				if(!$user_by_dealer->insert()) {
					$error=2;
					break;
				}
			} else {
				$user_by_dealer->setSerial_usr($user_id_to);
				$user_by_dealer->setStatus_ubd("ACTIVE");
				$user_by_dealer->setPercentage_ubd($txtPercentage);
				
				if(!$user_by_dealer->update()) {
					$error=2;
					break;
				}
			}
		}
	} else {
		$error=3;
	}

	http_redirect('modules/comissionistByDealer/fChangeComissionist/'.$error);
?>
