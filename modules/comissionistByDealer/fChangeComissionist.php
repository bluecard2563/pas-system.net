<?php
/**
 * File: fChangeComissionist
 * Author: Patricio Astudillo M.
 * Creation Date: 29/02/2012 
 * Last Modified: 29/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');
	$zone=new Zone($db);
	$zoneList=$zone->getZones();

	$smarty->register('error,zoneList');
	$smarty->display();
?>
