<?php 
/*
File: pNewComissionistByDealer.php
Author: David Bergmann
Creation Date: 01/02/2010
Last Modified: 02/02/2010
Modified By: David Bergmann
*/
/*Debug::print_r($_POST);
die();*/
if($_POST['dealersTo']) {
	$dealer=new Dealer($db);
        $error=1;
	foreach($_POST['dealersTo'] as $key=>$dT) {
		//Debug::print_r($_POST);
		//die();
                $user_by_dealer = new UserByDealer($db);
				if($key=='0'){
					$dealer->setSerial_dea($dT);
					$dealer->getData();
					$user_by_dealer->setSerial_dea($dealer->getDea_serial_dea());
					$user_by_dealer->getData();
					//Debug::print_r($user_by_dealer);
					//die();
					//If the user changes it`s deactivated and the new one is inserted
					if($user_by_dealer->getSerial_usr()!=$_POST['hdnSerial_usu']) {
						$user_by_dealer->deactivateUser();
						$user_by_dealer->setSerial_usr($_POST['hdnSerial_usu']);
						$user_by_dealer->setStatus_ubd("ACTIVE");
						$user_by_dealer->setPercentage_ubd($_POST['txtPercentage']);
						if(!$user_by_dealer->insert()) {
							$error=2;
							break;
						}
					}
					else{
						$user_by_dealer->setSerial_usr($_POST['hdnSerial_usu']);
						$user_by_dealer->setStatus_ubd("ACTIVE");
						$user_by_dealer->setPercentage_ubd($_POST['txtPercentage']);
						if(!$user_by_dealer->update()) {
							$error=2;
							break;
						}
					}
				}
                $user_by_dealer->setSerial_dea($dT);
                $user_by_dealer->getData();
                //If the user changes it`s deactivated and the new one is inserted
                if($user_by_dealer->getSerial_usr()!=$_POST['hdnSerial_usu']) {
                    $user_by_dealer->deactivateUser();
                    $user_by_dealer->setSerial_usr($_POST['hdnSerial_usu']);
                    $user_by_dealer->setStatus_ubd("ACTIVE");
					$user_by_dealer->setPercentage_ubd($_POST['txtPercentage']);
                    if(!$user_by_dealer->insert()) {
                        $error=2;
                        break;
                    }
                }
				else{
					$user_by_dealer->setSerial_usr($_POST['hdnSerial_usu']);
					$user_by_dealer->setStatus_ubd("ACTIVE");
					$user_by_dealer->setPercentage_ubd($_POST['txtPercentage']);
					if(!$user_by_dealer->update()) {
						$error=2;
						break;
					}
				}
	}
}
else {
	$error=3;
}

http_redirect('modules/comissionistByDealer/fNewComissionistByDealer/'.$error);
?>