<?php
/**
 * File:  pNewCupon
 * Author: Patricio Astudillo
 * Creation Date: 11-abr-2011, 11:34:13
 * Description:
 * Last Modified: 26-abr-2011, 14:55:00
 * Modified by: Santiago Arellano
 */

	Request::setInteger('selCustomerPromo');
	Request::setInteger('txtNumberToCreate');

	$cPromo = new CustomerPromo($db, $selCustomerPromo);

	if($cPromo -> getData()){
		$xCupon = new Cupon($db);
		$xCupon -> setExpire_date_cup($cPromo -> getEnd_date_ccp());
		$xCupon -> setSerial_ccp($cPromo -> getSerial_ccp());
		$xCupon -> setStatus_cup('ACTIVE');
		$xCupon -> setLimit_amount_cup(0);
		$error = 1;
		
		for($i = 1 ; $i <= $txtNumberToCreate; $i++){
			$number = Cupon::generateCuponNumber($db, $cPromo -> getSerial_ccp(), $i);
			$xCupon -> setNumber_cup($number);

			if(!$xCupon -> insert()){ //INSERT FAILED
				$error = 3;
				break;
			}
		}
	}else{ //CUSTOMER PROMO LOAD DATA NOT POSSIBLE.
		$error = 2;
	}

	http_redirect('modules/cupon/fNewCupon/'.$error);
?>
