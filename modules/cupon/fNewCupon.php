<?php
/**
 * File:  fNewCupon
 * Author: Patricio Astudillo
 * Creation Date: 11-abr-2011, 8:58:34
 * Description: Displays the interface to create one or many cupons.
 * Last Modified: 11-abr-2011, 8:58:34
 * Modified by:
 */

	Request::setInteger('0:error');

	$country_list = CustomerPromo::getCountriesWithPromo($db);

	if(is_array($country_list)){
		foreach($country_list as &$c){
			if($c['name_cou'] == 'Todos'){
				$c['name_cou'] = 'Ventas Web / Telef&oacute;nicas';
			}
		}
	}

	$smarty -> register('country_list,error');
	$smarty -> display();
?>
