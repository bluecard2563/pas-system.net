<?php
/*
File: pServiceByCountry.php
Author: Miguel Ponce
Creation Date: 17/03/2010
Last Modified:
Modified By:
*/

$error = '';
$band = 0;
if($_POST['serial_ser'] != ''){
    $sbc=new ServicesByCountry($db);
    $serial_ser = $_POST['serial_ser'];
    $sbc->setSerial_ser($serial_ser);

    $countries = $_POST['countries'];
    foreach($countries as $c){
        
        //PRODUCT BY COUNTRY SECTION
        
        $serial_cou = $c['selCountry'];
        $sbc->setSerial_cou($c['selCountry']);
        if($c['countryStatus'])
            $sbc->setStatus_sbc($c['countryStatus']);
        else
            $sbc->setStatus_sbc('ACTIVE');
        
        $sbc->setPrice_sbc($c['price_sbc']);
        $sbc->setCost_sbc($c['cost_sbc']);
        $sbc->setCoverage_sbc($c['coverage_sbc']);

        if($c['serial_sbc'] == '' || $c['serial_sbc'] == NULL){//IS A NEW ASSIGNED COUNTRY
            if($sbc->serviceByCountryExists() == 0){//VERIFY IF THE ASSIGNED COUNTRY EXISTS IN THE DB
                $serial_sbc = $sbc->insert();
            }
        }else{//UPDATE THE DATA FOR THE ASSIGNED COUNTRY
            $serial_sbc = $c['serial_sbc'];
            $sbc->setSerial_sbc($serial_sbc);
            if(!$sbc->update()){
                $band = 1;
            }
            $sbd = new ServicesByDealer($db);
            $assigned_dealers = $sbd->getDealerServiceByCountry($serial_sbc);//GET ALL THE ASSIGNED DEALERS TO THIS COUNTRY TO ACTIVATE OR DEACTIVATE THE DEALER
            $sbc->setSerial_sbc(NULL);
        }
        $taxes = $c['taxes'];
        $to_assign_dealers = $c['dealersTo'];
        //TAXES BY PRODUCT BY COUNTRY SECTION
        if($serial_sbc != '' && $serial_sbc !== false){//IF THE COUNTRY WAS INSERTED OR IS AN UPDATED COUNTRY INSERT THE TAXES
            $tbs = new TaxesByService($db);
            $tbs->setSerial_sbc($serial_sbc);
            $tbs->delete();//DELETE ALL THE TAXES ASSIGNED TO THIS COUNTRY TO UPDATE THEM
            if(is_array($taxes)){
                foreach($taxes as $t){
                    $tbs->setSerial_tax($t);
                    if($tbs->taxByServiceExists() == 0){//VERIFY IF THE TAX TO THIS COUNTRY ALREADY EXISTS
                        if(!$tbs->insert()){
                            $band = 1;
                        }
                    }
                }
            }

//debug::print_r($to_assign_dealers);
//debug::print_r($assigned_dealers);

            //DEALER BY SERVICE BY COUNTRY SECTION
            if(is_array($to_assign_dealers)){//IF THERE ARE TO ASSIGN DEALERS
                $sbd = new ServicesByDealer($db);
                foreach($to_assign_dealers as $d){//INSERT THE ASSIGNED DEALERS
                        $sbd->setSerial_dea($d);
                        $sbd->setSerial_sbc($serial_sbc);
                        if(isset($c['countryStatus'])){//IF THE COUNTRY WAS UPDATED SET THE STATUS
                            $sbd->setStatus_sbd($c['countryStatus']);
                        }else{//IF IS A NEW COUNTRY ALL THE ASSIGNED COUNTRIES ARE ACTIVE
                            $sbd->setStatus_sbd('ACTIVE');
                        }
                        $serial_sbd = $sbd->serviceByDealerExists();//VERIFY IF THE PRODUCT BY DEALER EXISTS
                        if($serial_sbd == 0){//IF NOT EXISTS THE INSERT
                            if(!$sbd->insert()){
                                $band = 1;//BAND TO CHECK IF THERE WAS AN ERROR
                            }
                        }else{//IF THE PRODUCT BY DEALER EXISTS UPDATE IT
                            if($c['countryStatus'] == 'INACTIVE'){//CHECK IF THE COUNTRY WAS DISABLED TO DISABLED ALL THE ASSIGNED DEALERS TO THIS COUNTRY
                                $sbd->setSerial_sbc($serial_sbc);//SET THE SERIAL OF THE COUNTRY
                                $sbd->setSerial_sbd(NULL);
                                $sbd->setStatus_sbd('INACTIVE');
                            }else{//IF THE COUNTRY IS ACTIVE THEN ACTIVATE THE DEALERS THAT WAS SELECTED
                                $sbd->setSerial_sbd($serial_sbd);
                                $sbd->setStatus_sbd('ACTIVE');
                            }

                            if(!$sbd->update()){
                                $band = 1;
                            }
                        }
                }


                if(is_array($assigned_dealers)){//IF THERE ARE ASSIGNED DEALERS IN THE DB
                    foreach($assigned_dealers as $ad){
                        if(!in_array($ad, $to_assign_dealers)){//CHECK IF AN ASSGNED DEALER IS NOT IN THE TO ASSIGN DEALERS LIST THEN DISABLE THE DEALER BY PRODUCT
                            $sbd->setSerial_dea($ad);
                            $serial_sbd = $sbd->serviceByDealerExists();
                            $sbd->setSerial_sbc(NULL);
                            $sbd->setSerial_sbd($serial_sbd);
                            $sbd->setStatus_sbd('INACTIVE');
                            if(!$sbd->update()){
								$band = 1;
                            }
                        }
                    }
                }
            }else{//IF THERE ARE NO TO ASSIGN DEALERS THE DEACTIVE ALL THE DEALERS BY PRODUCT BY COUNTRY
                $sbd = new ServicesByDealer($db);
                $sbd->setSerial_sbc($serial_sbc);
                $sbd->setSerial_sbd(NULL);
                $sbd->setStatus_sbd('INACTIVE');
                if(!$sbd->update()){
                    $band = 1;
                }
            }
        }
        if($band == 1){//THERE WAS AN ERROR IN THE PROCESS
            $error = 2;
        }else{
            $error = 1;//EVERYthing WORKED OK
        }
    }
}else{
    $error = 3;
}


if($_POST['btnGuardarSalir'] || $error != 1){
    http_redirect('modules/services/serviceByCountry/fSearchServiceByCountry/'.$error);
}
?>
