<?php
/*
File: pUpdateServiceTranslation.php
Author: Edwin Salvador
Creation Date: 12/02/2010
LastModified: 
Modified By: 
*/

$sbl=new ServicesbyLanguage($db,$_POST['serial_sbl']);

if($sbl->getData()){
    $sbl->setName_sbl($_POST['txtSblName']);
    $sbl->setDescription_sbl($_POST['txtSblDesc']);
    
    if($sbl->update()){
        http_redirect('modules/services/fSearchServiceTranslation/2');
    }else{
        http_redirect('modules/services/fSearchServiceTranslation/4');
    }
}else{
    http_redirect('modules/services/fSearchServiceTranslation/4');
}
?>
