<?php 
/*
File: fUpdateService.php
Author: Edwin Salvador
Creation Date: 12/02/2010
Last Modified:
Modified By:
*/

Request::setInteger('hdnSblID');

$sbl = new ServicesbyLanguage($db, $hdnSblID);

if($sbl->getData()){
    $service_id = $sbl->getSerial_ser();
    $service=new Service($db,$service_id);
    $sbc=new ServicesByCountry($db);
    $sbc->setSerial_ser($service_id);
    $sbc->setSerial_cou('1');
    $sbc->getDataBySerial_ser();
    
    if($service->getData()){
            $data['name_sbl'] = $sbl->getName_sbl();
            $data['description_sbl'] = $sbl->getDescription_sbl();
            $data['serial_lang'] = $sbl->getSerial_lang();
            $data['serial_ser'] = $service_id;
            $data['serial_sbl'] = $hdnSblID;
            $data['serial_sbc'] = $sbc->getSerial_sbc();
            $data['status_ser'] = $service->getStatus_ser();
            $data['fee_type_ser'] = $service->getFee_type_ser();
            $data['price_sbc'] = $sbc->getPrice_sbc();
            $data['cost_sbc'] = $sbc->getCost_sbc();
            $data['coverage_sbc'] = $sbc->getCoverage_sbc();
    }else{
            http_redirect('modules/services/fSearchService/3');
    }
}else{
    http_redirect('modules/services/fSearchService/3');
}
$smarty->register('data,titles,translations,languageList,statusList');
$smarty->display();
?>