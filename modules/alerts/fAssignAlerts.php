<?php
/*
    Document   : fReAssignAlerts.php
    Created on : Apr 28, 2010, 2:54:20 PM
    Author     : H3Dur4k
    Description:
        retrieve necessary data to re assign alerts between Assistance Groups.
*/
//check for groups with alerts
Request::setInteger('0:error');
$assistanceGroup=new AssistanceGroup($db);
$fromGroups=$assistanceGroup->getGroupsWithAlerts();
if($fromGroups!='')
		$smarty->register('fromGroups');
else
	$error=4;

$smarty->register('error');
$smarty->display();
?>
