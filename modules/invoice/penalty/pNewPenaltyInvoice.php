<?php

/*
 * File: pNewPenaltyInvoice.php
 * Author: Patricio Astudillo
 * Creation Date: 18/06/2010, 03:27:57 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 18/06/2010, 03:27:57 PM
 */

$txtCardNumber = $_POST['hdnCardNumber'];

if ($_POST['txtPenalty']) {
	$sale = new Sales($db);
	$sale->setCardNumber_sal($txtCardNumber);

	if ($sale->getDataByCardNumber()) {
		$invoice = new Invoice($db, $sale->getSerial_inv());

		if ($invoice->getData()) {
			$invoice_aux_data = $invoice->getAuxData();
			//*********************** ERP REPLICATION SECTION ********************
			$processCanContinueWithERPReplication = true;
			
			if(ERPConnectionFunctions::invoiceHasERPConnection($invoice_aux_data['serial_man'])){
				if($invoice->getErp_id()){
					$cnote_erp_data['erp_invoice_id'] = $invoice->getErp_id();
					$cnote_erp_data['penalty_fee'] = $_POST['txtPenalty'];
					$cnote_erp_data['IncludeItems'] = array((string)$sale->getCardNumber_sal());

					$erp_response = ERP_logActivity('CREDIT_NOTE', $cnote_erp_data);

					if($erp_response){
						$cnote_number = $erp_response['cnote_number'];
					}else{
						$processCanContinueWithERPReplication = false;
					}
				}
			}
			
			if($processCanContinueWithERPReplication){
				/* CREATE CREDIT NOTE */
				/* BILLING INFORMATION */
				$counter = new Counter($db, $sale->getSerial_cnt());
				$counter->getData();
				$aux = $counter->getAux_cnt();
				/* END INFO */

				/* GET APPLIED DISCOUNTS */
				$discounts = ($invoice->getDiscount_prcg_inv() + $invoice->getOther_dscnt_inv()) / 100; //CRITICAL - DO NOT TRUNCATE DECIMALS
				/* END DISCOUNTS */

				/* GET APPLIED TAXES */
				$taxes = $invoice->getApplied_taxes_inv();
				$taxes = unserialize($taxes);
				$taxesValue = 0;
				if (is_array($taxes)) {
					foreach ($taxes as $t) {
						$taxesValue+=$t['tax_percentage'];
					}
				}
				$taxesValue = $taxesValue / 100; //CRITICAL - DO NOT TRUNCATE DECIMALS
				/* END TAXES */

				//Amount paid by the credit note.
				$paidAmount = $sale->getTotal_sal();

				$paidAmount-=($paidAmount * $discounts);
				$paidAmount-=number_format($_POST['txtPenalty'], '2');
				$paidAmount+=($paidAmount * $taxesValue);

				$paidAmount = number_format(round($paidAmount, '2'), 2, '.', '');

				$mbc = new ManagerbyCountry($db, $aux['serial_mbc']);
				$mbc->getData();

				$misc['type'] = 'PENALTY';
				$misc['serial'] = $sale->getSerial_sal();
				$misc['db'] = $db;
				$misc['serial_man'] = $mbc->getSerial_man();
				$misc['amount'] = $paidAmount;
				$misc['penalty'] = number_format($_POST['txtPenalty'], '2');
				if($cnote_number):
					$misc['cnote_number'] = $cnote_number;
				endif;
				$cNote = GlobalFunctions::generateCreditNote($misc);
				/* END CREDIT NOTE */

				if ($cNote['error'] == 'success') {
					$serial_pay = $invoice->getSerial_pay();
					$payment = new Payment($db, $serial_pay);
					$payment_exist = $payment->getData();

					if (!$payment_exist) { //NO PAYMENT EXISTS
						/* CREATING THE PAYMENT */
						$payment->setStatus_pay('PARTIAL');
						$payment->setTotal_to_pay_pay($invoice->getTotal_inv());
						$payment->setTotal_payed_pay($paidAmount);
						$payment->setExcess_amount_available_pay('0');

						$serial_pay = $payment->insert();
						/* END PAYMENT */
					} else { //THERE'S A PREVIOUS PAYMENT
						$payment->setTotal_payed_pay($payment->getTotal_payed_pay() + $paidAmount);
						if ($payment->getTotal_to_pay_pay() < $payment->getTotal_payed_pay()) {
							$payment->setStatus_pay('EXCESS');
							$payment->setExcess_amount_available_pay($payment->getTotal_payed_pay() - $payment->getTotal_to_pay_pay());
						}
						$payment->update();
					}

					/* CREATING THE PAYMENT DETAIL */
					$paymentDetail = new PaymentDetail($db);
					$paymentDetail->setSerial_pay($serial_pay);
					$paymentDetail->setSerial_usr($_SESSION['serial_usr']);
					$paymentDetail->setSerial_cn($cNote['id']);
					$paymentDetail->setAmount_pyf($paidAmount);
					$paymentDetail->setType_pyf('CREDIT_NOTE');
					$paymentDetail->setComments_pyf('N.C. por Penalidad');
					$paymentDetail->setDate_pyf(date('Y-m-d'));
					$paymentDetail->setDocumentNumber_pyf($cNote['number']);
					/* END PAYMENT DETAIL */

					if ($paymentDetail->insert()) {
						/* UPDATING CREDIT NOTE INFO */
						$cn = new CreditNote($db, $cNote['id']);
						$cn->getData();
						$cn->setPayment_status_cn('PAID');
						$cn->updatePayment_status_cn();
						/* END UPDATING */

						/* UPDATING THE SALES AS VOID */
						$sale->setStatus_sal('VOID');
						if ($sale->updateStatus()) {
							//*********************** DELETE ALL TRAVELS REGISTERED FOR VOID SALES
							TravelerLog::voidAllTravelsRegistered($db, $sale->getSerial_sal());

							//***************** INTERNATIONAL FEE REFUND ****************
							//NO VOID IS CHARGED ON MANAGERS BILL. DECISION MADE BY JFPONCE ON DEC-2 2013
							GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal());

							$error = 1;
						} else {
							$error = 4;
						}
						/* END UPDATING */

						/* UPDATING INVOICE */
						$invoice->setSerial_pay($serial_pay);
						$invoice->update();
						/* END INVOICE */
					} else {
						$error = 5; //ERROR TO INSERT PAYMENT DETAIL
					}
				} else {
					switch ($cNote['error']) {
						case 'insert_error':
							$error = 6;
							break;
						case 'update_error':
							$error = 7;
							break;
						case 'cNote_error':
							$error = 8;
							break;
						case 'no_document':
							$error = 9;
							break;
					}
				}
			}else{ //NO ERP CONNECTION
				$error = 10;
			}
		} else {//Invoice info didn't load.
			$error = 3;
		}
	} else { //Card doesn't exist.
		$error = 2;
	}
	http_redirect('modules/invoice/penalty/fNewPenaltyInvoice/' . $error . '/' . $cNote['id']);
} else {
	http_redirect('modules/invoice/penalty/fNewPenaltyInvoice');
}
?>