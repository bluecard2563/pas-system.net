<?php
/*
File: pVoidInvoiceRequest.php
Author: Santiago Benitez
Creation Date: 08/04/2010
Creation Date: 08/04/2010
Modified By: Santiago Benitez
*/

$invLog = new InvoiceLog($db);
$sales=$_POST['sales'];
$checkedSales=$_POST['chkSales'];
$salesCost=$_POST['salesCost'];
$error=2;
foreach($sales as $k=>$serial_sal){
    $invLog->setSerial_inv($_POST['hdnSerialInv']);
    $invLog->setSerial_usr($_SESSION['serial_usr']);
    $invLog->setSerial_sal($serial_sal);
    $invLog->setObservation_inl($_POST['txtObservationsVoid']);
    $invLog->setPenalty_fee_inl($salesCost[$k]);
    $invLog->setVoid_sales_inl(checkSales($serial_sal,$checkedSales));
    if(!$invLog->insert()){
        $error=1;
        break;
    }
}
if($error == 2){
	$alert = new Alert($db);
	$alert -> setMessage_alt('Usted tiene una solicitud pendiente para anulaci&oacute;n de factura');
	$alert -> setType_alt('INVOICE');
	$alert -> setStatus_alt('PENDING');
	$alert -> setRemote_id($_POST['hdnSerialInv']);
	$alert -> setTable_name('invoice/fAuthorizeInvoiceLog/');
	
	if(!$alert -> autoSend($_SESSION['serial_usr'])){
		$error = 4;
	}
}
function checkSales($serial_sal,$chkSales){
    if($chkSales){
       foreach($chkSales as $ch){
            if($ch==$serial_sal){
                return 'YES';
            }
        }
    }
    return 'NO';
}
http_redirect('modules/invoice/fSearchInvoice/'.$error);
?>