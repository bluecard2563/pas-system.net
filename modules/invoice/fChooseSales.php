<?php
/*
File:fChooseSales.php
Author: Nicolas Flores
Creation Date:02/03/2010
Modified By:
Last Modified: 
*/
Request::setInteger('0:error');
Request::setInteger('1:inserted_serial_inv');
//get the list of countries for the user logged in
$user = new User($db, $_SESSION['serial_usr']);
$user->getData();
$user = get_object_vars($user);
$ptp_use=$user['ptp_use'];
$country=new Country($db);
$countryList=$country->getInvoiceCountries($_SESSION['countryList'], $_SESSION['serial_mbc']);
//Language to be used on data retrieve
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//Dealer Types
$dealerT=new DealerType($db);
$typeList=$dealerT->getDealerTypes(NULL,$language->getSerial_lang());

//CATEGORY
$dealer=new Dealer($db);
$categoryList=$dealer->getCategories();
$serial_usr=$_SESSION['serial_usr'];
$smarty->register('serial_usr');
$smarty -> register('countryList,error,typeList,categoryList,inserted_serial_inv,ptp_use');
$smarty->display();
?>
