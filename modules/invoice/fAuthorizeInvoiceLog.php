<?php
/*
File: fAuthorizeInvoiceLog.php
Author: Santiago Ben�tez
Creation Date: 14/04/2010 10:06
Last Modified: 14/04/2010
Modified By: Santiago Ben�tez
*/

Request::setInteger('0:serial_inv');
Request::setInteger('1:serial_mbc');
Request::setInteger('2:error');

//zones
$zone= new  Zone($db);
$zoneList=$zone->getZones();

//invoice
$invoice=new Invoice($db,$serial_inv);
if($invoice->getData()){
    $invoiceData['serial_inv']=$invoice->getSerial_inv();
    $invoiceData['serial_cus']=$invoice->getSerial_cus();
    $invoiceData['serial_pay']=$invoice->getSerial_pay();
    $invoiceData['serial_dea']=$invoice->getSerial_dea();
    $invoiceData['serial_dbm']=$invoice->getSerial_dbm();
    $invoiceData['date_inv']=$invoice->getDate_inv();
    $invoiceData['due_date_inv']=$invoice->getDue_date_inv();
    $invoiceData['number_inv']=$invoice->getNumber_inv();
    $invoiceData['discount_prcg_inv']=$invoice->getDiscount_prcg_inv();
    $invoiceData['other_dscnt_inv']=$invoice->getOther_dscnt_inv();
    $invoiceData['comision_prcg_inv']=$invoice->getComision_prcg_inv();
    $invoiceData['comments_inv']=$invoice->getComments_inv();
    $invoiceData['printed_inv']=$invoice->getPrinted_inv();
    $invoiceData['status_inv']=$invoice->getStatus_inv();
    $invoiceData['subtotal_inv']=$invoice->getSubtotal_inv();
    $invoiceData['total_inv']=$invoice->getTotal_inv();

	/* RETRIEVE INVOICE_LOG INFO */
    $invoiceLog=new InvoiceLog($db);
    $invoiceLog->setSerial_inv($serial_inv);
    $invoiceLogs=$invoiceLog->getInvoiceLogs('PENDING');
	$observationInl=$invoiceLogs['0']['observation_inl'];
	$penalty=$invoiceLogs['0']['penalty_fee_inl'];
	/* END INVOICE_LOG*/

    if($invoiceData['serial_cus']){
        $customer=new Customer($db, $invoiceData['serial_cus']);
        $customer->getData();
        $extraData=get_object_vars($customer);

        //CITY
        $serial_cit=$customer->getSerial_cit();
        $city= new City($db,$serial_cit);
        $city->getData();
        $extraData['name_cit']=$city->getName_cit();

        //COUNTRY
        $serial_cou=$extraData['auxData']['serial_cou'];
        $country=new Country($db,$serial_cou);
        $country->getData();
        $extraData['name_cou']=$country->getName_cou();

    }else{
        $dealer=new Dealer($db, $invoiceData['serial_dea']);
        $dealer->getData();
        $extraData=get_object_vars($dealer);
    }
}

//sales
$sale=new Sales($db);
$salesList=$sale->getSalesByInvoiceLog($serial_inv);

$smarty->register('error,zoneList,invoiceData,extraData,salesList,global_salesStatus,penalty,invoiceLogs,observationInl,serial_mbc');
$smarty->display();
?>