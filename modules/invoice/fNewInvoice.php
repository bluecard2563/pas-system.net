<?php

/*
  File:
  Author: Nicolas Flores
  Creation Date:08/03/2010
  Modified By: Santiago Benitez
  Last Modified: 18/03/2010
 */

	Request::setInteger('selBranch');
	$serial_mbc = $_POST['selManager'];
	
	/********* MANAGER'S INFO FOR INVOICE NUMBER **********/
	$manager = new ManagerbyCountry($db, $serial_mbc);
	$manager->getData();
	$managerData['serial_mbc'] = $serial_mbc;
	$managerData['serial_cou'] = $manager->getSerial_cou();
	$managerData['invoice_number_mbc'] = $manager->getInvoice_number_mbc();
	$managerData['serial_man'] = $manager->getSerial_man();

	/************ GET SALES ID'S TO PROCESS *********/
	$salesIds = $_POST['chkSales'];
	$salesIdString = implode(",", $salesIds);
	
	/************ CHECK FOR PROMO'S DISCOUNT **********/
	foreach ($salesIds as $sId) {
		$existOtherDiscount = Sales::checkSaleHaveOtherDiscount($db, $sId);
		if ($existOtherDiscount == 1)
			break;
	}
	
	/**************** BRANCH INFO FOR INVOICE HEADER *********************/
	$branch = new Dealer($db, $selBranch);
	$branch->getData();
	$branchData = get_object_vars($branch);
	unset($branchData['db']);
	$branchData['today'] = date('d/m/Y');
	
	/* RETRIEVE SALES'S TOTAL PRICE */
	$sales = new Sales($db);
	$salesT = $sales->getSalesTotal($salesIdString);
	$salesTotal = number_format($salesT, 2, '.', '');

	/**************** PRODUCT HAS COMISION *********************/
	$salesHandler = new Sales($db, $salesIds[0]);
	$salesHandler->getData();
	$productByDealer = new ProductByDealer($db, $salesHandler->getSerial_pbd());
	$productInformation = $productByDealer->getProductByProductDealerId($db, $salesHandler->getSerial_pbd());
	$productHasComision = $productInformation[0]['has_comision_pro'];
	
	/* RETRIEVE SALES'S TOTAL COST */
	$salesCost = number_format($sales->getCostTotal($salesIdString), 2, '.', '');
	if ($branchData['type_percentage_dea'] == 'DISCOUNT' && $productHasComision == 'YES') {
		$totalAfterD = $salesT - ($salesT * $branchData['percentage_dea'] / 100);
	} else {
		if($branchData['percentage_dea'] > 0){
			$branchData['commission'] = $branchData['percentage_dea'] / 100;
		}else{
			$branchData['commission'] = 0;
		}
		
		$totalAfterD = $salesT;
	}
	$totalAfterDiscount = number_format($totalAfterD, 2, '.', '');

	/* RETRIEVE SALE'S INFO */
	$sale = new Sales($db, $salesIds[0]);
	$sale->getData();
	
	if (sizeof($_POST['chkSales']) == 1) {
		$observations = $sale->getObservations_sal();
	}
	
	/******* GET TAXES IF ANY *******/
	$productByDealer = new ProductByDealer($db, $sale->getSerial_pbd());
	$productByDealer->getData();
	$taxes = $productByDealer->getTaxes();
	$taxesToA = 0;
	
	if (is_array($taxes)) {
		foreach ($taxes as $t) {
			$taxesToA += $t['percentage_tax'];
		}
	}

	$taxesToApply = number_format($taxesToA, 2, '.', '');
//	$total = number_format($totalAfterD * (1 + ($taxesToA / 100)), 2, '.', '');
	$total = number_format($totalAfterD, 2, '.', '');
	
	/********** CUSTOMER INFO IF ONLY ONE SALE ON INVOICE ***********/
	if (sizeof($salesIds) == 1 && $sale->getSerial_cus()) {
		$customer = new Customer($db, $sale->getSerial_cus());
		$customer->getData();
		$customerData = get_object_vars($customer);
		$typesList = $customer->getAllTypes();
		unset($customerData['db']);
		
		//retrieves a citylist by country
		$city = new City($db);
		$cityList = $city->getCitiesByCountry($customerData['auxData']['serial_cou']);
	}
	
	/**************** RETRIEVE COMMON DATA FOR CUSTOMER FORM ****************/
	$countryList = Country::getAllAvailableCountries($db);
	$customer = new Customer($db);
	$typesList = $customer->getAllTypes();

	/******************* MANAGER RIGHTS VALIDATIONS ************************/
	$manager_rights = Dealer::branchWithManagerRightsParent($db, $selBranch);
	
	/***** INVOICE NUMBER CONTROL *****/
	if($branchData['aux_data']['serial_cou'] == '62'): //BURNED VALUE FOR ECUADOR
		$validate_holder_document = 'YES';
	else:
		$validate_holder_document = 'NO';
	endif;

	/**************** AGREEMENTS ****************/
	$agreement = new Agreement($db);
	$agreementList = $agreement->getAllActiveAgreements();

	$smarty->register('existOtherDiscount,branchData,salesTotal,totalAfterDiscount,total,taxes,salesCost,taxesToApply');
	$smarty->register('customerData,typesList,countryList,cityList,questionList,parameterValue,parameterCondition');
	$smarty->register('maxSerial,answerList,managerData,salesIds,observations,manager_rights,validate_holder_document');
	$smarty->register('agreementList,productHasComision');
	$smarty->display();
?>
