<?php

/*
  File: pNewInvoice.php
  Author: Santiago Benitez
  Creation Date:22/03/2010
  Modified By: Santiago Benitez
  Last Modified: 22/03/2010
 */

set_time_limit(36000);
ini_set('memory_limit','1024M');

$error = 1;
$flag = 0;
$invoice = new Invoice($db);
$customer = new Customer($db);
$mbc = new ManagerbyCountry($db, $_POST['hdnSerialMbc']);
$mbc->getData();

//customer
if ($_POST['bill_to_radio'] == 'CUSTOMER') {
	$customer->setDocument_cus($_POST['txtCustomerDocument']);
	$customer->setSerial_cit($_POST['selCity']);
	$customer->setFirstname_cus($_POST['txtFirstName']);
	$customer->setLastname_cus($_POST['txtLastName']);
	$customer->setPhone1_cus($_POST['txtPhone1']);
	$customer->setPhone2_cus($_POST['txtPhone2']);
	$customer->setAddress_cus($_POST['txtAddress']);
	$customer->setEmail_cus($_POST['txtEmail']);
	if ($_POST['selType']) {
		$customer->setType_cus($_POST['selType']);
	} else {
		$customer->setType_cus($_POST['hdnTypeDbc']);
	}
	$customer->setStatus_cus('ACTIVE');
	//when a new customer is going to be inserted
	if ($_POST['hdnCustomerAction'] == 'INSERT') {
		$flag = 1;
		$customer->setVip_cus(0);
		$password = GlobalFunctions::generatePassword(6, 8, false);
		$customer->setPassword_cus(md5($password));

		$serial_cus = $customer->insert();
		if (!$serial_cus) {
			$error = 2;
		} else {
			/* Generating the info for sending the e-mail */
			$misc['textForEmail'] = 'Estimado(a) ' . $_POST['txtFirstName'] . ' ' . $_POST['txtLastName'] . '<br><br> Si usted desea hacer uso de nuestro servicio de ventas
                                   on-line, ingrese a <a href="http://www.planet-assist.com/">www.planet-assist.com</a> y reg&iacute;strese con estos datos: ';
			$misc['customer'] = $_POST['txtFirstName'] . ' ' . $_POST['txtLastName'];
			$misc['username'] = $customer->getDocument_cus();
			$misc['pswd'] = $password;
			$misc['email'] = $customer->getEmail_cus();
			/* END E-mail Info */
			//Debug::print_r($misc);
		}
	} else {
		//CUSTOMER IS NOT UPDATED
	}
}

//credit day
$creditDay = new CreditDay($db, $_POST['hdnSerialCdd']);
if (!$creditDay->getData()) {
	$error = 5;
}
//Debug::print_r($error);die();
//document by country
if ($error == 1) {
	$serial_dbm = DocumentByManager::retrieveSelfDBMSerial($db, $_POST['hdnSerialMan'], $_POST['hdnTypeDbc'], 'INVOICE');
	$number_from_erp = false;
//		Debug::print_r($_POST['hdnSerialMbc']);die();
	//****************** ERP_CONNECTIO TO GET INVOICE NUMBER AND PROCEED *************************
	if(ERPConnectionFunctions::invoiceHasERPConnection($_POST['hdnSerialMbc'])){
		$erp_misc_data['discount'] = $_POST['txtDiscount'];
		$erp_misc_data['other_discount'] = $_POST['txtOtherDiscount'];
		if ($_POST['bill_to_radio'] == 'DEALER') {
			$erp_misc_data['holder_id'] = $_POST['hdnSerialDea'];
			$erp_misc_data['holder_type'] = 'D';
		} else {
			if ($_POST['hdnSerialCus']) {
				$erp_misc_data['holder_id'] = $_POST['hdnSerialCus'];
			} else {
				$erp_misc_data['holder_id'] = $serial_cus;
			}

			$erp_misc_data['holder_type'] = 'C';
		}

		$erp_misc_data['sales_ids'] = array();
		$erp_misc_data['operation_type'] = 1; //SYSTEM SALE
		$erp_ids_complete = true;

		foreach($_POST['hdnSaleId'] as $s){
			$erp_id_for_sale = ERPConnectionFunctions::getSaleDataForOO($db, $s);

			if($erp_id_for_sale){
				array_push($erp_misc_data['sales_ids'], $erp_id_for_sale['id_erp_sal']);
			}else{
				$erp_ids_complete = false;
			}
		}
                //Tandi Connection 
//                $saleArray = $_POST;
//                Debug::print_r($saleArray);die();
//                $tandiResponse = TandiConnectionFunctions::tandiEmision($saleArray);
//                if(!$tandiResponse){
//                    $error = 12;
                }
//                Debug::var_dump($erp_misc_data);
//		Debug::print_r($erp_misc_data);die();
		if(count($_POST['hdnSaleId']) == count($erp_misc_data['sales_ids'])){
			$erp_response = ERP_logActivity('INVOICE', $erp_misc_data); //ERP ACTIVITY
			$number_from_erp = true;
			
			if(!$erp_response){
				$error = 12;
			}
		}else{
			$error = 13;
		}
//	}
//	Debug::print_r('error and i dont know');die();
	if (!$serial_dbm) {
		$error = 6;
	}
}
//Debug::print_r($tandiResponse['idFactura']);
//invoice
//Debug::print_r($error);
//Debug::print_r($tandiResponse);die();
if ($error == 1) {
	$days = $creditDay->getDays_cdd();
	$invoice->setComments_inv($_POST['txtObservations']);
	$invoice->setDate_inv($_POST['txtDate']);
	if ($_POST['hdnTypePercentage'] == 'COMISSION') { //if percentage is aplied to comission
		$invoice->setComision_prcg_inv($_POST['hdnComissionValue']);
	} else { //if percentage is aplied to discount
		//check if the invoice is for customer, in this case save as comission
		if ($_POST['bill_to_radio'] == 'CUSTOMER') {
			$invoice->setComision_prcg_inv($_POST['hdnComissionValue']);
		} else {
			$invoice->setDiscount_prcg_inv($_POST['txtDiscount']);
		}
	}
	$date = $_POST['txtDate'];
	$cd = strtotime(substr($date, 3, 3) . substr($date, 0, 3) . substr($date, 6, 4));
	$dueDate = date('d/m/Y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $days, date('Y', $cd)));
	$invoice->setDue_date_inv($dueDate);

	//************ INVOICE NUMBER ACCORDING WITH ERP OR NOT
	if(!$erp_response){
		if ($mbc->getInvoice_number_mbc() == 'AUTOMATIC') {
			$nextAvailableNumber = DocumentByManager::retrieveNextDocumentNumber($db, $_POST['hdnSerialMan'], $_POST['hdnTypeDbc'], 'INVOICE', TRUE);

			if (!$nextAvailableNumber) {
				$error = 8;
			}
		} else {
			$nextAvailableNumber = $_POST['txtInvoiceNumber'];
		}
	}else{
		$nextAvailableNumber = $erp_response['number_inv'];
		
		//KEEP NUMBERS SYNC BETWEEN SYSTEMS
		$serial_dbm = DocumentByManager::retrieveSelfDBMSerial($db, $_POST['hdnSerialMan'], $_POST['hdnTypeDbc'], 'INVOICE');
		DocumentByManager::moveToNextValue($db, $serial_dbm, $nextAvailableNumber);
		
		$invoice->setErp_id($erp_response['erp_id']);
//		$invoice->setErp_id($tandiResponse['idFactura']);
	}
	
//        $invoice->setErp_id(intval($esponse['idFactura']));
	$invoice->setNumber_inv(intval($nextAvailableNumber));
//	$invoice->setNumber_inv($tandiResponse['numeroFactura']);
	$invoice->setOther_dscnt_inv($_POST['txtOtherDiscount']);
	if ($_POST['bill_to_radio'] == 'DEALER') {
		$invoice->setSerial_dea($_POST['hdnSerialDea']);
	} else {
		if ($_POST['hdnSerialCus']) {
			$invoice->setSerial_cus($_POST['hdnSerialCus']);
		} else {
			$invoice->setSerial_cus($serial_cus);
		}
	}
	$invoice->setSerial_dbm($serial_dbm);
	$invoice->setSubtotal_inv($_POST['txtSubTotal']);
	$invoice->setTotal_inv($_POST['txtTotal']);
	$invoice->setPayPhone_id($_POST['txtSalesOrder']);

	$namesTax = $_POST['hdnNameTax'];
	$percentagesTax = $_POST['hdnPercentageTax'];
	if ($namesTax) {
		foreach ($namesTax as $k => $n) {
			$misc_taxes[$k]['tax_name'] = $n;
			$misc_taxes[$k]['tax_percentage'] = $percentagesTax[$k];
		}
		$invoice->setApplied_taxes_inv(serialize($misc_taxes));
	}
//        Debug::print_r($invoice);die();
	$serial_inv = $invoice->insert();
	if (!$serial_inv) {
		$error = 7;
	}
}

//update sales setting serial invoice 
if ($error == 1) {
	$salesIds = $_POST['hdnSaleId'];
	foreach ($salesIds as $s) {
		$sale = New Sales($db, $s);
		if (!$sale->updateInvoice($serial_inv)) {
			$error = 9;
			break;
		}
	}
}

//sending mail to customer
if ($error == 1 && $flag == 1) {
	if ($_POST['bill_to_radio'] == 'CUSTOMER' && $_POST['txtMail'] != '') {
		if (!GlobalFunctions::sendMail($misc, 'newClient')) { //Sending the client his login info.
			$error = 10;
		}
	}
}

http_redirect('modules/invoice/fChooseSales/' . $error . '/' . $serial_inv);
?>