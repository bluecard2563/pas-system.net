<?php 
/*
File: pNewLanguage.php
Author: Santiago Benítez
Creation Date: 20/01/2010 12:35
Last Modified: 20/01/2010 12:35
Modified By: Santiago Benítez
*/
$language=new Language($db);
$language->setName_lang($_POST['txtNameLanguage']);
$language->setCode_lang($_POST['txtCodeLanguage']);
if($language->insert()){
	$error=1;
}else{
	$error=2;
}

http_redirect('modules/language/fNewLanguage/'.$error);
?>