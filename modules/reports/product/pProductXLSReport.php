<?php
/*
File: pProductXLSReport
Author: David Bergmann
Creation Date: 27/05/2010
Last Modified:
Modified By:
*/

$productByCountry = new ProductByCountry($db);
$product_list = $productByCountry->getProductsByCountry($_POST[selCountry], $_SESSION['serial_lang'],$_POST['selStatus']);

if(is_array($product_list)) {
    foreach ($product_list as &$pl) {
        $benefit_by_product = new BenefitsByProduct($db);
        $aux = $benefit_by_product->getBenefitsByProduct($pl['serial_pro'], $_SESSION['serial_lang']);
        $pl['benefits'] = $aux;
    }
//1.38
    $report;
    foreach($product_list as &$pl) {
        $report.="<table>
            <tr>
                <td><b>Nombre:</b></td>
                <td>".$pl['name_pbl']."</td>
            </tr>
            <tr>
                <td><b>Descripci&oacute;n:</b></td>
                <td>".$pl['description_pbl']."</td>
            </tr>
            <tr>
                <td><b>Caracter&iacute;sticas:</b></td>
            </tr>
        </table>";

        $report.="<table border=1>
            <tr>
                <td><b>M&aacute;ximo de Acompa&ntilde;antes</b></td>
                <td><b>Viajes por per&iacute;odo</b></td>
                <td><b>Menores Gratis</b></td>
                <td><b>Mayores Gratis</b></td>
                <td><b>Precio para Familiares diferente</b></td>
                <td><b>Precio para C&oacute;nyuje diferente</b></td>
                <td><b>V&aacute;lida dentro del pa&iacute;s de emisi&oacute;n</b></td>
                <td><b>V&aacute;lida dentro del pa&iacute;s de residencia</b></td>
                <td><b>Masivo</b></td>
                <td><b>Pa&iacute;ses Schengen</b></td>
                <td><b>Adulto Mayor</b></td>
                <td><b>Tiene Servicios</b></td>
            </tr>
            <tr>
                <td>".$pl['max_extras_pro']."</td>
                <td>".$pl['flights_pro']."</td>
                <td>".$pl['children_pro']."</td>
                <td>".$pl['adults_pro']."</td>
                <td>".$global_yes_no[$pl['relative_pro']]."</td>
                <td>".$global_yes_no[$pl['spouse_pro']]."</td>
                <td>".$global_yes_no[$pl['emission_country_pro']]."</td>
                <td>".$global_yes_no[$pl['residence_country_pro']]."</td>
                <td>".$global_yes_no[$pl['masive_pro']]."</td>
                <td>".$global_yes_no[$pl['schengen_pro']]."</td>
                <td>".$global_yes_no[$pl['senior_pro']]."</td>
                <td>".$global_yes_no[$pl['has_services_pro']]."</td>
            </tr>
        </table>";

        $report.="<table>
            <tr>
                <td><b>Beneficios:</b></td>
            </tr>
            <tr>
        </table>";

        $report.="<table border=1>
            <tr>
                <td><b>Nombre</b></td>
                <td><b>Monto de cobertura</b></td>
                <td><b>Restricci&oacute;n</b></td>
            </tr>";
        foreach ($pl['benefits'] as $b){
            $report.="<tr><td>".$b['description_bbl']."</td>";
            if($b['price_bxp']>0) {
                $report.="<td>".$b['price_bxp']."</td>";
            } else {
                $report.="<td>N/A</td>";
            }
            $report.="<td>".$b['restriction_price_bxp']."</td></tr>";
        }
        $report.="</table>";
        $report.="<table><tr>&nbsp;</tr></table>";
    }
}
$country = new Country($db,$_POST[selCountry]);
$country->getData();

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Productos en ".$country->getName_cou().".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $report;
?>