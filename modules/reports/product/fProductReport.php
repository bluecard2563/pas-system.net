<?php
/*
File: fProductReport
Author: David Bergmann
Creation Date: 27/05/2010
Last Modified:
Modified By:
*/

$zone = new Zone($db);
$nameZoneList=$zone->getZones();

$product_by_country = new ProductByCountry($db);
$statusList = $product_by_country->getStatusList();

$smarty->register('nameZoneList,statusList,global_status,global_yes_no');
$smarty->display();
?>
