<?php
/**
 * file: fProductCaractheristics
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2012
 * Modification Date:  17/05/2012
 * Modified By: Patricio Astudillo
 */

	$product = new Product($db);
	$product_list = $product->getProducts('es');

	$smarty->register('product_list');
	$smarty->display();
?>
