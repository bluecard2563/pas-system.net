<?php
/**
 * File: pPricesByProductXLS.php
 * Author: Ing. Patricio Astudillo
 * Creation Date: N/A
 * Last Modified: 29/05/2012
 * Modified By: Ing. Patricio Astudillo
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');

	Request::setInteger('selZone');
	Request::setInteger('selCountry');
	Request::setInteger('selProduct');
	
	/*** GETTING NAMES ***/
	$zone = new Zone($db, $selZone);
	$zone->getData();
	$name_zon = $zone->getName_zon();//GETTING ZONE NAME
	$country = new Country($db, $selCountry);
	$country->getData();
	$name_cou = $country->getName_cou();//GETTING COUNTRY NAME
	
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$header = '<table>
					<tr><td colspan=6>'.$date.'</td></tr>
					<tr></tr>
					<tr><th></th><th colspan=5><b>Reporte de Precios por Producto</b></th></tr>
					 <tr></tr>
			  </table>';
	
	$filters = "<table>
					<tr>
						<td align='right'><b>Zona: </b></td>
						<td align='left'>".$name_zon."</td>
					</tr>
					<tr>
						<td align='right'><b>Pa&iacute;s:</b></td>
						<td align='left'>".$name_cou."</td>
					</tr>
					<tr></tr>
				</table>";
	
	/*** DATA RECOVERY PROCESS ***/
	$products_list = ProductByCountry::getProductsPricesByCountry($db, $selCountry, $_SESSION['language'], $selProduct);

	if($products_list){
		$pbpbc = new PriceByProductByCountry($db);
		
		$table = "<table>";		
		
		foreach($products_list as $p):
			$prices_list = $pbpbc -> getPricesByProductByCountry($p['serial_pro'], $selCountry);
			if(!$prices_list):
				$prices_list = $pbpbc -> getPricesByProductByCountry($p['serial_pro'], '1');
			endif;
			
			$table .= "<tr>
						<td><b>Producto: </b></td>
						<td colspan='3'>{$p['name_pbl']}</td>
					</tr>
					<tr>
						<td><b>D&iacute;as</b></td>
						<td><b>Costo</b></td>
						<td><b>Precio</b></td>
						<td><b>Referencia (M&iacute;n)</b></td>
						<td><b>Precio (M&aacute;sx)</b></td>
					</tr>";
			
			foreach($prices_list as $pr):
				$table .= "<tr>
							<td>{$pr['duration_ppc']}</td>
							<td>{$pr['cost_ppc']}</td>
							<td>{$pr['price_ppc']}</td>
							<td>{$pr['min_ppc']}</td>
							<td>{$pr['max_ppc']}</td>
						 </tr>";
			endforeach;			
			$table .= "<tr><td colspan='4'>&nbsp;</td></tr>";
		endforeach;
		
		$table .= "</table>";
	}else{
		$table = "No existen productos para el pa&iacute;s seleccionado";
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=ListadoPrecios_".$country->getCode_cou().".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $header;
	echo $filters;
	echo $table;
?>