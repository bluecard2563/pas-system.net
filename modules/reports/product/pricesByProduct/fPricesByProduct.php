<?php
/*
File: fPricesByProduct.php
Author: Gabriela Guerrero
Creation Date: 09/07/2010
Modified By:
Last Modified:
*/
Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getZonesPricesByProduct($_SESSION['countryList'],$_SESSION['serial_lang']);

$smarty -> register('zonesList');
$smarty->display();
?>