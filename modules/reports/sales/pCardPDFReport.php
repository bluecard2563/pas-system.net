<?php
/*
File: pCardPDFReport
Author: David Bergmann
Creation Date: 31/05/2010
Last Modified: 24/06/2010
Modified By: David Bergmann
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE TARJETAS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$sale_status = $_POST['selStatus'];
if(is_array($sale_status)){
	foreach($sale_status as &$s){
		$s = "'".$s."'";
	}
	$sale_status = implode (',', $sale_status);
}

$cardList = Sales::getCardReport($db, $_POST['selCountry'],$_POST['rdoDate'],$_POST['txtBeginDate'],$_POST['txtEndDate'],
                                 $_POST['selCity'],$_POST['selManager'],$_POST['selComissionist'], $_POST['selDealer'],$_POST['selBranch'],$_POST['selProduct'],
                                 $_POST['selType'], $sale_status,$_POST['selOperator'],$_POST['txtAmount'], $_POST['selStockType'],$_POST['selOrderBy']);

$cardAmount = 0;
$totalAmount = 0;
if(is_array($cardList)) {
	$cardAmount = count($cardList);
	foreach ($cardList as $c) {
		$totalAmount +=$c['col8'];
	}
}

$pdf->ezText('<b>'.utf8_decode("Número de tarjetas: ").'</b>'.$cardAmount, 10, array('justification'=>'left'));
$pdf->ezSetDy(-3);
$pdf->ezText('<b>'.utf8_decode("Total de las ventas: ").'</b>'.number_format($totalAmount, 2, '.', ','), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

if(is_array($cardList)) {
    $titles=array('col1'=>utf8_decode('<b># Tarjeta</b>'),
                  'col2'=>  html_entity_decode('<b>Fec. Emisi&oacute;n</b>'),
                  'col3'=>utf8_decode('<b>Cliente</b>'),
                  'col4'=>utf8_decode('<b>Producto</b>'),
                  'col5'=>utf8_decode('<b>Desde</b>'),
                  'col6'=>utf8_decode('<b>Hasta</b>'),
                  'col7'=>utf8_decode('<b>Tiempo</b>'),
                  'col8'=>utf8_decode('<b>Precio</b>'),
                  'col9'=>utf8_decode('<b>Estado</b>'),
                  'col10'=>utf8_decode('<b>Comercializador</b>'),
                  'col11'=>utf8_decode('<b>Counter</b>'),
				  'col12'=>utf8_decode('<b>Venta</b>'));

    foreach ($cardList as &$cl) {
    	if($cl['col9'] != 'FREE'){
    		$cl['col9'] = $global_salesStatus[$cl['col9']];
    	}
    }
    $pdf->ezTable($cardList,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>60),
                                        'col2'=>array('justification'=>'center','width'=>60),
                                        'col3'=>array('justification'=>'center','width'=>100),
                                        'col4'=>array('justification'=>'center','width'=>60),
                                        'col5'=>array('justification'=>'center','width'=>60),
                                        'col6'=>array('justification'=>'center','width'=>60),
                                        'col7'=>array('justification'=>'center','width'=>50),
                                        'col8'=>array('justification'=>'center','width'=>50),
                                        'col9'=>array('justification'=>'center','width'=>60),
                                        'col10'=>array('justification'=>'center','width'=>100),
                                        'col11'=>array('justification'=>'center','width'=>100))));
        $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();

?>