<?php
/**
 * File:  pGrossSalesReportPDF
 * Author: Patricio Astudillo
 * Creation Date: 25-abr-2011, 9:16:23
 * Description:
 * Last Modified: 25-abr-2011, 9:16:23
 * Modified by:
 */

	include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	
	/* REQUIRED VALUES */
	Request::setInteger('selCountry');
	Request::setString('rdoDate');
	Request::setString('selMonth');
	Request::setString('selYear');
	Request::setString('selOrderBy');
	$date_of_report = $selMonth.'/'.$selYear;

	$name_cou = new Country($db, $selCountry);
	$name_cou -> getData();
	$name_cou = $name_cou -> getName_cou();
	/* REQUIRED VALUES */

	/* OPTIONAL VALUES */
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selComissionist');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setString('selStockType');
	Request::setString('selOperator');
	Request::setFloat('txtAmount');
	$product_list = $_POST['selProduct'];
	
	if(is_array($product_list))	$product_list = implode (',', $product_list);
	
	$sale_type = $_POST['selType'];
	if(is_array($sale_type)){
		foreach($sale_type as &$s){
			$s = "'".$s."'";
		}
		$sale_type = implode (',', $sale_type);
	}

	$status_sal = $_POST['selStatus'];
	if(is_array($status_sal)){
		foreach($status_sal as &$st){
			$st = "'".$st."'";
		}
		$status_sal = implode (',', $status_sal);
	}

	if($txtAmount){
		$amount_clause = $selOperator.' '.$txtAmount;
	}
	/* OPTIONAL VALUES */

	$gross_sales = Sales::getGrossSalesForReport($db, $selCountry, $rdoDate, $date_of_report, $selOrderBy,
						$selCity, $selManager, $selComissionist, $selDealer, $selBranch, $selStockType, $sale_type,
						$product_list, $status_sal, $amount_clause);

	if($gross_sales){
		$processed_array = array();
		$refunded_array = array();
		$void_array = array();
		$net_total = 0;
		$total_refund = 0;
		$total_void = 0;
		$total_processed = 0;
		$number_of_cards = 0;
		$number_of_cards_refund_void=0;

		foreach($gross_sales as $g){
			$g['status_sal'] = $global_salesStatus[$g['status_sal']];
			$g['stock_type_sal'] = $global_salesStockType[$g['stock_type_sal']];


			switch($g['register_type']){
				case 'PROCESSED':
					array_push($processed_array, $g);
					$total_processed += $g['total_sal'];
					$number_of_cards +=1;
					break;
				case 'REFUND':
					array_push($refunded_array, $g);
					$total_refund += $g['total_sal'];
					$number_of_cards_refund_void+=1;
					break;
				case 'VOID':
					array_push($void_array, $g);
					$total_void += $g['total_sal'];
					$number_of_cards_refund_void+=1;
					break;
			}
		}

		$net_total = $total_processed - $total_refund - $total_void;
	}

	/*********************************** WRITING THE PDF *****************************************/
	$pdf->ezText('<b>REPORTE DE VENTAS BRUTAS</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);

	//DEFAULT TITLES
	$headerDesc = array();
	array_push($headerDesc,array(	'col1'=> utf8_decode('<b>Mes:</b>'),
									'col2'=> $global_weekMonthsTwoDigits[$selMonth],
									'col3'=> html_entity_decode('<b>A&ntilde;o:</b>'),
									'col4'=> $selYear
	));

	$arrayCountry=array('col1'=> html_entity_decode('<b>Pa&iacute;s:</b>'),
						'col2'=> $name_cou);

			//TITLES BY FILTERS
	if($selManager || $selCity){
		$name_mbc = ManagerbyCountry::getManagerByCountryName($db, $selManager);
		if(!$name_mbc) $name_mbc = 'Todos';

		$city = new City($db, $selCity);
		$city -> getData();
		$name_cit = $city -> getName_cit();
		if(!$name_cit) $name_cit = 'Todos';

			$arrayCountry['col3']= '<b>Representante:</b>';
			$arrayCountry['col4']= $name_mbc;

			array_push($headerDesc,$arrayCountry);

			array_push($headerDesc,array('col1'=> '<b>Ciudad:</b>',
										'col2'=> $name_cit
			));
	}else{
		array_push($headerDesc,$arrayCountry);
	}
									
	array_push($headerDesc,array(	'col1'=> utf8_decode('<b>Total de las Ventas:</b>'),
									'col2'=> '$ '.  number_format($total_processed, '2'),
									'col3'=> '<b>Total de Anulaciones:</b>',
									'col4'=> '$ '.  number_format($total_void, '2')
	));
	array_push($headerDesc,array(	'col1'=> '<b>Total de Reembolsos:</b>',
									'col2'=> '$ '.  number_format($total_refund, '2'),
									'col3'=> '<b>Total Ventas:</b>',
									'col4'=> '$ '.  number_format($net_total, '2')
	));
	array_push($headerDesc,array(	'col1'=> html_entity_decode('<b>N&uacute;mero de Tarjetas Vendidas:</b>'),
									'col2'=> $number_of_cards,
									'col3'=> html_entity_decode('<b>N&uacute;mero de Tarjetas Reembosadas/Anuladas:</b>'),
									'col4'=> $number_of_cards_refund_void
	));

	if($selComissionist || $selDealer){
		$name_ubd = UserByDealer::getUserByDealerName($db, $selComissionist);
		if(!$name_ubd) $name_ubd = 'Todos';

		$dealer = new Dealer($db, $selDealer);
		$dealer -> getData();
		$name_dea = $dealer -> getName_dea();
		if(!$name_dea) $name_dea = 'Todos';

		array_push($headerDesc,array(	'col1'=> '<b>Responsable:</b>',
										'col2'=> $name_ubd,
										'col3'=> '<b>Comercializador:</b>',
										'col4'=> $name_dea
		));
	}

	if($selBranch || $selStockType){
		$dealer = new Dealer($db, $selBranch);
		$dealer -> getData();
		$name_bra = $dealer -> getName_dea();
		if(!$name_bra) $name_bra = 'Todos';

		array_push($headerDesc,array(	'col1'=> '<b>Sucursal:</b>',
										'col2'=> $name_bra,
										'col3'=> '<b>Stock:</b>',
										'col4'=> ucwords($selStockType)
		));
	}

	if($amount_clause){
		array_push($headerDesc,array(	'col1'=> '<b>Monto:</b>',
										'col2'=> $amount_clause
		));
	}

	$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),'',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'325',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150),
					  	'col3'=>array('justification'=>'left','width'=>120),
					  	'col4'=>array('justification'=>'left','width'=>150)
			  )));
	$pdf->ezSetDy(-10);

	if($gross_sales){
		$titles = array('card_number_sal' => '<b># Tarjeta</b>',
						'emission_date_sal' => html_entity_decode('<b>Fec. Emisi&oacute;n</b>'),
						'name_cus' => '<b>Cliente</b>',
						'name_pbl' => '<b>Producto</b>',
						'begin_date_sal' => '<b>Desde</b>',
						'end_date_sal' => '<b>Hasta</b>',
						'days_sal' => '<b>Tiempo</b>',
						'total_sal' => '<b>Precio</b>',
						'status_sal' => '<b>Estado</b>',
						'name_dea' => '<b>Comercializador</b>',
						'name_cnt' => '<b>Counter</b>',
						'stock_type_sal' => '<b>Venta</b>');

		$parameters = array('card_number_sal' => array('justification'=>'center','width'=>50),
							'emission_date_sal' => array('justification'=>'center','width'=>57),
							'name_cus' => array('justification'=>'center','width'=>110),
							'name_pbl' => array('justification'=>'center','width'=>70),
							'begin_date_sal' => array('justification'=>'center','width'=>57),
							'end_date_sal' => array('justification'=>'center','width'=>57),
							'days_sal' => array('justification'=>'center','width'=>45),
							'total_sal' => array('justification'=>'center','width'=>60),
							'status_sal' => array('justification'=>'center','width'=>60),
							'name_dea' => array('justification'=>'center','width'=>100),
							'name_cnt' => array('justification'=>'center','width'=>80),
							'stock_type_sal' => array('justification'=>'center','width'=>50));
		
		$pdf->ezTable($processed_array,
					  $titles,
					  '<b>TARJETAS PROCESADAS</b>',
					  array('showHeadings'=>1,
							'shaded'=>1,
							'showLines'=>1,
							'xPos'=>'center',
							'fontSize' => 9,
							'titleFontSize' => 10,
							'cols' => $parameters
						  ));

		$pdf->ezSetDy(-10);

		if(count($void_array) > 0){
			$pdf->ezTable($void_array,
						  $titles,
						  '<b>TARJETAS ANULADAS</b>',
						  array('showHeadings'=>1,
								'shaded'=>1,
								'showLines'=>1,
								'xPos'=>'center',
								'fontSize' => 9,
								'titleFontSize' => 10,
								'cols' => $parameters
							  ));

			$pdf->ezSetDy(-10);
		}

		if(count($refunded_array) > 0){
			$pdf->ezTable($refunded_array,
						  $titles,
						  '<b>TARJETAS REEMBOLSADAS</b>',
						  array('showHeadings'=>1,
								'shaded'=>1,
								'showLines'=>1,
								'xPos'=>'center',
								'fontSize' => 9,
								'titleFontSize' => 10,
								'cols' => $parameters
							  ));

			$pdf->ezSetDy(-10);
		}
	} else {
		$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados.'), 10, array('justification' =>'center'));
	}

	$pdf->ezStream();
?>
