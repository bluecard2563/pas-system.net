<?php
/*
File: pCardManagerXLSReport

*/

set_time_limit(36000);
ini_set('memory_limit','512M');

$sale_status = $_POST['selStatus'];
if(is_array($sale_status)){
	foreach($sale_status as &$s){
		$s = "'".$s."'";
	}
	$sale_status = implode (',', $sale_status);
}

$cardList = Sales::getCardManagerReport($db, $_POST['selCountry'],$_POST['rdoDate'],$_POST['txtBeginDate'],$_POST['txtEndDate'],
                                 $_POST['selCity'],$_POST['selManager'],$_POST['selComissionist'], $_POST['selDealer'],$_POST['selBranch'],$_POST['selProduct'],
                                 $_POST['selType'], $sale_status,$_POST['selOperator'],$_POST['txtAmount'],$_POST['selStockType'],$_POST['selOrderBy']);

$cardAmount = 0;
$totalAmount = 0;
$netValue = 0;
if(is_array($cardList)) {
	$cardAmount = count($cardList);
	foreach ($cardList as $c) {
		$totalAmount +=$c['col8'];
	}
}


$table = "<table border='1'>
			<tr>
				<td>N&uacute;mero de tarjetas:</td>
				<td>$cardAmount</td>
			</tr>
			<tr>
				<td>Total de las ventas:</td>
				<td>".number_format($totalAmount,2,',','')."</td>
			</tr>
		  </table>";

if(is_array($cardList)) {
    $table.="<table border='1'>
			<tr>
				<th><b># Tarjeta</b></th>
				<th><b>Fec. Emisi&oacute;n</b></th>
				<th><b>Cliente</b></th>
				<th><b>No. Identificaci&oacute;n</b></th>
				<th><b>No. Asegurados</b></th>
				<th><b>Fecha de Nacimiento</b></th>
				<th><b>Edad</b></th>
				<th><b>Producto</b></th>
				<th><b>Desde</b></th>
				<th><b>Hasta</b></th>
				<th><b>Tiempo</b></th>
                                <th><b>Valor Facturado</b></th>
				<th><b>Estado</b></th>
				<th><b>Comercializador</b></th>
				<th><b>Counter</b></th>
				<th><b>Destino</b></th>
			</tr>";

    foreach ($cardList as $cl) {
        $total_discount = $cl['discount']+$cl['other_discount'];
        if ($total_discount==0){
            $netValue = $cl['col8'];
        }
        else{
            $netValue = $cl['col8'] - ($cl['col8'] * ($total_discount/100));
        }
        $table.="<tr>
                 <td>".$cl['col1']."</td>
                 <td>".$cl['col2']."</td>
                 <td>".$cl['col3']."</td>
		 <td>'".$cl['document_cus']."</td>
		 <td>'".$cl['total_pas']."</td>
		 <td>".$cl['birthdate_cus']."</td>
		 <td>".GlobalFunctions::getMyAge($cl['birthdate_cus'])."</td>
		 <td>".$cl['col4']."</td>
                 <td>".$cl['col5']."</td>
                 <td>".$cl['col6']."</td>
                 <td>".$cl['col7']."</td>   
                 <td>".number_format($netValue,2,',','')."</td>
                 <td>".$global_salesStatus[$cl['col9']]."</td>
                 <td>".$cl['col10']."</td>
                 <td>".$cl['col11']."</td>
				 <td>".$cl['name_country']."</td>
				</tr>";
    }
    $table.="</table>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Tarjetas.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $table;
?>
