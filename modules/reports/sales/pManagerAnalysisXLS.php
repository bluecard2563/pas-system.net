<?php

/*
  File: fManagerAnalysisXLS

 */

//get filters
$serial_zon = $_POST['selZone'];
$serial_cou = $_POST['selCountry'];
$serial_mbc = $_POST['selManager'];
$begin_date = $_POST['txtBeginDate'];
$end_date = $_POST['txtEndDate'];

//get values for the header
$zone = new Zone($db, $serial_zon);
$zone->getData();
$name_zon = $zone->getName_zon();

$country = new Country($db, $serial_cou);
$country->getData();
$name_cou = $country->getName_cou();

if ($serial_mbc) {
	$managerList = ManagerbyCountry::getManagersWithDealers($db, $serial_cou, $serial_mbc);
} else {
	$managerList = ManagerbyCountry::getManagersWithDealers($db, $serial_cou, 1); //if a manager is not selected in the filter form
}

if (is_array($managerList)) {
	
	$pos = 0;
	$total_sales_by_country = 0;
	$total_cost_by_country=0;
	$total_effective_commisions=0;
	$total_commisions = 0;        
	$total_by_user = 0;

        $total_void_by_country=0;
	$total_free_by_country=0;
	$total_refunded_by_country = 0;
        $total_efective_sale_by_country = 0;
        
	foreach ($managerList as $mbc) {
		$manager_info[$pos]['name'] = $mbc['name_man'];
		
		$sales_by_ubd = Sales::getInfoForManagerAnalysisReport($db, $serial_cou, $mbc['serial_mbc'], $begin_date, $end_date);
		$pos2 = 0;
		$total_effective_comission=0;
		$total_commisions = 0;        
		$total_by_user = 0;
		$total_sales_by_mbc = 0;
		$total_cost_by_mbc = 0;
		$total_commisions_by_mbc = 0;
		$total_by_user_by_mbc = 0;
		$total_gastos_administrativos_by_mbc = 0;
		$total_rentabilidad_by_mbc = 0;
		$total_void_by_mbc=0;
		$total_free_by_mbc=0;
		$total_refunded_by_mbc = 0;
                $total_efective_sale_by_mbc = 0;
                
                $user_by_dealer = array();
		if (is_array($sales_by_ubd)) {
			foreach ($sales_by_ubd as $sales) {
				if (is_array($sales)) {
					$void_free_refunded = $sales['void'] + $sales['free'] + $sales['refunded'];
					$total_commisions = ($sales['sales']-$void_free_refunded) * ($sales['effective_comission']/100);
					$total_by_user = ($sales['sales']-$void_free_refunded) - $sales['cost'] - $total_commisions;
					
                                        
                                        
					$gastos_administrativos = ($sales['sales']-$void_free_refunded) * 0.33;
					$rentabilidad = $total_by_user - $gastos_administrativos - $void_free_refunded ;
					$porcentaje_rentabilidad = (($rentabilidad * 100 )/ ($sales['sales']- $void_free_refunded));
					
					//aux array for each user by dealer
					$user_by_dealer [$pos2]['name_usr'] = $sales['name_usr'];
					$user_by_dealer [$pos2]['sales'] = number_format($sales['sales'], 2, ',', '');
					$user_by_dealer [$pos2]['cost'] = number_format($sales['cost'], 2, ',', '');
					$user_by_dealer [$pos2]['effective_comission'] = number_format($sales['effective_comission'], 2, ',', '');
					$user_by_dealer [$pos2]['total_commisions'] = number_format($total_commisions, 2, ',', '');
					$user_by_dealer [$pos2]['total_by_user'] = number_format($total_by_user, 2, ',', '');
					
					$user_by_dealer [$pos2]['gastos_administrativos'] = number_format($gastos_administrativos, 2, ',', '');
					$user_by_dealer [$pos2]['rentabilidad'] = number_format($rentabilidad, 2, ',', '');
					$user_by_dealer [$pos2]['porcentaje_rentabilidad'] = number_format($porcentaje_rentabilidad, 2, ',', '');
                                        
                                        $user_by_dealer [$pos2]['void'] = number_format($sales['void'], 2, ',', '');
					$user_by_dealer [$pos2]['free'] = number_format($sales['free'], 2, ',', '');
					$user_by_dealer [$pos2]['refunded'] = number_format($sales['refunded'], 2, ',', '');
                                        if($sales['sales'] > 0){
						
						$user_by_dealer [$pos2]['efective_sale'] = number_format($sales['sales'] - $void_free_refunded, 2,',','');
					}
					$pos2++;
					//calulate the total values for the manager
					$total_sales_by_mbc += $sales['sales'];
					$total_cost_by_mbc += $sales['cost'];
					$total_commisions_by_mbc += $total_commisions;
					$total_by_user_by_mbc += $total_by_user;
					$total_gastos_administrativos_by_mbc += $gastos_administrativos;
					$total_rentabilidad_by_mbc += $rentabilidad;
					
                                        $total_void_by_mbc += $sales['void'];
					$total_free_by_mbc += $sales['free'];
					$total_refunded_by_mbc += $sales['refunded'];
                                        $total_efective_sale_by_mbc += number_format(( $sales['sales'] - $void_free_refunded),2,',','');
					
				}
			}
			$total_effective_comission_by_mbc = ($total_commisions_by_mbc*100)/$total_efective_sale_by_mbc;
			$total_porcentaje_rentabilidad_by_mbc = ($total_rentabilidad_by_mbc*100)/$total_efective_sale_by_mbc;
			//calculates total percentage
			$total_percentage_by_mbc = number_format((($total_void_by_mbc + $total_free_by_mbc + $total_refunded_by_mbc) * 100) / $total_efective_sale_by_mbc, 2, ',', '');
			
			//calculte the total of the country
			$total_sales_by_country += $total_sales_by_mbc;
			$total_cost_by_country += $total_cost_by_mbc;
			$total_commisions_by_country += $total_commisions_by_mbc;
			$total_by_user_by_country += $total_by_user_by_mbc;
			$total_gastos_administrativos_by_country += $total_gastos_administrativos_by_mbc;
			$total_rentabilidad_by_country += $total_rentabilidad_by_mbc;
                        
                        $total_void_by_country += $total_void_by_mbc;
			$total_free_by_country += $total_free_by_mbc;
			$total_refunded_by_country += $total_refunded_by_mbc;
			$total_efective_sale_by_country += $total_efective_sale_by_mbc;
                        
			//generates the final array with the info of all managers
			$manager_info[$pos]['sales_info'] = $user_by_dealer;
			$manager_info[$pos]['totals_by_mbc'][0]['name'] = "<b>Total" . $mbc['name_man'] . "</b>";
			$manager_info[$pos]['totals_by_mbc'][0]['total_sales_by_mbc'] = number_format($total_sales_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_cost_by_mbc'] = number_format($total_cost_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_effective_comission_by_mbc'] = number_format($total_effective_comission_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_commisions_by_mbc'] = number_format($total_commisions_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_by_user_by_mbc'] = number_format($total_by_user_by_mbc, 2, ',', '');
			
			$manager_info[$pos]['totals_by_mbc'][0]['total_gastos_administrativos_by_mbc'] = number_format($total_gastos_administrativos_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_rentabilidad_by_mbc'] = number_format($total_rentabilidad_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_porcentaje_rentabilidad_by_mbc'] = number_format($total_porcentaje_rentabilidad_by_mbc, 2, ',', '');

                        
                        $manager_info[$pos]['totals_by_mbc'][0]['total_void_by_mbc'] = number_format($total_void_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_free_by_mbc'] = number_format($total_free_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_refunded_by_mbc'] = number_format($total_refunded_by_mbc, 2, ',', '');
                        $manager_info[$pos]['totals_by_mbc'][0]['total_efective_sale'] = number_format($total_efective_sale_by_mbc, 2, ',', '');
                        
			$pos++;
		}
			
	}
	
	if ($total_sales_by_country) {
		$total_effective_comission_by_country = ($total_commisions_by_country*100)/$total_efective_sale_by_country;
		$total_porcentaje_rentabilidad_by_country = ($total_rentabilidad_by_country*100)/$total_efective_sale_by_country;
	
		//calculates total percentage for the country
		$total_percentage_by_country = number_format((($total_void_by_country + $total_free_by_country + $total_refunded_by_country) * 100) / $total_efective_sale_by_country, 2, ',', '');
		//adding the country total values to the array
		$manager_info[$pos]['total_country'][0]['name'] = "<b>Total " . $name_cou . "</b>";
		$manager_info[$pos]['total_country'][0]['total_sales_by_country'] = number_format($total_sales_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_cost_by_country'] = number_format($total_cost_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_effective_comission_by_country'] = number_format($total_effective_comission_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_commisions_by_country'] = number_format($total_commisions_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_by_user_by_country'] = number_format($total_by_user_by_country, 2, ',', '');
		
		$manager_info[$pos]['total_country'][0]['total_gastos_administrativos_by_country'] = number_format($total_gastos_administrativos_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_rentabilidad_by_country'] = number_format($total_rentabilidad_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_porcentaje_rentabilidad_by_country'] = number_format($total_porcentaje_rentabilidad_by_country, 2, ',', '');
	
                $manager_info[$pos]['total_country'][0]['total_void_by_country'] = number_format($total_void_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_free_by_country'] = number_format($total_free_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_refunded_by_country'] = number_format($total_refunded_by_country, 2, ',', '');
                $manager_info[$pos]['total_country'][0]['total_efective_sale_by_country'] = number_format($total_efective_sale_by_country, 2, ',', '');
        }
}

		/*	 * *EXCEL REPORT** */
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=Analisis_Gerencial_Responsable.xls");
		header("Pragma: no-cache");
		header("Expires: 0");

if ($total_sales_by_country) {
	if (is_array($manager_info)) {
		
		$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
		$title = "<table  border='1'>
						<tr><td colspan=6>$date</td></tr>
						<tr></tr>
						<tr><td colspan=6 align='center'><b>Reporte de An&aacute;lisis Gerencial por Responsable</b></td></tr>
						 <tr></tr>
				  </table>";
		echo $title;

		//get manager name for header
		if ($serial_mbc) {
			$manager_by_country = New ManagerbyCountry($db, $serial_mbc);
			$manager_by_country->getData();
			$manager = new Manager($db, $manager_by_country->getSerial_man());
			$manager->getData();
			$name_man = $manager->getName_man();
		} else {
			$name_man = "Todos";
		}
		//HEADER
		$parameters = "<table border='1'>
							<tr>
								<td align='right'><b>Zona: </b></td><td align='left'>" . $name_zon . "</td>
								<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
							</tr>
							<tr>
								<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
							</tr>
							<tr>
								<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $begin_date . "</td>
								<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $end_date . "</td>
							</tr>
						
						</table><br>";
		echo $parameters;
		//DATA
		$auxArr = array_keys($manager_info);
		$last = end($auxArr);

		foreach ($manager_info as $key1 => $info) {
			if (is_array($info)) {
				if ($key1 != $last) {//header of the table
					$table = "<br/><table border='1'>
									<tr>
										<td align='center'><b>" . $info['name'] . "</b></td>
										<td align='center'><b>Ventas</b></td>
                                                                                
                                                                                <td align='center'><b>Anuladas</b></td>
										<td align='center'><b>Free</b></td>
										<td align='center'><b>Reembolsos</b></td>
                                                                                <td align='center'><b>Venta Efectiva</b></td>
                                                                                
										<td align='center'><b>Costos</b></td>
										<td align='center'><b>Comision Dolares</b></td>
										<td align='center'><b>% Comision</b></td>
										<td align='center'><b>Total</b></td>
										<td align='center'><b>Gastos Administrativos</b></td>
										<td align='center'><b>Rentabilidad</b></td>
										<td align='center'><b>% Rentabilidad</b></td>
									</tr>";

					foreach ($info as $key2 => $i) {
						if ($key2 == 'sales_info') {
							foreach ($i as $ubd) {//data rows
								$table.="<tr>
											<td align='center'>" . $ubd['name_usr'] . "</td>
											<td align='center'>" . $ubd['sales'] . "</td>
                                                                                            
                                                                                        <td align='center'>" . $ubd['void'] . "</td>
											<td align='center'>" . $ubd['free'] . "</td>
											<td align='center'>" . $ubd['refunded'] . "</td>
                                                                                        <td align='center'>" . $ubd['efective_sale'] . "</td>

											<td align='center'>" . $ubd['cost'] . "</td>
											<td align='center'>" . $ubd['total_commisions'] . "</td>	
											<td align='center'>" . $ubd['effective_comission'] . "</td>
											<td align='center'>" . $ubd['total_by_user'] . "</td>
											<td align='center'>" . $ubd['gastos_administrativos'] . "</td>
											<td align='center'>" . $ubd['rentabilidad'] . "</td>
											<td align='center'>" . $ubd['porcentaje_rentabilidad'] . "</td>
										</tr>";
							}
						}
						if ($key2 == 'totals_by_mbc') {
							$table.="<tr>
											<td align='center'><b>" . $i['0']['name'] . "<b/></td>
											<td align='center'><b>" . $i['0']['total_sales_by_mbc'] . "</b></td>
                                                                                            
                                                                                        <td align='center'><b>" . $i['0']['total_void_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_free_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_refunded_by_mbc'] . "</b></td>
                                                                                        <td align='center'><b>" . $i['0']['total_efective_sale'] . "</b></td>

											<td align='center'><b>" . $i['0']['total_cost_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_commisions_by_mbc'] . "</b></td>	
											<td align='center'><b>" . $i['0']['total_effective_comission_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_by_user_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_gastos_administrativos_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_rentabilidad_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_porcentaje_rentabilidad_by_mbc'] . "</b></td>
										</tr>";
						}
					}
					$table.="</table><br />";
					echo $table;
				} else {
					if (!$serial_mbc) {
						////totals for the country
						foreach ($info['total_country'] as $key3 => $totals_country) {
							$total_country_table = "<table border='1'>
										<tr></tr>
										<tr>
											<td align='center'><b>" . $totals_country['name'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_sales_by_country'] . "</b></td>
                                                                                        
                                                                                        <td align='center'><b>" . $totals_country['total_void_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_free_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_refunded_by_country'] . "</b></td>
                                                                                        <td align='center'><b>" . $totals_country['total_efective_sale_by_country'] . "</td>

											<td align='center'><b>" . $totals_country['total_cost_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_commisions_by_country'] . "</b></td>	
											<td align='center'><b>" . $totals_country['total_effective_comission_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_by_user_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_gastos_administrativos_by_country'] . "</td>
											<td align='center'><b>" . $totals_country['total_rentabilidad_by_country'] . "</td>
											<td align='center'><b>" . $totals_country['total_porcentaje_rentabilidad_by_country'] . "</td>
										</tr>
									  </table>";
						}
						echo $total_country_table;
						
					}
				}
			}
		}
		/*	 * *END EXCEL REPORT** */
	}
}else {
		echo '<i>No existen registros para los parametros seleccionados.</i>';
	}
?>