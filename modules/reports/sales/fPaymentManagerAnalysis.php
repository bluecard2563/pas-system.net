<?php

/**
 * Name: fPaymentManagerAnalysis
 * Created by: pastudillo
 * Created on: 24-may-2017
 * Description: 
 * Modified by: pastudillo
 * Modified on: 24-may-2017
 */

Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getAllowedZones($_SESSION['countryList']);
$today=date('d/m/Y');
$first_day='01/01/2009';

$smarty -> register('zonesList,error,today,first_day');
$smarty->display();