<?php
/*
 * File: fViewFreeSalesReport.php
 * Author: Patricio Astudillo
 * Creation Date: 31/05/2010, 02:30:21 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 31/05/2010, 02:30:21 PM
 */

$zone=new Zone($db);
$zoneList=$zone->getZones();

$smarty->register('zoneList');
$smarty->display();
?>
