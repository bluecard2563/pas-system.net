<?php

if(isset($_POST['selZone']) && isset($_POST['selCountry'])){
   
	$sale = new Sales($db);
	$salesRaw = array();

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$date_from = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
	$date_to = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';
	$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
     
    $salesRaw = $sale->getFreeSalesByZone($serial_zon, $serial_cou, $_SESSION['serial_mbc'],$_SESSION['serial_dea'],
												$serial_man,$serial_cit,$serial_dea,$serial_bra,$date_from,$date_to,
												$status);
        //debug::print_r($salesRaw);die();
        if(!$salesRaw){
            http_redirect('modules/reports/sales/fFreeCardReport');
        }
            
			header('Content-type: application/vnd.ms-excel');
			header("Content-Disposition: attachment; filename=Ventas_Free.xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
			$title="<table>
						<tr><td colspan=6>".$date."</td></tr><tr></tr>
						<tr>
							<th></th>
							<th></th>
							<th><b>Reporte de Ventas Free</b></th>
						</tr>
						<tr></tr>
					</table>";
			echo $title;

			$statusCard = 'Todos'; $manager = 'Todos'; $city = 'Todos'; $dealer = 'Todos'; $branch = 'Todos'; $dateFrom = 'Todos'; $dateTo = 'Todos';
			if($status){
				if($status == 'ACTIVE'){
					$statusCard = 'Autorizadas';
				}
				else{
					if($status == 'DENIED'){
						$statusCard = 'Negadas';
					}
					else{
						$statusCard = 'Pendientes';
					}
				}
			}

			if($serial_man){
				$manager = $salesRaw[0]['name_man'];
			}
			if($serial_cit){
				$city = $salesRaw[0]['name_cit'];
			}
			if($serial_dea){
				$dealer = $salesRaw[0]['name_dea'];
			}
			if($serial_bra){
				$branch = $salesRaw[0]['name_bra'];
			}
			if($date_from && $date_to){
				$dateFrom = $date_from;
				$dateTo = $date_to;
			}

			$parameters = "<table>
								<tr><td  align='right'><b>Zona:</b></td><td  align='left'>".$salesRaw[0]['name_zon']."</td><td  align='right'><b>".utf8_decode(País).":</b></td><td  align='left'>".$salesRaw[0]['name_cou']."</td></tr>
								<tr><td  align='right'><b>Representante:</b></td><td  align='left'>".$manager."</td><td  align='right'><b>Ciudad:</b></td><td  align='left'>".$city."</td></tr>
								<tr><td  align='right'><b>Comercializador:</b></td><td  align='left'>".$dealer."</td><td  align='right'><b>Sucursal:</b></td><td  align='left'>".$branch."</td></tr>
								<tr><td  align='right'><b>Fecha Desde:</b></td><td  align='left'>".$date_from."</td><td  align='right'><b>Fecha Hasta:</b></td><td  align='left'>".$date_to."</td></tr>
								<tr><td  align='right'><b>Estado:</b></td><td  align='left'>".$statusCard."</td></tr>
								<tr></tr>
						  </table>";
			echo $parameters;

			$branch = '';
			$sales = "";
			$totalFeeSal=0;
			$totalCostSalesFree=0;
			$sizeSalesRow=count($salesRaw);                        
            foreach ($salesRaw as $key=> $sal) {

				$sLog=new SalesLog($db);
				$sLogArr=$sLog->getSalesLogBySale($sal['serial_sal']);
				$observations = unserialize($sLogArr[0]['description_slg']);

				$user=new User($db,$observations['global_info']['requester_usr'] );
				$user->getData();

				$observations['global_info']['requester_usr']=$user->getFirstname_usr()." ".$user->getLastname_usr();

				$user->setSerial_usr($observations['global_info']['authorizing_usr']);
				$user->getData();

				$observations['global_info']['authorizing_usr']=$user->getFirstname_usr()." ".$user->getLastname_usr();

				$totalFeeSal+=$sal['fee_sal'];
				$totalCostSalesFree+=$sal['total_cost_sal'];
                                
				if($sal['serial_bra'] != $branch){
					$branchHeader = "<table><tr><th></th>
										<th><b>Comercializador: </b>".$sal['name_dea']."</th>
										<th><b>Sucursal: </b>".$sal['name_bra']."</th>
									</tr></table>";
					echo $branchHeader;
					$sales = "<table border='1'><tr>".
								"<th><b>Fecha</b></th>".
								"<th><b># Tarjeta</b></th>".
								"<th><b>Responsable</b></th>".
								"<th><b>Persona que ".utf8_decode(Solicitó)."</b></th>".
								"<th><b>Persona que ".utf8_decode(Autorizó)."</b></th>".
								"<th><b>".utf8_decode(Días)."</b></th>".
								"<th><b>Tarifa</b></th>".
								"<th><b>Destino</b></th>".
								"<th><b>Producto</b></th>".
								"<th><b>Observaciones de Solicitud</b></th>".
								"<th><b>Observaciones de ".utf8_decode(Autorización)."</b></th>".
								"<th><b>Costo</b></th></tr>"
							;
				}
                                if($key+1<$sizeSalesRow){
				$sales .= "<tr>
							<th>".$sal['date_sal']."</th>
							<th>".$sal['card_number_sal']."</th>
							<th>".$sal['name_usr']."</th>
							<th>".$observations['global_info']['requester_usr']."</th>
							<th>".$observations['global_info']['authorizing_usr']."</th>
							<th>".$sal['days_sal']."</th>
							<th>".number_format($sal['fee_sal'], 2, ',', '')."</th>
							<th>".$sal['destination']."</th>
							<th>".$sal['name_pbl']."</th>
							<th>".$observations['global_info']['request_obs']."</th>
							<th>".$observations['global_info']['authorizing_obs']."</th>
							<th>".number_format($sal['total_cost_sal'], 2, ',', '')."</th>
						   </tr>";
                                }elseif($key+1==$sizeSalesRow){
 				$sales .= "<tr>
							<th><b>Total Ventas Free</b></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>".number_format($totalFeeSal, 2, ',', '')."</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>".number_format($totalCostSalesFree, 2, ',', '')."</th>
						   </tr>";                                   
                                    
                                }
				if($sal['serial_bra']!=$salesRaw[$key+1]['serial_bra']){
					$sales .= "</table>";
					echo $sales;
					$sales = "";
				}
				$branch = $sal['serial_bra'];
            }			
}else{
    http_redirect('modules/reports/sales/fFreeCardReport');
}


?>