<?php
/**
 * File:  pEffectiveGrossSalesReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 25-abr-2011, 9:16:29
 * Description:
 * Last Modified: 25-abr-2011, 9:16:29
 * Modified by:
 */

	/* REQUIRED VALUES */
	Request::setInteger('selCountry');
	Request::setString('rdoDate');
	Request::setString('selMonth');
	Request::setString('selYear');
	Request::setString('selOrderBy');
	$date_of_report = $selMonth.'/'.$selYear;

	$name_cou = new Country($db, $selCountry);
	$name_cou -> getData();
	$name_cou = $name_cou -> getName_cou();
	/* REQUIRED VALUES */

	/* OPTIONAL VALUES */
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selComissionist');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setString('selStockType');
	Request::setString('selOperator');
	Request::setFloat('txtAmount');
	$product_list = $_POST['selProduct'];
	
	if(is_array($product_list))	$product_list = implode (',', $product_list);

	$sale_type = $_POST['selType'];
	if(is_array($sale_type)){
		foreach($sale_type as &$s){
			$s = "'".$s."'";
		}
		$sale_type = implode (',', $sale_type);
	}

	$status_sal = $_POST['selStatus'];
	if(is_array($status_sal)){
		foreach($status_sal as &$st){
			$st = "'".$st."'";
		}
		$status_sal = implode (',', $status_sal);
	}

	if($txtAmount){
		$amount_clause = $selOperator.' '.$txtAmount;
	}
	/* OPTIONAL VALUES */

	$gross_sales = Sales::getGrossSalesForReport($db, $selCountry, $rdoDate, $date_of_report, $selOrderBy,
						$selCity, $selManager, $selComissionist, $selDealer, $selBranch, $selStockType, $sale_type,
						$product_list, $status_sal, $amount_clause, true);

	if($gross_sales){
		$processed_array = array();
		$refunded_array = array();
		$void_array = array();
		$net_total = 0;
		$total_refund = 0;
		$total_void = 0;
		$total_processed = 0;
		$number_of_cards = 0;
		$number_of_cards_refund_void=0;

		foreach($gross_sales as $g){
			$g['status_sal'] = $global_salesStatus[$g['status_sal']];
			$g['stock_type_sal'] = $global_salesStockType[$g['stock_type_sal']];


			switch($g['register_type']){
				case 'PROCESSED':
					array_push($processed_array, $g);
					$total_processed += $g['total_sal'];
					$number_of_cards +=1;
					break;
				case 'REFUND':
					array_push($refunded_array, $g);
					$total_refund += $g['total_sal'];
					$number_of_cards_refund_void+=1;
					break;
				case 'VOID':
					array_push($void_array, $g);
					$total_void += $g['total_sal'];
					$number_of_cards_refund_void+=1;
					break;
			}
		}

		$net_total = $total_processed - $total_refund - $total_void;
	}

	/*********************************** DISPLAYING THE XLS *****************************************/
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = '<table>
					<tr><td colspan=6>'.$date.'</td></tr>
					<tr></tr>
					<tr>
						<th></th>
						<th><b>Reporte de Ventas Brutas Efectivas</b></th>
					</tr>
					<tr></tr>
			  </table>';

	//APLIED FILTERS
	$filters = "<table>
					<tr>
						<td><b>Mes:</b></td>
						<td>{$global_weekMonthsTwoDigits[$selMonth]}</td>
						<td><b>A&ntilde;o:</b></td>
						<td>$selYear</td>
					</tr>
					<tr>
						<td><b>Pa&iacute;s:</b></td>
						<td>$name_cou</td>";

		//TITLES BY FILTERS
	if($selManager || $selCity){
		$name_mbc = ManagerbyCountry::getManagerByCountryName($db, $selManager);
		if(!$name_mbc) $name_mbc = 'Todos';

		$city = new City($db, $selCity);
		$city -> getData();
		$name_cit = $city -> getName_cit();
		if(!$name_cit) $name_cit = 'Todos';

		$filters .="
						<td><b>Representante:</b></td>
						<td>$name_mbc</td>
					</tr>

					<tr>
						<td><b>Ciudad:</b></td>
						<td>$name_cit</td>
					</tr>";
	}else{

		$filters .="</tr>";
	}


		$filters .="<tr>
						<td><b>Total de las Ventas:</b></td>
						<td>$ ".number_format($total_processed, '2', ',', '')."</td>
						<td><b>Total de Anulaciones:</b></td>
						<td>$ ".number_format($total_void, '2', ',', '')."</td>
					</tr>
					<tr>
						<td><b>Total de Reembolsos:</b></td>
						<td>$ ".number_format($total_refund, '2', ',', '')."</td>
						<td><b>Total de Ventas:</b></td>
						<td>$ ".number_format($net_total, '2', ',', '')."</td>
					</tr>
					<tr>
						<td><b>N&uacute;mero de Tarjetas Vendidas:</b></td>
						<td>$number_of_cards</td>
						<td><b>N&uacute;mero de Tarjetas Reembolsadas/Anuladas:</b></td>
						<td>$number_of_cards_refund_void</td>
					</tr>";


	if($selComissionist || $selDealer){
		$name_ubd = UserByDealer::getUserByDealerName($db, $selComissionist);
		if(!$name_ubd) $name_ubd = 'Todos';

		$dealer = new Dealer($db, $selDealer);
		$dealer -> getData();
		$name_dea = $dealer -> getName_dea();
		if(!$name_dea) $name_dea = 'Todos';

		$filters .="<tr>
						<td><b>Responsable:</b></td>
						<td>$name_ubd</td>
						<td><b>Comercializador:</b></td>
						<td>$name_dea</td>
					</tr>";
	}

	if($selBranch || $selStockType){
		$dealer = new Dealer($db, $selBranch);
		$dealer -> getData();
		$name_bra = $dealer -> getName_dea();
		if(!$name_bra) $name_bra = 'Todos';

		$filters .="<tr>
						<td><b>Sucursal:</b></td>
						<td>$name_bra</td>
						<td><b>Stock:</b></td>
						<td>".ucwords($selStockType)."</td>
					</tr>";
	}

	if($amount_clause){
		$filters .="<tr>
						<td><b>Monto:</b></td>
						<td>$amount_clause</td>
						<td colspan='2'>&nbsp;</td>
					</tr>";
	}

	$filters .= "</table><br />";

	if($gross_sales){
		$titles = "	<tr>
						<td><b># Tarjeta</b></td>
						<td><b>Fec. Emisi&oacute;n</b></td>
						<td><b>Cliente</b></td>
						<td><b>Producto</b></td>
						<td><b>Desde</b></td>
						<td><b>Hasta</b></td>
						<td><b>Tiempo</b></td>
						<td><b>Precio</b></td>
						<td><b>Estado</b></td>
						<td><b>Comercializador</b></td>
						<td><b>Counter</b></td>
						<td><b>Venta</b></td>
					</tr>";

		foreach($processed_array as $p){
			$processed .= "<tr>
								<td>{$p['card_number_sal']}</td>
								<td>{$p['emission_date_sal']}</td>
								<td>{$p['name_cus']}</td>
								<td>{$p['name_pbl']}</td>
								<td>{$p['begin_date_sal']}</td>
								<td>{$p['end_date_sal']}</td>
								<td>{$p['days_sal']}</td>
								<td>".number_format($p['total_sal'],2,',','')."</td>
								<td>{$p['status_sal']}</td>
								<td>{$p['name_dea']}</td>
								<td>{$p['name_cnt']}</td>
								<td>{$p['stock_type_sal']}</td>
							</tr>";
		}

		if(count($void_array) > 0){
			$void = "<table border='1'>
						<tr>
							<td colspan='12'><b>TARJETAS ANULADAS</b></td>
						</tr>
						$titles";
			foreach($void_array as $v){
				$void .= "<tr>
							<td>{$v['card_number_sal']}</td>
							<td>{$v['emission_date_sal']}</td>
							<td>{$v['name_cus']}</td>
							<td>{$v['name_pbl']}</td>
							<td>{$v['begin_date_sal']}</td>
							<td>{$v['end_date_sal']}</td>
							<td>{$v['days_sal']}</td>
							<td>{$v['total_sal']}</td>
							<td>{$v['status_sal']}</td>
							<td>{$v['name_dea']}</td>
							<td>{$v['name_cnt']}</td>
							<td>{$v['stock_type_sal']}</td>
						</tr>";
			}
			$void .= "</table><br />";
		}


		if(count($refunded_array) > 0){
			$refund = "<table border='1'>
							<tr>
								<td colspan='12'><b>TARJETAS REEMBOLSADAS</b></td>
							</tr>
							$titles";
			foreach($refunded_array as $r){
				$refund .= "<tr>
							<td>{$r['card_number_sal']}</td>
							<td>{$r['emission_date_sal']}</td>
							<td>{$r['name_cus']}</td>
							<td>{$r['name_pbl']}</td>
							<td>{$r['begin_date_sal']}</td>
							<td>{$r['end_date_sal']}</td>
							<td>{$r['days_sal']}</td>
							<td>{$r['total_sal']}</td>
							<td>{$r['status_sal']}</td>
							<td>{$r['name_dea']}</td>
							<td>{$r['name_cnt']}</td>
							<td>{$r['stock_type_sal']}</td>
						</tr>";
			}
			$refund .= "</table><br />";
		}

		$final_data = "<table border='1'>$titles $processed </table><br />";
		$final_data.= $void;
		$final_data.= $refund;

	} else {
		$final_data = '<i>No existen registros para los par&aacute;metros seleccionados.</i>';
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Ventas_Brutas.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $filters;
	echo $final_data
?>
