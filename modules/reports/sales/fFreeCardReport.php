<?php
/*
File:fFreeCardReport.php
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Modified By: 
Last Modified: 
*/
Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getZonesByFreeCard($_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty -> register('zonesList,error');
$smarty->display();
?>