<?php

/**
 * Name: pPaymentManagerAnalysisXLS
 * Created by: pastudillo
 * Created on: 24-may-2017
 * Description: 
 * Modified by: pastudillo
 * Modified on: 24-may-2017
 */

//Debug::print_r($_POST);

$dataSet = Reports::getPaidDataForManagementAnalysis($db, $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selCountry'], $_POST['selManager']);

$total_sold = 0;
$total_invoiced = 0;
$total_refunded = 0;
$total_diference = 0;
$total_discount = 0;

//Debug::print_r($dataSet); die;
if($dataSet){
    $finalDataArray = array();
    
    foreach($dataSet as $item){
        if(!isset($finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']])){
            $finalDataArray[$item['serial_mbc']]['manager_name'] = $item['name_man'];
            
            $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['responisble'] = $item['responsible'];
            $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['sold'] = 0;
            $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['invoiced'] = 0;
            $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['refunded'] = 0;
            $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['effective'] = 0;
            $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['discount'] = 0;
        }
        
        if(!isset($finalDataArray[$item['serial_mbc']]['totals'])){
            $finalDataArray[$item['serial_mbc']]['totals']['sold'] = 0;
            $finalDataArray[$item['serial_mbc']]['totals']['invoiced'] = 0;
            $finalDataArray[$item['serial_mbc']]['totals']['refunded'] = 0;
            $finalDataArray[$item['serial_mbc']]['totals']['effective'] = 0;
            $finalDataArray[$item['serial_mbc']]['totals']['discount'] = 0;
        }
        
        $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['sold'] += $item['total_sal'];
        $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['invoiced'] += $item['calculatedInvoice'];
        $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['refunded'] += $item['total_refunded'];
        $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['effective'] += ($item['total_sal'] - $item['total_refunded']);
        $finalDataArray[$item['serial_mbc']]['responsibles'][$item['serial_usr']]['discount'] += $item['amount_discount'];
        
        $finalDataArray[$item['serial_mbc']]['totals']['sold'] += $item['total_sal'];
        $finalDataArray[$item['serial_mbc']]['totals']['invoiced'] += $item['calculatedInvoice'];
        $finalDataArray[$item['serial_mbc']]['totals']['refunded'] += $item['total_refunded'];
        $finalDataArray[$item['serial_mbc']]['totals']['effective'] += ($item['total_sal'] - $item['total_refunded']);
        $finalDataArray[$item['serial_mbc']]['totals']['discount'] += $item['amount_discount'];
        
        $total_sold += $item['total_sal'];
        $total_invoiced += $item['calculatedInvoice'];
        $total_refunded += $item['total_refunded'];
        $total_diference += ($item['total_sal'] - $item['total_refunded']);
        $total_discount += $item['amount_discount'];
    }
}

//Debug::print_r($finalDataArray);

/***EXCEL REPORT** */
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Analisis_Pagos_Gerencial_Responsable.xls");
header("Pragma: no-cache");
header("Expires: 0");

$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
$title = "<table  border='1'>
                <tr><td colspan=6>$date</td></tr>
                <tr></tr>
                <tr><td colspan=6 align='center'><b>An&aacute;lisis de Cobranza Gerencial por Responsable</b></td></tr>
                 <tr></tr>
          </table>";
echo $title;

//get manager name for header
if ($serial_mbc) {
    $manager_by_country = New ManagerbyCountry($db, $serial_mbc);
    $manager_by_country->getData();
    $manager = new Manager($db, $manager_by_country->getSerial_man());
    $manager->getData();
    $name_man = $manager->getName_man();
} else {
    $name_man = "Todos";
}

//get values for the header
$zone = new Zone($db, $_POST['selZone']);
$zone->getData();
$name_zon = $zone->getName_zon();

$country = new Country($db, $_POST['selCountry']);
$country->getData();
$name_cou = $country->getName_cou();

//HEADER
$parameters = "<table border='1'>
                    <tr>
                        <td align='right'><b>Zona: </b></td><td align='left'>" . $name_zon . "</td>
                        <td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
                    </tr>
                    <tr>
                        <td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
                    </tr>
                    <tr>
                        <td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $_POST['txtBeginDate'] . "</td>
                        <td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $_POST['txtEndDate'] . "</td>
                    </tr>

                </table><br>";
echo $parameters;

if($finalDataArray){
    $masterTable = "";
    foreach($finalDataArray as $branch){
        $tempTable = "<table border='1'>
                        <tr><td align='center' colspan='6'><b>{$branch['manager_name']}</b></tr>
                        <tr>
                            <td align='center'><b>Responsable</b></td>
                            <td align='center'><b>Venta Bruta</b></td>
                            <td align='center'><b>Venta Neta</b></td>
                            <td align='center'><b>Reembolsos</b></td>
                            <td align='center'><b>Venta Efectiva</b></td>
                            <td align='center'><b>Descuentos Generados</b></td>
                        </tr>";
        
        if(isset($branch)){
            foreach($branch['responsibles'] as $line){
               $tempTable .= "<tr>
                                <td align='center'>{$line['responisble']}</td>
                                <td align='center'>{$line['sold']}</td>
                                <td align='center'>{$line['invoiced']}</td>
                                <td align='center'>{$line['refunded']}</td>
                                <td align='center'>{$line['effective']}</td>
                                <td align='center'>{$line['discount']}</td>
                            </tr>";
            }
        }
        
        $tempTable .= "<tr>
                            <td align='center'><b>TOTAL</b></td>
                            <td align='center'>{$branch['totals']['sold']}</td>
                            <td align='center'>{$branch['totals']['invoiced']}</td>
                            <td align='center'>{$branch['totals']['refunded']}</td>
                            <td align='center'>{$branch['totals']['effective']}</td>
                            <td align='center'>{$branch['totals']['discount']}</td>
                        </tr>";
        
        
        $tempTable .= "</table>";
        
        $masterTable .= $tempTable."<br />";
    }
    
    echo $masterTable; 
}else{
    echo 'No existen datos para los filtros seleccionados';
}