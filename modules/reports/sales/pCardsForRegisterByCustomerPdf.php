<?php
/*
    Document   : pcardsForRegisterByCustomerPdf
    Created on : 31-may-2010, 9:18:49
    Author     : Nicolas
    Description:
        Generates a PDF file with all cards available for travel registering
*/
Request::setInteger("0:serial_cus");
$customer=new Customer($db,$serial_cus);
$customer->getData();
$list=$customer->getRegisterCardByCustomer(TRUE);
include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
$pdf->ezSetDy(-20);
//title

if($list){
	foreach($list as &$li){
		for($i=0;$i<7;$i++){
			unset($li[$i]);
		}
	if($li['card_number_sal']=='')
				$li['card_number_sal']='N/A';
	}
$pdf->ezText("<b>Tarjetas que registran viajes de {$customer->getFirstname_cus()} {$customer->getLastname_cus()}</b>", 12, array('justification' =>'center'));
$pdf->ezSetDy(-10);
$pdf->ezTable($list,
				  array('name_pbl'=>'<b>Producto</b>',
					    'card_number_sal'=>'<b>No. Tarjeta</b>',
					    'begin_date_sal'=>'<b>Vigencia Desde</b>',
					    'end_date_sal'=>'<b>Vigencia Hasta</b>',
						'days_sal'=>'<b>'.utf8_decode('Días Contratados').'</b>',
						'available_days'=>'<b>'.utf8_decode('Días Disponibles').'</b>'),
				  '',
				  array('xPos'=>'center',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'name_pbl'=>array('justification'=>'center','width'=>60),
							'card_number_sal'=>array('justification'=>'center','width'=>120),
							'begin_date_sal'=>array('justification'=>'center','width'=>70),
							'end_date_sal'=>array('justification'=>'center','width'=>60),
							'days_sal'=>array('justification'=>'center','width'=>60),
							'available_days'=>array('justification'=>'center','width'=>60))));
}else{
	$pdf->ezText("No existen tarjetas que permitan registrar viajes.", 12, array('justification' =>'center'));
}
$pdf->ezStream();
?>
