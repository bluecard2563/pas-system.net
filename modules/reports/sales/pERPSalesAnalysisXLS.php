<?php
/**
 * File: pERPSalesAnalysisXLS.php
 * Author: Patricio Astudillo
 * Creation Date: 27/10/2016
 * Modified By:
 * Last Modified:
*/

//GET ALL THE INVICES MADE IN THAT PERIOD OF TIME FOR THE COUNTRY|MANAGER SELECTED
$invoices = Reports::getInvoicesForERPMatch($db, $_POST['selCountry'], $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selManager']);

if(!$invoices){
	http_redirect($url); //ERROR NO SALES REGISTERED
}

$totalsArray = array('invoiced'=>0, 'creditNote'=>0, 'qtyInvoices'=>0, 'qtyCreditNotes'=>0);
$detailedList = array();

foreach($invoices as $inv){
	$bc_branch = ERPConnectionFunctions::getStoreID($inv['serial_mbc']);
	
	//GROSS SALES INFORMATION
	$salesData = Reports::getSummarizedSalesForInvoice($db, $inv['serial_inv']);
	
	//KEEP TRACK FOR GENERAL INVOICES AMOUNTS AND QUANTITIES
	$totalsArray['invoiced'] += $inv['total_inv'];
	$totalsArray['qtyInvoices'] ++;
	
	//BUILD INVOICE DATA
	$tempArray = array();
	$tempArray['id'] = $inv['serial_inv'];
	$tempArray['manager'] = $inv['name_man'];
	$tempArray['document'] = 'INVOICE';
	$tempArray['number'] = $bc_branch.'-'.ERPConnectionFunctions::getDepartmentID($inv['serial_mbc']).'-'.str_pad($inv['number_inv'], 9, '0', STR_PAD_LEFT);
	$tempArray['date'] = $inv['date_inv'];
	
	$tempArray['sold'] = $salesData['sold'];
	$tempArray['subtotal'] = $inv['subtotal_inv'];
	
	$tempArray['discount'] = $salesData['discount'];
	
	//TAXES SEPARATION
	$taxes = unserialize($inv['applied_taxes_inv']);
	if($taxes){
		foreach($taxes as $taxLine){
			$taxAmount += $inv['subtotal_inv'] * ($taxLine['tax_percentage']/100);
		}
		
		$tempArray['tax'] = $taxAmount;
	}else{
		$tempArray['tax'] = 0;
	}
	
	$tempArray['billTotal'] = $inv['total_inv'];
	$tempArray['comission'] = $salesData['comission'];
	array_push($detailedList, $tempArray);
}

//GET ALL CREDIT NOTES IN SYSTEM FOR DEDUCTIONS
if($_POST['rdInclude'] == 'YES'){
	$myCn = new CreditNote($db);
	$creditNotes = $myCn -> getCreditNotesByParameters(NULL,NULL, $_POST['txtBeginDate'], $_POST['txtEndDate'], NULL, NULL, NULL, NULL, $_POST['selCountry']);

	if($creditNotes){
		foreach($creditNotes as $cn){
			$bc_branch = ERPConnectionFunctions::getStoreID($cn['serial_mbc']);

			//KEEP TRACK FOR GENERAL CREDIT NOTES AMOUNTS AND QUANTITIES
			$totalsArray['creditNote'] += $cn['amount_cn'];
			$totalsArray['qtyCreditNotes'] ++;

			$tempArray = array();
			$tempArray['id'] = $inv['serial_CN'];
			$tempArray['manager'] = $cn['name_man'];
			$tempArray['document'] = 'CREDIT_NOTE';
			$tempArray['number'] = $bc_branch.'-'.ERPConnectionFunctions::getDepartmentID($inv['serial_mbc']).'-'.str_pad($cn['number_cn'], 9, '0', STR_PAD_LEFT);
			$tempArray['date'] = $cn['date_cn'];

			$tempArray['sold'] = 0;
			$tempArray['subtotal'] = 0;

			$tempArray['discount'] = 0;
			$tempArray['tax'] = 0;

			$tempArray['billTotal'] = $cn['amount_cn'];
			array_push($detailedList, $tempArray);
		}
	}
}

/*********************************** DISPLAYING THE XLS *****************************************/
$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
$title = '<table>
				<tr><td colspan=6>'.$date.'</td></tr>
				<tr></tr>
				<tr>
					<th></th>
					<th><b>Cuadre Contable de Ventas</b></th>
				</tr>
				<tr></tr>
		  </table><br />';

//APLIED FILTERS
$filters = "<table>
				<tr>
					<td><b>Pa&iacute;s:</b></td>
					<td>{$_POST['selCountry']}</td>
					<td><b>A&ntilde;o:</b></td>
					<td>$selYear</td>
				</tr>

				<tr>
					<td><b>Fecha Desde:</b></td>
					<td>{$_POST['txtBeginDate']}</td>
					<td><b>Fecha Hasta:</b></td>
					<td>{$_POST['txtEndDate']}</td>
				</tr>";
$filters .= "</table><br />";

if(count($detailedList) > 0){
		$data = "	<table>
						<tr>
							<td><b>Representante</b></td>
							<td><b>Tipo de Documento</b></td>
							<td><b>Nro. Documento</b></td>
							<td><b>Fecha Emisi&oacute;n</b></td>
							<td><b>Sumatoria Ventas</b></td>
							<td><b>Descuentos</b></td>
							<td><b>Subtotal Factura</b></td>
							<td><b>Impuestos</b></td>
							<td><b>Total</b></td>
						</tr>";

		foreach($detailedList as $d){
			$data .= "<tr>
								<td>{$d['manager']}</td>
								<td>{$global_documentPurpose[$d['document']]}</td>
								<td>{$d['number']}</td>
								<td>{$d['date']}</td>
								<td>{$d['sold']}</td>
								<td>{$d['discount']}</td>
								<td>{$d['subtotal']}</td>
								<td>{$d['tax']}</td>
								<td>{$d['billTotal']}</td>
							</tr>";
		}
		
		$data .= "</table><br />";
}

//TOTAL DATA
$totals = "<table>
				<tr>
					<td colspan='4'><b>RESUMEN</b></td>
				</tr>
				<tr>
					<td><b>Nro. Facturas:</b></td>
					<td>{$totalsArray['qtyInvoices']}</td>
					<td><b>Total Facturas:</b></td>
					<td>{$totalsArray['invoiced']}</td>
				</tr>

				<tr>
					<td><b>Nro. Notas de Cr&eacute;dito:</b></td>
					<td>{$totalsArray['qtyCreditNotes']}</td>
					<td><b>Total NC:</b></td>
					<td>{$totalsArray['creditNote']}</td>
				</tr>";
$totals .= "</table><br />";

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=CUADRE_CONTABLE_VENTAS.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $title;
echo $filters;
echo $data;
echo $totals;