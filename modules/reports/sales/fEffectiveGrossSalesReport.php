<?php
/**
 * File:  fEffectiveGrossSalesReport
 * Author: Patricio Astudillo
 * Creation Date: 25-abr-2011, 15:50:57
 * Description:
 * Last Modified: 25-abr-2011, 15:50:57
 * Modified by:
 */

	//ZONE
	$zone = new Zone($db);
	$nameZoneList=$zone->getZones();

	//STATUS

	$typeList = Sales::getSalesTypes($db);
	$stockTypeList = Sales::getSalesStock($db);
	$order_by = array('card_number'=>'N&uacute;mero de tarjeta','in_date'=>'Fecha de ingreso');

	$current_year = (int)date('Y');
	$year_array = array();
	for($i = 2008; $i <= $current_year; $i++){
		array_push($year_array, $i);
	}

	$smarty->register('nameZoneList,global_salesStatus,typeList,global_cardSalestype,stockTypeList,order_by');
	$smarty->register('year_array');
	$smarty -> display();
?>
