<?php
/*
 * File: pViewCardsByCustomerPDF.php
 * Author: Patricio Astudillo
 * Creation Date: 28/05/2010, 12:13:51 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 28/05/2010, 12:13:51 PM
 */

Request::setInteger('0:serial_cus');

if($serial_cus){
	$cardsInfo=Sales::getCardsByCustomerReport($db, $serial_cus);
	$customer=new Customer($db, $serial_cus);
	$customer->getData();

	if(is_array($cardsInfo)){
		foreach($cardsInfo as &$c){
			$c['status_sal']=$global_salesStatus[$c['status_sal']];
			if($c['card_number_sal']==''){
				$c['card_number_sal']='N/A';
			}
			
			for($i=0;$i<5;$i++){
				unset($c[$i]);
			}
		}		
	}
}

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

//HEADER
$pdf->ezText('<b>REPORTE DE TARJETAS POR CLIENTE</b>', 12, array('justification'=>'center'));

$pdf->ezSetDy(-10);
$headerDesc=array();
$descLine1=array('col1'=>utf8_decode('<b>Nombre del Cliente:</b>'),
				 'col2'=>$customer->getFirstname_cus().' '.$customer->getLastname_cus());
array_push($headerDesc,$descLine1);

$descLine2=array('col1'=>utf8_decode('<b>Documento:</b>'),
				 'col2'=>$customer->getDocument_cus());
array_push($headerDesc,$descLine2);

$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150))));
$pdf->ezSetDy(-10);
//END HEADERS

if(is_array($cardsInfo)){
	/*BEGIN REGISTERS*/
	$pdf->ezTable($cardsInfo,
			  array('card_number_sal'=>'<b>No. de Tarjeta</b>',
					'name_pbl'=>'<b>Tarjetas Vendidas</b>',
					'begin_date_sal'=>'<b>Tarjetas Vendidas</b>',
				    'end_date_sal'=>'<b>Tarjetas Vendidas</b>',
				    'status_sal'=>'<b>Tarjetas Vendidas</b>'),
					'Listado de Tarjetas',
			  array('showHeadings'=>1,
					'shaded'=>1,
					'showLines'=>2,
					'xPos'=>'center',
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8,
					'fontSize' => 8,
					'titleFontSize' => 8,
					'cols'=>array(
						'name_pbl'=>array('justification'=>'center','width'=>130),
						'items_sold'=>array('justification'=>'center','width'=>80))));
	/*END REGISTERS*/
}else{
	$pdf->ezText(utf8_decode('Hubo errores en la carga de datos. Lo sentimos'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>
