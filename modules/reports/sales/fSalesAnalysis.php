<?php
/*
File: fSalesAnalysis.php
Author: Gabriela Guerrero
Creation Date: 06/07/2010
Modified By:
Last Modified:
*/
Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getZonesSalesAnalysis($_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);

$smarty -> register('zonesList,error');
$smarty->display();
?>