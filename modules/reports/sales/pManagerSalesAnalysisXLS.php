<?php

/*
  File: fSalesAnalysis.php
  Author: Esteban Angulo
  Creation Date: 04/05/2011
  Modified By:
  Last Modified:
 */

//get filters
$serial_zon = $_POST['selZone'];
$serial_cou = $_POST['selCountry'];
$serial_mbc = $_POST['selManager'];
$begin_date = $_POST['txtBeginDate'];
$end_date = $_POST['txtEndDate'];

//get values for the header
$zone = new Zone($db, $serial_zon);
$zone->getData();
$name_zon = $zone->getName_zon();

$country = new Country($db, $serial_cou);
$country->getData();
$name_cou = $country->getName_cou();

if ($serial_mbc) {
	$managerList = ManagerbyCountry::getManagersWithDealers($db, $serial_cou, $serial_mbc);
} else {
	$managerList = ManagerbyCountry::getManagersWithDealers($db, $serial_cou, 1); //if a manager is not selected in the filter form
}

if (is_array($managerList)) {
	$pos = 0;
	$total_sales_by_country = 0;
	//$total_void_free_refunded_by_country = 0;
	$total_void_by_country=0;
	$total_free_by_country=0;
	$total_refunded_by_country = 0;        
	$total_percentage_by_country = 0;
	$total_efective_sale_by_country = 0;

	//Create the arrays to use them in the Graphic
	$totals_by_mbc_graphic_array = Array();
	$names_graphic_array = Array();

	foreach ($managerList as $mbc) {
		$manager_info[$pos]['name'] = $mbc['name_man'];
		
		$sales_by_mbc = Sales::getInfoForManagerSalesAnalysisReport($db, $serial_cou, $mbc['serial_mbc'], $begin_date, $end_date, FALSE);
		$pos2 = 0;
		$total_sales_by_mbc = 0;
		$total_void_by_mbc=0;
		$total_free_by_mbc=0;
		$total_refunded_by_mbc = 0;
		$total_percentage_by_mbc = 0;
		$total_efective_sale_by_mbc = 0;
		$user_by_dealer = array();
		if (is_array($sales_by_mbc)) {
			foreach ($sales_by_mbc as $sales) {
				if (is_array($sales)) {
					$void_free_refunded = $sales['void'] + $sales['free'] + $sales['refunded'];

					//aux array for each user by dealer
					$user_by_dealer [$pos2]['name_usr'] = $sales['name_usr'];
					$user_by_dealer [$pos2]['sales'] = number_format($sales['sales'], 2, ',', '');
					$user_by_dealer [$pos2]['void'] = number_format($sales['void'], 2, ',', '');
					$user_by_dealer [$pos2]['free'] = number_format($sales['free'], 2, ',', '');
					$user_by_dealer [$pos2]['refunded'] = number_format($sales['refunded'], 2, ',', '');
					if($sales['sales'] > 0){
						$user_by_dealer [$pos2]['void_percentage'] = number_format(($void_free_refunded * 100) / $sales['sales'], 2);
						$user_by_dealer [$pos2]['efective_sale'] = number_format($sales['sales'] - $void_free_refunded, 2);
					}	
					$pos2++;
					//calulate the total values for the manager
					$total_sales_by_mbc += $sales['sales'];
					$total_void_by_mbc += $sales['void'];
					$total_free_by_mbc += $sales['free'];
					$total_refunded_by_mbc += $sales['refunded'];
					$total_efective_sale_by_mbc += ( $sales['sales'] - $void_free_refunded);
				}
			}

			//calculates total percentage
			$total_percentage_by_mbc = number_format((($total_void_by_mbc + $total_free_by_mbc + $total_refunded_by_mbc) * 100) / $total_sales_by_mbc, 2, ',', '');

			//calculte the total of the country
			$total_sales_by_country += $total_sales_by_mbc;
			$total_void_by_country += $total_void_by_mbc;
			$total_free_by_country += $total_free_by_mbc;
			$total_refunded_by_country += $total_refunded_by_mbc;
			$total_efective_sale_by_country += $total_efective_sale_by_mbc;

			//generates the final array with the info of all managers
			$manager_info[$pos]['sales_info'] = $user_by_dealer;
			$manager_info[$pos]['totals_by_mbc'][0]['name'] = "<b>Total Ventas Brutas " . $mbc['name_man'] . "</b>";
			$manager_info[$pos]['totals_by_mbc'][0]['total_sales_by_mbc'] = number_format($total_sales_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_void_by_mbc'] = number_format($total_void_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_free_by_mbc'] = number_format($total_free_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_refunded_by_mbc'] = number_format($total_refunded_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_percentage_by_mbc'] = number_format($total_percentage_by_mbc, 2, ',', '');
			$manager_info[$pos]['totals_by_mbc'][0]['total_efective_sale'] = number_format($total_efective_sale_by_mbc, 2, ',', '');

			//save the manager name
			array_push($names_graphic_array, $mbc['name_man']);
			//Adding values to the totals_by_mbc array used in the graphic
			array_push($totals_by_mbc_graphic_array, $total_efective_sale_by_mbc);
			//Increase the postion for the next mbc
			$pos++;
		}
	}
     //fix for negative values
       foreach($totals_by_mbc_graphic_array as &$tg){
           if($tg<0){
               $tg = 0;
           }
       }
   //

	if ($total_sales_by_country) {
		//calculates total percentage for the country
		$total_percentage_by_country = number_format((($total_void_by_country + $total_free_by_country + $total_refunded_by_country) * 100) / $total_sales_by_country, 2, ',', '');
		//adding the country total values to the array
		$manager_info[$pos]['total_country'][0]['name'] = "<b>Total " . $name_cou . "</b>";
		$manager_info[$pos]['total_country'][0]['total_sales_by_country'] = number_format($total_sales_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_void_by_country'] = number_format($total_void_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_free_by_country'] = number_format($total_free_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_refunded_by_country'] = number_format($total_refunded_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_percentage_by_country'] = number_format($total_percentage_by_country, 2, ',', '');
		$manager_info[$pos]['total_country'][0]['total_efective_sale_by_country'] = number_format($total_efective_sale_by_country, 2, ',', '');
	}
}

		/*	 * *EXCEL REPORT** */
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=Reporte_de_Analisis_Ventas_Gerencial.xls");
		header("Pragma: no-cache");
		header("Expires: 0");

if ($total_sales_by_country) {
	if (is_array($manager_info)) {
		/*	 * GRAPHIC** */
		// Standard inclusions of pChart and pData Classes
		include(DOCUMENT_ROOT . "lib/pChart/pChart/pData.class");
		include(DOCUMENT_ROOT . "lib/pChart/pChart/pChart.class");

		// Dataset definition
		$DataSet = new pData;
		$DataSet->AddPoint($totals_by_mbc_graphic_array, "Serie1"); //Data to create the Pie
		$DataSet->AddPoint($names_graphic_array, "Serie2"); //Data to create the Legend of the graphic
		$DataSet->AddAllSeries();
		$DataSet->SetAbsciseLabelSerie("Serie2"); //Adding Labels
		// Initialise the graph
		$Test = new pChart(800, 400);
		$Test->setFontProperties(DOCUMENT_ROOT . "lib/pChart/Fonts/tahoma.ttf", 8); //setting font from Library files
		// Draw the pie chart
		$Test->AntialiasQuality = 0;
		$Test->setShadowProperties(1, 1, 200, 200, 200);
		$Test->drawFlatPieGraphWithShadow($DataSet->GetData(), $DataSet->GetDataDescription(), 200, 150, 120, PIE_PERCENTAGE, 8);
		$Test->clearShadow();

		$Test->drawPieLegend(375, 5, $DataSet->GetData(), $DataSet->GetDataDescription(), 250, 250, 250); //Draw the legend
		//Render and Saving the image in the selected folder
		$Test->Render(DOCUMENT_ROOT . "system_temp_files/ManagerSalesAnalysis.png");
		/*	 * *END GRAPHIC* */

		$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
		$title = "<table  border='1'>
						<tr><td colspan=6>$date</td></tr>
						<tr></tr>
						<tr><td colspan=6 align='center'><b>Reporte de An&aacute;lisis de Ventas Gerencial</b></td></tr>
						 <tr></tr>
				  </table>";
		echo $title;

		//get manager name for header
		if ($serial_mbc) {
			$manager_by_country = New ManagerbyCountry($db, $serial_mbc);
			$manager_by_country->getData();
			$manager = new Manager($db, $manager_by_country->getSerial_man());
			$manager->getData();
			$name_man = $manager->getName_man();
		} else {
			$name_man = "Todos";
		}
		//HEADER
		$parameters = "<table border='1'>
							<tr>
								<td align='right'><b>Zona: </b></td><td align='left'>" . $name_zon . "</td>
								<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
							</tr>
							<tr>
								<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
							</tr>
							<tr>
								<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $begin_date . "</td>
								<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $end_date . "</td>
							</tr>
							<tr>
								<td colspan='4'><i>* El total de ventas incluye el valor de ventas FREE</i></td>
							</tr>
						</table><br>";
		echo $parameters;
		//DATA
		$auxArr = array_keys($manager_info);
		$last = end($auxArr);

		foreach ($manager_info as $key1 => $info) {
			if (is_array($info)) {
				if ($key1 != $last) {//header of the table
					$table = "<br/><table border='1'>
									<tr>
										<td align='center'><b>" . $info['name'] . "</b></td>
										<td align='center'><b>Ventas</b></td>
										<td align='center'><b>Anuladas</b></td>
										<td align='center'><b>Free</b></td>
										<td align='center'><b>Reembolsos</b></td>
										<td align='center'><b>% Anulaci&oacute;n</b></td>
										<td align='center'><b>Venta Efectiva</b></td>
									</tr>";

					foreach ($info as $key2 => $i) {
						if ($key2 == 'sales_info') {
							foreach ($i as $ubd) {//data rows
								$table.="<tr>
											<td align='center'>" . $ubd['name_usr'] . "</td>
											<td align='center'>" . $ubd['sales'] . "</td>
											<td align='center'>" . $ubd['void'] . "</td>
											<td align='center'>" . $ubd['free'] . "</td>
											<td align='center'>" . $ubd['refunded'] . "</td>
											<td align='center'>" . $ubd['void_percentage'] . "</td>
											<td align='center'>" . $ubd['efective_sale'] . "</td>
										</tr>";
							}
						}
						if ($key2 == 'totals_by_mbc') {
							$table.="<tr>
											<td align='center'><b>" . $i['0']['name'] . "<b/></td>
											<td align='center'><b>" . $i['0']['total_sales_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_void_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_free_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_refunded_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_percentage_by_mbc'] . "</b></td>
											<td align='center'><b>" . $i['0']['total_efective_sale'] . "</b></td>
										</tr>";
						}
					}
					$table.="</table><br />";
					echo $table;
				} else {
					if (!$serial_mbc) {
						////totals for the country
						foreach ($info['total_country'] as $key3 => $totals_country) {
							$total_country_table = "<table border='1'>
										<tr></tr>
										<tr>
											<td align='center'><b>" . $totals_country['name'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_sales_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_void_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_free_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_refunded_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_percentage_by_country'] . "</b></td>
											<td align='center'><b>" . $totals_country['total_efective_sale_by_country'] . "</td>
										</tr>
									  </table>";
						}
						echo $total_country_table;
						/** Adding the Graphic as an image to the Excel document* */
						$graphic_path = DOCUMENT_ROOT . "system_temp_files/ManagerSalesAnalysis.png";
						$graphic = "<table border='1'>
										<tr></tr>
										<tr>
											<td colspan=6 align='center'><b>Gr&aacute;fico</b></td>
										</tr>
										<tr>
											<td colspan=6 align='center'>&nbsp;<img src=" . '"' . $graphic_path . '"' . "/></td>
										</tr>
									  </table>";
						echo $graphic;
						/**					 * */
					}
				}
			}
		}
		/*	 * *END EXCEL REPORT** */
	}
}else {
		echo '<i>No existen registros para los par&aacute;metros seleccionados.</i>';
	}
?>