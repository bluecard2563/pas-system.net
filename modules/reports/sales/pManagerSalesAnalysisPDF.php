<?php

/*
  File: fSalesAnalysis.php
  Author: Esteban Angulo
  Creation Date: 04/05/2011
  Modified By:
  Last Modified:
 */

//get filters
$serial_zon = $_POST['selZone'];
$serial_cou = $_POST['selCountry'];
$serial_mbc = $_POST['selManager'];
$begin_date = $_POST['txtBeginDate'];
$end_date = $_POST['txtEndDate'];

$dateFrom = DateTime::createFromFormat('d/m/Y', $begin_date)->format('Y-m-d');
$dateTo = DateTime::createFromFormat('d/m/Y', $end_date)->format('Y-m-d');

//get values for the header
$zone = new Zone($db, $serial_zon);
$zone->getData();
$name_zon = $zone->getName_zon();

$country = new Country($db, $serial_cou);
$country->getData();
$name_cou = $country->getName_cou();

if ($serial_mbc) {
	$managerList = ManagerbyCountry::getManagersWithDealers($db, $serial_cou, $serial_mbc);
} else {
	$managerList = ManagerbyCountry::getManagersWithDealers($db, $serial_cou, 1); //if a manager is not selected in the filter form
}

if (is_array($managerList)) {
	$pos = 0;
	$total_sales_by_country = 0;
	//$total_void_free_refunded_by_country = 0;
	$total_void_by_country=0;
	$total_free_by_country=0;
	$total_refunded_by_country = 0;
	$total_percentage_by_country = 0;
	$total_efective_sale_by_country = 0;

	//Create the arrays to use them in the Graphic
	$totals_by_mbc_graphic_array = Array();
	$names_graphic_array = Array();
//Debug::print_r($managerList);die();
	foreach ($managerList as $mbc) {
		$manager_info[$pos]['name'] = $mbc['name_man'];

		$sales_by_mbc = Sales::getInfoForManagerSalesAnalysisReportModify($db, $serial_cou, $mbc['serial_mbc'], $dateFrom, $dateTo);
		$pos2 = 0;
		$total_sales_by_mbc = 0;
		$total_void_by_mbc=0;
		$total_free_by_mbc=0;
		$total_refunded_by_mbc = 0;
		$total_percentage_by_mbc = 0;
		$total_efective_sale_by_mbc = 0;
		$user_by_dealer = array();
		if (is_array($sales_by_mbc)) {			
			foreach ($sales_by_mbc as $sales) {
				if (is_array($sales)) {
					$void_free_refunded = $sales['void'] + $sales['free'] + $sales['refunded'];

					//aux array for each user by dealer
					$user_by_dealer [$pos2]['name_usr'] = $sales['name_usr'];
					$user_by_dealer [$pos2]['total_inv'] = number_format($sales['total_inv'], 2);
					$user_by_dealer [$pos2]['void'] = number_format($sales['void'], 2);
					$user_by_dealer [$pos2]['total_discount_Comission'] = number_format($sales['total_discount_Comission'], 2);
					$user_by_dealer [$pos2]['total_other_discount'] = number_format($sales['total_other_discount'], 2);
					if($sales['total_inv'] > 0){
						$user_by_dealer [$pos2]['void_percentage'] = number_format(($sales['void'] * 100) / $sales['total_inv'], 2);
						$user_by_dealer [$pos2]['efective_sale'] = number_format($sales['total_inv'] - $sales['void'], 2);
					}					
					$pos2++;
					//calulate the total values for the manager
					$total_sales_by_mbc += $sales['total_inv'];
					$total_void_by_mbc += $sales['void'];
					$total_total_discount_Comission += $sales['total_discount_Comission'];
					$total_total_other_discount += $sales['total_other_discount'];
					$total_efective_sale_by_mbc += ( $sales['total_inv'] - $sales['void']);
				}
			}

			//calculates total percentage
			$total_percentage_by_mbc = number_format((($total_void_by_mbc) * 100) / $total_sales_by_mbc, 2);

			//calculte the total of the country
			$total_sales_by_country += $total_sales_by_mbc;
			$total_void_by_country += $total_void_by_mbc;
			$total_total_discount_Comission += $total_total_discount_Comission;
			$total_total_other_discount += $total_total_other_discount;
			$total_efective_sale_by_country += $total_efective_sale_by_mbc;

			//generates the final array with the info of all managers
			$manager_info[$pos]['sales_info'] = $user_by_dealer;
			$manager_info[$pos]['totals_by_mbc'][0]['name'] = "<b>Total Ventas Brutas " . $mbc['name_man'] . "</b>";
			$manager_info[$pos]['totals_by_mbc'][0]['total_sales_by_mbc'] = number_format($total_sales_by_mbc, 2);
			$manager_info[$pos]['totals_by_mbc'][0]['total_void_by_mbc'] = number_format($total_void_by_mbc, 2);
			$manager_info[$pos]['totals_by_mbc'][0]['total_total_discount_Comission'] = number_format($total_total_discount_Comission, 2);
			$manager_info[$pos]['totals_by_mbc'][0]['total_total_other_discount'] = number_format($total_total_other_discount, 2);
			$manager_info[$pos]['totals_by_mbc'][0]['total_percentage_by_mbc'] = number_format($total_percentage_by_mbc, 2);
			$manager_info[$pos]['totals_by_mbc'][0]['total_efective_sale'] = number_format($total_efective_sale_by_mbc, 2);

			//save the manager name
			array_push($names_graphic_array, $mbc['name_man']);
			//Adding values to the totals_by_mbc array used in the graphic
			array_push($totals_by_mbc_graphic_array, $total_efective_sale_by_mbc);
			//Increase the postion for the next mbc
			$pos++;
		}

	}
  //fix for negative values
       foreach($totals_by_mbc_graphic_array as &$tg){
           if($tg<0){
               $tg = 0;
           }
       }
   //
	if ($total_sales_by_country) {
		//calculates total percentage for the country
		$total_percentage_by_country = number_format((($total_void_by_country) * 100) / $total_sales_by_country, 2);
		//adding the country total values to the array
		$manager_info[$pos]['total_country'][0]['name'] = "<b>Total " . $name_cou . "</b>";
		$manager_info[$pos]['total_country'][0]['total_sales_by_country'] = number_format($total_sales_by_country, 2);
		$manager_info[$pos]['total_country'][0]['total_void_by_country'] = number_format($total_void_by_country, 2);
		$manager_info[$pos]['total_country'][0]['total_total_discount_Comission'] = number_format($total_total_discount_Comission, 2);
		$manager_info[$pos]['total_country'][0]['total_total_other_discount'] = number_format($total_total_other_discount, 2);
		$manager_info[$pos]['total_country'][0]['total_percentage_by_country'] = number_format($total_percentage_by_country, 2);
		$manager_info[$pos]['total_country'][0]['total_efective_sale_by_country'] = number_format($total_efective_sale_by_country, 2);
	}
}

		/*	 * *PDF REPORT** */
		include_once DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';
		$pdf->ezSetDy(+20);
		$pdf->ezText('<b>Reporte de ' . utf8_decode(Análisis) . ' de Ventas Gerencial</b>', 14, array('justification' => 'center'));
		$pdf->ezSetDy(-10);
		$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
		$pdf->ezText($date, 10, array('justification' => 'center'));
		$pdf->setColor(0, 0, 0);
		$pdf->ezSetDy(-20);

if ($total_sales_by_country) {
	if (is_array($manager_info)) {
		/*	 * GRAPHIC** */
		// Standard inclusions of pChart and pData Classes
		include(DOCUMENT_ROOT . "lib/pChart/pChart/pData.class");
		include(DOCUMENT_ROOT . "lib/pChart/pChart/pChart.class");

		// Dataset definition
		$DataSet = new pData;
		$DataSet->AddPoint($totals_by_mbc_graphic_array, "Serie1"); //Data to create the Pie
		$DataSet->AddPoint($names_graphic_array, "Serie2"); //Data to create the Legend of the graphic
		$DataSet->AddAllSeries();
		$DataSet->SetAbsciseLabelSerie("Serie2"); //Adding Labels
		// Initialise the graph
		$Test = new pChart(800, 400);
		$Test->setFontProperties(DOCUMENT_ROOT . "lib/pChart/Fonts/tahoma.ttf", 8); //setting font from Library files
		// Draw the pie chart
		$Test->AntialiasQuality = 0;
		$Test->setShadowProperties(1, 1, 200, 200, 200);
		$Test->drawFlatPieGraphWithShadow($DataSet->GetData(), $DataSet->GetDataDescription(), 200, 150, 120, PIE_PERCENTAGE, 8);
		$Test->clearShadow();

		$Test->drawPieLegend(375, 5, $DataSet->GetData(), $DataSet->GetDataDescription(), 250, 250, 250); //Draw the legend
		//Render and Saving the image in the selected folder
		$Test->Render(DOCUMENT_ROOT . "system_temp_files/ManagerSalesAnalysis.png");
		/*	 * *END GRAPHIC* */

		//get manager name for header
		if ($serial_mbc) {
			$manager_by_country = New ManagerbyCountry($db, $serial_mbc);
			$manager_by_country->getData();
			$manager = new Manager($db, $manager_by_country->getSerial_man());
			$manager->getData();
			$name_man = $manager->getName_man();
		} else {
			$name_man = "Todos";
		}
		//HEADER
		$parameters = array(
			'0' => array('0' => '<b>Zona: </b>', '1' => $name_zon, '2' => '<b>' . html_entity_decode("Pa&iacute;s") . ':</b>', '3' => $name_cou),
			'1' => array('0' => '<b>Representante:</b>', '1' => $name_man),
			'2' => array('0' => '<b>Desde:</b>', '1' => $begin_date, '2' => '<b>Hasta:</b>', '3' => $end_date),
		);

		$pdf->ezTable($parameters, array('0' => '', '1' => '', '2' => '', '3' => ''), '', array('showHeadings' => 0, 'shaded' => 0, 'showLines' => 0, 'fontSize' => 10, 'xPos' => '300',
			'cols' => array(
				'0' => array('justification' => 'right', 'width' => 100),
				'1' => array('justification' => 'left', 'width' => 130),
				'2' => array('justification' => 'right', 'width' => 100),
				'3' => array('justification' => 'left', 'width' => 130)
			)
		));
		$pdf->ezText('* El total de ventas incluye el valor de ventas FREE', 7.5, array('justification' => 'center'));
		$pdf->ezSetDy(-20);
		//DATA
		$auxArr = array_keys($manager_info);
		$last = end($auxArr);

		for ($i = 0; $i <= sizeof($auxArr) - 1; $i++) {
			$pdf->ezTable(	$manager_info[$i]['sales_info'],
							array(	'name_usr' => '<b>' . $manager_info[$i]['name'] . '</b>',
									'total_inv' => '<b>Ventas</b>',
									'void' => '<b>Anuladas</b>',
									'total_discount_Comission' => '<b>Descuentos Comisión</b>',
									'total_other_discount' => '<b>Otros Descuentos</b>',
									'void_percentage' => '<b>% ' . html_entity_decode("Anulaci&oacute;n") . ' </b>',
									'efective_sale' => '<b>Venta Efectiva</b>'),
							'',
							array(	'showHeadings' => 1,
									'shaded' => 1,
									'showLines' => 2,
									'fontSize' => 10,
									'xPos' => '300',
									'cols' => array(
										'name_usr' => array('justification' => 'center', 'width' => 120),
										'total_inv' => array('justification' => 'center', 'width' => 70),
										'void' => array('justification' => 'center', 'width' => 60),
										'total_discount_Comission' => array('justification' => 'center', 'width' => 50),
										'total_other_discount' => array('justification' => 'center', 'width' => 70),
										'void_percentage' => array('justification' => 'center', 'width' => 80),
										'efective_sale' => array('justification' => 'center', 'width' => 80)
									)
			));

	//		Debug::print_r($manager_info);
	//		die;

			$pdf->ezTable(	$manager_info[$i]['totals_by_mbc'],
							array(	'name' => '',
									'total_sales_by_mbc' => '',
									'total_void_by_mbc' => '',
									'total_total_discount_Comission' => '',
									'total_total_other_discount' => '',
									'total_percentage_by_mbc' => '',
									'total_efective_sale' => ''),
							'',
							array(	'showHeadings' => 0,
									'shaded' => 0,
									'showLines' => 1,
									'fontSize' => 10,
									'xPos' => '300',
									'cols' => array(
										'name' => array('justification' => 'center', 'width' => 120),
										'total_sales_by_mbc' => array('justification' => 'center', 'width' => 70),
										'total_void_by_mbc' => array('justification' => 'center', 'width' => 60),
										'total_total_discount_Comission' => array('justification' => 'center', 'width' => 50),
										'total_total_other_discount' => array('justification' => 'center', 'width' => 70),
										'total_percentage_by_mbc' => array('justification' => 'center', 'width' => 80),
										'total_efective_sale' => array('justification' => 'center', 'width' => 80)
									)
			));
			$pdf->ezSetDy(-20);
		}

		//Country Total Results are printed only if there is no mbc specified
		if (!$serial_mbc) {
			$pdf->ezSetDy(+20);
			$pdf->ezTable(	$manager_info[$last]['total_country'],
							array(	'name' => '',
									'total_sales_by_country' => '',
									'total_void_by_country' => '',
									'total_total_discount_Comission' => '',
									'total_total_other_discount' => '',
									'total_percentage_by_country' => '',
									'total_efective_sale_by_country' => ''),
							'',
							array(	'showHeadings' => 0,
									'shaded' => 0,
									'showLines' => 1,
									'fontSize' => 10,
									'xPos' => '300',
							'cols' => array(
								'name' => array('justification' => 'center', 'width' => 120),
								'total_sales_by_country' => array('justification' => 'center', 'width' => 70),
								'total_void_by_country' => array('justification' => 'center', 'width' => 60),
								'total_total_discount_Comission' => array('justification' => 'center', 'width' => 50),
								'total_total_other_discount' => array('justification' => 'center', 'width' => 70),
								'total_percentage_by_country' => array('justification' => 'center', 'width' => 80),
								'total_efective_sale_by_country' => array('justification' => 'center', 'width' => 80)
							)
			));

			//Adding the Graphic in a new Page of the Pdf document
			$pdf->ezNewPage();
			$pdf->ezSetDy(+20);
			$pdf->ezText(html_entity_decode('<b>Gr&aacute;fico</b>'), 14, array('justification' => 'center'));
			$pdf->addPngFromFile(DOCUMENT_ROOT . "system_temp_files/ManagerSalesAnalysis.png", 50, 480, 500, 230);
		}

		$pdf->ezStream();
		/*	 * *END PDF** */
	}
}else {
		$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados.'), 10, array('justification' =>'center'));
		$pdf->ezStream();
	  }


?>
