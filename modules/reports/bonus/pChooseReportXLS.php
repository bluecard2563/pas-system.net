<?php
/*
 * File: pChooseReportXLS.php
 * Author: Patricio Astudillo
 * Creation Date: 20/07/2010, 02:38:09 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 16/07/2010, 02:38:09 PM
 */

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Reporte_de_Reembolsos.xls");
header("Pragma: no-cache");
header("Expires: 0");

$parameters=array();

if($_POST['rdType']=='PARAMETER'){ //FILTERS BY PARAMETERS
	$zone=new Zone($db, $_POST['selZone']);
	$zone->getData();
	$zone=$zone->getName_zon();
	$country=new Country($db, $_POST['selCountry']);
	$country->getData();
	$country=$country->getName_cou();
	$aux=array('col1'=>'<b>Zona:</b>',
			   'col2'=>$zone,
			   'col3'=>html_entity_decode('<b>Pa&iacute;s:</b>'),
			   'col4'=>$country);
	array_push($parameters, $aux);

	$manager=ManagerbyCountry::getManagerByCountryName($db, $_POST['selManager']);
	$city=new City($db, $_POST['selCity']);
	$city->getData();
	$city=$city->getName_cit();
	$aux=array('col1'=>'<b>Representante:</b>',
			   'col2'=>$manager,
			   'col3'=>'<b>Ciudad:</b>',
			   'col4'=>$city);
	array_push($parameters, $aux);

	$branch=new Dealer($db, $_POST['selBranch']);
	$branch->getData();
	$branch=$branch->getName_dea();
	$dealer=new Dealer($db, $_POST['selDealer']);
	$dealer->getData();
	$dealer=$dealer->getName_dea();
	$aux=array('col1'=>'<b>Comercializador:</b>',
			   'col2'=>$dealer,
			   'col3'=>'<b>Sucursal de Comercializador:</b>',
			   'col4'=>$branch);
	array_push($parameters, $aux);

	if($_POST['selCounter']){
		$counter=new Counter($db, $_POST['selCounter']);
		$counter->getData();
		$counter=$counter->getAux_cnt();
		$counter=$counter['counter'];
	}else{
		$counter='Todos';
	}

	if($_POST['selStatus']){
		$status=$global_appliedBonusStatus[$_POST['selStatus']];
	}else{
		$status='Todos';
	}
	$aux=array('col1'=>'<b>Asesor de Venta:</b>',
			   'col2'=>$counter,
			   'col3'=>'<b>Estado:</b>',
			   'col4'=>$status);
	array_push($parameters, $aux);

	if($_POST['txtBeginDate'] && $_POST['txtEndDate']){
		$aux=array('col1'=>'<b>Fecha Desde:</b>',
			   'col2'=>$_POST['txtBeginDate'],
			   'col3'=>'<b>Fecha Hasta:</b>',
			   'col4'=>$_POST['txtEndDate']);
		array_push($parameters, $aux);
	}

	if($_POST['selCounter']){ //Report for a specific counter
		$misc['type']='COUNTER';
		$misc['identifier']=$_POST['selCounter'];
	}else{ //Report for a whole branch
		$misc['type']='BRANCH';
		$misc['identifier']=$_POST['selBranch'];
	}
}else{ //FILTERS BY CARD NUMBER
	$aux=array('col1'=>'<b># de Tarjeta:</b>',
			   'col2'=>$_POST['txtCardNumber'],
			   'col3'=>'',
			   'col4'=>'');
	array_push($parameters, $aux);

	$misc['type']='CARD';
	$misc['identifier']=$_POST['txtCardNumber'];
}

$data_list=AppliedBonus::getBonusForReport($db, $misc, $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selStatus']);

if(is_array($data_list)){
	foreach($data_list as &$d){
		$d['status_apb']=$global_appliedBonusStatus[$d['status_apb']];
	}
}

$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
$title = "<table>
				<tr>
					<td colspan=6>".$date."</td>
				</tr>
				<tr></tr>
				<tr>
					<th></th>
					<th><b>REPORTE DE INCENTIVOS</b></th>
				 </tr>
				<tr></tr>
		 </table>";
echo $title;

$filters = "<table>
				<tr>
					<th></th>
					<th><b>Filtros Utilizados.-</b></th>
				</tr>
		    </table>";
echo $filters;

$parameters_table="<table>";
foreach($parameters as $p){
	$parameters_table.="<tr>
							<td>".$p['col1']."</td>
							<td>".$p['col2']."</td>
							<td>".$p['col3']."</td>
							<td>".$p['col4']."</td>
						</tr>";
}
$parameters_table.="</table><br>";
echo $parameters_table;


if(is_array($data_list) && sizeof($data_list)>0){
	$data_table="<table>
					<tr>
						<td><b>Ciudad</b></td>
						<td><b>Sucursal de Comercializador</b></td>
						<td><b>Tarjeta</b></td>
						<td><b>Producto</b></td>
						<td><b>Cliente</b></td>
						<td><b>Valor de la Tarjeta</b></td>
						<td><b>Valor del Incentivo</b></td>
						<td><b>Estado</b></td>
					</tr>";

	foreach($data_list as $d){
		$data_table.="<tr>
						<td>".$d['name_cit']."</td>
						<td>".$d['name_dea']."</td>
						<td>".$d['card_number_sal']."</td>
						<td>".$d['name_pbl']."</td>
						<td>".$d['customer_name']."</td>
						<td>".$d['total_sal']."</td>
						<td>".$d['total_amount_apb']."</td>
						<td>".$d['status_apb']."</td>
					  </tr>";
	}

	$data_table.="</table>";
}else{
	$data_table="<table><tr><td>".html_entity_decode('No se tienen incentivos registrados de acuerdo a los par&aacute;metros ingresados.')."</td></tr></table>";
}

echo $data_table;
?>