<?php
/**
 * File:  pBonusToPayXLS
 * Author: Patricio Astudillo
 * Creation Date: 04-abr-2011, 11:30:51
 * Description:
 * Last Modified: 04-abr-2011, 11:30:51
 * Modified by:
 */

	Request::setString('rdType');
	Request::setInteger('selCountry');
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selCounter');
    Request::setString('txtBeginDate');	
	Request::setString('txtEndDate');	
	global $date;

	$date = html_entity_decode($date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );
//	$bonus_list = AppliedBonus::getBonusToPayForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter);
  	$bonus_list = AppliedBonus::getBonusToPayForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter, $txtBeginDate, $txtEndDate);


	if(is_array($bonus_list)) {
		$total_to_pay = 0;
		$total_to_refund = 0;
		$total_to_authorized = 0;
		$title = "<table>
			<tr>
				<td colspan='9'><b>REPORTE DE INCENTIVOS POR PAGAR</b></td>
			</tr>
			<tr>
				<td colspan='9'>$date</td>
			</tr>
			<tr>&nbsp;</tr>
			<tr>&nbsp;</tr>";

		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		foreach($bonus_list as $key=>&$item){
			$invoice_discounts = ($item['invoice_discounts']) / 100;
			$dealer_comission_percentage = $item['comision_prcg_inv'] / 100;

			$item['base_amount'] = $item['total_sal'] - ($item['total_sal'] * $invoice_discounts);
			$item['base_amount'] = $item['base_amount'] - ($item['base_amount'] * $dealer_comission_percentage);
			$item['base_amount'] = number_format($item['base_amount'], 2);

			if ($item['status_apb'] == 'REFUNDED'){
				$total_to_refund += $item['pending_amount_apb'];
				$item['status_apb'] = $global_appliedBonusStatus[$item['status_apb']];

				//Add negative sign for report
				$item['pending_amount_apb']='-'.$item['pending_amount_apb'];
			}else if($item['ondate_apb'] == 'NO' && $item['authorized_apb'] == NULL ){
				//$total_to_authorized += $item['pending_amount_apb'];
				$item['status_apb'] = 'Por Autorizar';
				//$total_to_pay += $item['pending_amount_apb'];
				unset($bonus_list[$key]);
			}else{
				$item['status_apb'] = $global_appliedBonusStatus[$item['status_apb']];
				$total_to_pay += $item['pending_amount_apb'];
			}
								
			$manager_name = $item['name_man'];
			$name_dea = $item['name_dea'];
		}
		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		
		/* PRINT THE EXCEL */
		foreach($bonus_list as $item){
			$records_info .= "<tr>
								<td>{$item['card_number_sal']}</td>
								<td>{$item['number_inv']}</td>
								<td>{$item['name_pbl']}</td>
								<td>{$item['base_amount']}</td>
								<td>{$item['pending_amount_apb']}</td>
								<td>{$item['status_apb']}</td>
								<td>{$item['counter']}</td>
								<td>{$item['responsible_name']}</td>";
			if(!$selBranch) $records_info .= " <td>{$item['name_dea']}</td>";
			if(!$selManager) $records_info .= " <td>{$item['name_man']}</td>";
			$records_info .= "</tr>";
		}
		/* PRINT THE EXCEL */	
		
		$total = 0;
		$total = $total_to_pay - $total_to_refund - $total_to_authorized;
		if($selManager) $title .= "<tr>
									<td><b>Representante: </b></td>
									<td colspan='8'>$manager_name</td>
								</tr>";
		if($selBranch) $title .= "<tr>
									<td><b>Sucursal de comercializador: </b></td>
									<td colspan='8'>$name_dea</td>
								</tr>";

		$title .= "<tr>
					<td><b>Total a Favor: </b></td>
					<td align='left' colspan='8'>$total_to_pay</td>
				  </tr>
		          <tr>
					<td><b>Total de reembolsos: </b></td>
					<td align='left' colspan='8'>$total_to_refund</td>
				  </tr>
					<tr>
					<td><b>Total disponible para pagar: </b></td>
					<td align='left' colspan='8'>$total</td>
				  </tr>
			</table><br /><br />";


		$records = "<table>
						<tr>
							<td colspan='9'>
								<b>Listado de Incentivos</b>
							</td>
						</tr>
						<tr>
							<td><b># Tarjeta</b></td>
							<td><b># Factura</b></td>
							<td><b>Nombre del Producto</b></td>
							<td><b>Valor Neto</b></td>
							<td><b>Valor del Incentivo</b></td>
							<td><b>Estado</b></td>
							<td><b>Counter</b></td>
							<td><b>Responsable</b></td>";
		if(!$selBranch) $records .= " <td><b>Sucursal de comercializador</b></td>";
		if(!$selManager) $records .= " <td><b>Representante</b></td>";
							
		$records .="	</tr>
						$records_info
					</table>";
	} else {
		$title = '';
		$records = 'No existen datos para los filtros seleccionados.';
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Incentivos_por_Pagar.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $title;
	echo $records;
?>
