<?php
/**
 * File:  pAuthorizedBonusXLS
 * Author: Patricio Astudillo
 * Creation Date: 05-abr-2011, 13:23:20
 * Description:
 * Last Modified: 05-abr-2011, 13:23:20
 * Modified by:
 */

	Request::setString('rdType');
	Request::setInteger('selCountry');
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selCounter');
	Request::setString('selStatus');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	global $date;
   
	$date = html_entity_decode($date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );
	$bonus_list = AppliedBonus::getAuthorizedBonusForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter, $selStatus, $txtBeginDate, $txtEndDate);

	if(is_array($bonus_list)) {
		$total_to_pay = 0;
		$title = "<table>
			<tr>
				<td colspan='11'><b>REPORTE DE INCENTIVOS POR PAGAR</b></td>
			</tr>
			<tr>
				<td colspan='11'>$date</td>
			</tr>
			<tr>&nbsp;</tr>
			<tr>&nbsp;</tr>";

		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		foreach($bonus_list as &$item){
			$invoice_discounts = ($item['invoice_discounts']) / 100;
			$dealer_comission_percentage = $item['comision_prcg_inv'] / 100;

			$item['base_amount'] = $item['total_sal'] - ($item['total_sal'] * $invoice_discounts);
			$item['base_amount'] = $item['base_amount'] - ($item['base_amount'] * $dealer_comission_percentage);
			$item['base_amount'] = number_format($item['base_amount'], 2);

			$records_info .= "<tr>
								<td>{$item['card_number_sal']}</td>
								<td>{$item['number_inv']}</td>
								<td>{$item['due_date_inv']}</td>
								<td>{$item['date_pay']}</td>
					  			<td>{$item['notPaid_days']}</td>
								<td>{$item['base_amount']}</td>
								<td>{$item['total_amount_apb']}</td>
								<td>{$item['authorized_apb']}</td>
								<td>{$item['authorized_by']}</td>
								<td>{$item['authorized_date_apb']}</td>
								<td>{$item['observations_apb']}</td>
								<td>{$item['responsible_name']}</td>";
								
			if(!$selBranch) $records_info .= " <td>{$item['name_dea']}</td>";
			if(!$selManager) $records_info .= " <td>{$item['name_man']}</td>";
			$records_info .= "</tr>";

			$manager_name = $item['name_man'];
			$name_dea = $item['name_dea'];
		}
		/* CALCULATIONS FOR THE SALE'S NET VALUE */

		if($selManager) $title .= "<tr>
									<td><b>Representante: </b></td>
									<td colspan='8'>$manager_name</td>
								</tr>";
		if($selBranch) $title .= "<tr>
									<td><b>Sucursal de comercializador: </b></td>
									<td colspan='8'>$name_dea</td>
								</tr>";

		$title .= "
			</table><br /><br />";


		$records = "<table>
						<tr>
							<td colspan='11'>
								<b>Listado de Incentivos</b>
							</td>
						</tr>
						<tr>
							<td><b># Tarjeta</b></td>
							<td><b># Factura</b></td>
							<td><b>Fecha Maxima de Pago</b></td>
							<td><b>Fecha Pago</b></td>
							<td><b>D&iacute;as Impago</b></td>
							<td><b>Valor Neto</b></td>
							<td><b>Valor del Incentivo</b></td>
							<td><b>Estado</b></td>
							<td><b>Autorizado Por</b></td>
							<td><b>Fecha de Autorizaci&oacute;n</b></td>
							<td><b>Motivo</b></td>
							<td><b>Responsable</b></td>";

		if(!$selBranch) $records .= " <td><b>Sucursal de comercializador</b></td>";
		if(!$selManager) $records .= " <td><b>Representante</b></td>";

		$records .="	</tr>
						$records_info
					</table>";
	} else {
		$title = '';
		$records = 'No existen datos para los filtros seleccionados.';
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Incentivos_Autorizados.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $title;
	echo $records;
?>
