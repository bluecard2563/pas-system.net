<?php
/**
 * File:  pPaidBonusXLS
 * Author: Patricio Astudillo
 * Creation Date: 05-abr-2011, 11:11:28
 * Description:
 * Last Modified: 05-abr-2011, 11:11:28
 * Modified by:
 */

	Request::setString('rdType');
	Request::setInteger('selCountry');
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selCounter');
    Request::setString('txtBeginDate');	
	Request::setString('txtEndDate');
	global $date;
	$date = html_entity_decode($date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );
	$bonus_list = AppliedBonus::getBonusPaidForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter, $txtBeginDate, $txtEndDate);

	if(is_array($bonus_list)) {
		$total_to_pay = 0;
		$title = "<table>
			<tr>
				<td colspan='9'><b>REPORTE DE INCENTIVOS ENTREGADOS</b></td>
			</tr>
			<tr>
				<td colspan='9'>$date</td>
			</tr>
			<tr>&nbsp;</tr>
			<tr>&nbsp;</tr>";

		foreach($bonus_list as &$item){
			$records_info .= "<tr>
								<td>{$item['liquidation_number']}</td>
								<td>{$item['date_lqn']}</td>
								<td>{$item['name_dea']}</td>
								<td>{$item['counter']}</td>
								<td>{$item['paid_amount']}</td>
								<td>{$item['pending_amount_apb']}</td>
							</tr>";
			
			$manager_name = $item['name_man'];
			$name_dea = $item['name_dea'];
			$counter_name = $item['counter'];
			$total_to_pay = $item['pending_amount_apb'];
		}

		if($selManager) $title .= "<tr>
									<td><b>Representante: </b></td>
									<td colspan='8'>$manager_name</td>
								</tr>";
		if($selBranch) $title .= "<tr>
									<td><b>Sucursal de Comercializador: </b></td>
									<td colspan='8'>$name_dea</td>
								</tr>";

		$title .= "<tr>
					<td><b>Total a Pagar: </b></td>
					<td colspan='8'>$total_to_pay</td>
				</tr>
			</table><br /><br />";


		$records = "<table>
						<tr>
							<td colspan='9'>
								<b>Listado de Incentivos</b>
							</td>
						</tr>
						<tr>
							<td><b># Liquidaci&oacute;n</b></td>
							<td><b>Fecha de Liquidaci&oacute;n</b></td>
							<td><b>Sucursal de Comercializador</b></td>
							<td><b>Counter</b></td>
							<td><b>Valor Entregado</b></td>
							<td><b>Valor Pendiente</b></td>";

		$records .="	</tr>
						$records_info
					</table>";
	} else {
		$title = '';
		$records = 'No existen datos para los filtros seleccionados.';
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Incentivos_Entregados.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $title;
	echo $records;
?>