<?php
/**
 * File:  pPaidBonusPDF
 * Author: Patricio Astudillo
 * Creation Date: 05-abr-2011, 11:10:57
 * Description:
 * Last Modified: 05-abr-2011, 11:10:57
 * Modified by:
 */

	Request::setString('rdType');
	Request::setInteger('selCountry');
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selCounter');
    Request::setString('txtBeginDate');	
	Request::setString('txtEndDate');
	global $date;
	$date = html_entity_decode($date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year'].' a las '.date("h:i:s a", time()) );

	$bonus_list = AppliedBonus::getBonusPaidForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter, $txtBeginDate, $txtEndDate);
	include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

	$pdf->ezText('<b>'.("REPORTE DE INCENTIVOS ENTREGADOS").'</b>', 12, array('justification'=>'center'));
	$pdf->ezText($date, 9, array('justification'=>'center'));
	$pdf->ezSetDy(-10);

	if(is_array($bonus_list)) {
		$total_to_pay = 0;
		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		foreach($bonus_list as &$item){
			$manager_name = $item['name_man'];
			$name_dea = $item['name_dea'];
			$counter_name = $item['counter'];
			$total_to_pay = $item['pending_amount_apb'];
		}
		/* CALCULATIONS FOR THE SALE'S NET VALUE */


		if($selManager) $pdf->ezText('<b>Representante: </b>'.$manager_name, 11, array('justification'=>'left'));
		if($selBranch) $pdf->ezText('<b>Sucursal de Comercializador: </b>'.$name_dea, 11, array('justification'=>'left'));
		$pdf->ezText('<b>Pendiente por pagar: </b>$'.number_format($total_to_pay, 2), 11, array('justification'=>'left'));
		$pdf->ezSetDy(-20);

		$titles=array('liquidation_number'=>  html_entity_decode('<b># Liquidaci&oacute;n</b>'),
					  'date_lqn'=>html_entity_decode('<b>Fecha de Liquidaci&oacute;n</b>'),
					  'name_dea'=>utf8_decode('<b>Sucursal de Comercializador</b>'),
					  'counter'=>utf8_decode('<b>Counter</b>'),
					  'paid_amount'=>utf8_decode('<b>Valor Entregado</b>'),
					  'pending_amount_apb'=>utf8_decode('<b>Valor Pendiente</b>'));

		$pdf->ezTable($bonus_list,
						$titles,
						'<b>Listado de Incentivos</b>',
						array('showHeadings'=>1,
							'shaded'=>1,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
									'liquidation_number'=>array('justification'=>'center','width'=>80),
									'date_lqn'=>array('justification'=>'center','width'=>70),
									'name_dea'=>array('justification'=>'center','width'=>100),
									'counter'=>array('justification'=>'center','width'=>90),
									'paid_amount'=>array('justification'=>'center','width'=>60),
									'pending_amount_apb'=>array('justification'=>'center','width'=>60))
					  ));

			$pdf->ezSetDy(-20);
	} else {
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}

	$pdf->ezStream();
?>
