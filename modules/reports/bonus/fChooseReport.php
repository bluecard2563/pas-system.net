<?php
/*
 * File: fChooseReport.php
 * Author: Nicolas Flores
 * Creation Date: 29/06/2010, 16:36:58
 * Modified By: Patricio Astudillo 
 */

$zoneWithBonusList=Zone::getZonesWithAppliedBonus($db);
$today=date('d/m/Y');

$smarty->register('zoneWithBonusList,today');
$smarty->display();
?>
