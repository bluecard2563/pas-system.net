<?php
/**
 * File:  pAuthorizedBonusPDF
 * Author: Patricio Astudillo
 * Creation Date: 05-abr-2011, 13:23:09
 * Description:
 * Last Modified: 05-abr-2011, 13:23:09
 * Modified by:
 */

	Request::setString('rdType');
	Request::setInteger('selCountry');
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selCounter');
	Request::setString('selStatus');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
    
	$bonus_list = AppliedBonus::getAuthorizedBonusForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter, $selStatus, $txtBeginDate, $txtEndDate);
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

	$pdf->ezText('<b>'.("REPORTE DE INCENTIVOS POR PAGAR").'</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);

	if(is_array($bonus_list)) {
		$total_authorized = 0;
		$total_denied = 0;
		$total_to_pay = 0;
		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		foreach($bonus_list as &$item){
			$invoice_discounts = ($item['invoice_discounts']) / 100;
			$dealer_comission_percentage = $item['comision_prcg_inv'] / 100;


			$item['base_amount'] = $item['total_sal'] - ($item['total_sal'] * $invoice_discounts);
			$item['base_amount'] = $item['base_amount'] - ($item['base_amount'] * $dealer_comission_percentage);
			$item['base_amount'] = number_format($item['base_amount'], 2);

			if ($item['authorized_apb'] == 'Autorizado'){
				$total_authorized += $item['total_amount_apb'];
			}else{
				$total_denied += $item['total_amount_apb'];
			}
			$manager_name = $item['name_man'];
			$name_dea = $item['name_dea'];
		}
		/* CALCULATIONS FOR THE SALE'S NET VALUE */


		if($selManager) $pdf->ezText('<b>Representante: </b>'.$manager_name, 11, array('justification'=>'left'));
		if($selBranch) $pdf->ezText('<b>Sucursal de Comercializador: </b>'.$name_dea, 11, array('justification'=>'left'));
		$pdf->ezText('<b>Total de incentivos autorizados: </b>$'.number_format($total_authorized, 2), 11, array('justification'=>'left'));
		$pdf->ezText('<b>Total de incentivos negados: </b>$'.number_format($total_denied, 2), 11, array('justification'=>'left'));

		$pdf->ezSetDy(-10);

		$titles=array('card_number_sal'=>('<b># Tarjeta</b>'),
					  'number_inv'=>('<b># Factura</b>'),
					  'due_date_inv'=>('<b>Fecha Maxima Pago</b>'),
					  'date_pay'=>('<b>Fecha pago</b>'),
					  'notPaid_days'=>  html_entity_decode('<b>D&iacute;as Impago</b>'),
					  'base_amount'=>utf8_decode('<b>Valor Neto</b>'),
					  'total_amount_apb'=>('<b>Valor del Incentivo</b>'),
					  'authorized_apb'=>'<b>Estado</b>',
					  'authorized_by'=>('<b>Autorizado Por</b>'),
					  'authorized_date_apb'=>html_entity_decode('<b>Fecha de Autorizaci&oacute;n</b>'),
					  'observations_apb'=>('<b>Motivo</b>'),
					  'responsible_name'=>('<b>Responsable</b>'));

		if(!$selBranch) $titles['name_dea'] = '<b>Sucursal de comercializador</b>';
		if(!$selManager) $titles['name_man'] =  '<b>Representante</b>';

		$pdf->ezTable($bonus_list,
						$titles,
						'<b>LISTADO DE INCENTIVOS</b>',
						array('showHeadings'=>1,
							'shaded'=>1,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 10,
							'cols'=>array(
									'card_number_sal'=>array('justification'=>'center','width'=>40),
									'number_inv'=>array('justification'=>'center','width'=>40),
									'due_date_inv'=>array('justification'=>'center','width'=>70),
								    'date_pay'=>array('justification'=>'center','width'=>70),
									'notPaid_days'=>array('justification'=>'center','width'=>40),
								'base_amount'=>array('justification'=>'center','width'=>40),
									'total_amount_apb'=>array('justification'=>'center','width'=>50),
									'authorized_apb'=>array('justification'=>'center','width'=>50),									
									'authorized_by'=>array('justification'=>'center','width'=>60),
									'authorized_date_apb'=>array('justification'=>'center','width'=>70),
									'observations_apb'=>array('justification'=>'center','width'=>90),
									'responsible_name'=>array('justification'=>'center','width'=>60),
									'name_dea'=>array('justification'=>'center','width'=>90),
									'name_man'=>array('justification'=>'center','width'=>90))
					  ));

			$pdf->ezSetDy(-20);
	} else {
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}

	$pdf->ezStream();	
?>
