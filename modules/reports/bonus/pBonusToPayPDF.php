<?php
/**
 * File:  pBonusToPayPDF
 * Author: Patricio Astudillo
 * Creation Date: 04-abr-2011, 11:15:42
 * Description:
 * Last Modified: 04-abr-2011, 11:15:42
 * Modified by:
 */
	
	Request::setString('rdType');
	Request::setInteger('selCountry');
	Request::setInteger('selCity');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selCounter');	
	Request::setString('txtBeginDate');	
	Request::setString('txtEndDate');
    
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	$bonus_list = AppliedBonus::getBonusToPayForReport($db, $rdType, $selCountry, $selCity, $selManager, $selDealer, $selBranch, $selCounter, $txtBeginDate, $txtEndDate);

	$pdf->ezText('<b>'.("REPORTE DE INCENTIVOS POR PAGAR").'</b>', 12, array('justification'=>'center'));
	$pdf->ezText($date, 9, array('justification'=>'center'));
	$pdf->ezSetDy(-10);

	if(is_array($bonus_list)) {
		$total_to_pay = 0;
		$total_to_refund = 0;
		$total_to_authorized = 0;
		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		foreach($bonus_list as $key=>&$item){
			$invoice_discounts = ($item['invoice_discounts']) / 100;
			$dealer_comission_percentage = $item['comision_prcg_inv'] / 100;

			$item['base_amount'] = $item['total_sal'] - ($item['total_sal'] * $invoice_discounts);
			$item['base_amount'] = $item['base_amount'] - ($item['base_amount'] * $dealer_comission_percentage);
			$item['base_amount'] = number_format($item['base_amount'], 2);
			if ($item['status_apb'] == 'REFUNDED'){
				$total_to_refund += $item['pending_amount_apb'];
				$item['status_apb'] = $global_appliedBonusStatus[$item['status_apb']];

				//Add negative sign for report
				$item['pending_amount_apb']='-'.$item['pending_amount_apb'];
			}else if($item['ondate_apb'] == 'NO' && $item['authorized_apb'] == NULL ){
				//$total_to_authorized += $item['pending_amount_apb'];
				$item['status_apb'] = 'Por Autorizar';
				//$total_to_pay += $item['pending_amount_apb'];
				unset($bonus_list[$key]);
			}else{
				$item['status_apb'] = $global_appliedBonusStatus[$item['status_apb']];
				$total_to_pay += $item['pending_amount_apb'];
			}
			
			$manager_name = $item['name_man'];
			$name_dea = $item['name_dea'];
		}
		/* CALCULATIONS FOR THE SALE'S NET VALUE */
		$total = 0;
		$total = $total_to_pay - $total_to_refund - $total_to_authorized;
		
		if($selManager) $pdf->ezText('<b>Representante: </b>'.$manager_name, 11, array('justification'=>'left'));
		if($selBranch) $pdf->ezText('<b>Sucursal de Comercializador: </b>'.$name_dea, 11, array('justification'=>'left'));
		$pdf->ezText('<b>Total a favor: </b>$'.number_format($total_to_pay, 2), 11, array('justification'=>'left'));
		$pdf->ezText('<b>Total de reembolsos: </b>$'.number_format($total_to_refund, 2), 11, array('justification'=>'left'));
		$pdf->ezText('<b>Total disponible para pagar: </b>$'.number_format($total, 2), 11, array('justification'=>'left'));
		$pdf->ezSetDy(-20);
		
		$titles=array('card_number_sal'=>utf8_decode('<b># Tarjeta</b>'),
					  'number_inv'=>utf8_decode('<b># Factura</b>'),
					  'base_amount'=>utf8_decode('<b>Valor Neto</b>'),
					  'total_amount_apb'=>utf8_decode('<b>Valor del Incentivo</b>'),
					  'pending_amount_apb'=>utf8_decode('<b>Valor Pendiente</b>'),
					  'status_apb'=>utf8_decode('<b>Estado</b>'),
					  'counter'=>utf8_decode('<b>Counter</b>'),
		              'responsible_name'=>utf8_decode('<b>Responsable</b>'));

		if(!$selBranch) $titles['name_dea'] = '<b>Sucursal de comercializador</b>';
		if(!$selManager) $titles['name_man'] =  '<b>Representante</b>';
		
		$pdf->ezTable($bonus_list,
						$titles,
						'<b>Listado de Incentivos</b>',
						array('showHeadings'=>1,
							'shaded'=>1,
							'showLines'=>2,
							'xPos'=>'center',
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8,
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
									'card_number_sal'=>array('justification'=>'center','width'=>50),
									'number_inv'=>array('justification'=>'center','width'=>50),
									'base_amount'=>array('justification'=>'center','width'=>50),
									'total_amount_apb'=>array('justification'=>'center','width'=>50),
									'pending_amount_apb'=>array('justification'=>'center','width'=>50),
								    'status_apb'=>array('justification'=>'center','width'=>60),
									'counter'=>array('justification'=>'center','width'=>90),
								    'responsible_name'=>array('justification'=>'center','width'=>90),
									'name_dea'=>array('justification'=>'center','width'=>110),
									'name_man'=>array('justification'=>'center','width'=>90))
					  ));
		
			$pdf->ezSetDy(-20);
	} else {
		$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados.'), 10, array('justification' =>'center'));
	}

	$pdf->ezStream();	
?>
