<?php
/**
 * File: dealerFixedComissionCode
 * Author: Patricio Astudillo
 * Creation Date: 31-ene-2013
 * Last Modified: 31-ene-2013
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('selManager');
	Request::setInteger('selResponsible');
	Request::setInteger('selDealer');
	Request::setString('selBranch');
	
	$misc['serial_mbc'] = $selManager;
	$misc['serial_usr'] = $selResponsible;
	$misc['serial_dea'] = $selDealer;
	
	//*********************** GET HEADERS INFO FOR FILE *************************
	$manager_name = new Manager($db, $selManager);
	$manager_name -> getData();
	$manager_name = $manager_name->getName_man();
	
	if($selResponsible):
		$resp_name = new User($db, $selResponsible);
		$resp_name -> getData();
		$resp_name = $resp_name->getFirstname_usr().' '.$resp_name->getLastname_usr();
	else:
		$resp_name = 'Todos';
	endif;
	
	if($selDealer):
		$dealer_name = new Dealer($db, $selDealer);
		$dealer_name -> getData();
		$dealer_name = $dealer_name->getName_dea();
	else:
		$dealer_name = 'Todos';
	endif;
	
	if($selBranch):
		$branch_name = new Dealer($db, $selBranch);
		$branch_name -> getData();
		$branch_name = $branch_name->getName_dea();
	else:
		$branch_name = 'Todos';
	endif;
	
	
	//******************** MAKE FINAL ARRAY GROUPED BY DEALER *******************
	$apComission = new AppliedComission($db);
	$comissionsData = $apComission->getComissionForReport($selComissionsTo, $selBranch, $selType, $txtStartDate, $txtEndDate, NULL, $misc);

	if($comissionsData):
		$current_dea = NULL;
		$final_info_array = array();
		$zeroValue = 0;
		$totalSold = 0;
		$totalRefunded = 0;
		$effective_sale_total = 0;
		
		foreach($comissionsData as &$c){
			if($current_dea != $c['serial_dea']):
				//FORMAT VALUES IN ARRAY
				if(is_array($final_info_array[$current_dea]['detailed_info'])){
					foreach($final_info_array[$current_dea]['detailed_info'] as &$details){
						$details['base_value'] = number_format($details['base_value'], 2, '.', '');
						$details['fixed_comission_percentage'] = number_format($details['fixed_comission_percentage'], 2, '.', '');
						$details['value_dea_com'] = number_format($details['value_dea_com'], 2, '.', '');
					}
					
					$final_info_array[$current_dea]['to_pay'] = number_format($final_info_array[$current_dea]['to_pay'], 2, '.', '');;
					$final_info_array[$current_dea]['to_refund'] = number_format($final_info_array[$current_dea]['to_refund'], 2, '.', '');;
					$final_info_array[$current_dea]['efective'] = number_format($final_info_array[$current_dea]['efective'], 2, '.', '');;
				}				
				
				$current_dea = $c['serial_dea'];
				$final_info_array[$c['serial_dea']]['dealer'] = $c['name_dea'];
				$final_info_array[$c['serial_dea']]['to_pay'] = 0;
				$final_info_array[$c['serial_dea']]['to_refund'] = 0;
				$final_info_array[$c['serial_dea']]['efective'] = 0;
				$final_info_array[$c['serial_dea']]['detailed_info'] = array();
			endif;
			
			if ($c['type_com'] == 'IN') {
				//VALUES FOR SINGLE LINE
				$c['to_comission'] = $c['value_dea_com'];
				$c['toDiscount'] = $zeroValue;
				
				//TOTALIZE NUMBERS PER DEALER AND PER GROUP
				$final_info_array[$c['serial_dea']]['to_pay'] += $c['value_dea_com'];
				$final_info_array[$c['serial_dea']]['efective'] += $c['base_value'];
				$totalSold += $c['value_dea_com'];
				$effective_sale_total += $c['base_value'];
				
			} else {
				//VALUES FOR SINGLE LINE
				$c['to_comission'] = $zeroValue;
				$c['toDiscount'] = $c['value_dea_com'];
				
				//TOTALIZE NUMBERS PER DEALER AND PER GROUP
				$final_info_array[$c['serial_dea']]['to_refund'] += $c['value_dea_com'];
				$final_info_array[$c['serial_dea']]['efective'] -= $c['base_value'];
				$totalRefunded += $c['value_dea_com'];
				$effective_sale_total -= $c['base_value'];
			}
			
			//FORMAT NUMBERS ON THE GO FOR SPEED THINGS UP
			$final_info_array[$c['serial_dea']]['to_pay'] = number_format($final_info_array[$c['serial_dea']]['to_pay'], 2, '.', '');
			$final_info_array[$c['serial_dea']]['to_refund'] = number_format($final_info_array[$c['serial_dea']]['to_refund'], 2, '.', '');
			$final_info_array[$c['serial_dea']]['efective'] = number_format($final_info_array[$c['serial_dea']]['efective'], 2, '.', '');
			$final_info_array[$c['serial_dea']]['base_value'] = number_format($final_info_array[$c['serial_dea']]['base_value'], 2, '.', '');
			$final_info_array[$c['serial_dea']]['fixed_comission_percentage'] = number_format($final_info_array[$c['serial_dea']]['fixed_comission_percentage'], 2, '.', '');
			$final_info_array[$c['serial_dea']]['value_dea_com'] = number_format($final_info_array[$c['serial_dea']]['value_dea_com'], 2, '.', '');
			

			//ADD LINE TO MAIN ARRAY
			array_push($final_info_array[$c['serial_dea']]['detailed_info'], $c);
		}
		
		$effective_sale_total = number_format($effective_sale_total, 2, '.', '');
	endif;
?>
