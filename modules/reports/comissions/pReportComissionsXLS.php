<?php
/*
  Document   : pReportComissionsXLS
  Created on : 08-jun-2010, 15:16:31
  Author     : Nicolas
  Description:
  Generate a XLS report of comissions
 */

	Request::setString('selComissionsTo');
	Request::setString('selType');
	Request::setString('txtStartDate');
	Request::setString('txtEndDate');
	if (!$selComissionsTo):
		http_redirect('modules/reports/comissions/fSelectComissionsReport');
	endif;
	
	if($selComissionsTo != 'DEALER'){
		switch ($selComissionsTo) {
			case 'MANAGER':
				Request::setString('selManager');
				$serial = $selManager;
				break;
			case 'RESPONSIBLE':
				Request::setString('selResponsible');
				$serial = $selResponsible;
				break;
			case 'DEALER':
				Request::setString('selBranch');
				$serial = $selBranch;
				break;
			case 'SUBMANAGER':
				Request::setString('selDealer');
				$serial = $selDealer;
				break;
		}

		$apComission = new AppliedComission($db);
		$comissionsData = $apComission->getComissionForReport($selComissionsTo, $serial, $selType, $txtStartDate, $txtEndDate);

		if ($comissionsData) {
			$totalSold = 0;
			$totalRefunded = 0;
			$comissionToPay = 0;

			switch ($selComissionsTo) {
				case 'MANAGER':
					$manager = new Manager($db, $serial);
					$manager->getData();
					$payTo = $manager->getName_man();
					$payToId = $manager->getDocument_man();
					$payToPhone = $manager->getPhone_man();
					$payToAddress = $manager->getAddress_man();

					if ($comissionsData) {
						foreach ($comissionsData as &$s) {
							$s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
							$s['comissionPercentage'] = ($s['value_mbc_com'] * 100) / $s['net_total_sal'];
							$s['comissionValue'] = $s['value_mbc_com'];
						}
					}
					break;
				case 'RESPONSIBLE':
					$user = new User($db, $serial);
					$user->getData();
					$payTo = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
					$payToId = $user->getDocument_usr();
					$payToPhone = $user->getPhone_usr();
					$payToAddress = $user->getAddress_usr();

					if ($comissionsData) {
						foreach ($comissionsData as &$s) {
							$s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
							if ($s['net_total_sal'] > 0) {
								$s['comissionPercentage'] = ($s['value_ubd_com'] * 100) / $s['net_total_sal'];
							}
							$s['comissionValue'] = $s['value_ubd_com'];
						}
					}
					break;
				case 'DEALER':
				case 'SUBMANAGER':
					$dealer = new Dealer($db, $serial);
					if ($dealer->getData()) {
						$payTo = $dealer->getName_dea();
						$payToId = $dealer->getId_dea();
						$payToPhone = $dealer->getPhone1_dea();
						$payToAddress = $dealer->getAddress_dea();
					} else {
						$payTo = 'N/A';
						$payToId = 'N/A';
						$payToPhone = 'N/A';
						$payToAddress = 'N/A';
					}

					if ($comissionsData) {
						foreach ($comissionsData as &$s) {
							$s['net_total_sal'] = $s['base_value'];
							$s['comissionPercentage'] = ($s['value_dea_com'] * 100) / $s['net_total_sal'];
							$s['comissionValue'] = $s['value_dea_com'];
						}
					}
					break;
			}
		}

		$comissionToPay = $totalSold - $totalRefunded;
		$comissionToPay = number_format($comissionToPay, 2, '.', '');
		$totalSold = number_format($totalSold, 2, '.', '');
		$totalRefunded = number_format($totalRefunded, 2, '.', '');
		$effective_sale_total = 0;

		if ($comissionsData) {
			foreach ($comissionsData as &$s) {
				if ($s['type_com'] == 'IN') {
					$totalSold += $s['comissionValue'];
					$s['to_comission'] = $s['comissionValue'];
					$s['toDiscount'] = $zeroValue;
					$effective_sale_total += $s['net_total_sal'];
				} else {
					$totalRefunded += $s['comissionValue'];
					$s['to_comission'] = $zeroValue;
					$s['toDiscount'] = $s['comissionValue'];
					$effective_sale_total -= $s['net_total_sal'];
				}

				//style some stuff:
				$s['net_total_sal'] = number_format($s['net_total_sal'], 2, '.', '');
				$s['fixed_comission_percentage'] = number_format($s['fixed_comission_percentage'], 2, '.', '');
				$s['comissionValue'] = number_format($s['comissionValue'], 2, '.', '');
			}
		}

		$effective_sale_total = number_format($effective_sale_total, 2, '.', '');

		
		/************************ DISPLAY DATA ********************/
		$headerValues = "<table border='1'>";
		$headerValues .= "<tr>
							<td><b>Pagar a:</b></td>
							<td>" . $payTo . "</td>
							<td><b>RUC/CI:</b></td>
							<td>&nbsp;" . $payToId . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Fecha:</b></td>
							<td>" . date('d/m/Y') . "</td>
							<td><b>Tel&eacute;fono:</b></td>
							<td>&nbsp;" . $payToPhone . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Comisi&oacute;n a Favor:</b></td>
							<td>" . $totalSold . "</td>
							<td><b>Direcci&oacute;n:</b></td>
							<td>" . $payToAddress . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Comisi&oacute;n a Descontar:</b></td>
							<td>" . $totalRefunded . "</td>
							<td><b>Total Ventas Efectivas:</b></td>
							<td>" . $effective_sale_total . "</td>
						</tr>";

		$headerValues .= "<tr>
							<td><b>Valor Comisi&oacute;n:</b></td>
							<td>" . ($totalSold - $totalRefunded) . "</td>
						</tr>";

		if ($txtEndDate != '' && $txtStartDate != '') {
			$headerValues .= "<tr>
							<td><b>Fecha Desde:</b></td>
							<td>" . $txtStartDate . "</td>
							<td><b>Fecha Hasta:</b></td>
							<td>" . $txtEndDate . "</td>
						</tr>";
		}
		$headerValues .= "</table><br />";

		$data = "<table border='1'><tr>
					<td><b># Tarjeta</b></td>
					<td><b># Factura</b></td>
					<td><b>Apellido y Nombre</b></td>
					<td><b>Producto</b></td>
					<td><b>Tiempo</b></td>
					<td><b>Agencia</b></td>
					<td><b>Valor Neto</b></td>
					<td><b>% Comisi&ocuate;n</b></td>
					<td><b>Fecha</b></td>
					<td><b>Comisi&ocuate;n</b></td>
					<td><b>Descuento</b></td>
				</tr>";

		foreach ($comissionsData as $cd) {
			$data .= "<tr>
						<td>" . $cd['card_number_sal'] . "</td>
						<td>" . $cd['number_inv'] . "</td>
						<td>" . $cd['name_cus'] . "</td>
						<td>" . $cd['name_pbl'] . "</td>
						<td>" . $cd['days_sal'] . "</td>
						<td>" . $cd['name_dea'] . "</td>
						<td>" . $cd['net_total_sal'] . "</td>
						<td>" . $cd['fixed_comission_percentage'] . "</td>
						<td>" . $cd['report_date'] . "</td>
						<td>" . $cd['to_comission'] . "</td>
						<td>" . $cd['toDiscount'] . "</td>
					</tr>";
		}
		$data .= "	<tr>
						<td colspan='9'><b>Sub - totales</b></td>
						<td>" . $totalSold . "</td>
						<td>" . $totalRefunded . "</td>
					</tr>
					<tr>
						<td colspan='8'>&nbsp;</td>
						<td align='right'><b>TOTAL</b></td>
						<td colspan='2'> $" . ($totalSold - $totalRefunded) . "</td>
					</tr>
				</table>";
	}else{
		require_once(DOCUMENT_ROOT.'modules/reports/comissions/dealerFixedComissionCode.php');
		
		//*********************** FILTERS ON HEADER ***********************
		$headerValues = "<table border='1'>";
		$headerValues .= "<tr>
							<td><b>Fecha:</b></td>
							<td>" . date('d/m/Y') . "</td>
							<td><b>Representante:</b></td>
							<td>" . $manager_name . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Responsable:</b></td>
							<td>" . $resp_name . "</td>
							<td><b>Comercializador:</b></td>
							<td>" . $dealer_name . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Sucursal:</b></td>
							<td>" . $branch_name . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Comisi&oacute;n a Favor:</b></td>
							<td>" . $totalSold . "</td>
							<td><b>Comisi&oacute;n a Descontar:</b></td>
							<td>" . $totalRefunded . "</td>
						</tr>";
		$headerValues .= "<tr>
							<td><b>Total Ventas Efectivas:</b></td>
							<td>" . $effective_sale_total . "</td>
							<td><b>Valor Comisi&oacute;n:</b></td>
							<td>" . ($totalSold - $totalRefunded) . "</td>
						</tr>";

		if ($txtEndDate != '' && $txtStartDate != '') {
			$headerValues .= "<tr>
							<td><b>Fecha Desde:</b></td>
							<td>" . $txtStartDate . "</td>
							<td><b>Fecha Hasta:</b></td>
							<td>" . $txtEndDate . "</td>
						</tr>";
		}
		$headerValues .= "</table><br />";
		
		$data = "<table>";
		foreach($final_info_array as $group){
			$data .= "<tr>
						<td colspan='11'>
							<center><b>".strtoupper($group['dealer'])."</b></center>
						</td>
					</tr>
					<tr>
						<td><b># Tarjeta</b></td>
						<td><b># Factura</b></td>
						<td><b>Apellido y Nombre</b></td>
						<td><b>Producto</b></td>
						<td><b>Tiempo</b></td>
						<td><b>Agencia</b></td>
						<td><b>Valor Neto</b></td>
						<td><b>% Comisi&ocuate;n</b></td>
						<td><b>Fecha</b></td>
						<td><b>Comisi&ocuate;n</b></td>
						<td><b>Descuento</b></td>
					</tr>";
			
			foreach ($group['detailed_info'] as $cd) {
				$data .= "<tr>
							<td>" . $cd['card_number_sal'] . "</td>
							<td>" . $cd['number_inv'] . "</td>
							<td>" . $cd['name_cus'] . "</td>
							<td>" . $cd['name_pbl'] . "</td>
							<td>" . $cd['days_sal'] . "</td>
							<td>" . $cd['name_dea'] . "</td>
							<td>" . $cd['base_value'] . "</td>
							<td>" . $cd['fixed_comission_percentage'] . "</td>
							<td>" . $cd['report_date'] . "</td>
							<td>" . $cd['to_comission'] . "</td>
							<td>" . $cd['toDiscount'] . "</td>
						</tr>";
			}
			
			$data .= "<tr>
						<td colspan='9'>
							<center><i>TOTAL POR COMERCIALIZADOR</i></center>
						</td>
						<td>" . $group['to_pay'] . "</td>
						<td>" . $group['to_refund'] . "</td>
					 </tr>
					 <tr>
						<td colspan='11'>
							&nbsp;
						</td>
					 </tr>
					 ";
		}
		$data .= "</table>";
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_de_Comisiones.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$title = "<table><tr><td></td><td><b>Reporte de Comisiones</b></td></tr></table>";
	
	echo $title;
	echo $headerValues;
	echo $data;
?>
