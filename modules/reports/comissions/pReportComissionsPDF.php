<?php
/*
  Document   : pReportComissionsPDF
  Created on : 08-jun-2010, 15:16:31
  Author     : Nicolas
  Description:
  Generate a PDF report of comissions
 */

	Request::setString('selComissionsTo');
	Request::setString('selType');
	Request::setString('txtStartDate');
	Request::setString('txtEndDate');
	
	if (!$selComissionsTo):
		http_redirect('modules/reports/comissions/fSelectComissionsReport');
	endif;
	
	include DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';
	
	$pdf->ezText('<b>REPORTE DE COMISIONES</b>', 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	
	if($selComissionsTo != 'DEALER'){
		switch ($selComissionsTo) {
			case 'MANAGER':
				Request::setString('selManager');
				$serial = $selManager;
				break;
			case 'RESPONSIBLE':
				Request::setString('selResponsible');
				$serial = $selResponsible;
				break;
			case 'DEALER':
				Request::setString('selBranch');
				$serial = $selBranch;
				break;
			case 'SUBMANAGER':
				Request::setString('selDealer');
				$serial = $selDealer;
				break;
		}

		$apComission = new AppliedComission($db);
		$comissionsData = $apComission->getComissionForReport($selComissionsTo, $serial, $selType, $txtStartDate, $txtEndDate);
		
		if ($comissionsData) {
			$totalSold = 0;
			$totalRefunded = 0;
			$comissionToPay = 0;

			switch ($selComissionsTo) {
				case 'MANAGER':
					$manager = new Manager($db, $serial);
					$manager->getData();
					$payTo = $manager->getName_man();
					$payToId = $manager->getDocument_man();
					$payToPhone = $manager->getPhone_man();
					$payToAddress = $manager->getAddress_man();

					if ($comissionsData) {
						foreach ($comissionsData as &$s) {
							$s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
							$s['comissionPercentage'] = ($s['value_mbc_com'] * 100) / $s['net_total_sal'];
							$s['comissionValue'] = $s['value_mbc_com'];
						}
					}
					break;
				case 'RESPONSIBLE':
					$user = new User($db, $serial);
					$user->getData();
					$payTo = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
					$payToId = $user->getDocument_usr();
					$payToPhone = $user->getPhone_usr();
					$payToAddress = $user->getAddress_usr();

					if ($comissionsData) {
						foreach ($comissionsData as &$s) {
							$s['net_total_sal'] = $s['base_value'] - $s['value_dea_com'];
							if ($s['net_total_sal'] > 0) {
								$s['comissionPercentage'] = ($s['value_ubd_com'] * 100) / $s['net_total_sal'];
							}
							$s['comissionValue'] = $s['value_ubd_com'];
						}
					}
					break;
				case 'DEALER':
				case 'SUBMANAGER':
					$dealer = new Dealer($db, $serial);
					if ($dealer->getData()) {
						$payTo = $dealer->getName_dea();
						$payToId = $dealer->getId_dea();
						$payToPhone = $dealer->getPhone1_dea();
						$payToAddress = $dealer->getAddress_dea();
					} else {
						$payTo = 'N/A';
						$payToId = 'N/A';
						$payToPhone = 'N/A';
						$payToAddress = 'N/A';
					}

					if ($comissionsData) {
						foreach ($comissionsData as &$s) {
							$s['net_total_sal'] = $s['base_value'];
							$s['comissionPercentage'] = ($s['value_dea_com'] * 100) / $s['net_total_sal'];
							$s['comissionValue'] = $s['value_dea_com'];
						}
					}
					break;
			}
		}
		
		//CALCULATE THE TOTAL SOLD AND TOTAL REFUNDED
		$totalSold = 0;
		$totalRefunded = 0;
		$effective_sale_total = 0;
		$zeroValue = number_format(0, 2, '.', '');

		if ($comissionsData) {
			foreach ($comissionsData as &$s) {
				if ($s['type_com'] == 'IN') {
					$totalSold += $s['comissionValue'];
					$s['to_comission'] = $s['comissionValue'];
					$s['toDiscount'] = $zeroValue;
					$effective_sale_total += $s['net_total_sal'];
				} else {
					$totalRefunded += $s['comissionValue'];
					$s['to_comission'] = $zeroValue;
					$s['toDiscount'] = $s['comissionValue'];
					$effective_sale_total -= $s['net_total_sal'];
				}

				//style some stuff:
				$s['net_total_sal'] = number_format($s['net_total_sal'], 2, '.', '');
				$s['fixed_comission_percentage'] = number_format($s['fixed_comission_percentage'], 2, '.', '');
				$s['comissionValue'] = number_format($s['comissionValue'], 2, '.', '');
			}
		}

		$today = date('d/m/Y');
		$comissionToPay = $totalSold - $totalRefunded;
		$comissionToPay = number_format($comissionToPay, 2, '.', '');
		$totalSold = number_format($totalSold, 2, '.', '');
		$totalRefunded = number_format($totalRefunded, 2, '.', '');
		$effective_sale_total = number_format($effective_sale_total, 2, '.', '');

		//HEADERS
		$headerValues = array();
		$headerLine1 = array('col1' => '<b>Pagar a:</b>',
			'col2' => $payTo,
			'col3' => '<b>RUC/CI:</b>',
			'col4' => $payToId);
		array_push($headerValues, $headerLine1);
		$headerLine2 = array('col1' => '<b>Fecha:</b>',
			'col2' => $today,
			'col3' => html_entity_decode('<b>Tel&eacute;fono:</b>'),
			'col4' => $payToPhone);
		array_push($headerValues, $headerLine2);
		$headerLine3 = array('col1' => html_entity_decode('<b>Comisi&oacute;n a Favor:</b>'),
			'col2' => number_format($totalSold, 2, '.', ''),
			'col3' => utf8_decode('<b>Dirección:</b>'),
			'col4' => utf8_decode($payToAddress));
		array_push($headerValues, $headerLine3);
		$headerLine4 = array('col1' => html_entity_decode('<b>Comisi&oacute;n a Descontar:</b>'),
			'col2' => number_format($totalRefunded, 2, '.', ''),
			'col3' => utf8_decode('<b>Total Ventas Efectivas:</b>'),
			'col4' => $effective_sale_total);
		array_push($headerValues, $headerLine4);
		$headerLine5 = array('col1' => html_entity_decode('<b>Valor Comisi&oacute;n:</b>'),
			'col2' => number_format($comissionToPay, 2, '.', ''));
		array_push($headerValues, $headerLine5);
		if ($txtEndDate != '' && $txtStartDate != '') {
			$headerLine6 = array('col1' => utf8_decode('<b>Fecha Desde:</b>'),
				'col2' => $txtStartDate,
				'col3' => utf8_decode('<b>Fecha Hasta:</b>'),
				'col4' => $txtEndDate);
			array_push($headerValues, $headerLine6);
		}
		$pdf->ezTable($headerValues, array('col1' => '', 'col2' => '', 'col3' => '', 'col4' => ''), '', array('showHeadings' => 0,
			'shaded' => 0,
			'showLines' => 2,
			'xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'col1' => array('justification' => 'left', 'width' => 100),
				'col2' => array('justification' => 'left', 'width' => 250),
				'col3' => array('justification' => 'left', 'width' => 100),
				'col4' => array('justification' => 'left', 'width' => 305))));
		$pdf->ezSetDy(-10);

		$pdf->ezTable($comissionsData, array('card_number_sal' => '<b># Tarjeta</b>',
			'number_inv' => '<b># Factura</b>',
			'name_cus' => '<b>Apellido y Nombre</b>',
			'name_pbl' => '<b>Producto</b>',
			'days_sal' => '<b>Tiempo</b>',
			'name_dea' => '<b>Agencia</b>',
			'net_total_sal' => '<b>Valor Neto</b>',
			'fixed_comission_percentage' => html_entity_decode('<b>% Comisi&oacute;n</b>'),
			'report_date' => '<b>Fecha</b>',
			'to_comission' => html_entity_decode('<b>Comisi&oacute;n</b>'),
			'toDiscount' => '<b>Descuento</b>'), '', array('xPos' => 'center',
			'innerLineThickness' => 0.8,
			'outerLineThickness' => 1.2,
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'card_number_sal' => array('justification' => 'center', 'width' => 48),
				'number_inv' => array('justification' => 'center', 'width' => 48),
				'name_cus' => array('justification' => 'center', 'width' => 100),
				'name_pbl' => array('justification' => 'center', 'width' => 100),
				'days_sal' => array('justification' => 'center', 'width' => 50),
				'name_dea' => array('justification' => 'center', 'width' => 100),
				'total_sal' => array('justification' => 'center', 'width' => 50),
				'comissionPercentage' => array('justification' => 'center', 'width' => 50),
				'report_date' => array('justification' => 'center', 'width' => 55),
				'to_comission' => array('justification' => 'center', 'width' => 55),
				'toDiscount' => array('justification' => 'center', 'width' => 55))));

		//preparing totals array
		$totalValues = array();
		$totalLine1 = array('col1' => number_format($totalSold, 2, '.', ''),
			'col2' => number_format($totalRefunded, 2, '.', ''));
		array_push($totalValues, $totalLine1);
		$totalLine2 = array('col1' => '<b>TOTAL:</b>',
			'col2' => number_format($totalSold - $totalRefunded, 2, '.', ''));
		array_push($totalValues, $totalLine2);
		$pdf->ezTable($totalValues, array('col1' => '', 'col2' => ''), '', array('showHeadings' => 0,
			'innerLineThickness' => 0.8,
			'outerLineThickness' => 1.2,
			'shaded' => 1,
			'showLines' => 2,
			'xPos' => 721,
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'col1' => array('justification' => 'center', 'width' => 55),
				'col2' => array('justification' => 'center', 'width' => 55))));
		
		
	}else{
		require_once(DOCUMENT_ROOT.'modules/reports/comissions/dealerFixedComissionCode.php');
		
		//*********************** FILTERS ON HEADER ***********************
		$headerValues = array();
		$headerLine1 = array('col1' => '<b>Fecha:</b>',
			'col2' => date('d/m/Y'),
			'col3' => '<b>Representante:</b>',
			'col4' => $manager_name);
		array_push($headerValues, $headerLine1);
		$headerLine2 = array('col1' => '<b>Responsable:</b>',
			'col2' => $resp_name,
			'col3' => html_entity_decode('<b>Comercializador:</b>'),
			'col4' => $dealer_name);
		array_push($headerValues, $headerLine2);
		$headerLine2 = array('col1' => '<b>Sucursal:</b>',
			'col2' => $branch_name,
			'col3' => '',
			'col4' => '');
		array_push($headerValues, $headerLine2);
		$headerLine3 = array('col1' => html_entity_decode('<b>Comisi&oacute;n a Favor:</b>'),
			'col2' => number_format($totalSold, 2, '.', ''),
			'col3' => html_entity_decode('<b>Comisi&oacute;n a Descontar:</b>'),
			'col4' => $totalRefunded);
		array_push($headerValues, $headerLine3);
		$headerLine4 = array('col1' => html_entity_decode('<b>Total Ventas Efectivas:</b>'),
			'col2' => number_format($effective_sale_total, 2, '.', ''),
			'col3' => html_entity_decode('<b>Valor Comisi&oacute;n:</b>'),
			'col4' => ($totalSold - $totalRefunded));
		array_push($headerValues, $headerLine4);
		if ($txtEndDate != '' && $txtStartDate != '') {
			$headerLine6 = array('col1' => utf8_decode('<b>Fecha Desde:</b>'),
				'col2' => $txtStartDate,
				'col3' => utf8_decode('<b>Fecha Hasta:</b>'),
				'col4' => $txtEndDate);
			array_push($headerValues, $headerLine6);
		}
		$pdf->ezTable($headerValues, array('col1' => '', 'col2' => '', 'col3' => '', 'col4' => ''), '', array('showHeadings' => 0,
			'shaded' => 0,
			'showLines' => 2,
			'xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'col1' => array('justification' => 'left', 'width' => 100),
				'col2' => array('justification' => 'left', 'width' => 250),
				'col3' => array('justification' => 'left', 'width' => 100),
				'col4' => array('justification' => 'left', 'width' => 305))));
		$pdf->ezSetDy(-10);
		
		//*********************** DATA ***********************
		
		foreach($final_info_array as $group){
			$pdf->ezTable($group['detailed_info'], 
						array(	'card_number_sal' => '<b># Tarjeta</b>',
								'number_inv' => '<b># Factura</b>',
								'name_cus' => '<b>Apellido y Nombre</b>',
								'name_pbl' => '<b>Producto</b>',
								'days_sal' => '<b>Tiempo</b>',
								'name_dea' => '<b>Agencia</b>',
								'base_value' => '<b>Valor Neto</b>',
								'fixed_comission_percentage' => html_entity_decode('<b>% Comisi&oacute;n</b>'),
								'report_date' => '<b>Fecha</b>',
								'to_comission' => html_entity_decode('<b>Comisi&oacute;n</b>'),
								'toDiscount' => '<b>Descuento</b>'), 
								'<b>'.$group['dealer'].'</b>', 
								array('xPos' => 'center',
										'innerLineThickness' => 0.8,
										'outerLineThickness' => 1.2,
										'fontSize' => 8,
										'titleFontSize' => 8,
										'cols' => array(
											'card_number_sal' => array('justification' => 'center', 'width' => 48),
											'number_inv' => array('justification' => 'center', 'width' => 48),
											'name_cus' => array('justification' => 'center', 'width' => 100),
											'name_pbl' => array('justification' => 'center', 'width' => 100),
											'days_sal' => array('justification' => 'center', 'width' => 50),
											'name_dea' => array('justification' => 'center', 'width' => 100),
											'base_value' => array('justification' => 'center', 'width' => 50),
											'comissionPercentage' => array('justification' => 'center', 'width' => 50),
											'report_date' => array('justification' => 'center', 'width' => 55),
											'to_comission' => array('justification' => 'center', 'width' => 55),
											'toDiscount' => array('justification' => 'center', 'width' => 55))));
			
			$subtotal_array = array(array('title' => '<i>TOTAL POR COMERCIALIZADOR</i>',
									'to_pay' => $group['to_pay'],
									'to_refund' => $group['to_refund']));
			
			$pdf->ezTable(	$subtotal_array, 
							array(	'title' => '<b># Tarjeta</b>',
									'to_pay' => '<b># Factura</b>',
									'to_refund' => '<b>Apellido y Nombre</b>'), 
							'', 
							array(	'xPos' => 'center',
									'showHeadings' => 0,
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 1.2,
									'fontSize' => 8,
									'titleFontSize' => 8,
									'cols' => array(
										'title' => array('justification' => 'center', 'width' => 602),
										'to_pay' => array('justification' => 'center', 'width' => 55),
										'to_refund' => array('justification' => 'center', 'width' => 55))));
			$pdf->ezSetDy(-10);
		}
	}
	
	$pdf->ezStream();
?>
