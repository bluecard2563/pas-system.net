<?php
/**
 * File:  fAverageComissions
 * Author: Patricio Astudillo
 * Creation Date: 27-abr-2011, 15:56:56
 * Description:
 * Last Modified: 27-abr-2011, 15:56:56
 * Modified by:
 */

	$zone = new Zone($db);
	$zone_list = $zone ->getDealerZones($_SESSION['countryList']);
	$today = date('d/m/Y');
	
	$smarty -> register('zone_list,today');
	$smarty -> display();
?>
