<?php

/*
  Document   : pReportStock
  Created on : 27-may-2010, 14:18:09
  Author     : Nicolas
  Description:
  generates a pdf Stock report based on the parameters recived
 */
Request::setString('selZone,selCountry,selManager,selResponsable,selDealer,selBranch,selCounter,selType,txtBeginDate,txtEndDate');
$stock = new Stock($db);
$rows = $stock->queryForGeneralReport($selZone, $selCountry, $selManager, $selResponsable, $selDealer, 
											$selBranch, $selCounter, $selType, $txtBeginDate, $txtEndDate);
//check if type filter was used, to change the titles
if ($selType != '') {
	$typeTitle = "($selType)";
} else {
	$typeTitle = '';
}

//
include DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';
$pdf->ezStopPageNumbers();
$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);
$pdf->ezSetDy(-20);
if ($selCounter != '') {
	$counter = new Counter($db, $selCounter);
	$counter->getData();
	$user = new User($db, $counter->getSerial_usr());
	$user->getData();
	$pdf->ezText('<b>' . 'REPORTE DE STOCK COUNTER <i>' . utf8_decode($user->getFirstname_usr()) . ' ' . utf8_decode($user->getLastname_usr()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('from_sto' => 'Desde', 'to_sto' => 'Hasta', 'quantity' => 'Cantidad', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 60))));
	} else {
		$pdf->ezText('Este counter no posee stock asignado', 10, array('justification' => 'center'));
	}
} elseif ($selBranch != '') {
	$branch = new Dealer($db, $selBranch);
	$branch->getData();
	$pdf->ezText('<b>' . 'REPORTE DE STOCK SUCURSAL DE COMERCIALIZADOR <i>' . utf8_decode($branch->getName_dea()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('counter_name' => 'Counter', 'from_sto' => 'Desde', 'to_sto' => 'Hasta', 'quantity' => 'Cantidad', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'counter_name' => array('justification' => 'center', 'width' => 120),
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 60))));
	} else {
		$pdf->ezText('Esta Sucursal no posee stock asignado', 10, array('justification' => 'center'));
	}
} elseif ($selDealer != '') {
	$dealer = new Dealer($db, $selDealer);
	$dealer->getData();
	$pdf->ezText('<b>' . 'REPORTE DE STOCK COMERCIALIZADOR <i>' . utf8_decode($dealer->getName_dea()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('name_cou' => utf8_decode('País'), 'name_man' => 'Representante', 'responsible_name' => 'Responsable', 'branch_name' => 'Sucursal del Comercializador', 'from_sto' => 'Desde', 'to_sto' => 'Hasta', 'quantity' => 'Cantidad', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'name_cou' => array('justification' => 'center', 'width' => 80),
				'name_man' => array('justification' => 'center', 'width' => 90),
				'responsible_name' => array('justification' => 'center', 'width' => 100),
				'branch_name' => array('justification' => 'center', 'width' => 100),
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 70))));
	} else {
		$pdf->ezText('Este Comercializador no posee stock asignado', 10, array('justification' => 'center'));
	}
} elseif ($selResponsable != '') {
	$user = new User($db, $selResponsable);
	$user->getData();
	$pdf->ezText('<b>' . 'REPORTE DE STOCK RESPONSABLE <i>' . utf8_decode($user->getLastname_usr() . ' ' . $user->getFirstname_usr()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('name_cou' => utf8_decode('País'), 'name_man' => 'Representante', 'responsible_name' => 'Responsable', 'name_dea' => 'Comercializador', 'from_sto' => 'Desde', 'to_sto' => 'Hasta', 'quantity' => 'Cantidad', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'name_cou' => array('justification' => 'center', 'width' => 80),
				'name_man' => array('justification' => 'center', 'width' => 90),
				'responsible_name' => array('justification' => 'center', 'width' => 100),
				'name_dea' => array('justification' => 'center', 'width' => 100),
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 70))));
	} else {
		$pdf->ezText(utf8_decode('Ningún Comercializador del Responsable posee stock asignado'), 10, array('justification' => 'center'));
	}
} elseif ($selManager != '') {
	$manager_by_country = new ManagerbyCountry($db, $selManager);
	$manager_by_country->getData();
	$manager = new Manager($db, $manager_by_country->getSerial_mbc());
	$manager->getData();
	$pdf->ezText('<b>' . 'REPORTE DE STOCK REPRESENTANTE <i>' . utf8_decode($manager->getName_man()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('name_cou' => utf8_decode('País'), 'name_man' => 'Representante', 'responsible_name' => 'Responsable', 'name_dea' => 'Comercializador', 'from_sto' => 'Desde', 'to_sto' => 'Hasta', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'name_cou' => array('justification' => 'center', 'width' => 80),
				'name_man' => array('justification' => 'center', 'width' => 90),
				'responsible_name' => array('justification' => 'center', 'width' => 100),
				'name_dea' => array('justification' => 'center', 'width' => 100),
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 70))));
	} else {
		$pdf->ezText(utf8_decode('Ningún Comercializador del representante posee stock asignado'), 10, array('justification' => 'center'));
	}
} elseif ($selCountry != '') {
	$country = new Country($db, $selCountry);
	$country->getData();
	$pdf->ezText('<b>' . 'REPORTE DE STOCK PAIS <i>' . utf8_decode($country->getName_cou()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('name_cou' => utf8_decode('País'), 'name_man' => 'Representante', 'responsible_name' => 'Responsable', 'name_dea' => 'Comercializador', 'from_sto' => 'Desde', 'to_sto' => 'Hasta', 'quantity' => 'Cantidad', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'name_cou' => array('justification' => 'center', 'width' => 80),
				'name_man' => array('justification' => 'center', 'width' => 90),
				'responsible_name' => array('justification' => 'center', 'width' => 100),
				'name_dea' => array('justification' => 'center', 'width' => 100),
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 70))));
	} else {
		$pdf->ezText(utf8_decode('Ningun Comercializador del país posee stock asignado'), 10, array('justification' => 'center'));
	}
} else {
	$zone = new Zone($db, $selZone);
	$zone->getData();
	$pdf->ezText('<b>' . 'REPORTE DE ZONA <i>' . utf8_decode($zone->getName_zon()) . "</i>$typeTitle</b>", 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	if (is_array($rows)) {
		$pdf->ezTable($rows, array('name_cou' => utf8_decode('País'), 'from_sto' => 'Desde', 'to_sto' => 'Hasta', 'quantity' => 'Cantidad', 'date_sto' => utf8_decode('Fecha de Asignación'), 'type_sto' => 'Tipo', 'assigned_by' => 'Asignado por'), '', array('xPos' => 'center',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'name_cou' => array('justification' => 'center', 'width' => 80),
				'from_sto' => array('justification' => 'center', 'width' => 50),
				'to_sto' => array('justification' => 'center', 'width' => 50),
				'quantity' => array('justification' => 'center', 'width' => 50),
				'date_sto' => array('justification' => 'center', 'width' => 110),
				'type_sto' => array('justification' => 'center', 'width' => 60),
				'assigned_by' => array('justification' => 'center', 'width' => 70))));
	} else {
		$pdf->ezText(utf8_decode('Ningun Comercializador del país posee stock asignado'), 10, array('justification' => 'center'));
	}
}

$pdf->ezStream();
?>
