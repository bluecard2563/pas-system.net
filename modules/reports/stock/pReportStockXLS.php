<?php
/**
 * File: pReportStockXLS
 * Author: Patricio Astudillo
 * Creation Date: 17/11/2011
 * Modified By: Ptricio Astudillo
 * Last Modified: 17/11/2011
 */
	set_time_limit(36000);
	ini_set('memory_limit','512M');
	
	Request::setString('selZone,selCountry,selManager,selResponsable,selDealer,selBranch,selCounter,selType,txtBeginDate,txtEndDate');
	
	$stock = new Stock($db);
	$rows = $stock->queryForGeneralReport($selZone, $selCountry, $selManager, $selResponsable, $selDealer, 
											$selBranch, $selCounter, $selType, $txtBeginDate, $txtEndDate);
	
	$file_title = $date = ($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);
	
	if($rows){
		//check if type filter was used, to change the titles
		if ($selType != '') {
			$typeTitle = "($selType)";
		} else {
			$typeTitle = '';
		}

		if ($selCounter != '') {
			$counter = new Counter($db, $selCounter);
			$counter->getData();
			$user = new User($db, $counter->getSerial_usr());
			$user->getData();

			$report_title = "<b>REPORTE DE STOCK COUNTER</b> ".$user->getFirstname_usr().' '.$user->getLastname_usr();
			$fields_and_titles = array(	'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'quantity' => 'Cantidad', 
										'date_sto' => 'Fecha de Asignaci&oacute;n', 
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');

		} elseif ($selBranch != '') {
			$branch = new Dealer($db, $selBranch);
			$branch->getData();

			$report_title = "<b>REPORTE DE STOCK SUCURSAL DE COMERCIALIZADOR</b> ".$branch->getName_dea();
			$fields_and_titles = array(	'counter_name' => 'Counter', 
										'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'quantity' => 'Cantidad', 
										'date_sto' => 'Fecha de Asignaci&oacute;n', 
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');

		} elseif ($selDealer != '') {
			$dealer = new Dealer($db, $selDealer);
			$dealer->getData();

			$report_title = "<b>REPORTE DE STOCK COMERCIALIZADOR</b> ".$dealer->getName_dea();
			$fields_and_titles = array(	'name_cou' => 'Pa&iacute;s', 
										'name_man' => 'Representante', 
										'responsible_name' => 'Responsable', 
										'branch_name' => 'Sucursal del Comercializador', 
										'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'quantity' => 'Cantidad', 
										'date_sto' => 'Fecha de Asignaci&oacute;n', 
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');

		} elseif ($selResponsable != '') {
			$user = new User($db, $selResponsable);
			$user->getData();

			$report_title =  "<b>REPORTE DE STOCK RESPONSABLE</b> ".$user->getLastname_usr().' '.$user->getFirstname_usr();
			$fields_and_titles = array(	'name_cou' => 'Pa&iacute;s', 
										'name_man' => 'Representante', 
										'responsible_name' => 'Responsable', 
										'name_dea' => 'Comercializador', 
										'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'quantity' => 'Cantidad', 
										'date_sto' => 'Fecha de Asignaci&oacute;n', 
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');

		} elseif ($selManager != '') {
			$manager_by_country = new ManagerbyCountry($db, $selManager);
			$manager_by_country->getData();
			$manager = new Manager($db, $manager_by_country->getSerial_mbc());
			$manager->getData();

			$report_title =  "<b>REPORTE DE STOCK REPRESENTANTE</b> ".$manager->getName_man();
			$fields_and_titles = array( 'name_cou' => 'Pa&iacute;s', 
										'name_man' => 'Representante', 
										'responsible_name' => 'Responsable', 
										'name_dea' => 'Comercializador', 
										'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'date_sto' => 'Fecha de Asignaci&oacute;n',
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');

		} elseif ($selCountry != '') {
			$country = new Country($db, $selCountry);
			$country->getData();

			$report_title =  "<b>REPORTE DE STOCK PA&Iacute;S</b> ".$country->getName_cou();
			$fields_and_titles = array(	'name_cou' => 'Pa&iacute;s', 
										'name_man' => 'Representante', 
										'responsible_name' => 'Responsable', 
										'name_dea' => 'Comercializador', 
										'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'quantity' => 'Cantidad', 
										'date_sto' => 'Fecha de Asignaci&oacute;n', 
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');

		} else {
			$zone = new Zone($db, $selZone);
			$zone->getData();

			$report_title = "<b>REPORTE DE STOCK ZONA</b> ".$zone->getName_zon();

			$fields_and_titles = array(	'name_cou' => 'Pa&iacute;s', 
										'from_sto' => 'Desde', 
										'to_sto' => 'Hasta', 
										'quantity' => 'Cantidad',
										'date_sto' => 'Fecha de Asignaci&oacute;n', 
										'type_sto' => 'Tipo', 
										'assigned_by' => 'Asignado por');
		}
		$report_body = $report_title.'<br>';
		
		$report_body.= "<b>Reporte Desde:</b> ".$txtBeginDate."  <b>Hasta:</b> ".$txtEndDate."<br><br>";
		
		$report_body .= '<table border=1>';
			$report_body .= '<tr>';
				foreach($fields_and_titles as $title){
					$report_body .= "<td><b>$title</b></td>";
				}
			$report_body .= '</tr>';

			foreach($rows as $register){
				$report_body .= '<tr>';
				foreach($fields_and_titles as $key=>$title){
					$report_body .= "<td>$register[$key]</td>";
				}
				$report_body .= '</tr>';
			}			
		$report_body .= '</table>';
	}else{
		$report_body = '<b>REPORTE DE STOCK</b><br><br> No existen registros para los filtros utilizados';
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=ReporteStockAsignado.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $file_title.'<br /><br />';
	echo $report_body;
?>
