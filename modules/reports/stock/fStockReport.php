<?php
/*
  Document   : fStockReport
  Created on : 27-may-2010, 9:54:58
  Author     : Nicolas
  Description:
  interface with filters to create an stock report
 */

	//get list of existing zones
	$zone = new Zone($db);
	$nameZoneList = $zone->getZones();
	
	//get the list of stock types
	$stock = new Stock($db);
	$typeList = $stock->getTypeValues();

	$today = date('d/m/Y');

	$smarty->register('nameZoneList,typeList,today');
	$smarty->display();
?>
