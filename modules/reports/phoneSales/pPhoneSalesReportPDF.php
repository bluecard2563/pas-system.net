<?php
/*
File: pPhoneSalesReportPDF
Author: David Bergmann
Creation Date: 20/07/2010
Last Modified:
Modified By:
*/
include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

//HEADER
$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);

$pdf->ezText('<b>'.utf8_decode("REPORTE DE VENTAS TELEFÓNICAS").'</b>', 12, array('justification'=>'center'));
$pdf->ezText(utf8_decode($date), 10, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$user = new User($db, $_POST['selCounter']);
$user->getData();

$pdf->ezText('<b>'.utf8_decode("Asesor de Venta: ").'</b>'.utf8_decode($user->getFirstname_usr()." ".$user->getLastname_usr()), 10, array('justification'=>'left'));
$pdf->ezSetDy(-5);
$pdf->ezText('<b>'.utf8_decode("Fecha de Inicio: ").'</b>'.$_POST['beginDate'], 10, array('justification'=>'left'));
$pdf->ezSetDy(-5);
$pdf->ezText('<b>'.utf8_decode("Fecha Fin: ").'</b>'.$_POST['endDate'], 10, array('justification'=>'left'));
$pdf->ezSetDy(-20);

$report = Sales::getPhoneSalesReport($db, $_POST['selCounter'], $_POST['beginDate'], $_POST['endDate']);


if(is_array($report)) {
    $titles=array('name_pbl'=>utf8_decode('<b>Nombre del Producto</b>'),
                  'card_number'=>utf8_decode('<b>Número de Tarjeta</b>'),
				  'number_inv'=>utf8_decode('<b>Número de la factura</b>'),
                  'emission_date_sal'=>utf8_decode('<b>Fecha de Emisión de la Tarjeta</b>'),
                  'total_sal'=>utf8_decode('<b>Valor de la venta</b>'),
                  'total_inv'=>utf8_decode('<b>Valor de la factura</b>')
                  );

	$totalSale = 0;
	$totalInvoice = 0;
	foreach($report as $r) {
		$totalSale += $r['total_sal'];
		$totalInvoice += $r['total_inv'];
	}

	array_push($report, array('name_pbl'=>"",
							  'card_number'=>"",
							  'number_inv'=>"",
							  'emission_date_sal'=>"Total:",
							  'total_sal'=>number_format($totalSale,2,".",""),
							  'total_inv'=>number_format($totalInvoice,2,".","")));


    $pdf->ezTable($report,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'name_pbl'=>array('justification'=>'center','width'=>60),
                                        'card_number'=>array('justification'=>'center','width'=>60),
										'number_inv'=>array('justification'=>'center','width'=>60),
                                        'emission_date_sal'=>array('justification'=>'center','width'=>60),
                                        'total_sal'=>array('justification'=>'center','width'=>60),
                                        'total_inv'=>array('justification'=>'center','width'=>60))));
        $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('El counter telefónico no ha realizado ninguna venta en el rango de fechas seleccionado.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>