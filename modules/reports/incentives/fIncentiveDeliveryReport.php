<?php
/**
 * Allows to obtain the data for the report of the incentives delivery.
 * @author Santi Albuja
 * @date 6/Jul/2021
 * @var string $db
 * @var FormData $smarty
 */

$today = date('d/m/Y');
$countryId = 62;
$managerByCountry = new ManagerbyCountry($db);
$managerList = $managerByCountry->getManagerByCountry($countryId, 1);

$smarty->register('today,managerList');
$smarty->display();
