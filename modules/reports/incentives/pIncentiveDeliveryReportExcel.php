<?php
/**
 * Allows to obtain the data for the report of the incentives delivery.
 * @author Santi Albuja
 * @date 6/Jul/2021
 * @var string $db
 * @var int $selManager
 * @var int $selDelivery
 */

set_time_limit(36000);
ini_set('memory_limit', '512M');

Request::setString('selManager');
Request::setString('selDelivery');

$userSession = $_SESSION['serial_usr'];
$invoice = new Invoice($db);

$table = "";
$incentives = $invoice->getIncentives($db, $selManager, $selDelivery);

if (!empty($incentives)) {

    $table .= "
        <!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            </head>
        <body>
        <table border='1'>
            <tr>
                 <td align='center'><b>No</b></td>
                 <td align='center'><b>Sucursal</b></td>
                 <td align='center'><b>Comercializador</b></td>
                 <td align='center'><b>Total</b></td>
                 <td align='center'><b>Bonos</b></td>
                 <td align='center'><b>Estado</b></td>
                 <td align='center'><b>Entregado Por</b></td>
                 <td align='center'><b>Fecha de Entrega</b></td>
                 <td align='center'><b>Fecha de Registro</b></td>
        
    ";

    foreach ($incentives as $incentive) {

        $table .= "
            <tr>
                <td align='center'>" . $incentive['serial_idc'] . "</td>
                <td align='center'>" . $incentive['name_man'] . "</td>
                <td align='center'>" . $incentive['name_dea'] . "</td>
                <td align='center'>" . $incentive['total'] . "</td>
                <td align='center'>" . $incentive['bonus'] . "</td>
                <td align='center'>" . $incentive['deliverystatus'] . "</td>
                <td align='center'>" . $incentive['deliveredbyuser'] . "</td>
                <td align='center'>" . $incentive['delivery_date'] . "</td>
                <td align='center'>" . $incentive['created_at'] . "</td>
            </tr>
        ";

    }

    $table .= "
                </tr>
            </table>  
        </body>
    </html>
    ";

} else {
    $table .= "<b>No existen registros</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteEntregaIncentivos.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;