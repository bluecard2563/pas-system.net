<?php
set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('selDateType');
Request::setString('txtDateFrom');
Request::setString('txtDateTo');

$dateFrom = DateTime::createFromFormat('d/m/Y', $txtDateFrom);
$dateTo = DateTime::createFromFormat('d/m/Y', $txtDateTo);
$dFrom = $dateFrom->format('Y-m-d'); //Date From in mysql format
$dTo = $dateTo->format('Y-m-d'); //Date To in mysql format

$data = array();
$customer = new Customer($db);
$data = $customer->getCustomerTripLastWeek($selDateType, $dFrom, $dTo);

if (is_array($data)){
    $table.="
    <table border='1'>
		<tr>
			<td  align='center'><b>Cliente</b></td>
			<td  align='center'><b>Celular</b></td>
			<td  align='center'><b>Telefono</b></td>
			<td  align='center'><b>Correo</b></td>
			<td  align='center'><b>Destino</b></td>
			<td  align='center'><b>Producto</b></td>
			<td  align='center'><b>Ciudad Venta</b></td>
			<td  align='center'><b>Fecha</b></td>
			<td  align='center'><b>Tarjeta</b></td>";
                foreach($data as $key=>$c){
                    $table.="
                     <tr>
                                <td  align='center'>".$data[$key]['Cliente']."</td>
                                <td  align='center'>".$data[$key]['Celular']."</td>
                                <td  align='center'>".$data[$key]['Telefono']."</td>
                                <td  align='center'>".$data[$key]['Correo']."</td>
                                <td  align='center'>".$data[$key]['Destino']."</td>
                                <td  align='center'>".$data[$key]['Producto']."</td>
                                <td  align='center'>".$data[$key]['Ciudad Venta']."</td>
                                <td  align='center'>".$data[$key]['Fecha']."</td>
                                <td  align='center'>".$data[$key]['Tarjeta']."</td>
                    </tr>";
                }
    $table.="</tr>
	</table>";

}
else{
    $table.="<b>No existen clientes registrados</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteViaje.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;