<?php
/*
File:pCustomerReportExcel
Author: Esteban Angulo
Creation Date: 28/05/2010
Description: Generates an Excel file with all customers by the filters applied

*/

set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('selCountry');
Request::setString('selCity');
Request::setString('selStatus');
$customer=new Customer($db);
$data=$customer->getMailList($db, $selCountry, $selCity);
$country=new Country($db, $selCountry);
$country->getData();
$zone=new Zone($db, $country->getSerial_zon());
$zone->getData();
$allCities=0;
$allStatus=0;
if($selCity!=''){
	$city=new City($db, $selCity);
	$city->getData();
	$cityName=$city->getName_cit();
	$allCities=1;
}else{
	$cityName='Todos';
	$allCities=2;
}



	
if(is_array($data)){
	
	
$header="<br><table border='1'>
			<tr>
				<td colspan='2' align='center'><b>Datos del Reporte de Clientes</b></td>
			</tr>
			<tr>
				<td  align='center'><b>Zona</b></td>
				<td	 align='center'>".$zone->getName_zon()."</td>
			</tr>
			<tr>
				<td  align='center'>".utf8_decode('<b>Pa&iacute;s</b>')."</td>
				<td	 align='center'>".$country->getName_cou()."</td>
			</tr>
			<tr>
				<td  align='center'><b>Ciudad</b></td>
				<td	 align='center'>".$cityName."</td>
			</tr>
			<tr>
				<td  align='center'><b>Estado</b></td>
				<td	 align='center'>".$status."</td>
			</tr>
		  </table>";
$table.="<br>";
$table.="<table border='1'>
		<tr>
			<td colspan='".$colspan."' align='center'><b>Reporte de Clientes</b></td>
		</tr>
		<tr>
		
			<td  align='center'><b>Nombre y Apellido</b></td>
			<td  align='center'><b>E-Mail</b></td>";

foreach($data as $key=>$c){
$table.="   	<tr>
					
					<td  align='center'>".$data[$key]['nombre']."</td>
				
					<td  align='center'>".$data[$key]['email']."</td>";

	
	
$table.="		</tr>";
}
$table.="	</tr>

		</table>";
}
else{
	$table.="<b>No existen clientes registrados</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteClientes.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $header;
echo $table;
?>
