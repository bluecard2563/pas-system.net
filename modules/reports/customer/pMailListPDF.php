<?php
/*
File:pMailListPDF.php
Author: Yessica Rios
Creation Date: 27/05/2010
*/

set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('selCountry');
Request::setString('selCity');

$customer =new Customer($db);

$data=$customer->getMailList($db, $selCountry, $selCity);
$country=new Country($db, $selCountry);
$country->getData();
$zone=new Zone($db, $country->getSerial_zon());
$zone->getData();
$allCities=0;
$allStatus=0;
if($selCity!='')
{
	$city=new City($db, $selCity);
	$city->getData();
	$cityName=$city->getName_cit();
	$allCities=1;
}else
{
	$cityName='Todos';
	$allCities=2;
}



include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
//HEADER
$pdf->ezText('<b>REPORTE DE CORREOS</b>', 12, array('justification'=>'center'));

$pdf->ezSetDy(-20);
$headerDesc=array();
$descLine1=array('col1'=>utf8_decode('<b>Zona:</b>'),
				 'col2'=>$zone->getName_zon());
array_push($headerDesc,$descLine1);
$descLine2=array('col1'=>utf8_decode('<b>País:</b>'),
				 'col2'=>$country->getName_cou());
array_push($headerDesc,$descLine2);
$descLine3=array('col1'=>'<b>Ciudad:</b>',
				 'col2'=>$cityName);
array_push($headerDesc,$descLine3);
$descLine4=array('col1'=>'<b>Estado:</b>',
				 'col2'=>$status);
array_push($headerDesc,$descLine4);
$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>100),
						'col2'=>array('justification'=>'left','width'=>100))));
$pdf->ezSetDy(-20);
//END HEADERS

	/*PARAMS FOR CREATING PDF FILE*/
	
		$header = array('nombre'=>'<b>Nombre y Apellido</b>',
						'email'=>'<b>E-Mail</b>');
		$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 12,
						'titleFontSize' => 8,
						'cols'=>array(
							'nombre'=>array('justification'=>'center','width'=>250),
							'email'=>array('justification'=>'center','width'=>200)));
	
	/*END PARAMS*/
	$pdf->ezTable($data,$header,
						$tableTitle,
						$params);
		$pdf->ezSetDy(-20);



$pdf->ezStream();
?>
