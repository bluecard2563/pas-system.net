<?php
/*
File:pCustomerReportPDF.php
Author: Esteban Angulo
Creation Date: 27/05/2010
*/

set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('selCountry');
Request::setString('selCity');
Request::setString('selStatus');
Request::setString('txtBeginDate');
Request::setString('txtEndDate');
$customer=new Customer($db);
$data=$customer->getCustomersForReport($selCountry, $selCity, $selStatus,$txtBeginDate,$txtEndDate);
$country=new Country($db, $selCountry);
$country->getData();
$zone=new Zone($db, $country->getSerial_zon());
$zone->getData();
$allCities=0;
$allStatus=0;
if($selCity!=''){
	$city=new City($db, $selCity);
	$city->getData();
	$cityName=$city->getName_cit();
	$allCities=1;
}else{
	$cityName='Todos';
	$allCities=2;
}
if($selStatus!=''){
	if($selStatus=='ACTIVE'){
		$status='Regular';
	}
	elseif($selStatus=='INACTIVE'){
		$status='Lista Negra';
	}
	$allStatus=1;
}
else{
	$status='Todos';
	$allStatus=2;
}

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
//HEADER
$pdf->ezText('<b>REPORTE DE CLIENTES</b>', 12, array('justification'=>'center'));

$pdf->ezSetDy(-10);
$headerDesc=array();
$descLine1=array('col1'=>utf8_decode('<b>Zona:</b>'),
				 'col2'=>$zone->getName_zon());
array_push($headerDesc,$descLine1);
$descLine2=array('col1'=>utf8_decode('<b>País:</b>'),
				 'col2'=>$country->getName_cou());
array_push($headerDesc,$descLine2);
$descLine3=array('col1'=>'<b>Ciudad:</b>',
				 'col2'=>$cityName);
array_push($headerDesc,$descLine3);
$descLine4=array('col1'=>'<b>Estado:</b>',
				 'col2'=>$status);
array_push($headerDesc,$descLine4);
$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150))));
$pdf->ezSetDy(-10);
//END HEADERS

if(is_array($data)){
	$customers=array();
	foreach($data as $key=>$c){
		$customers[$key]['document_cus']=$c['document_cus'];
		$customers[$key]['name_cus']=$c['complete_name'];
		$customers[$key]['birthdate_cus']=$c['birthdate_cus'];
        $customers[$key]['number_contact']=$c['numero_de_contrato'];
        $customers[$key]['date_sal']=$c['fecha_venta'];
		if($c['phone1_cus']=='' ){
			$customers[$key]['phone1_cus']='-';
		}
		else{
			$customers[$key]['phone1_cus']=$c['phone1_cus'];
		}
		if($c['cellphone_cus']=='' ){
			$customers[$key]['cellphone_cus']='-';
		}
		else{
			$customers[$key]['cellphone_cus']=$c['cellphone_cus'];
		}
		$customers[$key]['email_cus']=$c['email_cus'];
		if($c['relative_cus']==''){
			$customers[$key]['relative_cus']='-';
		}
		else{
			$customers[$key]['relative_cus']=$c['relative_cus'];
		}
		if($c['relative_phone_cus']==''){
			$customers[$key]['relative_phone_cus']='-';
		}
		else{
			$customers[$key]['relative_phone_cus']=$c['relative_phone_cus'];
		}
		if($c['vip_cus']=='0' || $c['vip_cus']=='' ){
			$customers[$key]['vip_cus']='NO';
		}
		else{
			$customers[$key]['vip_cus']='SI';
		}
		if($allStatus==2){
			if($c['status_cus']=='ACTIVE'){
				$customers[$key]['status_cus']='Regular';
			}
			elseif($c['status_cus']=='INACTIVE'){
				$customers[$key]['status_cus']='Lista Negra';
			}
		}
		if($allCities==2){
			$customers[$key]['city_cus']=$c['name_cit'];
		}
	}
	/*PARAMS FOR CREATING PDF FILE*/
	if($allCities==2 && $allStatus==2){
		$header = array('number_contact'=>'<b># Contrato</b>',
            			'date_sal'=>'<b>Fecha Venta</b>',
						'document_cus'=>'<b>Documento</b>',
						'name_cus'=>'<b>Nombre y Apellido</b>',
						'birthdate_cus'=>'<b>Fecha de Nacimiento</b>',
						'phone1_cus'=>utf8_decode('<b>Teléfono</b>'),
						'cellphone_cus'=>'<b>Celular</b>',
						'email_cus'=>'<b>E-Mail</b>',
						'city_cus'=>'<b>Ciudad</b>',
						'relative_cus'=>'<b>Persona de Contacto</b>',
						'relative_phone_cus'=>utf8_decode('<b>Teléfono de Contacto</b>'),
						'vip_cus'=>'<b>Cliente VIP</b>',
						'status_cus'=>'<b>Estado</b>');
		$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_contact'=>array('justification'=>'center','width'=>40),
							'date_sal'=>array('justification'=>'center','width'=>50),
							'document_cus'=>array('justification'=>'center','width'=>65),
							'name_cus'=>array('justification'=>'center','width'=>100),
							'birthdate_cus'=>array('justification'=>'center','width'=>55),
							'phone1_cus'=>array('justification'=>'center','width'=>60),
							'cellphone_cus'=>array('justification'=>'center','width'=>60),
							'email_cus'=>array('justification'=>'center','width'=>120),
							'city_cus'=>array('justification'=>'center','width'=>40),
							'relative_cus'=>array('justification'=>'center','width'=>100),
							'relative_phone_cus'=>array('justification'=>'center','width'=>60),
							'vip_cus'=>array('justification'=>'center','width'=>40),
							'status_cus'=>array('justification'=>'center','width'=>60)));
	}
	elseif($allCities==2 && $allStatus!=2){
		$header = array('number_contact'=>'<b># Contrato</b>',
            'date_sal'=>'<b>Fecha Venta</b>',
						'document_cus'=>'<b>Nombre y Apellido</b>',
						'name_cus'=>'<b>Nombre y Apellido</b>',
						'birthdate_cus'=>'<b>Fecha de Nacimiento</b>',
						'phone1_cus'=>utf8_decode('<b>Teléfono</b>'),
						'cellphone_cus'=>'<b>Celular</b>',
						'email_cus'=>'<b>E-Mail</b>',
						'city_cus'=>'<b>Ciudad</b>',
						'relative_cus'=>'<b>Persona de Contacto</b>',
						'relative_phone_cus'=>utf8_decode('<b>Teléfono de Contacto</b>'),
						'vip_cus'=>'<b>Cleinte VIP</b>');
		$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_contact'=>array('justification'=>'center','width'=>40),
							'date_sal'=>array('justification'=>'center','width'=>50),
							'document_cus'=>array('justification'=>'center','width'=>65),
							'name_cus'=>array('justification'=>'center','width'=>100),
							'birthdate_cus'=>array('justification'=>'center','width'=>55),
							'phone1_cus'=>array('justification'=>'center','width'=>60),
							'cellphone_cus'=>array('justification'=>'center','width'=>60),
							'email_cus'=>array('justification'=>'center','width'=>120),
							'city_cus'=>array('justification'=>'center','width'=>40),
							'relative_cus'=>array('justification'=>'center','width'=>100),
							'relative_phone_cus'=>array('justification'=>'center','width'=>60),
							'vip_cus'=>array('justification'=>'center','width'=>40)));
	}
	if($allCities!=2 && $allStatus==2){
		$header = array('number_contact'=>'<b># Contrato</b>',
            'date_sal'=>'<b>Fecha Venta</b>',
						'document_cus'=>'<b>Nombre y Apellido</b>',
						'name_cus'=>'<b>Nombre y Apellido</b>',
						'birthdate_cus'=>'<b>Fecha de Nacimiento</b>',
						'phone1_cus'=>utf8_decode('<b>Teléfono</b>'),
						'cellphone_cus'=>'<b>Celular</b>',
						'email_cus'=>'<b>E-Mail</b>',
						'relative_cus'=>'<b>Persona de Contacto</b>',
						'relative_phone_cus'=>utf8_decode('<b>Teléfono de Contacto</b>'),
						'vip_cus'=>'<b>Cleinte VIP</b>',
						'status_cus'=>'<b>Estado</b>');
		$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_contact'=>array('justification'=>'center','width'=>40),
							'date_sal'=>array('justification'=>'center','width'=>50),
							'document_cus'=>array('justification'=>'center','width'=>65),
							'name_cus'=>array('justification'=>'center','width'=>100),
							'birthdate_cus'=>array('justification'=>'center','width'=>55),
							'phone1_cus'=>array('justification'=>'center','width'=>60),
							'cellphone_cus'=>array('justification'=>'center','width'=>60),
							'email_cus'=>array('justification'=>'center','width'=>120),
							'relative_cus'=>array('justification'=>'center','width'=>100),
							'relative_phone_cus'=>array('justification'=>'center','width'=>60),
							'vip_cus'=>array('justification'=>'center','width'=>40),
							'status_cus'=>array('justification'=>'center','width'=>60)));
	}
	elseif($allCities==1 && $allStatus==1){
		$header = array('number_contact'=>'<b># Contrato</b>',
            'date_sal'=>'<b>Fecha Venta</b>',
						'document_cus'=>'<b>Nombre y Apellido</b>',
						'name_cus'=>'<b>Nombre y Apellido</b>',
						'birthdate_cus'=>'<b>Fecha de Nacimiento</b>',
						'phone1_cus'=>utf8_decode('<b>Teléfono</b>'),
						'cellphone_cus'=>'<b>Celular</b>',
						'email_cus'=>'<b>E-Mail</b>',
						'relative_cus'=>'<b>Persona de Contacto</b>',
						'relative_phone_cus'=>utf8_decode('<b>Teléfono de Contacto</b>'),
						'vip_cus'=>'<b>Cleinte VIP</b>');
		$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_contact'=>array('justification'=>'center','width'=>40),
							'date_sal'=>array('justification'=>'center','width'=>50),
							'document_cus'=>array('justification'=>'center','width'=>65),
							'name_cus'=>array('justification'=>'center','width'=>100),
							'birthdate_cus'=>array('justification'=>'center','width'=>55),
							'phone1_cus'=>array('justification'=>'center','width'=>60),
							'cellphone_cus'=>array('justification'=>'center','width'=>60),
							'email_cus'=>array('justification'=>'center','width'=>120),
							'relative_cus'=>array('justification'=>'center','width'=>100),
							'relative_phone_cus'=>array('justification'=>'center','width'=>60),
							'vip_cus'=>array('justification'=>'center','width'=>40)));
	}
	/*END PARAMS*/
	$pdf->ezTable($customers,$header,
						$tableTitle,
						$params);
		$pdf->ezSetDy(-10);

}else{
	$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>
