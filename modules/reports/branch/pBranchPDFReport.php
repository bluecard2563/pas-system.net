<?php
/*
File: pBranchPDFReport
Author: David Bergmann
Creation Date: 04/01/2011
Last Modified:
Modified By:
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE SUCURSALES DE COMERCIALIZADOR").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dea_serial_dea = $_SESSION['serial_dea'];
}

$dealerList = Dealer::getBranchReport($db, $_POST['selCountry'], $_POST['selManager'], $_POST['selDealer'], $_POST['selResponsible'], $_POST['selStatus'], $dea_serial_dea);

if(is_array($dealerList)) {
    $titles=array('col1'=>utf8_decode('<b>Nombre</b>'),
                  'col2'=>utf8_decode('<b>Categoría</b>'),
				  'col3'=>utf8_decode('<b># visitas al mes</b>'),
				  'col4'=>utf8_decode('<b>Valor de %</b>'),
				  'col5'=>utf8_decode('<b>Ciudad</b>'),
                  'col6'=>utf8_decode('<b>Sector</b>'),
				  'col7'=>utf8_decode('<b>Dirección</b>'),
				  'col8'=>utf8_decode('<b>Teléfono</b>'),
				  'col9'=>utf8_decode('<b>Gerente</b>'),
				  'col10'=>utf8_decode('<b>Contacto</b>'),
				  'col11'=>utf8_decode('<b>Mail de contacto</b>'),
                  'col20'=>utf8_decode('<b>Responsable</b>')
    );
	
	$data = array();
	
	foreach ($dealerList as $d) {
		$aux = array('col1'=>$d['name_dea'],
					  'col2'=>$d['category_dea'],
					  'col3'=>$d['visits_number_dea'],
					  'col4'=>$d['percentage_dea'],
					  'col5'=>$d['name_cit'],
					  'col6'=>$d['name_sec'],
					  'col7'=>$d['address_dea'],
					  'col8'=>$d['phone1_dea'],
					  'col9'=>$d['manager_name_dea'],
					  'col10'=>$d['contact_dea'],
					  'col11'=>$d['email_contact_dea'],
					  'col20'=>$d['responsible']
        );
					 array_push($data, $aux);
	}



    $pdf->ezTable($data,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>80),
                                        'col2'=>array('justification'=>'center','width'=>50),
										'col3'=>array('justification'=>'center','width'=>50),
										'col4'=>array('justification'=>'center','width'=>50),
                                        'col5'=>array('justification'=>'center','width'=>50),
										'col6'=>array('justification'=>'center','width'=>50),
										'col7'=>array('justification'=>'center','width'=>110),
										'col8'=>array('justification'=>'center','width'=>60),
										'col9'=>array('justification'=>'center','width'=>90),
										'col10'=>array('justification'=>'center','width'=>90),
										'col11'=>array('justification'=>'center','width'=>90),
										'col20'=>array('justification'=>'center','width'=>90)
                                )));
        $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>