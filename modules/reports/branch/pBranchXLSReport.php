<?php
/*
File: pBranchXLSReport
Author: David Bergmann
Creation Date: 04/01/2011
Last Modified:
Modified By:
*/

//If $_SESSION['serial_dea'] is set, loads only the user's dealer
if($_SESSION['serial_dea'] && $_SESSION['serial_mbc'] != 1) {
	$dea_serial_dea = $_SESSION['serial_dea'];
}

$dealerList = Dealer::getBranchReport($db, $_POST['selCountry'], $_POST['selManager'], $_POST['selDealer'], $_POST['selResponsible'],$_POST['selStatus'], $dea_serial_dea);

$report = "<table>
           <tr>
                <th>Razon Social</th>
                 <th>Codigo</th>
                <th>Ruc</th>
                <th>Categor&iacute;a</th>
				<th># visitas al mes</th>
				<th>Valor de %</th>
				<th>Días de Crédito</th>
                <th>Ciudad</th>
				<th>Sector</th>
				<th>Direcci&oacute;n</th>
				<th>Tel&eacute;fono</th>
				<th>Tel&eacute;fono 2</th>
				<th>Representante Legal</th>
				<th>Contacto</th>
				<th>Mail de contacto</th>
				<th>Responsable</th>   
           </tr>";
foreach ($dealerList as $d) {
    $report .= "<tr>
                    <td>".$d['name_dea']."</td>
                     <td>".$d['code_dea']."</td>
                     <td>".$d['id_dea']."</td>
                    <td>".$d['category_dea']."</td>
                    <td>".$d['visits_number_dea']."</td>
					<td>".$d['percentage_dea']."</td>
					<td>".$d['days_cdd']."</td>
					<td>".$d['name_cit']."</td>
					<td>".$d['name_sec']."</td>
					<td>".$d['address_dea']."</td>
                    <td>".$d['phone1_dea']."</td>
                    <td>".$d['phone2_dea']."</td>
                    <td>".$d['manager_name_dea']."</td>
					<td>".$d['contact_dea']."</td>
					<td>".$d['email_contact_dea']."</td>
					<td>".$d['responsible']."</td>
               </tr>";
}
$report .= "</table>";

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Reporte_de_Sucursales.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $report;

?>