<?php
/*
File: pProductionCardReportXLS
Author: Lenin Padilla
Creation Date: 01/8/2018
Last Modified: 
Modified By: 
*/

set_time_limit(36000);
ini_set('memory_limit', '512M');

$sale_status = $_POST['selStatus'];
if (is_array($sale_status)) {
    foreach ($sale_status as &$s) {
        $s = "'" . $s . "'";
    }
    $sale_status = implode(',', $sale_status);
}
//Debug::print_r($_POST['selStatus'][0]);die();
if ($_POST['selStatus'][0] == "VOID") {
    $cardList = User::getUserVoid($db, $_POST['selCountry'], $sale_status, $_POST['txtBeginDate'], $_POST['txtEndDate']);

    $cardAmount = 0;
    $totalAmount = 0;
    if (is_array($cardList)) {
        $cardAmount = count($cardList);
        foreach ($cardList as $c) {
            $totalAmount += $c['col9'];
            $report = $c[9];
        }
    }

    echo '<b>REPORTE PRODUCCI&Oacute;N</b><br /><br />';

    $table = "<table border='1'>
			<tr>
				<td><b>Reporte de</b></td>
				<td>$report</td>
			</tr>
		  </table><br /><br />";

    if (is_array($cardList)) {
        $table .= "<table border='1'><tr>
            <th><b># Tarjeta</b></th>
            <th><b>Fec. Emisi&oacute;n</b></th>
            <th><b>Fec. Anulaci&oacute;n</b></th>
            <th><b>Nombre Cliente</b></th>
            <th><b>Nombre Producto</b></th>
            <th><b>Inicio Vigencia</b></th>
            <th><b>Fin Vigencia</b></th>
            <th><b>N&uacute;mero d&iacute;as</b></th>
            <th><b>Total Ventas</b></th>
            <th><b>Estado venta</b></th>
            <th><b>Comercializador</b></th>
			<th><b>Id dea</b></th>
			<th><b>Code dea</b></th>
			<th><b>Name manager</b></th>
			<th><b>responsable</b></th></tr>";

        foreach ($cardList as $cl) {
            $table .= "<tr>
                 <td>" . $cl[0] . "</td>
                 <td>" . $cl[1] . "</td>
                 <td>" . $cl[2] . "</td>
                 <td>" . $cl[3] . "</td>
                 <td>" . $cl[4] . "</td>
                 <td>" . $cl[5] . "</td>
                 <td>" . $cl[6] . "</td>
                 <td>" . $cl[7] . "</td>
                 <td>" . number_format($cl[8], 2, ',', '') . "</td>
                 <td>" . $cl[9] . "</td>
                 <td>" . $cl[10] . "</td>
                 <td>" . $cl[11] . "</td>
				 <td>" . $cl[12] . "</td>
				 <td>" . $cl[13] . "</td>
				 <td>" . $cl[14] . "</td></tr>";
        }
        $table .= "</table>";
    }

} elseif ($_POST['selStatus'][0] == "REFUNDED") {

    $cardList = User::getUserRefunded($db, $_POST['selCountry'], $sale_status, $_POST['txtBeginDate'], $_POST['txtEndDate']);

    $cardAmount = 0;
    $totalAmount = 0;
    if (is_array($cardList)) {
        $cardAmount = count($cardList);
        foreach ($cardList as $c) {
            $totalAmount += $c['col9'];
            $report = $c[9];
        }
    }

    echo '<b>REPORTE PRODUCCI&Oacute;N</b><br /><br />';

    $table = "<table border='1'>
			<tr>
				<td><b>Reporte de</b></td>
				<td>$report</td>
			</tr>
		  </table><br /><br />";

    if (is_array($cardList)) {
        $table .= "<table border='1'><tr>
            <th><b># Tarjeta</b></th>
            <th><b>Fec. Emisi&oacute;n</b></th>
            <th><b>Fec. Anulaci&oacute;n</b></th>
            <th><b>Nombre Cliente</b></th>
            <th><b>Nombre Producto</b></th>
            <th><b>Inicio Vigencia</b></th>
            <th><b>Fin Vigencia</b></th>
            <th><b>N&uacute;mero d&iacute;as</b></th>
            <th><b>Total Ventas</b></th>
            <th><b>Estado venta</b></th>
            <th><b>Comercializador</b></th>
			<th><b>Id dea</b></th>
			<th><b>Code dea</b></th>
			<th><b>Name manager</b></th>
			<th><b>responsable</b></th></tr>";

        foreach ($cardList as $cl) {
            $table .= "<tr>
                 <td>" . $cl[0] . "</td>
                 <td>" . $cl[1] . "</td>
                 <td>" . $cl[2] . "</td>
                 <td>" . $cl[3] . "</td>
                 <td>" . $cl[4] . "</td>
                 <td>" . $cl[5] . "</td>
                 <td>" . $cl[6] . "</td>
                 <td>" . $cl[7] . "</td>
                 <td>" . number_format($cl[8], 2, ',', '') . "</td>
                 <td>" . $cl[9] . "</td>
                 <td>" . $cl[10] . "</td>
                 <td>" . $cl[11] . "</td>
				 <td>" . $cl[12] . "</td>
				 <td>" . $cl[13] . "</td>
				 <td>" . $cl[14] . "</td></tr>";
        }
        $table .= "</table>";
    }
} elseif ($_POST['selStatus'][0] == "SALES") {
    $cardList_total = User::getUserAll($db, $_POST['selCountry'], $_POST['txtBeginDate'], $_POST['txtEndDate']);

    if (is_array($cardList_total)) {
        $cardAmount = count($cardList_total);
        foreach ($cardList_total as $c) {
            $totalAmount += $c['col9'];
            $report = $c[12];
        }
    }

    echo '<b>REPORTE PRODUCCI&Oacute;N</b><br /><br />';

    $table = "<table border='1'>
			<tr>
				<td><b>Reporte de Ventas</b></td>
				
			</tr>
		  </table><br /><br />";

    if (is_array($cardList_total)) {
        $table .= "<table border='1'><tr>
            <th><b># Tarjeta</b></th>
            <th><b>Serial</b></th>
            <th><b>Fec. Emisi&oacute;n</b></th>
            <th><b>Nombre Cliente</b></th>
            <th><b>Nombre Producto</b></th>
            <th><b>Inicio Vigencia</b></th>
            <th><b>Fin Vigencia</b></th>
            <th><b>N&uacute;mero d&iacute;as</b></th>
            <th><b>Total Ventas</b></th>
            <th><b>Producto</b></th>
            <th><b>Cliente</b></th>
            <th><b>Fecha de nacimiento</b></th>
            <th><b>Estado venta</b></th>
            <th><b>Comercializador</b></th>
			<th><b>Id dea</b></th>
			<th><b>Code dea</b></th>
			<th><b>Name manager</b></th>
			<th><b>responsable</b></th></tr>";

        foreach ($cardList_total as $cl) {
            $table .= "<tr>
                 <td>" . $cl[0] . "</td>
                 <td>" . $cl[1] . "</td>
                 <td>" . $cl[2] . "</td>
                 <td>" . $cl[3] . "</td>
                 <td>" . $cl[4] . "</td>
                 <td>" . $cl[5] . "</td>
                 <td>" . $cl[6] . "</td>
                 <td>" . $cl[7] . "</td>
                 <td>" . number_format($cl[8], 2, ',', '') . "</td>
                 <td>" . $cl[9] . "</td>
                 <td>" . $cl[10] . "</td>
                 <td>" . $cl[11] . "</td>
				 <td>" . $cl[12] . "</td>
				 <td>" . $cl[13] . "</td>
				 <td>" . $cl[14] . "</td>
				 <td>" . $cl[15] . "</td>
				 <td>" . $cl[16] . "</td>
				 <td>" . $cl[17] . "</td></tr>";
        }
        $table .= "</table>";
    }
}


header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteProducción.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $table;
?>
