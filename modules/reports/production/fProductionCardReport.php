<?php
/*
File: fProductionCardReport
Author: Lenin Padilla
Creation Date: 01/8/2018
Last Modified:
Modified By:
*/

//ZONE
$zone = new Zone($db);
$nameZoneList=$zone->getZones();

//STATUS
$statusList = Sales::getSalesStatus($db);


$smarty->register('nameZoneList,statusList,global_salesStatus,typeList,global_cardSalestype,stockTypeList,order_by');
$smarty->display();
?>
