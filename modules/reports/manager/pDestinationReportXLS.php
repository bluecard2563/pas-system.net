<?php
/**
 * File: pDestinationReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 14/05/2012
 * Last Modified: 14/05/2012
 * Modified By: Patricio Astudillo
 * 
 */

	$destinations = Sales::getDestinationsValues($db, $_POST['selCountry'], $_POST['txtBeginDate'], 
						$_POST['txtEndDate'], $_POST['rdoDate'], $_POST['selCity'], 
						$_POST['selManager'], $_POST['selComissionist'], $_POST['selDealer'],
						$_POST['selBranch']);
	
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title="<table>
				<tr><td colspan=6>".$date."</td></tr><tr></tr>
				<tr>
					<th></th>
					<th></th>
					<th><b>Reporte de Destinos de Viaje</b></th>
				</tr>
				<tr></tr>
			</table>";
	
	/* PARAMETERS */
	$zone = new Zone($db, $_POST['selZone']);
	$zone ->getData();
	$zone = $zone->getName_zon();
	
	$country = new Country($db, $_POST['selCountry']);
	$country ->getData();
	$country = $country->getName_cou();
	
	$manager = ManagerbyCountry::getManagerByCountryName($db, $_POST['selManager']);
	$manager = $manager?$manager:'Todos';
	
	$city = new City($db, $_POST['selCity']);
	$city ->getData();
	$city = $city->getName_cit();
	$city = $city?$city:'Todos';
	
	$user = new User($db, $_POST['selComissionist']);
	$user ->getData();
	$user = $user->getFirstname_usr().' '.$user->getLastname_usr();
	$user = ($user != ' ')?$user:'Todos';
	
	$dealer = new Dealer($db, $_POST['selDealer']);
	$dealer ->getData();
	$dealer = $dealer ->getName_dea();
	$dealer = $dealer?$dealer:'Todos';
	
	$branch = new Dealer($db, $_POST['selBranch']);
	$branch ->getData();
	$branch = $branch ->getName_dea();
	$branch = $branch?$branch:'Todos';
	
	$parameters = "<table>
						<tr>
							<td  align='right'><b>Zona:</b></td><td  align='left'>$zone</td>
							<td  align='right'><b>Pa&iacute;s:</b></td><td  align='left'>$country</td>
						</tr>
						<tr>
							<td  align='right'><b>Representante:</b></td><td  align='left'>".$manager."</td>
							<td  align='right'><b>Ciudad:</b></td><td  align='left'>".$city."</td>
						</tr>
						<tr>
							<td  align='right'><b>Responsable:</b></td><td  align='left'>".$user."</td>
						</tr>
						<tr>
							<td  align='right'><b>Comercializador:</b></td><td  align='left'>".$dealer."</td>
							<td  align='right'><b>Sucursal:</b></td><td  align='left'>".$branch."</td>
						</tr>
						<tr>
							<td  align='right'><b>Fecha Desde:</b></td><td  align='left'>".$_POST['txtBeginDate']."</td>
							<td  align='right'><b>Fecha Hasta:</b></td><td  align='left'>".$_POST['txtEndDate']."</td>
						</tr>
						<tr></tr>
					</table>";
	
	
	if($destinations)
	{		
		foreach($destinations as $d):
			$total_sold += $d['travels'];
		endforeach;
				
		$table = '<table>
					<tr>
						<td colspan="2" style="text-align: center;"><b>Destino</b></td>
						<td rowspan="2"><b>No. de Viajes</b></td>
						<td rowspan="2"><b>Porcentaje</b></td>
					</tr>
					<tr>
						<td><b>Pa&iacute;s</b></td>
						<td><b>Ciudad</b></td>
					</tr>';
		
		foreach($destinations as &$d):
			$table .= "<tr>
							<td>{$d['name_cou']}</td>
							<td>{$d['name_cit']}</td>
							<td>{$d['travels']}</td>
							<td>".number_format(($d['travels'] / $total_sold) * 100, 2)."%</td>
						</tr>";
		endforeach;
		
		$table .= "</table>";
	}
	else
	{
		$table = 'No existen registros para los filtros seleccionados';
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Destinos_viaje.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title.'<br>';
	echo $parameters.'<br>';
	echo $table;

?>
