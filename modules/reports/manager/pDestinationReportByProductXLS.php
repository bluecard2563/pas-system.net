<?php
/**
 * File: pDestinationReportXLS
 * Author: Luis Salvador
 * Creation Date: 15/01/2013
 * Last Modified: 15/01/2013
 * Modified By: Luis Salvador
 * 
 */

$destinations = Sales::getDestinationsByProduct($db, $_POST['selCountry'], $_POST['txtBeginDate'],$_POST['txtEndDate'], $_POST['rdoDate'], 
        $_POST['selManager'], $_POST['selProduct'], $_POST['selComissionist'], $_POST['selDealer'],
						$_POST['selBranch'], $_POST['selType']);
	        
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title="<table>
				<tr><td colspan=6>".$date."</td></tr><tr></tr>
				<tr>
					<th></th>
					<th></th>
					<th><b>Reporte de Destinos de Viaje por Producto</b></th>
				</tr>
				<tr></tr>
			</table>";
	
	/* PARAMETERS */
	$zone = new Zone($db, $_POST['selZone']);
	$zone ->getData();
	$zone = $zone->getName_zon();
	
	$country = new Country($db, $_POST['selCountry']);
	$country ->getData();
	$country = $country->getName_cou();
	
	$manager = ManagerbyCountry::getManagerByCountryName($db, $_POST['selManager']);
	$manager = $manager?$manager:'Todos';
	
	$parameters = "<table>
						<tr>
							<td  align='right'><b>Zona:</b></td><td  align='left'>$zone</td>
							<td  align='right'><b>Pa&iacute;s:</b></td><td  align='left'>$country</td>
						</tr>
						<tr>
							<td  align='right'><b>Representante:</b></td><td  align='left'>".$manager."</td>
						</tr>
						<tr>
							<td  align='right'><b>Fecha Desde:</b></td><td  align='left'>".$_POST['txtBeginDate']."</td>
							<td  align='right'><b>Fecha Hasta:</b></td><td  align='left'>".$_POST['txtEndDate']."</td>
						</tr>
						<tr></tr>
					</table>";
	
	
	if($destinations)
	{		
		$table = '<table>
					<tr>
						<td rowspan="2" style="text-align: center;"><b>Producto</b></td>
						<td rowspan="2" style="text-align: center;"><b>Destino</b></td>
						<td rowspan="2" style="text-align: center;"><b>No. de Viajes</b></td>
					</tr>
                                        <tr></tr>';
		
		foreach($destinations as &$d):
			$table .= "<tr>
					<td>{$d['name_pbl']}</td>
					<td>{$d['name_cou']}</td>
					<td>{$d['travels']}</td>
							
				</tr>";
		endforeach;
		
		$table .= "</table>";
	}
	else
	{
		$table = 'No existen registros para los filtros seleccionados';
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Destinos_viaje.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title.'<br>';
	echo $parameters.'<br>';
	echo $table;

?>


