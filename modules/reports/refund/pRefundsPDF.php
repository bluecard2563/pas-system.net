<?php

if( ( isset($_POST['txtCustomerData']) ) || ( isset($_POST['selCountry']) && isset($_POST['selManager']) ) ){
   
	$refund = new Refund($db);
	$refundsRaw = array();
    $refunds = array();

	if($_POST['hdnFlag']==2){
		$serial_cou = $_POST['selCountry'];
		$serial_man = $_POST['selManager'];
		$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
		$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
		$serial_usr = (isset($_POST['selResponsible'])) ? $_POST['selResponsible'] : '';
		$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
		$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
		$dateFrom = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
		$dateTo = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';

		$refundsRaw = $refund->getRefundsByManager($serial_cou,$serial_man,
													$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea'],
													$status,$serial_cit,$serial_usr,$serial_dea,$serial_bra,
													$dateFrom,$dateTo);
		}
	else{
		$serial_cus = $_POST['hdnCustomerID'];
		$refundsRaw = $refund->getRefundsByCustomer($serial_cus,$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
	}
        //debug::print_r($refundsRaw);die();
			
        if(!$refundsRaw){
            http_redirect('modules/reports/refund/fRefunds');
        }
			$table = array(
				'requestDate'=>'<b>Fecha Inicio</b>',
				'customer'=>'<b>Cliente</b>',
				'branch'=>'<b>Sucursal</b>',
				'card'=>'<b># Tarjeta</b>',
				'creditNote'=>'<b># Nota '.utf8_decode(Crédito).'</b>',
				'status'=>'<b>Estado</b>',
				'totalSal'=>'<b>Valor Tarjeta</b>',
				'retainedValue'=>'<b>Valor Retenido</b>',
				'totalPay'=>'<b>Valor a Pagar</b>',
				'authDate'=>'<b>Fecha '.utf8_decode(Autorización).'</b>'
			);
			$alignTable = array(
				'requestDate'=>array('justification'=>'center','width'=>60),
				'customer'=>array('justification'=>'center','width'=>80),
				'branch'=>array('justification'=>'center','width'=>70),
				'card'=>array('justification'=>'center','width'=>45),
				'creditNote'=>array('justification'=>'center','width'=>50),
				'status'=>array('justification'=>'center','width'=>45),
				'totalSal'=>array('justification'=>'center','width'=>50),
				'retainedValue'=>array('justification'=>'center','width'=>55),
				'totalPay'=>array('justification'=>'center','width'=>50),
				'authDate'=>array('justification'=>'center','width'=>65)
			);
			$optionsTable = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>0,
				'fontSize' => 9,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 12,
				'rowGap' => 5,
				'colGap' => 5,
				'lineCol'=>array(0.8,0.8,0.8),
				'xPos'=>'300',
				'xOrientation'=>'center',
				'maxWidth' => 805,
				'cols' =>$alignTable,
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
			);
			include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
			$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
			$pdf->setColor(0.57, 0.58, 0.60);
			$pdf->ezText($date,13,array('justification'=>'left'));
			$pdf->setColor(0, 0, 0);
			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>Reporte de Reembolsos</b>',14,array('justification'=>'center'));
			$statusRefund = 'Todos'; $responsible = 'Todos'; $city = 'Todos'; $dealer = 'Todos'; $branch = 'Todos'; $date_from = 'Todos'; $date_to = 'Todos';
			$pdf->ezSetDy(-20);
			$parameters = array();
			if($_POST['hdnFlag']==2){	
				if($serial_cit){
					$city = $refundsRaw[0]['name_cit'];
				}
				if($serial_usr){
					$responsible = $refundsRaw[0]['name_usr'];
				}
				if($serial_dea){
					$dealer = $refundsRaw[0]['name_dea'];
				}
				if($serial_bra){
					$branch = $refundsRaw[0]['name_bra'];
				}
				if($dateFrom && $date_to){
					$date_from = $dateFrom;
					$date_to = $date_to;
				}
				if($status){
					$statusRefund = $global_refundStatus[$refundsRaw[0]['status_ref']];
				}
				$parameters = array(
									'0'=>array('0'=>'<b>'.utf8_decode(País).': </b>', '1'=>$refundsRaw[0]['name_cou'], '2'=>'<b>Representante:</b>', '3'=>$refundsRaw[0]['name_man']),
									'1'=>array('0'=>'<b>Ciudad:</b>','1'=>$city,'2'=>'<b>Responsable:</b>','3'=>$responsible),
									'2'=>array('0'=>'<b>Comercializador:</b>','1'=>$dealer,'2'=>'<b>Sucursal:</b>','3'=>$branch),
									'3'=>array('0'=>'<b>Fecha Desde:</b>','1'=>$date_from,'2'=>'<b>Fecha Hasta:</b>','3'=>$date_to),
									'4'=>array('0'=>'<b>Estado:</b>','1'=>$statusRefund,'2'=>'','3'=>'')
									);
				$pdf->ezTable($parameters,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'330',
									'cols'=>array(
									'0'=>array('justification'=>'right','width'=>100),
									'1'=>array('justification'=>'left','width'=>130),
									'2'=>array('justification'=>'right','width'=>100),
									'3'=>array('justification'=>'left','width'=>130)
            )			));
			}
			else{
				$parameters[0]['0']='<b>Cliente: </b>';
				$parameters[0]['1']=$refundsRaw[0]['name_cus'];
				$pdf->ezTable($parameters,array('0'=>'','1'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,
															'fontSize' => 12,'xPos'=>'160','lineCol'=>array(0.8,0.8,0.8)));
			}
			$pdf->ezSetDy(-20);
			$dealer = '';
			$rowsTable = array();
			
            foreach ($refundsRaw as $key => $ref) {
				if($ref['serial_dea'] != $dealer){
					$pdf->ezText('<b>Reembolsos en el Comercializador: </b>'.$ref['name_dea'],12,array('justification'=>'center'));
					$pdf->ezSetDy(-20);
				}	
				$retained = '';
				if($ref['retained_type_ref'] == 'PERCENTAGE'){
					$retained = number_format($ref['retained_value_ref'], 0, '.', '').'%';
				}
				else{
					$retained = number_format($ref['retained_value_ref'], 2, '.', '');
				}
				$rowsTable[] = array(
								'requestDate'=>$ref['request_date_ref'],
								'customer'=>$ref['name_cus'],
								'branch'=>$ref['name_bra'],
								'card'=>$ref['card_number_sal'],
								'creditNote'=>$ref['number_cn'],
								'status'=>$global_refundStatus[$ref['status_ref']],
								'totalSal'=>number_format($ref['total_sal'], 2, '.', ''),
								'retainedValue'=>$retained,
								'totalPay'=>number_format($ref['total_to_pay_ref'], 2, '.', ''),
								'authDate'=>$ref['auth_date_ref']
								);
				if($refundsRaw[$key]['serial_dea'] != $refundsRaw[$key+1]['serial_dea']){
					if($_POST['hdnFlag']==1){
						unset($table['customer']);
					}
					else{
						if($serial_bra){
							unset($table['branch']);
						}
						if($status){
							unset($table['status']);
						}
					}
					$pdf->ezSetDy(-20);
					$pdf->ezTable($rowsTable, $table, NULL,$optionsTable);
					$pdf->ezSetDy(-40);
					unset($rowsTable);
				}
				$dealer = $ref['serial_dea'];
            }			
			$pdf->ezStream();
}else{
    http_redirect('modules/reports/refund/fRefunds');
}


?>