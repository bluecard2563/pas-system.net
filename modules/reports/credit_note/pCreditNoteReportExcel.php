<?php
/*
	File: pCreditNoteReportExcel
	Author: Santiago Borja
	Creation Date: 10/06/2010
	Description: Generates an Excel file with all invoices
*/

	Request::setString('hdnClientID');
	Request::setString('selManager');
	Request::setString('selDocument');
	Request::setString('txtNumInvoice');
	Request::setString('txtNumCreditNote');
	Request::setString('selCity');
	Request::setString('selDealer');
	Request::setString('selType');
	Request::setString('txtStartDate');
	Request::setString('txtEndDate');
	Request::setString('selCreditNote');
	Request::setString('selZone');
	Request::setString('selCountry');
	
	$colspan = 9;
	
	switch($selCreditNote){
		case 'CUSTOMER':
			$myCn = new CreditNote($db);
			$creditNotes = $myCn -> getCreditNotesByParameters($hdnClientID,NULL, $txtStartDate, $txtEndDate);
			
			//FILTERS
			$customer = new Customer($db, $hdnClientID);
			$customer -> getData();
			$variable_filters .= "<tr><td colspan='2' align='left'><b>Cliente:</b></td>
								<td colspan='2' align='left'>".utf8_decode($customer -> getFirstname_cus().' '.$customer -> getLastname_cus())."</td></tr>";
			break;
		case 'INVOICE':
		    $inv = new Invoice($db);

			$mbc=new ManagerbyCountry($db, $selManager);
			$mbc->getData();
			$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $mbc->getSerial_man(), $selDocument, 'INVOICE');

		    $serial_inv = $inv -> getInvoice($txtNumInvoice, $serial_dbm, false);
		    $serial_inv = $serial_inv['serial_inv'];
		    
		    if($serial_inv){
		    	$myCn = new CreditNote($db);
				$creditNotes = $myCn -> getCreditNotesByParameters(NULL,$serial_inv, $txtStartDate, $txtEndDate);
		    }
		    
		    //FILTERS
		    $manager = new Manager($db, $selManager);
			$manager -> getData();
			$variable_filters .= "<tr><td colspan='2' align='left'><b>Representante:</b></td>
								<td colspan='2' align='left'>".utf8_decode($manager -> getName_man())."</td></tr>";
			$variable_filters .= "<tr><td colspan='2' align='left'><b>Documento a:</b></td>
								<td colspan='2' align='left'>".utf8_decode($global_typeManager[$selDocument])."</td></tr>";
			$variable_filters .= "<tr><td colspan='2' align='left'><b>Factura #:</b></td>
								<td colspan='2' align='left'>".utf8_decode($txtNumInvoice)."</td></tr>";
			break;
		case 'CREDIT_NOTE':
			//TODO FILTER WITH THE REST OF THE PARAMETERS
			$creditNote = new CreditNote($db);
			$serialCN=CreditNote::getCreditNotesByNumberAndManagerByCountry($db, $selManager, $txtNumCreditNote, $selType);
			
			if(is_array($serialCN)){
				if(sizeof($serialCN)==1){
					$creditNote -> setSerial_cn($serialCN[0]['serial_cn']);

					if($creditNote -> getData()){
						$creditNotes = array(get_object_vars($creditNote));
						unset($creditNotes['db']);
						$creditNotes[0]['number_inv']=$serialCN[0]['number_inv'];
						$aux_inv=new Invoice($db, $serialCN[0]['serial_inv']);
						$aux_inv->getData();
						$aux_inv_data=$aux_inv->getAuxData();
						
						$creditNotes[0]['cus_name']=$aux_inv_data['name_cus'];
						$creditNotes[0]['date_inv']=$aux_inv->getDate_inv();
						$creditNotes[0]['total_inv']=$aux_inv->getTotal_inv();
						$creditNotes[0]['saldo']=$aux_inv->getTotal_inv()-$creditNotes['amount_cn'];

						if($creditNotes['serial_inv']!=''){
							$creditNotes[0]['concept']=utf8_decode('Anulación');
						}else if($creditNotes['serial_sal']!=''){
							$creditNotes[0]['concept']='Penalidad';
						}else{
							$creditNotes[0]['concept']='Reembolso';
						}
					}
				}
			}

			//Debug::print_r($creditNotes);
			
			//FILTERS
		    $manager = new Manager($db, $selManager);
			$manager -> getData();
			$variable_filters .= "<tr><td colspan='2' align='left'><b>Representante:</b></td>
								<td colspan='2' align='left'>".$manager -> getName_man()."</td></tr>";
			$variable_filters .= "<tr><td colspan='2' align='left'><b>Documento a:</b></td>
								<td colspan='2' align='left'>".utf8_decode($global_typeManager[$selDocument])."</td></tr>";
			$variable_filters .= utf8_decode("<tr><td colspan='2' align='left'><b>Nota de Crédito #:</b></td>
								<td colspan='2' align='left'>".$txtNumCreditNote."</td></tr>");
			break;
		case 'GENERAL':
			$myCn = new CreditNote($db);
			$creditNotes = $myCn -> getCreditNotesByParameters(NULL,NULL, $txtStartDate, $txtEndDate, $selManager, $selCity,$selDealer,$selType);

			//FILTERS
		    $manager = new Manager($db, $selManager);
			$manager -> getData();
			$zone=new Zone($db, $selZone);
			$zone -> getData();
			$country=new Country($db, $selCountry);
			$country->getData();

			if($selCity){
				$city=new City($db, $selCity);
				$city->getData();
				$city_name=$city->getName_cit();
			}else{
				$city_name='Todas';
			}

			if($selDealer){
				$dealer=new Dealer($db, $selDealer);
				$dealer->getData();
				$dealer_name=$dealer->getName_dea();
			}else{
				$dealer_name='Todos';
			}

			switch($selType){
				case '1':
					$type_name='Reembolso';
					break;
				case '2':
					$type_name = 'Anulaci&oacute;n de Factura';
					break;
				case '3':
					$type_name = 'Anulaci&oacute;n por Penalidad';
					break;
				case '4':
					$type_name = 'Incobrables';
					break;
				default:
					$type_name = 'Todos';
					break;
			}

			$variable_filters .= "<tr>
									<td align='left'><b>Zona:</b></td>
									<td align='left'>".$zone->getName_zon()."</td>
									<td align='left'>".utf8_decode("<b>País:</b>")."</td>
									<td align='left'>".$country->getName_cou()."</td>
								  </tr>";									
			$variable_filters .= "<tr>
									<td align='left'><b>Representante:</b></td>
									<td align='left'>".$manager -> getName_man()."</td>
									<td align='left'><b>Ciudad:</b></td>
									<td align='left'>".$city_name."</td>
								  </tr>";
			$variable_filters .= "<tr>
									<td align='left'><b>Comercializador:</b></td>
									<td align='left'>".$dealer_name."</td>
									<td align='left'><b>Tipo:</b></td>
									<td align='left'>".$type_name."</td>
								  </tr>";
			break;
	}
	
	if($txtStartDate){
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Fecha Desde:</b></td>
					<td colspan='2' align='left'>".utf8_decode($txtStartDate)."</td></tr>";
	}
	if($txtEndDate){
		$variable_filters .= "<tr><td colspan='2' align='left'><b>Fecha Hasta:</b></td>
					<td colspan='2' align='left'>".utf8_decode($txtEndDate)."</td></tr>";
	}
	
	//TABLE
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b># Nota</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Fecha Nota</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Concepto</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Valor</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b># Factura</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Cliente</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Fecha</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Valor Fac</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Observaciones</b></td>";
	$variable_rows .= "<td style='background-color: #CCCCCC;' align='center'><b>Representante</b></td>";
	
	$table.="<table border='0'>
				<tr><td colspan='".$colspan."' align='center'><b>Sistema Blue Card</b></td></tr>
				<tr><td colspan='".$colspan."' align='center' style='font-size: 10px; border-bottom: 1px solid #000000;'>".$_SESSION['cityForReports'].", ".date("F j, Y, g:i a")."</td></tr>
				<tr><td colspan='".$colspan."' align='center'><b>Reporte de Notas de Credito desde ".$txtStartDate." hasta ".$txtEndDate." </b></td></tr>
				<tr><td colspan='".$colspan."' align='center'>&nbsp;</td></tr>
				<tr><td colspan='".$colspan."' align='center'>&nbsp;</td></tr>
			</table>";
	
	$table.="<table border='0'>
				".$variable_filters."
			</table><br>";
	
	if($creditNotes  && count($creditNotes) > 0 ){
		//TABLE HEADER:
		$table.="<table border='1'>
				<tr>
					<td style='background-color: #CCCCCC;' colspan='4' align='center'><b>Nota de Credito</b></td>
					<td style='background-color: #CCCCCC;' colspan='6' align='center'><b>Factura</b></td>
					<td style='background-color: #CCCCCC;' rowspan='2' align='center'><b>Deducciones</b></td>
				</tr>
				<tr>
					".$variable_rows."
				</tr>";
		foreach($creditNotes as $key=>$inv){
			$variable_results = '';
			$table.="<tr>
						<td  align='center'>".$inv['number_cn']."</td>
						<td  align='center'>".$inv['date_cn']."</td>
						<td  align='center'>".$inv['concept']."</td>
						<td  align='center'>".$inv['amount_cn']."</td>
						<td  align='center'>".$inv['number_inv']."</td>
						<td  align='center'>".$inv['cus_name']."</td>
						<td  align='center'>".$inv['date_inv']."</td>
						<td  align='center'>".$inv['total_inv']."</td>
						<td  align='center'>".$inv['observations']."</td>
						<td  align='center'>".$inv['representante']."</td>
						<td  align='center'>".$inv['saldo']."</td>
					</tr>";
		}
		$table.="</table>";
	}else{
		$table.=utf8_decode("<b>No existen registros para los parámetros seleccionados</b>");
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=ReporteClientes.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $header;
	echo $table;
?>
