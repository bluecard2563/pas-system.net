<?php
/*
 * File:pCreditNoteReportPDF.php
 * Author: Santiago Borja
 * Creation Date: 10/06/2010
 * Modified By: Patricio Astudillo
 * Modificacion Date: 25/06/2010 
*/

	$headerDesc = array();
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	
	//HEADER
	$pdf->ezText(utf8_decode('<b>REPORTE DE NOTAS DE CRÉDITO</b>'), 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	Request::setString('selCreditNote');
	Request::setString('hdnClientID');
	Request::setString('selManager');
	Request::setString('selDocument');
	Request::setString('txtNumInvoice');
	Request::setString('txtNumCreditNote');
	Request::setString('selCity');
	Request::setString('selDealer');
	Request::setString('selType');
	Request::setString('txtStartDate');
	Request::setString('txtEndDate');
	Request::setString('selZone');
	Request::setString('selCountry');
	
	switch($selCreditNote){
		case 'CUSTOMER':
			$myCn = new CreditNote($db);
			$creditNotes = $myCn -> getCreditNotesByParameters($hdnClientID,NULL, $txtStartDate, $txtEndDate);
			
			//FILTERS
			$customer = new Customer($db, $hdnClientID);
			$customer -> getData();
			$descLine1 = array('col1'=>utf8_decode('<b>Cliente</b>'),
						 'col2'=>utf8_decode(utf8_encode($customer -> getFirstname_cus().' '.$customer -> getLastname_cus())));
			array_push($headerDesc,$descLine1);
			break;
		case 'INVOICE':
		    $inv = new Invoice($db);

			$mbc=new ManagerbyCountry($db, $selManager);
			$mbc->getData();
			$serial_dbm=DocumentByManager::retrieveSelfDBMSerial($db, $mbc->getSerial_man(), $selDocument, 'INVOICE');
		    
		    $serial_inv = $inv -> getInvoice($txtNumInvoice, $serial_dbm);
		    $serial_inv = $serial_inv['serial_inv'];

		    if($serial_inv){
		    	$myCn = new CreditNote($db);
				$creditNotes = $myCn -> getCreditNotesByParameters(NULL,$serial_inv, $txtStartDate, $txtEndDate);
		    }
		    
		    //FILTERS
		    $manager = new Manager($db, $selManager);
			$manager -> getData();
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Representante:</b>'),
						 'col2'=>utf8_decode($manager -> getName_man())));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Documento a:</b>'),
						 'col2'=>utf8_decode(($global_typeManagerPdf[$selDocument]))));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Factura #:</b>'),
						 'col2'=>utf8_decode($txtNumInvoice)));
			break;
		case 'CREDIT_NOTE':
			$creditNote = new CreditNote($db);
			$serialCN=CreditNote::getCreditNotesByNumberAndManagerByCountry($db, $selManager, $txtNumCreditNote, $selType);
			
			if(is_array($serialCN)){
				if(sizeof($serialCN)==1){
					$creditNote -> setSerial_cn($serialCN[0]['serial_cn']);
					
					if($creditNote -> getData()){
						$creditNotes = array(get_object_vars($creditNote));
						unset($creditNotes['db']);
						$creditNotes[0]['number_inv']=$serialCN[0]['number_inv'];
						$aux_inv=new Invoice($db, $serialCN[0]['serial_inv']);
						$aux_inv->getData();
						$aux_inv_data=$aux_inv->getAuxData();

						$creditNotes[0]['cus_name']=$aux_inv_data['name_cus'];
						$creditNotes[0]['date_inv']=$aux_inv->getDate_inv();
						$creditNotes[0]['total_inv']=$aux_inv->getTotal_inv();
						$creditNotes[0]['saldo']=$aux_inv->getTotal_inv()-$creditNotes['0']['amount_cn'];

						if($creditNotes['serial_inv']!=''){
							$creditNotes[0]['concept']=utf8_decode('Anulación');
						}else if($creditNotes['serial_sal']!=''){
							$creditNotes[0]['concept']='Penalidad';
						}else{
							$creditNotes[0]['concept']='Reembolso';
						}
					}
				}
			}
			
			//FILTERS:
		    $manager = new Manager($db, $selManager);
			$manager -> getData();
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Representante:</b>'),
						 'col2'=>$manager -> getName_man()));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Documento a:</b>'),
						 'col2'=>html_entity_decode($global_typeManager[$selDocument])));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Nota de Crédito #:</b>'),
						 'col2'=>$txtNumCreditNote));
			break;
		case 'GENERAL':
			$myCn = new CreditNote($db);
			$creditNotes = $myCn -> getCreditNotesByParameters(NULL,NULL, $txtStartDate, $txtEndDate, $selManager, $selCity,$selDealer,$selType);

			//FILTERS
		    $manager = new Manager($db, $selManager);
			$manager -> getData();
            
            if ($manager ->getName_man()){
                $name_manager = $manager ->getName_man();
            }else{
                $name_manager = 'Todos';
            }
            
			$zone=new Zone($db, $selZone);
			$zone -> getData();
			$country=new Country($db, $selCountry);
			$country->getData();

			if($selCity){
				$city=new City($db, $selCity);
				$city->getData();
				$city_name=$city->getName_cit();
			}else{
				$city_name='Todas';
			}

			if($selDealer){
				$dealer=new Dealer($db, $selDealer);
				$dealer->getData();
				$dealer_name=$dealer->getName_dea();
			}else{
				$dealer_name='Todos';
			}

			switch($selType){
				case '1':
					$type_name='Reembolso';
					break;
				case '2':
					$type_name = html_entity_decode('Anulaci&oacute;n de Factura');
					break;
				case '3':
					$type_name = html_entity_decode('Anulaci&oacute;n por Penalidad');
					break;
				case '4':
					$type_name = 'Incobrables';
					break;
				default:
					$type_name = 'Todos';
					break;
			}

			array_push($headerDesc,array('col1'=>utf8_decode('<b>Zona:</b>'),
										 'col2'=>$zone->getName_zon()));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>País:</b>'),
										 'col2'=>$country->getName_cou()));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Representante:</b>'),
//										 'col2'=>$manager -> getName_man()));
										 'col2'=>$name_manager));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Ciudad:</b>'),
										 'col2'=>$city_name));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Comercializador:</b>'),
										 'col2'=>$dealer_name));
			array_push($headerDesc,array('col1'=>utf8_decode('<b>Tipo:</b>'),
										 'col2'=>$type_name));
			break;
	}
	
	if($txtStartDate){
		array_push($headerDesc,array('col1'=>utf8_decode('<b>Fecha Desde</b>'),
						 'col2'=>utf8_decode($txtStartDate)));
	}
	if($txtEndDate){
		array_push($headerDesc,array('col1'=>utf8_decode('<b>Fecha Hasta</b>'),
				 'col2'=>utf8_decode($txtEndDate)));
	}
	
	//TABLE
	$header['number_cn'] = '<b># Nota</b>';
	$header['date_cn'] = '<b>Fecha Nota</b>';
	$header['concept'] = '<b>Concepto</b>';
	$header['amount_cn'] = '<b>Valor</b>';
	$header['number_inv'] = '<b># Factura</b>';
	$header['cus_name'] = '<b>Cliente</b>';
	$header['date_inv'] = '<b>Fecha</b>';
	$header['total_inv'] = '<b>Valor Fac</b>';
	$header['observations'] = '<b>Observaciones</b>';
	$header['saldo'] = '<b>Deduciones</b>';
    $header['representante'] = '<b>Representante</b>';
	
	//BODY:
	$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150))));
	$pdf->ezSetDy(-10);
	//END HEADERS

	/*PARAMS FOR CREATING PDF FILE*/
	$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_cn'=>array('justification'=>'center','width'=>50),
							'date_cn'=>array('justification'=>'center','width'=>60),
							'concept'=>array('justification'=>'center','width'=>60),
							'amount_cn'=>array('justification'=>'center','width'=>50),
							'number_inv'=>array('justification'=>'center','width'=>50),
							'cus_name'=>array('justification'=>'center','width'=>120),
							'date_inv'=>array('justification'=>'center','width'=>60),
							'total_inv'=>array('justification'=>'center','width'=>50),
							'observations'=>array('justification'=>'center','width'=>120),
							'saldo'=>array('justification'=>'center','width'=>60),
                            'representante'=>array('justification'=>'center','width'=>60)));
	
	if(is_array($creditNotes) && count($creditNotes) > 0 ){
		$pdf->ezTable($creditNotes,$header,$tableTitle,$params);
		$pdf->ezSetDy(-10);
	}else{
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}
	
	$pdf->ezStream();
?>