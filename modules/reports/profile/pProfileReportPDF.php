<?php
/*
File:pProfileReportPDF
Author: Esteban Angulo
Creation Date: 21/07/2010
*/
//echo "PDF".'<br/>';

$profile=new ProfileByCountry($db);

$allOptions=$profile->show_options($_SESSION['language']);

$data=$profile->getprofile_by_countrys($_POST['selCountry']);

if(is_array($data)){
	foreach($data as &$d){
		$profile->setSerial_pbc($d['serial_pbc']);
		//Debug::print_r($profile);
		$d['options'][]=$profile->show_options($_SESSION['language']);
	}

	$hasOptions=array();
	foreach($data as &$prf){
		$prf['options']=$prf['options']['0'];
	}
//Debug::print_r($data);
	//Debug::print_r(generateReport($allOptions,$data,$hasOptions));
	$info=generateReport($allOptions,$data,$hasOptions);
}

/***********************************************
@Name: generateReport
@Description: Runs over the hole allOptions array and calls the function search
@Params: $allOptions -> All Options array
 *		 $data-> Array with all profiles with its own options
 *		 $hasOptions-> Array that will be returned with all data processed
@Returns: $hasOptions -> Array with all the options of the system and which profile has each option
***********************************************/
function generateReport($allOptions,$data,&$hasOptions,$tab='') {
	foreach($allOptions as $all){
		$aux['name']=html_entity_decode($tab).utf8_decode(utf8_encode($all['name_obl']));
		array_push($hasOptions,search($all,$data,$aux));
		if(is_array($all['options'])){
			generateReport($all['options'],$data,$hasOptions,$tab.'&#09;&#09;&#09;&#09;&#09;');
		}
	}
	return $hasOptions;
}


/***********************************************
@Name: search
@Description: Runs over all profile array and calls the function find Options
@Params: $all -> All Options array
 *		 $data-> Array with all profiles with its own options
 *		 $aux-> Array that will be returned with all data processed
@Returns: $aux -> Array with all the options of the a profile 
***********************************************/
function search($all,$data,$aux){
	foreach($data as $prf){
		if(findOptions($all['serial_opt'],$prf['options'])){
			$aux[$prf['serial_pbc']]='X';
		}
		else{
			$aux[$prf['serial_pbc']]=' ';
		}
	}
	return $aux;
}


/***********************************************
@Name: findOptions
@Description: Compare the searched option with the options that the profile has
@Params: $serial_opt -> Serached Option
 *		 $optByPrf-> Array with all Options of the profile
@Returns: true-> if found
 *		  false-> if not found
***********************************************/
function findOptions($serial_opt, $optByPrf){
	$return = false;
	if(is_array($optByPrf)){
		foreach($optByPrf as $key=>$opt){
			if($serial_opt == $opt['serial_opt']){
				$return = true;
				break;
			} else {
				if(is_array($opt['options'])){
					if(findOptions($serial_opt, $opt['options'])){
						$return = true;
						break;
					}
				}
			}
		}
	}
	return $return;
}
//Debug::print_r($info);

$zone=new Zone($db);
$zone->setSerial_zon($_POST['selZone']);
$zone->getData();
$country=new Country($db);
$country->setSerial_cou($_POST['selCountry']);
$country->getData();


/**PDF**/
include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
//HEADER
$pdf->ezText('<b>REPORTE DE PERFILES</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);
$headerDesc=array();
$descLine1=array('col1'=>utf8_decode('<b>Zona:</b>'),
				 'col2'=>$zone->getName_zon());
array_push($headerDesc,$descLine1);
$descLine2=array('col1'=>utf8_decode('<b>País:</b>'),
				 'col2'=>$country->getName_cou());
array_push($headerDesc,$descLine2);
$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150))));
$pdf->ezSetDy(-10);
//END HEADER
if(is_array($info)){
	/******AUX Arrays for PDF*/
	$cols['name']=array('justification'=>'left','width'=>220);
	$header['name']=utf8_decode('<b>Nombre de la opción del Menú</b>');

$profileList=array();
	foreach($data as $key=>$p){
		$key=$key+1;
//		$keys[]=$key;
//		$profileList[$key]=utf8_decode('<b>'.$p['name_pbc'].'</b>');
		$descLine3=array('col1'=>$key,
						 'col2'=>utf8_decode($p['name_pbc']));
		array_push($profileList,$descLine3);
		$header[$p['serial_pbc']]=utf8_decode('<b>'.$key.'</b>');
		$cols[$p['serial_pbc']]=array('justification'=>'center','width'=>20);
	}
	if($key<=30){
		$pdf->ezSetDy(-10);
		$pdf->ezText(utf8_decode('<b>Descripción de Perfiles</b>'), 11, array('justification'=>'left','left'=>10));
		$pdf->ezSetDy(-10);
		$pdf->ezTable($profileList,
				  array('col1'=>utf8_decode('<b>#</b>'),'col2'=>'<b>Nombre del Perfil</b>'),
				  '',
				  array('showHeadings'=>1,
						'shaded'=>0,
						'showLines'=>0,
						'xPos'=>'200',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>40),
							'col2'=>array('justification'=>'left','width'=>250))));


				$params = array('showHeadings'=>1,
								'shaded'=>1,
								'showLines'=>2,
								'xPos'=>'center',
								'innerLineThickness' => 0.8,
								'outerLineThickness' => 0.8,
								'fontSize' => 8,
								'titleFontSize' => 8,
								'cols'=>$cols);

		$pdf->ezSetDy(-20);
		$pdf->ezTable($info,$header,$tableTitle,$params);
		$pdf->ezSetDy(-10);
	}
	else{
		$pdf->ezText('<b>El Reporte es demasiado extenso, no puede ser mostrado</b>', 12, array('justification'=>'center'));
	}
}
else{
	$pdf->ezText(utf8_decode('<b>NO EXISTEN PERFILES PARA EL PAÍS SELECCIONADO</b>'), 12, array('justification'=>'center'));
}
$pdf->ezStream();
?>
