<?php
/*
 * File: fManualVirtualCardsRelation.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2011, 11:43 AM
 */

if($_SESSION['serial_mbc'] != 1){
	$serial_mbc = $_SESSION['serial_mbc'];
}else{
	$serial_mbc = NULL;
}

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$country = new Country($db);
$countryList = $country->getCountriesWithSales($db, $_SESSION['countryList'], $serial_mbc, $serial_dea);
if($countryList){
	$smarty->register('countryList');
}
$smarty->display();
?>
