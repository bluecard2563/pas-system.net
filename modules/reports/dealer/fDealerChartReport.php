<?php
/*
 * File: fDealerChartReport.php
 * Author: Patricio Astudillo
 * Creation Date: 29/06/2010, 04:43:55 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 29/06/2010, 04:43:55 PM
 */

$zone=new Zone($db);
if($_SESSION['serial_mbc']!=1){
	$serial_mbc=$_SESSION['serial_mbc'];
}else{
	$serial_mbc=NULL;
}
$zoneList=$zone->getDealerZones($_SESSION['countryList'], $serial_mbc);
$today=date('d/m/Y');
$first_day='01/01/2009';

$smarty->register('zoneList,today,first_day');
$smarty->display();
?>
