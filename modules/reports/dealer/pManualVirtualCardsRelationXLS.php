<?php
/*
 * File: pManualVirtualCardsRelationXLS.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2011, 04:18 PM
 */

Request::setString('selCountry,selCity,selManager,selResponsible,txtBeginDate,txtEndDate');

$sales = new Sales($db);
$cardsSoldInfo = $sales->getCardsSoldByCountry($db, $selCountry, $txtBeginDate, $txtEndDate, $selCity, $selManager, $selResponsible);
if($cardsSoldInfo){
	//MAIN HEADER
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=Relacion_Tarjetas_Virtuales_Manuales.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
		$title="<table>
					<tr>
						<th></th>
						<th colspan=6><b>Reporte de Relaci&oacute;n Tarjetas Virtuales y Manuales</b></th>
					</tr>
					<tr></tr>
					<tr>
						<th></th>
						<td colspan=6>".$date."</td></tr><tr>
					</tr>
				</table>";
		echo $title;

	//DATES SELECTED
		$selectedDates="<table>
							<tr>
								<td></td>
								<td><b>Fecha Seleccionada: </b></td>
								<td>".$txtBeginDate." - ".$txtEndDate."</td>
							</tr>
						</table>";
		echo $selectedDates;

	//COUNTRY NAME
		$country = new Country($db, $selCountry);
		$country->getData();
		$countryName = $country->getName_cou();

	//LOOPING INFO TO PRINT
		$dealersList = "<table border='1'><tr>".
								"<th><b>Sucursal</b></th>".
								"<th><b>Cantidad Virtuales</b></th>".
								"<th><b>Cantidad Manuales</b></th>"
							;
		$virtualCardsByResponsible = 0; $manualCardsByResponsible = 0;
		$perVirtualCardsByResponsible = 0; $perManualCardsByResponsible = 0;
		foreach($cardsSoldInfo as $key=>&$cardsRelation){
			//MAKING DEALER'S ARRAY
					$virtualTotal = 0;
					$manualTotal = 0;
					if($cardsRelation['stock_type_sal'] == 'VIRTUAL'){
						$virtualTotal = $cardsRelation['total_cards'];
					}
					else{
						$manualTotal = $cardsRelation['total_cards'];
					}

					//CHECKING NEXT ITEM IN ARRAY $cardsSoldInfo, ITS THE SAME AS THIS ITERATION
						$next = $cardsSoldInfo[$key+1];
							if($next['serial_dea'] == $cardsRelation['serial_dea']){
								if($next['stock_type_sal'] == 'VIRTUAL'){
									$virtualTotal = $next['total_cards'];
								}
								else{
									$manualTotal = $next['total_cards'];
								}
								unset($cardsSoldInfo[$key+1]);
							}
					//ADDING TO VAR
						$dealersList .= "<tr>
								<th>".$cardsRelation['name_dea']."</th>
								<th>".$virtualTotal."</th>
								<th>".$manualTotal."</th>
							   </tr>";
						$virtualCardsByResponsible += $virtualTotal;
						$manualCardsByResponsible += $manualTotal;
						
				if(!$cardsSoldInfo[$key+1]){
					$next = $cardsSoldInfo[$key+2];
				}

				//PRINTING DEALER'S LIST
					if($next['serial_usr'] != $cardsRelation['serial_usr']){
						//RESUME BY RESPONSIBLE
							$resumeLocation="<table>
										<tr></tr>
										<tr>
											<td><b>Pa&iacute;s: </b></td><td>".$countryName."</td>
											<td><b>Ciudad: </b></td><td>".$cardsRelation['name_cit']."</td>
										</tr>
										<tr>
											<td><b>Representante: </b></td><td>".$cardsRelation['name_man']."</td>
											<td><b>Responsable: </b></td><td>".$cardsRelation['name_usr']."</td>
										</tr>
									</table>";
							echo $resumeLocation;

							$perVirtualCardsByResponsible = number_format(round($virtualCardsByResponsible*100/($virtualCardsByResponsible+$manualCardsByResponsible), 2), 2);
							$perManualCardsByResponsible = number_format(round($manualCardsByResponsible*100/($virtualCardsByResponsible+$manualCardsByResponsible), 2), 2);

							$resumeCards="<table>
											<tr></tr>
											<tr>
												<td></td>
												<td><b>Cantidad</b></td>
												<td><b>Porcentaje</b></td>
											</tr>
											<tr>
												<td>N&uacute;mero de Tarjetas Vendidas</td>
												<td>".($virtualCardsByResponsible+$manualCardsByResponsible)."</td>
												<td>100.00%</td>
											</tr>
											<tr>
												<td>Virtuales</td>
												<td>".$virtualCardsByResponsible."</td>
												<td>".$perVirtualCardsByResponsible."%</td>
											</tr>
											<tr>
												<td>Manuales</td>
												<td>".$manualCardsByResponsible."</td>
												<td>".$perManualCardsByResponsible."%</td>
											</tr>
										</table>";
							echo $resumeCards;

							$virtualCardsByResponsible = 0; $manualCardsByResponsible = 0;
							$perVirtualCardsByResponsible = 0; $perManualCardsByResponsible = 0;

						//DEALER'S LIST
							$dealersList .= "</table>";
							echo $dealersList;
							$dealersList = "<table border='1'><tr>".
								"<th><b>Sucursal</b></th>".
								"<th><b>Cantidad Virtuales</b></th>".
								"<th><b>Cantidad Manuales</b></th>"
							;
					}
		}
}
else{
	http_redirect('modules/reports/dealer/fManualVirtualCardsRelation');
}

?>
