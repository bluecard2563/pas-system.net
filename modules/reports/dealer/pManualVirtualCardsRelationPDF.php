<?php
/*
 * File: pManualVirtualCardsRelationPDF.php
 * Author: Gabriela Guerrero
 * Creation Date: 04/05/2011, 04:18 PM
 */

Request::setString('selCountry,selCity,selManager,selResponsible,txtBeginDate,txtEndDate');

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

$sales = new Sales($db);
$cardsSoldInfo = $sales->getCardsSoldByCountry($db, $selCountry, $txtBeginDate, $txtEndDate, $selCity, $selManager, $selResponsible);
if($cardsSoldInfo){//Debug::print_r($cardsSoldInfo);die();
	//COUNTRY NAME
		$country = new Country($db, $selCountry);
		$country->getData();
		$countryName = $country->getName_cou();

	//TITLE
		$pdf->setColor(0, 0, 0);
		$pdf->ezSetDy(-20);
		$pdf->ezText(utf8_decode('<b>Reporte de Relación de Tarjetas Virtuales y Manuales</b>'),14,array('justification'=>'center'));
		$pdf->ezSetDy(-20);

	//DATE
		$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
		$pdf->ezText($date,10,array('justification'=>'center'));

	//DATES SELECTED
		$pdf->ezSetDy(-20);
		$pdf->ezText('<b>Fecha Seleccionada: </b>'.$txtBeginDate.' - '.$txtEndDate, 10, array('justification'=>'center'));
		/*$parameters = array('0'=>array('0'=>'<b>Fecha Inicio:</b>','1'=>$txtBeginDate),
							'1'=>array('0'=>'<b>Fecha Fin:</b>','1'=>$txtEndDate)
						);
		$pdf->ezTable($parameters,array('0'=>'','1'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'250',
								'cols'=>array(
								'0'=>array('justification'=>'left','width'=>100),
								'1'=>array('justification'=>'left','width'=>130)
		)			));*/

		$pdf->ezSetDy(-40);

	//BODY

		//DEFINING TABLE FORMAT FOR DELAER'S LIST
			$colsAux = array(
							'dealer'=>'<b>Sucursal</b>',
							'total_virtual'=>'<b>Cantidad Virtuales</b>',
							'total_manual'=>'<b>Cantidad Manuales</b>'
						);
			$alignCols= array(
							'dealer'=>array('justification'=>'center','width'=>180),
							'total_virtual'=>array('justification'=>'center','width'=>120),
							'total_manual'=>array('justification'=>'center','width'=>120)
						);
			$options = array(
							'showLines'=>2,
							'showHeadings'=>1,
							'shaded'=>0,
							'fontSize' => 10,
							'textCol' =>array(0,0,0),
							'titleFontSize' => 12,
							'rowGap' => 5,
							'colGap' => 5,
							'lineCol'=>array(0.8,0.8,0.8),
							'xPos'=>'center',
							'xOrientation'=>'center',
							'maxWidth' => 400,
							'cols' =>$alignCols,
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8
						);

		//LOOPING INFO TO PRINT
			$dealersList = array();
			$virtualCardsByResponsible = 0; $manualCardsByResponsible = 0;
			$perVirtualCardsByResponsible = 0; $perManualCardsByResponsible = 0;
			foreach($cardsSoldInfo as $key=>&$cardsRelation){
				//MAKING DEALER'S ARRAY
					$virtualTotal = 0;
					$manualTotal = 0;
					if($cardsRelation['stock_type_sal'] == 'VIRTUAL'){
						$virtualTotal = $cardsRelation['total_cards'];
					}
					else{
						$manualTotal = $cardsRelation['total_cards'];
					}

					//CHECKING NEXT ITEM IN ARRAY $cardsSoldInfo, ITS THE SAME AS THIS ITERATION
						$next = $cardsSoldInfo[$key+1];
							if($next['serial_dea'] == $cardsRelation['serial_dea']){
								if($next['stock_type_sal'] == 'VIRTUAL'){
									$virtualTotal = $next['total_cards'];
								}
								else{
									$manualTotal = $next['total_cards'];
								}
								unset($cardsSoldInfo[$key+1]);
							}
					//ADDING TO ARRAY
						$dealersList[] = array(
											'dealer' => $cardsRelation['name_dea'],
											'total_virtual' => $virtualTotal,
											'total_manual' => $manualTotal
										);
						$virtualCardsByResponsible += $virtualTotal;
						$manualCardsByResponsible += $manualTotal;

				if(!$cardsSoldInfo[$key+1]){
					$next = $cardsSoldInfo[$key+2];
				}
				//PRINTING DEALER'S LIST
					if($next['serial_usr'] != $cardsRelation['serial_usr']){
						//RESUME BY RESPONSIBLE
							$header = array('0'=>array('0'=>utf8_decode('<b>País:</b>'),'1'=>$countryName,'2'=>'<b>Ciudad:</b>','3'=>$cardsRelation['name_cit']),
										'1'=>array('0'=>'<b>Representante:</b>','1'=>$cardsRelation['name_man'],'2'=>'<b>Responsable:</b>','3'=>$cardsRelation['name_usr'])
											);
							$pdf->ezTable($header,array('0'=>'','1'=>'','2'=>'','3'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'330',
													'cols'=>array(
													'0'=>array('justification'=>'right','width'=>100),
													'1'=>array('justification'=>'left','width'=>130),
													'2'=>array('justification'=>'right','width'=>100),
													'3'=>array('justification'=>'left','width'=>130)
							)			));
							$pdf->ezSetDy(-20);
							$perVirtualCardsByResponsible = number_format(round($virtualCardsByResponsible*100/($virtualCardsByResponsible+$manualCardsByResponsible), 2), 2);
							$perManualCardsByResponsible = number_format(round($manualCardsByResponsible*100/($virtualCardsByResponsible+$manualCardsByResponsible), 2), 2);

							$resume = array('0'=>array('0'=>'', '1'=>'<b>Cantidad</b>','2'=>'<b>Porcentaje</b>'),
											'1'=>array('0'=>  utf8_decode('Número de Tarjetas Vendidas'), '1'=>($virtualCardsByResponsible+$manualCardsByResponsible),'2'=>'100.00%'),
											'2'=>array('0'=>'Virtuales', '1'=>$virtualCardsByResponsible,'2'=>$perVirtualCardsByResponsible.'%'),
											'3'=>array('0'=>'Manuales', '1'=>$manualCardsByResponsible,'2'=>$perManualCardsByResponsible.'%')
											);
							$pdf->ezTable($resume,array('0'=>'','1'=>'','2'=>''),'',array('showHeadings'=>0,'shaded'=>0,'showLines'=>0,'fontSize' => 10,'xPos'=>'330',
													'cols'=>array(
													'0'=>array('justification'=>'left','width'=>160),
													'1'=>array('justification'=>'center','width'=>100),
													'2'=>array('justification'=>'center','width'=>100)
							)			));
							$pdf->ezSetDy(-20);
							$virtualCardsByResponsible = 0; $manualCardsByResponsible = 0;
							$perVirtualCardsByResponsible = 0; $perManualCardsByResponsible = 0;
							
						//DEALER'S LIST
							$pdf->ezTable($dealersList, $colsAux, NULL, $options);
							$pdf->ezSetDy(-20);
							$dealersList = array();
					}
			}
	
	$pdf->ezStream();
}
else{
	 http_redirect('modules/reports/dealer/fManualVirtualCardsRelation');
}
?>
