<?php
/*
 * File: fDealerEffectivenessReport.php
 * Author: Gabriela Guerrero
 * Creation Date: 05/05/2011, 03:54 PM
 */

Request::setInteger('0:error');

if($_SESSION['serial_mbc'] != 1){
	$serial_mbc = $_SESSION['serial_mbc'];
}else{
	$serial_mbc = NULL;
}

if(isset ($_SESSION['serial_dea']) ){
	$serial_dea = $_SESSION['serial_dea'];
}else{
	$serial_dea = NULL;
}

$country = new Country($db);
$countryList = $country->getCountriesWithAssignedDealers($db, $_SESSION['countryList'], $serial_mbc, $serial_dea);
if($countryList){
	$smarty->register('countryList');
}
$smarty->register('error');
$smarty->display();
?>
