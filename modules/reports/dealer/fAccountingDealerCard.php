<?php
/**
 * File: fAccountingDealerCard
 * Author: Patricio Astudillo
 * Creation Date: 26-jul-2012
 * Last Modified: 26-jul-2012
 * Modified By: Patricio Astudillo
 */

	$zone = new Zone($db);
	$zone_list = $zone->getZones();
	
	$smarty->register('zone_list');
	$smarty->display();
?>
