<?php
/**
 * File: pAccountingDealerCardXLS
 * Author: Patricio Astudillo
 * Creation Date: 26-jul-2012
 * Last Modified: 26-jul-2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selManager');
	Request::setString('selDealer');
	Request::setString('selBranch');
	
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('txtCxCFrom');
	
	$accounting_chart = array();
	
	$cxc_list = Payment::getAccountingLimitedSalesInformation($db, $selManager, $txtBeginDate, 
																$selDealer, $selBranch);
	
	if(is_array($cxc_list)){
		foreach($cxc_list as $cxc_item){
			$accounting_chart[$cxc_item['main_id']] = array();
			
			$accounting_chart[$cxc_item['main_id']]['name_dea'] = $cxc_item['main_name'];
			$accounting_chart[$cxc_item['main_id']]['seller'] = $cxc_item['seller'];
			$accounting_chart[$cxc_item['main_id']]['in_values'] = $cxc_item['in_values'];
			$accounting_chart[$cxc_item['main_id']]['due_values'] = $cxc_item['due_values'];
			
			$accounting_chart[$cxc_item['main_id']]['sales'] = 0;
				
			$accounting_chart[$cxc_item['main_id']]['payments'] = 0;
		}
	}
	
	$sales_list = Payment::getAccountingRangeSalesInformation($db, $selManager, $txtBeginDate, 
															$txtEndDate, $selDealer, $selBranch);
	
	if(is_array($sales_list)){
		foreach($sales_list as $sale_item){
			if(array_key_exists($sale_item['main_id'], $accounting_chart)){
				$accounting_chart[$sale_item['main_id']]['sales'] = $sale_item['pending_amounts'];
			}else{
				$accounting_chart[$sale_item['main_id']] = array();
			
				$accounting_chart[$sale_item['main_id']]['name_dea'] = $sale_item['main_name'];
				$accounting_chart[$sale_item['main_id']]['seller'] = $sale_item['seller'];
				$accounting_chart[$sale_item['main_id']]['in_values'] = 0;
				$accounting_chart[$sale_item['main_id']]['due_values'] = 0;
				
				$accounting_chart[$sale_item['main_id']]['sales'] = $sale_item['pending_amounts'];
			}
			
		}
	}
	
	$payments_list = Payment::getAccountingPaymentInformation($db, $selManager, $txtBeginDate, 
															$txtEndDate, $selDealer, $selBranch);
	
	if(is_array($payments_list)){
		foreach($payments_list as $pay_item){
			if(array_key_exists($pay_item['main_id'], $accounting_chart)){
				$accounting_chart[$pay_item['main_id']]['payments'] += $pay_item['total_payed_pay'];
			}else{
				$accounting_chart[$pay_item['main_id']] = array();
			
				$accounting_chart[$pay_item['main_id']]['name_dea'] = $pay_item['main_name'];
				$accounting_chart[$pay_item['main_id']]['seller'] = $pay_item['seller'];
				$accounting_chart[$pay_item['main_id']]['in_values'] = 0;
				$accounting_chart[$pay_item['main_id']]['due_values'] = 0;
				
				$accounting_chart[$pay_item['main_id']]['sales'] = 0;
				
				$accounting_chart[$pay_item['main_id']]['payments'] = $pay_item['total_payed_pay'];
			}
			
		}
	}
	
	$displaced_list = Payment::getAccountingDisplacedInvoicePaymentInformation($db, $selManager, $txtBeginDate, $txtEndDate,
															$selDealer, $selBranch);
	ksort($accounting_chart);
	
	/*********************************** DISPLAYING THE XLS *****************************************/
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = '<table>
					<tr><td colspan=6>'.$date.'</td></tr>
					<tr></tr>
					<tr>
						<th></th>
						<th><b>Reporte de An&aacute;lisis de Cartera</b></th>
					</tr>
					<tr></tr>
			  </table>';

	//APLIED FILTERS
	$filters = "<table>
					<tr>
						<td><b>Representante:</b></td>
						<td>".ManagerbyCountry::getManagerByCountryName($db, $selManager)."</td>
					</tr>

					<tr>
						<td><b>Fecha Desde:</b></td>
						<td>$txtBeginDate</td>
						<td><b>Fecha Hasta:</b></td>
						<td>$txtEndDate</td>
					</tr>
				</table><br />";
	
	
	if(is_array($accounting_chart)){
		$table = "<table>
					<tr>
						<td rowspan='2'><b>Comercializador</b></td>
						<td rowspan='2'><b>Responsable</b></td>
						<td colspan='2'><b>Cuentas por Cobrar</b></td>
						<td rowspan='2'><b>Ventas</b></td>
						<td rowspan='2'><b>Cobros</b></td>
						<td rowspan='2'><b>CxC</b></td>
					</tr>
					<tr>
						<td><b>Dentro de Cr&eacute;dito</b></td>
						<td><b>Cartera Vencida</b></td>
					</tr>";
		
		foreach($accounting_chart as $key=>&$item){
			$cxc_values = ($item['in_values'] + $item['due_values'] + $item['sales']) - $item['payments'];
			
			if($cxc_values < 0 && $displaced_list){
				foreach($displaced_list as $dp){
					if($key == $dp['main_id']){
						if($dp['type_payment'] == 'IN'){
							$item['in_values'] += $dp['total_inv'];
						}else{
							$item['due_values'] += $dp['total_inv'];
						}
						
						$cxc_values = ($item['in_values'] + $item['due_values'] + $item['sales']) - $item['payments'];
						
						break;
					}
				}
			}

			$cxc_values = number_format($cxc_values, 2);
			
			$table .= "<tr>
							<td>{$item['name_dea']}</td>
							<td>{$item['seller']}</td>
							<td>{$item['in_values']}</td>
							<td>{$item['due_values']}</td>
							<td>{$item['sales']}</td>
							<td>{$item['payments']}</td>
							<td>{$cxc_values}</td>
						</tr>";
		}
		
		$table .= "</table>";
	}else{
		$table = 'No existen registros para los filtros seleccionados';
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Analisis_Cartera.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $filters;
	echo $table;
?>
