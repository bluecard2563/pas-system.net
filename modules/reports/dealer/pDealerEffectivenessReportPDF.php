<?php

/*
 * File: pDealerEffectivenessReportPDF.php
 * Author: Gabriela Guerrero
 * Creation Date: 06/05/2011, 11:26 AM
 */

Request::setString('selCountry,selCity,selManager,selResponsible,txtBeginDate,txtEndDate');

include_once DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';

$sales = new Sales($db);
$cardsSoldInfo = $sales->getVirtualCardsSoldInfoByDealer($db, $selCountry, $txtBeginDate, $txtEndDate, $selCity, $selManager, $selResponsible);
if ($cardsSoldInfo) {
	//COUNTRY NAME
	$country = new Country($db, $selCountry);
	$country->getData();
	$countryName = $country->getName_cou();

	//TITLE
	$pdf->setColor(0, 0, 0);
	$pdf->ezSetDy(-20);
	$pdf->ezText(utf8_decode('<b>Reporte de Efectividad de Responsables</b>'), 14, array('justification' => 'center'));
	//DATE
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$pdf->ezText($date, 10, array('justification' => 'center'));

	//DATES SELECTED
	$pdf->ezSetDy(-10);
	$pdf->ezText('<b>Fecha Seleccionada: </b>' . $txtBeginDate . ' - ' . $txtEndDate, 10, array('justification' => 'left'));
	$pdf->ezSetDy(-30);

	//BODY
	//DEFINING TABLE FORMAT FOR DELAER'S LIST
	$colsAux = array(
		'number' => '<b>Ranking</b>',
		'dealer' => '<b>Sucursal</b>',
		'total_virtual' => '<b>Cantidad</b>',
		'sales_amount' => '<b>Ventas</b>',
		'sold' => html_entity_decode('<b>Vendi&oacute;</b>')
	);
	$alignCols = array(
		'number' => array('justification' => 'center', 'width' => 60),
		'dealer' => array('justification' => 'center', 'width' => 180),
		'total_virtual' => array('justification' => 'center', 'width' => 80),
		'sales_amount' => array('justification' => 'center', 'width' => 80),
		'sold' => array('justification' => 'center', 'width' => 60)
	);
	$options = array(
		'showLines' => 2,
		'showHeadings' => 1,
		'shaded' => 0,
		'fontSize' => 10,
		'textCol' => array(0, 0, 0),
		'titleFontSize' => 12,
		'rowGap' => 5,
		'colGap' => 5,
		'lineCol' => array(0.8, 0.8, 0.8),
		'xPos' => 'center',
		'xOrientation' => 'center',
		'cols' => $alignCols,
		'innerLineThickness' => 0.8,
		'outerLineThickness' => 0.8
	);
	//LOOPING INFO TO PRINT
	$dealersList = array(); //debug::print_r($cardsSoldInfo);die();
	$dealersNumber = 0;
	$dealersWhoSold = 0;
	$bestDealer = '';
	foreach ($cardsSoldInfo as $key => &$dealer) {
		//PRINTING MANAGER HEADER
		if ($dealer['serial_mbc'] != $cardsSoldInfo[$key - 1]['serial_mbc']) {
			$ranking = 1;
			$manager = array('0' => array('0' => $countryName, '1' => $dealer['name_cit']));
			$pdf->ezTable(	$manager, 
							array(	'0' => html_entity_decode('<b>Pa&iacute;s</b>'), 
									'1' => '<b>Ciudad</b>'), 
							'<b>Representante: </b>'.$dealer['name_man'], 
							array(	'showHeadings' => 1, 
									'shaded' => 0, 
									'showLines' => 2, 
									'fontSize' => 10, 
									'titleFontSize' => 11,
									'xPos' => 'center',
									'xOrientation' => 'center',
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'cols' => array(
										'0' => array('justification' => 'center', 'width' => 100),
										'1' => array('justification' => 'center', 'width' => 130))
									)				
			);
			$pdf->ezSetDy(-20);
		}
		
		//ADDING TO ARRAY
		$sold = 'NO';
		$dealersNumber++;
		if ($dealer['total_cards'] > 0) {
			$sold = 'SI';
			$dealersWhoSold++;
		}
		$dealersList[] = array(
			'number' => $ranking++,
			'dealer' => $dealer['name_dea'],
			'total_virtual' => $dealer['total_cards'],
			'sales_amount' => $dealer['sales_amount'],
			'sold' => $sold
		);
		
		if($bestDealer == ''){
			$bestDealer = $dealer['name_dea'];
		}
		

		//PRINTING RESPONSIBLE WITH DEALERS
		if ($dealer['serial_usr'] != $cardsSoldInfo[$key + 1]['serial_usr'] || $dealer['serial_mbc'] != $cardsSoldInfo[$key + 1]['serial_mbc']) {
			//RESPONSIBLE EFFECTIVENESS
			$effectivenessPercentage = number_format(round($dealersWhoSold * 100 / $dealersNumber, 2), 2);
			$effectiveness = array(	'0' => array('0' => '# de comercializadores', 
												 '1' => $dealersNumber, 
												 '2' => '100.00%'),
									'1' => array('0' => '# de comercializadores que vendieron', 
												 '1' => $dealersWhoSold, 
												 '2' => $effectivenessPercentage . '%')
			);
			$pdf->ezTable(	$effectiveness, 
							array(	'0' => '', 
									'1' => '<b>Cantidad</b>', 
									'2' => '<b>Porcentaje</b>'), 
							'<b>Responsable: </b>'.$dealer['name_usr'], 
							array(	'showHeadings' => 1, 
									'shaded' => 0, 
									'showLines' => 2, 
									'fontSize' => 10, 
									'titleFontSize' => 11,
									'xPos' => 'center',
									'xOrientation' => 'center',
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'cols' => array(
										'0' => array('justification' => 'left', 'width' => 150),
										'1' => array('justification' => 'center', 'width' => 70),
										'2' => array('justification' => 'center', 'width' => 70)
			)));
			
			$bestDealerTable = array('0' => array('0' => 'Mejor sucursal de comercializador', '1' => $bestDealer));
			$pdf->ezTable(	$bestDealerTable, 
							array(	'0' => '', 
									'1' => ''), 
							'', 
							array(	'showHeadings' => 0, 
									'shaded' => 0, 
									'showLines' => 1, 
									'fontSize' => 10, 
									'outerLineThickness' => 0.8,
									'xPos' => 'center',
									'xOrientation' => 'center',
									'cols' => array(
										'0' => array('justification' => 'left', 'width' => 150),
										'1' => array('justification' => 'center', 'width' => 140)
									)
							)
			);
			$bestDealer = '';
			$pdf->ezSetDy(-20);

			//DEALER'S LIST
			$pdf->ezTable($dealersList, $colsAux, '<b>Detalle de Sucursales</b>', $options);
			$pdf->ezSetDy(-20);

			$dealersList = array();
			$dealersNumber = 0;
			$dealersWhoSold = 0;
			$bestDealer = '';
		}
	}

	$pdf->ezStream();
} else {
	http_redirect('modules/reports/dealer/fDealerEffectivenessReport/1');
}
?>
