<?php
/*
 * File: pDealerChartReportPDF.php
 * Author: Patricio Astudillo
 * Creation Date: 30/06/2010, 01:54:00 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 14/06/2012, 01:54:00 PM
 */

	Request::setString('selBranch');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('selCountry');
	$dealer = new Dealer($db, $selBranch);

	if ($dealer->getData()) {
		/*	 * ************************************************
		* ************* SALES SECTION *********************
		* *************************************************
		* Retrieves all the Sales information of a       *
		* specific branch according to it's NET          *
		* and GROSS sales during the current year,       *
		* and the last year. The report can be modified  *
		* if the user enters a period of time in the	  *
		* reports interface.							  *
		* ************************************************* */
		if ($txtBeginDate != '' && $txtEndDate != '') {
			$datesText = "Reporte entre el " . $txtBeginDate . " y el " . $txtEndDate;

			$beginDateArray = explode('/', $txtBeginDate);
			$endDateArray = explode('/', $txtEndDate);
		} else {
			$beginDateArray = array('0' => '01', '1' => '01', '2' => date('Y'));

			if ((int) date('m') > 1) {
				$month = strtotime('-1 month', strtotime(date('Y-m-d')));
				$month = date('Y-m-d', $month);
				$month = explode('-', $month);
				$month = $month['1'];
			} else {
				$month = date('m');
			}

			$endDateArray = array('0' => date('d'), '1' => $month, '2' => date('Y'));
			$datesText = "Reporte entre el 01/01/" . date('Y') . " y el " . $endDateArray['0'] . '/' . $endDateArray['1'] . '/' . $endDateArray['2'];
		}

		$salesList = Sales::getDealerSalesForChartReport($db, $selBranch, $beginDateArray, $endDateArray);
		//Debug::print_r($salesList);
		/*	 * ************ END SALES SECTION **************** */
		$total_current_sales = array('NET' => 0, 'GROSS' => 0);
		$total_old_sales = array('NET' => 0, 'GROSS' => 0);
		;

		if ($salesList) {
			$total_bonus_paid = 0;
			$total_bonus_pending = 0;
			$global_sales_detail = array();
			$assistances = array();

			foreach ($salesList as &$s) {
				/* NET SALES */
				$eTaxes = unserialize($s['applied_taxes_inv']);
				$eTaxesAmount = 0;
				if (is_array($eTaxes)) {
					foreach ($eTaxes as $t) {
						$eTaxesAmount+=$t['tax_percentage'];
					}
					$eTaxesAmount = $eTaxesAmount / 100;
				}

				$net_sale = 0;
				$eDiscounts = $s['discounts'] / 100;
				$net_sale = $s['total_sal'] - ($s['total_sal'] * $eDiscounts);
				$net_sale+=($net_sale * $eTaxesAmount);
				$s['net_sale'] = $net_sale;
				/* END NET */

				$month = explode('/', $s['emission_date_sal']);
				$month = (int) $month['1'];

				if ($s['type'] == 'CURRENT') {
					/* GETTING THE BONUS INFORMATIONS BY SALE */
					$s['bonus_paid'] = 0;
					$s['bonus_pending'] = 0;
					$auxBonus = AppliedBonus::getBonusInfoBySale($db, $s['serial_sal']);
					//Debug::print_r($$auxBonus);
					if (is_array($auxBonus)) {
						foreach ($auxBonus as $a) {
							$s['bonus_paid']+=$a['amount_paid'];
							$total_bonus_paid+=$a['amount_paid'];
							$s['bonus_pending']+=$a['pending_amount_apb'];
							$total_bonus_pending+=$a['pending_amount_apb'];
						}
						//echo 'Pagado: '.$total_bonus_paid.' Por Pagar: '.$total_bonus_pending.'<br>';
					}
					/* END INFORMATION */

					/* GENERAL SALES AMOUNTS */
					$global_values[$month]['net_current']+=$net_sale;
					$global_values[$month]['gross_current']+=$s['total_sal'];
					$total_net_current+=$net_sale;
					$total_gross_current+=$s['total_sal'];
					/* END GENERAL */

					/* SALES GLOBAL DETAIL */
					$global_sales_detail[$s['serial_pro']]['name_pbl'] = $s['name_pbl'];
					$global_sales_detail[$s['serial_pro']]['quantity']+=1;
					$global_sales_detail[$s['serial_pro']]['net_value']+=$net_sale;
					$global_sales_detail[$s['serial_pro']]['gross_value']+=$s['total_sal'];
					if ($s['has_refunds'] == 'YES') {
						$global_sales_detail[$s['serial_pro']]['refunds']+=1;
					} else {
						$global_sales_detail[$s['serial_pro']]['refunds']+=0;
					}
					
					$global_sales_detail[$s['serial_pro']]['reserve_paid']+=$s['total_paid'];
					/* END DETAIL */
				} else {
					/* GENERAL SALES AMOUNTS */
					$global_values[$month]['net_previous']+=$net_sale;
					$global_values[$month]['gross_previous']+=$s['total_sal'];
					$total_net_previous+=$net_sale;
					$total_gross_previous+=$s['total_sal'];
					/* END GENERAL */
				}
			}

			/* CALCULATE INCREASE PERCENTAGE */
			if (is_array($global_values)) {
				$formed_global_array = array();
				foreach ($global_values as $key => &$gv) {
					if ($gv['net_previous']) {
						$gv['net_percentage'] = number_format((($gv['net_current'] - $gv['net_previous']) / $gv['net_previous']) * 100, 2);
						$gv['gross_percentage'] = number_format((($gv['gross_current'] - $gv['gross_previous']) / $gv['gross_previous']) * 100, 2);
					} else {
						$gv['net_previous'] = number_format(0, 2);
						$gv['gross_previous'] = number_format(0, 2);

						$gv['net_percentage'] = 'N/A';
						$gv['gross_percentage'] = 'N/A';
					}

					array_push($formed_global_array, array('month' => $global_weekMonths[$key],
						'orderingWeight' => $key,
						'net_previous' => number_format($gv['net_previous'], 2),
						'net_current' => number_format($gv['net_current'], 2),
						'net_percentage' => $gv['net_percentage'],
						'extra' => $global_weekMonths[$key],
						'gross_previous' => number_format($gv['gross_previous'], 2),
						'gross_current' => number_format($gv['gross_current'], 2),
						'gross_percentage' => $gv['gross_percentage']));
				}
			}

			if ($total_net_previous) {
				$total_net_increase = number_format((($total_net_current - $total_net_previous) / $total_net_previous) * 100, 2);
				$total_gross_increase = number_format((($total_gross_current - $total_gross_previous) / $total_gross_previous) * 100, 2);
			} else {
				$total_net_increase = 'N/A';
				$total_gross_increase = 'N/A';
			}

			array_push($formed_global_array, array('month' => '<b>TOTAL</b>',
				'orderingWeight' => 13,
				'net_previous' => number_format($total_net_previous, 2),
				'net_current' => number_format($total_net_current, 2),
				'net_percentage' => $total_net_increase,
				'extra' => '<b>TOTAL</b>',
				'gross_previous' => number_format($total_gross_previous, 2),
				'gross_current' => number_format($total_gross_current, 2),
				'gross_percentage' => $total_gross_increase));
			/* END PERCENTAGE */
		}

		/* VISIT ARRAY FORMATION */
		$visitList = DealerVisit::getVisitsForDealerChartReport($db, $selBranch);
		if (sizeof($visitList) > 0) {
			foreach ($visitList as &$a) {
				$a['type_vis'] = html_entity_decode($global_visit_type[$a['type_vis']]);
			}
		}
		/* END VISIT FORMATION */

		include_once DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';

		//HEADER
		$pdf->ezText('<b>FICHA DE LA SUCURSAL DE COMERCIALIZADOR</b>', 12, array('justification' => 'center'));
		$pdf->ezSetDy(-10);
		$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . $global_weekDaysNumb[$date['weekday']] . ' ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);
		$pdf->ezText($date, 10, array('justification' => 'center'));
		$pdf->ezSetDy(-20);

		$pdf->ezText('<b>COMERCIALIZADOR: ' . $dealer->getName_dea() . '</b>', 12, array('justification' => 'center'));
		$pdf->ezSetDy(-10);
		$pdf->ezText($datesText, 10, array('justification' => 'center'));
		$pdf->ezSetDy(-10);

		/* SALES GLOBALS */
		$pdf->ezText('<b>VENTAS</b>', 12, array('justification' => 'center'));
		$pdf->ezSetDy(-5);

		//FIX: WE HAVE TO ORDER THE MONTHS:
		for ($i = 0; $i < count($formed_global_array) - 1; $i++) {
			for ($j = $i; $j < count($formed_global_array); $j++) {
				if ($formed_global_array[$i]['orderingWeight'] > $formed_global_array[$j]['orderingWeight']) {
					$temp = $formed_global_array[$i];
					$formed_global_array[$i] = $formed_global_array[$j];
					$formed_global_array[$j] = $temp;
				}
			}
		}

		if (sizeof($formed_global_array) > 0) {
			$blogCustomer = array();
			$titles = array('col1' => '<b>Ventas Netas</b>',
				'col2' => '<b>Ventas Brutas</b>');
			array_push($blogCustomer, $titles);
			$pdf->ezTable($blogCustomer, array('col1' => '',
				'col2' => ''), '', array('showHeadings' => 0,
				'shaded' => 0,
				'showLines' => 2,
				'xPos' => '300',
				'fontSize' => 9,
				'titleFontSize' => 11,
				'cols' => array(
					'col1' => array('justification' => 'center', 'width' => 230),
					'col2' => array('justification' => 'center', 'width' => 230)
					)));
			$pdf->ezSetDy(-2);
			$pdf->ezTable($formed_global_array, array('month' => '',
				'net_previous' => ($beginDateArray['2'] - 1),
				'net_current' => $beginDateArray['2'],
				'net_percentage' => '%',
				'extra' => '',
				'gross_previous' => ($beginDateArray['2'] - 1),
				'gross_current' => $beginDateArray['2'],
				'gross_percentage' => '%'), '', array('showHeadings' => 1,
				'shaded' => 0,
				'showLines' => 2,
				'xPos' => '300',
				'fontSize' => 9,
				'titleFontSize' => 9,
				'cols' => array(
					'month' => array('justification' => 'center', 'width' => 65),
					'net_previous' => array('justification' => 'center', 'width' => 55),
					'net_current' => array('justification' => 'center', 'width' => 55),
					'net_percentage' => array('justification' => 'center', 'width' => 55),
					'extra' => array('justification' => 'center', 'width' => 65),
					'gross_previous' => array('justification' => 'center', 'width' => 55),
					'gross_current' => array('justification' => 'center', 'width' => 55),
					'gross_percentage' => array('justification' => 'center', 'width' => 55)
					)));
		} else {
			$pdf->ezText('No se tienen ventas registradas para este comercializador', 10, array('justification' => 'center'));
			$pdf->ezSetDy(-10);
		}
		/* END SALES GLOBAL */

		/* VISIT SECTION */
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>VISITAS</b>', 12, array('justification' => 'center'));
		//Debug::print_r($visitList);

		if (sizeof($visitList) > 0) {
			$pdf->ezSetDy(-3);

			$pdf->ezTable($visitList, array('start_vis' => '<b>Fecha</b>',
				'type_vis' => '<b>Motivo</b>',
				'comments_obv' => '<b>Observaciones</b>',
				'name_usr' => '<b>Visitado por</b>'), '', array('showHeadings' => 1,
				'shaded' => 0,
				'showLines' => 2,
				'xPos' => '300',
				'fontSize' => 9,
				'titleFontSize' => 9,
				'cols' => array(
					'start_vis' => array('justification' => 'center', 'width' => 90),
					'type_vis' => array('justification' => 'center', 'width' => 120),
					'comments_obv' => array('justification' => 'center', 'width' => 120),
					'name_usr' => array('justification' => 'center', 'width' => 90))
			));
		} else {
			$pdf->ezText('No existen visitas registradas a este comercializador', 10, array('justification' => 'center'));
		}
		/* END VISIT SECTION */

		/* BONUS SECTION */
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>INCENTIVOS</b>', 12, array('justification' => 'center'));
		$pdf->ezSetDy(-3);

		$bonusInfo = array();
		$bonus_line1 = array('col1' => '<b>Total Pagado: </b>',
			'col2' => '$ ' . number_format($total_bonus_paid, 2));
		array_push($bonusInfo, $bonus_line1);

		$bonus_line2 = array('col1' => '<b>Pendiente de Pago: </b>',
			'col2' => '$ ' . number_format($total_bonus_pending, 2));
		array_push($bonusInfo, $bonus_line2);

		$pdf->ezTable($bonusInfo, array('col1' => '',
			'col2' => ''), '', array('showHeadings' => 0,
			'shaded' => 0,
			'showLines' => 0,
			'xPos' => '176',
			'fontSize' => 9,
			'titleFontSize' => 11,
			'cols' => array(
				'col1' => array('justification' => 'left', 'width' => 110),
				'col2' => array('justification' => 'left', 'width' => 80)
				)));
		/* END BONUS SECTION */

		/* ASSISTANCES SECTION */
		$assistances = File::getAllFilesForBranchCard($db, $selBranch, $txtBeginDate, $txtEndDate, $_SESSION['serial_lang']);
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>ASISTENCIAS</b>', 12, array('justification' => 'center'));

		if ($assistances) {
			$total_paid = 0;
			foreach ($assistances as &$al) {
				$al['status_fle'] = utf8_decode(html_entity_decode($global_fileStatus[$al['status_fle']]));
				$total_paid += $al['authorized'];
			}
			
			array_push($assistances, array('name_cus' => '', 'card_number_sal'=>'', 'name_pbl'=>'', 'status_fle'=>'<b>TOTAL</b>', 'authorized'=> $total_paid));
		
			$pdf->ezSetDy(-3);
			$pdf->ezTable($assistances, array('creation_date_fle' => utf8_decode(html_entity_decode('<b>Fecha de Creaci&oacute;n</b>')),
				'name_cus' => '<b>Cliente</b>',
				'card_number_sal' => '<b>Tarjeta</b>',
				'name_pbl' => '<b>Producto</b>',
				'status_fle' => '<b>Estado</b>',
				'authorized' => '<b>Valor Pagado</b>'), '', array('showHeadings' => 1,
				'shaded' => 0,
				'showLines' => 2,
				'xPos' => '300',
				'fontSize' => 9,
				'titleFontSize' => 9,
				'cols' => array(
					'creation_date_fle' => array('justification' => 'center', 'width' => 90),
					'name_cus' => array('justification' => 'center', 'width' => 120),
					'card_number_sal' => array('justification' => 'center', 'width' => 55),
					'name_pbl' => array('justification' => 'center', 'width' => 55),
					'status_fle' => array('justification' => 'center', 'width' => 55),
					'total_paid' => array('justification' => 'center', 'width' => 60))
			));
		} else {
			$pdf->ezText('No se tienen asistencias registradas para este comercializador', 10, array('justification' => 'center'));
			$pdf->ezSetDy(-10);
		}
		/* END ASSISTANCES SECTION */


		/* SALES DETAIL */
		if (sizeof($global_sales_detail) > 0) {
            $qty = $net_value = $gross_value = $refunds = $reserve_pending = $reserve_paid = 0;
			
			//ASSISTANCE PAYMENTS FOR DEALER AND PRODUCT
			$payment_info = File::getFilePaymentPercentage($db, $txtBeginDate, $txtEndDate, $selCountry, $_SESSION['serial_lang'], 'DEALER_PRODUCT', $selBranch);
			
			foreach ($global_sales_detail as $key => &$gs) {
				if($payment_info){
					foreach($payment_info as $pi){
						if($pi['serial_pro'] == $key){
							$gs['reserve_paid'] = $pi['authorized'];
						}
					}
				}
				
                $qty += $gs['quantity'];
                $net_value += $gs['net_value'];
                $gross_value += $gs['gross_value'];
                $refunds += $gs['refunds'];
                $reserve_paid += $gs['reserve_paid'];
                
				$gs['net_value'] = number_format($gs['net_value'], 2);
				$gs['gross_value'] = number_format($gs['gross_value'], 2);
				$gs['reserve_paid'] = number_format($gs['reserve_paid'], 2);
			}
			
			
            $global_sales_detail[] = array(
                'name_pbl' => '<b>TOTAL</b>',
				'quantity' => $qty,
				'net_value' => number_format($net_value, 2),
				'gross_value' => number_format($gross_value, 2),
				'refunds' => $refunds,
				'reserve_pending' => number_format($reserve_pending, 2),
				'reserve_paid' => number_format($reserve_paid, 2)
            );
            
			$pdf->ezSetDy(-10);
			$pdf->ezText('<b>DETALLE DE VENTAS</b>', 12, array('justification' => 'center'));
			$pdf->ezSetDy(-3);
			$pdf->ezTable($global_sales_detail, array('name_pbl' => '<b>Producto</b>',
				'quantity' => '<b>Cantidad</b>',
				'net_value' => '<b>Valor Neto</b>',
				'gross_value' => '<b>Valor Bruto</b>',
				'refunds' => '<b>Reembolsadas</b>',
				'reserve_paid' => '<b>Reclamos Pagados</b>'), '', array('showHeadings' => 1,
				'shaded' => 0,
				'showLines' => 2,
				'xPos' => '300',
				'fontSize' => 9,
				'titleFontSize' => 9,
				'cols' => array(
					'name_pbl' => array('justification' => 'center', 'width' => 90),
					'quantity' => array('justification' => 'center', 'width' => 55),
					'net_value' => array('justification' => 'center', 'width' => 55),
					'gross_value' => array('justification' => 'center', 'width' => 55),
					'refunds' => array('justification' => 'center', 'width' => 80),
					'reserve_paid' => array('justification' => 'center', 'width' => 55)
					)));
		}
		/* END SALES DETAIL */
		$pdf->ezStream();
	} else {
		http_redirect('modules/reports/dealer/fDealerChartReport/1');
	}
?>