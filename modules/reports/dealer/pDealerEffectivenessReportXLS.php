<?php

/*
 * File: pDealerEffectivenessReportXLS.php
 * Author: Gabriela Guerrero
 * Creation Date: 08/05/2011, 09:43 PM
 */

Request::setString('selCountry,selCity,selManager,selResponsible,txtBeginDate,txtEndDate');

$sales = new Sales($db);
$cardsSoldInfo = $sales->getVirtualCardsSoldInfoByDealer($db, $selCountry, $txtBeginDate, $txtEndDate, $selCity, $selManager, $selResponsible);
$cardsSoldInfoLastYear = $sales->getVirtualCardsSoldInfoByDealer($db, $selCountry, $txtBeginDate, $txtEndDate, $selCity, $selManager, $selResponsible, true);

if ($cardsSoldInfo) {
	//MAIN HEADER
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Efectividad_Responsables.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table>
					<tr>
						<th colspan=6><b>Reporte de Efectividad de Responsables</b></td>
					</tr>
					<tr></tr>
					<tr>
						<td></td>
						<td colspan=6>" . $date . "</td></tr><tr>
					</tr>
				</table>";
	echo $title;

	//DATES SELECTED
	$selectedDates = "<table>
							<tr>
								<td></td>
								<td><b>Fecha Seleccionada: </b></td>
								<td>" . $txtBeginDate . " - " . $txtEndDate . "</td>
							</tr>
						</table>";
	echo $selectedDates;

	//COUNTRY NAME
	$country = new Country($db, $selCountry);
	$country->getData();
	$countryName = $country->getName_cou();

	//BODY
	//LOOPING INFO TO PRINT
	$dealersList = "<table border='1'>
								<tr>" .
									"<td><b>Ranking</b></td>" .
									"<td><b>Sucursal</b></td>" .
									"<td><b>Cantidad Actual</b></td>" .
                                    "<td><b>Cantidad Anterior</b></td>" .
									"<td><b>Ventas Actuales</b></td>" .
                                    "<td><b>Ventas Anteriores</b></td>" .
									"<td><b>Vendi&oacute;</b></td>";
	$dealersNumber = 0;
	$dealersWhoSold = 0;
	$bestDealer = '';
	foreach ($cardsSoldInfo as $key => &$dealer) {
		//PRINTING MANAGER HEADER
		if ($dealer['serial_mbc'] != $cardsSoldInfo[$key - 1]['serial_mbc']) {
			$ranking = 1;
			$manager = "<table>
								<tr>
									<td colspan='2' align='center'>
										<b>Representante: </b> {$dealer['name_man']}
									</td>
								</tr>
								<tr>
									<td><b>Pa&iacute;s</b></td>
									<td><b>Ciudad</b></td>
								</tr>
								<tr>
									<td>" . $countryName . "</td>
									<td>" . $dealer['name_cit'] . "</td>
								</tr>
						</table><br />";
			echo $manager;
		}
		
		//ADDING TO ARRAY
		$sold = 'NO';
		$dealersNumber++;
		if ($dealer['total_cards'] > 0) {
			$sold = 'SI';
			$dealersWhoSold++;
		}
        $lastYearSales = 0;
        $lastYearQty = 0;
        //SEARCH IF DEALER HAS SALES IN PRVIOUS YEAR ARRAY
        foreach($cardsSoldInfoLastYear as $lastYearDealerSales){
            if($lastYearDealerSales['serial_dea'] == $dealer['serial_dea']){
                $lastYearSales = $lastYearDealerSales['sales_amount'];
                $lastYearQty = $lastYearDealerSales['total_cards'];
                break;
            }
        }
        
        
        
		$dealersList .= "<tr>
							<td>" . $ranking++ . "</td>
							<td>" . $dealer['name_dea'] . "</td>
							<td>" . $dealer['total_cards'] . "</td>
                            <td>" . $lastYearQty . "</td>
							<td>" . $dealer['sales_amount'] . "</td>
                            <td>" . $lastYearSales . "</td>
							<td>" . $sold . "</td>
					   </tr>";
		if($bestDealer == ''){
			$bestDealer = $dealer['name_dea'];
		}

		//PRINTING RESPONSIBLE WITH DEALERS
		if ($dealer['serial_usr'] != $cardsSoldInfo[$key + 1]['serial_usr'] || $dealer['serial_mbc'] != $cardsSoldInfo[$key + 1]['serial_mbc']) {
			//RESPONSIBLE EFFECTIVENESS
			$effectivenessPercentage = number_format(round($dealersWhoSold * 100 / $dealersNumber, 2), 2);
			$responsible = "<table>
									<tr>
										<td colspan='3' align='center'>
											<b>Responsable: </b>" . $dealer['name_usr'] . "
										</td>
									</tr>
									<tr>
										<td></td>
										<td><b>Cantidad</b></td>
										<td><b>Porcentaje</b></td>
									</tr>
									<tr>
										<td># de comercializadores</td>
										<td>$dealersNumber</td>
										<td>100.00%</td>
									</tr>
									<tr>
										<td># de comercializadores que vendieron</td>
										<td>$dealersWhoSold</td>
										<td>$effectivenessPercentage%</td>
									</tr>
									<tr>
										<td colspan='2'>Mejor sucursal de comercializador: </td>
										<td>$bestDealer</td>
									</tr>
							</table><br />";
			echo $responsible;

			//DEALER'S LIST
			$dealersList .= "</table><br />";
			echo $dealersList;

			$dealersList = "<table border='1'>
								<tr>" .
									"<td><b>Ranking</b></td>" .
									"<td><b>Sucursal</b></td>" .
									"<td><b>Cantidad Actual</b></td>" .
                                    "<td><b>Cantidad Anterior</b></td>" .
									"<td><b>Ventas Actuales</b></td>" .
                                    "<td><b>Ventas Anteriores</b></td>" .
									"<td><b>Vendi&oacute;</b></td>";
			
			$dealersNumber = 0;
			$dealersWhoSold = 0;
			$bestDealer = '';
		}
	}
} else {
	http_redirect('modules/reports/dealer/fDealerEffectivenessReport/1');
}
?>
