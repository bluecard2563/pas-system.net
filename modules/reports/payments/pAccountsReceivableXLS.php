<?php

set_time_limit(36000);
ini_set('memory_limit', '1024M');

if (isset($_POST['selZone']) && isset($_POST['selCountry']) && isset($_POST['selManager'])) {
	$invoice = new Invoice($db);
	$invoicesRaw = array();

	$serial_zon = (isset($_POST['selZone'])) ? $_POST['selZone'] : '';
	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$date_from = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
	$date_to = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';
	$days = (isset($_POST['selDays'])) ? $_POST['selDays'] : '';
	$category_dea = (isset($_POST['selCategory'])) ? $_POST['selCategory'] : '';
	if (isset($_POST['selComissionist'])) {
		$comissionist = $_POST['selComissionist'];
		$user = new User($db, $comissionist);
		$user->getData();
		$comissionist_name = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
	} else {
		$comissionist = '';
	}

	$invoicesRaw = $invoice->getNotPayedInvoices($serial_zon, $serial_cou, $serial_man, $_SESSION['serial_mbc'], $_SESSION['dea_serial_dea'], $serial_cit, $serial_dea, $serial_bra, $date_from, $date_to, $days, $category_dea, $comissionist);
	//debug::print_r($invoicesRaw);die();
	if (!$invoicesRaw) {
		http_redirect('modules/reports/payments/fAccountsReceivable');
	}

	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$main_header = "<table>
							<tr><td colspan=6>" . $date . "</td></tr>
							<tr></tr>
							<tr>
								<td colspan='6' align='center'><b>Reporte de Cuentas por Cobrar</b></td>
							</tr>
						</table>";

	$requiredFields = array(
		array('0' => '<b>Zona: </b>' . $invoicesRaw[0]['name_zon'],
			'1' => html_entity_decode('<b>Pa&iacute;s: </b>') . $invoicesRaw[0]['name_cou']),
		array('0' => '<b>Representante: </b>' . $invoicesRaw[0]['name_man'])
	);

	$line1 = array();
	//DEALER FILTER
	$dealer_info = html_entity_decode('<b>Comercializador: </b>');
	($serial_dea) ? $dealer_info.=$invoicesRaw[0]['name_dea'] : $dealer_info.='Todos';
	array_push($line1, $dealer_info);
	//BRANCH FILTER
	$branch_info = html_entity_decode('<b>Sucursal: </b>');
	($serial_bra) ? $branch_info.=$invoicesRaw[0]['name_bra'] : $branch_info.='Todos';
	array_push($line1, $branch_info);
	array_push($requiredFields, $line1);

	$line2 = array();
	//CITY FILTER
	$city_info = html_entity_decode('<b>Ciudad: </b>');
	($serial_cit) ? $city_info.=$invoicesRaw[0]['name_cit'] : $city_info.='Todos';
	array_push($line2, $city_info);
	//COMISSIONIST FILTER
	$comissionist_info = html_entity_decode('<b>Responsable: </b>');
	($comissionist_name) ? $comissionist_info.=$comissionist_name : $comissionist_info.='Todos';
	array_push($line2, $comissionist_info);
	array_push($requiredFields, $line2);

	$line3 = array();
	//CATEGORY FILTER
	$category_info = html_entity_decode('<b>Categor&iacute;a: </b>');
	($category_dea) ? $category_info.=$category_dea : $category_info.='Todos';
	array_push($line3, $category_info);
	//DAYS FILTER
	$days_info = html_entity_decode('<b>D&iacute;as Hasta: </b>');
	if ($days) {
		$days = $days + 29;
		if ($days == 120) {
			$days = '90+';
		}
		$days_info.=$days;
	} else {
		$days_info.='N/A';
	}
	array_push($line3, $days_info);
	array_push($requiredFields, $line3);

	$line4 = array();
	//BEGIN DATE FILTER
	$beginDate_info = html_entity_decode('<b>Fecha Desde: </b>');
	($date_from) ? $beginDate_info.=$date_from : $beginDate_info.='N/A';
	array_push($line4, $beginDate_info);
	//END DATE FILTER
	$endDate_info = html_entity_decode('<b>Fecha Hasta: </b>');
	($date_to) ? $endDate_info.=$date_to : $endDate_info.='N/A';
	array_push($line4, $endDate_info);
	array_push($requiredFields, $line4);


	$filters_table = "<table>";
	foreach ($requiredFields as $row) {
		$filters_table.="<tr>";
		foreach ($row as $item) {
			$filters_table.="<td>$item</td>";
		}
		$filters_table.="</tr>";
	}
	$filters_table.="</table><br />";

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Cuentas_por_Cobrar.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $main_header;
	echo $filters_table;
	echo $parameters;

	//GRAND TOTAL CALCULATION
	$grandTotal = 0;
	$totalInTime = 0;
	$totalUntil30 = 0;
	$totalUntil60 = 0;
	$totalUntil90 = 0;
	$totalPlus90 = 0;
	foreach ($invoicesRaw as $key => $inv) {
		if($inv['days_status'] == 'OUT'){
			if ($inv['diff'] <= 30) {
				$totalUntil30 += $inv['total_inv'];
			} elseif ($inv['diff'] > 30 && $inv['diff'] <= 60) {
				$totalUntil60 += $inv['total_inv'];
			} elseif ($inv['diff'] > 60 && $inv['diff'] <= 90) {
				$totalUntil90 += $inv['total_inv'];
			} else {
				$totalPlus90 += $inv['total_inv'];
			}
		}else{
			$totalInTime += $inv['total_inv'];
		}
		
		$grandTotal += $inv['total_inv'];
	}
	echo "<table>
			<tr>
				<th></th>
			</tr>
			<tr>
				<th align='left'><b>Total dentro del cr&eacute;dito: </b>" . number_format($totalInTime, 2, '.', ',') . "</th>
			</tr>
			<tr>
				<th align='left'><b>Total vencido hasta 30 d&iacute;as: </b>" . number_format($totalUntil30, 2, '.', ',') . "</th>
			</tr>
			<tr>
				<th align='left'><b>Total vencido hasta 60 d&iacute;as: </b>" . number_format($totalUntil60, 2, '.', ',') . "</th>
			</tr>
			<tr>
				<th align='left'><b>Total vencido hasta 90 d&iacute;as: </b>" . number_format($totalUntil90, 2, '.', ',') . "</th>
			</tr>
			<tr>
				<th align='left'><b>Total vencido m&aacute;s de 90 d&iacute;as: </b>" . number_format($totalPlus90, 2, '.', ',') . "</th>
			</tr>
			<tr>
				<th align='left'><b>Total a Cobrar: </b>" . number_format($grandTotal, 2, '.', ',') . "</th>
			</tr>
		</table>";
	$branch = '';
	$total_for_branch = 0;

	//debug::print_r($invoicesRaw);die();
	foreach ($invoicesRaw as $key => $inv) {
		if ($inv['serial_bra'] != $branch) {
			$amm30 = 0;
			$amm60 = 0;
			$amm90 = 0;
			$amm91 = 0;
			$ammIn = 0;
			
			$branchHeader = "<table><tr><th></th>";
			$branchHeader.="<th><b>Comercializador: </b>" . $inv['name_dea'] . "</th></tr>";
			$branchHeader.="<tr><th></th><th><b>Sucursal: </b>" . $inv['name_bra'] . "</th></tr></table>";
			echo $branchHeader;
			$invoices = "<table border='1'><tr>" .
					html_entity_decode("<th><b>Representante</b></th>") .
					html_entity_decode("<th><b>Responsable</b></th>") .
					"<th><b>Comercializador</b></th>" .
					"<th><b>Sucursal</b></th>" .
					"<th><b>Fecha</b></th>" .
					"<th><b># Factura / # Tarjeta</b></th>" .
					html_entity_decode("<th><b>Facturado a / Tel&eacute;fono</b></th>") .
					"<th><b>Impago</b></th>" .
					/* "<th><b>D&iacute;as de Cr&eacute;dito</b></th>". */
					"<th><b>Dentro del Cr&eacute;dito</b></th>";

			switch ($days) {
				case '30':
					$invoices.="<th><b>Vencido hasta 30 d&iacute;as</b></th></tr>";
					break;
				case '60':
					$invoices.="<th><b>Vencido hasta 60 d&iacute;as</b></th></tr>";
					break;
				case '90':
					$invoices.="<th><b>Vencido hasta 90 d&iacute;as</b></th></tr>";
					break;
				case '90+':
					$invoices.="<th><b>Vencido m&aacute;s de 90 d&iacute;as</b></th></tr>";
					break;
				default:
					$invoices.=	"<th><b>Vencido hasta 30 d&iacute;as</b></th>" .
								"<th><b>Vencido hasta 60 d&iacute;as</b></th>" .
								"<th><b>Vencido hasta 90 d&iacute;as</b></th>" .
								"<th><b>Vencido m&aacute;s de 90 d&iacute;as</b></th></tr>";
			}
		}

		$nameInv = '';
		$phoneInv = '';
		if ($inv['serial_dea']) {
			$nameInv = $inv['name_bra'];
			$phoneInv = $inv['phone_bra'];
		} else {
			$nameInv = $inv['first_name_cus'] . ' ' . $inv['last_name_cus'];
			$phoneInv = $inv['phone1_cus'];
		}

		$daysNum = 0;
		if ($inv['diff'] > 0) {
			$daysNum = $inv['diff'];
		}

		$ammount30 = 0;
		$ammount60 = 0;
		$ammount90 = 0;
		$ammount91 = 0;
		$amountIN = 0;
		
		if($inv['days_status'] == 'OUT'){
			if ($inv['diff'] <= 30) {
				$ammount30 = number_format($inv['total_inv'], 2, '.', '');
			} elseif ($inv['diff'] > 30 && $inv['diff'] <= 60) {
				$ammount60 = number_format($inv['total_inv'], 2, '.', '');
			} elseif ($inv['diff'] > 60 && $inv['diff'] <= 90) {
				$ammount90 = number_format($inv['total_inv'], 2, '.', '');
			} else {
				$ammount91 = number_format($inv['total_inv'], 2, '.', '');
			}
		}else{
			$amountIN = number_format($inv['total_inv'], 2, '.', '');
		}
		

		$card = $inv['payment_status'].'&nbsp;' . $inv['number_inv'] . ' / ';
		($inv['card_number_sal']) ? $card.=$inv['card_group'] : $card.='N/A';

		$amm30 += $ammount30;
		$amm60 += $ammount60;
		$amm90 += $ammount90;
		$amm91 += $ammount91;
		$ammIn += $amountIN;
		
		$invoices.= "<tr>".
					"<th>" . $inv['name_man'] . "</th>".
					"<th>" . $inv['name_usr'] . "</th>".
                    "<th>" . $inv['name_dea'] . "</th>" .
					"<th>" . $inv['name_bra'] . "</th>" .
					"<th>" . $inv['date_inv'] . "</th>" .
					"<th>" . $card . "</th>" .
					"<th>" . $nameInv . " / " . '&nbsp;' . $phoneInv . "</th>" .
					"<th>" . $daysNum . "</th>" .
					/* "<th>" . $inv['days_cdd'] . "</th>" . */
					"<th>" . $amountIN . "</th>";

		switch ($days) {
			case '30':
				$invoices.="<th>" . $ammount30 . "</th></tr>";
				break;
			case '60':
				$invoices.="<th>" . $ammount60 . "</th></tr>";
				break;
			case '90':
				$invoices.="<th>" . $ammount90 . "</th></tr>";
				break;
			case '90+':
				$invoices.="<th>" . $ammount91 . "</th></tr>";
				break;
			default:
				$invoices.="<th>" . $ammount30 . "</th>" .
						"<th>" . $ammount60 . "</th>" .
						"<th>" . $ammount90 . "</th>" .
						"<th>" . $ammount91 . "</th></tr>";
		}

		//TOTAL SUM FOR THE BRANCH
		$total_for_branch+=$inv['total_inv'];

		if ($inv['serial_bra'] != $invoicesRaw[$key + 1]['serial_bra']) {
			$invoices.= "<tr><th></th>" .
						"<th></th>" .
						"<th></th>" .
						"<th></th>" .
						"<th>TOTAL</th>".
						"<th>$ammIn</th>";

			switch ($days) {
				case '30':
					$invoices.="<th>" . $amm30 . "</th></tr>";
					break;
				case '60':
					$invoices.="<th>" . $amm60 . "</th></tr>";
					break;
				case '90':
					$invoices.="<th>" . $amm90 . "</th></tr>";
					break;
				case '90+':
					$invoices.="<th>" . $amm91 . "</th></tr>";
					break;
				default:
					$invoices.= "<th>" . $amm30 . "</th>" .
							"<th>" . $amm60 . "</th>" .
							"<th>" . $amm90 . "</th>" .
							"<th>" . $amm91 . "</th></tr>";
			}

			$invoices.="<tr><th colspan='3'></th>
							<th colspan='2'>Total de la Sucursal: " . number_format($total_for_branch, 2, '.', '') . "</th>";

			$total_for_branch = 0;
			echo $invoices . '</table>';
		}
		$branch = $inv['serial_bra'];
	}
} else {
	http_redirect('modules/reports/payments/fAccountsReceivable');
}
?>