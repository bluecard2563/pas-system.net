<?php

	$payment = new Payment($db);
	$paymentsRaw = array();
	$payments = array();

	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';

	$name_cou = new Country($db, $serial_cou);
	$name_cou ->getData();
	$name_cou = $name_cou->getName_cou();

	$name_mbc = ManagerbyCountry::getManagerByCountryName($db, $serial_mbc);

	$paymentsRaw = Payment::getExcessPayments($db, $serial_cou,	$serial_mbc, $_SESSION['serial_mbc'],
												$status, $serial_dea, $serial_bra, $_SESSION['dea_serial_dea']);
	//Debug::print_r($paymentsRaw); die;

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Tarjetas.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title = "<table>
					<tr><td colspan=6>".$date."</td></tr><tr></tr>
					<tr><th></th>
						<th><b>Reporte de Pagos en Exceso</b></th>
						</tr>
					<tr></tr>
				</table>";
	echo $title;

	$parameters = "<table>
						<tr>
							<td align='right'><b>Pa&iacute;s:: </b></td>
							<td>".$name_cou."</td>
						</tr>
						<tr>
							<td align='right'><b>Representante:</b></td>
							<td>".$name_mbc."</td>
						</tr>
					</table>";
	echo $parameters;

	if($paymentsRaw){
		$data = "<table border='1'>
					<tr>
						<td><b>Ref. de Pago</b></td>
						<td><b>No. de Pago(s)</b></td>
						<td><b>Fecha de Cobro</b></td>
						<td><b>Comercializador</b></td>
						<td><b>Facturas</b></td>
						<td><b>Total a Cobrar</b></td>
						<td><b>Total Cobrado</b></td>
						<td><b>Exceso</b></td>
						<td><b>Monto Disponible</b></td>
						<td><b>Fecha de Liquidaci&oacute;n</b></td>
						<td><b>Usuario que Liquid&oacute;</b>'</td>
					</tr>";

		foreach ($paymentsRaw as $ref) {
			$data .= "<tr>
						<td>{$ref['serial_pay']}</td>
						<td>{$ref['payments_group']}</td>
						<td>{$ref['date_pay']}</td>
						<td>{$ref['name_dea']}</td>
						<td>{$ref['invoice_group']}</td>
						<td>{$ref['total_to_pay_pay']}</td>
						<td>{$ref['total_payed_pay']}</td>
						<td>{$ref['excess_amount']}</td>
						<td>{$ref['excess_amount_available_pay']}</td>
						<td>{$ref['liq_date']}</td>
						<td>{$ref['liq_usr']}</td>
					</tr>";
		}

		$data .= "</table>";
	}else{
		$data = 'No existen registros para los par&aacute;metros seleccionados.';
	}

	echo $data;
?>