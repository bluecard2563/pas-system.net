<?php

include DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';

if (isset($_POST['selZone']) && isset($_POST['selCountry'])) {
	$invoice = new Invoice($db);
	$invoicesRaw = array();
	$invoices = array();
	$colsAux = array();
	$alignCols = array();

	$serial_zon = (isset($_POST['selZone'])) ? $_POST['selZone'] : '';
	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$date_from = (isset($_POST['txtDateFrom'])) ? $_POST['txtDateFrom'] : '';
	$date_to = (isset($_POST['txtDateTo'])) ? $_POST['txtDateTo'] : '';
	$days = (isset($_POST['selDays'])) ? $_POST['selDays'] : '';
	$category_dea = (isset($_POST['selCategory'])) ? $_POST['selCategory'] : '';
	if (isset($_POST['selComissionist'])) {
		$comissionist = $_POST['selComissionist'];
		$user = new User($db, $comissionist);
		$user->getData();
		$comissionist_name = $user->getFirstname_usr() . ' ' . $user->getLastname_usr();
	} else {
		$comissionist = '';
	}

	$invoicesRaw = $invoice->getNotPayedInvoices($serial_zon, $serial_cou, $serial_man, $_SESSION['serial_mbc'], $_SESSION['dea_serial_dea'], $serial_cit, $serial_dea, $serial_bra, $date_from, $date_to, $days, $category_dea, $comissionist);

	//Debug::print_r($invoicesRaw); die();

	if (!$invoicesRaw) {
		http_redirect('modules/reports/payments/fAccountsReceivable');
	}
	
	//************************************** PDF TABLE PARAMETERS
	$colsAux = array(
		'date' => '<b>Fecha</b>',
		'numberInv' => '<b># Factura / # Tarjeta</b>',
		'dealer' => utf8_decode(html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>')),
		'days' => '<b>Impago</b>',
		/* 'days_cdd' => utf8_decode(html_entity_decode('<b>D&iacute;as de Cr&eacute;dito</b>')), */
		'in_credit' => utf8_decode(html_entity_decode('<b>Dentro del Cr&eacute;dito</b>')),
		'til30' => utf8_decode(html_entity_decode('<b>Vencido hasta 30 d&iacute;as</b>')),
		'til60' => utf8_decode(html_entity_decode('<b>Vencido hasta 60 d&iacute;as</b>')),
		'til90' => utf8_decode(html_entity_decode('<b>Vencido hasta 90 d&iacute;as</b>')),
		'over90' => utf8_decode(html_entity_decode('<b>Vencido m&aacute;s de 90 d&iacute;as</b>'))
	);
	$alignCols = array(
		'date' => array('justification' => 'center', 'width' => 100),
		'numberInv' => array('justification' => 'center', 'width' => 120),
		'dealer' => array('justification' => 'center', 'width' => 150),
		'days' => array('justification' => 'center', 'width' => 50),
		/* 'days_cdd' => array('justification' => 'center', 'width' => 60), */
		'in_credit' => array('justification' => 'center', 'width' => 60),
		'til30' => array('justification' => 'center', 'width' => 60),
		'til60' => array('justification' => 'center', 'width' => 60),
		'til90' => array('justification' => 'center', 'width' => 60),
		'over90' => array('justification' => 'center', 'width' => 60)
	);
	$options = array(
		'showLines' => 2,
		'showHeadings' => 1,
		'shaded' => 0,
		'fontSize' => 10,
		'textCol' => array(0, 0, 0),
		'titleFontSize' => 12,
		'rowGap' => 5,
		'colGap' => 5,
		'lineCol' => array(0.8, 0.8, 0.8),
		'xPos' => 'center',
		'xOrientation' => 'center',
		'maxWidth' => 805,
		'cols' => $alignCols,
		'innerLineThickness' => 0.8,
		'outerLineThickness' => 0.8
	);

    if($serial_man == '0'){
	$requiredFields = array(
		array('0' => '<b>Zona: </b>' . $invoicesRaw[0]['name_zon'],
			'1' => utf8_decode(html_entity_decode('<b>Pa&iacute;s: </b>')) . $invoicesRaw[0]['name_cou']),
            array('0' => '<b>Representante: </b> Todos')
        );
    }else{
        $requiredFields = array(
            array('0' => '<b>Zona: </b>' . $invoicesRaw[0]['name_zon'],
                '1' => utf8_decode(html_entity_decode('<b>Pa&iacute;s: </b>')) . $invoicesRaw[0]['name_cou']),
		array('0' => '<b>Representante: </b>' . $invoicesRaw[0]['name_man'])
	);
    }

	//************************************** FILTERS SECTION
	$line1 = array();
	//DEALER FILTER
	$dealer_info = utf8_decode(html_entity_decode('<b>Comercializador: </b>'));
	($serial_dea) ? $dealer_info.=$invoicesRaw[0]['name_dea'] : $dealer_info.='Todos';
	array_push($line1, $dealer_info);
	//BRANCH FILTER
	$branch_info = utf8_decode(html_entity_decode('<b>Sucursal: </b>'));
	($serial_bra) ? $branch_info.=$invoicesRaw[0]['name_bra'] : $branch_info.='Todos';
	array_push($line1, $branch_info);
	array_push($requiredFields, $line1);

	$line2 = array();
	//CITY FILTER
	$city_info = utf8_decode(html_entity_decode('<b>Ciudad: </b>'));
	($serial_cit) ? $city_info.=$invoicesRaw[0]['name_cit'] : $city_info.='Todos';
	array_push($line2, $city_info);
	//COMISSIONIST FILTER
	$comissionist_info = utf8_decode(html_entity_decode('<b>Responsable: </b>'));
	($comissionist_name) ? $comissionist_info.=$comissionist_name : $comissionist_info.='Todos';
	array_push($line2, $comissionist_info);
	array_push($requiredFields, $line2);

	$line3 = array();
	//CATEGORY FILTER
	$category_info = utf8_decode(html_entity_decode('<b>Categor&iacute;a: </b>'));
	($category_dea) ? $category_info.=$category_dea : $category_info.='Todos';
	array_push($line3, $category_info);
	//DAYS FILTER
	$days_info = utf8_decode(html_entity_decode('<b>D&iacute;as Hasta: </b>'));
	if ($days) {
		$days = $days + 29;
		if ($days == 120) {
			$days = '90+';
		}
		$days_info.=$days;
	} else {
		$days_info.='N/A';
	}
	array_push($line3, $days_info);
	array_push($requiredFields, $line3);

	$line4 = array();
	//BEGIN DATE FILTER
	$beginDate_info = utf8_decode(html_entity_decode('<b>Fecha Desde: </b>'));
	($date_from) ? $beginDate_info.=$date_from : $beginDate_info.='N/A';
	array_push($line4, $beginDate_info);
	//END DATE FILTER
	$endDate_info = utf8_decode(html_entity_decode('<b>Fecha Hasta: </b>'));
	($date_to) ? $endDate_info.=$date_to : $endDate_info.='N/A';
	array_push($line4, $endDate_info);
	array_push($requiredFields, $line4);

	//**************************************	BUILD PDF HEADERS
	$all = $pdf->openObject();
	$pdf->saveState();
	$pdf->addText(50, 50, 10, utf8_decode(html_entity_decode('*Las tarjetas N/A son del tipo corporativo, no generan ning&uacute;n n&uacute;mero al momento de la venta.')));
	$pdf->restoreState();
	$pdf->closeObject();
	$pdf->addObject($all, 'all');
	$pdf->ezText('<b>Reporte de Cuentas por Cobrar</b>', 12, array('justification' => 'center'));
	$pdf->ezSetDy(-10);
	$pdf->ezTable($requiredFields, array('0' => '0',
		'1' => '1'), '', array('showHeadings' => 0,
		'shaded' => 0,
		'showLines' => 0,
		'fontSize' => 10,
		'xOrientation' => 'center',
		'xPos' => 360,
		'cols' => array(
			'0' => array('justification' => 'left', 'width' => 350),
			'1' => array('justification' => 'left', 'width' => 270)
		)
	));
	$branch = '';

	//***************************** GRAND TOTAL CALCULATION
	$grandTotal = 0;
	$totalInTime = 0;
	$totalUntil30 = 0;
	$totalUntil60 = 0;
	$totalUntil90 = 0;
	$totalPlus90 = 0;
	foreach ($invoicesRaw as $key => $inv) {
		if($inv['days_status'] == 'OUT'){
			if ($inv['diff'] <= 30) {
				$totalUntil30 += $inv['total_inv'];
			} elseif ($inv['diff'] > 30 && $inv['diff'] <= 60) {
				$totalUntil60 += $inv['total_inv'];
			} elseif ($inv['diff'] > 60 && $inv['diff'] <= 90) {
				$totalUntil90 += $inv['total_inv'];
			} else {
				$totalPlus90 += $inv['total_inv'];
			}
		}else{
			$totalInTime += $inv['total_inv'];
		}
		
		$grandTotal += $inv['total_inv'];
	}
	
	$pdf->ezSetDy(-10);
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Total dentro del cr&eacute;dito: </b>')) . number_format($totalInTime, 2, '.', ','), 10, array('justification' => 'left'));
	$pdf->ezSetDy(-2);
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Total vencido hasta 30 d&iacute;as: </b>')) . number_format($totalUntil30, 2, '.', ','), 10, array('justification' => 'left'));
	$pdf->ezSetDy(-2);
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Total vencido hasta 60 d&iacute;as: </b>')) . number_format($totalUntil60, 2, '.', ','), 10, array('justification' => 'left'));
	$pdf->ezSetDy(-2);
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Total vencido hasta 90 d&iacute;as: </b>')) . number_format($totalUntil90, 2, '.', ','), 10, array('justification' => 'left'));
	$pdf->ezSetDy(-2);
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Total vencido m&aacute;s de 90 d&iacute;as: </b>')) . number_format($totalPlus90, 2, '.', ','), 10, array('justification' => 'left'));
	$pdf->ezSetDy(-2);
	$pdf->ezText('<b>Total a Cobrar: </b>' . number_format($grandTotal, 2, '.', ','), 10, array('justification' => 'left'));
	$pdf->ezSetDy(-10);
	
	
	//***************************** DETAILED BRANCH TABLES
	$total_for_branch = 0;
	foreach ($invoicesRaw as $key => $inv) {
		if ($inv['serial_bra'] != $branch) {
			if ($total_for_branch > 0) {
				$pdf->ezText('<b>Total de la Sucursal: </b>' . number_format($total_for_branch, 2, '.', ''), 10, array('justification' => 'center'));
				$pdf->ezSetDy(-10);
			}
//            echo print_r($inv[name_man]);
			$pdf->ezText('<b>REPRESENTANTE: </b>' . $inv['name_man'] . 
                        ' / <b>RESPONSABLE: </b>' . $inv['name_usr'] .
                        ' / <b>COMERCIALIZADOR: </b>' . $inv['name_dea'] .
					'     /     <b>SUCURSAL: </b>' . $inv['name_bra'] .
					utf8_decode(html_entity_decode('     /     <b>TEL&Eacute;FONO: </b>')) . $inv['phone_bra'], 10, array('justification' => 'center'));
			$total_for_branch = 0;
			$pdf->ezSetDy(-5);
		}
		//TOTAL SUM FOR THE BRANCH
		$total_for_branch+=$inv['total_inv'];

		$nameInv = '';
		$phoneInv = '';
		if ($inv['serial_dea']) {
			$nameInv = $inv['name_bra'];
			$phoneInv = $inv['phone_bra'];
		} else {
			$nameInv = $inv['first_name_cus'] . ' ' . $inv['last_name_cus'];
			$phoneInv = $inv['phone1_cus'];
		}

		$daysNum = 0;
		if ($inv['diff'] > 0) {
			$daysNum = $inv['diff'];
		}

		$ammount30 = 0;
		$ammount60 = 0;
		$ammount90 = 0;
		$ammount91 = 0;
		$amountIN = 0;
		
		if($inv['days_status'] == 'OUT'){
			if ($inv['diff'] <= 30) {
				$ammount30 = number_format($inv['total_inv'], 2, '.', '');
			} elseif ($inv['diff'] > 30 && $inv['diff'] <= 60) {
				$ammount60 = number_format($inv['total_inv'], 2, '.', '');
			} elseif ($inv['diff'] > 60 && $inv['diff'] <= 90) {
				$ammount90 = number_format($inv['total_inv'], 2, '.', '');
			} else {
				$ammount91 = number_format($inv['total_inv'], 2, '.', '');
			}
		}else{
			$amountIN = number_format($inv['total_inv'], 2, '.', '');
		}

		

		($inv['card_number_sal']) ? $card = $inv['card_group'] : $card = 'N/A';

		$invoices[] = array(
			'date' => $inv['date_inv'],
			'numberInv' => $inv['payment_status'].' '.$inv['number_inv'] . ' / ' . $card,
			'dealer' => $nameInv . ' / ' . $phoneInv,
			'days' => $daysNum,
			/* 'days_cdd' => $inv['days_cdd'], */
			'in_credit' => $amountIN,
			'til30' => $ammount30,
			'til60' => $ammount60,
			'til90' => $ammount90,
			'over90' => $ammount91
		);

		if ($inv['serial_bra'] != $invoicesRaw[$key + 1]['serial_bra']) {
			$ammIn = 0;
			$amm30 = 0;
			$amm60 = 0;
			$amm90 = 0;
			$amm91 = 0;
			if ($invoices) {
				foreach ($invoices as $i) {
					$ammIn = $ammIn + $i['in_credit'];
					$amm30 = $amm30 + $i['til30'];
					$amm60 = $amm60 + $i['til60'];
					$amm90 = $amm90 + $i['til90'];
					$amm91 = $amm91 + $i['over90'];
				}
			}

			$invoices[] = array('date' => '',
				'numberInv' => '',
				'dealer' => '',
				'days' => 'TOTAL',
				'in_credit' => number_format($ammIn, 2, '.', ''),
				'til30' => number_format($amm30, 2, '.', ''),
				'til60' => number_format($amm60, 2, '.', ''),
				'til90' => number_format($amm90, 2, '.', ''),
				'over90' => number_format($amm91, 2, '.', '')
			);

			switch ($days) {
				case '30':
					$colsAux = array(
						'date' => '<b>Fecha</b>',
						'numberInv' => '<b># Factura / # Tarjeta</b>',
						'dealer' => utf8_decode(html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>')),
						'days' => '<b>Impago</b>',
						/* 'days_cdd' => utf8_decode(html_entity_decode('<b>D&iacute;as de Cr&eacute;dito</b>')), */
						'til30' => '<b>Hasta 30</b>'
					);
					break;
				case '60':
					$colsAux = array(
						'date' => '<b>Fecha</b>',
						'numberInv' => '<b># Factura / # Tarjeta</b>',
						'dealer' => utf8_decode(html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>')),
						'days' => '<b>Impago</b>',
						/* 'days_cdd' => utf8_decode(html_entity_decode('<b>D&iacute;as de Cr&eacute;dito</b>')), */
						'til60' => '<b>Hasta 60</b>'
					);
					break;
				case '90':
					$colsAux = array(
						'date' => '<b>Fecha</b>',
						'numberInv' => '<b># Factura / # Tarjeta</b>',
						'dealer' => utf8_decode(html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>')),
						'days' => '<b>Impago</b>',
						/* 'days_cdd' => utf8_decode(html_entity_decode('<b>D&iacute;as de Cr&eacute;dito</b>')), */
						'til90' => '<b>Hasta 90</b>'
					);
					break;
				case '90+':
					$colsAux = array(
						'date' => '<b>Fecha</b>',
						'numberInv' => '<b># Factura / # Tarjeta</b>',
						'dealer' => utf8_decode(html_entity_decode('<b>Facturado a / Tel&eacute;fono</b>')),
						'days' => '<b>Impago</b>',
						/* 'days_cdd' => utf8_decode(html_entity_decode('<b>D&iacute;as de Cr&eacute;dito</b>')), */
						'over90' => utf8_decode(html_entity_decode('<b>M&aacute;s de 90</b>'))
					);
					break;
			}

			$pdf->ezTable($invoices, $colsAux, NULL, $options);
			$pdf->ezSetDy(-10);
			$invoices = array();
		}

		$branch = $inv['serial_bra'];
	}
	$pdf->ezStream();
} else {
	http_redirect('modules/reports/payments/fAccountsReceivable');
}
?>