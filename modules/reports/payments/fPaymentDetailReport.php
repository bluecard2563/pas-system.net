<?php
/**
 * File:  fPaymentDetailReport
 * Author: Patricio Astudillo
 * Creation Date: 01-jun-2011, 11:04:53
 * Description:
 * Last Modified: 01-jun-2011, 11:04:53
 * Modified by:
 */
	
	$county = new Country($db);
	$countriesList = $county->getCountriesWithPayments($_SESSION['countryList'],$_SESSION['serial_mbc'],$_SESSION['dea_serial_dea']);
	
	$today = date('d/m/Y');

	$smarty -> register('countriesList,today');
	$smarty -> display();
?>
