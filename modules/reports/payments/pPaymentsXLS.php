<?php
set_time_limit(36000);
ini_set('memory_limit','512M');
if( ( isset($_POST['txtDateFrom']) && isset($_POST['txtDateTo']) ) || 
	( isset($_POST['selCountry']) && isset($_POST['selManager']) && isset($_POST['txtInvoice']) )
){
	$payment = new Payment($db);
	$paymentsRaw = array();
    $payments = array();

	if(isset($_POST['txtDateFrom'])){
		$dateFrom = $_POST['txtDateFrom'];
		$dateTo = $_POST['txtDateTo'];
		$serial_cus = (isset($_POST['txtCustomer'])) ? $_POST['txtCustomer'] : '';
		$card_number_sal = (isset($_POST['txtCard'])) ? $_POST['txtCard'] : '';
		$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
		$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
		$serial_man = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
		$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
		$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
		$type = $_POST['selType'];
		$serial_comissionist = $_POST['selComissionist'];

		$paymentsRaw = $payment->getPayments($dateFrom, $dateTo, $_SESSION['serial_mbc'], $_SESSION['dea_serial_dea'],
											$serial_cus, $card_number_sal, $serial_cou, $serial_man, $serial_dea,
											$serial_bra, $type, $serial_comissionist, $serial_cit);		
		
	} else{
		$serial_cou = $_POST['selCountry'];
		$serial_mbc = $_POST['selManager'];
		$number_inv = $_POST['txtInvoice'];
		$type = $_POST['rdType'];
		$paymentsRaw = $payment->getPaymentsInvoice($serial_cou,$serial_mbc,$number_inv,$type,$_SESSION['serial_mbc'],
													$_SESSION['dea_serial_dea']);
	}

	//Debug::print_r($paymentsRaw);die();
	if(!$paymentsRaw){
		http_redirect('modules/reports/payments/fPayments/no_registers');
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_de_Cobros.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	if(isset($_POST['txtDateFrom'])){
		$mainHeader = "<table>
							<tr><td colspan=6>".$date."</td></tr>
							<tr></tr>
							<tr><td colspan=6 align='center'><b>REPORTE DE COBROS</b></td></tr>
							<tr></tr>
							<tr>
								<td><b>Desde: </b></td>
								<td>".$dateFrom."</td>
								<td><b>Hasta: </b></td>
								<td>".$dateTo."</td>
							</tr>
							<tr></tr>
						</table>";
		echo $mainHeader;
		$parameters = array(
							'0'=>array('0'=>'<b>Cliente: </b>', '1'=>'Todos', '2'=>'<b>Tarjeta: </b>', '3'=>'Todos'),
							'1'=>array('0'=>'<b>Pa&iacute;s: </b>', '1'=>'Todos', '2'=>'<b>Representante: </b>', '3'=>'Todos'),
							'2'=>array('0'=>'<b>Comercializador: </b>', '1'=>'Todos', '2'=>'<b>Sucursal: </b>', '3'=>'Todos'),
							'3'=>array('0'=>'<b>Tipo: </b>', '1'=>'Todos', '2'=>'', '3'=>''),
							);
		if($serial_cus){
			$parameters['0']['1'] = $paymentsRaw[0]['name_customer_group'][0].' '.$paymentsRaw[0]['last_customer_group'][0];
		}
		if($card_number_sal){
			$parameters['0']['3'] = $paymentsRaw[0]['card_group'][0];
		}
		if($serial_cou){
			$parameters['1']['1'] = $paymentsRaw[0]['name_cou'];
		}
		if($serial_man){
			$parameters['1']['3'] = $paymentsRaw[0]['name_man'];
		}
		if($serial_dea){
			$parameters['2']['1'] = $paymentsRaw[0]['name_dea'];
		}
		if($serial_bra){
			$parameters['2']['3'] = $paymentsRaw[0]['name_bra'];
		}
		if($type){
			if($type == 1){
				$parameters['3']['1'] = 'Total';
			}
			else{
				$parameters['3']['1'] = 'Abono';
			}
		}
		$parametersTable = "<table>";
		foreach($parameters as $p){
			$parametersTable .= "<tr><td align='right'><b>".$p['0']."</b></td><td align='left'>".$p['1']."</td>
									<td align='right'><b>".$p['2']."</b></td><td align='left'>".$p['3']."</td></tr>";
		}
		$parametersTable .= "</table>";
		echo $parametersTable;
	}else{
		$mainHeader = "<table>
							<tr>
								<td colspan=6>".$date."</td>
							</tr>
							<tr></tr>
							<tr><td colspan=6 align='center'><b>REPORTE DE COBROS</b></td></tr>
							<tr></tr>
							<tr>
								<td><b>Pa&iacute;s: </b></td>
								<td>".$paymentsRaw[0]['name_cou']."</td>
								<td><b>Representante: </b></td>
								<td>".$paymentsRaw[0]['name_dea']."</td>
							</tr>
							<tr>
								<td><b>Tipo de Persona: </b></td>
								<td>".ucfirst($type)."</td>
								<td><b>N&uacute;mero de Factura: </b></td>
								<td>".$number_inv."</td>
							</tr>
						</table>";
		echo $mainHeader;
	}

	$title = "<br /><table>
				<tr>
					<td colspan='7' align='center'>
					<b>".strtoupper($dealer_name)."</b>
					</td>
				</tr>
				<tr>
					<td><b>Fecha</b></td>
					<td><b>Ref. de Pagos</b></td>
					<td><b>Nota(s) de Pago</b></td>
					<td><b>Factura(s)</b></td>
					<td><b>Cliente</b></td>
					<td><b>Comercializador</b></td>
					<td><b>Valor Pagado</b></td>
					<td><b>Monto en Exceso</b></td>
				</tr>";
	echo $title;
	$data="";
	foreach ($paymentsRaw as $key=>$pay) {
		$data.="<tr>
			<td>".$pay['date_pay']."</td>
			<td>".$pay['serial_pay']."</td>
			<td>".$pay['payment_numbers']."</td>
			<td>".$pay['invoice_group']."</td>
			<td>".$pay['name_customer_group']."</td>
			<td>".$pay['name_bra']."</td>
			<td>".number_format($pay['total_payed_pay'], 2, '.', '')."</td>
			<td>".$pay['excess']."</td>
		</tr>";
	}
	echo $data . "</table>";
}else{
    http_redirect('modules/reports/payments/fPayments/no_registers');
}
?>