<?php
/**
 * File:  pPaymentDetailReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 01-jun-2011, 11:45:25
 * Description:
 * Last Modified: 01-jun-2011, 11:45:25
 * Modified by:
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	
	Request::setInteger('selCountry');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setString('txtDateFrom');
	Request::setString('txtDateTo');
	
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table border='0'>
				<tr><td colspan=6>$date</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Detalle de Pagos</b></td></tr>
				 <tr></tr>
			</table><br />";
	
	//****************************************************** FILTER SECTION	
	if($selCountry){
		$name_cou = new Country($db, $selCountry);
		$name_cou -> getData();
		$name_cou = $name_cou -> getName_cou();
	}else{
		$name_cou = "Todos";
	}
	
	if($selManager){
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
	}else{
		$name_man = "Todos";
	}
	
	if($selDealer){
		$name_dea = new Dealer($db, $selDealer);
		$name_dea -> getData();
		$name_dea = $name_dea -> getName_dea().' - '.$name_dea ->getCode_dea();
	}else{
		$name_dea = "Todos";
	}
	
	if($selBranch){
		$name_bra = new Dealer($db, $selBranch);
		$name_bra -> getData();
		$name_bra = $name_bra -> getName_dea().' - '.$name_bra ->getCode_dea();
	}else{
		$name_bra = "Todos";
	}
	
	
	$parameters = "<table border='1'>
					<tr>
						<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
						<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
					</tr>
					<tr>
						<td align='right'><b>Comercialziador:</b></td><td align='left'>" . $name_dea . "</td>
						<td align='right'><b>Sucursal:</b></td><td align='left'>" . $name_bra . "</td>
					</tr>
					<tr>
						<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $txtDateFrom . "</td>
						<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $txtDateTo . "</td>
					</tr>
					<tr></tr>
				</table><br />";
		
	//*****************************************************  GETTING THE INFORMACITION
	$payment_forms = new PaymentDetail($db);
	$payment_forms = $payment_forms -> getPaymentForms();
	
	$payment_list = PaymentDetail::getPaymentInfoForReport($db, $selCountry, $txtDateFrom, $txtDateTo, 
																$selManager, $selDealer, $selBranch);	
	if($payment_list){
		$table = "<table border='1'>
					<tr>
						<td rowspan='2'><b>Fecha</td>
						<td rowspan='2'><b># Pago</td>
						<td rowspan='2'><b>Factura(s)</td>
						<td rowspan='2'><b>Cliente</td>
						<td rowspan='2'><b>Comercializador</td>
						<td rowspan='2'><b>Valor Pagado</td>
						<td colspan='10'><b>Formas de Pago</td>
						<td rowspan='2'><b>Monto en Exceso</td>
						<td rowspan='2'><b>Ultimo Pago</td>
					</tr>
					<tr>
						<td><b>Efectivo</b></td>
						<td><b>Tarjeta de Cr&eacute;dito</b></td>
						<td><b>Cheque</b></td>
						<td><b>Notas de Cr&eacute;dito</b></td>						
						<td><b>Transferencia</b></td>
						<td><b>Exceso</b></td>
						<td><b>Incobrable</b></td>
						<td><b>Pay Pal</b></td>
						<td><b>Retenci&oacute;n</b></td>
						<td><b>Cruce con comisi&oacute;n</b></td>
					</tr>";
		
		
		foreach($payment_list as &$item){
			$item_payment_forms = PaymentDetail::getPaymentDetailsForReport($db, $item['serial_pay']);
			
			foreach($payment_forms as $forms){
				$item[$forms] = number_format(0, 2);
				foreach($item_payment_forms as $ipf){
					if($ipf['type_pyf'] == $forms){
						$item[$forms] = number_format($ipf['amount'], 2);
						break;
					}
				}
			}
			
			$table .="	<tr>
							<td>{$item['date_pay']}</td>
							<td>{$item['number_pyf']}</td>
							<td>{$item['number_inv']}</td>
							<td>{$item['name_cus']}</td>
							<td>{$item['name_dea']}</td>
							<td>{$item['total_payed_pay']}</td>
							<td>{$item['CASH']}</td>
							<td>{$item['CREDIT_CARD']}</td>
							<td>{$item['CHECK']}</td>
							<td>{$item['CREDIT_NOTE']}</td>
							<td>{$item['TRANSFER']}</td>
							<td>{$item['EXCESS']}</td>
							<td>{$item['UNCOLLECTABLE']}</td>
							<td>{$item['PAYPAL']}</td>
							<td>{$item['RETENTION']}</td>
							<td>{$item['COMMISSION']}</td>	
							<td>{$item['excess']}</td>	
							<td>{$item['last_payment']}</td>	
						</tr>";			
		}
		
		$table .= "</table>";
	}else{
		$table = "No existen registros para los filtros seleccionados.";
	}	
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Detalle_Pago.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $parameters;
	echo $table;
?>
