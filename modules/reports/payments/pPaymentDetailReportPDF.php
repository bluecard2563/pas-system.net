<?php
/**
 * File:  pPaymentDetailReportPDF
 * Author: Patricio Astudillo
 * Creation Date: 01-jun-2011, 16:45:25
 * Description:
 * Last Modified: 01-jun-2011, 16:45:25
 * Modified by:
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	
	Request::setInteger('selCountry');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setString('txtDateFrom');
	Request::setString('txtDateTo');
	
	//****************************************************** FILTER SECTION	
	if($selCountry){
		$name_cou = new Country($db, $selCountry);
		$name_cou -> getData();
		$name_cou = $name_cou -> getName_cou();
	}else{
		$name_cou = "Todos";
	}
	
	if($selManager){
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
	}else{
		$name_man = "Todos";
	}
	
	if($selDealer){
		$name_dea = new Dealer($db, $selDealer);
		$name_dea -> getData();
		$name_dea = $name_dea -> getName_dea().' - '.$name_dea ->getCode_dea();
	}else{
		$name_dea = "Todos";
	}
	
	if($selBranch){
		$name_bra = new Dealer($db, $selBranch);
		$name_bra -> getData();
		$name_bra = $name_bra -> getName_dea().' - '.$name_bra ->getCode_dea();
	}else{
		$name_bra = "Todos";
	}
	
	include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	$pdf->ezText('<b>Reporte de Reembolsos</b>',14,array('justification'=>'center'));
	$pdf->ezSetDy(-20);
	
	
	$parameters = array('0' => array('0' => utf8_decode(html_entity_decode('<b>Pa&iacute;s</b>')),
									 '1' => $name_cou,
									 '2' => '<b>Representante</b>',
									 '3' => $name_man),
						'1' => array('0' => '<b>Comercializador</b>',
									 '1' => $name_dea,
									 '2' => '<b>Sucursal</b>',
									 '3' => $name_bra),
						'2' => array('0' => '<b>Fecha Desde</b>',
									 '1' => $txtDateFrom,
									 '2' => '<b>FEcha Hasta</b>',
									 '3' => $txtDateTo));

	$pdf->ezTable($parameters,'','',
                          array('showHeadings'=>0,
                                'shaded'=>0,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'cols'=>array(
                                        '0'=>array('justification'=>'right','width'=>80),
                                        '1'=>array('justification'=>'left','width'=>100),
                                        '2'=>array('justification'=>'right','width'=>80),
                                        '3'=>array('justification'=>'left','width'=>100))));
	$pdf->ezSetDy(-10);
	
	//*****************************************************  GETTING THE INFORMACITION
	$payment_forms = new PaymentDetail($db);
	$payment_forms = $payment_forms -> getPaymentForms();
	
	$payment_list = PaymentDetail::getPaymentInfoForReport($db, $selCountry, $txtDateFrom, $txtDateTo, $selManager, $selDealer, $selBranch);	
	
	if($payment_list){		
		foreach($payment_list as &$item){
			$item_payment_forms = PaymentDetail::getPaymentDetailsForReport($db, $item['serial_pay']);
			
			foreach($payment_forms as $forms){
				$item[$forms] = number_format(0, 2);
				foreach($item_payment_forms as $ipf){
					if($ipf['type_pyf'] == $forms){
						$item[$forms] = number_format($ipf['amount'], 2);
						break;
					}
				}
			}		
		}
		
		$titles = array(	'date_pay'=>('<b>Fecha</b>'),
							'number_pyf'=>('<b># Pago</b>'),
							'number_inv'=>('<b>Factura(s)</b>'),
							'name_cus'=>('<b>Cliente</b>'),
							'name_dea'=>('<b>Comercializador</b>'),
							'total_payed_pay'=>('<b>Valor Pagado</b>'),
							'CASH'=>('<b>Efectivo</b>'),
							'CREDIT_CARD'=> utf8_decode(html_entity_decode('<b>Tarjeta de Cr&eacute;dito</b>')),
							'CHECK'=>('<b>Cheque</b>'),
							'CREDIT_NOTE'=> utf8_decode(html_entity_decode('<b>Notas de Cr&eacute;dito</b>')),
							'TRANSFER'=>('<b>Transferencia</b>'),
							'EXCESS'=>('<b>Exceso</b>'),
							'UNCOLLECTABLE'=>('<b>Incobrable</b>'),
							'PAYPAL'=>('<b>Pay Pal</b>'),
							'RETENTION'=> utf8_decode(html_entity_decode('<b>Retenci&oacute;n</b>')),
							'COMMISSION'=> utf8_decode(html_entity_decode('<b>Cruce con comisi&oacute;n</b>')),
							'excess'=>('<b>Monto en Exceso</b>'),	
							'last_payment'=> ('<b>Ultimo Pago</b>'));
		
		$pdf->ezTable($payment_list,$titles,'<b>LISTADO DE PAGOS</b>',
							  array('showHeadings'=>1,
									'shaded'=>1,
									'showLines'=>2,
									'xPos'=>'center',
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'fontSize' => 8,
									'titleFontSize' => 9,
									'cols'=>array(
											'date_pay'=>array('justification'=>'center','width'=>53),
											'number_pyf'=>array('justification'=>'center','width'=>40),
											'number_inv'=>array('justification'=>'center','width'=>40),
											'name_cus'=>array('justification'=>'center','width'=>60),
											'name_dea'=>array('justification'=>'center','width'=>60),
											'total_payed_pay'=>array('justification'=>'center','width'=>50),
											'CASH'=>array('justification'=>'center','width'=>40),
											'CREDIT_CARD'=>array('justification'=>'center','width'=>50),
											'CHECK'=>array('justification'=>'center','width'=>40),
											'CREDIT_NOTE'=>array('justification'=>'center','width'=>40),
											'TRANSFER'=>array('justification'=>'center','width'=>40),
											'EXCESS'=>array('justification'=>'center','width'=>40),
											'UNCOLLECTABLE'=>array('justification'=>'center','width'=>45),
											'PAYPAL'=>array('justification'=>'center','width'=>40),
											'RETENTION'=>array('justification'=>'center','width'=>40),
											'COMMISSION'=>array('justification'=>'center','width'=>50),
											'excess'=>array('justification'=>'center','width'=>40),
											'last_payment'=>array('justification'=>'center','width'=>53)
									)));
		
	}else{
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}
	
	$pdf->ezStream();
?>
