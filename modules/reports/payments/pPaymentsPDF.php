<?php
set_time_limit(36000);
ini_set('memory_limit','512M');

$payment = new Payment($db);
$paymentsRaw = array();
$payments = array();

if(isset($_POST['txtDateFrom'])){
	$dateFrom = $_POST['txtDateFrom'];
	$dateTo = $_POST['txtDateTo'];
	$serial_cus = (isset($_POST['txtCustomer'])) ? $_POST['txtCustomer'] : '';
	$card_number_sal = (isset($_POST['txtCard'])) ? $_POST['txtCard'] : '';
	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_cit = (isset($_POST['selCity'])) ? $_POST['selCity'] : '';
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$type = $_POST['selType'];
	$serial_comissionist = $_POST['selComissionist'];
	
	$paymentsRaw = $payment->getPayments($dateFrom, $dateTo, $_SESSION['serial_mbc'], $_SESSION['dea_serial_dea'],
											$serial_cus, $card_number_sal, $serial_cou, $serial_mbc, $serial_dea,
											$serial_bra, $type, $serial_comissionist, $serial_cit);
}else{
	$serial_cou = $_POST['selCountry'];
	$serial_mbc = $_POST['selManager'];
	$number_inv = $_POST['txtInvoice'];
	$type = $_POST['rdType'];
	$paymentsRaw = $payment->getPaymentsInvoice($serial_cou, $serial_mbc, $number_inv, $type, $_SESSION['serial_mbc'],
												$_SESSION['dea_serial_dea']);
}

//Debug::print_r($paymentsRaw);die;

if(!$paymentsRaw){
	include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
	$pdf->addText(150,700,10,html_entity_decode('No existen resultados para los filtros seleccionados.'));
	$pdf->ezStream();
	return;
}

if(isset($_POST['txtDateFrom'])){
	$requiredFields = array();

	$line2=array();
	//COUNTRY FILTER
	$country=html_entity_decode('<b>Pa&iacute;s: </b>');
	($serial_cou)?$country.=$paymentsRaw[0]['name_cou']:$country.='Todos';
	array_push($line2,$country);
	//MANAGER FILTER
	$manager='<b>Representante: </b>';
	($serial_mbc)?$manager.=$paymentsRaw[0]['name_man']:$manager.='Todos';
	array_push($line2,$manager);
	array_push($requiredFields, $line2);

	$line3=array();
	//DEALER FILTER
	$dealer_info=html_entity_decode('<b>Comercializador: </b>');
	($serial_dea)?$dealer_info.=$paymentsRaw[0]['name_dea']:$dealer_info.='Todos';
	array_push($line3,$dealer_info);
	//BRANCH FILTER
	$branch_info=html_entity_decode('<b>Sucursal: </b>');
	($serial_bra)?$branch_info.=$paymentsRaw[0]['name_bra']:$branch_info.='Todos';
	array_push($line3,$branch_info);
	array_push($requiredFields, $line3);

	$line1=array();
	array_push($line1,($serial_cus)?'<b>Cliente: </b>'.$paymentsRaw[0]['name_customer_group'][0].' '.$paymentsRaw[0]['last_customer_group'][0]:'<b>Cliente: </b>Todos');
	array_push($line1, ($card_number_sal)?'<b>No. Tarjeta: </b>'.$paymentsRaw[0]['name_customer_group'][0].' '.$paymentsRaw[0]['last_customer_group'][0]:'<b>No. Tarjeta: </b>N/A');
	array_push($requiredFields, $line1);

	$line0=array('0' => '<b>Desde: </b>'.$dateFrom,
				 '1' => '<b>Hasta: </b>'.$dateTo);
	array_push($requiredFields, $line0);

	//TYPE FILTER
	$line4 = array();
	$type_info = '<b>Tipo: </b>';
	switch($type){
		case 1:
			$type_info .= 'Total';
			break;
		case 2:
			$type_info .= 'Abono';
			break;
		case 3:
			$type_info .= 'Todos';
			break;
	}
	array_push($line4,$type_info);
	array_push($requiredFields, $line4);
}else{
	$requiredFields = array(
							array('0' => html_entity_decode('<b>Pa&iacute;s: </b>').$paymentsRaw[0]['name_cou'],
								  '1' => '<b>Representante: </b>'.$paymentsRaw[0]['name_dea']),
							array('0' => '<b>Tipo de Persona: </b>'.html_entity_decode($global_typeManager[$type]),
								  '1' => html_entity_decode('<b>N&uacute;mero de Factura: </b>').$number_inv)
							);
}

include DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

$all = $pdf->openObject();
$pdf->saveState();
$pdf->addText(50,50,10,html_entity_decode('*Las tarjetas N/A son del tipo corporativo, no generan ning&uacute;n n&uacute;mero al momento de la venta.'));
$pdf->restoreState();
$pdf->closeObject();
$pdf->addObject($all,'all');

$pdf->ezText('<b>Reporte de Cobros</b>',11,array('justification' => 'center'));
$pdf->ezSetDy(-5);

$pdf->ezTable($requiredFields,array('0' => '0',
									'1' => '1'),
							'',
							array('showHeadings' => 0,
								  'shaded' => 0,
								  'showLines' => 0,
								  'fontSize' => 9,
								  'cols' => array(
											'0' => array('justification' => 'left','width' => 270),
											'1' => array('justification' => 'left','width' => 270)
										  )
							));
$pdf->ezSetDy(-5);

switch($type){
	case 1:
		$type_info .= 'Total';
		break;
	case 2:
		$type_info .= 'Abono';
		break;
	case 3:
		$type_info .= 'Todos';
		break;
}
	
if($type == 3){ //ALL
	$reportHeaders = array(
		'date' => '<b>Fecha</b>',
		'payment' => '<b>Ref. de Pagos</b>',
		'payment_numbers' => '<b>Nota(s) de Pago</b>',
		'invoice' => '<b>Factura(s)</b>',
		'customer' => '<b>Cliente</b>',
		'branch' => '<b>Comercializador</b>',
		'amount' => '<b>Valor Pagado</b>',
		'type' => '<b>Tipo</b>',
		'excess' => '<b>Monto en Exceso</b>'
	);
	$reportColumns = array(
		'date' => array('justification' => 'center','width' => 60),
		'payment' => array('justification' => 'center','width' => 40),
		'payment_numbers' => array('justification' => 'center','width' => 40),
		'invoice' => array('justification' => 'center','width' => 50),
		'customer' => array('justification' => 'left','width' => 130),
		'branch' => array('justification' => 'center','width' => 100),
		'amount' => array('justification' => 'center','width' => 50),
		'type' => array('justification' => 'center','width' => 40),
		'excess' => array('justification' => 'center','width' => 70)
	);
}else{
	$reportHeaders = array(
		'date' => '<b>Fecha</b>',
		'payment' => '<b>Referencia de Pago</b>',
		'payment_numbers' => '<b>Nota(s) de Pago</b>',
		'invoice' => '<b>Factura(s)</b>',
		'customer' => '<b>Cliente</b>',
		'branch' => '<b>Comercializador</b>',
		'amount' => '<b>Valor Pagado</b>',
		'excess' => '<b>Monto en Exceso</b>'
	);
	$reportColumns = array(
		'date' => array('justification' => 'center','width' => 60),
		'payment' => array('justification' => 'center','width' => 50),
		'payment_numbers' => array('justification' => 'center','width' => 40),
		'invoice' => array('justification' => 'center','width' => 80),
		'customer' => array('justification' => 'left','width' => 130),
		'branch' => array('justification' => 'center','width' => 100),
		'amount' => array('justification' => 'center','width' => 50),
		'excess' => array('justification' => 'center','width' => 70)
	);
}
global $global_paymentsStatus;

foreach ($paymentsRaw as $key => &$pay){
	$pay = array(
		'date' => $pay['date_pay'],
		'payment' => $pay['serial_pay'],
		'payment_numbers' => $pay['payment_numbers'],
		'invoice' => $pay['invoice_group'],
		'customer' => $pay['name_customer_group'],
		'branch' => $pay['name_bra'],
		'amount' => number_format($pay['total_payed_pay'], 2, '.', ''),
		'type' => $global_paymentsStatus[$pay['type']],
		'excess' => $pay['excess']
	);
}

$pdf -> ezTable(	$paymentsRaw,
				  $reportHeaders,
				  '<b>'.$dealer_name.'</b>',
				  array(
					'showLines' => 1,
					'showHeadings' => 1,
					'shaded' => 1,
					'fontSize' => 9,
					'textCol'  => array(0,0,0),
					'titleFontSize' => 9,
					'rowGap' => 2,
					'colGap' => 2,
					'lineCol' => array(0.4,0.4,0.4),
					'xPos' =>  300,
					'xOrientation' => 'center',
					'maxWidth' => 805,
					'cols' => $reportColumns,
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8
					)
);

$pdf->ezSetDy(-20);
$pdf->ezStream();
?>