<?php
   
	$payment = new Payment($db);
	$paymentsRaw = array();
    $payments = array();

	$serial_cou = (isset($_POST['selCountry'])) ? $_POST['selCountry'] : '';
	$serial_mbc = (isset($_POST['selManager'])) ? $_POST['selManager'] : '';
	$serial_dea = (isset($_POST['selDealer'])) ? $_POST['selDealer'] : '';
	$serial_bra = (isset($_POST['selBranch'])) ? $_POST['selBranch'] : '';
	$status = (isset($_POST['selStatus'])) ? $_POST['selStatus'] : '';
	
	$name_cou = new Country($db, $serial_cou);
	$name_cou ->getData();
	$name_cou = $name_cou->getName_cou();
	
	$name_mbc = ManagerbyCountry::getManagerByCountryName($db, $serial_mbc);
	
	$reportTitle = '<b>Reporte de Pagos en Exceso</b>';
	$paymentsRaw = Payment::getExcessPayments($db, $serial_cou,	$serial_mbc, $_SESSION['serial_mbc'],
												$status, $serial_dea, $serial_bra, $_SESSION['dea_serial_dea']);
	
	include DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	
	$pdf->ezText('<b>'.("REPORTE DE PAGOS EN EXCESO").'</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	$pdf->ezText('<b>'.html_entity_decode("Pa&iacute;s: ").'</b>'.$name_cou, 10, array('justification'=>'left'));
	$pdf->ezText('<b>'."Representante: ".'</b>'.$name_mbc, 10, array('justification'=>'left'));
	$pdf->ezText('<b>'."Fecha del reporte: ".'</b>'.date('d/m/Y'), 10, array('justification'=>'left'));
	$pdf->ezSetDy(-10);
	
	//Debug::print_r($paymentsRaw); die;
	
	if($paymentsRaw){
		$titles = array('serial_pay'=> ('<b>Ref. de Pago</b>'),
						'payments_group'=> ('<b>No. de Pago(s)</b>'),
						'date_pay'=>('<b>Fecha de Cobro</b>'),
						'name_dea'=>('<b>Comercializador</b>'),
						'invoice_group'=>('<b>Facturas</b>'),
						'total_to_pay_pay'=>('<b>Total a Cobrar</b>'),
						'total_payed_pay'=>('<b>Total Cobrado</b>'),
						'excess_amount'=>('<b>Exceso</b>'),
						'excess_amount_available_pay'=>('<b>Monto Disponible</b>'),
						'liq_date'=>  html_entity_decode('<b>Fecha de Liquidaci&oacute;n</b>'),
						'liq_usr'=> html_entity_decode('<b>Usuario que Liquid&oacute;</b>')
						);
		
		
		$pdf->ezTable(	$paymentsRaw,
						$titles,
						'',
						array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'serial_pay'=>array('justification'=>'center','width'=>60),
                                        'payments_group'=>array('justification'=>'center','width'=>60),
                                        'date_pay'=>array('justification'=>'center','width'=>60),
                                        'name_dea'=>array('justification'=>'center','width'=>100),
										'invoice_group'=>array('justification'=>'center','width'=>60),
                                        'total_to_pay_pay'=>array('justification'=>'center','width'=>60),
                                        'total_payed_pay'=>array('justification'=>'center','width'=>60),
                                        'excess_amount'=>array('justification'=>'center','width'=>50),
                                        'excess_amount_available_pay'=>array('justification'=>'center','width'=>50),
                                        'liq_date'=>array('justification'=>'center','width'=>60),
                                        'liq_usr'=>array('justification'=>'center','width'=>100))));
		
		
		
	}else{
		$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados.'),
					10, array('justification' =>'center'));
	}

	$pdf->ezStream();

?>