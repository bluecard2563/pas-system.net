<?php
/**
 * File:  pLoginAuditReportXLS
 * Author: Santiago Arellano
 * Creation Date: 04/07/2011
 * Description:
 * Last Modified:
 * Modified by:
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');

	Request::setInteger('selCountry');
	Request::setInteger('selManager');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setString('txtDateFrom');
	Request::setString('txtDateTo');

	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table border='0'>
				<tr><td colspan=6>$date</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Control de Login</b></td></tr>
				 <tr></tr>
			</table><br />";

	//****************************************************** FILTER SECTION
	if($selCountry){
		$name_cou = new Country($db, $selCountry);
		$name_cou -> getData();
		$name_cou = $name_cou -> getName_cou();
	}else{
		$name_cou = "Todos";
	}

	if($selManager){
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
	}else{
		$name_man = "Todos";
	}

	if($selDealer){
		$name_dea = new Dealer($db, $selDealer);
		$name_dea -> getData();
		$name_dea = $name_dea -> getName_dea().' - '.$name_dea ->getCode_dea();
	}else{
		$name_dea = "Todos";
	}

	if($selBranch){
		$name_bra = new Dealer($db, $selBranch);
		$name_bra -> getData();
		$name_bra = $name_bra -> getName_dea().' - '.$name_bra ->getCode_dea();
	}else{
		$name_bra = "Todos";
	}


	$parameters = "<table border='1'>
					<tr>
						<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
						<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
					</tr>
					<tr>
						<td align='right'><b>Comercializador:</b></td><td align='left'>" . $name_dea . "</td>
						<td align='right'><b>Sucursal:</b></td><td align='left'>" . $name_bra . "</td>
					</tr>
					<tr>
						<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $txtDateFrom . "</td>
						<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $txtDateTo . "</td>
					</tr>
					<tr></tr>
				</table><br />";

	//*****************************************************  GETTING THE INFORMACITION

	$login_control_list = LoginControl::getLoginAuditReport($db, $txtDateFrom, $txtDateTo,
																$selManager, $selDealer, $selBranch);
	if($login_control_list){
		$table = "<table border='1'>
					<tr>
						<th><b>Nombre de Usuario</th>
						<th><b>Username</th>
						<th><b>Empresa</th>
						<th><b>Fecha de Ingreso</th>
						<th><b>Fecha de Salida</th>
						<th><b>IP</th>
					</tr>";
$last_username="";
		foreach($login_control_list as $lcl){
			$table .="	<tr>";
			
			if($lcl['username_usr']!=$last_username){
				$table .="	<td>{$lcl['name_usr']}</td>
							<td>{$lcl['username_usr']}</td>";
			}else{
				$table .="	<td></td>
							<td></td>";
			}
			
			$table .="		<td>{$lcl['entity']}</td>
							<td>{$lcl['login_date_lgn']}</td>
							<td>{$lcl['logout_date_lgn']}</td>
							<td>{$lcl['ip_lgn']}</td>
						</tr>";
			$last_username=$lcl['username_usr'];
		}

		$table .= "</table>";
	}else{
		$table = "No existen registros para los filtros seleccionados.";
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Detalle_Ingreso.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $title;
	echo $parameters;
	echo $table;
?>
