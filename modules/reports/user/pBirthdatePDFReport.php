<?php
/*
File: pBirthdatePDFReport
Author: David Bergmann
Creation Date: 07/06/2010
Last Modified:
Modified By:
*/
include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

//HEADER
$pdf->ezText('<b>'.html_entity_decode("REPORTE DE CUMPLEA&Ntilde;OS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$data = User::getBirthdateReport($db,$_POST['selCountry'],$_POST['selCity'],$_POST['selManager'],$_POST['selResponsable'],$_POST['selMonth']);
$dataManagers = User::getBirthdateReportManager($db,$_POST['selCountry'],$_POST['selCity'],$_POST['selManager'],$_POST['selResponsable'],$_POST['selMonth']);

$fullData = array_merge($dataManagers, $data);

if(is_array($fullData)) {
    $titles=array('col1'=>utf8_decode('<b>Documento #</b>'),
                  'col2'=>utf8_decode('<b>Usuario</b>'),
				  'col3'=>utf8_decode('<b>Sucursal / Representante</b>'),
				  'col6'=>utf8_decode('<b>Responsable</b>'),
				  'col4'=>html_entity_decode('<b>Direcci&oacute;n</b>'),
                  'col5'=>utf8_decode('<b>Fecha de nacimiento</b>'));

    $pdf->ezTable($fullData, $titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
                                        'col1'=>array('justification'=>'center','width'=>60),
                                        'col2'=>array('justification'=>'center','width'=>100),
										'col3'=>array('justification'=>'center','width'=>120),
										'col6'=>array('justification'=>'center','width'=>80),
										'col4'=>array('justification'=>'center','width'=>120),
                                        'col5'=>array('justification'=>'center','width'=>60))));
        $pdf->ezSetDy(-20);
} else {
    $pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>
