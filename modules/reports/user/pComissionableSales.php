<?php

$salesList = Reports::getComissionableSalesList($db, $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selManager']);

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
$pdf->ezText('<b>'.html_entity_decode("REPORTE DE VENTAS COMISIONABLES").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);
	
if($salesList){
	$total_sold = 0;
	$total_discount = 0;
	$total_invoiced = 0;
	
	$salesByCnt = array();
	$displayData = array();
	
	foreach($salesList as $sale){
                //Initiate Value - discount
                $total_sal = $sale['total_sal'] - $sale['discount'];
                //Check Taxes
                if($sale['applied_taxes_inv'] != NULL)
                {
                    $taxes = unserialize($sale['applied_taxes_inv']);
                    if($taxes)
                    {
                        $sum_tx = 0;
                        foreach($taxes as $tax)
                        {
//                            $tx = (($sale['total_sal'] * $tax['tax_percentage']) /100);
                            $tx = (($total_sal * $tax['tax_percentage']) /100);
                            $total_sal -= $tx;
                            $sum_tx += $tx;
                        }
                    }
                    else{
//                        $total_sal = $sale['total_sal'];
                        $total_sal = $total_sal;
                    }
                }
                else{
//                    $total_sal = $sale['total_sal'];
                    $total_sal = $total_sal;
                }
                
		$total_sold += $sale['total_sal'];
//		$total_sold += $total_sal;
		$total_discount += $sale['discount'];
		$total_invoiced += ($sale['total_sal'] - $sale['discount']);
//		$total_invoiced += $total_sal;
		
		if(isset($salesByCnt[$sale['serial_cnt']])){
//			$salesByCnt[$sale['serial_cnt']]['sales'] += ($sale['total_sal'] - $sale['discount']);
			$salesByCnt[$sale['serial_cnt']]['sales'] += round($total_sal,2);
		}else{
			$salesByCnt[$sale['serial_cnt']]['name'] = $sale['counter'];
//			$salesByCnt[$sale['serial_cnt']]['sales'] += round(($sale['total_sal'] - $sale['discount']),2);
			$salesByCnt[$sale['serial_cnt']]['sales'] += round($total_sal,2);
			$salesByCnt[$sale['serial_cnt']]['comision'] = 0;
		}
		
		
		$tempArray = array();
		$tempArray['col1'] = $sale['counter'];
		$tempArray['col2'] = $sale['card_number'];
		$tempArray['col3'] = $sale['in_date_sal'];
		$tempArray['col4'] = $sale['date_pay'];
		$tempArray['col5'] = $sale['total_sal'];
		$tempArray['col6'] = $sale['discount'];
//		$tempArray['col7'] = ($sale['total_sal'] - $sale['discount']);
		$tempArray['col7'] = round((($sale['total_sal'] - $sale['discount']) - round($total_sal, 2)),2);
		$tempArray['col8'] = round($total_sal, 2);
		
		array_push($displayData, $tempArray);
	}
	
	//******* CALCULATE THE COMISSION RANGE TO APPLY *********
	$comissionTable = $mbcCommissionTable[$_POST['selManager']];
	
	$comPercentage = 0;
	foreach($comissionTable as $range){
		if($total_invoiced >= $range['base'] && $total_invoiced <= $range['top']){
			$comPercentage = $range['comissionPercentage'] / 100;
			break;
		}
	}
	
	//*************** APPLY COMISSION TO EACH SALES MAN ***********
	foreach($salesByCnt as &$salesMan){
		$salesMan['comision'] = round($salesMan['sales'] * $comPercentage, 2);
	}
	
	$summaryValues = array(array('sales'=>$total_sold, 'discount'=>$total_discount, 'comissionable'=>$total_invoiced, 'comision'=> ($comPercentage * 100).' %'));
	
	//******** VALIDATION DATA **********
//	echo 'Sold: '.$total_sold.' Discount: '.$total_discount.' Invoiced: '.$total_invoiced.' ComPercentage: '.$comPercentage;
//	Debug::print_r($salesByCnt);
	
	//************************** MAKING PDF *****************
	$tempMbc = new ManagerbyCountry($db, $_POST['selManager']);
	$tempMbc->getData();
	$tempMan = new Manager($db, $tempMbc->getSerial_man());
	$tempMan->getData();
	
	$pdf->ezText('<b>Representante:</b> '.$tempMan->getName_man(), 10, array('justification'=>'left'));
	$pdf->ezText('<b>Fecha Desde:</b> '.$_POST['txtBeginDate'], 10, array('justification'=>'left'));
	$pdf->ezText('<b>Fecha Hasta:</b> '.$_POST['txtEndDate'], 10, array('justification'=>'left'));
	$pdf->ezSetDy(-10);
	
	$titles=array('sales'=>utf8_decode('<b>Total Ventas</b>'),
                  'discount'=>utf8_decode('<b>Total Descuento</b>'),
				  'comissionable'=>utf8_decode('<b>Monto a Comisionar</b>'),
				  'comision'=>utf8_decode('<b>% Comision</b>')
				);
	
	$pdf->ezTable($summaryValues, $titles,'',
					array('showHeadings'=>1,
						  'shaded'=>1,
						  'showLines'=>2,
						  'xPos'=>'center',
						  'innerLineThickness' => 0.8,
						  'outerLineThickness' => 0.8,
						  'fontSize' => 8,
						  'titleFontSize' => 8,
						  'cols'=>array(
								  'sales'=>array('justification'=>'center','width'=>80),
								  'discount'=>array('justification'=>'center','width'=>70),
								  'comissionable'=>array('justification'=>'center','width'=>60),
								  'comision'=>array('justification'=>'center','width'=>60))));
	$pdf->ezSetDy(-20);
	
	$pdf->ezText('<b>'.html_entity_decode("Resumen por Agente de Ventas").'</b>', 10, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	$titles=array('name'=>utf8_decode('<b>Agente de Ventas</b>'),
                  'sales'=>utf8_decode('<b>Total Ventido</b>'),
				  'comision'=>utf8_decode('<b>Monto a Comisionar</b>')
				);
	
	$pdf->ezTable($salesByCnt, $titles,'',
					array('showHeadings'=>1,
						  'shaded'=>1,
						  'showLines'=>2,
						  'xPos'=>'center',
						  'innerLineThickness' => 0.8,
						  'outerLineThickness' => 0.8,
						  'fontSize' => 8,
						  'titleFontSize' => 8,
						  'cols'=>array(
								  'col1'=>array('justification'=>'center','width'=>100),
								  'col2'=>array('justification'=>'center','width'=>50),
								  'col3'=>array('justification'=>'center','width'=>60))));
	$pdf->ezSetDy(-20);
	
	$pdf->ezText('<b>'.html_entity_decode("Detalle de Ventas").'</b>', 10, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	$titles=array('col1'=>utf8_decode('<b>Agente de Ventas</b>'),
                  'col2'=>utf8_decode('<b>No. tarjeta</b>'),
				  'col3'=>utf8_decode('<b>Fecha de Emision</b>'),
				  'col4'=>utf8_decode('<b>Fecha Pago</b>'),
				  'col5'=>utf8_decode('<b>Valor Tarjeta</b>'),
				  'col6'=>utf8_decode('<b>Descuento</b>'),
				  'col7'=>utf8_decode('<b>Seguro Campesino</b>'),
				  'col8'=>html_entity_decode('<b>Valor Comisionable</b>')
				);
	
	$pdf->ezTable($displayData, $titles,'',
					array('showHeadings'=>1,
						  'shaded'=>1,
						  'showLines'=>2,
						  'xPos'=>'center',
						  'innerLineThickness' => 0.8,
						  'outerLineThickness' => 0.8,
						  'fontSize' => 8,
						  'titleFontSize' => 8,
						  'cols'=>array(
								  'col1'=>array('justification'=>'center','width'=>100),
								  'col2'=>array('justification'=>'center','width'=>50),
								  'col3'=>array('justification'=>'center','width'=>60),
								  'col4'=>array('justification'=>'center','width'=>60),
								  'col5'=>array('justification'=>'center','width'=>60),
							      'col6'=>array('justification'=>'center','width'=>60),
								  'col7'=>array('justification'=>'center','width'=>70))));
	$pdf->ezSetDy(-20);
                  
	
}else{
	$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();