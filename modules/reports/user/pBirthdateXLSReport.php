<?php
/*
File: pBirthdateXLSReport
Author: David Bergmann
Creation Date: 07/06/2010
Last Modified:
Modified By:
*/

$data = User::getBirthdateReport($db,$_POST['selCountry'],$_POST['selCity'],$_POST['selManager'],$_POST['selResponsable'],$_POST['selMonth']);
$dataManagers = User::getBirthdateReportManager($db,$_POST['selCountry'],$_POST['selCity'],$_POST['selManager'],$_POST['selResponsable'],$_POST['selMonth']);

$fullData = array_merge($dataManagers, $data);

$report = "<table>
           <tr>
                <th>Documento #</th>
                <th>Usuario</th>
				<th>Sucursal / Representante</th>
				<th>Responsable</th>
				<th>Direcci&oacute;n</th>
                <th>Fecha de nacimiento</th>
           </tr>";
foreach ($fullData as $d) {
    $report .= "<tr>
                    <td>".$d['col1']."</td>
                    <td>".$d['col2']."</td>
                    <td>".$d['col3']."</td>
					<td>".$d['col6']."</td>
					<td>".$d['col4']."</td>
					<td>".$d['col5']."</td>
               </tr>";
}
$report .= "</table>";

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=".utf8_decode("Cumpleanios").".xls");
header("Pragma: no-cache");
header("Expires: 0");

echo $report;
?>
