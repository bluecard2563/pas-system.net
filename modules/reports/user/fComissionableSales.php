<?php
/**
 * File: ERPSalesAnalysis.php
 * Author: Patricio Astudillo
 * Creation Date: 27/10/2016
 * Modified By:
 * Last Modified:
*/

Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getAllowedZones($_SESSION['countryList']);
$today=date('d/m/Y');
$first_day='01/01/2009';

$smarty -> register('zonesList,error,today,first_day');
$smarty->display();
?>
