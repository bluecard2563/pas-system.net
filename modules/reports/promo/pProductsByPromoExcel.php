<?php
/*
File:pProductsByPromoExcel
Author: Esteban Angulo
Creation Date: 01/07/2010
*/

Request::setString('selZone');
Request::setString('selCountry');
Request::setString('selCity');
Request::setString('selManager');
Request::setString('selDealer');

switch ($_POST['selApliedTo']){
	case 'COUNTRY': $pbc=new PromoByCountry($db);
					$data=$pbc->getPromosForReport($selCountry);
					break;
	case 'CITY':	$pbc=new PromoByCity($db);
					$data=$pbc->getPromosForReport($selCity);
					break;
	case 'MANAGER':	$pbmbc=new PromoByManager($db);
					$data=$pbmbc->getPromosForReport($selManager);
					break;
	case 'DEALER':	$pbd=new PromoByDealer($db);
					$data=$pbd->getPromosForReport($selDealer);
					break;
	
	default: $data='';
}


$zone=new Zone($db, $selZone);
$zone->getData();
$zoneName=$zone->getName_zon();

$country=new Country($db, $selCountry);
$country->getData();

if($selCity!=''){
	$city=new City($db, $selCity);
	$city->getData();
}

if($selManager!=''){
	$mbc=new ManagerbyCountry($db, $selManager);
	$mbc->getData();
	$man=new Manager($db, $mbc->getSerial_man());
	$man->getData();
}

if($selDealer!=''){
	$dealer=new Dealer($db, $selDealer);
	$dealer->getData();
}


$header.="<br><br><br><table border='1'>
			<tr>
				<td colspan='2' align='center'>".utf8_decode('<b>REPORTE DE PRODUCTOS POR PROMOCIÓN</b>')."</td>
			</tr>
			<tr>
				<td  align='center'><b>Zona</b></td>
				<td	 align='center'>".$zone->getName_zon()."</td>
			</tr>
			<tr>
				<td  align='center'>".utf8_decode('<b>Pa&iacute;s</b>')."</td>
				<td	 align='center'>".$country->getName_cou()."</td>
			</tr>";
if($selCity!=''){
	$header.="<tr>
				<td  align='center'><b>Ciudad</b></td>
				<td	 align='center'>".$city->getName_cit()."</td>
			</tr>";
}
if($selManager!=''){
	$header.="<tr>
				<td  align='center'><b>Representante</b></td>
				<td	 align='center'>".$man->getName_man()."</td>
			</tr>";
}
if($selDealer!=''){
	$header.="<tr>
				<td  align='center'><b>Representante</b></td>
				<td	 align='center'>".$dealer->getName_dea()."</td>
			</tr>";
}

$header.="</table>";
//END HEADERS
$table.="<br>";

if(is_array($data)){
	$serial_prm=$data['0']['serial_prm'];
	foreach($data as $key=>$p){
		if($p['serial_prm']==$serial_prm){
			$promos[$key]['serial']=$p['serial_prm'];
			$promos[$key]['promo']=$p['name_prm'];
			$promos[$key]['product']=$p['name_pbl'];
			$promos[$key]['start_val_pbp']=$p['starting_value_pbp'];
			if($p['end_value_pbp']){
				if($p['type_prm']=='AMOUNT' || $p['type_prm']=='PERCENTAGE'){
					$promos[$key]['end_val_pbp']='N/A';
				}
				elseif($p['type_prm']=='RANGE'){
					$promos[$key]['end_val_pbp']='En Adelante';
				}
			}
			else{
				$promos[$key]['end_val_pbp']=$p['end_value_pbp'];
			}
			$promos[$key]['prize']=$p['name_pri'];
			$title=$promos[$key]['promo'];
		}
		else{
			$table.="<table border='1'>
						<tr>
								<td colspan='4' align='center'>".'<b>'.$title.'</b>'."</td>
						</tr>
						<tr>
								<td  align='center'><b>Nombre del Producto</b></td>
								<td  align='center'><b>Monto Base</b></td>
								<td  align='center'>".utf8_decode('<b>Monto Límite</b>')."</td>
								<td  align='center'>".utf8_decode('<b>Premio</b>')."</td>
						</tr>";
			foreach($promos as $key=>$pr){
				
				$table.="	
							<tr>
								<td  align='center'>".$promos[$key]['product']."</td>
								<td  align='center'>&nbsp;".$promos[$key]['start_val_pbp']."</td>
								<td  align='center'>&nbsp;".$promos[$key]['end_val_pbp']."</td>
								<td  align='center'>".$promos[$key]['prize']."</td>
							</tr>";
						
			}
			$table.="</table>";
			$table.="<br>";

			$serial_prm=$p['serial_prm'];
			$promos=array();
			$promos[$key]['serial']=$p['serial_prm'];
			$promos[$key]['promo']=$p['name_prm'];
			$promos[$key]['product']=$p['name_pbl'];
			$promos[$key]['start_val_pbp']=$p['starting_value_pbp'];
			if($p['end_value_pbp']=='0'){
				if($p['type_prm']=='AMOUNT' || $p['type_prm']=='PERCENTAGE'){
					$promos[$key]['end_val_pbp']='N/A';
				}
				elseif($p['type_prm']=='RANGE'){
					$promos[$key]['end_val_pbp']='En Adelante';
				}
			}
			else{
				$promos[$key]['end_val_pbp']=$p['end_value_pbp'];
			}
			$promos[$key]['prize']=$p['name_pri'];
			$title=$promos[$key]['promo'];
		}

		
		
	}
	$table.="<table border='1'>
						<tr>
								<td colspan='4' align='center'>".'<b>'.$title.'</b>'."</td>
						</tr>
						<tr>
								<td  align='center'><b>Nombre del Producto</b></td>
								<td  align='center'><b>Monto Base</b></td>
								<td  align='center'>".utf8_decode('<b>Monto Límite</b>')."</td>
								<td  align='center'>".utf8_decode('<b>Premio</b>')."</td>
						</tr>";
			foreach($promos as $key=>$pr){

				$table.="
							<tr>
								<td  align='center'>".$promos[$key]['product']."</td>
								<td  align='center'>&nbsp;".$promos[$key]['start_val_pbp']."</td>
								<td  align='center'>&nbsp;".$promos[$key]['end_val_pbp']."</td>
								<td  align='center'>".$promos[$key]['prize']."</td>
							</tr>";

			}
			$table.="</table>";
			$table.="<br>";	
}
else{
	$table.="<b>No existen promociones registrados</b>";
}
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteProductosPorPromocion.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $header;
echo $table;
?>
