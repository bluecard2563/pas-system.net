<?php
/*
File:pViewPromoPDF
Author: Esteban Angulo
Creation Date: 02/07/2010
*/
Request::setInteger('0:serial_prm');
$promo=new Promo($db);
$promo->setSerial_prm($serial_prm);
$promo->getData();
global $global_promoTypes,$global_saleTypes,$global_periodTypes,$global_weekDaysNumb,$global_repeatTypes;

if ($promo->getNotifications_prm()=='0'){
	$notify='No';
}
elseif($promo->getNotifications_prm()=='1'){
	$notify='Si';
}

if ($promo->getWeekends_prm()=='NO'){
	$weekend='No';
}
elseif($promo->getWeekends_prm()=='YES'){
	$weekend='Si';
}

if ($promo->getRepeat_prm()=='0'){
	$repeat='No';
}
elseif($promo->getRepeat_prm()=='1'){
	$repeat='Si';
}

$promoDays=explode(',', $promo->getPeriodDays_prm());
$days=array();
foreach ($promoDays as $key=>$l){
	$days[$key]=html_entity_decode($global_weekDaysNumb[$l]);

}
$daysAll=implode(', ', $days);

$table.="<table border='0'><tr><td colspan='4' align='center'> </td></tr></table>";
$table.="<table border='1'>
						<tr>
								<td colspan='4' align='center'><b>".utf8_decode("Reporte de la Promoción: ").$promo->getName_prm()."</b></td>
						</tr>
						<tr>
								<td  align='left'>".utf8_decode('<b>Descripción:</b>')."</td>
								<td  align='left' width='300'>".$promo->getDescripction_prm()."</td>
								<td  align='left'>".utf8_decode('<b>Condiciones:</b>')."</td>
								<td  align='left'>".$global_promoTypes[$promo->getType_prm()]."<br>".utf8_decode('<b>Envío de Notitificaciones: </b>').$notify."</td>
						</tr>
						<tr>
								<td  align='left'>".utf8_decode('<b>Fecha de Inicio:</b>')."</td>
								<td  align='left'>".$promo->getBeginDate_prm()."</td>
								<td  align='left'>".utf8_decode('<b>Fecha de Fin:</b>')."</td>
								<td  align='left'>".$promo->getEndDate_prm()."</td>
						</tr>
						<tr>
								<td  align='left'>".utf8_decode('<b>Valor de la Venta:</b>')."</td>
								<td  align='left'>".$global_saleTypes[$promo->getTypeSale_prm()]."</td>
								<td  align='left'>".utf8_decode('<b>Días Aplicables:</b>')."</td>
								<td  align='left'>".html_entity_decode($global_periodTypes[$promo->getPeriodType_prm()])."</td>
						</tr>
						<tr>
								<td  align='left'>".utf8_decode('<b>Se repite la Promoción:</b>')."</td>
								<td  align='left'>".$repeat."<br><b>Periodicidad: </b>".$global_repeatTypes[$promo->getRepeatType_prm()]."<br>".utf8_decode('<b>Día de la Promoción: </b>').$daysAll." </td>
								<td  align='left'>".utf8_decode('<b>Aplica Fin de Semana:</b>')."</td>
								<td  align='left'>".$weekend."</td>
						</tr>
		</table>";
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReportePromocion.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;
?>
