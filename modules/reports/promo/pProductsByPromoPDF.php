<?php
/*
File:pProductsByPromoPDF
Author: Esteban Angulo
Creation Date: 30/06/2010
*/
//Debug::print_r($_POST);
//die();
Request::setString('selZone');
Request::setString('selCountry');
Request::setString('selCity');
Request::setString('selManager');
Request::setString('selDealer');

switch ($_POST['selApliedTo']){
	case 'COUNTRY': $pbc=new PromoByCountry($db);
					$data=$pbc->getPromosForReport($selCountry);
					break;
	case 'CITY':	$pbc=new PromoByCity($db);
					$data=$pbc->getPromosForReport($selCity);
					break;
	case 'MANAGER':	$pbmbc=new PromoByManager($db);
					$data=$pbmbc->getPromosForReport($selManager);
					break;
	case 'DEALER':	$pbd=new PromoByDealer($db);
					$data=$pbd->getPromosForReport($selDealer);
					break;
	
	default: $data='';
}
/*if($_POST['selApliedTo']=='COUNTRY'){
	$pbc=new PromoByCountry($db);
	$data=$pbc->getPromosForReport($selCountry);
}*/

/*if($data==''){
	$error=1;
	http_redirect('modules/reports/promo/fProductsByPromo/'.$error);
}
else{*/
	//Debug::print_r($data);
	//die();

	$zone=new Zone($db, $selZone);
	$zone->getData();
	$zoneName=$zone->getName_zon();

	$country=new Country($db, $selCountry);
	$country->getData();

	if($selCity!=''){
		$city=new City($db, $selCity);
		$city->getData();
	}

	if($selManager!=''){
		$mbc=new ManagerbyCountry($db, $selManager);
		$mbc->getData();
		$man=new Manager($db, $mbc->getSerial_man());
		$man->getData();
	}

	if($selDealer!=''){
		$dealer=new Dealer($db, $selDealer);
		$dealer->getData();
	}

	include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
	//HEADER
	$pdf->ezText(utf8_decode('<b>REPORTE DE PRODUCTOS POR PROMOCIÓN</b>'), 12, array('justification'=>'center'));

	$pdf->ezSetDy(-10);
	$headerDesc=array();
	$descLine1=array('col1'=>utf8_decode('<b>Zona:</b>'),
					 'col2'=>$zone->getName_zon());
	array_push($headerDesc,$descLine1);
	$descLine2=array('col1'=>utf8_decode('<b>País:</b>'),
					 'col2'=>$country->getName_cou());
	array_push($headerDesc,$descLine2);
	if($selCity!=''){
		$descLine3=array('col1'=>'<b>Ciudad:</b>',
						 'col2'=>$city->getName_cit());
		array_push($headerDesc,$descLine3);
	}
	if($selManager!=''){
		$descLine4=array('col1'=>'<b>Representante:</b>',
						 'col2'=>$man->getName_man());
		array_push($headerDesc,$descLine4);
	}
	if($selDealer!=''){
		$descLine4=array('col1'=>'<b>Comercializador:</b>',
						 'col2'=>$dealer->getName_dea());
		array_push($headerDesc,$descLine4);
	}
	$pdf->ezTable($headerDesc,
				  array('col1'=>'','col2'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>0,
						'xPos'=>'200',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>85),
							'col2'=>array('justification'=>'left','width'=>150))));
	$pdf->ezSetDy(-10);
	//END HEADERS

	if(is_array($data)){
		$serial_prm=$data['0']['serial_prm'];
		foreach($data as $key=>$p){
			if($p['serial_prm']==$serial_prm){
				$promos[$key]['serial']=$p['serial_prm'];
				$promos[$key]['promo']=$p['name_prm'];
				$promos[$key]['product']=$p['name_pbl'];
				$promos[$key]['start_val_pbp']=$p['starting_value_pbp'];
				if($p['end_value_pbp']){
					if($p['type_prm']=='AMOUNT' || $p['type_prm']=='PERCENTAGE'){
						$promos[$key]['end_val_pbp']='N/A';
					}
					elseif($p['type_prm']=='RANGE'){
						$promos[$key]['end_val_pbp']='En Adelante';
					}
				}
				else{
					$promos[$key]['end_val_pbp']=$p['end_value_pbp'];
				}
				$promos[$key]['prize']=$p['name_pri'];
				$tableTitle=$promos[$key]['promo'];
			}
			else{

				$header = array('product'=>'<b>Nombre del Producto</b>',
								'start_val_pbp'=>'<b>Monto Base</b>',
								'end_val_pbp'=>utf8_decode('<b>Monto Límite</b>'),
								'prize'=>utf8_decode('<b>Premio</b>'));
				$params = array('showHeadings'=>1,
								'shaded'=>1,
								'showLines'=>2,
								'xPos'=>'center',
								'innerLineThickness' => 0.8,
								'outerLineThickness' => 0.8,
								'fontSize' => 8,
								'titleFontSize' => 8,
								'cols'=>array(
									'product'=>array('justification'=>'center','width'=>65),
									'start_val_pbp'=>array('justification'=>'center','width'=>100),
									'end_val_pbp'=>array('justification'=>'center','width'=>70),
									'prize'=>array('justification'=>'center','width'=>60)));
				$pdf->ezTable($promos,$header,
								$tableTitle,
								$params);
				$pdf->ezSetDy(-10);

				$serial_prm=$p['serial_prm'];
				$promos=array();
				$promos[$key]['serial']=$p['serial_prm'];
				$promos[$key]['promo']=$p['name_prm'];
				$promos[$key]['product']=$p['name_pbl'];
				$promos[$key]['start_val_pbp']=$p['starting_value_pbp'];
				if($p['end_value_pbp']=='0'){
					if($p['type_prm']=='AMOUNT' || $p['type_prm']=='PERCENTAGE'){
						$promos[$key]['end_val_pbp']='N/A';
					}
					elseif($p['type_prm']=='RANGE'){
						$promos[$key]['end_val_pbp']='En Adelante';
					}
				}
				else{
					$promos[$key]['end_val_pbp']=$p['end_value_pbp'];
				}
				$promos[$key]['prize']=$p['name_pri'];
				$tableTitle=$promos[$key]['promo'];
			}



		}
		//Debug::print_r(current($promos));
		//$tableTitle=$promos[$key]['promo'];
		$header = array('product'=>'<b>Nombre del Producto</b>',
								'start_val_pbp'=>'<b>Monto Base</b>',
								'end_val_pbp'=>utf8_decode('<b>Monto Límite</b>'),
								'prize'=>utf8_decode('<b>Premio</b>'));
				$params = array('showHeadings'=>1,
								'shaded'=>1,
								'showLines'=>2,
								'xPos'=>'center',
								'innerLineThickness' => 0.8,
								'outerLineThickness' => 0.8,
								'fontSize' => 8,
								'titleFontSize' => 8,
								'cols'=>array(
									'product'=>array('justification'=>'center','width'=>65),
									'start_val_pbp'=>array('justification'=>'center','width'=>100),
									'end_val_pbp'=>array('justification'=>'center','width'=>70),
									'prize'=>array('justification'=>'center','width'=>60)));
				$pdf->ezTable($promos,$header,
								$tableTitle,
								$params);
				$pdf->ezSetDy(-10);

	}
	else{
		$pdf->ezText(utf8_decode('<b>No existen promociones registradas para esos datos</b>'), 12, array('justification'=>'center'));
	}

	//die();
	$pdf->ezStream();
//}
?>
