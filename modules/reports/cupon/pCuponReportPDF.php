<?php

    Request::setString('0:serial_ccp');

    $customerPromo=new CustomerPromo($db);
    $customerPromo->setSerial_ccp($serial_ccp);
    $customerPromo->getData();

    $customerPromoName=$customerPromo->getName_ccp();
    $customerPromoDescription=$customerPromo->getDescription_ccp();
    $customerPromoExpireDate=$customerPromo->getEnd_date_ccp();
    $customerPromoStatus=$customerPromo->getStatus_ccp();
    $customerPromoPrefix=$customerPromo->getCupon_prefix_ccp();

    $cupon=CustomerPromo::getActiveNumberCouponsByPromo($db, $serial_ccp);
    
    $cuponTable=array();
    $aux_line_array = array();
    foreach($cupon as $c){
        if(count($aux_line_array) < 3 ){
            array_push($aux_line_array, $customerPromoPrefix."-".$c['number_cup']);
        }else{
            array_push($cuponTable, $aux_line_array);
            $aux_line_array = array();
            array_push($aux_line_array, $customerPromoPrefix."-".$c['number_cup']);
        }
    }
    //Push the last elements
    array_push($cuponTable, $aux_line_array);

	include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

	//HEADER
	$pdf->ezText('<b>CUPONES DISPONIBLES</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);
	$pdf->ezText($date, 10, array('justification'=>'center'));
	$pdf->ezSetDy(-20);
        $pdf->ezText(html_entity_decode('<b>PROMOCI&Oacute;N: ').$customerPromoName.'</b>', 12, array('justification'=>'center'));
        $pdf->ezSetDy(-10);
        $pdf->ezText(html_entity_decode('<b>ESTADO: ').'</b>'.$global_customerPromoStatus[$customerPromoStatus], 11, array('justification'=>'left'));
        $pdf->ezSetDy(-10);
        $pdf->ezText(html_entity_decode('<b>EXPIRACI&Oacute;N: ').'</b>'.$customerPromoExpireDate, 11, array('justification'=>'left'));
        $pdf->ezSetDy(-10);
        $pdf->ezText(html_entity_decode('<b>DESCRIPCI&Oacute;N: ').'</b>'.$customerPromoDescription, 11, array('justification'=>'left'));
        $pdf->ezSetDy(-20);
       	
	
		$pdf->ezTable($cuponTable,'',
			  '<b>CUPONES</b>',
                              array('showHeadings'=>0,
                                            'shaded'=>1,
                                            'showLines'=>2,
                                            'xPos'=>'300',
                                            'fontSize' => 9,
                                            'titleFontSize' => 11,
                                            'cols'=>array(
                                                    '0'=>array('justification'=>'center','width'=>100),
                                                    '1'=>array('justification'=>'center','width'=>100),
                                                    '2'=>array('justification'=>'center','width'=>100)
                                                    )));
        
        $pdf->ezStream();

?>
