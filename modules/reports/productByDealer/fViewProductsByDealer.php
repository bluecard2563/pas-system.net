<?php
/*
 * File: fViewProductsByDealer.php
 * Author: Patricio Astudillo
 * Creation Date: 27/05/2010, 10:25:16 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 27/05/2010, 10:25:16 AM
 */

$zone=new Zone($db);
$zoneList=$zone->getZones();

$smarty->register('zoneList');
$smarty->display();
?>
