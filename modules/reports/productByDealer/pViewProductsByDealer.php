<?php
/*
 * File: pViewProductsByDealer.php
 * Author: Patricio Astudillo
 * Creation Date: 27/05/2010, 11:14:16 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 27/05/2010, 11:14:16 AM
 */

Request::setString('selCountry');
Request::setString('selManager');
Request::setString('selDealer');

/* Getting the Product List in a Specific Country */
$pxc = new ProductByCountry($db);
$pxclist = $pxc -> getProductsByCountry($selCountry, $_SESSION['serial_lang']);
/* Getting the Product List in a Specific Country */

/* Getting the DEaler List According to the paramters */
$params = array();
$aux = array('field'=>'mbc.serial_cou',
		     'value'=> $selCountry,
		     'like'=> 1);
array_push($params,$aux);

$aux=array('field'=>'d.dea_serial_dea',
		   'value'=> 'NULL',
		   'like'=> 1);
array_push($params,$aux);

if($selManager){
	$aux=array('field'=>'mbc.serial_mbc',
			   'value'=> $selManager,
			   'like'=> 1);
	array_push($params,$aux);
}

if($selDealer){
	$aux=array('field'=>'d.serial_dea',
			   'value'=> $selDealer,
			   'like'=> 1);
	array_push($params,$aux);
}

$dealer=new Dealer($db);
$dealerList=$dealer->getDealers($params);

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Productos_x_Comercializador.xls");
header("Pragma: no-cache");
header("Expires: 0");

$reportContent = "<table border='0'><tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='4'><b>Reporte de Productos por Comercializador</b></td></tr></table>";

$reportContent .= "<table border='0'><tr><td>&nbsp;</td></tr></table>";

$cou = new Country($db, $selCountry);
$cou -> getData();

$filterCountry = $cou -> getName_cou();
$filterManager = 'TODOS';
$filterDealer = 'TODOS';

if($selManager){
	$man = new Manager($db, $selManager);
	$man -> getData();
	$filterManager = $man -> getName_man();
}

if($selDealer){
	$dea = new Dealer($db, $selDealer);
	$dea -> getData();
	$filterDealer = $dea -> getName_dea();
}

$reportContent .= "<table border='0'>
					<tr><td colspan='2' align='center'><b>Filtros</b></td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td><b>Pais:</b></td><td>$filterCountry</td></tr>
					<tr><td><b>Representante:</b></td><td>$filterManager</td></tr>
					<tr><td><b>Comercializador:</b></td><td>$filterDealer</td></tr>
				  </table>";

$reportContent .= "<table border='0'><tr><td>&nbsp;</td></tr></table>";
$reportContent .= "<table border='0'><tr><td>&nbsp;</td></tr></table>";

if(is_array($dealerList) && is_array($pxclist)){
	$total_product = sizeof($pxclist);
	$reportContent .= "<table border='1'>";
	$reportContent .= "<tr><td>&nbsp;</td>";
	foreach ($pxclist as $pro){
		$reportContent .= "<td><b>" . $pro['name_pbl'] . "</b></td>";
	}
	$reportContent .= "</tr>";

	foreach($dealerList as $dea){
		$reportContent.="<tr>";
		$reportContent.="<td><b>" . $dea['name_dea'] . "</b></td>";
		foreach ($pxclist as $pro){
			$reportContent.="<td align='center'>";
			if(ProductByDealer::pbdExists($db, $pro['serial_pxc'], $dea['serial_dea'])){
				$reportContent.="X";
			}else{
				$reportContent.="&nbsp;";
			}
			$reportContent.="</td>";
		}
		$reportContent.="</tr>";
	}
	$reportContent.="</table>";
}else{
	$reportContent .= "<table border='0'><tr><td colspan='4'>No existen resultados para los filtros seleccionados</td></tr></table>";
}

echo $reportContent;
?>