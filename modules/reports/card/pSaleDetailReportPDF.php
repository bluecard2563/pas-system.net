<?php
/*
File: pSaleDetailReportPDF.php
Author: Santiago Borja
Creation Date: 10/06/2010
*/
	Request::setString('selCreditNote');
	Request::setString('txtNumCard');
	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selManager');
	Request::setString('selDocumentTo');
	Request::setString('txtNumInvoice');
	Request::setString('txtClient');
	Request::setString('hdnClientID');
	Request::setString('saleSerial');
	
	$tableInformation = array();
	
	include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';
	
	$sale = new Sales($db, $saleSerial);
	$sale -> getData();
	
	//CUSTOMER:
	$serialCus = $sale -> getSerial_cus();
	$customer = new Customer($db, $serialCus);
	$customer -> getData();
	$customer=get_object_vars($customer);
	
	$customerCity = new City($db, $customer['serial_cit']);
	$customerCity -> getData();
	$customerCountry = new Country($db, $customerCity -> getSerial_cou());
	$customerCountry -> getData();
	
	//TRIP INFO:
	$dataInfo = $sale->getSaleProduct($_SESSION['serial_lang']);
	$extra=new Extras($db,$sale -> getSerial_sal());
	$dataExt = Extras::getExtrasBySale($db, $sale -> getSerial_sal());
	$log = new SalesLog($db);
	$dataLog = $log -> getSalesLogBySale($sale -> getSerial_sal());
	$file = new File($db);
	$dataFiles = $file -> getFilesByCard($sale -> getCardNumber_sal());

	$userByDealer = new UserByDealer($db);
	$userByDealerData = UserByDealer::getUserByDealerByCard($db, $sale->getSerial_sal());
	
	$fee_ext = 0;
	if($dataExt){
		foreach($dataExt as $dE){
			$fee_ext = $fee_ext+$dE['fee_ext'];
		}
	}
	$dataServ = $sale -> getSaleServices($_SESSION['serial_lang']);
	$dataBen = $sale -> getSaleBenefits($_SESSION['serial_lang']);
	
	//TABLE DATA:
	$tableInformation['nameCustomer'] = $customer['first_name_cus'].' '.$customer['last_name_cus'];
	$tableInformation['documentCustomer'] = $customer['document_cus'];
	$tableInformation['birthdateCustomer'] = $customer['birthdate_cus'];
	$tableInformation['emailCustomer'] = $customer['email_cus'];
	$tableInformation['phoneCustomer'] = $customer['phone1_cus'];
	$tableInformation['cellphoneCustomer'] = $customer['cellphone_cus'];
	$tableInformation['addressCustomer'] = $customer['address_cus'];
	$tableInformation['countryCustomer'] = $customerCountry -> getName_cou();
	$tableInformation['cityCustomer'] = $customerCity -> getName_cit();
	
	$tableInformation['emergencyContact'] = $customer['relative_cus'];
	$tableInformation['emergencyContactPhone'] = $customer['relative_phone_cus'];
	$tableInformation['beginDate'] = ''.$dataInfo['begin_date_sal'];
	$tableInformation['endDate'] = ''.$dataInfo['end_date_sal'];
        $tableInformation['emissionDate'] = ''.$dataInfo['date_sal'];
	
	if($dataInfo['flights_pro'] == 1){
		$tableInformation['destination'] = $dataInfo['country_sal'].' - '.$dataInfo['city_sal'];
	}
	$tableInformation['productName'] = $dataInfo['name_pbl'];
	$tableInformation['productPrice'] = number_format($dataInfo['fee_sal'] + $fee_ext, 2, '.', ',');
	
	if($dataServ){
		foreach($dataServ as $dS){
			$service = array();
			$service['name'] = $dS['name_sbl'];
			$service['coverage'] = $dS['coverage_sbc'];
			$tableInformation['productServices'][] = $service;
		}
	}
	
	if($dataBen){
		foreach($dataBen as $d){
			$benefit = array();
			$benefit['name'] = ''.$d['description_bbl'];
			
			if((int)$d['price_bxp'] == 0){
				$benefit['amount'] = ''.$d['alias_con'];
			}
			else{
				$benefit['amount'] = '$ '.$d['price_bxp'];
				if($d['restriction_price_bxp']){
					$benefit['amount'] .= '  ('.$d['restriction_price_bxp'].' x '.$d['description_rbl'].')';
				}
			}
			$tableInformation['productBenefits'][] = $benefit;
		}
	}
	
	if($dataExt){
		foreach($dataExt as $d){
			$extra = array();
			$extra['document'] = $d['document_cus'];
			$extra['name'] = $d['first_name_cus'].' '.$d['last_name_cus'];
			$extra['age'] = GlobalFunctions::getMyAge($d['birthdate_cus']);
			$extra['fee'] = '$'.number_format($d['fee_ext'], 2, '.', '');
			$tableInformation['productExtras'][] = $extra;
		}
	}
	
	if($dataLog){
		foreach($dataLog as $d){
			$extra = array();
			$myData = unserialize($d['description_slg']);
						
			$logX['user'] = $d['first_name_usr'].' '.$d['last_name_usr'];
			$logX['date'] = $d['date_slg'];
			$logX['description'] = $myData['Description'];
			$logX['request_obs'] = $myData['global_info']['request_obs'];
			$logX['authorization_obs'] = $myData['global_info']['authorizing_obs'];
			
			switch($d['type_slg']){
				case 'VOID_INVOICE':
					$invoice=new Invoice($db);

					$invoice->setSerial_inv($myData['old_serial_inv']);
					$NumberInvOld=$invoice->getData();
					$logX['old_val'] = $invoice->getNumber_inv();

					$invoice->setSerial_inv($sale -> getSerial_inv());
					$NumberInvNew=$invoice->getData();
					$logX['new_val'] = $invoice->getNumber_inv();
					break;
				case 'ADD_DAYS':
					$logX['old_val'] = $myData['old_days_sal'];
					$logX['new_val'] = $myData['new_days_sal'];
					break;
				case 'STAND_BY':
					$logX['old_val'] = $global_salesStatus[$myData['old_status_sal']];
					$logX['new_val'] = $global_salesStatus[$myData['new_status_sal']];
					
					if(is_array($myData['travel_dates_log'])){
						$logX['old_val'] .= " (<i>Fecha de Inicio de Viaje:</i> ".$myData['travel_dates_log']['old_begin_date'];
						$logX['old_val'] .= " - <i>Fecha de Fin de Viaje:</i> ".$myData['travel_dates_log']['old_end_date'].")";
						
						$logX['new_val'] .= " (<i>Fecha de Inicio de Viaje:</i> ".$myData['travel_dates_log']['new_begin_date'];
						$logX['new_val'] .= " - <i>Fecha de Fin de Viaje:</i> ".$myData['travel_dates_log']['new_end_date'].")";
					}					
					break;
				case 'REFUNDED':
					$logX['old_val'] = $global_salesStatus[$myData['old_status_sal']];
					$logX['new_val'] = $global_salesStatus[$myData['new_status_sal']];
					break;
				case 'BLOCKED':
					$logX['old_val'] = $global_salesStatus[$myData['old_status_sal']];
					$logX['new_val'] = $global_salesStatus[$myData['new_status_sal']];
					break;
				case 'MODIFICATION':
					$old_sale_info=$myData['old_sale'];
					$new_sale_info=$myData['new_sale'];

					$oldData="<b>Inicio de cobertura:</b> ".$old_sale_info['begin_date_sal']."\n";
					$oldData.="<b>Fin de Cobertura:</b> ".$old_sale_info['end_date_sal']."\n";
					$oldData.="<b>Destino:</b> ".Country::getCountryNameByCityCode($db, $old_sale_info['serial_cit']);

					$newData="<b>Autorizado por:</b> ".$d['authorized_by']."\n";
					$newData.="<b>Inicio de cobertura:</b> ".$new_sale_info['begin_date_sal']."\n";
					$newData.="<b>Fin de Cobertura:</b> ".$new_sale_info['end_date_sal']."\n";
					$newData.="<b>Destino:</b> ".Country::getCountryNameByCityCode($db, $new_sale_info['serial_cit']);

					$logX['description'].=" \n\n<b>Estado:</b> ".$global_sales_log_status[$d['status_slg']];

					$logX['old_val'] =$oldData;
					$logX['new_val'] =$newData;
					break;
				case 'CUSTOMER_PROMO':
					$logX['description'] = html_entity_decode('Promoci&oacute;n a cliente.');
					$logX['old_val'] = html_entity_decode('Venta antes del Cup&oacute;n: ').$myData['old_sale']['total_sal'];
					$logX['new_val'] = html_entity_decode('Venta despu&eacute;s del Cup&oacute;n: ').$myData['new_sale']['total_sal'];
					break;
			}	
			
			$tableInformation['productLogs'][] = $logX;
		}
	}
	
	if($dataFiles){
		foreach($dataFiles as $d){
			$extra = array();
			$fileX['user'] = $d['name_cus'];
			$fileX['number'] = $d['serial_fle'];
			$fileX['date'] = $d['creation_date_fle'];
			$fileX['diagnosis'] = $d['diagnosis_fle'];
			$fileX['status'] = $d['status_fle'];
			$tableInformation['productFiles'][] = $fileX;
		}
	}	
	
	//*********************************************************** COUNTER ***************************************************
	$cnt = new Counter($db, $sale -> getSerial_cnt());
	$cnt -> getData();
	$usr = new User($db, $cnt -> getSerial_usr());
	$usr -> getData();
	$tableInformation['counterName'] = $usr -> getFirstname_usr() .' '. $usr -> getLastname_usr();
	
	$dealer = new Dealer($db, $cnt -> getSerial_dea());
	$dealer -> getData();
	$tableInformation['dealerName'] = $dealer -> getName_dea() . ' - ' . $dealer -> getCode_dea();
	
	$tableInformation['observations'] = $sale -> getObservations_sal();
	
	
	//*********************************************************** THE PDF REPORT ********************************************
	/*PARAMS FOR CREATING PDF FILE*/
	$header = array('number_inv' => '<b># Fac</b>',
					'card_number_sal' => '<b># Tarjeta</b>',
					'qty' => '<b>Cant.</b>',
					'date_inv' => '<b>Fec. Emision</b>',
					'name_cus' => '<b>Cliente</b>',
					'facturado_a' => '<b>Facturado a</b>',
					'total_inv' => '<b>Valor</b>');
	$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_inv'=>array('justification'=>'center','width'=>65),
							'card_number_sal'=>array('justification'=>'center','width'=>100),
							'qty'=>array('justification'=>'center','width'=>70),
							'date_inv'=>array('justification'=>'center','width'=>60),
							'name_cus'=>array('justification'=>'center','width'=>60),
							'facturado_a'=>array('justification'=>'center','width'=>120),
							'total_inv'=>array('justification'=>'center','width'=>40)));
	
	//HEADER
	$pdf->ezText('<b>REPORTE DE TARJETAS - DETALLE DE VENTA</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	//HEADER FILTERS:
	$headerDesc = array();
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Cliente:</b>'),
									'col2'=>$tableInformation['nameCustomer'],
									'col3'=>utf8_decode('<b>Documento:</b>'),
									'col4'=> utf8_decode($tableInformation['documentCustomer'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Fecha de Nacimiento:</b>'),
									'col2'=>$tableInformation['birthdateCustomer'],
									'col3'=>utf8_decode('<b>E-mail:</b>'),
									'col4'=> utf8_decode($tableInformation['emailCustomer'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Telefono:</b>'),
									'col2'=>$tableInformation['phoneCustomer'],
									'col3'=>utf8_decode('<b>Celular:</b>'),
									'col4'=> utf8_decode($tableInformation['cellphoneCustomer'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Direccion:</b>'),
									'col2'=>$tableInformation['addressCustomer'],
									'col3'=>utf8_decode('<b>Destino:</b>'),
									'col4'=> $tableInformation['destination']
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Pais:</b>'),
									'col2'=>$tableInformation['countryCustomer'],
									'col3'=>utf8_decode('<b>Ciudad:</b>'),
									'col4'=>$tableInformation['cityCustomer']
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Viaje Desde:</b>'),
									'col2'=> utf8_decode($tableInformation['beginDate']),
									'col3'=>utf8_decode('<b>Viaje Hasta:</b>'),
									'col4'=> utf8_decode($tableInformation['endDate'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Contacto de Emergencia:</b>'),
									'col2'=>$tableInformation['emergencyContact'],
									'col3'=>utf8_decode('<b>Telefono del contacto:</b>'),
									'col4'=> utf8_decode($tableInformation['emergencyContactPhone'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Producto:</b>'),
									'col2'=> utf8_decode($tableInformation['productName']),
									'col3'=>utf8_decode('<b>Valor Total:</b>'),
									'col4'=> utf8_decode($tableInformation['productPrice'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Asesor de Venta:</b>'),
									'col2'=> utf8_decode($tableInformation['counterName']),
									'col3'=>utf8_decode('<b>Sucursal de Com:</b>'),
									'col4'=> utf8_decode($tableInformation['dealerName'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Observaciones:</b>'),
									'col2'=>utf8_decode($tableInformation['observations']),
                                                                        'col3'=>utf8_decode('<b>Fecha Venta:</b>'),
									'col4'=> utf8_decode($tableInformation['emissionDate'])
	));
	array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Responsable:</b>'),
									'col2'=>utf8_decode($userByDealerData)
	));

	$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),'',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'325',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150),
					  	'col3'=>array('justification'=>'left','width'=>120),
					  	'col4'=>array('justification'=>'left','width'=>150)
			  )));
	$pdf->ezSetDy(-10);
	
	
	//******************************************************** BENEFITS TABLE ******************************************
	
	if(count($tableInformation['productServices']) > 0){
		$pdf->ezText('<b>SERVICIOS</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['name'] = '<b>Servicio</b>';
		$params['name'] = array('justification'=>'center');
		$header['coverage'] = '<b>Cobertura</b>';
		$params['coverage'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productServices'],$header,'',$params);
		$pdf->ezSetDy(-10);
	}
	
	if(count($tableInformation['productBenefits']) > 0){
		$pdf->ezText('<b>BENEFICIOS</b>', 10, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['name'] = '<b>Descripcion</b>';
		$params['name'] = array('justification'=>'center');
		$header['amount'] = '<b>Cobertura</b>';
		$params['amount'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productBenefits'],$header,'',$params);
		$pdf->ezSetDy(-10);
	}
	
	if(count($tableInformation['productExtras']) > 0){
		$pdf->ezText('<b>ADICIONALES</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['document'] = '<b>Documento</b>';
		$params['document'] = array('justification'=>'center');
		$header['name'] = '<b>Nombre</b>';
		$params['name'] = array('justification'=>'center');
		$header['age'] = '<b>Edad</b>';
		$params['age'] = array('justification'=>'center');
		$header['fee'] = '<b>Tarifa</b>';
		$params['fee'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productExtras'],$header,'',$params);
	}
	
	if(count($tableInformation['productLogs']) > 0){
		$pdf->ezSetDy(-10);
		$pdf -> ezTable(  $tableInformation['productLogs'],
						  array(
								'user' => '<b>Usuario</b>',
								'description' => '<b>Detalle</b>',
								'request_obs' => html_entity_decode('<b>Obs. de la Solicitud</b>'),
								'old_val' => '<b>Valor Anterior</b>',
								'new_val' => '<b>Valor Nuevo</b>',
								'authorization_obs' => html_entity_decode('<b>Obs. de la Autorizaci&oacute;n</b>'),
								'date' =>  '<b>Fecha</b>'
						  ),
						  '<b>MODIFICACIONES</b>',
						  array(
							'showLines' => 1,
							'showHeadings' => 1,
							'shaded' => 1,
							'fontSize' => 9,
							'textCol'  => array(0,0,0),
							'titleFontSize' => 11,
							'rowGap' => 2,
							'colGap' => 2,
							'lineCol' => array(0.4,0.4,0.4),
							'xPos' =>  300,
							'xOrientation' => 'center',
							'maxWidth' => 805,
							'cols' =>  array(
											'user' => array('justification' => 'left','width' => 80),
											'description' => array('justification' => 'left','width' => 70),
											'request_obs' => array('justification' => 'left','width' => 80),
											'old_val' => array('justification' => 'left','width' => 80),
											'new_val' => array('justification' => 'left','width' => 80),
											'authorization_obs' => array('justification' => 'left','width' => 70),
											'date' => array('justification' => 'left','width' => 50)
										),
							'innerLineThickness' => 0.8,
							'outerLineThickness' => 0.8
							)
		);
	}
	
	if(count($tableInformation['productFiles']) > 0){
		$pdf->ezSetDy(-10);
		$pdf->ezText('<b>ASISTENCIAS</b>', 12, array('justification'=>'center'));
		$pdf->ezSetDy(-10);
		unset($header);
		unset($params);
		$header['number'] = '<b>Expediente</b>';
		$params['number'] = array('justification'=>'center');
		$header['user'] = '<b>Cliente</b>';
		$params['user'] = array('justification'=>'center');
		$header['date'] = '<b>Fecha de Creacion</b>';
		$params['date'] = array('justification'=>'center');
		$header['diagnosis'] = '<b>Diagnostico</b>';
		$params['diagnosis'] = array('justification'=>'center');
		$header['status'] = '<b>Estado</b>';
		$params['status'] = array('justification'=>'center');
		$params['xPos'] = 'center';
		$params['width'] = 500;
		$pdf -> ezTable($tableInformation['productFiles'],$header,'',$params);
	}
	
	$pdf->ezStream();
?>