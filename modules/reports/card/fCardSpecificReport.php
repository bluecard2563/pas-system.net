<?php
/*
File: fCardSpecificReport
Author: Santiago Borja
Creation Date: 
*/
$zone=new Zone($db);
$nameZoneList=$zone->getZones();

$customer=new Customer($db);
$custStatusList=$customer->getAllStatus();

$invoice=new Invoice($db);
$invStatusList=$invoice->getAllStatus();

$documentByCountry = new DocumentByCountry($db);
$purposeList = $documentByCountry->getDocumentTypeValues_dbc($db);

$smarty->register('nameZoneList,custStatusList,invStatusList,purposeList');
$smarty->display();
?>
