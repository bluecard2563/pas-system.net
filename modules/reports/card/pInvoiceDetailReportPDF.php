<?php
/*
File:pInvoiceReportPDF.php
Author: Santiago Borja
Creation Date: 10/06/2010
*/

Request::setString('selCreditNote');
Request::setString('txtNumCard');
Request::setString('selZone');
Request::setString('selCountry');
Request::setString('selManager');
Request::setString('selDocumentTo');
Request::setString('txtNumInvoice');
Request::setString('txtClient');
Request::setString('hdnClientID');
Request::setString('saleSerial');
$tableInformation = array();
include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

$sale = new Sales($db, $saleSerial);
$sale -> getData();
$inv = new Invoice($db, $sale -> getSerial_inv());
$inv -> getData();
$inv = get_object_vars($inv);

//INVOICE:
$tableInformation['date'] = $inv['date_inv'];
$tableInformation['due_date'] = $inv['due_date_inv'];
$tableInformation['number'] = $inv['number_inv'];
$tableInformation['status'] = $inv['status_inv'];
$tableInformation['total'] = $inv['total_inv'];
$tableInformation['has_credit'] = $inv['has_credit_note_inv'];
$invoice_headers_info=Invoice::getInvoiceDetailInformation($db, $inv['serial_inv']);

//TABLE DATA:
$cn = new CreditNote($db);
$creditNotes = $cn -> getCreditNotesByParameters(NULL, $sale -> getSerial_inv());
if($creditNotes){
	foreach($creditNotes as $d){
		$cnn = array();
		$cnn['client'] = $d['cus_name'];
		$cnn['date'] = $d['date_cn'];
		$cnn['amount'] = $d['amount_cn'];
		$cnn['concept'] = $d['concept'];
		$cnn['saldo'] = $d['saldo'];
		$cnn['numero'] = $d['number_cn'];
		$cnn['card'] = $d['card_number_sal'];
		$tableInformation['creditNotes'][] = $cnn;
	}
}

//PAYMENTS:
if($inv['serial_pay']){
	$pay = new Payment($db, $inv['serial_pay']);
	$pay -> getData();
	
	$tableInformation['total_payed_pay'] = $pay -> getTotal_payed_pay();
	$tableInformation['saldo'] = $pay -> getTotal_to_pay_pay() - $pay -> getTotal_payed_pay();
	if($tableInformation['saldo']<0){
		$tableInformation['saldo']=$tableInformation['saldo'].' (Exceso)';
	}

	//PAYMENTS DETAIL:
	$paymentDetail=new PaymentDetail ($db, NULL, $inv['serial_pay']);
	$paymentDetail=$paymentDetail->getPaymentDetails();
	$detail_information=array();

	if(is_array($paymentDetail)){
		foreach($paymentDetail as $p){
			$aux['date_pyf']=$p['date_pyf'];
			$aux['amount_pyf']=$p['amount_pyf'];
			$aux['type_pyf'] = utf8_decode(html_entity_decode($global_paymentForms[$p['type_pyf']]));
			$aux['serial_pyf'] = $p['serial_pay'];
			$aux['number_pyf'] = $p['number_pyf'];

			switch($p['type_pyf']){
				case 'CASH':
					break;
				case 'CREDIT_CARD':
					$aux['detailed_info']='<b>Tarjeta:</b> '.$p['comments_pyf'];
					$aux['detailed_info'].=utf8_decode(' <b>Número:</b> ').$p['document_number_pyf'];
					break;
				case 'CHECK':
					$aux['detailed_info']='<b>Banco:</b> '.$p['comments_pyf'];
					$aux['detailed_info'].=utf8_decode(' <b>Número:</b> ').$p['document_number_pyf'];
					break;
				case 'CREDIT_NOTE':
					$c_note=new CreditNote($db, $p['serial_cn']);
					$c_note->getData();

					$aux['detailed_info']=utf8_decode('<b>Número:</b> ').$c_note->getNumber_cn();
					break;
				case 'TRANSFER':
					$aux['detailed_info']='<b>Banco:</b> '.$p['comments_pyf'];
					$aux['detailed_info'].=utf8_decode(' <b>Número:</b> ').$p['document_number_pyf'];
					break;
				case 'EXCESS':
					break;
				case 'UNCOLLECTABLE':
					$c_note=new CreditNote($db, $p['serial_cn']);
					$c_note->getData();

					$aux['detailed_info']='<b>Incobrable:</b> '.$p['comments_pyf'];
					$aux['detailed_info']=utf8_decode('<b>Número:</b> ').$c_note->getNumber_cn();
					break;
				case 'PAYPAL':
					break;
			}

			array_push($detail_information, $aux);
			unset($aux);
		}
	}
}else{
	$tableInformation['total_payed_pay']='N/A';
	$tableInformation['saldo'] = $inv['total_inv'];
}

/*HEADERS*/
$pdf->ezText('<b>REPORTE DE TARJETA - DETALLE DE FACTURA</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);
$headerDesc = array();

array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Facturado a:</b>'),
								'col2'=>$invoice_headers_info['invoiced_to'],
								'col3'=>'<b>RUC/CI:</b>',
								'col4'=>$invoice_headers_info['document']
));

array_push($headerDesc,array(	'col1'=>'<b>Fecha:</b>',
								'col2'=>$tableInformation['date'],
								'col3'=>utf8_decode('<b>Teléfono:</b>'),
								'col4'=>$invoice_headers_info['phone']
));

array_push($headerDesc,array(	'col1'=>utf8_decode('<b>Dirección:</b>'),
								'col2'=>$invoice_headers_info['address'],
								'col3'=>'<b>Vence:</b>',
								'col4'=>$tableInformation['due_date']
));

array_push($headerDesc,array(	'col1'=>'<b>Estado:</b>',
								'col2'=>utf8_decode($global_invoice_status[$tableInformation['status']]),
								'col3'=>utf8_decode('<b>Pagada en Días de Crédito:</b>'),
								'col4'=> $invoice_headers_info['in_date_pay']
));

array_push($headerDesc,array(	'col1'=>'<b>Total a pagar:</b>',
								'col2'=> $tableInformation['total'],
								'col3'=>'<b>Abonos</b>',
								'col4'=> $tableInformation['total_payed_pay'],
));

array_push($headerDesc,array(	'col1'=>'<b>Saldos:</b>',
								'col2'=>$tableInformation['saldo'],
								'col3'=>'<b># Factura</b>',
								'col4'=>$tableInformation['number'],
));

$pdf->ezTable($headerDesc,
		  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),'',
		  array('showHeadings'=>0,
				'shaded'=>0,
				'showLines'=>0,
				'xPos'=>'330',
				'fontSize' => 9,
				'titleFontSize' => 11,
				'cols'=>array(
					'col1'=>array('justification'=>'left','width'=>120),
					'col2'=>array('justification'=>'left','width'=>150),
					'col3'=>array('justification'=>'left','width'=>120),
					'col4'=>array('justification'=>'left','width'=>150)
		  )));

$pdf->ezSetDy(-10);
if($inv['comments_inv']):
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Observaciones de Facturaci&oacute;n: </b>').$inv['comments_inv']), 8, array('justification'=>'left'));
	$pdf->ezSetDy(-10);
endif;
$pdf->ezText(utf8_decode('* El valor pagado y pendiente pueden abarcar a más de una factura.'), 8, array('justification'=>'center'));
/*END HEADERS*/

/* INVOICE DETAILS */
$pdf->ezSetDy(-10);
$pdf->ezText('<b>DESGLOCE DE LA FACTURA</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$invoice = new Invoice($db);
$invoiceCardsDetail = $invoice->getInvoiceSales($inv['serial_inv']);

if ($invoiceCardsDetail) {
	$taxes = unserialize($invoiceCardsDetail[0]['applied_taxes_inv']);
	$discountPercentage+=$invoiceCardsDetail[0]['discount_prcg_inv'];
	$otherDiscountPercentage+=$invoiceCardsDetail[0]['other_dscnt_inv'];
	foreach ($invoiceCardsDetail as &$icd) {
		$icd['name'] .= " / " . $icd['number_inv'];
		$salesTotal+=$icd['total_sal'];
		unset($icd['discount_prcg_inv']);
		unset($icd['other_dscnt_inv']);
		unset($icd['applied_taxes_inv']);
		unset($icd['number_inv']);
	}
}
//calculate discounts and subtotal
$discount = $discountPercentage +$otherDiscountPercentage;

$subTotal1 = ($salesTotal - (($salesTotal * $discount) /100));

$subtotal = $subTotal1;
if ($taxes) {
	foreach ($taxes as $tx) {
		$taxesPercentage+=$tx['tax_percentage'];
	}
}
$taxesTotalSC = round((($subtotal * 100)/100.50),2,PHP_ROUND_HALF_UP);
$taxesTotal = $subtotal - $taxesTotalSC;
$subtotal = $taxesTotalSC;
$invoiceTotal = $subtotal + $taxesTotal;

$inv_detail = array();
$aux_detail = array( '0' => '<b>Total de Ventas:</b>',
					 '1' => number_format($salesTotal, 2, '.', ''));
array_push($inv_detail, $aux_detail);

$aux_detail = array( '0' => '<b>% Descuento:</b>',
					 '1' => number_format($discountPercentage, 2, '.', ''));
array_push($inv_detail, $aux_detail);

$aux_detail = array( '0' => '<b>% Otro Descuento:</b>',
					 '1' => number_format($otherDiscountPercentage, 2, '.', ''));
array_push($inv_detail, $aux_detail);

$aux_detail = array( '0' => '<b>Sub-total:</b>',
					 '1' => number_format($subtotal, 2, '.', ''));
array_push($inv_detail, $aux_detail);

$aux_detail = array( '0' => '<b>Impuestos:</b>',
					 '1' => number_format($taxesTotal, 2, '.', ''));
array_push($inv_detail, $aux_detail);

$aux_detail = array( '0' => '<b>Total:</b>',
					 '1' => number_format($invoiceTotal, 2, '.', ''));
array_push($inv_detail, $aux_detail);

$pdf->ezTable($inv_detail,
		  array('0'=>'','1'=>''),
			'',
		  array('showHeadings'=>0,
				'shaded'=>0,
				'showLines'=>0,
				'xPos'=>'330',
				'fontSize' => 9,
				'titleFontSize' => 11,
				'cols'=>array(
					'0'=>array('justification'=>'right','width'=>100),
					'1'=>array('justification'=>'left','width'=>100)
		  )));

$pdf->ezText(utf8_decode('* El Valor del impuesto Campesino puede variar de la factura original debido a los servicios capitados.'), 8, array('justification'=>'center'));

$pdf->ezSetDy(-10);
/* INVOICE DETAILS */

/*SALES INFORMATION*/
$header = array('number_inv' => '<b># Factura</b>',
				'card_number_sal' => '<b># Tarjeta</b>',
				'qty' => '<b>Cant.</b>',
				'date_inv' => '<b>Fec. Emision</b>',
				'name_cus' => '<b>Cliente</b>',
				'facturado_a' => '<b>Facturado a</b>',
				'total_inv' => '<b>Valor</b>');
$params = array('showHeadings'=>1,
					'shaded'=>1,
					'showLines'=>2,
					'xPos'=>'center',
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8,
					'fontSize' => 8,
					'titleFontSize' => 8,
					'cols'=>array(
						'number_inv'=>array('justification'=>'center','width'=>65),
						'card_number_sal'=>array('justification'=>'center','width'=>100),
						'qty'=>array('justification'=>'center','width'=>70),
						'date_inv'=>array('justification'=>'center','width'=>60),
						'name_cus'=>array('justification'=>'center','width'=>60),
						'facturado_a'=>array('justification'=>'center','width'=>120),
						'total_inv'=>array('justification'=>'center','width'=>40)));

/*END SALES*/

/*CREDIT NOTES INFORMATION*/
if(count($tableInformation['creditNotes']) > 0){
	$pdf->ezSetDy(-5);
	$pdf->ezText('<b>NOTAS DE CREDITO</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	unset($header);
	unset($params);
	$header['client'] = '<b>Cliente</b>';
	$params['client'] = array('justification'=>'center');
	$header['date'] = '<b>Fecha</b>';
	$params['date'] = array('justification'=>'center');
	$header['amount'] = '<b>Monto</b>';
	$params['amount'] = array('justification'=>'center');
	$header['concept'] = '<b>Concepto</b>';
	$params['concept'] = array('justification'=>'center');
	$header['saldo'] = '<b>Saldo</b>';
	$params['saldo'] = array('justification'=>'center');
	$header['numero'] = utf8_decode('<b>Número:</b> ');
	$params['numero'] = array('justification'=>'center');
	$header['card'] = '<b>Card:</b>';
	$params['card'] = array('justification'=>'center');
	$params['xPos'] = 'center';
	$params['width'] = 550;
	$pdf -> ezTable($tableInformation['creditNotes'],$header,'',$params);
	$pdf->ezSetDy(-10);
}
/*END CREDIT NOTES INFORMATION*/

/*PAYMENT DETAILS INFORMATION*/
if(count($detail_information) > 0){
	$pdf->ezSetDy(-10);
	$pdf->ezText('<b>DETALLES DE PAGOS</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);

	$header = array('date_pyf' => '<b>Fecha</b>',
					'serial_pyf' => '<b>Ref. de Pago</b>',
					'number_pyf' => '<b># de Pago</b>',
					'amount_pyf' => '<b>Monto</b>',
					'type_pyf' => '<b>Tipo</b>',					
					'detailed_info' => '<b>Detalles</b>'
					);
	$params = array('showHeadings'=>1,
						'shaded'=>0,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 10,
						'cols'=>array(
							'date_pyf'=>array('justification'=>'center','width'=>65),
							'amount_pyf'=>array('justification'=>'center','width'=>100),
							'type_pyf'=>array('justification'=>'center','width'=>100),
							'detailed_info'=>array('justification'=>'center','width'=>120)));

	$pdf -> ezTable($detail_information,$header,'',$params);
	$pdf->ezSetDy(-10);
}
/*END PAYMENT DETAILS INFORMATION*/

$pdf->ezStream();
?>
