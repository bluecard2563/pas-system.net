<?php
/**
 * File:  fLossRateReport
 * Author: Patricio Astudillo
 * Creation Date: 04-may-2011, 9:01:22
 * Description:
 * Last Modified: 04-may-2011, 9:01:22
 * Modified by:
 */

	$zone = new Zone($db);
	$zone_list = $zone ->getDealerZones($_SESSION['countryList']);

	$today = date('d/m/Y');
	$first_day='01/01/2009';

    $fileTypes = File::getTypeValues($db);
    $fileStatus = File::getStatusValues($db);

	$smarty -> register('zone_list,today,first_day,fileTypes,fileStatus');
	$smarty -> display();
?>