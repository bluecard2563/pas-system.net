<?php
/**
 * File: pFilesByServiceProviderXLS
 * Author: Ing. Patricio Astudillo
 * Creation Date: 28-jun-2012
 * Last Modified: 28-jun-2012
 * Modified By: Ing. Patricio Astudillo
 */

	Request::setInteger('selProvider');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');

	$assitance_group = ServiceProvider::getFilesByProvidedSummary($db, $selProvider, $txtBeginDate, $txtEndDate);
	$provider = new ServiceProvider($db, $selProvider);
	$provider->getData();
	$name_provider = $provider->getName_spv();
	//Debug::print_r($assitance_group);
	
	$summary = array();
	$changeOfProvider = 0;
	$month = 0;
	$months_array = array();
	global $global_weekMonthsTwoDigits;
	
	if($assitance_group){
		foreach($assitance_group as $item)
		{
			if($changeOfProvider != $item['serial_spv']):
				$changeOfProvider = $item['serial_spv'];
				$month = $item['month'];
				$summary[$changeOfProvider]['files'][$month] = 0;
				$months_array[$month] = true;

				$summary[$changeOfProvider]['name_spv'] = $item['name_spv'];
				$summary[$changeOfProvider]['total_files'] = 0;
			endif;

			if($month != $item['month']):
				$month = $item['month'];
				$summary[$changeOfProvider]['files'][$month] = 0;
				$months_array[$month] = true;
			endif;

			$summary[$changeOfProvider]['files'][$month] ++;
			$summary[$changeOfProvider]['total_files'] ++;
		}
		
		//Debug::print_r($summary);
		//Debug::print_r($months_array);
		
		//******************* HEADER
		$information = "<table border=1>
						<tr>
							<td><b>Coordinador</b></td>
						";
		
		foreach($months_array as $key=>$item){
			$information .= "<td><b>".$global_weekMonthsTwoDigits[$key]."</b></td>";
		}
		
		$information .= "	<td><b>Total</b></td>
						</tr>";
		
		//****************** BODY
		$graphic_info = array();
		$graphic_info['names'] = array();
		$graphic_info['totals'] = array();
		
		foreach($summary as $k => $data)
		{
			$information .= "<tr>
								<td>".$data['name_spv']."</td>";
			
			
			foreach($months_array as $key=>$item){
				$information .= "<td>";
				$information .= array_key_exists($key, $data['files'])?$data['files'][$key]:0;
				$information .= "</td>";
			}
			
			$information .= "	<td>".$data['total_files']."</td>
							</tr>";
			array_push($graphic_info['names'], $data['name_spv']);
			array_push($graphic_info['totals'], $data['total_files']);
		}
		
		$information .= "</table>";
		
		/************************************* GRAPHIC ***************************************/
		// Standard inclusions of pChart and pData Classes
		include(DOCUMENT_ROOT . "lib/pChart/pChart/pData.class");
		include(DOCUMENT_ROOT . "lib/pChart/pChart/pChart.class");

		// Dataset definition
		$DataSet = new pData;
		$DataSet->AddPoint($graphic_info['totals'], "Serie1"); //Data to create the Pie
		$DataSet->AddPoint($graphic_info['names'], "Serie2"); //Data to create the Legend of the graphic
		$DataSet->AddAllSeries();
		$DataSet->SetAbsciseLabelSerie("Serie2"); //Adding Labels
		// Initialise the graph
		$Test = new pChart(800, 400);
		$Test->setFontProperties(DOCUMENT_ROOT . "lib/pChart/Fonts/tahoma.ttf", 8); //setting font from Library files
		// Draw the pie chart
		$Test->AntialiasQuality = 0;
		$Test->setShadowProperties(1, 1, 200, 200, 200);
		$Test->drawFlatPieGraphWithShadow($DataSet->GetData(), $DataSet->GetDataDescription(), 200, 150, 120, PIE_PERCENTAGE, 8);
		$Test->clearShadow();

		$Test->drawPieLegend(375, 5, $DataSet->GetData(), $DataSet->GetDataDescription(), 250, 250, 250); //Draw the legend
		//Render and Saving the image in the selected folder
		$graphic_url = "system_temp_files/filesReport_".time().".png";
		//$Test->Render(DOCUMENT_ROOT . $graphic_url);
		/************************************* GRAPHIC ***************************************/
		
		$information .= "<br /><br />";
		//$information .= "<img src='".URL.$graphic_url."' alt='graphic'/>";
		
	}else{
		$information = "No existen registros para los filtros seleccionados.";
	}
	
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table>
				<tr>
					<td colspan='9'><b>REPORTE DE CASOS POR COORDINADOR</b></td>
				</tr>
				<tr>
					<td colspan='9'>$date</td>
				</tr>
				<tr>&nbsp;</tr>
				<tr>&nbsp;</tr>";
	if($txtBeginDate):
		$title .= "	<tr>
						<td><b>Asistencias creadas desde: </b></td>
						<td align='left' colspan='8'>$txtBeginDate</td>
					</tr>";
	endif;
	
	if($txtEndDate):
		$title .= "	<tr>
						<td><b>Asistencias creadas hasta: </b></td>
						<td align='left' colspan='8'>$txtEndDate</td>
					</tr>";
	endif;
	
	if ($selProvider) {
		$title .= "<tr>
						<td><b>Coordinador: </b></td>
						<td colspan='8'>$name_provider</td>
					</tr>";
	} 
	$title .= "</table><br /><br />";
	
	header("Expires: 0");
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=CasosXCoordinador_".date('m_d_Y').".xls");
	header("Pragma: no-cache");
	header("Cache-control: private");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Description: File Transfer");

	echo $title;
	echo $information;
?>
