<?php
/*
File:fAssistanceReport.php
Author: Esteban Angulo
Creation Date: 11/06/2010
*/
$zone=new Zone($db);
$zoneList=$zone->getZones();
if($_SESSION['user_type']=='DEALER'){
	$user=new User($db);
	$user->setSerial_usr($_SESSION['serial_usr']);
	$user->getData();
	$serial_dea=$user->getSerial_dea();
	$dealer=new Dealer($db);
	$dealer->setSerial_dea($serial_dea);
	$dealer->getData();
	$name=$dealer->getName_dea();
	$show='NO';
	$smarty->register('serial_dea');
}
else{
	$show='YES';
	//$show='NO';
}
$smarty->register('zoneList,show');
$smarty->display();
?>
