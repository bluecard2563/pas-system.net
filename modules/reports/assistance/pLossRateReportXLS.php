<?php
/**
 * File:  pLossRateReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 04-may-2011, 11:22:18
 * Description:
 * Last Modified: 04-may-2011, 11:22:18
 * Modified by:
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');

	Request::setInteger('selZone');
	Request::setInteger('selCountry');
	Request::setInteger('selManager');
	Request::setInteger('selComissionist');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selProduct');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('rdDateFilter');
    $selectedFileTypes = $_POST['selTypeFle'];
    $selectedFileStatus =$_POST['selStatusFle'];
    if(is_array($selectedFileTypes)){
        foreach($selectedFileTypes as &$st){
            $st = "'".$st."'";
        }
       $textFileTypes = implode(',',$selectedFileTypes );
    }
    if(is_array($selectedFileStatus)){
        foreach($selectedFileStatus as &$ss){
             $ss = "'".$ss."'";
        }
        $textFileStatus = implode(',',$selectedFileStatus );
    }
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table border='0'>
				<tr><td colspan=6>$date</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Casos</b></td></tr>
				 <tr></tr>
			</table><br />";
	
	//****************************************************** FILTER SECTION
	$name_zon = new Zone($db, $selZone);
	$name_zon -> getData();
	$name_zon = $name_zon -> getName_zon();
	
	if($selCountry){
		$name_cou = new Country($db, $selCountry);
		$name_cou -> getData();
		$name_cou = $name_cou -> getName_cou();
	}else{
		$name_cou = "Todos";
	}
	
	if($selManager){
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
	}else{
		$name_man = "Todos";
	}
	
	if($selComissionist){
		$name_ubd = new User($db, $selComissionist);
		$name_ubd -> getData();
		$name_ubd = $name_ubd ->getFirstname_usr().' '.$name_ubd ->getLastname_usr();
	}else{
		$name_ubd = "Todos";
	}
	
	if($selDealer){
		$name_dea = new Dealer($db, $selDealer);
		$name_dea -> getData();
		$name_dea = $name_dea -> getName_dea().' - '.$name_dea ->getCode_dea();
	}else{
		$name_dea = "Todos";
	}
	
	if($selBranch){
		$name_bra = new Dealer($db, $selBranch);
		$name_bra -> getData();
		$name_bra = $name_bra -> getName_dea().' - '.$name_bra ->getCode_dea();
	}else{
		$name_bra = "Todos";
	}
	
	if($selProduct){
		$name_pro = new ProductbyLanguage($db);
		$name_pro -> setSerial_pro($selProduct);
		$name_pro ->setSerial_lang($_SESSION['serial_lang']);
		$name_pro -> getDataByProductAndLanguage();
		$name_pro = $name_pro -> getName_pbl();		
	}else{
		$name_pro = "Todos";
	}
	
	
	$parameters = "<table border='1'>
					<tr>
						<td align='right'><b>Zona: </b></td><td align='left'>" . $name_zon . "</td>
						<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
					</tr>
					<tr>
						<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
						<td align='right'><b>Responsable:</b></td><td align='left'>" . $name_ubd . "</td>
					</tr>
					<tr>
						<td align='right'><b>Comercializador:</b></td><td align='left'>" . $name_dea . "</td>
						<td align='right'><b>Sucursal:</b></td><td align='left'>" . $name_bra . "</td>
					</tr>
					<tr>
						<td align='right'><b>Producto:</b></td><td align='left'>" . $name_pro . "</td>
					</tr>
					<tr>
						<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $txtBeginDate . "</td>
						<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $txtEndDate . "</td>
					</tr>
					<tr></tr>
				</table><br />";
	
	if($selCountry){ 		
		//************************************** SPECIFIC REPORT FOR EACH COUNTRY ************************************
		$assistance_info = File::getLossRateInformation($db, $selCountry, $txtBeginDate, $txtEndDate, $rdDateFilter, $selManager, 
													$selComissionist, $selDealer, $selBranch, $selProduct , $textFileTypes ,$textFileStatus);

		/**Calculate authorized amount and presented amount if it have many documents by file**/

		$assistance_info_aux = array();
		$exist=0;
		if(is_array($assistance_info) && sizeof($assistance_info)>0){
			foreach($assistance_info as $key1 => &$ai){

				if(is_numeric($ai['presented_amount'])){

					$exist=0;
						foreach($assistance_info_aux as $aia){
							if($ai['serial_bxp']==$aia['serial_bxp'] && $ai['serial_fle']==$aia['serial_fle'] && $ai['serial_spv']==$aia['serial_spv']){
								$exist=1;
							}
						}

						if($exist==0){
							$aux=$ai;
							$aux['number_doc']=1;
								foreach($assistance_info as $key2 =>  &$ai2){
									if($key2!=$key1 && is_numeric($ai2['presented_amount'])){
										if($ai['serial_bxp']==$ai2['serial_bxp'] && $ai['serial_fle']==$ai2['serial_fle'] && $ai['serial_spv']==$ai2['serial_spv']){
											$aux['presented_amount']+=$ai2['presented_amount'];
											$aux['authorized_amount']+=$ai2['authorized_amount'];
	//										$aux['repricing_amount']+=$ai2['repricing_amount'];
	//										$aux['fee_amount']+=$ai2['fee_amount'];
											$aux['number_doc']+=1;
										}
									}
								}
								$aux['estimated_amount_spf'] = number_format($aux['estimated_amount_spf'],2,',','');
								$aux['presented_amount'] = number_format($aux['presented_amount'],2,',','');
	//							$aux['repricing_amount'] = number_format($aux['repricing_amount'],2,',','');
	//							$aux['fee_amount'] = number_format($aux['fee_amount'],2,',','');
								$aux['authorized_amount'] = number_format($aux['authorized_amount'],2,',','');

								if($aux['presented_amount']!=0 && $aux['authorized_amount']!=0){
									$aux['estimated_amount_spf']=0;
								}

								array_push($assistance_info_aux, $aux);
						}
				}else{
					$ai['number_doc']=1;
					array_push($assistance_info_aux, $ai);
				}
			}
		}
		if($assistance_info_aux){
			$table = "<table border=1>
				<tr>
					<td><b>Fecha de Ocurrencia</b></td>
					<td><b>Pa&iacute;s de Emisi&oacute;n</b></td>
					<td><b>Pa&iacute;s de Asistencia</b></td>
					<td><b>Pasajero</b></td>
					<td><b>Tarjeta</b></td>
					<td><b>Producto</b></td>
					<td><b>Causa</b></td>
					<td><b>Tipo Asistencia</b></td>
					<td><b>Diagn&oacute;stico</b></td>
					<td><b>Reserva</b></td>
					<td><b>Valor Presentado</b></td>
					<td><b>Repricing</b></td>
					<td><b>Fee</b></td>
					<td><b>Total Real</b></td>
					<td><b>Corresponsal</b></td>
					<td><b>Beneficio</b></td>
					<td><b>Documentos Involucrados</b></td>
                    <td><b>Estado de Caso</b></td>
					<td><b>Fecha del Pago</b></td>
					<td><b>Creado hace (d&iacute;as)</b></td>
				</tr>
				";
		if(is_array($assistance_info_aux) && sizeof($assistance_info_aux)>0){
			foreach($assistance_info_aux as $a){
				$table .= "<tr>
							<td>{$a['incident_date_fle']}</td>
							<td>{$a['sold_country']}</td>
							<td>{$a['assistance_country']}</td>
							<td>{$a['name_cus']}</td>
							<td>{$a['card_number']}</td>
							<td>{$a['name_product']}</td>
							<td>{$a['cause_fle']}</td>
							<td>{$global_fileTypes[$a['type_fle']]}</td>
							<td>{$a['diagnosis_fle']}</td>
							<td>{$a['estimated_amount_spf']}</td>
							<td>{$a['presented_amount']}</td>
							<td>0</td>
							<td>0</td>
							<td>{$a['authorized_amount']}</td>
							<td>{$a['name_spv']}</td>
							<td>{$a['description_bbl']}</td>
							<td>{$a['number_doc']}</td>
                            <td>{$global_fileStatus[$a['status_fle']]}</td>
							<td>{$a['dispatched_date_prq']}</td>
							<td>{$a['elapsed_time']}</td>
						</tr>";
			}
		}	

			$table .="  </table>";
		}else{
			$table = "<br /> <b> No existe informaci&oacute;n para los filtros seleccionados</b>";
		}	
	}else{
		//************************************************ GLOBAL REPORT ******************************************
		$assistance_info = File::getLossRateInformationGlobal($db, $selZone, $txtBeginDate, $txtEndDate, $rdDateFilter);
		
		if($assistance_info){
									
			$table = "<table border=1>
				<tr>
					<td><b>Pa&iacute;s</b></td>
					<td><b>Ventas Brutas</b></td>
					<td><b>Reservas Siniestros</b></td>
					<td><b>Valor Total Presentado</b></td>
					<td><b>Valor Real Siniestros</b></td>
					<td><b>% &Iacute;ndice de Siniestralidad Reservas</b></td>
					<td><b>% &Iacute;ndice de Siniestralidad Real</b></td>
				</tr>
				";
		
			foreach($assistance_info as $a){
				$reserve_percentage = number_format(($a['estimated_amount_spf'] / $a['total_sold']) * 100 , 2);
				$real_percentage = number_format(($a['authorized_amount'] / $a['total_sold']) * 100 , 2);
				
				$table .= "<tr>
							<td>{$a['name_cou']}</td>
							<td>".number_format($a['total_sold'], 2)."</td>
							<td>".number_format($a['estimated_amount_spf'], 2)."</td>
							<td>".number_format($a['presented_amount'], 2)."</td>
							<td>".number_format($a['authorized_amount'], 2)."</td>
							<td>$reserve_percentage</td>
							<td>$real_percentage</td>
						</tr>";
			}
			$table .="  </table>";
		}else{
			$table = "<br /> <b> No existe informaci&oacute;n para los filtros seleccionados</b>";
		}
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Casos.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $parameters;	
	echo $table;
?>
