<?php
/*
File: pMedicalCheckupReportPDF
Author: David Bergmann
Creation Date: 16/12/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:serial_prq');
include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

/*Data*/
$data = File::getMedicalCheckupReport($db,$serial_prq);
//die(Debug::print_r($data));
/*End Data*/

$pdf->ezText(html_entity_decode('<b>INFORME DE AUDITOR&Iacute;A M&Eacute;DICA</b>'), 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

/*CUSTOMER DATA*/
$pdf->ezText(html_entity_decode('<b>Datos del cliente:</b>'), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$customer_info[0] = array('col1'=>utf8_decode('<b>Nombre:</b>'),
						  'col2'=>$data['name_cus'],
						  'col3'=>utf8_decode('<b>Fecha de nacimiento:</b>'),
						  'col4'=>$data['birthdate_cus']);

$pdf->ezTable($customer_info,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>60),
						'col2'=>array('justification'=>'left','width'=>160),
						'col3'=>array('justification'=>'left','width'=>120),
						'col4'=>array('justification'=>'left','width'=>100),
						)));

$pdf->ezSetDy(-10);

$history_info[0] = array('col1'=>utf8_decode('<b>Historial Médico:</b>'));
if($data['medical_background_cus']) {
	$history_info[1] = array('col1'=>$data['medical_background_cus']);
} else {
	$history_info[1] = array('col1'=>utf8_decode('No existen registros.'));
}


$pdf->ezTable($history_info, array('col1'=>''), '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>440))));

$pdf->ezSetDy(-10);
/*END CUSTOMER DATA*/

/*SALE DATA*/
$pdf->ezText(html_entity_decode('<b>Datos de la venta:</b>'), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$sale_info[0] = array('col1'=>utf8_decode('<b>Número de tarjeta:</b>'),
					  'col2'=>$data['card_number'],
					  'col3'=>utf8_decode('<b>Número de expediente:</b>'),
					  'col4'=>$data['serial_fle']);
$sale_info[1] = array('col1'=>utf8_decode('<b>Producto:</b>'),
					  'col2'=>$data['name_pbl'],
					  'col3'=>utf8_decode('<b>Fecha de compra:</b>'),
					  'col4'=>$data['emission_date_sal']);
$sale_info[2] = array('col1'=>utf8_decode('<b>Inicio de vigencia:</b>'),
					  'col2'=>$data['begin_date_sal'],
					  'col3'=>utf8_decode('<b>Fin de vigencia:</b>'),
					  'col4'=>$data['end_date_sal']);

$pdf->ezTable($sale_info,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>110),
						'col2'=>array('justification'=>'left','width'=>100),
						'col3'=>array('justification'=>'left','width'=>140),
						'col4'=>array('justification'=>'left','width'=>90),
						)));
$pdf->ezSetDy(-10);
/*END SALE DATA*/

/*MEDICAL DATA*/
$pdf->ezText(html_entity_decode('<b>Datos m&eacute;dicos:</b>'), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$medical_info[0] = array('col1'=>utf8_decode('<b>Tipo de asistencia:</b>'),
						 'col2'=>$data['type_fle']);
$medical_info[1] = array('col1'=>utf8_decode('<b>Diagnóstico:</b>'),
						 'col2'=>$data['diagnosis_fle']);
$medical_info[2] = array('col1'=>utf8_decode('<b>Causa:</b>'),
						 'col2'=>$data['cause_fle'],
						 'col3'=>'',
						 'col4'=>'');

$pdf->ezTable($medical_info,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>110),
						'col2'=>array('justification'=>'left','width'=>330))));
$pdf->ezSetDy(-10);
/*END MEDICAL DATA*/

/*MEDICAL CHECKUP DATA*/
$pdf->ezText(html_entity_decode('<b>Conclusi&oacute;n m&eacute;dica:</b>'), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$medical_checkup[0] = array('col1'=>$data['auditor_comments']);

$pdf->ezTable($medical_checkup, array('col1'=>''), '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
				    'splitRows' => 1,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>440))));

/*END MEDICAL CHECKUP DATA*/

$pdf->ezStream();
?>