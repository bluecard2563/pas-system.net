<?php
/**
 * File: pNonPaidAssistance
 * Author: Patricio Astudillo M.
 * Creation Date: 23/02/2012 03:57:47 PM
 * Last Modified: 23/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('selCountry');
	Request::setString('selManager');
	Request::setString('selResponsable');
	Request::setString('selDealer');
	Request::setString('txtCardNumber');
 
	
	if(!$txtCardNumber){ //GET THE INFO FOR GEOGRAPHIC FILTERS
		$xCountry = new Country($db, $selCountry);
		$xCountry ->getData();
		$name_cou = ($xCountry ->getName_cou())?$xCountry ->getName_cou():'Todos';
		
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
		$name_man = ($name_man)?$name_man:'Todos';
		
		$xUser = new User($db, $selResponsable);
		$xUser ->getData();
		$name_usr = ($xUser ->getFirstname_usr())?$xUser ->getFirstname_usr().' '.$xUser ->getLastname_usr():'Todos';
		
		$xDealer = new Dealer($db, $selDealer);
		$xDealer ->getData();
		$name_dea = ($xDealer ->getName_dea())?$xDealer ->getName_dea():'Todos';
		
		$filters_info = "<tr>
							<td><b>Pa&iacute;s:</b></td>
							<td>$name_cou</td>
						</tr>
						<tr>
							<td><b>Representante:</b></td>
							<td>$name_man</td>
						</tr>
						<tr>
							<td><b>Responsable:</b></td>
							<td>$name_usr</td>
						</tr>
						<tr>
							<td><b>Comercializador:</b></td>
							<td>$name_dea</td>
						</tr>";
		
		
		
		$file_list = File::getAssistancesWithNonPaidInvoices($db, true, $selCountry, $selManager, $selResponsable, $selDealer);
	}else{ //GET THE INFORMATION FOR A SINGLE CARD
		$sale = new Sales($db);
		$sale->setCardNumber_sal($txtCardNumber);
		$sale->getDataByCardNumber();
		$aux_data = $sale->getAux();
		
		$filters_info = "<tr>
							<td><b>No. Tarjeta</b></td>
							<td align='left'>$txtCardNumber</td>
						</tr>";
		
		$file_list = File::getAssistancesWithNonPaidInvoicesBySale($db, $sale->getSerial_sal(), $aux_data['serial_trl']);
	}
	
	//Debug::print_r($file_list);
	
	//******************************* HEADER AND FILTERS FOR THE TABLE
	global $date;
	$date = html_entity_decode($date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year'] . ' a las ' . date("G:i:s a"));
	
	$title = "<table>
				<tr>
					<td colspan='9'><b>REPORTE DE ASISTENCIAS CON TARJETAS IMPAGAS</b></td>
				</tr>
				<tr>
					<td colspan='9'>$date</td>
				</tr>
				<tr>&nbsp;</tr>
				<tr>&nbsp;</tr>
				$filters_info
			</table><br /><br />";
	
	if($file_list){
		$table = '<table border="1" class="dataTable" id="files_table">
			<THEAD>
				<tr class="header">
					<th style="text-align: center;">
						No. Tarjeta
					</th>
					<th style="text-align: center;">
						Cliente
					</th>
					<th style="text-align: center;">
						Inicio de Vigencia
					</th>
					<th style="text-align: center;">
						Fin de Vigencia
					</th>
					<th style="text-align: center;">
						Fecha de Asistencia
					</th>
					<th style="text-align: center;">
						Fecha de la Factura
					</th>
					<th style="text-align: center;">
						Tiempo de Cr&eacute;dito
					</th>
					<th style="text-align: center;">
						Valor a Pagar
					</th>
					<th style="text-align: center;">
						Comercializador
					</th>
					<th style="text-align: center;">
						Responsable
					</th>				
				</tr>
			</THEAD>

			<TBODY>';
		
		foreach($file_list as $cl){
			$table .= '<tr  style="padding:5px 5px 5px 5px; text-align:left;">
							<td class="tableField">
								'.$cl['card_number_sal'].'
							</td>
							<td class="tableField">
								'.$cl['name_cus'].'
							</td>
							<td class="tableField">
								'.$cl['begin_date_sal'].'
							</td>
							<td class="tableField">
								'.$cl['end_date_sal'].'
							</td>
							<td class="tableField">
								'.$cl['creation_date_fle'].'
							</td>
							<td class="tableField">
								'.$cl['date_inv'].'
							</td>
							<td class="tableField">
								'.$cl['days_cdd'].'
							</td>
							<td class="tableField">
								'.$cl['total_inv'].'
							</td>
							<td class="tableField">
								'.$cl['name_dea'].'
							</td>
							<td class="tableField">
								'.$cl['name_ubd'].'
							</td>
						</tr>';
		}
		
		$table .= '	</TBODY>
					</table>';
	}else{
		$table = '<b>No existen registros para los parametros seleccionados</b>';
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Pagos_Asistencia.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $table;
?>
