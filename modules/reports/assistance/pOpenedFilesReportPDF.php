<?php
/**
 * File:  pOpenedFilesReportPDF
 * Author: Patricio Astudillo
 * Creation Date: 03-ago-2011, 12:05:53
 * Description:
 * Last Modified: 16-ago-2011, 10:00:00
 * Modified by: Nicolas Flores
 */

	Request::setInteger('selGroup');
	Request::setString('today');
	Request::setString('txtDateFilter');
	
	if(!$txtDateFilter){
		$txtDateFilter = $today;
	}
	
	$files_list = AssistanceGroupsByUsers::getOpenedFilesByDate($db, $selGroup, $txtDateFilter);
	
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';
	
	$pdf->ezText('<b>Reporte de Apertura de Expedientes</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	$name_asg = new AssistanceGroup($db, $selGroup);
	$name_asg -> getData();
	$name_asg = $name_asg -> getName_asg();
	
	$headerDesc=array();
	$descLine1=array('col1'=>'<b>Grupo de Asistencias:</b>',
					 'col2'=>$name_asg);
	array_push($headerDesc,$descLine1);
	$descLine2=array('col1'=>'<b>Fecha:</b>',
					 'col2'=>$txtDateFilter);
	array_push($headerDesc,$descLine2);
	
	$pdf->ezTable($headerDesc,
				  array('col1'=>'','col2'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>0,
						'xPos'=>'150',
						'fontSize' => 9,
						'titleFontSize' => 11,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>120),
							'col2'=>array('justification'=>'left','width'=>120)
							)));
	$pdf->ezSetDy(-20);
	
	if($files_list){	
		foreach($files_list as &$a){
			$a['status_fle'] = utf8_decode(html_entity_decode($global_fileStatus[$a['status_fle']]));
		}
		
		$pdf->ezSetDy(-10);
		$pdf->ezTable(	$files_list,
						array(	'creation_hour'=>'<b>Hora de Apertura</b>',
								'card_number_sal'=>'<b>No. Tarjeta</b>',
								'serial_fle'=>'<b>Expediente</b>',
								'diagnosis_fle'=>'<b>Motivo de la Llamada</b>',
								'status_fle'=>utf8_decode('<b>Estado</b>'),
								'cause_fle'=>utf8_decode('<b>Observaciones</b>'),
								'opening_usr'=>utf8_decode('<b>Operador</b>'),
								'estimated_amount'=>utf8_decode('<b>Valor Estimado de Asistencia (USD)</b>')
						),
						'',
						array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'400',
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'creation_hour'=>array('justification'=>'center','width'=>50),
							'card_number_sal'=>array('justification'=>'center','width'=>60),
							'serial_fle'=>array('justification'=>'center','width'=>60),
							'diagnosis_fle'=>array('justification'=>'center','width'=>110),
							'status_fle'=>array('justification'=>'center','width'=>80),
							'cause_fle'=>array('justification'=>'center','width'=>100),
							'opening_usr'=>array('justification'=>'center','width'=>100),
							'estimated_amount'=>array('justification'=>'center','width'=>80)
						)));

			
	}else{
		$pdf->ezText('<b>No existen datos para los filtros seleccionados</b>', 10, array('justification'=>'center'));
	}
	
	$pdf->ezStream();
?>
