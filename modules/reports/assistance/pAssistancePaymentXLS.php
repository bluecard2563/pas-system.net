<?php
/*
  Document   : pAssistancePaymentXLS
  Created on : 28/09/2011, 11:21:36 PM
  Author     : Luis Salvador
 * Modified on: 08/09/2016
 * Modified BY: Patricio Astudillo
 */

	Request::setString('selPayTo,txtBeginDate,txtEndDate,selProvider,selCountry,selCity,selectedBoxes,selProcessAs');

	//************************** FILTERS SECTION ********************************
	global $date;
	$date = html_entity_decode($date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year'] . ' a las ' . date("G:i:s a"));
	$selected_claims = NULL;
	$pay_to = $global_billTo[$selPayTo];

	switch ($selPayTo) {
		case 'CLIENT':
			$name_cou = new Country($db, $selCountry);
			$name_cou->getData();
			$name_cou = $name_cou->getName_cou();

			if ($selCity) {
				$name_cit = new City($db, $selCity);
				$name_cit->getData();
				$name_cit = $name_cit->getName_cit();
			} else {
				$name_cit = 'Todas';
			}

			break;
		case 'PROVIDER':
			$provider = new ServiceProvider($db, $selProvider);
			$provider->getData();
			$name_provider = $provider->getName_spv();
			break;

		default:
			break;
	}

	if (is_array($_POST['chkClaims']))
		$selected_claims = implode(',', $_POST['chkClaims']);
	if ($selectedBoxes != '')
		$selected_claims = $selectedBoxes;

	$claims_list = PaymentRequest::getClaimsForReport($db, $selPayTo, $txtBeginDate, $txtEndDate, $selProcessAs, $selProvider, $selCity, $selected_claims);

	$title = "<table>
					<tr>
						<td colspan='9'><b>LISTADO DE FACTURAS DE ASISTENCIA</b></td>
					</tr>
					<tr>
						<td colspan='9'>$date</td>
					</tr>
					<tr>&nbsp;</tr>
					<tr>&nbsp;</tr>
					<tr>
						<td><b>PAGO A: </b></td>
						<td align='left' colspan='8'>$pay_to</td>
					</tr>
					<tr>
						<td><b>FECHA DESDE: </b></td>
						<td align='left' colspan='8'>$txtBeginDate</td>
					</tr>
					<tr>
						<td><b>FECHA HASTA:  </b></td>
						<td align='left' colspan='8'>$txtEndDate</td>
					</tr>";

	if ($selPayTo == 'PROVIDER') {
		$title .= "<tr>
						<td><b>COORDINADOR: </b></td>
						<td colspan='8'>$name_provider</td>
					</tr>";
	} elseif ($selPayTo == 'CLIENT') {
		$title .= " <tr>
						<td><b>PAIS: </b></td>
						<td colspan='8'>$name_cou</td>
					</tr>
					<tr>
						<td><b>CIUDAD: </b></td>
						<td align='left' colspan='8'>$name_cit</td>
					</tr>";
	}
	$title .= "%%total_claimed%%</table><br /><br />";

	//******************************** BUIDING THE REPORT ******************************
	if (is_array($claims_list)) {
		$total_claimed = 0;

		foreach ($claims_list as &$item) {
			if ($item['type_prq'] == 'CREDIT_NOTE') {
				$item['amount_authorized_prq'] = $item['amount_authorized_prq'] * -1;
			}
			If ($item['status'] == 'PENDING') {
				$days = $item['daysnp'];
				$fechapago = $item['date'];
			} else {
				$days = $item['days'];
				$fechapago = $item['dispatched_date_prq'];
			}
			Switch ($selProcessAs):
				case 'PAYABLE': $var = 'Pago';
					$var2 = '';
					break;
				case 'NOPAYABLE': $var = 'Actual';
					$var2 = 'Mora';
					break;
				case 'COLLECTABLE': $var = 'Cobro';
					$var2 = '';
					break;
				case 'NOCOLLECTABLE': $var = 'Actual';
					$var2 = 'Mora';
					break;
			endswitch;

			$item['type_prq'] = html_entity_decode($global_payment_request_type[$item['type_prq']]);
			$total_claimed += $item['amount_authorized_prq'];

			$records_info .= "<tr>
									<td>{$item['card_number_sal']}</td>									
									<td>{$item['serial_fle']}</td>
									<td>{$item['diagnosis_fle']}</td>
									<td>{$item['cause_fle']}</td>    
									<td>{$item['name_dbf']}</td>
									<td>{$item['name_cou']}</td>
									<td>{$item['comment_dbf']}</td>
									<td>{$item['type_prq']}</td>
									<td>{$item['amount_reclaimed_prq']}</td>
									<td>{$item['amount_authorized_prq']}</td>
									<td>{$item['status_prq']}</td>
									<td>{$item['incident_date_fle']}</td>
									<td>{$item['name_cus']}</td>
									<td>{$item['name_spv']}</td>
									<td>{$item['destination_cou']}</td>
									<td>{$item['request_date_prq']}</td>
									<td>{$fechapago}</td>
									<td>{$days}</td>
									<td>{$global_typeDocumentFile[$item['status']]}</td>
							  </tr>";
		}

		$records = "<table>
								<tr>
									<td colspan='9'>
										<b>Listado de Documentos</b>
									</td>
								</tr>
								<tr>
									<td><b>Tarjeta</b></td>
									<td><b>Expediente</b></td>
									<td><b>Diagnostico</b></td>
									<td><b>Causa</b></td>
									<td><b>Documento</b></td>
									<td><b>Pais Asistencia</b></td>
									<td><b>N. Documento</b></td>
									<td><b>Concepto</b></td>
									<td><b>Valor Presentado</b></td>
									<td><b>Valor Aprobado</b></td>
									<td><b>Estado</b></td>
									<td><b>Fecha Ocurrencia</b></td>
									<td><b>Cliente</b></td>
									<td><b>Corresponsal</b></td>
									<td><b>Pais de Destino</b></td>
									<td><b>Fecha de Carga</b></td>
									<td><b>Fecha $var</b></td>
									<td><b>Dias $var2</b></td>
									<td><b>Estado</b></td>";
		if ($selType == 'ALL') {
			$records .= "<td>Estado</td>";
		}

		$records .="	</tr>
							$records_info
						</table>";

		$total_claimed = "<tr>
							<td><b>Total</b></td>
							<td>$" . number_format($total_claimed, 2, '.', '') . "</td>
						  </tr>";
	} else {
		$records = 'No existen datos para los filtros seleccionados.';
		$total_claimed = "";
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Pagos_Asistencia.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$title = str_replace('%%total_claimed%%', $total_claimed, $title);

	echo $title;
	echo $records;
?>