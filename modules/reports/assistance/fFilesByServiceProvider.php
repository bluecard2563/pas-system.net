<?php
/**
 * File: fFilesByServiceProvider
 * Author: Patricio Astudillo
 * Creation Date: 26/06/2012
 * Last Modified: 26/06/2012
 * Modified By: Patricio Astudillo
 * 
 */

	$spv = new ServiceProvider($db);
	$providers = $spv ->getAllServiceProviders();
	$today = date('d/m/Y');

	$smarty -> register('providers,today');
	$smarty -> display();
?>
