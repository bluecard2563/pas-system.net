<?php
/**
 * File: fFileLog
 * Author: Patricio Astudillo
 * Creation Date: 06/08/2015
 * Last Modified By: 06/08/2015
 */

	Request::setInteger('0:error');
	
	$smarty->register('error');
	$smarty->display();