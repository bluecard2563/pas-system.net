<?php

/**
 * File: pFileLogXLS
 * Author: Patricio Astudillo
 * Creation Date: 06/08/2015
 * Last Modified By: 06/08/2015
 */

Request::setString('txtBeginDate');
Request::setString('txtEndDate');
Request::setInteger('txtFileNro');

if ($txtFileNro) {
	$log_array = FilesLog::getFileHistory($db, $txtFileNro);
} else {
	$log_array = FilesLog::getFilesLogForAudit($db, $txtBeginDate, $txtEndDate);
}

//Debug::print_r($log_array);

if ($log_array) {
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . $global_weekDaysNumb[$date['weekday']] . ' ' . $date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year']);

	$title = '<table>
							<tr> <th colspan="9"><b>' . 'SISTEMA ' . $global_system_name . '</b></th> </tr>
							<tr> <th colspan="9">' . $date . '</th> </tr>
							<tr></tr>
							<tr> <th colspan="9"><b>' . html_entity_decode("REPORTE DE STATUS DE EXPEDIENTE") . '</b></th> </tr>
							<tr></tr>';
	if(!$txtFileNro):
		$title .= '			<tr> <td><b>Fecha Inicio: </b></td> <td>' . $txtBeginDate . '</td> </tr>
							<tr> <td><b>Fecha Fin: </b></td> <td>' . $txtEndDate . '</td> </tr>';
	endif;
	
	if($txtFileNro):
		$title .= '				<tr> <td><b>Nro. de Caso: </b></td> <td>' . $txtFileNro . '</td> </tr>';
	endif;
	$title .= '				<tr></tr>
					</table>';


	$data = '<table> '
                . '<tr> '
                    . '<th><b>Nro. Expediente</b></th> '
					. '<th><b>Estado</b></th> '
                    . '<th><b>Fecha de Apertura</b></th> '
					. '<th><b>Usuario de Apertura</b></th> '
                    . '<th><b>Fecha de Paso/Cierre</b></th> '
					. '<th><b>Usuario de Paso/Cierre</b></th> '
                    . '<th><b>Tiempo en el estado actual</b></th> '
                . '</tr>';
	
	foreach ($log_array as $item) {
		$data .= '<tr> '
                    . '<td>'.$item['serial_fle'].'</td> '
					. '<td>'.$global_fileStatus[$item['status']].'</td> '
                    . '<td>'.$item['opened_on'].'</td> '
                    . '<td>'.$item['opener'].'</td> '
                    . '<td>'.$item['finished_on'].'</td> '
                    . '<td>'.$item['finisher'].'</td> '
                    . '<td>'.$item['timeElapsed'].'</td> '
                . '</tr>';
	}
	
	$data .= '</table>';
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=LogDeExpediente.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
			
	echo $title.'<br />';
	echo $data;
	
} else {
	http_redirect('modules/reports/assistance/fFileLog/2');
}