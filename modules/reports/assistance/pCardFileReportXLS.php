<?php
/**
 * File:  pCardFileReport
 * Author: Patricio Astudillo
 * Creation Date: 05-oct-2011, 15:22:18
 * Description:
 * Last Modified: 16-ago-2016, 11:25:53
 * Modified by: David Rosales
 */

	set_time_limit(36000);
	ini_set('memory_limit','1024M');

	Request::setInteger('selZone');
	Request::setInteger('selCountry');
	Request::setInteger('selManager');
	Request::setInteger('selComissionist');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setInteger('selProduct');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('rdDateFilter');
	
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table border='0'>
				<tr><td colspan=6>$date</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Casos</b></td></tr>
				 <tr></tr>
			</table><br />";
	
	//****************************************************** FILTER SECTION
	$name_zon = new Zone($db, $selZone);
	$name_zon -> getData();
	$name_zon = $name_zon -> getName_zon();
	
	if($selCountry){
		$name_cou = new Country($db, $selCountry);
		$name_cou -> getData();
		$name_cou = $name_cou -> getName_cou();
	}else{
		$name_cou = "Todos";
	}
	
	if($selManager){
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
	}else{
		$name_man = "Todos";
	}
	
	if($selComissionist){
		$name_ubd = new User($db, $selComissionist);
		$name_ubd -> getData();
		$name_ubd = $name_ubd ->getFirstname_usr().' '.$name_ubd ->getLastname_usr();
	}else{
		$name_ubd = "Todos";
	}
	
	if($selDealer){
		$name_dea = new Dealer($db, $selDealer);
		$name_dea -> getData();
		$name_dea = $name_dea -> getName_dea().' - '.$name_dea ->getCode_dea();
	}else{
		$name_dea = "Todos";
	}
	
	if($selBranch){
		$name_bra = new Dealer($db, $selBranch);
		$name_bra -> getData();
		$name_bra = $name_bra -> getName_dea().' - '.$name_bra ->getCode_dea();
	}else{
		$name_bra = "Todos";
	}
	
	if($selProduct){
		$name_pro = new ProductbyLanguage($db);
		$name_pro -> setSerial_pro($selProduct);
		$name_pro ->setSerial_lang($_SESSION['serial_lang']);
		$name_pro -> getDataByProductAndLanguage();
		$name_pro = $name_pro -> getName_pbl();		
	}else{
		$name_pro = "Todos";
	}
	
	$parameters = "<table border='1'>
					<tr>
						<td align='right'><b>Zona: </b></td><td align='left'>" . $name_zon . "</td>
						<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
					</tr>
					<tr>
						<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
						<td align='right'><b>Responsable:</b></td><td align='left'>" . $name_ubd . "</td>
					</tr>
					<tr>
						<td align='right'><b>Comercializador:</b></td><td align='left'>" . $name_dea . "</td>
						<td align='right'><b>Sucursal:</b></td><td align='left'>" . $name_bra . "</td>
					</tr>
					<tr>
						<td align='right'><b>Producto:</b></td><td align='left'>" . $name_pro . "</td>
					</tr>
					<tr>
						<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $txtBeginDate . "</td>
						<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $txtEndDate . "</td>
					</tr>
					<tr></tr>
				</table><br />";
			
	//************************************** MAIN INFORMATION PER CARD  ************************************
	$assistance_info = File::getLossRateInformationByCard($db, $selCountry, $txtBeginDate, $txtEndDate, $rdDateFilter, $selManager, 
												$selComissionist, $selDealer, $selBranch, $selProduct);

	if($assistance_info){
		$table = "<table border=1>
			<tr>
				<td><b>Pa&iacute;s de Emisi&oacute;n</b></td>
				<td><b>Pa&iacute;s de Asistencia</b></td>
                                <td><b>Ciudad de Asistencia</b></td>
				<td><b>Cliente</b></td>
				<td><b>Fecha de Nacimiento</b></td>
				<td><b>Tarjeta</b></td>
				<td><b>Fecha Inicio</b></td>
				<td><b>Fecha Fin</b></td>
				<td><b>Producto</b></td>
				<td><b>No. Expediente</b></td>
				<td><b>Coordinador</b></td>
				<td><b>Fecha de Ocurrencia</b></td>
				<td><b>Estado</b></td>
				<td><b>Tipo</b></td>
				<td><b>Diagn&oacute;stico</b></td>
				<td><b>Causa</b></td>
				<td><b>Valor Estimado</b></td>
				<td><b>Valor Presentado por Pagar</b></td>
				<td><b>Valor Autorizado por Pagar</b></td>
			</tr>
			";
		
		foreach($assistance_info as $card){
			$financial_info_payable = File::getAssistantFinancialInformationByFileandProvider($db, $card['serial_fle'], $card['serial_spv']);
			$estimated_amount = File::getEstimatedAmountByCard ($db, $card['serial_fle'],$card['serial_spv']);
			
			if(!$financial_info_payable){
				$financial_info_payable['authorized_amount'] = number_format(0, 2);
				$financial_info_payable['presented_amount'] = number_format(0, 2);
			}
                        
			$table .= "<tr>
                                        <td>{$card['name_cou']}</td>
                                        <td>{$card['asistance_country']}</td>
                                        <td>{$card['assistance_city']}</td>
                                        <td>{$card['name_cus']}</td>
                                        <td>{$card['birthdate_cus']}</td>	
                                        <td>{$card['card_number_sal']}</td>
                                        <td>{$card['begin_date']}</td>
                                        <td>{$card['end_date']}</td>
                                        <td>{$card['name_pbl']}</td>
                                        <td>{$card['serial_fle']}</td>
                                        <td>{$financial_info_payable['provider']}</td>
                                        <td>{$card['incident_date']}</td>
                                        <td>{$global_fileStatus[$card['status_fle']]}</td>
                                        <td>{$card['type_fle']}</td>
                                        <td>{$card['diagnosis_fle']}</td>
                                        <td>{$card['cause_fle']}</td>
                                        <td>{$estimated_amount['estimated_amount']}</td>
                                        <td>{$financial_info_payable['presented']}</td>
                                        <td>{$financial_info_payable['authorized']}</td>
                                </tr>"; 

		}
		$table .="  </table>";		
	}else{
		$table = "<br /> <b> No existe informaci&oacute;n para los filtros seleccionados</b>";
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Casos.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo $title;
	echo $parameters;	
	echo $table;
?>
