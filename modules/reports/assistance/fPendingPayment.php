<?php

/*
  Document   : fPendingPayment
  Created on : 23/08/2011, 12:18:40 PM
  Author     : Nicolas Flores
 */

$payRequest = new PaymentRequest($db);
$payTypes = $payRequest->getPaymentRequestTypes($db);
$docByFile = new DocumentsByFile($db);
$docsTo = $docByFile->getDocumentToList($db);

$smarty->register('payTypes,docsTo');
$smarty->display();
?>
