<?php

/*
  File: passistanceAlertReportPDF
  Author: Santiago Arellano
  Creation Date: 06/07/2011
  Last Modified:
  Modified By:
 */
//die(Debug::print_r($_POST));

Request::setInteger('assistanceGroup');
Request::setInteger('alertOwner');
Request::setString('radioChoice');
Request::setString('radioChoiceDate');
Request::setString('from');
Request::setString('to');

include_once DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';

//HEADER
$pdf->ezText('<b>' . utf8_decode("ALERTAS DE ASISTENCIAS") . '</b>', 12, array('justification' => 'center'));
$pdf->ezSetDy(-10);


$alertsList = Alert::getAssistanceAlertByUsersORGroup($db, $assistanceGroup, $alertOwner,
				$radioChoice, $radioChoiceDate, $from, $to);

if (is_array($alertsList)) {
	foreach ($alertsList as &$al) {
		$al['col4'] = $global_alertStatus[$al['col4']];
	}
}

	if ($assistanceGroup >= 1) {
		$assistanceGroupObj = new AssistanceGroup($db, $assistanceGroup);
		$assistanceGroupObj->getData();
		$assistanceGroupName = $assistanceGroupObj->getName_asg();

		$pdf->ezText('<b>' . utf8_decode("Grupo de Asistencia: ") . '</b>' . $assistanceGroupName, 10, array('justification' => 'left'));
	} else
	if ($alertOwner >= 1) {
		$user = new User($db, $alertOwner);
		$user->getData();
		$alertOwnerName = $user->getFirstname_usr() . " " . $user->getLastname_usr();

		$pdf->ezText('<b>' . utf8_decode("Creador de Alerta: ") . '</b>' . $alertOwnerName, 10, array('justification' => 'left'));
	}

	$pdf->ezSetDy(-3);
	$pdf->ezText('<b>' . utf8_decode("Fecha: ") . '</b>Del ' . $from . " hasta el " . $to, 10, array('justification' => 'left'));
	$pdf->ezSetDy(-10);

	if (is_array($alertsList)) {
		$titles = array(
			'in_date_alt' => utf8_decode('<b>Fecha de Creación</b>'),
			'card_number_sal' => utf8_decode('<b># Tarjeta</b>'),
			'PAX' => utf8_decode('<b>Cliente</b>'),
			'status_alt' => utf8_decode('<b>Estado de Alerta</b>'),
			'message_alt' => utf8_decode('<b>Mensaje</b>'),
			'served_date_alt' => utf8_decode('<b>Fecha Máxima de Atención</b>'));

		$pdf->ezTable($alertsList, $titles, '',
				array('showHeadings' => 1,
					'shaded' => 1,
					'showLines' => 2,
					'xPos' => 'center',
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8,
					'fontSize' => 11,
					'titleFontSize' => 8,
					'cols' => array(
						'in_date_alt' => array('justification' => 'center', 'width' => 100),
						'card_number_sal' => array('justification' => 'center', 'width' => 80),
						'PAX' => array('justification' => 'center', 'width' => 150),
						'status_alt' => array('justification' => 'center', 'width' => 100),
						'message_alt' => array('justification' => 'center', 'width' => 100),
						'served_date_alt' => array('justification' => 'center', 'width' => 100),)));
		$pdf->ezSetDy(-20);
	} else {
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' => 'center'));
	}

	$pdf->ezStream();
?>