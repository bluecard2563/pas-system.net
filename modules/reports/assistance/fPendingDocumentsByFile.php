<?php
/**
 * File: fPendingDocumentsByFile
 * Author: Patricio Astudillo M.
 * Creation Date: 17/02/2012 
 * Last Modified: 17/02/2012
 * Modified By: Patricio Astudillo
 */

	$country_list = PendingDocumentsByFile::getCountriesWithPendingDocuments($db);
	
	$smarty->register('country_list');
	$smarty->display();
?>
