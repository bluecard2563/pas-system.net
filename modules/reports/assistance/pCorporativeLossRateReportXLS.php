<?php
/**
 * file: pCorporativeLossRateReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 27/04/2012
 * Modification Date:  27/04/2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:serial_sal');
	
	$sale_info = Sales::getSalesBasicInfo($db, $serial_sal);
	$files_info = TravelerLog::getCorporativeAssistanceInfo($db, $serial_sal);
	
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$title="<table>
				<tr><td colspan=6>".$date."</td></tr><tr></tr>
				<tr>
					<th></th>
					<th></th>
					<th><b>Reporte de Siniestralidad - Corporativos</b></th>
				</tr>
				<tr></tr>
			</table>";
	
	$card_info = "<table>
						<tr><td  align='right'><b>Cliente:</b></td><td align='left'>".$sale_info['name_cus']."</td></tr>
						<tr><td  align='right'><b>Producto:</b></td><td align='left'>".$sale_info['name_pbl']."</td></tr>
						<tr><td  align='right'><b>Fecha de Ingreso:</b></td><td align='left'>".$sale_info['in_date_sal']."</td></tr>
						<tr><td  align='right'><b>Inicio de cobertura:</b></td><td align='left'>".$sale_info['begin_date_sal']."</td></tr>
						<tr><td  align='right'><b>Fin de cobertura:</b></td><td align='left'>".$sale_info['end_date_sal']."</td></tr>
						<tr><td  align='right'><b>D&iacute;as Contratados:</b></td><td align='left'>".$sale_info['days_sal']."</td></tr>
						<tr><td  align='right'><b>Total de la Venta:</b></td><td align='left'>".$sale_info['total_sal']."</td></tr>
						<tr></tr>
					</table>";
	
	if($files_info && $sale_info){
		$table = "<table>
						<tr>
							<td><b>Pasajero</b></td>
							<td><b>No. Tarjeta</b></td>
							<td><b>Registro de Viaje</b></td>
							<td><b>Inicio de Viaje</b></td>
							<td><b>Fin de Viaje</b></td>
							<td><b>No. Expediente</b></td>
							<td><b>Estado del Expediente</b></td>
							<td><b>Fecha de la Asistencia</b></td>
							<td><b>Diagn&oacute;stico</b></td>
							<td><b>Monto Estimado</b></td>
							<td><b>Monto Autorizado</b></td>
							<td><b>Monto Presentado</b></td>
						</tr>";
		
		foreach($files_info as $f){
			$table .="<tr>
							<td>{$f['name_cus']}</td>
							<td>{$f['card_number']}</td>
							<td>{$f['in_date_trl']}</td>
							<td>{$f['start_trl']}</td>
							<td>{$f['end_trl']}</td>
							<td>{$f['serial_fle']}</td>
							<td>{$global_fileStatus[$f['status_fle']]}</td>
							<td>{$f['creation_date_fle']}</td>
							<td>{$f['diagnosis_fle']}</td>
							<td>{$f['estimated_amount_spf']}</td>
							<td>{$f['amount_authorized_bbd']}</td>
							<td>{$f['amount_presented_bbd']}</td>
						</tr>";
		}
		
		$table .="</table>";
	}else{
		$table = "No existe informaci&oacute;n de asistencias para la tarjeta seleccionada.";
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Siniestralidad_Corporativas.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
			
	echo $title.'<br>';
	echo $card_info.'<br>';
	echo $table;
?>
