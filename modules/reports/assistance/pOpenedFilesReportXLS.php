<?php
/**
 * File:  pOpenedFilesReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 03-ago-2011, 11:05:53
 * Description:
  * Last Modified: 16-ago-2011, 10:00:00
 * Modified by: Nicolas Flores
 */

	Request::setInteger('selGroup');
	Request::setString('today');
	Request::setString('txtDateFilter');
	
	if(!$txtDateFilter){
		$txtDateFilter = $today;
	}
	
	$files_list = AssistanceGroupsByUsers::getOpenedFilesByDate($db, $selGroup, $txtDateFilter);
	
	if($files_list){
		$table = "<table border=1>
				<tr>
					<td><b>Hora de Apertura</b></td>
					<td><b>No. Tarjeta</b></td>
					<td><b>Expediente</b></td>
					<td><b>Motivo de la Llamada</b></td>
					<td><b>Estado</b></td>
					<td><b>Observaciones</b></td>
					<td><b>Operador </b></td>
					<td><b>Valor Estimado de Asistencia</b></td>
				</tr>
				";
		
		foreach($files_list as $a){
			$a['status_fle'] = $global_fileStatus[$a['status_fle']];
			
			$table .= "<tr>
						<td>{$a['creation_hour']}</td>
						<td>{$a['card_number_sal']}</td>
						<td>{$a['serial_fle']}</td>
						<td>{$a['diagnosis_fle']}</td>
						<td>{$a['status_fle']}</td>
						<td>{$a['cause_fle']}</td>
						<td>{$a['opening_usr']}</td>
						<td>{$a['estimated_amount']}</td>
					</tr>";
		}

		$table .="  </table>";		
	}else{
		$table = "<br>No existen datos para los filtros seleccionados.";
	}
	
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table border='0'>
				<tr><td colspan=6>$date</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Apertura de Expedientes</b></td></tr>
				 <tr></tr>
			</table><br />";
	
	//****************************************************** FILTER SECTION
	$name_asg = new AssistanceGroup($db, $selGroup);
	$name_asg -> getData();
	$name_asg = $name_asg -> getName_asg();	
	
	$parameters = "<table border='1'>
					<tr>
						<td align='right'><b>Grupo de Asistencias: </b></td><td align='left'>" . $name_asg . "</td>
						<td align='right'><b>Fecha:</b></td><td align='left'>" . $txtDateFilter . "</td>
					</tr>	
				</table><br />";
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Apertura_Casos.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $parameters;	
	echo $table;	
?>
