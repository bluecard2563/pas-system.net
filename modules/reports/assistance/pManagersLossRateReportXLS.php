<?php
/**
 * File:  pManagersLossRateReportXLS
 * Author: Patricio Astudillo
 * Creation Date: 02-ago-2011, 10:25:34
 * Description: Prepare and display an XLS report where all the information of assistances are made
 *				ordered by product and according to a set of parameters sent by the user.
 * Last Modified: 02-ago-2011, 10:25:34
 * Modified by:
 */

	Request::setInteger('selZone');
	Request::setInteger('selCountry');
	Request::setInteger('selManager');
	Request::setInteger('selCity');
	Request::setInteger('selDealer');
	Request::setInteger('selBranch');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	Request::setString('rdDateFilter');
	
	//************************************************* GATHERING THE INFORMATION
	$total_sold_by_product = Sales::totalSoldByProductByDates($db, $txtBeginDate, $txtEndDate, $rdDateFilter, $selCountry, $selManager, $selDealer, $selBranch, $selCity);
	//Para eliminar tarjetas anuladas
        //$total_void_by_product = Sales::getInfoForLossReport($db, $txtBeginDate, $txtEndDate, $rdDateFilter, $selCountry, $selManager, $selDealer, $selBranch, $selCity);
        if($total_sold_by_product){
		$table = "<table border=1>
				<tr>
					<td><b>Producto</b></td>
					<td><b>Ventas</b></td>
                                        <td><b>COSTO</b></td>
					<td><b>Reservas</b></td>
					<td><b>Fees Pendientes</b></td>					
					<td><b>Fees Pagados</b></td>
					<td><b>Coordinaci&oacute;n Pendientes</b></td>
					<td><b>Coordinaci&oacute;n Pagadas</b></td>
					<td><b>Auditor&iacute;a Pendientes</b></td>
					<td><b>Auditor&iacute;a Pagados</b></td>
					<td><b>Repricing Pendientes</b></td>
					<td><b>Repricing Pagados</b></td>
					<td><b>Nota de Cr&eacute;dito Pendiente</b></td>
					<td><b>Nota de Cr&eacute;dito Pagado</b></td>
					<td><b>TOTAL Siniestros</b></td>
					<td><b>TOTAL Siniestros con Reserva</b></td>
					<td><b>Siniestralidad sobre Venta</b></td>
                                        <td><b>Siniestralidad con Reserva</b></td>
				</tr>
				";
		
		foreach($total_sold_by_product as $a){
			$row_total_amount =  $a['paid_fees']+$a['paid_management']+$a['paid_repricing']+$a['paid_auditory']-$a['paid_credit_note']+$a['unpaid_fees']+$a['unpaid_management']+$a['unpaid_auditory']+$a['unpaid_repricing']-$a['unpaid_credit_note'] ;
                        if ($a['cost_sold']== 0){
                            $a['cost_sold'] = 1;
                        }
                        if ($a['total_sold']== 0){
                            $a['total_sold'] = 1;
                        }
			$loss_under_cost = (($row_total_amount * 100) / $a['cost_sold']);
			$loss_under_sale = (($row_total_amount * 100) / $a['total_sold']);
			
			/* TOTAL SECTION */
			$general_total_sold += $a['total_sold'];
			$general_total_reserved += $a['estimated_amount'];
                        $general_loss_total_amount += $row_total_amount;
			$general_total_cost += $a['cost_sold'];
                        $row_total_amount_reserve = $row_total_amount + $a['estimated_amount'];
                        $loss_under_reserve = (($row_total_amount_reserve * 100) / $a['total_sold']);
                        $general_loss_reserved_total_amount += $row_total_amount_reserve;
                        
			/* TOTAL SECTION */
			
			$table .= "<tr>
						<td>{$a['name_pbl']}</td>
						<td>{$a['total_sold']}</td>
                                                <td>{$a['cost_sold']}</td>
						<td>{$a['estimated_amount']}</td>
						<td>{$a['unpaid_fees']}</td>
						<td>{$a['paid_fees']}</td>
						<td>{$a['unpaid_management']}</td>
						<td>{$a['paid_management']}</td>
						<td>{$a['unpaid_auditory']}</td>
						<td>{$a['paid_auditory']}</td>
						<td>{$a['unpaid_repricing']}</td>
						<td>{$a['paid_repricing']}</td>
						<td>{$a['unpaid_credit_note']}</td>
						<td>{$a['paid_credit_note']}</td>
						<td>".number_format($row_total_amount, 2, '.', '')."</td>
						<td>".number_format($row_total_amount_reserve, 2, '.', '')."</td>
						<td>".number_format($loss_under_sale, 2, '.', '')." %</td>
                                                <td>".number_format($loss_under_reserve, 2, '.', '')." %</td>    
					</tr>";
		}	
		
		/* TOTAL SECTION */
		$general_loss_rate = ($general_loss_total_amount * 100) / $general_total_sold;
                $generalcost_loss_rate = ($general_total_cost * 100) / $general_total_sold;
                $general_loss_rate_reserved = ($general_loss_reserved_total_amount * 100) / $general_total_sold;
		$table .= "<tr>
						<td><b>TOTAL</b></td>
						<td>".number_format($general_total_sold, 2, '.', '')."</td>
                                                <td>".number_format($general_total_cost, 2, '.', '')."</td>    
						<td>".number_format($general_total_reserved, 2, '.', '')."</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>".number_format($general_loss_total_amount, 2, '.', '')."</td>
                                                <td>".number_format($general_loss_reserved_total_amount, 2, '.', '')."</td>
						
                                                <td>".number_format($general_loss_rate, 2, '.', '')." %</td>
                                                <td>".number_format($general_loss_rate_reserved, 2, '.', '')." %</td>
					</tr>";
		/* TOTAL SECTION */

		$table .="  </table>";
	}else{
		$table = "<br>No existen datos para los filtros seleccionados.";
	}
	
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table border='0'>
				<tr><td colspan=6>$date</td></tr>
				<tr></tr>
				<tr><td colspan=6 align='center'><b>Reporte de Siniestralidad</b></td></tr>
				 <tr></tr>
			</table><br />";
	
	//****************************************************** FILTER SECTION
	$name_zon = new Zone($db, $selZone);
	$name_zon -> getData();
	$name_zon = $name_zon -> getName_zon();
	
	if($selCountry){
		$name_cou = new Country($db, $selCountry);
		$name_cou -> getData();
		$name_cou = $name_cou -> getName_cou();
	}else{
		$name_cou = "Todos";
	}
	
	if($selManager){
		$name_man = ManagerbyCountry::getManagerByCountryName($db, $selManager);
	}else{
		$name_man = "Todos";
	}
	
	if($selCity){
		$name_cit = new City($db, $selCity);
		$name_cit -> getData();
		$name_cit = $name_cit -> getName_cit();
	}else{
		$name_cit = "Todos";
	}
	
	if($selDealer){
		$name_dea = new Dealer($db, $selDealer);
		$name_dea -> getData();
		$name_dea = $name_dea -> getName_dea().' - '.$name_dea ->getCode_dea();
	}else{
		$name_dea = "Todos";
	}
	
	if($selBranch){
		$name_bra = new Dealer($db, $selBranch);
		$name_bra -> getData();
		$name_bra = $name_bra -> getName_dea().' - '.$name_bra ->getCode_dea();
	}else{
		$name_bra = "Todos";
	}	
	
	if($rdDateFilter == 'in_date_sal'){
		$name_field = "Fecha de Ingreso al Sistema";
	}else{
		$name_field = "Fecha de Inicio de Cobertura";
	}
	
	$parameters = "<table border='1'>
					<tr>
						<td align='right'><b>Zona: </b></td><td align='left'>" . $name_zon . "</td>
						<td align='right'><b>Pa&iacute;s:</b></td><td align='left'>" . $name_cou . "</td>
					</tr>
					<tr>
						<td align='right'><b>Representante:</b></td><td align='left'>" . $name_man . "</td>
						<td align='right'><b>Ciudad:</b></td><td align='left'>" . $name_cit . "</td>
					</tr>
					<tr>
						<td align='right'><b>Comercializador:</b></td><td align='left'>" . $name_dea . "</td>
						<td align='right'><b>Sucursal:</b></td><td align='left'>" . $name_bra . "</td>
					</tr>
					<tr>
						<td align='right'><b>Fecha Desde:</b></td><td align='left'>" . $txtBeginDate . "</td>
						<td align='right'><b>Fecha Hasta:</b></td><td align='left'>" . $txtEndDate . "</td>
					</tr>
					<tr>
						<td align='right'><b>An&aacute;lisis de:</b></td><td align='left'>" . $name_field . "</td>
					</tr>				
					<tr></tr>
				</table><br />";
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Siniestralidad.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $parameters;	
	echo $table;
?>
