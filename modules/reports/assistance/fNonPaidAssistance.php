<?php
/**
 * File: fNonPaidAssistance
 * Author: Patricio Astudillo M.
 * Creation Date: 21/02/2012 
 * Last Modified: 21/02/2012
 * Modified By: Patricio Astudillo
 */

	$country_list = File::getAssistancesWithNonPaidInvoices($db);

	$smarty -> register('country_list');
	$smarty -> display();
?>
