<?php
/**
 * File: fCardFileReport
 * Author: Patricio Astudillo
 * Creation Date: 05/10/2011, 02:38:10 PM
 * Last Modified: 05/10/2011, 02:38:10 PM
 * Modified By:
 */

	$zone = new Zone($db);
	$zone_list = $zone ->getDealerZones($_SESSION['countryList']);

	$today = date('d/m/Y');
	$first_day='01/01/2009';

	$smarty -> register('zone_list,today,first_day');
	$smarty -> display();
?>
