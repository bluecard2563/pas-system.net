<?php
/*
File: pAssistanceReport
Author: David Bergmann
Creation Date: 15/12/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:serial_fle');

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

/*Data*/
$file = new File($db,$serial_fle);
$data = $file->getFileReport();
$contacts=unserialize($data['contanct_info_fle']);
/*End Data*/

$pdf->ezText(utf8_decode(html_entity_decode('<b>INFORME DE BIT&Aacute;CORA</b>')), 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$pdf->ezText(utf8_decode(html_entity_decode('<b>Informaci&oacute;n del expediente:</b>')), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$file_info[0] = array('col1'=>utf8_decode('<b># de Expediente:</b>'),
					  'col2'=>$data['serial_fle'],
					  'col3'=>utf8_decode('<b>Nombre del Cliente:</b>'),
					  'col4'=>$data['name_cus']);

$file_info[1] = array('col1'=>utf8_decode('<b># de Tarjeta:</b>'),
					  'col2'=>$data['card_number'],
					  'col3'=>utf8_decode('<b>Nombre del Operador:</b>'),
					  'col4'=>$data['name_usr']);

$file_info[2] = array('col1'=>utf8_decode('<b>Tipo de Producto:</b>'),
					  'col2'=>$data['name_pbl'],
					  'col3'=>utf8_decode('<b>Fecha de Inicio:</b>'),
					  'col4'=>$data['creation_date']);

$file_info[3] = array('col1'=>utf8_decode('<b>Estado:</b>'),
					  'col2'=>utf8_decode(html_entity_decode($global_fileStatus[$data['status_fle']])),
					  'col3'=>'',
					  'col4'=>'');

$pdf->ezTable($file_info,
			  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>100),
						'col2'=>array('justification'=>'left','width'=>120),
						'col3'=>array('justification'=>'left','width'=>100),
						'col4'=>array('justification'=>'left','width'=>120),
						)));

$pdf->ezSetDy(-20);


$medical_info[0] =array('col1'=>utf8_decode('<b>Causa:</b>'),
						'col2'=>$data['cause_fle']);

$medical_info[1] =array('col1'=>utf8_decode(html_entity_decode('<b>Diagn&oacute;stico:</b>')),
						'col2'=>$data['diagnosis_fle']);

$pdf->ezTable($medical_info,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>2,
					'xPos'=>'300',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>100),
						'col2'=>array('justification'=>'left','width'=>340)
						)));
$pdf->ezSetDy(-20);


if(is_array($contacts)){
	$pdf->ezText(utf8_decode(html_entity_decode('<b>Informaci&oacute;n de contacto:</b>')), 10, array('justification'=>'left'));
	$pdf->ezSetDy(-10);

	foreach($contacts as $key=>$c){
		$contacts_info[$key]['num']=$key+1;
		$contacts_info[$key]['name']=$contacts[$key]['name'];
		$contacts_info[$key]['phone']=$contacts[$key]['phone'];
		$contacts_info[$key]['address']=$contacts[$key]['address'];
	}

	$header = array('num'=>utf8_decode('<b>#</b>'),
					'name'=>utf8_decode('<b>Nombre y Apellido</b>'),
					'phone'=>utf8_decode(html_entity_decode('<b>Tel&eacute;fono</b>')),
					'address'=>utf8_decode(html_entity_decode('<b>Direcci&oacute;n</b>')));


	$pdf->ezTable($contacts_info, $header, '',
					array(	'showHeadings'=>1,
							'shaded'=>1,
							'showLines'=>2,
							'xPos'=>'300',
							'fontSize' => 8,
							'titleFontSize' => 8,
							'cols'=>array(
								'num'=>array('justification'=>'center','width'=>40),
								'name'=>array('justification'=>'center','width'=>110),
								'phone'=>array('justification'=>'center','width'=>110),
								'address'=>array('justification'=>'center','width'=>180))));
}

$pdf->ezSetDy(-20);

$pdf->ezText(utf8_decode(html_entity_decode('<b>Informaci&oacute;n de bit&aacute;cora:</b>')), 10, array('justification'=>'left'));
$pdf->ezSetDy(-10);

$client_blog[0] = array('col1'=>  utf8_decode(html_entity_decode('<b>Bit&aacute;cora Cliente</b>')));
$client_blog[1] = array('col1'=>$data['client_blog']);
//Debug::print_r($client_blog); die;
$pdf->ezTable($client_blog, array('col1'=>''), '',
					array(	'showHeadings'=>0,
							'shaded'=>0,
							'showLines'=>2,
							'xPos'=>'300',
							'fontSize' => 8,
							'titleFontSize' => 8,
							'splitRows' => 1,
							'cols'=>array(
								'col1'=>array('justification'=>'left','width'=>440))));

$pdf->ezSetDy(-20);

if ($_SESSION['serial_mbc'] == 1) {
    $operator_blog[0] = array('col1'=>utf8_decode(html_entity_decode('<b>Bit&aacute;cora Operador</b>')));
    $operator_blog[1] = array('col1'=>$data['operator_blog']);

    $pdf->ezTable($operator_blog, array('col1'=>''), '',
                                            array(	'showHeadings'=>0,
                                                            'shaded'=>0,
                                                            'showLines'=>2,
                                                            'xPos'=>'300',
                                                            'fontSize' => 8,
                                                            'titleFontSize' => 8,
                                                            'splitRows' => 1,
                                                            'cols'=>array(
                                                                    'col1'=>array('justification'=>'left','width'=>440))));
}

$pdf->ezStream();
?>