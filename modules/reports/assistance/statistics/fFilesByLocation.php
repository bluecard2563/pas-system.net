<?php
/**
 * File: fFilesByLocation
 * Author: Patricio Astudillo
 * Creation Date: 22-oct-2012
 * Last Modified: 22-oct-2012
 * Modified By: Patricio Astudillo
 */
	
	$countries = Country::getCountriesWithFiles($db);
	$today = date('d/m/Y');
	
	$smarty->register('countries,today');
	$smarty->display();

?>
