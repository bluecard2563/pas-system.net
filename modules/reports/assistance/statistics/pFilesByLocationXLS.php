<?php
/**
 * File: pFilesByLocationXLS
 * Author: Patricio Astudillo
 * Creation Date: 22-oct-2012
 * Last Modified: 22-oct-2012
 * Modified By: Patricio Astudillo
 */
	
	Request::setInteger('selCountry');
	Request::setString('rdDetailed');
	Request::setString('txtBeginDate');
	Request::setString('txtEndDate');
	($rdDetailed == 'YES') ? $detailed = TRUE : $detailed = FALSE;
	
	$files = File::getFilesByCountryStatistics($db, $selCountry, $txtBeginDate, $txtEndDate, $detailed);
	
	
	//*****************************************************  HEADER SECTION
	$date = html_entity_decode($_SESSION['cityForReports'] . ', ' . date('d') . ' de ' . $global_weekMonths[intval(date('m'))] . ' de ' . date('Y') . ' a las ' . date("G:i:s a"));
	$title = "<table>
				<tr>
					<td colspan='9'><b>REPORTE DE CASOS POR UBICACI&Oacute;N</b></td>
				</tr>
				<tr>
					<td colspan='9'>$date</td>
				</tr>
				<tr>&nbsp;</tr>
				<tr>&nbsp;</tr>
				<tr>
					<td><b>No. Expedientes: </b></td>
					<td align='left' colspan='8'>%%FILES_NUM%%</td>
				</tr>";
	
	if($txtBeginDate):
		$title .= "	<tr>
						<td><b>Asistencias creadas desde: </b></td>
						<td align='left' colspan='8'>$txtBeginDate</td>
					</tr>";
	endif;
	
	if($txtEndDate):
		$title .= "	<tr>
						<td><b>Asistencias creadas hasta: </b></td>
						<td align='left' colspan='8'>$txtEndDate</td>
					</tr>";
	endif;
	
	if ($selCountry) {
		$country = new Country($db, $selCountry);
		$country->getData();
	
		$title .= "<tr>
						<td><b>Pa&iacute;s: </b></td>
						<td colspan='8'>{$country->getName_cou()}</td>
					</tr>";
	} 
	$title .= "</table><br /><br />";
	
	if($files){
		if($detailed){
			$table = "<table>
					<tr>
						<td>No. Expediente</td>
						<td>No. Tarjeta</td>
						<td>Operador</td>
						<td>Paciente</td>
						<td>Diagn&oacute;stico</td>
						<td>Estado</td>
						<td>Tipo</td>
						<td>Fecha de Ocurrencia</td>
						<td>Ciudad</td>
						<td>Pa&iacute;s</td>
					</tr>";
		}else{
			$table = "<table>
					<tr>
						<td><b>Pa&iacute;s</b></td>
						<td><b>Ciudad</b></td>
						<td><b>Expedientes Creados</b></td>
						<td><b>% Pa&iacute;s</b></td>
						<td><b>% Consolidado</b></td>
					</tr>";
		}
		
		if(!$detailed){
			$country_total = array();
			$general_total = 0;
			$current_country = 0;
			
			foreach($files as $fsum){
				if($current_country != $fsum['serial_cou']){
					$country_total[$fsum['serial_cou']] = 0;
					$current_country = $fsum['serial_cou'];
				}
				
				$country_total[$fsum['serial_cou']] += $fsum['files_per_city'];
				$general_total += $fsum['files_per_city'];
			}
			
			//Debug::print_r($country_total);
		}
		
		foreach($files as $f){
			if($detailed){
				$table .= " <tr>
								<td>{$f['serial_fle']}</td>
								<td>{$f['card_number_sal']}</td>
								<td>{$f['opening_usr']}</td>
								<td>{$f['name_pax']}</td>
								<td>{$f['diagnosis_fle']}</td>
								<td>{$global_fileStatus[$f['status_fle']]}</td>
								<td>{$global_fileTypes[$f['type_fle']]}</td>
								<td>{$f['incident_date_fle']}</td>
								<td>{$f['name_cit']}</td>
								<td>{$f['name_cou']}</td>
							</tr>";
			}else{
				$table .= " <tr>
								<td>{$f['name_cou']}</td>
								<td>{$f['name_cit']}</td>
								<td>{$f['files_per_city']}</td>
								<td>".number_format(($f['files_per_city']/$country_total[$f['serial_cou']]) * 100, 2)."%</td>
								<td>".number_format(($f['files_per_city']/$general_total) * 100, 2)."%</td>
							</tr>";
			}
		}
		
		$table .= " </table>";
		
		$title = str_replace('%%FILES_NUM%%', $general_total, $title);
	}else{
		$table = "<h3>No existen registros para los filtros seleccionados</h3>";
		$title = str_replace('%%FILES_NUM%%', 'N/A', $title);
	}

	header("Expires: 0");
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=CasosXUbicacion_".date('m_d_Y').".xls");
	header("Pragma: no-cache");
	header("Cache-control: private");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Description: File Transfer");
	
	echo $title;
	echo $table;

?>
