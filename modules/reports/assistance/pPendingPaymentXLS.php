<?php

/*
  Document   : pPendingPaymentXLS
  Created on : 23/08/2011, 12:21:36 PM
  Author     : Nicolas Flores
 */
Request::setString('selPayTo,selType,txtBeginDate,txtEndDate');

$paymentRequest = new PaymentRequest($db);
$pendingPayments=$paymentRequest->getPendingPayments($selPayTo,$selType,$txtBeginDate,$txtEndDate);

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Pagos_Pendientes.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

?>