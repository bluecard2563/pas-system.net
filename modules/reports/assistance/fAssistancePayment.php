<?php

/*
  Document   : fAssistancePayment
  Created on : 23/08/2011, 12:18:40 PM
  Author     : Luis Salvador
 */

$docByFile = new DocumentsByFile($db);
$docsTo = $docByFile->getDocumentToList($db);

$provider = new ServiceProvider($db);
$providerList = $provider->getAllServiceProviders($db);

$zoneList = PaymentRequest::getZonesWithClaims($db, 'CLIENT');
$today = date('d/m/Y');

$smarty->register('zoneList,docsTo,providerList,today');
$smarty->display();
?>
