<?php
/*
  Document   : pAssistancePaymentPDF
  Created on : 28/09/2011, 11:21:36 PM
  Author     : Luis Salvador
 */

	Request::setString('selPayTo,txtBeginDate,txtEndDate,selProvider,selCountry,selCity,selectedBoxes,selProcessAs');
	
	//************************** FILTERS SECTION ********************************
	global $date;
	$date = html_entity_decode($date['day'] . ' de ' . $global_weekMonths[$date['month']] . ' de ' . $date['year'] . ' a las ' . date("G:i:s a"));
	$selected_claims = NULL;
	$pay_to = $global_billTo[$selPayTo];
	
	switch($selPayTo){
		case 'CLIENT':
			$name_cou = new Country($db, $selCountry);
			$name_cou ->getData();
			$name_cou = $name_cou ->getName_cou();

			if($selCity){
				$name_cit = new City($db, $selCity);
				$name_cit -> getData();
				$name_cit = $name_cit->getName_cit();
			}else{
				$name_cit = 'Todas';
			}
	
			break;
		case 'PROVIDER':
			$provider = new ServiceProvider($db, $selProvider);
			$provider->getData();
			$name_provider = $provider->getName_spv();
			break;
		
		default:
			break;
	}	
	
	if(is_array($_POST['chkClaims'])) $selected_claims = implode(',', $_POST['chkClaims']);
	if($selectedBoxes != '') $selected_claims = $selectedBoxes; 

	$claims_list = PaymentRequest::getClaimsForReport($db, $selPayTo, $txtBeginDate, $txtEndDate, $selProcessAs, $selProvider, $selCity, $selected_claims);

	include_once DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';

	$pdf->ezText('<b>' . ("REPORTE DE PAGOS DE ASISTENCIA") . '</b>', 12, array('justification' => 'center'));
	$pdf->ezText($date, 9, array('justification' => 'center'));
	$pdf->ezSetDy(-10);

	$pdf->ezText('<b>Pago a: </b>' . $pay_to, 10, array('justification' => 'left'));
	$pdf->ezText('<b>Pagos Desde: </b>' . $txtBeginDate, 10, array('justification' => 'left'));
	$pdf->ezText('<b>Pagos Hasta: </b>' . $txtEndDate, 10, array('justification' => 'left'));
	
	if ($selPayTo == 'PROVIDER') {
		$pdf->ezText('<b>Coordinador: </b>' . $name_provider, 10, array('justification' => 'left'));
	} elseif($selPayTo == 'CLIENT') {
		$pdf->ezText(html_entity_decode('<b>Pa&iacute;s: </b>') . $name_cou, 10, array('justification' => 'left'));
		$pdf->ezText('<b>Ciudad: </b>' . $name_cit, 10, array('justification' => 'left'));
	}

	if (is_array($claims_list)) {
		$total_claimed = 0;
		foreach ($claims_list as &$item) {
			$item['type_prq'] = html_entity_decode($global_payment_request_type[$item['type_prq']]);
			$item['status'] = $global_typeDocumentFile[$item['status']];
			
			$total_claimed += $item['amount_authorized_prq'];
		}
		
		$pdf->ezText('<b>Total Pagado: </b>$' . number_format($total_claimed, 2, '.', ''), 10, array('justification' => 'left'));
		$pdf->ezSetDy(-20);
		
		$titles = array('name_dbf' => utf8_decode('<b>Documento</b>'),
						'name_cou' => utf8_decode('<b>Pais</b>'),
						'serial_prq' => utf8_decode('<b>N. Documento</b>'),
						'type_prq' => utf8_decode('<b>Concepto</b>'),
						'amount_authorized_prq' => utf8_decode('<b>Valor Aprobado</b>'),
						'request_date' => utf8_decode('<b>Fecha</b>'),
						'name_cus' => utf8_decode('<b>Cliente</b>'));

		$pdf->ezTable(	$claims_list,
						$titles, 
						'<b>Listado de Documentos Asistencia</b>', 
						array(	'showHeadings' => 1,
								'shaded' => 1,
								'showLines' => 2,
								'xPos' => 'center',
								'innerLineThickness' => 0.8,
								'outerLineThickness' => 0.8,
								'fontSize' => 8,
								'titleFontSize' => 8,
								'cols' => array(
									'name_dbf' => array('justification' => 'center', 'width' => 60),
									'name_cou' => array('justification' => 'center', 'width' => 60),
									'serial_prq' => array('justification' => 'center', 'width' => 60),
									'type_prq' => array('justification' => 'center', 'width' => 70),
									'amount_authorized_prq' => array('justification' => 'center', 'width' => 60),
									'request_date' => array('justification' => 'center', 'width' => 70),
									'name_cus' => array('justification' => 'center', 'width' => 90),
									'status' => array('justification' => 'center', 'width' => 60)
									)
								)
				);
	} else {
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' => 'center'));
	}

	$pdf->ezStream();
?>
