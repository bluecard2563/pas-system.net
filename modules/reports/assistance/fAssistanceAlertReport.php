<?php

$assistanceGroup=new AssistanceGroup($db);
$assistanceGroupList=$assistanceGroup->getGroupsWithAlerts();

$ownerAlert=new Alert($db);
$ownerAlertList=$ownerAlert->getAssistanceAlertUsers($db);

$smarty->register('assistanceGroupList,ownerAlertList');
$smarty->display();
?>
