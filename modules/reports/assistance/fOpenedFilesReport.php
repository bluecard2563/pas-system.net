<?php
/**
 * File:  fOpenedFilesReport
 * Author: Patricio Astudillo
 * Creation Date: 03-ago-2011, 10:07:58
 * Description:
 * Last Modified: 03-ago-2011, 10:07:58
 * Modified by:
 */
	
	//GET THE COUNTRY OF WORK TO FLTER THE OPTIONS IN REPORT
	$serial_cou = GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
	$assistance_groups = AssistanceGroup::getFilteredGroups($db, $serial_cou);
	
	$today = date('d/m/Y');
	$first_day='01/01/2009';
	
	$smarty -> register('assistance_groups,today,first_day');
	$smarty -> display();
?>
