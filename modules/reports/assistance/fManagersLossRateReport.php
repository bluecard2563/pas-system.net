<?php
/**
 * File:  fManagersLossRateReport
 * Author: Patricio Astudillo
 * Creation Date: 02-ago-2011, 9:29:17
 * Description:
 * Last Modified: 02-ago-2011, 9:29:17
 * Modified by:
 */

	$zonesList = new Zone($db);
	$zonesList = $zonesList ->getAllowedZones($_SESSION['countryList']);
	
	$today = date('d/m/Y');
	$first_day='01/01/2009';
	
	$smarty -> register('zonesList,today,first_day');
	$smarty -> display();
?>
