<?php

/*
  File: passistanceAlertReportXLS
  Author: Valeria Andino
  Creation Date: 28/07/2016
  Last Modified:
  Modified By:
 */
//die(Debug::print_r($_POST));

Request::setInteger('assistanceGroup');
Request::setInteger('alertOwner');
Request::setString('radioChoice');
Request::setString('radioChoiceDate');
Request::setString('from');
Request::setString('to');

$alertsList = Alert::getAssistanceAlertByUsersORGroup($db, $assistanceGroup, $alertOwner,
				$radioChoice, $radioChoiceDate, $from, $to);
if ($alertsList){
$title = "<table>
				<tr>
					<td colspan='9'><b>REPORTE DE ALERTAS DEL SISTEMA</b></td>
				</tr>				
				<tr>&nbsp;</tr>
				<tr>&nbsp;</tr>
				<tr>
					<td><b>GRUPO DE ASISTENCIAS </b></td>
					<td align='left' colspan='8'>$assistanceGroup</td>
				</tr>
				<tr>
					<td><b>FECHA DESDE: </b></td>
					<td align='left' colspan='8'>$from</td>
				</tr>
				<tr>
					<td><b>FECHA HASTA:  </b></td>
					<td align='left' colspan='8'>$to</td>
				</tr><br />";

            
           $records = "<table border=1>							
						<tr>
							<td><b>Grupo de Asistencia</b></td>
							<td><b>Creado por</b></td>
							<td><b>Fecha de Creaci&oacute;n</b></td>
							<td><b># Tarjeta</b></td>
							<td><b>Cliente</b></td>
							<td><b>Estado de Alerta</b></td>
							<td><b>Revisado por</b></td>
							<td><b>Mensaje</b></td>
							<td><b>Fecha M&aacute;xima de Atenci&oacute;n</b></td>
						</tr>";                                                            
         foreach ($alertsList as &$item) {
         $records .= "<tr>
						<td>{$item['name_asg']}</td>
						<td>{$item['requester']}</td>
						<td>{$item['in_date_alt']}</td>
						<td>{$item['card_number_sal']}</td>
						<td>{$item['PAX']}</td>
						<td>{$global_alertStatus[$item['status_alt']]}</td>
						<td>{$item['solver']}</td>
						<td>{$item['message_alt']}</td>
						<td>{$item['served_date_alt']}</td>
					</tr>";
         }
         $record .= "</table>";
    }else {
		$records = 'No existen datos para los filtros seleccionados.';
		$total_claimed = "";
	}
    
    header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Alertas.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;    
	echo $records;
?>