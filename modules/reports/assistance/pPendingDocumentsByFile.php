<?php
/**
 * File: pPendingDocumentsByFile
 * Author: Patricio Astudillo M.
 * Creation Date: 17/02/2012 
 * Last Modified: 17/02/2012
 * Modified By: Patricio Astudillo
 */

	Request::setString('selCountry');
	
	$country = new Country($db, $selCountry);
	$country ->getData();
	
	$documents_list = PendingDocumentsByFile::getPendingDocumentsByCountry($db, $country ->getSerial_cou());
	
	/*********************************** DISPLAYING THE XLS *****************************************/
	$date = html_entity_decode($_SESSION['cityForReports'].', '.date('d').' de '.$global_weekMonths[intval(date('m'))].' de '.date('Y').' a las '.date("G:i:s a") );
	$name_cou = $country ->getName_cou()?$country ->getName_cou():"Todos";
	$title = "<table>
					<tr><td colspan=6>$date</td></tr>
					<tr></tr>
					<tr>
						<td colspan=6><b>Reporte de Documentos Pendientes por Recibir</b></td>
					</tr>
			  </table><br />
			  <table>
					<tr>
						<td><b>Pa&iacute;s:</b></td>
						<td>$name_cou</td>
					</tr>
				</table><br />";
	
	
	if($documents_list){
		$table = "<table border='1'>
					<tr>
						<td colspan='11' style='text-align: center;'><b>DETALLE DE DOCUMENTOS</b></td>
					</tr>
					<tr>
						<td><b>No. Tarjeta</b></td>
						<td><b>No. Expediente</b></td>
						<td><b>Cliente</b></td>
						<td><b>Diagn&oacute;stico</b></td>
						<td><b>Causa</b></td>
						<td><b>Inicio del Viaje</b></td>
						<td><b>Fin del Viaje</b></td>
						<td><b>Inicio de Asistencia</b></td>
						<td><b>D&iacute;as Transcurridos</b></td>
						<td><b>Emisor del Documento</b></td>
						<td><b>Documentos Pendientes</b></td>
					</tr>";		
		
		foreach($documents_list as $doc){
			$table .= "	<tr>
							<td>{$doc['card_number_sal']}</td>
							<td>{$doc['serial_fle']}</td>
							<td>{$doc['name_cus']}</td>
							<td>{$doc['diagnosis_fle']}</td>
							<td>{$doc['cause_fle']}</td>
							<td>{$doc['begin_date_sal']}</td>
							<td>{$doc['end_date_sal']}</td>
							<td>{$doc['creation_date_fle']}</td>
							<td>{$doc['days_elapsed']}</td>
							<td>{$doc['sender_name']}</td>
							<td>{$doc['pending_documents']}</td>
						</tr>";
		}
		$table .= "</table><br />";
	}else{
		$table = '<i>No existen registros para los par&aacute;metros seleccionados.</i>';
	}
	
	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_DocumentosPendientes.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo $title;
	echo $headers;
	echo $table;	
?>
