<?php
/*
File:pInvoiceReportPDF.php
Author: Santiago Borja
Creation Date: 10/06/2010
*/

	

	Request::setString('selZone');
	Request::setString('selCountry');
	Request::setString('selCity');
	Request::setString('selManager');
	Request::setString('selResponsable');
	Request::setString('selDealer');
	Request::setString('selBranch');
	Request::setString('selAmount');
	Request::setString('txtAmount');
	Request::setString('txtStartDate');
	Request::setString('txtEndDate');
	global $global_invoice_status;
	$invoices = Invoice::getInvoicesReport($db,$selCountry, $selManager,$selDealer,$selBranch,$selResponsable,$_POST['selType'],$txtStartDate,$txtEndDate,$selAmount,$txtAmount);
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

	/*PARAMS FOR CREATING PDF FILE*/
	$header = array('number_inv' => '<b># Documento</b>',
	'fecha_vencimiento' => '<b>Fecha de vencimiento</b>',
					'name_doc' => '<b>Documento</b>',
					'card_number_sal' => '<b># Tarjeta</b>',
					'qty' => '<b>Cant.</b>',
					'date_inv' => html_entity_decode('<b>Fec. Emisi&oacute;n</b>'),
					'name_cus' => '<b>Cliente</b>',
					'facturado_a' => '<b>Facturado a</b>',
					'total_inv' => '<b>Valor</b>');
	$params = array('showHeadings'=>1,
						'shaded'=>1,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'number_inv'=>array('justification'=>'center','width'=>70),
							'fecha_vencimiento'=>array('justification'=>'center','width'=>70),
							'name_doc'=>array('justification'=>'center','width'=>80),
							'card_number_sal'=>array('justification'=>'center','width'=>50),
							'qty'=>array('justification'=>'center','width'=>40),
							'date_inv'=>array('justification'=>'center','width'=>60),
							'name_cus'=>array('justification'=>'center','width'=>80),
							'facturado_a'=>array('justification'=>'center','width'=>80),
							'total_inv'=>array('justification'=>'center','width'=>40)));
	
	//HEADER
	$pdf->ezText('<b>REPORTE DE FACTURAS</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
	
	//HEADER FILTERS:
	$headerDesc=array();
	
	$country = new Country($db, $selCountry);
	$country -> getData();
	$descLine1 = array('col1'=>html_entity_decode('<b>Pa&iacute;s:</b>'),
					 'col2'=> utf8_decode($country -> getName_cou()));
	array_push($headerDesc,$descLine1);

	$mbc=new ManagerbyCountry($db, $selManager);
	$mbc->getData();
	$manager = new Manager($db, $mbc->getSerial_man());
	$manager -> getData();
	$descLine2 = array('col1'=>utf8_decode('<b>Representante</b>'),
					 'col2'=>utf8_decode($manager -> getName_man()));
	array_push($headerDesc,$descLine2);
	
	if($selResponsable){
		$responsible = new User($db, $selResponsable);
		$responsible -> getData();
		$descLine3 = array('col1'=>utf8_decode('<b>Responsable</b>'),
					 'col2'=>utf8_decode($responsible -> getFirstname_usr().' '.$responsible -> getLastname_usr()));
		array_push($headerDesc,$descLine3);
	}else{
		$header['name_rep'] = '<b>Responsable</b>';
		$params['cols']['name_rep'] = array('justification'=>'center','width'=>80);
	}
	if($selDealer){
		$dealer = new Dealer($db, $selDealer);
		$dealer -> getData();
		$descLine4 = array('col1'=>utf8_decode('<b>Comercializador</b>'),
						   'col2'=>$dealer -> getName_dea());
		array_push($headerDesc,$descLine4);
	}
	if($selBranch){
		$branch = new Dealer($db, $selBranch);
		$branch -> getData();
		$descLine5 = array('col1'=>utf8_decode('<b>Sucursal</b>'),
					 'col2'=>$branch -> getName_dea());
		array_push($headerDesc,$descLine5);
	}else{
		$header['name_bra'] = '<b>Sucursal</b>';
		$params['cols']['name_bra'] = array('justification'=>'center','width'=>60);
	}
	if($selType){
		$descLine6 = array('col1'=>utf8_decode('<b>Estado</b>'),
					 'col2'=>$global_invoice_status[$selType]);
		array_push($headerDesc,$descLine6);
	}else{
		$header['status_inv'] = '<b>Estado</b>';
		$params['cols']['status_inv'] = array('justification'=>'center','width'=>80);
	}
	
	$header['counter_inv'] = '<b>Counter</b>';
	$params['cols']['counter_inv'] = array('justification'=>'center','width'=>80);
	
	if($selAmount){
		$descLine7 = array('col1'=>utf8_decode('<b>Valor</b>'),
					 'col2'=>$selAmount.' '.$txtAmount);
		array_push($headerDesc,$descLine7);
	}
	
	$descLine8 = array('col1'=>utf8_decode('<b>Fecha de Inicio</b>'),
					 'col2'=>$txtStartDate);
	array_push($headerDesc,$descLine8);
	
	$descLine9 = array('col1'=>utf8_decode('<b>Fecha Fin</b>'),
					 'col2'=>$txtEndDate);
	array_push($headerDesc,$descLine9);

	$total = 0;
	if(is_array($invoices)) {
		foreach($invoices as &$t){
			$total += $t['total_inv'];
			
			$t['qty'] = count(explode(',', $t['card_number_sal']));
		}
		$descLine10 = array('col1'=>utf8_decode('<b>Total</b>'),
						   'col2'=>'$'.$total);
		array_push($headerDesc,$descLine10);
	}

	//BODY:
	$pdf->ezTable($headerDesc,
			  array('col1'=>'','col2'=>''),
			  '',
			  array('showHeadings'=>0,
					'shaded'=>0,
					'showLines'=>0,
					'xPos'=>'200',
					'fontSize' => 9,
					'titleFontSize' => 11,
					'cols'=>array(
						'col1'=>array('justification'=>'left','width'=>120),
						'col2'=>array('justification'=>'left','width'=>150))));
	$pdf->ezSetDy(-10);
	//END HEADERS
	if(is_array($invoices)) {
		foreach($invoices as $key=>&$i){
			$i['status_inv']=$global_invoice_status[$i['status_inv']];
		}
	}
	
	if(is_array($invoices)){
		/*END PARAMS*/
		$pdf->ezTable($invoices,$header,$tableTitle,$params);
		$pdf->ezSetDy(-10);
	}else{
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}
	
	$pdf->ezStream();
?>