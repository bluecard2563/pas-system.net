<?php
/**
 * Allows to obtain the consolidated incentives report
 * @author Santi Albuja
 * @date 9/Jun/2021
 * @var string $db
 * @var string $txtDateFrom
 * @var strin $txtDateTo
 * @var int $selManager
 * @var int $selCountry
 */

set_time_limit(36000);
ini_set('memory_limit', '512M');

Request::setString('selCountry');
Request::setString('selManager');
Request::setString('txtDateFrom');
Request::setString('txtDateTo');

$table = "";
$userSession = $_SESSION['serial_usr'];
$dateFrom = DateTime::createFromFormat('d/m/Y', $txtDateFrom)->format('Y-m-d');
$dateTo = DateTime::createFromFormat('d/m/Y', $txtDateTo)->format('Y-m-d');
$daysFilter = 7;
$minSalesValue = 500;
$insurancePercentage = 0.50; // porcentaje de seguro campesino

$sales = Invoice::getConsolidatedIncentivesData($db, $selCountry, $selManager, $dateFrom, $dateTo, $daysFilter);

if (!empty($sales)) {

    foreach ($sales as $sale) {
        // $netValue = Invoice::getSaleNetValue($sale);
        $saleValue = $sale['total_sal']; // Valor de Venta
        $commissionDiscountPercentage = $sale['discount_prcg_inv']; // Porcentaje de descuento por comision
        $otherDiscountPercentage = $sale['other_dscnt_inv']; // Porcentaje de descuento por otros
        $commissionDiscount = ($saleValue * $commissionDiscountPercentage) / 100; // Valor de descuentos por comision
        $otherDiscount = ($saleValue * $otherDiscountPercentage) / 100; // Valor de otros descuentos
        $insurance = ($saleValue - $commissionDiscount - $otherDiscount) * $insurancePercentage / 100; // valor de seguro campesino
        $netValue = $saleValue - $otherDiscount - $insurance; // valor venta - valor de otros descuentos - seguro campesino

        $data[] = [
            'serial_usr' => $userSession,
            'serial_sal' => $sale["serial_sal"],
            'serial_man' => $sale["serial_man"],
            'serial_dea' => $sale["serial_dea"],
            'card_number_sal' => $sale["card_number_sal"],
            'number_inv' => $sale["number_inv"],
            'number_cn' => $sale["number_cn"],
            'name_man' => trim($sale["name_man"]),
            'name_dea' => trim($sale["name_dea"]),
            'net_value' => $netValue,
        ];
    }

    $insertTempData = Invoice::insertIncentivesTempData($db, $data);
    $managers = Invoice::getManagersTempData($db, $userSession);

    $table .= "
        <!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            </head>
        <body>
        <table border='1'>
    ";

    foreach ($managers as $manager) {
        $table .= "
            <tr>
                <td align='left' style='font-weight:bold'>" . strtoupper($manager['name_man']) . "</td>
                <td style='font-weight:bold'>" . $manager['totalmanager'] . "</td>
                <td style='font-weight:bold'>Bonos</td>
            </tr>
        ";

        $dealers = Invoice::getDealersTempData($db, $manager['serial_man'], $userSession);

        foreach ($dealers as $dealer) {
            $bonus = floor($dealer['totaldealer'] / $minSalesValue);

            $deliveryData[] = [
                'serial_usr' => $userSession,
                'serial_sal' => $dealer["serial_sal"],
                'serial_man' => $dealer["serial_man"],
                'serial_dea' => $dealer["serial_dea"],
                'name_man' => trim($dealer["name_man"]),
                'name_dea' => trim($dealer["name_dea"]),
                'total' => $dealer["totaldealer"],
                'bonus' => $bonus
            ];

            $table .= "
                <tr>
                    <td align='left'>" . $dealer['name_dea'] . "</td>
                    <td>" . $dealer['totaldealer'] . "</td>
                    <td>" . $bonus . "</td>
                </tr>
            ";
        }
    }

    // Insert delivery bonus data into incentive delivery control from incentive consolidate report data.
    Invoice::insertIncentivesDeliveryData($db, $deliveryData);

    $table .= "
        </table>  
        </body>
        </html>
    ";

} else {
    $table .= "<b>No existen registros</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteConsolidadoIncentivos.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;
$delete = Invoice::deleteIncentivesTempData($db, $userSession);
