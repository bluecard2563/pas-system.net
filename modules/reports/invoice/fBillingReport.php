<?php
/**
 * Created by PhpStorm.
 * User: Santi
 * Date: 6/11/2018
 * Time: 13:42
 */

$today = date('d/m/Y');
$serial_cou = 62;
$country = new Country($db);
$countryList = $country->getOwnCountries($serial_cou);
$managerBC = new ManagerbyCountry($db);
$managerList = $managerBC->getManagerByCountry($serial_cou, '1');

$smarty->register('today,countryList,managerList');
$smarty->display();