<?php
/**
 * Created by PhpStorm.
 * User: Santi
 * Date: 6/11/2018
 * Time: 13:42
 */

set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('selCountry');
Request::setString('selManager');
Request::setString('txtDateFrom');
Request::setString('txtDateTo');

$dateFrom = DateTime::createFromFormat('d/m/Y', $txtDateFrom)->format('Y-m-d');
$dateTo = DateTime::createFromFormat('d/m/Y', $txtDateTo)->format('Y-m-d');

$data = array();
$taxes = array();
$managerData = Manager::getManagerDataBySerial($db,$selManager);
$managerName = $managerData['name_man'];
$data = Invoice::getBillingData($db,$selCountry,$managerName, $dateFrom, $dateTo);

if (is_array($data)){
    $table.="
<!DOCTYPE html>
    <html>
        <head>
            <meta charset=\"UTF-8\">
        </head>
            <body>
                <table border='1'>
                    <tr>
                        <td  align='center'><b>Sucursal</b></td>
                        <td  align='center'><b>Ramo</b></td>
                        <td  align='center'><b>Poliza</b></td>
                        <td  align='center'><b>Responsable</b></td>
                        <td  align='center'><b>Cod.Agente</b></td>
                        <td  align='center'><b>Agente</b></td>
                        <td  align='center'><b>Producto</b></td>
                        <td  align='center'><b>Fecha Contabilidad</b></td>
                        <td  align='center'><b>Fecha Ncredito</b></td>
                        <td  align='center'><b>Fecha Emisión</b></td>
                        <td  align='center'><b>Fecha Desde</b></td>
                        <td  align='center'><b>Fecha Hasta</b></td>
                        <td  align='center'><b>Identificación</b></td>
                        <td  align='center'><b>Cliente</b></td>
                        <td  align='center'><b>Fecha Nacimiento</b></td>
                        <td  align='center'><b>Tipo Cliente</b></td>
                        <td  align='center'><b>Tipo Documento</b></td>
                        <td  align='center'><b>Serie Facturación</b></td>
                        <td  align='center'><b>#Nota de Credito</b></td>
                        <td  align='center'><b>Comisión</b></td>
                        <td  align='center'><b>Valor Prima</b></td>
                        <td  align='center'><b>%SC</b></td>
                        <td  align='center'><b>Valor Impuesto</b></td>
                        <td  align='center'><b>Valor Prima sin SC</b></td>
                        <td  align='center'><b>Tipo de Tax</b></td>
                        <td  align='center'><b>%Descuento</b></td>
                        <td  align='center'><b>%Otro Descuento</b></td>
                        <td  align='center'><b>Descuento</b></td>
                        <td  align='center'><b>Valor Factura</b></td>
                        <td  align='center'><b>#Clientes</b></td>
                        <td  align='center'><b>Counter</b></td>
                        <td  align='center'><b>DiasComprados</b></td>
                        <td  align='center'><b>RucIntermediario</b></td>
                        <td  align='center'><b>Destino</b></td>
                        ";
                foreach($data as $key=>$c){
                    //Segurto Campesino Validation, we only need to get the taxes applied that have seguro campesino
                    $serializedTaxesApplied = $data[$key]['TaxesApplied'];
                    $valueWithOutTax = "";
                    if(!empty($serializedTaxesApplied)){
                        $taxes = unserialize($serializedTaxesApplied);
                        if (!empty($taxes)){
                            foreach ($taxes as $tax){
                                $taxApplied = isset($tax['tax_name']) ? $tax['tax_name']: 'NA';
                                $taxAppliedValue = isset($tax['tax_percentage']) ? $tax['tax_percentage']: '0.00';
                            }
                        }
                        else{
                            $taxApplied = 'NA';
                            $taxAppliedValue = "0.00";
                        }
                    }
                    else{
                        $taxApplied = 'NA';
                        $taxAppliedValue = "0.00";
                    }
                    if (!empty($taxAppliedValue)){
                        if($data[$key]['Descuento'] > 0 || $data[$key]['OtroDescuento'] > 0) {
                        $valorImpuestoPrev = $data[$key]['ValorPrimaSC'] - number_format(($data[$key]['ValorPrimaSC'] * $taxAppliedValue)/100,2,".","");
                            $valueDiscount =  number_format($valorImpuestoPrev * (($data[$key]['Descuento']+ $data[$key]['OtroDescuento'])/100),2,".","");
                            $valueWithOutTax = number_format($valorImpuestoPrev - $valueDiscount,2,".","");
                            $valorImpuesto =number_format(($valueWithOutTax * $taxAppliedValue)/100,2,".","");

                        } else {
                            $valorImpuesto = number_format(($data[$key]['ValorPrimaSC'] * $taxAppliedValue)/100,2,".","");
                        //$valueWithOutTax = number_format($data[$key]['ValorPrimaSC'] - $taxAppliedValue, 2, '.', ',') ;
                        $valueWithOutTax = number_format($data[$key]['ValorPrimaSC'] - $valorImpuesto, 2,".","");
                        $valueDiscount = $data[$key]['ValorPrimaSC'] * (($data[$key]['Descuento'] + $data[$key]['OtroDescuento'])/100);
                        } 
                        
                        $valueDiscount = number_format($valueDiscount,2,".","");
                    }
                    else{
                        $valueWithOutTax = "0.00";
                    }
					if($data[$key]['NotaCredito']){
						$valoFactura=($valueDiscount+((-1)*$data[$key]['ValorFactura']));
						//$valorImpuesto = number_format(($valoFactura * $taxAppliedValue)/100,2,'.', ',');
                        $valorImpuestoPrev = ((-1)*$data[$key]['ValorFactura']) - number_format((((-1)*$data[$key]['ValorFactura']) * $taxAppliedValue)/100,2,".","");
                        $valueDiscountNC =  number_format($valorImpuestoPrev * (($data[$key]['Descuento']+ $data[$key]['OtroDescuento'])/100),2,".","");
                        $valueWithOutTax = number_format($valorImpuestoPrev,2,".","");
                         $valorImpuesto =number_format(($valueWithOutTax * $taxAppliedValue)/100,2,".","");
						
						 //$valueWithOutTax = number_format($valoFactura - $valorImpuesto, 2, '.', ',');
                        //$valueDiscount = $valoFactura * (($data[$key]['Descuento'] + $data[$key]['OtroDescuento'])/100);
                        //$valueDiscount = number_format($valueDiscount,2, '.', ',');
						
					}else{
						$valoFactura=$data[$key]['ValorPrimaSC'];
						$valorImpuesto=$valorImpuesto;
					}


                    $table.="
                                 <tr>
                                    <td  align='center'>".$data[$key]['Sucursal']."</td>
                                    <td  align='center'>".$data[$key]['Ramo']."</td>
                                    <td  align='center'>".$data[$key]['Poliza']."</td>
                                    <td  align='center'>".$data[$key]['Responsable']."</td>
                                    <td  align='center'>".$data[$key]['CodAgente']."</td>
                                    <td  align='center'>".$data[$key]['Agente']."</td>
                                    <td  align='center'>".$data[$key]['Producto']."</td>
                                    <td  align='center'>".$data[$key]['FechaContabilidad']."</td>
                                    <td  align='center'>".$data[$key]['FechaCN']."</td>
                                    <td  align='center'>".$data[$key]['FechaEmision']."</td>
                                    <td  align='center'>".$data[$key]['FechaVigDesde']."</td>
                                    <td  align='center'>".$data[$key]['FechaVigHasta']."</td>
                                    <td  align='center'>".$data[$key]['Documento']."</td>
                                    <td  align='center'>".$data[$key]['Cliente']."</td>
                                    <td  align='center'>".$data[$key]['FechaNacimiento']."</td>
                                    <td  align='center'>".$data[$key]['TipoCliente']."</td>
                                    <td  align='center'>".$data[$key]['TipoDocumento']."</td>
                                    <td  align='center'>".$data[$key]['SerieFactura']."</td>
                                    <td  align='center'>".$data[$key]['NotaCredito']."</td>
                                    <td  align='center'>".$data[$key]['Comision']."</td>
                                    <td  align='center'>".$valoFactura."</td>
                                    <td  align='center'>".$taxAppliedValue."</td>
                                    <td  align='center'>".$valorImpuesto."</td>
                                    <td  align='center'>".$valueWithOutTax."</td>
                                    <td  align='center'>".$taxApplied."</td>
                                    <td  align='center'>".$data[$key]['Descuento']."</td>
                                    <td  align='center'>".$data[$key]['OtroDescuento']."</td>
                                    <td  align='center'>".$valueDiscount."</td>
                                    <td  align='center'>".$data[$key]['ValorFactura']."</td>
                                    <td  align='center'>".$data[$key]['NClientes']."</td>
                                    <td  align='center'>".$data[$key]['Counter']."</td>
                                    <td  align='center'>".$data[$key]['DiasComprados']."</td>
                                    <td  align='center'>".$data[$key]['RucIntermediario']."</td>
                                    <td  align='center'>".$data[$key]['Destino']."</td>
                                   
                                         
                                   
                                </tr>";
                }
                $table.="</tr>
	        </table>
	    </body>
	</html>
	";
}
else{
    $table.="<b>No existen clientes registrados</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteFacturacion.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;
