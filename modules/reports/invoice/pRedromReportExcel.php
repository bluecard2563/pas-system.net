<?php
/**
 * Created by PhpStorm.
 * User: Lenin Padilla
 * Date: 29/01/2025
 * Time: 13:42
 */

set_time_limit(36000);
ini_set('memory_limit','512M');

Request::setString('txtDateFrom');
Request::setString('txtDateTo');

$dateFrom = DateTime::createFromFormat('d/m/Y', $txtDateFrom)->format('Y-m-d');
$dateTo = DateTime::createFromFormat('d/m/Y', $txtDateTo)->format('Y-m-d');

$data = Invoice::getRedromData($db,$dateFrom, $dateTo);

if (is_array($data)){
    $table.="
<!DOCTYPE html>
    <html>
        <head>
            <meta charset=\"UTF-8\">
        </head>
            <body>
                <table border='1'>
                    <tr>
                        <td  align='center'><b>CEDULA</b></td>
                        <td  align='center'><b>NOMBRE</b></td>
                        <td  align='center'><b>FEC_NACIMIENTO</b></td>
                        <td  align='center'><b>TELEFONO</b></td>
                        <td  align='center'><b>PÓLIZA</b></td>
                        <td  align='center'><b>FECHA</b></td>
                        <td  align='center'><b>PRODUCTO</b></td>
                        <td  align='center'><b>ESTADO</b></td>
                        
                        ";
                foreach($data as $key=>$c){

                    $table.="
                                 <tr>
                                    <td  align='center'>".$data[$key]['CEDULA']."</td>
                                    <td  align='center'>".$data[$key]['NOMBRE']."</td>
                                    <td  align='center'>".$data[$key]['FEC_NACIMIENTO']."</td>
                                    <td  align='center'>".$data[$key]['PHONE']."</td>
                                    <td  align='center'>".$data[$key]['CONTRACT']."</td>
                                    <td  align='center'>".$data[$key]['FECHA']."</td>
                                    <td  align='center'>".$data[$key]['PRODUCTO']."</td>
                                    <td  align='center'>".$data[$key]['ESTADO']."</td> 
                                </tr>";
                }
                $table.="</tr>
	        </table>
	    </body>
	</html>
	";
}
else{
    $table.="<b>No existen clientes registrados</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteREDROM.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;
