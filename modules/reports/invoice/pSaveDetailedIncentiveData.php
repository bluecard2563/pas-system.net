<?php

/**
 * Allows to obtain the data from the detailed incentives report and save it in the database.
 * @author Santi Albuja
 * @date 25/Jun/2021
 * @var int $managerId
 * @var int $countryId
 * @var string $db
 * @var string $txtDateFrom
 * @var strin $txtDateTo
 */

Request::setString('countryId');
Request::setString('managerId');
Request::setString('txtDateFrom');
Request::setString('txtDateTo');

$data = array();
$taxes = array();
$userSession = $_SESSION['serial_usr'];
$dateFrom = DateTime::createFromFormat('d/m/Y', $txtDateFrom)->format('Y-m-d');
$dateTo = DateTime::createFromFormat('d/m/Y', $txtDateTo)->format('Y-m-d');
$daysFilter = 7;

$detailedIncentives = Invoice::getDetailedIncentivesData($db, $countryId, $managerId, $dateFrom, $dateTo, $daysFilter);

if (is_array($detailedIncentives)) {

    foreach ($detailedIncentives as $key => $value) {

        // The next code is used to calculate and get the net value of the sale for each detailed incentives data.
        $serializedTaxesApplied = $detailedIncentives[$key]['TaxesApplied'];
        $netValue = "";

        if (!empty($serializedTaxesApplied)) {
            $taxes = unserialize($serializedTaxesApplied);
            if (!empty($taxes)) {
                foreach ($taxes as $tax) {
                    $taxApplied = isset($tax['tax_name']) ? $tax['tax_name'] : 'NA';
                    $taxAppliedValue = isset($tax['tax_percentage']) ? $tax['tax_percentage'] : '0.00';
                }
            } else {
                $taxApplied = 'NA';
                $taxAppliedValue = "0.00";
            }
        } else {
            $taxApplied = 'NA';
            $taxAppliedValue = "0.00";
        }

        if (!empty($taxAppliedValue)) {
            if ($detailedIncentives[$key]['Descuento'] > 0 || $detailedIncentives[$key]['OtroDescuento'] > 0) {
                $taxPrevValue = $detailedIncentives[$key]['ValorPrimaSC'] - number_format(($detailedIncentives[$key]['ValorPrimaSC'] * $taxAppliedValue) / 100, 2, '.', ',');
                $discountValue = number_format($taxPrevValue * (($detailedIncentives[$key]['Descuento'] + $detailedIncentives[$key]['OtroDescuento']) / 100), 2, '.', ',');
                $netValue = number_format($taxPrevValue - $discountValue, 2, '.', '');
            } else {
                $taxValue = number_format(($detailedIncentives[$key]['ValorPrimaSC'] * $taxAppliedValue) / 100, 2, '.', ',');
                $netValue = number_format($detailedIncentives[$key]['ValorPrimaSC'] - $taxValue, 2, '.', '');
            }
        } else {
            $netValue = "0.00";
        }

        if ($detailedIncentives[$key]['NotaCredito']) {
            $invoiceValue = ($discountValue + ((-1) * $detailedIncentives[$key]['ValorFactura']));
            $taxPrevValue = ((-1) * $detailedIncentives[$key]['ValorFactura']) - number_format((((-1) * $detailedIncentives[$key]['ValorFactura']) * $taxAppliedValue) / 100, 2, '.', ',');
            $netValue = "-" . number_format($taxPrevValue, 2, '.', '');
        } else {
            $invoiceValue = $detailedIncentives[$key]['ValorPrimaSC'];
            $taxValue = $taxValue;
        }
        // End of the calculate net value.

        // Save the necessary information in a new data array
        $incentivesDetail[] = [
            'serial_usr' => $userSession,
            'serial_sal' => $detailedIncentives[$key]['serial_sal'],
            'serial_inv' => $detailedIncentives[$key]['serial_inv'],
            'serial_cn' => $detailedIncentives[$key]['serial_cn'],
            'card_number_sal' => $detailedIncentives[$key]['Poliza'],
            'number_inv' => $detailedIncentives[$key]['Factura'],
            'number_cn' => $detailedIncentives[$key]['NotaCredito'],
            'type' => $detailedIncentives[$key]['Tipo'],
            'name_man' => trim($detailedIncentives[$key]['Sucursal']),
            'name_dea' => trim($detailedIncentives[$key]['Comercializador']),
            'name_cus' => trim($detailedIncentives[$key]['Cliente']),
            'name_pbl' => trim($detailedIncentives[$key]['Producto']),
            'date_inv' => $detailedIncentives[$key]['FechaContabilidad'],
            'date_cn' => $detailedIncentives[$key]['FechaNotaCredito'],
            'in_date_sal' => $detailedIncentives[$key]['FechaEmision'],
            'date_pay' => $detailedIncentives[$key]['FechaPago'],
            'paydays' => $detailedIncentives[$key]['Dias'],
            'net_value' => $netValue,
        ];

    }

    $response = Invoice::insertDetailedIncentivesData($db, $incentivesDetail);
    echo $response;
    exit();

}
