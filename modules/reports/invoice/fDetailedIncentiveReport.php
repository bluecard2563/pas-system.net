<?php
/**
 * Allows to obtain the new incentives report based on the general billing report query
 * @author Santi Albuja
 * @date 9/Jun/2021
 * @var string $db
 * @var FormData $smarty
 */

$today = date('d/m/Y');
$countryId = 62;
$country = new Country($db);
$countryList = $country->getOwnCountries($countryId);

$managerByCountry = new ManagerbyCountry($db);
$managerList = $managerByCountry->getManagerByCountry($countryId, 1);

$smarty->register('today,countryList,managerList');
$smarty->display();
