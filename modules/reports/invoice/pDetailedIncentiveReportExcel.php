<?php
/**
 * Allows to obtain the new incentives report based on the general billing report query
 * @author Santi Albuja
 * @date 9/Jun/2021
 * @var string $db
 * @var string $txtDateFrom
 * @var strin $txtDateTo
 * @var int $selManager
 * @var int $selCountry
 * @var string $table
 *
 */

set_time_limit(36000);
ini_set('memory_limit', '512M');

Request::setString('selCountry');
Request::setString('selManager');
Request::setString('txtDateFrom');
Request::setString('txtDateTo');

$data = array();
$taxes = array();
$dateFrom = DateTime::createFromFormat('d/m/Y', $txtDateFrom)->format('Y-m-d');
$dateTo = DateTime::createFromFormat('d/m/Y', $txtDateTo)->format('Y-m-d');
$daysFilter = 7;
$insurancePercentage = 0.50; // porcentaje de seguro campesino

$data = Invoice::getDetailedIncentivesData($db, $selCountry, $selManager, $dateFrom, $dateTo, $daysFilter);

if (is_array($data)) {
    $table .= "
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset=\"UTF-8\">
            </head>
            <body>
                <table border='1'>
                    <tr>
                        <td align='center'><b>Poliza</b></td>
                        <td align='center'><b>Factura</b></td>
                        <td align='center'><b>Nota Credito</b></td>
                        <td align='center'><b>Tipo</b></td>
                        <td align='center'><b>Sucursal</b></td>
                        <td align='center'><b>Comercializador</b></td>
                        <td align='center'><b>Cliente</b></td>
                        <td align='center'><b>Producto</b></td>
                        <td align='center'><b>Fecha Contabilidad</b></td>
                        <td align='center'><b>Fecha Nota de Credito</b></td>
                        <td align='center'><b>Fecha de Emision</b></td>
                        <td align='center'><b>Fecha Pago</b></td>
                        <td align='center'><b>Dias Pago</b></td>
                        <td align='center'><b>% Otros Descuentos</b></td>
                        <td align='center'><b>Valor de Venta</b></td>
                        <td align='center'><b>Seguro Campesino</b></td>
                        <td align='center'><b>Prima Neta</b></td>
    ";

    foreach ($data as $key => $value) {

        $saleValue = $data[$key]['ValorPrimaSC']; // Valor de Venta
        $commissionDiscountPercentage = $data[$key]['Descuento']; // Porcentaje de descuento por comision
        $otherDiscountPercentage = $data[$key]['OtroDescuento']; // Porcentaje de descuento por otros
        $commissionDiscount = ($saleValue * $commissionDiscountPercentage) / 100; // Valor de descuentos por comision
        $otherDiscount = ($saleValue * $otherDiscountPercentage) / 100; // Valor de descuento otros
        $insurance = ($saleValue - $commissionDiscount - $otherDiscount) * $insurancePercentage / 100; // valor de seguro campesino
        $netValue = $saleValue - $otherDiscount - $insurance; // valor venta - valor de otros descuentos - seguro campesino

        // Add formatting to results.
        $insurance = number_format($insurance, 2, '.', ',');
        $netValue = number_format($netValue,2,'.',',');

        // Add (-) symbol before the value only for credit note values
        if ($data[$key]['NotaCredito']) {
            $saleValue = '-' . $saleValue;
            $insurance = '-' . $insurance;
            $netValue = '-' . $netValue;
        }

        $table .= "
            <tr>
                <td align='center'>" . $data[$key]['Poliza'] . "</td>
                <td align='center'>" . $data[$key]['Factura'] . "</td>
                <td align='center'>" . $data[$key]['NotaCredito'] . "</td>
                <td align='center'>" . $data[$key]['Tipo'] . "</td>
                <td align='center'>" . $data[$key]['Sucursal'] . "</td>
                <td align='center'>" . $data[$key]['Comercializador'] . "</td>
                <td align='center'>" . $data[$key]['Cliente'] . "</td>
                <td align='center'>" . $data[$key]['Producto'] . "</td>
                <td align='center'>" . $data[$key]['FechaContabilidad'] . "</td>
                <td align='center'>" . $data[$key]['FechaNotaCredito'] . "</td>
                <td align='center'>" . $data[$key]['FechaEmision'] . "</td>
                <td align='center'>" . $data[$key]['FechaPago'] . "</td>
                <td align='center'>" . $data[$key]['Dias'] . "</td>
                <td align='center'>" . $otherDiscountPercentage . "</td>
                <td align='center'>" . $saleValue . "</td>
                <td align='center'>" . $insurance . "</td>
                <td align='center'>" . $netValue . "</td>
        
            </tr>
        ";
    }
    $table .= "
                </tr>
            </table>  
        </body>
    </html>
    ";
} else {
    $table .= "<b>No existen registros</b>";
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=ReporteDetalladoIncentivos.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo $table;
