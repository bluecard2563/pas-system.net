<?php
/*
File: pAuthorizedAlertsByUserReport
Author: David Bergmann
Creation Date: 16/02/2011
Last Modified:
Modified By:
*/

$user = new User($db, $_POST['selUser']);
$user ->getData();
$user_full_name = $user->getFirstname_usr().' '.$user->getLastname_usr();

$alert_list = Alert::getAlertHistoryForUserReport($db, $_POST['selUser'], $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selStatus']);

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

$pdf->ezText(html_entity_decode('<b>INFORME DE AUTORIZACIONES POR USUARIO</b>'), 12, array('justification'=>'center'));
$pdf->ezSetDy(-10);


if($alert_list){
	$data = array();

	foreach($alert_list as $item){
		$description = unserialize($item['description_slg']);
		$global_info = $description['global_info'];
		$aux = array();

		switch ($item['type_slg']) {
			case 'VOID_INVOICE':
				$aux['request_date'] = $item['date_slg'];
				$aux['request_obs'] = $item['description_slg'];
				$aux['authorizing_date'] = $item['authorizing_date'];
				$aux['authorizing_obs'] = $item['authorizing_obs'];

				$aux['number_inv'] = ($item['number_inv']);
				$aux['card_number'] = $item['card_number_sal'];
				$aux['type_slg'] = html_entity_decode($global_sales_log_type[$item['type_slg']]);
				$aux['requester_usr'] = ($item['requester_usr']);
				$aux['start_coverage'] = $global_yes_no[$item['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$item['status_slg']];
				$aux['authorizing_usr'] = ($item['authorizing_usr']);
				array_push($data, $aux);
				break;

			case 'FREE':
				$aux['request_date'] = $global_info['request_date'];
				$aux['request_obs'] = $global_info['request_obs'];
				$aux['authorizing_date'] = $global_info['authorizing_date'];
				$aux['authorizing_obs'] = $global_info['authorizing_obs'];

				$aux['number_inv'] = 'N/A';
				$aux['card_number'] = $item['card_number_sal'];
				$aux['type_slg'] = html_entity_decode($global_sales_log_type[$item['type_slg']]);
				$aux['requester_usr'] = ($item['requester_usr']);
				$aux['start_coverage'] = $global_yes_no[$item['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$item['status_slg']];
				$aux['authorizing_usr'] = ($item['authorizing_usr']);
				array_push($data, $aux);
				break;

			case 'STAND_BY':
				if($global_info){
					$aux['request_date'] = $global_info['request_date'];
					$aux['request_obs'] = $global_info['request_obs'];
					$aux['authorizing_date'] = $global_info['authorizing_date'];
					$aux['authorizing_obs'] = $global_info['authorizing_obs'];
				}else{
					$aux['request_date'] = $item['date_slg'];
					$aux['request_obs'] = utf8_decode($description['observations']);
					$aux['authorizing_date'] = 'N/A';
					$aux['authorizing_obs'] = 'N/A';
				}

				$aux['number_inv'] = ($item['number_inv']);
				$aux['card_number'] = $item['card_number_sal'];
				$aux['type_slg'] = html_entity_decode($global_sales_log_type[$item['type_slg']]);
				$aux['requester_usr'] = ($item['requester_usr']);
				$aux['start_coverage'] = $global_yes_no[$item['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$item['status_slg']];
				$aux['authorizing_usr'] = ($item['authorizing_usr']);
				array_push($data, $aux);
				break;

			default:
				if($global_info){
					$aux['request_date'] = $global_info['request_date'];
					$aux['request_obs'] = $global_info['request_obs'];
					$aux['authorizing_date'] = $global_info['authorizing_date'];
					$aux['authorizing_obs'] = $global_info['authorizing_obs'];
				}else{
					$aux['request_date'] = $item['date_slg'];
					$aux['request_obs'] = utf8_decode($description['modifyObs']);
					$aux['authorizing_date'] = 'N/A';
					$aux['authorizing_obs'] = utf8_decode($description['authorize']);
				}

				$aux['number_inv'] = ($item['number_inv']);
				$aux['card_number'] = $item['card_number_sal'];
				$aux['type_slg'] = html_entity_decode($global_sales_log_type[$item['type_slg']]);
				$aux['requester_usr'] = ($item['requester_usr']);
				$aux['start_coverage'] = $global_yes_no[$item['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$item['status_slg']];
				$aux['authorizing_usr'] = ($item['authorizing_usr']);
				array_push($data, $aux);
		}
	}

	usort($data, create_function('$a,$b', 'return strcmp($a["card_number"], $b["card_number"]);'));

	$titles = array('card_number' =>  utf8_decode('<b>Número de tarjeta</b>'),
					'number_inv' =>  utf8_decode('<b>Número de factura</b>'),
					'type_slg' =>  utf8_decode('<b>Tipo de Autorización</b>'),
					'request_date' =>  utf8_decode('<b>Fecha solicitud</b>'),
					'requester_usr' =>  utf8_decode('<b>Solicitado por</b>'),
					'request_obs' =>  utf8_decode('<b>Observaciones de la solicitud</b>'),
					'start_coverage' =>  utf8_decode('<b>Inició cobertura?</b>'),
					'status_slg' =>  utf8_decode('<b>Estado</b>'),
					'authorizing_usr' =>  utf8_decode('<b>Usuario que autoriza</b>'),
					'authorizing_obs' =>  utf8_decode('<b>Observación de la autorización</b>'),
					'authorizing_date' =>  utf8_decode('<b>Fecha de autorización</b>'));

	$pdf->ezTable($data,$titles,
						  "<b>Listado de Autorizaciones de $user_full_name</b>",
						  array('showHeadings'=>1,
								'shaded'=>0,
								'showLines'=>2,
								'xPos'=>'center',
								'fontSize' => 9,
								'titleFontSize' => 11,
								'cols'=>array(
									'card_number'=>array('justification'=>'center','width'=>60),
									'number_inv'=>array('justification'=>'center','width'=>60),
									'type_slg'=>array('justification'=>'center','width'=>80),
									'request_date'=>array('justification'=>'center','width'=>60),
									'requester_usr'=>array('justification'=>'center','width'=>100),
									'request_obs'=>array('justification'=>'center','width'=>90),
									'start_coverage'=>array('justification'=>'center','width'=>60),
									'status_slg'=>array('justification'=>'center','width'=>60),
									'authorizing_usr'=>array('justification'=>'center','width'=>100),
									'authorizing_obs'=>array('justification'=>'center','width'=>80),
									'authorizing_date'=>array('justification'=>'center','width'=>60)
									)));
}else{
	$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros utilizados'), 10, array('justification'=>'center'));
}

$pdf->ezStream();
?>