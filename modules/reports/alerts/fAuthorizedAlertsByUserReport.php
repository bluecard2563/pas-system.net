<?php
/*
File: fAuthorizedAlertsByUserReportPDF
Author: David Bergmann
Creation Date: 16/02/2011
Last Modified:
Modified By:
*/

$zone = new Zone($db);
$zoneList = $zone->getZones();

$statusList = SalesLog::getSalesLogStatus($db);

$smarty->register('zoneList,global_sales_log_status,statusList,global_alertTypes,typeList');
$smarty->display();
?>