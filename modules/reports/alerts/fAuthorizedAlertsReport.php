<?php
/*
File: fAuthorizedAlertsReport
Author: David Bergmann
Creation Date: 14/02/2011
Last Modified:
Modified By:
*/

$zone = new Zone($db);
$zoneList = $zone->getZones();

$statusList = SalesLog::getSalesLogStatus($db);

$typeList = array('VOID_INVOICE','STAND_BY','MODIFICATION','SPECIAL_MODIFICATION','FREE_SALE','VOID_SALE');

$smarty->register('zoneList,global_sales_log_status,statusList,global_alertTypes,typeList');
$smarty->display();
?>