<?php
/*
File: pAuthorizedAlertsReportPDF
Author: David Bergmann
Creation Date: 14/02/2011
Last Modified:
Modified By:
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

$pdf->ezText(html_entity_decode('<b>INFORME DE AUTORIZACIONES POR ALERTA</b>'), 12, array('justification'=>'center'));
$pdf->ezSetDy(-5);

$pdf->ezText(html_entity_decode($global_alertTypes[$_POST['selType']]), 12, array('justification'=>'center'));
$pdf->ezSetDy(-20);


switch ($_POST['selType']) {
	case 'VOID_INVOICE':
		$report = InvoiceLog::getLogsForAlertReport($db, $_POST['selManager'], $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selStatus']);

		if(is_array($report)) {
			$data = array();
			foreach ($report as $r) {
				unset ($aux);
				$aux['request_date'] = $r['in_date_inl'];
				$aux['request_obs'] = $r['observation_inl'];
				$aux['authorizing_date'] = $r['authorizing_date'];
				$aux['authorizing_obs'] = $r['authorizing_obs_inl'];
				$aux['number_inv'] = $r['number_inv'];
				$aux['card_number'] = $r['card_number'];
				$aux['name_usr'] = $r['name_usr'];
				$aux['start_coverage'] = $global_yes_no[$r['start_coverage']];
				$aux['status_inl'] = $global_sales_log_status[$r['status_inl']];
				$aux['usr_name_usr'] = $r['usr_name_usr'];

				array_push($data, $aux);
				unset ($aux);
			}

			$titles = array('number_inv' => utf8_decode('<b>Numero de factura</b>'),
							'card_number' =>  utf8_decode('<b>Tarjetas</b>'),
							'request_date' =>  utf8_decode('<b>Fecha solicitud</b>'),
							'name_usr' =>  utf8_decode('<b>Solicitado por</b>'),
							'request_obs' =>  utf8_decode('<b>Observaciones de la solicitud</b>'),
							'start_coverage' =>  utf8_decode('<b>Inició cobertura?</b>'),
							'status_inl' =>  utf8_decode('<b>Estado</b>'),
							'usr_name_usr' =>  utf8_decode('<b>Usuario que autoriza</b>'),
							'authorizing_date' =>  utf8_decode('<b>Fecha de Autorización</b>'),
							'authorizing_obs' =>  utf8_decode('<b>Observaciones de Autorización</b>'));


			$pdf->ezTable($data,$titles,
						  '',
						  array('showHeadings'=>1,
								'shaded'=>0,
								'showLines'=>2,
								'xPos'=>'425',
								'fontSize' => 9,
								'titleFontSize' => 11,
								'cols'=>array(
									'serial_inv'=>array('justification'=>'center','width'=>60),
									'card_number'=>array('justification'=>'center','width'=>60),
									'request_date'=>array('justification'=>'center','width'=>60),
									'name_usr'=>array('justification'=>'center','width'=>100),
									'request_obs'=>array('justification'=>'left','width'=>100),
									'start_coverage'=>array('justification'=>'center','width'=>70),
									'status_inl'=>array('justification'=>'center','width'=>70),
									'usr_name_usr'=>array('justification'=>'center','width'=>100),
									'authorizing_date'=>array('justification'=>'center','width'=>60),
									'authorizing_obs'=>array('justification'=>'center','width'=>100)
									)));
		} else {
			$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados'), 11, array('justification'=>'center'));
		}
		break;

	case 'FREE_SALE':
		$report =  SalesLog::getSalesLogReport($db, $_POST['selManager'], $_POST['txtBeginDate'], $_POST['txtEndDate'], 'FREE', $_POST['selStatus']);
		
		if(is_array($report)) {
			$data = array();
			foreach ($report as $r) {
				unset ($aux);
				$description = unserialize($r['description_slg']);
				$global_info = $description['global_info'];
				$aux['request_date'] = $global_info['request_date'];
				$aux['request_obs'] = $global_info['request_obs'];
				$aux['authorizing_date'] = $global_info['authorizing_date'];
				$aux['authorizing_obs'] = $global_info['authorizing_obs'];

				$aux['card_number'] = $r['card_number'];
				$aux['name_usr'] = utf8_decode($r['name_usr']);
				$aux['start_coverage'] = $global_yes_no[$r['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$r['status_slg']];
				$aux['usr_name_usr'] = utf8_decode($r['usr_name_usr']);

				array_push($data, $aux);
				unset ($aux);
			}

			$titles = array('card_number' =>  utf8_decode('<b>Número de tarjeta</b>'),
							'request_date' =>  utf8_decode('<b>Fecha solicitud</b>'),
							'name_usr' =>  utf8_decode('<b>Solicitado por</b>'),
							'request_obs' =>  utf8_decode('<b>Observaciones de la solicitud</b>'),
							'start_coverage' =>  utf8_decode('<b>Inició cobertura?</b>'),
							'status_slg' =>  utf8_decode('<b>Estado</b>'),
							'usr_name_usr' =>  utf8_decode('<b>Usuario que autoriza</b>'),
							'authorizing_date' =>  utf8_decode('<b>Fecha de Autorización</b>'),
							'authorizing_obs' =>  utf8_decode('<b>Observaciones de Autorización</b>'));


			$pdf->ezTable($data,$titles,
						  '',
						  array('showHeadings'=>1,
								'shaded'=>0,
								'showLines'=>2,
								'xPos'=>'425',
								'fontSize' => 9,
								'titleFontSize' => 11,
								'cols'=>array(
									'card_number'=>array('justification'=>'center','width'=>60),
									'request_date'=>array('justification'=>'center','width'=>60),
									'name_usr'=>array('justification'=>'center','width'=>100),
									'request_obs'=>array('justification'=>'left','width'=>100),
									'start_coverage'=>array('justification'=>'center','width'=>60),
									'status_slg'=>array('justification'=>'center','width'=>70),
									'description_slg'=>array('justification'=>'left','width'=>100),
									'usr_name_usr'=>array('justification'=>'center','width'=>100),
									'authorizing_date'=>array('justification'=>'center','width'=>60),
									'authorizing_obs'=>array('justification'=>'center','width'=>100)
									)));
		}else{
			$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados'), 11, array('justification'=>'center'));
		}
		
		break;

	case 'STAND_BY':
		$report =  SalesLog::getSalesLogReport($db, $_POST['selManager'], $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selType'], $_POST['selStatus']);
		if(is_array($report)) {
			$data = array();
			foreach ($report as $r) {
				unset ($aux);
				$description = unserialize($r['description_slg']);
				$global_info = $description['global_info'];

				if($global_info){
					$aux['request_date'] = $global_info['request_date'];
					$aux['request_obs'] = $global_info['request_obs'];
					$aux['authorizing_date'] = $global_info['authorizing_date'];
					$aux['authorizing_obs'] = $global_info['authorizing_obs'];
				}else{
					$aux['request_date'] = $r['date_slg'];
					$aux['request_obs'] = $description['observations'];
					$aux['authorizing_date'] = 'N/A';
					$aux['authorizing_obs'] = 'N/A';
				}

				$aux['card_number'] = $r['card_number'];
				$aux['name_usr'] = utf8_decode($r['name_usr']);
				$aux['start_coverage'] = $global_yes_no[$r['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$r['status_slg']];
				$aux['usr_name_usr'] = utf8_decode($r['usr_name_usr']);

				array_push($data, $aux);
				unset ($aux);
			}

			$titles = array('card_number' =>  utf8_decode('<b>Número de tarjeta</b>'),
							'request_date' =>  utf8_decode('<b>Fecha solicitud</b>'),
							'name_usr' =>  utf8_decode('<b>Solicitado por</b>'),
							'request_obs' =>  utf8_decode('<b>Observaciones de la solicitud</b>'),
							'start_coverage' =>  utf8_decode('<b>Inició cobertura?</b>'),
							'status_slg' =>  utf8_decode('<b>Estado</b>'),
							'usr_name_usr' =>  utf8_decode('<b>Usuario que autoriza</b>'),
							'authorizing_date' =>  utf8_decode('<b>Fecha de Autorización</b>'),
							'authorizing_obs' =>  utf8_decode('<b>Observaciones de Autorización</b>'));


			$pdf->ezTable($data,$titles,
						  '',
						  array('showHeadings'=>1,
								'shaded'=>0,
								'showLines'=>2,
								'xPos'=>'425',
								'fontSize' => 9,
								'titleFontSize' => 11,
								'cols'=>array(
									'card_number'=>array('justification'=>'center','width'=>60),
									'request_date'=>array('justification'=>'center','width'=>60),
									'name_usr'=>array('justification'=>'center','width'=>100),
									'request_obs'=>array('justification'=>'left','width'=>100),
									'start_coverage'=>array('justification'=>'center','width'=>70),
									'status_slg'=>array('justification'=>'center','width'=>70),
									'usr_name_usr'=>array('justification'=>'center','width'=>100),
									'authorizing_date'=>array('justification'=>'center','width'=>60),
									'authorizing_obs'=>array('justification'=>'center','width'=>100)
									)));
		} else {
			$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados'), 11, array('justification'=>'center'));
		}
		break;

	default:
		$report =  SalesLog::getSalesLogReport($db, $_POST['selManager'], $_POST['txtBeginDate'], $_POST['txtEndDate'], $_POST['selType'], $_POST['selStatus']);
		if(is_array($report)) {
			$data = array();
			foreach ($report as $r) {
				unset ($aux);
				$description = unserialize($r['description_slg']);
				$global_info = $description['global_info'];

				if($global_info){
					$aux['request_date'] = $global_info['request_date'];
					$aux['request_obs'] = $global_info['request_obs'];
					$aux['authorizing_date'] = $global_info['authorizing_date'];
					$aux['authorizing_obs'] = $global_info['authorizing_obs'];
				}else{
					$aux['request_date'] = $r['date_slg'];
					$aux['request_obs'] = utf8_decode($description['observations']);
					$aux['authorizing_date'] = 'N/A';
					$aux['authorizing_obs'] = ($description['authorize'] && trim($description['authorize']) != '')?utf8_decode($description['authorize']):'N/A';
				}
				
				$aux['card_number'] = $r['card_number'];
				$aux['name_usr'] = utf8_decode($r['name_usr']);
				$aux['start_coverage'] = $global_yes_no[$r['start_coverage']];
				$aux['status_slg'] = $global_sales_log_status[$r['status_slg']];
				$aux['usr_name_usr'] = utf8_decode($r['usr_name_usr']);

				array_push($data, $aux);
				unset ($aux);
			}

			$titles = array('card_number' =>  utf8_decode('<b>Número de tarjeta</b>'),
							'request_date' =>  utf8_decode('<b>Fecha solicitud</b>'),
							'name_usr' =>  utf8_decode('<b>Solicitado por</b>'),
							'request_obs' =>  utf8_decode('<b>Observaciones de la solicitud</b>'),
							'start_coverage' =>  utf8_decode('<b>Inició cobertura?</b>'),
							'status_slg' =>  utf8_decode('<b>Estado</b>'),
							'usr_name_usr' =>  utf8_decode('<b>Usuario que autoriza</b>'),
							'authorizing_date' =>  utf8_decode('<b>Fecha de Autorización</b>'),
							'authorizing_obs' =>  utf8_decode('<b>Observaciones de Autorización</b>'));


			$pdf->ezTable($data,$titles,
						  '',
						  array('showHeadings'=>1,
								'shaded'=>0,
								'showLines'=>2,
								'xPos'=>'425',
								'fontSize' => 9,
								'titleFontSize' => 11,
								'cols'=>array(
									'card_number'=>array('justification'=>'center','width'=>60),
									'request_date'=>array('justification'=>'center','width'=>60),
									'name_usr'=>array('justification'=>'center','width'=>100),
									'request_obs'=>array('justification'=>'left','width'=>100),
									'start_coverage'=>array('justification'=>'center','width'=>60),
									'status_slg'=>array('justification'=>'center','width'=>70),
									'description_slg'=>array('justification'=>'left','width'=>100),
									'usr_name_usr'=>array('justification'=>'center','width'=>100),
									'authorizing_date'=>array('justification'=>'center','width'=>60),
									'authorizing_obs'=>array('justification'=>'center','width'=>100)
									)));
		} else {
			$pdf->ezText(html_entity_decode('No existen registros para los par&aacute;metros seleccionados'), 11, array('justification'=>'center'));
		}
		
		break;
}
 
$pdf->ezStream();
?>