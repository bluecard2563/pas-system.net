<?php
/*
File: pUpdateCurrency.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010
Modified By: David Bergmann
*/
$currency = new Currency($db);
$data['serial_cur'] = $_POST['hdnSerial_cur'];
$currency -> setSerial_cur($data['serial_cur']);
$currency -> getData();
$data['name_cur'] = $_POST['txtNameCurrency'];
$data['exchange_fee_cur'] = $_POST['txtExchange_fee_cur'];
$data['symbol_cur'] = $_POST['txtSymbol_cur'];
$data['status_cur'] = $_POST['selStatus'];

$currency -> setName_cur($data['name_cur']);
$currency -> setExchange_fee_cur($data['exchange_fee_cur']);
$currency -> setSymbol_cur($data['symbol_cur']);
$currency -> setStatus_cur($data['status_cur']);
	
if($currency -> update()){
	http_redirect("modules/currency/fSearchCurrency/1");
}else{
	$error = 1;
}

$smarty->register('error,data');
$smarty->display('modules/currency/fUpdateCurrency.'.$_SESSION['language'].'.tpl');


?>