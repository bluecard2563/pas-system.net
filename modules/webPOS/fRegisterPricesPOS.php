<?php
/**
 * File: fRegisterPricesPOS
 * Author: Patricio Astudillo
 * Creation Date: 05-oct-2012
 * Last Modified: 05-oct-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('selProduct');
	
	$pbd = new ProductByDealer($db, $selProduct);
	$pbd->getData();
	$serial_pbd = $pbd->getSerial_pbd();
	
	$pxc = new ProductByCountry($db, $pbd->getSerial_pxc());
	$pxc->getData();
	$serial_pro = $pxc->getSerial_pro();
	$serial_cou = $pxc->getSerial_cou();
	
	if($serial_pro && $serial_cou){
		$product = new Product($db,$serial_pro);
		$data = $product->getInfoProduct($_SESSION['language']);
		$data = $data[0];
		$pbpbc = new PriceByProductByCountry($db);
		
		/************* GET REFERENTIAL PRICES - FIRST ALL THEN SPECIFIC COUNTRY *****************/
		$ref_prices = $pbpbc->getPricesByProductByCountry($serial_pro, '1');
		if(is_array($ref_prices)){
			$data['ref_prices'] = $ref_prices;
		}

		$prices = $pbpbc->getPricesByProductByCountry($serial_pro, $serial_cou);
		if(is_array($prices)){
			$data['ref_prices'] = $prices;
		}
		/************* GET REFERENTIAL PRICES - FIRST ALL THEN SPECIFIC COUNTRY *****************/
		
		
		/************* GET REGISTERED PRICES *****************/
		$ppos = new PriceByPOS($db);
		
		$prices = PriceByPOS::getPricesByProductByPOS($db, $serial_pbd);
		$data['prices'] = array('');
		if(is_array($prices)){
			$data['prices'] = $prices;
		}
		$count = sizeof($data['prices'])-1;
		/************* GET REGISTERED PRICES *****************/

		
	}else{
		http_redirect('modules/webPOS/fManagePOSPrices/4');
	}

	$smarty->register('data,serial_cou,count,serial_pbd');
	$smarty->display();
?>
