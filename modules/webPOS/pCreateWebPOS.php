<?php
/**
 * File: pCreateWebPOS
 * Author: Patricio Astudillo
 * Creation Date: 01-oct-2012
 * Last Modified: 01-oct-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('hdnSerial_dea');
	Request::setString('txtIP');
	Request::setString('selStatus');
	Request::setString('txtUsername');
	Request::setString('txtPassword');
	Request::setString('change_password');
	
	
	$pos = new WebPOS($db, $hdnSerial_dea);
	$pos->setIp_pos($txtIP);
	$pos->setStatus_pos($selStatus);
	$pos->setUsername_pos(mysql_escape_string(strtolower($txtUsername)));
	
	if($change_password == 'YES'){
		$pos->setPassword_pos(md5($txtPassword));
	}
	
	if($pos->save()){
		$error = 2;
	}else{
		$error = 3;
	}
	
	http_redirect('modules/webPOS/fRegisterWebPOS/'.$error);
?>
