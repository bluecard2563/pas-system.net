<?php
/**
 * File: fCreateWebPOS
 * Author: Patricio Astudillo
 * Creation Date: 28-sep-2012
 * Last Modified: 28-sep-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('selDealer');
	
	/*********** DEALER INFORMATION ***************/
	$dealer = new Dealer($db, $selDealer);
	if(!$dealer -> getData()){
		http_redirect('modules/webPOS/fRegisterWebPOS/1');
	}
	$dealer = get_object_vars($dealer);
	
	$dealer['creator_name'] = $dealer['aux_data']['creator_name'];
	$dealer['dea_name_cou'] = Country::getCountryNameByCityCode($db, $dealer['aux_data']['serial_cit']);
	
	$dealer['dea_name_cit'] = new City($db, $dealer['aux_data']['serial_cit']);
	$dealer['dea_name_cit'] ->getData();
	$dealer['dea_name_cit'] = $dealer['dea_name_cit']->getName_cit();
	
	$dealer['name_zon'] = new Zone($db, $dealer['aux_data']['serial_zon']);
	$dealer['name_zon']->getData();
	$dealer['name_zon'] = $dealer['name_zon']->getName_zon();
	
	$dealer['dea_name_sec'] = new Sector($db, $dealer['aux_data']['serial_sec']);
	$dealer['dea_name_sec']->getData();
	$dealer['dea_name_sec'] = $dealer['dea_name_sec']->getName_sec();
	
	/*********** DEALER INFORMATION ***************/
	
	$pos = new WebPOS($db, $dealer['serial_dea']);
	$pos = get_object_vars($pos);
	$statusList = WebPOS::getStatusList($db);
	
	$smarty->register('pos,dealer,statusList');
	$smarty->display();

?>
