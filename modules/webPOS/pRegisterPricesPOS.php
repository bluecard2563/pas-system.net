<?php
/*
  File: pPricesByCountry.php
  Author: Edwin Salvador
  Creation Date: 09/03/2010
  Last Modified:
  Modified By:
 */

	Request::setInteger('serial_pbd');
	$price_by_product = $_POST['price_by_product'];

	if ($serial_pbd) {
		$pricePOS = new PriceByPOS($db);

		$pricePOS->setSerial_pbd($serial_pbd);
		$pricePOS->delete();

		if (is_array($price_by_product)) {
			foreach ($price_by_product as $data) {
				$pricePOS->setDuration_pps($data['duration_ppc']);
				if (isset($data['price_ppc'])) {
					$pricePOS->setPrice_pps($data['price_ppc']);
				} else {
					$pricePOS->setMin_pps($data['min_ppc']);
					$pricePOS->setMax_pps($data['max_ppc']);
				}
				$pricePOS->setCost_pps($data['cost_ppc']);

				if ($pricePOS->insert()) {
					$error = 2;
				} else {
					$error = 3;
				}
			}
		}
	} else {
		$error = 3;
	}

	http_redirect('modules/webPOS/fManagePOSPrices/'.$error);
?>
