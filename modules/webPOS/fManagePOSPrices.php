<?php
/**
 * File: fManagePOSPrices
 * Author: Patricio Astudillo
 * Creation Date: 04-oct-2012
 * Last Modified: 04-oct-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('0:error');
	
	$pos_list = WebPOS::getAvailablePOSByStatus($db);

	$smarty->register('error,pos_list');
	$smarty->display();
?>
