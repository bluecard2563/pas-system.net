<?php
/**
 * File: fRegisterWebPOS
 * Author: Patricio Astudillo
 * Creation Date: 28-sep-2012
 * Last Modified: 28-sep-2012
 * Modified By: Patricio Astudillo
 */
	
	Request::setInteger('0:error');

	$zone = new Zone($db);
	$zonesList = $zone->getAllowedZones($_SESSION['countryList']);

	$smarty->register('zonesList,error');

	$smarty -> display();
?>
