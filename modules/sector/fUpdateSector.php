<?php
/*
File: fUpdateSector.php
Author: David Bergmann
Creation Date: 07/01/2010
Last Modified:
Modified By:
*/

Request::setString('selSector');

$sector = new Sector($db);
$sector->setSerial_sec($selSector);

if($sector -> getData()){
	$data['serial_sec'] = $sector -> getSerial_sec();
	$data['serial_cit'] = $sector -> getSerial_cit();
	$data['code_sec'] = $sector -> getCode_sec();
	$data['name_sec'] = $sector -> getName_sec();
	//Debug::print_r($data);
}else{	
	http_redirect('modules/sector/fSearchSector/2');
}

$smarty -> register('data');
$smarty -> display();
?>