<?php 
/*
File: pNewSector.php
Author: David Bergmann
Creation Date:
Last Modified:
Modified By:
*/

$sector=new Sector($db);
$sector->setSerial_cit($_POST['selCity']);
$sector->setCode_sec($_POST['txtCodeSector']);
$sector->setName_sec($_POST['txtNameSector']);

if($sector->insert()){
	$error=1;
}else{
	$error=2;
}

http_redirect('modules/sector/fNewSector/'.$error);
?>