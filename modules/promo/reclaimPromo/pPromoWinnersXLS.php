<?php
ini_set("max_execution_time", "3600");
/*
 * File: pPromoWinnersXLS.php
 * Author: Patricio Astudillo
 * Creation Date: 15/06/2010, 11:43:33 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 15/06/2010, 11:43:33 AM
 */

Request::setString('0:serial_prm');

$promo = new Promo($db, $serial_prm);
if ($promo->getData()) {
	$days = $promo->getPeriodDays_prm();
	$finalDays = array();
	$weekendPromo = $promo->getWeekends_prm();

	//We get all the available prizes.
	$pbp = new PrizeByPromo($db);
	$prizesByPromo = $pbp->getPrizesByPromoToReclaim($serial_prm, $promo->getType_prm());
	if (is_array($prizesByPromo)) {
		$product_by_country_serials = array();
		foreach ($prizesByPromo as &$p) {
			$p['base_amount'] = $p['starting_value_pbp'];
			$p['products_in_prize'] = explode(',', $p['serials']);
			array_push($product_by_country_serials, $p['serials']);
		}

		$product_by_country_serials = implode(',', $product_by_country_serials);
	}

	/* GET PREVIOUS WINNERS */
	$previousWinners = PromoWinner::hasWinners($db, $serial_prm);
	if ($previousWinners) {
		$aux = array();
		foreach ($previousWinners as $pw) {
			array_push($aux, $pw['serial_dea']);
		}
		$previousWinners = implode(',', $aux);
	}
	/* END GETTING PREVIOUS WINNERS */

	/* Making a string with all the days that apply in the promo */
	if ($promo->getRepeat_prm() == '1' && $promo->getPeriodType_prm() == 'SEVERAL DAYS') { //It repeats in a period of time.
		if ($promo->getPeriodType_prm() == 'WEEKLY') { //It repeats Weekly
			$finalDays = explode(',', $days);
		} else { //It repeats Monthly
			if (stripos($days, ',')) { //If there is a string with a coma
				$days = explode(',', $days);

				if (is_array($days)) {
					foreach ($days as $a) {
						if (stripos($a, '-')) {
							$aux = explode('-', $a);
							for ($i = $aux['0']; $i <= $aux['1']; $i++) {
								if (!in_array($i, $finalDays)) {
									array_push($finalDays, $i);
								}
							}
						} else {
							array_push($finalDays, $a);
						}
					}
				}
			} elseif (stripos($days, '-')) { //If there's only a '-' in the string.
				$days = explode('-', $days);

				for ($i = $days['0']; $i <= $days['1']; $i++) {
					array_push($finalDays, $i);
				}
			} else { //It's only one day in the stack
				array_push($finalDays, $days);
			}

			$finalDays = implode(',', $finalDays);
		}
	} else if ($promo->getRepeat_prm() == '0') { //It doesn't repeat.
		switch ($promo->getPeriodType_prm()) {
			case 'ONE DAY':
				$finalDays = $promo->getBeginDate_prm();
				break;
			case 'RANDOM':
				$finalDays = $days;
				break;
		}
	}
	/* END making array */

	//Obtain all the suitable sales for the dealers.
	$salesList = Promo::getPromoWinners($db, $serial_prm, $promo->getAppliedTo_prm(), $promo->getRepeat_prm(), $promo->getPeriodType_prm(), $finalDays, $promo->getType_prm(), $promo->getWeekends_prm(), $promo->getRepeatType_prm(), $product_by_country_serials, NULL, $previousWinners);

	if (is_array($salesList)) {
		$count = -1;
		$current_dealer = 0;
		$winnerList = array();

		//*************************** SWEEPING SALES ARRAY TO FORM THE WINNER'S ARRAY **********************
		foreach ($salesList as $sl) {
			if ($current_dealer != $sl['serial_dea']) {
				$current_dealer = $sl['serial_dea'];
				$count++;
				$winnerList[$count]['serial_dea'] = $current_dealer;
				$winnerList[$count]['name_dea'] = $sl['name_dea'];
			}
			$amount_sold = 0;

			//******************************** VALID SALE'S AMOUNT FOR PROMO ********************************
			if ($promo->getTypeSale_prm() == 'NET') {
				$eTaxes = unserialize($sl['applied_taxes_inv']);
				$eTaxesAmount = 0;
				if (is_array($eTaxes)) {
					foreach ($eTaxes as $t) {
						$eTaxesAmount += $t['tax_percentage'];
					}
					$eTaxesAmount = $eTaxesAmount / 100;
				}

				$eDiscounts = $sl['discounts'] / 100;
				$amount_sold = $sl['amount_available'] - ($sl['amount_available'] * $eDiscounts);
				$amount_sold += ($amount_sold * $eTaxesAmount);
			} else {
				$amount_sold = $sl['amount_available'];
			}

			$winnerList[$count]['total_sold'] += $amount_sold;
			$winnerList[$count]['sales_by_product'][$sl['serial_pxc']] += $amount_sold;
		}
		//Debug::print_r($winnerList); die;
		//Transforming the promo object into an array
		$promo = get_object_vars($promo);
		unset($promo['db']);
		$neededPrizes = array();

		/* Sweeping the prize array */
		if (is_array($winnerList)) {
			foreach ($winnerList as $key => &$w) {
				$w['prizes'] = array();

				foreach ($prizesByPromo as $item) {
					/* GETTIGN THE AMOUNTS */
					$amountSoldPerProduct = 0;
					foreach ($w['sales_by_product'] as $serial_pxc => $amount_sold_pxc) {
						if (in_array($serial_pxc, $item['products_in_prize'])) {
							$amountSoldPerProduct += $amount_sold_pxc;
						}
					}
					/* GETTIGN THE AMOUNTS */

					if ($amountSoldPerProduct > 0) {
						switch ($promo['type_prm']) {
							case 'PERCENTAGE':
								$item['base_amount'] = ($item['starting_value_pbp'] / 100) * $w['total_sold'];
								//echo 'Productos: '.$item['serials'].' Disponible: '.$amountSoldPerProduct.' - Desde: '.$item['base_amount'].'<br>';

								if ($amountSoldPerProduct >= $item['base_amount'] && $item['base_amount'] > 0) {
									$item['quantity'] = 1;
									$item['amountSoldToReclaim'] = $amountSoldPerProduct;
									if ($w['first_prize']) {
										array_push($w['prizes'], $item);
									} else {
										$w['first_prize'] = $item;
									}
								}
								break;
							case 'RANGE':
								$item['base_amount'] = $item['starting_value_pbp'];
								//echo 'Disponible: '.$amountSoldPerProduct.' - Desde: '.$item['base_amount'].' Hasta: '.$item['end_value_pbp'].'<br>';

								if ($item['end_value_pbp'] > 0) {
									if ($amountSoldPerProduct >= $item['base_amount'] && $amountSoldPerProduct < $item['end_value_pbp']) {
										$item['quantity'] = 1;
										$w['first_prize'] = $item;
									}
								} else {
									if ($amountSoldPerProduct >= $item['base_amount']) {
										$item['quantity'] = 1;
										$w['first_prize'] = $item;
									}
								}
								break;
							case 'AMOUNT':
								$item['base_amount'] = $item['starting_value_pbp'];

								//echo 'Disponible: '.$amountSoldPerProduct.' - Desde: '.$item['starting_value_pbp'].'<br>';
								if ($amountSoldPerProduct >= $item['starting_value_pbp']) {
									$item['amountSoldToReclaim'] = $amountSoldPerProduct;
									$prizeQ = (int) ($amountSoldPerProduct / $item['starting_value_pbp']);
									$item['quantity'] = $prizeQ;

									if ($prizeQ > 1) {
										$amountSoldPerProduct -= ($item['base_amount'] * $prizeQ);
									} else {
										$amountSoldPerProduct -= $item['starting_value_pbp'];
									}

									if ($w['first_prize']) {
										array_push($w['prizes'], $item);
									} else {
										$w['first_prize'] = $item;
									}
								}
								break;
						}

						/* SUM NEEDED PRIZES */
						if ($item['quantity'] > 0) {
							$neededPrizes[$item['serial_pbp']]['name_pri'] = $item['name_pri'];
							$neededPrizes[$item['serial_pbp']]['quantity_needed']+=$item['quantity'];
							$neededPrizes[$item['serial_pbp']]['serial_pri'] = $item['serial_pri'];
							$neededPrizes[$item['serial_pbp']]['serial_pbp'] = $item['serial_pbp'];

							if ($neededPrizes[$item['serial_pbp']]['quantity_needed'] >= $item['stock_pri']) {
								$restockNeeded = 1;
							}
						}
					}
				}
				/* END FOREACH PRIZES */

				if (!$w['first_prize']) {
					unset($winnerList[$key]);
				}
			}
			/* END FOREACH WINNERS */
		}
		/* END IF_ARRAY */
	} else {
		//no sales available
	}
} else {
	//NO promo loaded
}

if ($winnerList) {


	$winnerTable = '<br>
		<table id="winnerTable">
			<THEAD>
				<tr>
					<td align="center" colspan="5">
						Ganadores de la Promoci&oacute;n "' . $promo['name_prm'] . '"
					</td>
				</tr>
				<tr>
					<td align="center" colspan="3">
						Informaci&oacute;n General
					</td>
					<td align="center" colspan="2">
						Premios
					</td>
				</tr>
			</THEAD>
			<TBODY>';

	foreach ($winnerList as $w) {
		$cont0 = 1;
		$winnerTable.='
				<tr>
					<td class="items">
						No.
					</td>
					<td class="items">
						Comercializador
					</td>
					<td class="items">
						Total Vendido ($)
					</td>
					<td class="items">
						Cantidad
					</td>
					<td class="items">
						Premio
					</td>
				</tr>
				<tr>
					<td align="center" class="tableField">
						' . $cont0 . '
					</td>
					<td align="center" class="tableField">
						' . $w['name_dea'] . '
					</td>
					<td align="center" class="tableField">
						' . $w['total_sold'] . '
					</td>
					<td align="center" class="tableField">
						' . $w['first_prize']['quantity'] . '
					</td>
					<td align="center" class="tableField">
						' . $w['first_prize']['name_pri'] . '
					</td>
				</tr>';
		if (sizeof($w['prizes']) > 0) {
			foreach ($w['prizes'] as $p) {
				$winnerTable.=' <tr>
										<td colspan="3">&nbsp;</td>
										<td align="center" class="tableField">
											' . $p['quantity'] . '
										</td>
										<td align="center" class="tableField">
											' . $p['name_pri'] . '
										</td>
									</tr>';
			}
		}
		$cont0++;
	}
	$winnerTable.='</TBODY></table>';

//	header('Content-type: application/vnd.ms-excel');
//	header("Content-Disposition: attachment; filename=promoWinnerList.xls");
//	header("Pragma: no-cache");
//	header("Expires: 0");
	echo $winnerTable;
}
?>