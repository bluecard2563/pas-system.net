<?php
/*
File: fNewPromo.php
Author: Patricio Astudillo
Creation Date: 01/02/2010 10:36 am
LastModified: 01/02/2010
Modified By: Patricio Astudillo
*/
Request::setInteger('0:error');

$promo=new Promo($db);
$typeList=$promo->getTypeList();
$typeSale=$promo->getTypeSale();
$periodType=$promo->getPeriodType();
$repeatType=$promo->getRepeatType();
$apliedTo=$promo->getAppliedType();

$curDate=date("d/m/Y");

$prize=new Prize($db);
$prizeList=$prize->getPrize('PROMO');

$product=new Product($db);
$productList=$product->getProducts($_SESSION['language']);

$smarty->register('typeList,typeSale,periodType,repeatType,apliedTo,error,prizeList,productList,curDate');
$smarty->display()
?>
