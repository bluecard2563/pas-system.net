<?php
/*
 * File: fSearchPendingApplications.php
 * Author: Patricio Astudillo
 * Creation Date: 21/04/2010, 12:25:39 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 26/04/2010, 12:14:39 PM
 */

//Get the user's who can dispatch refund applications
$parameter=new Parameter($db,'18');
$parameter->getData();
$userSerials=unserialize($parameter->getValue_par());
$hasAuthorization=0;
foreach($userSerials as &$u){
	$u=explode(',', $u);
	if(in_array($_SESSION['serial_usr'], $u)){
		$hasAuthorization=1;
	}
}

if(!$hasAuthorization):
	$parameter=new Parameter($db,'40');
	$parameter->getData();
	$userSerials=unserialize($parameter->getValue_par());
	$hasAuthorization=0;
	foreach($userSerials as &$u){
		$u=explode(',', $u);
		if(in_array($_SESSION['serial_usr'], $u)){
			$hasAuthorization=1;
		}
	}
endif;

if($hasAuthorization==1){
	//Get all the pending refund applications
	$refundApplications=Refund::getPendingApplications($db);

	Request::setString('0:error');
	Request::setString('1:serial_cn');

	if($serial_cn){
		$number_cn=CreditNote::getCreditNoteNumber($db, $serial_cn);
	}

	$smarty->register('refundApplications,error,number_cn,serial_cn');
}else{
	$not_allowed_user=1;
}

$smarty->register('not_allowed_user');
$smarty->display();
?>