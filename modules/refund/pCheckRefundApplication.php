<?php
/*
 * File: pCheckRefundApplication.php
 * Author: Patricio Astudillo
 * Creation Date: 26/04/2010, 04:51:55 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 26/04/2010, 04:51:55 PM
 * Modifies By: Nicolas Flores
 * Last Modified: 13/06/2011, 14:40:55 PM (VOID-REFUNDED)
 */

Request::setString('rdDiscountInCN');

if($_POST['hdnSerial_ref']){
	$refund=new Refund($db, $_POST['hdnSerial_ref']);
	$refund->getData();
	$refund->setAuthDate_ref(date('d/m/Y'));
	$user=new User($db,$refund->getSerial_usr());
	$user->getData();
	$misc['email']=$user->getEmail_usr();

	//Get the sale information so we can make the refund of the comissions/bonus/promo
	$sale=new Sales($db, $refund->getSerial_sal());
	$sale->getData();
	$misc['serial_sal']=$refund->getSerial_sal();

	//Set all the information for the credit note.
	$invoice=new Invoice($db, $sale->getSerial_inv());
	$invoice->getData();
	$invoice=get_object_vars($invoice);
	$counter=new Counter($db, $sale->getSerial_cnt());
	$counter->getData();
	$dealer=new Dealer($db, $counter->getSerial_dea());
	$dealer->getData();
	$mbc=new ManagerbyCountry($db, $dealer->getSerial_mbc());
	$mbc->getData();
	
	//Get the refund criteria.
	//TODO

	//Make the array of parameters for creating the Credit Note in DB
	$misc['db']=$db;
	$misc['type']='REFUND';
	$misc['serial']=$refund->getSerial_ref();
	$misc['serial_man']=$mbc->getSerial_man();
	$misc['observations']=$_POST['txtObservations'];
	$misc['card_number']=$sale->getCardNumber_sal();
	$misc['resolution']=$global_refundStatus[$_POST['selStatus']];
	$misc['dealer_name']=$dealer->getName_dea();
    $misc['paymentStatus']='PENDING';

	if($invoice['aux']['type_cus']!=''){
		$misc['person_type']=$invoice['aux']['type_cus'];
	}else{
		$misc['person_type']='LEGAL_ENTITY';
	}

	//Set the alert parameters so that we can marked it as done.
	$alert=new Alert($db);
	if($_POST['selStatus']=='APROVED'){
		//************************ PENALTY AMOUNTS **************************
		$misc['penalty'] = 0;
		if($_POST['paidAmounts'] && $rdDiscountInCN == 'YES'){				
			$comission_paid = AppliedComission::hasPayedComissionsBySale($db, $sale -> getSerial_sal(), Dealer::getLastComissionPaymentDate($db, $dealer -> getSerial_dea()) );
			$misc['penalty'] = $comission_paid['value_dea_com'];
			$paidAmounts=true;
		}else{
			$paidAmounts=false;
		}

		if($_POST['paidBonus'] && $rdDiscountInCN == 'YES'){
			$bonus = AppliedBonus::hasPayedBonusBySale($db, $sale -> getSerial_sal(), Dealer::getLastComissionPaymentDate($db, $dealer -> getSerial_dea()) );
			$misc['penalty'] += $bonus['total_amount_apb'];
			$paidBonus=true;
		}else{
			$paidBonus=false;
		}
		
		//************************* ERP VALIDATION AND RESTRICTIONS ******************
		$proceed_to_refund = true;
		if(ERPConnectionFunctions::invoiceHasERPConnection($mbc->getSerial_mbc())){
			$retainValue = $_POST['txtPenaltyFee'];
			if($_POST['selPenaltyType'] == 'PERCENTAGE'){
				$retainValue = $sale->getTotal_sal() * ($_POST['txtPenaltyFee'] / 100);
			}
			
			$cnote_erp_data['erp_invoice_id'] = $invoice['erp_id'];
			$cnote_erp_data['penalty_fee'] = $misc['penalty'] + $retainValue;
			$cnote_erp_data['IncludeItems'] = array((string)$sale->getCardNumber_sal());

			$erp_response = ERP_logActivity('REFUND', $cnote_erp_data);

			if($erp_response){
				$misc['cnote_number'] = $erp_response['cnote_number'];
			}else{
				$error = 8; //ERROR
				$proceed_to_refund = false;
			}
		}
		
		if($proceed_to_refund){
			$refund->setStatus_ref('APROVED');
			$refund->setTotalToPay_ref($_POST['total_to_pay']);
			$refund->setRetainType_ref($_POST['selPenaltyType']);
			$refund->setRetainValue_ref($_POST['txtPenaltyFee']);

			if($refund->update()){
				//Expire Traveler log registers for card
				if(Sales::hasTravelsRegistered($db, $refund->getSerial_sal())){
					TravelerLog::expireAllTravels($db, $refund->getSerial_sal());
				}

				/*Put the Sale in REFUND status*/
				$sLog=new SalesLog($db);
				$sLog->setSerial_sal($sale->getSerial_sal());
				$sLog->setSerial_usr($_SESSION['serial_usr']);
				$sLog->setUsr_serial_usr($_SESSION['serial_usr']);
				$sLog->setType_slg('REFUNDED');
				$sLog->setStatus_slg('AUTHORIZED');
				$miscLog=array('old_status_sal'=>$sale->getStatus_sal(),
							'new_status_sal'=>'REFUNDED',
							'Description'=>'Refund of a sale.',
							'Observations'=>$_POST['txtObservations']);

				$miscLog['global_info']['requester_usr'] = $_SESSION['serial_usr'];
				$miscLog['global_info']['request_date'] = date('d/m/Y');
				$miscLog['global_info']['request_obs'] = 'Cambio directo del sistema';
				$miscLog['global_info']['authorizing_usr'] = $_SESSION['serial_usr'];
				$miscLog['global_info']['authorizing_date'] = date('d/m/Y');
				$miscLog['global_info']['authorizing_obs'] = $_POST['txtObservations'];

				$sLog->setDescription_slg(serialize($miscLog));
				$sLog->insert();

				$sale->setStatus_sal('REFUNDED');
				$sale->update();
				
				GlobalFunctions::refundInternationalFee($db, $sale->getSerial_sal());
				/*End Sale Modification*/

				$misc['amount'] = $refund->getTotalToPay_ref();

				$result = creditNoteProcess($misc, $alert, $_POST, $paidAmounts, $paidBonus, $to_discount);			
				$error = $result['error'];
			}else{
				$error = 2;
			}
		}
	}else{
		$refund->setStatus_ref('DENIED');

		if($refund->update()){
			if($refund->getReason_ref() == 'SPECIAL'):
				$alert->setServeAlert('SPECIAL_REFUND',$_POST['hdnSerial_ref']);
			else:
				$alert->setServeAlert('REFUND',$_POST['hdnSerial_ref']);
			endif;
			
			GlobalFunctions::sendMail($misc, 'refund_application');
			$error=1;
		}else{
			$error=2;
		}
	}
	http_redirect('modules/refund/fSearchPendingAplications/'.$error.'/'.$result['cNoteID']);
}else{
	http_redirect('modules/refund/fSearchPendingAplications');
}


/*
 * @name: creditNoteProcess
 * @Description: The function for the entire process of creating the credit note.
 * @params: $misc, $alert object.
 * @returns: $error
 */
function creditNoteProcess($misc, $alert, $datosPost, $paidAmounts, $paidBonus){
	$cNote = GlobalFunctions::generateCreditNote($misc);

	$misc['cNoteNumber']=$cNote['number'];
	$misc['cNoteID']=$cNote['id'];

	switch($cNote['error']){
		case 'no_document': $error=3;
			break;
		case 'insert_error': $error=4;
			break;
		case 'update_error': $error=5;
			break;
		case 'cNote_error': $error=6;
			break;
		case 'success':
			$error=1;
			
			if($datosPost['selRefundReason'] == 'SPECIAL'):
				$alert->setServeAlert('SPECIAL_REFUND',$_POST['hdnSerial_ref']);
			else:
				$alert->setServeAlert('REFUND',$_POST['hdnSerial_ref']);
			endif;
			
			$alert->setServeAlert('REFUND',$datosPost['hdnSerial_ref']);
			$result['cNoteID']=$cNote['id'];	
			
			//REFUND COMISSIONS
			$apComission=new AppliedComission($misc['db']);
			$apComission->setSerial_sal($misc['serial_sal']);
			if($paidAmounts){
				$apComission->refundSale('REMOVE');
			}else{
				$apComission->refundSale();
			}
			
			//REFUND BONUS
			if(!$paidBonus){
				GlobalFunctions::refundBonus($misc, $misc['serial_sal']);
			}
			
			//Send Cotification E-mail
			GlobalFunctions::sendMail($misc, 'refund_application');
			break;
	}

	$result['error']=$error;
	return $result;
}
?>