<?php
/*
 * File: pNewRefundRequest.php
 * Author: Patricio Astudillo
 * Creation Date: 15/04/2010, 06:05:09 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 15/04/2010, 06:05:09 PM
 */

$refund=new Refund($db);
$refund->setSerial_sal($_POST['hdnSerial_sal']);
$refund->setSerial_usr($_SESSION['serial_usr']);
$refund->setStatus_ref('STAND-BY');
$refund->setTotalToPay_ref($_POST['total_to_pay']);
$refund->setComments_ref(specialchars($_POST['txtObservations']));
$refund->setType_ref($_POST['hdnRefundType']);
$refund->setDiscount_ref($_POST['hdnDiscount']);
$refund->setOtherDiscount_ref($_POST['hdnOtherDiscount']);
$_POST['document']['other']=$_POST['OtherDocument'];
$refund->setDocument_ref(serialize($_POST['document']));
$refund->setReason_ref($_POST['selRefundReason']);
$refund->setRetainType_ref($_POST['selPenaltyType']);
$refund->setRetainValue_ref($_POST['txtPenaltyFee']);
$refundID=$refund->insert();

ERP_logActivity('new', $refund);//ERP ACTIVITY

if($refundID){
	$alert = new Alert($db);
	$alert -> setMessage_alt('Solicitud de Reembolso pendiente.');
	$alert -> setStatus_alt('PENDING');
	$alert -> setRemote_id($refundID);
	$alert -> setTable_name('refund/fCheckRefundApplication/');
	
	if($refund ->getReason_ref() != 'SPECIAL'):
		$alert -> setMessage_alt('Solicitud de Reembolso pendiente.');
		$alert -> setType_alt('REFUND');
	else:
		$alert -> setMessage_alt('Solicitud de Reembolso Especial pendiente (100%).');
		$alert -> setType_alt('SPECIAL_REFUND');
	endif;
	
	if(!$alert -> autoSend($_SESSION['serial_usr'])){
		$error = 3;
	}else{
		$error = 1;
	}
	/*END ALERTS*/
}else{ //The refund application wans't inserted.
	$error=2;
}

http_redirect('main/refund/'.$error);
?>
