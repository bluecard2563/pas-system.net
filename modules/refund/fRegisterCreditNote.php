<?php
/**
 * File: fRegisterCreditNote
 * Author: Patricio Astudillo
 * Creation Date: 18-mar-2013
 * Last Modified: 18-mar-2013
 * Modified By: Patricio Astudillo
 */

	$refunds_list = Refund::getRefundsWithNoCNotes($db);
	
	$smarty->register('refunds_list');
	$smarty->display();
?>
