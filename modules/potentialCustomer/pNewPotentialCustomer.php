<?php
/*
File: pNewPotentialCustomer
Author: Esteban Angulo
Creation Date: 14/07/2010
*/
//Debug::print_r($_POST);
//die();
$potentialCus=new PotentialCustomers($db);
$potentialCus->setSerial_cit($_POST['selCity']);
$potentialCus->setSerial_usr($_SESSION['serial_usr']);
$potentialCus->setFirstname_pcs($_POST['txtFirstName']);
$potentialCus->setLastname_pcs($_POST['txtLastName']);
$potentialCus->setDocument_pcs($_POST['txtDocument']);
$potentialCus->setPhone1_pcs($_POST['txtPhone']);
$potentialCus->setEmail_pcs($_POST['txtMail']);
$potentialCus->setBirthdate_pcs($_POST['txtBirthdate']);
$potentialCus->setCellphone_pcs($_POST['txtCellphone']);
$potentialCus->setComments_pcs(specialchars($_POST['comments']));
$potentialCus->setStatus_pcs('ACTIVE');
$serial_pcs=$potentialCus->insert();

if($serial_pcs){
	$error=1;
}
else{
	$error=2;
}

http_redirect('modules/potentialCustomer/fNewPotentialCustomer/'.$error);
?>
