<?php
/*
File: fUpdateTax.php
Author: David Bergmann
Creation Date: 11/01/2010 09:18
Last Modified: David Bergmann
Modified By: 28/01/2010
*/

Request::setString('selTax');

$tax = new Tax($db);
$tax->setSerial_tax($selTax);

//STATUS
$statusList=$tax->getStatusList();

if($tax->getData()){
	$data['serial_tax'] = $tax -> getSerial_tax();
	$data['name_tax'] = $tax -> getName_tax();
	$data['percentage_tax'] = $tax -> getPercentage_tax();
	$data['serial_cou'] = $tax -> getSerial_cou();
	$data['status_tax'] = $tax -> getStatus_tax();
}else{
	http_redirect('modules/tax/fSearchTax/2');
}

$smarty -> register('data,statusList');
$smarty -> display();

?>