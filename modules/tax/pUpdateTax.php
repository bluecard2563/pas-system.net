<?php
/*
File: fUpdateTax.php
Author: David Bergmann
Creation Date: 11/01/2010
Last Modified: David Bergmann
Modified By: 28/01/2010
*/

$tax = new Tax($db);
     
$data['serial_tax'] = $_POST['hdnSerial_tax'];
$tax -> setSerial_tax($data['serial_tax']);
$tax -> getData();

$data['name_tax'] = $_POST['txtNameTax'];
$tax -> setName_tax($data['name_tax']);
$data['percentage_tax'] = $_POST['txtPercentageTax'];
$tax -> setPercentage_tax($data['percentage_tax']);
$data['status_tax'] = $_POST['selStatus'];
$tax -> setStatus_tax($data['status_tax']);


if($tax -> update()){
	http_redirect("modules/tax/fSearchTax/1");
}else{
	$error = 1;
}

$smarty->register('error,data');
$smarty->display('modules/tax/fUpdateTax.'.$_SESSION['language'].'.tpl');
?>