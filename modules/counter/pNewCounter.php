<?php 
/*
File: pNewCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 9:20
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

$user=new User($db);
$counter=new Counter($db);
$profile=new ProfileByCountry($db);
$parameter= new Parameter($db,3);
$favorite = new Favorites($db);

$parameter -> getData();
$textForEmail=$parameter -> getDescription_par();
$favorites = [109,368,369,370,371,372];


/*Generates Username and Password*/
$misc['db']=$db;
$misc['class']='user';
$misc['fields']='username_usr';
$misc['firstname']=$_POST['txtFirstName'];
$misc['lastname']=$_POST['txtLastName'];

$username=GlobalFunctions::generateUser($misc);
$pass=GlobalFunctions::generatePassword(6, 8, false);
/*End Generates Username and Password*/

if($_POST['isleader']=='YES'){
    $hasLeader = $user->hasLeader($_POST['selBranch']);
    if ($hasLeader){
        http_redirect('modules/counter/fNewCounter/5');
    }
}
	$user->setFirstname_usr($_POST['txtFirstName']);
	$user->setLastname_usr($_POST['txtLastName']);
	$user->setDocument_usr($_POST['txtDocument']);
	$user->setAddress_usr($_POST['txtAddress']);
	$user->setPhone_usr($_POST['txtPhone']);
	$user->setCellphone_usr($_POST['txtCellphone']);
	$user->setBirthdate_usr($_POST['txtBirthdate']);
	$user->setEmail_usr($_POST['txtMail']);
	$user->setPosition_usr($_POST['txtPosition']);
	$user->setUsername_usr($username);
	$user->setPassword_usr($pass);
	$user->setSerial_pbc($_POST['selProfile']);
	$user ->setSerial_dea($_POST['selBranch']);
	$user->setSerial_cit($_POST['selCity']);
    $user->setBelongsto_usr('DEALER');
    $user->setVisitsNumber_usr(0);
	$user->setSellFree_usr('NO');
	$user->setIn_assistance_group_usr('NO');
        $user->setIsLeader_usr($_POST['isleader']);
$userID=$user->insert();

if($userID){
	$counter->setSerial_dea($_POST['selBranch']);
	$counter->setSerial_usr($userID);
	$counter->setStatus_cnt('ACTIVE');

    // Santi 26-Abr-2021: After the user has been created, we associate favorites with only the commercial user type (DEALER)
    if ($user->getBelongsto_usr() === 'DEALER') {
        // For each favorite option insert each favorite option to the new user.
        foreach ($favorites as $fav) {
            $favorite->addUserFavorites($userID, $fav);
        }
    }

	if($counter->insert()){

        $userNameLname=$_POST['txtFirstName']." ".$_POST['txtLastName'];

		$misc=array('textForEmail'=>$textForEmail,'username'=>$username,'pswd'=>$pass,'email'=>$_POST['txtMail'],'user'=>$userNameLname);

		if(GlobalFunctions::sendMail($misc, 'new_user')){
			http_redirect('modules/counter/fNewCounter/1');
		}else{
			http_redirect('modules/counter/fNewCounter/4');
		}
	}else{
		http_redirect('modules/counter/fNewCounter/3');
	}
}
else{
	http_redirect('modules/counter/fNewCounter/2');
}
?>