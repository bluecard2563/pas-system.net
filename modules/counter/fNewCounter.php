<?php 
/*
File: fNewCounter.php
Author: Patricio Astudillo
Creation Date: 13/01/2010 9:38
Last Modified: 14/07/2010
Modified By: David Bergmann
*/

Request::setInteger('0:error');

$user= new User($db);
$profile_by_country=new ProfileByCountry($db);
$country=new Country($db);
$city= new City($db);

/*LOADING DEFAULT DATA*/
//Countries
$countryList=$country->getOwnCountries($_SESSION['countryList']);

/*Recover profiles form the system*/
$profileList=$profile_by_country->getActiveProfile_by_countryList($_SESSION['countryList']);

$smarty->register('error,profileList,countryList,userTypeList');
$smarty->display();
?>