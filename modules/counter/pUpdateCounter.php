<?php 
/*
File: pNewCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 9:20
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

$counter = new Counter($db, $_POST['hdnCounterID']);
$counter -> getData();
if( $_POST['hdnUpdateType'] == 'SPECIAL'){
	$returnSite = 'fSearchCounterSpecial';
}else{
	$returnSite = 'fSearchCounter';
}

if($_POST['isleader']=='YES'){
    $userD = new User($db, $counter -> getSerial_usr());
    $userD -> getData();
    
    $hasLeader = $userD->hasLeader($counter -> getSerial_dea());
    //echo $hasLeader; die();
    if ($hasLeader){
        $error=2;
        http_redirect('modules/counter/fUpdateCounter/2'); //error
    }
}

if($counter -> getData()){
    $counter -> setStatus_cnt($_POST['selStatus']);
    
    $userData = new User($db, $counter -> getSerial_usr());
    $userData -> getData();
    
    $userData -> setFirstname_usr($_POST['txtFirstName']);
    $userData -> setLastname_usr($_POST['txtLastName']);
    $userData -> setCellphone_usr($_POST['txtCellphone']);
    $userData -> setBirthdate_usr($_POST['txtBirthdate']);
    $userData -> setEmail_usr($_POST['txtMail']);
    $userData -> setPosition_usr($_POST['txtPosition']);
    $userData -> setPhone_usr($_POST['txtPhone']);
    $userData -> setAddress_usr($_POST['txtAddress']);
    $userData -> setStatus_usr($_POST['selStatus']);
    $userData -> setIsLeader_usr($_POST['isleader']);
	$userData -> setPtp_use($_POST['ptp_use']);

    if($userData -> update() && $counter->update()){
    	
    	//IF checked, change username and email it:
	    if($_POST['chkChangeUsername'] == 'on'){
	    	$misc['db'] = $db;
			$misc['class'] = 'user';
			$misc['fields'] = 'username_usr';
			$misc['firstname'] = $userData -> getFirstname_usr();
			$misc['lastname'] = $userData -> getLastname_usr();
	    	$username = GlobalFunctions::generateUser($misc);
	    	$userData -> setUsername_usr($username);
	    	
	    	if(!$userData -> update()){
	    		http_redirect('modules/counter/fUpdateCounter/1'); //error
	    	}
	    	
	    	$misc['user'] = $userData -> getFirstname_usr() . ' ' . $userData -> getLastname_usr();
	    	$misc['username'] = $username;
	    	$misc['pswd'] = 'El mismo, no ha cambiado.';
	    	$misc['email'] = $userData -> getEmail_usr();
		    if(!GlobalFunctions::sendMail($misc, 'username_updated')){
				http_redirect('modules/counter/fUpdateCounter/1'); //error
			}
		}	
		http_redirect("modules/counter/$returnSite/1");
    }
}

//RELOAD INFO AND SEND ERROR MESSAGE:
$counter->getData();
$auxData=$counter->getAux_cnt();
$data['serial_cnt']=$hdnCounterID;
$data['status_cnt']=$counter->getStatus_cnt();
$data['dealer']=$auxData['dealer'];
$data['branch']=$auxData['branch'];
$data['counter']=$auxData['counter'];
$error=1;

$statusList=$counter->getStatusTypes();
$smarty->register('data,statusList,error');
$smarty->display('modules/counter/fUpdateCounter.'.$_SESSION['language'].'.tpl');
?>