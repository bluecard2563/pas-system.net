<?php 
/*
File: fUpdateCounter.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 15:27
Last Modified: 14/01/2010
Modified By: Patricio Astudillo
*/

	Request::setInteger('0:error');
	Request::setInteger('hdnCounterID');
	Request::setString('hdnUpdateType');
	
	$counter = new Counter($db, $hdnCounterID);
	
	if($counter -> getData()){
	    $auxData = $counter -> getAux_cnt();
	    $data['serial_cnt'] = $hdnCounterID;
	    $data['status_cnt'] = $counter -> getStatus_cnt();
	    $data['dealer'] = $auxData['dealer'];
	    $data['branch'] = $auxData['branch'];
	    $data['counter'] = $auxData['counter'];
	    $statusList = $counter -> getStatusTypes();
	    
	    $userData = new User($db, $counter -> getSerial_usr());
	    $userData -> getData();
	    
	    $city = new City($db, $userData -> getSerial_cit());
	    $city -> getData();
	    
	    $country = new Country($db, $city -> getSerial_cou());
	    $country -> getData();
	    
            $userData = get_object_vars($userData);
            $userData['country_usr'] = $country -> getName_cou();
            $userData['city_usr'] = $city -> getName_cit();
            
            $serial_dealer = $counter -> getSerial_dea();
            $dealer = new Dealer($db,$serial_dealer);
            $dealer ->getData();
            $bonusTo = $dealer ->getBonus_to_dea();
            
	}
	
	$smarty -> register('data,statusList,error,userData,hdnUpdateType,bonusTo');
	$smarty -> display();
?>