<?php
/*
File: fUpdateServiceProvider.php
Author: Mario L�pez
Creation Date: 19/02/2010
Last Modified: 22/02/2009
Modified By: Patricio Astudillo
Last Modified:24/06/2010
Modified By:Nicolas Flores (using table instead of autocompleter)
*/

Request::setInteger('0:hdnServProvSerial');
if($hdnServProvSerial){
    $serviceProvider = new ServiceProvider($db,$hdnServProvSerial);
    $serviceTypes = $serviceProvider->getAllTypes();
    $serviceStates = $serviceProvider->getAllStates();
    $serviceProvider->getData();
    $serviceProvider=get_object_vars($serviceProvider);
	unset($serviceProvider['db']);
	$serviceProvider['phone_spv']=unserialize($serviceProvider['phone_spv']);
	$serviceProvider['initial_phone']=$serviceProvider['phone_spv']['0'];
	unset($serviceProvider['phone_spv']['0']);

	
	if(is_array(unserialize($serviceProvider['email_spv']))) {
		$serviceProvider['email_spv']=unserialize($serviceProvider['email_spv']);
		$serviceProvider['initial_email']=$serviceProvider['email_spv']['0'];
		unset($serviceProvider['email_spv']['0']);
	} else {
		$serviceProvider['initial_email'] = $serviceProvider['email_spv'];
		unset($serviceProvider['email_spv']);
	}

    $city= new  City($db);
    $cities=$city->getCitiesByCountry($serviceProvider['aux']['serial_cou'], $_SESSION['cityList']);
    $country=new Country($db);
    $countryList=$country->getOwnCountries($_SESSION['countryList']);
    unset($serviceProvider['db']);
    
    $smarty->register('serviceTypes,serviceStates,cities,serviceProvider,countryList');
    $smarty->display();
}else{
    http_redirect('modules/serviceProvider/fSearchServiceProvider/2');
}
?>
