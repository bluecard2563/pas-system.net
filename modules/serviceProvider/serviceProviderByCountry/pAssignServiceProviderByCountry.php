<?php
/*
File: pAssignServiceProviderByCountry.php
Author: Mario Lopez
Creation Date: 28/02/2010
Modified By: Edwin Salvador
Last Modified: 01/03/2010
*/
Request::setInteger('hdnServProvSerial');
$error=1;
$countryList=$_POST['dealersTo'];//THE SELECTED COUNTRIES
$servProvByCountry=new ServiceProviderByCountry($db);
$servProvByCountry->setSerial_spv($hdnServProvSerial);
$assigned_countries = $servProvByCountry->getServiceProviderByCountry();//THE COUNTRIES THAT WERE ASSIGNED BEFORE
if(is_array($assigned_countries)){
    foreach($assigned_countries as &$ac){//GET THE SERIALS OF THE ASSIGNED COUNTRIES
        $ac = $ac['serial_cou'];
    }
}
if(is_array($countryList)){//IF THE USER SELECTED ANY COUNTRY
    foreach($countryList as $c){
        $servProvByCountry->setSerial_cou($c);
        if(is_array($assigned_countries)){//IF THERE IS ANY ASSIGNED COUNTRY
            if(!in_array($c, $assigned_countries)){//IF THE SELECTED COUNTRY HAS NOT BEEN ASSIGNED BEFORE THEN INSERT IT
                $servProvByCountry->setStatus_spc('ACTIVE');
                if(!$servProvByCountry->insert()){
                    $error=3;
                }
            }else{//IF THE SELECTED COUNTRY HAS BEEN ASSIGNED BEFORE THEN ACTIVATE IT
                $servProvByCountry->setStatus_spc('ACTIVE');
                if(!$servProvByCountry->update()){
                    $error =3;
                }
                if(is_array($assigned_countries)){//DELETE FROM THE ASSIGNED COUNTRIES ARRAY THE CURRENT COUNTRY
                    unset($assigned_countries[array_search($c, $assigned_countries)]);
                }
            }
        }else{//IF IS THE FIRST TIME ASSIGNING COUNTRIES
           $servProvByCountry->setStatus_spc('ACTIVE');
            if(!$servProvByCountry->insert()){
                $error=3;
            }
        }
    }
}

if(is_array($assigned_countries)){//IF THERE IS A COUNTRY AT THIS POINT HAS TO BE DISABLED
    foreach($assigned_countries as $key=>$aco){
        $servProvByCountry->setSerial_cou($aco);
        $servProvByCountry->setStatus_spc('INACTIVE');
        if(!$servProvByCountry->update()){
            $error =3;
        }
    }
}

http_redirect('modules/serviceProvider/serviceProviderByCountry/fSearchServiceProviderByCountry/'.$error);
?>
