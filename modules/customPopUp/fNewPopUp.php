<?php
/**
 * File: fNewPopUp
 * Author: Patricio Astudillo
 * Creation Date: 20/04/2015
 * Last Modified By: 20/04/2015
 */

	Request::setInteger('0:add_id');
	
	$currentAdd = new CustomAdds($db, $add_id);
	$currentAdd = get_object_vars($currentAdd);
	unset($currentAdd['db']);
	
	//************* GET ALL SCOPES
	$scopes = AddScope::getScopeForAdd($db, $currentAdd['id']);
	if($scopes){
		$scopeType = $scopes['0']['addType'];
	}
	
	$mbcEC = new ManagerbyCountry($db);
	$mbcEC = $mbcEC->getManagerByCountry('62', '1');
	
	$responsibleList = UserByDealer::getResponsiblesByCountry($db, '62');
	
	$smarty->register('add_id,currentAdd,mbcEC,responsibleList,scopeType,scopes');
	$smarty->display();