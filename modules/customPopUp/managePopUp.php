<?php
/**
 * File: managePopUp
 * Author: Patricio Astudillo
 * Creation Date: 20/04/2015
 * Last Modified: 
 * Modified By: 
 */
	Request::setInteger('0:error');
	
	$popUps = CustomAdds::getCustomAdds($db);
	
	$smarty->register('popUps,error');
	$smarty->display();