<?php
/**
 * File: pManagePopUp
 * Author: Patricio Astudillo
 * Creation Date: 21/04/2015
 * Last Modified By: 21/04/2015
 */

$cAdd = new CustomAdds($db);
if(isset($_POST['hdnID'])){
	$cAddID = $_POST['hdnID'];
	$cAdd->setId($cAddID);
	$cAdd->getData();
}

//UPLOADING ART FILE
$allowedArtExtensions = array("image/png","image/jpeg","image/pjpeg");
$artFileUploaded = false;

if(isset($_FILES['txtArt']['name'])){
	if(in_array($_FILES["txtArt"]["type"], $allowedArtExtensions)){
		
			$artFileName = $_FILES["txtArt"]['name'];
			
			if(copy($_FILES['txtArt']['tmp_name'], DOCUMENT_ROOT.'img/customAdds/'.$artFileName)){
				$artFileUploaded = true;
			}
	}else{
		http_redirect('modules/customPopUp/managePopUp/2');	//BAD ART EXTENSION
	}
}

//UPLOADING ART FILE
$allowedPDFExtensions = array("application/pdf");
$pdfFileUploaded = false;
$pdfFileName= null;

if(isset($_FILES['txtPDF']['name']) && $_FILES['txtPDF']['name'] != NULL){
	if(in_array($_FILES["txtPDF"]["type"], $allowedPDFExtensions)){
		
			$pdfFileName = $_FILES["txtPDF"]['name'];
			
			if(copy($_FILES['txtPDF']['tmp_name'], DOCUMENT_ROOT.'pdfs/customAdds/'.$pdfFileName)){
				$pdfFileUploaded = true;
			}
	}else{
		http_redirect('modules/customPopUp/managePopUp/3');	//BAD PDF EXTENSION
	}
}else{
	$pdfFileUploaded = true;
}

if($pdfFileUploaded && $artFileUploaded){	
	$cAdd->setPublishDate($_POST['txtPublishDate']);
	$cAdd->setCreatedBy($_SESSION['serial_usr']);
	$cAdd->setDescription(html_entity_decode(trim($_POST['arDescription'])));
	$cAdd->setExpireDate($_POST['txtExpirationDate']);
	$cAdd->setTitle($_POST['txtTitle']);
	$cAdd->setWebLink($_POST['txtLink']);
	$cAdd->setImage($artFileName);
	$cAdd->setPdfFile($pdfFileName);
	
	if($cAdd->getId()){
		$cAdd->update();
	}else{
		$cAdd->insert();
	}
	
	if($cAdd->getId()){
		//RESET ALL PREVIOUS INFORMATION OF SCOPE
		AddScope::removeAllScope($db, $cAdd->getId());
		
		
		//INSERT NEW SCOPE
		$addScope = new AddScope($db);
		$addScope->setAddsID($cAdd->getId());
		
		if(isset($_POST['chManager'])){ //FOR MANAGER	
			$addScope->setAddType('MANAGER');
			
			foreach($_POST['chManager'] as $serial_mbc){	
				$addScope->setSerial_mbc($serial_mbc);
				
				if(!$addScope->insert()){
					//die('here');
					http_redirect('modules/customPopUp/managePopUp/6');	//SCOPE INSERT FAILED
				}
				
				$addScope->setId(NULL);
			}
		}else{ //FOR RESPONSIBLE
			$addScope->setAddType('RESPONSIBLE');
			
			foreach($_POST['chResponsible'] as $serial_usr){	
				$addScope->setResponsibleID($serial_usr);
				
				if(!$addScope->insert()){
					//die('here');
					http_redirect('modules/customPopUp/managePopUp/6');	//SCOPE INSERT FAILED
				}
				
				$addScope->setId(NULL);
			}
		}
	
		http_redirect('modules/customPopUp/managePopUp/1');	//SUCCESS
	}else{
		http_redirect('modules/customPopUp/managePopUp/4');	//BAD INSERT CUSTOM ADD
	}
}else{
	http_redirect('modules/customPopUp/managePopUp/5');	//BAD UPLOAD FILES
}
