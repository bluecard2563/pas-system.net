<?php
/**
 * Allows to obtain the data from the incentives delivery control.
 * @author Santi Albuja
 * @date 1/Jul/2021
 * @var string $db
 * @var string $txtDateFrom
 * @var strin $txtDateTo
 */

$userId = $_SESSION['serial_usr'];
$user = new User($db, $userId);
$manager = new Manager($db);
$invoice = new Invoice($db);

// Get information from the logged user.
$userData = $user->getData(true);
$managerByCountryId = $user->getSerial_mbc();
$belongsToManager = $user->getBelongsto_usr();
$isManager = $user->getBelongsto_usr() === 'MANAGER' ? true : false;
$accessAllowed = true;
$incentives = array();

if ($isManager) {
    if (!empty($managerByCountryId)) {
        // Get the manager data by manager by country associated to the logged user.
        $managerData = $manager->getManagerByManagerByCountryId($managerByCountryId);
        $managerId = $managerData['serial_man'];

        // If the logged user manager id is equal to 1 then get all the records from the delivery control. Otherwise get the records associated with the manager id.
        if ($managerId == 1) {
            $incentives = $invoice->getIncentives($db);
        } else {
            $incentives = $invoice->getIncentivesByManagerId($db, $managerId);
        }
    }
} else {
    $accessAllowed = false;
}

$smarty->register('error,accessAllowed,incentives');
$smarty->display();

