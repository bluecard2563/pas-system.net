<?php
/**
 * Allows to update the data of the incentives delivery control.
 * @author Santi Albuja
 * @date 1/Jul/2021
 * @var string $db
 * @var string $action
 * @var int $incentiveDeliveryId
 * @var int $delivered
 */

Request::setInteger('incentiveDeliveryId');
Request::setInteger('delivered');
Request::setString('action');
$action = trim($action);
$userSession = $_SESSION['serial_usr'];

if ($action === 'updateDelivery') {
    $invoice = new Invoice($db);
    $response = $invoice->updateIncentivesDelivery($db, $incentiveDeliveryId, $delivered, $userSession);
    echo $response;
    exit();
}
