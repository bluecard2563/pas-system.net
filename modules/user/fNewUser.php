<?php
/*
File: fNewUser.php
Author: Esteban Angulo
Creation Date: 24/12/2009 12:00
Last Modified:06/01/2010 
*/
Request::setInteger('0:error');
$user= new User($db, $_SESSION['serial_usr']);
$profile_by_country=new ProfileByCountry($db);
$country=new Country($db);
$city= new City($db);

/*LOADING DEFAULT DATA*/
//Countries
$countryList=$country->getOwnCountries($_SESSION['countryList']);

//Cities
$cityAccessList=$city->getCityAccessListbyUser($_SESSION['serial_usr']);

/*Recover profiles form the system*/
$profileList=$profile_by_country->getActiveProfile_by_countryList($_SESSION['countryList']);
foreach($profileList as &$pl){
	$pl['name_pbc'] = $pl['name_pbc'];
}

/*Recoves all available type of users form the system*/
$userTypeList=$user->getAllTypes();
if($user->isPlanetAssistUser()){
    $planetAssistUser=1;
    $serial_cou=$country->countryExists('ecuador');
}


$smarty->register('error,profileList,countryList,userTypeList,cityAccessList,planetAssistUser,serial_cou');
$smarty->display();
?>
