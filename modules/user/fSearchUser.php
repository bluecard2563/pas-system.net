<?php
/*
File: fSearchUser.php
Author: Esteban Angulo
Creation Date: 24/12/2009 12:00
Last Modified:06/01/2010 
*/
	Request::setInteger('0:message');
	$user = new User($db);
	
	$users = $user -> getUsers();
	if(is_array($users))
		foreach($users as $key=>$u){
				$users_list [$key] = $u['first_name_usr'].' '.$u['last_name_usr'].' - '.$u['username_usr'];
		}
	
	$smarty -> register('users_list,message');
	$smarty->display();
?>