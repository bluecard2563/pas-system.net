<?php
/*
File: fUpdateDocumentType.php
Author: Santiago Ben�tez
Creation Date: 11/01/2010 15:22
Last Modified: 16/02/2010
Modified By: Edwin Salvador
*/
Request::setString('0:serial_doc');
$documentType = new DocumentType($db);
$documentType->setSerial_doc($serial_doc);
	
if($documentType->getData()){
    $zone= new  Zone($db);
    $zonesList=$zone->getZones();
    $country = new Country($db);
	$purposeValues=DocumentByManager::getPurposeValues_dbm($db);

	$managerByCountry = new ManagerbyCountry($db);

    $data['serial_doc'] = $documentType -> getSerial_doc();
    $data['name_doc'] = $documentType -> getName_doc();

    $dbm = new DocumentByManager($db);
    $dbm->setSerial_doc($data['serial_doc']);
    $assigned_managers = $dbm->getAssignedManagers();

	if(is_array($assigned_managers)){
		foreach($assigned_managers as &$am){
		   $am['zone_countries']=array();
		   $country_list = $country->getCountriesByZone($am['serial_zon'],$_SESSION['countryList']);

		   $am['country_managers']=$managerByCountry->getManagerByCountry($am['serial_cou'], 1);
		   
		   foreach($country_list as $key=>$cl){
			   $am['zone_countries'][$key]=array();
			   $am['zone_countries'][$key]['serial_cou'] = $cl['serial_cou'];
			   $am['zone_countries'][$key]['name_cou'] = $cl['name_cou'];
		   }
		   $am['is_used'] = $dbm->checkIsUsed_dbm($am['serial_dbm']);
		}
	}
    $count = sizeof($assigned_managers);
    $mandatory_manager = $assigned_managers[0];
    unset($assigned_managers[0]);
    //Debug::print_r($assigned_countries);
}else{
    http_redirect('modules/document/fSearchDocumentType/2');
}

$smarty -> register('data,assigned_managers,zonesList,mandatory_manager,count,purposeValues');
$smarty -> display();

?>