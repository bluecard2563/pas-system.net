<?php
/*
File: pUpdateDocumentType.php
Author: Santiago Benítez
Creation Date: 11/01/2010 
Last Modified: 16/02/2010
Modified By: Edwin Salvador
*/

Request::setString('hdnSerial_doc');
Request::setString('txtNameDocumentType');
Request::setString('managersCount');

$documentType = new DocumentType($db);
$documentType -> setSerial_doc($hdnSerial_doc);

if($documentType -> getData()){
    $documentType -> setName_doc($txtNameDocumentType);

    if($documentType -> update()){
        $dbm = new DocumentByManager($db);
        $dbm->setSerial_doc($hdnSerial_doc);
		
        if($dbm -> delete()){
			$band = 0;
            for($i=0; $i<=$managersCount; $i++){
                if($_POST['is_used'][$i] == 0){
				
                    if($_POST['selManager'.$i] != '' && $_POST['selType'.$i]!= ''){
                        $dbm->setSerial_man($_POST['selManager'.$i]);
                        $dbm->setSerial_doc($hdnSerial_doc);
                        $dbm->setType_dbm($_POST['selType'.$i]);
						$dbm->setPurpose_dbm($_POST['selPurpose'.$i]);
						$dbm->setNumber_dbm(1);

						if(!DocumentByManager::sameDocumentExists($db, $hdnSerial_doc, $_POST['selManager'.$i], $_POST['selPurpose'.$i], $_POST['selType'.$i])){
							if(!$dbm->insert()){
								$band = 1;
							}
						}
                    }
                }
            }
			
			if( $band == 0 ){
				$error = 1;
			}else{
				$error = 2;
			}
        }else{
            $error = 4;
        }
    }else{
        $error=5;
    }
}else{
    $error =6;
}

http_redirect('modules/document/fSearchDocumentType/'.$error);//error in updating document type
?>
