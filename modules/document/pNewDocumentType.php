<?php 
/*
File: pNewDocumentType.php
Author: Santiago Ben�tez
Creation Date: 11/01/2010 
Last Modified: 15/02/2010 12:10
Modified By: Edwin Salvador
*/

$documentType=new DocumentType($db);
$documentType->setName_doc($_POST['txtNameDocumentType']);

$assigned_managers = $_POST['managersCount'];
$serial_doc = $documentType->insert();
if($serial_doc !== false){
    for($i=0; $i<=$assigned_managers; $i++){
        if($_POST['selManager'.$i] != '' && $_POST['selType'.$i]!= ''){
            $dbm = new DocumentByManager($db);
            $dbm->setSerial_man($_POST['selManager'.$i]);
            $dbm->setSerial_doc($serial_doc);
            $dbm->setType_dbm($_POST['selType'.$i]);
			$dbm->setPurpose_dbm($_POST['selPurpose'.$i]);
			$dbm->setNumber_dbm(1);
            $dbm->insert();
        }
    }
    $error=1;
}else{
    $error=2;
}
http_redirect('modules/document/fNewDocumentType/'.$error);
?>