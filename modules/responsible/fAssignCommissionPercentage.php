<?php
/*
File:fAssignCommission
Author: Esteban Angulo
Creation Date: 21/06/2010
*/
Request::setInteger('0:error');
$zone=new Zone($db);
$zoneList=$zone->getZones();
$user= new User($db, $_SESSION['serial_usr']);
if($user->isPlanetAssistUser()){
    $planetAssistUser=1;
}

$smarty->register('zoneList,planetAssistUser,error');
$smarty->display();
?>
