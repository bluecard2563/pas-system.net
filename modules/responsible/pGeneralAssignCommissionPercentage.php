<?php
/*
File:pGeneralAssignCommission
Author: Esteban Angulo
Creation Date: 21/06/2010
*/
//Debug::print_r($_POST);

$user=new User($db);
if($_POST['selResponsible']){
	$user->setSerial_usr($_POST['selResponsible']);
}
else{
	$user->setSerial_usr($_POST['userTBL']);
}

$user->getData();
$user->setCommission_percentage_usr($_POST['txtPercentage']);

if($user->update()){
	$userByDealer=new UserByDealer($db);
	$responsibleList=$userByDealer->getResponsibles($user->getSerial_usr());
	if(is_array($responsibleList)){
		foreach($responsibleList as $key=>$ubd){
			$user_by_dealer=new UserByDealer($db);
			$user_by_dealer->setSerial_usr($ubd['serial_ubd']);
			$user_by_dealer->setSerial_dea($ubd['serial_dea']);
			$user_by_dealer->getData();
			$user_by_dealer->setPercentage_ubd($_POST['txtPercentage']);
			if(!$user_by_dealer->update()) {
				$error=2;
				break;
			}
		}
	}
	$error=1;
}
else{
	$error=2;
}
http_redirect('modules/responsible/fGeneralAssignCommissionPercentage/'.$error);

?>
