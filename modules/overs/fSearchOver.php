<?php 
/*
File: fSearchOver.php
Author: Santiago Borja 
Creation Date: 17/03/2010
*/

	Request::setInteger('0:error');
	
	$over = new Over($db);
	$apliedTo=$over->getAppliedType();  

	$over_active = Over::getOvers($db);
	$smarty->register('apliedTo,error,over_active');
	$smarty -> display();
?>