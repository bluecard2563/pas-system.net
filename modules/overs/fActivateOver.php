<?php 
/*
File: fSearchOver.php
Author: Santiago Borja 
Creation Date: 17/03/2010
*/

	Request::setInteger('0:error');
	
	$over = new Over($db);
	$apliedTo=$over->getAppliedType();
	
	$oversToActivate = Over::getOversByStatus($db,'PENDING');
	
	$smarty->register('apliedTo,error,oversToActivate');
	$smarty->display();
?>