<?php
/*
File: pUpdateOver.php
Author: Santiago Borja 
Creation Date: 17/03/2010
*/

	$apliedTo = $_POST['selApliedTo'];
	
	/*Sets Over Basic Information*/
	$over = new Over($db, $_POST['serial_ove']);
	$error = 1;
	
	if($over -> getData()){
		$over -> setEndDate_ove($_POST['txtEndDate']);
		$over -> setBeginDate_ove($_POST['txtBeginDate']);
		$over -> setPercentageIncrease_ove($_POST['increasePer']);
		$over -> setPercentageComission_ove($_POST['comissionPer']);
		$over -> setName_ove($_POST['txtName']);
		$over -> setAppliedTo_ove($apliedTo);

		if($over -> update()){
			//Putting all previous information as INACTIVE/DENIED to insert new ones.
			$disable_appOvers = $over -> setAllAppliedOversInactive();
			$disable_InfoOvers = InfoOver::disablePreviousActiveInfoOver($db, $over -> getSerial_ove());

			if($disable_appOvers && $disable_InfoOvers){
				//BASIC INFORMATION FOR APPLIED OVERS
				$appOver = new AppliedOver($db);
				$appOver -> setStatus_apo('ACTIVE');
				
				//BASIC INFORMATION FOR INFO OVERS
				$infoOver = new InfoOver($db);
				$infoOver -> setSerial_ove($over -> getSerial_ove());
				$infoOver -> setBegin_date_ino($_POST['txtInfoBeginDate']);
				$infoOver -> setEnd_date_ino($_POST['txtInfoEndDate']);
				$infoOver -> setStatus_ino('ACTIVE');
				$infoOver -> setAmount_ino('0');				
				
				switch($apliedTo){ /*WE insert the details for the over according to it's type*/
					case 'MANAGER':
						$appOver -> setSerial_mbc($_POST['selManager']);
						$serialApo = $appOver -> insert();
						
						if($serialApo){							
							$infoOver -> setSerial_apo($serialApo);							
							$serialInfo = $infoOver -> insert();
							
							if(!$serialInfo){ //INFO OVER NOT INSERTED
								$error = 6;
							}
						}else{ //APPLIED OVER NOT INSERTED
							$error = 5;
						}
						break;
					case 'DEALER':
						$dealers = $_POST['dealersTo'];						
						
						foreach($dealers as $dea){
							$appOver -> setSerial_dea($dea);
							$serialApo = $appOver -> insert();
							
							if($serialApo){								
								$infoOver -> setSerial_apo($serialApo);								
								$serialInfo = $infoOver -> insert();
								
								if(!$serialInfo){ //INFO OVER NOT INSERTED
									$error = 6;
								}
							}else{ //APPLIED OVER NOT INSERTED
								$error = 5;
								break;
							}
						}
						break;
				}
			}else{ //PREVIOUS RECORDS NOT DISABLED
				$error = 4;
			}
		}else{ //BASIC OVER INFORMATION NOT UPDATED
			$error = 3;
		}
	}else{ //OVER INFORMATION NOT LOADED
		$error = 2;
	}
	
    http_redirect('modules/overs/fSearchOver/'.$error);    
?>