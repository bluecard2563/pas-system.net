<?php
/*
 * File: pPrintCreditNote.php
 * Author: Patricio Astudillo
 * Creation Date: 03/05/2010, 09:38:03 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 03/05/2010, 09:38:03 AM
 */

$cNoteOld = new CreditNote($db, $_POST['serial_cn']);
$cNoteOld -> getData();
if($cNoteOld -> getStatus_cn() == 'ACTIVE'){
	$cNoteOld -> setStatus_cn('VOID');

	/*GET CREDIT NOTE DATA TO GENERATE THE NEW NUMBER*/
	$mbc = new ManagerbyCountry($db, $_POST['selManager']);
	$mbc -> getData();

	$misc['type'] = $_POST['hdnType'];
	$misc['serial_man'] = $mbc->getSerial_man();
	$misc['person_type'] = $_POST['type_dbc'];
	$misc['amount'] = $cNoteOld -> getAmount_cn();
	$misc['db'] = $db;
	/*END NUMBER*/

	if($_POST['hdnType'] == 'INVOICE'){
		$misc['serial'] = $cNoteOld -> getSerial_inv();
		$misc['serial_inl'] = InvoiceLog::getSerialLogByInvoice($db, $cNoteOld -> getSerial_inv());

		/*GET INVOICE DATA*/
		$invoice = new Invoice($db,$cNoteOld->getSerial_inv());
		$invoice -> getData();
		$invoice = get_object_vars($invoice);
		unset($invoice['db']);
		/*END INVOICE*/
	}else if($_POST['hdnType'] == 'REFUND'){
		$misc['serial'] = $cNoteOld -> getSerial_ref();

		/*GET REFUND DATA*/
		$refund = new Refund($db, $cNoteOld -> getSerial_ref());
		$refund -> getData();
		$refund = get_object_vars($refund);
		unset($refund['db']);
		/*END REFUND*/

		$sale = new Sales($db, $refund['serial_sal']);
		$sale -> getData();
		$sale = get_object_vars($sale);
		unset($sale['db']);

		$cNotePDF['serial_sal'] = $sale['serial_sal'];
		$cNotePDF['serial_ref'] = $refund['serial_ref'];
		$misc['paymentStatus'] = $cNoteOld -> getPayment_status_cn();

		$invoice = new Invoice($db, $sale['serial_inv']);
		$invoice -> getData();
		$invoice = get_object_vars($invoice);
	}else{ //PENALTY		
		$misc['serial'] = $cNoteOld -> getSerial_sal();
		$misc['penalty'] = $cNoteOld -> getPenalty_fee_cn();
	}

	$cNote = GlobalFunctions::generateCreditNote($misc);
	$misc['cNoteNumber'] = $cNote['number'];
	$misc['cNoteID'] = $cNote['id'];

	if($cNote['number'] > 0){
		if($cNoteOld -> update()){
			$serial_generated_cNote=$misc['cNoteID'];

			if($misc['type']=='PENALTY'){
				$paymentDetail=new PaymentDetail($db);
				$payInfo=$paymentDetail->getPaymentDetailSerial($cNoteOld->getSerial_cn());

				$paymentDetail->setSerial_pyf($payInfo[0]['serial_pyf']);
				$paymentDetail->getData();
				$paymentDetail->setStatus_pyf('VOID');
				$paymentDetail->setObservation_pyf(utf8_decode('Anulada por reimpresión de N.C.'));
				$paymentDetail->update();

				$pDetailNew=new PaymentDetail($db);
				$pDetailNew->setSerial_pay($payInfo[0]['serial_pay']);
				$pDetailNew->setSerial_cn($serial_generated_cNote);
				$pDetailNew->setSerial_usr($_SESSION['serial_usr']);
				$pDetailNew->setType_pyf($paymentDetail->getType_pyf());
				$pDetailNew->setAmount_pyf($paymentDetail->getAmount_pyf());
				$pDetailNew->setDate_pyf($paymentDetail->getDate_pyf());
				$pDetailNew->setComments_pyf($paymentDetail->getComments_pyf());
				$pDetailNew->setDocumentNumber_pyf($cNote['number']);
				$pDetailNew->getStatus_pyf('ACTIVE');
				$pDetailNew->insert();
			}
			$_POST['serial_cn'] = $cNote['id'];
			include(DOCUMENT_ROOT.'modules/creditNote/pPrintFirstCreditNote.php');
		}else{
			echo 'Error al Actualizar la vieja nota de cr&eacute;dito.';
		}
	}else{
		echo 'Error al insertar la nueva nota de cr&eacute;dito.';
	}
}else{
	http_redirect('modules/creditNote/fPrintCreditNote/1');
}
?>