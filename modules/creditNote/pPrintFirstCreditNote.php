<?php
/*
 * File: pPrintFirstCreditNote.php
 * Author: Patricio Astudillo
 * Creation Date: 10/05/2010, 08:44:20 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 10/05/2010, 08:44:20 AM
 * Modifies By: Nicolas Flores
 * Last Modified: 10/05/2010, 08:44:20 AM
 */
Request::setString('0:serial_cn');
if(!$serial_cn){//Validation to confirm that it's the first time to print a credit note or if it comes from the Reprint Credit Note.
	if($_POST['serial_cn']){
		//case is for reprinting with the same credit note number
		$serial_cn = $_POST['serial_cn'];
	}else{
		$serial_cn = $serial_generated_cNote;
	}
}
$cNote = new CreditNote($db, $serial_cn);
$invoice = new Invoice($db);
$misc = array();

//parameters for number of sales an taxes to be shown
$maxSales = 3;
$maxTaxes = 5;

if($cNote -> getData()){
	if($cNote -> getPrinted_cn()=='NO'||$_POST['serial_cn']){
		if($cNote -> getSerial_inv()!=""){//CREDIT NOTE BY VOID INVOICE
			$misc['type']='INVOICE';
			$misc['serial']=$cNote -> getSerial_inv();

			/*GET INVOICE DATA*/
			$invoice -> setSerial_inv($cNote -> getSerial_inv());
			$invoice -> getData();
			
			$tempLog = new InvoiceLog($db, NULL, $cNote -> getSerial_inv());
			$invLogs = $tempLog -> getInvoiceLogs();//TODO THIS SHOULD BE A STATIC CALL!!!
			
			$sale = new Sales($db, $invLogs[0]['serial_sal']);
			$sale -> getData();
			$sale=get_object_vars($sale);
			unset($sale['db']);
		}else if($cNote -> getSerial_ref()!=''){ //CREDIT NOTE BY REFUND
			$misc['type']='REFUND';
			$misc['serial']=$cNote -> getSerial_ref();

			/*GET REFUND DATA*/
			$refund=new Refund($db, $cNote -> getSerial_ref());
			$refund -> getData();
			$refund=get_object_vars($refund);
			unset($refund['db']);
			/*END REFUND*/

			$sale=new Sales($db, $refund['serial_sal']);
			$sale -> getData();
			$sale=get_object_vars($sale);
			unset($sale['db']);

			$cNotePDF['serial_sal']=$sale['serial_sal'];
			$cNotePDF['serial_ref']=$refund['serial_ref'];

			$invoice -> setSerial_inv($sale['serial_inv']);
			$invoice -> getData();
		}else if($cNote ->getSerial_pay() != ''){ //UNCOLLECTABLE CREDIT NOTE
			$misc['type'] = 'PAYMENT';
			$misc['serial'] = $cNote -> getSerial_pay();
			
			/* GET GENERAL DATA DATA */
			$invoice_data = Invoice::getInvoiceByUncollectablePayments($db, $cNote -> getSerial_pay());
			
			$invoice -> setSerial_inv($invoice_data['serial_inv']);
			$invoice -> getData();
			
			$sale = new Sales($db, $invoice_data['serial_sal']);
			$sale -> getData();
			$sale = get_object_vars($sale);
			unset($sale['db']);
			
		}else{ //CREDIT NOTE BY PENALTY
			$sale=new Sales($db, $cNote -> getSerial_sal());
			$sale -> getData();
			$sale=get_object_vars($sale);
			unset($sale['db']);

			$invoice -> setSerial_inv($sale['serial_inv']);
			$invoice -> getData();

			$misc['type']='PENALTY';
			$cNotePDF['serial']=$cNote -> getSerial_sal();
		}

		//GET THE DEALER'S CREDIT DAYS:
		$tempCnt = new Counter($db, $sale['serial_cnt']);
		$tempCnt -> getData();
		$tmpDea = new Dealer($db, $tempCnt -> getSerial_dea());
		$tmpDea -> getData();
		$tmpComm = new Dealer($db, $tmpDea -> getDea_serial_dea());
		$tmpComm -> getData();
		$tmpCred = new CreditDay($db, $tmpComm -> getSerial_cdd());
		$tmpCred -> getData();
		$creditDays = $tmpCred -> getDays_cdd();

		$invoiceAuxData = $invoice -> getAuxData();
		if($invoiceAuxData['type_cus']!=''){
			$misc['person_type']=$invoiceAuxData['type_cus'];
			$customer=new Customer($db, $invoice -> getSerial_cus());
			$customer -> getData();
			$cNotePDF['name']=$customer -> getFirstname_cus().' '.$customer->getLastname_cus();
			$cNotePDF['address'] = $customer -> getAddress_cus();
			$cNotePDF['document'] = $customer -> getDocument_cus();
			$cNotePDF['phone'] = $customer -> getPhone1_cus();
		}else{
			$misc['person_type']='LEGAL_ENTITY';
			$dealer=new Dealer($db, $invoice -> getSerial_dea());
			$dealer -> getData();
			$cNotePDF['name']=$dealer -> getName_dea();
			$cNotePDF['address'] = $dealer -> getAddress_dea();
			$cNotePDF['document'] = $dealer -> getId_dea();
			$cNotePDF['phone'] = $dealer -> getPhone1_dea();
		}

		$cNotePDF['number_inv']=$invoice -> getNumber_inv();
		$cNotePDF['type']=$misc['type'];
		$cNotePDF['serial_inv']=$invoice -> getSerial_inv();
		$cNotePDF['date_inv']=$invoice -> getDate_inv();
		//$cNotePDF['due_date_inv']=$invoice -> getDue_date_inv();
		$cNotePDF['date_cn']=$cNote ->getDate_cn();
		
		//TRICKY strtotime FUNCTION: WE TAKE dmY, convert into Ymd to be processed by strtotime and after that get back to dmY:
		//$cNotePDF['due_date_inv'] = date("d/m/Y",strtotime(date("Y/m/d",GlobalFunctions::strToTimeDdMmYyyy($cNotePDF['date_cn'])) . ' +' . $creditDays . ' days'));
		/*End PDF Info*/

		/*********************************** BEGIN PDF **************************************************************/
		//HEADER
		$pdf = new Cezpdf('A5','landscape');
		$pdf -> selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
		$pdf -> ezSetMargins(80,50,60,50);
		
		$description=array();
		$description[0]=array('col1'=>'<b>Factura a: </b>',
							  'col2'=>$cNotePDF['name'],
							  'col3'=>'<b>RUC/CI: </b>',
							  'col4'=>$cNotePDF['document']);
		$description[1]=array('col1'=>'<b>Fecha: </b>',
							  'col2'=>$cNotePDF['date_cn'],
							  'col3'=>utf8_decode(html_entity_decode('<b>Tel&eacute;fono: </b>')),
							  'col4'=>$cNotePDF['phone']);
		$description[2]=array('col1'=>utf8_decode(html_entity_decode('<b>Direcci&oacute;n: </b>')),
							  'col2'=>$cNotePDF['address']);
//							  'col3'=>'<b>Factura vence: </b>',
//							  'col4'=>$cNotePDF['due_date_inv']);

		$options = array(
				'showLines'=>0,
				'showHeadings'=>0,
				'shaded'=>0,
				'fontSize' => 8,
				'titleFontSize' => 8,
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 550,
				'cols' =>array(
					  'col1'=>array('justification'=>'left','width'=>80),
					  'col2'=>array('justification'=>'left','width'=>160),
					  'col3'=>array('justification'=>'left','width'=>80),
					  'col4'=>array('justification'=>'left','width'=>130)
				),
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
		);
		
		$pdf -> ezSetDy(-45);
		$pdf -> ezTable($description, array('col1'=>'Col1','col2'=>'Col2','col3'=>'Col3','col4'=>'Col4'), NULL, $options);
		//END HEADERS

		//BODY
		$totals=array();
		$totals[0]['col1'] = 'Suman:';
		$totals[0]['col2'] = 0;

		if($cNotePDF['type']=='INVOICE'){
			$inLog=new InvoiceLog($db);

			$salesRaw=$inLog -> getVoidInvoiceSales($invoice -> getSerial_inv());
			
			//ITERATE TO ADD THE CREDIT NOTE # TO THE NAME:
			if(is_array($salesRaw)){
				foreach($salesRaw as &$row){
					$row['customer_name'] .= ' / CN# ' . $cNote -> getNumber_cn();
				}
			}

			/**** CALCULATING ALL THE DATA ****/
			$numberOfSales=sizeof($salesRaw);
			//calculate sales Total
			if($salesRaw){
				$taxes=unserialize($salesRaw[0]['applied_taxes_inv']);
				$discountPercentage+=$salesRaw[0]['discount_prcg_inv'];
				$otherDiscountPercentage+=$salesRaw[0]['other_dscnt_inv'];
				foreach($salesRaw as &$sal){
					$salesTotal+=$sal['total_sal'];
					unset($sal['discount_prcg_inv']);
					unset($sal['other_dscnt_inv']);
					unset($sal['applied_taxes_inv']);
					unset($sal['international_fee_inv']);
				}
			}
			//calculate discounts and subtotal
			$discount=$salesTotal*($discountPercentage/100);
			$otherDiscount=$salesTotal*($otherDiscountPercentage/100);
			$subtotal=$salesTotal-($discount+$otherDiscount);
			//calculate taxes
			if($taxes){
				foreach($taxes as $tx){
				$taxesPercentage+=$tx['tax_percentage'];
				}
			}
			$taxesTotal=$subtotal*($taxesPercentage/100);
			$invoiceTotal=$subtotal+$taxesTotal;
			/**** END CALCULATING ****/

			/*WRITE THE INVOICE TABLE*/
			$invoiceTable = array();
			if($numberOfSales > $maxSales){
				$tableLine1 = array('card_number_sal'=>'',
								 'customer_name'=>utf8_decode('Servicio de asistencia en viaje según detalle adjunto.'),
								 'name_pbl'=>'',
								 'total_sal'=>'');
				array_push($invoiceTable,$tableLine1);
			}else{
				$invoiceTable = $salesRaw;
			}

			$pdf -> ezSetDy(-10);
			$pdf -> ezTable($invoiceTable,
				  array('card_number_sal'=>'<b>Tarjeta No</b>','customer_name'=>'<b>Nombre</b>','name_pbl'=>'<b>Tipo Tarjeta</b>','total_sal'=>'<b>Valor</b>'),
				  '',
				  array('showHeadings'=>1,
						'shaded'=>0,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'card_number_sal'=>array('justification'=>'center','width'=>70),
							'customer_name'=>array('justification'=>'center','width'=>220),
							'name_pbl'=>array('justification'=>'center','width'=>130),
							'total_sal'=>array('justification'=>'right','width'=>80))));
			/*END WRITE TABLE*/
			
			//giving format for numbers
			$salesTotal=number_format($salesTotal, 2, '.', '');
			$discount=number_format($discount, 2, '.', '');
			$otherDiscount=number_format($otherDiscount, 2, '.', '');
			$subtotal=number_format($subtotal, 2, '.', '');
			$taxesTotal=number_format($taxesTotal, 2, '.', '');
			$invoiceTotal=number_format($invoiceTotal, 2, '.', '');
			$discountPercentage=number_format($discountPercentage, 2, '.', '');
			$otherDiscountPercentage=number_format($otherDiscountPercentage, 2, '.', '');
			$taxesPercentage=number_format($taxesPercentage, 2, '.', '');

			$invoiceValues=array();
			$valuesLine1=array('col1'=>'<b>Suman:</b>',
							 'col2'=>$salesTotal);
			array_push($invoiceValues,$valuesLine1);
			$valuesLine2=array('col1'=>'<b>Descuento('.$discountPercentage.'%):</b>',
							 'col2'=>$discount);
			array_push($invoiceValues,$valuesLine2);
			if($otherDiscount>0){
				$valuesLine3=array('col1'=>'<b>Otro Descuento('.$otherDiscountPercentage.'%):</b>',
							 'col2'=>$otherDiscount);
				array_push($invoiceValues,$valuesLine3);
			}
			$valuesLine4=array('col1'=>'<b>Subtotal:</b>',
							 'col2'=>$subtotal);
			array_push($invoiceValues,$valuesLine4);
			//check if there are taxes and if they are more than 2 taxes applied
			if($taxes){
				if(sizeof($taxes)>$maxTaxes){
					$valuesLine5=array('col1'=>'<b>Impuestos('.$taxesPercentage.'%):</b>',
									 'col2'=>$taxesTotal);
					array_push($invoiceValues,$valuesLine5);
					$taxesText='';
					foreach($taxes as $t){
						$taxesText.="{$t['tax_name']}:{$t['tax_percentage']}%, ";
					}
					$taxesText=substr($taxesText,0,strlen($taxesText)-2);
				}else{
					foreach($taxes as $t){
						$$t['tax_name']=array('col1'=>'<b>'.$t['tax_name'].'('.$t['tax_percentage'].'%):</b>',
									 'col2'=>number_format(($subtotal*($t['tax_percentage']/100)),2, '.',''));
						array_push($invoiceValues,$$t['tax_name']);
					}
				}
			}else{
				$valuesLine5=array('col1'=>'<b>Impuestos(0%):</b>',
									 'col2'=>'0.00');
					array_push($invoiceValues,$valuesLine5);
			}
			//Debug::print_r($taxes);
			$valuesLine6=array('col1'=>'<b>Valor Total:</b>',
							 'col2'=>$invoiceTotal);
			array_push($invoiceValues,$valuesLine6);

			$pdf -> ezTable($invoiceValues,
							  array('col1'=>'','col2'=>''),
							  '',
							  array('showHeadings'=>0,
									'shaded'=>0,
									'showLines'=>2,
									'xPos'=>342.5,
									'xOrientation'=>'right',
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'fontSize' => 8,
									'titleFontSize' => 8,
									'cols'=>array(
										'col1'=>array('justification'=>'right','width'=>130),
										'col2'=>array('justification'=>'right','width'=>80))));

			if($taxesText){
				$pdf -> ezSetDy(-5);
				$pdf -> ezText('Impuestos Aplicados: '.$taxesText, '', array('justification' =>'left'));
			}
			$pdf -> ezSetDy(-5);
		
			$textFooter = utf8_decode(html_entity_decode('Nota de Cr&eacute;dito de por Anulaci&oacute;n de Factura / Factura# ' . $invoice -> getNumber_inv()));
			$pdf -> ezText($textFooter, 8 ,array('justification' =>'center'));
			
			if($numberOfSales>$maxSales){
			//invoice detail
				$pdf -> ezNewPage();
				$pdf -> ezStartPageNumbers(545,40,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
				$pdf -> ezTable($salesRaw,
							  array('card_number_sal'=>'<b>Tarjeta No</b>','customer_name'=>'<b>Nombre</b>','name_pbl'=>'<b>Tipo Tarjeta</b>','total_sal'=>'<b>Valor</b>'),
							  '<b>DETALLE DE LA FACTURA</b>',
							  array('fontSize' => 8,
									'titleFontSize' => 8,
								  'cols'=>array(
										'card_number_sal'=>array('justification'=>'center','width'=>70),
										'customer_name'=>array('justification'=>'center','width'=>200),
										'name_pbl'=>array('justification'=>'center','width'=>150),
										'total_sal'=>array('justification'=>'center','width'=>80)),
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'xPos'=>'center',
									'xOrientation'=>'center'));
				$total=array();
				$valuesTotal=array('col1'=>'<b>Suman:</b>',
							 'col2'=>$salesTotal);
				array_push($total,$valuesTotal);

				$pdf -> ezTable($total,
							  array('col1'=>'','col2'=>''),
							  '',
							  array('showHeadings'=>0,
									'shaded'=>0,
									'showLines'=>2,
									'xPos'=>322.5,
									'xOrientation'=>'right',
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'fontSize' => 8,
									'titleFontSize' => 8,
									'cols'=>array(
										'col1'=>array('justification'=>'right','width'=>150),
										'col2'=>array('justification'=>'center','width'=>80))));
			}
			//$pdf -> ezStopPageNumbers();
		}else if($cNotePDF['type']=='REFUND'){//If it's a REFUND credit note
			$sale=new Sales($db, $cNotePDF['serial_sal']);
			$sale -> getData();
			$customer=new Customer($db, $sale -> getSerial_cus());
			$customer -> getData();
			$cutomer_name=$customer -> getFirstname_cus().' '.$customer -> getLastname_cus();
			$card_number=$sale -> getCardNumber_sal();
			$refund=new Refund($db, $cNotePDF['serial_ref']);
			$refund -> getData();
			$dealer_info=Counter::getCountersDealer($db, $sale -> getSerial_cnt());

			/*WRITE THE SALE INFO*/
			$salesRaw = $invoice -> getInvoiceSales($invoice -> getSerial_inv(), $sale -> getSerial_sal());
			
			//ITERATE TO ADD THE CREDIT NOTE # TO THE NAME:
			if(is_array($salesRaw)){
				foreach($salesRaw as &$row){
					$row['name'] .= ' / CN# ' . $cNote -> getNumber_cn();
				}
			}
			
			
			$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

			$pdf -> ezSetDy(-10);
			$pdf -> ezTable($salesRaw, array(
										'card_number_sal'=>'<b>Tarjeta No.</b>',
										'name'=>'<b>Nombre</b>',
										'name_pbl'=>'<b>Tipo de Tarjeta</b>',
										'total_sal'=>'<b>Valor</b>'),
							NULL, $options);
			/*END THE WRITING*/

			/**** CALCULATING ALL THE VALUES***/
			$total_sal = $sale -> getTotal_sal();
			$amountsPaid = $cNote -> getPenalty_fee_cn();
			
			$discount=$total_sal*($invoice -> getDiscount_prcg_inv()/100);
			$other_discount=$total_sal*($invoice -> getOther_dscnt_inv()/100);
			//Penalty
			if($refund -> getRetainedType_ref()=='PERCENTAGE'){
				$penalty= $total_sal*($refund -> getRetainedValue_ref()/100);
				$penalty_label='Penalidad ('.$refund -> getRetainedValue_ref().'%):';
			}else{
				$penalty=$refund -> getRetainedValue_ref();
				$penalty_label = 'Penalidad (Valor Fijo):';
			}

			$sub_total=$total_sal-$amountsPaid-$discount-$other_discount-$penalty;
			//Taxes
			$taxes_fee=0;
			$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
			if(is_array($taxesArr)){
				foreach($taxesArr as $t){
						$taxes_fee+=$t['tax_percentage'];
				}
			}
			$taxes_amount=($taxes_fee*$sub_total)/100;
			$total_refund=$taxes_amount+$sub_total;

			//Formating the numbers
				$total_sal=number_format($total_sal, 2, '.', '');
				$amountsPaid=number_format($amountsPaid, 2, '.', '');
				$discount=number_format($discount, 2, '.', '');
				$other_discount=number_format($other_discount, 2, '.', '');
				$penalty=number_format($penalty, 2, '.', '');
				$sub_total=number_format($sub_total, 2, '.', '');
				$taxes_amount=number_format($taxes_amount, 2, '.', '');
				$total_refund=number_format($total_refund, 2, '.', '');
			//End Formating
			/**** END CALCULATING****/

			/*WRITE TOTALS TABLE*/
				$totals[0]['col2'] = $sale -> getTotal_sal();

				$line_1=array('col1'=>'Deducciones:',
							  'col2'=>$amountsPaid);
				array_push($totals,$line_1);

				$line_2=array('col1'=>'Descuento ('.$invoice -> getDiscount_prcg_inv().'%):',
							  'col2'=>$discount);
				array_push($totals,$line_2);

				$line_3=array('col1'=>'Otro descuento ('.$invoice -> getOther_dscnt_inv().'%):',
							  'col2'=>$other_discount);
				array_push($totals,$line_3);
				
				$line_4=array('col1'=>$penalty_label,
							  'col2'=>$penalty);
				array_push($totals,$line_4);

				$line_5=array('col1'=>'Subtotal:',
							  'col2'=>$sub_total);
				array_push($totals,$line_5);
				
				if($taxesArr){
					if(sizeof($taxesArr)>$maxTaxes){
						$valuesLine5=array('col1'=>'<b>Impuestos('.$taxes_fee.'%):</b>',
										   'col2'=>$taxes_amount);
						array_push($invoiceValues,$valuesLine5);
						$taxesText='';
						foreach($taxesArr as $t){
							$taxesText.="{$t['tax_name']}:{$t['tax_percentage']}%, ";
						}
						$taxesText=substr($taxesText,0,strlen($taxesText)-2);
					}else{
						foreach($taxesArr as $t){
							$$t['tax_name']=array('col1'=>$t['tax_name'].'('.$t['tax_percentage'].'%):',
										          'col2'=>number_format(($sub_total*($t['tax_percentage']/100)),2, '.',''));
							array_push($totals,$$t['tax_name']);
						}
					}
				}else{
					$line_6=array('col1'=>'<b>Impuestos(0%):</b>',
								  'col2'=>'0.00');
					array_push($totals,$line_6);
				}

				$line_7=array('col1'=>'Valor Total:',
							  'col2'=>$total_refund);
				array_push($totals,$line_7);

				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75.5)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf -> ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				if($taxesText){
					$pdf -> ezSetDy(-5);
					$pdf -> ezText('Impuestos Aplicados: '.$taxesText, '', array('justification' =>'left'));
				}

				$pdf -> ezSetDy(-5);
				
				if($refund->getReason_ref() == 'SPECIAL'):
					$special_ref_text = 'Especial ';
				endif;
				
				$textFooter = utf8_decode(html_entity_decode("Nota de Cr&eacute;dito por Reembolso {$special_ref_text}/ Factura# " . $invoice -> getNumber_inv()));
				$pdf -> ezText($textFooter, 8, array('justification' =>'center'));
                                /**** END TOTALS TABLE ****/
		
		}else if($cNotePDF['type'] == 'PAYMENT'){ //UNCOLLECTABLE CREDIT NOTE
			$inv = new Invoice($db);
			$sales = $inv->getInvoiceSales($cNotePDF['serial_inv']);

			//ITERATE TO ADD THE CREDIT NOTE # TO THE NAME:
			if(is_array($sales)){
				foreach($sales as &$row){
					$row['name'] .= ' / CN# ' . $cNote -> getNumber_cn();
				}
			}
			
			/**** CALCULATING ALL THE DATA ****/
			$numberOfSales = sizeof($sales);
			
			//calculate sales Total
			if($sales){
				$taxes = unserialize($sales[0]['applied_taxes_inv']);
				$discountPercentage += $sales[0]['discount_prcg_inv'];
				$otherDiscountPercentage += $sales[0]['other_dscnt_inv'];
				
				foreach($sales as &$sal){
					$salesTotal += $sal['total_sal'];
					unset($sal['discount_prcg_inv']);
					unset($sal['other_dscnt_inv']);
					unset($sal['applied_taxes_inv']);
					unset($sal['international_fee_inv']);
				}
			}
			
			//calculate discounts and subtotal
			$discount = $salesTotal * ($discountPercentage / 100);
			$otherDiscount = $salesTotal * ($otherDiscountPercentage / 100);
			$subtotal = $salesTotal - ($discount + $otherDiscount);
			
			//calculate taxes
			if ($taxes) {
				foreach ($taxes as $tx) {
					$taxesPercentage+=$tx['tax_percentage'];
				}
			}
			
			$taxesTotal = $subtotal * ($taxesPercentage / 100);
			$invoiceTotal = $subtotal + $taxesTotal;
			/**** END CALCULATING ****/

			/* WRITE THE INVOICE TABLE */
			$invoiceTable = array();
			if ($numberOfSales > $maxSales) {
				$tableLine1 = array('card_number_sal' => '',
					'name' => utf8_decode(html_entity_decode('Servicio de asistencia en viaje seg&uacute;n detalle adjunto.')),
					'name_pbl' => '',
					'total_sal' => '');
				array_push($invoiceTable, $tableLine1);
			} else {
				$invoiceTable = $sales;
			}

			$pdf -> ezSetDy(-10);
			$pdf -> ezTable($invoiceTable,
				  array('card_number_sal'=>'<b>Tarjeta No</b>',
						'name'=>'<b>Nombre</b>',
						'name_pbl'=>'<b>Tipo Tarjeta</b>',
						'total_sal'=>'<b>Valor</b>'),
				  '',
				  array('showHeadings'=>1,
						'shaded'=>0,
						'showLines'=>2,
						'xPos'=>'center',
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'card_number_sal'=>array('justification'=>'center','width'=>70),
							'name'=>array('justification'=>'center','width'=>220),
							'name_pbl'=>array('justification'=>'center','width'=>130),
							'total_sal'=>array('justification'=>'right','width'=>80))));
			/*END WRITE TABLE*/
			
			//giving format for numbers
			$salesTotal = number_format($salesTotal, 2, '.', '');
			$discount = number_format($discount, 2, '.', '');
			$otherDiscount = number_format($otherDiscount, 2, '.', '');
			$discountPercentage = number_format($discountPercentage, 2, '.', '');
			$otherDiscountPercentage = number_format($otherDiscountPercentage, 2, '.', '');
			$subtotal = number_format($subtotal, 2, '.', '');
			$taxesTotal = number_format($taxesTotal, 2, '.', '');
			$taxesPercentage = number_format($taxesPercentage, 2, '.', '');
			$prepaid_amount = number_format(PaymentDetail::getPreDepositsForUncollectablePayments($db, $misc['serial']), 2, '.', '');
			$invoiceTotal = number_format($cNote->getAmount_cn(), 2, '.', '');

			$invoiceValues = array();
			$valuesLine1 = array('col1' => '<b>Suman:</b>',
				'col2' => $salesTotal);
			array_push($invoiceValues, $valuesLine1);
			$valuesLine2 = array('col1' => '<b>Descuento(' . $discountPercentage . '%):</b>',
				'col2' => $discount);
			array_push($invoiceValues, $valuesLine2);
			if ($otherDiscount > 0) {
				$valuesLine3 = array('col1' => '<b>Otro Descuento(' . $otherDiscountPercentage . '%):</b>',
					'col2' => $otherDiscount);
				array_push($invoiceValues, $valuesLine3);
			}
			$valuesLine4 = array('col1' => '<b>Subtotal:</b>',
				'col2' => $subtotal);
			array_push($invoiceValues, $valuesLine4);
			//check if there are taxes and if they are more than 2 taxes applied
			if ($taxes) {
				if (sizeof($taxes) > $maxTaxes) {
					$valuesLine5 = array('col1' => '<b>Impuestos(' . $taxesPercentage . '%):</b>',
						'col2' => $taxesTotal);
					array_push($invoiceValues, $valuesLine5);
					$taxesText = '';
					foreach ($taxes as $t) {
						$taxesText.="{$t['tax_name']}:{$t['tax_percentage']}%, ";
					}
					$taxesText = substr($taxesText, 0, strlen($taxesText) - 2);
				} else {
					foreach ($taxes as $t) {
						$$t['tax_name'] = array('col1' => '<b>' . $t['tax_name'] . '(' . $t['tax_percentage'] . '%):</b>',
							'col2' => number_format(($subtotal * ($t['tax_percentage'] / 100)), 2, '.', ''));
						array_push($invoiceValues, $$t['tax_name']);
					}
				}
			} else {
				$valuesLine5 = array('col1' => '<b>Impuestos(0%):</b>',
					'col2' => '0.00');
				array_push($invoiceValues, $valuesLine5);
			}
			
			$valuesLine6 = array('col1' => '<b>Abonos:</b>',
								 'col2' => $prepaid_amount);
			array_push($invoiceValues, $valuesLine6);
			
			$valuesLine7 = array('col1' => '<b>Valor Total:</b>',
								 'col2' => $invoiceTotal);
			array_push($invoiceValues, $valuesLine7);

			$pdf->ezTable($invoiceValues, array('col1' => '', 'col2' => ''), '', array('showHeadings' => 0,
				'shaded' => 0,
				'showLines' => 2,
				'xPos' => 342.5,
				'xOrientation' => 'right',
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8,
				'fontSize' => 8,
				'titleFontSize' => 8,
				'cols' => array(
					'col1' => array('justification' => 'right', 'width' => 130),
					'col2' => array('justification' => 'right', 'width' => 80))));

			if ($taxesText) {
				$pdf->ezSetDy(-5);
				$pdf->ezText('Impuestos Aplicados: ' . $taxesText, '', array('justification' => 'left'));
			}
			$pdf->ezSetDy(-5);

			$textFooter = utf8_decode(html_entity_decode('Nota de Cr&eacute;dito por Anulaci&oacute;n Especial / Factura # ' . $invoice->getNumber_inv()));
			$pdf->ezText($textFooter, 8, array('justification' => 'center'));

			if ($numberOfSales > $maxSales) {
				//invoice detail
				$pdf->ezNewPage();
				$pdf->ezStartPageNumbers(545, 40, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);
				$pdf->ezTable($sales, array(	'card_number_sal' => '<b>Tarjeta No</b>', 
												'name' => '<b>Nombre</b>', 
												'name_pbl' => '<b>Tipo Tarjeta</b>', 
												'total_sal' => '<b>Valor</b>'), 
							'<b>DETALLE DE LA FACTURA</b>', 
							array(	'fontSize' => 8,
									'titleFontSize' => 8,
									'cols' => array(
										'card_number_sal' => array('justification' => 'center', 'width' => 70),
										'name' => array('justification' => 'center', 'width' => 200),
										'name_pbl' => array('justification' => 'center', 'width' => 150),
										'total_sal' => array('justification' => 'center', 'width' => 80)),
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'xPos' => 'center',
									'xOrientation' => 'center'));
				$total = array();
				$valuesTotal = array(	'col1' => '<b>Suman:</b>',
										'col2' => $salesTotal);
				array_push($total, $valuesTotal);

				$pdf->ezTable($total, array('col1' => '', 'col2' => ''), '', array('showHeadings' => 0,
					'shaded' => 0,
					'showLines' => 2,
					'xPos' => 322.5,
					'xOrientation' => 'right',
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8,
					'fontSize' => 8,
					'titleFontSize' => 8,
					'cols' => array(
						'col1' => array('justification' => 'right', 'width' => 150),
						'col2' => array('justification' => 'center', 'width' => 80))));
			}
				
		}else{ //PENALTY
			$sale=new Sales($db, $cNotePDF['serial']);
			$sale -> getData();

			$customer=new Customer($db, $sale -> getSerial_cus());
			$customer -> getData();
			$cutomer_name=$customer -> getFirstname_cus().' '.$customer -> getLastname_cus();
			$card_number=$sale -> getCardNumber_sal();

			/*WRITE THE SALE INFO*/
			$salesRaw=$invoice -> getInvoiceSales($invoice -> getSerial_inv(), $sale -> getSerial_sal());
			
			//ITERATE TO ADD THE CREDIT NOTE # TO THE NAME:
			if(is_array($salesRaw)){
				foreach($salesRaw as &$row){
					$row['name'] .= ' / CN# ' . $cNote -> getNumber_cn();
				}
			}
			
			$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

			$pdf -> ezSetDy(-10);
			$pdf -> ezTable($salesRaw, array(
										'card_number_sal'=>'<b>Tarjeta No.</b>',
										'name'=>'<b>Nombre</b>',
										'name_pbl'=>'<b>Tipo de Tarjeta</b>',
										'total_sal'=>'<b>Valor</b>'),
							NULL, $options);
			/*END THE WRITING*/

			/**** CALCULATING ALL THE VALUES***/
			$penalty=$cNote -> getPenalty_fee_cn();
			$total_sal=$sale -> getTotal_sal();
			$discount=$total_sal*($invoice -> getDiscount_prcg_inv()/100);
			$other_discount=$total_sal*($invoice -> getOther_dscnt_inv()/100);

			$sub_total=$total_sal-$discount-$other_discount-$penalty;

			//Taxes
			$taxes_fee=0;
			$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
			if(is_array($taxesArr)){
				foreach($taxesArr as $t){
						$taxes_fee+=$t['tax_percentage'];
				}
			}
			$taxes_amount=($taxes_fee*$sub_total)/100;
			$totalCN=$taxes_amount+$sub_total;

			//Formating the numbers
				$total_sal=number_format($total_sal, 2, '.', '');
				$discount=number_format($discount, 2, '.', '');
				$other_discount=number_format($other_discount, 2, '.', '');
				$sub_total=number_format($sub_total, 2, '.', '');
				$taxes_amount=number_format($taxes_amount, 2, '.', '');
				$totalCN=number_format($totalCN, 2, '.', '');
				$penalty=number_format($penalty, 2, '.', '');
			//End Formating
			/**** END CALCULATING****/

			/*WRITE TOTALS TABLE*/
				$totals[0]['col2'] = $sale -> getTotal_sal();
				$line_1=array('col1'=>'Descuento ('.$invoice -> getDiscount_prcg_inv().'%):',
							  'col2'=>$discount);
				array_push($totals,$line_1);

				$line_2=array('col1'=>'Otro descuento ('.$invoice -> getOther_dscnt_inv().'%):',
							  'col2'=>$other_discount);
				array_push($totals,$line_2);
				
				$line_6=array('col1'=>'Penalidad:',
							  'col2'=>$penalty);
				array_push($totals,$line_6);

				$line_3=array('col1'=>'Subtotal:',
							  'col2'=>$sub_total);
				array_push($totals,$line_3);
				
				if($taxesArr){
					if(sizeof($taxesArr)>$maxTaxes){
						$valuesLine4=array('col1'=>'<b>Impuestos('.$taxes_fee.'%):</b>',
										   'col2'=>$taxes_amount);
						array_push($invoiceValues,$valuesLine4);
						$taxesText='';
						foreach($taxesArr as $t){
							$taxesText.="{$t['tax_name']}:{$t['tax_percentage']}%, ";
						}
						$taxesText=substr($taxesText,0,strlen($taxesText)-2);
					}else{
						foreach($taxesArr as $t){
							$$t['tax_name']=array('col1'=>$t['tax_name'].'('.$t['tax_percentage'].'%):',
										          'col2'=>number_format(($sub_total*($t['tax_percentage']/100)),2, '.',''));
							array_push($totals,$$t['tax_name']);
						}
					}
				}else{
					$line_5=array('col1'=>'<b>Impuestos(0%):</b>',
								  'col2'=>'0.00');
					array_push($totals,$line_5);
				}

				$line_7=array('col1'=>'Valor Total:',
							  'col2'=>$totalCN);
				array_push($totals,$line_7);

				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75.5)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf -> ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				if($taxesText){
					$pdf -> ezSetDy(-5);
					$pdf -> ezText('Impuestos Aplicados: '.$taxesText, '', array('justification' =>'left'));
				}

				$pdf -> ezSetDy(-5);
				
				$textFooter = utf8_decode(html_entity_decode('Nota de Cr&eacute;dito por Penalidad / Factura# ' . $invoice -> getNumber_inv()));
				$pdf -> ezText($textFooter, 8, array('justification' =>'center'));
			/**** END TOTALS TABLE ****/
		}
		//END BODY

		$cNote -> setPrinted_cn('YES');
		$cNote -> update();
		$pdf -> ezStream();
	}else{
		http_redirect('main/creditNote/2');
	}
}else{
	http_redirect('main/creditNote/1');
}
?>