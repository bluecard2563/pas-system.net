<?php
/*
  File: pUpdateAssistance
  Author: David Bergmann
  Creation Date: 20/04/2010
  Last Modified: 28/12/2010
  Modified By: David Bergmann
 */

	$_SESSION['card_number'] = $_POST['hdnCardNumber'];

	$file = new File($db, $_POST['hdnSerialFle']);
	$file -> getData();
	$file -> setSerial_cit($_POST['selCity']);
	$file -> setDiagnosis_fle($_POST['txtDiagnosis']);
	$file -> setCause_fle($_POST['txtCause']);
	$file -> setType_fle($_POST['txtAssistanceType']);
	$file->setContanct_info_fle(mysql_real_escape_string($file->getContanct_info_fle()));

	if ($_POST['txtIncidentDate']) {
		$file -> setIncident_date_fle($_POST['txtIncidentDate']);
	}

	if ($file -> update()) {
		//Update services medical type
		if ($_POST['selMedicalType']) {
			$file -> updateFileMedicalType($_POST['selMedicalType']);
		}

		$customer = new Customer($db, $file -> getSerial_cus());
		$customer -> getData();
		$customer -> setMedical_background_cus($_POST['txtMedicalBackground']);
		$customer -> update();

		$blog = new Blog($db);
		$blog -> setSerial_fle($_POST['hdnSerialFle']);
		$blog -> setType_blg("CLIENT");
		$blog -> setEntry_blg($_POST['txtClientBlog']);

		if ($blog -> update()) {
			$blog -> setType_blg("OPERATOR");
			$blog -> setEntry_blg($_POST['txtOperatorBlog']);

			if ($blog -> update()) {
				if ($_POST['hdnStatusFile'] == "first_open") {
					/* CHECK INVOICE */
					/* IF THE SALE HASN'T BEEN PAID AND ITS OUT OF CREDIT DAYS */
					/* CREATES A NEW APPLIANCE */
					/* OTHERWISE SENDS INFORMATION EMAIL */
					$sale = new Sales($db, $file -> getSerial_sal());
					$sale -> getData();
					//verifies if the sale has an invoice
					$invoice = new Invoice($db, $sale -> getSerial_inv());
					$invoice -> getData();

					if ($invoice -> getStatus_inv() == "STAND-BY" && $invoice -> daysToDueDate() < 0) {
						createTemporaryCustomer($db, $file);
					} else {
						sendMail($db, $file, $_POST['hdnStatusFile']);
					}
					/* END CHECK INVOICE */

					
					http_redirect('modules/assistance/fEditAssistance/1');
				} else {
					sendMail($db, $file, $_POST['hdnStatusFile']);
					http_redirect('modules/assistance/fEditAssistance/3');
				}
			} else {
				ErrorLog::log($db, 'ASSISTANCE ERROR', 'OPERATOR BLOG UPDATE FAILED');
				http_redirect('modules/assistance/fEditAssistance/2');
			}
		 } else {
			ErrorLog::log($db, 'ASSISTANCE ERROR', 'CLIENT BLOG UPDATE FAILED');
			http_redirect('modules/assistance/fEditAssistance/2');
		}
	} else {
		ErrorLog::log($db, 'ASSISTANCE ERROR', 'FILE UPDATE FAILED');
		http_redirect('modules/assistance/fEditAssistance/2');
	}

	/**
	 * @name createTemporaryCustomer
	 * @param <File> $file
	 * @uses creates a temporary customer and sends a notification e-mail
	 */
	function createTemporaryCustomer($db, $file) {
		$sale = new Sales($db, $file -> getSerial_sal());
		$sale -> getData();
		$customer = new Customer($db, $file -> getSerial_cus());
		$customer -> getData();
		$city = new City($db, $customer -> getSerial_cit());
		$city -> getData();
		$city = new City($db, $sale -> getSerial_cit());
		$city -> getData();
		$dealer = Dealer::getDealerByCardNumber($db, $sale -> getCardNumber_sal());

		$temporary_customer = new TemporaryCustomer($db);
		$temporary_customer -> setSerial_cou($city -> getSerial_cou());
		$temporary_customer -> setSerial_fle($file -> getSerial_fle());
		$temporary_customer -> setCou_serial_cou($city -> getSerial_cou());
		$temporary_customer -> setSerial_usr($_SESSION['serial_usr']);
		$temporary_customer -> setDocument_tcu($customer -> getDocument_cus());
		$temporary_customer -> setFirst_name_tcu($customer -> getFirstname_cus());
		$temporary_customer -> setLast_name_tcu($customer -> getLastname_cus());
		$temporary_customer -> setBirthdate_tcu($customer -> getBirthdate_cus());
		$temporary_customer -> setAgency_name_tcu($dealer[0]['name_dea']);
		$temporary_customer -> setPhone_tcu($customer -> getPhone1_cus());
		$temporary_customer -> setContact_phone_tcu($customer -> getCellphone_cus());
		$temporary_customer -> setContact_address_tcu($customer -> getAddress_cus());
		$temporary_customer -> setCard_number_tcu($sale -> getCardNumber_sal());
		//$temporary_customer -> setExtra_tcu(utf8_decode($blog -> getEntry_blg()));
		$temporary_customer -> setProblem_description_tcu($file -> getDiagnosis_fle() . utf8_decode(" (Venta no pagada y fuera de días de crédito)"));
		$temporary_customer -> setStatus_tcu('REQUESTED');

		/* E-MAIL INFO */
		$misc['textInfo'] = "Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s o ingrese a <a href='" . URL . "' target='blank'>PAS</a>.";
		//Customer and agency information
		$country = new Country($db, $temporary_customer -> getSerial_cou());
		$country -> getData();
		$misc['country'] = $country -> getName_cou();
		$misc['agency'] = $temporary_customer -> getAgency_name_tcu();
		$country = new Country($db, $temporary_customer -> getCou_serial_cou());
		$country -> getData();
		$misc['travelCountry'] = $country -> getName_cou();
		$misc['document'] = $temporary_customer -> getDocument_tcu();
		$misc['name'] = $temporary_customer -> getFirst_name_tcu() . " " . $temporary_customer -> getLast_name_tcu();
		$misc['birthDate'] = $temporary_customer -> getBirthdate_tcu();
		$misc['phone'] = $temporary_customer -> getPhone_tcu();
		$misc['contactAddress'] = $temporary_customer -> getContact_address_tcu();
		$misc['contactPhone'] = $temporary_customer -> getContact_phone_tcu();
		$misc['cardNumber'] = $temporary_customer -> getCard_number_tcu();
		$misc['extra'] = $temporary_customer -> getExtra_tcu();
		$misc['problem'] = $temporary_customer -> getProblem_description_tcu();
		//Card Information
		$misc['textForEmail'] = "Informaci&oacute;n de la tarjeta";
		$misc['cardInfo'] = true;
		$misc['emission'] = $sale -> getEmissionDate_sal();
		$misc['begin'] = $sale -> getBeginDate_sal();
		$misc['end'] = $sale -> getEndDate_sal();
		$misc['status'] = $sale -> getStatus_sal();
		$user = new User($db, $_SESSION['serial_usr']);
		$user -> getData();
		$misc['user'] = $user -> getFirstname_usr() . " " . $user -> getLastname_usr();
		
		$counter = new Counter($db, $sale -> getSerial_cnt());
		$counter -> getData();

		$parameter = new Parameter($db, '34'); //Parameters for Liquidation Permitions.
		$parameter -> getData();
		$assistanceEmails = unserialize($parameter -> getValue_par());
		$misc['email'] = $assistanceEmails['request'];

		if (!$temporary_customer -> insert()) {
			http_redirect('modules/assistance/fEditAssistance/5');
		// } elseif (!GlobalFunctions::sendMail($misc, 'newTemporaryCustomer')) {
		// 	http_redirect('modules/assistance/fEditAssistance/6');
		} else {
			return;
		}
	}

	/**
	 * @name sendMail
	 * @param <File> $file
	 * @uses Sends a notification e-mail
	 */
	function sendMail($db, $file, $status) {
		/* INFORMATION E-MAIL INFO */
		$sale = new Sales($db, $file -> getSerial_sal());
		$sale -> getData();
		$customer = new Customer($db, $file -> getSerial_cus());
		$customer -> getData();
		$counter = new Counter($db, $sale -> getSerial_cnt());
		$counter -> getData();
		$dealer = new Dealer($db, $counter -> getSerial_dea());
		$dealer -> getData();
		$sector = new Sector($db, $dealer -> getSerial_sec());
		$sector -> getData();
		$city = new City($db, $sector -> getSerial_cit());
		$city -> getData();
		$country = new Country($db, $city -> getSerial_cou());
		$country -> getData();

		$misc['cardNumber'] = $_POST['hdnCardNumber'];
		$misc['emission'] = $sale -> getEmissionDate_sal();
		$misc['begin_date'] = $sale ->getBeginDate_sal();
		$misc['end_date'] = $sale ->getEndDate_sal();
		$misc['country'] = $country -> getName_cou();
		$misc['status'] = $sale -> getStatus_sal();

		$invoice = new Invoice($db, $sale -> getSerial_inv());
		$invoice -> getData();
		$payment = new Payment($db, $invoice -> getSerial_pay());
		$payment -> getData();

		$misc['payment'] = $payment -> getDate_pay();
		$misc['serialFile'] = $file -> getSerial_fle();
		$misc['customer'] = $customer -> getFirstname_cus() . " " . $customer -> getLastname_cus();
		
		$due_invoice_data = Invoice::getInvoiceDueStatus($db, $invoice->getSerial_inv());

		$productByDealer = new ProductByDealer($db, $sale -> getSerial_pbd());
		$productByDealer -> getData();
		$productByCountry = new ProductByCountry($db, $productByDealer -> getSerial_pxc());
		$productByCountry -> getData();
		$productByLanguage = new ProductbyLanguage($db);
		$productByLanguage -> setSerial_pro($productByCountry -> getSerial_pro());
		$productByLanguage -> setSerial_lang($_SESSION['serial_lang']);
		$productByLanguage -> getDataByProductAndLanguage();
		$misc['product'] = $productByLanguage -> getName_pbl();

		$user = new User($db, $_SESSION['serial_usr']);
		$user -> getData();
		$misc['user'] = $user -> getFirstname_usr() . " " . $user -> getLastname_usr();
		$misc['dealer'] = $dealer -> getName_dea();
		$misc['date'] = $file -> getCreation_date_fle();
		$misc['time'] = date('H:i:s');
		$misc['textInfo'] = "Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s o ingrese a <a href='" . URL . "' target='blank'>PAS</a>.";
		/* END INFORMATION E-MAIL INFO */
		
		//**************** NOTIFICATIONS TO DEALER ****************
		if($dealer->getNotificacions_dea() == 'YES'){
			$dealers_assistance_responsible_mail = $dealer->getEmail_assistance_dea();
		}

		$parameter = new Parameter($db, '34'); //Parameters for Liquidation Permitions.
		$parameter -> getData();
		$assistanceEmails = unserialize($parameter -> getValue_par());

		if ($status == 'first_open') {
			$misc['title'] = $country -> getName_cou()." - INFORMATIVO DE NUEVO CASO";
			$misc['text'] = "Se ha creado el expediente {$file -> getSerial_fle()} para la siguiente tarjeta:";
			$misc['email'] = $assistanceEmails['create'];

			/* SEND MAIL TO DEALER'S RESPONSIBLE */
			$serial_usr = $dealer ->getAuxData_dea();
			$serial_usr = $serial_usr['serial_usr'];
			$user = new User($db, $serial_usr);
			$user -> getData();

			if($user ->getEmail_usr()) array_push ($misc['email'], $user ->getEmail_usr());
			if($dealers_assistance_responsible_mail) array_push ($misc['email'], $dealers_assistance_responsible_mail);
			/* SEND MAIL TO DEALER'S RESPONSIBLE */
			
			//******************** NOTIFICATION FOR DUE INVOICES ASSISTANCES ********************
			if($due_invoice_data){
				$due_notification = array();
				$due_notification['card_number'] = $misc['cardNumber'];
				$due_notification['holder_name'] = $misc['customer'];
				$due_notification['responsible_name'] = $user->getFirstname_usr().' '.$user->getLastname_usr();
				$due_notification['dealer_name'] = $dealer->getName_dea();
				$due_notification['days_due'] = $due_invoice_data['days_due'];
				$due_notification['product_name'] = $misc['product'];
				
				$parameter = new Parameter($db, 49); //Emails for due assistances
				$parameter->getData();
				$due_notification['email'] = explode(',', $parameter->getValue_par());
				
				if (!GlobalFunctions::sendMail($due_notification, 'due_assistance')) {
					ErrorLog::log($db, 'ASSISTANCE NOTIFICACION ERROR', 'Notificacion de asistencia fuera de dias de credito no enviada.');
				}
			}
		} else {
			$misc['title'] = "INFORMATIVO ACTUALIZACIONES EN CASO";
			$misc['text'] = "Se han efectuado actualizaciones en el expediente {$file -> getSerial_fle()} de la siguiente tarjeta:";
			$misc['email'] = $assistanceEmails['update'];
		}

		if (!GlobalFunctions::sendMail($misc, 'newAssistance')) {
			ErrorLog::log($db, 'ASSISTANCE ERROR', 'Envio de mail se cayo. sendMail');
			http_redirect('modules/assistance/fEditAssistance/4');
		}

		//ErrorLog::log($db, 'ASSISTANCE ERROR', 'sendMail - Todo bien');
		return;
	}
?>