<?php

/*
  File: pDispatchAssistanceClaims
  Author: David Bergmann
  Creation Date: 11/06/2010
  Last Modified: 24/06/2010
  Modified By: David Bergmann
 */
Request::setString('selectedBoxes');

$selectedBoxes = str_replace('&', ',', str_replace('chk=', '', $selectedBoxes));

$chkClaims_array = explode(",", $selectedBoxes);

//VERIFIES IF ANY CLAIM IS SELECTED
if (is_array($chkClaims_array)) {
	//Document Validation
	if ($_FILES['flePaymentRequest']['name'] != '') {
		$file_name = $_FILES['flePaymentRequest']['name'];
		$ext = strtolower(end(explode(".", $_FILES['flePaymentRequest']['name'])));
		$allowedExtensions = array("txt", "doc", "docx", "xls", "xlsx", "pdf", "gif", "jpg", "jpeg", "png", "bmp");

		//Verifies if the file has the allowed extensions
		if (in_array(end(explode(".", $file_name)), $allowedExtensions)) {
			$path = DOCUMENT_ROOT . 'assistanceFiles/assistancePayments/';
			$newFile = 'paymentRequest_' . date('U') . '.' . $ext;
			$path .= $newFile;
			if (move_uploaded_file($_FILES['flePaymentRequest']['tmp_name'], $path)) {
				$_FILES['flePaymentRequest']['name'] = "paymentRequest" . rand(0, 1000);
				$serial_usr = $_SESSION['serial_usr'];

				/*** SEND E-MAIL FOR LIQUIDATION ***/
				$_POST['chkClaims'] = $selectedBoxes;
				$_POST['liquidationType'] = 'liquidation';
				$_POST['selProvider'] = $_POST['selProvider'];
				$provider_liquidation = true;

				/***** PAYMENT EMAILS **** */
				$parameter = new Parameter($db, '39'); //Parameters for Coordinator Payments
				$parameter->getData();
				$suitable_mails = explode(',', $parameter->getValue_par());

				if (is_array($suitable_mails)) {
					$payment_emails = array();
					foreach ($suitable_mails as $p) {
						$user = new User($db, $p);
						$user->getData();
						array_push($payment_emails, $user->getEmail_usr());
					}
				}
				/***** PAYMENT EMAILS **** */

				require_once( DOCUMENT_ROOT . 'rpc/assistance/createLiquidationDocument.rpc.php');
				/*** SEND E-MAIL FOR LIQUIDATION ***/

				if (json_decode($process_result) == TRUE) {
					//LIQUIDATES EACH CLAIM
					foreach ($chkClaims_array as $c) {
						$paymentRequest = new PaymentRequest($db, $c);
						$paymentRequest->getData();
						$paymentRequest->setAprovationUser($serial_usr);
						$paymentRequest->setDispatched_prq("YES");
						$paymentRequest->setDispatched_date_prq(date("d/m/y"));
						$paymentRequest->setFile_url_prq($path);

						if (!$paymentRequest->update()) {
							http_redirect('modules/assistance/fDispatchAssistanceClaims/2');
						}
					}
					http_redirect('modules/assistance/fDispatchAssistanceClaims/1');
				} else {
					http_redirect('modules/assistance/fDispatchAssistanceClaims/6');
				}
			} else {
				http_redirect('modules/assistance/fDispatchAssistanceClaims/2');
			}
		} else {
			http_redirect('modules/assistance/fDispatchAssistanceClaims/4');
		}
	} else {
		http_redirect('modules/assistance/fDispatchAssistanceClaims/5');
	}
} else {
	http_redirect('modules/assistance/fDispatchAssistanceClaims/3');
}
?>