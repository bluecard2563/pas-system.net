<?php
/*
 * File: fViewFile.php
 * Author: Patricio Astudillo
 * Creation Date: 19/05/2010, 03:37:20 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 19/05/2010, 03:37:20 PM
 */

Request::setString('0:serial_prq');
Request::setString('1:read_only');

//die(Debug::print_r($_REQUEST));

$pRequest=new PaymentRequest($db, $serial_prq);

if($pRequest->getData()){
	$file=new File($db, $pRequest->getSerial_fle());
	$file->getData();
	
	$clientBlog=new Blog($db);
	$clientBlog->setSerial_fle($pRequest->getSerial_fle());
	$clientBlog->getDataByFile('CLIENT');
	$clientBlog=get_object_vars($clientBlog);
	unset($clientBlog['db']);
	$clientBlog['entry_blg']=specialchars($clientBlog['entry_blg']);
	
	$operatorBlog=new Blog($db);
	$operatorBlog->setSerial_fle($pRequest->getSerial_fle());
	$operatorBlog->getDataByFile('OPERATOR');
	$operatorBlog=get_object_vars($operatorBlog);
	unset($operatorBlog['db']);
	$operatorBlog['entry_blg']=specialchars($operatorBlog['entry_blg']);
	
	$documents=DocumentsByFile::getDocumentsInfoByFile($db, $pRequest->getSerial_fle());

	$customer=new Customer($db, $file->getSerial_cus());
	$customer->getData();

	$seniorAnswers=Answer::getQuestionsAnswers($db, $file->getSerial_cus());

	$customer=get_object_vars($customer);
	unset($customer['db']);

	$sale=new Sales($db, $file->getSerial_sal());
	$saleInfo=$sale->getSaleProduct($_SESSION['serial_lang']);
	$saleBenefits=$sale->getSaleBenefits($_SESSION['serial_lang']);
	$saleServices=$sale->getSaleServices($_SESSION['serial_lang']);

	$previousFiles=File::getOldFilesByCustomer($db, $file->getSerial_cus(), $file->getSerial_fle());

	$file=get_object_vars($file);
	unset($file['db']);

	if($read_only){
		$auditor_comments = $pRequest->getAuditor_comments_prq();
		$smarty->register('auditor_comments,read_only');
	}
	
	$smarty->register('customer,file,clientBlog,operatorBlog,seniorAnswers,serial_prq,documents,saleInfo,saleBenefits,saleServices,previousFiles,global_fileStatus');
	$smarty->display();
}else{
	http_redirect('modules/assistance/medicalCheckup/fSelectMedicalCheckup/2');
}
?>
