<?php
/*
 * File: pViewFile.php
 * Author: Patricio Astudillo
 * Creation Date: 21/05/2010, 12:26:16 PM
 * Modifies By: Santiago Arellano
 * Last Modified: 21/05/2010, 03:12:49 PM
 */
Request::setString('txtObservations');
Request::setString('hdnSerial_prq');

$pRequest=new PaymentRequest($db, $hdnSerial_prq);

if($pRequest->getData()){
	$pRequest->setAuditor_comments_prq(specialchars($txtObservations));
	$pRequest->setStatus_prq('AUDITED');
	$pRequest->setAudited_date_prq(date('d/m/Y'));

	/*Insert file*/
	if($_FILES['medical_checkup'])

		 $filename = strtolower($_FILES['medical_checkup']['name']) ;
		 $exts = split("[/\\.]", $filename) ;
		 $n = count($exts)-1;
		 $exts = $exts[$n];

         $user = new User($db,$pRequest->getSerial_usr());
         $user->getData();
		 $serial_fle=$pRequest->getSerial_fle();

		 if(!is_dir(DOCUMENT_ROOT."assistanceFiles/file_$serial_fle")){
			mkdir(DOCUMENT_ROOT."assistanceFiles/file_$serial_fle", 0777);
		 }else{
			chmod(DOCUMENT_ROOT."assistanceFiles/file_$serial_fle", 0777);
		 }

			$file_name = PaymentRequest::generateFileName($db,$exts);
				if(copy($_FILES['medical_checkup']['tmp_name'],DOCUMENT_ROOT.'assistanceFiles/file_'.$serial_fle.'/'.$file_name)){
				   $pRequest->setFile_url_prq(utf8_encode('assistanceFiles/file_'.$serial_fle.'/'.$file_name));
				}

        /*E-MAIL INFO*/
        $user = new User($db,$pRequest->getSerial_usr());
        $user->getData();
        $misc['userName']=$user->getFirstname_usr()." ".$user->getLastname_usr();
        $misc['serial_fle']=$pRequest->getSerial_fle();
        $misc['email'][0] = $user->getEmail_usr();

		$parameter = new Parameter($db, '34'); //Parameters for Liquidation Permitions.
		$parameter->getData();
		$assistanceEmails = unserialize($parameter->getValue_par());

		if(is_array($assistanceEmails['medical_checkup'])){
			foreach ($assistanceEmails['medical_checkup'] as $a) {
				if(!in_array($a, $misc['email'])){
					array_push($misc['email'], $a);
				}
			}
		}
        /*END E-MAIL INFO*/

	if($pRequest->update()){
            if(GlobalFunctions::sendMail($misc,'medicalCheckup')) {
				$error=1;
            } else {
                $error=4;
            }
	}else{
		$error=3;
	}
}else{
	$error=2;
}
http_redirect('modules/assistance/medicalCheckup/fSelectMedicalCheckup/'.$error);
?>