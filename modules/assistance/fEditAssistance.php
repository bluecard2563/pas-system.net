<?php
/*
File: fEditAssistance
Author: David Bergmann
Creation Date: 29/03/2010
Last Modified: 02/02/2011
Modified By: David Bergmann
*/

Request::setInteger('0:error');
Request::setInteger('1:serial_fle');
Request::setInteger('2:other_card');

/*GET CARD NUMBER*/
if($other_card) {
    $_SESSION['card_number'] = $other_card;
    $data['card_number'] = $other_card;
} elseif($serial_fle!="0"){
    $smarty->register('serial_fle');
    $data['card_number'] = File::getCardNumberByFile($db, $serial_fle);
}elseif($_SESSION['card_number']) {
    $data['card_number'] = $_SESSION['card_number'];
}

$_SESSION['card_number'] = $data['card_number'];
/*END GET CARD NUMBER*/

if( $data['card_number']) {
    /*LIQUIDATION PERMISSIONS*/
    $parameter=new Parameter($db, '21');
    $parameter->getData();
    $permitedUsers=unserialize($parameter->getValue_par());

    $serial_mbc = User::getManagerByUser($db, $_SESSION['serial_usr']);

    if($serial_mbc=='1'){ //If the user is a PLANETASSIST's user, we set the country to Ecuador 62.
            $serial_mbc='2';
    }
    $permitedUsers[$serial_mbc]=explode(',', $permitedUsers[$serial_mbc]);
    /*USERS WITH LIQUIDATION PERMISSION*/
	if(is_array($permitedUsers[$serial_mbc])) {
		foreach ($permitedUsers[$serial_mbc] as $p) {
			if($_SESSION['serial_usr'] == $p) {
				$liquidation_permission = $_SESSION['serial_usr'];
				$smarty->register('liquidation_permission');
			}
		}
	}
	/*END LIQUIDATION PERMISSIONS*/
	$data = array_merge($data, Sales::getAssistanceDataByCardNumber($db,$data['card_number']));
	//die(Debug::print_r($data));
    /*INVOICE STATUS*/
    if($data['status_inv'] == "STAND-BY") {
        if($data['days_to_dueDate']<0) {
            $data['status_inv'] = 1;
        } else {
            $data['status_inv'] = 2;
        }
        
    } else {
		unset($data['status_inv']);
	}
	/*END INVOICE STATUS*/

	/*EMISSION STATUS*/
    if($data['days_since_emition'] < 48) {
        $data['emition'] = 1;
    }
	if($data['status_sal'] == "EXPIRED") {
		$data['emition'] = 2;
	}elseif($data['status_sal'] == "STANDBY"){
		http_redirect('modules/assistance/fNewAssistance/3');
	}elseif($data['status_sal'] == "VOID"){
		http_redirect('modules/assistance/fNewAssistance/5');
	}elseif($data['status_sal'] == "REFUNDED"){
		http_redirect('modules/assistance/fNewAssistance/7');
	}
	/*END EMISSION STATUS*/

	/*SALES LOG*/
    $salesLog = new SalesLog($db);
    $salesHistory = $salesLog->getAuthorizadSalesLogBySale($data['serial_sal']);
    $sl_titles = array("#","Fecha","Tipo","Estado");
    if(is_array($salesHistory)) {
        $smarty->register('salesHistory,sl_titles,global_sales_log_status');
    }
	/*END SALES LOG*/

    /*CARD FILES*/
    $files = new File($db);
	$data['fileList'] = $files->getFilesByCard($data['card_number']);
	$data['all_files']=array();
	$data['opened_files']=array();
	$data['closed_files']=array();
	if(is_array($data['fileList'])) {
		foreach($data['fileList'] as &$fl) {
			if(PaymentRequest::fileHasPendingRequests($db,$fl['serial_fle'])) {
				$fl['status_prq'] = 'AUDIT_REQUEST';
			}
			
			//**************** PROVIDERS GRADE SECTION
			$grades = CommentsByServiceProvider::getGradeStatusForProviders($db, $fl['serial_fle']);
			$fl['all_graded'] = TRUE;
			if($grades){
				foreach($grades as $g){
					if($g['graded'] == 'NO'){
						$fl['all_graded'] = FALSE;
						break;
					}
				}
			}else{
				$fl['all_graded'] = FALSE;
			}			
			
			
			if($fl['status_fle'] == 'open'){
				array_push($data['opened_files'], $fl);
				//array_push($data['all_files'], $fl);
			}
			if($fl['status_fle'] == 'closed' || $fl['status_fle'] == 'void'){
				array_push($data['closed_files'], $fl);
			}
			if($fl['status_fle'] == 'in_liquidation'){
				array_push($data['opened_files'], $fl);
				array_push($data['all_files'], $fl);
			}
		}
	}

	$data['file_titles'] = array("Expediente","Destino","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia","Abrir","Enviar a Liquidar","Registrar Informaci&oacute;n");
	$data['closed_file_titles'] = array("No.Expediente","Destino","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia","Estado", "Abrir");
    $data['liquidation_titles'] = array("No.Expediente","Destino","Ciudad","Cliente","Diagn&oacute;stico","Inicio de Asistencia","Liquidar","Auditor&iacute;a M&eacute;dica");
	/*END CARD FILES*/

    /*CARD APPLIANCES*/	
    $temporary_customer = new TemporaryCustomer($db);
    $data['appliances'] = $temporary_customer->getTemporaryCustomersByCard($card_number);
    $data['appliance_titles'] = array("No.Expediente","Destino","Cliente","Problema","Expediente","Estado");


    /*MEDICAL CHECKUP*/
	if($liquidation_permission) {
		$parameter = new Parameter($db,'25');
		$parameter->getData();
		$auditor_array = unserialize($parameter->getValue_par());

		if(is_array($auditor_array)) {
			$country_list=array();
			foreach($auditor_array as $key => $a) {
				$country = new Country($db,$key);
				$country->getData();
				array_push($country_list, array('serial_cou'=>$key, 'name_cou'=>$country->getName_cou()));
			}
			$smarty->register('country_list');
		}
	}
    $smarty->register('error,data');
    $smarty -> display();
} else {
    http_redirect('modules/assistance/fNewAssistance');
}
?>