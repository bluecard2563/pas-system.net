<?php
/*
  File: pLiquidationDocumentXLS.php
  Author: David Rosales
  Creation Date: 21/01/2014
  Last Modified: 
  Modified By: 
 */

$xlsname = 'liquidacion_' . date('U');
$title = '';
$data = '';
global $global_system_name;
global $date;

Request::setString('chkClaims');

if(!$chkClaims && isset($_POST['chkClaims'])){
	$chkClaims = $_POST['chkClaims'];
}
$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=".$xlsname.".xls");
header("Pragma: no-cache");
header("Expires: 0");

if ($chkClaims) {

	$paymentRequests = PaymentRequest::getLiquidationDocument($db, $chkClaims);

	//HEADER
        
	if ($_POST['liquidationType'] == 'preliquidation') {
		if ($_POST['selProvider']) {
                    $serviceProvider = new ServiceProvider($db, $_POST['selProvider']);
                    $serviceProvider->getData();
                    $title .= '<table>
                                <tr> <th colspan="9"><b>' .'SISTEMA '.$global_system_name . '</b></th> </tr>
                                <tr> <th colspan="9">' .$date. '</th> </tr>
                                <tr></tr>
                                <tr> <th colspan="9"><b>' .html_entity_decode("PRELIQUIDACI&Oacute;N DE SERVICIOS") . '</b></th> </tr>
                                <tr></tr>
                                <tr> <td><b>' . html_entity_decode("Coordinador: ") . '</b></td> <td>' . $serviceProvider->getName_spv() . '</td> </tr>
                                <tr></tr>
                        </table>';
		} else {
                    $title .= '<table>
                                <tr> <th colspan="9"><b>' .'SISTEMA '.$global_system_name . '</b></th> </tr>
                                <tr> <th colspan="9">' .$date. '</th> </tr>
                                <tr></tr>
                                <tr> <th><b>' .html_entity_decode("PRELIQUIDACI&Oacute;N DE SERVICIOS") . '</b></th> </tr>
                                <tr></tr>
                                <tr> <td><b>' . html_entity_decode("Cliente: ") . '</b></td> <td>' . $paymentRequests['0']['name_cus'] . '</td> </tr>
                                <tr></tr>
                        </table>';
		}
	} else {
		if ($_POST['selProvider']) {
                    $serviceProvider = new ServiceProvider($db, $_POST['selProvider']);
                    $serviceProvider->getData();
                    $title .= '<table>
                                <tr> <th colspan="9"><b>' .'SISTEMA '.$global_system_name . '</b></th> </tr>
                                <tr> <th colspan="9">' .$date. '</th> </tr>
                                <tr></tr>
                                <tr> <th><b>' .html_entity_decode("LIQUIDACI&Oacute;N DE SERVICIOS") . '</b></th> </tr>
                                <tr></tr>
                                <tr> <td><b>' . html_entity_decode("Coordinador: ") . '</b></td> <td>' . $serviceProvider->getName_spv() . '</td> </tr>
                                <tr></tr>
                        </table>';
		} else {
                    $title .= '<table>
                                <tr> <th colspan="9"><b>' .'SISTEMA '.$global_system_name . '</b></th> </tr>
                                <tr> <th colspan="9">' .$date. '</th> </tr>
                                <tr></tr>
                                <tr> <th><b>' .html_entity_decode("LIQUIDACI&Oacute;N DE SERVICIOS") . '</b></th> </tr>
                                <tr></tr>
                                <tr> <td><b>' . html_entity_decode("Cliente: ") . '</b></td> <td>' . $serviceProvider->getName_spv() . '</td> </tr>
                                <tr></tr>
                        </table>';
		}
	}
       
        
	$report = array();
	$totalReclaimed = 0;
	$totalAuthorised = 0;
        
        $data .= '<table> '
                . '<tr> '
                    . '<th><b>Cliente</b></th> '
                    . '<th><b>Expediente #</b></th> '
                    . '<th><b>Valor presentado</b></th> '
                    . '<th><b>Valor pagado</b></th> '
                    . '<th>'.html_entity_decode('<b>Nota de Cr&eacute;dito</b>').'</th> '
                    . '<th><b>Tarjeta #</b></th> '
                    . '<th><b>Fecha de Ocurrencia</b></th> '
                    . '<th><b>Causa</b></th> '
                    . '<th><b>Documento #</b></th> '
                    . '<th><b>Beneficios</b></th> '
                . '</tr>';
        
	if (is_array($paymentRequests)) {
            foreach ($paymentRequests as &$r) {
                $aux = array();
                $aux['col1'] = $r['name_cus'];
                $aux['col2'] = $r['serial_fle'];
                $aux['col3'] = $r['amount_reclaimed_prq'];
                if ($r['type_prq'] == 'CREDIT_NOTE') {
                        $aux['col3'] = '0';
                        $aux['col9'] = $r['amount_authorized_prq'];
                        $aux['col4'] = '0';
                } else {
                        $aux['col4'] = $r['amount_authorized_prq'];
                        $aux['col9'] = '0';
                }
                $aux['col5'] = $r['card_number'];
                $aux['col10'] = $r['creation_date_fle'];
                $aux['col6'] = $r['diagnosis_fle'];
                $aux['col7'] = $r['comment_dbf'];
                //benefits
                $benefits = BenefitsByDocument::getBenefitsByDocuments($db, $r['serial_dbf']);
                if (is_array($benefits)) {
                        $end = end($benefits);
                        foreach ($benefits as $b) {
                                $aux['col8'] .= "{$b['description_bbl']} \n                                     (\${$b['amount_authorized_bbd']})";
                                if ($b != $end) {
                                        $aux['col8'] .= ",\n";
                                }
                        }
                }
                //services
                $services = BenefitsByDocument::getServicesByDocuments($db, $r['serial_dbf']);
                if (is_array($services)) {
                        $end = end($services);
                        if (is_array($benefits)) {
                                $aux['col8'] .= ",\n";
                        }
                        foreach ($services as $s) {
                                $aux['col8'] .= "{$s['name_sbl']} \n                                     (\${$s['amount_authorized_bbd']})";
                                if ($s != $end) {
                                        $aux['col8'] .= ",\n";
                                }
                        }
                }

                array_push($report, $aux);
                if ($r['type_prq'] == 'CREDIT_NOTE') {
                        $totalToRefund += $r['amount_authorized_prq'];
                } else {
                        $totalAuthorised += $r['amount_authorized_prq'];
                        $totalReclaimed+=$r['amount_reclaimed_prq'];
                }
                
                $data .= '<tr> '
                    . '<td>'.$aux['col1'].'</td> '
                    . '<td>'.$aux['col2'].'</td> '
                    . '<td>'.$aux['col3'].'</td> '
                    . '<td>'.$aux['col4'].'</td> '
                    . '<td>'.$aux['col9'].'</td> '
                    . '<td>'.$aux['col5'].'</td> '
                    . '<td>'.$aux['col10'].'</td> '
                    . '<td>'.$aux['col6'].'</td> '
                    . '<td>'.$aux['col7'].'</td> '
                    . '<td>'.$aux['col8'].'</td> '
                . '</tr>';
            }
	}
        
        $data .= '<tr> '
                    . '<td><b>Sub Totales</b></td> '
                    . '<td>'.number_format($totalReclaimed, 2).'</td> '
                    . '<td>'.number_format($totalAuthorised, 2).'</td> '
                    . '<td>'.number_format($totalToRefund, 2).'</td> '
                . '</tr>';
        
        $data .= '<tr> '
                    . '<td><b>Total</b></td> '
                    . '<td>'.number_format($totalAuthorised - $totalToRefund, 2).'</td> '
                . '</tr>';
} 

echo $title;

echo $data;

?>