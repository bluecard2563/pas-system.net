<?php
/*
File: pNewTemporaryCustomer
Author: David Bergmann
Creation Date: 04/04/2010
Last Modified: 01/06/2010
Modified By: David Bergmann
*/

$temporary_customer = new TemporaryCustomer($db);
$temporary_customer->setSerial_cou($_POST['selCountry']);
$temporary_customer->setCou_serial_cou($_POST['selCountryTravel']);
$temporary_customer->setSerial_usr($_SESSION['serial_usr']);
$temporary_customer->setDocument_tcu($_POST['txtDocument']);
$temporary_customer->setFirst_name_tcu($_POST['txtFirstName']);
$temporary_customer->setLast_name_tcu($_POST['txtLastName']);
$temporary_customer->setBirthdate_tcu($_POST['txtBirthDate']);
$temporary_customer->setAgency_name_tcu($_POST['txtAgencyName']);
$temporary_customer->setPhone_tcu($_POST['txtPhone']);
$temporary_customer->setContact_phone_tcu($_POST['txtContactPhone']);
$temporary_customer->setContact_address_tcu($_POST['txtContactAddress']);
$temporary_customer->setCard_number_tcu($_POST['hdnCardNumber']);

$user = new User($db,$_SESSION['serial_usr']);
$user->getData();

$extras = "Requested by: ".$user->getFirstname_usr()." ".$user->getLastname_usr()."<br />Date:".date("d/m/Y H:i:s")."<br />".specialchars($_POST['txtExtras']);
$temporary_customer->setExtra_tcu($extras);
$problem = "Requested by: ".$user->getFirstname_usr()." ".$user->getLastname_usr()."<br />Date:".date("d/m/Y H:i:s")."<br />".specialchars($_POST['txtProblem']);
$temporary_customer->setProblem_description_tcu($problem);

/*E-MAIL INFO*/
$misc['user'] = $_SESSION['user_name'];		//REQUESTER USER
$misc['textInfo']="Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s o ingrese a <a href='".URL."' target='blank'>PAS</a>.";
$country = new Country($db, $temporary_customer->getSerial_cou());
$country->getData();
$misc['country'] = $country->getName_cou();
$misc['agency']=$temporary_customer->getAgency_name_tcu();
$country = new Country($db, $temporary_customer->getCou_serial_cou());
$country->getData();
$misc['travelCountry']=$country->getName_cou();
$misc['document']=$temporary_customer->getDocument_tcu();
$misc['name']=$temporary_customer->getFirst_name_tcu()." ".$temporary_customer->getLast_name_tcu();
$misc['birthDate']=$temporary_customer->getBirthdate_tcu();
$misc['phone']=$temporary_customer->getPhone_tcu();
$misc['contactAddress']=$temporary_customer->getContact_address_tcu();
$misc['contactPhone']=$temporary_customer->getContact_phone_tcu();
$misc['cardNumber']=$temporary_customer->getCard_number_tcu();
$misc['extra']=$temporary_customer->getExtra_tcu();
$misc['problem']=$temporary_customer->getProblem_description_tcu();

$sale = new Sales($db);
$sale->setCardNumber_sal($misc['cardNumber']);
if($sale->getDataByCardNumber()) {
    $misc['textForEmail']="Informaci&oacute;n de la tarjeta";
    $misc['cardInfo']=true;
    $misc['emission']=$sale->getEmissionDate_sal();
    $misc['begin']=$sale->getBeginDate_sal();
    $misc['end']=$sale->getEndDate_sal();
    $misc['status']=$sale->getStatus_sal();

    
    $counter = new Counter($db, $sale->getSerial_cnt());
    $counter->getData();
    
    $userByDealer = new UserByDealer($db);
    $userByDealer->setSerial_dea($counter->getSerial_dea());
    $userByDealer->getData();
    $user = new User($db,$userByDealer->getSerial_usr());
    $user->getData();
    $misc['email'] = array($user->getEmail_usr());
} else {
    $managerByCountry = new ManagerbyCountry($db);
    $managerInfo = $managerByCountry->getManagerByCountry($temporary_customer->getSerial_cou(),$_SESSION['serial_mbc']);
    $misc['email'] = array();
    if(is_array($managerInfo)) {
        foreach ($managerInfo as $m) {
            array_push($misc['email'], $m['contact_email_man']);
        }
    }
    $misc['textForEmail']="La tarjeta #".$misc['cardNumber']." no existe en el sistema.";
}
/*END E-MAIL INFO*/


// if($temporary_customer->insert()) {
//     if(GlobalFunctions::sendMail($misc, 'newTemporaryCustomer')){
//         http_redirect('modules/assistance/fNewAssistance/12');
//     } else {
//         http_redirect('modules/assistance/fNewAssistance/10');
//     }
// } else {
// 	http_redirect('modules/assistance/fNewAssistance/9');
// }
?>