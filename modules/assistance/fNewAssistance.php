<?php
/*
File: fNewAssistance
Author: David Bergmann
Creation Date: 26/03/2010
Last Modified: 11/06/2010
Modified By: David Bergmann
*/

Request::setInteger('0:error');

if(!$error) {
    unset($_SESSION['card_number']);
}

$card_number = $_SESSION['card_number'];

$assistanceGroupsByUsers = new AssistanceGroupsByUsers($db, $_SESSION['serial_usr']);
$group_id = $assistanceGroupsByUsers->getCurrentUserGroup();

if($group_id) {
    $smarty->register('error,card_number');
    $smarty -> display();
} else {
    http_redirect('main/assistance/1');
}
?>