<?php
/**
 * File: pVoidFiles
 * Author: Patricio Astudillo
 * Creation Date: 26/09/2011, 01:02:37 PM
 * Last Modified: 26/09/2011, 01:02:37 PM
 * Modified By:
 */
	Request::setString('txtObservations');
	$txtObservations = addslashes(mysql_escape_string($txtObservations));
	$files_list = explode(',', $_POST['hdnOneRequired']);
	$error = 1;
	
	foreach($files_list as $f){
		if(!File::voidFileRegistration($db, $f, 'void', $_SESSION['serial_usr'], $txtObservations)){
			$error = 2;
			break;
		}
	}
	
	http_redirect('modules/assistance/fVoidFiles/'.$error);
?>
