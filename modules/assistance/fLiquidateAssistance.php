<?php
/*
File: fLiquidateAssistance
Author: David Bergmann
Creation Date: 02/06/2010
Last Modified: 03/02/2010
Modified By: David Bergmann
*/

Request::setInteger('0:serial_fle');
Request::setString('1:acces_permition');

$card_number = $_SESSION['card_number'];

if($acces_permition && $acces_permition!="view") {
    http_redirect('modules/assistance/fNewAssistance');
}

$file = new File($db,$serial_fle);
if($file->getData()){
    $data['serial_fle'] = $serial_fle;
    $data['diagnosis_fle'] = $file->getDiagnosis_fle();
    $data['type_fle'] = $file->getType_fle();
    $data['cause_fle'] = $file->getCause_fle();
    $data['serial_cit'] = $file->getSerial_cit();
    $data['type_fle'] = $file->getType_fle();
    $data['serial_cit'] = $file->getSerial_cit();
	$data['incident_date_fle'] = $file->getIncident_date_fle();
}
//PENDING DOCUMENTS BY FILE
$pendingDocs = PendingDocumentsByFile::getPendingDocumentsByFile($db, $serial_fle);
if(is_array($pendingDocs)){
	foreach($pendingDocs as &$doc){
		if($doc['serial_spv']!=''){
			$doc['pendingFrom']='PROVIDER';
			$provider = new ServiceProvider($db,$doc['serial_spv']);
			$provider->getData();
			$doc['namePendingFrom']=$provider->getName_spv();
		}elseif($doc['serial_cus']!=''){
			$doc['pendingFrom']='CLIENT';
			$customer = new Customer($db,$doc['serial_cus']);
			$customer->getData();
			$doc['namePendingFrom']=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
		}
	}
}
/*CUSTOMER INFO*/
$customer = new Customer($db,$file->getSerial_cus());
$customer->getData();
$data['medical_background_cus'] = $customer->getMedical_background_cus();

/*CITIES*/
$city = new City($db,$file->getSerial_cit());
$city->getData();
$data['serial_cou'] = $city->getSerial_cou();
$data['name_cit'] = $city->getName_cit();

/*COUNTRIES*/
$country = new Country($db,$city->getSerial_cou());
$country->getData();
$data['name_cou'] = $country->getName_cou();

/*SERVICE PROVIDERS*/
$serviceProviderByFile = new ServiceProviderByFile($db);
$data['providerList'] = $serviceProviderByFile->getServiceProvidersByFile($serial_fle);


/*MEDICAL TYPES*/
$data['medicalTypes'] = $serviceProviderByFile->getMedicalTypes();

/*MEDICAL CHECKUPS*/
$data['medicalCheckupList'] = PaymentRequest::getMedicalCheckupsByFile($db, $serial_fle);
$data['medicalCheckupTitles'] = array('#','Fecha de solicitud','Fecha de auditor&iacute;a');

//******* BLOG ************
$blog['customer'] = Blog::getBlogForFile($db, $serial_fle, 'CLIENT');
$blog['customer'] = $blog['customer']['entry_blg'];
$blog['operator'] = Blog::getBlogForFile($db, $serial_fle, 'OPERATOR');
$blog['operator'] = $blog['operator']['entry_blg'];

//********* OTHER ASSISTANCES FOR CUSTOMER *************
$other_files = $file->getOtherCardFilesByCustomer($file->getSerial_sal(), $file->getSerial_cus());

$smarty->register('card_number,data,global_medicalType,acces_permition,pendingDocs,blog,other_files');
$smarty -> display();
?>