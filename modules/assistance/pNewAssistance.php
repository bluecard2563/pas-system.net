<?php

/*
  File: pNewAssistance
  Author: David Bergmann
  Creation Date: 29/03/2010
  Last Modified: 01/04/2010
  Modified By: David Bergmann
 */
ini_set('memory_limit', '512M');
ini_set('max_execution_time', '3600');

Request::setInteger('0:masive');

$_SESSION['card_number'] = $_POST['txtCard'];

if (!Sales::checkIfSaleIsMasiveByCardNumber($db, $_SESSION['card_number'])) {
	$sale = Sales::getAssistanceSaleInfoByCardNumber($db, $_SESSION['card_number'], $masive);

	if ($sale) {
		if ($sale['free_sal'] == "YES") {
			if ($sale['status_sal'] == "REQUESTED" || $sale['status_sal'] == "DENIED" || $sale['status_sal'] == "BLOCKED") {
				http_redirect('modules/assistance/fNewAssistance/11');
			}
		} else {
			if ($sale['status_sal'] != "ACTIVE") {
				if ($sale['status_sal'] == "VOID") {
					http_redirect('modules/assistance/fNewAssistance/5');
				} elseif ($sale['status_sal'] == "DENIED") {
					http_redirect('modules/assistance/fNewAssistance/6');
				} elseif ($sale['status_sal'] == "REFUNDED") {
					http_redirect('modules/assistance/fNewAssistance/7');
				} else {
					if ($sale['serial_inv'] != NULL) {
						$invoice = new Invoice($db, $sale['serial_inv']);
						$invoice->getData();
						//verifies if the invoice hasn't been voided
						if ($invoice->status_inv == "VOID") {
							http_redirect('modules/assistance/fNewAssistance/4');
						}
					} else {
						http_redirect('modules/assistance/fNewAssistance/2');
					}
				}
			}
		}
	} else {
		http_redirect('modules/assistance/fNewAssistance/1');
	}
}else{
	http_redirect('modules/assistance/fNewAssistance/13');
}
//Pasa
http_redirect('modules/assistance/fEditAssistance');
?>
