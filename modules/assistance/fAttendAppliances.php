<?php
/*
File: fAttendAppliances
Author: David Bergmann
Creation Date: 10/06/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$serial_cou = GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
if($serial_cou == 1){ //VALIDATION FOR PAS USERS.
	$serial_cou = 62;
}

$apliances = TemporaryCustomer::getTemporaryCustomersByCountry($db, $serial_cou);

if(is_array($apliances)){
	foreach($apliances as &$a){
		$a['status_sal']=$global_salesStatus[$a['status_sal']];
	}
	
	$titles = array('Tarjeta #','Estado de la Tarjeta','Usuario Solicitante','Solicitud #','Nombre de Agencia','Cliente','Documento','Tel&eacute;fono','Pa&iacute;s destino','Problema','Comentarios');
	$total_pages=(int)(count($apliances)/10)+1;

	$smarty->register('error,apliances,titles,total_pages');
}

$smarty -> display();
?>