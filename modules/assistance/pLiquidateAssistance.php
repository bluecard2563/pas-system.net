<?php

/*
  File: pLiquidateAssistance
  Author: David Bergmann
  Creation Date: 05/05/2010
  Last Modified:
  Modified By:
 */

Request::setInteger('0:serial_fle');

$file = new File($db, $serial_fle);
$file->getData();
$file->setStatus_fle("closed");
$file->setClosing_usr($_SESSION['serial_usr']);

/* GET THE CARD NUMBER */
if ($file->getSerial_trl()) {
	$tr_log = new TravelerLog($db, $file->getSerial_trl());
	$tr_log->getData();
	$begin_date = $tr_log->getStart_trl();
	$end_date = $tr_log->getEnd_trl();
	$card_number = $tr_log->getCard_number_trl();
	if (!$card_number) {
		$sale = new Sales($db, $tr_log->getSerial_sal());
		$sale->getData();
		$card_number = $sale->getCardNumber_sal();
	}
} else {
	$sale = new Sales($db, $file->getSerial_sal());
	$sale->getData();
	$begin_date = $sale->getBeginDate_sal();
	$end_date = $sale->getEndDate_sal();
	$card_number = $sale->getCardNumber_sal();
}
/* GET THE CARD NUMBER */

$parameter = new Parameter($db, '34');
$parameter->getData();
$sendMail = unserialize($parameter->getValue_par());

$customer = new Customer($db, $file->getSerial_cus());
$customer->getData();
$nameCus = $customer->getFirstname_cus() . " " . $customer->getLastname_cus();

$sale = new Sales($db, $file->getSerial_sal());
$sale->getData();
$counter = new Counter($db, $sale->getSerial_cnt());
$counter->getData();
$dealer = new Dealer($db, $counter->getSerial_dea());
$dealer->getData();
$sector = new Sector($db, $dealer->getSerial_sec());
$sector->getData();
$city = new City($db, $sector->getSerial_cit());
$city->getData();
$country = new Country($db, $city->getSerial_cou());
$country->getData();
$countryCardSold = $country->getName_cou();

$misc['email'] = $sendMail['close'];
$misc['card_number'] = $card_number;
$misc['diagnosis'] = $file->getDiagnosis_fle();
$misc['type_fle'] = $global_fileTypes[$file->getType_fle()];
$misc['serial_fle'] = $file->getSerial_fle();
$misc['nameCus'] = $nameCus;
$misc['incident_date_fle'] = $file->getIncident_date_fle();
$misc['begin_date'] = $begin_date;
$misc['end_date'] = $end_date;
$misc['countryCard'] = $countryCardSold;

if ($file->update()) {
	$tempFLog = FilesLog::getActiveLogForFileAndStatus($db, $serial_fle, 'in_liquidation');
	
	if($tempFLog){
		$closingFLog = new FilesLog($db, $tempFLog['id']);
		$closingFLog ->setFinished_user($_SESSION['serial_usr']);
		$closingFLog ->save();
	}

	GlobalFunctions::sendMail($misc, 'closeAssistance');
	http_redirect('modules/assistance/fEditAssistance/3/' . $serial_fle);
}
?>
