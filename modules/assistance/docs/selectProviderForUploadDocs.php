<?php
/**
 * File: selectProviderForUploadDocs
 * Author: Patricio Astudillo
 * Creation Date: 19/08/2015
 * Last Modified:
 * Modified By:
 */

	$spvs = CustomLiquidationFunctions::getUsedProvidersInLiquidatedFiles($db);

	$smarty->register('spvs');
	$smarty->display();