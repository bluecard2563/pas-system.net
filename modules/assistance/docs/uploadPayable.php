<?php

/**
 * File: uploadPayable
 * Author: Patricio Astudillo
 * Creation Date: 19/08/2015
 * Last Modified:
 * Modified By:
 */

	Request::setString('0:serial_fle');
	Request::setString('1:serial_spv');
	Request::setString('2:error');

	/* UPLOADED FILES */
	$fileList = DocumentsByFile::getDocumentsInfoByFile($db, $serial_fle, $serial_spv);
	if (is_array($fileList)) {
		foreach ($fileList as &$fl) {
			/* $fl['name_dbf'] = utf8_encode($fl['name_dbf']);
			  $fl['comment_dbf'] = utf8_encode($fl['comment_dbf']);
			  $fl['url_dbf'] = utf8_encode($fl['url_dbf']);
			  $fl['name_spv'] = utf8_encode($fl['name_spv']);
			  $fl['observations_prq'] = utf8_encode($fl['observations_prq']); */
			$fl['file_url_prq'] = str_replace(DOCUMENT_ROOT, '', $fl['file_url_prq']);
			$fl['type_prq'] = $global_payment_request_type[$fl['type_prq']];
		}
	}
	$data['fileList'] = $fileList;

	$types_prq = PaymentRequest::getPaymentRequestTypes($db);

	$uploadedFilesTitles = array('Documento', 'No. Documento', 'Monto Presentado ($)', 'Monto Aprobado / Negado ($)',
		'Pago', 'Fecha del Documento', 'Estado', 'Beneficios', 'Observaciones', 'Tipo de Archivo', 'Archivo');

	if ($extended) {
		array_push($uploadedFilesTitles, 'Fecha de Pago');
		array_push($uploadedFilesTitles, 'Ver Pago');
	}
	
	$file = new File($db, $serial_fle);
	$file -> getData();
	
	$sale = new Sales($db, $file->getSerial_sal());
	$sale->getData();
	
	$customer = new Customer($db, $file->getSerial_cus());
	$customer->getData();
	
	$customData = array();
	$customData['serial_sal'] = $sale->getSerial_sal();
	$customData['serial_fle'] = $serial_fle;
	$customData['card_number'] = $sale->getCardNumber_sal();
	$customData['diagnosis'] = $file->getDiagnosis_fle();
	$customData['pax'] = $customer->getFirstname_cus().' '.$customer->getLastname_cus();
	
	$customUploadScript = true;
	include_once DOCUMENT_ROOT.'rpc/assistance/loadBenefitChooser.rpc.php';


	$date = getdate();
	$data['date'] = $date['mday'] . '/' . $date['mon'] . '/' . $date['year'];
	$smarty->register('uploadedFilesTitles,data,global_billTo,global_paymentRequestStatus,acces_permition,types_prq,customData,serial_spv,error');
	$smarty->display();