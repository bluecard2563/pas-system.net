<?php
/**
 * File: pUploadPayable
 * Author: Patricio Astudillo
 * Creation Date: 20/08/2015
 * Last Modified By: 20/08/2015
 */
 //Debug::print_r($_POST); die;

$serial_fle = $_POST['hdnSerial_fle'];
$serial_spv = $_POST['hdnSerial_spv'];

$name_dbf = $_POST['txtFileName'];
$commnet_dbf = $_POST['txtFileDescription'];
$billTo = $_POST['selBillTo'];
$document_date = $_POST['txtDocumentDate'];

$status_prq = $_POST['selStatusPrq'];
$txtTotalPresentedAmount = $_POST['txtTotalPresentedAmount'];
$totalAuthorizadAmount = $_POST['txtTotalAuthorizadAmount'];
$observations = $_POST['txtObservations'];
$type_prq = $_POST['selTypePrq'];
$serial_bxp = $_POST['filesToUpload'];

$authorized_amount = $_POST['authorized_amounts'];
$presented_amount = $_POST['presented_amounts'];

if (!$serial_bxp) {
	http_redirect('modules/assistance/docs/uploadPayable/'.$serial_fle.'/'.$serial_spv.'/2'); //NO BENEFITS CHOSEN
}

$cur_file_name = $_FILES['fileAddress']['name'];
$file_name = explode(".", $cur_file_name);
$new_file_name = "file_" . time('U') . "." . $file_name[sizeof($file_name) - 1];
$path = DOCUMENT_ROOT . "assistanceFiles/payableDocs/file_$serial_fle/$new_file_name";

$documentByFile = new DocumentsByFile($db);
$documentByFile->setSerial_fle($serial_fle);
$documentByFile->setName_dbf($name_dbf);
$documentByFile->setComment_dbf($commnet_dbf);
$documentByFile->setUrl_dbf("assistanceFiles/payableDocs/file_$serial_fle/$new_file_name");
$documentByFile->setDocument_to_dbf($billTo);
$documentByFile->setDocument_date_dbf($document_date);
$serial_dbf = $documentByFile->insert();

if ($serial_dbf) {
	//assign benefits to document
	$benefitsByDocument = new BenefitsByDocument($db);
	$benefitsByDocument->setSerial_dbf($serial_dbf);
	$error = false;

	$paymentRequest = new PaymentRequest($db);
	$paymentRequest->setSerial_usr($_SESSION['serial_usr']);
	$paymentRequest->setSerial_fle($serial_fle);
	$paymentRequest->setSerial_dbf($serial_dbf);
	$paymentRequest->setStatus_prq($status_prq);
	$paymentRequest->setAmount_reclaimed_prq($txtTotalPresentedAmount);
	$paymentRequest->setAmount_authorized_prq($totalAuthorizadAmount);
	$paymentRequest->setObservations_prq($observations);
	$paymentRequest->setType_prq($type_prq);

	if (!$paymentRequest->insert()) {
		http_redirect('modules/assistance/docs/uploadPayable/'.$serial_fle.'/'.$serial_spv.'/3'); //PRQ FAILED INSERT
	}

	$benefits = explode(",", $serial_bxp);
	$authorized_amounts = explode(",", $authorized_amount);
	$presented_amounts = explode(",", $presented_amount);

	foreach ($benefits as $key => $b) {
		$benefitsByDocument->setSerial_spf($b);
		$benefitsByDocument->setAmount_authorized_bbd($authorized_amounts[$key]);
		$benefitsByDocument->setAmount_presented_bbd($presented_amounts[$key]);

		if (!$benefitsByDocument->insert()) {
			$error = true;
		}
	}

	if (!$error) {
		//check if directory for this file exists.
		if (!is_dir(DOCUMENT_ROOT . "assistanceFiles/payableDocs/file_$serial_fle")) {
			mkdir(DOCUMENT_ROOT . "assistanceFiles/payableDocs/file_$serial_fle", 0777);
		}
		//move the file to the final path
		if (move_uploaded_file($_FILES['fileAddress']['tmp_name'], $path)) {
			http_redirect('modules/assistance/docs/uploadPayable/'.$serial_fle.'/'.$serial_spv.'/1');
		} else {
			http_redirect('modules/assistance/docs/uploadPayable/'.$serial_fle.'/'.$serial_spv.'/4'); //UPLOAD FILE ERROR.
		}
	} else {
		http_redirect('modules/assistance/docs/uploadPayable/'.$serial_fle.'/'.$serial_spv.'/5'); //BENEFITS CHOOSER INSERT ERROR
	}
} else {
	http_redirect('modules/assistance/docs/uploadPayable/'.$serial_fle.'/'.$serial_spv.'/6'); //DOCUMENT INSERT ERROR
}