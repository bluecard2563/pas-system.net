<?php
/**
 * File: liquidateCase.php
 * Author: Patricio Astudillo
 * Creation Date: 07/03/2014
 * Last Modified: 07/03/2014
 * Modified By: Patricio Astudillo
*/

	Request::setInteger('0:error');
	Request::setInteger('1:settlement_id');
	
	$nonSettledCases = CaseLiquidationGlobals::retrieveNonSettledCases($db);
	
	$smarty->register('nonSettledCases,error,settlement_id');
	$smarty->display();
?>