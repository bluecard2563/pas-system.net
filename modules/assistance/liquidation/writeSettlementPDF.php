<?php
/**
 * File: writeSettlementPDF
 * Author: Patricio Astudillo
 * Creation Date: 10/04/2014
 * Last Modified: 10/04/2014
 * Modified By: Patricio Astudillo
*/

	include_once DOCUMENT_ROOT.'modules/assistance/liquidation/settlement_text.php';
	
	$document = PaymentRequest::getPayOffDocument($db, implode($invoice_list, ","));

	if($document['serial_cou'] == 62){
		$company_name = "BLUE CARD ECUADOR S.A.";
		$company_logo = DOCUMENT_ROOT."img/contract_logos/logoblue_contracts.jpg";
	} else {
		$company_name = "PLANET ASSIST INC.";
		$company_logo = DOCUMENT_ROOT."img/contract_logos/logopas_contracts.jpg";
	}
	
	$pdf = & new Cezpdf('A4', 'portrait');
	//WIDTH: 600	HEIGHT: 800
	set_time_limit(36000);
	ini_set('memory_limit', '1024M');

	$pdf->selectFont(DOCUMENT_ROOT . 'lib/PDF/fonts/Helvetica.afm');
	$pdf->ezSetCmMargins(1, 1, 2, 1);
	$all = $pdf->openObject();
	$pdf->saveState();
	
	$pdf->addJpegFromFile($company_logo, 410, 790, 150);

	$pdf->setColor(0, 0, 0);

	$pdf->ezStartPageNumbers(560, 30, 10, '', '{PAGENUM} de {TOTALPAGENUM}', 1);
	$pdf->restoreState();
	$pdf->closeObject();
	$pdf->addObject($all, 'all');
	$pdf->saveState();
	
	//Positioning:
	$leftMostX = 55;
	$verticalPointer = 800;
	$max_height = 800;
	
	//Global Variables
	global $smallTextSize;
	global $medTextSize;
	global $stdTextSize;
	global $largeTextSize;
	global $titleTextSize;

	//Colors:
	global $blue;
	global $lightBlue;
	global $refText;
	global $red;
	global $black;
	global $white;

	global $refLines;
	global $rimacBckGnd;
	global $lightGray;
	
	//********************************************************* THE PDF ***************************************************
	//************************ TITLE **************************
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 500, $titleTextSize, $settlementTags['title'], 'left');
	
	//************************** FIRST SECTION ****************
	$verticalPointer -= 30;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 500, $largeTextSize, $settlementTags['section'][1]['title'], 'left');
	
	$verticalPointer -= 20;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 140, $stdTextSize, $settlementTags['section'][1]['contract']);
	$pdf -> addTextWrap($leftMostX + 140, $verticalPointer, 560, $stdTextSize, $settlementDataArray['contract_no']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 140, $stdTextSize, $settlementTags['section'][1]['coverage']);
	$pdf -> addTextWrap($leftMostX + 140, $verticalPointer, 80, $stdTextSize, $settlementTags['section'][1]['begin_coverage']);
	$pdf -> addTextWrap($leftMostX + 220, $verticalPointer, 80, $stdTextSize, $settlementDataArray['begin_coverage']);
	$pdf -> addTextWrap($leftMostX + 300, $verticalPointer, 80, $stdTextSize, $settlementTags['section'][1]['end_coverage']);
	$pdf -> addTextWrap($leftMostX + 380, $verticalPointer, 80, $stdTextSize, $settlementDataArray['end_coverage']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 140, $stdTextSize, $settlementTags['section'][1]['product']);
	$pdf -> addTextWrap($leftMostX + 140, $verticalPointer, 560, $stdTextSize, $settlementDataArray['product']);
	
	//************************** SECOND SECTION ****************
	$verticalPointer -= 30;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 500, $largeTextSize, $settlementTags['section'][2]['title'], 'left');
	
	$verticalPointer -= 20;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][2]['file']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 125, $stdTextSize, $settlementDataArray['file_no']);
	$pdf -> addTextWrap($leftMostX + 250, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][2]['service']);
	$pdf -> addTextWrap($leftMostX + 375, $verticalPointer, 125, $stdTextSize, $settlementDataArray['service']);
	
	$verticalPointer -= 12;	
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][2]['incident_date']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 560, $stdTextSize, $settlementDataArray['service_date']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][2]['country']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 125, $stdTextSize, $settlementDataArray['service_country']);
	$pdf -> addTextWrap($leftMostX + 250, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][2]['city']);
	$pdf -> addTextWrap($leftMostX + 375, $verticalPointer, 125, $stdTextSize, $settlementDataArray['service_city']);
	
	
	//************************** THIRD SECTION ****************
	if ($verticalPointer <= 50) {
		$pdf->ezNewPage();
		$verticalPointer = 800;
	}else{
		$verticalPointer -= 30;
	}
	
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 500, $largeTextSize, $settlementTags['section'][3]['title'], 'left');
	
	$verticalPointer -= 20;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][3]['beneficiary']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 575, $stdTextSize, $settlementDataArray['beneficiary']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][3]['beneficiary_document']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 575, $stdTextSize, $settlementDataArray['beneficiary_document']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][3]['beneficiary_birthdate']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 125, $stdTextSize, $settlementDataArray['beneficiary_birthdate']);
	$pdf -> addTextWrap($leftMostX + 250, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][3]['beneficiary_age']);
	$pdf -> addTextWrap($leftMostX + 375, $verticalPointer, 125, $stdTextSize, $settlementDataArray['beneficiary_age']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][3]['holder']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 575, $stdTextSize, $settlementDataArray['holder']);
	
	$verticalPointer -= 12;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 125, $stdTextSize, $settlementTags['section'][3]['holder_document']);
	$pdf -> addTextWrap($leftMostX + 125, $verticalPointer, 575, $stdTextSize, $settlementDataArray['holder_document']);
	
	//************************** FOURTH SECTION ****************
	if ($verticalPointer <= 50) {
		$pdf->ezNewPage();
		$verticalPointer = 800;
	}else{
		$verticalPointer -= 30;
	}
	
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 500, $largeTextSize, $settlementTags['section'][4]['title'], 'left');
	
	$pdf->ezSetDy(-($max_height - ($verticalPointer - 25)));
	$pdf->ezTable($settlementDataArray['coveredItems'], 
						array(	'benefit' => '<b>Beneficio</b>',
								'document_no' => '<b>No. Comprobante</b>',
								'coverage' => '<b>Monto Presentado</b>',
								'refund' => '<b>Monto Aprobado</b>',
								'recieved' => '<b>Pagado a</b>'),
						'',
						array('showHeadings'=>1,
                                'shaded'=>0,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'fontSize' => 8,
                                'titleFontSize' => 12,
                                'cols'=>array(
                                        'benefit'=>array('justification'=>'center','width'=>100),
										'document_no'=>array('justification'=>'center','width'=>100),
                                        'coverage'=>array('justification'=>'center','width'=>100),
										'refund'=>array('justification'=>'center','width'=>100),
										'recieved'=>array('justification'=>'center','width'=>100)
									)));
	
	$pdf ->ezText('<b>Total Presentado:</b> '.$settlementDataArray['coveredItemsTotal']['requested'], $stdTextSize);
	$pdf ->ezText('<b>Total Aprobado:</b> '.$settlementDataArray['coveredItemsTotal']['aproved'], $stdTextSize);
	
	//********************* LEGAL CONDITIONS *****************
	$verticalPointer = $pdf->ezGetDy();
	$verticalPointer -= 20;
	
	$cadena = 'El veloz murci�lago hind� com�a feliz cardillo y kiwi.';
	$patrones = array(	'/day/',
						'/month/',
						'/year/',
						'/contract/', 
						'/company/', 
						'/beneficiary/',
						'/currentDay/',
						'/currentMonth/',
						'/currentYear/');
	$sustituciones = array(	$settlementDataArray['sale_day'],
							$settlementDataArray['sale_month'],
							$settlementDataArray['sale_year'],
							$settlementDataArray['contract_no'],
							$company_name,
							$settlementDataArray['beneficiary'],
							date('d'),
							$global_weekMonthsTwoDigits[date('m')],
							date('Y'));
	
	$verticalStep = 10;
	foreach($settlementTags['clause_lines'] as $clause_line){
		$clause_formated = preg_replace($patrones, $sustituciones, $clause_line);
		$widthText = $pdf->getTextWidth($stdTextSize, $clause_formated);

		if ($widthText < 500) {
			$pdf->addTextWrap($leftMostX, $verticalPointer, 500, $stdTextSize, $bullet . $clause_formated, 'full');
			$verticalPointer -= $verticalStep;
		} else {
			$a = $pdf->addTextWrap($leftMostX, $verticalPointer, 500, $stdTextSize, $bullet . $clause_formated, 'full');
			$verticalPointer -= $verticalStep;
			for ($j = 1; $j <= (int)($widthText / 500); $j++) {
				$a = $pdf->addTextWrap($leftMostX, $verticalPointer, 500, $stdTextSize, '' . $a, 'full');
				$verticalPointer -= $verticalStep;
				if ($verticalPointer <= 50) {
					$pdf->ezNewPage();
					$verticalPointer = 750;
				}
			}
		}

		if ($verticalPointer <= 50) {
			$pdf->ezNewPage();
			$verticalPointer = 750;
		}	
		
		$verticalPointer -= 8;
	}
	
	//************************** SIGNING SECTION ****************
	if ($verticalPointer <= 54) {
		$pdf->ezNewPage();
		$verticalPointer = 750;
	}else{
		$verticalPointer -= 20;
	}
	
	$pdf -> line($leftMostX + 15, $verticalPointer, $leftMostX + 215, $verticalPointer);
	$pdf -> line($leftMostX + 280, $verticalPointer, $leftMostX + 465, $verticalPointer);
			
	$verticalPointer -= 20;
	$pdf -> addTextWrap($leftMostX, $verticalPointer, 250, $stdTextSize, $settlementTags['signature_pas'], 'center');
	$pdf -> addTextWrap($leftMostX + 250, $verticalPointer, 250, $stdTextSize, $settlementTags['signature_holder'], 'center');
	$verticalPointer -= 15;
	$pdf -> addTextWrap($leftMostX + 250, $verticalPointer, 250, $stdTextSize, $settlementTags['signature_holder_document']);
	
	if($display=='1'){
		$pdf -> ezStream();
	}else{
		$pdfcode = $pdf -> ezOutput();
		$fp=fopen(DOCUMENT_ROOT .'system_temp_files/settlement_'.$settlement_id.'.pdf','wb');

		fwrite($fp,$pdfcode);
		fclose($fp);
	}
?>