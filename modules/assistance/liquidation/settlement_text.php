<?php
/**
 * File: settlement_text
 * Author: Patricio Astudillo
 * Creation Date: 10/04/2014
 * Last Modified: 10/04/2014
 * Modified By: Patricio Astudillo
*/

	$settlementTags = array();
	$settlementTags['title'] = utf8_decode(html_entity_decode('<b>ACTA DE FINIQUITO Y LIQUIDACI&Oacute;N</b>'));
	
	$settlementTags['section'][1]['title'] = '<b>1. Datos del Contrato </b>';
	$settlementTags['section'][1]['contract'] = utf8_decode(html_entity_decode('<b>N&uacute;mero del Contrato: </b>'));
	$settlementTags['section'][1]['coverage'] = '<b>Vigencia del Contrato </b>';
	$settlementTags['section'][1]['begin_coverage'] = '<b>Desde: </b>';
	$settlementTags['section'][1]['end_coverage'] = '<b>Hasta: </b>';
	$settlementTags['section'][1]['product'] = '<b>Producto: </b>';
	
	$settlementTags['section'][2]['title'] = '<b>2. Datos del Reclamo</b>';
	$settlementTags['section'][2]['file'] = '<b>Expediente No.: </b>';
	$settlementTags['section'][2]['service'] = '<b>Tipo de Servicio: </b>';
	$settlementTags['section'][2]['incident_date'] = '<b>Fecha de Ocurrencia: </b>';
	$settlementTags['section'][2]['country'] = utf8_decode(html_entity_decode('<b>Pa&iacute;s de Ocurrencia: </b>'));
	$settlementTags['section'][2]['city'] = '<b>Ciudad de Ocurrencia: </b>';
	
	$settlementTags['section'][3]['title'] = '<b>3. Datos del Beneficiario</b>';
	$settlementTags['section'][3]['beneficiary'] = '<b>Beneficiario: </b>';
	$settlementTags['section'][3]['beneficiary_document'] = '<b>D.N.I./Pasaporte: </b>';
	$settlementTags['section'][3]['beneficiary_birthdate'] = '<b>Fecha de Nacimiento: </b>';
	$settlementTags['section'][3]['beneficiary_age'] = '<b>Edad: </b>';
	$settlementTags['section'][3]['holder'] = '<b>Contratante: </b>';
	$settlementTags['section'][3]['holder_document'] = '<b>D.N.I./Pasaporte: </b>';
	
	$settlementTags['section'][4]['title'] = utf8_decode(html_entity_decode('<b>4. Detalles de la Liquidaci&oacute;n</b>'));
	$settlementTags['clause_lines'] = array ( utf8_decode(html_entity_decode('Comparecen a la celebraci&oacute;n de la presente Acta de Finiquito y Liquidaci&oacute;n (el "Acta"), por una parte company ("LA COMPA&Ntilde;&Iacute;A") debidamente representada por quien suscribe el presente documento; y, por otra, el se&ntilde;or(a) beneficiary (el "Beneficiario"). Las partes acuerdan suscribir la presente Acta, al tenor de las siguientes consideraciones:')),
											utf8_decode(html_entity_decode('1. Con fecha day de month de year, el Beneficiario suscribi&oacute; el contrato n&uacute;mero contract (el "Contrato"), mediante el cual adquiri&oacute; los servicios de asistencia en viajes que presta LA COMPA&Ntilde;&Iacute;A.')),
											utf8_decode(html_entity_decode('2.	El Beneficiario/Contratante declara haber recibido por concepto de (beneficio), bajo el Contrato arriba descrito; y que los valores generados por los beneficios descritos fueron asumidos en su totalidad por LA COMPA&Ntilde;&Iacute;A y/o reembolsados al Beneficiario/Contratante acorde al presente detalle de liquidaci&oacute;n.')),
											utf8_decode(html_entity_decode('3.	El Beneficiario ser&aacute; el &uacute;nico responsable por el pago de cualquier impuesto, tasa o tributo aplicable o derivado o  en relaci&oacute;n a los servicios o pagos realizados por la cobertura recibida sea en jurisdicci&oacute;n ecuatoriana como extranjera.')),
											utf8_decode(html_entity_decode('4.	En virtud de lo expuesto, de la asistencia brindada, las partes se otorgan amplio finiquito y declaran que nada tienen que reclamarse en el presente ni en el futuro por la relaci&oacute;n comercial descrita en los antecedentes.')),
											utf8_decode(html_entity_decode('5.	El Beneficiario/Contratante por sus propios derechos, declara que no existe ning&uacute;n reclamo pendiente contra LA COMPA&Ntilde;&Iacute;A ni sus afiliadas, vinculadas, relacionadas, ni contra sus representantes, funcionarios ni empleados. As&iacute; mismo declara que a solicitud de LA COMPA&Ntilde;&Iacute;A, reconocer&aacute; ante Juez o Notario, la autenticidad de sus firmas y r&uacute;bricas que consigna en la presente Acta.')),
											utf8_decode(html_entity_decode('6. El Contratante podr&aacute; representar al Beneficiario bajo las siguientes circunstancias:')),
											utf8_decode(html_entity_decode('	a)	Si, el Beneficiario fuera menor de edad al momento de la emisi&oacute;n del Contrato;')),
											utf8_decode(html_entity_decode('	b)	Si, el Beneficiario  se encontrase fuera del pa&iacute;s de origen y/o residencia habitual;')),
											utf8_decode(html_entity_decode('	c)	Si, el Beneficiario presentase alg&uacute;n tipo de discapacidad f&iacute;sica, mental u otra.')),
											utf8_decode(html_entity_decode('Adem&aacute;s el Contratante deber&aacute; presentar la respectiva documentaci&oacute;n de respaldo.')),
											utf8_decode(html_entity_decode('Las partes aceptan el contenido &iacute;ntegro del presente documento, lo suscriben en tres ejemplares de igual contenido y valor el currentDay de currentMonth de currentYear.'))
											);
	
	$settlementTags['signature_pas'] = utf8_decode(html_entity_decode('FIRMA DE LA COMPA&Ntilde;&Iacute;A'));
	$settlementTags['signature_holder'] = 'FIRMA DE BENEFICIARIO/CONTRATANTE';
	$settlementTags['signature_holder_document'] = '         D.N.I./PAS.:';
?>
