<?php
/**
 * File: generateSettlement
 * Author: Patricio Astudillo
 * Creation Date: 10/04/2014
 * Last Modified: 10/04/2014
 * Modified By: Patricio Astudillo
*/
	$invoice_list = $_POST['requestItems'];
	
	//******************************** GATHERING GENERAL DATA FOR REPORT **************************
	$first_prequest_id = $invoice_list[0];
	
	$temp_prq = new PaymentRequest($db, $first_prequest_id);
	$temp_prq->getData();
	
	//********* THE FILE 
	$fileObject = new File($db, $temp_prq->getSerial_fle());
	$fileObject->getData();
	$fileObjectComplementaryData = $fileObject->getAux_data();
	
	//********* THE BENEFICIARY
	$beneficiaryObject = new Customer($db, $fileObject->getSerial_cus());
	$beneficiaryObject->getData();
	
	//******** THE AMOUNTS COVERED ON ALL SELECTED INVOICES
	$covered_benefits = array();
	$total_amount_requested = 0;
	$total_amount_paid = 0;
	$total_amount_refunded = 0;
	
	foreach($invoice_list as $prq_line){
		$prqLine = new PaymentRequest($db, $prq_line);
		$prqLine->getData();
		
		$documentObject = new DocumentsByFile($db, $prqLine->getSerial_dbf());
		$documentObject->getData();
		
		if($documentObject->getDocument_to_dbf() == 'CLIENT'){
			$documentToTag = 'Beneficiario';
		}else{
			$documentToTag = 'Red de Asistencia';
		}
		
		$benefitList = BenefitsByDocument::getBenefitsByDocuments($db, $prqLine->getSerial_dbf());
		$serviceList = BenefitsByDocument::getServicesByDocuments($db, $prqLine->getSerial_dbf());
		
		//Benefits
		if(is_array($benefitList)) {
			foreach ($benefitList as &$bl) {
				array_push($covered_benefits, array('benefit' => $bl['description_bbl'],
													'document_no' => $documentObject->getComment_dbf(),
													'coverage' => 'USD '.$bl['amount_presented_bbd'],
													'refund' => 'USD '.$bl['amount_authorized_bbd'],
													'recieved' => $documentToTag
						));
				
				$total_amount_requested += $bl['amount_presented_bbd'];
				if($documentObject->getDocument_to_dbf() == 'CLIENT'){
					$total_amount_refunded += $bl['amount_authorized_bbd'];
				}else{
					$total_amount_paid += $bl['amount_authorized_bbd'];
				}
			}
		}


		//Services
		if(is_array($serviceList)) {
			foreach ($serviceList as &$sl) {
				array_push($covered_benefits, array('benefit' => $sl['name_sbl'],
													'document_no' => $documentObject->getComment_dbf(),
													'coverage' => 'USD '.$sl['amount_presented_bbd'],
													'refund' => 'USD '.$sl['amount_authorized_bbd'],
													'recieved' => $documentToTag
						));
				
				$total_amount_requested += $sl['amount_presented_bbd'];
				if($documentObject->getDocument_to_dbf() == 'CLIENT'){
					$total_amount_refunded += $bl['amount_authorized_bbd'];
				}else{
					$total_amount_paid += $bl['amount_authorized_bbd'];
				}
			}
		}
	}
	
	//********* TOTAL SECTION
	$total_array = array('requested' => 'USD '.number_format($total_amount_requested, '2', '.', ','),
						 'aproved' => 'USD '.number_format($total_amount_refunded + $total_amount_paid, '2', '.', ','));
	
	//********* THE CARD AND BENEFITS DATA
	$settlementBasicData = CaseLiquidationGlobals::getSaleDataForSettlement($db, $fileObject->getSerial_sal());
	
	if(!$settlementBasicData){ //NO BASIC FUNCTION
		http_redirect('modules/assistance/liquidation/liquidateCase/2');
	}
	
	$tempHolderData = $settlementBasicData[0];
	
	$settlementDataArray = array();
	$settlementDataArray['contract_no'] = $tempHolderData['card_number_sal'];
	$settlementDataArray['begin_coverage'] = $tempHolderData['begin_date_sal'];
	$settlementDataArray['end_coverage'] = $tempHolderData['end_date_sal'];
	$settlementDataArray['product'] = $tempHolderData['name_pbl'];
	$tempEmissionArray = split('/', $tempHolderData['emission_date_sal']);
	$settlementDataArray['sale_day'] = $tempEmissionArray[0];
	$settlementDataArray['sale_month'] = $global_weekMonthsTwoDigits[$tempEmissionArray[1]];
	$settlementDataArray['sale_year'] = $tempEmissionArray[2];
	
	$settlementDataArray['file_no'] = $fileObject->getSerial_fle();
	$settlementDataArray['service'] = $global_fileTypes[$fileObject->getType_fle()];
	$settlementDataArray['service_date'] = $fileObject->getIncident_date_fle();
	$settlementDataArray['service_country'] = $fileObjectComplementaryData['name_cou'];
	$settlementDataArray['service_city'] = $fileObjectComplementaryData['name_cit'];
	
	
	$settlementDataArray['beneficiary'] = $beneficiaryObject->getFirstname_cus().' '.$beneficiaryObject->getLastname_cus();
	$settlementDataArray['beneficiary_document'] = $beneficiaryObject->getDocument_cus();
	$settlementDataArray['beneficiary_birthdate'] = $beneficiaryObject->getBirthdate_cus();
	$auckwardFix = split('/', $beneficiaryObject->getBirthdate_cus());
	$settlementDataArray['beneficiary_age'] = GlobalFunctions::getMyAge($auckwardFix[0].'/'.$auckwardFix[1].'/'.$auckwardFix[2]);
	$settlementDataArray['holder'] = $tempHolderData['holder'];
	$settlementDataArray['holder_document'] = $tempHolderData['holder_document'];
	$settlementDataArray['benefits_array'] = array();
	$settlementDataArray['coveredItems'] = $covered_benefits;
	$settlementDataArray['coveredItemsTotal'] = $total_array;
	
	foreach ($settlementBasicData as $benefit){
		$tempBenefitLineArray = array();
		$tempBenefitLineArray['tag'] = $benefit['description_bbl'];
		$tempBenefitLineArray['covergae_amount'] = ($benefit['price_bxp'] > 0)?$benefit['symbol_cur'].' '.$benefit['price_bxp']:$benefit['alias_cbl'].' ';
		$tempBenefitLineArray['covergae_amount'] .= ($benefit['restriction_price_bxp'])?$benefit['restriction_price_bxp']:'';
		
		array_push($settlementDataArray['benefits_array'], $tempBenefitLineArray);
	}
	
	if(count($settlementDataArray['coveredItems']) == 0 ||  count($settlementDataArray['benefits_array']) == 0){
		http_redirect('modules/assistance/liquidation/liquidateCase/3'); //NO COVERED DATA
	}
	
	//******************************** REGISTER ELEMENTS AS SETTLED *******************************
	$parameter = new Parameter($db, 50); //GENERAL LIQUIDATION NUMBERS
	$parameter->getData();
	Parameter::setNextValue($db, 50);
	
	$settlementObject = new Settlement($db);
	$settlementObject->setCreator($_SESSION['serial_usr']);
	$settlementObject->setNumber($parameter->getValue_par());
	$settlementObject->setTotal_refunded($total_amount_refunded);
	$settlementObject->setTotal_requested($total_amount_requested);
	$settlementObject->setTotal_paid($total_amount_paid);
	
	if(!$settlementObject->save()){
		http_redirect('modules/assistance/liquidation/liquidateCase/4'); //SETTLEMENT HEADER NOT SAVED
	}
	
	if(!Settlement::saveSettlementDetails($db, $settlementObject->getId(), $invoice_list)){
		http_redirect('modules/assistance/liquidation/liquidateCase/5'); //SETTLEMENT DETAILS NOT FULLY SAVED
	}
	
	$settlement_process = CaseLiquidationGlobals::settleCaseInvoices($db, implode(',',$invoice_list));
	
	if(!$settlement_process){
		http_redirect('modules/assistance/liquidation/liquidateCase/6'); //INVOICES NOT MARKED AS SETTLED
	}
	
	//******************************** PRINT PDF *******************************
	$settlement_id = $settlementObject->getId();
	include_once DOCUMENT_ROOT.'modules/assistance/liquidation/writeSettlementPDF.php';
	
	http_redirect('modules/assistance/liquidation/liquidateCase/1/'.$settlement_id); //INVOICE WELL GENERATED
?>
