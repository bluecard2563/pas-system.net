<?php
/**
 * File: fManageFiles
 * Author: Patricio Astudillo
 * Creation Date: 07-sep-2011, 9:16:59
 * Last Modified: 07-sep-2011, 9:16:59
 * Modified By:
*/

	/*LIQUIDATION PERMISSIONS*/
    // $parameter=new Parameter($db, '21');
    // $parameter->getData();
    // $permitedUsers=unserialize($parameter->getValue_par());

    // $serial_mbc = User::getManagerByUser($db, $_SESSION['serial_usr']);
    // if($serial_mbc=='1'){ //If the user is a PLANETASSIST's user, we set the country to Ecuador 62.
	// 	$serial_mbc='2';
    // }
    // $permitedUsers[$serial_mbc]=explode(',', $permitedUsers[$serial_mbc]);
    
	// if(is_array($permitedUsers[$serial_mbc])) {
	// 	foreach ($permitedUsers[$serial_mbc] as $p) {
	// 		if($_SESSION['serial_usr'] == $p) {
	// 			$has_permission = true;
	// 		}
	// 	}
	// }
	/*END LIQUIDATION PERMISSIONS*/

	// if($has_permission){
		$file_list = File::getFilesByStatusToManage($db, '\'in_liquidation\'');

		$smarty -> register('real_amount_spf,file_list');
	// }

	$smarty -> register('has_permission');
	$smarty -> display();
?>