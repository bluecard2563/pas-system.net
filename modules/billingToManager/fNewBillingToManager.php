<?php
/*
File: fNewBillingToManager
Author: David Bergmann
Creation Date: 05/10/2010
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$countryList = Manager::getCountriesWithManager($db);
$today = date('d/m/Y');

$par = new Parameter($db, 42); //PARAMETER FOR INTERNATIONAL INVOICING
$par->getData();
$invoice_number = $par->getValue_par();
$invoice_number = str_pad($invoice_number, 5, '0', STR_PAD_LEFT);

$smarty->register('error,countryList,today,invoice_number');
$smarty->display();
?>