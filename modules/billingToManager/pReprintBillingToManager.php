<?php
/**
 * File: pReprintBillingToManager
 * Author: Patricio Astudillo
 * Creation Date: 14-ago-2012
 * Last Modified: 14-ago-2012
 * Modified By: Patricio Astudillo
 */

	Request::setInteger('selManager');
	Request::setString('txtInvoiceNumber');
	
	$serial_btm = BillingToManager::checkBillForReprint($db, $selManager, $txtInvoiceNumber);
	
	if(!$serial_btm){
		http_redirect('modules/billingToManager/fReprintBillingToManager/no_invoice');
	}
	
	$billingToManager = new BillingToManager($db, $serial_btm);
	$billingToManager ->getData();
	
	$manager = new Manager($db, $billingToManager->getSerial_man());
	$manager->getData();
	$aux_data = $manager->getAuxData_man();
	
	include_once DOCUMENT_ROOT.'modules/billingToManager/pInvoiceBlueprint.php';
	
	$pdf -> ezStream();
?>
