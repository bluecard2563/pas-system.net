<?php
/*
File: fPayBillingToManagerByCountry
Author: Nicolás Flores P.
Creation Date: 16/06/2011
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
Request::setString('1:fileName');

$countryList = Manager::getCountriesWithManager($db);
if($fileName){
	if(file_exists(DOCUMENT_ROOT.'modules/billingToManager/reports/'.$fileName.'.pdf')){
		$smarty->register('fileName');
	}
}
$smarty->register('error,countryList');
$smarty->display();
?>