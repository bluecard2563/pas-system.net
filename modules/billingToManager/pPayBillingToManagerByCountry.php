<?php
/*
File: pPayBillingToManagerByCountry
Author: Nicolás Flores
Creation Date: 16/06/2011
Last Modified:
Modified By:
*/
$invoice_number= $_POST['hdnDocumentNumber'];
$serial_cou=$_POST['selCountry'];
$pay_bill=true;
include_once DOCUMENT_ROOT.'rpc/billingToManager/reprintBillingCountryReport.rpc.php';
$error = false;
if(!$pdfReady){
	ErrorLog::log($db, 'pPayBillingToManagerByCountry ', 'pdf NOT READY');
	$error=true;
}else{
	if(is_array($managersFromCountry)){
		$sales=new Sales($db);
		
		//pay international fee for each manager
		foreach($managersFromCountry as $mfc){	
			if(!$sales->payInternationaFee($mfc['serial_man'], $mfc['serial_btm'])){
				$error = true;
				break;
			}
			$billingToManager = new BillingToManager($db, $mfc['serial_btm']);
			$billingToManager->getData();
			$billingToManager->setStatus_btm('PAID');
			if(!$billingToManager->update()){
				$error = true;
				break;
			}
		}
	} else{
		$error = true;
	}
}
if($error){
	http_redirect('modules/billingToManager/fPayBillingToManagerByCountry/2');
}else{
	http_redirect('modules/billingToManager/fPayBillingToManagerByCountry/1/'.$pdfname);
}
?>