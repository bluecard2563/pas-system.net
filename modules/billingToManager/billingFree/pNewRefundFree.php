<?php
/*
 * File: pNewRefundSales.php
 * Author: Nicolas Flores
 * Creation Date: 27/06/2011
 */
Request::setString('selectedBoxes');
if($selectedBoxes !=''){
	$selectedBoxes = str_replace('&',',',str_replace('=on', '', $selectedBoxes));
	if(Sales::updateInternationalFeeToRefund($db,$selectedBoxes,TRUE)){
		http_redirect('modules/billingToManager/billingFree/fNewRefundFree/1');
	}else{
		http_redirect('modules/billingToManager/billingFree/fNewRefundFree/2');
	}
}else
{
	http_redirect('modules/billingToManager/billingFree/fNewRefundFree/3');
}
?>
