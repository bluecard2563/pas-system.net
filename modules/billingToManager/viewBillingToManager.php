<?php
/**
 * File: viewBillingToManager
 * Author: Patricio Astudillo
 * Creation Date: 17/12/2013
 * Last Modified: 17/12/2013
 * Modified By: Patricio Astudillo
 */
	set_time_limit(36000);
	ini_set('memory_limit', '512M');
	include_once DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';

	Request::setString('0:serial_btm');
	Request::setString('1:reprint_type');
	
	if($reprint_type == 'pending'){
		$reprint_type_string = 'PRE-LIQUIDACI&Oacute;N';
	}else{
		$reprint_type_string = 'FACTURA';
	}
	
	$header = BillingDetail::getInvoiceHeader($db, $serial_btm);
	$sales = BillingDetail::getInvoiceFullDetailBySerialBtm($db, $serial_btm);
	$header['total_cards'] = count($sales);
	//Debug::print_r($header); die;
	
	//HEADER
	$pdf->ezText('<b>' . html_entity_decode("REIMPRESI&Oacute;N DE $reprint_type_string DE REPRESENTANTES") . '</b>', 14, array('justification' => 'center'));
	$pdf->ezSetDy(-20);

	$pdf->ezText('<b>' . utf8_decode("Representante: ") . '</b>' . $header['name_man'], 9, array('justification' => 'left'));
	$pdf->ezSetDy(-1);

	$pdf->ezText('<b>' . utf8_decode("Fecha de reporte actual: ") . '</b>' . $header['date_man'], 9, array('justification' => 'left'));
	$pdf->ezSetDy(-1);
	$pdf->ezText('<b>' . html_entity_decode("N&uacute;mero de tarjetas vendidas: ") . '</b>' . $header['total_cards'], 9, array('justification' => 'left'));
	$pdf->ezSetDy(-20);
		
	if($sales){
		$salesToShow = array();
		$total = 0;

		foreach ($sales as $s) {
			//data for PDF
			$aux = array();
			$aux['card_number_sal'] = $s['card_number_sal'];
			$aux['status_sal'] = $global_salesStatus[$s['status_sal']];
			$aux['in_date_sal'] = $s['in_date_sal'];
			$aux['name_cus'] = $s['name_cus'];
			$aux['name_pbl'] = $s['name_pbl'];
			$aux['begin_date_sal'] = $s['begin_date_sal'];
			$aux['end_date_sal'] = $s['end_date_sal'];
			$aux['days_sal'] = $s['days_sal'];
			$aux['total_sal'] = $s['total_sal'];
			$aux['name_dea'] = $s['name_dea'];
			$aux['name_usr'] = $s['name_usr'];

			if ($s['international_fee_status'] == 'REFUND') {
				$aux['international_fee_status'] = "A Reembolsar";
				$aux['international_fee_amount'] = "(" . $s['international_fee_amount'] . ")";
				$total -= $s['international_fee_amount'];
			} else {
				$aux['international_fee_status'] = "A Cobrar";
				$aux['international_fee_amount'] = $s['international_fee_amount'];
				$total += $s['international_fee_amount'];
			}
			array_push($salesToShow, $aux);
		}

		//adds total row
		$aux = array();
		$aux['card_number_sal'] = '';
		$aux['status_sal'] = '';
		$aux['in_date_sal'] = '';
		$aux['name_cus'] = '';
		$aux['name_pbl'] = '';
		$aux['begin_date_sal'] = '';
		$aux['end_date_sal'] = '';
		$aux['days_sal'] = '';
		$aux['total_sal'] = '';
		$aux['name_dea'] = '';
		$aux['name_usr'] = '';
		$aux['international_fee_status'] = "<b>Total:</b>";
		$aux['international_fee_amount'] = number_format($total, 2, '.', '');
		array_push($salesToShow, $aux);


		$titles = array('card_number_sal' => utf8_decode('<b># Tarjeta</b>'),
			'status_sal' => '<b>Estado de la Tarjeta</b>',
			'in_date_sal' => html_entity_decode('<b>Fec. Emisi&oacute;n</b>'),
			'name_cus' => utf8_decode('<b>Cliente</b>'),
			'name_pbl' => utf8_decode('<b>Producto</b>'),
			'begin_date_sal' => utf8_decode('<b>Desde</b>'),
			'end_date_sal' => utf8_decode('<b>Hasta</b>'),
			'days_sal' => utf8_decode('<b>Tiempo</b>'),
			'total_sal' => utf8_decode('<b>Precio</b>'),
			'name_dea' => utf8_decode('<b>Comercializador</b>'),
			'name_usr' => utf8_decode('<b>Counter</b>'),
			'international_fee_status' => utf8_decode('<b>Estado</b>'),
			'international_fee_amount' => utf8_decode('<b>Monto</b>'));

		$pdf->ezTable($salesToShow, $titles, '', array('showHeadings' => 1,
			'shaded' => 1,
			'showLines' => 2,
			'xPos' => 'center',
			'innerLineThickness' => 0.8,
			'outerLineThickness' => 0.8,
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'card_number_sal' => array('justification' => 'center', 'width' => 40),
				'status_sal' => array('justification' => 'center', 'width' => 55),
				'in_date_sal' => array('justification' => 'center', 'width' => 55),
				'name_cus' => array('justification' => 'center', 'width' => 90),
				'name_pbl' => array('justification' => 'center', 'width' => 60),
				'begin_date_sal' => array('justification' => 'center', 'width' => 55),
				'end_date_sal' => array('justification' => 'center', 'width' => 55),
				'days_sal' => array('justification' => 'center', 'width' => 40),
				'total_sal' => array('justification' => 'center', 'width' => 50),
				'name_dea' => array('justification' => 'center', 'width' => 90),
				'name_usr' => array('justification' => 'center', 'width' => 90),
				'international_fee_status' => array('justification' => 'center', 'width' => 70),
				'international_fee_amount' => array('justification' => 'center', 'width' => 50))));

		$pdf->ezSetDy(-20);
		$pdf->ezText('<b>' . utf8_decode("Nota: ") . '</b>' . html_entity_decode("Si el monto aparece con par&eacute;ntesis '()', &Eacute;ste monto es a favor del representante."), 8, array('justification' => 'left'));
	}else{
		$pdf->ezText('<i>No se pudo cargar el listado completo</i>', 12, array('justification' => 'center'));
	}
		
	$pdf->ezStream();
?>
