<?php

/*
  File: pNewBillingToManagerByCountry
  Author: Nicolás FLores
  Creation Date: 14/06/2011
  Last Modified:
  Modified By:
 */
set_time_limit(36000);
ini_set('memory_limit', '512M');
include_once DOCUMENT_ROOT . 'lib/PDFHeadersHorizontal.inc.php';

Request::setString('txtToDate');
Request::setInteger('selCountry');

if ($selCountry) {
	$country = new Country($db, $selCountry);
	$country->getData();
	$manager = new Manager($db);
	$params = array("c.serial_cou" => $selCountry);
	$managersFromCountry = $manager->getManagers($params);

	if ($managersFromCountry) {
		//titles for each Table
		$titles = array('card_number_sal' => utf8_decode('<b># Tarjeta</b>'),
			'status_sal' => '<b>Estado de la Tarjeta</b>',
			'in_date_sal' => html_entity_decode('<b>Fec. Emisi&oacute;n</b>'),
			'name_cus' => utf8_decode('<b>Cliente</b>'),
			'name_pbl' => utf8_decode('<b>Producto</b>'),
			'begin_date_sal' => utf8_decode('<b>Desde</b>'),
			'end_date_sal' => utf8_decode('<b>Hasta</b>'),
			'days_sal' => utf8_decode('<b>Tiempo</b>'),
			'total_sal' => utf8_decode('<b>Precio</b>'),
			'name_dea' => utf8_decode('<b>Comercializador</b>'),
			'name_usr' => utf8_decode('<b>Counter</b>'),
			'international_fee_status' => utf8_decode('<b>Estado</b>'),
			'international_fee_amount' => utf8_decode('<b>Monto</b>'));
		//HEADER
		$pdf->ezText('<b>' . html_entity_decode("FACTURACI&Oacute;N A REPRESENTANTES DE ") . strtoupper($country->getName_cou()) . '</b>', 14, array('justification' => 'center'));
		$pdf->ezSetDy(-5);
		$tables_data = array(); //array with all the data for the PDF tables.
		$tables_data['invoice_total'] = 0;
		$tables_data['managers'] = array();
		foreach ($managersFromCountry as $man) {
			$sales = Manager::getBillingToManagerReport($db, $man['serial_man'], $_POST['rdoOrderBy'], NULL, $_POST['txtToDate']);
			$manager = new Manager($db, $man['serial_man']);
			$manager->getData();
			$tables_data[$man['serial_man']] = array();
			$tables_data['managers'][$man['serial_man']]['name_man'] = $manager->getName_man();
			if (!$txtToDate) {
				$date = date('d/m/Y');
			} else {
				$date = $txtToDate;
			}
			$tables_data['managers'][$man['serial_man']]['date'] = $date;
			$billingToManager=new BillingToManager($db);
			$lastDate = $billingToManager->getLastBillToManager($man['serial_man']);
			if (!$lastDate) {
				$lastDate = 'N/A';
			}
			$tables_data['managers'][$man['serial_man']]['last_date'] = $lastDate;
			if (is_array($sales)) {
				$totalSales = sizeof($sales);
			} else {
				$totalSales = 0;
			}
			$tables_data['managers'][$man['serial_man']]['total_sales'] = $totalSales;
			$salesToShow = array();
			$total = 0;
			if (is_array($sales)) {
				foreach ($sales as $s) {
					//data for PDF
					$aux = array();
					$aux['card_number_sal'] = $s['card_number_sal'];
					$aux['status_sal'] = $global_salesStatus[$s['status_sal']];
					$aux['in_date_sal'] = $s['in_date_sal'];
					$aux['name_cus'] = $s['name_cus'];
					$aux['name_pbl'] = $s['name_pbl'];
					$aux['begin_date_sal'] = $s['begin_date_sal'];
					$aux['end_date_sal'] = $s['end_date_sal'];
					$aux['days_sal'] = $s['days_sal'];
					$aux['total_sal'] = $s['total_sal'];
					$aux['name_dea'] = $s['name_dea'];
					$aux['name_usr'] = $s['name_usr'];

					if ($s['international_fee_status'] == 'TO_REFUND') {
						$aux['international_fee_status'] = "A Reembolsar";
						$aux['international_fee_amount'] = "(" . $s['international_fee_amount'] . ")";
						$total -= $s['international_fee_amount'];
					} else {
						$aux['international_fee_status'] = "A Cobrar";
						$aux['international_fee_amount'] = $s['international_fee_amount'];
						$total += $s['international_fee_amount'];
					}
					array_push($salesToShow, $aux);
				}
				//insert in database
				$billingToManager = new BillingToManager($db);
				$billingToManager->setSerial_man($man['serial_man']);
				$billingToManager->setDate_btm($date);
				$billingToManager->setAmount_btm($total);
				$billingToManager->setBilling_number_btm($_POST['txtDocNumber']);
				$billingToManager->setType_btm('COUNTRY');

				if (!$billingToManager->insert()) {
					http_redirect('modules/billingToManager/fNewBillingToManagerByCountry/1');
				}
			}
			$aux = array();
			$aux['card_number_sal'] = '';
			$aux['status_sal'] = '';
			$aux['in_date_sal'] = '';
			$aux['name_cus'] = '';
			$aux['name_pbl'] = '';
			$aux['begin_date_sal'] = '';
			$aux['end_date_sal'] = '';
			$aux['days_sal'] = '';
			$aux['total_sal'] = '';
			$aux['name_dea'] = '';
			$aux['name_usr'] = '';
			$aux['international_fee_status'] = "<b>Total:</b>";
			$aux['international_fee_amount'] = number_format($total, 2, '.', '');
			array_push($salesToShow, $aux);
			$tables_data['managers'][$man['serial_man']]['sales_to_show'] = $salesToShow;
			$tables_data['invoice_total']+= $total;
		}//end foreach	
		$pdf->ezText('<b>' . html_entity_decode("Monto total de ventas: ") . $tables_data['invoice_total'] . '</b>', 14, array('justification' => 'center'));
		$pdf->ezSetDy(-20);
		foreach ($tables_data['managers'] as $td) {
			$pdf->ezText('<b>' . utf8_decode("Representante: ") . '</b>' . $td['name_man'], 9, array('justification' => 'left'));
			$pdf->ezSetDy(-1);
			$pdf->ezText('<b>' . utf8_decode("Fecha de reporte anterior: ") . '</b>' . $td['last_date'], 9, array('justification' => 'left'));
			$pdf->ezSetDy(-1);
			$pdf->ezText('<b>' . utf8_decode("Fecha de reporte actual: ") . '</b>' . $td['date'], 9, array('justification' => 'left'));
			$pdf->ezSetDy(-1);
			$pdf->ezText('<b>' . utf8_decode("Número de tarjetas vendidas: ") . '</b>' . $td['total_sales'], 9, array('justification' => 'left'));
			$pdf->ezSetDy(-20);

			$pdf->ezTable($td['sales_to_show'], $titles, '', array('showHeadings' => 1,
				'shaded' => 1,
				'showLines' => 2,
				'xPos' => 'center',
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8,
				'fontSize' => 8,
				'titleFontSize' => 8,
				'cols' => array(
					'card_number_sal' => array('justification' => 'center', 'width' => 40),
					'status_sal' => array('justification' => 'center', 'width' => 55),
					'in_date_sal' => array('justification' => 'center', 'width' => 55),
					'name_cus' => array('justification' => 'center', 'width' => 90),
					'name_pbl' => array('justification' => 'center', 'width' => 60),
					'begin_date_sal' => array('justification' => 'center', 'width' => 55),
					'end_date_sal' => array('justification' => 'center', 'width' => 55),
					'days_sal' => array('justification' => 'center', 'width' => 40),
					'total_sal' => array('justification' => 'center', 'width' => 50),
					'name_dea' => array('justification' => 'center', 'width' => 90),
					'name_usr' => array('justification' => 'center', 'width' => 90),
					'international_fee_status' => array('justification' => 'center', 'width' => 70),
					'international_fee_amount' => array('justification' => 'center', 'width' => 50))));

			$pdf->ezSetDy(-20);
			$pdf->ezText('<b>' . utf8_decode("Nota: ") . '</b>' . utf8_decode("Si el monto aparece con paréntesis '()', éste monto es a favor del representante."), 8, array('justification' => 'left'));
			$pdf->ezSetDy(-50);
		}
		$pdf->ezStream();
	} else {//end if(managersByCountry)
		http_redirect('modules/billingToManager/fNewBillingToManagerByCountry/2');
	}
} else {//end if($selCountry)
	http_redirect('modules/billingToManager/fNewBillingToManagerByCountry/3');
}
?>