<?php
/*
File: fNewBillingToManagerByCountry.php
Author: Nicolás Flores
Creation Date: 14/06/2011
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$countryList = Manager::getCountriesWithManager($db);
$today = date('d/m/Y');

$smarty->register('error,countryList,today');
$smarty->display();
?>