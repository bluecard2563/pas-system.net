<?php
/**
 * File: fReprintBillingToManager
 * Author: Patricio Astudillo
 * Creation Date: 14-ago-2012
 * Last Modified: 14-ago-2012
 * Modified By: Patricio Astudillo
 */
	
	Request::setString('0:error');
	
	$countryList = Manager::getCountriesWithManager($db);
	
	$smarty->register('error,countryList');
	$smarty->display();

?>
