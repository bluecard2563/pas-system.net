<?php
/*
 * File: pNewRefundSales.php
 * Author: Nicolas Flores
 * Creation Date: 21/06/2011
 */
Request::setString('selectedBoxes');
if($selectedBoxes !=''){
	$selectedBoxes = str_replace('&',',',str_replace('=on', '', $selectedBoxes));
	if(Sales::updateInternationalFeeToRefund($db, $selectedBoxes, TRUE)){
		http_redirect('modules/billingToManager/refundSales/fNewRefundSales/1');
	}else{
		http_redirect('modules/billingToManager/refundSales/fNewRefundSales/2');
	}
}else
{
	http_redirect('modules/billingToManager/refundSales/fNewRefundSales/3');
}
?>
