<?php
/**
 * File: pInvoiceBlueprint
 * Author: Patricio Astudillo
 * Creation Date: 31-jul-2012
 * Last Modified: 31-jul-2012
 * Modified By: Patricio Astudillo
 */

	$pdf =& new Cezpdf('A4','portrait');
	//WIDTH: 600
	//HEIGHT: 800
	set_time_limit(36000);
	ini_set('memory_limit','1024M');

	$pdf -> selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
	$pdf -> ezSetCmMargins(1,3,2,1);
	$all = $pdf->openObject();
	$pdf -> saveState();
	
	$pdf->addJpegFromFile(DOCUMENT_ROOT."img/contract_logos/logopas_contracts.jpg", 60, 750, 250);
	$pdf->setColor(0, 0, 0);

	$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
	$pdf->restoreState();
	$pdf->closeObject();
	$pdf->addObject($all,'all');
	
	$header_table[] = array('col1' => '<b>DATE</b>',
							'col2' => '<b>INVOICE</b>');
	$header_table[] = array('col1' => $billingToManager ->getBilling_date_btm(),
							'col2' => 'CM - '.$billingToManager ->getBilling_number_btm());
	$header_table[] = array('col1' => '<b>TERM</b>',
							'col2' => '<b>DUE DATE</b>');
	$header_table[] = array('col1' => '0',
							'col2' => $billingToManager ->getBilling_date_btm());
	
	$pdf->ezTable(	$header_table, 
					NULL, 
					NULL, 
					array(
						'showLines' => 2,
						'showHeadings' => 0,
						'shaded' => 0,
						'fontSize' => 10,
						'textCol' => array(0, 0, 0),
						'titleFontSize' => 12,
						'rowGap' => 2,
						'colGap' => 2,
						'xPos' => 450,
						'maxWidth' => 805,
						'cols' => array('col1' => array('justification'=>'center','width'=>80),
										'col2' => array('justification'=>'center','width'=>80)),
						'innerLineThickness' => 0.5,
						'outerLineThickness' => 0.5
					)
				);

	$pdf->ezSetDy(-30);
	
	$manager_data = array();
	$temp = array(	'col1' => $manager->getName_man());						
	array_push($manager_data, $temp);
	$temp = array(	'col1' => $manager->getAddress_man());						
	array_push($manager_data, $temp);
	$temp = array(	'col1' => $aux_data['geographic_info']);						
	array_push($manager_data, $temp);
	$temp = array(	'col1' => $manager->getPhone_man());						
	array_push($manager_data, $temp);
	
	//Debug::print_r($manager_data); die;
	$pdf -> ezTable($manager_data,
					NULL,
					'<b>BILL TO</b>',
					array(
						'showLines' => 2,
						'showHeadings' => 0,
						'shaded' => 0,
						'fontSize' => 10,
						'textCol' => array(0, 0, 0),
						'titleFontSize' => 12,
						'rowGap' => 2,
						'colGap' => 2,
						'xPos' => 'center',
						'maxWidth' => 805,
						'cols' => array('col1' => array('justification'=>'center','width'=>300)),
						'innerLineThickness' => 0.5,
						'outerLineThickness' => 0.5
						)
					);
	
	$pdf->ezSetDy(-20);
	
	$summary_table = array();
	$temp = array(	'col1' => '<b>AMOUNT DUE</b>',
					'col2' => '<b>ENCLOSED</b>');						
	array_push($summary_table, $temp);
	$temp = array(	'col1' => 'USD '.  number_format($billingToManager ->getAmount_btm(), 2),
					'col2' => '');
	array_push($summary_table, $temp);
	
	$pdf->ezTable(	$summary_table, 
					NULL, 
					NULL, 
					array(
						'showLines' => 2,
						'showHeadings' => 0,
						'shaded' => 0,
						'fontSize' => 10,
						'textCol' => array(0, 0, 0),
						'titleFontSize' => 12,
						'rowGap' => 2,
						'colGap' => 2,
						'xPos' => 450,
						'maxWidth' => 805,
						'cols' => array('col1' => array('justification'=>'center','width'=>80),
										'col2' => array('justification'=>'center','width'=>80)),
						'innerLineThickness' => 0.5,
						'outerLineThickness' => 0.5
					)
				);
	
	
	$pdf->ezSetDy(-20);
	
	$summary_table = array();
	$summary_titles = array(	'col1' => '<b>DATE</b>',
								'col2' => '<b>SERVICE</b>',
								'col3' => '<b>QUANTITY</b>',
								'col4' => '<b>RATE</b>',
								'col5' => '<b>AMOUNT</b>');
	$invoice_month = $billingToManager->getDate_btm();
	$invoice_month = explode('/', $invoice_month);
	
	$temp = array(	'col1' => $billingToManager ->getBilling_date_btm(),
					'col2' => 'Tarjetas de asistencia en viajes correspondientes al mes de '.$global_weekMonthsTwoDigits[$invoice_month['1']].' de '.$invoice_month['2'],
					'col3' => '',
					'col4' => '',
					'col5' => number_format($billingToManager ->getAmount_btm(), 2));
	array_push($summary_table, $temp);
	
	$temp = array(	'col1' => '',
					'col2' => '',
					'col3' => '',
					'col4' => '',
					'col5' => '');
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	array_push($summary_table, $temp);
	
	$pdf->ezTable(	$summary_table, 
					$summary_titles, 
					NULL, 
					array(
						'showLines' => 1,
						'showHeadings' => 1,
						'shaded' => 0,
						'fontSize' => 10,
						'textCol' => array(0, 0, 0),
						'titleFontSize' => 12,
						'xPos' => 'center',
						'maxWidth' => 805,
						'cols' => array('col1' => array('justification'=>'center','width'=>70),
										'col2' => array('justification'=>'center','width'=>250),
										'col3' => array('justification'=>'center','width'=>70),
										'col4' => array('justification'=>'center','width'=>60),
										'col5' => array('justification'=>'center','width'=>80)),
						'innerLineThickness' => 0.5,
						'outerLineThickness' => 0.5
					)
				);
	
	$total_table = array();
	$temp = array(	'col1' => '<b>TOTAL</b>',
					'col2' => 'USD '.number_format($billingToManager ->getAmount_btm(), 2),
					);
	array_push($total_table, $temp);
	$pdf->ezTable(	$total_table, 
					NULL, 
					NULL, 
					array(
						'showLines' => 1,
						'showHeadings' => 0,
						'shaded' => 0,
						'fontSize' => 10,
						'textCol' => array(0, 0, 0),
						'titleFontSize' => 12,
						'xPos' => 'center',
						'maxWidth' => 805,
						'cols' => array('col1' => array('justification'=>'center','width'=>450),
										'col2' => array('justification'=>'center','width'=>80)),
						'innerLineThickness' => 0.5,
						'outerLineThickness' => 0.5
					)
				);
	
?>
