<?php
/*
 * File: pChooseUncollectableInvoices.php
 * Author: Patricio Astudillo
 * Creation Date: 23/06/2010, 11:19:33 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 23/06/2010, 11:19:33 AM
 */

$invoice=new Invoice($db);


//$uncollectables=$_POST['check'];
Request::setString('selectedBoxes');
$selectedBoxes = str_replace('&',',',str_replace('check=', '', $selectedBoxes));
$uncollectables=explode(",",$selectedBoxes);
$dbm = new DocumentByManager($db);
$used_cnotes = array();

foreach($uncollectables as $u){
	$payment=new Payment($db);
	$pDetail=new PaymentDetail($db);
	$invoice -> setSerial_inv($u);
	$invoice -> getData();
	
	/**** CREDIT NOTE GENERAL INFORMATION ****/
	$cnote_information['db'] = $db;
	$cnote_information['type'] = "PAYMENT";
	$cnote_information['paymentStatus'] = 'PAID';
	
	if($invoice->getSerial_cus()):
		$aux_type_cus = $invoice->getAuxData();
		$cnote_information['person_type'] = $aux_type_cus['type_cus'];
	else:
		$cnote_information['person_type'] = 'LEGAL_ENTITY';
	endif;
	
	$dbm ->setSerial_dbm($invoice->getSerial_dbm());
	$dbm ->getData();
	$cnote_information['serial_man'] = $dbm->getSerial_man();
	/**** CREDIT NOTE GENERAL INFORMATION ****/
	

	if($invoice->getSerial_pay()!=''){ //The payment is PARTIAL
		$payment->setSerial_pay($invoice->getSerial_pay());
		$payment->getData();
		$uncAmount = $payment->getTotal_to_pay_pay()-$payment->getTotal_payed_pay();
		$cnote_information['amount'] = $uncAmount;
		$cnote_information['serial'] = $payment->getSerial_pay();
		
		//****** CREATE THE CREDIT NOTE **********
		$cnote = GlobalFunctions::generateCreditNote($cnote_information);

		/*FILL THE PAYMENTS DETAIL*/
		$pDetail->setSerial_pay($payment->getSerial_pay());
		$pDetail->setSerial_cn($cnote['id']);
		$pDetail->setSerial_usr($_SESSION['serial_usr']);
		$pDetail->setAmount_pyf($uncAmount);
		$pDetail->setDocumentNumber_pyf($cnote['number']);
		$pDetail->setChecked_pyf('YES');
		$pDetail->setType_pyf('UNCOLLECTABLE');
		$pDetail->setStatus_pyf('ACTIVE');
		$pDetail->setComments_pyf(html_entity_decode('Nota de cr&eacute;dito - Incobrable'));
		$pDetail->insert();
		/*END PAYMENT DETAIL*/

		/*UPDATING THE PAYMENT*/
		$payment->setTotal_payed_pay($payment->getTotal_to_pay_pay());
		$payment->setStatus_pay('UNCOLLECTABLE');
		$payment->setExcess_amount_available_pay('0');
		$payment->update();
		
		ERP_logActivity('update', $payment);//ERP ACTIVITY
		/*END PAYMENT*/
	}else{ //NO PAYMENT IS REGISTERED
		$uncAmount = $invoice->getTotal_inv();
		$payment->setStatus_pay('UNCOLLECTABLE');
		$payment->setExcess_amount_available_pay('0');
		$payment->setObservation_pay('Cuenta Incobrable');
		$payment->setTotal_payed_pay($uncAmount);
		$payment->setTotal_to_pay_pay($uncAmount);
		$serial_pay=$payment->insert();
		
		ERP_logActivity('new', $payment);//ERP ACTIVITY
		
		$cnote_information['amount'] = $uncAmount;
		$cnote_information['serial'] = $serial_pay;
		
		//****** CREATE THE CREDIT NOTE **********
		$cnote = GlobalFunctions::generateCreditNote($cnote_information);

		/*FILL THE PAYMENTS DETAIL*/
		$pDetail->setSerial_pay($serial_pay);
		$pDetail->setSerial_cn($cnote['id']);
		$pDetail->setSerial_usr($_SESSION['serial_usr']);
		$pDetail->setAmount_pyf($uncAmount);
		$pDetail->setDocumentNumber_pyf($cnote['number']);
		$pDetail->setChecked_pyf('YES');
		$pDetail->setType_pyf('UNCOLLECTABLE');
		$pDetail->setStatus_pyf('ACTIVE');
		$pDetail->setComments_pyf(html_entity_decode('Nota de cr&eacute;dito - Incobrable'));
		$pDetail->insert();
		/*END PAYMENT DETAIL*/

		$invoice->setSerial_pay($serial_pay);
	}
	
	array_push($used_cnotes, $cnote['number']);

	/*UPDATING THE INVOICE*/
	$invoice->setStatus_inv('UNCOLLECTABLE');
	if($invoice->update()){
		ERP_logActivity('update', $invoice);//ERP ACTIVITY
		
		$error=1;
	}else{
		$error=2;
		break;
	}
	/*END INVOICE*/
}

$_SESSION['cnotes_numbers'] = $used_cnotes;

http_redirect('modules/payments/fChooseUncollectableInvoices/'.$error);
?>
