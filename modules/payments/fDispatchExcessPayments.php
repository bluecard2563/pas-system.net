<?php
/*
 * File: fDispatchExcessPayments.php
 * Author: Patricio Astudillo
 * Creation Date: 17/05/2010, 12:19:14 PM
 * Modifies By: Patricio Astudillo
 * Last Modified: 17/05/2010, 12:19:14 PM
 */

Request::setString('0:error');
$excessCountries=Country::getCountriesWithExcessPayments($db);

//CATEGORY
$dealer=new Dealer($db);
$categoryList=$dealer->getCategories();

$smarty->register('excessCountries,categoryList,error');
$smarty->display();
?>
