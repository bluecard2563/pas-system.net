<?php
/**
 * File: pGlobalPayment
 * Author: Patricio Astudillo
 * Creation Date: 07/02/2012
 * Modified By: Ptricio Astudillo
 * Last Modified: 07/02/2012
 */

	$_SESSION['current_sale_products'] = 'SERVCODE';
	$_SESSION['current_sale_total'] = round($_POST['txtTotalAmount'], 2);
	
	$description = 'PAS GLOBAL SERVICES';
	
	include(DOCUMENT_ROOT.'lib/merchantAccountConnection.inc.php');
	
	$report_information = array();
	$report_information['serial_usr'] = $_SESSION['serial_usr'];
	$report_information['amount_paid'] = round($_POST['txtTotalAmount'], 2);
	$report_information['payment_document'] = $_SESSION['ccnum'];
	$report_information['payment_date'] = date('d/m/Y H:i');
	$report_information['observations'] = $_POST['txtObservations'];
	unset($_SESSION['ccnum']);
	

	if($payment_completed) {
		$error = 1;
		$transaction_response = 0; //NO ERRORS
	} else {
		$report_information['error_code'] = $custom_error_code;
		$error = 0;
		ErrorLog::log($db, 'MERCHANT_ACCOUNT_SERVICE_PAYMENT_ERROR', $report_information);
	}
	
	http_redirect('modules/payments/creditCardPayments/fGlobalPayment/'.$error.'/'.$transaction_response);
?>
