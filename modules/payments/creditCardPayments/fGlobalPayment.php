<?php
/**
 * File: fGlobalPayment
 * Author: Patricio Astudillo
 * Creation Date: 07/02/2012
 * Modified By: Ptricio Astudillo
 * Last Modified: 07/02/2012
 */

	Request::setInteger('0:error');
	Request::setInteger('1:merchantError');

	$countryList = Country::getAllCountries($db,false);
	$yearlist = array();
    $year=date('Y');
    for($i=0; $i<11; $i++) {
        array_push($yearlist, $year+$i);
    }		

	$smarty -> register('countryList,yearlist,merchantError,error');
	$smarty->display();
?>
