<?php

/*
 * File: pDispatchExcessPayments.php
 * Author: Patricio Astudillo
 * Creation Date: 18/05/2010, 09:06:09 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 18/05/2010, 09:06:09 AM
 */

//Debug::print_r($_POST);
$payment = new Payment($db);
if(isset($_POST['chkPayment'])){
	$item_list = $_POST['chkPayment'];
}else{
	if(strstr($_POST['selectedPayments'], ',')){
		$item_list = explode(',', $_POST['selectedPayments']);
	}else{
		$item_list = $_POST['selectedPayments'];
	}
}

if ($item_list) {
	$paymentsSerials = implode(',', $item_list);
	$paymentData = $payment->getDispatchedPayments($paymentsSerials);

	if (is_array($paymentData)) {
		$receiver = $paymentData['0']['receiver'];
		$branch = new Dealer($db, $_POST['selBranch']);
		$branch->getData();
		$total_to_pay = 0;
		$details = array();
		$cont = 1;
		foreach ($paymentData as $pl) {
			$total_to_pay+=$pl['excess_amount'];
			$descLine1 = array('col1' => $cont,
				'col8' => $pl['serial_pay'],
				'col2' => $pl['payment_number'],
				'col3' => $pl['date_pay'],
				'col4' => $pl['total_to_pay_pay'],
				'col5' => $pl['total_payed_pay'],
				'col6' => $pl['excess_amount'],
				'col7' => $pl['number_inv']
			);
			array_push($details, $descLine1);
			$cont++;
		}
		$total_to_pay = number_format($total_to_pay, '2');

		include_once DOCUMENT_ROOT . 'lib/PDFHeadersVertical.inc.php';

		//HEADER
		$pdf->ezText(utf8_decode('LIQUIDACIÓN DE PAGOS EN EXCESO'), 12, array('justification' => 'center')); //title of each page

		$pdf->ezSetDy(-10);
		$headerDesc = array();
		$descLine1 = array('col1' => utf8_decode('<b>Liquidación a nombre de:</b>'),
			'col2' => $receiver);
		array_push($headerDesc, $descLine1);
		$descLine2 = array('col1' => '<b>Fecha:</b>',
			'col2' => date('d/m/Y'));
		array_push($headerDesc, $descLine2);
		$descLine3 = array('col1' => '<b>Monto a Liquidar:</b>',
			'col2' => '$ ' . $total_to_pay);
		array_push($headerDesc, $descLine3);
		$pdf->ezTable($headerDesc, array('col1' => '', 'col2' => ''), '', array('showHeadings' => 0,
			'shaded' => 0,
			'showLines' => 0,
			'xPos' => '200',
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'col1' => array('justification' => 'left', 'width' => 120),
				'col2' => array('justification' => 'left', 'width' => 150))));
		$pdf->ezSetDy(-10);
		//END HEADERS

		/* EXCESS PAYMENTS DETAILS */
		$pdf->ezText(utf8_decode('DETALLES DE LA LIQUIDACIÓN'), 12, array('justification' => 'center')); //title of each page
		$pdf->ezSetDy(-10);
		$pdf->ezTable($details, array('col1' => '<b>No.</b>',
			'col8' => '<b>Ref. Pago</b>',
			'col2' => '<b>No Pago</b>',
			'col3' => '<b>Fecha del Pago</b>',
			'col4' => '<b>Monto a pagar</b>',
			'col5' => '<b>Monto Pagado</b>',
			'col6' => '<b>Monto en Exceso</b>',
			'col7' => '<b>Factura No</b>'), '', array('showHeadings' => 1,
			'shaded' => 0,
			'showLines' => 2,
			'xPos' => 'center',
			'innerLineThickness' => 0.8,
			'outerLineThickness' => 0.8,
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'col1' => array('justification' => 'center', 'width' => 30),
				'col8' => array('justification' => 'center', 'width' => 60),
				'col2' => array('justification' => 'center', 'width' => 60),
				'col3' => array('justification' => 'center', 'width' => 100),
				'col4' => array('justification' => 'center', 'width' => 70),
				'col5' => array('justification' => 'center', 'width' => 70),
				'col6' => array('justification' => 'center', 'width' => 80),
				'col7' => array('justification' => 'center', 'width' => 80))));

		$total = array();
		$totalLine1 = array('col1' => '<b>Total:</b>',
			'col2' => '$ ' . $total_to_pay
		);
		array_push($total, $totalLine1);
		$pdf->ezTable($total, array('col1' => '',
			'col2' => ''), '', array('showHeadings' => 0,
			'shaded' => 0,
			'showLines' => 2,
			'xPos' => '507',
			'innerLineThickness' => 0.8,
			'outerLineThickness' => 0.8,
			'fontSize' => 8,
			'titleFontSize' => 8,
			'cols' => array(
				'col1' => array('justification' => 'center', 'width' => 80),
				'col2' => array('justification' => 'center', 'width' => 80))));
		/* END DETAILS */

		//$pdf->ezStopPageNumbers();
		/* UPDATE THE PAYMENTS WITHOUT THE EXCESS AMOUNT */
		$error = 1;
		foreach ($item_list as $pl) {
			$payment->setSerial_pay($pl);
			$payment->getData();
			$excessAmount = $payment->getExcess_amount_available_pay();
			$payment->setExcess_amount_available_pay('0');

			if ($payment->update()) {
				$excessPayment = new ExcessPaymentsLiquidation($db);
				$excessPayment->setSerial_pay($pl);
				$excessPayment->setSerial_usr($_SESSION['serial_usr']);
				$excessPayment->setAmount_paid_xpl($excessAmount);
				if (!$excessPayment->insert()) {
					$error = 2;
					break;
				}
			} else {
				$error = 5;
				break;
			}
		}
		/* END UPDATE */

		if ($error == 1) {
			$pdf->ezStream();
		} else {
			http_redirect('modules/payments/fDispatchExcessPayments/' . $error);
		}
	} else {
		http_redirect('modules/payments/fDispatchExcessPayments/3');
	}
} else {
	http_redirect('modules/payments/fDispatchExcessPayments/4');
}
?>