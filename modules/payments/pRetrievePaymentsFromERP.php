<?php
/**
 * File: pRetrievePaymentsFromERP
 * Author: Patricio Astudillo
 * Creation Date: 02-may-2013
 * Last Modified: 02-may-2013
 * Modified By: Patricio Astudillo
 */

	ERPConnectionFunctions::fillPASWithERPPayments($db);
	
	http_redirect('modules/payments/fRetrievePaymentsFromERP/1');
?>
