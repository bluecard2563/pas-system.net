<?php
/*
File: pNewPayment
Author: David Bergmann
Creation Date: 05/08/2010
Last Modified:
Modified By:
*/

Request::setinteger('serial_pay');//the payment serial just if is a partially payed invoice
$total_to_pay = $_SESSION['current_sale_total']; //the total of the invoice
$serial_invs = explode("_", $_SESSION['current_sale_products']); //the list of the selected invoices

if(!USE_CONVERGE){
    include(DOCUMENT_ROOT.'lib/merchantAccountConnection.inc.php');
}else{
    Request::setString('0:payment_completed');
    Request::setString('1:serial_pay');
}

if($payment_completed == 'SUCCESS') {
	$document_info = Invoice::getBasicInvoiceInformation($db, $serial_invs[0]);

	//UPDATE THE NUMBER
	$number_payment_note=DocumentByManager::retrieveNextDocumentNumber($db, $document_info['serial_man'], 'BOTH', 'PAYMENT_NOTE', TRUE);
	if(!$number_payment_note){
		http_redirect('modules/payments/PtpPayments/fChooseInvoice/no-document');
	}
	
    //Payment
    if($serial_pay) {
        $payment = new Payment($db, $serial_pay);
        $payment->getData();
        $payment->setTotal_payed_pay($payment->getTotal_to_pay_pay()+$total_to_pay);
        $payment->setTotal_to_pay_pay($total_to_pay);
        $payment->setStatus_pay("PAID");
        $payment->setExcess_amount_available_pay(0);
        if(!$payment->update()){
            $serial_pay = NULL;
        }
    } else {
        $payment = new Payment($db);
        $payment->setTotal_to_pay_pay($total_to_pay);
        $payment->setTotal_payed_pay($total_to_pay);
        $payment->setStatus_pay("PAID");
        $payment->setExcess_amount_available_pay(0);
        $serial_pay = $payment->insert();
    }

    //Payment Detail
    if($serial_pay){
		Payment::registerEfectivePaymentDate($db, $serial_pay);
		
        $paymentDetail = new PaymentDetail($db);
        $paymentDetail->setSerial_pay($serial_pay);
        $paymentDetail->setType_pyf("ELAVON");
        $paymentDetail->setAmount_pyf($total_to_pay);
        $paymentDetail->setDate_pyf(date('d/m/Y'));
        $paymentDetail->setDocumentNumber_pyf($_SESSION['ccnum']);
        $paymentDetail->setComments_pyf($credit_ccard_type);
        $paymentDetail->setSerial_usr($_SESSION['serial_usr']);
		$paymentDetail->setNumber_pyf($number_payment_note);
        if($paymentDetail->insert()){
            $error = 1;
        }else{
            $error = 4;//ERROR TO INSERT PAYMENT DETAIL
        }

        //Invoice
        if($error == 1){
            foreach($serial_invs as $serial_inv){
                $invoice = new Invoice($db,$serial_inv);
                $invoice->getData();

                if(!$invoice->getSerial_pay()){//if the invoice doesn't have a payment assigned then assign the current payment
                    $invoice->setSerial_pay($serial_pay);//update the invoice serial_pay
                }
                
                $invoice->setStatus_inv('PAID');//update the invoice status
                //CHANGE ALL REGISTERED SALES TO 'ACTIVE' STATUS
                $sale = new Sales($db);
                $saleList=$sale->getSalesByInvoice($serial_inv);

                if(sizeof($saleList)>0){
                    foreach($saleList as $s){
                        if($s['status_sal']=='REGISTERED'){
                            $sale->setSerial_sal($s['serial_sal']);
                            if(!$sale->changeStatus('ACTIVE')){ //THE SALES WEREN'T
                                $error=6;
                                break 2;
                            }
                        }
                    }
                }

                $misc['db']=$db;
                $misc['type_com']='IN';
                $misc['serial_fre']=NULL;
                $misc['percentage_fre']=NULL;
                GlobalFunctions::calculateCommissions($misc,$serial_inv);
                GlobalFunctions::calculateBonus($misc,$serial_inv);


                if(!$invoice->update()){//update the serial_pay and  the status_inv
                    $error = 5;//ERROR TO ASSIGN THE PAYMENT TO THE INVOICE
                }
            }
        }
    }else{
        $error = 3;//ERROR TO INSERT THE PAYMENT
    }
    unset($_SESSION['ccnum']);
	http_redirect('modules/payments/PtpPayments/fChooseInvoice/'.$error.'/0/'.$serial_pay);
} else {
    http_redirect('modules/payments/PtpPayments/fChooseInvoice/3/'.$transaction_response);
}
?>