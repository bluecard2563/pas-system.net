<?php
/*

*/
Request::setString('selectedBoxes');

$selectedBoxes = str_replace('&',',',str_replace('chk=', '', $selectedBoxes));

$chkClaims=explode(",",$selectedBoxes);

if(is_array($chkClaims)) {
    $_SESSION['current_sale_products'] = implode("_", $chkClaims);
    $totalInvoice = 0;
    $arraySerialInv = array();
    foreach ($chkClaims as $i) {
        $invoice = new Invoice($db, $i);
//        echo $i."dasda";
        $invoice->getData();
        if($invoice->getSerial_pay()) {
            $serial_pay = $invoice->getSerial_pay();
            $payment = new Payment($db, $serial_pay);
            $payment->getData();
            $totalInvoice += ($payment->getTotal_to_pay_pay() - $payment->getTotal_payed_pay());
        } else {
            $totalInvoice += $invoice->getTotal_inv();
        }

        array_push($arraySerialInv,$i);

    }
    $arraySerialInv=urlencode(serialize($arraySerialInv));
    //Debug::print_r($arraySerialInv);die();


    $totalInvoice = number_format($totalInvoice, 2,".","");

    $_SESSION['current_sale_total'] = $totalInvoice;
    $countryList = Country::getAllCountries($db,false);
    $yearlist = array();
    $year=date('Y');
    for($i=0; $i<11; $i++) {
        array_push($yearlist, $year+$i);
    }
} else {
    http_redirect('modules/payments/PtpPayments/fChooseInvoice/2');
}

$smarty -> register('countryList,global_weekMonths,yearlist,totalInvoice,serial_pay,arraySerialInv');
$smarty->display();
?>