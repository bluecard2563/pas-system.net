<?php
/*
File:fChooseInvoice.php
Author: Edwin Salvador
Creation Date:10/03/2010
Modified By:
Last Modified: 
*/
Request::setString('0:error');
Request::setString('1:fileNumber');
//get the list of countries for the user logged in
$country=new Country($db);
$countryList=$country->getPaymentCountries($_SESSION['countryList'], $_SESSION['serial_mbc']);
//Language to be used on data retrieve
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//DEALER CATEGORIES
$dealer=new Dealer($db);
$categoryList=$dealer->getCategories();

$smarty -> register('countryList,error,categoryList,fileNumber');
$smarty->display();
?>
