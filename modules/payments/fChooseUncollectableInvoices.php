<?php
/*
 * File: fChooseUncollectableInvoices.php
 * Author: Patricio Astudillo
 * Creation Date: 22/06/2010, 10:31:19 AM
 * Modifies By: Patricio Astudillo
 * Last Modified: 22/06/2010, 10:31:19 AM
 */

Request::setString('0:error');

$param=new Parameter($db, '26'); //Uncollectable parameter
$param->getData();
$param=$param->getValue_par();

$managerList=$mbc=ManagerbyCountry::getManagersWithUncollectableInvoices($db, $param);
unset($_REQUEST);

//***** GET CREDIT NOTES NUMBERS FOR PRINTING
if(isset($_SESSION['cnotes_numbers'])):
	$cnote_number = $_SESSION['cnotes_numbers'];
	unset($_SESSION['cnotes_numbers']);
endif;

$smarty->register('managerList,param,error,cnote_number');
$smarty->display();
?>