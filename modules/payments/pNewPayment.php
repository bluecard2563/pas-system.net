<?php
/*
File: pNewPayment.php
Author: Edwin Salvador
Creation Date:12/03/2010
Modified By:
Last Modified: 
*/

$paymentsList = $_POST['payments'];
Request::setFloat('total_to_pay');//the total of the invoice
Request::setString('status_pay');//the status of the payment 'PAID', 'PARTIAL','EXCESS',etc
Request::setString('serial_invs');//the list of the selected invoices
Request::setinteger('serial_pay');//the payment serial just if is a partially payed invoice
Request::setinteger('serial_dea');//the dealer serial
Request::setFloat('total_payed');//the total of the partial payments

$last_payments=0;
$dealer=new Dealer($db, $serial_dea);
$dealer->getData();

$mbc=new ManagerbyCountry($db, $dealer->getSerial_mbc());
$mbc->getData();

//UPDATE THE NUMBER
$number_payment_note=DocumentByManager::retrieveNextDocumentNumber($db, $mbc->getSerial_man(), 'BOTH', 'PAYMENT_NOTE', TRUE);
if(!$number_payment_note){
	http_redirect('modules/payments/fChooseInvoice/no-document');
}

if($paymentsList){
    foreach($paymentsList as $pl){
        $last_payments += $pl['paymentVal'];//get the sum of the current payment and the partial payments
    }
    
    $total_payed+=$last_payments;
    $payment = new Payment($db,$serial_pay);
	$sale = new Sales($db);
	$payment->getData();
    $payment->setTotal_to_pay_pay($total_to_pay);
    $payment->setTotal_payed_pay($total_payed);
    if($last_payments>$total_to_pay){
        $payment->setExcess_amount_available_pay($last_payments-$total_to_pay);
    }else{
        $payment->setExcess_amount_available_pay(0);
    }
    $payment->setStatus_pay($status_pay);
	if(!$serial_pay){
		$serial_pay = $payment->insert();
	}else{
		if(!$payment->update()){
			$serial_pay = NULL;
		}
	}
    
    if($serial_pay){
        if(is_array($paymentsList)){
            foreach($paymentsList as $pl){
                $paymentDetail = new PaymentDetail($db);
                $paymentDetail->setSerial_pay($serial_pay);
                $paymentDetail->setType_pyf($pl['paymentType']);
                $paymentDetail->setAmount_pyf($pl['paymentVal']);
                $paymentDetail->setDate_pyf(date('d/m/Y'));
                $paymentDetail->setDocumentNumber_pyf($pl['paymentDocument']);
				$paymentDetail->setNumber_pyf($number_payment_note);
				
                if($pl['paymentDesc']){
                    $paymentDetail->setComments_pyf($pl['paymentDesc']);
                }elseif($pl['paymentCard']){
                    if($pl['paymentCard']!= 'OTHER'){
                        $paymentDetail->setComments_pyf($pl['paymentCard']);
                    }else{
                        $paymentDetail->setComments_pyf($pl['paymentOther']);
                    }
                }elseif($pl['paymentBank']){
                    if($pl['paymentBank']!= 'OTHER'){
                        $paymentDetail->setComments_pyf($pl['paymentBank']);
                    }else{
                        $paymentDetail->setComments_pyf($pl['paymentOther']);
                    }
                }elseif($pl['paymentExcess']){
                        $paymentDetail->setComments_pyf('Pago en exceso');
                }elseif($pl['creditNotes']){
                        $paymentDetail->setComments_pyf(utf8_decode('Nota de crédito'));
                        $paymentDetail->setSerial_cn($pl['creditNotes']);
                }
				$paymentDetail->setSerial_usr($_SESSION['serial_usr']);
                if($paymentDetail->insert()){
                    $error = 1;
                }else{
                    $error = 3;//ERROR TO INSERT PAYMENT DETAIL
                }
                if($pl['paymentType']=='EXCESS'){
                    $pay=new Payment($db,$pl['paymentExcess']);
                    if($pay->updateExcess_amount_available_pay($pl['paymentVal'])){
                        $error = 1;
                    }else{
                        $error = 3;//ERROR TO INSERT PAYMENT DETAIL
                    }
                }elseif($pl['paymentType']=='CREDIT_NOTE'){
                    $cn=new CreditNote($db,$pl['creditNotes']);
                    $cn->setPayment_status_cn('PAID');
                    if($cn->updatePayment_status_cn()){
                        $error = 1;
                    }else{
                        $error = 3;//ERROR TO INSERT PAYMENT DETAIL
                    }
                }
            }

            /*TODO change the invoice status*/
            if($error == 1){
                $serial_invs = explode(',',$serial_invs);
                foreach($serial_invs as $serial_inv){
                    $invoice = new Invoice($db,$serial_inv);
                    $invoice->getData();
					//Debug::print_r($invoice);
                    if(!$invoice->getSerial_pay()){//if the invoice doesn't have a payment assigned then assign the current payment
                        $invoice->setSerial_pay($serial_pay);//update the invoice serial_pay
                    }
                    if(($status_pay == 'PAID' || $status_pay == 'EXCESS') && $invoice->getStatus_inv()!='VOID'){
                        $invoice->setStatus_inv('PAID');//update the invoice status
						Payment::registerEfectivePaymentDate($db, $serial_pay);
						
						//CHANGE ALL REGISTERED SALES TO 'ACTIVE' STATUS
						$saleList=$sale->getSalesByInvoice($serial_inv);
						
						if(sizeof($saleList)>0){
							foreach($saleList as $s){
								if($s['status_sal']=='REGISTERED'){
									$sale->setSerial_sal($s['serial_sal']);
									if(!$sale->changeStatus('ACTIVE')){ //THE SALES WEREN'T 
										$error=5;
										break 2;
									}
								}
							}
						}						

                    }
					
                    if(!$invoice->update()){//update the serial_pay and  the status_inv
                        $error = 4;//ERROR TO ASSIGN THE PAYMENT TO THE INVOICE
                    }
                    $misc['db']=$db;
                    $misc['type_com']='IN';
                    $misc['serial_fre']=NULL;
                    $misc['percentage_fre']=NULL;
                    GlobalFunctions::calculateCommissions($misc,$serial_inv);
                    GlobalFunctions::calculateBonus($misc,$serial_inv);
                }                
            }
        }else{
            $error = 3;//ERROR TO INSERT PAYMENT DETAIL
        }
    }else{
        $error = 2;//ERROR TO INSERT THE PAYMENT
    }
}
http_redirect('modules/payments/fChooseInvoice/'.$error.'/'.$serial_pay);
?>