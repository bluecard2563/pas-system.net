<?php
/*
File: pVoidPayment.php
Author: Edwin Salvador
Creation Date:19/03/2010
Modified By:
Last Modified: 
*/
//Debug::print_r($_POST);die;
Request::setInteger('serial_pay');
Request::setString('status_pay');
Request::setFloat('total_payed');
Request::setString('observation_pay');

$paymentsStatus = $_POST['payments'];
$error = 1;

if($serial_pay && $status_pay && isset($total_payed) && is_array($paymentsStatus)){
	foreach($paymentsStatus as $serial_pyf=>$pyf){
		$paymentDetail = new PaymentDetail($db, $serial_pyf);
		$paymentDetail->setStatus_pyf($pyf['status_pyf']);
		if($pyf['observation_pyf']){
			$paymentDetail->setObservation_pyf($pyf['observation_pyf']);
		}
		if(!$paymentDetail->update()){
			$error = 3;
		}
	}
	if($error == 1){
		$payment = new Payment($db, $serial_pay);
		$payment->getData();
		$payment->setStatus_pay($status_pay);
		$payment->setTotal_payed_pay($total_payed);

		if($observation_pay){
			if($payment->getObservation_pay() != ''){
				$observation_pay = $payment->getObservation_pay().'<br />'.$observation_pay;
			}
			$payment->setObservation_pay($observation_pay);
		}
		if($payment->update()){
			ERP_logActivity('update', $payment);//ERP ACTIVITY
			
			$invoice = new Invoice($db);
			if($status_pay == 'VOID' || $status_pay == 'PARTIAL'){
				$status_inv = 'STAND-BY';
			}elseif ($status_pay == 'PAID' || $status_pay == 'EXCESS') {
				$status_inv = 'PAID';
			}
			if(!$invoice->updateStatus($serial_pay,$status_inv)){
				$error = 3;
			}
		}else{
			$error = 3;
		}
	}
}else{
	$error = 3;
}
http_redirect('modules/payments/fChoosePaymentToVoid/'.$error);
?>