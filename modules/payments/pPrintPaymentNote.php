<?php
/*
    Document   : pPrintInvoice.php
    Created on : May 6, 2010, 11:00 AM
    Author     : Santiago Benitez
    Description:
        Generates a PDF file for payment note printing.
*/
Request::setInteger('0:serial_pay');

//dealer
$dealer=new Dealer($db);
$dealer->setSerial_dea($dealer->getDealerByPayment($serial_pay));
$dealer->getData();

//manager
$mbc=new ManagerbyCountry($db, $dealer->getSerial_mbc());
$mbc->getData();

//user
$user=new User($db, $_SESSION['serial_usr']);
$user->getData();
//payment
$payment=new Payment($db,$serial_pay);
$payment->getData();
$totalPayed=$payment->getTotal_payed_pay();
$totalToPay=$payment->getTotal_to_pay_pay();
if($totalPayed>=$totalToPay){
   $pendingAmount=0;
}else{
   $pendingAmount=$totalToPay-$totalPayed;
}
$excessAmount = $payment->getTotal_payed_pay() - $payment->getTotal_to_pay_pay();

//Payment Details
//payment's data
$details = array();
$paymentDetail = new PaymentDetail($db);
$paymentDetail->setSerial_pay($serial_pay);
$details = $paymentDetail->getPaymentDetails(TRUE);
foreach($details as &$d){
    $d['type_pyf']=html_entity_decode($global_paymentForms[$d['type_pyf']]);
	$number = $d['number_pyf'];
	$date = $d['date_pyf'];
}

//payment note number
$invoice=new Invoice($db);
$invoices=array();
$invoices=$invoice->getInvoicesByPayment($serial_pay);

//general data
$reportTitle=utf8_decode('<b>Nota de Pago #'.$number.'</b>');
if(sizeof($invoices)==1){
	if($invoices['0']['serial_cus']!=''){
		$cus=new Customer($db);
		$cus->setserial_cus($invoices['0']['serial_cus']);
		$cus->getData();
		$description=array();
		$description[0]=array('col1' => '<b>Cobro a: </b>',
							  'col2' => $cus->getFirstname_cus().' '.$cus->getLastname_cus(),
							  'col3' => '<b>RUC/CI: </b>',
							  'col4' => $cus->getDocument_cus());
		$description[1]=array('col1' => '<b>Fecha: </b>',
							  'col2' => $date,
							  'col3' => utf8_decode('<b>Teléfono: </b>'),
							  'col4' => $cus->getPhone1_cus());
		$description[2]=array('col1' => utf8_decode('<b>Dirección: </b>'),
							  'col2' => $cus->getAddress_cus(),
							  'col3' => '<b>Usuario: </b>',
							  'col4' => ($user->getFirstname_usr()." ".$user->getLastname_usr()));
	}
	else{
		$description=array();
		$description[0]=array('col1'=>'<b>Cobro a: </b>','col2'=>$dealer->getName_dea(),'col3'=>'<b>RUC/CI: </b>','col4'=>$dealer->getId_dea());
		$description[1]=array('col1'=>'<b>Fecha: </b>','col2'=>$date,'col3'=>utf8_decode('<b>Teléfono: </b>'),'col4'=>$dealer->getPhone1_dea());
		$description[2]=array('col1'=>utf8_decode('<b>Dirección: </b>'),'col2'=>$dealer->getAddress_dea(),'col3'=>'<b>Usuario: </b>','col4'=>($user->getFirstname_usr()." ".$user->getLastname_usr()));
	}
	/*$aux=$invoice->getDirectSalesByPayment($serial_pay);
	if($aux!=false){
		$cus=new Customer($db);
		$cus->setserial_cus($aux);
		$cus->getData();
		$description=array();
		$description[0]=array('col1'=>'<b>Cobro a: </b>','col2'=>$cus->getFirstname_cus().' '.$cus->getLastname_cus(),'col3'=>'<b>RUC/CI: </b>','col4'=>$cus->getDocument_cus());
		$description[1]=array('col1'=>'<b>Fecha: </b>','col2'=>$payment->getDate_pay(),'col3'=>utf8_decode('<b>Teléfono: </b>'),'col4'=>$cus->getPhone1_cus());
		$description[2]=array('col1'=>utf8_decode('<b>Dirección: </b>'),'col2'=>$cus->getAddress_cus(),'col3'=>'<b>Usuario: </b>','col4'=>($user->getFirstname_usr()." ".$user->getLastname_usr()));
	}*/
}
else{
		$description=array();
		$description[0]=array('col1'=>'<b>Cobro a: </b>','col2'=>$dealer->getName_dea(),'col3'=>'<b>RUC/CI: </b>','col4'=>$dealer->getId_dea());
		$description[1]=array('col1'=>'<b>Fecha: </b>','col2'=>$payment->getDate_pay(),'col3'=>utf8_decode('<b>Teléfono: </b>'),'col4'=>$dealer->getPhone1_dea());
		$description[2]=array('col1'=>utf8_decode('<b>Dirección: </b>'),'col2'=>$dealer->getAddress_dea(),'col3'=>'<b>Usuario: </b>','col4'=>($user->getFirstname_usr()." ".$user->getLastname_usr()));
	}

foreach($invoices as &$inv){
    if($inv['name_cus']){
        $inv['name']=$inv['name_cus'];
    }else{
        $inv['name']=$inv['name_dea'];
    }
}
$totalInvoices=array();
$totalInvoices[0]['col1']='<b>Valor Total:</b>';
$totalInvoices[0]['col2']=$totalToPay;
$totalInvoices[0]['col2']='<b>'.number_format($totalInvoices[0]['col2'], 2, '.', '').'</b>';

$totalPayments=array();
$totalPayments[0]['col1']='<b>Total pagado:</b>';
$totalPayments[0]['col2']=$totalPayed;
$totalPayments[0]['col2']='<b>'.number_format($totalPayments[0]['col2'], 2, '.', '').'</b>';
$totalPayments[1]['col1']='<b>Monto Pendiente:</b>';
$totalPayments[1]['col2']=$pendingAmount;
$totalPayments[1]['col2']='<b>'.number_format($totalPayments[1]['col2'], 2, '.', '').'</b>';
if($excessAmount > 0 && $excessAmount){
    $totalPayments[2]['col1']='<b>Monto en Exceso:</b>';
    $totalPayments[2]['col2']=$excessAmount;
    $totalPayments[2]['col2']='<b>'.number_format($totalPayments[2]['col2'], 2, '.', '').'</b>';   
}

//HEADER
$pdf =& new Cezpdf('A5','landscape');
$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
$pdf -> ezSetMargins(90,90,60,50);
//END HEADERS
//BEGIN DESCRIPTION
$options = array(
				'showLines'=>0,
				'showHeadings'=>0,
				'shaded'=>0,
				'fontSize' => 8,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 8,
				'rowGap' => 2,
				'colGap' => 5,
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 520,
				'cols' =>array(
						  'col1'=>array('justification'=>'left','width'=>80),
						  'col2'=>array('justification'=>'left','width'=>230),
						  'col3'=>array('justification'=>'left','width'=>80),
						  'col4'=>array('justification'=>'left','width'=>130)
				),
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
);

$pdf->ezSetDy(-10);
$pdf->ezText($reportTitle, 10, array('justification' =>'center'));
$pdf->ezSetDy(-10);
$pdf->ezTable($description, array('col1'=>'Col1','col2'=>'Col2','col3'=>'Col3','col4'=>'Col4'), NULL, $options);
$pdf->ezSetDy(-10);
$pdf->ezText('<b>Detalle de Facturas:</b>', 8, array('justification' =>'left'));
//END DESCRIPTION
//BEGIN INVOICE TABLE
$options = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>2,
				'shadeCol'=>array(1,1,1),
				'shadeCol2'=>array(1,1,1),
				'fontSize' => 8,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 8,
				'rowGap' => 2,
				'colGap' => 5,
				'lineCol'=>array(0,0,0),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 440,
				'cols' =>array(
						  'number_inv'=>array('justification'=>'center','width'=>80),
						  'name'=>array('justification'=>'left','width'=>180),
						  'date_inv'=>array('justification'=>'left','width'=>130),
						  'total_inv'=>array('justification'=>'right','width'=>80)
				),
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
);

$pdf->ezSetDy(-10);
$pdf->ezTable($invoices, array(
								'number_inv'=>'<b>Factura No.</b>',
								'name'=>'<b>Facturado a</b>',
								'date_inv'=>utf8_decode('<b>F. Emisión</b>'),
								'total_inv'=>'<b>Valor Total</b>'),
						NULL, $options);
//END INVOICE TABLE
//BEGIN INVOICE TOTALS
$options = array(
				'showLines'=>2,
				'showHeadings'=>0,
				'shaded'=>0,
				'fontSize' => 8,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 8,
				'rowGap' => 2,
				'colGap' => 5,
				'lineCol'=>array(0,0,0),
				'xPos'=>432.6,
				'xOrientation'=>'center',
				'maxWidth' => 180,
				'cols' =>array(
								  'col1'=>array('justification'=>'right','width'=>130),
								  'col2'=>array('justification'=>'right','width'=>79.8)
				),
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
);
$pdf->ezTable($totalInvoices, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);
$pdf->ezSetDy(-10);
//END INVOICE TOTALS

$pdf->ezSetDy(-10);
$pdf->ezText('<b>Detalle de Pagos:</b>', 8, array('justification' =>'left'));
//BEGIN PAYMENT TABLE
$options = array(
				'showLines'=>2,
				'showHeadings'=>1,
				'shaded'=>2,
				'shadeCol'=>array(1,1,1),
				'shadeCol2'=>array(1,1,1),
				'fontSize' => 8,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 8,
				'rowGap' => 2,
				'colGap' => 5,
				'lineCol'=>array(0,0,0),
				'xPos'=>'center',
				'xOrientation'=>'center',
				'maxWidth' => 440,
				'cols' =>array(
						  'date_pyf'=>array('justification'=>'center','width'=>120),
						  'type_pyf'=>array('justification'=>'left','width'=>100),
						  'comments_pyf'=>array('justification'=>'left','width'=>120),
						  'document_number_pyf'=>array('justification'=>'center','width'=>80),
						  'amount_pyf'=>array('justification'=>'right','width'=>50)
				),
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
);

$pdf->ezSetDy(-10);
$pdf->ezTable($details, array(
								'date_pyf'=>'<b>Fecha</b>',
								'type_pyf'=>'<b>Tipo</b>',
								'comments_pyf'=>utf8_decode('<b>Descripción</b>'),
								'document_number_pyf'=>'<b>Documento No.</b>',
								'amount_pyf'=>'<b>Monto</b>'),
			NULL, $options);
//END PAYMENT TABLE
//BEGIN PAYMENT TOTALS
$options = array(
				'showLines'=>2,
				'showHeadings'=>0,
				'shaded'=>0,
				'fontSize' => 8,
				'textCol' =>array(0,0,0),
				'titleFontSize' => 8,
				'rowGap' => 2,
				'colGap' => 5,
				'lineCol'=>array(0,0,0),
				'xPos'=>472.6,
				'xOrientation'=>'center',
				'maxWidth' => 180,
				'cols' =>array(
								  'col1'=>array('justification'=>'right','width'=>80),
								  'col2'=>array('justification'=>'right','width'=>50)
				),
				'innerLineThickness' => 0.8,
				'outerLineThickness' => 0.8
);

$pdf->ezTable($totalPayments, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

$pdf->ezStream();
?>