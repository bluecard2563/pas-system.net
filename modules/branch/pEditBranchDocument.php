<?php

/**
 * File: pEditBranchDocument
 * Author: Patricio Astudillo
 * Creation Date: 07/05/2012
 * Last Modified: 07/05/2012
 * Modified By: Patricio Astudillo
 * 
 */

	Request::setInteger('hdnBranchID');
	Request::setString('txtNewDocument');
	
	$modifier_user_info = User::getBasicInformationForUser($db, $_SESSION['serial_usr']);	
	$dealer = new Dealer($db, $hdnBranchID);
	$dealer->getData();
	
	$misc['titles'] = array();
	
	if (strcasecmp($dealer->getId_dea(), $_POST['txtNewDocument'])) {
        $dealer->setId_dea($_POST['txtNewDocument']);
        $misc['data']['id'] = $_POST['txtNewDocument'];
        array_push($misc['titles'], "RUC/CI");
    }
	
	if ($dealer->getName_dea() != $_POST['txtName']) {
        $dealer->setName_dea(specialchars($_POST['txtName']));
        $misc['data']['name'] = specialchars($_POST['txtName']);
        array_push($misc['titles'], "Nombre");
    }
	
	if ($dealer->getBank_dea() != $_POST['selBank']) {
        $dealer->setBank_dea($_POST['selBank']);
        $misc['data']['bank'] = $_POST['selBank'];
        array_push($misc['titles'], "Banco");
    }

    if ($dealer->getAccount_number_dea() != $_POST['txtAccountNumber']) {
        $dealer->setAccount_number_dea($_POST['txtAccountNumber']);
        $misc['data']['account_number'] = $_POST['txtAccountNumber'];
        array_push($misc['titles'], "N&uacute;mero de Cuenta");
    }

    if ($dealer->getAccount_type_dea() != $_POST['selAccountType']) {
        $dealer->setAccount_type_dea($_POST['selAccountType']);
        $misc['data']['account_type'] = $global_account_types[$_POST['selAccountType']];
        array_push($misc['titles'], "Tipo de Cuenta");
    }
	
	/* EMAIL NOTIFICATION SETTINGS */
	$code = Dealer::getDealerCode($db, $dealer -> getSerial_dea());
	$parentDealer = new Dealer($db, $dealer -> getDea_serial_dea());
	$parentDealer -> getData();
	
	$managerByCountry = new ManagerbyCountry($db, $parentDealer -> getSerial_mbc());
	$managerByCountry -> getData();
	$manager = new Manager($db, $managerByCountry -> getSerial_man());
	$manager -> getData();
	
	$parameter = new Parameter($db);
	$parameter -> setSerial_par('41');	//Accountant Information
	$parameter -> getData();
	/* EMAIL NOTIFICATION SETTINGS */
	
	if($dealer->update()){
		if(sizeof($misc['titles']) > 0){
			$misc['textForEmail'] = "Se ha actualizado la siguiente informaci&oacute;n de la sucursal <b>" . $dealer -> getName_dea() . 
									"</b> cod : <u>{$dealer->getCode_dea()}</u>, del comercializador <b>" . $parentDealer -> getName_dea() . "</b> cod : <u>" . 
									$parentDealer -> getSerial_dea() . "</u><br><br>
									La persona que actualiz&oacute; el registro fue: ".$modifier_user_info['first_name_usr'].' '.$modifier_user_info['last_name_usr'].'<br><br>'; 

			$misc['textInfo'] = "Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";

			$misc['comissionistMail'] = $parameter ->getValue_par();
			$misc['managerMail'] = $manager -> getContact_email_man();
			
			if(GlobalFunctions::sendMail($misc, 'updateDealer')){
				http_redirect('modules/branch/fEditBranchDocument/1');
			} else {
				ErrorLog::log($db, 'UPDATE BRANCH DOCUMENT: FAILED TO SEND MAIL', $misc);
				http_redirect('modules/branch/fEditBranchDocument/2');
			}
		}else{
			http_redirect('modules/branch/fEditBranchDocument/1');
		}
	}else{
		ErrorLog::log($db, 'UPDATE BRANCH DOCUMENT: UPDATE FAILED', $misc);
		http_redirect('modules/branch/fEditBranchDocument/2');
	}
?>
