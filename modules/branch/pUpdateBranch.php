<?php

/*
 * File: pUpdateBranch.php
 * Author: David Bergmann
 * Creation Date: 15/01/2010
 * Modified By: Santiago Borja
 * Last Modified: 21/02/2011
 */

Request::setString("limited_update");
$dealer = new Dealer($db, $_POST['hdnSerialDealer']);

if($limited_update == 'YES'){
    $redirection_url = "modules/branch/fLimitedSearchBranch/";
}else{
    $redirection_url = "modules/branch/fSearchBranch/";
}

if (!Dealer::isBranch($db, $dealer->getSerial_dea())) {
    ErrorLog::log($db, 'UPDATE BRANCH: DEALER CODE FOUND', $dealer);
    http_redirect($redirection_url.'4');
}
if ($dealer->getData()) {
    /* NOTIFICATION MAIL INFO */
    $code = Dealer::getDealerCode($db, $dealer->getSerial_dea());
    $parentDealer = new Dealer($db, $dealer->getDea_serial_dea());
    $parentDealer->getData();

    $misc['textForEmail'] = "Se ha actualizado la siguiente informaci&oacute;n de la sucursal <b>" . $dealer->getName_dea() .
            "</b> cod : <u>$code</u>, del comercializador <b>" . $parentDealer->getName_dea() . "</b> cod : <u>" .
            $parentDealer->getSerial_dea() . "</u>:<br><br>";
    $misc['textInfo'] = "Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
    $misc['titles'] = array();

    $dealer->setSerial_mbc($parentDealer->getSerial_mbc());

    if ($dealer->getSerial_sec() != $_POST['selSector']) {
        $dealer->setSerial_sec($_POST['selSector']);
        $sector = new Sector($db, $_POST['selSector']);
        $sector->getData();
        $misc['data']['sector'] = $sector->getName_sec();
        array_push($misc['titles'], "Sector");
    }

    if ($dealer->getSerial_dlt() != $_POST['selDealerT']) {
        $dealer->setSerial_dlt($_POST['selDealerT']);
        $dealerType = new DealerType($db, $_POST['selDealerT']);
        $dealerType->getData();
        $dealerType->getDealerTypeByLanguage($_SESSION['serial_lang']);
        $misc['data']['dealerType'] = $dealerType->getDescription_dtl();
        array_push($misc['titles'], "Tipo");
    }

    if ($dealer->getCategory_dea() != $_POST['selCategory']) {
        $dealer->setCategory_dea($_POST['selCategory']);
        $misc['data']['category'] = $_POST['selCategory'];
        array_push($misc['titles'], "Categor&iacute;a");
    }

    if ($dealer->getName_dea() != $_POST['txtName']) {
        $dealer->setName_dea(specialchars($_POST['txtName']));
        $misc['data']['name'] = specialchars($_POST['txtName']);
        array_push($misc['titles'], "Nombre");
    }

    if ($dealer->getStatus_dea() != $_POST['selStatus']) {
        $dealer->setStatus_dea($_POST['selStatus']);
        $misc['data']['status'] = $global_status[$_POST['selStatus']];
        array_push($misc['titles'], "Estado");
		
		$status_value = $_POST['selStatus'];
    }

    if ($dealer->getAddress_dea() != $_POST['txtAddress']) {
        $dealer->setAddress_dea($_POST['txtAddress']);
        $misc['data']['address'] = $_POST['txtAddress'];
        array_push($misc['titles'], "Direcci&oacute;n");
    }

    if ($dealer->getPhone1_dea() != $_POST['txtPhone1']) {
        $dealer->setPhone1_dea($_POST['txtPhone1']);
        $misc['data']['phone1'] = $_POST['txtPhone1'];
        array_push($misc['titles'], "Tel&eacute;fono #1");
    }

    if ($dealer->getPhone2_dea() != $_POST['txtPhone2']) {
        $dealer->setPhone2_dea($_POST['txtPhone2']);
        $misc['data']['phone2'] = $_POST['txtPhone2'];
        array_push($misc['titles'], "Tel&eacute;fono #2");
    }

    if ($dealer->getContract_date_dea() != $_POST['txtContractDate']) {
        $dealer->setContract_date_dea($_POST['txtContractDate']);
        $misc['data']['contractDate'] = $_POST['txtContractDate'];
        array_push($misc['titles'], "Fecha de contrato");
    }

    if ($dealer->getFax_dea() != $_POST['txtFax']) {
        $dealer->setFax_dea($_POST['txtFax']);
        $misc['data']['fax'] = $_POST['txtFax'];
        array_push($misc['titles'], "Fax");
    }

    if ($dealer->getEmail1_dea() != $_POST['txtMail1']) {
        $dealer->setEmail1_dea($_POST['txtMail1']);
        $misc['data']['email1'] = $_POST['txtMail1'];
        array_push($misc['titles'], "E-mail #1");
    }

    if ($dealer->getEmail2_dea() != $_POST['txtMail2']) {
        $dealer->setEmail2_dea($_POST['txtMail2']);
        $misc['data']['email2'] = $_POST['txtMail2'];
        array_push($misc['titles'], "E-mail #2");
    }

    if ($dealer->getContact_dea() != $_POST['txtContact']) {
        $dealer->setContact_dea($_POST['txtContact']);
        $misc['data']['contact'] = $_POST['txtContact'];
        array_push($misc['titles'], "Nombre del Contacto");
    }

    if ($dealer->getPhone_contact_dea() != $_POST['txtContactPhone']) {
        $dealer->setPhone_contact_dea($_POST['txtContactPhone']);
        $misc['data']['phoneContact'] = $_POST['txtContactPhone'];
        array_push($misc['titles'], "Tel&eacute;fono del Contacto");
    }

    if ($dealer->getEmail_contact_dea() != $_POST['txtContactMail']) {
        $dealer->setEmail_contact_dea($_POST['txtContactMail']);
        $misc['data']['emailContact'] = $_POST['txtContactMail'];
        array_push($misc['titles'], "E-mail del Contacto");
    }

    if ($dealer->getVisits_number_dea() != $_POST['txtVisitAmount']) {
        $dealer->setVisits_number_dea($_POST['txtVisitAmount']);
        $misc['data']['visitsNumber'] = $_POST['txtVisitAmount'];
        array_push($misc['titles'], "N&uacute;mero de Visitas al Mes");
    }

    if ($dealer->getPay_contact_dea() != $_POST['txtContactPay']) {
        $dealer->setPay_contact_dea($_POST['txtContactPay']);
        $misc['data']['payContact'] = $_POST['txtContactPay'];
        array_push($misc['titles'], "Contacto Pago");
    }

    if ($dealer->getBill_to_dea() != $_POST['chkBillTo']) {
        $dealer->setBill_to_dea($_POST['chkBillTo']);
        $misc['data']['bill_to_dea'] = $global_billTo[$_POST['chkBillTo']];
        array_push($misc['titles'], "Factura a");
    }

    if ($dealer->getPayment_deadline_dea() != $_POST['selPayment']) {
        $dealer->setPayment_deadline_dea($_POST['selPayment']);
        $misc['data']['payDeadline'] = $global_weekDays[$_POST['selPayment']];
        array_push($misc['titles'], "D&iacute;a de Pago");
    }

    if ($dealer->getSerial_cdd() != $_POST['selCreditD']) {
        $dealer->setSerial_cdd($_POST['selCreditD']);
        $creditDay = new CreditDay($db, $_POST['selCreditD']);
        $creditDay->getData();
        $misc['data']['creditDay'] = $creditDay->getDays_cdd();
        array_push($misc['titles'], "D&iacute;as de cr&eacute;dito");
    }

    if ($dealer->getPercentage_dea() != $_POST['txtPercentage']) {
        $dealer->setPercentage_dea($_POST['txtPercentage']);
        $misc['data']['percentage'] = $_POST['txtPercentage'];
        array_push($misc['titles'], "Porcentaje");
    }

    if ($dealer->getType_percentage_dea() != $_POST['selType']) {
        $dealer->setType_percentage_dea($_POST['selType']);
        $misc['data']['percentageType'] = $global_pType[$_POST['selType']];
        array_push($misc['titles'], "Tipo de porcentaje");
    }

    if ($dealer->getAssistance_contact_dea() != $_POST['txtAssistName']) {
        $dealer->setAssistance_contact_dea($_POST['txtAssistName']);
        $misc['data']['assistanceContact'] = $_POST['txtAssistName'];
        array_push($misc['titles'], "Nombre de asistencias");
    }

    if ($dealer->getEmail_assistance_dea() != $_POST['txtAssistEmail']) {
        $dealer->setEmail_assistance_dea($_POST['txtAssistEmail']);
        $misc['data']['emailAssistance'] = $_POST['txtAssistEmail'];
        array_push($misc['titles'], "E-mail de asistencias");
    }

    if ($dealer->getBonus_to_dea() != $_POST['chkBonusTo']) {
        $dealer->setBonus_to_dea($_POST['chkBonusTo']);
        $misc['data']['bonus_to_dea'] = $global_bonusTo[$_POST['chkBonusTo']];
        array_push($misc['titles'], "Incentivo a");
    }

    if ($dealer->getManager_name_dea() != $_POST['txtNameManager']) {
        $dealer->setManager_name_dea($_POST['txtNameManager']);
        $misc['data']['manager_name'] = $_POST['txtNameManager'];
        array_push($misc['titles'], "Nombre del Gerente");
    }

    if ($dealer->getManager_phone_dea() != $_POST['txtPhoneManager']) {
        $dealer->setManager_phone_dea($_POST['txtPhoneManager']);
        $misc['data']['manager_phone'] = $_POST['txtPhoneManager'];
        array_push($misc['titles'], "Tel&eacute;fono del Gerente");
    }

    if ($dealer->getManager_email_dea() != $_POST['txtMailManager']) {
        $dealer->setManager_email_dea($_POST['txtMailManager']);
        $misc['data']['manager_email'] = $_POST['txtMailManager'];
        array_push($misc['titles'], "E-mail del Gerente");
    }

    if ($dealer->getManager_birthday_dea() != $_POST['txtBirthdayManager']) {
        $dealer->setManager_birthday_dea($_POST['txtBirthdayManager']);
        $misc['data']['manager_birthday'] = $_POST['txtBirthdayManager'];
        array_push($misc['titles'], "Fecha de Nacimiento del Gerente");
    }

    if ($dealer->getBank_dea() != $_POST['selBank']) {
        $dealer->setBank_dea($_POST['selBank']);
        $misc['data']['bank'] = $_POST['selBank'];
        array_push($misc['titles'], "Banco");
    }

    if ($dealer->getAccount_number_dea() != $_POST['txtAccountNumber']) {
        $dealer->setAccount_number_dea($_POST['txtAccountNumber']);
        $misc['data']['account_number'] = $_POST['txtAccountNumber'];
        array_push($misc['titles'], "N&uacute;mero de Cuenta");
    }

    if ($dealer->getAccount_type_dea() != $_POST['selAccountType']) {
        $dealer->setAccount_type_dea($_POST['selAccountType']);
        $misc['data']['account_type'] = $global_account_types[$_POST['selAccountType']];
        array_push($misc['titles'], "Tipo de Cuenta");
    }
	
	if($dealer ->getMinimum_kits_dea() != $_POST['txtMinimumKits']){
		$dealer -> setMinimum_kits_dea($_POST['txtMinimumKits']);
		$misc['data']['txtMinimumKits'] = $_POST['txtMinimumKits'];
		array_push($misc['titles'], "Stock M&iacute;nimo");
	}
	
	if($dealer ->getNotificacions_dea() != $_POST['rdNotifications']){
		$dealer ->setNotificacions_dea($_POST['rdNotifications']);
		$misc['data']['rdNotifications'] = $_POST['rdNotifications'];
		array_push($misc['titles'], "Recibe Notificaciones?");
	}

if (($dealer->getLogo_dea() != $_FILES['txtLogo_dea']['name'])&&( $_FILES['txtLogo_dea']['name'])!= NULL)
	{		

		//UPLOADING LOGO FILE
			$allowedArtExtensions = array("image/jpeg","image/pjpeg");
			$artFileUploaded = false;

			if(isset($_FILES['txtLogo_dea']['name']))
			{
				if(in_array($_FILES["txtLogo_dea"]["type"], $allowedArtExtensions))
				{
					if($_FILES["txtLogo_dea"]["size"]<2097152)
					{
					
                        //CHANGE LOGO NAME
						$artFileName = $code.'.'.$_FILES["txtLogo_dea"]['name'];
//						$artFileName = $code.'.'.date("Y/m/d");
//						$artFileName = $code;
                        
                        //CHECK IF FILE EXIST
                        //if (file_exists(DOCUMENT_ROOT.'img/dea_logos/'.$artFileName)) {
                          //  unlink(DOCUMENT_ROOT.'img/dea_logos/'.$artFileName);
                        //} else {
//                            nada
                        //}
						if(copy($_FILES['txtLogo_dea']['tmp_name'], DOCUMENT_ROOT.'img/dea_logos/'.$artFileName))
						{
							$artFileUploaded = true;
						}
					}else {
						http_redirect('modules/dealer/fSearchDealer/9');
					}
				}else{
					http_redirect('modules/dealer/fSearchDealer/8');
				}
			}
			$dealer->setLogo_dea($artFileName);
			

		}

    /* END NOTIFICATION MAIL INFO */
    if ($_POST['hdnSerialUsr']) {
        $user_by_dealer = new UserByDealer($db);
        $user_by_dealer->setSerial_dea($dealer->getSerial_dea());
        $user_by_dealer->getData();
        $user = new User($db, $user_by_dealer->getSerial_usr());
        $user->getData();
        $misc['comissionistMail'] = $user->getEmail_usr();

        $managerByCountry = new ManagerbyCountry($db, $parentDealer->getSerial_mbc());
        $managerByCountry->getData();
        $manager = new Manager($db, $managerByCountry->getSerial_man());
        $manager->getData();
        $misc['managerMail'] = $manager->getContact_email_man();
        $user = new User($db, $_POST['hdnSerialUsr']);
        $user->getData();

        //If the user changes:
        if ($user_by_dealer->getSerial_usr() != $_POST['hdnSerialUsr']) {
            $user_by_dealer->deactivateUser();
            $user_by_dealer->setSerial_usr($_POST['hdnSerialUsr']);
            $user_by_dealer->setStatus_ubd("ACTIVE");
            $user_by_dealer->setPercentage_ubd($user->getCommission_percentage_usr());
            if (!$user_by_dealer->insert()) {
                ErrorLog::log($db, 'UPDATE BRANCH: FAILED TO INSERT UBD ', $user_by_dealer); //SAFETY LOG
                http_redirect($redirection_url.'2');
            }
        }
    }

    if ($dealer->update()) {
		//DEACTIVE USER'S IF APPLIES
		if($status_value){
			User::changeUserStatus($db, $status_value, 'DEALER', $dealer->getSerial_dea());
		}

        if (GlobalFunctions::sendMail($misc, 'updateDealer')) {
            http_redirect($redirection_url.'1');
        } else {
            ErrorLog::log($db, 'UPDATE BRANCH: FAILED TO SEND MAIL', $misc);
            http_redirect($redirection_url.'2');
        }
    } else {
        ErrorLog::log($db, 'UPDATE BRANCH: FAILED TO UPDATE', $dealer);
        http_redirect($redirection_url.'4');
    }
} else {
    ErrorLog::log($db, 'UPDATE BRANCH: COULD NOT GET DATA', $dealer);
    http_redirect($redirection_url.'4');
}
?>