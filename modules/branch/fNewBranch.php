<?php 
/*
File: fNewBranch.php
Author: David Bergmann
Creation Date: 11:49
Last Modified:
Modified By:
*/

Request::setInteger('0:error');
$dealer = new Dealer($db);
$user = new User($db,$_SESSION['serial_usr']);
$user -> getData();

/*RETRIEVING DEFAULT DATA*/

//CATEGORY
$categoryList = $dealer -> getCategories();

//STATUS
$statusList = $dealer -> getStatusList();

//PAYMENT DEDLINE
$deadlineList = $dealer -> getDedline();

//DISCOUNT/COMISSION
$typeList = $dealer -> getPercentageType();

//MANAGERS
$managerbc = new ManagerbyCountry($db,$_SESSION['serial_mbc']);
$managerbc -> getData();
if($user -> isMainManagerUser($_SESSION['serial_usr'])) { //It's a PLANETASSIST's user
	//Countries
	$country = new Country($db);
	$countryList = $country -> getOwnCountries($_SESSION['countryList']);
	$isMainManagerUser = 1;
}
else { //It's a MANAGER's user
	$serialMbc = $_SESSION['serial_mbc'];
	$isOfficialManager = $managerbc -> getOfficial_mbc();
	$hasOfficialSeller = $managerbc -> hasOfficialSeller();
	$country = new Country($db,$managerbc -> getSerial_cou());
	$country -> getData();
	$nameCountry = $country -> getName_cou();
	$idCountry = $country -> getSerial_cou();
	$manager = new Manager($db,$managerbc -> getSerial_man());
	$manager -> getData();
	$mbc = new ManagerbyCountry($db);
	$nameManager = $manager -> getName_man();
	$idManager = $_SESSION['serial_mbc'];
	//Cities
	$city = new City($db);
	$cityList=$city -> getCitiesByCountry($managerbc -> getSerial_cou(),$_SESSION['cityList']);
	//Comissionists
	$comissionistList = $user -> getUsersByManager($_SESSION['serial_mbc']);
	//CreditDays
	$creditD = new CreditDay($db);
	$dataByCountry = $creditD -> getCreditDays($managerbc -> getSerial_cou());
}
/*END DEFAULT DATA*/

$user = new User($db, $_SESSION['serial_usr']);
$paUser = !$user -> isPlanetAssistUser();

$today=date('d/m/Y');

//NEW: If we display the success message (1) we add the branch's code:
$recentlyCreatedBranchCode = 0;
if($error == 1){
    Request::setInteger('1:recentlyCreatedBranchSerial');
    $branch = new Dealer($db,$recentlyCreatedBranchSerial);
    if($branch -> getData()){
		$auxData = $branch -> getAuxData_dea();
		$country = new Country($db,$auxData['serial_cou']);
		$country -> getData();
		$city = new City($db,$auxData['serial_cit']);
		$city -> getData();
		$recentlyCreatedBranchCode = $country -> getCode_cou() . '-' . $city -> getCode_cit() . '-' . $branch->getDea_serial_dea() . '-' . $branch -> getCode_dea();
    }
}

/* CASH MANAGEMENT SECTION */
$parameterBank = new Parameter($db,5);//BANKS
$parameterBank -> getData();
$parameterBank = get_object_vars($parameterBank);
$banks = explode(',',$parameterBank['value_par']);
$account_types = Dealer::getAccountTypes($db);

$smarty->register('today,banks,account_types');
$smarty -> register('error,recentlyCreatedBranchCode,typeList,countryList,dealerList,statusList,deadlineList,categoryList,comissionistList,paUser,nameCountry,nameManager,cityList,comissionistList,dataByCountry,idCountry,serialMbc,isMainManagerUser');
$smarty -> display();
?>