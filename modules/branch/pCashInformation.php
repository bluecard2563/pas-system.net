<?php
/**
 * File: pCashInformation
 * Author: Ing. Patricio Astudillo
 * Creation Date: 20-jun-2012
 * Last Modified: 20-jun-2012
 * Modified By: Ing. Patricio Astudillo
 */

	$dealer = new Dealer($db, $_POST['hdnSerial_dea']);
	if($dealer -> getData()){
		/*NOTIFICATION MAIL INFO*/
		$code = Dealer::getDealerCode($db, $dealer -> getSerial_dea());
		$parentDealer = new Dealer($db, $dealer -> getDea_serial_dea());
		$parentDealer -> getData();

		//USER THAT PERFORMES THE UPDATE
		$user = new User($db, $_SESSION['serial_usr']);
		$user -> getData();
		$name_usr = $user->getFirstname_usr().' '.$user->getLastname_usr();
		
		//CURRENT RESPONSIBLE MAIL
		$user_by_dealer = new UserByDealer($db);
        $user_by_dealer -> setSerial_dea($dealer -> getSerial_dea());
        $user_by_dealer -> getData();
        $finalUser = new User($db, $user_by_dealer -> getSerial_usr());
        $finalUser -> getData();
		
        $misc['comissionistMail'] = $finalUser -> getEmail_usr();
		$misc['managerMail'] = 'gtravez@bluecard.com.ec';
		
		$misc['textForEmail'] = "Se ha actualizado la siguiente informaci&oacute;n de CASH Management de la sucursal <b>" . $dealer -> getName_dea() . 
								"</b> cod : <u>$code</u>, del comercializador <b>" . $parentDealer -> getName_dea() . "</b> cod : <u>" . 
								$parentDealer -> getSerial_dea() . "</u>. La persona que realiz� la actualizaci&oacute;n fue: $name_usr<br><br>";
		$misc['textInfo'] = "Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.";
		$misc['titles'] = array();
		
		$dealer -> setBank_dea($_POST['selBank']);
		$misc['data']['bank'] = $_POST['selBank'];
		array_push($misc['titles'], "Banco");
		
		$dealer -> setAccount_number_dea($_POST['txtAccountNumber']);
		$misc['data']['account_number'] = $_POST['txtAccountNumber'];
		array_push($misc['titles'], "N&uacute;mero de Cuenta");
		
		$dealer -> setAccount_type_dea($_POST['selAccountType']);
		$misc['data']['account_type'] = $global_account_types[$_POST['selAccountType']];
		array_push($misc['titles'], "Tipo de Cuenta");
		
		if($dealer -> update()){
			ERP_logActivity('update', $dealer);//ERP ACTIVITY

			if(GlobalFunctions::sendMail($misc, 'updateDealer')){
				http_redirect('modules/branch/fCashInformation/1');
			} else {
				ErrorLog::log($db, 'UPDATE BRANCH: FAILED TO SEND MAIL', $misc);
				http_redirect('modules/branch/fCashInformation/2');
			}
		}else{
			ErrorLog::log($db, 'UPDATE BRANCH: FAILED TO UPDATE', $dealer);
			http_redirect('modules/branch/fCashInformation/3');
		}
	}else{
		ErrorLog::log($db, 'UPDATE BRANCH: COULD NOT GET DATA', $dealer);
		http_redirect('modules/branch/fCashInformation/4');
	}
?>
