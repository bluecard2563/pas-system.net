<?php
/*
  File: fUpdateBranch.php
  Author: David Bergmann
  Creation Date: 14/01/2010
  Last Modified: Patricio Astudillo
  Modified By: 18/07/2012
 */

Request::setInteger('0:error');
$serial_dea = $_POST['selBranch'];
$dealer = new Dealer($db, $serial_dea);

/* RETRIEVING DEFAULT DATA */
//Language
$language = new Language($db);
$language->getDataByCode($_SESSION['language']);

//Dealer Types
$dealerT = new DealerType($db);
$dTypesList = $dealerT->getDealerTypes(NULL, $language->getSerial_lang());

//CATEGORY
$categoryList = $dealer->getCategories();

//STATUS
$statusList = $dealer->getStatusList();

//PAYMENT DEDLINE
$deadlineList = $dealer->getDedline();

//DISCOUNT/COMISSION
$typeList = $dealer->getPercentageType();
/* END DEFAULT DATA */


if ($dealer->getData()) {
    //BRANCH DATA
    $data['serial_dea'] = $serial_dea;
    $data['serial_sec'] = $dealer->getSerial_sec();
    $data['serial_mbc'] = $dealer->getSerial_mbc();
    $data['serial_cdd'] = $dealer->getSerial_cdd();
    $data['serial_cdd'] = $dealer->getSerial_cdd();
    $data['serial_dlt'] = $dealer->getSerial_dlt();
    $data['status_dea'] = $dealer->getStatus_dea();
    $data['id_dea'] = $dealer->getId_dea();
    $data['code_dea'] = $dealer->getCode_dea();
    $data['official_dea'] = $dealer->getOfficial_seller_dea();
    $data['name_dea'] = $dealer->getName_dea();
    $data['category_dea'] = $dealer->getCategory_dea();
    $data['address_dea'] = $dealer->getAddress_dea();
    $data['phone1_dea'] = $dealer->getPhone1_dea();
    $data['phone2_dea'] = $dealer->getPhone2_dea();
    $data['contract_date'] = $dealer->getContract_date_dea();
    $data['fax_dea'] = $dealer->getFax_dea();
    $data['email1_dea'] = $dealer->getEmail1_dea();
    $data['email2_dea'] = $dealer->getEmail2_dea();
    $data['contact_dea'] = $dealer->getContact_dea();
    $data['phone_contact'] = $dealer->getPhone_contact_dea();
    $data['email_contact'] = $dealer->getEmail_contact_dea();
    $data['visits_number'] = $dealer->getVisits_number_dea();
    $data['percentage_dea'] = $dealer->getPercentage_dea();
    $data['type_dea'] = $dealer->getType_percentage_dea();
    $data['payment_dedline'] = $dealer->getPayment_deadline_dea();
    $data['bill_to'] = $dealer->getBill_to_dea();
    $data['dea_serial_dea'] = $dealer->getDea_serial_dea();
    $data['pay_contact'] = $dealer->getPay_contact_dea();
    $data['assistance_contact'] = $dealer->getAssistance_contact_dea();
    $data['email_assistance'] = $dealer->getEmail_assistance_dea();
    $data['branch_number'] = $dealer->getBranch_number_dea();
    $data['bonus_to_dea'] = $dealer->getBonus_to_dea();
    $data['manager_name'] = $dealer->getManager_name_dea();
    $data['manager_phone'] = $dealer->getManager_phone_dea();
    $data['manager_email'] = $dealer->getManager_email_dea();
    $data['manager_birthday'] = $dealer->getManager_birthday_dea();
    $data['created_on'] = $dealer->getCreated_on_dea();
    $data['aux'] = $dealer->getAuxData_dea();
    $data['creator_name'] = $data['aux']['creator_name'];
    $data['bank'] = $dealer->getBank_dea();
    $data['account_type'] = $dealer->getAccount_type_dea();
    $data['account_number'] = $dealer->getAccount_number_dea();
	$data['minimumKits'] = $dealer->getMinimum_kits_dea();
	$data['notifications'] = $dealer->getNotificacions_dea();

    $creditD = new CreditDay($db);
    $creditDList = $creditD->getCreditDays($data['aux']['serial_cou']);

    //MANAGER DATA
    $user_by_dealer = new UserByDealer($db);
    $user_by_dealer->setSerial_dea($serial_dea);
    $user_by_dealer->getData();
    $data['serial_usr'] = $user_by_dealer->getSerial_usr();

    $comissionistUser = new User($db, $user_by_dealer->getSerial_usr());
    $comissionistUser->getData();
    $data['comissionist_serial_mbc'] = $comissionistUser->getSerial_mbc();
    $data['comissionist_name'] = $comissionistUser->getFirstname_usr() . ' ' . $comissionistUser->getLastname_usr();

    //DEALER DATA
    $parentDealer = new Dealer($db, $dealer->getDea_serial_dea());
    $parentDealer->getData();

    $data['dea_serial_dea'] = $parentDealer->getSerial_dea();
    $data['dea_name_dea'] = $parentDealer->getName_dea();
    $data['dea_serial_sec'] = $parentDealer->getSerial_sec();
    $data['dea_code_dea'] = $parentDealer->getCode_dea();
    $data['dea_aux'] = $parentDealer->getAuxData_dea();
    $data['manager_rights'] = $parentDealer->getManager_rights_dea();
    $data['parent_comission'] = $parentDealer->getPercentage_dea();

    $zone = new Zone($db, $_POST['selZone']);
    $zone->getData();
    $data['name_zon'] = $zone->getName_zon();

    $country = new Country($db, $data['dea_aux']['serial_cou']);
    $country->getData();
    $data['dea_name_cou'] = $country->getName_cou();

    $city = new City($db, $data['dea_aux']['serial_cit']);
    $city->getData();
    $data['dea_name_cit'] = $city->getName_cit();

    $sector = new Sector($db, $data['dea_serial_sec']);
    $sector->getData();
    $data['dea_name_sec'] = $sector->getName_sec();

    $country = new Country($db, $data['aux']['serial_cou']);
    $country->getData();
    $data['name_cou'] = $country->getName_cou();
    $data['code_cou'] = $country->getCode_cou();

    $city = new City($db, $data['aux']['serial_cit']);
    $city->getData();
    $data['name_cit'] = $city->getName_cit();
    $data['code_cit'] = $city->getCode_cit();
    //SECTORS
    $sector = new Sector($db);
    $sectorList = $sector->getSectorsByCity($data['aux']['serial_cit']);

    //RESPONSIBLES
    $user = new User($db);

    //PLANETASSIST USERS
    $mainComissionistList = $user->getMainManagerUsers();

    $user = new User($db, $_SESSION['serial_usr']);
    $paUser = !$user->isPlanetAssistUser();

    $managerbc = new ManagerbyCountry($db, $data['serial_mbc']);
    $managerbc->getData();
    $mbcComissionistList = $user->getUsersByManager($data['serial_mbc']);
    if ($user->isMainManagerUser($_SESSION['serial_usr'])) { //It's a PLANETASSIST's user
        $isMainManagerUser = 1;
        $managerList = $managerbc->getManagerbyCountry($data['aux']['serial_cou'], $_SESSION['serial_mbc']);
    } else { //It's a MANAGER's user
        $managerbc = new Manager($db, $managerbc->getSerial_man());
        $managerbc->getData();
        $data['nameManager'] = $managerbc->getName_man();
    }

    $serial_mbc = $_POST['selManager'];
    $smarty->register('data,sectorList,creditDList,mainComissionistList,mbcComissionistList,serial_mbc,paUser,isMainManagerUser');
} else {
    http_redirect('modules/branch/fSearchBranch/2');
}

/* CASH MANAGEMENT SECTION */
$parameterBank = new Parameter($db, 5); //BANKS
$parameterBank->getData();
$parameterBank = get_object_vars($parameterBank);
$banks = explode(',', $parameterBank['value_par']);
$account_types = Dealer::getAccountTypes($db);

$today = date('d/m/Y');

$smarty->register('today,banks,account_types');
$smarty->register('error,dTypesList,statusList,deadlineList,categoryList,typeList,sectorList');
$smarty->display();
?>