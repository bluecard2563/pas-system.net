<?php
/**
 * File: fCashInformation
 * Author: Ing. Patricio Astudillo
 * Creation Date: 20-jun-2012
 * Last Modified: 20-jun-2012
 * Modified By: Ing. Patricio Astudillo
 */
	Request::setString('0:error');

	$serial_bra = $_SESSION['serial_dea'];
	$branch = new Dealer($db, $serial_bra);
	if($branch -> getData()):
		$branch = get_object_vars($branch);
		unset($branch['db']);

		/* CASH MANAGEMENT SECTION */
		$parameterBank = new Parameter($db,5);//BANKS
		$parameterBank -> getData();
		$parameterBank = get_object_vars($parameterBank);
		$banks = explode(',',$parameterBank['value_par']);
		$account_types = Dealer::getAccountTypes($db);
		
		$smarty->register('today,banks,account_types');	
	else:
		unset($branch);
	endif;
	

	$smarty->register('branch,error');	
	$smarty -> display();
?>
