<?php 
/*
File: fSearchBranch.php
Author: David Bergmann
Creation Date: 14/01/2010
Last Modified: Gabriela Guerrero
Modified By: 24/06/2010
*/

Request::setInteger('0:error');

//Zones list
$zone = new Zone($db);
$zonesList = $zone->getAllowedZones($_SESSION['countryList']);

$smarty->register('zonesList,error');
$smarty->display();
?>