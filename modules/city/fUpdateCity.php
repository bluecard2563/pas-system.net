<?php
/*
File: fUpdateCity.php
Author: Pablo Puente
Creation Date: 28/12/2009 
Last Modified: 06/01/2010 15:37
Modified By: David Bergmann
*/

Request::setString('selCity');

$city = new City($db);
$city->setSerial_cit($selCity);

if($city -> getData()){
	$data['serial_cit'] = $city -> getSerial_cit();
	$data['serial_cou'] = $city -> getSerial_cou();
	$data['name_cit'] = $city -> getName_cit();
        $data['code_cit'] = $city -> getCode_cit();
	//Debug::print_r($data);
}else{	
	http_redirect('modules/city/fSearchCity/2');
}
$smarty -> register('data,message');
$smarty -> display();

?>