<?php
/*
File: fNewPayPhonePayment
Author: David Rosales
Creation Date: 08/09/2016
Last Modified:
Modified By:
*/

Request::setString('selectedBoxes');

//Set variables initial value
$titles = array("Fecha de Factura","# de Factura","Facturado a","Valor");
$totalInvoice = $amountWithTax = $tax = $amountWithoutTax = $amount = 0;
$invoicesList = array();

//Selected Invoices
$checkedInvoices = str_replace('&',',',str_replace('chk=', '', $selectedBoxes));
$serialInvoicesArr = explode(",",$checkedInvoices);

if(is_array($serialInvoicesArr) && $serialInvoicesArr[0] != '') {	
    foreach ($serialInvoicesArr as $serial_inv) {
		//Tax value by Invoice, tax percentage by Invoice
		$tax_val = $tax_per = 0;
		
        $invoice = new Invoice($db, $serial_inv);
        $invoice->getData();
		$tax_val += $invoice->getTotal_inv() - $invoice->getSubtotal_inv();
		
		if ($tax_val > 0 && $invoice->getApplied_taxes_inv() != NULL){
			$taxes_inv = unserialize($invoice->getApplied_taxes_inv());
			if (is_array($taxes_inv)){
				$tax_per = $taxes_inv[0]['tax_percentage'];
			}
		}
		
		//Add details for presentation
		$invoiceDetail['date_inv'] = $invoice->getDate_inv();
		$invoiceDetail['number_inv'] = $invoice->getNumber_inv();
		$invoiceDetail['invoice_to'] = $invoice->getAuxData()['name_cus'];
			
		//Calcs if partial payment and if not
        if($invoice->getSerial_pay()) {
			$payment = new Payment($db, $invoice->getSerial_pay());
            $payment->getData();
			//Partial amount payed
			$totalPendingPayment = $payment->getTotal_to_pay_pay() - $payment->getTotal_payed_pay();
			
			//Check if tax percentage and if amount applies taxes
			if ($tax_per > 0){
				$pendingTax = round($tax_per * $totalPendingPayment / (100+$tax_per),2);
				
				$amountWithTax += $totalPendingPayment - $pendingTax;
				$tax += $pendingTax;
			} else{
				$amountWithoutTax = $totalPendingPayment;
			}
			
			$invoiceDetail['total_to_pay'] = $totalPendingPayment;
            $totalInvoice += $totalPendingPayment;
        } else {
			//Calcs when no partial payment
			if ($invoice->getTotal_inv() > $invoice->getSubtotal_inv()){
				$tax += $invoice->getTotal_inv() - $invoice->getSubtotal_inv();
				$amountWithTax += $invoice->getSubtotal_inv();
			} else {
				$amountWithoutTax += $invoice->getTotal_inv();
			}
			
			$invoiceDetail['total_to_pay'] = $invoice->getTotal_inv();
            $totalInvoice += $invoice->getTotal_inv();
        }
		$invoicesList[] = $invoiceDetail;
    }

	//total amount
    $totalInvoice = number_format($totalInvoice, 2,".","");
	$amount = $amountWithTax + $tax + $amountWithoutTax;
	
	//Int values
	$amountWithTax *= 100;
	$tax *= 100;
	$amountWithoutTax *= 100;
	$amount *= 100;
} else {
    http_redirect('modules/payments/creditCardPayments/fChooseInvoice/2');
}

$smarty->register('totalInvoice,titles,invoicesList,amountWithTax,tax,amountWithoutTax,amount,selectedBoxes'); //selectedBoxes
$smarty->display();
