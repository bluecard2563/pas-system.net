<?php
/*
File: fValidatePayPhonePayment
Author: David Rosales
Creation Date: 15/09/2016
Last Modified:
Modified By:
*/

Request::setInteger('0:error');

$payphoneIDs = "";
		
if($_SESSION['serial_dea']) { //If it's a dealer's user
    $data['userType']="DEALER";
	$data['dealerID']=$_SESSION['serial_dea'];
	
    $branch = new Dealer($db,$_SESSION['serial_dea']);
    $invoicesList = $branch->getInvoicesByBranch(true);
	
	if(is_array($invoicesList)){
        foreach($invoicesList as $invoice){
            $payphone_arr[] = $invoice['serial_inv'].'_'.$invoice['payphone_id']; 
        }
		$payphoneIDs = implode('|', $payphone_arr);
    }
    $titles = array("Fecha de Factura","# de Factura","Facturado a","Total a Pagar");
} elseif($_SESSION['serial_mbc']) {
    $user = new User($db);
    if($user->isMainManagerUser($_SESSION['serial_usr'])) { //If it's PLANETASSIST'S user
        $data['userType']="PLANETASSIST";
        //get the list of countries for the user logged in
        $country=new Country($db);
        $data['countryList']=$country->getPaymentCountries($_SESSION['countryList'], $_SESSION['serial_mbc']);
    } else { //If it's a manager's user
        $data['userType']="MANAGER";
        $managerbc = new ManagerbyCountry($db, $_SESSION['serial_mbc']);
        $managerbc->getData();
        $country = new Country($db,$managerbc->getSerial_cou());
        $country->getData();
        $data['nameCountry'] = $country->getName_cou();
        $city=new City($db);
        $data['cityList']=$city->getPaymentCitiesByCountry($country->getSerial_cou(), $_SESSION['serial_mbc'], $_SESSION['cityList']);
        $manager = new Manager($db,$managerbc->getSerial_man());
        $manager->getData();
        $data['nameManager'] = $manager->getName_man();
        $data['serialMbc'] = $_SESSION['serial_mbc'];
    }
}

$error_description = '';
if ($error == 8 && isset($_SESSION['payPhoneAlerts'])){
	if(!is_array($_SESSION['payPhoneAlerts'])){
		$error_description[] = $_SESSION['payPhoneAlerts'];
	} else {
		$error_description = $_SESSION['payPhoneAlerts'];
	}
}

$smarty -> register('error,data,error_description,payphoneIDs,invoicesList,titles');
$smarty->display();