<?php
/*
File: execute_payment
Author: David Rosales
Creation Date: 08/09/2016
Last Modified:
Modified By:
*/

Request::setString('amountWithTax');
Request::setString('tax');
Request::setString('amountWithoutTax');
Request::setString('amount');
Request::setString('txtRegionCode');
Request::setString('txtPhoneNum');
Request::setString('selectedBoxes');
$_SESSION['payPhoneAlerts'] = null;
//Display errors when redirect
$result = 8;
$paymentErrorMsg = 'Existieron errores durante el proceso. Intente m&aacute;s tarde.';

if(isset($selectedBoxes) && isset($amountWithTax) && isset($tax) && isset($amountWithoutTax) && isset($amount) && isset($txtRegionCode) && isset($txtPhoneNum)) {
	//Serial invoices array
	$checkedInvoices = str_replace('&',',',str_replace('chk=', '', $selectedBoxes));
	$serialInvoicesArr = explode(",",$checkedInvoices);

	//PayPhone API Connection	
	$payphoneSetUp = new PayPhone(CLIENT_PRIVATE_KEY, APPLICATION_PUBLIC_KEY, TOKEN, APPLICATION_ID, null, CLIENT_ID, CLIENT_SECRET, API_SANDBOX_URL);

	//Set transaction and get TransactionID
	$dataTransactionRequest = $payphoneSetUp->setPayPhoneRequest($amount, $amountWithTax, $amountWithoutTax, "es", $tax, -5, NULL, NULL, NULL, NULL, $txtPhoneNum, $txtRegionCode);
	
	if (isset($dataTransactionRequest['result']->TransactionId)){
		//Process payment
		$send_bill = $payphoneSetUp->doPayPhoneTransaction($dataTransactionRequest['result']->TransactionId);
			
		if ($send_bill['msg'] == "success"){
			if (is_array($serialInvoicesArr)){
				$result = 7;
				//Set Payphone Transaction ID
				foreach ($serialInvoicesArr as $serial_inv) {
					$invoice = new Invoice($db, $serial_inv);
					$invoice->getData();
					$invoice->setPayPhone_id($dataTransactionRequest['result']->TransactionId);
					$invoice->update();
				}
			} else {
				$_SESSION['payPhoneAlerts'] = $paymentErrorMsg;
			}
		} else {
			$_SESSION['payPhoneAlerts'] = $paymentErrorMsg;
		}
		
	} elseif ($dataTransactionRequest['msg'] == 'exception_error' && $dataTransactionRequest['result']->ErrorCode == 556){
		$_SESSION['payPhoneAlerts'] = 'Cliente de PayPhone no encontrado para el tel&eacute;fono ingresado.';
	} elseif ($dataTransactionRequest['msg'] == 'exception_error') {
		$_SESSION['payPhoneAlerts'] = $dataTransactionRequest['result']->Message;
	} elseif ($dataTransactionRequest['msg'] == 'error' && $dataTransactionRequest['result'] == 'RegionCodeNotFound') {
		$_SESSION['payPhoneAlerts'] = 'C&oacute;digo de Regi&oacute;n incorrecto.';
	} else {
		$_SESSION['payPhoneAlerts'] = $paymentErrorMsg;
	}

	http_redirect('modules/payphone/fValidatePayPhonePayment/'.$result);

} else {
    http_redirect('modules/payments/creditCardPayments/fChooseInvoice/2');
}
