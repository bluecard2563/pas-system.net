<?php
/*
File: process_request
Author: David Rosales
Creation Date: 16/09/2016
Last Modified:
Modified By:
*/

Request::setString('payphoneIDs');
Request::setString('transactionIDs');
//serial_mbc
if($_SESSION['serial_dea']) { 
    $branch = new Dealer($db,$_SESSION['serial_dea']);
	$branch->getData();
	$serial_mbc = $branch->getSerial_mbc();
} elseif($_SESSION['serial_mbc']) {
    $serial_mbc = $_SESSION['serial_mbc'];
}

$_SESSION['payPhoneAlerts'] = null;
//Display errors when redirect
$result = 8;
$validationErrorMsg = 'No se ha podido validar los pagos. Intente m&aacute;s tarde.';


if(isset($payphoneIDs) || isset ($transactionIDs)) {
	if ($payphoneIDs) {
		$pPhoneTransactionId = $payphoneIDs;
	} else {
		$pPhoneTransactionId = $transactionIDs;
	}
	//Payphone TransactionID
	$invoicesArr = explode('|', $pPhoneTransactionId);
	
	//PayPhone API Connection	
	$payphoneSetUp = new PayPhone(CLIENT_PRIVATE_KEY, APPLICATION_PUBLIC_KEY, TOKEN, APPLICATION_ID, null, CLIENT_ID, CLIENT_SECRET, API_SANDBOX_URL);
	
	if(is_array($invoicesArr)){
        foreach($invoicesArr as $item){
			$total_to_pay = 0;
			//Get Transaction Status
			$payphoneIDsarr = explode('_', $item);
			
			if (is_array($payphoneIDsarr)){
				//Invoices serial
				$serial_inv = $payphoneIDsarr[0];
				$serialInvArr = explode(',',$serial_inv);
				//Transaction ID
				$transactionID = $payphoneIDsarr[1];
				
				//Calculate TOTALS
				foreach ($serialInvArr as $serial_inv) {
					$invoice = new Invoice($db, $serial_inv);
					$invoice->getData();
					//Calcs if partial payment and if not
					if($invoice->getSerial_pay()) {
						$payment = new Payment($db, $invoice->getSerial_pay());
						$payment->getData();
						//Partial amount payed
						$total_to_pay += $payment->getTotal_to_pay_pay() - $payment->getTotal_payed_pay();
					} else {
						$total_to_pay += $invoice->getTotal_inv();
					}
				}
				
				//Check transaction status
				$transactionStatus = $payphoneSetUp->statusPayPhoneTransaction($transactionID);
				
				if ($transactionStatus['msg']=='success'){
					if ($transactionStatus['result']=='Canceled'){
						//Get Invoices number
						$number_invoices = '';
						foreach ($serialInvArr as $inv) {
							$invoice = new Invoice($db, $inv);
							$invoice->getData();
							$number_invoices .= ($number_invoices != '')? ', '.$invoice->getNumber_inv() : $invoice->getNumber_inv();
						}
						$_SESSION['payPhoneAlerts'][] = 'El usuario no proceso el pago de la/s factura/s: '.$number_invoices;
					} else {
						//Get Invoice
						$invoice = new Invoice($db, $serialInvArr[0]);
						$invoice->getData();
						//Get Document By Manager
						$document_info = Invoice::getBasicInvoiceInformation($db, $serialInvArr[0]);

						//UPDATE THE NUMBER
						$number_payment_note=DocumentByManager::retrieveNextDocumentNumber($db, $document_info['serial_man'], 'BOTH', 'PAYMENT_NOTE', TRUE);
						if (!$number_payment_note){
							http_redirect('modules/payments/creditCardPayments/fChooseInvoice/no-document');
						}

						//Payment
						$serial_pay = NULL;
						if ($invoice->getSerial_pay()) {
							$payment = new Payment($db, $invoice->getSerial_pay());
							$payment->getData();
							if ($payment->getStatus_pay() == 'PARTIAL'){
								$payment->setTotal_payed_pay($payment->getTotal_to_pay_pay()+$total_to_pay);
								$payment->setTotal_to_pay_pay($total_to_pay);
								$payment->setStatus_pay("PAID");
								$payment->setExcess_amount_available_pay(0);
								$payment->update();
								$serial_pay = $payment->getStatus_pay();
							} else {
								$result = 1;
							}
						} else {
							$payment = new Payment($db);
							$payment->setTotal_to_pay_pay($total_to_pay);
							$payment->setTotal_payed_pay($total_to_pay);
							$payment->setStatus_pay("PAID");
							$payment->setExcess_amount_available_pay(0);
							$serial_pay = $payment->insert();
						}

						//Payment Detail
						if ($serial_pay){
							Payment::registerEfectivePaymentDate($db, $serial_pay);

							$paymentDetail = new PaymentDetail($db);
							$paymentDetail->setSerial_pay($serial_pay);
							$paymentDetail->setType_pyf("PAYPHONE");
							$paymentDetail->setDocumentNumber_pyf($transactionID);
							$paymentDetail->setAmount_pyf($total_to_pay);
							$paymentDetail->setDate_pyf(date('d/m/Y'));
							$paymentDetail->setComments_pyf("PayPhone Payment");
							$paymentDetail->setSerial_usr($_SESSION['serial_usr']);
							$paymentDetail->setNumber_pyf($number_payment_note);
							if ($paymentDetail->insert()){
								$result = 1;
							} else {
								$result = 4;//ERROR TO INSERT PAYMENT DETAIL
							}
						}
						
						//Invoice
						if ($result == 1){
							//Update payment
							foreach ($serialInvArr as $serial_inv) {
								$invoice = new Invoice($db,$serial_inv);
								$invoice->getData();
								if(!$invoice->getSerial_pay()){//if the invoice doesn't have a payment assigned then assign the current payment
									$invoice->setSerial_pay($serial_pay);//update the invoice serial_pay
								}
								$invoice->setStatus_inv('PAID');//update the invoice status
								
								//CHANGE ALL REGISTERED SALES TO 'ACTIVE' STATUS
								$sale = new Sales($db);
								$saleList=$sale->getSalesByInvoice($serial_inv);
								if(sizeof($saleList)>0){
									foreach($saleList as $s){
										if($s['status_sal']=='REGISTERED'){
											$sale->setSerial_sal($s['serial_sal']);
											if(!$sale->changeStatus('ACTIVE')){ //THE SALES WEREN'T
												$result=6;
												break 2;
											}
										}
									}
								}

								$misc['db']=$db;
								$misc['type_com']='IN';
								$misc['serial_fre']=NULL;
								$misc['percentage_fre']=NULL;
								//GlobalFunctions::calculateCommissions($misc,$serial_inv);
								//GlobalFunctions::calculateBonus($misc,$serial_inv);

								if(!$invoice->update()){//update the serial_pay and  the status_inv
									$result = 5;//ERROR TO ASSIGN THE PAYMENT TO THE INVOICE
								}
							}
							
							//Migrate Payment To ERP
							if (isset($serial_pay) && isset($serial_mbc)){
								ERPConnectionFunctions::migrateWebPaymentToERP($db, $serial_pay, $serial_mbc);
							}
						}
					}
				} elseif ($transactionStatus['msg'] == 'exception_error'){
					$_SESSION['payPhoneAlerts'][] = 'Transacci&oacute;n ID: '.$transactionID.' no encontrada.';
				} else {
					$_SESSION['payPhoneAlerts'][] = $validationErrorMsg;
				}
				if (isset($_SESSION['payPhoneAlerts'])){
					//Clean PayPhoneID variables
					foreach ($serialInvArr as $serial_inv) {
						$invoice = new Invoice($db, $serial_inv);
						$invoice->getData();
						$invoice->setPayPhone_id(NULL);
						$invoice->update();
					}
				}
			}
        }
    }
	
	http_redirect('modules/payphone/fValidatePayPhonePayment/'.$result);

} else {
	$_SESSION['payPhoneAlerts'][] = $validationErrorMsg;
    http_redirect('modules/payphone/fValidatePayPhonePayment/'.$result);
}


