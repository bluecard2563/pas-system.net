<?php
/*
File: pAssignKit.php
Author: Santiago Ben�tez
Creation Date: 23/02/2010 15:01
Last Modified: 20/01/2011
Modified By: Patricio Astudillo
*/

Request::setString('selBranch');
Request::setString('hdnKitByDealerID');
Request::setString('hdnKitsTotal');
Request::setString('selAction');
Request::setString('txtAmount');

$kit = new KitByDealer($db);
$kit->setSerial_dea($selBranch);
if($kit->getDataByDealer()){
	$serial_kbd=$kit->getSerial_kbd();
}else{
	$kit->setTotal_kbd(0);
	$serial_kbd=$kit->insert();

	if(!$serial_kbd){
		http_redirect('modules/kitsByDealer/fAssignKitByDealer/3');
	}
}

$process_done = GlobalFunctions::assignStockKits($db, $selBranch, $serial_kbd, $selAction, $txtAmount);

if($process_done){
    http_redirect('modules/kitsByDealer/fAssignKitByDealer/1');
}else{
    http_redirect('modules/kitsByDealer/fAssignKitByDealer/2');
}
?>