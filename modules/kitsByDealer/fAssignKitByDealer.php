<?php
/*
File: fAssignKit.php
Author: Santiago Ben�tez
Creation Date: 22/02/2010 15:37
Last Modified: 12/08/2010 15:37
Modified By: David Bergmann
*/

Request::setInteger('0:error');
Request::setString('selZone,selCountry,selManager,selDealer,selBranch');


$zone= new  Zone($db);
$country= new  Country($db);
$manager= new  ManagerbyCountry($db);
$dealer= new  Dealer($db);

$params=array();
$paramsBranch=array();
$paramItem=0;

/*Load dealers if selected*/
$aux=array('field'=>'d.status_dea',
           'value'=> 'ACTIVE',
           'like'=> 1);
array_push($params,$aux);
$aux=array('field'=>'dtl.serial_lang',
           'value'=> $_SESSION['serial_lang'],
           'like'=> 1);
array_push($params,$aux);
$aux=array('field'=>'d.serial_mbc',
           'value'=> $selManager,
           'like'=> 1);
array_push($params,$aux);
$aux=array('field'=>'d.dea_serial_dea',
           'value'=> 'NULL',
           'like'=> 1);
array_push($params,$aux);
/*End load dealers if selected*/

/*Load branches if selected*/
$aux=array('field'=>'d.status_dea',
           'value'=> 'ACTIVE',
           'like'=> 1);
array_push($paramsBranch,$aux);
$aux=array('field'=>'dtl.serial_lang',
           'value'=> $_SESSION['serial_lang'],
           'like'=> 1);
array_push($paramsBranch,$aux);
$aux=array('field'=>'d.serial_mbc',
           'value'=> $selManager,
           'like'=> 1);
array_push($paramsBranch,$aux);
$aux=array('field'=>'d.dea_serial_dea',
           'value'=> $selDealer,
           'like'=> 1);
array_push($paramsBranch,$aux);
/*End oad branches if selected*/

/*Recover all available type of contacts from the system*/
$zoneList=$zone->getZones();
if($selZone){
    $countryList=$country->getCountriesByZone($selZone,$_SESSION['countryList']);
    $managerList=$manager->getManagerbyCountry($selCountry, $_SESSION['serial_mbc']);
    $dealerList=$dealer->getDealers($params);
	$branchList=$dealer->getDealers($paramsBranch);
}


$smarty->register('error,zoneList,countryList,managerList,dealerList,branchList,selZone,selCountry,selManager,selDealer,selBranch');
$smarty->display();
?>