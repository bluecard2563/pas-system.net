<?php
/*
File: fDealerPendingPaymentsReport
Author: David Bergmann
Creation Date: 22/07/2010
Last Modified:
Modified By:
*/

$serial_dea = $_SESSION['serial_dea'];

if($serial_dea) {
	if(User::isSubManagerUser($db, $_SESSION['serial_usr'])):
		$dealer = new Dealer($db, $_SESSION['serial_dea']);
		$dealer -> getData();

		$branchList = Dealer::getAllBranchesByDealers($db, $dealer->getDea_serial_dea());
		$invoicesList = array();
		
		foreach($branchList as $bl):
			$dealer ->setSerial_dea($bl['serial_dea']);
			$dealer -> getData();
			
			$temp_invoices = $dealer->getInvoicesByBranch();
			
			if($temp_invoices){
				$invoicesList = array_merge($invoicesList, $temp_invoices);
			}
		endforeach;
	else:
		$branch = new Dealer($db, $serial_dea);
		$branch->getData();

		$invoicesList = $branch->getInvoicesByBranch();
	endif;
	
	$total_days = array();
	$total_days['total_30'] = '0.00';
	$total_days['total_60'] = '0.00';
	$total_days['total_90'] = '0.00';
	$total_days['total_more'] = '0.00';
	$total_days['total'] = 0;

	if(is_array($invoicesList)){

		$titles = array("Fecha de Factura","# de Factura","Facturado a","D&iacute;as de Cr&eacute;dito","Impago","Hasta 30","Hasta 60","Hasta 90","M&aacute;s de 90");

		foreach($invoicesList as &$i){
			if($i['days'] < 0 ){
				$i['days']=0;
			}
			if($i['days'] <= 30){
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_30'] += $i['total_to_pay'];
				}else{
					$total_days['total_30'] += $i['total_to_pay_fee'];
				}
			}elseif($i['days'] > 30 && $i['days'] <= 60){
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_60'] += $i['total_to_pay'];
				}else{
					$total_days['total_60'] += $i['total_to_pay_fee'];
				}
			}elseif($i['days'] > 60 && $i['days'] <= 90){
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_90'] += $i['total_to_pay'];
				}else{
					$total_days['total_90'] += $i['total_to_pay_fee'];
				}
			}else{
				if($i['status_inv']=='STAND-BY'){
					$total_days['total_more'] += $i['total_to_pay'];
				}else{
					$total_days['total_more'] += $i['total_to_pay_fee'];
				}
			}
			if($i['status_inv']=='STAND-BY'){
				$total_days['total'] += $i['total_to_pay'];
			}else{
				$total_days['total'] += $i['total_to_pay_fee'];
			}
			if($i['status_pay'] == 'PARTIAL' || $i['status_pay'] == 'ONLINE_PENDING'){
				$i['number_inv'] = $i['number_inv'].' (A)';
			}elseif($i['status_pay'] == 'VOID'){
				$i['number_inv'] = $i['number_inv'].' (N)';
			}
			if($i['status_inv']=='VOID'){
				$i['number_inv'] = $i['number_inv'].' (CI)';
			}
		}

		$total_days['total_30'] = number_format($total_days['total_30'], 2 , '.', '');
		$total_days['total_60'] = number_format($total_days['total_60'], 2 , '.', '');
		$total_days['total_90'] = number_format($total_days['total_90'], 2 , '.', '');
		$total_days['total_more'] = number_format($total_days['total_more'], 2 , '.', '');
		$total_days['total'] = number_format($total_days['total'], 2 , '.', '');
		
		$invoice_count = count($invoicesList);
	}
}

$smarty->register('serial_dea,invoicesList,titles,total_days,creditDays,invoice_count');
$smarty->display();
?>