<?php
/**
 * File:  pDealerServiceReportXLS
 * Author: Valeria Andino
 * Creation Date: 01/08/2016
 * Description:
 * Last Modified: 
 * Modified by:
 */

	$serial_zon = $_POST['selZone'];
	$serial_cou = $_POST['selCountry'];
	$serial_mbc = $_POST['selManager'];
	$serial_dea = $_POST['selDealer'];
	$serial_sbl = $_POST['selService'];
	$from = $_POST['from'];
	$to = $_POST['to'];

	//get values for the header
	$zone = new Zone($db, $serial_zon);
	$zone->getData();
	$name_zon = $zone->getName_zon();

	$country = new Country($db, $serial_cou);
	$country->getData();
	$name_cou = $country->getName_cou();

	if ($serial_mbc) {
		$manager_by_country = New ManagerbyCountry($db, $serial_mbc);
		$manager_by_country->getData();
		$manager = new Manager($db, $manager_by_country->getSerial_man());
		$manager->getData();
		$name_man = $manager->getName_man();
	} else {
		$name_man = "Todos";
	}
	if ($serial_dea) {

		$dealer = new Dealer($db, $serial_dea);
		$dealer->getData();
		$dealer = $dealer->getName_dea();
	} else {
		$dealer = "Todos";
	}
	if ($serial_sbl) {
		$service = new ServicesbyLanguage($db, $serial_sbl);
		$service->getData();
		$name_ser = $service->getName_sbl();
	} else {
		$name_ser = "Todos";
	}
	$sales_list = ServicesbyLanguage::getReportByServices($db, $from, $to, $serial_sbl, $serial_cou, $serial_mbc, $serial_dea);

	$title = "<table>
					<tr>
						<td colspan='9'><b>REPORTE SERVICIOS</b></td>
					</tr>				
					<tr>&nbsp;</tr>
					<tr>&nbsp;</tr>		
					<tr>
						<td><b>ZONA: </b></td>
						<td align='left' colspan='8'>$name_zon</td>
					</tr>
					<tr>
						<td><b>PAIS: </b></td>
						<td align='left' colspan='8'>$name_cou</td>
					</tr>
					<tr>
					   <td><b>REPRESENTANTE:</b></td>
					   <td>" . $name_man . "</td>
					</tr>
					<tr>
						<td><b>DEALER:</b></td>
						<td>" . $dealer . "</td>
					</tr>
					<tr>
						<td><b>SERVICIO :</b></td>
						<td>" . $name_ser . "</td>
					</tr>
					<tr>
						<td><b>FECHA DESDE: </b></td>
						<td align='left' colspan='8'>$from</td>
					</tr>
					<tr>
						<td><b>FECHA HASTA:  </b></td>
						<td align='left' colspan='8'>$to</td>
					</tr>";
	$title .= "</table><br/>";

	if ($sales_list) {
		$records = " <table border=1>	
							   <tr>
								<td><b>Representante</b></td>
								<td><b>Comercializador</b></td>
								<td><b># Tarjeta</b></td>
								<td><b>Fec. Emisi&oacute;n</b></td>
								<td><b>Cliente</b></td>
								<td><b>Documento</b></td>
								<td><b>Inicio de Vigencia</b></td>
								<td><b>Fin de Vigencia</b></td>
								<td><b>Pa&iacute;s de Destino</b></td>	
								<td><b>Servicio</b></td>
							</tr>";

		foreach ($sales_list as $key => $s) {

			$records .= "<tr>
								<td>{$s['name_man']}</td>
								<td>{$s['name_dea']}</td>
								<td>{$s['card_number_sal']}</td>
								<td>{$s['emission_date_sal']}</td>
								<td>{$s['name']}</td>
								<td>{$s['document_cus']}</td>
								<td>{$s['begin_date_sal']}</td>
								<td>{$s['end_date_sal']}</td>
								<td>{$s['country']}</td>	
								<td>{$s['name_sbl']}</td>
							</tr>";
		}
		$records .= "</table>";
	} else {
		$records = '<b>No existen registros para los parámetros seleccionados.</b>';
	}

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Reporte_Servicios.xls");
	header("Pragma: no-cache");
	header("Expires: 0");	

	echo $title;
	echo $records;
