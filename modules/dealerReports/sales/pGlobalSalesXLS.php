<?php
/**
 * File:  pGlobalSalesXLS
 * Author: Patricio Astudillo
 * Creation Date: 20-abr-2011, 10:56:01
 * Description:
 * Last Modified: 20-abr-2011, 10:56:01
 * Modified by:
 */

	$begin_date = $_POST['txtBeginDate'];
	$end_date = $_POST['txtEndDate'];
	$branches_serials = implode(',', $_POST['dealersTo']);

	$sales_list = Dealer::getGlobalSalesReport($db, $branches_serials, $begin_date, $end_date);

	header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Ventas_de_Sucursales.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$header = "<table>
					<tr>
						<td colspan='3'><b>REPORTE DE VENTAS DE SUCURSALES</b></td>
					</tr>
				</table><br />";

	$header .= "<table>
					<tr>
						<td><b>Fecha de Inicio</b></td>
						<td>$begin_date</td>
						<td><b>Fecha de Fin</b></td>
						<td>$end_date</td>
					</tr>
				</table><br /><br />";
	echo $header;

	if($sales_list){
		$table_title = "<tr>
							<td><b># Tarjeta</b></td>
							<td><b>Fec. Emisi&oacute;n</b></td>
							<td><b>Cliente</b></td>
							<td><b>Producto</b></td>
							<td><b>Inicio de Vigencia</b></td>
							<td><b>Fin de Vigencia</b></td>
							<td><b>Duraci&oacute;n</b></td>
							<td><b>Estado</b></td>
							<td><b>Counter</b></td>
							<td><b>Total</b></td>
						</tr>";

		$current_branch_id = $sales_list['0']['serial_dea'];
		$current_branch_name = $sales_list['0']['code_dea'].' - '.$sales_list['0']['name_dea'];
		$current_table = '';
		$total_sold = 0;
		$countSalesList=count($sales_list);

		foreach($sales_list as $key=>$s){
			if($s['serial_dea'] != $current_branch_id){
				$current_table .= "<tr>
									<td colspan='8'>&nbsp;</td>
									<td><b>TOTAL</b></td>
									<td>$ ".  number_format($total_sold, 2)."</td>
								</tr>";

				echo "<table>
						<tr>
							<td colspan='10'><b>$current_branch_name</b></td>
						<tr>";
				echo $table_title;
				echo $current_table;
				echo "</table><br /> <br />";
				
				$current_table = '';
				$total_sold = 0;
				$current_branch_id=$s['serial_dea'];
			}

			$current_table .= "<tr>
									<td>{$s['card_number_sal']}</td>
									<td>{$s['emission_date_sal']}</td>
									<td>{$s['name_cus']}</td>
									<td>{$s['name_pbl']}</td>
									<td>{$s['begin_date_sal']}</td>
									<td>{$s['end_date_sal']}</td>
									<td>{$s['days_sal']}</td>
									<td>{$s['status_sal']}</td>
									<td>{$s['name_cnt']}</td>
									<td>{$s['total_sal']}</td>
								</tr>";

			$total_sold += $s['total_sal'];
			$current_branch_id = $s['serial_dea'];
			$current_branch_name = $s['code_dea'].' - '.$s['name_dea'];

			if($key==($countSalesList-1)){
				$current_table .= "<tr>
									<td colspan='8'>&nbsp;</td>
									<td><b>TOTAL</b></td>
									<td>$ ".  number_format($total_sold, 2)."</td>
								</tr>";

				echo "<table>
						<tr>
							<td colspan='10'><b>$current_branch_name</b></td>
						<tr>";
				echo $table_title;
				echo $current_table;
				echo "</table><br /> <br />";
			}
		}
	}else{
		echo 'No existen registros para los parámetros seleccionados.';
	}
?>
