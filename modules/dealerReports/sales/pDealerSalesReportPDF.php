<?php
/*
File: pDealerSalesReportPDF
Author: David Bergmann
Creation Date: 22/07/2010
Last Modified: 23/07/2010
Modified By: David Bergmann
*/

include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

$dealer = new Dealer($db, $_SESSION['serial_dea']);
$dealer->getData();

//HEADER
$pdf->ezText('<b>'.utf8_decode("REPORTE DE VENTAS").'</b>', 12, array('justification'=>'center'));
$pdf->ezSetDy(-20);

//PARAMETRS
$pdf->ezText('<b>'.utf8_decode("Sucursal de comercializador: ").'</b>'.utf8_decode($dealer->getName_dea()), 8, array('justification'=>'left'));
$pdf->ezSetDy(-3);
if($_POST['rdoSearch'] == "CUSTOMER") {
	$customer = new Customer($db, $_POST['hdnSerialCus']);
	$customer->getData();
	$pdf->ezText('<b>'.utf8_decode("Cliente: ").'</b>'.utf8_decode($customer->getFirstname_cus()." ".$customer->getLastname_cus()), 8, array('justification'=>'left'));
} elseif($_POST['rdoSearch'] == "PARAMETER") {
	$pdf->ezText('<b>'.utf8_decode("-- Parámetros --").'</b>', 8, array('justification'=>'left'));
	$pdf->ezSetDy(-3);
	if($_POST['selStatus']) {
		$pdf->ezText('<b>'.utf8_decode("Estado: ").'</b>'.utf8_decode($global_salesStatus[$_POST['selStatus']]), 8, array('justification'=>'left'));
		$pdf->ezSetDy(-3);
	}
	if($_POST['selProduct']) {
		$productName = ProductByDealer::getProductsByDealerByLanguage($db, $_POST['selProduct'], $_SESSION['serial_lang']);
		$pdf->ezText('<b>'.utf8_decode("Producto: ").'</b>'.utf8_decode($productName), 8, array('justification'=>'left'));
		$pdf->ezSetDy(-3);
	}

	if($_POST['rdoDate'] == 'coverage')
		$date_string = "(cobertura)";
	else
		$date_string = html_entity_decode ("(emisi&oacute;n)");

	$pdf->ezText('<b>'."Fecha $date_string inicio: ".'</b>'.$_POST['txtBeginDate'], 8, array('justification'=>'left'));
	$pdf->ezSetDy(-3);
	$pdf->ezText('<b>'."Fecha $date_string fin: ".'</b>'.$_POST['txtEndDate'], 8, array('justification'=>'left'));
}
$pdf->ezSetDy(-20);

//***** FILTER RESUKTS BY SESSION USER'S COUNTER. IF ANY
$serial_cnt = Counter::isCounterByDealer($db, $_SESSION['serial_usr'], $_SESSION['serial_dea']);

//Data
$report = Sales::getDealerSalesReport($db, $_POST['rdoSearch'], $_SESSION['serial_dea'], 
										$_POST['hdnSerialCus'],$_POST['txtBeginDate'], $_POST['txtEndDate'], 
										$_POST['selStatus'], $_POST['selProduct'],$_POST['rdoDate'], $serial_cnt);

$titles = array("card_number"=>utf8_decode("<b>Tarjeta #</b>"),
				"emission_date_sal"=>utf8_decode("<b>Fecha de emisión</b>"),
				"name_cus"=> utf8_decode("<b>Cliente</b>"),
				"name_pbl"=>  utf8_decode("<b>Producto</b>"),
				"begin_date_sal"=>utf8_decode("<b>Fecha de inicio</b>"),
				"end_date_sal"=>utf8_decode("<b>Fecha fin</b>"),
				"days_sal"=>utf8_decode("<b>Tiempo</b>"),
				"status_sal"=>utf8_decode("<b>Estado</b>"),
				"name_usr"=>utf8_decode("<b>Counter</b>"),
				"total_sal"=>utf8_decode("<b>Precio</b>"));

if(is_array($report)) {
	$totalSal = 0;
	foreach ($report as &$r) {
		$r['name_cus'] = $r['name_cus'];
		$r['name_pbl'] = $r['name_pbl'];
		$r['status_sal'] = $global_salesStatus[$r['status_sal']];
		if($r['days_sal']>1) {
			$days = "días";
		} else {
			$days = "día";
		}
		$r['days_sal'] = $r['days_sal']." ".utf8_decode($days);

		$r['name_usr'] = $r['name_usr'];
		$totalSal += $r['total_sal'];
	}
	array_push($report, array("card_number"=>"",
							  "emission_date_sal"=>"",
							  "name_cus"=>"",
							  "name_pbl"=>"",
							  "begin_date_sal"=>"",
							  "end_date_sal"=>"",
							  "days_sal"=>"",
							  "status_sal"=>"",
							  "name_usr"=>"Total:",
							  "total_sal"=>number_format($totalSal, 2,".","")));

	$pdf->ezTable($report,$titles,'',
                          array('showHeadings'=>1,
                                'shaded'=>1,
                                'showLines'=>2,
                                'xPos'=>'center',
                                'innerLineThickness' => 0.8,
                                'outerLineThickness' => 0.8,
                                'fontSize' => 8,
                                'titleFontSize' => 8,
                                'cols'=>array(
								'card_number'=>array('justification'=>'center','width'=>50),
								'emission_date_sal'=>array('justification'=>'center','width'=>75),
								'name_cus'=>array('justification'=>'center','width'=>100),
								'name_pbl'=>array('justification'=>'center','width'=>100),
								'begin_date_sal'=>array('justification'=>'center','width'=>75),
								'end_date_sal'=>array('justification'=>'center','width'=>75),
								'days_sal'=>array('justification'=>'center','width'=>50),
								'status_sal'=>array('justification'=>'center','width'=>60),
								'name_usr'=>array('justification'=>'center','width'=>100),
								'total_sal'=>array('justification'=>'center','width'=>50))));
} else {
	$pdf->ezText(utf8_decode('La sucursal de comercializador no tiene ventas correspondientes a los parámetros ingresados.'), 10, array('justification' =>'center'));
}

$pdf->ezStream();
?>