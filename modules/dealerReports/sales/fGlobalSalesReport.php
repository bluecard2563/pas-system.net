<?php
/**
 * File:  fGlobalSalesReport
 * Author: Patricio Astudillo
 * Creation Date: 19-abr-2011, 12:37:32
 * Description:
 * Last Modified: 19-abr-2011, 12:37:32
 * Modified by:
 */
	$serial_dea = $_SESSION['serial_dea'];
	$dealer = new Dealer($db, $serial_dea);
	$dealer -> getData();
	
	if(!User::isSubManagerUser($db, $_SESSION['serial_usr'])):
		$limited_branch_id = $dealer->getSerial_dea();
	endif;
	
	$branch_list = Dealer::getAllBranchesByDealers($db, $dealer->getDea_serial_dea(), $limited_branch_id);
	$today = date('d/m/Y');

	$smarty -> register('serial_dea,branch_list,today');
	$smarty -> display();
?>
