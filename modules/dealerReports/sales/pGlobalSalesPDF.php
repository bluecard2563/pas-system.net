<?php
/**
 * File:  pGlobalSalesPDF
 * Author: Patricio Astudillo
 * Creation Date: 20-abr-2011, 9:12:01
 * Description:
 * Last Modified: 20-abr-2011, 9:12:01
 * Modified by:
 */

	$begin_date = $_POST['txtBeginDate'];
	$end_date = $_POST['txtEndDate'];
	$branches_serials = implode(',', $_POST['dealersTo']);

	$sales_list = Dealer::getGlobalSalesReport($db, $branches_serials, $begin_date, $end_date);
	include_once DOCUMENT_ROOT.'lib/PDFHeadersHorizontal.inc.php';

	$pdf->ezText('<b>'.utf8_decode("REPORTE DE VENTAS DE SUCURSALES").'</b>', 12, array('justification'=>'center'));
	$pdf->ezSetDy(-10);

	$headerValues=array();
	$headerLine1=array('col1'=>'<b>Fecha de Inicio:</b>',
					   'col2'=>$begin_date,
					   'col3'=>'<b>Fecha de Fin:</b>',
					   'col4'=>$end_date);
	array_push($headerValues,$headerLine1);
	
	$pdf->ezTable($headerValues,
				  array('col1'=>'','col2'=>'','col3'=>'','col4'=>''),
				  '',
				  array('showHeadings'=>0,
						'shaded'=>0,
						'showLines'=>0,
						'xOrientation' => 'left',
						'xPos'=>470,
						'fontSize' => 8,
						'titleFontSize' => 8,
						'cols'=>array(
							'col1'=>array('justification'=>'left','width'=>100),
							'col2'=>array('justification'=>'left','width'=>100),
							'col3'=>array('justification'=>'left','width'=>100),
							'col4'=>array('justification'=>'left','width'=>100))));
	$pdf->ezSetDy(-20);	
	
	if($sales_list){
		$titles = array('card_number_sal' => '<b># Tarjeta</b>',
						'emission_date_sal' => html_entity_decode('<b>Fec. Emisi&oacute;n</b>'),
						'name_cus' => '<b>Cliente</b>',
						'name_pbl' => '<b>Producto</b>',
						'begin_date_sal' => '<b>Inicio de Vigencia</b>',
						'end_date_sal' => '<b>Fin de Vigencia</b>',
						'days_sal' => html_entity_decode('<b>Duraci&oacute;n</b>'),
						'status_sal' => '<b>Estado</b>',
						'name_cnt' => '<b>Counter</b>',
						'total_sal' => '<b>Total</b>');
		$options = array('showHeadings'=>1,
									'shaded'=>1,
									'showLines'=>2,
									'xPos'=>'center',
									'innerLineThickness' => 0.8,
									'outerLineThickness' => 0.8,
									'fontSize' => 8,
									'titleFontSize' => 8,
									'cols'=>array(
											'card_number_sal'=>array('justification'=>'center','width'=>50),
											'emission_date_sal'=>array('justification'=>'center','width'=>60),
											'name_cus'=>array('justification'=>'center','width'=>110),
											'name_pbl'=>array('justification'=>'center','width'=>60),
											'begin_date_sal'=>array('justification'=>'center','width'=>60),
											'end_date_sal'=>array('justification'=>'center','width'=>60),
											'days_sal'=>array('justification'=>'center','width'=>50),
											'status_sal'=>array('justification'=>'center','width'=>60),
											'name_cnt'=>array('justification'=>'center','width'=>110),
											'total_sal'=>array('justification'=>'center','width'=>60)));

		$current_branch_id = $sales_list['0']['serial_dea'];
		$current_branch_name = $sales_list['0']['code_dea'].' - '.$sales_list['0']['name_dea'];
		$current_array = array();
		$total_sold = 0;

		$countSalesList=count($sales_list);

		foreach($sales_list as $key=>$s){
			if($s['serial_dea'] != $current_branch_id){
				array_push($current_array, array( "name_cnt" => "<b>TOTAL<b>",
												  "total_sal" => '$ '.number_format($total_sold, 2)));
				$pdf->ezTable($current_array, $titles, "<b>{$current_branch_name}</b>", $options);

				$pdf->ezSetDy(-20);
				$current_array = array();
				$total_sold = 0;
				$current_branch_id=$s['serial_dea'];
			}

			array_push($current_array, $s);
			$total_sold += $s['total_sal'];
			$current_branch_id = $s['serial_dea'];
			$current_branch_name = $s['code_dea'].' - '.$s['name_dea'];

			if($key==($countSalesList-1)){
				array_push($current_array, array( "name_cnt" => "<b>TOTAL<b>",
												  "total_sal" => '$ '.number_format($total_sold, 2)));
				$pdf->ezTable($current_array, $titles, "<b>{$current_branch_name}</b>", $options);

				$pdf->ezSetDy(-20);
			}
		}		
	}else{
		$pdf->ezText(utf8_decode('No existen registros para los parámetros seleccionados.'), 10, array('justification' =>'center'));
	}

	$pdf->ezStream();
?>
