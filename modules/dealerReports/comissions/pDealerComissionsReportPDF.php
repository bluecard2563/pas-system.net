<?php
/*
 * File: pDealerComissionsReportPDF.php
 * Author: Patricio Astudillo
 * Creation Date: 22-jul-2010, 10:10:16
 * Modified By: Patricio Astudillo
 */

$dealer=new Dealer($db, $_SESSION['serial_dea']);
$dealer->getData();
$receiver_name=$dealer->getName_dea();

if($_POST['selStatus']){
	$misc['status']=$_POST['selStatus'];
	if($_POST['selStatus']=='PAID'){
		$status='Pagadas';
	}else{
		$status='Pendientes';
	}
}else{
	$status='Todos';
}

if($_POST['txtCardNumber']){
	$card_number=$_POST['txtCardNumber'];
	$misc['card_number']=$card_number;
}else{
	$card_number='N/A';
}

$comissions_raw_list= new AppliedComission($db);
$comissions_raw_list= $comissions_raw_list->getComissionForReport('DEALER', $_SESSION['serial_dea'], TRUE, $_POST['txtBeginDate'], $_POST['txtEndDate'], TRUE, $misc);

include_once DOCUMENT_ROOT.'lib/PDFHeadersVertical.inc.php';

//Headers
$pdf->ezText('<b>REPORTE DE COMISIONES</b>', 12, array('justification'=>'center'));
$date = html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']);
$pdf->ezSetDy(-2);
$pdf->ezText(utf8_decode($date), 9, array('justification'=>'center'));
$pdf->ezSetDy(-10);

$pdf->ezText('<b>FILTROS UTILIZADOS.-</b>', 11, array('justification'=>'left'));
$pdf->ezSetDy(-10);
$pdf->ezText('<b>Comisiones para: </b>'.$receiver_name, 10, array('justification'=>'left'));
$pdf->ezText('<b>Estado: </b>'.$status, 10, array('justification'=>'left'));
$pdf->ezText('<b>No. Tarjeta: </b>'.$card_number, 10, array('justification'=>'left'));

if(is_array($comissions_raw_list)){
	$final_array_data=array();
	$total_in_in_favor=0;
	$total_refund_in_favor=0;
	$total_paid=0;

	foreach($comissions_raw_list as &$b){ //FORMAT THE RAW ARRAY AND CALCULATE THE TOTALS.
		switch($b['status']){
			case 'Pendiente':
				if($b['type_com']=='IN'){
					$total_in_in_favor+=$b['value_dea_com'];
				}else{
					$total_refund_in_favor+=$b['value_dea_com'];
				}
				break;
			case 'Pagado':
				if($b['type_com']=='IN'){
					$total_paid+=$b['value_dea_com'];
				}else{
					$total_paid-=$b['value_dea_com'];
				}
				break;
		}

		if($b['type_com']=='IN'){
			$b['type_com']='A Favor';
		}else{
			$b['type_com']='Por Reembolsar';
		}

		array_push($final_array_data, $b);
	}

	$pdf->ezSetDy(-10);
	$pdf->ezTable($final_array_data,
				  array('card_number_sal'=>'<b>No. Tarjeta</b>',
						'name_pbl'=>'<b>Producto</b>',
						'name_cus'=>'<b>Cliente</b>',
						'number_inv'=>'<b>No. Factura</b>',
						'value_dea_com'=>html_entity_decode('<b>Comisi&oacute;n</b>'),
						'type_com'=>'<b>Tipo</b>',
						'status'=>'<b>Estado</b>'),
				  '',
				  array('showHeadings'=>1,
						'shaded'=>0,
						'showLines'=>2,
						'xPos'=>'center',
						'fontSize' => 9,
						'titleFontSize' => 9,
						'cols'=>array(
							'card_number_sal'=>array('justification'=>'center','width'=>55),
							'name_pbl'=>array('justification'=>'center','width'=>80),
							'name_cus'=>array('justification'=>'center','width'=>120),
							'number_inv'=>array('justification'=>'center','width'=>55),
							'value_dea_com'=>array('justification'=>'center','width'=>55),
							'type_com'=>array('justification'=>'center','width'=>80),
							'status'=>array('justification'=>'center','width'=>55)
							)));

	$total_in_in_favor=number_format($total_in_in_favor, 2, '.', '');
	$total_paid=number_format($total_paid, 2, '.', '');
	$total_refund_in_favor=number_format($total_refund_in_favor, 2, '.', '');

	$pdf->ezSetDy(-10);
	$pdf->ezText('<b>Total a Favor: </b>$'.$total_in_in_favor, 11, array('justification'=>'left'));
	$pdf->ezText('<b>Saldo por Reembolsar: </b>$'.$total_refund_in_favor, 11, array('justification'=>'left'));
	$pdf->ezText('<b>Total Pagado: </b>$'.$total_paid, 11, array('justification'=>'left'));
	$pdf->ezSetDy(-10);
}else{
	$pdf->ezSetDy(-10);
	$pdf->ezText(html_entity_decode('No se tienen comisiones registrados de acuerdo a los par&aacute;metros ingresados.'), 10, array('justification'=>'center'));
	$pdf->ezSetDy(-10);
}

$pdf->ezStream();
?>