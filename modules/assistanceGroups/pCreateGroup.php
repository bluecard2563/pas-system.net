<?php
/*
    Document   : pCreateGroups.php
    Created on : Apr 23, 2010, 11:37:44 AM
    Author     : H3Dur4k
    Description:
        Register the new group in db.
*/
Request::setInteger('selCountry');
Request::setString('txtGroupsName');
if(!$selCountry || !$txtGroupsName)
	die('Not enought arguments');
else{
	$assistanceGroup=new AssistanceGroup($db);
	$assistanceGroup->setSerial_cou($selCountry);
	$assistanceGroup->setName_asg($txtGroupsName);
	if($assistanceGroup->insert()){
		$error=1;
	}else{
		$error=2;
	}
 http_redirect("modules/assistanceGroups/fCreateGroup/$error");
}
?>
