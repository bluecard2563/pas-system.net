<?php
/*
    Document   : fEditGroup.php
    Created on : May 3, 2010, 9:14:57 AM
    Author     : H3Dur4k
    Description:
        Edit a group register.
*/
Request::setInteger('selGroup');

$assistanceGroup=new AssistanceGroup($db,$selGroup);
$assistanceGroup->getData();
$group=get_object_vars($assistanceGroup);
unset($group['db']);
$smarty->register('group');
$smarty->display();
?>
