<?php
/*
    Document   : fAssignGroup.php
    Created on : Apr 26, 2010, 10:02:05 AM
    Author     : H3Dur4k
    Description:
        Get data to assign a user to an assistance group.
*/
Request::setInteger('0:error');
$zone=new Zone($db);
$zoneList=$zone->getZones();

$country=new Country($db);
$countryList=$country->getCountriesWithAssistanceGroups();
if($countryList)
		$smarty->register('countryList');
$smarty->register('error,zoneList');
$smarty->display();
?>
