<?php
/*
    Document   : pAssignGroup.php
    Created on : Apr 27, 2010, 4:12:59 PM
    Author     : H3Dur4k
    Description:
        assign user to an assistance group
*/
Request::setInteger('selGroup');
//check for user who already are in the group
$assistanceGroup=new AssistanceGroup($db,$selGroup);
$userList=$assistanceGroup->getUsersInGroup();
$assistanceGroupUser=new AssistanceGroupsByUsers($db);
$error=1;
$user=new User($db);
//if there are old register of users in group gotta compare.
if($userList){
	if(is_array($_POST['usersTo'])){
		foreach($_POST['usersTo'] as $userTo){
			$elementKey=-1;
			if(sizeof($userList)>0){
				//check if there are users already register
				foreach($userList as $key => $alreadyMember){
						if($alreadyMember['serial_usr']==$userTo){
							//case user is already in group but inactive
							if($alreadyMember['status_agu']=='INACTIVE'){
								$assistanceGroupUser->setSerial_usr($userTo);
								$assistanceGroupUser->setSerial_asg($selGroup);
								$assistanceGroupUser->setStatus_agu('ACTIVE');
								$assistanceGroupUser->setType_agu($alreadyMember['type_agu']);
								if(!$assistanceGroupUser->update()){
									$error=2;
								}else{
									$user->setSerial_usr($userTo);
									$user->getData();
									$user->setIn_assistance_group_usr('YES');
									$user->update();
								}
							}
							$elementKey=$key;
							break;
						}
				}
				//unset the user who has already been proccessed
				if($elementKey!=-1){
					unset($userList[$elementKey]);
				}else{
					//register if user was not part of the group as a new member
					$assistanceGroupUser->setSerial_usr($userTo);
					$assistanceGroupUser->setSerial_asg($selGroup);
					$assistanceGroupUser->setStatus_agu('ACTIVE');
					$assistanceGroupUser->setType_agu('OPERATOR');
					if(!$assistanceGroupUser->insert()){
						$error=2;
					}else{
							$user->setSerial_usr($userTo);
							$user->getData();
							$user->setIn_assistance_group_usr('YES');
							$user->update();
						}
				}
			}
			else{//if there are no more old users insert all the new ones
				$assistanceGroupUser->setSerial_usr($userTo);
				$assistanceGroupUser->setSerial_asg($selGroup);
				$assistanceGroupUser->setStatus_agu('ACTIVE');
				$assistanceGroupUser->setType_agu('OPERATOR');
				if(!$assistanceGroupUser->insert()){
					$error=2;
				}else{
						$user->setSerial_usr($userTo);
						$user->getData();
						$user->setIn_assistance_group_usr('YES');
						$user->update();
					}
			}
		}
	}
	//check if there are old member users who are not part of the group anymore
	if($userList){
		foreach($userList as $ul){
			if($ul['status_agu']=='ACTIVE'){
				$assistanceGroupUser->setSerial_usr($ul['serial_usr']);
				$assistanceGroupUser->setSerial_asg($selGroup);
				$assistanceGroupUser->setStatus_agu('INACTIVE');
				$assistanceGroupUser->setType_agu($ul['type_agu']);
				if(!$assistanceGroupUser->update()){
					$error=2;
				}
				else{
						$user->setSerial_usr($ul['serial_usr']);
						$user->getData();
						$user->setIn_assistance_group_usr('NO');
						$user->update();
					}
			}
		}
	}

}else{
	//if there are no old member users register all as new ones
	foreach($_POST['usersTo'] as $userTo){
		$assistanceGroupUser->setSerial_usr($userTo);
		$assistanceGroupUser->setSerial_asg($selGroup);
		$assistanceGroupUser->setStatus_agu('ACTIVE');
		$assistanceGroupUser->setType_agu('OPERATOR');
		if(!$assistanceGroupUser->insert()){
					$error=2;
		}else{
			$user->setSerial_usr($userTo);
			$user->getData();
			$user->setIn_assistance_group_usr('YES');
			$user->update();
		}
	}
}
http_redirect("modules/assistanceGroups/fAssignGroup/$error");
?>
