<?php
/**
 * Created by: Santi Albuja
 * Date: 11/10/2018
 * Time: 13:24
 */

$agreement = new Agreement($db);

if (isset($_POST['txtNameAgreement'])){
    $agreement->setName_agr($_POST['txtNameAgreement']);
}
if (isset($_POST['txtDiscountAgreement'])){
    $agreement->setDiscountValue_agr($_POST['txtDiscountAgreement']);
}

if (isset($_POST['selStatusAgreement'])){
    $agreement->setStatus_agr($_POST['selStatusAgreement']);
}

if ($agreement->insert()){
    $error = 1;
}
else{
    $error = 2;
}
http_redirect('modules/agreements/fNewAgreement/'.$error);