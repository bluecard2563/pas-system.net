<?php
Request::setString('0:serial_agr');
$agreement = new Agreement($db);
$agreement->setSerial_agr($serial_agr);

if ($agreement->getData()){
    $data['serial_agr'] = $agreement->getSerial_agr();
    $data['name_agr'] = $agreement->getName_agr();
    $data['discount_value_agr'] = $agreement->getDiscountValue_agr();
    $data['status_agr'] = $agreement->getStatus_agr();
}
else{
    http_redirect('modules/agreements/fSearchAgreement/2');
}

$agreementStatusList = $agreement->getAllAgreementsStatus();
$smarty -> register('data,agreementStatusList,error');
$smarty -> display();
