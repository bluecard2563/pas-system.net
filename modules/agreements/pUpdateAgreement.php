<?php
/**
 * Created by: Santi Albuja
 * Date: 15/10/2018
 */
$agreement = new Agreement($db);
$agreement->setSerial_agr($_POST['hdnSerial_agr']);
$agreement->setName_agr($_POST['txtNameAgreement']);
$agreement->setDiscountValue_agr($_POST['txtDiscountAgreement']);
$agreement->setStatus_agr($_POST['selStatusAgreement']);

if($agreement->update()){
    http_redirect("modules/agreements/fSearchAgreement/1");
}
else{
    $error = 1;
}
$smarty->register('error,data');
$smarty->display('modules/agreements/fUpdateAgreement.'.$_SESSION['language'].'.tpl');