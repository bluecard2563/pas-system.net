<?php
/**
 * Created by: Santi Albuja
 * Date: 11/10/2018
 * Time: 13:24
 */

Request::setInteger('0:error');
$agreement = new Agreement($db);
$agreementList = $agreement->getAllAgreements();
$smarty -> register('error,agreementList');
$smarty -> display();