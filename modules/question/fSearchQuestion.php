<?php 
/*
File: fSearchQuestion.php
Author: Santiago Benítez
Creation Date: 12/01/2010 13:54
Last Modified: 12/01/2010 13:54
Modified By: Santiago Benítez
*/

Request::setInteger('0:error');
//language
$languageList = Language::getAllLanguages($db);
$actualLanguage = $_SESSION['serial_lang'];
//question
$question=new QuestionByLanguage($db);
$question->setSerial_lang($actualLanguage);
$questions=$question->getQuestionsByLanguage();

$smarty->register('error,languageList,actualLanguage,questions');
$smarty->display();
?>