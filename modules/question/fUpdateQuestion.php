<?php 
/*
File: fUpdateQuestion.php
Author: Santiago Benítez
Creation Date: 12/01/2010 14:18
Last Modified: 12/01/2010 14:18
Modified By: Santiago Benítez
*/
Request::setInteger('hdnQuestionID');

$questionbl=new QuestionByLanguage($db,$hdnQuestionID);

if($questionbl->getData()){
	$data['text_qbl']=$questionbl->getText_qbl();
	$data['serial_que']=$questionbl->getSerial_que();
	$data['serial_qbl']=$hdnQuestionID;
	
	$language = new Language($db);
	$languageList = $language->getRemainingQuestionLanguages($data['serial_que']);
	$actualLanguage = $_SESSION['serial_lang'];
	
	$question=new Question($db,$questionbl->getSerial_que());
	$statusList=$question->getStatusValues();
	$typeList=$question->getTypeValues();
	
	$questionbl=new QuestionByLanguage($db);
	$questionbl->setSerial_que($data['serial_que']);
	$dataTranslations=$questionbl->getQuestionsBySerial(); //data that is going to be displayed in the table
	$titles=array('#','Idioma','Pregunta','Actualizar'); //table's titles
	$text="Nueva Traducci&oacute;n"; //button's text
	$smarty->register('dataTranslations,titles,text,languageList,actualLanguage');
	if($question->getData()){
		$data['status_que']=$question->getStatus_que();
		$data['type_que']=$question->getType_que();
	}else{
		http_redirect('modules/question/fSearchQuestion/3');
	}
}else{
	http_redirect('modules/question/fSearchQuestion/3');
}
$smarty->register('data,statusList,typeList');
$smarty->display();
?>