<?php
/*
File: pNewQuestion.php
Author: Santiago Benítez
Creation Date: 12/01/2010 12:10
Last Modified: 12/01/2010 12:10
Modified By: Santiago Benítez
*/
$question=new Question($db);
$question->setType_que($_POST['selType']);

if($question->insert()){
	$language=new Language($db);
	$language->getDataByCode($_SESSION['language']);
	
	$questionbl=new QuestionByLanguage($db);
	$questionbl->setSerial_que($question->getLastSerial());
	$questionbl->setSerial_lang($language->getSerial_lang());
	$questionbl->setText_qbl($_POST['txtText']);

	if($questionbl->insert()){
		http_redirect('modules/question/fNewQuestion/1'); 
	}
	else{
		http_redirect('modules/question/fNewQuestion/2'); 
	}
}
else{
	http_redirect('modules/question/fNewQuestion/2'); 
}

?>
