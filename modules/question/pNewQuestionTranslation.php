<?php 
/*
File: fUpdateQuestion.php
Author: Santiago Benítez
Creation Date: 22/01/2010 10:18
Last Modified: 22/01/2010 10:18
Modified By: Santiago Benítez
*/
Request::setInteger('serial_que');

$language=new Language($db);

$questionbl=new QuestionByLanguage($db);
$questionbl->setSerial_que($serial_que);
$questionbl->setSerial_lang($_POST['selLanguage']);
$questionbl->setText_qbl($_POST['txtText']);

if($questionbl->insert()){
	http_redirect('modules/question/fSearchQuestion/5'); 
}
else{
	http_redirect('modules/question/fSearchQuestion/6'); 
}
?>