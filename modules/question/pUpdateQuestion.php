<?php
/*
File: pUpdateQuestionTranslation.php
Author: Santiago Benítez
Creation Date: 12/01/2010 14:56
Last Modified: 12/01/2010 14:56
Modified By: Santiago Benítez
*/

Request::setInteger('hdnQuestionID');

$question=new Question($db,$hdnQuestionID);

	
	if($question->getData()){
		$question->setType_que($_POST['selType']);
		$question->setStatus_que($_POST['selStatus']);
		
		if($question->update()){
			http_redirect('modules/question/fSearchQuestion/2');
		}else{
			$error=1;
			$data['serial_que']=$hdnQuestionID;
			$question->getData();
			$data['type_que']=$question->getType_que();
			$data['status_que']=$question->getStatus_que();
			$smarty->register('error');
		}
	}



$smarty->register('data,statusList,typeList');
$smarty->display('modules/question/fUpdateQuestion.'.$_SESSION['language'].'.tpl');
?>