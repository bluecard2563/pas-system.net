<?php
/*
  File: pNewFreelance.class.php
  Author: Esteban Angulo
  Creation Date: 30/12/2009 16:37
  Last Modified: 24/08/2011
 */

	$freelance = new Freelance($db);
	$freelance->setName_fre($_POST['txtFirstname']);
	$freelance->setDocument_fre($_POST['txtDocument']);
	$freelance->setAddress_fre($_POST['txtAddress']);
	$freelance->setPhone1_fre($_POST['txtPhone1']);
	$freelance->setPhone2_fre($_POST['txtPhone2']);
	$freelance->setEmail_fre($_POST['txtMail']);
	$freelance->setComission_prctg_free($_POST['txtComission']);
	$freelance->setUrl_free($_POST['txtUrl']);

	if ($freelance->insert()) {//Insert was correct
		$error = 1;
	} else {//There was an error
		$error = 2;
	}

	http_redirect('main/freelance/' . $error);
?>