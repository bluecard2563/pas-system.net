<?php 
/*
File: fSearchFreelance.php
Author: Esteban Angulo
Creation Date: 31/12/2009 09:40
Last Modified: 31/12/2009
Modified By: 
*/

	Request::setInteger('0:error');
	$freelance = new Freelance($db);
	$freelance_list = $freelance->getFreelances();

	$smarty->register('error,freelance_list');
	$smarty->display();
?>