<?php 
/*
File: fUpdateFreelance.php
Author: Esteban Angulo
Creation Date: 31/12/2009 10:15
Last Modified: 06/01/2010
Modified By:  
*/

Request::setInteger('0:serial_fre');

$freelance=new Freelance($db, $serial_fre);

if($freelance->getData()){
	$data['serial_fre']=$serial_fre;
	$data['name_fre']=$freelance->getName_fre();
	$data['document_fre']=$freelance->getDocument_fre();
	$data['address_fre']=$freelance->getAddress_fre();
	$data['phone1_fre']=$freelance->getPhone1_fre();
	$data['phone2_fre']=$freelance->getPhone2_fre();
	$data['email_fre']=$freelance->getEmail_fre();
	$data['comission_fre']=$freelance->getComission_prctg_free();
	$data['url_fre']=$freelance->getUrl_free();
	$data['status_fre']=$freelance->getStatus_fre();
	
	$status_fre = Freelance::getAllStatus($db);
	
	$smarty->register('error,data,status_fre');
	$smarty->display();
}else{
	http_redirect('modules/freelance/fSearchFreelance/1');
}
?>