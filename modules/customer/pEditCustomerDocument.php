<?php
/*
File: pEditCustomerDocument
Author: David Bergmann
Creation Date: 11/02/2011
Last Modified:
Modified By:
*/

$customer = new Customer($db, $_POST['hdnCustomerID']);
$customer->getData();

$customer->setDocument_cus($_POST['txtNewDocument']);

if($customer->update()) {
	http_redirect('modules/customer/fEditCustomerDocument/1');
} else {
	http_redirect('modules/customer/fEditCustomerDocument/2');
}

?>