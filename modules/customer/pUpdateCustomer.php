<?php

/*
  File: pUpdateCustomer.php
  Author: Patricio Astudillo M.
  Creation Date: 30/12/2009 11:10
  Last Modified: 29/12/2010
  Modified By: Patricio Astudillo
 */

$customer = new Customer($db, $_POST['serial_cus']);

//**************** RESET PASSWORD ONLY SECTION *************
if ($_POST['hdnResetPassword'] == '1') {
	$customer->getData();
	$password = GlobalFunctions::generatePassword(6, 8, false);
	$customer->setPassword_cus(md5($password));

	$misc['textForEmail'] = 'Estimado(a) ' . $_POST['txtFirstName'] . ' ' . $_POST['txtLastName'] . '<br><br> Su clave ha sido reestablecida.
                           Para poder ingresar nuevamente al sistema sus datos son: ';
	$misc['pswd'] = $password;
	$misc['email'] = $customer->getEmail_cus();
	$misc['username'] = $customer->getEmail_cus();

	if (GlobalFunctions::sendMail($misc, 'resetPassword')) { //Sending the client his login info.
		http_redirect('modules/customer/fSearchCustomer/1');
	} else {
		http_redirect('modules/customer/fSearchCustomer/4');
	}
}

//**************** FULL CUSTOMER UPDATE *************
if ($customer->getData()) {
	$customer->setSerial_cit($_POST['selCity']);
	$customer->setType_cus($_POST['selType']);
	$customer->setDocument_cus($_POST['txtDocument']);

	if ($_POST['txtFirstName'] && $_POST['selType'] == 'PERSON') {
		$customer->setFirstname_cus($_POST['txtFirstName']);
		$customer->setLastname_cus($_POST['txtLastName']);
		$customer->setRelative_cus($_POST['txtRelative']);
		$customer->setBirthdate_cus($_POST['txtBirthdate']);
	} else if ($_POST['txtFirstName1'] && $_POST['selType'] == 'LEGAL_ENTITY') {
		$customer->setFirstname_cus($_POST['txtFirstName1']);
		$customer->setRelative_cus($_POST['txtManager']);
		$customer->setLastname_cus('');
		$customer->setBirthdate_cus('');
	}

	if ($_POST['txtAddress'] && $_POST['selType'] == 'PERSON') {
		$customer->setAddress_cus($_POST['txtAddress']);
	} elseif ($_POST['txtAddress1'] && $_POST['selType'] == 'LEGAL_ENTITY') {
		$customer->setAddress_cus($_POST['txtAddress1']);
	}

	$customer->setPhone1_cus($_POST['txtPhone1']);
	$customer->setPhone2_cus($_POST['txtPhone2']);
	$customer->setCellphone_cus($_POST['txtCellphone']);
	$customer->setEmail_cus($_POST['txtMail']);
    $customer->setGender_cus($_POST['rdGender']);
	$customer->setMedical_background_cus(specialchars($_POST['txtMedicalHistory']));

	($_POST['txtPhoneRelative']) ? $customer->setRelative_phone_cus($_POST['txtPhoneRelative']) : $customer->setRelative_phone_cus($_POST['txtPhoneManager']);
	($_POST['chkVip']) ? $customer->setVip_cus($_POST['chkVip']) : $customer->setVip_cus('0');

	$customer->setStatus_cus($_POST['selStatus']);
	if ($_POST['txtPassword']) {
		$customer->setPassword_cus(md5($_POST['txtPassword']));
	}

	$sessionLanguage = $_SESSION['serial_lang'];
	$questionList = Question::getActiveQuestions($db, $sessionLanguage, $_POST['serial_cus']);

	$answer = new Answer($db);
	$answer->setSerial_cus($hdnCustomerID);
	$answerList = $answer->getAnswers();

	/* PARAMETERS */
	$parameter = new Parameter($db, 1); //-->
	$parameter->getData();
	$parameterValue = $parameter->getValue_par(); //value of the paramater used to display or not the questions
	$parameterCondition = $parameter->getCondition_par(); //condition of the paramater used to display or not the questions
	/* END PARAMETERS */

	if ($customer->update()) {
		commitERPCustomer($db, $customer->getSerial_cus()); //ERP ACTIVITY

		$flag = 0;
		Answer::deleteMyAnswers($db, $_POST['serial_cus']);
		foreach ($questionList as $q) {
			if ($q['serial_ans']) {
				$answer = new Answer($db, $q['serial_ans']);
			} else {
				$answer = new Answer($db);
			}

			$answer->setSerial_cus($_POST['serial_cus']);
			$answer->setSerial_que($q['serial_que']);
			$ans = $_POST['question_' . $q['serial_qbl']];

			if ($ans) {
				if ($q['type_que'] == 'YN') {
					$answer->setYesno_ans($ans);
				} else {
					$answer->setAnswer_ans($ans);
				}

				if ($answer->insert()) {
					$flag = 0;
				} else {
					$flag = 1;
					break;
				}
			}
		}

		if ($flag == 0) {
			http_redirect('modules/customer/fSearchCustomer/2');
		} else {
			http_redirect('modules/customer/fSearchCustomer/7');
		}
	} else {
		http_redirect('modules/customer/fSearchCustomer/6');
	}
} else {
	http_redirect('modules/customer/fSearchCustomer/5');
}
?>
