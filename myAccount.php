<?php

/*
  File: pMyAccount.php
  Author: Esteban Angulo
  Creation Date: 17/02/2010
  Last Modified:
  Modified By:
 */

    include_once DOCUMENT_ROOT . 'lib/qrLibrary/qrlib.php';

    $user = new User($db, $_SESSION['serial_usr']);
    $serial_usr = $_SESSION['serial_usr'];
    $user->getData();
    $user = get_object_vars($user);
    unset($user['db']);
    
    if($user['serial_dea']){
        $organization = new Dealer($db, $user['serial_dea']);
        $organization->getData();
        $organization = $organization->getName_dea();
    }else{
        $organization = ManagerbyCountry::getManagerByCountryName($db, $user['serial_mbc']);
    }

    $city = new City($db, $user['serial_cit']);
    $city->getData();

    $country = new Country($db, $city->getSerial_cou());
    $country->getData();
    $country = $country->getName_cou();
    $city = $city->getName_cit();

    $vc = new Vcard();
    $vc->data['display_name'] = 'VCard - Planet Assist';
    $vc->data['first_name'] = $user['first_name_usr'];
    $vc->data['last_name'] = $user['last_name_usr'];
    $vc->data['company'] = $organization;
    $vc->data['title'] = $user['position_usr'];
    $vc->data['email1'] = $user['email_usr'];
    $vc->data['office_tel'] = $user['phone_usr'];
    $vc->data['cell_tel'] = $user['cellphone_usr'];
    $vc->build();

    $vcard_name = 'vcard_' . time() . '.png';
    $vcard_phisical_route = DOCUMENT_ROOT . 'system_temp_files/' . $vcard_name;
    $vcard_virtual_route = DOCUMENT_ROOT_LOGIC . 'system_temp_files/' . $vcard_name;

    QRcode::png($vc->card, $vcard_phisical_route, 'L', 3, 2);

    $smarty->register('user,city,country,serial_usr,vcard_virtual_route');
    $smarty->display('myAccount.' . $_SESSION['language'] . '.tpl');
?>
