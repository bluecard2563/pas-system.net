<?php
/* 
File: pMyAccount.php
Author: Esteban Angulo
Creation Date: 17/02/2010
Last Modified:
Modified By:
 */

$user = new User($db, $_SESSION['serial_usr']);
$user->getData();
$user->setFirstname_usr($_POST['txtFirstName']);
$user->setLastname_usr($_POST['txtLastName']);
$user->setAddress_usr($_POST['txtAddress']);
$user->setPhone_usr($_POST['txtPhone']);
$user->setCellphone_usr($_POST['txtCellphone']);
$user->setEmail_usr($_POST['txtMail']);

if($_POST['txtPassword']){
	$user -> setPassword_usr(md5($_POST['txtPassword']));
	$password_reset=true;
}

if($user -> update()){
	if($password_reset){
		http_redirect('index/3');
	}else{
		http_redirect('main/myAccount/1');
	}
}else{
	if($password_reset){
		http_redirect('index/4');
	}else{
		http_redirect('main/myAccount/2');
	}
}
?>