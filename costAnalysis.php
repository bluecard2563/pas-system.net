<?php
/**
 * Name: costAnalysis
 * Created by: pastudillo
 * Created on: 28-mar-2017
 * Description: 
 * Modified by: pastudillo
 * Modified on: 28-mar-2017
 */

include_once DOCUMENT_ROOT.'lib/globalInsuranceVars.inc.php';

$beginDate = '01/01/2016';
$endDate = '01/06/2016';

$sql = "SELECT m.name_man, d.name_dea, pbl.name_pbl, s.card_number_sal, s.total_sal, 
                (IFNULL(i.discount_prcg_inv, 0) + IFNULL(i.comision_prcg_inv, 0)) AS 'comissionPercentage', other_dscnt_inv,
                total_inv, DATE_FORMAT(s.emission_date_sal, '%m') AS 'mes',
                SUM(IFNULL(value_ubd_com, 0)) AS 'salesComission'
        FROM sales s
        JOIN invoice i on i.serial_inv = s.serial_inv
            AND s.emission_date_sal BETWEEN STR_TO_DATE('$beginDate', '%d/%m/%Y') AND STR_TO_DATE('$endDate', '%d/%m/%Y')
            AND s.status_sal IN ('ACTIVE', 'EXPIRED', 'STAND-BY')
        JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
        JOIN dealer d ON d.serial_dea = pbd.serial_dea
        JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
            AND mbc.serial_cou = 62
        JOIN manager m ON m.serial_man = mbc.serial_man
        
        JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
        JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro
            AND pbl.serial_lang = 2 
        LEFT JOIN applied_comissions com ON com.serial_sal = s.serial_sal
        GROUP BY s.serial_sal
        ";

//Debug::print_r($sql); die;
$result = $db->getAll($sql);

if($result){
    $table =  '<table>
                    <tr>
                        <td><b>Sucursal</b></td>
                        <td><b>Comercializador</b></td>
                        <td><b>Producto</b></td>
                        <td><b>Nro. Tarjeta</b></td>
                        <td><b>Mes Emision</b></td>
                        <td><b>Total Venta</b></td>
                        <td><b>Total Facturado Real</b></td>
                        <td><b>Total Nuevas Facturas </b></td>                        
                        <td><b>Monto Descuento</b></td>
                        <td><b>Monto Com. Tarjeta Credito</b></td>
                        <td><b>Monto Reaseguro</b></td>
                        <td><b>Monto ISD Reaseguro</b></td>
                        <td><b>Monto Costo Planet</b></td>
                        <td><b>Monto Comision Canal</b></td>
                        <td><b>Monto IVA Com. Canal</b></td>
                        <td><b>Monto Com. Responsable</b></td>
                        <td><b>Monto Siniestralidad</b></td>
                        <td><b>Monto ISD Siniestralidad</b></td>
                        <td><b>Monto IESS</b></td>
                        <td><b>Monto Incentivos</b></td>
                        <td><b>Monto IVA Incentivos</b></td>
                        <td><b>Monto Reserva Tecnica</b></td>
                        <td><b>SALDO</b></td>
                    </tr>
                    ';
    
    foreach($result as $sale){
        $primaNeta = $sale['total_sal'] - ($sale['total_sal'] * ($sale['other_dscnt_inv'] / 100));
        
        $totalInvoiced = round($primaNeta + ($emissionRightsFee + $managementFee + ($primaNeta * $farmersTax)), 2);
        
        $costs = array();
        $costs['creditCardComission'] = round($totalInvoiced * $creditCardComissionPercentage, 2);
        
        $costs['reinsuranceFee'] = round($sale['total_sal'] * $reinsurancePercentage, 2);
        
        $costs['isdReinsurance'] = round($costs['reinsuranceFee'] * $isdTaxPercentage, 2);
        
        $costs['costPA'] = round($sale['total_sal'] * $paincPercentage, 2);
        
        $costs['comCanal'] = round($primaNeta * ($sale['comissionPercentage'] / 100), 2);
        
        $costs['vatComCanal'] = round($costs['comCanal'] * $vatPercentage, 2);
        
        $costs['comUBD'] = ($sale['salesComission'] > 0)?$sale['salesComission']:round($primaNeta * $ubdPercentge, 2);
        
        $costs['accidentRate'] = round($sale['total_sal'] * $accidentRatePercentage, 2);
        
        $costs['isdAccidentRate'] = round($costs['accidentRate'] * $isdTaxPercentage, 2);
        
        $costs['iessCom'] = round($costs['comUBD'] * $iessPercentage, 2);
        
        $costs['bonus'] = round($primaNeta * $bonusPercentage, 2);
        
        $costs['vatBonus'] = round($costs['bonus'] * $vatPercentage, 2);
        
        $costs['reserve'] = round($sale['total_sal'] * $operationReserve, 2);
        
        $saldo = $sale['total_sal'];
        
        foreach($costs as $cost_item){
            $saldo -= $cost_item;
        }
        
        $table .= "<tr>
                        <td>{$sale['name_man']}</td>
                        <td>{$sale['name_dea']}</td>
                        <td>{$sale['name_pbl']}</td>
                        <td>{$sale['card_number_sal']}</td>
                        <td>'{$sale['mes']}</td>
                        <td>{$sale['total_sal']}</td>
                        <td>{$sale['total_inv']}</td>
                        <td>$totalInvoiced</td>
                        <td>{$sale['other_dscnt_inv']}</td>
                        <td>{$costs['creditCardComission']}</td>
                        <td>{$costs['reinsuranceFee']}</td>
                        <td>{$costs['isdReinsurance']}</td>
                        <td>{$costs['costPA']}</td>
                        <td>{$costs['comCanal']}</td>
                        <td>{$costs['vatComCanal']}</td>
                        <td>{$costs['comUBD']}</td>
                        <td>{$costs['accidentRate']}</td>
                        <td>{$costs['isdAccidentRate']}</td>
                        <td>{$costs['iessCom']}</td>
                        <td>{$costs['bonus']}</td>
                        <td>{$costs['vatBonus']}</td>
                        <td>{$costs['reserve']}</td>
                        <td>".round($saldo, 2)."</td>
                </tr>";
    }
    
    header('Content-type: application/vnd.ms-excel');
	header("Content-Disposition: attachment; filename=Analisis_Costos.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
    
    echo $table;
}
