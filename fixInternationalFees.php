<?php
/**
 * File: fixInternationalFees
 * Author: Patricio Astudillo
 * Creation Date: 03/12/2013
 * Last Modified: 03/12/2013
 * Modified By: Patricio Astudillo
 */

	$sql = "UPDATE sales SET 
					`international_fee_amount` = total_cost_sal,
					`international_fee_used` = 'NO'
			WHERE serial_sal >= 181669;
			
			UPDATE sales SET 
				`international_fee_status` = 'TO_COLLECT'
			WHERE serial_sal >= 181669
			AND status_sal IN ('ACTIVE', 'EXPIRED', 'REGISTERED', 'STANDBY');";
	
	//************************* GENERAL FIX FOR VOID AND REFUND *******************************
	$sql = "SELECT serial_sal, international_fee_amount, status_sal, free_sal,
					international_fee_status, emission_date_sal
			FROM sales
			WHERE serial_sal >= 181669
			AND international_fee_status <> 'TO_COLLECT'";
	
	$sales = $db->getAll($sql);
	
	$sale = new Sales($db);
	foreach($sales as $item){
		$sale->setSerial_sal($item['serial_sal']);
		$sale->getData();
		$sale->setInternationalFeeStatus_sal('TO_REFUND');
		
		if($item['free_sal'] == 'NO'){ //Free Sales always Charge
			switch($item['status_sal']){
				case 'VOID':
					if(wasVoidedBeforeCoverage($db, $item['serial_sal'])){
						$sale->setInternationalFeeAmount_sal('0');
					}
					break;
				case 'REFUNDED':
					if(wasRefundedBeforeCoverage($db, $item['serial_sal'])){
						$sale->setInternationalFeeAmount_sal('0');
					}
					break;
			}
		}
		
		if(!$sale->update()):
			die("ERROR ON SALES ID: {$item['serial_sal']}");
		endif;
	}
	
	
	function wasVoidedBeforeCoverage($db, $serial_sal){
		$sql = "SELECT IF(DATEDIFF(s.begin_date_sal, date_slg) > 0, 'YES', 'NO') AS 'before_coverage'
				FROM sales s
				JOIN sales_log sl ON sl.serial_sal = s.serial_sal
					AND sl.type_slg IN ('VOID_INVOICE', 'VOID_SALE')
					AND status_slg = 'AUTHORIZED'
				WHERE s.serial_sal = $serial_sal";
		
		$result = $db -> getOne($sql);
		
		if($result){
			if($result == 'YES'){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	function wasRefundedBeforeCoverage($db, $serial_sal){
		$sql = "SELECT IF(DATEDIFF(s.begin_date_sal, auth_date_ref) > 0, 'YES', 'NO') AS 'before_coverage'
				FROM sales s
				JOIN refund r ON r.serial_sal = s.serial_sal
					AND status_ref = 'APROVED'
				WHERE s.serial_sal = $serial_sal";
		
		$result = $db -> getOne($sql);
		
		if($result){
			if($result == 'YES'){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
?>
