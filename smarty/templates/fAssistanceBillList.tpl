
{assign var="title" value='FACTURAS DE ASISTENCIAS'}

<form name="frmAssistanceBillList" id="frmAssistanceBillList" method="post" action="{$document_root}pAssistanceBillList">

	<div class="span-24 last title ">
    	Facturas de Asistencias<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
			{if $error eq 1}
			<div class="span-10 success" align="center">
				Actualizaci&oacute;n de las facturas exitosa.
			</div>
			{elseif $error eq 2}
			<div class="span-10 error" align="center">
				Ocurri&oacute; un error al actualizar las facturas.
			</div>
			{/if}
    </div>
    {/if}

	<div class="span-9 prepend-8 last">
		<ul id="alerts" class="alerts"></ul>
	</div>
	
	<div class="prepend-3 span-18 last line">

		<div class="prepend-6 span-3 label">* Tipo de Factura:</div>
		<div class="span-5 append-3 last" id="selManagerContainer">
			<select id="type_prq" name="type_prq" title="El campo 'Tipo de Factura' es obligatorio">
				<option value="">- Seleccione -</option>
				{foreach from=$type_prq_list key="k" item="tprq"}
				<option value="{$k}">{$tprq}</option>
				{/foreach}
			</select>
		</div><br><br>
		<div class="prepend-5 span-7 label">Seleccione las facturas a ser actualizadas:</div><br><br>

		<table border="1" class="dataTable" align="center" id="assistanceTable">
			<thead>
				<tr class="header">
					<th style="text-align: center;">Seleccione</th>
					<th style="text-align: center;">Documento</th>
					<th style="text-align: center;">No Documento</th>
					<th style="text-align: center;">Valor Presentado</th>
					<th style="text-align: center;">Valor Aprobado</th>
					<th style="text-align: center;">Fecha de Pedido</th>
					<th style="text-align: center;">Estado</th>
				</tr>
			</thead>
			<tbody>
			{foreach name="assistanceBill" from=$assistance_bill_list item="a" key="k"}
				<tr {if $smarty.foreach.assistanceBill.iteration is even}class="odd"{else}class="even"{/if} >
					<td class="tableField"><input type="checkbox" name="{$a.serial_prq}" id="{$a.serial_prq}"/></td>
					<td class="tableField">{$a.name_dbf}</td>
					<td class="tableField">{$a.comment_dbf}</td>
					<td class="tableField">{$a.amount_reclaimed_prq}</td>
					<td class="tableField">{$a.amount_authorized_prq}</td>
					<td class="tableField">{$a.request_date_prq|date_format:'%d/%m/%Y'}</td>
					<td class="tableField">{$global_paymentRequestStatus[$a.status_prq]}</td>
				</tr>
			{/foreach}
			</tbody>
		</table>

	</div>
	<div class="span-24 last buttons line">
		<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
		<input type="submit" name="btnSave" id="btnSave" value="Guardar" >
	</div>
</form>


