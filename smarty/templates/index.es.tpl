{*<img src="{$document_root}img/BAS.png" style="height:100px;margin-left: 0%; margin-bottom: -100px;">*}

<form name="frmLogin" id="frmLogin" method="post" action="{$document_root}index" class="login_form">
	<div class="prepend-2 span-10 line" style="margin-top: 130px;">
		<div class="span-3 right">
			<span class="login-label">Usuario:</span>
		</div>
		<div class="span-6">
			<input type="text" name="txtSystemUser" id="txtSystemUser" class="text-background" title="El campo 'Usuario' es obligatorio." />
		</div>
	</div>
	
	<div class="prepend-2 span-10">
		<div class="span-3 right">
			<span class="login-label">Clave:</span>
		</div>
		<div class="span-6">
			<input type="password" name="txtSystemPassword" id="txtSystemPassword" class="text-background" title="El campo 'Contrase&ntilde;a ' es obligatorio."/>
		</div>
	</div>
	
	<div class="span-12 center" style="margin-bottom: 18px;">
		<label class="lostPass" id="lostPass">Olvid&eacute; mi clave</label>
	</div>

    
    <div class="span-12 center">
		<input type="button" id="btnSubmit" class="login_submit" value="Ingresar" />
		<input type="hidden" name="systemToken" value="{$systemToken}" id="systemToken" />
		<input type="hidden" name="autoLogin" value="1" id="autoLogin" />
    </div>
              <img src="{$document_root}img/contract_logos/logoblue_contracts.jpg" style="height:30px;margin-top:30px;margin-left: 34%;">
</form>

<div class="span-12 last login-errors" style="margin-top: 100px;">
	<div class="prepend-1 span-9 append-1 last">
		{if $error eq 1}
			<div class="span-9 last error" align="center">
				Contrase&ntilde;a incorrecta, por favor ingrese nuevamente!
			</div>
		{elseif $error eq 2}
			<div class="span-9 last error" align="center">
				El usuario no existe, por favor ingrese nuevamente!
			</div>
		{elseif $error eq 3}
			 <div class="span-9 last success" align="center">
				Contrase&ntilde;a cambiada exitosamente!
			</div>
		{elseif $error eq 4}
			<div class="span-9 last error" align="center">
				Hubo errores en el cambio de contrase&ntilde;a . Por favor vuelva a intentarlo.
			</div>
		{elseif $error eq 5}
			<div class="span-9 last error" align="center">
				El usuario no esta activo.<br>
				Comun&iacute;quese con el Administrador.
			</div>
		{elseif $error eq 6}
			<div class="span-9 last success" align="center">
				Bienvenido(a) al PAS! <br><br>
				La actualizaci&oacute;n de datos se realiz&oacute; con &eacute;xito!<br>
				Ingrese nuevamente con sus nuevos datos.
			</div>
		{elseif $error eq 7}
			<div class="span-9 last error" align="center">
				Lo sentimos, hubo errores en la actualizaci&oacute;n de datos. Por favor vuelva a intentarlo o comun&iacute;quese con el adminsitrador.
			</div>
		{elseif $error eq 8}
			<div class="span-9 last error" align="center">
				Hubo errores al configurar su primer acceso. Comun&iacute;quese con el administrador.
			</div>
		{elseif $error eq 9}
			<div class="span-9 last error" align="center">
				Login Invalido, por favor ingrese sus datos para ingresar.
			</div>
		{elseif $error eq 10}
			<div class="span-9 last success" align="center">
				Bienvenido(a) al PAS! <br><br>
				La actualizaci&oacute;n de datos se realiz&oacute; con &eacute;xito!<br>
				Su nombre de usuario fue modificado, por favor revise su correo para poder continuar.<br>
				Ingrese nuevamente con sus nuevos datos.
			</div>
		{elseif $error eq 11}
			<div class="span-9 last success" align="center">
				Bienvenido(a) al PAS! <br><br>
				La actualizaci&oacute;n de datos se realiz&oacute; con &eacute;xito!<br>
				Su nombre de usuario fue modificado, sin emabrgo no se pudo enviar sus datos por correo, por favor contacte al administrador.<br>
				Ingrese nuevamente con sus nuevos datos.
			</div>
		{/if}
	</div>
	
	<div class="span-12 last">
		<ul id="alerts" class="alerts"></ul>
	</div>
</div>