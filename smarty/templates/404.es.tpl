{assign var="title" value="PAGINA NO ENCONTRADA"}
<br /><br />
<h1 align="center">P&aacute;gina no encontrada</h1>

<h3 align="center">
	Lo sentimos, no se ha encontrado la p&aacute;gina a la que trata de ingresar.
</h3>

<h5 align="center">
	<a href="{$document_root}main">VOLVER</a>
</h5>
