<div class="siteMaintenanceBox" align="center">
	<img src="{$document_root}img/planetIcon.png" border="0" width="100" alt="front_img" class="constructionIcon"/>
	<div class="constructionContainer">
		<span class="constructionMessage">
			We apologize! The site is currently in maintenance.
		</span>
	</div>
</div>