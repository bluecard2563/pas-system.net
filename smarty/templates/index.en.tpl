<form name="frmLogin" id="frmLogin" method="post" action="{$document_root}index" class="login_form">
    <div class="span-24 last head-labels">
        <div class="prepend-6 span-6">
            <span class="line login-label">User Name:</span>
        </div>
        <div class="span-6">
            <span class="line login-label">Password:</span>
        </div>
    </div>
    <div class="span-24 last">
        <div class="prepend-6 span-6">
            <input type="text" name="txtUser" id="txtUser" class="fondoTextField" value="" title="The field 'User Name' is required." />
        </div>
        <div class="span-6 append-6 last">
            <input type="password" name="txtPassword" id="txtPassword" class="fondoTextField" value="" title="The field 'Password' is required."/>
        </div>
    </div>
    <div class="span-24 last">
        <div class="submit-area prepend-6 span-12 append-6 last" align="center">
                <input type="submit" name="btnSubmit" id="btnSubmit" value="" class="login-btn"/>
            </div>
    </div>
    <div class="span-24 last login-errors">
        <div class="prepend-6 span-12 append-6 last">
            {if $error}
                <div class="span-11 last error" align="center">
                    The credentials are incorrect, please try again!
                </div>
            {/if}
            <div class="span-12 last">
                <ul id="alerts" class="alerts"></ul>
            </div>
        </div>
    </div>
    <!-- QUITAR ESTO CUANDO SE INSTALE!!! -->
    <div class="span-6 append-6 last">
            <select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio.">
            <option value="">-Select-</option>
            {foreach from=$languageList item="l"}
            <option value="{$l.code_lang}">{$l.name_lang}</option>
            {/foreach}
        </select>
    </div>
    <!-- HASTA AQUÍ!!! -->
</form>