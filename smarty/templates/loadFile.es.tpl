<input type="hidden" name="hdnClosedFile" id="hdnClosedFile" value="{$closed}" />
<div id="loadFileDialog">
    <form name="frmEditFile" id="frmEditFile" method="post" action="{$document_root}modules/assistance/pUpdateAssistance" class="form_smarty">
        <input type="hidden" name="hdnCardNumberEdit" id="hdnCardNumberEdit" value="{$card_number}"/>
        <div class="span-23 last title ">
            <div class="span-23 last" id="fileNumber">Expediente {$data.serial_fle}</div><br />
            <input type="hidden" name="hdnSerialFleEdit" id="hdnSerialFleEdit" value="{$data.serial_fle}" />
            <label>(*) Campos Obligatorios</label>
        </div>

        <div class="span-6 span-10 prepend-7 last">
            <ul id="alertsDialogEdit" class="alerts"></ul>
        </div>

        <!--LEFT SECTION-->
        <div class="span-11" id="dataTab">
            <!--LOCATION DATA-->
            <div class="span-12" id="locationDataEdit">
                <div class="span-12 last line separator">
                    <label>Datos de ubicaci&oacute;n:</label>
                </div>


                <div class="span-10 last line">
                    <div class="span-6">
                        <label>* Pa&iacute;s:</label>
                    </div>
                    <div class="span-4 last">
                        {if $closed}
                            {$data.name_cou|htmlall}
                            <input type="hidden" name="selCountryEdit" id="selCountryEdit" value="{$data.serial_cou}"/>
                        {else}
                        <select name="selCountryEdit" id="selCountryEdit" title='El campo "Pa&iacute;s" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$countryList item="cl"}
                                <option value="{$cl.serial_cou}" {if $data.serial_cou eq $cl.serial_cou}selected{/if}>{$cl.name_cou}</option>
                            {/foreach}
                        </select>
                        {/if}
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-6">
                        <label>* Ciudad:</label>
                    </div>
                    <div class="span-4 last line" id="cityContainerEdit">
                        {if $closed}
                            {$data.name_cit|htmlall}
                        {else}
                        <select name="selCityEdit" id="selCityEdit"  title='El campo "Ciudad" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$cityList item="c"}
                                <option value="{$c.serial_cit}" {if $data.serial_cit eq $c.serial_cit}selected{/if}>{$c.name_cit}</option>
                            {/foreach}
                        </select>
                        {/if}
                    </div>
                </div>

                <div class="span-12 last line separator">
                    <label>Datos del contacto:</label>
                </div>

                <div class="span-12 last">
                    <ul id="alertsLocationEdit" class="alerts"></ul>
                </div>

                <div class="prepend-4 span-6 last line" id="displayContactFormContainerEdit">
                    <a id="displayContactFormEdit" style="cursor: pointer;{if $closed}display: none;{/if}">A&ntilde;adir nuevo contacto</a>
                </div>

                <div class="span-10 last line" id="contactFormContainerEdit" style="display: none;">
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>*Nombre del contactante:</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactNameEdit" id="contactNameEdit" title='El campo "Nombre del contactante" es obligatorio.' />
                        </div>
                    </div>
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>*Tel&eacute;fono fijo:</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactPhoneEdit" id="contactPhoneEdit" title='El campo "Tel&eacute;fono fijo" es obligatorio.' />
                        </div>
                    </div>
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>*Tel&eacute;fono movil:</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactMobilePhoneEdit" id="contactMobilePhoneEdit" />
                        </div>
                    </div>
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>Tel&eacute;fono (otro):</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactOtherPhoneEdit" id="contactOtherPhoneEdit" />
                        </div>
                    </div>

                    <div class="span-10 last line">
                        <div class="span-3">
                            <label>*Direcci&oacute;n y  referencias:</label>
                        </div>
                        <div class="span-7 last">
                            <textarea style="height: 45px; width: 300px;" name="txtContactReferenceEdit" id="txtContactReferenceEdit" title='El campo "Ubicaci&oacute;n y  referencias" es obligatorio.'></textarea>
                        </div>
                    </div>
                    <div class="prepend-9 span-2 last line">
                        <input type="button" name="btnAddLocationEdit" id="btnAddLocationEdit" value="A&ntilde;adir" />
                    </div>
                </div>

                <div class="span-12 last line" id="locationContainerEdit">
                    {if $contact_info}
                    <center>
                        <div class="span-12 last line">
                            <table border="0" class="paginate-1 max-pages-7" id="location_table_edit">
                                <thead>
                                    <tr bgcolor="#284787">
                                        {foreach name="titles" from=$titles item="t"}
                                            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                                {$t}
                                            </td>
                                        {/foreach}
                                    </tr>
                                </thead>

                                <tbody>
                                {foreach name="contact_info" from=$contact_info item="ci"}
                                <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.contact_info.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$smarty.foreach.contact_info.iteration}
                                    </td>
                                     <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.name}
                                    </td>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.phone}
                                    </td>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.address|htmlall}
                                    </td>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.date}
                                    </td>
                                </tr>
                                {/foreach}

                                </tbody>
                            </table>
                        </div>
                         <div class="span-11 last pageNavPosition" id="pageNavPositionLocationEdit"></div>
                    </center>
                    {/if}
                </div>

                <div class="prepend-4 span-6 last line">
                    <input type="button" id="nextTabEdit" value="Datos de cobertura &raquo;" />
                </div>
            </div>
            <!--END LOCATION DATA-->

            <!--BENEFITS DATA-->
            <div class="span-12" id="benefitsDataEdit" style="display: none;">
                <div class="span-12 last line separator">
                    <label>Datos de cobertura:</label>
                </div>

                <div class="span-12 last line" id="benefitsMessagesEdit" style="display: none;">
                    <div class="prepend-2 span-8 append-2 last">
                        <div class="span-8" align="center" id="benefitsInnerMessageEdit"></div>
                    </div>
                </div>

                <div class="span-12 last line">
                    <div class="span-4">
                        <label>Tipo de servicio:</label>
                    </div>
                    <div class="span-3">
						{$global_serviceProviderType[$data.service_type]}
						<input type="hidden" id="hdnServiceTypeEdit" name="hdnServiceTypeEdit" value="$data.service_type" />
					</div>
                    
                </div>

                <div class="span-12 last line">
                    <div class="span-7 append-5">
                        <div class="span-4">
                            <label>Coordinador:</label>
                        </div>
                        <div class="span-2" id="providerContainerEdit">
                            <select name="selProviderEdit" id="selProviderEdit" >
                                <option value="">- Seleccione -</option>
								{foreach from=$providerList item="sp"}
									<option value="{$sp.serial_spv}">{$sp.name_spv}</option>
								{/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="prepend-4 span-1 last line specifications">
                        <a style="cursor: pointer" id="displayProviderInfoEdit">Informaci&oacute;n</a>
                    </div>
                </div>

                <div class="span-12 last line" id="medicalTypeEdit" {if $data.service_type neq 'MEDICAL'}style="display: none;"{/if} {if $closed}disabled{/if}>
                    <div class="span-4">
                        <label>Coordinaci&oacute;n m&eacute;dica:</label>
                    </div>
                    <div class="span-3">
                        <select name="selMedicalTypeEdit" id="selMedicalTypeEdit" title="El campo 'Coordinaci&oacute;n m&eacute;dica' es obligatorio">
                            <option value="">- Seleccione -</option>
                            {foreach from=$medicalTypes item="l"}
                                <option value="{$l}" {if $data.medical_type eq $l}selected{/if}>{$global_medicalType.$l}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="span-12 last line" id="benefitsContainerEdit">
                    <center>Seleccione un coordinador.</center>
                </div>

                <div class="prepend-4  span-6">
                    <input type="button" id="prevTabEdit" value="&laquo; Datos de ubicaci&oacute;n" />
                </div>
            </div>
            <!--END BENEFITS DATA-->
        </div>
        <!--END LEFT SECTION-->

        <!--RIGHT SECTION-->
        <div class="prepend-2 span-11 last line">
            <!--MEDICAL DATA-->
            <div class="span-11 last line">
                <div class="span-11 last line separator">
                    <label>Datos m&eacute;dicos:</label>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>* Tipo de asistencia:</label>
                    </div>
                    <div class="span-6 last">
                        {if $closed}
							{$data.type_fle|htmlall}
                        {else}
                        <input type="text" name="txtAssistanceTypeEdit" id="txtAssistanceTypeEdit" value="{$data.type_fle}" title='El campo "Tipo de asistencia" es obligatorio.' />
                        {/if}
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>* Diagn&oacute;stico:</label>
                    </div>
                    <div class="span-6 last">
                        {if $closed}
							{$data.diagnosis_fle|htmlall}
                        {else}
                        <input type="text" name="txtDiagnosisEdit" id="txtDiagnosisEdit" value="{$data.diagnosis_fle}" title='El campo "Diagn&oacute;stico" es obligatorio.' />
                        {/if}
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>Causa:</label>
                    </div>
                    <div class="span-6 last">
                        <textarea name="txtCauseEdit" id="txtCauseEdit" style="height: 35px; width: 275px;" {if $closed}readonly{/if}>{$data.cause_fle|htmlall}</textarea>
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>Antecedentes m&eacute;dicos:</label>
                    </div>
                    <div class="span-6 last">
                        <textarea name="txtMedicalBackgroundEdit" id="txtMedicalBackgroundEdit" style="height: 35px; width: 275px;" {if $closed}readonly{/if}>{$data.medical_background_cus}</textarea>
                    </div>
                </div>
            </div>
            <!--END MEDICAL DATA-->
            <div class="span-11 last line">
                <!--CLIENT BLOG-->
                <div class="span-11 last line" id="clientBlogEdit">
                    <div class="span-11 last line separator">
                        <label>Bit&aacute;cora Cliente:</label>
                    </div>
                    <div class="span-11">
                        <div class="span-11 last line">
                            <textarea readonly name="txtClientBlogEdit" id="txtClientBlogEdit" style="background-color: #d7e8f9;">{$client_blog|strip_tags:true}</textarea>
                        </div>
                        {if not $closed}
                        <div class="span-11 last line">
                            <textarea style="height: 30px; width: 315px; background-color: #d7e8f9;" name="txtInputClientBlogEdit" id="txtInputClientBlogEdit"></textarea>
                            <input type="button" id="btnClientBlogEdit" value="Agregar" style="height: 30px;" />
                        </div>
                        {/if}
                    </div>
                    <div class="span-11">
                        <input type="button" id="btnOperatorEdit" value="Operador" />
                    </div>
                </div>
                <!--END CLIENT BLOG-->

                <!--OPERATOR BLOG-->
                <div class="span-11 last line operatorBlog" id="operatorBlogEdit" style="display: none;">
                    <div class="span-11 last line separator">
                        <label>Bit&aacute;cora Operador:</label>
                    </div>
                    <div class="span-11">
                        <div class="span-11 last line">
                            <textarea readonly name="txtOperatorBlogEdit" id="txtOperatorBlogEdit" style="background-color: #FFDFFF;">{$operator_blog|strip_tags:true}</textarea>
                        </div>
                        {if not $closed}
                        <div class="span-11 last line">
                            <textarea style="height: 30px; width: 315px; background-color: #FFDFFF;" name="txtInputOperatorBlogEdit" id="txtInputOperatorBlogEdit"></textarea>
                            <input type="button" id="btnOperatorBlogEdit" value="Agregar" style="height: 30px;" />
                        </div>
                        {/if}
                    </div>
                    <div class="span-11">
                        <input type="button" id="btnClientEdit" value="Cliente" />
                    </div>
                </div>
                <!--END OPERATOR BLOG-->
            </div>
            {if not $closed}
            <a id="newAlertEdit" style="cursor: pointer; text-decoration:none; font-weight: bold;">
                <img src="{$document_root}img/createAlert.gif" />
                Crear alerta
            </a>
            <br />
            <a id="viewAlertEdit" style="cursor: pointer; text-decoration:none; font-weight: bold;">
                <img src="{$document_root}img/createAlert.gif" />
                Ver alertas
            </a>
            {/if}
        </div>
        <!--END RIGHT SECTION-->
    </form>
	<input type="hidden" id="reopen_permission" value="{$reopen_permission}" />
</div>

{literal}
<script type="text/javascript">
    /*VALIDATION SECTION*/
	hide_ajax_loader();
    $("#frmEditFile").validate({
        errorLabelContainer: "#alertsDialogEdit",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountryEdit: {
                required: true
            },
            selCityEdit: {
                required: true
            },
            txtDiagnosisEdit: {
                required: true
            },
            txtAssistanceTypeEdit: {
                required: true
            },
			selMedicalTypeEdit: {
				required: function() {
					if($('#medicalTypeEdit').css("display") == 'none')
						return false;
					else
						return true;
				}
			}
        }
    });
    /*END VALIDATION SECTION*/

    var browserCheck = (document.all) ? 1 : 0; // ternary operator statements to check if IE. set 1 if is, set 0 if not.

    if($("#location_table_edit").length>0) {
        pager = new Pager('location_table_edit', 2 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPositionLocationEdit'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    if($("#benefits_table_edit").length>0) {
        pager2 = new Pager('benefits_table_edit', 5 , 'Siguiente', 'Anterior');
        pager2.init(7); //Max Pages
        pager2.showPageNav('pager', 'pageNavPositionEdit'); //Div where the navigation buttons will be placed.
        pager2.showPage(1); //Starting page
    }


    if($('#hdnClosedFile').val()=='closed') {
        $("#loadFileDialog").dialog({
                bgiframe: true,
                autoOpen: true,
                autoResize:true,
                height: 600,
                width: 1000,
                modal: true,
                resizable: false,
                closeOnEscape: false,
                open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
                title:"Actualizar Expediente",
                buttons: {
                    'Salir': function() {
						$(this).dialog('close');
						//show_ajax_loader();
                        //location.href=document_root+"modules/assistance/fEditAssistance";
                    },
                    'Reabrir Expediente': function() {
						var reopen_permission = $('#reopen_permission').val();

						if(reopen_permission == 'allowed'){
							if(confirm("Est\u00E1 seguro de reabrir el expediente?")) {
								show_ajax_loader();
								$.getJSON(document_root+"rpc/assistance/reopenFile.rpc", {serial_fle:$('#hdnSerialFleEdit').val()}, function(data){
									if(data) {
										location.href=document_root+"modules/assistance/fEditAssistance/3";
									} else {
										location.href=document_root+"modules/assistance/fEditAssistance/2";
									}
								});
							}
						}else{
							alert('Lo sentimos. Su usario no tiene permisos para reabrir el expediente.');
						}
                    }
                }
        });
    } else {
        $("#loadFileDialog").dialog({
                bgiframe: true,
                autoOpen: true,
                autoResize:true,
                height: 690,
                width: 1000,
                modal: true,
                resizable: false,
                closeOnEscape: false,
                open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
                title:"Actualizar Expediente",
                buttons: {
                    'Guardar y Salir': function() {
						if($("#frmEditFile").valid()) {
							show_ajax_loader();
							$("#frmEditFile").submit();
						}
                    },
					'Enviar e-mail': function() {
						show_ajax_loader();
						if($('#txtDiagnosisEdit').val()) {
							$('#messageText').html('Enviando e-mail');
							$('#uploading_file').dialog('open');
							$.getJSON(document_root+"rpc/assistance/sendInformationEmail.rpc",
							{
							  card_number: $('#hdnCardNumberEdit').val(),
							  serial_fle: $('#hdnSerialFleEdit').val(),
							  diagnosis: $('#txtDiagnosisEdit').val()
							}, function(data){
								hide_ajax_loader();
								$('#uploading_file').dialog('close');
								if(data) {
									alert("E-mail enviado al supervisor de liquidaciones exitosamente.");
								} else {
									alert("Error al enviar e-mail al supervisor de liquidaciones.");
								}
							});
						} else {
							hide_ajax_loader();
							alert("Para poder enviar el e-mail informativo debe ingresar el diagn\u00F3stico de la asistencia.");
						}
					}
                }
        });
    }
    

    /*BLOGS*/
    $("#txtClientBlogEdit").scrollTop(50000);
    $("#txtOperatorBlogEdit").scrollTop(50000);

    $("#btnOperatorEdit").bind("click",function(){
        $("#clientBlogEdit").hide("slow");
        $("#operatorBlogEdit").show("slow");
    });
    $("#btnClientEdit").bind("click",function(){
        $("#operatorBlogEdit").hide("slow");
        $("#clientBlogEdit").show("slow");
    });

    $("#btnClientBlogEdit").bind("click",function(){
        if($("#txtInputClientBlogEdit").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
                $("#txtClientBlogEdit").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlogEdit").val($("#txtClientBlogEdit").val()+"\n");
                $("#txtClientBlogEdit").append($("#txtInputClientBlogEdit").val());
                $("#txtClientBlogEdit").val($("#txtClientBlogEdit").val()+"\n");
            } else {//other browsers
                $("#txtClientBlogEdit").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+": \n");
                $("#txtClientBlogEdit").append($("#txtInputClientBlogEdit").val()+"\n");
            }
            $("#txtInputClientBlogEdit").val("");
            $("#txtClientBlogEdit").scrollTop(50000);
        }
    });

    $("#txtInputClientBlogEdit").bind("keyup",function(event){
        if(event.keyCode == '13' && $("#txtInputClientBlogEdit").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
                $("#txtClientBlogEdit").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlogEdit").val($("#txtClientBlogEdit").val()+"\n");
                $("#txtClientBlogEdit").append($("#txtInputClientBlogEdit").val());
                $("#txtClientBlogEdit").val($("#txtClientBlogEdit").val()+"\n");
            } else {//other browsers
                $("#txtClientBlogEdit").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+": \n");
                $("#txtClientBlogEdit").append($("#txtInputClientBlogEdit").val()+"\n");
            }
            $("#txtInputClientBlogEdit").val("");
            $("#txtClientBlogEdit").scrollTop(50000);
        }
    });

    $("#btnOperatorBlogEdit").bind("click",function(){
        if($("#txtInputOperatorBlogEdit").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
                $("#txtOperatorBlogEdit").append("<br />"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlogEdit").val($("#txtOperatorBlogEdit").val()+"\n");
                $("#txtOperatorBlogEdit").append("<br />"+$("#txtInputOperatorBlogEdit").val());
                $("#txtOperatorBlogEdit").val($("#txtOperatorBlogEdit").val()+"\n");
            }else{//other browsers
                $("#txtOperatorBlogEdit").append("\n"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlogEdit").append("\n"+$("#txtInputOperatorBlogEdit").val());
            }
            $("#txtInputOperatorBlogEdit").val("");
            $("#txtOperatorBlogEdit").scrollTop(50000);
        }
    });

    $("#txtInputOperatorBlogEdit").bind("keyup",function(event){
        if(event.keyCode == '13' && $("#txtInputOperatorBlogEdit").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
                $("#txtOperatorBlogEdit").append("<br />"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlogEdit").val($("#txtOperatorBlogEdit").val()+"\n");
                $("#txtOperatorBlogEdit").append("<br />"+$("#txtInputOperatorBlogEdit").val());
                $("#txtOperatorBlogEdit").val($("#txtOperatorBlogEdit").val()+"\n");
            }else{//other browsers
                $("#txtOperatorBlogEdit").append("\n"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlogEdit").append("\n"+$("#txtInputOperatorBlogEdit").val());
            }
            $("#txtInputOperatorBlogEdit").val("");
            $("#txtOperatorBlogEdit").scrollTop(50000);
        }
    });
    /*END BLOG*/

    /*DIALOG TABS*/
    $("#nextTabEdit").bind("click",function(){
        $("#locationDataEdit").hide();
        $("#benefitsDataEdit").show();
    });
    $("#prevTabEdit").bind("click",function(){
        $("#benefitsDataEdit").hide();
        $("#locationDataEdit").show();
    });
    /*END DIALOG TABS*/


    /*LOCATION CONTAINER*/
    $('#displayContactFormEdit').bind("click",function(){
        $('#displayContactFormEdit').toggle("fast");
        $('#contactFormContainerEdit').toggle("fast");
    });

    $('#btnAddLocationEdit').bind("click",function(){
        //Validates the required fields in locaition container
        var valid = true;
        //RULES
        var digits = /^\d+$/;
        var textOnly = /^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$/;

        //MESSAGES
        $('#alertsLocationEdit').html("");
        if($('#contactNameEdit').val() == "") { //Validates if the contact name field is not empty
            $('#alertsLocationEdit').append('<li><b>El campo "Nombre del contactante" es obligatorio.</b></li>');
            valid=false;
        } else if(!textOnly.test($('#contactNameEdit').val())) { //Validates if the contact name field cantains only text
            $('#alertsLocationEdit').append('<li><b>El campo "Nombre del contactante" admite solo letras.</b></li>');
            valid=false;
        }
        if($('#contactPhoneEdit').val() == "") { //Validates if the phone field is not empty
            $('#alertsLocationEdit').append('<li><b>El campo "Tel&eacute;fono fijo" es obligatorio.</b></li>');
            valid=false;
        } else if(!digits.test($('#contactPhoneEdit').val())) { //Validates if the phone field contains only digits
            $('#alertsLocationEdit').append('<li><b>El campo "Tel&eacute;fono fijo" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if(!digits.test($('#contactMobilePhoneEdit').val()) && $('#contactMobilePhoneEdit').val() != "") { //Validates if the mobile phone field contains only digits
            $('#alertsLocationEdit').append('<li><b>El campo "Tel&eacute;fono movil" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if(!digits.test($('#contactOtherPhoneEdit').val()) && $('#contactOtherPhoneEdit').val() != "") { //Validates if the other phone field contains only digits
            $('#alertsLocationEdit').append('<li><b>El campo "Tel&eacute;fono (otro)" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if($('#txtContactReferenceEdit').val() == "") {
            $('#alertsLocationEdit').append('<li><b>El campo "Direcci&oacute;n y referencias" es obligatorio.</b></li>');
            valid=false;
        }

        if(valid) {
            $('#alertsLocationEdit').css("display", "none");

            var phone = $('#contactPhoneEdit').val();
            if($('#contactMobilePhoneEdit').val() != ""){
                phone+=" / "+$('#contactMobilePhoneEdit').val();
            }
            if($('#contactOtherPhoneEdit').val() != ""){
                phone+=" / "+$('#contactOtherPhoneEdit').val();
            }

            $('#locationContainerEdit').load(document_root+"rpc/assistance/addAssistanceLocations.rpc",
            {
                serial_fle: $('#hdnSerialFleEdit').val(),
                contact_name: $('#contactNameEdit').val(),
                contact_phone: phone,
                contact_address: $('#txtContactReferenceEdit').val()
            },function(){
                $('#contactNameEdit').val("");
                $('#contactPhoneEdit').val("");
                $('#contactMobilePhoneEdit').val("");
                $('#contactOtherPhoneEdit').val("");
                $('#txtContactReferenceEdit').val("");
                $('#displayContactFormEdit').toggle("fast");
                $('#contactFormContainerEdit').toggle("fast");
            });
        } else {
            $('#alertsLocationEdit').css("display", "block");
        }
    });
    /*END LOCATION CONTAINER*/

    /*ALERTS DIALOG*/
    $("#newAlertEdit").bind("click", function(){
         $("#createAlertsDialog").dialog('open');
    });
    $('#viewAlertEdit').bind("click", function(){
        $('#alertsByFileContainer').load(document_root+"rpc/assistance/loadAlertsByFile.rpc", {serial_fle:$('#hdnSerialFleEdit').val()})
        $("#viewAlertsDialog").dialog('open');
    });
    /*END ALERTS DIALOG*/

    /*LOAD PROVIDERS*/
    if($('#hdnClosedFile').val() != 'closed') {
        $("#selCountryEdit").bind("change", function(){
			$('#benefitsContainerEdit').html('<div class="span-12 last line" id="benefitsContainerEdit"><center>Seleccione un coordinador.</center></div>');
            if(this.value==""){
                    $("#selCityEdit option[value='']").attr("selected",true);
                    $("#selProviderEdit option[value='']").attr("selected",true);
            }
            $('#cityContainerEdit').load(document_root+"rpc/loadCities.rpc",{serial_cou: this.value, serial_sel: "Edit", all: true});
            $('#providerContainerEdit').load(document_root+"rpc/loadServiceProviderByCountry.rpc",
            {serial_cou: this.value, type_spv: $('#hdnServiceTypeEdit').val(), sel_id:"Edit"}, function() {
				loadBenefits();
			});
        });
    }

    $('#displayProviderInfoEdit').bind("click", function(){
        if($('#selProviderEdit').val() != '') {
            $('#providersInfoContainer').load(document_root+"rpc/assistance/loadContactsByServiceProvider.rpc",{serial_spv:$('#selProviderEdit').val()});
            $("#providersInfoDialog").dialog( "option", "title", $('#selProviderEdit option:selected').text() );
            $("#providersInfoDialog").dialog('open');
        } else {
            alert("Debe seleccionar un coordinador.");
        }
    });
    /*LOAD PROVIDERS*/

	/*END LOAD BENEFITS*/
	loadBenefits();
	/*END LOAD BENEFITS*/
	function loadBenefits() {
		$('#selProviderEdit').bind("change", function(){
			if(this.value != '') {
			$('#benefitsContainerEdit').html("<center><img src = '"+document_root+"img/loading_small.gif'><b>Cargando Servicios</b></center>");
			$('#benefitsContainerEdit').load(	document_root+"rpc/assistance/loadBenefitsByServiceProvider.rpc",
												{serial_spv:this.value,card_number:$("#hdnCardNumberEdit").val(),serial_fle:$("#hdnSerialFleEdit").val(),edit_dialog:"edit"});
			} else {
				$('#benefitsContainerEdit').html('<center>Seleccione un coordinador.</center>');
			}
		});
	}
	/*END LOAD BENEFITS*/

    function str_replace (search, replace, subject, count) {
            f = [].concat(search),
            r = [].concat(replace),
            s = subject,
            ra = r instanceof Array, sa = s instanceof Array;s = [].concat(s);
        if (count) {
            this.window[count] = 0;
        }
         for (i=0, sl=s.length; i < sl; i++) {
            if (s[i] === '') {
                continue;
            }
            for (j=0, fl=f.length; j < fl; j++) {temp = s[i]+'';
                repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
                s[i] = (temp).split(f[j]).join(repl);
                if (count && s[i] !== temp) {
                    this.window[count] += (temp.length-s[i].length)/f[j].length;}}
        }
        return sa ? s : s[0];
    }
    
    function moneyFormat(amount) {
        var val = parseFloat(amount);
        if (isNaN(val)) { return "0.00"; }
        if (val <= 0) { return "0.00"; }
        val += "";
        // Next two lines remove anything beyond 2 decimal places
        if (val.indexOf('.') == -1) { return val+".00"; }
        else { val = val.substring(0,val.indexOf('.')+3); }
        val = (val == Math.floor(val)) ? val + '.00' : ((val*10 ==
        Math.floor(val*10)) ? val + '0' : val);
        return val;
    }
</script>
{/literal}