{assign var="title" value="CORREO"}
<div class="append-7 span-10 prepend-7 last mainAlerts">
{if $status eq "PENDING"}
        <div class="span-10 success" align="center">
            Su transacción con número de referencia: {$reference} está siendo procesada, en un momento recibirá un correo de confirmación.
        </div>
{elseif $status eq "APPROVED"}
    <div class="span-10 success" align="center">
        Su transacción con número de referencia: {$reference} ha sido APROBADA.
    </div>
{elseif $status eq "REJECTED"}
    <div class="span-10 success" align="center">
        Su transacción con número de referencia: {$reference} ha sido RECHAZADA.
    </div>

{/if}
</div>