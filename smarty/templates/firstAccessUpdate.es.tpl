{assign var="title" value="ACTUALIZACI&Oacute;N DE DATOS"}
<form name="frmFirstAccess" id="frmFirstAccess" method="post" action="{$document_root}pFirstAccessUpdate" class="form_smarty">
	<div class="span-24 last title">
		Actualizaci&oacute;n de Datos <br />
		<label>(*) Campos Obligatorios</label>
	</div>

	{if $error}
	<div class="prepend-6 span-12 append-6 last">
		{if $error eq 'error'}
			<div class="span-11 last error" align="center">
				Debe actualizar sus datos antes de poder navergar por el PAS
			</div>
		{/if}
	</div>
	{/if}

	<div class="span-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Nombre:</div>
		<div class="span-5">  
			<input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$user.first_name_usr}">
		</div>

		<div class="span-3 label">* Apellido</div>
		<div class="span-4 append-4 last">  
			<input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$user.last_name_usr}">
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
		<div class="span-5">
			<input type="text" name="txtDocument" id="txtDocument" title='El campo "Identificaci&oacute;n" es obligatorio' value="{$user.document_usr}">
		</div>

		<div class="span-3 label">* Direcci&oacute;n:</div>
		<div class="span-4 append-4 last">  
			<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$user.address_usr}">
		</div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
        <div class="span-5">
        	{$country}
        </div>

        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">
        	{$city}
         </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-5"> 
			<input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio' value="{$user.phone_usr}">
        </div>

        <div class="span-3 label">Celular:</div>
        <div class="span-4 append-4 last">  
			<input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio' value="{$user.cellphone_usr}">
        </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
			<input type="text" name="txtBirthdate" id="txtBirthdate" value="{$user.birthdate_usr}" title="El campo 'Fecha de Nacimiento' es obligatorio." />
		</div>

        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> 
			<input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio' value="{$user.email_usr}">
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Anterior Contrase&ntilde;a:</div>
        <div class="span-5">
			<input type="password" name="txtOldPassword" id="txtOldPassword" title='El campo "Anterior Contrase&ntilde;a" es obligatorio'>
		</div>

        <div class="span-3 label">* Nueva Contrase&ntilde;a:</div>
        <div class="span-4 append-4 last">
			<input type="password" name="txtPassword" id="txtPassword" title='El campo "Contrase&ntilde;a" es obligatorio'>
		</div>

		<div class="prepend-13 span-3 label">* Confirmar Contrase&ntilde;a:</div>
		<div class="span-4 append-4 last">
		  <input type="password" name="txtRePassword" id="txtRePassword" title='El campo "Contrase&ntilde;a" es obligatorio'>
		</div>
	</div>
	<div class="span-24 last buttons">
		<input type="submit" name="btnOk" id="btnOk" value="Actualizar">
		<input type="hidden" name="today" id="today" value="{$today}"/>
		<input type="hidden" name="serial_usr" id="serial_usr" value="{$serial_usr}" />
	</div>
</form>