{if $dealerList}
    <select name="selDealer" id="selDealer">
        <option value="">- Seleccione -</option>
        {foreach from=$dealerList item="d"}
		<option value="{$d.serial_dea}" >{$d.name_dea|htmlall} - {$d.code_dea}</option>
        {/foreach}
    </select>
{else}
	{if $serial_cit neq ''}
		No existen comercializadores registrados en la ciudad seleccionada.
    {/if}
    <select name="selDealer" id="selDealer" title='El campo "Sucursal" es obligatorio' {if $serial_cit neq ''}style="display:none"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}