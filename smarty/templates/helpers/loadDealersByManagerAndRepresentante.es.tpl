<div class="prepend-5 span-6 append-8 label line">* Seleccione los comercializadores:</div>
<div class="prepend-5 span-10 append-4 last line" id="branchContainer">
    <div class="span-4">
        <div align="center">Seleccionados</div>
        <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-4 last" title="El campo de responsables es obligatorio."></select>
    </div>
    <div class="span-2 last buttonPane">
        <br />
        <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
        <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
        <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
        <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
    </div>
    <div class="span-4 last line" id="dealersFromContainer">
        <div align="center">Existentes</div>
        <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last"></select>
    </div>
</div>

<div class="prepend-5 span-10 append-4 last line">
    <center>* Todas los comercializadores en el lado izquierdo deben estar seleccionados
    para poder a&ntilde;adirles a la promoci&oacute;n.</center>
</div>

{literal}
<script language="javascript">
$("#selDealer").bind("change",function(){//loadBranches(this.value)});
/*LIST SELECTOR*/
$("#moveAll").click( function () {
	$("#dealersFrom option").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeAll").click( function () {
	$("#dealersTo option").each(function (i) {
			if($(this).attr('class') == $('#selDealer').val()) {
				$(this).remove();
		}
	});
	loadBranches($('#selDealer').val());	
});

$("#moveSelected").click( function () {
	$("#dealersFrom option:selected").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeSelected").click( function () {				  
	$("#dealersTo option:selected").each(function (i) {
			if($(this).attr('class') == $('#selDealer').val()) {
				$(this).remove();
		}
	});
	//loadBranches($('#selDealer').val());
});
/*END LIST SELECTOR*/
/*function loadBranches(deaID) {
	var selBranches="0";
	for(var i=0; i < $("#dealersTo *").length; i++) {
		if(i==0) {
			selBranches+=$("#dealersTo").children()[i].value;
		}
		else{
			selBranches+=","+$("#dealersTo").children()[i].value;
		}
	}
	$("#dealersFromContainer").load(document_root+"rpc/loadBranchByDealer.rpc",{serial_dea: deaID, selBranches: selBranches});
}*/
</script>
{/literal}