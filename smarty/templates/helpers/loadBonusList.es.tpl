{if $bonusList}
<center>
    <div class="span-19 last line">
        <table border="1" class="dataTable" id="theTable">
            <THEAD>
                <tr class="header">
                    <th align="center" style="text-align: center;">
                        <input type="checkbox" name="selAll" id="selAll" value="{$bl.serial_dbb}" />
                    </th>
                    {foreach from=$titles item="t"}
                    <th align="center" style="text-align: center;">
                        {$t}
                    </th>
                    {/foreach}
                </tr>
            </THEAD>
            <TBODY>

				{foreach name="bonusList" from=$bonusList item="bl"}
				<tr  {if $smarty.foreach.bonusList.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
					<td class="tableField">
						<input type="checkbox" id="chk{$bl.serial_bon}" name="serial" value="{$bl.serial_bon}" />
					</td>

					<td class="tableField">
						{$bl.name_cou}
					</td>
					<td class="tableField">
						{$bl.name_cit}
					</td>
					<td class="tableField">
						{$bl.name_dea}
					</td>
					<td class="tableField">
						{$bl.name_pbl}
					</td>
					<td class="tableField">
						{$bl.percentage_bon}%
					</td>

					<td class="tableField">
						<img src="{$document_root}img/pencil.png" border="0" id="update{$bl.serial_bon}" value="{$bl.serial_bon}" dealer="{$bl.name_dea}" product="{$bl.name_pbl}" percentage="{$bl.percentage_bon}"  class="image" width="25px" height="25px" style="cursor:pointer" />
					</td>
					<td class="tableField">
						<img src="{$document_root}img/inactive.gif" border="0" id="deactivate{$bl.serial_bon}" value="{$bl.serial_bon}"  class="delete" width="25px" height="25px" style="cursor:pointer" />
					</td>
				</tr>
				{/foreach}

            </TBODY>
        </table>
		<input type="hidden" name="hdnchecks" id="hdnchecks" title="Seleccione al menos un registro." value="" />
    </div>
</center>

<div class="span-19 last line">
    <div class="span-19 label">Porcentaje:
		<input type="text" name="txtNewPercentage" id="txtNewPercentage" title='El campo "Porcentaje" es obligatorio.' />
		<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
		<input type="submit" value="Actualizar" name="btnUpdate" id="btnUpdate" />
    </div>
</div>

{else}
{if $serial_dea}<div class="prepend-5 last line">El comercializador seleccionado no tiene incentivos asignados.</div>{/if}
{/if}