<div class="span-24 last line separator">
	<label>Informaci&oacute;n de la Venta</label>
</div>

{if $sale_data}
	{if $sale_data.id_erp_sal}
		<div class="span-10 append-7 prepend-7 last">
			<div class="span-10 error center">
				La venta ya consta en el ERP. Consulte con el administrador.
			</div>
		</div>
	{else}
		<div class="span-12 append-6 prepend-6 last">
			<div class="span-12 last line">
				<table border="0" id="travelsTable" style="color: #000000;">
					<THEAD>
					<tr bgcolor="#284787">
						<td align="center" class="tableTitle">
							# de Tarjeta
						</td>
						<td align="center"  class="tableTitle">
							Tipo
						</td>
						<td align="center"  class="tableTitle">
							Cliente
						</td>
					</tr>
					</THEAD>

					<TBODY>
						<tr bgcolor="#e8f2fb" >
							<td align="center" class="tableField">
								{if $sale_data.card_number_sal}
									{$sale_data.card_number_sal}
								{else}
									N/A
								{/if}
							</td>
							<td align="center" class="tableField">
								{$sale_data.product|htmlall}
							</td>
							<td align="center">
								{$sale_data.customer|htmlall}
							</td>
						</tr>
					</TBODY>
				</table>
			</div>

			<div class="span-10 center">
				<input type="submit" id="btnSubmit" name="btnSubmit" value="Migrar a ERP" />
				<input type="hidden" id="hdnSerialSal" name="hdnSerialSal" value="{$sale_data.serial_sal}" />
			</div>
		</div>
	{/if}
{else}
	<div class="span-10 append-7 prepend-7 last">
		<div class="span-10 error center">
			La tarjeta ingresada no existe en el sistema.
		</div>
	</div>
{/if}