{if $providerList}
    <select name="selProvider" id="selProvider" title='El campo "Proveedor" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach from=$providerList item="l"}
        	<option value="{$l.serial_spv}" >{$l.name_spv}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
		No existen proveedores registrados para este pa&iacute;s.
    {/if}
    <select name="selProvider" id="selProvider" title='El campo "Proveedor" es obligatorio' {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}