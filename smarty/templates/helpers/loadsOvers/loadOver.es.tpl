{if $overList}
    <select name="selOver" id="selOver" title='El campo "Over" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="overs" from=$overList item="l"}
		<option value="{$l.serial_ove}" name="{$l.name_ove}"> {$l.name_ove} </option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
		No existen overs activos para ese pais.
    {/if}
    
{/if}
