<div class="span-24 last line">
	<div class="prepend-8 span-4  label">* Pa&iacute;s: </div>
    <div class="span-8" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$loadedCountries item=cou}
            	<option value="{$cou.serial_cou}" {if $cou.serial_cou eq $myCountry}selected="selected"{/if}>{$cou.name_cou|htmlall}</option>
            {/foreach}
        </select>
    </div>
</div>
<div class="span-24 last line">
 <div class="prepend-8 span-4  label"> Representante: </div>
	    <div class="span-8 last" id="managerContainer">
	        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
	            <option value="">- Seleccione -</option>
	        </select>
	    </div>
</div>
{if $appliedTo eq 'DEALER'}

<div class="span-24 last line">
	    <div class="prepend-8 span-4  label">Responsable </div>
	    <div class="span-8 last" id="responsibleContainer">
	        <select name="selResponsable" id="selResponsable">
			  <option value="">- Seleccione -</option>
			</select>
	    </div>
</div>
<div class="span-24 label">&nbsp;</div>
<div class="prepend-8 span-6 append-8 label line">* Seleccione las sucursales:</div>
<div class="span-24 last line" id="branchContainer">
    <div class="span-11">
        <div align="center">Seleccionadas</div>
        <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-11 last" title="El campo de responsables es obligatorio.">
        {if $myDealers}
	        {foreach from=$myDealers item="dea"}
				<option value="{$dea.serial_dea}"  managerByCountry="{$dea.serial_mbc}" responsible="{$dea.serial_usr}" selected="selected">{$dea.code_dea} - {$dea.name_dea}</option>
			{/foreach}
        {/if}
        </select>
    </div>
    <div class="span-2 last buttonPane">
        <br />
        <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
        <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
        <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
        <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
    </div>
    <div class="span-11 last line" id="dealersFromContainer">
        <div align="center">Existentes</div>
        <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-11 last"></select>
    </div>
</div>

<div class="prepend-7 span-10 append-4 last line">
    <center>* Todas las sucursales en el lado izquierdo deben estar seleccionadas
    para poder a&ntilde;adirles a la promoci&oacute;n.</center>
</div>
{/if}


{literal}
<script language="javascript">
/*LIST SELECTOR*/
$("#moveAll").click( function () {
	$("#dealersFrom option").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" responsible="'+$(this).attr('responsible')+'" managerbycountry="'+$(this).attr('managerbycountry')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeAll").click( function () {
	$("#dealersTo option").each(function (i) {
           if($('#selResponsable').val()!='' && $('#selManager').val()!=''){
			if($(this).attr('responsible')==$('#selResponsable').val() && $(this).attr('managerbycountry')==$('#selManager').val()){
				$("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" responsible="'+$(this).attr('responsible')+'" managerbycountry="'+$(this).attr('managerbycountry')+'" selected="selected">'+$(this).text()+'</option>');
			}
		}else{
			if($('#selManager').val()!='' && $('#selResponsable').val()=='' ){
				if($(this).attr('managerbycountry')==$('#selManager').val()){
					$("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" responsible="'+$(this).attr('responsible')+'" managerbycountry="'+$(this).attr('managerbycountry')+'" selected="selected">'+$(this).text()+'</option>');
				}
			}
		}
		$(this).remove();
	});
});

$("#moveSelected").click( function () {
	$("#dealersFrom option:selected").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" responsible="'+$(this).attr('responsible')+'" managerbycountry="'+$(this).attr('managerbycountry')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeSelected").click( function () {
	$("#dealersTo option:selected").each(function (i) {
		if($('#selResponsable').val()!='' && $('#selManager').val()!=''){
			if($(this).attr('responsible')==$('#selResponsable').val() && $(this).attr('managerbycountry')==$('#selManager').val()){
				$("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" responsible="'+$(this).attr('responsible')+'" managerbycountry="'+$(this).attr('managerbycountry')+'" selected="selected">'+$(this).text()+'</option>');
			}
		}else{
			if($('#selManager').val()!='' && $('#selResponsable').val()=='' ){
				if($(this).attr('managerbycountry')==$('#selManager').val()){
					$("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" responsible="'+$(this).attr('responsible')+'" managerbycountry="'+$(this).attr('managerbycountry')+'" selected="selected">'+$(this).text()+'</option>');
				}
			}
		}
		$(this).remove();
	});
});
/*END LIST SELECTOR*/
</script>
{/literal}
<script language="javascript">
	//THIS CALL MUST BE OUTSIDE OF THE IF BELOW
	loadCountry('{$myCountry}');
</script>
{if $myCountry}
	<script language="javascript">
		loadManagerData('{$myCountry}','{$manManager}');
	</script>
{/if}