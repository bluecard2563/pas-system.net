<div class="span-24 last title">
   	OVERS A PAGAR
</div>
{if $oversToPay}
	<div class="span-24 last line">
	   	<div class="span-1 pushLeft">No.</div>
	   	<div class="span-1 pushLeft">Sel</div>
	   	<div class="span-7 pushLeft">Nombre</div>
		<div class="span-5 pushLeft">Per&iacute;odo</div>
	    <div class="span-3 pushCenter">Crecimiento Propuesto</div>
		<div class="span-3 pushCenter">Crecimiento Realizado</div>
	    <div class="span-3 pushCenter">Over A Pagar</div>
	</div>
	<hr />
	<div id="all-overs-list">
		{foreach from=$oversToPay item="ove" key=cid}
			<div class="span-24 last line table_record" id="banner_{$ove.serial_apo}">
				<div class="span-1 list_text" id="num_{$ove.serial_apo}">{$cid+1}</div>
				<div class="span-1 list_text" id="chk_{$ove.serial_apo}"><input type=checkbox name="overPay_{$ove.serial_apo}_{$ove.serial_ove}" id="overPay_{$ove.serial_apo}"></div>
				<div class="span-7 list_text" id="name_{$ove.serial_apo}">{$ove.name_app}</div>
		    	<div class="span-5 list_text" id="per_{$ove.serial_apo}">{$ove.begin_date_ino}  al  {$ove.end_date_ino}</div>
				<div class="span-3 list_text" align="center" id="inc_{$ove.serial_apo}">{$ove.percentage_increase_ove}</div>
				<div class="span-3 list_text" align="center" id="inc_{$ove.serial_apo}">{$ove.percentage_ino}</div>
	        	<div class="span-3 last list_text" align="center" id="pay_{$ove.serial_apo}">{$ove.amount_ino}</div>
			</div>
		{/foreach}
	</div>
	<div class="span-2 last title">
    	<input type="submit" name="btnAuthorize" id="btnAuthorize" value="Autorizar e Imprimr" >
    </div>
{else}
	<div class="span-18 prepend-6 last line pushLeft">
	   	NO SE PRODUJERON RESULTADOS, POR FAVOR VAR&Iacute;E LOS PAR&Aacute;METROS DEL FILTRO.
	</div>
{/if}