
{if $invoicesList}
    <div class="span-24 last title">
            Lista de Cuentas por Cobrar<br />
    </div>
<div class="span-24 last">
    <table border="1" class="dataTable" id="invoicesTable">
        <THEAD>
            <tr class="header">
                <th align="center">
                    <input type="checkbox" id="selAll" name="selAll">
                </th>
                <th align="center">
                    Fecha de Factura
                </th>
                <th align="center">
                    # de Factura
                </th>
                <th align="center">
                    Facturado a
                </th>
                <th align="center">
                    Impago
                </th>
                <th align="center">
                    Hasta 30
                </th>
                <th align="center">
                    Hasta 60
                </th>
                <th align="center">
                    Hasta 90
                </th>
                <th align="center">
                    M&aacute;s de 90
                </th>
            </tr>
        </THEAD>
        <TBODY>
              {foreach name="invoices" from=$invoicesList item="i"}
                <tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if} >
                    <td align="center" class="tableField">
                        <input type="checkbox" value="{$i.serial_inv}" name="chk" id="{$i.serial_inv}" class="invoiceChk" payment="{$i.serial_pay}" />
                    </td>
                    <td align="center" class="tableField">
                       {$i.date_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.number_inv}               
                    </td>
                    <td align="center" class="tableField">
                        {$i.invoice_to|htmlall}
                    </td>
                    <td align="center" class="tableField">
                        {$i.days}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days lt 31}
                            {if $i.status_inv eq 'STAND-BY'}
                                {$i.total_to_pay}
                            {else}
                                {$i.total_to_pay_fee}
                            {/if}
                        {/if}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days gt 30 && $i.days lt 61}
                            {if $i.status_inv eq 'STAND-BY'}
                                {$i.total_to_pay}
                            {else}
                                {$i.total_to_pay_fee}
                            {/if}
                        {/if}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days gt 60 && $i.days lt 91}
                            {if $i.status_inv eq 'STAND-BY'}
                                {$i.total_to_pay}
                            {else}
                                {$i.total_to_pay_fee}
                            {/if}
                        {/if}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days gt 90}
                            {if $i.status_inv eq 'STAND-BY'}
                                {$i.total_to_pay}
                            {else}
                                {$i.total_to_pay_fee}
                            {/if}
                        {/if}
                    </td>
                    <input type="hidden" class="hdnInvoiceToSerial" name="hdnInvoiceToSerial" id="{if $i.serial_dea}dealer_{$i.serial_dea}{else}customer_{$i.serial_cus}{/if}" value="{if $i.serial_dea}{$i.serial_dea}{else}{$i.serial_cus}{/if}" />
                    <input type="hidden" class="hdnInvoiceTo" name="hdnInvoiceTo" value="{if $i.serial_dea}dea{else}cus{/if}" />
                    <input type="hidden" class="hdnSerialInv" name="hdnSerialInv" value="{$i.serial_inv}" />
                </tr>
              {/foreach}
        </TBODY>
    </table>
</div>
<div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>
<input type="hidden" name="selectedInvoices" id="selectedInvoices" title="Debe seleccionar una venta." />
<input type="hidden" name="payedSelected" id="payedSelected" value="0" />
<input type="hidden" name="total_pages" id="total_pages" value="{$total_pages}" />

<div class="prepend-10 span-4 line">
    <table border="0" id="invoicesTable">
        <THEAD>
            <tr bgcolor="#284787">
                <td align="center" class="tableTitle">
                    D&iacute;as
                </td>
                <td align="center"  class="tableTitle">
                    Total
                </td>
            </tr>
        </THEAD>
        <TBODY>
            <tr bgcolor="#c5dee7">
                <td align="center" class="tableField">30</td>
                <td align="center" class="tableFieldNumber">{$total_days.total_30}</td>
            </tr>
            <tr bgcolor="#e8f2fb">
                <td align="center" class="tableField">60</td>
                <td align="center" class="tableFieldNumber">{$total_days.total_60}</td>
            </tr>
            <tr bgcolor="#c5dee7">
                <td align="center" class="tableField">90</td>
                <td align="center" class="tableFieldNumber">{$total_days.total_90}</td>
            </tr>
            <tr bgcolor="#e8f2fb">
                <td align="center" class="tableField">M&aacute;s</td>
                <td align="center" class="tableFieldNumber">{$total_days.total_more}</td>
            </tr>
            <tr bgcolor="#284787">
                <th align="center" class="tableTitle">Total</th>
                <th align="center" class="tableTitle">{$total_days.total}</th>
            </tr>
        </TBODY>
    </table>
</div>
<div class="span-24 last center line">
	<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
	<input type="hidden" id="hdnInvoiceToSerialAux" name="hdnInvoiceToSerialAux"/>
	<input type="hidden" id="hdnInvoiceToAux" name="hdnInvoiceToAux"/>
	<input type="hidden" id="hdnSerialInvAux" name="hdnSerialInvAux"/>
    <input type="submit" value="Pagar" class="ui-state-default ui-corner-all" />
	<input type="button" value="Imprimir" onclick="printInvoices('{$serial_dea}');" class="ui-state-default ui-corner-all" />
</div>
<div class="span-24 last">
            <br/>* (A): Abono{*, (CI): Costo Internacional*}<br />
</div>
{else}
<div class="span-10 append-5 prepend-7"><b>No existen cuentas por cobrar para esta Sucursal de Comercializador.</b></div>
{/if}
