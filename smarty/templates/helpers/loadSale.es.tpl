{if $serial_sal}
<div class="span-24 last line separator">
   <label>Datos del Registro de Viaje</label>
</div>

<div class="prepend-3 span-18 append-3 last line" >
	<div class="span-3" >
		<label>N&uacute;mero de Tarjeta</label>
	</div>
	<div class="span-3" >
		{$cardNumber_sal}
	</div>
	<div class="span-3" >
		<label>Total de la Tarjeta</label>
	</div>
	<div class="span-3 last" >
		{$currency.symbol_cur} {$total_sal}
	</div>
	<div class="span-3" >
		<label>Producto</label>
	</div>
	<div class="span-3 last" >
		{$name_pbl|htmlall}
	</div>
</div>

<div class="prepend-3 span-18 append-3 last line" >
	<div class="span-3" >
		<label>Cliente CI:</label>
	</div>
	<div class="span-3" >
		{$document_cus}
	</div>
	<div class="span-3" >
		<label>Nombres:</label>
	</div>
	<div class="span-3 last" >
		{$firstname_cus|htmlall}
	</div>
	<div class="span-3" >
		<label>Apellidos:</label>
	</div>
	<div class="span-3 last" >
		{$lastname_cus|htmlall}
	</div>
</div>

<div class="prepend-3 span-18 append-3 last line" >
	<div class="span-3" >
		<label>Fecha de Emisi&oacute;n</label>
	</div>
	<div class="span-3" >
		{$emissionDate_sal}
	</div>
	<div class="span-3" >
		<label>Fecha de Salida</label>
	</div>
	<div class="span-3 last" >
		{$beginDate_sal}
	</div>
	<div class="span-3" >
		<label>Fecha de Retorno</label>
	</div>
	<div class="span-3 last" >
		{$endDate_sal}
	</div>
</div>
{/if}