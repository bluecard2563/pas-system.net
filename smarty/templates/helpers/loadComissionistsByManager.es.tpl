{if $comissionistList}
    <select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="comissionist" from=$comissionistList item="l"}
        	<option value="{$l.serial_usr}" {if $smarty.foreach.comissionist.total eq 1}selected{/if}>{$l.name_usr}</option>
        {/foreach}
    </select>
{else}
	{if $serial_mbc}
		No existen Responsables registrados.
	{else}
	<select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio'>
        <option value="">- Seleccione -</option>
    </select>
	{/if}
{/if}