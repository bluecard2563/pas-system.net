{if $customerList}
<div class="span-24 last">
	<table border="0" id="customersTable" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<th style="text-align: center;">
					No.
				</th>
				<th style="text-align: center;">
					Documento
				</th>
				<th style="text-align: center;">
					Nombre Completo
				</th>
				<th style="text-align: center;">
					Fecha de Nacimiento
				</th>
				<th style="text-align: center;">
					Tel&eacute;fono
				</th>
				<th style="text-align: center;">
					E-mail
				</th>
				<th style="text-align: center;">
					C&oacute;digo ERP
				</th>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="customer" from=$customerList item="s"}
			<tr {if $smarty.foreach.customer.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.customer.iteration}
				</td>
				<td align="center" class="tableField">
					{$s.document_cus}
				</td>
				<td align="center" class="tableField">
					{$s.first_name_cus|htmlall} {$s.last_name_cus|htmlall}
				</td>
				<td align="center" class="tableField">
					{$s.birthdate_cus}
				</td>
				<td align="center" class="tableField">
					{if $s.phone1_cus neq ''}{$s.phone1_cus}{else}N/A{/if}
				</td>
				<td align="center" class="tableField">
					{if $s.email_cus neq ''}{$s.email_cus}{else}N/A{/if}
				</td>
				<td align="center" class="tableField">
					{$s.oo_code}
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last line buttons" id="pageNavPosition"></div>
{else}
<div class="prepend-7 append-7 span-10 last">
	<div class="span-10 error">
		No existen clientes con el patr&oacute;n de b&uacute;squeda ingresado.
	</div>
</div>
{/if}