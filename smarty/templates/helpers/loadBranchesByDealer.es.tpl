{if $branchList}
    <select name="selBranch" id="selBranch" >
        <option value="">{if $reports eq 'true'}- Todos -{else}- Seleccione -{/if}</option>
        {foreach from=$branchList item="b"}
		<option value="{$b.serial_dea}" >{$b.name_dea|htmlall} - {$b.code_dea}</option>
        {/foreach}
    </select>
{else}
    <select name="selBranch" id="selBranch" >
      <option value="">{if $reports eq 'true'}- Todos -{else}- Seleccione -{/if}</option>
    </select>
{/if}