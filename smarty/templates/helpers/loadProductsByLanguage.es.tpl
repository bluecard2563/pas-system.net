{if $productList}
<table border="0" id="product_table">
	<thead>
		<tr bgcolor="#284787">
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				#
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Producto
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Editar
			</td>
		</tr>
	</thead>

	<tbody>
	{foreach name="productList" from=$productList item="l"}
	<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.productList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
		<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
			{$smarty.foreach.productList.iteration}
		</td>
		<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
			{$l.name_pbl}
		</td>
		<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
			<a id="btnUpdate{$l.serial_pro}" serial_pro="{$l.serial_pro}" style="cursor: pointer;">Editar</a>
		</td>
	</tr>
	{/foreach}
	</tbody>
</table>
{else}
	<center>No hay Productos registrados para el Idioma seleccionado.</center>
{/if}
<div class="span-12 last line pageNavPosition" id="pageNavPosition"></div>