{if $managerList}
	<select name="selManager{$serial_sel}" id="selManager{$serial_sel}" title='El campo "Representante" es obligatorio.'>
		{if $notRequired}
			<option value="">- Todos -</option>
		{else}
			<option value="">- {if $opt_text}{$opt_text}{else}Todos{/if} -</option>
		{/if}
  		{foreach from=$managerList item="l" name="managers"}
  			<option value="{if $manager_only eq 'YES'}{$l.serial_man}{else}{$l.serial_mbc}{/if}" official="{$l.official_mbc}" {if $user neq 1 && $smarty.foreach.managers.total eq 1}selected{/if} >{$l.name_man|htmlall}</option>
  		{/foreach}
	</select>
	{if $manManager}
    	<script language="javascript">
    		loadManagerSelect('{$manManager}');
   		</script>
    {/if}
{else}
	{if $serial_cou neq ''}
	No existen representantes registrados.
    {/if}
    <select name="selManager{$serial_sel}" id="selManager{$serial_sel}" title='El campo "Representante" es obligatorio.' {if $serial_cou neq ''}style="display:none;"{/if}>
            <option value="" selected>- {if $opt_text}{$opt_text}{else}Todos{/if} -</option>
    </select>
{/if}