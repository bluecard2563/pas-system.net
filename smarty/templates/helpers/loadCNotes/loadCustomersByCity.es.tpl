{if $noData neq 1}
    {if $customerList}
    <div class="prepend-4 span-4 label">*Cliente:</div>
    <div class="span-5 append-11 last line">
        <select name="selCustomer" id="selCustomer" title='El campo "Cliente" es obligatorio.' >
            <option value="">- Seleccione -</option>
            {foreach from=$customerList item="l"}
                <option value="{$l.serial_cus}">{$l.complete_name}</option>
            {/foreach}
        </select>
    </div>
    {else}
    <div class="prepend-8 span-9 append-4 last">No existen Clientes en el ciudad seleccionada.</div>
    {/if}
{else}
	<div class="prepend-4 span-4 label">*Cliente:</div>
    <div class="span-5 append-11 last line">
        <select name="selCustomer" id="selCustomer" title='El campo "Cliente" es obligatorio.' >
            <option value="">- Seleccione -</option>
        </select>
    </div>
{/if}