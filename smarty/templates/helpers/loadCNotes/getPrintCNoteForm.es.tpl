<div class="span-24 last">
	{if $cNote}
	
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Monto de la Nota de Cr&eacute;dito:</div>
			<div class="span-5">
				{$cNote.amount_cn}
			</div>

			<div class="span-3 label">* Fecha de Generaci&oacute;n:</div>
			<div class="span-4 append-4 last" id="cityContainer">
				{$cNote.date_cn}
			</div>
			<input type="hidden" name="typeCNote" id="typeCNote" value="{$type}"/>
			<input type="hidden" name="serial_cn" id="serial_cn" value="{$cNote.serial_cn}"/>
			<input type="hidden" name="type_dbc" id="type_dbc" value=""/>
			<input type="hidden" name="serial_dbc" id="serial_dbc" value=""/>
			<input type="hidden" name="serial_cou" id="serial_cou" value=""/>
		</div>

		<div class="span-24 last line buttons">
			<input type="submit" name="btnSubmit" id="btnSubmit" value="Imprimir Nota de Cr&eacute;dito" />
		</div>
	{else}
	<div class="prepend-7 span-10 append-7 last">
		<div class="span-10 error">
			{if $error eq 1}
				Se han encontrado m&aacute;s de una nota de cr&eacute;dito con esa numeraci&oacute;n. Por favor comun&iacute;quese con el adminsitrador.
			{elseif $error eq 2}
				No existe una Nota de Cr&eacute;dito con esa numeraci&oacute;n de acuerdo a los par&aacute;metros ingresados.
			{elseif $error eq 3}
				No se ha podido cargar la informaci&oacute;n de la nota de cr&eacute;dito. Por favor contacte al administrador.
			{/if}
		</div>
	</div>
	{/if}
</div>