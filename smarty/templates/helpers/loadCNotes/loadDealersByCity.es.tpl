{if $noData neq 1}
    {if $dealerList}
    <div class="prepend-4 span-4 label">*Comercializador:</div>
    <div class="span-5 append-11 last line">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
            <option value="">- Seleccione -</option>
            {foreach from=$dealerList item="l"}
                <option value="{$l.serial_dea}">{$l.name_dea}</option>
            {/foreach}
        </select>
    </div>
    {else}
    <div class="prepend-8 span-9 append-4 last">No existen Comercializadores en el ciudad seleccionada.</div>
    {/if}
{else}
	<div class="prepend-4 span-4 label">*Comercializador:</div>
    <div class="span-5 append-11 last line">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
            <option value="">- Seleccione -</option>
        </select>
    </div>
{/if}