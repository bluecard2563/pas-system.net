
{if $creditNoteList AND $exist eq 1}
<br/>
    <div class="span-24 last title">
        Lista de notas de cr&eacute;dito Elegibles<br />
    </div>
       <table border="0" id="creditNotesTable">
                <THEAD>
                <tr bgcolor="#284787">
                    <td align="center" class="tableTitle">
                        <input type="checkbox" id="selAll" name="selAll">
                    </td>
                    <td align="center" class="tableTitle">
                        # de Nota de Cr&eacute;dito
                    </td>
                    <td align="center"  class="tableTitle">
                        Factura a nombre de
                    </td>
                    <td align="center" class="tableTitle">
                        Facturado a
                    </td>
                    <td align="center" class="tableTitle">
                        Fecha Emisi&oacute;n
                    </td>
                    <td align="center" class="tableTitle">
                        Total
                    </td>
                </tr>
                </THEAD>
                <TBODY>
                  {foreach name="cNotes" from=$creditNoteList item="c"}
                  {if $c.name_cus OR $c.name_dea}
                    <tr {if $smarty.foreach.cNotes.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                        <td align="center" class="tableField">
                            <input type="checkbox" value="{$c.serial_cn}" name="chkCreditNotes[]" class="creditNotesChk" taxes="{$c.taxes}"/>
                        </td>
                        <td align="center" class="tableField">
                           {if $c.number_cn}{$c.number_cn}{else}N/A{/if}
                        </td>
                        <td align="center" class="tableField">
                            {if $c.name_cus}Cliente{else}Comercializador{/if}
                        </td>
                        <td align="center" class="tableField">
                            {if $c.name_cus}{$c.name_cus}{else}{$c.name_dea}{/if}
                        </td>
                        <td align="center" class="tableField">
                            {$c.date_cn}
                        </td>
                        <td align="center" class="tableField">
                            {$c.amount_cn}
                        </td>
                    </tr>
                    {/if}
                  {/foreach}
                </TBODY>
       </table>
 <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
 <input type="hidden" name="selectedCreditNotes" id="selectedCreditNotes" title="Debe seleccionar al menos una nota de cr&eacute;dito.">
<div class="span-24 last center">
    <br/>
    <input type="submit" value="Despachar">
</div>
{else}
<div class="span-10 append-5 prepend-7 label" style="text-align: center;">No existen notas de cr&eacute;dito para el {if $cnfor eq 'cus'}cliente{else}comercializador{/if} seleccionado.</div>
{/if}