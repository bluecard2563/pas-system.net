{if $branchList}
<div align="center">Existentes</div>
<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" >
{foreach from=$branchList item="l"}
    <option value="{$l.serial_dea}" class="{$serial_dea}">{$l.name_dea} - {$l.branch_number_dea}</option>
{/foreach}
</select>
{elseif not $serial_dea}
<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last"></select>
{else}
No existen m&aacute;s sucursales disponibles para el comercializador seleccionado.
{/if}