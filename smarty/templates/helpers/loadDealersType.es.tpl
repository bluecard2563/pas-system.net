
{if $dataByLanguage}
	<select name="selType" id="selType" class="span-5">
	  <option value="">- Todos -</option>
	  {foreach from=$dataByLanguage item="l"}
		<option value="{$l.serial_dlt}" {if $dataByLanguage|@count eq 1} selected{/if}>{$l.description_dtl|htmlall}</option>
	  {/foreach}
	</select>
{else}
	No existen Tipos de comercializador disponibles.
	<select name="selType" id="selType" style="display: none;">
	  <option value="">- TODOS -</option>
	</select>
{/if}