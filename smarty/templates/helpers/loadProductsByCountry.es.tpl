{if $products}
<center>
    <div class="span-14 last line">
        <table border="0" class="paginate-1 max-pages-7" id="table">
            <thead>
                <tr bgcolor="#284787">
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        <input type="checkbox" id="selAll" />
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        Nombre
                    </td>
                </tr>
            </thead>

            <tbody>
            {foreach name="productList" from=$products item="p"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.productList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					<input type="checkbox" name="chkProduct[]" id="chkProduct{$p.serial_pxc}" value="{$p.serial_pxc}" />
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$p.name_pbl}
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>

     <div class="span-14 last line pageNavPosition" id="pageNavPosition"></div>

	 <div class="span-14 last buttons line">
		 <input type="submit" id="btnSubmit" value="Asignar Productos" />
	 </div>
</center>
{else}
	{if $serial_cou neq ""}
	<center>No existen productos asignados a este pa&iacute;s.</center>
	{/if}
{/if}

{literal}
<script type="text/javascript">
if($("#table").length>0) {
	var pager = new Pager('table', 8, 'Siguiente', 'Anterior');
	pager.init(7); //Max Pages
	pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
	pager.showPage(1); //Starting page
}

$('#selAll').bind("click",function(){
	if($('#selAll').attr("checked")) {
		$("input[id^='chkProduct']").attr("checked", true);
	} else {
		$("input[id^='chkProduct']").attr("checked", false);
	}
});

$("input[id^='chkProduct']").bind("click",function(){
	if(!$(this).attr("checked")) {
		$('#selAll').attr("checked", false);
	}
});
</script>
{/literal}