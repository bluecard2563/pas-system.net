{if $userList}
<select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio.' >
    <option value="">- Seleccione -</option>
    {foreach from=$userList item="l"}
        <option id="{$l.serial_usr}" value="{$l.serial_usr}" isComissionist="{$l.is_comissionist}">{$l.first_name_usr} {$l.last_name_usr}</option>
    {/foreach}
</select>
{else}
	{if $serial_cou neq ''}
		No existen Usuarios de tipo "Representante" en el pa&iacute;s seleccionado.
    {/if}
    <select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio.' {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}