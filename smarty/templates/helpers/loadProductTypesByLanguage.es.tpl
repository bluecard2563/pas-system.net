{if $productTypeList}
<table border="0" id="product_table">
	<thead>
		<tr bgcolor="#284787">
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				#
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Tipo de Producto
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Editar
			</td>
		</tr>
	</thead>

	<tbody>
	{foreach name="productTypeList" from=$productTypeList item="l"}
	<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.productTypeList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
		<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
			{$smarty.foreach.productTypeList.iteration}
		</td>
		<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
			{$l.name_ptl|htmlall}
		</td>
		<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
			<a id="btnUpdate{$l.serial_tpp}" serial_tpp="{$l.serial_tpp}" style="cursor: pointer;">Editar</a>
		</td>
	</tr>
	{/foreach}
	</tbody>
</table>
{else}
	<center>No hay Tipos Productos registrados para el Idioma seleccionado.</center>
{/if}
<div class="span-12 last line pageNavPosition" id="pageNavPosition"></div>