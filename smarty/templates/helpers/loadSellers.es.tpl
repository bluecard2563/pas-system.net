    {if $sellerList}
    <select name="selSeller" id="selSeller" title='El campo "Usuario" es obligatorio'>
      <option value="">- Seleccione -</option>
      {foreach from=$sellerList item="l"}
            <option value="{$l.serial_cnt}" >{$l.name|htmlall}</option>
      {/foreach}
    </select>
    {else}
       No existen Counters En la Sucursal de Comercializador.
    {/if}
