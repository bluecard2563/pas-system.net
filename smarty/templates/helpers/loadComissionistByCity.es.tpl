{if $comissionistList}
    <select name="selComissionist{$serial_sel}" id="selComissionist{$serial_sel}" title='El campo "Responsable" es obligatorio'>
        <option value="">- {if $text}{$text}{else}Seleccione{/if} -</option>
        {foreach name="comissionist" from=$comissionistList item="l"}
        	<option value="{$l.serial_usr}" {if $smarty.foreach.comissionist.total eq 1 AND $text neq 'Todos'}selected{/if}>{$l.name_usr}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cit neq ''}
		No existen Responsables registrados.
    {/if}
    <select name="selComissionist{$serial_sel}" id="selComissionist{$serial_sel}" title='El campo "Responsable" es obligatorio' {if $serial_cit neq ''}style="display:none;"{/if}>
      <option value="">- {if $text}{$text}{else}Seleccione{/if} -</option>
    </select>
{/if}
