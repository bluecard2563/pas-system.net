{if $files}
<center>
    <div class="prepend-1 span-22 last line">
        <table border="0" class="paginate-1 max-pages-7" id="files_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="t"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                        {$t}
                    </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="fileList" from=$files item="fl"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.fileList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white; padding:5px 5px 10px 5px; text-align:center;">
                    <input type="radio" name="rdoFiles" id="rdoFiles{$fl.serial_fle}" value="{$fl.serial_fle}"/>
                </td>
                 <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$fl.serial_fle}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$fl.name_cou}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$fl.name_cit}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$fl.name_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$fl.diagnosis_fle}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$fl.creation_date_fle}
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
	<input type="hidden" id="hdnFileCount" value="{$count}"/>
	<div class="prepend-1 span-22 last pageNavPosition" id="pageNavPositionFiles"></div>
</center>
{else}
<div class="span-24 last line center">
	<i>No existen reclamos por liquidar para esta tarjeta.</i>
</div>
{/if}