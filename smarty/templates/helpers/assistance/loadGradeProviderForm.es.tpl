
<div class="span-10 prepend-2 last">
	    <ul id="docGradeAlerts" class="alerts"></ul>
</div>
<form id="newRateServiceProvider" name="newRateServiceProvider">
	<div class="span-13 last line">
			<div class="span-5 label">
				Coordinador:
			</div>
			<div class="span-6">
				<select class="span-6" id="selServiceProvider" title="El campo 'Coordinador' es obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from=$fileComments item="fc"}
					<option value="{$fc.serial_spv}">{$fc.name_spv}</option>
					{/foreach}
				</select>
			</div>
	</div>
	<div id="serviceProviderGradeComment">
	</div>	
	<input type="hidden" id="serial_fle" name="serial_fle" value="{$serial_fle}"/>
</form>
