{if $benefitList}
<center>
	<input type="hidden" id="hdnSerial_dbf" name="hdnSerial_dbf" value="{$serial_dbf}" />
	<input type="hidden" id="hdnSerial_prq" name="hdnSerial_prq" value="{$serial_prq}" />

    <div class="span-17 last line">
		<div class="prepend-4 span-9 append-4 last line">
			<div class="span-9" id="benefitsMessage" style="display: none;"></div>
		</div>

        <table border="0" id="benefits_view_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="l"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        {$l}
                    </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="benefitList" from=$benefitList item="l"}
            <tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.benefitList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$l.description_bbl}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.price_bxp}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.restriction_price_bxp}
                </td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{if $edit}
					<input  type="text" id="amount_presented_{$l.serial_spf}" serial="{$l.serial_spf}" value="{$l.amount_presented_bbd}" style="width: 50px;" />
					<input  type="hidden" id="hdnPresented_{$l.serial_spf}" value="{$l.amount_presented_bbd}" />
					{else}
						${$l.amount_presented_bbd}
					{/if}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {if $edit}
						<input type="text" id="amount_authorized_{$l.serial_spf}" serial="{$l.serial_spf}" value="{$l.amount_authorized_bbd}" style="width: 50px;" />
						<input type="hidden" id="hdnAuthorized_{$l.serial_spf}" value="{$l.amount_authorized_bbd}" />
					{else}
						${$l.amount_authorized_bbd}
					{/if}
                </td>
            </tr>
            {/foreach}
            {foreach name="serviceList" from=$serviceList item="l"}
            <tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.serviceList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.name_sbl}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.coverage_sbc}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    -
                </td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {if $edit}
						<input  type="text" id="amount_presented_{$l.serial_spf}" serial="{$l.serial_spf}" value="{$l.amount_presented_bbd}" style="width: 50px;" />
					{else}
						${$l.amount_presented_bbd}
					{/if}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {if $edit}
						<input type="text" id="amount_authorized_{$l.serial_spf}" serial="{$l.serial_spf}" value="{$l.amount_authorized_bbd}" style="width: 50px;" />
					{else}
						${$l.amount_authorized_bbd}
					{/if}
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    <div class="span-17 last pageNavPosition" id="pageNavPositionView"></div>
	{if $edit}
	<div class="prepend-10 span-7 last line">
		<input type="button" id="saveAmountChanges" value="Guardar Cambios" />
		<div id="imgLoading" style="display: none;"><img src = "{$document_root}img/loading_small.gif" />Guardando Cambios</div>
	</div>
	{/if}
</center>
{else}
No existen beneficios disponibles para el coordinador seleccionado.
{/if}

{literal}
<script type="text/javascript">
    if($("#benefits_view_table").length>0) {
        pager = new Pager('benefits_view_table', 5 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPositionView'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	$('#saveAmountChanges').bind('click', function(){
		$('#saveAmountChanges').toggle();
		$('#imgLoading').toggle();
		var valid = true;
		var total_presented = 0;
		var total_authorized = 0;
		$("input[id^='amount_presented_']").each(function(){
			if(this.value != $('#hdnPresented_'+$(this).attr('serial')).val() || $('#amount_authorized_'+$(this).attr('serial')).val() != $('#hdnAuthorized_'+$(this).attr('serial')).val()) {
				$.getJSON(document_root+"rpc/assistance/updateBenefitByDocument.rpc", {serial_dbf: $('#hdnSerial_dbf').val(), serial_spf:$(this).attr('serial'), presented:this.value, authorized:$('#amount_authorized_'+$(this).attr('serial')).val()}, function(data){
					if(!data) {
						valid = false;
						return false;
					}
				});
			}
			total_presented += parseFloat(this.value);
			total_authorized += parseFloat($('#amount_authorized_'+$(this).attr('serial')).val());
		});
		if(valid) {
			$.getJSON(document_root+"rpc/assistance/updatePaymentRequest.rpc", {serial_prq: $('#hdnSerial_prq').val(), presented:total_presented, authorized:total_authorized}, function(data){
				if(data) {
					$('#reclaimedContainer_'+$('#hdnSerial_prq').val()).html(total_presented);
					$('#authorizedContainer_'+$('#hdnSerial_prq').val()).html(total_authorized);
					showMessage('Cambios guardados exitosamente.','success');
				} else {
					showMessage('Ocurrio un error al guardar los cambios.','error');
				}
			})
			
		} else {
			showMessage('Ocurrio un error al guardar los cambios.','error');
		}
	});

	function showMessage(text, type){
	$('#saveAmountChanges').toggle();
		$('#imgLoading').toggle();
		$('#benefitsMessage').html(text);
		$('#benefitsMessage').addClass(type);
		$('#benefitsMessage').css('display', 'block');
		setTimeout(function(){
			$('#benefitsMessage').hide("slide", {direction: "up"}, 500);
			$('#benefitsMessage').html('');
			$('#benefitsMessage').removeClass(type);
			;
		},2000);
	}

	$("input[id^='amount_presented_']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
    });

	$("input[id^='amount_authorized_']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
    });
</script>
{/literal}