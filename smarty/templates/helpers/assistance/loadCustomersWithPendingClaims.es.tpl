{if $customerList}
    <select name="selCustomer" id="selCustomer" title="El campo 'Cliente' es obligatorio">
        <option value="">- Seleccione -</option>
        {foreach name="customers" from=$customerList item="l"}
            <option value="{$l.serial_cus}" {if $customerList|@count eq 1}selected{/if}>{$l.name_cus} - {$l.document_cus}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cit neq ''}
        No existen clientes registrados.
    {/if}
    <select name="selCustomer" id="selCustomer" title="El campo 'Cliente' es obligatorio" {if $serial_cit neq ''}style="display:none;"{/if}>
        <option value="">- Seleccione -</option>
    </select>
{/if}