{if $files}
	<div class="span-24 last line" id="files">
		{foreach from=$files item=f}
		<h3><a href="#">Expediente #{$f.serial_fle}</a></h3>
		<div>

			<div class="span-23 last line">
				<div class="span-5 label">Nombre del beneficiario:</div>
				<div class="span-4 last">{$f.name_cus}</div>
			</div>

			<div class="prepend-1 span-23 last line separator">
				<label>Reclamaciones:</label>
			</div>

			<div class="prepend-1 span-22 last line">
				<table border="0" id="claimsTable">
					<thead>
						<tr bgcolor="#284787">
							{foreach name="titles_dialog" from=$titles item="t"}
								<td align="center" class="tableTitle">
									{$t}
								</td>
							{/foreach}
						</tr>
					</thead>
					<tbody>
					{if $f.fileList}
					{foreach name="files" from=$f.fileList item="fl"}
					<tr {if $smarty.foreach.files.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
						<td align="center" class="tableField">
							{$fl.name_dbf|htmlall}
						</td>
						<td align="center" class="tableField">
							{$fl.comment_dbf|htmlall}
						</td>
						<td align="center" class="tableField">
							{$fl.amount_reclaimed_prq}
						</td>
						<td align="center" class="tableField">
							<!--input type="text" id="authorized_{$fl.serial_bxp}_{$fl.serial_dbf}" value="{$fl.amount_authorized_prq}" /-->
							{$fl.amount_authorized_prq}
						</td>
						<td align="center" class="tableField">
							{$global_billTo[$fl.document_to_dbf]}
						</td>
						<td align="center" class="tableField">
							{$fl.date_dbf}
						</td>
						<td align="center" class="tableField" id="changeStatusPrqContainer{$fl.serial_prq}">
							<font color='{if $fl.status_prq eq "DENIED"}red{/if}'>
								{$global_paymentRequestStatus[$fl.status_prq]}
							</font>
						</td>
						<td align="center" class="tableField">
							<a id="viewDocument{$fl.serial_dbf}" style="cursor: pointer" serial="{$fl.serial_dbf}">Ver</a>
						</td>
						<td align="center" class="tableField">
							{$fl.observations_prq|htmlall}
						</td>
						<td align="center" class="tableField">
							{$fl.type_prq}
						</td>
						<td align="center" class="tableField">
							<a href="{$document_root}{$fl.url_dbf}" target="blank">Abrir</a>
						</td>
						<td align="center" class="tableField">
							<a id="deleteDocument{$fl.serial_dbf}" serial_dbf="{$fl.serial_dbf}" style="cursor: pointer;"><img src="{$document_root}img/remove-icon.png" /></a>
						</td>
					</tr>
					{/foreach}
					{/if}
					</tbody>
				</table>
			</div>

			<div class="prepend-1 span-22 last pageNavPosition" id="pageNavPosition"></div>

		</div>
		{/foreach}
	</div>
{else}
	<center>La tarjeta seleccionada no tiene expedientes abiertos con reclamaciones.</center>
{/if}

{literal}
<script type="text/javascript">
	var pager;
	if($("#claimsTable").length>0) {
		pager = new Pager('claimsTable', 10 , 'Siguiente', 'Anterior');
		pager.init(7); //Max Pages
		pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
		pager.showPage(1); //Starting page
	}

	$("a[id^='deleteDocument']").bind('click',function(){
		if(confirm("El documento se eliminara definitivamente del sistema.\n\u00bfDesea proceder?")){
			$("#deleting_file").dialog('open');
			$.getJSON(document_root+"rpc/assistance/deleteUploadedFile.rpc", {serial_dbf: $(this).attr('serial_dbf')}, function(data){
				$("#deleting_file").dialog('close');
				//hide_ajax_loader();
				if(data) {
					$("#fileContainer").load(document_root+"rpc/assistance/loadFilesLiquidation.rpc", {card_num:$("#txtCard").val()}, function(){
						$("#files").accordion({autoHeight: false, collapsible: true, active:true});
					});
				} else {
					alert("Ocurrio un error.");
				}
			});
		}
	});

	$("a[id^='viewDocument']").bind("click", function(){
		$("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{serial_dbf:$(this).attr("serial")});
		$("#fileViewBenefitsDialog").dialog("open");
	});
</script>
{/literal}