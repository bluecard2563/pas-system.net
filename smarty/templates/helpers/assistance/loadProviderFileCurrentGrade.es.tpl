{if !$commentDescription}
<input type="hidden" value="1" id="toValidate"/>
{/if}
<div class="span-13 last line">
				<div class="span-5 label">
					Calificaci&oacute;n:
				</div>
				<div class="span-6">
					{if $commentDescription}
					<i>{$global_comments_by_service_provider_calification[$commentDescription.calification_csp]}</i>
					{else}
					<select class="span-6" id="selGrade" title="El campo 'Calificación' es obligatorio">
						<option value="">- Seleccione -</option>
						{foreach from=$grades item="g"}
						<option value="{$g}">{$global_comments_by_service_provider_calification.$g}</option>
						{/foreach}
					</select>
					{/if}
				</div>
		</div>
		<div class="span-13 last line">
				<div class="span-5 label">
					Observaciones:
				</div>
				<div class="span-6">
					{if $commentDescription}
					<textarea class="span-6" style="border: 0; font-style: italic;">{$commentDescription.observations_csp}</textarea>
					{else}
					<textarea id="txtRateObservation" class="span-6"  title="El campo 'Observaciones' es obligatorio"></textarea>
					{/if}
				</div>
		</div>