{if $data}
	<div class="prepend-1 span-7 last line">
		<table border="0" class="paginate-1 max-pages-7" id="benefits_table_dialog">
			<tr bgcolor="#284787">
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					#
				</td>
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					Coordinador
				</td>
			</tr>
			{foreach name="providerList" from=$data item="d"}
				<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.providerList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
					<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
						{$smarty.foreach.providerList.iteration}
					</td>
					<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						{$d.name_spv}
					</td>
				</tr>
			{/foreach}
		</table>
	</div>
{else}
	<center>No se est&aacute; utilizando ning&uacute;n proveedor.</center>
{/if}