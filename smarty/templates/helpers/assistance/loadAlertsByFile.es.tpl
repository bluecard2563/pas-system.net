{if $alerts}
<center>
    <div class="span-15 last line">
        <table border="0" class="paginate-1 max-pages-7" id="alerts_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="t"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                        {$t}
                    </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="alerts" from=$alerts item="al"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.alerts.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$smarty.foreach.alerts.iteration}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$al.message_alt|htmlall}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$al.in_date_alt}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$al.alert_time_alt}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    <input type="checkbox" id="atendAlert{$al.serial_alt}" serial_alt="{$al.serial_alt}"/>
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
     <div class="span-15 last pageNavPosition" id="pageNavPositionAlerts"></div>
     <div class="prepend-13 span-2 last">
         <input type="button" id="saveAttendedAlerts" value="Guardar" />
     </div>
</center>
{else}
    <center>
        <ul id="alerts" class="alerts" style="display: block; text-align: center">
            No hay alertas pendientes para este caso.
        </ul>
    </center>
{/if}

{literal}
<script type="text/javascript">
    if($("#alerts_table").length>0){
        pager = new Pager('alerts_table', 3 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPositionAlerts'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    $('#saveAttendedAlerts').bind("click", function(){
        $("input:checked[id^='atendAlert']").each(function(){
            $.getJSON(document_root+"rpc/assistance/attendAlerts.rpc", {serial_alt:$(this).attr('serial_alt')}, function(data){
                if(data){
                    $("#viewAlertsDialog").dialog('close');
                    if($('#hdnSerialFleEdit').val()) {
                        $('#alertsByFileContainer').load(document_root+"rpc/assistance/loadAlertsByFile.rpc", {serial_fle:$('#hdnSerialFleEdit').val()})
                    } else {
                        $('#alertsByFileContainer').load(document_root+"rpc/assistance/loadAlertsByFile.rpc", {serial_fle:$('#hdnSerialFle').val()})
                    }
                    $("#viewAlertsDialog").dialog('open');
                }
            });
        });
    });
</script>
{/literal}