
<div class="span-10 prepend-2 last">
	    <ul id="docPendingAlerts" class="alerts"></ul>
</div>
<form id="newPendingDocument" name="newPendingDocument">
	<div class="span-13 last line">
			<div class="span-5 label">
			   A Recibir de:
			</div>
			<div class="span-6">
				<select id="receiveFrom" class="span-6">
					<option value="client">Cliente</option>
					<option value="provider">Coordinador</option>
				</select>
			</div>
	</div>
	<div class="span-13 last line" id="docProviderContainer" style="display: none;">
			<div class="span-5 label">
				Coordinador:
			</div>
			<div class="span-6">
				<select class="span-6" id="docServiceProvider" title="El campo 'Coordinador' es obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from=$providers item="pv"}
					<option value="{$pv.serial_spv}">{$pv.name_spv}</option>
					{/foreach}
				</select>
			</div>
	</div>
	<div class="span-13 last line">
			<div class="span-5 label">
				Documento:
			</div>
			<div class="span-6">
				<input type="text" class="span-6" id="pendingDocument" name="pendingDocument" title="El campo 'Documento' es obligatorio"/>
			</div>
	</div>
	<input type="hidden" id="serial_cus" name="serial_cus" value="{$serial_cus}"/>
	<input type="hidden" id="serial_fle" name="serial_fle" value="{$serial_fle}"/>
</form>
{if $registeredDocs}
<a class="prepend-5 span-8" id="showRegisteredPendingDocs" style="cursor: pointer;">Ver Documentos Registrados</a>
<div class="prepend-1 span-13 last line" id="alreadyPendingDocuments" style="display:none;">
	<table>
		<thead>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					Documento
				</td>
				<td align="center" class="tableTitle">
					Recibir De
				</td>
				<td align="center" class="tableTitle">
					Nombre
				</td>
			</tr>
		</thead>
		<tbody>
			{foreach name="docs" from=$registeredDocs item="dc"}
				<tr {if $smarty.foreach.docs.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
					<td align="center">
						{$dc.name_pdf}
					</td>
					<td align="center">
						{if $dc.pendingFrom eq 'CLIENT'}Cliente{else} Proveedor{/if}
					</td>
					<td align="center">
						{$dc.namePendingFrom}
					</td>
				</tr>			
			{/foreach}
		</tbody>
	</table>
</div>
{/if}