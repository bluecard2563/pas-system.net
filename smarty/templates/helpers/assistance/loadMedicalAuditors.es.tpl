{if $serial_cou}
    {if $auditor_list}
    <center>
        <div class="span-14 last line">
            <table border="0" class="paginate-1 max-pages-7" id="auditors_table">
                <thead>
                    <tr bgcolor="#284787">
                        {foreach from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                            {$t}
                        </td>
                        {/foreach}
                    </tr>
                </thead>

                <tbody>
                {foreach name="auditors" from=$auditor_list item="al"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.auditors.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$smarty.foreach.auditors.iteration}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$al.name_usr}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$al.pending_req}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <a style="cursor: pointer;" id="asignAuditor{$al.serial_usr}" serial_usr="{$al.serial_usr}">Asignar</a>
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </div>

         <div class="span-14 last pageNavPosition" id="pageNavPosition"></div>
    </center>
    {else}
    <center>La tarjeta no tiene expedientes cerrados previos.</center>
    {/if}
{else}
<center>Seleccione un pa&iacute;s.</center>
{/if}

{literal}
<script type="text/javascript">
if($("#auditors_table").length>0) {
    var pager = new Pager('auditors_table', 3 , 'Siguiente', 'Anterior');
    pager.init(7); //Max Pages
    pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
    pager.showPage(1); //Starting page
}

$("a[id^='asignAuditor']").bind("click",function(){
    var serial_usr = $(this).attr('serial_usr');
    $.getJSON(document_root+"rpc/assistance/checkPendingRequests.rpc",
        {
            serial_fle:$('#hdnSerialFleCheckup').val()
        },
        function(exists){
            if(exists) {
                alert("El expediente ya tiene una auditor\u00EDa m\u00E9dica pendiente.");
            } else {
                $("#messageText").html('Enviando a auditor\u00EDa m\u00E9dica.');
                $("#uploading_file").dialog('open');
                $.getJSON(document_root+"rpc/assistance/createMedicalCheckup.rpc",
                {
                    serial_fle:$('#hdnSerialFleCheckup').val(),
                    serial_usr: serial_usr
                },
                function(data){
                    if(data) {
                        $('#uploading_file').dialog('close');
                        $('#paymentRequestInnerMessage').addClass('success');
                        $('#paymentRequestInnerMessage').html("Enviado exitosamente a auditoria medica.");
                        $('#paymentRequestMessages').show();
                        $('#paymentRequestContainer').load(document_root+"rpc/assistance/loadMedicalAuditors.rpc",{serial_cou: $('#medicalCheckupCountry option:selected').val()});
						$('#medCheckupContainer'+$('#hdnSerialFleCheckup').val()).html('En espera de respuesta.');
					} else {
                        $("#uploading_file").dialog('close');
                        $('#paymentRequestInnerMessage').addClass('error');
                        $('#paymentRequestInnerMessage').html("No se pudo enviar a auditoria medica.");
                        $('#paymentRequestMessages').show();
                    }
                });
            }
        });
});
</script>
{/literal}