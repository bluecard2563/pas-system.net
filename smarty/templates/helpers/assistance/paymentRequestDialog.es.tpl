<div id="loadPaymentRequestDialog">
    <div class="span-12 last line separator">
        <label>Selecci&oacute;n de auditor m&eacute;dico:</label>
    </div>
    <div class="span-12 last line">
        <div class="prepend-1 span-4 label">
            Pa&iacute;s:
        </div>
        <div class="span-4 append-1">
            <select>
                <option value="">- Seleccione -</option>
                {foreach from=$country_list item="cl"}
                <option value="{$cl.serial_cou}">{$cl.name_cou}</option>
                {/foreach}
            </select>
        </div>
    </div>
</div>