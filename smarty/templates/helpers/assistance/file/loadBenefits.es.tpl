{if $data.benefits}
<center>
	<div class="prepend-2 span-19 last line">
		<table border="0" class="paginate-1 max-pages-7" id="benefits_table">
			<thead>
				<tr bgcolor="#284787">
					{foreach from=$data.benefit_titles item="t"}
					<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						{$t}
					</td>
					{/foreach}
				</tr>
			</thead>

			<tbody>
			{foreach name="benefitList" from=$data.benefits item="bl"}
			<tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.benefitList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$smarty.foreach.benefitList.iteration}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$bl.description_bbl|htmlall}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$bl.alias_con|htmlall}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{if $bl.price_bxp neq 0}
						{$bl.symbol_cur|htmlall} {$bl.price_bxp}
					{else}
						N/A
					{/if}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{if $bl.restriction_price_bxp}
						{$bl.restriction_price_bxp|htmlall}
					{else}
						-
					{/if}
				</td>
			</tr>
			{/foreach}

			</tbody>
		</table>
	</div>

	 <div class="span-22 last line pageNavPosition" id="pageNavPositionBenefits"></div>
</center>
{else}
	<div class="prepend-9 span-5 last line">La tarjeta no tiene beneficios.</div>
{/if}

 {if $services}
<div class="span-22 last line separator">
	<label>Servicios Adicionales:</label>
</div>
<center>
	<div class="prepend-2 span-19 last line">
		<table border="0" class="paginate-1 max-pages-7" id="services_table">
			<thead>
				<tr bgcolor="#284787">
					{foreach from=$data.service_titles item="t"}
					<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						{$t}
					</td>
					{/foreach}
				</tr>
			</thead>

			<tbody>
			{foreach name="serviceList" from=$data.services item="sl"}
			<tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.serviceList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$smarty.foreach.serviceList.iteration}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$sl.name_sbl|htmlall}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$sl.coverage_sbc|htmlall}
				</td>
			</tr>
			{/foreach}

			</tbody>
		</table>
	</div>

	 <div class="span-22 last line pageNavPosition" id="pageNavPositionServices"></div>
</center>
{/if}