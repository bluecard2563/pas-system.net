<div class="span-23 last line" id="customers">
	{foreach from=$data.customer item="c"}
	<h3><a href="#">{$c.first_name_cus} {$c.last_name_cus}</a></h3>
	<div>
		<!--CUSTOMER INFO-->
		<div class="span-22 last line separator">
			<label>Datos del Cliente:</label>
		</div>
		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Documento:</div>
			<div class="span-4">{if $c.document_cus}{$c.document_cus}{else}-{/if}</div>
			<div class="span-4 label">Tipo de Cliente:</div>
			<div class="span-4 append-1 last">
					{if $c.type_cus eq 'PERSON'}Persona Natural{elseif $c.type_cus eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{else}-{/if}
			</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line" id="divName" {if $c.type_cus neq PERSON}style="display: none"{/if}>
			<div class="prepend-1 span-4 label">Nombre:</div>
			<div class="span-4">{if $c.first_name_cus}{$c.first_name_cus}{else}-{/if}</div>
			<div class="span-4 label">Apellido:</div>
			<div class="span-4 append-1 last">{if $c.last_name_cus}{$c.last_name_cus}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line" id="divName2" {if $c.type_cus eq PERSON}style="display: none"{/if}>
			<div class="prepend-1 span-4 label">Raz&oacute;n Social:</div>
			<div class="span-4">{if $c.first_name_cus}{$c.first_name_cus}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Pa&iacute;s:</div>
			<div class="span-4">{if $c.name_cou}{$c.name_cou}{else}-{/if}</div>
			<div class="span-4 label">Ciudad:</div>
			<div class="span-4 append-1 last">{if $c.name_cit}{$c.name_cit}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line"  id="divAddress" {if $c.type_cus neq PERSON}style="display: none"{/if}>
			<div class="prepend-1 span-4 label">Fecha de Nacimiento:</div>
			<div class="span-4">{if $c.b_date}{$c.b_date}{else}-{/if}</div>
			<div class="span-4 label">Direcci&oacute;n:</div>
			<div class="span-4 append-1 last">{if $c.address_cus}{$c.address_cus}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line" id="divAddress2" {if $c.type_cus eq PERSON}style="display: none"{/if}>
			<div class="prepend-1 span-4 label">Direcci&oacute;n:</div>
			<div class="span-4">{if $c.address_cus}{$c.address_cus|htmlall}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
			<div class="span-4">{if $c.phone1_cus}{$c.phone1_cus}{else}-{/if}</div>
			<div class="span-4 label">Tel&eacute;fono Secundario:</div>
			<div class="span-4 append-1 last">{if $c.phone2_cus}{$c.phone2_cus}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Celular:</div>
			<div class="span-4">{if $c.cellphone_cus}{$c.cellphone_cus}{else}-{/if}</div>
			<div class="span-4 label">E-mail:</div>
			<div class="span-4 append-1 last">{if $c.email_cus}{$c.email_cus}{else}-{/if}</div>
		</div>


		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Familiar:</div>
			<div class="span-4">{if $c.relative_cus}{$c.relative_cus}{else}-{/if}</div>
			<div class="span-4 label">Tel&eacute;fono del familiar:</div>
			<div class="span-4 append-1 last">{if $c.relative_phone_cus}{$c.relative_phone_cus}{else}-{/if}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-8 span-4">
					{if $c.vip_cus eq 1}*Es cliente VIP*{/if}
			</div>
		</div>
		<!--END CUSTOMER INFO-->

		<!--TRAVEL INFO-->
		<div class="span-22 last line separator">
			<label>Datos del Viaje:</label>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Fecha de salida:</div>
			<div class="span-4">{if $c.start_date}{$c.start_date}{else}-{/if}</div>
			<div class="span-4 label">Fecha de llegada:</div>
			<div class="span-4 append-1 last">{if $c.end_date}{$c.end_date}{else}-{/if}</div>
		</div>
		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Pa&iacute;s destino:</div>
			<div class="span-4">{if $c.country_des}{$c.country_des|htmlall}{else}-{/if}</div>
			<div class="span-4 label">Ciudad destino:</div>
			<div class="span-4 append-1 last">{if $c.city_des}{$c.city_des|htmlall}{else}-{/if}</div>
		</div>
		<!--END TRAVEL INFO-->

		<!--MEDICAL INFO-->
		<div class="span-22 last line separator">
			<label>Historial M&eacute;dico:</label>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			{if $c.medical_background_cus}
			<div class="prepend-4 span-10 append-4">
				<textarea id="medicalBackground{$c.serial_cus}" readonly>{$c.medical_background_cus|htmlall}</textarea>
			</div>
			{else}
			<center>El cliente no tiene registros en su historial m&eacute;dico.</center>
			{/if}
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			{if $c.other_cards}
			<center>
				<label>El cliente tiene otros expedientes en las siguientes tarjetas:</label>
				{foreach name="otherCardsList" from=$c.other_cards item="oc"}
					{if $smarty.foreach.otherCardsList.first}
						<a href="{$document_root}modules/assistance/fEditAssistance/0/0/{$oc.card_number}" target="_blank">{$oc.card_number}</a>
					{else}
					-&nbsp<a href="{$document_root}modules/assistance/fEditAssistance/0/0/{$oc.card_number}" target="_blank">{$oc.card_number}</a>
					{/if}
				{/foreach}
			</center>
			{/if}
		</div>
		<!--END MEDICAL INFO-->

		<div class="prepend-18 span-4 last line">
			<div class="span-4">
				<input id="createFile{$c.serial_cus}" serial="{$c.serial_cus}" serial_trl="{$c.serial_trl}" type="button" value="Crear expediente" />
			</div>
		</div>
	</div>
	{/foreach}
</div>