<div class="span-24">
	{if $providerList}
	<div class="span-24 last line">
		<div class="prepend-7 span-5 right">
			<label>* Seleccione un Coordinador:</label>
		</div>
		
		<div class="span-9 last">
			<select name="selProviderHistory" id="selProviderHistory" >
				<option value="">- Seleccione -</option>
				{foreach from=$providerList item="sp"}
					<option value="{$sp.serial_spv}">{$sp.name_spv|htmlall}</option>
				{/foreach}
			</select>
		</div>
	</div>
			
	<div class="span-24 last line" id="documents_history_container"></div>
	{else}
		<div class="prepend-6 span-12 append-6 last">
			<div class="span-12 error center">
				No existen coordinadores asociados a este expediente.
			</div>
		</div>
	{/if}
</div>