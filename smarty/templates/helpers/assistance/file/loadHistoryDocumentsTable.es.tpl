{if $data.fileList}
<div class="span-22 last line">
	<table border="0">
		<thead>
			<tr bgcolor="#284787">
				{foreach name="titles_dialog" from=$titles item="t"}
					<td align="center" class="tableTitle">
						{$t}
					</td>
				{/foreach}
			</tr>
		</thead>
		<tbody>
		{foreach name="files" from=$data.fileList item="fl"}
		<tr {if $smarty.foreach.files.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
			<td align="center" class="tableField">
				{$fl.name_dbf|htmlall}
			</td>
			<td align="center" class="tableField">
				{$fl.comment_dbf|htmlall}
			</td>
			<td id="reclaimedContainer_{$fl.serial_prq}" align="center" class="tableField">
				{$fl.amount_reclaimed_prq}
			</td>
			<td id="authorizedContainer_{$fl.serial_prq}" align="center" class="tableField">
				{$fl.amount_authorized_prq}
			</td>
			<td align="center" class="tableField">
				{$global_billTo[$fl.document_to_dbf]}
			</td>
			<td align="center" class="tableField">
				{$fl.date_dbf}
			</td>
			<td align="center" class="tableField" id="changeStatusPrqContainer{$fl.serial_prq}">
				{if $fl.status_prq eq "STAND-BY"}
				<select name="changeStatusPrq" id="changeStatusPrq{$fl.serial_prq}" serial_prq="{$fl.serial_prq}">
					<option value="STAND-BY" selected>Pendiente</option>
					<option value="APROVED">Aprobado</option>
					<option value="DENIED">Negado</option>
				</select>
				{else}
					{if $fl.status_prq eq "DENIED"}<font color="red">{/if}
					{$global_paymentRequestStatus[$fl.status_prq]}
					{if $fl.status_prq eq "DENIED"}</font>{/if}
				{/if}
			</td>
			<td align="center" class="tableField">
				{if $fl.status_prq eq "STAND-BY"}
					<input type="hidden" name="authorized_amounts_{$fl.serial_prq}" id="authorized_amounts_{$fl.serial_prq}" />
					<input type="hidden" name="presented_amounts_{$fl.serial_prq}" id="presented_amounts_{$fl.serial_prq}" />
					<a id="editDocument{$fl.serial_prq}" style="cursor: pointer" serial_dbf="{$fl.serial_dbf}" serial_prq="{$fl.serial_prq}">Ver</a>
				{else}
					<a id="viewDocument{$fl.serial_dbf}" style="cursor: pointer" serial_dbf="{$fl.serial_dbf}">Ver</a>
				{/if}
			</td>
			<td align="center" class="tableField">
				{$fl.observations_prq|htmlall}
			</td>
			<td align="center" class="tableField">
				{$fl.type_prq}
			</td>
			<td align="center" class="tableField">
				<a href="{$document_root}{$fl.url_dbf}" target="blank">Abrir</a>
			</td>
			<td align="center" class="tableField">
				{if $fl.dispatched_prq == 'YES'}
					{$fl.dispatched_date_prq}
				{else}-{/if}
			</td>
			<td align="center" class="tableField">
				{if $fl.dispatched_prq == 'YES'}
					<a href="{$document_root}{$fl.file_url_prq}" target="blank">Abrir</a>
				{else}
					-
				{/if}
			</td>
		</tr>
		{/foreach}
		</tbody>
	</table>
</div>
{else}
	<div class="prepend-6 span-12 append-6 last">
		<div class="span-12 error center">
			No existen coordinadores asociados a este expediente.
		</div>
	</div>
{/if}