{if $files_list}
	<div class="prepend-1 span-22 last line">
		<table border="1" class="dataTable" id="files_table">
			<thead>
				<tr class="header">
					<th class="center">
						No. Expediente
					</th>
					<th class="center">
						No. Tarjeta
					</th>
					<th class="center">
						Asegurado
					</th>
					<th class="center">
						Diagn&oacute;stico
					</th>
					<th class="center">
						Fecha de Ocurrencia
					</th>
					<th class="center">
						Subir Documentos
					</th>
				</tr>
			</thead>
			<tbody>
				{foreach name="files" from=$files_list item="f"}
					<tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.files.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
						<td class="tableField">
							{$f.serial_fle}
						</td>
						<td class="tableField">
							{$f.card_number_sal}
						</td>
						<td class="tableField">
							{$f.pax|htmlall}
						</td>
						<td class="tableField">
							{$f.diagnosis_fle|htmlall}
						</td>
						<td class="tableField">
							{$f.incident_date_fle}
						</td>
						<td class="tableField">
							<a href="{$document_root}modules/assistance/docs/uploadPayable/{$f.serial_fle}/{$provider_id}">Subir</a>
						</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
{else}
	<div class="span-24 last line center">
		<i>No existen expedientes asociados a este proveedor.</i>
	</div>
{/if}