{if $contactList}
<center>
    <div class="span-18 last line">
        <table border="0" class="paginate-1 max-pages-7" id="contacts_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="t"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        {$t}
                    </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="contacts" from=$contactList item="cl"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.contacts.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.name_cou}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.name_cit}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.name_csp}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.phone_csp}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.email_csp}
                </td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {if $cl.email2_csp}{$cl.email2_csp}{else}-{/if}
                </td>
            </tr>
            {/foreach}

            </tbody>
        </table>
    </div>

     <div class="span-18 last line pageNavPosition" id="pageNavPositionContacts"></div>
</center>
{else}
<center>El coordinador no tiene operadores registrados.</center>
{/if}

{literal}
<script type="text/javascript">
if($("#contacts_table").length>0) {
    pager = new Pager('contacts_table', 5 , 'Siguiente', 'Anterior');
    pager.init(11); //Max Pages
    pager.showPageNav('pager', 'pageNavPositionContacts'); //Div where the navigation buttons will be placed.
    pager.showPage(1); //Starting page
}
</script>
{/literal}