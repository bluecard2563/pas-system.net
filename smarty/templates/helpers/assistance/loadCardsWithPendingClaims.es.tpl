{if $cardList}
    <select name="selCard" id="selCard" title="El campo 'Tarjeta' es obligatorio">
        <option value="">- Seleccione -</option>
        {foreach name="$Cards" from=$cardList item="l"}
            <option value="{$l.card_number}" {if $cardList|@count eq 1}selected{/if}> Tarjeta #{$l.card_number}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cus neq ''}
        No existen paises registrados.
    {/if}
    <select name="selCard" id="selCard" title="El campo 'Tarjeta' es obligatorio" {if $serial_cus neq ''}style="display:none;"{/if}>
        <option value="">- Seleccione -</option>
    </select>
{/if}