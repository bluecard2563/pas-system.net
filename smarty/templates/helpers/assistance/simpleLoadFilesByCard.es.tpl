{if $files_list}
	<form name="frmVoidFiles" id="frmVoidFiles" action="{$document_root}modules/assistance/pVoidFiles" method="post">
		<div class="span-24 last line">
			<div class="prepend-4 span-3">
				<label>* Observaciones:</label>
			</div>
			
			<div class="span-8">
				<textarea name="txtObservations" id="txtObservations" title="El campo 'Observaciones' es obligatorio"></textarea>
			</div>
		</div>
		
		
		<div class="prepend-1 span-22 last line">
			<table border="1" class="dataTable" id="files_table">
				<thead>
					<tr class="header">
						<th class="center">&nbsp;</th>
						<th class="center">
							No. Expediente
						</th>
						<th class="center">
							Pasajero
						</th>
						<th class="center">
							Estado
						</th>	
						<th class="center">
							Diagn&oacute;stico
						</th>	
						<th class="center">
							Fecha de Apertura
						</th>
					</tr>
				</thead>
				<tbody>
				{foreach name="files" from=$files_list item="f"}
				<tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.files.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
					<td class="tableField">
						<input type="checkbox" id="chkFile_{$f.serial_fle}" name="chkFiles[]" value="{$f.serial_fle}" />
					</td>
					<td class="tableField">
						{$f.serial_fle}
					</td>
					<td class="tableField">
						{$f.customer|htmlall}
					</td>
					<td class="tableField">
						{$global_fileStatus[$f.status_fle]}
					</td>
					<td class="tableField">
						{$f.diagnosis_fle|htmlall}
					</td>
					<td class="tableField">
						{$f.creation_date_fle}
					</td>
				</tr>
				{/foreach}
				</tbody>
			</table>
		</div>

		<div class="span-24 last line buttons">
			<input type="submit" name="btnSubmit" id="btnSubmit" value="Anular" />
			<input type="hidden" name="hdnOneRequired" id="hdnOneRequired" value="" title="Seleccione al menos un expediente." />
		</div>
	</form>
{else}
	<div class="span-24 last line center">
		<i>No existen expedientes asociados a esta tarjeta.</i>
	</div>
{/if}