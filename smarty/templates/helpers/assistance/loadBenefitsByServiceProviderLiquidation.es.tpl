<div class="span-22 last line separator">
    <label>Calificación del Coordinador:</label>
</div>
{if $comment}
	  <div class="prepend-6 span-13 last line">
				<div class="span-5 label">
					Calificaci&oacute;n:
				</div>
				<div class="span-6">
					<i>{$global_comments_by_service_provider_calification[$comment.calification_csp]}</i>
				</div>
		</div>
		<div class="prepend-6 span-13 last line">
				<div class="span-5 label">
					Observaciones:
				</div>
				<div class="span-6">
					<textarea class="span-6" disabled="disabled" style="border: 0; font-style: italic;">{$comment.observations_csp}</textarea>
				</div>
		</div>
{else}
<center><b>El Coordinador a&uacute;n no ha sido calificado.</b></center>
{/if}
<div class="span-22 last line separator">
    <label>Beneficios:</label>
</div>

{if $benefits}
    <center>
        <input type="hidden" id="hdnMedicalType" value="{$medical_tpe}"/>
        <input type="hidden" id="hdnAsistanceCost" value="{$assistanceCost}"/>
        <div class="span-22 last line">
            <table border="0" class="paginate-1 max-pages-7" id="benefitsTable">
                <thead>
                    <tr bgcolor="#284787">
                        {foreach name="titles_dialog" from=$titles_liq item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                {$t}
                            </td>
                        {/foreach}
                    </tr>
                </thead>

                <tbody>
                {foreach name="benefitList" from=$benefits item="bl"}
                {if $benefitsList[$bl.serial_bxp]}
                    <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="{cycle values="#d7e8f9,#e8f2fb"}">
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$bl.description_bbl}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            <input type="hidden" style="width: 75px;" name="disponibleAmountBen{$bl.serial_bxp}" id="disponibleAmountBen{$bl.serial_bxp}" serial="{$bl.serial_bxp}" value="{if $bl.price_bxp neq 0}{$bl.price_bxp+$benefitsList[$bl.serial_bxp].estimated_amount_spf}{else}N/A{/if}" readonly />
                            <input type="text" style="width: 75px;" name="maxAmountBen{$bl.serial_bxp}" id="maxAmountBen{$bl.serial_bxp}" value="{if $bl.price_bxp neq 0}{$bl.price_bxp}{else}N/A{/if}" readonly />
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            <input type="text" style="width: 75px;" name="estimatedAmountBen{$bl.serial_bxp}" id="estimatedAmountBen{$bl.serial_bxp}" serial="{$bl.serial_bxp}" value="{$benefitsList[$bl.serial_bxp].estimated_amount_spf}" {if $acces_request eq "view"}readonly{/if} />
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {if $bl.restriction_price_bxp}
                                {$bl.restriction_price_bxp}
                            {else}
                                -
                            {/if }
                        </td>
                    </tr>
                {/if}
                {/foreach}
                {foreach name="serviceList" from=$services item="sl"}
                {if $servicesList[$sl.serial_sbc]}
                    <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="{cycle values="#d7e8f9,#e8f2fb"}">
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$sl.name_sbl}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            <input type="hidden" style="width: 75px;" name="disponibleAmountSer{$sl.serial_sbc}" id="disponibleAmountSer{$sl.serial_sbc}" serial="{$sl.serial_sbc}" value="{$sl.coverage_sbc+$servicesList[$sl.serial_sbc].estimated_amount_spf}" readonly />
                            <input type="text" style="width: 75px;" name="maxAmountSer{$sl.serial_sbc}" id="maxAmountSer{$sl.serial_sbc}" value="{$sl.coverage_sbc}" readonly />
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            <input type="text" style="width: 75px;" name="estimatedAmountSer{$sl.serial_sbc}" id="estimatedAmountSer{$sl.serial_sbc}" serial="{$sl.serial_sbc}" value="{$servicesList[$sl.serial_sbc].estimated_amount_spf}" {if $acces_request eq "view"}readonly{/if} />
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            -
                        </td>
                    </tr>
                {/if}
                {/foreach}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;">
                    <td style="padding:5px 5px 5px 5px; text-align:center;">&nbsp;</td>
                    <td style="padding:5px 5px 5px 5px; text-align:right;">
                        Total:
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="totalEstimatedAmount" id="totalEstimatedAmount" value="{$totalEstimatedAmount}" readonly />
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
         <div class="prepend-1 span-22 last line pageNavPosition" id="pageNavPositionBenefits"></div>
    </center>

    {if $acces_request neq "view"}
    <div class="prepend-8 span-6 last line">
        <div class="span-4">
            <input type="button" id="btnCreateReport" value="Generar Reporte" />
        </div>
        <div class="span-2 last line">
            <input type="button" id="btnSaveBenefitsByProvider" value="Guardar Cambios" />
        </div>
    </div>
    {/if}
{else}
    <center>La tarjeta no tiene beneficios.</center>
{/if}


{literal}
<script type="text/javascript">
    $("input[id^='estimatedAmountBen']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
        if(parseFloat(this.value) > parseFloat($('#disponibleAmountBen'+$(this).attr('serial')).val())) {
            alert("El valor sobrepasa el monto m\u00E1ximo.");
            this.value = '';
        }
        $('#maxAmountBen'+$(this).attr('serial')).val($('#disponibleAmountBen'+$(this).attr('serial')).val()-$('#estimatedAmountBen'+$(this).attr('serial')).val());
        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("input[id^='estimatedAmountSer']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });

    $("input[id^='estimatedAmountSer']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
        if(parseFloat(this.value) > parseFloat($('#disponibleAmountSer'+$(this).attr('serial')).val())) {
            alert("El valor sobrepasa el monto m\u00E1ximo.");
            this.value = '';
        }
        $('#maxAmountSer'+$(this).attr('serial')).val($('#disponibleAmountSer'+$(this).attr('serial')).val()-$('#estimatedAmountSer'+$(this).attr('serial')).val());
        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("input[id^='estimatedAmountSer']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });


    //Create Report
    $('#btnCreateReport').bind("click",function(){
        $.ajax({
          type: "POST",
          url: document_root+'rpc/assistance/createServiceProvicerReport.rpc',
          data:  {
                    hdnSerialFle: $('#txtSerialFle').val(),
                    card_number: $('#txtCardNumber').val(),
                    txtDiagnosis: $('#txtDiagnosis').val(),
                    txtAssistanceType: $('#txtAssistanceType').val(),
					txtMedicalBackground: $('#txtMedicalBackground').val(),
                    selCity: $('#hdnSerialCit').val(),
                    serial_spv: $('#selProviders').val()
                 },
          success: function(data){
              if(data) {
                  window.location = document_root+"rpc/assistance/"+data;
                  $.getJSON(document_root+'rpc/assistance/unlinkServiceProviderReport.rpc', {report_name:data}, function(){
                      if(!data) {
                          alert("El archivo no fue borrado del servidor.");
                      }
                  });
              } else {
                  alert("Guarde los cambios antes de generar el reporte.");
              }
          }
        });
    });

    //Saves changes
    $('#btnSaveBenefitsByProvider').bind("click", function(){
        $('#benefitsInnerMessage').addClass('success');
        $('#benefitsInnerMessage').html("Cambios guardados exitosamente.");
        $("input[id^='estimatedAmountBen']").each(function(){
           $.getJSON(document_root+'rpc/assistance/addServiceProviderByFile.rpc',
                    {
                        serial_bxp:$(this).attr("serial"),
                        serial_fle:$('#txtSerialFle').val(),
                        serial_spv:$('#selProviders').val(),
                        estimated_amt:this.value,
                        medical_tpe:$('#selMedicalType').val()
                    },
                    function(data) {
                        if(!data) {
                            $('#benefitsInnerMessage').removeClass();
                            $('#benefitsInnerMessage').addClass('error');
                            $('#benefitsInnerMessage').html("Hubieron errores al guardar los cambios.");
                            return false;
                        }
           });
        });

        $("input[id^='estimatedAmountSer']").each(function(){
           $.getJSON(document_root+'rpc/assistance/addServiceProviderByFile.rpc',
                    {
                        serial_sbc:$(this).attr("serial"),
                        serial_fle:$('#txtSerialFle').val(),
                        serial_spv:$('#selProviders').val(),
                        estimated_amt:this.value,
                        medical_tpe:$('#selMedicalType').val()
                    },
                    function(data) {
                        if(!data) {
                            $('#benefitsInnerMessage').removeClass();
                            $('#benefitsInnerMessage').addClass('error');
                            $('#benefitsInnerMessage').html("Hubieron errores al guardar los cambios.");
                            return false;
                        }
           });
        });

        $('#benefitsMessages').show();
        setTimeout(function(){
            $('#benefitsMessages').hide("slide", {direction: "up"}, 500);
        },2500)
    });

    function moneyFormat(amount) {
        var val = parseFloat(amount);
        if (isNaN(val)) {return "0.00";}
        if (val <= 0) {return "0.00";}
        val += "";
        // Next two lines remove anything beyond 2 decimal places
        if (val.indexOf('.') == -1) {return val+".00";}
        else {val = val.substring(0,val.indexOf('.')+3);}
        val = (val == Math.floor(val)) ? val + '.00' : ((val*10 ==
        Math.floor(val*10)) ? val + '0' : val);
        return val;
    }

</script>
{/literal}