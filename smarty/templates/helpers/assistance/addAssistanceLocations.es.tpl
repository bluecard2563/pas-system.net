{if $contact_info}
<center>
    <div class="span-12 last line">
        <table border="0" class="paginate-1 max-pages-7" id="location_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach name="titles" from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                            {$t}
                        </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="contact_info" from=$contact_info item="ci"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.contact_info.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$smarty.foreach.contact_info.iteration}
                </td>
                 <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ci.name|htmlall}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ci.phone}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ci.address|htmlall}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ci.date}
                </td>
            </tr>
            {/foreach}

            </tbody>
        </table>
    </div>
     <div class="span-11 last pageNavPosition" id="pageNavPositionLocation"></div>
</center>

{literal}
<script type="text/javascript">
    pager = new Pager('location_table', 3 , 'Siguiente', 'Anterior');
    pager.init(7); //Max Pages
    pager.showPageNav('pager', 'pageNavPositionLocation'); //Div where the navigation buttons will be placed.
    pager.showPage(1); //Starting page
</script>
{/literal}
{/if}