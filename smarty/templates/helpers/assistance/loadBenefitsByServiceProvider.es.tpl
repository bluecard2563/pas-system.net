{if $serial_spv neq ''}
    {if $benefits}
    <center>
        <input type="hidden" id="hdnMedicalTpe" value="{$medical_tpe}"/>
        <div class="span-12 last line">
            <table border="0" class="paginate-1 max-pages-7" id="benefits_table_dialog">
                <thead>
                    <tr bgcolor="#284787">
                        {if $status neq "closed"}<td align="center" style="border: solid 1px white; padding:0px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">
                            <input type="checkbox" name="selAll" id="selAll" />
                        </td>{/if}
                        {foreach name="titles_dialog" from=$titles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                {$t}
                            </td>
                        {/foreach}
                    </tr>
                </thead>

                <tbody>
                {foreach name="benefitList" from=$benefits item="bl"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.benefitList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                    {if $status neq "closed"}<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                        <input type="checkbox" class="chkBen" id="chkBen{$bl.serial_bxp}" name="serial_bxp[]" value="{$bl.serial_bxp}" {if $benefitsList[$bl.serial_bxp] or $bl.price_bxp eq "0"}blocked="true" disabled checked{else}blocked="false"{/if} title="Debe seleccionar al menos un beneficio." />
                    </td>{/if}
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$bl.description_bbl}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="maxAmountBen{$bl.serial_bxp}" id="maxAmountBen{$bl.serial_bxp}" serial="{$bl.serial_bxp}" value="{$bl.price_bxp}" readonly />
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="estimatedAmountBen{$bl.serial_bxp}" id="estimatedAmountBen{$bl.serial_bxp}" serial="{$bl.serial_bxp}" value="{$benefitsList[$bl.serial_bxp].estimated_amount_spf}" {if $benefitsList[$bl.serial_bxp]}blocked="true"{else}blocked="false"{/if} disabled />
                    </td>
                </tr>
                {/foreach}
                {foreach name="serviceList" from=$services item="sl"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.servicesList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                    {if $status neq "closed"}<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                        <input type="checkbox" id="chkSer{$sl.serial_sbc}" name="serial_sbc[]" value="{$sl.serial_sbc}"  {if $servicesList[$sl.serial_sbc] or $sl.coverage_sbc eq "0"}blocked="true" disabled checked{else}blocked="false"{/if} />
                    </td>{/if}
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$sl.name_sbl}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="maxAmountSer{$sl.serial_sbc}" id="maxAmountSer{$sl.serial_sbc}" serial="{$sl.serial_sbc}" value="{$sl.coverage_sbc}" readonly />
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="estimatedAmountSer{$sl.serial_sbc}" id="estimatedAmountSer{$sl.serial_sbc}" serial="{$sl.serial_sbc}" value="{$servicesList[$sl.serial_sbc].estimated_amount_spf}"  {if $servicesList[$sl.serial_sbc]}blocked="true"{else}blocked="false"{/if} disabled/>
                    </td>
                </tr>
                {/foreach}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;">
                    {if $status neq "closed"}<td style="padding:5px 5px 5px 5px; text-align:center;"></td>{/if}
                    <td style="padding:5px 5px 5px 5px; text-align:right;">
                        Total:
                    </td>
                    <td style="padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="totalMaxAmount" id="totalMaxAmount" value="{$totalMaxAmount}" readonly />
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <input type="text" style="width: 75px;" name="totalEstimatedAmount" id="totalEstimatedAmount" value="{$totalEstimatedAmount}" readonly />
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
         <div class="span-12 last line pageNavPosition" id="pageNavPositionDialog"></div>
    </center>

    <div class="prepend-4 span-6 last line">
        {if $status neq "closed"}
        <div class="span-4">
            <input type="button" id="btnCreateReport" value="Generar Reporte" />
            <div id="reportContainer"></div>
        </div>
        <div class="span-2 last line">
            <input type="button" id="btnSaveBenefitsByProvider" value="Guardar Coordinador" />
        </div>
        {/if}
    </div>
    {else}
        <center>La tarjeta no tiene beneficios.</center>
    {/if}
{else}
    <center>Seleccione un coordinador.</center>
{/if}



{literal}
<script type="text/javascript">
    if($("#benefits_table_dialog").length>0) {
        var pager = new Pager('benefits_table_dialog', 5 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPositionDialog'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    $('#selAll').bind("click",function(){
        if($('#selAll').attr("checked")) {
            $("input[id^='chkBen'][blocked=false]").attr("checked", true);
            $("input[id^='chkSer'][blocked=false]").attr("checked", true);
            $("input[id^='estimatedAmountBen'][blocked=false]").attr("disabled", false);
            $("input[id^='estimatedAmountSer'][blocked=false]").attr("disabled", false);
        } else {
            $("input[id^='chkBen'][blocked=false]").attr("checked", false);
            $("input[id^='chkSer'][blocked=false]").attr("checked", false);
            $("input[id^='estimatedAmountBen'][blocked=false]").attr("disabled", true);
            $("input[id^='estimatedAmountBen'][blocked=false]").val("");
            $("input[id^='estimatedAmountSer'][blocked=false]").attr("disabled", true);
            $("input[id^='estimatedAmountSer'][blocked=false]").val("");
        }

        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });

    $("input[id^='chkBen']").bind("click",function(){
        if($(this).attr("checked")) {
            $("#estimatedAmountBen"+$(this).val()).attr("disabled", false);
        } else {
            $('#selAll').attr("checked", false);
            $("#estimatedAmountBen"+$(this).val()).attr("disabled", true);
            $("#estimatedAmountBen"+$(this).val()).val("");
        }

        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("input[id^='estimatedAmountSer']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });

    $("input[id^='chkSer']").bind("click",function(){
        if($(this).attr("checked")) {
            $("#estimatedAmountSer"+$(this).val()).attr("disabled", false);
        } else {
            $('#selAll').attr("checked", false);
            $("#estimatedAmountSer"+$(this).val()).attr("disabled", true);
            $("#estimatedAmountSer"+$(this).val()).val("");
        }

        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("input[id^='estimatedAmountSer']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });

    $("input[id^='estimatedAmountBen']").bind("focusout",function(){
        if(!/(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value) || /^-/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
        if(parseFloat(this.value) > parseFloat($('#maxAmountBen'+$(this).attr('serial')).val())) {
            alert("El valor sobrepasa el monto m\u00E1ximo.");
            this.value = '';
        }
        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("input[id^='estimatedAmountSer']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });

    $("input[id^='estimatedAmountSer']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
        if(parseFloat(this.value) > parseFloat($('#maxAmountSer'+$(this).attr('serial')).val())) {
            alert("El valor sobrepasa el monto m\u00E1ximo.");
            this.value = '';
        }
        var sum=0;
        $("input[id^='estimatedAmountBen']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("input[id^='estimatedAmountSer']").each(function(){
            if($(this).val()) {
                sum=sum+parseFloat($(this).val());
            }
        });
        $("#totalEstimatedAmount").val(moneyFormat(sum));
    });

    //Create Report
    $('#btnCreateReport').bind("click",function(){
		if($('#txtDiagnosis').val() && $('#txtAssistanceType').val() && $('#selCity').val()){
			$.ajax({
			  type: "POST",
			  url: document_root+'rpc/assistance/createServiceProvicerReport.rpc',
			  data:  {
					hdnSerialFle: $('#hdnSerialFle').val(),
					card_number: $('#hdnCardNumber').val(),
					txtDiagnosis: $('#txtDiagnosis').val(),
					txtAssistanceType: $('#txtAssistanceType').val(),
					txtMedicalBackground: $('#txtMedicalBackground').val(),
					selCity: $('#selCity').val(),
					serial_spv: $('#selProvider').val()
			  },
			  success: function(data){
				  if(data) {
					  window.location = document_root+"rpc/assistance/"+data;
					  $.getJSON(document_root+'rpc/assistance/unlinkServiceProviderReport.rpc', {report_name:data}, function(){
						  if(!data) {
							  alert("El archivo no fue borrado del servidor.");
						  }
					  });
				  } else {
					  alert("Guarde los cambios antes de generar el reporte.");
				  }
			  },
			  error: function(){
				  alert("Ocurrio un error.");
			  }
			});
		} else {
			alert("Debe ingresar todos los campos requeridos.");
		}
    });

    //Saves changes
    $('#btnSaveBenefitsByProvider').bind("click", function(){
        var success = true;
		var selected = true;
        //Save Benefits
		$("input:checked[id^='chkBen'][blocked=false]").each(function(){
			if($('#estimatedAmountBen'+this.value).val()< 0){
				selected = false;
			}
		});
		$("input:checked[id^='chkSer'][blocked=false]").each(function(){
			if($('#estimatedAmountSer'+this.value).val()< 0) {
				selected = false;
			}
		});

		if(selected){
			$("input:checked[id^='chkBen'][blocked=false]").each(function(){
			   if($('#estimatedAmountBen'+this.value).val()>=0){
				   $.getJSON(document_root+'rpc/assistance/addServiceProviderByFile.rpc',
						{
							serial_bxp:this.value,
							serial_fle:$('#hdnSerialFle').val(),
							serial_spv:$('#selProvider').val(),
							estimated_amt:$('#estimatedAmountBen'+this.value).val(),
							medical_tpe:$('#selMedicalType').val()
						},
						function(data) {
							if(data) {
								$('#estimatedAmountBen'+data).attr("disabled", true);
								$('#estimatedAmountBen'+data).attr("blocked", true);
								$('#chkBen'+data).attr("blocked", true);
								$('#chkBen'+data).attr("disabled", true);
								if($('#maxAmountBen'+data).val()!='N/A'){
									$('#maxAmountBen'+data).val(parseFloat($('#maxAmountBen'+data).val())-parseFloat($('#estimatedAmountBen'+data).val()));
								}	
								if($('#selServiceType option:selected').text() != '') {
									$('#serviceTypeContainer').html($('#selServiceType option:selected').text());
								}
							} else {
								success = false;
							}
				   });
			   }
			});

		   //Save services
		   $("input:checked[id^='chkSer'][blocked=false]").each(function(){
				if($('#estimatedAmountSer'+this.value).val()>=0){
				   $.getJSON(document_root+'rpc/assistance/addServiceProviderByFile.rpc',
						{
							serial_sbc:this.value,
							serial_fle:$('#hdnSerialFle').val(),
							serial_spv:$('#selProvider').val(),
							estimated_amt:$('#estimatedAmountSer'+this.value).val(),
							medical_tpe:$('#selMedicalType').val()
						},
						function(data) {
							if(data) {
								$('#estimatedAmountSer'+data).attr("disabled", true);
								$('#estimatedAmountSer'+data).attr("blocked", true);
								$('#chkSer'+data).attr("blocked", true);
								$('#chkSer'+data).attr("disabled", true);
								if($('#maxAmountSer'+data).val()!='N/A'){
									$('#maxAmountSer'+data).val(parseFloat($('#maxAmountSer'+data).val())-parseFloat($('#estimatedAmountSer'+data).val()));
								}	
							} else {
								success = false;
							}
				   });
			   }
		   });
			//Show messages
			if(success) {
				$('#benefitsInnerMessage').addClass('success');
				$('#benefitsInnerMessage').html("Cambios guardados exitosamente.");
			} else {
				$('#benefitsInnerMessage').addClass('error');
				$('#benefitsInnerMessage').html("Hubieron errores al guardar los cambios.");
			}
			$('#benefitsMessages').show();
			setTimeout(function(){
				$('#benefitsMessages').hide("slide", {direction: "up"}, 500);
			},2000);

		} else {
			alert("Todos los beneficios/servicios seleccionados deben tener un monto estimado.");
		}
       
    });
</script>
{/literal}