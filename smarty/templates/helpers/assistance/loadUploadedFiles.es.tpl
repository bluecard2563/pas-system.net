<div class="prepend-7 span-9 append-5 last">
    <ul id="alertsUpload" class="alerts"></ul>
</div>

<input type="hidden" name="hdnAccesPermition" id="hdnAccesPermition" value="{$acces_permition}"/>
{if $acces_permition neq "view" || $data.fileList}
    <div class="span-22 last line">
        <table border="0">
            <thead>
                <tr bgcolor="#284787">
                    {foreach name="titles_dialog" from=$titles item="t"}
                        <td align="center" class="tableTitle">
                            {$t}
                        </td>
                    {/foreach}
                </tr>
            </thead>
            <tbody>
            {if $data.fileList}
            {foreach name="files" from=$data.fileList item="fl"}
            <tr {if $smarty.foreach.files.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td align="center" class="tableField">
                    {$fl.name_dbf|htmlall}
                </td>
                <td align="center" class="tableField">
                    {$fl.comment_dbf|htmlall}
                </td>
                <td id="reclaimedContainer_{$fl.serial_prq}" align="center" class="tableField">
                    {$fl.amount_reclaimed_prq}
                </td>
                <td id="authorizedContainer_{$fl.serial_prq}" align="center" class="tableField">
                    {$fl.amount_authorized_prq}
                </td>
                <td align="center" class="tableField">
                    {$global_billTo[$fl.document_to_dbf]}
                </td>
                <td align="center" class="tableField">
                    {$fl.document_date_dbf}
                </td>
                <td align="center" class="tableField" id="changeStatusPrqContainer{$fl.serial_prq}">
                    {if $fl.status_prq eq "STAND-BY"}
                    <select name="changeStatusPrq" id="changeStatusPrq{$fl.serial_prq}" serial_prq="{$fl.serial_prq}">
                        <option value="STAND-BY" selected>Pendiente</option>
                        <option value="APROVED">Aprobado</option>
                        <option value="DENIED">Negado</option>
                    </select>
                    {else}
						{if $fl.status_prq eq "DENIED"}<font color="red">{/if}
                        {$global_paymentRequestStatus[$fl.status_prq]}
                        {if $fl.status_prq eq "DENIED"}</font>{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
					{if $fl.status_prq eq "STAND-BY"}
						<input type="hidden" name="authorized_amounts_{$fl.serial_prq}" id="authorized_amounts_{$fl.serial_prq}" />
						<input type="hidden" name="presented_amounts_{$fl.serial_prq}" id="presented_amounts_{$fl.serial_prq}" />
						<a id="editDocument{$fl.serial_prq}" style="cursor: pointer" serial_dbf="{$fl.serial_dbf}" serial_prq="{$fl.serial_prq}">Ver</a>
                    {else}
						<a id="viewDocument{$fl.serial_dbf}" style="cursor: pointer" serial_dbf="{$fl.serial_dbf}">Ver</a>
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {$fl.observations_prq|htmlall}
                </td>
				<td align="center" class="tableField">
                    {$fl.type_prq}
                </td>
                <td align="center" class="tableField">
                    <a href="{$document_root}{$fl.url_dbf}" target="blank">Abrir</a>
                </td>
            </tr>
            {/foreach}
            {/if}
            {if $acces_permition neq "view"}
            <tr style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9">
                <td align="center" class="tableField">
                    <input type="text" style="width: 70px;" name="txtFileName" id="txtFileName" />
                </td>
                <td align="center" class="tableField">
                    <input type="text" style="width: 70px;" name="txtFileDescription" id="txtFileDescription" />
                </td>
                <td align="center" class="tableField">
                    <input type="text" style="width: 50px;" name="txtTotalPresentedAmount" id="txtTotalPresentedAmount" value="0.00" disabled />
                </td>
                <td align="center" class="tableField">
                    <input type="text" style="width: 50px;" name="txtTotalAuthorizadAmount" id="txtTotalAuthorizadAmount" value="0.00" disabled />
                </td>
                <td align="center" class="tableField">
                    <select name="selBillTo" id="selBillTo">
                        <option value="">- Seleccione -</option>
                        <option value="CLIENT">Cliente</option>
                        <option value="PROVIDER">Coordinador</option>
                        <option value="DIRECT">Directo</option>
						<option value="NONE">No Aplica</option>
                    </select>
                </td>
                <td align="center" class="tableField">
                    <input type="text" style="width: 70px;" name="txtDocumentDate" id="txtDocumentDate" value="" />
                </td>
                <td align="center" class="tableField">
                    <select name="selStatusPrq" id="selStatusPrq">
                        <option value="">- Seleccione -</option>
                        <option value="STAND-BY">Pendiente</option>
                        <option value="APROVED">Aprobado</option>
                        <option value="DENIED">Negado</option>
                    </select>
                </td>
                <td align="center" class="tableField">
                    <input type="hidden" name="filesToUpload" id="filesToUpload" />
                    <input type="hidden" name="authorized_amounts" id="authorized_amounts" />
					<input type="hidden" name="presented_amounts" id="presented_amounts" />
                    <a id="benefitsAssign" style="cursor: pointer">Asignar</a>
                </td>
                <td align="center" class="tableField">
                    <input type="text" style="width: 90px;" name="txtObservations" id="txtObservations" />
                </td>
				<td align="center" class="tableField">
                    <select name="selTypePrq" id="selTypePrq">
                        <option value="">- Seleccione -</option>
						{foreach from=$types_prq item="name" key="key"}
                        <option value="{$key}">{$name}</option>
						{/foreach}
                    </select>
                </td>
                <td align="center" class="tableField">
                    <a id="fileAddress" style="cursor: pointer">Examinar</a>
                </td>
            </tr>
            <tr  style="padding:5px 5px 5px 5px; text-align:left;">
                <td colspan="10">&nbsp;</td>
                <td style="text-align:center;">
                    <input id="uploadButton" type="button" value="Subir"/>
                    <input type="hidden" name="hdnToday" id="hdnToday" value="{$data.date}" />
                </td>
            </tr>
            {/if}
            </tbody>
        </table>
    </div>
{else}
<center>No se ha subido ning&uacute;n archivo.</center>
{/if}

{literal}
<script type="text/javascript">
    if($('#txtDocumentDate').length > 0){
        setDefaultCalendar($('#txtDocumentDate'),'-1y','+0d', $('#hdnToday').val());
    }
        
    if($('#hdnAccesPermition').val()!= "view") {
        var upload = new AjaxUpload('fileAddress', {
            action: document_root+"rpc/assistance/uploadFile.rpc",
            autoSubmit: false,
            onChange: function(file, extension){
                $('#fileAddress').text(file);
            },
            onSubmit : function(file , ext){
                if (! (ext && /^(doc|docx|xls|xlsx|pdf|txt|jpg|jpeg|gif|bmp|png)$/i.test(ext))){
                    alert('Error: la extensi\u00F3n del archivo no es permitida');
                    $('#fileAddress').text('Examinar');
                    $("#uploading_file").dialog('close');
                    return false;
                } else {
                    $('#fileAddress').text('Cargando ' + file);
                }
            },
            onComplete : function(file,response){
                $("#uploading_file").dialog('close');
                if(response == "success") {
                    $('#liquidationInnerMessage').addClass('success');
                    $('#liquidationInnerMessage').html("Archivo subido exitosamente.");
                    $('#liquidationMessage').show();
                } else if(response == "noBenefits"){
                    $('#liquidationInnerMessage').addClass('error');
                    $('#liquidationInnerMessage').html("Debe adjuntar al menos un beneficio al archivo.");
                    $('#liquidationMessage').show();
                } else {
                    $('#liquidationInnerMessage').addClass('error');
                    $('#liquidationInnerMessage').html("Hubieron errores al subir el archivo "+file+".");
                    $('#liquidationMessage').show();
                }
                $('#uploadFiles').load(document_root+"rpc/assistance/loadUploadedFiles.rpc", {serial_fle: $('#txtSerialFle').val(),serial_spv:$("#selProviders").val()});
                setTimeout(function(){
                    $('#liquidationMessage').hide("slide", {direction: "up"}, 2000);
                },2000);
            }
        });

        $("#uploadButton").bind("click", function(){
            //Validation
            var valid = true;
            //RULES
            var number = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
            //MESSAGES
            $('#alertsUpload').html("");
            if($('#txtFileName').val() == "") { //Validates if the file name field is not empty
                $('#alertsUpload').append('<li><b>El campo "Documento" es obligatorio.</b></li>');
                valid=false;
            }

            if($('#txtTotalPresentedAmount').val() == "") { //Validates if the amount field is not empty
                $('#alertsUpload').append('<li><b>El campo "Monto Factura" es obligatorio.</b></li>');
                valid=false;
            }

            if(!number.test($('#txtTotalPresentedAmount').val()) && $('#txtTotalPresentedAmount').val() != "") { //Validates if the amount field contains only numbers
                $('#alertsUpload').append('<li><b>El campo "Monto Factura" admite solo n\u00FAmeros.</b></li>');
                valid=false;
            }

            if(!number.test($('#txtTotalAuthorizadAmount').val()) && $('#txtTotalAuthorizadAmount').val() != "") { //Validates if the amount field contains only numbers
                $('#alertsUpload').append('<li><b>El campo "Monto Aprovado" admite solo n\u00FAmeros.</b></li>');
                valid=false;
            }

			if($('#selStatusPrq option:selected').val() == "") { //Validates if the field selStatusPrq is not empty
				$('#alertsUpload').append('<li><b>El campo "Estado" admite solo n\u00FAmeros.</b></li>');
                valid=false;
			}

            if($('#selProvidersUpload').val() == "") { //Validates if the field selProvidersUpload is not empty
                $('#alertsUpload').append('<li><b>El campo "Coordinador" es obligatorio.</b></li>');
                valid=false;
            }

            if($('#selBillTo').val() == "") { //Validates if the field selBillTo is not empty
                $('#alertsUpload').append('<li><b>El campo "Pago a" es obligatorio.</b></li>');
                valid=false;
            }
			
			if($('#selTypePrq').val() == "") { //Validates if the field selBillTo is not empty
                $('#alertsUpload').append('<li><b>El campo "Tipo de Archivo" es obligatorio.</b></li>');
                valid=false;
            }

            if($('#fileAddress').text() == "Examinar") { //Validates if a file is selected
                $('#alertsUpload').append('<li><b>Seleccione un archivo.</b></li>');
                valid=false;
            }
			
			if($("input:checked[id^='chkBenUp']").length == 0) { //Validates if a benefit is selected
                $('#alertsUpload').append('<li><b>Seleccione al menos un beneficio.</b></li>');
                valid=false;
            }
                
            if($('#txtDocumentDate').val() == "") { //Validates if the field txtDocumentDate is not empty
                $('#alertsUpload').append('<li><b>El campo "Fecha del Documento" es obligatorio.</b></li>');
                valid=false;
            }   

            if(valid) {
                $('#alertsUpload').css("display", "none");
                $("#messageText").html('Subiendo documento.');
                $("#uploading_file").dialog('open');
                upload.setData({
                    serial_fle: $('#txtSerialFle').val(),
                    name: $('#txtFileName').val(),
                    description: $('#txtFileDescription').val(),
                    txtTotalPresentedAmount: $('#txtTotalPresentedAmount').val(),
                    totalAuthorizadAmount: $('#txtTotalAuthorizadAmount').val(),
                    billTo: $('#selBillTo').val(),
                    serial_bxp: $('#filesToUpload').val(),
                    authorized_amount: $('#authorized_amounts').val(),
					presented_amount: $('#presented_amounts').val(),
                    status_prq: $('#selStatusPrq').val(),
                    observations: $('#txtObservations').val(),
					type_prq: $('#selTypePrq').val(),
                    document_date: $('#txtDocumentDate').val()
                });
                upload.submit();
                $("input:checked[id^='chkBenUp']").each(function(){
                    $('#txtPresentedAmount'+this.value).val('');
					$('#txtAuthorizadAmount'+this.value).val('');
                    $('#txtPresentedAmount'+this.value).attr("disabled",true);
					$('#txtAuthorizadAmount'+this.value).attr("disabled",true);
                    $(this).attr("checked", false);
                });
                $('#selAll').attr("checked", false);
            } else {
                $('#alertsUpload').css("display", "block");
            }
        });

        $('#benefitsAssign ').bind("click", function(){
			if($('#selStatusPrq option:selected').val() == "") {
				alert("Debe seleccionar el estado primero.");
			} else {
				$('#fileBenefitsDialog').dialog('open');
			}
        });

		$('#selStatusPrq').bind('change',function(){
			$("input:checked[id^='chkBenUp']").each(function(){
				$('#txtPresentedAmount'+this.value).val('');
				$('#txtAuthorizadAmount'+this.value).val('');
				$('#txtPresentedAmount'+this.value).attr("disabled",true);
				$('#txtAuthorizadAmount'+this.value).attr("disabled",true);
				$(this).attr("checked",false);
				$('#selAll').attr("checked",false);
			});
			$('#txtTotalAuthorizadAmount').val('0.00');
			$('#txtTotalPresentedAmount').val('0.00');
			$('#authorized_amounts').val('');
			$('#presented_amounts').val('');
		});

    }

    $("select[id^='changeStatusPrq']").bind("change", function(){
        if(this.value=="APROVED" || this.value=="DENIED") {
			show_ajax_loader();
            $.getJSON(document_root+"rpc/assistance/updatePaymentRequestStatus.rpc", 
				{
					serial_prq:$(this).attr('serial_prq'), 
					status_prq:this.value
				}, function(data){
					if(data) {
						$('#uploadFiles').load(document_root+"rpc/assistance/loadUploadedFiles.rpc", 
							{
								serial_fle: $('#txtSerialFle').val(),
								serial_spv:$("#selProviders").val()
							}, function(){
								hide_ajax_loader();
							});
					} else {
						hide_ajax_loader();
						alert("Ha ocurrido un error.");
					}
            });
        }
    });

    $("a[id^='viewDocument']").bind("click", function(){
		$("#viewBenefitsUploadContainer").html("<center><img src = '"+document_root+"img/loading_small.gif'>Cargando Beneficios</center>");
        $("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{serial_dbf:$(this).attr("serial_dbf")});
        $("#fileViewBenefitsDialog").dialog("open");
    });

	$("a[id^='editDocument']").bind("click", function(){
		$("#viewBenefitsUploadContainer").html("<center><img src = '"+document_root+"img/loading_small.gif'>Cargando Beneficios.</center>");
        $("#viewBenefitsUploadContainer").load(document_root+"rpc/assistance/loadBenefitsByDocument.rpc",{serial_dbf:$(this).attr("serial_dbf"), serial_prq:$(this).attr("serial_prq"),edit:"edit"});
        $("#fileViewBenefitsDialog").dialog("open");
    });
</script>
{/literal}