<div id="loadFileDialog">
    <form name="frmEditFile" id="frmEditFile" method="post" action="{$document_root}modules/assistance/pUpdateAssistance" class="form_smarty">
        <input type="hidden" name="hdnStatusFile" id="hdnStatusFile" value="{$status}" />
		<input type="hidden" name="hdnCardNumber" id="hdnCardNumber" value="{$card_number}"/>
        <div class="span-23 last title ">
            <div class="span-23 last" id="fileNumber">Expediente {$data.serial_fle}</div><br />
            <input type="hidden" name="hdnSerialFle" id="hdnSerialFle" value="{$data.serial_fle}" />
            <label>(*) Campos Obligatorios</label>
        </div>

        <div class="span-6 span-10 prepend-7 last">
            <ul id="alertsDialog" class="alerts"></ul>
        </div>

        <!--LEFT SECTION-->
        <div class="span-11" id="dataTab">
            <!--LOCATION DATA-->
            <div class="span-12" id="locationData">
                <div class="span-12 last line separator">
                    <label>Datos de ubicaci&oacute;n:</label>
                </div>


                <div class="span-10 last line">
                    <div class="span-6">
                        <label>* Pa&iacute;s:</label>
                    </div>
                    <div class="span-4 last">
                        {if $status eq "closed"}
                            {$data.name_cou|htmlall}
                            <input type="hidden" name="selCountry" id="selCountry" value="{$data.serial_cou}"/>
                        {else}
                        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$countryList item="cl"}
                                <option value="{$cl.serial_cou}" {if $data.serial_cou eq $cl.serial_cou}selected{/if}>{$cl.name_cou}</option>
                            {/foreach}
                        </select>
                        {/if}
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-6">
                        <label>* Ciudad:</label>
                    </div>
					<div class="span-4 last line">
						<div class="span-4 last line" id="cityContainer">
							{if $status eq "closed"}
								{$data.name_cit|htmlall}
							{else}
							<select name="selCity" id="selCity"  title='El campo "Ciudad" es obligatorio.'>
								<option value="">- Seleccione -</option>
								{foreach from=$cityList item="c"}
									<option value="{$c.serial_cit}" {if $data.serial_cit eq $c.serial_cit}selected{/if}>{$c.name_cit}</option>
								{/foreach}
							</select>
							{/if}
						</div>
						{if $status neq "closed"}
						<div class="span-4 last line center">
							<a href="#" id="newCity">Nueva Ciudad</a>
						</div>
						{/if}
					</div>
                </div>

                <div class="span-12 last line separator">
                    <label>Datos del contacto:</label>
                </div>

                <div class="span-12 last">
                    <ul id="alertsLocation" class="alerts"></ul>
                </div>

                <div class="prepend-4 span-6 last line" id="displayContactFormContainer">
                    <a id="displayContactForm" style="cursor: pointer;{if $status eq "closed"}display: none;{/if}">A&ntilde;adir nuevo contacto</a>
                </div>

                <div class="span-10 last line" id="contactFormContainer" style="display: none;">
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>*Nombre del contactante:</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactName" id="contactName" title='El campo "Nombre del contactante" es obligatorio.' />
                        </div>
                    </div>
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>*Tel&eacute;fono fijo:</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactPhone" id="contactPhone" title='El campo "Tel&eacute;fono fijo" es obligatorio.' />
							<input type="hidden" name="contactPhoneRequired" id="contactPhoneRequired" value="" />
						</div>
                    </div>
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>*Tel&eacute;fono movil:</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactMobilePhone" id="contactMobilePhone" />
                        </div>
                    </div>
                    <div class="span-10 last line">
                        <div class="span-6">
                            <label>Tel&eacute;fono (otro):</label>
                        </div>
                        <div class="span-4 last">
                            <input type="text" name="contactOtherPhone" id="contactOtherPhone" />
                        </div>
                    </div>

                    <div class="span-10 last line">
                        <div class="span-3">
                            <label>*Direcci&oacute;n y  referencias:</label>
                        </div>
                        <div class="span-7 last">
                            <textarea style="height: 45px; width: 300px;" name="txtContactReference" id="txtContactReference" title='El campo "Ubicaci&oacute;n y  referencias" es obligatorio.'></textarea>
                        </div>
                    </div>
                    <div class="prepend-9 span-1 last line">
                        <input type="button" name="btnAddLocation" id="btnAddLocation" value="A&ntilde;adir" />
                    </div>

					<div class="prepend-5 span-5 last line">
						<a id="btnCancelLocation" style="cursor: pointer;{if $status eq "closed"}display: none;{/if}">Cancelar</a>
					</div>
                </div>
				<input type="hidden" id="hdnHasLocation" title="Debe registrar al menos un contacto"/>
                <div class="span-12 last line" id="locationContainer">
                    {if $contact_info}
                    <center>
                        <div class="span-12 last line">
                            <table border="0" class="paginate-1 max-pages-7" id="location_table">
                                <thead>
                                    <tr bgcolor="#284787">
                                        {foreach name="titles" from=$titles item="t"}
                                            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                                {$t}
                                            </td>
                                        {/foreach}
                                    </tr>
                                </thead>

                                <tbody>
                                {foreach name="contact_info" from=$contact_info item="ci"}
                                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.contact_info.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$smarty.foreach.contact_info.iteration}
                                    </td>
                                     <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.name|htmlall}
                                    </td>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.phone}
                                    </td>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.address|htmlall}
                                    </td>
                                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                        {$ci.date}
                                    </td>
                                </tr>
                                {/foreach}

                                </tbody>
                            </table>
                        </div>
                         <div class="span-11 last pageNavPosition" id="pageNavPositionLocation"></div>
                    </center>
                    {/if}
                </div>

                <div class="prepend-4 span-6 last line">
                    <input type="button" id="nextTab" value="Datos de cobertura &raquo;" />
                </div>
            </div>
            <!--END LOCATION DATA-->

            <!--BENEFITS DATA-->
            <div class="span-12" id="benefitsData" style="display: none;">
                <div class="span-12 last line separator">
                    <label>Datos de cobertura:</label>
                </div>

                <div class="span-12 last line" id="benefitsMessages" style="display: none;">
                    <div class="prepend-2 span-8 append-2 last">
                        <div class="span-8" align="center" id="benefitsInnerMessage"></div>
                    </div>
                </div>

				<div class="span-12 last line">
                    <div class="prepend-3 span-4 last line specifications">
                        <a style="cursor: pointer" id="displayUsedProviders">Ver Coordinadores Utilizados</a>
                    </div>
                </div>

                <div class="span-12 last line">
                    <div class="span-4">
                        <label>Tipo de servicio:</label>
                    </div>
                    <div class="span-3" id="serviceTypeContainer">
						{if $data.service_type}
							{$global_serviceProviderType[$data.service_type]}
							<input type="hidden" id="selServiceType" name="selServiceType" value="{$data.service_type}" />
						{else}
						<select name="selServiceType" id="selServiceType" >
							<option value="">- Seleccione -</option>
							{foreach from=$serviceTypes item="l"}
								{if $l neq 'BOTH'}
								<option value="{$l}">{$global_serviceProviderType.$l}</option>
								{/if}
							{/foreach}
						</select>
						{/if}
					</div>
                    
                </div>

                <div class="span-12 last line">
                    <div class="span-7 append-5">
                        <div class="span-4">
                            <label>Coordinador:</label>
                        </div>
                        <div class="span-2" id="providerContainer">
                            <select name="selProvider" id="selProvider" >
                                <option value="">- Seleccione -</option>
								{if $data.service_type}
									{foreach from=$providerList item="sp"}
										<option value="{$sp.serial_spv}">{$sp.name_spv}</option>
									{/foreach}
								{/if}
                            </select>
                        </div>
                    </div>
                    <div class="prepend-4 span-1 last line specifications">
                        <a style="cursor: pointer" id="displayProviderInfo">Informaci&oacute;n</a>
                    </div>
                </div>

                <div class="span-12 last line" id="medicalType" {if $data.service_type neq 'MEDICAL'}style="display: none;"{/if} {if $status eq "closed"}disabled{/if}>
                    <div class="span-4">
                        <label>Coordinaci&oacute;n m&eacute;dica:</label>
                    </div>
                    <div class="span-3">
						{if $status eq "closed"}
							{$global_medicalType[$data.medical_type]}
						{else}
                        <select name="selMedicalType" id="selMedicalType" title="El campo 'Coordinaci&oacute;n m&eacute;dica' es obligatorio">
                            <option value="">- Seleccione -</option>
                            {foreach from=$medicalTypes item="l"}
                                <option value="{$l}" {if $data.medical_type eq $l}selected{/if}>{$global_medicalType.$l}</option>
                            {/foreach}
                        </select>
						{/if}
                    </div>
                </div>

                <div class="span-12 last line" id="benefitsContainerFile">
                    <center>Seleccione un coordinador.</center>
                </div>

                <div class="prepend-4  span-6">
                    <input type="button" id="prevTab" value="&laquo; Datos de ubicaci&oacute;n" />
                </div>
            </div>
            <!--END BENEFITS DATA-->
        </div>
        <!--END LEFT SECTION-->

        <!--RIGHT SECTION-->
        <div class="prepend-2 span-11 last line">
            <!--MEDICAL DATA-->
            <div class="span-11 last line">
                <div class="span-11 last line separator">
                    <label>Datos m&eacute;dicos:</label>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>* Tipo de asistencia:</label>
                    </div>
                    <div class="span-6 last">
                        {if $status eq "closed"}
							{$data.type_fle|htmlall}
                        {else}
						 <select name="txtAssistanceType" id="txtAssistanceType" title='El campo "Tipo de asistencia" es obligatorio.' >
                            <option value="">- Seleccione -</option>
                            {foreach from=$fileTypes item="ft"}
                                <option value="{$ft}" {if $data.type_fle eq $ft}selected{/if}>{$global_fileTypes[$ft]}</option>
                            {/foreach}
                        </select>
                       
                        {/if}
                    </div>
                </div>

				<div class="span-10 last line">
                    <div class="span-4">
                        <label>* Fecha de ocurrencia:</label>
                    </div>
                    <div class="span-6 last">
                        {if $status eq "closed"}
							{$data.incident_date_fle}
						{else}
                        <input type="text" name="txtIncidentDate" id="txtIncidentDate" value="{$data.incident_date_fle}" title='El campo "Fecha de ocurrencia" es obligatorio.' />
                        {/if}
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>* Diagn&oacute;stico:</label>
                    </div>
                    <div class="span-6 last">
                        {if $status eq "closed"}
							{$data.diagnosis_fle|htmlall}
                        {else}
                        <input type="text" name="txtDiagnosis" id="txtDiagnosis" value="{$data.diagnosis_fle|htmlall}" title='El campo "Diagn&oacute;stico" es obligatorio.' />
                        {/if}
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>Causa:</label>
                    </div>
                    <div class="span-6 last">
                        <textarea name="txtCause" id="txtCause" style="height: 35px; width: 275px;" {if $status eq "closed"}readonly{/if}>{$data.cause_fle|htmlall}</textarea>
                    </div>
                </div>

                <div class="span-10 last line">
                    <div class="span-4">
                        <label>Antecedentes m&eacute;dicos:</label>
                    </div>
                    <div class="span-6 last">
                        <textarea name="txtMedicalBackground" id="txtMedicalBackground" style="height: 35px; width: 275px;" {if $status eq "closed"}readonly{/if}>{$data.medical_background_cus|htmlall}</textarea>
                    </div>
                </div>
            </div>
            <!--END MEDICAL DATA-->
            <div class="span-11 last line">
                <!--CLIENT BLOG-->
                <div class="span-11 last line" id="clientBlog">
                    <div class="span-11 last line separator">
                        <label>Bit&aacute;cora Cliente:</label>
                    </div>
                    <div class="span-11">
                        <div class="span-11 last line">
                            <textarea readonly name="txtClientBlog" id="txtClientBlog" style="background-color: #d7e8f9;">{$client_blog|strip_tags:true}</textarea>
							<input type="hidden" name="blogHeader" id="blogHeader" value="{$blogHeader|strip_tags:true}">
                        </div>
                        {if $status neq "closed"}
                        <div class="span-11 last line">
                            <textarea style="height: 30px; width: 315px; background-color: #d7e8f9;" name="txtInputClientBlog" id="txtInputClientBlog"></textarea>
                            <input type="button" id="btnClientBlog" value="Agregar" style="height: 30px;" />
                        </div>
                        {/if}
                    </div>
                    <div class="span-11">
                        <input type="button" id="btnOperator" value="Operador" />
                    </div>
                </div>
					 <!--CLIENT BLOG VALIDATION-->
					<input type="hidden" id="hdnClientCharacters"/>
					<input type="hidden" id="hdnClientValidation" title="Debe ingresar informaci&oacute;n en la Bit&aacute;cora de Cliente"/>
					 <!--END CLIENT BLOG VALIDATION-->
                <!--END CLIENT BLOG-->

                <!--OPERATOR BLOG-->
                <div class="span-11 last line operatorBlog" id="operatorBlog" style="display: none;">
                    <div class="span-11 last line separator">
                        <label>Bit&aacute;cora Operador:</label>
                    </div>
                    <div class="span-11">
                        <div class="span-11 last line">
                            <textarea readonly name="txtOperatorBlog" id="txtOperatorBlog" style="background-color: #FFDFFF;">{$operator_blog|strip_tags:true}</textarea>
                        </div>
                        {if $status neq "closed"}
                        <div class="span-11 last line">
                            <textarea style="height: 30px; width: 315px; background-color: #FFDFFF;" name="txtInputOperatorBlog" id="txtInputOperatorBlog"></textarea>
                            <input type="button" id="btnOperatorBlog" value="Agregar" style="height: 30px;" />
                        </div>
                        {/if}
                    </div>
                    <div class="span-11">
                        <input type="button" id="btnClient" value="Cliente" />
                    </div>
                </div>
					<!--OPERATOR BLOG VALIDATION-->
					<input type="hidden" id="hdnOperatorCharacters"/>
					<input type="hidden" id="hdnOperatorValidation" title="Debe ingresar informaci&oacute;n en la Bit&aacute;cora de Operador"/>
					<!--END OPERATOR BLOG VALIDATION-->
                <!--END OPERATOR BLOG-->
            </div>
            {if not $status eq "closed"}
            <a id="newAlert" style="cursor: pointer; text-decoration:none; font-weight: bold;">
                <img src="{$document_root}img/createAlert.gif" />
                Crear alerta
            </a>
            <br />
            <a id="viewAlert" style="cursor: pointer; text-decoration:none; font-weight: bold;">
                <img src="{$document_root}img/createAlert.gif" />
                Ver alertas
            </a>
            {/if}
        </div>
        <!--END RIGHT SECTION-->
    </form>
	<input type="hidden" id="reopen_permission" value="{$reopen_permission}" />
	<div id="newCityContainer">
		<div class="span-10 last line">
			<div class="span-10 center last line">Ingreso de Nueva Ciudad</div>
			
			<div class="span-10 last line">
				<div class="span-4">* Nombre: </div>
				<div class="span-5">
					<input type="text" name="txtNameCity" id="txtNameCity" />
				</div>
			</div>
			
			<div class="span-10 last line">
				<div class="span-4">* C&oacute;digo: </div>
				<div class="span-5">
					<input type="text" name="txtCodeCity" id="txtCodeCity" />
				</div>
			</div>
		</div>
	</div>
</div>

{literal}
<script type="text/javascript">
	hide_ajax_loader();
	
	/*INCIDENT DATE CALENDAR*/
	setDefaultCalendar($("#txtIncidentDate"),'-2y','+0d','0');
	/*END INCIDENT DATE CALENDAR*/
	
	/*Function to validate if there is at least one contact registered
	 *This function is called before validate and submit the frmEditFile form */
	function checkContactInfo(){
		if($.trim($('#locationContainer').html()).length > 0){
					$('#hdnHasLocation').val(1);
		}else{
			$('#hdnHasLocation').val('');
		}	
	}
	/*Function to validate if there is at least one change in each blog
	 *This function is called before validate and submit the frmEditFile form */
	function checkBlogChanges(){
		//check operator blog 
		if($('#hdnOperatorCharacters').val() < $('#txtOperatorBlog').val().length){
				$('#hdnOperatorValidation').val(1);
		}else{
				$('#hdnOperatorValidation').val('');
		}
		//check operator blog 
		if($('#hdnClientCharacters').val() < $('#txtClientBlog').val().length){
			$('#hdnClientValidation').val(1);
		}else{
			$('#hdnClientValidation').val('');
		}
	}

	/*VALIDATION SECTION*/
    $("#frmEditFile").validate({
        errorLabelContainer: "#alertsDialog",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selCountry: {
                required: true
            },
            selCity: {
                required: true
            },
            txtDiagnosis: {
                required: true
            },
            txtAssistanceType: {
                required: true
            },
			txtIncidentDate: {
				required: true,
				date: true
			},
			selMedicalType: {
				required: function() {
					if($('#medicalType').css("display") == 'none')
						return false;
					else
						return true;
				}
			},
			hdnHasLocation:{
				required: true
			},
			hdnClientValidation:{
				required: true
			},
			hdnOperatorValidation:{
				required: true
			}
        }	
    });
    /*END VALIDATION SECTION*/

    var browserCheck = (document.all) ? 1 : 0; // ternary operator statements to check if IE. set 1 if is, set 0 if not.

    if($("#location_table").length>0) {
        pager = new Pager('location_table', 2 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPositionLocation'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    if($('#hdnStatusFile').val()=='closed') {
        $("#loadFileDialog").dialog({
                bgiframe: true,
                autoOpen: true,
                autoResize:true,
                height: 720,
                width: 1000,
                modal: true,
                resizable: false,
                closeOnEscape: false,
                open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
                title:"Actualizar Expediente",
                buttons: {
                    'Salir': function() {
						$(this).dialog('close');
						show_ajax_loader();
                        location.href=document_root+"modules/assistance/fEditAssistance";
                    },
                    'Reabrir Expediente': function() {
						var reopen_permission = $('#reopen_permission').val();

						if(reopen_permission == 'allowed'){
							if(confirm("Est\u00E1 seguro de reabrir el expediente?")) {
								show_ajax_loader();
								$.getJSON(document_root+"rpc/assistance/reopenFile.rpc", {serial_fle:$('#hdnSerialFle').val()}, function(data){
									if(data) {
										location.href=document_root+"modules/assistance/fEditAssistance/3";
									} else {
										location.href=document_root+"modules/assistance/fEditAssistance/2";
									}
								});
							}
						}else{
							alert('Lo sentimos. Su usario no tiene permisos para reabrir el expediente.');
						}
                    }
                }
        });
    } else {
        $("#loadFileDialog").dialog({
                bgiframe: true,
                autoOpen: true,
                autoResize:true,
                height: 720,
                width: 1000,
                modal: true,
                resizable: false,
                closeOnEscape: false,
                open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();},
                title:"Actualizar Expediente",
                buttons: {
                    'Guardar y Salir': function() {
						checkContactInfo();//check if there is contact info registered
						checkBlogChanges();//check if there are changes on blogs
						if($("#frmEditFile").valid()) {
							show_ajax_loader();
							$("#frmEditFile").submit();
						}
                    }/*,
					'Enviar e-mail': function() {
						show_ajax_loader();
						if($('#txtDiagnosis').val()) {
							$('#messageText').html('Enviando e-mail');
							$('#uploading_file').dialog('open');
							$.getJSON(document_root+"rpc/assistance/sendInformationEmail.rpc",
							{
							  card_number: $('#hdnCardNumber').val(),
							  serial_fle: $('#hdnSerialFle').val(),
							  diagnosis: $('#txtDiagnosis').val()
							}, function(data){
								hide_ajax_loader();
								$('#uploading_file').dialog('close');
								if(data) {
									alert("E-mail enviado al supervisor de liquidaciones exitosamente.");
								} else {
									alert("Error al enviar e-mail al supervisor de liquidaciones.");
								}
							});
						} else {
							hide_ajax_loader();
							alert("Para poder enviar el e-mail informativo debe ingresar el diagn\u00F3stico de la asistencia.");
						}
					}*/
                }
        });
    }
    

    /*BLOGS*/
    $("#txtClientBlog").scrollTop(50000);
    $("#txtOperatorBlog").scrollTop(50000);

    $("#btnOperator").bind("click",function(){
        $("#clientBlog").hide("slow");
        $("#operatorBlog").show("slow");
    });
    $("#btnClient").bind("click",function(){
        $("#operatorBlog").hide("slow");
        $("#clientBlog").show("slow");
    });
	var bandHeaderInsCli=0;
    $("#btnClientBlog").bind("click",function(){
        if($("#txtInputClientBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsCli==0 && $("#blogHeader").val() != ""){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n");
                $("#txtClientBlog").append(specialchars($("#txtInputClientBlog").val()));
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n\n");
				bandHeaderInsCli=1;
            } else {//other browsers
				if(bandHeaderInsCli==0 && $("#blogHeader").val() != ""){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").append(specialchars("\n"+$("#txtInputClientBlog").val())+"\n\n");
				bandHeaderInsCli=1;
            }
            $("#txtInputClientBlog").val("");
            $("#txtClientBlog").scrollTop(50000);
        }
    });

    $("#txtInputClientBlog").bind("keyup",function(event){
        if(event.keyCode == '13' && $("#txtInputClientBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsCli==0 && $("#blogHeader").val()!=''){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n");
                $("#txtClientBlog").append($("#txtInputClientBlog").val());
                $("#txtClientBlog").val($("#txtClientBlog").val()+"\n");
				bandHeaderInsCli=1;
            } else {//other browsers
				if(bandHeaderInsCli==0 && $("#blogHeader").val()!=''){
					$("#txtClientBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtClientBlog").append(date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtClientBlog").append("\n"+$("#txtInputClientBlog").val()+"\n");
				bandHeaderInsCli=1;
            }
            $("#txtInputClientBlog").val("");
            $("#txtClientBlog").scrollTop(50000);
        }
    });
	var bandHeaderInsOp=0;
    $("#btnOperatorBlog").bind("click",function(){
        if($("#txtInputOperatorBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("<br />"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
                $("#txtOperatorBlog").append("<br />"+specialchars($("#txtInputOperatorBlog").val()));
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
				bandHeaderInsOp=1;
            }else{//other browsers
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("\n"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").append("\n"+specialchars($("#txtInputOperatorBlog").val())+"\n");
				bandHeaderInsOp=1;
            }
            $("#txtInputOperatorBlog").val("");
            $("#txtOperatorBlog").scrollTop(50000);
        }
    });

    $("#txtInputOperatorBlog").bind("keyup",function(event){
        if(event.keyCode == '13' && $("#txtInputOperatorBlog").val() != ""){
            var date = new Date()
            if( browserCheck > 0 ) {//if IE
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("<br />"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
                $("#txtOperatorBlog").append("<br />"+$("#txtInputOperatorBlog").val());
                $("#txtOperatorBlog").val($("#txtOperatorBlog").val()+"\n");
				bandHeaderInsOp=1;
            }else{//other browsers
				if(bandHeaderInsOp==0 && $("#blogHeader").val() != ""){
					$("#txtOperatorBlog").append("\n"+$("#blogHeader").val()+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+"\n\n");
				}
                $("#txtOperatorBlog").append("\n"+date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+
                " "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+":");
                $("#txtOperatorBlog").append("\n"+$("#txtInputOperatorBlog").val());
				bandHeaderInsOp=1;
            }
            $("#txtInputOperatorBlog").val("");
            $("#txtOperatorBlog").scrollTop(50000);
        }
    });
    /*END BLOG*/

    /*DIALOG TABS*/
    $("#nextTab").bind("click",function(){
        $("#locationData").hide();
        $("#benefitsData").show();
    });
    $("#prevTab").bind("click",function(){
        $("#benefitsData").hide();
        $("#locationData").show();
    });
    /*END DIALOG TABS*/


    /*LOCATION CONTAINER*/
    $('#displayContactForm').bind("click",function(){
        $('#displayContactForm').toggle("fast");
        $('#contactFormContainer').toggle("fast");
    });

	$('#contactPhone').bind('focusout',function(){
			if(this.value != '' || $('#contactMobilePhone').val() != '') {
				$('#contactPhoneRequired').val('1');
			} else {
				$('#contactPhoneRequired').val('');
			}
		});

		$('#contactMobilePhone').bind('focusout',function(){
			if(this.value != '' || $('#contactPhone').val() != '') {
				$('#contactPhoneRequired').val('1');
			} else {
				$('#contactPhoneRequired').val('');
			}
		});

    $('#btnAddLocation').bind("click",function(){
        //Validates the required fields in locaition container
        var valid = true;
        //RULES
        var digits = /^\d+$/;
        var textOnly = /^[a-zA-Z\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DC\u00FC\u00D1\u00F1 ]+$/;

        //MESSAGES
        $('#alertsLocation').html("");
        if($('#contactName').val() == "") { //Validates if the contact name field is not empty
            $('#alertsLocation').append('<li><b>El campo "Nombre del contactante" es obligatorio.</b></li>');
            valid=false;
        } else if(!textOnly.test($('#contactName').val())) { //Validates if the contact name field cantains only text
            $('#alertsLocation').append('<li><b>El campo "Nombre del contactante" admite solo letras.</b></li>');
            valid=false;
        }
        if($('#contactPhoneRequired').val() == "") { //Validates if the phone field is not empty
            $('#alertsLocation').append('<li><b>Debe ingresar al menos un n&uacute;mero de tel&eacute;fono.</b></li>');
            valid=false;
        }
		if(!digits.test($('#contactPhone').val()) && $('#contactPhone').val() != "") { //Validates if the phone field contains only digits
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono fijo" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if(!digits.test($('#contactMobilePhone').val()) && $('#contactMobilePhone').val() != "") { //Validates if the mobile phone field contains only digits
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono movil" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if(!digits.test($('#contactOtherPhone').val()) && $('#contactOtherPhone').val() != "") { //Validates if the other phone field contains only digits
            $('#alertsLocation').append('<li><b>El campo "Tel&eacute;fono (otro)" admite solo n&uacute;meros.</b></li>');
            valid=false;
        }
        if($('#txtContactReference').val() == "") {
            $('#alertsLocation').append('<li><b>El campo "Direcci&oacute;n y referencias" es obligatorio.</b></li>');
            valid=false;
        }

        if(valid) {
            $('#alertsLocation').css("display", "none");

            var phone = $('#contactPhone').val();
            if($('#contactMobilePhone').val() != ""){
				if(phone != "") {
					phone+=" / ";
				}
                phone+=$('#contactMobilePhone').val();
            }
            if($('#contactOtherPhone').val() != ""){
                phone+=" / "+$('#contactOtherPhone').val();
            }

            $('#locationContainer').load(document_root+"rpc/assistance/addAssistanceLocations.rpc",
            {
                serial_fle: $('#hdnSerialFle').val(),
                contact_name: $('#contactName').val(),
                contact_phone: phone,
                contact_address: $('#txtContactReference').val()
            },function(){
                $('#contactName').val("");
                $('#contactPhone').val("");
                $('#contactMobilePhone').val("");
                $('#contactOtherPhone').val("");
                $('#txtContactReference').val("");
                $('#displayContactForm').toggle("fast");
                $('#contactFormContainer').toggle("fast");
            });
        } else {
            $('#alertsLocation').css("display", "block");
        }
    });

	$('#btnCancelLocation').bind('click',function(){
		$('#alertsLocation').css("display", "none");
		$('#displayContactForm').toggle("fast");
        $('#contactFormContainer').toggle("fast");
		$('#contactName').val('');
		$('#contactPhone').val('');
		$('#contactMobilePhone').val('');
		$('#contactPhoneRequired').val('');
		$('#contactOtherPhone').val('');
		$('#txtContactReference').val('');
	});
	/*END LOCATION CONTAINER*/

    /*ALERTS DIALOG*/
    $("#newAlert").bind("click", function(){
         $("#createAlertsDialog").dialog('open');
    });
    $('#viewAlert').bind("click", function(){
        $('#alertsByFileContainer').load(document_root+"rpc/assistance/loadAlertsByFile.rpc", {serial_fle:$('#hdnSerialFle').val()})
        $("#viewAlertsDialog").dialog('open');
    });
    /*END ALERTS DIALOG*/

    /*LOAD PROVIDERS*/
    if($('#hdnStatusFile').val() != 'closed') {
        $("#selCountry").bind("change", function(){
			$('#benefitsContainerFile').html('<div class="span-12 last line" id="benefitsContainerFile"><center>Seleccione un coordinador.</center></div>');
            if(this.value==""){
                    $("#selCity option[value='']").attr("selected",true);
                    $("#selProvider option[value='']").attr("selected",true);
            }
            $('#cityContainer').load(document_root+"rpc/loadCities.rpc",{serial_cou: this.value, all: true});
            $('#providerContainer').load(document_root+"rpc/loadServiceProviderByCountry.rpc",
            {serial_cou: this.value, type_spv: $('#selServiceType').val()}, function() {
				loadBenefits();
			});
        });
    }

	$('#selServiceType').bind("change", function(){
        if(this.value==""){
            $("#selProvider option[value='']").attr("selected",true);
            $("#medicalType option[value='']").attr("selected",true);
            $('#medicalType').css("display", "none");
			$("#benefitsContainerFile").html('');
        }

        $('#providerContainer').load(document_root+"rpc/loadServiceProviderByCountry.rpc",
        {serial_cou: $('#selCountry').val(), type_spv: this.value},function(){
			loadBenefits();
            if($('#selServiceType').val() == "MEDICAL") {
                $('#medicalType').css("display", "block");
            } else {
                $('#medicalType').css("display", "none");
            }
        });
    });

    $('#displayProviderInfo').bind("click", function(){
        if($('#selProvider').val() != '') {
            $('#providersInfoContainer').load(document_root+"rpc/assistance/loadContactsByServiceProvider.rpc",{serial_spv:$('#selProvider').val()});
            $("#providersInfoDialog").dialog( "option", "title", $('#selProvider option:selected').text() );
            $("#providersInfoDialog").dialog('open');
        } else {
            alert("Debe seleccionar un coordinador.");
        }
    });

	$('#displayUsedProviders').bind("click", function(){
		$('#usedProvidersContainer').load(document_root+"rpc/assistance/loadUsedProviders.rpc",{serial_fle:$("#hdnSerialFle").val()});
		$("#usedProvidersDialog").dialog('open');
    });
    /*LOAD PROVIDERS*/

	/*LOAD BENEFITS*/
	loadBenefits();
	function loadBenefits() {
		$('#selProvider').bind("change", function(){
			if(this.value != '') {
			$('#benefitsContainerFile').html("<center><img src = '"+document_root+"img/loading_small.gif'><b>Cargando Servicios</b></center>");
			$('#benefitsContainerFile').load(	document_root+"rpc/assistance/loadBenefitsByServiceProvider.rpc",
												{serial_spv:this.value,card_number:$("#hdnCardNumber").val(),serial_fle:$("#hdnSerialFle").val(),status:$("#hdnStatusFile").val()});
			} else {
				$('#benefitsContainerFile').html('<center>Seleccione un coordinador.</center>');
			}
		});
	}
	/*END LOAD BENEFITS*/

    function str_replace (search, replace, subject, count) {
            f = [].concat(search),
            r = [].concat(replace),
            s = subject,
            ra = r instanceof Array, sa = s instanceof Array;s = [].concat(s);
        if (count) {
            this.window[count] = 0;
        }
         for (i=0, sl=s.length; i < sl; i++) {
            if (s[i] === '') {
                continue;
            }
            for (j=0, fl=f.length; j < fl; j++) {temp = s[i]+'';
                repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
                s[i] = (temp).split(f[j]).join(repl);
                if (count && s[i] !== temp) {
                    this.window[count] += (temp.length-s[i].length)/f[j].length;}}
        }
        return sa ? s : s[0];
    }
    
    function moneyFormat(amount) {
        var val = parseFloat(amount);
        if (isNaN(val)) { return "0.00"; }
        if (val <= 0) { return "0.00"; }
        val += "";
        // Next two lines remove anything beyond 2 decimal places
        if (val.indexOf('.') == -1) { return val+".00"; }
        else { val = val.substring(0,val.indexOf('.')+3); }
        val = (val == Math.floor(val)) ? val + '.00' : ((val*10 ==
        Math.floor(val*10)) ? val + '0' : val);
        return val;
    }
		
	/* NEW CITY CODE */
	$("#newCityContainer").dialog({
			bgiframe: true,
			autoOpen: false,
			autoResize:true,
			height: 300,
			width: 450,
			modal: true,
			stack: true,
			resizable: false,
			title:"Nueva Ciudad",
			buttons: {
				'Salir': function() {
					cleanCityForm();
					$(this).dialog('close');
				},
				'Guardar': function(){
					var submit_dialog = false;
						
					switch(validateCityDialog()){
						case 'valid':
								submit_dialog = true;
							break;
						case 'no_name':
								alert('Ingrese el nombre de la ciudad');
							break;
						case 'no_code':
								alert('Ingrese el c\u00F3digo de la ciudad');
							break;
						case 'no_country':
								alert('Primero seleccione un pa\u00EDs');
							break;
					}
					
					if(submit_dialog){
						var serial_cou = $('#selCountry').val();
						show_ajax_loader();
					
						$.ajax({
								url: document_root+"rpc/assistance/newCity.rpc",
								type: "post",
								data: ({
									serial_cou: serial_cou,
									name_cit: function (){
										return $('#txtNameCity').val();
									},
									code_cit: function (){
										return $('#txtCodeCity').val();
									}
								}),
								success: function(data){
									var parts = data.split("%S%");
										
									if(parts[0]=='sucess'){
										$("#newCityContainer").dialog('close');
										cleanCityForm();
										
										$('#cityContainer').load(document_root+"rpc/loadCities.rpc",
											{
												serial_cou: serial_cou, 
												all: true
											}, function(){
												
													
												$("#selCity option").filter(function() {
													return $(this).val() == parts[1];
												}).attr('selected', true);
												
												hide_ajax_loader();
											});
									}else{
										hide_ajax_loader();
										alert(parts[0]);
									}
								}
						});
					}
				}
			}
	});
		
	$('#newCity').bind('click', function(){
		if($('#selCountry').val() != ''){
			$("#newCityContainer").dialog('open');
		}else{
			alert('Primero seleccione un pa\u00EDs');
		}
	});
		
	function cleanCityForm(){
		$('#txtNameCity').val('');
		$('#txtCodeCity').val('');
	}
		
	function validateCityDialog(){
		if($('#selCountry').val() != ''){
			if($('#txtCodeCity').val() == ''){
				return 'no_code';
			}else if($('#txtNameCity').val() == ''){
				return 'no_name';
			}else{
				return 'valid';
			}
		}else{
			return 'no_country';
		}
	}
</script>
{/literal}