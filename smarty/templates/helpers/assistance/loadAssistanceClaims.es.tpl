{if $claimList}
<div class="span-24 last line separator">
    <label>Reclamos pendientes:</label>
</div>
		<div class="span-24 last line" name="divAmountSum" id="divAmountSum" style="display: none">
			<div class="prepend-10 span-3">
				<label>Monto acumulado:</label>
			</div>
			<div class="span-2">
				<label name="amountSum" id="amountSum">0</label>
			</div>
		</div>

    <center>
        <div class="prepend-1 span-22 last line">
            <table border="1" class="dataTable" id="claims_table">
                <thead>
                    <tr class="header">
                        <th style="text-align: center;">
                            {if $document_to_dbf neq 'DIRECT'}
                            <input type="checkbox" name="selAll" id="selAll"/>
                            {/if}
                        </th>
                        {foreach from=$titles item="t"}
                        <th style="text-align: center;">
                            {$t}
                        </th>
                        {/foreach}
                    </tr>
                </thead>

                <tbody>
                {foreach name="claims" from=$claimList item="cl"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.claims.iteration is even} class="odd"{else}class="even"{/if}>
                    <td class="tableField">
                        {if $document_to_dbf eq 'DIRECT'}
                        <input type="radio" name="chk" id="chkClaims{$cl.serial_prq}" amount="{$cl.amount_authorized_prq}" billTo="{$cl.document_to_dbf}" serial_cus="{$cl.serial_cus}" value="{$cl.serial_prq}"/>
                        {else}
                        <input type="checkbox" name="chk" id="chkClaims{$cl.serial_prq}" amount="{$cl.amount_authorized_prq}" billTo="{$cl.document_to_dbf}" serial_cus="{$cl.serial_cus}" value="{$cl.serial_prq}"/>
                        {/if}
                    </td>
                    <td class="tableField">
                        {$cl.name_dbf}
                    </td>
					<td class="tableField">
                        {$cl.name_cou}
                    </td>
                    <td class="tableField">
                        {$cl.comment_dbf}
                    </td>
					<td class="tableField">
                        {$cl.type_prq}
                    </td>
                    <td class="tableField">
                        {$cl.amount_authorized_prq}
                    </td>
                    <td class="tableField">
                        {$cl.request_date}
                    </td>
                    <td class="tableField">
                        {$cl.document_date_dbf}
                    </td>
                    <td class="tableField">
                        {$cl.name_cus}
                    </td>
                    <td class="tableField">
                        <a style="cursor: pointer;" id="viewDocument{$cl.serial_dbf}" serial="{$cl.serial_dbf}">Ver</a>
                    </td>
                    <td class="tableField">
                        <a href="{$document_root}{$cl.url_dbf }" target="blank">Abrir</a>
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
		<input type="hidden" id="hdtItemCount" value="{$count}"/>
		<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
		<div class="prepend-1 span-22 last pageNavPosition" id="pageNavPosition"></div>
</center>
{else}
    {if $document_to_dbf eq 'DIRECT'}
        <center><b>No existen Reclamos de Pago {$global_billTo.$document_to_dbf} pendientes.</b></center><br>
    {elseif $document_to_dbf eq 'PROVIDER'}
        <center><b>No existen Reclamos de Pago pendientes para este coordinador.</b></center><br>
    {elseif $document_to_dbf eq 'CLIENT'}
        <center><b>No existen Reclamos de Pago pendientes para este cliente.</b></center><br>
    {/if}
{/if}
