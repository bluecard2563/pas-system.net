{if $invoice_list}
    <form name="frmGenerateSettlement" id="frmGenerateSettlement" action="{$document_root}modules/assistance/liquidation/generateSettlement" method="post">
        <div class="span-17 append-1 prepend-1 last line">
            <table border="0" id="invoicesTable" style="color: #000000;">
                <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            Documento No.
                        </td>
                        <td align="center" class="tableTitle">
                            Descripci&oacute;n
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha del Documento
                        </td>
                        <td align="center" class="tableTitle">
                            Pagado A
                        </td>
                         <td align="center" class="tableTitle">
                            Seleccione
                        </td>
                    </tr>
                </THEAD>
                <TBODY>
                    {foreach name="invoices" from=$invoice_list item="s"}
                        <tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                {$s.comment_dbf|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.name_dbf|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.document_date}
                            </td>
                            <td align="center" class="tableField">
                                {$global_billTo[$s.document_to_dbf]|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                <input type="checkbox" name="requestItems[]" id="chkInvoice_{$s.serial_prq}" value="{$s.serial_prq}"/>
                            </td>
                        </tr>
                    {/foreach}
                </TBODY>
            </table>
        </div>
    </form>
{else}
    <div class="span-10 append-1 prepend-1">
        <div class="span-10 error">
            Existi&oacute; un error en la carga de facturas. Por favor int&eacute;ntelo m&aacute;s tarde.
        </div>
    </div>
{/if}