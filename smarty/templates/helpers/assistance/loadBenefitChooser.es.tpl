{if $benefitsByServiceProvider}
<center>
    <div class="span-17 last line">
        <table border="0"id="benefits_upload_table">
            <thead>
                <tr bgcolor="#284787">
                    <td align="center" style="border: solid 1px white; padding:0px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">
                        <input type="checkbox" name="selAll" id="selAll" />
                    </td>
                    {foreach from=$titles item="l"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        {$l}
                    </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="benefitList" from=$benefitsByServiceProvider item="l"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.benefitList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <input type="checkbox" id="chkBenUp{$l.serial_spf}" name="serial_bxp[]" value="{$l.serial_spf}"  title="Debe seleccionar al menos un beneficio." />
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.description_bbl}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {if $l.price_bxp neq 'N/A'}${/if}{$l.price_bxp}
                    <input type="hidden" style="width: 50px;" name="maxAmount{$l.serial_spf}" id="maxAmount{$l.serial_spf}" value="{$l.price_bxp}" readonly/>
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.restriction_price_bxp}
                </td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <input type="text" style="width: 50px;" name="txtPresentedAmount{$l.serial_spf}" id="txtPresentedAmount{$l.serial_spf}" serial="{$l.serial_spf}" serial_bxp="{$l.serial_bxp}" disabled />
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <input type="text" style="width: 50px;" name="txtAuthorizadAmount{$l.serial_spf}" id="txtAuthorizadAmount{$l.serial_spf}" serial="{$l.serial_spf}" serial_bxp="{$l.serial_bxp}" disabled />
                </td>
            </tr>
            {/foreach}
            {foreach name="serviceList" from=$servicesByServiceProvider item="l"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.serviceList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <input type="checkbox" id="chkBenUp{$l.serial_spf}" name="serial_bxp[]" value="{$l.serial_spf}"  title="Debe seleccionar al menos un beneficio." />
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.name_sbl}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    ${$l.coverage_sbc}
                    <input type="hidden" style="width: 50px;" name="maxAmount{$l.serial_spf}" id="maxAmount{$l.serial_spf}" value="{$l.coverage_sbc}" readonly/>
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    -
                </td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <input type="text" style="width: 50px;" name="txtPresentedAmount{$l.serial_spf}" id="txtPresentedAmount{$l.serial_spf}" serial="{$l.serial_spf}" disabled />
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <input type="text" style="width: 50px;" name="txtAuthorizadAmount{$l.serial_spf}" id="txtAuthorizadAmount{$l.serial_spf}" serial="{$l.serial_spf}" disabled />
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
	<div class="span-17 last line pageNavPosition" id="pageNavPosition"></div>
</center>
{else}
No existen beneficios disponibles para el coordinador seleccionado.
{/if}

{literal}
<script type="text/javascript">
    if($("#benefits_upload_table").length>0) {
        pager = new Pager('benefits_upload_table', 5 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    $("#selAll").bind("click",function(){
        if($('#selAll').attr("checked")) {
            $("input[id^='chkBenUp']").attr("checked", true);
            $("input[id^='txtAuthorizadAmount']").attr("disabled",false);
			$("input[id^='txtPresentedAmount']").attr("disabled",false);
        } else {
            $("input[id^='chkBenUp']").attr("checked", false);
            $("input[id^='txtAuthorizadAmount']").attr("disabled",true);
            $("input[id^='txtAuthorizadAmount']").val('');
			$("input[id^='txtPresentedAmount']").attr("disabled",true);
            $("input[id^='txtPresentedAmount']").val('');
        }
    });

    $("input[id^='chkBenUp']").bind("click",function(){
        if(!$(this).attr("checked")) {
            $('#selAll').attr("checked", false);
            $('#txtAuthorizadAmount'+this.value).val('');
            $('#txtAuthorizadAmount'+this.value).attr("disabled",true);
			$('#txtPresentedAmount'+this.value).val('');
            $('#txtPresentedAmount'+this.value).attr("disabled",true);
        } else {
            $('#txtAuthorizadAmount'+this.value).attr("disabled",false);
			$('#txtPresentedAmount'+this.value).attr("disabled",false);
        }
    });

    $("input[id^='txtAuthorizadAmount']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }

		//Max AMount to be paid for a benefit.
		if($('#selStatusPrq option:selected').val() == "APROVED") {
			var max_amount=parseFloat($('#maxAmount'+$(this).attr('serial')).val());
			if(max_amount>0){ //If the max amount is grater than 0 (no limit)
				//Preventing of pay a grater value than the available for a specific benefit
				if(parseFloat(this.value) > max_amount) {
					alert("El valor sobrepasa el monto m\u00E1ximo disponible. Verifique los montos previamente cancelados.");
					this.value = '';
				}
			}
		}
    });

	$("input[id^='txtPresentedAmount']").bind("focusout",function(){
        if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(this.value)) {
            alert("Ingrese un valor v\u00E1lido.");
            this.value = '';
        }
    });
</script>
{/literal}