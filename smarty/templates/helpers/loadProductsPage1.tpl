 <div class="span-19 last line">
            <div class="prepend-1 span-8 label">
                <label>* Nombre del Producto :</label>
            </div>

        <div class="span-8 append-2 last ">
            <div>
                <input type="text" name="txtName_pro" id="txtName_pro" class="span-7" value="{$data.0.name_ptl}"/>
            </div>
        </div>
    </div>
    <div class="span-19 last line">
        <div class="prepend-1 span-8 label">
            <label>* Descripci&oacute;n del Producto :</label>
            </div>

        <div class="span-8 append-2 last ">
            <div>
                <textarea  name="txtDescription_pro" id="txtDescription_pro" class="span-7">{$data.0.description_ptl}</textarea>
            </div>
        </div>
    </div>
    <div class="span-19 last line">
        <div class="prepend-1 span-8 label">
            <label>* Imagen Web:</label>
        </div>

        <div class="span-8 append-2 last ">
            <div>
                <input type="file" name="image_pro" id="image_pro"/>
            </div>
        </div>
    </div>

    <div class="prepend-9 span-10 last line">
        {if $data.0.image_tpp}<img src="{$document_root}img/products/{$data.0.image_tpp}" height="60" width="60" />{/if}
        <input type="hidden" name="hdnImage_pro" id="hdnImage_pro" value="{$data.0.image_tpp}" />
    </div>

<div class="span-19 last line separator">
    	<label>Condiciones:</label>
</div>

<div class="span-19 last line">
    <div class="prepend-1 span-4 label">Duraci&oacute;n M&iacute;nima:</div>
    <div class="span-4">
    	<input type="text" name="min_duration_pro" id="min_duration_pro" class="span-2" {if $data.0.min_duration_tpp}value="{$data.0.min_duration_tpp}"{/if} /> en D&iacute;as
    </div>
    <div class="span-4 label">Duraci&oacute;n M&aacute;xima:</div>
    <div class="span-4 append-1 last">
	    <input type="text" name="max_duration_pro" id="max_duration_pro" class="span-2" {if $data.0.max_duration_tpp}value="{$data.0.max_duration_tpp}"{/if} /> en D&iacute;as
    </div>
</div>

<div class="span-19 last line">
    <div class="prepend-1 span-4 label">M&iacute;nimo Acompa&ntilde;antes:</div>
    <div class="span-4">
	    <input type="text" name="min_extras_pro" id="min_extras_pro" class="span-2" {if $data.0.min_extras_tpp}value="{$data.0.min_extras_tpp}"{/if}/>
    </div>
    <div class="span-4 label">M&aacute;ximo Acompa&ntilde;antes:</div>
    <div class="span-4 append-1 last">
    	<input type="text" name="max_extras_pro" id="max_extras_pro" class="span-2" {if $data.0.max_extras_tpp}value="{$data.0.max_extras_tpp}"{/if}/>
    </div>
</div>

<div class="span-19 last line">
    <div class="prepend-1 span-4 label">Menores gratis:</div>
    <div class="span-4">
    	<input type="text" name="children_pro" id="children_pro" class="span-2" {if $data.0.children_tpp}value="{$data.0.children_tpp}"{/if}/>
    </div>
    <div class="span-4 label">Viajes por Per&iacute;odo: </div>
    <div class="span-4 append-1 last">
	   <select id="flights_pro" name="flights_pro"><option value="0" {if $data.0.flights_tpp eq 0} selected="selected" {/if}>Varios</option><option value="1"  {if $data.0.flights_tpp eq 1} selected="selected" {/if}>1 solo viaje</option></select>
    </div>
</div>

<div class="span-19 last line">
    <div class="prepend-1 span-4 label">Comisionable y Aplica Descuentos:</div>
    <div class="span-4">
        <input type="checkbox" name="has_comision_pro" id="has_comision_pro" value="1" {if $data.0.has_comision_tpp} checked="checked" {/if}/>
    </div>
    <div class="span-4 label">Disponible en P&aacute;gina Web:</div>
    <div class="span-4 append-1 last">
        <input type="checkbox" name="in_web_pro" id="in_web_pro" value="1" {if $data.0.in_web_tpp} checked="checked" {/if}/>
    </div>
</div>

<div class="span-19 last line">
    <div class="prepend-1 span-4 label">Precio para Familiares Diferente:</div>
    <div class="span-4">
    	<input type="checkbox" name="relative_pro" id="relative_pro" value="1" {if $data.0.relative_tpp} checked="checked" {/if}/>
    </div>

    <div class="span-4 label">Precio para Conyuige Diferente:</div>
    <div class="span-4 append-1 last">
        <input type="checkbox" name="spouse_pro" id="spouse_pro" value="1" {if $data.0.spouse_tpp} checked="checked" {/if}/>
    </div>
</div>

<div class="span-19 last line">
    <div class="prepend-1 span-4 label">V&aacute;lida dentro del pa&iacute;s de emisi&oacute;n:</div>
    <div class="span-4">
        <input type="checkbox" name="emission_country_pro" id="emission_country_pro" value="1" {if $data.0.emission_country_tpp} checked="checked" {/if}/>
    </div>
    <div class="span-4 label">V&aacute;lida dentro del pa&iacute;s de residencia:</div>
    <div class="span-4 append-1 last">
        <input type="checkbox" name="residence_country_pro" id="residence_country_pro" value="1" {if $data.0.residence_country_tpp} checked="checked" {/if}/>
    </div>
</div>

<div class="span-19 last line">

    <div class="prepend-1  span-4 label"><sup>1</sup> Varias tarjetas en un solo contrato:</div>
    <div class="span-4 ">
	    <input type="checkbox" name="extras_by_contract_pro" id="extras_by_contract_pro" value="1" {if $data.0.extras_by_contract_tpp} checked="checked" {/if}/>
    </div>
	<div class="span-4 label">Es masivo</div>
    <div class="span-4 append-1 last ">
        <input type="checkbox" name="masive_pro" id="masive_pro" value="1" {if $data.0.masive_tpp} checked="checked" {/if}/>
    </div>

</div>

<div class="span-19 last line">
	<div class="prepend-1 span-4 label">Paises Schengen:</div>
    <div class="span-4 last" >
        <input type="checkbox" name="schengen_pro" id="schengen_pro" value="1" {if $data.0.schengen_tpp} checked="checked" {/if}/>
    </div>
</div>

<div class="span-19 last line">
<sup>1</sup> en caso de no seleccionar esta casilla, y existen adicionales estos se agregarán a la misma tarjeta
</div>     