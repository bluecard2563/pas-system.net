<div class="span-24 last line separator">
	<label>Cartera de Productos</label>
</div>

{if $product_list}
<form name="frmAsignProductPOS" id="frmAsignProductPOS" method="post" action="{$document_root}modules/webPOS/fRegisterPricesPOS">
	<div class="span-24 last line center">
		<label>* Seleccione el producto al que desea registrar precios.</label>
	</div>
	
	<div class="prepend-9 span-5 last line">
		<select name="selProduct" id="selProduct" title="El campo 'Producto' es obligatorio." >
			<option value="">- Seleccione -</option>
			{foreach from=$product_list item="p"}
				<option value="{$p.serial_pbd}">{$p.name_pbl|htmlall}</option>
			{/foreach}
		</select>
	</div>
	
	<div class="span-24 last line center">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar Precios" />
		<input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$serial_dea}" />
	</div>
</form>
{else}
	<div class="prepend-7 span-10 append-7">
		<div class="span-10 center error">
			No se encuentran productos registrados para este comercializador. <br />Para hacerlo haga click 
			<a target="_blank" href="{$document_root}modules/product/productByCountry/fSearchProductByCountry">aqu&iacute;</a>
		</div>
	</div>
{/if}