{if $usersList}
	<div align="center">Existentes</div>
	<select id="usersFrom{$count}" class="selectMultiple span-4 last" name="usersFrom{$count}" multiple="">
		{foreach from=$usersList item="uL"}
			<option selected value="{$uL.serial_usr}">{$uL.first_name_usr|htmlall} {$uL.last_name_usr|htmlall}</option>
		{/foreach}
	</select>
{else}
	<div align="center">Existentes</div>
	No existen usuarios en esta ciudad.
{/if}
