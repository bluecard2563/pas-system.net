{if $error eq 7}
	<div class="span-24">
		<div class="prepend-7">
			<div class="span-9 error">
				El usuario no tiene todos los permisos para modificar la alerta.
			</div>
		</div>
	</div>
{else}
	{if $error eq 8 || $error eq 9}
	<div class="span-24">
		<div class="prepend-7">
			<div class="span-9 error">
				Existi&oacute; un error al modificar el par&aacute;metro, por favor int&eacute;ntelo nuevamente.
			</div>
		</div>
	</div>
	{else}
			{foreach key=key item=pI from=$parameterInfo name=cont}
			<div id="country{$smarty.foreach.cont.iteration}" class="span-24">
				<div class="prepend-5 span-7" id="countryWrapper{$smarty.foreach.cont.iteration}">
					<div class="span-2 label">* Pa&iacute;s:</div>
					<div class="span-5 last" id="countryContainer{$smarty.foreach.cont.iteration}">
						<select name="selCountry{$smarty.foreach.cont.iteration}" id="selCountry{$smarty.foreach.cont.iteration}" title='El campo "Pa&iacute;s" es obligatorio.' count="{$smarty.foreach.cont.iteration}">
								<option value="">- Seleccione -</option>
								{foreach from=$countriesList item="cl"}
										{if $cl.serial_cou eq $pI.country}
											<option selected value="{$cl.serial_cou}">{$cl.name_cou}</option>
										{else}
											<option value="{$cl.serial_cou}">{$cl.name_cou}</option>
										{/if}
								{/foreach}
						</select>
					</div>
				</div>

				<div class="prepend-2 span-6 line" id="managerWrapper{$smarty.foreach.cont.iteration}">
					<div class="span-3 label">* Representante:</div>
					<input type="hidden" name="originalMbc{$smarty.foreach.cont.iteration}" id="originalMbc{$smarty.foreach.cont.iteration}" value="{$pI.originalMbc}">
					<div class="span-3 last" id="managerContainer{$smarty.foreach.cont.iteration}">
						<select name="selManager{$smarty.foreach.cont.iteration}" id="selManager{$smarty.foreach.cont.iteration}"  count="{$smarty.foreach.cont.iteration}">
								<option value="">- Seleccione -</option>
						</select>
					</div>
				</div>

				<div class="prepend-4 span-6 last line" id="cityWrapper{$smarty.foreach.cont.iteration}">
					<div class="span-3 label">* Ciudad:</div>
					<div class="span-3 last" id="cityContainer{$smarty.foreach.cont.iteration}">
						<select name="selCity{$smarty.foreach.cont.iteration}" id="selCity{$smarty.foreach.cont.iteration}" count="{$smarty.foreach.cont.iteration}">
								<option value="">- Seleccione -</option>
						</select>
					</div>
				</div>

				<div class="span-24 last line">
					<div class="prepend-8 span-13 append-1 last" id="usersContainer{$smarty.foreach.cont.iteration}">
							<div class="span-4 line" id="usersToContainer{$smarty.foreach.cont.iteration}">
								<div align="center">Seleccionados</div>
								<select multiple id="usersTo{$smarty.foreach.cont.iteration}" name="usersTo[{$smarty.foreach.cont.iteration}][]" class="selectMultiple span-4 last"  count="{$smarty.foreach.cont.iteration}" title='El campo "Seleccionados" es obligatorio.'>
									{foreach from=$pI.users item="uL"}
										<option selected value="{$uL.serial_usr}">{$uL.name_usr|htmlall}</option>
									{/foreach}
								</select>
							</div>

							<div class="span-2 buttonPane">
								<br />
								<input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
								<input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
								<input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
								<input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
							</div>

							<div class="span-4"  id="usersFromContainer{$smarty.foreach.cont.iteration}">
								<div align="center">Existentes</div>
								<select multiple="multiple" id="usersFrom{$smarty.foreach.cont.iteration}" name="usersFrom{$smarty.foreach.cont.iteration}" class="selectMultiple span-4 last">
								</select>
							</div>

							<div class="prepend-4 span-2 last">
								<a href="#" onclick="deleteManager('{$smarty.foreach.cont.iteration}'); return false;" id="delete{$smarty.foreach.cont.iteration}">Eliminar</a>
							</div>
					</div>
				</div>
			</div>

			{/foreach}
			<input type="hidden" name="countriesCount" id="countriesCount" value="{$parameterInfo|@count}"/>

			<div class="span-24 last line" id="countriesContainer"></div>
				<div class="span-24 last line">
					<div class="prepend-19 span-5">
						<input type="button" name="btnAddCountry" id="btnAddCountry" value="Asignar otro Representante" />
					</div>
				</div>
			<div class="span-24 last buttons line">
				<input type="submit" name="btnInsert" id="btnInsert" value="Insertar" onclick="selectAll();" />
			</div>
	{/if}
{/if}