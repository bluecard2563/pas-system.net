<form id="frmDeactivateUserDialog" name="frmDeactivateUserDialog" method="post" action="#">
{if $desc neq ''}
<div class="span-1 span-10 prepend-1 last line">
    <ul id="alerts_DeactivateUserDialog" class="alerts"></ul>
</div>
<div class="span-12 last line line title" id="warningTitle">
	   Advertencia
</div>

<div class="span-12 last  line">
&nbsp;
</div>
	{if $desc eq 'responsible'}
		<div class="span-19 last  line">
			El Usuario no puede ser deasctivado porque es Responsable de una Sucursal.
		</div>
	{/if}
	{if $desc eq 'comissionist'}
		<div class="span-19 last  line">
			El Usuario no puede ser deasctivado porque es Responsable de un Pa&iacute;s.
		</div>
	{/if}
	{if $desc eq 'audit'}
		<div class="span-19 last  line">
			El Usuario no puede ser deasctivado porque a&uacute;n tiene una auditoria pendiente.
		</div>
	{/if}

</form>
{/if}