<div class="prepend-4 span-16 append-4 last line">
    {if $kitDetailsList}
    <table border="0" class="paginate-10 max-pages-7" id="theTable" style="color: #000000;">
        <THEAD>
            <tr bgcolor="#284787">
                <td align="center" class="tableTitle">
                    Acci&oacute;n
                </td>
                <td align="center" class="tableTitle">
                    Cantidad (Kits)
                </td>
                <td align="center" class="tableTitle">
					Fecha
                </td>
            </tr>
        </THEAD>
        <TBODY>
            {foreach name="list" from=$kitDetailsList item="kl" key="k"}
            <tr {if $smarty.foreach.list.iteration is even}bgcolor="#d7e8f9"{else} bgcolor="#e8f2fb" {/if}>
                <td class="tableField">{if $kl.action_kdt eq GIVEN}Entrega{else}Devoluci&oacute;n{/if}</td>
                <td class="tableField">{$kl.amount_kdt}</td>
                <td class="tableField">{$kl.date}</td>
            </tr>
            {/foreach}
        </TBODY>
        
    </table>
    {else}
    <div class="span-24 last line">
        <b>No se han realizado entregas ni devoluciones de kits al comercializador con los par&aacute;metros que realizo la consulta.</b>
    </div>
    {/if}
</div>
<div class="span-24 last buttons line">
    <input type="submit" name="btnAssingKits" id="btnAssingKits" value="Ir a Asignaci&oacute;n de Kits" >
</div>
