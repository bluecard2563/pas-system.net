{if $countryList}
    <select name="selCountry{$serial_sel}" id="selCountry{$serial_sel}" title="El campo 'Pa&iacute;s' es obligatorio">
        <option value="">- Seleccione -</option>
        {foreach name="countries" from=$countryList item="l"}
            <option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou}</option>
        {/foreach}
    </select>
{else}
	{if $serial_zon neq ''}
    No existen paises registrados.
    {/if}
    <select name="selCountry{$serial_sel}" id="selCountry{$serial_sel}" title="El campo 'Pa&iacute;s' es obligatorio" {if $serial_zon neq ''}style="display:none;"{/if}>
        <option value="">- Seleccione -</option>
    </select>
{/if}

{literal}
<script type="text/javascript">
	$('#selCountry{/literal}{$serial_sel}{literal}').bind("change",function(){
		$('#hdnFlagCity').val($('#hdnFlagCity').val()+1);
	});
</script>
{/literal}