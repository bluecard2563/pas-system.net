{if $responsablesList}
<select name="selResponsable" id="selResponsable">
  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
  {foreach from=$responsablesList item="l"}
		<option value="{$l.serial_usr}" >{$l.first_name_usr|htmlall} {$l.last_name_usr|htmlall}</option>
  {/foreach}
</select>
{else}
	{if $opt_text}
	<select name="selResponsable" id="selResponsable">
	  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
	</select>
	{else}
	   No existen Responsables del Representante Seleccionado.
	{/if}
{/if}
