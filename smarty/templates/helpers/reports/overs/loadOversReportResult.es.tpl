{if $overs}
	<div class="span-24 last line">
	   	<div class="span-1 pushLeft">No</div>
	   	<div class="span-6 pushLeft">Nombre</div>
	   	<div class="span-3 pushLeft">Tipo</div>
	    <div class="span-7 pushLeft">Periodo</div>
	    <div class="span-2 pushLeft">Crecimiento</div>
	    <div class="span-2 pushLeft">A Pagar</div>
	</div>
	<hr />
	<div id="all-overs-list">
		{foreach from=$overs item="ove" key=cid}
			<div class="span-24 last line table_record" id="banner_{$ove.serial_apo}">
				<div class="span-1 list_text" id="num_{$ove.serial_apo}">{$cid+1}</div>
				<div class="span-6 list_text" id="name_{$ove.serial_apo}">{$ove.name_app}</div>
		    	<div class="span-3 list_text" id="type_{$ove.serial_apo}">{$ove.applied_to_ove}</div>
		    	<div class="span-7 list_text" id="per_{$ove.serial_apo}">{$ove.begin_date_ino}  al  {$ove.end_date_ino}</div>
	        	<div class="span-2 list_text" id="inc_{$ove.serial_apo}">{$ove.percentage_increase_ove}</div>
	        	<div class="span-2 last list_text" id="pay_{$ove.serial_apo}">{$ove.amount_ino}</div>
			</div>
		{/foreach}
	</div>
{else}
<div class="prepend-6 span-10 append-7 last line">
	<div class="span-12 error center">
		No existen overs pendientes o activos que tengan valores por pagar.<br>
		Ingrese otros par&aacute;metros de b&uacute;squeda.
	</div>
</div>
{/if}