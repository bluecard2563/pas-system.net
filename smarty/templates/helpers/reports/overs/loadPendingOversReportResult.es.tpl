{if $overs}
	<table border="0" id="theTable" class="span-22">
	   	<THEAD>
	       	<tr>
	            <td align="center">
	                No
	            </td>
	            <td align="center">
	                Nombre
	            </td>
	            <td align="center">
	                Representante/Sucursal
	            </td>
	            <td align="center">
	                Periodo del Over
	            </td>
	            <td align="center">
	                A&ntilde;os de Evaluaci&oacute;n
	            </td>
	            <td align="center">
	                Reporte
	            </td>
			</tr>
		</THEAD>
		<TBODY>
	
		{foreach from=$overs item="ove" key=cid}
			<tr {if $cid%2 ==0 }bgcolor="#CCCCCC"{else}bgcolor="#FFFFFF"{/if} >
				<td align="center" class="list_text">
               		{$cid+1}
               	</td>
               	<td align="center" class="list_text">
               		{$ove.name_ove}
               	</td>
               	<td align="center" class="list_text">
               		{$ove.dea_name}
               	</td>
               	<td align="center" class="list_text">
               		{$ove.begin_date_ino}  al  {$ove.end_date_ino}
               	</td>
               	<td align="center" class="list_text">
               		{$ove.evaluationYear} - {$ove.evaluationYear-1}
               	</td>
               	<td align="center" class="list_text">
               		<div class="span-2 last list_text" id="pay_{$ove.serial_apo}">
				        <input type="button" id="imgSubmitPDF_{$ove.serial_apo}" class="PDF" onclick="sendInfo('PDF',{$ove.serial_apo},{$ove.serial_ove});"/>
				        <input type="button" id="imgSubmitXLS_{$ove.serial_apo}"  class="XLS"  onclick="sendInfo('XLS',{$ove.serial_apo},{$ove.serial_ove});"/>
					</div>
               	</td>
		{/foreach}
		</TBODY>
</table>
<div class="span-17 prepend-3 last pageNavPosition" id="pageNavPosition"></div>

<script type="text/javascript">
        /*@object: Pager
         *@Description: JS library that paginates a table.
         *@Params: IDTable, Max Items per Page, Label for 'Next', Label for 'Prev'
         **/
        var pager = new Pager('theTable', '{$itemsPerPage}' , 'Siguiente', 'Anterior');
        pager.init('{$maxPages}'); //Max Pages
        alert('{$maxPages}');
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
</script>

{else}
	No existen resultados para los filtros seleccionados.
{/if}