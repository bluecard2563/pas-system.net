{if $cityList}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
        <option value="">Todos</option>
        {foreach name="cities" from=$cityList item="c"}
		<option value="{$c.serial_cit}" {if $smarty.foreach.cities.total eq 1}selected{/if} >{$c.name_cit|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio' >
      <option value="">Todos</option>
    </select>
{/if}