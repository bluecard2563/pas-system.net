{if $mbcList}
    <select name="selManager" id="selManager" class="span-4">
        {if $required}
			<option value="">- Seleccione -</option>
			<option value="0">Todos</option>
		{else}
			<option value="">Todos</option>
		{/if}
        {foreach from=$mbcList item="m"}
			<option value="{$m.serial_mbc}" {if $mbcList|@count eq 1} selected{/if}>{$m.name_man|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selManager" id="selManager" class="span-4">
      <option value="">Todos</option>
    </select>
{/if}