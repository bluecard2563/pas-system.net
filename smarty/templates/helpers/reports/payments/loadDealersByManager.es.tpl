{if $dealerList}
    <select name="selDealer" id="selDealer" class="span-7">
        <option value="">Todos</option>
        {foreach from=$dealerList item="d"}
		<option value="{$d.serial_dea}" >{$d.name_dea|htmlall} - {$d.code_dea}</option>
        {/foreach}
    </select>
{else}
    <select name="selDealer" id="selDealer">
      <option value="">Todos</option>
    </select>
{/if}