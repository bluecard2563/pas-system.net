{if $dealersList}
    <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach from=$dealersList item="d"}
			<option value="{$d.serial_dea}" >{$d.name_dea|htmlall} - {$d.code_dea}</option>
        {/foreach}
    </select>
{else}
	No existen Comercializadores registrados.
	<select name="selDealer" id="selDealer" class="hide" title='El campo "Comercializador" es obligatorio'>
        <option value="">- Seleccione -</option>
    </select>
{/if}