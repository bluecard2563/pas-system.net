{if $countryList}
    <select name="selCountry" id="selCountry">
		{if $required}
			<option value="">- Seleccione -</option>
		{else}
			<option value="">Todos</option>
		{/if}
        {foreach name="countries" from=$countryList item="c"}
		<option value="{$c.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if} >{$c.name_cou}</option>
        {/foreach}
    </select>
{else}
    <select name="selCountry" id="selCountry">
      <option value="">Todos</option>
    </select>
{/if}