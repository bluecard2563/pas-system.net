{if $userList}
	<div class="span-3 label">Liquidado Por:</div>
	<div class="span-5 last" id="userContainer">
		<select name="selUser" id="selUser" >
			{foreach from=$userList item="u"}
			<option value="{$u.serial_usr}" >{$u.first_name_usr} {$u.last_name_usr}</option>
			{/foreach}
		</select>
	</div>
    
{else}
    No se encontraron registros.
{/if}