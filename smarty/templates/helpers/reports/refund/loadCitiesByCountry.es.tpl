{if $cityList}
    <select name="selCity" id="selCity">
        <option value="">- Todos -</option>
        {foreach from=$cityList item="c"}
		<option value="{$c.serial_cit}" >{$c.name_cit|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selCity" id="selCity" >
      <option value="">- Todos -</option>
    </select>
{/if}