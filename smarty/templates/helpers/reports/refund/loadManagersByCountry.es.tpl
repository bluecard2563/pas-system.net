{if $mbcList}
    <select name="selManager" id="selManager">
		<option value="">- Seleccione -</option>
        {foreach name="manager" from=$mbcList item="m"}
			<option value="{$m.serial_mbc}" {if $smarty.foreach.manager.total eq 1}selected{/if} >{$m.name_man|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selManager" id="selManager">
      <option value="">- Seleccione -</option>
    </select>
{/if}