{if $branchList}
    <select name="selBranch" id="selBranch" >
        <option value="">- Todos -</option>
        {foreach from=$branchList item="b"}
			<option value="{$b.serial_dea}" >{$b.name_dea|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selBranch" id="selBranch" >
      <option value="">- Todos -</option>
    </select>
{/if}