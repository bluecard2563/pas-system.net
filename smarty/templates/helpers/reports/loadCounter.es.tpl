{if $counterList}
<center>
    <div class="span-24 last line">
        <table border="0" class="paginate-1 max-pages-7" id="counter_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach name="titles" from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                            {$t}
                        </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="counter" from=$counterList item="cl"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.counter.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$smarty.foreach.counter.iteration}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.cnt_name}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.birthdate_usr}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.username_usr}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.phone_usr}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$cl.email_usr}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$global_yes_no[$cl.sell_free_usr]}
                </td>
            </tr>
            {/foreach}

            </tbody>
        </table>
    </div>
     <div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>
</center>
{else}
<center>No existen counters registrados en la sucursal seleccionada.</center>
{/if}