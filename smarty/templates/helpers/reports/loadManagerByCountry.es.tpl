{if $managerByCountryList}
<center>
    <div class="span-24 last line">
        <table border="0" class="paginate-1 max-pages-7" id="manager_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach name="titles" from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                            {$t}
                        </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="managers" from=$managerByCountryList item="ml"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.managers.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$smarty.foreach.managers.iteration}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ml.name_man}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ml.contact_man}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ml.contact_email_man}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$global_pType[$ml.type_percentage_mbc]}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$ml.percentage_mbc}%
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$global_weekDays[$ml.payment_deadline_mbc]}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$global_yes_no[$ml.exclusive_mbc]}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$global_yes_no[$ml.official_mbc]}
                </td>
            </tr>
            {/foreach}

            </tbody>
        </table>
    </div>
     <div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>
</center>
{else}
<center>No existen representantes registrados en el pa&iacute;s seleccionado.</center>
{/if}