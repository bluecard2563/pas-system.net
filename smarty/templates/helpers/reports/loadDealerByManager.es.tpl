{if $dealerList}
	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
  		<option value="">{if $opt_text}- {$opt_text} -{else}- Seleccione -{/if}</option>
  		{foreach from=$dealerList item="l" name="dealers"}
  			<option value="{$l.serial_dea}" {if $dealerList|@count eq 1} selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
  		{/foreach}
	</select>
	{if $manManager}
    	<script language="javascript">
    		loadManagerSelect('{$manManager}');
   		</script>
    {/if}
{else}
    {if $serial_mbc neq ''}
	No existen comercializadores registrados.
    {/if}
    <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' {if $serial_mbc neq ''}style="display:none;"{/if}>
            <option value="" selected>- Seleccione -</option>
    </select>
{/if}