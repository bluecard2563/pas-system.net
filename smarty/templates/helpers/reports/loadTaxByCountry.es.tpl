{if $taxList}
<center>
    <div class="span-8 last line">
        <table border="0" class="paginate-1 max-pages-7" id="tax_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach name="titles" from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                            {$t}
                        </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="taxes" from=$taxList item="tl"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.taxes.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$smarty.foreach.taxes.iteration}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$tl.name_tax}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$tl.percentage_tax}
                </td>
            </tr>
            {/foreach}

            </tbody>
        </table>
    </div>
     <div class="span-8 last line pageNavPosition" id="pageNavPosition"></div>
</center>
{else}
<center>No existen impuestos registrados en el pa&iacute;s seleccionado.</center>
{/if}