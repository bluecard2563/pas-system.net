{if $counterList neq''}
	<div class="span-5 last">
			<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$counterList key=key item="l" name="counter"}
					<option value="{$l.serial_usr}" {if $smarty.foreach.counter.total eq 1}selected{/if}>{$l.cnt_name|htmlall}</option>
				{/foreach}
			</select>
	</div>
{else}
	{if $serial_dea neq ''}
		El pa&iacute;s seleccionado no tiene countes asignados.
	{/if}
	<div class="span-5 last">
			<select name="selCounter" id="selCounter" title='El campo "Seleccione un counter" es obligatorio.' {if $serial_dea neq ''}style="display:none;"{/if}>
				<option value="">- Seleccione -</option>
			</select>
	</div>
{/if}