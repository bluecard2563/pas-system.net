{if $responsible_list}
    <select name="selResponsable" id="selResponsable" class="span-5">
        <option value="">- Todos -</option>
        {foreach from=$responsible_list item="l"}
            <option value="{$l.serial_usr}" {if $responsible_list|@count eq 1}selected{/if}>{$l.name_usr|htmlall}</option>
        {/foreach}
    </select>
{else}
	{if $serial_mbc neq ''}
		No existen responsables con facturas pendientes.
	{/if}
    <select name="selResponsable" id="selResponsable" class="span-5" {if $serial_mbc neq ''}style="display:none;"{/if}>
        <option value="">- Todos -</option>
    </select>
{/if}