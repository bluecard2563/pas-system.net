{if $zoneList}
    <select name="selZone" id="selZone" title="El campo 'Zona' es obligatorio">
        <option value="">- Seleccione -</option>
        {foreach name="zones" from=$zoneList item="l"}
            <option value="{$l.serial_zon}" {if $zoneList|@count eq 1}selected{/if}>{$l.name_zon}</option>
        {/foreach}
    </select>
{else}
    {if $serial_zon neq ''}
        No existen zonas registrados.
    {/if}
    <select name="selZone" id="selZone" title="El campo 'Zona' es obligatorio" {if $serial_zon neq ''}style="display:none;"{/if}>
        <option value="">- Seleccione -</option>
    </select>
{/if}