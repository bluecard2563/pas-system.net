{if $dealerList}
	<select name="{if $branch}selBranch{else}selDealer{/if}" id="{if $branch}selBranch{else}selDealer{/if}" title='El campo "{if $branch}Sucursal de Comercializador{else}Comercializador{/if}" es obligatorio'>
		<option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
		{foreach from=$dealerList item="l" name="dealer"}
			<option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall}</option>
		{/foreach}
	</select>
{else}
	{if $branch}
		{if $serial_cit and $serial_usr and $serial_dea}
			No existen sucursales de comercializador registrados.
		{else}
		<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
			<option value="">- Seleccione -</option>
		</select>
		{/if}
	{else}
		{if $serial_cit and $serial_usr}
			No existen comercializadores registrados.
		{else}
		<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
			<option value="">- Seleccione -</option>
		</select>
		{/if}
	{/if}

{/if}