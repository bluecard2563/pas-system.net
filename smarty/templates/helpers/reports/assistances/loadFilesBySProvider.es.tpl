{if $summary}
<div class="prepend-7 span-10 append-7 last line">
	<table border="0" class="paginate-1 max-pages-7" id="files_table">
		<thead>
			<tr bgcolor="#284787">
				<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
					Coordinador
                </td>
				<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
					No. Expedientes
                </td>
			</tr>
		</thead>

		<tbody>
			{foreach from=$summary item="s"}
			<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9">
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$s.name_spv|htmlall}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$s.files_number}
				</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
</div>
		
<div class="span-24 last line center">
	<input type="submit" name="btnSubmit" id="btnSubmit" value="Ver Detalle de Asistencias" />
</div>

{else}
	<div class="prepend-3 span-12 last label line">
		No existen registros para los filtros seleccionados
	</div>
{/if}