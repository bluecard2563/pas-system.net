{if $fileList}
<center>
            <div class="prepend-3 span-19 append-2 last line">
                <table border="0" class="paginate-1 max-pages-7" id="files_table">
                    <thead>
                        <tr bgcolor="#284787">
                            {foreach from=$fileTitles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                {$t}
                            </td>
                            {/foreach}
                        </tr>
                    </thead>

                    <tbody>
                    {foreach name="fileList" from=$fileList item="fl"}
                    <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.fileList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$fl.serial_fle}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$fl.name_cou|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$fl.name_cit|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$fl.name_cus|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$fl.diagnosis_fle|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$fl.creation_date_fle}
                        </td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						   	{if $fl.status_fle eq 'open'}Abierto{elseif $fl.status_fle eq 'closed'}Cerrado{elseif $fl.status_fle eq 'stand-by'}Pendiente{/if}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						   	<img src="{$document_root}img/file_acrobat.gif" border="0" fileId="{$fl.serial_fle}" id="imgSubmitPDF_{$smarty.foreach.fileList.iteration}" name="imgSubmitPDF_{$smarty.foreach.fileList.iteration}">
                        </td>
                    </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>

             <div class="span-24 last pageNavPosition" id="pageNavPositionFiles"></div>
        </center>
		<input type="hidden" name="hdnPages" id="hdnPages" value="{$pages}" />
{else}
	<div class="prepend-7 span-10 append-7 last label line">
		La sucursal seleccionada no tiene asistencias
	</div>
{/if}

   