{if $data}
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n del cliente:</label>
    </div>

    <center>
        <div class="span-24 last line">
            <table border="0" class="paginate-1 max-pages-7" id="theTable">
                <THEAD>
                    <tr bgcolor="#284787">
                        {foreach from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;  color: #FFFFFF;">
                            {$t}
                        </td>
                        {/foreach}
                    </tr>
                </THEAD>
                <TBODY>

                {foreach name ="data" from=$data item="d"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.data.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                     <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.name_cou}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.name_cit}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.document_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.first_name_cus} {$d.last_name_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.birthdate_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.phone1_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.cellphone_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.email_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.address_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <a id="viewFiles{$d.serial_cus}" serial="{$d.serial_cus}" style="cursor: pointer;">Ver expedientes</a>
                    </td>
                </tr>
                {/foreach}
                </TBODY>
            </table>
        </div>
        <div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>

		<div class="span-24 last line" id="displayFilesContainer"></div>

    </center>
{else}
    {if $name_cus}<center>No existen expedientes abiertos con el nombre ingresado.</center>{/if}
{/if}



{literal}
<script type="text/javascript">
    if($("#theTable").length>0) {
        var pager = new Pager('theTable', 8 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    $("a[id^='viewFiles']").bind("click",function(){
        if($(this).html()=="Ver expedientes") {
			$("#displayFilesContainer").html('');
            $("a[id^='viewFiles']").html("Ver expedientes");
            $(this).html("Ocultar expedientes");
            $("#displayFilesContainer").load(document_root+"rpc/reports/assistance/loadFileByCustomer.rpc",{serial_cus:$(this).attr('serial')},function(){
				$("img[id^='imgSubmitPDF']").bind("click", function(){
					$('#frmBlogReport').attr('action',document_root+'modules/reports/assistance/pAssistanceReport/'+$(this).attr('serial_fle'));
					$('#frmBlogReport').submit();
				});
			});
        } else {
            $("a[id^='viewFiles']").html("Ver expedientes");
            $("#displayFilesContainer").html('');
        }
    });
</script>
{/literal}