{if $data}
<center>
            <div class="span-24 last line">
                <table border="0" class="paginate-1 max-pages-7" id="files_table">
                    <thead>
                        <tr bgcolor="#284787">
                            {foreach from=$titles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                {$t}
                            </td>
                            {/foreach}
                        </tr>
                    </thead>

                    <tbody>
					{foreach name="files" from=$data item="d"}
                    <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="{cycle values='#d7e8f9,#e8f2fb'}">
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$d.serial_fle}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$d.name_cou|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$d.name_cit|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$d.name_cus|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$d.diagnosis_fle|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$d.creation_date_fle}
                        </td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						   	{if $d.status_fle eq 'open'}Abierto{elseif $d.status_fle eq 'closed'}Cerrado{elseif $d.status_fle eq 'stand-by'}Pendiente{/if}
                        </td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						   	{$d.audited_date_prq}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							<img src="{$document_root}img/file_acrobat.gif" border="0" serial_prq="{$d.serial_prq}" id="imgSubmitPDF_{$d.serial_prq}" name="imgSubmitPDF_{$d.serial_prq}" style="cursor: pointer;">
                        </td>
                    </tr>
					{/foreach}
                    </tbody>
                </table>
            </div>

		<div class="span-24 last line pageNavPosition" id="pageNavPositionFiles"></div>
        </center>
{else}
	<div class="prepend-4 span-12 last label line">
		No existen expedientes auditados para la tarjeta ingresada.
	</div>
{/if}

{literal}
<script type="text/javascript">
	if($("#files_table").length>0) {
        var pager2 = new Pager('files_table', 5 , 'Siguiente', 'Anterior');
        pager2.init(7); //Max Pages
        pager2.showPageNav('pager2', 'pageNavPositionFiles'); //Div where the navigation buttons will be placed.
        pager2.showPage(1); //Starting page
    }
</script>
{/literal}