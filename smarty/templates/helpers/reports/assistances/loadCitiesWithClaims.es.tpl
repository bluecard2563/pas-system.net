{if $cityList}
    <select name="selCity" id="selCity" title="El campo 'Ciudad' es obligatorio">
        <option value="">- Seleccione -</option>
        {foreach name="cities" from=$cityList item="l"}
            <option value="{$l.serial_cit}" {if $cityList|@count eq 1}selected{/if}>{$l.name_cit}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
        No existen ciudades con reclamos.
    {/if}
    <select name="selCity" id="selCity" title="El campo 'Ciudad' es obligatorio" {if $serial_cou neq ''}style="display:none;"{/if}>
        <option value="">- Seleccione -</option>
    </select>
{/if}