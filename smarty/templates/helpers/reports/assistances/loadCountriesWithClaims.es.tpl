{if $countryList}
    <select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio">
        <option value="">- Seleccione -</option>
        {foreach name="countries" from=$countryList item="l"}
            <option value="{$l.serial_cou}" {if $countryList|@count eq 1}selected{/if}>{$l.name_cou}</option>
        {/foreach}
    </select>
{else}
    {if $serial_zon neq ''}
        No existen paises registrados.
    {/if}
    <select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio" {if $serial_zon neq ''}style="display:none;"{/if}>
        <option value="">- Seleccione -</option>
    </select>
{/if}