{if $data}
<center>
            <div class="span-24 last line">
                <table border="0" class="paginate-1 max-pages-7" id="files_table">
                    <thead>
                        <tr bgcolor="#284787">
                            {foreach from=$titles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                {$t}
                            </td>
                            {/foreach}
                        </tr>
                    </thead>

                    <tbody>

                    <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9">
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$data.serial_fle}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$data.name_cou|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$data.name_cit|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$data.name_cus|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$data.diagnosis_fle|htmlall}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$data.creation_date_fle}
                        </td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						   	{if $data.status_fle eq 'open'}Abierto{elseif $data.status_fle eq 'closed'}Cerrado{elseif $data.status_fle eq 'stand-by'}Pendiente{/if}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							<img src="{$document_root}img/file_acrobat.gif" border="0" serial_fle="{$data.serial_fle}" id="imgSubmitPDF" name="imgSubmitPDF" style="cursor: pointer;">
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </center>
{else}
	<div class="prepend-3 span-12 last label line">
		No existe el n&uacute;mero de expediente ingresado.
	</div>
{/if}