{if $dealer_list}
    <select name="selDealer" id="selDealer" class="span-5">
        <option value="">- Todos -</option>
        {foreach from=$dealer_list item="l"}
            <option value="{$l.dea_serial_dea}" {if $dealer_list|@count eq 1}selected{/if}>{$l.name_dea|htmlall}</option>
        {/foreach}
    </select>
{else}
	{if $serial_ubd neq ''}
		No existen comercializadores con facturas pendientes.
	{/if}
    <select name="selDealer" id="selDealer" class="span-5" {if $serial_ubd neq ''}style="display:none;"{/if}>
        <option value="">- Todos -</option>
    </select>
{/if}