{if $file_list}
<div class="prepend-1 span-22 append-1">
	<div class="span-22 last line separator">
		<label>Expedientes</label>
	</div>
</div>

<div class="prepend-1 span-22 append-1 last line">
	<table border="1" class="dataTable" id="files_table">
		<THEAD>
			<tr class="header">
				<th style="text-align: center;">
					No. Tarjeta
				</th>
				<th style="text-align: center;">
					Cliente
				</th>
				<th style="text-align: center;">
					Inicio de Vigencia
				</th>
				<th style="text-align: center;">
					Fin de Vigencia
				</th>
				<th style="text-align: center;">
					Fecha de Asistencia
				</th>
				<th style="text-align: center;">
					Fecha de la Factura
				</th>
				<th style="text-align: center;">
					Tiempo de Cr&eacute;dito
				</th>
				<th style="text-align: center;">
					Valor a Pagar
				</th>
				<th style="text-align: center;">
					Comercializador
				</th>
				<th style="text-align: center;">
					Responsable
				</th>				
			</tr>
		</THEAD>

		<TBODY>
		{foreach name="files" from=$file_list item="cl"}
		<tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.files.iteration is even} class="odd"{else}class="even"{/if}>
			<td class="tableField">
				{$cl.card_number_sal}
			</td>
			<td class="tableField">
				{$cl.name_cus|htmlall}
			</td>
			<td class="tableField">
				{$cl.begin_date_sal}
			</td>
			<td class="tableField">
				{$cl.end_date_sal}
			</td>
			<td class="tableField">
				{$cl.creation_date_fle}
			</td>
			<td class="tableField">
				{$cl.date_inv}
			</td>
			<td class="tableField">
				{$cl.days_cdd}
			</td>
			<td class="tableField">
				{$cl.total_inv}
			</td>
			<td class="tableField">
				{$cl.name_dea|htmlall}
			</td>
			<td class="tableField">
				{$cl.name_ubd|htmlall}
			</td>
		</tr>
		{/foreach}
		</TBODY>
	</table>
</div>		

<div class="span-24 buttons last line">
	<input type="button" class="XLS" id="btnGenerateXLS" name="btnGenerateXLS" />
</div>
{else}
	<center><b>No existen registros para los par&aacute;metros seleccionados</b></center><br />
{/if}