{if $manager_list}
    <select name="selManager" id="selManager" class="span-5">
        <option value="">- Todos -</option>
        {foreach name="manager" from=$manager_list item="l"}
            <option value="{$l.serial_mbc}" {if $manager_list|@count eq 1}selected{/if}>{$l.name_man}</option>
        {/foreach}
    </select>
{else}
	{if $serial_cou neq ''}
		No existen representantes con facturas pendientes.
	{/if}
    <select name="selManager" id="selManager" class="span-5" {if $serial_cou neq ''}style="display:none;"{/if}>
        <option value="">- Todos -</option>
    </select>
{/if}