{if $claimList}
<div class="span-24 last line separator">
    <label>Reclamos:</label>
</div>
    <center>
        <div class="prepend-1 span-22 last line">
            <table border="1" class="dataTable" id="claims_table">
                <thead>
                    <tr class="header">
                        <th style="text-align: center;">
                            <input type="checkbox" id="selAll" name="selAll"/>
                        </th>
						{foreach from=$titles item="t"}
                        <th style="text-align: center;">
                            {$t}
                        </th>
                        {/foreach}
                    </tr>
                </thead>

                <tbody>
                {foreach name="claims" from=$claimList item="cl"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.claims.iteration is even} class="odd"{else}class="even"{/if}>
                    <td class="tableField">
                        <input type="checkbox" name="chkClaims[]" id="chkClaims{$cl.serial_prq}" value="{$cl.serial_prq}" class="bonusChk" />
						<!-- <input type="checkbox" name="chk" id="chkClaims{$cl.serial_prq}" amount="{$cl.amount_authorized_prq}" billTo="{$cl.document_to_dbf}" serial_cus="{$cl.serial_cus}" value="{$cl.serial_prq}"/>-->
                    </td>
                    <td class="tableField">
                        {$cl.name_dbf}
                    </td>
					<td class="tableField">
                        {$cl.name_cou}
                    </td>
                    <td class="tableField">
                        {$cl.comment_dbf}
                    </td>
					<td class="tableField">
                        {$cl.type_prq}
                    </td>
                    <td class="tableField">
                        {$cl.amount_authorized_prq}
                    </td>
                    <td class="tableField">
                        {$cl.request_date}
                    </td>
                    <td class="tableField">
                        {$cl.name_cus}
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </div>		
		
		<div class="span-24 center last line" id="buttonContainer">
			<input type="button" class="PDF" id="btnGeneratePDF" name="btnGeneratePDF" />
			<input type="button" class="XLS" id="btnGenerateXLS" name="btnGenerateXLS" />
			<input type="hidden" id="hdtItemCount" value="{$count}" />
			<input type="hidden" id="selectedBoxes" name="selectedBoxes" value="" />
		</div>
</center>
{else}
    {if $document_to_dbf eq 'DIRECT'}
        <center><b>No existen Reclamos de Pago {$global_billTo.$document_to_dbf}.</b></center><br>
    {elseif $document_to_dbf eq 'PROVIDER'}
        <center><b>No existen Reclamos de Pago para este coordinador.</b></center><br>
    {elseif $document_to_dbf eq 'CLIENT'}
        <center><b>No existen Reclamos de Pago para los filtros seleccionados</b></center><br>
    {else}
        <center><b>No existen Reclamos de Pago para los filtros seleccionados</b></center><br>
    {/if}
{/if}
