{if $cardsInfo}
<div class="span-24 last line title">
	Listado de Tarjetas
</div>

<div class="span-24 last line">
	<table id="cardsTable">
		<tr>
			<td class="tableTitles"><b>#</b></td>
			<td class="tableTitles"><b>No. de Tarjeta</b></td>
			<td class="tableTitles"><b>Producto</b></td>
			<td class="tableTitles"><b>Inicio de Vigencia</b></td>
			<td class="tableTitles"><b>Fin de Vigencia</b></td>
			<td class="tableTitles"><b>Estado</b></td>
		</tr>
		{foreach name="cards" from=$cardsInfo item="l"}
		<tr class="{cycle values='even-row,odd-row'}">
			<td class="refundTable">{$smarty.foreach.cards.iteration}</td>
			<td class="refundTable">{if $l.card_number_sal}{$l.card_number_sal}{else}N/A{/if}</td>
			<td class="refundTable">{$l.name_pbl|htmlall}</td>
			<td class="refundTable">{$l.begin_date_sal}</td>
			<td class="refundTable">{$l.end_date_sal}</td>
			<td class="refundTable">{$global_salesStatus[$l.status_sal]}</td>
		</tr>
		{/foreach}
	</table>
</div>

<div class="span-24 last buttons line" id="pageNavPosition"></div>

<div class="span-24 last buttons line">
	<a href="{$document_root}modules/reports/sales/pViewCardsByCustomerPDF/{$serial_cus}" target="_blank"><img src="{$document_root}img/file_acrobat.gif" border="0"/></a>
</div>
{else}
<div class="prepend-7 span-10 append-7 last line">
	<div class="span-10 error">
		No existe ning&uacute;n cliente con la informaci&oacute;n ingresada.
	</div>
</div>
{/if}