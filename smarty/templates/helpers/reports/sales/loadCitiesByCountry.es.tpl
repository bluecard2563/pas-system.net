{if $cityList}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
        <option value="">Todos</option>
        {foreach from=$cityList item="c"}
		<option value="{$c.serial_cit}" >{$c.name_cit}</option>
        {/foreach}
    </select>
{else}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio' >
      <option value="">Todos</option>
    </select>
{/if}