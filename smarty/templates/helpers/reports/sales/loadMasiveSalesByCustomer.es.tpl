{if $msales}
<center>
	<div class="prepend-3 span-19 append-2 last line">
		<table border="0" class="paginate-1 max-pages-7" id="cardsTable{if $serial_sal}Child{/if}">
			<thead>
				<tr bgcolor="#284787">
					<td align="center" style="border: solid 1px white; padding:0px 5px 5px 5px;">&nbsp;</td>
					{foreach from=$titles item="t"}
					<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
						{$t}
					</td>
					{/foreach}
				</tr>
			</thead>

			<tbody>
			{foreach name="cardList" from=$msales item="cl"}
			<tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.cardList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
				<td style="border: solid 1px white; padding:0px 5px 5px 5px; text-align:center;">
					<input type="radio" name="rdoCard{if $serial_sal}Child{else}Parent{/if}[]" id="rdoCard{if $serial_sal}Child{else}Parent{/if}{$cl.serial_sal}" value="{$cl.serial_sal}">
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$cl.card_number_sal}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$cl.name_pbl}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$cl.emission_date_sal}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$cl.begin_date_sal}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$cl.end_date_sal}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$global_salesStatus[$cl.status_sal]}
				</td>
			</tr>
			{/foreach}
			</tbody>
		</table>
	</div>

	<div class="span-24 last line pageNavPosition" id="pageNavPosition{if $serial_sal}Child{/if}"></div>

	{if $serial_sal}
	<div class="span-24 last buttons line">
		Generar reporte:
		<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
		<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
	</div>
	{else}
	<div class="span-24 last buttons line">
		<input type="button" name="btnChildrenCards" id="btnChildrenCards" value="Ver tarjetas anexas" />
	</div>
	{/if}

</center>
{else}
	<center>El cliente seleccionado no tiene tarjetas masivas.</center>
{/if}

{literal}
<script type="text/javascript">
if($("#cardsTable").length>0) {
	var pager = new Pager('cardsTable', 5 , 'Siguiente', 'Anterior');
	pager.init(7); //Max Pages
	pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
	pager.showPage(1); //Starting page
}
if($("#cardsTableChild").length>0) {
	var pager = new Pager('cardsTableChild', 5 , 'Siguiente', 'Anterior');
	pager.init(7); //Max Pages
	pager.showPageNav('pager', 'pageNavPositionChild'); //Div where the navigation buttons will be placed.
	pager.showPage(1); //Starting page
}
</script>
{/literal}