{if $responsibleList}
    <select name="selResponsible" id="selResponsible">
        <option value="">Todos</option>
        {foreach from=$responsibleList item="r"}
			<option value="{$r.serial_usr}" >{$r.name_usr|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selResponsible" id="selResponsible" >
      <option value="">Todos</option>
    </select>
{/if}