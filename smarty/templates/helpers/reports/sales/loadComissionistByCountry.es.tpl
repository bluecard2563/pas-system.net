{if $comissionistList}
    <select name="selProduct" id="selProduct">
        <option value="">- Seleccione -</option>
        {foreach from=$comissionistList item="l"}
        	<option value="{$l.serial_usr}">{$l.first_name_usr} {$l.last_name_usr}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
		No existen responsables registrados.
    {/if}
    <select name="selProduct" id="selProduct" {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}