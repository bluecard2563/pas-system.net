{if $responsibleList}
    <select name="selResponsible" id="selResponsible">
        <option value="">{if $reports eq 'true'}- Todos -{else}- Seleccione -{/if}</option>
        {foreach from=$responsibleList item="l"}
        	<option value="{$l.serial_usr}">{$l.name_usr|htmlall}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cit neq ''}
		No existen responsables registrados.
    {/if}
    <select name="selResponsible" id="selResponsible" {if $serial_cit neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}