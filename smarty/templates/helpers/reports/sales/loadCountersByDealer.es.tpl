{if $counterList}
	<select name="selCounter" id="selCounter" title="El campo 'Counter' es obligatorio.">
        <option value="">- Todos -</option>
        {foreach name="counters" from=$counterList item="c"}
            <option value="{$c.serial_cnt}"{if $smarty.foreach.counters.total eq 1}selected{/if} >{$c.cnt_name}</option>
        {/foreach}
    </select>
{else}
    <select name="selCounter" id="selCounter" title="El campo 'Counter' es obligatorio.">
        <option value="">- Todos -</option>
    </select>
{/if}