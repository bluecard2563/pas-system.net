{if $countryList}
	<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
        <option value="">- Seleccione -</option>
        {foreach name="countries" from=$countryList item="c"}
		<option value="{$c.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if} >{$c.name_cou}</option>
        {/foreach}
    </select>
{else}
    <select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
      <option value="">- Seleccione -</option>
    </select>
{/if}