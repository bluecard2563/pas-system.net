{if $productList}
	<select name="selProduct{if $multiple eq 'true'}[]{/if}" id="selProduct" {if $multiple eq 'true'}multiple{/if}>
		{if !$multiple}
        <option value="">{if $reports eq 'true'}- Todos-{else}- Seleccione -{/if}</option>
		{/if}
        {foreach from=$productList item="l"}
        	<option value="{$l.serial_pro}">{$l.name_pbl|htmlall}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
		No existen productos registrados.
    {/if}
    <select name="selProduct" id="selProduct" {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}