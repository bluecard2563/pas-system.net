{if $dealerList}
    <select name="selDealer" id="selDealer">
        <option value="">{if $opt_text}- {$opt_text} -{else}- Seleccione -{/if}</option>
        {foreach from=$dealerList item="d"}
		<option value="{$d.serial_dea}" >{$d.name_dea}</option>
        {/foreach}
    </select>
{else}
    <select name="selDealer" id="selDealer">
      <option value="">Todos</option>
    </select>
{/if}