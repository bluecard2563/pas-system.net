{if $dealerList}
    <select name="selDealer" id="selDealer">
        <option value="">Todos</option>
        {foreach from=$dealerList item="d"}
			<option value="{$d.serial_dea}" >{$d.name_dea|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selDealer" id="selDealer">
      <option value="">Todos</option>
    </select>
{/if}