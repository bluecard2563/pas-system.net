{if $dealerList}
<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach from=$dealerList item="l" name="dealer"}
        <option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall}</option>
  {/foreach}
</select>
{else}
    {if $serial_cit}
        No existen comercializadores registrados.
    {/if}
    <select name="selDealer" id="selDealer" title='El campo "Comercializadora" es obligatorio' {if $serial_cit}style="display:none"{/if}>
      <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
    </select>
{/if}