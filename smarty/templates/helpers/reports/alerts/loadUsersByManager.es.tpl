{if $comissionistList}
<select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio.'>
	<option value="">- Seleccione -</option>
	{foreach from=$comissionistList item="d"}
		<option value="{$d.serial_usr}" >{$d.first_name_usr|htmlall} {$d.last_name_usr|htmlall}</option>
	{/foreach}
</select>
{else}
	{if $serial_mbc neq ''}
		No existen usuarios registrados en el sistema.
    {/if}
    <select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio' {if $serial_mbc neq ''}style="display:none"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}