{if $responsiblesList}
	<select name="selResponsible" id="selResponsible" class="span-5">
	  <option value="">- Seleccione -</option>
	  {foreach from=$responsiblesList item="l"}
		<option value="{$l.serial_usr}" {if $responsiblesList|@count eq 1} selected{/if}>{$l.name_usr|htmlall}</option>
	  {/foreach}
	</select>
{else}
	No existen Responsables activos para el representante seleccionado.
	<select name="selResponsible" id="selResponsible" style="display: none;">
	  <option value="">- Seleccione -</option>
	</select>
{/if}