{if $managerList}
	<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
	  <option value="">{if $opt_text neq ''}- {$opt_text} -{else}- Seleccione -{/if}</option>
	  {foreach from=$managerList item="l"}
		<option value="{$l.serial_mbc}" {if $managerList|@count eq 1} selected{/if}>{$l.name_man|htmlall}</option>
	  {/foreach}
	</select>
{else}
	No existen Representantes con Comercializadores en el Pa&iacute; seleccionado.
	<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.' style="display: none;">
	  <option value="">{if $opt_text neq ''}- {$opt_text} -{else}- Seleccione -{/if}</option>
	</select>
{/if}