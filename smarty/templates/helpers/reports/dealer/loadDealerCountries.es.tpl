{if $countryList}
	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
	  <option value="">- Seleccione -</option>
	  {foreach from=$countryList item="l"}
		<option value="{$l.serial_cou}" {if $countryList|@count eq 1} selected{/if}>{$l.name_cou|htmlall}</option>
	  {/foreach}
	</select>
{else}
	No existen Paises con Comercializadores en la Zona seleccionada.
	<select name="selCountry" id="selCountry" title='El campo "Pa&i&iacute;s" es obligatorio.' style="display: none;">
	  <option value="">- Seleccione -</option>
	</select>
{/if}