{if $branchList}
	<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
	  <option value="">- Seleccione -</option>
	  {foreach from=$branchList item="l"}
		<option value="{$l.serial_dea}" {if $branchList|@count eq 1} selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
	  {/foreach}
	</select>
{else}
	No existen comercializadores para el responsable seleccionado.
	<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.' style="display: none;">
	  <option value="">- Seleccione -</option>
	</select>
{/if}