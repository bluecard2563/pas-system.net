{if $assignedDealers}
	<div class="span-24 last title">
		Resumen de Efectividad de Comercializadores<br />
	</div>
	<br/>
	<div class="prepend-4 span-16 last line">
		<table border="0" id="alertsTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle"></td>
					<td align="center" class="tableTitle">Cantidad</td>
					<td align="center" class="tableTitle">Porcentaje</td>
				</tr>
			</THEAD>
			<TBODY>
				<tr bgcolor="#d7e8f9">
					<td align="center" class="tableField"># de Comercializadores</td>
					<td align="center" class="tableField">{$assignedDealers}</td>
					<td align="center" class="tableField">100.00%</td>
				</tr>
				<tr bgcolor="#e8f2fb">
					<td align="center" class="tableField"># de Comercializadores que Vendieron</td>
					<td align="center" class="tableField">{$dealersSelling}</td>
					<td align="center" class="tableField">{$sellingPercentage}%</td>
				</tr>
				<tr bgcolor="#d7e8f9">
					<td align="center" class="tableField">Mejor sucursal de Comercializador (# Tarjetas)</td>
					<td align="center" colspan="2" class="tableField">{$nameDealerCards}</td>
				</tr>
				<tr bgcolor="#d7e8f9">
					<td align="center" class="tableField">Mejor sucursal de Comercializador (Total Vendido)</td>
					<td align="center" colspan="2" class="tableField">{$nameDealer}</td>
				</tr>
			</TBODY>
		</table>
	</div>
	<div class="span-24 last line">
        <div class="prepend-10 span-5">
            Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS" class="XLS"/>
        </div>
    </div>
{else}
<div class="prepend-8 span-10 last">
	No se encontraron comercializadores asignados.
</div>
{/if}