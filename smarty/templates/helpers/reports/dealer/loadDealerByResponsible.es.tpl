{if $dealerList}
	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
	  <option value="">- Seleccione -</option>
	  {foreach from=$dealerList item="l"}
		<option value="{$l.serial_dea}" {if $dealerList|@count eq 1} selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
	  {/foreach}
	</select>
{else}
	No existen comercializadores para el responsable seleccionado.
	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' style="display: none;">
	  <option value="">- Seleccione -</option>
	</select>
{/if}