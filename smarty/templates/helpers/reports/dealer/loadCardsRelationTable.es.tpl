{if $cardsResume}
	<div class="span-24 last title">
		Resumen de Tarjetas Vendidas<br />
	</div>
	<br/>
	<div class="prepend-6 span-12 last line">
		<table border="0" id="alertsTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle"></td>
					<td align="center" class="tableTitle">Cantidad</td>
					<td align="center" class="tableTitle">Porcentaje</td>
				</tr>
			</THEAD>
			<TBODY>
				<tr bgcolor="#d7e8f9">
					<td align="center" class="tableField">N&uacute;mero de Tarjetas Vendidas</td>
					<td align="center" class="tableField">{$total}</td>
					<td align="center" class="tableField">100.00%</td>
				</tr>
				<tr bgcolor="#e8f2fb">
					<td align="center" class="tableField">Virtuales</td>
					<td align="center" class="tableField">{$cardsResume.VIRTUAL}</td>
					<td align="center" class="tableField">{$virtualPercentage}%</td>
				</tr>
				<tr bgcolor="#d7e8f9">
					<td align="center" class="tableField">Manuales</td>
					<td align="center" class="tableField">{$cardsResume.MANUAL}</td>
					<td align="center" class="tableField">{$manualPercentage}%</td>
				</tr>
			</TBODY>
		</table>
	</div>
	<div class="span-24 last line">
        <div class="prepend-10 span-5">
            Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS" class="XLS"/>
        </div>
    </div>
{else}
<div class="prepend-8 span-10 last">
	No se encontraron tarjetas disponibles para los par&aacute;metros seleccionados.
</div>
{/if}