{if $cityList}
	<select name="selCity" id="selCity" class="span-5">
	  <option value="">- Seleccione -</option>
	  {foreach from=$cityList item="l"}
		<option value="{$l.serial_cit}" {if $cityList|@count eq 1} selected{/if}>{$l.name_cit|htmlall}</option>
	  {/foreach}
	</select>
{else}
	No existen Ciudades disponibles.
	<select name="selCity" id="selCity" style="display: none;">
	  <option value="">- Seleccione -</option>
	</select>
{/if}