{if $dealerList}
    <div class="span-24 last separator">Sucursales</div>

    <div class="span-24 last line">
        <table border="1" class="dataTable" align="center" id="branch_table">
            <thead>
                <tr class="header">
                    {foreach name="titles" from=$titles item="t"}
                        <th style="text-align: center;">
                            {$t}
                        </th>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
                {foreach name="dealers" from=$dealerList item="dl"}
                    <tr {if $smarty.foreach.dealers.iteration is even}class="odd"{else}class="even"{/if}>
                        <td class="tableField">
                            {$dl.name_dea}
                        </td>
                        <td class="tableField">
                            {$dl.id_dea}
                        </td>
                        <td class="tableField">
                            {$dl.percentage_dea} ({$dl.type_percentage_dea})
                        </td>
                        <td class="tableField">
                            {$dl.days_cdd}
                        </td>
                        <td class="tableField">
                            {$dl.name_cit}
                        </td>
                        <td class="tableField">
                            {$dl.name_sec}
                        </td>
                        <td class="tableField">
                            {$dl.address_dea}
                        </td>
                        <td class="tableField">
                            {$dl.phone1_dea}
                        </td>
                        <td class="tableField">
                            {$dl.responsible}
                        </td>
                        <td class="tableField">
                            {$dl.manager_name_dea}
                        </td>
                        <td class="tableField">
                            {$dl.contact_dea}
                        </td>
                        <td class="tableField">
                            {$dl.email_contact_dea}
                        </td>
                    </tr>
                {/foreach}

            </tbody>
        </table>
    </div>
{else}
    <center>No existen sucursales registradas que cumplan los par&aacute;metros seleccionados.</center>
{/if}