{if $dealerList}
<select name="selDealer" id="selDealer">
	<option value="">- Todos -</option>
	{foreach from=$dealerList item="d"}
	<option value="{$d.serial_dea}" {if $dealerList|@count eq 1}selected{/if}>{$d.name_dea|htmlall} - {$d.code_dea}</option>
	{/foreach}
</select>
{else}
No existen comercializadores para el representante seleccionado.
{/if}