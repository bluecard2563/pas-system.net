{if $countriesWithBonus}
	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			* Pa&iacute;s:
		</div>
		<div class="span-6">
			<select name="selCountry" id="selCountry" title='El campo "Pa&i&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$countriesWithBonus item="c"}
				<option value="{$c.serial_cou}" {if $countriesWithBonus|@count eq 1} selected{/if}>{$c.name_cou|htmlall}</option>
			  {/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			Ciudad:
		</div>
		<div class="span-6" id="cityContainer">
			<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			Representante:
		</div>
		<div class="span-6" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			Comercializador:
		</div>
		<div class="span-6" id="dealerContainer">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			Sucursal:
		</div>
		<div class="span-6" id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	{if $bonus_to eq 'COUNTER'}
	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			Asesor de Venta:
		</div>
		<div class="span-6" id="counterContainer">
			<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	{/if}

	
	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			Tipo:
		</div>
		<div class="span-6" id="branchContainer">
			<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio.'>
			  <option value="">- Todos -</option>
			  <option value="YES">Autorizados</option>
			  <option value="NO">Negados</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line">
		<input type="hidden" name="today" id="today" value="{$today}" />
		<div class="prepend-3 span-4 label">
			Fecha Desde:
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" value="" />
		</div>

		<div class="span-3 label">
			Fecha Hasta:
		</div>
		<div class="span-6 append-2 last">
			<input type="text" name="txtEndDate" id="txtEndDate" value="" />
		</div>
	</div>
	
	<div class="span-24 last line buttons">
		<img id="imgSubmit_XLS" src="{$document_root}img/page_excel.png" alt="Reporte Excel" title="Reporte XLS" />
		<img id="imgSubmit_PDF" src="{$document_root}img/file_acrobat.gif" alt="Reporte Excel" title="Reporte PDF" />
	</div>
{else}
<div class="prepend-7 span-10 append-7 last line">
	<div class="span-10 error center">
		No existen incentivos pendientes por pagar.
	</div>
</div>
{/if}