{if $branchList}
    <select name="selBranch" id="selBranch" >
        <option value="">Todos</option>
        {foreach from=$branchList item="b"}
		<option value="{$b.serial_dea}" {if $branchList|@count eq 1} selected{/if} >{$b.name_dea|htmlall} - {$b.code_dea}</option>
        {/foreach}
    </select>
{else}
    <select name="selBranch" id="selBranch" >
      <option value="">Todos</option>
    </select>
{/if}