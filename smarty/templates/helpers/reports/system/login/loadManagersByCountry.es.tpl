{if $mbcList}
    <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.' class="span-4">
			<option value="">- Seleccione -</option>
        {foreach from=$mbcList item="m"}
			<option value="{$m.serial_mbc}" {if $mbcList|@count eq 1} selected{/if}>{$m.name_man|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selManager" id="selManager" class="span-4">
      <option value="">Todos</option>
    </select>
{/if}