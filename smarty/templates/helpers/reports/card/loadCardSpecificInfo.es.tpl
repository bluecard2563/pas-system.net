{if $resultList}
<div class="span-24 last line">
<table border="0" id="card_table">
	<thead>
		<tr bgcolor="#284787">
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				# Tarjeta
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Nombre Cliente
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Producto
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Estado Venta
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				# Factura
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Estado Factura
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				# Liq. Incentivos
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Usuario
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Detalle Venta
			</td>
			<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Detalle Factura
			</td>
                        <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
				Imprimir Factura
			</td>
		</tr>
	</thead>

	<tbody>
	{foreach name="results" from=$resultList item="res" key=cid}
	<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.taxes.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.num_card}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.cus_name|htmlall}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.prod_name|htmlall}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.status|htmlall}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.num_inv}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$global_invoice_status[$res.status_inv]}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.liq_number}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{$res.user_name|htmlall}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			<input type="button" name="btnGenerateSaleReportPDF_{$res.serial_sal}" id="btnGenerateSaleReportPDF_{$res.serial_sal}" class="PDF" />
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{if $res.num_inv && $res.num_inv neq 'FREE'}
				<input type="button" name="btnGenerateInvoiceReportPDF_{$res.serial_sal}" id="btnGenerateInvoiceReportPDF_{$res.serial_sal}" class="PDF" />
			{/if}
		</td>
		<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
			{if $res.erp_number_inv && $res.erp_holder_document}
            	{if $res.date_inv < '2018-07-16'}
					<A HREF="https://bluecard.saas.la/intranet/{$res.erp_number_inv}/{$res.erp_holder_document}/PublicPDF/" target="_blank">Imprimir PDF</A>
                {else}
					 <A HREF="https://www.pas-system.net/loadOwnInvoicesPDF.php?number={$res.erp_number_inv}" target="_blank">Imprimir PDF</A>
                {/if}
			{/if}
		</td>
	</tr>
	{/foreach}

	</tbody>
</table>
</div>
<div class="span-24 last line buttons" id="pageNavPosition"></div>
{else}
	<center>No existen resultados para los parametros ingresados.</center>
{/if}