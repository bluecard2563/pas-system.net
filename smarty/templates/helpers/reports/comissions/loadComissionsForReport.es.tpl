{if $comissionsData}
<div class="prepend-7 span-10 append-7 last line">
	<center>Generar Reporte en:<br/>
		<input type="image" src="{$document_root}img/file_acrobat.gif" id="pdfButton" name="pdfButton"/>
		<input type="image" src="{$document_root}img/page_excel.png"  id="xlsButton" name="xlsButton"/>
	</center>
</div>
{else}
<div class="prepend-7 span-10 append-7 last line">
	<div class="span-10 error">
		No existen registros de comisiones que cumplan con los par&aacute;metros ingresados.
	</div>
</div>
{/if}