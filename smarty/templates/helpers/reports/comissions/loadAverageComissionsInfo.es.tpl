<div class="prepend-3 span-18 append-3 lat line">
	{if $gross_comissions}
	<table border="1" id="alertsTable" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					Responsable
				</td>
				<td align="center" class="tableTitle">
					Comisi&oacute;n Promedio Te&oacute;rica
				</td>
				<td align="center" class="tableTitle">
					Comisi&oacute;n Promedio Efectiva
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="comissions" from=$gross_comissions key="k" item="s"}
			<tr {if $smarty.foreach.comissions.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					<b>{$s.seller|htmlall}</b>
				</td>
				<td align="center" class="tableField">
					{$s.theoric_comission}%
				</td>
				<td align="center" class="tableField">
					{$s.effective_comission}%
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
	{else}
		<div class="prepend-3 span-12 append-3 center">
			<div class="span-12 center error">
				No existe informaci&oacute;n para los par&aacute;metros seleccionados.
			</div>
		</div>
	{/if}
</div>