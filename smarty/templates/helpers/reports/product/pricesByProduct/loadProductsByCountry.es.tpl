{if $productList}
    <select name="selProduct" id="selProduct">
        <option value="">- Seleccione -</option>
        {foreach from=$productList item="p"}
		<option value="{$p.serial_pro}" >{$p.name_pbl|htmlall}</option>
        {/foreach}
    </select>
{else}
    <select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio'>
      <option value="">- Seleccione -</option>
    </select>
{/if}