 <div class="span-19 last line">
    <div class="prepend-1 span-4 label">Comisionable y Aplica Descuentos:</div>
    <div class="span-5">
    
        <input type="checkbox" name="has_comision_tpp" id="has_comision_tpp" value="1" {if $has_comision_tpp eq 1} checked="checked" {/if}/>
    </div>
    <div class="span-3 label">Paises Schengen:</div>
    <div class="span-4 append-2 last" >
        <input type="checkbox" name="schengen_tpp" id="schengen_tpp" value="1" {if $schengen_tpp eq 1} checked="checked" {/if}/>
    </div>
</div>

 <div class="span-19 last line">
    <div class="prepend-1 span-4 label">Disponible en P&aacute;gina Web:</div>
    <div class="span-5">
        <input type="checkbox" name="in_web_tpp" id="in_web_tpp" value="1" {if $in_web_tpp eq 1} checked="checked" {/if}/>
    </div>
    <div class="span-3 label">Precio para Conyuige Diferente:</div>
    <div class="span-4 append-2 last">
        <input type="checkbox" name="spouse_tpp" id="spouse_tpp" value="1" {if $spouse_tpp eq 1} checked="checked" {/if}/>
    </div>
</div>
    
    
 <div class="span-19 last line">
    <div class="prepend-1 span-4 label">V&aacute;lida dentro del pa&iacute;s de emisi&oacute;n:</div>
    <div class="span-5">
        <input type="checkbox" name="emission_country_tpp" id="emission_country_tpp" value="1" {if $emission_country_tpp eq 1} checked="checked" {/if}/>
    </div>
    <div class="span-3 label">V&aacute;lida dentro del pa&iacute;s de residencia:</div>
    <div class="span-4 append-2 last">
        <input type="checkbox" name="residence_country_tpp" id="residence_country_tpp" value="1" {if $residence_country_tpp eq 1} checked="checked" {/if}/>
    </div>
</div>
         
        
 <div class="span-19 last line">
    <div class="prepend-1 span-4 label">Precio para Familiares Diferente:</div>
    <div class="span-5">
    	<input type="checkbox" name="relative_tpp" id="relative_tpp" value="1" {if $relative_tpp eq 1} checked="checked" {/if}/>
    </div>
    <div class="span-3 label">Varias tarjetas en un solo contrato:</div>
    <div class="span-4 append-2 last">
	    <input type="checkbox" name="extras_by_contract_tpp" id="extras_by_contract_tpp" value="1" {if $extras_by_contract_tpp eq 1} checked="checked" {/if}/>
    </div>
</div>
                        
<div class="span-19 last line">
*en caso de no seleccionar esta casilla, y existen adicionales estos se agregar&aacute;n a la misma tarjeta

</div>     
        
        
<div class="span-19 last line">
    <div class="prepend-1 span-4 label">Duraci&oacute;n M&aacute;xima:</div>
    <div class="span-5">
    	<input type="text" name="max_duration_tpp" id="max_duration_tpp" {if $max_duration_tpp} value="{$max_duration_tpp}"{/if}/>
    </div>
    <div class="span-3 label">Duraci&oacute;n M&iacute;nima:</div>
    <div class="span-4 append-2 last">
	    <input type="text" name="min_duration_tpp" id="min_duration_tpp" {if $min_duration_tpp} value="{$min_duration_tpp}"{/if}/>
    </div>
</div>       
         
                 
<div class="span-19 last line">
    <div class="prepend-1 span-4 label">M&aacute;ximo Acompa&ntilde;antes:</div>
    <div class="span-5">
    	<input type="text" name="max_extras_tpp" id="max_extras_tpp" {if $max_extras_tpp} value="{$max_extras_tpp}"{/if}/>
    </div>
    <div class="span-3 label">M&iacute;nimo Acompa&ntilde;antes:</div>
    <div class="span-4 append-2 last">
	    <input type="text" name="min_extras_tpp" id="min_extras_tpp" {if $min_extras_tpp} value="{$min_extras_tpp}"{/if}/>
    </div>
</div> 
         
<div class="span-19 last line">         
    <div class="prepend-1 span-4 label">Menores de 12 a&ntilde;os:</div>
    <div class="span-5">
    	<input type="text" name="children_tpp" id="children_tpp" {if $children_tpp} value="{$children_tpp}"{/if}/>
    </div>
    <div class="span-3 label">Viajes por Per&iacute;odo: </div>
    <div class="span-4 append-2 last">
       <select id="flights_tpp" name="flights_tpp">
       	<option value="0" {if $flights_tpp eq 0}selected="selected"{/if}>Varios</option>
        <option value="1" {if $flights_tpp eq 1}selected="selected"{/if}>1 solo viaje</option>
       </select>
    </div>
</div> 

<div class="span-19 last line">      
    <div class="prepend-1 span-4 label">Es masivo</div>
    <div class="span-5">
        <input type="checkbox" name="masive_tpp" id="masive_tpp" value="1" {if $masive_tpp eq 1} checked="checked" {/if}/>  
    </div>
</div>    