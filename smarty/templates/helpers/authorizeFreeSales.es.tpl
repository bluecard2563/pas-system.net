{if $pendingFreeSales}
<form name="frmAuthorizeFree" id="frmAuthorizeFree" method="post" action="{$document_root}modules/sales/pAuthorizeFreeSales">
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="prepend-5 span-14 append-5 last line">
        <div class="prepend-3 span-4 label">* Pasar a estado:</div>
        <div class="span-6">
            <select name="selAction" id="selAction" title='El campo "Estado" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$saleStatus item="l" key="k"}
                    <option value="{$k}">{$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

	<div class="append-24 last line">
        <div class="prepend-4 span-5 label">* Observaciones:</div>
		<div class="span-8 append-7 last"><textarea name="txtObservations" id="txtObservations" title="El campo 'Observaciones' es obligatorio. Y debe tener un minimo de 15 caracteres"></textarea></div>
    </div>

	<div class="span-24 last">&nbsp;</div>
	
    <div class="append-24 last line">
        {generate_table_check data=$pendingFreeSales fields=$misc.2 serial=$misc.1 titles=$misc.0 text=$misc.3}
    </div>

    <input type="hidden" name="hdnOneSelection" id="hdnOneSelection" value="" />
</form>
{else}
<div class="append-7 span-10 prepend-7 last ">
	<div class="span-10 error" align="center">
		No existen solicitudes de ventas free pendientes seg&uacute;n el criterio de b&uacute;squeda
	</div>
</div>
{/if}