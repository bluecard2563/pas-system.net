<div class="span-24 last line separator">
	<label>Asignaci&oacute;n de Kits:</label>
</div>

<div class="span-24 last line">
	<div class="prepend-9 span-4 label">Kits Entregados:</div>
	<div class="span-5">
		{if $kitsTotal}{$kitsTotal}{else}0{/if}
	</div>
	<input type="hidden" id="hdnKitsTotal" name="hdnKitsTotal" value="{if $kitsTotal}{$kitsTotal}{else}0{/if}" />
</div>

 <div class="span-24 last line line">
	<div class="prepend-6 span-5 label">* Cantidad:</div>
	<div class="span-5">
		<input type="text" id="txtAmount" name="txtAmount" title="El campo 'Cantidad' es obligatorio." />
	</div>
</div>

<div class="span-24 last line line">
	<div class="prepend-6 span-5 label">* Acci&oacute;n:</div>
	<div class="span-5">
		<select name="selAction" id="selAction" title="El campo 'Acci&oacute;n' es obligatorio.">
			<option value="">- Seleccione -</option>
			{foreach from=$actionList item=al}
			<option value="{$al}">{if $al eq GIVEN}Entrega de Kits{else}Devoluci&oacute;n de Kits{/if}</option>
			{/foreach}
		</select>
	</div>
</div>

<input type="hidden" id="hdnKitByDealerID" name="hdnKitByDealerID" value="{$kitByDealerID}" />
<div class="span-24 last line"></div>
<div class="span-24 last buttons line">
    <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
</div>