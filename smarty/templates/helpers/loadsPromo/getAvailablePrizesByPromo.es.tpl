{if $promo_not_loaded}
		<div class="span-10 append-7 prepend-7 last line" >
			<div class="span-10 error center">
				No existen Ganadores para la promoci&oacute;n escogida.
			</div>
		</div>
{else}
<div class="span-24 last line separator">
		<label>Bases de la Promoci&oacute;n</label>
	</div>

	<div class="append-6 span-12 prepend-6 last line">
			<table id="prizeTable">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						No.
					</td>
					<td align="center" class="tableTitle">
						Premio
					</td>
					<td align="center" class="tableTitle">
						Productos Participantes
					</td>
					<td align="center" class="tableTitle">
						Monto Base
					</td>
					{if $promo.type_prm eq 'RANGE'}
					<td align="center" class="tableTitle">
						Monto L&iacute;mite
					</td>
					{/if}
					<td align="center" class="tableTitle">
						Stock
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="pr" from=$prizesByPromo item="p"}
				<tr {if $smarty.foreach.pr.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						{$smarty.foreach.pr.iteration}
					</td>
					<td align="center" class="tableField">
						{$p.name_pri|htmlall}
					</td>
					<td align="center" class="tableField">
						{$p.name_pbl|htmlall}
					</td>
					<td align="center" class="tableField">
						{$p.base_amount|number_format:2}
					</td>
					{if $promo.type_prm eq 'RANGE'}
					<td align="center" class="tableField">
						{$p.end_value_pbp|number_format:2}
					</td>
					{/if}
					<td align="center" class="tableField" id="text_{$p.serial_pbp}">
						{$p.stock_pri}
					</td>
				</tr>
				{/foreach}
			</TBODY>
			</table>
	</div>
	<div class="span-24 last buttons line" id="navPrizes"></div>

	<div class="span-24 last line separator">
		<label>Ganadores de la Promoci&oacute;n</label>
	</div>

	{if $winnerList}
	<div class="span-24 last line">
		<table id="winnerTable">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle" colspan="5">
						Ganadores de la Promoci&oacute;n "{$promo.name_prm}"
					</td>
				</tr>
				<tr bgcolor="#284787">
					<td align="center" colspan="3" class="tableTitle subTitle">
						Informaci&oacute;n General
					</td>
					<td align="center" colspan="2" class="tableTitle subTitle">
						Premios
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="winner" from=$winnerList item="w"}
				<tr bgcolor="#4173AF" style="color: #FFFFFF;">
					<td class="items">
						No.
					</td>
					<td class="items">
						Comercializador
					</td>
					<td class="items">
						Total Vendido ($)
					</td>
					<td class="items">
						Cantidad
					</td>
					<td class="items">
						Premio
					</td>
				</tr>
				<tr {if $smarty.foreach.winner.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
					<td align="center" class="tableField">
						{$smarty.foreach.winner.iteration}
					</td>
					<td align="center" class="tableField">
						{$w.name_dea}
					</td>
					<td align="center" class="tableField">
						{$w.total_sold}
					</td>
					<td align="center" class="tableField">
						{$w.first_prize.quantity}
					</td>
					<td align="center" class="tableField">
						{$w.first_prize.name_pri}
					</td>
				</tr>
				{if $w.prizes}
					{foreach name="prizes" from=$w.prizes item="p"}
					<tr {if $smarty.foreach.prizes.iteration+$smarty.foreach.winner.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
						<td colspan="3">&nbsp;</td>
						<td align="center" class="tableField">
							{$p.quantity}
						</td>
						<td align="center" class="tableField">
							{$p.name_pri}
						</td>
					</tr>
					{/foreach}
				{/if}
				{/foreach}
			</TBODY>
		</table>
	</div>
	<div class="span-24 last buttons line" id="navWinner"></div>
	<div class="prepend-7 span-10 append-7 last center line">
		<a href="{$document_root}modules/promo/reclaimPromo/pPromoWinnersXLS/{$serial_prm}" target="_blank">Exportar a Excel</a>
	</div>

	<div class="span-24 last line separator">
		<label>Total de Premios Requeridos</label>
	</div>

	<div class="span-10 append-7 prepend-7 last line">
		<table id="neededPrizes">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					Premio
				</td>
				<td align="center" class="tableTitle">
					Cantidad Necesaria
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="needed" from=$neededPrizes item="np"}
			<tr {if $smarty.foreach.needed.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$np.name_pri}
				</td>
				<td align="center" class="tableField">
					{$np.quantity_needed}
					<input type="hidden" name="hdnPrizeNeeded[]" id="hdnPrizeNeeded_{$smarty.foreach.needed.iteration}" value="{$np.serial_pri}" quantity="{$np.quantity_needed}" serial_pbp="{$np.serial_pbp}" />
				</td>
			</tr>
			{/foreach}
		</TBODY>
		</table>
	</div>
	<div class="span-24 last buttons line" id="navNeededPrizes"></div>

	<div class="span-24 last buttons line">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Liquidar Premios" />
		{if $restockNeeded}
		<input type="button" name="btnIncreaseStock" id="btnIncreaseStock" value="Reasignar Stock" />
		<input type="hidden" name="hdnStock" id="hdnStock" value="" title="El stock actual no es suficiente para liquidar a todos los ganadores." />
		{else}
		<input type="hidden" name="hdnStock" id="hdnStock" value="1" title="El stock actual no es suficiente para liquidar a todos los ganadores." />
		{/if}
	</div>

	<div id="loader">
		<div id="ajax_loader"></div>
		<div class="ajax_loader_text">Cargando Stock...</div>
	</div>
	{else}
		<div class="span-10 append-7 prepend-7 last line" >
			<div class="span-10 error center">
				No existen Ganadores para la promoci&oacute;n escogida.
			</div>
		</div>
	{/if}
{/if}	