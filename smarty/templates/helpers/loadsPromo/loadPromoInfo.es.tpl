<div class="span-19 last line">
    <div class="prepend-1 span-4  label"> Zona: </div>
    <div class="span-5">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$zoneList item="l"}
            <option value="{$l.serial_zon}">{$l.name_zon|htmlall}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-3  label">* Pa&iacute;s: </div>
    <div class="span-5" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

{if $appliedTo eq 'MANAGER'}
<div class="span-19 last line">
        <div class="prepend-1 span-4  label">* Representante: </div>
        <div class="span-5 last" id="managerContainer">
            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
</div>
{/if}

{if $appliedTo eq 'CITY' or $appliedTo eq 'DEALER'}
<div class="span-19 last line">
    <div class="prepend-1 span-4  label">* Ciudad: </div>
    <div class="span-5 last" id="cityContainer">
        <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
</div>
{/if}

{if $appliedTo eq 'DEALER'}
<div class="prepend-5 span-6 append-8 label line">* Seleccione los comercializadores:</div>
<div class="prepend-5 span-10 append-4 last line" id="branchContainer">
    <div class="span-4">
        <div align="center">Seleccionados</div>
        <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-4 last" title="El campo de responsables es obligatorio."></select>
    </div>
    <div class="span-2 last buttonPane">
        <br />
        <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
        <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
        <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
        <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
    </div>
    <div class="span-4 last line" id="dealersFromContainer">
        <div align="center">Existentes</div>
        <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last"></select>
    </div>
</div>

<div class="prepend-5 span-10 append-4 last line">
    <center>* Todas los comercializadores en el lado izquierdo deben estar seleccionados
    para poder a&ntilde;adirles a la promoci&oacute;n.</center>
</div>
{/if}


{literal}
<script language="javascript">
/*LIST SELECTOR*/
$("#moveAll").click( function () {
	$("#dealersFrom option").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeAll").click( function () {
	$("#dealersTo option").each(function (i) {
            if($(this).attr('city')==$("#dealersFrom").attr('city')){
		$("#dealersFrom").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
                $(this).remove();
            }
	});
});

$("#moveSelected").click( function () {
	$("#dealersFrom option:selected").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeSelected").click( function () {
	$("#dealersTo option:selected").each(function (i) {
            if($(this).attr('city')==$("#dealersFrom").attr('city')){
		$("#dealersFrom").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" city="'+$(this).attr('city')+'" selected="selected">'+$(this).text()+'</option>');
                $(this).remove();
            }
	});
});
/*END LIST SELECTOR*/

$('#selZone').bind('change',function(){
   loadCountry(this.value);
});
</script>
{/literal}