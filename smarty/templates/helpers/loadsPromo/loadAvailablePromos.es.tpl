{if $promoList}
<div class="prepend-7 span-4 label">* Promoci&oacute;n </div>
<div class="span-5 append-3 last">
	<select name="selPromo" id="selPromo" title='El campo "Promoci&oacute;n" es obligatorio.'>
		<option value="">- Seleccione -</option>
		{foreach from=$promoList item="p" name='promo'}
		<option value="{$p.serial_prm}" repeat_type="{$p.repeat_type_prm}" period_type="{$p.period_type_prm}" repeat="{$p.repeat_prm}" type_sale="{$p.type_sale_prm}" {if $promoList|@count eq 1} selected{/if}>{$p.name_prm}</option>
		{/foreach}
	</select>
</div>
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 error center">
		No existen promociones por liquidar para el criterio seleccionado.
	</div>
</div>
{/if}