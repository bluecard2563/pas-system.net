
<div class="span-19 last line">
    <div class="prepend-1 span-4  label">* Zona: </div>
    <div class="span-5">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$zoneList item="l"}
            <option value="{$l.serial_zon}">{$l.name_zon|htmlall}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-3  label">* Pa&iacute;s: </div>
    <div class="span-5" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
</div>


{if $appliedTo eq 'MANAGER'}
<div class="span-19 last line">
        <div class="prepend-1 span-4  label">* Representante: </div>
        <div class="span-5 last" id="managerContainer">
            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
</div>
{/if}

{if $appliedTo eq 'CITY'}
<div class="span-19 last line">
    <div class="prepend-1 span-4  label">* Ciudad: </div>
    <div class="span-5 last" id="cityContainer">
        <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
</div>
{/if}

{if $appliedTo eq 'DEALER'}
<div class="span-19 last line">
    <div class="prepend-1 span-4  label">* Ciudad: </div>
    <div class="span-5 " id="cityContainer">
        <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
	<div class="span-3  label">* Comercializador: </div>
    <div class="span-5" id="dealerContainer">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
</div>
{/if}


<!--<div class="span-19 last line">
    <div class="prepend-4 span-5  label">* Nombre de la Promoci&oacute;n: </div>
    <div class="span-4 append-6 last">
        <input type="text" id="txtName" name="txtName" title="El campo 'Nombre' es obligatorio."/>
    </div>
</div>-->

{literal}
<script language="javascript">
$('#selZone').bind('change',function(){
   loadCountry(this.value);
   cleanHistory();
   {/literal}{if $appliedTo eq 'CITY'}{literal}
   $('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
   {/literal}{/if}{literal}

   {/literal}{if $appliedTo eq 'MANAGER'}{literal}
   $('#selManager').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
   {/literal}{/if}{literal}
   {/literal}{if $appliedTo eq 'DEALER'}{literal}
   $('#selCity').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
   $('#selDealer').find('option').remove().end().append('<option value="">- Seleccione -</option>').val('');
   {/literal}{/if}{literal}
});
</script>
{/literal}