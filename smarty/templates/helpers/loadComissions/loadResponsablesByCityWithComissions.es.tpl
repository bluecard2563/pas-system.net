{if $responsablesList}
<select name="selResponsible" id="selResponsible" title="El campo 'Responsables' es obligatorio">
  <option value="">- Seleccione -</option>
  {foreach from=$responsablesList item="l" name="responsables"}
        <option value="{$l.serial_usr}" {if $smarty.foreach.responsables.total eq 1}selected{/if}>{$l.name_usr|htmlall}</option>
  {/foreach}
</select>
{else}
	{if $is_report}
		 No existen Responsables con comisiones en la ciudad seleccionada.
	{else}
		 No existen Responsables con comisiones pendientes en la ciudad seleccionada.
	{/if}
	<input type="hidden" name="selResponsible" id="selResponsible" title="El campo 'Responsables' es obligatorio"/>
{/if}
