{if $dealerList}
<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
  {foreach from=$dealerList item="l" name="dealer"}
		<option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
  {/foreach}
</select>
{else}
	{if $is_report}
		"No existen Comercializadores con Comisiones"
	{else}
		"No existen Comercializadores con Comisiones Pendientes"
	{/if}
	<input type="hidden" name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'/>
{/if}