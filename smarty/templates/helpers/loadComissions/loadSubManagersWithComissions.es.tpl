{if $sub_managers}
<select name="selSubManager" id="selSubManager" title='El campo "Sub-representante" es obligatorio'>
  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
  {foreach from=$sub_managers item="l" name="man"}
		<option value="{$l.serial_dea}" {if $smarty.foreach.man.total eq 1}selected{/if}>{$l.name_dea|htmlall}</option>
  {/foreach}
</select>
{else}
	{if !$for_report}
		No existen Sub-Representantes con Comisiones Pendientes
	{/if}
	<input type="hidden" name="selManager" id="selManager" title='El campo "Sub-representante" es obligatorio'/>
{/if}