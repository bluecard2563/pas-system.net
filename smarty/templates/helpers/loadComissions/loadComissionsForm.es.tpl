{if $countryList}
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
		<div class="span-5">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$countryList name="countries" item="l"}
						<option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou|htmlall}</option>
					{/foreach}
				</select>
		</div>
		{if $comissions_to eq 'RESPONSIBLE' or $comissions_to eq 'SUBMANAGER'}
			<div class="span-3 label">* Ciudad:</div>
			<div class="span-4 append-4 last" id="cityContainer">
				<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		{elseif $comissions_to eq 'MANAGER' or $comissions_to eq 'DEALER'}
			<div class="span-3 label">* Representante:</div>
			<div class="span-4 append-4 last" id="managerContainer">
				<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		{/if}
	</div>

	{if $comissions_to eq 'DEALER'}
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label" >Responsables:</div>
			<div class="span-5" id="reponsiblesContainer">
				<select name="selResponsible" id="selResponsible" title='El campo "Responsables" es obligatorio'>
					<option value="">- Seleccione -</option>
				</select>
			 </div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label" >Comercializador:</div>
			<div class="span-5" id="dealerContainer">
				<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
					<option value="">- Seleccione -</option>
				</select>
			 </div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Sucursal de Comercializador:</div>
			<div class="span-5" id="branchContainer">
				<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
						<option value="">- Seleccione -</option>
				</select>
			 </div>
		</div>
	{elseif $comissions_to eq 'RESPONSIBLE'}
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label" >* Responsables:</div>
			<div class="span-5" id="reponsiblesContainer">
				<select name="selResponsible" id="selResponsible" title='El campo "Responsables" es obligatorio'>
					<option value="">- Seleccione -</option>
				</select>
			 </div>
		</div>
	{elseif $comissions_to eq 'SUBMANAGER'}
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label" >* Comercializador:</div>
			<div class="span-5" id="dealerContainer">
				<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
					<option value="">- Seleccione -</option>
				</select>
			 </div>
		</div>
	{/if}

	{if $is_report}
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label" >* Tipo:</div>
		<div class="span-5" >
			<select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
				<option value="">- Seleccione -</option>
				<option value="PENDING">Pendientes</option>
				<option value="PAID">Pagadas</option>
			</select>
		 </div>

		<div class="span-3 label">Fecha del &uacute;ltimo pago:</div>
		<div class="span-4 append-4 last">
			<label id="last_paid_date"></label>
		 </div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label" >Desde:</div>
		<div class="span-5">
			<input type="text" id="txtStartDate" name="txtStartDate"/>
		 </div>

		<div class="span-3 label">Hasta:</div>
		<div class="span-5 append-3 last">
			<input type="text" id="txtEndDate" name="txtEndDate"/>
		 </div>
	</div>
	{/if}

	<div class="span-24 last line buttons">
		<input type="button" name="btnFindComissions" id="btnFindComissions" value="Verificar Comisiones" />
	</div>

	<div class="span-24 last line" id="comissionsContainer"></div>
{else}
	<div class="prepend-6 span-12 append-6 last line">
		<div class="span-12 error center">
			No existen registros de comisiones a ning&uacute;n nivel.
		</div>
	</div>
{/if}