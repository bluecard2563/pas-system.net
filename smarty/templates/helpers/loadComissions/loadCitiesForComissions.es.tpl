{if $cityList}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
        <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
        {foreach name="cities" from=$cityList item="l"}
        	<option value="{$l.serial_cit}" {if $smarty.foreach.cities.total eq 1}selected{/if}>{$l.name_cit|htmlall}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
		No existen ciudades registradas.
    {/if}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio' {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
    </select>
{/if}
