{if $managers}
<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio'>
  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
  {foreach from=$managers item="l" name="man"}
		<option value="{$l.serial_mbc}" {if $smarty.foreach.man.total eq 1}selected{/if}>{$l.name_man|htmlall}</option>
  {/foreach}
</select>
{else}
	{if !$for_report}
		No existen Representantes con Comisiones Pendientes
	{/if}
	<input type="hidden" name="selManager" id="selManager" title='El campo "Representante" es obligatorio'/>
{/if}