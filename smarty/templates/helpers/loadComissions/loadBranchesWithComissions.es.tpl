{if $dealerList}
<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
  {foreach from=$dealerList item="l" name="dealer"}
		<option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
  {/foreach}
</select>
{else}
	{if $is_report}
		"No existen Sucursales de Comercializador con Comisiones"
	{else}
		"No existen Sucursales de Comercializador con Comisiones Pendientes"
	{/if}
	<input type="hidden" name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'/>
{/if}