{if $salesInfo}
<div class="prepend-7 span-10 append-7 last line">
	<table border="1" id="tblRefundApplications" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					Descripci&oacute;n
				</td>
				<td align="center" class="tableTitle">
					Monto:
				</td>
			</tr>
		</THEAD>
		<TBODY>
			<tr bgcolor="#d7e8f9" >
				<td align="center" class="tableField">
					<b>Comisi&oacute;n a favor:</b>
				</td>
				<td align="center" class="tableField">
					$ {$totalSold|number_format:2}
				</td>
			</tr>
			<tr bgcolor="#e8f2fb" >
				<td align="center" class="tableField">
					<b>Comisi&oacute;n a Descontar:</b>
				</td>
				<td align="center" class="tableField">
					$ {$totalRefunded|number_format:2}
				</td>
			</tr>
			<tr bgcolor="#d7e8f9" >
				<td align="center" class="tableField">
					<b>Monto a comisionar:</b>
				</td>
				<td align="center" class="tableField">
					$ {$totalComission|number_format:2}
				</td>
			</tr>
		</TBODY>
	</table>
</div>
<div class="prepend-7 span-10 append-7 last line">
	<center>
		<a 	id="previewComissionsLiquidationLink" 
			href="#" 
			onclick="submit_preview_form()">Ver Pre Liquidaci&oacute;n (PDF)</a>
            <br>
		<a 	id="previewComissionsLiquidationLinkExcel" 
			href="#" 
			onclick="submit_preview_form_excel()">Ver Pre Liquidaci&oacute;n (Excel)</a>
	</center>
</div>
 <div class="span-24 last buttons line">
    	<input type="button" name="btnLiquidar" id="btnLiquidar" value="Liquidar" onclick="submit_form()">
    </div>
{else}
<div class="prepend-7 span-10 append-7 last line">
	<div class="span-10 error">
		No existen registros a liquidar
	</div>
</div>
{/if}