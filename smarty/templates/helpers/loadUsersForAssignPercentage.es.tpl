{if $userList}
        <div class="span-4 append-4 last" id="responsibleContainer">
			<select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
				<option value="">- Seleccione -</option>
					{foreach from=$userList item="l"}
						<option value="{$l.serial_usr}" >{$l.first_name_usr|htmlall} {$l.last_name_usr|htmlall}</option>
					{/foreach}
			</select>
		</div>
{else}
	{if $planetUserList}
		<div class="prepend-7 span-10 append-7 last line">
			<table border="0" id="tblUserForAssignPercentage" style="color: #000000;">
				<THEAD>
					<tr bgcolor="#284787">
						<td align="center" class="tableTitle">
							No.
						</td>
						<td align="center" class="tableTitle">
							Nombre
						</td>
					</tr>
				</THEAD>
				<TBODY>
					{foreach name="alert" from=$planetUserList item="paU"}
					<tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
						<td align="center" class="tableField">
							<input type='radio' name='userTBL' id='userTBL' value='{$paU.serial_usr}'  title='Debe seleccionar un Usuario' />
							{$smarty.foreach.alert.iteration}
						</td>
						<td align="center" class="tableField">
							{$paU.first_name_usr|htmlall} {$paU.last_name_usr|htmlall}
						</td>
					</tr>
					{/foreach}
				</TBODY>
			</table>
		</div>
		<div class="span-24 last buttons" id="pageNavPosition"></div>
		<input type="hidden" name="hdnPages" id="hdnPages" value="{$pages}" />
	{else}
	    {if $serial_mbc neq '' && $type eq 'PLANET'}
		<div class="prepend-5 span-10 append-7 label last line">
			No existen Usuarios de PLANETASSIST.
		</div>
		{elseif $serial_mbc neq ''}
			<div class="span-8 last  label line" style="text-align: left">
				No existen Usuarios del Representante seleccionado.
			</div>

		{elseif $serial_mbc eq ''}
		<div class="span-8 last label line " style="text-align: left">
				Seleccione un representante.
			</div>
		{/if}
	   <!--  <div class="span-17 last line">
			No existen Usuarios de PLANETASSIST.
		</div>-->
	{/if}
{/if}


