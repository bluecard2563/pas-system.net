{if $noData neq 1}
    {if $dealerList}
    <select name="{if $branch}selBranch{else}selDealer{/if}" id="{if $branch}selBranch{else}selDealer{/if}" title='El campo "{if $branch}Sucursal de Comercializador{else}Comercializadora{/if}" es obligatorio'>
      <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
      {foreach from=$dealerList item="l" name="dealer"}
            <option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
      {/foreach}
    </select>
    {else}
        {if $params}
                    No existen comercializadores registrados.
        {/if}
        <select name="selDealer" id="selDealer" title='El campo "Comercializadora" es obligatorio' {if $params}style="display:none"{/if}>
          <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
        </select>
    {/if}
{else}
<select name="{if $branch}selBranch{else}selDealer{/if}" id="{if $branch}selBranch{else}selDealer{/if}" title='El campo "{if $branch}Sucursal de Comercializador{else}Comercializadora{/if}" es obligatorio'>
    <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
</select>
{/if}