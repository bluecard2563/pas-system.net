<div class="estimator-header left-spacing">
	Tarjeta de Cr&eacute;dito
</div>
<hr class="hrBenefits"/>
<div class="prepend-1 span-3">
	<img src="{$document_root}img/cards.jpg" border="0" />
</div>
<div class="span-7">&nbsp;</div>
<div class="span-9 blueText estimatorLine">
	<div class="span-9 blueText estimatorLine"><div class="span-3 estimatorLabelAge last">N&uacute;mero de la Tarjeta:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">C&oacute;digo de Seguridad:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">Nombre en la Tarjeta:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
  		<div class="span-3 estimatorLabel">
  			Fecha de Vencimiento:
  		</div>
  		<div class="estimatorDateBox last">
            <input type="text" class="dark-record" name="txtExpirationDate" id="txtExpirationDate" title='El campo "Fecha de Nacimiento" es obligatorio'>
        </div>
  	</div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">Direcci&oacute;n:</div>
		<div class="span-4 append-1 last">
	    	<textarea class="dark-record textAreaCreditCard" name="txtId" id="txtId" title='El campo "Id" es obligatorio' />
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">Ciudad:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">Estado/Provincia:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">C&oacute;digo Zip/Postal:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
    
    <div class="span-9 blueText estimatorLine">
  		<div class="span-3 estimatorLabel">
  			Pa&iacute;s:
  		</div>
  		<div class="span-4 append-1 last" id="selProductC">
            <select name="selCardCountry" id="selCardCountry" class="select" title="El campo 'Lugar de Destino' es obligatorio.">
                <option value="" serial_pro="">- Seleccione -</option>
                {foreach from=$countries item="cou"}
                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" >{$cou.name_cou}</option>	
                {/foreach}
            </select>
        </div>
  	</div>
    
    
    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Tel&eacute;fono:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
    
    <div class="span-9 blueText estimatorLine">
	    <div class="span-3 estimatorLabelAge last">E-mail:</div>
		<div class="span-4 append-1 last">
	    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
	    </div>
    </div>
</div>
<div class="bottomAdvise span-8">
	<p>
		By providing this information you agree to Elavon&#39;s Policy and Terms of use Privacy Policy: http://www.internetsecure.com/privacy.html Terms of Use: http_//internetsecure.com/termsofuse.html
	<p/>
</div>
{literal}
	<script type="text/javascript">
		$('.dark-record').corner("10px");
		setDefaultCalendar($("#txtExpirationDate"),'+0d','+10y','+1d');
		$('#selCardCountry').sSelect({ddMaxHeight: 100});
	</script>
{/literal}