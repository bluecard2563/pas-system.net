{if $prices}
	<div class="span-4 last" id="{if $isSmall}selTripDaysC{else}selTripDaysFull{/if}">
		<select name="selTripDays" id="selTripDays" class="select" title="El campo 'Cantidad M&aacute;xima de Dias por Viaje' es obligatorio.">
		     <option value="" serial_pro="">- Seleccione -</option>
		     {foreach from=$prices item="pri" key=cid}
		     	<option value="{$pri}" id="tripDays_{$pri}" >{$pri}</option>	
		     {/foreach}
		</select>
	</div>
{else}
	<div class="span-4 redText">
   		Seleccione menos d&iacute;as de viaje.
   	</div>
   	<div class="hideMe">
   		<select name="selTripDays" id="selTripDays" class="select " title="El campo 'Cantidad M&aacute;xima de Dias por Viaje' es obligatorio.">
			<option value="" serial_pro="">- Seleccione -</option>
		</select>
	</div>
{/if}
{if $selectedVal}
	<script type="text/javascript">
		$('#selTripDays').val({$selectedVal});
	</script>
{/if}
{literal}
	<script type="text/javascript">
		$('#selTripDays').sSelect({ddMaxHeight: 300});
	</script>
{/literal}