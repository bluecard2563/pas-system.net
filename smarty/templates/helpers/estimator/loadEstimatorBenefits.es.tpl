<div class="blueText span-15">
	<div class="span-1 estimatorLabelTitle last">
		Cantidad
	</div>
	<div class="span-3 estimatorLabelTitle last">
		Producto
	</div>
	<div class="span-1 estimatorLabelTitle last">
		Precio
	</div>
	<div class="span-3 estimatorLabelTitle last">
		Beneficio
	</div>
	<div class="span-2 estimatorLabelTitle last">
		Cobertura
	</div>
	<div class="span-1 estimatorLabelTitle last">
		Beneficios
	</div>
</div>
<hr class="hrBenefits"/>
<div id="scrollListCartProd">
	{foreach from=$allProducts item=pro key=cid}
		<div class="blueText benefitsListing {if $cid%2 ==0 }dark-record{/if}">
			<div class="span-1 estimatorLabel last">
				{$pro.cards_pod}
			</div>
			<div class="span-3 estimatorLabel last">
				{$pro.name_pbl|htmlall}
				{if $pro.schengen == 'YES'}
					<img src="{$document_root}img/schengen.gif" border="0" />
				{/if}
			</div>
			<div class="span-1 estimatorLabel last">
				{$pro.symbol_cur}&nbsp;{$pro.total_price_pod|number_format:2:",":"."}
			</div>
			<div class="span-3 estimatorLabel last">
				{$pro.pro_benefit_name|htmlall}&nbsp;
			</div>
			<div class="span-2 estimatorLabel last">
				{$pro.pro_benefit_cost|htmlall}&nbsp;
			</div>
			<div class="span-2 estimatorLabel last">
				<span id="dialogAllBenefits_{$cid}" class="estimatorLink">Ver Todos</span>
			</div>
		</div>
	{/foreach}
</div>
<hr/>

<div class="prepend-2 span-11">
	{if $showDates}
		<div class="span-5 estimatorLabel">
			Fecha de Salida: {$txtBeginDate}<br/>
			Fecha de Regreso: {$txtEndDate}<br/>
		</div>
	
		<div class="span-3 estimatorLabel">
			Dias: {$targetDays}<br/>
		</div>
	{/if}
	<div class="span-4 estimatorLabel">
		Precio Total: {$allProducts.0.symbol_cur}&nbsp;{$totalEstimatorPrice|number_format:2:",":"."}<br/>
	</div>
</div>
<div class="prepend-2 span-11">
	<div class="span-11 estimatorLabel">
		*<img src="{$document_root}img/schengen.gif" border="0" />
		Este &iacute;cono aparece junto a los productos aceptados en las Embajadas de los Pa&iacute;ses Schengen.
	</div>
</div>
<div class="span-11">&nbsp;</div>
<input type="hidden" id="errorMessage" value="{$errorMessage}">