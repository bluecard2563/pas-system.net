<form id="estimatorFullForm" action="gogogo" method="post">
	{if $smarty.session.serial_customer_estimator}
		<span class="estimatorOuterLabel prepend-5">Cliente: {$smarty.session.name_customer_estimator}
			<span id="quitCustomer" class="clickable" onmouseout="popUp(event,'tipOne')" onmouseover="popUp(event,'tipOne')">
				<img src="{$document_root}img/inactive.gif" border="0" width="10" height="10"/>
			</span>
		</span>
		<div id="tipOne" class="tip">Click para salir del Carrito de este Cliente</div>
	{/if}
	<div class="span-15">&nbsp;</div>
	<div class="wizard-nav tabsPanel">
		<div class="redTab" id="tab_page_1">
        	<div class="innerRedText">Cotizar</div>
        </div>
        <div class="redTab unselectedTab" id="tab_page_2">
        	<div class="innerRedText">Beneficios</div>
        </div>
        <div class="redTab unselectedTab" id="tab_page_3">
        	{if $cartNotEmpty}
  				<span class="innerRedText clickable" id="goToCartLink">
  					Carrito
  				</span>
  			{else}
  				<div class="innerRedText">
	        		Carrito
	        	</div>
	        {/if}
        </div>
    </div>
	<div class="span-15 estimator wizardpage" id="page_1">
		<div class="span-15 last pushDown">
		    <ul id="alerts" class="alertsEstimator"></ul>
		</div>
		<div class="estimator-header left-spacing" id="nav_next">
			Cotizar
		</div>
		<hr class="hrMargin1">
		
		<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Lugar de Partida:
	  		</div>
	  		<div class="span-4 append-1 last" id="selProductC">
	            <select name="selDeparture" id="selDeparture" class="select" title="El campo 'Lugar de Partida' es obligatorio.">
	                <option value="" serial_pro="">- Seleccione -</option>
	                {foreach from=$countries item="cou"}
	                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" >{$cou.name_cou}</option>	
	                {/foreach}
	            </select>
	        </div>
	  	</div>
		
	  	<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Lugar de Destino:
	  		</div>
	  		<div id="destinationHideBox">
		  		<div class="span-4 append-1 last" id="selProductC">
		            <select name="selDestination" id="selDestination" class="select" title="El campo 'Lugar de Destino' es obligatorio.">
		                <option value="" serial_pro="">- Seleccione -</option>
		                {foreach from=$countries item="cou"}
		                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" >{$cou.name_cou}</option>	
		                {/foreach}
		            </select>
		        </div>
		   	</div>
	        <div id="destinationFixed" class="span-4 append-1 last" style="display: none;">Pa&iacute;s de Partida</div>
	        <input type="hidden" id="destinationRestricted" name="destinationRestricted" value="0">
	  	</div>
	  	
	  	<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Fecha de Salida:
	  		</div>
	  		<div class="estimatorDateBox last">
	            <input type="text" class="dark-record" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha de Salida" es obligatorio'>
	        </div>
	  	</div>
	  	
	  	<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Fecha de Regreso:
	  		</div>
	  		<div class="estimatorDateBox last">
	            <input type="text" class="dark-record" name="txtEndDate" id="txtEndDate" title='El campo "Fecha de Regreso" es obligatorio'>
	        </div>
	  	</div>
	  	
	  	<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			D&iacute;as:
	  		</div>
	  		<div class="estimatorDateBox last" id="daysDiff">
	            0
	        </div>
	  	</div>
	  	
	  	<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Producto:
	  		</div>
	  		<div class="span-4 append-1 last" id="selProductC">
	  			<div id="selProductContainer">
		            <select name="selProduct" id="selProduct" class="select" title="El campo 'Producto' es obligatorio.">
		                <option value="" serial_pro="">- Seleccione -</option>
		            </select>
		    	</div>
            </div>
	  	</div>
	  	<input type="hidden" value="0" name="maxExtrasField" id="maxExtrasField">
	  	
	  	<div class="span-15 blueText estimatorLine hideMe" id="spouseTravelingField">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Viaja con su C&oacute;nyuge:
	  		</div>
	  		<div class="estimatorDateBox last">
	            <input type=checkbox name="checkPartner" id="checkPartner">
	        </div>
	  	</div>
	  	
	  	<div class="span-15 blueText estimatorLine executiveBox hideMe" id="executiveBoxField">
	  		<div class="estimatorLabel executiveTitle">
	  			Ejecutiva y Ejecutiva Premium
	  		</div>
	  		<div class="span-7 estimatorLabel last tripDaysRow">
	  			Este producto permite realizar varios viajes en un a&ntilde;o
	  		</div>
	  		
	  		<hr class="noSpace">
	  		
	  		<div class="span-5 estimatorLabel tripDaysRow">
	  			Cantidad m&aacute;xima de d&iacute;as por viaje:
	  		</div>
            <div id="estimatorTripDays" class="tripDaysRow">
				<div class="span-4 last" id="selProductC">
				    <select name="selTripDays" id="selTripDays" class="select" title="El campo 'Cantidad M&aacute;xima de Dias por Viaje' es obligatorio.">
				    </select>
     			</div>
			</div>
	  	</div>
	  	
	  	<div class="span-15 blueText estimatorLine">
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Viajeros mayores a {$adultMinAge} a&ntilde;os:&nbsp;&nbsp;<span id="maxOverSpan"></span>
	  			<br/>(Incluido el titular)
	  		</div>
	        <div class="span-4 append-1 last" id="adultsInput">
	            <input type="text" class="dark-record" name="txtOverAge" id="txtOverAge" title='El campo "Viajeros mayores de {$adultMinAge} a&ntilde;os" es obligatorio'>
	        </div>
	        <div class="span-4 append-1 last" id="adultsInputAltn" style="display: none;">
	            Este producto no permite mayores
	        </div>
	  	</div>
	  	
	  	<input type="hidden" name="extrasRestricted" id="extrasRestricted" value="BOTH">
	  	
	  	<div class="span-15 blueText estimatorLine" >
	  		<div class="span-5 prepend-2 estimatorLabel">
	  			Viajeros menores a {$adultMinAge} a&ntilde;os:&nbsp;&nbsp;<span id="maxUnderSpan"></span>
	  		</div>
	  		<div class="span-4 append-1 last" id="childrenInput">
	            <input type="text" class="dark-record" width="10" name="txtUnderAge" id="txtUnderAge" title='El campo "Viajeros menores de {$adultMinAge} a&ntilde;os" es obligatorio'>
	        </div>
	        <div class="span-4 append-1 last" id="childrenInputAltn" style="display: none;">
	            Este producto no permite menores
	        </div>
	  	</div>
	  	
	  	<div class="prepend-1 span-8 blueText estimatorLine" >
	  		<div class="span-12 prepend-1" >
	  			&nbsp;
	  			{if $cartNotEmpty}
	  				<span class="red_message">*Tiene productos en el carrito, para verlos de click en el Tab 'Carrito'</span>
	  			{/if}
	  		</div>
	  	</div>
	  	<input type="hidden" value="0" name="isSmall" id="isSmall">
	</div>
	<div class="span-15 estimator wizardpage" id="page_2">
		<div id="benefitsPageContent"> </div>
	</div>
	<div class="span-15 estimator wizardpage" id="page_3">
		<div id="shoppingCartPageContent"> </div>
	</div>
	<!-- 
	<div class="span-15 estimator wizardpage" id="page_4">
		<div id="assistanceCardPageContent"> </div>
	</div>
	<div class="span-15 estimator wizardpage" id="page_5">
		<div id="creditCardPageContent"> </div>
	</div>
	 -->
</form>
<script type="text/javascript">
	loadEstimatorInfo('{$estimatorData}');
</script>