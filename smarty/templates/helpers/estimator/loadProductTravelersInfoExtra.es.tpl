<div class="red_title">
	Acompa&ntilde;ante {$cid+1}
</div>
<div class="span-5 blue_text">
	<div class="span-6 blue_text">
		<div class="span-4 last">
	    	* Identificaci&oacute;n:
	    </div>
		<input type="text" name="extra[{$cid}][idExtra]" class="text" id="idExtra_{$cid}" elementID="{$cid}" title='El campo "Identificaci&oacute;n" para el Acompa&ntilde;ante {$cid+1} es obligatorio'/ value="{$extra.idExtra}">
		<input type="hidden" name="extra[{$cid}][serialCus]" id="serialCus_{$cid}" value=""/>
	</div>
	<div class="span-6 blue_text">
		<div class="span-4 last">
	    	* Nombre:
	    </div>
		<input type="text"  id="nameExtra_{$cid}" class="text" name="extra[{$cid}][nameExtra]" title='El campo "Nombre" es obligatorio' value="{$extra.nameExtra|htmlall}"/>
	</div>
	<div class="span-6 blue_text">
		<div class="span-4 last">
	    	* Fecha de Nacimiento:
	    </div>
		<input type="text"  id="birthdayExtra_{$cid}" class="text-med" name="extra[{$cid}][birthdayExtra]" elementID="{$cid}" title='El campo "Fecha de Nacimiento" es obligatorio' value="{$extra.birthdayExtra}"/>
	</div>
</div>
<div class="span-5 blue_text last">
	<div class="span-5 blue_text last" id="selectRegister">
		<div class="span-5 last">
	    	* Tipo de Relaci&oacute;n con el titular:
	    </div>
        <select  id="selRelationship_{$cid}" name="extra[{$cid}][selRelationship]" title='El campo "Tipo de Relaci&oacute;n" es obligatorio.' elementID="{$cid}">
                <option value="">- Seleccione -</option>
             {foreach from=$relationshipTypeList item="l"}
                 <option value="{$l}" {if $l eq $extra.selRelationship }selected{/if}>{$global_extraRelationshipType.$l}</option>
             {/foreach}
            </select>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-4 last">
	    	* Apellido:
	    </div>
        <input type="text"  id="lastnameExtra_{$cid}" class="text" name="extra[{$cid}][lastnameExtra]" title='El campo "Apellido" es obligatorio' value="{$extra.lastnameExtra|htmlall}"/>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-4 last">
	    	Email:
	    </div>
        <input type="text"  id="emailExtra_{$cid}" class="text" name="extra[{$cid}][emailExtra]" elementID="{$cid}" title='El campo "Email" es obligatorio' value="{$extra.emailExtra}"/>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-6 clickable" id="questionsExtra_{$cid}" style="display: none;">
	    	<u>Configurar Pregutas de Adulto Mayor</u>
	    </div>
	</div>
</div>
<div id="hiddenAnswers_{$cid+1}"></div>