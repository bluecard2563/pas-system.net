<div class="span-16 last line title">
	Preguntas Adulto Mayor<br />
</div>

{foreach from=$questionList item="q"}
<div class="span-16 last line">
	<div class="prepend-1 span-12">
		{$q.text_qbl|htmlall}
	</div>
	<div class="span-1 append-1 last">
	{if $q.type_que eq YN}
	<select name="question_{$customerLocation}_{$q.serial_que}" id="question_{$customerLocation}_{$q.serial_que}" title="Es necesario responder todas las preguntas para poder continuar">
		<option value="">- Seleccione -</option>
		<option value="YES" {if $q.yesno_ans eq YES}selected{/if}>S&iacute;</option>    
		<option value="NO" {if $q.yesno_ans eq NO}selected{/if}>No</option>
	</select>
	{else}
		<textarea name="txaAns" id="txaAns" value="" title="Es necesario responder todas las preguntas para poder continuar" class="textArea"></textarea>
	{/if}
	</div>
</div>
<div class="span-16 last line">
</div>
{/foreach}