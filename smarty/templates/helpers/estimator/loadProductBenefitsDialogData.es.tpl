<div class="prepend-1 estimator-header left-spacing span-8">
	{$pro.name_pbl|htmlall}
</div>
<hr class="hrBenefits">
<table border="0" id="theTable">
   	<THEAD>
       	<tr>
            <td align="center">
                Beneficios
            </td>
            <td align="center">
                Cobertura
            </td>
		</tr>
	</THEAD>
	<TBODY>
	{foreach from=$pro.allBenefits item=ben key=cid}
		<tr {if $cid%2 ==0 }bgcolor="#CCCCCC"{else}bgcolor="#FFFFFF"{/if} >
			<td align="center" class="tableField pushLeft">
               	{$ben.description_bbl|htmlall}
               </td>
               <td align="center" class="tableField pushLeft">
               	{if $ben.price_bxp == 0}
					{$ben.description_cbl|htmlall}
				{else}
					{$ben.symbol_cur|htmlall}
					{$ben.price_bxp|number_format:2:",":"."}
				{/if}
				{if $ben.restriction_price_bxp > 0}
					({$ben.symbol_cur}{$ben.restriction_price_bxp|htmlall} {$ben.description_rbl|htmlall})
				{/if}
               </td>
           </tr>
	{/foreach}
</TBODY>
</table>
<div class="span-17 last pageNavPosition" id="pageNavPosition"></div>

<hr class="hrBenefits"/>

{literal}
<script type="text/javascript">
        /*@object: Pager
         *@Description: JS library that paginates a table.
         *@Params: IDTable, Max Items per Page, Label for 'Next', Label for 'Prev'
         **/
        var pager = new Pager('theTable', 7 , 'Siguiente', 'Anterior');
        pager.init(25); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
</script>
{/literal}

<div class="blueText estimatorLineBenefits">
	<div class="span-10 estimatorLabel imRed last">
		Precio del Producto: {$pro.allBenefits[0].symbol_cur}{$pro.total_price_pod|number_format:2:",":"."}
	</div>
</div>
<br/>
<div class="blueText span-17">
	<div class="span-5 simpleLabel boldText">Mayores de Edad Incluidos:</div>
	<div class="span-2 simpleLabel">{$adultsIncluded}</div>
</div>
<div class="blueText span-17">
	<div class="span-5 simpleLabel boldText">Menores de Edad Incluidos:</div>
	<div class="span-2 simpleLabel">{$childrenIncluded}</div>
</div>
<div class="blueText span-17">
	<div class="span-8 simpleLabel boldText">M&aacute;ximo n&uacute;mero de acompa&ntilde;antes por tarjeta:</div>
	<div class="span-3 simpleLabel">{$maxExtras}</div>
</div>