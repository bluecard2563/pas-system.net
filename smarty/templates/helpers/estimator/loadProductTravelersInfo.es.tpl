<div id="scrollable-field">
	<input type="hidden" value="{$cardIndex}" name="cardIndex" id="cardIndex">
	<div class="red_title">
		Informaci&oacute;n del Titular
	</div>
	<img src="{$document_root}img/customer/dotted-line.png" border="0" class="push_right"/>
	<div class="inner_box span-11" id="holderContainer">
		<div id="hiddenAnswers_0">
			{foreach from=$product.holder.answers item=answ key=cid}
				<input type="hidden" value="{$answ}" id="answer_0_{$cid}" name="answer_0_{$cid}">
			{/foreach}
		</div>
		<div class="span-5 blue_text">
			<div class="span-6 blue_text">
				<div class="span-4 last">
			    	* Identificaci&oacute;n:
			    </div>
				<input type="text" name="txtDocumentCustomer" class="text" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' value="{$product.holder.document_cus|htmlall}"/>
			</div>
			<div class="span-6 blue_text">
				<div class="span-4 last">
					* Apellido:
				</div>
			    <input type="text" name="txtLastnameCustomer" class="text" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' value="{$product.holder.last_name_cus|htmlall}"/>
			</div>
			<div class="span-6 blue_text" id="selectRegister">
				<div class="span-4 last">
					* Pa&iacute;s de Residencia:
				</div>
				<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
		           	<option value="">- Seleccione -</option>
		               {foreach from=$countries item="cou"}
		                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" {if $cou.serial_cou == $product.holder.serial_cou}selected{/if}>{$cou.name_cou|htmlall}</option>	
		               {/foreach}
		        </select>
			</div>
			<div class="span-6 blue_text" id="selectRegister">
				<div class="span-4 last">
					* Direcci&oacute;n:
				</div>
				<input type="text" name="txtAddress" id="txtAddress" class="text" title='El campo "Direcci&oacute;n" es obligatorio' value="{$product.holder.address_cus|htmlall}"/>
			</div>
			<div class="span-6 blue_text">
				<div class="span-4 last">
					Celular:
				</div>
				<input type="text" name="txtCellphoneCustomer" class="text" id="txtCellphoneCustomer" value="{$product.holder.cellphone_cus}"/>
			</div>
			<div class="span-6 blue_text">
				<div class="span-4 last">
					*Contacto en caso de Emergencia:
				</div>
		        <input type="text" name="txtRelative" id="txtRelative" class="text" title='El campo "Contacto en caso de Emergencia" es obligatorio' value="{$product.holder.relative_cus|htmlall}"/>
			</div>
		</div>
		<div class="span-5 blue_text last">
			<div class="span-5 blue_text last">
				<div class="span-4 last">
			    	* Nombre:
			    </div>
		        <input type="text" name="txtNameCustomer" id="txtNameCustomer" class="text" title='El campo "Nombre" es obligatorio' value="{$product.holder.first_name_cus|htmlall}"/>
			</div>
			<div class="span-5 blue_text last">
				<div class="span-4 last">
			    	* Fecha de Nacimiento:
			    </div>
		        <input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" class="text-med" title='El campo "Fecha de Nacimiento" es obligatorio' value="{$product.holder.birthdate_cus}"/>
			</div>
			<div class="span-5 blue_text last" id="selectRegister">
				<div class="span-4 last">
			    	* Ciudad:
			    </div>
			    <div id="cityContainerCustomer">
		        	<select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
		        	{if $cityList}
				        <option value="">- Seleccione -</option>
				        {foreach name="cities" from=$cityList item="l"}
				        	<option value="{$l.serial_cit}" {if $smarty.foreach.cities.total eq 1 || $l.serial_cit eq $product.holder.serial_cit }selected{/if}>{$l.name_cit}</option>
				        {/foreach}
					{else}
					      <option value="">- Seleccione -</option>
					{/if}
					</select>
		        </div>
			</div>
			<div class="span-5 blue_text last">
				<div class="span-4 last">
			    	* Tel&eacute;fono:
			    </div>
		        <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" class="text" title='El campo "Tel&eacute;fono" es obligatorio' value="{$product.holder.phone1_cus}"/>
			</div>
			<div class="span-5 blue_text last">
			 	<div class="span-4 last">
			    	* Email:
			    </div>
		        <input type="text" name="txtMailCustomer" id="txtMailCustomer" class="text" title='El campo "Email" es obligatorio' value="{$product.holder.email_cus}"/>
			</div>
			<div class="span-5 blue_text last">
				<div class="span-4 last">
			    	*Tel&eacute;fono del contacto:
			    </div>
		       	<input type="text" name="txtPhoneRelative" id="txtPhoneRelative" class="text" title='El campo "Tel&eacute;fono del contacto" es obligatorio' value="{$product.holder.relative_phone_cus}"/>
			</div>
			<div class="span-5 blue_text last">
				<div class="span-5 last clickable" id="questionsLinkHolder" style="display: none;">
			    	<u>Configurar Pregutas de Adulto Mayor</u>
			    </div>
			</div>
		</div>
	</div>
	
	{if $product.extras|@count > 0}
		<div class="span-12">&nbsp;</div>
		<div class="red_title">
			Informaci&oacute;n de los acompa&ntilde;antes
		</div>
		<img src="{$document_root}img/customer/dotted-line.png" border="0" class="push_right"/>
	{/if}
	{foreach from=$product.extras item=extra key=cid}
		<div class="inner_box span-11 line" id="extra_{$cid}">
			<div class="red_title">
				Acompa&ntilde;ante {$cid+1}
			</div>
			<div class="span-5 blue_text">
				<div class="span-6 blue_text">
					<div class="span-4 last">
				    	* Identificaci&oacute;n:
				    </div>
					<input type="text" name="extra[{$cid}][idExtra]" class="text" id="idExtra_{$cid}" elementID="{$cid}" title='El campo "Identificaci&oacute;n" para el Acompa&ntilde;ante {$cid+1} es obligatorio'/ value="{$extra.idExtra}">
					<input type="hidden" name="extra[{$cid}][serialCus]" id="serialCus_{$cid}" value=""/>
				</div>
				<div class="span-6 blue_text">
					<div class="span-4 last">
				    	* Nombre:
				    </div>
					<input type="text"  id="nameExtra_{$cid}" class="text" name="extra[{$cid}][nameExtra]" title='El campo "Nombre" es obligatorio' value="{$extra.nameExtra|htmlall}"/>
				</div>
				<div class="span-6 blue_text">
					<div class="span-4 last">
				    	* Fecha de Nacimiento:
				    </div>
					<input type="text"  id="birthdayExtra_{$cid}" class="text-med" name="extra[{$cid}][birthdayExtra]" elementID="{$cid}" title='El campo "Fecha de Nacimiento" es obligatorio' value="{$extra.birthdayExtra}"/>
				</div>
			</div>
			<div class="span-5 blue_text last">
				<div class="span-5 blue_text last" id="selectRegister">
					<div class="span-5 last">
				    	* Tipo de Relaci&oacute;n con el titular:
				    </div>
			        <select  id="selRelationship_{$cid}" name="extra[{$cid}][selRelationship]" title='El campo "Tipo de Relaci&oacute;n" es obligatorio.' elementID="{$cid}">
		                 <option value="">- Seleccione -</option>
			             {foreach from=$relationshipTypeList item="l"}
			                 <option value="{$l}" {if $l eq $extra.selRelationship }selected{/if}>{$global_extraRelationshipType.$l}</option>
			             {/foreach}
		             </select>
				</div>
				<div class="span-5 blue_text last">
					<div class="span-4 last">
				    	* Apellido:
				    </div>
			        <input type="text"  id="lastnameExtra_{$cid}" class="text" name="extra[{$cid}][lastnameExtra]" title='El campo "Apellido" es obligatorio' value="{$extra.lastnameExtra|htmlall}"/>
				</div>
				<div class="span-5 blue_text last">
					<div class="span-4 last">
				    	Email:
				    </div>
			        <input type="text"  id="emailExtra_{$cid}" class="text" name="extra[{$cid}][emailExtra]" elementID="{$cid}" title='El campo "Email" es obligatorio' value="{$extra.emailExtra}"/>
				</div>
			</div>
			<div class="span-5 blue_text last">
				<div class="span-6 clickable" id="questionsExtra_{$cid}" style="display: none;">
			    	<u>Configurar Pregutas de Adulto Mayor</u>
			    </div>
			</div>
			<div id="hiddenAnswers_{$cid+1}">
				{foreach from=$extra.answers item=answ key=cidd}
					<input type="hidden" value="{$answ}" id="answer_{$cid+1}_{$cidd}" name="answer_{$cid+1}_{$cidd}">
				{/foreach}
			</div>
		</div>
	{/foreach}
	<div class="span-11 line">&nbsp;</div>
</div>
<div id="questionsDialog" title="Preguntas del Adulto Mayor">
	{include file="templates/fEstimatorElderlyQuestions.tpl"}
</div>