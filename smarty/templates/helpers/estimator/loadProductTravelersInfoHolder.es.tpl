<div id="hiddenAnswers_0"></div>
<div class="span-5 blue_text">
	<div class="span-6 blue_text">
		<div class="span-4 last">
	    	* Identificaci&oacute;n:
	    </div>
		<input type="text" name="txtDocumentCustomer" class="text" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' value="{$holder.document_cus|htmlall}"/>
	</div>
	<div class="span-6 blue_text">
		<div class="span-4 last">
			* Apellido:
		</div>
	    <input type="text" name="txtLastnameCustomer" class="text" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' value="{$holder.last_name_cus|htmlall}"/>
	</div>
	<div class="span-6 blue_text" id="selectRegister">
		<div class="span-4 last">
			* Pa&iacute;s de Residencia:
		</div>
		<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
           	<option value="">- Seleccione -</option>
               {foreach from=$countries item="cou"}
                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" {if $cou.serial_cou == $holder.serial_cou}selected{/if}>{$cou.name_cou|htmlall}</option>	
               {/foreach}
        </select>
	</div>
	<div class="span-6 blue_text" id="selectRegister">
		<div class="span-4 last">
			* Direcci&oacute;n:
		</div>
		<input type="text" name="txtAddress" id="txtAddress" class="text" title='El campo "Direcci&oacute;n" es obligatorio' value="{$holder.address_cus|htmlall}"/>
	</div>
	<div class="span-6 blue_text">
		<div class="span-4 last">
			Celular:
		</div>
		<input type="text" name="txtCellphoneCustomer" class="text" id="txtCellphoneCustomer" value="{$holder.cellphone_cus}"/>
	</div>
	<div class="span-6 blue_text">
		<div class="span-4 last">
			*Contacto en caso de Emergencia:
		</div>
        <input type="text" name="txtRelative" id="txtRelative" class="text" title='El campo "Contacto en caso de Emergencia" es obligatorio' value="{$holder.relative_cus|htmlall}"/>
	</div>
</div>
<div class="span-5 blue_text last">
	<div class="span-5 blue_text last">
		<div class="span-4 last">
	    	* Nombre:
	    </div>
        <input type="text" name="txtNameCustomer" id="txtNameCustomer" class="text" title='El campo "Nombre" es obligatorio' value="{$holder.first_name_cus|htmlall}"/>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-4 last">
	    	* Fecha de Nacimiento:
	    </div>
        <input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" class="text-med" title='El campo "Fecha de Nacimiento" es obligatorio' value="{$holder.birthdate_cus}"/>
	</div>
	<div class="span-5 blue_text last" id="selectRegister">
		<div class="span-4 last">
	    	* Ciudad:
	    </div>
	    <div id="cityContainerCustomer">
        	<select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
        	{if $cityList}
		        <option value="">- Seleccione -</option>
		        {foreach name="cities" from=$cityList item="l"}
		        	<option value="{$l.serial_cit}" {if $smarty.foreach.cities.total eq 1 || $l.serial_cit eq $holder.serial_cit }selected{/if}>{$l.name_cit}</option>
		        {/foreach}
			{else}
			      <option value="">- Seleccione -</option>
			{/if}
			</select>
        </div>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-4 last">
	    	* Tel&eacute;fono:
	    </div>
        <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" class="text" title='El campo "Tel&eacute;fono" es obligatorio' value="{$holder.phone1_cus}"/>
	</div>
	<div class="span-5 blue_text last">
	 	<div class="span-4 last">
	    	* Email:
	    </div>
        <input type="text" name="txtMailCustomer" id="txtMailCustomer" class="text" title='El campo "Email" es obligatorio' value="{$holder.email_cus}"/>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-4 last">
	    	*Tel&eacute;fono del contacto:
	    </div>
       	<input type="text" name="txtPhoneRelative" id="txtPhoneRelative" class="text" title='El campo "Tel&eacute;fono del contacto" es obligatorio' value="{$holder.relative_phone_cus}"/>
	</div>
	<div class="span-5 blue_text last">
		<div class="span-5 last clickable" id="questionsLinkHolder" style="display: none;">
	    	<u>Configurar Pregutas de Adulto Mayor</u>
	    </div>
	</div>
</div>