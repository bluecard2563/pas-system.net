<div id="scrollListCardAssistCard">
	{foreach from=$assistCards item=card key=cid}
		<div class="estimator-header left-spacing">
			Tarjeta {$card.number}
		</div>
		<hr class="hrBenefits"/>
		<div class="estimatorCartLabel left-spacing span-5">
			Datos del Titular de la Tarjeta {$card.number}
		</div>
		<div class="estimatorCartLabelRight span-2">
			Titular
			<input type=checkbox name="checkTitular_{$card.number}" id="checkTitular_{$card.number}" class="checkProductCart">
		</div>
		<hr class="hrBenefits">
		<div class="span-9 blueText estimatorLine">
			<div class="span-9 blueText estimatorLine">
				<div class="span-3 estimatorLabelAge last">Id:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
				<div class="span-2 estimatorLabel last"><input type="radio" name="radioIdType_{$card.number}" value="1" checked="checked">CC/RUC</div>
				<div class="span-2 estimatorLabel last"><input type="radio" name="radioIdType_{$card.number}" value="2">Pasaporte</div>
				<div class="span-2 estimatorLabel last"><input type="radio" name="radioIdType_{$card.number}" value="3">Otro</div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Apellidos:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Nombres:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
		  		<div class="span-3 estimatorLabel">
		  			Fecha de Nacimiento:
		  		</div>
		  		<div class="estimatorDateBox last">
		            <input type="text" class="dark-record" name="txtBirthDate_{$card.number}" id="txtBirthDate_{$card.number}" title='El campo "Fecha de Nacimiento" es obligatorio'>
		        </div>
		  	</div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Celular:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">E-mail:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Tel&eacute;fono 2:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Direcci&oacute;n:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
		  		<div class="span-3 estimatorLabel">
		  			Pa&iacute;s de Residencia:
		  		</div>
		  		<div class="span-4 append-1 last" id="selProductC">
		            <select name="selResidence_{$card.number}" id="selResidence_{$card.number}" class="select" title="El campo 'Lugar de Destino' es obligatorio.">
		                <option value="" serial_pro="">- Seleccione -</option>
		                {foreach from=$countries item="cou"}
		                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" >{$cou.name_cou}</option>	
		                {/foreach}
		            </select>
		        </div>
		  	</div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Tipo:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Id" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Contacto en caso de Emergencia:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Contacto en caso de Emergencia" es obligatorio'>
			    </div>
		    </div>
		    
		    <div class="span-9 blueText estimatorLine">
			    <div class="span-3 estimatorLabelAge last">Tel&eacute;fono del contacto:</div>
				<div class="span-4 append-1 last">
			    	<input type="text" class="dark-record" name="txtId" id="txtId" title='El campo "Tel&eacute;fono del contacto" es obligatorio'>
			    </div>
			</div>
		</div>
		<script type="text/javascript">
			loadAssistCardRow({$card.number});
		</script>
	{/foreach}
</div>

<hr class="hrBenefits">
<div class="grayBack">
	<div class="blueText estimatorLineBenefits">
		<div class="span-2 estimatorLabel last">
			Cantidad
		</div>
		<div class="span-3 estimatorLabelBenefits last">
			Producto
		</div>
		<div class="span-3 estimatorLabelBenefits last">
			Precio
		</div>
	</div>
	<hr class="hrBenefits"/>
	<div id="scrollListProdAssistCard">
		{foreach from=$allProducts item=pro key=cid}
			<div class="blueText estimatorLineBenefits {if $cid%2 ==0 }dark-record{/if}">
				<div class="span-2 estimatorLabel last">
					<div class="span-1 last">
						<input type=checkbox name="checkProduct" id="checkProduct_{$pro.pro_serial}" class="checkProductCart">
					</div>
					<div class="span-1 last">
						{$pro.quantity}
					</div>
				</div>
				<div class="span-3 estimatorLabelCart last">
					{$pro.name}
				</div>
				<div class="span-3 estimatorLabelCart last">
					{$pro.totalCost}
				</div>
			</div>
		{/foreach}
	</div>
</div>
<div class="span-7">&nbsp;</div>
<div class="estimatorLabel span-7 last">
	<div class="prepend-3 span-2">
		Precio Total:
	</div>
	<div class="span-2 last">
		<div class="dark-record priceCart">{$totalPrice}</div>
	</div>
</div>

{literal}
	<script type="text/javascript">
		$('.dark-record').corner("10px");
		$('#scrollListCardAssistCard').jScrollPane({showArrows:true });
		$('#scrollListProdAssistCard').jScrollPane({showArrows:true });
	</script>
{/literal}