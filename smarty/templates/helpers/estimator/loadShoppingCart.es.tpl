<div class="estimator-header left-spacing">
	Carrito de Compras
</div>
<hr class="hrBenefits"/>
<div class="estimatorCartLabel left-spacing">
	Datos del Producto
</div>
<div class="span-1 pushRight ">
	<img src="{$document_root}img/carrito.jpg" border="0" />
</div>

<hr class="hrBenefits">
<div class="grayBack">
	<div class="blueText estimatorLineBenefits">
		<div class="span-2 estimatorLabel last">
			Cantidad
		</div>
		<div class="span-6 estimatorLabel last">
			Producto
		</div>
		<div class="span-2 estimatorLabel last">
			Precio
		</div>
		<div class="span-2 estimatorLabel last">
			Condiciones
		</div>
	</div>
	<hr class="hrBenefits"/>
	<div id="scrollListCartProd">
		{foreach from=$cartProducts item=pro key=cid}
			<div class="blueText estimatorLineBenefits {if $cid%2 ==0 }dark-record{/if}">
				<div class="span-2 estimatorLabelCart last">
					<div class="span-1 last">
						<input type=checkbox name="cartProductSelected_{$cid}" id="cartProductSelected_{$cid}" class="checkProductCart">
					</div>
					<div class="span-1 last">
						{$pro.cards_pod}
					</div>
				</div>
				<div class="span-6 estimatorLabelCart last">
					{$pro.name_pbl|htmlall}
				</div>
				<div class="span-2 estimatorLabelCart last">
					{$pro.symbol_cur}&nbsp;{$pro.total_price_pod|number_format:2:",":"."}
				</div>
				<div class="span-2 estimatorLabelCart last">
					<div class="generalConditions clickable" onclick="loadProductGNCFiles({$pro.serial_pro});">
						<img src="{$document_root}img/products/pdf.png" class="adobePDFImageMini" width="17" height="17"/>
						&nbsp;Abrir
					</div>
				</div>
			</div>
		{/foreach}
	</div>
</div>
<hr/>
<div class="estimatorLabel span-7 last">
	<div class="prepend-3 span-2">
		Precio Total:
	</div>
	<div class="span-2 last">
		<div class="dark-record priceCart">{$cartProducts.0.symbol_cur}&nbsp;{$totalCartPrice|number_format:2:",":"."}</div>
	</div>
</div>
<div class="span-15">&nbsp;</div>
<div class="prepend-3 span-5" >
	<div class="blueButton medWidth" id="addAnotherProduct">Agregar otro producto</div>
</div>
<div class="span-15">&nbsp;</div>
<div id="priceWarning">
	<div class="span-15 prepend-2">
		<span class="red_link" >El precio final podr&iacute;a variar una vez ingresada la informaci&oacute;n de los viajeros.</span>
	</div>
	<div class="span-15">&nbsp;</div>
</div>
<script type="text/javascript">
	loadShoppingCartScript("{$estimatorUrl}");
</script>