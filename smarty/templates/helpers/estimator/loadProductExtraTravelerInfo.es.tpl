<div class="span-24">&nbsp;</div>
	<div class="span-14 prepend-4 last title">
    	Acompa&ntilde;ante {$cid+1}
	</div>
	<div class="span-24">&nbsp;</div>
	<div id="extra_{$cid}" class="span-24 last line">
	    <div class="span-24 last line">
	         <div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
	         	<div class="span-5">
	            	<input type="text" name="extra[{$cid}][idExtra]" id="idExtra_{$cid}" elementID="{$cid}" title='El campo "Identificaci&oacute;n" es obligatorio'/>
					<input type="hidden" name="extra[{$cid}][serialCus]" id="serialCus_{$cid}" value=""/>
				</div>
	         <div class="span-3 label">* Tipo de Relaci&oacute;n con el titular:</div>
	         <div class="span-4 append-4 last">
	             <select name="extra[{$cid}][selRelationship]" id="selRelationship_{$cid}" title='El campo "Tipo de Relaci&oacute;n" es obligatorio.' elementID="{$cid}">
	                 <option value="">- Seleccione -</option>
		             {foreach from=$relationshipTypeList item="l"}
		                 <option value="{$l}">{$global_extraRelationshipType.$l}</option>
		             {/foreach}
	             </select>
	         </div>   
	    </div>
	
	    <div class="span-24 last line" >
            <div class="prepend-4 span-4 label">* Nombre:</div> 
            <div class="span-5">
                <input type="text" name="extra[{$pos}][nameExtra]" id="nameExtra_{$pos}"  title='El campo "Nombre" es obligatorio' />
            </div>

            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="extra[{$pos}][lastnameExtra]" id="lastnameExtra_{$pos}" title='El campo "Apellido" es obligatorio' />
            </div>
        </div>
	
	     <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                <input type="text" name="extra[{$pos}][birthdayExtra]" id="birthdayExtra_{$pos}" elementID="{$pos}" title='El campo "Fecha de Nacimiento" es obligatorio'/>
            </div>

            <div class="span-3 label">* Email:</div>
            <div class="span-4 append-4 last">
				<input type="text" name="extra[{$pos}][emailExtra]" id="emailExtra_{$pos}" elementID="{$pos}" title='El campo "Email" es obligatorio'/>
			</div>
        </div>
	
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Tarifa:</div>
            <div class="span-5" id="extraPriceContainer_{$pos}"></div>
			<input type="hidden" name="extra[{$pos}][hdnPriceExtra]" id="hdnPriceExtra_{$pos}" value="0"/>
			<input type="hidden" name="extra[{$pos}][hdnCostExtra]" id="hdnCostExtra_{$pos}" value="0"/>
		</div>
    	<div class="span-24 last line separatorExtra"></div>
	</div>
</div>