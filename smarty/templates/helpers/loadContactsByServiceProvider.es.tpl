{if $contactByServiceProviderList}
    <select name="selContactByServiceProvider" id="selContactByServiceProvider" title='El campo "Operador" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach from=$contactByServiceProviderList item="l"}
        	<option value="{$l.serial_csp}" >{$l.name_csp}</option>
        {/foreach}
    </select>
{else}
    {if $serial_spv neq ''}
		No existen proveedores registrados para este coordinador.
    {/if}
    <select name="selContactByServiceProvider" id="selContactByServiceProvider" title='El campo "Operador" es obligatorio' {if $serial_spv neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}