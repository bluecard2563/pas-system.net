{if $comissionistList}
    <select name="selComissionist" id="selComissionist" title='El campo "Usuario" es obligatorio.' >
        <option value="">- Seleccione -</option>
        {foreach from=$comissionistList item="l"}
        	<option value="{$l.serial_usr}">{$l.first_name_usr} {$l.last_name_usr}</option>
        {/foreach}
    </select>
{else}
<div class="span-5 append-4 last">No existen Usuarios de tipo "Responsable" disponibles.</div>
{/if}