{foreach from=$managersList item=ml}    
    <div manager="{$ml.serial_mbc}" class="manager" id="update">
        <div class="span-24 last line">&nbsp;</div>
        <hr />

        <input type="hidden" id="hdnOfficial{$ml.serial_mbc}" name="hdnOfficial{$ml.serial_mbc}" value="{if $ml.official_mbc eq YES}YES{else}NO{/if}" />
		<input type="hidden" id="hdnExclusive{$ml.serial_mbc}" name="hdnExclusive{$ml.serial_mbc}" value="{if $ml.exclusive_mbc eq YES}YES{else}NO{/if}" />

        <div class="span-24 last line title">{if $ml.exclusive_mbc eq YES}Representante Exclusivo {if $ml.official_mbc eq YES}y Oficial{/if}{else}Sub-representante {if $ml.official_mbc eq YES}Oficial{/if}{/if}:


        {$ml.name_user}</div>
        <input type="hidden" id="hdnUser{$ml.serial_mbc}" name="hdnUser{$ml.serial_mbc}" value="{$ml.serial_usr}" />
        <div class="span-24 last  line">&nbsp;</div>
        <div class="span-24 last  line">
        	<div class="prepend-9 span-3 label">Pa&iacute;s:</div>
            <div class="span-4">{$ml.name_cou|htmlall}</div>
            <input type="hidden" id="hdnCountry{$ml.serial_mbc}" name="hdnCountry{$ml.serial_mbc}" value="{$ml.serial_cou}" />
        </div>
        <div class="span-24 last  line">
        	<div class="prepend-9 span-3 label">Estado:</div>
            {assign var="status" value=$ml.status_mbc}
            <div class="span-4">{$global_status.$status}</div>
            <input type="hidden" id="hdnStatus{$ml.serial_mbc}" name="hdnStatus{$ml.serial_mbc}" value="{$ml.status_mbc}" />
        </div>
        <div class="span-24 last  line">
            <div class="span-3 prepend-10 buttons">
            <input type="button" name="btnEdit"  edit="{$ml.serial_mbc}" id="btnEdit" value="Editar" class="ui-state-default ui-corner-all"/>
        	</div>
        </div>
   </div>
   {literal}
<script type="text/javascript">
	$('#lastButtons').show();
	$('[edit="'+{/literal}{$ml.serial_mbc}{literal}+'"]').click(function(){loadUpdateManagerDialog($(this).attr("edit"));});
	{/literal}{if $ml.exclusive_mbc eq YES}{literal}
		$('[class="hiddenExclusiveCountry"][value="{/literal}{$ml.name_cou|htmlall}{literal}"]').attr('hdnManager','{/literal}{$ml.serial_mbc}{literal}');
		
	{/literal}{/if}{literal}
        {/literal}{if $ml.official_mbc eq YES}{literal}
		$('[class="hiddenOfficialCountry"][value="{/literal}{$ml.name_cou|htmlall}{literal}"]').attr('hdnManager','{/literal}{$ml.serial_mbc}{literal}');

	{/literal}{/if}{literal}
</script>
{/literal}   
{/foreach}

