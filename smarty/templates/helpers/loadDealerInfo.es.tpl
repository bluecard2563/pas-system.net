<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Id:</div>
    <div class="span-5">
        <input type="text" name="txtID" id="txtID" value="{if $data.id_dea}{$data.id_dea}{/if}" title='El campo "Id" es obligatorio.' />
    </div>
    
    <div class="span-3 label">* Nombre:</div>
    <div class="span-4 append-4 last" id="categoryContainer">
        <input type="text" name="txtName" id="txtName" value="{if $data.name_dea}{$data.name_dea|htmlall}{/if}" title='El campo "Nombre" es obligatorio.' />
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Tipo de Comercializador:</div>
    <div class="span-5" id="categoryContainer"> 
        <select name="selDealerT" id="selDealerT" title='El campo "Tipo de Comercializador" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$dTypesList item="dt"}
                <option value="{$dt.serial_dlt}" {if $data.serial_dlt eq $dt.serial_dlt}selected{/if}>{$dt.name}</option>
            {/foreach}
        </select>
    </div>
    
    <div class="span-3 label">* Categor&iacute;a:</div>
    <div class="span-4 append-4 last" id="categoryContainer">
        <select name="selCategory" id="selCategory" title='El campo "Categor&iacute;a" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$categoryList item="l"}
                <option value="{$l}" {if $data.category_dea eq $l}selected{/if}>{$l}</option>
            {/foreach}
        </select> 
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Estado:</div>
    <div class="span-5">
    	<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$statusList item="l"}
                    <option value="{$l}" {if $data.status_dea eq $l}selected{/if}>{$global_status.$l}</option>
                {/foreach}
        </select>
    </div>
    
    <div class="span-3 label">* N&uacute;mero de sucursal de comercializador:</div>
    <div class="span-4 append-4 last" id="branchNumber"></div>        
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Tel&eacute;fono #1:</div>
    <div class="span-5">
    	<input type="text" name="txtPhone1" id="txtPhone1" value="{if $data.phone1_dea}{$data.phone1_dea}{/if}" title='El campo "Tel&eacute;fono 1" es obligatorio'>
    </div>
    
    <div class="span-3 label">* Direcci&oacute;n:</div>
    <div class="span-4 append-4 last">
    	<input type="text" name="txtAddress" id="txtAddress" value="{if $data.address_dea}{$data.address_dea|htmlall}{/if}" title='El campo "Direcci&oacute;n" es obligatorio'>
  	</div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Fecha del Contrato:</div>
    <div class="span-5">
         <input type="text" name="txtContractDate" id="txtContractDate" value="{if $data.contract_date_dea}{$data.contract_date_dea}{/if}" title='El campo "Fecha del Contrato" es obligatorio'>
    </div>
    
    <div class="span-3 label">Tel&eacute;fono 2:</div>
    <div class="span-4 append-4 last">
    	<input type="text" name="txtPhone2" id="txtPhone2" value="{if $data.phone2_dea}{$data.phone2_dea}{/if}">
  	</div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* E-mail #1:</div>
    <div class="span-5">
    	<input type="text" name="txtMail1" id="txtMail1" value="{if $data.email1_dea}{$data.email1_dea}{/if}" title='El campo "E-mail #1" es obligatorio.'>
    </div>
    
    <div class="span-3 label">Fax:</div>
    <div class="span-4 append-4 last">
    	<input type="text" name="txtFax" id="txtFax" value="{if $data.fax_dea}{$data.fax_dea}{/if}">
   	</div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Nombre contacto Cobranzas:</div>
    <div class="span-5">
    	<input type="text" name="txtContact" id="txtContact" title='El campo de "Nombre del Contacto" es obligatorio.' />
    </div>
    
    <div class="span-3 label">* Email de Facturación:</div>
    <div class="span-4 append-4 last">
    	<input type="text" name="txtMail2" id="txtMail2" value="{if $data.email2_dea}{$data.email2_dea}{/if}" title='El campo "E-mail F.E." es obligatorio.' />
   	</div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Email de Cobranzas:</div>
    <div class="span-5">
    	<input type="text" name="txtContactMail" id="txtContactMail" title='El campo de "E-mail del Contacto" es obligatorio.' />
   </div>
    
    <div class="span-3 label">* Tel&eacute;fono del Contacto:</div>
    <div class="span-4 append-4 last">
    	<input type="text" name="txtContactPhone" id="txtContactPhone" title='El campo "Tel&eacute;fono del Contacto" es obligatorio.'/>
   	</div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* N&uacute;mero de visitas al mes:</div>
    <div class="span-5">
		<input type="text" name="txtVisitAmount" id="txtVisitAmount" value="{if $data.visits_number}{$data.visits_number}{/if}" title='El campo de "N&uacute;mero de visitas al mes" es obligatorio.' />
   </div>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">M&iacute;nimo de Stock (%):</div>
	<div class="span-5 last">
		<input type="text" name="txtMinimumKits" id="txtMinimumKits" title='El campo "M&iacute;nimo de Stock" es obligatorio.' value="{$data.minimumKits}" />
	</div>
	
	<div class="span-3 label">Logo:</div>
		<div class="span-4 append-4 last">
            <input type="file" name="txtLogo_dea" id="txtLogo_dea" />
        </div>
</div>   

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">M&iacute;nimo de Stock (%):</div>
	<div class="span-5 last">
		<input type="text" name="txtMinimumKits" id="txtMinimumKits" title='El campo "M&iacute;nimo de Stock" es obligatorio.' value="{$data.minimumKits}" />
	</div>
</div>
   
<div class="span-24 last">
	<input type="hidden" name="hdnSerialDealer" id="hdnSerialDealer" value="{if $data.serial_dea}{$data.serial_dea}{/if}" />
	<input type="hidden" name="hdnSerialManager" id="hdnSerialManager" value="{if $data.serial_mbc}{$data.serial_mbc}{/if}" />
	<input type="hidden" name="hdnManagerRights" id="hdnManagerRights" value="{if $data.manager_rights}{$data.manager_rights}{/if}" />
	<input type="hidden" name="hdnParentComission" id="hdnParentComission" value="{if $data.parent_comission}{$data.parent_comission}{/if}" />
</div>



{literal}
<script type="text/javascript">
$("#txtCode").bind("change",function (){
	$('#hdnFlagCode').val($('#hdnFlagCode').val()+1);
})
setDefaultCalendar($("#txtContractDate"),'-100y','+0d','-20y');
</script>
{/literal} 