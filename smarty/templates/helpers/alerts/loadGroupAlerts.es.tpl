
 <div class="span-24 last line separator">
    	<label>Alertas Pendientes en el Grupo</label>
   </div>
    {if $alertsList}
    <br/>
        <div class="span-24 last title">
                Lista de Alertas Pendientes<br />
        </div>
           <table border="0" id="alertsTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            <input type="checkbox" id="selAll" name="selAll">
                        </td>
                        <td align="center" class="tableTitle">
                            Usuario
                        </td>
                        <td align="center"  class="tableTitle">
                            mensaje
                        </td>
                        <td align="center" class="tableTitle">
                            Ingresada el
                        </td>
                        <td align="center" class="tableTitle">
                            Configurada para el
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="alerts" from=$alertsList item="s"}
                        <tr {if $smarty.foreach.alerts.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                <input type="checkbox" value="{$s.serial_alt}" name="chkAlerts[]" class="alertsChk"/>
                            </td>
                            <td align="center" class="tableField">
                               {$s.first_name_usr|htmlall} {$s.last_name_usr|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.message_alt|htmlall}
                            </td>
                            <td align="center" class="tableField">
                               {$s.in_date_alt}
                            </td>
                            <td align="center" class="tableField">
                                {$s.alert_time_alt}
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
     <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
    <input type="hidden" name="selectedAlerts" id="selectedAlerts" title="Debe seleccionar almenos una alerta.">
     <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Asignar" />
    </div>
    {else}
         <div class="span-10 append-5 prepend-7 label">No existen alertas pendientes para este grupo.</div>
    {/if}
