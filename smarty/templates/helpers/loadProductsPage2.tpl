<div class="span-19 line last" id="benefitsTable">
    <div class="span-19 last line">
        <div class="prepend-1 span-1 subtitle">x
        </div>
        <div class="span-4 subtitle">Beneficios
        </div>
        <div class="span-3 subtitle">Condiciones
        </div>
        <div class="span-2 subtitle">$
        </div>
        <div class="span-2 subtitle">Cobertura
        </div>
        <div class="span-2 subtitle">Valor Restr.
        </div>
        <div class="span-3 append-1 subtitle last">Restricci&oacute;n
        </div>
    </div>

{foreach from=$benefits item="benefit" key="key"}
    {assign var="pos" value=$benefit.serial_ben}
    <div class="span-19 last line">
		<div class="prepend-1  span-1">
			<input type="checkbox" id="status_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][status_bpt]" {if $benefitsByType.$pos} checked="checked" {/if}/>
            {if  $benefitsByType.$pos.serial_bpt}
            <input type="hidden" id="serial_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_bpt]" class="span-2" value="{$benefitsByType.$pos.serial_bpt}"/>
{/if}
        </div>
        <div class="span-4">
        {$benefit.description_bbl}
        </div>
        <div class="span-3">
        <select id="serial_ben_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_con]" class="span-3" >
	         <option value=0 >Ninguna</option>
        {foreach from=$conditions item="condition" key="keycon"}
            <option value={$condition.serial_con} {if $benefitsByType.$pos.serial_con eq $condition.serial_con} selected="selected"{/if}>

                {$condition.description_cbl}
            </option>
        {/foreach}
        </select>
        </div>
        <div class="span-2">
        <select id="currency_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_cur]" class="span-2">
        <option value="" >Ning.</option>

        {foreach from=$currencies item="currency" key="keycur"}
            <option value={$currency.serial_cur} {if $benefitsByType.$pos.serial_cur eq $currency.serial_cur} selected="selected"{/if}>
            {$currency.symbol_cur}
            </option>
        {/foreach}
        </select>
        </div>
        <div class="span-2">
        <input type="text" id="price_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][price_bpt]" class="span-2" {if $benefitsByType.$pos.price_bpt}  value="{$benefitsByType.$pos.price_bpt}"{/if}/>
        </div>
        <div class="span-2">
        <input type="text" id="restriction_price_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][restriction_price_bpt]"  class="span-2" {if $benefitsByType.$pos.restriction_price_bpt}  value="{$benefitsByType.$pos.restriction_price_bpt}"{/if}/>
        </div>
        <div class="span-3 append-1 last">
        <select id="serial_rst_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_rst]" class="span-3">
        	<option value=0 >Ninguna</option>
        {foreach from=$restrictions item="restriction" key="keyrst"}


        <option value={$restriction.serial_rst} {if $benefitsByType.$pos.serial_rst eq $restriction.serial_rst} selected="selected"{/if}>
                {$restriction.description_rbl}
            </option>
        {/foreach}
        </select>
        </div>
	</div>
    {/foreach}
</div>

