{if $servicesListByDealer}
<div class="span-24 last line separator">
	<label>Servicios Adicionales</label> <br />
</div>

<div class="span-24 last line center">
	<i>* El precio del servicio adicional es por persona.</i>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">* Servicios:</div>
	<div class="span-11">
		{foreach from=$servicesListByDealer item="l"}
		<input type="checkbox" name="chkService[]" id="chkService_{$l.serial_sbd}" value="{$l.serial_sbd}" price="{$l.price_sbc}" cost="{$l.cost_sbc}" type_fee="{$l.fee_type_ser}" > 
			{$l.name_sbl|htmlall} <i>(USD {$l.price_sbc} {if $l.fee_type_ser eq 'TOTAL'}por cobertura.{else}por d&iacute;a.{/if})</i> <br>
		{/foreach}
	</div>
</div>
	
<div class="span-24 center last">
	{if !$multisales}
		<b>Precio por Servicios:</b>
	{/if}
	
	<label id="servicePriceContainer" style="font-weight: normal;"></label>
	<input type="hidden" name="hdnServicePrice" id="hdnServicePrice" value="" />
	<input type="hidden" name="hdnServiceCost" id="hdnServiceCost" value="" />
</div>
{/if}