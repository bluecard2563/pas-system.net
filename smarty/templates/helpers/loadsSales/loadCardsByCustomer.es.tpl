{if $error eq 0}
    <br/>
        <div class="span-24 last title">
                Lista de tarjetas del cliente <br />
        </div>
	<div class="span-24 last line">
		<table border="0" id="travelsTable" style="color: #000000;">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            No.
                        </td>
                        <td align="center" class="tableTitle">
                            # de Tarjeta
                        </td>
                        <td align="center"  class="tableTitle">
                            Tipo
                        </td>
                        <td style="width: 220px;" align="center" class="tableTitle">
							
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                    {if $list}
                      {foreach name="corporative" from=$list item="s"}
                        <tr {if $smarty.foreach.corporative.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                               {$smarty.foreach.corporative.iteration}
                            </td>
                            <td align="center" class="tableField">
								{if $s.card_number_sal}
									{$s.card_number_sal} {if $s.card_number_trl}({$s.card_number_trl}){/if}{if $s.masive eq 'YES'} {if $s.sal_serial_sal eq ''} (Principal) {else} (Registro de Clientes){/if}{/if}
								{else}
									N/A
								{/if}
                            </td>
                            <td align="center" class="tableField">
                                {$s.name_pbl|htmlall}
                            </td>
							<td align="center">
								{if $s.masive eq 'YES'}
									{if $s.sal_serial_sal neq ''}
										<a style="cursor: pointer;" id="showCards{$s.serial_sal}" name="showCards{$s.serial_sal}" serialSal="{$s.serial_sal}" product="{$s.name_pbl|htmlall}">Ver Tarjetas</a>
									{else}
										<a style="cursor: pointer;" href="{$document_root}modules/sales/pPrintSalePDF/{$s.serial_sal}/1" target="_blank">Imprimir</a>
									{/if}
								{else}
									{if $s.serial_trl}
										<a href="{$document_root}modules/sales/travelRegister/pPrintRegister/{$s.serial_trl}" target="_blank">Imprimir</a>
									{else}
										<a href="{$document_root}modules/sales/pPrintSalePDF/{$s.serial_sal}/1" target="_blank">Imprimir Adéndum</a>
                                        {if $s.serial_con && USE_NEW_CONTRACT}
                                            | <a href="{$document_root}modules/sales/pPrintContractPDF/{$s.serial_sal}/1" target="_blank">Imprimir Contrato</a>
                                        {/if}
									{/if}
								{/if}
							</td></tr>
                      {/foreach}
                    {else}
						{foreach name="sales" from=$sales item="s"}
                    	<tr bgcolor="#e8f2fb">
                            <td align="center" class="tableField">
                               {$smarty.foreach.sales.iteration}
                            </td>
                            <td align="center" class="tableField">
                               {if $s.card_number_sal}
								  {$s.card_number_sal}
								   {if $s.masive eq 'YES'}
									   {if $s.sal_serial_sal eq ''}
										   (Principal)
									   {else}
										   (Registro de Clientes)
									   {/if}
								   {/if}
							   	{else}
									N/A
								{/if}
                            </td>
                            <td align="center" class="tableField">
                               {$s.name_pbl|htmlall}
                            </td>
							<td align="center">
								{if $s.serial_trl}
									<a href="{$document_root}modules/sales/travelRegister/pPrintRegister/{$s.serial_trl}" target="_blank">Imprimir</a>
								{else}
									<a style="text-align: center; cursor: pointer;" href="{$document_root}modules/sales/pPrintSalePDF/{$s.serial_sal}/1" target="_blank">Imprimir Adéndum</a>
                                    {if $s.serial_con && USE_NEW_CONTRACT}
                                        | <a href="{$document_root}modules/sales/pPrintContractPDF/{$s.serial_sal}/1" target="_blank">Imprimir Contrato</a>
                                    {/if}
								{/if}
								{if $s.masive eq 'YES'}
									{if $s.sal_serial_sal neq ''}
										&emsp;&emsp;&emsp;<a style="text-align: center; cursor: pointer;" id="showCards{$s.serial_sal}" name="showCards{$s.serial_sal}" serialSal="{$s.serial_sal}" product="{$s.name_pbl|htmlall}">Ver Tarjetas</a>
									{/if}
								{/if}
							</td>
                        </tr>
						{/foreach}
                    {/if}
                    </TBODY>
           </table>
	</div>
     <div class="span-24 last pageNavPosition" id="travelPageNavPosition"></div>
{else}
	<div class="span-10 append-7 prepend-7 label">
		<div class="span-10 center error">
		{if $error eq 1}
			 El Cliente no posee Tarjetas o no ha sido autorizada todav&iacute;a.<br>Por favor intente nuevamente.
		{elseif $error eq 2}
			No existe una tarjeta con ese n&uacute;mero o no ha sido autorizada todav&iacute;a.<br>Por favor intente nuevamente.
		{elseif $error eq 3}
			La tarjeta debe ser autorizada antes de continuar.
		{elseif $error eq 4}
			La tarjeta ingresada no est&aacute; dentro de su stock.
		{/if}
		</div>
	 </div>
{/if}