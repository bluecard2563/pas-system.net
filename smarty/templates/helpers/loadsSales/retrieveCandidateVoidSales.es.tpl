{if $myStock}
	Seleccione todas las ventas que desea anular<br/><br/>
	<table border="0" id="stockTable" style="color: #000000;" class="span-13 line">
		<tr bgcolor="#284787">
			<td align="center" class="tableTitle span-1">
				&nbsp;
			</td>
			<td align="center" class="tableTitle span-5">
				N&uacute;mero de tarjeta
			</td>
			<td align="center" class="tableTitle span-2">
				Estado
			</td>
			<td align="center" class="tableTitle span-5">
				Fecha de Venta
			</td>
		</tr>
		{foreach from=$myStock item=sto key=key}
			<tr {if $smarty.foreach.country.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
				<td align="center" class="tableField  span-1">
					<input type="checkbox" name="check_{$key}" id="check_{$key}">
				</td>
				<td align="center" class="tableField  span-5">
					{$key}
				</td>
				<td align="center" class="tableField  span-2">
					{if $sto.status eq 'SOLD'}
						Vendida
					{else}
						No Vendida
					{/if}
				</td>
				<td align="center" class="tableField  span-5">
					{$sto.date_sold}
				</td>
			</tr>
		{/foreach}
	</table>
	<div class="span-11 last pageNavPosition" id="pageNavPositionLocation"></div>
	<div class="span-24 line">&nbsp;</div>
	<div class="prepend-5">
        <input type="submit" name="btnVoidCheckedSales" id="btnVoidCheckedSales" value="Solicitar Anulaci&oacute;n"/>
    </div>
	<input type="hidden" id="hdItemsCount" value="{$totalCards}">
{else}
	La sucursal seleccionada no tiene stock disponible para anular.
{/if}