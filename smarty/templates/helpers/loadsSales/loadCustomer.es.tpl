<div class="span-24 last line separator">
    <label>Datos del Cliente</label>
</div>

{if $data.vip_cus eq 1}
    <div class="span-24 last line">
        <div class="prepend-11 span-4 label">Es cliente VIP</div>
    </div>
{/if}

{if $data.status_cus eq 'INACTIVE'}
    <div class="append-7 span-10 prepend-7 last line">
        <div class=" span-10 error center">
			El <b>Cliente</b> se encuentra <b>en Lista Negra</b>. <br />La <b>venta</b> de una p&oacute;liza al cliente est&aacute; <b>restringida</b>.
		</div>
    </div>
{/if}

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">*Tipo de Documento</div>
    <div id="customerDocumentTypeContainer" class="span-4 append-2 last">
        <select name="selCustomerDocumentTypeTravelRecord" id="selCustomerDocumentTypeTravelRecord" title='El campo "Tipo de Documento" es obligatorio.'>
            <option value="">- Seleccione -</option>
            <option value="c" {if $data.document_type eq 'c'}selected{/if}>Cédula</option>
            <option value="p" {if $data.document_type eq 'p'}selected{/if}>Pasaporte</option>
        </select>
        <input type="hidden" name="hdnValidIdentificationTravel" id="hdnValidIdentificationTravel" value=""/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
    <div class="span-5">
         <input type="text" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' {if $data.document_cus}value="{$data.document_cus}"{/if}/>
         <input type="hidden" name="serialCus" id="serialCus"  {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
         <input type="hidden" name="hdnCustomerTravelHasStatement" id="hdnCustomerTravelHasStatement" value="{$data.customer_statement}"/>
    </div>

    <div class="span-3 label">*Tipo de Cliente</div>
    {if $data.serial_cus}
        <div id="customerType" class="span-4 append-2 last">
            {if $data.type_cus eq 'PERSON'}Persona Natural{elseif $data.type_cus eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}
        </div>
    {else}
        <div class="span-4 append-2 last">
            <select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$typesList item="l"}
                    {if $l eq 'PERSON'}
                        <option value="{$l}">{if $l eq 'PERSON'}Persona Natural{/if}</option>
                    {/if}
                {/foreach}
            </select>
        </div>
    {/if}

</div>

{if  $data.type_cus eq LEGAL_ENTITY}
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Raz&oacute;n Social:</div>
        <div class="span-5">
            <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Raz&oacute;n Social" es obligatorio'  readonly="readonly"
            	{if $data.firstname_cus}value="{$data.firstname_cus|htmlall}"{/if} />
        </div>
    </div>
{else}
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Nombre" es obligatorio'  {if $data.firstname_cus}value="{$data.firstname_cus|htmlall}" {/if} />
        </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' {if $data.lastname_cus}value="{$data.lastname_cus|htmlall}" {/if} />
        </div>
    </div>
{/if}

<div class="span-24 last line" id="genderDiv">
    <div class="prepend-4 span-4 label">* G&eacute;nero:</div>
    <div class="span-5">  
        <input type="radio" name="rdGender" id="rdGender" value="M" {if $data.gender_cus eq 'M'}checked{/if} title='El campo "Genero" es obligatorio.'/> Masculino <br />
        <input type="radio" name="rdGender" id="rdGender" value="F" {if $data.gender_cus eq 'F'}checked{/if} title='El campo "Genero" es obligatorio.'/> Femenino
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
    <div class="span-5">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}" {if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou|htmlall}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-3 label">* Ciudad</div>
    <div class="span-4 append-4 last" id="cityContainerCustomer">
            <select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad de Residencia" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$cityList item="l"}
                <option value="{$l.serial_cit}" {if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit|htmlall}</option>
            {/foreach}
        </select>
     </div>
</div>

{if  $data.type_cus eq LEGAL_ENTITY}
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5">
           <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data.address_cus}value="{$data.address_cus|htmlall}"{/if} />
        </div>
    </div>
{else}
	<div class="span-24 last line" id="divAddress1">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
            <input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" title='El campo "Fecha de Nacimiento" es obligatorio' {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}/>
        </div>

        <div class="span-3 label">* Direcci&oacute;n:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data.address_cus}value="{$data.address_cus|htmlall}"{/if} />
        </div>
    </div>
{/if}

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
    <div class="span-5">
        <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" title='El campo "Tel&eacute;fono 1" es obligatorio' {if $data.phone1_cus}value="{$data.phone1_cus}"{/if}/>
    </div>

    <div class="span-3 label">Tel&eacute;fono 2:</div>
    <div class="span-4 append-4 last">
        <input type="text" name="txtPhone2Customer" id="txtPhone2Customer"  {if $data.phone2_cus}value="{$data.phone2_cus}"{/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">Celular:</div>
    <div class="span-5">
        <input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer"  {if $data.cellphone_cus}value="{$data.cellphone_cus}"{/if}/>
    </div>

    <div class="span-3 label">Email:</div>
    <div class="span-4 append-4 last">
        <input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio'  {if $data.email_cus}value="{$data.email_cus}"{/if}/>
    </div>
</div>

{if  $data.type_cus eq LEGAL_ENTITY}
    <div class="span-24  last line">
        <div class="prepend-4 span-4 label">*Gerente:</div>
        <div class="span-5">
                <input type="text" name="txtManager" id="txtManager" title='El campo "Gerente" es obligatorio' {if $data.relative_cus}value="{$data.relative_cus|htmlall}"{/if}/>
        </div>

        <div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio' {if $data.relative_phone_cus}value="{$data.relative_phone_cus}"{/if}/> </div>
    </div>
{else}
	<div class="span-24  last line">
        <div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
        <div class="span-5">
                <input type="text" name="txtRelative" id="txtRelative" title='El campo "Contacto en caso de Emergencia" es obligatorio' {if $data.relative_cus}value="{$data.relative_cus|htmlall}"{/if}/>
        </div>

        <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio' {if $data.relative_phone_cus}value="{$data.relative_phone_cus}"{/if}/> </div>
    </div>
{/if}

<div class="prepend-15 span-4 last line" id="divOpenDialog" style="display:none">
    <div class="span-5"> <a href="#" name="openDialog" id="openDialog">Llenar Declaraci&oacute;n de Salud</a></div>
</div>

<div class="span-24 last">
	<input type="hidden" name="hdnVip" id="hdnVip" value="{$data.vip_cus}" />
	<input type="hidden" name="hdnStatus" id="hdnStatus" value="{$data.status_cus}" />	
</div>

<div id="hiddenAnswers">
	{foreach from=$answerList item=al}
    	<input type="hidden" name="dbquestion_{$al.serial_que}" id="question_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}" />
        <input type="hidden" name="dbanswer_{$al.serial_que}" id="answer_{$al.serial_que}" value="{$al.serial_ans}" />
    {/foreach}
</div>
