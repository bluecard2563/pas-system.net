{if $show_line eq 'YES'}<div style="" id="line" class="span-24 last line separatorExtra"></div>{/if}
{if $hasProducts eq 'NO'}
    <div class="prepend-7 span-13 append-5 last line">
        Este comercializador actualmente no tiene asignado una cartera de productos.<br>
            Por favor comuníquese con el administrador.
    </div>
{else}
<div class="span-24 last line" id="divSaleType">
        <div class="prepend-4 span-4 label">* Tipo de Venta:</div>
        <div class="span-3">
            <input type='radio' name='sale_type' id='sale_type' value='VIRTUAL' checked title='El campo "Tipo de Venta" es obligatorio' /> Virtual
        </div>
        <div class="span-3">
             <input type='radio' name='sale_type' id='sale_type' value='MANUAL' title='El campo "Tipo de Venta" es obligatorio' /> Manual
        </div>
    </div>

     <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* No de Tarjeta:</div>
        <div class="span-5">
            <input type="text" name="txtCardNum" id="txtCardNum" title='El campo "N&uacute;mero de Tarjeta" es obligatorio' value="AUTOMATICO" />
        </div>

        <div class="span-3 label">C&oacute;digo Vendedor:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtCodCounter" id="txtCodCounter" {if $sellerCode}value="{$sellerCode}"{/if} />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtEmissionPlace" id="txtEmissionPlace" {if $data.emissionPlace}value="{$data.emissionPlace|htmlall}"{/if} />
        </div>

        <div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtEmissionDate" id="txtEmissionDate" title='El campo "Fecha de Emisi&oacute;n" es obligatorio' {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Asesor de Venta:</div>
        <div class="span-5">
            <input type="text" name="txtNameCounter" id="txtNameCounter" {if $data.counterName}value="{$data.counterName|htmlall}"{/if} />
        </div>

        <div class="span-3 label">* Comercializador:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtDealer" id="txtDealer" {if $data.name_dea}value="{$data.name_dea|htmlall}"{/if} />
        </div>
    </div>
	{if $sellFree eq 'YES'}
	<div class="span-24 last line" id="divSellFree">
        <div class="prepend-4 span-4 label">* Es Free:</div>
        <div class="span-5">
            <input type="radio" name="rdIsFree" id="rdIsFree" value="YES" title="El campo 'Es Free' es obligatorio." /> S&iacute;
			<input type="radio" name="rdIsFree" id="rdIsFree" value="NO" checked /> No
        </div>
    </div>
	{/if}

	<div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
        <div class="prepend-4 span-4 label">* Moneda:</div>
        <div class="span-5">
			{foreach from=$data.currencies item="l" key="k"}
            <input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur|htmlall}<br/>
			{/foreach}
        </div>
    </div>

<input type="hidden" name="hdnCardNumRPC" id="hdnCardNumRPC" {if $nextAvailableNumber}value="{$nextAvailableNumber}"{/if} />
<input type="hidden" name="hdnEmissionDateRPC" id="hdnEmissionDateRPC" {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
<input type="hidden" name="hdnSerial_cntRPC" id="hdnSerial_cntRPC" {if $serial_cnt}value="{$serial_cnt}"{/if} />
<input type="hidden" name="hdnSerial_couRPC" id="hdnSerial_couRPC" {if $data.serial_cou}value="{$data.serial_cou}"{/if} />
<input type="hidden" name="hdnOfCounterRPC" id="hdnOfCounterRPC" value="{$ofCounter}" />
{/if}