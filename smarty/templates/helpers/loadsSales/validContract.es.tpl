<div class="prepend-7 span-10 append-7">
    <div class="span-10 center {if $valid_contract}success{else}error{/if}">
        {if $valid_contract}
            El contrato es v&aacute;lido.
        {else}
            El contrato no es v&aacute;lido. Por favor contacte al representante de Blue Card m&aacute;s cercano.
        {/if}
    </div>
</div>