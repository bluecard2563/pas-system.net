<div class="prepend-6 span-12 append-6 last line">
	<table border="0" id="alertsTable" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle" colspan="4">
					DATOS DE LA VENTA
				</td>
			</tr>
		</THEAD>
		<TBODY>
			<tr bgcolor="#d7e8f9">
				<td align="center" class="tableField">
					<b>Tipo de Venta:</b>
				</td>
				<td align="center" class="tableField">
					Virtual
				</td>
				<td align="center" class="tableField">
					<b>Fecha de Emisi&oacute;n:</b>
				</td>
				<td align="center" class="tableField">
					{$counter_info.emissionDate}
				</td>
			</tr>
			<tr bgcolor="#e8f2fb">
				<td align="center" class="tableField">
					<b>Lugar de Emisi&oacute;n:</b>
				</td>
				<td align="center" class="tableField">
					{$counter_info.emissionPlace}
				</td>
				<td align="center" class="tableField">
					<b>Comercializador:</b>
				</td>
				<td align="center" class="tableField">
					{$counter_info.name_dea}
				</td>
			</tr>
			<tr bgcolor="#d7e8f9" >
				<td align="center" class="tableField">
					<b>Asesor de Venta:</b>
				</td>
				<td align="center" class="tableField">
					{$counter_info.counterName}
					<input type="hidden" name="hdn_counter_remote_id" id="hdn_counter_remote_id" value="{$serial_cnt}"/>
				</td>
				<td align="center" class="tableField">
					<b>C&oacute;digo del Vendedor:</b>
				</td>
				<td align="center" class="tableField">
					{$counter_info.sellerCode}
				</td>
			</tr>
		</TBODY>
	</table>
</div>