{if $salesList}
    <div class="span-24 last title line">
        <br/>Lista de ventas Elegibles<br />
    </div>
    <div class="span-24 last line center">
        Formato del Archivo <a href="{$document_root}pdfs/formato.xls" target="_blank"><img src="{$document_root}img/page_excel.png" alt="Excel" /></a>
    </div>
    <div class="span-22 append-1 prepend-1 last line" style="height: 450px;">
        <div class="span-22 last line current" id="accordion_container_1" style="display: none;">
            {assign var="count" value="1"}
            {foreach name="sales" from=$salesList item="s"}
                <h3><a href="#">Tarjeta No. {if $s.card_number_sal}{$s.card_number_sal}{else}N/A{/if}</a></h3>
                <div id="sale_info_{$s.serial_sal}" style="height: 250px;">
                    <div class="prepend-2 span-18 last line separator">
                        <label>Datos de la Venta</label>
                    </div>

                    <div class="span-20 last line">
                        <div class="prepend-2 span-4">
                            <label>Producto: </label>
                        </div>
                        <div class="span-4">
                            {$s.name_pbl|htmlall}
                        </div>
                        <div class="prepend-2 span-4">
                            <label>Titular: </label>
                        </div>
                        <div class="span-4 last">
                    {if $s.customer_name}{$s.customer_name|htmlall}{else}{$s.legal_entity_name|htmlall}{/if}
                </div>
            </div>
            <div class="span-20 last line">
                <div class="prepend-2 span-4">
                    <label>Fecha Emisi&oacute;n: </label>
                </div>
                <div class="span-4">
                    {$s.emission_date}
                </div>
                <div class="prepend-2 span-4">
                    <label>Total: </label>
                </div>
                <div class="span-4 last">
                    {$s.total_sal}
                </div>
            </div>
            <div class="span-20 last line">
                <div class="prepend-2 span-4">
                    <label>Estado: </label>
                </div>
                <div class="span-4">
                    {$global_salesStatus[$s.status_sal]}
                </div>
                <div class="prepend-2 span-4">
                    <label>Impuestos: </label>
                </div>
                <div class="span-4 last">
                    {$subs.names_taxes}
                </div>
            </div>

            <div class="prepend-2 span-18 last line separator">
                <label>Registro de Viajes</label>
            </div>

            <div class="prepend-2 span-18 last line center">
                <table border="0" id="salesTable_{$smarty.foreach.sales.iteration}">
                    <THEAD>
                        <tr bgcolor="#284787">
                            <td align="center" class="tableTitle">
                                Fecha del Registro
                            </td>
                            <td align="center" class="tableTitle">
                                Total
                            </td>
                            <td align="center" class="tableTitle">
                                Estado
                            </td>
                            <td align="center" class="tableTitle">
                                Ver Detalles
                            </td>
                            <td align="center" class="tableTitle">
                                Pasajeros Registrados
                            </td>
                        </tr>
                    </THEAD>
                    <TBODY>
                        <tr id="loadNewCustomers" class="loadNewCustomers" sal_serial_sal="{$s.serial_sal}" style="cursor: pointer;">
                            <td align="center" class="tableField" colspan="5">Nueva Carga de Cliente</td>
                        </tr>
                        {foreach name="subSales" from=$s.sales item="subs"}
                            <tr {if $smarty.foreach.subSales.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
                                <td align="center" class="tableField">
                                    {$subs.emission_date}
                                </td>
                                <td align="center" class="tableField">
                                    {$subs.symbol_cur} {$subs.total_sal}
                                </td>
                                <td align="center" class="tableField">
                                    {$global_salesStatus[$subs.status_sal]}
                                </td>
                                <td align="center" class="tableField pointer" id="loadCustomers_{$subs.serial_sal}" sal_serial_sal="{$subs.serial_sal}" has_travelers="{if $subs.registered_people neq 'NO'}YES{else}NO{/if}">
                                    Ver Detalles
                                </td>
                                <td align="center" class="tableField pointer" id="loadTravelers_{$subs.serial_sal}" sal_serial_sal="{$subs.serial_sal}">
                                    {if $subs.registered_people eq 'NO'}
                                        Registrar
                                    {else}
                                        {$subs.registered_people}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    </TBODY>
                </table>
            </div>
            <div class="prepend-2 span-18 buttons last line" id="pagingTable_{$smarty.foreach.sales.iteration}"></div>
        </div>
        {if !$smarty.foreach.sales.last && $smarty.foreach.sales.iteration % 5 == 0}
            {assign var="count" value=$count+1}
        </div><div id="accordion_container_{$count}" style="display: none;">
        {else}
            {if $smarty.foreach.sales.last}
            </div>
        {/if}
    {/if}
{/foreach}
<input type="hidden" name="hdnTotal_sales" id="hdnTotal_sales" value="{$total_masive_sales}" />
</div>

<div class="span-24 last buttons line">
    {assign var="i" value="1"}
    <a onclick="display_pages('first');" class="paging_item" id="link_first"> << Inicio </a>&nbsp;
    {while ($i <= $count)}
        <a onclick="display_pages('{$i}')" class="paging_item current_link" id="link_{$i}"> {$i} </a> &nbsp;
        {assign var="i" value=$i+1}
    {/while}
    <a onclick="display_pages('last');" class="paging_item" id="link_last">Fin >></a>&nbsp;
    <input type="hidden" name="hdnTotalPages" id="hdnTotalPages" value="{$count}" />
</div>

<div class="last line" id="saleInfo" title="Datos del Registro del Viaje"></div>

<div class="last line" id="uploadContainer">
    <div class="prepend-1 span-10 center">
        <ul id="alerts_travelers" class="alerts"></ul>
    </div>
    <form name="frmChooseSales" id="frmChooseSales" method="post" action="{$document_root}modules/sales/pNewSubMasiveSale" enctype="multipart/form-data">
        <input type="hidden" name="sal_serial_sal" id="sal_serial_sal" >

        <div class="span-10 last line center">
            Carga de Archivo: <input type="radio" name="rdType" id="rdUploadType_file" value="FILE" checked />
            Precio Fijo: <input type="radio" name="rdType" id="rdUploadType_fixed" value="FIXED" />
        </div>

        <div class="span-10 last line" {if $currenciesNumber eq 1}style="display:none"{/if}>
            <div class="prepend-1 span-4 label">* Moneda:</div>
            <div class="span-4">
                {foreach from=$currencies item="l" key="k"}
                    <input type="radio" name="rdCurrency" id="rdCurrency{$k}" value="{$l.serial_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur|htmlall}<br/>
                {/foreach}
            </div>
        </div>

        <div class="span-10 last line" id="fileContainer" >
            <div class="prepend-1 span-4 label">* Archivo de carga:</div>
            <div class="span-5 last">
                <input name="file_source" id="file_source" type="file" title='El campo "Archivo de carga" es obligatorio.' /><br />
                <span>(Ingrese &uacute;nicamente archivos excel)</span>
            </div>
        </div>

        <div class="span-10 last line" id="fixedContainer" >
            <div class="prepend-1 span-4 label">Costo fijo:</div>
            <div class="span-5 last">
                <input name="cost_sal" id="cost_sal" type="text" title='El campo "Costo fijo" es obligatorio.' />
            </div>
        </div>


        <div id="customersMasive" class="span-10 prepend-1 last line"></div>
    </form>
</div>




<div id="loadPeopleContainer" title="Registro de Viajeros">
    <div class="last line separator">
        <label>Registro de Pasajeros</label>
    </div>

    <div class="prepend-3 span-8 center">
        <ul id="alerts_dialog" class="alerts"></ul>
    </div>

    <form id="frmRegisterTravelers" name="frmRegisterTravelers" action="{$document_root}modules/sales/pLoadCustomersMasive" method="post" enctype="multipart/form-data">
        <div class="prepend-3 span-2">
            <label>Archivo:</label>
        </div>

        <div class="span-5">
            <input id="travelers_list" name="travelers_list" type="file" enctype="multipart/form-data" method="post" title="Escoja un archivo Excel"/><br />
            <span>(Ingrese &uacute;nicamente archivos excel)</span>
            <input type="hidden" name="hdnSubSaleID" id="hdnSubSaleID" title="La venta no se ha asociado correctamente" />
        </div>
    </form>
</div>

<div id="showTravelersContainer" title="Pasajeros Registrados">
    <div id="showTravelersList" class="prepend-1 span-8 append-1"></div>
</div>

{else}
    <div class="span-10 append-5 prepend-7 label" style="text-align: center;">
        No existen ventas masivas para esta Sucursal de Comercializador.
    </div>
{/if}