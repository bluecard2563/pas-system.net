<div class="span-24 last line" id="customerInvoicingData">
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">* Documento:</div>
        <div class="span-5">
            <input type="text" name="txtCustomerDocument" id="txtCustomerDocument" title='El campo "Documento" es obligatorio.'value="{$customerData.document_cus}">
        </div>

        <div class="span-3 label">* Tipo de Documento:</div>
        <div class="span-6 last">
            <input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
            <input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" /> RUC
            <input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label" id="labelName">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre - Facturaci&oacute;n" es obligatorio.' value="{$customerData.first_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

        <div id="lastNameContainer">
            <div class="span-3 label">*Apellido:</div>
            <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido - Facturaci&oacute;n" es obligatorio' value="{$customerData.last_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
        </div>                
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n - Facturaci&oacute;n" es obligatorio' >
        </div>

        <div class="span-3 label">*Tel&eacute;fono Principal:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal - Facturaci&oacute;n" es obligatorio'>
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
        <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"/></div>

        <div class="span-3 label">Correo Electr&oacute;nico:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="txtEmail" id="txtEmail" title='El campo "Correo Electr&oacute;nico - Facturaci&oacute;n" es obligatorio'>
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
        <div class="span-5"> <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s - Facturaci&oacute;n" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.auxData.serial_cou}selected{/if}>{$l.name_cou}</option>
                {/foreach}
            </select> </div>

        <div class="span-3 label">*Ciudad:</div>
        <div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCity" title='El campo "Ciudad - Facturaci&oacute;n" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$cityList item="l"}
                    <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit}</option>
                {/foreach}
            </select> 
        </div>
    </div>
</div>