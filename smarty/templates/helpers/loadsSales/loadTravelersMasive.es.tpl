<center>
	<div class="span-12 last line">
		<table border="0" class="paginate-1 max-pages-7" id="customers_tale">
			<thead>
				<tr bgcolor="#284787">
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						#
					</td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Nombre
					</td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Fecha de Nacimiento
					</td>
				</tr>
			</thead>

			<tbody>
			{foreach name="customerList" from=$customers item="c"}
			<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.customerList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$smarty.foreach.customerList.iteration}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$c.name_cus}
				</td>
				<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
					{$c.birthdate_cus}
				</td>
			</tr>
			{/foreach}

			</tbody>
		</table>
	</div>
	 <div class="span-12 last line pageNavPosition" id="pageNavPosition"></div>
</center>