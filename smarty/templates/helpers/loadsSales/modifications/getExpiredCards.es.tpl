{if $data.serial_sal}
	{if $data.status_sal eq 'EXPIRED'}
		{if not $data.reactivated}
			<div class="span-20 last line separator">
				<label>Informaci&oacute;n de la Tarjeta:</label>
			</div>

			<div class="span-20 last line">
				<table border="0" id="card_info_table">
					<thead>
						<tr bgcolor="#284787">
							{foreach from=$data.titles item="l"}
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								{$l}
							</td>
							{/foreach}
						</tr>
					</thead>
					<tbody>
						<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9" {*bgcolor="#e8f2fb"*}>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.name_cus}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.name_pro}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.emission_date}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.begin_date}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.end_date}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.total_sal}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$data.total_inv}
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			{if $data.extras}
			<div class="span-20 last line separator">
				<label>Adicionales de la Tarjeta:</label>
			</div>

			<div class="span-20 last line">
				<table border="0" id="extras_table">
					<thead>
						<tr bgcolor="#284787">
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								#
							</td>
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								Nombre
							</td>
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								Tipo de relaci&oacute;n
							</td>
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								Fecha de nacimiento
							</td>
						</tr>
					</thead>
					<tbody>
						{foreach name="extras" from=$data.extras item="l"}
						<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.extras.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$smarty.foreach.extras.iteration}
							</td>
							 <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$l.first_name_cus} {$l.last_name_cus}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$global_extraRelationshipType[$l.relationship_ext]}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$l.birthdate_cus}
							</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
			<div class="span-20 last line pageNavPosition" id="extrasPageNavPosition"></div>
			{/if}

			{if $data.sales_log}
			<div class="span-20 last line separator">
				<label>Historial de la Tarjeta:</label>
			</div>

			<div class="prepend-2 span-16 last line">
				<table border="0" id="log_table">
					<thead>
						<tr bgcolor="#284787">
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								#
							</td>
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								Descripci&oacute;n
							</td>
							<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
								Fecha
							</td>
						</tr>
					</thead>
					<tbody>
						{foreach name="salesLog" from=$data.sales_log item="l"}
						<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.salesLog.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$smarty.foreach.salesLog.iteration}
							</td>
							 <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$global_salesStatus[$l.description_slg.old_status_sal]} -> {$global_salesStatus[$l.description_slg.new_status_sal]}
							</td>
							<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
								{$l.date_slg}
							</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
			<div class="span-20 last line pageNavPosition" id="logPageNavPosition"></div>
			{/if}

			<div class="span-20 last line separator">
				<label>Nueva informaci&oacute;n:</label>
			</div>
			<form name="frmReactivateCard" id="frmReactivateCard" action="{$document_root}modules/sales/modifications/pReactivateCard" method="post">
				<input type="hidden" name="serial_sal" id="serial_sal" value="{$data.serial_sal}" />
				<input type="hidden" name="old_end_date" id="old_end_date" value="{$data.end_date}" />
				<input type="hidden" name="days_sal" id="days_sal" value="{$data.days_sal}" />
				<div class="span-20 last line">
					<div class="prepend-1 span-4 label">* Fecha de inicio:</div>
					<div class="span-5" id="sectorContainer">
						<input type="text" name="txtBeginDateSal" id="txtBeginDateSal" title='El campo "Fecha de inicio" es obligatorio.' />
					</div>

					<div class="span-4 label">* Fecha fin:</div>
					<div class="span-5 last" id="managerContainer">
						<input type="text" name="txtEndDateSal" id="txtEndDateSal" title='El campo "Fecha fin" es obligatorio.' />
					</div>
				</div>

				<div class="span-20 last line">
					<div class="prepend-3 span-4 label">* Raz&oacute;n para la activaci&oacute;n:</div>
					<div class="span-4" id="sectorContainer">
						<textarea name="txtDescription" id="txtDescription" title='El campo "Raz&oacute;n para la activaci&oacute;n" es obligatorio.'></textarea>
					</div>
				</div>

				<div class="span-20 last buttons line">
					<input type="submit" name="btnSubmit" id="btnSubmit" value="Aceptar" />
				</div>
			</form>
		{else}
			<div class="span-7 span-11 prepend-5  last" align="center">
			<div class="span-10 error" align="center">
				La tarjeta ya fue reactivada o tiene una solicitud de reactivaci&oacute;n pendiente.
			</div>
		</div>
		{/if}
	{else}
		<div class="span-7 span-11 prepend-5  last" align="center">
			<div class="span-10 error" align="center">
				La tarjeta no está en estado "Expirada".
			</div>
		</div>
	{/if}
{else}
    <div class="span-7 span-11 prepend-5  last" align="center">
		<div class="span-10 error" align="center">
			No existe una venta para ese n&uacute;mero de tarjeta.
		</div>
    </div>
{/if}

{literal}
<script type="text/javascript">
	var pager;
	var pager2;
	if($("#extras_table").length>0) {
        pager = new Pager('extras_table', 7 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'extrasPageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

	if($("#log_table").length>0) {
        pager2 = new Pager('log_table', 7 , 'Siguiente', 'Anterior');
        pager2.init(7); //Max Pages
        pager2.showPageNav('pager2', 'logPageNavPosition'); //Div where the navigation buttons will be placed.
        pager2.showPage(1); //Starting page
    }

	setDefaultCalendar($("#txtBeginDateSal"),'-0y','+100y','+0d');
	setDefaultCalendar($("#txtEndDateSal"),'-0y','+100y','+0d');
</script>
{/literal}