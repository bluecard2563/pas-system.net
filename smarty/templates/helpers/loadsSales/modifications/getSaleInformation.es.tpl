{if $sale}
<form action="{$document_root}modules/sales/modifications/pAddDays" id="frmAddDays" name="frmAddDays" method="post">
	<div class="span-24 last line separator">
		<label>Datos del Cliente</label>
	</div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
        <div class="span-5">
            {$customer.document_cus}
        </div>
        <div class="span-3 label">*Tipo de Cliente</div>
        <div class="span-4 append-2 last">
			{$global_typeManager[$customer.type_cus]}<br>
        </div>
    </div>

    <div class="span-24 last line" id="divName">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            {$customer.first_name_cus|htmlall}
        </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last">
            {$customer.last_name_cus|htmlall}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
        <div class="span-5">
        	{$sale.aux.customerCountry|htmlall}
        </div>

        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainerCustomer">
        	{$sale.aux.customerCity}
         </div>
    </div>

    <div class="span-24 last line" id="divAddress">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
            {$customer.birthdate_cus}
        </div>

        <div class="span-3 label">* Direcci&oacute;n:</div>
        <div class="span-4 append-4 last">
            {$customer.address_cus|htmlall}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
        <div class="span-5">
             {$customer.phone1_cus}
        </div>

        <div class="span-3 label">Tel&eacute;fono 2:</div>
        <div class="span-4 append-4 last">
			{if $customer.phone2_cus}
				{$customer.phone2_cus}
			{else}
				&nbsp;
			{/if}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Celular:</div>
        <div class="span-5">
            {if $customer.cellphone_cus}
				{$customer.cellphone_cus}
			{else}
				&nbsp;
			{/if}
        </div>

        <div class="span-3 label">* Email:</div>
        <div class="span-4 append-4 last">
            {$customer.email_cus}
        </div>
    </div>

    <div class="span-24  last line" id="divRelative">
        <div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
        <div class="span-5">
        	{$customer.relative_cus|htmlall}
        </div>

        <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
        <div class="span-4 append-4 last">
			{$customer.relative_phone_cus}
		</div>
    </div>

    <div class="span-24 last line separator">
        <label>Datos del Viaje</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Salida:</div>
        <div class="span-5">
            <label id="lblDepartureDate">{$sale.begin_date_sal}</label>
        </div>

        <div class="span-3 label">* Fecha de Retorno:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtArrivalDate" id="txtArrivalDate"  title='El campo "Fecha de Retorno" es obligatorio' value="{$sale.end_date_sal}"/>
			<input type="hidden" id="oldArrivalDate" name="oldArrivalDate" value="{$sale.end_date_sal}" />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer">* Pa&iacute;s de Destino:</div>
        <div class="span-5">
            {$sale.aux.destinationCountry}
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
        	{$sale.aux.destinationCity}
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer"># de D&iacute;as:</div>
         <div class="span-5">
             <label id="txtDaysBetween">{$sale.days_sal}</label>
         </div>
         <input type="hidden" name="hddDays" id="hddDays" value="{$sale.days_sal}"/>
		 <input type="hidden" name="hddOldDays" id="hddOldDays" value="{$sale.days_sal}"/>
    </div>

	<div class="span-24 last buttons line">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar Cambio" >
	</div>
	<input type="hidden" name="serial_sal" id="serial_sal" value="{$sale.serial_sal}" />
</form>
{else}
	<div class="prepend-8 span-8 append-8 last">
	{if $error eq 1}
		<div class="span-8 error">
			El n&uacute;mero de tarjeta ingresada no existe.
		</div>
	{elseif $error eq 2}
		<div class="span-8 error">
			La tarjeta ingresada ya ha tenido ampliaciones de d&iacute;as anteriormente.
		</div>
	{elseif $error eq 3}
		<div class="span-8 error">
			La tarjeta ingresada admite varios viajes, por lo que no se puede a&ntilde;adir d&iacute;as a la misma.
		</div>
	{elseif $error eq 4}
		<div class="span-8 error">
			La tarjeta ha sido reactivada, por lo que no puede ser modificada. Lo sentimos.
		</div>
	{/if}
	</div>
{/if}