{if $sale}
    {if $type eq 'STAND_BY'}
        <form action="{$document_root}modules/sales/modifications/pChangeStandBy" id="frmChangeStatus" name="frmChangeStatus" method="post">
        {/if}

        {if $type eq 'BLOCKED'}
            <form action="{$document_root}modules/sales/modifications/pChangeBlocked" id="frmChangeStatus" name="frmChangeStatus" method="post">
            {/if}
            <div class="span-24 last line separator">
                <label>Datos de la Tarjeta</label>
            </div>

            <div class="span-24 last line">
                {if $type eq 'STAND_BY'}
                    <div class="prepend-6 span-4 label">* Estado:</div>
                    <div class="span-3">
                        {if $status neq 'STANDBY'}
                            <input type="radio" name="rdStatus" id="rdStatus_Active" value="{$sale.status_sal}" checked /> {$global_salesStatus.$status}
                        {else}
                            <input type="radio" name="rdStatus" id="rdStatus_Active" value="{$oldStatus}" checked /> {$global_salesStatus.$oldStatus}
                        {/if}
                    </div>
                    <div class="span-5">
                        <input type="radio" name="rdStatus" id="rdStatus_StandBy" value="STANDBY" {if $sale.status_sal eq 'STANDBY'}checked{/if} /> Stand - By
                    </div>
                {else}
                    <div class="prepend-6 span-4 label">* Estado:</div>
                    <div class="span-3">
                        {if $status neq 'BLOCKED'}
                            <input type="radio" name="rdStatus" id="rdStatus_Active" value="{$sale.status_sal}" checked /> {$global_salesStatus.$status}
                        {else}
                            <input type="radio" name="rdStatus" id="rdStatus_Active" value="{$oldStatus}" checked /> {$global_salesStatus.$oldStatus}
                        {/if}
                    </div>
                    <div class="span-5">
                        <input type="radio" name="rdStatus" id="rdStatus_Blocked" value="BLOCKED" {if $sale.status_sal eq 'BLOCKED'}checked{/if} /> Bloquear
                    </div>
                {/if}
            </div>

            <div class="span-24 last line">
                <div class="prepend-2 span-5 label">Inicio de Cobertura:</div>
                <div class="span-5">
                    {if $status eq 'STANDBY'}
                        <input type="text" name="txtBeginDate" id="txtBeginDate" value="{$sale.begin_date_sal}" title='El campo "Inicio de Cobertura" es obligatorio.'/>
                    {else}
                        {$sale.begin_date_sal}
                    {/if}
                </div>
                <div class="span-5 label">Fin de Cobertura:</div>
                <div class="span-5 last">
                    {if $status eq 'STANDBY'}
                        <input type="text" name="txtEndDate" id="txtEndDate" value="{$sale.end_date_sal}" title='El campo "Fin de Cobertura" es obligatorio.'/>
                    {else}
                        {$sale.end_date_sal}
                    {/if}
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-2 span-5 label">D&iacute;as de Cobertura:</div>
                <div class="span-5">
                    <span id="txtDays">{$sale.days_sal}</span>
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-2 span-5 label">Nombre del Producto:</div>
                <div class="span-5">{$sale.aux.product|htmlall}</div>
                <div class="span-5 label">Nombre del Cliente:</div>
                <div class="span-5 last">{$customer.first_name_cus|htmlall}&nbsp;{$customer.last_name_cus|htmlall}</div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-2 span-5 label">Comercializador:</div>
                <div class="span-5">{$auxData.dealer|htmlall}</div>
                <div class="span-5 label">Sucursal de Comercializador:</div>
                <div class="span-5 last">{$auxData.branch|htmlall}</div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-3 span-4 label">* Comentarios:</div>
                <div class="span-3">
                    <textarea name="txtComments" id="txtComments" title='El campo "Comentarios" es obligatorio.'></textarea>
                </div>
            </div>

            {if $logs}
                <div class="span-24 last line separator">
                    <label>Historial de Cambios</label>
                </div>
                <div class="span-24 last line">
                    <table border="1" id="salesLogTable">
                        <tr class="header">
                            <th>NUM. CAMBIO</th>
                            <th>FECHA</th>
                            <th>CAMBIO</th>
                            <th>USUARIO</th>
                            <th>ESTADO</th>
                            <th>COMENTARIOS</th>
                        </tr>
                        {counter start=0 skip=1 print=false}
                        {foreach name="alert" from=$logs item=curr}
                            <tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                                <td>{counter}</td>
                                <td>{$curr.date_slg}</td>
                                <td>{$global_salesStatus[$curr.description_slg.old_status_sal]} ==> {$global_salesStatus[$curr.description_slg.new_status_sal]}</td>
                                <td>{$curr.first_name_usr}&nbsp;{$curr.last_name_usr}</td>
                                <td>{$global_sales_log_status[$curr.status_slg]}</td>
                                <td>{$curr.description_slg.observations|htmlall}</td>
                            </tr>
                        {/foreach}
                    </table>
                </div>
                <div class="span-24 last buttons" id="salesLogNav"></div>
            {/if}

            <div class="span-24 last buttons line">
                <input type="button" name="btnSubmit" id="btnSubmit" value="{if $needsAproval}Solicitar Autorizaci&oacute;n{else}Registrar Cambio{/if}" onclick="disableBtn();" />
            </div>
            <input type="hidden" name="serial_sal" id="serial_sal" value="{$sale.serial_sal}" />
            <input type="hidden" name="oldStatus" id="oldStatus" value="{$oldStatus}" />
            <input type="hidden" name="status" id="status" value="{$status}" />
            <input type="hidden" name="cardNumber" id="cardNumber" value="{$card_number}" />
            <input type="hidden" name="customerName" id="customerName" value="{$customer.first_name_cus}" />
            <input type="hidden" name="customerSurname" id="customerSurname" value="{$customer.last_name_cus}" />
            <input type="hidden" name="hdnNeedAproval" id="hdnNeedAproval" value="{$needsAproval}" />
        </form>
    </form>
{else}
    <div class="prepend-8 span-8 append-8 last">
        {if $error eq 1}
            <div class="span-8 error">
                El n&uacute;mero de tarjeta ingresada no existe.
            </div>
        {elseif $error eq 2}
            <div class="span-8 error">
                No se puede continuar porque la tarjeta ingresada ya ha sido dada de baja por uno de los siguiente motivos:&nbsp;
                <ul>
                    <li>Reembolso</li>
                    <li>Anulaci&oacute;n</li>
                    <li>Tarjeta Expirada</li>
                    <li>Tarjeta Bloqueada</li>
                </ul>
            </div>
        {elseif $error eq 3}
            <div class="span-8 error">
                El tiempo de {$parameter.value_par} meses para cambiar tarjetas a Stand-By ha terminado. Lo sentimos.
            </div>
        {elseif $error eq 4}
            <div class="span-8 error">
                No se puede continuar porque la tarjeta ingresada tiene un proceso de reembolso pendiente o ya fue aprovado.
            </div>
        {elseif $error eq 5}
            <div class="span-8 error">
                Existe una solicitud de cambio a Stan-By pendiente para esta tarjeta. Lo sentimos.
            </div>
        {elseif $error eq 6}
            <div class="span-8 error center">
                Existe una solicitud pendiente para bloquear esta tarjeta. Lo sentimos.
            </div>
        {elseif $error eq 7}
            <div class="span-8 error center">
                La tarjeta ha sido reactivada por lo que no puede modificarse. Lo sentimos.
            </div>
        {elseif $error eq 8}
            <div class="span-8 error center">
                La tarjeta tiene asistencias relacionadas. <br>Lo sentimos.
            </div>
        {elseif $error eq 9}
            <div class="span-8 error center">
                La tarjeta debe estar facturada. <br>Lo sentimos.
            </div>
        {elseif $error eq 10}
            <div class="span-8 error center">
                La tarjeta no puede ser revisada ya que corresponde a otro canal de distribuci&oacute;n.
            </div>
        {/if}
    </div>
{/if}