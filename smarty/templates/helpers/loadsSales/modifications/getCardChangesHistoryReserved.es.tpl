
{if $sale}
    {if $oldStatus eq 'RESERVED'}
        <form action="{$document_root}modules/sales/modifications/pValidateReservedSale" id="frmValidateReservedSale" name="frmValidateReservedSale" method="post">    
            <div class="span-24 last line separator">
                <label>Datos de la Tarjeta</label>
            </div>

            <div class="span-24 last line">                  
                <div class="prepend-4 span-4 label">Nombre del Cliente:</div>
                <div class="span-5 ">{$customer.first_name_cus|htmlall}&nbsp;{$customer.last_name_cus|htmlall}</div>                       
                             
                <div class="span-3 label">Fecha de Reserva</div>
                <div class="span-4 append-2 last">{$inDate}</div>
            
            </div>
            
            <div class="span-24 last line">                   
                <div class="prepend-4 span-4 label">Fecha Inicio Viaje</div>
                <div class="span-5 ">{$beginDate}</div>
                                       
                <div class="span-3 label">Fecha Fin Viaje</div>
                <div class="span-4 append-4 last">{$endDate}</div>
            </div>
            
            <div class="span-24 last line">                   
                <div class="prepend-4 span-4 label">Destino</div>
                <div class="span-5 ">{$name_cou}</div>           
                             
                <div class="span-3 label">Precio</div>
                <div class="span-4 append-4 last">{$price}</div>
            </div>    
            
            <div class="span-24 last line">                   
                <div class="prepend-4 span-4 label">Total Pasajeros</div>
                <div class="span-5 append-4 last ">{$totalCus}</div>         
      
            </div>
            
            
            <div class="span-24 last buttons line">
                <input type="button" name="btnSubmit" id="btnSubmit" value="VALIDAR RESERVA" onclick="disableBtn();" />
            </div>
            <input type="hidden" name="serial_sal" id="serial_sal" value="{$serialSal}" />
            <input type="hidden" name="oldStatus" id="oldStatus" value="{$oldStatus}" />
            <input type="hidden" name="cardNumber" id="cardNumber" value="{$card_number}" />
            <input type="hidden" name="customerName" id="customerName" value="{$customer.first_name_cus}" />
            <input type="hidden" name="customerSurname" id="customerSurname" value="{$customer.last_name_cus}" />
        </form>
    {else}
        <div class="prepend-8 span-8 append-8 last">        
            <div class="span-8 error">
                El n�mero de tarjeta no es una reserva
            </div>	       
        {/if}
    {else}
        <div class="prepend-8 span-8 append-8 last">
            {if $error eq 1}
                <div class="span-8 error">
                    El n&uacute;mero de tarjeta ingresada no existe.
                </div>	
            </div>
        {/if}
    {/if}