<div class="span-24 last line separator">
    <label>Datos del Cliente</label>
</div>

{if $data.vip_cus eq 1}
    <div class="append-7 span-10 prepend-7 last line">
        <div class=" span-10 notice center">INFO: Es cliente VIP.</div>
    </div>
{/if}

{if $data.status_cus eq 'INACTIVE'}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 error center">
            La <b>venta</b> de una p&oacute;liza al cliente est&aacute; <b>restringida</b>.
            <input type="hidden" name="nmBlackListed" id="nmBlackListed" value="true" />
        </div>

    </div>
{/if}

<div class="span-24 last line">
    <div class="prepend-3 span-4 label">*Tipo de Documento</div>
    <div class="span-4 append-2 last">
        <select name="selCustomerDocumentType" id="selCustomerDocumentType" title='El campo "Tipo de Documento" es obligatorio.'>
            <option value="">- Seleccione -</option>
            <option value="c" {if $data.document_type eq 'c'}selected{/if}>Cédula</option>
            <option value="r" {if $data.document_type eq 'r'}selected{/if}>Ruc</option>
            <option value="p" {if $data.document_type eq 'p'}selected{/if}>Pasaporte</option>
        </select>
        <input type="hidden" name="hdnValidIdentification" id="hdnValidIdentification" value=""/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label">* Identificaci&oacute;n:</div>
    <div class="span-5">
        <input type="text" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' {if $data.document_cus}value="{$data.document_cus}"{/if}/>
        <input type="hidden" name="serialCus" id="serialCus"  {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
        <input type="hidden" name="hdnMainCustomerHasStatement" id="hdnMainCustomerHasStatement" value="{$data.customer_statement}" />
    </div>
    <div class="span-3 label">*Tipo de Cliente</div>
    <div id="customerType" class="span-4 append-2 last">
        {if $data.firstname_cus}
            <input type="hidden" name="selType" id="selType" value="{$data.type_cus}">
            {$global_typeManager[$data.type_cus]}
        {else}
            <select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.' {if $data.firstname_cus}disabled="disabled"{/if}>
                <option value="">- Seleccione -</option>
                {foreach from=$typesList item="l"}
                    <option value="{$l}" {if $l eq $data.type_cus}selected{/if}>
                        {$global_typeManager[$l]}
                    </option>
                {/foreach}
            </select>
        {/if}

    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label" id="customerNameLabel">
        *&nbsp;
       	{if  $data.type_cus eq LEGAL_ENTITY}
            Raz&oacute;n Social
       	{else}
            Nombre
       	{/if}
       	:
    </div>
    <div class="span-5">
        <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Nombre" es obligatorio' {if $data.firstname_cus}value="{$data.firstname_cus|htmlall}" {/if} />
    </div>

    <div id="customerLastNameLabelContainer" {if  $data.type_cus eq LEGAL_ENTITY}style="display: none;"{/if}>
        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' readonly="readonly" {if $data.lastname_cus}value="{$data.lastname_cus|htmlall}" {/if} />
        </div>
    </div>
</div>

<div class="prepend-2 span-19 append-3 last line" id="genderDiv" {if $data.type_cus neq PERSON}style="display: none"{/if}>
    <div class="prepend-1 span-4 label">* G&eacute;nero:</div>
    <div class="span-5">  
        <input type="radio" name="rdGender" id="rdGender" value="M" {if $data.gender_cus eq 'M'}checked{/if} /> Masculino <br />
        <input type="radio" name="rdGender" id="rdGender" value="F" {if $data.gender_cus eq 'F'}checked{/if} /> Femenino
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
    <div class="span-5">
        <select name="selCountry" id="selCountry" class="span-5" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}" {if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou|htmlall}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-3 label">* Ciudad</div>
    <div class="span-4 append-4 last" id="cityContainerCustomer">
        <select name="selCityCustomer" id="selCityCustomer" class="span-4" title='El campo "Ciudad" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$cityList item="l"}
                <option value="{$l.serial_cit}" {if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit|htmlall}</option>
            {/foreach}
        </select>
    </div>
</div>


<div class="span-20 prepend-4 last line" id="divAddress1">
    <div id="customerBirthdateContainer" {if  $data.type_cus eq LEGAL_ENTITY}style="display: none;"{/if}>
        <div class="span-3 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
            <input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" title='El campo "Fecha de Nacimiento" es obligatorio' {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}/>
        </div>
    </div>

    <div class="span-3 label">
       	* Direcci&oacute;n:
    </div>
    <div class="span-4 append-4 last">
        <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data.address_cus}value="{$data.address_cus|htmlall}"{/if} />
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label">Tel&eacute;fono Fijo:</div>
    <div class="span-5">
        <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" {if $data.phone1_cus}value="{$data.phone1_cus}"{/if}/>
    </div>

    <div class="span-3 label">Tel&eacute;fono Secundario:</div>
    <div class="span-4 append-4 last">
        <input type="text" name="txtPhone2Customer" id="txtPhone2Customer"  {if $data.phone2_cus}value="{$data.phone2_cus}"{/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label">* Celular:</div>
    <div class="span-5">
        <input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer" title='El campo "Celular" es obligatorio'  {if $data.cellphone_cus}value="{$data.cellphone_cus}"{/if}/>
    </div>

    <div class="span-3 label">* Email:</div>
    <div class="span-4 append-4 last">
        <input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio'  {if $data.email_cus}value="{$data.email_cus}"{/if}/>
    </div>
</div>

<div class="span-24  last line">
    <div class="prepend-4 span-3 label" id="customerContactLabel">
       	*&nbsp;
       	{if  $data.type_cus eq LEGAL_ENTITY}
            Gerente
       	{else}
            Contacto de Emergencia
       	{/if}
       	:
    </div>
    <div class="span-5">
        <input type="text" name="txtRelative" id="txtRelative" title='El campo "Contacto en caso de Emergencia" es obligatorio' {if $data.relative_cus}value="{$data.relative_cus|htmlall}"{/if}/>
    </div>

    <div class="span-3 label" id="customerContactPhoneLabel">
       	*&nbsp;Tel&eacute;fono del
       	{if  $data.type_cus eq LEGAL_ENTITY}
            gerente
       	{else}
            contacto
       	{/if}
       	:
    </div>
    <div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio' {if $data.relative_phone_cus}value="{$data.relative_phone_cus}"{/if}/> </div>
</div>

<!-- Declaracion de Salud Boton -->
<div class="span-24 last line" id="healthStatementContainer" style="display: none;">
    <input type="button" name="healthStatementBtn" id="healthStatementBtn" title="Declaración de Salud" value="Declaración de Salud" onclick="openHealthStatement(event, null, null, false);">
    <button type="button" name="linkHealthStatementBtn" id="linkHealthStatementBtn" title="Enlazar declaración de salud" onclick="checkStatementRefreshButton(event, null, false);">Enlazar Declaración</button>
</div>

<div class="span-24 last">
    <input type="hidden" name="hdnVip" id="hdnVip" value="{$data.vip_cus}" />
    <input type="hidden" name="hdnStatus" id="hdnStatus" value="{$data.status_cus}" />	
</div>

<div class="span-24 last line" id="repContainer" style="display: none;">
    <div class="span-24 last line separator">
        <label>Datos del Representante Legal</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Identificaci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="repDocument" id="repDocument"  title='El campo "Identificaci&oacute;n - Representante" es obligatorio' />
            <input type="hidden" name="repSerialCus" id="repSerialCus" />
            <input type="hidden" name="repType" id="repType" value="PERSON">
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Nombre:</div>
        <div class="span-5">
            <input type="text" name="repName" id="repName" title='El campo "Nombre - Representante" es obligatorio' />
        </div>


        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="repLastname" id="repLastname" title='El campo "Apellido - Representante" es obligatorio'/>
        </div>
    </div>

    <div class="span-20 prepend-4 last line" id="divAddress1">    
        <div class="span-3 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
            <input type="text" name="repBirthday" id="repBirthday" title='El campo "Fecha de Nacimiento - Representante" es obligatorio' />
        </div>

        <div class="span-3 label">Email:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="repEmail" id="repEmail" title='El campo "Email - Representante" es obligatorio'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="repCountryContainer">* Pa&iacute;s de Residencia:</div>
        <div class="span-5">
            <select name="repCountry" id="repCountry" class="span-5" title='El campo "Pa&iacute;s de Residencia - Representante" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}">{$l.name_cou|htmlall}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="repCityContainerCustomer">
            <select name="selCityRepresentative" id="selCityRepresentative" class="span-4" title='El campo "Ciudad - Representante" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$cityList item="l"}
                    <option value="{$l.serial_cit}">{$l.name_cit|htmlall}</option>
                {/foreach}
            </select>
        </div>
    </div>
</div>

<div class="prepend-4 span-16 append-4 last line" id="customerQuestionsContainer" style="display: none;"></div>
<div id="hiddenAnswers">
    {foreach from=$answerList item=al}
        <input type="hidden" name="cust_question_{$al.serial_que}" id="cust_question_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}" />
    {/foreach}
</div>
