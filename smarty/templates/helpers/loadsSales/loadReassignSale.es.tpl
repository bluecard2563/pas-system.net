
<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
                <select name="selCountryReassign" id="selCountryReassign" title='El campo "Pa&iacute;s" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou|htmlall}</option>
                    {/foreach}
                </select>
        </div>

        <div class="span-3 label">* Ciudad:</div>
        <div class="span-4 append-4 last" id="cityContainerReassign">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label" >* Comercializador:</div>
		<div class="span-5" id="dealerContainerReassign">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Sucursal de Comercializador:</div>
		<div class="span-5 " id="branchContainerReassign">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal de comercializador" es obligatorio'>
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Asesor de Venta:</div>
		<div class="span-5 last" id="counterContainerReassign">
				<select name="selCounter" id="selCounter" title='El campo "Seleccione un counter" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$serial_cnt key=key item="l"}
					<option value="{$l.serial_cnt}">{$l.name_dea}</option>
				{/foreach}
			</select>
		 </div>
</div>

<div class="span-24 last line separatorExtra" id="line" style="display: none"></div>