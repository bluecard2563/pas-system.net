{if $promo_info && $serial_cup}
	<div class="prepend-3 span-18 append-3 last line">
		<table border="0" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						Promoci&oacute;n
					</td>
					<td align="center" class="tableTitle">
						Aplica a
					</td>
					<td align="center" class="tableTitle">
						Descuento en esta compra <br />
						({$promo_info.discount_percentage_ccp} %)
					</td>
					<td align="center"  class="tableTitle">
						Descuento en pr&oacute;xima compra <br />
						({$promo_info.other_discount_ccp} %)
					</td>	
				</tr>
			</THEAD>
			<TBODY>
				<tr bgcolor="#d7e8f9" >
					<td align="center" class="tableField">
						{$promo_info.name_ccp|htmlall}
					</td>
					<td align="center" class="tableField">
						{$global_customer_promo_types[$promo_info.type_ccp]}
					</td>
					<td align="center" class="tableField">
						{$symbol} {$promo_info.current_sale}
						<input type="hidden" name="hdnPromoDiscount" id="hdnPromoDiscount" value="{$promo_info.current_sale}" />
					</td>
					<td align="center" class="tableField">
						{$symbol} {$promo_info.next_sale}
					</td>
				</tr>
			</TBODY>
		</table>
	</div>
{else}
	<div class="prepend-7 span-10 append-7 last line">
		<div class="span-10 error center">
			El cup&oacute;n ingresado no es v&aacute;lido.
		</div>
	</div>
{/if}