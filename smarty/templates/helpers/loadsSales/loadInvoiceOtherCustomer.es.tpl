
<div class="prepend-4 span-19 append-2 last line">
    <div class="span-3 label">* Tipo de Documento:</div>
    <div class="span-5 last">
        {if $validate_holder_document eq 'NO'}
            {if $customerData}
                {if $customerData.type_cus eq 'PERSON'}
                    Persona Natural
                {elseif $customerData.type_cus eq 'LEGAL_ENTITY'}
                    Persona Jur&iacute;dica
                {/if}
            {else}
                <select name="selTypeOther" id="selTypeOther" title='El campo "Tipo de Cliente" es obligatorio.' >
                    <option value="">- Seleccione -</option>
                    {foreach from=$typesList item="l"}
                        <option value="{$l}" {if $l eq $customerData.type_cus}selected{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                    {/foreach}
                </select>
            {/if}
        {else}
            <input type="radio" name="OtherrdDocumentType" id="OtherrdDocumentType_CI" value="CI" person_type="PERSON" {if $customerData.document_type_cus eq 'c'}checked{/if}/> CI
            <input type="radio" name="OtherrdDocumentType" id="OtherrdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.document_type_cus eq 'r'}checked{/if}/> RUC
            <input type="radio" name="OtherrdDocumentType" id="OtherrdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" {if $customerData.document_type_cus eq 'p'}checked{/if}/> Pasaporte
            <input type="hidden" name="selTypeOther" id="selTypeOther" value="{$customerData.type_cus}" />
            <input type="hidden" name="selOtherDocumentType" id="selOtherDocumentType" {if $customerData.document_type_cus}value="{$customerData.document_type_cus}"{/if} title='El campo "Tipo de Documento" de facturación es obligatorio'/>
        {/if}
    </div>
    <div class="span-3 label">* Documento:</div>
    <div class="span-5">
        <input type="text" name="txtOtherDocument" id="txtOtherDocument" title='El campo "Documento" de facturación es obligatorio.'value="{$customerData.document_cus}" />
    </div>
</div>
<div class="prepend-2 span-19 append-3 last line" id="genderOtherDiv" {if $data.type_cus eq LEGAL_ENTITY}style="display: none"{/if} title='El campo "Genero" es obligatorio.'>
    <div class="prepend-1 span-4 label">* G&eacute;nero:</div>
    <div class="span-5">  
        <input type="radio" name="rdOtherGender" id="rdOtherGender" value="M" title='El campo "Genero" de facturación es obligatorio.' {if $customerData.gender_cus eq 'M'}checked{/if} /> Masculino <br />
        <input type="radio" name="rdOtherGender" id="rdOtherGender" value="F" title='El campo "Genero" de facturación es obligatorio.' {if $customerData.gender_cus eq 'F'}checked{/if} /> Femenino
    </div>
    <div class="span-3 label">* Fecha de Nacimiento:</div>
                <input type="text" name="txtBirthdayOtherCustomer" id="txtBirthdayOtherCustomer" title='El campo "Fecha de Nacimiento" de facturación es obligatorio' {if $customerData.birthdate_cus}value="{$customerData.birthdate_cus}"{/if}/>
</div>

<div class="prepend-2 span-19 append-3 last line">
    <div class="prepend-1 span-4 label" id="labelName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}*Razo&oacuten Social: {else}*Nombre:{/if}</div>

    <div class="span-5">  <input type="text" name="txtOtherFirstName" id="txtOtherFirstNameInvoice" title='El campo "Nombre" de facturación es obligatorio.' value="{$customerData.first_name_cus|htmlall}" {if $customerData}{/if}> </div>

    <div id="lastNameOtherContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
        <div class="span-3 label">*Apellido:</div>
        <div class="span-4 append-2 last">  <input type="text" name="txtOtherLastName" id="txtOtherLastNameInvoice" title='El campo "Apellido" de facturación es obligatorio' value="{$customerData.last_name_cus|htmlall}" {if $customerData}{/if}> </div>
    </div>                
</div>

<div class="prepend-2 span-19 append-3 last line">
    <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
    <div class="span-5">
        <input type="text" name="txtOtherAddress" id="txtOtherAddress" title='El campo "Direcci&oacute;n" de facturación es obligatorio' value="{$customerData.address_cus|htmlall}" >
    </div>

    <div class="span-3 label">*Tel&eacute;fono Principal:</div>
    <div class="span-4 append-2 last">
        <input type="text" name="txtOtherPhone1" id="txtOtherPhone1" title='El campo "Tel&eacute;fono Principal" de facturación es obligatorio' value="{$customerData.phone1_cus}" >
    </div>
</div>

<div class="prepend-2 span-19 append-3 last line">
    <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
    <div class="span-5"> <input type="text" name="txtOtherPhone2" id="txtOtherPhone2"  value="{$customerData.phone2_cus}"/></div>

    <div class="span-3 label">*Correo Electr&oacute;nico:</div>
    <div class="span-4 append-2 last">
        <input type="text" name="txtOtherEmail" id="txtOtherEmail" title='El campo "Correo Electr&oacute;nico" de facturación es obligatorio' value="{$customerData.email_cus}" >
    </div>
</div>

<div class="prepend-2 span-19 append-3 last line">
    <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
    <div class="span-5"> <select name="selOtherCountry" id="selOtherCountry" title='El campo "Pa&iacute;s" de facturación es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.serial_cou}selected{/if}>{$l.name_cou}</option>
            {/foreach}
        </select> </div>

    <div class="span-3 label">*Ciudad:</div>
    <div class="span-4 append-2 last" id="cityOtherContainer"> <select name="selOtherCity" id="selOtherCity" title='El campo "Ciudad" de facturación es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$cityList item="l"}
                <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit}</option>
            {/foreach}
        </select> 
    </div>
</div>
