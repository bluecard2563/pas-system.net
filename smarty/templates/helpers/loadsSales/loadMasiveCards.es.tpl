{if $masiveCards}
<div class="span-24 last line separator">
    <label>Tarjetas de Venta Masiva:</label>
</div>

<center>
	<div class="prepend-1 span-22 last line">
		<table border="1" class="dataTable" id="cards_table">
			<thead>
				<tr class="header">

					<th style="text-align: center;">
							Tarjeta
					</th>

					<th style="text-align: center;">
							Cliente
					</th>

					<th style="text-align: center;">
							Email
					</th>

					<th style="text-align: center;">
							Producto
					</th>

					<th style="text-align: center;">
							Imprimir
					</th>

					<th style="text-align: center;">
							Enviar a Cliente
					</th>
				</tr>
			</thead>

			<tbody>
                {foreach name="masiveCards" from=$masiveCards item="mc"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.claims.iteration is even} class="odd"{else}class="even"{/if}>

                    <td class="tableField">
                         {$mc.card_number_trl}
                    </td>
                    <td class="tableField">
						 {$mc.name_cus|htmlall}
                    </td>
                    <td class="tableField">
						 {$mc.email_cus}
                    </td>
                    <td class="tableField">
						 {$product|htmlall}
                    </td>
					
                    <td class="tableField">
                        <a style="cursor: pointer;" href="{$document_root}modules/sales/travelRegister/pPrintRegister/{$mc.serial_trl}/1" target="_blank" id="printDocument{$mc.serial_trl}" name="printDocument{$mc.serial_trl}">Imprimir</a>
                    </td>
                    <td class="tableField">
						{if $mc.email_cus neq '-'}
							<a style="cursor: pointer;" id="sendEmail{$mc.card_number_trl}" name="sendEmail{$mc.card_number_trl}" serial="{$mc.serial_trl}" pdf="modules/sales/contracts/contract_{$mc.serial_sal}.pdf" email="{$mc.email_cus}" >Enviar</a>
						{else}
							El cliente no tiene email
						{/if}
                    </td>
                </tr>
                {/foreach}
			</tbody>
		</table>
	</div>
	<input type="hidden" id="hdtItemCount" value="{$count}"/>
	<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>

</center>
{else}
<center><b>No hay datos para la venta seleccionada.</b></center><br>
{/if}
