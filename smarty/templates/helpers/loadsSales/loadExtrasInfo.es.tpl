{if $extraPosition eq 0}
<div id="extra_{$extraPosition}" class="span-24 last line">
    <div class="span-24 last line">
            <div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="extra[{$extraPosition}][idExtra]" id="idExtra_{$extraPosition}" title='El campo "Identificaci&oacute;n" es obligatorio, extra {$extraPosition+1}'
                	{if $data.document_cus}value="{$data.document_cus}"{/if}
                	{if $editionAllowed == 'NO'}disabled{/if}/>
				<input type="hidden" name="extra[{$extraPosition}][serialCus]" id="serialCus_{$extraPosition}"  {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
                <input type="hidden" name="hdnExtraCustomerHasStatement_{$extraPosition}" id="hdnExtraCustomerHasStatement_{$extraPosition}" value="{$data.customer_statement}" />
			</div>
            <div class="span-3 label">* Número de acompañante:</div>
            <div class="span-4 append-4 last">
                <select name="extra[{$extraPosition}][selRelationship]" id="selRelationship_{$extraPosition}" title='El campo "Tipo de Relaci&oacute;n" es obligatorio, extra {$extraPosition+1}.' elementID="{$extraPosition}" {if $editionAllowed == 'NO'}disabled{/if}>
                    <option value="SPOUSE">Acompañante 1</option>
                {*{foreach from=$relationshipTypeList item="l"}*}
                    {*<option value="1" {if $l eq $data.relationship_cus}selected{/if}>{$global_extraRelationshipType.$l}</option>*}
                {*{/foreach}*}
                </select>
            </div>

    </div>

     <div class="span-24 last line" >
            <div class="prepend-4 span-4 label">* Nombre:</div>
            <div class="span-5">
                <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][nameExtra]" id="nameExtra_{$extraPosition}" title='El campo "Nombre" es obligatorio, extra {$extraPosition+1}'
                	{if $data.firstname_cus}value="{$data.firstname_cus|htmlall}" {/if}
                	{if $editionAllowed == 'NO'}disabled{/if}/>
            </div>

            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-4 last">
                <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][lastnameExtra]" id="lastnameExtra_{$extraPosition}" title='El campo "Apellido" es obligatorio, extra {$extraPosition+1}'
					{if $data.lastname_cus}value="{$data.lastname_cus|htmlall}" {/if}
					{if $editionAllowed == 'NO'}disabled{/if}/>
            </div>
        </div>

     <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                <input type="text" name="extra[{$extraPosition}][birthdayExtra]" id="birthdayExtra_{$extraPosition}" title='El campo "Fecha de Nacimiento" es obligatorio, extra {$extraPosition+1}'
					{if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}
					{if $editionAllowed == 'NO'}disabled{/if}/>
            </div>

             <div class="span-3 label">* Email:</div>
            <div class="span-4 append-4 last">
				<input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][emailExtra]" id="emailExtra_{$extraPosition}" elementID="{$extraPosition}" title='El campo "Email" es obligatorio, extra {$extraPosition+1}'
					{if $data.email_cus}value="{$data.email_cus}"{/if}
					{if $editionAllowed == 'NO'}disabled{/if}/>
			</div>
     </div>

	<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Tarifa:</div>
            <div class="span-5" id="extraPriceContainer_{$extraPosition}"></div>
			<input type="hidden" name="extra[{$extraPosition}][hdnPriceExtra]" id="hdnPriceExtra_{$extraPosition}" value="0"/>
			<input type="hidden" name="extra[{$extraPosition}][hdnCostExtra]" id="hdnCostExtra_{$extraPosition}" value="0"/>
	</div>

    <!-- Declaracion de Salud Boton -->
    <div id="extraContainerBtn_{$extraPosition}" class="span-24 last line"  {if $data.customerExist eq false}style="display: none;"{/if}>
        <input type="button" id="healthStatementExtraBtn_{$extraPosition}" value="Declaración de Salud" title="Declaración de Salud" onclick="openHealthStatement(event, {$extraPosition}, true);">
        <button type="button" name="healthStatementExistBtn" id="healthStatementExistBtn" title="Enlazar Declaración de salud" onclick="checkStatementRefreshButton(event, {$extraPosition}, true);">Enlazar Declaración</button>
    </div>

	<div class="prepend-4 span-16 append-4 last line" id="questionsContainer_{$extraPosition}" style="display: none;"></div>
        <div id="hiddenAnswers">
    	{foreach from=$answerList item=al}
        	<input type="hidden" name="question{$extraPosition}_{$al.serial_que}" id="question_{$extraPosition}_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}"  groupid="{$extraPosition}"/>
            <input type="hidden" name="answer_{$extraPosition}_{$al.serial_que}" id="answer_{$extraPosition}_{$al.serial_que}" value="{$al.serial_ans}"  groupid="{$extraPosition}"/>
        {/foreach}
    </div>
	<input type="hidden" name="hdnSerial_cusExtra_{$extraPosition}" id="hdnSerial_cusExtra_{$extraPosition}" {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
    <div class="span-24 last line separatorExtra"></div>
</div>



{elseif $extraPosition eq 1}



<div id="extra_{$extraPosition}" class="span-24 last line">
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="extra[{$extraPosition}][idExtra]" id="idExtra_{$extraPosition}" title='El campo "Identificaci&oacute;n" es obligatorio, extra {$extraPosition+1}'
                   {if $data.document_cus}value="{$data.document_cus}"{/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
            <input type="hidden" name="extra[{$extraPosition}][serialCus]" id="serialCus_{$extraPosition}"  {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
            <input type="hidden" name="hdnExtraCustomerHasStatement_{$extraPosition}" id="hdnExtraCustomerHasStatement_{$extraPosition}" value="{$data.customer_statement}" />
        </div>
        <div class="span-3 label">* Número de acompañante:</div>
        <div class="span-4 append-4 last">
            <select name="extra[{$extraPosition}][selRelationship]" id="selRelationship_{$extraPosition}" title='El campo "Tipo de Relaci&oacute;n" es obligatorio, extra {$extraPosition+1}.' elementID="{$extraPosition}" {if $editionAllowed == 'NO'}disabled{/if}>
                <option value="RELATIVE">Acompañante 2</option>
                {*{foreach from=$relationshipTypeList item="l"}*}
                {*<option value="1" {if $l eq $data.relationship_cus}selected{/if}>{$global_extraRelationshipType.$l}</option>*}
                {*{/foreach}*}
            </select>
        </div>

    </div>

    <div class="span-24 last line" >
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][nameExtra]" id="nameExtra_{$extraPosition}" title='El campo "Nombre" es obligatorio, extra {$extraPosition+1}'
                   {if $data.firstname_cus}value="{$data.firstname_cus|htmlall}" {/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last">
            <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][lastnameExtra]" id="lastnameExtra_{$extraPosition}" title='El campo "Apellido" es obligatorio, extra {$extraPosition+1}'
                   {if $data.lastname_cus}value="{$data.lastname_cus|htmlall}" {/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
            <input type="text" name="extra[{$extraPosition}][birthdayExtra]" id="birthdayExtra_{$extraPosition}" title='El campo "Fecha de Nacimiento" es obligatorio, extra {$extraPosition+1}'
                   {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">* Email:</div>
        <div class="span-4 append-4 last">
            <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][emailExtra]" id="emailExtra_{$extraPosition}" elementID="{$extraPosition}" title='El campo "Email" es obligatorio, extra {$extraPosition+1}'
                   {if $data.email_cus}value="{$data.email_cus}"{/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tarifa:</div>
        <div class="span-5" id="extraPriceContainer_{$extraPosition}"></div>
        <input type="hidden" name="extra[{$extraPosition}][hdnPriceExtra]" id="hdnPriceExtra_{$extraPosition}" value="0"/>
        <input type="hidden" name="extra[{$extraPosition}][hdnCostExtra]" id="hdnCostExtra_{$extraPosition}" value="0"/>
    </div>

    <!-- Declaracion de Salud Boton -->
    <div id="extraContainerBtn_{$extraPosition}" class="span-24 last line" {if $data.customerExist eq false}style="display: none;"{/if}>
        <input type="button" id="healthStatementExtraBtn_{$extraPosition}" value="Declaración de Salud" title="Declaración de Salud" onclick="openHealthStatement(event, {$extraPosition}, true);">
        <button type="button" title="Enlazar declaración de salud" onclick="checkStatementRefreshButton(event, {$extraPosition}, true);">Enlazar Declaración</button>
    </div>

    <div class="prepend-4 span-16 append-4 last line" id="questionsContainer_{$extraPosition}" style="display: none;"></div>
    <div id="hiddenAnswers">
        {foreach from=$answerList item=al}
            <input type="hidden" name="question{$extraPosition}_{$al.serial_que}" id="question_{$extraPosition}_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}"  groupid="{$extraPosition}"/>
            <input type="hidden" name="answer_{$extraPosition}_{$al.serial_que}" id="answer_{$extraPosition}_{$al.serial_que}" value="{$al.serial_ans}"  groupid="{$extraPosition}"/>
        {/foreach}
    </div>
    <input type="hidden" name="hdnSerial_cusExtra_{$extraPosition}" id="hdnSerial_cusExtra_{$extraPosition}" {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
    <div class="span-24 last line separatorExtra"></div>
</div>


{else}

<div id="extra_{$extraPosition}" class="span-24 last line">
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="extra[{$extraPosition}][idExtra]" id="idExtra_{$extraPosition}" title='El campo "Identificaci&oacute;n" es obligatorio, extra {$extraPosition+1}'
                   {if $data.document_cus}value="{$data.document_cus}"{/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
            <input type="hidden" name="extra[{$extraPosition}][serialCus]" id="serialCus_{$extraPosition}"  {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
            <input type="hidden" name="hdnExtraCustomerHasStatement_{$extraPosition}" id="hdnExtraCustomerHasStatement_{$extraPosition}" value="{$data.customer_statement}" />
        </div>
        <div class="span-3 label">* Número de acompañante:</div>
        <div class="span-4 append-4 last">
            <select name="extra[{$extraPosition}][selRelationship]" id="selRelationship_{$extraPosition}" title='El campo "Tipo de Relaci&oacute;n" es obligatorio, extra {$extraPosition+1}.' elementID="{$extraPosition}" {if $editionAllowed == 'NO'}disabled{/if}>
                <option value="OTHER">Otros</option>
                {*{foreach from=$relationshipTypeList item="l"}*}
                {*<option value="1" {if $l eq $data.relationship_cus}selected{/if}>{$global_extraRelationshipType.$l}</option>*}
                {*{/foreach}*}
            </select>
        </div>

    </div>

    <div class="span-24 last line" >
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][nameExtra]" id="nameExtra_{$extraPosition}" title='El campo "Nombre" es obligatorio, extra {$extraPosition+1}'
                   {if $data.firstname_cus}value="{$data.firstname_cus|htmlall}" {/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last">
            <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][lastnameExtra]" id="lastnameExtra_{$extraPosition}" title='El campo "Apellido" es obligatorio, extra {$extraPosition+1}'
                   {if $data.lastname_cus}value="{$data.lastname_cus|htmlall}" {/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5">
            <input type="text" name="extra[{$extraPosition}][birthdayExtra]" id="birthdayExtra_{$extraPosition}" title='El campo "Fecha de Nacimiento" es obligatorio, extra {$extraPosition+1}'
                   {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">* Email:</div>
        <div class="span-4 append-4 last">
            <input type="text" class="extra_{$extraPosition}" name="extra[{$extraPosition}][emailExtra]" id="emailExtra_{$extraPosition}" elementID="{$extraPosition}" title='El campo "Email" es obligatorio, extra {$extraPosition+1}'
                   {if $data.email_cus}value="{$data.email_cus}"{/if}
                    {if $editionAllowed == 'NO'}disabled{/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tarifa:</div>
        <div class="span-5" id="extraPriceContainer_{$extraPosition}"></div>
        <input type="hidden" name="extra[{$extraPosition}][hdnPriceExtra]" id="hdnPriceExtra_{$extraPosition}" value="0"/>
        <input type="hidden" name="extra[{$extraPosition}][hdnCostExtra]" id="hdnCostExtra_{$extraPosition}" value="0"/>
    </div>

    <!-- Declaracion de Salud Boton -->
    <div id="extraContainerBtn_{$extraPosition}" class="span-24 last line" {if $data.customerExist eq false}style="display: none;"{/if}>
        <input type="button" id="healthStatementExtraBtn_{$extraPosition}" value="Declaración de Salud" title="Declaración de Salud" onclick="openHealthStatement(event, {$extraPosition}, true);">
        <button type="button" title="Enlazar declaración de salud" onclick="checkStatementRefreshButton(event, {$extraPosition}, true);">Enlazar Declaración</button>
    </div>

    <div class="prepend-4 span-16 append-4 last line" id="questionsContainer_{$extraPosition}" style="display: none;"></div>
    <div id="hiddenAnswers">
        {foreach from=$answerList item=al}
            <input type="hidden" name="question{$extraPosition}_{$al.serial_que}" id="question_{$extraPosition}_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}"  groupid="{$extraPosition}"/>
            <input type="hidden" name="answer_{$extraPosition}_{$al.serial_que}" id="answer_{$extraPosition}_{$al.serial_que}" value="{$al.serial_ans}"  groupid="{$extraPosition}"/>
        {/foreach}
    </div>
    <input type="hidden" name="hdnSerial_cusExtra_{$extraPosition}" id="hdnSerial_cusExtra_{$extraPosition}" {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
    <div class="span-24 last line separatorExtra"></div>
</div>
{/if}
