{if $pos eq 0}
<div class="span-24 last line separator" id="extasTitle">
    <label>Extras</label>
</div>
{/if}
<div id="extra_{$pos}" class="span-24 last line">
    <div class="span-24 last line">
            <div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="extra[{$pos}][idExtra]" id="idExtra_{$pos}" elementID="{$pos}" title='El campo "Identificaci&oacute;n" es obligatorio'/>
				<input type="hidden" name="extra[{$pos}][serialCus]" id="serialCus_{$pos}" value=""/>
			</div>
            <div class="span-3 label">* Tipo de Relaci&oacute;n con el titular:</div>
            <div class="span-4 append-4 last">
                <select name="extra[{$pos}][selRelationship]" id="selRelationship_{$pos}" title='El campo "Tipo de Relaci&oacute;n" es obligatorio.' elementID="{$pos}">
                    <option value="">- Seleccione -</option>
                {foreach from=$relationshipTypeList item="l"}
                    <option value="{$l}">{$global_extraRelationshipType.$l}</option>
                {/foreach}
                </select>
            </div>   

    </div>

     <div class="span-24 last line" >
            <div class="prepend-4 span-4 label">* Nombre:</div> 
            <div class="span-5">
                <input type="text" name="extra[{$pos}][nameExtra]" id="nameExtra_{$pos}"  title='El campo "Nombre" es obligatorio' />
            </div>

            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="extra[{$pos}][lastnameExtra]" id="lastnameExtra_{$pos}" title='El campo "Apellido" es obligatorio' />
            </div>
        </div>

     <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                <input type="text" name="extra[{$pos}][birthdayExtra]" id="birthdayExtra_{$pos}" elementID="{$pos}" title='El campo "Fecha de Nacimiento" es obligatorio'/>
            </div>

            <div class="span-3 label">Email:</div>
            <div class="span-4 append-4 last">
				<input type="text" name="extra[{$pos}][emailExtra]" id="emailExtra_{$pos}" elementID="{$pos}" title='El campo "Email" es obligatorio'/>
			</div>
        </div>

	<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Tarifa:</div>
            <div class="span-5" id="extraPriceContainer_{$pos}"></div>
			<input type="hidden" name="extra[{$pos}][hdnPriceExtra]" id="hdnPriceExtra_{$pos}" value="0"/>
			<input type="hidden" name="extra[{$pos}][hdnCostExtra]" id="hdnCostExtra_{$pos}" value="0"/>
	</div>

	<input type="hidden" name="percentage_spouse_{$pos}" id="percentage_spouse_{$pos}" {if $percentages.percentage_spouse_pxc}value="{$percentages.percentage_spouse_pxc}"{/if} />
	<input type="hidden" name="percentage_extras_{$pos}" id="percentage_extras_{$pos}" {if $percentages.percentage_extras_pxc}value="{$percentages.percentage_extras_pxc}"{/if}  />
	<input type="hidden" name="percentage_children_{$pos}" id="percentage_children_{$pos}" {if $percentages.percentage_children_pxc}value="{$percentages.percentage_children_pxc}"{/if} />

	<div class="prepend-4 span-16 append-4 last line " id="questionsContainer_{$pos}"></div>
    <div class="span-24 last line separatorExtra"></div>
</div>