<div class="prepend-2 span-19 append-3 last line">
                    <div class="prepend-1 span-4 label">Id:</div>
                    <div class="span-5">
                        {$branchData.id_dea}
                        <input type="hidden" name="hdnDealerDocument" id="hdnDealerDocument" value="{$branchData.id_dea}" />
                    </div>
                </div>

                <div class="prepend-2 span-19 append-3 last line">
                    <div class="prepend-1 span-4 label">Nombre:</div>
                    <div class="span-5">
                        {$branchData.name_dea}
                    </div>

                    <div class="span-3 label">Direcci&oacute;n</div>
                    <div class="span-6 last">
                        {$branchData.address_dea}
                    </div>
                </div>

                <div class="prepend-2 span-19 append-3 last line">
                    <div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
                    <div class="span-5">
                        {$branchData.phone1_dea}
                    </div>

                    <div class="span-3 label">Tel&eacute;fono 2:</div>
                    <div class="span-4 append-2 last">
                        {$branchData.phone2_dea}
                    </div>
                </div>

                <div class="prepend-2 span-19 append-3 last line">
                    <div class="prepend-1 span-4 label">Correo Electr&oacute;nico:</div>
                    <div class="span-5">
                        {$branchData.email2_dea}
                    </div>
                </div>