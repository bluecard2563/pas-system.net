{if $productListByDealer}
   <select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio.'>
        <option value="" serial_pro="">- Seleccione -</option>
        {foreach from=$productListByDealer item="l"}
            <option value="{$l.serial_pbd}" 
            		id="product_{$l.serial_pbd}" 
            		serial_pro="{$l.serial_pro}" 
            		max_extras_pro="{$l.max_extras_pro}" 
            		range_price="{$l.price_by_range_pro}" 
            		calculate_price="{$l.calculate_pro}" 
            		services="{$l.has_services_pro}" 
            		serial_pxc="{$l.serial_pxc}" 
					price_by_day_pro="{$l.price_by_day_pro}"
					children_pro="{$l.children_pro}" 
					adults_pro="{$l.adults_pro}" 
					flights_pro="{$l.flights_pro}" 
					generate_number_pro="{$l.generate_number_pro}" 
					senior_pro="{$l.senior_pro}" 
					show_price_pro="{$l.show_price_pro}"
					extras_restricted_to_pro="{$l.extras_restricted_to_pro}"
					percentage_extras="{$l.percentage_extras}" 
					percentage_children="{$l.percentage_children}" 
					percentage_spouse="{$l.percentage_spouse}"
					destination_restricted_pro="{$l.destination_restricted_pro}"
					restricted_children_pro="{$l.restricted_children_pro}"
					individual_pro="{$l.individual_pro}"
					>
						{$l.name_pbl|htmlall}
			</option>
        {/foreach}
    </select>
{else}
	<select name="selProduct" id="selProduct" title='El Counter seleccionado no tiene productos asignados.' style="display: none">
		<option value="" serial_pro="">- Seleccione -</option>
	</select>
	El Counter seleccionado no tiene productos asignados.
{/if}
