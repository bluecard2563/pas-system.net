{if $prices}
    {*Verifies if the product has a specific price or a range one.*}
    {if $range_price eq 'YES'}
        Por Rangos: {$closestDurationRangeMax} - {$closestDurationRangeMin}
    {else}
        {$symbol} {$fixedPrice}
    {/if}
{else}
    {if $message}
        {$message}
    {else}
        {$symbol} {$fixedPrice}
    {/if}
{/if}

<input type="hidden" name="hdnPrice" id="hdnPrice" value="{$closestDurationPrice}" />
<input type="hidden" name="hdnCost" id="hdnCost" value="{$closestDurationCost}" />
<input type="hidden" name="hdnPriceAdultSenior" id="hdnPriceAdultSenior" value="{$fixedPrice}"/>