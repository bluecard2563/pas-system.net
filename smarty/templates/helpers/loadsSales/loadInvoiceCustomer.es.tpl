
        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">* Documento:</div>
            <div class="span-5">
                            <input type="text" name="txtCustomerDocument" id="txtCustomerDocumentInvoice" title='El campo "Documento" es obligatorio.'value="{$customerData.document_cus}" readonly="readonly"/>
            </div>

            <div class="span-3 label">* Tipo de Documento:</div>
            <div class="span-6 last">
                {if $validate_holder_document eq 'NO'}
                    {if $customerData}
                        {if $customerData.type_cus eq 'PERSON'}
                            Persona Natural
                        {elseif $customerData.type_cus eq 'LEGAL_ENTITY'}
                            Persona Jur&iacute;dica
                        {/if}
                    {else}
                        <select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.' >
                            <option value="">- Seleccione -</option>
                            {foreach from=$typesList item="l"}
                                <option value="{$l}" {if $l eq $customerData.type_cus}selected{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                            {/foreach}
                        </select>
                    {/if}
                {else}
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.type_cus eq 'LEGAL_ENTITY'}checked{/if} /> RUC					
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
                    <input type="hidden" name="selType" id="selType" value="{$customerData.type_cus}" />
                {/if}				
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label" id="labelName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}*Razo&oacute;n Social: {else}*Nombre:{/if}</div>
            
            <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstNameInvoice" title='El campo "Nombre" es obligatorio.' value="{$customerData.first_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

            <div id="lastNameContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
                <div class="span-3 label">*Apellido:</div>
                <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastNameInvoice" title='El campo "Apellido" es obligatorio' value="{$customerData.last_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
            </div>                
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="txtAddress" id="txtAddressInvoice" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus|htmlall}" readonly="readonly">
            </div>

            <div class="span-3 label">*Tel&eacute;fono Principal:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtPhone1" id="txtPhone1Invoice" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}" readonly="readonly">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
            <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2Invoice"  value="{$customerData.phone2_cus}"/></div>

            <div class="span-3 label">Correo Electr&oacute;nico:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtEmail" id="txtEmailInvoice" title='El campo "Correo Electr&oacute;nico" es obligatorio' value="{$customerData.email_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
            <div class="span-5"> <select name="selCountry" id="selCountryInvoice" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.serial_cou}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select> </div>

            <div class="span-3 label">*Ciudad:</div>
            <div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCityInvoice" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                </select> 
            </div>
        </div>