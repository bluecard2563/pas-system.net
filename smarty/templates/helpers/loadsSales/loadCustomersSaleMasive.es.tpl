{if $toShow}
<center>
	{assign var="container" value="none"}
	{*generate_admin_table data=$toShow titles=$titles*}

	<div class="span-20 last line">
		<table border="0" class="paginate-1 max-pages-7 admin_table" id="theTable">
			<THEAD>
				<tr bgcolor="#284787">
					{foreach name="titles" from=$titles item="t"}
						<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
							{$t}
						</td>
					{/foreach}
				</tr>
			</THEAD><TBODY>
				{foreach name="customerList" from=$toShow item="cl"}
					<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.customerList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$smarty.foreach.customerList.iteration}
							<input type="hidden" name="serial_cus[]" value="{$cl.serial_cus}" />
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$cl.document_usu}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$cl.first_name_cus}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$cl.last_name_cus}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$cl.phone1_cus}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$cl.email_cus}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" id="price_{$cl.serial_cus}">
							{$symbol} {$cl.price}
						</td>
					</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
</center>
<input type="hidden" value="{$total_customers}" id="total_customers"/>
<input type="hidden" value="{$total_relatives}" id="total_relatives"/>
<input type="hidden" value="{$total_spouse}" id="total_spouse"/>
<input type="hidden" value="{$total_children}" id="total_children"/>
<input type="hidden" value="{$total_children_free}" id="total_children_fre"/>
<input type="hidden" value="{$total_relatives_free}" id="total_realtives_fre"/>
<input type="hidden" value="{$sub_total}" name="sub_total" id="sub_total"/>


<div class="span-22 last line">
	<div class="prepend-2 span-4 label">Total Titulares:</div>
	<div class="span-5 last">{$total_customers}</div>
</div>
<div class="span-22 last line">
	<div class="prepend-2 span-4 label">Total Conyuges:</div>
	<div class="span-5 last">{$total_spouse}</div>
</div>
<div class="span-22 last line">
	<div class="prepend-2 span-4 label">Total Menores:</div>
	<div class="span-5 last">{$total_children + $total_children_free}</div>
	<div class="span-3 label">Menores Gratis:</div>
	<div class="span-4 append-4 last">{$total_children_free}</div>
</div>

<div class="span-22 last line">
	<div class="prepend-2 span-4 label">Total Adultos:</div>
	<div class="span-5 last">{$total_relatives + $total_relatives_free}</div>
	<div class="span-3 label">Adultos Gratis:</div>
	<div class="span-4 append-4 last">{$total_relatives_free}</div>
</div>
<div class="span-22 last line">
	<div class="prepend-2 span-4 label">Subtotal:</div>
	<div class="span-5 last">{$symbol} {$sub_total_cur}</div>
</div>

<div class="span-20 append-2 last line buttons">
	<input type="submit" id="uploadTrlLog" name="uploadTrlLog" value="Crear"/>
</div>
{else}
	<center><div class="span-20 last line"><b>El archivo seleccionado tiene inconsistencia de datos.</b></div></center>
	{if $err}
	<center><b>
		{if $err eq '5'}
			La tarjeta no acepta extras.
		{elseif $err eq '6'}
			La tarjeta s&oacute;lo acepta ni&ntilde;os.
		{elseif $err eq '7'}
			La tarjeta s&oacute;lo acepta adultos.
		{/if}
	</b></center>
	{/if}
{/if}