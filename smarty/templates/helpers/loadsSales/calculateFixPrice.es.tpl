{if $prices}
    <div class="prepend-4 span-4 label">* No. D&iacute;as:</div>
    <div class="span-5 last" style="text-align: left;">
            <select name="selDayNumber" id="selDayNumber" title='El campo "No. D&iacute;as" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$prices item="l"}
                <option value="{$l.serial_ppc}" price="{$l.price_ppc}" cost="{$l.cost_ppc}" id="pPrice_{$l.serial_ppc}" duration_ppc="{$l.duration_ppc}">{$l.duration_ppc}</option>
            {/foreach}
        </select>
    </div>
{else}
    No existen tarifas de venta para el producto seleccionado.
{/if}

<input type="hidden" name="hdnPrice" id="hdnPrice" value="" />
<input type="hidden" name="hdnCost" id="hdnCost" value="" />