{if $customers_list}
<div class="prepend-2 span-20 append-2 last line">
	<table border="0" id="customers_table" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					Nombre
				</td>
				<td align="center" class="tableTitle">
					Apellido
				</td>
				<td align="center"  class="tableTitle">
					Fecha de Nacimiento
				</td>
				<td align="center"  class="tableTitle">
					D&iacute;as del Viaje
				</td>
				<td align="center" class="tableTitle">
					Precio Titular
				</td>
				<td align="center" class="tableTitle">
					Precio Total
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="customers" from=$customers_list item="c"}
			<tr {if $smarty.foreach.customers.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.customers.iteration}
				</td>
				<td align="center" class="tableField">
					{$c.first_name_cus}
				</td>
				<td align="center" class="tableField">
					{$c.last_name_cus}
				</td>
				<td align="center" class="tableField">
					{$c.birthdate_cus}
				</td>
				<td align="center" class="tableField">
					{$c.travel_days}
				</td>
				<td align="center" class="tableField">
					{$c.price}
				</td>
				<td align="center" class="tableField">
					{$c.total_price}
					<input type="hidden" name="customer_list[{$c.serial_cus}][serial]" value="{$c.serial_cus}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][price]" value="{$c.price}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][cost]" value="{$c.cost}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][total_price]" value="{$c.total_price}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][total_cost]" value="{$c.total_cost}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][destination_cit]" value="{$c.destination_cit}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][start_trl]" value="{$c.start_trl}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][end_trl]" value="{$c.end_trl}" />
					<input type="hidden" name="customer_list[{$c.serial_cus}][travel_days]" value="{$c.travel_days}" />
				</td>
			</tr>
			{if $c.extras}
				{foreach name="extras" from=$c.extras item="e"}
				<tr {if $smarty.foreach.extras.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">&nbsp;</td>
					<td align="center" class="tableField">
						{$e.first_name_cus|htmlall}
					</td>
					<td align="center" class="tableField">
						{$e.last_name_cus|htmlall}
					</td>
					<td align="center" class="tableField">
						{$e.birthdate_cus}
					</td>
					<td align="center" class="tableField">
						{$c.travel_days}
					</td>
					<td align="center" class="tableField">
						{$e.price}
						<input type="hidden" name="customer_list[{$c.serial_cus}][extras][{$e.serial_cus}][serial]" id="customer_item_{$e.serial_cus}" value="{$e.serial_cus}" />
						<input type="hidden" name="customer_list[{$c.serial_cus}][extras][{$e.serial_cus}][relationship]" value="{$e.relationship_ext}" />
						<input type="hidden" name="customer_list[{$c.serial_cus}][extras][{$e.serial_cus}][price]" value="{$e.price}" />
						<input type="hidden" name="customer_list[{$c.serial_cus}][extras][{$e.serial_cus}][cost]" value="{$e.cost}" />
					</td>
					<td align="center" class="tableField">&nbsp;</td>
				</tr>
				{/foreach}
			{/if}
			{/foreach}
		</TBODY>
	</table>
	<input type="hidden" name="hdnTotalItems" id="hdnTotalItems" value="{$total_items}" />
</div>
<div class="span-24 last line center" id="paging_controls"></div>

{else}
<div class="prepend-7 span-10 append-7 last line">
	<div class="span-10 center error">
		Hubo errores al cargar la informaci&oacute;n del archivo. <br><br>
		Por favor vuelva a intentarlo.
	</div>
</div>
{/if}