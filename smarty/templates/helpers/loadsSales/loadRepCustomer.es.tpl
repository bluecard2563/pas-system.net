<div class="span-24 last line separator">
    <label>Datos del Representante Legal</label>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label">* Identificaci&oacute;n:</div>
    <div class="span-5">
        <input type="text" name="repDocument" id="repDocument"  title='El campo "Identificaci&oacute;n - Representante" es obligatorio' {if $data.document_cus}value="{$data.document_cus}"{/if} />
        <input type="hidden" name="repSerialCus" id="repSerialCus" {if $data.serial_cus}value="{$data.serial_cus}"{/if} />
        <input type="hidden" name="repType" id="repType" value="PERSON">
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label">* Nombre:</div>
    <div class="span-5">
        <input type="text" name="repName" id="repName" title='El campo "Nombre - Representante" es obligatorio' {if $data.firstname_cus}value="{$data.firstname_cus|htmlall}" {/if} />
    </div>


    <div class="span-3 label">* Apellido:</div>
    <div class="span-4 append-2 last">
        <input type="text" name="repLastname" id="repLastname" title='El campo "Apellido - Representante" es obligatorio' {if $data.lastname_cus}value="{$data.lastname_cus|htmlall}" {/if} />
    </div>
</div>

<div class="span-24 last line" id="genderDiv" {if $data.type_cus eq LEGAL_ENTITY}style="display: none"{/if} title='El campo "Genero" es obligatorio.'>
    <div class="prepend-4 span-4 label">* G&eacute;nero:</div>
    <div class="span-5">  
        <input type="radio" name="rdGender" id="rdGender" value="H" {if $data.gender_cus eq 'H'}checked{/if} /> Hombre <br />
        <input type="radio" name="rdGender" id="rdGender" value="M" {if $data.gender_cus eq 'M'}checked{/if} /> Mujer
    </div>
</div>

<div class="span-20 prepend-4 last line" id="divAddress1">    
    <div class="span-3 label">* Fecha de Nacimiento:</div>
    <div class="span-5">
        <input type="text" name="repBirthday" id="repBirthday" title='El campo "Fecha de Nacimiento - Representante" es obligatorio' {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if} />
    </div>

    <div class="span-3 label">Email:</div>
    <div class="span-4 append-2 last">
        <input type="text" name="repEmail" id="repEmail" title='El campo "Email - Representante" es obligatorio' {if $data.email_cus}value="{$data.email_cus}"{/if} />
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-3 label" id="repCountryContainer">* Pa&iacute;s de Residencia:</div>
    <div class="span-5">
        <select name="repCountry" id="repCountry" class="span-5" title='El campo "Pa&iacute;s de Residencia - Representante" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}" {if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou|htmlall}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-3 label">* Ciudad</div>
    <div class="span-4 append-4 last" id="repCityContainerCustomer">
        <select name="selCityRepresentative" id="selCityRepresentative" class="span-4" title='El campo "Ciudad - Representante" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$cityList item="l"}
                <option value="{$l.serial_cit}" {if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit|htmlall}</option>
            {/foreach}
        </select>
    </div>
</div>