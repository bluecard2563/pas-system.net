
<div class="span-16 last line title">
    DECLARACI&Oacute;N DE SALUD<br />
 </div>
{foreach from=$questionList item="q"}
<div class="span-16 last line">
	<div class="prepend-1 span-12">
		{$q.text_qbl|htmlall}
	</div>
	<div class="span-1 append-1 last">
	{if $q.type_que eq YN}
	<select name="answer{$extraPosition}_{$q.serial_que}" id="answer{$extraPosition}_{$q.serial_que}" groupid="{$extraPosition}" title="Es necesario responder todas las preguntas para poder continuar"
		{if $editionAllowed == 'NO'}disabled{/if}>
		<option value="">- Seleccione -</option>
		<option value="YES" {if $q.yesno_ans eq YES}selected{/if}>S&iacute;</option>    
		<option value="NO" {if $q.yesno_ans eq NO}selected{/if}>No</option>
	</select>
	{else}
		<textarea name="txaAns" id="txaAns" value="" title="Es necesario responder todas las preguntas para poder continuar" class="textArea"></textarea>
	{/if}
	</div>
</div>
<div class="span-16 last line">
</div>
{/foreach}
