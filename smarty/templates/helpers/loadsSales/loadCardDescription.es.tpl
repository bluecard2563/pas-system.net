{if $cardInfo}
    <br/>
        <div class="span-24 last title">
                Detalles de la tarjeta ingresada<br />
        </div>
	<div class="span-24 last line">
           <table border="0" id="salesTable" style="color: #000000;">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            No. Tarjeta
                        </td>
						<td align="center" class="tableTitle">
                            Cliente
                        </td>
                        <td align="center"  class="tableTitle">
                            Tipo
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Inicio
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Expira
                        </td>
                        <td align="center" class="tableTitle">
                            D&iacute;as Contratados
                        </td>
                        <td align="center" class="tableTitle">
                            D&iacute;as Disponibles
                        </td>
						{if $front_type eq 'report'}
						<td align="center" class="tableTitle">
                           Viajes - PDF
                        </td>
						<td align="center" class="tableTitle">
                           Viajes - XLS
                        </td>
						{else}
						<td align="center" class="tableTitle">
                            Registrar
                        </td>
						{/if}

                    </tr>
                    </THEAD>
                    <TBODY>
                        <tr bgcolor="#d7e8f9">
                            <td align="center" class="tableField">
								{$cardInfo.card_number_sal}
                            </td>
							 <td align="center" class="tableField">
								{$cardInfo.customer_name|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$cardInfo.name_pbl|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$cardInfo.begin_date_sal}
                            </td>
                            <td align="center" class="tableField">
                                {$cardInfo.end_date_sal}
                            </td>
                            <td align="center" class="tableField">
                                {$cardInfo.days_sal}
                            </td>
                            <td align="center" class="tableField">
                                 {$cardInfo.available_days}
                            </td>
							{if $front_type eq 'report'}
							<td align="center" class="tableTitle">
								<a href="{$document_root}modules/reports/sales/pTravelsByCardPDF/{$cardInfo.serial_sal}" target="_blank"><img src="{$document_root}img/file_acrobat.gif" alt="pdf"/></a>
							</td>
							<td align="center" class="tableTitle">
								<a href="{$document_root}modules/reports/sales/pTravelsByCardExcel/{$cardInfo.serial_sal}" target="_blank"><img src="{$document_root}img/page_excel.png" alt="xls"/></a>
							</td>
							{else}
							<td align="center" class="tableField">
								{if $cardInfo.available_days > 0}<a href="{$document_root}modules/sales/travelRegister/fRegisterTravel/{$cardInfo.serial_sal}">Registrar</a>{else}No tiene dias disponibles{/if}
							</td>
							{/if}

                        </tr>
                    </TBODY>
           </table>
	</div>
     <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
{else}
<div class="span-10 append-5 prepend-7 label">No hay informaci&oacute;n relacionada a la tarjeta dada.</div>
{/if}