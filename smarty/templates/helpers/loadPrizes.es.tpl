{if $prizeList}
<select name="selPrize" id="selPrize" title='El campo "Premio" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach name="prizes" from=$prizeList item="l"}
  <option value="{$l.serial_pri}" {if $smarty.foreach.prizes.total eq 1}selected{/if}>{$l.name_pri}</option>
  {/foreach}
</select>
{else}
    {if $serial_cou neq '' or $type_pri neq ''}
		No existen premios registrados.
    {/if}
    <select name="selPrize" id="selPrize" title='El campo "Premio" es obligatorio' {if $serial_cou neq '' or $type_pri neq ''}style="display:none"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}