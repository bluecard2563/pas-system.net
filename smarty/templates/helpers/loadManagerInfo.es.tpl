<div class="span-24 last line separator">
		<label>Informaci&oacute;n del Gerente:</label>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">* Nombre del Gerente:</div>
	<div class="span-5">
		<input type="text" name="txtNameManager" id="txtNameManager" title='El campo de "Nombre del Gerente" es obligatorio.'  value="{$data.manager_name}"/>
	</div>

	<div class="span-3 label">* Tel&eacute;fono del Gerente:</div>
	<div class="span-4 append-4 last">
		<input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio.' value="{$data.manager_phone}"/>
	</div>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">* E-mail del Gerente:</div>
	<div class="span-5">
		<input type="text" name="txtMailManager" id="txtMailManager" title='El campo de "E-mail del Gerente" es obligatorio.' value="{$data.manager_email}"/>
	</div>

	<div class="span-3 label">Fecha de Nacimiento:</div>
	<div class="span-5 append-3 last">
		<input type="text" name="txtBirthdayManager" id="txtBirthdayManager" value="{$data.manager_birthday}"/>
	</div>
</div>
{literal}
<script type="text/javascript">
	setDefaultCalendar($("#txtBirthdayManager"),'-100y','+0d');
</script>
{/literal}