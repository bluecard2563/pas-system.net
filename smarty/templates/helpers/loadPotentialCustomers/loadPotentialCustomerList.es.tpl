{if $potentialCustomerList}
<div class="span-24 last line">
	<table border="0" id="tblPotentialCustomers" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					Nombre
				</td>
				<td align="center" class="tableTitle">
					Identificaci&oacute;n
				</td>
				<td align="center" class="tableTitle">
					Fecha de Nacimiento
				</td>
				<td align="center" class="tableTitle">
					Tel&eacute;fono
				</td>
				<td align="center"  class="tableTitle">
					E-Mail
				</td>
				<td align="center" class="tableTitle">
					Comentarios
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="pCus" from=$potentialCustomerList item="pc"}
			<tr {if $smarty.foreach.pCus.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.pCus.iteration}
				</td>
				<td align="center" class="tableField">
					{$pc.first_name_pcs|htmlall} {$pc.last_name_pcs|htmlall}
				</td>
				<td align="center" class="tableField">
					{$pc.document_pcs}
				</td>
				<td align="center" class="tableField">
					{if $pc.birthdate_pcs eq ''}-{else}{$pc.birthdate_pcs}{/if}
				</td>
				<td align="center" class="tableField">
					{$pc.phone1_pcs}
				</td>
				<td align="center" class="tableField">
					{$pc.email_pcs}
				</td>
				<td align="center" class="tableField">
					{if $pc.comments_pcs eq ''}-{else}{$pc.comments_pcs|htmlall}{/if}
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last buttons" id="pageNavPosition"></div>
{else}
<div class="prepend-7 span-9 append-8 label last" align="center">
		No existen Clientes Potenciales con los datos ingresados.
</div>
{/if}