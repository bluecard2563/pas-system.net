{if $productList}
    <select name="selProduct" id="selProduct" class="select" title="El campo 'Producto' es obligatorio.">
         <option value="" serial_pro="">- Seleccione -</option>
         <option value="-1" serial_pro="-1">- Todos los Productos -</option>
         {foreach from=$productList item="l"}
         	<option value="{$l.serial_pxc}" id="product_{$l.serial_pxc}">{$l.name_pbl}</option>	
         {/foreach}
     </select>
{else}
    No existen productos asignados al pa&iacute;s.
    <select id="selProduct" name="selProduct"  title="Por favor seleccione un producto." style="display: none;" >
        <option value="">- Seleccione -</option>
    </select>
{/if}