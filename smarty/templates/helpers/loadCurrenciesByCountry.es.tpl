{if $currencyList}
<center>
    <div class="span-19 last line">
        <table border="0" class="paginate-1 max-pages-7" id="theTable">
            <THEAD>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="t"}
                    <td align="center" class="tableTitle">
                        {$t}
                    </td>
                    {/foreach}
                </tr>
            </THEAD>
            <TBODY>

            {foreach name="currencyList" from=$currencyList item="cl"}
            <tr {if $smarty.foreach.currencyList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td class="tableField">
                    {$smarty.foreach.currencyList.iteration}
                </td>

                <td class="tableField">
                    {$cl.name_cou}
                </td>
                <td class="tableField">
                    {$cl.name_cur} ({$cl.symbol_cur})
                <td class="tableField">
                    <input type="radio" id="rad{$cl.serial_cur}" serial_cur="{$cl.serial_cur}" name="officialCur" value="{$cl.serial_cur}" {if $cl.official_cbc eq "YES"} official="YES" checked {/if} />
                </td>
                <td class="tableField">
                    {if $cl.status_cbc eq "ACTIVE"}Activo{else}Inactivo{/if}
                </td>
                <td class="tableField">
                    {if $cl.serial_cur neq "1"}
                        {if $cl.status_cbc eq "ACTIVE"}
                        <img src="{$document_root}img/inactive.gif" border="0" id="deactivate{$cl.serial_cur}" serial_cur="{$cl.serial_cur}" official_cbc="{$cl.official_cbc}" class="delete" width="25px" height="25px" style="cursor:pointer" />
                        {else}
                        <img src="{$document_root}img/active.gif" border="0" id="activate{$cl.serial_cur}" serial_cur="{$cl.serial_cur}" official_cbc="{$cl.official_cbc}" class="delete" width="25px" height="25px" style="cursor:pointer" />
                        {/if}
                    {else}
                    -
                    {/if}
                </td>
            </tr>
            {/foreach}
            </TBODY>
        </table>
    </div>
</center>
{if $currency}
<div class="span-19 last line separator">
        <label>Asignar nueva moneda:</label>
</div>

<div class="span-19 last line">
    <div class="prepend-5 span-2">
        <label>* Moneda:</label>
    </div>
    <div class="span-3">
        <select name="selCurrency" id="selCurrency" title='El campo "Moneda" es obligatorio.'>
          <option value="">- Seleccione -</option>
          {foreach from=$currency item="l"}
          <option value="{$l.serial_cur}">{$l.name_cur}</option>
          {/foreach}
        </select>
    </div>

    <div class="span-4 last line">
        <input type="checkbox" name="chkOfficial" id="chkOfficial" /> Moneda oficial
    </div>

    <div class="span-19 last buttons line">
        <input type="button" value="Asignar moneda" name="btnNewCurByCou" id="btnNewCurByCou" />
    </div>
    
</div>
{else}
<div class="prepend-5 last line">El pa&iacute;s ya tiene asignadas todas las monedas registradas.</div>
{/if}
{else}
{if $serial_cou}<div class="prepend-5 last line">El pa&iacute;s seleccionado no tiene monedas asignadas.</div>{/if}
{/if}

{literal}
<script type="text/javascript">
    $("#frmSearchBonus").validate({
        errorLabelContainer: "#alerts",
        wrapper: "li",
        onfocusout: false,
        onkeyup: false,
        rules: {
            selZone: {
                required: true
            },
            selCountry: {
                required: true
            },
            txtNewPercentage: {
                percentage: true
            },
        messages: {
            txtNewPercentage:{
                percentage: 'El campo "Porcentaje" solo admite valores entre 1 y 100.'
            }
        }
        }
    });

    $('#selAll').bind("click",function(){
        if($('#selAll').attr("checked")) {
            $("input[id^='chk']").attr("checked", true);
        } else {
            $("input[id^='chk']").attr("checked", false);
        }
    });


    $("input[id^='rad'][official!='YES']").bind("click",function(){
        if(confirm("Est\u00e1 seguro de que quiere que esta sea la moneda oficial?")) {
        $.ajax({
            type: "POST",
            url: document_root+"rpc/changeOfficialCurrencyByCountry.rpc",
            data: ({
                serial_cur : $(this).attr('serial_cur'),
                serial_cou : $("#selCountry").val(),
                official_cbc : "YES",
                status_cbc : "ACTIVE"
            }),
            dataType: "json",
            success: function(data) {
                if(data) {
                    $('#currencyContainer').load(document_root+"rpc/loadCurrenciesByCountry.rpc",
                    {serial_cou:$('#selCountry').val()});
                } else {
                    alert("Ocurrio un error. Intentar de nuevo.");
                }
            }
        });
        } else {
            $("input[id^='rad'][official='YES']").attr('checked',true);
        }
    });

    $("img[id^='deactivate']").bind("click",function(){
        if($(this).attr('official_cbc')=="NO") {
            if(confirm("Est\u00e1 seguro de que quiere desactivar la moneda?")) {
            $.ajax({
                type: "POST",
                url: document_root+"rpc/changeCurrencyByCountryStatus.rpc",
                data: ({
                    serial_cur : $(this).attr('serial_cur'),
                    serial_cou : $("#selCountry").val(),
                    official_cbc : $(this).attr('official_cbc'),
                    status_cbc : "INACTIVE"
                }),
                dataType: "json",
                success: function(data) {
                    if(data) {
                        $('#currencyContainer').load(document_root+"rpc/loadCurrenciesByCountry.rpc",
                        {serial_cou:$('#selCountry').val()});
                    } else {
                        alert("Ocurrio un error. Intentar de nuevo.");
                    }
                }
            });
            }
        } else {
            alert("No se puede desactivar la moneda oficial.");
        }
    });

    $("img[id^='activate']").bind("click",function(){
        if(confirm("Est\u00e1 seguro de que quiere activar la moneda?")) {
        $.ajax({
            type: "POST",
            url: document_root+"rpc/changeCurrencyByCountryStatus.rpc",
            data: ({
                serial_cur : $(this).attr('serial_cur'),
                serial_cou : $("#selCountry").val(),
                official_cbc : $(this).attr('official_cbc'),
                status_cbc : "ACTIVE"
            }),
            dataType: "json",
            success: function(data) {
                if(data) {
                    $('#currencyContainer').load(document_root+"rpc/loadCurrenciesByCountry.rpc",
                    {serial_cou:$('#selCountry').val()});
                } else {
                    alert("Ocurrio un error. Intentar de nuevo.");
                }
            }
        });
        }
    });

    $("#btnNewCurByCou").bind("click",function(){
        if($("#chkOfficial").is(':checked')) {
            var isOfficial = "YES";
        } else {
            isOfficial = "NO";
        }
        $.ajax({
            type: "POST",
            url: document_root+"rpc/addCurrencyByCountry.rpc",
            data: ({
                serial_cur : $("#selCurrency").val(),
                serial_cou : $("#selCountry").val(),
                official_cbc : isOfficial,
                status_cbc : "ACTIVE"
            }),
            dataType: "json",
            success: function(data) {
                if(data) {
                    $('#currencyContainer').load(document_root+"rpc/loadCurrenciesByCountry.rpc",
                    {serial_cou:$('#selCountry').val()});
                } else {
                    alert("Ocurrio un error. Intentar de nuevo.");
                }
            }
        });
    });
</script>
{/literal}