{if $taxList}
<select name="selTax" id="selTax" title='El campo "Impuesto" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach from=$taxList item="l"}
  <option value="{$l.serial_tax}" >{$l.name_tax}</option>
  {/foreach}
</select>
{else}
	{if $serial_cou neq ''}
		No existen impuestos registrados.
    {/if}
    <select name="selTax" id="selTax" title='El campo "Impuesto" es obligatorio' {if $serial_cou neq ''}style="display:none"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}