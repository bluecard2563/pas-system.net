{if $mbcList}
    <select name="selManager" id="selManager">
        <option value="">- Seleccione -</option>
        {foreach from=$mbcList item="m"}
		<option value="{$m.serial_man}" >{$m.name_man}</option>
        {/foreach}
    </select>
{else}
    <select name="selManager" id="selManager">
      <option value="">- Seleccione -</option>
    </select>
{/if}