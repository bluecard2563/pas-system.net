{if $invoicesList}
    <table border="1" class="dataTable" id="invoicesTable">
        <THEAD>
            <tr class="header">
                <th style="text-align: center;">
                    <input type="checkbox" id="selAll" name="selAll">
                </th>
                {foreach from=$titles item="t"}
                <th style="text-align: center;">
                    {$t}
                </th>
                {/foreach}
            </tr>
        </THEAD>
        <TBODY>
            {foreach name="invoices" from=$invoicesList item="i"}
            <tr {if $smarty.foreach.invoices.iteration is even}class="odd"{else}class="even"{/if}>
                <td align="center" class="tableField">
                    <input type="checkbox" class="invoiceChk" value="{$i.serial_inv}" name="chk" id="chkInvoice{$i.serial_inv}" payment="{$i.serial_pay}" />
                </td>
                <td align="center" class="tableField">
                   {$i.date_inv}
                </td>
                <td align="center" class="tableField">
                    {$i.number_inv}
                </td>
                <td align="center" class="tableField">
                    {$i.invoice_to|htmlall}
                </td>
                <td align="center" class="tableField">
                    {$i.days}
                </td>
                <td align="center" class="tableField">
                    {if $i.days lt 31}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {if $i.days gt 30 && $i.days lt 61}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {if $i.days gt 60 && $i.days lt 91}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {if $i.days gt 90}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
            </tr>
            {/foreach}
        </TBODY>
    </table>

    <div class="span-24 last">
        <br/>* (A): Abono{*, (CI): Costo Internacional*}<br />
    </div>

    <div class="span-24 last buttons line">
        {if $convergeButton}<input type="submit" name="pay_cd" id="pay_cd" value="Pagar Tarjeta Cr&eacute;dito" />{/if}
        {if $payPhoneButton}<input type="submit" name="pay_pp" id="pay_pp" value="Pagar PayPhone" />{/if}
    </div>
{else}
    {if $serial_dea neq ''}<center><b>No existen cuentas por cobrar para esta Sucursal de Comercializador.</b></center>{/if}
{/if}