{if $responsablesList}
<select name="selResponsible" id="selResponsible" title="El campo 'Responsable' es obligatorio">
  <option value="">- Seleccione -</option>
  {foreach from=$responsablesList item="l" name="responsables"}
        <option value="{$l.serial_usr}" {if $smarty.foreach.responsables.total eq 1}selected{/if}>{$l.name_usr|htmlall}</option>
  {/foreach}
</select>
{else}
    {if $serial_cit neq '' and $serial_mbc neq ''}
	No existen responsables con facturas por pagar pendientes.
    {/if}
    <select name="selResponsible" id="selResponsible" title='El campo "Responsables" es obligatorio' {if $serial_cit neq '' and $serial_mbc neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
    
{/if}
