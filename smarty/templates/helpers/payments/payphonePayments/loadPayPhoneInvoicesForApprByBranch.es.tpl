{if $invoicesList}
    <table border="1" class="dataTable" id="payedInvoices">
        <THEAD>
            <tr bgcolor="#284787">
                {foreach from=$titles item="t"}
                    <th style="text-align: center;">
                        {$t}
                    </th>
                {/foreach}
            </tr>
        </THEAD>
        <TBODY>
            {foreach name="invoices" from=$invoicesList item="i"}
                <tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if} >
                    <td align="center" class="tableField">
                        {$i.date_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.number_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.invoice_to}
                    </td>
                    <td align="center" class="tableField">
                        {$i.total_to_pay}
                    </td>
                </tr>
            {/foreach}
        </TBODY>
    </table>
    <div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>
   
    <div class="span-24 last buttons line">
        <input type="hidden" name="transactionIDs" id="transactionIDs" value="{$transactionIDs}"/>
        <input type="button" name="vldInvoices" id="vldInvoices" value="Validar Pagos" />
    </div>
{else}
    <center><b>No existen pagos para Validar para esta Sucursal de Comercializador.</b></center>
{/if}
