{if $invoicesList}
    <table border="1" class="dataTable" id="invoicesTable">
        <THEAD>
            <tr class="header">
                <th style="text-align: center;">
                    <input type="checkbox" id="selAll" name="selAll" disabled>
                </th>
                {foreach from=$titles item="t"}
                <th style="text-align: center;">
                    {$t}
                </th>
                {/foreach}
            </tr>
        </THEAD>
        <TBODY>
            {foreach name="invoices" from=$invoicesList item="i"}
            <tr {if $smarty.foreach.invoices.iteration is even}class="odd"{else}class="even"{/if}>
                <td align="center" class="tableField">
                    {if $i.status=="PENDING"}
                        <input type="checkbox" class="invoiceChk" value="{$i.serial_inv}" name="chk" id="chkInvoice{$i.serial_inv}" payment="{$i.serial_pay}" disabled/>
                    {elseif $i.status=="APPROVED"}
                        <input type="checkbox" class="invoiceChk" value="{$i.serial_inv}" name="chk" id="chkInvoice{$i.serial_inv}" payment="{$i.serial_pay}" disabled/>
                    {else}
                        <input type="checkbox" class="invoiceChk" value="{$i.serial_inv}" name="chk" id="chkInvoice{$i.serial_inv}" payment="{$i.serial_pay}"/>
                    {/if}

                </td>
                <td align="center" class="tableField">
                   {$i.date_inv}
                </td>
                <td align="center" class="tableField">
                    {$i.number_inv}
                </td>
                <td align="center" class="tableField">
                    {$i.invoice_to|htmlall}
                </td>
                <td align="center" class="tableField">
                    {$i.days}
                </td>
                <td align="center" class="tableField">
                    {if $i.days lt 31}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {if $i.days gt 30 && $i.days lt 61}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {if $i.days gt 60 && $i.days lt 91}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
                <td align="center" class="tableField">
                    {if $i.days gt 90}
                        {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                    {/if}
                </td>
				{if (USE_PTP)  && $ptp_use eq "YES"}
                <td align="center" class="tableField">
                <form>
                    <input title="Ejecutar pago desde celular" name="btnPtPM" id="btnPtPM" type="button" value="Envio mensaje" onclick="document.getElementById('checkme').disabled=true;document.getElementById('btnPtPS').disabled=true;this.disabled=true;window.open('{$document_root}modules/sales/placeToPay/pSendMailPaymentPtoP.php?serial_inv={$i.serial_inv}&mail=true','_blank')"/>
                </form>
                </td>
				{/if}


            </tr>
            {/foreach}
        </TBODY>
    </table>

    <div class="span-24 last">
        <br/>* (A): Abono{*, (CI): Costo Internacional*}<br />
    </div>
{if (USE_PTP) && $ptp_use eq "YES"}
    <div class="span-24 last buttons line">
        {if $convergeButton}<input type="submit" name="pay_cd" id="pay_cd" value="Pagar Tarjeta Cr&eacute;dito" />{/if}

            {*<input type="submit" name="pay_ptp" id="pay_ptp" value="Pagar Place To Pay" />*}

    </div>
{else}
    {if $serial_dea neq ''}<center><b>No existen cuentas por cobrar para esta Sucursal de Comercializador.</b></center>{/if}
{/if}

{/if}