{if $dealerList}
    <select name="{if $serial_dea}selBranch{else}selDealer{/if}" id="{if $serial_dea}selBranch{else}selDealer{/if}" title='El campo {if $serial_dea}"Sucursal"{else}"Comercializador"{/if} es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="dealers" from=$dealerList item="l"}
            <option value="{$l.serial_dea}" {if $smarty.foreach.dealers.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
        {/foreach}
    </select>
{else}
    {if $serial_dea}
        {if $serial_dea neq '' and $serial_mbc neq ''}
            No existen sucursales con facturas por pagar pendientes.
        {/if}
        <select name="selBranch" id="selBranch" title='El campo {if $serial_dea}"Sucursal"{else}"Comercializador"{/if} es obligatorio' {if $serial_dea neq ''and $serial_mbc neq ''}style="display:none;"{/if}>
          <option value="">- Seleccione -</option>
        </select>
    {else}
        {if $serial_cit neq '' and $serial_usr neq '' and $serial_mbc neq ''}
            No existen comercializadores con facturas por pagar pendientes.
        {/if}
        <select name="selDealer" id="selDealer" title='El campo {if $serial_dea}"Sucursal"{else}"Comercializador"{/if} es obligatorio' {if $serial_cit neq ''and $serial_usr neq '' and $serial_mbc neq ''}style="display:none;"{/if}>
          <option value="">- Seleccione -</option>
        </select>
    {/if}
{/if}