{if $cityList}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="cities" from=$cityList item="l"}
        	<option value="{$l.serial_cit}" name="{$l.name_cit|htmlall}" {if $smarty.foreach.cities.total eq 1}selected{/if}>{$l.name_cit|htmlall}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq '' and $serial_mbc neq ''}
	No existen ciudades con facturas por pagar pendientes.
    {/if}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio' {if $serial_cou neq '' and $serial_mbc neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}