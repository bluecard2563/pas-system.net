{if $managerByCountryList}
    <select name="selManager" id="selManager" title='El campo "Ciudad" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="managers" from=$managerByCountryList item="l"}
        	<option value="{$l.serial_mbc}" {if $smarty.foreach.managers.total eq 1}selected{/if}>{$l.name_man|htmlall}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq ''}
	No existen representantes con facturas por pagar pendientes.
    {/if}
    <select name="selManager" id="selManager" title='El campo "Ciudad" es obligatorio' {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}