    {if $dealerList}
    <div class="span-5 last line">
        <select name="selBranch" id="selBranch" title='El campo "Sucursalde Comercializador" es obligatorio.' >
            <option value="">- Seleccione -</option>
            {foreach from=$dealerList name="dealers" item="l"}
                <option value="{$l.serial_dea}" {if $smarty.foreach.dealers.total eq 1}selected{/if}>{$l.name_dea}</option>
            {/foreach}
        </select>
    </div>
    {else}
    <div class="span-5 last line">
        <select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
            <option value="">- Seleccione -</option>
        </select>
    </div>
    {/if}
