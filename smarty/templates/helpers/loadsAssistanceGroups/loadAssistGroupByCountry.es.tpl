{if $groupList neq''}
<div class="span-5 last">
		<select name="selGroup" id="selGroup" title='El campo "Grupo" es obligatorio'>
			<option value="">- Seleccione -</option>
			{foreach from=$groupList name="groups" item="l"}
				<option value="{$l.serial_asg}" {if $smarty.foreach.groups.total eq 1}selected{/if}>{$l.name_asg|htmlall}</option>
			{/foreach}
		</select>
</div>
{else}
<div class="span-5 last">
		<select name="selGroup" id="selGroup" title='El campo "Grupo" es obligatorio'>
			<option value="">- Seleccione -</option>
		</select>
</div>
{/if}