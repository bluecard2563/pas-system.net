<div align="center">Disponibles</div>

{if $userList}
<select multiple id="usersFrom" name="usersFrom[]" class="selectMultiple span-4 last" >
	{if $type eq 'planet'}
		{foreach from=$userList item="l"}
		<option value="{$l.serial_usr}" class="planet">{$l.first_name_usr|htmlall}&nbsp;{$l.last_name_usr|htmlall}</option>
		{/foreach}
	{elseif $type eq 'dealer'}
		{foreach from=$userList item="l"}
		<option value="{$l.serial_usr}" class="dealer">{$l.first_name_usr|htmlall}&nbsp;{$l.last_name_usr|htmlall}</option>
		{/foreach}
	{elseif $type eq 'manager'}
		{foreach from=$userList item="l"}
		<option value="{$l.serial_usr}" class="manager">{$l.first_name_usr|htmlall}&nbsp;{$l.last_name_usr|htmlall}</option>
		{/foreach}
	{/if}
</select>
{else}
<select multiple id="usersFrom" name="usersFrom[]" class="selectMultiple span-4 last">
</select>
{/if}
