{if $sale}
<form name="frmRefundRequest" id="frmRefundRequest" action="{$document_root}modules/refund/pNewRefundRequest" method="post" >
	<div class="prepend-8 span-8 append-8 line">
		<div class="span-8 tableTitles">
		{if $refundType eq 'REGULAR'}
			Reembolso Regular
		{else}
			Reembolso por penalidad
		{/if}
		</div>
	</div>

	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line separator">
		<label>Datos de la Venta</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Cliente:</div>
        <div class="span-5">
            {$customer.first_name_cus|htmlall} {$customer.last_name_cus|htmlall}
        </div>

        <div class="span-3 label">Producto:</div>
        <div class="span-4 append-4 last">
            {$sale.aux.product|htmlall}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Comercializador:</div>
        <div class="span-5">
            {$dealer.dealer_name|htmlall}
        </div>

        <div class="span-3 label">Sucursal de Comercializador:</div>
        <div class="span-4 append-4 last">
            {$dealer.branch_name|htmlall}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Total de la Venta (1):</div>
        <div class="span-5">
            USD {$sale.total_sal}
			<input type="hidden" name="hdnTotalSale" id="hdnTotalSale" value="{$sale.total_sal}" />
        </div>

		<div class="span-3 label">Impuestos Aplicados (2):</div>
        <div class="span-4 append-4 last">
			{if $invoice.applied_taxes_inv}
			<table id="taxTable">
				<tr>
					<td class="tableTitles"><b>Impuesto</b></td>
					<td class="tableTitles"><b>Valor %</b></td>
				</tr>
				{foreach from=$invoice.applied_taxes_inv item="l"}
				<tr class="{cycle values='even-row,odd-row'}">
					<td class="refundTable">{$l.tax_name}</td>
					<td class="refundTable">{$l.tax_percentage}</td>
				</tr>
				{/foreach}
			</table>
			{else}
				No existen impuestos aplicados.
			{/if}
			<input type="hidden" name="hdnTaxes" id="hdnTaxes" value="{$taxes}"/>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Descuentos Aplicados (3):</div>
        <div class="span-5">
            % {if $invoice.discount_prcg_inv}{$invoice.discount_prcg_inv}{else}0{/if}
			<input type="hidden" name="hdnDiscount" id="hdnDiscount" value="{if $invoice.discount_prcg_inv}{$invoice.discount_prcg_inv}{else}0{/if}" />
        </div>

		<div class="span-3 label">Otros Descuentos (4):</div>
        <div class="span-4 append-4 last">
			% {if $invoice.other_dscnt_inv}{$invoice.other_dscnt_inv}{else}0{/if}
			<input type="hidden" name="hdnOtherDiscount" id="hdnOtherDiscount" value="{if $invoice.other_dscnt_inv}{$invoice.other_dscnt_inv}{else}0{/if}" />
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Valor Final de la Venta (1-3-4)*(2):</div>
        <div class="span-5">
            USD {$totalRefund}
			<input type="hidden" name="hdnTotalRefund" id="hdnTotalRefund" value="{$totalRefund}" />
        </div>
	</div>

	<div class="span-24 last line separator">
		<label>Datos de Cobertura</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Desde:</div>
        <div class="span-5">
            {$sale.begin_date_sal}
        </div>

        <div class="span-3 label">Hasta:</div>
        <div class="span-4 append-4 last">
            {$sale.end_date_sal}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Fecha de Emisi&oacute;n de la Factura:</div>
        <div class="span-5">
            {$invoice.date_inv}
        </div>
		
		{if $refundType eq 'REGULAR'}
        <div class="span-3 label">Fecha del Pago:</div>
        <div class="span-4 append-4 last">
            {$payment.date_pay}
        </div>
		{/if}
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Fecha de Emisi&oacute;n de la Venta:</div>
        <div class="span-5">
            {$sale.emission_date_sal}
        </div>

        <div class="span-3 label">Fecha de Ingreso al Sistema</div>
        <div class="span-4 append-4 last">
            {$sale.in_date_sal}
        </div>
	</div>

	<div class="span-24 last line separator">
		<label>Datos de la Solicitud</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Motivo del Reembolso:</div>
        <div class="span-5">
            <select id="selRefundReason" name="selRefundReason" title="El campo 'Motivo' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$refundData.reason item="l"}
				<option value="{$l}">{$global_refundReasons.$l}</option>
				{/foreach}
			</select>
        </div>

        <div class="span-3 label">* Documentos Requeridos:</div>
        <div class="span-7 append-1 last">
			{foreach from=$documentList item="l" name="documentList"}
				<input type="checkbox" name="document[]" id="document_{$smarty.foreach.documentList.iteration}" value="{$l|htmlall}" /> {$l|htmlall}<br>
			{/foreach}
			<input type="hidden" name="hdnDocument" id="hdnDocument" value="" title="Debe escoger al menos un 'Documento'.">
			<b><i>Otro:</i></b> <input type="text" name="OtherDocument" id="document_0" />
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Tipo de Retenci&oacute;n:</div>
        <div class="span-5">
			<select id="selPenaltyType" name="selPenaltyType" title="El campo 'Tipo de retenci&o&oacute;n' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$refundData.retainedType item="l"}
				<option value="{$l}" {if $directSaleFee.type eq $l}selected{/if}>{$global_refundRetainedTypes.$l}</option>
				{/foreach}
			</select>
        </div>

        <div class="span-3 label">* Valor Retenido:</div>
        <div class="span-7 append-1 last">
			<input type="text" id="txtPenaltyFee" readonly name="txtPenaltyFee" title="El 'Valor Retenido' es obligatorio." value="0"/>
                        <!--value="$directSaleFee.value}-->
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Observaciones:</div>
        <div class="span-5">
            <textarea name="txtObservations" id="observations" title="El campo 'Observaciones' es obligatorio"></textarea>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-10 append-10" style="text-align: center; font-style: italic;">
			{if $paiedAmounts}
				Valores Cancelados en esta venta:<br>
				<table id="tblDiscounts" name="tblDiscounts">
				<tr>
					<td class="tableTitles"><b>Concepto</b></td>
					<td class="tableTitles"><b>Valor</b></td>
				</tr>
				{if $comission}
				<tr class="{cycle values='even-row,odd-row'}">
					<td class="refundTable">Comisi&oacute;n: </td>
					<td class="refundTable">
						$ {$comission.value_dea_com}
						<input type="hidden" value="{$comission.value_dea_com}" id="discount_comission" name="discount_comission" />
					</td>
				</tr>
				{/if}
				{if $bonus}
				<tr class="{cycle values='even-row,odd-row'}">
					<td class="refundTable">Incentivos: </td>
					<td class="refundTable">
						$ {$bonus.total_amount_apb}
						<input type="hidden" value="{$bonus.total_amount_apb}" id="discount_bonus" name="discount_bonus"/>
					</td>
				</tr>
				{/if}
			</table>
			{else}
				No se ha pagado ninguna comisi&oacute;n o incentivo para esta tarjeta.
			{/if}
		</div>

		<div class="prepend-8 span-5 label">Valor a Reembolsar:</div>
        <div class="append-3 span-8 last">
			<label id="lblTotalToPay"></label>
			<input type="hidden" id="total_to_pay" name="total_to_pay" value=""/>
        </div>
	</div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Enviar solicitud"/>
		<input type="hidden" name="hdnRefundType" id="hdnRefundType" value="{$refundType}"/>
		<input type="hidden" name="hdnDirectSale" id="hdnDirectSale" value="{$sale.direct_sale_sal}"/>
		<input type="hidden" name="hdnSerial_sal" id="hdnSerial_sal" value="{$sale.serial_sal}"/>
	</div>
</form>
{else}
	<div class="prepend-8 span-8 append-8 last">
	{if $error eq 1}
		<div class="span-8 error">
			No se puede continuar porque la tarjeta ingresada no existe.
		</div>
	{elseif $error eq 2}
		<div class="span-8 error">
			No se puede procesar el reembolso porque la tarjeta a&uacute;n no ha sido puesta en Stand-By.
		</div>
	{elseif $error eq 3}
		<div class="span-8 error">
			Hubo errores en la carga del formulario de solicitud. Por favor comun&iacute;quese con el administrador.
		</div>
	{elseif $error eq 4}
		<div class="span-8 error">
			No se puede continuar debido a que la tarjeta no ha sido pagada en su totalidad.
		</div>
	{elseif $error eq 5}
		<div class="span-8 error">
			Ya existe un proceso iniciado de reembolso para esta tarjeta.
		</div>
	{elseif $error eq 6}
		<div class="span-8 error">
			La tarjeta no puede ser reembolsada ya que es del tipo corporativa. Lo sentimos.
		</div>
	{elseif $error eq 7}
		<div class="span-8 error">
			La tarjeta ya tiene asistencias asociadas por lo que no puede ser reembolsada.
		</div>
	{elseif $error eq 8}
		<div class="span-8 error">
			La tarjeta tiene un historial de reactivaciones por lo que no puede ser reembolsada. Lo sentimos.
		</div>
	{elseif $error eq 9}
		<div class="span-8 error">
			La tarjeta tiene solicitudes de modificaci&oacute;n pendientes. Lo sentimos.
		</div>
	{elseif $error eq 10}
		<div class="span-8 error">
			La tarjeta no puede ser reembolsada ya que es del tipo masiva. Lo sentimos.
		</div>
	{/if}
	</div>
{/if}