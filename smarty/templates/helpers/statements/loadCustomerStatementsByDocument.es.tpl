{if $customerStatements}
    <div id="filterContainer" class="last line">
        <input type="text" id="search" placeholder="Buscar" style="width: 40%; height: 20px; float: left; margin-left: 10px; margin-bottom: 5px;" />
        <input type="button" id="btnNewSearch" name="btnNewSearch" value="Nueva Búsqueda"  style="width: 16%; height: 25px; float: right; margin-right: 10px; margin-bottom: 5px;" onclick="cleanForm()"/>
    </div>
    <div class="last line" style="margin-left: 10px; margin-right: 10px;">
        <table id="customerStatementsTable" border="1" class="dataTable" align="center">
            <thead>
            <tr class="header">
                <th style="text-align: center; height: 30px;">Id</th>
                <th style="text-align: center; height: 30px;">Tarjeta</th>
                <th style="text-align: center; height: 30px;">Documento</th>
                <th style="text-align: center; height: 30px;">Nombre</th>
                <th style="text-align: center; height: 30px;">Email</th>
                <th style="text-align: center; height: 30px;">Origen</th>
                <th style="text-align: center; height: 30px;">Fecha Creación</th>
                <th style="text-align: center; height: 30px;" colspan="3">Acciones</th>
            </tr>
            </thead>
            <tbody>
            {foreach name="customerStatements" from=$customerStatements item=cus}
                <tr {if $smarty.foreach.customer.iteration is even}class="odd {$cus.hidden}"
                    {else}class="even {$cus.hidden}"{/if} >
                    <td class="tableField">{$cus.statement_id}</td>
                    <td class="tableField">{$cus.card_number}</td>
                    <td class="tableField">{$cus.customer_document}</td>
                    <td class="tableField">{$cus.customer_name}</td>
                    <td class="tableField">{$cus.customer_email}</td>
                    <td class="tableField">{$cus.system_origin_es}</td>
                    <td class="tableField">{$cus.created_at}</td>
                    <td class="tableField">
                        <a id="{$cus.statement_id}" class="{$cus.disabled}" href="{$cus.statement_link}"
                           target="_blank">
                            <span class="fa fa-file-pdf-o fa-lg {$cus.disabled}"
                                  title="Ver Declaración de Salud"></span>
                        </a>
                    </td>
                    <td class="tableField">
                    <span id="{$cus.statement_id}" class="fa fa-envelope-o fa-lg pointer {$cus.disabled}"
                          title="Reenviar declaración via email" onclick="sendStatementByEmail(this)" ;></span>
                    </td>
                    <td class="tableField">
                        <label class="checkbox-container" title="Activar/Desactivar">
                            <input type="checkbox" id="{$cus.statement_id}" value="{$cus.statement_id}"
                                   {if $cus.status eq 'ACTIVE'}checked='checked'{/if}
                                   onclick="changeCustomerStatementStatus(this);">
                            <span class="checkmark"></span>
                        </label>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <div id="pageNavPosition" class="span-24 last line buttons"></div>
    </div>
{else}
    <div id="notFoundMessage" class="prepend-8 span-8 append-8 last" align="center">
        <div class="span-8 error last">
            No se encontraron Declaraciones de Salud del Cliente Solicitado.
        </div>
    </div>
{/if}
