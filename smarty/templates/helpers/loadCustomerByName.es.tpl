{if $data}
    <div class="span-21 last line separator">
        <label>Informaci&oacute;n del cliente:</label>
    </div>

    <center>
        <div class="span-21 last line">
            <table border="0" class="paginate-1 max-pages-7" id="theTable">
                <THEAD>
                    <tr bgcolor="#284787">
                        {foreach from=$titles item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;  color: #FFFFFF;">
                            {$t}
                        </td>
                        {/foreach}
                    </tr>
                </THEAD>
                <TBODY>

                {foreach name ="data" from=$data item="d"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.data.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                     <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.name_cou}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.name_cit}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.document_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.first_name_cus} {$d.last_name_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.birthdate_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.phone1_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.cellphone_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.email_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$d.address_cus}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        <a id="viewSales{$d.serial_cus}" serial="{$d.serial_cus}" style="cursor: pointer;">Ver tarjetas</a>
                    </td>
                </tr>
                {/foreach}
                </TBODY>
            </table>
        </div>
        <div class="span-22 last pageNavPosition" id="pageNavPosition"></div>
        
    </center>
    {foreach from=$sales key=k item="sal"}
    <div class="span-21 last line" id="salesContainer{$k}" style="display:none">
            <div class="span-21 last line separator">
                <label>Informaci&oacute;n de la(s) tarjeta(s):</label>
            </div>
            {if $sal}
            <center>
                <div class="span-21 last line">
                    <table border="0" class="paginate-1 max-pages-7" id="theTable">
                        <THEAD>
                            <tr bgcolor="#00b3f0">
                                {foreach from=$titles_sal item="t"}
                                <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">
                                    <font color=#000000>
                                    {$t}
                                    </font>
                                </td>
                                {/foreach}
                            </tr>
                        </THEAD>
                        <TBODY>

                        {foreach name="sales" from=$sal item="s"}
                        {if $s.card_number_sal neq ''}
                        <tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.sales.iteration is even} bgcolor="#c5dee7" {else} bgcolor="#e5e5e5" {/if}>
                             <td id="cardNumber{$s.card_number_sal}" style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; color: red; cursor: pointer">
                                {$s.card_number_sal}
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$s.emission_date_sal}
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$s.begin_date_sal}
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$s.end_date_sal}
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; {if $s.status_sal neq 'ACTIVE'}color:#CC0033;{/if}">
                                {$global_salesStatus[$s.status_sal]}
                            </td>
                        </tr>
                        {/if}
                        {/foreach}
                        </TBODY>
                    </table>
                </div>
            </center>
            {else}
                <center>El cliente seleccionado no tiene ninguna tarjeta activa asignada.</center>
            {/if}
        </div>
    {/foreach}
{else}
    {if $name_cus}<center>No existe ninguna coincidencia en el sistema con el nombre ingresado.</center>{/if}
{/if}



{literal}
<script type="text/javascript">
    if($("#theTable").length>0) {
        var pager = new Pager('theTable', 8 , 'Siguiente', 'Anterior');
        pager.init(7); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
    }

    $("a[id^='viewSales']").bind("click",function(){
        if($(this).html()=="Ver tarjetas") {
            $("a[id^='viewSales']").html("Ver tarjetas");
            $(this).html("Ocultar tarjetas");
            $("[id^='salesContainer']").hide();
            $("#salesContainer"+$(this).attr("serial")).show();
        } else {
            $("a[id^='viewSales']").html("Ver tarjetas");
            $("[id^='salesContainer']").hide();
        }
    });

    $("td[id^='cardNumber']").bind("click",function(){
        $("#txtCard").val(trim($(this).html()));
        $("#customerInfoContainer").html("");
        $("#txtCustomerID").val("");
        $("#txtCustomerName").val("");
    });

    function trim (myString) {
        return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
    }
</script>
{/literal}