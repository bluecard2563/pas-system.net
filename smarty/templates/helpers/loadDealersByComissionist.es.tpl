{if $noData neq 1}
    {if $dealerList}
    <div class="prepend-5 span-5 label">*Seleccione el comercializador:</div>
    <div class="span-5 append-3 last line">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
            <option value="">- Seleccione -</option>
            {foreach from=$dealerList item="l"}
                <option value="{$l.serial_dea}">{$l.name_dea|htmlall} - {$l.code_dea}</option>
            {/foreach}
        </select>
    </div>
    {else}
    <div class="prepend-8 span-9 append-4 last">No existen Comercializadores para el responsable seleccionado.</div>
    {/if}
{else}
	<div class="prepend-5 span-5 label">*Seleccione el comercializador:</div>
    <div class="span-5 append-3 last line">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
            <option value="">- Seleccione -</option>
        </select>
    </div>
{/if}

{literal}
<script language="javascript">
$("#selDealer").bind("change",function(){loadBranches(this.value)});
/*LIST SELECTOR*/
$("#moveAll").click( function () {
	$("#dealersFrom option").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'" class="'+$(this).attr('class')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeAll").click( function () {
	$("#dealersTo option").each(function (i) {
			if($(this).attr('class') == $('#selDealer').val()) {
				$(this).remove();
		}
	});
	loadBranches($('#selDealer').val());	
});

$("#moveSelected").click( function () {
	$("#dealersFrom option:selected").each(function (i) {
		$("#dealersTo").append('<option value="'+$(this).attr('value')+'"  class="'+$(this).attr('class')+'" selected="selected">'+$(this).text()+'</option>');
		$(this).remove();
	});
});

$("#removeSelected").click( function () {				  
	$("#dealersTo option:selected").each(function (i) {
			if($(this).attr('class') == $('#selDealer').val()) {
				$(this).remove();
		}
	});
	loadBranches($('#selDealer').val());
});
/*END LIST SELECTOR*/
function loadBranches(deaID) {
	var selBranches="0";
	for(var i=0; i < $("#dealersTo *").length; i++) {
		if(i==0) {
			selBranches+=$("#dealersTo").children()[i].value;
		}
		else{
			selBranches+=","+$("#dealersTo").children()[i].value;
		}
	}
	$("#dealersFromContainer").load(document_root+"rpc/loadBranchByDealer.rpc",{serial_dea: deaID, selBranches: selBranches});
}
</script>
{/literal}