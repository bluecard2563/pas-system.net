{if $uncollectables}
<div class="span-24 last line">
	<table id="uncollectableTable">
		<thead>
			 <tr class="header">
				<th style="text-align: center;"><input type="checkbox" name="checkAll" id="checkAll" /></th>
				<th style="text-align: center;">N&uacute;mero de Tarjetas</th>
				<th style="text-align: center;">Sucursal Comercializador</th>
				<th style="text-align: center;">Fecha de Emisi&oacute;n</th>
				<th style="text-align: center;">Fecha M&aacute;xima de Pago</th>
				<th style="text-align: center;">D&iacute;as Impago</th>
				<th style="text-align: center;">Monto de la Factura</th>
				<th style="text-align: center;">Monto Incobrable</th>
			</tr>
		</thead>
		<tbody>
		{foreach from=$uncollectables item="u" name="uncollectables"}
			<tr {if $smarty.foreach.uncollectables.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
				<td align="center" class="tableField">
					<input type="checkbox" name="check" id="check_{$u.serial_inv}" value="{$u.serial_inv}" />
				</td>
				<td align="center" class="tableField">{$u.cards}</td>
				<td align="center" class="tableField">{$u.name_dea|htmlall}</td>
				<td align="center" class="tableField">{$u.date_inv}</td>
				<td align="center" class="tableField">{$u.due_date_inv}</td>
				<td align="center" class="tableField">{$u.uncollectable_time}</td>
				<td align="center" class="tableField">$ {$u.total_inv}</td>
				<td align="center" class="tableField">$ {$u.uncollectable_amount}</td>
			</tr>
		{/foreach}
		</tbody>
	</table>
	<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
</div>

<div class="span-24 last line center" id="pageNavigation"></div>

<div class="span-24 last line buttons">
	<input type="hidden" name="pages" id="pages" value="{$pages}"/>
	<input type="hidden" name="hdnValidateOne" id="hdnValidateOne" value="" title="Seleccione al menos un registro."/>
	<input type="submit" name="btnSubmit" id="btnSubmit" value="Dar de Baja" />
</div>
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 error">
		No existen facturas incobrables para el representante seleccionado.
	</div>
</div>
{/if}