{if $excessPaymentsList}
<br/>
	<div class="span-24 last title">
			Lista de Pagos en Exceso elegibles<br />
			<label>Recuerde que &uacute;nicamente se pueden liquidar valores de una misma persona.</label>
	</div>
	   <table border="0" id="paymentsTable">
				<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						<input type="checkbox" id="selAll" name="selAll">
					</td>
					<td align="center" class="tableTitle">
						No. de Pago
					</td>
					<td align="center" class="tableTitle">
						Fecha del Pago
					</td>
					<td align="center"  class="tableTitle">
						Monto a Pagar
					</td>
					<td align="center"  class="tableTitle">
						Monto Pagado
					</td>
					<td align="center"  class="tableTitle">
						Exceso Generado
					</td>
					<td align="center" class="tableTitle">
						Disponible
					</td>
					<td align="center" class="tableTitle">
						Monto a favor de:
					</td>
				</tr>
				</THEAD>
				<TBODY>
				  {foreach name="payments" from=$excessPaymentsList item="s"}
					<tr {if $smarty.foreach.payments.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
						<td align="center" class="tableField">
							<input type="checkbox" value="{$s.serial_pay}" name="chkPayment[]" class="paymentsChk" remote_id="{$s.serial}" in_favor_to="{$s.in_favor_to}"/>
						</td>
						<td align="center" class="tableField">
						   {$s.payment_number}
						</td>
						<td align="center" class="tableField">
						   {$s.date_pay}
						</td>
						<td align="center" class="tableField">
						   {$s.total_to_pay_pay}
						</td>
						<td align="center" class="tableField">
						   {$s.total_payed_pay}
						</td>
						<td align="center" class="tableField">
							{$s.excess_amount}
						</td>
						<td align="center" class="tableField">
							{$s.excess_amount_available_pay}
						</td>
						<td align="center" class="tableField">
							{$s.receiver|htmlall}
						</td>
					</tr>
				  {/foreach}
				</TBODY>
	   </table>
<div class="span-24 last pageNavPosition line" id="pageNavPosition"></div>

<div class="span-24 last center">
	<input type="button" name="btnSubmit" id="btnSubmit" value="Liquidar">
	<input type="hidden" name="selectedPayments" id="selectedPayments" title="Debe seleccionar almenos un pago.">
</div>
{else}
	 <div class="span-10 append-5 prepend-7 label" style="text-align: center;">No existen pagos en exceso para este comercializador.</div>
{/if}