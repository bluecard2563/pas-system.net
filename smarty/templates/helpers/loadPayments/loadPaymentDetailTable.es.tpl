{if $detailPaymentsList}
<br/>
<div class="span-24 last title">
	Lista de Pagos elegibles<br />
</div>

<table border="1" class="dataTable" id="paymentsTable">
	<THEAD>
		<tr class="header">
			<th align="center">
				<input type="checkbox" id="selAll" name="selAll"/>
			</th>
			<th align="center">
						Pago
			</th>
			<th align="center">
						Fecha del Pago
			</th>
			<th align="center">
						Factura
			</th>
			<th align="center">
						Tipo
			</th>
			<th align="center">
						Documento
			</th>
			<th align="center">
						Valor
			</th>
			<th align="center">
						Comercializador
			</th>
			<th align="center">
						Usuario
			</th>
		</tr>
	</THEAD>
	<TBODY>
		{foreach name="payments" from=$detailPaymentsList item="s"}
		<tr {if $smarty.foreach.payments.iteration is even}class="odd"{else}class="even"{/if} >
			<td align="center" class="tableField">
				<input type="checkbox" value="{$s.number_pyf}" id="chkPayment" name="chkPayment" class="paymentsChk"/>
			</td>
			<td align="center" class="tableField">
				{$s.number_pyf}
			</td>
			<td align="center" class="tableField">
				{$s.date_pay}
			</td>
			<td align="center" class="tableField">
				{$s.invoice_group}
			</td>
			<td align="center" class="tableField">
				{$global_paymentForms[$s.type_pyf]}
			</td>
			<td align="center" class="tableField">
			    {$s.comments_pyf|htmlall} {if $s.document_number_pyf} / {$s.document_number_pyf} {/if}
			</td>
			<td align="center" class="tableField">
				{$s.amount_pyf|string_format:"%.2f"}
			</td>
			<td align="center" class="tableField">
				{$s.name_dea}
			</td>
			<td align="center" class="tableField">
				{$s.name_usr}
			</td>
		</tr>
		{/foreach}
	</TBODY>
</table>
<div class="span-24 last center line">
	<b>TOTAL A RECIBIR: </b>: <label class="subTitle">{$total_to_verify}</label>
</div>
<div class="span-24 last pageNavPosition line" id="pageNavPosition"></div>

<div class="span-24 last center">
	<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
	<input type="button" value="Registrar" id="submitBtn" name="submitBtn"/>
	<input type="hidden" name="hdnTotalItems" id="hdnTotalItems" value="{$total_pages}" />
</div>
{else}
<div class="span-10 append-5 prepend-7 label" style="text-align: center;">No existen pagos para este representante.</div>
{/if}