    {if $dealerList}
    <div class="prepend-3 span-4 label">Comercializador:</div>
    	<div class="span-5">
        	<select name="selDealer" id="selDealer">
                  <option value="">- Seleccione -</option>
                  {foreach name="dealers" from=$dealerList item="l"}
                        <option value="{$l.serial_dea}" {if $smarty.foreach.dealers.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
                  {/foreach}
                </select>
        </div>
  <div class="span-3 label">Sucursal de Comercializador:</div>
  <div class="span-5 append-3" id="branchContainer">
        	<select name="selBranch" id="selBranch">
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    {else}
      <div class="span-12 append-6 prepend-6 label">No existen Comercializadores que cumplan con los criterios seleccionados.</div>
    {/if}