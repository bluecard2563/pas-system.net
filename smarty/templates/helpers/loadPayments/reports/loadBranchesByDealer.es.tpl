{if $branchList}
    <select name="selBranch" id="selBranch" >
        <option value="">- Seleccione -</option>
        {foreach from=$branchList item="b"}
		<option value="{$b.serial_dea}" >{$b.name_dea} - {$b.code_dea}</option>
        {/foreach}
    </select>
{else}
    <select name="selBranch" id="selBranch" >
      <option value="">- Seleccione -</option>
    </select>
{/if}