{if $userList}
    <select name="selResponsable" id="selResponsable" title='El campo "Responsable" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach from=$userList item="u"}
		<option value="{$u.serial_usr}" >{$u.first_name_usr} {$u.last_name_usr}</option>
        {/foreach}
    </select>
{else}
    <select name="selResponsable" id="selResponsable" title='El campo "Responsable" es obligatorio' >
      <option value="">- Seleccione -</option>
    </select>
{/if}