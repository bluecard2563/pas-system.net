{if $userList}
    <select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="users" from=$userList item="l"}
        	<option value="{$l.serial_usr}" {if $smarty.foreach.users.total eq 1}selected{/if}>{$l.name_usr|htmlall}</option>
        {/foreach}
    </select>
{else}
    {if $serial_mbc neq '' and $serial_dea neq ''}
		No existen usuarios disponibles para el representante seleccionado.
    {/if}
    <select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio' {if $serial_mbc neq '' and $serial_dea neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}