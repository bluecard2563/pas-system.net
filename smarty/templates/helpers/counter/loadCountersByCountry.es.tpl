{if $counterList}
<center>
    <div class="span-14 last line">
        <table border="0" class="paginate-1 max-pages-7" id="benefits_upload_table">
            <thead>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="l"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        {$l}
                    </td>
                    {/foreach}
                </tr>
            </thead>

            <tbody>
            {foreach name="counterList" from=$counterList item="l"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.counterList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$smarty.foreach.counterList.iteration}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.cnt_name}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$global_status[$l.status_cnt]}
                </td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{if $l.status_cnt eq 'ACTIVE'}
					<a style="cursor: pointer;" id="changeStatus{$l.serial_usr}" value="INACTIVE" serial_usr="{$l.serial_usr}">Desactivar</a>
					{else}
					<a style="cursor: pointer;" id="changeStatus{$l.serial_usr}" value="ACTIVE" serial_usr="{$l.serial_usr}">Activar</a>
					{/if}
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
     <div class="span-14 last line pageNavPosition" id="pageNavPosition"></div>
</center>
{else}
	{if $serial_dea neq ""}
	<center>No existen counters telef&oacute;nicos asignados a este pa&iacute;s.</center>
	{/if}
{/if}