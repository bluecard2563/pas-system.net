{if $comissionistList}
	<div class="span-8 last" id="responsableContainer">
        <select name="selResp" id="selResp" title='El campo "Responsable" es obligatorio.'>
          <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
          {if $data.serial_usr}
	            {foreach from=$comissionistList item="l"}
	                <option value="{$l.serial_usr}" {if $l.serial_usr eq $data.serial_usr}selected{/if}>{$l.first_name_usr|escape:"htmlall"} {$l.last_name_usr|escape:"htmlall"}</option>
	            {/foreach}
			{else}
				{foreach from=$comissionistList item="l"}
	                <option value="{$l.serial_usr}" >{$l.first_name_usr|escape:"htmlall"} {$l.last_name_usr|escape:"htmlall"}</option>
	            {/foreach}
			{/if}
        </select>
    </div>
</div>
{else}
    {if $serial_mbc neq ''}
        <div class="span-8 last">
            No existen Usuarios del Representante seleccionado.
        </div>
    {/if}
{/if}