{if $product_list}
<select name="{$id_name_value}{if $multiple eq 'YES'}[]{/if}" id="{$id_name_value}" title="El campo 'Producto' es obligatorio" {$multiple_choice}>
	{if $multiple eq 'NO'}<option value="">- Seleccione -</option>{/if}
	{foreach from=$product_list item="p"}
		<option value="{$p.serial_pxc}">{$p.name_pbl|htmlall}</option>
	{/foreach}
</select>
{else}
<label>No existen Productos asignados.</label>
{/if}