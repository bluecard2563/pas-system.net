{if $promo_list}
<form id="frmUpdatePromo" name="frmUpdatePromo" method="post" action="{$document_root}modules/clientPromo/fUpdatePromo" >
	<div class="span-24 last line">
		<table border="0" id="promoTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						&nbsp;
					</td>
					<td align="center" class="tableTitle">
						Nombre
					</td>
					<td align="center" class="tableTitle">
						Fecha Inicio
					</td>
					<td align="center"  class="tableTitle">
						Fecha Fin
					</td>
					<td align="center" class="tableTitle">
						Tipo
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="promo" from=$promo_list item="s"}
				<tr {if $smarty.foreach.promo.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						<input type="radio" name="rdClientPromo" id="rdClientPromo" value="{$s.serial_ccp}" />
					</td>
					<td align="center" class="tableField">
						{$s.name_ccp|htmlall}
					</td>
					<td align="center" class="tableField">
						{$s.begin_date_ccp}
					</td>
					<td align="center" class="tableField">
						{$s.end_date_ccp}
					</td>
					<td align="center" class="tableField">
						{$global_customerPromoTypes[$s.type_ccp]}
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
	
	<div class="span-24 last pageNavPosition line" id="alertPageNavPosition"></div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Continuar" />
		<input type="hidden" name="hdnTotalPages" id="hdnTotalPages" value="{$pages_number}" />
	</div>
</form>
{else}
<div class="prepend-7 span-10 append-7">
	<div class="span-10 error">
		No existen promociones con de acuerdo a los par&aacute;metros seleccionados.
	</div>
</div>
{/if}