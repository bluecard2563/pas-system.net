        <div class="span-19 last line" id="generalConditionsTable">
            {foreach from=$generalConditions item="generalCondition" key="key"}
            <div class="span-19 last line" >
                <div class="prepend-2 span-1">
                    <input type="radio" id="gnc-{$generalCondition.serial_gcn}" name="generalCondition" value="{$generalCondition.serial_gcn}"{if $conditionsByType.0.serial_gcn eq $generalCondition.serial_gcn }  checked="checked"{/if} />
                </div>
                <div class="span-7">
                    {$generalCondition.name_gcn}
                </div>
                <div class="span-1">
                 <a href="{$document_root}pdfs/generalConditions/{$generalCondition.url_gcn}" target="_blank"><img src="{$document_root}img/ico-pdf.gif" border="0" /></a>
                </div>

            </div>
            {/foreach}
        </div>
