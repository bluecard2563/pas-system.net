{if $dealerList}
<select name="selDealer" id="selDealer" title='El campo "Sucursal de Comercializador" es obligatorio'>
  {if $report}
  <option value="">- Todos -</option>
  {else}
  <option value="">- Seleccione -</option>
  {/if}
  {foreach from=$dealerList item="l"}
  <option value="{$l.serial_dea}" >{$l.name_dea}</option>
  {/foreach}
</select>
{else}
{if $serial_usr neq ''}
        No existen comercializadores registrados en el comercializador seleccionado.
{/if}
{/if}