<div class="span-24 last title">
  Log actualización Comercializador<br />
</div>
{*<div class="span-24 append-2 last line buttons">*}
{*<button id="refresh-table">Actualizar estados</button>*}
{*</div>*}

<div class="span-24 last line">
  <table border="1" class="dataTable" id="theTable">
    <THEAD>
    <tr bgcolor="#284787">

      <td align="center"
          style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
        Id
      </td>
      <td align="center"
          style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
        Usuario Actualizador
      </td>
        {*<td align="center"*}
        {*style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">*}
        {*Datos Originales*}
        {*</td>*}
      <td align="center"
          style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
        Datos Modificados
      </td>
      <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
        Fecha de modificación
      </td>
    </tr>
    </THEAD>
    <TBODY>
    {foreach name="logInfo" from=$dealer_log item="li"}
      <tr style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9" bgcolor="#e8f2fb">
        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
            {$li.serial_user}
          <input type="hidden" name="serial_cus[]" value="AS"/>
        </td>
        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
            {$li.usuario_actualizador}
        </td>
          {*<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">*}
          {*{$li.datos_previos}*}
          {*</td>*}
        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
            {foreach from=$li.datos_actualizados|unserialize item="ls" key="key"}
              <strong>{$key} :</strong> {$ls}
              <br>
            {/foreach}
        </td>
        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
            {$li.fecha_actualizacion}
        </td>
      </tr>
    {/foreach}
    </TBODY>
  </table>
</div>