{if $auxList}
	{if $all}
		<div class="prepend-4 span-16 append-4 last line">
			<table border="0" id="tblPromos" style="color: #000000;">
				<THEAD>
					<tr bgcolor="#284787">
						<td align="center" class="tableTitle">
							No.
						</td>
						<td align="center" class="tableTitle">
							Nombre
						</td>
						<td align="center" class="tableTitle">
							Ver
						</td>
						<td align="center" class="tableTitle">
							PDF
						</td>
						<td align="center" class="tableTitle">
							Excel
						</td>
					</tr>
				</THEAD>
				<TBODY>
					{foreach name="promo" from=$auxList item="a"}
					<tr {if $smarty.foreach.promo.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
						<td align="center" class="tableField">
							{$smarty.foreach.promo.iteration}
						</td>
						<td align="center" class="tableField">
							{$a.name_prm|htmlall}
						</td>
						<td align="center" class="tableField">
							<a href="{$document_root}modules/promo/fViewPromo/{$a.serial_prm}">Revisar</a>
						</td>
						<td align="center" class="tableField">
							<a href="{$document_root}modules/reports/promo/pViewPromoPDF/{$a.serial_prm}" target="blank"><img src="{$document_root}img/file_acrobat.gif" border="0" id="imgSubmitPDF"></a>
						</td>
						<td align="center" class="tableField">
							<a href="{$document_root}modules/reports/promo/pViewPromoExcel/{$a.serial_prm}" target="blank"><img src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS"></a>
						</td>
					</tr>
					{/foreach}
				</TBODY>
			</table>
		</div>
		<div class="span-24 last buttons" id="pageNavPosition"></div>
		<input type="hidden" name="hdnPages" id="hdnPages" value="{$pages}" />
	{else}
		<div class="prepend-7 span-5 label">* Promoci&oacute;n: </div>
		<div class="span-4 append-6 last">
			<select name="selPromo" id="selPromo" title="El campo 'Promoci&oacute;n' es obligatorio">
				<option value="">- Seleccione -</option>
				{foreach name="promo" from=$auxList item="l"}
					<option value="{$l.serial_prm}"{if $smarty.foreach.promo.total eq 1}selected{/if}>{$l.name_prm}</option>
				{/foreach}
			</select>
		</div>
	{/if}
{else}
	<div class="span-24">
		<div class="prepend-7 span-10 append-7 label line">
			No existen promociones registradas al momento.
			<div class="prepend-7 span-10 append-7 label line" style="display: none">
				<select  name="selPromo" id="selPromo" title="El campo 'Promoci&oacute;n' es obligatorio">
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>
	</div>
{/if}