{if $serviceList}
<div class="span-14 append-5 prepend-5 last line">
	<table border="0" id="servicesTable" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					Seleccione
				</td>
				<td align="center" class="tableTitle">
					Nombre
				</td>
				<td align="center" class="tableTitle">
					Descripci&oacute;n
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="services" from=$serviceList item="s"}
			<tr {if $smarty.foreach.services.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					<input type="radio" name="rdService[]" id="rdService_{$s.serial_ser}" value="{$s.serial_ser}"/>
				</td>
				<td align="center" class="tableField">
					{$s.name_sbl|htmlall}
				</td>
				<td align="center" class="tableField">
					{$s.description_sbl|htmlall}
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last line buttons" id="pageNavPosition"></div>

<div class="span-24 last buttons line">
	<input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
</div>

<input type="hidden" name="hdnSblID" id="hdnSblID" value="" title="Seleccione al menos un servicio." />
{else}
<div class="span-10 append-7 prepend-7">
	<div class="span-10 error">
		No existen servicios traducidos en el idioma seleccionado.
	</div>
</div>
{/if}