<!--
    Created on : Apr 19, 2010, 3:24:18 PM
    Author     : H3Dur4k
    Description:
        interface to insert an alert.
 -->
{literal}
<script type="text/javascript">
	 var date = new Date();
	 var day =date.getDate();
	 var month=date.getMonth()+1;
	 if(month <10)
		 month='0'+month;
	 var year= date.getFullYear();
	 
	 var today = day+"/"+month+"/"+year;
	$(document).ready(function(){
		//setDefaultCalendar($("#txtAlertDate"),'+0d','+100y');
		$("#txtAlertDate").bind("keyup", function(e){
			$(this).val(formatDate($(this).val()));
		});

		$("#btnInsertAlert").bind("click", function(e){
			$('#success_message').css('display','none');
			$('#error_message').css('display','none');
		 });
		/*SECCION DE VALIDACIONES DEL FORMULARIO*/
		$("#frmRegisterAlert").validate({
			errorLabelContainer: "#alerts_alerts",
			wrapper: "li",
			onfocusout: false,
			onkeyup: false,
			rules: {
				txtAlertDate: {
					required: true,
					date:true
				},
				txtAlertMessage: {
					required: true
				}
			},
			messages: {
				txtAlertDate: {
					date: "Ingrese la fecha en el formato establecido, dd/mm/YYYY."
				}
			},
		submitHandler: function(form) {
			if(dayDiff($("#hdnTodayDate").val(),$('#txtAlertDate').val())>0){
				$.ajax({
					url: document_root+"rpc/alerts/insertAlert.rpc",
					type: "post",
					data: ({date : $('#txtAlertDate').val(),
							time:$('#selAlertHour').val()+':'+$('#selAlertMinute').val(),
							description:$('#txtAlertMessage').val(),
							table_name:$('#table').val(),
							remote_id:$('#remote').val(),
							user:$('#user').val(),
							type:$('#type').val(),
							file:$('#file').val()}),
					success: function(data) {
						if(data=='true'){
							$('#success_message').css('display','block');
							$('#error_message').css('display','none');
							form.reset();
						}else{
							$('#success_message').css('display','none');
							$('#error_message').css('display','block');
						}
					}
					});
		  }else{
			$('#alerts_alerts').html($('#alerts_alerts').html()+"<label for='txtAlertMessage' generated='true' class='error_validation' style='display: block;'>La Fecha debe ser mayor o igual a hoy.</label>");
			$('#alerts_alerts').css('display','block');
		  }
		}
	/*FIN DE LA SECCION DE VALIDACIONES*/
		});
	});
</script>
{/literal}
 <form action="" id="frmRegisterAlert" name="frmRegisterAlert" method="post">
	 <input type="hidden" id="hdnTodayDate" name="hdnTodayDate" value="{$today}"/>
	<div class="span-10">
		<div class="span-10 last title ">
				Registro de Alerta<br />
				<label>(*) Campos Obligatorios</label>
		</div>
		<div class="span-10 success" align="center" id="success_message" style="display:none;">
            	Alerta registrada exitosamente!
        </div>
		<div class="span-10 error" align="center" id="error_message" style="display:none;">
            	Hubo errores al registrar la alerta, vuelva a intentarlo
        </div>
		<div class="append-1 span-8 prepend-1 last">
        <ul id="alerts_alerts" class="alerts"></ul>
      </div>
		<input type="hidden" id="remote" name="remote" value="{$remote_id}"/>
		<input type="hidden" id="table" name="table" value="{$table_name}"/>
		<input type="hidden" id="user" name="user" value="{$serial_usr}"/>
		<input type="hidden" id="type" name="type" value="{$type}"/>
		<input type="hidden" id="file" name="file" value="{if $serial_fle}{$serial_fle}{else}0{/if}"/>
		<div class="span-10 last line">
			<div class="span-3">
				<label>*Fecha:</label>
			</div>
			<div class="span-6 last">
				<input type="text" id="txtAlertDate" name="txtAlertDate" title="El campo fecha es obligatorio."/>
			</div>
		</div>
		<div class="span-10 last line">
			<div class="span-3">
				<label>Hora:</label>
			</div>
			<div class="span-6">
				<select name="selAlertHour" id="selAlertHour">
				  {foreach from=$array_hours item="l"}
				  <option value="{$l}">{$l}</option>
				  {/foreach}
				</select>&nbsp;HH:
				<select name="selAlertMinute" id="selAlertMinute" title='El campo "Minutos" es obligatorio'>
				  {foreach from=$array_minutes item="l"}
				  <option value="{$l}">{$l}</option>
				  {/foreach}
				</select>&nbsp;mm
			</div>
		</div>
		<div class="span-10 last line">
			<div class="span-3">
				<label>*Descripci&oacute;n:</label>
			</div>
			<div class="span-6 last">
				<textarea class="span-7" id="txtAlertMessage" name="txtAlertMessage" title="El campo Descripci&oacute;n es obligatorio."></textarea>
			</div>
		</div>
		<div class="span-10 last buttons line">
			<input type="submit" name="btnInsertAlert" id="btnInsertAlert" value="Registrar" >
		</div>
	</div>
</form>
 