{if $table}
<div class="span-19 last line">
	{$table}
     <div class="span-19 last pageNavPosition" id="pageNavPosition"></div>
</div>
{literal}
<script type="text/javascript">
        /*@object: Pager
         *@Description: JS library that paginates a table.
         *@Params: IDTable, Max Items per Page, Label for 'Next', Label for 'Prev'
         **/
        var pager = new Pager('theTable', 10 , 'Siguiente', 'Anterior');
        pager.init(25); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
</script>
{/literal}
{/if}
{if $text}
<div class="span-19 buttons last line">
	<input type="button" id="show" name="show" value="{$text}" />
</div>
{/if}

{literal}
<script type="text/javascript">
	$("img.image").bind("click", function(e){
		loadFormData($(this).attr("id"));
		$('select.select').hide();
		$('#dialog').dialog('open');
	});

    $("img.delete").bind("click", function(e){
		deleteItem($(this).attr("id"));
	});
	
	$('#show').bind("click",function(e){
		
                $("#newInfo").css("display","inline");
        //$("#frmUpdateProductTranslation").validate({
	//	errorLabelContainer: "#alerts",
	//	wrapper: "li",
	//	onfocusout: false,
	//	onkeyup: false,
	//	rules: {
	//		rdStatus: {
	//			required: true
        //                }
	//	}
        //    });
        //   if($("#frmUpdateProductTranslation").valid()){
        //     $("#frmUpdateProductTranslation").submit();
        //    }
	});
</script>
{/literal}