{literal}
<script type= "text/javascript">/*<![CDATA[*/
$(document).ready(function(){

	/* example 1 */
	var button = $('#button1'), interval;
	new AjaxUpload(button,{
		//action: 'upload-test.php', // I disabled uploads in this example for security reasons
		action: 'upload.htm',
		name: 'myfile',
		onSubmit : function(file, ext){
			// change button text, when user selects file
			button.text('Uploading');

			// If you want to allow uploading only 1 file at time,
			// you can disable upload button
			this.disable();

			// Uploding -> Uploading. -> Uploading...
			interval = window.setInterval(function(){
				var text = button.text();
				if (text.length < 13){
					button.text(text + '.');
				} else {
					button.text('Uploading');
				}
			}, 200);
		},
		onComplete: function(file, response){
			button.text('Upload');

			window.clearInterval(interval);

			// enable upload button
			this.enable();

			// add file to the list
			$('<li></li>').appendTo('#example1 .files').text(file);
		}
	});
});/*]]>*/</script>{/literal}

<div class="span-11 last line">
    <center><div id="upload_message">
        Seleccione el archivo que desea subir.
    </div></center><br />

    <div class="span-11 last line">
        <div class="prepend-2 span-3">
            <label>Nombre:</label>
        </div>
        <div class="span-4 append-1 last line">
            <input type="text" name="txtFileName" id="txtFileName"/>
        </div>
    </div>

    <div class="span-11 last line">
        <div class="prepend-2 span-3">
            <label>Monto:</label>
        </div>
        <div class="span-4 append-1 last line">
            <input type="text" name="txtFileAmount" id="txtFileAmount"/>
        </div>
    </div>
    
    <div class="span-11 last line">
        <div class="prepend-2 span-3">
            <label>Comentario:</label>
        </div>
        <div class="span-5 last line">
            <textarea style="width: 200px; height: 50px;" name="txtFileComment" id="txtFileComment"></textarea>
        </div>
    </div>

    <div class="span-11 last line">
        <div class="prepend-2 span-3">
            <label>Archivo:</label>
        </div>
        <div class="span-4 append-1 last line">
            <div id="button1" class="button">Upload</div>
        </div>
    </div>
</div>