{if $table}
<div class="prepend-1 span-22 append-1 last line">
	{$table}
    <div class="span-22 last pageNavPosition" id="pageNavPosition"></div>
</div>
{literal}
<script type="text/javascript">
        /*@object: Pager
         *@Description: JS library that paginates a table.
         *@Params: IDTable, Max Items per Page, Label for 'Next', Label for 'Prev'
         **/
        var pager = new Pager('theTable', 10 , 'Siguiente', 'Anterior');
        pager.init(50); //Max Pages
        pager.showPageNav('pager', 'pageNavPosition'); //Div where the navigation buttons will be placed.
        pager.showPage(1); //Starting page
</script>
{/literal}
{/if}
<div class="span-24 last line buttons">
	<input type="submit" id="show" name="show" value="{$text}" />
</div>

{literal}
<script type="text/javascript">

$("#checkAll").bind("change", function(e){
		if($(this).is(":checked")){
			$('[name^="{/literal}{$serial}{literal}"]').attr("checked",true);
		}
		else{
			$('[name^="{/literal}{$serial}{literal}"]').attr("checked",false);
		}
	});
	/*$("img.image").bind("click", function(e){
		loadFormData($(this).attr("id"));
		$('select.select').hide();
		$('#dialog').dialog('open');
	});

        $("img.delete").bind("click", function(e){
		deleteItem($(this).attr("id"));
	});
	
	$('#show').bind("click",function(e){
		//$("#newInfo").css("display","inline");
		
	});*/
</script>
{/literal}