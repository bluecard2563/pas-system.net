{if $productList}
    <select id="selProduct" name="selProduct"  title="Por favor seleccione un producto." >
        <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
            {foreach name="products" from="$productList" item="p"}
            <option value="{$p.serial_pro}" {if $smarty.foreach.products.total eq 1}selected="selected"{/if}>
                {$p.name_pbl}
            </option>
            {/foreach}
    </select>
{else}
    No existen productos asignados al pa&iacute;s.
    <select id="selProduct" name="selProduct"  title="Por favor seleccione un producto." style="display: none;" >
       <option value="" selected>- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
    </select>
{/if}