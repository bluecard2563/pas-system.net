{if $promos_list}
	<select name="selCustomerPromo" id="selCustomerPromo" title="El campo 'Promoci&oacute;n' es obligatorio">
		<option value="">- Seleccione -</option>
		{foreach from=$promos_list item="c"}
			<option value="{$c.serial_ccp}" {if $promos_list|@count eq 1}selected{/if}>{$c.name_ccp|htmlall}</option>
		{/foreach}
	</select>
{else}
	<i>No existen promociones en el pa&iacute;s seleccionado.</i>
	<input type="hidden" name="selCustomerPromo" id="selCustomerPromo" title="El campo 'Promoci&oacute;n' es obligatorio" value="" />
{/if}