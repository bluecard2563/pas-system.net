{if $countryList }
   {if $aux eq 'MANAGER'}
        <div class="span-24 last line" >
                <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
                    <div class="span-5">
                            <select name="selCountryEx" id="selCountryEx" title='El campo "Pa&iacute;s" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$countryList item="l"}
                                <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou|htmlall}</option>
                            {/foreach}
                        </select>
                    </div>

                <div class="span-3 label">* Representante:</div>
                <div class="span-4 append-4 last" id="managerContainer">
                        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio'>
                            <option value="">- Seleccione -</option>
                    </select>
                 </div>
                </div>
   {/if}
   {if $aux eq 'DEALER'}
            <div class="span-24 last line" >
                <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
                    <div class="span-5">
                            <select name="selCountryEx" id="selCountryEx" title='El campo "Pa&iacute;s" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$countryList item="l"}
                                <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou|htmlall}</option>
                            {/foreach}
                        </select>
                    </div>

                <div class="span-3 label">* Comercializador:</div>
                <div class="span-4 append-4 last" id="dealerContainer">
                        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
                            <option value="">- Seleccione -</option>
                    </select>
                 </div>
                </div>

            <div class="span-24 last line" id="Dealer">
                <div class="prepend-4 span-4 label">* Sucursal de Comercializador:</div>
                    <div class="span-5" id="branchContainer">
                             <select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
                            <option>- Seleccione -</option>
                             </select>
                    </div>
        </div>
   {/if}
{/if}