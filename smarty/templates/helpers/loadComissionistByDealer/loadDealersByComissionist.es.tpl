{if $dealer_list}
	<div class="prepend-4 span-16 last line">
		<table border="1" class="dataTable" id="dealer_table">
			<thead>
				<tr class="header">
					<th style="text-align: center;">
						<input type="checkbox" id="selAll" name="selAll"/>
					</th>
					<th style="text-align: center;">
						Comercializador
					</th>
					<th style="text-align: center;">
						Sucursales Asociadas
					</th>				
				</tr>
			</thead>

			<tbody>
			{foreach name="dealer" from=$dealer_list item="cl"}
			<tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.dealer.iteration is even} class="odd"{else}class="even"{/if}>
				<td class="tableField">
					<input type="checkbox" name="chkClaims[]" id="chkClaims{$cl.serial_dea}" value="{$cl.serial_dea}" class="bonusChk" />
				</td>
				<td class="tableField">
					{$cl.name_dea|htmlall}
				</td>
				<td class="tableField">
					{$cl.branch_count}
				</td>
			</tr>
			{/foreach}
			</tbody>
		</table>
	</div>	

	<div class="span-24 last buttons line">
		<input type="button" name="btnInsert" id="btnInsert" value="Reasginar" />
		<input type="hidden" name="hdnDealersTo" id="hdnDealersTo" value="" title="Seleccione al menos un Comercializador" />
	</div>
{else}
	<div class="prepend-6 span-12 append-6">
		<div class="span-12 center error">
			El responsable no tiene comercializadores asignados.
		</div>
	</div>
{/if}