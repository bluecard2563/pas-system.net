{if $counterList neq''}
<div class="span-5 last">
		<select name="selCounter" id="selCounter" title='El campo "Seleccione un counter" es obligatorio'>
			<option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
			{foreach from=$counterList key=key item="l" name="counter"}
				<option value="{$l.serial_cnt}" >{$l.cnt_name|htmlall}</option>
			{/foreach}
		</select>
</div>
{else}
<div class="span-5 last">
		<select name="selCounter" id="selCounter" title='El campo "Seleccione un counter" es obligatorio'>
			<option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
		</select>
</div>
{/if}