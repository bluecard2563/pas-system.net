{if $profiles}
	<select id="selProfile" name="selProfile" title='El campo "Perfil" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach name=profiles from="$profiles" item="p"}
               		<option value="{$p.serial_pbc}"{if $smarty.foreach.profiles.total eq 1}selected{/if}>{$p.name_pbc|htmlall}</option>
               	{/foreach}
     </select>
     {else}
        {if $serial_cou neq ''}
            No existen perfiles registrados.
        {/if}
		{if $counter}
			No Existen Perfiles de Counter en ese pa&iacute;s.
			<select name="selProfile" id="selProfile" title='El campo "Perfil" es obligatorio' style="display:none;">
				<option value="">- Seleccione -</option>
			</select>
		{else}
			<select name="selProfile" id="selProfile" title='El campo "Perfil" es obligatorio' {if $serial_cou neq ''}style="display:none;"{/if}>
			  <option value="">- Seleccione -</option>
			</select>
		{/if}
{/if}