{if $dataByCountry}
<select name="selCreditD" id="selCreditD" title='El campo "D&iacute;a de Cr&eacute;dito" es obligatorio.'>
  <option value="">- Seleccione -</option>
  {foreach from=$dataByCountry item="l"}
  	<option value="{$l.serial}" >{$l.days_cdd}</option>
  {/foreach}
</select>
{else}
	{if $serial_cou neq ''}
	No existen d&iacute;as de cr&eacute;dito registradas para este pa&iacute;s.
    {/if}
    <select name="selCreditD" id="selCreditD" title='El campo "D&iacute;a de Cr&eacute;dito" es obligatorio.' {if $serial_cou neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}