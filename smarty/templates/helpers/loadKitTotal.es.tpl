<div class="span-24 last line center">
	<br />
	<li>Kits que posee actualmente el comercializador:&nbsp;{if $kitsTotal}{$kitsTotal}{else}0{/if}</li>
    <input type="hidden" id="hdnKitsTotal" name="hdnKitsTotal" value="{if $kitsTotal}{$kitsTotal}{else}0{/if}" />
</div>

{if $kitByDealerID}
 <div class="span-24 last line line">
         <div class="prepend-3 span-4 label">Consultar desde:</div>
    	<div class="span-5">
            <input type="text" id="txtFromDate" name="txtFromDate" />
        </div>

         <div class="span-3 label">Consultar hasta:</div>
        <div class="span-5">
            <input type="text" id="txtToDate" name="txtToDate" />
        </div>
   </div>
    <input type="hidden" id="hdnKitByDealerID" name="hdnKitByDealerID" value="{$kitByDealerID}" />
    <div class="span-24 last line"></div>
    <div class="span-24 last buttons line">
        <input type="button" name="btnDetails" id="btnDetails" value="Consultar Detalles" >
    </div>
{else}
	<div class="span-24 last line center">
		No se ha realizado ninguna entrega ni devoluci&oacute;n de kits para este comercializador.
	</div>

	<div class="span-24 last buttons line">
		<input type="submit" name="btnAssingKits" id="btnAssingKits" value="Ir a Asignaci&oacute;n de Kits" >
	</div>
{/if}
