{if $documentList}
        <select name="selDocument" id="selDocument" title='El campo "Tipo de documento" es obligatorio'>
			<option value="">- Seleccione -</option>
			{foreach name="documents" from=$documentList item="l" }
			<option value="{$l.type_dbm}" {if $smarty.foreach.documents.total eq 1}selected{/if}>{$global_typeManager[$l.type_dbm]}</option>
  		{/foreach}
        </select>
{else}
    {if $serial_cou neq ''}
	No existen documentos registrados para este pa&iacute;s.
    {/if}
    <select name="selDocument" id="selDocument" title='El campo "Tipo de documento" es obligatorio.'>
      <option value="">- Seleccione -</option>
    </select>
{/if}