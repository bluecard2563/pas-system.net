{if $sectorList}
<select name="selSector{$serial_sel}" id="selSector{$serial_sel}" title='El campo "Sector" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach name="sectors" from=$sectorList item="l"}
  <option value="{$l.serial_sec}" {if $smarty.foreach.sectors.total eq 1}selected{/if}>{$l.name_sec}</option>
  {/foreach}
</select>
{else}
	{if $serial_cit neq ''}
		No existen sectores registrados.
    {/if}
    <select name="selSector{$serial_sel}" id="selSector{$serial_sel}" title='El campo "Sector" es obligatorio' {if $serial_cit neq ''}style="display:none"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}