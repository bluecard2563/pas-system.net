{if $data}
<div class="span-21 last line separator">
    <label>Informaci&oacute;n del cliente:</label>
</div>
<center>
    <div class="span-21 last line">
        <table border="0" class="paginate-1 max-pages-7" id="theTable">
            <THEAD>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="t"}
                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        {$t}
                    </td>
                    {/foreach}
                </tr>
            </THEAD>
            <TBODY>

            <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e5e5e5" >
                 <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.name_cou}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.name_cit}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.document_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.first_name_cus} {$data.last_name_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.birthdate_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.phone1_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.cellphone_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.email_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    {$data.address_cus}
                </td>
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                    <a id="viewSales" style="cursor: pointer;">Ver tarjetas</a>
                </td>
            </tr>

            </TBODY>
        </table>
    </div>
</center>


<div class="span-21 last line" id="salesContainer" style="display: none;">
    <div class="span-21 last line separator">
        <label>Informaci&oacute;n de la(s) tarjeta(s):</label>
    </div>
    {if $sales}
    <center>
        <div class="span-21 last line">
            <table border="0" class="paginate-1 max-pages-7" id="theTable">
                <THEAD>
                    <tr bgcolor="#00b3f0">
                        {foreach from=$titles_sal item="t"}
                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">
                            <font color=#000000>
                            {$t}
                            </font>
                        </td>
                        {/foreach}
                    </tr>
                </THEAD>
                <TBODY>

                {foreach name="sales" from=$sales item="s"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.sales.iteration is even} bgcolor="#c5dee7" {else} bgcolor="#e5e5e5" {/if}>
                     <td id="cardNumber{$s.card_number_sal}" style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; color: red; cursor: pointer">
                        {$s.card_number_sal}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$s.emission_date_sal}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$s.begin_date_sal}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                        {$s.end_date_sal}
                    </td>
                    <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; {if $s.status_sal eq 'STANDBY'} color: #CC0033;{/if}">
                        {$global_salesStatus[$s.status_sal]}
                    </td>
                </tr>
                {/foreach}

                </TBODY>
            </table>
        </div>
    </center>
    {else}
        <center>El cliente seleccionado no tiene ninguna tarjeta activa asignada.</center>
    {/if}
</div>


{else}
    {if $document_cus}<center>No existe un cliente con el documento ingresado.</center>{/if}
{/if}

{literal}
<script type="text/javascript">
    $("#viewSales").toggle(
        function(){
            $(this).html("Ocultar tarjetas");
            $("#salesContainer").slideToggle("fast");
        },function(){
             $(this).html("Ver tarjetas");
            $("#salesContainer").slideToggle("fast");
    });

    $("td[id^='cardNumber']").bind("click",function(){
        $("#txtCard").val(trim($(this).html()));
        $("#customerInfoContainer").html("");
        $("#txtCustomerID").val("");
        $("#txtCustomerName").val("");
    });
    
    function trim (myString) {
        return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
    }
</script>
{/literal}