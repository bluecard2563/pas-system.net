{if $noData neq 1}
    {if $dealerList}
    <select name="selDealer" id="selDealer" title='El campo "Comercializadora" es obligatorio'>
      <option value="">- Seleccione -</option>
      {foreach from=$dealerList item="l"}
      <option value="{$l.serial_dea}" >{$l.name_dea|htmlall}</option>
      {/foreach}
    </select>
    {else}
            {if $serial_sec neq ''}
                    No existen comercializadores registrados en el sector seleccionado.
        {/if}
        <select name="selDealer" id="selDealer" title='El campo "Comercializadora" es obligatorio' {if $serial_dea neq ''}style="display:none"{/if}>
          <option value="">- Seleccione -</option>
        </select>
    {/if}
{else}
    <select name="selDealer" id="selDealer" title='El campo "Comercializadora" es obligatorio'>
      <option value="">- Seleccione -</option>
      {/foreach}
    </select>
{/if}