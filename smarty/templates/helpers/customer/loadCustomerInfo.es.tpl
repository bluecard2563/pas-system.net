<div class="span-24 last line line">

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n del Cliente:</label>
    </div>

	<!--div class="append-8 span-10 prepend-6 last ">
		<div id="alerts" class="span-10" align="center"></div>
	</div-->

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">Nombre:</div>
		<div class="span-5">
			{$data.first_name_cus|htmlall} {$data.last_name_cus|htmlall}
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">Documento:</div>
		<div class="span-5">
			{$data.document_cus}
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Nuevo Documento</div>
		<div class="span-5">
			<input type="text" id="txtNewDocument" name="txtNewDocument" title="El campo 'Nuevo Documento' es obligatorio." />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Confirmar Nuevo Documento:</div>
		<div class="span-5">
			<input type="text" id="txtNewDocumentConf" name="txtNewDocumentConf" title="El campo 'Confirmar Nuevo Documento' es obligatorio." />
		</div>
	</div>

	<div class="span-24 last buttons line">
        <input type="submit" name="btnUpdate" id="btnUpdate" value="Guardar Cambios" />
    </div>
</div>