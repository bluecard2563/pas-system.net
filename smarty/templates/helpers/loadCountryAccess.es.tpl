 {if $cityAccessList}
   <hr />
    <div class="span-24 last line title">
        Pa&iacute;ses de Acceso:
    </div>
    <div class="prepend-3 span-17 append-4 last" align="center">
           Al seleccionar un pa&iacute;s, el usuario podr&aacute; ingresar a todas las ciudades y comercializadores de ese pa&iacute;s.
    </div>
    <div class="span-24 line">
        {generate_country_access_list title1='countries' title2='cities' data=$countryAccessList sel=$cityAccessList javaS='checkAllCountries();'  javaS2='checkAllCities(this.value);' nameChAll='chkAllCountries' nameChAll2='chkAllCities' chkAll=1}
    </div>
  <hr />
 {else}
 	 {if $userID neq 'NULL'}
		 <hr />
			 <div class="span-24 last line title">
				Pa&iacute;ses de Acceso:
			</div>
			<div class="prepend-3 span-17 append-4 last" align="center">
				   Al seleccionar un pa&iacute;s, el usuario podr&aacute; ingresar a todas las ciudades y comercializadores de ese pa&iacute;s.
			</div>
			<div class="span-24 line">
				{generate_country_access_list title1='countries' title2='cities' data=$countryAccessList sel=[] javaS='checkAllCountries();'  javaS2='checkAllCities(this.value);' nameChAll='chkAllCountries' nameChAll2='chkAllCities' chkAll=1}
			</div>
		 <hr />
	  {/if}
 {/if}