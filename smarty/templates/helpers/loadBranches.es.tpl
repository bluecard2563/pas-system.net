{if $branchList}
<select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio'>
  <option value="">{if $opt_text}- {$opt_text} -{else}- Seleccione -{/if}</option>
  {foreach from=$branchList item="l"}
  <option value="{$l.serial_dea}" >{$l.name_dea|htmlall} - {$l.code_dea}</option>
  {/foreach}
</select>
{else}
	{if $serial_dea neq ''}
		No existen sucursales registradas en el comercializador seleccionado.
    {/if}
    <select name="selDealer" id="selDealer" title='El campo "Sucursal" es obligatorio' {if $serial_dea neq ''}style="display:none"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}