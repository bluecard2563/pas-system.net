{if $dealersTree}
<div class="span-8 append-2 label line">* Seleccione las sucursales de comercializador que van a ser visitadas e indique el n&uacute;mero de visitas que se van a realizar:</div>
<div class="prepend-1 span-9 last line">
    <ul id="dealerTree" class="treeview-red">
        {foreach from=$dealersTree item="l"}
        <li><span>{$l.name_dea}</span>
            <ul>
                {if $l.branches}
                    {foreach from=$l.branches item="b"}
                    <li><input type="text" size="2" id="{$b.serial_dea}"> {$b.name_dea|htmlall}
                    {/foreach}
                {else}
                    <li>No existen sucursales de comercializador.</li>
                {/if}
            </ul>
            
        </li>
        {/foreach}
    </ul>
</div>
{else}
    {if $serial_user}
        <div class="prepend-1 span-9 last line">Al responsable seleccionado no se le ha asignado ningun comecializador.</div>
    {else}
        <div class="prepend-1 span-9 last line">Seleccione un responsable de la lista.</div>
    {/if}
{/if}

{literal}
<script language="javascript">
    $("#dealerTree").treeview({
        animated: "fast",
        //unique: true,
        //persist: "location",//(location,cache)
        collapsed: true
    });
</script>
{/literal}