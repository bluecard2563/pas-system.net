{if $comissionistList}
<div class="span-17 last line">
    <input type="radio" name="rdoSelComissionist" id="rdoSelComissionistMa" checked/>Usuarios del representante seleccionado:
    <select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio.'>
        <option value="">- Seleccione -</option>
		{if $data.serial_usr}
            {foreach from=$comissionistList item="l"}
                <option value="{$l.serial_usr}" {if $l.serial_usr eq $data.serial_usr}selected{/if}>{$l.first_name_usr|escape:"htmlall"} {$l.last_name_usr|escape:"htmlall"}</option>
            {/foreach}
		{else}
			{foreach from=$comissionistList item="l"}
                <option value="{$l.serial_usr}" >{$l.first_name_usr|escape:"htmlall"} {$l.last_name_usr|escape:"htmlall"}</option>
            {/foreach}
		{/if}
    </select>
</div>
{else}
    {if $serial_mbc neq ''}
        <div class="span-17 last line">
            No existen Usuarios del Representante seleccionado.
        </div>
        <div class="span-17 last line">
            <select name="selComissionist" id="selComissionist"style="display:none;">
              <option value="">- Seleccione -</option>
            </select>
        </div>
    {else}
        <div class="span-17 last line">
            Seleccione un representante.
        </div>
    {/if}
    
{/if}

{if $mainComissionistList}
	{if !$paUser}
    <div class="span-17 last line">
        <input type="radio" name="rdoSelComissionist" id="rdoSelComissionistPa"/>Usuarios {$global_system_name}:
        <select name="selComissionistPa" id="selComissionistPa" title='El campo "Responsable" es obligatorio.' disabled>
            <option value="">- Seleccione -</option>
			{if $data.serial_usr}
                {foreach from=$mainComissionistList item="l"}
                    <option value="{$l.serial_usr}" {if $l.serial_usr eq $data.serial_usr}selected{/if}>{$l.first_name_usr|escape:"htmlall"}  {$l.last_name_usr|escape:"htmlall"} </option>
                {/foreach}
			{else}
				{foreach from=$mainComissionistList item="l"}
                    <option value="{$l.serial_usr}" >{$l.first_name_usr|escape:"htmlall"}  {$l.last_name_usr|escape:"htmlall"} </option>
                {/foreach}
			{/if}
        </select>
    </div>
	{/if}
{else}
    <div class="span-17 last line">
        No existen Usuarios de {$global_system_name}.
    </div>
{/if}

{literal}
<script>
    $('#rdoSelComissionistMa').bind('click',function(){
                                                $('#selComissionistPa').attr('disabled', true);
                                                $('#selComissionist').attr('disabled', false);
                                             });
    $('#rdoSelComissionistPa').bind('click',function(){
                                                $('#selComissionist').attr('disabled', true);
                                                $('#selComissionistPa').attr('disabled', false);
                                             });
    $("#selComissionist").bind("change", function(e){
          $("#hdnSerialUsr").val($("#selComissionist").val());
    });
    $("#selComissionistPa").bind("change", function(e){
          $("#hdnSerialUsr").val($("#selComissionistPa").val());
    });
</script>
{/literal}