{if $small_container eq 'YES'}
    <div class="span-19 last line">
        <div class="prepend-1 span-4 label">* Documento:</div>
        <div class="span-5">
            <input type="text" name="txtCustomerDocument" id="txtCustomerDocument" title='El campo "Documento" es obligatorio.'value="{$document_cus}">
        </div>

        <div class="span-3 label">* Tipo de Documento:</div>
        <div class="span-6 last">
            <input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
            <input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.type_cus eq 'LEGAL_ENTITY'}checked{/if} /> RUC
            <input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
            <input type="hidden" name="selType" id="selType" value="{$customerData.type_cus}" />
        </div>
    </div>

    <div class="span-19 last line">
        <div class="prepend-1 span-4 label" id="labelName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}* Razo&oacute;n Social: {else}* Nombre:{/if}</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$customerData.firstname_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

        <div id="lastNameContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$customerData.lastname_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
        </div>                
    </div>

    <div class="span-19 last line">
        <div class="prepend-1 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus|htmlall}">
        </div>

        <div class="span-3 label">* Tel&eacute;fono Principal:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}">
        </div>
    </div>

    <div class="span-19 last line">
        <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
        <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"  value="{$customerData.phone2_cus}"/></div>
    </div>

    <div class="span-19 last line">
        <div class="prepend-1 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5"> 
            <select name="selCountry" id="selCountry" class="span-5" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.serial_cou}selected{/if}>{$l.name_cou|htmlall}</option>
                {/foreach}
            </select> 
        </div>

        <div class="span-3 label">* Ciudad:</div>
        <div class="span-4 append-2 last" id="cityContainer"> 
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                {foreach from=$cityList item="l"}
                    <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit|htmlall}</option>
                {/foreach}
            </select> 
        </div>
    </div>

    <div class="span-19 last line center" id="newHolderActionContainer">
        <input type="button" name="btnChangeHolder" id="btnChangeHolder" value="Cambiar Titular de Factura" />
        <input type="hidden" name="hdnSerialCusRPC" id="hdnSerialCusRPC" value="{$customerData.serial_cus}" />
    </div>
{else}
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Documento:</div>
        <div class="span-5">
            <input type="text" name="txtCustomerDocument" id="txtCustomerDocument" title='El campo "Documento" es obligatorio.' value="{$document_cus}">
        </div>

        <div class="span-3 label">*Tipo de Cliente:</div>
        <div class="span-6 last">
            {if $document_validation eq 'YES'}
                <input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
                <input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.type_cus eq 'LEGAL_ENTITY'}checked{/if} /> RUC					
                <input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
                <input type="hidden" name="selType" id="selType" value="{if $customerData.type_cus}{$customerData.type_cus}{else}PERSON{/if}" />
            {else}
                {if $customerData}
                    {if $customerData.type_cus eq 'PERSON'}
                        Persona Natural
                    {elseif $customerData.type_cus eq 'LEGAL_ENTITY'}
                        Persona Jur&iacute;dica
                    {/if}
                    <input type="hidden" name="selType" id="selType" value="{$customerData.type_cus}"/>
                {else}
                    <select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$typesList item="l"}
                            <option value="{$l}" {if $l eq $customerData.type_cus}selected{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                        {/foreach}
                    </select>
                {/if}
            {/if}
        </div>
    </div>
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label" id="labelName">*Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$customerData.firstname_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

        <div id="lastNameContainer">
            <div class="span-3 label">*Apellido:</div>
            <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$customerData.lastname_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus|htmlall}" {if $customerData}readonly="readonly"{/if}>
        </div>

        <div class="span-3 label">*Tel&eacute;fono Principal:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}" {if $customerData}readonly="readonly"{/if}>
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
        <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"  value="{$customerData.phone2_cus}" {if $customerData}readonly="readonly"{/if}/></div>
        
        <div class="span-3 label">Correo Electr&oacute;nico:</div>
        <div class="span-4 append-2 last">
            <input type="text" name="txtEmail" id="txtEmail" title='El campo "Correo Electr&oacute;nico" es obligatorio' value="{$customerData.email_cus}" {if $customerData}readonly="readonly"{/if} />
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
        <div class="span-5"> 
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.serial_cou}selected{/if}>{$l.name_cou|htmlall}</option>
                {/foreach}
            </select> 
        </div>

        <div class="span-3 label">*Ciudad:</div>
        <div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$cityList item="l"}
                    <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit|htmlall}</option>
                {/foreach}
            </select> </div>
    </div>

    <input type="hidden" name="hdnMaxSerialRPC" id="hdnMaxSerialRPC" value="{$maxSerial}" />
    <input type="hidden" name="hdnTypeDbcRPC" id="hdnTypeDbcRCP" value="{if $customerData.type_cus eq 'PERSON'}PERSON{else}LEGAL_ENTITY{/if}" />
    <input type="hidden" name="hdnSerialCusRPC" id="hdnSerialCusRPC" value="{$customerData.serial_cus}" />
    <input type="hidden" name="hdnCustomerActionRPC" id="hdnCustomerActionRPC" value="{if $customerData}UPDATE{else}INSERT{/if}" />
{/if}
