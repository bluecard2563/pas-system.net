{if $auxData}
	<div class="span-24 last line">
		<table id="salesTable">
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">Cliente</td>
				<td align="center" class="tableTitle">Comercializador</td>
				<td align="center" class="tableTitle">Producto</td>
				<td align="center" class="tableTitle">Monto de la Venta</td>
			</tr>
			<tr bgcolor="#d7e8f9">
				<td align="center" class="tableField">{$auxData.customer|htmlall}</td>
				<td align="center" class="tableField">{$auxData.dealer|htmlall}</td>
				<td align="center" class="tableField">{$auxData.product|htmlall}</td>
				<td align="center" class="tableField">$ {$sale.total_sal}</td>
			</tr>
		</table>
		<input type="hidden" id="hdnTotal" name="hdnTotal" value="{$sale.total_sal}" />
	</div>

	<div class="span-24 last line">
		<div class="prepend-8 span-4 label" id="labelName">* Penalidad ($):</div>
		<div class="span-5 append-7 last">
			<input type="text" readonly name="txtPenalty" id="txtPenalty" title="El campo 'Penalidad' es obligatorio." />
		</div>
		<input type="hidden" name="hdnCardNumber" id="hdnCardNumber" value="{$number_card}" />
	</div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar" />
	</div>
{else}
	<div class="prepend-7 span-10 append-7 last line">
		<div class="span-10 error center">
			{if $error eq 1}
				La tarjeta ingresada no existe en el sistema.
			{elseif $error eq 2}
				La tarjeta ingresada a&uacute;n no ha sido facturada.
			{elseif $error eq 3}
				No se pudo cargar la informaci&oacute;n de la tarjeta.
			{elseif $error eq 4}
				La tarjeta ingresada ya tiene un pago asociado. No se puede continuar.
			{elseif $error eq 5}
				La tarjeta no es apta para la anulaci&oacute;n por penalidad. Su estado es: '{$global_salesStatus.$status}'
			{elseif $error eq 6}
				La tarjeta ya ha sido anulada previamente.
			{elseif $error eq 7}
				La tarjeta ingresada fue emitida por otro representante. Por favor comun&iacute;quese con el administrador.
			{/if}
		</div>
	</div>
{/if}