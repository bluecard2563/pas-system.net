{if $invoiceLogList}
<br/>
    <div class="span-24 last title">
        Lista de solicitudes para anulaci&oacute;n de factura<br />
    </div>
       <table border="0" id="invoiceLogsTable">
                <THEAD>
                <tr bgcolor="#284787">
                    <td align="center" class="tableTitle">
                        # de Factura
                    </td>
                    <td align="center"  class="tableTitle">
                        Factura a nombre de
                    </td>
                    <td align="center"  class="tableTitle">
                        Facturado a
                    </td>
                    <td align="center" class="tableTitle">
                        Fecha de Emisi&oacute;n
                    </td>
                    <td align="center" class="tableTitle">
                        Fecha de Vencimiento
                    </td>
                    <td align="center" class="tableTitle">
                        Total de la Factura
                    </td>
                    <td align="center" class="tableTitle">
                        Solicit&oacute;
                    </td>
                    <td align="center" class="tableTitle">
                        Fecha de Solicitud
                    </td>
                    <td align="center" class="tableTitle">
                        Ver Solicitud
                    </td>

                </tr>
                </THEAD>
                <TBODY>
                  {foreach name="invoiceLogs" from=$invoiceLogList item="i"}
                    <tr {if $smarty.foreach.invoiceLogs.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                        <td align="center" class="tableField">
                           {$i.number_inv}
                        </td>
                        <td align="center" class="tableField">
                            {if $i.customer_name}Cliente{else}Comercializador{/if}
                        </td>
                        <td align="center" class="tableField">
                            {if $i.customer_name}{$i.customer_name|htmlall}{else}{$i.name_dea|htmlall}{/if}
                        </td>
                        <td align="center" class="tableField">
                            {$i.date_inv}
                        </td>
                        <td align="center" class="tableField">
                            {$i.due_date_inv}
                        </td>
                        <td align="center" class="tableField">
                            USD {$i.total_inv}
                        </td>
                        <td align="center" class="tableField">
                            {$i.user_name}
                        </td>
                        <td align="center" class="tableField">
                            {$i.in_date_inl}
                        </td>
                        <td id="void_{$i.serial_inv}" align="center" class="tableField">
                            <a href="{$document_root}modules/invoice/fAuthorizeInvoiceLog/{$i.serial_inv}/{$serial_mbc}">ver</a>
                        </td>
                    </tr>
                  {/foreach}
                </TBODY>
       </table>
 <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
{else}
<div class="span-10 append-5 prepend-7 label">No existen solicitudes de anulaci&oacute;n de facturas pendientes para las opciones seleccionadas.</div>
{/if}