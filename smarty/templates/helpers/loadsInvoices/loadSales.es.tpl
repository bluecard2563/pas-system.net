{if $serial_dbm}
    {if $salesList}
    <br/>
        <div class="span-24 last title">
                Lista de ventas Elegibles<br />
        </div>
	<div class="span-24 last line">
           <table border="0" id="salesTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            <input type="checkbox" id="selAll" name="selAll"/>
                        </td>
                        <td align="center" class="tableTitle">
                            # de Tarjeta
                        </td>
                        <td align="center"  class="tableTitle">
                            Producto
                        </td>
                        <td align="center" class="tableTitle">
                            Titular
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Emisi&oacute;n
                        </td>
                        <td align="center" class="tableTitle">
                            Total
                        </td>
                        <td align="center" class="tableTitle">
                            Estado
                        </td>
						{if $erp_connection}
						<td align="center" class="tableTitle">
                            En ERP?
                        </td>
						{/if}
                        <td align="center" class="tableTitle">
                            Impuestos
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="sales" from=$salesList item="s"}
                        <tr {if !$erp_connection}
								{if $smarty.foreach.sales.iteration is even}
									bgcolor="#d7e8f9"
								{else}bgcolor="#e8f2fb"{/if}
							{else}
								{if $s.on_erp eq 'NO'}
									bgcolor="#F34646"
								{else}
									{if $smarty.foreach.sales.iteration is even}
										bgcolor="#d7e8f9"
									{else}bgcolor="#e8f2fb"{/if}
								{/if}
							{/if}>
                            <td align="center" class="tableField">
                                <input type="checkbox" value="{$s.serial_sal}" name="chkSales[]" class="salesChk" taxes="{$s.taxes}"/>
								<input type="hidden" name="serial_pxc[]" id="serial_pxc" value="{$s.serial_pxc}" serial_pxc="{$s.serial_pxc}"/>
                            </td>
                            <td align="center" class="tableField">
                               {if $s.card_number_sal}{$s.card_number_sal}{else}N/A{/if}
                            </td>
                            <td align="center" class="tableField">
                                {$s.name_pbl|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {if $s.customer_name}{$s.customer_name|htmlall}{else}{$s.legal_entity_name|htmlall}{/if}
                            </td>
                            <td align="center" class="tableField">
                                {$s.emission_date}
                            </td>
                            <td align="center" class="tableField">
                                {$s.total_sal}
                            </td>
                            <td align="center" class="tableField">
                                 {$global_salesStatus[$s.status_sal]}
                            </td>
							{if $erp_connection}
							<td align="center" class="tableField">
                                 {$global_yes_no[$s.on_erp]}
                            </td>
							{/if}
                            <td align="center" class="tableField">
                                 {$s.names_taxes}
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
	</div>
    <div class="span-24 last pageNavPosition line" id="pageNavPosition"></div>
    
    <div class="span-24 last center">
		<input type="hidden" name="selectedSales" id="selectedSales" title="Debe seleccionar al menos una venta." />
		<input type="hidden" name="total_pages" id="total_pages" value="{$total_pages}" />
        <input type="submit" value="Facturar">
    </div>
    {else}
         <div class="span-10 append-5 prepend-7 label" style="text-align: center;">No existen ventas pendientes por facturar para esta Sucursal de Comercializador.</div>
    {/if}
{else}
<div class="span-10 append-5 prepend-7 label" style="text-align: center;">No existen documentos para facturar asignados al representante seleccionado.</div>
{/if}