{if $providerList}
    <select name="selProvider{$sel_id}" id="selProvider{$sel_id}" title='El campo "Proveedor de Servicios" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="providers" from=$providerList item="l"}
        	<option value="{$l.serial_spv}">{$l.name_spv}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cou neq '' && $type_spv neq ''}
		No existen proveedores de servicios registrados.
    {/if}
    <select name="selProvider{$sel_id}" id="selProvider{$sel_id}"  title='El campo "Coordinador" es obligatorio' {if $serial_cou neq '' AND $type_spv neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}