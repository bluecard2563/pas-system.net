{if $data}

    {if  $data.type_cus eq LEGAL_ENTITY}
        <div class="span-24 last line separator">
            <label>Datos del Cliente</label>
        </div>

        {if $data.vip_cus eq 1}
        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">Es cliente VIP</div>
        </div>

        {/if}

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
            <div class="span-5">
                 {if $data.document_cus}{$data.document_cus}{/if}
                 <input type="hidden" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' {if $data.document_cus}value="{$data.document_cus}"{/if}/>
            </div>
            <div class="span-3 label">*Tipo de Cliente</div>
            <div class="span-4 append-2 last">
               {if $data.type_cus}{$data.type_cus}{/if}
            </div>
        </div>

        <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Raz&oacute;n Social:</div>
        <div class="span-5">
            <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Raz&oacute;n Social" es obligatorio'  readonly="readonly" {if $data.firstname_cus}value="{$data.firstname_cus}"{/if} />
        </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
            <div class="span-5">
                    <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Ciudad</div>
            <div class="span-4 append-4 last" id="cityContainerCustomer">
                    <select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit}</option>
                    {/foreach}
                </select>
             </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
            <div class="span-5">
               <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data.address_cus}value="{$data.address_cus}"{/if} />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
            <div class="span-5">
                <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" title='El campo "Tel&eacute;fono 1" es obligatorio' {if $data.phone1_cus}value="{$data.phone1_cus}"{/if}/>
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhone2Customer" id="txtPhone2Customer"  {if $data.phone2_cus}value="{$data.phone2_cus}"{/if}/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Celular:</div>
            <div class="span-5">
                <input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer"  {if $data.cellphone_cus}value="{$data.cellphone_cus}"{/if}/>
            </div>

            <div class="span-3 label"> Email:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio'  {if $data.email_cus}value="{$data.email_cus}"{/if}/>
            </div>
        </div>

        <div class="span-24  last line">
            <div class="prepend-4 span-4 label">*Familiar:</div>
            <div class="span-5">
                    <input type="text" name="txtRelative" id="txtRelative" title='El campo "Familiar" es obligatorio' {if $data.relative_cus}value="{$data.relative_cus}"{/if}/>
            </div>

            <div class="span-3 label">*Tel&eacute;fono del familiar:</div>
            <div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del familiar" es obligatorio' {if $data.relative_phone_cus}value="{$data.relative_phone_cus}"{/if}/> </div>
        </div>

        <div class="prepend-15 span-4 last line" id="divOpenDialog">
            <div class="span-5"> <a href="#" name="openDialog" id="openDialog">Configurar Preguntas</a></div>
        </div>

        <input type="hidden" name="hdnVip" id="hdnVip" value="{$data.vip_cus}" />

        <div id="hiddenAnswers">
    	{foreach from=$answerList item=al}
        	<input type="hidden" name="question_{$al.serial_que}" id="question_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}" />
            <input type="hidden" name="answer_{$al.serial_que}" id="answer_{$al.serial_que}" value="{$al.serial_ans}" />
        {/foreach}
    </div>

    {elseif  $data.type_cus eq PERSON}
                <div class="span-24 last line separator">
            <label>Datos del Cliente</label>
        </div>

        {if $data.vip_cus eq 1}
        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">Es cliente VIP</div>
        </div>

        {/if}

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
            <div class="span-5">
                 {if $data.document_cus}{$data.document_cus}{/if}
                 <input type="hidden" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' {if $data.document_cus}value="{$data.document_cus}"{/if}/>
            </div>
            <div class="span-3 label">*Tipo de Cliente</div>
            <div class="span-4 append-2 last">
               {if $data.type_cus}{$data.type_cus}{/if}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre:</div>
            <div class="span-5">
                <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Nombre" es obligatorio' readonly="readonly" {if $data.firstname_cus}value="{$data.firstname_cus}" {/if} />
            </div>

            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' readonly="readonly" {if $data.lastname_cus}value="{$data.lastname_cus}" {/if} />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
            <div class="span-5">
                    <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Ciudad</div>
            <div class="span-4 append-4 last" id="cityContainerCustomer">
                    <select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit}</option>
                    {/foreach}
                </select>
             </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                {if $data.birthdate_cus}{$data.birthdate_cus}{/if}
                <input type="hidden" name="txtBirthdayCustomer" id="txtBirthdayCustomer" title='El campo "Fecha de Nacimiento" es obligatorio' {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}/>
            </div>

            <div class="span-3 label">* Direcci&oacute;n:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data.address_cus}value="{$data.address_cus}"{/if} />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
            <div class="span-5">
                <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" title='El campo "Tel&eacute;fono 1" es obligatorio' {if $data.phone1_cus}value="{$data.phone1_cus}"{/if}/>
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhone2Customer" id="txtPhone2Customer"  {if $data.phone2_cus}value="{$data.phone2_cus}"{/if}/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Celular:</div>
            <div class="span-5">
                <input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer"  {if $data.cellphone_cus}value="{$data.cellphone_cus}"{/if}/>
            </div>

            <div class="span-3 label">* Email:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio'  {if $data.email_cus}value="{$data.email_cus}"{/if}/>
            </div>
        </div>

        <div class="span-24  last line">
            <div class="prepend-4 span-4 label">*Familiar:</div>
            <div class="span-5">
                    <input type="text" name="txtRelative" id="txtRelative" title='El campo "Familiar" es obligatorio' {if $data.relative_cus}value="{$data.relative_cus}"{/if}/>
            </div>

            <div class="span-3 label">*Tel&eacute;fono del familiar:</div>
            <div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del familiar" es obligatorio' {if $data.relative_phone_cus}value="{$data.relative_phone_cus}"{/if}/> </div>
        </div>

        <div class="prepend-15 span-4 last line" id="divOpenDialog">
            <div class="span-5"> <a href="#" name="openDialog" id="openDialog">Configurar Preguntas</a></div>
        </div>

        <input type="hidden" name="hdnVip" id="hdnVip" value="{$data.vip_cus}" />

        <div id="hiddenAnswers">
    	{foreach from=$answerList item=al}
        	<input type="hidden" name="question_{$al.serial_que}" id="question_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}" />
            <input type="hidden" name="answer_{$al.serial_que}" id="answer_{$al.serial_que}" value="{$al.serial_ans}" />
        {/foreach}
    </div>
   {/if}
{/if}
{literal}
<script type="text/javascript">
    loadDialog();
$('#openDialog').click(function(){
        $('#dialog').dialog('open');
    });

 </script>
{/literal}
