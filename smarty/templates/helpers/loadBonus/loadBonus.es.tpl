<div class="span-24 last line separator">
	<label>Incentivos en d&iacute;as de Cr&eacute;dito</label>
</div>
{if ($creditOnDate or $againstOnDate) and $totalOnDate}
	<div class="prepend-7 span-10 append-7 last line">
		<table border="1" id="tblBonusOnDate" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						Descripci&oacute;n
					</td>
					<td align="center" class="tableTitle">
						Monto
					</td>
				</tr>
			</THEAD>
			<TBODY>
				<tr bgcolor="#d7e8f9" >
					<td align="center" class="tableField">
						<b>Incentivos a favor:</b>
					</td>
					<td align="center" class="tableField">
						$ {$creditOnDate|number_format:2}
					</td>
				</tr>
				<tr bgcolor="#e8f2fb" >
					<td align="center" class="tableField">
						<b>Incentivos a Descontar:</b>
					</td>
					<td align="center" class="tableField">
						$ {$againstOnDate|number_format:2}
					</td>
				</tr>
				<tr bgcolor="#d7e8f9" >
					<td align="center" class="tableField">
						<b>Total Incentivos:</b>
					</td>
					<td align="center" class="tableField">
						$ {$totalOnDate|number_format:2}
					</td>
				</tr>
			</TBODY>
		</table>
	</div>
{else}
	<div class="prepend-7 span-10 append-7">
		<center>No existen Incentivos por pagos a tiempo</center>
	</div>
{/if}
<div class="span-24 last line separator">
	<label>Incentivos fuera de d&iacute;as de Cr&eacute;dito</label>
</div>
{if ($creditOutDate or $againstOutDate) and $totalOutDate}
	<div class="prepend-7 span-10 append-7 last line">
		<table border="1" id="tblBonusOutDate" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						Descripci&oacute;n
					</td>
					<td align="center" class="tableTitle">
						Monto
					</td>
				</tr>
			</THEAD>
			<TBODY>
				<tr bgcolor="#d7e8f9" >
					<td align="center" class="tableField">
						<b>Incentivos a favor:</b>
					</td>
					<td align="center" class="tableField">
						$ {$creditOutDate|number_format:2}
					</td>
				</tr>
				<tr bgcolor="#e8f2fb" >
					<td align="center" class="tableField">
						<b>Incentivos a Descontar:</b>
					</td>
					<td align="center" class="tableField">
						$ {$againstOutDate|number_format:2}
					</td>
				</tr>
				<tr bgcolor="#d7e8f9" >
					<td align="center" class="tableField">
						<b>Total Incentivos:</b>
					</td>
					<td align="center" class="tableField">
						$ {$totalOutDate|number_format:2}
					</td>
				</tr>
			</TBODY>
		</table>
	</div>
{else}

	<div class="prepend-7 span-10 append-7 line">
		<center>No existen Incentivos por pagos fuera d&iacute;as de Cr&eacute;dito</center>
	</div>
{/if}

{if ($creditOnDate or $againstOnDate or $creditOutDate or $againstOutDate) and ($totalOnDate or $totalOutDate )}
	<div class="span-24 last line buttons">
		<a target="_blank" href="{$document_root}modules/bonus/liquidation/liquidationsPreview/{$bonus_to}/{$serial}" style="cursor: pointer;" id="bonus-preview">Ver Pre - liquidaci&oacute;n</a>
	</div>

	<div class="span-24 last line separator">
		<label>Incentivos a Liquidar</label>
	</div>
	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Pagar incentivos:</div>
        <div class="span-4 append-8 last">
            <select name="selToPay" id="selToPay" title='El campo "Pagar incentivos" es obligatorio.' class="selectWidht">
                <option value="">- Seleccione -</option>
				{foreach from=$toPayOptions name="options" item="l"}
					<option value="{$l.value}"{if $smarty.foreach.options.total eq 1}selected{/if}>{$l.option}</option>
				{/foreach}
            </select>
        </div>
	</div>
	<div class="span-24 last line buttons">
		<input type="submit" name="btnPayBonus" id="btnPayBonus" value="Liquidar Incentivos" />
	</div>
{/if}