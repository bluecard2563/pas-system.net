{if $dealerList}
<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach from=$dealerList item="l" name="dealer"}
		<option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
  {/foreach}
</select>
{else}
	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
        <option value="">- Seleccione -</option>
    </select>
{/if}