{if $countryList}
<div class="span-24 last line">
	<div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
	<div class="span-5">
		<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			<option value="">- Seleccione -</option>
			{foreach from=$countryList name="countries" item="l"}
				<option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou|htmlall}</option>
			{/foreach}
		</select>
	</div>
</div>
<div class="span-24 last line">
	<div class="prepend-7 span-4 label">* Ciudad:</div>
	<div class="span-5 last" id="cityContainer">
		<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
			<option value="">- Seleccione -</option>
		</select>
	</div>
</div>

<div class="span-24 last line buttons">
	<input type="button" name="btnFindBonus" id="btnFindBonus" value="Verificar Incentivos" />
</div>

<div class="span-24 last line" id="bonusContainer"></div>
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 error">
			<center>No existen Incentivos fuera de fecha para ser autorizados</center>
	</div>
</div>
{/if}