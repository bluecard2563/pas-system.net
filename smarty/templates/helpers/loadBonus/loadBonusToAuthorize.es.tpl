
<div class="span-24 last line separator">
	<label>Incentivos por autorizar:</label>
</div>
{if $bonusToAuthorize}
<div class="prepend-2 span-20 append-2 last line">
	<table border="1" class="dataTable" id="tblBonus">
		<THEAD>
			<tr class="header">
				<th align="center">
                    <input type="checkbox" id="selAll" name="selAll"/>
                </th>
				<th align="center">
					Comercializador
				</th>
				<th align="center">
					Por pagar a
				</th>
				<th align="center">
					No. de Tarjeta
				</th>
				<th align="center">
					Fecha m&aacute;x. de Pago
				</th>
				<th align="center">
					Fecha de Pago
				</th>
				<th align="center">
					Monto
				</th>
				<th align="center">
					Tipo
				</th>
			</tr>
		</THEAD>
		<TBODY>
			 {foreach name="bonus" from=$bonusToAuthorize item="s"}
			<tr {if $smarty.foreach.bonus.iteration is even}class="odd"{else}class="even"{/if} >
				<td align="center" class="tableField">
					<input type="checkbox" value="{$s.serial_apb}" name="chkBonus" class="bonusChk"/>
				</td>
				<td align="center" class="tableField">
					{$s.name_dea|htmlall}
				</td>
				<td align="center" class="tableField">
					{$s.pay_to|htmlall}
				</td>
				<td align="center" class="tableField">
					{if $s.card_number_sal neq ''}{$s.card_number_sal}{else}N/A{/if}
				</td>
				<td align="center" class="tableField">
					{$s.due_date_inv}
				</td>
				<td align="center" class="tableField">
					{$s.date_pyf}
				</td>
				<td align="center" class="tableField">
					{$s.total_amount_apb|number_format:2}
				</td>
				<td align="center" class="tableField">
					{if $s.status_apb eq 'REFUNDED'}En contra{else}A favor{/if}
				</td>
			</tr>
            {/foreach}				
		</TBODY>
	</table>
</div>
<div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
<div class="span-24 last line"></div>
<div class="span-24 last line">
	<div class="prepend-7 span-4 label">* Autorizar:</div>
	<div class="span-4 append-8 last">
		<select name="selToAuthorize" id="selToAuthorize" title='El campo "Autorizar" es obligatorio.'>
			<option value="">- Seleccione -</option>
			<option value="YES">SI</option>
			<option value="NO">NO</option>
		</select>
	</div>
</div>
<div class="span-24 last line">
	<div class="prepend-7 span-4 label">* Observaciones:</div>
	<div class="span-4 append-8 last">
		<textarea name="txtObservations" id="txtObservations" style="width: 220px; height: 80px;" title="El campo 'Observaciones' es obligatorio."></textarea>
	</div>
</div>
<div class="span-24 last line buttons">
	<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
    <input type="hidden" name="selectedBonus" id="selectedBonus" title="Debe seleccionar al menos un incentivo.">
	<input type="submit" name="btnAuthorizeBonus" id="btnAuthorizeBonus" value="Aplicar" />
</div>
{else}
<div class="prepend-7 span-10 append-7">
	<center>No existen Incentivos fuera de fecha para ser autorizados</center>
</div>
{/if}


