{if $data}
	<select name="selResponsible" id="selResponsible">
  		<option value="">{if $opt_text}- {$opt_text} -{else}- Seleccione -{/if}</option>
  		{foreach from=$data item="l" name="users"}
  			<option value="{$l.serial_usr}" {if $data|@count eq 1} selected{/if}>{$l.name_usr|htmlall}</option>
  		{/foreach}
	</select>
{else}
    {if $serial_cit neq ''}
	No existen responsables asignados.
    {/if}
    <select name="selResponsible" id="selResponsible" {if $serial_cit neq ''}style="display:none;"{/if}>
            <option value="" selected>- Seleccione -</option>
    </select>
{/if}