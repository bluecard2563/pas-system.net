{if $dealerList}
<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach from=$dealerList item="l" name="dealer"}
		<option value="{$l.serial_dea}" {if $smarty.foreach.dealer.total eq 1}selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
  {/foreach}
</select>
{else}
	<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
		<option value="">- Seleccione -</option>
	</select>
{/if}