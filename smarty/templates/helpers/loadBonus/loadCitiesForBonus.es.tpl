{if $cityList}
    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="cities" from=$cityList item="l"}
        	<option value="{$l.serial_cit}" {if $smarty.foreach.cities.total eq 1}selected{/if}>{$l.name_cit|htmlall}</option>
        {/foreach}
    </select>
{else}
	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
        <option value="">- Seleccione -</option>
    </select>
{/if}
