{if $counterList}
<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio'>
  <option value="">- Seleccione -</option>
  {foreach from=$counterList item="l" name="counter"}
		<option value="{$l.serial_cnt}" {if $smarty.foreach.counter.total eq 1}selected{/if}>{$l.first_name_usr|htmlall} {$l.last_name_usr|htmlall}</option>
  {/foreach}
</select>
{else}
	<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio'>
        <option value="">- Seleccione -</option>
    </select>
{/if}