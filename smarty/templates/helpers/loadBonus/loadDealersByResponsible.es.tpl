{if $data}
	<select name="selDealer" id="selDealer">
  		<option value="">{if $opt_text}- {$opt_text} -{else}- Seleccione -{/if}</option>
  		{foreach from=$data item="l" name="users"}
  			<option value="{$l.serial_dea}" {if $data|@count eq 1} selected{/if}>{$l.name_dea|htmlall} - {$l.code_dea}</option>
  		{/foreach}
	</select>
{else}
    {if $serial_usr neq ''}
	No existen comercializadores asignados.
    {/if}
    <select name="selDealer" id="selDealer" {if $serial_usr neq ''}style="display:none;"{/if}>
            <option value="" selected>- Seleccione -</option>
    </select>
{/if}