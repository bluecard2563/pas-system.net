<div class="span-24 last line line">

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n del Comercializador:</label>
    </div>

	<!--div class="append-8 span-10 prepend-6 last ">
		<div id="alerts" class="span-10" align="center"></div>
	</div-->

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Raz&oacute;n Social:</div>
		<div class="span-5">
			<input type="text" id="txtName" name="txtName" title="El campo 'Raz�oacute;n Social' es obligatorio." value="{$dealer_data.name_dea|htmlall}" />
		</div>

		<div class="span-3 label">* RUC/CI:</div>
		<div class="span-4 append-4 last">
			<input type="text" id="txtNewDocument" name="txtNewDocument" title="El campo 'Nuevo Documento' es obligatorio." value="{$dealer_data.id_dea}" />
		</div>
	</div>
		
	<div class="span-24 last line separator">
		<label>Informaci&oacute;n de CASH Management:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Banco:</div>
		<div class="span-5">
			<select name="selBank" id="selBank">
                <option value="">- Seleccione -</option>
                {foreach from=$banks item="b"}
                    <option value="{$b|trim}" {if $dealer_data.bank_dea eq $b|trim}selected{/if}>{$b}</option>
                {/foreach}
            </select>
		</div>

		<div class="span-3 label">Tipo de Cuenta:</div>
		<div class="span-4 append-4 last">
			<select name="selAccountType" id="selAccountType">
                <option value="">- Seleccione -</option>
                {foreach from=$account_types item="ac"}
                    <option value="{$ac}" {if $dealer_data.account_type_dea eq $ac}selected{/if}>{$global_account_types[$ac]}</option>
                {/foreach}
            </select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">N&uacute;mero de Cuenta:</div>
		<div class="span-5">
			<input type="text" name="txtAccountNumber" id="txtAccountNumber" value="{$dealer_data.account_number_dea}" />
		</div>
	</div>

	<div class="span-24 last buttons line">
        <input type="submit" name="btnUpdate" id="btnUpdate" value="Guardar Cambios" />
    </div>
</div>