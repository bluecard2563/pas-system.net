{if $dealerList}
<div class="span-24 last line separator">
	<label>Lista de Comercializadores</label>
</div>
<div class="append-5 span-14 prepend-5 last line">
	<table border="0" id="dealer_table">
		<thead>
			<tr bgcolor="#284787">
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					<input type="checkbox" name="chkAll" id="chkAll" />
				</td>
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					Comercializador
				</td>
			</tr>
		</thead>
		<tbody>
		{foreach name="dealerList" from=$dealerList item="l"}
		<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.dealerList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				<input type="checkbox" name="chkDealers[]" id="chkDealer_{$l.serial_dea}" value="{$l.serial_dea}" />
			</td>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				{$l.name_dea|htmlall}
			</td>
		</tr>
		{/foreach}
		</tbody>
	</table>
	<input type="hidden" name="pages" id="pages" value="{$pages}" />
</div>
<div class="span-24 last line center" id="table_paging"></div>
{else}
<div class="append-5 span-14 prepend-5">
	<div class="span-14 last error center">
		No existen comercializadores registrados de acuerdo al par&aacute;metor de b&uacute;squeda ingresado.
	</div>
</div>
{/if}