{if $productList}
<div class="span-24 last line separator">
	<label>Lista de Productos</label>
</div>
<div class="prepend-5 append-5 span-14 last line">
	<table border="0" id="product_table">
		<thead>
			<tr bgcolor="#284787">
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					<input type="checkbox" name="chkAll_products" id="chkAll_products" />
				</td>
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					Producto
				</td>
			</tr>
		</thead>
		<tbody>
		{foreach name="productList" from=$productList item="l"}
		<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.productList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				<input type="checkbox" name="chkProduct[]" id="chkProduct_{$l.serial_pro}" value="{$l.serial_pxc}" {if $l.asigned && $l.asigned eq 'YES'}checked{/if} />
			</td>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				{$l.name_pbl|htmlall}
			</td>
		</tr>
		{/foreach}
		</tbody>
	</table>
	<input type="hidden" name="pages_product" id="pages_product" value="{$pages}" />
</div>
<div class="span-24 last line center" id="table_paging_product"></div>
{else}
<div class="append-5 span-14 prepend-5">
	<div class="span-14 last error center">
		No existen asignados para el pa&iacute;s seleccionado.
	</div>
</div>
{/if}