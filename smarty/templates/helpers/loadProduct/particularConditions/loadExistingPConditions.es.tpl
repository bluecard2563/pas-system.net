<div class="span-24 last line">
	<div class="span-24 last line separator">
	<label>Condiciones Particulares Asociadas</label>
	</div>

</div>

{if $pconditions_list}
	<div class="prepend-2 span-20 append-2 last line">
		<table border="1" class="dataTable" id="tblPConditions">
			<thead>
				<tr class="header">					
					<th style="text-align: center;" >
						No.
					</th>
					<th style="text-align: center;" >
						Condici&oacute;n Particular
					</th>
					<th style="text-align: center;" >
						Estado
					</th>
					<th style="text-align: center;" >
						Activar/Desactivar
					</th>
				</tr>
			</thead>
			<tbody>
			{foreach name="pConditions" from=$pconditions_list item="pc"}
			<tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.pConditions.iteration is even}class="even"{else}class="odd"{/if}>
				<td align="center" class="tableField">
					{$pc.weight_pcp}
				</td>
				<td align="center" class="tableField">
					{$pc.name_prc|truncate_html}
				</td>
				<td align="center" class="tableField">
					{$global_status[$pc.status_pcp]}
				</td>
				<td align="center" class="tableField">
					<a class="link" id="pCondition_{$pc.serial_pcp}" status_to="{if $pc.status_pcp eq 'ACTIVE'}INACTIVE{else}ACTIVE{/if}" serial_pcp="{$pc.serial_pcp}">Cambiar Estado</a>
				</td>
			</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
{else}
	<div class="prepend-7 span-10 append-7 last line">
		<div class="span-10 error center">
			No existen condiciones particulares asociadas al momento para el producto y pa�s seleccionado.
		</div>
	</div>
{/if}

<div class="span-24 last buttons line">
	<input type="submit" name="btnAsign" id="btnAsign" value="Asignar" />
	<input type="hidden" name="hdnRemoteValidation" id="hdnRemoteValidation" value="1" />
</div>		
