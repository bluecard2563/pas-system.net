<form name="frmNewPCondition" id="frmNewPCondition" method="post" action="{$document_root}modules/particular_conditions/pRegisterTranslation">

	<div class="prepend-7 span-10 append-7 last line">
		<i>* Ingrese la traducci&oacute;n en el cuadro a continuaci&oacute;n:</i>
	</div>
	
	<div class="prepend-5 append-5 span-14 last line">
		<textarea name="txtDescription" id="txtDescription">{if $translation}{$translation.name_prc}{/if}</textarea>
	</div>
    
    <div class="span-24 last buttons line">
		<input type="button" name="btnRegistrar" id="btnRegistrar" value="Registrar" />
		<textarea class="hide_me" name="hdnDescription" id="hdnDescription">{if $translation}{$translation.name_prc}{/if}</textarea>
		<input type="hidden" name="hdnSerial_lang" id="hdnSerial_lang" value="{$serial_lang}" />
		<input type="hidden" name="hdnPrc_Serial_prc" id="hdnPrc_Serial_prc" value="{$serial_prc}" />
		<input type="hidden" name="hdnSerial_prc" id="hdnSerial_prc" value="{$serial_prc_translation}" />
    </div>
</form>