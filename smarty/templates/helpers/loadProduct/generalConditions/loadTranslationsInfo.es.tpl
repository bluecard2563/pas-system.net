{if $gconditions_list}
<div class="prepend-3 span-19 append-2 last line">
	<div class="prepend-1 span-8 label">* Nombre:</div>
	<div class="span-8 append-2 last">
		<input type="text" name="txtCondition" id="txtCondition" title="El campo 'Condici&oacute;n General' es obligatorio." value="{$gconditions_list.0.name_glc}">
	</div>
</div>

<div class="prepend-4 span-16 append-4 last line">
	<table border="0" id="translationsTable" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					Documento
				</td>
				<td align="center" class="tableTitle" colspan="3">
					Actividades
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="conditions" from=$gconditions_list item="s"}
			<tr {if $smarty.foreach.conditions.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.conditions.iteration}
				</td>
				<td align="center" class="tableField">
					{$s.url_glc|htmlall}
				</td>
				<td align="center" class="tableField">
					<a class="link_item" href="{$document_root}pdfs/generalConditions/{$s.url_glc|htmlall}" target="_blank">Ver</a>
				</td>
				<td align="center" class="tableField">
					<a class="link_item" id="update_{$s.serial_glc}" element_id="{$s.serial_glc}" >Actualizar</a>
				</td>
				<td align="center" class="tableField">
					<a class="link_item" id="translate_{$s.serial_glc}" element_id="{$s.serial_glc}" >A&ntilde;adir archivo <br>en otro idioma</a>
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>

<div class="span-24 last buttons line">
	<input type="submit" name="btnInsert" id="btnInsert" value="Actualizar" />
	<input type="hidden" name="hdnType" id="hdnType" value="UPDATE_CONDITION" />
</div>
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 error">
		No existen condiciones generales para este idioma por el momento.
	</div>
</div>
{/if}