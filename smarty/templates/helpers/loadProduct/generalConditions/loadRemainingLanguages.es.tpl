<select name="selLanguageTranslation" id="selLanguageTranslation" title="El campo 'Idioma' es obligatorio.">
	<option value="">- Seleccione -</option>
	{if $remainingLanguages}
		{foreach from=$remainingLanguages item="l"}
		<option value="{$l.serial_lang}">{$l.name_lang}</option>
		{/foreach}
	{/if}
</select>