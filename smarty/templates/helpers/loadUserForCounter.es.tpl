{if $auxList}
     <select name="selUser" id="selUser" title='El campo "Seleccione Usuario" es obligatorio.'>
            	<option value="">- Seleccione -</option>
        {foreach name="users" from=$auxList item="l"}
        	<option value="{$l.serial_usr}" {if $smarty.foreach.users.total eq 1}selected{/if}>{$l.first_name_usr|htmlall} {$l.last_name_usr|htmlall}</option>
        {/foreach}
     </select>
{else}
    <input type="hidden" id="selUser"  title='El campo "Seleccione Usuario" es obligatorio.' />
	No existen Usuarios para asignar.
{/if}