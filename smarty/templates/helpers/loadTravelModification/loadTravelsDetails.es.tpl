{if $activeRegisterTravels}
    <br/>
        <div class="span-24 last title">
                Viajes modificables de la tarjeta ingresada<br />
        </div>
	<div class="span-24 last line">
           <table border="0" id="travelsTable">
                    <THEAD>
                    <tr bgcolor="#00b3f0">
                        <td align="center" class="tableTitle">
                            # de Tarjeta
                        </td>
						<td align="center" class="tableTitle">
                            Cliente
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Inicio
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Expira
                        </td>
                        <td align="center" class="tableTitle">
                            Ciudad Destino
                        </td>
                        <td align="center" class="tableTitle">
                            Anular
                        </td>
						<td align="center" class="tableTitle">
                            Modificar
                        </td>

                    </tr>
                    </THEAD>
                    <TBODY>
                       {foreach name="registers" from=$activeRegisterTravels item="s"}
                        <tr {if $smarty.foreach.registers.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                               {$s.card_number}
                            </td>
                            <td align="center" class="tableField">
                                {$s.customer_name|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.start_trl}
                            </td>
                            <td align="center" class="tableField">
                                {$s.end_trl}
                            </td>
                            <td align="center" class="tableField">
                                {$s.name_cit}
                            </td>
                            <td align="center" class="tableField">
                                <a class="void_travel" serial_trl="{$s.serial_trl}" style="cursor: pointer;">Anular</a>
                            </td>
							<td align="center" class="tableField">
								<a href="{$document_root}modules/sales/travelModification/fUpdateTravel/{$s.serial_trl}">Modificar</a>
							</td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
	</div>
     <div class="span-24 last pageNavPosition" id="pageTravelPosition"></div>
{else}
<div class="span-10 append-5 prepend-7 label">La Tarjeta ingresada no tiene viajes registrados activos.</div>
{/if}