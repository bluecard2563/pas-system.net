{if $mbcList}
<select name="selManager" id="selManager" class="span-5" title="El campo 'Representante' es obligatorio.">
	  <option value="">- Seleccione -</option>
	  {foreach from=$mbcList item="l"}
		<option value="{$l.serial_mbc}" {if $mbcList|@count eq 1} selected{/if}>{$l.name_man|htmlall}</option>
	  {/foreach}
	</select>
{else}
	No existen Representantes disponibles.
	<select name="selManager" id="selManager" style="display: none;">
	  <option value="">- Seleccione -</option>
	</select>
{/if}