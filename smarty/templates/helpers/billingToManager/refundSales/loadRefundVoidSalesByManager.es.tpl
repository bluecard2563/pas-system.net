{if $selectableSales}
<div class="span-24 last separator">Ventas en el periodo seleccionado</div>
	<div class="prepend-6 span-12 append-6 last" style="padding-top: 20px;">
		<div class="span-12 notice center">
			Todos los elementos marcados ser&aacute;n <b>EXCLU&Iacute;DOS</b> de la pr&oacute;xima factura.
		</div>		
	</div>
		<table border="1" class="dataTable" align="center" id="salesTable">
		<thead>
			<tr class="header">
				<th style="text-align: center;"># de Tarjeta</th>
				<th style="text-align: center;">Sucursal</th>
				<th style="text-align: center;">Estado</th>
				<th style="text-align: center;">Costo Internacional</th>
				<th style="text-align: center;">Precio de Venta</th>
				<th style="text-align: center;">Obs. de Solicitud</th>
				<th style="text-align: center;">Obs. de Autorizaci&oacute;n</th>
				<th style="text-align: center;">Autorizado por</th>
				<th style="text-align: center;"># d&iacute;as entre STAND BY e inicio de vigencia</th>
				<th style="text-align: center;">Seleccione</th>
		  </tr>
		</thead>
		<tbody>
			{foreach name="sales" from=$selectableSales item=s}
			<tr {if $smarty.foreach.sales.iteration is even}class="odd"{else}class="even"{/if} >
				<td class="tableField">{if $s.card_number_sal}{$s.card_number_sal}{else}N/A{/if}</td>
				<td class="tableField">{$s.name_dea|htmlall}</td>
				<td class="tableField">{$global_salesStatus[$s.status_sal]}</td>
				<td class="tableField">{$s.international_fee_amount}</td>
				<td class="tableField">{$s.total_sal}</td>
				<td class="tableField">{$s.request_obs|htmlall}</td>
				<td class="tableField">{$s.observations|htmlall}</td>
				<td class="tableField">{$s.first_name_usr|htmlall} {$s.last_name_usr|htmlall}</td>
				<td class="tableField">{$s.days_after_stand_by}</td>
				<td class="tableField"><input type="checkbox"  id="{$s.serial_sal}" name="{$s.serial_sal}" /></td>
			</tr>
			{/foreach}
		</tbody>
		</table>
<div class="span-24 last buttons line">
		<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
		<input type="submit" name="btnSave" id="btnSave" value="Guardar"/>
		
</div>
{else}
	<div class="  prepend-7 span-10 append-7 last"><br/>
			<div class="span-10" align="center">
				No existen Ventas registradas
			</div>
	</div>
{/if}