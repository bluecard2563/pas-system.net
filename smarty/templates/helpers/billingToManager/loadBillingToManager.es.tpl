{if $bill}
<center>
    <div class="span-10 last line">
        <table border="0" class="paginate-1 max-pages-7" id="benefits_upload_table">
            <thead>
                <tr bgcolor="#284787">
                    <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						N&uacute;mero de factura
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Fecha
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Monto
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Ver
                    </td>
                </tr>
            </thead>

            <tbody>
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9">
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$bill.billing_number_btm}
					<input type="hidden" id="hdnSerial_btm" name="hdnSerial_btm" value="{$bill.serial_btm}" />
				</td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$bill.date_btm}
				</td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$bill.amount_btm}
				</td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					<input type="button" id="reprintBillingReport" class="PDF" onclick="window.open('{$global_url}modules/billingToManager/viewBillingToManager/{$bill.serial_btm}/pending')" />
				</td>
            </tr>
            </tbody>
        </table>
    </div>
</center>

<div class="span-10 last buttons line">
	<input type="submit" name="btnPayBillingToManager" id="btnPayBillingToManager" value="Liquidar Factura" >
</div>
{else}
	{if $serial_man}
		<center>El representante seleccionado no tiene facturas pendientes.</center>
	{/if}
{/if}