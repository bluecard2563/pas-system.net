{if $managerList}
<select name="selManager" id="selManager" title="El campo 'Manager' es obligatorio.">
	<option value="">- Seleccione -</option>
	{foreach from=$managerList item="l"}
		<option value="{$l.serial_man}">{$l.name_man|htmlall}</option>
	{/foreach}
</select>
{else}
	<select name="selManager" id="selManager" title="El campo 'Manager' es obligatorio." {if $serial_cou}style="display:none;"{/if}>
		<option value="">- Seleccione -</option>
	</select>
	{if $serial_cou}
	No se encontr&oacute; ningun representante para el pa&iacute;s seleccionado.
	{/if}
{/if}