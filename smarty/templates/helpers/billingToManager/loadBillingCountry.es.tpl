
{if $bill}
<input type="hidden" id="hdnDocumentNumber" name="hdnDocumentNumber" value="{$bill.0.billing_number_btm}" />

<div class="span-10 last line">
	<div class="span-1 left label">Fecha:</div>
	<div class="span-2 left last">
			{$bill.0.date_btm}
    </div>
</div>
<div class="span-10 last line">
	<div class="span-3 left label"># de Factura:</div>
	<div class="span-1  last">
			{$bill.0.billing_number_btm}
    </div>
	<div class="span-1 prepend-4 right label">Ver:</div>
	<div class="span-1 right last">
		<input type="button" id="reprintBillingCountryReport" class="PDF"/>
    </div>
</div>
<center>
    <div class="span-10 last line">
        <table border="0">
            <thead>
                <tr bgcolor="#284787">
                    <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Representante
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						Monto
                    </td>
                </tr>
            </thead>
            <tbody>
			{foreach from=$bill item="bl"}	
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9">
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{if $bl.name_man neq ''}{$bl.name_man}{else}<b>TOTAL:</b>{/if}
				</td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
					{$bl.amount_btm}
				</td>
            </tr>
			{/foreach}
            </tbody>
        </table>
    </div>
</center>

<div class="span-10 last buttons line">
	<input type="submit" name="btnPayBillingToManagerByCountry" id="btnPayBillingToManagerByCountry" value="Liquidar Factura" >
</div>
{else}
	{if $serial_cou}
	<center>El Pa&iacute;s seleccionado no tiene representantes con facturas pendientes.</center>
	{/if}
{/if}