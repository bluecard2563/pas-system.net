{if $listOfBranches}
<div align="center">Existentes</div>
<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-11 last">
	{foreach from=$listOfBranches item="l"}
  <option value="{$l.serial_dea}" managerByCountry="{$serial_mbc}" responsible="{$serial_usr}">{$l.code_dea} - {$l.name_dea|htmlall}</option>
  {/foreach}

</select>
{else}
<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-11 last" style="display:none;">
</select>
No existen sucursales disponibles para los filtros aplicados.
{/if}