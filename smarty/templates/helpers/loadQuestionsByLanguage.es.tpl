{if $questions}
        <div class="prepend-4 span-20 last line">Preguntas registradas para el idioma seleccionado.</div>
        <div class="prepend-4 span-16 append-4 last line">
            <table border="0" id="questionsTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            No.
                        </td>
                        <td align="center" class="tableTitle">
                            pregunta
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="que" from=$questions item="q"}
                        <tr {if $smarty.foreach.que.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                <b>{$smarty.foreach.que.iteration}</b>
                            </td>
                            <td align="left">
                                {$q.text_qbl|htmlall}
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
        </div>
        <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
{else}
    <div class="prepend-4 span-20 last line">
        No existen preguntas registradas para el idioma seleccionado.
    </div>   
{/if}