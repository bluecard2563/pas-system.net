{if $responsablesList}
<select name="selResponsable" id="selResponsable" title="El campo 'Responsables' es obligatorio">
  <option value="">- {if $opt_text}{$opt_text}{else}Seleccione{/if} -</option>
  {foreach from=$responsablesList item="l" name="responsables"}
        <option value="{$l.serial_usr}" {if $smarty.foreach.responsables.total eq 1}selected{/if}>{$l.name_usr|htmlall}</option>
  {/foreach}
</select>
{else}
    No existen Responsables en la ciudad seleccionada.
{/if}
