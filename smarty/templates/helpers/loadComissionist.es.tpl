{if $comissionistList}
    <select name="selComissionist{$serial_sel}" id="selComissionist{$serial_sel}" title='El campo "Responsable" es obligatorio'>
        <option value="">- Seleccione -</option>
        {foreach name="comissionist" from=$comissionistList item="l"}
        	<option value="{$l.serial_usr}" {if $smarty.foreach.comissionist.total eq 1}selected{/if}>{$l.name_usr}</option>
        {/foreach}
    </select>
{else}
    {if $serial_cit neq ''}
		No existen Responsables registrados.
    {/if}
    <select name="selComissionist{$serial_sel}" id="selComissionist{$serial_sel}" title='El campo "Responsable" es obligatorio' {if $serial_cit neq ''}style="display:none;"{/if}>
      <option value="">- Seleccione -</option>
    </select>
{/if}

{literal}
<script type="text/javascript">
	$('#dealerContainer').load(document_root+"rpc/loadDealersByComissionist.rpc",{serial_cit: {/literal}{$serial_cit}{literal}, serial_usr: $("#selComissionist{/literal}{$serial_sel}{literal}").val() });
	//$('#selComissionist').bind("change",function(){
		//$('#hdnFlagCodeSector').val($('#hdnFlagCodeSector').val()+1);
		//$('#hdnFlagNameSector').val($('#hdnFlagNameSector').val()+1);
	//});
</script>
{/literal}