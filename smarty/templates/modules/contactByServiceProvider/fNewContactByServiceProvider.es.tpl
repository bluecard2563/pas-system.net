{assign var="title" value="NUEVO OPERADOR"}
<form name="frmNewContactByServiceProvider" id="frmNewContactByServiceProvider" method="post" action="{$document_root}modules/contactByServiceProvider/pNewContactByServiceProvider" class="">
    <div class="span-24 last title">
    	Registro de Nuevo Operador de Coordinador<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {else}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

     <div class="span-14 prepend-5 append-5 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

   <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Residencia:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneContainer">* Zona de Residencia:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s de Residencia:</div>
        <div class="span-5 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select>
         </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" >* Ciudad de Residencia:</div>
        <div class="span-5" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>


    <div class="span-24 last line separator">
        <label>Informaci&oacute;n del Proveedor:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneProvContainer">* Zona del Proveedor:</div>
        <div class="span-5">
            <select name="selZoneProv" id="selZoneProv" title='El campo "Zona del Proveedor" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s del Proveedor:</div>
        <div class="span-5 append-4 last" id="countryProvContainer">
            <select name="selCountryProv" id="selCountryProv" title='El campo "Pa&iacute;s del Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select>
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Proveedor:</div>
        <div class="span-5" id="providerContainer">
            <select name="selProvider" id="selProvider" title='El campo "Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n General:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Nombre:</div>
        <div class="span-5"> <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio'> </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-5 append-4 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio'> </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Tel&eacute;fono:</div>
        <div class="span-5">  <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio'> </div>
        <div class="span-3 label">* Email 1:</div>
        <div class="span-5 append-4 last">  
			<input type="text" name="txtEmail" id="txtEmail" title='El campo "Email 1" es obligatorio' />
			<input type="hidden" name="hdnEmail" id="hdnEmail" value="0" />
        </div>
    </div>

	<div class="span-24 last line">
       <div class="prepend-4 span-3 label">* Tipo:</div>
        <div class="span-5">
             <select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$typeList item=tl}
                    <option value="{$tl}">{if $tl eq ASSISTANCE}Asistencias{elseif $tl eq PAYMENT}Facturas{else}Contacto{/if}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">Email 2:</div>
        <div class="span-5 append-4 last">
			<input type="text" name="txtEmail2" id="txtEmail2" title='El campo "Email 2" es obligatorio' />
			<input type="hidden" name="hdnEmail2" id="hdnEmail2" value="0" />
        </div>
    </div>
	
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" >
   </div>
</form>
