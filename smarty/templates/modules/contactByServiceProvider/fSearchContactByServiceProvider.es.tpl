{assign var="title" value="BUSCAR OPERADOR"}
<div class="span-24 last title">Buscar Operador</div>
<form name="frmSearchContactByServiceProvider" id="frmSearchContactByServiceProvider" action="{$document_root}modules/contactByServiceProvider/fUpdateContactByServiceProvider" method="post" >
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El Operador ingresado no existe. Seleccione uno de la lista.
                {/if}
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneProvContainer">* Zona del Proveedor:</div>
        <div class="span-5">
            <select name="selZoneProv" id="selZoneProv" title='El campo "Zona del Proveedor" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s del Proveedor:</div>
        <div class="span-5 append-4 last" id="countryProvContainer">
            <select name="selCountryProv" id="selCountryProv" title='El campo "Pa&iacute;s del Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select>
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Coordinador:</div>
        <div class="span-5" id="providerContainer">
            <select name="selProvider" id="selProvider" title='El campo "Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select>
        </div>
        <div class="span-3 label">* Operador:</div>
        <div class="span-5 append-4 last" id="contactByServiceProviderContainer">
            <select name="selContactByServiceProvider" id="selContactByServiceProvider" title='El campo "Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line">
    </div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" />
    </div>
    <input type="hidden" name="hdnContactID" id="hdnContactID" value="" />
</form>