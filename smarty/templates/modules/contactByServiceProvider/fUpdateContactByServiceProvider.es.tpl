{assign var="title" value="ACTUALIZAR OPERADOR"}
<form name="frmUpdateContactByServiceProvider" id="frmUpdateContactByServiceProvider" method="post" action="{$document_root}modules/contactByServiceProvider/pUpdateContactByServiceProvider" class="">
	<div class="span-24 last title">
    	Actualizar Operador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 error" align="center">
            Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
        </div>
    </div>
    {/if}

     <div class="span-14 prepend-5 append-5 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

   <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Residencia:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneContainer">* Zona de Residencia:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}" {if $zl.serial_zon eq $data.aux.serial_zon} selected="selected"{/if}>{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s de Residencia:</div>
        <div class="span-5 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="4">- Seleccione -</option>
                {foreach from=$countryList item="cl"}
                    <option id="{$cl.serial_cou}" value="{$cl.serial_cou}"{if $cl.serial_cou eq $data.aux.serial_cou} selected="selected"{/if}>{$cl.name_cou}</option>
                {/foreach}
            </select>
         </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" >* Ciudad de Residencia:</div>
        <div class="span-5" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$cityList item="cl"}
                    <option id="{$cl.serial_cit}" value="{$cl.serial_cit}"{if $cl.serial_cit eq $data.serial_cit} selected="selected"{/if}>{$cl.name_cit}</option>
                {/foreach}
            </select>
         </div>
    </div>


    <div class="span-24 last line separator">
        <label>Informaci&oacute;n del Proveedor:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneProvContainer">* Zona del Proveedor:</div>
        <div class="span-5">
            <select name="selZoneProv" id="selZoneProv" title='El campo "Zona del Proveedor" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}" {if $zl.serial_zon eq $data.aux.prov_serial_zon} selected="selected"{/if}>{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s del Proveedor:</div>
        <div class="span-5 append-4 last" id="countryProvContainer">
            <select name="selCountryProv" id="selCountryProv" title='El campo "Pa&iacute;s del Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
                {foreach from=$countryProvList item="cl"}
                    <option id="{$cl.serial_cou}" value="{$cl.serial_cou}"{if $cl.serial_cou eq $data.aux.prov_serial_cou} selected="selected"{/if}>{$cl.name_cou}</option>
                {/foreach}
            </select>
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Proveedor:</div>
        <div class="span-5" id="providerContainer">
            <select name="selProvider" id="selProvider" title='El campo "Proveedor" es obligatorio'>
                <option value="4">- Seleccione -</option>
                {foreach from=$providerList item="pl"}
                    <option id="{$pl.serial_spv}" value="{$pl.serial_spv}"{if $pl.serial_spv eq $data.serial_spv} selected="selected"{/if}>{$pl.name_spv}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n General:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Nombre:</div>
        <div class="span-5"> <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio' value="{$data.first_name_csp}"> </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-5 append-4 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$data.last_name_csp}"> </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Tel&eacute;fono:</div>
        <div class="span-5">  <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio' value="{$data.phone_csp}"> </div>
        <div class="span-3 label">* Email:</div>
        <div class="span-5 append-4 last">  
			<input type="text" name="txtEmail" id="txtEmail" title='El campo "Email" es obligatorio' value="{$data.email_csp}" />
			<input type="hidden" name="hdnEmail" id="hdnEmail" value="0" />
        </div>
    </div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Tipo:</div>
        <div class="span-5">
             <select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$typeList item=tl}
                    <option value="{$tl}" {if $tl eq $data.type_csp} selected="selected"{/if}>{if $tl eq ASSISTANCE}Asistencias{elseif $tl eq PAYMENT}Facturas{else}Contacto{/if}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">Email 2:</div>
        <div class="span-5 append-4 last">
			<input type="text" name="txtEmail2" id="txtEmail2" title='El campo "Email 2" es obligatorio' value="{$data.email2_csp}" />
			<input type="hidden" name="hdnEmail2" id="hdnEmail2" value="0" />
        </div>
    </div>


    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Estado:</div>
        <div class="span-5">
            <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$statusList item=sl}
                    <option value="{$sl}" {if $sl eq $data.status_csp}selected="selected"{/if}>{$global_status.$sl}</option>
                {/foreach}
            </select>
        </div>
    </div>
	
    <div class="span-24 last buttons line">
        <input type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" />
   </div>
   <input type="hidden" name="hdnContactID" id="hdnContactID" value="{$data.serial_csp}"/>
</form>
