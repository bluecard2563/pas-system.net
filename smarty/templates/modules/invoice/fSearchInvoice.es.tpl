{assign var="title" value="B&Uacute;SQUEDA DE FACTURA PARA SOLICITAR ANULACI&Oacute;N"}
<div class="span-24 last title ">
    	B&uacute;squeda de Factura para solicitar anulaci&oacute;n<br />
        <label>(*) Campos Obligatorios</label>
</div>
<div class="span-24 last line"></div>

<form name="frmSearchInvoice" id="frmSearchInvoice" action="{$document_root}modules/invoice/fVoidInvoiceRequest" method="post" >
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Hubo errores en la solicitud de anulaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 2}
                    Solicitud de anulaci&oacute;n exitosa!
                {elseif $error eq 3}
                    La factura ingresada no existe. Seleccione una de la lista.
                {elseif $error eq 4}
                    La solicitud se ha creado exitosamente. Sin embargo, la alerta no pudo <br>crearse para todos los usuarios asignados.
                {/if}
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
        <div class="prepend-8 span-3 label" id="zoneContainer">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-8 span-3 label">* Pa&iacute;s:</div>
        <div class="span-5" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-3 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-8 span-3 label">* Documento Aplicado a:</div>
        <div class="span-5" id="documentContainer">
            <select name="selDocument" id="selDocument" title='El campo "Tipo de documento" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>

    <div class="span-24 last line" id="invoiceContainer" style="display:none;">
        <div class="prepend-5 span-6 label">* Ingrese el n&uacute;mero de factura:</div>
        <div class="span-5 last">
            <input type="text" name="txtInvoiceNumber" id="txtInvoiceNumber" title="El campo de b&uacute;squeda es obligatorio." class="span-4"/>
        </div>
    </div>
    <div class="span-24 last line"></div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" />
    </div>
    
</form>