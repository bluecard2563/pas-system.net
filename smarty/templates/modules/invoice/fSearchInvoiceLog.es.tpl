{assign var="title" value="B&Uacute;SQUEDA DE SOLICITUDES PARA ANULAR FACTURA"}
<div class="span-24 last title ">
    	B&uacute;squeda de Solicitudes para anular factura<br />
        <label>(*) Campos Obligatorios</label>
</div>
<div class="span-24 last line"></div>

<form name="frmSearchInvoiceLog" id="frmSearchInvoiceLog">
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Autorizaci&oacute;n exitosa!
                    {if $number_cn}
                        {if $number_cn neq -1}
						<br /><b><a href="{$document_root}modules/creditNote/pPrintFirstCreditNote/{$serial_cn}" target="_blank">Imprimir Nota de Cr&eacute;dito No. {$number_cn}</a></b>
                        {/if}
                    {/if}
                {elseif $error eq 2}
                    Hubo errores al cambiar el estado de la solicitud. Por favor vuelva a intentarlo.
                {elseif $error eq 3}
                    Hubo errores al cambiar el estado de la factura. Por favor vuelva a intentarlo.
                {elseif $error eq 4}
                    Hubo errores al cambiar el estado en ventas. Por favor vuelva a intentarlo.
                {elseif $error eq 5}
                    La venta fue anulada exitosamente, pero hubo errores en el registro de modificaci&oacute;n de ventas. Por favor vuelva a intentarlo.
                {elseif $error eq 6}
                    Hubo errores en las alertas. Por favor vuelva a intentarlo.
                {elseif $error eq 7}
                    No existe un documento para notas de cr&eacute;dito en el pa&iacute;s en el que se encuentra. <br>
                    Por favor comun&iacute;quese con el administrador.
                {elseif $error eq 8}
                    Hubo errores al registrar el nuevo n&uacute;mero de la nota de cr&eacute;dito creada.
                {elseif $error eq 9}
                    Hubo errores al actualizar el n&uacute;mero de la nota de cre&eacute;dito.
                {elseif $error eq 10}
                    La nota de cr&eacute;dito no se ha podido generar. Por favor comun&iacute;quese con el administrador.
				{elseif $error eq 11}
                    {if $erp_message}
						{$erp_message}
					{else}
						La solicitud no se ha podido procesar ya que no hubo conexi&oacute;n con el ERP. Por favor comun&iacute;quese con el administrador.
					{/if}
                {/if}
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    {if $zoneList}
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneContainer">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-5 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
        <div class="span-3 label">* Tipo de documento:</div>
        <div class="span-5 append-4 last" id="documentContainer">
            <select name="selDocument" id="selDocument" title='El campo "Tipo de documento" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>

	<div class="span-24 last buttons line">
        <input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
    </div>

    <div class="span-24 last line" id="invoiceLogContainer"></div>
    
    {else}
    <div class="span-24 last line" align="center">
        No Existen solicitudes pendientes.
    </div>
    {/if}
</form>