<!--/*
File: fChooseSales.es.tpl
Author: Nicolas Flores
Creation Date: 02/03/2010
Last Modified: 
Modified by: 
*/-->


{assign var="title" value="SELECCION DE VENTAS A FACTURAR"}
<form name="frmChooseSales" id="frmChooseSales" method="post" action="{$document_root}modules/invoice/fNewInvoice">
	<div class="span-24 last title">
    	Selecci&oacute;n de Ventas a Facturar<br />
        <label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-7 span-10 prepend-7 last " id="messageContainer">
    	{if $error eq 1}
            <div class="success" align="center">
            	Ventas Facturadas<br/>
				{if $inserted_serial_inv}<a href="{$document_root}modules/invoice/pPrintInvoice/{$inserted_serial_inv}" target="_blank">Imprimir Factura</a>
				
				 {/if}
				 
            </div>
			 <br>

                {if $inserted_serial_inv}
				{if (USE_PTP) && $ptp_use eq "YES"}
                    <div class="center">
                        <img src="http://sandbox.pas-system.net/img/place_to_pay/placetoplay.png" alt="" style="height: 70px;width: 180px;">
                        <img src="http://sandbox.pas-system.net/img/place_to_pay/logos_tarjetasANT.png" alt="">
                    </div>
                    <div class="center">
                        <a href="" id="pre">Preguntas frecuentes</a><br>
                        <br>
                        <input type="checkbox" id="checkme"/><label for="check">Acepto</label> <a href="" id="term">Términos y condiciones.</a><br /><br>
                        <div style="float: left">
                            <form>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input title="Ejecutar pago desde computador" name="btnPtPS" id="btnPtPS" type="button" value="Pagar ahora" style="display: none" onclick="window.open('{$document_root}modules/sales/placeToPay/pPaymentPtoP.php?serial_inv={$inserted_serial_inv}','_blank')" disabled/>
                            </form>
                        </div>
                        <div style="float: left">
                            <form>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input title="Ejecutar pago desde celular" name="btnPtPM" id="btnPtPM" type="button" value="Pago desde dispositivo cliente" onclick="window.open('{$document_root}modules/sales/placeToPay/pSendMailPaymentPtoP.php?serial_inv={$inserted_serial_inv}&mail=true','_blank')" disabled/>
                            </form>
                        </div>
                    </div>
				{/if}	
				 {/if}			
        {else}
            <div class="error center">
                {if $error eq 2}
                No se pudo registrar el nuevo cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 3}
                No se pudo actualizar el cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 4}
                Los cambios en el cliente se efectuaron exitosamente, pero hubo errores en el formulario de preguntas por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 5}
                Los cambios en el cliente se efectuaron exitosamente, pero hubo errores al calcular la fecha l&iacute;mite por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 6}
                No se puede facturar para este cliente debido a que no existe un documento designado para este tipo de cliente en su pa&iacute;s.
                {elseif $error eq 7}
                Los cambios en el cliente se efectuaron exitosamente, pero hubo errores en la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 8}
                La facturaci&oacute;n se realiz&oacute; exitosamente. Sin embargo, no se pudo actualizar el n&uacute;mero de factura ni la venta,<br> comuniquese con el administrador.
                {elseif $error eq 9}
                Hubo errores al setear las ventas como facturadas.
                {elseif $error eq 10}
                Hubo errores al enviar el mail al cliente con la contrase&ntilde;a generada.
				{elseif $error eq 11}
				La Factura #{$inserted_serial_inv} ya fue impresa.
				{elseif $error eq 12}
					No se puede procesar la factura debido a que no existe conexi&oacute;n con el ERP. Por favor vuelva a Intentarlo.
				{elseif $error eq 13}
					No se puede procesar la factura debido a que el ERP no cuenta con la misma cantidad de ventas seleccionadas.
                {/if}
            </div>
        {/if}
    </div>
 {/if}
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
 {if $countryList}
    <div class="span-24 last line"></div>
     <div class="span-24 last line">
         <div class="prepend-3 span-4 label">*Pa&iacute;s:</div>
        <div class="span-5"> <select name="selCountry" id="selCountry">
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $countryList|@count eq 1} selected{/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">*Representante:</div>
        <div class="span-5 append-3 last" id="managerContainer">
        	<select name="selManager" id="selManager">
                    <option value="">- Seleccione -</option>
                </select>
         </div>
   </div>

    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Responsable:</div>
    	<div class="span-5" id="responsableContainer">
        	<select name="selResponsable" id="selResponsable">
            	<option value="">- Seleccione -</option>
            </select>
        </div>

        <div class="span-3 label">Categor&iacute;a:</div>
        <div class="span-5 append-3">
        	<select name="selCategory" id="selCategory">
            	<option value="">- Seleccione -</option>
                {if $categoryList}
                    {foreach from=$categoryList item="l"}
                        <option value="{$l}">{$l}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
   </div>
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Tipo:</div>
    	<div class="span-5">
        	<select name="selType" id="selType">
            	<option value="">- Seleccione -</option>
                {if $typeList}
                    {foreach from=$typeList item="l"}
                        <option value="{$l.serial_dlt}">{$l.name}</option>
                    {/foreach}
                {/if}
            </select>
        </div>

        
        <div class="span-5 prepend-3">
            <input type="button" id="searchBranches" value="Buscar"/>
        </div>
   </div>
    <div class="span-24 last line" id="dealerContainer">
        
    </div>
   <div class="span-24 last line" id="salesContainer">

    </div>
    {else}
    <div class="span-24 last line" align="center">No existen ventas por facturar.</div>
    {/if}


</form>

<textarea id="customPopUp2" style="display:none;">
		<div class="span-19 center">
		    <strong>Términos y condiciones</strong>
		</div>
		<div class="center">
		<p align="justify">CONDICIONES DE USO www.bluecard.com.ec Bienvenido a BlueCard. Si usted visita o compra a trav&eacute;s de la p&aacute;gina web de BlueCard est&aacute; aceptando las condiciones de la misma. Por favor lea detenidamente cada vez que usted solicite, ahora o en un futuro nuestros servicios, cada una de las tarjetas de asistencia en viajes est&aacute; sujeta a t&eacute;rminos y condiciones especiales que son aceptadas al momento de la compra.  COMUNICACIONES ELECTR&Oacute;NICAS  Cuando visita nuestro sitio web, usted est&aacute; comunic&aacute;ndose de manera electr&oacute;nica. Por lo cual usted est&aacute; consiente en recibir de nuestra parte correos electr&oacute;nicos de avisos, divulgaciones, acuerdos, promociones y otras comunicaciones estando de acuerdo en que estas satisfacen cualquier requerimiento legal en comunicaciones escritas.  COPYRIGHT Todo el contenido incluido en este sitio, como texto, gr&aacute;ficos, logotipos, iconos, im&aacute;genes, clips de audio, descargas digitales, compilaciones de datos, c&oacute;digo de fuente y software, es de propiedad de BLUE CARD basado en las leyes internacionales de Copyright.  LICENCIA Y ACCESO AL SITIO  BLUECARD le otorga una licencia limitada para acceder y hacer uso personal de este sitio y no descargar o modificar cualquier porci&oacute;n de &eacute;l, excepto con el consentimiento expreso y por escrito de BLUECARD.  Esta licencia no incluye cualquier reventa o uso comercial del sitio o de su contenido, la colecci&oacute;n y uso de cualquier listado de planes, descripciones o precios; cualquier uso derivado de este sitio o su contenido; cualquier descarga o copiado de informaci&oacute;n de la cuenta para el beneficio de otro comerciante, o cualquier uso de miner&iacute;a de datos, robots o similares recopilaciones de datos y herramientas de extracci&oacute;n. Este sitio o cualquier porci&oacute;n de &eacute;ste no puede ser reproducido, duplicado, copiado, vendido, revendido, visitado o explotado con fines comerciales sin consentimiento expreso y por escrito de BLUECARD. Usted no puede enmarcar o utilizar t&eacute;cnicas de enmarcado para incluir cualquier marca, logo u otra informaci&oacute;n propietaria (incluyendo im&aacute;genes, textos, dise&ntilde;o de p&aacute;gina, o forma) de BLUECARD sin consentimiento expreso y por escrito. No puede utilizar cualquier etiqueta o cualquier otro &quot;texto oculto&quot; utilizando el nombre de BLUECARD o marcas comerciales sin el consentimiento expreso y por escrito de BLUECARD. Cualquier uso no autorizado termina el permiso o licencia otorgado por BLUECARD. Se le concede una licencia limitada, revocable, y el derecho no exclusivo para crear un hiperv&iacute;nculo a la p&aacute;gina principal de BLUECARD, siempre y cuando el v&iacute;nculo no represente los planes o servicios de BLUECARD, de una manera falsa, enga&ntilde;osa, despectiva o de manera ofensiva. No puede utilizar ning&uacute;n logotipo de BLUECARD o cualquier otro gr&aacute;fico patentado o marca registrada como parte del enlace sin permiso expreso por escrito.  SU CUENTA  Si usted utiliza este sitio, es responsable de mantener la confidencialidad de su cuenta, su contrase&ntilde;a y de restringir el acceso a su computadora, y usted est&aacute; de acuerdo en aceptar la responsabilidad de todas las actividades que ocurran bajo su cuenta o contrase&ntilde;a. BLUECARD se reserva el derecho de rechazar prestar el servicio, cerrar cuentas, retirar o editar contenidos, o cancelar pedidos a su entera discreci&oacute;n.  INFORMACI&Oacute;N INGRESADA  BLUECARD no se responsabiliza por el mal ingreso de informaci&oacute;n de documentos de identidad, pasaportes, nombres, fechas de nacimiento, direcciones, tel&eacute;fonos, contactos y direcciones de correo electr&oacute;nico. Usted no puede utilizar una direcci&oacute;n falsa de correo electr&oacute;nico, suplantar a cualquier persona o entidad, o de otra manera inducir a un error en cuanto a la procedencia de una tarjeta u otro contenido. BLUECARD se reserva el derecho (pero no la obligaci&oacute;n) de eliminar o editar dicho contenido.  RIESGO DE P&Eacute;RDIDA  Al realizar una compra virtual usted recibir&aacute; un n&uacute;mero &uacute;nico de contrato v&iacute;a correo electr&oacute;nico, el cual est&aacute; registrado en nuestro sistema para el momento que usted requiera una asistencia. De darse el caso que usted no reciba el correo electr&oacute;nico puede solicitarlo a trav&eacute;s de nuestra p&aacute;gina web www.bluecard.com.ec en el men&uacute; cont&aacute;ctenos.</p>

<p>PLANES  BLUECARD tiene especificaciones y coberturas muy definidas dentro de sus planes, por lo tanto BLUECARD se asegura de cumplir con las especificaciones definidas para el plan adquirido dentro de las condiciones generales especificadas para el servicio brindado.  Planes Tradicionales.- Turista, Europa Plus, Senior, Platino, Estudiantil, Deportes y Competencias. Planes Elites.-Turista Elite, Usa Elite, Europa Elite, Platino Elite, Senior Elite, Estudiantil Elite, Estudiantil Elite Premium, Ejecutiva Premium, Deportes y Competencias Elite, Precompra Elite, Precompra Elite Primium. PRECIO  Excepto cuando se indique lo contrario, en nuestra p&aacute;gina web se muestra la lista de precios para los planes, estos representan el precio de venta incluido impuestos.  CAMBIOS DE FECHA Y STAND BY  Como parte de nuestro servicio al cliente brindamos la posibilidad de colocar su tarjeta en Stand by y realizar cambios en las fechas de viaje, amparados en las siguientes pol&iacute;ticas de BLUECARD. &bull;Para solicitar cambios en el estado de una tarjeta a stand by o realizar cambios en las fechas de cobertura de la misma, el cliente debe notificar con al menos 72 horas de anticipaci&oacute;n al inicio de la vigencia de la tarjeta, v&iacute;a correo electr&oacute;nico o a trav&eacute;s de las comunicaciones brindadas en nuestra p&aacute;gina web. (Cont&aacute;ctenos, Chat Support, Skype), el operador los podr&aacute; realizar siempre y cuando no haya iniciado cobertura. &bull;El estado Stand by de una tarjeta tiene una duraci&oacute;n de 180 d&iacute;as a partir de la fecha de inicio del primer viaje, luego de &eacute;sta, se caducar&aacute; autom&aacute;ticamente.  ANULACI&Oacute;N DE TARJETAS Y REEMBOLSOS BLUECARD buscar&aacute; siempre brindar el mejor servicio de calidad a sus clientes por tal motivo en compras realizadas a trav&eacute;s de nuestra p&aacute;gina web se aplican las siguientes pol&iacute;ticas de anulaci&oacute;n y reembolsos:  &bull; No se permite la anulaci&oacute;n de un contrato por fallas en los datos ingresados, ya que es responsabilidad del cliente el ingresar su informaci&oacute;n de la manera indicada.  &bull; Bajo ning&uacute;n concepto se proceder&aacute; con solicitudes de anulaci&oacute;n o reembolso de contratos con fecha posterior al inicio de su vigencia.  &bull; El cliente podr&aacute; solicitar el reembolso siempre y cuando notifique con 72 horas de anticipaci&oacute;n al inicio de la vigencia de su contrato, y solamente en casos de negaci&oacute;n de visa, y por causas de fuerza mayor. En estos casos BLUECARD reembolsar&aacute; el 70% del monto cancelado.  &bull; Toda persona que solicite un reembolso y se le haya enviado el contrato BlueCard deber&aacute; enviar la siguiente documentaci&oacute;n necesaria para tramitar el reintegro de los valores cancelados:   * Contrato original de BlueCard.  * Original de la carta de negativa de visa de la Embajada o Consulado con la firma y sello en la misma. Se debe solicitar a las embajadas que se realice la autentificaci&oacute;n de los documentos con firma y sello a color para evitar la falsificaci&oacute;n de las mismas.  * Copia a color del pasaporte con los sellos de recepci&oacute;n por parte de la Embajada o Consulado a la cual se solicit&oacute; el visado.  POL&Iacute;TICAS DEL SITIO, MODIFICACI&Oacute;N Y SEPARABILIDAD  Por favor, revise nuestras pol&iacute;ticas, condiciones generales, condiciones particulares y precios en este sitio. Estas pol&iacute;ticas tambi&eacute;n gobiernan en su visita a BLUECARD. Nos reservamos el derecho de hacer cambios en el sitio, sus pol&iacute;ticas, y las Condiciones de Uso en cualquier momento. Si cualquiera de estas condiciones se considerar&aacute; inv&aacute;lida, nula o inaplicable por cualquier raz&oacute;n, la condici&oacute;n se considerar&aacute; separable y no afectar&aacute; a la validez y la aplicabilidad de su contrato.  NUESTRA DIRECCI&Oacute;N  Gonzalo Serrano N37-13 y Jos&eacute; Correa. Quito &ndash; Ecuador  Este sitio y condiciones de uso, &copy; 2010 Blue Card. Todos los derechos reservados. </p>
		<p align="justify">
		TÉRMINOS Y CONDICIONES DE PAGO

                Debo y pagaré incondicionalmente sin protesto al Emisor de la tarjeta de crédito el total de los valores expresados en esta

                    Autorización de Orden de Cargo, en el lugar y fecha que se reconvenga. En caso de mora pagaré la tasa máxima autorizada

                    para el emisor de la tarjeta de crédito y las tarifas que esta institución establezca por gestiones de la cartera vencida.





                Eximo al emisor de la tarjeta de crédito, de cualquier responsabilidad por los valores reportados por BLUECARD Por lo

                    cual desde ya renuncio a cualquier reclamación y a iniciar cualquier acción legal en contra de las indicadas Instituciones, las

                    mismas que no requerirán de otro instrumento o documento para procesar en mi tarjeta de crédito, los valores generados,

                    los mismos que desde ya los acepto y reconozco como obligación.



                Me comprometo expresamente a enviar comunicación escrita con 60 días de anticipación, tanto a BLUECARD como a la oficina

                    Matriz del Emisor, en caso de revocación de la presente autorización, de lo contrario se entenderá vigente la orden de cargo

                    expresada por los servicios antes determinados.



                De igual manera autorizo que en caso de pérdida, hurto, robo o cualquier circunstancia por el que fuera cambiado el número

                    de la tarjeta de crédito antes singularizada, se cuenta con el nuevo número que se me asigne para efectuar todos los pagos

                    de manera incondicional dentro del período correspondiente, de tal manera que el cambio del número indicado no sea

                    causa para no cancelar los valores que adeude.





                Adicionalmente, declaro libre y voluntariamente que el Plan de Asistencia en Viajes solicitado a la compañía BLUECARD S.A,

                    ampara bienes/servicios de procedencia lícita y que los mismos no están ligados con actividades de narcotráfico, lavado de activos o

                    financiamiento del terrorismo; igualmente declaro que los fondos con los cuales se pagará la prima de este seguro tienen

                    origen lícito y no guarda ninguna relación con las actividades mencionadas anteriormente.



                Declaro que la información contenida en este formulario es verdadera, completa y proporciona la información de modo

                    confiable y actualizado; además declaro conocer y aceptar que es mi obligación actualizar anualmente mis datos personales,

                    así como el comunicar y documentar de manera inmediata a BLUECARD S.A cualquier cambio en la información que hubiere

                    proporcionado. Durante la vigencia de la relación con la compañía me comprometo a proveer de la documentación e

                    información que me sea solicitada.





                Acepto y certifico que estoy consciente del producto o servicio contratado y de que he leído detalladamente los términos y

                    condiciones a ser aplicados, por lo que los acepto libre y voluntariamente.





                ACUERDO DE RESPONSABILIDAD Y USO DE MEDIOS ELECTRÓNICOS



                El Cliente, a través de la suscripción de este acuerdo, manifiesta estar interesado en utilizar los mecanismos virtuales

                    establecidos, tales como pero no limitados a, correo electrónico, mensajes SMS, aplicativo móvil, redes sociales y el portal

                    web, para lo cual expresa su voluntad de utilizar de manera preferente el medio de notificaciones a través de la Internet,

                    en el buzón personal del correo electrónico proporcionado y otorgando su consentimiento para ello.

                    El Cliente conoce y acepta expresamente que la suscripción de este acuerdo no impide a BLUECARD, cuando las circunstancias

                    así lo requieran,realizar la notificación al Cliente por los otros medios establecidos en la ley o en este acuerdo.



                Responsabilidades del Cliente y/o Comercializador





                    El Cliente y/o Comercializador asume la responsabilidad total del uso, tanto de la clave de usuario, así como de la veracidad de la

                        información para la suscripción, renovación y gestión de contratos de Asistencia en Viajes y acceso a los

                        servicios que BLUECARD ponga a su disposición a través de Internet y aplicativos móviles, por lo que en caso de mal

                        uso, pérdida o disposición arbitraria de su clave personal, el Cliente libera de toda responsabilidad sobre los efectos,

                        consecuencias y/o daños y perjuicios tanto patrimoniales como morales que pueda sufrir.



                    El Cliente y/o Comercializador acepta que todas las transacciones realizadas a través de Internet o aplicativo móvil se garantizarán

                        mediante la clave de usuario del cliente y de ella se derivarán todas las responsabilidades de carácter comercial que

                        hoy se desprenden de la firma autógrafa, según señala la “Ley de Comercio Electrónico, Firmas Electrónicas y

                        Mensajes de Datos”, y en base al principio de libertad tecnológica estipulado en el mismo cuerpo legal, las partes

                        acuerdan que la clave proporcionada por BLUECARD al cliente, surtirá los mismos efectos que una firma electrónica,

                        por lo que, tanto su funcionamiento como su aplicación se entenderán como una completa equivalencia funcional,

                        técnica y jurídica.



                    El Cliente y/o Comercializador declara conocer y aceptar que las contraseñas personales constituyen uno de los componentes básicos

                        de la seguridad del proceso de contratación y utilización de los mecanismos virtuales que BLUECARD pone a

                        disposición de sus afiliados; la validez jurídica de la firma, y la seguridad en el acceso a las evidencias y otros datos

                        personales, y deben por tanto estar especialmente protegidas.



                    El Cliente y/o Comercializador declara conocer que la contraseña como llaves de acceso al sistema, deben ser estrictamente

                        confidenciales y personales, y cualquier incidencia que comprometa su confidencialidad debe ser inmediatamente

                        comunicada a BLUECARD para que sea subsanada en el menor plazo de tiempo posible, en caso de que esto no suceda

                        o el Cliente sea objeto de delitos informáticos producto de su falta de cuidado en el manejo de esta información y

                        sus claves, el Cliente libera de responsabilidad sobre los efectos, consecuencias y/o daños y perjuicios tanto

                        patrimoniales como morales que pueda sufrir.
</p>


		</div>
		<div class="span-12">
		</div>
</textarea>

<textarea id="customPopUp3" style="display:none;">
		<div class="span-19 center"><strong>Preguntas frecuentes</strong></div>
		<div class="center">
		<p align="justify">
		<p style="font-weight: bold">¿Qué es PlacetoPay el Boton de Pagos de BlueCard?</p>
		 PlacetoPay es la plataforma de pagos electrónicos que procesa en línea las transacciones generadas (Facturas - Contratos de Viajero) en BAS (BlueAssist System) con las formas de pago habilitadas para tal fin.
		<p style="font-weight: bold">¿Cómo puedo pagar?</p>
		  En el BAS usted podrá realizar su pago utilizando: Tarjetas de Crédito de las siguientes franquicias: Diners, Visa y MasterCard; de todos los bancos en lo que corresponde a pago corriente y en cuanto a diferido, únicamente por el momento las tarjetas emitidas por Banco Pichincha, Loja, BGR y Machala.
		<p style="font-weight: bold">¿Puedo realizar el pago cualquier día y a cualquier hora?</p>
		Sí, en nuestro sistema BAS podrás realizar tus compras en línea los 7 días de la semana, las 24 horas del día a sólo un clic de distancia.
		<p style="font-weight: bold">¿Puedo cambiar la forma de pago?</p>
		 Si aún no has finalizado tu pago, podrás volver al paso inicial y elegir la forma de pago que prefieras. Una vez finalizada la compra no es posible cambiar la forma de pago.
		<p style="font-weight: bold">¿Es seguro ingresar mis datos bancarios en este sitio web?</p>
		   Para proteger tus datos BlueCard delega en PlacetoPay la captura de la información sensible. Nuestra plataforma de pagos cumple con los estándares exigidos por la norma internacional PCI DSS de seguridad en transacciones con tarjeta de crédito. Además tiene certificado de seguridad SSL expedido por GeoTrust una compañía Verisign, el cual garantiza comunicaciones seguras mediante la encriptación de todos los datos hacia y desde el sitio; de esta manera te podrás sentir seguro a la hora de ingresar la información de su tarjeta.



                        Durante el proceso de pago, en el navegador se muestra el nombre de la organización autenticada, la autoridad que lo certifica y la barra de dirección cambia a color verde. Estas características son visibles de inmediato, dan garantía y confianza para completar la transacción en PlacetoPay.



                        PlacetoPay - BlueCard también cuenta con el monitoreo constante de McAfee Secure y la firma de mensajes electrónicos con Certicámara.
		<p style="font-weight: bold">¿Pagar electrónicamente tiene algún valor para mí como comprador?</p>
		 No, los pagos electrónicos realizados a través de PlacetoPay - BlueCard no generan costos adicionales para el comprador.
		 <p style="font-weight: bold">¿Qué debo hacer si mi transacción no concluyó?</p>
		    En primera instancia deberás revisar si llegó un mail de confirmación del pago en tu cuenta de correo electrónico (la inscrita en el momento de realizar el pago), en caso de no haberlo recibido, deberás contactar a soporte@placetopay.com para confirmar el estado de la transacción.
		<p style="font-weight: bold">¿Qué debo hacer si no recibí el comprobante de pago?</p>
		 Por cada transacción aprobada a través de PlacetoPay - BlueCard, recibirás un comprobante del pago con la

                        referencia de compra en la dirección de correo electrónico que indicaste al momento de pagar.

                        Si no lo recibes, podrás contactar a soporte@placetopay.com, para solicitar el reenvío del

                        comprobante a la misma dirección de correo electrónico registrada al momento de pagar.



                        Para mayor información comuníquese con nosotros al 593-2-3332253 o a través de nuestro correo electrónico: info@bluecard.com.ec
            </p>
		</div>
</textarea>