<!--/*
File: fChooseSales.es.tpl
Author: Nicolas Flores
Creation Date: 02/03/2010
Last Modified: 
Modified by: 
*/-->


{assign var="title" value="SELECCION DE VENTAS A FACTURAR"}
<form name="frmChooseSales" id="frmChooseSales" method="post" action="{$document_root}modules/invoice/fNewInvoice">
	<div class="span-24 last title">
    	Selecci&oacute;n de Ventas a Facturar<br />
        <label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-7 span-10 prepend-7 last " id="messageContainer">
    	{if $error eq 1}
           <div class="success" align="center">
            	Ventas Facturadas<br/>
				{if $inserted_serial_inv}<a href="{$document_root}modules/invoice/pPrintInvoice/{$inserted_serial_inv}" target="_blank">Imprimir Factura</a>{/if}
            </div>
        {else}
            <div class="error center">
                {if $error eq 2}
                No se pudo registrar el nuevo cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 3}
                No se pudo actualizar el cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 4}
                Los cambios en el cliente se efectuaron exitosamente, pero hubo errores en el formulario de preguntas por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 5}
                Los cambios en el cliente se efectuaron exitosamente, pero hubo errores al calcular la fecha l&iacute;mite por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 6}
                No se puede facturar para este cliente debido a que no existe un documento designado para este tipo de cliente en su pa&iacute;s.
                {elseif $error eq 7}
                Los cambios en el cliente se efectuaron exitosamente, pero hubo errores en la facturaci&oacute;n, comuniquese con el administrador.
                {elseif $error eq 8}
                La facturaci&oacute;n se realiz&oacute; exitosamente. Sin embargo, no se pudo actualizar el n&uacute;mero de factura ni la venta,<br> comuniquese con el administrador.
                {elseif $error eq 9}
                Hubo errores al setear las ventas como facturadas.
                {elseif $error eq 10}
                Hubo errores al enviar el mail al cliente con la contrase&ntilde;a generada.
				{elseif $error eq 11}
				La Factura #{$inserted_serial_inv} ya fue impresa.
				{elseif $error eq 12}
					No se puede procesar la factura debido a que no existe conexi&oacute;n con el ERP. Por favor vuelva a Intentarlo.
				{elseif $error eq 13}
					No se puede procesar la factura debido a que el ERP no cuenta con la misma cantidad de ventas seleccionadas.
                {/if}
            </div>
        {/if}
    </div>
 {/if}
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
 {if $countryList}
    <div class="span-24 last line"></div>
     <div class="span-24 last line">
         <div class="prepend-3 span-4 label">*Pa&iacute;s:</div>
        <div class="span-5"> <select name="selCountry" id="selCountry">
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $countryList|@count eq 1} selected{/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">*Representante:</div>
        <div class="span-5 append-3 last" id="managerContainer">
        	<select name="selManager" id="selManager">
                    <option value="">- Seleccione -</option>
                </select>
         </div>
   </div>

    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Responsable:</div>
    	<div class="span-5" id="responsableContainer">
        	<select name="selResponsable" id="selResponsable">
            	<option value="">- Seleccione -</option>
            </select>
        </div>

        <div class="span-3 label">Categor&iacute;a:</div>
        <div class="span-5 append-3">
        	<select name="selCategory" id="selCategory">
            	<option value="">- Seleccione -</option>
                {if $categoryList}
                    {foreach from=$categoryList item="l"}
                        <option value="{$l}">{$l}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
   </div>
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Tipo:</div>
    	<div class="span-5">
        	<select name="selType" id="selType">
            	<option value="">- Seleccione -</option>
                {if $typeList}
                    {foreach from=$typeList item="l"}
                        <option value="{$l.serial_dlt}">{$l.name}</option>
                    {/foreach}
                {/if}
            </select>
        </div>

        
        <div class="span-5 prepend-3">
            <input type="button" id="searchBranches" value="Buscar"/>
        </div>
   </div>
    <div class="span-24 last line" id="dealerContainer">
        
    </div>
   <div class="span-24 last line" id="salesContainer">

    </div>
    {else}
    <div class="span-24 last line" align="center">No existen ventas por facturar.</div>
    {/if}


</form>
