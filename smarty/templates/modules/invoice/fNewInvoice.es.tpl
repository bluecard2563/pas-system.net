<!--/*
File: fNewInvoice.es.tpl
Author: Nicolas Flores
Creation Date: 09/03/2010
Last Modified:
Modified by:
*/-->

{assign var="title" value="NUEVA FACTURA"}
<form name="frmInvoice" id="frmInvoice" method="post" action="{$document_root}modules/invoice/pNewInvoice">
    <div class="span-24 last title">
        Nueva Factura<br />
        <label>(*) Campos Obligatorios</label><br><br>
        <label>El documento a utilizar se seleccionar&aacute; de acuerdo<br> al tipo de persona a la cual se le facturar&aacute;. </label>
    </div>
    {if $error}
        <div class="append-7 span-10 prepend-7 last " id="messageContainer">
            {if $error eq 1}
                <div class="span-12 success" align="center">
                    Ventas Facturadas
                </div>
            {else}
                <div class="span-12 error" align="center">
                    {if $error eq 2}
                        No se pudo registrar el nuevo cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                    {elseif $error eq 3}
                        No se pudo actualizar el cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                    {elseif $error eq 4}
                        Los cambios en el cliente se efectuaron exitosamente, pero hubo errores en el formulario de preguntas por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                    {elseif $error eq 5}
                        Los cambios en el cliente se efectuaron exitosamente, pero hubo errores al calcular la fecha l&iacute;mite por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                    {elseif $error eq 6}
                        Este cliente no puede facturar debido a que no existe un documento designado para este tipo de cliente en su pa&iacute;s.
                    {elseif $error eq 7}
                        Los cambios en el cliente se efectuaron exitosamente, pero hubo errores en la facturaci&oacute;n, comuniquese con el administrador.
                    {elseif $error eq 8}
                        No se pudo actualizar el n&uacute;mero de factura, comuniquese con el administrador.
                    {elseif $error eq 9}
                        Hubo errores al setear las ventas como facturadas.
                    {elseif $error eq 10}
                        Hubo errores al enviar el mail al cliente con la contrase&ntilde;a generada.
                    {/if}
                </div>
            {/if}
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Factura:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">*Comercializador:</div>
        <div class="span-5">
            <input type="text" id="txtDealer" name="txtDealer" readonly="readonly" value="{$branchData.name_dea}" title="El campo 'Comercializador' es obligatorio"/>
        </div>

        <div class="span-3 label">*Factura No.:</div>
        <div class="span-5 append-3 last" id="invoiceNumberContainer">
            <input type="text" id="txtInvoiceNumber" name="txtInvoiceNumber" {if $managerData.invoice_number_mbc eq AUTOMATIC}readonly="readonly"{/if} title="El campo 'Factura No.' es obligatorio"/>
            <input type="hidden" id="hdnSerialDcn" name="hdnSerialDcn" value=""/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">*Fecha:</div>
        <div class="span-5">
            <input type="text" id="txtDate" name="txtDate" readonly="readonly" value="{$branchData.today}" title="El campo 'Fecha' es obligatorio"/>
        </div>

        <div class="span-3 label">*Total Venta(s)</div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtSalesTotal" name="txtSalesTotal" readonly="readonly" value="{$salesTotal}" title="El campo 'Total Venta(s)' es obligatorio"/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">*Descuento (%):</div>
        <div class="span-5">
            <input type="text" id="txtDiscount" name="txtDiscount" readonly="readonly" value="{if $branchData.type_percentage_dea eq DISCOUNT && $productHasComision eq 'YES'}{$branchData.percentage_dea}{else}0{/if}" title="El campo 'Descuento (%)' es obligatorio"/>
        </div>

        <div class="span-3 label">*SubTotal:</div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtSubTotal" name="txtSubTotal" readonly="readonly" value="{$totalAfterDiscount}" title="El campo 'SubTotal' es obligatorio"/>
            <input type="hidden" id="h_TotalCost" name="h_TotalCost" value="{$salesCost}"/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Otro Descuento (%):</div>
        <div class="span-5">
            <input type="text" {if $existOtherDiscount eq 1}readonly="readonly"{/if} id="txtOtherDiscount" name="txtOtherDiscount"/>
        </div>

        <div class="span-3 label">*Total:</div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtTotal" name="txtTotal" readonly="readonly" value="{$total}" title="El campo 'Total' es obligatorio"/>
            <input type="hidden" id="h_taxesToApply" name="h_taxesToApply"  value="{$taxesToApply}"/>
            <input type="hidden" id="h_Total" name="h_Total"  value="{$total}"/>
        </div>
    </div>

    {if $agreementList}
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Descuento Convenio (%):</div>
        <div class="span-1 append-1 last">
            <select name="selAgreement" id="selAgreement" onchange="setAgreementDiscountToInvoice(this);">
                <option value="">- Seleccione -</option>
                {foreach from=$agreementList item="agreement"}
                    <option value="{$agreement.discount_value_agr}" data-id="{$agreement.serial_agr}"{if $agreement.serial_agr eq $agreement.serial_cit}selected{/if}>{$agreement.name_agr}</option>
                {/foreach}
            </select>
            <input type="hidden" name="hdnAgreementId" id="hdnAgreementId" value="" />
        </div>
    </div>
    {/if}

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Observaciones:</div>
        <div class="span-13">
            <textarea id="txtObservations" name="txtObservations" cols="30" rows="5">{if $existOtherDiscount eq 1}PAS Cup&oacute;n Aplicado{/if}</textarea>
        </div>
    </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">Orden de Compra: </div>
            <div class="span-5">
                <input type="text" id="txtSalesOrder" name="txtSalesOrder"/>
            </div>
            {if $observations}
                <div class="span-3 label">Observaciones de la Venta:</div>
                <div class="span-5" style="text-align: justify; color: #E51937; font-style: italic;">
                    {$observations}
                </div>
            {/if}
        </div>

    {if $taxes}
        <div class="span-24 last line separator">
            <label>Impuestos Aplicados:</label>
        </div>
        <div class="span-24 last line">
            <center>
                <table border="0" id="salesTable" style="width: 150px;">
                    <THEAD>
                        <tr bgcolor="#284787">
                            <td align="center" class="tableTitle">
                                Impuesto
                            </td>
                            <td align="center" class="tableTitle">
                                Porcentaje
                            </td>

                        </tr>
                    </THEAD>
                    <TBODY>
                        {foreach name="taxes" from=$taxes item="s" key="k"}
                            <tr {if $smarty.foreach.taxes.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                                <td align="center" class="tableField">
                                    {$s.name_tax}
                                </td>
                                <td align="center" class="tableField">
                                    {$s.percentage_tax}
                                </td>

                            </tr>
                        <input type="hidden" id="hdnNameTax_{$k}" name="hdnNameTax[]" value="{$s.name_tax}">
                        <input type="hidden" id="hdnPercentageTax_{$k}" name="hdnPercentageTax[]" value="{$s.percentage_tax}" />
                    {/foreach}
                    </TBODY>
                </table>
            </center>
        </div>
    {/if}



    <div class="span-24 last line separator">
        <div class="span-5">
            <label>Factura a nombre de:</label>
        </div>
        <div class="prepend-13 span-6 last" style="padding-top:10px;">
            <label for="radio_customer" style="color: #000000;">Cliente:</label>
            <input type="radio" id="radio_customer" name="bill_to_radio" value="CUSTOMER" {if $branchData.bill_to_dea eq 'CUSTOMER'}checked="checked"{/if}/>
            <label for="radio_dealer" style="color: #000000;">Comercializador:</label>
            <input type="radio" id="radio_dealer" name="bill_to_radio" value="DEALER" {if $branchData.bill_to_dea eq 'DEALER'}checked="checked"{/if}/>
        </div>
    </div>


    <div class="span-24 last line" id="customerData" {if $branchData.bill_to_dea eq 'DEALER'}style="display:none;"{/if}>
        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">* Documento:</div>
            <div class="span-5">
                <input type="text" name="txtCustomerDocument" id="txtCustomerDocument" title='El campo "Documento" es obligatorio.'value="{$customerData.document_cus}">
            </div>

            <div class="span-3 label">* Tipo de Documento:</div>
            <div class="span-6 last">
                {if $validate_holder_document eq 'NO'}
                    {if $customerData}
                        {if $customerData.type_cus eq 'PERSON'}
                            Persona Natural
                        {elseif $customerData.type_cus eq 'LEGAL_ENTITY'}
                            Persona Jur&iacute;dica
                        {/if}
                    {else}
                        <select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.' >
                            <option value="">- Seleccione -</option>
                            {foreach from=$typesList item="l"}
                                <option value="{$l}" {if $l eq $customerData.type_cus}selected{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                            {/foreach}
                        </select>
                    {/if}
                {else}
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.type_cus eq 'LEGAL_ENTITY'}checked{/if} /> RUC					
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
                    <input type="hidden" name="selType" id="selType" value="{$customerData.type_cus}" />
                {/if}				
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label" id="labelName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}*Razo&oacute;n Social: {else}*Nombre:{/if}</div>
            <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$customerData.first_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

            <div id="lastNameContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
                <div class="span-3 label">*Apellido:</div>
                <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$customerData.last_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
            </div>                
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus|htmlall}">
            </div>

            <div class="span-3 label">*Tel&eacute;fono Principal:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
            <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"  value="{$customerData.phone2_cus}"/></div>

            <div class="span-3 label">Correo Electr&oacute;nico:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtEmail" id="txtEmail" title='El campo "Correo Electr&oacute;nico" es obligatorio' value="{$customerData.email_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
            <div class="span-5"> <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.auxData.serial_cou}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select> </div>

            <div class="span-3 label">*Ciudad:</div>
            <div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                </select> 
            </div>
        </div>
    </div>

    <div class="span-24 last line" id="dealerData" {if $branchData.bill_to_dea eq 'CUSTOMER'}style="display:none;"{/if}>
        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Id:</div>
            <div class="span-5">
                {$branchData.id_dea}
                <input type="hidden" name="hdnDealerDocument" id="hdnDealerDocument" value="{$branchData.id_dea}" />
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Nombre:</div>
            <div class="span-5">
                {$branchData.name_dea}
            </div>

            <div class="span-3 label">Direcci&oacute;n</div>
            <div class="span-6 last">
                {$branchData.address_dea}
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
            <div class="span-5">
                {$branchData.phone1_dea}
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-2 last">
                {$branchData.phone2_dea}
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Correo Electr&oacute;nico:</div>
            <div class="span-5">
                {$branchData.email2_dea}
            </div>
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input id="btnRegister" type="submit" value="Registrar" name="btnRegister"/>
        <input type="hidden" name="hdnSerialMan" id="hdnSerialMan" value="{$managerData.serial_man}" />
        <input type="hidden" name="hdnSerialMbc" id="hdnSerialMbc" value="{$managerData.serial_mbc}" />
        <input type="hidden" name="hdnSerialDea" id="hdnSerialDea" value="{$branchData.serial_dea}" />
        <input type="hidden" name="hdnSerialCdd" id="hdnSerialCdd" value="{$branchData.serial_cdd}" />
        <input type="hidden" name="hdnTypePercentage" id="hdnTypePercentage" value="{$branchData.type_percentage_dea}" />
        <input type="hidden" name="hdnComissionValue" id="hdnComissionValue" value="{if $branchData.type_percentage_dea eq COMISSION}{$branchData.percentage_dea}{else}0{/if}"/>
        <input type="hidden" name="hdnManagerRights" id="hdnManagerRights" value="{$manager_rights.manager_rights_dea}" />
        <input type="hidden" name="hdnManagerRightsComission" id="hdnManagerRightsComission" value="{$manager_rights.percentage_dea}" />
        <input type="hidden" name="hdnValidateHolderDocument" id="hdnValidateHolderDocument" value="{$validate_holder_document}" />		
        <input type="hidden" name="hdnComission" id="hdnComission" value="{$branchData.commission}" />
        <input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
        <input type="hidden" name="hdnTypeDbc" id="hdnTypeDbc" value="{if $branchData.bill_to_dea eq 'DEALER' OR $customerData.type_cus eq 'LEGAL_ENTITY'}LEGAL_ENTITY{elseif $customerData.type_cus eq 'PERSON'}PERSON{/if}" />
        <input type="hidden" name="hdnSerialCus" id="hdnSerialCus" value="{$customerData.serial_cus}" />
        <input type="hidden" name="hdnCustomerAction" id="hdnCustomerAction" value="{if $customerData}UPDATE{else}INSERT{/if}" />

        {foreach from=$salesIds item="s" key="k"}
            <input type="hidden" name="hdnSaleId[{$k}]" id="hdnSaleId_{$k}" value="{$s}" />
        {/foreach}
    </div>  

</form>
