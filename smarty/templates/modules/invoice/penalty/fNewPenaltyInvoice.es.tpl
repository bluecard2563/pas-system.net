{assign var="title" value="ANULACI&Oacute;N POR PENALIDAD"}
<div class="span-24 last title">
	Anulaci&oacute;n por Penalidad<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
 <div class="append-7 span-10 prepend-7 last">
	 <div class="span-10 {if $error eq 1}success{else}error{/if}" id="confirmDiv" align="center">
		{if $error eq 1}
			La tarjeta se ha anulado exitosamente. <br>
			Imprima la nota de cr&eacute;dito No. {$number_cn} <a href="{$document_root}modules/creditNote/pPrintFirstCreditNote/{$serial_cn}" target="_blank">aqu&iacute;</a>
		{elseif $error eq 2}
			El n&uacute;mero de tarjeta ingresado no existe.
		{elseif $error eq 3}
			La informaci&oacute;n de la factura no se ha podido cargar.
		{elseif $error eq 4}
			La tarjeta no se ha podido cambiar de estado a 'ANULADA'. Lo sentimos.
		{elseif $error eq 5}
			No se ha podido ingresar a la nota de cr&eacute;dito como forma de pago.
		{elseif $error eq 6}
			No se ha podido registrar la numeraci&oacute;n de la nota de cr&eacute;dito.
		{elseif $error eq 7}
			El n&uacute;mero de la nota de cr&eacute;dito no se ha podido actualizar.
		{elseif $error eq 8}
			No se ha podido ingresar la nota de cr&eacute;dito.
		{elseif $error eq 9}
			No existe un documento v&aacute;lido para actuar como nota de cr&eacute;dito <br>en el pa&iacute;s de emisi&oacute;n de la tarjeta.
		{elseif $error eq 10}
			{if $erp_message}
				{$erp_message}
			{else}
				La solicitud no se ha podido procesar ya que no hubo conexi&oacute;n con el ERP. Por favor comun&iacute;quese con el administrador.
			{/if}
		{/if}
		
	</div>
</div>
{/if}

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-8 label">* Ingrese el n&uacute;mero de tarjeta</div>
	<div class="span-8 append-4 last">
		<input type="text" name="txtCardNumber" id="txtCardNumber" title="El campo de 'Tarjeta' es obligatorio." />
	</div>
</div>

<div class="span-24 last buttons line">
	<input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
</div>

<form name="frmPenaltyInvoice" id="frmPenaltyInvoice" action="{$document_root}modules/invoice/penalty/pNewPenaltyInvoice" method="post" >
	<div class="span-24 last line" id="invoiceInfo"></div>
</form>