<!--/*
File: fVoidInvoiceRequest.es.tpl
Author: Santiago Benitez
Creation Date: 07/04/2010
Last Modified:
Modified by:
*/-->

{assign var="title" value="SOLICITUD PARA ANULACI&Oacute;N DE FACTURA"}
<form name="frmVoidInvoiceRequest" id="frmVoidInvoiceRequest" method="post" action="{$document_root}modules/invoice/pVoidInvoiceRequest">
	<div class="span-24 last title">
                Solicitud Para Anulaci&oacute;n de Factura<br />
        <label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-7 span-10 prepend-7 last " id="messageContainer">
    	{if $error eq 1}
           <div class="span-12 success" align="center">
            	Solicitud registrada exitosamente.
            </div>
        {else}
            <div class="span-12 error" align="center">
                {if $error eq 2}
                No se pudo registrar el nuevo cliente por lo cual no se pudo llevar a cabo la facturaci&oacute;n, comuniquese con el administrador.
                {/if}
            </div>
        {/if}
    </div>
 {/if}
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line"></div>
     <div class="span-24 last line separator">
        <label>Informaci&oacute;n de la Factura:</label>
    </div>
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Factura No.:</div>
        <div class="span-5">
            <input type="text" id="txtInvoiceNumber" name="txtInvoiceNumber" readonly="readonly" value="{$invoiceData.number_inv}"/>
        </div>
        <div class="span-3 label">Estado:</div>
        <div class="span-5 append-3 last">
        	<input type="text" id="txtStatus" name="txtStatus" readonly="readonly" value="{if $invoiceData.status_inv eq STAND-BY}En espera{/if}"/>
        </div>
   </div>
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Fecha de Emisi&oacute;n:</div>
        <div class="span-5">
                 <input type="text" id="txtDate" name="txtDate" readonly="readonly" value="{$invoiceData.date_inv}"/>
        </div>
        <div class="span-3 label">Fecha de Vencimiento</div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtDueDate" name="txtDueDate" readonly="readonly" value="{$invoiceData.due_date_inv}"/>
         </div>
   </div>
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Descuento (%):</div>
        <div class="span-5">
                 <input type="text" id="txtDiscount" name="txtDiscount" readonly="readonly" value="{$invoiceData.discount_prcg_inv}"/>
        </div>
        <div class="span-3 label">SubTotal:</div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtSubTotal" name="txtSubTotal" readonly="readonly" value="{$invoiceData.subtotal_inv}"/>
         </div>
   </div>
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">Otro Descuento (%):</div>
        <div class="span-5">
                <input type="text" id="txtOtherDiscount" name="txtOtherDiscount" readonly="readonly" value="{$invoiceData.other_dscnt_inv}"/>
        </div>
        <div class="span-3 label">Total:</div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtTotal" name="txtTotal" readonly="readonly" value="{$invoiceData.total_inv}"/>
         </div>
   </div>
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Observaciones de la factura:</div>
        <div class="span-13">
            <textarea id="txtObservations" name="txtObservations" cols="30" rows="5" readonly="readonly">{$invoiceData.comments_inv}</textarea>
        </div>
    </div>
    <div class="span-24 last line separator">
        <div class="span-5">
                    <label>Factura a nombre de:</label>
        </div>
    </div>
    {if $invoiceData.serial_cus}
    <div class="span-24 last line" id="customerData">
           <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Documento:</div>
                <div class="span-5">
                        <input type="text" name="txtCustomerDocument" id="txtCustomerDocument" readonly="readonly" value="{$extraData.document_cus}">
                </div>

                <div class="span-3 label">Tipo de Cliente:</div>
                <div class="span-4 append-2 last">
                        <input type="text" name="txtCustomerType" id="txtCustomerType" readonly="readonly" value="{if $extraData.type_cus eq 'PERSON'}Persona Natural{elseif $extraData.type_cus eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}">
                </div>
          </div>
            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Nombre:</div>
                <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" value="{$extraData.first_name_cus}" readonly="readonly"> </div>

                <div id="lastNameContainer" {if $extraData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
                    <div class="span-3 label">Apellido:</div>
                    <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" value="{$extraData.last_name_cus}" readonly="readonly"> </div>
                </div>
           </div>

            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Direcci&oacute;n:</div>
                <div class="span-5">
                        <input type="text" name="txtAddress" id="txtAddress" value="{$extraData.address_cus}" readonly="readonly">
                </div>

                <div class="span-3 label">Tel&eacute;fono Principal:</div>
                <div class="span-4 append-2 last">
                        <input type="text" name="txtPhone1" id="txtPhone1" value="{$extraData.phone1_cus}" readonly="readonly">
                </div>
           </div>

            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
                <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"  value="{$extraData.phone2_cus}" readonly="readonly"/></div>

                <div class="span-3 label">Celular:</div>
                <div class="span-4 append-2 last">  <input type="text" name="txtCellphone" id="txtCellphone" value="{$extraData.cellphone_cus}" readonly="readonly"/> </div>
            </div>

            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Pa&iacute;s:</div>
                <div class="span-5"> <input type="text" name="txtCountry" id="txtCountry" value="{$extraData.name_cou}" readonly="readonly"/> </div>

                <div class="span-3 label">Ciudad:</div>
                <div class="span-4 append-2 last" id="cityContainer"> <input type="text" name="txtCity" id="txtCity" value="{$extraData.name_cit}" readonly="readonly"/></div>
                </div>

            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">E-mail:</div>
                <div class="span-5"> <input type="text" name="txtMail" id="txtMail" value="{$extraData.email_cus}" readonly="readonly"> </div>
                <div id="birthdateContainer" {if $extraData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
                    <div class="span-3 label">Fecha de Nacimiento:</div>
                    <div class="span-4 append-2 last"> <input type="text" name="txtBirthdate" id="txtBirthdate" value="{$extraData.birthdate_cus}" readonly="readonly"> </div>
                </div>

           </div>

            <div class="prepend-2 span-19 append-3 last line">
                {if $extraData.type_cus neq 'LEGAL_ENTITY'}
                <div class="prepend-1 span-4 label">Familiar:</div>
                <div class="span-5">
                        <input type="text" name="txtRelative" id="txtRelative"  value="{$extraData.relative_cus}" readonly="readonly">
                </div>

                <div class="span-3 label">Tel&eacute;fono del familiar:</div>
                <div class="span-4 append-2 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative"  value="{$extraData.relative_phone_cus}" readonly="readonly"> </div>
                {else}
                <div class="prepend-1 span-4 label">Gerente</div>
                <div class="span-5">
                        <input type="text" name="txtRelative" id="txtRelative" value="{$extraData.relative_cus}" readonly="readonly">
                </div>

                <div class="span-3 label">Tel&eacute;fono del gerente:</div>
                <div class="span-4 append-2 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" value="{$extraData.relative_phone_cus}" readonly="readonly"> </div>
                {/if}
        </div>
    </div>
    {else}
    <div class="span-24 last line" id="dealerData">
                <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Id</div>
                <div class="span-5">
                    <input type="text" name="txtDocument" id="txtDocument" title='El campo "Documento" es obligatorio.' readonly="readonly" value="{$extraData.id_dea}">
                </div>
          </div>
            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Nombre:</div>
                <div class="span-5">  <input type="text" name="txtName" id="txtName" readonly="readonly" value="{$extraData.name_dea}"> </div>

                <div class="span-3 label">Direcci&oacute;n</div>
                <div class="span-4 append-2 last">  <input type="text" name="txtAddressDea" id="txtAddressDea" readonly="readonly" value="{$extraData.address_dea}"> </div>
            </div>

            <div class="prepend-2 span-19 append-3 last line">
                <div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
                <div class="span-5">
                        <input type="text" name="txtPhoneDea1" id="txtPhoneDea1"  readonly="readonly" value="{$extraData.phone1_dea}">
                </div>

                <div class="span-3 label">Tel&eacute;fono 2:</div>
                <div class="span-4 append-2 last">
                        <input type="text" name="txtPhoneDea2" id="txtPhoneDea2"  readonly="readonly" value="{$extraData.phone2_dea}">
                </div>
           </div>
    </div>
    {/if}
    <div class="span-24 last line separator">
        <div class="span-5">
            <label>Anulaci&oacute;n de Factura:</label>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">*Escriba motivo de la anulaci&oacute;n:</div>
        <div class="span-13">
            <textarea id="txtObservationsVoid" name="txtObservationsVoid" cols="30" rows="5" title="Debe escribir el motivo por el cu&aacute;l desea anular la factura."></textarea>
        </div>
    </div>
    <br/>
    <div class="span-24 last line separator">
        <div class="span-5">
            <label>Anulaci&oacute;n de Ventas:</label>
        </div>
    </div>
    <div class="span-24 last line">Seleccione las ventas que ser&aacute;n anuladas, las no seleccionadas quedar&aacute;n en estado Registrada.</div>
           <table border="0" id="salesTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            <input type="checkbox" id="selAll" name="selAll">
                        </td>
                        <td align="center" class="tableTitle">
                            # de Tarjeta
                        </td>
                        <td align="center"  class="tableTitle">
                            Producto
                        </td>
                        <td align="center" class="tableTitle">
                            Titular
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Emisi&oacute;n
                        </td>
                        <td align="center" class="tableTitle">
                            Total
                        </td>
                        <td align="center" class="tableTitle">
                            Estado
                        </td>

                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="sales" from=$salesList item="s"}
                        <tr {if $smarty.foreach.sales.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                <input type="checkbox" value="{$s.serial_sal}" name="chkSales[]" class="salesChk" product_type="{$s.serial_pbd}"/>
                            </td>
                            <td align="center" class="tableField">
                               {if $s.card_number_sal}{$s.card_number_sal}{else}N/A{/if}
                            </td>
                            <td align="center" class="tableField">
                                {$s.name_pbl}
                            </td>
                            <td align="center" class="tableField">
                                {if $s.customer_name}{$s.customer_name}{else}{$s.legal_entity_name}{/if}
                            </td>
                            <td align="center" class="tableField">
                                {$s.emission_date}
                            </td>
                            <td align="center" class="tableField">
                                {$s.total_sal}
                            </td>
                            <td align="center" class="tableField">
                                 {$global_salesStatus[$s.status_sal]}
                            </td>
                        </tr>
                        <input type="hidden" name="salesCost[]" value="{$s.total_cost_sal}"/>
                        <input type="hidden" name="sales[]" value="{$s.serial_sal}"/>
                      {/foreach}
                    </TBODY>
           </table>
     <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
    <input type="hidden" name="selectedSales" id="selectedSales" title="Debe seleccionar almenos una venta.">
    <input type="hidden" name="hdnSerialInv" id="hdnSerialInv" value="{$invoiceData.serial_inv}">
    <div class="span-24 last buttons line">
        <br/>
        <input id="btnRegister" type="submit" value="Solicitar Anulaci&oacute;n" name="btnRegister"/>
    </div>
</form>
<div id="dialog" title="Preguntas">
        {include file="templates/modules/customer/fCustomerDialog.$language.tpl"}
</div>