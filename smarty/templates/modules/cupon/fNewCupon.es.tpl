{assign var=title value="GENERAR CUPONES"}
<div class="span-24 last title ">
	GENERAR CUPONES<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
	{if $error eq 1}		
		Los cupones se crearon exitosamente!
	{elseif $error eq 2}
		No se pudo ingresar la totalidad de los cupones.
	{elseif $error eq 3}
		No se pudieron cargar los datos de la promoci&oacute;n. Lo sentimos.
	{/if}
	</div>
</div>
{/if}

<div class="span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $country_list}
	<form name="frmNewCupon" id="frmNewCupon" method="post" action="{$document_root}modules/cupon/pNewCupon" class="form_smarty">
		<div class="span-24 last">
			<div class="span-24 last line">
				<div class="prepend-7 span-5 label">
					* Pa&iacute;s:
				</div>
				<div class="span-7 append-2 last">
					<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio">
						<option value="">- Seleccione -</option>
						{foreach from=$country_list item="c"}
							<option value="{$c.serial_cou}">{$c.name_cou}</option>
						{/foreach}
					</select>
				</div>
			</div>

			<div class="span-24 last line">
				<div class="prepend-7 span-5 label">
					* Promoci&oacute;n:
				</div>
				<div class="span-7 append-2 last" id="promoContainer">
					<select name="selCustomerPromo" id="selCustomerPromo" title="El campo 'Promoci&oacute;n' es obligatorio">
						<option value="">- Seleccione -</option>
					</select>
				</div>
				<div class="prepend-12 span-9 last" id="infoContainer"></div>
			</div>

			<div class="span-24 last line">
				<div class="prepend-7 span-5 label">
					* N&uacute;mero de Cupones a Crear:
				</div>
				<div class="span-7 append-2 last">
					<input type="text" name="txtNumberToCreate" id="txtNumberToCreate" title="El campo 'N&uacute;mero de Cupones a Crear' es obligatorio." />
				</div>
			</div>

			<div class="span-24 last buttons line">
				<input type="submit" name="btnInsert" id="btnInsert" value="Insertar" class="" >
			</div>
		</div>
	</form>
{else}
	<div class="prepend-7 span-10 append-7">
		<div class="span-10 error center">
			No existen promociones ingresadas al sistema todav&iacute;a.
		</div>
	</div>
{/if}