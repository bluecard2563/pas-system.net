{assign var="title" value="CAMBIO DE RESPONSABLE"}
<form name="frmNewCounter" id="frmNewCounter" method="post" action="{$document_root}modules/comissionistByDealer/pNewComissionistByDealer" class="">
	<div class="span-24 last title ">
    	Cambio de Responsable<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	El responsable se ha asignado exitosamente!
        {elseif $error eq 2}
            	No se pudo asignar el responsable.
         {elseif $error eq 3}
            	No se seleccion&oacute; ning&uacute;n comercializador.
        {/if}
        </div>
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="span-24 last line separator">
    	<label>*Seleccionar usuario:</label>
   </div>
   
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">  
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">  
        	<select name="selCountryUser" id="selCountryUser" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
     <div class="span-24 last line">
     	<div class="prepend-4 span-4 label">*Ciudad:</div>
        <div class="span-5" id="cityContainer">     	                                  
            <select name="selCityUser" id="selCityUser" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-7 span-7 label">*Ingrese el nombre o documento del usuario:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtUser" id="txtUser" title="El campo de usuario es obligatorio."  />
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-7 label">*Porcentaje de comisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            <input class="span-2" type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje de comisi&oacute;n" es obligatorio.'  />
        </div>
    </div>
    
    <div class="span-24 last line separator">
    	<label>*Seleccionar sucursal(es) de Comercializador:</label>
   </div>
   
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">  
        	<select name="selZoneAssign" id="selZoneAssign" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryAssignContainer">  
        	<select name="selCountryAssign" id="selCountryAssign" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
     <div class="span-24 last line">
     	<div class="prepend-4 span-4 label">*Ciudad:</div>
        <div class="span-5" id="cityAssignContainer">     	                                  
            <select name="selCityAssign" id="selCityAssign" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>

		<div class="span-3 label">* Responsable:</div>
        <div class="span-4 append-4 last" id="comissionistAssignContainer">
        	<select name="selComissionistAssign" id="selComissionistAssign" title='El campo "Responsable" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>


    </div>
    
    <div class="span-24 last line" id="dealerContainer">
    <div class="prepend-5 span-5 label">*Seleccione el comercializador:</div>
        <div class="span-5 append-3 last line">
            <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-5 span-6 label">*Seleccione las sucursales de Comercializador:</div>
        <div class="prepend-7 span-10 append-4 last line" id="branchContainer">
            <div class="span-4">
                <div align="center">Seleccionados</div>
                <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-4 last" title="El campo de sucursales de comercializador es obligatorio."></select>
            </div>
            <div class="span-2 last buttonPane">
                <br />
                <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
            </div>
            <div class="span-4 last" id="dealersFromContainer">
                <div align="center">Existentes</div>
                <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last">
                </select>
            </div>
            
            <div class="span-13 append-6">
                *Todas las sucursales en el lado izquierdo deben estar seleccionadas<br />
                para poder asignarles el responsable seleccionado.
            </div>
        </div>
    </div>
    
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" />
        <input type="hidden" name="hdnSerial_usu" id="hdnSerial_usu" value="" />
    </div>
</form>