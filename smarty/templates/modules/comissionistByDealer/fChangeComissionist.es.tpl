{assign var="title" value="CAMBIO GENERAL DE RESPONSABLE"}
<form name="frmNewCounter" id="frmNewCounter" method="post" action="{$document_root}modules/comissionistByDealer/pChangeComissionist">
	<div class="span-24 last title ">
    	Cambio General de Responsable<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
		{if $error eq 1}
			Los comercializadores y sus sucursales fueron reasignados exitosamente!
		{elseif $error eq 2}
			El cambio de responsable no pudo finalizar. Por favor vuelva a intentarlo
		{elseif $error eq 3}
			No se seleccion&oacute; ning&uacute;n comercializador. Por favor vuelva a intentarlo.
		{/if}
        </div>
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="span-24 last line separator">
    	<label>* Nuevo Responsable</label>
   </div>
   
   <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-5">  
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">  
        	<select name="selCountryUser" id="selCountryUser" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
     <div class="span-24 last line">
     	<div class="prepend-3 span-4 label">*Ciudad:</div>
        <div class="span-5" id="cityContainer">     	                                  
            <select name="selCityUser" id="selCityUser" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-5 span-7 label">*Ingrese el nombre o documento del usuario:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtUser" id="txtUser" title="El campo de usuario es obligatorio." class="span-5"  />
			<input type="hidden" name="hdnSerial_usu" id="hdnSerial_usu" value="" title="El campo de usuario es obligatorio." />
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-5 span-7 label">*Porcentaje de comisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            <input class="span-2" type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje de comisi&oacute;n" es obligatorio.'  />
        </div>
    </div>
    
    <div class="span-24 last line separator">
    	<label>* Comercializadores a Reasignar:</label>
   </div>
   
   <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-5">  
        	<select name="selZoneAssign" id="selZoneAssign" title='El campo "Zona  (a Reasignar)" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryAssignContainer">  
        	<select name="selCountryAssign" id="selCountryAssign" title='El campo "Pa&iacute;s  (a Reasignar)" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
     <div class="span-24 last line">
     	<div class="prepend-3 span-4 label">*Ciudad:</div>
        <div class="span-5" id="cityAssignContainer">     	                                  
            <select name="selCityAssign" id="selCityAssign" title='El campo "Ciudad (a Reasignar)" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>

		<div class="span-3 label">* Responsable:</div>
        <div class="span-4 append-4 last" id="comissionistAssignContainer">
        	<select name="selComissionistAssign" id="selComissionistAssign" title='El campo "Responsable (a Reasignar)" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    
    <div class="span-24 last line" id="dealersContainer"></div>
</form>