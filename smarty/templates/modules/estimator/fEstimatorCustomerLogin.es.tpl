{assign var="title" value="SELECCION DE CLIENTE TITULAR"} 
<form name="frmCustomerLogin" id="frmCustomerLogin" method="post" action="{$document_root}modules/estimator/pEstimatorLoginCustomer" class="">
	{if $error}
	    <div class="append-8 span-12 prepend-6 last ">
	        <div class="span-12 {if $error eq 1}success{else $error}error{/if}" align="center">
	            {if $error eq 1}
	                Su cuenta fue creada exitosamente, para acceder a su cuenta, ingrese su correo y clave en la cabecera de esta pantalla.
	            {elseif $error eq 2}
	                Sus datos fueron registrados, sin embargo el email de confirmaci&oacute;n no pudo ser enviado.
	            {elseif $error eq 3}
	                Hubo errores al crear su cuenta, por favor vuelva a intentarlo.
	            {/if}
	        </div>
	    </div>
    {/if}
    
	<div class="span-24 last title">
		Ingreso del Cliente Titular de la Tarjeta<br />
		<label>(*) Campos Obligatorios</label>
	</div>
	
	<div class="span-7 span-10 prepend-7 last">
	     <ul id="alerts" class="alerts"></ul>
	</div>
	
	<div class="span-24 last" id="customerContainer">
	    <div class="span-24 last line separator">
	        <label>Datos del Titular de la Tarjeta</label>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
	        <div class="span-5">
	            <input type="text" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio'/>
	        </div>
	        <div class="span-3 label">*Tipo de Cliente</div>
	        <div class="span-4 append-2 last">
	        	<select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
	            	<option value="">- Seleccione -</option>
	                	{foreach from=$typesList item="l"}
	                <option value="{$l}">{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                {/foreach}
            </select>
	        </div>
	    </div>
	
	    <div class="span-24 last line" id="divName">
	        <div class="prepend-4 span-4 label">* Nombre:</div>
	        <div class="span-5">
	            <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Nombre" es obligatorio' />
	        </div>
	
	        <div class="span-3 label">* Apellido:</div>
	        <div class="span-4 append-4 last">
	            <input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' />
	        </div>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
	        <div class="span-5">
	        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
	            	<option value="">- Seleccione -</option>
	                {foreach from=$countryList item="l"}
	                    <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
	                {/foreach}
	            </select>
	        </div>
	
	        <div class="span-3 label">* Ciudad</div>
	        <div class="span-4 append-4 last" id="cityContainerCustomer">
	        	<select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
	            	<option value="">- Seleccione -</option>
	            </select>
	         </div>
	    </div>
	
	    <div class="span-24 last line" id="divAddress">
	        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
	        <div class="span-5">
	            <input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" title='El campo "Fecha de Nacimiento" es obligatorio'/>
	        </div>
	
	        <div class="span-3 label">* Direcci&oacute;n:</div>
	        <div class="span-4 append-4 last">
	            <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' />
	        </div>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
	        <div class="span-5">
	            <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" title='El campo "Tel&eacute;fono 1" es obligatorio'/>
	        </div>
	
	        <div class="span-3 label">Tel&eacute;fono 2:</div>
	        <div class="span-4 append-4 last">
	            <input type="text" name="txtPhone2Customer" id="txtPhone2Customer" />
	        </div>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="prepend-4 span-4 label">Celular:</div>
	        <div class="span-5">
	            <input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer" />
	        </div>
	
	        <div class="span-3 label">*Email:</div>
	        <div class="span-4 append-4 last">
	            <input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio' />
	        </div>
	    </div>
	
	    <div class="span-24  last line" id="divRelative">
	        <div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
	        <div class="span-5">
	        	<input type="text" name="txtRelative" id="txtRelative" title='El campo "Contacto en caso de Emergencia" es obligatorio'/>
	        </div>
	
	        <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
	        <div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio'/> </div>
	    </div>
	</div>
	
	<div class="span-24 last buttons">
		<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
	</div>
</form>