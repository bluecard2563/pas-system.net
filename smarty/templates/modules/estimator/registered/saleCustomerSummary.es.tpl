{assign var="title" value="DETALLE DE COMPRA"} 

<div class="span-24 last" id="titularContainer">
	<div class="span-24 last" >&nbsp;</div>
   	<div class="span-14 prepend-5 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
	<form id="saleTypeForm" class="creditCardForm" method="post">
		<div class="span-24">
	    		{if $error}
	    			<div class="span-24"><div class="span-6">&nbsp;</div>
			    	<div class=" span-11 last {if    $error == 3 ||
			        								 $error == 30
			        								}success
			        							 {else $error}error{/if}" align="center" id="messages">
			          {if $error == 'payPalCancel'}
			          		Ha cancelado la compra en PayPal.
			          {elseif $error eq 2}
			          	La clave es incorrecta, por favor ingrese nuevamente!
			          {elseif $error eq 3}
			          	Su cuenta fue creada exitosamente, ahora puede ingresar con sus datos de usuario.
			          {elseif $error eq 11}
			              	Error al Guardar la informaci&oacute;n del Titular.
			          {elseif $error eq 12}
			          		Error al Guardar la informaci&oacute;n del pago.
			          {elseif $error eq 13}
			          		Error al Guardar la informaci&oacute;n del detalle del pago.
			          {elseif $error eq 14}
			          		Error al Guardar la informaci&oacute;n del Documento.
			          {elseif $error eq 15}
			          		Error al Actualizar la informaci&oacute;n del Documento.
			          {elseif $error eq 16}
			          		Error al Guardar la informaci&oacute;n de la Factura.
			          {elseif $error eq 17}
			          		Error al Guardar la informaci&oacute;n de la venta.
			          {elseif $error eq 18}
			          		Error al Guardar la informaci&oacute;n de los Acompa&ntilde;antes.
			          {elseif $error eq 19}
			          		Error al Guardar el registro del viaje.
			          {elseif $error eq 20}
			          		Error al Guardar el registro de los Acompa&ntilde;antes.
			          {elseif $error eq 21}
			          		Los datos ingresados para la tarjeta de Credito son incorrectos
			          {elseif $error eq 22}
			          		Hubo un error al procesar su compra, por favor pongase en contacto con el Administrador.
			          {elseif $error eq 23}
			          		Hubo problemas con el env&iacute;o del correo.
			          {elseif $error eq 24}
			          		Hubo errores al obtener el n&uacute;mero de tarjeta. Comun&iacute;quese con el administrador antes de continuar.

			          {elseif $error eq 30}
			               La venta se ha realizado exitosamente!
			          {elseif $error eq 31}
							Lo sentimos, la transacci&oacute;n tuvo errores. Por favor comun&iacute;quese con las oficinas de
							Planet Assist en su pa&iacute;s para mayor informaci&oacute;n.
					  {elseif $error eq 32}
					  		La compra por PayPal fue Cancelada.
					  
					  
					  {elseif $error eq 40}
							La venta se ha realizado exitosamente!
					  {elseif $error eq 41}
							Ha ocurrido un error en la validaci&oacute;n de su tarjeta de cr&eacute;dito. Error: 
							{handle_merchant_account_errors error=$extra_error_code}
					  {elseif $error eq 42}
							No se tiene la informaci&oacute;n necesaria para continuar con el pago con tarjeta de cr&eacute;dito.
							Por favor vuelva a cotizar el producto.
			          {/if}
			     </div></div>
			{/if}
				
			{if $ecuadorian_departure}
			<div class="span-24">
				<div class="span-6">&nbsp;</div>
				<div class=" span-11 last notice" align="center">
					Su pago incluir&aacute; el 5% de Impuesto a la Salida de Divisas (ISD) del Ecuador.
				</div>
			</div>
			{/if}
		
			{if $code_parts}
				<div class="prepend-7 span-10 append-7 last">
				{if $cupon_applied}
					<div class="span-10 success center">
						<b>El cup&oacute;n fue aplicado! </b><br />
						<i style="font-size: 11px;">*Si no existe un descuento en la compra actual, <br />
							se generar&aacute; otro cup&oacute;n para su siguiente compra.</i>
					</div>	
				{else}
					<div class="span-10 error center">
						El cup&oacute;n no es v&aacute;lido.
					</div>	
				{/if
				</div>}
			{/if}
	    	<div class="span-6">&nbsp;</div>
	    	<div class="span-12 summary_main">
				<div class="red_title single_field">
					Detalle de la Compra a Realizar.
				</div>
				<img src="{$document_root}img/customer/summary/dotted.png" style="margin-right:7px;" />
				<div class="span-12">&nbsp;</div>
				<div class="span-11 line">
					<table id="taxTable" class="prepend-1 tableBuy no_space">
						<tr class="table_header">
							<td class="tax_header"><b>Producto</b></td>
							<td class="tax_header"><b>Producto</b></td>
							<td class="tax_header"><b>Cup&oacute;n</b></td>
							<td class="tax_header rightText"><b>Precio</b></td>
						</tr>
					</table>
					<hr class="no_space">
					<table id="taxTable" class="prepend-1 tableBuy">
						{foreach from=$productPricesTable item=tabRow key=cid}
						<tr class="table_header">
						{if $tabRow.type == 'product'}
							<td class="tax_header row_a">{$tabRow.num}</td>
							<td class="tax_header">{$tabRow.name}</td>
							<td class="tax_header">{if $tabRow.cupon neq '0.00'}- {$tabRow.cupon}{else}N/A{/if}</td>
							<td class="tax_header rightText">{$smarty.session.web_currency}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
						{elseif $tabRow.type == 'tax'}
							<td class="rightText tax_header" colspan="3">{$tabRow.name}</td>
							<td class="rightText tax_header rightText">{$smarty.session.web_currency}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
						{elseif $tabRow.type == 'total'}
							<td class="rightText tax_header" colspan="3">{$tabRow.name}</td>
							<td class="rightText tax_header">{$smarty.session.web_currency}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
						{/if}
						</tr>
						{/foreach}
					</table>
				</div>

				<div class="span-12">&nbsp;</div>
				<div class="red_title single_field">
					Aplicar cup&oacute;n de promociones
				</div>
				<img src="{$document_root}img/customer/summary/dotted.png" style="margin-right:7px;" />
				<div class="span-12">&nbsp;</div>
				<div class="span-11 line">
					<div class="prepend-1">
						<b class="tax_header">C&oacute;digo de cup&oacute;n:</b>
						<input type="text" name="txtCoupon" id="txtCoupon">
						<input type="button" id="btnCoupon" value="Aplicar">
					</div>
				</div>
				
				<div class="span-12">&nbsp;</div>
				<div class="span-11 line">
					<div class="prepend-3 span-8 last line">
						<b class="tax_header">Factura a otro nombre?</b>
						<input type="button" id="btnNewHolder" value="Ingresar Datos">
					</div>
					<div id="newHolderInfoContainer" class="prepend-3 span-8 last line" style="display: none;">
						<b class="tax_header">No. Documento:</b> <b id="lblDocument"></b> <br />
						<b class="tax_header">Raz&oacute;n Social:</b> <b id="lblFullName"></b> <br />
						<input type="button" id="btnRemoveInvoiceHolder" name="btnRemoveInvoiceHolder" value="Deshacer" />
					</div>
				</div>
				
				<div class="span-12">&nbsp;</div>
				<div class="span-3 prepend-4 last clickable" align="center">
					<div class="blueButton shortWidth" id="continueButton">Continuar</div>
					<div class="span-3">&nbsp;</div>
					<a target="_blank" href="https://www.internetsecure.com/cgi-bin/certified.mhtml?merchant_number=51815">
						<img src="{$document_root}img/customer/summary/isPowered.png" id="internetSecureLink" align="middle"/>
					</a>
				</div>
			</div>
		</div>
	</form>
</div>


<div id="newHolderInfo">
	<form id="invoiceHolder" name="invoiceHolder" method="post">
		<div class="prepend-4 span-11 append-4 last line">
			<div class="span-11 last">
				<ul id="alertsNewHolder" class="alerts"></ul>
			</div>
		</div>
		
		<div class="span-19 last line" id="customerData">
			<div class="span-19 last line">
				<div class="prepend-1 span-4 label">* Documento:</div>
				<div class="span-5">
					<input type="text" name="txtCustomerDocument" id="txtCustomerDocument" title='El campo "Documento" es obligatorio.'value="{$customerData.document_cus}">
				</div>

				<div class="span-3 label">* Tipo de Documento:</div>
				<div class="span-6 last">
					<input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
					<input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" /> RUC					
					<input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
					<input type="hidden" name="selType" id="selType" value="{$customerData.type_cus}" />
				</div>
			</div>

			<div class="span-19 last line">
				<div class="prepend-1 span-4 label" id="labelName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}* Razo&oacute;n Social: {else}* Nombre:{/if}</div>
				<div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$customerData.first_name_cus}" {if $customerData}readonly="readonly"{/if}> </div>

				<div id="lastNameContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
					<div class="span-3 label">* Apellido:</div>
					<div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$customerData.last_name_cus}" {if $customerData}readonly="readonly"{/if}> </div>
				</div>                
			</div>

			<div class="span-19 last line">
				<div class="prepend-1 span-4 label">* Direcci&oacute;n:</div>
				<div class="span-5">
					<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus}">
				</div>

				<div class="span-3 label">* Tel&eacute;fono Principal:</div>
				<div class="span-4 append-2 last">
					<input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}">
				</div>
			</div>

			<div class="span-19 last line">
				<div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
				<div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"  value="{$customerData.phone2_cus}"/></div>
			</div>

			<div class="span-19 last line">
				<div class="prepend-1 span-4 label">* Pa&iacute;s:</div>
				<div class="span-5"> 
					<select name="selCountry" id="selCountry" class="span-5" title='El campo "Pa&iacute;s" es obligatorio'>
						<option value="">- Seleccione -</option>
						{foreach from=$countryList item="l"}
							<option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.auxData.serial_cou}selected{/if}>{$l.name_cou}</option>
						{/foreach}
					</select> </div>

				<div class="span-3 label">* Ciudad:</div>
				<div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
						<option value="">- Seleccione -</option>
					</select> 
				</div>
			</div>
		</div>
	</form>
</div>