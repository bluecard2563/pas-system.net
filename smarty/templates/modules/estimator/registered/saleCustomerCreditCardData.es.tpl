{assign var="title" value="DATOS DE TARJETA DE CREDITO"} 

{if $error}
	<div class="span-24 last login-errors">
	    <div class="prepend-6 span-12 append-6 last">
	        	<div class="span-11 last error" align="center" id="messages">
	          {if $error eq 1}
	              	Error al Guardar la informaci&oacute;n del Titular.
	          {elseif $error eq 2}
	          		Error al Guardar la informaci&oacute;n del pago.
	          {elseif $error eq 3}
	          		Error al Guardar la informaci&oacute;n del detalle del pago.
	          {elseif $error eq 4}
	          		Error al Guardar la informaci&oacute;n del Documento.
	          {elseif $error eq 5}
	          		Error al Actualizar la informaci&oacute;n del Documento.
	          {elseif $error eq 6}
	          		Error al Guardar la informaci&oacute;n de la Factura.
	          {elseif $error eq 7}
	          		Error al Guardar la informaci&oacute;n de la venta.
	          {elseif $error eq 8}
	          		Error al Guardar la informaci&oacute;n de los Acompa&ntilde;antes.
	          {elseif $error eq 9}
	          		Error al Guardar el registro del viaje.
	          {elseif $error eq 10}
	          		Error al Guardar el registro de los Acompa&ntilde;antes.
	          {elseif $error eq 11}
	          		Los datos ingresados para la tarjeta de Credito son incorrectos
	          {/if}
	         </div>
	    </div>
	</div>
{/if}
<div class="span-24 last" id="titularContainer">
	<div class="span-24 last" >&nbsp;</div>
   	<div class="span-14 prepend-5 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
        
        {if $use_converge}
            <form id="creditCardForm" class="creditCardForm" action="{$document_root}modules/estimator/epayment/pValidateEPayment" method="post">
        {else}
            <form id="creditCardForm" class="creditCardForm" action="{$document_root}modules/estimator/registered/pBuyShoppingCart" method="post">
        {/if}
		<div class="span-24">
	    	<div class="span-6">&nbsp;</div>
	    	<div class="span-12 register_main">
				<div class="prepend-3 red_title single_field">
					Datos de la tarjeta de cr&eacute;dito
				</div>
				<div class="prepend-2 tax_header">*Detalle de la compra:</div>
				<div class="span-10 prepend-1">
					<table id="taxTable" class="prepend-2 tableBuy no_space">
						<tr class="table_header">
							<td class="tax_header"><b>Producto</b></td>
							<td class="tax_header"><b>Producto</b></td>
							<td class="tax_header"><b>Cup&oacute;n</b></td>
							<td class="tax_header"><b>Precio</b></td>
						</tr>
					</table>
					<hr class="no_space">
					<table id="taxTable" class="prepend-2 tableBuy">
						{foreach from=$productPricesTable item=tabRow key=cid}
						<tr>
						{if $tabRow.type == 'product'}
							<td class="tax_header row_a">{$tabRow.num}</td>
							<td class="tax_header">{$tabRow.name}</td>
							<td class="tax_header">{if $tabRow.cupon neq '0.00'}- {$tabRow.cupon}{else}N/A{/if}</td>
							<td class="tax_header rightText">{$symbolCur}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
						{elseif $tabRow.type == 'tax'}
							<td class="rightText tax_header" colspan="2">{$tabRow.name}</td>
							<td class="rightText tax_header rightText">{$symbolCur}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
                                                 {elseif $tabRow.type == 'subtotal'}
							<td class="rightText tax_header" colspan="3">Sub total:</td>
							<td class="rightText tax_header">{$smarty.session.web_currency}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
						{elseif $tabRow.type == 'total'}
							<td class="rightText tax_header" colspan="3">{$tabRow.name}</td>
							<td class="rightText tax_header">{$symbolCur}&nbsp;{$tabRow.price|number_format:2:",":"."}</td>
						{/if}
						</tr>
						{/foreach}
					</table>
				</div>
				
				<div class="inner_box span-11 line">
					<div class="red_title line">
						Informaci&oacute;n de la tarjeta
						<hr class="no_space">
					</div>
					<div class="span-7 line">
						<img src="{$document_root}img/credit_card_logos/amex2.gif" align="right" style="margin-right:4px;">
						<img src="{$document_root}img/credit_card_logos/discover2.gif" align="right" style="margin-right:4px;"/>
						<img src="{$document_root}img/credit_card_logos/master2.gif" align="right" style="margin-right:4px;"/>
						<img src="{$document_root}img/credit_card_logos/visa2.gif" align="right" style="margin-right:4px;"/>
					</div>
					<div class="span-6 blue_text">
						<div class="span-6 line">
							<div class="span-6 blue_text">
				    			<div class="span-4 last">
				    				*N&uacute;mero de Tarjeta:
				    			</div>
				    			<div class="code-box">
				    				*C&oacute;digo de Verificaci&oacute;n:
				    			</div>
				    		</div>
				    		<div class="span-6 blue_text">
				    			<input tabindex="1" type="text" class="text-med" name="txtCardNum" id="txtCardNum"  title='El campo "N&uacute;mero de Tarjeta" es obligatorio'/>
				       			<input tabindex="2" type="text" class="text-short" name="txtSecCode" id="txtSecCode"  title='El campo "C&oacute;digo de Seguridad" es obligatorio'/>
				    		</div>
		    			</div>
		    			
		    			<div class="span-6 blue_text line">
			    			*Nombre en la Tarjeta:
			    			<input tabindex="4" type="text" class="text-med" name="txtNameCard" id="txtNameCard" title='El campo "Nombre en la Tarjeta" es obligatorio' />
			    		</div>
		    		
		    			<div class="span-6 line">
				       		* Fecha de Expiraci&oacute;n:
				       		<div id="selectExpirationMonth" class="expirationBox">
					       		<select tabindex="6" name="selMonth" id="selMonth" title='El campo "Mes" en la Fecha de vencimiento es obligatorio.' class="monthDropdown">
						        	<option value=""></option>
						            {foreach from=$months item="mon" key=cid}
						             	<option value="{if $cid<9}0{/if}{$cid+1}" id="month_{$cid}" >{$mon} ({if $cid<9}0{/if}{$cid+1})</option>
						            {/foreach}
						        </select>
						    </div>
					        <div id="selectExpirationYear" class="span-1">
					       		<select tabindex="7" name="selYear" id="selYear" title='El campo "A&ntilde;o" en la Fecha de vencimiento es obligatorio.' class="yearDropdown">
						        	<option value=""></option>
						            {foreach from=$years item="year" key=cid}
						             	<option value="{$year}" id="year_{$cid}" >{$year}</option>
						            {/foreach}
						        </select>
						    </div>
				       </div>
				       
				       <div class="span-6 blue_text">
			    			* Direcci&oacute;n:
			    			<textarea tabindex="9" class="textarea" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio'></textarea>
			    		</div>
		    		</div>
		    		
		    		<div class="span-5 blue_text last single_field">
		    			<div class="span-5 blue_text last line">
			    			*Pa&iacute;s:
							<div id="selectRegister">
					       		<select tabindex="3" name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
					           		<option value="">- Seleccione -</option>
					               	{foreach from=$countries item="cou"}
					                	<option value="{$cou.serial_cou}" id="product_{$cou.serial_cou}" >{$cou.name_cou}</option>	
					                {/foreach}
					           </select>
					       </div>
			    		</div>
		    			
		    			<div class="span-5 blue_text last line">
			    			* Ciudad:
			    			<div id="selectRegister">
			    				<div id="cityContainerCustomer">
				       				<select tabindex="5" name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
				           				<option value="">- Seleccione -</option>
						           </select>
					           </div>
					        </div>
						</div>					        
				        
				        <div class="span-5 blue_text last line">
			    			* Estado/Provincia:
							<input tabindex="8" type="text" class="text" name="txtState" id="txtState" title='El campo "Estado/Provincia" es obligatorio'/>
			    		</div>
			    		
			    		<div class="span-5 blue_text line">
			    			* C&oacute;digo Postal/Zip:
			           		<input tabindex="10" type="text" class="text" name="txtZip" id="txtZip" title='El campo "C&oacute;digo Postal/Zip" es obligatorio'/>
			    		</div>
			    		
			    		<div class="span-5 blue_text last line">
			    			* Tel&eacute;fono:
							<input tabindex="11" type="text" class="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio'/>
			    		</div>
			    		<div class="span-5 blue_text last line">
			    			* E-mail:
							<input tabindex="12" type="text" class="text" name="txtMail" id="txtMail" title='El campo "Email" es obligatorio'/>
			    		</div>
		    		</div>
		    		<div class="prepend-4 span-3 append-5 line">
					    <div class="blueButton shortWidth" id="btnSend">Comprar</div>
				    </div>
				</div>
				<div class="terms_use span-12" style="color: #000000; font-style: italic; text-align: center;">
					By providing this information you agree to Elavon's
					<a href="http://www.internetsecure.com/privacy.html" target="_blank">Policy and Terms of use</a>
					<br />and it's <a href="http://www.internetsecure.com/terms.htm" target="_blank">Terms of Use</a>
				</div>
			</div>
		</div>
	</form>
</div>