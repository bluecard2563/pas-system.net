{assign var="title" value="REGISTRO DE VIAJES"}

<div class="span-24">&nbsp;</div>
<div class="span-24 last login-errors">
    <div class="prepend-6 span-12 append-6 last">
        {if $error neq 0}
         	<div class="span-10 error" align="center">
          		{if $error eq 1}
					Los Datos del Cliente no se pudieron actualizar. <br/>
				{/if}
				{if $error eq 2}
					El cliente no pudo ser registrado. Intentelo nuevamente. <br/>
				{/if}
				{if $error eq 3}
					Las preguntas de adulto mayor no se han podido registrar. Intentelo nuevamente. <br/>
				{/if}
				{if $success eq 0}
					No se ha podido registrar el Viaje. <br/>
				{/if}
        	</div>
      	{/if}
		{if $success eq 1}
			<div class="span-10 success" align="center">
            	Se ha registrado el viaje Exitosamente!<br/>
            	<a  id="printRegister" href="{$document_root}customer/pPrintRegister/{$printingId}" target="_blank">Imprimir Registro</a>
          	</div>
		{/if}
	</div>
</div>
<div class="span-4 prepend-10 last title">
		Registro de Viajes<br/>
</div>
<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>
<div class="span-24 last line" id="cardsContainer">
</div>
<script type="text/javascript">
	loadCustomerCards('{$customerSerial}');
</script>
