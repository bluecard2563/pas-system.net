{if $list}
    <br/>
        <div class="span-24 last title">
                Lista de tarjetas del cliente en las que se puede registrar viajes<br />
        </div>
	<div class="span-24 last line">
		<table border="0" id="travelsTable" style="color: #000000;">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            No.
                        </td>
                        <td align="center" class="tableTitle">
                            # de Tarjeta
                        </td>
                        <td align="center"  class="tableTitle">
                            Tipo
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Inicio
                        </td>
                        <td align="center" class="tableTitle">
                            Fecha Expira
                        </td>
                        <td align="center" class="tableTitle">
                            D&iacute;as Contratados
                        </td>
                        <td align="center" class="tableTitle">
                            D&iacute;as Disponibles
                        </td>
						<td align="center" class="tableTitle">
                            Registrar
                        </td>

                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="corporative" from=$list item="s"}
                        <tr {if $smarty.foreach.corporative.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                               {$smarty.foreach.corporative.iteration}
                            </td>
                            <td align="center" class="tableField">
                               {if $s.card_number_sal}{$s.card_number_sal}{else}N/A{/if}
                            </td>
                            <td align="center" class="tableField">
                                {$s.name_pbl|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.begin_date_sal}
                            </td>
                            <td align="center" class="tableField">
                                {$s.end_date_sal}
                            </td>
                            <td align="center" class="tableField">
                                {$s.days_sal}
                            </td>
                            <td align="center" class="tableField">
                                 {$s.available_days}
                            </td>
							<td align="center" class="tableField">
								{if $s.available_days > 0}<a href="{$document_root}customer/fRegisterTravel/{$s.serial_sal}">Registrar</a>{else}--{/if}
							</td>

                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
	</div>
     <div class="span-24 last pageNavPosition" id="travelPageNavPosition"></div>
{else}
	 <div class="span-10 append-5 prepend-7 label">El Cliente no posee Tarjetas Corporativas.</div>
{/if}