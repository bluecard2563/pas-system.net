{assign var="title" value="COTIZADOR DE PRODUCTOS"} 
<div class="span-24 last login-errors">
    <div class="prepend-6 span-12 append-6 last">
        {if $error}
        	<div class="span-11 last {if $error == 1 || $error == -1}success {else $error}error{/if}" align="center" id="messages">
          {if $error < 2}
				Su compra fue procesada exitosamente!
				{if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}
          {elseif $error eq 2}
          		Hubo errores al guardar el carrito de compras, por favor vuelva a intentarlo.
          {elseif $error eq 3}
          		Hubo errores al guardar los productos en el carrito de compras, por favor vuelva a intentarlo.
          {elseif $error eq 4}
          		El producto seleccionado no se encuentra disponible para compra Telef&oacute;nica.
          {elseif $error eq 5}
          		El carrito no pudo ser encontrado, por favor vuelva a intentarlo.
          {/if}
         </div>
     {/if}
    </div>
</div>
<div class="span-24 last " align="center">
	<div id="quoteEstimatorFull" class="prepend-4 span-13">
		{estimator_full}
	</div>
</div>
<div id="dialogBenefits" title="Detalle de Beneficios">
	{include file="templates/modules/estimator/productBenefitsDialog.tpl"}
</div>
<div id="dialogGeneralConditions" title="Condiciones Generales">
	{include file="templates/fGeneralConditionsSelector.tpl"}
</div>
<script type="text/javascript">
	goTo = {$goTo}+'';
</script>