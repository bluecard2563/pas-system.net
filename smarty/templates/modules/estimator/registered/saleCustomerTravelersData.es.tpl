{assign var="title" value="DATOS DE VIAJEROS"} 

<div class="span-24 last" id="titularContainer">
	<div class="span-24">&nbsp</div>
	<div class="span-6 prepend-9 last title_data">
         Ingreso de Datos de Viajeros<br/>
    </div>
    <input type="hidden" id="hdnElderlyAge" name="hdnElderlyAge" value="{$elderlyAge}">
    
    {if $sessionProducts}
    	<div class="span-11 prepend-6 append-7 top_text">
	    	* Seleccione el Producto para el cual desea ingresar los datos.<br/>
	    	* Todos los datos ingresados, ser&aacute;n almacenados momentaneamente hasta finalizar la compra. Si sale de esta p&aacute;gina, la informaci&oacute;n se perder&aacute;.<br/>
	    	<img src="{$document_root}img/alert.png" border="0" class="product_alert"/>Este &iacute;cono indica que la informaci&oacute;n del producto a&uacute;n no ha sido ingresada en su totalidad y no ser&aacute; agregado a la compra. 
	    </div>
	    
	    <div class="span-14 prepend-5 last">
	        <ul id="alerts" class="alerts"></ul>
	    </div>
    
	    <div class="span-24">&nbsp;</div>
	    <div class="span-24 line">
	    	<div class="span-6">&nbsp;</div>
	    	<div class="span-12">
	    		{foreach from=$sessionProducts item=pro key=cid}
					<div class="redTab {if $cid != $productSelected}unselectedTab{/if} clickable" id="productBox_{$cid}">
			        	{if !$pro.informationComplete}
			        		<img src="{$document_root}img/alert.png" border="0" class="product_alert"/>
			        		<div class="innerRedText">Producto {$cid+1}</div>
			        	{else}
							<div class="innerRedTextMargin">Producto {$cid+1}</div>
			        	{/if}
			        </div>
		        {/foreach}
		    </div>
		    
		    <div class="span-6">&nbsp;</div>
		    <div class="span-12 register_main">
		    	<div id="cards_content">
		    		<div class="">&nbsp;</div>
		    		<div class="red_title">
						Tarjetas
					</div>
			    	{foreach from=$sessionProducts.$productSelected.cards item=card key=cdid}
					    <div class="status_header line">
					    	<div class="closeCross span-1" id="closeCross_{$cdid}">
								<img src="{$document_root}img/remove-icon.png" border="0" class="status_icon clickable" id="closeCross_{$cdid}"/>
							</div>
					    	<div id="cardBox_{$cdid}" class="clickable">
					    		<div class="span-7 last">
									{$sessionProducts.$productSelected.name_pbl} {$cdid+1}
								</div>
								{if $card.informationComplete}
									<div class="span-3 green_text">
										<img src="{$document_root}img/customer/green-dot.png" border="0" class="status_icon"/>
										Ingresado
									</div>
						    	{else}
						    		<div class="span-3 red_text">
							    		<img src="{$document_root}img/customer/red-dot.png" border="0" class="status_icon"/>
										Incompleta
									</div>
								{/if}
							</div>
						</div>
				    {/foreach}
				</div>
				<form id="travelersInfoForm" class="travelersInfoForm" action="{$document_root}modules/estimator/registered/pSaveTravelersData" method="post">
					<input type="hidden" id="prodIndex" name="prodIndex" value="{$productSelected}">
					<div class="blue_title">
						{$sessionProducts.$productSelected.name_pbl}
					</div>
					
					<div id="extrasRestriction" style="display: none;">{$sessionProducts.$productSelected.extrasRestriction}</div>
					<div id="seniorRestriction" style="display: none;">{$sessionProducts.$productSelected.seniorRestriction}</div>
					
					<img src="{$document_root}img/customer/dotted-line.png" border="0" class="push_right"/>
					<div class="red_title prepend-3">
						Viajeros del Producto #{$productSelected+1}: {$sessionProducts.$productSelected.name_pbl}
					</div>
					<div class="span-11">&nbsp;</div>
					<div class="span-11 prepend-1">
						<div class="desc_row_a prodInfoHeader last">Fecha de Salida:</div>
						<div class="span-2 prodInfoDesc last">{$sessionProducts.$productSelected.begin_date_pod}</div>
						
						<div class="desc_row_a prodInfoHeader last">Fecha de Regreso:</div>
						<div class="span-2 prodInfoDesc last">{$sessionProducts.$productSelected.end_date_pod}</div>
					</div>
					
					<div class="span-11 prepend-1">
						<div class="desc_row_a prodInfoHeader last">Precio Titular:</div>
						<div class="span-2 prodInfoDesc last">{$sessionProducts.$productSelected.symbol_cur}&nbsp;{$sessionProducts.$productSelected.cost_pod|number_format:2:",":"."}</div>
						<div class="desc_row_a prodInfoHeader last">Precio Esposa:</div>
							<div class="span-2 prodInfoDesc last">
								{if $sessionProducts.$productSelected.price_spouse_pod > 0 }
									{$sessionProducts.$productSelected.symbol_cur}&nbsp;{$sessionProducts.$productSelected.price_spouse_pod|number_format:2:",":"."}
								{else}
									N/A
								{/if}
							</div>
					</div>
					
					<div class="span-11 prepend-1">
						<div class="desc_row_a prodInfoHeader last">Familiar Mayor de Edad:</div>
						<div class="span-2 prodInfoDesc last">
							{if $sessionProducts.$productSelected.extrasRestriction != 'CHILDREN' && $sessionProducts.$productSelected.price_extra_pod > 0}
								{$sessionProducts.$productSelected.symbol_cur}&nbsp;{$sessionProducts.$productSelected.price_extra_pod|number_format:2:",":"."}
							{else}
								N/A
							{/if}
						</div>
						
						<div class="desc_row_a prodInfoHeader last">Mayores de Edad Incluidos:</div>
						<div class="span-2 prodInfoDesc last">
							{if $sessionProducts.$productSelected.extrasRestriction != 'CHILDREN' && $adultsExtra > 0}
								{$adultsExtra}
							{else}
								N/A
							{/if}
						</div>
					</div>
					
					<div class="span-11 prepend-1">
						<div class="desc_row_a prodInfoHeader last">Familiar Menor de Edad:</div>
						<div class="span-2 prodInfoDesc last">
							{if $sessionProducts.$productSelected.extrasRestriction != 'ADULTS' && $sessionProducts.$productSelected.price_children_pod > 0}
								{$sessionProducts.$productSelected.symbol_cur}&nbsp;{$sessionProducts.$productSelected.price_children_pod|number_format:2:",":"."}
							{else}
									N/A
							{/if}
						</div>
						
						<div class="desc_row_a prodInfoHeader last">Menores de Edad Incluidos:</div>
						<div class="span-2 prodInfoDesc last">
							{if $sessionProducts.$productSelected.extrasRestriction != 'ADULTS' && $childrenExtra > 0}
								{$childrenExtra}
							{else}
									N/A
							{/if}
						</div>
					</div>
					
					<div class="span-11 prepend-1">
						<div class="span-6 prodInfoHeader last">M&aacute;ximo n&uacute;mero de acompa&ntilde;antes por tarjeta:</div>
						<div class="span-1 prodInfoDesc last">
							{if $maxExtras > 0}
								{$maxExtras}
							{else}
									N/A
							{/if}
						</div>
					</div>
					<div class="span-12">&nbsp;</div>
						
					<div id="productTravelersInfoContainer" class="span-12"></div>
						
					<div class="span-12" align="center">
						<div class="buttonsBox" align="center">
							<div class="blueButton shortWidth floatLeft" id="btnExit">Salir</div>
							<div class="blueButton shortWidth floatRight" id="btnSend">Guardar</div>
				    	</div>
				    </div>
				</form>
			</div>
		</div>
	{else}
		<div class="span-24">&nbsp</div>
		<div class="prepend-4 span-16 label">No existen productos pendientes de venta, utilice el Carrito de Compras para seleccionar sus productos</div>
	{/if}
	<div class="prepend-6 span-12" align="center">
		{if $goodToGo}
			<div class="blueButton longWidth" id="continueBox">Comprar Productos Ingresados</div>
		{/if}
		<div class="blueButton shortWidth" id="returnBox">Volver</div>
	</div>
</div>