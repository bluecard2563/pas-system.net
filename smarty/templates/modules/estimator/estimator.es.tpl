{assign var="title" value="COTIZADOR DE PRODUCTOS"} 
<div class="span-24 last " align="center">
	<div id="quoteEstimatorFull" class="prepend-4 span-13">
		{estimator_full}
	</div>
</div>
<div id="dialogBenefits" title="Detalle de Beneficios">
	{include file="templates/modules/estimator/productBenefitsDialog.tpl"}
</div>
<div id="dialogGeneralConditions" title="Condiciones Generales">
	{include file="templates/fGeneralConditionsSelector.tpl"}
</div>
<script type="text/javascript">
	goTo = {$goTo}+'';
</script>