{assign var=title value="ASIGNACI&Oacute;N DE TARIFAS DE PRODUCTOS POR PAISES"}
<form name="frmPBC" id="frmPBC" method="post" action="{$document_root}modules/webPOS/pRegisterPricesPOS">
    <div class="span-24 last title line">
        Asignaci&oacute;n de Tarifas de productos por Pa&iacute;s
    </div>

    <div class="span-24 last line label">
        <h4><strong>Nombre del Producto:</strong> {$data.name_pbl}</h4>
    </div>
	
    {if $data.ref_prices}
        <div class="prepend-5 span-19 last line">
            <h4><strong>Precios Referenciales:</strong></h4>
        </div>

        <div class="prepend-8 {if $data.price_by_range_pro eq 'NO'}span-7{else}span-9{/if} line last">
			<table id="referentialPrices" border="0">
			<tr bgcolor="#284787">
				<td width="22" style="padding:0 5px 0px 5px; font-weight: bold; color: #FFFFFF;">#</td>
				<td width="40" style="padding:0 5px 0px 5px; font-weight: bold; color: #FFFFFF;">D&iacute;as</td>
				{if $data.price_by_range_pro eq 'NO'}
					<td width="30" style="padding:0 5px 0px 5px; font-weight: bold; color: #FFFFFF;">Precio</td>
				{else}
					<td width="30" style="padding:0 5px 0px 5px; font-weight: bold; color: #FFFFFF;">Precio M&iacute;nimo</td>
					<td width="30" style="padding:0 5px 0px 5px; font-weight: bold; color: #FFFFFF;">Precio M&aacute;ximo</td>
				{/if}
				<td width="20" style="padding:0 5px 0px 5px; font-weight: bold; color: #FFFFFF;">Costo</td>
			</tr>

			{foreach from=$data.ref_prices item="p" name="ref_prices"}
			<tr class="{if $data.price_by_range_pro eq 'NO'}span-7{else}span-9{/if} last {cycle values='even-row,odd-row'} ">
				<td width="22">{$smarty.foreach.ref_prices.iteration}</td>
				<td width="40">{$p.duration_ppc}</td>
				{if $data.price_by_range_pro eq 'NO'}
					<td width="30">{$p.price_ppc}</td>
				{else}
					<td width="30">{$p.min_ppc}</td>
					<td width="30">{$p.max_ppc}</td>
				{/if}
				<td width="20">{$p.cost_ppc}</td>
			</tr>
			{/foreach}
			</table>
		</div>
		<div class="prepend-8 buttons line {if $data.price_by_range_pro eq 'NO'}span-7{else}span-9{/if} last line" id="pageNavPosition"></div>
        
    {/if}
	
	<div class="span-24 last line separator">
		<label>Precios para el POS: XXXXXX</label>
	</div>

    <div class="prepend-6 span-12 last">
        <ul id="alerts" class="alerts"><li><strong>* CLIC EN EL MENSAJE DE ERROR PARA IR HASTA EL ELEMENTO *</strong><br /><br /></li></ul>
    </div>
	
    <div class="prepend-6 {if $data.price_by_range_pro eq 'NO'}span-12 append-6{else}span-15 append-5{/if} last">
        {*<!--<div class="span-1">#</div>-->*}
        <div class="prepend-1 span-12 last line">
            <label for="percentage">Porcentaje Costo:</label>
            <input type="text" name="percentage" id="percentage" class="span-2" />%
        </div>
        <div class=" span-3 subtitle">D&iacute;as</div>

        {if $data.price_by_range_pro eq 'NO'}
            <div class="span-3 subtitle price">Precio</div>
        {else}
            <div class="span-3 subtitle min" >Min.</div>
            <div class="span-3 subtitle max">M&aacute;x.</div>
        {/if}

        <div class="span-3 subtitle last">Costo</div>

        <div id="price_by_product">
            {foreach from=$data.prices name=prices key=key item=p}
            <div id="price_{$key}" class="{if $data.price_by_range_pro eq 'NO'}span-12 append-6{else}span-15 append-5{/if} last line">
                {*<!--<div class="span-1" id="number">1</div>-->*}

                <div class="span-3 subtitle">
                    <input type="text" id="duration_ppc_{$key}" name="price_by_product[{$key}][duration_ppc]" value="{$p.duration_pps}" class="span-2 duration" title='El campo "Dias" es obligatorio' />
                </div>

               {if $data.price_by_range_pro eq 'NO'}
                    <div class="span-3 subtitle">
                        <input type="text" id="price_ppc_{$key}" name="price_by_product[{$key}][price_ppc]" value="{$p.price_pps}" class="span-2 getcost" title='El campo "Precio" es obligatorio' />
                    </div>
                {else}
                    <div class="span-3 subtitle">
                        <input type="text" id="min_ppc_{$key}" name="price_by_product[{$key}][min_ppc]" value="{$p.min_pps}" class="span-2 getcost" title='El campo "M&iacute;nimo" es obligatorio' />
                    </div>
                    <div class="span-3 subtitle">
                        <input type="text" id="max_ppc_{$key}" name="price_by_product[{$key}][max_ppc]" value="{$p.max_pps}" class="span-2 maxi" title='El campo "M&aacute;ximo" es obligatorio' />
                    </div>
                {/if}

                <div class="span-3 subtitle last">
                    <input type="text" id="cost_ppc_{$key}" name="price_by_product[{$key}][cost_ppc]" value="{$p.cost_pps}" class="span-2 cost" title='El campo "Costo" es obligatorio' />
                </div>
                
                <div class="span-3 subtitle last">
                    <input type="button" id="delete_{$key}" class="deletePrice ui-state-default ui-corner-all" value="eliminar"/>
                </div>
            </div>
            {/foreach}
        </div>
		
        <div id="new_prices"></div>
    </div>
    
    <div class="span-24 last line title">
        <input type="button" id="addPrice" value="Nuevo Precio" class="ui-state-default ui-corner-all" />
		<input type="hidden" name="count" id="count" value="{$count}" />
        <input type="hidden" name="range" id="range" value="{$data.price_by_range_pro}" />
		<input type="hidden"  name="repeated" id="repeated" />
		<input type="hidden" name="serial_pbd" id="serial_pbd" value="{$serial_pbd}" />
    </div>

    <div class="span-24 buttons line">
		<input type="submit" name="btnInsertar" id="btnInsertar" value="Guardar Cambios"> |
		<a class="pointer" onclick="location.href='{$document_root}modules/webPOS/fManagePOSPrices'">Cancelar</a>
    </div>
</form>