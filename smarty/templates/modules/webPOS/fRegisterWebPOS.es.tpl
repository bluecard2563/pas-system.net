{assign var=title value="ADMINSITRAR PUNTOS DE VENTA WEB"}
<div class="span-24 last title line">
	Administrar Puntos de Venta Web <br />
	<label>(*) Campos Obligatorios</label>
</div>

<form name="frmRegisterWebPOS" id="frmRegisterWebPOS" action="{$document_root}modules/webPOS/fCreateWebPOS" method="post" />
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
				{if $error eq 1}
					No se pudo cargar la información seleccionada. Por favor vuelva a intentarlo.
				{elseif $error eq 2}
					La informaci&oacute;n se guard&oacute; exitosamente!
				{elseif $error eq 3}
					Hubo errores al guardar la informaci&oacute;n. Si el problema persiste, comun&iacute;quese con el administrador.
				{/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	 <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name="zones" from=$zonesList item="z"}
				<option value="{$z.serial_zon}">{$z.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
        	<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Ciudad:</div>
        <div class="span-5" id="cityContainer">
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

	<div class="span-24 last line">

		<div class="prepend-4 span-4 label">* Comercializador:</div>
        <div class="span-5" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
       </div>
	</div>

    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
</form>
