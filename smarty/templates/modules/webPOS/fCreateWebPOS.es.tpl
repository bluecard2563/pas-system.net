{assign var=title value="REGISTRO DE PUNTO DE VENTA WEB"}
<div class="span-24 last title line">
	Registro de Punto de Venta Web <br />
	<label>(*) Campos Obligatorios</label>
</div>

 <div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmCreateWebPOS" id="frmCreateWebPOS" method="post" action="{$document_root}modules/webPOS/pCreateWebPOS" >
	<div class="span-24 last line separator">
		<label>Info. Gogr&aacute;fica Punto de Venta Web:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Zona:</div>
		<div class="span-5">{$dealer.name_zon}</div>

		<div class="span-3 label">Pa&iacute;s:</div>
		<div class="span-4 append-4 last">{$dealer.dea_name_cou}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Ciudad:</div>
		<div class="span-5">{$dealer.dea_name_cit}</div>

		<div class="span-3 label">Sector:</div>
		<div class="span-4 append-4 last">{$dealer.dea_name_sec}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Punto de Venta Web:</div>
		<div class="span-5">{$dealer.name_dea}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Creado Por:</div>
		<div class="span-4">{if $dealer.creator_name}{$dealer.creator_name}{else}N/A{/if}</div>


		<div class="span-5 label">Fecha de Creaci&oacute;n:</div>
		<div class="span-3">{if $dealer.created_on_dea}{$dealer.created_on_dea}{else}N/A{/if}</div>
	</div>

	<div class="span-24 last line separator">
		<label>Cartilla T&eacute;cnica:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Direcci&oacute;n IP:</div>
		<div class="span-5">
			<input type="text" name="txtIP" id="txtIP" value="{$pos.ip_pos}" title="El campo 'IP' es obligatorio" />
		</div>

		<div class="span-3 label">* Estado:</div>
		<div class="span-4 append-4 last">
			<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$statusList item="l"}
					<option value="{$l}" {if $pos.status_pos eq $l}selected{/if}>{$global_status.$l}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Nombre de Usuario:</div>
		<div class="span-5">
			<input type="text" name="txtUsername" id="txtUsername" value="{$pos.username_pos}" title="El campo 'Nombre de Usuario' es obligatorio" {if $pos.serial_pos}readonly=""{/if} />
		</div>

		<div class="span-3 label">Fecha de Modificaci&oacute;n:</div>
		<div class="span-4 append-4 last">
			{if $pos.lastmodified_pos}{$pos.lastmodified_pos}{else}N/A{/if}
		</div>
	</div>
		
	<div class="prepend-17 span-4 last line" {if !$pos.password_pos}style="display: none;"{/if}>
		<a href="#" id="changePAssword">Actualizar Contrase&ntilde;a</a>
	</div>

	<div class="span-24 last line" id="password_container" {if $pos.password_pos}style="display: none;"{/if}>
		<div class="prepend-4 span-4 label">* Contrase&ntilde;a:</div>
		<div class="span-5">
			<input type="password" name="txtPassword" id="txtPassword" value="{$pos.password_pos}" title="El campo 'Contrase&ntilde;a' es obligatorio" />
		</div>

		<div class="span-3 label">* Confirmar Contrase&ntilde;a:</div>
		<div class="span-4 append-4 last">
			<input type="password" name="txtConfirmPassword" id="txtConfirmPassword" value="{$pos.password_pos}" title="El campo 'Confirmar Contrase&ntilde;a' es obligatorio" />
		</div>
	</div>
	
	<div class="span-24 last center">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Guardar" />
		<input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$dealer.serial_dea}" />
		<input type="hidden" name="hdnSerial_pos" id="hdnSerial_pos" value="{$pos.serial_pos}" />
		<input type="hidden" name="change_password" id="change_password" value="{if $pos.password_pos}NO{else}YES{/if}" />
	</div>
</form>