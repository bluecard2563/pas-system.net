{assign var=title value="ADMINSITRAR PRECIOS DE POS"}
<div class="span-24 last title line">
	Administrar Precios de POS <br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	<div class="append-7 span-10 prepend-7 last">
		<div class="{if $error eq 1 || $error eq 2}success{else}error{/if} " align="center" >
			{if $error eq 1}
				La asignaci&oacute;n de paises se realiz&oacute; correctamente.
			{elseif $error eq 2}
				Las tarifas se asignaron exitosamente.
			{elseif $error eq 3}
				Existi&oacute; un error al asignar las tarifas por favor int&eacute;ntelo nuevamente.
			{elseif $error eq 4}
				El producto seleccionado no existe por favor int&eacute;ntelo nuevamente.
			{/if}
		</div>
	</div>
{/if}

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="prepend-4 span-16 append-4 last line">
	<table border="1" class="dataTable" id="pos_table">
		<thead>
			<tr class="header">
				<th style="text-align: center;">
					Seleccione
				</th>
				<th style="text-align: center;">
					Comercializador Web
				</th>
				<th style="text-align: center;">
					No. Documento
				</th>
			</tr>
		</thead>

		<tbody>
			{foreach name="pos" from=$pos_list item="pos"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.pos.iteration is even} class="odd"{else}class="even"{/if}>
					 <td class="tableField">
						<input type="radio" name="chk[]" id="chkPOS_{$pos.serial_pos}" value="{$pos.serial_dea}"/>
                    </td>
                    <td class="tableField">
                        {$pos.name_dea}
                    </td>
					<td class="tableField">
                        {$pos.id_dea}
                    </td>
                </tr>
			{/foreach}
		</tbody>
	</table>
</div>
		
<div class="span-24 last line" id="productContainer"></div>