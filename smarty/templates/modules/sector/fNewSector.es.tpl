{assign var="title" value="NUEVO SECTOR"}
<form name="frmNewSector" id="frmNewSector" method="post" action="{$document_root}modules/sector/pNewSector" class="">

	<div class="span-24 last title ">
    	Registro de Nuevo Sector<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    		{if $error eq 1}
            	El sector se ha registrado exitosamente!
        	{else}
            	No se pudo insertar el sector.
       		{/if}
    		</div>
        </div>
    </div>
    {/if}
    
    <div class="append-7 span-10 prepend-8 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
   	<div class="span-24 last line"> 
        <div class="prepend-8 span-4">
            <label>Zona:</label>
        </div>        
        <div class="span-9 last">     	                                  
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
              <option value="">- Seleccione -</option>
              {foreach from=$nameZoneList item="l"}
              <option value="{$l.serial_zon}" {if $data.serial_zon eq $l.serial_zon}selected="selected"{/if} >{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>
    </div>
    
    <div class="span-24 last line"> 
        <div class="prepend-8 span-4">
                <label>*Pa&iacute;s:</label>
        </div>
        <div class="span-9 last" id="countryContainer">     	                                  
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    
    <div class="span-24 last line"> 
        <div class="prepend-8 span-4">
                <label>*Ciudad:</label>
        </div>
        <div class="span-9 last" id="cityContainer">     	                                  
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    
    <div class="span-24 last line"> 
        <div class="prepend-8 span-4">
             <label>*Codigo del sector:</label>
        </div>
        <div class="span-10 last">
             <input type="text" name="txtCodeSector" id="txtCodeSector" title="">
        </div> 
    </div>
    
	<div class="span-24 last line"> 
        <div class="prepend-8 span-4">
             <label>*Nombre del sector:</label>
        </div>
        <div class="span-10 last">
             <input type="text" name="txtNameSector" id="txtNameSector" title="">
        </div> 
	</div>
	<div class="span-24 last buttons line">
		<input type="submit" name="btnInsert" id="btnInsert" value="Insertar" >
	</div>
    <input type="hidden" name="hdnFlagCodeSector" id="hdnFlagCodeSector" value="1" />
    <input type="hidden" name="hdnFlagNameSector" id="hdnFlagNameSector" value="1" />
</form>