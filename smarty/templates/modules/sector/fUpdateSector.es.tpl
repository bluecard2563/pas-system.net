{assign var="title" value="ACTUALIZAR SECTOR"}

<form name="frmUpdateSector" id="frmUpdateSector" method="post" action="{$document_root}modules/sector/pUpdateSector" class="form_smarty">

	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Sector<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 error" align="center">
            {if $error eq 1}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
	   
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
              
    <div class="span-24 last">
    	<div class="span-24 last line"> 
            <div class="prepend-8 span-4 append-0">
                <label>*Nuevo c&oacute;digo del sector:</label>
            </div>
            <div class="span-10 last">
                <div class="">
                    <input type="text" name="txtCodeSector" id="txtCodeSector" value="{$data.code_sec}" >
                </div>
            </div>    
        </div>
        
        <div class="span-24 last line"> 
            <div class="prepend-8 span-4 append-0">
                <label>*Nuevo nombre del sector:</label>
            </div>
            <div class="span-10 last">
                <div class="">
                    <input type="text" name="txtNameSector" id="txtNameSector" value="{$data.name_sec}" >
                </div>
            </div>
        </div>
	</div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_sec" id="hdnSerial_sec" {if $data.serial_sec}value="{$data.serial_sec}"{/if} >
        <input type="hidden" name="hdnSerial_cit" id="hdnSerial_cit" {if $data.serial_cit}value="{$data.serial_cit}"{/if} >
    </div>
</form>