{assign var="title" value="BUSCAR COUNTER"}
<script type="text/javascript" src="{$document_root}js/modules/counter/searchCounterCommons.js"></script>
<form name="frmSearchCounter" id="frmSearchCounter" method="post" action="{$document_root}modules/counter/fUpdateCounter" class="">
	<div class="span-24 last title ">
    	b&uacute;squeda de Counter<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-11 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	El counter se ha actualizado exitosamente!
        {elseif $error eq 2}
            	El counter ingresado no consta en el sistema.
        {elseif $error eq 3}
            	Hubo errores en la actualizaci&oacute;n
        {/if}
        </div>
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
   
	 <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
        <div class="span-5">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" >* Comercializador:</div>
        <div class="span-5" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Sucursal:</div>
        <div class="span-5" id="branchContainer">
        	<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
                    <option value="">- Seleccione -</option>
            </select>
         </div>
	</div>
    
    <div class="span-18 prepend-6 last line" id="countersContainer">
        
	</div>
   
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" style="display: none;" value="Buscar" >
   </div>
   <input type="hidden" name="hdnCounterID" id="hdnCounterID" value="" />
   <input type="hidden" name="hdnUpdateType" id="hdnUpdateType" value="NORMAL" />
</form>