{assign var="title" value="ACTUALIZAR COUNTER TELEF&Oacute;NICO"}
<form name="frmAssignPhoneCounter" id="frmAssignPhoneCounter" method="post" action="{$document_root}modules/counter/pAssignPhoneCounter" class="">
	<div class="span-24 last line title">
		Actualizaci&oacute;n de Counter Telef&oacute;nico<br />
		<label>(*) Campos Obligatorios</label>
	</div>

	{if $error}
	<div class="append-7 span-10 prepend-7 last line">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
		{if $error eq 1}
			El counter se ha actualizado exitosamente!
		{elseif $error eq 2}
			No se pudo actualizar el counter.
		{/if}
		</div>
	</div>
	{/if}

	<div class="append-7 span-10 prepend-7 last line">
		<ul id="alerts" class="alerts"></ul>
	</div>

	{if $countryList}
	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
		<div class="span-5">
			<select name="selCountryBranch" id="selCountryBranch" title='El campo "Pa&iacute;s" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_dea}">{$l.name_cou}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="prepend-5 span-24 append-5 last line" id="userContainer"></div>


	{else}
		<center>No existen pa&iacute;ses con comercializadores que realicen ventas telef&oacute;nicas.</center>
	{/if}
</form>