{assign var="title" value="NUEVO COUNTER"}
<form name="frmNewCounter" id="frmNewCounter" method="post" action="{$document_root}modules/counter/pNewCounter" class="">
	<div class="span-24 last title ">
    	Nuevo  Counter<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1 or $error eq 9}success{else}error{/if}" align="center">
    	{if $error eq 1}
			El counter se ha registrado exitosamente!
        {elseif $error eq 2}
			El usuario no se pudo crear. Por lo que la creaci&oacute;n del counter no pudo concluuir.
		{elseif $error eq 3}
			El usuario se cre&oacute; exitosamente, pero no se pudo asignar el counter.
		{elseif $error eq 4}
			El counter se cre&oacute; exitosamente, pero no se envi&oacute; el e-mail con su usario y contrase&ntilde;a.
                {elseif $error eq 5}
			El comercializador ya tiene un lider asignado. Por favor actualice o inactive el lider anterior antes de continuar.
        {/if}
        </div>
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
   
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.'> 
        </div>
        
        <div class="span-3 label">* Apellido</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio'> </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
        <div class="span-5"> <input type="text" name="txtDocument" id="txtDocument" title='El campo "Identificaci&oacute;n" es obligatoria'> </div>
        
        <div class="span-3 label">Direcci&oacute;n:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio'> </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
        <div class="span-5">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
				{/foreach}
			</select>
        </div>
		
        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">
			<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
				<option value="">- Seleccione -</option>
			</select>
         </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" >* Comercializador:</div>
        <div class="span-5" id="dealerContainer">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
				<option value="">- Seleccione -</option>
			</select>
         </div>
    </div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Sucursal:</div>
        <div class="span-5" id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio'>
					<option value="">- Seleccione -</option>
			</select>
         </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-5"> <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio'>
        </div>
        
        <div class="span-3 label">Celular:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio'>
        </div>
	</div>
    
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5"> <input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio'> </div>
        
        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio'> </div>
    </div>
    
        <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Cargo:</div>
        <div class="span-5"> <input type="text" name="txtPosition" id="txtPosition"> </div>
        <div class="span-3 label">* Es Lider:</div>
        <div class="span-4 append-4 last"id="isleader"> 
            <select name="isleader" id="isleader" title='El campo "Es Lider" es obligatorio'>
                    <option value="NO">NO</option>
                    <option value="YES">SI</option>
            </select>
        </div>
	</div>
	
       
      
     
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Tipo:</div>
        <div class="span-5"> Counter</div>
        <div class="span-3 label">* Perfil:</div>
        <!--<div class="span-4 append-4 last">Counter</div>-->
		<div class="span-4 append-4 last" id="profileContainer">
        	<select name="selProfile" id="selProfile" title='El campo "Sucursal de Comercializador" es obligatorio'>
                    <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>
    
       
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" >
   </div>
   <input type="hidden" name="hdnUserID" id="hdnUserID" value="" />
   <!--<input type="hidden" name="hdnProfile" id="hdnProfile" value="Counter" />-->
</form>