{assign var="title" value="ACTUALIZAR COUNTER"}
 <div class="span-24 last title line">
    Actualizaci&oacute;n de Counter<br />
    <label>(*) Campos Obligatorios</label>
</div>
{if $data}
<form name="frmUpdateCounter" id="frmUpdateCounter" method="post" action="{$document_root}modules/counter/pUpdateCounter" class="">
    {if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 error" align="center">
    	{if $error eq 1}
            	Hubo errores en la actualizaci&oacute;
        {/if}
        {if $error eq 2}
            	El comercializador ya tiene un lider asignado. Por favor actualice o inactive el lider anterior antes de continuar.
        {/if}
        </div>
    </div>
    {/if}

   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>

   <div class="span-24 last line">
        <div class="prepend-3 span-8 label"> Comercializador:</div>
        <div class="span-8 append-4 last">
        	{$data.dealer}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-8 label"> Sucursal de Comercializador:</div>
        <div class="span-8 append-4 last">
        	{$data.branch}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-8 label"> Asesor de Venta:</div>
        <div class="span-8 append-4 last">
        	{$data.counter}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-8 label">* Estado:</div>
        <div class="span-8 append-4 last">
            <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item="l"}
                    <option value="{$l}" {if $l eq $data.status_cnt} selected="selected"{/if}>{$global_status.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-3 span-8 label">Datos del usuario:</div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">ID:</div>
        <div class="span-5">{$userData.document_usr}</div>
        
        <div class="span-3 label">Username:</div>
        <div class="span-3"> 
        	{$userData.username_usr}
        </div>
        <div class="span-4 append-1 last"> 
        	&nbsp;
        	{if $hdnUpdateType eq 'SPECIAL'}
        		<input type="checkbox" name="chkChangeUsername" id="chkChangeUsername" onclick="if(!confirm('El username se volvera a generar automaticamente al atualizar y sera enviado por mail.'))return false;">
        		Cambiar username
        	{/if}
        </div>
	</div>
	<input type="hidden" name="hdnUpdateType" id="hdnUpdateType" value="{$hdnUpdateType}" />
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Tel&eacute;fono:</div>
        <div class="span-5">
        	<input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio.' value="{$userData.phone_usr}">
        </div>
        
         <div class="span-3 label">Direcci&oacute;n:</div>
        <div class="span-6 append-2 last">
        	<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio.' value="{$userData.address_usr}">
        </div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Pa&iacute;s:</div>
        <div class="span-5">{$userData.country_usr}</div>
        
        <div class="span-3 label">Ciudad:</div>
        <div class="span-4 append-4 last"> 
        	{$userData.city_usr}
        </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5"> <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$userData.first_name_usr}"> </div>
        
        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last"> 
        	<input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$userData.last_name_usr}">
        </div>
	</div>
	
	 <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Celular:</div>
        <div class="span-5"> <input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio' value="{$userData.cellphone_usr}"> </div>
        
        <div class="span-3 label">* Fecha de Nacimiento:</div>
        <div class="span-4 append-4 last"> 
        	<input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio' value="{$userData.birthdate_usr}">
        </div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* E-mail:</div>
        <div class="span-5"> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio' value="{$userData.email_usr}"> </div>

		<div class="span-3 label">* Cargo:</div>
        <div class="span-4 append-4 last"> 
        	<input type="text" name="txtPosition" id="txtPosition" title='El campo "Cargo" es obligatorio' value="{$userData.position_usr}">
        </div>
	</div>
        {if $bonusTo eq 'DEALER'}
         <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Es Lider:</div>
        <div class="span-5"id="isleader"> 
             <select name="isleader" id="isleader" title='El campo "Es Lider" es obligatorio'>
                    <option value="NO" {if "NO" eq $userData.isLeader_usr} selected="selected"{/if}>NO</option>
                    <option value="YES" {if "YES" eq $userData.isLeader_usr} selected="selected"{/if}>SI</option>
            </select>
       </div>
	   <div class="span-3 label">Uso pago PlacetoPay:</div>
             <div class="span-5"id="ptp_use">
                 <select name="ptp_use" id="ptp_use" title='El campo "Uso pago PlacetoPay" es obligatorio'>
                     <option value="NO" {if "NO" eq $userData.ptp_use} selected="selected"{/if}>NO</option>
                     <option value="YES" {if "YES" eq $userData.ptp_use} selected="selected"{/if}>SI</option>
                 </select>
             </div>
	</div>
            {else}
                <input type="hidden" name="isleader" id="isleader" value="NO"/>
        {/if}
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Actualizar" >
   </div>
   <input type="hidden" name="hdnCounterID" id="hdnCounterID" value="{$data.serial_cnt}" />
</form>
{else}
<div class="append-7 span-10 prepend-7 last">

     {if $error}

    	<div class="span-11 error" align="center">
    	{if $error eq 1}
            	Hubo errores en la actualizaci&oacute;
        {/if}
        {if $error eq 2}
            	El comercializador ya tiene un lider asignado. Por favor actualice o inactive el lider anterior antes de continuar.
        {/if}
        </div>

    {/if}

</div>
{/if}