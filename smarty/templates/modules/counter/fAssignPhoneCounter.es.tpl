{assign var="title" value="ASIGNAR COUNTER TELEF&Oacute;NICO"}
<form name="frmAssignPhoneCounter" id="frmAssignPhoneCounter" method="post" action="{$document_root}modules/counter/pAssignPhoneCounter" class="">
	<div class="span-24 last line title">
		Asignaci&oacute;n de Counter Telef&oacute;nico<br />
		<label>(*) Campos Obligatorios</label>
	</div>

	{if $error}
	<div class="append-7 span-10 prepend-7 last">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
		{if $error eq 1}
			El counter se ha registrado exitosamente!
		{elseif $error eq 2}
			No se pudo insertar el counter.
		{elseif $error eq 3}
			El counter se ha registrado exitosamente. Pero no se pudo insertar el counter general.
		{/if}
		</div>
	</div>
	{/if}

	<div class="append-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	{if $countryList}
	<div class="span-24 last line separator">
		<label>Asignar al pa&iacute;s:</label>
    </div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
		<div class="span-5">
			<select name="selCountryBranch" id="selCountryBranch" title='El campo "Pa&iacute;s" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_dea}">{$l.name_cou}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line separator">
    	<label>Seleccionar Usuario:</label>
    </div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Representante:</div>
		<div class="span-5">
			<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
				<option value="">- Seleccione -</option>
				 {foreach from=$managerList item="l"}
					<option value="{$l.serial_mbc}">{$l.name_man} ({$l.name_cou})</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Usuario:</div>
		<div class="span-5 last" id="userContainer">
		   <select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio.'>
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>


	<div class="span-24 last buttons line">
		<input type="submit" name="btnInsert" id="btnInsert" value="Asignar" >
	</div>
	{else}
		<center>No existen pa&iacute;ses con comercializadores que realicen ventas telef&oacute;nicas.</center>
	{/if}
</form>