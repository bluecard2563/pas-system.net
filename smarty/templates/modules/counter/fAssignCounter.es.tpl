{assign var="title" value="ASIGNAR COUNTER"}
<form name="frmAssignCounter" id="frmAssignCounter" method="post" action="{$document_root}modules/counter/pAssignCounter" class="">
	<div class="span-24 last title ">
    	Asignaci&oacute;n de Counter<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	El counter se ha registrado exitosamente!
        {elseif $error eq 2}
            	No se pudo insertar el counter.
        {/if}
        </div>
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                 {foreach from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

		<div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
    </div>
    
    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Comercializador:</div>
        <div class="span-5" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Sucursal de Comercializador:</div>
        <div class="span-5" id="branchContainer">
        	<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
   <!-- <div class="span-24 last line">
        <div class="prepend-5 span-8 label">Ingrese el nombre o documento del usuario:</div>
        <div class="span-6 append-5 last">
            <input type="text" name="txtCounterData" id="txtCounterData" title="El campo de b&uacute;squeda es obligatorio."  />
        </div>
    </div>-->

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">*Seleccione el Usuario:</div>
        <div class="span-5 last" id="userContainer">
           <select name="selUser" id="selUser" title='El campo "Seleccione Usuario" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

   
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Asignar" >
   </div>
   <input type="hidden" name="hdnUserID" id="hdnUserID" value="" />
</form>