{assign var="title" value="ASIGNAR USUARIO A GRUPO DE ASISTENCIA"}
<form name="frmAssignAlert" id="frmAssignAlert" method="post" action="{$document_root}modules/alerts/pAssignAlerts">
	<div class="span-24 last title ">
    	Reasignaci&oacute;n de Alertas de Grupos de Asistencia<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	La asignaci&oacute;n a sido Exitosa!
        {elseif $error eq 2}
            	No Existen suficientes datos para realizar la asignaci&oacute;n.
		{elseif $error eq 3}
				Hubo un error en la asignaci&oacute;n, vuelva a intentarlo.
		{elseif $error eq 4}
				No existen alertas creadas.
		{/if}
        </div>
    </div>
    {/if}

   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
{if $error neq 4}
   <div class="span-24 last line separator">
    	<label>Selecci&oacute;n de Grupos</label>
   </div>

   <div class="span-24 last line">
	   <div class="prepend-3 span-4 label">* Grupo con alertas:</div>
        <div class="span-5">
			<select name="selFromGroup" id="selFromGroup" title='El campo "Grupo con Alertas" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$fromGroups item="fg"}
                    <option value="{$fg.serial_asg}">{$fg.code_cou}-{$fg.name_asg}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Grupo a Asignar Alertas:</div>
        <div class="span-4 append-4 last" id="groupToContainer">
        	<select name="selToGroup" id="selToGroup" title='El campo "Grupo a Asignar Alertas" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
	<div id="alertsContainer">
	</div>
{/if}
</form>