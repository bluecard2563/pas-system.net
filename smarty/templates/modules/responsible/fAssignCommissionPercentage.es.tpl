{assign var="title" value="ASIGNACI&Oacute;N DE PORCENTAJE DE COMISI&Oacute;N"}
<form name="frmAssignCommission" id="frmAssignCommission" method="post" action="{$document_root}modules/responsible/pAssignCommissionPercentage">
    <div class="span-24 last title">
    	Asignaci&oacute;n de Procentaje de Comisi&oacute;n<br>
          <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El usuario ha sido actualizado exitosamente!
            </div>
		{elseif $error eq 2}
			<div class="span-10 error" align="center">
            	Se ha producido un error en la actualizaci&oacute;n.
            </div>
        {/if}
    </div>
    {/if}

	<div class="span-7 span-10 prepend-7 last alerts">
	    <ul id="alerts" class="alerts"></ul>
    </div>


		<div class="span-24 last line " id="userType"  {if $planetAssistUser}style="display: block"{else}style="display: none"{/if}>
			<div class="prepend-4 span-4 label">* Seleccione:</div>
			<div class="span-5">
				<input type='radio' name='user_type' id='user_type' value='USER'  title='El campo "Tipo de Usuario" es obligatorio' /> Usuarios de Representante
			</div>
			<div class="span-5">
				 <input type='radio' name='user_type' id='user_type' value='PLANET' title='El campo "Tipo de Usuario" es obligatorio' /> Usuarios {$global_system_name}
			</div>
		</div>

	<div class="span-24 last line" id="planetUsersContainer"></div>
<div class="span-24 last line" id="dataContainer" {if $planetAssistUser}style="display: none"{/if}>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
					<option value="{$l.serial_zon}">{$l.name_zon}</option>
				{/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
				<option value="">- Seleccione -</option>
			</select>
         </div>
	</div>
	

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label" >* Representante:</div>
        <div class="span-5" id="managerContainer">
        	<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
		<div class="span-3 label">* Responsable:</div>
        <div class="span-4 append-4 last" id="responsibleContainer">
        	<select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
				<option value="">- Seleccione -</option>
			</select>
         </div>
	</div>
</div>

	<div class="span-24 last line" id="percentageData" {if $planetAssistUser}style="display: none"{/if}>
		<div class="prepend-8 span-4 label" id="countryContainer">* Porcentaje de Comisi&oacute;n:</div>
        <div class="span-5">
			<input type="text" id="txtPercentage" name="txtPercentage" title='El campo "Porcentaje de Comisi&oacute;n" es obligatorio'>
        </div>
	</div>

    <div class="span-24 last buttons">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
	{if $planetAssistUser}
		<input type="hidden" name="hdnPlanetAssistUser" id="hdnPlanetAssistUser" value="{$planetAssistUser}">
	{/if}
</form>