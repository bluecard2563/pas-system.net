{assign var="title" value="NUEVO COORDINADOR INTERNACIONAL"}
<form name="frmNewServiceProvider" id="frmNewServiceProvider" method="post" action="{$document_root}modules/serviceProvider/pNewServiceProvider">
    <div class="span-24 last title">
    	Registro de Nuevo Coordinador Internacional<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            El Coordinador se ha registrado exitosamente!
        {elseif $error eq 2}
            Hubo errores en el ingreso del coordinador. Por favor vuelva a intentarlo.
        {/if}
        </div>
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line separator">
       <label>Informaci&oacute;n General</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5 append-6 last" >
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Ciudad:</div>
        <div class="span-5 append-6 last" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Nombre:</div>
        <div class="span-5 append-6 last">
            <input type="text" name="txtServProvName" id="txtServProvName" title='El campo "Nombre" es obligatorio' />
        </div>
    </div>

	<div class="span-24 last line separator">
       <label>Informaci&oacute;n de Contacto</label>
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5 append-6 last">
            <input type="text" name="txtServProvAddress" id="txtServProvAddress" title='El campo "Direcci&oacute;n" es obligatorio' />
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Email:</div>
        <div class="span-6 append-5 last line">
				<input type="text" name="txtServProvEmail[]" id="txtServProvEmail_0" title='El campo "Email Inicial" es obligatorio' />
				<input type="hidden" name="email_count" id="email_count" value="0" />

				<a id="add_email">A&ntilde;adir</a>
		</div>
		<div class="prepend-11 span-8 last" id="email_container"></div>		
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-6 append-5 last line">
            <input type="text" name="txtServProvPhone[]" id="txtServProvPhone_0" title='El campo "Tel&eacute;fono Inicial" es obligatorio' />
			<input type="hidden" name="phones_count" id="phones_count" value="0" />
			<a id="add_phone">A&ntilde;adir</a>
        </div>
		<div class="prepend-11 span-8 last" id="phones_container"></div>	
    </div>

    <div class="span-24 last line separator">
       <label>Informaci&oacute;n de Servicios</label>
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Tipo de Servicio:</div>
        <div class="span-5 append-6 last">
            <select name="servProvType" id="servProvType" onchange="showAssistanceCosts(this.value);" title='El campo "Tipo de Servicio" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$serviceTypes item="l"}
                <option value="{$l}">{$global_serviceProviderType.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Costo del Servicio:</div>
        <div class="span-5 append-6 last">
            <input type="text" name="txtServProvCost" id="txtServProvCost" title='El campo "Costo del Servicio" es obligatorio' />
        </div>
    </div>

    <div id="assistanceCosts"></div>

    <div class="prepend-11 span-5 append 6 last line">
        <input type="Submit" name="btnAdd" id="btnAdd" value="Agregar Nuevo" />
    </div>
</form>