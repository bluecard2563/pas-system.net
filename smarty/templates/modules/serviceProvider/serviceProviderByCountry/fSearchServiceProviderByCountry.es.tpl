{assign var=title value="MODIFICAR COBERTURA DE COORDINADOR INTERNACIONAL"}
    <div class="span-24 last title">MOdificar Cobertura de Coordinador Internacional</div>
    {if $error}
        <div class="prepend-6 span-12 last">
            <div class="{if $error eq 1}success{else}error{/if} " align="center" >
                {if $error eq 1}
                    El Proveedor ha sido asignado exitosamente.
                {elseif $error eq 2}
                     No existe el proveedor ingresado. Por favor seleccione uno de la lista.
                {elseif $error eq 3 || $error eq 4}
                    Existi&oacute; un error al asignar el proveedor. Por favor intente nuevamente.
                {/if}
                
            </div>
        </div>
    {/if}
{if $providerList}
    <br/>
	<div class="span-12 prepend-6 append-6 last line">
		<table border="dotted" id="providersTable" style="color: #000000;">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            No.
                        </td>
                        <td align="center" class="tableTitle">
                           Coordinador
                        </td>
                        <td align="center"  class="tableTitle">
                            Actualizar
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="provider" from=$providerList item="s"}
                        <tr {if $smarty.foreach.provider.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                               {$smarty.foreach.provider.iteration}
                            </td>
                            <td align="center" class="tableField">
                              {$s.name_spv}
                            </td>
                            <td align="center" class="tableField">
								<a href="{$document_root}modules/serviceProvider/serviceProviderByCountry/fAssignServiceProviderByCountry/{$s.serial_spv}">Actualizar</a>
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
	</div>
<div class="span-24 last pageNavPosition" id="providerPageNavPosition"></div>
{else}
	 <div class="span-10 append-5 prepend-7 label">No existen Coordinadores registrados en el sistema.</div>
{/if}