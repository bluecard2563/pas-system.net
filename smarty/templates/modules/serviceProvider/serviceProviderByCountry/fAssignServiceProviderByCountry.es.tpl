{assign var="title" value="ASIGNACION DE COORDINADOR INTERNACIONAL"}
<form name="frmAssignServiceProviderByCountry" id="frmAssignServiceProviderByCountry" method="post" action="{$document_root}modules/serviceProvider/serviceProviderByCountry/pAssignServiceProviderByCountry" class="">
    <div class="span-24 last title ">
        Asignaci&oacute;n de Coordinador Internacional<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="append-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>*Seleccionar pa&iacute;s(es):</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Zona:</div>
        <div class="span-5">
            <select name="selZoneAssign" id="selZoneAssign" title='Elija una zona.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l" name="zones"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-5 span-6 label">*Seleccione los pa&iacute;ses:</div>
        <div class="prepend-7 span-10 append-4 last line" id="branchContainer">
            <div class="span-4">
                <div align="center">Seleccionados</div>
                <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-4 last" title="Paises Asignados">
                    {if $assigned_countries}
                        {foreach from=$assigned_countries item=ac}
                            <option value="{$ac.serial_cou}" zone="{$ac.serial_zon}">{$ac.name_cou}</option>;
                        {/foreach}
                    {/if}
                </select>
            </div>
            <div class="span-2 last buttonPane">
                <br />
                <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
            </div>
            <div class="span-4 last" id="dealersFromContainer">
                Seleccione una zona
            </div>
        </div>
    </div>


    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Guardar Cambios" />
    </div>
    <input type="hidden" name="hdnServProvSerial" id="hdnServProvSerial" value="{$hdnServProvSerial}"/>
</form>