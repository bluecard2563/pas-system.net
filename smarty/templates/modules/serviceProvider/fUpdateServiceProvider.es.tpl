{assign var="title" value="ACTUALIZAR COORDINADOR INTERNACIONAL"}
<form name="frmUpdateServiceProvider" id="frmUpdateServiceProvider" method="post" action="{$document_root}modules/serviceProvider/pUpdateServiceProvider">
    <div class="span-24 last title">
    	Actualizaci&oacute;n de Coordinador Internacional<br />
        <label>(*) Campos Obligatorios</label>
    </div>

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line separator">
       <label>Informaci&oacute;n General</label>
    </div>    

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Pa&iacute;s</div>
        <div class="span-5 append-6 last">
            <select name="selCountry" id="selCountry" title='El campo "Pa&i&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}" {if $l.serial_cou eq $serviceProvider.aux.serial_cou}selected="selected"{/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Ciudad:</div>
        <div class="span-5 append-6 last" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$cities item="l"}
                <option value="{$l.serial_cit}" {if $l.serial_cit eq $serviceProvider.serial_cit}selected="selected"{/if}>{$l.name_cit}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Nombre:</div>
        <div class="span-5 append-6 last">
            <input type="text" name="txtServProvName" id="txtServProvName" title='El campo "Nombre" es obligatorio' value="{$serviceProvider.name_spv}" />
            <input type="hidden" name="hdnServProvName" id="hdnServProvName" value="{$serviceProvider.name_spv}" />
        </div>
    </div>
	
	<div class="span-24 last line separator">
       <label>Informaci&oacute;n de Contacto</label>
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5 append-6 last">
            <input type="text" name="txtServProvAddress" id="txtServProvAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$serviceProvider.address_spv}" />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Email:</div>
        <div class="span-6 append-5 last line">
            <input type="text" name="txtServProvEmail[]" id="txtServProvEmail_0" title='El campo "Email Inicial" es obligatorio' value="{$serviceProvider.initial_email}" />
            <input type="hidden" name="email_count" id="email_count" value="{$serviceProvider.email_spv|@count}" />
			<a id="add_email">A&ntilde;adir</a>
		</div>
		<div class="prepend-11 span-8 last line" id="email_container">
			{if $serviceProvider.email_spv|@count > 0}
				{foreach from=$serviceProvider.email_spv item="e" key="m"}
					<div class="span-7 last line" id="email_container_{$m}">
						<input type="text" name="txtServProvEmail[]" id="txtServProvEmail_{$m}" title='El campo "Email Adicional" es obligatorio' value="{$e}" />
						&nbsp;<a id="remove_email_{$m}">Quitar</a>
					</div>
				{/foreach}
			{/if}
		</div>
    </div>

	 <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-6 append-5 last line">
            <input type="text" name="txtServProvPhone[]" id="txtServProvPhone_0" title='El campo "Tel&eacute;fono Inicial" es obligatorio' value="{$serviceProvider.initial_phone}" />
			<input type="hidden" name="phones_count" id="phones_count" value="{$serviceProvider.phone_spv|@count}" />
			<a id="add_phone">A&ntilde;adir</a>
		</div>
		<div class="prepend-11 span-8 last line" id="phones_container">
			{if $serviceProvider.phone_spv|@count > 0}
				{foreach from=$serviceProvider.phone_spv item="p" key="k"}
					<div class="span-7 last line" id="container_{$k}">
						<input type="text" name="txtServProvPhone[]" id="txtServProvPhone_{$k}" title='El campo "Tel&eacute;fono Adicional" es obligatorio' value="{$p}" />
						&nbsp;<a id="remove_phone_{$k}">Quitar</a>
					</div>
				{/foreach}
			{/if}
		</div>	
    </div>

	<div class="span-24 last line separator">
       <label>Informaci&oacute;n de Servicios</label>
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Tipo de Servicio:</div>
        <div class="span-5 append-6 last">
            <select name="servProvType" id="servProvType" onchange="showAssistanceCosts(this.value);" title='El campo "Tipo de Servicio" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$serviceTypes item="l"}
                <option value="{$l}" {if $l eq $serviceProvider.type_spv}selected="selected"{/if}>{$global_serviceProviderType.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Costo del Servicio:</div>
        <div class="span-5 append-6 last">
            <input type="text" name="txtServProvCost" id="txtServProvCost" title='El campo "Costo del Servicio" es obligatorio' value="{$serviceProvider.service_spv}" />
        </div>
    </div>

    <div id="assistanceCosts">
        {if $serviceProvider.type_spv eq "MEDICAL"}
            <div class="span-24 last line">
                <div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Simple:</div>
                <div class="span-5 append-6 last">
                    <input type="text" name="txtServProvMedSimCost" id="txtServProvMedSimCost" title="El campo \"Costo de Asistencia M&eacute;dica Simple\" es obligatorio" value="{$serviceProvider.medical_simple_spv}" />
                </div>
             </div>
             <div class="span-24 last line">
                <div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Compleja:</div>
                <div class="span-5 append-6 last">
                    <input type="text" name="txtServProvMedComCost" id="txtServProvMedComCost" title="El campo \"Costo de Asistencia M&eacute;dica Compleja\" es obligatorio" value="{$serviceProvider.medical_complex_spv}" />
                </div>
             </div>
        {elseif $serviceProvider.type_spv eq "TECHNICAL"}
            <div class="span-24 last line">
                <div class="prepend-7 span-4 label">* Costo de Asistencia T&eacute;cnica:</div>
                <div class="span-5 append-6 last">
                    <input type="text" name="txtServProvTechCost" id="txtServProvTechCost" title="El campo \"Costo de Asistencia T&eacute;cnica\" es obligatorio" value="{$serviceProvider.technical_spv}" />
                </div>
            </div>
        {elseif $serviceProvider.type_spv eq "BOTH"}
            <div class="span-24 last line">
                <div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Simple:</div>
                <div class="span-5 append-6 last">
                    <input type="text" name="txtServProvMedSimCost" id="txtServProvMedSimCost" title="El campo \"Costo de Asistencia M&eacute;dica Simple\" es obligatorio" value="{$serviceProvider.medical_simple_spv}" />
                </div>
             </div>
             <div class="span-24 last line">
                <div class="prepend-7 span-4 label">* Costo de Asistencia M&eacute;dica Compleja:</div>
                <div class="span-5 append-6 last">
                    <input type="text" name="txtServProvMedComCost" id="txtServProvMedComCost" title="El campo \"Costo de Asistencia M&eacute;dica Compleja\" es obligatorio" value="{$serviceProvider.medical_complex_spv}" />
                </div>
             </div>
            <div class="span-24 last line">
                <div class="prepend-7 span-4 label">* Costo de Asistencia T&eacute;cnica:</div>
                <div class="span-5 append-6 last">
                    <input type="text" name="txtServProvTechCost" id="txtServProvTechCost" title="El campo \"Costo de Asistencia T&eacute;cnica\" es obligatorio" value="{$serviceProvider.technical_spv}" />
                </div>
            </div>
        {/if}
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">Activo/Inactivo:</div>
        <div class="span-5 append-6 last">
            <select name="servProvStatus" id="servProvStatus" title='Activar/Desactivar Proveedor de Servicios.'>
                {foreach from=$serviceStates item="l"}
                <option value="{$l}" {if $l eq $serviceProvider.status_spv}selected="selected"{/if}>{$global_status.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="prepend-11 span-5 append 6 last line">
        <input type="Submit" name="btnAdd" id="btnAdd" value="Actualizar" />
		<input type="hidden" name="hdnServProvId" value="{$serviceProvider.serial_spv}" />
    </div>
</form>