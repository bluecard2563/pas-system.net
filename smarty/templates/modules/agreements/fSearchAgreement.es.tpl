{assign var="title" value="BUSCAR CONVENIO"}
<form name="frmSearchAgreement" id="frmSearchAgreement" action="{$document_root}modules/agreements/fUpdateAgreement" method="post" class="form_smarty">
    <div class="span-24 last title">
        Buscar Convenio<br />
    </div>

    {if $error}
        <div class="append-7 span-10 prepend-7 last ">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    El convenio se actualiz&oacute; correctamente!
                {else}
                    No existe el convenio ingresado. Por favor seleccione uno de la lista.
                {/if}
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    {if $agreementList}
        <div class="span-24 last line">
            <table border="1" class="dataTable" align="center" id="agreementTable">
                <thead>
                    <tr class="header">
                        <th style="text-align: center;">No.</th>
                        <th style="text-align: center;">Convenio</th>
                        <th style="text-align: center;">Descuento</th>
                        <th style="text-align: center;">Estado</th>
                        <th style="text-align: center;">Acci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                {foreach name="agreement" from=$agreementList item=agr}
                    <tr {if $smarty.foreach.agreement.iteration is even}class="odd"{else}class="even"{/if} >
                        <td class="tableField">{$smarty.foreach.agreement.iteration}</td>
                        <td class="tableField">{$agr.name_agr}</td>
                        <td class="tableField">{$agr.discount_value_agr}</td>
                        <td class="tableField">{$global_status[$agr.status_agr]}</td>
                        <td class="tableField">
                            <a href="{$document_root}modules/agreements/fUpdateAgreement/{$agr.serial_agr}">Actualizar</a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    {else}
        <div class="prepend-8 span-8 append-8 last" align="center">
            <div class="span-8 error last">
                No existen convenios.
            </div>
        </div>
    {/if}
</form>