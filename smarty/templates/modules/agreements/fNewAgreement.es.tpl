{assign var="title" value="NUEVO CONVENIO"}
<form name="frmNewAgreement" id="frmNewAgreement" method="post" action="{$document_root}modules/agreements/pNewAgreement" class="form_smarty">

    <div class="span-24 last title ">
        Registro de Nuevo Convenio<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
        <div class="append-7 span-10 prepend-7 last">
            {if $error eq 1}
                <div class="span-10 success" align="center">
                    El tipo de convenio se ha registrado exitosamente!
                </div>
            {else}
                <div class="span-10 error" align="center">
                    No se pudo insertar.
                </div>
            {/if}
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last">

        <div class="span-24 last line">
            <div class="prepend-7 span-5 append-0">
                <label>*Nombre de convenio:</label>
            </div>
            <div class="span-7 append-2 last">
                <div class="">
                    <input type="text" name="txtNameAgreement" id="txtNameAgreement" title='El campo "Nombre de convenio" es requerido.'>
                </div>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-7 span-5 append-0">
                <div class="">
                    <label>*Descuento (%):</label>
                </div>
            </div>
            <div class="span-7 append-2 last">
                <div class="controles">
                    <input type="text" class="number-input" name="txtDiscountAgreement" id="txtDiscountAgreement" title='El campo "Descuento" es requerido.' onkeypress="return onlyDecimalKeyPress(this,event)">
                </div>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-7 span-5 append-0">
                <div class="">

                    <label>*Estado:</label>
                </div>
            </div>
            <div class="span-7 append-2 last">
                <div class="controles">
                    <select name="selStatusAgreement" id="selStatusAgreement" title='El campo "Estado" es requerido'>
                        <option value="">- Seleccione -</option>
                        <option value="Active">Activo</option>
                        <option value="Inactive">Inactivo</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="span-24 last buttons line">
            <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" class="" >
        </div>
    </div>
</form>