{assign var="title" value="ACTUALIZAR CONVENIO"}
<form name="frmUpdateAgreement" id="frmUpdateAgreement" method="post" action="{$document_root}modules/agreements/pUpdateAgreement" class="form_smarty">

    <div class="span-24 last title ">
        Actualizaci&oacute;n de Convenios<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
        <div class="append-7 span-10 prepend-7 last ">
            <div class="span-10 error" align="center">
                {if $error eq 1}
                    Existen errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-6">
            <div class="">
                <label>*Nombre de convenio:</label>
            </div>
        </div>
        <div class="span-8 append-1 last">
            <div class="">
                <input type="text" name="txtNameAgreement" id="txtNameAgreement" title='El campo "Nombre de convenio" es requerido' {if $data.name_agr}value="{$data.name_agr}"{/if}>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-6">
            <div class="">
                <label>*Descuento (%):</label>
            </div>
        </div>
        <div class="span-8 append-1 last">
            <div class="">
                <input type="text" class="number-input" name="txtDiscountAgreement" id="txtDiscountAgreement" title='El campo "Descuento" es requerido' {if $data.discount_value_agr}value="{$data.discount_value_agr}"{/if} onkeypress="return onlyDecimalKeyPress(this,event)">
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-6">
            <div class="">
                <label>*Estado:</label>
            </div>
        </div>
        <div class="span-8 append-1 last">
            <select name="selStatusAgreement" id="selStatusAgreement" title='El campo "Estado" es requerido.'>
                {foreach from=$agreementStatusList item="status"}
                    <option value="{$status}" {if $data.status_agr eq $status}selected{/if}>{$global_status[$status]}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input maxlength="30" type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_agr" id="hdnSerial_agr" {if $data.serial_agr}value="{$data.serial_agr}"{/if} >
    </div>
</form>