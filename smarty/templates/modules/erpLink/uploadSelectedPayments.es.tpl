{assign var="title" value="REPLICAR VENTA A ERP"}
<div class="span-24 last title">
    Carga Individual de Recibos desde ERP<br/>
</div>

{if $success}
    <div class="span-7 span-10 prepend-7 last">
        <div class="span-10 success center">
            Carga Exitosa!
        </div>
    </div>
{/if}

<div class="span-7 span-10 prepend-7 last">
    <ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line" >
    <form name="frmUploadReceipts" id="frmUploadReceipts" method="post" action="{$document_root}modules/erpLink/uploadSelectedPayments">
        <div class="prepend-4 span-14 append-5 last line">
            <div class="prepend-3 span-4 label">* Fecha Desde:</div>
            <div class="span-6">
                <input type="text" id="txtFrom" name="txtFrom" title="Ingrese la Fecha Desde para la consulta"/>
            </div>
        </div>
        
        <div class="prepend-4 span-14 append-5 last line">
            <div class="prepend-3 span-4 label">* Fecha Hasta:</div>
            <div class="span-6">
                <input type="text" id="txtTo" name="txtTo" title="Ingrese la Fecha Hasta para la consulta"/>
            </div>
        </div>
        
        <div class="prepend-4 span-14 append-5 last line">
            <div class="prepend-3 span-4 label">* IDs de Recibos:</div>
            <div class="span-6">
                <input type="text" id="txtReceipts" name="txtReceipts" class="span-4" title="Ingrese uno o varios recibos separados por comas (,)"/>
            </div>
        </div>

        <div class="span-24 last buttons line">
            <input id="btnRegister" type="button" value="Buscar" name="btnRegister"/>
        </div>
    </form>
</div>