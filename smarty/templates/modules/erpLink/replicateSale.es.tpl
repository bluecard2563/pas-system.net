{assign var="title" value="REPLICAR VENTA A ERP"}
<div class="span-24 last title">
	Replicar Venta a ERP<br/>
</div>

{if $error}
	<div class="span-7 span-10 prepend-7 last">
		<div class="span-10 {if $error eq 1}success{else}error{/if} center">
			{if $error eq 1}
				Replicaci&oacute;n exitosa.
			{else}
				No pudo enviarse la informaci&oacute;n al ERP. Si el problema persiste, consulte al administrador.
			{/if}
		</div>
	</div>
{/if}

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line" >
	<form name="frmSelectSale" id="frmSelectSale" method="post" action="{$document_root}modules/erpLink/pReplicateSale">
		<div class="prepend-4 span-14 append-5 last line">
			<div class="prepend-3 span-4 label">* N&uacute;mero de Tarjeta:</div>
			<div class="span-6">
				<input type="text" id="txtCardNumber" name="txtCardNumber" class="span-6"/>
			</div>
		</div>
		
		<div class="span-24 last buttons line">
			<input id="btnRegister" type="button" value="Buscar" name="btnRegister"/>
		</div>
		
		<div class="span-24 last line" id="OODataContainer"></div>
	</form>
</div>