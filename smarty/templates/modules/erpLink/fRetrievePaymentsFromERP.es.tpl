{assign var="title" value="ALMACENAR COBROS DESDE ERP"}
<form name="frmChooseInvoice" id="frmChooseInvoice" method="post" action="{$document_root}modules/erpLink/pRetrievePaymentsFromERP">
    <div class="span-24 last title">
    	Almacenar Cobros Desde ERP <br/>
		<label>(*) Recuerde que solo se registrar&aacute;n cobros aprobados a la fecha</label>
    </div>
	
	{if $error eq 1}
		<div class="prepend-7 span-10 append-7 last line">
			<div class="span-10 center success">
				La carga se ha realizado con &eacute;xito!
			</div>
		</div>
	{/if}
	
	<div class="prepend-7 span-10 append-7 last line">
		<div class="span-5">
			<label>&Uacute;ltima Fecha de sincronizaci&oacute;n:</label>
		</div>
		<div class="span-5 last">
			{$last_sync_date}
		</div>
	</div>
	
	<div class="span-24 center">
		<input type="button" name="btnSubmit" id="btnSubmit" value="Cargar Cobros" />
	</div>
</form>