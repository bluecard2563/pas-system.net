{assign var="title" value="REPLICAR VENTAS TELEFONICAS"}
<form name="frmChooseInvoice" id="frmChooseInvoice" method="post" action="{$document_root}modules/erpLink/pReplicatePOSSales">
    <div class="span-24 last title">
		Replicar Ventas Telef&oacute;nicas <br/>
    </div>
	
	<div class="prepend-7 span-10 append-7 last line">
	{if $error eq 'success'}
		<div class="span-10 center success">
			La sincronizaci&oacute;n se ha realizado con &eacute;xito!
		</div>
	{elseif $error eq 'error'}
		<div class="span-10 center error">
			Ocurri&oacute; un error en la replicaci&oacute;n. <br />Por favor haga una captura de pantalla y p&aacute;sela al administrador.
		</div>
	{/if}
	</div>
	
	<div class="prepend-7 span-10 append-7 last line">
		<div class="span-5">
			<label>&Uacute;ltima Fecha de sincronizaci&oacute;n:</label>
		</div>
		<div class="span-5 last">
			{$last_sync_date}
		</div>
	</div>
	
	<div class="span-24 center">
		<input type="button" name="btnSubmit" id="btnSubmit" value="Enviar Transacciones" />
	</div>
</form>