{assign var="title" value="BUSCAR IDIOMA"}
<form name="frmSearchLanguage" id="frmSearchLanguage" action="{$document_root}modules/language/fUpdateLanguage" method="post" class="form_smarty">

	<div class="span-24 last title ">
    	Buscar Idioma<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}     
        <div class="prepend-7 span-10 append-7  last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    El idioma se actualiz&oacute; correctamente!
                {else}
                    No existe el idioma ingresado.
                {/if}                                 
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-4 span-19 append-1 last line">
        <div class="prepend-1 span-8 label">*Ingrese el nombre del idioma:</div>
        
        <div class="span-8 append-2 last"> 
                <input type="text" id="txtNameLanguage" name="txtNameLanguage" title="El campo 'Nombre' es obligatorio."  />
        </div>
   </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="serial_lang" id="serial_lang" value="" />
</form>