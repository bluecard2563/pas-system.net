{assign var="title" value="ACTUALIZAR IDIOMA"}
<form name="frmUpdateLanguage" id="frmUpdateLanguage" method="post" action="{$document_root}modules/language/pUpdateLanguage" class="form_smarty">

	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Idioma<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 error" align="center">
            {if $error eq 1}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
	     
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">*Nuevo nombre de Idioma:</div>
        
        <div class="span-4 last"> 
               <input type="text" name="txtNameLanguage" id="txtNameLanguage" title="El campo 'Nombre' es obligatorio." {if $data.name_lang}value="{$data.name_lang}"{/if}>   
        </div>
        <div class="span-4 label">*Nuevo C&oacute;digo:</div>
        
        <div class="span-4 last"> 
                <input type="text" name="txtCodeLanguage" id="txtCodeLanguage" title="El campo 'C&oacute;digo' es obligatorio." {if $data.code_lang}value="{$data.code_lang}"{/if}>      
        </div>
   </div>
    
    <div class="span-24 last buttons line">
    	<input maxlength="30" type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_lang" id="hdnSerial_lang" {if $data.serial_lang}value="{$data.serial_lang}"{/if} >
    </div>
</form>