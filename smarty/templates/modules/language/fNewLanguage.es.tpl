{assign var="title" value="NUEVO IDIOMA"}
<form name="frmNewLanguage" id="frmNewLanguage" method="post" action="{$document_root}modules/language/pNewLanguage" class="">

    <div class="span-24 last title ">
    	Registro de Nuevo Idioma<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El idioma se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error">
            	No se pudo insertar el idioma.
            </div>
        {/if}
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="prepend-3 span-19 append-2 last line">
        <div class="span-4 label">*Idioma:</div>
        
        <div class="span-4 append-1 last"> 
                <input type="text" name="txtNameLanguage" id="txtNameLanguage" title="El campo 'Idioma' es obligatorio.">      
        </div>
        <div class="span-4 label">*C&oacute;digo:</div>
        
        <div class="span-4 append-1 last"> 
                <input type="text" name="txtCodeLanguage" id="txtCodeLanguage" title="El campo 'C&oacute;digo' es obligatorio.">      
        </div>
   </div>
   
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" >
   </div>
</form>