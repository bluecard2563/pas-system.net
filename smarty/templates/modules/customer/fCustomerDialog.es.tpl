<form id="frmCustomerDialog" name="frmCustomerDialog" method="post" action="#">	
	<div class="span-1 span-14 prepend-1 last line">
	    <ul id="alerts_dialog" class="alerts"></ul>
	</div>
	
	{foreach from=$questionList item="q" key="k"}    
		<div class="span-16 last line">
			{if $q.type_que neq YN}<div class="span-16 last line">{/if}
		    <div class="prepend-1 span-12">
		        {$k+1}) {$q.text_qbl}
		    </div>
		    {if $q.type_que neq YN}</div>{/if}
		    <div class="{if $q.type_que eq YN}span-1 append-1{else}prepend-1 span-16{/if}last">
			    {if $q.type_que eq YN}
				    <select name="question_{$q.serial_qbl}" id="question_{$q.serial_qbl}" title="La pregunta {$k+1}) es obligatoria">
				        <option value="">- Seleccione -</option>
				        <option value="YES" {if $q.yesno_ans eq YES}selected{/if}>S&iacute;</option>    
				        <option value="NO" {if $q.yesno_ans eq NO}selected{/if}>No</option>           
				    </select>
			    {else}
			        <textarea name="question_{$q.serial_qbl}" id="question_{$q.serial_qbl}"  title="La pregunta {$k+1}) es obligatoria" class="textArea">{if $q.answer_ans}{$q.answer_ans}{/if}</textarea>
			    {/if}
			    <input type="hidden" name="answer_{$q.serial_qbl}" id="answer_{$q.serial_qbl}" value="{$q.serial_ans}"/>
		    </div>
		</div>
			
		<div class="span-16 last line"></div>
	{/foreach}
</form>