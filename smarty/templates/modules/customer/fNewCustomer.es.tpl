{assign var="title" value="NUEVO CLIENTE"} 
<form name="frmNewCustomer" id="frmNewCustomer" method="post" action="{$document_root}modules/customer/pNewCustomer" class="">

   <div class="prepend-2 span-19 append-3 last title">
    	Registro de Nuevo Cliente<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-8 span-10 prepend-6 last ">
        <div class="span-10 {if $error eq 1}success{else $error}error{/if}" align="center">
            {if $error eq 1}
                El cliente se ha registrado exitosamente!
            {elseif $error eq 2}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {elseif $error eq 3}
                Hubo errores al enviar el e-mail de usuario y contrase&ntilde;a. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-8 span-10 prepend-6 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Documento:</div>
        <div class="span-5">  
        	<input type="text" name="txtDocument" id="txtDocument" title='El campo "Documento" es obligatorio.'> 
        </div>
        
        <div class="span-3 label">*Tipo de Cliente</div>
        <div class="span-4 append-2 last">  
        	<select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$typesList item="l"}
                    <option value="{$l}">{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                {/foreach}
            </select>
        </div>
  </div>

    <div class="prepend-2 span-19 append-3 last line" id="divName">
        <div class="prepend-1 span-4 label">*Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.'> </div>
        
        <div class="span-3 label">*Apellido</div>
        <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio'> </div>
	</div>

        <div class="prepend-2 span-19 append-3 last line" id="divName2" style="display: none">
        <div class="prepend-1 span-4 label">* Raz&oacute;n Social:</div>
        <div class="span-5">
            <input type="text" name="txtFirstName1" id="txtFirstName1" title='El campo "Raz&oacute;n Social" es obligatorio' />
        </div>

    </div>

    <div class="prepend-2 span-19 append-3 last line" id="genderDiv" {if $data.type_cus neq PERSON}style="display: none"{/if}>
        <div class="prepend-1 span-4 label">* G&eacute;nero:</div>
        <div class="span-5">  
            <input type="radio" name="rdGender" id="rdGender" value="M" {if $data.gender_cus eq 'H'}checked{/if} /> Masculino <br />
            <input type="radio" name="rdGender" id="rdGender" value="F" {if $data.gender_cus eq 'M'}checked{/if} /> Femenino
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
        <div class="span-5"> <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}">{$l.name_cou|htmlall}</option>
                {/foreach}
            </select> </div>

        <div class="span-3 label">*Ciudad:</div>
        <div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select> </div>
	</div>
    
    <div class="prepend-2 span-19 append-3 last line"  id="divAddress">
       <div class="prepend-1 span-4 label">*Fecha de Nacimiento:</div>
        <div class="span-5"> <input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio'> </div>
        
        <div class="span-3 label">*Direcci&oacute;n:</div>
        <div class="span-4 append-2 last">  
        	<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio'> 
        </div>
    </div>

    <div class="prepend-2 span-19 append-3 last line" id="divAddress2" style="display: none">
        <div class="prepend-1 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5">
           <input type="text" name="txtAddress1" id="txtAddress1" title='El campo "Direcci&oacute;n" es obligatorio' />
        </div>

    </div>


    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
        <div class="span-5"> <input type="text" name="txtPhone1" id="txtPhone1"> </div>
        
        <div class="span-3 label">Tel&eacute;fono Secundario:</div>
        <div class="span-4 append-2 last"> <input type="text" name="txtPhone2" id="txtPhone2" /></div>
    </div>
    
    
   
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">* Celular:</div>
        <div class="span-5"> <input type="text" name="txtCellphone" id="txtCellphone" /> </div>
        
        <div class="span-3 label">E-mail:</div>
        <div class="span-4 append-2 last"> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio'> </div>
	</div>
    
    <div class="prepend-2 span-19 append-3 last line" id="divRelative">
        <div class="prepend-1 span-4 label">*Contacto en caso de Emergencia:</div>
        <div class="span-5"> 
        	<input type="text" name="txtRelative" id="txtRelative" title='El campo "Contacto en caso de Emergencia" es obligatorio'>
        </div>
        
        <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
        <div class="span-4 append-2 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio'> </div>
	</div>

	<div class="prepend-2 span-19 append-3   last line" id="divManager" style="display: none">
        <div class="prepend-1 span-4 label">*Gerente:</div>
        <div class="span-5">
        	<input type="text" name="txtManager" id="txtManager" title='El campo "Gerente" es obligatorio'/>
        </div>

        <div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
        <div class="span-4 append-2 last"> <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del gerente" es obligatorio'/> </div>
    </div>
    
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">VIP:</div>
        <div class="span-5">
        	<input type="checkbox" name="chkVip" id="chkVip" value="1"> 
        </div>
    </div>
    
    <div class="span-19 last line" id="divOpenDialog">
        <div class="prepend-1 span-4 label"></div>
        <div class="span-5"> <a href="#" name="openDialog" id="openDialog">Configurar Preguntas</a></div>
    </div>
    
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
    
    <input type="hidden" name="hdnParameterValue" id="hdnParameterValue" value="{$parameterValue}" />
    <input type="hidden" name="hdnParameterCondition" id="hdnParameterCondition" value="{$parameterCondition}" />
    <input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
    <input type="hidden" name="hdnQuestionsFlag" id="hdnQuestionsFlag" value="0" />
    <input type="hidden" name="hdnQuestionsExists" id="hdnQuestionsExists" value="{if $questionList}1{else}0{/if}" />
    <div id="hiddenAnswers">
    	
    </div>
</form>
<div id="dialog" title="Preguntas">
        {include file="templates/modules/customer/fCustomerDialog.$language.tpl"}
</div>

