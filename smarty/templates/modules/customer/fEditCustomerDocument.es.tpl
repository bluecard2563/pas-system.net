{assign var="title" value="ACTUALIZAR DOCUMENTO DE CLIENTE"}

<div class="span-24 last title">Actualizar Documento de Cliente<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	 <div class="append-7 span-10 prepend-7 last ">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Actualizaci&oacute;n exitosa!
			{elseif $error eq 2}
				Ocurrieron errores al actualizar el documento;
			{/if}
		</div>
	</div>
{/if}

<form name="frmEditCustomersDocument" id="frmEditCustomersDocument" action="{$document_root}modules/customer/pEditCustomerDocument" method="post">
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-6 label">* Nombre o documento del cliente:</div>
        <div class="span-10 append-2 last">
            <input type="text" name="txtCustomerData" id="txtCustomerData" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
			<input type="hidden" name="hdnCustomerID" id="hdnCustomerID" value="" />
        </div>
	</div>

	<div class="span-24 last buttons line">
        <input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
    </div>

	<div class="span-24 last line" id="customerInfoContainer"></div>

</form>