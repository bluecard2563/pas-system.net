{assign var="title" value="B&Uacute;SQUEDA DE CLIENTES"}
<div class="prepend-2 span-19 append-3 last title">
	B&uacute;squeda de Clientes<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-8 span-10 prepend-6 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="prepend-2 span-19 append-3 last line">
	<div class="prepend-3 span-5 label">* Ingrese un patr&oacute;n de b&uacute;squeda del nombre del cliente o e-mail:</div>
	<div class="span-5">
		<input type="text" name="txtPattern" id="txtPattern" title='El campo "Patr&oacute;n" es obligatorio.' class="autocompleter">
	</div>
</div>

<div class="span-24 last line buttons">
	<input type="button" name="btnSearch" id="btnSearch" value="Buscar"/>
</div>


<div class="span-24 last line" id="customerContainer"></div>