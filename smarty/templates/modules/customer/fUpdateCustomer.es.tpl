{assign var="title" value="ACTUALIZAR CLIENTE"} 
<form name="frmUpdateCustomer" id="frmUpdateCustomer" method="post" action="{$document_root}modules/customer/pUpdateCustomer" class="">
    <div class="prepend-2 span-19 append-3 last title">
		Actualizar Cliente<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
		<div class="append-8 span-10 prepend-6 last ">
			<div class="span-10 error" align="center">
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
			</div>
		</div>
    {/if}

	<div class="span-8 span-10 prepend-6 last">
		<ul id="alerts" class="alerts"></ul>
    </div>

	<div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-4 label">*Documento:</div>
        <div class="span-5">  
			{if $data}{$data.document_cus}{/if} 
			<input type="hidden" name="txtDocument" id="txtDocument" value="{$data.document_cus}" />
		</div>
		<div class="span-3 label">*Tipo de Cliente</div>
		<div class="span-4 append-2 last">  
			<select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$typesList item="l"}
					<option value="{$l}" {if $data}{if $data.type_cus eq $l} selected="selected"{/if}{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="prepend-2 span-19 append-3 last line" id="divName" {if $data.type_cus neq PERSON}style="display: none"{/if}>
		<div class="prepend-1 span-4 label">*Nombre:</div>
		<div class="span-5">  
			<input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' {if $data} value="{$data.first_name_cus}"{/if}> 
		</div>

		<div class="span-3 label">*Apellido</div>
		<div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' {if $data} value="{$data.last_name_cus}"{/if}> </div>
	</div>

        <div class="prepend-2 span-19 append-3 last line" id="genderDiv" {if $data.type_cus neq PERSON}style="display: none"{/if}>
            <div class="prepend-1 span-4 label">* G&eacute;nero:</div>
            <div class="span-5">  
                <input type="radio" name="rdGender" id="rdGender" value="M" {if $data.gender_cus eq 'M'}checked{/if} /> Masculino <br />
                <input type="radio" name="rdGender" id="rdGender" value="F" {if $data.gender_cus eq 'F'}checked{/if} /> Femenino
            </div>
        </div>

	<div class="prepend-2 span-19 append-3 last line" id="divName2" {if $data.type_cus eq PERSON}style="display: none"{/if}>
		<div class="prepend-1 span-4 label">* Raz&oacute;n Social:</div>
		<div class="span-5">
			<input type="text" name="txtFirstName1" id="txtFirstName1" title='El campo "Raz&oacute;n Social" es obligatorio' {if $data} value="{$data.first_name_cus}"{/if} />
		</div>
	</div>

	<div class="prepend-2 span-19 append-3 last line">
		<div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
		<div class="span-5"> 
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}" {if $data}{if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}{/if}>{$l.name_cou|htmlall}</option>
				{/foreach}
			</select> 
		</div>
		<div class="span-3 label">*Ciudad:</div>
		<div class="span-4 append-2 last" id="cityContainer">
			<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$cityList item="l"}
					<option value="{$l.serial_cit}" {if $data}{if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}{/if}>{$l.name_cit|htmlall}</option>
				{/foreach}
			</select> 
		</div>
	</div>


	<div class="prepend-2 span-19 append-3 last line"  id="divAddress" {if $data.type_cus neq PERSON}style="display: none"{/if}>
		<div class="prepend-1 span-4 label">*Fecha de Nacimiento:</div>
		<div class="span-5"> <input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio' {if $data} value="{$data.birthdate_cus}"{/if} /> </div>

		<div class="span-3 label">*Direcci&oacute;n:</div>
		<div class="span-4 append-2 last">
			<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data} value="{$data.address_cus}"{/if} />
		</div>
	</div>

	<div class="prepend-2 span-19 append-3 last line" id="divAddress2" {if $data.type_cus eq PERSON}style="display: none"{/if}>
		<div class="prepend-1 span-4 label">* Direcci&oacute;n:</div>
		<div class="span-5">
			<input type="text" name="txtAddress1" id="txtAddress1" title='El campo "Direcci&oacute;n" es obligatorio' {if $data} value="{$data.address_cus}"{/if} />
		</div>
	</div>

	<div class="prepend-2 span-19 append-3 last line">
		<div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
		<div class="span-5"> <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' {if $data} value="{$data.phone1_cus}"{/if}/> </div>

		<div class="span-3 label">Tel&eacute;fono Secundario:</div>
		<div class="span-4 append-2 last"> <input type="text" name="txtPhone2" id="txtPhone2" {if $data} value="{$data.phone2_cus}"{/if} /></div>
	</div>

	<div class="prepend-2 span-19 append-3 last line">
		<div class="prepend-1 span-4 label">*Celular:</div>
		<div class="span-5"> <input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio'  {if $data} value="{$data.cellphone_cus}"{/if} /> </div>

		<div class="span-3 label">*E-mail:</div>
		<div class="span-4 append-2 last"> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio' {if $data} value="{$data.email_cus}"{/if}/> </div>
	</div>

	<div class="prepend-2 span-19 append-3 last line" id="divRelative" {if $data.type_cus neq PERSON}style="display: none"{/if}>
		<div class="prepend-1 span-4 label">*Contacto en caso de Emergencia:</div>
		<div class="span-5"> 
			<input type="text" name="txtRelative" id="txtRelative" title='El campo "Contacto en caso de Emergencia" es obligatorio' {if $data} value="{$data.relative_cus}"{/if}>
		</div>
		<div class="span-3 label">*Tel&eacute;fono del contacto:</div>
		<div class="span-4 append-2 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio' {if $data} value="{$data.relative_phone_cus}"{/if}> </div>
	</div>

	<div class="prepend-2 span-19 append-3   last line" id="divManager" {if $data.type_cus eq PERSON}style="display: none"{/if}>
		<div class="prepend-1 span-4 label">*Gerente:</div>
		<div class="span-5">
			<input type="text" name="txtManager" id="txtManager" title='El campo "Gerente" es obligatorio' {if $data} value="{$data.relative_cus}"{/if} />
		</div>

		<div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
		<div class="span-4 append-2 last"> <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del gerente" es obligatorio' {if $data} value="{$data.relative_phone_cus}"{/if} /> </div>
	</div>

	<div class="prepend-2 span-19 append-3 last line">
		<div class="prepend-1 span-4 label">VIP:</div>
		<div class="span-5">
			<input type="checkbox" name="chkVip" id="chkVip" value="1" {if $data}{if $data.vip_cus eq 1}checked="checked"{/if}{/if}> 
		</div>  
		<div class="span-3 label">*Estado:</div>
		<div class="span-4 append-2 last">
			<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$statusList item="l"}
					<option value="{$l}" {if $data}{if $data.status_cus eq $l} selected="selected"{/if}{/if}>{if $l eq 'ACTIVE'}Activo{elseif $l eq 'INACTIVE'}Inactivo{/if}</option>
				{/foreach}
			</select>
		</div>
	</div>
			
	<div class="prepend-2 span-19 append-3 last line">
		<div class="prepend-1 span-4 label">Historial m&eacute;dico:</div>
		<div class="span-5">
			<textarea name="txtMedicalHistory" id="txtMedicalHistory">{$data.medical_history}</textarea>
		</div>  
	</div> 

	<div class="span-19 last line">
		<div class="prepend-15 span-4 last line" id="showPassword">
			<a class="a-change" id="resetPassword">Resetear Contrase&ntilde;a</a>
		</div>
		<div class="prepend-15 span-4 last line" id="divOpenDialog">
			<div class="span-5"> <a href="#" name="openDialog" id="openDialog">Configurar Preguntas</a></div>
		</div>
	</div>    

	<div class="span-24 last buttons line">
		<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
		<input type="hidden" name="serial_cus" id="serial_cus" value="{if $data}{$data.serial_cus}{/if}" />
		<input type="hidden" name="hdnParameterValue" id="hdnParameterValue" value="{$parameterValue}" />
		<input type="hidden" name="hdnResetPassword" id="hdnResetPassword" value="0" />
		<input type="hidden" name="hdnConfirm" id="hdnConfirm" {if $data} value="{$data.relative_phone_cus}"{/if} title='Una o m&aacute;s preguntas est&aacute;n sin responder. Asegurese de que todas esten contestadas antes de continuar.'/>
		<input type="hidden" name="hdnParameterCondition" id="hdnParameterCondition" value="{$parameterCondition}" />
		<input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
	</div>

	<div id="hiddenAnswers">
		{if $answerList}
			{foreach from=$answerList item=al}
				<input type="hidden" name="question_{$al.serial_que}" id="question_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}" />
				<input type="hidden" name="answer_{$al.serial_que}" id="answer_{$al.serial_que}" value="{$al.serial_ans}" />
			{/foreach}
		{/if}
	</div>
</form>
	
<div id="dialog" title="Preguntas">
	{include file="templates/modules/customer/fCustomerDialog.$language.tpl"}
</div>
