{assign var="title" value="BUSCAR CLIENTE"}
 <div class="prepend-2 span-19 append-3 last title">Buscar Cliente</div>

<form name="frmSearchCustomer" id="frmSearchCustomer" action="{$document_root}modules/customer/fUpdateCustomer" method="post" class="form_smarty">	
    {if $error}
         <div class="append-8 span-10 prepend-6 last ">
            <div class="span-10 {if $error eq 2 || $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    La contrase&ntilde;a se ha reestablecido exitosamente!
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El cliente ingresado no existe.
                {elseif $error eq 4}
                    Hubo errores en el env&iacute;o del e-mail al usuario. Vuelva a restablecer la contrase&ntilde;a.
				{elseif $error eq 5}
                    Hubo errores en la carga de datos del cliente. Por favor vuelva a intentarlo.
				{elseif $error eq 6}
					Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
				{elseif $error eq 7}
					Hubo errores en la actualizaci&oacute;n de la declaraci&oacute;n de salud. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-8 span-10 prepend-6 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-5 label">Ingrese el nombre o documento del cliente:</div>
        <div class="span-11 append-2 last">
            <input type="text" name="txtCustomerData" id="txtCustomerData" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
        </div>
	</div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnCustomerID" id="hdnCustomerID" value="" />
</form>