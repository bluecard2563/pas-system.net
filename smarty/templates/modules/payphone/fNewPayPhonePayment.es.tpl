{assign var="title" value="PAGO PAYPHONE"}
<form name="frmPayPhoneInvoice" id="frmPayPhoneInvoice" method="post" action="{$document_root}modules/payphone/execute_payment" class="">
    <div class="span-24 last title line">
        Pago PayPhone<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last title line">
        <div class="prepend-10 span-4 append-10">
            <img style="background-color: #F9A030; height: 30px; padding: 3px;" src="{$document_root}img/credit_card_logos/payphone.png" alt="PayPhone"/>
        </div>
    </div>
        
    <div class="span-24 last line">
        <div class="prepend-2 span-5 append-17 label">Detalle de Facturas a Pagar</div>
    </div>
        
    <div class="prepend-2 span-18 append-4 last line">
        {if $invoicesList}
            <table border="1" class="dataTable" id="invoicesResumeTable">
                <THEAD>
                    <tr class="header">
                        {foreach from=$titles item="t"}
                        <th style="text-align: center;">
                            {$t}
                        </th>
                        {/foreach}
                    </tr>
                </THEAD>
                <TBODY>
                    {foreach name="invoices" from=$invoicesList item="i"}
                    <tr {if $smarty.foreach.invoices.iteration is even}class="odd"{else}class="even"{/if}>
                        <td align="center" class="tableField">
                           {$i.date_inv}
                        </td>
                        <td align="center" class="tableField">
                            {$i.number_inv}
                        </td>
                        <td align="center" class="tableField">
                            {$i.invoice_to|htmlall}
                        </td>
                        <td align="center" class="tableField">
                            {$i.total_to_pay}
                        </td>
                    </tr>
                    {/foreach}
                </TBODY>
            </table>
        {else}
            <center><b>No existen facturas por pagar.</b></center>
        {/if}
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-5 label">Total a pagar:</div>
        <div class="span-2 last">${$totalInvoice}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* C&oacute;digo de &Aacute;rea:</div>
        <div class="span-4">
            <input type="text" name="txtRegionCode" id="txtRegionCode" size="4" title='El campo "C&oacute;digo de &Aacute;rea" es obligatorio.'/>
        </div>

        <div class="span-5 label">* N&uacute;mero de Tel&eacute;fono:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtPhoneNum" id="txtPhoneNum" title='El campo "N&uacute;mero de Tel&eacute;fono" es obligatorio.'/>
        </div>
    </div>
    

    <div class="span-24 last buttons line" style="padding-top: 30px;">
        <input type="button" name="payInvoices" id="payInvoices" value="Procesar Pago"/>
        <input type="hidden" name="amountWithTax" value="{$amountWithTax}"/>
        <input type="hidden" name="tax" value="{$tax}"/>
        <input type="hidden" name="amountWithoutTax" value="{$amountWithoutTax}"/>
        <input type="hidden" name="amount" value="{$amount}"/>
         <input type="hidden" name="selectedBoxes" value="{$selectedBoxes}"/>
    </div>
</form>
