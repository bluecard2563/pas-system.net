{assign var="title" value="VALIDAR PAGOS PAYPHONE"}
<form name="frmValidatePPhonePayment" id="frmValidatePPhonePayment" method="post" action="{$document_root}modules/payphone/process_request" class="">
    <div class="span-24 last title line">
        Validar Pagos con PayPhone<br />
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 {if $error eq 1 or $error eq 7}success{else}error{/if}" align="center">
            {if $error eq 1}
                Gracias por su pago! 
            {elseif $error eq 2}
                Debe seleccionar al menos una factura.
            {elseif $error eq 3}
                Hubo errores en el cobro. Por favor vuelva a intentarlo.
            {elseif $error eq 4}
                Hubo errores al registrar los detalles del pago. Por favor vuelva a intentarlo.
            {elseif $error eq 5}
                Existi&oacute; un error al actualizar la factura. Por favor p&oacute;ngase en contacto con el administrador.
            {elseif $error eq 6}
                Existi&oacute; un error al actualizar una de las ventas. Por favor contacte al administrador inmediatamente.
            {elseif $error eq 7}
                Para finalizar, por favor procese el pago a trav&eacute;s de PayPhone en su celular.
            {elseif $error eq 8}
                <ul>
                {foreach from=$error_description item="msg"}
                    <li>{$msg}</li>
                {/foreach}
                </ul>
            {/if}
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    {if $data.userType neq "DEALER"}
	{if $data.countryList or $data.cityList}
        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">{if  $data.userType eq "PLANETASSIST"}* {/if}Pa&iacute;s:</div>
            <div class="span-5">
                {if  $data.userType eq "PLANETASSIST"}
                    <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach name="countries" from=$data.countryList item="l"}
                            <option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou}</option>
                        {/foreach}
                    </select>
                {else}{$data.nameCountry}{/if}
            </div>

            <div class="span-3 label">{if  $data.userType eq "PLANETASSIST"}* {/if}Representante:</div>
            <div class="span-4 append-4 last" id="managerContainer">
                {if  $data.userType eq "PLANETASSIST"}
                    <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                    </select>
                {else}
                    {$data.nameManager}
                    <input type="hidden" name="hdnSerialMbc" id="hdnSerialMbc" value="{$data.serialMbc}"/>
                {/if}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Ciudad:</div>
            <div class="span-5"  id="cityContainer">
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {if $data.cityList}
                    {foreach name="cityList" from=$data.cityList item="l"}
                        <option value="{$l.serial_cit}" {if $smarty.foreach.cityList.total eq 1}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                    {/if}
                </select>
            </div>

            <div class="span-3 label">* Responsable:</div>
            <div class="span-4 append-4 last" id="responsibleContainer">
                <select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Comercializador:</div>
            <div class="span-5" id="dealerContainer">
                <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
		</div>
		<div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Sucursal:</div>
            <div class="span-4 append-4 last" id="branchContainer">
                <select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>
	{else}
            <center><b>Por favor, espere un momento.</b></center>
	{/if}
    {/if}

    <div class="span-24 last line" id="invoiceContainer">
        {if $invoicesList}
        <table border="1" class="dataTable" id="payedInvoices">
            <THEAD>
                <tr bgcolor="#284787">
                    {foreach from=$titles item="t"}
                    <th style="text-align: center;">
                        {$t}
                    </th>
                    {/foreach}
                </tr>
            </THEAD>
            <TBODY>
                {foreach name="invoices" from=$invoicesList item="i"}
                <tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if} >
                    <td align="center" class="tableField">
                       {$i.date_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.number_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.invoice_to}
                    </td>
                    <td align="center" class="tableField">
                        {$i.total_to_pay}
                    </td>
                </tr>
                {/foreach}
            </TBODY>
        </table>
        <div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>

        <div class="span-24 last buttons line">
            <input type="submit" name="vldInvoices" id="vldInvoices" value="Validar Pagos" />
        </div>
        {else}
            {if $data.userType eq "DEALER"}<center><b>No existen pagos para Validar para esta Sucursal de Comercializador.</b></center>{/if}
        {/if}
    </div>
    
    <input type="hidden" name="payphoneIDs" id="payphoneIDs" value="{$payphoneIDs}"/>
    <input type="hidden" name="selectedDealer" id="selectedDealer" value="{$data.dealerID}" />
</form>
