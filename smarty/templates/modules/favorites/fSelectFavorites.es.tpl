{assign var=title value="ENLACES FAVORITOS"}
<div class="span-24 last title ">
	 Selecci&oacute;n de Enlaces Favoritos
</div>

{if $error}
<div class="prepend-6 span-12 last">
	{if $error eq 1}
		<div class="success" align="center">
			Los enlaces fueron ingresados exitosamente!
		</div>
	{else}
		<div class="error" align="center">
			Hubo errores durante el proceso. Por favor vuelva a intentarlo.
		</div>
	{/if}
</div>
{/if}

{if $options || $myFavorites}
<form name="frmFavorites" id="frmFavorites" method="post" action="{$document_root}modules/favorites/pSaveFavorites">
	<div class="span-24 last line">
		<div class="prepend-1 span-11">
			<div class="prepend-1 span-10 bordered">
				<ul id="sortable1" class="connectedSortable">
					<li class="ui-state-default ui-state-disabled">Enlaces Disponibles</li>
					{if $options}
					{foreach from=$options item="opt"}
						<li class="ui-state-default" id="option_{$opt.serial_opt}">{$opt.name_obl}</li>
					{/foreach}
					{/if}
				</ul>
			</div>
		</div>

		<div class="prepend-1 span-10 last bordered">
			<ul id="sortable2" class="connectedSortable">
				<li class="ui-state-highlight ui-state-disabled">Enlaces a utilizar</li>
				{if $myFavorites}
				{foreach from=$myFavorites item="opt"}
					<li class="ui-state-highlight" id="option_{$opt.serial_opt}">{$opt.name_obl}</li>
				{/foreach}
				{/if}
			</ul>
		</div>
	</div>

	<div class="span-24 last buttons">
		<input type="button" id="btnSubmit" value="Guardar" />
		<textarea class="hide" id="txtSelected" name="txtSelected"></textarea>
	</div>
</form>
{else}
	<div class="prepend-6 span-12 append-6">
		<div class="span-12 center error">
			No existen enlaces para usarlos como favoritos.
		</div>
	</div>
{/if}