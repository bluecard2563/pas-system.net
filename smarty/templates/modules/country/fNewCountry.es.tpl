{assign var="title" value="NUEVO PA&Iacute;S"}
<form name="frmNewCountry" id="frmNewCountry" method="post" action="{$document_root}modules/country/pNewCountry" class="form_smarty" enctype="multipart/form-data">

    <div class="span-24 last title ">
    	Registro de Nuevo Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    El Pa&iacute;s se ha registrado exitosamente!
                {elseif $error eq 2}
                    No se pudo insertar.
                {elseif $error eq 3}
                    Revise el tama�o de la Imagen de la Bandera. M&aacute;x. 300 KB
                {elseif $error eq 4}
                    Revise las dimensiones de la Imagen de la Bandera. M&aacute;x. 150x150px
                {elseif $error eq 5}
                    Error al copiar al Servidor la Imagen de la Bandera.
                {elseif $error eq 6}
                    La Imagen de la Bandera debe ser JPG, GIF o PNG
                {elseif $error eq 7}
                    Revise el tama�o de la Imagen de la Tarjeta. M&aacute;x. 300 KB
                {elseif $error eq 8}
                    Revise las dimensiones de la Imagen de la Tarjeta. M&aacute;x. 150x150px
                {elseif $error eq 9}
                    Error al copiar al Servidor la Imagen de la Tarjeta.
                {elseif $error eq 10}
                    La Imagen de la Tarjeta debe ser JPG, GIF o PNG.
                {elseif $error eq 11}
                    Revise el tama�o de la Imagen del Banner. M&aacute;x. 300 KB
                {elseif $error eq 12}
                    Revise las dimensiones de la Imagen del Banner. M&aacute;x. 150x150px
                {elseif $error eq 13}
                    Error al copiar al Servidor la Imagen del Banner.
                {elseif $error eq 14}
                    La Imagen del Banner debe ser JPG, GIF o PNG.
                {elseif $error eq 14}
                    No se pudo asignar "Dollar" como moneda oficial del pa&iacute;s.
                {/if}
            </div>
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">  
        <div class="prepend-7 span-5">
            <label>*Zona:</label>
        </div>
        <div class="span-10 last">     	                                  
            <select name="selZone" id="selZone" title='El campo Zona es obligatorio.'>
                 <option value="">-Seleccione-</option>
                {foreach from=$nameZoneList item="l"}
                <option value="{$l.serial_zon}">{$l.name_zon}</option>
             {/foreach}
            </select>
        </div>   
    </div> 
    
    <div class="span-24 last line">  
        <div class="prepend-7 span-5">
             <label>*C&oacute;digo pa&iacute;s:</label>
        </div>
        <div class="span-10 last">
             <input type="text" name="txtCodeCountry" id="txtCodeCountry" title='El campo "C&oacute;digo" es obligatorio.'>
        </div>        
	</div>
    
    <div class="span-24 last line">  
        <div class="prepend-7 span-5">
             <label>*Nombre pa&iacute;s:</label>
        </div>
        <div class="span-10 last">
             <input type="text" name="txtNameCountry" id="txtNameCountry" title='El campo "Nombre" es obligatorio.'>
        </div>        
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-5">
             <label>Idioma:</label>
        </div>
        <div class="span-10 last">
            <select name="selLang" id="selLang">
                 <option value="">-Seleccione-</option>
                {foreach from=$languageList item="l"}
                <option value="{$l.serial_lang}">{$l.name_lang}</option>
             {/foreach}
            </select>
        </div>
        <div class="prepend-6 span-10 append-4 specifications">
            Idioma que se usar&aacute; en la p&aacute;gina web al escojer el pa&iacute;s<br>
            de no tenerlo no saldr&aacute; en las opciones.
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-5">
            <label>Tel&eacute;fono para Asistencias:</label>
        </div>
        <div class="span-10 last">
             <input type="text" name="txtPhoneCountry" id="txtPhoneCountry" />
        </div>
        <div class="prepend-6 span-10 append-4 specifications">
            Este tel&eacute;fono se mostrar&aacute; para asistencias en la p&aacute;gina web<br>
            de no tenerlo no saldr&aacute; en la lista.
        </div>
    </div>
    
    <div class="span-24 last line">  
        <div class="prepend-7 span-5 ">
           <label>*Bandera:</label>
        </div> 
        <div class="span-10 last">    
           <input type="file" name="fileFlagCou" id="fileFlagCou" title='El campo "Bandera" es obligatorio.'>
        </div>  
        <div class="prepend-8 span-10 append-4 specifications">
           	Dimensiones m&aacute;ximas: 150x150px<br />
			Tama&ntilde;o m&aacute;ximo: 300KB
        </div>
    </div> 
    
    <div class="span-24 last line">    
        <div class="prepend-7 span-5 ">
           <label>*Tarjeta:</label>
        </div>       
        <div class="span-10 last">
           <input type="file" name="fileCardCou" id="fileCardCou" title='El campo "Tarjeta" es obligatorio.'>
        </div>
        <div class="prepend-8 span-10 append-4 specifications">
           	Dimensiones m&aacute;ximas: 150x150px<br />
			Tama&ntilde;o m&aacute;ximo: 300KB
        </div>
	</div> 
    
    <div class="span-24 last line">   
        <div class="prepend-7 span-5  ">
           <label>*Banner:</label>
        </div>
        <div class="span-10 last">
           <input type="file" name="fileBannerCou" id="fileBannerCou" title='El campo "Banner" es obligatorio.'>
        </div>
        <div class="prepend-8 span-10 append-4 specifications">
           	Dimensiones m&aacute;ximas: 150x150px<br />
			Tama&ntilde;o m&aacute;ximo: 300KB
        </div>
	</div>
      
    <div class="span-24 last buttons line">
         <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" class="" >
    </div>   
</form>