{assign var="title" value="ACTUALIZAR PA&Iacute;S"}
<form name="frmUpdateCountry" id="frmUpdateCountry" method="post" action="{$document_root}modules/country/pUpdateCountry" class="form_smarty" enctype="multipart/form-data">
	
    {if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            	{if $error eq 2}
                     Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 3}
                    Revise el tama�o de la Imagen de la Bandera. M&aacute;x. 300 KB
                {elseif $error eq 4}
                    Revise las dimensiones de la Imagen de la Bandera. M&aacute;x. 150x150px
                {elseif $error eq 5}
                    Error al copiar al Servidor la Imagen de la Bandera.
                {elseif $error eq 6}
                    La Imagen de la Bandera debe ser JPG, GIF o PNG
                {elseif $error eq 7}
                    Revise el tama�o de la Imagen de la Tarjeta. M&aacute;x. 300 KB
                {elseif $error eq 8}
                    Revise las dimensiones de la Imagen de la Tarjeta. M&aacute;x. 150x150px
                {elseif $error eq 9}
                    Error al copiar al Servidor la Imagen de la Tarjeta.
                {elseif $error eq 10}
                    La Imagen de la Tarjeta debe ser JPG, GIF o PNG.
                {elseif $error eq 11}
                    Revise el tama�o de la Imagen del Banner. M&aacute;x. 300 KB
                {elseif $error eq 12}
                    Revise las dimensiones de la Imagen del Banner. M&aacute;x. 150x150px
                {elseif $error eq 13}
                    Error al copiar al Servidor la Imagen del Banner.
                {elseif $error eq 14}
                    La Imagen del Banner debe ser JPG, GIF o PNG.
                {/if}
            </div>
        </div>
    </div>
	{/if}

    
    
	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
 	 <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-5">
            <label>*Zona:</label>
        </div>
        <div class="span-10 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                 <option value="">-Seleccione-</option>
                {foreach from=$nameZoneList item="l"}
                <option value="{$l.serial_zon}" {if $data.serial_zon eq $l.serial_zon}selected{/if}>{$l.name_zon}</option>
             {/foreach}
            </select>
        </div>
    </div>
    <div class="span-24 last line">  
        <div class="prepend-7 span-5">
            <label>*C&oacute;digo pa&iacute;s:</label>
        </div>         
        <div class="span-12 last">
			<input type="text" name="txtCodeCountry" id="txtCodeCountry" title='El campo "C&oacute;digo" es obligatorio.' value="{$data.code_cou}">
        </div>        
    </div>
    
    <div class="span-24 last line">  
        <div class="prepend-7 span-5">
            <label>*Nombre pa&iacute;s:</label>
        </div>         
        <div class="span-12 last">
           <input type="text" name="txtNameCountry" id="txtNameCountry" title='El campo "Nombre" es obligatorio.' value="{$data.name_cou}">            
        </div>        
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-5">
             <label>Idioma:</label>
        </div>
        <div class="span-10 last">
             <select name="selLang" id="selLang">
                 <option value="">-Seleccione-</option>
                {foreach from=$languageList item="l"}
                <option value="{$l.serial_lang}" {if $data.serial_lang eq $l.serial_lang}selected="selected"{/if}>{$l.name_lang}</option>
             {/foreach}
            </select>
        </div>
        <div class="prepend-6 span-10 append-4 specifications">
            Idioma que se usar&aacute; en la p&aacute;gina web al escojer el pa&iacute;s<br>
            de no tenerlo no saldr&aacute; en las opciones.
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-5">
            <label>Tel&eacute;fono para Asistencias:</label>
        </div>
        <div class="span-10 last">
            <input type="text" name="txtPhoneCountry" id="txtPhoneCountry" value="{$data.phone_cou}" />
        </div>
        <div class="prepend-6 span-10 append-4 specifications">
            Este tel&eacute;fono se mostrar&aacute; para asistencias en la p&aacute;gina web<br>
            de no tenerlo no saldr&aacute; en la lista.
        </div>
    </div>
    
    <div class="span-24 last line">  
        <div class="prepend-7 span-5 ">
           <label>*Bandera:</label>
        </div> 
        <div class="span-12 last">    
           <input type="file" name="fileFlagCou" id="fileFlagCou" value="{$data.flag_cou}">
        </div>  
        <div class="prepend-7 span-10 append-4 specifications">
           	Dimensiones m&aacute;ximas: 150x150px<br />
			Tama&ntilde;o m&aacute;ximo: 300KB
        </div>
        {if $data.flag_cou}
		<div class="prepend-11 span-9 country-image">
        	<img src="{$document_root}img/country/flag/{$data.flag_cou}" max-width="150px" />
        </div>
        {/if}
    </div> 
    
    <div class="span-24 last line">    
        <div class="prepend-7 span-5 ">
           <label>*Tarjeta:</label>
        </div>       
        <div class="span-12 last">
           <input type="file" name="fileCardCou" id="fileCardCou" value="{$data.card_cou}">
        </div>
        <div class="prepend-7 span-10 append-4 specifications">
           	Dimensiones m&aacute;ximas: 150x150px<br />
			Tama&ntilde;o m&aacute;ximo: 300KB
        </div>
        {if $data.card_cou}
		<div class="prepend-11 span-9 country-image">
        	<img src="{$document_root}img/country/card/{$data.card_cou}" max-width="150px" />
        </div>
        {/if}
	</div> 
    
    <div class="span-24 last line">   
        <div class="prepend-7 span-5  ">
           <label>*Banner:</label>
        </div>
        <div class="span-12 last">
           <input type="file" name="fileBannerCou" id="fileBannerCou" value="{$data.banner_cou}">
        </div>
        <div class="prepend-7 span-10 append-4 specifications">
           	Dimensiones m&aacute;ximas: 150x150px<br />
			Tama&ntilde;o m&aacute;ximo: 300KB
        </div>
        {if $data.banner_cou}
		<div class="prepend-11 span-9 country-image">
        	<img src="{$document_root}img/country/banner/{$data.banner_cou}" max-width="150px" />
        </div>
        {/if}
	</div>
       
    <div class="span-24 last buttons line">
       <input type="submit" name="btnInsert" id="btnInsert" value="Actualizar" class="" >
       <input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" {if $data.serial_cou}value="{$data.serial_cou}"{/if} >
    </div>
   
</form>

