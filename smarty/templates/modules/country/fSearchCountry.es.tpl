{assign var="title" value="BUSCAR PA&Iacute;S"}
<form name="frmSearchCountry" id="frmSearchCountry" method="post" action="{$document_root}modules/country/fUpdateCountry" class="form_smarty">                        
    
	<div class="span-24 last title ">
    	Buscar Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    El Pa&iacute;s se ha actualizado exitosamente!
                {else}
                	No existe el pa&iacute;s ingresado. Por favor seleccione uno de la lista.      
                {/if}
            </div>
        </div>
    </div>
	{/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
 
	<div class="span-24 last line">
        <div class="prepend-6 span-6 label">*Seleccione la zona:</div>

        <div class="span-8 append-2 last">
             <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name=zones from=$zoneList item="l"}
                    <option id="{$l.serial_zon}" value="{$l.serial_zon}"{if $smarty.foreach.zones.total eq 1}selected{/if}>{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
   </div>

	<div class="span-24 last line">
        <div class="prepend-6 span-6 label">*Seleccione el pa&iacute;s:</div>

        <div class="span-8 append-2 last" id="countryContainer">
             <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
   </div>
	
   <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" class="" >
        <input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" {if $data.serial_cou}value="{$data.serial_cou}"{/if} >
   </div>
   
</form>


