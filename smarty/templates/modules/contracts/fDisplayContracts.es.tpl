{assign var="title" value="ADMINISTRACI&Oacute;N DE CONTRATOS PERSONALIZADOS"}


<form name="frmNewContract" id="frmNewContract" method="post" action="{$document_root}modules/contracts/pNewContract" class="">
	<div class="span-24 last title ">
    	ADMINISTRACI&Oacute;N DE CONTRATOS POR CLIENTE<br />
    </div>
    
	{if $error}
	    <div class="append-7 span-10 prepend-7 last">
	    	<div class="span-11 {if $error eq 1 or $error eq 9}success{else}error{/if}" align="center">
	    	{if $error eq 1}
				El contrato se ha ingresado exitosamente!
	        {elseif $error eq 2}
				El contrato no pudo ser ingresado, por favor int&eacute;ntelo nuevamente.
	        {/if}
	        </div>
	    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
   
   <div class="span-24 line">&nbsp;</div>
   
   <div class="span-24 last line">
        <div class="prepend-6 span-4 label">* Cliente:</div>
        <div class="span-5">
			<input type="text" name="txtCustomer" id="txtCustomer" title='El campo "Cliente" es obligatorio.' size="35">
        </div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-6 span-4 label">* Producto:</div>
        <div class="span-5">
        	<select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$products key=key item="pro"}
					<option value="{$pro.serial_pro}">{$pro.name_pbl}</option>
				{/foreach}
			</select>
        </div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-6 span-4 label">* Nombre del Contrato:</div>
        <div class="span-5">
			<input type="text" name="txtContractName" id="txtContractName" title='El campo "Nombre del Contrato" es obligatorio.' size="35">
        </div>
	</div>
    
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Guardar Contrato" >
   </div>
   
   <input type="hidden" name="hdnCustomerID" id="hdnCustomerID" value="" />
</form>