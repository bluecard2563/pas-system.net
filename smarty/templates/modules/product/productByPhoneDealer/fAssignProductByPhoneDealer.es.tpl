{assign var="title" value="ASIGNAR PRODUCTOS A COMERCIALIZADO TELEF&Oacute;NICO"}
<form name="frmAssignPhoneProduct" id="frmAssignPhoneProduct" method="post" action="{$document_root}modules/product/productByPhoneDealer/pAssignProductByPhoneDealer" class="">
	<div class="span-24 last line title">
		Asignaci&oacute;n de Productos a Comercializador Telef&oacute;nico<br />
		<label>(*) Campos Obligatorios</label>
	</div>

	{if $error}
	<div class="append-7 span-10 prepend-7 last">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
		{if $error eq 1}
			Productos registrados exitosamente!
		{elseif $error eq 2}
			No se pudo asignar los productos.
		{elseif $error eq 3}
			No se seleccionaron productos.
		{/if}
		</div>
	</div>
	{/if}

	<div class="append-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	{if $countryList}
	<div class="span-24 last line separator">
    	<label>Asignar productos a:</label>
    </div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
		<div class="span-5">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}" serial_dea="{$l.serial_dea}">{$l.name_cou}</option>
				{/foreach}
			</select>
			<input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value=""/>
		</div>
	</div>

	<div class="prepend-5 span-14 append-5 last line" id="productContainer"></div>

	{else}
		<center>No existen pa&iacute;ses con comercializadores que realicen ventas telef&oacute;nicas.</center>
	{/if}
</form>