<form name="frmNewProduct" id="frmNewProduct" method="post" action="{$document_root}modules/product/pNewProduct" class="form_smarty">

	<div class="span-9 append-5 prepend-5 last title">
    	Registro de Nuevo Producto
    </div>
	{if $error}
    <div class="append-5 span-9 prepend-5 last">
    	{if $error eq 1}
        	<div class="span-9 success" align="center">
            	El producto se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-9 success" align="center">
            	Error al insertar.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-5 span-9 prepend-5 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

     
    <div class="span-19 last line">
            <div class="prepend-1 span-8 label">
                <label>Nombre del Producto :</label>
            </div>
        
        <div class="span-8 append-2 last ">
            <div class="">
                <input type="text" name="txtName_pro" id="txtName_pro" class="span-7"/>
            </div>
        </div>
     </div>   
        
	<div class="span-19 last line">
    	<div class="prepend-1 span-8 label">Tipo de Producto:</div>
		<div class="span-8 append-2 last"> 
        	<select name="selSerial_tpp" id="selSerial_tpp" title='El campo "Tipo de Producto" es obligatorio' class="span-5"  onchange="loadChecks(this.value);">
                <option value="">- Seleccione -</option>
                {foreach from=$productTypes item="l"}
                    <option value="{$l.serial_tpp}"{if $status eq $l.serial_tpp}selected="selected"{/if} >{$l.name_tpp}</option>
                {/foreach}
            </select> 
		</div>
	</div>
    
    <div class="span-19 last line">
            <div class="prepend-1 span-8 label">
                <label>Precio:</label>
            </div>
        
        <div class="span-8 append-2 last ">
            <div class="">
                <input type="text" name="txtPrice_pro" id="txtPrice_pro" class="span-3"/>
            </div>
        </div>
     </div>   
        
	<div class="span-19 last line" id="checkContainer">
		{ generate_check_table list=$list }
	</div>
    
  	<div class="span-19 last line" id="checkContainer">
		{foreach from="$benefits" item="benefit" key="key"}
        	<input type="checkbox" value="{$benefits.serial_ben}" id="{$benefits.serial_ben}" name="benefits"/>
			{$benefit.description_ben}
            
        {/foreach}
	</div>
        
     <div class="span-19 last line">   
        <div class="prepend-9 span-4 append-6">
            <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" class="" >
        </div>
    </div>
</form>