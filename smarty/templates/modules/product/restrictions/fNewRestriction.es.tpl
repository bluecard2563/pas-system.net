{assign var="title" value="NUEVA RESTRICCI&Oacute;N"}
<form name="frmNewRestriction" id="frmNewRestriction" method="post" action="{$document_root}modules/product/restrictions/pNewRestriction" class="">
	<div class="span-24 last title">
    	Registro de Nuevo tipo de Restricci&oacute;n<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            El tipo de restricci&oacute;n se ha registrado exitosamente!
        {elseif $error eq 2}
            No se pudo registrar el tipo de restricci&oacute;n.
        {elseif $error eq 3}
            La traducci&oacute;n no pudo ser ingresada.
        {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">Descripci&oacute;n: </div>
        <div class="span-8 append-2 last">
            <input type="text" name="txtDescription" id="txtDescription" title="El campo 'Traducci&oacute;n' es obligatorio."/>
        </div>
    </div>
       
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
    </div>
</form>