<form id="frmUpdateTranslation" name="frmUpdateTranslation" method="post" action="{$document_root}modules/product/restrictions/pUpdateTranslation" >
<div class="span-1 span-8 prepend-1 last line">
    <ul id="alerts_dialog" class="alerts"></ul>
</div>

<div class="span-10 last line">
    <div class="prepend-1 span-4 label">
    	Idioma:
    </div>
    <div class="span-4 append-1 last" id="language"></div>
</div>
    
<div class="span-10 last line">
    <div class="prepend-1 span-4 label">
    	* Descripci&oacute;n:
    </div>
    <div class="span-4 append-1 last">
    	<input type="text" name="txtRestriction" id="txtRestriction" value="" title="El campo 'Descripci&oacute;n' es obligatorio." />
    </div>
</div>

<input type="hidden" name="serial_rbl" id="serial_rbl" />
<input type="hidden" name="serial_lang" id="serial_lang" />
</form>