{assign var="title" value="ACTUALIZAR RESTRICCI&Oacute;N"}
<div class="span-24 last title">
    Actualizar Restricci&oacute;n<br />
        <label>(*) Campos Obligatorios</label>
</div>
{if $dataO}
<form name="frmUpdateRestriction" id="frmUpdateRestriction" method="post" action="{$document_root}modules/product/restrictions/pUpdateRestriction">
    {if $error}
    <div class="append-7 span-10 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                La actualizaci&oacute;n fue exitosa!
            {elseif $error eq 2}
                No se pudo actualizar el tipo de restricci&oacute;n.
            {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">Estado: </div>
        <div class="span-8 append-2 last"> 
            {foreach from=$statusList item="l"}
                <input type="radio" name="rdStatus" id="rdStatus" value="{$l}" {if $l eq $dataO.status_rst}checked{/if}>
                {$global_status.$l}
            {/foreach}
        </div>
    </div>

    <div class="prepend-3 span-19 append-2 last line" id="adminTable">
        {assign var="container" value="none"}
        {generate_admin_table data=$data titles=$titles text="Registrar Traducci&oacute;n"}
    </div>

    <input type="hidden" name="itemCount" id="itemCount" value="{$itemCount}" />
    <input type="hidden" name="hdnSerialRBL" id="hdnSerialRBL" value="" />
    <input type="hidden" name="hdnRestrictionID" id="hdnRestrictionID" value="{$dataO.serial_rst}" />
</form>

<form name="frmNewTranslation" id="frmNewTranslation" method="post" action="{$document_root}modules/product/restrictions/pNewTranslation">
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts_t" class="alerts"></ul>
    </div>
    
    <div id="newInfo" class="span-24 last line">
        <div class="span-24 last title">
            Nueva Traducci&oacute;n
</div>
        {if $languageList}
         <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">* Idioma: </div>
            <div class="span-8 append-2 last">
                <select id="selLanguage" name="selLanguage" title="El campo 'Idioma' es obligatorio." class="select">
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
         </div>
        
         <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">* Descripci&oacute;n: </div>
            <div class="span-8 append-2 last">
                <input type="text" name="txtDescription" id="txtDescription" title="El campo 'Descripci&oacute;n' es obligatorio.">
            </div>
         </div>

        <div class="span-24 last buttons line">
            <input type="button" name="btnInsert" id="btnInsert" value="Registrar" class="" >
        </div>
    </div>
    <input type="hidden" name="hdnRestrictionID" id="hdnRestrictionIDT" value="{$dataO.serial_rst}" />
    {else}
        <div class="span-14 append-5 last">
            <div class="span-14 label">Ya existen traducciones para esta restricci&oacute;n en todos los idiomas.</div>
        </div>
    {/if}
</form>

<div id="dialog" title="Actualizar ">
    {include file="templates/modules/product/restrictions/fUpdateTranslation.$language.tpl"}
</div>

{else}
    <div class="append-5 span-10 prepend-4 last">
        <div class="span-10 error" align="center">
            No se pudo cargar la informaci&oacute;n del tipo de restricci&oacute;n.
        </div>
    </div>
{/if}