{assign var="title" value="BUSCAR TRADUCCI&Oacute;N PRODUCTO"} 
<form name="frmSearchProduct" id="frmSearchProduct" action="{$document_root}modules/product/fUpdateProductTranslation" method="post" class="form_smarty">
	<div class="append-7 span-10 prepend-7 last title">Buscar Producto</div>
    {if $message}
    <div class="append-7 span-10 prepend-7 last showMessages" >
        <div class="span-10 {if $message eq 1}success{else}error{/if}">
            {if $message eq 2}
                 No existe el tipo de producto ingresado.
            {elseif $message eq 3}
                 Error al Actualizar el Tipo de Producto
            {elseif $message eq 1}
                 Actualizaci&oacute;n exitosa
            {/if}
        </div>
    </div>
    {/if}
    <div class="append-7 span-10 prepend-7 last showMessages">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            Idioma:
        </div>
        <div class="span-4 append-8 last ">
            <select id="selLanguage" name="selLanguage"  class="span-6" >
                {foreach from=$languages item="language"}
                <option value="{$language.code_lang}" {if $code_lang eq $language.code_lang}selected{/if}>
                    {$language.name_lang}
                </option>
                {/foreach}
            </select>
        </div>
    </div>    

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            Ingrese el nombre del producto:
        </div>
        <div class="span-8 append-4 last ">
            <input type="text" id="txtName_pro" name="txtName_pro" class="span-8" />
        </div>
    </div>
	<div class="append-7 prepend-7 span-10  last buttons ">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
        <input type="hidden" id="hdnSerial_pro" name="hdnSerial_pro"/>
    </div>
</form>