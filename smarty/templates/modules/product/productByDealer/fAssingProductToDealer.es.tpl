{assign var='title' value='ASIGNACI&Oacute;N DE PRODUCTOS A COMERCIALIZADOR'}
<form name="frmAssignProduct" id="frmAssignProduct" method="post" action="{$document_root}modules/product/productByDealer/pAssingProductToDealer" class="">
<div class="span-24 last line title">
	Asignaci&oacute;n de Productos a Comercializador<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
	{if $error eq 1}
		Productos registrados exitosamente!
	{elseif $error eq 2}
		No se pudo asignar los productos.
	{elseif $error eq 3}
		No se seleccionaron productos.
	{/if}
	</div>
</div>
{/if}

<div class="append-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $zonesList}
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name="zones" from=$zonesList item="z"}
				<option value="{$z.serial_zon}">{$z.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
        	<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>

		<div class="span-3 label">* Ciudad:</div>
        <div class="span-4 append-4 last" id="cityContainer">
        	<select name="selCity" id="selCity">
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Responsable:</div>
        <div class="span-5" id="comissionistContainer">
        	<select name="selComissionist" id="selComissionist">
            	<option value="">- Todos -</option>
            </select>
        </div>
	</div>

	<div class="span-24 last line" id="dealerContainer"></div>
	<div class="span-24 last line" id="productContainer"></div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar Cambios" />
		<input type="hidden" name="hdnSelectOne" id="hdnSelectOne" value="" title="Seleccione al menos un producto para continuar" />
	</div>
{else}
	<div class="span-24 last line separator">
        <label>Lista de Comercializadores</label>
    </div>
	{if $dealerList}
		<div class="span-14 prepend-5 append-5 last">
			<table border="0" id="dealer_table">
				<thead>
					<tr bgcolor="#284787">
						<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
							<input type="checkbox" name="chkAll" id="chkAll" />
						</td>
						<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
							Comercializador
						</td>
					</tr>
				</thead>
				<tbody>
				{foreach name="dealerList" from=$dealerList item="l"}
				<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.productList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
					<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
						<input type="checkbox" name="chkDealers[]" id="chkDealer_{$l.serial_dea}" value="{$l.serial_dea}" />
					</td>
					<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
						{$l.name_dea}
					</td>
				</tr>
				{/foreach}
				</tbody>
			</table>
		</div>
		<div class="span-24 last line buttons" id="paging_dealers"></div>
		
		<div class="span-24 last line" id="productContainer"></div>

		<div class="span-24 last line buttons">
			<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar Cambios" />
			<input type="hidden" name="hdnSelectOne" id="hdnSelectOne" value="" title="Seleccione al menos un producto para continuar" />
			<input type="hidden" name="pages_dealer" id="pages_dealer" value="{$pages}" />
			<input type="hidden" name="selCountry" id="selCountry" value="{$countryID}" />
		</div>
	{else}
	<div class="span-12 append-6 prepend-6 last">
		<div class="span-12 error center">
			No existen comercializados asignados a su representante.
			Comun&iacute;quese con el administrador.
		</div>
	</div>
	{/if}
{/if}
</form>