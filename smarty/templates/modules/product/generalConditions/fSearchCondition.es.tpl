{assign var="title" value="ACTUALIZAR CONDICI&Oacute;N GENERAL"}
<div class="span-24 last title">
	Actualizar Condici&oacute;n General<br />
	<label>(*) Seleccione una condici&oacute;n para actualizarla</label>
</div>

{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El nuevo archivo se ha registrado exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	El registro del nuevo archivo fall&oacute;. Por favor comun&iacute;quese con el adminsitrador.
            </div>
		{elseif $error eq 3}
        	<div class="span-10 error" align="center">
            	Hubo errores al copiar el archivo al servidor. La operaci&oacute;n fue cancelada.
            </div>
		{elseif $error eq 4}
        	<div class="span-10 error" align="center">
            	La extensi&oacute;n del archivo es incorrecta. Por favor vuelva a intentarlo con un archivo PDF.
            </div>
		{elseif $error eq 5}
        	<div class="span-10 error" align="center">
            	Los datos anteriores no se pudieron cargar. La operaci&oacute;n fue cancelada.
            </div>
		{elseif $error eq 6}
        	<div class="span-10 success" align="center">
            	La traducci&oacute;n y el nuevo estado fueron registrados exitosamente!
            </div>
		{elseif $error eq 7}
        	<div class="span-10 error" align="center">
            	Hubo errores al registrar el nuevo estado. Comun&iacute;quese con el administrador.
            </div>
		{elseif $error eq 8}
        	<div class="span-10 error" align="center">
            	Se ha actualizado el archivo correctamente. Sin embargo, no se pudo eliminar el archivo anterior. <br>
				Comun&iacute;quese con el administrador.
            </div>
		{elseif $error eq 9}
        	<div class="span-10 error" align="center">
            	La traducci&oacute;n de la Condici&oacute;n General, no pudo ser actualizada. La operaci&oacute;n fue cancelada.
            </div>
		{elseif $error eq 10}
        	<div class="span-10 error" align="center">
            	Hubo errores internos en la operaci&oacute;n. Por favor comun&iacute;quese con el administrador.
            </div>
        {/if}
    </div>
 {/if}

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $gConditionList}
<form name="frmGConditions" id="frmGConditions" method="post" action="{$document_root}modules/product/generalConditions/fUpdateCondition">
	<div class="span-12 append-6 prepend-6 line last">
		<table id="conditionsTable">
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">No.</td>
				<td align="center" class="tableTitle">Seleccione</td>
				<td align="center" class="tableTitle">Nombre</td>
			</tr>
			{foreach name="conditions" from=$gConditionList item="g"}
			<tr {if $smarty.foreach.conditions.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
				<td align="center" class="tableField">{$smarty.foreach.conditions.iteration}</td>
				<td align="center" class="tableTitle"><input type="radio" name="rdConditions" id="rdCondition_{$g.serial_gcn}" value="{$g.serial_gcn}" /> </td>
				<td align="center" class="tableField">{$g.name_glc}</td>
			</tr>
			{/foreach}
		</table>
		<input type="hidden" name="hdnSelectOne" id="hdnSelectOne" value="" title="Seleccione una Condici&oacute;n General"/>
	</div>
	<div class="span-24 last line buttons" id="navPosition"></div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Traducir/Actualizar" />
	</div>
</form>
{else}
<div class="span-10 append-7 prepend-7">
	<div class="span-10 error">
		No existen condiciones generales registradas para el idioma actualmente utilizado.
	</div>
</div>
{/if}