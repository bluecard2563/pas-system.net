<form name="frmNewCondition" id="frmNewCondition" method="post" action="{$document_root}modules/product/generalConditions/pNewConditionDialog" class="" enctype="multipart/form-data"  target="iframeUpload">
    <div class="span-1 span-10 prepend-1 last">
	    <ul id="alertsDialog" class="alerts"></ul>
    </div>

	<div id="confirmMsj" class="prepend-1 span-10 last" style="display: none">
		<div id="innerConfirmMsj" class="span-10 success" align="center">
			El producto se ha registrado exitosamente!
		</div>
    </div>
    
    <div class="span-10 last line ">
		<div class="span-4 label">Condici&oacute;n General:</div>
        <div class="span-6 last"> 
        	<input type="text" name="txtCondition" id="txtCondition" title="El campo 'Condici&oacute;n General' es obligatorio." />
			<input type="hidden" name="hdnSerial_lang" id="hdnSerial_lang" value="{$serial_lang}" />
        </div>
	</div>
    
	<div class="span-10 last line">
		<div class="span-4 label">Archivo Planet Assist:</div>
		<div class="span-6 last">
			<div id="general_condition_planet_msj" class="span-6"></div>
			<input type="hidden" id="hdnPlanetFileRealName" name="hdnPlanetFileRealName" value="" />
			<input type="file" id="general_condition_planet" name="general_condition_planet" /><br /><br />
			<input type="hidden" id="hdnPlanetFileName" name="hdnPlanetFileName" value="" title="El campo 'Archivo Planet Assist' es obligatorio." />
			<div id="general_condition_planet_file" class="span-6 last"></div>
		</div>
	</div>

	<div class="span-10 last line">
		<div class="span-4 label">Archivo Ecuador:</div>
		<div class="span-6 last" id="general_condition_ecuador_container">
			<div id="general_condition_ecuador_msj" class="span-6"></div>
			<input type="hidden" id="hdnEcuadorFileRealName" name="hdnEcuadorFileRealName" value="" />
			<input type="file" id="general_condition_ecuador" name="general_condition_ecuador" /><br /><br />
			<input type="hidden" id="hdnEcuadorFileName" name="hdnEcuadorFileName" value="" title="El campo 'Archivo Ecuador' es obligatorio." />
			<div id="general_condition_ecuador_file" class="span-6 last"></div>
		</div>
	</div>
</form>