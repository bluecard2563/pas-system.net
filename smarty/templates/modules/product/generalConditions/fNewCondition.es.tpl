{assign var="title" value="NUEVA CONDICI&Oacute;N GENERAL"}
<form name="frmNewCondition" id="frmNewCondition" method="post" action="{$document_root}modules/product/generalConditions/pNewCondition" class="" enctype="multipart/form-data">
	<div class="span-24 last title">
    	Registro de Nueva Condici&oacute;n General<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	La condici&oacute;n general se ha registrado exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	El archivo para Planet Assist no pudo ser copiado al servidor. La operación se ha cancelado.
            </div>
		{elseif $error eq 3}
        	<div class="span-10 error" align="center">
            	La Condici&oacute;n General para Planet Assist no pudo ser ingresada.
            </div>
		{elseif $error eq 4}
        	<div class="span-10 error" align="center">
            	El archivo para Ecuador no pudo ser copiado al servidor. La operación se ha cancelado.
            </div>
		{elseif $error eq 5}
        	<div class="span-10 error" align="center">
            	La Condici&oacute;n General para Ecuador no pudo ser ingresada.
            </div>
		{elseif $error eq 6}
        	<div class="span-10 error" align="center">
            	La Condici&oacute;n General no pudo ser ingresada. Comun&iacute;quese con el administrador.
            </div>
		{elseif $error eq 7}
        	<div class="span-10 error" align="center">
            	Los archivos ingresados no tienen el formato PDF requerido. Lo sentimos.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="prepend-3 span-19 append-2 last line">
		<div class="prepend-1 span-8 label">Condici&oacute;n General</div>
        <div class="span-8 append-2 last"> 
        	<input type="text" name="txtCondition" id="txtCondition" title="El campo 'Condici&oacute;n General' es obligatorio.">
        </div>
	</div>
    
     <div class="prepend-3 span-19 append-2 last line">
		<div class="prepend-1 span-8 label">Archivo Planet Assist:</div>
        <div class="span-8 append-2 last"> 
	        <input type="file" id="general_condition_planet" name="general_condition_planet" /><br /><br />
        </div>
	</div>

	<div class="prepend-3 span-19 append-2 last line">
		<div class="prepend-1 span-8 label">Archivo Ecuador:</div>
        <div class="span-8 append-2 last">
	        <input type="file" id="general_condition_ecuador" name="general_condition_ecuador" /><br /><br />
        </div>
	</div>
           
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
		<input type="hidden" name="hdnLanguage" id="hdnLanguage" value="{$language}" />
    </div>
</form>