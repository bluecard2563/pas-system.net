{assign var="title" value="ACTUALIZACI&Oacute;N DE CONDICI&Oacute;N GENERAL"}
<form name="frmNewCondition" id="frmNewCondition" method="post" action="{$document_root}modules/product/generalConditions/pUpdateCondition" class="">
	<div class="span-24 last title">
    	Actualizaci&oacute;n de Condici&oacute;n General<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	La condici&oacute;n general se ha actualizado exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	*No se pudo actualizar a la condici&oacute;n.
            </div>
		{elseif $error eq 3}
        	<div class="span-10 error" align="center">
            	No se pudo subir el archivo asociado a la condici&oacute;n.
            </div>
		{elseif $error eq 4}
        	<div class="span-10 success" align="center">
            	La condici&oacute;n general se ha actualizado exitosamente <br>
				Sin embargo, no se pudo eliminar el antiguo archivo asociado a esa condici&oacute;n.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
	
	<div class="prepend-3 span-19 append-2 last line">
		<div class="prepend-1 span-8 label">* Estado:</div>
        <div class="span-8 append-2 last">
			<select name="selStatus" id="selStatus" title="El campo 'Estado' es obligatorio">
				<option value="">- Seleccione -</option>
				{foreach from=$status_list item="s"}
				<option value="{$s}" {if $s eq $gcn.status_gcn}selected{/if}>{$global_status.$s}</option>
				{/foreach}
			</select>
        </div>
	</div>

	<div class="prepend-3 span-19 append-2 last line">
		<div class="prepend-1 span-8 label">* Idioma: </div>
        <div class="span-8 append-2 last">
			<select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio." >
				<option value="">- Seleccione -</option>
				{foreach from=$languageList item="l"}
				<option value="{$l.serial_lang}" {if $l.serial_lang eq 2}selected{/if}>{$l.name_lang}</option>
				{/foreach}
			</select>
        </div>
		<input type="hidden" name="hdnSerial_gcn" id="hdnSerial_gcn" value="{$serial_gcn}" />
	</div>

	<div class="span-24 last line" id="file_translations">
	<div class="prepend-3 span-19 append-2 last line">
		<div class="prepend-1 span-8 label">* Nombre:</div>
        <div class="span-8 append-2 last">
			<input type="text" name="txtCondition" id="txtCondition" title="El campo 'Condici&oacute;n General' es obligatorio." value="{$gconditions_list.0.name_glc}">
        </div>
	</div>	
    
    <div class="prepend-4 span-16 append-4 last line">
		<table border="0" id="translationsTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						No.
					</td>
					<td align="center" class="tableTitle">
						Documento
					</td>
					<td align="center" class="tableTitle" colspan="3">
						Actividades
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="conditions" from=$gconditions_list item="s"}
				<tr {if $smarty.foreach.conditions.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						{$smarty.foreach.conditions.iteration}
					</td>
					<td align="center" class="tableField">
						{$s.url_glc}
					</td>
					<td align="center" class="tableField">
						<a class="link_item" href="{$document_root}pdfs/generalConditions/{$s.url_glc}" target="_blank">Ver</a>
					</td>
					<td align="center" class="tableField">
						<a class="link_item" id="update_{$s.serial_glc}" element_id="{$s.serial_glc}" >Actualizar</a>
					</td>
					<td align="center" class="tableField">
						<a class="link_item" id="translate_{$s.serial_glc}" element_id="{$s.serial_glc}" >A&ntilde;adir archivo <br>en otro idioma</a>
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>

	<div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Actualizar" />
		<input type="hidden" name="hdnType" id="hdnType" value="UPDATE_CONDITION" />
    </div>
	</div>
</form>

<div id="updateFile" title="Actualizar Archivo">
<form action="{$document_root}modules/product/generalConditions/pUpdateCondition" method="post" name="frmReplaceFile" id="frmReplaceFile" enctype="multipart/form-data">
<div class="span-12 notice center line">
	<label>Recuerde que al actualizar el archivo, el PDF anterior ser&aacute; eliminado del sistema.</label>
</div>

<div class="span-2 span-9 prepend-2 last">
	<ul id="alerts_replacement" class="alerts"></ul>
</div>

<div class="span-12">
	<div class="prepend-2 span-3 label">Archivo (PDF):</div>
	<div class="span-5 last">
		<input type="file" id="general_condition_planet" name="general_condition_planet" title="El campo 'Archivo' es obligatorio." /><br /><br />
	</div>
</div>
	
<div class="span-12 buttons">
	<input type="hidden" name="hdnSerial_glc" id="hdnSerial_glc" value="" />
	<input type="hidden" name="hdnType" id="hdnType" value="REPLACE_FILE" />
</div>
</form>
</div>

<div id="addFile" title="A&ntilde;adir Archivo en otro Idioma">
<form action="{$document_root}modules/product/generalConditions/pUpdateCondition" method="post" name="frmAddFile" id="frmAddFile" enctype="multipart/form-data">
<div class="span-2 span-9 prepend-2 last">
	<ul id="alerts_add" class="alerts"></ul>
</div>

<div class="span-12 last line">
	<div class="prepend-2 span-3 label">Idioma:</div>
	<div class="span-5 last" id="remaining_languages">
		<select name="selLanguageTranslation" id="selLanguageTranslation" title="El campo 'Idioma' es obligatorio.">
			<option value="">- Seleccione -</option>
			{foreach from=$remainingLanguages item="l"}
			<option value="{$l.serial_lang}">{$l.name_lang}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-12 last line">
	<div class="prepend-2 span-3 label">Archivo (PDF):</div>
	<div class="span-5 last">
		<input type="file" id="general_condition_planet" name="general_condition_planet" title="El campo 'Archivo' es obligatorio." /><br /><br />
	</div>
</div>

<div class="span-12 buttons">
	<input type="hidden" name="hdnAddSerial_glc" id="hdnAddSerial_glc" value="" />
	<input type="hidden" name="hdnType" id="hdnType" value="ADD_FILE" />
</div>
</form>
</div>