{assign var="title" value="ACTUALIZAR PRODUCTO"} 
<form name="frmNewProduct" id="frmNewProduct" method="post" action="{$document_root}modules/product/pUpdateProduct" enctype="multipart/form-data" class="form_smarty">
	<input type="hidden" name="hdnSold" id="hdnSold" value="{$sold}">

	<div class="span-10 append-7 prepend-7 last title">
    	Actualizar Producto<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="wizard-nav prepend-2 span-20 append-2 last">
	<a href="#FirstPage"><div class="span-5">1. Producto</div></a>
        <a href="#SecondPage"><div class="span-5">2. Beneficios</div></a>
        <a href="#ThirdPage"><div class="span-5 ">3. Condiciones Generales</div></a>
        <a href="#FourthPage"><div class="span-5 last">4. Precios Base</div></a>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
            <div class="span-10 success" align="center">
            	El tipo de producto se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	error al insertar.
            </div>
        {/if}
    </div>
    {/if}

    <div class="span-3 span-18 prepend-3 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

<div id="FirstPage" class="wizardpage span-24 last">
    <div id="Page1" class="span-24 last">

    <div class="span-24 last line separator">
        <label class="append-1">Activo:</label>
		<input type="checkbox" name="status_pro" id="status_pro" value="ACTIVE" {if $data.0.status_pro eq 'ACTIVE'} checked="checked" {/if}/>
    </div>


    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
			<label>* Tipo de Producto :</label>
        </div>
        <div class="span-8 append-4 last ">
            <select id="serial_tpp" name="serial_tpp" class="span-7" {if $sold}disabled{/if}>
               <option value="" >- Seleccione una -</option>
                {foreach from=$productTypes item="productType" key="serial_tpp"}
                    <option value={$productType.serial_tpp}  {if $data.0.serial_tpp eq $productType.serial_tpp}selected="selected"{/if}>{$productType.name_ptl}</option>
                {/foreach}
            </select>
        </div>
    </div>


	<div class="span-24 last line">
		<div class="prepend-4 span-8 label">
			<label>* Nombre del Producto :</label>
		</div>
		<div class="span-8 append-4 last ">
			<div>
				<input type="text" name="txtName_pro" id="txtName_pro" class="span-7" {if $soreld}disabled{/if}  {if $data.0.name_pbl}value="{$data.0.name_pbl}"{/if}/>
				<input type="hidden" name="serial_pro" id="serial_pro" value="{$data.0.serial_pro}" />

			</div>
		</div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Descripci&oacute;n del Producto :</label>
		</div>

        <div class="span-8 append-4 last ">
            <div>
                <textarea  name="txtDescription_pro" id="txtDescription_pro" class="span-7" {if $sold}disabled{/if}>{if $data.0.description_pbl}{$data.0.description_pbl}{/if}</textarea>
            </div>
        </div>
    </div>
    <div class="span-24 last line" {if $sold}style="display: none;"{/if}>
        <div class="prepend-4 span-8 label">
            <label>* Imagen Web:</label>
        </div>
        <div class="span-8 append-4 last">
            <div>
                <input type="file" name="image_pro" id="image_pro"/>
            </div>
        </div>
    </div>

    <div class="prepend-13 span-10 last line">
        {if $data.0.image_pro}<img src="{$document_root}img/products/{$data.0.image_pro}" height="60" width="60" />{/if}
        <input type="hidden" name="hdnImage_pro" id="hdnImage_pro" value="{$data.0.image_pro}" />
    </div>

<div class="span-24 last line separator">
    	<label>Condiciones:</label>
</div>


<div class="span-24 last line">
    <div class="prepend-4 span-4 label multipleFlights">M&aacute;ximo Acompa&ntilde;antes:</div>
    <div class="span-4">
	    <input type="text" name="max_extras_pro" id="max_extras_pro" class="span-2" {if $sold}disabled{/if}  {if $data.0.max_extras_pro}value="{$data.0.max_extras_pro}"{/if}/>
    </div>
    <div class="span-4 label">Viajes por Per&iacute;odo: </div>
    <div class="span-4 append-1 last">
	   <select id="flights_pro" name="flights_pro" {if $sold}disabled{/if}><option value="0" {if $data.0.flights_pro eq 0} selected="selected" {/if}>Varios</option><option value="1"  {if $data.0.flights_pro eq 1} selected="selected" {/if}>1 solo viaje</option></select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label multipleFlights">Menores gratis:</div>
    <div class="span-4">
    	<input type="text" name="children_pro" id="children_pro" class="span-2" {if $sold}disabled{/if} {if $data.0.children_pro}value="{$data.0.children_pro}"{/if} />
    </div>
    <div class="span-4 label multipleFlights">Mayores gratis: </div>
    <div class="span-4 append-1 last">
	   <input type="text" name="adults_pro" id="adults_pro" class="span-2" {if $sold}disabled{/if} {if $data.0.adults_pro}value="{$data.0.adults_pro}"{/if} />
    </div>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label multipleFlights">Restriccion de edad de los acompa&ntilde;antes:</div>
	<div class="span-4">
		<select name="selExtrasRestricted" id="selExtrasRestricted" disabled>
			<option value="">-Seleccione-</option>
			{foreach from=$extraTypes item="l"}
			<option value="{$l}" {if $data.0.extras_restricted_to_pro eq $l}selected{/if}>{$global_extraTypes[$l]}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label ">Comisionable y Aplica Descuentos:</div>
    <div class="span-4">
        <input type="checkbox" name="has_comision_pro" id="has_comision_pro" value="YES" {if $sold}disabled{/if} {if $data.0.has_comision_pro eq 'YES'} checked="checked" {/if}/>
    </div>
    <div class="span-4 label masive">Disponible en P&aacute;gina Web:</div>
    <div class="span-4 append-1 last">
        <input type="checkbox" name="in_web_pro" id="in_web_pro" value="YES" class="masive" {if $sold}disabled{/if} {if $data.0.in_web_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label multipleFlights">Precio para Familiares Diferente:</div>
    <div class="span-4">
    	<input type="checkbox" name="relative_pro" id="relative_pro" value="YES" {if $sold}disabled{/if} {if $data.0.relative_pro eq 'YES'} checked="checked" {/if}/>
    </div>

    <div class="span-4 label multipleFlights">Precio para C&oacute;nyuge Diferente:</div>
    <div class="span-4 append-1 last">
        <input type="checkbox" name="spouse_pro" id="spouse_pro" value="YES" {if $sold}disabled{/if} {if $data.0.spouse_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">V&aacute;lida dentro del pa&iacute;s de emisi&oacute;n:</div>
    <div class="span-4">
        <input type="checkbox" name="emission_country_pro" id="emission_country_pro" value="YES" {if $sold}disabled{/if} {if $data.0.emission_country_pro eq 'YES'} checked="checked" {/if}/>
    </div>
    <div class="span-4 label">V&aacute;lida dentro del pa&iacute;s de residencia:</div>
    <div class="span-4 append-1 last">
        <input type="checkbox" name="residence_country_pro" id="residence_country_pro" value="YES" {if $sold}disabled{/if} {if $data.0.residence_country_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

<div class="span-24 last line">

    <div class="prepend-4  span-4 label masive">Registros de Viaje Genera Tarjeta:</div>
    <div class="span-4 ">
	    <input type="checkbox" name="third_party_register_pro" id="third_party_register_pro" value="YES" class="masive" {if $sold}disabled{/if} {if $data.0.third_party_register_pro eq 'YES'} checked="checked" {/if}/>
    </div>
	<div class="span-4 label">Es Masivo:</div>
    <div class="span-4 append-1 last ">
        <input type="checkbox" name="masive_pro" id="masive_pro" value="YES" {if $sold}disabled{/if} {if $data.0.masive_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">Paises Schengen:</div>
    <div class="span-4" >
        <input type="checkbox" name="schengen_pro" id="schengen_pro" value="YES" {if $sold}disabled{/if} {if $data.0.schengen_pro eq 'YES'} checked="checked" {/if}/>
    </div>
    
    <div class="span-4 label">Adulto Mayor:</div>
    <div class="span-4 append-1 last ">
        <input type="checkbox" name="senior_pro" id="senior_pro" value="YES" {if $sold}disabled{/if} {if $data.0.senior_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">S&oacute;lo Viajes Internos:</div>
    <div class="span-4" >
        <input type="checkbox" name="destination_restricted_pro" id="destination_restricted_pro" value="YES" {if $sold}disabled{/if} {if $data.0.destination_restricted_pro eq 'YES'} checked="checked" {/if}/>
    </div>

    <div class="span-4 label">Genera N&uacute;mero de tarjeta en la Venta:</div>
    <div class="span-4 append-1 last ">
        <input type="checkbox" name="generate_number_pro" id="generate_number_pro" value="YES" class="masiveOn" {if $sold}disabled{/if} {if $data.0.generate_number_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">Tiene Servicios:</div>
    <div class="span-4" >
        <input type="checkbox" name="has_services_pro" id="has_services_pro" value="YES" class="masive" {if $sold}disabled{/if} {if $data.0.has_services_pro eq 'YES'} checked="checked" {/if}/>
    </div>
    <div class="span-4 label">Necesita Representacion:</div>
    <div class="span-4 append-1 last ">
        <input type="checkbox" name="generate_representatative" id="generate_representative" value="YES" {if $sold}disabled{/if} {if $data.0.representative_pro eq 'YES'} checked="checked" {/if}/>
    </div>
</div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Restricci&oacute;n Niños:</div>
            <div class="span-4" >
                <input type="checkbox" name="restricted_children_pro" id="restricted_children_pro" value="YES" {if $sold}disabled{/if} {if $data.0.restricted_children_pro eq 'YES'} checked="checked" {/if}/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Producto Individual 365:</div>
            <div class="span-4" >
                <input type="checkbox" name="individual_pro" id="individual_pro" value="YES" {if $sold}disabled{/if} {if $data.0.individual_pro eq 'YES'} checked="checked" {/if} />
            </div>
        </div>




<div class="span-24 last line">
	<sup>1</sup> en caso de no seleccionar esta casilla, y existen adicionales estos se agregar&aacute;n a la misma tarjeta
</div>
    </div>
</div>

<div id="SecondPage" class="wizardpage span-24 last">
        <div id="Page2" class="span-24 last">

    <div class="span-24 last line">
        <div class="prepend-5 span-1 subtitle">x
        </div>
        <div class="span-4 subtitle">Beneficios
        </div>
        <div class="span-3 subtitle">Condiciones
        </div>

        <div class="span-2 subtitle">Cobertura
        </div>
        <div class="span-2 subtitle">Valor Restr.
        </div>
        <div class="span-3 append-4 subtitle last">Restricci&oacute;n
        </div>
    </div>
<div class="span-24 line last" id="benefitsTable">
	{foreach from=$benefits item="benefit" key="key"}
		{assign var="pos" value=$benefit.serial_ben}
		<div class="span-24 last line">
			<div class="prepend-5  span-1">
				<input type="checkbox" id="status_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][status_bpt]"  {if $sold}disabled{/if} {if $benefitsByProduct.$pos} checked="checked" {/if}/>
				{if  $benefitsByProduct.$pos.serial_bxp}
					<input type="hidden" id="serial_bxp_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_bxp]" class="span-2" value="{$benefitsByProduct.$pos.serial_bxp}"/>
				{/if}
			</div>
			<div id="description_ben_{$benefit.serial_ben}" class="span-4">
				{$benefit.description_bbl}
			</div>
			<div class="span-3">
				<select id="serial_ben_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_con]" class="span-3"  {if $sold}disabled{/if}>
					<option value="" >Ninguna</option>
					{foreach from=$conditions item="condition" key="keycon"}
						<option value={$condition.serial_con} {if $benefitsByProduct.$pos.serial_con eq $condition.serial_con} selected="selected"{/if}>{$condition.description_cbl}</option>
					{/foreach}
				</select>
			</div>
			<div class="span-2">
				<input type="text" id="price_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][price_bpt]" class="span-2" {if $sold}disabled{/if} {if $benefitsByProduct.$pos.price_bxp}  value="{$benefitsByProduct.$pos.price_bxp}"{/if}/>
			</div>
			<div class="span-2">
				<input type="text" id="restriction_price_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][restriction_price_bpt]"  class="span-2" {if $sold}disabled{/if}  {if $benefitsByProduct.$pos.restriction_price_bxp}  value="{$benefitsByProduct.$pos.restriction_price_bxp}"{/if}/>
			</div>
			<div class="span-3 append-4 last">
				<select id="serial_rst_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_rst]" class="span-3" {if $sold}disabled{/if}>
					<option value="" >Ninguna</option>
				{foreach from=$restrictions item="restriction" key="keyrst"}
					<option value={$restriction.serial_rst} {if $benefitsByProduct.$pos.serial_rst eq $restriction.serial_rst} selected="selected"{/if}>{$restriction.description_rbl}</option>
				{/foreach}
				</select>
			</div>
		</div>
    {/foreach}
</div>
            </div>
<div class="span-24 last line title"  {if $sold}style="display: none;"{/if}>
	<input type="button" id="create-Benefit" value="Nuevo Beneficio"  class="ui-state-default ui-corner-all"/>
</div>

</div>

<div id="ThirdPage" class="wizardpage span-24 last">

	<div class="span-24 last line" >
		<div class="prepend-4 span-1"><b>&nbsp;</b></div>
		<div class="span-10"><b>Descripci&oacute;n</b></div>
		<div class="span-4"><b>Planet Assist</b></div>
		<div class="span-4"><b>Ecuador</b></div>
	</div>

	<div class="span-24 last line" id="generalConditionsTable">
		{foreach from=$generalConditions item="g" key="key"}
		<div class="span-24 last line">
			<div class="prepend-4 span-1">
				<input type="radio" id="generalCondition" name="generalCondition" value="{$g.serial_gcn}" {if $sold}disabled{/if} {if $conditionsByProduct.0.serial_gcn eq $g.serial_gcn}checked="checked"{/if} />
			</div>
			<div class="span-10">
				{$g.name_glc}
			</div>
			<div class="span-4">
				{if $g.url_planetassist}<a href="{$document_root}pdfs/generalConditions/{$g.url_planetassist}" target="_blank"><img src="{$document_root}img/ico-pdf.gif" border="0" /></a>{else}&nbsp;{/if}
			</div>
			<div class="span-4">
				{if $g.url_ecuador}<a href="{$document_root}pdfs/generalConditions/{$g.url_ecuador}" target="_blank"><img src="{$document_root}img/ico-pdf.gif" border="0" /></a>{else}&nbsp;{/if}
			</div>
		</div>
		{/foreach}
	</div>
	<div class="span-24 last line title">
		<input type="button" id="create-user" value="Nueva Condici&oacute;n General" class="ui-state-default ui-corner-all" />
	</div>
</div>

<div id="FourthPage" class="wizardpage span-24 last">




    <div class="span-24 last line">
        <div class="spouseContainer">
            <div class="span-3 prepend-2 label">* % C&oacute;nyugue:</div>
            <div class="span-4 last">
                <input type="text" title="El campo &quot;C&oacute;nyugue&quot; es obligatorio" id="percentage_spouse_pxc" name="percentage_spouse_pxc" class="span-2"  {if $sold}disabled{/if} {if $product_by_country.percentage_spouse_pxc}value="{$product_by_country.percentage_spouse_pxc}"{/if}>
            </div>
        </div>
        <div class="childrenContainer">
            <div class="span-3 label">* % Extras:</div>
            <div class="span-4 last">
                <input type="text" title="El campo &quot;Extras&quot; es obligatorio" id="percentage_extras_pxc" name="percentage_extras_pxc" class="span-2"  {if $sold}disabled{/if} {if $product_by_country.percentage_extras_pxc}value="{$product_by_country.percentage_extras_pxc}"{/if}>
            </div>
        </div>
        <div class="extrasContainer">
            <div class="span-4 label">* % Hijos:</div>
            <div class="span-4 last">
                <input type="text" title="El campo &quot;Hijos&quot; es obligatorio" id="percentage_children_pxc" name="percentage_children_pxc" class="span-2"  {if $sold}disabled{/if} {if $product_by_country.percentage_children_pxc}value="{$product_by_country.percentage_children_pxc}"{/if}>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
<!--        <div class="span-3 prepend-2 label">Porcentaje Costo:</div>
        <div id="spouseContainer0" class="span-4 last">
            <input type="text" id="percentage" name="percentage" class="span-2" />%
        </div>

        <div class="span-3 label">Precios por rango:</div>
        <div id="childrenContainer0" class="span-4 last">
            <input type="checkbox" value="YES" id="price_by_range" name="price_by_range" {if $data.0.price_by_range_pro eq 'YES'} checked="checked" {/if}/>
        </div>-->

 <div class="span-3 prepend-2 label">Porcentaje Costo:</div>
        <div id="spouseContainer0" class="span-4 last">
            <input type="text" id="percentage" name="percentage" class="span-2" />%
        </div>

        <div class="span-3 label singleFlight masive">Precios por rango:</div>
        <div id="childrenContainer0" class="span-1 ">
            <input type="checkbox" value="YES" id="price_by_range" class="singleFlight masive" name="price_by_range" {if $sold}disabled{/if} {if $data.0.price_by_range_pro eq 'YES'} checked="checked" {/if}/>
        </div>

        <div class="span-3 label masive">Precio autocalculado:</div>
        <div class="span-1 last" >
            <input type="checkbox" name="calculate_pro" id="calculate_pro" value="YES" class="masiveOn" {if $sold}disabled{/if} {if $data.0.calculate_pro eq 'YES'} checked="checked" {/if}/>
        </div>

        <div class="span-3 label calculateDisable singleFlight masive">Precio por d&iacute;as:</div>
        <div class="span-1 last" >
            <input type="checkbox" name="price_by_day_pro" id="price_by_day_pro" class="singleFlight masive" value="YES" {if $sold}disabled{/if} {if $data.0.price_by_day_pro eq 'YES'} checked="checked" {/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="span-3 prepend-2 label  multipleFlights relatedToRange masive">Precio d&iacute;a adicional:</div>
        <div id="spouseContainer0" class="span-4 last">
            <input type="text" id="aditional_day_pxc" name="aditional_day_pxc" class="span-2 relatedToRange masive" {if $product_by_country.aditional_day_pxc}value="{$product_by_country.aditional_day_pxc}"{/if}/>
        </div>

        <div class="span-3 label">Limite de d&iacute;as:</div>
        <div id="childrenContainer0" class="span-1 ">
            <input type="checkbox" value="YES" id="limit_pro" name="limit_pro" {if $sold}disabled{/if} {if $data.0.limit_pro eq 'YES'} checked="checked" {/if}  />
        </div>

        <div class="span-3 label relatedToRange masive">Desplegar D&iacute;as:</div>
        <div class="span-1 last" >
            <input type="checkbox" name="show_price_pro" id="show_price_pro" value="YES" class="relatedToRange masive" {if $sold}disabled{/if} {if $data.0.show_price_pro eq 'YES'} checked="checked" {/if} />
        </div>
    </div>

    <div class="prepend-3  span-18 append-3 last" id="price_by_product">
        <div class="span-18 last line" >
            <div class="span-1">#</div>
            <div class=" span-3 subtitle">D&iacute;as</div>
            <div class="span-3 subtitle price">Precio</div>
            <div class="span-3 subtitle min" >Min.</div>
            <div class="span-3 subtitle max">M&aacute;x.</div>
            <div class="span-3 subtitle last">Costo</div>
        </div>
        <input type="hidden"  name="repeated" id="repeated" />
        {if $prices}
        {foreach from=$prices key="k" item="data" name="price"}
        <div class="span-16 last line divItem" divNumber="{$smarty.foreach.price.iteration}">
            <div class="span-1 number">{$smarty.foreach.price.iteration}</div>
            <div class=" span-3 subtitle">
				<input type="text" id="price_by_product_{$smarty.foreach.price.iteration}_duration_ppc" name="price_by_product[{$smarty.foreach.price.iteration}][duration_ppc]" class="price_by_product span-2" durationnumber="{$smarty.foreach.price.iteration}"  value="{$data.duration_ppc}"/></div>
            <div class="span-3 subtitle price"><input type="text" id="price_by_product_{$smarty.foreach.price.iteration}_price_ppc" name="price_by_product[{$smarty.foreach.price.iteration}][price_ppc]" priceNumber="{$smarty.foreach.price.iteration}"  class="span-2" value="{$data.price_ppc}"/></div>
            <div class="span-3 subtitle min"><input type="text" id="price_by_product_{$smarty.foreach.price.iteration}_min_ppc" name="price_by_product[{$smarty.foreach.price.iteration}][min_ppc]" minnumber="{$smarty.foreach.price.iteration}"  class="span-2" value="{$data.min_ppc}" /></div>
            <div class="span-3 subtitle max"><input type="text" id="price_by_product_{$smarty.foreach.price.iteration}_max_ppc" name="price_by_product[{$smarty.foreach.price.iteration}][max_ppc]" maxnumber="{$smarty.foreach.price.iteration}"  class="span-2" value="{$data.max_ppc}"/></div>
            <div class="span-3 subtitle last">
				<input type="text" id="price_by_product_{$smarty.foreach.price.iteration}_cost_ppc" name="price_by_product[{$smarty.foreach.price.iteration}][cost_ppc]" class="span-2" value="{$data.cost_ppc}"/></div>
			<div class="span-3 subtitle last" id="deletePriceDiv_{$smarty.foreach.price.iteration}">

			{if $smarty.foreach.price.last eq true and $smarty.foreach.price.iteration neq 1}
				<input type="button" id="deletePriceButton_{$smarty.foreach.price.iteration}" class="deletePrice ui-state-default ui-corner-all" value="eliminar" itemNumber="{$smarty.foreach.price.iteration}"/>
            {/if}
            </div>

        </div>
        {/foreach}
        {else}
            <div class="span-16 last line divItem" divNumber="1">
                <div class="span-1 number">1</div>
                <div class=" span-3 subtitle"><input type="text" id="price_by_product_1_duration_ppc" name="price_by_product[1][duration_ppc]" class="price_by_product span-2" durationnumber="1" /></div>
                <div class="span-3 subtitle price"><input type="text" id="price_by_product_1_price_ppc" name="price_by_product[1][price_ppc]" priceNumber="1"  class="span-2" /></div>
                <div class="span-3 subtitle min"><input type="text" id="price_by_product_1_min_ppc" name="price_by_product[1][min_ppc]" minnumber="1"  class="span-2" /></div>
                <div class="span-3 subtitle max"><input type="text" id="price_by_product_1_max_ppc" name="price_by_product[1][max_ppc]" maxnumber="1"  class="span-2" /></div>
                <div class="span-3 subtitle last"><input type="text" id="price_by_product_1_cost_ppc" name="price_by_product[1][cost_ppc]" class="span-2" /></div>
            </div>
        {/if}

    </div>
    <div class="span-24 last line title">
        <input type="button" id="newPrice" value="Nuevo Precio" class="ui-state-default ui-corner-all" />
    </div>



</div>



</form>

<div id="dialog" title="Nueva Condici&oacute;n General">
{include file="templates/modules/product/generalConditions/fNewConditionDialog.tpl"}
</div>

<div id="dialogBenefit" title="Nuevo Beneficio">
{include file="templates/modules/product/benefits/fNewBenefitDialog.tpl"}
</div>
