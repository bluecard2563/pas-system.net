{assign var="title" value="NUEVO TIPO DE PRODUCTO"}
<form name="frmNewProductType" id="frmNewProductType" method="post" action="{$document_root}modules/product/productType/pNewProductType" enctype="multipart/form-data" class="form_smarty">

	<div class="span-10 append-7 prepend-7 last title">
    	Nuevo Tipo de Producto<br/>
        <label>(*) Campos Obligatorios</label>
    </div>
    
    <div class="wizard-nav prepend-3 span-18 append-3 last">
	    <a href="#FirstPage"><div class="span-6">1. Tipo de Producto</div></a>
        <a href="#SecondPage"><div class="span-6">2. Beneficios</div></a>
        <a href="#ThirdPage"><div class="span-6 last">3. Condiciones Generales</div></a>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El tipo de producto se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	error al insertar.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-3 span-18 prepend-3 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

<div id="FirstPage" class="wizardpage span-24 last">
  <div class="span-24 last line">
            <div class="prepend-4 span-8 label">
                <label>* Nombre del Tipo de Producto :</label>
            </div>
        
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" name="txtName_tpp" id="txtName_tpp" class="span-7"/>
            </div>
        </div>
    </div>   
    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Descripci&oacute;n del Tipo de Producto :</label>
            </div>

        <div class="span-8 append-4 last ">
            <div>
                <textarea  name="txtDescription_tpp" id="txtDescription_tpp" class="span-7"></textarea>
            </div>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Imagen Web:</label>
            </div>

        <div class="span-8 append-4 last ">
            <div>
                <input type="file" name="image_tpp" id="image_tpp"/>
            </div>
        </div>
    </div>

	<div class="span-24 last line separator">
    	<label>Condiciones:</label>
	</div>
 
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label multipleFlights">M&aacute;ximo Acompa&ntilde;antes:</div>
		<div class="span-4 ">
		<input type="text" name="max_extras_tpp" id="max_extras_tpp" class="span-2 multipleFlights" />
		</div>
		<div class="span-4 label">Viajes por Per&iacute;odo:</div>
		<div class="span-4 append-4 last">
			<select id="flights_tpp" name="flights_tpp"><option value="">Seleccione uno</option><option value="0">Varios</option><option value="1">1 solo viaje</option></select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label multipleFlights">Menores gratis:</div>
		<div class="span-4">
			<input type="text" name="children_tpp" id="children_tpp" class="span-2 multipleFlights" disabled />
		</div>
		<div class="span-4 label multipleFlights">Mayores gratis: </div>
		<div class="span-4 append-4 last">
		<input type="text" name="adults_tpp" id="adults_tpp" class="span-2 multipleFlights" disabled />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label multipleFlights">Restriccion de edad de los acompa&ntilde;antes:</div>
		<div class="span-4">
			<select name="selExtrasRestricted" id="selExtrasRestricted" disabled>
				<option value="">-Seleccione-</option>
				{foreach from=$extraTypes item="l"}
				<option value="{$l}">{$global_extraTypes[$l]}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Comisionable y Aplica Descuentos:</div>
		<div class="span-4">
			<input type="checkbox" name="has_comision_tpp" id="has_comision_tpp" value="YES"/>
		</div>
		<div class="span-4 label masive">Disponible en P&aacute;gina Web:</div>
		<div class="span-4 append-1 last">
			<input type="checkbox" name="in_web_tpp" id="in_web_tpp" value="YES" class="masive"/>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label multipleFlights">Precio para Familiares Diferente:</div>
		<div class="span-4">
			<input type="checkbox" name="relative_tpp" id="relative_tpp" value="YES" class="multipleFlights" disabled/>
		</div>
		<div class="span-4 label multipleFlights">Precio para C&oacute;nyuge Diferente:</div>
		<div class="span-4 append-1 last">
			<input type="checkbox" name="spouse_tpp" id="spouse_tpp" value="YES" class="multipleFlights" disabled/>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">V&aacute;lida dentro del pa&iacute;s de emisi&oacute;n:</div>
		<div class="span-4">
			<input type="checkbox" name="emission_country_tpp" id="emission_country_tpp" value="YES"/>
		</div>
		<div class="span-4 label">V&aacute;lida dentro del pa&iacute;s de residencia:</div>
		<div class="span-4 append-1 last">
			<input type="checkbox" name="residence_country_tpp" id="residence_country_tpp" value="YES"/>
		</div>
	</div>
             
	<div class="span-24 last line">
		<div class="prepend-4  span-4 label masive">Registros de Viaje Genera Tarjeta:</div>
		<div class="span-4 ">
			<input type="checkbox" name="third_party_register_tpp" id="third_party_register_tpp" value="YES" class="masive"/>
		</div>
			<div class="span-4 label ">Es Masivo:</div>
		<div class="span-4 append-1 last ">
			<input type="checkbox" name="masive_tpp" id="masive_tpp" value="YES" />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Paises Schengen:</div>
		<div class="span-4 " >
			<input type="checkbox" name="schengen_tpp" id="schengen_tpp" value="YES"/>
		</div>
		<div class="span-4 label">Adulto Mayor:</div>
		<div class="span-4 append-1 last ">
			<input type="checkbox" name="senior_tpp" id="senior_tpp" value="YES"/>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">S&oacute;lo Viajes Internos:</div>
		<div class="span-4 " >
			<input type="checkbox" name="destination_restricted_tpp" id="destination_restricted_tpp" value="YES"/>
		</div>
	</div>
</div> 
 
<div id="SecondPage" class="wizardpage span-24 last">
    <div class="span-24 last line">
        <div class="prepend-5 span-1 subtitle">x
        </div>
        <div class="span-4 subtitle">Beneficios
        </div>
        <div class="span-3 subtitle">Condiciones
        </div>
        <div class="span-2 subtitle">Cobertura    
        </div>
        <div class="span-2 subtitle">Valor Restr.
        </div> 
        <div class="span-3 append-4 subtitle last">Restricci&oacute;n
        </div>
    </div>
<div class="span-24 line last" id="benefitsTable">
	{foreach from=$benefits item="benefit" key="key"}
    <div class="span-24 last line">
		<div class="prepend-5  span-1">
			<input type="checkbox" id="status_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][status_bpt]" />
        </div>
        <div id="description_ben_{$benefit.serial_ben}" class="span-4">
        {$benefit.description_bbl} 
        </div>
        <div class="span-3">
        <select id="serial_ben_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_con]" class="span-3">
	    <option value="" >Ninguna</option>
        {foreach from=$conditions item="condition" key="keycon"}
            <option value={$condition.serial_con}>{$condition.description_cbl}</option>
        {/foreach}
        </select>
        </div>

        <div class="span-2">        
        <input type="text" id="price_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][price_bpt]" class="span-2"/>
        </div>
        <div class="span-2">
        <input type="text" id="restriction_price_bpt_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][restriction_price_bpt]"  class="span-2"/>
        </div> 
        <div class="span-3 append-4 last">
        <select id="serial_rst_{$benefit.serial_ben}" name="benefit[{$benefit.serial_ben}][serial_rst]" class="span-3">
        	<option value="" >Ninguna</option>
        {foreach from=$restrictions item="restriction" key="keyrst"}
        	<option value={$restriction.serial_rst}>{$restriction.description_rbl}</option>
        {/foreach}
        </select>
        </div>
	</div>
    {/foreach}
</div>
<div class="span-24 last line title">
         <input type="button" id="create-Benefit" value="Nuevo Beneficio"  class="ui-state-default ui-corner-all"/> 
</div> 
   
</div>
<div id="ThirdPage" class="wizardpage span-24 last">

	<div class="span-24 last line" >
		<div class="prepend-4 span-1"><b>&nbsp;</b></div>
		<div class="span-10"><b>Descripci&oacute;n</b></div>
		<div class="span-4"><b>Planet Assist</b></div>
		<div class="span-4"><b>Ecuador</b></div>
	</div>
  
	<div class="span-24 last line" id="generalConditionsTable">
		{foreach from=$generalConditions item="g" key="key"}
		<div class="span-24 last line">
			<div class="prepend-4 span-1">
				<input type="radio" id="generalCondition" name="generalCondition" value="{$g.serial_gcn}"  />
			</div>
			<div class="span-10">
				{$g.name_glc}
			</div>
			<div class="span-4">
				{if $g.url_planetassist}<a href="{$document_root}pdfs/generalConditions/{$g.url_planetassist}" target="_blank"><img src="{$document_root}img/ico-pdf.gif" border="0" /></a>{else}&nbsp;{/if}
			</div>
			<div class="span-4">
				{if $g.url_ecuador}<a href="{$document_root}pdfs/generalConditions/{$g.url_ecuador}" target="_blank"><img src="{$document_root}img/ico-pdf.gif" border="0" /></a>{else}&nbsp;{/if}
			</div>
		</div>
		{/foreach}
	</div>
	<div class="span-24 last line title">
		<input type="button" id="create-user" value="Nueva Condici&oacute;n General" class="ui-state-default ui-corner-all" />
	</div>
</div>

 
</form>

<div id="dialog" title="Nueva Condici&oacute;n General">
{include file="templates/modules/product/generalConditions/fNewConditionDialog.tpl"}
</div>

<div id="dialogBenefit" title="Nuevo Beneficio">
{include file="templates/modules/product/benefits/fNewBenefitDialog.tpl"}
</div>
