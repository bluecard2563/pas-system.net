{assign var="title" value="TRADUCIR TIPO DE PRODUCTO" }
<form name="frmSearchProductTypeTranslation" id="frmSearchProductTypeTranslation" action="{$document_root}modules/product/productType/fUpdateProductTypeTranslation" method="post" class="form_smarty">
	<div class="span-24 last title">Buscar Tipo de Producto</div>
    {if $message}
    <div class="append-7 span-10 prepend-7 last">
    <div class="span-10 showMessages  {if $message eq 1}success{else}error{/if} " >
                {if $message eq 2}
                     No existe el tipo de producto ingresado.
                {/if}
                {if $message eq 3}
                     Error al Actualizar el Tipo de Producto
                {/if}
                {if $message eq 1}
                     Actualizaci&oacute;n exitosa
                {/if}
    </div>
    </div>
    {/if}

    <div class="append-7 span-10 prepend-7 last showMessages">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">
            Idioma:
        </div>
        <div class="span-4 append-6 last ">
            <select id="selLanguage" name="selLanguage"  class="span-6" >
                {foreach from=$languages item="language"  }
                <option value="{$language.code_lang}" {if $code_lang eq $language.code_lang}selected{/if}>
                    {$language.name_lang}
                </option>
                {/foreach}
            </select>
        </div>
    </div>    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">
            Ingrese el nombre del tipo de producto:
        </div>
        <div class="span-8 append-2 last ">
            <input type="text" id="txtName_tpp" name="txtName_tpp" class="span-8" />
        </div>
    </div>
	<div class="span-24  last buttons ">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
        <input type="hidden" id="hdnSerial_tpp" name="hdnSerial_tpp"/>
    </div>
</form>