{assign value="TRADUCCIONES DE TIPOS DE PRODUCTO" var="title"}

<form name="frmUpdateProductTypeTranslation" id="frmUpdateProductTypeTranslation" method="post" action="{$document_root}modules/product/productType/pUpdateProductTypeTranslation"  enctype="multipart/form-data" class="form_smarty">

	<div class="span-24 last title">
    	Traducir Tipo de Producto<br />
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El tipo de producto se ha actualizado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	error al insertar.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <input type="hidden" name="serial_tpp" id="serial_tpp" value="{$data.0.serial_tpp}" />

    <div class="span-24 last line" id="adminTable">
        {assign var="container" value="none"}
        {generate_admin_table data=$translations titles=$titles text="Registrar Traducci&oacute;n"}
    </div>
</form>

<form name="frmNewTranslation" id="frmNewTranslation" method="post" action="{$document_root}modules/product/productType/pNewTranslation">
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts_t" class="alerts"></ul>
    </div>

    <div id="newInfo" class="prepend-1 span-19 append-2 last line">
        {if $languageList}
         <div class="prepend-1 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">Idioma: </div>
            <div class="span-8 append-2 last">
                <select id="selLanguage" name="selLanguage" title="El campo 'Idioma' es obligatorio." class="select">
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
         </div>

        <div class="prepend-1 span-19 append-2 last line">
                <div class="prepend-1 span-8 label">
                    <label>* Nombre del Tipo de Producto :</label>
                </div>
        
            <div class="span-8 append-2 last ">
                <div>
                    <input type="text" name="txtName_tpp" id="txtName_tpp" class="span-7" {if $data.0.name_ptl} value="{$data.0.name_ptl}" {/if}/>
                </div>
            </div>
        </div>
        <div class="prepend-1 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">
                <label>* Descripci&oacute;n del Tipo de Producto :</label>
                </div>
        
            <div class="span-8 append-2 last ">
                <div>
                    <textarea  name="txtDescription_tpp" id="txtDescription_tpp" class="span-7" >{if $data.0.description_ptl}{$data.0.description_ptl}{/if}</textarea>
                </div>
            </div>
        </div>
         
         
         

        <div class="span-24 last buttons line">
            <input type="button" name="btnInsert" id="btnInsert" value="Registrar" class="" >
        </div>
    </div>
    <input type="hidden" name="serial_tpp" id="serial_tpp" value="{$serial_tpp}" />
    {else}
        <div class="append-5 span-14 prepend-5 last">
            <div class="span-14 label">Ya existen traducciones para esta restricci&oacute;n en todos los idiomas.</div>
        </div>
    {/if}
</form>

<div id="dialog" title="Actualizar ">
    {include file="templates/modules/product/productType/fUpdateTranslation.$language.tpl"}
</div>