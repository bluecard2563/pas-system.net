{assign var="title" value="BUSCAR TIPO DE PRODUCTO"} 
<form name="frmSearchProductType" id="frmSearchProductType" action="{$document_root}modules/product/productType/fUpdateProductType" method="post" class="form_smarty">
	<div class="append-7 span-10 prepend-7 last title">Buscar Tipo de Producto</div>
    
    {if $message}
    <div class="append-7 span-10 prepend-7 last">
    <div class="span-10 last showMessages  {if $message eq 1}success{else}error{/if} " >
                {if $message eq 2}
                     No existe el tipo de producto ingresado.
                {/if}
                {if $message eq 3}
                     Error al Actualizar el Tipo de Producto
                {/if}
                {if $message eq 1}
                     Actualizaci&oacute;n exitosa
                {/if}
    </div>
    </div>
    {/if}
    <div class="append-7 span-10 prepend-7 last showMessages">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-8 label">
            Idioma:
        </div>
        <div class="span-4 append-8 last ">
            <select id="selLanguage" name="selLanguage" class="span-5" >
                {foreach from=$languages item="language"  }
                <option value="{$language.code_lang}" {if $code_lang eq $language.code_lang}selected{/if}>
                    {$language.name_lang}
                </option>
                {/foreach}
            </select>
        </div>
    </div>    

	<div class="prepend-6 span-12 last line" id="productContainer">
		{if $productTypeList}
		<table border="0" id="product_table">
            <thead>
                <tr bgcolor="#284787">
                    <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        #
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        Tipo de Producto
                    </td>
					<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                        Editar
                    </td>
                </tr>
            </thead>

            <tbody>
            {foreach name="productTypeList" from=$productTypeList item="l"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.productTypeList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$smarty.foreach.productTypeList.iteration}
                </td>
                <td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    {$l.name_ptl}
                </td>
				<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
                    <a id="btnUpdate{$l.serial_tpp}" serial_tpp="{$l.serial_tpp}" style="cursor: pointer;">Editar</a>
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
		{else}
			<center>No hay Tipos Productos registrados para el Idioma seleccionado.</center>
		{/if}
		<div class="span-12 last line pageNavPosition" id="pageNavPosition"></div>
	</div>
	<input type="hidden" name="hdnSerial_tpp" id="hdnSerial_tpp" value="" />
</form>