{assign var="title" value="ACTUALIZAR TRADUCCI&Oacute;N PRODUCTO"} 
<form name="frmUpdateProductTranslation" id="frmUpdateProductTranslation" method="post" action="{$document_root}modules/product/productType/pUpdateProductTypeTranslation"  enctype="multipart/form-data" class="form_smarty">

	<div class="span-10 append-7 prepend-7 last title">
    	Traducir Producto<br />
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El tipo de producto se ha actualizado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	error al insertar.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <input type="hidden" name="serial_tpp" id="serial_tpp" value="{$data.0.serial_tpp}" />

    <div class="prepend-3 span-19 append-2 last line" id="adminTable">
        {assign var="container" value="none"}
        {generate_admin_table data=$translations titles=$titles text="Registrar Traducci&oacute;n"}
    </div>
</form>

<form name="frmNewTranslation" id="frmNewTranslation" method="post" action="{$document_root}modules/product/pNewTranslation">
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts_t" class="alerts"></ul>
    </div>

    <div id="newInfo" class="span-24 last line">
        {if $languageList}
         <div class="span-24 last line">
            <div class="prepend-4 span-8 label">Idioma: </div>
            <div class="span-8 append-4 last">
                <select id="selLanguage" name="selLanguage" title="El campo 'Idioma' es obligatorio." class="select">
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
         </div>

        <div class="span-24 last line">
                <div class="prepend-4 span-8 label">
                    <label>* Nombre del Producto :</label>
                </div>
        
            <div class="span-8 append-4 last ">
                <div>
                    <input type="text" name="txtName_pro" id="txtName_pro" class="span-7" {if $data.0.name_pbl} value="{$data.0.name_pbl}" {/if}/>
                </div>
            </div>
        </div>
        <div class="span-24 last line">
            <div class="prepend-4 span-8 label">
                <label>* Descripci&oacute;n del Producto :</label>
                </div>
        
            <div class="span-8 append-4 last ">
                <div>
                    <textarea  name="txtDescription_pro" id="txtDescription_pro" class="span-7" >{if $data.0.description_pbl}{$data.0.description_pbl}{/if}</textarea>
                </div>
            </div>
        </div>
         
         
         

        <div class="span-24 last buttons line">
            <input type="button" name="btnInsert" id="btnInsert" value="Registrar" class="" >
        </div>
    </div>
    <input type="hidden" name="serial_pro" id="serial_pro" value="{$serial_pro}" />
    {else}
        <div class="span-24 last">
            <div class="span-24 label">Ya existen traducciones para esta restricci&oacute;n en todos los idiomas.</div>
        </div>
    {/if}
</form>

<div id="dialog" title="Actualizar ">
    {include file="templates/modules/product/fUpdateTranslation.$language.tpl"}
</div>


<!--<div id="dialog" title="Nueva Condici&oacute;n General">
{include file="templates/modules/product/generalConditions/fNewConditionDialog.tpl"}
</div>

<div id="dialogBenefit" title="Nuevo Beneficio">
{include file="templates/modules/product/benefits/fNewBenefitDialog.tpl"}
</div>-->
