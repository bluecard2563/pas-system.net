{assign var=title value="DESTINOS POR PRODUCTO"}
<form name="frmDestinations" id="frmDestinations" method="post" action="{$document_root}modules/product/productByCountry/pRegisterDestinations">
    <div class="span-24 last title ">
        Asignaci&oacute;n de Destinos por Producto<br />
		<label>(*) Campos Obligatorios</label>
    </div>
	
	<div class="span-24 last line">
		<table border="1" class="dataTable" align="center" id="destinationTable">
			<thead>
				<tr class="header">
					<th style="text-align: center;">
						<input type="checkbox" name="selAll" id="selAll"/>
					</th>
					<th style="text-align: center;">C&oacute;digo</th>
					<th style="text-align: center;">Zona</th>
					<th style="text-align: center;">Pa&iacute;s</th>
				</tr>
			</thead>
			<tbody>
				{foreach name="country" from=$completeDestinations item=c}
				<tr {if $smarty.foreach.country.iteration is even}class="odd"{else}class="even"{/if} >
					<td class="tableField">
						{html_checkboxWithValue input_name="chkDestination" serial_cou=$c.serial_cou preLoadedDestinations=$allowed_destinations}
					</td>
					<td class="tableField">{$c.code_cou}</td>
					<td class="tableField">{$c.name_zon}</td>
					<td class="tableField">{$c.name_cou}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
			
	<div class="span-24 last line center">
		<input type="button" name="btnSubmit" id="btnSubmit" value="Registrar" />
		<input type="button" name="btnBack" id="btnBack" value="Volver" />
		<input type="hidden" name="hdnSerial_pxc" id="hdnSerial" value="{$serial_pxc}" />
		<textarea name="txtValues" id="txtValue" class="hide"></textarea>
	</div>
	
</form>