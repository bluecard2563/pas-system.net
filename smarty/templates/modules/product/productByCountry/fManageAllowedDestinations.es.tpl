{assign var=title value="DESTINOS POR PRODUCTO"}
<form name="frmSearchPricesBC" id="frmSearchPricesBC" action="{$document_root}modules/product/productByCountry/registerDestinations" method="post" class="form_smarty">
    <div class="span-24 last title">ADMINISTRACI&Oacute;N DE DESTINOS POR PRODUCTO</div>
    {if $error}
        <div class="prepend-6 span-12 last">
            <div class="{if $error eq 1 || $error eq 2}success{else}error{/if} " align="center" >
                {if $error eq 1}
                    El proceso termin&oacute; exitosamente.
                {elseif $error eq 2}
                    No existe un listado de destinos permitidos. La operaci&oacute;n fue interrumpida.
                {elseif $error eq 3}
                    Hubo errores al ingresar uno de los destinos. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    <div class="prepend-7 span-9 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-2 label">
            Zona:
        </div>
        <div class="span-5 last">
            <select id="selZone" name="selZone"  title="Por favor seleccione una zona." >
                <option value="">-Seleccione-</option>
                {foreach from="$zoneList" item="z"}
                <option value="{$z.serial_zon}" {if $z.serial_zon eq $serial_zon}selected="selected"{/if}>
                    {$z.name_zon}
                </option>
                {/foreach}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-8 span-2 label">
            Pa&iacute;s:
        </div>
        <div class="span-5 last" id="countryContainer">
            <select id="selCountry" name="selCountry"  title="Por favor seleccione un pa&iacute;s." >
                <option value="">-Seleccione-</option>
                {if $countryList}
                    {foreach from="$countryList" item="c"}
                    <option value="{$c.serial_cou}" {if $c.serial_cou eq $serial_cou}selected="selected"{/if}>
                        {$c.name_cou}
                    </option>
                    {/foreach}
                {/if}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-8 span-2 label">
            Producto:
        </div>
        <div class="span-5 last" id="productContainer">
            <select id="selProduct" name="selProduct"  title="Por favor seleccione un producto." >
                <option value="">-Seleccione-</option>
                {if $productList}
                    {foreach from="$productList" item="p"}
                    <option value="{$p.serial_pro}" {if $p.serial_pro eq $serial_pro}selected="selected"{/if}>
                        {$p.name_pbl}
                    </option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last buttons ">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
</form>