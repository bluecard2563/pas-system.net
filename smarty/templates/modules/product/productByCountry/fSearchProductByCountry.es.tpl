{assign var=title value="ASIGNACI&Oacute;N DE PRODUCTOS POR PAISES"}
<form name="frmSearchProductBC" id="frmSearchProductBC" action="{$document_root}modules/product/productByCountry/fProductByCountry" method="post" class="form_smarty">
    <div class="span-24 last title">Buscar Producto</div>
    {if $error}
        <div class="prepend-7 span-10 last">
            {if $error eq 1}
				<div class="span-10 notice last line" align="center">
					RECUERDE QUE DEBE <B>CREAR EL PRODUCTO EN EL ERP</b> PARA ASEGURAR EL FUNCIONAMIENTO DE LOS M&Oacute;DULOS DE FACTURACI&Oacute;N Y COBRANZAS <b>EN ECUADOR</b>
				</div>
			{/if}
			
			<div class="span-10 last line {if $error eq 1}success{else}error{/if}" align="center" >
                {if $error eq 2}
                     No existe el producto ingresado. Por favor seleccione uno de la lista.
                {elseif $error eq 3}
                    Existi&oacute; un error al asignar el Producto a los paises.
                {elseif $error eq 1}
                    Asignaci&oacute;n exitosa.
                {/if}
            </div>
        </div>
    {/if}
    <div class="prepend-7 span-9 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
    <div class="prepend-7 span-2 label">
        Idioma:
    </div>
    <div class="span-15 last ">
        <select id="selLanguage" name="selLanguage"  class="span-6" title="Por favor seleccione un lenguaje." >
            {foreach from=$languages item="language"  }
            <option value="{$language.code_lang}" {if $code_lang eq $language.code_lang}selected{/if}>
                {$language.name_lang}
            </option>
            {/foreach}
        </select>
    </div>
    </div>

    <div class="span-24 last line">
    <div class="prepend-3 span-6 label">
        Ingrese el nombre del producto:
    </div>
    <div class="span-15 last ">
        <input type="text" id="txtName_pro" name="txtName_pro" class="span-8" title="Por favor ingrese el nombre de un producto." />
    </div>
    </div>

    <div class="span-24 last buttons ">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
        <input type="hidden" id="hdnSerial_pro" name="hdnSerial_pro" title="Por favor ingrese el nombre de un producto v&aacute;lido."/>
    </div>
</form>