{assign var=title value="ASIGNACI&Oacute;N DE TARIFAS DE PRODUCTOS POR PAISES"}
<form name="frmSearchPricesBC" id="frmSearchPricesBC" action="{$document_root}modules/product/pricesByCountry/fPricesByCountry" method="post" class="form_smarty">
    <div class="span-24 last title">Seleccione el pa&iacute;s y el producto para asignar las tarifas</div>
    {if $error}
        <div class="prepend-6 span-12 last">
            <div class="{if $error eq 1 || $error eq 2}success{else}error{/if} " align="center" >
                {if $error eq 1}
                    La asignaci&oacute;n de paises se realiz&oacute; correctamente.
                {/if}
                {if $error eq 2}
                    Las tarifas se asignaron exitosamente.
                {/if}
                {if $error eq 3}
                    Existi&oacute; un error al asignar las tarifas por favor int&eacute;ntelo nuevamente.
                {/if}
                {if $error eq 4}
                    El producto seleccionado no existe por favor int&eacute;ntelo nuevamente.
                {/if}
            </div>
        </div>
    {/if}
    <div class="prepend-7 span-9 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-2 label">
            Zona:
        </div>
        <div class="span-5 last">
            <select id="selZone" name="selZone"  title="Por favor seleccione una zona." >
                <option value="">-Seleccione-</option>
                {foreach from="$zoneList" item="z"}
                <option value="{$z.serial_zon}" {if $z.serial_zon eq $serial_zon}selected="selected"{/if}>
                    {$z.name_zon}
                </option>
                {/foreach}
            </select>
        </div>

        <div class="span-2 label">
            Pa&iacute;s:
        </div>
        <div class="span-5 last" id="countryContainer">
            <select id="selCountry" name="selCountry"  title="Por favor seleccione un pa&iacute;s." >
                <option value="">-Seleccione-</option>
                {if $countryList}
                    {foreach from="$countryList" item="c"}
                    <option value="{$c.serial_cou}" {if $c.serial_cou eq $serial_cou}selected="selected"{/if}>
                        {$c.name_cou}
                    </option>
                    {/foreach}
                {/if}
            </select>
        </div>

        <div class="span-3 label">
            Producto:
        </div>
        <div class="span-5 last" id="productContainer">
            <select id="selProduct" name="selProduct"  title="Por favor seleccione un producto." >
                <option value="">-Seleccione-</option>
                {if $productList}
                    {foreach from="$productList" item="p"}
                    <option value="{$p.serial_pro}" {if $p.serial_pro eq $serial_pro}selected="selected"{/if}>
                        {$p.name_pbl}
                    </option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last buttons ">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
</form>