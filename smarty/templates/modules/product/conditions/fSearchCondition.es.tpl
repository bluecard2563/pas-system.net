{assign var="title" value="BUSCAR CONDICI&Oacute;N"}
<form name="frmSearchCondition" id="frmSearchCondition" action="{$document_root}modules/product/conditions/fUpdateCondition" method="post" class="form_smarty">
	<div class="span-24 last title">Buscar Tipo de Condici&oacute;n<br />
        <label>(*) Campos Obligatorios</label></div>
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2 or $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    La traducci&oacute;n se ha ingresado exitosamente!
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El tipo de condici&oacute;n ingresado no existe.
                {elseif $error eq 4}
                    Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 5}
                    Hubo errores en el registro de la traducci&oacute;n. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">* Seleccione el Idioma:</div>
        <div class="span-8 append-2 last">
        <select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio." >
            <option value="">- Seleccione -</option>
            {foreach from=$languageList item="l"}
            <option value="{$l.serial_lang}" {if $l.serial_lang eq 2}selected{/if}>{$l.name_lang}</option>
            {/foreach}
        </select>
        </div>
    </div>

    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">* Ingrese el tipo de Condici&oacute;n:</div>
        <div class="span-8 append-2 last">
            <input type="text" name="txtCondition" id="txtCondition" size="27" title="El campo 'Tipo de Condici&oacute;n' es obligatorio." />
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnConditionID" id="hdnConditionID" value="" />
</form>