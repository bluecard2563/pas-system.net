{assign var="title" value="ACTUALIZAR CONDICI&Oacute;N"}
<div class="span-24 last title">
    Actualizar Condici&oacute;n<br />
        <label>(*) Campos Obligatorios</label>
</div>
{if $dataO}
<form name="frmUpdateCondicion" id="frmUpdateCondicion">
    {if $error}
    <div class="append-7 span-10 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                La actualizaci&oacute;n fue exitosa!
            {elseif $error eq 2}
                No se pudo actualizar el tipo de condici&oacute;n.
            {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line" id="adminTable">
		<div class="span-19 last line">
			<table border="0" id="translationsTable" style="color: #000000;">
				<THEAD>
					<tr bgcolor="#284787">
						<td align="center" class="tableTitle">
							No.
						</td>
						<td align="center" class="tableTitle">
							Traducci&oacute;n
						</td>
						<td align="center" class="tableTitle">
							Idioma
						</td>
						<td align="center"  class="tableTitle">
							Actualizar
						</td>
					</tr>
				</THEAD>
				<TBODY>
					{foreach name="translations" from=$data item="s"}
					<tr {if $smarty.foreach.translations.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
						<td align="center" class="tableField">
							{$smarty.foreach.translations.iteration}
							<input type="hidden" name="serial_lang_{$s.serial}" id="serial_lang_{$s.serial}" value="{$s.serial_lang}"/>
						</td>
						<td align="center" class="tableField">
							{$s.description_cbl}
							<input type="hidden" name="description_cbl_{$s.serial}" id="description_cbl_{$s.serial}" value="{$s.description_cbl}"/>
						</td>
						<td align="center" class="tableField">
							{$s.name_lang}
							<input type="hidden" name="name_lang_{$s.serial}" id="name_lang_{$s.serial}" value="{$s.name_lang}"/>
						</td>
						<td align="center" class="tableField">
							<img id="{$s.serial}" class="image" height="25px" border="0" width="25px" style="cursor: pointer;" src="{$document_root}/img/pencil.png" />
						</td>

					</tr>
					{/foreach}
				</TBODY>
			</table>
		</div>
    </div>
	 <div class="span-24 buttons last line" id="pageNavPosition"></div>

	<div class="span-24 last line buttons">
		<input type="button" name="show" id="show" value="Registrar Traducci&oacute;n" />
	</div>

    <input type="hidden" name="itemCount" id="itemCount" value="{$itemCount}" />
    <input type="hidden" name="hdnConditionID" id="hdnConditionID" value="{$dataO.serial_con}" />
</form>

<form name="frmNewTranslation" id="frmNewTranslation" method="post" action="{$document_root}modules/product/conditions/pNewTranslation">
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts_t" class="alerts"></ul>
    </div>
    
    <div id="newInfo" class="span-24 last line">
        <div class="span-24 last title">
            Nueva Traducci&oacute;n
		</div>
        {if $languageList}
         <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">* Idioma: </div>
            <div class="span-8 append-2 last">
                <select id="selLanguage" name="selLanguage" title="El campo 'Idioma' es obligatorio." class="select">
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
         </div>
        
         <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">* Descripci&oacute;n: </div>
            <div class="span-8 append-2 last">
                <input type="text" name="txtDescription" id="txtDescription" title="El campo 'Descripci&oacute;n' es obligatorio.">
            </div>
         </div>

        <div class="span-24 last buttons line">
            <input type="button" name="btnInsert" id="btnInsert" value="Registrar" class="" >
        </div>
    </div>
    <input type="hidden" name="hdnConditionID" id="hdnConditionIDT" value="{$dataO.serial_con}" />
    {else}
        <div class="span-24 center last">
            <label>Ya existen traducciones para esta restricci&oacute;n en todos los idiomas.</label>
        </div>
    {/if}
</form>

<div id="dialog" title="Actualizar ">
    {include file="templates/modules/product/conditions/fUpdateTranslation.$language.tpl"}
</div>

{else}
    <div class="append-5 span-10 prepend-4 last">
        <div class="span-10 error" align="center">
            No se pudo cargar la informaci&oacute;n del tipo de condici&oacute;n.
        </div>
    </div>
{/if}