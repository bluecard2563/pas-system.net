{assign var="title" value="ACTUALIZAR BENEFICIO"}
<div class="span-24 last title">
    Actualizar Beneficio<br />
    <label>(*) Campos Obligatorios</label>
</div>
{if $data}
<form name="frmUpdateBenefit" id="frmUpdateBenefit" method="post" action="{$document_root}modules/product/benefits/pUpdateBenefit" class="">    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
        <div class="span-10 error" align="center">
            No se pudo actualizar al beneficio.
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
   <div class="prepend-4 span-19 last line"> 
	    <div class="prepend-4 span-4 label">*Seleccione la Categoria:</div>
        <div class="span-4 append-7 last" id="categoriesContainer">     	                                  
            <select name="selCategories" id="selCategories" title='El campo "Categoría" es obligatorio'>
              {foreach from=$benefitsCategoriesList item="b"}
              <option value="{$b.serial_bcat}" {if $b.serial_bcat eq $data.serial_bcat}selected{/if}>{$b.description_bcat}</option>
              {/foreach}
            </select>
   		</div>
    </div>
        
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-5 span-4 label">Estado: </div>
        <div class="span-8 append-2 last">
            {foreach from=$statusList item="l"}
                <input type="radio" name="rdStatus" id="rdStatus" value="{$l}" {if $l eq $data.status_ben}checked{/if}>
                {$global_status.$l}
            {/foreach}
        </div>
    </div>

    <div class="prepend-3 span-19 last-2 last line" id="adminTable">
        {assign var="container" value="none"}
        {generate_admin_table data=$translations titles=$titles text="Actualizar"}
    </div>
       
    <input type="hidden" name="hdnBenefitID" id="hdnBenefitID" value="{$data.serial_ben}" />
</form>

<form name="frmNewTranslation" id="frmNewTranslation" method="post" action="{$document_root}modules/product/benefits/pNewTranslation">
    <div class="span-5 span-9 prepend-5 last">
	    <ul id="alerts_t" class="alerts"></ul>
    </div>

    <div id="newInfo" class="span-19 last line">
        <div class="span-24 last title">
            Nueva Traducci&oacute;n
        </div>
        {if $languageList}
         <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">Idioma: </div>
            <div class="span-8 append-2 last">
                <select id="selLanguage" name="selLanguage" title="El campo 'Idioma' es obligatorio." class="select">
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
         </div>

         <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-8 label">Descripci&oacute;n: </div>
            <div class="span-8 append-2 last">
                <input type="text" name="txtDescription" id="txtDescription" title="El campo 'Descripci&oacute;n' es obligatorio.">
            </div>
         </div>

        <div class="span-24 last buttons line">
            <input type="button" name="btnInsert" id="btnInsert" value="Registrar" class="" >
        </div>
    </div>
    <input type="hidden" name="hdnBenefitID" id="hdnBenefitIDT" value="{$data.serial_ben}" />
    {else}
        <div class="span-14 append-5 last">
            <div class="span-14 label">Ya existen traducciones para este beneficio en todos los idiomas.</div>
        </div>
    {/if}
</form>

<div id="dialog" title="Actualizar ">
    {include file="templates/modules/product/benefits/fUpdateTranslation.$language.tpl"}
</div>

{else}
	<div class="append-5 span-10 prepend-4 last">
        <div class="span-10 error" align="center">
            No se pudo cargar la informaci&oacute;n del Beneficio.
        </div>
    </div>
{/if}