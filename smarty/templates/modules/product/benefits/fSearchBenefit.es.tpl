{assign var="title" value="BUSCAR BENEFICIO"}
<form name="frmSearchBenefit" id="frmSearchBenefit" action="{$document_root}modules/product/benefits/fUpdateBenefit" method="post" class="form_smarty">
   <div class="span-24 last title">Buscar Beneficio<br />
        <label>(*) Campos Obligatorios</label></div>
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2 or $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    La traducci&oacute;n se ha ingresado exitosamente!
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El beneficio ingresado no existe.
                {elseif $error eq 4}
                    Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 5}
                    Hubo errores en el registro de la traducci&oacute;n. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-4 span-5 label">* Idioma:</div>
        <div class="span-5 append-5 last">
            <select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio">
                <option value="">- Seleccione -</option>
                {foreach from=$languageList item="l"}
                <option value="{$l.serial_lang}">{$l.name_lang}</option>
                {/foreach}
            </select>
        </div>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-5 label">* Ingrese el Beneficio:</div>
        <div class="span-11 append-2 last">
            <input type="text" name="txtNameBenefit" id="txtNameBenefit" title="El campo 'Descripci&oacute;n' es obligatorio." class="autocompleter" />
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnBenefitID" id="hdnBenefitID" value="" />
</form>