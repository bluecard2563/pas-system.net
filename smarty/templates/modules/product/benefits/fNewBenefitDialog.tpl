<form name="frmNewBenefit" id="frmNewBenefit"  method="post" action="{$document_root}modules/product/benefits/pNewBenefitDialog" class="">

    <div class="span-1 span-8 prepend-1 last">
	    <ul id="alertsDialogBenefit" class="alerts"></ul>
    </div>
    
    <div class="span-10 last line ">
		<div class="span-4 label">Estado Beneficio:</div>
        <div class="span-6 last"> 
        	<select name="selStatusBenefit" id="selStatusBenefit" title="El campo 'Estado' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item=sl}
                	<option value="{$sl}">{if $sl eq 'ACTIVE'}Activo{elseif $sl eq 'INACTIVE'}Inactivo{/if}</option>
                {/foreach}
            </select>
        </div>
	</div>

        <div class="span-10 last line">
            <div class="span-4 label">Priorizaci&oacute;n:</div>
            <div class="span-6 last">
                    <input type="text" name="txtWeightBenefit" id="txtWeightBenefit" value="" title="El campo 'Priorizaci&oacute;n' es obligatorio." >
            </div>
        </div>    

     <div class="span-10 last line">
         <div class="span-4 label">Descripci&oacute;n Beneficio:</div>
        <div class="span-6 last"> 
	        <textarea name="txtDescBenefit" id="txtDescBenefit" title="El campo 'Descripci&oacute;n' es obligatorio." class="txtArea"></textarea>
        </div>
	</div>
</form>