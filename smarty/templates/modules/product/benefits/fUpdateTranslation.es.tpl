<form id="frmUpdateTranslation" name="frmUpdateTranslation" method="post" action="{$document_root}modules/product/benefits/pUpdateTranslation" >
<div class="span-1 span-8 prepend-1 last line">
    <ul id="alerts_dialog" class="alerts"></ul>
</div>

<div class="span-10 last line">
    <div class="span-4 label">
    	Idioma:
    </div>
    <div class="span-4 append-1 last" id="language"></div>
</div>

<div class="span-10 last line">
    <div class="span-4 label">Priorizaci&oacute;n:</div>
    <div class="span-4 append-1 last">
            <input type="text" name="txtWeightBenefit" id="txtWeightBenefit" value="" title="El campo 'Priorizaci&oacute;n' es obligatorio." >
    </div>
</div>

<div class="span-10 last line">
    <div class="span-4 label">
    	* Descripci&oacute;n:
    </div>
    <div class="span-4 append-1 last">
    	<input type="text" name="txtBenefit" id="txtBenefit" value="" title="El campo 'Descripci&oacute;n' es obligatorio." />
    </div>
</div>

<input type="hidden" name="serial_bbl" id="serial_bbl" />
<input type="hidden" name="serial_lang" id="serial_lang" />
<input type="hidden" name="serial_ben" id="serial_ben" />
</form>