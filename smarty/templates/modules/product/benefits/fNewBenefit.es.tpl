{assign var="title" value="NUEVO BENEFICIO"}
<form name="frmNewBenefit" id="frmNewBenefit" method="post" action="{$document_root}modules/product/benefits/pNewBenefit" class="">
    <div class="span-24 last title">
    	Registro de Nuevo Beneficio<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
        {if $error eq 1}
            El beneficio se ha registrado exitosamente!
        {elseif $error eq 2}
            No se pudo registrar al beneficio.
        {elseif $error eq 3}
            No se pudo registrar la traducci&oacute;n del beneficio. Por favor comun&iacute;quese con el administrador.
        {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-19 last line"> 
	    <div class="prepend-4 span-4 label">*Seleccione la Categoria:</div>
        <div class="span-4 append-7 last" id="categoriesContainer">     	                                  
            <select name="selCategories" id="selCategories" title='El campo "Categoría" es obligatorio'>
              <option value="">- Seleccione -</option>
              {foreach from=$benefitsCategoriesList item="l"}
              <option value="{$l.serial_bcat}">{$l.description_bcat}</option>
              {/foreach}
            </select>
   		</div>
    </div>
            
    <div class="span-19 last line">
        <div class="prepend-4 span-4 label">Priorizaci&oacute;n Beneficio:</div>
    	<div class="span-4 append-7 last">
        	<input type="text" name="txtWeightBenefit" id="txtWeightBenefit" title="El campo 'Priorizaci&oacute;n' es obligatorio." >
        </div>
    </div>

    <div class="span-19 last line">
        <div class="prepend-4 span-4 label">* Descripci&oacute;n Beneficio:</div>
    	<div class="span-4 append-7 last">
        	<textarea name="txtDescBenefit" id="txtDescBenefit" title="El campo 'Descripci&oacute;n' es obligatorio." ></textarea>
        </div>
    </div>
       
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
    </div>
</form>