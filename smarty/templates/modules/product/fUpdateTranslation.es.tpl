<form id="frmUpdateTranslation" name="frmUpdateTranslation" method="post" action="{$document_root}modules/product/pUpdateTranslation" >
<div class="span-1 span-8 prepend-1 last line">
    <ul id="alerts_dialog" class="alerts"></ul>
</div>

<div class="span-10 last line">
    <div class="prepend-1 span-4 label">
    	Idioma:
    </div>
    <div class="span-4 append-1 last" id="language"></div>
</div>
    
<div class="span-10 last line">
            <div class="prepend-1 span-4 label">
                <label>* Nombre del Producto :</label>
            </div>

        <div class="span-4 append-1 last ">
            <div>
                <input type="text" name="diaName_pro" id="diaName_pro" class="span-4" />
            </div>
        </div>
    </div>
<div class="span-10 last line">
        <div class="prepend-1 span-4 label">
            <label>* Descripci&oacute;n del Producto :</label>
        </div>

        <div class="span-4 append-1 last ">
            <div>
                <textarea  name="txtDescription_pro" id="txtDescription_pro" class="span-4" ></textarea>
            </div>
        </div>
    </div>

<input type="hidden" name="serial_pbl" id="serial_pbl" />
<input type="hidden" name="serial_lang" id="serial_lang" />
</form>

{literal}
<script>
	$(document).ready(function(){
	
  $("#dialog").dialog({
		bgiframe: true,
		autoOpen: false,
		height: 400,
		width: 410,
		modal: true,
		buttons: {
			'Actualizar': function() {
				  $("#frmUpdateTranslation").validate({
						errorLabelContainer: "#alerts_dialog",
						wrapper: "li",
						onfocusout: false,
						onkeyup: false,
						rules: {
							txtBenefit: {
								required: true,
								remote: {
									url: document_root+"rpc/remoteValidation/product/checkBenefit.rpc",
									type: "post",
									data: {
									  serial_lang: function() {
										return $("#serial_lang").val();
									  },
									  serial_ben: function() {
										return $("#hdnBenefitID").val();
									  }
									}
								}
							}
						},
						messages: {
							txtBenefit: {
								remote: "La traducci&oacute;n ingresada ya existe en el sistema."
							}
						}
					});

				   if($('#frmUpdateTranslation').valid()){
					$('#frmUpdateTranslation').submit();
				   }
				},
			Cancelar: function() {
				$('#alerts_dialog').css('display','none');
				$(this).dialog('close');
			}
		  },
		close: function() {
			$('select.select').show();
		}
	});
});

</script>
{/literal}