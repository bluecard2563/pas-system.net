{assign var="title" value="NUEVO COMERCIALIZADOR"} 
<form name="frmNewDealer" id="frmNewDealer" method="post" action="{$document_root}modules/dealer/pNewDealer" class="">
    <div class="span-24 last title">
        Registro de Nuevo Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                El comercializador se ha registrado exitosamente!<br/>
                El c&oacute;digo es {$recentlyCreatedDealerCode}
            {elseif $error eq 2}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {elseif $error eq 3}
                Se ingres&oacute; el comercializador exitosamente pero hubo problemas al crear la sucursal de comercializador.
            {elseif $error eq 4}
                Se ingres&oacute; el comercializador exitosamente pero hubo problemas al asignar el responsable.
            {elseif $error eq 5}
                Se ingres&oacute; el comercializador exitosamente pero hubo problemas al asignar el responsable de la sucursal de comercializador.
            {elseif $error eq 6}
                 Se ingres&oacute; el comercializador y la sucursal de comercializador exitosamente pero hubo problemas al asignar el responsables.
            {elseif $error eq 7}
                 Hubo errores en el ingreso del c&oacute;digo. Por favor vuelva a intentarlo.
             {elseif $error eq 8}
                Se ingres&oacute; el comercializador y la sucursal de comercializador exitosamente pero hubo problemas al enviar e-mail de notificaci&oacute;n.
            {elseif $error eq 9}
                Se actualiz&oacute; el comercializador exitosamente pero hubo problemas al enviar e-mail de notificaci&oacute;n.
{elseif $error eq 10}
                El formato de la imagen no es correcto.
			{elseif $error eq 11}
				El tama�o de la imagen supera el tama�o m�ximo permitido.
            {/if}
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
            {if $isMainManagerUser}
                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach name="countries" from=$countryList item="l"}
                        <option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select>
            {else}
                {$nameCountry}
				<input type="hidden" name="selCountry" id="selCountry" value="{$idCountry}" />
            {/if}
        </div>

        <div class="span-3 label">* Ciudad:</div>
        <div class="span-4 append-4 last" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {if $cityList}
                    {foreach name="cities" from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $smarty.foreach.cities.total eq 1}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Sector:</div>
        <div class="span-5" id="sectorContainer">
            <select name="selSector" id="selSector" title='El campo "Sector" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>

        <div class="span-3 label">* Representante:</div>
        <div class="span-4 append-4 last" id="managerContainer">
            {if $isMainManagerUser}
                <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            {else}
                {$nameManager|upper}
                <input type="hidden" name="hdnOfficialMbc" id="hdnOfficialMbc" value="{$isOfficialManager}" />
            {/if}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-16 span-5 last line" id="officialSellerContainer">
            {if not $hasOfficialSeller}
            	<input type="checkbox" name="chkOfficialSeller" id="chkOfficialSeller" value="1" />Vendedor oficial
            {/if}
        </div>
		<div class="prepend-16 span-5 last line" id="phoneSalesContainer">
            {if $isMainManagerUser && not $hasPhoneSales}
				<input type="checkbox" name="chkPhoneSales" id="chkPhoneSales" value="1" />Venta Telef&oacute;nica
            {/if}
        </div>
    </div>

	

    <div class="span-24 last line separator">
        <label>Asignaci&oacute;n de responsable:</label>
    </div>

    <div class="prepend-8 span-17 last line" id="comissionistContainer">
        {if $serialMbc}
        <div class="span-4 label">* Responsable:</div>
        <div class="span-5">
			{if $comissionistList}
                <select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                        {foreach from=$comissionistList item="l"}
                            <option value="{$l.serial_usr}" >{$l.first_name_usr} {$l.last_name_usr}</option>
                        {/foreach}
                </select>
			{else}
			No existen usuarios registrados.
			{/if}
        </div>

        {else}
            <div class="prepend-2 span-6">Seleccione un representante.</div>
        {/if}
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n General:</label>
    </div>
	
	{if $isMainManagerUser}
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Es Sub-Representante?:</div>
        <div class="span-5">
            Si <input type="radio" name="rdManagerRights[]" id="rdManagerRights_YES" value="YES" />
			No <input type="radio" name="rdManagerRights[]" id="rdManagerRights_NO" value="NO" checked="" />
        </div>
    </div>
	{else}
	<div class="span-24 last">
        <input type="hidden" name="rdManagerRights[]" id="rdManagerRights_NO" value="NO" />
    </div>
	{/if}

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Id:</div>
        <div class="span-5">
                <input type="text" name="txtID" id="txtID" title='El campo "Id" es obligatorio.' />
        </div>

        <div class="span-3 label">* Nombre:</div>
        <div class="span-4 append-4 last" id="categoryContainer">
            <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tipo de Comercializador:</div>
        <div class="span-5" id="categoryContainer">
                <select name="selDealerT" id="selDealerT" title='El campo "Tipo de Comercializador" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$dTypesList item="dt"}
                        <option value="{$dt.serial_dlt}">{$dt.name}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Categor&iacute;a:</div>
        <div class="span-4 append-4 last" id="categoryContainer">
                <select name="selCategory" id="selCategory" title='El campo "Categor&iacute;a" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$categoryList item="l"}
                    <option value="{$l}">{$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio'>
        </div>

        <div class="span-3 label">* Estado:</div>
        <div class="span-4 append-4 last">
                <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$statusList item="l"}
                    <option value="{$l}" {if $l eq "ACTIVE"}selected{/if}>{$global_status.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono #1:</div>
        <div class="span-5">
            <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono 1" es obligatorio'>
        </div>

        <div class="span-3 label">Tel&eacute;fono 2:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtPhone2" id="txtPhone2" >
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Fax:</div>
        <div class="span-5">
                <input type="text" name="txtFax" id="txtFax">
        </div>

        <div class="span-3 label">* Fecha del Contrato:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtContractDate" id="txtContractDate" title='El campo "Fecha del Contrato" es obligatorio'>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* E-mail Comercializador:</div>
        <div class="span-5">
            <input type="text" name="txtMail1" id="txtMail1" title='El campo "E-mail #1" es obligatorio.'>
        </div>

        <div class="span-3 label">* Email de Facturación:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtMail2" id="txtMail2" title='El campo "E-mail F.E." es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre contacto Cobranzas:</div>
        <div class="span-5">
            <input type="text" name="txtContact" id="txtContact" title='El campo de "Nombre del Contacto" es obligatorio.' />
        </div>

        <div class="span-3 label">* Tel&eacute;fono del Contacto:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtContactPhone" id="txtContactPhone" title='El campo "Tel&eacute;fono del Contacto" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Email de Cobranzas:</div>
        <div class="span-5">
            <input type="text" name="txtContactMail" id="txtContactMail" title='El campo de "E-mail del Contacto" es obligatorio.' />
        </div>

        <div id="visitsContainer">
            <div class="span-3 label">* N&uacute;mero de visitas al mes:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtVisitAmount" id="txtVisitAmount" title='El campo "N&uacute;mero de visitas al mes" es obligatorio.'/>
            </div>
        </div>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">M&iacute;nimo de Stock (%):</div>
        <div class="span-4 last">
            <input type="text" name="txtMinimumKits" id="txtMinimumKits" value="" />
        </div>
		
		<div class="span-3 label">Logo:</div>
        <div class="span-4 append-4 last">
            <input type="file" name="txtLogo_dea" id="txtLogo_dea" title="Formato: JPG  Tama�o m&aacute;x: 2MB"/>
	</div>
	</div>

	<div class="span-24 last line separator">
        <label>Informaci&oacute;n del Gerente:</label>
    </div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre del Gerente:</div>
        <div class="span-5">
            <input type="text" name="txtNameManager" id="txtNameManager" title='El campo de "Nombre del Gerente" es obligatorio.' />
        </div>

        <div class="span-3 label">* Tel&eacute;fono del Gerente:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* E-mail del Gerente:</div>
        <div class="span-5">
            <input type="text" name="txtMailManager" id="txtMailManager" title='El campo de "E-mail del Gerente" es obligatorio.' />
        </div>

        <div class="span-3 label">Fecha de Nacimiento:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtBirthdayManager" id="txtBirthdayManager"/>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Pagos:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Contacto Pago:</div>
        <div class="span-5">
                <input type="text" name="txtContactPay" id="txtContactPay" title='El campo de "Contacto Pago" es obligatorio.' />
        </div>

        <div class="span-3 label">* Factura a nombre del:</div>
        <div class="span-4 append-4 last">
            <input type="radio" name="chkBillTo" id="chkBillTo" title='El campo de "Factura" es obligatorio.' value="DEALER" checked /> Comercializador <br />
            <input type="radio" name="chkBillTo" id="chkBillTo" value="CUSTOMER" /> Cliente
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* D&iacute;a de Pago:</div>
        <div class="span-5">
                <select name="selPayment" id="selPayment" title='El campo "D&iacute;a de Pago" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$deadlineList item="l"}
                    <option value="{$l}">{$global_weekDays.$l}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* D&iacute;as de Cr&eacute;dito:</div>
        <div class="span-4 append-4 last" id="creditDContainer">
                <select name="selCreditD" id="selCreditD" title='El campo "D&iacute;a de Cr&eacute;dito" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$dataByCountry item="l"}
                    <option value="{$l.serial}" >{$l.days_cdd}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="hideSection">
        <div class="prepend-4 span-4 label">* Porcentaje:</div>
        <div class="span-5"> <input type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje" es obligatorio.'/> </div>

        <div class="span-3 label">* Tipo:</div>
        <div class="span-4 append-4 last">
            <select name="selType" id="selType" title='El campo de "Tipo" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$typeList item="l"}
                    <option value="{$l}">{$global_pType.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="hideSection">
        <div class="prepend-4 span-4 label">* Incentivo a:</div>
        <div class="span-5">
            <input type="radio" name="chkBonusTo" id="chkBonusTo" title='El campo de "Incentivo" es obligatorio.' value="DEALER" checked /> Comercializador <br />
            <input type="radio" name="chkBonusTo" id="chkBonusTo" value="COUNTER" /> Counter
        </div>
    </div>
	
	<div class="span-24 last line separator">
        <label>Informaci&oacute;n de CASH Management:</label>
    </div>
			
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Banco:</div>
        <div class="span-5">
			<select name="selBank" id="selBank">
                <option value="">- Seleccione -</option>
                {foreach from=$banks item="b"}
                    <option value="{$b}">{$b}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">Tipo de Cuenta:</div>
        <div class="span-4 append-4 last">
            <select name="selAccountType" id="selAccountType">
                <option value="">- Seleccione -</option>
                {foreach from=$account_types item="ac"}
                    <option value="{$ac}">{$global_account_types[$ac]}</option>
                {/foreach}
            </select>
        </div>
    </div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">N&uacute;mero de Cuenta:</div>
        <div class="span-5">
			<input type="text" name="txtAccountNumber" id="txtAccountNumber" />
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Pre liquidación:</label>
    </div>
            
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Recibir Notificaciones?:</div>
        <div class="span-5">
            Si <input type="radio" name="rdNotifications" id="rdNotifications" value="YES" /> 
            No <input type="radio" name="rdNotifications" id="rdNotifications" value="NO" checked /> 
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
                <input type="text" name="txtAssistName" id="txtAssistName" title='El campo de "Nombre (Asistencias)" es obligatorio.' />
        </div>

        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtAssistEmail" id="txtAssistEmail" title='El campo de "E-mail (Assistencias)" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="submit" name="btnRegister" id="btnRegister" value="Registrar" />
    </div>
    <input type="hidden" name="hdnFlagCode" id="hdnFlagCode" value="1" />
    <input type="hidden" name="hdnSerialMbc" id="hdnSerialMbc" value="{$serialMbc}"/>
    <input type="hidden" name="hdnSerialUsr" id="hdnSerialUsr" value="" title='El campo "Responsable" es obligatorio.' />
	<input type="hidden" name="today" id="today" value="{$today}" />
</form>