{assign var="title" value="NUEVO TIPO DE COMERCIALIZADOR"}
<form name="frmNewDealerType" id="frmNewDealerType" method="post" action="{$document_root}modules/dealer/dealerType/pNewDealerType" />
	<div class="span-24 last title">
    	Registro de Nuevo Tipo de Comercializador
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
      		El tipo de comercializador se ha registrado exitosamente!
        {elseif $error eq 2}
      		No se pudo registrar al nuevo tipo.
        {elseif $error eq 3}
      		Se registro el nuevo tipo pero no su nombre en el idioma seleccionado.
        {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
   	<div class="span-24 last line">
        <div class="prepend-7 span-5 label">*Alias:</div>
        <div class="span-10 last">
        	<input type="text" name="txtAlias" id="txtAlias" title="El campo 'Alias' es obligatorio.">
        </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-7 span-5 label">*Nombre:</div>
        <div class="span-10 last" id="branchContainer">
            <input type="text" name="txtDesc" id="txtDesc" title="El campo 'Nombre' es obligatorio." />
        </div>
	</div>
       
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" />
        <input type="hidden" name="serialLanguage" id="serialLanguage" value="{$language}" />
    </div>
</form>