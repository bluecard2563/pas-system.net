{assign var="title" value="ACTUALIZAR TIPO DE COMERCIALIZADOR"}

<form name="frmUpdateDealerTypeStatus" id="frmUpdateDealerTypeStatus" method="post" action="{$document_root}modules/dealer/dealerType/pUpdateDealerTypeStatus">
    <div class="span-24 last line">
    <div class="prepend-10 span-2"><input type="radio" name="rdoStatus" id="rdoStatusAct" {if $status_dlt eq "ACTIVE"}checked{/if} value="ACTIVE" />Activo</div>
    <div class="span-4 append-6 last"><input type="radio" name="rdoStatus" id="rdoStatus2Inact" {if $status_dlt eq "INACTIVE"}checked{/if} value="INACTIVE" />Inactivo</div>
    </div>
    <input type="hidden" name="hdnSerialDealerType" id="hdnSerialDealerType" value="{$hdnDealTypeID}" />
</form>

<form name="frmTranslateDealerType" id="frmTranslateDealerType" method="post" action="{$document_root}modules/dealer/dealerType/pTranslateDealerType">
    <div class=" prepend-2 span-19 append-2 last line">
        {assign var="container" value="none"}
        {generate_admin_table data=$data titles=$titles text=$text}
    </div>
    
    
    <div class="span-24 last line" id="newInfo">
    	<div class="span-24 last title">
    		Nueva Traducci&oacute;n
    	</div>
        
        <div class="span-7 span-10 prepend-7 last">
            <ul id="alerts" class="alerts"></ul>
        </div>
        
    	{if $languageList}
        <div class="span-5 span-9 prepend-5 last">
            <ul id="alerts" class="alerts"></ul>
        </div>
        
        <div class="span-24 last line">
            <div class="prepend-5 span-7 label">Idioma:</div>
            <div class="span-7 append-5 last">
                <select name="selLanguage" id="selLanguage" title='El campo "Idioma" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        
        <div class="span-24 last line">
            <div class="prepend-5 span-7 label">Nombre:</div>
            <div class="span-7 append-5 last">
                <input type="text" name="txtDesc" id="txtDesc" title="El campo 'Nombre' es obligatorio."> 
            </div>
        </div>

        <div class="span-24 last line buttons">
            <input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar">
        </div>
        <input type="hidden" name="hdnSerialDealerType" id="hdnSerialDealerType" value="{$hdnDealTypeID}" />
        {else}
        <div class="prepend-8 span-14 last line">
            Ya existen traducciones para todos los lenguajes disponibles.
        </div>
        {/if}
    </div>
</form>

<div id="dialog" title="Actualizar Tipo de Comercializador">
	{include file="templates/modules/dealer/dealerType/fUpdataDealerTypeDialog.$language.tpl"}
</div>

