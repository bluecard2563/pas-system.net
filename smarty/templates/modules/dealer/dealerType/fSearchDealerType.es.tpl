{assign var="title" value="BUSCAR TIPO DE COMERCIALIZADOR"}
<form name="frmSearchDealerType" id="frmSearchDealerType" action="{$document_root}modules/dealer/dealerType/fUpdateDealerType" method="post" />
	<div class="span-24 last title">Buscar Tipo de Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1 or $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 2}
                    Traducci&oacute;n exitosa!
                {elseif $error eq 3}
                    El tipo de comercializador no existe. 
				{elseif $error eq 4}
                      Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 5}
                      Hubo errores en la traducci&oacute;n. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-8 span-4 label">*Idioma:</div>
        <div class="span-8 append-4 last">
          	<select name="selLanguage" id="selLanguage" title='El campo "Idioma" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$languageList item="l"}
                    <option value="{$l.serial_lang}">{$l.name_lang}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">*Ingrese el Tipo de Comercializador:</div>
        <div class="span-8 append-2 last">
            <input type="text" name="txtPattern" id="txtPattern" size="34" title="El campo 'Tipo' es obligatorio." />
        </div>
    </div>
        
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
        <input type="hidden" name="hdnDealTypeID" id="hdnDealTypeID" />
    </div>
</form>