{assign var="title" value="NEW DEALER TYPE"}
<form name="frmNewDealerType" id="frmNewDealerType" method="post" action="{$document_root}modules/dealer/dealerType/pNewDealerType" />
	<div class="span-19 last title">
    	New Dealer Type
    </div>
    
	{if $error}
    <div class="append-5 span-10 prepend-4 last">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
      		The Dealer Type has been registered!
        {elseif $error eq 2}
      		We're sorry. There were errors in the process.
        {elseif $error eq 3}
      		The Dealer Type has benn registered. But we failed in register the translation.
        {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-5 span-9 prepend-5 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
   	<div class="span-19 last line">
        <div class="prepend-4 span-5 label">* Alias:</div>
        <div class="span-10 last">
        	<input type="text" name="txtAlias" id="txtAlias" title="The field 'Alias' is required.">
        </div>
	</div>
    
    <div class="span-19 last line">
        <div class="prepend-4 span-5 label">* Name</div>
        <div class="span-10 last" id="branchContainer">
            <input type="text" name="txtDesc" id="txtDesc" title="The field 'Nombre' is required." />
        </div>
	</div>
       
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Register" />
        <input type="hidden" name="serialLanguage" id="serialLanguage" value="{$language}" />
    </div>
</form>