{assign var="title" value="BUSCAR COMERCIALIZADOR"}
<div class="span-24 last title">Buscar Comercializador</div>
<form name="frmSearchDealer" id="frmSearchDealer" action="{$document_root}modules/dealer/fUpdateDealer" method="post" />
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El comercializador ingresado no existe.
                {elseif $error eq 4}
                    El comercializador se actualiz&oacute; exitosamente pero hubo errores al actualizar el responsable.
                {elseif $error eq 5}
                    El comercializador se actualiz&oacute; exitosamente pero hubo errores al actualizar el responsable de las sucursales de comercializador.
                {elseif $error eq 6}
                    Hubo un error al procesar la informaci&oacute;n, por favor contacte al administrador.
                {elseif $error eq 7}
                	Actualizaci&oacute;n exitosa pero hubo errores al enviar e-mail de notificaci&oacute;n.
{elseif $error eq 8}
					Formato de imagen incorrecto, intente subiendo una imagen JPG 
				{elseif $error eq 9}
					El tamaño de la imagen supera el máx permitido
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
 <!--   <div class="span-24 last line">
        <div class="prepend-3 span-8 label">Ingrese el nombre o c&oacute;digo del comercializador:</div>
        <div class="span-11 append-2 last">
            <input type="text" name="txtDealerData" id="txtDealerData" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
        </div>
	</div>-->

	 <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name="zones" from=$zonesList item="z"}
		<option value="{$z.serial_zon}">{$z.name_zon|htmlall}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
        	<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Ciudad:</div>
        <div class="span-5" id="cityContainer">
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

	<div class="span-24 last line">

		<div class="prepend-4 span-4 label">* Comercializador:</div>
        <div class="span-5" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
       </div>
	</div>

    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnDealerID" id="hdnDealerID" value="" />
</form>