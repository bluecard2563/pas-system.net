{assign var="title" value="ACTUALIZAR COMERCIALIZADOR"} 
<div class="span-24 last title">
    Actualizar Comercializador<br />
{if $data}<label>(*) Campos Obligatorios</label>{/if}
</div>
{if $data}  
    <form name="frmUpdateDealer" id="frmUpdateDealer" method="post" action="{$document_root}modules/dealer/pUpdateDealer" class="" enctype="multipart/form-data" >
        {if $error}
            <div class="append-7 span-10 prepend-7 last ">
                <div class="span-10 error" align="center">
                    {if $error eq 1}
                        Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                    {/if}
                </div>
            </div>
        {/if}

        <div class="span-7 span-10 prepend-7 last">
            <ul id="alerts" class="alerts"></ul>
        </div>

        <div class="span-24 last line separator">
            <label>Informaci&oacute;n Geogr&aacute;fica:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Pa&iacute;s:</div>
            <div class="span-5">{$data.name_cou}</div>

            <div class="span-3 label">Ciudad:</div>
            <div class="span-4 append-4 last">{$data.name_cit}</div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Sector:</div>
            <div class="span-5">
                <select name="selSector" id="selSector" title="El campo 'Sector' es obligatorio.">
                    <option value=''>- Seleccione -</option>
                    {foreach from=$sectorList item=s}
                        <option value="{$s.serial_sec}" {if $s.serial_sec eq $data.serial_sec}selected{/if}>{$s.name_sec}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">Representante:</div>
            <div class="span-5">{$data.name_man}</div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">C&oacute;digo:</div>
            <div class="span-4">{$data.code_cou|upper}-{$data.code_cit|upper}-{$data.code_dea}</div>


            <div class="span-5 label">Comercializador Oficial:</div>
            <div class="span-3">{if $data.official_dea eq "YES"}Si{else}No{/if}</div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Creado Por:</div>
            <div class="span-4">{if $data.creator_name}{$data.creator_name}{else}N/A{/if}</div>


            <div class="span-5 label">Fecha de Creaci&oacute;n:</div>
            <div class="span-3">{if $data.created_on}{$data.created_on}{else}N/A{/if}</div>
        </div>
        
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Responsable:</div>
            <div class="span-4">
                {if $data.comissionist_name neq ''}{$data.comissionist_name}{else}N/A{/if}
                <input type="hidden" name="rdoSelComissionist" id="rdoSelComissionist" value="{$data.serial_usr}" />
            </div>
        </div>


        <div class="span-24 last line separator">
            <label>Informaci&oacute;n de General:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Sub-Representante?:</div>
            <div class="span-5">
                {if $data.manager_rights eq 'YES'}Si{else}No{/if}
                <input type="hidden" name="rdManagerRights[]" id="rdManagerRights" value="{$data.manager_rights}" />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Id:</div>
            <div class="span-5">
                {$data.id_dea}
                <input type="hidden" name="txtID" id="txtID" title='El campo "Id" es obligatorio.' value="{$data.id_dea}" />
            </div>

            <div class="span-3 label">* Nombre:</div>
            <div class="span-4 append-4 last"> 
                {$data.name_dea|htmlall}
                <input type="hidden" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' value="{$data.name_dea}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tipo de Comercializador:</div>
            <div class="span-5" id="categoryContainer"> 
                <select name="selDealerT" id="selDealerT" title='El campo "Tipo de Comercializador" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$dTypesList item="dt"}
                        <option value="{$dt.serial_dlt}" {if $dt.serial_dlt eq $data.serial_dlt} selected="selected"{/if}>{$dt.name|htmlall}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Categor&iacute;a:</div>
            <div class="span-4 append-4 last" id="categoryContainer"> 
                <select name="selCategory" id="selCategory" title='El campo "Categor&iacute;a" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$categoryList item="l"}
                        <option value="{$l}" {if $l eq $data.category_dea} selected="selected"{/if}>{$l}</option>
                    {/foreach}
                </select> 
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$data.address_dea}">
            </div>

            <div class="span-3 label">* Estado:</div>
            <div class="span-4 append-4 last">
                <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$statusList item="l"}
                        <option value="{$l}" {if $l eq $data.status_dea} selected="selected"{/if}>{$global_status.$l}</option>
                    {/foreach}
                </select>
            </div>       
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono #1:</div>
            <div class="span-5">
                <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono 1" es obligatorio' value="{$data.phone1_dea}">
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhone2" id="txtPhone2" value="{$data.phone2_dea}">
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Fax:</div>
            <div class="span-5">
                <input type="text" name="txtFax" id="txtFax" value="{$data.fax_dea}"> 
            </div>

            <div class="span-3 label">* Fecha del Contrato:</div>
            <div class="span-5 append-3 last">
                {$data.contract_date}
                <input type="hidden" name="txtContractDate" id="txtContractDate" title='El campo "Fecha del Contrato" es obligatorio' value="{$data.contract_date}">
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail Comercializador::</div>
            <div class="span-5">
                <input type="text" name="txtMail1" id="txtMail1" title='El campo "E-mail #1" es obligatorio.' value="{$data.email1_dea}">
            </div>

            <div class="span-3 label">* Email de Facturación:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtMail2" id="txtMail2" value="{$data.email2_dea}" title='El campo "E-mail F.E." es obligatorio.'/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre contacto Cobranzas:</div>
            <div class="span-5">
                <input type="text" name="txtContact" id="txtContact" title='El campo de "Nombre del Contacto" es obligatorio.' value="{$data.contact_dea|htmlall}"/>
            </div>

            <div class="span-3 label">* Tel&eacute;fono del Contacto:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtContactPhone" id="txtContactPhone" title='El campo "Tel&eacute;fono del Contacto" es obligatorio.' value="{$data.phone_contact}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Email de Cobranzas:</div>
            <div class="span-5">
                <input type="text" name="txtContactMail" id="txtContactMail" title='El campo de "E-mail del Contacto" es obligatorio.' value="{$data.email_contact}"/>
            </div>

            <div class="span-3 label">* N&uacute;mero de visitas al mes:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtVisitAmount" id="txtVisitAmount" title='El campo "N&uacute;mero de visitas al mes" es obligatorio.' value="{$data.visits_number}"/>
            </div>
        </div>
			
<div class="span-24 last line">
			{if $data.logo_dea!=NULL}
			<div class="prepend-4 span-4 label">Logo Actual:</div>
			<div class="span-6 last">
				<img src="{$document_root}img/dea_logos/{$data.logo_dea}" width="100"/>
			</div>
			{/if}
			
			
			{if $data.logo_dea!=NULL}
			<div class="span-2 label">Logo:</div>
			
			{elseif $data.logo_dea==NULL}
			<div class="span-6 label">Logo:</div>
			{/if}
			<div class="span-4 append-4 last">
				<input type="file" name="txtLogo_dea" id="txtLogo_dea" title="Formato: JPG  Tamaño m&aacute;x: 2MB" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">M&iacute;nimo de Stock (%):</div>
			<div class="span-4 last">
				<input type="text" name="txtMinimumKits" id="txtMinimumKits" title='El campo "M&iacute;nimo de Stock" es obligatorio.' value="{$data.minimumKits}" />
			</div>
		</div>

        <div class="span-24 last line separator">
            <label>Informaci&oacute;n del Gerente:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre del Gerente:</div>
            <div class="span-5">
                <input type="text" name="txtNameManager" id="txtNameManager" title='El campo de "Nombre del Gerente" es obligatorio.'  value="{$data.manager_name|htmlall}"/>
            </div>

            <div class="span-3 label">* Tel&eacute;fono del Gerente:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio.' value="{$data.manager_phone}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail del Gerente:</div>
            <div class="span-5">
                <input type="text" name="txtMailManager" id="txtMailManager" title='El campo de "E-mail del Gerente" es obligatorio.' value="{$data.manager_email}"/>
            </div>

            <div class="span-3 label">Fecha de Nacimiento:</div>
            <div class="span-5 append-3 last">
                <input type="text" name="txtBirthdayManager" id="txtBirthdayManager" value="{$data.manager_birthday}"/>
            </div>
        </div>

        <div class="span-24 last line separator">
            <label>Informaci&oacute;n de Pagos:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Contacto Pago:</div>
            <div class="span-5">
                <input type="text" name="txtContactPay" id="txtContactPay" title='El campo de "Contacto Pago" es obligatorio.' value="{$data.pay_contact|htmlall}"/>
            </div>   

            <div class="span-3 label">* Factura a nombre del:</div>
            <div class="span-4 append-4 last"> 
                <input type="radio" name="chkBillTo" id="chkBillTo" title='El campo de "Factura a" es obligatorio.' value="DEALER" {if $data.bill_to eq "DEALER"} checked="checked"{/if} /> Comercializador <br />
                <input type="radio" name="chkBillTo" id="chkBillTo" value="CUSTOMER" {if $data.bill_to eq "CUSTOMER"} checked="checked"{/if}/> Cliente
            </div>    
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* D&iacute;a de Pago:</div>
            <div class="span-5">
                <select name="selPayment" id="selPayment" title='El campo "D&iacute;a de Pago" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$deadlineList item="l"}
                        <option value="{$l}" {if $l eq $data.payment_dedline} selected="selected"{/if}>{$global_weekDays.$l}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* D&iacute;as de Cr&eacute;dito:</div>
            <div class="span-4 append-4 last" id="creditDContainer">  
                <select name="selCreditD" id="selCreditD" title='El campo "D&iacute;a de Cr&eacute;dito" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$creditDList item="l"}
                        <option value="{$l.serial}" {if $l.serial eq $data.serial_cdd} selected="selected"{/if}>{$l.days_cdd}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Porcentaje:</div>
            <div class="span-5"> <input type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje" es obligatorio.' value="{$data.percentage_dea}"/> </div>

            <div class="span-3 label">* Tipo:</div>
            <div class="span-4 append-4 last">
                <select name="selType" id="selType" title='El campo de "Tipo" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$typeList item="l"}
                        <option value="{$l}" {if $l eq $data.type_dea} selected="selected"{/if}>{$global_pType.$l}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="span-24 last line" id="hideSection">
            <div class="prepend-4 span-4 label">* Incentivo a:</div>
            <div class="span-5">
                <input type="radio" name="chkBonusTo" id="chkBonusTo" title='El campo de "Incentivo" es obligatorio.' value="DEALER" {if $data.bonus_to_dea eq "DEALER"}checked{/if} /> Comercializador <br />
                <input type="radio" name="chkBonusTo" id="chkBonusTo" value="COUNTER"  {if $data.bonus_to_dea eq "COUNTER"}checked{/if}/> Counter
            </div>
        </div>

        <div class="span-24 last line separator">
            <label>Informaci&oacute;n de CASH Management:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Banco:</div>
            <div class="span-5">
            {* <option value="">- Seleccione -</option> *}
            <select name="selBank" id="selBank">
                    <option value="">- Seleccione -</option>
                    {foreach from=$banks item="b"}
                        <option value="{$b}"{if $b eq $data.bank}selected="selected"{/if}>{$b}</option>
                    {/foreach}
                    </select>
                {* {if $data.bank}{$data.bank}{else}N/A{/if}
                <input type="text" name="selBank" id="selBank" value="{$data.bank}" /> *}
            {* </div> *}
            </div>
            <div class="span-3 label">Tipo de Cuenta:</div>
            <div class="span-4 append-4 last">
            <select name="selAccountType" id="selAccountType">
            <option value="">- Seleccione -</option>
            {foreach from=$account_types item="ac"}
                <option value="{$ac}" {if $ac eq $data.account_type}selected="selected"{/if}>{$global_account_types[$ac]}</option>
            {/foreach}
        </select>
{* 
                {if $data.account_type}{$global_account_types[$data.account_type]}{else}N/A{/if}
                <input type="text" name="selAccountType" id="selAccountType" value="{$data.account_type}" /> *}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">N&uacute;mero de Cuenta:</div>
            <div class="span-5">
                {* {if $data.account_number}{$data.account_number}{else}N/A{/if} *}
                <input type="text" name="txtAccountNumber" id="txtAccountNumber" value="{$data.account_number}" />
            </div>
        </div>

        <div class="span-24 last line separator">
            <label>Informaci&oacute;n de Pre liquidación:</label>
        </div>
        
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Recibir Notificaciones?:</div>
            <div class="span-5">
                Si <input type="radio" name="rdNotifications" id="rdNotifications" value="YES" {if $data.notifications eq "YES"} checked="checked"{/if} /> 
                No <input type="radio" name="rdNotifications" id="rdNotifications" value="NO" {if $data.notifications eq "NO"} checked="checked"{/if} /> 
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre:</div>
            <div class="span-5">
                <input type="text" name="txtAssistName" id="txtAssistName" title='El campo de "Nombre (Asistencias)" es obligatorio.' value="{$data.assistance_contact|htmlall}"/>
            </div>

            <div class="span-3 label">* E-mail:</div>
            <div class="span-4 append-4 last"> <input type="text" name="txtAssistEmail" id="txtAssistEmail" title='El campo de "E-mail (Asistencias)" es obligatorio.' value="{$data.email_assistance}"/> </div>
        </div>

        <div class="span-24 last buttons line">
            <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Actualizar" >
        </div>
        <input type="hidden" name="serial_dea" id="serial_dea" value="{$data.serial_dea}" />
        <input type="hidden" name="hdnSerialMbc" id="hdnSerialMbc" value="{$data.serial_mbc}"/>
        <input type="hidden" name="hdnSerialUsr" id="hdnSerialUsr" value="{$data.serial_usr}" title='El campo "Responsable" es obligatorio.' />
        <input type="hidden" name="today" id="today" value="{$today}" />
    </form>
{else}
    <div class="prepend-7 span-10 append-7 last">
        <div class="span-10 error" align="center">
            Hubo un error en la carga de datos del Comercializador
        </div>
    </div>
{/if}