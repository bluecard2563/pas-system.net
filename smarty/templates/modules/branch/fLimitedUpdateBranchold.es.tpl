{assign var="title" value="ACTUALIZAR SUCURSAL DE COMERCIALIZADOR"} 
<form name="frmUpdateBranch" id="frmUpdateBranch" method="post" action="{$document_root}modules/branch/pUpdateBranch" class="">
    <div class="span-24 last title">
        Actualizaci&oacute;n de Sucursal de Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
        <div class="append-7 span-10 prepend-7 last ">
            <div class="span-10 error" align="center">
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <!--Dealer Geographic Information-->
    <div class="span-24 last line separator">
        <label>Info. Geogr&aacute;fica del Comercializador:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Zona:</div>
        <div class="span-5">{$data.name_zon}</div>

        <div class="span-3 label">Pa&iacute;s:</div>
        <div class="span-4 append-4 last">{$data.dea_name_cou}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Ciudad:</div>
        <div class="span-5">{$data.dea_name_cit}</div>

        <div class="span-3 label">Sector:</div>
        <div class="span-4 append-4 last">{$data.dea_name_sec}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-5 span-3 label">Comercializador:</div>
        <div class="span-5">{$data.dea_name_dea}</div>

        <div class="span-5 label">Comercializador Oficial:</div>
        <div class="span-3">{if $data.official_dea eq "YES"}Si{else}No{/if}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Creado Por:</div>
        <div class="span-4">{if $data.creator_name}{$data.creator_name}{else}N/A{/if}</div>


        <div class="span-5 label">Fecha de Creaci&oacute;n:</div>
        <div class="span-3">{if $data.created_on}{$data.created_on}{else}N/A{/if}</div>
    </div>
    <!--End of Dealer Geographic Information-->

    <!--Geographic Information-->
    <div class="span-24 last line separator">
        <label>Info. Geogr&aacute;fica de la Sucursal:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Pa&iacute;s:</div>
        <div class="span-5">{$data.name_cou}</div>

        <div class="span-3 label">Ciudad:</div>
        <div class="span-4 append-4 last">{$data.name_cit}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Sector:</div>
        <div class="span-5">  
            {foreach from=$sectorList item="l"}
                {if $data.serial_sec eq $l.serial_sec}
                    {$l.name_sec}
                {/if}
            {/foreach}
            
            <input type="hidden" name="selSector" id="selSector" value="{$data.serial_sec}" />
            </select>
        </div>

        <div class="span-3 label">C&oacute;digo:</div>
        <div class="span-4 append-4 last">{$data.code_cou|upper}-{$data.code_cit|upper}-{$data.dea_code_dea}-{$data.code_dea}</div>
    </div>
    <!--End of Geographic Information-->

    <!--Responsible-->	
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Responsable:</div>
        <div class="span-5" id="sectorContainer">  
            {$data.comissionist_name}
            <input type="hidden" name="selComissionist" id="selComissionist" value="{$data.serial_usr}"/>
        </div>
    </div>
    <!--End of Responsible-->

    <!--General Information-->
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n General:</label>
    </div>

    <div  class="span-24 last line" id="generalInfo">
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Id:</div>
            <div class="span-5">{$data.id_dea}</div>

            <div class="span-3 label">* Nombre:</div>
            <div class="span-4 append-4 last"> 
                {$data.name_dea}
                <input type="hidden" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' value="{$data.name_dea}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tipo de Comercializador:</div>
            <div class="span-5">
                {foreach from=$dTypesList item="dt"}
                    {if $data.serial_dlt eq $dt.serial_dlt}
                        {$dt.name}
                    {/if}
                {/foreach}
                <input type="hidden" name="selDealerT" id="selDealerT" title='El campo "Tipo de Comercializador" es obligatorio.' value="{$data.serial_dlt}"/>
            </div>

            <div class="span-3 label">* Categor&iacute;a:</div>
            <div class="span-4 append-4 last" id="categoryContainer"> 
                {$data.category_dea}
                <input type="hidden" name="selCategory" id="selCategory" title='El campo "Categor&iacute;a" es obligatorio.' value="{$data.category_dea}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Estado:</div>
            <div class="span-5">
                {$global_status[$data.status_dea]}
                <input type="hidden" name="selStatus" id="selStatus" value="{$data.status_dea}" />
            </div>

            <div class="span-3 label">* N&uacute;mero de sucursal de Comercializador:</div>
            <div class="span-4 append-4 last"> {$data.branch_number}</div>         
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono #1:</div>
            <div class="span-5">
                <input type="text" name="txtPhone1" id="txtPhone1" {if $data.phone1_dea}value="{$data.phone1_dea}"{/if} title='El campo "Tel&eacute;fono 1" es obligatorio'>
            </div>

            <div class="span-3 label">* Direcci&oacute;n:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtAddress" id="txtAddress" {if $data.address_dea}value="{$data.address_dea}"{/if} title='El campo "Direcci&oacute;n" es obligatorio'>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha del Contrato:</div>
            <div class="span-5">
                {$data.contract_date}
                <input type="hidden" name="txtContractDate" id="txtContractDate" title='El campo "Fecha del Contrato" es obligatorio' value="{$data.contract_date}">
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhone2" id="txtPhone2" {if $data.phone2_dea}value="{$data.phone2_dea}"{/if}>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail #1:</div>
            <div class="span-5">
                <input type="text" name="txtMail1" id="txtMail1" {if $data.email1_dea}value="{$data.email1_dea}"{/if} title='El campo "E-mail #1" es obligatorio.'>
            </div>

            <div class="span-3 label">Fax:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtFax" id="txtFax" {if $data.fax_dea}value="{$data.fax_dea}"{/if}>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre del Contacto:</div>
            <div class="span-5">
                <input type="text" name="txtContact" id="txtContact" {if $data.contact_dea}value="{$data.contact_dea}"{/if} title='El campo de "Nombre del Contacto" es obligatorio.' />
            </div>

            <div class="span-3 label">E-mail #2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtMail2" id="txtMail2" {if $data.email2_dea}value="{$data.email2_dea}"{/if} />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail del Contacto:</div>
            <div class="span-5">
                <input type="text" name="txtContactMail" id="txtContactMail" {if $data.email_contact}value="{$data.email_contact}"{/if} title='El campo de "E-mail del Contacto" es obligatorio.' />
            </div>

            <div class="span-3 label">* Tel&eacute;fono del Contacto:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtContactPhone" id="txtContactPhone" {if $data.phone_contact}value="{$data.phone_contact}"{/if} title='El campo "Tel&eacute;fono del Contacto" es obligatorio.'/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* N&uacute;mero de visitas al mes:</div>
            <div class="span-5">
                {$data.visits_number}
                <input type="hidden" name="txtVisitAmount" id="txtVisitAmount" {if $data.visits_number}value="{$data.visits_number}"{/if} title='El campo de "N&uacute;mero de visitas al mes" es obligatorio.' />
            </div>
        </div>
    </div>
    <!--End of General Information-->

    <!--Manager Information-->
    <div  class="span-24 last line" id="managerInfo">
        <div class="span-24 last line separator">
            <label>Informaci&oacute;n del Gerente:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre del Gerente:</div>
            <div class="span-5">
                <input type="text" name="txtNameManager" id="txtNameManager" title='El campo de "Nombre del Gerente" es obligatorio.'  value="{$data.manager_name}"/>
            </div>

            <div class="span-3 label">* Tel&eacute;fono del Gerente:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio.' value="{$data.manager_phone}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail del Gerente:</div>
            <div class="span-5">
                <input type="text" name="txtMailManager" id="txtMailManager" title='El campo de "E-mail del Gerente" es obligatorio.' value="{$data.manager_email}"/>
            </div>

            <div class="span-3 label">Fecha de Nacimiento:</div>
            <div class="span-5 append-3 last">
                <input type="text" name="txtBirthdayManager" id="txtBirthdayManager" value="{$data.manager_birthday}"/>
            </div>
        </div>
    </div>
    <!--End of Manager Information-->

    <!--Payment Information-->
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Pagos:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Contacto Pago:</div>
        <div class="span-5">
            <input type="text" name="txtContactPay" id="txtContactPay" {if $data.pay_contact}value="{$data.pay_contact}"{/if} title='El campo de "Contacto Pago" es obligatorio.' />
        </div>   

        <div class="span-3 label">* Factura a nombre del:</div>
        <div class="span-4 append-4 last"> 
			{if $data.bill_to eq "CUSTOMER"}Cliente{else}Comercializador{/if}            
            <input type="hidden" name="chkBillTo" id="chkBillTo" value="{$data.bill_to}" /> 
        </div>    
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* D&iacute;a de Pago:</div>
        <div class="span-5">
            <select name="selPayment" id="selPayment" title='El campo "D&iacute;a de Pago" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$deadlineList item="l"}
                    <option value="{$l}" {if $l eq $data.payment_dedline}selected{/if}>{$global_weekDays.$l}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* D&iacute;as de Cr&eacute;dito:</div>
        <div class="span-4 append-4 last" id="creditDContainer">  
            {foreach from=$creditDList item="l"}
                {if $l.serial eq $data.serial_cdd} 
                    {$l.days_cdd}
                {/if}
            {/foreach}			
            <input type="hidden" name="selCreditD" value="{$data.serial_cdd}" />           
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Porcentaje:</div>
        <div class="span-5"> 
            {$data.percentage_dea} 
            <input type="hidden" name="txtPercentage" id="txtPercentage" value="{$data.percentage_dea}" />
        </div>

        <div class="span-3 label">*Tipo:</div>
        <div class="span-4 append-4 last">
            {$global_pType[$data.type_dea]}
            <input type="hidden" name="selType" value="{$data.type_dea}" />
        </div>
    </div>

    <div class="span-24 last line" id="hideSection">
        <div class="prepend-4 span-4 label">* Incentivo a:</div>
        <div class="span-5">
            {$global_bonusTo[$data.bonus_to_dea]}
            <input type="hidden" name="chkBonusTo" value="{$data.bonus_to_dea}" />
        </div>
    </div>
    <!--End of Payment Information-->

    <!--Asistance Information-->
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Asistencias:</label>
    </div>    

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            {$data.assistance_contact}
            <input type="hidden" name="txtAssistName" id="txtAssistName" {if $data.assistance_contact}value="{$data.assistance_contact}"{/if} title='El campo de "Nombre (Asistencias)" es obligatorio.' />
        </div>

        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> 
            {$data.email_assistance}
            <input type="hidden" name="txtAssistEmail" id="txtAssistEmail" {if $data.email_assistance}value="{$data.email_assistance}"{/if} title='El campo de "E-mail (Asistencias)" es obligatorio.' /> 
        </div>
    </div>
    <!--end ofAsistance Information-->

    <div class="span-24 last buttons line">
        <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Actualizar" >
        <input type="hidden" name="hdnDeaSerialDealer" id="hdnDeaSerialDealer" {if $data.serial_dea}value="{$data.dea_serial_dea}"{/if} />
        <input type="hidden" name="hdnSerialDealer" id="hdnSerialDealer" {if $data.serial_dea}value="{$data.serial_dea}"{/if} />
        <input type="hidden" name="hdnSerialUsr" id="hdnSerialUsr" value=""  />
        <input type="hidden" name="today" id="today" value="{$today}" />
        <input type="hidden" name="hdnManagerRights" id="hdnManagerRights" value="{if $data.manager_rights}{$data.manager_rights}{/if}" />
        <input type="hidden" name="hdnParentComission" id="hdnParentComission" value="{if $data.parent_comission}{$data.parent_comission}{/if}" />
        <input type="hidden" name="limited_update" id="limited_update" value="YES" />
    </div>
</form>