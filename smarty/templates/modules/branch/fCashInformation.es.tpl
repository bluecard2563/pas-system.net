{assign var="title" value="INFORMACI&Oacute;N CASH MANAGEMENT"}
<div class="span-24 last title">Informaci&oacute;n de CASH Management<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	 <div class="append-7 span-10 prepend-7 last ">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Actualizaci&oacute;n exitosa!
			{elseif $error eq 2}
				La actualizaci&oacute;n termin&oacute; correctamente, pero no se envi&oacute; el correo de 
				confirmaci&oacute;n.
			{elseif $error eq 3}
				La actualizaci&oacute;n no se pudo completar. Por favor contacte al adminsitrador.
			{elseif $error eq 4}
				No se pudo iniciar el proceso de actualizaci&oacute;n. Los datos no concuerdan.
			{/if}
		</div>
	</div>
{/if}

{if $branch}
<form name="frmCashEdit" id="frmCashEdit" action="{$document_root}modules/branch/pCashInformation" method="post">
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de la Sucursal:</label>
    </div>
			
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Raz&oacute;n Social:</div>
        <div class="span-5">
			{$branch.name_dea}
        </div>

        <div class="span-3 label">RUC:</div>
        <div class="span-4 append-4 last">
            {$branch.id_dea}
        </div>
    </div>
	
	<div class="span-24 last line separator">
        <label>Informaci&oacute;n de CASH Management:</label>
    </div>
			
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Banco:</div>
        <div class="span-5">
			<select name="selBank" id="selBank">
                <option value="">- Seleccione -</option>
                {foreach from=$banks item="b"}
                    <option value="{$b}" {if $b eq $branch.bank_dea}selected{/if}>{$b}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">Tipo de Cuenta:</div>
        <div class="span-4 append-4 last">
            <select name="selAccountType" id="selAccountType">
                <option value="">- Seleccione -</option>
                {foreach from=$account_types item="ac"}
                    <option value="{$ac}" {if $ac eq $branch.account_type_dea}selected{/if}>{$global_account_types[$ac]}</option>
                {/foreach}
            </select>
        </div>
    </div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">N&uacute;mero de Cuenta:</div>
        <div class="span-5">
			<input type="text" name="txtAccountNumber" id="txtAccountNumber" value="{$branch.account_number_dea}"/>
        </div>
    </div>
			
	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Guardar" />
		<input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$branch.serial_dea}" />
	</div>

</form>
{else}
	<div class="prepend-7 span-10 append-7 last">
		<div class="span-10 error center">
			Usted no tiene permisos para realizar esta operaci&oacute;n.<br />
			Comun&iacute;quese con el adminsitrador.
		</div>
	</div>
{/if}