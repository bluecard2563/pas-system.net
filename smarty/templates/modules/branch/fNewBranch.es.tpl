{assign var="title" value="NUEVA SUCURSAL DE COMERCIALIZADOR"} 
<form name="frmNewBranch" id="frmNewBranch" method="post" action="{$document_root}modules/branch/pNewBranch" class="">
	<div class="span-24 last title">
    	Registro de Nueva Sucursal de Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                La sucursal de comercializador se ha registrado exitosamente!<br/>
                El c&oacute;digo es {$recentlyCreatedBranchCode}
            {elseif $error eq 2}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {elseif $error eq 3}
                La sucursal de comercializador se ha registrado exitosamente, pero hubo errores en el ingreso del responsable.
            {elseif $error eq 4}
            La sucursal de comercializador se ha registrado exitosamente, pero hubo errores enviando el e-mail de notificaci&oacute;n.
			{elseif $error eq 5}
                El formato de la imagen no es correcto.
            {elseif $error eq 6}
				El tama�o de la imagen supera el m�ximo permitido.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <!--Geographic Information-->
    <div class="span-24 last line separator">
    	<label>Info. Geogr&aacute;fica del Comercializador:</label>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
        	{if $isMainManagerUser}
                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach name="countries" from=$countryList item="l"}
                        <option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select>
            {else}
                {$nameCountry}
				<input type="hidden" name="selCountry" id="selCountry" value="{$idCountry}" />
            {/if}
        </div>
        
        <div class="span-3 label">* Representante:</div>
        <div class="span-4 append-4 last" id="managerDealerContainer">
			{if $isMainManagerUser}
                <select name="selManagerDealer" id="selManagerDealer" title='El campo "Representante" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            {else}
                {$nameManager|upper}
				<input type="hidden" name="selManagerDealer" id="selManagerDealer" value="{$serialMbc}" />
            {/if}
        </div>
	</div>

    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Ciudad:</div>
        <div class="span-5" id="cityDealerContainer">
			{if $isMainManagerUser}
                <select name="selCityDealer" id="selCityDealer" title='El campo "Ciudad" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
            {else}
                <select name="selCityDealer" id="selCityDealer" title='El campo "Ciudad" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$cityList item="c"}
					<option value="{$c.serial_cit}">{$c.name_cit}</option>
					{/foreach}
				</select>
            {/if}
        </div>

        <div class="span-3 label">* Sector:</div>
        <div class="span-4 append-4 last" id="sectorDealerContainer">
        	<select name="selSectorDealer" id="selSectorDealer" title='El campo "Sector" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>        
        
	</div>

	<div class="span-24 last line">

		<div class="prepend-4 span-4 label">* Comercializador:</div>
        <div class="span-5 last" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
       </div>

	</div>
    
    <div class="span-24 last line separator">
    	<label>Info. Geogr&aacute;fica de la Sucursal:</label>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Ciudad:</div>
        <div class="span-5" id="cityContainer">
			{if $isMainManagerUser}
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
            {else}
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$cityList item="c"}
					<option value="{$c.serial_cit}">{$c.name_cit}</option>
					{/foreach}
				</select>
            {/if}
        </div>
        
        <div class="span-3 label">* Sector:</div>
        <div class="span-4 append-4 last" id="sectorContainer">  
        	<select name="selSector" id="selSector" title='El campo "Sector" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
    <!--End of Geographic Information-->

	<!--Responsible-->
	<div class="span-24 last line separator">
        <label>Asignaci&oacute;n de responsable:</label>
    </div>
	
	<div class="span-24 last line" id="comissionistContainer">
        {if $serialMbc}
        <div class="span-4 label">* Responsable:</div>
        <div class="span-5">
			{if $comissionistList}
			<select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio.'>
				<option value="">- Seleccione -</option>
					{foreach from=$comissionistList item="l"}
						<option value="{$l.serial_usr}" >{$l.first_name_usr} {$l.last_name_usr}</option>
					{/foreach}
			</select>
			{else}
			No existen usuarios registrados.
			{/if}
        </div>
        {else}
            <div class="span-24 last line center" id="comissionistContainer">
				Seleccione un representante.
			</div>
        {/if}
    </div>
    <!--End of Responsible-->
    
    <!--General Information-->
    <div class="span-24 last line separator">
    	<label>Informaci&oacute;n General:</label>
    </div>
    
    <div  class="span-24 last line center" id="generalInfo">
    Seleccione un comercializador.
    </div>
    <!--End of General Information-->
    
	<!--Manager Information-->
	<div  class="span-24 last line" id="managerInfo">
		<div class="span-24 last line separator">
			<label>Informaci&oacute;n del Gerente:</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Nombre del Gerente:</div>
			<div class="span-5">
				<input type="text" name="txtNameManager" id="txtNameManager" title='El campo de "Nombre del Gerente" es obligatorio.' />
			</div>

			<div class="span-3 label">* Tel&eacute;fono del Gerente:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio.'/>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* E-mail del Gerente:</div>
			<div class="span-5">
				<input type="text" name="txtMailManager" id="txtMailManager" title='El campo de "E-mail del Gerente" es obligatorio.' />
			</div>

			<div class="span-3 label">Fecha de Nacimiento:</div>
			<div class="span-5 append-3 last">
				<input type="text" name="txtBirthdayManager" id="txtBirthdayManager"/>
			</div>
		</div>
	</div>
	<!--End of Manager Information-->

    <!--Payment Information-->
    <div class="span-24 last line separator">
    	<label>Informaci&oacute;n de Pagos:</label>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Contacto Pago:</div>
        <div class="span-5">
        	<input type="text" name="txtContactPay" id="txtContactPay" title='El campo de "Contacto Pago" es obligatorio.' />
        </div>   
        
        <div class="span-3 label">* Factura a nombre del:</div>
        <div class="span-4 append-4 last"> 
        	<input type="radio" name="chkBillTo" id="chkBillTo" title='El campo de "Factura" es obligatorio.' value="DEALER" checked /> Comercializador <br />
            <input type="radio" name="chkBillTo" id="chkBillTo" value="CUSTOMER" /> Cliente
        </div>    
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* D&iacute;a de Pago:</div>
        <div class="span-5">
        	<select name="selPayment" id="selPayment" title='El campo "D&iacute;a de Pago" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$deadlineList item="l"}
                    <option value="{$l}">{$global_weekDays.$l}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* D&iacute;as de Cr&eacute;dito:</div>
        <div class="span-4 append-4 last" id="creditDContainer">  
        	<select name="selCreditD" id="selCreditD" title='El campo "D&iacute;a de Cr&eacute;dito" es obligatorio.'>
            	<option value="">- Seleccione -</option>
				{if $dataByCountry}
				{foreach from=$dataByCountry item="l"}
                    <option value="{$l.serial}" >{$l.days_cdd}</option>
                {/foreach}
				{/if}
            </select>
        </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Porcentaje:</div>
        <div class="span-5"> <input type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje" es obligatorio.'/> </div>
        
        <div class="span-3 label">*Tipo:</div>
        <div class="span-4 append-4 last">
            <select name="selType" id="selType" title='El campo de "Tipo" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item="l"}
                    <option value="{$l}">{$global_pType.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="hideSection">
        <div class="prepend-4 span-4 label">* Incentivo a:</div>
        <div class="span-5">
            <input type="radio" name="chkBonusTo" id="chkBonusTo" title='El campo de "Incentivo" es obligatorio.' value="DEALER" checked /> Comercializador <br />
            <input type="radio" name="chkBonusTo" id="chkBonusTo" value="COUNTER" /> Counter
        </div>
    </div>
    <!--End of Payment Information-->
	
	<div class="span-24 last line separator">
        <label>Informaci&oacute;n de CASH Management:</label>
    </div>
			
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Banco:</div>
        <div class="span-5">
			<select name="selBank" id="selBank">
                <option value="">- Seleccione -</option>
                {foreach from=$banks item="b"}
                    <option value="{$b|trim}">{$b}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">Tipo de Cuenta:</div>
        <div class="span-4 append-4 last">
            <select name="selAccountType" id="selAccountType">
                <option value="">- Seleccione -</option>
                {foreach from=$account_types item="ac"}
                    <option value="{$ac}">{$global_account_types[$ac]}</option>
                {/foreach}
            </select>
        </div>
    </div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">N&uacute;mero de Cuenta:</div>
        <div class="span-5">
			<input type="text" name="txtAccountNumber" id="txtAccountNumber" />
        </div>
    </div>
    
    <!--Asistance Information-->
    <div class="span-24 last line separator">
    	<label>Informaci&oacute;n de Asistencias:</label>
    </div>   
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Recibir Notificaciones?:</div>
        <div class="span-5">
            Si <input type="radio" name="rdNotifications" id="rdNotifications" value="YES" /> 
            No <input type="radio" name="rdNotifications" id="rdNotifications" value="NO" checked /> 
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
        	<input type="text" name="txtAssistName" id="txtAssistName" title='El campo de "Nombre (Asistencias)" es obligatorio.' />
        </div>
        
        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtAssistEmail" id="txtAssistEmail" title='El campo de "E-mail (Asistencias)" es obligatorio.' /> </div>
	</div>
    <!--end ofAsistance Information-->
    
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegister" id="btnRegister" value="Registrar" />
    </div>
    <input type="hidden" name="hdnFlagCode" id="hdnFlagCode" value="1" />
	<input type="hidden" name="hdnSerialUsr" id="hdnSerialUsr" value=""  />
	<input type="hidden" name="today" id="today" value="{$today}" />
</form>