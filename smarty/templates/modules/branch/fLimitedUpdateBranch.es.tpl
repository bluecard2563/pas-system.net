﻿{assign var="title" value="ACTUALIZAR SUCURSAL DE COMERCIALIZADOR"} 
<form name="frmUpdateBranch" id="frmUpdateBranch" method="post" action="{$document_root}modules/branch/pUpdateBranch" class="" enctype="multipart/form-data">
    <div class="span-24 last title">
        Actualizaci&oacute;n de Sucursal de Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
        <div class="append-7 span-10 prepend-7 last ">
            <div class="span-10 error" align="center">
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <!--Dealer Geographic Information-->
    <div class="span-24 last line separator">
        <label>Info. Geogr&aacute;fica del Comercializador:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Zona:</div>
        <div class="span-5">{$data.name_zon}</div>

        <div class="span-3 label">Pa&iacute;s:</div>
        <div class="span-4 append-4 last">{$data.dea_name_cou}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Ciudad:</div>
        <div class="span-5">{$data.dea_name_cit}</div>

        <div class="span-3 label">Sector:</div>
        <div class="span-4 append-4 last">{$data.dea_name_sec}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-5 span-3 label">Comercializador:</div>
        <div class="span-5">{$data.dea_name_dea|htmlall}</div>

        <div class="span-5 label">Comercializador Oficial:</div>
        <div class="span-3">{if $data.official_dea eq "YES"}Si{else}No{/if}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Creado Por:</div>
        <div class="span-4">{if $data.creator_name}{$data.creator_name}{else}N/A{/if}</div>


        <div class="span-5 label">Fecha de Creaci&oacute;n:</div>
        <div class="span-3">{if $data.created_on}{$data.created_on}{else}N/A{/if}</div>
    </div>
    <!--End of Dealer Geographic Information-->

    <!--Geographic Information-->
    <div class="span-24 last line separator">
        <label>Info. Geogr&aacute;fica de la Sucursal:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Pa&iacute;s:</div>
        <div class="span-5">{$data.name_cou}</div>

        <div class="span-3 label">Ciudad:</div>
        <div class="span-4 append-4 last">{$data.name_cit}</div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Sector:</div>
        <div class="span-5" id="sectorContainer">  
            <select name="selSector" id="selSector" title='El campo "Sector" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$sectorList item="l"}
                    <option value="{$l.serial_sec}" {if $data.serial_sec eq $l.serial_sec}selected{/if}>{$l.name_sec}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">C&oacute;digo:</div>
        <div class="span-4 append-4 last">{$data.code_cou|upper}-{$data.code_cit|upper}-{$data.dea_code_dea}-{$data.code_dea}</div>
    </div>
    <!--End of Geographic Information-->

    <!--Responsible-->	
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Responsable:</div>
        <div class="span-5" id="sectorContainer">  
            {$data.comissionist_name}
            <input type="hidden" name="selComissionist" id="selComissionist" value="{$data.serial_usr}"/>
        </div>
    </div>
    <!--End of Responsible-->

    <!--General Information-->
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n General:</label>
    </div>

    <div  class="span-24 last line" id="generalInfo">
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Id:</div>
            <div class="span-5">{$data.id_dea}</div>

            <div class="span-3 label">* Nombre:</div>
            <div class="span-4 append-4 last"> 
                {$data.name_dea|htmlall}
                <input type="hidden" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' value="{$data.name_dea}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tipo de Comercializador:</div>
            <div class="span-5" id="categoryContainer"> 
                <select name="selDealerT" id="selDealerT" title='El campo "Tipo de Comercializador" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$dTypesList item="dt"}
                        <option value="{$dt.serial_dlt}" {if $data.serial_dlt eq $dt.serial_dlt}selected{/if}>{$dt.name|htmlall}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Categor&iacute;a:</div>
            <div class="span-4 append-4 last" id="categoryContainer"> 
                <select name="selCategory" id="selCategory" title='El campo "Categor&iacute;a" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$categoryList item="l"}
                        <option value="{$l}" {if $data.category_dea eq $l}selected{/if}>{$l}</option>
                    {/foreach}
                </select> 
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Estado:</div>
            <div class="span-5">
                <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$statusList item="l"}
                        <option value="{$l}" {if $data.status_dea eq $l}selected{/if}>{$global_status.$l}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* N&uacute;mero de sucursal de Comercializador:</div>
            <div class="span-4 append-4 last"> {$data.branch_number}</div>         
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono #1:</div>
            <div class="span-5">
                <input type="text" name="txtPhone1" id="txtPhone1" {if $data.phone1_dea}value="{$data.phone1_dea}"{/if} title='El campo "Tel&eacute;fono 1" es obligatorio'>
            </div>

            <div class="span-3 label">* Direcci&oacute;n:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtAddress" id="txtAddress" {if $data.address_dea}value="{$data.address_dea|htmlall}"{/if} title='El campo "Direcci&oacute;n" es obligatorio'>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha del Contrato:</div>
            <div class="span-5">
                {$data.contract_date}
                <input type="hidden" name="txtContractDate" id="txtContractDate" title='El campo "Fecha del Contrato" es obligatorio' value="{$data.contract_date}">
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhone2" id="txtPhone2" {if $data.phone2_dea}value="{$data.phone2_dea}"{/if}>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail #1:</div>
            <div class="span-5">
                <input type="text" name="txtMail1" id="txtMail1" {if $data.email1_dea}value="{$data.email1_dea}"{/if} title='El campo "E-mail #1" es obligatorio.'>
            </div>

            <div class="span-3 label">Fax:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtFax" id="txtFax" {if $data.fax_dea}value="{$data.fax_dea}"{/if}>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre contacto Cobranzas:</div>
            <div class="span-5">
                <input type="text" name="txtContact" id="txtContact" {if $data.contact_dea}value="{$data.contact_dea|htmlall}"{/if} title='El campo de "Nombre del Contacto" es obligatorio.' />
            </div>

            <div class="span-3 label">* Email de Facturación:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtMail2" id="txtMail2" {if $data.email2_dea}value="{$data.email2_dea}"{/if} title='El campo "E-mail F.E." es obligatorio.' />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Email de Cobranzas:</div>
            <div class="span-5">
                <input type="text" name="txtContactMail" id="txtContactMail" {if $data.email_contact}value="{$data.email_contact}"{/if} title='El campo de "E-mail del Contacto" es obligatorio.' />
            </div>

            <div class="span-3 label">* Tel&eacute;fono del contacto:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtContactPhone" id="txtContactPhone" {if $data.phone_contact}value="{$data.phone_contact}"{/if} title='El campo "Tel&eacute;fono del Contacto" es obligatorio.'/>
            </div>
        </div>
			
<div class="span-24 last line">
			{if $data.logo_dea!=NULL}
			<div class="prepend-4 span-4 label">Logo Actual:</div>
			<div class="span-6 last">
				<img src="{$document_root}img/dea_logos/{$data.logo_dea}" width="100"/>
			</div>
			{/if}
			
			<div class="span-6 label">Logo:</div>
			<div class="span-4 append-4 last">
				<input type="file" name="txtLogo_dea" id="txtLogo_dea" title="Formato: JPG  Tamaño m&aacute;x: 2MB" />
			</div>
		</div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* N&uacute;mero de visitas al mes:</div>
            <div class="span-5">
                {$data.visits_number}
                <input type="hidden" name="txtVisitAmount" id="txtVisitAmount" {if $data.visits_number}value="{$data.visits_number}"{/if} title='El campo de "N&uacute;mero de visitas al mes" es obligatorio.' />
            </div>
        </div>
			
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">M&iacute;nimo de Stock (%):</div>
			<div class="span-5 last">
				{$data.minimumKits}%
				<input type="hidden" name="txtMinimumKits" id="txtMinimumKits" title='El campo "M&iacute;nimo de Stock" es obligatorio.' value="{$data.minimumKits}" />
			</div>
			
			
		</div>
			
			
    </div>
    <!--End of General Information-->

    <!--Manager Information-->
    <div  class="span-24 last line" id="managerInfo">
        <div class="span-24 last line separator">
            <label>Informaci&oacute;n del Gerente:</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre del Gerente:</div>
            <div class="span-5">
                <input type="text" name="txtNameManager" id="txtNameManager" title='El campo de "Nombre del Gerente" es obligatorio.'  value="{$data.manager_name|htmlall}"/>
            </div>

            <div class="span-3 label">* Tel&eacute;fono del Gerente:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio.' value="{$data.manager_phone}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* E-mail del Gerente:</div>
            <div class="span-5">
                <input type="text" name="txtMailManager" id="txtMailManager" title='El campo de "E-mail del Gerente" es obligatorio.' value="{$data.manager_email}"/>
            </div>

            <div class="span-3 label">Fecha de Nacimiento:</div>
            <div class="span-5 append-3 last">
                <input type="text" name="txtBirthdayManager" id="txtBirthdayManager" value="{$data.manager_birthday}"/>
            </div>
        </div>
    </div>
    <!--End of Manager Information-->

    <!--Payment Information-->
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Pagos:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Contacto Pago:</div>
        <div class="span-5">
            <input type="text" name="txtContactPay" id="txtContactPay" {if $data.pay_contact}value="{$data.pay_contact|htmlall}"{/if} title='El campo de "Contacto Pago" es obligatorio.' />
        </div>   

        <div class="span-3 label">* Factura a nombre del:</div>
          <div class="span-4 append-4 last"> 
            <input type="radio" name="chkBillTo" id="chkBillTo" title='El campo de "Factura" es obligatorio.' value="DEALER" {if $data.bill_to eq "DEALER"} checked{/if} /> Comercializador <br />
            <input type="radio" name="chkBillTo" id="chkBillTo" value="CUSTOMER" {if $data.bill_to eq "CUSTOMER"} checked{/if} /> Cliente
        </div>    
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* D&iacute;a de Pago:</div>
        <div class="span-5">
            <select name="selPayment" id="selPayment" title='El campo "D&iacute;a de Pago" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$deadlineList item="l"}
                    <option value="{$l}" {if $l eq $data.payment_dedline}selected{/if}>{$global_weekDays.$l}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* D&iacute;as de Cr&eacute;dito:</div>
        <div class="span-4 append-4 last" id="creditDContainer">  
            <select name="selCreditD" id="selCreditD" title='El campo "D&iacute;a de Cr&eacute;dito" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$creditDList item="l"}
                    <option value="{$l.serial}" {if $l.serial eq $data.serial_cdd} selected="selected"{/if}>{$l.days_cdd}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Porcentaje:</div>
        <div class="span-5"> 
            {$data.percentage_dea} 
            <input type="hidden" name="txtPercentage" id="txtPercentage" value="{$data.percentage_dea}" />
        </div>

        <div class="span-3 label">*Tipo:</div>
        <div class="span-4 append-4 last">
            <select name="selType" id="selType" title='El campo de "Tipo" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$typeList item="l"}
                    <option value="{$l}" {if $l eq $data.type_dea} selected="selected"{/if}>{$global_pType.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="hideSection">
        <div class="prepend-4 span-4 label">* Incentivo a:</div>
        <div class="span-5">
            <input type="radio" name="chkBonusTo" id="chkBonusTo" title='El campo de "Incentivo" es obligatorio.' value="DEALER" {if $data.bonus_to_dea eq "DEALER"}checked{/if} /> Comercializador <br />
            <input type="radio" name="chkBonusTo" id="chkBonusTo" value="COUNTER" {if $data.bonus_to_dea eq "COUNTER"}checked{/if} /> Counter
        </div>
    </div>
    <!--End of Payment Information-->

    <!--Asistance Information-->
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Pre liquidación:</label>
    </div>    
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Recibir Notificaciones?:</div>
        <div class="span-5">
            Si <input type="radio" name="rdNotifications" id="rdNotifications" value="YES" {if $data.notifications eq "YES"} checked="checked"{/if} /> 
            No <input type="radio" name="rdNotifications" id="rdNotifications" value="NO" {if $data.notifications eq "NO"} checked="checked"{/if} /> 
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            <input type="text" name="txtAssistName" id="txtAssistName" {if $data.assistance_contact}value="{$data.assistance_contact|htmlall}"{/if} title='El campo de "Nombre (Asistencias)" es obligatorio.' />
        </div>

        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtAssistEmail" id="txtAssistEmail" {if $data.email_assistance}value="{$data.email_assistance}"{/if} title='El campo de "E-mail (Asistencias)" es obligatorio.' /> </div>
    </div>
    <!--end ofAsistance Information-->

    <div class="span-24 last buttons line">
        <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Actualizar" >
        <input type="hidden" name="hdnDeaSerialDealer" id="hdnDeaSerialDealer" {if $data.serial_dea}value="{$data.dea_serial_dea}"{/if} />
        <input type="hidden" name="hdnSerialDealer" id="hdnSerialDealer" {if $data.serial_dea}value="{$data.serial_dea}"{/if} />
        <input type="hidden" name="hdnSerialUsr" id="hdnSerialUsr" value=""  />
        <input type="hidden" name="today" id="today" value="{$today}" />
        <input type="hidden" name="hdnManagerRights" id="hdnManagerRights" value="{if $data.manager_rights}{$data.manager_rights}{/if}" />
        <input type="hidden" name="hdnParentComission" id="hdnParentComission" value="{if $data.parent_comission}{$data.parent_comission}{/if}" />
        <input type="hidden" name="limited_update" id="limited_update" value="YES" />
    </div>
</form>