{assign var="title" value="CARTA CONTABLE DE LA SUCURSAL"}

<div class="span-24 last title">Carta Contable de la Sucursal<br />
	<label>
		(*) Campos Obligatorios <br /><br />
		Para cambios de raz&oacute;n social, comun&iacute;quese con el Departamento de Sistemas
	</label>
</div>

{if $error}
	 <div class="append-7 span-10 prepend-7 last ">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Actualizaci&oacute;n exitosa!
			{elseif $error eq 2}
				Ocurrieron errores al actualizar el documento;
			{/if}
		</div>
	</div>
{/if}

<form name="frmEditBranchDocument" id="frmEditBranchDocument" action="{$document_root}modules/branch/pEditBranchDocument" method="post">
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-8 label">* Nombre, RUC o c&oacute;digo de la sucursal:</div>
        <div class="span-10 append-1 last">
            <input type="text" name="txtBranch" id="txtBranch" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
			<input type="hidden" name="hdnBranchID" id="hdnBranchID" value="" />
        </div>
	</div>

	<div class="span-24 last buttons line">
        <input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
    </div>

	<div class="span-24 last line" id="branchInfoContainer"></div>

</form>