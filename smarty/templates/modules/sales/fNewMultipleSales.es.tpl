{assign var="title" value="NUEVA VENTA M&Uacute;LTIPLE DE TARJETAS"}
<div class="span-24 last title">
	 Nueva Venta M&uacute;ltiple de Tarjetas<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $counter_information}
	<div class="span-24 last line separator">
	   <label>Informaci&oacute;n de la Venta</label>
	</div>

	<form name="frmMultipleSales" id="frmMultipleSales" action="{$document_root}modules/sales/pNewMultipleSales" method="post" >
	{if $counter_information|@count eq 1}
		{if $reasign_sale}
			<div class="span-24 last line">
				<div class="prepend-7 span-10 append-7 last line">
					<div class="span-5 label">Reasignar Venta:</div>
					<div class="span-5 last">
						<input type="checkbox" name="chkReasignSale" id="chkReasignSale" />
					 </div>
				</div>
			</div>
			<div class="span-24 last line" id="counterInfoContainer"></div>
			<div class="span-24 last line" id="saleInfoContainer"></div>
		{/if}

		<div class="span-24 last line" id="ownCounterInfo">
			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">Tipo de Venta:</div>
				<div class="span-3">
					Virtual
				</div>
			</div>

			 <div class="span-24 last line">
				<div class="prepend-4 span-4 label">C&oacute;digo Vendedor:</div>
				<div class="span-5">
					{$sale_data.sellerCode}
				</div>

				<div class="span-3 label">Lugar de Emisi&oacute;n:</div>
				<div class="span-4 append-4 last">
					{$sale_data.emissionPlace}
				</div>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">Fecha de Emisi&oacute;n:</div>
				<div class="span-5">
					{$sale_data.emissionDate}
				</div>

				<div class="span-3 label">Asesor de Venta:</div>
				<div class="span-5 append-3 last">
					{$sale_data.counter}
				</div>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">Comercializador:</div>
				<div class="span-5">
					{$counter_information.0.name_dea}
				</div>
			</div>
		</div>		
	{else}
		{if $reasign_sale}
			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">* Seleccione una sucursal de comercializador para realizar la Venta:</div>
                <div class="span-5 last">
                        <select name="selMultipleCounter" id="selMultipleCounter" title='El campo "Seleccione un counter" es obligatorio'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$counter_information key=key item="l"}
                            <option value="{$l.serial_cnt}">{$l.name_dea}</option>
                        {/foreach}
                    </select>
                 </div>

				<div class="span-4 label">Reasignar Venta:</div>
                <div class="span-5 last">
					<input type="checkbox" name="chkReasignSale" id="chkReasignSale" />
                 </div>
			</div>
		{else}
			<div class="prepend-7 span-10 append-7 last line">
				<div class="span-5 label">* Seleccione una sucursal de comercializador para realizar la Venta:</div>
                <div class="span-5">
                        <select name="selMultipleCounter" id="selMultipleCounter" title='El campo "Seleccione un counter" es obligatorio'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$counter_information key=key item="l"}
                            <option value="{$l.serial_cnt}">{$l.name_dea}</option>
                        {/foreach}
                    </select>
                 </div>
			</div>
		{/if}
		<div class="span-24 last line" id="counterInfoContainer"></div>
		<div class="span-24 last line" id="saleInfoContainer"></div>
	{/if}

	<div class="span-24 last line separator">
	   <label>Datos del Producto</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-10 append-7 last line">
			<div class="span-4 label">* Producto:</div>
			<div class="span-5 last" id="productContainer">
				<select name="selProduct" id="selProduct" title="El campo 'Producto' es Obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from=$product_list item="p"}
						<option id="product_{$p.serial_pbd}" value="{$p.serial_pbd}" has_services="{$p.has_services_pro}">{$p.name_pbl}</option>
					{/foreach}
				</select>
			 </div>
		</div>
	</div>
	
	<div class="span-24 last line" id="servicesContainer"></div>

	<div class="span-24 last line separator">
	   <label>Datos de los Clientes</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-10 append-7 last line" id="chooseFileDiv" style="display: none;">
			<div class="span-4 label">* Ingrese un Archivo:</div>
			<div class="span-5 last">
				<input type="file" name="flMassiveList" id="flMassiveList" title="El campo 'Archivo' es obligatorio" />
			 </div>
		</div>
		<div class="prepend-10 span-10 append-4 last line" id="messageFileDiv">
			Primero Seleccione un producto.
		</div>
		<div class="span-24 last line" id="cards_container"></div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-10 span-10 append-4 last line">
			<a href="{$document_root}pdfs/formato.xls" target="_blank">Ver Formato de Lista de Clientes</a>
		</div>
	</div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnConfirm" id="btnConfirm" value="Confirmar Venta" />
		<input type="hidden" name="hdnLoadedClients" id="hdnLoadedClients" value="1" title="Debe cargar al menos un cliente." />
		<input type="hidden" name="hdnCurrent_counter_id" id="hdnCurrent_counter_id" {if $counter_information|@count eq 1}value="{$counter_information.0.serial_cnt}"{/if} />
		<input type="hidden" name="hdnCounter_id_toUse" id="hdnCounter_id_toUse" {if $counter_information|@count eq 1}value="{$counter_information.0.serial_cnt}"{/if} />
	</div>
	</form>
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 center error">
		Usted no est&aacute; asignado como counter. Comun&iacute;quese con el adminsitrador.
	</div>
</div>
{/if}