{assign var="title" value="MODIFICAR REGISTRO VIAJES"}
<div class="span-24 last title">
	Modificaci&oacute;n de Registro de Viajes<br/>
</div>
<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>
<form name="frmModifyRegisterTravel" id="frmModifyRegisterTravel" method="post" action="{$document_root}modules/sales/travelModification/pUpdateTravel">
	{if $customerData}
		<div class="span-24 last" id="customerContainer">
			<div class="span-24 last line separator">
				<label>Datos del Cliente</label>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
				<div class="span-5">
					<input type="text" name="txtDocumentCustomer" id="txtDocumentCustomer" value="{$customerData.document_cus}"  title='El campo "Identificaci&oacute;n" es obligatorio'/>
				</div>
				<div class="span-3 label">*Tipo de Cliente</div>
				<div class="span-4 append-2 last">
					<select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
						<option value="">- Seleccione -</option>
						{foreach from=$typesList item="l"}
							<option value="{$l}" {if $l eq $customerData.type_cus}selected="selected"{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
						{/foreach}
					</select>
				</div>
			</div>

			<div class="span-24 last line" id="divName">
				<div class="prepend-4 span-4 label">* Nombre:</div>
				<div class="span-5">
					<input type="text" name="txtNameCustomer" id="txtNameCustomer" value="{$customerData.first_name_cus}" title='El campo "Nombre" es obligatorio' />
				</div>

				<div class="span-3 label">* Apellido:</div>
				<div class="span-4 append-4 last">
					<input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" value="{$customerData.last_name_cus}" title='El campo "Apellido" es obligatorio' />
				</div>
			</div>

			<div class="span-24 last line" id="divName2" style="display: none">
				<div class="prepend-4 span-4 label">* Raz&oacute;n Social:</div>
				<div class="span-5">
					<input type="text" name="txtNameCustomer1" id="txtNameCustomer1" value="{$customerData.first_name_cus}" title='El campo "Nombre" es obligatorio' />
				</div>

			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
				<div class="span-5">
					<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
						<option value="">- Seleccione -</option>
						{foreach from=$countryList item="l"}
							<option id="{$l.serial_cou}" value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.auxData.serial_cou}selected="selected"{/if}>{$l.name_cou}</option>
						{/foreach}
					</select>
				</div>

				<div class="span-3 label">* Ciudad</div>
				<div class="span-4 append-4 last" id="cityContainerCustomer">
					<select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
						<option value="">- Seleccione -</option>
						{foreach from=$cityListCustomer item="cl"}
							<option id="{$cl.serial_cit}" value="{$cl.serial_cit}" {if $cl.serial_cit eq $customerData.serial_cit}selected="selected"{/if}>{$cl.name_cit}</option>
						{/foreach}
					</select>
				</div>
			</div>

			<div class="span-24 last line" id="divAddress">
				<div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
				<div class="span-5">
					<input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" value="{$customerData.birthdate_cus}" title='El campo "Fecha de Nacimiento" es obligatorio'/>
				</div>

				<div class="span-3 label">* Direcci&oacute;n:</div>
				<div class="span-4 append-4 last">
					<input type="text" name="txtAddress" id="txtAddress" value="{$customerData.address_cus}" title='El campo "Direcci&oacute;n" es obligatorio' />
				</div>
			</div>

			<div class="span-24 last line" id="divAddress2" style="display: none">
				<div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
				<div class="span-5">
					<input type="text" name="txtAddress1" id="txtAddress1"  value="{$customerData.address_cus}" title='El campo "Direcci&oacute;n" es obligatorio' />
				</div>

			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
				<div class="span-5">
					<input type="text" name="txtPhone1Customer" id="txtPhone1Customer" value="{$customerData.phone1_cus}" title='El campo "Tel&eacute;fono 1" es obligatorio'/>
				</div>

				<div class="span-3 label">Tel&eacute;fono 2:</div>
				<div class="span-4 append-4 last">
					<input type="text" name="txtPhone2Customer" value="{$customerData.phone2_cus}" id="txtPhone2Customer" />
				</div>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">Celular:</div>
				<div class="span-5">
					<input type="text" name="txtCellphoneCustomer" value="{$customerData.cellphone_cus}" id="txtCellphoneCustomer" />
				</div>

				<div class="span-3 label">* Email:</div>
				<div class="span-4 append-4 last">
					<input type="text" name="txtMailCustomer" id="txtMailCustomer" value="{$customerData.email_cus}" title='El campo "Email" es obligatorio' />
				</div>
			</div>

			<div class="span-24  last line" id="divRelative">
				<div class="prepend-4 span-4 label">*Familiar:</div>
				<div class="span-5">
					<input type="text" name="txtRelative" id="txtRelative" value="{$customerData.relative_cus}" title='El campo "Familiar" es obligatorio'/>
				</div>

				<div class="span-3 label">*Tel&eacute;fono del familiar:</div>
				<div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" value="{$customerData.relative_phone_cus}" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del familiar" es obligatorio'/> </div>
			</div>

			<div class="span-24  last line" id="divManager" style="display: none">
				<div class="prepend-4 span-4 label">*Gerente:</div>
				<div class="span-5">
					<input type="text" name="txtManager" id="txtManager" value="{$customerData.relative_cus}" title='El campo "Gerente" es obligatorio'/>
				</div>

				<div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
				<div class="span-4 append-4 last"> <input type="text" name="txtPhoneManager" value="{$customerData.relative_phone_cus}" id="txtPhoneManager" title='El campo "Tel&eacute;fono del gerente" es obligatorio'/> </div>
			</div>

			<div class="prepend-14 span-5 append-5 last line" id="divOpenDialog">
				<div class="span-5"> <a href="#" name="openDialog" id="openDialog">Configurar Preguntas</a></div>
			</div>
			<div id="hiddenAnswers"></div>
		</div>
	{else}
		<input type="hidden" id="cur_start_date_travel" name="cur_start_date_travel" value="{$travelData.start_trl}"/>
		<input type="hidden" id="cur_end_date_travel" name="cur_end_date_travel" value="{$travelData.end_trl}"/>
		<input type="hidden" id="date_last_travel" name="date_last_travel" value="{$date_last_travel}"/>
		<input type="hidden" id="serial_cus" name="serial_cus" value="{$serial_cus}"/>
	{/if}
	<div class="span-24 last line separator">
		<label>Validez de la Tarjeta</label>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Desde:</div>
        <div class="span-5">
			<label id="start_dt" name="start_dt">{$cardInfo.begin_date_sal}</label>
        </div>

        <div class="span-3 label">Hasta:</div>
        <div class="span-4 append-4 last">
			<label id="end_dt" name="end_dt">{$cardInfo.end_date_sal}</label>
        </div>
    </div>

	<div class="span-24 last line separator">
        <label>Datos del Viaje</label>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">D&iacute;as Contratados: </div>
        <div class="span-5">
            <label>{$cardInfo.days_sal}</label>
        </div>
        <div class="span-3 label">D&iacute;as Disponibles: </div>
        <div class="span-5 append-3 last">
            <label id="days_available">{if $productData.limit_pro eq 'YES'}{$days_available+$travelData.days_trl}</label> Incluye viaje actual.{else}{$days_available}</label>{/if}
			<input type="hidden" id="days_used" value="{$days_available+$travelData.days_trl}"/>
			<input type="hidden" id="days_current" value="{$travelData.days_trl}"/>

        </div>
    </div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Partida:</div>
        <div class="span-5">
            <input type="text" name="txtDepartureDate" id="txtDepartureDate" value="{$travelData.start_trl}" title='El campo "Fecha de Salida" es obligatorio' />
        </div>

        <div class="span-3 label">* Fecha de Retorno:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtArrivalDate" id="txtArrivalDate" value="{$travelData.end_trl}"  title='El campo "Fecha de Retorno" es obligatorio'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer">* Pa&iacute;s de Destino:</div>
        <div class="span-5">
            <select name="selCountryDestination" id="selCountryDestination" title='El campo "Pa&iacute;s de Destino" es obligatorio.' {if $destinationRestricted}disabled{/if}>
				<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}" {if $l.serial_cou eq $travelData.serial_cou}selected="selected"{/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
			<select name="selCityDestination" id="selCityDestination" title='El campo "Ciudad de Destino" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$cityList item="cl"}
                    <option id="{$cl.serial_cit}" value="{$cl.serial_cit}" {if $cl.serial_cit eq $travelData.serial_cit}selected="selected"{/if}>{$cl.name_cit}</option>
                {/foreach}
            </select>
		</div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer"># de D&iacute;as:</div>
		<div class="span-5">
			<label id="txtDaysBetween"></label>
		</div>
		<input type="hidden" name="hddDays" id="hddDays" value=""/>
    </div>

	<div class="span-24 last buttons line">
		<input type="submit" value="Registrar" id="btnRegisterTravel" name="btnRegisterTravel"/>
		<input type="hidden" name="current_trl" id="current_trl" value="{$travelData.serial_trl}" />
		<input type="hidden" name="hdnParameterCondition" id="hdnParameterCondition" value="{$parameterCondition}" />
		<input type="hidden" name="hdnParameterValue" id="hdnParameterValue" value="{$parameterValue}" />
		<input type="hidden" name="hdnQuestionsFlag" id="hdnQuestionsFlag" value="0" />
		{if $productData.third_party_register_pro eq 'YES'}
			<input type="hidden" name="hdnQuestionsExists" id="hdnQuestionsExists" value="{if $questionList}1{else}0{/if}" />
		{/if}
		<input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
		<input type="hidden" name="today" id="today" value="{$today}" />
		<input type="hidden" name="all_answered" id="all_answered"/>
		<input type="hidden" name="should_check_questions" id="should_check_questions"/>
		<input type="hidden" name="allow_senior" id="allow_senior" value="{if $productData.senior_pro eq 'YES'}1{else}0{/if}"/>
		<input type="hidden" name="hdnSerial_pxc" id="hdnSerial_pxc" value="{$serial_pxc}" />
	</div>
</form>
<div id="dialog" title="Preguntas">
    {include file="templates/modules/customer/fCustomerDialog.$language.tpl"}
</div>
