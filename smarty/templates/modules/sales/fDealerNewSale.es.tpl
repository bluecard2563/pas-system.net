{assign var="title" value="NUEVA VENTA"}
<script type="text/javascript" src="{$document_root}js/modules/sales/globalInvoicingFunctions.js"></script>
<script type="text/javascript" src="{$document_root}js/modules/sales/globalSalesFunctions.js"></script>
<form name="frmNewSale" id="frmNewSale" method="post" action="{$document_root}modules/sales/pNewSale" class="">
    <input type="hidden" name="isEdition" id="isEdition" value="0">
    <div class="span-24 last title">
        Nueva Venta<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de la Venta</label>
    </div>


    <div class="span-24 last line" id="saleInfoPACounter">


        {if $show_line eq 'YES'}<div style="" id="line" class="span-24 last line separatorExtra"></div>{/if}
        {if $hasProducts eq 'NO'}
            <div class="prepend-7 span-13 append-5 last line">
                Este comercializador actualmente no tiene asignado una cartera de productos.<br>
                Por favor comuníquese con el administrador.
            </div>
        {else}

            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* No de Tarjeta:</div>
                <div class="span-5">
                    <input type="text" name="txtCardNum" id="txtCardNum" title='El campo "N&uacute;mero de Tarjeta" es obligatorio' value="AUTOMATICO" />
                </div>

                <div class="span-3 label">C&oacute;digo Vendedor:</div>
                <div class="span-4 append-4 last">
                    <input type="text" name="txtCodCounter" id="txtCodCounter" {if $sellerCode}value="{$sellerCode}"{/if} />
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
                <div class="span-5">
                    <input type="text" name="txtEmissionPlace" id="txtEmissionPlace" {if $data.emissionPlace}value="{$data.emissionPlace|htmlall}"{/if} />
                </div>

                <div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
                <div class="span-5 append-3 last">
                    <input type="text" name="txtEmissionDate" id="txtEmissionDate" title='El campo "Fecha de Emisi&oacute;n" es obligatorio' {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Asesor de Venta:</div>
                <div class="span-5">
                    <input type="text" name="txtNameCounter" id="txtNameCounter" {if $data.counterName}value="{$data.counterName|htmlall}"{/if} />
                </div>

                <div class="span-3 label">* Comercializador:</div>
                <div class="span-4 append-4 last">
                    <input type="text" name="txtDealer" id="txtDealer" {if $data.name_dea}value="{$data.name_dea|htmlall}"{/if} />
                </div>
            </div>
            {if $sellFree eq 'YES'}
                <div class="span-24 last line" id="divSellFree">
                    <div class="prepend-4 span-4 label">* Es Free:</div>
                    <div class="span-5">
                        <input type="radio" name="rdIsFree" id="rdIsFree" value="YES" title="El campo 'Es Free' es obligatorio." /> S&iacute;
                        <input type="radio" name="rdIsFree" id="rdIsFree" value="NO" checked /> No
                    </div>
                </div>
            {/if}

            <div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
                <div class="prepend-4 span-4 label">* Moneda:</div>
                <div class="span-5">
                    {foreach from=$data.currencies item="l" key="k"}
                        <input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur|htmlall}<br/>
                    {/foreach}
                </div>
            </div>

            <input type="hidden" name="hdnCardNumRPC" id="hdnCardNumRPC" {if $nextAvailableNumber}value="{$nextAvailableNumber}"{/if} />
            <input type="hidden" name="hdnEmissionDateRPC" id="hdnEmissionDateRPC" {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
            <input type="hidden" name="hdnSerial_cntRPC" id="hdnSerial_cntRPC" {if $serial_cnt}value="{$serial_cnt}"{/if} />
            <input type="hidden" name="hdnSerial_couRPC" id="hdnSerial_couRPC" {if $data.serial_cou}value="{$data.serial_cou}"{/if} />
            <input type="hidden" name="hdnOfCounterRPC" id="hdnOfCounterRPC" value="{$ofCounter}" />
        {/if}



    </div>

    <div class="span-24 last" id="customerContainer"></div>

    <div class="span-24 last line separator">
        <label>Datos del Producto</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Producto:</div>
        <div class="span-5" id="productsContainer">
            <select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio.'>
                <option value="" serial_pro="">- Seleccione -</option>
                {foreach from=$productListByDealer item="l"}
                    <option value="{$l.serial_pbd}"
                            id="product_{$l.serial_pbd}"
                            serial_pro="{$l.serial_pro}"
                            max_extras_pro="{$l.max_extras_pro}"
                            range_price="{$l.price_by_range_pro}"
                            calculate_price="{$l.calculate_pro}"
                            services="{$l.has_services_pro}"
                            serial_pxc="{$l.serial_pxc}"
                            price_by_day_pro="{$l.price_by_day_pro}"
                            children_pro="{$l.children_pro}"
                            adults_pro="{$l.adults_pro}"
                            flights_pro="{$l.flights_pro}"
                            generate_number_pro="{$l.generate_number_pro}"
                            senior_pro="{$l.senior_pro}"
                            show_price_pro="{$l.show_price_pro}"
                            extras_restricted_to_pro="{$l.extras_restricted_to_pro}"
                            percentage_extras="{$l.percentage_extras}"
                            percentage_children="{$l.percentage_children}"
                            percentage_spouse="{$l.percentage_spouse}"

                            destination_restricted_pro="{$l.destination_restricted_pro}"
                            restricted_children_pro="{$l.restricted_children_pro}"
                            individual_pro="{$l.individual_pro}"
                            >
                        {$l.name_pbl}
                    </option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">Precio:</div>
        <div class="span-4 append-4 last line" id="priceContainer"></div>
    </div>

    <div class="span-24 last line separator">
        <label>Datos del Viaje</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="depDate">* Fecha de Salida:</div>
        <div class="prepend-4 span-4 label" style="display:none" id="startCoverage">* Inicio de Cobertura:</div>
        <div class="span-5">
            <input type="text" name="txtDepartureDate" id="txtDepartureDate" title='El campo "Fecha de Salida" es obligatorio' />
        </div>


        <div class="span-3 label" id="arrDate">* Fecha de Retorno:</div>
        <div class="span-3 label" style="display:none" id="endCoverage">* Fin de Cobertura:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtArrivalDate" id="txtArrivalDate"  title='El campo "Fecha de Retorno" es obligatorio'/>
        </div>
    </div>

    <div class="span-24 last line" id="divDestination">
        <div class="prepend-4 span-4 label">* Pa&iacute;s de Destino:</div>
        <div class="span-5" id="countryDestinationContainer">
            <select class="span-5" name="selCountryDestination" id="selCountryDestination" title='El campo "Pa&iacute;s de Destino" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
            <select name="selCityDestination" id="selCityDestination" title='El campo "Ciudad de Destino" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer"># de D&iacute;as:</div>
        <div class="span-5">
            <label id="txtDaysBetween"></label>
        </div>
        <input type="hidden" name="hddDays" id="hddDays" value=""/>
    </div>

    <div class="span-24 last line">
        <div class="span-13 label" id="rangeSelect"></div>
        <div class="span-11 label last" id="rangePricesContainer"></div>
    </div>

    <div class="span-24 last line" id="extrasGlobalSection">
        <div class="span-24 last line" id="extrasBox">
            <div class="span-24 last line separator" id="extasTitle">
                <label>Extras</label>
            </div>
            <div id="extrasContainer"></div>
            <div class="prepend-9 span-10 append-4 last line">
                <label>Valor por Extras: USD. </label>
                <label id="totalPriceExtras">{$sale.totalFeeExtras}</label>
            </div>
        </div>
        <div class="span-24 last line" id="extrasContainer"></div>
        <input type="hidden" name="hdnTotalPriceExtra" id="hdnTotalPriceExtra" value=""/>
        <input type="hidden" name="hdnTotalCostExtra" id="hdnTotalCostExtra" value=""/>
        <input type="hidden" name="hdnChildrenFree" id="hdnChildrenFree"/>
        <input type="hidden" name="hdnAdultsFree" id="hdnAdultsFree"/>
    </div>

    <input type="hidden" name="count" id="count" value="0" />

    <div class="span-24 last line" id="addNew" style="display: none" >
        <div class="prepend-11 span-3 append-1" >
            <input class="span-4 ui-state-default ui-corner-all" type="button" name="btnAdd" id="btnAdd" value="Agregar Extra" >
        </div>
        <div class="span-3" >
            <input class="span-3 ui-state-default ui-corner-all" type="button" name="btnDeleteExtra" id="btnDeleteExtra" value="Eliminar" style="display: none;"    />
        </div>
        <div class="span-3 last" >
            <input type="button" name="btnResetExtra" id="btnResetExtra" class="span-3 ui-state-default ui-corner-all" value="Resetear" style="display: none;" />
        </div>
    </div>


    <div class="span-24 line" id="servicesContainer"></div>


    <div class="span-24 last line separator">
        <label>Total de la Venta</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-1">
            <label>Total:</label>
        </div>
        <div class="span-4 append-7 last " id="totalContainer"></div>
    </div>

    {*
    <div class="span-24 last line">
    <div class="prepend-8 span-4">
    <label>Total Incluido Impuestos:</label>
    </div>
    <div class="span-4 append-7 last " id="totalGrossContainer"></div>
    </div>
    *}

    {if $isTherePromoForCountry}
        <div class="span-24 last line separator">
            <div class="span-6">
                <label>Aplicar cup&oacute;n de Promociones</label>
            </div>
            <div class="prepend-3 span-6 center" style="color: #000;">
                <input type="radio" name="rdCouponApply" id="rdCouponApply_YES" value="YES" /> Si
                <input type="radio" name="rdCouponApply" id="rdCouponApply_NO" value="NO" checked /> No
            </div>
        </div>
        <div class="span-24 last line" id="generalCustomerPromoContainer" style="display: none;">
            <div class="span-24 last center">
                <div class="span-5 prepend-7 label">
                    C&oacute;digo de Cup&oacute;n:
                </div>
                <div class="span-8 left">
                    <input type="text" name="txtCoupon" id="txtCoupon" title='El campo "Cupon" es obligatorio' />
                    <img src="{$document_root}img/loading_small.gif" alt="Cargando..." id="promo_loader" style="display: none;" />
                    <input type="button" name="btnCustomerPromoButon" id="btnCustomerPromoButon" value="Validar" />
                </div>
            </div>

            <div class="prepend-3 span-21 last line center">
                <i style="font-size: 10px;">Para actualizar la informaci&oacute;n, por favor haga click en "Validar"</i>
            </div>

            <div class="span-24 last center" id="customerPromo_container"></div>
        </div>
    {/if}
    
    {*if $validate_holder_document == 'YES'}
        <div class="span-24 last line separator">
            <label>Datos Facturaci&oacute;n</label>
        </div>

        <div class="span-24 last line center">
            <input type="radio" name="rdInvoicing" id="rdInvoicing_PAX" value="PAX" checked="checked">Al Titular 
            <input type="radio" name="rdInvoicing" id="rdInvoicing_OTHER" value="OTHER" >Otros Datos
        </div>

        <div class="span-24 last line" id="invoiceDataContainer"></div>
    {/if*}
    
    <div class="span-24 last line separator">
        <div class="span-6">
            <label>Forma de Pago</label>
        </div>
    </div>
    
    <div class="prepend-10 span-6 append-6 last line">
        {foreach from="$custom_payments" key="key" item="payment"}
            <input type="radio" name="rdPaymentForm" id="rdPaymentForm_{$key}" value="{$key}" {if $key=='CASH'}checked{/if} /> {$payment} <br />
        {/foreach}
    </div>

    <div class="span-24 last line separator">
        <label>Observaciones</label>
    </div>

    <div class="span-24 last line">
        <div class="span-24 last line center">
            <b>&iquest; ENTREG&Oacute; KIT F&Iacute;SICO ?</b> <br />
            <input type="radio" name="rdDeliveredKit" id="rdDeliveredKitYES" value="1" checked="checked">Si 
            <input type="radio" name="rdDeliveredKit" id="rdDeliveredKitNO" value="0" >No
        </div>
        
        <div class="prepend-7">
            <textarea name="observations" id="observations" title='El campo "Observaciones" es obligatorio'></textarea>
        </div>
    </div>
    <div class="span-24 last line separator">
        <div class="span-5">
            <label>Factura a nombre de:</label>
        </div>
    </div>
    <div class="prepend-8 span-12 last" style="padding-top: 5px">
    <label for="selectInv">Tipo de Facturación:</label>
        <select name="selectInv" id="selectInv">
            <option value="autoInv">Facturación Automatica</option>
            <option value="noInv">Facturar aplicando Convenio</option>
        </select>
    </div>
    <div class="prepend-9 span-12 last" id="radioBill" style="padding-top: 5px;padding-bottom: 5px">
        <label for="radio_customer" style="color: #000000;">Cliente:</label>
        <input type="radio" id="radio_customer" name="bill_to_radio" value="CUSTOMER" {if $branchData.bill_to_dea eq 'CUSTOMER'}checked="checked"{/if}/>
        <label for="radio_dealer" style="color: #000000;">Comercializador:</label>
        <input type="radio" id="radio_dealer" name="bill_to_radio" value="DEALER" {if $branchData.bill_to_dea eq 'DEALER'}checked="checked"{/if}/>
        <label for="radio_dealer" style="color: #000000;">Otro:</label>
        <input type="radio" id="radio_dealer" name="bill_to_radio" value="OTHER" {if $branchData.bill_to_dea eq 'OTHER'}checked="checked"{/if}/>
    </div>
    <div class="span-10 prepend-7" style="padding-top: 10px">
        <div class="span-10  notice center" id="invLabel" style="display:none; padding-top: 5px;">El contrato no sera facturado automaticamente, el proceso se facturación seguira igual</div>
        
    </div>
        
    <div class="span-24 last line" id="customerData" {if $branchData.bill_to_dea neq 'CUSTOMER'}style="display:none;"{/if}>
        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">* Documento:</div>
            <div class="span-5">
                <input type="text" name="txtCustomerDocument" id="txtCustomerDocument" title='El campo "Documento" es obligatorio.' value="{$customerData.document_cus}"/>
            </div>

            <div class="span-3 label">* Tipo de Documento:</div>
            <div class="span-6 last">
                {if $validate_holder_document eq 'NO'}
                    {if $customerData}
                        {if $customerData.type_cus eq 'PERSON'}
                            Persona Natural
                        {elseif $customerData.type_cus eq 'LEGAL_ENTITY'}
                            Persona Jur&iacute;dica
                        {/if}
                    {else}
                        <select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.' >
                            <option value="">- Seleccione -</option>
                            {foreach from=$typesList item="l"}
                                <option value="{$l}" {if $l eq $customerData.type_cus}selected{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                            {/foreach}
                        </select>
                    {/if}
                {else}
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.type_cus eq 'LEGAL_ENTITY'}checked{/if} /> RUC					
                    <input type="radio" name="rdDocumentType" id="rdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
                    <input type="hidden" name="selType" id="selType" value="{$customerData.type_cus}" />
                {/if}				
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label" id="labelName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}*Razo&oacute;n Social: {else}*Nombre:{/if}</div>
            <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' value="{$customerData.first_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

            <div id="lastNameContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
                <div class="span-3 label">*Apellido:</div>
                <div class="span-4 append-2 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' value="{$customerData.last_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
            </div>                
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus|htmlall}">
            </div>

            <div class="span-3 label">*Tel&eacute;fono Principal:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
            <div class="span-5"> <input type="text" name="txtPhone2" id="txtPhone2"  value="{$customerData.phone2_cus}"/></div>

            <div class="span-3 label">Correo Electr&oacute;nico:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtEmail" id="txtEmail" title='El campo "Correo Electr&oacute;nico" es obligatorio' value="{$customerData.email_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
            <div class="span-5"> <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.auxData.serial_cou}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select> </div>

            <div class="span-3 label">*Ciudad:</div>
            <div class="span-4 append-2 last" id="cityContainer"> <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                </select> 
            </div>
        </div>
    </div>
    <div class="span-24 last line" id="otherData" {if $branchData.bill_to_dea neq 'OTHER'}style="display:none;"{/if}>
        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">* Documento:</div>
            <div class="span-5">
                <input type="text" name="txtOtherDocument" id="txtOtherDocument" title='El campo "Documento" es obligatorio.'value="{$customerData.document_cus}">
            </div>

            <div class="span-3 label">* Tipo de Documento:</div>
            <div class="span-6 last">
                {if $validate_holder_document eq 'NO'}
                    {if $customerData}
                        {if $customerData.type_cus eq 'PERSON'}
                            Persona Natural
                        {elseif $customerData.type_cus eq 'LEGAL_ENTITY'}
                            Persona Jur&iacute;dica
                        {/if}
                    {else}
                        <select name="selTypeOther" id="selTypeOther" title='El campo "Tipo de Cliente" es obligatorio.' >
                            <option value="">- Seleccione -</option>
                            {foreach from=$typesList item="l"}
                                <option value="{$l}" {if $l eq $customerData.type_cus}selected{/if}>{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>
                            {/foreach}
                        </select>
                    {/if}
                {else}
                    <input type="radio" name="OtherrdDocumentType" id="OtherrdDocumentType_CI" value="CI" person_type="PERSON" checked /> CI
                    <input type="radio" name="OtherrdDocumentType" id="OtherrdDocumentType_RUC" value="RUC" person_type="LEGAL_ENTITY" {if $customerData.type_cus eq 'LEGAL_ENTITY'}checked{/if} /> RUC					
                    <input type="radio" name="OtherrdDocumentType" id="OtherrdDocumentType_PASSPORT" value="PASSPORT" person_type="PERSON" /> Pasaporte
                    <input type="hidden" name="selTypeOther" id="selTypeOther" value="{$customerData.type_cus}" />
                {/if}				
            </div>
        </div>
        <div class="prepend-2 span-19 append-3 last line" id="genderOtherDiv" {if $data.type_cus eq LEGAL_ENTITY}style="display: none"{/if} title='El campo "Genero" es obligatorio.'>
            <div class="prepend-1 span-4 label">* G&eacute;nero:</div>
            <div class="span-5">  
                <input type="radio" name="rdOtherGender" id="rdOtherGender" value="M" title='El campo "Genero" es obligatorio.' {if $customerData.gender_cus eq 'M'}checked{/if} /> Masculino <br />
                <input type="radio" name="rdOtherGender" id="rdOtherGender" value="F" title='El campo "Genero" es obligatorio.' {if $customerData.gender_cus eq 'F'}checked{/if} /> Femenino
            </div>
            <div class="span-3 label">* Fecha de Nacimiento:</div>
            <input type="text" name="txtBirthdayOtherCustomer" id="txtBirthdayOtherCustomer" title='El campo "Fecha de Nacimiento" es obligatorio' {if $customerData.birthdate_cus}value="{$customerData.birthdate_cus}"{/if}/>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label" id="labelOtherName">{if $customerData.type_cus eq 'LEGAL_ENTITY'}*Razo&oacute;n Social: {else}*Nombre:{/if}</div>
            <div class="span-5">  <input type="text" name="txtOtherFirstName" id="txtOtherFirstName" title='El campo "Nombre" es obligatorio.' value="{$customerData.first_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>

            <div id="lastNameOtherContainer" {if $customerData.type_cus eq 'LEGAL_ENTITY'}style="display: none;"{/if}>
                <div class="span-3 label">*Apellido:</div>
                <div class="span-4 append-2 last">  <input type="text" name="txtOtherLastName" id="txtOtherLastName" title='El campo "Apellido" es obligatorio' value="{$customerData.last_name_cus|htmlall}" {if $customerData}readonly="readonly"{/if}> </div>
            </div>                
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Direcci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="txOthertAddress" id="txtOtherAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$customerData.address_cus|htmlall}">
            </div>

            <div class="span-3 label">*Tel&eacute;fono Principal:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtOtherPhone1" id="txtOtherPhone1" title='El campo "Tel&eacute;fono Principal" es obligatorio' value="{$customerData.phone1_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Tel&eacute;fono Secundario:</div>
            <div class="span-5"> <input type="text" name="txtOtherPhone2" id="txtOtherPhone2"  value="{$customerData.phone2_cus}"/></div>

            <div class="span-3 label">Correo Electr&oacute;nico:</div>
            <div class="span-4 append-2 last">
                <input type="text" name="txtOtherEmail" id="txtOtherEmail" title='El campo "Correo Electr&oacute;nico" es obligatorio' value="{$customerData.email_cus}">
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">*Pa&iacute;s:</div>
            <div class="span-5"> <select name="selOtherCountry" id="selOtherCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $l.serial_cou eq $customerData.auxData.serial_cou}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select> </div>

            <div class="span-3 label">*Ciudad:</div>
            <div class="span-4 append-2 last" id="cityOtherContainer"> <select name="selOtherCity" id="selOtherCity" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$cityList item="l"}
                        <option value="{$l.serial_cit}" {if $l.serial_cit eq $customerData.serial_cit}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                </select> 
            </div>
        </div>
    </div>

    <div class="span-24 last line" id="dealerData" {if $branchData.bill_to_dea neq 'DEALER'}style="display:none;"{/if}>
        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Id:</div>
            <div class="span-5">
                {$branchData.id_dea}
                <input type="hidden" name="hdnDealerDocument" id="hdnDealerDocument" value="{$branchData.id_dea}" />
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Nombre:</div>
            <div class="span-5">
                {$branchData.name_dea}
            </div>

            <div class="span-3 label">Direcci&oacute;n</div>
            <div class="span-6 last">
                {$branchData.address_dea}
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Tel&eacute;fono Principal:</div>
            <div class="span-5">
                {$branchData.phone1_dea}
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-2 last">
                {$branchData.phone2_dea}
            </div>
        </div>

        <div class="prepend-2 span-19 append-3 last line">
            <div class="prepend-1 span-4 label">Correo Electr&oacute;nico:</div>
            <div class="span-5">
                {$branchData.email2_dea}
            </div>
        </div>
    </div>

    <div class="span-24 last buttons">
        <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
    <input type="hidden" name="hdnCardNum" id="hdnCardNum" {if $nextAvailableNumber}value="{$nextAvailableNumber}"{/if} />
    <input type="hidden" name="hdnEmissionDate" id="hdnEmissionDate" {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
    <input type="hidden" name="hdnAge" id="hdnAge" value="{$age}" />
    <input type="hidden" name="hdnElderlyAgeLimit" id="hdnElderlyAgeLimit" value="{$elderlyAgeLimit}" />
    <input type="hidden" name="hdnDisplayQuestionsAge" id="hdnDisplayQuestionsAge" value="{$ageForDisplayQuestions}" />
    <input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
    <input type="hidden" name="hdnSaveQuestions" id="hdnSaveQuestions" value="0" />
    <input type="hidden" name="hdnQuestionsExists" id="hdnQuestionsExists" value="{if $questionList}1{else}0{/if}" />
    <input type="hidden" name="hdnQuestionsAnswered" id="hdnQuestionsAnswered" value="0" />
    <input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" value="{$serial_cou}" />
    <input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$data.serialDealer}" />
    <input type="hidden" name="hdnSerial_cou" id="hdnCntSerial_cou" value="{$serial_cou}" />
    <input type="hidden" name="hdnSerial_cnt" id="hdnSerial_cnt" value="{$serial_cnt}" />
    <input type="hidden" name="hdnOriginal_cnt" id="hdnOriginal_cnt" value="{$serial_cnt}" />
    <input type="hidden" name="hdnTotal" id="hdnTotal" value="" />
    <input type="hidden" name="hdnTotalCost" id="hdnTotalCost" value="" />
    <input type="hidden" name="hdnMaxCoverTime" id="hdnMaxCoverTime" value="{$maxCoverTime}" />
    <input type="hidden" name="hdnDirectSale" id="hdnDirectSale" value="{$ofCounter}" />
    <input type="hidden" name="hdnFlights" id="hdnFlights" value="NO" />
    <input type="hidden" name="sale_type" id="sale_type" value="VIRTUAL" />
    <input type="hidden" name="hdnValidateHolderDocument" id="hdnValidateHolderDocument" value="{$validate_holder_document}" />
    <input type="hidden" name="hdnInvoicingDataValidation" id="hdnInvoicingDataValidation" value="" title="El Nro. Documento de Facturaci&oacute;n es incorrecto." />
    <input type="hidden" name="hdnTypeDbc" id="hdnTypeDbc" value="PERSON" />
    <input type="hidden" name="hdnCustomerDocument" id="hdnCustomerDocument"/>
    <input type="hidden" name="hdnTypeCustomer" id="hdnTypeCustomer"/>
    <input type="hidden" name="hdnSessionUser" id="hdnSessionUser" value="{$sessionUser}"/>
</form>
