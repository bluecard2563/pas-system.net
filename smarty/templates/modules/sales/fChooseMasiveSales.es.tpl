<!--/*
File: fChooseSales.es.tpl
Author: Nicolas Flores
Creation Date: 02/03/2010
Last Modified: 
Modified by: 
*/-->


{assign var="title" value="SELECCION DE VENTAS MASIVAS"}
<div class="span-24 last title">
	Selecci&oacute;n la Venta Masiva para la carga de clientes<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	<div class="append-7 span-10 prepend-7 last">
		{if $error eq 'success'}
			<div class="span-10 success" align="center">
            	La venta se ha registrado exitosamente.
            </div>
		{elseif $error eq 'wrong_extension'}
			<div class="span-10 error" align="center">
            	La extensi&oacute;n del archivo es err&oacute;nea. <br />
            	S&oacute;lo se admiten .xls y .xlsx
            </div>
		{elseif $error eq 'upload_error'}
			<div class="span-10 error" align="center">
            	Hubo un error en la carga del archivo.
            </div>
		{elseif $error eq 'customer_secuence_error'}
			<div class="span-10 error" align="center">
            	La secuencia de pasajeros en el archivo es incorrecta.
            </div>
		{elseif $error eq 'extras_not_allowed'}
			<div class="span-10 error" align="center">
            	El producto seleccionado no admite adicionales.
            </div>
		{elseif $error eq 'customer_extras_secuence_error'}
			<div class="span-10 error" align="center">
            	Existe un error en las relaciones entre clientes. Revise el archivo.
            </div>
		{elseif $error eq 'wrong_numbers_format'}
			<div class="span-10 error" align="center">
            	El formato para los numeros de tel&eacute;fono de los clientes es incorrecto.<br /> 
            	Por favor rev&iacute;selo.
            </div>
		{elseif $error eq 'seniors_not_allowed'}
			<div class="span-10 error" align="center">
            	No se admiten adultos mayores como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>			
		{elseif $error eq 'only_children_allowed_for_extras'}
			<div class="span-10 error" align="center">
            	Solamente se admiten menores como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'only_adults_allowed_for_extras'}
			<div class="span-10 error" align="center">
            	Solamente se admiten adultos como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'no_file_found'}
			<div class="span-10 error" align="center">
            	No se pudo encontrar el archivo a cargar. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'no_price_found'}
			<div class="span-10 error" align="center">
            	No se pudo encontrar un precio para uno de los pasajeros. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'duplicated_spouse'}
			<div class="span-10 error" align="center">
            	Existe un registor duplicado de "Conyuge" para un cliente. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'extras_not_allowed'}
			<div class="span-10 error" align="center">
            	Este producto no admite m&aacute;s adicionales. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'unknown_relationship'}
			<div class="span-10 error" align="center">
            	Existe una relaci&oacute;n con el titular desconocida. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'destination_unknown'}
			<div class="span-10 error" align="center">
            	El destino ingresado es desconocido. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'wrong_travel_dates'}
            <div class="span-10 error" align="center">
            	Las fechas de viaje son incorrectas. <br /> 
            	Por favor revise el archivo.
            </div>
        {elseif $error eq 'insert_error'}
            <div class="span-10 error" align="center">
            	Hubo errores al ingresar a los pasajeros. Por favor vuelva a intentarlo.
            </div>
		{elseif $error eq 'birthdate_missing'}
            <div class="span-10 error" align="center">
            	Hubo errores al ingresar a los pasajeros. Hace falta una fecha de nacimiento en el archivo.
            </div>
		{elseif $error eq 'document_missing'}
            <div class="span-10 error" align="center">
            	Hubo errores al ingresar a los pasajeros. Uno de los pasajeros no tiene su documento de identificaci&oacute;n.
            </div>		
        {/if}
	</div>
{/if}
 
<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $countryList}
	<div class="span-24 last line"></div>
		<div class="span-24 last line">
		<div class="prepend-3 span-4 label">*Pa&iacute;s:</div>
		<div class="span-5">
			<select name="selCountry" id="selCountry">
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}" {if $countryList|@count eq 1} selected{/if}>{$l.name_cou}</option>
				{/foreach}
			</select>
		</div>
		<div class="span-3 label">*Representante:</div>
		<div class="span-5 append-3 last" id="managerContainer">
			<select name="selManager" id="selManager">
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">Responsable:</div>
		<div class="span-5" id="responsableContainer">
			<select name="selResponsable" id="selResponsable">
				<option value="">- Seleccione -</option>
			</select>
		</div>

		<div class="span-3 label">Categoria:</div>
		<div class="span-5 append-3">
			<select name="selCategory" id="selCategory">
				<option value="">- Seleccione -</option>
				{if $categoryList}
					{foreach from=$categoryList item="l"}
						<option value="{$l}">{$l}</option>
					{/foreach}
				{/if}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">Tipo:</div>
		<div class="span-5">
			<select name="selType" id="selType">
				<option value="">- Seleccione -</option>
				{if $typeList}
					{foreach from=$typeList item="l"}
						<option value="{$l.serial_dlt}">{$l.name}</option>
					{/foreach}
				{/if}
			</select>
		</div>


		<div class="span-5 prepend-3">
			<input type="button" id="searchBranches" value="Buscar"/>
		</div>
	</div>

	<div class="span-24 last line" id="dealerContainer"></div>
	<div class="span-24 last line" id="salesContainer"></div>

	<!--UPLOAD FILE DIALOG-->
	<div id="uploading_file">
		<div class="prepend-1 span-2" style="margin-top: 27px;">
			<img src="{$document_root}img/ajax-loader.gif"/>
		</div>
		<div class="span-3" style="margin-top: 20px;">
			<span id="messageText">Subiendo archivo</span>
			Espere por favor.
		</div>
	</div>
	<!--END UPLOAD FILE DIALOG-->

{else}
	<div class="span-24 last line" align="center">Lo sentimos, no existen ventas masivas activas.</div>
{/if}
