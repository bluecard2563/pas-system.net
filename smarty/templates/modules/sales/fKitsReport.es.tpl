{assign var="title" value="REPORTE DE KITS ENTREGADOS"}
<form name="frmKitsReport" id="frmKitsReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        Reporte de Kits entregados<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>*Zona:</label>
        </div>
        <div class="span-6 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$nameZoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3">
            <label>*Pa&iacute;s:</label>
        </div>
        <div class="span-4 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Ciudad:</label>
        </div>
        <div class="span-6 last" id="cityContainer">
            <select name="selCity" id="selCity">
                <option value="">- Todos -</option>
            </select>
        </div>

        <div class="span-3">
            <label>Representante:</label>
        </div>
        <div class="span-4 append-4 last" id="managerContainer">
            <select name="selManager" id="selManager">
                <option value="">- Todos -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Responsable:</label>
        </div>
        <div class="span-6 last" id="comissionistsContainer">
            <select name="selComissionist" id="selComissionist">
                <option value="">- Todos -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Comercializador:</label>
        </div>
        <div class="span-6 last" id="comercializadorContainer">
            <select name="selDealer" id="selDealer">
                <option value="">- Todos -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Sucursal:</label>
        </div>
        <div class="span-4 append-4 last" id="branchContainer">
            <select name="selBranch" id="selBranch">
                <option value="">- Todos -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Counter:</label>
        </div>
        <div class="span-4 append-4 last" id="countersContainer">
            <select name="selCounter" id="selCounter">
                <option value="">- Todos -</option>
            </select>
        </div>
    </div>            

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>* Fecha inicio:</label>
        </div>
        <div class="span-6 last">
            <input type="text" id="txtBeginDate" name="txtBeginDate" title='El campo "Fecha inicio" es obligatorio.'/>
        </div>

        <div class="span-3">
            <label>* Fecha fin:</label>
        </div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtEndDate" name="txtEndDate" title='El campo "Fecha fin" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
    </div>
</form>