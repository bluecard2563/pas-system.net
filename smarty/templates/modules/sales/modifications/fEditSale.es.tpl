{assign var="title" value="SOLICITUD DE MODIFICACI&Oacute;N DE  VENTA"}
<script type="text/javascript" src="{$document_root}js/modules/sales/globalInvoicingFunctions.js"></script>
<script type="text/javascript" src="{$document_root}js/modules/sales/globalSalesFunctions.js"></script>
<form name="frmEditSale" id="frmEditSale" method="post" action="{$document_root}modules/sales/modifications/pEditSale" class="">
    <input type="hidden" name="isEdition" id="isEdition" value="1">
    <div class="span-24 last title">
        Solicitud de Modificaci&oacute;n de Venta<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $sale.modify eq 'NO'}
        <div class="span-24 last title">
            <label>Esta venta ya ha sido facturada, la modificaci&oacute;n es limitada</label>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de la Venta</label>
    </div>
    <input type="hidden" name="hdnModify" id="hdnModify" value="{$sale.modify}" />

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tipo de Venta:</div>
        <div class="span-3">
            <input type='radio' name='sale_type' id='sale_type' value='VIRTUAL' title='El campo "Tipo de Venta" es obligatorio' {if $sale.stock_type_sal=='VIRTUAL'}checked{/if} /> Virtual
        </div>
        <div class="span-3">
            <input type='radio' name='sale_type' id='sale_type' value='MANUAL' title='El campo "Tipo de Venta" es obligatorio' {if $sale.stock_type_sal=='MANUAL'}checked{/if} /> Manual
        </div>
        <input type="hidden" name="hdnStockType" id="hdnStockType" value="{$sale.stock_type_sal}" />
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* No de Tarjeta:</div>
        <div class="span-5">
            <input type="text" name="txtCardNum" id="txtCardNum" title='El campo "N&uacute;mero de Tarjeta" es obligatorio' 
                   {if $sale.card_number_sal}value="{$sale.card_number_sal}"{/if}
                   {if $sale.modify eq 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">C&oacute;digo Vendedor:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtCodCounter" id="txtCodCounter" 
                   {if $sellerCode}value="{$sellerCode}"{/if}
                   {if $sale.modify eq 'NO'}disabled{/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtEmissionPlace" id="txtEmissionPlace" 
                   {if $data.emissionPlace}value="{$data.emissionPlace}"{/if}
                   {if $sale.modify eq 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtEmissionDate" id="txtEmissionDate" title='El campo "Fecha de Emisi&oacute;n" es obligatorio' 
                   {if $emissionDate}value={$emissionDate}{/if}
                   {if $sale.modify eq 'NO'}disabled{/if}/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Asesor de Venta:</div>
        <div class="span-5">
            <input type="text" name="txtNameCounter" id="txtNameCounter" 
                   {if $data.counterName}value="{$data.counterName}"{/if}
                   {if $sale.modify eq 'NO'}disabled{/if}/>
        </div>

        <div class="span-3 label">* Comercializador:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtDealer" id="txtDealer" 
                   {if $data.name_dea}value="{$data.name_dea}"{/if}
                   {if $sale.modify eq 'NO'}disabled{/if}/>
        </div>
    </div>

    {if $sale.free_sal eq 'YES'}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Es Free:</div>
            <div class="span-5">
                <input type="radio" name="rdIsFree" id="rdIsFree" value="YES" title="El campo 'Es Free' es obligatorio." checked/> S&iacute;
                <input type="radio" name="rdIsFree" id="rdIsFree" value="NO" /> No
            </div>
        </div>
    {/if}

    <div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
        <div class="prepend-4 span-4 label">* Moneda:</div>
        <div class="span-5">
            {foreach from=$data.currencies item="l" key="k"}
                <input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.sales_serial_cur eq $l.serial_cur}checked='checked'{/if} /> {$l.name_cur}<br/>
            {/foreach}
        </div>
    </div>

    <div class="span-24 last" id="customerContainer">
        <div class="span-24 last line separator">
            <label>Datos del Cliente</label>
        </div>

        {if $customer.vip_cus eq 1}
            <div class="append-7 span-10 prepend-7 last line">
                <div class=" span-10 notice">INFO: Es cliente VIP.</div>
            </div>
        {/if}
        {if $customer.status_cus eq 'INACTIVE'}
            <div class="append-7 span-10 prepend-7 last line">
                <div class=" span-10 notice">INFO: El Cliente se encuentra en estado Inactivo.</div>
            </div>
        {/if}

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
            <div class="span-5">
                <input type="text" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio' 
                       value="{$customer.document_cus}"
                       {if $sale.modify eq 'NO'}disabled{/if}/>
                <input type="hidden" name="serialCus" id="serialCus"  
                       {if $customer.serial_cus}value="{$customer.serial_cus}"{/if}/>
            </div>
            <div class="span-3 label">*Tipo de Cliente</div>
            <div class="span-4 append-2 last">
                <input type="hidden" value="{$customer.type_cus}" id="selType" name="selType">
                <select name="invalidSelType" id="invalidSelType" title='El campo "Tipo de Cliente" es obligatorio.' disabled="disabled">
                    <option value="">- Seleccione -</option>
                    {foreach from=$typesList item="l"}
                        <option value="{$l}" {if $l eq $customer.type_cus}selected{/if}>
                            {if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}
                        </option>
                    {/foreach}
                </select>
            </div>
        </div>

        {if $customer.type_cus eq 'PERSON'}
            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Nombre:</div>
                <div class="span-5">
                    <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Nombre" es obligatorio' value="{$customer.first_name_cus}" 
                           {if $sale.modify eq 'NO'}disabled{/if}/>
                </div>

                <div class="span-3 label">* Apellido:</div>
                <div class="span-4 append-4 last">
                    <input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' value="{$customer.last_name_cus|htmlall}"
                           {if $sale.modify eq 'NO'}disabled{/if}/>
                </div>
            </div>
        {else}
            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Raz&oacute;n Social:</div>
                <div class="span-5">
                    <input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Raz&oacute;n Social" es obligatorio'  disabled {if $customer.firstname_cus}value="{$customer.firstname_cus}"{/if} />
                </div>
            </div>
        {/if}

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
            <div class="span-5">
                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $customer.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou|htmlall}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Ciudad</div>
            <div class="span-4 append-4 last" id="cityContainerCustomer">
                <select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$customerCityList item="l"}
                        <option value="{$l.serial_cit}" {if $customer.serial_cit eq $l.serial_cit}selected="selected"{/if}>{$l.name_cit|htmlall}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        {if $customer.type_cus eq 'PERSON'}
            <div class="span-24 last line" id="divAddress1">
                <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
                <div class="span-5">
                    <input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" title='El campo "Fecha de Nacimiento" es obligatorio' 
                           {if $customer.birthdate_cus}value="{$customer.birthdate_cus}"{/if}
                           {if $sale.modify eq 'NO'}disabled{/if}/>
                </div>

                <div class="span-3 label">* Direcci&oacute;n:</div>
                <div class="span-4 append-4 last">
                    <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $customer.address_cus}value="{$customer.address_cus}"{/if} />
                </div>
            </div>
        {else}
            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
                <div class="span-5">
                    <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' 
                           {if $customer.address_cus}value="{$customer.address_cus}"{/if}/>
                </div>
            </div>
        {/if}

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
            <div class="span-5">
                <input type="text" name="txtPhone1Customer" id="txtPhone1Customer" title='El campo "Tel&eacute;fono 1" es obligatorio' 
                       {if $customer.phone1_cus}value="{$customer.phone1_cus}"{/if}/>
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtPhone2Customer" id="txtPhone2Customer"  
                       {if $customer.phone2_cus}value="{$customer.phone2_cus}"{/if}/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Celular:</div>
            <div class="span-5">
                <input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer"  
                       {if $customer.cellphone_cus}value="{$customer.cellphone_cus}"{/if}/>
            </div>

            <div class="span-3 label">Email:</div>
            <div class="span-4 append-4 last">
                <input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio'  
                       {if $customer.email_cus}value="{$customer.email_cus}"{/if}/>
            </div>
        </div>

        {if $customer.type_cus eq 'PERSON'}
            <div class="span-24  last line">
                <div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
                <div class="span-5">
                    <input type="text" name="txtRelative" id="txtRelative" title='El campo "Contacto en caso de Emergencia" es obligatorio' 
                           {if $customer.relative_cus}value="{$customer.relative_cus}"{/if}/>
                </div>

                <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
                <div class="span-5 append-2 last">
                    <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio' 
                           {if $customer.relative_phone_cus}value="{$customer.relative_phone_cus}"{/if}/>
                </div>
            </div>
        {else}
            <div class="span-24  last line">
                <div class="prepend-4 span-4 label">*Gerente:</div>
                <div class="span-5">
                    <input type="text" name="txtManager" id="txtManager" title='El campo "Gerente" es obligatorio' 
                           {if $customer.relative_cus}value="{$customer.relative_cus}"{/if}/>
                </div>

                <div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
                <div class="span-4 append-4 last">
                    <input type="text" name="txtPhoneManager" id="txtPhoneManager" title='El campo "Tel&eacute;fono del Gerente" es obligatorio' 
                           {if $customer.relative_phone_cus}value="{$customer.relative_phone_cus}"{/if}/>
                </div>
            </div>
        {/if}

        <input type="hidden" name="hdnVip" id="hdnVip" value="{$customer.vip_cus}" />
        <div class="prepend-4 span-16 append-4 last line" id="customerQuestionsContainer"></div>
        <div id="hiddenAnswers">
            {foreach from=$answerList item=al}
                <input type="hidden" name="question_{$al.serial_que}" id="question_{$al.serial_que}" value="{if $al.answer_ans}{$al.answer_ans}{else}{$al.yesno_ans}{/if}" />
                <input type="hidden" name="answer_{$al.serial_que}" id="answer_{$al.serial_que}" value="{$al.serial_ans}" />
            {/foreach}
        </div>
    </div>

    <div class="span-24 last line" id="repContainer" style="display: none;">
        <div class="span-24 last line separator">
            <label>Datos del Representante Legal</label>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-3 label">* Identificaci&oacute;n:</div>
            <div class="span-5">
                {$repCustomer.document_cus}
                <input type="hidden" name="repSerialCus" id="repSerialCus" {if $repCustomer.serial_cus}value="{$repCustomer.serial_cus}"{/if} />
                <input type="hidden" name="repType" id="repType" value="PERSON">
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-3 label">* Nombre:</div>
            <div class="span-5">
                {$repCustomer.first_name_cus}
            </div>


            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-2 last">
                {$repCustomer.last_name_cus}
            </div>
        </div>

        <div class="span-20 prepend-4 last line" id="divAddress1">    
            <div class="span-3 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                {$repCustomer.birthdate_cus}
            </div>

            <div class="span-3 label">Email:</div>
            <div class="span-4 append-2 last">
                {$repCustomer.email_cus}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-3 label" id="repCountryContainer">* Pa&iacute;s de Residencia:</div>
            <div class="span-5">
                <select name="repCountry" id="repCountry" class="span-5" title='El campo "Pa&iacute;s de Residencia - Representante" es obligatorio.' disabled="true">
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $repCustomer.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou|htmlall}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Ciudad</div>
            <div class="span-4 append-4 last" id="repCityContainerCustomer">
                <select name="repCityCustomer" id="repCityCustomer" class="span-4" title='El campo "Ciudad - Representante" es obligatorio' disabled="true">
                    <option value="">- Seleccione -</option>
                    {foreach from=$repCustomerCityList item="l"}
                        <option value="{$l.serial_cit}" {if $repCustomer.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit|htmlall}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Datos del Producto</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Producto:</div>
        <div class="span-5" id="productsContainer">
            <select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio.' {if $sale.modify eq 'NO'}disabled{/if}>
                <option value="" serial_pro="">- Seleccione -</option>
                {foreach from=$productListByDealer item="l"}
                    <option value="{$l.serial_pbd}" 
                            id="product_{$l.serial_pbd}" 
                            serial_pro="{$l.serial_pro}"
                            max_extras_pro="{$l.max_extras_pro}" 
                            range_price="{$l.price_by_range_pro}" 
                            calculate_price="{$l.calculate_pro}" 
                            services="{$l.has_services_pro}" 
                            serial_pxc="{$l.serial_pxc}" 
                            price_by_day_pro="{$l.price_by_day_pro}" 
                            children_pro="{$l.children_pro}" 
                            adults_pro="{$l.adults_pro}" 
                            flights_pro="{$l.flights_pro}" 
                            generate_number_pro="{$l.generate_number_pro}" 
                            senior_pro="{$l.senior_pro}" 
                            show_price_pro="{$l.show_price_pro}" 
                            extras_restricted_to_pro="{$l.extras_restricted_to_pro}"
                            percentage_extras="{$l.percentage_extras}" 
                            percentage_children="{$l.percentage_children}" 
                            percentage_spouse="{$l.percentage_spouse}" 

                            destination_restricted_pro="{$l.destination_restricted_pro}"
                            individual_pro="{$l.individual_pro}"
                            {if $sale.serial_pbd eq $l.serial_pbd} selected="selected"{/if}
                            >
                        {$l.name_pbl}
                    </option>
                {/foreach}
            </select>
        </div>
        <input type="hidden" name="hdnProduct" id="hdnProduct" value="{$sale.serial_pbd}"/>
        <div class="span-3 label">Precio:</div>
        <div class="span-4 append-4 last line" id="priceContainer">
            USD {$sale.fee_sal}
            <input type="hidden" name="hdnPrice" id="hdnPrice" value="{$sale.fee_sal}"/>
            <input type="hidden" name="hdnCost" id="hdnCost" value="{$sale.cost_sal}"/>

        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Datos del Viaje</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="depDate">* Fecha de Salida:</div>
        <div class="prepend-4 span-4 label" style="display:none" id="startCoverage">* Inicio de Cobertura:</div>
        <div class="span-5">
            <input type="text" name="txtDepartureDate" id="txtDepartureDate" title='El campo "Fecha de Salida" es obligatorio' value="{$sale.begin_date_sal}"/>
        </div>

        <div class="span-3 label" id="arrDate">* Fecha de Retorno:</div>
        <div class="span-3 label" style="display:none" id="endCoverage">* Fin de Cobertura:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtArrivalDate" id="txtArrivalDate"  title='El campo "Fecha de Retorno" es obligatorio' value="{$sale.end_date_sal}"/>
        </div>
    </div>

    <div class="span-24 last line" id="divDestination" {if !$sale.serial_cou}style="display: none;"{/if}>
        <div class="prepend-4 span-4 label" id="countryDestinationContainer">* Pa&iacute;s de Destino:</div>
        <div class="span-5">
            <select name="selCountryDestination" id="selCountryDestination" title='El campo "Pa&iacute;s de Destino" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}" {if $sale.serial_cou eq $l.serial_cou} selected="selected"{/if} >{$l.name_cou|htmlall}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
            <select name="selCityDestination" id="selCityDestination" title='El campo "Ciudad de Destino" es obligatorio'>
                <option value="">- Seleccione -</option>
                {foreach from=$cityList item="l"}

                    <option id="{$l.serial_cit}" value="{$l.serial_cit}" {if $sale.serial_cit eq $l.serial_cit} selected="selected"{/if} >{$l.name_cit|htmlall}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label"># de D&iacute;as:</div>
        <div class="span-5">
            <label id="txtDaysBetween">{$sale.days_sal}</label>
            <input type="hidden" value="{$sale.days_sal}" id="hdnBackupDays" name="hdnBackupDays" />
        </div>
        <input type="hidden" name="hddDays" id="hddDays" value="{$sale.days_sal}"/>
    </div>

    <div class="span-24 last line">
        <div class="span-13 label" id="rangeSelect"></div>
        <div class="span-11 label last" id="rangePricesContainer"></div>
    </div>

    <div class="span-24 last line" id="extrasGlobalSection">
        <div class="span-24 last line" id="extrasBox">
            <div class="span-24 last line separator" id="extasTitle">
                <label>Extras</label>
            </div>
            <div id="extrasContainer"></div>
            <div class="prepend-9 span-10 append-4 last line">
                <label>Valor por Extras: USD. </label>
                <label id="totalPriceExtras">{$sale.totalFeeExtras}</label>
            </div>
        </div>
        <div class="span-24 last line" id="extrasContainer"></div>
        <input type="hidden" name="hdnTotalPriceExtra" id="hdnTotalPriceExtra" value=""/>
        <input type="hidden" name="hdnTotalCostExtra" id="hdnTotalCostExtra" value=""/>
        <input type="hidden" name="hdnChildrenFree" id="hdnChildrenFree"/>
        <input type="hidden" name="hdnAdultsFree" id="hdnAdultsFree"/>
    </div>

    <input type="hidden" name="count" id="count" value="0"/>

    <div class="span-24 last line" id="globalExtrasControl" {if $sale.modify eq 'NO'}style="display: none"{/if}>
        <div class="span-24 last line" id="addNew" style="display: none" >
            <div class="prepend-13 span-3 ">
                <input class="span-3 ui-state-default ui-corner-all" type="button" name="btnAdd" id="btnAdd" value="Agregar" >
            </div>
            <div class="span-3">
                <input class="span-3 ui-state-default ui-corner-all" type="button" name="btnDeleteExtra" id="btnDeleteExtra" value="Eliminar" style="display: none;" />
            </div>
            <div class="span-3 last">
                <input type="button" name="btnResetExtra" id="btnResetExtra" class="span-3 ui-state-default ui-corner-all" value="Resetear" style="display: none;"/>
            </div>
        </div>
    </div>

    <div class="span-24 line" id="servicesContainer">	
        {if $sbcbs}
            <div class="span-24 last line separator">
                <label>Servicios Adicionales</label> <br />
            </div>

            <div class="span-24 last line center">
                <i>* El precio del servicio adicional es por persona.</i>
            </div>

            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Servicios:</div>
                <div class="span-11">
                    {foreach from=$servicesListByDealer item="l"}
                        <input type="checkbox" name="chkService[]" id="chkService_{$l.serial_sbd}" value="{$l.serial_sbd}" price="{$l.price_sbc}" cost="{$l.cost_sbc}" type_fee="{$l.fee_type_ser}" {foreach from=$sbcbs item="s"}{if $l.serial_sbd eq $s.serial_sbd}checked{/if}{/foreach} {if $sale.modify eq 'NO'}disabled{/if} > 
                        {$l.name_sbl} <i>(USD {$l.price_sbc} {if $l.fee_type_ser eq 'TOTAL'}por cobertura.{else}por d&iacute;a.{/if})</i> <br>
                    {/foreach}
                </div>
            </div>

            <div class="span-24 center last">
                <b>Precio por Servicios:</b>
                <label id="servicePriceContainer" style="font-weight: normal;"> USD {$service_total_price}</label>
                <input type="hidden" name="hdnServicePrice" id="hdnServicePrice" value="{$service_total_price}" />
                <input type="hidden" name="hdnServiceCost" id="hdnServiceCost" value="{$service_total_cost}" />
            </div>
        {/if}
    </div>	

    <div class="span-24 last line separator">
        <label>Total de la Venta</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-1">
            <label>Total:</label>
        </div>
        <div class="span-4 append-7 last " id="totalContainer">USD {$sale.total_sal}</div>
    </div>

    <div class="span-24 last line separator">
        <label>Observaciones</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label line">
            Observaciones Anteriores:
        </div>
        <div class="span-8 line">
            {if $sale.observations_sal}{$data.counterName}: {$sale.observations_sal}{/if}
        </div>
        <div class="span-24 last line separatorExtra"></div>
        <div class="prepend-4 span-4 label">
            * Observaciones de la Modificaci&oacute;n:
        </div>
        <div class="span-8 line">
            <textarea name="observations" id="observations" title='El campo "Observaciones" es obligatorio'></textarea>
        </div>
        <input type="hidden" name="hdnModifier" id="hdnModifier" value="{$nameUser}" />

        <div class="span-24 last line separator">
            <label>Historial de Solicitudes</label>
        </div>
        <div class="span-24 last">
            {if $history}
                <div class="prepend-2 span-20 append-2 last line">
                    <table border="0" id="tblModifyApplications" style="color: #000000;">
                        <THEAD>
                            <tr bgcolor="#284787">
                                <td align="center" class="tableTitle">
                                    No.
                                </td>
                                <td align="center" class="tableTitle">
                                    Fecha
                                </td>
                                <td align="center" class="tableTitle">
                                    Solicitante
                                </td>
                                <td align="center" class="tableTitle">
                                    Observaciones
                                </td>
                                <td align="center"  class="tableTitle">
                                    Estado
                                </td>
                            </tr>
                        </THEAD>
                        <TBODY>
                            {foreach name="modHist" from=$history item="h"}
                                <tr {if $smarty.foreach.modHist.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                                    <td align="center" class="tableField">
                                        {$smarty.foreach.modHist.iteration}
                                    </td>
                                    <td align="center" class="tableField">
                                        {$h.modDate}
                                    </td>
                                    <td align="center" class="tableField">
                                        {$h.applicant}
                                    </td>
                                    <td align="center" class="tableField">
                                        {$h.obs}
                                    </td>
                                    <td align="center" class="tableField">
                                        {if $h.status eq 'DENIED'}Negada
                                        {elseif $h.status eq 'AUTHORIZED'} Autorizada
                                        {elseif $h.status eq 'PENDING'}Pendiente
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </TBODY>
                    </table>
                </div>
            {else}
                <div class="prepend-9 span-15">
                    No existen solicitudes asociadas a esta tarjeta
                </div>
            {/if}
            <div class="span-24 last buttons" id="pageNavPosition"></div>
        </div>
    </div>

    <div class="span-24 last buttons">
        <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Actualizar">
        <input type="hidden" name="hdnCardNum" id="hdnCardNum" {if $sale.card_number_sal}value="{$sale.card_number_sal}"{/if} />
        <input type="hidden" name="hdnEmissionDate" id="hdnEmissionDate" {if $emissionDate}value="{$emissionDate}"{/if} />
        <input type="hidden" name="hdnAge" id="hdnAge" value="{$age}" />
        <input type="hidden" name="hdnElderlyAgeLimit" id="hdnElderlyAgeLimit" value="{$elderlyAgeLimit}" />
        <input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
        <input type="hidden" name="hdnQuestionsFlag" id="hdnQuestionsFlag" value="0" />
        <input type="hidden" name="hdnQuestionsExists" id="hdnQuestionsExists" value="{if $questionList}1{else}0{/if}" />
        <input type="hidden" name="hdnQuestionsAnswered" id="hdnQuestionsAnswered" value="0" />
        <input type="hidden" name="hdnSerial_sal" id="hdnSerial_sal" value="{$sale.serial_sal}" />
        <input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" value="{$product_of_sale_info.serial_cou}" />
        <input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$data.serialDealer}" />
        <input type="hidden" name="hdnSerial_cnt" id="hdnSerial_cnt" value="{$sale.serial_cnt}" />
        <input type="hidden" name="hdnTotal" id="hdnTotal" value="{$sale.total_sal}" />
        <input type="hidden" name="hdnTotalCost" id="hdnTotalCost" value="{$sale.total_cost_sal}" />
        <input type="hidden" name="hdnMaxCoverTime" id="hdnMaxCoverTime" value="{$maxCoverTime}" />
        <input type="hidden" name="hdnInDateSal" id="hdnInDateSal" value="{$sale.in_date_sal}" />
        <input type="hidden" name="hdnSerial_inv" id="hdnSerial_inv" value="{$sale.serial_inv}" />
        <input type="hidden" name="hdnDocumentCustomer" id="hdnDocumentCustomer" value="{$customer.document_cus}"/>
        <input type="hidden" name="hdnFligths" id="hdnFligths" value="{$fligths}"/>
        <input type="hidden" name="hdnOriginalPrice" id="hdnFligths" value="{$sale.fee_sal}"/>
        <input type="hidden" name="hdnOriginalCost" id="hdnFligths" value="{$sale.cost_sal}"/>
        <input type="hidden" name="hasRepData" id="hasRepData" value="{if $repCustomer.serial_cus}YES{else}NO{/if}"/>
        {if $sbcbs AND $sale.modify eq 'NO'}
            {foreach from=$sbcbs item="l"}
                <input type="hidden" name="hdnFixedServices[]"  value="{$l.serial_sbd}" /> 
            {/foreach}
        {/if}
        <input type="hidden" name="hdnIsEditSaleForm" id="hdnIsEditSaleForm" value="1" />
    </div>
</form>