{assign var="title" value="DESPACHAR SOLICITUDES"}
<div class="span-24 last title">
	Despachar Solicitudes de Reactivaci&oacute;n<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $data.cards}
<div class="prepend-7 span-10 last line">
	<table border="0" class="paginate-1 max-pages-7" id="cards_table">
		<thead>
			<tr bgcolor="#284787">
				{foreach from=$data.titles item="l"}
				<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
					{$l}
				</td>
				{/foreach}
			</tr>
		</thead>

		<tbody>
		{foreach name="cardList" from=$data.cards item="l"}
		<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.benefitList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				{$l.card_number_sal}
			</td>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				{$l.name_cus}
				<input type="hidden" style="width: 50px;" name="maxAmount{$l.serial_spf}" id="maxAmount{$l.serial_spf}" value="{$l.price_bxp}" readonly/>
			</td>
			<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
				<a href="{$document_root}modules/sales/modifications/fDispatchReactivationApplications/{$l.serial_slg}">Atender</a>
			</td>
		</tr>
		{/foreach}
		</tbody>
	</table>
</div>
<div class="span-17 last line pageNavPosition" id="pageNavPosition"></div>
{else}
<div class="span-7 span-11 prepend-7  last" align="center">
	<div class="span-10 error" align="center">
		No hay solicitudes pendientes.
	</div>
</div>
{/if}