{assign var="title" value="DESPACHAR SOLICITUDES"}
<div class="span-24 last title">
	 Despachar Solicitudes de Reactivaci&oacute;n de Tarjeta<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmDispatchApplications" id="frmDispatchApplications" action="{$document_root}modules/sales/modifications/pDispatchReactivationApplications" method="post">
	<div class="span-24 last line separator">
	   <label>Datos de la Solicitud</label>
	</div>

	<div class="span-24 last line">

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">N&uacute;mero de Tarjeta:</div>
			<div class="span-5">
				{$data.card_number}
			</div>

			<div class="span-3 label">* Aprobaci&oacute;n: </div>
			<div class="span-4 append-4 last line">
				<select name="selStatus" id="selStatus" title="El campo 'Aprobaci&oacute;n' es obligatorio.">
					<option value="">- Seleccione -</option>
					{foreach from=$data.all_status item="sl"}
					{if $sl neq 'PENDING'}
					<option value="{$sl}">{$global_sales_log_status.$sl}</option>
					{/if}
					{/foreach}
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Nombre del Producto:</div>
			<div class="span-5">{$data.product}</div>
			<div class="span-3 label">Nombre del Cliente:</div>
			<div class="span-4 append-4 last line">{$data.customer_name}</div>
		</div>

		<div class="span-24 last line separator">
			<label>Datos Antiguos de la Tarjeta</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-7 span-4 label">Estado:</div>
			<div class="span-3">{$global_salesStatus[$data.description.old_status_sal]}</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-6 span-3 label">Inicio de Cobertura:</div>
			<div class="span-3">{$data.description.old_begin_date}</div>
			<div class="span-3 label">Fin de Cobertura:</div>
			<div class="span-3 last">{$data.description.old_end_date}</div>
		</div>

		<div class="span-24 last line separator">
			<label>Datos Nuevos de la Tarjeta</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-7 span-4 label">Estado:</div>
			<div class="span-3">{$global_salesStatus[$data.description.new_status_sal]}</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-6 span-3 label">Inicio de Cobertura:</div>
			<div class="span-3">{$data.description.new_begin_date}</div>
			<div class="span-3 label">Fin de Cobertura:</div>
			<div class="span-3 last">{$data.description.new_end_date}</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-6 span-3 label">Raz&oacute;n para la activaci&oacute;n:</div>
			<div class="span-9">{$data.description.comments_request}</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-6 span-3 label">* Comentarios:</div>
			<div class="span-6">
				<textarea name="txtComments" id="txtComments" title='El campo "Comentarios" es obligatorio.'></textarea>
			</div>
		</div>

	</div>

	<input type="hidden" name="hdnSerial_slg" id="hdnSerial_slg" value="{$data.serial_slg}" />

	<div class="span-24 last buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar" />
	</div>
</form>