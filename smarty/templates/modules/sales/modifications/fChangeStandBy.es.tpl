{assign var="title" value="CAMBIOS A STAND-BY"}
<div class="span-24 last title">
	 Cambios a Stand-By<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line separator">
   <label>N&uacute;mero de Tarjeta</label>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">
		* N&uacute;mero de Tarjeta:
	</div>
	<div class="span-5 appen-8 last">
		<input type="text" id="txtCardNumber" name="txtCardNumber" />
	</div>
</div>

<div class="span-24 last buttons line">
	<input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
</div>
<input type="hidden" id="limitTime" name="limitTime" value="{$parameter.value_par}" />

<div class="span-24 last line" id="cardContainer"></div>