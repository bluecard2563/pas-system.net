{assign var="title" value="REACTIVACI&Oacute;N DE TARJETAS EXPIRADAS"}
<div class="span-24 last title line">
	 Reactivaci&oacute;n de Tarjetas Expiradas<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last">
    <ul id="alerts" class="alerts"></ul>
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last ">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
		{if $error eq 1}
			Se ha enviado la solicitud de modificaci&oacute;n exitosamente.
		{elseif $error eq 2}
			Hubo errores en el ingreso del historial de tarjeta.
		{elseif $error eq 3}
			Hubo errores en la creaci&oacute;n de la alerta.
		{elseif $error eq 4}
			Hubo errores al enviar el e-mail de la solicitud de reactivaci&oacute;n.
		{/if}
	</div>
</div>
{/if}

<div class="span-24 last line">
  <div class="prepend-3 span-8 label">N&uacute;mero de tarjeta:</div>
  <div class="span-11 append-2 last">
      <input type="text" name="txtCardNo" id="txtCardNo" title="El campo de b&uacute;squeda es obligatorio." />
  </div>
</div>

<div class="span-24 last buttons line">
    <input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
</div>

<div class="prepend-2 span-20 last line" id="cardInformation"></div>