{assign var="title" value="DESPACHAR SOLICITUDES"}
<div class="span-24 last title">
	 Despachar Solicitudes de Bloqueo de Tarjeta<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmDispatchApplications" id="frmDispatchApplications" action="{$document_root}modules/sales/modifications/pDispatchBlockedApplications" method="post">
<div class="span-24 last line separator">
   <label>Datos de la Solicitud</label>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">N&uacute;mero de Tarjeta:</div>
	<div class="span-5">
		{$sale.card_number_sal}
	</div>

	<div class="span-3 label">* Solicitante: </div>
	<div class="span-4 append-4 last">
		<select name="selStatus" id="selStatus" title="El campo 'Solicitud' es obligatorio.">
			<option value="">- Seleccione -</option>
			{foreach from=$sLogStatus item="sl"}
				{if $sl neq 'PENDING'}
				<option value="{$sl}">{$global_sales_log_status.$sl}</option>
				{/if}
			{/foreach}
                </select>
	</div>
        &nbsp;
        <div class="span-24 last line separator">
		<label>Datos de la Tarjeta</label>
	</div>

	<div class="span-24 last line">
                <div class="prepend-7 span-4 label">Estado:</div>
                    <div class="span-3">{$global_salesStatus[$sale.status_sal]}</div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-6 span-3 label">Inicio de Cobertura:</div>
            <div class="span-3">{$sale.begin_date_sal}</div>
            <div class="span-3 label">Fin de Cobertura:</div>
            <div class="span-3 last">{$sale.end_date_sal}</div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-5 span-4 label">Nombre del Producto:</div>
            <div class="span-3">{$sale.aux.product}</div>
            <div class="span-3 label">Nombre del Cliente:</div>
            <div class="span-3 last">{$customer.first_name_cus}&nbsp;{$customer.last_name_cus}</div>
        </div>

        {if $logs}
            <div class="span-24 last line separator">
                <label>Historial de Cambios</label>
            </div>
            <div class="span-24 last line">
                <table border="1" id="salesLogTable" class="salesLogTable">
                      <tr class="header">
                        <th>NUM. CAMBIO</th>
                        <th>FECHA</th>
                        <th>CAMBIO</th>
                        <th>USUARIO</th>
                        <th>ESTADO</th>
						<th>COMENTARIOS</th>
                      </tr>
                        {counter start=0 skip=1 print=false}
                        {foreach name="alert" from=$logs item=curr}
                        <tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td>{counter}</td>
                            <td>{$curr.date_slg}</td>
                            <td>{$global_salesStatus[$curr.description_slg.old_status_sal]} ==> {$global_salesStatus[$curr.description_slg.new_status_sal]}</td>
                            <td>{$curr.first_name_usr}&nbsp;{$curr.last_name_usr}</td>
                            <td>{$global_sales_log_status[$curr.status_slg]}</td>
							<td>{$curr.description_slg.observations}</td>
                        </tr>
                        {/foreach}
                </table>
             </div>
             <div class="span-24 last buttons" id="salesLogNav"></div>
        {/if}
</div>

	<input type="hidden" name="hdnSerial_slg" id="hdnSerial_slg" value="{$sLog.serial_slg}" />

<div class="span-24 last buttons">
	<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar" />
</div>
</form>