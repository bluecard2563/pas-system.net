{assign var="title" value="SOLICITUDES DE MODIFICACI&Oacute;N DE VENTA"}
<div class="span-24 last title">
	Solicitudes Pendientes de Modificaci&oacute;n de Venta<br>
	  <label>(*) Campos Obligatorios</label>
</div>
{if $error}
	<div class="append-7 span-10 prepend-7 last">
		<div class="span-10 {if $error eq 1 || $error eq 2}success{else}error{/if}" align="center">
			{if $error eq 1}
				La venta ha sido modificada existosamente
			{elseif $error eq 2}
				La solicitud de venta ha sido negada exitosamente.
			{elseif $error eq 3}
				Hubo un error en la negaci&oacute;n de la solicitud de venta.<br>
				Por favor comun&iacute;quese con el adminsitrador.
			{elseif $error eq 4}
				Hubo un error al actualizar el estado de la modificaci&oacute;n de la solicitud de venta.<br>
				Por favor comun&iacute;quese con el adminsitrador.
			{elseif $error eq 5}
				Hubo un error al actualizar los datos modificados en la venta.<br>
				Por favor comun&iacute;quese con el adminsitrador.
			{elseif $error eq 6}
				Hubo un error al atender la alerta.<br>
				Por favor comun&iacute;quese con el adminsitrador.
			{/if}
		</div>
	</div>
{/if}