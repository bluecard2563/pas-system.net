{assign var="title" value="BUSCAR TARJETA PARA MODIFICACI&Oacute;N"}
<form name="frmSearchCard" id="frmSearchCard" method="post" action="{$document_root}modules/sales/modifications/fEditSale" class="">

    <div class="span-24 last title">
        Buscar Tarjeta para Modificaci&oacute;n<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
        <div class="span-7 span-11 prepend-7  last" align="center">
            {if $error eq 1}
                <div class="span-9 error" align="center">
                    No existe una venta para ese n&uacute;mero de tarjeta.
                </div>
            {elseif $error eq 2}
                <div class="span-9 error" align="center">
                    La tarjeta ingresada es Corparativa o Ejecutiva y ya tiene viajes registrados.<br/>
                    Comun&iacute;quese con el administrador.
                </div>
            {elseif $error eq 3}
                <div class="span-10 error" align="center">
                    Su comercializador actualmente no tiene asignado una cartera de productos. <br>
                    Por favor comun&iacute;quese con el administrador.
                </div>
            {elseif $error eq 4}
                <div class="span-10 error" align="center">
                    No se puede modificar esta tarjeta porque tiene una solicitud pendiente.
                </div>
            {elseif $error eq 5}
                <div class="span-10 error" align="center">
                    No se puede modificar esta tarjeta porque la solicitud a&uacute;n no ha sido aprobada.
                </div>
            {elseif $error eq 6}
                <div class="span-10 error" align="center">
                    No se puede modificar esta tarjeta.<br/>
                    Comun&iacute;quese con el administrador para verificar el estado de la misma.
                </div>
            {elseif $error eq 7}
                <div class="span-10 error" align="center">
                    No se puede modificar esta tarjeta porque ya se encuentra en tiempo de vigencia.<br/>
                    Comun&iacute;quese con el administrador.
                </div>
            {elseif $error eq 8}
                <div class="span-10 error" align="center">
                    No se puede modificar esta tarjeta porque es del tipo masiva.<br/>
                    Comun&iacute;quese con el administrador.
                </div>
            {elseif $error eq 9}
                <div class="span-10 notice" align="center">
                    No se puede modificar esta tarjeta porque se encuentra en STAND-BY. Act&iacute;vela primero antes de continuar.
                </div>
            {elseif $error eq 10}
                <div class="span-8 error center">
                    La tarjeta no puede ser revisada ya que corresponde a otro canal de distribuci&oacute;n.
                </div>
            {/if}
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Datos de la Tarjeta</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-5 label">
            * N&uacute;mero de Tarjeta:
        </div>
        <div class="span-5 appen-8 last">
            <input type="text" id="txtCardNumber" name="txtCardNumber" title="El campo 'N&uacute;mero de Tarjeta es obligatorio.'"/>
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
</form>


