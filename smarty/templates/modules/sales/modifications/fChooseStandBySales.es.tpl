{assign var="title" value="SOLICITUDES STAND-BY"}
<div class="span-24 last title">
	 Solicitudes de Cambio a Stand-by<br />
</div>

{if $error}
<div class="span-7 span-10 prepend-7 last">
	<div class="span-10 {if $error eq 1}success{else}error{/if}">
		{if $error eq 1}
			Se ha realizado el cambio exitosamente.
		{elseif $error eq 2}
			Se ha autorizado el cambio. Sin embargo, hubo problemas al desactivar la alarma.
		{elseif $error eq 3}
			Hubo errores al actualizar la solicitud de cambio. La venta no ha sido afectada.
		{elseif $error eq 4}
			La tarjeta no se ha podido cambiar a Stand-By. Comun&iacute;quese con el adminsitrador
		{/if}
	</div>
</div>
{/if}

{if $standByRequest}
<div class="span-24 last line">
	<table border="0" id="tblStandByRequest" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					No. Tarjeta
				</td>
				<td align="center" class="tableTitle">
					Solicitante
				</td>
				<td align="center" class="tableTitle">
					Pa&iacute;s de la Venta
				</td>
				<td align="center"  class="tableTitle">
					Sucursal de Comercializador
				</td>
				<td align="center" class="tableTitle">
					Fecha de Solicitud
				</td>
				<td align="center" class="tableTitle">
					Revisar
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="alert" from=$standByRequest item="s"}
			<tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.alert.iteration}
				</td>
				<td align="center" class="tableField">
					{$s.card_number_sal}
				</td>
				<td align="center" class="tableField">
					{$s.applicant}
				</td>
				<td align="center" class="tableField">
					{$s.name_cou}
				</td>
				<td align="center" class="tableField">
					{$s.name_dea}
				</td>
				<td align="center" class="tableField">
					{$s.date_slg}
				</td>
				<td align="center" class="tableField">
					<a href="{$document_root}modules/sales/modifications/fDispatchStandByApplications/{$s.serial_slg}">Revisar</a>
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last buttons" id="pageNavPosition"></div>
{else}
<div class="span-7 span-10 prepend-7 last">
	<div class="span-10 error">
		No existen solicitudes pendientes.
	</div>
</div>
{/if}