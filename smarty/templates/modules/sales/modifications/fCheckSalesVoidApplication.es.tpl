{assign var="title" value="SOLICITUD DE MODIFICACI&Oacute;N DE  VENTA"}
<form name="frmCheckSalesApplication" id="frmCheckSalesApplication" method="post" action="{$document_root}modules/sales/modifications/pCheckSalesApplication" class="">
    <div class="span-24 last title">
		Solicitud de Modificaci&oacute;n de Venta<br />
        <label>(*) Campos Obligatorios</label>
    </div>
 <div class="wizard-nav  prepend-3 span-18 append-3 last">
		<a href="#FirstPage"><div class="span-6">1. Venta Original</div></a>
        <a href="#SecondPage"><div class="span-6">2. Modificaciones en la Venta</div></a>
		<a href="#ThirdPage"><div class="span-6 last">3. Aceptar la Solicitud</div></a>
</div>

   <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
<div id="FirstPage" class="wizardpage span-24 last">
	<div class="span-24 last line separator">
       <label>Informaci&oacute;n de la Venta</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tipo de Venta:</div>
        <div class="span-3">
            {if $saleOld.stock_type_sal=='VIRTUAL'}Virtual
			{elseif $saleOld.stock_type_sal=='MANUAL'}Manual
			{/if}
        </div>
    </div>

     <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* No de Tarjeta:</div>
        <div class="span-5">
			{if $saleOld.card_number_sal}{$saleOld.card_number_sal}{/if}
        </div>

        <div class="span-3 label">C&oacute;digo Vendedor:</div>
        <div class="span-4 append-4 last">
			{if $sellerCode}{$sellerCode}{/if}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
        <div class="span-5">
            {if $data.emissionPlace}{$data.emissionPlace}{/if}
        </div>

        <div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            {if $saleOld.emission_date_sal}{$saleOld.emission_date_sal}{/if}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Asesor de Venta:</div>
        <div class="span-5">
            {if $data.counterName}{$data.counterName}{/if}
        </div>

        <div class="span-3 label">* Comercializador:</div>
        <div class="span-4 append-4 last">
             {if $data.name_dea}{$data.name_dea}{/if}
        </div>
    </div>

   {if $sale.free_sal eq 'YES'}
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Es Free:</div>
        <div class="span-5">
            S&iacute;
        </div>
    </div>
   {/if}

   <div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
        <div class="prepend-4 span-4 label">* Moneda:</div>
        <div class="span-5">
			{foreach from=$data.currencies item="l" key="k"}
            <input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur}<br/>
			{/foreach}
        </div>
    </div>

<div class="span-24 last" id="customerContainer">
	<div class="span-24 last line separator">
            <label>Datos del Cliente</label>
    </div>
	{if $customer_old.vip_cus eq 1}
        <div class="span-24 last line">
            <div class="prepend-11 span-4 label">Es cliente VIP</div>
        </div>

        {/if}
		{if $customer_old.status_cus eq 'INACTIVE'}
        <div class="append-7 span-10 prepend-7 last line">
            <div class=" span-10 error">NOTA: El Cliente se encuentra en estado Inactivo.</div>
        </div>
        {/if}
	<div class="span-24 last line">
	<div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
            <div class="span-5">

                 {if $customer_old.document_cus}{$customer_old.document_cus}{/if}
            </div>
            <div class="span-3 label">*Tipo de Cliente</div>
            <div class="span-4 append-2 last">
               {if $customer_old.type_cus eq 'PERSON'}Persona Natural{elseif $customer_old.type_cus eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}
            </div>
	</div>
	{if $customer_old.type_cus eq 'PERSON'}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre:</div>
            <div class="span-5">
                {if $customer_old.first_name_cus}{$customer_old.first_name_cus}{/if}
            </div>

            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-4 last">
                {if $customer_old.last_name_cus}{$customer_old.last_name_cus}{/if}
            </div>
        </div>
	{elseif $customer_old.type_cus eq 'LEGAL_ENTITY'}
		<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Raz&oacute;n Social:</div>
        <div class="span-5">
            {if $customer_old.first_name_cus}{$customer_old.first_name_cus}{/if}
        </div>
        </div>
	{/if}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
            <div class="span-5">
                    {foreach from=$countryList item="l"}
                         {if $customer.auxData.serial_cou eq $l.serial_cou}{$l.name_cou}{/if}
                    {/foreach}

            </div>

            <div class="span-3 label">* Ciudad</div>
            <div class="span-4 append-4 last" id="cityContainerCustomer">
                    {foreach from=$cityListCus_old item="l"}
						{if $customer_old.serial_cit eq $l.serial_cit}{$l.name_cit}{/if}
                    {/foreach}
             </div>
        </div>
{if $customer_old.type_cus eq 'PERSON'}
        <div class="span-24 last line" id="divAddress1">
            <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                {if $customer_old.birthdate_cus}{$customer_old.birthdate_cus}{/if}
            </div>

            <div class="span-3 label">* Direcci&oacute;n:</div>
            <div class="span-4 append-4 last">
                {if $customer_old.address_cus}{$customer_old.address_cus}{/if}
            </div>
        </div>
{elseif $customer_old.type_cus eq 'LEGAL_ENTITY'}
		<div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
            <div class="span-5">
               {if $customer_old.address_cus}{$customer_old.address_cus}{/if}
            </div>
        </div>
{/if}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
            <div class="span-5">
                {if $customer_old.phone1_cus}{$customer_old.phone1_cus}{/if}
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                {if $customer_old.phone2_cus}{$customer_old.phone2_cus}{/if}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Celular:</div>
            <div class="span-5">
                {if $customer_old.cellphone_cus}{$customer_old.cellphone_cus}{else}&nbsp;{/if}
            </div>

            <div class="span-3 label">* Email:</div>
            <div class="span-4 append-4 last">
                {if $customer_old.email_cus}{$customer_old.email_cus}{/if}
            </div>
        </div>
{if $customer_old.type_cus eq 'PERSON'}
        <div class="span-24  last line">
            <div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
            <div class="span-5">
                     {if $customer_old.relative_cus}{$customer_old.relative_cus}{/if}
            </div>

            <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
            <div class="span-4 append-4 last">
				{if $customer_old.relative_phone_cus}{$customer_old.relative_phone_cus}{/if}
			</div>
        </div>
{elseif $customer_old.type_cus eq 'LEGAL_ENTITY'}
		<div class="span-24  last line">
            <div class="prepend-4 span-4 label">*Gerente:</div>
            <div class="span-5">
                     {if $customer_old.relative_cus}{$customer_old.relative_cus}{/if}
            </div>

            <div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
            <div class="span-4 append-4 last">
				{if $customer_old.relative_phone_cus}{$customer_old.relative_phone_cus}{/if}
			</div>
        </div>
{/if}

</div>
    <div class="span-24 last line separator">
        <label>Datos del Viaje</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="depDate">* Fecha de Salida:</div>
				<div class="prepend-4 span-4 label" style="display:none" id="startCoverage">* Inicio de Cobertura:</div>
        <div class="span-5">
           {if $saleOld.begin_date_sal}{$saleOld.begin_date_sal}{/if}
        </div>

        <div class="span-3 label" id="arrDate">* Fecha de Retorno:</div>
				<div class="span-3 label" style="display:none" id="endCoverage">* Fin de Cobertura:</div>
        <div class="span-5 append-3 last">
            {if {$saleOld.end_date_sal}{$saleOld.end_date_sal}{/if}
        </div>
    </div>

    <div class="span-24 last line" id="divDestination">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer">* Pa&iacute;s de Destino:</div>
        <div class="span-5">
                {foreach from=$countryList item="l"}
                    {if $saleOld.serial_cou eq $l.serial_cou}{$l.name_cou}{/if}
                {/foreach}
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
				{foreach from=$cityListOld item="l"}
                    {if $saleOld.serial_cit eq $l.serial_cit}{$l.name_cit}{/if}
                {/foreach}
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label"># de D&iacute;as:</div>
         <div class="span-5">
             <label id="txtDaysBetween">{$saleOld.days_sal}</label>
         </div>
         <input type="hidden" name="hddDays" id="hddDays" value="{$sale.days_sal}"/>
    </div>

    <div class="span-24 last line separator">
        <label>Datos del Producto</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Producto:</div>
        <div class="span-5" id="productsContainer">
			{foreach from=$productListByDealer item="l"}
				{if $saleOld.serial_pbd eq $l.serial_pbd}{$l.name_pbl}{/if}
        	{/foreach}
        </div>
		<input type="hidden" name="hdnProduct" id="hdnProduct" value="{$sale.serial_pbd}"/>
        <div class="span-3 label">Precio:</div>
        <div class="span-4 append-4 last line" id="priceContainer">
			USD {$saleOld.fee_sal}
		</div>
    </div>


    <div class="span-24 last line">
        <div class="span-13 label" id="rangeSelect"></div>
        <div class="span-11 label last" id="rangePricesContainer"></div>
    </div>
  {if $extraArray_old}
   <div class="span-24 last line">


	   <div class="span-24 last line" id="extrasContainer">
		   <div class="span-24 last line separator" id="extasTitle">
				<label>Extras</label>
			</div>
		  {foreach from=$extraArray_old item="l" key="e"}
				<div id="extra_{$e}" class="span-24 last line">
					<div class="span-24 last line">
							<div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
							<div class="span-5">
								{if $l.document_cus}{$l.document_cus}{/if}
								<input type="hidden" name="extra[{$e}][serialCus]" id="serialCus_{$e}"  {if $l.serial_cus}value="{$l.serial_cus}"{/if}/>
							</div>
							<div class="span-3 label">* Tipo de Relaci&oacute;n con el titular:</div>
							<div class="span-4 append-4 last">
								{foreach from=$relationshipTypeList item="r"}
									{if $l.relationship_ext eq $r}{$global_extraRelationshipType.$r}{/if}
								{/foreach}
							</div>

					</div>

					 <div class="span-24 last line" >
							<div class="prepend-4 span-4 label">* Nombre:</div>
							<div class="span-5">
								{if $l.first_name_cus}{$l.first_name_cus}{/if}
							</div>

							<div class="span-3 label">* Apellido:</div>
							<div class="span-4 append-4 last">
								 {if $l.last_name_cus}{$l.last_name_cus}{/if}
							</div>
						</div>

					 <div class="span-24 last line">
							<div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
							<div class="span-5">
								{if $l.birthdate_cus}{$l.birthdate_cus}{/if}
							</div>

							 <div class="span-3 label">Email:</div>
							<div class="span-4 append-4 last">
								{if $l.email_cus}{$l.email_cus}{/if}
							</div>
						</div>

					<div class="span-24 last line">
							<div class="prepend-4 span-4 label">* Tarifa:</div>
							<div class="span-5" id="extraPriceContainer_{$e}">{if $l.fee_ext}USD {$l.fee_ext}{/if}</div>
							<input type="hidden" name="extra[{$e}][hdnPriceExtra]" id="hdnPriceExtra_{$e}" value="{$l.fee_ext}"/>
							<input type="hidden" name="extra[{$e}][hdnCostExtra]" id="hdnCostExtra_{$e}" value="{$l.cost_ext}"/>
					</div>

					<input type="hidden" name="percentage_spouse_{$e}" id="percentage_spouse_{$e}" {if $percentages.percentage_spouse_pxc}value="{$percentages.percentage_spouse_pxc}"{/if} />
					<input type="hidden" name="percentage_extras_{$e}" id="percentage_extras_{$e}" {if $percentages.percentage_extras_pxc}value="{$percentages.percentage_extras_pxc}"{/if}  />
					<input type="hidden" name="percentage_children_{$e}" id="percentage_children_{$e}" {if $percentages.percentage_children_pxc}value="{$percentages.percentage_children_pxc}"{/if} />


					<input type="hidden" name="hdnSerial_cusExtra" id="hdnSerial_cusExtra" {if $l.serial_cus}value="{$l.serial_cus}"{/if}/>
					<div class="span-24 last line separatorExtra"></div>
				</div>
		   {/foreach}
	   </div>
   </div>

    <input type="hidden" name="count" id="count" value="{$sizeExtras}"/>

{/if}

{if $sbcbs_old}
    <div class="span-24 line"  id="servicesContainer">
	{if $servicesListByDealer}
        <div class="span-24 last line separator">
            <label>Datos del Servicio</label>
        </div>
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Servicio:</div>
            <div class="span-5">
                {foreach from=$servicesListByDealer item="l"}
					{foreach from=$sbcbs_old item="s"}{if $l.serial_sbd eq $s.serial_sbd}{$l.name_sbl}<br>{/if}{/foreach}
                {/foreach}
            </div>

            <div class="span-3 label">Precio:</div>
            <div class="span-4 append-4 last">
                <label id="servicePriceContainer" style="font-weight: normal;">
					{foreach from=$servicesListByDealer item="l"}
					{foreach from=$sbcbs_old item="s"}{if $l.serial_sbd eq $s.serial_sbd}{$l.price_sbc}<br>{/if}{/foreach}
					{/foreach}
				</label>
                <input type="hidden" name="hdnServicePrice" id="hdnServicePrice" value="">
                <input type="hidden" name="hdnServiceCost" id="hdnServiceCost" value="">
            </div>
        </div>
	 {/if}
    </div>
{/if}

    <div class="span-24 last line separator">
        <label>Total de la Venta</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-1">
            <label>Total:</label>
        </div>
        <div class="span-4 append-7 last " id="totalContainer">USD {$saleOld.total_sal}</div>
    </div>

    <div class="span-24 last line separator">
        <label>Observaciones</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7">
            {if $saleOld.observations_sal}{$data.counterName}: {$saleOld.observations_sal}{/if}
        </div>
    </div>
</div>
<div id="SecondPage" class="wizardpage span-24 last">
	 <div class="span-24 last line separator">
       <label>Informaci&oacute;n de la Venta</label>
    </div>
   {if $sale.modify eq 'NO'}<input type="hidden" name="hdnModify" id="hdnModify" value="{$sale.modify}" />{/if}
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tipo de Venta:</div>
        <div class="span-3">
            {if $sale.stock_type_sal=='VIRTUAL'}Virtual
			{elseif $sale.stock_type_sal=='MANUAL'}Manual
			{/if}
        </div>
    </div>

     <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* No de Tarjeta:</div>
        <div class="span-5">
			{if $sale.card_number_sal}{$sale.card_number_sal}{/if}
        </div>

        <div class="span-3 label">C&oacute;digo Vendedor:</div>
        <div class="span-4 append-4 last">
			{if $sellerCode}{$sellerCode}{/if}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
        <div class="span-5">
            {if $data.emissionPlace}{$data.emissionPlace}{/if}
        </div>

        <div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            {if $sale.emission_date_sal}{$sale.emission_date_sal}{/if}
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Asesor de Venta:</div>
        <div class="span-5">
            {if $data.counterName}{$data.counterName}{/if}
        </div>

        <div class="span-3 label">* Comercializador:</div>
        <div class="span-4 append-4 last">
             {if $data.name_dea}{$data.name_dea}{/if}
        </div>
    </div>

   {if $sale.free_sal eq 'YES'}
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Es Free:</div>
        <div class="span-5">
            S&iacute;
        </div>
    </div>
   {/if}

   <div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
        <div class="prepend-4 span-4 label">* Moneda:</div>
        <div class="span-5">
			{foreach from=$data.currencies item="l" key="k"}
            <input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur}<br/>
			{/foreach}
        </div>
    </div>

<div class="span-24 last" id="customerContainer">
	<div class="span-24 last line separator">
            <label>Datos del Cliente</label>
    </div>
	{if $customer.vip_cus eq 1}
        <div class="span-24 last line">
            <div class="prepend-11 span-4 label">Es cliente VIP</div>
        </div>

        {/if}
		{if $customer.status_cus eq 'INACTIVE'}
        <div class="append-7 span-10 prepend-7 last line">
            <div class=" span-10 error">NOTA: El Cliente se encuentra en estado Inactivo.</div>
        </div>
        {/if}
	<div class="span-24 last line">
	<div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
            <div class="span-5">

                 {if $customer.document_cus}{$customer.document_cus}{/if}
                 <input type="hidden" name="serialCus" id="serialCus"  {if $data.serial_cus}value="{$data.serial_cus}"{/if}/>
            </div>
            <div class="span-3 label">*Tipo de Cliente</div>
            <div class="span-4 append-2 last">
               {if $customer.type_cus eq 'PERSON'}Persona Natural{elseif $customer.type_cus eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}
            </div>
	</div>
	{if $customer.type_cus eq 'PERSON'}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Nombre:</div>
            <div class="span-5">
                {if $customer.first_name_cus}{$customer.first_name_cus}{/if}
            </div>

            <div class="span-3 label">* Apellido:</div>
            <div class="span-4 append-4 last">
                {if $customer.last_name_cus}{$customer.last_name_cus}{/if}
            </div>
        </div>
	{elseif $customer.type_cus eq 'LEGAL_ENTITY'}
		<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Raz&oacute;n Social:</div>
        <div class="span-5">
            {if $customer.first_name_cus}{$customer.first_name_cus}{/if}
        </div>
        </div>
	{/if}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
            <div class="span-5">
                    {foreach from=$countryList item="l"}
                         {if $customer.auxData.serial_cou eq $l.serial_cou}{$l.name_cou}{/if}
                    {/foreach}

            </div>

            <div class="span-3 label">* Ciudad</div>
            <div class="span-4 append-4 last" id="cityContainerCustomer">
                    {foreach from=$cityListCus item="l"}
						{if $customer.serial_cit eq $l.serial_cit}{$l.name_cit}{/if}
                    {/foreach}
             </div>
        </div>
{if $customer.type_cus eq 'PERSON'}
        <div class="span-24 last line" id="divAddress1">
            <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
            <div class="span-5">
                {if $customer.birthdate_cus}{$customer.birthdate_cus}{/if}
            </div>

            <div class="span-3 label">* Direcci&oacute;n:</div>
            <div class="span-4 append-4 last">
                {if $customer.address_cus}{$customer.address_cus}{/if}
            </div>
        </div>
{elseif $customer.type_cus eq 'LEGAL_ENTITY'}
		<div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
            <div class="span-5">
               {if $customer.address_cus}{$customer.address_cus}{/if}
            </div>
        </div>
{/if}
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
            <div class="span-5">
                {if $customer.phone1_cus}{$customer.phone1_cus}{/if}
            </div>

            <div class="span-3 label">Tel&eacute;fono 2:</div>
            <div class="span-4 append-4 last">
                {if $customer.phone2_cus}{$customer.phone2_cus}{/if}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">Celular:</div>
            <div class="span-5">
                {if $customer.cellphone_cus}{$customer.cellphone_cus}{else}&nbsp;{/if}
            </div>

            <div class="span-3 label">* Email:</div>
            <div class="span-4 append-4 last">
                {if $customer.email_cus}{$customer.email_cus}{/if}
            </div>
        </div>
{if $customer.type_cus eq 'PERSON'}
        <div class="span-24  last line">
            <div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
            <div class="span-5">
                     {if $customer.relative_cus}{$customer.relative_cus}{/if}
            </div>

            <div class="span-3 label">*Tel&eacute;fono del contacto:</div>
            <div class="span-4 append-4 last">
				{if $customer.relative_phone_cus}{$customer.relative_phone_cus}{/if}
			</div>
        </div>
{elseif $customer.type_cus eq 'LEGAL_ENTITY'}
		<div class="span-24  last line">
            <div class="prepend-4 span-4 label">*Gerente:</div>
            <div class="span-5">
                     {if $customer.relative_cus}{$customer.relative_cus}{/if}
            </div>

            <div class="span-3 label">*Tel&eacute;fono del Gerente:</div>
            <div class="span-4 append-4 last">
				{if $customer.relative_phone_cus}{$customer.relative_phone_cus}{/if}
			</div>
        </div>
{/if}

</div>
    <div class="span-24 last line separator">
        <label>Datos del Viaje</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="depDate">* Fecha de Salida:</div>
				<div class="prepend-4 span-4 label" style="display:none" id="startCoverage">* Inicio de Cobertura:</div>
        <div class="span-5">
           {if $sale.begin_date_sal}{$sale.begin_date_sal}{/if}
        </div>

        <div class="span-3 label" id="arrDate">* Fecha de Retorno:</div>
				<div class="span-3 label" style="display:none" id="endCoverage">* Fin de Cobertura:</div>
        <div class="span-5 append-3 last">
            {if {$sale.end_date_sal}{$sale.end_date_sal}{/if}
        </div>
    </div>

    <div class="span-24 last line" id="divDestination">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer">* Pa&iacute;s de Destino:</div>
        <div class="span-5">
                {foreach from=$countryList item="l"}
                    {if $sale.serial_cou eq $l.serial_cou}{$l.name_cou}{/if}
                {/foreach}
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
				{foreach from=$cityList item="l"}
                    {if $sale.serial_cit eq $l.serial_cit}{$l.name_cit}{/if}
                {/foreach}
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label"># de D&iacute;as:</div>
         <div class="span-5">
             <label id="txtDaysBetween">{$sale.days_sal}</label>
         </div>
         <input type="hidden" name="hddDays" id="hddDays" value="{$sale.days_sal}"/>
    </div>

    <div class="span-24 last line separator">
        <label>Datos del Producto</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Producto:</div>
        <div class="span-5" id="productsContainer">
			{foreach from=$productListByDealer item="l"}
				{if $sale.serial_pbd eq $l.serial_pbd}{$l.name_pbl}{/if}
        	{/foreach}
        </div>
		<input type="hidden" name="hdnProduct" id="hdnProduct" value="{$sale.serial_pbd}"/>
        <div class="span-3 label">Precio:</div>
        <div class="span-4 append-4 last line" id="priceContainer">
			USD {$sale.fee_sal}
		</div>
    </div>


    <div class="span-24 last line">
        <div class="span-13 label" id="rangeSelect"></div>
        <div class="span-11 label last" id="rangePricesContainer"></div>
    </div>
{if $extraArray && $extraArray.0.idExtra neq ''}
   <div class="span-24 last line">
	   <div class="span-24 last line" id="extrasContainer">
		   <div class="span-24 last line separator" id="extasTitle">
				<label>Extras</label>
			</div>
		  {foreach from=$extraArray item="l" key="e"}
				<div id="extra_{$e}" class="span-24 last line">
					<div class="span-24 last line">
							<div class="prepend-4 span-4 label ">* Identificaci&oacute;n:</div>
							<div class="span-5">
								{if $l.idExtra}{$l.idExtra}{/if}
								<input type="hidden" name="extra[{$e}][serialCus]" id="serialCus_{$e}"  {if $l.serialCus}value="{$l.serialCus}"{/if}/>
							</div>
							<div class="span-3 label">* Tipo de Relaci&oacute;n con el titular:</div>
							<div class="span-4 append-4 last">
								{foreach from=$relationshipTypeList item="r"}
									{if $l.selRelationship eq $r}{$global_extraRelationshipType.$r}{/if}
								{/foreach}
							</div>

					</div>

					 <div class="span-24 last line" >
							<div class="prepend-4 span-4 label">* Nombre:</div>
							<div class="span-5">
								{if $l.nameExtra}{$l.nameExtra}{/if}
							</div>

							<div class="span-3 label">* Apellido:</div>
							<div class="span-4 append-4 last">
								 {if $l.lastnameExtra}{$l.lastnameExtra}{/if}
							</div>
						</div>

					 <div class="span-24 last line">
							<div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
							<div class="span-5">
								{if $l.birthdayExtra}{$l.birthdayExtra}{/if}
							</div>

							 <div class="span-3 label">Email:</div>
							<div class="span-4 append-4 last">
								{if $l.emailExtra}{$l.emailExtra}{/if}
							</div>
						</div>

					<div class="span-24 last line">
							<div class="prepend-4 span-4 label">* Tarifa:</div>
							<div class="span-5" id="extraPriceContainer_{$e}">{if $l.hdnPriceExtra}USD {$l.hdnPriceExtra}{/if}</div>
							<input type="hidden" name="extra[{$e}][hdnPriceExtra]" id="hdnPriceExtra_{$e}" value="{$l.hdnPriceExtra}"/>
							<input type="hidden" name="extra[{$e}][hdnCostExtra]" id="hdnCostExtra_{$e}" value="{$l.hdnCostExtra}"/>
					</div>

					<input type="hidden" name="percentage_spouse_{$e}" id="percentage_spouse_{$e}" {if $percentages.percentage_spouse_pxc}value="{$percentages.percentage_spouse_pxc}"{/if} />
					<input type="hidden" name="percentage_extras_{$e}" id="percentage_extras_{$e}" {if $percentages.percentage_extras_pxc}value="{$percentages.percentage_extras_pxc}"{/if}  />
					<input type="hidden" name="percentage_children_{$e}" id="percentage_children_{$e}" {if $percentages.percentage_children_pxc}value="{$percentages.percentage_children_pxc}"{/if} />


					<input type="hidden" name="hdnSerial_cusExtra" id="hdnSerial_cusExtra" {if $l.serialCus}value="{$l.serialCus}"{/if}/>
					<div class="span-24 last line separatorExtra"></div>
				</div>
		   {/foreach}
	   </div>
   </div>

    <input type="hidden" name="count" id="count" value="{$sizeExtras}"/>

{/if}
{if $sale.Services neq ''}
    <div class="span-24 line"  id="servicesContainer">

	{if $servicesListByDealer}
        <div class="span-24 last line separator">
            <label>Datos del Servicio</label>
        </div>
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Servicio:</div>
            <div class="span-5">
                {foreach from=$servicesListByDealer item="l"}
					{foreach from=$sale.Services item="s"}{if $l.serial_sbd eq $s.serial_sbd}{$l.name_sbl}<br>{/if}{/foreach}
                {/foreach}
            </div>

            <div class="span-3 label">Precio:</div>
            <div class="span-4 append-4 last">
                <label id="servicePriceContainer" style="font-weight: normal;">
					{foreach from=$servicesListByDealer item="l"}
					{foreach from=$sale.Services item="s"}{if $l.serial_sbd eq $s.serial_sbd}{$l.price_sbc}<br>{/if}{/foreach}
					{/foreach}
				</label>
                <input type="hidden" name="hdnServicePrice" id="hdnServicePrice" value="">
                <input type="hidden" name="hdnServiceCost" id="hdnServiceCost" value="">
            </div>
        </div>
	 {/if}
    </div>
{/if}

    <div class="span-24 last line separator">
        <label>Total de la Venta</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-1">
            <label>Total:</label>
        </div>
        <div class="span-4 append-7 last " id="totalContainer">USD {$sale.total_sal}</div>
    </div>

    <div class="span-24 last line separator">
        <label>Observaciones</label>
    </div>
    <div class="span-24 last line">
		<div class="span-24 last">
			<div class="prepend-4 span-5 label line">
				Observaciones de la Venta Original:
			</div>
			<div class="prepend-7 line">
				{if $saleOld.observations_sal}{$data.counterName}: {$saleOld.observations_sal}{/if}
			</div>
		</div>
		<div class="span-24 last">
			<div class="span-24 last line separatorExtra"></div>
			<div class="prepend-4 span-5 label">
				Observaciones de la Modificaci&oacute;n:
			</div>			
			<div class="prepend-7">
			   {if $sale.observations_sal}{$sale.modifier}: {$sale.observations_sal}{/if}
			</div>
		</div>
    </div>
			
</div>
<div id="ThirdPage" class="wizardpage span-24 last">
	<div class="span-24 last line separator">
        <label>Historial de Solicitudes</label>
    </div>
	<div class="span-24 last">
				<div class="prepend-2 span-20 append-2 last line">
					<table border="0" id="tblModifyApplications" style="color: #000000;">
						<THEAD>
							<tr bgcolor="#284787">
								<td align="center" class="tableTitle">
									No.
								</td>
								<td align="center" class="tableTitle">
									Fecha
								</td>
								<td align="center" class="tableTitle">
									Solicitante
								</td>
								<td align="center" class="tableTitle">
									Observaciones
								</td>
								<td align="center"  class="tableTitle">
									Estado
								</td>
							</tr>
						</THEAD>
						<TBODY>
							{foreach name="modHist" from=$history item="h"}
							<tr {if $smarty.foreach.modHist.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
								<td align="center" class="tableField">
									{$smarty.foreach.modHist.iteration}
								</td>
								<td align="center" class="tableField">
									{$h.modDate}
								</td>
								<td align="center" class="tableField">
									{$h.applicant}
								</td>
								<td align="center" class="tableField">
									{$h.obs}
								</td>
								<td align="center" class="tableField">
									{if $h.status eq 'DENIED'}Negada
									{elseif $h.status eq 'AUTHORIZED'} Autorizada
									{elseif $h.status eq 'PENDING'}Pendiente
									{/if}
								</td>
							</tr>
							{/foreach}
						</TBODY>
					</table>
				</div>
				<div class="span-24 last buttons" id="pageNavPosition"></div>
		</div>
	<div class="span-24 last line separator">
        <label>Observaciones de la Autorizaci&oacute;n</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7">
			<textarea name="authorizeObs" id="authorizeObs" title='El campo "Observaciones de la Autorizaci&oacute;n" es obligatorio'></textarea>
        </div>
    </div>
	<div class="span-24 last line separator">
        <label>Autorizaci&oacute;n</label>
    </div>

    <div class="span-24 last line">
		<div class="prepend-9 span-3 last">
            <label>Autorizar:</label>
        </div>
		<div class="span-6 append-3">
			<select name="selAuthorize" id="selAuthorize" title='El campo "Autorizar" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$sLogStatus key=key item="l"}
					<option value="{$l}">{$global_sales_log_status.$l}</option>
				{/foreach}
			</select>
		</div>
    </div>
</div>
<input type="hidden" name="hdnSerial_sal" id="hdnSerial_sal" value="{$sale.serial_sal}" />
<input type="hidden" name="hdnSerial_slog" id="hdnSerial_slog" value="{$serial_slg}" />
</form>

