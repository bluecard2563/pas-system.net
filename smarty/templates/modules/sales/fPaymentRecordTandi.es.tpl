{assign var="title" value="HISTORIAL DE PAGOS"}
<script type="text/javascript" src="{$document_root}js/modules/sales/fPaymentRecordTandi.js"></script>
<div class="span-24 last title">
    Pagos Tandi<br/>
</div>
<div class="span-24 append-2 last line buttons">
    <form>
        <input title="Actualizar" name="refresh-table" id="refresh-table" type="button" value="Actualizar" onclick="this.value='Actualizando...';window.location.href='{$document_root}modules/sales/pPaymentRecordTandi.php'"/>
    </form>

</div>
<div class="span-24 last line">
    <table border="0" id="theTable">
        <THEAD>
        <tr bgcolor="#284787">

            <td align="center"
                style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Id
            </td>
            <td align="center"
                style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Referencia
            </td>
            <td align="center"
                style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Nombre
            </td>
            <td align="center"
                style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Apellido
            </td>
            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Cédula
            </td>
            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Email
            </td>
            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Dirección
            </td>
            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Valor Total
            </td>
            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Estado de pago
            </td>
            <td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                Estado de pago TANDI
            </td>
        </tr>
        </THEAD>
        <TBODY>
        {foreach name="rpInfo" from=$recordPayment item="rp"}
        <tr style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#d7e8f9" bgcolor="#e8f2fb">
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                {$rp.id}
                <input type="hidden" name="serial_cus[]" value="AS"/>
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                {$rp.referencia}
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                {$rp.nombre}
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                {$rp.apellido}
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                {$rp.cedula}
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                {$rp.email}
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" id="price_">
                {$rp.direccion}
            </td>
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" id="price_">
                {$rp.total} USD
            </td>
            {if $rp.estado_de_pago eq null}
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" id="price_">
                PENDING
            </td>
                {else}
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" id="price_">
                    {$rp.estado_de_pago}
                </td>
            {/if}
            {if $rp.estado_de_pago_tandi eq "ERROR"}
            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" bgcolor="#E8AFAF" bgcolor="#E8AFAF" id="price_">
                {$rp.estado_de_pago_tandi}
            </td>
            {else}
                <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;" id="price_">
                    {$rp.estado_de_pago_tandi}
                </td>
            {/if}
        </tr>
       {/foreach}
        </TBODY>
    </table>
</div>


