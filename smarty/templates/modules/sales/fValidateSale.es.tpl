{assign var="title" value="VALIDAR CONTRATO"}
<div class="span-24 last title">
    Validar Contrato<br/>
    <label class="label">* Campos Obligatorios</label>
</div>

<div class="span-24 last line">
    <div class="span-24 last line">
        <div class="prepend-6 span-4 label">* N&uacute;mero de Tarjeta:</div>
        <div class="span-6">
            <input type="text" id="txtCardNumber" name="txtCardNumber" class="span-6"/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-6 span-4 label">* C&oacute;digo Verificador:</div>
        <div class="span-6">
            <input type="text" id="txtVerificationCode" name="txtVerificationCode" class="span-6"/>
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input id="btnRegister" type="button" value="Buscar" name="btnRegister"/>
    </div>
</div>

<div class="span-24 last line" id="verificationContainer"></div>