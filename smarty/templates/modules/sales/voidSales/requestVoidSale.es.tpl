{if $unable eq 'TRUE'}
	<div class="append-7 span-10 prepend-7 last mainAlerts">
		<div class="span-10 error" align="center">
          	Su perfil no permite anulaci&oacute;n de tarjetas.
		</div>
	</div>
{else}
	{assign var="title" value="ANULACI&Oacute;N DE TARJETA"}
	<form name="frmVoidRequest" id="frmVoidRequest" action="{$document_root}modules/sales/voidSales/pVoidCheckedSales" method="post">
	    <div class="span-24 last line title">
	        SOLICITUD DE ANULACI&Oacute;N DE TARJETA<br />
	        <label>(*) Campos Obligatorios</label><br />
	        <label>Recuerde que no puede anular una venta que ya ha sido facturada</label>
	    </div>
	
		{if $error}
			<div class="append-7 span-10 prepend-7 last mainAlerts">
		    	{if $error eq 'OK'}
		       		<div class="span-10 success" align="center">
		            	La solicitud se ingres&oacute; exitosamente.
		            </div>
		        {else}
		        	<div class="span-10 error" align="center">
		        		La solicitud no pudo ser procesada.<br/>
		            	{if $error eq 'REQUESTED'}
			            	Ya existe otra solicitud pendiente para esta tarjeta
			            {elseif $error eq 'INVOICED'}
			            	La tarjeta ya fue facturada por lo que no es posible anularla
			            {elseif $error eq 'INVALID'}
			            	El n&uacute;mero de tarjeta ingresado no es v&aacute;lido
			            {elseif $error eq 'SAVE_FAULT'}
			            	La solicitud no pudo ser guardada, comuniquese con el administrador.
			            {elseif $error eq 'ALERT_FAULT'}
			            	La solicitud no pudo ser enviada, comuniquese con el administrador.
			            {elseif $error eq 'NO_CARD'}
			            	La tarjeta no pudo ser procesada, por favor vuevla a intentarlo.
			            {/if}
		            </div>
				{/if}
			</div>
		{/if}
			
	    <div class="prepend-7 span-10 append-7 last line">
	            <ul id="alerts" class="alerts"></ul>
	    </div>
	    
	    <div class="span-24 last line">
	        <div class="prepend-3 span-1">
	            <input type="radio" checked="checked" id="radioSingleCard" name="checkVoidType">
	        </div>
	        <div class="span-3">
	            <label>Una sola tarjeta:</label>
	        </div>
	        <div class="span-5">
	            <input type="text" id="txtCardNumber" name="txtCardNumber" title="El campor 'N&uacute;mero de tarjeta' es obligatorio">
	        </div>
	        <div class="span-6 last">
	            <label class="smallTextAlert">(Tarjetas que ya han sido vendidas)</label>
	        </div>
	    </div>
	    <hr class="line"/>
	    <div class="span-24 last line">
	        <div class="prepend-3 span-1">
	            <input type="radio" id="radioMultipleCards" name="checkVoidType">
	        </div>
	        <div class="span-4">
	            <label>Varias Tarjetas:</label>
	        </div>
	    </div>
		<div class="span-24 last line">&nbsp;</div>
	    <div class="span-24 last line">
	        <div class="prepend-4 span-3">
	            <label>*Zona:</label>
	        </div>
	        <div class="span-6 last">
	            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
	              <option value="">- Seleccione -</option>
	              {foreach from=$nameZoneList item="l"}
	              <option value="{$l.serial_zon}">{$l.name_zon}</option>
	              {/foreach}
	            </select>
	        </div>
	
	        <div class="span-3">
	            <label>*Pa&iacute;s:</label>
	        </div>
	        <div class="span-4 append-4 last" id="countryContainer">
	            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
	              <option value="">- Seleccione -</option>
	            </select>
	        </div>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="prepend-4 span-3">
	            <label>Representante:</label>
	        </div>
	        <div class="span-4 append-4 last" id="managerContainer">
	            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
	              <option value="">- Todos -</option>
	            </select>
	        </div>
	    </div>
	    
	    <div class="span-24 last line">
	        <div class="prepend-4 span-3">
	            <label>Responsable:</label>
	        </div>
	        <div class="span-8 last" id="comissionistsContainer">
	            <select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio.'>
	              <option value="">- Todos -</option>
	            </select>
	        </div>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="prepend-4 span-3">
	            <label>Comercializador:</label>
	        </div>
	        <div class="span-8 last" id="dealerContainer">
	            <select name="selDealerDea" id="selDealerDea" title='El campo "Comercializador" es obligatorio.'>
	              <option value="">- Todos -</option>
	            </select>
	        </div>
		</div>
		
		<div class="span-24 last line">
	        <div class="prepend-4 span-3">
	            <label>Sucursal:</label>
	        </div>
	        <div class="span-8 append-4 last" id="branchContainer">
	            <select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
	              <option value="">- Todos -</option>
	            </select>
	        </div>
	    </div>
	    
	    <div class="span-12 prepend-7 last line">
	        <b>* Observaciones:</b>
	    </div>
	    <div class="span-24 last buttons line">
	        <textarea class="textarea" name="txtObservation" id="txtObservation" title='El campo "Observaciones" es obligatorio'></textarea>
	    </div>
	    <div class="span-24 last buttons line">
	        <input type="button" name="btnRequestVoid" id="btnRequestVoid" value="Solicitar"/>
	    </div>
	    <div class="prepend-6 span-12 append-4">
			<div id="requestResults" />
		</div>
	</form>
{/if}