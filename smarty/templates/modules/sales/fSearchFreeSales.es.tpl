<!--/*
File: fSearchFreeSales.es.tpl
Author: Mario López
Creation Date:31/03/2010
Modified By:
Last Modified:
*/-->

{assign var="title" value="AUTORIZAR VENTAS FREE"}
<div class="span-24 last title">
	Autorizaci&oacute;n de Ventas Free<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $countryList}
<div class="append-8 span-10 prepend-6 last " id="messageContainer" style="display: none;">
    <div class="span-12 error" align="center">
        Pa&iacute;s es requerido
    </div>
</div>

<div class="span-7 span-10 prepend-7 last">
    <ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
    <div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
    <div class="span-5">
        <select name="selCountry" id="selCountry">
            <option value="">- Seleccione -</option>
            {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}" {if $countryList|@count==1} selected{/if}>{$l.name_cou}</option>
            {/foreach}
        </select>
    </div>
    <div class="span-3 label">Ciudad:</div>
    <div class="span-5 append-3 last" id="cityContainer">
        Seleccione un pa&iacute;s
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-3 span-4 label">Responsable:</div>
    <div class="span-5" id="responsableContainer">
        Seleccione una ciudad
    </div>
</div>

<div class="span-24 last line" id="dealerContainer"></div>

<div class="span-24 last line">
    <div class="prepend-10 span-5">
        <input type="button" id="searchBranches" value="Buscar Ventas Free" onclick="evaluateFilters();"/>
    </div>
</div>
<div class="span-24 last" id="resultSet"></div>
{else}
	<div class="span-7 span-10 prepend-7 last">
		<div class="span-10 error">
			No existen ventas free por autorizar.
		</div>
	</div>
{/if}