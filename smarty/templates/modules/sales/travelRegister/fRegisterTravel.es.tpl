{assign var="title" value="REGISTRAR VIAJES"}
<div class="span-24 last title">
		Registro de Viajes<br/>
</div>
<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>
<form name="frmRegisterTravel" id="frmRegisterTravel" method="post" action="{$document_root}modules/sales/travelRegister/pRegisterTravel">
	{if $productData.third_party_register_pro eq 'YES'}
	<div class="span-24 last" id="customerContainer">
		<div class="span-24 last line separator">
			<label>Datos del Cliente</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">*Tipo de Documento</div>
			<div class="span-4 append-2 last">
				<select name="selCustomerDocumentTypeTravelRecord" id="selCustomerDocumentTypeTravelRecord" title='El campo "Tipo de Documento" es obligatorio.'>
					<option value="">- Seleccione -</option>
					<option value="c">Cédula</option>
					<option value="p">Pasaporte</option>
				</select>
				<input type="hidden" name="hdnValidIdentificationTravelRecord" id="hdnValidIdentificationTravelRecord" value=""/>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
			<div class="span-5">
				<input type="text" name="txtDocumentCustomer" id="txtDocumentCustomer"  title='El campo "Identificaci&oacute;n" es obligatorio'/>
			</div>
			<div class="span-3 label">*Tipo de Cliente</div>
			<div class="span-4 append-2 last">
				<select name="selType" id="selType" title='El campo "Tipo de Cliente" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$typesList item="l"}
                        {if $l eq 'PERSON'}
							<!--<option value="{$l}">{if $l eq 'PERSON'}Persona Natural{elseif $l eq 'LEGAL_ENTITY'}Persona Jur&iacute;dica{/if}</option>-->
							<option value="{$l}">{if $l eq 'PERSON'}Persona Natural{/if}</option>
						{/if}
					{/foreach}
				</select>
			</div>
		</div>

		<div class="span-24 last line" id="divName">
			<div class="prepend-4 span-4 label">* Nombre:</div>
			<div class="span-5">
				<input type="text" name="txtNameCustomer" id="txtNameCustomer" title='El campo "Nombre" es obligatorio' />
			</div>

			<div class="span-3 label">* Apellido:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtLastnameCustomer" id="txtLastnameCustomer" title='El campo "Apellido" es obligatorio' />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s de Residencia:</div>
			<div class="span-5">
				<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s de Residencia" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">* Ciudad</div>
			<div class="span-4 append-4 last" id="cityContainerCustomer">
				<select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad de Residencia" es obligatorio'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line" id="divAddress">
			<div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
			<div class="span-5">
				<input type="text" name="txtBirthdayCustomer" id="txtBirthdayCustomer" title='El campo "Fecha de Nacimiento" es obligatorio'/>
			</div>

			<div class="span-3 label">* Direcci&oacute;n:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
			<div class="span-5">
				<input type="text" name="txtPhone1Customer" id="txtPhone1Customer" title='El campo "Tel&eacute;fono 1" es obligatorio'/>
			</div>

			<div class="span-3 label">Tel&eacute;fono 2:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtPhone2Customer" id="txtPhone2Customer" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Celular:</div>
			<div class="span-5">
				<input type="text" name="txtCellphoneCustomer" id="txtCellphoneCustomer" />
			</div>

			<div class="span-3 label">Email:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtMailCustomer" id="txtMailCustomer" title='El campo "Email" es obligatorio' />
			</div>
		</div>

		<div class="span-24  last line" id="divRelative">
			<div class="prepend-4 span-4 label">*Contacto en caso de Emergencia:</div>
			<div class="span-5">
				<input type="text" name="txtRelative" id="txtRelative" title='El campo "Cotnacto en caso de Emergencia" es obligatorio'/>
			</div>

			<div class="span-3 label">*Tel&eacute;fono del contacto:</div>
			<div class="span-4 append-4 last"> <input type="text" name="txtPhoneRelative" id="txtPhoneRelative" title='El campo "Tel&eacute;fono del contacto" es obligatorio'/> </div>
		</div>
	</div>

	<!--<div class="prepend-4 span-16 append-4 last line" id="customerQuestionsContainer"></div>-->

	<!-- Declaracion de Salud Boton -->
	<div class="span-24 last line" id="healthStatementContainer" style="display: none;">
		<input type="button" name="healthStatementTravelBtn" id="healthStatementTravelBtn" value="Declaración de Salud" onclick="openHealthStatement();">
		<button type="button" name="linkHealthStatementTravelBtn" id="linkHealthStatementTravelBtn" title="Enlazar declaración de salud" onclick="checkStatementRefreshButton();">Enlazar Declaración</button>
		<input type="hidden" name="hdnTravelStatementOrigin" id="hdnTravelStatementOrigin" value="" />
	</div>

	{else}
		<input type="hidden" id="date_last_travel" name="date_last_travel" value="{$date_last_travel}"/>
		<input type="hidden" id="serial_cus" name="serial_cus" value="{$serial_cus}"/>

	{/if}
	<div class="span-24 last line separator">
		<label>Validez de la Tarjeta</label>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Desde:</div>
        <div class="span-5">
			<label id="start_dt" name="start_dt">{$saleData.begin_date_sal}</label>
        </div>

        <div class="span-3 label">Hasta:</div>
        <div class="span-4 append-4 last">
			<label id="end_dt" name="end_dt">{$saleData.end_date_sal}</label>
        </div>
    </div>

	<div class="span-24 last line separator">
        <label>Datos del Viaje</label>
	</div>
	{if $productData.generate_number_pro eq 'NO'}
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">No. de Tarjeta:</div>
        <div class="span-5">
            <label>{$nextAvailableNumber}</label>
			<input type="hidden" id="generates_card_number" name="generates_card_number" value="1"/>
			<input type="hidden" id="hdnCounter" name="hdnCounter" value="{$serial_cnt}"/>
        </div>
	</div>
	{/if}
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">D&iacute;as Contratados: </div>
        <div class="span-5">
            <label>{$saleData.days_sal}</label>
        </div>
        <div class="span-3 label">D&iacute;as Disponibles: </div>
        <div class="span-5 append-3 last">
            <label id="days_available">{$days_available}</label>
        </div>
    </div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Partida:</div>
        <div class="span-5">
            <input type="text" name="txtDepartureDate" id="txtDepartureDate" title='El campo "Fecha de Salida" es obligatorio' />
        </div>

        <div class="span-3 label">* Fecha de Retorno:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtArrivalDate" id="txtArrivalDate"  title='El campo "Fecha de Retorno" es obligatorio'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer">* Pa&iacute;s de Destino:</div>
        <div class="span-5">
            <select name="selCountryDestination" id="selCountryDestination" title='El campo "Pa&iacute;s de Destino" es obligatorio.' {if $destinationRestricted}disabled{/if}>
					<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
				<option id="{$l.serial_cou}" value="{$l.serial_cou}" {if $serial_cou eq $l.serial_cou}selected{/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad de Destino:</div>
        <div class="span-4 append-4 last" id="cityDestinationContainer">
			<select name="selCityDestination" id="selCityDestination" title='El campo "Ciudad de Destino" es obligatorio'>
				<option value="">- Seleccione -</option>
            </select>
		</div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryDestinationContainer"># de D&iacute;as:</div>
		<div class="span-5">
			<label id="txtDaysBetween"></label>
		</div>
		<input type="hidden" name="hddDays" id="hddDays" value=""/>
    </div>

	<div class="span-24 last buttons line">
		<input type="submit" value="Registrar" id="btnRegisterTravel" name="btnRegisterTravel"/>
		<input type="hidden" name="hdnParameterCondition" id="hdnParameterCondition" value="{$parameterCondition}" />
		<input type="hidden" name="hdnParameterValue" id="hdnParameterValue" value="{$parameterValue}" />
		<input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
		<input type="hidden" name="has_questions" id="has_questions" value="NO" />
		<input type="hidden" name="hdnSale" id="hdnSale" value="{$serial_sal}" />
		<input type="hidden" name="today" id="today" value="{$today}" />
		<input type="hidden" name="allow_senior" id="allow_senior" value="{if $productData.senior_pro eq 'YES'}1{else}0{/if}"/>
		<input type="hidden" name="hdnSeniorPro" id="hdnSeniorPro" value="{$productData.senior_pro}" />
		<input type="hidden" name="hdnSerial_pxc" id="hdnSerial_pxc" value="{$serial_pxc}" />
		<input type="hidden" name="hdnParentCardNumber" id="hdnParentCardNumber" value="{$cardNumber}" />
		<input type="hidden" name="hdnCardNumber" id="hdnCardNumber" value="{$nextAvailableNumber}" />
		<input type="hidden" name="hdnSessionUser" id="hdnSessionUser" value="{$sessionUser}"/>
<input type="hidden" id="isThirdPartyRegisterPro" name="hdnIsHealthStatementRequired" value="{$productData.third_party_register_pro}"/>
	</div>
</form>

{if $travelsRegistered}
<div class="span-24 last line separator">
	<label>Registros Relacionados a la Tarjeta.</label>
	<input type="hidden" name="total_travels" id="total_travels" value="{$total_travels}" />
</div>
<br/>
<div class="span-24 last line">
	<table border="0" id="oldRegistersTable">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					# de Tarjeta
				</td>
				<td align="center" class="tableTitle">
					Cliente
				</td>
				<td align="center"  class="tableTitle">
					Ciudad
				</td>
				<td align="center" class="tableTitle">
					Fecha de Partida
				</td>
				<td align="center" class="tableTitle">
					Fecha de Retorno
				</td>
				<td align="center" class="tableTitle">
					D&iacute;as Viajados
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="registers" from=$travelsRegistered item="s"}
			<tr {if $smarty.foreach.registers.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
								{$s.card_number}
				</td>
				<td align="center" class="tableField">
								{$s.customer_name}
				</td>
				<td align="center" class="tableField">
					{$s.name_cit}
				</td>
				<td align="center" class="tableField">
					{$s.start_trl}
				</td>
				<td align="center" class="tableField">
					{$s.end_trl}
				</td>
				<td align="center" class="tableField">
					{$s.days_trl}
				</td>
			</tr>
					{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last pageNavPosition" id="travelPageNavPosition"></div>
{/if}
