{assign var="title" value="REGISTRAR VIAJES"}
<div class="span-24 last title">
	{if $type eq 'report'}Reporte de {/if}Registro de Viajes<br/>
</div>
 <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

<div class="span-24 last line" id="executiveContainer" >
	<div class="span-24 last title">
		<label>Ingrese el # de tarjeta &oacute; el nombre del Cliente.</label><br/>
	</div>
	<form name="frmTravelRegister" id="frmTravelRegister" method="post" action="">
		<div class="prepend-5 span-14 append-5 last line">
			<div class="prepend-3 span-4 label">N&uacute;mero de Tarjeta:</div>
			<div class="span-6">
				<input type="text" id="txtCardNumber" name="txtCardNumber" class="span-6"/>
			</div>
		</div>

		<div class="prepend-5 span-14 append-5 last line">
			<div class="prepend-3 span-4 label">Nombre del Cliente:</div>
			<div class="span-6">
				<input type="text" id="txtCustomerName" name="txtCustomerName" class="span-6"/>
				<input type="hidden" id="hdnSerial_cus" name="hdnSerial_cus"/>
			</div>
		</div>
	<div class="span-24 last buttons line">
			<input id="btnRegister" type="submit" value="Buscar" name="btnRegister"/>
		</div>
	</form>
</div>
	<input type="hidden" id="frontType" name="frontType" value="{$type}"/>
<div class="span-24 last line" id="cardsContainer">
</div>
