{assign var="title" value="NUEVA VENTA DE MASIVO"}
<script type="text/javascript" src="{$document_root}js/modules/sales/globalSalesFunctions.js"></script>
<form name="frmNewMasiveSale" id="frmNewMasiveSale" method="post" action="{$document_root}modules/sales/pNewMasiveSale" class="">
    <div class="span-24 last title">
         Nueva Venta de Masivo <br />
        <label>(*) Campos Obligatorios</label>
    </div>

   <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

   <div class="span-24 last line separator">
       <label>Informaci&oacute;n de la Venta</label>
    </div>

	{if $planetUser eq 1}
		{if $hasDealers neq 0}
			<div class="span-24 last line">
				<div class="prepend-5 span-6 label" id="counterSelection" >* Seleccione una sucursal de comercializador para realizar la Venta:</div>
				<div class="span-6 last" id="counterContainer">
						<select name="selCounter" id="selCounter" title='El campo "Seleccione un counter" es obligatorio'>
						<option value="">- Seleccione -</option>
						{foreach from=$serial_cnt key=key item="l"}
							<option value="{$l.serial_cnt}">{$l.name_dea}</option>
						{/foreach}
					</select>
				 </div>
			</div>
	
			<div class="span-24 last line" id="saleInfoPACounter"></div>
		{else}
			<div class="span-24 last line" id="saleInfoPACounter"> 
				<div class="span-24 last line">
					<div class="prepend-4 span-4 label">* Tipo de Venta:</div>
					<div class="span-3">
						Virtual
					</div>
				</div>

				 <div class="span-24 last line">
					<div class="prepend-4 span-4 label">* No de Tarjeta:</div>
					<div class="span-5">
						<input type="text" name="txtCardNum" id="txtCardNum" title='El campo "N&uacute;mero de Tarjeta" es obligatorio' value="AUTOMATICO" />
					</div>

					<div class="span-3 label">C&oacute;digo Vendedor:</div>
					<div class="span-4 append-4 last">
						<input type="text" name="txtCodCounter" id="txtCodCounter" {if $sellerCode}value="{$sellerCode}"{/if} />
					</div>
				</div>

				<div class="span-24 last line">
					<div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
					<div class="span-5">
						<input type="text" name="txtEmissionPlace" id="txtEmissionPlace" {if $data.emissionPlace}value="{$data.emissionPlace}"{/if} />
					</div>

					<div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
					<div class="span-5 append-3 last">
						<input type="text" name="txtEmissionDate" id="txtEmissionDate" title='El campo "Fecha de Emisi&oacute;n" es obligatorio' {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
					</div>
				</div>

				<div class="span-24 last line">
					<div class="prepend-4 span-4 label">* Asesor de Venta:</div>
					<div class="span-5">
						<input type="text" name="txtNameCounter" id="txtNameCounter" {if $data.counterName}value="{$data.counterName}"{/if} />
					</div>

					<div class="span-3 label">* Comercializador:</div>
					<div class="span-4 append-4 last">
						<input type="text" name="txtDealer" id="txtDealer" {if $data.name_dea}value="{$data.name_dea}"{/if} />
					</div>
				</div>

			   <div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
					<div class="prepend-4 span-4 label">* Moneda:</div>
					<div class="span-5">
						{foreach from=$data.currencies item="l" key="k"}
						<input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur}<br/>
						{/foreach}
					</div>
				</div>
			</div>
		{/if}
   {else}
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tipo de Venta:</div>
        <div class="span-3">
			Virtual            
        </div>
    </div>

     <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* No de Tarjeta:</div>
        <div class="span-5">
            <input type="text" name="txtCardNum" id="txtCardNum" title='El campo "N&uacute;mero de Tarjeta" es obligatorio' {if $nextAvailableNumber}value="{$nextAvailableNumber}"{/if} />
        </div>

        <div class="span-3 label">C&oacute;digo Vendedor:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtCodCounter" id="txtCodCounter" {if $sellerCode}value="{$sellerCode}"{/if} />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Lugar de Emisi&oacute;n:</div>
        <div class="span-5">
            <input type="text" name="txtEmissionPlace" id="txtEmissionPlace" {if $data.emissionPlace}value="{$data.emissionPlace}"{/if} />
        </div>

        <div class="span-3 label">* Fecha de Emisi&oacute;n:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtEmissionDate" id="txtEmissionDate" title='El campo "Fecha de Emisi&oacute;n" es obligatorio' {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Asesor de Venta:</div>
        <div class="span-5">
            <input type="text" name="txtNameCounter" id="txtNameCounter" {if $data.counterName}value="{$data.counterName}"{/if} />
        </div>

        <div class="span-3 label">* Comercializador:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtDealer" id="txtDealer" {if $data.name_dea}value="{$data.name_dea}"{/if} />
        </div>
    </div>

   {*<!--{if $sellFree eq 'YES'}
   <div class="span-24 last line" id="divSellFree">
        <div class="prepend-4 span-4 label">* Es Free:</div>
        <div class="span-5">
            <input type="radio" name="rdIsFree" id="rdIsFree" value="YES" title="El campo 'Es Free' es obligatorio." /> S&iacute;
			<input type="radio" name="rdIsFree" id="rdIsFree" value="NO" checked /> No
        </div>
    </div>
   {/if}-->*}
   
   <div class="span-24 last line" {if $data.currenciesNumber eq 1}style="display:none"{/if}>
        <div class="prepend-4 span-4 label">* Moneda:</div>
        <div class="span-5">
			{foreach from=$data.currencies item="l" key="k"}
            <input type="radio" name="rdCurrency" id="rdCurrency{$k}" symbol="{$l.symbol_cur}" value="{$l.exchange_fee_cur}" {if $l.official_cbc eq 'YES'}checked='checked'{/if}/> {$l.name_cur}<br/>
			{/foreach}
        </div>
    </div>

   {/if}
	   <!----------------------------------------  DATOS DEL CLIENTE ----------------------------------------->
		<div class="span-24 last" id="customerContainer"></div>
	   <!----------------------------------------  DATOS DEL CLIENTE ----------------------------------------->

		<!--------------------------------------- DATOS DEL PRODUCTO --------------------------------------->


		<div class="span-24 last line separator">
			<label>Datos del Producto</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Producto:</div>
			<div class="span-5" id="productsContainer">
				<select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio.'>
					<option value="" serial_pro="">- Seleccione -</option>
					{foreach from=$productListByDealer item="l"}
						<option value="{$l.serial_pbd}" id="product_{$l.serial_pbd}" serial_pro="{$l.serial_pro}" max_extras_pro="{$l.max_extras_pro}" range_price="{$l.price_by_range_pro}" calculate_price="{$l.calculate_pro}" services="{$l.has_services_pro}" serial_pxc="{$l.serial_pxc}" price_by_day_pro="{$l.price_by_day_pro}"
								children_pro="{$l.children_pro}" adults_pro="{$l.adults_pro}" flights_pro="{$l.flights_pro}" generate_number_pro="{$l.generate_number_pro}" senior_pro="{$l.senior_pro}" show_price_pro="{$l.show_price_pro}" masive_pro="{$l.masive_pro}" >{$l.name_pbl}</option>
					{/foreach}
				</select>
			</div>

		</div>


		<div class="span-24 last line">
			<div class="span-13 label" id="rangeSelect"></div>
			<div class="span-11 label last" id="rangePricesContainer"></div>
		</div>

		<!--<div class="span-24 last line" >
			<div class="prepend-4 span-4 label">* Carga Masiva:</div>
			<div class="span-5 last">
					<input id="button3" type="file" enctype="multipart/form-data" method="post"/>
			</div>
		</div>-->

		<!--<div id="customersMasive" class="append-2 span-20 prepend-2 last line"  >


		</div>-->

		<div class="span-24 line" style="display: none;" id="servicesContainer">
		{if $servicesListByDealer}
			<div class="span-24 last line separator">
				<label>Datos del Servicio</label>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-4 label">* Servicio:</div>
				<div class="span-5">
					{foreach from=$servicesListByDealer item="l"}
					<input type="checkbox" name="chkService_{$l.serial_sbd}" id="chkService_{$l.serial_sbd}" value="{$l.serial_sbd}" price="{$l.price_sbc}" cost="{$l.cost_sbc}" type_fee="{$l.fee_type_ser}"> {$l.name_sbl}<br>
					{/foreach}
				</div>

				<div class="span-3 label">Precio:</div>
				<div class="span-4 append-4 last">
					<label id="servicePriceContainer" style="font-weight: normal;"></label>
					<input type="hidden" name="hdnServicePrice" id="hdnServicePrice" value="">
					<input type="hidden" name="hdnServiceCost" id="hdnServiceCost" value="">
				</div>
			</div>
		 {/if}
		</div>

		<!---------------------------------------  DATOS DEL PRODUCTO -------------------------------------->

	   <!----------------------------------------  DATOS DEL VIAJE ----------------------------------------->

		<div class="span-24 last line separator">
			<label>Datos del Viaje</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Inicio de Cobertura:</div>
			<div class="span-5">
				<input type="text" name="txtDepartureDate" id="txtDepartureDate" title='El campo "Inicio de Cobertura" es obligatorio' />
			</div>

			<div class="span-3 label">* Fin de Cobertura:</div>
			<div class="span-5 append-3 last">
				<input type="text" name="txtArrivalDate" id="txtArrivalDate"  title='El campo "Fin de Cobertura" es obligatorio'/>
			</div>
		</div>

		<div class="span-24 last line" id="divDestination">
			<div class="prepend-4 span-4 label" id="countryDestinationContainer">Pa&iacute;s de Destino:</div>
			<div class="span-5">
				<select name="selCountryDestination" id="selCountryDestination" title='El campo "Pa&iacute;s de Destino" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$countryList item="l"}
						<option id="{$l.serial_cou}" value="{$l.serial_cou}">{$l.name_cou}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">* Ciudad de Destino:</div>
			<div class="span-4 append-4 last" id="cityDestinationContainer">
				<select name="selCityDestination" id="selCityDestination" title='El campo "Ciudad de Destino" es obligatorio'>
					<option value="">- Seleccione -</option>
				</select>
			 </div>
		</div>
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label" id="countryDestinationContainer"># de D&iacute;as:</div>
			 <div class="span-5">
				 <label id="txtDaysBetween"></label>
			 </div>
			 <input type="hidden" name="hddDays" id="hddDays" value="" />
		</div>

	<!----------------------------------------  DATOS DEL VIAJE ---------------------------------------->
	

    <!--<div class="span-24 last line separator">
        <label>Total de la Venta</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-1">
            <label>Total:</label>
        </div>
        <div class="span-4 append-7 last " id="totalContainer"></div>
    </div>-->

    <div class="span-24 last line separator">
        <label>Observaciones</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7">
            <textarea name="observations" id="observations"></textarea>
        </div>
    </div>

    <div class="span-24 last buttons">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
    <input type="hidden" name="hdnCardNum" id="hdnCardNum" {if $nextAvailableNumber}value="{$nextAvailableNumber}"{/if} />
    <input type="hidden" name="hdnEmissionDate" id="hdnEmissionDate" {if $data.emissionDate}value="{$data.emissionDate}"{/if} />
	<input type="hidden" name="hdnAge" id="hdnAge" value="{$age}" />
    <input type="hidden" name="hdnParameterValue" id="hdnParameterValue" value="{$parameterValue}" />
    <input type="hidden" name="hdnMaxSerial" id="hdnMaxSerial" value="{$maxSerial}" />
    <input type="hidden" name="hdnParameterCondition" id="hdnParameterCondition" value="{$parameterCondition}" />
    <input type="hidden" name="hdnQuestionsFlag" id="hdnQuestionsFlag" value="0" />
    <input type="hidden" name="hdnQuestionsExists" id="hdnQuestionsExists" value="{if $questionList}1{else}0{/if}" />
    <input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" value="{$serial_cou}" />
    <input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$data.serialDealer}" />
	<input type="hidden" name="hdnSerial_cou" id="hdnCntSerial_cou" value="{$serial_cou}" />
    <input type="hidden" name="hdnSerial_cnt" id="hdnSerial_cnt" value="{$serial_cnt}" />
    <input type="hidden" name="hdnTotal" id="hdnTotal" value="" />
    <input type="hidden" name="hdnTotalCost" id="hdnTotalCost" value="" />
	<input type="hidden" name="hdnMaxCoverTime" id="hdnMaxCoverTime" value="{$maxCoverTime}" />
	<input type="hidden" name="hdnDirectSale" id="hdnDirectSale" value="{if $ofCounter}{$ofCounter}{else}0{/if}" />
</form>




