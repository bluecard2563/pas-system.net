{assign var="title" value="BUSCAR INCENTIVOS"}
<form name="frmSearchBonus" id="frmSearchBonus" method="post" action="{$document_root}modules/bonus/pUpdateBonus" class="">
    <div class="span-24 last title">
        B&uacute;squeda de Incentivos<br />
    <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Cambios realizados exitosamente!
            {else}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}


     <div class="prepend-7 span-10 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3  label">* Zona: </div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-5  label">* Pa&iacute;s: </div>
        <div class="span-5" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3  label">* Ciudad: </div>
        <div class="span-5" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>

		<div class="span-5  label">* Responsable: </div>
        <div class="span-5" id="responsibleContainer">
            <select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Comercializador:</div>
        <div class="span-5 append-3 last line" id="dealerContainer">
            <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="prepend-7 span-10 last buttons line">
        <input type="button" value="Ver" name="btnSearch" id="btnSearch" />
    </div>

    <!--Table with the Bonuses-->
    <div class="prepend-3 span-19 last line" id="checkContainer"></div>
    <!--End Table with the Bonuses-->
</form>

<!--Edition Dialog-->
<div id="editBonusDialog" title="Dialog Title">
    <form name="frmUpdateBonus" id="frmUpdateBonus" method="post" action="{$document_root}modules/bonus/pUpdateSingleBonus" class="">
    <div class="span-10 last">
        <ul id="alertsDialog" class="alerts"></ul>
    </div>

    <input type="hidden" name="hdnSerialBon" id="hdnSerialBon" value="" />

    <div class="span-10 last line" id="timeEd">
        <div class="span-4">Comercializador:</div>
        <div class="span-4 append-1 last" id="dealerName"></div>
    </div>

    <div class="span-10 last line">
        <div class="span-4">Producto:</div>
        <div class="span-4 append-1 last" id="productName"> </div>
    </div>
    <div class="span-10 last line">
        <div class="span-4">* Porcentaje:</div>
        <div class="span-4 append-1 last" id="percentage">
            <input type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje" es obligatorio.' value="" />
        </div>
    </div>
    </form>
</div>
<!--End Edition Dialog-->