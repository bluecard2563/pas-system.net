{assign var="title" value="NUEVO INCENTIVO"}
<form name="frmNewBonus" id="frmNewBonus" method="post" action="{$document_root}modules/bonus/pNewBonus" class="">

    <div class="span-24 last title">
        Registro de Nuevo Incentivo<br />
    <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {elseif $error eq 2}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {elseif $error eq 3}
               Algunos incentivos no se han podido crear debido a que ya existen para el o los productos en uno o algunos comercializadores  , el detalle en la siguiente tabla:
            {/if}
        </div>
    </div>
    {/if}

    <!--NON-INSERTED BONUSES-->
    {if $nonInserted}
    <div class="span-24 last line separator">
        <label>Incentivos no ingresados:</label>
    </div>
    <div class="prepend-3 span-19 last line">
        <center>
            <div class="span-19 last line">
                <table border="0" class="paginate-1 max-pages-7" id="theTable" style="color: #000000;">
                    <THEAD>
                        <tr bgcolor="#284787">
                            {foreach from=$titles item="t"}
                            <td align="center" style="padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">
                                <font color=#FFFFFF>
                                {$t}
                                </font>
                            </td>
                            {/foreach}
                        </tr>
                    </THEAD>
                    <TBODY>

                    {foreach name="bonusList" from=$nonInserted item="bl"}
                    <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.bonusList.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
                        <td style="padding:5px 5px 5px 5px; text-align:center;">
                            {$bl.name_cou}
                        </td>
                        <td style="padding:5px 5px 5px 5px; text-align:center;">
                            {$bl.name_cit}
                        </td>
                        <td style="padding:5px 5px 5px 5px; text-align:center;">
                            {$bl.name_dea}
                        </td>
                        <td style="padding:5px 5px 5px 5px; text-align:center;">
                            {$bl.name_pxc}
                        </td>
                        <td style="padding:5px 5px 5px 5px; text-align:center;">
                            {$bl.percentage}%
                        </td>
                    </tr>
                    {/foreach}

                    </TBODY>
                </table>
            </div>
        </center>
    </div>
	<div class="span-24 last pageNavPosition" id="failedPageNavPosition"></div>
    {/if}
    <!--END NON-INSERTED BONUSES-->

     <div class="prepend-7 span-10 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3  label"> *Zona: </div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-5  label">* Pa&iacute;s: </div>
        <div class="span-5" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3  label">* Ciudad: </div>
        <div class="span-5" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>

		<div class="span-5  label">* Responsable: </div>
        <div class="span-5" id="responsibleContainer">
            <select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line separator">
        <label>Opciones de Incentivo:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-3 label">* Porcentaje</div>
        <div class="span-5">  <input type="text" name="txtPercentage" id="txtPercentage" title='El campo "Porcentaje" es obligatorio.'>
        </div>
    </div>
    <div class="span-24 last line"></div>
    <div class="prepend-8 span-6 append-6 label line">* Seleccione los comercializadores:</div>
    <div class="span-24 append-2 last line" id="branchContainer">
        <div class="span-11">
            <div align="center">Seleccionados</div>
            <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-11 last" title="El campo de comercializadores es obligatorio.">
            </select>
        </div>
        <div class="span-2 last buttonPane">
            <br />
            <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
            <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
            <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
            <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
        </div>
        <div class="span-11 last line" id="dealersFromContainer">
            <div align="center">Existentes</div>
            <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-11 last" city="{$geoData.serial_cit}">
            </select>
        </div>
    </div>
    <div class="span-24 last line"></div>
    <div class="prepend-8 span-6 append-6 label line">* Seleccione los Productos:</div>
    <div class="span-24 append-2 last line" id="productsContainer">
        <div class="span-11">
			<div align="center">Seleccionados</div>
            <select multiple id="productsTo" name="productsTo[]" class="selectMultiple span-11 last" title="El campo de productos es obligatorio.">
            </select>
        </div>
        <div class="span-2 last buttonPane"><br>
            <input type="button" id="moveSelectedProducts" name="moveSelectedProducts" value="|<" class="span-2 last"/>
            <input type="button" id="moveAllProducts" name="moveAllProducts" value="<<" class="span-2 last"/>
            <input type="button" id="removeAllProducts" name="removeAllProducts" value=">>" class="span-2 last"/>
            <input type="button" id="removeSelectedProducts" name="removeSelectedProducts" value=">|" class="span-2 last "/>
        </div>
        <div class="span-11 last line" id="productsFromContainer">
			<div align="center">Existentes</div>
            <select multiple id="productsFrom" name="productsFrom[]" class="selectMultiple span-11 last" city="{$geoData.serial_cit}">
            </select>
        </div>
    </div>
    <div class="prepend-7 span-10 last buttons line">
        <input id="btnInsert" type="button" value="Insertar" name="btnInsert"/>
    </div>
</form>