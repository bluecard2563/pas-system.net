{assign var="title" value="LIQUIDACI&Oacute;N DE INCENTIVOS"}
<form name="frmSelectBonus" id="frmSelectBonus" method="post" action="{$document_root}modules/bonus/liquidation/fPayBonus" >
    <div class="span-24 last title">
    	Liquidaci&oacute;n de Incentivos<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {else}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

	<div class="span-10 prepend-7 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line separator">
        <label>Datos de la Liquidaci&oacute;n:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Liquidaciones a:</div>
        <div class="span-4 append-8 last">
            <select name="selBonusTo" id="selBonusTo" title='El campo "Liquidaciones a" es obligatorio.' class="selectWidht">
                <option value="">- Seleccione -</option>
				<option value="DEALER">Comercializador</option>
				<option value="COUNTER">Counter</option>
            </select>
        </div>
    </div>
	<div class="span-24 last line" id="formContainer"></div>
</form>