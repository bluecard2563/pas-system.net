{assign var="title" value="SELECCI&Oacute;N DE PREMIOS PARA INCENTIVO"}
<form name="frmSelectPrizes" id="frmSelectPrizes" method="post" onsubmit="submit_redirect();" target="_blank" action="{$document_root}modules/bonus/liquidation/pPayBonus" >
    <div class="span-24 last title">
    	Selecci&oacute;n de Premios para incentivo<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {else}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

	<div class="span-10 prepend-7 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

<div class="span-24 last line separator">
	<label>Datos de Incentivos:</label>
</div>
<div class="span-24 last line">
		<div class="prepend-7 span-10 append-7 last line">
			<table border="1" id="tblBonus" style="color: #000000;">
				<THEAD>
					<tr bgcolor="#284787">
						<td align="center" class="tableTitle">
							Descripci&oacute;n
						</td>
						<td align="center" class="tableTitle">
							Monto
						</td>
					</tr>
				</THEAD>
				<TBODY>
					<tr bgcolor="#d7e8f9" >
						<td align="center" class="tableField">
							<b>Incentivos a favor:</b>
						</td>
						<td align="center" class="tableField">
							$ {$creditBonus|number_format:2}
						</td>
					</tr>
					<tr bgcolor="#e8f2fb" >
						<td align="center" class="tableField">
							<b>Incentivos a Descontar:</b>
						</td>
						<td align="center" class="tableField">
							$ {$againstBonus|number_format:2}
						</td>
					</tr>
					<tr bgcolor="#d7e8f9" >
						<td align="center" class="tableField">
							<b>Total Incentivos:</b>
						</td>
						<td align="center" class="tableField">
							$ {$totalBonus|number_format:2}
							<input type="hidden" id="maxBonus" name="maxBonus" value="{$totalBonus}"/>
						</td>
					</tr>
				</TBODY>
			</table>
		</div>
</div>

<div class="span-24 last line separator">
        <label>Incentivos seleccionables:</label>
</div>
{if $availablePrizes}
<div class="span-24 last line">
	<div class="prepend-17 span-3 label">Total a Reclamar:</div>
        <div class="span-3">
			$<input type="text" id="liquidationTotal" name="liquidationTotal" readonly value="0.00" class="span-2" style="border: none;background: none;" title="Los incentivos a reclamar no pueden superar el total de incentivos a favor."/>
        </div>
</div>
	<div class="span-24 last line">
           <table border="0" id="prizesTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                           Canjear
                        </td>
                        <td align="center" class="tableTitle">
                           Premio
                        </td>
                        <td align="center"  class="tableTitle">
                            Stock
                        </td>
                        <td align="center" class="tableTitle">
                            Monto Base
                        </td>
                        <td align="center" class="tableTitle">
                            *Tipo
                        </td>
                        <td align="center" class="tableTitle">
                            Cantidad
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="prizes" from=$availablePrizes item="s"}
                        <tr {if $smarty.foreach.prizes.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                <input type="checkbox" value="{$s.serial_pri}" id="chkPrize_{$s.serial_pri}" name="chkPrizes[]" class="prizesChk"/>
                            </td>
                            <td align="center" class="tableField">
                               {$s.name_pri|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.stock_pri}
                            </td>
                            <td align="center" class="tableField">
                                {$s.amount_pri}
                            </td>
                            <td align="center" class="tableField">
                                {$global_bonus_coverage[$s.coverage_pri]}
                            </td>
                            <td align="center" class="tableField">
                                <input size="5" type="text" id="quantity_{$s.serial_pri}" name="quantity_{$s.serial_pri}" class="inputQuantity" title="El campo de 'Cantidad' para  '{$s.name_pri|htmlall}' es requerido" style="display: none;" stock="{$s.stock_pri}" p_type="{$s.coverage_pri}" base="{$s.amount_pri}" />
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
	</div>
    <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
	<input type="hidden" name="selectedPrize" id="selectedPrize" title="Debe seleccionar al menos un premio."/>
	<input type="hidden" name="payTo" id="payTo"value="{$selBonusTo}"/>
	<input type="hidden" name="serialTo" id="serialTo" value="{$serial_to}"/>
	<input type="hidden" name="selToPay" id="selToPay" value="{$selToPay}"/>
<div class="span-24 last line">
	<br/>
		* Total: Debe ingresar el valor deseado.
	<br/>
		* Parcial: Debe ingresar el n&uacute;mero de premios deseado.
	<br/>
</div>
<div class="span-24 last center">
	<br/>
	<input type="submit" value="Liquidar">
</div>
{else}
<div class="span-17 prepend-7 last">
        <div class="span-10 error" align="center">
                No existen premios disponibles para el canje de Incentivos.
        </div>
</div>
{/if}
</form>