{assign var="title" value="LIQUIDACI&Oacute;N DE INCENTIVOS"}
<form name="frmPayBonus" id="frmPayBonus" method="post" >
    <div class="span-24 last title">
    	Liquidaci&oacute;n de Incentivos<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error eq "1"}
    <div class="append-7 span-10 prepend-7 last">
        <div class="span-10 success center">
            La Liquidaci&oacute;n fue procesada exitosamente!
        </div>
    </div>
    {/if}
    
    {if $pendingLiquidation}
    <div class="span-24 last line">
        <table border="1" class="dataTable" align="center" id="liquidationTable">
            <thead>
                <tr class="header">
                    <th style="text-align: center;">
                        <input type="checkbox" name="chkAll" id="chkAll" />
                    </th>
                    <th style="text-align: center;"># Liquidaci&oacute;n</th>
                    <th style="text-align: center;">Comercializador</th>
                    <th style="text-align: center;">Fecha de Solicitud</th>
                    <th style="text-align: center;">Monto</th>
                    <th style="text-align: center;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {foreach name="liquidation" from=$pendingLiquidation item=l}
                <tr {if $smarty.foreach.liquidation.iteration is even}class="odd"{else}class="even"{/if} >
                    <td class="tableField">
                        <input type="radio" name="chkBonusLiquidation[]" id="rdBonusLiquidation_{$l.serial_lqn}" value="{$l.serial_lqn}"/>
                    </td>
                    <td class="tableField">{$l.serial_lqn}</td>
                    <td class="tableField">{$l.name_dea}</td>
                    <td class="tableField">{$l.request_date}</td>
                    <td class="tableField">
                        {$l.total_lqn}
                        <input type="hidden" name="hdnTotal_{$l.serial_lqn}" id="hdnTotal_{$l.serial_lqn}" value="{$l.total_lqn}" />
                    </td>
                    <td class="tableField">
                        <label class="link" id="voidRegistry_{$l.serial_lqn}" registryID="{$l.serial_lqn}">Invalidar Liquidaci&oacute;n</label>
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
            
    <div class="span-24 center last line">
        <b>Total a Canjear:</b> <label id="lblTotal">0.00</label>
    </div>
            
    <div class="span-24 buttons lst line">
        <input type="button" name="btnExportPurchaseList" id="btnExportPurchaseList" value="Exportar Lista de Compras" /> <br><br>
        <input type="button" name="btnLiquidate" id="btnLiquidate" value="Liquidar" />
    </div>
    {else}
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 error center">
                No existen liquidaciones pendientes en el sistema.
            </div>
        </div>
    {/if}
</form>