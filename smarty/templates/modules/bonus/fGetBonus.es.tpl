{assign var="title" value="CANJE DE INCENTIVOS"}
 <div class="prepend-2 span-19 append-3 last title">Canje de Incentivos</div>
<input type="hidden" name="availablePoints" id="availablePoints" value={$availablePoints}>

{if $serial_dea}   
    {if $bonusTo eq "DEALER"}
        {if $isLeader eq "YES"}
            {if $availablePoints}
                <div class="span-10 append-7 prepend-7 last">
                <div class="span-10 success" align="center">
                 Puntos Disponibles: {$availablePoints} <br>
                Para ver un detalle de puntos haga click <a href="{$document_root}modules/bonus/pExportAvailableBonus" target="_blank">AQU&Iacute;</a>
                <br>Recuerde que por cada d&oacute;lar de incentivos se acreditan 3 puntos
                </div>
                </div>
            {else}
                <div class="span-10 append-7 prepend-7 last">
                    <div class="span-10 error center">
			No existen puntos disponibles para el canje.
                    </div>
                </div>
            {/if}
        {else}
            <div class="span-10 append-7 prepend-7 last">
		<div class="span-10 error center">
			Usted no es un Usuario Lider, por favor comuniquese con su Asesor Comercial.
		</div>
             </div>
        {/if}
    {else}
        {if $availablePoints}
            <div class="span-10 append-7 prepend-7 last">
                <div class="span-10 success" align="center">
                 Puntos Disponibles: {$availablePoints} <br>
                Para ver un detalle de puntos haga click <a href="{$document_root}modules/bonus/pExportAvailableBonus" target="_blank">AQU&Iacute;</a>
                <br>Recuerde que por cada d&oacute;lar de incentivos se acreditan 3 puntos
                </div>
            </div>
        {else}
            <div class="span-10 append-7 prepend-7 last">
		<div class="span-10 error center">
			No existen puntos disponibles para el canje.
		</div>
            </div>
	{/if}
    {/if}

{else}
    <div class="span-10 append-7 prepend-7 last">
	<div class="span-10 error center">
            Unicamente usuarios de comercializador puede efectuar el Canje de Puntos.
	</div>
    </div>	
{/if}

<form name="frmSelectPrizes" id="frmSelectPrizes" method="post" action="{$document_root}modules/bonus/pGetBonus">

{if $availablePrizes}
    <div class="span-10 append-7 prepend-5 last label">
        <input type="hidden" name="selectedPrize" id="selectedPrize" title="Debe seleccionar al menos un premio."/>
    </div>

<div class="span-24 last line">
	<div class="span-10 append-7 prepend-5 last label">Total de puntos a Reclamar:
		<input type="text" id="liquidationTotal" name="liquidationTotal" readonly value="0.00" class="span-2" style="border: none;background: none;" title="Valor incorrecto"/>
        </div>
</div>
	<div class="prepend-2 span-20 last line">
           <table border="0" id="prizesTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                           Canjear
                        </td>
                        <td align="center" class="tableTitle">
                           Premio
                        </td>
                        <td align="center" class="tableTitle">
                            Puntos Base
                        </td>
                        <td align="center" class="tableTitle">
                            Cantidad
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="prizes" from=$availablePrizes item="s"}
                        <tr {if $smarty.foreach.prizes.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                <input type="checkbox" value="{$s.serial_pri}" id="chkPrize_{$s.serial_pri}" name="chkPrizes[]" class="prizesChk"/>
                            </td>
                            <td align="center" class="tableField">
                               {$s.name_pri|htmlall}
                            </td>
                            <td align="center" class="tableField">
                                {$s.points_amount}
                            </td>
                            <td align="center" class="tableField">
                                <input size="5" type="text" id="quantity_{$s.serial_pri}" name="quantity_{$s.serial_pri}" class="inputQuantity" title="El campo de 'Cantidad' para  '{$s.name_pri|htmlall}' es requerido" style="display: none;" p_type="{$s.coverage_pri}" base="{$s.points_amount}" />
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
	</div>
    <div class="prepend-2 span-20 last pageNavPosition" id="pageNavPosition"></div>
	
	
	<input type="hidden" name="availablePoints" id="availablePoints" value="{$availablePoints}"/>

<div class="span-24 last buttons line">
        <input type="submit" value="CANJEAR INCENTIVOS" align="center">
</div>
{else}
<div class="span-17 prepend-7 last">
        <div class="span-10 error" align="center">
                No existen premios disponibles para el canje de Incentivos. Contacte a su asesor comercial para más opciones
        </div>
</div>
{/if}

</form>