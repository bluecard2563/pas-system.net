{assign var="title" value="AUTORIZACI&Oacute;N DE INCENTIVOS"}
<form name="frmSelectBonus" id="frmSelectBonus" method="post" action="{$document_root}modules/bonus/pAuthorizeOutDateBonus" >
    <div class="span-24 last title">
    	Autorizaci&oacute;n de Incentivos<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {elseif $error eq 2}
                Algunos registros no pudieron ser procesados. Por favor vuelva a intentarlo.
			{elseif $error eq 3}
			    Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

	<div class="span-10 prepend-7 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line separator">
        <label>Datos de Incentivos:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Incentivos de:</div>
        <div class="span-4 append-8 last">
            <select name="selBonusTo" id="selBonusTo" title='El campo "Liquidaciones a" es obligatorio.' class="selectWidht">
                <option value="">- Seleccione -</option>
				<option value="DEALER">Comercializador</option>
				<option value="COUNTER">Counter</option>
            </select>
        </div>
    </div>
	<div class="span-24 last line" id="formContainer"></div>
</form>