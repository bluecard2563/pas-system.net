{assign var="title" value="VER CLIENTES POTENCIALES"}
<form name="frmViewPotentialCustomer" id="frmViewPotentialCustomer"  method="post"  class="">
	<div class="span-24 last title">
    	Ver Clientes Potenciales <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El cliente se ha registrado exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last alerts">
	    <ul id="alerts" class="alerts"></ul>
    </div>


    <div class="span-24 last line">
        <div class="prepend-7 span-4 label" id="zoneContainer">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name=zones from=$zoneList item="l"}
                    <option id="{$l.serial_zon}" value="{$l.serial_zon}"{if $smarty.foreach.zones.total eq 1}selected{/if}>{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">Ciudad:</div>
        <div class="span-5 last" id="cityContainer">
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Todas -</option>
            </select>
         </div>
	</div>
	<div class="span-24 line last"></div>

    <div class="span-24 line last" id="potentialCustomers">
    </div>

    <div class="span-24 last buttons">
    	<input type="button" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
</form>
