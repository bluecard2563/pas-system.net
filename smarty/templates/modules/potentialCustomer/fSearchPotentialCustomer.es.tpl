{assign var="title" value="BUSCAR CLIENTE POTENCIAL"}
 <div class="prepend-2 span-19 append-3 last title">Buscar Cliente Potencial</div>

<form name="frmSearchPotentialCustomer" id="frmSearchPotentialCustomer" action="{$document_root}modules/potentialCustomer/fUpdatePotentialCustomer" method="post" class="form_smarty">
    {if $error}
         <div class="append-8 span-10 prepend-6 last ">
            <div class="span-10 {if $error eq 2 || $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 2}
				Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 3}
					El cliente ingresado no existe.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-8 span-10 prepend-6 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-2 span-19 append-3 last line">
        <div class="prepend-1 span-5 label">Ingrese el nombre o documento del cliente:</div>
        <div class="span-11 append-2 last">
            <input type="text" name="txtCustomerData" id="txtCustomerData" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
        </div>
	</div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnCustomerID" id="hdnCustomerID" value="" />
</form>