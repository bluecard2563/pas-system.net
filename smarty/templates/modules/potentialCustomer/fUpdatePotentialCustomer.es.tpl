{assign var="title" value="CLIENTES POTENCIALES"}
<form name="frmUpdatePotentialCustomer" id="frmUpdatePotentialCustomer" method="post" action="{$document_root}modules/potentialCustomer/pUpdatePotentialCustomer" class="">
	<div class="span-24 last title">
    	Actualizaci&oacute;n de un Cliente Potencial <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El cliente se ha registrado exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last alerts">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' {if $data}value='{$data.first_name_pcs}'{/if}>
        </div>

        <div class="span-3 label">* Apellido</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' {if $data}value='{$data.last_name_pcs}'{/if}> </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
        <div class="span-5"> <input type="text" name="txtDocument" id="txtDocument" title='El campo "Identificaci&oacute;n" es obligatoria' {if $data}value='{$data.document_pcs}'{/if}> </div>

        <div class="span-3 label"> Fecha de Nacimiento:</div>
        <div class="span-5 append-1"> <input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio' {if $data}value='{$data.birthdate_pcs}'{/if}> </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
        <div class="span-5">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name=countries from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}"{if $data}{if $data.serial_cou eq $l.serial_cou} selected="selected"{/if}{/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Seleccione -</option>
				{foreach from=$cityList item="l"}
                    <option value="{$l.serial_cit}" {if $data}{if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}{/if}>{$l.name_cit}</option>
                {/foreach}
            </select>
         </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-5"> <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio'  {if $data}value='{$data.phone1_pcs}'{/if}>
        </div>

        <div class="span-3 label">Celular:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio'  {if $data}value='{$data.cellphone_pcs}'{/if}>
        </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* E-mail:</div>
        <div class="span-5 "> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio'  {if $data}value='{$data.email_pcs}'{/if}> </div>

		<div class="span-3 label">*Estado:</div>
        <div class="span-4 append-2 last">
        	<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item="l"}
                    <option value="{$l}" {if $data}{if $data.status_pcs eq $l} selected="selected"{/if}{/if}>{if $l eq 'ACTIVE'}Activo{elseif $l eq 'INACTIVE'}Inactivo{/if}</option>
                {/foreach}
            </select>
		</div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Comentarios:</div>
		 <div class="prepend-7">
            <textarea name="comments" id="comments">{if $data}{$data.comments_pcs}{/if}</textarea>
        </div>
	</div>

    <div class="span-24 last buttons">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>

	 <input type="hidden" name="serial_pcs" id="serial_pcs" value="{if $data}{$data.serial_pcs}{/if}" />
</form>
