{assign var="title" value="NUEVO CALENDARIO"}

<form name="frmNewDealerVisit" id="frmNewDealerVisit">
    <div class="span-24 last line title">
        Calendario de Visitas a Comercializadores<br />
        <label>(*) Campos Obligatorios</label>
    </div>


    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 success" align="center" style="display:none" id="successMsg">
            Los cambios en el calendario se realizaron exitosamente!
        </div>
        <div class="span-10 error" align="center" style="display:none" id="errorMsg">
            Hubo errores en el ingreso. Por favor vuelva a intentarlo.
        </div>
    </div>
    
    <!--Generate Calendar Data-->
    <div class="span-9 append-1 last line">
        <div class="span-9 last line separator">
            <label>Crear calendario:</label>
        </div>
        <div class="prepend-1 span-8 last line">
            <input type="radio" name="rdoUser" id="rdoSelfUser" checked/>Crear calendario propio
            <input type="hidden" name="hdnSerialUsr" id="hdnSerialUsr" value="{$serial_user}" />
        </div>

        {if $comissionistList}
        <div class="prepend-1 span-8 last line">
            <input type="radio" name="rdoUser" id="rdoUser" />Crear calendario para &nbsp;&nbsp;
            <select name="selUser" id="selUser" disabled>
                <option value="">- Seleccione -</option>
                {foreach name="users" from=$comissionistList item="l"}
                    <option value="{$l.serial_usr}"{if $smarty.foreach.users.total eq 1}selected{/if}>{$l.first_name_usr} {$l.last_name_usr}</option>
                {/foreach}
            </select>
        </div>
        {/if}

        <div class="prepend-2 span-6 last line">
            <input type="button" name="btnCreateCal" id="btnCreateCal" value="Generar Calendario" />
        </div>
    </div>
    <!--End Generate Calendar Data-->

    <div class="span-14 last line">
        <div class="span-14 last line separator">
            <label>Propuesta:</label>
        </div>
        <!--Calendar-->
        <div id="calendar" class="span-14 last line"></div>
        <!--End Calendar-->
    </div>

    <input type="hidden" name="hdnEvents" id="hdnEvents" value="{$usr_events}" />
    <input type="hidden" name="hdnUser" id="hdnUser" value="{$serial_user}" />
</form>


<!--NEW EVENT DIALOG-->
<div id="newEventDialog" title="Dialog Title">
    <!--VALIDATION MESSAGES-->
    <ul id="validateNewEventDialog" class="span-8 last line alerts" style="display:none;">
        <li>
            <label id="validateDealer" class="error_validation" style="display:none; font-size: 12px;">
                El campo "Comercializador" es obligatortio.
            </label>
        </li>
        <li class="span-9" align="center" >
            <label  id="validateType" class="error_validation" style="display:none; font-size: 12px;">
                El campo "Tipo" es obligatortio.
            </label>
        </li>
    </ul>
    <!--END VALIDATION MESSAGES-->

    <div class="span-10 last line">
        <div class="span-4">Hora:</div>
        <div class="span-4 append-1 last" id="eventTime"></div>
        <input type="hidden" name="hdnEventTime" id="hdnEventTime" />
    </div>

    <div class="span-10 last line">
        <div class="span-4">* Comercializador:</div>
        <div class="span-4 append-1 last" id="dealerContainer">
            <select name="selDealer" id="selDealer" >
                <option value="">- Seleccione -</option>
                {foreach name="dealers" from=$dealerList item="l"}
                    <option value="{$l.serial_dea}"{if $smarty.foreach.dealers.total eq 1}selected{/if}>{$l.name_dea}</option>
                {/foreach}
            </select>
        </div>
    </div>

	<div class="span-10 last line">
        <div class="span-4">* Sucursal:</div>
        <div class="span-4 append-1 last" id="BranchContainer">
            <select name="selBranch" id="selBranch">
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-10 last line">
        <div class="span-4">*Tipo:</div>
        <select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$typeList item="l"}
                <option value="{$l}">{$global_visit_type.$l}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-10 last line">
        <div class="span-4">Estado:</div>
        <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
            {foreach from=$statusList item="l"}
                <option value="{$l}" {if $l eq 'PENDING'}selected{/if}>{$global_visit_status.$l}</option>
            {/foreach}
        </select>
    </div>
</div>
<!--END NEW EVENT DIALOG-->

<!--EDIT EVENT DIALOG-->
<div id="editEventDialog" title="Dialog Title">
    <!--VALIDATION MESSAGES-->
    <ul id="validateEditEventDialog" class="span-8 last line alerts" style="display:none;">
        <li>
            <label id="validateTypeEd" class="error_validation" style="display:none; font-size: 12px;">
                El campo "Tipo" es obligatortio.
            </label>
        </li>
    </ul>
    <!--END VALIDATION MESSAGES-->

    <div class="span-10 last line" id="timeEd">
        <div class="span-4">Hora:</div>
        <div class="span-4 append-1 last" id="eventTimeEd"></div>
    </div>

    <div class="span-10 last line">
        <div class="span-4">Comercializador:</div>
        <div class="span-4 append-1 last" id="eventTitleEd">
        </div>
    </div>

    <div class="span-10 last line">
        <div class="span-4">* Tipo:</div>
        <select name="selTypeEd" id="selTypeEd" title='El campo "Tipo" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$typeList item="l"}
                <option value="{$l}">{$global_visit_type.$l}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-10 last line">
        <div class="span-4">Estado:</div>
            <select name="selStatusEd" id="selStatusEd" title='El campo "Estado" es obligatorio'>
                {foreach from=$statusList item="l"}
                    <option value="{$l}">{$global_visit_status.$l}</option>
                {/foreach}
            </select>
    </div>
</div>
<!--END EDIT EVENT DIALOG-->

<!--OBSERVATIONS DIALOG-->
<div id="observationsDialog" title="Dialog Title">
    <!--VALIDATION MESSAGES-->
    <ul id="validateObservationsDialog" class="span-9 last line alerts" style="display:none;">
        <li>
            <label id="validateHost" class="error_validation" style="display:none; font-size: 12px;">
                El campo "Persona que atendi&oacute;" es obligatortio.
            </label>
        </li>
        <li class="span-9" align="center" >
            <label  id="validateComments" class="error_validation" style="display:none; font-size: 12px;">
                El campo "Comentarios" es obligatortio.
            </label>
        </li>
        <li class="span-9" align="center" >
            <label id="validateStrategy" class="error_validation" style="display:none; font-size: 12px;">
                El campo "Estrategia" es obligatortio.
            </label>
        </li>
    </ul>
    <!--END VALIDATION MESSAGES-->

    <div class="span-10 last line">
        <div class="span-4">* Persona que atendi&oacute;:</div>
        <div class="span-4 append-1 last">
            <input type="text" name="txtHost" id="txtHost">
        </div>
    </div>

    <div class="span-10 last line">
        <div class="span-3">* Comentarios:</div>
        <div class="span-7 last">
            <textarea name="txtComments" id="txtComments" style="width: 250px; height: 80px;"></textarea>
        </div>
    </div>

    <div class="span-10 last line">
        <div class="span-3">* Estrategia:</div>
        <div class="span-7 last">
            <textarea name="txtStrategy" id="txtStrategy" style="width: 250px; height: 80px;"></textarea>
        </div>
    </div>
</div>
<!--END OBSERVATIONS DIALOG-->

<!--VISITED DIALOG-->
<div id="visitedDialog" title="Dialog Title">
	<div class="span-10 last line">
        <div class="span-4">Comercializador:</div>
        <div class="span-4 append-1 last" id="eventTitleVis"></div>
    </div>

    <div class="span-10 last line">
        <div class="span-4">Tipo:</div>
        <select name="selTypeVis" id="selTypeVis" title='El campo "Tipo" es obligatorio'>
            <option value="">- Seleccione -</option>
            {foreach from=$typeList item="l"}
                <option value="{$l}">{$global_visit_type.$l}</option>
            {/foreach}
        </select>
    </div>

    <div class="span-10 last line">
        <div class="span-4">Persona que atendi&oacute;:</div>
        <div class="span-4 append-1 last" id="eventHostVis"></div>
    </div>

    <div class="span-10 last line">
        <div class="span-3">Comentarios:</div>
        <div class="span-7 last">
            <textarea disabled name="txtCommentsVis" id="txtCommentsVis" style="width: 250px; height: 80px;"></textarea>
        </div>
    </div>

    <div class="span-10 last line">
        <div class="span-3">Estrategia:</div>
        <div class="span-7 last">
            <textarea disabled name="txtStrategyVis" id="txtStrategyVis" style="width: 250px; height: 80px;"></textarea>
        </div>
    </div>
</div>
<!--END VISITED DIALOG-->