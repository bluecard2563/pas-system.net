{assign var="title" value="REPORTE DE VISITAS"}

<form name="frmDealerVisitReport" id="frmDealerVisitReport" action="{$document_root}modules/dealerVisit/pVisitsReport" method="post">
    <div class="span-24 last line title">
        Crar reporte de visitas<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="append-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Responsable:</div>
        <div class="span-5">
            {if $comissionistList}
            <select name="selUser" id="selUser">
                <option value="">- Todos -</option>
                {foreach from=$comissionistList item="l"}
                <option value="{$l.serial_usr}">{$l.first_name_usr} {$l.last_name_usr}</option>
                {/foreach}
            </select>
            {else}
            No existen usuarios registrados.
            {/if}
        </div>

        <div class="span-3 label">Comercializador:</div>
        <div class="span-5 append-3 last line" id="dealerContainer">
            <select name="selDealer" id="selDealer">
                <option value="">- Todos -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Tipo de visita:</div>
        <div class="span-5">
            <select name="selTypeVis" id="selTypeVis">
                <option value="">- Todos -</option>
                {foreach from=$typeList item="l"}
                    <option value="{$l}">{$global_visit_type.$l}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">Estado de la visita:</div>
        <div class="span-5 append-3 last line">
            <select name="selStatusVis" id="selStatusVis">
                <option value="">- Todos -</option>
                {foreach from=$statusList item="l"}
                    <option value="{$l}">{$global_visit_status.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>


    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Fecha desde:</div>
        <div class="span-5">
            <input type="text" name="txtBeginDate" id="txtBeginDate" />
        </div>

        <div class="span-3 label">Fecha hasta:</div>
        <div class="span-5 append-3 last line">
            <input type="text" name="txtEndDate" id="txtEndDate" />
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Generar Reporte" >
    </div>
</form>