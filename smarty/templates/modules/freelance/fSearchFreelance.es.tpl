{assign var="title" value="BUSCAR AGENTE LIBRE"}
<div class="span-24 last title">Buscar Agente Libre<br />
	<label>(*) Campos Obligatorios</label>
</div>
{if $freelance_list}
    {if $error}
		<div class="prepend-7 span-10 append-7 last">
			<div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
				{if $error eq 2}
					La actualizaci&oacute;n ha sido exitosa!
				{elseif $error eq 1}
					El agente libre ingresado no existe.
				{/if}
			</div>
		</div>
    {/if}
    
    <div class="span-24 last line">
        <table border="1" class="dataTable" align="center" id="freelanceTable">
			<thead>
				<tr class="header">
					<th style="text-align: center;">Documento</th>
					<th style="text-align: center;">Agente</th>
					<th style="text-align: center;">Estado</th>
					<th style="text-align: center;">Fecha de Ingreso</th>
					<th style="text-align: center;">&Uacute;ltima Liquidaci&oacute;n</th>
					<th style="text-align: center;">Acci&oacute;n</th>
				</tr>
			</thead>
			<tbody>
				{foreach name="freelance" from=$freelance_list item=f}
				<tr {if $smarty.foreach.freelance.iteration is even}class="odd"{else}class="even"{/if} >
					<td class="tableField">{$f.document_fre}</td>
					<td class="tableField">{$f.name_fre}</td>
					<td class="tableField">{$global_status[$f.status_fre]}</td>
					<td class="tableField">{$f.registration_date_fre}</td>
					<td class="tableField">{$f.last_commission_paid_fre}</td>
					<td class="tableField">
						<a href="{$document_root}modules/freelance/fUpdateFreelance/{$f.serial_fre}">Actualizar</a>
					</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
{else}
	<div class="append-7 span-10 prepend-7 last">
		<div class=" span-10 error" align="center">
			No existen independientes registrados en el sistema.
		</div>
	</div>
{/if}