<!--
/*
File: fNewFreelance.class.tpl
Author: Esteban Angulo
Creation Date: 30/12/2009 16:37
Last Modified:30/12/2009 
*/
-->
{assign var="title" value="ACTUALIZACI&Oacute;N AGENTE LIBRE "} 
<form name="frmUpdateFreelance" id="frmUpdateFreelance" method="post" action="{$document_root}modules/freelance/pUpdateFreelance" class="">
	<div class="span-24 last title">
    	Actualizaci&oacute;n de Agente Libre <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-5 span-9 prepend-5 last ">
    	{if $error eq 1}
        	<div class="span-10 success align="center"">
            	La actualizaci&oacute;n se ha realizado exitosamente!
            </div>
        {else}
        	<div class="span-10 error align="center"">
            	Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">  
			<input type="text" name="txtFirstname" id="txtFirstname" title='El campo "Nombre" es obligatorio.' value="{$data.name_fre}" /> 
		</div>
        
        <div class="span-3 label">* RUC</div>
        <div class="span-4 append-4 last">  
			<input type="text" name="txtDocument" id="txtDocument" title='El campo "RUC" es obligatorio' value="{$data.document_fre}" />
		</div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
        <div class="span-5"> 
			<input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono 1" es obligatorio' value="{$data.phone1_fre}" />
		</div>
        
        <div class="span-3 label">Tel&eacute;fono 2</div>
        <div class="span-4 append-4 last">
			<input type="text" name="txtPhone2" id="txtPhone2" value="{$data.phone2_fre}" />
		</div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5"> 
			<input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' value="{$data.address_fre}" />
		</div>
        
        <div class="span-3 label">*  E-mail:</div>
        <div class="span-4 append-4 last">  
			<input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio' value="{$data.email_fre}" /> 
		</div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* URL:</div>
        <div class="span-5"> 
			<input type="text" name="txtUrl" id="txtUrl" title='El campo "URL" es obligatorio' value="{$data.url_fre}" /> 
		</div>
        
        <div class="span-3 label">* % Comisi&oacute;n</div>
        <div class="span-4 append-4 last"> 
			<input type="text" name="txtComission" id="txtComission" title='El campo "% Comisi&oacute;n" es obligatorio' value="{$data.comission_fre}" />
		</div>
	</div>    
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Estado:</div>
        <div class="span-5"> 
			<select name="selStatus" id="selStatus" title="El campo 'Estado' es olbigatorio." >
				<option value="">- Seleccione -</option>
				{foreach from=$status_fre item="s"}
					<option value="{$s}" {if $s eq $data.status_fre}selected{/if}>{$global_status.$s}</option>
				{/foreach}
			</select>
		</div>
        
        <div class="span-3 label">&nbsp;</div>
        <div class="span-4 append-4 last"> &nbsp;</div>
	</div>
        
    <div class="span-24 last buttons">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" />
		<input type="hidden" name="txtSerial_fre" id="txtSerial_fre" value="{if $data}{$data.serial_fre}{/if}" />
    </div>    
</form>