<!--
/*
File: fNewFreelance.class.tpl
Author: Esteban Angulo
Creation Date: 30/12/2009 16:37
Last Modified:30/12/2009 
*/
-->
{assign var="title" value="NUEVO AGENTE LIBRE"} 
<form name="frmNewFreelance" id="frmNewFreelance" method="post" action="{$document_root}modules/freelance/pNewFreelance" class="">
	<div class="span-24 last title">
    	Registro de Agente Libre<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-5 span-9 prepend-5 last ">
    	{if $error eq 1}
        	<div class="span-10 success center" >
            	El Agente Libre se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error center" >
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstname" id="txtFirstname" title='El campo "Nombre" es obligatorio.'> </div>
        
        <div class="span-3 label">* RUC</div>
        <div class="span-4 append-4 last">  
			<input type="text" name="txtDocument" id="txtDocument" title='El campo "RUC" es obligatorio' />
		</div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono 1:</div>
        <div class="span-5"> 
			<input type="text" name="txtPhone1" id="txtPhone1" title='El campo "Tel&eacute;fono 1" es obligatorio' />
		</div>
        
        <div class="span-3 label">Tel&eacute;fono 2</div>
        <div class="span-4 append-4 last">
			<input type="text" name="txtPhone2" id="txtPhone2" />
		</div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Direcci&oacute;n:</div>
        <div class="span-5"> <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' /></div>
        
        <div class="span-3 label">*  E-mail:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio' /> </div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* URL:</div>
        <div class="span-5"> <input type="text" name="txtUrl" id="txtUrl" title='El campo "URL" es obligatorio' /> </div>
        
        <div class="span-3 label">* % Comisi&oacute;n</div>
        <div class="span-4 append-4 last"> 
			<input type="text" name="txtComission" id="txtComission" title='El campo "% Comisi&oacute;n" es obligatorio'/>
		</div>
	</div>    
        
    <div class="span-24 last buttons">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
    <input type="hidden" name="serial_fre" id="serial_fre" value="{if $data}{$data.serial_fre}{/if}" />
</form>