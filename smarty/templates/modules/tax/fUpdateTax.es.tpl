{assign var="title" value="ACTUALIZAR IMPUESTO"}
<form name="frmUpdateTax" id="frmUpdateTax" method="post" action="{$document_root}modules/tax/pUpdateTax" class="form_smarty">

	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Impuesto<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 error" align="center">
            {if $error eq 1}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
	<div class="span-24 last line">
        <div class="prepend-8 span-4">
            <label>*Nombre:</label>
        </div>
        <div class="span-9 last">
            <input type="text" name="txtNameTax" id="txtNameTax" title="El campo 'Nombre' es obligatorio" {if $data.name_tax}value="{$data.name_tax}"{/if}>        
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-8 span-4">
            <label>*Porcentaje:</label>
        </div>
        <div class="span-9 last">
            <input type="text" name="txtPercentageTax" id="txtPercentageTax" title="El campo 'Porcentaje' es obligatorio" {if $data.percentage_tax}value="{$data.percentage_tax}"{/if}>        
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-8 span-4">
            <label>*Estado:</label>
        </div>
        <div class="span-9 last">
            <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
                {foreach from=$statusList item="l"}
                    <option value="{$l}" {if $data.status_tax eq $l}selected{/if}>{$global_status.$l}</option>
                {/foreach}
            </select>      
        </div>
    </div>  
    
    <div class="span-24 last buttons line">
    	<input maxlength="30" type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_tax" id="hdnSerial_tax" {if $data.serial_tax}value="{$data.serial_tax}"{/if} >
        <input type="hidden" name="selCountry" id="selCountry" {if $data.serial_cou}value="{$data.serial_cou}"{/if} >
    </div>
</form>