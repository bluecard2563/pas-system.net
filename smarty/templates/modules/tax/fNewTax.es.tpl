{assign var="title" value="NUEVO IMPUESTO"}
<form name="frmNewTax" id="frmNewTax" method="post" action="{$document_root}modules/tax/pNewTax" class="">

	<div class="span-24 last title ">
    	Registro de Nuevo Impuesto<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                        El Impuesto se ha registrado exitosamente!
                {else}
                        No se pudo insertar el Impuesto.
                {/if}
            </div>
        </div>
    </div>
    {/if}
    
    <div class="append-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line"> 
	    <div class="prepend-4 span-8 label">*Seleccione el pa&iacute;s:</div>
        <div class="span-8 append-4 last" id="countryContainer">     	                                  
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
              <option value="">- Seleccione -</option>
              {foreach from=$countryList item="l"}
              <option value="{$l.serial_cou}">{$l.name_cou}</option>
              {/foreach}
            </select>
   		</div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">*Nombre:</div>
        <div class="span-8 append-4 last"> 
                <input type="text" name="txtNameTax" id="txtNameTax" title="El campo 'Nombre' es obligatorio.">      
        </div>
    </div>
    
	<div class="span-24 last line">
        <div class="prepend-4 span-8 label">*Porcentaje:</div>
        <div class="span-8 append-4 last"> 
                <input type="text" name="txtPercentageTax" id="txtPercentageTax" title="El campo 'Porcentaje' es obligatorio.">      
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" />
    </div>
    <input type="hidden" name="hdnFlagTax" id="hdnFlagTax" value="1" />
</form>