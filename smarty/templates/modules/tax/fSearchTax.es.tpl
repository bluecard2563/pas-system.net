{assign var="title" value="BUSCAR IMPUESTO"}
<form name="frmSearchTax" id="frmSearchTax" action="{$document_root}modules/tax/fUpdateTax" method="post" class="form_smarty">

	<div class="span-24 last title ">
    	Buscar Impuesto<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}     
        <div class="prepend-7 span-10 append-7  last">      
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    El Impuesto se actualiz&oacute; correctamente!
                {else}
                    No existe el Impuesto ingresado. Por favor seleccione uno de la lista.
                {/if}                                 
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line"> 
	    <div class="prepend-5 span-8 label">*Seleccione el pa&iacute;s:</div>
        <div class="span-8 append-3 last" id="countryContainer">     	                                  
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
              <option value="">- Seleccione -</option>
              {foreach from=$countryList item="l"}
              <option value="{$l.serial_cou}">{$l.name_cou}</option>
              {/foreach}
            </select>
   		</div>
	</div>
    
    <div class="span-24 last line"> 
	    <div class="prepend-5 span-8 label">*Seleccione el impuesto:</div>
        <div class="span-8 append-3 last" id="taxContainer">     	                                  
            <select name="selTax" id="selTax" title='El campo "Impuesto" es obligatorio'>
              <option value="">- Seleccione -</option>
            </select>
   		</div>
	</div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="serial_tax" id="serial_tax" value="" />
</form>