<form id="frmNewManagerDialog" name="frmNewManagerDialog" method="post" action="#">	
	<div class="span-1 span-8 prepend-1 last line">
		<ul id="alerts_NewManagerDialog" class="alerts"></ul>
	</div>
	<div class="span-10 last line line title">
		   Datos del Representante
	</div>

	<div class="span-10 last  line">
		<div class="span-3 label">* Pa&iacute;s:</div>
		<div class="span-3">
			<select name="selCountries" id="selCountries" title='El campo "Pa&iacute;s" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$exclusiveCountryList item="cl"}
				<option value="{$cl.serial_cou}">{$cl.name_cou}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-10 last  line">
		&nbsp;
	</div>

	<div class="span-10 last  line">
		<div id="exclusiveManager">
			<div class="prepend-1 span-2 label">
				<input type="checkbox" name="chkExclusive" id="chkExclusive" />
			</div>
			<div class="span-5">Representante Exclusivo</div>
		</div>
		<div id="subManager">
			<div class="prepend-1  span-9"><li>Ya existe un representante exclusivo para este pa&iacute;s.</li></div>
		</div>
	</div>

	<div class="span-10 last  line">
		<div id="officialManager">
			<div class="prepend-1 span-2 label">
				<input type="checkbox" name="chkOfficial" id="chkOfficial" />
			</div>
			<div class="span-5">
				Representante Oficial
			</div>
		</div>
		<div id="subOfficial">
			<div class="prepend-1  span-9"><li>Ya existe un representante oficial para este pa&iacute;s.</li></div>
		</div>
	</div>
	
</form>
