{assign var="title" value="ACTUALIZAR REPRESENTANTE"} 
<form name="frmUpdateManager" id="frmUpdateManager" method="post" action="{$document_root}modules/manager/pUpdateManager" class="">

	<div class="span-24 last title">
    	Actualizar Representante<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    <div class="wizard-nav prepend-6 span-14 append-4 last">
		<a href="#FirstPage"><div class="span-6">1. Datos del Representante</div></a>
        <a href="#SecondPage"><div class="span-6">2. Asignaci&oacute;n de Pa&iacute;ses</div></a>
	</div>
	{if $error}
    <div class="append-5 span-10 prepend-4 last ">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha actualizado exitosamente!
            {else} 
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div id="FirstPage" class="wizardpage span-24 last">
    	<div id="Page1" class="span-24 last">            
            <div class="span-24 last line separator">
                <label>Informaci&oacute;n Geogr&aacute;fica:</label>
            </div>
            <div class="span-24 last line">
                <div class="prepend-4 span-3 label" id="zoneContainer">* Zona:</div>
                <div class="span-5">
                    <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$zonesList item="zl"}
                            <option id="{$zl.serial_zon}" value="{$zl.serial_zon}" {if $zl.serial_zon eq $data.aux.serial_zon} selected="selected"{/if}>{$zl.name_zon}</option>
                        {/foreach}
                    </select> 
                </div>
                
                <div class="span-3 label">* Pa&iacute;s:</div>
                <div class="span-5 append-4 last" id="countryContainer">
                    <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                        <option value="4">- Seleccione -</option>
                        {foreach from=$countryList item="cl"}
                            <option id="{$cl.serial_cou}" value="{$cl.serial_cou}"{if $cl.serial_cou eq $data.aux.serial_cou} selected="selected"{/if}>{$cl.name_cou}</option>
                        {/foreach}
                    </select> 
                 </div>
            </div>
            
            <div class="span-24 last line">
                <div class="prepend-4 span-3 label" >* Ciudad:</div>
                <div class="span-5" id="cityContainer">
                    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$cityList item="cl"}
                            <option id="{$cl.serial_cit}" value="{$cl.serial_cit}"{if $cl.serial_cit eq $data.serial_cit} selected="selected"{/if}>{$cl.name_cit}</option>
                        {/foreach}
                    </select> 
                 </div>
            </div>
            
            <div class="span-24 last line separator">
                <label>Informaci&oacute;n General:</label>
            </div>
            <div class="span-24 last line">
                <div class="prepend-4 span-3 label">* Documento:</div>
                <div class="span-5">{$data.document_man}</div>
                
                <div class="span-3 label">* Nombre:</div>
                <div class="span-5 append-4 last">{$data.name_man}</div>
            </div>
            
            <div class="span-24 last line">
                <div class="prepend-4 span-3 label">* Direcci&oacute;n:</div>
                <div class="span-5"> <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatoria' value="{$data.address_man}"> </div>
                
                <div class="span-3 label">* Tel&eacute;fono:</div>
                <div class="span-5 append-4 last">  <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio' value="{$data.phone_man}"> </div>
            </div>
            
            <div class="span-24 last line">
                <div class="prepend-4 span-3 label">* Fecha del Contrato:</div>
                <div class="span-5"> <input type="text" name="txtContractDate" id="txtContractDate" title='El campo "Fecha del Contrato" es obligatorio' value="{$data.contract_date_man}"> </div>
                
                <div class="span-3 label">* Contacto:</div>
                <div class="span-5 append-4 last">  <input type="text" name="txtContact" id="txtContact" title='El campo "Contacto" es obligatorio' value="{$data.contact_man}">
                </div>
            </div>
            
            <div class="span-24 last line">
                <div class="prepend-4 span-3 label">* Tel&eacute;fono del Contacto:</div>
                <div class="span-5"> <input type="text" name="txtContactPhone" id="txtContactPhone" title='El campo "Tel&eacute;fono del Contacto" es obligatorio' value="{$data.contact_phone_man}"> </div>
                
                <div class="span-3 label">* E-mail del Contacto:</div>
                <div class="span-5 append-4 last"> <input type="text" name="txtContactEmail" id="txtContactEmail" title='El campo "E-mail del Contacto" es obligatorio' value="{$data.contact_email_man}"> </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-4 span-3 label">* Tipo:</div>
                <div class="span-5">
                     <select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$typeList item=tl}
                            <option value="{$tl}" {if $tl eq $data.type_man} selected="selected"{/if}>{if $tl eq PERSON}Persona Natural{else}Persona Jur&iacute;dica{/if}</option>
                        {/foreach}
                    </select>                 
                </div>

				<div class="span-3 label"><input type="checkbox" name="txtSalesOnlyMan" id="txtSalesOnlyMan" {if $data.sales_only_man eq 'YES'}checked{/if}></div>
				<div class="span-5 append-4 last"><b>Representante que realiza &uacute;nicamente ventas</b></div>
            </div>

			<div class="span-24 last line separator">
				<label>Informaci&oacute;n de Pagos:</label>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-3 label">* Tipo de Porcentaje:</div>
				<div class="span-5">
					<select name="selPercentageType" id="selPercentageType" title='El campo "Tipo de Porcentaje" es obligatorio'>
						<option value="">- Seleccione -</option>
						{foreach from=$typePercentageList item="l"}
						<option value="{$l}" {if $data.aux.type_percentage_mbc eq $l}selected="selected"{/if} >{$global_pType.$l}</option>
						{/foreach}
					</select>
				</div>

				<div class="span-3 label">* Fecha L&iacute;mite de Pago:</div>
				<div class="span-5 append-4 last">
					<select name="selPaymentDeadline" id="selPaymentDeadline" title='El campo "Fecha L&iacute;mite de Pago" es obligatorio'>
						<option value="">- Seleccione -</option>
						{foreach from=$paymentDeadlineList item="l"}
						<option value="{$l}" {if $data.aux.payment_deadline_mbc eq $l}selected="selected"{/if} >{$global_weekDays.$l}</option>
						{/foreach}
					</select>
				</div>
			</div>

			<div class="span-24 last line">
				<div class="prepend-4 span-3 label">* Valor Porcentaje:</div>
				<div class="span-5">
					<input type="text" name="txtPercentage" id="txtPercentage" title='El campo "Valor Porcentaje" es obligatorio' value="{$data.aux.percentage_mbc}" />
				</div>

				<div class="span-3 label">* Facturaci&oacute;n:</div>
				<div class="span-5 append-4 last">
				<select name="selInvoice" id="selInvoice" title='El campo "Facturaci&oacute;n" es obligatorio'>
					<option value="">- Seleccione -</option>
					{foreach from=$invoiceNumberList item="il"}
					<option value="{$il}" {if $data.aux.invoice_number_mbc eq $il}selected="selected"{/if}>{if $il eq AUTOMATIC}Autom&aacute;tica{else}Manual{/if}</option>
					{/foreach}
				</select>
				</div>
			</div>

        </div>
    </div>
     
    <div id="SecondPage" class="wizardpage span-24 last">
        <div id="Page2" class="span-24 last">
        	<div id="managerData"></div>
            <div id="managerHiddens"></div>            
            <div id="lastButtons"><hr /></div>

            <div class="span-24 last buttons">
				<input type="button" name="btnAssignNew" id="btnAssignNew" value="Asignar Pa&iacute;s" class="ui-state-default ui-corner-all" >
				<div class="span-24 last buttons">&nbsp;</div>
            </div>
    
            <div id="exclusiveHiddens">
                {foreach from=$exclusiveCountryList item="cl" key=k}
					{if $cl.exclusive_mbc eq YES} <input type="hidden" name="hdnExclusiveCountry_{$k}" hdnManager="" id="hdnExclusiveCountry_{$k}" value="{$cl.name_cou}" class="hiddenExclusiveCountry"/>{/if}
                {/foreach}
            </div>
            <div id="officialHiddens">
                {foreach from=$exclusiveCountryList item="cl" key=k}
					{if $cl.official_mbc eq YES} <input type="hidden" name="hdnOfficialCountry_{$k}" hdnManager="" id="hdnOfficialCountry_{$k}" value="{$cl.name_cou}" class="hiddenOfficialCountry"/>{/if}
                {/foreach}
            </div>
            <input type="hidden" name="hdnExclusiveSize" id="hdnExclusiveSize" value="{$exclusiveCountryList|@count}"/> 
            <input type="hidden" name="hdnSelectedDiv" id="hdnSelectedDiv" />
            <input type="hidden" name="hdnCont" id="hdnCont" value=""/> 
            <input type="hidden" name="hdnManagerId" id="hdnManagerId" value="{$data.serial_man}"/> 
    	</div>
    </div>  
</form>

<div id="newManagerDialog" title="Asignar Representante por pa&iacute;s">
        {include file="templates/modules/manager/fNewManagerDialog.$language.tpl"}
</div>
<div id="updateManagerDialog" title="Asignar Representante por pa&iacute;s">
        {include file="templates/modules/manager/fUpdateManagerDialog.$language.tpl"}
</div>
