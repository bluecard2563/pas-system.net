{assign var="title" value="ASIGNAR COMERCIALIZADOR"} 
<form name="frmAssignDealer" id="frmAssignDealer" method="post" action="{$document_root}modules/manager/pAssignDealer" class="">

	<div class="span-24 last title">
            Asignar Comercializador<br />
            <label>(*) Campos Obligatorios</label>
        </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-12 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Los comercializadores han sido asignados satisfactoriamente
            {else} 
                Hubo errores en la asignaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
    	&nbsp;
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneContainer">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zonesList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select> 
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-5 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="4">- Seleccione -</option>
            </select> 
         </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" >* Representante:</div>
        <div class="span-5" id="managerContainer">
            <select name="selManagerA" id="selManagerA" title='El campo "Representante" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select> 
         </div>
    </div>
    <div id="dealers">
        <div class="span-24 last line">
               &nbsp;
          </div>
        <div class="span-24 last line separator">
               <label>Seleccione los parametros:</label>
          </div>
          <div class="span-24 last line">
            &nbsp;
        </div>
        <div class="span-24 last line">
            <div class="prepend-2 span-4 label" >* Comercializadores:</div>
            <div class="span-4" id="dealersContainer">
                <select name="selDealer" id="selDealer" title='El campo "Comercializadores" es obligatorio' multiple="multiple">
                </select> 
             </div>
        </div>
        <div class="span-24 last line">  
            <div class="prepend-2 span-5 label" >* Representante al que se va a asignar:</div>
            <div class="span-4" id="newManagerContainer">
                <select name="selManagerB" id="selManagerB" title='El campo "Representante" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$managerList item="ml"}
                        <option id="{$ml.serial_man}" value="{$ml.serial_man}">{$ml.name_man}</option>
                    {/foreach}
                </select> 
             </div>
          </div>
          <div class="span-24 last line">
               &nbsp;
          </div>
          <div class="span-24 last buttons line">
               <input type="submit" name="btnAssign" id="btnAssign" value="Asignar"/>
          </div>
          <div class="span-24 last buttons">&nbsp;</div>
    </div>

    <div class="span-24 last line">
               &nbsp;
    </div>
    <div class="span-24 last line" id="noDealers">
        <div class="prepend-4 span-20 label" >En este pa&iacute;s no existen m&aacute;s representantes a los cu&aacute;les se les pueda asignar comercializadores.</div>
    </div>
    
</form>
