{assign var="title" value="BUSCAR REPRESENTANTE"}
<div class="span-24 last title">Buscar Representante</div>
<form name="frmSearchManager" id="frmSearchManager" action="{$document_root}modules/manager/fUpdateManager" method="post" />	
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El Representante ingresado no existe. 
                {elseif $error eq 4}
                    Hubo errores al enviar el e-mail con la informaci&oacute;n del Representante. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-3 span-8 label">Ingrese el nombre o documento del Representante:</div>
        <div class="span-11 append-2 last">
            <input type="text" name="txtManagerData" id="txtManagerData" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
        </div>
	</div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnManagerID" id="hdnManagerID" value="" />
</form>