<form id="frmUpdateManagerDialog" name="frmUpdateManagerDialog" method="post" action="#" >	
	<div class="span-1 span-8 prepend-1 last line">
		<ul id="alerts_UpdateManagerDialog" class="alerts"></ul>
	</div>

	<div class="span-10 last line line title">
		Datos del Representante
	</div>
	
	<div class="span-10 last  line">
		&nbsp;
	</div>
     		
	<div class="span-10 last  line">
		<div class="span-3 label">* Pa&iacute;s:</div>
		<div class="span-3 append-2">
			<select name="selCountriesUpdate" id="selCountriesUpdate" title='El campo "Pa&iacute;s" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$exclusiveCountryList item="cl"}
				<option value="{$cl.serial_cou}">{$cl.name_cou}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-10 last  line">
		<div class="span-3 label">* Estado:</div>
		<div class="span-3 append-2">
			<select name="selStatusUpdate" id="selStatusUpdate" title='El campo "Estado" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$statusList item="sl"}
				<option value="{$sl}">{$global_status.$sl}</option>
				{/foreach}
			</select>
		</div>
	</div>
            
	<div class="span-10 last  line">
		&nbsp;
	</div>

	<div class="span-10 last  line">
		<div id="exclusiveManagerUpdate">
			<div class="prepend-1 span-2 label">
				<input type="checkbox" name="chkExclusiveUpdate" id="chkExclusiveUpdate" />
			</div>
			<div class="span-5">Representante Exclusivo</div>
		</div>
		<div id="subManagerUpdate">
			<div class="prepend-1  span-9"><li>Ya existe un representante exclusivo para este pa&iacute;s.</li></div>
		</div>
	</div>

	<div class="span-10 last  line">
		<div id="officialManagerUpdate">
			<div class="prepend-1 span-2 label">
				<input type="checkbox" name="chkOfficialUpdate" id="chkOfficialUpdate" />
			</div>
			<div class="span-5">
				Representante Oficial
			</div>
		</div>
		<div id="subOfficialUpdate">
			<div class="prepend-1  span-9"><li>Ya existe un representante oficial para este pa&iacute;s.</li></div>
		</div>
	</div>

</form>