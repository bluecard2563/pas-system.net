{assign var="title" value="REASIGNAR COMERCIALIZADORES A REPRESENTANTE"}
<form name="frmReAssignDealer" id="frmReAssignDealer" method="post" action="{$document_root}modules/manager/pReAssignDealer" class="">
	<div class="span-24 last title ">
    	Reasignaci&oacute;n de comercializadores a representante<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	Los comercializadores se han reasignado exitosamente!
        {elseif $error eq 2}
            	No se pudo reasignar los comercializadores.
         {elseif $error eq 3}
            	No se seleccion&oacute; ning&uacute;n comercializador.
        {/if}
        </div>
    </div>
    {/if}

   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
    <div class="span-24 last line"></div>
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
     	<div class="prepend-4 span-4 label">* Estado:</div>
        <div class="span-5">
            <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
                <option value="">- Seleccione -</option>
				{foreach from=$dealer_status item="s"}
				<option value="{$s}">{$global_status.$s}</option>
				{/foreach}
            </select>
        </div>
    </div>

	<div class="span-24 last line">
     	<div class="prepend-4 span-4 label">*Representante Origen:</div>
        <div class="span-5" id="managerContainerSource">
            <select name="selManagerSource" id="selManagerSource" title='El campo "Representante Origen" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4  label">* Representante Destino:</div>
        <div class="span-5" id="managerContainerDestiny">
        	<select name="selManagerDestiny" id="selManagerDestiny" title='El campo "Representante Destino" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-5 span-6 label">*Seleccione los comercializadores que desea reasignar:</div>
        <div class="prepend-7 span-10 append-4 last line" id="dealerContainer">
            <div class="span-4">
                <div align="center">Seleccionados</div>
                <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-4 last" title="El campo de sucursales de comercializador es obligatorio."></select>
            </div>
            <div class="span-2 last buttonPane">
                <br />
                <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
            </div>
            <div class="span-4 last" id="dealersFromContainer">
                <div align="center">Existentes</div>
                <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last">
                </select>
            </div>
        </div>
    </div>


    <div class="span-24 last buttons line">
        <input type="submit" name="btnReAssign" id="btnReAssign" value="Reasignar" />
        <input type="hidden" name="hdnSerial_usu" id="hdnSerial_usu" value="" />
    </div>
</form>