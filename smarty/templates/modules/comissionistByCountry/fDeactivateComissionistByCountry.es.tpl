{assign var="title" value="RESPONSABLES ACTIVOS DE PA&Iacute;S"}
<form name="frmDeactivateComissionistByCountry" id="frmDeactivateComissionistByCountry" method="post" action="{$document_root}modules/comissionistByCountry/pDeactivateComissionistByCountry" class="">
	<div class="span-24 last title ">
    	Responsables Activos de Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	Responsables actualizados exitosamente.
        {elseif $error eq 2}
            	Hubieron errores al actualizar los responsables.
        {elseif $error eq 3}
            	No se selecciono ningun usuario.
        {/if}
        </div>
    </div>
    {/if}

   <div class="append-7 span-10 prepend-7 last">
       <ul id="alerts" class="alerts"></ul>
   </div>

   <div class="span-24 last line separator">
    	<label>*Seleccionar usuario:</label>
   </div>
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountryUser" id="selCountryUser" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-4 label">Responsables:</div>
        <div class="span-5" id="comissionistList" style="border: solid 1px silver; width:175px;  height: 150px; overflow: auto; padding: 5px;">
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Aceptar" />
    </div>
</form>