{assign var="title" value="ASIGNAR RESPONSABLE DE PA&Iacute;S"}
<form name="frmNewComissionistByCountry" id="frmNewComissionistByCountry" method="post" action="{$document_root}modules/comissionistByCountry/pNewComissionistByCountry" class="">
	<div class="span-24 last title ">
    	Asignaci&oacute;n de Responsable de Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	El responsable se ha asignado exitosamente!
        {elseif $error eq 2}
            	No se pudo asignar el responsable.
        {/if}
        </div>
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
   
   <div class="span-24 last line separator">
    	<label>*Seleccionar usuario:</label>
   </div>
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">  
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">  
        	<select name="selCountryUser" id="selCountryUser" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Seleccione el usuario:</div>
        <div class="span-5" id="userContainer">  
            <select name="selUser" id="selUser" title='El campo "Usuario" es obligatorio.' >
            <option value="">- Seleccione -</option>
        </select>
        </div>
        
        <div class="span-3 label">* Comisi&oacute;n:</div>
        <div class="span-4 append-4 last" id="countryContainer">  
            <input type="text" name="txtComission" id="txtComission" title='El campo "Comisi&oacute;n" es obligatorio.' />
        </div>
    </div>
    
    <div class="span-24 last line separator">
        <label>*Asignar pa&iacute;s:</label>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">  
            <select name="selZoneAssign" id="selZoneAssign" title='El campo "Zona" en "Asignar pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryAssignContainer">  
            <select name="selCountryAssign" id="selCountryAssign" title='El campo "Pa&iacute;s" en "Asignar pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    
    <div class="span-4 append-4 last"> 
        <div class="span-24 last line separator">
            <label>Asignar supervisor:</label>
        </div>
        
        <div class="span-24 last line">
            <div class="prepend-7 span-6 label">Seleccione el supervisor:</div>
            <div class="span-5" id="supervisorContainer">  
                <select name="selComissionist" id="selComissionist" title='El campo "Usuario" es obligatorio.' >
                <option value="">- Seleccione -</option>
            </select>
            </div>
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" />
        <input type="hidden" name="hdnValidateFlag" id="hdnValidateFlag" />
    </div>
</form>