{assign var="title" value="NUEVO POP-UP"}
<div class="span-24 last title">{if $$add_id} Actualizar {else} Nuevo {/if} Pop-Up<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="append-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmNewPopUp" id="frmNewPopUp" method="post" action="{$document_root}modules/customPopUp/pManagePopUp" enctype="multipart/form-data" >
	<!--BASIC INFO-->
    <div class="span-24 last line separator">
		<label>Informaci&oacute;n General:</label>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-3 span-6 label">
			* T&iacute;tulo:
		</div>
		
		<div class="span-6 append-6 last">
			<input type="text" class="span-6" name="txtTitle" id="txtTitle" title="El campo es 'T&iacute;tulo' obligatorio" {if $currentAdd.title}value="{$currentAdd.title}"{/if} />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-3 span-6 label">
			* Arte:
		</div>
		
		<div class="span-6 append-6 last">
			<input type="file" class="span-6" name="txtArt" id="txtArt" title="El campo es 'Arte' obligatorio" {if $currentAdd.image}value="{$currentAdd.image}"{/if} />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-3 span-6 label">
			* Fecha Publicaci&oacute;n:
		</div>
		
		<div class="span-6 append-6 last">
			<input type="text" class="span-5" name="txtPublishDate" id="txtPublishDate" title="El campo es 'Fecha Publicaci&oacute;n' obligatorio" {if $currentAdd.publishDate}value="{$currentAdd.publishDate}"{/if} />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-3 span-6 label">
			* Fecha Expiraci&oacute;n:
		</div>
		
		<div class="span-6 append-6 last">
			<input type="text" class="span-5" name="txtExpirationDate" id="txtExpirationDate" title="El campo es 'Fecha Expiraci&oacute;n:' obligatorio" {if $currentAdd.expireDate}value="{$currentAdd.expireDate}"{/if} />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-3 span-6 label">
			Link Web:
		</div>
		
		<div class="span-6 append-6 last">
			<input type="text" class="span-6" name="txtLink" id="txtLink" title="El campo es 'Link Web' obligatorio" {if $currentAdd.webLink}value="{$currentAdd.webLink}"{/if} />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-3 span-6 label">
			Archivo PDF:
		</div>
		
		<div class="span-6 append-6 last">
			<input type="file" class="span-6" name="txtPDF" id="txtPDF" title="El campo es 'Archivo PDF' obligatorio" />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-6 span-6 append-12 last line label">
			Descripci&oacute;n:
		</div>
		
		<div class="prepend-3 span-16 append-3 last">
			<textarea name="arDescription" id="arDescription"></textarea>
		</div>
	</div>
	
	<!-- SCOPE -->
    <div class="span-24 last line separator">
		<label>Alcance:</label>
    </div>
	
	<div class="span-24 last line center">
		<input type="radio" name="rdScope" id="rdScope_manager" value="MANAGER" checked="checked" /> Representante
		<input type="radio" name="rdScope" id="rdScope_responsible" value="RESPONSIBLE" /> Responsable Comercial
	</div>
	
	<div class="span-24 last line" id="scopeManager">
		<div class="prepend-6 span-12 append-6 last line">
			<div class="span-6 label">
				Representante:
			</div>
			<div class="span-6 last">
				{foreach from=$mbcEC item="mbc"}
					<input type="checkbox" name="chManager[]" id="chManager_{$mbc.serial_mbc}" value="{$mbc.serial_mbc}" 
							{if $scopeType == 'MANAGER'} 
								{foreach from=$scopes item="saved"}
									{if $saved.serial_mbc == $mbc.serial_mbc}
										checked="checked"
									{/if}
								{/foreach}
							{/if} /> {$mbc.name_man|htmlall} <br />
				{/foreach}
			</div>
		</div>
	</div>
				
	<div class="span-24 last line" id="scopeResponsible" style="display:none;">
		<div class="prepend-6 span-12 append-6 last line">
			<div class="span-6 label">
				Responsable:
			</div>
			<div class="span-6 last">
				{foreach from=$responsibleList item="res"}
					<input type="checkbox" name="chResponsible[]" id="chResponsible_{$res.serial_usr}" value="{$res.serial_usr}" 
							{if $scopeType == 'RESPONSIBLE'} 
								{foreach from=$scopes item="saved"}
									{if $saved.responsibleID == $res.serial_usr}
										checked="checked"
									{/if}
								{/foreach}
							{/if} /> {$res.name_usr|htmlall}<br />
				{/foreach}
			</div>
		</div>
	</div>
	
	
	<div class="span-24 last line buttons">
		<input type="button" id="btnSubmit" value="Procesar" />
		<input type="hidden" name="hdnID" id="hdnID" value="{$add_id}" />
		<input type="hidden" name="hdnScopeSelected" id="hdnScopeSelected" value="" />
	</div>
</form>