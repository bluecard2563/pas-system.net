{assign var="title" value="ADMINISTRAR POP-UPS"}
<div class="span-24 last title">Administrar Pop-Ups<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	 <div class="append-7 span-10 prepend-7 last ">
		<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Creaci&oacute;n exitosa!
			{elseif $error eq 2}
				La extensi&oacute;n de la im&aacute;gen no es correcta. El proceso fue interrumpido.
			{elseif $error eq 3}
				La extensi&oacute;n del archivo no es un PDF. El proceso fue interrumpido.
			{elseif $error eq 4}
				Hubo errores al registrar el Pop-Up comun&iacute;quese con el administrador.
			{elseif $error eq 5}
				No se puede crear el Pop-Up debido a que uno de los archivos no fue cargado al sistema. Vuelva a intentarlo.
			{elseif $error eq 6}
				Hubo errores al ingresar el &aacute;mbito del PopUp. Por favor verif&iacute;quelo.
			{/if}
		</div>
	</div>
{/if}

<div class="prepend-3 span-12 append-5 last line">
	<a href="{$document_root}modules/customPopUp/fNewPopUp">NUEVO POP UP</a>
</div>

{if $popUps}
<div class="prepend-1 span-22 append-1 last line">
	<table border="1" id="tblPopUps" class="dataTable">
		<thead>
			<tr class="header" >
				<th align="center" >
					No.
				</th>
				<th align="center">
					T&iacute;tulo
				</th>
				<th align="center">
					Fecha Publicaci&oacute;n
				</th>
				<th align="center">
					Fecha Expiraci&oacute;n
				</th>
				<th align="center">
				</th>
			</tr>
		</thead>
		<tbody>
			{foreach name="pop" from=$popUps item="p"}
			<tr {if $smarty.foreach.pop.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.pop.iteration}
				</td>
				<td align="center" class="tableField">
					{$p.title|htmlall}
				</td>
				<td align="center" class="tableField">
					{$p.publishDate}
				</td>
				<td align="center" class="tableField">
					{$p.expireDate}
				</td>
				<td align="center" class="tableField">
					<a href="{$document_root}modules/customPopUp/fNewPopUp/{$p.id}">Actualizar</a>
				</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
</div>
{/if}