{assign var="title" value="NUEVO OVER"}
<form name="frmNewOver" id="frmNewOver" method="post" action="{$document_root}modules/overs/pNewOver">
    <div class="span-24 last title">
    	Registro de Nuevo Over<br>
          <label>(*) Campos Obligatorios</label>
    </div>

    <div class="wizard-nav span-24 last line">
    	<div class="prepend-5 span-1">&nbsp;</div>
         <a href="#newOver"><div class="span-6">1. Creaci&oacute;n de Over</div></a>
         <a href="#assignOver"><div class="span-6">2. Asignaci&oacute;n de Over</div></a>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            El Over se ha registrado exitosamente!
        {elseif $error eq 2}
            Hubo errores en el ingreso del Over. Por favor vuelva a intentarlo.
        {elseif $error eq 3}
            No se pudo asociar el Over al representante seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 4}
            No se pudo asociar el Over al comercializador. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 5}
            No se pudo almacenar el Over para el representante seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 6}
            No se pudo almacenar el Over para el comercializador seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 7}
            El rango de d&iacute;as del Over. no es v&aacute;lido, debe ser ascendente, ejemplo: 1,4-5,15-17,19,25  .
        {/if}
        </div>
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div id="newOver" class="wizardpage span-24 last sectionPage">
        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">* Nombre:</div>
            <div class="span-5 append-6 last">  <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.'> </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Inicio de Vigencia:</div>
            <div class="span-5">
                <input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Inicio de Vigencia:" es obligatorio'>
            </div>
            <div class="span-3 label">* Fin de Vigencia: </div>
            <div class="span-5">
                <input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fin de Vigencia:" es obligatorio'>
            </div>
        </div>
		
		<div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Inicio de An&aacute;lisis:</div>
            <div class="span-5">
                <input type="text" name="txtInfoBeginDate" id="txtInfoBeginDate" disabled="" title='El campo "Inicio de An&aacute;lisis" es obligatorio'>
            </div>
            <div class="span-3 label">* Fin de An&aacute;lisis: </div>
            <div class="span-5">
                <input type="text" name="txtInfoEndDate" id="txtInfoEndDate" disabled="" title='El campo "Fin de An&aacute;lisis" es obligatorio'>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Porcentaje de Incremento:</div>
            <div class="span-5">
                <input type="text" name="increasePer" id="increasePer" title='El campo "Porcentaje de Incremento" es obligatorio'>
            </div>
            <div class="span-3 label">* Comisi&oacute;n del Over: </div>
            <div class="span-5">
                <input type="text" name="comissionPer" id="comissionPer" title='El campo "Comisi&oacute;n del Over" es obligatorio'>
            </div>
        </div>
    </div>
    
    <div id="assignOver" class="wizardpage span-24 last line">
        <div class="prepend-7 span-5 label">* Over Asignado a: </div>
        <div class="span-6 append-6 last line">
            <select name="selApliedTo" id="selApliedTo" title='El campo "Asignado a" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$apliedTo item="l"}
                <option value="{$l}">{$global_appliedToTypes.$l}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-24 last line sectionPage" id="appliedToContainer"></div>
    </div>
</form>