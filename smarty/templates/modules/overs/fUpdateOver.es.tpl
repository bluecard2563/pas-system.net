{assign var="title" value="EDITAR OVER"}
<form name="frmNewOver" id="frmNewOver" method="post" action="{$document_root}modules/overs/pUpdateOver">
    <div class="span-24 last title">
    	Editar Over<br>
          <label>(*) Campos Obligatorios</label>
    </div>

    <div class="wizard-nav span-24 last line">
    	<div class="prepend-5 span-1">&nbsp;</div>
         <a href="#newOver"><div class="span-6">1. Datos del Over</div></a>
         <a href="#assignOver"><div class="span-6">2. Asignaci&oacute;n de Over</div></a>
    </div>

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div id="newOver" class="wizardpage span-24 last sectionPage">
        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">* Nombre:</div>
            <div class="span-5 append-6 last">  <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' value="{$data.name}"> </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Inicio de Vigencia:</div>
            <div class="span-5">
                <input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Inicio de Vigencia:" es obligatorio' value="{$data.beginDate}">
            </div>
            <div class="span-3 label">* Fin de Vigencia: </div>
            <div class="span-5">
                <input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fin de Vigencia:" es obligatorio' value="{$data.endDate}">
            </div>
        </div>
		
		<div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Inicio de An&aacute;lisis:</div>
            <div class="span-5">
                <input disabled type="text" name="txtInfoBeginDate" id="txtInfoBeginDate" title='El campo "Inicio de An&aacute;lisis" es obligatorio' value="{$info_data.begin_date_ino}" />
            </div>
            <div class="span-3 label">* Fin de An&aacute;lisis: </div>
            <div class="span-5">
                <input disabled type="text" name="txtInfoEndDate" id="txtInfoEndDate" title='El campo "Fin de An&aacute;lisis" es obligatorio' value="{$info_data.end_date_ino}" />
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Porcentaje de Incremento:</div>
            <div class="span-5">
                <input type="text" name="increasePer" id="increasePer" title='El campo "Porcentaje de Incremento" es obligatorio' value="{$data.percentage_increase_ove}">
            </div>
            <div class="span-3 label">* Comisi&oacute;n del Over: </div>
            <div class="span-5">
                <input type="text" name="comissionPer" id="comissionPer" title='El campo "Comisi&oacute;n del Over" es obligatorio' value="{$data.percentage_comission_ove}">
            </div>
        </div>
    </div>
    
    <div id="assignOver" class="wizardpage span-24 last line">
        <div class="prepend-7 span-5 label">* Over Asignado a: </div>
        <div class="span-6 append-6 last line">
            <select name="selApliedTo" id="selApliedTo" title='El campo "Asignado a" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$apliedTo item="l"}
                	<option value="{$l}">{$global_appliedToTypes.$l}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="span-24 last line sectionPage" id="appliedToContainer"></div>
    </div>

     <input type="hidden" name="serial_ove" id="serial_ove" value="{$data.serial_ove}">
</form>
<script type="text/javascript">
	loadAppliedTo('{$data.appliedTo}','{$data.serial_ove}','{$data.managerCountry}','{$data.managerId}');
</script>