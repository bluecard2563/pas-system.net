{assign var="title" value="BUSCAR OVER"}
<form name="frmSearchOver" id="frmSearchOver" method="post" action="{$document_root}modules/overs/fUpdateOver">
    <div class="span-24 last title">
    	B&uacute;squeda de Over<br>
          <label>(*) Campos Obligatorios</label>
    </div>
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
   	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            El Over se ha actualizado exitosamente!
        {elseif $error eq 2}
			El over seleccionado no existe. Por favor vuelva a seleccionar el Over. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 3}
			La informaci&oacute;n b&aacute;sica del over no se pudo actualizar. El proceso no concluy&oacute;.
        {elseif $error eq 4}
			El proceso no se llev&oacute; a cabo ya que no se pudieron desactivar los registros anteriores. Vuelva a intentarlo.
        {elseif $error eq 5}
            No se pudo asociar el Over con el representante/comercializador seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 6}
            No se pudo registrar la vigencia del over. Por favor comun&iacute;quese con el administrador.
        {/if}
        </div>
    </div>

    {/if} 

	<div class="prepend-4 span-17 last line">
		<table border="1" class="dataTable" id="overs_table">
			<thead>
				<tr class="header">
					<th class="center">&nbsp;</th>
					<th class="center">
						Nombre
					</th>
					<th class="center">
						Aplicado a
					</th>
					<th class="center">
						Fecha de Creaci&oacute;n
					</th>
					<th class="center">
						Estado
					</th>
				</tr>
			</thead>

			<tbody>
			{foreach name="overs" from=$over_active item="o"}
			<tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.overs.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
				<td class="tableField">
					<input type="radio" name="radOver" id="radOver_{$o.serial_ove}"  value="{$o.serial_ove}"/>
				</td>
				<td class="tableField">
					{$o.name_ove}
				</td>
				<td class="tableField">
					{$global_overAppliedTo[$o.applied_to_ove]}
				</td>
				<td class="tableField">
					{$o.creation_date_ove}
				</td>
				<td class="tableField">
					{$global_status[$o.status_ove]}
				</td>
			</tr>
			{/foreach}
			</tbody>
		</table>
	</div>

    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Actualizar" >
		<input type="hidden" id="selectedOver" name="selectedOver" value="" title="Debe seleccionar un Over">
    </div>

	<div class="span-24 last line" id="preview_container"></div>
</form>