{assign var="title" value="ACTIVAR OVER"}
    <div class="span-24 last title">
    	ACTIVACI&Oacute;N de Over<br>
          <label>(*) Campos Obligatorios</label>
    </div>
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            Activaci&oacute;n exitosa!
        {elseif $error eq 2}
            La activaci&oacute;n fall&oacute;, por favor intentelo nuevamente.
        {/if}
        </div>
    </div>
    {/if}
<form name="frmActivateOver" id="frmActivateOver" method="post" action="{$document_root}modules/overs/pActivateListOvers">
	
	{if $oversToActivate}
		<div class="span-24 last title">
		   	OVERS PENDIENTES POR ACTIVAR
		</div>
		<div class="span-24 last line">
		   	<div class="span-1 pushLeft">No</div>
		   	<div class="span-1 pushLeft">Sel</div>
		   	<div class="span-6 pushLeft">Nombre</div>
		   	<div class="span-3 pushLeft">Tipo</div>
		    <div class="span-7 pushLeft">Periodo</div>
		    <div class="span-2 pushLeft">Crecimiento</div>
		    <div class="span-2 pushLeft">Comisi&oacute;n</div>
		</div>
		<hr />
		<div id="all-overs-list">
			{foreach from=$oversToActivate item="ove" key=cid name="overs"}
				<div class="span-24 last line table_record" id="banner_{$ove.serial_ove}">
					<div class="span-1 list_text" id="num_{$ove.serial_ove}">{$smarty.foreach.overs.iteration}</div>
					<div class="span-1 list_text" id="chk_{$ove.serial_ove}"><input type=checkbox name="overActivate_{$ove.serial_ove}" id="overActivate_{$ove.serial_ove}"></div>
					<div class="span-6 list_text" id="name_{$ove.serial_ove}">{$ove.name_ove}</div>
			    	<div class="span-3 list_text" id="type_{$ove.serial_ove}">{$global_overAppliedTo[$ove.applied_to_ove]}</div>
			    	<div class="span-7 list_text" id="per_{$ove.serial_ove}">{$ove.begin_date_ove}  al  {$ove.end_date_ove}</div>
		        	<div class="span-2 list_text" id="inc_{$ove.serial_ove}">{$ove.percentage_increase_ove}%</div>
		        	<div class="span-2 last list_text" id="pay_{$ove.serial_ove}">{$ove.percentage_comission_ove}%</div>
				</div>
			{/foreach}
		</div>
		<div class="prepend-11 span-2 last title">
	    	<input type="submit" name="btnActivate" id="btnActivate" value="Activar" >
	    </div>
	{else}
		<hr/>
		<div class="span-17 prepend-9 last line pushLeft">
		   	NO EXISTEN OVERS POR ACTIVAR AL MOMENTO
		</div>
	{/if}
</form>