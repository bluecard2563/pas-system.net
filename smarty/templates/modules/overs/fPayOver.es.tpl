{assign var="title" value="OVERS - AUTORIZACI&Oacute;N DE PAGO"}
<form name="frmPayOver" id="frmPayOver" method="post" action="">
    <div class="span-24 last title">
    	Autorizaci&oacute;n de Pago de Overs<br>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            La autorizacion fue registrada correctamente!
			{elseif $error eq 2}
            Hubo errores en el registro de la autorizaci&oacute;n. Por favor vuelva a intentarlo.
			{elseif $error eq 3}
            No se pudo asociar el Over al representante seleccionado. Por favor comun&iacute;quese con el administrador.
			{elseif $error eq 4}
            No se pudo asociar el Over al comercializador. Por favor comun&iacute;quese con el administrador.
			{elseif $error eq 5}
            No se pudo almacenar el Over para el representante seleccionado. Por favor comun&iacute;quese con el administrador.
			{elseif $error eq 6}
            No se pudo almacenar el Over para el comercializador seleccionado. Por favor comun&iacute;quese con el administrador.
			{elseif $error eq 7}
            El rango de d&iacute;as del Over. no es v&aacute;lido, debe ser ascendente, ejemplo: 1,4-5,15-17,19,25  .
			{/if}
        </div>
    </div>
    {/if}

	<div class="span-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Filtros de B&uacute;squeda</label>
    </div>

	<div class="span-24 line last">
		<div class="span-12" align="center">
			<div class="prepend-5 span-3 label">* Over Asignado a: </div>
			<div class="span-3">
			<select name="selApliedTo" id="selApliedTo" title='El campo "ASIGNADO A" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$apliedTo item="l"}
				<option value="{$l}">{$global_appliedToTypes.$l}</option>
				{/foreach}
			</select>
			</div>
		</div>
		<div class="span-12 last" align="center">
			<div class="span-3  label">* Pa&iacute;s: </div>
			<div class="span-4" id="countryContainer">
				<select name="selCountry" id="selCountry" title='El campo "PAIS" es obligatorio.' >
				<option value="">- Seleccione -</option>
				{foreach from=$loadedCountries item=cou}
				<option value="{$cou.serial_cou}">{$cou.name_cou}</option>
				{/foreach}
				</select>
			</div>
		</div>
	</div>
	<div class="span-24 line last">
		<div class="span-15" align="center">
			<div class="prepend-5 span-3 label">* Over: </div>
			<div class="span-5 last" id="overContainer" align="left">
				<select name="selOver" id="selOver" title='El campo "OVER" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>
	</div>
	<div class="span-24 line"></div>
	<div class="span-24 buttons last" align="center">
		<input type="submit" name="btnFiltrar" id="btnFiltrar" value="Filtrar" >
	</div>
</form>

<div class="span-24 last line separator">
    <label>Resultados</label>
</div>

<form name="frmAuthorizeOver" id="frmAuthorizeOver" method="post" action="{$document_root}modules/overs/pAuthorizePrintOvers" target="_blank">
	<div id="oversPayResults"></div>
</form>