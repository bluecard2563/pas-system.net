<!--/*
File: fNewStock.tpl
Author: Nicolas Flores
Creation Date: 06/01/2010
Last Modified: 06/01/2010
*/-->
{assign var="title" value="NUEVO STOCK"} 
<form name="frmNewStock" id="frmNewStock" method="post" action="{$document_root}modules/stock/pNewStock">
	<div class="span-24 last title">
    	Registro de Nuevo Stock<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last" id="messageContainer">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El stock se ha creado exitosamente!
				{if $from && $to}
                Rangos:<br />
                Desde: {$from} Hasta: {$to}
                {/if}
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
		<div class="prepend-8 span-3 label">* Tipo:</div>
    	<div class="span-5">
        	<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=sl}
					{if $sl neq 'VIRTUAL'}
					<option value="{$sl}" selected>{$sl|lower|capitalize}</option>
					{/if}
                {/foreach}
            </select>
        </div>
	</div>
	
	<div class="span-24 last line">
        <div class="prepend-8 span-3 label">* Cantidad:</div>
        <div class="span-5 last">  <input type="text" name="txtAmountStock" id="txtAmountStock" title="El campo 'Cantidad' es obligatorio"> </div>
	</div>




    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
</form>