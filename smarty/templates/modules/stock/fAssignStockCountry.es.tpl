<!--/*
File: fAssignStockCountry.es.tpl
Author: Nicolas Flores
Creation Date: 06/01/2010
Last Modified: 06/01/2010
*/-->
{assign var="title" value="ASIGNAR STOCK A PAIS"} 
<form name="frmAssignStockCountry" id="frmAssignStockCountry" method="post" action="{$document_root}modules/stock/pAssignStockCountry">
	<div class="span-24 last line title">
    	Asignar Stock a Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-7 span-10 prepend-7 last " id="messageContainer">
    	{if $error eq 1}
           <div class="span-10 success" align="center">
            	Stock asignado exitosamente!<br/>
                {if $assigned_ranges}
					Rangos:
						{foreach from=$assigned_ranges item="ar"}
							<br />Desde: {$ar.from} Hasta: {$ar.to}
						{/foreach}
                {/if}
            </div>
        {elseif $error eq 2}
            <div class="span-10 error" align="center">
            	Hubo errores en la asignaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
        {else}
            <div class="span-10 error" align="center">
            	No existe el stock suficiente para asignar al pais. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
 {/if}
     <div class="span-7 span-10 prepend-7 last line">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
     <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-5"> <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$nameZoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
   </div>
    
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">* Tipo:</div>
    	<div class="span-5">
        	<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=sl}
                	{if $sl neq 'VIRTUAL'}
						<option value="{$sl}" selected>{$sl|lower|capitalize}</option>
					{/if}
                {/foreach}
            </select>
        </div>
        
        <div class="span-3 label">* Cantidad:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtAmountStock" id="txtAmountStock" title="El campo 'Cantidad' es obligatorio"> </div>
   </div>

    <div class="span-24 center last line">
		{foreach from=$typeList item="st"}
			   Stock {$st|lower|capitalize} Disponible:{$availableStock.$st} <input type="hidden" id="{$st}" value="{$availableStock.$st}"/><br />
		{/foreach}
    </div>
 
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
</form>