<!--/*
File: fAssignStockBranch.es.tpl
Author: Nicolas Flores
Creation Date: 14/01/2010
Last Modified: 25/01/2010
*/-->
{assign var="title" value="ASIGNAR STOCK A SUCURSAL DE COMERCIALIZADOR"}
<form name="frmAssignStockBranch" id="frmAssignStockBranch" method="post" action="{$document_root}modules/stock/pAssignStockBranch">
	<div class="span-24 last title">
    	Asignar Stock a Sucursal de Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-7 span-10 prepend-7 last " id="messageContainer">
    	{if $error eq 1}
           <div class="span-10 success" align="center">
            	Stock asignado exitosamente!<br/>
                {if $assigned_ranges}
               		Rangos:
					{foreach from=$assigned_ranges item="ar"}
						<br />Desde: {$ar.from} Hasta: {$ar.to}
					{/foreach}
                {/if}
				{if $documentURL}
					<a id="openStockDocument" url="{$documentURL}" href="{$document_root}system_temp_files/{$documentURL}" target="_blank"><br /><br />Imprimir acta de asignaci&oacute;n de stock.</a>
				{/if}
            </div>
        {elseif $error eq 2}
            <div class="span-10 error" align="center">
            	El stock fue asignado. Pero los kits no fueron creados adecuadamente.
            </div>
        {elseif $error eq 3}
            <div class="span-10 error" align="center">
            	El stock fue asignado. Sin embargo el detalle de los kits no se pudo crear.
            </div>
		{elseif $error eq 35}
            <div class="span-10 error" align="center">
            	No se pudo asignar el stock. Comun&iacute;quese con el administrador.
            </div>
        {/if}
    </div>
 {/if}
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5"> <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
	
    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Comercializador:</div>
        <div class="span-4 append-4 last" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
	</div>

    <div class="span-24 last line">
         <div class="prepend-6 span-5 label" >* Sucursal de Comercializador:</div>
        <div class="span-5" id="branchContainer">
            <select name="selBranch" id="selBranch" title="El campo 'Sucursal de Comercializador' es obligatorio.">
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

	<div class="span-24 last line">
         <div class="prepend-7 span-4 label" >* Tipo:</div>
        <div class="span-5">
            <select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=sl}
                	{if $sl neq 'VIRTUAL'}
						<option value="{$sl}" selected>{$sl|lower|capitalize}</option>
					{/if}
                {/foreach}
            </select>
        </div>
    </div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Cantidad</div>
		<div class="span-4 append-2 last">
			 <input type="text" name="txtAmountStock" id="txtAmountStock" title="El campo 'Cantidad' es obligatorio">
		</div>
	</div>

    <div class="span-24 center last line" id="availableContainer">

    </div>

    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
</form>