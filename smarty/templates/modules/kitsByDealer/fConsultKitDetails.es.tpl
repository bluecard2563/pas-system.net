<!--/*
File: fAssignKit.es.tpl
Author: Santiago Ben�tez
Creation Date: 22/02/2010
Last Modified: 22/02/2010
Modified by: Santiago Ben�tez
*/-->
{assign var="title" value="CONSULTA DE KITS"}
<form name="frmConsultKitDetails" id="frmConsultKitDetails" method="post" action="{$document_root}modules/kitsByDealer/fAssignKitByDealer">
	<div class="span-24 last title">
    	Consulta de Kits<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	<div class="span-24 last line separator">
		<label>Sucursal de Comercializador:</label>
	</div>

     <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-5"> <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-5 append-3 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
         </div>
   </div>

    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">* Representate:</div>
    	<div class="span-5" id="managerContainer">
        	<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
            	<option value="">- Seleccione -</option>
            </select>
        </div>
   </div>

	<div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Comercializador:</div>
    	<div class="span-5" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title="El campo 'Comercializador' es obligatorio.">
            	<option value="">- Seleccione -</option>
            </select>
        </div>
   </div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Sucursal:</div>
		<div class="span-5" id="branchContainer">
			<select name="selBranch" id="selBranch" title="El campo 'Sucursal' es obligatorio.">
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div id="kitsContainer" class="span-24 last line"></div>
	<div id="detailsContainer" class="span-24 last line"></div>
</form>