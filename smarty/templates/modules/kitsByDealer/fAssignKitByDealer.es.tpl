<!--/*
File: fAssignKit.es.tpl
Author: Santiago Ben�tez
Creation Date: 22/02/2010
Last Modified: 11/08/2010
Modified by: David Bergmann
*/-->
{assign var="title" value="ASIGNACI&Oacute;N DE KITS"}
<form name="frmAssignKitByDealer" id="frmAssignKitByDealer" method="post" action="{$document_root}modules/kitsByDealer/pAssignKitByDealer">
	<div class="span-24 last title">
    	Asignaci&oacute;n de Kits Por Sucursal de Comercializador<br />
        <label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-7 span-10 prepend-7 last " id="messageContainer">
    	{if $error eq 1}
           <div class="span-12 success" align="center">
            	Kits asignados exitosamente!
            </div>
        {elseif $error eq 2}
            <div class="span-12 error" align="center">
            	Hubo errores en la asignaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
        {else}
            <div class="span-12 error" align="center">
            	No existe kits disponibles para asignar al pais. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
 {/if}
	<div class="span-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line separator">
		<label>Sucursal de Comercializador:</label>
	</div>

    <div class="span-24 last line"></div>
     <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-5"> <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}" {if $l.serial_zon eq $selZone}selected{/if}>{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-5 append-3 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {if $countryList}
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $l.serial_cou eq $selCountry}selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                    {/if}
                </select>
         </div>
   </div>

    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">* Representate:</div>
    	<div class="span-5" id="managerContainer">
        	<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {if $managerList}
                    {foreach from=$managerList item="l"}
                        <option value="{$l.serial_mbc}" {if $l.serial_mbc eq $selManager}selected{/if}>{$l.name_man}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
   </div>

   <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Comercializador:</div>
    	<div class="span-5" id="dealerContainer">
        	<select name="selDealer" id="selDealer" title="El campo 'Comercializador' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {if $dealerList}
                    {foreach from=$dealerList item="l"}
                        <option value="{$l.serial_dea}" {if $l.serial_dea eq $selDealer}selected{/if}>{$l.name_dea} - {$l.code_dea}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
   </div>

   <div class="span-24 last line">
         <div class="prepend-3 span-4 label">* Sucursal:</div>
    	<div class="span-5" id="branchContainer">
        	<select name="selBranch" id="selBranch" title="El campo 'Sucursal' es obligatorio.">
            	<option value="">- Seleccione -</option>
				{if $branchList}
					{foreach from=$branchList item="l"}
						<option value="{$l.serial_dea}" {if $l.serial_dea eq $selBranch}selected{/if}>{$l.name_dea} - {$l.code_dea}</option>
					{/foreach}
				{/if}
            </select>
        </div>
   </div>

	<div id="kitsContainer" class="span-24 last line"></div>
 
</form>