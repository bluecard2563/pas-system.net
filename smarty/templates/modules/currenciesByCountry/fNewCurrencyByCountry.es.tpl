{assign var="title" value="MONEDAS POR PA&Iacute;S"}
<form name="frmSearchCurrencyByCountry" id="frmSearchCurrencyByCountry" method="post" action="{$document_root}modules/currenciesByCountry/pNewCurrencyByCountry" class="">

    <div class="span-24 last title ">
    	Monedas por Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="span-24 last line">
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    		{if $error eq 1}
            	La moneda se ha registrado exitosamente!
        	{else}
            	No se pudo insertar el sector.
       		{/if}
    		</div>
        </div>
    </div>
    {/if}

    <div class="append-7 span-10 prepend-8 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-4">
            <label>Zona:</label>
        </div>
        <div class="span-9 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
              <option value="">- Seleccione -</option>
              {foreach from=$nameZoneList item="l"}
              <option value="{$l.serial_zon}" {if $data.serial_zon eq $l.serial_zon}selected="selected"{/if} >{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-4">
                <label>*Pa&iacute;s:</label>
        </div>
        <div class="span-9 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="button" name="btnDisplay" id="btnDisplay" value="Ver" >
    </div>

    <!--CURRENCIES LIST-->
    <div class="prepend-3 span-19 last line" id="currencyContainer"></div>
    <!--END CURRENCIES LIST-->
</form>