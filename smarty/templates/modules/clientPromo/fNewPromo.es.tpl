{assign var="title" value="PROMOCI&Oacute;N A CLIENTES"}
<script type="text/javascript" src="{$document_root}js/modules/clientPromo/clientPromoCommons.js"></script>
<div class="span-24 last title ">
	Promoci&oacute;n a Clientes<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
	{if $error eq '1'}
		La promoci&oacute;n fue ingresada exitosamente.
	{elseif $error eq '2'}
		Hubo errores en el ingreso. Comun&iacute;quese con el administrador.
	{elseif $error eq '3'}
		La promoci&oacute;n fue ingresada. Sin embargo, los productos asociados no se ingresaron.
	{/if}
	</div>
</div>
{/if}

<div class="append-7 span-10 prepend-7 last">
   <ul id="alerts" class="alerts"></ul>
</div>

<form name="frmCustomerPromo" id="frmCustomerPromo" method="post" action="{$document_root}modules/clientPromo/pNewPromo" >
	<div id="firstPage" class="span-24 last line">
		<div class="span-24 last line separator">
			<label>Informaci&oacute;n de la Promoci&oacute;n</label>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Nombre:</div>
			<div class="span-5">
				<input type="text" name="txtName" id="txtName" title="El campo 'Nombre' es olbigatorio" />
			</div>

			<div class="span-3 label">* C&oacute;digo:</div>
			<div class="span-4 append-4 last">
                            <input maxlength="3" type="text" id="txtCode" name="txtCode" title="El campo 'C&oacute;digo' es olbigatorio"/>
			</div>
		</div>
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
			<div class="span-5">
				<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from = $countryList item = "p"}
						<option value="{$p.serial_cou}">{$p.name_cou}</option>
					{/foreach}
				</select>
			</div>
		</div>		
		
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Fecha Inicio:</div>
			<div class="span-5">
				<input type="text" name="txtBeginDate" id="txtBeginDate" title="El campo 'Inicio' es olbigatorio" />
			</div>

			<div class="span-3 label">* Fecha Fin:</div>
			<div class="span-5 append-3 last">
				<input type="text" id="txtEndDate" name="txtEndDate" title="El campo 'Fin' es olbigatorio" />
			</div>
		</div>
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Aplicada a:</div>
			<div class="span-5">
				<select name="selScope[]" id="selScope" title="El campo 'Aplicada a' es obligatorio" multiple>
					{foreach from = $customer_promo_scopes item = "s"}
						<option value="{$s}">{$global_customerPromoScopes.$s}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">* Formas de Pago:</div>
			<div class="span-4 append-4 last">
				<select name="selPaymentForm[]" id="selPaymentForm" title="El campo 'Formas de Pago' es obligatorio" multiple>
					{foreach from = $customer_promo_payment_forms item = "p"}
						<option value="{$p}" id="{$p}">{$global_customerPromoPaymentForms.$p}</option>
					{/foreach}
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Tipo:</div>
			<div class="span-5">
				<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from = $customer_promo_types item = "s"}
						<option value="{$s}">{$global_customerPromoTypes.$s}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">* % Descuento:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtDiscount" id="txtDiscount" title="El campo 'Descuento' es obligatorio" />
			</div>
		</div>

		<div class="span-24 last line" id="otherDiscountContainer" style="display: none;">
			<div class="prepend-4 span-4 label">* % Pr&oacute;ximo Descuento:</div>
			<div class="span-5">
				<input type="text" name="txtOtherDiscount" id="txtOtherDiscount" title="El campo 'Pr&oacute;ximo Descuento' es obligatorio" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Descripci&oacute;n:</div>
			<div class="span-5 last">
				<textarea id="txtDescription" name="txtDescription" title="El campo 'Descripci&oacute;n' es olbigatorio"></textarea>
			</div>
		</div>
	</div>

	<div id="secondPage" class="span-24 last line">
		<div class="span-24 last line separator">
			<label>Informaci&oacute;n de Productos</label>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-5 span-5 label">* Productos Aplicados:</div>
			<div class="span-4 append-4 last" id="appliedProductContainer">
                                <select name="selAppliedProduct" id="selAppliedProduct" multiple title="El campo 'Producto Aplicado' es obligatorio">
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>
	</div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar" />
		<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
		<input type="hidden" name="hdnRemote" id="hdnRemote" value="" />
		<input type="hidden" name="selAppliedProduct[]" id="applied_product_hidden" value="" />
	</div>
</form>