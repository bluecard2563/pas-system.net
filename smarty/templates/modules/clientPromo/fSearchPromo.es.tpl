{assign var="title" value="B&Uacute;SQUEDA DE PROMOCI&Oacute;N"}
<script type="text/javascript" src="{$document_root}js/modules/clientPromo/clientPromoCommons.js"></script>
<div class="span-24 last title">
	B&uacute;squeda de Promoci&oacute;n
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
	{if $error eq '1'}
		La promoci&oacute;n fue actualizada exitosamente.
	{elseif $error eq '2'}
		Hubo errores en la actualizaci&oacute;n. Comun&iacute;quese con el administrador.
	{elseif $error eq '3'}
		La promoci&oacute;n fue actualizada. Sin embargo, los productos asociados no se actualizaron.
	{elseif $error eq 'no_log'}
		No se pudo cargar la informaci&oacute;n de la promoci&oacute;n.
	{/if}
	</div>
</div>
{/if}

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">* Pa&iacute;s: </div>
	<div class="span-4 append-6 last">
		<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			<option value="">- Seleccione -</option>
			{foreach from=$countryList item="c"}
			<option value="{$c.serial_cou}">{$c.name_cou}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">* Estado de la Promoci&oacute;n: </div>
	<div class="span-4 append-6 last">
		<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio.'>
			<option value="">- Seleccione -</option>
			{foreach from=$customer_promo_status item="l"}
			<option value="{$l}">{$global_customerPromoStatus.$l}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-24 last line buttons">
	<input type="button" id="btnSearch" name="btnSearch" value="Buscar" />
</div>

<div class="span-24 last line" id="customerPromo_container"></div>