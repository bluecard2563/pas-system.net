{assign var="title" value="NUEVA ZONA"}
<form name="frmNewZone" id="frmNewZone" method="post" action="{$document_root}modules/zone/pNewZone" class="">

	<div class="span-24 last title ">
    	Registro de Nueva Zona<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	La zona se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error">
            	No se pudo insertar la zona.
            </div>
        {/if}
    </div>
    {/if}
    
   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="span-24 last line">
        <div class="prepend-7 span-5 label">*Nombre zona:</div>
        
        <div class="span-8 append-2 last"> 
                <input type="text" name="txtNameZone" id="txtNameZone" title="El campo 'Nombre' es obligatorio.">      
        </div>
   </div>
   
   <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" >
   </div>
</form>