{assign var="title" value="BUSCAR ZONA"}
<form name="frmSearchZone" id="frmSearchZone" action="{$document_root}modules/zone/fUpdateZone" method="post" class="form_smarty">

	<div class="span-24 last title ">
    	Buscar Zona<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}     
        <div class="prepend-7 span-10 append-7  last">      
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    La Zona se actualiz&oacute; correctamente!
                {else}
                    No existe la zona ingresada. Por favor seleccione una de la lista.
                {/if}                                 
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
	<div class="span-24 last line">
        <div class="prepend-6 span-6 label">*Seleccione la zona:</div>

        <div class="span-8 append-2 last">
             <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name=zones from=$zoneList item="l"}
                    <option id="{$l.serial_zon}" value="{$l.serial_zon}"{if $smarty.foreach.zones.total eq 1}selected{/if}>{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
   </div>

    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="serial_zon" id="serial_zon" value="" />
</form>