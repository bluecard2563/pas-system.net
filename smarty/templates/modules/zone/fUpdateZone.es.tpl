{assign var="title" value="ACTUALIZAR ZONA"}
<form name="frmUpdateZone" id="frmUpdateZone" method="post" action="{$document_root}modules/zone/pUpdateZone" class="form_smarty">

	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Zona<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 error" align="center">
            {if $error eq 1}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">          
        <div class="prepend-8 span-4">
            <label>*Nuevo nombre de Zona:</label>
        </div>
        <div class="span-9 last">
            <input type="text" name="txtNameZone" id="txtNameZone" title='El campo nombre es obligatorio' {if $data.name_zon}value="{$data.name_zon}"{/if}>        
        </div>
    </div>
    <div class="span-24 last buttons line">
    	<input maxlength="30" type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_zon" id="hdnSerial_zon" {if $data.serial_zon}value="{$data.serial_zon}"{/if} >
    </div>
</form>