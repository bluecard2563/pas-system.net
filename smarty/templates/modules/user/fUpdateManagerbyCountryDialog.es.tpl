<form name="frmUpdateManagerbyCountry" id="frmUpdateManagerbyCountry" method="post" class="form_smarty">
<div class="span-1 span-8 prepend-1 last line">
    <ul id="alerts_dialog" class="alerts"></ul>
</div>
   
<div class="span-10 last line">
	<div class="prepend-1 span-4">
    	Representante:
    </div>
    <div class="span-4 append-1 last">
       	<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
     		<option value="">- Seleccione -</option>
            {foreach from=$mbc_ManagerList item="i"}
        	<option value="{$i.serial_mbc}">{$i.first_name_usr} {$i.last_name_usr}</option>
            {/foreach}
        </select>
    </div>
</div>
</form>