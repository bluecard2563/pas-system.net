<!--/*
File: fSearchUser.tpl
Author: Esteban Angulo
Creation Date: 24/12/2009 12:00
Last Modified:28/12/2009 
*/-->
{assign var="title" value="B&Uacute;SQUEDA DE USUARIO"} 
<form name="frmSearchUser" id="frmSearchUser" action="{$document_root}modules/user/fUpdateUser" method="post" class="form_smarty">
	<div class="span-24 last title">B&uacute;squeda de Usuario<br />
    	<label>(*) Campos Obligatorios</label>
    </div>
  {if $message}
    <div class="span-7 span-10 prepend-7  last" align="center">
    	{if $message eq 1}
        	<div class="span-9 error" align="center">
            	El usuario ingresado no existe.
            </div>
        {else}
        	<div class="span-9 error" align="center">
            	Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    
    
    
    <div class="span-24 last  line">
    <div class="prepend-4 span-8 label"> * Ingrese el nombre del usuario  o username:</div>
    <div class="span-8 append-4  show grid last "><input type="text" id="txtSearch" name="txtSearch"  title="Ingrese un Usuario."  class="span-8"/></div>
	</div>
   
    <div class="span-24 last buttons">
        <input type="submit" name="btnFind" id="btnFind" value="Continuar" >
    </div>
    <input type="hidden" name="serial_usr" id="serial_usr" value="" />
</form>
