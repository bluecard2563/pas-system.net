{assign var="title" value="ACTUALIZACI&Oacute;N DE USUARIO"} 
<form name="frmUpdateUser" id="frmUpdateUser" method="post" action="{$document_root}modules/user/pUpdateUser" class="form_smarty">
    <div class="span-24 last title">
    	Actualizaci&oacute;n de Usuario <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $message}
    <div class="append-5 span-11 prepend-5 last line">
    	{if $message eq 1}
        	<div class="span-9 success" align="center">
            	El usuario se ha actualizado exitosamente!
            </div>
        {else}
        	<div class="span-9 error" align="center">
            	Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Usuario:</div>
        <div class="span-5"> {$data.username_usr}  <input type="hidden" value="{$data.username_usr}" name="txtUsername" id="txtUsername" />  </div>
        
        <div class="span-3 label"></div>
        <div class="span-4 append-4 last"> </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5"> <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.' {if $data.first_name_usr}value="{$data.first_name_usr}"{/if} > </div>
        
        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio' {if $data.last_name_usr}value="{$data.last_name_usr}"{/if}> </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
        <div class="span-5"> {$data.document_usr}<input type="hidden" name="txtDocument" id="txtDocument" value="{$data.document_usr}"> </div>
        
        <div class="span-3 label">Direcci&oacute;n</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio' {if $data.address_usr}value="{$data.address_usr}"{/if}> </div>
	</div>
	 
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
        <div class="span-5"> 
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $data.aux.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou}</option>
                {/foreach}                                 
            </select> 
        </div>
        
        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">  
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            <option value="">- Seleccione -</option>
                {foreach from=$cityList item="l"}
                    <option value="{$l.serial_cit}" {if $data.serial_cit eq $l.serial_cit} selected="selected"{/if}>{$l.name_cit}</option>
                {/foreach}   
            </select> 
         </div>
	</div>
  
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-5"> <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio' {if $data.phone_usr}value="{$data.phone_usr}"{/if}> </div>
        
        <div class="span-3 label">Celular</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio' {if $data.cellphone_usr}value="{$data.cellphone_usr}"{/if}> </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5"> <input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio' {if $data.birthdate_usr}value="{$data.birthdate_usr}"{/if}> </div>
        
        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio' {if $data.email_usr}value="{$data.email_usr}"{/if}> </div>
	</div>
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Estado:</div>
        <div class="span-5">  
        	<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item="l"}
              <option value="{$l}" {if $data.status_usr eq $l}selected="selected"{/if} >{$global_status.$l}</option>
                {/foreach}
            </select> 
        </div>
        
        <div class="span-3 label">* Cargo:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtPosition" id="txtPosition" title='El campo "Cargo" es obligatorio' {if $data.position_usr}value="{$data.position_usr}"{/if}> </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Usuario de:</div>
        <div class="span-5">
            {if $planetAssistUser}
                {$global_system_name}
               <input type="hidden" name="hddPAUser" id="hddPAUser" value="PLANETASSIST" />
               <!--<input type="hidden" name="ecuadorID" id="ecuadorID" value="{$serial_cou}" />-->
			   <input type="hidden" name="ecuadorID" id="ecuadorID" value="1" />
            {else}
                {if $data.belongsto_usr eq 'MANAGER'}REPRESENTANTE
				{elseif $data.belongsto_usr eq 'DEALER'} COMERCIALIZADOR
				{/if}
            {/if}
        </div>
        <div class="span-3 label">* Perfil:</div>
        <div class="span-4 append-4 last" id="profileContainer"> 
        	<select name="selProfile" id="selProfile" title='El campo "Perfil" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$profileList item=l}
                    <option value="{$l.serial_pbc}" {if $data.serial_pbc eq $l.serial_pbc}selected="selected"{/if}>{$l.name_pbc}</option>
                {/foreach}
            </select> 
        </div>
    </div>

    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">N&uacute;mero de Visitas:</div>
        <div class="span-5 last">
        	<input type="text" name="txtVisitsNumber" id="txtVisitsNumber" value="{$data.visits_number_usr}">
        </div>
		<div class="span-3 label">* Vende Free?:</div>
        <div class="span-4 append-4 last">
        	<input type='radio' name='rdSellFree' id='rdSellFree' value='YES' title='El campo "Vende Free" es obligatorio' {if $data.sell_free_usr eq 'YES'}checked{/if} /> S&iacute;
            <input type='radio' name='rdSellFree' id='rdSellFree' value='NO' title='El campo "Vende Free" es obligatorio' {if $data.sell_free_usr eq 'NO'}checked{/if}/> No
        </div>
    </div>

   <div class="span-24 last line">
       <div class="prepend-20 span-4 last">
            <a id="resetPassword" style="cursor: pointer">Resetear contrase&ntilde;a</a>
        </div>
    </div>

	<div class="span-24 line" style="display: none" id="deactivateUserDialog" title="Advertencia"></div>

    <div class="span-24 last line" id="extraData">

		<div class="prepend-7 append-7 span-10 last line">
			<div class="span-10 center line"><label>ACCESOS ACTUALES:</label></div>
			{if $data.countries}
			<table border="0" id="city_table" style="color: #000000;">
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						Pa&iacute;s
					</td>
					<td align="center" class="tableTitle">
						Ciudades
					</td>
				</tr>
			{foreach from=$data.countries item=l name="country"}
				<tr {if $smarty.foreach.country.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
					<td align="center" class="tableField">
						{$l.name_cou}
					</td>
					<td align="center" class="tableField">
						{getFirstArrayItem array=$l.cities}
					</td>
				</tr>
				{if $l.cities|@count gt 1}
				{foreach from=$l.cities name="city" item=c}
					{if !$smarty.foreach.city.first}
					<tr {if $smarty.foreach.city.iteration + $smarty.foreach.country.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
						<td align="center" class="tableField">
							&nbsp;
						</td>
						<td align="center" class="tableField">
							{$c}
						</td>
					</tr>
					{/if}
				{/foreach}
				{/if}
			{/foreach}
			</table>
			{/if}
		</div>
		<div class="span-16 prepend-4 append-4 last line center" id="table_paging"></div>
		
        <div class="span-24 last line buttons">
            <input type="button" name="btnEdit" id="btnEdit" value="Editar Accesos">
        </div>
    </div>

    <div class="span-24 line last" id="CountryAccess" >
    </div>
    
    <!--[if IE 6]>&nbsp;<![endif]-->
     
    <div class="span-24 last buttons">
    	<input type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar">
    </div>
    
	<input type="hidden" name="serial_usr" id="serial_usr" {if $data.serial_usr}value="{$data.serial_usr}"{/if} >
	<input type="hidden" name="numDealersHdd" id="numDealersHdd" value="{$data.numDealers}" >
	<input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" value="{$data.aux.serial_cou}" >
	<input type="hidden" name="hdnSerial_cit" id="hdnSerial_cit" value="{$data.serial_cit}" >
	<input type="hidden" name="hdnSerial_dea" id="hdnSerial_dea" value="{$data.serial_dea}" >
	<input type="hidden" name="hddCountryAccess" id="hddCountryAccess" value="0" />
	<input type="hidden" name="hddBelongsto" id="hddBelongsto" value="{$data.belongsto_usr}" />
	<input type="hidden" name="hddCountries" id="hddCountries" value="{$data.hasCountries}" />
	<input type="hidden" name="hddValidateCountries" id="hddValidateCountries" value="" title='Debe seleccionar al menos un pa&iacute;s'/>
	<input type="hidden" name="hddValidateCities" id="hddValidateCities" value="" title='Debe seleccionar al menos una ciudad' />
	<input type="hidden" name="hddNewPassword" id="hddNewPassword" value="">
</form>