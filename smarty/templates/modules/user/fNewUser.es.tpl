
{assign var="title" value="NUEVO USUARIO"} 
<form name="frmNewUser" id="frmNewUser" method="post" action="{$document_root}modules/user/pNewUser" class="">
	<div class="span-24 last title">
    	Registro de Nuevo Usuario <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El usuario se ha registrado exitosamente!
            </div>
        {/if}
        {if $error eq 2}
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {/if}
        {if $error eq 3}
        	<div class="span-10 error" align="center">
            	Ya existe un representante exclusivo para ese Pa&iacute;s. <br />
                Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last alerts">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtFirstName" id="txtFirstName" title='El campo "Nombre" es obligatorio.'> 
        </div>
        
        <div class="span-3 label">* Apellido</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtLastName" id="txtLastName" title='El campo "Apellido" es obligatorio'> </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Identificaci&oacute;n:</div>
        <div class="span-5"> <input type="text" name="txtDocument" id="txtDocument" title='El campo "Identificaci&oacute;n" es obligatoria'> </div>
        
        <div class="span-3 label">Direcci&oacute;n:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtAddress" id="txtAddress" title='El campo "Direcci&oacute;n" es obligatorio'> </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label" id="countryContainer">* Pa&iacute;s:</div>
        <div class="span-5"> 
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach name=countries from=$countryList item="l"}
                    <option id="{$l.serial_cou}" value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou}</option>
                {/foreach}
            </select> 
        </div>
		
        <div class="span-3 label">* Ciudad</div>
        <div class="span-4 append-4 last" id="cityContainer">  
        	<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select> 
         </div>
	</div>
        
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Tel&eacute;fono:</div>
        <div class="span-5"> <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio'>
        </div>
        
        <div class="span-3 label">Celular:</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtCellphone" id="txtCellphone" title='El campo "Celular" es obligatorio'> 
        </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Fecha de Nacimiento:</div>
        <div class="span-5"> <input type="text" name="txtBirthdate" id="txtBirthdate" title='El campo "Fecha de Nacimiento" es obligatorio'> </div>
        
        <div class="span-3 label">* E-mail:</div>
        <div class="span-4 append-4 last"> <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio'> </div>
	</div>

        <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Cargo:</div>
        <div class="span-5"> <input type="text" name="txtPosition" id="txtPosition" title='El campo "Cargo" es obligatorio'> </div>

        <div class="span-3 label">* Perfil:</div>
        <div class="span-4 append-4 last" id="profileContainer">
        	<select name="selProfile" id="selProfile" title='El campo "Perfil" es obligatorio'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Usuario de:</div>
        <div class="span-5"> 
                <input type='radio' name='belongsto_usr' id='belongsto_usr' value='MANAGER' title='El campo "Usuario de" es obligatorio' /> Representante<br/>
                <input type='radio' name='belongsto_usr' id='belongsto_usr' value='DEALER' title='El campo "Usuario de" es obligatorio' /> Comercializador<br/>
                {if $planetAssistUser}
                <input type='radio' name='belongsto_usr' id='belongsto_usr' value='PLANETASSIST' title='El campo "Usuario de" es obligatorio' /> {$global_system_name}<br/>
                <input type="hidden" name="ecuadorID" id="ecuadorID" value="1" />
                {/if}
        </div>
        <div class="span-3 label">N&uacute;mero de Visitas:</div>
        <div class="span-4 append-4 last">
        	<input type="text" name="txtVisitsNumber" id="txtVisitsNumber">
        </div>
		<br /><br />
        <div {if $planetAssistUser neq 1}style="display:none"{/if}>
            <div class="span-3 label">* Vende Free?:</div>
            <div class="span-4 append-4 last">
                    <input type='radio' name='rdSellFree' id='rdSellFree' value='YES' title='El campo "Vende Free" es obligatorio' /> S&iacute;
                <input type='radio' name='rdSellFree' id='rdSellFree' value='NO' title='El campo "Vende Free" es obligatorio' checked/> No<br/>
            </div>
        </div>
    </div>


    <div class="span-24 last line" id="extraData">
    </div>

    <div class="span-24 line last" id="CountryAccess">
    </div>
   
    <div class="span-24 last buttons">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>

    <input type="hidden" name="serial_usr" id="serial_usr" value="{if $data}{$data.serial_usr}{/if}" />
    <input type="hidden" name="hddSerialCountry" id="hddSerialCountry" value="" />
    <input type="hidden" name="hddValidateCountries" id="hddValidateCountries" value="" title='Debe seleccionar al menos un pa&iacute;s'/>
    <input type="hidden" name="hddValidateCities" id="hddValidateCities" value="" title='Debe seleccionar al menos una ciudad' />
</form>