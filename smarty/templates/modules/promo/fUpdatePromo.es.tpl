{assign var="title" value="ACTUALIZAR PROMOCI&Oacute;N"}
<form name="frmUpdatePromo" id="frmUpdatePromo" method="post" action="{$document_root}modules/promo/pUpdatePromo">
    <div class="span-24 last title">
    	Actualizar Promoci&oacute;n<br>
          <label>(*) Campos Obligatorios</label>
    </div>

    <div class="wizard-nav span-24 last line">
         <a href="#newPromo"><div class="span-6">1. Creaci&oacute;n de Promoci&oacute;n</div></a>
         <a href="#products"><div class="span-6">2. Productos</div></a>
         <a href="#appliedTo"><div class="span-6">3. Participantes</div></a>
         <a href="#assignPrizes"><div class="span-6 last">4. Premios</div></a>
    </div>
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div id="newPromo" class="wizardpage span-24 last sectionPage">
        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">* Nombre:</div>
            <div class="span-5 append-6 last">  <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' value="{$data.name}"> </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">* Descripci&oacute;n:</div>
            <div class="span-5 append-6 last">
                <textarea name="txtDescription" id="txtDescription" title="El campo 'Descripci&oacute;n' es obligatorio." class="txtArea">{$data.description}</textarea>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha de Inicio:</div>
            <div class="span-5">
                <input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio' value="{$data.beginDate}"/>
            </div>
            <div class="span-3 label">* Fecha de Fin: </div>
            <div class="span-5">
                <input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio' value="{$data.endDate}"/>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Condiciones de la Promoci&oacute;n:</div>
            <div class="span-5">
                <select name="selType" id="selType" title='El campo "Condiciones" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$typeList item="l"}
                    <option value="{$l}" {if $data.type eq $l}selected{/if}>{$global_promoTypes.$l}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Valor de Venta:</div>
            <div class="span-5">
                <select name="selSaleType" id="selSaleType" title='El campo "Valor de Venta" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$typeSale item="l"}
                    <option value="{$l}" {if $data.typeSale eq $l}selected{/if}>{$global_saleTypes.$l}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* # D&iacute;as Aplicables: </div>
            <div class="span-5">
                <select name="selPeriod" id="selPeriod" title='El campo "# D&iacute;as Aplicables" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$periodType item="l"}
                    <option value="{$l}" {if $data.periodType eq $l}selected{/if}>{$global_periodTypes.$l}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Se repite la Promoci&oacute;n?: </div>
            <div class="span-5">
                No <input type="radio" name="rdRepeat" id="rdRepeat" title='El campo "Se Repite" es obligatorio' value="0" {if $data.repeat eq 0}checked{/if}>
                Si <input type="radio" name="rdRepeat" id="rdRepeat" title='El campo "Se Repite" es obligatorio' value="1" {if $data.repeat eq 1}checked{/if}>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Aplica fines de semana: </div>
            <div class="span-5 last">
                No <input type="radio" name="rdWeekends" id="rdWeekends" title='El campo "Aplica fines de semana" es obligatorio' value="NO" {if $data.weekends eq NO}checked{/if}/>
                Si <input type="radio" name="rdWeekends" id="rdWeekends" title='El campo "Aplica fines de semana" es obligatorio' value="YES" {if $data.weekends eq YES}checked{/if}/>
            </div>
            <div id="repeatCriteria">
                <div class="span-3 label">* Periodicidad: </div>
            <div class="span-5 last">
                <select name="selRepeatType" id="selRepeatType" title='El campo "Periodicidad" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$repeatType item="l"}
                    <option value="{$l}" {if $data.repeatType eq $l}selected{/if}>{$global_repeatTypes.$l}</option>
                    {/foreach}
                </select>
            </div>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Env&iacute;o de Notificaciones: </div>
            <div class="span-5">
                No <input type="radio" name="rdNotification" id="rdNotification" title='El campo "Notificaciones" es obligatorio' value="0" {if $data.notifications eq 0}checked{/if}/>
                Si <input type="radio" name="rdNotification" id="rdNotification" title='El campo "Notificaciones" es obligatorio' value="1" {if $data.notifications eq 1}checked{/if}/>
            </div>
            <div class="span-8 last" id="weekDayContainer">
                <div class="span-3 label">* D&iacute;a de la Promoci&oacute;n:</div>
                <div class="span-5 last" id="weekDay">
                    {if not $data.periodDays}
                        &nbsp;
                    {elseif $data.periodType eq RANDOM}
                        <div id="randomDay">
                        {if $data.repeatType eq WEEKLY}
                            {$data.day}
                        {else}
                            {$data.periodDays}
                        {/if}                        
                        &nbsp;&nbsp;<a href="#" name="generateRand" id="generateRand">Generar Otro</a>
                        </div>
                    {else}
                        {if $data.repeatType eq WEEKLY}
                            {if $data.periodType eq 'ONE DAY'}
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_1' value='1' class='weekDayInfo' {if $data.periodDays eq 1}checked{/if}/> Lunes<br/>
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_2' value='2' class='weekDayInfo' {if $data.periodDays eq 2}checked{/if}/> Martes<br/>
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_3' value='3' class='weekDayInfo'{if $data.periodDays eq 3}checked{/if}/> Mi&eacute;rcoles<br/>
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_4' value='4' class='weekDayInfo' {if $data.periodDays eq 4}checked{/if}/> Jueves<br/>
                            <input type='radio' name='weekDayInfo[]' id='weekDayInfo_5' value='5' class='weekDayInfo' {if $data.periodDays eq 5}checked{/if}/> Viernes<br/>
                            <div id=weekendsInfo>
                                <input type='radio' name='weekDayInfo[]' id='weekDayInfo_6' value='6' class='weekDayInfo' {if $data.periodDays eq 6}checked{/if}/> S&aacute;bado<br/>
                                <input type='radio' name='weekDayInfo[]' id='weekDayInfo_7' value='7' class='weekDayInfo' {if $data.periodDays eq 7}checked{/if}/> Domingo<br/>
                            </div>
                            {else}
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_1' value='1' class='weekDayInfo' {foreach from=$data.periodDays item="l"}{if $l eq 1}checked{/if}{/foreach}/> Lunes<br/>
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_2' value='2' class='weekDayInfo' {foreach from=$data.periodDays item="l"}{if $l eq 2}checked{/if}{/foreach}/> Martes<br/>
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_3' value='3' class='weekDayInfo'{foreach from=$data.periodDays item="l"}{if $l eq 3}checked{/if}{/foreach}/> Mi&eacute;rcoles<br/>
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_4' value='4' class='weekDayInfo' {foreach from=$data.periodDays item="l"}{if $l eq 4}checked{/if}{/foreach}/> Jueves<br/>
                            <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_5' value='5' class='weekDayInfo' {foreach from=$data.periodDays item="l"}{if $l eq 5}checked{/if}{/foreach}/> Viernes<br/>
                            <div id=weekendsInfo>
                                <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_6' value='6' class='weekDayInfo' {foreach from=$data.periodDays item="l"}{if $l eq 6}checked{/if}{/foreach}/> S&aacute;bado<br/>
                                <input type='checkbox' name='weekDayInfo[]' id='weekDayInfo_7' value='7' class='weekDayInfo' {foreach from=$data.periodDays item="l"}{if $l eq 7}checked{/if}{/foreach}/> Domingo<br/>
                            </div>
                            {/if}
                        {else}
                            <input type="text" name="weekDayInfo" id="weekDayInfo_Calendar" title="El campo \'D&iacute;a de la Promoci&oacute;n\' es obligatorio." value="{$data.periodDays}"/>
                        {/if}
                    {/if}
                </div>
            </div>

        </div>
    </div>

    <div id="products" class="wizardpage span-24 last line sectionPage">
        <div class="prepend-8 span-8 append-8 last label line">* Productos Participantes: </div>
        <div class="prepend-8 span-8 append-8 last line">
            <select name="selProducts" id="selProducts" title='El campo "Productos Participantes" es obligatorio.' multiple class="multipleSelect">
                {foreach from=$productList item="l"}
                <option value="{$l.serial_pro}" {foreach from=$selectedProducts item="sp"}{if $sp.serial_pro eq $l.serial_pro}selected{/if}{/foreach}>{$l.name_pbl}</option>
                {/foreach}
            </select>
        </div>
		<div class="prepend-7 span-10 append-7 lat line center">
			<label><i>(*) Presione la tecla "Ctrl" para seleccionar m&aacute;s de un producto a la vez.</i></label>
		</div>
    </div>

    <div id="appliedTo" class="wizardpage span-24 last line">
        <div class="prepend-7 span-5 label">* Promociones Aplicadas a: </div>
        <div class="span-6 append-6 last line">
            <select name="selApliedTo" id="selApliedTo" title='El campo "Aplicadas a" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$apliedTo item="l"}
                <option value="{$l}"{if $data.appliedTo eq $l}selected{/if}>{$global_appliedToTypes.$l}</option>
                {/foreach}
            </select>
        </div>

        <div class="prepend-3 span-21 last line sectionPage" id="appliedToContainer">
            <div class="span-19 last line">
                <div class="prepend-1 span-4  label"> Zona: </div>
                <div class="span-5">
                    <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$zoneList item="l"}
                        <option value="{$l.serial_zon}" {if $l.serial_zon eq $geoData.serial_zon} selected{/if}>{$l.name_zon}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="span-3  label">* Pa&iacute;s: </div>
                <div class="span-5" id="countryContainer">
                    <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$countryList item="cl"}
                            <option id="{$cl.serial_cou}" value="{$cl.serial_cou}"{if $cl.serial_cou eq $geoData.serial_cou} selected{/if}>{$cl.name_cou}</option>
                        {/foreach}
                    </select>
                </div>
            </div>

            {if $data.appliedTo eq 'MANAGER'}
            <div class="span-19 last line">
                    <div class="prepend-1 span-4  label">* Representante: </div>
                    <div class="span-5 last" id="managerContainer">
                        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$managerList item="l"}
                                <option value="{$l.serial_mbc}" {if $l.serial_mbc eq $geoData.serial_mbc} selected="selected"{/if}>{$l.name_man}</option>
                            {/foreach}
                        </select>
                    </div>
            </div>
            {/if}

            {if $data.appliedTo eq 'CITY' or $data.appliedTo eq 'DEALER'}
            <div class="span-19 last line">
                <div class="prepend-1 span-4  label">* Ciudad: </div>
                <div class="span-5 last" id="cityContainer">
                    <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$cityList item="cl"}
                            <option id="{$cl.serial_cit}" value="{$cl.serial_cit}"{if $cl.serial_cit eq $geoData.serial_cit} selected{/if}>{$cl.name_cit}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            {/if}

            {if $data.appliedTo eq 'DEALER'}
            <div class="prepend-5 span-6 append-8 label line">* Seleccione los comercializadores:</div>
            <div class="prepend-5 span-10 append-4 last line" id="branchContainer">
                <div class="span-4">
                    <div align="center">Seleccionados</div>
                    <select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-4 last" title="El campo de responsables es obligatorio.">
                        {foreach from=$dealerList item="l"}
                            {foreach from=$data.aux item="d"}
                                {if $d.serial_dea eq $l.serial_dea}
                                    <option value="{$l.serial_dea}" city="{$geoData.serial_cit}" selected>{$l.name_dea}</option>
                                {/if}
                            {/foreach}
                        {/foreach}
                    </select>
                </div>
                <div class="span-2 last buttonPane">
                    <br />
                    <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                    <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                    <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                    <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
                </div>
                <div class="span-4 last line" id="dealersFromContainer">
                    <div align="center">Existentes</div>
                    <select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-4 last" city="{$geoData.serial_cit}">                        
                        {foreach from=$dealerList item="l"}
                        {assign var="deaFlag" value=0}
                            {foreach from=$data.aux item="d"}                            
                                {if $d.serial_dea eq $l.serial_dea}
                                    {assign var="deaFlag" value=1}
                                {/if}
                            {/foreach}
                            {if $deaFlag eq 0}
                                <option value="{$l.serial_dea}" city="{$geoData.serial_cit}">{$l.name_dea}</option>
                            {/if}
                        {/foreach}     
                    </select>
                </div>
            </div>

            <div class="prepend-5 span-10 append-4 last line">
                <center>* Todos los comercializadores en el lado izquierdo deben estar seleccionados
                para poder a&ntilde;adirles a la promoci&oacute;n.</center>
            </div>
            {/if}
        </div>
    </div>

    <div id="assignPrizes" class="wizardpage span-24 last line sectionPage">
        {foreach from=$data.prizes item="dp" key="k"}
            {assign var="numberPrize" value=$k}
            {if $k eq 0}
            <div class="span-24 last title line"><label>Condici&oacute;n No. {$k+1}</label></div>
            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">* Productos que Aplican:</div>
                <div class="span-5">
                    <select name="selProduct[{$k+1}][]" id="selProduct_{$k+1}" title='El campo "Productos ({$k+1})" es obligatorio.' case="{$k+1}" multiple class="appliedProducts">
                        {foreach from=$selectedProducts item="l"}
                            <option value="{$l.serial_pro}" {foreach from=$data.productsByCountryByPrize item="d"}{if $d.serial_pbp eq $dp.serial_pbp AND $d.serial_pro eq $l.serial_pro}selected{/if}{/foreach}>{$l.name_pbl}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="span-9 line">
                <div class="span-3 label">* Monto Base:</div>
                <div class="span-5">
                    <input type="text" name="txtBeginP[]" id="txtBeginP_{$k+1}" title='El campo "Monto Base ({$k+1})" es obligatorio.' position="{$k+1}" value="{$dp.starting_value_pbp}"/>
                </div>
                </div>

                <div class="span-9 line">
                <div class="span-3 label">Monto L&iacute;mite:</div>
                <div class="span-5">
                    <input type="text" name="txtEndP[]" id="txtEndP_{$k+1}" title='El campo "Monto L&iacute;mite ({$k+1})" es obligatorio.' position="{$k+1}" value="{$dp.end_value_pbp}"/>
                </div>
                </div>

                <div class="span-9 line">
                    <div class="span-3 label">* Premio:</div>
                    <div class="span-5">
                        <select name="selPrize[]" id="selPrize_{$k+1}" case="{$k+1}" title='El campo "Premio ({$k+1})" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$prizeList item="l"}
                                <option value="{$l.serial_pri}" {if $l.serial_pri eq $dp.serial_pri}selected{/if}>{$l.name_pri}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>                 
            {/if}
            {/foreach}
            <div class="span-24 last line" id="morePrizes">
                {foreach from=$data.prizes item="dp" key="k"}
                {if $k neq 0}
                <div id="prize_{$k+1}" class="span-24 last line">
                            <div class="span-24 last title line"><label>Condici&oacute;n No. {$k+1}</label></div>
                        <div class="span-24 last line">
                            <div class="prepend-4 span-4 label">* Productos que Aplican:</div>
                            <div class="span-5">
                                <select name="selProduct[{$k+1}][]" id="selProduct_{$k+1}" title='El campo "Productos ({$k+1})" es obligatorio.' case="{$k+1}" multiple class="appliedProducts">
                                    {foreach from=$selectedProducts item="l"}
                                        <option value="{$l.serial_pro}" {foreach from=$data.productsByCountryByPrize item="d"}{if $d.serial_pbp eq $dp.serial_pbp AND $d.serial_pro eq $l.serial_pro}selected{/if}{/foreach}>{$l.name_pbl}</option>
                                    {/foreach}
                                </select>
                            </div>

                            <div class="span-9 line">
                            <div class="span-3 label">* Monto Base:</div>
                            <div class="span-5">
                                <input type="text" name="txtBeginP[]" id="txtBeginP_{$k+1}" title='El campo "Monto Base ({$k+1})" es obligatorio.' position="{$k+1}" value="{$dp.starting_value_pbp}"/>
                            </div>
                            </div>

                            <div class="span-9 line">
                            <div class="span-3 label">Monto L&iacute;mite:</div>
                            <div class="span-5">
                                <input type="text" name="txtEndP[]" id="txtEndP_{$k+1}" title='El campo "Monto L&iacute;mite ({$k+1})" es obligatorio.' position="{$k+1}" value="{$dp.end_value_pbp}"/>
                            </div>
                            </div>

                            <div class="span-9 line">
                                <div class="span-3 label">* Premio:</div>
                                <div class="span-5">
                                    <select name="selPrize[]" id="selPrize_{$k+1}" case="{$k+1}" title='El campo "Premio ({$k+1})" es obligatorio.'>
                                        <option value="">- Seleccione -</option>
                                        {foreach from=$prizeList item="l"}
                                            <option value="{$l.serial_pri}" {if $l.serial_pri eq $dp.serial_pri}selected{/if}>{$l.name_pri}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/if}
                {/foreach}
            </div>

		<div class="prepend-6 span-12 append-6 last line" id="rangeWarning" {if $data.type neq 'RANGE'}style="display: none;"{/if}>
			<label>* Para definir el &uacute;ltimo intervalo, ingrese '0' como el "Monto L&iacute;mite" del rango</label>
		</div>

		<div class="prepend-6 span-12 append-6 last line" id="typeWarning" {if $data.type eq 'RANGE'}style="display: none;"{/if}>
			<label>* El "Monto L&iacute;mite", es aplicable s&oacute;lo a promociones del tipo rango</label>
		</div>
        
        <div class="prepend-14 span-10 last line">
                <input type="button" name="btnAdd_1" id="btnAdd_1" value="Agregar Nuevo" class="ui-state-default">
                <input type="button" name="btnRemove_1" id="btnRemove_1" value="Quitar Elemento" class="ui-state-default">
        </div>
    </div>

     <input type="hidden" name="hdnCurDate" id="hdnCurDate" value="{$curDate}"/>
     <input type="hidden" name="hdnNumberPrize" id="hdnNumberPrize" value="{$numberPrize+1}"/>
     <input type="hidden" name="hddPeriod" id="hddPeriod" value="{$data.periodType}">
     <input type="hidden" name="hddRepeatType" id="hddRepeatType" value="{$data.repeatType}">
     <input type="hidden" name="hdnRandom" id="hdnRandom" value="{$data.periodDays}">
     <input type="hidden" name="hdnPromoID" id="hdnPromoID" value="{$data.serial_prm}">
</form>
<div id="dialog" title="N&uacute;mero Aleatorio">
        {include file="templates/modules/promo/fPromoDialog.$language.tpl"}
</div>