{assign var="title" value="CANJES DE PROMOCI&Oacute;N"}
<div class="span-24 last title">
	Canjes de Promoci&oacute;n<br>
	  <label>(*) Campos Obligatorios</label>
</div>

{if $error}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 {if $error eq 1}success{else}error{/if}">
		{if $error eq 1}
			La promoci&oacute;n se ha liquidado exitosamente!
		{elseif $error eq 2}
			No se han encontrado ganadores para la promoci&oacute;n seleccionada.
		{elseif $error eq 3}
			Hubo errores al registrar uno de los ganadores. Comun&iacute;quese con el adminsitrador.
		{elseif $error eq 4}
			No existen suficientes premios para liquidar a todos los ganadores de la promoci&oacute;n. La operaci&oacute;n fue interrumpida.
		{elseif $error eq 5}
			No existen ventas seleccionables para determinar a ganadores de la promoci&oacute;n.
		{elseif $error eq 6}
			Hubo errores al cargar la promoci&oacute;n. Por favor vuelva a intentarlo.
		{elseif $error eq 7}
			Hubo errores al registrar a uno de los ganadores. Por favor vuelva a revisar la lista de ganadores.
		{/if}
	</div>
</div>
{/if}

<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmReclaimPromo" id="frmReclaimPromo" action="{$document_root}modules/promo/reclaimPromo/pReclaimPromo" method="post">
<div class="span-24 last line">
	<div class="prepend-7 span-5 label">* Promociones Aplicadas a: </div>
	<div class="span-5 append-7 last">
		<select name="selApliedTo" id="selApliedTo" title='El campo "Aplicadas a" es obligatorio.'>
			<option value="">- Seleccione -</option>
			{foreach from=$apliedTo item="l"}
			<option value="{$l}">{$global_appliedToTypes.$l}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-24 last line" id="promoContainer"></div>

<div class="span-24 last line" id="prizeInfoContainer"></div>
</form>