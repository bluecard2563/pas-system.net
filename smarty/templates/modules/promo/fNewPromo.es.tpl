{assign var="title" value="NUEVA PROMOCI&Oacute;N"}
<form name="frmNewPromo" id="frmNewPromo" method="post" action="{$document_root}modules/promo/pNewPromo">
    <div class="span-24 last title">
    	Registro de Nueva Promoci&oacute;n<br>
          <label>(*) Campos Obligatorios</label>
    </div>

    <div class="wizard-nav span-24 last line">
         <a href="#newPromo"><div class="span-6">1. Creaci&oacute;n de Promoci&oacute;n</div></a>
         <a href="#products"><div class="span-6">2. Productos</div></a>
         <a href="#appliedTo"><div class="span-6">3. Participantes</div></a>
         <a href="#assignPrizes"><div class="span-6 last">4. Premios</div></a>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            La promoci&oacute;n se ha registrado exitosamente!
        {elseif $error eq 2}
            Hubo errores en el ingreso de la promoci&oacute;n. Por favor vuelva a intentarlo.
        {elseif $error eq 3}
            No se pudo asociar la promoci&oacute;n al pa&iacute;s seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 4}
            No se pudo asociar la promoci&oacute;n con la ciudad seleccionada. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 5}
            No se pudo asociar la promoci&oacute;n con el representante seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 6}
            No se pudo asociar la promoci&oacute;n con los comercializadores seleccionados. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 7}
            El rango de d&iacute;as de la promo. no es v&aacute;lido, debe ser ascendente,            ejemplo: 1,4-5,15-17,19,25  .
        {elseif $error eq 8}
			La promoci&oacute;n se ha creado exitosamente. Sin embargo, no se pudo asociar la promoci&oacute;n
			con algunos premios seleccionados. <br><br>Por favor comun&iacute;quese con el administrador.
        {/if}
        </div>
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div id="newPromo" class="wizardpage span-24 last sectionPage">
        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">* Nombre:</div>
            <div class="span-5 append-6 last">  <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.'> </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-7 span-4 label">* Descripci&oacute;n:</div>
            <div class="span-5 append-6 last">
                <textarea name="txtDescription" id="txtDescription" title="El campo 'Descripci&oacute;n' es obligatorio." class="txtArea"></textarea>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Fecha de Inicio:</div>
            <div class="span-5">
                <input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio'>
            </div>
            <div class="span-3 label">* Fecha de Fin: </div>
            <div class="span-5">
                <input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio'>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Condiciones de la Promoci&oacute;n:</div>
            <div class="span-5">
                <select name="selType" id="selType" title='El campo "Condiciones" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$typeList item="l"}
                    <option value="{$l}">{$global_promoTypes.$l}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Valor de Venta:</div>
            <div class="span-5">
                <select name="selSaleType" id="selSaleType" title='El campo "Valor de Venta" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$typeSale item="l"}
                    <option value="{$l}">{$global_saleTypes.$l}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* # D&iacute;as Aplicables: </div>
            <div class="span-5">
                <select name="selPeriod" id="selPeriod" title='El campo "# D&iacute;as Aplicables" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$periodType item="l"}
                    <option value="{$l}">{$global_periodTypes.$l}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Se repite la Promoci&oacute;n?: </div>
            <div class="span-5">
                No <input type="radio" name="rdRepeat" id="rdRepeat" title='El campo "Se Repite" es obligatorio' value="0">
                Si <input type="radio" name="rdRepeat" id="rdRepeat" title='El campo "Se Repite" es obligatorio' value="1">
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Aplica fines de semana: </div>
            <div class="span-5 last">
                No <input type="radio" name="rdWeekends" id="rdWeekends" title='El campo "Aplica fines de semana" es obligatorio' value="NO" >
                Si <input type="radio" name="rdWeekends" id="rdWeekends" title='El campo "Aplica fines de semana" es obligatorio' value="YES">
            </div>
            <div id="repeatCriteria">
                <div class="span-3 label">* Periodicidad: </div>
            <div class="span-5 last">
                <select name="selRepeatType" id="selRepeatType" title='El campo "Periodicidad" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$repeatType item="l"}
                    <option value="{$l}">{$global_repeatTypes.$l}</option>
                    {/foreach}
                </select>
            </div>
            </div>            
        </div>

        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Env&iacute;o de Notificaciones: </div>
            <div class="span-5">
                No <input type="radio" name="rdNotification" id="rdNotification" title='El campo "Notificaciones" es obligatorio' value="0" >
                Si <input type="radio" name="rdNotification" id="rdNotification" title='El campo "Notificaciones" es obligatorio' value="1">
            </div>
            <div class="span-8 last" id="weekDayContainer">
                <div class="span-3 label">* D&iacute;a de la Promoci&oacute;n:</div>
                <div class="span-5 last" id="weekDay">&nbsp;</div>
            </div>
            
        </div>
    </div>

    <div id="products" class="wizardpage span-24 last line sectionPage">
        <div class="prepend-8 span-8 append-8 last label line">* Productos Participantes: </div>
        <div class="prepend-8 span-8 append-8 last line">
            <select name="selProducts" id="selProducts" title='El campo "Productos Participantes" es obligatorio.' multiple class="multipleSelect">
                {foreach from=$productList item="l"}
                <option value="{$l.serial_pro}">{$l.name_pbl}</option>
                {/foreach}
            </select>
        </div>
		<div class="prepend-7 span-10 append-7 lat line center">
			<label><i>(*) Presione la tecla "Ctrl" para seleccionar m&aacute;s de un producto a la vez.</i></label>
		</div>
    </div>
    
    <div id="appliedTo" class="wizardpage span-24 last line">
        <div class="prepend-7 span-5 label">* Promociones Aplicadas a: </div>
        <div class="span-6 append-6 last line">
            <select name="selApliedTo" id="selApliedTo" title='El campo "Aplicadas a" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$apliedTo item="l"}
                <option value="{$l}">{$global_appliedToTypes.$l}</option>
                {/foreach}
            </select>
        </div>
        
        <div class="prepend-3 span-21 last line sectionPage" id="appliedToContainer"></div>
    </div>

    <div id="assignPrizes" class="wizardpage span-24 last line sectionPage">
        <div class="span-24 last title line"><label>Condici&oacute;n No. 1</label></div>
        <div class="span-24 last line">
            <div class="prepend-4 span-4 label">* Productos que Aplican:</div>
            <div class="span-5">
                <select name="selProduct[1][]" id="selProduct_1" title='El campo "Productos (1)" es obligatorio.' case="1" multiple class="appliedProducts">
                    <option></option>
                </select>
            </div>

            <div class="span-9 line">
            <div class="span-3 label">* Monto Base:</div>
            <div class="span-5">
                <input type="text" name="txtBeginP[]" id="txtBeginP_1" title='El campo "Monto Base (1)" es obligatorio.' position="1"/>
            </div>
            </div>

            <div class="span-9 line" id="endPriceContainer">
                <div class="span-3 label">* Monto L&iacute;mite:</div>
                <div class="span-5">
                    <input type="text" name="txtEndP[]" id="txtEndP_1" title='El campo "Monto L&iacute;mite (1)" es obligatorio.' position="1"/>
                </div>
            </div>

            <div class="span-9 line">
                <div class="span-3 label">* Premio:</div>
                <div class="span-5">
                    <select name="selPrize[]" id="selPrize_1" case="1" title='El campo "Premio (1)" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$prizeList item="l"}
                        <option value="{$l.serial_pri}">{$l.name_pri}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>

        <div class="span-24 last line" id="morePrizes"></div>

		<div class="prepend-6 span-12 append-6 last line" id="rangeWarning" style="display: none;">
			<label>* Para definir el &uacute;ltimo intervalo, ingrese '0' como el "Monto L&iacute;mite" del rango</label>
		</div>

		<div class="prepend-6 span-12 append-6 last line" id="typeWarning" style="display: none;">
			<label>* El "Monto L&iacute;mite", es aplicable s&oacute;lo a promociones del tipo rango</label>
		</div>
        
        <div class="prepend-14 span-10 last line">
            <input type="button" name="btnAdd_1" id="btnAdd_1" value="Agregar Nuevo" class="ui-state-default">
            <input type="button" name="btnRemove_1" id="btnRemove_1" value="Quitar Elemento" class="ui-state-default">
        </div>
    </div>

     <input type="hidden" name="hdnCurDate" id="hdnCurDate" value="{$curDate}"/>
     <input type="hidden" name="hdnNumberPrize" id="hdnNumberPrize" value="1"/>
     <input type="hidden" name="hddPeriod" id="hddPeriod" value="">
     <input type="hidden" name="hddRepeatType" id="hddRepeatType" value="">
     <input type="hidden" name="hdnRandom" id="hdnRandom" value="">
</form>
<div id="dialog" title="N&uacute;mero Aleatorio">
        {include file="templates/modules/promo/fPromoDialog.$language.tpl"}
</div>