{assign var="title" value="BUSCAR PROMOCI&Oacute;N"}
<form name="frmSearchAllPromo" id="frmSearchAllPromo" method="post" action="{$document_root}modules/promo/fViewPromo">
    <div class="span-24 last title">
    	B&uacute;squeda de Promoci&oacute;n<br>
          <label>(*) Campos Obligatorios</label>
    </div>
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            La promoci&oacute;n se ha actualizado exitosamente!
        {elseif $error eq 2}
            Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
        {elseif $error eq 3}
            No se pudo actualizar la promoci&oacute;n al pa&iacute;s seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 4}
            No se pudo actualizar la promoci&oacute;n con la ciudad seleccionada. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 5}
            No se pudo actualizar la promoci&oacute;n con el representante seleccionado. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 6}
            No se pudo actualizar la promoci&oacute;n con los comercializadores seleccionados. Por favor comun&iacute;quese con el administrador.
        {elseif $error eq 7}
            La promoci&oacute;n ingresada no existe. Por favor vuelva a intentarlo.        
        {elseif $error eq 8}
            El rango de d&iacute;as de la promo. no es v&aacute;lido, debe ser ascendente,            ejemplo: 1,4-5,15-17,19,25  .
        {/if}
        </div>
    </div>

    {/if}     

    <div class="span-24 last line">
        <div class="prepend-7 span-5 label">* Promociones Aplicadas a: </div>
        <div class="span-4 append-6 last">
            <select name="selApliedTo" id="selApliedTo" title='El campo "Aplicadas a" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$apliedTo item="l"}
                <option value="{$l}">{$global_appliedToTypes.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>    

    <div class="prepend-3 span-20 last line" id="appliedToContainer"></div>
	<div class="span-24 last line" id="promoContainer"></div>

   <!--<div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Ver" >
    </div>-->
    <input type="hidden" name="hdnPromoID" id="hdnPromoID" value=""/>
</form>