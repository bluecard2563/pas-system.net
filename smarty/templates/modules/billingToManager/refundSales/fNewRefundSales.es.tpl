{assign var="title" value="RETIRAR COSTO INTERNACIONAL"}
<form name="frmNewRefundSales" id="frmNewRefundSales" method="post" action="{$document_root}modules/billingToManager/refundSales/pNewRefundSales">

	<div class="span-24 last title ">
    	Retirar Costo Internacional<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
			{if $error eq 1}
			<div class="span-10 success" align="center">
				Actualizaci&oacute;n de ventas exitosa.
			</div>	
			{elseif $error eq 2}
			<div class="span-10 success" align="center">
				Ocurri&oacute; un error al excluir las ventas.
			</div>	
			{elseif $error eq 3}
			<div class="span-10 success" align="center">
				No se seleccion&oacute; ninguna venta para modificar.
			</div>	
			{/if}
    </div>
    {/if}

	<div class="append-6 span-12 prepend-6 last">
		<ul id="alerts" class="alerts"></ul>
	</div>


	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">* Pa&iacute;s:</div>
        <div class="span-6 last">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}">{$l.name_cou}</option>
				{/foreach}
			</select>
        </div>
		<div class="span-3 label">* Representante:</div>
        <div class="span-5 append-3 last" id="selManagerContainer">
			<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
				<option value="">- Seleccione -</option>
			</select>
        </div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>* Fecha inicio:</label>
        </div>
        <div class="span-6 last">
            <input type="text" id="txtBeginDate" name="txtBeginDate" title='El campo "Fecha inicio" es obligatorio.'/>
        </div>

        <div class="span-3">
            <label>* Fecha fin:</label>
        </div>
        <div class="span-6 append-2 last">
            <input type="text" id="txtEndDate" name="txtEndDate" title='El campo "Fecha fin" es obligatorio.'/>
        </div>
    </div>
	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last buttons line">
		<input type="button" name="btnSearch" id="btnSearch" value="Buscar" >
	</div>
	<div class="span-24 last line" id="managerSales">
		
	</div>
</form>