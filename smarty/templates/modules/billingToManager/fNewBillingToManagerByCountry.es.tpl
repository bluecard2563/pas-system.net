{assign var="title" value="FACTURACI&Oacute;N A REPRESENTANTES POR PAIS"}
<form name="frmNewBillingToManagerByCountry" id="frmNewBillingToManagerByCountry" method="post" action="{$document_root}modules/billingToManager/pNewBillingToManagerByCountry" target="_blank" class="">

	<div class="span-24 last title ">
    	Facturaci&oacute;n a Representantes Por Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
		<div class="span-10 error" align="center">
			{if $error eq 1}
				Ocurri&oacute; un error al crear factura.
			{elseif $error eq 2}
				No existen representantes en el Pa&iacute;s seleccionado.
			{elseif $error eq 3}
				No se seleccion&oacute; un pa&iacute;s v&aacute;lido.
			{/if}
		</div>
    </div>
    {/if}

	<div class="append-6 span-12 prepend-6 last">
		<ul id="alerts" class="alerts"></ul>
	</div>


	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Pa&iacute;s:</div>
        <div class="span-8 append-2 last">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}">{$l.name_cou}</option>
				{/foreach}
			</select>
        </div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* N&uacute;mero de factura:</div>
        <div class="span-8 append-2 last">
			<input type="text" name="txtDocNumber" id="txtDocNumber"  title="El campo 'N&uacute;mero de factura' es obligatorio." />
        </div>
	</div>

	<div class="span-24 last">
		<div class="prepend-6 span-5 label">* Ordenar por: </div>
        <div class="span-8 append-2 last">
			<input type="radio" name="rdoOrderBy" value="dea.name_dea" checked> Por Comercializador
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-11 span-8 append-2 last">
			<input type="radio" name="rdoOrderBy" value="sal.emission_date_sal"> Por Fecha
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">Fecha Hasta:</div>
        <div class="span-8 append-2 last">
			<input type="text" name="txtToDate" id="txtToDate"  title="El campo 'Fecha Hasta' es obligatorio." />
        </div>
	</div>

	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last buttons line">
		<input type="button" name="btnReport" id="btnReport" value="Generar Informe" >
		<input type="hidden" name="hdnRemoteValidation" id="hdnRemoteValidation" value="1" />
		<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
	</div>

</form>