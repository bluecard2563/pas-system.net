{assign var="title" value="REIMPRESI&Oacute;N DE FACTURA A REPRESENTANTES"}
<form name="frmReprintBill" id="frmReprintBill" method="post" target="_blank" action="{$document_root}modules/billingToManager/pReprintBillingToManager">

	<div class="span-24 last title ">
		Reimpresi&oacute;n de Factura a Representantes<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	
	{if $error eq 'no_invoice'}
    <div class="append-7 span-10 prepend-7 last">
		<div id="errorContainer" class="span-10 error" align="center">
			No existe una factura para los datos ingresados.
		</div>
    </div>
    {/if}

	<div class="append-6 span-12 prepend-6 last line">
		<ul id="alerts" class="alerts"></ul>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Pa&iacute;s:</div>
        <div class="span-8 append-2 last">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio." class="span-4">
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}">{$l.name_cou}</option>
				{/foreach}
			</select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Representante:</div>
        <div class="span-8 append-2 last" id="selManagerContainer">
			<select name="selManager" id="selManager" title="El campo 'Manager' es obligatorio." class="span-4">
				<option value="">- Seleccione -</option>
			</select>
        </div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* No. de Factura:</div>
        <div class="span-8 append-2 last">
			CM - <input type="text" name="txtInvoiceNumber" id="txtInvoiceNumber" title="El campo 'No. de Factura' es obligatorio." class="span-2"/>
        </div>
	</div>

	<div class="span-24 buttons last line">
		<input type="button" name="btnSubmit" id="btnSubmit" value="Reimprimir Factura" />
	</div>
</form>