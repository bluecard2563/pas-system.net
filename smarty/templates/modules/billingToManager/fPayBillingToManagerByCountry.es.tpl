{assign var="title" value="PAGO DE FACTURAS A REPRESENTANTES POR PAIS"}
<form name="frmPayBillingToManagerByCountry" id="frmPayBillingToManagerByCountry" method="post" action="{$document_root}modules/billingToManager/pPayBillingToManagerByCountry" class="">

	<div class="span-24 last title ">
    	Pago de Facturas a Representantes Por Pa&iacute;s<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
		<div id="errorContainer" class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Factura a representante liquidada exitosamente.
				{if $fileName}
					<br/>
					<input type="hidden" value="{$fileName}" id="hdnFileName"/>
					<a id="viewInvoiceLink" href="{$document_root}modules/billingToManager/reports/{$fileName}.pdf" target="_blank">Ver Factura</a>
				{/if}
			{elseif $error eq 2}
				Hubo errores al liquidar la factura, cont&aacute;ctese con el Administrador.
			{/if}
		</div>
    </div>
    {/if}

	<div class="append-6 span-12 prepend-6 last">
		<ul id="alerts" class="alerts"></ul>
	</div>


	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Pa&iacute;s:</div>
        <div class="span-8 append-2 last">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}">{$l.name_cou}</option>
				{/foreach}
			</select>
        </div>
	</div>

	<div class="span-24 last line">&nbsp;</div>

	<div class="prepend-7 span-10 append-7 last line" id="billingToManagerByCountryContainer"></div>
</form>