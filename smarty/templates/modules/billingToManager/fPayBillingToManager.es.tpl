{assign var="title" value="FACTURACI&Oacute;N A REPRESENTANTES"}
<form name="frmPayBillingToManager" id="frmPayBillingToManager" method="post" action="{$document_root}modules/billingToManager/pPayBillingToManager" class="">

	<div class="span-24 last title ">
		Facturaci&oacute;n a Representantes<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
		<div id="errorContainer" class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Factura a representante liquidada exitosamente.
				{if $fileName}
					<br/>
					<input type="hidden" value="{$fileName}" id="hdnFileName"/>
					<a id="viewInvoiceLink" href="{$document_root}modules/billingToManager/reports/{$fileName}.pdf" target="_blank">Ver Factura</a>
				{/if}
			{elseif $error eq 2}
				Error al liquidar la factura, no hay ventas para facturar.
			{elseif $error eq 3}
				Error al liquidar la factura, no se pudieron actualizar todas las ventas.
			{elseif $error eq 4}
				Error al liquidar la factura, no se pudo registrar la factura como pagada.
			{/if}
		</div>
    </div>
    {/if}

	<div class="append-6 span-12 prepend-6 last">
		<ul id="alerts" class="alerts"></ul>
	</div>


	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Pa&iacute;s:</div>
        <div class="span-8 append-2 last">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_cou}">{$l.name_cou}</option>
				{/foreach}
			</select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Representante:</div>
        <div class="span-8 append-2 last" id="selManagerContainer">
			<select name="selManager" id="selManager" title="El campo 'Manager' es obligatorio.">
				<option value="">- Seleccione -</option>
			</select>
        </div>
	</div>

	<div class="span-24 last line">&nbsp;</div>

	<div class="prepend-7 span-10 append-7 last line" id="billingToManagerContainer"></div>
</form>