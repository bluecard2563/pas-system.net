<form id="frmUpdateCreditDays" name="frmUpdateCreditDays" method="post" action="{$document_root}modules/creditDay/pUpdateCreditDay" >	
<div class="span-1 span-8 prepend-1 last line">
    <ul id="alerts_dialog" class="alerts"></ul>
</div>
    
<div class="span-10 last line">
	<div class="prepend-1 span-4">
    	D&iacute;as de Cr&eacute;dito:
    </div>
    <div class="span-4 append-1 last">
    	<input type="text" name="txtUpCreditD" id="txtUpCreditD" value="" title="El campo 'D&iacute;as' es obligatorio." />
    </div>
</div>

<div class="span-10 last line">
	<div class="prepend-1 span-4">
    	Estado:
    </div>
    <div class="span-4 append-1 last">
       	<select name="selStatus" id="selStatus" title="El campo 'Estado' es obligatorio.">
     		<option value="">- Seleccione -</option>
            {foreach from=$statusList item="i"}
        	<option value="{$i}" text="{$global_status.$i}">{$global_status.$i}</option>
            {/foreach}
        </select>
    </div>
</div>
<input type="hidden" name="serialItem" id="serialItem" />
<input type="hidden" name="serial_cou" id="serial_cou" />
</form>