{assign var="title" value="ADMINISTRACI&Oacute;N DE D&Iacute;AS DE CR&Eacute;DITO"}
<form id="frmNewCreditDays" name="frmNewCreditDays" method="post" action="{$document_root}modules/creditDay/pNewCreditDay" >	
	<div class="prepend-3 span-19 append-2 last title ">
    	Administraci&oacute;n de D&iacute;as de Cr&eacute;dito
    </div>

	{if $error}
    <div class="append-6 span-10 prepend-7 last">
    	<div class="span-10 {if $error eq 1 or $error eq 3}success{else}error{/if}" align="center">
  		{if $error eq 1}
            El d&iacute;a de cr&eacute;dito se ha registrado exitosamente!
        {elseif $error eq 2}
            No se pudo registrar el d&iacute; de cr&eacute;dito.
        {elseif $error eq 3}
        	El d&iacute;a de cr&eacute;dito se ha actualizado exitosamente!
        {elseif $error eq 4}
        	No se pudo actualizar el d&iacute;a de cr&eacute;dito.
        {/if}
        </div>
    </div>
    {/if}
    
    <div class="append-7 span-9 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-8 label">Seleccione el pa&iacute;s</div>
        <div class="span-8 append-2 last">     	                                  
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio' class="select" >
              <option value="">- Seleccione -</option>
              {foreach from=$countryList item="l"}
              <option value="{$l.serial_cou}" >{$l.name_cou}</option>
              {/foreach}
            </select>
        </div>
    </div>        
    <div class="prepend-3 span-19 append-2 last" id="creditDayInfo"></div>
    
    <div class="prepend-3 span-19 append-2 last line" id="newInfo">
    	<div class="span-19 last title ">
    		Nuevo D&iacute;a de Cr&eacute;dito
    	</div>
    
        <div class="span-19 last line">
            <div class="prepend-1 span-8 label">D&iacute;a de Cr&eacute;dito:</div>
            <div class="span-8 append-2 last">
                <input type="text" name="txtCDay" id="txtCDay" title="El campo 'D&iacute;a' es obligatorio."> 
            </div>
        </div>

        <div class="span-19 last line buttons">
            <input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar">
        </div>
    </div>
</form>
<div id="dialog" title="Actualizar D&iacute;a de Cr&eacute;dito">
	{include file="templates/modules/creditDay/fUpdateCreditDayDialog.$language.tpl"}
</div>