{assign var="title" value="ACTUALIZAR CONDICI&Oacute;N PARTICULAR"} 
<form name="frmNewPCondition" id="frmNewPCondition" method="post" action="{$document_root}modules/particular_conditions/{if $prc}pUpdatePCondition{else}fUpdatePCondition{/if}">
	<div class="span-24 last title">
		Actualizar Condici&oacute;n Particular<br>
		<label>(*) Campos Obligatorios</label>
    </div>
	
	{if $error}
		<div class="append-7 span-10 prepend-7 last ">
			<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
				{if $error eq 1}
					El registro se ha actualizado exitosamente!
				{else}
					Hubo errores en la actualización. Por favor vuelva a intentarlo.
				{/if}
			</div>
		</div>
    {/if}

	<div class="span-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
    </div>

	{if $prc}
		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">* Idioma:</div>
			<div class="span-5">  
				{$language.name_lang}
				<input type="hidden" name="hdnLanguage" id="hdnLanguage" value="{$prc.serial_lang}" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">* Estado:</div>
			<div class="span-5">  
				<select name="selStatus" id="selStatus" title="El campo 'Estado' es obligaotio" >
					<option value="">- Seleccione -</option>
					{foreach from=$status_list item="s"}
					<option value="{$s}" {if $s eq $prc.status_prc}selected{/if}>{$global_status[$s]}</option>
					{/foreach}
				</select>
			</div>
		</div>

		<div class="prepend-7 span-10 append-7 last line">
			<i>* Ingrese las condiciones generales en el cuadro a continuaci&oacute;n:</i>
		</div>

		<div class="prepend-5 append-5 span-14 last line">
			<textarea name="txtDescription" id="txtDescription"></textarea>
		</div>

		<div class="span-24 last buttons line">
			<input type="button" name="btnRegistrar" id="btnRegistrar" value="Actualizar" />
			<textarea class="hide_me" name="hdnDescription" id="hdnDescription">{$prc.name_prc}</textarea>
			<input type="hidden" name="hdnSerial_prc" id="hdnSerial_prc" value="{$prc.serial_prc}" />
		</div>
	{else}
		{if $pconditions_list}
			<div class="prepend-5 span-14 append-5 last line">
				<table border="0" id="salesTable">
					<THEAD>
					<tr bgcolor="#284787">
						<td align="center" class="tableTitle">
							Condici&oacute;n Particular
						</td>
						<td align="center"  class="tableTitle">
							Estado
						</td>
						<td align="center" class="tableTitle">
							Actualizar
						</td>
					</tr>
					</THEAD>
					<TBODY>
					{foreach name="conditions" from=$pconditions_list item="p"}
						<tr {if $smarty.foreach.conditions.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
							<td align="center" class="tableField">
								{$p.name_prc|truncate_html}
							</td>
							<td align="center" class="tableField">
								{$global_status[$p.status_prc]}
							</td>
							<td align="center" class="tableField">
								<a href="{$document_root}modules/particular_conditions/fUpdatePCondition/0/{$p.serial_prc}" >Ir</a>
							</td>
						</tr>
					{/foreach}
					</TBODY>
				</table>
			</div>
		{else}
			<div class="prepend-7 span-10 append-7 last line">
				<div class="span-10 error">
					No existen registros para actualizar.
				</div>
			</div>
		{/if}
	{/if}
</form>