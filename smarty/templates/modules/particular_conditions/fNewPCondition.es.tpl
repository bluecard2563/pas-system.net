{assign var="title" value="NUEVA CONDICI&Oacute;N PARTICULAR"} 
<form name="frmNewPCondition" id="frmNewPCondition" method="post" action="{$document_root}modules/particular_conditions/pNewPCondition">
	<div class="span-24 last title">
		Nueva Condici&oacute;n Particular<br>
		<label>(*) Campos Obligatorios</label>
    </div>
	
	{if $error}
		<div class="append-7 span-10 prepend-7 last ">
			<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
				{if $error eq 1}
					El registro se ha ingresado exitosamente!
				{else}
					Hubo errores en el ingreso. Por favor vuelva a intentarlo.
				{/if}
			</div>
		</div>
    {/if}

	<div class="span-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-4 label">* Idioma:</div>
        <div class="span-5">  
			Espa&ntilde;ol
			<input type="hidden" name="hdnLanguage" id="hdnLanguage" value="2" />
		</div>
	</div>
	
	<div class="prepend-7 span-10 append-7 last line">
		<i>* Ingrese las condiciones generales en el cuadro a continuaci&oacute;n:</i>
	</div>
	
	<div class="prepend-5 append-5 span-14 last line">
		<textarea name="txtDescription" id="txtDescription"></textarea>
	</div>
    
    <div class="span-24 last buttons line">
		<input type="button" name="btnRegistrar" id="btnRegistrar" value="Registrar" />
		<textarea class="hide_me" name="hdnDescription" id="hdnDescription"></textarea>
    </div>
</form>