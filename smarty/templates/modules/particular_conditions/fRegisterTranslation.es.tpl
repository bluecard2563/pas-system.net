{assign var=title value="REGISTRAR TRADUCCI&Oacute;N"}
<div class="span-24 last title">
	Registrar Traducci&oacute;n de Condici&oacute;n Particular <br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last alerts">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line center label">
	Texto Original:
</div>
	
<div class="prepend-7 span-10 append-7 last line">
	<div class="span-10" style="text-align: justify;">  
		{$pcp_data.name_prc}
	</div>
</div>
	
<div class="span-24 last line">
	<div class="prepend-7 span-4 label">
		* Idioma:
		<input type="hidden" name="hdnSerial_prc" id="hdnSerial_prc" value="{$pcp_data.serial_prc}" />
	</div>
	<div class="span-5">  
		<select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio">
			<option value="">- Seleccione -</option>
			{foreach from=$languages item="l"}
				<option value="{$l.serial_lang}">{$l.name_lang}</option>
			{/foreach}
		</select>
	</div>
</div>
		
<div class="span-24 last line" id="translationContainer"></div>