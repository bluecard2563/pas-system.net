{assign var=title value="TRADUCIR CONDICIONES PARTICULARES"}
<div class="span-24 last title">
	Traducir Condiciones Particulares
</div>

{if $error}
	<div class="prepend-7 span-10 append-7 last">
		<div class="{if $error eq 1}success{else}error{/if} span10" align="center" >
			{if $error eq 1}
				La traducci&oacute;n se registr&oacute; exitosamente!
			{elseif $error eq 2}
				Hubo errores al registrar la traducci&oacute;n. Vuelva a intentarlo.
			{elseif $error eq 3}
				Hubo errores en la carga de datos. Por favor vuelva a intentarlo.
			{/if}
		</div>
	</div>
{/if}

{if $pconditions_list}
	<div class="prepend-4 span-16 append-4 last line">
		<table border="1" class="dataTable" id="tblPConditions">
			<thead>
				<tr class="header">					
					<th style="text-align: center;" >
						No.
					</th>
					<th style="text-align: center;" >
						Condici&oacute;n Particular
					</th>
					<th style="text-align: center;" >
						Traducir
					</th>
				</tr>
			</thead>
			<tbody>
			{foreach name="pConditions" from=$pconditions_list item="pc"}
			<tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.pConditions.iteration is even}class="even"{else}class="odd"{/if}>
				<td align="center" class="tableField">
					{$smarty.foreach.pConditions.iteration}
				</td>
				<td align="center" class="tableField">
					{$pc.name_prc|truncate_html}
				</td>
				<td align="center" class="tableField">
					<a class="link" href="{$document_root}modules/particular_conditions/fRegisterTranslation/{$pc.serial_prc}" >Ir</a>
				</td>
			</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
{else}
	<div class="prepend-7 span-10 append-7 last">
		<div class="error span10" align="center" >
			No existe condiciones particulares en el sistema.
		</div>
	</div>
{/if}