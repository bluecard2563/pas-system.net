{assign var=title value="ASIGNACI&Oacute;N DE CONDICIONES PARTICULARES"}
<form name="frmAssignPCondition" id="frmAssignPCondition" action="{$document_root}modules/particular_conditions/pAssociatePCondition" method="post" class="form_smarty">
    <div class="span-24 last title">Asignaci&oacute;n de Condiciones Particulares</div>
	
    {if $error}
        <div class="prepend-6 span-12 last">
            <div class="{if $error eq 1 || $error eq 2}success{else}error{/if} " align="center" >
                {if $error eq 1}
                    La asignaci&oacute;n de paises se realiz&oacute; correctamente.
                {/if}
                {if $error eq 2}
                    Las tarifas se asignaron exitosamente.
                {/if}
                {if $error eq 3}
                    Existi&oacute; un error al asignar las tarifas por favor int&eacute;ntelo nuevamente.
                {/if}
                {if $error eq 4}
                    El producto seleccionado no existe por favor int&eacute;ntelo nuevamente.
                {/if}
            </div>
        </div>
    {/if}
	
	{if $pc_list}
    <div class="prepend-7 span-9 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			 * Zona:
		</div>
		
		<div class="span-5 label">
			<select id="selZone" name="selZone" class="span-5" title="Por favor seleccione una zona." >
                <option value="">-Seleccione-</option>
                {foreach from="$zoneList" item="z"}
                <option value="{$z.serial_zon}" {if $z.serial_zon eq $serial_zon}selected="selected"{/if}>
                    {$z.name_zon}
                </option>
                {/foreach}
            </select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			* Pa&iacute;s:
		</div>
		
		<div class="span-5 left" id="countryContainer">
			<select id="selCountry" name="selCountry" class="span-5" title="Por favor seleccione un pa&iacute;s." >
                <option value="">-Seleccione-</option>
            </select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			 * Producto:
		</div>
		
		<div class="span-5 left" id="productContainer">
			<select id="selProduct" name="selProduct" class="span-5"  title="Por favor seleccione un producto." >
                <option value="">-Seleccione-</option>
            </select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			* Condici&oacute;n Particular:
		</div>
		
		<div class="span-5 left">
			<select id="selPCondition" name="selPCondition" class="span-5"  title="Por favor seleccione una Condici&oacute;n Particular." >
                <option value="">-Seleccione-</option>
                {if $pc_list}
                    {foreach from="$pc_list" item="p"}
                    <option value="{$p.serial_prc}">
                        {$p.name_prc|truncate_html}
                    </option>
                    {/foreach}
                {/if}
            </select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			* Orden de Despliegue:
		</div>
		
		<div class="span-5 left">
			<input type="text" name="txtWeight" id="txtWeight" title="El campo 'Orden' es obligatorio" />
		</div>
	</div>
			
	<div class="span-24 last line buttons">
		<input type="button" name="btnValidate" id="btnValidate" value="Validar" />
	</div>
			
	<div class="span24 last line" id="currentConditionsContainer"></div>
	
	{else}
		<div class="prepend-7 span-10 append-7 last">
            <div class="error span10" align="center" >
                No existe condiciones particulares en el sistema.
            </div>
        </div>
	{/if}
</form>