{assign var="title" value="REPORTE DE CUENTAS POR PAGAR"}
<div class="span-24 last line title">
    Reporte de cuentas por pagar<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmPendingPaymentsReport" id="frmPendingPaymentsReport" method="post" action="{$document_root}modules/dealerReports/payments/pDealerPendingPaymentsReportPDF" target="blank">
	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	{if $serial_dea}
	{if $invoicesList}
	<div class="prepend-1 span-22 append-1 last">
		<table border="0" id="invoicesTable">
			<THEAD>
				<tr bgcolor="#284787">
					{foreach from="$titles" item="t"}
					<td align="center" style="border: solid 1px white; padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						{$t}
					</td>
					{/foreach}
				</tr>
			</THEAD>
			<TBODY>
				  {foreach name="invoices" from=$invoicesList item="i"}
					<tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if} >
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
						   {$i.date_inv}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$i.number_inv}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$i.invoice_to|htmlall}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$creditDays}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$i.days}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{if $i.days lt 31}
								{if $i.status_inv eq 'STAND-BY'}
									{$i.total_to_pay}
								{else}
									{$i.total_to_pay_fee}
								{/if}
							{/if}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{if $i.days gt 30 && $i.days lt 61}
								{if $i.status_inv eq 'STAND-BY'}
									{$i.total_to_pay}
								{else}
									{$i.total_to_pay_fee}
								{/if}
							{/if}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{if $i.days gt 60 && $i.days lt 91}
								{if $i.status_inv eq 'STAND-BY'}
									{$i.total_to_pay}
								{else}
									{$i.total_to_pay_fee}
								{/if}
							{/if}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{if $i.days gt 90}
								{if $i.status_inv eq 'STAND-BY'}
									{$i.total_to_pay}
								{else}
									{$i.total_to_pay_fee}
								{/if}
							{/if}
						</td>
					</tr>
				  {/foreach}
				  <tr>
					  <td align="center" class="tableTitle" style="border: solid 1px white;">&nbsp;</td>
					  <td align="center" class="tableTitle" style="border: solid 1px white;">&nbsp;</td>
					  <td align="center" class="tableTitle" style="border: solid 1px white;">&nbsp;</td>
					  <td align="center" class="tableTitle" style="border: solid 1px white;">&nbsp;</td>
					  <td align="center" bgcolor="#284787" style="border: solid 1px white; padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						  Total:
					  </td>
					  <td align="center" bgcolor="#284787" style="border: solid 1px white; padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						  {$total_days.total_30}
					  </td>
					  <td align="center" bgcolor="#284787" style="border: solid 1px white; padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						  {$total_days.total_60}
					  </td>
					  <td align="center" bgcolor="#284787" style="border: solid 1px white; padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						  {$total_days.total_90}
					  </td>
					  <td align="center" bgcolor="#284787" style="border: solid 1px white; padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
						  {$total_days.total_more}
					  </td>
				  </tr>
			</TBODY>
		</table>
	</div>
	<div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>

	<div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
		<input type="hidden" name="hdnCount" id="hdnCount" value="{$invoice_count}" />
    </div>
	{else}
	<div class="span-10 append-7 prepend-7 last">
		<div class="span-10 error center">
			No existen cuentas por cobrar para esta Sucursal de Comercializador.
		</div>
	</div>
		
	{/if}
	{else}
	<div class="span-10 append-7 prepend-7">
		<div class="span-10 error center">
			Usted es un usuario del tipo representante. Para obtener informaci&oacute;n sobre cuentas por pagar haga uso de los <b>Reportes Gerenciales</b>. <br><br>
			En el caso de que no tenga acceso a &eacute;stos, comun&iacute;quese con el administrador.
		</div>
	</div>
	{/if}
</form>