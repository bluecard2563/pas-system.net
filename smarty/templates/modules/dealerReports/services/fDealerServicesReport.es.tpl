{assign var="title" value="REPORTE DE SERVICIOS"}
<div class="span-24 last title">
    Servicios por Comercializador<br />
    <label>(*) Campos Obligatorios</label>
</div>
<div class="span-24 last line">
    {if not $zoneList}
        <div class="span-12 last error" id="message" align="center">
            No se encontraron registros.
        </div>
    {else}

        <div class="span-7 span-10 prepend-7 last line">
            <ul id="alerts" class="alerts"></ul>
        </div>

        <form target="_blank" name="frmDealerServicesReport" id="frmDealerServicesReport" method="post">

            <div class="span-24 last line">

                <div class="prepend-4 span-4 label">* Zona:</div>
                <div class="span-5">
                    <select name="selZone" id="selZone" title="El campo 'Zona' es olbigatorio.">
                        <option value="">- Seleccione -</option>
                        {foreach from=$zoneList item="z"}
                            <option value="{$z.serial_zon}">{$z.name_zon}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="span-3 label">* Pa&iacute;s:</div>
                <div class="span-5 append-3 last" id="countryContainer">
                    <select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio">
                        <option value="">- Seleccione -</option>
                    </select>
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">Representante:</div>
                <div class="span-5" id="managerContainer">
                    <select name="selManager" id="selManager">
                        <option value="">- Seleccione -</option>
                    </select>
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-4 span-4 label">Comercializador</div>
                <div class="span-5" id="dealerContainer">
                    <select name="selDealer" id="selDealer">
                        <option value="">- Todos -</option>
                    </select>
                </div>
            </div>

            <div class="span-24 last line">
                <div class="span-8 label">Servicio:</div>
                <div class="span-9 last" id="servicesContainer">
                    <select name="selService" id="selService" title='El campo "Beneficio" es obligatorio'>
                        <option value="">- Todos -</option>
                    </select>
                </div>  
            </div>   

            <div class="span-24 last line">

                <label class="prepend-6" for="from">*Fecha Inicio</label>
                <input readonly type="text" id="from" name="from" title="El campo 'Fecha Inicio' es obligatorio"/>
                <label class="prepend-1" for="to">*Fecha Fin</label>
                <input readonly type="text" id="to" name="to" title="El campo 'Fecha Fin' es obligatorio"/>

            </div>    

            <div class="span-24 last line" id="generate">
                <div class="prepend-10 span-5">
                    Generar reporte:
                    <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"/>
                </div>
            </div>
        </form>
    {/if}
</div>