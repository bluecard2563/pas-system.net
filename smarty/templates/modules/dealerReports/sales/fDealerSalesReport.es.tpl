{assign var="title" value="REPORTE DE VENTAS"}
<div class="span-24 last title">
	Reporte de Ventas<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $serial_dea}
<form id="frmDealerSalesReport" name="frmDealerSalesReport" action="{$document_root}modules/dealerReports/sales/pDealerSalesReportPDF" method="post" target="_blank">
	<div class="span-7 span-10 prepend-7 last line">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="prepend-2 span-20 append-2 last line">
        <div class="prepend-1 span-7 label">
			<input type="radio" name="rdoSearch" id="rdoCustomerSearch" value="CUSTOMER" checked/>
			Por nombre o documento del cliente:
		</div>
        <div class="span-10 append-2 last">
            <input type="text" name="txtCustomerData" id="txtCustomerData" class="autocompleter" />
			<input type="hidden" name="hdnSerialCus" id="hdnSerialCus" value=""  title="Seleccione un cliente de la lista." />
		</div>
	</div>	
	
	<div class="prepend-4 span-20 last line">
		<input type="radio" name="rdoSearch" id="rdoParameterSearch" value="PARAMETER"/><b>Por par&aacute;metros:</b>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			Estado de la Tarjeta:
		</div>
		<div class="span-6 last">
			<select id="selStatus" name="selStatus" disabled>
				<option value="">- Todos -</option>
				{foreach from=$statusList item="s"}
					<option value="{$s}">{$global_salesStatus.$s}</option>
				{/foreach}
			</select>
		</div>
		<div class="span-3 label">
			Producto:
		</div>
		<div class="span-6 append-2 last">
			<select id="selProduct" name="selProduct" disabled>
				<option value="">- Todos -</option>
				{foreach from=$productList item="p"}
					<option value="{$p.serial_pbd}">{$p.name_pbl}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-3 span-4 label">
            Fecha de:
        </div>
        <div class="span-6 last">
            <input type="radio" id="rdoDateEmission" name="rdoDate" value="emission" checked /> Emisi&oacute;n<br />
            <input type="radio" id="rdoDateCoverage" name="rdoDate" value="coverage"/> Cobertura
        </div>
    </div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Fecha Inicio:
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio.' disabled />
		</div>
		<div class="span-3 label">
			* Fecha Fin:
		</div>
		<div class="span-6 append-2 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio.' disabled />
		</div>
	</div>

	<div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
    </div>
</form>
{else}
<div class="span-10 append-7 prepend-7">
	<div class="span-10 error center">
		Usted es un usuario del tipo representante. Para obtener informaci&oacute;n sobre ventas haga uso de los <b>Reportes Gerenciales</b>. <br><br>
		En el caso de que no tenga acceso a &eacute;stos, comun&iacute;quese con el administrador.
	</div>
</div>
{/if}