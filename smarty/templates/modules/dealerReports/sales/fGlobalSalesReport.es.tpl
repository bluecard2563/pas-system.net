{assign var=title value="REPORTE GENERAL DE VENTAS"}
<div class="span-24 last title">
	Reporte de Ventas de Sucursales<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $serial_dea}
	{if $branch_list}
		<form id="frmGlobalSales" name="frmGlobalSales" action="{$document_root}modules/dealerReports/sales/pGlobalSalesPDF" method="post" target="_blank">
			<div class="span-7 span-10 prepend-7 last line">
				<ul id="alerts" class="alerts"></ul>
			</div>

			<div class="span-24 last line separator">
				<label>Informaci&oacute;n de la Sucursal</label>
			</div>

			<div class="span-24 last line">
				<div class="prepend-5 span-6 label">*Seleccione las sucursales:</div>
				<div class="span-24 last line">
					<div class="prepend-2 span-9">
						<div align="center">Seleccionados</div>
						<select multiple id="dealersTo" name="dealersTo[]" class="selectMultiple span-9 last" title="El campo de sucursales de comercializador es obligatorio."></select>
					</div>
					<div class="span-2 last buttonPane">
						<br />
						<input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
						<input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
						<input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
						<input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
					</div>
					<div class="span-9 last" id="dealersFromContainer">
						<div align="center">Existentes</div>
						<select multiple id="dealersFrom" name="dealersFrom[]" class="selectMultiple span-9 last">
							{foreach from=$branch_list item="b"}
							<option value="{$b.serial_dea}">{$b.code_dea} - {$b.name_dea}</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>

			<div class="span-24 last line separator">
				<label>Fechas del Reporte</label>
			</div>

			<div class="span-24 last line">
				<div class="prepend-3 span-4 label">
					* Fecha Inicio:
				</div>
				<div class="span-6 last">
					<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio.' />
				</div>
				<div class="span-3 label">
					* Fecha Fin:
				</div>
				<div class="span-6 append-2 last">
					<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio.' />
				</div>
			</div>

			<div class="span-24 last buttons line">
				Generar reporte:
				<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
				<input type="button" name="btnGenerateXLS" id="btnGenerateXLS" class="XLS" />
				<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
			</div>
		</form>
	{else}
		<div class="span-10 append-7 prepend-7">
			<div class="span-10 error center">
				No existen sucursales registradas con el documento de su comercializador.
			</div>
		</div>
	{/if}
{else}
	<div class="span-10 append-7 prepend-7">
		<div class="span-10 error center">
			Usted es un usuario del tipo representante. Para obtener informaci&oacute;n sobre ventas haga uso de los <b>Reportes Gerenciales</b>. <br><br>
			En el caso de que no tenga acceso a &eacute;stos, comun&iacute;quese con el administrador.
		</div>
	</div>
{/if}