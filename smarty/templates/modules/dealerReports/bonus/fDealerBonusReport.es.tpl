{assign var="title" value="REPORTE DE INCENTIVOS"}
<div class="span-24 last title">
	Reporte de Incentivos<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $statusList}
<form id="frmDealerBonusReport" name="frmDealerBonusReport" action="{$document_root}modules/dealerReports/bonus/pDealerBonusReportPDF" method="post" target="_blank">
	<div class="span-24 last line buttons">
		<input type="radio" name="rdFilterType" value="CARD" id="CARD" checked />
			<label for="CARD">No. Tarjeta</label>
		<input type="radio" name="rdFilterType" value="STATUS" id="STATUS" />
			<label for="STATUS">Estado</label>
    </div>
	
	<div class="span-24 last line" id="cardContainer">
		<div class="prepend-8 span-4 label">
			* No. Tarjeta:
		</div>
		<div class="span-6 append-6 last">
			<input type="text" name="txtCardNumber" id="txtCardNumber" title="El campo 'No. de Tarjeta' es obligatorio." />
		</div>
	</div>
	
	<div class="span-24 last line" id="statusContainer">
		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">
				* Estado de la Tarjeta:
			</div>
			<div class="span-6 append-6 last">
				<select id="selStatus" name="selStatus" title="El campo 'Estado' es obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from=$statusList item="s"}
						{if $s neq 'REFUNDED'}
						<option value="{$s}">{$global_appliedBonusStatus.$s}</option>
						{/if}
					{/foreach}
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">
				* Fecha Inicio:
			</div>
			<div class="span-6 last">
				<input type="text" name="txtBeginDate" id="txtBeginDate" title="El campo 'Fecha Inicio' es obligatorio." />
			</div>
			<div class="span-3 label">
				* Fecha Fin:
			</div>
			<div class="span-6 append-2 last">
				<input type="text" name="txtEndDate" id="txtEndDate" title="El campo 'Fecha Fin' es obligatorio."/>
			</div>
			<input type="hidden" name="today" id="today" value="{$today}" />
		</div>
	</div>

	<div class="span-24 last line buttons">
        <input type="submit" name="btnSubmit" id="btnSubmit" value="Generar Reporte" />
    </div>
</form>
{else}
<div class="span-10 append-7 prepend-7">
	<div class="span-10 error center">
		Usted es un usuario del tipo representante. Para obtener informaci&oacute;n sobre incentivos haga uso de los <b>Reportes Gerenciales</b>. <br><br>
		En el caso de que no tenga acceso a &eacute;stos, comun&iacute;quese con el administrador.
	</div>
</div>
{/if}