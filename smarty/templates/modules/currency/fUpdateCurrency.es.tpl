{assign var="title" value="ACTUALIZAR MONEDA"}
<form name="frmUpdateCurrency" id="frmUpdateCurrency" method="post" action="{$document_root}modules/currency/pUpdateCurrency" class="form_smarty">

	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Moneda<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 error" align="center">
            {if $error eq 1}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line"> 
        <div class="prepend-7 span-6">
            <div class="">
                <label>*Nombre de la moneda:</label>
            </div>
        </div>
        <div class="span-8 append-1 last">
            <div class="">
                <input type="text" name="txtNameCurrency" id="txtNameCurrency" title='El campo "Nombre de la moneda" es obligatorio' {if $data.name_cur}value="{$data.name_cur}"{/if}>
            </div>
        </div>
	</div>
    
    <div class="span-24 last line"> 
        <div class="prepend-7 span-6">
            <div class="">
                <label>*Cambio en relaci&oacute;n al d&oacute;lar:</label>
            </div>
        </div>
        <div class="span-8 append-1 last">
            <div class="">
                <input type="text" name="txtExchange_fee_cur" id="txtExchange_fee_cur" title='El campo "Cambio en relaci&oacute;n al d&oacute;lar" es obligatorio' {if $data.exchange_fee_cur}value="{$data.exchange_fee_cur}"{/if}>
            </div>
        </div>
    </div>
    
    <div class="span-24 last line"> 
        <div class="prepend-7 span-6">
            <div class="">
                <label>*Nuevo s&iacute;mbolo de la moneda:</label>
            </div>
        </div>
        <div class="span-8 append-1 last">
            <div class="">
                <input type="text" name="txtSymbol_cur" id="txtSymbol_cur" title='El campo "S&iacute;mbolo de la moneda" es obligatorio' {if $data.symbol_cur}value="{$data.symbol_cur}"{/if}>
            </div>
        </div>
    </div>
     
	<div class="span-24 last line"> 
        <div class="prepend-7 span-6">
            <div class="">
                <label>Estado de la moneda:</label>
            </div>
        </div>
        <div class="span-8 append-1 last">     	                                  
            <select name="selStatus" id="selStatus" title='El campo "Estado de la moneda" es obligatorio.'>
                 {foreach from=$statusList item="l"}
                 <option value="{$l}" {if $data.status_cur eq $l}selected{/if}>{$global_status.$l}</option>
                 {/foreach}
            </select>
        </div>
    </div>
	
    <div class="span-24 last buttons line">
    	<input maxlength="30" type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_cur" id="hdnSerial_cur" {if $data.serial_cur}value="{$data.serial_cur}"{/if} >
    </div>
</form>

