{assign var="title" value="NUEVA MONEDA"}
<form name="frmNewCurrency" id="frmNewCurrency" method="post" action="{$document_root}modules/currency/pNewCurrency" class="form_smarty">

	<div class="span-24 last title ">
    	Registro de Nueva Moneda<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	La moneda se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	No se pudo insertar.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
     
    <div class="span-24 last">
    
        <div class="span-24 last line"> 
            <div class="prepend-7 span-5 append-0">
                <label>*Nombre de la moneda:</label>
            </div>
            <div class="span-7 append-2 last">
                <div class="">
                    <input type="text" name="txtNameCurrency" id="txtNameCurrency" title='El campo "Nombre de la moneda" es obligatorio.'>
                </div>
            </div>
        </div>
        
        <div class="span-24 last line"> 
            <div class="prepend-7 span-5 append-0">
                <div class="">
                    <label>*Cambio en relaci&oacute;n al d&oacute;lar:</label>
                </div>
            </div>
            <div class="span-7 append-2 last">
                <div class="controles">
                    <input type="text" name="txtExchange_fee_cur" id="txtExchange_fee_cur" title='El campo "Cambio en relaci&oacute;n al d&oacute;lar" es obligatorio.'>
                </div>
            </div>
        </div>
        
        <div class="span-24 last line"> 
             <div class="prepend-7 span-5 append-0">
                <div class="">
                    <label>*S&iacute;mbolo:</label>
                </div>
            </div>
            <div class="span-7 append-2 last">
                <div class="controles">
                    <input type="text" name="txtSymbol_cur" id="txtSymbol_cur" title='El campo "S&iacute;mbolo" es obligatorio.'>
                </div>
            </div>
     	</div>
        <div class="span-24 last buttons line">
            <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" class="" >
        </div>
    </div>
</form>