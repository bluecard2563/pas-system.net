{assign var="title" value="BUSCAR MONEDA"}
<form name="frmSearchCurrency" id="frmSearchCurrency" action="{$document_root}modules/currency/fUpdateCurrency" method="post" class="form_smarty">

	<div class="span-24 last title ">
        Buscar Moneda<br />
        <!--<label>(*) Campos Obligatorios</label>-->
    </div>
     
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                La Moneda se actualiz&oacute; correctamente!
            {else}
            	No existe la moneda ingresada. Por favor seleccione una de la lista.
            {/if}
        </div>
    </div>
    {/if} 
           
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
     </div>
    
   
{if $currList}
<div class="prepend-5 span-14 append-5 last line">
	<table border="0" id="tblCurrency" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					Nombre
				</td>
				<td align="center" class="tableTitle">
					S&iacute;mbolo
				</td>
				<td align="center" class="tableTitle">
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="alert" from=$currList item="c"}
			<tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.alert.iteration}
				</td>
				<td align="center" class="tableField">
					{$c.name_cur}
				</td>
				<td align="center" class="tableField">
					{$c.symbol_cur}
				</td>
				<td align="center" class="tableField">
					<a href="{$document_root}modules/currency/fUpdateCurrency/{$c.serial_cur}">Actualizar</a>
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last buttons" id="pageNavPosition"></div>
{else}
<div class="prepend-8 span-8 append-8 last" align="center">
	<div class="span-8 error last">
		No existen monedas.
	</div>
</div>
{/if}
    
</form>