{assign var="title" value="CONFIRMAR VENTA WEB"}
<form name="frmConfirmSale" id="frmConfirmSale" method="post" action="{$document_root}modules/confirmOnlineSales/fDoConfirm">
    <div class="span-24 last title">
    	Confimraci&oacute;n de venta<br>
		<label>(*) Campos Obligatorios</label>
    </div>
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    {if $error}
	    <div class="append-7 span-10 prepend-7 last ">
	        <div class="span-10 {if $error eq -1}success{else}error{/if}" id="messageDiv" align="center">
	    	{if $error eq -1}
	            La Venta fue confirmada exitosamente!.
	        {elseif $error eq 1}
	            La venta seleccionada ya fue confirmada anteriormente.
	        {elseif $error eq 2}
	            No se encontr&oacute; la Factura asociada a la venta.
	        {elseif $error eq 3}
	            No se Encontr&oacute; la Venta asociada al c&oacute;digo ingresado.
	        {/if}
	        </div>
	    </div>
    {/if}     

    <div class="span-24 last line">
        <div class="prepend-7 span-5 label">* Ingrese el C&oacute;digo de Confirmaci&oacute;n: </div>
        <div class="span-4 append-6 last">
            <input type="text" name="txtSerialInput" id="txtSerialInput" title='El campo "Serial del Pago" es obligatorio.'>
        </div>
    </div>

    <div class="prepend-3 span-20 last line" id="searchResults"></div>

    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Buscar" >
    </div>
</form>