{assign var="title" value="ACTAS DE FINIQUITO"}
<div class="span-24 last title">
    ACTAS DE FINIQUITO<br />
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last ">
    <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
        {if $error eq 1}
            El acta de finiquito se ha generado correctamente. Para imprimirla, haga click <a href="{$document_root}system_temp_files/settlement_{$settlement_id}" target="_blank">aqu&iacute;</a>
        {elseif $error eq 2}
            No se puede generar el acta de finiquito. Por favor intente m&aacute;s tarde.
        {elseif $error eq 3}
            Error al general el Acta. No se tiene suficiente informaci&oacute;n para continuar.
        {elseif $error eq 4}
            Error al general el Acta. No existe informaci&oacute;n de cobertura que aplique.
        {elseif $error eq 5}
            Error al general el Acta. No se pudo guardar la cabecera dle acta correctamente.
        {elseif $error eq 6}
            Error al general el Acta. No se pudo generar el acta completamente. El proceso fue terminado.
        {/if}
    </div>
</div>
{/if}

{if $nonSettledCases}

<div class="span-24 last line separator">
    <label>Casos pendientes:</label>
</div>

<center>
    <div class="span-24 last line">
        <table border="1" class="dataTable" id="casesTable">
            <thead>
                <tr class="header">
                    <th style="text-align: center;" >
                        No. Expediente
                    </th>
                    <th style="text-align: center;" >
                        No. Tarjeta
                    </th>
                    <th style="text-align: center;" >
                        Beneficiario
                    </th>
                    <th style="text-align: center;" >
                        Acciones
                    </th>
                </tr>
            </thead>
            <tbody>
            {foreach name="cases" from=$nonSettledCases item="a"}
            <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.cases.iteration is even}class="odd"{else}class="even"{/if}>
                <td align="center" class="tableField">
                    {$a.serial_fle}
                </td>
                <td align="center" class="tableField">
                    {$a.policy}
                </td>
                <td align="center" class="tableField">
                    {$a.beneficiary}
                </td>
                <td align="center" class="tableField">
                    <a id="case_{$a.serial_fle}" case_no="{$a.serial_fle}">Generar Finiquito</a>
                </td>
            </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</center>
            
<div id="liquidation_container"></div>
{else}
        <center>No hay solicitudes pendientes en su pa&iacute;s.</center>
{/if}