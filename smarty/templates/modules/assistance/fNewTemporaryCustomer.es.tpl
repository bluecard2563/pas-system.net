{assign var="title" value="SOLICITUD DE CLIENTE TEMPORAL"}
<form name="frmNewTempDealer" id="frmNewTempDealer" method="post" action="{$document_root}modules/assistance/pNewTemporaryCustomer" class="form_smarty">
    <div class="span-24 last title">
        Solicitud de Cliente Temporal<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                La solicitud fue ingresado exitosamente.
            {elseif $error eq 2}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n del operador:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona del operador" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach name="zones" from=$zoneList item="l"}
                    <option value="{$l.serial_zon}"{if $smarty.foreach.zones.total eq 1}selected{/if}>{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s del operador" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre de la agencia:</div>
        <div class="span-5">
            <input type="text" name="txtAgencyName" id="txtAgencyName" title='El campo "Nombre de la agencia" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n del cliente:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
            <select name="selTravelZone" id="selTravelZone" title='El campo "Zona del cliente" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach name="zones" from=$zoneList item="l"}
                    <option value="{$l.serial_zon}"{if $smarty.foreach.zones.total eq 1 or $l.serial_zon eq $data.serial_zon}selected{/if}>{$l.name_zon}</option>
                    {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="travelCountryContainer">
            <select name="selCountryTravel" id="selCountryTravel" title='El campo "Pa&iacute;s del cliente" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {if $countryList}
                {foreach name="countries" from=$countryList item="l"}
                    <option value="{$l.serial_cou}"{if $l.serial_cou eq $data.serial_cou}selected{/if}>{$l.name_cou}</option>
                {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Nombre:</div>
        <div class="span-5">
            <input type="text" name="txtFirstName" id="txtFirstName" {if $data.first_name}value="{$data.first_name}"{/if} title='El campo "Nombre" es obligatorio.' />
        </div>

        <div class="span-3 label">* Apellido:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtLastName" id="txtLastName" {if $data.last_name}value="{$data.last_name}"{/if} title='El campo "Apellido" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Documento:</div>
        <div class="span-5">
            <input type="text" name="txtDocument" id="txtDocument" {if $data.document_cus}value="{$data.document_cus}"{/if} title='El campo "Documento" es obligatorio.' />
        </div>

        <div class="span-3 label">* Fecha de nacimiento:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtBirthDate" id="txtBirthDate" {if $data.birthdate_cus}value="{$data.birthdate_cus}"{/if}  title='El campo "Fecha de nacimiento" es obligatorio.' />
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Telefono:</div>
        <div class="span-5">
            <input type="text" name="txtPhone" id="txtPhone" {if $data.phone1_cus}value="{$data.phone1_cus}"{/if} title='El campo "Tel&eacute;fono" es obligatorio.' />
        </div>

        <div class="span-3 label">* Tel&eacute;fono de contacto:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtContactPhone" id="txtContactPhone" title='El campo "Tel&eacute;fono de contacto" es obligatorio.' />
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Direcci&oacute;n de contacto:</div>
        <div class="span-5">
            <input type="text" name="txtContactAddress" id="txtContactAddress" title='El campo "Direcci&oacute;n de contacto" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de la solicitud:</label>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-5 span-4 label">* Descrpci&oacute;n del problema:</div>
        <div class="span-5">
            <textarea name="txtProblem" id="txtProblem"  title='El campo "Descrpci&oacute;n del problema" es obligatorio.'></textarea>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-5 span-4 label">Comentarios:</div>
        <div class="span-5">
            <textarea name="txtExtras" id="txtExtras"></textarea>
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="hidden" name="hdnCardNumber" id="hdnCardNumber" value="{$card_number}"/>
        <input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" />
    </div>

</form>