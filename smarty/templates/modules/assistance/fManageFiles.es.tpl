{assign var="title" value="ADMINISTRCI&Oacute;N DE EXPEDIENTES"}
<div class="span-24 last title ">
	Administraci&oacute;n de Expedientes<br />
	<label>(*) Campos Obligatorios</label>
</div>

{* {if $has_permission} *}
	{if $file_list}
		<div class="span-24 separator">
			<label>Expedientes Actuales</label>
		</div>

		<div class="span-24 last line">
			<table border="1" class="dataTable" align="center" id="filesTable">
				<thead>
					<tr class="header">
						<th style="text-align: center;">ID</th>
						<th style="text-align: center;"># Tarjeta</th>
						<th style="text-align: center;">Diagn&oacute;stico</th>
						<th style="text-align: center;">Cliente</th>
						<th style="text-align: center;">Valor Estimada</th>
						<th style="text-align: center;">Valor Real</th>
						<th style="text-align: center;">Pa&iacute;s</th>
						<th style="text-align: center;">Estado</th>
					</tr>
				</thead>
				<tbody>
					{foreach name="files" from=$file_list item=f}
					<tr {if $smarty.foreach.files.iteration is even}class="odd"{else}class="even"{/if} >
						<td class="tableField">
							<a href="{$document_root}modules/assistance/fLiquidateAssistance/{$f.serial_fle}">{$f.serial_fle}</a>
						</td>
						<td class="tableField">{$f.card_number_sal}</td>
						<td class="tableField">{$f.diagnosis_fle}</td>
						<td class="tableField">{$f.customer}</td>
						<td class="tableField">{$f.total_estimated}</td>
						<td class="tableField">{$f.total_real}</td>
						<td class="tableField">{$f.name_cou}</td>
						<td class="tableField">{$global_fileStatus[$f.status_fle]}</td>
					</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	{else}
		<div class="append-7 span-10 prepend-7 last">
			<div class="span-10 error">
				No existen expedientes en el sistema.
			</div>
		</div>
	{/if}
{* {else}
	<div class="append-7 span-10 prepend-7 last">
		<div class="span-10 error">
			Usted no tiene permisos para ingresar a esta p&aacute;gina.<br />
			Por favor contacte al administrador.
		</div>
	</div>
{/if} *}