{assign var="title" value="NUEVO/ACTUALIZAR EXPEDIENTE"}
<form id="frmNewAssistance" name="frmNewAssistance" method="post" action="{$document_root}modules/assistance/pNewAssistance" class="form_smarty">
    <div class="span-24 last title ">
    	Crear o Modificar Expediente<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="span-24 last line">
        <div class="append-5 span-14 prepend-5 last">
            <div class="span-14 {if $error < 12 or $error >12}error{else}success{/if}" align="center">
            {if $error eq 1}
                La tarjeta #{$card_number} no existe en el sistema, por favor contactese con el Jefe Operativo.
                <br /><input name="btnTempCustomer" id="btnTempCustomer" type="button" value="Solicitud" />
            {elseif $error eq 2}
                La tarjeta #{$card_number} no ha sido facturada, por favor contactese con el Jefe Operativo.
                <br /><input name="btnTempCustomer" id="btnTempCustomer" type="button" value="Solicitud" />
            {elseif $error eq 3}
                La factura de la tarjeta #{$card_number} se encuentra en stand-by, por favor contactese con el Jefe Operativo.
            {elseif $error eq 4}
                La factura de la tarjeta #{$card_number} ha sido anulada, por favor contactese con el Jefe Operativo.
            {elseif $error eq 5}
                La tarjeta #{$card_number} ha sido anulada, por favor contactese con el Jefe Operativo.
            {elseif $error eq 6}
                La tarjeta #{$card_number} ha sido rechazada, por favor contactese con el Jefe Operativo.
            {elseif $error eq 7}
                La tarjeta #{$card_number} ha sido reembolsada, por favor contactese con el Jefe Operativo.
            {elseif $error eq 9}
                Hubo problemas al crear el cliente temporal. Por favor vuelva a intentarlo.
                <br /><input name="btnTempCustomer" id="btnTempCustomer" type="button" value="Solicitud" />
            {elseif $error eq 10}
                Hubo problemas al enviar la notificaci&oacute;n de nuevo cliente temporal.
			{elseif $error eq 11}
                La tarjeta #{$card_number} es free, para poder recibir asistencias debe estar autorizada.
            {elseif $error eq 12}
                Cliente temporal ingresado exitosamente.
            {elseif $error eq 13}
				El n&uacute;mero de tarjeta ingresado es una venta masiva, ingrese n&uacute;meros de tarjetas con formato 'numeroTarjeta_#'.
            {/if}
            </div>
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-8 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4">
            <label>N&uacute;mero de Tarjeta:</label>
        </div>
        <div class="span-4 last">
            <input type="text" name="txtCard" id="txtCard" title='El campo "Tarjeta" es obligatorio'>
        </div>
        <div class="prepend-1 span-6 last line">
            <input type="button" name="btnSubmit" id="btnSubmit" value="Validar" class=""  />
        </div>
    </div>

</form>

<form id="frmSearchCustomer" name="frmSearchCustomer" action="" class="form_smarty">
    <div class="span-24 last line" id="customerContainer">
        <div class="prepend-11 span-5" id="BtnSrchCustomer">
            <a name="srchCustomer" id="srchCustomer" style="cursor: pointer">Buscar cliente</a>
        </div>

        <div class="span-24" id="searchCustomerContainer"  style="display: none">
            <div class="span-24 last line separator">
                <label>Buscar Cliente:</label>
            </div>

            <div class="span-7 span-10 prepend-8 last line">
                <ul id="searchAlerts" class="alerts"></ul>
            </div>

            <div class="span-24 last title ">
                <label>Ingrese el documento o nombre del cliente para realizar la b&uacute;squeda.</label>
            </div>

            <div class="span-24 last line">
                <div class="prepend-7 span-4">
                    <label>Documento:</label>
                </div>
                <div class="span-4 last">
                    <input type="text" name="txtCustomerID" id="txtCustomerID" value="" />
                </div>
            </div>

            <div class="span-24 last line">
                <div class="prepend-7 span-4">
                    <label>Nombre:</label>
                </div>
                <div class="span-4 last">
                    <input type="text" name="txtCustomerName" id="txtCustomerName" value="" />
                </div>
            </div>

            <div class="span-24 last buttons line">
				<input type="hidden" name="hdnValidate" id="hdnValidate" title="Ingrese el documento o el nombre del cliente." value=""/>
                <input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
            </div>
        </div>
    </div>

    <div class="prepend-2 span-21 append-1" id="customerInfoContainer"></div>
</form>
