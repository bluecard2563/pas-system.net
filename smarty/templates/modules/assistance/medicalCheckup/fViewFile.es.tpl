{assign var="title" value="EXPEDIENTE DE ASISTENCIA"}
<div class="span-24 last title">
	Expediente de Asistencia<br />
	{if not $read_only}<label>* Campos Obligatorios</label>{/if}
</div>

<form name="frmMedicalCheckup" id="frmMedicalCheckup" action="{$document_root}modules/assistance/medicalCheckup/pViewFile" method="post" enctype="multipart/form-data" >
	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line separator">
		<label>Datos del Cliente</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">
			{if $customer.type_cus neq 'LEGAL_ENTITY'}Nombre:{else}Raz&oacute;n Social:{/if}
		</div>
        <div class="span-4">
            {$customer.first_name_cus} {$customer.last_name_cus}
        </div>

		{if $customer.type_cus neq 'LEGAL_ENTITY'}
        <div class="span-4 label">Fecha de Nacimiento:</div>
        <div class="span-4 append-4 last">
            {$customer.birthdate_cus}
        </div>
		{/if}
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Historial M&eacute;dico:</div>
        <div class="span-8">
			{if $customer.medical_background_cus}
				{$customer.medical_background_cus}
			{else}
				No existen registros.
			{/if}
        </div>

		{if $seniorAnswers}
        <div class="prepend-1 span-3 append-4 last"><a id="seniorQuestions">Declaraci&oacute;n de Salud</a></div>
		{/if}
	</div>

	<div class="span-24 last line separator">
		<label>Datos de la Venta</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Producto:</div>
        <div class="span-5">
            {$saleInfo.name_pbl}
        </div>

        <div class="span-3 label">Fecha de Compra:</div>
        <div class="span-4 append-4 last">
            {$saleInfo.emission_date_sal}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Inicio de Vigencia:</div>
        <div class="span-5">
            {$saleInfo.begin_date_sal}
        </div>

        <div class="span-3 label">Fin de Vigencia:</div>
        <div class="span-4 append-4 last">
            {$saleInfo.end_date_sal}
        </div>
	</div>

	{if $saleServices}
	<div class="span-24 last line separator">
		<label>Servicios Adicionales:</label>
	</div>

	<div class="span-24 last line">
		<table border="0" id="servicesTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						No. Archivo
					</td>
					<td align="center" class="tableTitle">
						Nombre
					</td>
					<td align="center" class="tableTitle">
						Cobertura
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="services" from=$saleServices item="a"}
				<tr {if $smarty.foreach.services.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						{$smarty.foreach.services.iteration}
					</td>
					<td align="center" class="tableField">
						{$a.name_sbl}
					</td>
					<td align="center" class="tableField">
						{$a.coverage_sbl}
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
	<div class="span-24 last line buttons" id="pageNavPositionServices"></div>
	{/if}

	<div class="span-24 last line separator">
		<label>Datos M&eacute;dicos</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Tipo de Asistencia:</div>
        <div class="span-4">
            {if $file.type_fle}
				{$file.type_fle}
			{else}
				N/A
			{/if}
        </div>

        <div class="span-4 label">Fecha de ocurrencia:</div>
        <div class="span-4 append-4 last">
            {$file.incident_date_fle}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Causa:</div>
        <div class="span-5">
            {$file.cause_fle}
        </div>

		<div class="span-3 label">Diagn&oacute;stico:</div>
        <div class="span-4 append-4 last">
            {$file.diagnosis_fle}
        </div>
	</div>

	<div class="span-24 last line separator">
		<label>Bit&aacute;cora</label>
	</div>

	<div class="prepend-2 append-2 span-20 last line">
		<div class="span-20" id="blog_tabs">
			<ul>
				<li><a href="#tabs-1">Cliente</a></li>
				<li><a href="#tabs-2">Operador</a></li>
			</ul>
			<div id="tabs-1" class="blog">
				{$clientBlog.entry_blg|strip_tags:true}
			</div>
			<div id="tabs-2" class="blog">
				{$operatorBlog.entry_blg|strip_tags:true}
			</div>
			
		</div>
	</div>	

	{if $documents}
	<div class="span-24 last line separator">
		<label>Documentos Relacionados:</label>
	</div>

	<div class="span-24 last line">
		<table border="0" id="documents" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						No. Archivo
					</td>
					<td align="center" class="tableTitle">
						Nombre
					</td>
					<td align="center" class="tableTitle">
						Fecha de Ingreso
					</td>
					<td align="center"  class="tableTitle">
						Monto Ingresado
					</td>
					<td align="center" class="tableTitle">
						Revisar
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="app" from=$documents item="a"}
				<tr {if $smarty.foreach.app.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						{$smarty.foreach.app.iteration}
					</td>
					<td align="center" class="tableField">
						{$a.name_dbf}
					</td>
					<td align="center" class="tableField">
						{$a.date_dbf}
					</td>
					<td align="center" class="tableField">
						{$a.amount_dbf}
					</td>
					<td align="center" class="tableField">
						<a target="_blank" href="{$document_root}{$a.url_dbf}">Ver Archivo</a>
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
	<div class="span-24 last line buttons" id="pageNavPosition"></div>
	{/if}

	{if $previousFiles}
	<div class="span-24 last line separator">
		<label>Casos Anteriores:</label>
	</div>

	<div class="span-24 last line">
		<table border="0" id="filesTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						No. Archivo
					</td>
					<td align="center" class="tableTitle">
						No. Tarjeta
					</td>
					<td align="center" class="tableTitle">
						Diagn&oacute;stico
					</td>
					<td align="center" class="tableTitle">
						Estado
					</td>
					<td align="center" class="tableTitle">
						Fecha del Expediente
					</td>
					<td align="center" class="tableTitle">
						Ver Expediente
					</td>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="files" from=$previousFiles item="a"}
				<tr {if $smarty.foreach.files.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						{$smarty.foreach.files.iteration}
					</td>
					<td align="center" class="tableField">
						{$a.card_number}
					</td>
					<td align="center" class="tableField">
						{$a.diagnosis_fle}
					</td>
					<td align="center" class="tableField">
						{$global_fileStatus[$a.status_fle]}
					</td>
					<td align="center" class="tableField">
						{$a.creation_date_fle}
					</td>
					<td align="center" class="tableField">
						<a href="{$document_root}modules/assistance/fLiquidateAssistance/{$a.serial_fle}/view" target="_blank" >Ver Expediente</a>
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
	<div class="span-24 last line buttons" id="pageNavPositionFiles"></div>
	{/if}

	<div class="span-24 last line separator">
		<label>Observaciones M&eacute;dicas:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">{if not $read_only}* {/if}Observaciones:</div>
        <div class="span-10">
			<textarea id="txtObservations" name="txtObservations" title="EL campo 'Observaciones' es obligatorio." {if $read_only}readonly{/if}>{$auditor_comments}</textarea>
        </div>
	</div>

	<div class="prepend-4 span-4 label">Subir Archivo:</div>
    <div class="span-10">
		<input type="file" id="medical_checkup" name="medical_checkup" />
	</div>

	<div class="span-24 last buttons">
		{if $read_only}
			<input type="button" name="btnBack" id="btnBack" value="Regresar" onclick="javascript:history.back()" />
		{else}
			<input type="button" name="btnSubmit" id="btnSubmit" value="Registrar" />
		{/if}
		<input type="hidden" name="hdnSerial_prq" id="hdnSerial_prq" value="{$serial_prq}"/>
	</div>
</form>

{if $seniorAnswers}
<div class="span-16" id="dialog">
	<div class="span-16 last line separator">
		<label>Preguntas Registradas</label>
	</div>
	{foreach from=$seniorAnswers item="s" key="k"}
	<div class="span-16 line">
		<div class="span-8">
			<strong>{$k+1}) {$s.text_qbl}</strong>
		</div>
		<div class="span-8 last">
			{if $s.yesno_ans neq ''}
				{$global_yes_no[$s.yesno_ans]}
			{else}
				{$s.answer_ans}
			{/if}
		</div>
	</div>
	{/foreach}
</div>
{/if}