{assign var="title" value="AUDITOR&Iacute;A M&Eacute;DICA"}
<div class="span-24 last title">
	Reclamaciones pendientes de Auditor&iacute;a M&eacute;dica<br />
</div>

{if $error}
<div class="span-7 prepend-7 append-7">
	<div class="span-10 center {if $error eq 1}success{else}error{/if}">
		{if $error eq 1}
			Se ha actualizado la reclamaci&oacute;n.
		{elseif $error eq 2}
			Hubo errores al cargar la informaci&oacute;n de la reclamaci&oacute;n.
		{elseif $error eq 3}
			Hubo errores al registrar los datos de la auditor&iacute;a. Por favor vuelva a intentarlo.
                {elseif $error eq 4}
			Hubo errores al enviar correo electr&oacute;nico de confirmaci&oacute;n.
		{/if}
	</div>
</div>
{/if}

{if $pendingApplications}
<div class="span-24 last line">
	<table border="0" id="applicationsTable" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No. Expediente
				</td>
				<td align="center" class="tableTitle">
					Nombre del Cliente
				</td>
				<td align="center" class="tableTitle">
					Nombre del Solicitante
				</td>
				<td align="center"  class="tableTitle">
					Diagn&oacute;stico
				</td>
				<td align="center" class="tableTitle">
					Fecha de Solicitud
				</td>
				<td align="center" class="tableTitle">
					Revisar
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="app" from=$pendingApplications item="a"}
			<tr {if $smarty.foreach.app.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$a.serial_fle}
				</td>
				<td align="center" class="tableField">
					{$a.name_cus}
				</td>
				<td align="center" class="tableField">
					{$a.name_usr}
				</td>
				<td align="center" class="tableField">
					{$a.diagnosis_fle}
				</td>
				<td align="center" class="tableField">
					{$a.request_date_prq}
				</td>
				<td align="center" class="tableField">
					<a href="{$document_root}modules/assistance/medicalCheckup/fViewFile/{$a.serial_prq}">Atender</a>
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last buttons" id="pageNavPosition"></div>
{else}
<div class="span-10 append-7 prepend-7">
	<div class="span-10 error center">No tiene reclamaciones en espera de auditor&iacute;a m&eacute;dica.</div>
</div>
{/if}
