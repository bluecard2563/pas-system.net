{assign var="title" value="ASISTENCIAS"} 
<div class="span-24 last line">

    {if $data.emition OR $data.status_inv}
    <div class="span-24 last">
        <div class="append-5 span-14 prepend-5 last">
            <div class="span-14 last error">
                <div class="span-1"><img src="{$document_root}img/information.png"></div>
                <div class="span-12" align="center" {if not $data.emition}style="margin-top: 7px;"{/if}>
                    {if $data.emition eq 1}La tarjeta fue emitida hace menos de 48 horas.<br/>{/if}
		    {if $data.emition eq 2}Tenga en cuenta que esta tarjeta ya ha caducado.<br/>{/if}
                </div>
                <div class="span-12" align="center" {if not $data.status_inv}style="margin-top: 14px;"{/if}>
                    {if $data.status_inv eq 1}La factura de la venta no ha sido pagada y est&aacute; fuera de los d&iacute;as de cr&eacute;dito.<br/>{/if}
                    {if $data.status_inv eq 2}La factura de la venta no ha sido pagada pero est&aacute; dentro de los d&iacute;as de cr&eacute;dito.<br/>{/if}
                </div>
            </div>
        </div>
    </div>
    {/if}

    {if $error}
    <div class="span-24 last line" id="successMessage">
        <div class="append-5 span-14 prepend-5 last">
            <div class="span-14 {if $error eq 1 or $error eq 3}success{else}error{/if}" align="center">
                {if $error eq 1}
                    Expediente ingresado exitosamente.
                {elseif $error eq 2}
                    Hubo errores en el expediente. Por favor vuelva a intentarlo.
                {elseif $error eq 3}
                    Expediente actualizado exitosamente.
                {elseif $error eq 4}
                    Expediente actualizado exitosamente pero hubo errores al enviar e-mail informativo.
				{elseif $error eq 5}
                    Hubo errores al crear la solicitud de asistencia.
				{elseif $error eq 6}
                    Hubo errores al enviar e-mail de solicitud de asistencia.
                {/if}
            </div>
        </div>
    </div>
    {/if}

    <div class="prepend-20 span-2 label">
    Tarjeta No.:
    </div>
    <div class="span-2 last">
    {$data.card_number}
	<input type="hidden" name="hdnCardNumber" id="hdnCardNumber" value="{$data.card_number}" />
	<input type="hidden" name="hdnSerialSale" id="hdnSerialSale" value="{$data.serial_sal}" />
	<input type="hidden" name="hdnSerialDea" id="hdnSerialDea" value="{$data.dea_serial_dea}" />
    </div>
    
</div>

<!--
MAIN CONTAINER
-->
<input type="hidden" id="hdnSelFilesTab" {if $serial_fle}value="{$serial_fle}"{elseif $other_card}value="{$other_card}"{/if}/>
<div class="span-24 last line" id="filesContainer">
    <ul>
        <li><a href="#tabs-1">Tarjeta</a></li>
        <li><a href="#tabs-2">Asegurados</a></li>
        <li><a href="#tabs-3">Casos Actuales</a></li>
        <li><a href="#tabs-4">Historial de Casos</a></li>
        <li><a href="#tabs-5">Solicitudes</a></li>
        {if $liquidation_permission}
        <li><a href="#tabs-6">Administraci&oacute;n de Casos</a></li>
        {/if}
        
    </ul>

    <div id=tabs-1><!--BENEFITS-->
		<div class="span-22 last line separator">
            <label>Informaci&oacute;n:</label>
        </div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Producto:</div>
			<div class="span-4">{if $data.name_pbl}{$data.name_pbl|htmlall}{else}-{/if}</div>
			<div class="span-4 label">Pa&iacute;s:</div>
			<div class="span-4 append-1 last">{$data.name_cou|htmlall}</div>
		</div>

		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Comercializador:</div>
			<div class="span-4">{$data.name_dea|htmlall}</div>
			<div class="span-4 label">Asesor de Venta:</div>
			<div class="span-4 append-1 last">{$data.name_cnt|htmlall}</div>
		</div>
		
		<div class="prepend-2 span-18 append-2 last line">
			<div class="prepend-1 span-4 label">Representante:</div>
			<div class="span-4">{$data.name_man|htmlall}</div>
		</div>

		<div class="span-22 last line separator">
            <label>Beneficios:</label>
        </div>

		<div class="span-22 last line" id="benefitsContainer">
			<div class="prepend-10 span-5 last line">
				<a id="loadBenefits" style="cursor: pointer;">Ver Beneficios</a>
				{*<img src = "{$document_root}img/loading_small.gif">Cargando Beneficios.*}
			</div>
		</div>

		<!--Sales Log-->
        {if $salesHistory}
        <div class="span-22">
            <center><a style="cursor: pointer;" id="displaySalesLog">Mostrar Historial de Cambios</a></center>
        </div>
        <div id="salesLogContainer" style="display: none;">
            <div class="span-22 last line separator">
                <label>Historial de Cambios:</label>
            </div>

            <center>
                <div class="prepend-2 span-19 last line">
                    <table border="0" class="paginate-1 max-pages-7" id="sales_log_table">
                        <thead>
                            <tr bgcolor="#284787">
                                {foreach from=$sl_titles item="t"}
                                <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                    {$t}
                                </td>
                                {/foreach}
                            </tr>
                        </thead>

                        <tbody>
                        {foreach name="salesHistory" from=$salesHistory item="sl"}
                        <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.salesHistory.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$smarty.foreach.salesHistory.iteration}
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$sl.date_slg }
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$sl.type_slg }
                            </td>
                            <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                {$global_sales_log_status[$sl.status_slg]}
                            </td>
                        </tr>
                        {/foreach}

                        </tbody>
                    </table>
                </div>

                 <div class="span-22 last line pageNavPosition" id="pageNavPositionSalesLog"></div>
            </center>
        </div>
        {/if}
		<!--End Sales Log-->
    </div>

    <div id=tabs-2><!--CUSTOMERS-->
        <div class="span-23 last line" id="beneficiariesContainer">
			<div class="prepend-9 span-5 last line"><img src = "{$document_root}img/loading_small.gif">Cargando Beneficiarios.</div>
        </div>
    </div>

    <div id=tabs-3> <!--FILES-->
        {if $data.opened_files}
        <div class="prepend-2 span-19 last line">
            <ul id="alerts" class="alerts" style="display: block; text-align: center">
                <li>A continuaci&oacute;n se presentan &uacute;nicamente los expedientes abiertos o en liquidaci&oacute;n pertenecientes a las personas relacionadas a esta tarjeta.</li>
            </ul>
        </div>
        
        <center>
            <div class="prepend-2 span-19 last line">
                <table border="0" class="paginate-1 max-pages-7" id="files_table">
                    <thead>
                        <tr bgcolor="#284787">
                            {foreach from=$data.file_titles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                {$t}
                            </td>
                            {/foreach}
                        </tr>
                    </thead>

                    <tbody>
                    {foreach name="fileList" from=$data.opened_files item="fl"}
						<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="{cycle values='#d7e8f9,#e8f2fb'}">
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.serial_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.name_cou}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.name_cit}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.name_cus}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.diagnosis_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.creation_date_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								<a id="openFile{$fl.serial_fle}" serial="{$fl.serial_fle}" style="cursor: pointer;">Abrir</a>
							</td>
							<td id="row_{$fl.serial_fle}" style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{if $fl.status_fle eq 'in_liquidation'}
								En liquidaci&oacute;n
								{else}
									<a id="toLiquidation{$fl.serial_fle}" serial="{$fl.serial_fle}" style="cursor: pointer;">Enviar</a>
								{/if}	
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								<a id="registerDocument{$fl.serial_fle}" serial="{$fl.serial_fle}" style="cursor: pointer;">Documentos</a>
								{if $fl.status_fle eq 'in_liquidation'} 
									| <a id="gradeProviders{$fl.serial_fle}" serial="{$fl.serial_fle}" style="cursor: pointer;">
										{if $fl.all_graded}Ver Calificaciones{else}Calificar{/if}
									</a>
								{/if}
							</td>
							{if $fl.serial_alt}
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								<img src="{$document_root}img/alert.png"/>
							</td>
							{/if}
						</tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>

             <div class="span-22 last pageNavPosition" id="pageNavPositionFiles"></div>
        </center>
        {else}
            <div class="prepend-7 span-10 last line">
                <ul id="alerts" class="alerts" style="display: block; text-align: center">
                    <li>La tarjeta no tiene expedientes abiertos previos.</li>
                </ul>
            </div>
        {/if}
    </div>

    <div id=tabs-4> <!--CLOSED FILES-->
        {if $data.closed_files}
        <div class="prepend-2 span-19 last line">
            <ul id="alerts" class="alerts" style="display: block; text-align: center">
                <li>A continuaci&oacute;n se presentan &uacute;nicamente los expedientes cerrados pertenecientes a las personas relacionadas a esta tarjeta.</li>
            </ul>
        </div>

        <center>
            <div class="prepend-2 span-19 last line">
                <table border="0" class="paginate-1 max-pages-7" id="files_closed_table">
                    <thead>
                        <tr bgcolor="#284787">
                            {foreach from=$data.closed_file_titles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                {$t}
                            </td>
                            {/foreach}
							<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                Documentaci&oacute;n
                            </td>
                        </tr>
                    </thead>

                    <tbody>
                    {foreach name="fileListLiquidation" from=$data.closed_files item="fl"}
						<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="{cycle values='#d7e8f9,#e8f2fb'}">
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.serial_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.name_cou}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.name_cit}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.name_cus}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.diagnosis_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fl.creation_date_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$global_fileStatus[$fl.status_fle]}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								<a id="viewFile{$fl.serial_fle}" serial="{$fl.serial_fle}" style="cursor: pointer;">Ver</a>
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								<a id="view_documents_file{$fl.serial_fle}" serial="{$fl.serial_fle}" style="cursor: pointer;">Ver</a>
							</td>
						</tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>

             <div class="span-22 last pageNavPosition" id="pageNavPositionClosedFiles"></div>
        </center>
        {else}
            <div class="prepend-7 span-10 last line">
                <ul id="alerts" class="alerts" style="display: block; text-align: center">
                    <li>La tarjeta no tiene expedientes cerrados previos.</li>
                </ul>
            </div>
        {/if}
    </div>

    <div id=tabs-5> <!--APPLIANCES-->
        {if $data.appliances}
        <div class="prepend-2 span-19 last line">
            <ul id="alerts" class="alerts" style="display: block; text-align: center">
                <li>A continuaci&oacute;n se presentan todas las solicitudes de expedientes  de las personas relacionadas a esta tarjeta.</li>
            </ul>
        </div>

        <center>
            <div class="prepend-2 span-19 last line">
                <table border="0" class="paginate-1 max-pages-7" id="apliances_table">
                    <thead>
                        <tr bgcolor="#284787">
                            {foreach from=$data.appliance_titles item="t"}
                            <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                {$t}
                            </td>
                            {/foreach}
                        </tr>
                    </thead>

                    <tbody>
                    {foreach name="applianceList" from=$data.appliances item="al"}
                    <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.applianceList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$al.serial_tcu}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$al.name_cou}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$al.first_name_tcu} {$al.last_name_tcu}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$al.problem_description_tcu}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {if $al.serial_fle}{$al.serial_fle}{else}-{/if}
                        </td>
                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                            {$global_ApplianceStatus[$al.status_tcu]}
                        </td>
                    </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>

             <div class="span-22 last pageNavPosition" id="pageNavPositionAppliances"></div>
        </center>
        {else}
            <div class="prepend-7 span-10 last line">
                <ul id="alerts" class="alerts" style="display: block; text-align: center">
                    <li>La tarjeta no tiene solicitudes pendientes.</li>
                </ul>
            </div>
        {/if}
    </div>
    {if $liquidation_permission}
    <div id=tabs-6> <!--LIQUIDATIONS-->
		{if $data.all_files}
		<div class="prepend-2 span-19 last line">
			<ul id="alerts" class="alerts" style="display: block; text-align: center">
				<li>A continuaci&oacute;n se presentan &uacute;nicamente los expedientes abiertos o en stand-by pertenecientes a las personas relacionadas a esta tarjeta.</li>
			</ul>
		</div>

		<center>
			<div class="prepend-1 span-21 last line">
				<table border="0" class="paginate-1 max-pages-7" id="liquidations_table">
					<thead>
						<tr bgcolor="#284787">
							{foreach from=$data.liquidation_titles item="t"}
							<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
								{$t}
							</td>
							{/foreach}
						</tr>
					</thead>

					<tbody>
					{foreach name="fileListLiquidation" from=$data.all_files item="fll"}
						<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="{cycle values='#d7e8f9,#e8f2fb'}">
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fll.serial_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fll.name_cou}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fll.name_cit}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fll.name_cus}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fll.diagnosis_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{$fll.creation_date_fle}
							</td>
							<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								<a id="openLiquidationFile{$fll.serial_fle}" serial="{$fll.serial_fle}" style="cursor: pointer;">Abrir</a>
							</td>
							<td id="medCheckupContainer{$fll.serial_fle}" style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
								{if $fll.status_prq eq 'AUDIT_REQUEST'}
									En espera de respuesta.
								{else}
									<a id="medicalCheckup{$fll.serial_fle}" serial="{$fll.serial_fle}" status="{$fll.status_prq}" style="cursor: pointer;">Enviar</a>
								{/if}
							</td>
						</tr>
					{/foreach}
					</tbody>
				</table>
			</div>
			 <div class="span-22 last pageNavPosition" id="pageNavPositionLiquidations"></div>

		</center>
		{else}
			<div class="prepend-7 span-10 last line">
				<ul id="alerts" class="alerts" style="display: block; text-align: center">
					<li>La tarjeta no tiene expedientes previos.</li>
				</ul>
			</div>
		{/if}
    </div>
    {/if}
</div>

<div class="span-24 last line">
    <div class="prepend-20 span-2">
        <input type="button" name="btnExit" id="btnExit" value="Atras" />
    </div>
</div>
<!--
END MAIN CONTAINER
-->


<!--
DIALOGS
-->
<!--GRADE SERVICE PROVIDER DIALOG-->
<div id="gradeServiceProviderDialog"></div>
<!--END GRADE SERVICE PROVIDER DIALOG-->
<!--ADD DOCUMENT DIALOG-->
<div id="addDocDialog"></div>
<!--END ADD DOCUMENT DIALOG-->

<!--EDIT FILE DIALOG-->
<div id="editeFilesDialog"></div>
<!--END EDIT FILE DIALOG-->

<!--CREATE ALERTS DIALOG-->
<div id="alertsDialogContainer"></div>
<!--END CREATE ALERTS DIALOG-->

<!--DISPLAY ALERTS DIALOG-->
<div id="viewAlertsDialog">
    <div class="span-15 last line separator">
        <label>Alertas de este caso:</label>
    </div>
    <div id="alertsByFileContainer"></div>
</div>
<!--END DISPLAY ALERTS DIALOG-->

<!--PROVIDERS INFO DIALOG-->
<div id="providersInfoDialog">
    <div class="span-18 last line separator">
        <label>Informaci&oacute;n de operadores:</label>
    </div>
    <div class="span-18 last line" id="providersInfoContainer"></div>
</div>
<!--END PROVIDERS INFO DIALOG-->

<!--USED PROVIDERS DIALOG-->
<div id="usedProvidersDialog">
    <div class="span-9 last line separator">
        <label>Coordinadores utilizados:</label>
    </div>
    <div class="span-9 last line" id="usedProvidersContainer"></div>
</div>
<!--END USED PROVIDERS DIALOG-->

<!--PAYMENT REQUEST DIALOG-->
<div id="paymentRequestDialog">
    <div class="span-14 last line separator">
        <label>Selecci&oacute;n de auditor m&eacute;dico:</label>
    </div>

    <input type="hidden" id="hdnSerialFleCheckup" value=""/>

    <div class="span-12 last line" id="paymentRequestMessages" style="display: none;">
        <div class="prepend-3 span-8 append-3 last">
            <div class="span-8" align="center" id="paymentRequestInnerMessage"></div>
        </div>
    </div>

    <div class="span-14 last line">
        <div class="prepend-2 span-4 label">
            Pa&iacute;s:
        </div>
        <div class="span-4 append-1">
            <select id="medicalCheckupCountry">
                <option value="">- Seleccione -</option>
                {foreach from=$country_list item="cl"}
                <option value="{$cl.serial_cou}">{$cl.name_cou}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-14 last line" id="paymentRequestContainer"></div>
</div>
<!--END PAYMENT REQUEST DIALOG-->

<!--UPLOAD FILE DIALOG-->
<div id="uploading_file">
    <div class="prepend-1 span-2" style="margin-top: 27px;">
        <img src="{$document_root}img/ajax-loader.gif"/>
    </div>
    <div class="span-3" style="margin-top: 20px;">
        <span id="messageText">Subiendo archivo</span>
        Espere por favor.
    </div>
</div>
<!--END UPLOAD FILE DIALOG-->

<!--UPLOAD FILE DIALOG-->
<div id="documents_by_file_container"></div>
<!--END UPLOAD FILE DIALOG-->