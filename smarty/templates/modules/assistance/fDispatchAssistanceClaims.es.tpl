{assign var="title" value="LIQUIDACI&Oacute;N DE RECLAMOS"}
<form name="frmDispatchClaims" id="frmDispatchClaims" action="{$document_root}modules/assistance/pDispatchAssistanceClaims" enctype="multipart/form-data" method="post" class="">
    <div class="span-24 last title line">
        Liquidaci&oacute;n de Reclamos<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
        {if $error eq 1}
            Reclamos pagados exitosamente.
            {if $file eq 1}<br /><a href="{$document_root}modules/assistance/Acta_de_Finiquito.xls" target="blank">Descargar Acta de Finiquito</a>{/if}
        {elseif $error eq 2}
            Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
        {elseif $error eq 3}
            Seleccione al menos un reclamo.
        {elseif $error eq 4}
            Ingrese un archivo de extenci&oacute;n v&aacute;lida.
        {elseif $error eq 5}
            El campo "Archivo" es obligatorio.
		{elseif $error eq 6}
            El PDF de la liquidaci&oacute;n no se cre&oacute; correctamente. Por favor vuelva a intentarlo.
        {/if}
        </div>
    </div>
    {/if}

    <div id="otherMessages" class="append-7 span-10 prepend-7 last line" style="display: none;">
        <div id="innerOtherMessages" class="span-10" align="center">
        </div>
    </div>

    <div class="append-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-9 span-3">
            <label>* Pago a:</label>
        </div>
        <div class="span-8 last">
            <select name="selBillTo" id="selBillTo" title='El campo "Pago a" es obligatorio.'>
              <option value="">- Seleccione -</option>
              {foreach from=$documentToList item="l"}
				  {if $l neq 'NONE'}
					<option value="{$l}">{$global_billTo.$l}</option>
				  {/if}
              {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="providerContainer" style="display: none;">
        {if $providerList}
        <div class="prepend-9 span-3">
            <label>* Coordinador:</label>
        </div>
        <div class="span-8 last">
            <select name="selProvider" id="selProvider" title='El campo "Coordinador:" es obligatorio.'>
              <option value="">- Seleccione -</option>
              {foreach from=$providerList item="l"}
              <option value="{$l.serial_spv}">{$l.name_spv|htmlall}</option>
              {/foreach}
            </select>
        </div>
        {else}
        <center><b>No existen Reclamos de Pago A Coordinador pendientes.</b></center>
        {/if}
    </div>

    <div class="span-24 last line" id="billToClientContainer" style="display: none;">
        {if $zoneList}
        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Zona:</div>
            <div class="span-6">
                <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                     <option value="">-Seleccione-</option>
                    {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">* Pa&iacute;s:</div>
            <div class="span-4 append-4 last" id="countryContainer">
                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Ciudad:</div>
            <div class="span-6" id="cityContainer">
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>

            <div class="span-3 label">* Cliente:</div>
            <div class="span-4 append-4 last" id="customerContainer">
                <select name="selCustomer" id="selCustomer" title='El campo "Cliente" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Tarjeta:</div>
            <div class="span-6" id="cardContainer">
                <select name="selCard" id="selCard" title='El campo "Tarjeta" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        {else}
            <center><b>No existen Reclamos de Pago A Cliente pendientes.</b></center>
        {/if}
    </div>

    <div class="span-24 last line" id="filesContainer"></div>

    <div class="span-24 last line" id="paymentRequestContainer"></div>

    <div class="span-24 last line" id="documentsContainer" style="display: none;">
        <div class="prepend-8 span-2">
            <label>* Archivo:</label>
        </div>
        <div class="span-8 last line">
            <input type="file" name="flePaymentRequest" id="flePaymentRequest" title='El campo "Archivo" es obligatorio.' value=""/>
        </div>
    </div>

	<div class="span-24 buttons last line" id="customerPreLiquidation" style="display: none;">
			<input type="button" id="btnCustomerPreLiquidation" name="btnCustomerPreLiquidation" value="Ver Pre-Liquidaci&oacute;n" />
		</div>


    <div class="span-24 last buttons line" id="preLiquidationContainer" style="display: none;">
        <div class="span-24 last buttons line">
            Ver Pre-Liquidaci&oacute;n
        </div>
        <div class="span-24 last buttons line">
            <input type="button" name="btnPreLiquidatePDF" id="btnPreLiquidatePDF" class="PDF" />
            <input type="button" name="btnPreLiquidateXLS" id="btnPreLiquidateXLS"  class="XLS"  />
            <textarea name="chkClaims" id="chkClaims" style="display: none;"></textarea>
            <input type="hidden" name="liquidationType" id="liquidationType"/>
        </div>
    </div>

	
    <div class="span-24 last buttons line" id="btnLiquidateContainer" style="display: none;">
        <input type="button" name="btnLiquidate" id="btnLiquidate"  value="Liquidar" />
	<input type="hidden" id="payOffDocumentPath" name="payOffDocumentPath" value=""/>
    </div>

    <!--FILE BENEFITS DIALOG-->
    <div id="fileViewBenefitsDialog">
        <div class="span-17 last line separator">
            <label>Beneficios asignados:</label>
        </div>
        <div class="span-17 last line" id="viewBenefitsUploadContainer"></div>
    </div>
    <!--END FILE BENEFITS DIALOG-->
</form>

<div id="progressDialog">
    <div class="prepend-1 span-2" style="margin-top: 27px;">
        <img src="{$document_root}img/ajax-loader.gif"/>
    </div>
    <div class="span-3" style="margin-top: 20px;">
        <span id="messageText"></span>
        Espere por favor.
    </div>
</div>