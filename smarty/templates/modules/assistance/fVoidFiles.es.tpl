{assign var="title" value="ANULACI&Oacute;N DE EXPEDIENTES"}
<div class="span-24 last title ">
	Anulaci&oacute;n de Expedientes<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	<div class="span-24 last line">
		<div class="append-5 span-14 prepend-5 last">
			<div class="span-14 {if $error neq 1}error{else}success{/if}" align="center">
				{if $error eq 1}
					El proceso termin&oacute; exitosamente.
				{else}
					Hubo errores en la operaci&oacute;n. Por favor vuelva a intentarlo.
				{/if}
			</div>
		</div>
	</div>
{/if}

<div class="span-7 span-10 prepend-8 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">
		<label>* N&uacute;mero de Tarjeta:</label>
	</div>
	<div class="span-4 last">
		<input type="text" name="txtCard" id="txtCard" title='El campo "Tarjeta" es obligatorio'>
	</div>
</div>

<div class="span-24 last line buttons">
	<input type="button" name="btnSearch" id="btnSearch" value="Validar" />
</div>

<div class="span-24 last line" id="fileTableContainer"></div>