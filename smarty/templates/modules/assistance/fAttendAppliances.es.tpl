{assign var="title" value="ATENDER SOLICITUDES"}
<form name="frmAttendAppliances" id="frmAttendAppliances" method="post" action="{$document_root}modules/assistance/pAttendAppliances" class="form_smarty">
    <div class="span-24 last title">
        Atender Solicitudes<br />
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                Cambios guardados exitosamente.
            {elseif $error eq 2}
                Hubo errores en la actualizaci&oacute;n.
            {elseif $error eq 3}
                Seleccione al menos una solicitud.
            {/if}
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts">asdf</ul>
    </div>
    {if $apliances}

    <div class="span-24 last line separator">
        <label>Solicitudes pendientes:</label>
    </div>

    <center>
        <div class="span-24 last line">
            <table border="1" class="dataTable" id="appliancesTable">
                <thead>
                    <tr class="header">
                        <th style="text-align: center;" >
                            <input type="checkbox" name="selAll" id="selAll" />
                        </th>
                        {foreach name="titles_dialog" from=$titles item="t"}
                            <th style="text-align: center;">
                                {$t}
                            </th>
                        {/foreach}
                    </tr>
                </thead>
                <tbody>
                {foreach name="aplianceList" from=$apliances item="a"}
                <tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.aplianceList.iteration is even}class="odd"{else}class="even"{/if}>
                    <td align="center" class="tableField">
                        <input type="checkbox" name="chkApliance" id="chkApliance" class="chkApliance" value="{$a.serial_tcu}">
                    </td>
                    <td align="center" class="tableField">
                        {$a.card_number_tcu}
                    </td>
                    <td align="center" class="tableField">
                        {$a.status_sal}
                    </td>
                    <td align="center" class="tableField">
                        {$a.user_name}
                    </td>
                    <td align="center" class="tableField">
                        {$a.serial_tcu}
                    </td>
                    <td align="center" class="tableField">
                        {$a.agency_name_tcu}
                    </td>
                    <td align="center" class="tableField">
                        {$a.first_name_tcu} {$a.last_name_tcu}
                    </td>
                    <td align="center" class="tableField">
                        {$a.document_tcu}
                    </td>
                    <td align="center" class="tableField">
                        {$a.phone_tcu}
                    </td>
                    <td align="center" class="tableField">
                        {$a.travel_name_cou}
                    </td>
					<td align="center" class="tableField">
                        <a id="show_description_{$a.serial_tcu}">Ver</a>
                    </td>
					<td align="center" class="tableField">
                        <a id="show_extra_info_{$a.serial_tcu}">Ver</a>
                    </td>
                </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
		<div class="span-24 last pageNavPosition" id="alertPageNavPosition"></div>

        <div class="span-24 last buttons line">
			<input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
            <input type="submit" name="btnApprove" id="btnRegister"  value="Aprobar" />
            <input type="submit" name="btnDeny" id="btnRegister" value="Negar" />
			<input type="hidden" name="hdntotal_items" id="hdntotal_items" value="{$total_pages}" />
        </div>
    </center>
	{foreach name="aplianceList" from=$apliances item="a"}
		 <div id="description_{$a.serial_tcu}">
			 {$a.problem_description_tcu}
		 </div>

		 <div id="extra_info_{$a.serial_tcu}">
			 {$a.extra_tcu}
		 </div>
	 {/foreach}
	{else}
		<center>No hay solicitudes pendientes en su pa&iacute;s.</center>
	{/if}
</form>