{assign var="title" value="LIQUIDACI&Oacute;N DE ASISTENCIAS"}
<form name="frmEditLiquidations" id="frmEditLiquidations">
    <div class="span-24 last title">
        Edici&oacute;n de Liquidaci&oacute;n de asistencias<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>


    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10" id="confirmationMessage" align="center" style="display: none;"></div>
    </div>

	<div class="span-24 last line">
		<div class="span-24 last">
			<div class="prepend-6 span-4 label">* N&uacute;mero de Tarjeta:</div>
			<div class="span-4">
				<input type="text" name="txtCard" id="txtCard" title="El campo 'N&uacute;mero de Tarjeta' es obligatorio."/>
			</div>
			<div class="span-5 last">
				<input type="button" name="btnSearch" id="btnSearch" value="Buscar"/>
			</div>
		</div>
	</div>

	<div class="span-24" id="fileContainer"><center>Ingrese un n&uacute;mero de tarjeta.</center></div>
</form>

<!--UPLOAD FILE DIALOG-->
<div id="deleting_file">
    <div class="prepend-1 span-2" style="margin-top: 27px;">
        <img src="{$document_root}img/ajax-loader.gif"/>
    </div>
    <div class="span-3" style="margin-top: 20px;">
        <span id="messageText">Eliminando archivo</span>
        Espere por favor.
    </div>
</div>
<!--END UPLOAD FILE DIALOG-->

<!--FILE BENEFITS DIALOG-->
<div id="fileViewBenefitsDialog">
    <div class="span-17 last line separator">
        <label>Beneficios asignados:</label>
    </div>
    <div class="span-17 last line" id="viewBenefitsUploadContainer"></div>
</div>
<!--END FILE BENEFITS DIALOG-->