{assign var="title" value="LIQUIDACI&Oacute;N DE ASISTENCIAS"}
{if $data.serial_fle}
<form name="frmNewBranch" id="frmNewBranch" method="post" action="{$document_root}modules/branch/pNewBranch" class="">
    <div class="span-24 last title">
        Liquidaci&oacute;n de asistencias<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <input type="hidden" name="accesPermition" id="accesPermition" value="{$acces_permition}"/>

    <!--Medical Information-->
    <div class="span-24 last line separator">
        <label>Datos m&eacute;dicos y de ubicaci&oacute;n:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-1 span-3 label">Tarjeta No.:</div>
        <div class="span-5  append-3">
            <input type="text" id="txtCardNumber" value="{$card_number}" readonly />
        </div>

        <div class="span-4 label">Expediente No.:</div>
        <div class="span-5 append-2 last">
            <input type="text" id="txtSerialFle" value="{$data.serial_fle}" readonly />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-1 span-3 label">Pa&iacute;s:</div>
        <div class="span-5  append-3">
            <input type="text" value="{$data.name_cou}" readonly />
        </div>

        <div class="span-4 label">Ciudad:</div>
        <div class="span-5 append-2 last">
            <input type="text" value="{$data.name_cit}" readonly />
            <input type="hidden" id="hdnSerialCit" value="{$data.serial_cit}"/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-1 span-3 label">Tipo de asistencia:</div>
        <div class="span-5  append-3">
            <input type="text" id="txtAssistanceType" value="{$global_fileTypes[$data.type_fle]}" readonly />
        </div>

        <div class="span-4 label">Fecha de ocurrencia:</div>
        <div class="span-5 append-2 last">
            <input type="text" id="txtIncidentDate" value="{$data.incident_date_fle}" readonly />
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-1 span-3 label">Diagn&oacute;stico:</div>
        <div class="span-5  append-3">
            <input type="text" id="txtDiagnosis" value="{$data.diagnosis_fle}" readonly />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-1 span-3 label">Causa:</div>
        <div class="span-5  append-3">
            <textarea style="height: 35px; width: 275px; font-family: 'Helvetica Neue',Arial,Helvetica,sans-serif; font-size: 13px;" readonly>{$data.cause_fle}</textarea>
        </div>

        <div class="span-4 label">Antecedentes m&eacute;dicos:</div>
        <div class="span-5 append-2 last">
            <textarea id="txtMedicalBackground" style="height: 35px; width: 275px; font-family: 'Helvetica Neue',Arial,Helvetica,sans-serif; font-size: 13px;" readonly>{$data.medical_background_cus}</textarea>
        </div>
    </div>
		
	{if $other_files}
	<div class="span-24 last line separator">
		<label>Historial de Asistencias</label>
	</div>
	
	<div class="prepend-2 span-18 append-2 last line">
		<center>
			<label>El cliente tiene otros expedientes en las siguientes tarjetas:</label>
			{foreach name="otherCardsList" from=$other_files item="oc"}
				{if $smarty.foreach.otherCardsList.first}
					<a href="{$document_root}modules/assistance/fEditAssistance/0/0/{$oc.card_number}" target="_blank">{$oc.card_number}</a>
				{else}
				-&nbsp<a href="{$document_root}modules/assistance/fEditAssistance/0/0/{$oc.card_number}" target="_blank">{$oc.card_number}</a>
				{/if}
			{/foreach}
		</center>	
	</div>
	{/if}
	
	<!--Blog-->
    <div class="span-24 last line separator">
		<label>Bit&aacute;cora</label>
	</div>

	<div class="prepend-4 append-4 span-16 last line">
		<div class="span-16" id="blog_tabs">
			<ul>
				<li><a href="#tabs-1">Cliente</a></li>
				<li><a href="#tabs-2">Operador</a></li>
			</ul>
			<div id="tabs-1" class="blog">
				{$blog.customer|strip_tags:true}
			</div>
			<div id="tabs-2" class="blog">
				{$blog.operator|strip_tags:true}
			</div>
			
		</div>
	</div>	

    <!--Checkup Information-->
    {if $data.medicalCheckupList}
	<div class="span-24 last line">
		<div class="prepend-10 span-5 last"><a id="showMedicalCheckup" style="cursor: pointer;">Ver datos de auditor&iacute;a m&eacute;dica</a></div>
		<input type="hidden" id="countMedicalCheckup" value="{$data.medicalCheckupList|@count}" />
	</div>
	<div id="medicalCheckupDialog">
		<center>
			<div class="prepend-1 span-15 last line">
				<table border="0" class="paginate-1 max-pages-7" id="medical_checkup">
					<thead>
						<tr bgcolor="#284787">
							{foreach name="medicalCheckupTitles" from=$data.medicalCheckupTitles item="t"}
								<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
									{$t}
								</td>
							{/foreach}
						</tr>
					</thead>

					<tbody>
					{foreach name="medicalCheckup" from=$data.medicalCheckupList item="ml"}
					<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e8f2fb">
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$smarty.foreach.medicalCheckup.iteration}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$ml.request_date_prq}
						</td>
						<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							{$ml.audited_date_prq}
						</td>
					</tr>
					<tr bgcolor="#284787">
						<td colspan="3" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
							Comentarios del auditor
						</td>
					</tr>
					<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e8f2fb">
						<td colspan="3" style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; overflow-y:scroll;">
							<textarea readonly style="height: 300px; width: 580px; background: none;">{$ml.auditor_comments}</textarea>
						</td>
					</tr>
					{if $ml.file_url_prq}
					<tr style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e8f2fb">
						<td colspan="3" style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
							<a href="{$document_root}{$ml.file_url_prq}" target="_blank">Ver archivo subido</a>
						</td>
					</tr>
					{/if}
					{/foreach}
					</tbody>
				</table>
			</div>
			<div class="prepend-1 span-15 last line pageNavPosition" id="pageNavPosition"></div>
		</center>
	</div>
    {/if}
	{if $pendingDocs}
	<!--Pending Documents-->

    <div class="span-24 last line separator">
        <label>Documentos a recibir:</label>
    </div>
	<div class="prepend-5 span-14 last line" id="registeredPendingDocuments">
		<table id="docsTable">
			<thead>
				<tr class="header">
					<th style="text-align: center;">
						Validar
					</th>
					<th style="text-align: center;">
						Documento
					</th>
					<th style="text-align: center;">
						Recibir De
					</th>
					<th style="text-align: center;">
						Nombre
					</th>
				</tr>
			</thead>
			<tbody>
				{foreach name="docs" from=$pendingDocs item="dc"}
					<tr {if $smarty.foreach.docs.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
						<td style="text-align: center;">
							<input type="checkbox" id="chkPending" name="chkPending" value="{$dc.serial_pdf}" {if $dc.status_pdf eq 'RECEIVED'}checked="checked" disabled="disabled"{/if}/>
						</td>
						<td style="text-align: center;">
							{$dc.name_pdf}
						</td>
						<td style="text-align: center;">
							{if $dc.pendingFrom eq 'CLIENT'}Cliente{else} Proveedor{/if}
						</td>
						<td style="text-align: center;">
							{$dc.namePendingFrom}
						</td>
					</tr>			
				{/foreach}
			</tbody>
		</table>
	</div>
	<div class="span-24 last buttons line">
		<input type="button" name="registerPendingDocs" id="registerPendingDocs" value="Registrar" />
	</div>
	{/if}
    <!--Service Providers-->

    <div class="span-24 last line separator">
        <label>Datos de cobertura:</label>
    </div>

    <div class="span-24 last line" id="benefitsMessages" style="display: none;">
        <div class="prepend-8 span-8 append-8 last">
            <div class="span-8" align="center" id="benefitsInnerMessage"></div>
        </div>
    </div>

    <div class="span-22 last line" id="uploadMessage" style="display: none;">
        <div class="prepend-7 span-8 append-7 last">
        <div class="span-8" align="center" id="uploadInnerMessage"></div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-6 span-8 append-10">
            <div class="prepend-2 span-3">
                <label>Coordinador:</label>
            </div>
            <div class="span-2">
                <select name="selProviders" id="selProviders">
                    <option value="">- Seleccione -</option>
                    {foreach from=$data.providerList item="pl"}
                    <option value="{$pl.serial_spv}" service_type="{$pl.type_spv}">{$pl.name_spv}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="prepend-11 span-1 last line specifications">
            <a style="cursor: pointer" id="displayProviderInfo">Informaci&oacute;n</a>
        </div>
    </div>

    <div class="span-24 last line" id="medicalType" style="display: none;">
        <div class="prepend-7 span-4">
            <label>Coordinaci&oacute;n m&eacute;dica:</label>
        </div>
        <div class="span-2">
            <select name="selMedicalType" id="selMedicalType" {if $acces_permition eq "view"}disabled{/if} >
                <option value="">- Seleccione -</option>
                {foreach from=$data.medicalTypes item="l"}
                    <option value="{$l}">{$global_medicalType.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="assistanceCost" style="display: none;">
        <div class="prepend-7 span-4">
            <label>Costo de Asistencia:</label>
        </div>
        <div class="span-2">
            <span id="txtAssistanceCost"></span>
        </div>
    </div>

    <div class="prepend-1 span-22 append-1 last line" id="benefitsContainer">
        <center>Seleccione un coordinador.</center>
    </div>

    <!--File Upload-->
	<div class="span-24 last line separator">
		<label>Reclamaciones:</label>
	</div>

	<div class="span-24 last line" id="liquidationMessage" style="display: none;">
		<div class="prepend-7 span-8 append-7 last">
		<div class="span-8" align="center" id="liquidationInnerMessage"></div>
		</div>
	</div>

    <div id="uploadFiles" class="span-24 last line">
		<center>Seleccione un coordinador.</center>
	</div>
    
</form>

<div class="span-24 last buttons line">
    <input type="button" name="btnCloseAssistance" id="btnCloseAssistance" value="Aceptar" />
</div>

<!--PROVIDERS INFO DIALOG-->
<div id="providersInfoDialog">
    <div class="span-18 last line separator">
        <label>Informaci&oacute;n de operadores:</label>
    </div>
    <div class="span-18 last line" id="providersInfoContainer"></div>
</div>
<!--END PROVIDERS INFO DIALOG-->

<!--UPLOAD FILE DIALOG-->
<div id="uploading_file">
    <div class="prepend-1 span-2" style="margin-top: 27px;">
        <img src="{$document_root}img/ajax-loader.gif"/>
    </div>
    <div class="span-3" style="margin-top: 20px;">
        <span id="messageText">Subiendo archivo</span>
        Espere por favor.
    </div>
</div>
<!--END UPLOAD FILE DIALOG-->

<!--FILE BENEFITS DIALOG-->
<div id="fileBenefitsDialog">
    <div class="span-17 last line separator">
        <label>Selecci&oacute;n de beneficios:</label>
    </div>
    <div class="span-17 last line" id="benefitsUploadContainer"></div>
</div>
<!--END FILE BENEFITS DIALOG-->

<!--FILE BENEFITS DIALOG-->
<div id="fileViewBenefitsDialog">
    <div class="span-17 last line separator">
        <label>Beneficios asignados:</label>
    </div>
    <div class="span-17 last line" id="viewBenefitsUploadContainer"></div>
</div>
<!--END FILE BENEFITS DIALOG-->
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 center error last">
		Hubo errores al cargar la informaci&oacute;n de su tarjeta. Por favor vuelva a intentarlo.
	</div>
</div>
{/if}
