{assign var="title" value="CARGA DE DOCUMENTOS - RED DE ASISTENCIA"}
<div class="span-24 last title ">
	CARGA DE DOCUMENTOS - RED DE ASISTENCIA<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	<div class="span-24 last line">
		<div class="append-5 span-14 prepend-5 last">
			<div class="span-14 {if $error neq 1}error{else}success{/if}" align="center">
				{if $error eq 1}
					El proceso termin&oacute; exitosamente.
				{else}
					Hubo errores en la operaci&oacute;n. Por favor vuelva a intentarlo.
				{/if}
			</div>
		</div>
	</div>
{/if}

<div class="span-7 span-10 prepend-8 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
	<div class="prepend-6 span-5 label">
		<label>* Proveedor:</label>
	</div>
	<div class="span-5 last">
		<select id="provider" class="span-5">
			<option value="">- Seleccione -</option>
			{foreach from=$spvs item="provider"}
			<option value="{$provider.serial_spv}">{$provider.name_spv}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-24 last line buttons">
	<input type="button" name="btnSearch" id="btnSearch" value="Validar" />
</div>

<div class="span-24 last line" id="fileTableContainer"></div>