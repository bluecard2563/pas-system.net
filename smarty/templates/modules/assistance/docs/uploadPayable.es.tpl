{assign var="title" value="CARGA DE DOCUMENTOS"}
<script src="{$document_root}js/modules/assistance/globalLiquidationFunctions.js" type="text/javascript"></script>
<div class="span-24 last title ">
	CARGA DE DOCUMENTOS<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
	<div class="append-7 span-10 prepend-7 last">
	   <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
			   Registro cargado exitosamente!
			{elseif $error eq 2}
			   Proceso no pudo iniciar ya que no se seleccion&oacute; ning&uacute;n beneficio.
			{elseif $error eq 3}
			   El Registro de pago no pudo ser creado. El proceso fue interrumpido.
			{elseif $error eq 4}
			   El proceso termin&oacute; con &eacute;xito. Sin embargo el archivo no pudo ser cargado.
			{elseif $error eq 5}
			   El proceso no puede continuar porque los beneficios no pudieron ser almacenados.
			{elseif $error eq 6}
				El proceso no puede continuar porque los el registro DBF no fue ingresado. Comun&iacute;quese con el administrador.
			{/if}
	   </div>
   </div>
{/if}

<div class="prepend-7 span-9 append-5 last">
    <ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line separator">
	<label>Datos del Expediente</label>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">Nro. de Tarjeta:</div>
	<div class="span-5">
		{$customData.card_number}
	</div>

	<div class="span-3 label">Nro. Expediente:</div>
	<div class="span-4 append-4 last">
		{$customData.serial_fle}
	</div>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-4 label">Asegurado:</div>
	<div class="span-5">
		{$customData.pax|htmlall}
	</div>

	<div class="span-3 label">DiagnÃ³stico:</div>
	<div class="span-7 append-1 last">
		<input type="text" name="txtDiagnosis" class="span-7" id="txtDiagnosis" title="El campo 'Nro. Documento' es obligatorio" value="{$customData.diagnosis}" />
	</div>
</div>
	
<div class="span-24 last line buttons">
	<input type="button" id="updateDiagnosis" name="updateDiagnosis" value="Actualizar Diagn&oacute;stico" />
</div>

<div class="span-24 last line separator">
	<label>Nuevo Documento</label>
</div>

<form name="frmUploadPayable" id="frmUploadPayable" method="post" action="{$document_root}modules/assistance/docs/pUploadPayable" enctype="multipart/form-data">
	<div class="span-24 last line">
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Documento:</div>
			<div class="span-5">
				<input type="text" name="txtFileName" id="txtFileName" title="El campo 'Documento' es obligatorio" />
			</div>

			<div class="span-3 label">* Nro. Documento:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtFileDescription" id="txtFileDescription" title="El campo 'Nro. Documento' es obligatorio" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Monto Presentado:</div>
			<div class="span-5">
				<input type="text" name="txtTotalPresentedAmount" id="txtTotalPresentedAmount" value="0.00" title="El campo 'Monto Presentado' es obligatorio" readonly="readonly" />
			</div>

			<div class="span-3 label">* Monto Aprobado/Negado:</div>
			<div class="span-4 append-4 last">
				<input type="text" name="txtTotalAuthorizadAmount" id="txtTotalAuthorizadAmount" value="0.00" title="El campo 'Monto Aprobado' es obligatorio" readonly="readonly" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Pago a:</div>
			<div class="span-5">
				<select name="selBillTo" id="selBillTo" title="El campo 'Pago a' es obligatorio">
					<option value="">- Seleccione -</option>
					<option value="CLIENT">Cliente</option>
					<option value="PROVIDER">Coordinador</option>
					<option value="DIRECT">Directo</option>
					<option value="NONE">No Aplica</option>
				</select>
			</div>

			<div class="span-3 label">* Fecha del Documento:</div>
			<div class="span-4 append-4 last">
				<input type="text"  name="txtDocumentDate" id="txtDocumentDate" title="El campo 'Fecha del Documento' es obligatorio" />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Estado:</div>
			<div class="span-5">
				<select name="selStatusPrq" id="selStatusPrq" title="El campo 'Estado' es obligatorio.">
					<option value="">- Seleccione -</option>
					<option value="STAND-BY">Pendiente</option>
					<option value="APROVED">Aprobado</option>
					<option value="DENIED">Negado</option>
				</select>

			</div>

			<div class="span-3 label">* Beneficios:</div>
			<div class="span-4 append-4 last">
				<input type="hidden" name="filesToUpload" id="filesToUpload" />
				<input type="hidden" name="authorized_amounts" id="authorized_amounts" />
				<input type="hidden" name="presented_amounts" id="presented_amounts" />
				<a id="benefitsAssign" style="cursor: pointer">Asignar</a>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Observaciones:</div>
			<div class="span-5">
				<input type="text" name="txtObservations" id="txtObservations" title="El campo 'Observaciones' es obligatorio" />
			</div>

			<div class="span-3 label">* Tipo de Archivos:</div>
			<div class="span-4 append-4 last">
				<select name="selTypePrq" id="selTypePrq" title="El campo 'Tipo de Archivo' es obligatorio">
					<option value="">- Seleccione -</option>
					{foreach from=$types_prq item="name" key="key"}
						<option value="{$key}">{$name}</option>
					{/foreach}
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Archivo:</div>
			<div class="span-5">
				<input type="file" id="fileAddress" name="fileAddress" title="El campo 'Archivo' es obligatorio" />
			</div>
		</div>

		<div class="span-24 buttons">
			<input id="uploadButton" type="submit" value="Procesar Documento"/>
			<input type="hidden" name="hdnSerial_spv" id="hdnSerial_spv" value="{$serial_spv}" />
			<input type="hidden" name="hdnToday" id="hdnToday" value="{$data.date}" />
			<input type="hidden" name="hdnSerial_fle" id="hdnSerial_fle" value="{$customData.serial_fle}" />
			<input type="hidden" name="hdnAccesPermition" id="hdnAccesPermition" value="{$acces_permition}"/>
		</div>
	</div>



	{if $data.fileList}
		<div class="span-24 last line separator">
			<label>Documentos Actuales</label>
		</div>

		<div class="span-24 last line">
			<table border="1" class="dataTable" id="files_table">
				<thead>
					<tr class="header">
						{foreach name="titles_dialog" from=$uploadedFilesTitles item="t"}
							<th align="center">
								{$t}
							</th>
						{/foreach}
					</tr>
				</thead>
				<tbody>
					{if $data.fileList}
						{foreach name="files" from=$data.fileList item="fl"}
							<tr {if $smarty.foreach.files.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
								<td align="center" class="tableField">
									{$fl.name_dbf|htmlall}
								</td>
								<td align="center" class="tableField">
									{$fl.comment_dbf|htmlall}
								</td>
								<td id="reclaimedContainer_{$fl.serial_prq}" align="center" class="tableField">
									{$fl.amount_reclaimed_prq}
								</td>
								<td id="authorizedContainer_{$fl.serial_prq}" align="center" class="tableField">
									{$fl.amount_authorized_prq}
								</td>
								<td align="center" class="tableField">
									{$global_billTo[$fl.document_to_dbf]}
								</td>
								<td align="center" class="tableField">
									{$fl.document_date_dbf}
								</td>
								<td align="center" class="tableField" id="changeStatusPrqContainer{$fl.serial_prq}">
									{if $fl.status_prq eq "STAND-BY"}
										<select name="changeStatusPrq" id="changeStatusPrq{$fl.serial_prq}" serial_prq="{$fl.serial_prq}">
											<option value="STAND-BY" selected>Pendiente</option>
											<option value="APROVED">Aprobado</option>
											<option value="DENIED">Negado</option>
										</select>
									{else}
										{if $fl.status_prq eq "DENIED"}<font color="red">{/if}
										{$global_paymentRequestStatus[$fl.status_prq]}
										{if $fl.status_prq eq "DENIED"}</font>{/if}
									{/if}
								</td>
								<td align="center" class="tableField">
									{if $fl.status_prq eq "STAND-BY"}
										<input type="hidden" name="authorized_amounts_{$fl.serial_prq}" id="authorized_amounts_{$fl.serial_prq}" />
										<input type="hidden" name="presented_amounts_{$fl.serial_prq}" id="presented_amounts_{$fl.serial_prq}" />
										<a id="editDocument{$fl.serial_prq}" style="cursor: pointer" serial_dbf="{$fl.serial_dbf}" serial_prq="{$fl.serial_prq}">Ver</a>
									{else}
										<a id="viewDocument{$fl.serial_dbf}" style="cursor: pointer" serial_dbf="{$fl.serial_dbf}">Ver</a>
									{/if}
								</td>
								<td align="center" class="tableField">
									{$fl.observations_prq|htmlall}
								</td>
								<td align="center" class="tableField">
									{$fl.type_prq}
								</td>
								<td align="center" class="tableField">
									<a href="{$document_root}{$fl.url_dbf}" target="blank">Abrir</a>
								</td>
							</tr>
						{/foreach}
					{/if}
				</tbody>
			</table>
		</div>
	{/if}

	<!--FILE BENEFITS DIALOG-->
	<div id="fileBenefitsDialog">
		<div class="span-17 last line separator">
			<label>Selecci&oacute;n de beneficios:</label>
		</div>
		<div class="span-17 last line" id="benefitsUploadContainer">
			{if $benefitsByServiceProvider}
				<div class="span-17 last line">
					<table border="0" id="benefits_upload_table">
						<thead>
							<tr bgcolor="#284787">
								<td align="center" style="border: solid 1px white; padding:0px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">
									<input type="checkbox" name="selAll" id="selAll" />
								</td>
								{foreach from=$titles item="l"}
									<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
										{$l}
									</td>
								{/foreach}
							</tr>
						</thead>

						<tbody>
							{foreach name="benefitList" from=$benefitsByServiceProvider item="l"}
								<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.benefitList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										<input type="checkbox" id="chkBenUp{$l.serial_spf}" name="serial_bxp[]" value="{$l.serial_spf}"  title="Debe seleccionar al menos un beneficio." />
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										{$l.description_bbl}
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										{if $l.price_bxp neq 'N/A'}${/if}{$l.price_bxp}
										<input type="hidden" style="width: 50px;" name="maxAmount{$l.serial_spf}" id="maxAmount{$l.serial_spf}" value="{$l.price_bxp}" readonly/>
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										{$l.restriction_price_bxp}
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										<input type="text" style="width: 50px;" name="txtPresentedAmount{$l.serial_spf}" id="txtPresentedAmount{$l.serial_spf}" serial="{$l.serial_spf}" serial_bxp="{$l.serial_bxp}" disabled />
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										<input type="text" style="width: 50px;" name="txtAuthorizadAmount{$l.serial_spf}" id="txtAuthorizadAmount{$l.serial_spf}" serial="{$l.serial_spf}" serial_bxp="{$l.serial_bxp}" disabled />
									</td>
								</tr>
							{/foreach}
							{foreach name="serviceList" from=$servicesByServiceProvider item="l"}
								<tr  style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.serviceList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										<input type="checkbox" id="chkBenUp{$l.serial_spf}" name="serial_bxp[]" value="{$l.serial_spf}"  title="Debe seleccionar al menos un beneficio." />
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										{$l.name_sbl}
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										${$l.coverage_sbc}
										<input type="hidden" style="width: 50px;" name="maxAmount{$l.serial_spf}" id="maxAmount{$l.serial_spf}" value="{$l.coverage_sbc}" readonly/>
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										-
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										<input type="text" style="width: 50px;" name="txtPresentedAmount{$l.serial_spf}" id="txtPresentedAmount{$l.serial_spf}" serial="{$l.serial_spf}" disabled />
									</td>
									<td style="border: solid 1px white;  padding:5px 5px 5px 5px; text-align:center;">
										<input type="text" style="width: 50px;" name="txtAuthorizadAmount{$l.serial_spf}" id="txtAuthorizadAmount{$l.serial_spf}" serial="{$l.serial_spf}" disabled />
									</td>
								</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
				<div class="span-17 last line pageNavPosition" id="pageNavPosition"></div>
			{else}
				No existen beneficios disponibles para el coordinador seleccionado.
			{/if}
		</div>
	</div>
	<!--END FILE BENEFITS DIALOG-->

	<!--FILE BENEFITS DIALOG-->
	<div id="fileViewBenefitsDialog">
		<div class="span-17 last line separator">
			<label>Beneficios asignados:</label>
		</div>
		<div class="span-17 last line" id="viewBenefitsUploadContainer"></div>
	</div>
	<!--END FILE BENEFITS DIALOG-->
</form>