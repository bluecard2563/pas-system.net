{assign var="title" value="ACTUALIZAR CIUDAD"}

<form name="frmUpdateCity" id="frmUpdateCity" method="post" action="{$document_root}modules/city/pUpdateCity" class="form_smarty">
	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Ciudad<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
    {if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 error" align="center">
            {if $error eq 1}
                Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}
	   
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-5">
            <label>*C&oacute;digo ciudad:</label>
        </div>
        <div class="span-8 last">
            <input type="text" name="txtCodeCity" id="txtCodeCity" value="{$data.code_cit}" title='El campo "C&oacute;digo" es obligatorio'>
        </div>
    </div>
              
    <div class="span-24 last line">
        <div class="prepend-8 span-5 append-0">
            <label>*Nombre de la ciudad:</label>
        </div>
        
        <div class="span-10 last">
            <div class="">
                <input type="text" name="txtNameCity" id="txtNameCity" value="{$data.name_cit}" title='El campo "Nombre Ciudad" es obligatorio.' >
            </div>
        </div>    
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_cit" id="hdnSerial_cit" {if $data.serial_cit}value="{$data.serial_cit}"{/if} >
        <input type="hidden" name="selCountry" id="selCountry" {if $data.serial_cou}value="{$data.serial_cou}"{/if} >
    </div>
</form>