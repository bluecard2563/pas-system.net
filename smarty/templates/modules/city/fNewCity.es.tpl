{assign var="title" value="NUEVA CIUDAD"}
<form id="frmNewCity" name="frmNewCity" method="post" action="{$document_root}modules/city/pNewCity" class="form_smarty">	
	<div class="span-24 last title ">
    	Registro de Nueva Ciudad<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    		{if $error eq 1}
            	La ciudad se ha registrado exitosamente!
        	{else}
            	No se pudo insertar.
        	{/if}
    		</div>
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-8 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
	<div class="span-24 last line"> 
        <div class="prepend-8 span-5">        
            <label>*Seleccione la zona:</label>
        </div>
        <div class="span-8 last">     	                                  
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
              <option value="">- Seleccione -</option>
              {foreach from=$nameZoneList item="l"}
              <option value="{$l.serial_zon}" {if $data.serial_zon eq $l.serial_zon}selected="selected"{/if} >{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>
	</div>
    
    <div class="span-24 last line"> 
        <div class="prepend-8 span-5">
            <label>*Seleccione el pa&iacute;s:</label>
        </div>
        <div class="span-8 last" id="countryContainer">     	                                  
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
              <option value="">- Seleccione -</option>
            </select>
            </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-5">
            <label>*C&oacute;digo ciudad:</label>
        </div>
        <div class="span-8 last">
            <input type="text" name="txtCodeCity" id="txtCodeCity" title='El campo "C&oacute;digo" es obligatorio'>
        </div>
    </div>

    <div class="span-24 last line"> 
        <div class="prepend-8 span-5">
            <label>*Nombre ciudad:</label>
        </div>
        <div class="span-8 last">
           <input type="text" name="txtNameCity" id="txtNameCity" title='El campo "Nombre" es obligatorio'>
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" class=""  />
    </div>
    <input type="hidden" name="hdnFlagCity" id="hdnFlagCity" value="1" />
</form>