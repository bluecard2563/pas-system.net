{assign var="title" value="BUSCAR CIUDAD"}
<form name="frmSearchCity" id="frmSearchCity" method="post" action="{$document_root}modules/city/fUpdateCity" class="form_smarty">
	<div class="span-24 last title ">
    	Buscar Ciudad<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	
    
    {if $error}
    <div class="span-24 last line">  
        <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
                {if $error eq 1}
                    La ciudad se actualiz&oacute; correctamente!
                {else}
                	No existe la ciudad ingresada. Por favor seleccione una de la lista.      
                {/if}
            </div>
        </div>
    </div>
	{/if}         
                

    <div class="span-7 span-10 prepend-7 last">
    	<ul id="alerts" class="alerts"></ul>
    </div>              
    
    <div class="prepend-3 span-19 append-2 last line"></div>    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                    {/foreach}
            </select>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
        </select>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-4 span-4 label">* Ciudad:</div>
        <div class="span-5" id="cityContainer">
        <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio'>
            	<option value="">- Seleccione -</option>
        </select>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line"></div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Editar" >
    </div>
</form>