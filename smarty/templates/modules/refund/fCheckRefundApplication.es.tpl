{assign var="title" value="AUTORIZAR REEMBOLSO"}
<div class="span-24 last title">
	 Aprobaciones de Reembolsos<br />
	<label>(*) Campos Obligatorios</label>
</div>

<form name="frmCheckApplication" id="frmCheckApplication" action="{$document_root}modules/refund/pCheckRefundApplication" method="post" >
	<div class="prepend-8 span-8 append-8 line">
		<div class="span-8 tableTitles">
		{if $refund.type_ref eq 'REGULAR'}
			Reembolso Regular
		{else}
			Reembolso por penalidad
		{/if}
		</div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-6 span-5 label">* Estado:</div>
        <div class="append-5 span-7 last">
			<select name="selStatus" id="selStatus" title="El campo 'Estado' es obligatorio." >
				<option value="">- Seleccione -</option>
				<option value="APROVED">Aprobado</option>
				<option value="DENIED">Negado</option>
			</select>
        </div>
	</div>

	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line separator">
		<label>Datos de la Venta</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Cliente:</div>
        <div class="span-5">
            {$customer.first_name_cus} {$customer.last_name_cus}
        </div>

        <div class="span-3 label">Producto:</div>
        <div class="span-4 append-4 last">
            {$sale.aux.product|htmlall}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Comercializador:</div>
        <div class="span-5">
            {$dealer.dealer_name|htmlall}
        </div>

        <div class="span-3 label">Sucursal de Comercializador:</div>
        <div class="span-4 append-4 last">
            {$dealer.branch_name|htmlall}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Total de la Venta (1):</div>
        <div class="span-5">
            USD {$sale.total_sal}
			<input type="hidden" name="hdnTotalSale" id="hdnTotalSale" value="{$sale.total_sal}" />
        </div>

		<div class="span-3 label">Impuestos Aplicados (2):</div>
        <div class="span-4 append-4 last">
			{if $invoice.applied_taxes_inv}
			<table id="taxTable">
				<tr>
					<td class="tableTitles"><b>Impuesto</b></td>
					<td class="tableTitles"><b>Valor %</b></td>
				</tr>
				{foreach from=$invoice.applied_taxes_inv item="l"}
				<tr class="{cycle values='even-row,odd-row'}">
					<td class="refundTable">{$l.tax_name}</td>
					<td class="refundTable">{$l.tax_percentage}</td>
				</tr>
				{/foreach}
			</table>
			{else}
				No existen impuestos aplicados.
			{/if}
			<input type="hidden" name="hdnTaxes" id="hdnTaxes" value="{$taxes}"/>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Descuentos Aplicados (3):</div>
        <div class="span-5">
            % {if $invoice.discount_prcg_inv}{$invoice.discount_prcg_inv}{else}0{/if}
			<input type="hidden" name="hdnDiscount" id="hdnDiscount" value="{if $invoice.discount_prcg_inv}{$invoice.discount_prcg_inv}{else}0{/if}" />
        </div>

		<div class="span-3 label">Otros Descuentos (4):</div>
        <div class="span-4 append-4 last">
            % {if $invoice.other_dscnt_inv}{$invoice.other_dscnt_inv}{else}0{/if}
			<input type="hidden" name="hdnOtherDiscount" id="hdnOtherDiscount" value="{if $invoice.other_dscnt_inv}{$invoice.other_dscnt_inv}{else}0{/if}" />
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Valor Final de la Venta (1-3-4)*(2):</div>
        <div class="span-5">
            USD {$totalRefund}
			<input type="hidden" name="hdnTotalRefund" id="hdnTotalRefund" value="{$totalRefund}" />
        </div>
	</div>

	<div class="span-24 last line separator">
		<label>Datos de Cobertura</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Desde:</div>
        <div class="span-5">
            {$sale.begin_date_sal}
        </div>

        <div class="span-3 label">Hasta:</div>
        <div class="span-4 append-4 last">
            {$sale.end_date_sal}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Fecha de Emisi&oacute;n de la Factura:</div>
        <div class="span-5">
            {$invoice.date_inv}
        </div>

		{if $refundType eq 'REGULAR'}
        <div class="span-3 label">Fecha del Pago:</div>
        <div class="span-4 append-4 last">
            {$payment.date_pay}
        </div>
		{/if}
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Fecha de Emisi&oacute;n de la Venta:</div>
        <div class="span-5">
            {$sale.emission_date_sal}
        </div>

        <div class="span-3 label">Fecha de Ingreso al Sistema</div>
        <div class="span-4 append-4 last">
            {$sale.in_date_sal}
        </div>
	</div>

	<div class="span-24 last line separator">
		<label>Datos de la Solicitud</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Motivo del Reembolso:</div>
        <div class="span-5">
			{$global_refundReasons[$refund.reason_ref]}
			<input type="hidden" id="selRefundReason" name="selRefundReason" value="{$refund.reason_ref}" />
        </div>

        <div class="span-3 label">* Documentos Requeridos:</div>
        <div class="span-7 append-1 last">
			{foreach from=$refund.document_ref item="d"}
				{$d} <br>
			{/foreach}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Tipo de Retenci&oacute;n:</div>
        <div class="span-5">
			<select id="selPenaltyType" name="selPenaltyType" title="El campo 'Tipo de retenci&oacute;n' es obligatorio." >
				<option value="">- Seleccione -</option>
				{foreach from=$refundData.retainedType item="l"}
				<option value="{$l}" {if $l eq $refund.retained_type_ref}selected{/if}>{$global_refundRetainedTypes.$l}</option>
				{/foreach}
			</select>
        </div>

        <div class="span-3 label">* Valor Retenido:</div>
        <div class="span-7 append-1 last">
			<input type="text" id="txtPenaltyFee" name="txtPenaltyFee" title="El 'Valor Retenido' es obligatorio." value="{$refund.retained_value_ref}"/>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Comentarios:</div>
        <div class="span-5">
            {$refund.aux.applicant}: {$refund.comments_ref}
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Observaciones:</div>
        <div class="span-5">
            <textarea name="txtObservations" id="observations" title="El campo 'Observaciones' es obligatorio y debe tener un minimo de 15 caracteres"></textarea>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-10 append-10 center" style="font-style: italic;">
			{if $paiedAmounts || $paidBonus}
				<div class="span-10 last line center">
					Valores Cancelados en esta venta:<br>
					<table id="tblDiscounts" name="tblDiscounts">
						<tr>
							<td class="tableTitles"><b>Concepto</b></td>
							<td class="tableTitles"><b>Valor</b></td>
						</tr>
						{if $comission}
						<tr class="{cycle values='even-row,odd-row'}">
							<td class="refundTable">Comisi&oacute;n: </td>
							<td class="refundTable">
								$ {$comission.value_dea_com}
								<input type="hidden" value="{$comission.value_dea_com}" id="discount_comission" name="discount_comission" />
							</td>
						</tr>
						{/if}
						{if $bonus}
						<tr class="{cycle values='even-row,odd-row'}">
							<td class="refundTable">Incentivos: </td>
							<td class="refundTable">
								$ {$bonus.total_amount_apb}
								<input type="hidden" value="{$bonus.total_amount_apb}" id="discount_bonus" name="discount_bonus"/>
							</td>
						</tr>
						{/if}
					</table>
				</div>
			
				<div class="span-10 last line center">
					<b>Desea que la comisi&oacute;n se descuente en: </b><br /> 
					Nota de Cr&eacute;dito <input type="radio" name="rdDiscountInCN" id="rdDiscountInCN_YES" value="YES" checked="checked" />
					Pr&oacute;xima Liquidaci&oacute;n <input type="radio" name="rdDiscountInCN" id="rdDiscountInCN_NO" value="NO" />
					
					<input type="hidden" name="paidAmounts" id="paidAmounts" value="1" />
					<input type="hidden" name="paidBonus" id="paidBonus" value="1" />
				</div>				
			{else}
				No se ha pagado ninguna comisi&oacute;n/incentivo/etc. para esta tarjeta.
			{/if}
		</div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-9 span-5 label">Valor estimado a Reembolsar:</div>
        <div class="append-3 span-7 last">
			<label id="lblTotalToPay">{$refund.total_to_pay_ref}</label>
			<input type="hidden" id="total_to_pay" name="total_to_pay" value="{$refund.total_to_pay_ref}"/>
        </div>
	</div>

	<div class="span-24 last line buttons">
		<input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar"/>
		<input type="hidden" name="hdnRefundType" id="hdnRefundType" value="{$refund.type_ref}"/>
		<input type="hidden" name="hdnDirectSale" id="hdnDirectSale" value="{$sale.direct_sale_sal}"/>
		<input type="hidden" name="hdnSerial_sal" id="hdnSerial_sal" value="{$sale.serial_sal}"/>
		<input type="hidden" name="hdnSerial_ref" id="hdnSerial_ref" value="{$serial_ref}"/>
	</div>
</form>