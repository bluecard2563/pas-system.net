{assign var="title" value="SOLICITUDES DE REEMBOLSO"}
<div class="span-24 last title">
	Solicitudes Pendientes de Reembolso<br>
	  <label>(*) Campos Obligatorios</label>
</div>
{if $error}
<div class="append-7 span-10 prepend-7 last">
	<div class="span-10 {if $error eq 1 || $error eq 9}success{else}error{/if}" align="center">
		{if $error eq 1}
			Se ha actualizado el reembolso exitosamente!{if $error eq 9}<br /> Dir&iacute;jase a la interfaz del ERP para la impresi&oacute;n de la Nota de Cr&eacute;dito.{/if}
			{if $serial_cn}
				{if $serial_cn neq -1}
				<br /><b><a href="{$document_root}modules/creditNote/pPrintFirstCreditNote/{$serial_cn}" target="_blank">Imprimir Nota de C&eacute;dito No. {$number_cn}</a></b>
				{/if}
			{/if}
		{elseif $error eq 2}
			Hubo errores en la actualizaci&oacute;n del reembolso.
		{elseif $error eq 3}
			No existe un documento para notas de cr&eacute;dito en el pa&iacute;s en el que se encuentra. <br>
			Por favor comun&iacute;quese con el adminsitrador.
		{elseif $error eq 4}
			Hubo errores al registrar el nuevo n&uacute;mero de la nota de cr&eacute;dito creada.
		{elseif $error eq 5}
			Hubo errores al actualizar el n&uacute;mero de la nota de cre&eacute;dito.
		{elseif $error eq 6}
			La nota de cr&eacute;dito no se ha podido generar. Por favor comun&iacute;quese con el administrador.
		{elseif $error eq 7}
			La solicitud solicitada no existe o ya fue procesada.
		{elseif $error eq 8}
			No existe conexi&oacute;n con el ERP en este momento y no se puede continuar. Por favor vuelva a intentarlo m&aacute;s tarde.
		{/if}
	</div>
</div>
{/if}

{if !$not_allowed_user}
{if $refundApplications}
<div class="span-24 last line">
	<table border="0" id="tblRefundApplications" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					No. Tarjeta
				</td>
				<td align="center" class="tableTitle">
					Solicitante
				</td>
				<td align="center" class="tableTitle">
					Pa&iacute;s de la Venta
				</td>
				<td align="center"  class="tableTitle">
					Sucursal de Comercializador
				</td>
				<td align="center" class="tableTitle">
					Fecha de Solicitud
				</td>
				<td align="center" class="tableTitle">
					Revisar
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="alert" from=$refundApplications item="r"}
			<tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.alert.iteration}
				</td>
				<td align="center" class="tableField">
					{$r.card_number_sal}
				</td>
				<td align="center" class="tableField">
					{$r.applicant}
				</td>
				<td align="center" class="tableField">
					{$r.name_cou}
				</td>
				<td align="center" class="tableField">
					{$r.name_dea}
				</td>
				<td align="center" class="tableField">
					{$r.request_date_ref}
				</td>
				<td align="center" class="tableField">
					<a href="{$document_root}modules/refund/fCheckRefundApplication/{$r.serial_ref}">Revisar</a>
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last buttons" id="pageNavPosition"></div>
{else}
<div class="prepend-8 span-8 append-8 last">
	<div class="span-8 error last center">
		No existen solicitudes de reembolso pendientes.
	</div>
</div>
{/if}
{else}
<div class="prepend-8 span-8 append-8 last">
	<div class="span-8 error last center">
		Usted no est&aacute; autorizado a despachar solicitudes de reembolso. Para hacerlo, por favor contacte al responsable de esto en su pa&iacute;s.
	</div>
</div>
{/if}