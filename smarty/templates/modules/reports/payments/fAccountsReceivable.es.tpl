<!--/*
File: fAccountsReceivable.es.tpl
Author: Gabriela Guerrero
Creation Date:31/05/2010
Modified By:
Last Modified:
*/-->
{assign var="title" value="REPORTE DE CUENTAS POR COBRAR"}
<form target="_blank" name="frmAccountsReceivable" id="frmAccountsReceivable" method="post">
    <div class="span-24 last title">
    	Reporte de Cuentas por Cobrar
    </div>
	<div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
   
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-4">
            <select name="selZone" id="selZone">
                <option value="">- Seleccione -</option>
                {foreach from=$zonesList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon|htmlall}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-4 label">* Pa&iacute;s:</div>
        <div class="span-5 last" id="paisContainer">
            Seleccione una Zona
        </div>

    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Representante:</div>
        <div class="span-5" id="representanteContainer">
            Seleccione un Pa&iacute;s
        </div>
        <div class="span-3 label">Ciudad:</div>
        <div class="span-5 append-3 last" id="cityContainer">
            Seleccione un Pa&iacute;s
        </div>
    </div>

    <div class="span-24 last line">
		<div class="prepend-4 span-3 label">Responsable:</div>
		<div class="span-6" id="comissionistsContainer">
			Seleccione un representante
		</div>
        <div class="span-6 last">&nbsp;</div>
    </div>
    
    <div class="span-24 last line">
		<div class="prepend-4 span-3 label">Categoria:</div>
		<div class="span-4">
			<select name="selCategory" id="selCategory">
				<option value="">Todos</option>
				{if $categoryList}
					{foreach from=$categoryList item="l"}
						<option value="{$l}">{$l}</option>
					{/foreach}
				{/if}
			</select>
		</div>
        <div class="span-4 label">Comercializador:</div>
        <div class="span-4 last" id="comercializadorContainer">
            Seleccione un Responsable
        </div>
    </div>

    <div class="span-24 last line">
		<div class="prepend-4 span-3 label">D&iacute;as Hasta:</div>
		<div class="span-5">
            <select name="selDays" id="selDays">
				<option value="">Todos</option>
				<option value="1"> Hasta 30 </option>
				<option value="31"> Hasta 60 </option>
				<option value="61"> Hasta 90 </option>
                                <option value="91"> M&aacute;s de 90 </option>
			 </select>
        </div>
        <div class="span-3 label">Sucursal:</div>
        <div class="span-5 last" id="sucursalContainer">
            Seleccione un Comercializador
        </div>
    </div>

	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">Fecha Desde:</div>
		<div class="span-5">
            <input type="text" name="txtDateFrom" id="txtDateFrom" title='El campo "Fecha Desde" es obligatorio'/>
        </div>
		<div class="span-3 label">Fecha Hasta:</div>
		<div class="span-5 last">
            <input type="text" name="txtDateTo" id="txtDateTo" title='El campo "Fecha Hasta" es obligatorio'/>
        </div>
	</div>


    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS" class="XLS"/>
        </div>
    </div>
    <div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>
</form>