<!--/*
File: fPayments.es.tpl
Author: Gabriela Guerrero
Creation Date:08/06/2010
Modified By:
Last Modified:
*/-->
{assign var="title" value="REPORTE DE PAGOS"}
    <div class="span-24 last title">
    	Reporte de Pagos
    </div>
    
	{if $error}
	<div class="append-7 span-10 prepend-7 last line">
		<div class="span-10 last error">
			Hubo errores al generar el reporte. Por favor Vuelva a intentarlo.
        </div>
    </div>
	{/if}

	{if not $countriesList}
	<div class="append-7 span-10 prepend-7 last line">
		<div class="span-12 last error" id="messageZone" align="center">
            No se encontraron registros.
        </div>
	</div>
	{else}

    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line">
		<div class="prepend-9 span-4 last">
            <label for="radio_general">Filtros Generales:</label>
            <input type="radio" id="radioGeneral" name="radioChoice" value="general" checked/>
        </div>
		<div class="span-4 last line">
            <label for="radio_invoice"># Factura:</label>
            <input type="radio" id="radioInvoice" name="radioChoice" value="invoice"/>
		</div>		
	</div>

	<form target="_blank" name="frmPaymentsInvoice" id="frmPaymentsInvoice" method="post">
	<div id="invoiceContainer" style="display: none;">
		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">* Pa&iacute;s:</div>
			<div class="span-4 last">
				<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s es obligatorio.'">
					<option value="">- Seleccione -</option>
					{foreach from=$countriesList item="l"}
						<option value="{$l.serial_cou}">{$l.name_cou}</option>
					{/foreach}
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">* Representante:</div>
			<div class="span-4 last" id="representanteContainer">
				<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">Tipo de Persona:</div>
			<div class="span-5 last">
				<input type="radio" value="PERSON" id="rdType_1" name="rdType" checked/>Natural <br>
				<input type="radio" value="LEGAL_ENTITY" id="rdType_2" name="rdType"/>Jur&iacute;dica
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-8 span-4 label">* N&uacute;mero de Factura:</div>
			<div class="span-4 last">
				<input type="text" class="span-4" name="txtInvoice" id="txtInvoice" title="El campo 'Num. Factura' es obligatorio." />
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-11 span-5">
				Generar reporte:
				<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" onclick="checkPaymentsInvoice(1);"/>
				<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  onclick="checkPaymentsInvoice(2);"/>
			</div>
		</div>

	</div>
</form>

<form target="_blank" name="frmPayments" id="frmPayments" method="post">
	<div id="generalContainer">
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Pa&iacute;s:</div>
			<div class="span-4">
				<select name="selCountry" id="selCountry">
					<option value="">Todos</option>
					{foreach from=$countriesList item="l"}
						<option value="{$l.serial_cou}">{$l.name_cou}</option>
					{/foreach}
				</select>
			</div>
			<div class="span-4 label">Representante:</div>
			<div class="span-4 last" id="representanteContainer">
				Seleccione un Pa&iacute;s
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Responsable:</div>
			<div class="span-4" id="comissionistContainer">
				Seleccione un Representante
			</div>
			
			<div class="span-4 label">Ciudad:</div>
			<div class="span-4 last" id="cityContainer">
				Seleccione un Pa&iacute;s
			</div>
		</div>
		
		<div class="prepend-4 span-16 append-4 last line">
			<div class="span-4 label">Comercializador:</div>
			<div class="span-11" id="comercializadorContainer">
				Seleccione un Responsable
			</div>
		</div>

		<div class="prepend-4 span-16 append-4 last line">
			<div class="span-4 label">Sucursal:</div>
			<div class="span-12 last" id="sucursalContainer">
				Seleccione un Comercializador
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Identificaci&oacute;n del Cliente:</div>
			<div class="span-4">
				<input type="text" class="span-4" name="txtCustomer" id="txtCustomer"/>
			</div>
			<div class="span-4 label">N&uacute;mero de Tarjeta:</div>
			<div class="span-4 last">
				<input type="text" class="span-4" name="txtCard" id="txtCard"/>
			</div>
		</div>		

		<div class="span-24 last line">
			<div class="prepend-5 span-3 label">Tipo:</div>
			<div class="span-4 line">
				<select name="selType" id="selType">
					<option value="3">Todos</option>
					<option value="1">Total</option>
					<option value="2">Abono</option>
				 </select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-5 span-3 label">*Fecha Desde:</div>
			<div class="span-5">
				<input type="text" name="txtDateFrom" id="txtDateFrom"/>
			</div>
			<div class="span-3 label">*Fecha Hasta:</div>
			<div class="span-5 last">
				<input type="text" name="txtDateTo" id="txtDateTo"/>
			</div>
		</div>
	
		<div class="span-24 last line">
			<div class="prepend-11 span-5">
				Generar reporte:
				<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" onclick="checkPayments(1);"/>
				<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  onclick="checkPayments(2);"/>
			</div>
		</div>
	</div>
    {/if}
</form>
<div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>