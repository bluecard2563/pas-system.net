<!--/*
File: fExcessPayments.es.tpl
Author: Gabriela Guerrero
Creation Date:04/06/2010
Modified By:
Last Modified:
*/-->
{assign var="title" value="REPORTE DE PAGOS EN EXCESO"}
<form target="_blank" name="frmExcessPayments" id="frmExcessPayments" method="post">
    <div class="span-24 last title">
    	Reporte de Pagos en Exceso
    </div>
	
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
	
    <div class="span-24 last line">
         <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
        <div class="span-4">
            <select name="selCountry" id="selCountry">
                <option value="">- Seleccione -</option>
                {foreach from=$countriesList item="l"}
                    <option value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
		<div class="span-3 label">* Representante:</div>
        <div class="span-5 last" id="representanteContainer">
            Seleccione un Pa&iacute;s
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-4 label">Comercializador:</div>
        <div class="span-4" id="comercializadorContainer">
            Seleccione un Representante
        </div>
    </div>
			
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Sucursal:</div>
        <div class="span-5 last" id="sucursalContainer">
            Seleccione un Comercializador
        </div>
    </div>


    <div class="span-24 last line">
		<div class="prepend-5 span-3 label">Estado:</div>
		<div class="span-4">
            <select name="selStatus" id="selStatus">
				<option value="Available">Disponibles</option>
				<option value="Paid">Liquidados</option>
			 </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" onclick="checkInvoices(1);"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  onclick="checkInvoices(2);"/>
        </div>
    </div>
    <div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>
</form>