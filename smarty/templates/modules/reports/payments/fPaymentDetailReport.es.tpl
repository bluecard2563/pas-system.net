{assign var="title" value="REPORTE DE DETALLES DE PAGO"}
<div class="span-24 last title">
	Reporte de Detalles de Pagos
</div>

<div class="span-7 span-10 prepend-7 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

{if not $countriesList}
	<div class="append-7 span-10 prepend-7 last line">
		<div class="span-12 last error" id="messageZone" align="center">
			No se encontraron registros.
		</div>
	</div>
{else}
	<form name="frmPaymentDetail" id="frmPaymentDetail" method="post" action="#" target="_blank">
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
			<div class="span-5">
				<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s es obligatorio.'" class="span-4">
					<option value="">- Seleccione -</option>
					{foreach from=$countriesList item="l"}
						<option value="{$l.serial_cou}">{$l.name_cou}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">Representante:</div>
			<div class="span-5 append-4 last" id="managerContainer">
				<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.' class="span-4">
					<option value="">- Todos -</option>
				</select>
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Comercializador:</div>
			<div class="span-5" id="dealerContainer">
				<select name="selDealer" id="selDealer" title="El campo 'Comercializador' es obligatorio." class="span-4">
					<option value="">- Todos -</option>
				</select>
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Sucursal:</div>
			<div class="span-5" id="branchContainer">
				<select name="selBranch" id="selBranch" title="El campo 'Sucursal' es obligatorio." class="span-4">
					<option value="">- Todos -</option>
				</select>
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">* Fecha desde:</div>
			<div class="span-5">
				<input type="text" name="txtDateFrom" id="txtDateFrom" title="El campo 'Fecha desde' es obligatorio." />
			</div>

			<div class="span-3 label">* Fecha Hasta:</div>
			<div class="span-5 append-4 last" >
				<input type="text" name="txtDateTo" id="txtDateTo" title="El campo 'Fecha hasta' es obligatorio." />
			</div>
		</div>

		<div class="span-24 last line center">
			Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGenerate_PDF" class="PDF" />
			<input type="button" name="btnGenerateXLS" id="btnGenerate_XLS"  class="XLS" />
			<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
		</div>
	</form>
{/if}