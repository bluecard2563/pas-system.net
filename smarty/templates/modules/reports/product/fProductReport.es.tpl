{assign var="title" value="REPORTE DE PRODUCTOS"}
<form name="frmProductReport" id="frmProductReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        Reporte de Productos<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-5">
            <label>*Seleccione la zona:</label>
        </div>
        <div class="span-8 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
              <option value="">- Seleccione -</option>
              {foreach from=$nameZoneList item="l"}
              <option value="{$l.serial_zon}">{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-5">
            <label>*Seleccione el pa&iacute;s:</label>
        </div>
        <div class="span-8 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
              <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-5">
            <label>Seleccione el estado:</label>
        </div>
        <div class="span-8 last">
            <select name="selStatus" id="selStatus">
              <option value="">- Todos -</option>
              {foreach from=$statusList item="l"}
              <option value="{$l}">{$global_status[$l]}</option>
              {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
    </div>
</form>