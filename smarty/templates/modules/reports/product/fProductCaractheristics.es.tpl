{assign var="title" value="REPORTE DE CARACTER&Iacute;STICAS DE PROUDUCTOS"}
<div class="span-10 append-7 prepend-7 last title">
	Reporte de Caracter&iacute;sticas de Productos<br />
</div>

{if $product_list}
<div class="span-24 last line">
<table border="1" class="dataTable" align="center" id="salesTable">
	<thead>
		<tr class="header">
			<th style="text-align: center;">No. Producto</th>
			<th style="text-align: center;">M�ximo de Acompa�antes</th>
			<th style="text-align: center;">Viajes por per�odo</th>
			<th style="text-align: center;"># Menores Gratis</th>
			<th style="text-align: center;"># Mayores Gratis</th>
			<th style="text-align: center;">Precio para Familiares diferente</th>
			<th style="text-align: center;">Precio para C�nyuje diferente</th>
			<th style="text-align: center;">V�lida dentro del pa�s de emisi�n</th>
			<th style="text-align: center;">V�lida dentro del pa�s de residencia</th>
			<th style="text-align: center;">Masivo</th>
			<th style="text-align: center;">Pa�ses Schengen</th>
			<th style="text-align: center;">Acepta Adultos Mayor</th>
			<th style="text-align: center;">Aplica Incentivos/Comisiones?</th>
			<th style="text-align: center;">Tiene Servicios</th>
		</tr>
	</thead>
	<tbody>
		{foreach name="sales" from=$product_list item=s}
		<tr {if $smarty.foreach.sales.iteration is even}class="odd"{else}class="even"{/if} >
			<td class="tableField">{$s.serial_pro}</td>
			<td class="tableField">{$s.name_pbl}</td>
			<td class="tableField">{$s.flights_pro}</td>
			<td class="tableField">{$s.children_pro}</td>
			<td class="tableField">{$s.adults_pro}</td>			
			<td class="tableField">{$global_yes_no[$s.relative_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.spouse_pro]}</td>			
			<td class="tableField">{$global_yes_no[$s.emission_country_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.residence_country_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.masive_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.schengen_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.senior_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.has_comision_pro]}</td>
			<td class="tableField">{$global_yes_no[$s.has_services_pro]}</td>			
		</tr>
		{/foreach}
	</tbody>
</table>
</div>
{else}
	No existen registros
{/if}