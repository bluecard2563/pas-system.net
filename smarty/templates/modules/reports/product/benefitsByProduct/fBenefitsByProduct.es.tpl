<!--/*
File: fBenefitsByProduct.es.tpl
Author: Gabriela Guerrero
Creation Date: 12/07/2010
Modified By:
Last Modified:
*/-->
{assign var="title" value="REPORTE DE BENEFICIOS POR PRODUCTO"}

    <div class="span-24 last title">
    	Reporte de Beneficios por Producto<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    <div class="append-8 span-10 prepend-6 last line" id="messageContainer">
		{if not $zonesList}
			<div class="span-12 last error" id="message" align="center">
				No se encontraron registros.
			</div>
		{else}
    </div>
    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

<form target="_blank" name="frmBenefitsByProduct" id="frmBenefitsByProduct" method="post">

		<div class="span-24 last line">

			<div class="span-3 label">* Zona:</div>
			<div class="span-4">
				<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
					<option value="">- Seleccione -</option>
					{foreach from=$zonesList item="l"}
						<option value="{$l.serial_zon}">{$l.name_zon}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">* Pa&iacute;s:</div>
			<div class="span-4" id="countryContainer">
				<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
					<option value="">- Seleccione -</option>
				 </select>
			</div>

			<div class="span-3 label">* Producto:</div>
			<div class="span-4 last" id="productContainer">
				<select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio'>
					<option value="">- Seleccione -</option>
				 </select>
			</div>

		</div>

	<div class="span-24 last line" id="generate">
		<div class="prepend-10 span-5">
			Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"/>
		</div>
	{/if}
	</div>

</form>

<div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>