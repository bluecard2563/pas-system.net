{assign var="title" value="REPORTE DE PRECIOS POR PRODUCTO"}

<div class="span-24 last title">
	Reporte de Precios por Producto<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

<form target="_blank" name="frmPricesByProduct" id="frmPricesByProduct" method="post">
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">* Zona:</div>
		<div class="span-4">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$zonesList item="l"}
					<option value="{$l.serial_zon}">{$l.name_zon}</option>
				{/foreach}
			</select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">* Pa&iacute;s:</div>
		<div class="span-4" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">Producto:</div>
		<div class="span-4 last" id="productContainer">
			<select name="selProduct" id="selProduct" title='El campo "Producto" es obligatorio'>
				<option value="">- Seleccione -</option>
			</select>
		</div>

	</div>
	
	<div class="span-24 last line center">
		Generar reporte:
		<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"/>
	</div>
</form>