{assign var="title" value="REPORTE DE VENTAS TELEF&Oacute;NICAS"}
<form name="frmPhoneSalesReport" id="frmPhoneSalesReport" method="post" action="{$document_root}modules/reports/phoneSales/pPhoneSalesReportPDF" target="blank" class="">
	<div class="span-24 last line title">
		Reporte de Ventas Telef&oacute;nicas<br />
		<label>(*) Campos Obligatorios</label>
	</div>

	<div class="append-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line separator">
        <label>Seleccionar Counter Telef&oacute;nico:</label>
    </div>

	{if $countryList}
	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
		<div class="span-5">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$countryList item="l"}
					<option value="{$l.serial_dea}">{$l.name_cou}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Asesor de Venta:</div>
		<div class="span-5" id="counterContainer">
			<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio.'>
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line separator">
        <label>Rango de Fechas:</label>
    </div>

	<div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Fecha inicio:</div>
        <div class="span-5">
            <input type="text" name="beginDate" id="beginDate" title='El campo "Fecha inicio" es obligatorio.' />
        </div>

        <div class="span-3 label">* Fecha fin:</div>
        <div class="span-5 append-4 last" id="cityContainer">
			<input type="text" name="endDate" id="endDate" title='El campo "Fecha fin" es obligatorio.' />
        </div>
    </div>

	<div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
    </div>
	{else}
		<center>No existen pa&iacute;ses con comercializadores que realicen ventas telef&oacute;nicas.</center>
	{/if}
</form>