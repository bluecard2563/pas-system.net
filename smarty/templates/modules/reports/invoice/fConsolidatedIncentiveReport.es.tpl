{assign var="title" value="REPORTE CONSOLIDADO DE INCENTIVOS"}
<div class="span-24 last line title">
    Reporte Consolidado Incentivos<br/>
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmConsolidatedIncentiveReport" id="frmConsolidatedIncentiveReport" action="" method="post" target="_blank">
    <input type="hidden" name="hdnToday" id="hdnToday" value="{$today}"/>
    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">*Pa&iacute;s:</div>
            <div class="span-5">
                <select name="selCountry" id="selCountry" title="El campo 'Pais' es obligatorio.">
                    <option value="">- Seleccione -</option>
                    {foreach from=$countryList item="l"}
                        <option value="{$l.serial_cou}" {if $countryList|@count eq 1} selected{/if}>{$l.name_cou}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">Representante:</div>
            <div class="span-5 append-3 last" id="managerContainer">
                <select name="selManager" id="selManager">
                    <option value="">- Todos -</option>
                    {foreach from=$managerList item="l"}
                        <option value="{$l.serial_man}" {if $managerList|@count eq 1} selected{/if}>{$l.name_man|htmlall}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">*Fecha desde:</div>
        <div class="span-5">
            <input type="text" name="txtDateFrom" id="txtDateFrom" title="El campo 'Fecha desde' es obligatorio."/>
        </div>
        <div class="span-3 label">*Fecha Hasta:</div>
        <div class="span-5 append-4 last">
            <input type="text" name="txtDateTo" id="txtDateTo" title="El campo 'Fecha hasta' es obligatorio."/>
        </div>
    </div>
    <div class="span-24 last buttons line">
        Generar reporte:
        <img style="cursor: pointer" src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
    </div>
</form>