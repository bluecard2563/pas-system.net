{assign var="title" value="REPORTE DE FACTURAS"}
<div class="span-24 last line title">
    Reporte de Facturas<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmInvoiceReport" id="frmInvoiceReport" action="{$document_root}modules/reports/invoice/pInvoiceReport" method="post" target="_blank">
<div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
</div>


<div class="span-24 last line">
    <div class="prepend-4 span-4 label">
        *Seleccione la zona:
    </div>
    <div class="span-5 last">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
          <option value="">- Seleccione -</option>
          {foreach from=$nameZoneList item="l"}
          <option value="{$l.serial_zon}">{$l.name_zon}</option>
          {/foreach}
        </select>
    </div>
	<div class="span-3 label">
        *Seleccione el pa&iacute;s:
    </div>
    <div class="span-4 append-4 last" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">
        *Seleccione la ciudad:
    </div>
    <div class="span-5 last" id="cityContainer">
        <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">
        *Seleccione Representante:
    </div>
    <div class="span-5 last" id="managerContainer">
        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
	<div class="span-3 label">
        Responsable:
    </div>
    <div class="span-4 append-4 last" id="responsableContainer">
        <select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
          <option value="">- Todos -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">
        Comercializador:
    </div>
    <div class="span-5 last" id="dealerContainer">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
          <option value="">- Todos -</option>
        </select>
    </div>
</div>
<div class="span-24 last line">
	<div class="prepend-4 span-4 label">
        Sucursal:
    </div>
    <div class="span-4 append-4 last" id="branchContainer">
        <select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
          <option value="">- Todos -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-4 span-4 label">
        Tipo:
    </div>
    <div class="span-5 last">
		{foreach from=$invStatusList item="l"}
	  		<input type="checkbox" name="selType[]" id="selType_{$l}" value="{$l}">{$global_invoice_status.$l}<br/>
    	{/foreach}
    </div>
	<div class="span-3 label">
        Monto:
    </div>
    <div class="span-4 append-4 last" id="countryContainer">
        <select name="selAmount" id="selAmount" title='El campo "Monto" es obligatorio.'>
          <option value="">- Todos -</option>
		  <option value=">"> > </option>
		  <option value="<"> < </option>
		  <option value="="> = </option>
        </select>
    </div>
</div>
<div class="span-24 last line" id="divAmountVal" style="display: none">
	<div class="prepend-13 span-3 label" style="padding-left: 510px" >
		Valor:
	</div>
	<div class="span-4  append-4 last" >
		<input type="text" id="txtAmount" name="txtAmount" >
	</div>
</div>
<div class="span-24 last">
    <div class="span-24 last line separator">
        <label>Rango de Fechas</label>
	</div>
 </div>

<div class="span-24 last line" >
        <div class="prepend-4 span-4 label">* Fecha de Inicio:</div>
        <div class="span-5">
            <input type="text" name="txtStartDate" id="txtStartDate" title='El campo "Fecha de Inicio" es obligatorio' />
        </div>

        <div class="span-3 label">* Fecha de Fin:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha de Fin" es obligatorio' />
        </div>
</div>

<div class="span-24 last buttons line">
    Generar reporte:
	<img src="{$document_root}img/file_acrobat.gif" border="0" id="imgSubmitPDF">
	<img src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
</div>

</form>