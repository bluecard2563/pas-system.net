{assign var="title" value="REPORTE REDROM"}
<div class="span-24 last line title">
    Reporte Redrom<br/>
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmRedromReport" id="frmRedromReport" action="" method="post" target="_blank">
    <input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">*Fecha desde:</div>
        <div class="span-5">
            <input type="text" name="txtDateFrom" id="txtDateFrom" title="El campo 'Fecha desde' es obligatorio." />
        </div>
        <div class="span-3 label">*Fecha Hasta:</div>
        <div class="span-5 append-4 last" >
            <input type="text" name="txtDateTo" id="txtDateTo" title="El campo 'Fecha hasta' es obligatorio." />
        </div>
    </div>


    <div class="span-24 last buttons line">
        Generar reporte:
        <img src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
    </div>
</form>