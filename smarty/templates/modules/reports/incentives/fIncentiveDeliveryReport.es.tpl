{assign var="title" value="REPORTE CONTROL DE ENTREGA DE INCENTIVOS"}
<div class="span-24 last line title">
    Reporte Control de Entrega de Incentivos<br/>
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmIncentiveDeliveryReport" id="frmIncentiveDeliveryReport" action="" method="post" target="_blank">
    <input type="hidden" name="hdnToday" id="hdnToday" value="{$today}"/>
    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">Representante:</div>
            <div class="span-5" id="managerContainer">
                <select name="selManager" id="selManager">
                    <option value="">- Todos -</option>
                    {foreach from=$managerList item="m"}
                        <option value="{$m.serial_man}" {if $managerList|@count eq 1} selected{/if}>{$m.name_man|htmlall}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-3 label">Estado del Bono:</div>
            <div class="span-5 append-3 last" id="deliveryContainer">
                <select name="selDelivery" id="selDelivery">
                    <option value="">- Todos -</option>
                    <option value="1">Entregados</option>
                    <option value="2">No Entregados</option>
                </select>
            </div>

        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <img style="cursor: pointer" src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
    </div>
</form>