{assign var="title" value="INFORME DE AUTORIZACIONES POR ALERTA"}
<div class="span-24 last line title">
    Informe de Autorizaciones por Alerta<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="span-24 last line separator">
	<label>Par&aacute;metros de b&uacute;squeda:</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmAlertsReport" id="frmAlertsReport" action="{$document_root}modules/reports/alerts/pAuthorizedAlertsReportPDF" method="post" target="_blank">
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Zona:</div>
		<div class="span-5 append-1">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$zoneList item="l"}
			  <option value="{$l.serial_zon}">{$l.name_zon}</option>
			  {/foreach}
			</select>
		</div>
		<div class="span-3 label">* Pa&iacute;s:</div>
		<div class="span-4 append-4 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Representante:</div>
		<div class="span-5 append-1" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Fecha desde:</div>
		<div class="span-5 append-1">
			<input type="text" id="txtBeginDate" name="txtBeginDate" title='El campo "Fecha desde" es obligatorio.' />
		</div>
		<div class="span-3 label">* Fecha hasta:</div>
		<div class="span-5 append-3 last">
			<input type="text" id="txtEndDate" name="txtEndDate" title='El campo "Fecha hasta" es obligatorio.' />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">Estado:</div>
		<div class="span-5 append-1">
			<select id="selStatus" name="selStatus" >
				<option value="">- Todos -</option>
				{foreach from=$statusList item="l"}
					<option value="{$l}">{$global_sales_log_status[$l]}</option>
				{/foreach}
			</select>
		</div>
		<div class="span-3 label">* Tipo:</div>
		<div class="span-4 append-4 last">
			<select id="selType" name="selType" title='El campo "Tipo" es obligatorio.' >
				<option value="">- Seleccione -</option>
				{foreach from=$typeList item="l"}
					<option value="{$l}">{$global_alertTypes[$l]}</option>
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
        <!--input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  /-->
    </div>
</form>