{assign var="title" value="PRODUCTOS POR PROMOCI&Oacute;N"}
<div class="span-24 last title">
	 Reporte de Productos por Promoci&oacute;n<br />
	<label>(*) Campos Obligatorios</label>
</div>

<form name="frmReportProductsByPromo" id="frmReportProductsByPromo"  method="post" target="_blank">

	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
    	{if $error eq 1}
        	<div class="span-10 error" align="center">
            	No existen promociones registradas para los datos seleccionados.
            </div>
        {/if}
    </div>
    {/if}

	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	    <div class="span-24 last line">
        <div class="prepend-7 span-5 label">* Promociones Aplicadas a: </div>
        <div class="span-4 append-6 last">
            <select name="selApliedTo" id="selApliedTo" title='El campo "Aplicadas a" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$apliedTo item="l"}
                <option value="{$l}">{$global_appliedToTypes.$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

	<div class="prepend-3 span-20 last line" id="appliedToContainer"></div>

	<div class="span-24 last buttons line" id="buttonsContainer" style="display:none">
    Generar reporte:
	<img src="{$document_root}img/file_acrobat.gif" border="0" id="imgSubmitPDF">
	<img src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
</div>
	
</form>