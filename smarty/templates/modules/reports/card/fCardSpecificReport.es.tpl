{assign var="title" value="REPORTE DE TARJETAS"}
<form name="frmCardSpecificReport" id="frmCardSpecificReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        BUSCAR TARJETA<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-4">
            <label>* B&uacute;squeda por:</label>
        </div>
        <div class="span-8 last">
            <select name="selCreditNote" id="selCreditNote" title='El campo "B&uacute;squeda por" es obligatorio.'>
              <option value="">- Seleccione -</option>
              <option value="CARD">N&uacute;mero de Tarjeta</option>
              <option value="INVOICE">N&uacute;mero de Factura</option>
              <option value="CUSTOMER">Cliente</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="numCardContainer" style="display: none;">
        <div class="prepend-7 span-4">
            <label>*N&uacute;mero de la Tarjeta:</label>
        </div>
        <div class="span-5 last">
            <input type="text" id="txtNumCard" name="txtNumCard" value=""/>
        </div>
	</div>
	
	<div id="numInvoiceContainer" style="display: none;">
		<div class="span-24 last line" id="selCountryContainer">
	        <div class="span-12">
	            <div class="prepend-3 span-4 label">* Zona:</div>
	            <div class="span-5 last">
	                <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
	                  <option value="">- Seleccione -</option>
	                  {foreach from=$nameZoneList item="l"}
	                  <option value="{$l.serial_zon}">{$l.name_zon}</option>
	                  {/foreach}
	                </select>
	            </div>
	        </div>
	
	        <div class="span-8">
	            <div class="span-4 label">* Pa&iacute;s:</div>
	            <div class="span-4 last" id="countryContainer">
	                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
	                  <option value="">- Seleccione -</option>
	                </select>
	            </div>
	        </div>
	    </div>
	
	    <div class="span-24 last line">
	        <div class="span-12" id="selManagerContainer">
	            <div class="prepend-3 span-4 label">* Representante:</div>
	            <div class="span-5 last" id="managerContainer">
	                <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
	                  <option value="">- Seleccione -</option>
	                </select>
	            </div>
	        </div>
	
	        <div class="span-8" id="selDocumentToContainer">
	            <div class="span-4 label">* Documento a:</div>
	            <div class="span-4 last" id="documentToContainer">
	                <select name="selDocument" id="selDocument" title='El campo "Documento a" es obligatorio.'>
	                  <option value="">- Seleccione -</option>
	                  {foreach from=$purposeList item="l"}
	                  <option value="{$l}">{$global_typeManager[$l]}</option>
	                  {/foreach}
	                </select>
	            </div>
	        </div>
	    </div>
	
	
		<div class="span-24 last line">
	        
	        
	        <div class="prepend-7 span-4">
	            <label>*N&uacute;mero de la Factura:</label>
	        </div>
	        <div class="span-45 last" >
	            <input type="text" id="txtNumInvoice" name="txtNumInvoice" value=""/>
	        </div>
	    </div>
	</div>

    <div class="span-24 last line" id="customerContainer" style="display: none;">
        <div class="prepend-7 span-3">
            <label>* Cliente:</label>
        </div>
        <div class="span-6 last" id="cityContainer">
            <input type="text" id="txtClient" name="txtClient" class="long_text" />
            <input type="hidden" id="hdnClientID" name="hdnClientID" value=""  title='El campo "Cliente" es obligatorio' />
        </div>
    </div>
    
    <div class="span-24 last buttons line">
	    <input type="button" name="btnGenerate" id="btnGenerate" value="Ver Resultados" class=""  />
	</div>
	<input type="hidden" id="saleSerial" name="saleSerial" value="">
	
	
	<div id="resultsContainer"> </div>
</form>