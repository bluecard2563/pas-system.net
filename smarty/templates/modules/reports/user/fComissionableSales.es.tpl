{assign var="title" value="VENTAS COMISIONABLES"}
<form target="_blank" name="frmComissionableSales" id="frmComissionableSales" method="post">
	<div class="span-24 last title">
		Reporte de Ventas Comisionables<br />
		<label>(*) Campos Obligatorios</label>
	</div>
	<div class="append-8 span-10 prepend-6 last line" id="messageContainer">
		{if not $zonesList}
			<div class="span-12 last error" id="message" align="center">
				No se encontraron registros.
			</div>
		{else}
	</div>
	<div class="span-7 span-10 prepend-7 last line">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Zona:</div>
		<div class="span-4">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
				<option value="">- Seleccione -</option>
				{foreach from=$zonesList item="l"}
					<option value="{$l.serial_zon}">{$l.name_zon}</option>
				{/foreach}
			</select>
		</div>
		<div class="span-5 label">* Pa&iacute;s:</div>
		<div class="span-6 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
				<option value="">- Seleccione -</option>
			 </select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Representante:
		</div>
		<div class="span-4 last" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante:" es obligatorio.'>
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Fecha Inicio:</div>
		<div class="span-6">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio'/>
		</div>
		<div class="span-3 label">* Fecha Fin:</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio'/>
		</div>
	</div>

	<div class="span-24 last line buttons">
		<div class="prepend-10 span-5">
			Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
		</div>
		<input type="hidden" id="today" name="today" value="{$today}" />
		<input type="hidden" id="first_day" name="first_day" value="{$first_day}" />
	</div>
	
	{/if}

</form>