{assign var="title" value="REPORTE DE CUMPLEA&Ntilde;OS"}
<form name="frmBirthdateReport" id="frmBirthdateReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        Reporte de Cumplea&ntilde;os<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-3 label">*Zona:</div>
        <div class="span-7 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
              <option value="">- Seleccione -</option>
              {foreach from=$nameZoneList item="l"}
              <option value="{$l.serial_zon}">{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>

        <div class="span-3 label">*Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
              <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-3 label">Ciudad:</div>
        <div class="span-7 last" id="cityContainer">
            <select name="selCity" id="selCity">
              <option value="">- Todos -</option>
            </select>
        </div>

        <div class="span-3 label">Representante:</div>
        <div class="span-4 append-4 last" id="managerContainer">
            <select name="selManager" id="selManager">
              <option value="">- Todos -</option>
            </select>
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-3 span-3 label">Responsable:</div>
        <div class="span-7 last" id="responsibleContainer">
            <select name="selResponsible" id="selResponsible">
              <option value="">- Todos -</option>
            </select>
        </div>

        <div class="span-3 label">Mes:</div>
        <div class="span-4 append-4 last">
            <select name="selMonth" id="selMonth">
              <option value="">- Todos -</option>
              {foreach from=$months item ="m"}
              <option value="{$m}">{$global_weekMonths.$m}</option>
              {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
    </div>
</form>