<!--/*
File: fRefunds.es.tpl
Author: Gabriela Guerrero
Creation Date: 30/06/2010
Modified By:
Last Modified:
*/-->
{assign var="title" value="REPORTE DE REEMBOLSOS"}

    <div class="span-24 last title">
    	Reporte de Reembolsos<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    <div class="append-8 span-10 prepend-6 last line" id="messageContainer">
		{if not $countriesList}
			<div class="span-12 last error" id="message" align="center">
				No se encontraron registros.
			</div>
		{else}
		 <div class="span-12 last error" id="messageCustomer" align="center" style="display: none;">
			 Ingrese un Cliente.
        </div>
		<div class="span-12 last error" id="messageCountry" align="center" style="display: none;">
			 Seleccione un Pa&iacute;s.
        </div>
		<div class="span-12 last error" id="messageManager" align="center" style="display: none;">
			 Seleccione un Representante.
        </div>
		<div class="span-12 last error" id="messageDates" align="center" style="display: none;">
			 Seleccione una Fecha.
        </div>
    </div>
    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line">
		<div class="prepend-9 span-3">
            <label for="radioCustomer">Por Cliente:</label>
            <input type="radio" id="radioCustomer" name="radioChoice" value="customer"/>
		</div>
		<div class="span-4 last line">
            <label for="radioParams">Por Par&aacute;metro:</label>
            <input type="radio" id="radioParams" name="radioChoice" value="params" checked/>
        </div>
	</div>

<form target="_blank" name="frmRefunds" id="frmRefunds" method="post">

	<div id="customerContainer" style="display: none;">

		<div class="span-24 last line">
			<div class="prepend-5 span-5 label">Ingrese el nombre o documento del cliente:</div>
			<div class="span-9 last">
				<input type="text" name="txtCustomerData" id="txtCustomerData" title="El campo de b&uacute;squeda es obligatorio." class="autocompleter" />
			</div>
		</div>

		<input type="hidden" name="hdnCustomerID" id="hdnCustomerID" value="" />
		
	</div>

	<div id="paramsContainer">

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
			<div class="span-4">
				<select name="selCountry" id="selCountry">
					<option value="">- Seleccione -</option>
					{foreach from=$countriesList item="l"}
						<option value="{$l.serial_cou}">{$l.name_cou}</option>
					{/foreach}
				</select>
			</div>
			<div class="span-4 label">* Representante:</div>
			<div class="span-4 last" id="representanteContainer">
				Seleccione un Pa&iacute;s
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Ciudad:</div>
			<div class="span-4" id="ciudadContainer">
				Seleccione un Pa&iacute;s
			</div>
			<div class="span-4 label">Responsable:</div>
			<div class="span-4 last" id="responsableContainer">
				Seleccione una Ciudad
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Comercializador:</div>
			<div class="span-4" id="comercializadorContainer">
				Seleccione un Responsable
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-4 label">Sucursal:</div>
			<div class="span-4" id="sucursalContainer">
				Seleccione un Comercializador
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-5 span-3 label">Estado:</div>
			<div class="span-4 line">
				<select name="selStatus" id="selStatus">
					<option value="">- Todos -</option>
					<option value="APROVED">Aprobada</option>
					<option value="STAND-BY">Stand-By</option>
					<option value="DENIED">Negada</option>
				 </select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-5 span-3 label">Fecha Desde:</div>
			<div class="span-5">
				<input type="text" name="txtDateFrom" id="txtDateFrom"/>
			</div>
			<div class="span-3 label">Fecha Hasta:</div>
			<div class="span-5 last">
				<input type="text" name="txtDateTo" id="txtDateTo"/>
			</div>
		</div>	
		
	</div>

	<div class="span-24 last line" id="generate">
		<div class="prepend-11 span-5">
			Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"/>
		</div>
	</div>
	{/if}
    <input type="hidden" name="hdnFlag" id="hdnFlag" value="2" />
	
</form>

<div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>