{assign var="title" value="REPORTE DE NOTAS DE CREDITO"}
<div class="span-24 last line title">
    Reporte de Notas de Credito<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmCreditNoteReport" id="frmCreditNoteReport" action="{$document_root}modules/reports/credit_note/pCreditNoteReport" method="post" target="_blank">
    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>


    <div class="span-24 last line">
        <div class="prepend-8 span-4">
            <label>* B&uacute;squeda por:</label>
        </div>
        <div class="span-8 last">
            <select name="selCreditNote" id="selCreditNote" title='El campo "B&uacute;squeda por" es obligatorio.'>
              <option value="">- Seleccione -</option>
              <option value="CUSTOMER">Cliente</option>
              <option value="INVOICE">Factura</option>
              <option value="CREDIT_NOTE">Nota de cr&eacute;dito</option>
              <option value="GENERAL">Par&aacute;metros</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="selCountryContainer" style="display: none;">
        <div class="span-12">
            <div class="prepend-3 span-4 label">* Zona:</div>
            <div class="span-5 last">
                <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                  {foreach from=$nameZoneList item="l"}
                  <option value="{$l.serial_zon}">{$l.name_zon}</option>
                  {/foreach}
                </select>
            </div>
        </div>

        <div class="span-8">
            <div class="span-4 label">* Pa&iacute;s:</div>
            <div class="span-4 last" id="countryContainer">
                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="span-12" id="selManagerContainer" style="display: none;">
            <div class="prepend-3 span-4 label"> Representante:</div>
            <div class="span-5 last" id="managerContainer">
                <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-8" id="selCityContainer" style="display: none;">
            <div class="span-4 label">Ciudad:</div>
            <div class="span-4 last" id="cityContainer">
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-8" id="selDocumentToContainer" style="display: none;">
            <div class="span-4 label">* Documento a:</div>
            <div class="span-4 last" id="documentToContainer">
                <select name="selDocument" id="selDocument" title='El campo "Documento a" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                  {foreach from=$purposeList item="l"}
                  <option value="{$l}">{$global_typeManager[$l]}</option>
                  {/foreach}
                </select>
            </div>
        </div>
    </div>

    <div class="span-24 last line" id="extraParamsContainer" style="display: none;">
        <div class="span-20 line">
            <div class="prepend-3 span-4 label">Comercializador:</div>
            <div class="span-5 last" id="dealerContainer">
                <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-20">
            <div class="prepend-3 span-4 label">Tipo:</div>
            <div class="span-4 last">
                <select name="selType" id="selType" title='El campo "Tipo" es obligatorio.'>
                  <option value="">- Seleccione -</option>
                  <option value="1">Reembolso</option>
                  <option value="2">Anulaci&oacute;n de Factura</option>
                  <option value="3">Penalidad</option>
				  <option value="4">Incobrable</option>
                </select>
            </div>
        </div>
    </div>

    <!--CUSTOMER-->
    <div class="span-24 last line" id="customerContainer" style="display: none;">
        <div class="prepend-8  span-2">
            <label>* Cliente:</label>
        </div>
        <div class="span-5 last">
            <input type="text" id="txtClient" name="txtClient" class="long_text" />
            <input type="hidden" id="hdnClientID" name="hdnClientID" value=""  title='El campo "Cliente" es obligatorio' />
        </div>
    </div>
    <!--INVOICE-->
    <div class="span-24 last line" id="invoiceContainer" style="display: none;">
        <div class="prepend-8  span-4">
            <label>* Factura #:</label>
        </div>
        <div class="span-5 last">
            <input type="text" id="txtNumInvoice" name="txtNumInvoice" title='El campo "# Factura" es obligatorio' />
        </div>
    </div>
    <!--CREDIT NOTE-->
    <div class="span-24 last line" id="creditNoteContainer" style="display: none;">
        <div class="prepend-8  span-4">
            <label>* Nota de Credito #:</label>
        </div>
        <div class="span-5 last">
            <input type="text" id="txtNumCreditNote" name="txtNumCreditNote" title='El campo "# Nota de Credito" es obligatorio' />
        </div>
    </div>

    <!--DATES-->
    <div class="span-24 last line separator">
        <label>Rango de Fechas</label>
    </div>

    <div class="span-24 last line" >
        <div class="prepend-3 span-4 label">Fecha de Inicio:</div>
        <div class="span-5">
            <input type="text" name="txtStartDate" id="txtStartDate" title='El campo "Fecha de Inicio" es obligatorio' />
        </div>

        <div class="span-4 label">Fecha de Fin:</div>
        <div class="span-5 append-3 last">
            <input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha de Fin" es obligatorio' />
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" id="imgSubmitPDF" class="PDF" />
        <input type="button" id="imgSubmitXLS"  class="XLS"  />
    </div>

</form>