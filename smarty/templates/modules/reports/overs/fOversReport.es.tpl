{assign var="title" value="REPORTE DE OVERS"}
<div class="span-24 last line title">
    Reporte de Overs<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmOversReport" id="frmOversReport" method="post">
<div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione la zona:</label>
    </div>
    <div class="span-8 last">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
          <option value="">- Seleccione -</option>
          {foreach from=$nameZoneList item="l"}
          	<option value="{$l.serial_zon}">{$l.name_zon}</option>
          {/foreach}
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione el pa&iacute;s:</label>
    </div>
    <div class="span-8 last" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">* Over Asignado a: </div>
    <div class="span-6 append-6 last line">
        <select name="selApliedTo" id="selApliedTo" title='El campo "Asignado a" es obligatorio.'>
            <option value="">- Seleccione -</option>
            {foreach from=$apliedTo item="l"}
            <option value="{$l}">{$global_appliedToTypes.$l}</option>
            {/foreach}
        </select>
    </div>
</div>

<div class="span-24 last line" id="managerContainerTop" style="display: none;">
    <div class="prepend-8 span-5">
        <label>*Seleccione el representante:</label>
    </div>
    <div class="span-8 last" id="managerContainer">
        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line" id="dealerContainerTop" style="display: none;">
    <div class="prepend-8 span-5">
        <label>*Seleccione el comercializador:</label>
    </div>
    <div class="span-8 last" id="dealerContainer">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last buttons line">
    <input type="submit" value="Generar Reporte"/>
</div>
</form>
<div id="reportResults"></div>