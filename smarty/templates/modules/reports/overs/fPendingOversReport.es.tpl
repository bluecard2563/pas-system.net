{assign var="title" value="REPORTE DE OVERS PENDIENTES"}
<div class="span-24 last line title">
    Reporte de Overs<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmOversReport" id="frmOversReport" method="post" target="_blank">
	<div class="prepend-7 span-10 append-7 last">
	        <ul id="alerts" class="alerts"></ul>
	</div>
	
	<div class="span-24 last line">
	    <div class="prepend-7 span-5 push_right">
	        <label>*Seleccione la zona:</label>
	    </div>
	    <div class="span-8 last push_left">
	        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
	          <option value="">- Seleccione -</option>
	          {foreach from=$nameZoneList item="l"}
	          	<option value="{$l.serial_zon}">{$l.name_zon}</option>
	          {/foreach}
	        </select>
	    </div>
	</div>
	
	<div class="span-24 last line">
	    <div class="prepend-7 span-5 push_right">
	        <label>*Seleccione el pa&iacute;s:</label>
	    </div>
	    <div class="span-8 last" id="countryContainer">
	        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
	          <option value="">- Seleccione -</option>
	        </select>
	    </div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-7 span-5 push_right">
			<b>* Over Asignado a:</b> 
		</div>
	    <div class="span-8 last">
	        <select name="selApliedTo" id="selApliedTo" title='El campo "Asignado a" es obligatorio.'>
	            <option value="">- Seleccione -</option>
	            {foreach from=$apliedTo item="l"}
	            <option value="{$l}">{$global_appliedToTypes.$l}</option>
	            {/foreach}
	        </select>
	    </div>
	</div>
	
	<div class="span-24 last line" id="managerContainerTop" style="display: none;">
		<div class="prepend-7 span-5 push_right">
	        <label>*Seleccione el Representante:</label>
	    </div>
	    <div class="span-8 last" id="managerContainer">
	        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
	          <option value="">- Seleccione -</option>
	        </select>
	    </div>
	</div>
	
	<div class="span-24 last" id="dealerContainerTop" style="display: none;">
	    <div class="prepend-7 span-5 push_right">
	        <label>Seleccione el Responsable:</label>
	    </div>
	    <div class="span-8 last line" id="responsableContainer">
	        <select name="selResp" id="selResp" title='El campo "Responsable" es obligatorio.'>
	          <option value="">- Todos -</option>
	        </select>
	    </div>
	    
	    <div class="prepend-7 span-5 push_right">
	        <label>Seleccione el comercializador:</label>
	    </div>
	    <div class="span-8 last line" id="dealerContainer">
	        <select name="selDealerDea" id="selDealerDea" title='El campo "Comercializador" es obligatorio.'>
	          <option value="">- Todos -</option>
	        </select>
	    </div>
	    
	    <div class="prepend-7 span-5 push_right">
	        <label>Seleccione la Sucursal:</label>
	    </div>
	    <div class="span-8 last line" id="branchContainer">
	        <select name="selDealer" id="selDealer" title='El campo "Sucursal" es obligatorio.'>
	          <option value="">- Todos -</option>
	        </select>
	    </div>
	</div>
    <div class="span-24 last buttons line">
	    <input type="button" value="Buscar" id="searchButton"/>
	</div>
	<div id="reportResults" class="span-22 prepend-1"></div>
</form>