{assign var="title" value="B&Uacute;SQUEDA DE FACTURA PARA REIMPRESI&OACUTE;N"}
<div class="span-24 last title ">
    	B&uacute;squeda de Factura para reimpresi&oacute;n<br />
        <label>(*) Campos Obligatorios</label>
</div>
<div class="span-24 last line"></div>

<form name="frmSearchInvoice" id="frmSearchInvoice" target="_blank" action="{$document_root}modules/invoice/pPrintInvoice" method="post" >
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error}
                
                {/if}
            </div>
        </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label" id="zoneContainer">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$zoneList item="zl"}
                    <option id="{$zl.serial_zon}" value="{$zl.serial_zon}">{$zl.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-5 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">* Representante:</div>
        <div class="span-5" id="managerContainer">
            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
        <div class="span-3 label">* Documento Aplicado a:</div>
        <div class="span-5 append-4 last" id="documentContainer">
            <select name="selDocument" id="selDocument" title='El campo "Tipo de documento" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
         </div>
    </div>
    <div class="span-24 last line" id="invoiceContainer" style="display:none;">
        <div class="prepend-4 span-8 label">* Ingrese el n&uacute;mero de factura:</div>
        <div class="span-8 append-4 last">
            <input type="text" name="txtInvoiceNumber" id="txtInvoiceNumber" title="El campo de b&uacute;squeda es obligatorio." class="span-4"/>
        </div>
    </div>
    <div class="span-24 last line">
    </div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" />
    </div>
</form>