{assign var="title" value="REPORTE PRODUCCIÓN"}
<form name="frmCardReport" id="frmCardReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        REPORTE PRODUCCI&Oacute;N<br/>
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>* Zona:</label>
        </div>
        <div class="span-6 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
                <option value="">- Seleccione -</option>
                {foreach from=$nameZoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3">
            <label>* Pa&iacute;s:</label>
        </div>
        <div class="span-4 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Producto:</label>
    </div>

        <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>* Fecha inicio:</label>
        </div>
        <div class="span-6 last">
            <input type="text" id="txtBeginDate" name="txtBeginDate" title='El campo "Fecha inicio" es obligatorio.'/>
        </div>

        <div class="span-3">
            <label>* Fecha fin:</label>
        </div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtEndDate" name="txtEndDate" title='El campo "Fecha fin" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>* Estado:</label>
        </div>
        <div class="span-6 last">
            <select name="selStatus[]" id="selStatus">
                <option value="VOID">Anulada</option>
                <option value="REFUNDED">Reembolsada</option>
                <option value="SALES">Ventas</option>

                {*{foreach from=$statusList item="sl"}*}
                    {*<option value="{$sl}">{$global_salesStatus[$sl]}</option>*}
                {*{/foreach}*}
            </select>
        </div>

    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        {*<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>*}
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS" class="XLS"/>
    </div>
</form>