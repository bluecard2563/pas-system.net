{assign var="title" value="PRODUCTOS POR COMERCIALIZADOR"}
<div class="span-24 last title">
	 Reporte de Productos por Comercializador<br />
	<label>(*) Campos Obligatorios</label>
</div>

<form name="frmReportProductByDealer" id="frmReportProductByDealer" action="{$document_root}modules/reports/productByDealer/pViewProductsByDealer" method="post" target="_blank">
	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
            <select name="selZone" id="selZone" title="El campo 'Zona' es olbigatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$zoneList item="z"}
				<option value="{$z.serial_zon}">{$z.name_zon}</option>
				{/foreach}
			</select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-5 append-3 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio">
				<option value="">- Seleccione -</option>
			</select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Representante:</div>
        <div class="span-5" id="managerContainer">
            <select name="selManager" id="selManager">
				<option value="">- Todos -</option>
			</select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Comercializador</div>
        <div class="span-5" id="dealerContainer">
            <select name="selDealer" id="selDealer">
				<option value="">- Todos -</option>
			</select>
        </div>
	</div>

	<div class="span-24 last line buttons">
		<img src="{$document_root}img/page_excel.png" border="0" id="imgSubmit" style="cursor: pointer;"><br><br>
		<label style="font-size: 11px;"><i>El reporte incluye solamente a los comercializadores <br>con productos asignados.</i></label>
	</div>
</form>