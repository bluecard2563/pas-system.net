{assign var="title" value="REPORTE DE INCENTIVOS"}
<div class="span-24 last title">
	Reporte de Incentivos<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-7 span-10 prepend-7 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $zoneWithBonusList}
<form id="frmBonusReport" name="frmBonusReport" action="#" method="post" target="_blank">
<div class="span-24 las line buttons">
	<input type="radio" name="rdType" id="PARAMETER" value="PARAMETER" checked>
		<label for="PARAMETER">Por Par&aacute;metros</label>
	<input type="radio" name="rdType" id="CARD" value="CARD">
		<label for="CARD">N&uacute;mero de Tarjeta</label>
</div>

<div class="span-24 last line" id="parameters">
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Zona:
		</div>
		<div class="span-6 last">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$zoneWithBonusList item="l"}
			  <option value="{$l.serial_zon}">{$l.name_zon}</option>
			  {/foreach}
			</select>
		</div>
		<div class="span-3 label">
			* Pa&iacute;s:
		</div>
		<div class="span-4 append-4 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Representante:
		</div>
		<div class="span-6 last" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
		<div class="span-3 label">
			* Ciudad:
		</div>
		<div class="span-4 append-4 last" id="cityContainer">
			<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Comercializador:
		</div>
		<div class="span-6 last" id="dealerContainer">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Sucursal de Comercializador
		</div>
		<div class="span-4 append-4 last" id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			Counters:
		</div>
		<div class="span-6 last" id="counterContainer">
			<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>

		<div class="span-3 label">
			Estado:
		</div>
		<div class="span-4 append-4 last">
			<select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio.'>
			  <option value="">- Todos -</option>
			  <option value="ACTIVE">Pendientes</option>
			  <option value="PAID">Pagados</option>
			  <option value="REFUNDED">Reembolsados</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<input type="hidden" name="today" id="today" value="{$today}" />
		<div class="prepend-3 span-4 label">
			Fecha Desde:
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" value="" />
		</div>

		<div class="span-3 label">
			Fecha Hasta:
		</div>
		<div class="span-6 append-2 last">
			<input type="text" name="txtEndDate" id="txtEndDate" value="" />
		</div>
	</div>
</div>

<div class="span-24 last line" id="cardNumber">
	<div class="prepend-8 span-4 label">
		# Tarjeta:
	</div>
	<div class="span-6 append-6 last">
		<input type="text" name="txtCardNumber" id="txtCardNumber" title="El campo '# de Tarjeta' es obligatorio." />
	</div>
</div>

<div class="span-24 last line buttons">
	Generar reporte:
	<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
	<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"/>
</div>
</form>
{else}
<div class="span-10 append-7 prepend-7 last line">
	<div class="span-10 error center">
		No existen incentivos generados en el sistema.
	</div>
</div>
{/if}