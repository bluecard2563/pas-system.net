{assign var="title" value="REPORTE DE STOCK"}
<form name="frmStockReport" id="frmStockReport" target="_blank" method="post" action="{$document_root}modules/reports/stock/pReportStock">
	<div class="span-24 last title">
		Reporte de Stock<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	<div class="span-7 span-10 prepend-7 last">
		<ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5"> <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
				<option value="">- Seleccione -</option>
                {foreach from=$nameZoneList item="l"}
				<option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
			<select name="selCountry" id="selCountry">
				<option value="">- Seleccione -</option>
            </select>
		</div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Representante:</div>
        <div class="span-5" id="managerContainer">
			<select name="selManager" id="selManager">
				<option value="">- Todos -</option>
            </select>
        </div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Responsable:</div>
        <div class="span-5" id="responsableContainer">
			<select name="selResponsable" id="selResponsable">
				<option value="">- Todos -</option>
            </select>
        </div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Comercializador:</div>
        <div class="span-5" id="dealerContainer">
			<select name="selDealer" id="selDealer">
				<option value="">- Todos -</option>
            </select>
		</div>
	</div>
    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">Sucursal de Comercializador:</div>
        <div class="span-4 append-4 last"id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
				<option value="">- Todas -</option>
            </select>
        </div>

	</div>

    <div class="span-24 last line">
		<div class="prepend-4 span-4 label">Asesor de Venta:</div>
        <div class="span-5" id="counterContainer">
			<select name="selCounter" id="selCounter" title='El campo "Counter" es obligatorio.'>
				<option value="">- Todos -</option>
            </select>
		</div>
		<div  class="span-3 label">Tipo:</div>
		<div class="span-4 append-4 last">
			<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
				<option value="">- Todos -</option>
                {foreach from=$typeList item=sl}
				<option value="{$sl}" >{$sl|lower|capitalize}</option>
                {/foreach}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Fecha Desde:</div>
        <div class="span-5">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title="El campo 'Fecha Desde' es obligatorio" />
		</div>
        <div  class="span-3 label">* Fecha Hasta:</div>
		<div class="span-5 append-3 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title="El campo 'Fecha Hasta' es obligatorio" />
        </div>
	</div>

    <div class="span-24 last buttons line">
		<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
		<input type="hidden" name="today" id="today" value="{$today}" />
    </div>
</form>