{assign var="title" value="LISTA DE CORREOS"}
<div class="span-24 last line title">
    Lista de Correos<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmMailList" id="frmMailList" action="{$document_root}modules/reports/customer/pMailList" method="post" target="_blank">
<div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione la zona:</label>
    </div>
    <div class="span-8 last">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
          <option value="">- Seleccione -</option>
          {foreach from=$nameZoneList item="l"}
          <option value="{$l.serial_zon}">{$l.name_zon}</option>
          {/foreach}
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione el pa&iacute;s:</label>
    </div>
    <div class="span-8 last" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>Seleccione la ciudad:</label>
    </div>
    <div class="span-8 last" id="cityContainer">
        <select name="selCity" id="selCity">
          <option value="">- Todos -</option>
        </select>
    </div>
</div>


<div class="span-24 last buttons line">
    Generar reporte:
	<img src="{$document_root}img/file_acrobat.gif" border="0" id="imgSubmitPDF">
	<img src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
</div>
	
</form>