{assign var="title" value="REPORTE DE VIAJES"}
<div class="span-24 last line title">
    Reporte<br/>
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmCustomerTripLastWeekReport" id="frmCustomerTripLastWeekReport" action="" method="post" target="_blank">
    <input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-8 span-5">
            <label>* Estado del Viaje:</label>
        </div>
        <div class="span-8 last">
            <select name="selDateType" id="selDateType" title="El campo 'Estado del viaje es obligatorio.'" class="span-4">
                <option value="">- Seleccione -</option>
                    <option value="beginDate">Salida</option>
                    <option value="endDate">Llegada</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Fecha desde:</div>
        <div class="span-5">
            <input type="text" name="txtDateFrom" id="txtDateFrom" title="El campo 'Fecha desde' es obligatorio." />
        </div>
        <div class="span-3 label">* Fecha Hasta:</div>
        <div class="span-5 append-4 last" >
            <input type="text" name="txtDateTo" id="txtDateTo" title="El campo 'Fecha hasta' es obligatorio." />
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <img src="{$document_root}img/page_excel.png" border="0" id="imgSubmitXLS">
    </div>
</form>