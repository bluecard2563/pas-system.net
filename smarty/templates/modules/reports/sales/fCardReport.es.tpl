{assign var="title" value="REPORTE DE TARJETAS"}
<form name="frmCardReport" id="frmCardReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        Reporte de Tarjetas<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>*Zona:</label>
        </div>
        <div class="span-6 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
              <option value="">- Seleccione -</option>
              {foreach from=$nameZoneList item="l"}
              <option value="{$l.serial_zon}">{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>

        <div class="span-3">
            <label>*Pa&iacute;s:</label>
        </div>
        <div class="span-4 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
              <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Ciudad:</label>
        </div>
        <div class="span-6 last" id="cityContainer">
            <select name="selCity" id="selCity">
              <option value="">- Todos -</option>
            </select>
        </div>

        <div class="span-3">
            <label>Representante:</label>
        </div>
        <div class="span-4 append-4 last" id="managerContainer">
            <select name="selManager" id="selManager">
              <option value="">- Todos -</option>
            </select>
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Responsable:</label>
        </div>
        <div class="span-6 last" id="comissionistsContainer">
            <select name="selComissionist" id="selComissionist">
              <option value="">- Todos -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Comercializador:</label>
        </div>
        <div class="span-6 last" id="comercializadorContainer">
            <select name="selDealer" id="selDealer">
              <option value="">- Todos -</option>
            </select>
        </div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Sucursal:</label>
        </div>
        <div class="span-4 append-4 last" id="branchContainer">
            <select name="selBranch" id="selBranch">
              <option value="">- Todos -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Producto:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Fecha de:</label>
        </div>
        <div class="span-6 last">
            <input type="radio" id="rdoDateEmission" name="rdoDate" value="insystem" checked /> Ingreso al sistema<br />
            <input type="radio" id="rdoDateCoverage" name="rdoDate" value="coverage"/> Cobertura
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>* Fecha inicio:</label>
        </div>
        <div class="span-6 last">
            <input type="text" id="txtBeginDate" name="txtBeginDate" title='El campo "Fecha inicio" es obligatorio.'/>
        </div>

        <div class="span-3">
            <label>* Fecha fin:</label>
        </div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtEndDate" name="txtEndDate" title='El campo "Fecha fin" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Tipo:</label>
        </div>
        <div class="span-6 last">
            <select name="selType" id="selType">
              <option value="">- Todos -</option>
              {foreach from=$typeList item="tl"}
              <option value="{$tl}">{$global_cardSalestype[$tl]}</option>
              {/foreach}
            </select>
        </div>

        <div class="span-3">
            <label>Producto:</label>
        </div>
        <div class="span-4 append-4 last" id="productContainer">
            <select name="selProduct" id="selProduct">
              <option value="">- Todos -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Estado:</label>
        </div>
        <div class="span-6 last">
            <select name="selStatus[]" id="selStatus" multiple>
              {foreach from=$statusList item="sl"}
              <option value="{$sl}">{$global_salesStatus[$sl]}</option>
              {/foreach}
            </select>
        </div>

        <div class="span-3">
            <label>Monto:</label>
        </div>
        <div class="span-1">
            <select name="selOperator" id="selOperator" style="width: 35px;">
              <option value="="> = </option>
              <option value=">"> > </option>
              <option value="<"> < </option>
            </select>
        </div>
        <div class="span-4 last">
            <input type="text" id="txtAmount" name="txtAmount" style="width: 100px;">
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Stock: </label>
        </div>
        <div class="span-6 last">
            <select name="selStockType" id="selStockType">
              <option value="">- Todos -</option>
              {foreach from=$stockTypeList item="st"}
              <option value="{$st}">{$global_salesStockType.$st}</option>
              {/foreach}
            </select>
        </div>
    </div>

	<div class="span-24 last line">
        <div class="prepend-4 span-3">
            <label>Ordenar por: </label>
        </div>
        <div class="span-6 last">
            <select name="selOrderBy" id="selOrderBy">
              {foreach from=$order_by key="k" item="ob"}
              <option value="{$k}">{$ob}</option>
              {/foreach}n>
            </select>
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
    </div>
</form>