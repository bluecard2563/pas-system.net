<!--/*
File: fFreeCardReport.es.tpl
Author: Gabriela Guerrero
Creation Date: 14/06/2010
Modified By:
Last Modified:
*/-->

{assign var="title" value="REPORTE DE TARJETAS FREE"}
<form target="_blank" name="frmFreeCard" id="frmFreeCard" method="post">

    <div class="span-24 last title">
    	Reporte de Tarjetas Free
    </div>

    <div class="append-8 span-10 prepend-6 last " id="messageContainer">
		{if not $zonesList}
			<div class="span-12 last error" id="message" align="center">
				 No se encontraron registros.
			</div>
		{else}
		<div class="span-12 last error" id="messageDates" align="center" style="display: none;">
			 Por favor, ingrese fechas v&aacute;lidas.
        </div>
    </div>

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
         <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-4">
            <select name="selZone" id="selZone" title="El campo 'Zona' es obligatorio">
                <option value="">- Seleccione -</option>
                {foreach from=$zonesList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-4 label">* Pa&iacute;s:</div>
        <div class="span-5 last" id="paisContainer">
            <select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio">
				<option value="">- Seleccione -</option>
			 </select>
        </div>

    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Representante:</div>
        <div class="span-5" id="representanteContainer">
            <select name="selManager" id="selManager">
				<option value="">Todos</option>
			 </select>
        </div>
        <div class="span-3 label">Ciudad:</div>
        <div class="span-5 append-3 last" id="cityContainer">
            <select name="selCity" id="selCity">
				<option value="">Todos</option>
			 </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Comercializador:</div>
        <div class="span-5" id="comercializadorContainer">
            <select name="selDealer" id="selDealer">
				<option value="">Todos</option>
			 </select>
        </div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-3 span-4 label">Sucursal:</div>
        <div class="span-5 last" id="sucursalContainer">
            <select name="selBranch" id="selBranch">
				<option value="">Todos</option>
			 </select>
        </div>
    </div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">Estado:</div>
		<div class="span-5 last">
            <select name="selStatus" id="selStatus">
				<option value="">Todos</option>
				<option value="ACTIVE"> Autorizadas </option>
				<option value="DENIED"> Negadas </option>
				<option value="REQUESTED"> Pendientes </option>
			 </select>
        </div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">Fecha Desde:</div>
		<div class="span-5">
            <input type="text" name="txtDateFrom" id="txtDateFrom" title='El campo "Fecha Desde" es obligatorio'/>
        </div>
		<div class="span-3 label">Fecha Hasta:</div>
		<div class="span-5 last">
            <input type="text" name="txtDateTo" id="txtDateTo" title='El campo "Fecha Hasta" es obligatorio'/>
        </div>
	</div>


    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" onclick="checkSales(1);"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS" class="XLS"  onclick="checkSales(2);"/>
        </div>
    </div>
	{/if}
    <div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>
</form>