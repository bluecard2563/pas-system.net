{assign var="title" value="REPORTE DE TARJETAS MASIVAS"}
<form name="frmCardReport" id="frmCardReport" action="" method="post" target="blank">
    <div class="span-24 last line title">
        Reporte de Tarjetas Masivas<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Cliente:</label>
    </div>

	<div class="span-24 last line">
		<div class="prepend-2 span-8 label">* Cliente:</div>
		<div class="span-8 append-4 last">
			<input type="text" name="txtCustomer" id="txtCustomer" size="34" title="El campo 'Cliente' es obligatorio." />
			<input type="hidden" name="hdnSerial_cus" id="hdnSerial_cus" value=""/>
		</div>
	</div>

	<div class="span-24 last buttons line">
			<input type="button" name="btnSearch" id="btnSearch" value="Ver ventas"/>
	</div>

	<div class="span-24 last line" id="parentCardsContainer"></div>

	<div class="span-24 last line" id="childrenCardsContainer"></div>

</form>