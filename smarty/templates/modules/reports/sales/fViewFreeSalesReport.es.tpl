{assign var="title" value="REPORTE VENTAS FREE"}
<div class="span-24 last line title">
    Reporte de Tarjetas Free<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
	<div class="prepend-4 span-3">
		<label>*Zona:</label>
	</div>
	<div class="span-6 last">
		<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
		  <option value="">- Seleccione -</option>
		  {foreach from=$zoneList item="l"}
		  <option value="{$l.serial_zon}">{$l.name_zon}</option>
		  {/foreach}
		</select>
	</div>

	<div class="span-3">
		<label>*Pa&iacute;s:</label>
	</div>
	<div class="span-4 append-4 last" id="countryContainer">
		<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
		  <option value="">- Seleccione -</option>
		</select>
	</div>
</div>

<div class="span-24 last buttons line">
	<input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
</div>

<div class="span-24 last line" id="cardsContainer"></div>