{assign var="title" value="TARJETAS POR CLIENTES"}
<div class="span-24 last line title">
    Reporte de Tarjetas por Clientes<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
	 <div class="prepend-5 span-6">
        <label>* Nombre y Apellido o Documento:</label>
    </div>
    <div class="span-8 append-5 last" id="countryContainer">
        <input type="text" name="txtNameCustomer" id="txtNameCustomer" value="" class="autocompleter" />
    </div>
	<input type="hidden" name="hdnSerial_cus" id="hdnSerial_cus" value=""/>
</div>

<div class="span-24 last buttons line">
	<input type="button" name="btnSearch" id="btnSearch" value="Buscar" />
</div>

<div class="span-24 last line" id="cardsContainer"></div>