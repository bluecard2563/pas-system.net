{assign var="title" value="REPORTE DE ANALISIS DE VENTAS"}
<div class="span-24 last title">
	Reporte de An&aacute;lisis de Ventas<br />
	<label>(*) Campos Obligatorios</label>
</div>
<div class="append-8 span-10 prepend-6 last line" id="messageContainer">
	{if not $zonesList}
		<div class="span-12 last error" id="message" align="center">
			No se encontraron registros.
		</div>
	{else}
</div>
<div class="span-7 span-10 prepend-7 last line">
	<ul id="alerts" class="alerts"></ul>
</div>

<form target="_blank" name="frmSalesAnalysis" id="frmSalesAnalysis" method="post">

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">* Zona:</div>
			<div class="span-4">
				<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
					<option value="">- Seleccione -</option>
					{foreach from=$zonesList item="l"}
						<option value="{$l.serial_zon}">{$l.name_zon}</option>
					{/foreach}
				</select>
			</div>
			<div class="span-4 label">* Pa&iacute;s:</div>
			<div class="span-5 last" id="countryContainer">
				<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
					<option value="">- Seleccione -</option>
				 </select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-3 label">* Fecha Desde:</div>
			<div class="span-5">
				<input type="text" name="txtDateFrom" id="txtDateFrom" title='El campo "Fecha Desde" es obligatorio'/>
			</div>
			<div class="span-3 label">* Fecha Hasta:</div>
			<div class="span-5 last">
				<input type="text" name="txtDateTo" id="txtDateTo" title='El campo "Fecha Hasta" es obligatorio'/>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Representante:</div>
			<div class="span-5" id="managerContainer">
				<select name="selManager" id="selManager">
					<option value="">Todos</option>
				 </select>
			</div>
			<div class="span-3 label">Ciudad:</div>
			<div class="span-5 append-3 last" id="cityContainer">
				<select name="selCity" id="selCity">
					<option value="">Todos</option>
				 </select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Responsable:</div>
			<div class="span-5" id="responsibleContainer">
				<select name="selResponsible" id="selResponsible">
					<option value="">Todos</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Comercializador:</div>
			<div class="span-5" id="dealerContainer">
				<select name="selDealer" id="selDealer">
					<option value="">Todos</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Sucursal:</div>
			<div class="span-5" id="branchContainer">
				<select name="selBranch" id="selBranch">
					<option value="">Todos</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Tipo de Venta:</div>
			<div class="span-5">
				<select name="selType" id="selType">
					<option value="netSale"> Neta </option>
					<option value="grossSale"> Bruta </option>
				 </select>
			</div>
		</div>

	<div class="span-24 last line" id="generate">
		<div class="prepend-10 span-5">
			Generar reporte:
			<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF"/>
			<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"/>
		</div>
	{/if}
	</div>
	
    <input type="hidden" name="hdnFlag" id="hdnFlag" value="2" />

</form>
<div class="span-24 last line label" id="emptyResult" style="text-align: center;"></div>