{assign var="title" value="REPORTE DE SUCURSALES"}
<div class="span-24 last line title">
    Reporte de Sucursales<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
</div>

<form name="frmBranchReport" id="frmBranchReport" action="" method="post" target="_blank">
	<div class="span-24 last line">
		<div class="prepend-7 span-5 label">*Seleccione la zona:</div>
		<div class="span-8 last">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$nameZoneList item="l"}
			  <option value="{$l.serial_zon}">{$l.name_zon|htmlall}</option>
			  {/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-5 label">*Seleccione el pa&iacute;s:</div>
		<div class="span-8 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-5 label">Seleccione el representante:</div>
		<div class="span-8 last" id="managerContainer">
			<select name="selManager" id="selManager">
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-5 label">Seleccione el comercializador:</div>
		<div class="span-8 last" id="dealerContainer">
			<select name="selDealer" id="selDealer">
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-5 label">Seleccione el responsable:</div>
		<div class="span-8 last" id="responsibleContainer">
			<select name="selResponsible" id="selResponsible">
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-5 label">Seleccione el Estado:</div>
		<div class="span-8 last" id="statusContainer">
			<select name="selStatus" id="selStatus">
			  
			  <option value="ACTIVE">Activo</option>
			  <option value="INACTIVE">Inactivo</option>
			</select>
		</div>
	</div>

	<div class="span-24 last buttons line">
		<input type="button" name="btnGenerate" id="btnGenerate" class="SCREEN" />
		<input type="button" name="btnGeneratePDF" id="btnGeneratePDF" class="PDF" />
		<input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS" />
	</div>

	<div class="span-24 last line" id="branchContainer"></div>
</form>