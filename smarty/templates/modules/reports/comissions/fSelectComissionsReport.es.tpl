{assign var="title" value="REPORTE DE COMISIONES"}
<form name="frmSelectComissions" id="frmSelectComissions" method="post" target="_blank" action="{$document_root}modules/comissions/pPayComission" >
    <div class="span-24 last title">
    	Reporte de Comisiones<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {else}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

	<div class="span-10 prepend-7 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line separator">
        <label>Datos de la Liquidaci&oacute;n:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label" id="zoneContainer">* Liquidaciones a:</div>
        <div class="span-5 append-7 last">
            <select name="selComissionsTo" id="selComissionsTo" title='El campo "Liquidaciones a" es obligatorio.' class="selectWidht">
                <option value="">- Seleccione -</option>
				<option value="MANAGER">Representante</option>
				<option value="SUBMANAGER">Sub-Representante</option>
				<option value="RESPONSIBLE">Responsable</option>
                <option value="DEALER">Sucursal de Comercializador</option>
            </select>
        </div>
		<input type="hidden" id="report_type" name="report_type"/>
    </div>
	<div class="span-24 last line" id="formContainer"></div>
</form>