{assign var=title value="REPORTE DE COMISIONES PROMEDIO"}
<div class="span-24 last title">
	Reporte de Comisiones Promedio<br />
	<label>(*) Campos Obligatorios</label>
</div>

<div class="span-10 prepend-7 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line separator">
	<label>Datos Geogr&aacute;ficos:</label>
</div>

<form name="frmAverageComissions" id="frmAverageComissions" >
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Zona:</div>
		<div class="span-4">
			<select name="selZone" id="selZone" title="El campo 'Zona' es obligatorio.">
				<option value="">- Seleccione -</option>
				{foreach from=$zone_list item="l"}
					<option value="{$l.serial_zon}">{$l.name_zon}</option>
				{/foreach}
			</select>
		</div>
		<div class="span-4 label">* Pa&iacute;s:</div>
		<div class="span-4 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="prepend-4 span-16 append-4 last line">
		<div class="span-4 label">Representante:</div>
		<div class="span-11" id="managerContainer">
			<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
				<option value="">- Todos -</option>
			</select>
		</div>
	</div>

	<div class="prepend-4 span-16 append-4 last line">
		<div class="span-4 label">Responsable:</div>
		<div class="span-11" id="comissionistContainer">
			<select name="selComissionist" id="selComissionist" title="El campo 'Responsable' es obligatorio.">
				<option value="">- Todos -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">* Fecha Desde:</div>
		<div class="span-5">
			<input type="text" name="txtStartDate" id="txtStartDate" title="El campo 'Fecha Desde' es obligatorio." />
		</div>
		<div class="span-4 label">* Fecha Hasta:</div>
		<div class="span-5 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title="El campo 'Fecha Hasta' es obligatorio." />
		</div>
	</div>

	<div class="span-24 last line buttons">
		<input type="button" name="btnValidate" id="btnValidate" value="Validar" />
		<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
	</div>

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n:</label>
	</div>

	<div class="span-24 last line center" id="infoContainer">
		<i>Seleccione los par&aacute;metros para la b&uacute;squeda.</i>
	</div>
</form>