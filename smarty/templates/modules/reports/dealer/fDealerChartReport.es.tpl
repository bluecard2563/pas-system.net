{assign var="title" value="FICHA DE LA SUCURSAL DE COMERCIALIZADOR"}
<div class="span-24 last line title">
    Ficha de la Sucursal de Comercializador<br />
    <label>(*) Campos Obligatorios</label>
</div>

<form name="frmDealerChart" id="frmDealerChart" action="{$document_root}modules/reports/dealer/pDealerChartReportPDF" method="post" target="_blank">
	<div class="prepend-7 span-10 append-7 last">
		<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Zona:
		</div>
		<div class="span-6 last">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$zoneList item="l"}
			  <option value="{$l.serial_zon}">{$l.name_zon|htmlall}</option>
			  {/foreach}
			</select>
		</div>
		<div class="span-3 label">
			* Pa&iacute;s:
		</div>
		<div class="span-4 append-4 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Representante:
		</div>
		<div class="span-6 last" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
		<div class="span-3 label">
			* Responsable:
		</div>
		<div class="span-4 append-4 last" id="responsibleContainer">
			<select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Comercializador:
		</div>
		<div class="span-6 last" id="dealerContainer">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Sucursal de Comercializador
		</div>
		<div class="span-4 append-4 last" id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			 Fecha Inicio:
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" />
		</div>
		<div class="span-3 label">
			 Fecha Fin:
		</div>
		<div class="span-6 append-2 last">
			<input type="text" name="txtEndDate" id="txtEndDate" />
		</div>
	</div>

	<div class="span-24 last line buttons">
		<input type="submit" id="btnSubmit" name="btnSubmit" value="Consultar" />
		<input type="hidden" id="today" name="today" value="{$today}" />
		<input type="hidden" id="first_day" name="first_day" value="{$first_day}" />
	</div>
</form>