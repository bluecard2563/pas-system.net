{assign var="title" value="ANALISIS DE CARTERA"}
<form name="frmAccountingAnalysis" id="frmAccountingAnalysis" action="" method="post" target="blank">
    <div class="span-24 last line title">
        An&aacute;lisis de Cartera<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
            <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">
            <label>* Zona:</label>
        </div>
        <div class="span-6 last">
            <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
              <option value="">- Seleccione -</option>
              {foreach from=$zone_list item="l"}
              <option value="{$l.serial_zon}">{$l.name_zon}</option>
              {/foreach}
            </select>
        </div>

        <div class="span-3 label">
            <label>* Pa&iacute;s:</label>
        </div>
        <div class="span-4 append-4 last" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
              <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">
            <label>* Representante:</label>
        </div>
        <div class="span-6 last" id="managerContainer">
            <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
              <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">
            <label>Comercializador:</label>
        </div>
        <div class="span-6 last" id="comercializadorContainer">
            <select name="selDealer" id="selDealer">
              <option value="">- Todos -</option>
            </select>
        </div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-3 label">
            <label>Sucursal:</label>
        </div>
        <div class="span-4 append-4 last" id="branchContainer">
            <select name="selBranch" id="selBranch">
              <option value="">- Todos -</option>
            </select>
        </div>
    </div>
			
	<div class="span-24 last line separator">
        <label>Cuentas por Cobrar:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-3 label">
            <label>* CxC hasta:</label>
        </div>
        <div class="span-6 last">
            <input type="text" id="txtCxCFrom" name="txtCxCFrom" title='El campo "CxC hasta" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label>Per&iacute;odo de Ventas/Cobros:</label>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">
            <label>* Transacciones desde:</label>
        </div>
        <div class="span-5 last">
            <input type="text" id="txtBeginDate" name="txtBeginDate" title='El campo "Transacciones desde" es obligatorio.' readonly="" />
        </div>

        <div class="span-4 label">
            <label>* Transacciones hasta:</label>
        </div>
        <div class="span-5 append-3 last">
            <input type="text" id="txtEndDate" name="txtEndDate" title='El campo "Transacciones hasta" es obligatorio.'/>
        </div>
    </div>

    <div class="span-24 last buttons line">
        Generar reporte:
        <input type="button" name="btnGenerateXLS" id="btnGenerateXLS"  class="XLS"  />
    </div>
</form>