{assign var="title" value="REPORTE DE EFECTIVIDAD DE RESPONSABLES"}
<div class="span-24 last line title">
    Reporte de Efectividad de Responsables<br />
    <label>(*) Campos Obligatorios</label><br /><br />
</div>
	{if $error}
		<div class="prepend-6 span-12 last">
			{if $error eq 1}
				<div class="error" align="center">
					Hubo un error en la generaci&oacute;n del archivo!
				</div>
			{/if}
		</div>
	{/if}

{if $countryList}
	<form name="frmDealerEffectiveness" id="frmDealerEffectiveness" action="" method="post" target="_blank">
		<div class="prepend-7 span-10 append-7 last line">
			<ul id="alerts" class="alerts"></ul>
		</div>

		<div class="span-24 last line">
			<div class="prepend-2 span-4 label">
				* Pa&iacute;s:
			</div>
			<div class="span-5">
				<select name="selCountry" id="selCountry" class="span-5" title='El campo "Pa&iacute;s" es obligatorio.'>
				  <option value="">- Seleccione -</option>
				  {foreach from=$countryList item="l"}
				  <option value="{$l.serial_cou}">{$l.name_cou}</option>
				  {/foreach}
				</select>
			</div>
			<div class="prepend-2 span-3 label">
				Ciudad:
			</div>
			<div class="span-5 line" id="cityContainer">
				<select name="selCity" id="selCity" class="span-5">
				  <option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-2 span-4 label">
				Representante:
			</div>
			<div class="span-5" id="managerContainer">
				<select name="selManager" id="selManager" class="span-5">
				  <option value="">- Seleccione -</option>
				</select>
			</div>
			<div class="prepend-2 span-3 label">
				Responsable:
			</div>
			<div class="span-5 line" id="responsibleContainer">
				<select name="selResponsible" id="selResponsible" class="span-5">
				  <option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-2 span-4 label">
				 * Fecha Inicio:
			</div>
			<div class="span-6 last">
				<input type="text" name="txtBeginDate" id="txtBeginDate" class="span-5" title='El campo "Fecha Inicio" es obligatorio.'/>
			</div>
			<div class="prepend-1 span-3 label">
				 * Fecha Fin:
			</div>
			<div class="span-6 line">
				<input type="text" name="txtEndDate" id="txtEndDate" class="span-5" title='El campo "Fecha Fin" es obligatorio.'/>
			</div>
		</div>

		<div class="prepend-10 span-4 last line buttons">
			<input type="button" id="btnSearch" name="btnSearch" value="Buscar" />
		</div>	

	</form>

	<div class="span-24 last line" id="effectivenessTableContainer"></div>
	
{else}
	<div class="prepend-7 span-10 append-7 last line" align="center">
		<div class="span-10 alerts" align="center">
			No se encontraron pa&iacute;ses disponibles.<br/>
		</div>
	</div>
{/if}