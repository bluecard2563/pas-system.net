{assign var="title" value="Reporte de Facturas Pendientes de Pago"}
<div class="span-24 last line title">
    Reporte de Facturas Pendientes de Pago<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form id="frmPendingPayment" name="frmPendingPayment" target="_blank" method="post" action="{$document_root}modules/reports/assistance/pPendingPaymentXLS">
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>* Pago a:</label>
		</div>
		<div class="span-6 last">
			<select name="selPayTo" id="selPayTo" title='El campo "Pago a" es obligatorio.'>
			<option value="">- Seleccione -</option>
			  {foreach from=$docsTo item="l"}
				  {if $l neq 'NONE'}
					<option value="{$l}">{$global_billTo.$l}</option>
				  {/if}
			  {/foreach}
			</select>
		</div>

		<div class="span-3 label">
			<label>* Tipo de Factura:</label>
		</div>
		<div class="span-4 append-4 last">
			<select name="selType" id="selType" title='El campo "Tipo Factura" es obligatorio.'>
			<option value="">- Seleccione -</option>
			  {foreach from=$payTypes key="k" item="tprq"}
				<option value="{$k}">{$tprq}</option>
				{/foreach}
			</select>
		</div>
	</div>
<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>* Fecha Inicio:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio' />
		</div>

		<div class="span-3 label">
			<label>* Fecha Fin:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio' />
		</div>
	</div>
	<div class="span-24 last line">
			<div class="prepend-11 span-5">
				Generar reporte:
				<input type="button" class="PDF" id="btnGeneratePDF" name="btnGeneratePDF">
				<input type="button" class="XLS" id="btnGenerateXLS" name="btnGenerateXLS">
			</div>
		</div>
</form>	