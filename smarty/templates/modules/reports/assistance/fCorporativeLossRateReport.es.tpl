{assign var="title" value="REPORTE DE SINIESTRALIDAD - CORPORATIVAS"}
<div class="span-24 last line title">
    Reporte de Siniestralidad - Corporativas<br />
    <label>(*) Campos Obligatorios</label>
</div>

<form name="frmCorpLossRate" id="frmCorpLossRate" method="post" action="{$document_root}modules/reports/assistance/pCorporativeLossRateReportXLS" target="_blank">
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			* Nombre del cliente:
		</div>
		<div class="span-6">
			<input type="text" class="span-6" name="txtCustomer" id="txtCustomer" title="El campo 'Nombre del cliente' solo admite letras." />
			<input type="hidden" id="hdnSerial_cus" name="hdnSerial_cus"/>
		</div>
	</div>
	
	<div class="span-24 center last line">
		<input type="button" name="btnSearch" id="btnSearch" value="Buscar Tarjetas" />
	</div>
	
	<div class="span-24 last line" id="cardsContainer"></div>
</form>