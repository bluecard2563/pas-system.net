{assign var="title" value="CASOS POR UBICACI&Oacute;N"}
<div class="span-24 last line title">
    Casos por Ubicaci&oacute;n<br />
    <label>(*) Campos Obligatorios</label>
</div>

<form id="frmFilesByLocationXLS" name="frmFilesByLocationXLS" target="_blank" method="post" action="{$document_root}modules/reports/assistance/statistics/pFilesByLocationXLS" >
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Pa&iacute;s:</label>
		</div>
		<div class="span-6 last">
			<select name="selCountry" id="selCountry">
				<option value="">- Seleccione -</option>
				{foreach from=$countries item="c"}
					<option value="{$c.serial_cou}">{$c.name_cou}</option>
				{/foreach}
			</select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Fecha Inicio:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" />
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Fecha Fin:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" />
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Detallado?:</label>
		</div>
		<div class="span-6 last">
			<input type="radio" name="rdDetailed" id="rdDetailed" value="YES" /> Si
			<input type="radio" name="rdDetailed" id="rdDetailed" value="NO" checked="" /> No
		</div>
	</div>
	
	<div class="span-24 last line center">
		<input type="submit" id="btnValidate" value="Buscar Expedientes" />
		<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
	</div>
			
	<div class="span-24 last line" id="detailsContainer"></div>
</form>