{assign var="title" value="INFORME DE ASISTENCIAS"}
<div class="span-24 last line title">
    Informe de Auditor&iacute;a M&eacute;dica<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmMedicalCheckupReport" id="frmMedicalCheckupReport" action="modules/reports/assistance/pMedicalCheckupReportPDF" method="post" target="_blank">
	<div class="prepend-6 span-12 append-6 last">
			<ul id="alerts" class="alerts"></ul>
	</div>


	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			* N&uacute;mero de tarjeta:
		</div>
		<div class="span-5">
			<input type="text" name="txtCard" id="txtCard" title="El campo 'N&uacute;mero de tarjeta' es obligatorio." />
		</div>
	</div>

	<div class="span-24 last buttons line">
		<input type="button" name="btnSearch" id="btnSearch" value="Buscar" class=""  />
	</div>

	<div class="span-24" id="filesInfoContainer"></div>

</form>