{assign var="title" value="Reporte de Casos por Coordinador"}
<div class="span-24 last line title">
    Reporte de Casos por Coordinador<br />
    <label>(*) Campos Obligatorios</label>
</div>

<form id="frmCardFileReport" name="frmFilesBySProviderXLS" target="_blank" method="post" action="{$document_root}modules/reports/assistance/pFilesByServiceProviderXLS" >
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Coordinador:</label>
		</div>
		<div class="span-6 last">
			<select name="selProvider" id="selProvider">
				<option value="">- Seleccione -</option>
				{foreach from=$providers item="p"}
					<option value="{$p.serial_spv}">{$p.name_spv}</option>
				{/foreach}
			</select>
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Fecha Inicio:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" />
		</div>
	</div>
			
	<div class="span-24 last line">
		<div class="prepend-7 span-3 label">
			<label>Fecha Fin:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" />
		</div>
	</div>
	
	<div class="span-24 last line center">
		<input type="button" id="btnValidate" value="Buscar Expedientes" />
		<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
	</div>
			
	<div class="span-24 last line" id="detailsContainer"></div>
</form>