{assign var="title" value="INFORME DE ASISTENCIAS"}
<div class="span-24 last line title">
    Informe de Asistencias<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmAssistanceReport" id="frmAssistanceReport" action="{$document_root}modules/reports/customer/pCustomerReport" method="post" target="_blank">
<div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
</div>
<div class="span-24 last line" {if $show eq 'NO'}style="display: none"{/if}>
    <div class="prepend-4 span-4 label">
        *Zona:
    </div>
    <div class="span-5">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
          <option value="">- Seleccione -</option>
          {foreach from=$zoneList item="l"}
          <option value="{$l.serial_zon}">{$l.name_zon}</option>
          {/foreach}
        </select>
    </div>
	<div class="span-3 label">
        *Pa&iacute;s:
    </div>
    <div class="span-4 append-4 last" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line" id="dealerInfo" {if $show eq 'NO'}style="display: none"{/if}>
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">
			*Ciudad:
		</div>
		 <div class="span-5" id="cityContainer" >
			<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">
			*Comercializador:
		</div>
		<div class="span-4 append-4 last" id="dealerContainer">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	 <div class="span-24 last line">
		<div class="prepend-4 span-4 label">
			*Sucursal:
		</div>
		 <div class="span-5" id="branchContainer" >
			<select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>
</div>
	<div class="span-24 last line" id="assistancesContainer">
		{if $show eq 'NO'}
			    <input type="hidden" name="hdnUser" id="hdnUser" value="{$serial_dea}" />
		{/if}
	</div>
</form>