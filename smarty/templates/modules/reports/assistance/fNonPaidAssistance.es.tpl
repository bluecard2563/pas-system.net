{assign var="title" value="Reporte de Asistencias con Tarjetas Impagas"}
<div class="span-24 last line title">
    REPORTE DE ASISTENCIAS CON TARJETAS IMPAGAS<br />
    <label>(*) Campos Obligatorios</label> <br /><br />
</div>

<form id="frmNonPaidAssistanceReport" name="frmNonPaidAssistanceReport" action="{$document_root}modules/reports/assistance/pNonPaidAssistance" target="_blank" method="post" >
	<div class="span-24 center last line">
		<input type="radio" name="rdFilter" id="filter_geographic" value="GEOGRAPHIC" checked="checked" /> Ubicaci&oacute;n
		<input type="radio" name="rdFilter" id="filter_card" value="CARD" /> N&uacute;mero de Tarjeta		
	</div>
	
	<div class="span-24 last" id="geographicFilter">
		<div class="span-24 last line">
			<div class="prepend-6 span-4 label">
				<label>Pa&iacute;s:</label>
			</div>
			<div class="span-6 last">
				<select name="selCountry" id="selCountry" class="span-5">
				  <option value="">- Todos -</option>
				  {foreach from=$country_list item="l"}
				  <option value="{$l.serial_cou}">
					  {if $l.name_cou eq "Todos"}
						Ventas Web
					  {else}
						{$l.name_cou}
					  {/if}
				  </option>
				  {/foreach}
				</select>
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-6 span-4 label">
				<label>Representante:</label>
			</div>
			<div class="span-6 last" id="managerContainer">
				<select name="selManager" id="selManager" class="span-5">
				  <option value="">- Todos -</option>
				</select>
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-6 span-4 label">
				<label>Responsable:</label>
			</div>
			<div class="span-6 last" id="responsibleContainer">
				<select name="selResponsable" id="selResponsable" class="span-5">
				  <option value="">- Todos -</option>
				</select>
			</div>
		</div>
		
		<div class="span-24 last line">
			<div class="prepend-6 span-4 label">
				<label>Comercializador</label>
			</div>
			<div class="span-6 last" id="dealerContainer">
				<select name="selDealer" id="selDealer" class="span-5">
				  <option value="">- Todos -</option>
				</select>
			</div>
		</div>
	</div>
	
	<div class="span-24 last line" id="cardNumberFilter">
		<div class="prepend-6 span-4 label">
			<label>* No. de Tarjeta:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtCardNumber" id="txtCardNumber" title="El campo 'No. de Tarjeta' es obligatorio" />
		</div>
	</div>
	
	<div class="span-24 last buttons line">
		<input type="button" name="btnValidate" id="btnValidate" value="Validar" />
	</div>
	
	<div class="span-24 last line" id="filesContainer"></div>
</form>