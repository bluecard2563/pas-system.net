{assign var="title" value="ALERTAS DE ASISTENCIAS"}
<div class="span-24 last line title">
    Alertas de Asistencias<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmAssistanceAlertReport" id="frmAssistanceAlertReport" method="post" action="#" target="_blank">
	<div class="span-24 line">
		<div class="prepend-8 span-5 last">
            <label for="radio_GroupAssistance">Por Grupo de Asistencia:</label>
            <input type="radio" id="radioGroupAssistance" name="radioChoice" value="group" checked/>
        </div>
		<div class="span-5 line">
            <label for="radio_OwnerAlert">Por Creador de Alerta:</label>
            <input type="radio" id="radioOwnerAlert" name="radioChoice" value="owner"/>
		</div>
	</div>

	<div style="display: block" id="assistanceGroupDiv" class="span-24">
		<div class="prepend-7 span-4 label">
			*Grupo de Asistencia:
		</div>
		 <div class="span-5">
			<select name="assistanceGroup" id="assistanceGroup" title='El campo "Grupo de Asistencia" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$assistanceGroupList item="asg"}
				<option value="{$asg.serial_asg}">{$asg.name_asg}</option>
			  {/foreach}
			</select>
		</div>
			<br><br>
	</div>

	<div style="display: none" id="alertOwnerDiv" class="span-24">
		<div class="prepend-7 span-4 label">
			*Creador de Alerta:
		</div>
		 <div class="span-5">
			<select name="alertOwner" id="alertOwner" title='El campo "Creador de Alerta" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$ownerAlertList item="ao"}
				<option value="{$ao.serial_usr}">{$ao.name_usr}</option>
			  {/foreach}
			</select>
		</div>
			<br><br>
	</div>

	<div class="span-24 line">

		<div class="prepend-8 span-5">
            <label for="radio_createDate">Fecha de Creaci&oacute;n :</label>
            <input type="radio" id="radioCreateDate" name="radioChoiceDate" value="create" checked/>
        </div>
		<div class="span-6 line">
            <label for="radio_servedDate">Fecha M&aacute;xima de Atenci&oacute;n :</label>
            <input type="radio" id="radioServedDate" name="radioChoiceDate" value="served"/>
		</div>
	</div>



	<div class="span-24 line">

		<label class="prepend-6" for="from">*Fecha Inicio</label>
		<input readonly type="text" id="from" name="from" title="El campo 'Fecha Inicio' es obligatorio"/>
		<label class="prepend-1" for="to">*Fecha Fin</label>
		<input readonly type="text" id="to" name="to" title="El campo 'Fecha Fin' es obligatorio"/>

	</div>


	<div class="span-24 last line center">
			Generar reporte:
		<input type="button" name="btnGeneratePDF" id="btnGenerate_PDF"  class="PDF" />
	</div>
</form>