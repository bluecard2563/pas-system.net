{assign var="title" value="Reporte de Status"}
<div class="span-24 last line title">
    Reporte de Status<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $error}
	<div class="append-7 span-10 prepend-7 last">
		<div class="span-10 error" align="center">
			No hay datos para los filtros seleccionados.
		</div>
	</div>
{/if}

<form id="frmFileLog" name="frmFileLog" target="_blank" method="post" action="{$document_root}modules/reports/assistance/pFileLogXLS">
	<div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>* Fecha Inicio:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio' />
		</div>

		<div class="span-3 label">
			<label>* Fecha Fin:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio' />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>Nro. Expediente:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtFileNro" id="txtFileNro" title='El campo "Nro. Expediente" es obligatorio' />
		</div>
	</div>
	
	<div class="span-24 last buttons line">
		<img src="{$document_root}img/page_excel.png" class="link_item" border="0" id="imgSubmitXLS">
		<input type="hidden" name="today" id="today" value="{$today}" />
	</div>
</form>