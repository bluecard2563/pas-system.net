{assign var="title" value="Detalle de Facturas de Asistencias"}
<div class="span-24 last line title">
    Reporte de Pagos Asistencia<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form id="frmAssistancePayment" name="frmAssistancePayment" target="_blank" method="post" >
	<!-- Start select Pay to -->
	<div class="span-24 last line">
            <div class="prepend-4 span-3 label">
			<label>* Tipo de Transacci&oacute;n:</label>
		</div>
		<div class="span-6 last">
			<select name="selProcessAs" id="selProcessAs" title='El campo "Tipo de Transacci&o&oacute;n" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$global_process_as key="key" item="value"}
					{if $key neq 'MIXED'}
						<option value="{$key}">{$value}</option>
					{/if}
				{/foreach}
			</select>
		</div>
                        
		<div class="span-3 label">
			<label>* Pago a:</label>
		</div>
		<div class="span-6 last">
			<select name="selPayTo" id="selPayTo" title='El campo "Pago a" es obligatorio.'>
				<option value="">- Seleccione -</option>
				{foreach from=$docsTo item="l"}
				{if $l neq 'NONE'}
				<option value="{$l}">{$global_billTo.$l}</option>
				{/if}
				{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line" id="datesContainer">
		<div class="prepend-4 span-3 label">
			<label>* Fecha Inicio:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio' />
		</div>

		<div class="span-3 label">
			<label>* Fecha Fin:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio' />
		</div>
	</div>

	<!-- Start Div Provider *coordinador container -->
	<div class="span-24 last line" id="providerContainer" style="display: none;">
        <div class="prepend-8 span-3 label">
            <label>* Coordinador:</label>
        </div>
        <div class="span-8 last">
            <select name="selProvider" id="selProvider" title='El campo "Coordinador:" es obligatorio.'>
				<option value="">- Todos -</option>
				{foreach from=$providerList item="l"}
				<option value="{$l.serial_spv}">{$l.name_spv}</option>
				{/foreach}
            </select>
        </div>
    </div>

	<!-- Start Div Client container -->
    <div class="span-24 last line" id="clientContainer" style="display: none;">
        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Zona:</div>
            <div class="span-6" id="zoneContainer">
                <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
					<option value="">-Seleccione-</option>
					{foreach from=$zoneList item="l"}
					<option value="{$l.serial_zon}">{$l.name_zon}</option>
					{/foreach}
                </select>
            </div>

            <div class="span-3 label">* Pa&iacute;s:</div>
            <div class="span-4 append-4 last" id="countryContainer">
                <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">Ciudad:</div>
            <div class="span-6" id="cityContainer">
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>
	</div>

	<div class="span-24 last line buttons">
		<input type="button" name="btnRegistrar" id="btnRegistrar" value="Verificar Datos"  />
		<input type="hidden" name="hdnToday" id="hdnToday" value="{$today}" />
    </div>

	<div class="span-24 last line" id="data_container"></div>
</form>	