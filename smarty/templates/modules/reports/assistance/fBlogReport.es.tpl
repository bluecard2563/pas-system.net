{assign var="title" value="INFORME DE ASISTENCIAS"}
<div class="span-24 last line title">
    Informe de Bit&aacute;cora<br />
    <label>(*) Campos Obligatorios</label>
</div>
<form name="frmBlogReport" id="frmBlogReport" action="modules/reports/assistance/pAssistanceReport" method="post" target="_blank">
	<div class="prepend-6 span-12 append-6 last">
			<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line">
		 <div class="prepend-9 span-12">
			Ingrese uno de los campos de b&uacute;squeda.
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			* Nombre del cliente:
		</div>
		<div class="span-5">
			<input type="text" name="txtCustomer" id="txtCustomer" title="El campo 'Nombre del cliente' solo admite letras." />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			* N&uacute;mero de tarjeta:
		</div>
		<div class="span-5">
			<input type="text" name="txtCard" id="txtCard" title="El campo 'N&uacute;mero de tarjeta' solo admite n&uacute;meros." />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">
			* N&uacute;mero de expediente:
		</div>
		<div class="span-5">
			<input type="text" name="txtFile" id="txtFile" title="El campo 'N&uacute;mero de expediente' solo admite n&uacute;meros." />
		</div>
	</div>

	<div class="span-24 last buttons line">
		<input type="hidden" name="hdnControl" id="hdnControl" value="" title="Ingrese uno de los campos de b&uacute;squeda." />
		<input type="button" name="btnSearch" id="btnSearch" value="Buscar" class=""  />
	</div>

	<div class="span-24" id="filesInfoContainer"></div>

</form>