{assign var="title" value="INFORME DE COMERCIALIZADOR"}
<div class="span-24 last line title">
    Informe de Comercializador
</div>

<form name="frmDealerReport" id="frmDealerReport" action="{$document_root}modules/reports/assistance/fDealerReport.php" method="post">

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n Geogr&aacute;fica:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Pa&iacute;s:</div>
		<div class="span-4">{$data.name_cou}</div>
		<div class="span-4 label">* Ciudad:</div>
		<div class="span-4 append-3 last">{$data.name_cit}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Sector:</div>
		<div class="span-4">{$data.name_sec}</div>
		<div class="span-4 label">* Representante:</div>
		<div class="span-4 append-3 last">{$data.name_man}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">C&oacute;digo:</div>
		<div class="span-4">{$data.code_dea}</div>
		<div class="span-4 label">Comercializador Oficial:</div>
		<div class="span-4 append-3 last">{$global_yes_no[$data.official_seller_dea]}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Responsable:</div>
		<div class="span-4">{$data.name_usr}</div>
	</div>

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n General:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Id:</div>
		<div class="span-4">{if $data.id_dea}{$data.id_dea}{else}-{/if}</div>
		<div class="span-4 label">Nombre:</div>
		<div class="span-4 append-3 last">{if $data.name_dea}{$data.name_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Tipo de Comercializador:</div>
		<div class="span-4">{if $data.description_dtl}{$data.description_dtl}{else}-{/if}</div>
		<div class="span-4 label">Categor&iacute;a:</div>
		<div class="span-4 append-3 last">{if $data.category_dea}{$data.category_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Direcci&oacute;n:</div>
		<div class="span-4">{if $data.address_dea}{$data.address_dea}{else}-{/if}</div>
		<div class="span-4 label">Estado:</div>
		<div class="span-4 append-3 last">{if $data.status_dea}{$global_status[$data.status_dea]}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Tel&eacute;fono #1:</div>
		<div class="span-4">{if $data.phone1_dea}{$data.phone1_dea}{else}-{/if}</div>
		<div class="span-4 label">Tel&eacute;fono #2:</div>
		<div class="span-4 append-3 last">{if $data.phone2_dea}{$data.phone2_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Nombre del Contacto:</div>
		<div class="span-4">{if $data.contact_dea}{$data.contact_dea}{else}-{/if}</div>
		<div class="span-4 label">Tel&eacute;fono del Contacto:</div>
		<div class="span-4 append-3 last">{if $data.phone2_dea}{$data.phone2_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">E-mail del Contacto:</div>
		<div class="span-4">{if $data.email_contact_dea}{$data.email_contact_dea}{else}-{/if}</div>
		<div class="span-4 label">N&uacute;mero de visitas al mes:</div>
		<div class="span-4 append-3 last">{if $data.visits_number_dea}{$data.visits_number_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n del Gerente:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Nombre del Gerente:</div>
		<div class="span-4">{if $data.manager_name_dea}{$data.manager_name_dea}{else}-{/if}</div>
		<div class="span-4 label">Tel&eacute;fono del Gerente:</div>
		<div class="span-4 append-3 last">{if $data.manager_phone_dea}{$data.manager_phone_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">E-mail del Gerente:</div>
		<div class="span-4">{if $data.manager_email_dea}{$data.manager_email_dea}{else}-{/if}</div>
		<div class="span-4 label">Fecha de Nacimiento:</div>
		<div class="span-4 append-3 last">{if $data.manager_birthday_dea}{$data.manager_birthday_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n de Pagos:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Contacto Pago:</div>
		<div class="span-4">{if $data.pay_contact_dea}{$data.pay_contact_dea}{else}-{/if}</div>
		<div class="span-4 label">Factura a nombre del:</div>
		<div class="span-4 append-3 last">{if $data.bill_to_dea}{$global_billTo[$data.bill_to_dea]}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">D&iacute;a de Pago:</div>
		<div class="span-4">{if $data.payment_deadline_dea}{$global_weekDays[$data.payment_deadline_dea]}{else}-{/if}</div>
		<div class="span-4 label">D&iacute;as de Cr&eacute;dito:</div>
		<div class="span-4 append-3 last">{if $data.days_cdd}{$data.days_cdd}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Porcentaje:</div>
		<div class="span-4">{if $data.percentage_dea}{$data.percentage_dea}{else}-{/if}</div>
		<div class="span-4 label">Tipo:</div>
		<div class="span-4 append-3 last">{if $data.type_percentage_dea}{$global_pType[$data.type_percentage_dea]}{else}-{/if}</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Insentivo a:</div>
		<div class="span-4">{if $data.bonus_to_dea}{$global_billTo[$data.bonus_to_dea]}{else}-{/if}</div>
	</div>

	<div class="span-24 last line separator">
		<label>Informaci&oacute;n de Asistencias:</label>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Nombre:</div>
		<div class="span-4">{if $data.assistance_contact_dea}{$data.assistance_contact_dea}{else}-{/if}</div>
		<div class="span-4 label">E-mail:</div>
		<div class="span-4 append-3 last">{if $data.email_assistance_dea}{$data.email_assistance_dea}{else}-{/if}</div>
	</div>

	<div class="span-24 last buttons line">
		<input type="submit" name="btnBack" id="btnBack" value="Aceptar" />
	</div>
</form>