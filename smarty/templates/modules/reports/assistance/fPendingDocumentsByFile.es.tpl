{assign var="title" value="Reporte de Documentos Pendientes por Recibir"}
<div class="span-24 last line title">
    REPORTE DE DOCUMENTOS PENDIENTES POR RECIBIR<br />
    <label>(*) Campos Obligatorios</label> <br /><br />
</div>

<form id="frmOpenedFilesReport" name="frmOpenedFilesReport" action="{$document_root}modules/reports/assistance/pPendingDocumentsByFile" target="_blank" method="post" >
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			<label>Pa&iacute;s:</label>
		</div>
		<div class="span-6 last">
			<select name="selCountry" id="selCountry" class="span-5">
			  <option value="">- Todos -</option>
			  {foreach from=$country_list item="l"}
			  <option value="{$l.serial_cou}">
				  {if $l.name_cou eq "Todos"}
					Ventas Web
				  {else}
					{$l.name_cou}
				  {/if}
			  </option>
			  {/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last buttons line">
		<img src="{$document_root}img/page_excel.png" class="link_item" border="0" id="imgSubmitXLS" />
	</div>
</form>