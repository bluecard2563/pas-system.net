{assign var="title" value="Reporte de Casos"}
<div class="span-24 last line title">
    Reporte de Casos<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form id="frmAssistanceLossRate" name="frmAssistanceLossRate" target="_blank" method="post">
	<div class="span-24 last line separator">
        <label>Informaci&oacute;n Geogr&aacute;fica:</label>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>* Zona:</label>
		</div>
		<div class="span-6 last">
			<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$zone_list item="l"}
			  <option value="{$l.serial_zon}">{$l.name_zon}</option>
			  {/foreach}
			</select>
		</div>

		<div class="span-3 label">
			<label>Pa&iacute;s:</label>
		</div>
		<div class="span-4 append-4 last" id="countryContainer">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			Representante:
		</div>
		<div class="span-6" id="managerContiner">
			<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			Responsable:
		</div>
		<div class="span-6" id="comissionistsContainer">
			<select name="selComissionist" id="selComissionist" title='El campo "Responsable" es obligatorio.'>
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			Comercializador:
		</div>
		<div class="span-6" id="dealerContiner">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			Sucursal:
		</div>
		<div class="span-6" id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
			  <option value="">- Todos -</option>
			</select>
		</div>
	</div>
	
	<div class="span-24 last line separator">
        <label>Informaci&oacute;n de la Venta:</label>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>* Fecha Inicio:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtBeginDate" id="txtBeginDate" title='El campo "Fecha Inicio" es obligatorio' />
		</div>

		<div class="span-3 label">
			<label>* Fecha Fin:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtEndDate" id="txtEndDate" title='El campo "Fecha Fin" es obligatorio' />
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>Aplicar Filtro de Fechas a:</label>
		</div>
		<div class="span-6 last">
			<input type="radio" value="in_date_sal" name="rdDateFilter" id="rdDateFilter_IN" checked="" /> Ingreso de la Tarjeta <br />
			<input type="radio" value="incident_date_fle" name="rdDateFilter" id="rdDateFilter_FILE" /> Ocurrencia del Expediente
		</div>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>Producto:</label>
		</div>
		<div class="span-6 last" id="productContainer">
			<select name="selProduct" id="selProduct">
			<option value="">- Todos -</option>
			</select>
		</div>
    </div>
    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de Casos:</label>
    </div>
    <div class="span-24 last line">
		<div class="prepend-4 span-3 label">
			<label>Tipo de ASistencia:</label>
		</div>
		<div class="span-6 last">
            <select multiple name="selTypeFle[]" id="selTypeFle" title='El campo "Zona" es obligatorio.'>
			  {foreach from=$fileTypes item="ft"}
			  <option value="{$ft}">{$global_fileTypes.$ft}</option>
			  {/foreach}
			</select>
		</div>

		<div class="span-3 label">
			<label>Estado de Caso:</label>
		</div>
		<div class="span-4 append-4 last" id="countryContainer">
			<select multiple name="selStatusFle[]" id="selStatusFle" title='El campo "Pa&iacute;s" es obligatorio.'>
			  {foreach from=$fileStatus item="fs"}
			  <option value="{$fs}">{$global_fileStatus.$fs}</option>
			  {/foreach}
			</select>
		</div>
	</div>


	<div class="span-24 last buttons line">
		<img src="{$document_root}img/page_excel.png" class="link_item" border="0" id="imgSubmitXLS">
		<input type="hidden" name="today" id="today" value="{$today}" />
		<input type="hidden" name="first_day" id="first_day" value="{$first_day}" />
	</div>
</form>