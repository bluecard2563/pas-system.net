{assign var="title" value="INFORME DE COMERCIALIZADOR"}
<div class="span-24 last line title">
    Informe de Comercializador<br />
    <label>(*) Campos Obligatorios</label>
</div>

<form name="frmDealerReport" id="frmDealerReport" action="{$document_root}modules/reports/assistance/fDealerReportInfo" method="post">
	<div class="prepend-7 span-10 append-7 last line">
			<ul id="alerts" class="alerts"></ul>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">
			* Pa&iacute;s:
		</div>
		<div class="span-5">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			  {foreach from=$countryList item="l"}
			  <option value="{$l.serial_cou}">{$l.name_cou}</option>
			  {/foreach}
			</select>
		</div>
		<div class="span-3 label">
			* Ciudad:
		</div>
		<div class="span-4 append-4 last" id="cityContainer">
			<select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Representante:</div>
		<div class="span-5" id="managerContainer">
			<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
		<div class="span-3 label">* Responsable:</div>
		<div class="span-4 append-4 last" id="comissionistContainer">
			<select name="selComissionist" id="selComissionist" title='El campo "Responsable:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Comercializador:</div>
		<div class="span-5" id="dealerContainer">
			<select name="selDealer" id="selDealer" title='El campo "Comercializador:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">* Sucursal:</div>
		<div class="span-5" id="branchContainer">
			<select name="selBranch" id="selBranch" title='El campo "Sucursal:" es obligatorio.'>
			  <option value="">- Seleccione -</option>
			</select>
		</div>
	</div>

	<div class="span-24 last buttons line">
		<input type="submit" name="btnSearch" id="btnSearch" value="Ver Informe" class=""  />
	</div>
</form>