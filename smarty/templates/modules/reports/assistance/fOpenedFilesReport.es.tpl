{assign var="title" value="Reporte de Apertura de Expedientes"}
<div class="span-24 last line title">
    Reporte de Apertura de Expedientes<br />
    <label>(*) Campos Obligatorios</label> <br /><br />
</div>

<div class="prepend-7 span-10 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form id="frmOpenedFilesReport" name="frmOpenedFilesReport" target="_blank" method="post" >
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			<label>* Grupo de Asistencia:</label>
		</div>
		<div class="span-6 last">
			<select name="selGroup" id="selGroup" title='El campo "Grupo de Asistencia" es obligatorio.' class="span-5">
			  <option value="">- Seleccione -</option>
			  {foreach from=$assistance_groups item="l"}
			  <option value="{$l.serial_asg}">{$l.name_asg}</option>
			  {/foreach}
			</select>
		</div>
	</div>
	
	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">
			<label>Fecha:</label>
		</div>
		<div class="span-6 last">
			<input type="text" name="txtDateFilter" id="txtDateFilter" />
		</div>
	</div>
	
	<div class="span-24 last line center">
		<label style="font-style: italic; font-size: 9px;">
			Si no se ingresa la fecha de an&aacute;lisis, <br />
			se obtendr&aacute;n los registros del d&iacute;a de hoy.
		</label><br />
	</div>
	
	<div class="span-24 last buttons line">
		<img src="{$document_root}img/page_excel.png" class="link_item" border="0" id="imgSubmitXLS">
		<img src="{$document_root}img/file_acrobat.gif" class="link_item" border="0" id="imgSubmitPDF">
		<input type="hidden" name="today" id="today" value="{$today}" />
		<input type="hidden" name="first_day" id="first_day" value="{$first_day}" />
	</div>
</form>