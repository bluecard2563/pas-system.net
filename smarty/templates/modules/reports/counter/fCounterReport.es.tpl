{assign var="title" value="REPORTE DE COUNTERS"}
<div class="span-24 last line title">
    Reporte de Counters<br />
    <label>(*) Campos Obligatorios</label>
</div>

<div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione la zona:</label>
    </div>
    <div class="span-8 last">
        <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
          <option value="">- Seleccione -</option>
          {foreach from=$nameZoneList item="l"}
          <option value="{$l.serial_zon}">{$l.name_zon}</option>
          {/foreach}
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione el pa&iacute;s:</label>
    </div>
    <div class="span-8 last" id="countryContainer">
        <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione el representante:</label>
    </div>
    <div class="span-8 last" id="managerContainer">
        <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione el comercializador:</label>
    </div>
    <div class="span-8 last" id="dealerContainer">
        <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last line">
    <div class="prepend-8 span-5">
        <label>*Seleccione la sucursal:</label>
    </div>
    <div class="span-8 last" id="branchContainer">
        <select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
          <option value="">- Seleccione -</option>
        </select>
    </div>
</div>

<div class="span-24 last buttons line">
    <input type="button" name="btnGenerate" id="btnGenerate" value="Generar Reporte" class=""  />
</div>

<div class="span-24 last line" id="counterContainer"></div>