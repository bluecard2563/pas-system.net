{assign var="title" value="REIMPRESI&Oacute;N DE NOTA DE CR&Eacute;DITO CON ANULACI&Oacute;N"}
<div class="span-24 last title">
	 Reimpres&iacute;on de Nota de Cr&eacute;dito con Anulaci&oacute;n<br />
	<label>(*) Campos Obligatorios</label>
</div>
<form name="frmPrintCNote" id="frmPrintCNote" method="post" action="{$document_root}modules/creditNote/pPrintCreditNote" target="_blank" >
<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>
	
{if $error}
<div class="span-7 span-10 prepend-7 last">
	<div class="error span-10">
		{if $error eq 1}
			La nota de cr&eacute;dito ingresada ya fue reimpresa!
		{/if}
	</div>
</div>
{/if}

<div class="span-24 last line separator">
	<label>Datos de la Nota de Cr&eacute;dito</label>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">
		* Pa&iacute;s de Emisi&oacute;n:
	</div>
	<div class="span-5 appen-8 last">
		<select name="selCountry" id="selCountry" title="El campo 'Pais de Emision' es Obligatorio">
			<option value="">- Seleccione -</option>
			{foreach from=$countryList item="c"}
			<option value="{$c.serial_cou}">{$c.name_cou}</option>
			{/foreach}
		</select>
	</div>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">
		* Representante:
	</div>
	<div class="span-5 appen-8 last" id="managerContainer">
		<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio.">
			<option value="">- Seleccione -</option>
		</select>
	</div>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">
		* Nota de Cr&eacute;dito por:
	</div>
	<div class="span-5 appen-8 last">
		<input type="radio" name="rdOrigin" id="rdOrigin_0" value="INVOICE" /> Anulaci&oacute;n de Factura <br/>
		<input type="radio" name="rdOrigin" id="rdOrigin_1" value="REFUND" /> Reembolso <br/>
		<input type="radio" name="rdOrigin" id="rdOrigin_1" value="PENALTY" /> Penalidad
		<input type="hidden"  name="hdnType" id="hdnType" value="" />
	</div>
</div>

<div class="span-24 last line">
	<div class="prepend-7 span-5 label">
		* N&uacute;mero de la Nota de Cr&eacute;dito:
	</div>
	<div class="span-5 appen-8 last">
		<input type="text" id="txtCreditNote" name="txtCardNumber" />
	</div>
</div>

<div class="span-24 last buttons line">
	<input type="button" name="btnSearch" id="btnSearch" value="Buscar" >
</div>

<div class="span-24 last line" id="creditNoteContainer"></div>
</form>
