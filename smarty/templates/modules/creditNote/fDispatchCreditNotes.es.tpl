{assign var="title" value="DESPACHO DE NOTAS DE CR&Eacute;DITO"}
<form name="frmDispatchCreditNotes" id="frmDispatchCreditNotes" method="post" action="{$document_root}modules/creditNote/pDispatchCreditNotes" class="">
	<div class="span-24 last title ">
    	Despacho de Notas de Cr&eacute;dito<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	Las notas de cr&eacute;dito se han despachado exitosamente exitosamente!
        {elseif $error eq 2}
            	No se pudo reasignar los comercializadores.
        {/if}
        </div>
    </div>
    {/if}

   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
    <div class="span-24 last line"></div>
    {if $zoneList}
   <div class="span-24 last line">
        <div class="prepend-4 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZone" id="selZone" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryContainer">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

     <div class="span-24 last line">
     	<div class="prepend-4 span-4 label">*Ciudad:</div>
        <div class="span-5" id="cityContainer">
            <select name="selCity" id="selCity" title='El campo "Representante Origen" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>
        <div class="span-3 label">* Notas por:</div>
        <div class="span-4 append-4 last">
        	<select name="selNotesFor" id="selNotesFor" title='El campo "Notas por" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                <option value="CUSTOMER">Cliente</option>
                <option value="DEALER">Comercializador</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line"></div>
    <div class="span-24 last line" id="dealerContainer" style="display: none">
        <div class="prepend-4 span-4 label">*Comercializador:</div>
        <div class="span-5 last line">
            <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line" id="customerContainer" style="display: none">
        <div class="prepend-4 span-4 label">*Cliente:</div>
        <div class="span-5 append-11 last line">
            <select name="selCustomer" id="selCustomer" title='El campo "Cliente" es obligatorio.' >
                <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    <div class="span-24 last line" id="creditNotesContainer">
    </div>
    {else}
    <div class="prepend-8 span-9 append-4 last">No existen Notas de Cr&eacute;dito por despachar.</div>
    {/if}

</form>
