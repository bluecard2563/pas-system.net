{assign var="title" value="BUSCAR PREMIO"}
<form name="frmSearchPrize" id="frmSearchPrize" action="{$document_root}modules/prize/fUpdatePrize" method="post" class="form_smarty">
	<div class="span-24 last title">Buscar Premio</div>
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El premio ingresado no existe.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="prepend-3 span-19 append-2 last line"></div>
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-4 span-4 label">* Tipo:</div>
        <div class="span-5"> 
        <select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item="l"}
                <option value="{$l}">{$global_prizeTypes.$l}</option>
                {/foreach}
        </select>
        </div>
    </div>
    <div class="span-24 last line" id="bonusContainer" style="display: none;">
        <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-4 span-4 label">* Zona:</div>
            <div class="span-5">
                <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$zoneList item="l"}
                        <option value="{$l.serial_zon}">{$l.name_zon}</option>
                        {/foreach}
                </select>
            </div>            
        </div>
        <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-4 span-4 label">* Pa&iacute;s:</div>
            <div class="span-5" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
            </select>
            </div>
        </div>

    </div>
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-4 span-4 label">* Premio:</div>
        <div class="span-5" id="prizeContainer">
            <select name="selPrize" id="selPrize" title='El campo "Premio" es obligatorio'>
                    <option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line"></div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Editar" >
    </div>
    <input type="hidden" name="hdnPrizeID" id="hdnPrizeID" value="" />
</form>