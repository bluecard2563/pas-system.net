<!--/*
File: fNewPrize.tpl
Author: Nicolas Flores
Creation Date: 04/01/2010
Last Modified: 12/01/2010 08:47
Modified by: Santiago Ben�tez
*/-->
{assign var="title" value="NUEVO PREMIO"} 
<form name="frmNewPrize" id="frmNewPrize" method="post" action="{$document_root}modules/prize/pNewPrize">
	<div class="span-24 last title">
    	Registro de Nuevo Premio<br>
          <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last ">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            El premio se ha registrado exitosamente!
        {else}
            Hubo errores en el ingreso. Por favor vuelva a intentarlo.
        {/if}
        </div>
    </div>
    {/if}
    
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.'> </div>
        
        <div class="span-3 label">* Costo:</div>
        <div class="span-4 append-2 last">  <input type="text" name="txtValue" id="txtValue" title='El campo "Valor" es obligatorio'> </div>
	</div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">* Stock:</div>
        <div class="span-5"> <input type="text" name="txtStock" id="txtStock" title='El campo "Stock" es obligatorio'> </div>
        <div class="span-3 label">*Tipo:</div>
        <div class="span-5">
        <select name="selType" id="selType" title='El campo "Tipo" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item="l"}
                <option value="{$l}">{$global_prizeTypes.$l}</option>
                {/foreach}
        </select>
        </div>
    </div>
    <div class="span-24 last line" id="bonusContainer" style="display: none;">
        <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-4 label">* Zona:</div>
            <div class="span-5">
                <select name="selZone" id="selZone" title='El campo "Zona" es obligatorio'>
                        <option value="">- Seleccione -</option>
                        {foreach from=$zoneList item="l"}
                        <option value="{$l.serial_zon}">{$l.name_zon}</option>
                        {/foreach}
                </select> 
            </div>
            <div class="span-3 label">* Pa&iacute;s:</div>
            <div class="span-5" id="countryContainer">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
            </select>
            </div>
        </div>
        <div class="prepend-3 span-19 append-2 last line">
            <div class="prepend-1 span-4 label">* Monto:</div>
            <div class="span-5"> <input type="text" name="txtAmount" id="txtAmount" title='El campo "Monto" es obligatorio'> </div>
            <div class="span-3 label">* Cobertura:</div>
            <div class="span-5">
            <select name="selCoverage" id="selCoverage" title='El campo "Cobertura" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$coverageList item="l"}
                        <option value="{$l}">{$global_prizeCoverages.$l}</option>
                    {/foreach}
            </select>
            </div>
        </div>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label"> Producto {$global_system_name}</div>
        <div class="span-5"> <input type="checkbox" name="chkProduct" id="chkProduct" value="1">  </div>
        <div id="productContainer" style="display: none;">
            <div class="span-3 label">*Productos:</div>
            <div class="span-5">
            <select name="selProduct" id="selProduct" title='El campo "Productos" es obligatorio'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$productList item="l"}
                        <option value="{$l.serial_pro}" {if $productList|@COUNT eq 1}selected{/if}>{$l.name_pbl}</option>
                    {/foreach}
            </select>
            </div>
        </div>        
    </div>
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
    <input type="hidden" name="hdnNamePrize" id="hdnNamePrize" value="1" />
</form>