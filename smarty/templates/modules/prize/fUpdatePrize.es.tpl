{assign var="title" value="ACTUALIZAR PREMIO"}
<div class="span-24 last title">
    Actualizar Premio<br>
      <label>(*) Campos Obligatorios</label>
</div>
{if $data}
<form name="frmUpdatePrize" id="frmUpdatePrize" method="post" action="{$document_root}modules/prize/pUpdatePrize" class="">    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
        <div class="span-10 error" align="center">
            No se pudo actualizar el Premio.
        </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">* Nombre:</div>
        <div class="span-5">  <input type="text" name="txtName" id="txtName" title='El campo "Nombre" es obligatorio.' value="{$data.name_pri}"> </div>
        
        <div class="span-3 label">* Costo:</div>
        <div class="span-4 append-2 last">  <input type="text" name="txtValue" id="txtValue" title='El campo "Valor" es obligatorio' value="{$data.value_pri}"> </div>
	</div>
    
    <div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">* Stock:</div>
        <div class="span-5"> <input type="text" name="txtStock" id="txtStock" title='El campo "Stock" es obligatorio' value="{$data.stock_pri}"> </div>
        
        <div class="span-3 label">* Estado:</div>
    	<div class="span-4 last"> 
        	<select name="selStatus" id="selStatus" title="El campo 'Estado' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item=sl}
                	<option value="{$sl}" {if $data.status_pri eq $sl}selected="selected"{/if}>{if $sl eq 'ACTIVE'}Activo{elseif $sl eq 'INACTIVE'}Inactivo{/if}</option>
                {/foreach}
            </select>
        </div>
	</div>  
			
	<div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">Monto Base:</div>
        <div class="span-5"> {$data.amount_pri} </div>
	</div>  
    
    <div class="prepend-3 span-19 append-2 last line" {if !$data.serial_pro}style="display: none;"{/if}>
        <div class="prepend-1 span-4 label"> Producto {$global_system_name}</div>
        <div class="span-5"> <input type="checkbox" name="chkProduct" id="chkProduct" value="1" disabled {if $data.serial_pro}checked{/if}>  </div>
        <div id="productContainer" {if !$data.serial_pro}style="display: none;"{/if}>
            <div class="span-3 label">*Productos:</div>
            <div class="span-5">
            <select name="selProduct" id="selProduct" title='El campo "Productos" es obligatorio' disabled>
                    <option value="">- Seleccione -</option>
                    {foreach from=$productList item="l"}
                        <option value="{$l.serial_pro}" {if $productList|@COUNT eq 1 or $l.serial_pro eq $data.serial_pro}selected{/if}>{$l.name_pbl}</option>
                    {/foreach}
            </select>
            </div>
        </div>
    </div>
           
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
    </div>
    <input type="hidden" name="hdnPrizeID" id="hdnPrizeID" value="{$data.serial_pri}" />
    <input type="hidden" name="hdnTypePri" id="hdnTypePri" value="{$data.type_pri}" />
    <input type="hidden" name="hdnSerialCou" id="hdnSerialCou" value="{$data.serial_cou}" />
    <input type="hidden" name="hdnCoverage" id="hdnCoverage" value="{$data.coverage_pri}" />
    <input type="hidden" name="hdnAmount" id="hdnAmount" value="{$data.amount_pri}" />
</form>
{else}
	<div class="append-7 span-10 prepend-7 last">
        <div class="span-10 error" align="center">
            No se pudo cargar la informaci&oacute;n del Premio.
        </div>
    </div>
{/if}