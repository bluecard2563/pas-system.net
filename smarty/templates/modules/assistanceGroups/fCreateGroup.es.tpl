<!--/*
File: fCreateGroup.es.tpl
Author: Nicolas Flores
Creation Date: 23.04.2010
*/-->
{assign var="title" value="NUEVO GRUPO DE ASISTENCIA"}
<form name="frmNewGroup" id="frmNewGroup" method="post" action="{$document_root}modules/assistanceGroups/pCreateGroup">
	<div class="span-24 last title">
    	Registro de Nuevo Grupo de Asistencia<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last" id="messageContainer">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El grupo se ha creado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
		<div class="prepend-2 span-4 label">* Pa&iacute;s</div>
    	<div class="span-5">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item=cl}
                	<option value="{$cl.serial_cou}" >{$cl.name_cou|lower|capitalize}</option>
                {/foreach}
            </select>
			<input type="hidden" name="nameValidate" id="nameValidate" value="0"/>
        </div>

        <div class="span-3 label">* Nombre</div>
        <div class="span-4 append-4 last">  <input type="text" name="txtGroupsName" id="txtGroupsName" title="El campo 'Nombre' es obligatorio"> </div>
	</div>
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnRegistrar" id="btnRegistrar" value="Registrar" >
    </div>
</form>