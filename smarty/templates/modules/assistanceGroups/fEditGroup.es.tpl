<!--/*
File: fEditGroup.es.tpl
Author: Nicolas Flores
Creation Date: 03.05.2010
*/-->
{assign var="title" value="EDITAR GRUPO DE ASISTENCIA"}
<form name="frmEditGroup" id="frmEditGroup" method="post" action="{$document_root}modules/assistanceGroups/pEditGroup">
	<div class="span-24 last title">
    	Editar Grupo de Asistencia<br />
        <label>(*) Campos Obligatorios</label>
    </div>
     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
		<div class="prepend-6 span-5 label">* Nombre</div>
    	<div class="span-5">
			<input type="text" id="txtGroupName" name="txtGroupName" value="{$group.name_asg}"/>
			<input type="hidden" id="hdnCountry" name="hdnCountry" value="{$group.serial_cou}"/>
			<input type="hidden" id="hdnGroup" name="hdnGroup" value="{$group.serial_asg}"/>
        </div>
	</div>
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnEditar" id="btnEditar" value="Editar" >
    </div>
</form>