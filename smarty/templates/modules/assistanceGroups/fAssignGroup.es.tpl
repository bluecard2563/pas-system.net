{assign var="title" value="ASIGNAR USUARIO A GRUPO DE ASISTENCIA"}
<form name="frmAssignGroup" id="frmAssignGroup" method="post" action="{$document_root}modules/assistanceGroups/pAssignGroup">
	<div class="span-24 last title ">
    	Asignaci&oacute;n de Usuario a Grupo de ASistencia<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	<div class="span-11 {if $error eq 1}success{else}error{/if}" align="center">
    	{if $error eq 1}
            	La asignaci&oacute;n a sido Exitosa!
        {elseif $error eq 2}
            	No se pudo realizar la asignaci&oacute;n.
		{/if}
        </div>
    </div>
    {/if}

   <div class="append-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="span-24 last line separator">
    	<label>Seleccionar grupo de Asistencia</label>
   </div>

   <div class="span-24 last line">
	   <div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
			<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="cl"}
                    <option value="{$cl.serial_cou}">{$cl.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Grupo:</div>
        <div class="span-4 append-4 last" id="groupContainer">
        	<select name="selGroup" id="selGroup" title='El campo "Grupo" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
	
   <div class="span-24 last line separator">
        <label>Seleccionar Usuarios</label>
   </div>

	<div class="span-24 last line">
		<div class="span-18 append-3 prepend-3 last center">
            <input id="radio_planet" name="user_of_radio" value="PLANET" checked="checked" type="radio">
			<label for="radio_planet" style="color: #000000;">{$global_system_name}</label> 
			<input id="radio_manager" name="user_of_radio" value="MANAGER" type="radio">
			<label for="radio_manager" style="color: #000000;">Representantes</label>
            <input id="radio_dealer" name="user_of_radio" value="DEALER" type="radio">
			<label for="radio_dealer" style="color: #000000;">Sucursales de Comercializador</label>
        </div>
	</div>

	<div id="dealerOptContainer" class="span-24" style="display: none;">
	<div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Zona:</div>
        <div class="span-5">
        	<select name="selZoneAssign" id="selZoneAssign" title='El campo "Zona" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$zoneList item="l"}
                    <option value="{$l.serial_zon}">{$l.name_zon}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Pa&iacute;s:</div>
        <div class="span-4 append-4 last" id="countryAssignContainer">
        	<select name="selCountryAssign" id="selCountryAssign" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
	</div>

     <div class="span-24 last line">
     	<div class="prepend-3 span-4 label">* Ciudad:</div>
        <div class="span-5" id="cityAssignContainer">
            <select name="selCityAssign" id="selCityAssign" title='El campo "Ciudad" es obligatorio'>
                <option value="">- Seleccione -</option>
            </select>
        </div>

		<div class="span-3 label">* Responsable:</div>
        <div class="span-4 append-4 last" id="comissionistAssignContainer">
        	<select name="selComissionistAssign" id="selComissionistAssign" title='El campo "Responsable" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
    	<div class="prepend-3 span-4 label">* Comercializador:</div>
        <div class="span-5" id="dealerContainer">
            <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.' >
                <option value="">- Seleccione -</option>
            </select>
        </div>
	</div>
	<div class="span-24 last line">
		<div class="prepend-3 span-4 label">* Sucursal de Comercializador:</div>
        <div class="span-4 append-4 last" id="branchContainer">
        	<select name="selBranch" id="selBranch" title='El campo "Sucursal de Comercializador" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    </div>
	</div>

	<div class="span-24 last line" id="managerContainer" style="display: none;">
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">* Zona:</div>
			<div class="span-5">
				<select name="selZoneAssignManager" id="selZoneAssignManager" title='El campo "Zona" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$zoneList item="l"}
						<option value="{$l.serial_zon}">{$l.name_zon}</option>
					{/foreach}
				</select>
			</div>

			<div class="span-3 label">* Pa&iacute;s:</div>
			<div class="span-4 append-4 last" id="countryAssignContainerManager">
				<select name="selCountryAssignManager" id="selCountryAssignManager" title='El campo "Pa&iacute;s" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">* Representante:</div>
			<div class="span-5" id="managerByCountryContainer">
				<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>
	</div>

    <div class="span-24 last line">
        <div class="prepend-5 span-6 label">*Seleccione los Usuarios</div>
        <div class="prepend-7 span-10 append-4 last line">
            <div class="span-4" id="assignedUsersContainer">
                <div align="center">Seleccionados</div>
                <select multiple id="usersTo" name="usersTo[]" class="selectMultiple span-4 last" title="El campo de usuarios es obligatorio.">
				</select>
            </div>
            <div class="span-2 last buttonPane">
                <br />
                <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
            </div>
            <div class="span-4 last" id="usersFromContainer">
                <div align="center">Disponibles</div>
                <select multiple id="usersFrom" name="usersFrom[]" class="selectMultiple span-4 last">
                </select>
            </div>

            <div class="span-13 append-6">
                *Todas los usuarios en el lado izquierdo deben estar seleccionados<br />
                para poder asignarles el grupo seleccionado.
            </div>
        </div>
    </div>


    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" />
    </div>
</form>