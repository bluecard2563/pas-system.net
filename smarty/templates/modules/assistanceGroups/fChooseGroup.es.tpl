<!--/*
File: fChooseGroup.es.tpl
Author: Nicolas Flores
Creation Date: 03.05.2010
*/-->
{assign var="title" value="SELLECION DE GRUPO DE ASISTENCIA"}
<form name="frmChooseGroup" id="frmChooseGroup" method="post" action="{$document_root}modules/assistanceGroups/fEditGroup">
	<div class="span-24 last title">
    	Seleccione Un Grupo de Asistencia para Editarlo<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="append-7 span-10 prepend-7 last" id="messageContainer">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El grupo se ha editado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	Hubo errores en el registro. Por favor vuelva a intentarlo.
            </div>
        {/if}
    </div>
    {/if}

     <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
		<div class="prepend-2 span-4 label">* Pa&iacute;s</div>
    	<div class="span-5">
			<select name="selCountry" id="selCountry" title="El campo 'Pa&iacute;s' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item=cl}
                	<option value="{$cl.serial_cou}" >{$cl.name_cou|lower|capitalize}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-3 label">* Grupo:</div>
        <div class="span-4 append-4 last" id="groupContainer">
        	<select name="selGroup" id="selGroup" title='El campo "Grupo" es obligatorio.'>
            	<option value="">- Seleccione -</option>
            </select>
        </div>
    <div class="span-24 last buttons line">
    	<input type="submit" name="btnEditar" id="btnEditar" value="Editar" >
    </div>
</form>