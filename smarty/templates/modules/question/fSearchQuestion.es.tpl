{assign var="title" value="BUSCAR PREGUNTA"}
<form name="frmSearchQuestion" id="frmSearchQuestion" method="post" action="{$document_root}modules/question/fUpdateQuestion" class="form_smarty">
	<div class="span-24 last title">Buscar Pregunta<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    {if $error}
         <div class="append-6 span-13 prepend-5 last">
            <div class="span-10 {if $error eq 2 or $error eq 5 or $error eq 7}success{else}error{/if}" align="center">
                {if $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    La pregunta ingresada no existe.
                {elseif $error eq 4}
                    Error al actualizar la traducci&oacute;n.
                {elseif $error eq 5}
                    Traducci&oacute;n exitosa!
                {elseif $error eq 6}
                    Error en la traducci&oacute;n.
                {elseif $error eq 7}
                    La traducci&oacute;n se elimin&oacute; exitosamente!
                {elseif $error eq 8}
                    Error al eliminar la traducci&oacute;n.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
    	<div class="prepend-2 span-5 label">Idioma:</div>
    	<div class="span-4 "> 
        	<select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio.">
                {foreach from=$languageList item=ll}
                <option value="{$ll.serial_lang}"{if $ll.serial_lang eq $actualLanguage} selected="selected"{/if}>{$ll.name_lang}</option>
                {/foreach}
            </select>
        </div>
	</div>
    
    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Pregunta:</div>
        <div class="span-11 append-6 last">
            <input type="text" name="txtText" id="txtText" title="El campo 'Pregunta' es obligatorio." class="autocompleter" />
        </div>
	</div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <div class="span-24 last line">&nbsp;</div>    
    <div class="span-24 last line" id="questionsContainer">
        {if $questions}
        <div class="prepend-4 span-20 last line">Preguntas registradas para el idioma seleccionado.</div>
        <div class="prepend-4 span-16 append-4 last line">
            <table border="0" id="questionsTable">
                    <THEAD>
                    <tr bgcolor="#284787">
                        <td align="center" class="tableTitle">
                            No.
                        </td>
                        <td align="center" class="tableTitle">
                            pregunta
                        </td>
                    </tr>
                    </THEAD>
                    <TBODY>
                      {foreach name="que" from=$questions item="q"}
                        <tr {if $smarty.foreach.que.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
                            <td align="center" class="tableField">
                                <b>{$smarty.foreach.que.iteration}</b>
                            </td>
                            <td align="left">
                                {$q.text_qbl}
                            </td>
                        </tr>
                      {/foreach}
                    </TBODY>
           </table>
        </div>
        <div class="span-24 last pageNavPosition" id="pageNavPosition"></div>
        {else}
        <div class="prepend-4 span-20 last line">
            No existen preguntas registradas para el idioma seleccionado.
        </div>
        {/if}
    </div>  
    <input type="hidden" name="hdnQuestionID" id="hdnQuestionID" value="" />

</form>
