{assign var="title" value="ACTUALIZAR PREGUNTA"}
    
{if $data}
<form name="frmUpdateQuestion" id="frmUpdateQuestion" method="post" action="{$document_root}modules/question/pUpdateQuestion" class="">    
	<div class="span-24 last title ">
    	Actualizar Pregunta
    </div>

	{if $error}
    <div class="append-6 span-13 prepend-5 last">
    	<div class="span-10 {if $error eq 1 or $error eq 3}success{else}error{/if}" align="center">
        	No se pudo actualizar la pregunta.
        </div>
    </div>
    {/if}
    <div class="span-7 span-10 prepend-13 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
   <div class="span-24 last line">
    	<div class="prepend-2 span-4 label">Tipo:</div>
    	<div class="span-5 "> 
        	<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=tl}
                	<option value="{$tl}" {if $data.type_que eq $tl} selected="selected" {/if}>{if $tl eq 'YN'}Si/No{elseif $tl eq 'OPEN'}Abierta{/if}</option>
                {/foreach}
            </select>
        </div>
        <div class="prepend-1 span-4 label">Estado:</div>
        <div class="span-5"> 
        	<select name="selStatus" id="selStatus" title="El campo 'Estado' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item=sl}
                	<option value="{$sl}"{if $data.status_que eq $sl} selected="selected" {/if}>{if $sl eq 'ACTIVE'}Activo{elseif $sl eq 'INACTIVE'}Inactivo{/if}</option>
                {/foreach}
            </select>
        </div>
	</div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
    </div>
    <div class="span-24 last">&nbsp;</div>
    <input type="hidden" name="hdnQuestionblID" id="hdnQuestionblID" value="{$data.serial_qbl}" />
    <input type="hidden" name="hdnQuestionID" id="hdnQuestionID" value="{$data.serial_que}" />
</form>

<form id="frmNewQuestionTranslation" name="frmNewQuestionTranslation" method="post" action="{$document_root}modules/question/pNewQuestionTranslation" />	
	<div class="span-24 last line">&nbsp;</div>
	<div class="span-24 last title ">
    	Traducciones
    </div>      
    <div class="prepend-3 span-19 append-2 last" id="questionTranslations">
    	{assign var="container" value="none"}
    	{generate_admin_table data=$dataTranslations titles=$titles text=$text}
    </div>
    
    <div class="span-24 last line">&nbsp;</div>
    <div class="span-24 last line" id="newInfo">  
    {if $languageList}  	
    	<div class="span-24 last title ">
    		Nueva Traducci&oacute;n
    	</div>
        <div class="span-24 last line">
        	<div class="prepend-4 span-4 label">Idioma:</div>
            <div class="span-5 ">
                <select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio.">
                    <option value=""  selected="selected" >- Seleccione -</option>
                    {foreach from=$languageList item=ll}
                        <option value="{$ll.serial_lang}">{$ll.name_lang}</option>
                    {/foreach}
                </select>
            </div>
            <div class="span-3 label">Pregunta:</div>
            <div class="span-4 append-4 last">
                <textarea name="txtText" id="txtText" title="El campo 'Pregunta' es obligatorio." class="txtArea"></textarea>
            </div>
        </div>
        <div class="span-24 last line buttons">
            <input type="submit" name="btnSubmit" id="btnSubmit" value="Registrar">
        </div>
        {else}
        <div class="span-24 last">
          <div class="span-17 label">Ya existen traducciones para esta pregunta en todos los idiomas</div>  
        </div>
        <div class="span-24 last">&nbsp;</div>
        <div class="span-24 last line buttons">
            <input type="button" name="btnOk" id="btnOk" value="Aceptar">
        </div>
    {/if}
     </div>
    <input type="hidden" name="serial_que" id="serial_que" value="{$data.serial_que}" />
    <input type="hidden" name="serial_lang" id="serial_lang" value="{$actualLanguage}" />
        
</form>
<div id="dialog" title="Actualizar Traducci&oacute;n">
	{include file="templates/modules/question/fUpdateQuestionTranslationDialog.$language.tpl"}
</div>

{else}
	<div class="append-5 span-10 prepend-4 last">
        <div class="span-10 error" align="center">
            No se pudo cargar la pregunta.
        </div>
    </div>
{/if}
