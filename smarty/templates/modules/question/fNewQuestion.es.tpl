{assign var="title" value="Nueva Pregunta"}
<form name="frmNewQuestion" id="frmNewQuestion" method="post" action="{$document_root}modules/question/pNewQuestion" class="">
	<div class="span-24 last title">
    	Nueva Pregunta<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-6 span-13 prepend-5 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	La pregunta se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	No se pudo registrar la pregunta.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
    	<div class="prepend-3 span-4 label">* Tipo:</div>
    	<div class="span-4"> 
        	<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=tl}
                	<option value="{$tl}">{if $tl eq 'YN'}Si/No{elseif $tl eq 'OPEN'}Abierta{/if}</option>
                {/foreach}
            </select>
        </div>
    
	    <div class="span-4 label">* Pregunta:</div>
    	<div class="span-4 append-5 last"> 
        	<textarea name="txtText" id="txtText" title="El campo 'Pregunta' es obligatorio." class="txtArea"></textarea>
        </div>
	</div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
    </div>
    <div class="span-24 last line">&nbsp;</div>

</form>
