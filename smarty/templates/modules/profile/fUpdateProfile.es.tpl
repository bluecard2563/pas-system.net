<!--/*
File: fUpdateProfile.tpl
Author: Esteban Angulo
Creation Date: 30/12/2009 11:20
Last Modified:
*/-->
{assign var="title" value="MODIFICAR PERFIL"} 
<form name="frmUpdateProfile" id="frmUpdateProfile" method="post" action="{$document_root}modules/profile/pUpdateProfile" >
	<div class="span-24 last title">
    	Modificaci&oacute;n de  Perfil <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="prepend-7 span-14 append-1 last">
        	<div class="span-10  error" align="center">
            {if $error eq 2}
                Ya existe un perfil con el nombre ingresado. Por favor intente con un nombre diferente.
            {elseif $error eq 3}
                No se pudo actualizar el perfil. Por favor int&eacute;ntelo nuevamente.
             {elseif $error eq 4}
                Existi&oacute; un error al asignar los procesos del perfil. Por favor int&eacute;ntelo nuevamente.
             {elseif $error eq 5}
                Por favor seleccione al menos un proceso.
             {elseif $mensaje eq 6}
                Hubo errores en la actualizaci&oacute;n de los usuarios con este perfil. Por favor int&eacute;ntelo nuevamente.
            {/if}
            </div>
    </div>
    {/if}
    
 <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>


<div class="span-24 last   line">
    <div class="prepend-3 span-8 label">* Nombre:</div>
    <div class="span-8 append-4 last"> <input type="text" name="txtProfileName" id="txtProfileName" {if $name}value="{$name}"{/if} title='El campo "Nombre" es obligatorio.'> </div>
</div>


<div class="span-24 last line">
    <div class="prepend-3 span-8 label">* Estado:</div>
    <div class="span-8 append-4 last"> <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item="l"}
              <option value="{$l}" {if $status eq $l}selected="selected"{/if} >{$global_status.$l}</option>
                {/foreach}
            </select> </div>
</div>

	<div class="span-24  last ">
		{foreach from=$info item="i"}
			<div class="span-24  line last">
				<div class="prepend-6 span-10  last ">
					 <label for="optgen[{$i.name}]">{$i.name}</label>
				</div>
				{if $i.parent_key eq ''}
					<div class="prepend-1 span-2 append-4 last">
						<input class="checkAllOpt" type="checkbox" name="optgen[]" id="optgen[{$i.serial}]" value="{$i.serial}" parent_check="{$i.serial}" {if $i.checked eq 'X'}checked="checked"{/if}/>
					</div>
				{elseif $i.child eq 'NO'}
					<div class="prepend-1 span-2 append-4  last" >
						<input type="checkbox" parent_key="{$i.superParent}" name="option[]" id="options[{$i.chkName}]" value="{$i.parent_key}-{$i.serial}" {if $i.checked eq 'X'}checked="checked"{/if}/>
					</div>
				{/if}
			</div>
		{/foreach}
	</div>

	

	<div class="span-24 last buttons">
    	<input type="submit" name="btnInsert" id="btnInsert" value="Actualizar">
    </div>
    <input type="hidden" name="update" id="update" value="1" />
    <input type="hidden" name="hddSerialProfile" id="hddSerialProfile" value="{$serial_pbc}" />
    <input type="hidden" name="hdnSerial_cou" id="hdnSerial_cou" value="{$country}" />
</form>