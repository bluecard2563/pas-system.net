<!--/*
File: fSearchProfile.tpl
Author: Esteban Angulo
Creation Date: 30/12/2009 11:00
Last Modified: 
*/-->
{assign var="title" value="BUSCAR PERFIL"} 
<form name="formSearchProfile" id="formSearchProfile"  method="post" action="{$document_root}modules/profile/fUpdateProfile" >
	<div class="span-24 last title">Consultar Perfil <br />
    	<label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="prepend-7 span-14 append-1 last">
        	<div class="span-10  {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                Perfil actualizado exit&oacute;samente.
            {/if}
            {if $error eq 2}
                Ya existe un perfil con el nombre ingresado. Por favor intente con un nombre diferente.
            {elseif $error eq 3}
                No se pudo actualizar el perfil. Por favor int&eacute;ntelo nuevamente.
             {elseif $error eq 4}
                Existi&oacute; un error al asignar los procesos del perfil. Por favor int&eacute;ntelo nuevamente.
             {elseif $error eq 5}
                Por favor seleccione al menos un proceso.
             {elseif $mensaje eq 6}
                Hubo errores en la actualizaci&oacute;n de los usuarios con este perfil. Por favor int&eacute;ntelo nuevamente.
            {/if}
            </div>
    </div>
    {/if}

	 <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last  line">
        <div class="prepend-3 span-8 label">* Pa&iacute;s:</div>
       	<div class="span-8 append-4 last">
        	<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $data.aux.serial_cou eq $l.serial_cou} selected="selected"{/if}>{$l.name_cou}</option>
                {/foreach}                                 
            </select>
        </div>
	</div>

    
    <div class="span-24 last  line">
        <div class="prepend-3 span-8 label">* Nombre del Perfil:</div>
        <div class="span-8 append-4 last" id="profileContainer">
        	<select id="selProfile" name="selProfile" title="Seleccione un perfil">
            	<option value="">- Seleccione -</option>
            </select> 
       </div>
	</div>
    
     
    <div class="span-24 last buttons">
         <input type="submit" name="btnSearch" value="Consultar Perfil"/> 
    </div>
</form> 
