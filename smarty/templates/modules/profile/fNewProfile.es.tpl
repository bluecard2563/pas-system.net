<!--/*
File: fNewProfile.tpl
Author: Esteban Angulo
Creation Date: 28/12/2009 18:00
Last Modified:28/12/2009 
*/-->
{assign var="title" value="NUEVO PERFIL"} 
<form name="frmNewProfile" id="frmNewProfile" method="post" action="{$document_root}modules/profile/pNewProfile" >
	<div class="span-24  last title">
    	Registro de Nuevo Perfil <br />
        <label>(*) Campos Obligatorios</label>
    </div>
	{if $error}
    <div class="prepend-7 span-14 append-1 last">
        	<div class="span-10  {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                Perfil creado exit&oacute;samente.
            {elseif $error eq 2}
                Ya existe un perfil con el nombre ingresado. Por favor intente con un nombre diferente.
            {elseif $error eq 3}
                No se pudo crear el perfil. Por favor int&eacute;ntelo nuevamente.
             {elseif $error eq 4}
                Existi&oacute; un error al asignar los procesos del perfil. Por favor int&eacute;ntelo nuevamente.
             {elseif $error eq 5}
                Por favor seleccione al menos un proceso.
            {/if}
            </div>

    </div>
    {/if}
    
 <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>


<div class="span-24 last   line">
    <div class="prepend-3 span-8 label">* Nombre:</div>
    <div class="span-8 append-4 last"> <input type="text" name="txtProfileName" id="txtProfileName" title='El campo "Nombre" es obligatorio.'> </div>
    <input type="hidden" name="hidenName" id="hidenName" value="1">
</div>

<div class="span-24 last line">
    <div class="prepend-3 span-8 label">* Pa&iacute;s:</div>
    <div class="span-8 append-4 last"> <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
             <option id="{$l.serial_cou}" value="{$l.serial_cou}"> {if $l.name_cou eq 'ALL'}Todos los pa&iacute;ses{else}{$l.name_cou}{/if}</option>
                {/foreach}
            </select> </div>
</div>


<div class="span-24 last line">
    <div class="prepend-3 span-8 label">* Estado:</div>
    <div class="span-8 append-4 last"> <select name="selStatus" id="selStatus" title='El campo "Estado" es obligatorio'>
            	<option value="">- Seleccione -</option>
                {foreach from=$statusList item="l"}
              <option value="{$l}" {if $data.status_usr eq $l}selected="selected"{/if} >{$global_status.$l}</option>
                {/foreach}
            </select> </div>
</div>

	<div class="span-24  last ">
		{foreach from=$info item="i"}
			<div class="span-24  line last">
				<div class="prepend-6 span-10  last ">
					 <label for="optgen[{$i.name}]">{$i.name}</label>
				</div>
				{if $i.parent_key eq ''}
					<div class="prepend-1 span-2 append-4 last">
						<input class="checkAllOpt" type="checkbox" name="optgen[]" id="optgen[{$i.serial}]" value="{$i.serial}" parent_check="{$i.serial}"/>
					</div>
				{elseif $i.child eq 'NO'}
					<div class="prepend-1 span-2 append-4  last" >
						<input type="checkbox" parent_key="{$i.superParent}" name="option[]" id="options[{$i.chkName}]" value="{$i.parent_key}-{$i.serial}"/>
					</div>
				{/if}
			</div>
		{/foreach}
	</div>

	<div class="span-24 last buttons">
    	<input type="submit" name="btnInsert" id="btnInsert" value="Registrar">
    </div>
</form>