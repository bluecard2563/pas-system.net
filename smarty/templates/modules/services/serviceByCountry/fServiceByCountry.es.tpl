{assign var=title value="ASIGNACI&Oacute;N DE SERVICIOS POR PAISES"}
<form name="frmPBC" id="frmPBC" method="post" action="{$document_root}modules/services/serviceByCountry/pServiceByCountry">
    <div class="span-24 last title ">
        Asignaci&oacute;n de Servicios por Paises
    </div>
    {if $error}
    <div class="prepend-6 span-12 last">
        {if $error eq 1}
            <div class="success" align="center">
                El Servicio se ha asignado exitosamente!
            </div>
        {else}
            <div class="error">
                Existi&oacute; un error al asignar el servicio por favor int&eacute;ntelo nuevamente.
            </div>
        {/if}
    </div>
    {/if}

    <br><hr>
    
    <input type="hidden" name="serial_ser" id="serial_ser" value="{$data.serial_ser}">

    <div class="span-24 last line label">
        <h4>Nombre del Servicio: {$data.name_sbl}</h4>

    </div>

    <div class="span-24 last line label">
        * Asignaci&oacute;n del servicio a los pa&iacute;ses.
    </div>

    <div class="prepend-6 span-12 last">
        <ul id="alerts" class="alerts"><li><strong>* CLIC EN EL MENSAJE DE ERROR PARA IR HASTA EL ELEMENTO *</strong><br /><br /></li></ul>
    </div>
    {if !$assigned_countries}
    <div class="span-24 last even-row" id="mandatoryCountry">
        <hr>
        <div class="span-24 last line">
            <div class="span-2 label">* Zona:</div>
            <div class="span-5" id="zoneContainer0">
                <select name="countries[0][selZone]" id="selZone0" title='El campo "Zona" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$zonesList item="zl"}
                        <option value="{$zl.serial_zon}">{$zl.name_zon}</option>
                    {/foreach}
                </select>
            </div>

            <div class="span-2 label">* Pa&iacute;s:</div>
            <div class="span-5" id="countryContainer0">
                <select name="countries[0][selCountry]" id="selCountry0" onchange="addSelectedCountry(this.value,0);" title='El campo "Pa&iacute;s" es obligatorio'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
            <input type="hidden" name="0" id="0" class="cou_arr" value="">
            <div class="span-3 label">* Impuestos:</div>
            <div class="span-7 last line" id="taxContainer0">Seleccione un pa&iacute;s</div>
            <div class="span-24 last" id="priceContainer0">Seleccione un pa&iacute;s</div>

        </div>

        <div class="span-24 line label">
            Asignar comercializadores
        </div>
        <div class="span-24 line" >
            <div class="span-4 label">Representantes:</div>
            <div class="span-5" id="managerContainer0">
                Seleccione un pa&iacute;s
            </div>
            <div class="span-4 label">Comercializadores: </div>
            <div class="span-10 append-1 last" id="dealersContainer0">
                <div class="span-4">
					<div align="center">Seleccionados</div>
                    <select multiple id="dealersTo" name="countries[0][dealersTo][]" class="selectMultiple span-4 last"></select>
                </div>
                <div class="span-2 last buttonPane">
					<br />
                    <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                    <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                    <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                    <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
                </div>
                <div class="span-4 last line" id="dealersFromContainer"><br />Seleccione un representante</div>
            </div>
        </div>
    </div>
    {/if}
    <div class="span-24 last line" id="countriesContainer">
        {if $assigned_countries}
            {foreach from="$assigned_countries" key="key" item="ac" name=country_items}
            <input type="hidden" name="countries[{$key}][serial_sbc]" id="serial_sbc{$key}" value="{$ac.serial_sbc}" />
            <div class="span-24 last {cycle values='even-row,odd-row'} " id="country{$key}">
                <hr>
                <div class="prepend-22 span-2 last"><a href="#" class="activeLink{$key}" onclick="statusCountry('{$key}'); return false;" id="delete{$key}" style="color: {if $ac.status_sbc eq 'ACTIVE'}#008000{else}#FF0000{/if}">{if $ac.status_sbc eq "ACTIVE"}Activo{else}Inactivo{/if}</a></div>
                <input type="hidden" value="{$ac.status_sbc}" id="countryStatus{$key}" name="countries[{$key}][countryStatus]" />
                <div class="span-24 last line">
                    <div class="span-2 label">* Zona:</div>
                    
                    <div class="span-5" id="zoneContainer{$key}" style="display:none;">
                        <select name="countries[{$key}][selZone]" id="selZone{$key}" title='El campo "Zona" es obligatorio.'>
                            <option value="">- Seleccione -</option>
                            {foreach from=$zonesList item="zl"}
                                <option value="{$zl.serial_zon}" {if $zl.serial_zon eq $ac.serial_zon}selected="selected"{/if}>{$zl.name_zon}</option>
                            {/foreach}
                        </select>
                    </div>
                    
                    <div class="span-5">
                        {foreach from=$zonesList item="zl"}
                            {if $zl.serial_zon eq $ac.serial_zon}
                                {$zl.name_zon}
                                <input type="hidden" name="countries[{$key}][selZone]" id="selZone{$key}" value="{$zl.serial_zon}" />
                            {/if}
                        {/foreach}
                    </div>

                    <div class="span-2 label">* Pa&iacute;s:</div>
                    <div class="span-5" id="countryContainer{$key}">
                        {foreach from=$ac.zone_countries item="zc"}
                            {if $ac.serial_cou eq $zc.serial_cou}
                                {$zc.name_cou}
                                <input type="hidden" name="countries[{$key}][selCountry]" id="selCountry{$key}" value="{$zc.serial_cou}" />
                            {/if}
                        {/foreach}
                    </div>
                    <input type="hidden" name="{$key}" id="{$key}" class="cou_arr" value="{$ac.serial_cou}">

                    <div class="span-3 label">* Impuestos:</div>
                    <div class="span-7 last" id="taxContainer{$key}">
                        {if $ac.country_taxes_serial}
                            {html_checkboxes values=$ac.country_taxes_serial output=$ac.country_taxes_name checked=$ac.assigned_taxes separator="<br>" name="countries[$key][taxes]"}
                        {else}
                        No existen impuestos en el pa&iacute;s seleccionado.
                        {/if}
                    </div>
                </div>

                <div class="span-24 last line">
                    <div class="span-3 label">Cobertura:</div>
                    <div class="span-4 last" id="coverageContainer{$key}">
                        <input type="text" class="span-2" name="countries[{$key}][coverage_sbc]" id="coverage_sbc_{$key}" value="{$ac.coverage_sbc}" title='El campo "C&oacute;nyugue" es obligatorio' />
                    </div>

                    <div class="span-3 label">Precio:</div>
                    <div class="span-4 last" id="priceContainer{$key}">
                    <input type="text" class="span-2" name="countries[{$key}][price_sbc]" id="price_sbc_{$key}" value="{$ac.price_sbc}" title='El campo "Hijos" es obligatorio' />
                        
                    </div>

                    <div class="span-4 label">Costo:</div>
                    <div class="span-2 last" id="costContainer{$key}">
                    <input type="text" class="span-2" name="countries[{$key}][cost_sbc]" id="cost_sbc_{$key}" value="{$ac.cost_sbc}" title='El campo "Extras" es obligatorio' />

                    </div>
                </div>
                <div class="span-24 line label">
                    Asignar comercializadores
                </div>
                <div class="span-24 line" >
                    <div class="span-4 label">Representantes:</div>
                    <div class="span-5" id="managerContainer{$key}">
                        {if $ac.managers}
                        <select id="selManager{$key}" name="countries[{$key}][selManager]" onchange="loadDealersByManager(this.value,'{$key}');">
                            <option value="">- Seleccione -</option>
                            {foreach from=$ac.managers item="m" name="managers"}
                                <option value="{$m.serial_mbc}" >{$m.name_man}</option>
                            {/foreach}
                        </select>
                        {else}
                            No existen representantes registrados.
                        {/if}
                    </div>
                    <div class="span-4 label">Comercializadores: </div>
                    <div class="span-10 append-1 last" id="dealersContainer{$key}">
                        <div class="span-4">
                            <div align="center">Seleccionados</div>
                            <select multiple id="dealersTo" name="countries[{$key}][dealersTo][]" class="selectMultiple span-4 last">
                                {foreach from=$ac.assigned_dealers item="ad"}
                                <option value="{$ad.serial_dea}" manager="{$ad.serial_mbc}">{$ad.name_dea}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="span-2 last buttonPane">
                            <br />
                            <input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
                            <input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
                            <input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
                            <input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
                        </div>
                        <div class="span-4 last line" id="dealersFromContainer">Seleccione un representante</div>
                    </div>
                </div>
            </div>
            {/foreach}
        {/if}
    </div>
    <input type="hidden" name="countriesCount" id="countriesCount" value="{$countriesCount}">
    <div class="span-24 last line">
        <div class="prepend-19 span-5">
            <input type="button" name="btnAddCountry" id="btnAddCountry" value="Asignar otro pa&iacute;s" >
        </div>
    </div>
    <br>
    <hr>

    <div class="span-24 buttons line">
        <div class="prepend-10 span-4">
            <input type="submit" name="btnGuardarSalir" id="btnGuardarSalir" value="Guardar y Salir">
        </div>
        <div class="span-2 last">
            <a href="#" onclick="location.href='{$document_root}modules/services/serviceByCountry/fSearchServiceByCountry';return false;">Cancelar</a>
        </div>
    </div>
</form>