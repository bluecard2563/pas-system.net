{assign var="title" value="BUSCAR SERVICIO"}
<form name="frmSearchService" id="frmSearchService" action="{$document_root}modules/services/serviceByCountry/fServiceByCountry" method="post" class="form_smarty">
    <div class="span-24 last title">Buscar Servicio</div>
    {if $error}
         <div class="prepend-7 span-10 last">
            <div class="{if $error eq 1 || $error eq 2 }success{else}error{/if}" align="center">
                {if $error eq 1}
                    La asignaci&oacute;n se ha ingresado exitosamente!
                {elseif $error eq 2}
                    Hubo errores en la asignaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 3}
                    El servicio ingresado no existe.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-9 span-2 label">Idioma:</div>
        <div class="span-5 last">
            <select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio">
                <option value="">- Seleccione -</option>
                {foreach from=$languageList item="l"}
                <option value="{$l.serial_lang}">{$l.name_lang}</option>
                {/foreach}
            </select>
        </div>
    </div>
    
    <div class="span-24 last line" id="serviceContainer"></div>
</form>