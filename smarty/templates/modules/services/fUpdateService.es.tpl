{assign var="title" value="ACTUALIZAR SERVICIO"}
<form name="frmUpdateService" id="frmUpdateService" method="post" action="{$document_root}modules/services/pUpdateService" class="">
    <div class="span-10 append-7 prepend-7 last title">
    	Actualizar Servicio<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
            <div class="span-10 success" align="center">
            	El producto se ha actualizado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	error al insertar.
            </div>
        {/if}
    </div>
    {/if}

    <div class="span-3 span-18 prepend-3 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Activo:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="checkbox" name="status_ser" id="status_ser" value="ACTIVE" {if $data.status_ser eq 'ACTIVE'}checked="checked"{/if}/>
            </div>
        </div>
    </div>


    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Nombre del Servicio :</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" name="name_sbl" id="name_sbl" class="span-7" value="{$data.name_sbl}"/>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Descripci&oacute;n del Servicio :</label>
            </div>

        <div class="span-8 append-4 last ">
            <div>
                <textarea  name="description_sbl" id="description_sbl" class="span-7" >{$data.description_sbl}</textarea>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Cobertura:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" id="coverage_sbc" name="coverage_sbc" class="span-2" value="{$data.coverage_sbc}"/>
            </div>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label class="append-1">Precios:</label>
    </div>


    <div class="span-24 last line">
        <div class=" prepend-8 span-8 append-4 last ">
            <div class="span-1">
                <input type="radio" value="PERDAY" id="fee_type_ser" name="fee_type_ser" {if $data.fee_type_ser eq 'PERDAY'}checked="checked"{/if}/>
            </div>
            <div class="span-3">
                <label>Precio por d&iacute;a</label>
            </div>
            <div class="span-1 last">
                <input type="radio" value="TOTAL" id="fee_type_ser" name="fee_type_ser" {if $data.fee_type_ser eq 'TOTAL'}checked="checked"{/if}/>
            </div>
            <div class="span-3">
                <label>Precio Total</label>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Precio:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" id="price_sbc" name="price_sbc" class="span-2" value="{$data.price_sbc}"/>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Costo:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" id="cost_sbc" name="cost_sbc" class="span-2" value="{$data.cost_sbc}"/>
            </div>
        </div>
    </div>
    <input type="hidden" name="serial_lang" id="serial_lang" value="{$data.serial_lang}" />
    <input type="hidden" name="serial_ser" id="serial_ser" value="{$data.serial_ser}" />
    <input type="hidden" name="serial_sbl" id="serial_sbl" value="{$data.serial_sbl}" />
    <input type="hidden" name="serial_sbc" id="serial_sbc" value="{$data.serial_sbc}" />
    <div class="span-24 last title">
         <input type="submit" id="UpdateService" value="Actualizar"  class="ui-state-default ui-corner-all"/>
    </div>
</form>