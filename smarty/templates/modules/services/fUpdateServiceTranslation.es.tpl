<form id="frmUpdateServiceTranslation" name="frmUpdateServiceTranslation" method="post" action="{$document_root}modules/services/pUpdateServiceTranslation" >
    <div class="span-10 last line">
        <ul id="alerts_dialog" class="alerts"></ul>
    </div>

    <div class="span-10 last line">
        <div class="prepend-1 span-3 label">
            Idioma:
        </div>
        <div class="span-4 append-1 last" id="language"></div>
    </div>

    <div class="span-10 last line">
        <div class="prepend-1 span-3 label">
            Nombre:
        </div>
        <div class="span-4 append-1 last">
            <input type="text" name="txtSblName" id="txtSblName" value="" title="El campo 'Nombre' es obligatorio." />
        </div>
    </div>

    <div class="span-10 last line">
        <div class="prepend-1 span-3 label">
            Descripci&oacute;n:
        </div>
        <div class="span-4 append-1 last">
            <textarea name="txtSblDesc" id="txtSblDesc" value="" class="txtArea" ></textarea>
        </div>
    </div>

    <input type="hidden" name="serial_sbl" id="serial_sbl" />
    <input type="hidden" name="serial_lang" id="serial_lang" />
</form>