{assign var="title" value="NUEVO SERVICIO"}
<form name="frmNewService" id="frmNewService" method="post" action="{$document_root}modules/services/pNewService" class="">


    <div class="span-10 append-7 prepend-7 last title">
    	Nuevo Servicio<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
            <div class="span-10 success" align="center">
            	El producto se ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	error al insertar.
            </div>
        {/if}
    </div>
    {/if}

    <div class="span-3 span-18 prepend-3 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Nombre del Servicio :</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" name="name_sbl" id="name_sbl" class="span-7"/>
            </div>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>* Descripci&oacute;n del Servicio :</label>
            </div>

        <div class="span-8 append-4 last ">
            <div>
                <textarea  name="description_sbl" id="description_sbl" class="span-7"></textarea>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Cobertura:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" id="coverage_sbc" name="coverage_sbc" class="span-2"/>
            </div>
        </div>
    </div>

    <div class="span-24 last line separator">
        <label class="append-1">Precios:</label>
    </div>


    <div class="span-24 last line">
        
        <div class=" prepend-8 span-8 append-4 last ">
            <div class="span-1">
                <input type="radio" value="PERDAY" id="fee_type_ser" name="fee_type_ser"/>
            </div>
            <div class="span-3">
                <label>Precio por d&iacute;a</label>
            </div>
            <div class="span-1 last">
                <input type="radio" value="TOTAL" id="fee_type_ser" name="fee_type_ser"/>
            </div>
            <div class="span-3">
                <label>Precio Total</label>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Precio:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" id="price_sbc" name="price_sbc" class="span-2"/>
            </div>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-4 span-8 label">
            <label>Costo:</label>
        </div>
        <div class="span-8 append-4 last ">
            <div>
                <input type="text" id="cost_sbc" name="cost_sbc" class="span-2"/>
            </div>
        </div>
    </div>

    <div class="span-24 last title">
         <input type="submit" id="newService" value="Insertar"  class="ui-state-default ui-corner-all"/>
    </div>
</form>