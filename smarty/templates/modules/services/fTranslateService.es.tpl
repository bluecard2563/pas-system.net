{assign var="title" value="ACTUALIZAR SERVICIO"}
<div class="span-24 last title">
    Actualizar Servicio
</div>
{if $data}

{if $error}
<div class="append-5 span-10 prepend-4 last">
	<div class="span-10 error" align="center">
		No se pudo actualizar el servicio.
	</div>
</div>
{/if}

<div class="prepend-7 span-10 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<div class="prepend-2 span-20 append-2 last line" id="adminTable">
	{assign var="container" value="none"}
	{generate_admin_table data=$translations titles=$titles text="Registrar Traducci&oacute;n"}
</div>   
<input type="hidden" name="hdnServiceID" id="hdnServiceID" value="{$data.serial_ser}" />


<form name="frmNewServiceTranslation" id="frmNewServiceTranslation" method="post" action="{$document_root}modules/services/pNewServiceTranslation">
    <div id="newInfo" class="span-24 last line">
        <div class="prepend-7 span-12 prepend-5 last">
	    <ul id="alerts_t" class="alerts"></ul>
        </div>
        {if $languageList}
         <div class="prepend-2 span-20 last line">
            <div class="prepend-7 span-2 label">Idioma: </div>
            <div class="span-8 append-2 last">
                <select id="selLanguage" name="selLanguage" title="El campo 'Idioma' es obligatorio." class="select">
                    <option value="">- Seleccione -</option>
                    {foreach from=$languageList item="l"}
                        <option value="{$l.serial_lang}">{$l.name_lang}</option>
                    {/foreach}
                </select>
            </div>
         </div>

        <div class="prepend-2 span-20 last line">
            <div class="prepend-7 span-2 label">Nombre: </div>
            <div class="span-8 append-2 last">
                <input type="text" name="txtSblName" id="txtSblName" title="El campo 'Nombre' es obligatorio." >
            </div>
        </div>

         <div class="prepend-2 span-20 last line">
            <div class="prepend-7 span-2 label">Descripci&oacute;n: </div>
            <div class="span-8 append-2 last">
                <textarea name="txtSblDesc" id="txtSblDesc" title="El campo 'Descripci&oacute;n' es obligatorio." class="txtArea" cols="100" rows="20"></textarea>
            </div>
         </div>

        <div class="span-24 last buttons line">
            <input type="button" name="btnInsert" id="btnInsert" value="Registrar" class="" >
        </div>
        <input type="hidden" name="hdnServiceID" id="hdnServiceID" value="{$data.serial_ser}" />
    </div>
    {else}
        <div class="span-14 append-5 last">
            <div class="span-14 label">No existen m&aacute;s idiomas disponibles para traducir este servicio.</div>
        </div>
    {/if}
</form>

<div id="dialog" title="Actualizar ">
    {include file="templates/modules/services/fUpdateServiceTranslation.$language.tpl"}
</div>
{else}
	<div class="append-5 span-10 prepend-4 last">
        <div class="span-10 error" align="center">
            No se pudo cargar la informaci&oacute;n del Servicio.
        </div>
    </div>
{/if}