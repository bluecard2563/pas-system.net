{assign var="title" value="BUSCAR SERVICIO"}
<form name="frmSearchService" id="frmSearchService" action="{$document_root}modules/services/fTranslateService" method="post" class="form_smarty">
    <div class="span-24 last title">Buscar Servicios</div>
    {if $error}
         <div class="prepend-7 span-10 last">
            <div class="{if $error eq 1 || $error eq 2 }success{else}error{/if}" align="center">
                {if $error eq 1}
                    La traducci&oacute;n se ha ingresado exitosamente!
                {elseif $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El servicio ingresado no existe.
                {elseif $error eq 4}
                    Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
                {elseif $error eq 5}
                    Hubo errores en el registro de la traducci&oacute;n. Por favor vuelva a intentarlo.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-9 span-2 label">Idioma:</div>
        <div class="span-5 last">
            <select name="selLanguage" id="selLanguage" title="El campo 'Idioma' es obligatorio">
                <option value="">- Seleccione -</option>
                {foreach from=$languageList item="l"}
                <option value="{$l.serial_lang}" {if $serial_lang eq $l.serial_lang}selected{/if}>{$l.name_lang}</option>
                {/foreach}
            </select>
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-5 span-4 label">Ingrese el Servicio:</div>
        <div class="span-11 last">
            <input type="text" name="txtSblName" id="txtSblName" title="Ingrese un servicio para actualizar." class="autocompleter" />
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnSblID" id="hdnSblID" value="" />
</form>