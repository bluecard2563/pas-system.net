{assign var="title" value="LIQUIDACI&Oacute;N DE COMISIONES A REPRESENTANTE"}
<form name="frmSelectComissions" id="frmSelectComissions" method="post" target="_blank" action="pPayComission">
	<input type="hidden" name="selComissionsTo" id="selComissionsTo" value="MANAGER">
    <div class="span-24 last title">
    	Liquidaci&oacute;n de Comisiones a Representante<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
    <div class="span-17 prepend-7 last">
        <div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">
            {if $error eq 1}
                Se ha registrado exitosamente!
            {else}
                Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            {/if}
        </div>
    </div>
    {/if}

	<div class="span-10 prepend-7 append-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>

	<div class="span-24 last line separator">
        <label>Datos de la Liquidaci&oacute;n:</label>
    </div>

	<div class="span-24 last line" id="formContainer">
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
			<div class="span-5">
				<select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
					<option value="">- Seleccione -</option>
					{foreach from=$countryList name="countries" item="l"}
						<option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou|htmlall}</option>
					{/foreach}
				</select>
			</div>
			<div class="span-3 label">* Representante:</div>
			<div class="span-4 append-4 last" id="managerContainer">
				<select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label" >* Fecha Hasta:</div>
			<input type="text" name="txtDateTo" id="txtDateTo" title='El campo "Fecha Hasta" es obligatorio' />
		</div>
		<div class="span-24 last line buttons">
			<input type="button" name="btnFindComissions" id="btnFindComissions" value="Verificar Comisiones" />
		</div>
		
		<div class="span-24 last line" id="comissionsContainer"></div>
	</div>
</form>