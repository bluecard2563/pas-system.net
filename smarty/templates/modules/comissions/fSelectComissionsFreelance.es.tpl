{assign var="title" value="LIQUIDACI&Oacute;N DE COMISIONES - AGENTES LIBRE"}
<div class="span-24 last title">
	Liquidaci&oacute;n de Comisiones a Agentes Libres<br />
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
<div class="span-17 prepend-7 last">
	<div class="span-10 {if $error eq 1}success{elseif $error eq 2}error{/if}" align="center">

	</div>
</div>
{/if}

<div class="span-10 prepend-7 append-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $freelance_with_comissions}	
<form name="frmSelectComissions" id="frmSelectComissions" method="post" target="_blank" action="pPayComission">
	<div class="span-24 last line separator">
		<label>Datos de la Liquidaci&oacute;n:</label>
	</div>

	<div class="prepend-4 span-17 last line">
		<table border="1" class="dataTable" id="comissions_table">
			<thead>
				<tr class="header">
					<th class="center">&nbsp;</th>
					<th class="center">
						Nombre
					</th>
					<th class="center">
						Documento
					</th>
					<th class="center">
						&Uacute;ltima Liquidaci&oacute;n
					</th>					
				</tr>
			</thead>

			<tbody>
			{foreach name="claims" from=$freelance_with_comissions item="c"}
			<tr style="padding:5px 5px 5px 5px; text-align:left;" {if $smarty.foreach.claims.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if}>
				<td class="tableField">
					<input type="radio" name="chkFreelance" id="chkFreelance_{$c.serial_fre}"  value="{$c.serial_fre}"/>
				</td>
				<td class="tableField">
					{$c.name_fre}
				</td>
				<td class="tableField">
					{$c.document_fre}
				</td>
				<td class="tableField">
					{$c.last_commission_paid_fre}
				</td>
			</tr>
			{/foreach}
			</tbody>
		</table>
	</div>

	<div class="span-24 last line">
		<div class="prepend-7 span-4 label">Fecha Hasta:</div>
		<input type="text" name="txtDateTo" id="txtDateTo" title='El campo "Fecha Hasta" es obligatorio' />
	</div>

	<div class="span-24 last line buttons">
		<input type="button" name="btnFindComissions" id="btnFindComissions" value="Verificar Comisiones" />
		<input type="hidden" name="selComissionsTo" id="selComissionsTo" value="FREELANCE" />
	</div>

	<div class="span-24 last line" id="preview_container"></div>	
</form>
{else}
<div class="prepend-7 span-10 append-7">
	<div class="span-10 error center">
		No existen registros a liquidar.
	</div>
</div>
{/if}