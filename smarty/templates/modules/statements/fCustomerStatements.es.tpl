{assign var="title" value="Declaración de Salud"}
<form name="frmCustomerStatement" id="frmCustomerStatement" action="" method="post" class="form_smarty">
    <div class="span-24 last title">
        Declaracion de Salud<br />
    </div>

    {if $customers}
        <div class="span-24 last line">
            <table border="1" class="dataTable" align="center" id="customerStatementsTable">
                <thead>
                    <tr class="header">
                        <th style="text-align: center;">Id.</th>
                        <th style="text-align: center;">Tarjeta</th>
                        <th style="text-align: center;">Documento</th>
                        <th style="text-align: center;">Nombre</th>
                        <th style="text-align: center;">Email</th>
                        <th style="text-align: center;">Origen</th>
                        <th style="text-align: center;">Fecha Creación</th>
                        <th style="text-align: center;" colspan="3">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                {foreach name="customer" from=$customers item=cus}
                    <tr {if $smarty.foreach.customer.iteration is even}class="odd {$cus.hidden}"{else}class="even {$cus.hidden}"{/if} >
                        <td class="tableField">{$cus.statement_id}</td>
                        <td class="tableField">{$cus.card_number}</td>
                        <td class="tableField">{$cus.customer_document}</td>
                        <td class="tableField">{$cus.customer_name}</td>
                        <td class="tableField">{$cus.customer_email}</td>
                        <td class="tableField">{$cus.system_origin_es}</td>
                        <td class="tableField">{$cus.created_at}</td>
                        <td class="tableField">
                            <a id="{$cus.statement_id}" class="{$cus.disabled}" href="{$cus.statement_link}" target="_blank">
                                <span class="fa fa-file-pdf-o fa-lg {$cus.disabled}" title="Ver Declaración de Salud"></span>
                            </a>
                        </td>
                        <td class="tableField">
                            <span id="{$cus.statement_id}" class="fa fa-envelope-o fa-lg pointer {$cus.disabled}" title="Reenviar declaración via email" onclick="sendCustomerStatementEmail(this)";></span>
                        </td>
                        <td class="tableField">
                            <label class="checkbox-container" title="Activar/Desactivar">
                                <input type="checkbox" id="{$cus.statement_id}" value="{$cus.statement_id}" {if $cus.status eq 'ACTIVE'}checked='checked'{/if} onclick="changeStatementStatus(this);">
                                <span class="checkmark"></span>
                            </label>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    {else}
        <div class="prepend-8 span-8 append-8 last" align="center">
            <div class="span-8 error last">
                No existen declaraciones.
            </div>
        </div>
    {/if}
</form>
