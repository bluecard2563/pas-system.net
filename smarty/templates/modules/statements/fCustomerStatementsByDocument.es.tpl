{assign var="title" value="Búsqueda de Declaración de Salud"}
<form id="frmCustomerStatementByDocument" name="frmCustomerStatementByDocument" action="" method="post"
      class="form_smarty">
    <div class="span-24 last line title" style="margin-bottom: 25px">
        BUSCAR DECLARACIÓN DE SALUD<br/>
        <label>(*) Campos Obligatorios</label>
    </div>

    <div class="prepend-7 span-10 append-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div id="searchContainer">
        <div id="customerContainer" class="span-24 last line" style="margin-bottom: 25px">
            <div class="prepend-6 span-6">
                <label>* Número de Identificación del Cliente:</label>
            </div>
            <div class="span-6 last">
                <input type="text" id="txtCustomerDocument" name="txtCustomerDocument" class="long_text" style="height: 20px;" onkeypress="onlyAlphanumericDigits(event)"/>
            </div>
        </div>

        <div class="span-24 last buttons line" style="margin-bottom: 25px;">
            <input type="button" id="btnSearch" name="btnSearch" value="Buscar" onclick="getCustomerStatementsByDocument()" style="width: 15%; height: 25px;"/>
        </div>
    </div>

    <div id="customerStatementContainer"></div>

    <div id="notFoundMessage" class="prepend-8 span-8 append-8 last" align="center">
        <div class="span-8 error last">
            No se encontraron Declaraciones de Salud para Cliente Solicitado.
        </div>
    </div>
</form>
