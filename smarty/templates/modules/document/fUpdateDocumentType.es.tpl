{assign var="title" value="ACTUALIZAR TIPO DE DOCUMENTO"}
<form name="frmUpdateDocumentType" id="frmUpdateDocumentType" method="post" action="{$document_root}modules/document/pUpdateDocumentType" class="form_smarty">
	<div class="span-24 last title ">
    	Actualizaci&oacute;n de Tipo de Documento
    </div>
    <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
    	<div class="prepend-8 span-4 label">
            <label>Tipo de Documento:</label>
        </div>
        <div class="span-8 append-4 last">
                <input type="text" name="txtNameDocumentType" id="txtNameDocumentType" title='El campo Tipo de Documento es obligatorio' {if $data.name_doc}value="{$data.name_doc}"{/if}>        
        </div>     
    </div>               

    <div class="span-24 last line label">
        * Asignaci&oacute;n del tipo de documento a los pa&iacute;ses.
    </div>

    <div class="span-24 last line" id="mandatoryCountry">
		<div class="span-24 last line" >
        <div class="prepend-5 span-6" id="zoneWrapper">
            <div class="span-2 label">* Zona:</div>
            <div class="span-3 last" id="zoneContainer0">
                <select name="selZone0" id="selZone0" title='El campo "Zona" es obligatorio.' {if $mandatory_manager.is_used eq "1"}disabled="disabled"{/if}>
                    <option value="">- Seleccione -</option>
                    {foreach from=$zonesList item="zl"}
                        <option value="{$zl.serial_zon}" {if $mandatory_manager.serial_zon eq $zl.serial_zon}selected="selected"{/if}>{$zl.name_zon}</option>
                    {/foreach}
                </select>
            </div>
        </div>

		<div class="span-9 append-4 last">
			<div class="span-3 label">* Pa&iacute;s:</div>
			<div class="span-4" id="countryContainer0">
				<select name="selCountry0" id="selCountry0" title='El campo "Pa&iacute;s" es obligatorio' {if $mandatory_manager.is_used eq "1"}disabled="disabled"{/if}>
					<option value="">- Seleccione -</option>
					{foreach from=$mandatory_manager.zone_countries item="zc"}
						<option value="{$zc.serial_cou}" {if $mandatory_manager.serial_cou eq $zc.serial_cou}selected="selected"{/if}>{$zc.name_cou}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<input type="hidden" name="0" id="0" class="cou_arr" value="{$mandatory_manager.serial_man}">
        <input type="hidden" name="is_used[0]" id="is_used0" value="{$mandatory_manager.is_used}">
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-3 label">* Representante:</div>
			<div class="span-5" id="managerContainer0">
				<select name="selManager0" id="selManager0" title="El campo Representante es obligatorio." {if $mandatory_manager.is_used eq "1"}disabled="disabled"{/if}>
					<option value="">- Seleccione -</option>
					{foreach from=$mandatory_manager.country_managers item="cm"}
					<option value="{$cm.serial_man}" {if $mandatory_manager.serial_man eq $cm.serial_man}selected="selected"{/if}>{$cm.name_man}</option>
					{/foreach}
				</select>
			</div>
		</div>


		<div class="span-24 last line" >
        <div class="prepend-5 span-6" id="typeWrapper">
            <div class="span-2 label">* Tipo:</div>
            <div class="span-4 last" id="typeContainer0">
                <select name="selType0" id="selType0" title='El campo "Tipo" es obligatorio' {if $mandatory_manager.is_used eq "1"}disabled="disabled"{/if}>
                    <option value="">- Seleccione -</option>
                    <option value="LEGAL_ENTITY" {if $mandatory_manager.type_dbm eq "LEGAL_ENTITY"}selected="selected"{/if}>Entidad Legal</option>
                    <option value="PERSON" {if $mandatory_manager.type_dbm eq "PERSON"}selected="selected"{/if}>Persona Natural</option>
                    <option value="BOTH" {if $mandatory_manager.type_dbm eq "BOTH"}selected="selected"{/if}>Ambos</option>
                </select>
             </div>
        </div>

		<div class="span-9" id="purposeWraper">
			<div class="span-3 label">* Uso:</div>
			<div class="span-4" id="purposeContainer0">
				<select name="selPurpose0" id="selPurpose0" title='El campo "Uso" es obligatorio' {if $mandatory_manager.is_used eq "1"}disabled="disabled"{/if}>
					<option value="">- Seleccione -</option>
					{foreach from=$purposeValues item="l"}
						<option value="{$l}" {if $l eq $mandatory_manager.purpose_dbm}selected{/if}>{$global_documentPurpose.$l}</option>
					{/foreach}
				</select>
			</div>
		</div>
			
		{if $mandatory_manager.is_used eq "1"}
		<div class="span-2 last">
			Usada.
		</div>
        {/if}
		</div>

		
		<input type="hidden" id="hdnUnique0" name="hdnUnique0" value="1" title="Por favor llene todos los campos para continuar." serial="0" />
		<input type="hidden" id="hdnSerial_dbm0" name="hdnSerial_dbm0" value="{$mandatory_manager.serial_dbm}" />
    </div>

    <div class="span-24 last line" id="countriesContainer">
        {foreach from="$assigned_managers" key="key" item="ac"}
            <div class="span-24 last line" id="manager{$key}">
                <div class="span-24 last line" >
					<div class="prepend-5 span-6">
						<div class="span-2 label">* Zona:</div>
						<div class="span-3" id="zoneContainer{$key}">
							<select name="selZone{$key}" id="selZone{$key}" title='El campo "Zona" es obligatorio.' onchange="loadCountries(this.value,{$key});" {if $ac.is_used eq '1'}disabled="disabled"{/if}>
								<option value="">- Seleccione -</option>
								{foreach from=$zonesList item="zl"}
									<option value="{$zl.serial_zon}" {if $ac.serial_zon eq $zl.serial_zon}selected="selected"{/if}>{$zl.name_zon}</option>
								{/foreach}
							</select>
						</div>
					</div>

					<div class="span-9 append-4 last">
						<div class="span-3 label">* Pa&iacute;s:</div>
						<div class="span-5" id="countryContainer{$key}">
							<select name="selCountry{$key}" id="selCountry{$key}" onchange="addSelectedCountry(this.value,{$key});" title='El campo "Pa&iacute;s" es obligatorio' {if $ac.is_used eq "1"}disabled="disabled"{/if}>
								<option value="">- Seleccione -</option>
								{foreach from=$ac.zone_countries item="zc"}
									<option value="{$zc.serial_cou}" {if $ac.serial_cou eq $zc.serial_cou}selected="selected"{/if}>{$zc.name_cou}</option>
								{/foreach}
							</select>
						</div>
						<input type="hidden" name="{$key}" id="{$key}" class="cou_arr" value="{$ac.serial_cou}">
						<input type="hidden" name="is_used[{$key}]" id="is_used{$key}" value="{$ac.is_used}">
					</div>
				</div>

				<div class="span-24 last line">
					<div class="prepend-4 span-3 label">* Representante:</div>
					<div class="span-5" id="managerContainer{$key}">
						<select name="selManager{$key}" id="selManager0" title="El campo Representante es obligatorio." {if $ac.is_used eq "1"}disabled="disabled"{/if}>
							<option value="">- Seleccione -</option>
							{foreach from=$ac.country_managers item="cm"}
							<option value="{$cm.serial_man}" {if $ac.serial_man eq $cm.serial_man}selected="selected"{/if}>{$cm.name_man}</option>
							{/foreach}
						</select>
					</div>
				</div>

				<div class="span-24 last line" >
					<div class="prepend-5 span-6">
						<div class="span-2 label">* Tipo:</div>
						<div class="span-4 last" id="typeContainer{$key}">
							<select name="selType{$key}" id="selType{$key}" title='El campo "Tipo" es obligatorio' {if $ac.is_used eq "1"}disabled="disabled"{/if}>
								<option value="">- Seleccione -</option>
								<option value="LEGAL_ENTITY" {if $ac.type_dbm eq "LEGAL_ENTITY"}selected="selected"{/if}>Entidad Legal</option>
								<option value="PERSON" {if $ac.type_dbm eq "PERSON"}selected="selected"{/if}>Persona Natural</option>
								<option value="BOTH" {if $ac.type_dbm eq "BOTH"}selected="selected"{/if}>Ambos</option>
							</select>
						 </div>
					</div>

					<div class="span-9" id="purposeWraper">
						<div class="span-3 label">* Uso:</div>
						<div class="span-4">
							<select name="selPurpose{$key}" id="selPurpose{$key}" title='El campo "Uso" es obligatorio' {if $ac.is_used eq "1"}disabled="disabled"{/if}>
								<option value="">- Seleccione -</option>
								{foreach from=$purposeValues item="l"}
									<option value="{$l}" {if $l eq $ac.purpose_dbm}selected{/if}>{$global_documentPurpose.$l}</option>
								{/foreach}
							</select>
						</div>
					</div>

					<div class="span-2 last">
					{if $ac.is_used eq "0"}
						<a href="#" onclick="deleteManager('{$key}'); return false;" id="delete{$key}">Eliminar</a>
					{else}
						Usada
					{/if}
					</div>
					<input type="hidden" id="hdnUnique{$key}" name="hdnUnique{$key}" value="1" title="Por favor llene todos los campos para continuar.({$key+1})" serial="{$key}" />
					<input type="hidden" id="hdnSerial_dbm{$key}" name="hdnSerial_dbm{$key}" value="{$ac.serial_dbm}" />
				</div>
            </div>
        {/foreach}
    </div>

    <div class="span-24 last line">
        <div class="prepend-19 span-5">
            <input type="button" name="btnAddCountry" id="btnAddCountry" value="Asignar otro pa&iacute;s" >
        </div>
    </div>
    
    <div class="span-24 last buttons line">
    	<input maxlength="30" type="submit" name="btnUpdate" id="btnUpdate" value="Actualizar" class="" >
        <input type="hidden" name="hdnSerial_doc" id="hdnSerial_doc" {if $data.serial_doc}value="{$data.serial_doc}"{/if} >
		<input type="hidden" name="managersCount" id="managersCount" value="{if $count > 0}{$count-1}{else}0{/if}">
    </div>
</form>