{assign var="title" value="BUSCAR TIPO DE DOCUMENTO"}
<form name="frmSearchDocumentType" id="frmSearchDocumentType" action="{$document_root}modules/document/fUpdateDocumentType" method="post" class="form_smarty">
	<div class="span-24 last title ">
    	Buscar Tipo de Documento
    </div>
    
    {if $message}     
        <div class="prepend-6 span-12 last">
            <div class="{if $message eq 1}success{else}error{/if}" align="center">
                {if $message eq 1}
                    El tipo de documento se actualiz&oacute; correctamente!
                {elseif $message eq 3}
                    No se pudo asignar el tipo de documento para un pa&iacute;s determinado, ya que
					existe otro documento para la raz&oacute;n social elegida.
                {elseif $message eq 2}
                    No existe el tipo de documento ingresado. Por favor seleccione uno de la lista.
				{elseif $message eq 4}
                    No se pudieron limpiar registros anteriores del representante.
				{elseif $message eq 5}
					No se pudo actualizar la informaci&oacute;n general del documento.
				{elseif $message eq 6}
					No se pudo cargar la informaci&oacute;n del documento. Lo sentimos.
                {/if}                                 
            </div>
        </div>
    {/if}
    
    <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    
   <!-- <div class="span-24 last line">
        <div class="prepend-7 span-5 label">Ingrese el tipo de documento:</div>
        
        <div class="span-8 append-4 last">
                <input type="text" id="txtNameDocumentType" name="txtNameDocumentType" title="El campo 'Tipo de Documento' es obligatorio."  />
        </div>
   </div>-->

{if $documentTypeList}
<div class="prepend-6 span-12 append-6 last line">
	<table border="0" id="tblCurrency" style="color: #000000;">
		<THEAD>
			<tr bgcolor="#284787">
				<td align="center" class="tableTitle">
					No.
				</td>
				<td align="center" class="tableTitle">
					Nombre
				</td>
				<td align="center" class="tableTitle">
				</td>
			</tr>
		</THEAD>
		<TBODY>
			{foreach name="alert" from=$documentTypeList item="dt"}
			<tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
				<td align="center" class="tableField">
					{$smarty.foreach.alert.iteration}
				</td>
				<td align="center" class="tableField">
					{$dt.name_doc}
				</td>
				<td align="center" class="tableField">
					<a href="{$document_root}modules/document/fUpdateDocumentType/{$dt.serial_doc}">Actualizar</a>
				</td>
			</tr>
			{/foreach}
		</TBODY>
	</table>
</div>
<div class="span-24 last buttons" id="pageNavPosition"></div>
{else}
<div class="prepend-8 span-8 append-8 last" align="center">
	<div class="span-8 error last">
		No existen Documentos ingresados.
	</div>
</div>
{/if}
    
    <!--<div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="serial_doc" id="serial_doc" value="" />-->
</form>