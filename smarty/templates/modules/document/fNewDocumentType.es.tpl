{assign var=title value="NUEVO TIPO DE DOCUMENTO"}
<form name="frmNewDocumentType" id="frmNewDocumentType" method="post" action="{$document_root}modules/document/pNewDocumentType" class="">
    <div class="span-24 last title ">
        Registro de Nuevo Tipo de Documento
    </div>
    {if $error}
    <div class="prepend-6 span-12 last">
        {if $error eq 1}
            <div class="success" align="center">
                El tipo de documento se ha registrado exitosamente!
            </div>
        {elseif $error eq 2}
            <div class="error">
                No se pudo insertar el tipo de documento.
            </div>
		{elseif $error eq 3}
            <div class="error">
                No se pudo asignar el tipo de documento para un Representante determinado, ya que
				existe otro documento para la raz&oacute;n social elegida.
            </div>
        {/if}
    </div>
    {/if}
    
   <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
   </div>
     
   <div class="span-24 last line">
        <div class="prepend-8 span-4 label">Tipo de Documento:</div>
        <div class="span-8 append-4 last">
                <input type="text" name="txtNameDocumentType" id="txtNameDocumentType" title="El campo 'Tipo de Documento' es obligatorio."/>
        </div>
   </div>

    <div class="span-24 last line label">
        * Asignaci&oacute;n del tipo de documento a los Representantes.
    </div>

    <div class="span-24 last line" id="mandatoryCountry">
		<div class="span-24 last line" >
			<div class="prepend-5 span-6" id="zoneWrapper">
				<div class="span-2 label">* Zona:</div>
				<div class="span-3 last" id="zoneContainer0">
					<select name="selZone0" id="selZone0" title='El campo "Zona" es obligatorio.'>
						<option value="">- Seleccione -</option>
						{foreach from=$zonesList item="zl"}
							<option value="{$zl.serial_zon}">{$zl.name_zon}</option>
						{/foreach}
					</select>
				</div>
			</div>

			<div class="span-9 append-4 last">
				<div class="span-3 label">* Pa&iacute;s:</div>
				<div class="span-4" id="countryContainer0">
					<select name="selCountry0" id="selCountry0" onchange="addSelectedCountry(this.val,0)" title='El campo "Pa&iacute;s" es obligatorio'>
						<option value="">- Seleccione -</option>
					</select>
				</div>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-3 label">* Representante:</div>
			<div class="span-5" id="managerContainer0">
				<select name="selManager0" id="selManager0" title='El campo "Representante" es obligatorio.'>
					<option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

			<div class="span-24 last line" >
			<div class="prepend-5 span-6" id="typeWrapper">
				<div class="span-2 label">* Tipo:</div>
				<div class="span-3 last" id="typeContainer0">
					<select name="selType0" id="selType0" title='El campo "Tipo" es obligatorio'>
						<option value="">- Seleccione -</option>
						<option value="LEGAL_ENTITY">Entidad Legal</option>
						<option value="PERSON">Persona Natural</option>
						<option value="BOTH">Ambos</option>
					</select>
				 </div>
			</div>

			<div class="stand-9 append-5 last" id="purposeWraper">
				<div class="span-3 label">* Uso:</div>
				<div class="span-4" id="purposeContainer0">
					<select name="selPurpose0" id="selPurpose0" title='El campo "Uso" es obligatorio'>
						<option value="">- Seleccione -</option>
						{foreach from=$purposeValues item="l"}
							<option value="{$l}">{$global_documentPurpose.$l}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>

		<input type="hidden" id="hdnUnique0" name="hdnUnique0" value="" title="Ya existe un documento para el tipo de persona seleccionada." />
    </div>

    <div class="span-24 last line" id="countriesContainer"></div>
    <input type="hidden" name="managersCount" id="managersCount" value="0"/>
    <div class="span-24 last line">
        <div class="prepend-19 span-5">
            <input type="button" name="btnAddCountry" id="btnAddCountry" value="Asignar otro pa&iacute;s" />
        </div>
    </div>
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Insertar" />
    </div>
</form>
    