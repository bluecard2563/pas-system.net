{assign var="title" value="Control de Entrega de Incentivos"}
<form name="frmIncentiveDelivery" id="frmIncentiveDelivery" action="" method="post" class="form_smarty">

    <div class="span-24 last title">
        Control de Entrega de Incentivos<br/>
    </div>

    {if $accessAllowed}
        {if $incentives}
            <div class="span-24 last line">
                <table border="1" class="dataTable" align="center" id="incentiveDeliveryTable">
                    <thead>
                    <tr class="header">
                        <th style="text-align: center;">No.</th>
                        <th style="text-align: center;">Sucursal</th>
                        <th style="text-align: center;">Comercializador</th>
                        <th style="text-align: center;">Total</th>
                        <th style="text-align: center;">Bonos</th>
                        <th style="text-align: center;">Fecha Creación</th>
                        <th style="text-align: center;" colspan="4">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach name="incentive" from=$incentives item=ins}
                        <tr {if $smarty.foreach.customer.iteration is even}class="odd {$cus.hidden}" {else}class="even {$cus.hidden}"{/if}>
                            <td class="tableField">{$ins.serial_idc}</td>
                            <td class="tableField">{$ins.name_man}</td>
                            <td class="tableField">{$ins.name_dea}</td>
                            <td class="tableField">{$ins.total}</td>
                            <td class="tableField">{$ins.bonus}</td>
                            <td class="tableField">{$ins.created_at}</td>
                            <td class="tableField">
                                <label class="checkbox-container" title="Registrar Entrega">
                                    <input type="checkbox" id="{$ins.serial_idc}" value="{$ins.serial_idc}" {if $ins.delivered eq '1'}checked='checked'{/if} onclick="updateDelivery(this);">
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        {else}
            <div class="prepend-8 span-8 append-8 last" align="center">
                <div class="span-8 error last">
                    No existen registros.
                </div>
            </div>
        {/if}
    {else}
        <div class="prepend-8 span-8 append-8 last" align="center">
            <div class="span-8 error last">
                No se permite el acceso a esta información para usuarios asignados como DEALER.
            </div>
        </div>

    {/if}
</form>