<!--/*
File: fNewPayment.es.tpl
Author: Edwin Salvador
Creation Date: 12/03/2010
Last Modified:
Modified by:
*/-->
{$salesCost}

{assign var="title" value="CUENTAS POR COBRAR"}
<form name="frmPayment" id="frmPayment" method="post" action="{$document_root}modules/payments/pNewPayment">
    <div class="span-24 last line title">
        Cuentas por Cobrar
    </div>
    <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Info. de Sucursal de Comercializador:</label>
    </div>
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Comercializador:</div>
        <div class="span-5">
            {$dealerData.name_dea}
        </div>
        <div class="span-3 label">Sucursal de Comercializador:</div>
        <div class="span-5">
            {$branchData.name_dea}
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">C&oacute;digo:</div>
        <div class="span-5">
            {$branchData.branch_code}
        </div>
        <div class="span-3 label">Dias de Cr&eacute;dito:</div>
        <div class="span-5">
            {$branchData.credit_day}
        </div>
    </div>
    
    <div class="span-24 last line separator">
        <div class="span-5">
            <label>Facturas a Pagar:</label>
        </div>
    </div>
    <div class="prepend-4 span-15 line last">
        <table border="0" id="invoicesTable">
            <THEAD>
                <tr bgcolor="#284787">
                    <td align="center" class="tableTitle">
                        # Factura
                    </td>
                    <td align="center" class="tableTitle">
                        Facturado a
                    </td>
                    <td align="center" class="tableTitle">
                        Fecha Factura
                    </td>
                    <td align="center" class="tableTitle">
                        Total
                    </td>
                    {if $countInvoices eq 1}
                        <td align="center" class="tableTitle">
                            Abonos
                        </td>
                        <td align="center" class="tableTitle">
                            Por Pagar
                        </td>
                    {/if}
                </tr>
            </THEAD>
            <TBODY>
                {foreach name="invoices" from=$invoicesList item="i"}
                    <tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if} >
                        <td align="center" class="tableField">
                            {$i.number_inv}
                        </td>
                        <td class="tableField">
                            {$i.invoice_to}
                        </td>
                        <td align="center" class="tableField">
                           {$i.date_inv}
                        </td>
                        <td class="tableFieldNumber">
                            {if $i.status_inv eq 'STAND-BY'}
                                {$i.total_inv}
                            {/if}
                        </td>
                        {if $countInvoices eq 1}
                            <td class="tableFieldNumber">
                                {$i.total_payed}
                            </td>
                            <td class="tableFieldNumber">
                            {if $i.status_inv eq 'STAND-BY'}
                                {$i.total_to_pay}
                            {else}
                                {$i.total_to_pay_fee}
                            {/if}
                                
                            </td>
                        {/if}
                    </tr>
                {/foreach}
                {if $countInvoices neq 1}
                <tr bgcolor="#00b3f0">
                    <td></td>
                    <td></td>
                    <th align="center" class="tableField">
                        TOTAL:
                    </th>
                    <th class="tableFieldNumber">
                        {$total}
                    </th>
                </tr>
                {/if}
            </TBODY>
        </table>
    </div>
    {if $details}
        <div class="span-24 last line separator">
            <div class="span-5">
                <label>Detalle de abonos realizados:</label>
            </div>
        </div>
        <input type="hidden" id="serial_pay" name="serial_pay" value="{$details.0.serial_pay}">
        <div class="prepend-2 span-20 line last">
        <table border="0" id="invoicesTable">
            <THEAD>
                <tr bgcolor="#284787">
                    <td align="center" class="tableTitle">
                        Fecha
                    </td>
                    <td align="center" class="tableTitle">
                        Tipo
                    </td>
                    <td align="center" class="tableTitle">
                        Descripci&oacute;n
                    </td>
                    <td align="center" class="tableTitle">
                        #Documento
                    </td>
                    <td align="center" class="tableTitle">
                        Monto
                    </td>
                    <td align="center" class="tableTitle">
                        Estado
                    </td>
                    <td align="center" class="tableTitle">
                        Observaciones
                    </td>
                </tr>
            </THEAD>
            <TBODY>
                {foreach from="$details" item="d" name="details" key="key"}
                    <tr {if $d.status_pyf eq "ACTIVE"}{if $smarty.foreach.details.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if}{else}bgcolor="#d7e8f9"{/if}>
                        <td class="tableField">
                            {$d.date_pyf}
                        </td>
                        <td class="tableField">
                            {$global_paymentForms[$d.type_pyf]}
                        </td>
                        <td class="tableField">
                            {$d.comments_pyf}
                        </td>
                        <td class="tableField">
                            {$d.document_number_pyf}
                        </td>
                        <td class="tableFieldNumber">
                            {$d.amount_pyf}
                        </td>
                        <td class="tableField">
                            {$global_paymentStatus[$d.status_pyf]}
                        </td>
                        <td class="tableField">
                            {$d.observation_pyf}
                        </td>
                    </tr>
                {/foreach}
                <tr bgcolor="#284787">
                    <td></td>
                    <td></td>
                    <td></td>
                    <th align="center" class="tableTitle">
                        TOTAL PAGOS ACTIVOS:
                    </th>
                    <th class="tableTitle">
                        {$invoicesList.0.total_payed}
                    </th>
                </tr>
            </TBODY>
        </table>
		<input type="hidden" name="total_payed" id="total_payed" value="{$invoicesList.0.total_payed}" />
    </div>

    {/if}
    <input type="hidden" name="total_to_pay" id="total_to_pay" value="{$total_pending}" />
    <input type="hidden" name="count_invoices" id="count_invoices" value="{$countInvoices}" />
    <input type="hidden" name="serial_invs" id="serial_invs" value="{$invoicesIds}" />
    <div class="span-24 last line separator">
        <div class="span-5">
            <label>Forma de Pago:</label>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">Tipo</div>
        <div class="span-5 label">Descripci&oacute;n</div>
        <div class="span-5 label"># Documento</div>
        <div class="span-5 last label">Valor</div>
    </div>
    <div id="paymentsContainer" class="span-24 last line">
        <div id="form_0" class="span-24 last line">
            <input type="hidden" name="serial_inv"/>
            <div class="prepend-2 span-5 label">
                <select name="payments[0][paymentType]" id="paymentType_0" title='El campo "Tipo" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from="$formsList" item="fl"}
                        <option value="{$fl}">{$global_paymentForms.$fl}</option>
                    {/foreach}
                </select>
            </div>
            <div class="span-5 label ">
                <input type="text" name="payments[0][paymentDesc]" id="paymentDesc_0" class="span-4" title='El campo "Descripci&oacute;n" es obligatorio.' />
                <select name="payments[0][paymentCard]" id="paymentCard_0" title='El campo "Descripci&oacute;n" es obligatorio.' style="display: none;">
                    <option value="">- Seleccione -</option>
                    {foreach from="$creditCards" item="c"}
                        <option value="{$c}">{$c}</option>
                    {/foreach}
                    <option value="OTHER">Otro</option>
                </select>
                <select name="payments[0][paymentExcess]" id="paymentExcess_0" title='El campo "Descripci&oacute;n" es obligatorio.' style="display: none;">
                    <option value="">- Seleccione -</option>
                    {foreach from="$payExcessList" item="c"}
                        <option value="{$c.serial_pay}">Ex: {$c.excess_amount_available_pay}</option>
                    {/foreach}
                </select>
                <select name="payments[0][creditNotes]" id="creditNotes_0" title='El campo "Descripci&oacute;n" es obligatorio.' style="display: none;">
                    <option value="">- Seleccione -</option>
                    {foreach from="$creditNoteList" item="c"}
                        <option value="{$c.serial_cn}" number="{$c.number_cn}">NC: {$c.amount_cn}</option>
                    {/foreach}
                </select>
                <select name="payments[0][paymentBank]" id="paymentBank_0" title='El campo "Descripci&oacute;n" es obligatorio.' style="display: none;">
                    <option value="">- Seleccione -</option>
                    {foreach from="$banks" item="b"}
                        <option value="{$b}">{$b}</option>
                    {/foreach}
                    <option value="OTHER">Otro</option>
                </select>
                <input type="text" name="payments[0][paymentOther]" id="paymentOther_0" class="span-4" value="Especifique" title='El campo "Descripci&oacute;n" es obligatorio.' style="display: none;" />
            </div>
            <div class="span-5 label">
                &nbsp;
                <input type="text" name="payments[0][paymentDocument]" id="paymentDocument_0" class="span-3" title='El campo "# Documento" es obligatorio.' />
            </div>
            <div class="span-5 last label">
                <input type="text" name="payments[0][paymentVal]" id="paymentVal_0" class="span-2 formVal" title='El campo "Valor" es obligatorio.' />
            </div>
        </div>
    </div>
    <input type="hidden" name="count" id="count" value="0" />
    <input type="hidden" name="status_pay" id="status_pay" value="" />
    <input type="hidden" name="serial_dea" id="serial_dea" value="{$dealerData.serial_dea}" />
    <div class="prepend-14 span-10 last line">
        <div class="span-5">
            <input type="button" name="btnAddForm" id="btnAddForm" value="A&ntilde;adir Forma de Pago" class=" span-5 ui-state-default ui-corner-all" />
        </div>
        <div class="span-5 last" >
            <input type="button" name="btnDeleteForm" id="btnDeleteForm" value="Eliminar Forma de Pago" class=" span-5 ui-state-default ui-corner-all" style="display: none;" />
        </div>
    </div>

    <div class="prepend-9 span-24 line last">
        <div class="span-3"><strong>Total por pagar: </strong></div>
        <div class="span-2 last">{$total_pending}</div>
    </div>
    <div class="prepend-9 span-24 line last">
        <div class="span-3"><strong>Total Pago Actual: </strong></div>
        <div id="curPayment" class="span-2 last">0.00</div>
    </div>
    <div class="prepend-9 span-24 line last">
        <div id="textPending" class="span-3"><strong>Saldo Pendiente: </strong></div>
        <div id="pending" class="span-2 last"><strong>{$total_pending}</strong></div>
    </div>
    <div class="prepend-11 span-5 last line">
        <input type="submit" name="btnPay" id="btnPay" value="Pagar" class="ui-state-default ui-corner-all" />
    </div>
</form>