{assign var="title" value="PAGOS CON TARJETA DE CR&Eacute;DITO"}
{if $use_converge}
    <form name="frmGlobalPayment" id="frmGlobalPayment" method="post" action="{$document_root}modules/estimator/epayment/executeCustomPayment/GLOBAL">
{else}
    <form name="frmGlobalPayment" id="frmGlobalPayment" method="post" action="{$document_root}modules/payments/creditCardPayments/pGlobalPayment">
{/if}
    <div class="span-24 last title line">
        Pagos con tarjeta de cr&eacute;dito<br />
        <label>(*) Campos Obligatorios</label>
    </div>
	
	{if $error}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            {if $error eq 1}
                Pago realizado exitosamente!
	    {else if $error eq 2}
                Hubo errores al procesar el pago. Por favor cont&aacute;ctese con el administrador.
            {/if}
        </div>
    </div>
    {/if}
	
	{if $merchantError}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 error" align="center">
            {handle_merchant_account_errors error=$merchantError}
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last title line">
        <img src="{$document_root}img/credit_card_logos/amex.gif" alt="American Express" />
        <img src="{$document_root}img/credit_card_logos/discover.gif" alt="Discover" />
        <img src="{$document_root}img/credit_card_logos/master.gif" alt="Master Card" />
        <img src="{$document_root}img/credit_card_logos/visa.gif" alt="Visa" />
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Total a pagar:</div>
        <div class="span-5">
			<input type="text" name="txtTotalAmount" id="txtTotalAmount" title="El campo 'Total a Pagar' es obligatorio" />
		</div>
    </div>
	
	<div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Concepto</div>
        <div class="span-4">
			<input type="text" name="txtObservations" id="txtObservations" style="width: 350px;" title='El campo "Concepto" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* N&uacute;mero de tarjeta:</div>
        <div class="span-4">
            <input type="text" name="txtCardNum" id="txtCardNum" title='El campo "N&uacute;mero de tarjeta" es obligatorio.' />
        </div>

        <div class="span-5 label">* C&oacute;digo de seguridad:</div>
        <div class="span-4 append-4 last">
            <input type="text" name="txtSecCode" id="txtSecCode" size="4" title='El campo "C&oacute;digo de seguridad" es obligatorio.' />
        </div>
    </div>
    
    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Nombre en la tarjeta:</div>
        <div class="span-4">
            <input type="text" name="txtNameCard" id="txtNameCard" title='El campo "Nombre en la tarjeta" es obligatorio.' />
        </div>

        <div class="span-5 label">* Fecha de vencimiento:</div>
        <div class="span-6 append-2 last">
            <select name="selMonth" id="selMonth" title='El campo "Fecha de vencimiento (Mes)" es obligatorio.'>
                <option value="">-Seleccione-</option>
                {foreach from=$global_weekMonths key="k" item="l"}
                <option value="{if $k<10}0{/if}{$k}">{$l} ({$k})</option>
                {/foreach}
            </select>
            <select name="selYear" id="selYear" title='El campo "Fecha de vencimiento (A&ntilde;o)" es obligatorio.'>
                <option value="">-Seleccione-</option>
                {foreach from=$yearlist item="l"}
                <option value="{$l}">{$l}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Pa&iacute;s:</div>
        <div class="span-5">
            <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.' >
                <option value="">-Seleccione-</option>
                {foreach from=$countryList item="l"}
                <option value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>

        <div class="span-4 label">* Ciudad:</div>
        <div class="span-6 append-2 last" id="cityContainer">
            <select name="selCityCustomer" id="selCityCustomer" title='El campo "Ciudad" es obligatorio.' >
                <option value="">-Seleccione-</option>
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Estado/Provincia:</div>
        <div class="span-4">
            <input type="text" name="txtState" id="txtState" title='El campo "Estado/Provincia" es obligatorio.' />
        </div>

        <div class="span-5 label">* C&oacute;digo Postal/Zip:</div>
        <div class="span-6 append-2 last">
            <input type="text" name="txtZip" id="txtZip" title='El campo "C&oacute;digo Postal/Zip" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Tel&eacute;fono:</div>
        <div class="span-4">
            <input type="text" name="txtPhone" id="txtPhone" title='El campo "Tel&eacute;fono" es obligatorio.' />
        </div>

        <div class="span-5 label">* E-mail:</div>
        <div class="span-6 append-2 last">
            <input type="text" name="txtMail" id="txtMail" title='El campo "E-mail" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-2 span-5 label">* Direcci&oacute;n:</div>
        <div class="span-4">
			<input type="text" name="txtAddress" id="txtAddress" style="width: 350px;" title='El campo "Direcci&oacute;n" es obligatorio.' />
        </div>
    </div>

    <div class="span-24 last buttons line">
        <input type="submit" name="payInvoices" id="payInvoices" value="Procesar Pago" />
    </div>
</form>
