{assign var="title" value="PAGOS CON TARJETA DE CR&Eacute;DITO"}
<form name="frmChooseInvoice" id="frmChooseInvoice" method="post">
    <div class="span-24 last title line">
        Pagos con tarjeta de cr&eacute;dito<br />
        <label>(*) Campos Obligatorios</label>
    </div>

    {if $error}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 {if $error eq 1 or $error eq 7}success{else}error{/if}" align="center">
            {if $error eq 1}
                Pago realizado exitosamente!
                {if $fileNumber}<br /><b></b><a id="printCreditNote" href="{$document_root}modules/payments/pPrintPaymentNote/{$fileNumber}" target="_blank">Imprimir Nota de Pago</a>{/if}
            {elseif $error eq 2}
                Debe seleccionar al menos una factura.
            {elseif $error eq 3}
                Hubo errores en el cobro. Por favor vuelva a intentarlo.
            {elseif $error eq 4}
                Hubo errores registrar los detalles del pago. Por favor vuelva a intentarlo.
            {elseif $error eq 5}
                Existi&oacute; un error al actualizar la factura. Por favor p&oacute;ngase en contacto con el administrador.
            {elseif $error eq 6}
                Existi&oacute; un error al actualizar una de las ventas. Por favor contacte al administrador inmediatamente.
            {elseif $error eq 7}
                Pago enviado para aprobaci&oacute;n en PayPhone app de su celular
            {elseif $error eq 8}
                {$error_description}
            {/if}
        </div>
    </div>
    {/if}

    {if $merchantError}
    <div class="append-7 span-10 prepend-7 last line">
        <div class="span-10 error" align="center">
            {handle_merchant_account_errors error=$merchantError}
        </div>
    </div>
    {/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    {if $data.userType neq "DEALER"}
	{if $data.countryList or $data.cityList}
        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">{if  $data.userType eq "PLANETASSIST"}* {/if}Pa&iacute;s:</div>
            <div class="span-5">
                {if  $data.userType eq "PLANETASSIST"}
                    <select name="selCountry" id="selCountry" title='El campo "Pa&iacute;s" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                        {foreach name="countries" from=$data.countryList item="l"}
                            <option value="{$l.serial_cou}"{if $smarty.foreach.countries.total eq 1}selected{/if}>{$l.name_cou}</option>
                        {/foreach}
                    </select>
                {else}{$data.nameCountry}{/if}
            </div>

            <div class="span-3 label">{if  $data.userType eq "PLANETASSIST"}* {/if}Representante:</div>
            <div class="span-4 append-4 last" id="managerContainer">
                {if  $data.userType eq "PLANETASSIST"}
                    <select name="selManager" id="selManager" title='El campo "Representante" es obligatorio.'>
                        <option value="">- Seleccione -</option>
                    </select>
                {else}
                    {$data.nameManager}
                    <input type="hidden" name="hdnSerialMbc" id="hdnSerialMbc" value="{$data.serialMbc}"/>
                {/if}
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Ciudad:</div>
            <div class="span-5"  id="cityContainer">
                <select name="selCity" id="selCity" title='El campo "Ciudad" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {if $data.cityList}
                    {foreach name="cityList" from=$data.cityList item="l"}
                        <option value="{$l.serial_cit}" {if $smarty.foreach.cityList.total eq 1}selected{/if}>{$l.name_cit}</option>
                    {/foreach}
                    {/if}
                </select>
            </div>

            <div class="span-3 label">* Responsable:</div>
            <div class="span-4 append-4 last" id="responsibleContainer">
                <select name="selResponsible" id="selResponsible" title='El campo "Responsable" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>

        <div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Comercializador:</div>
            <div class="span-5" id="dealerContainer">
                <select name="selDealer" id="selDealer" title='El campo "Comercializador" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
		</div>
		<div class="span-24 last line">
            <div class="prepend-3 span-4 label">* Sucursal:</div>
            <div class="span-4 append-4 last" id="branchContainer">
                <select name="selBranch" id="selBranch" title='El campo "Sucursal" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                </select>
            </div>
        </div>
	{else}
		<center><b>Por favor, espere un momento.</b></center>
	{/if}
    {/if}

    <div class="span-24 last line" id="invoiceContainer">
        {if $invoicesList}
        <table border="0" id="invoicesTable">
            <THEAD>
                <tr bgcolor="#284787">
                    <th align="center">
                        <input type="checkbox" id="selAll" name="selAll">
                    </th>
                    {foreach from=$titles item="t"}
                    <th align="center">
                        {$t}
                    </th>
                    {/foreach}
                </tr>
            </THEAD>
            <TBODY>
                {foreach name="invoices" from=$invoicesList item="i"}
                <tr {if $smarty.foreach.invoices.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e8f2fb"{/if} >
                    <td align="center" class="tableField">
                        <input type="checkbox" class="invoiceChk" value="{$i.serial_inv}" name="chk" id="chkInvoice{$i.serial_inv}" payment="{$i.serial_pay}" />
                    </td>
                    <td align="center" class="tableField">
                       {$i.date_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.number_inv}
                    </td>
                    <td align="center" class="tableField">
                        {$i.invoice_to}
                    </td>
                    <td align="center" class="tableField">
                        {$i.days}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days lt 31}
                            {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                        {/if}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days gt 30 && $i.days lt 61}
                            {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                        {/if}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days gt 60 && $i.days lt 91}
                            {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                        {/if}
                    </td>
                    <td align="center" class="tableField">
                        {if $i.days gt 90}
                            {if $i.status_inv eq 'STAND-BY'}{$i.total_to_pay}{else}{$i.total_to_pay_fee}{/if}
                        {/if}
                    </td>
                </tr>
                {/foreach}
            </TBODY>
        </table>
        <div class="span-24 last line pageNavPosition" id="pageNavPosition"></div>

        <div class="span-24 last">
            <br/>* (A): Abono{*, (CI): Costo Internacional*}<br />
        </div>

        <div class="span-24 last buttons line">
            {if $convergeButton}<input type="submit" name="pay_cd" id="pay_cd" value="Pagar Tarjeta Cr&eacute;dito" />{/if}
            {if $payPhoneButton}<input type="submit" name="pay_pp" id="pay_pp" value="Pagar PayPhone" />{/if}
        </div>
        {else}
            {if $data.userType eq "DEALER"}<center><b>No existen cuentas por cobrar para esta Sucursal de Comercializador.</b></center>{/if}
        {/if}
    </div>
    
    <input type="hidden" name="selectedDealer" id="selectedDealer" value="{$data.dealerID}" />
    <input type="hidden" name="selectedInvoices" id="selectedInvoices" title="Debe seleccionar una venta." />
    <input type="hidden" id="selectedBoxes" name="selectedBoxes"/>
    <input type="hidden" name="payedSelected" id="payedSelected" value="0" />
</form>