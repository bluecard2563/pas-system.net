<!--/*
File: fRegisterPayment.es.tpl
Author: Gabriela Guerrero
Creation Date: 16/06/2010
Last Modified: 12/07/2011
Modified by: Santiago Arellano
*/-->
{assign var="title" value="REGISTRAR PAGO"}
<form target="_blank" name="frmRegisterPayment" id="frmRegisterPayment" method="post" action="">
    <div class="span-24 last">
		 <div class="prepend-9 span-5 last title">
			 Registro de Pagos
		</div>
		<div class="prepend-10 span-5 last line">
			<label>(*) Campos Obligatorios</label>
		</div>
		<div class="append-8 span-10 prepend-6 last " id="messageContainer">
			<div class="span-12 last error" id="messageCountry" align="center" style="display: none;">
				 Pa&iacute;s es requerido
			</div>
			<div class="span-12 last error" id="messageManager" align="center" style="display: none;">
				Representante es requerido
			</div>
			<div class="span-12 last error" id="messageDates" align="center" style="display: none;">
				Seleccione una fecha
			</div>
		</div>
    </div>
{if $error}
    <div class="append-8 span-10 prepend-6 last line" id="messageContainer">
    	{if $error eq 1}
           <div class="span-12 success" align="center">
            	Se ha registrado los pagos.
            </div>        
        {/if}
		{if $error eq 2}
           <div class="span-12 error" align="center">
            	Seleccione los campos obligatorios.
            </div>
        {/if}
		{if $error eq 3}
           <div class="span-12 error" align="center">
            	Hubo problemas al registrar todos los pagos.
            </div>
        {/if}
    </div>
 {/if}
    <div class="span-7 span-10 prepend-7 last line">
        <ul id="alerts" class="alerts"></ul>
    </div>
    {if $countryList}
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
            <select name="selCountry" id="selCountry">
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" >{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Representante:</div>
        <div class="span-5 append-3 last" id="managerContainer">
            <select name="selManager" id="selManager">
				<option value="">- Seleccione -</option>
			 </select>
         </div>
    </div>

	<div class="span-24 last line">
		<div class="prepend-4 span-3 label">Fecha Desde:</div>
		<div class="span-5">
            <input type="text" name="txtDateFrom" id="txtDateFrom" title='El campo "Fecha Desde" es obligatorio'/>
        </div>
		<div class="span-3 label">Fecha Hasta:</div>
		<div class="span-5 last">
            <input type="text" name="txtDateTo" id="txtDateTo" title='El campo "Fecha Hasta" es obligatorio'/>
        </div>
	</div>	

    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            <input type="button" id="searchPayments" value="Consultar"  onclick="checkPayments();"/>
        </div>
    </div>

	<div class="span-24 last line" id="tableContainer">

	</div>

	<div class="span-24 last center" id="leastOne" style="display: none;">
		Debe seleccionar almenos un pago.
	</div>

    {else}
    <div class="prepend-7 span-10 last">
        No exiten pagos a registrar.
    </div>             
    {/if}
</form>