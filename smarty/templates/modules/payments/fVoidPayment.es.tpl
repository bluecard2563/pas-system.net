<!--/*
File: fNewPayment.es.tpl
Author: Edwin Salvador
Creation Date: 19/03/2010
Last Modified:
Modified by:
*/-->
{$salesCost}

{assign var="title" value="ANULACION DE PAGO"}
<form name="frmVoidPayment" id="frmVoidPayment" method="post" action="{$document_root}modules/payments/pVoidPayment">
    <div class="span-24 last line title">
        Anulaci&oacute;n de Pago
    </div>
    <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
    </div>

    <div class="span-24 last line separator">
        <label>Informaci&oacute;n de la Factura:</label>
    </div>
	{if $countInvoices gt 1}
		<div class="prepend-5 span-19 last line">
			<div class="span-12 last notice" align="center">
				Esta factura ha sido pagada en conjunto.
			</div>
		</div>
	{/if}
	{foreach from=$paymentData.invoices item="i" name="invoices"}
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label"># Factura:</div>
			<div class="span-5">
				{$i.number_inv}
			</div>
			<div class="span-3 label">Fecha:</div>
			<div class="span-5">
				{$i.date_inv}
			</div>
		</div>
		<div class="span-24 last line">
			<div class="prepend-3 span-4 label">Facturado a:</div>
			<div class="span-5">
				{if $i.name_cus}{$i.name_cus}{else}{$i.name_dea}{/if}
			</div>
			<div class="span-3 label">Valor de la Factura:</div>
			<div class="span-5">
				{$i.total_inv}
			</div>
		</div>
		{if !$smarty.foreach.invoices.last}
			<div class="span-24 last">
				<hr>
			</div>
		{/if}
	{/foreach}
	<div class="span-24 last line separator">
		<div class="span-5">
			<label>Detalle de abonos realizados:</label>
		</div>
	</div>
	<div class="span-24 last line">
        <div class="prepend-4 span-4 label">Total pagado:</div>
        <div class="span-5">
            {$paymentData.total_payed_pay}
        </div>
        <div class="span-3 label">Por pagar:</div>
        <div class="span-5">
            {$paymentData.pending_to_pay}
        </div>
    </div>
	
	<div class="prepend-2 span-19 line last">
		<table border="0" id="paymentsTable">
            <THEAD>
                <tr bgcolor="#00b3f0">
                    <td align="center" class="tableTitle">
                        Fecha
                    </td>
                    <td align="center" class="tableTitle">
                        Tipo
                    </td>
                    <td align="center" class="tableTitle">
                        Descripci&oacute;n
                    </td>
                    <td align="center" class="tableTitle">
                        #Documento
                    </td>
                    <td align="center" class="tableTitle">
                        Monto
                    </td>
                    <td align="center" class="tableTitle">
                        Estado
                    </td>
					<td align="center" class="tableTitle">
                        Observaciones
                    </td>
                </tr>
            </THEAD>
            <TBODY>
                {foreach from=$paymentData.details item=d name="details" key="key"}
                    <tr {if $d.status_pyf eq "ACTIVE"}{if $smarty.foreach.details.iteration is even}bgcolor="#c5dee7"{else}bgcolor="#e5e5e5"{/if}{else}bgcolor="#EEBFCA"{/if} >
                        <td class="tableField">
                            {$d.date_pyf}
                        </td>
                        <td class="tableField">
                            {$global_paymentForms[$d.type_pyf]}
                        </td>
                        <td class="tableField">
                            {$d.comments_pyf}
                        </td>
                        <td class="tableField">
                            {$d.document_number_pyf}
                        </td>
                        <td class="tableFieldNumber">
                            {$d.amount_pyf}
                        </td>
                        <td class="tableField">
                            {if $d.status_pyf eq 'ACTIVE'}
							<a class="status" onclick="changeStatus(this,'status_{$d.serial_pyf}','{$d.amount_pyf}','observation_{$d.serial_pyf}');" style="color:{if $d.status_pyf eq 'ACTIVE'}#008000{elseif $d.status_pyf eq 'VOID'}#FF0000{/if}">{$global_paymentStatus[$d.status_pyf]}</a>
							{else}
								{$global_paymentStatus[$d.status_pyf]}
							{/if}
							<input type="hidden" class="status_hidden" id="status_{$d.serial_pyf}" name="payments[{$d.serial_pyf}][status_pyf]" value="{$d.status_pyf}" />
                        </td>
						<td class="tableField">
                            {if $d.status_pyf eq 'ACTIVE'}
							<input type="text" class="observation" name="payments[{$d.serial_pyf}][observation_pyf]" id="observation_{$d.serial_pyf}" disabled="disabled" title='El campo "Observaciones" es obligatorio.' value="" />
							{else}
								{$d.observation_pyf}
								<input type="hidden" name="payments[{$d.serial_pyf}][observation_pyf]" id="observation_{$d.serial_pyf}" value="{$d.observation_pyf}" />
							{/if}
                        </td>
                    </tr>
                {/foreach}
				{if $paymentData.status_pay neq 'VOID'}
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					
					<td colspan="2" class="tableVoidAll">
						<a id="voidAll" class="void" onclick="voidAll();" style="color:#FF0000">Anular todos</a>
					</td>
					<td>
						<input type="text" class="observationAll" name="observation_pay" id="observation_all" disabled="disabled" title='El campo "Observaciones" es obligatorio.' value="" />
					</td>
				</tr>
				{/if}
            </TBODY>
        </table>
    </div>
	
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Total Pagos Activos:</div>
		<div class="span-2" id="sumActive">{$paymentData.total_active}</div>
		<input type="hidden" name="total_payed" id="total_payed" value="{$paymentData.total_active}" />

		<div class="span-4 label">Total Pagos Anulados:</div>
		<div class="span-2" id="sumVoid">{$paymentData.total_void}</div>
		
		<div class="span-2 label">Saldo:</div>
        <div class="span-2 last" id="pending">{$paymentData.pending_to_pay}</div>
		<input type="hidden" name="total_pending" id="total_pending" value="{$paymentData.pending_to_pay}" />
		<input type="hidden" name="total_inv" id="total_inv" value="{$paymentData.total_to_pay_pay}" />
	</div>
	{if $paymentData.observation_pay}
	<div class="span-24 last line">
		<div class="prepend-4 span-4 label">Observaciones del pago:</div>
		<div class="span-15">{$paymentData.observation_pay}</div>
	</div>
	{/if}
	<input type="hidden" name="status_pay" id="status_pay" value="" />
	<input type="hidden" name="serial_pay" id="serial_pay" value="{$paymentData.serial_pay}" />
    <div class="prepend-10 span-5 last line">
        <input type="submit" name="btnSave" id="btnSave" value="Guardar Cambios" class="ui-state-default ui-corner-all" />
    </div>
</form>