<!--/*
File: fPayedAccounts.es.tpl
Author: Mario L�pez
Creation Date:26/03/2010
Modified By:
Last Modified:
*/-->
{assign var="title" value="REPORTE DE CUENTAS"}
<form target="_blank" name="frmPayedAccounts" id="frmPayedAccounts" method="post" action="{$document_root}modules/payments/reports/pReportAccounts">
    <div class="span-24 last title">
    	Reporte de Cuentas
    </div>
    <div class="append-8 span-10 prepend-6 last " id="messageContainer" style="display: none;">
        <div class="span-12 error" align="center">
            Pa&iacute;s es requerido
        </div>
    </div>
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
            <select name="selCountry" id="selCountry">
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}">{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">Ciudad:</div>
        <div class="span-5 append-3 last" id="cityContainer">
            Seleccione un pa&iacute;s
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Responsable:</div>
    	<div class="span-5" id="responsableContainer">
            Seleccione una ciudad
        </div>

        <div class="span-3 label">Categoria:</div>
        <div class="span-5 append-3">
            <select name="selCategory" id="selCategory">
            	<option value="">- Seleccione -</option>
                {if $categoryList}
                    {foreach from=$categoryList item="l"}
                        <option value="{$l.category_id}">{$l.category_name}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last line" id="dealerContainer"></div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Detalle de Pagos:</div>
    	<div class="span-5">
            <input type="checkbox" name="chkDetallado" id="chkDetallado" value="1" />
        </div>

        <div class="span-3 label">Estado:</div>
        <div class="span-5 append-3">
            <select name="selStatus" id="selStatus">
            	<option value="">- Seleccione -</option>
				<option value="CxC">Cuentas por Cobrar</option>
                {if $paymentstates}
                    {foreach from=$paymentstates key="k" item="l"}
                        <option value="{$k}">{$l}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            <input type="button" id="searchBranches" value="Imprimir Reporte" onclick="checkInvoices();"/>
        </div>
    </div>

    <div class="span-24 last line label" id="emptyResult"></div>
</form>