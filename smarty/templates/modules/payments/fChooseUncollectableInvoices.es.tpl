{assign var="title" value="CUENTAS INCOBRABLES"}
<div class="span-24 last title">
	Selecci&oacute;n de Cuentas Incobrables <br/>
	<label>(*) Campos Obligatorios</label>
</div>

{if $error}
<div class="append-7 span-10 prepend-7 last " id="messageContainer">
	<div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
		{if $error eq 1}
			Las cuentas han sido dada de baja. <br />
			{if $cnote_number}
				La numeraci&oacute;n de las notas de cr&eacute;dito son: <br />
				{foreach from=$cnote_number item="cnote"}
					{$cnote}<br />
				{/foreach}
			{/if}
		{elseif $error eq 2}
			Hubo errores en actualizar una de las facturas. Por favor comun&iacute;quese con el administrador.
		{/if}
	</div>
</div>
{/if}



<div class="span-7 span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

{if $managerList}
<form name="frmUncollectableInvoices" id="frmUncollectableInvoices" action="{$document_root}modules/payments/pChooseUncollectableInvoices" method="post">
	<div class="span-24 last line">
		<div class="prepend-8 span-4 label">* Representante:</div>
		<div class="span-5 appen7 last">
			<select name="selManager" id="selManager" title="El campo 'Representante' es obligatorio">
				<option value="">- Seleccione -</option>
				{foreach from=$managerList item="l"}
					<option value="{$l.serial_mbc}" {if $managerList|@COUNT eq 1} selected {/if}>{$l.name_man}</option>
				{/foreach}
			</select>
		</div>
	</div>
	<input type="hidden" name="hdnParameter" id="hdnParameter" value="{$param}" />

	<div class="span-24 last line" id="invoiceContainer"></div>
</form>
{else}
<div class="span-10 append-7 prepend-7">
	<div class="span-10 error">
		No existen representantes con facturas incobrables.
	</div>
</div>
{/if}