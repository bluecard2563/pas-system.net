<!--/*
File: fChoosePaymentToVoid.es.tpl
Author: Edwin Salvador
Creation Date: 19/03/2010
Last Modified: 
Modified by: 
*/-->
{assign var="title" value="BUSCAR PAGOS POR FACTURA"}
<form name="frmChoosePaymentToVoid" id="frmChoosePaymentToVoid" method="post" action="{$document_root}modules/payments/fVoidPayment">
    <div class="span-24 last title">
    	BUSCAR PAGOS POR FACTURA
    </div>
{if $error}
    <div class="append-8 span-10 prepend-6 last " id="messageContainer">
    	{if $error eq 1}
           <div class="span-12 success" align="center">
            	Los pagos han sido anulados.
            </div>
        {else}
            {if $error eq 2}
                <div class="span-12 error" align="center">
                    El n&uacute;mero de factura no existe.
                </div>
			{else}
				{if $error eq 3}
					<div class="span-12 error" align="center">
						Existi&oacute; un error al anular los pagos. Por favor int&eacute;ntelo nuevamente.
					</div>
				{/if}
			{/if}
		{/if}
    </div>
 {/if}
    <div class="prepend-5 span-15 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    <div class="span-24 last line">
        <div class="prepend-7 span-5 label">N&uacute;mero de Factura: </div>
        <div class="span-5">
            <input type="text" name="txtInvoice" id="txtInvoice" title='Ingrese un n&uacute;mero de factura.' />
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            <input type="submit" id="searchPayment" name="searchPayment" value="Buscar"/>
        </div>
    </div>

    <div class="span-24 last line" id="paymentsContainer"></div>
</form>