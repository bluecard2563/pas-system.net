{assign var="title" value="ALMACENAR COBROS DESDE ERP"}
<form name="frmChooseInvoice" id="frmChooseInvoice" method="post" action="{$document_root}modules/payments/pRetrievePaymentsFromERP">
    <div class="span-24 last title">
    	Almacenar Cobros Desde ERP <br/>
		<label>(*) Recuerde que solo se registrar&aacute;n cobros aprobados a la fecha</label>
    </div>
	
	{if $error eq 1}
		<div class="prepend-7 span-10 append-7 last line">
			<div class="span-10 success">
				La carga se ha realizado con &eacute;xito!
			</div>
		</div>
	{/if}
	
	<input type="submit" name="btnSubmit" id="btnSubmit" value="Cargar Cobros" />
</form>