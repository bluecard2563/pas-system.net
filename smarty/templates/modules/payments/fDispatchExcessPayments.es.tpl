{*
Created By: Patricio Astudillo
Creation Date: 17/05/2010
*}

{assign var=title value="LIQUIDAR PAGOS EN EXCESO"}
<form name="frmExcessPayments" id="frmExcessPayments" method="post" action="{$document_root}modules/payments/pDispatchExcessPayments" target="_blank">
    <div class="span-24 last title">
    	Liquidaci&oacute;n de Pagos en Exceso<br />
        <label>(*) Campos Obligatorios</label>
    </div>

	{if $error}
		<div class="span-7 span-10 prepend-7 last">
			<div class="span-10 {if $error eq 1}success{else}error{/if}">
				{if $error eq 1}
					Los pagos se han liquidado exitosamente.
				{elseif $error eq 2}
					No se pudo registrar una de ls liquidaciones. Por Favor vuelva a intentarlo.
				{elseif $error eq 3}
					Hubo errores al cargar la informaci&oacute;n de los pagos a liquidar.
				{elseif $error eq 4}
					No se han seleccionado pagos a liquidar.
				{elseif $error eq 5}
					La información del cobro no a podido ser actualizada. Se ha interrumpido el proceso.
				{/if}
			</div>
		</div>
	{/if}

    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
	
    {if $excessCountries}
    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
            <select name="selCountry" id="selCountry">
                <option value="">- Seleccione -</option>
                {foreach from=$excessCountries item="l" name="country"}
                    <option value="{$l.serial_cou}" {if $smarty.foreach.country.total eq 1} selected {/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Ciudad:</div>
        <div class="span-5 last" id="cityContainer">
            Seleccione un pa&iacute;s
         </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-7 span-4 label">* Responsable:</div>
    	<div class="span-5" id="responsableContainer">
            Seleccione una ciudad
        </div>
	</div>

	<div class="span-24 last line">
        <div class="prepend-7 span-4 label">Categoria:</div>
        <div class="span-5">
            <select name="selCategory" id="selCategory">
            	<option value="">- Seleccione -</option>
                {if $categoryList}
                    {foreach from=$categoryList item="l"}
                        <option value="{$l}">{$l}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            <input type="button" id="searchBranches" value="Buscar"/>
        </div>
    </div>
    <div class="span-24 last line" id="dealerContainer"></div>

    <div class="span-24 last line" id="paymentsContainer"></div>
    {else}
    <div class="span-7 span-10 prepend-7 last">
		<div class="error span-10">
			No existen pagos en exceso por liquidar
		</div>
    </div>
    {/if}
</form>