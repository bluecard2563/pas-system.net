<!--/*
File: fChooseInvoice.es.tpl
Author: Edwin Salvador
Creation Date: 10/03/2010
Last Modified: 
Modified by: 
*/-->
{assign var="title" value="SELECCION DE CUENTA POR COBRAR"}
<form name="frmChooseInvoice" id="frmChooseInvoice" method="post" action="{$document_root}modules/payments/fNewPayment">
    <div class="span-24 last title">
    	Selecci&oacute;n de Cuenta por cobrar <br/>
		<label>(*) Campos Obligatorios</label>
    </div>
{if $error}
    <div class="append-8 span-10 prepend-6 last " id="messageContainer">
    	{if $error eq 1}
           <div class="span-12 success" align="center">
            	Las cuentas han sido cobradas.
                {if $fileNumber}<br /><b></b><a id="printCreditNote" href="{$document_root}modules/payments/pPrintPaymentNote/{$fileNumber}" target="_blank">Imprimir Nota de Pago</a>{/if}
            </div>
        {elseif $error eq 2}
			<div class="span-12 error" align="center">
				Hubo errores en el cobro. Por favor vuelva a intentarlo.
			</div>
		{elseif $error eq 3}
			<div class="span-12 error" align="center">
				Hubo errores registrar los detalles del pago. Por favor vuelva a intentarlo.
			</div>
		{elseif $error eq 4}
			<div class="span-12 error" align="center">
				Existi&oacute; un error al actualizar la factura. Por favor p&oacute;ngase en contacto con el administrador.
			</div>
		{elseif $error eq 5}
			<div class="span-12 error" align="center">
				Existi&oacute; un error al actualizar una de las ventas. Por favor contacte al administrador inmediatamente.
			</div>
		{elseif $error eq "no-document"}
			<div class="span-12 error" align="center">
				No se puede imprimir la Nota de Pago debido a que no hay un documento creado para el representante.
			</div>
        {/if}
    </div>
 {/if}
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts"></ul>
    </div>
    {if $countryList}
    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">* Pa&iacute;s:</div>
        <div class="span-5">
            <select name="selCountry" id="selCountry">
                <option value="">- Seleccione -</option>
                {foreach from=$countryList item="l"}
                    <option value="{$l.serial_cou}" {if $countryList|@COUNT eq 1} selected {/if}>{$l.name_cou}</option>
                {/foreach}
            </select>
        </div>
        <div class="span-3 label">* Ciudad:</div>
        <div class="span-5 append-3 last" id="cityContainer">
            Seleccione un pa&iacute;s
         </div>
    </div>

    <div class="span-24 last line">
        <div class="prepend-3 span-4 label">Responsable:</div>
    	<div class="span-5" id="responsableContainer">
            Seleccione una ciudad
        </div>

        <div class="span-3 label">Categoria:</div>
        <div class="span-5 append-3">
            <select name="selCategory" id="selCategory">
            	<option value="">- Seleccione -</option>
                {if $categoryList}
                    {foreach from=$categoryList item="l"}
                        <option value="{$l}">{$l}</option>
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>
    <div class="span-24 last line">
        <div class="prepend-11 span-5">
            <input type="button" id="searchBranches" value="Buscar"/>
        </div>
    </div>
    <div class="span-24 last line" id="dealerContainer"></div>

    <div class="span-24 last line" id="invoiceContainer"></div>
    {else}
    <div class="span-7 span-10 prepend-7 last">
        <ul id="alerts" class="alerts" style="display: block;">
            <li>
                <label class="error_validation" for="selType" generated="true" style="display: block;">No existen cuentas por cobrar por el momento.</label>
            </li>
        </ul>   
    </div>             
    {/if}
</form>
