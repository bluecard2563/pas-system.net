{assign var=title value="ADMINISTRAR VENTAS WEB"}
<div class="span-24 last title line">
	Administrar Ventas Web
</div>

{if $error}
<div class="prepend-7 span-10 append-10 last line">
	<div class="span-10 center {if $error eq 1}success{else}error{/if}">
		  {if $error == 'payPalCancel'}
				Ha cancelado la compra en PayPal.
		  {elseif $error eq 1}
				Las ventas web fueron procesadas correctamente!
		  {elseif $error eq 2}
			La clave es incorrecta, por favor ingrese nuevamente!
		  {elseif $error eq 3}
			Su cuenta fue creada exitosamente, ahora puede ingresar con sus datos de usuario.
		  {elseif $error eq 11}
				Error al Guardar la informaci&oacute;n del Titular.
		  {elseif $error eq 12}
				Error al Guardar la informaci&oacute;n del pago.
		  {elseif $error eq 13}
				Error al Guardar la informaci&oacute;n del detalle del pago.
		  {elseif $error eq 14}
				Error al Guardar la informaci&oacute;n del Documento.
		  {elseif $error eq 15}
				Error al Actualizar la informaci&oacute;n del Documento.
		  {elseif $error eq 16}
				Error al Guardar la informaci&oacute;n de la Factura.
		  {elseif $error eq 17}
				Error al Guardar la informaci&oacute;n de la venta.
		  {elseif $error eq 18}
				Error al Guardar la informaci&oacute;n de los Acompa&ntilde;antes.
		  {elseif $error eq 19}
				Error al Guardar el registro del viaje.
		  {elseif $error eq 20}
				Error al Guardar el registro de los Acompa&ntilde;antes.
		  {elseif $error eq 21}
				Los datos ingresados para la tarjeta de Credito son incorrectos
		  {elseif $error eq 22}
				Hubo un error al procesar su compra, por favor pongase en contacto con el Administrador.
		  {elseif $error eq 23}
				Hubo problemas con el env&iacute;o del correo.
		  {elseif $error eq 24}
				Hubo errores al obtener el n&uacute;mero de tarjeta. Comun&iacute;quese con el administrador antes de continuar.

		  {elseif $error eq 30}
			   La venta se ha realizado exitosamente!
		  {elseif $error eq 31}
				Lo sentimos, la transacci&oacute;n tuvo errores. Por favor comun&iacute;quese con las oficinas de
				Planet Assist en su pa&iacute;s para mayor informaci&oacute;n.
		  {elseif $error eq 32}
				La compra por PayPal fue Cancelada.


		  {elseif $error eq 40}
				La venta se ha realizado exitosamente!
		  {elseif $error eq 41}
				Ha ocurrido un error en la validaci&oacute;n de su tarjeta de cr&eacute;dito. Error: 
				{handle_merchant_account_errors error=$extra_error_code}
		  {elseif $error eq 42}
				No se tiene la informaci&oacute;n necesaria para continuar con el pago con tarjeta de cr&eacute;dito.
				Por favor vuelva a cotizar el producto.

		{/if}
	</div>
</div>
{/if}

{if $general_data}
<div class="span-10 prepend-7 last">
	<ul id="alerts" class="alerts"></ul>
</div>

<form name="frmWebSales" id="frmWebSales" action="{$document_root}modules/websales/pConfirmWebSales" method="post" >
	<div class="prepend-1 span-22 append-1 last line">
		<table border="0" id="salesTable">
			<THEAD>
				<tr>
					<th align="center" style="text-align: center;">
						Seleccione
					</th>
					<th align="center" style="text-align: center;">
						Producto
					</th>
					<th align="center" style="text-align: center;">
						No. Tarjetas
					</th>
					<th align="center" style="text-align: center;">
						Total
					</th>
					<th align="center" style="text-align: center;">
						Titulares
					</th>
					<th align="center" style="text-align: center;">
						Fecha
					</th>
					<th align="center" style="text-align: center;">
						Venta
					</th>
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="sales" from=$general_data item="s"}
				<tr {if $smarty.foreach.sales.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						<input type="checkbox" name="{$s.serial}" id="{$s.serial}"/>
					</td>
					<td align="center" class="tableField">
						{$s.product}
					</td>
					<td align="center" class="tableField">
						{$s.amount_of_cards}
					</td>
					<td align="center" class="tableField">
						${$s.total_sold}
					</td>
					<td align="center" class="tableField">
						{$s.holder}
					</td>
					<td align="center" class="tableField">
						{$s.date}
					</td>
					<td align="center" class="tableField">
						{$s.origin}
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
	<div class="span-24 last pageNavPosition line" id="alertPageNavPosition"></div>

	<div class="span-24 last line center">
		<input type="button" name="btnResale" id="btnResale" value="Volver a Procesar" /> &nbsp;
		<input type="button" name="btnRemove" id="btnRemove" value="Eliminar" />
		<input type="hidden" name="hdnTotalPages" id="hdnTotalPages" value="{$total_pages}" />
		<input type="hidden" name="selectedRows" id="selectedRows"/>
		<input type="hidden" name="action" id="action" value="" title="Seleccione al menos una venta." />
	</div>
</form>
{else}
	<div class="span-8 append-8 prepend-8" align="center">
		<div class="span-8 error">
			No existen ventas por confirmar pendientes.
		</div>
	</div>
{/if}