{assign var="title" value="NUEVO PARAMETRO"}
<form name="frmNewParameter" id="frmNewParameter" method="post" action="{$document_root}modules/parameter/pNewParameter" class="">

    <div class="span-24 last title">
    	Nuevo Par&aacute;metro<br />
        <label>(*) Campos Obligatorios</label>
    </div>
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
    	{if $error eq 1}
        	<div class="span-10 success" align="center">
            	El par&aacute;metro ha registrado exitosamente!
            </div>
        {else}
        	<div class="span-10 error" align="center">
            	No se pudo registrar el par&aacute;metro.
            </div>
        {/if}
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
    
	    <div class="span-4 label">* Nombre:</div>
    	<div class="span-4 last"> 
        	<input type="text" name="txtName" id="txtName" title="El campo 'Nombre' es obligatorio."></input>
        </div>
        <div class="span-4 label">* Tipo:</div>
    	<div class="span-4"> 
        	<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=tl}
                	<option value="{$tl}">{if $tl eq 'NUMBER'}Num&eacute;rico{elseif $tl eq 'TEXT'}Texto{/if}</option>
                {/foreach}
            </select>
        </div>

	</div>
    
	<div class="prepend-3 span-19 append-2 last line">        
        <div class="span-4 label">* Condici&oacute;n:</div>
    	<div class="span-4 append-2 last">
        	<select name="selCondition" id="selCondition">
            	<option value="">- Seleccione -</option>
                {foreach from=$conditionList item=cl}
                	<option value="{$cl}">{if $cl eq '<'}Menor{elseif $cl eq '>'}Mayor{elseif $cl eq '<='}Menor o Igual{elseif $cl eq '>='}Mayor o Igual{elseif $cl eq '='}Igual{/if}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line">        
        <div class="span-4 label">* Valor:</div>
    	<div class="span-4 append-2 last"> 
                <textarea name="txtValue" id="txtValue" class="textArea" title="El campo 'Valor' es obligatorio."></textarea>
        </div>
    </div>

    <div class="prepend-3 span-19 append-2 last line">
        <div class="span-4 label">* Descripci&oacute;n:</div>
    	<div class="span-4 append-2 last"> 
        	<textarea name="txaDescription" id="txaDescription" class="textArea" title="El campo 'Descripci&oacute;n' es obligatorio."></textarea>
        </div>
    </div>

       
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
    </div>
</form>