{assign var=title value="EDITAR PAR&Aacute;METROS DE ASISTENCIAS"}
<form name="frmEditParameter" id="frmEditParameter" method="post" action="{$document_root}modules/parameter/assistance/pEditParameter" class="">
	<div class="span-24 last title ">
		Receptores de Alertas de Asistencias
	</div>

	{if $error}
    <div class="prepend-6 span-12 last">
		<div class="{if $error eq 1}success{else}error{/if}" align="center">
			{if $error eq 1}
				Se ha modificado el par&aacute;metro exitosamente!
			{else}
				Existi&oacute; un error al modificar el par&aacute;metro, por favor int&eacute;ntelo nuevamente.
			{/if}
		</div>
    </div>
    {/if}

	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">Nombre:</div>
		<div class="span-8 append-4 last">
			<input type="text" name="txtName" size="27" value="{$data.name_par}" readonly />
			<input type="hidden" name="hdnSerial" id="hdnSerial" value="{$data.serial_par}" />
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-6 span-4 label">Tipo:</div>
		<div class="span-8 append-4 last">
			<select name="selType" id="selType">
				<option value="">- Seleccione -</option>
				{foreach from=$data.value_par key=k item=v}
					<option value="{$k}">{$global_assistanceEmailType[$k]}</option>
				{/foreach}
			</select>
		</div>
	</div>


	<div class="span-24 last line">&nbsp;</div>

	<div class="span-24 last line" id="selectionContainer" style="display: none">
		<div class="span-24 last line">
			<div class="prepend-4 span-3 label">Zona:</div>
			<div class="span-6 last">
				<select name="selZone" id="selZone">
				  <option value="">- Seleccione -</option>
				  {foreach from=$nameZoneList item="l"}
				  <option value="{$l.serial_zon}">{$l.name_zon}</option>
				  {/foreach}
				</select>
			</div>

			<div class="span-3 label">Pa&iacute;s</div>
			<div class="span-4 append-4 last" id="countryContainer">
				<select name="selCountry" id="selCountry">
				  <option value="">- Seleccione -</option>
				</select>
			</div>
		</div>

		<div class="span-24 last line">
			<div class="prepend-4 span-3 label">Representante:</div>
			<div class="span-6 last" id="managerContainer">
				<select name="selManager" id="selManager">
				  <option value="">- Todos -</option>
				</select>
			</div>
		</div>


		<div class="prepend-7 span-10 last line" id="usersContainer">
			<div class="span-4 line" id="usersToContainer">
				<div align="center">Seleccionados</div>
				<select multiple id="usersTo" name="usersTo[]" class="selectMultiple span-4 last"></select>
			</div>

			<div class="span-2 buttonPane">
					<center>
					<br />
					<br />
					<br />
					<input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
					<input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
					<input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
					<input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
				</center>
			</div>

			<div class="span-4 last"  id="usersFromContainer">
				<div align="center">Existentes</div>
				<select multiple id="usersFrom" name="usersFrom[]" class="selectMultiple span-4 last"></select>
			</div>
		</div>

		<div class="span-24 last line">&nbsp;</div>

		<div class="span-24 last buttons line">
			<input type="button" name="btnSave" id="btnSave" value="Guardar Cambios" />
		</div>
	</div>
</form>