{assign var="title" value="BUSCAR PARAMETRO"}
<form name="frmSearchParameter" id="frmSearchParameter" method="post" action="{$document_root}modules/parameter/fUpdateParameter" class="form_smarty">
	<div class="span-24 last title">Buscar Par&aacute;metro<br />
        <label>(*) Campos Obligatorios</label></div>
    {if $error}
         <div class="append-7 span-10 prepend-7 last">
            <div class="span-10 {if $error eq 2}success{else}error{/if}" align="center">
                {if $error eq 2}
                    Actualizaci&oacute;n exitosa!
                {elseif $error eq 3}
                    El par&aacute;metro ingresado no existe.
                {/if}
            </div>
        </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line" >
        <div class="prepend-5 span-3 label">* Nombre:</div>
        <div class="span-8 append-3 last">
            <input type="text" name="txtText" id="txtText" title="El campo 'Nombre' es obligatorio." class="" />
        </div>
    </div>
    
    <div class="span-24 last buttons line">
        <input type="submit" name="btnSearch" id="btnSearch" value="Buscar" >
    </div>
    <input type="hidden" name="hdnParameterID" id="hdnParameterID" value="" />

</form>
