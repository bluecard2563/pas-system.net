{assign var=title value="EDITAR PAR&Aacute;METROS DE ALERTAS"}
<form name="frmEditAlertsParameter" id="frmEditAlertsParameter" method="post" action="{$document_root}modules/parameter/pEditAlertsParameter" class="">

    <div class="span-24 last title ">
        Receptores de Alertas
    </div>
	{if $error neq 0}
    <div class="prepend-6 span-12 last">
        {if $error eq 1}
            <div class="success" align="center">
                Se ha modificado el par&aacute;metro exitosamente!
            </div>
        {else}
            <div class="error">
                Existi&oacute; un error al modificar el par&aacute;metro, por favor int&eacute;ntelo nuevamente.
            </div>
        {/if}
    </div>
    {/if}
   <div class="prepend-7 span-10 last">
        <ul id="alerts" class="alerts"></ul>
   </div>

   <div class="span-24 last line">
        <div class="prepend-6 span-4 label">Alerta:</div>
        <div class="span-8 append-4 last">
            <select name="selAlerts" id="selAlerts" title='El campo "Alerta" es obligatorio.'>
                    <option value="">- Seleccione -</option>
                    {foreach from=$alertsList item="al"}
                            <option value="{$al.serial_par}">{$al.name_par}</option>
                    {/foreach}
            </select>
        </div>
   </div>

   <div class="span-24 last line" id="selectsContainer">

	</div>

	<div class="span-7" id="countryWrapper" style="display: none">
		<div class="span-2 label">* Pais:</div>
		<div class="span-5 last" id="countryContainer">
			<select name="countriesList" id="countriesList" title='El campo "Pa&iacute;s" es obligatorio'>
					<option value="">- Seleccione -</option>
					{foreach from=$countriesList item="cl"}
							<option value="{$cl.serial_cou}">{$cl.name_cou}</option>
					{/foreach}
			</select>
		</div>
	</div>

	<div class="span-24 last line">
		<div class="prepend-8 span-10 last" id="usersContainer" style="display: none">
					<div class="span-4 line" id="usersToContainer">
						<div align="center">Seleccionados</div>
						<select multiple id="0usersTo" name="usersTo" class="selectMultiple span-4 last"  count="0" title='El campo "Seleccionados" es obligatorio.'></select>
					</div>

					<div class="span-2 buttonPane">
						<br />
						<input type="button" id="moveSelected" name="moveSelected" value="|<" class="span-2 last"/>
						<input type="button" id="moveAll" name="moveAll" value="<<" class="span-2 last"/>
						<input type="button" id="removeAll" name="removeAll" value=">>" class="span-2 last"/>
						<input type="button" id="removeSelected" name="removeSelected" value=">|" class="span-2 last "/>
					</div>

					<div class="span-4 last"  id="usersFromContainer">
						<div align="center">Existentes</div>
						<select multiple id="usersFrom" name="usersFrom" class="selectMultiple span-4 last"></select>
					</div>
			</div>
	</div>

	<input type="hidden" name="countriesCount" id="countriesCount" value="0"/>
	<input type="hidden" name="selectedManagers" id="selectedManagers"/>    

</form>
    