{assign var="title" value="ACTUALIZAR PARAMETRO"}
<div class="span-24 last title">
    	Actualizar Par&aacute;metro<br />
        <label>(*) Campos Obligatorios</label>
</div>
{if $data}
<form name="frmUpdateParameter" id="frmUpdateParameter" method="post" action="{$document_root}modules/parameter/pUpdateParameter" class="">
    
	{if $error}
    <div class="append-7 span-10 prepend-7 last">
        	<div class="span-10 error" align="center">
            	No se pudo actualizar el par&aacute;metro.
            </div>
    </div>
    {/if}
    
    <div class="span-7 span-10 prepend-7 last">
	    <ul id="alerts" class="alerts"></ul>
    </div>
    
    <div class="prepend-3 span-19 append-2 last line">
    
	    <div class="span-4 label">* Nombre:</div>
    	<div class="span-4 last"> 
        	<input type="text" name="txtName" id="txtName" title="El campo 'Nombre' es obligatorio." value="{$data.name_par}"></input>
        </div>
        <div class="span-4 label">* Tipo:</div>
    	<div class="span-4"> 
        	<select name="selType" id="selType" title="El campo 'Tipo' es obligatorio.">
            	<option value="">- Seleccione -</option>
                {foreach from=$typeList item=tl}
                	<option value="{$tl}" {if $data.type_par eq $tl} selected="selected" {/if}>{if $tl eq 'NUMBER'}Num&eacute;rico{elseif $tl eq 'TEXT'}Texto{/if}</option>
                {/foreach}
            </select>
        </div>

	</div>
    
	<div class="prepend-3 span-19 append-2 last line">
        <div class="prepend-1 span-4 label">* Condici&oacute;n:</div>
    	<div class="span-4"> 
        	<select name="selCondition" id="selCondition">
            	<option value="">- Seleccione -</option>
                {foreach from=$conditionList item=cl}
                	<option value="{$cl}" {if $data.condition_par eq $cl} selected="selected" {/if}>{if $cl eq '<'}Menor{elseif $cl eq '>'}Mayor{elseif $cl eq '<='}Menor o Igual{elseif $cl eq '>='}Mayor o Igual{elseif $cl eq '='}Igual{/if}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line">
        <div class="span-4 label">Valor:</div>
    	<div class="span-4 append-2 last">
                <textarea name="txtValue" id="txtValue" class="textArea" title="El campo 'Valor' es obligatorio.">{$data.value_par}</textarea>
        </div>
    </div>
    <div class="prepend-3 span-19 append-2 last line">
        <div class="span-4 label">* Descripci&oacute;n:</div>
    	<div class="span-4 append-2 last"> 
        	<textarea name="txaDescription" id="txaDescription" class="textArea">{$data.description_par}</textarea>
        </div>
    </div>

       
    <div class="span-24 last buttons line">
        <input type="submit" name="btnInsert" id="btnInsert" value="Registrar" class="" >
    </div>
	<input type="hidden" name="hdnParameterID" id="hdnParameterID" value="{$data.serial_par}"/>
</form>
{else}
	<div class="append-5 span-10 prepend-4 last">
        <div class="span-10 error" align="center">
            No se pudo cargar el par&aacute;metro.
        </div>
    </div>
{/if}