{assign var="title" value="P&Aacute;GINA PRINCIPAL"}
<div class="span-24 last mainContent">
{if $page neq ''}
<div class="append-7 span-10 prepend-7 last mainAlerts">
    {if $page eq 'user'}
    	{if $error eq 1}
       		 <div class="span-10 success" align="center">
            	El usuario se ha registrado exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        {elseif $error eq 3}
        	<div class="span-10 success" align="center">
            	El usuario se ha actualizado exitosamente!
            </div>
        {elseif $error eq 4}
        	<div class="span-10 error" align="center">
            	Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
		{elseif $error eq 5}
        	<div class="span-10 success" align="center">
            	La contrase&ntilde;a se ha reestablecido correctamente
            </div>
		{elseif $error eq 6}
        	<div class="span-10 success" align="center">
            	La contrase&ntilde;a se ha reestablecido correctamente<br>
				No se pudo enviar el e-mail.
            </div>
        {/if}
    {elseif $page eq 'freelance'}
        {if $error eq 1}
                <div class="span-10 success" align="center">
                    El Agente Libre se ha registrado exitosamente!
                </div>
        {elseif $error eq 2}
                <div class="span-10 error" align="center">
                    Hubo errores en el ingreso. Por favor vuelva a intentarlo.
                </div>
        {elseif $error eq 3}
            <div class="span-10 success" align="center">
            	La actualizaci&oacute;n se ha realizado exitosamente!
            </div>
        {/if}
    {elseif $page eq 'myAccount'}
    	{if $error eq 1}
       		 <div class="span-10 success" align="center">
            	Datos actualizados exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	Hubo errores en el cambio. Por favor vuelva a intentarlo.
            </div>
        {/if}
    {elseif $page eq 'sales'}
    	{if $error eq 1}
            <div class="span-10 success" align="center">
            	Contrase&ntilde;a cambiada exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	Solamente un usuario Counter puede realizar ventas
            </div>
        {elseif $error eq 3}
            <div class="span-10 error" align="center">
            	Su comercializador actualmente no tiene asignado una cartera de productos. <br>
                Por favor comun&iacute;quese con el administrador.
            </div>
        {elseif $error eq 4}
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente. 
                {if $sale.card_number_sal}<br> Su n&uacute;mero de tarjeta es: {$sale.card_number_sal}<br>{/if}
                {if $printingId}
                    <a  id="printContract"  serial_sal="{$printingId}" href="{$document_root}modules/sales/pPrintSalePDF/{$printingId}/1" target="_blank">Imprimir Ad&eacute;ndum</a> <br>
                    {if $contract_logic}
                        <a  id="printNewContract"  serial_sal="{$printingId}" href="{$document_root}modules/sales/pPrintContractPDF/{$printingId}/1" target="_blank">Imprimir Contrato</a> <br>
                    {/if}
                {/if}
                {if $printInvoiceData.total_number_inv neq 0}
                    {if $printInvoiceData.total_number_inv neq 99}
    {*                    <A HREF="https://bluecard.saas.la/intranet/{$printInvoiceData.total_number_inv}/{$printInvoiceData.document}/PublicPDF/" target="_blank">Imprimir PDF</A>*}
                            Su numero de factura es: {$printInvoiceData.total_number_inv}
                    {else}
                        El contrato no pudo ser facturado de manera autom&aacute;tica, el mismo ser&aacute; facturado manualmente. 
                     {/if}   
                {/if}    

                {if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}				
            </div>
        {elseif $error eq 5}
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente. <br>
                No se pudo enviar el mail con la informaci&oacute;n de ingreso al cliente.
                {if $sale.card_number_sal}<br> Su n&uacute;mero de tarjeta es: {$sale.card_number_sal}<br>{/if}
                {if $printingId}
                    <a  id="printContract"  serial_sal="{$printingId}" href="{$document_root}modules/sales/pPrintSalePDF/{$printingId}/1" target="_blank">Imprimir Ad&eacute;ndum</a> <br>
                    {if $contract_logic}
                        <a  id="printNewContract"  serial_sal="{$printingId}" href="{$document_root}modules/sales/pPrintContractPDF/{$printingId}/1" target="_blank">Imprimir Contrato</a> <br>
                    {/if}
                {/if}
                {if $printInvoiceData.total_number_inv neq 0}
                    {if $printInvoiceData.total_number_inv neq 99}
    {*                    <A HREF="https://bluecard.saas.la/intranet/{$printInvoiceData.total_number_inv}/{$printInvoiceData.document}/PublicPDF/" target="_blank">Imprimir PDF</A>*}
                            Su numero de factura es: {$printInvoiceData.total_number_inv}
                    {else}
                        El contrato no pudo ser facturado de manera autom&aacute;tica, el mismo ser&aacute; facturado manualmente. 
                     {/if}   
                {/if} 
				{if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}
            </div>
        {elseif $error eq 6}
            <div class="span-10 error" align="center">
            	Errores al ingresar un nuevo cliente <br>
                No se ha realizado la venta.
            </div>
        {elseif $error eq 7}
            <div class="span-10 error" align="center">
            	No se ha realizado la venta.
            </div>
        {elseif $error eq 8}
            <div class="span-10 error" align="center">
            	Error al agregar Servicios.
            </div>
        {elseif $error eq 9}
            <div class="span-10 error" align="center">
            	Error al agregar Extras.
            </div>
        {elseif $error eq 10}
            <div class="span-10 error" align="center">
            	Error al ingresar Nuevo Cliente.
            </div>
        {elseif $error eq 11}
            <div class="span-10 success" align="center">
            	La autorizaci&oacute;n de ventas se realiz&oacute; exitosamente.
				{if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}
			</div>
        {elseif $error eq 12}
            <div class="span-10 error" align="center">
            	Hubieron errores al autorizar las ventas.
            </div>
		{elseif $error eq 13}
            <div class="span-10 error" align="center">
            	No se pudo cargar correctamente la informaci&oacute;n del comercializador.<br>
				Por favor comun&iacute;quese con el administrador.
            </div>
		{elseif $error eq 14}
            <div class="span-10 error" align="center">
            	Los cambios se han registrado exitosamente! Sin embargo, <br>algunos de los e-mails de notificaci&oacute;n no se han podido enviar.
            </div>
		{elseif $error eq 15}
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente.<br>
				{if $sale.card_number_sal}Su n&uacute;mero de tarjeta es: {$sale.card_number_sal}<br>{/if}
				{if $printingId}
					<a href="{$document_root}modules/sales/travelRegister/fRegisterTravel/{$printingId}">Registro de Viajes</a> <br>
					<!--<a  id="printContract" serial_sal="{$printingId}" href="{$document_root}modules/sales/pPrintSalePDF/{$printingId}/1" target="_blank">Imprimir Contrato</a>-->
				{/if}
                                {if $printInvoiceData.total_number_inv neq 99}
{*                    <A HREF="https://bluecard.saas.la/intranet/{$printInvoiceData.total_number_inv}/{$printInvoiceData.document}/PublicPDF/" target="_blank">Imprimir PDF</A>*}
                        Su numero de factura es: {$printInvoiceData.total_number_inv}
                {else}
                    El contrato no pudo ser facturado de manera autom&aacute;tica, el mismo ser&aacute; facturado manualmente. 
                 {/if} 
				{if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}
			</div>
		{elseif $error eq 16}
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente.<br> Sin embargo, no se pudieron crear todas las alertas.
				{if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}
            </div>
        {elseif $error eq 17}
            <div class="span-10 success" align="center">
            	La Venta Telefonica ha sido realizada exitosamente.
				{if $cupon_code}<br> Su n&uacute;mero de cup&oacute;n es: {$cupon_code}{/if}
            </div>
        {elseif $error eq 18}
            <div class="span-10 success" align="center">
            	El n&uacute;mero de tarjeta seleccionado ya ha sido asignado a otra venta.
            	Por favor vuelva a generar la venta.
            </div>
        {elseif $error eq 19}
            <div class="span-10 success" align="center">
            	Ha sido imposible obtener El n&uacute;mero de tarjeta.
            	Por favor contacte al administrador.
            </div>
		{elseif $error eq 20}
            <div class="span-10 error" align="center">
            	La sucursal a la que usted pertenece no tiene una cartera de productos asignada.
				Lo sentimos.
            </div>
        {/if}
	{elseif $page eq 'masiveSales'}
		{if $error eq 'success'}
			<div class="span-10 success" align="center">
            	La venta se ha registrado exitosamente!
				{if $success eq 'YES'}
					<br /> Tenga presente que algunos viajeron no fueron inclu&iacute;dos por restricciones de destino en el producto.
				{/if}
            </div>
		{elseif $error eq 'wrong_extension'}
			<div class="span-10 error" align="center">
            	La extensi&oacute;n del archivo es err&oacute;nea. <br />
            	S&oacute;lo se admiten .xls y .xlsx
            </div>
		{elseif $error eq 'upload_error'}
			<div class="span-10 error" align="center">
            	Hubo un error en la carga del archivo.
            </div>
		{elseif $error eq 'customer_secuence_error'}
			<div class="span-10 error" align="center">
            	La secuencia de pasajeros en el archivo es incorrecta.
            </div>
		{elseif $error eq 'extras_not_allowed'}
			<div class="span-10 error" align="center">
            	El producto seleccionado no admite adicionales.
            </div>
		{elseif $error eq 'customer_extras_secuence_error'}
			<div class="span-10 error" align="center">
            	Existe un error en las relaciones entre clientes. Revise el archivo.
            </div>
		{elseif $error eq 'wrong_numbers_format'}
			<div class="span-10 error" align="center">
            	El formato para los numeros de tel&eacute;fono de los clientes es incorrecto.<br /> 
            	Por favor rev&iacute;selo.
            </div>
		{elseif $error eq 'seniors_not_allowed'}
			<div class="span-10 error" align="center">
            	No se admiten adultos mayores como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>			
		{elseif $error eq 'only_children_allowed_for_extras'}
			<div class="span-10 error" align="center">
            	Solamente se admiten menores como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'only_adults_allowed_for_extras'}
			<div class="span-10 error" align="center">
            	Solamente se admiten adultos como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'no_file_found'}
			<div class="span-10 error" align="center">
            	No se pudo encontrar el archivo a cargar. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'no_price_found'}
			<div class="span-10 error" align="center">
            	No se pudo encontrar un precio para uno de los pasajeros. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'duplicated_spouse'}
			<div class="span-10 error" align="center">
            	Existe un registor duplicado de "Conyuge" para un cliente. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'extras_not_allowed'}
			<div class="span-10 error" align="center">
            	Este producto no admite m&aacute;s adicionales. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'unknown_relationship'}
			<div class="span-10 error" align="center">
            	Existe una relaci&oacute;n con el titular desconocida. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'destination_unknown'}
			<div class="span-10 error" align="center">
            	El destino ingresado es desconocido. <br /> 
            	Por favor revise el archivo.
            </div>
		{elseif $error eq 'wrong_travel_dates'}
            <div class="span-10 error" align="center">
            	Las fechas de viaje son incorrectas. <br /> 
            	Por favor revise el archivo.
            </div>
        {elseif $error eq 'travelerlog_error'}
            <div class="span-10 error" align="center">
            	Hubo errores al ingresar a los pasajeros. Por favor comun&iacute;quese con el administrador.
            </div>
        {elseif $error eq 'main_subsale_insert_error'}
            <div class="span-10 error" align="center">
            	Hubo errores al registrar el viaje. Por favor vuelva a intentarlo.
            </div>
        {/if}
    {elseif $page eq 'dealers'}
    	{if $error eq 1}
            <div class="span-10 error" align="center">
            	Usted no tiene permiso para acceder a esta opci&oacute;n.
            </div>
        {/if}
    {elseif $page eq 'assistance'}
    	{if $error eq 1}
            <div class="span-10 error" align="center">
            	Usted no tiene permiso para dar Asistencias.
            </div>
        {/if}
    {elseif $page eq 'sales_modifications'}
        {if $error eq 1}
                <div class="span-10 success" align="center">
                    Se ha actualizado la informaci&oacute;n de la tarjeta!
                </div>
        {elseif $error eq 2}
                <div class="span-10 error" align="center">
                    Hubo errores en el registro de la solicitud.
                </div>
        {elseif $error eq 3}
            <div class="span-10 error" align="center">
            	Hubo errores al actualizar los datos de la tarjeta.
            </div>
		{elseif $error eq 4}
            <div class="span-10 error" align="center">
            	Hubo errores al cargar los datos de la tarjeta.
            </div>
		{elseif $error eq 5}
            <div class="span-10 error" align="center">
            	Se ha solicitado el cambio a gerencia. Sin embargo, no se pudieron generar las alertas para <br>los usuarios en su pa&iacute;s. Lo sentimos.
            </div>
		{elseif $error eq 6}
            <div class="span-10 error" align="center">
            	La tarjeta ya se encuentra en el estado al que se desea cambiar.
            </div>
		{elseif $error eq 7}
            <div class="span-10 error" align="center">
            	La fecha de inicio debe ser mayor a la fecha anterior y a la fecha actual.
            </div>
		{elseif $error eq 8}
            <div class="span-10 error" align="center">
            	EL n&uacute;mero de d&iacute;as debe ser igual o menor a los de la venta original.
            </div>
        {/if}
	{elseif $page eq 'editSale'}
        {if $error eq 1}
                <div class="span-10 success" align="center">
                   La solicitud se ingres&oacute; exitosamente.
                </div>
        {elseif $error eq 2}
                <div class="span-10 error" align="center">
                    Hubo errores al guardar el historial de la modificaci&oacute;n.
                </div>
        {elseif $error eq 3}
            <div class="span-10 error" align="center">
            	Hubo errores al actualizar los datos de la tarjeta.
            </div>
		{elseif $error eq 4}
            <div class="span-10 error" align="center">
            	La solicitud de reembolso se ingres&oacute; exitosamente. Sin embargo, no se pudieron generar todas las alertas a los usuarios correspondientes.
            </div>
		{elseif $error eq 5}
            <div class="span-10 error" align="center">
            	No se pudo registar al nuevo Cliente
            </div>
		{elseif $error eq 6}
            <div class="span-10 success" align="center">
            	 La solicitud se ingres&oacute; exitosamente. Sin embargo,<br>
                no se pudo enviar el mail con la informaci&oacute;n de ingreso al cliente.
            </div>
		{elseif $error eq 7}
            <div class="span-10 error" align="center">
            	 No se pudo realizar la actualizaci&oacute; del cliente.
            </div>
		{elseif $error eq 8}
            <div class="span-10 success" align="center">
				 La solicitud se ingres&oacute; exitosamente. Sin embargo,<br>
            	 no se pudo insertar o actualizar la informaci&oacute;n de las preguntas de adulto mayor de los extras.
            </div>
		{elseif $error eq 9}
                <div class="span-10 success" align="center">
					La venta ha sido modificada existosamente
                </div>
        {/if}

	{elseif $page eq 'travelerRegister'}
		<div class="span-10 {if $error eq 5}success{else}error{/if}" align="center">
			{if $error eq 0}
				No se ha podido registrar el Viaje. <br/>
			{elseif $error eq 1}
				Los Datos del Cliente no se pudieron actualizar. <br/>
			{elseif $error eq 2}
				El cliente no pudo ser registrado. Intentelo nuevamente. <br/>
			{elseif $error eq 3}
				Las preguntas de adulto mayor no se han podido registrar. Intentelo nuevamente. <br/>
			{elseif $error eq 4}
				No se puede registrar el viaje ya que la tarjeta no est&aacute; activa.<br/>
			{elseif $error eq 5}
				Se ha registrado el viaje Exitosamente!<br/>
				<a  id="printRegister" href="{$document_root}modules/sales/travelRegister/pPrintRegister/{$printingId}" target="_blank">Imprimir Registro</a>
			{/if}
		</div>
	{elseif $page eq 'travelerModification'}
    	{if $error neq 0}
            <div class="span-10 error" align="center">
            	{if $error eq 1}
					Los Datos del Cliente no se pudieron actualizar. <br/>
				{/if}
				{if $error eq 2}
					El cliente no pudo ser registrado. Intentelo nuevamente. <br/>
				{/if}
				{if $error eq 3}
					Las preguntas de adulto mayor no se han podido registrar. Intentelo nuevamente. <br/>
				{/if}
				{if $success eq 0}
					No se ha podido registrar la Modificaci&oacute;n del Viaje. <br/>
				{/if}
            </div>
        {/if}
		{if $success eq 1}
			<div class="span-10 success" align="center">
                    La modificaci&oacute;n del Viaje se a realizado exitosamente!<br/>
					<a  id="printRegister" href="{$document_root}modules/sales/travelRegister/pPrintRegister/{$printingId}" target="_blank">Imprimir Registro</a>
            </div>
		{/if}
	{elseif $page eq 'refund'}
    	{if $error neq 0}
            <div class="span-10 {if $error eq 1}success{else}error{/if}" align="center">
            	{if $error eq 1}
					La solicitud se ingres&oacute; exitosamente.
				{elseif $error eq 2}
					Hubo errores en el registro de la solicitud.
				{elseif $error eq 3}
					La solicitud de reembolso se ingres&oacute; exitosamente. Sin embargo, no se pudieron generar todas las alertas a los usuarios correspondientes.
				{/if}
            </div>
        {/if}
	{elseif $page eq 'creditNote'}
    	{if $error neq 0}
            <div class="span-10 error" align="center">
            	{if $error eq 1}
					La nota de cr&eacute;dito no existe.
				{elseif $error eq 2}
					La nota de cr&eacute;dito ya fue impresa.
				{/if}
            </div>
        {/if}
	{elseif $page eq 'printInvoice'}
    	{if $error neq 0}
            <div class="span-10 error" align="center">
            	{if $error eq 1}
					No se tienen suficientes datos para imprimir la Factura.
				{/if}
            </div>
        {/if}
	{elseif $page eq 'bonusLiquidation'}
    	{if $error neq 1}
            	 
            <div class="span-10 error" align="center">
            	{if $error eq 2}
					Hubo Un error al registrar los Incentivos que han sido Liquidados.
				{/if}
				{if $error eq 3}
					Hubo un error al registrar los premios canjeados.
				{/if}
				{if $error eq 4}
					No se ha podido registrar la liquidaci&oacute;n.
				{/if}
                                {if $error eq 5}
					Ya existe un Canje de Incentivos previo en proceso, por favor contactese con su Asesor Comercial para mayor informaci&oacute;n
				{/if}
                                {if $error eq 7}
					La Liquidaci&oacute;n ya fue impresa
				{/if}
            </div>    
                
            	{elseif $error eq 1}
                    <div class="span-10 success" align="center">
			Su solicitud de Canje Numero {$success} fue ingresada exitosamente
                    </div>
		{/if}
         
	{elseif $page eq 'comissions'}
		{if $error neq 0}
            <div class="span-10 error" align="center">
				{if $error neq 1}
					Hubo errores al registrar la liquidaci&oacute;n, Vuelva a intentarlo.
				{/if}
            </div>
        {/if}
	{elseif $page eq 'masive_sales'}
		{if $error eq 1}
            <div class="span-10 success" align="center">
            	Contrase&ntilde;a cambiada exitosamente!
            </div>
        {elseif $error eq 2}
        	<div class="span-10 error" align="center">
            	Solamente un usuario Counter puede realizar ventas
            </div>
        {elseif $error eq 3}
            <div class="span-10 error" align="center">
            	Su comercializador actualmente no tiene asignado una cartera de productos. <br>
                Por favor comun&iacute;quese con el administrador.
            </div>
        {elseif $error eq 4}
            <div class="span-10 success" align="center">
            	La Venta Masiva ha sido realizada exitosamente.<br>
				{if $sale.card_number_sal}Su n&uacute;mero de tarjeta es: {$sale.card_number_sal}<br>{/if}
				<a href="{$document_root}modules/sales/fChooseMasiveSales">Registro de Viajeros</a> <br>
				<a  id="printContract"  serial_sal="{$printingId}" href="{$document_root}modules/sales/pPrintSalePDF/{$printingId}/1" target="_blank">Imprimir Contrato</a>
            </div>
		{/if}
	{elseif $page eq 'pageError'}
		<div class="span-10 error" align="center">
			Usted no tiene permisos para esta acceder a la p&aacute;gina seleccionada.
		</div>
	{elseif $page eq 'estimator'}
    	{if $error eq 1}
            <div class="span-10 success" align="center">
				Su compra fue procesada exitosamente!
            </div>
        {/if}
        {if $error eq 2}
            <div class="span-10 error" align="center">
				No esta habilitado para realizar Ventas Telefonicas.<br/>
				Pese a que su perfil tiene asignada la opci&oacute;n de venta telef&oacute;nica, es necesario que tenga la habilitaci&oacute;n para realizar las mismas. 
				Por favor cont&aacute;ctese con el administrador.
            </div>
        {/if}
	{elseif $page eq 'myAccount'}
    	{if $error eq 1}
            <div class="span-10 success" align="center">
				Sus datos fueron actualizados exitosamente.
            </div>
        {elseif $error eq 2}
            <div class="span-10 error" align="center">
				Hubo errores al actualizar sus datos. Por favor comun&iacute;quese con el administrador.
            </div>
        {/if}
	{elseif $page eq 'firstAccessUpdate'}
    	{if $error eq 1}
            <div class="span-10 error" align="center">
            	Lo sentimos, sus datos ya han sido actualizados. <br><br> 
				Para cambiar alguna informaci&oacute;n, haga uso de la opci&oacute;n "Mi Cuenta"
				ubicada en la esquina superior derecha de su pantalla o comun&iacute;quese con el adminsitrador.
            </div>
        {/if}
	{elseif $page eq 'multi_sales'}
    	{if $error eq 1}
            <div class="span-10 success" align="center">
            	Las ventas de han registrado exitosamente!
            </div>
		{elseif $error eq 2}
            <div class="span-10 error" align="center">
            	Hubo errores al recibir la confirmaci&oacute;n de las ventas. No se ha realizado ninguna operaci&oacute;n.
            </div>
		{elseif $error eq 3}
            <div class="span-10 error" align="center">
            	Hubo errores al realizar una de las ventas. <br><br>
				La operaci&oacute;n se ha interrumpido. Comun&iacute;quese con el administrador.
            </div>
		{elseif $error eq 4}
            <div class="span-10 error" align="center">
            	Hubo errores al registrar los adicionales de una de las ventas. <br><br>
				La operaci&oacute;n se ha interrumpido. Comun&iacute;quese con el administrador.
            </div>
		{elseif $error eq 5}
            <div class="span-10 error" align="center">
				Hubo errores al registrar una de las ventas por restricci&oacute;n en el destino. <br><br>
				La operaci&oacute;n se ha interrumpido. Comun&iacute;quese con el administrador.
            </div>
        {/if}
        
        {if $success eq 'yes'}
            <div class="span-10 notice" align="center">
                Uno o varios de los clientes en el archivo no fueron procesados por encontrarse en <br />
                lista negra. Por favor revise el reporte de venta de tarjetas para verificar su <br />
                informaci&oacute;n.
            </div>
        {/if}
    {/if}
</div> 
	{if $page eq 'manteinance'}
		<div class="prepend-2 span-20 append-2 last line center" align="center">
			<img alt="En Mantenimiento" src="{$document_root}img/at_work.jpg" />
		</div>
	{/if}
{else}
<div class="prepend-5 span-19 last">
    <!--<img alt="mainLogo" src="{$document_root}img/mainBackground.gif" />-->
</div>
{/if}

{if $myFavorites}
	<div class="prepend-1 span-22 append-1 last line">
		<div class="span-22 last favorites_background" style="background: url('{$document_root}img/favorites_icons/favorites_background.png')">
			<div class="span-22 last line center fav_title">
				MIS FAVORITOS
			</div>
			
			<div class="span-22 last line">
				{foreach from=$myFavorites key="k" item="fav"}
				{if $k%5 == 0}
					<div class="span-22 last line">
				{/if}
						<div class="fav_button">

							<a href="{$document_root}{$fav.link_opt}">
								<img src="{$document_root}img/favorites_icons/{$fav.icon_opt}" />
								<label>{$fav.name_obl}</label>
							</a>

						</div>
				{if ($key+1)%5 == 0}
					</div><div class="span-22 last line">
				{/if}
				{/foreach}
			</div>
		</div>
	</div>
{/if}

</div>

{if $activePopUp}
	<div id="customPopUp">
		<div class="span-19 center">
			<img src="{$document_root}img/customAdds/{$activePopUp.image}" alt="customPopUp" width="800" />
		</div>
		
		{if $activePopUp.description}
		<div class="span-12">
			{$activePopUp.description|htmlall}
		</div>
		{/if}
		
		<div class="span-12">
			<input type="hidden" id="popTitle" value="{$activePopUp.title}" />
			<input type="hidden" id="popWebLink" value="{$activePopUp.webLink}" />
            <input type="hidden" id="pdfFile" value="{$document_root}pdfs/customAdds/{$activePopUp.pdfFile}"/>
		</div>
	</div>
{/if}