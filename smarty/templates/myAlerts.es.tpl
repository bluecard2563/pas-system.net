{assign var="title" value="MIS ALERTAS"}
<div class="span-24 last title">
	Alertas Pendientes<br />
</div>
<br/>
{if $error}
	<div class="append-8 span-8 prepend-8 last">
	   	{if $error eq 1}
			<div class="span-7 success" align="center">
	           	Alerta atendida.
			</div>
		{else}
			<div class="span-7 error" align="center">
						Hubo un problema al atender la alerta, por favor vuelva a intentarlo.
			</div>
		{/if}
	</div>
{/if}
{if $pending_alerts}
	<br/>
	<div class="span-24 last line">
		<table border="0" id="alertsTable" style="color: #000000;">
			<THEAD>
				<tr bgcolor="#284787">
					<td align="center" class="tableTitle">
						No.
					</td>
					<td align="center" class="tableTitle">
						Tipo
					</td>
					<td align="center" class="tableTitle">
						Descripci&oacute;n
					</td>
					<td align="center"  class="tableTitle">
						Fecha de Creaci&oacute;n
					</td>
					<td align="center" class="tableTitle">
						Solicitante
					</td>
					<td align="center" class="tableTitle">
						Representante
					</td>
					<td align="center" class="tableTitle">
						Revisar
					</td>
	
				</tr>
			</THEAD>
			<TBODY>
				{foreach name="alert" from=$pending_alerts item="s"}
				<tr {if $smarty.foreach.alert.iteration is even}bgcolor="#d7e8f9"{else}bgcolor="#e8f2fb"{/if} >
					<td align="center" class="tableField">
						{$smarty.foreach.alert.iteration}
					</td>
					<td align="center" class="tableField">
						{$global_AlertTypes[$s.type_alt]}
					</td>
					<td align="center" class="tableField">
						{if $s.type_alt == 'FREE'}
							<a href="#" id="alertDetails_{$s.serial_alt}" alert_id="{$s.serial_alt}">
								Ver detalles
							</a>
						{else}
							{$s.message_alt|htmlall}
						{/if}
					</td>
					<td align="center" class="tableField">
						{$s.in_date_alt}
					</td>
					<td align="center" class="tableField">
						{$s.first_name_usr|htmlall} {$s.last_name_usr|htmlall}
					</td>
					<td align="center" class="tableField">
						{$s.requester_manager}
					</td>
					<td align="center" class="tableField">
						{if $s.table_name neq ''}
							<a href="{$document_root}modules/{$s.table_name}{if $s.type_alt neq 'FREE' and $s.type_alt neq 'FILE'}{$s.remote_id}{elseif $s.type_alt eq 'FILE'}{$s.serial_fle}{/if}">
								Atender
							</a>
						{else}N/A{/if}
					</td>
				</tr>
				{/foreach}
			</TBODY>
		</table>
	</div>
	<div class="span-24 last pageNavPosition" id="alertPageNavPosition"></div>
{else}
	<div class="span-8 append-8 prepend-8" align="center">No existen alertas pendientes.</div>
{/if}
<div id="freeSalesDetailsDialog" title="Detalles de solicitud free">
	<div id="dialogContentContainer">No se pudo mostrar el contenido</div>
</div>