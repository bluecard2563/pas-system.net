<div class="span-14" align="center">
	<div class="span-6 prepend-1 emailText line">
		Direcci&oacute;n de Correo electr&oacute;nico:
	</div>
	<div class="span-5">
		<input type="text" name="cpMail" id="cpMail" title='El campo "E-mail" es obligatorio' class="inputEmail">
	</div>
	<div class="prepend-5 span-3 append-5 last">
		<div class="blueButton shortWidth">
    		<span id="goChangePass">Enviar</span>
    		<span id="goChangeStdBy" class="hide_me">Espere</span>
	    </div>
    </div>
    <br/>
    <div class="span-14 registerText" id="registerText">
		<span>Su correo electr&oacute;nico no fue encontrado, Por favor contacte con el administrador.</span>
	</div>
</div>