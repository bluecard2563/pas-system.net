<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1" />
<link rel="shortcut icon" href="{$document_root}img/favicon.ico" ></link>
<title>{$global_system_name}</title>

<!--GLOBAL CSS FILES-->
<link rel="stylesheet" href="{$document_root}css/screen.css" type="text/css" media="screen, projection" />
<!--[if IE 6]>
<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/src/ie.css" />
<![endif]-->
<link rel="stylesheet" href="{$document_root}css/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/jquery-ui/jquery-ui.css" />
<link rel="stylesheet" href="{$document_root}css/system_styles.css" type="text/css" />
<link rel="stylesheet" href="{$document_root}css/ie_system_styles.css" type="text/css" />
<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="{$document_root}css/ipad.css" type="text/css" />
<!--END OF THE GLOBAL CSS FILES-->
{if $css_file}
	<link rel="stylesheet" type="text/css" href="{$document_root}css/{$css_file}" />
{/if}
 
<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery.js"></script>
<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery-validation.js"></script>
<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery-ui.js"></script>
<script type="text/javascript" src="{$document_root}js/global_vars.js"></script>
<script type="text/javascript" src="{$document_root}js/global_functions.js"></script>
{if $js_file}
    <script type="text/javascript" src="{$document_root}js/{$js_file}"></script>
{/if}
</head>

<body class="inner_body container">
    <div id="login-body" class="span-13 last">
        {$buffer}
    </div>
    <div id="dialogChangePass" title="Olvido de Clave">
		{include file="templates/fChangePass.es.tpl"}
	</div>
</body>
</html>