<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1" />
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<link rel="shortcut icon" href="{$document_root}img/favicon.ico" />

		<!--GLOBAL CSS FILES-->
		<link rel="stylesheet" href="{$document_root}css/screen.css" type="text/css" media="screen, projection" />
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/stylish-select.css" />
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/jScrollPane.css" />
		<link rel="stylesheet" href="{$document_root}css/jquery.autocomplete.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="{$document_root}css/superfish-admin.css" media="screen" />
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/superfish-vertical.css" />
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/jquery-ui/jquery-ui-datepicker.css" />
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/menu-style.css" />
		<link rel='stylesheet' type='text/css' href='{$document_root}css/fullcalendar.css' />
		<link rel='stylesheet' type='text/css' href='{$document_root}css/fullcalendar-theme.css' />
		<link rel="stylesheet" href="{$document_root}css/jquery.treeview.css" />
		<link rel="stylesheet" href="{$document_root}css/red-treeview.css" />
		<link rel="stylesheet" href="{$document_root}css/system_styles.css" type="text/css" />
		<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="{$document_root}css/ipad.css" type="text/css" />
		<link rel="stylesheet" href="{$document_root}css/ie_system_styles.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="{$document_root}css/menu1.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="{$document_root}css/paging.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="{$document_root}css/tooltip.css" media="screen" />
		<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" media="all" href="{$document_root}css/src/ie.css" />
		<![endif]-->
		{if $css_file}
		<link rel="stylesheet" type="text/css" href="{$document_root}css/{$css_file}" />
		{/if}
		<!--END CSS FILES-->

		<!--GLOBAL JS FILES-->
		<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery.js" ></script>
		<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery-validation.js"></script>
		<script type="text/javascript" src="{$document_root}js/jScrollPane.js"></script>
		<script type="text/javascript" src="{$document_root}js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery-ui.js"></script>
		<script type='text/javascript' src='{$document_root}js/jqueryLibrary/jquery-datepicker.js'></script>
		<script type="text/javascript" src="{$document_root}js/jqueryLibrary/jquery-form.js"></script>
		<script type='text/javascript' src='{$document_root}js/jqueryLibrary/jquery.autocomplete.js'></script>
		<script type="text/javascript" src="{$document_root}js/estimator/jquery.wizard.js"></script>
		<script type="text/javascript" src="{$document_root}js/global_vars.js"></script>
		<script type="text/javascript" src="{$document_root}js/global_functions.js"></script>
		<script type="text/javascript" src="{$document_root}js/modules/estimator/estimator-commons.js"></script>
		<script type="text/javascript" src="{$document_root}js/system.js"></script>
		<script type='text/javascript' src='{$document_root}js/fullcalendar.js'></script>
		<script type="text/javascript" src="{$document_root}js/mbMenu.js"></script>
		<script type="text/javascript" src="{$document_root}js/jquery.hoverIntent.js"></script>
		<script type="text/javascript" src="{$document_root}js/paging.js"></script>
		<script type="text/javascript" src="{$document_root}js/ajaxupload.js"></script>
		<script type="text/javascript" src="{$document_root}js/tooltip.js"></script>

		{if $js_file}
		<script type="text/javascript" src="{$document_root}js/{$js_file}"></script>
		{/if}
		<!--END JS FILES-->
		<title>{$title}</title>
	</head>


	<body class="inner_body container">
		{*<!--[if lt IE 8]>
		<div class="container">
		<div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;padding:10px 0;'>
		<div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>
		  <div style='width: 75px; float: left;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-warning.jpg' alt='Warning'/></div>
		  <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>
			<div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>Usted est&aacute; usando un navegador obsoleto.</div>
			<div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>Para navegar mejor por este sitio, por favor, actualice su navegador.</div>
		  </div>
		  <div style='width: 75px; float: left;'><a href='http://www.mozilla-europe.org/es/firefox/' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox 3.5'/></a></div>
		  <div style='width: 75px; float: left;'><a href='http://www.microsoft.com/downloads/details.aspx?FamilyID=341c2ad5-8c3d-4347-8c03-08cdecd8852b&DisplayLang=es' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-ie8.jpg' style='border: none;' alt='Get Internet Explorer 8'/></a></div>
		  <div style='width: 73px; float: left;'><a href='http://www.apple.com/es/safari/download/' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari 4'/></a></div>
		  <div style='float: left;'><a href='http://www.google.com/chrome?hl=es' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>
		</div>
		</div>
		</div>
		<![endif]-->*}
		<div class="span-24 last banner">
			<div class="prepend-1 span-12">
				<img class="link" src="{$document_root}img/banner/pas-logo.png" onclick="location.href='{$document_root}main'" />
			</div>
			
			<div class="prepend-1 span-5">
				<div class="user_info">
					<div class="span-5 last line center">
						<label class="info_title">Bienvenido!</label>
					</div>
					
					<div class="span-5 last center">
						<label class="user_name">{$nombreUsuario}</label>
					</div>

					<div class="span-5 last line center">
						{assign var="weekday" value=$date.weekday}
					{assign var="month" value=$date.month}
						{$global_weekDaysNumb.$weekday}, {$date.day} de {$global_weekMonths.$month} del {$date.year}
						<div class="span-5" id="user_alerts_container" style="display: none;">
							  <a href="{$document_root}myAlerts" style="color:white; text-decoration: none; cursor: pointer;"><label id="user_alerts_number"></label>&nbsp;<img src="{$document_root}img/alert.png"/></a>
						  </div>
					</div>
				</div>
			</div>
			
			<div class="span-4">
				<input type="button" class="myaccount" value="Mi Cuenta" onclick="location.href='{$document_root}myAccount'"  title="Mi Cuenta"/>
				<input type="button" class="logout" value="Salir del Sistema" onclick="if(confirm('\u00BF Desea cerrar la sesi\u00F3n ?'))location.href='{$document_root}index'" title="Cerrar Sesi&oacute;n" />
			</div>
		</div>

		{get_menu menu=$menu}
		<div class="span-24 last">
			<div class="span-24 last contents">
				{$buffer}
			</div>
		</div>

		<div class="span-24 last footer">
			<a href="http://www.planet-assist.net" target="_blank"><img src="{$document_root}img/developedIcon.png" alt="Planet Assist Inc." border="0" /> </a> 
		</div>

		<div id="loader">
			<div id="ajax_loader"></div>
			<div class="ajax_loader_text">Cargando...</div>
		</div>
		<div id="JSWarning">
			<div id="ajax_loader"></div>
			<div class="disableJavascript"><div class="disableJavascriptIco"></div>Por favor active su Javascript </div>
		</div>
	</body>
</html>