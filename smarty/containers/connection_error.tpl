<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1" />
<link rel="shortcut icon" href="{$document_root}img/favicon.ico" />

<!--GLOBAL CSS FILES-->
<link rel="stylesheet" href="{$document_root}css/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="{$document_root}css/system_styles.css" type="text/css"/>
<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="{$document_root}css/ipad.css" type="text/css" />
<!--END OF THE GLOBAL CSS FILES-->
<title>{$title}</title>
</head>

<body class="inner_body container">
    <div class="span-24 last contents">{$buffer}</div>

	<div class="span-24 last line center" style="margin-top: 3px;">
		<a href="http://www.planet-assist.net" target="_blank"><img src="{$document_root}img/developedIcon.png" alt="Planet Assist Inc." border="0" /> </a> 
	</div>
</body>
</html>