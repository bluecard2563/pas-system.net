<?php /* Smarty version 2.6.22, created on 2023-09-14 10:33:55
         compiled from templates/main.es.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlall', 'templates/main.es.tpl', 1184, false),)), $this); ?>
<?php $this->assign('title', "P&Aacute;GINA PRINCIPAL"); ?>
<div class="span-24 last mainContent">
<?php if ($this->_tpl_vars['page'] != ''): ?>
<div class="append-7 span-10 prepend-7 last mainAlerts">
    <?php if ($this->_tpl_vars['page'] == 'user'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
       		 <div class="span-10 success" align="center">
            	El usuario se ha registrado exitosamente!
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
        	<div class="span-10 error" align="center">
            	Hubo errores en el ingreso. Por favor vuelva a intentarlo.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 3): ?>
        	<div class="span-10 success" align="center">
            	El usuario se ha actualizado exitosamente!
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 4): ?>
        	<div class="span-10 error" align="center">
            	Hubo errores en la actualizaci&oacute;n. Por favor vuelva a intentarlo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 5): ?>
        	<div class="span-10 success" align="center">
            	La contrase&ntilde;a se ha reestablecido correctamente
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 6): ?>
        	<div class="span-10 success" align="center">
            	La contrase&ntilde;a se ha reestablecido correctamente<br>
				No se pudo enviar el e-mail.
            </div>
        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['page'] == 'freelance'): ?>
        <?php if ($this->_tpl_vars['error'] == 1): ?>
                <div class="span-10 success" align="center">
                    El Agente Libre se ha registrado exitosamente!
                </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
                <div class="span-10 error" align="center">
                    Hubo errores en el ingreso. Por favor vuelva a intentarlo.
                </div>
        <?php elseif ($this->_tpl_vars['error'] == 3): ?>
            <div class="span-10 success" align="center">
            	La actualizaci&oacute;n se ha realizado exitosamente!
            </div>
        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['page'] == 'myAccount'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
       		 <div class="span-10 success" align="center">
            	Datos actualizados exitosamente!
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
        	<div class="span-10 error" align="center">
            	Hubo errores en el cambio. Por favor vuelva a intentarlo.
            </div>
        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['page'] == 'sales'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 success" align="center">
            	Contrase&ntilde;a cambiada exitosamente!
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
        	<div class="span-10 error" align="center">
            	Solamente un usuario Counter puede realizar ventas
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 3): ?>
            <div class="span-10 error" align="center">
            	Su comercializador actualmente no tiene asignado una cartera de productos. <br>
                Por favor comun&iacute;quese con el administrador.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 4): ?>
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente. 
                <?php if ($this->_tpl_vars['sale']['card_number_sal']): ?><br> Su n&uacute;mero de tarjeta es: <?php echo $this->_tpl_vars['sale']['card_number_sal']; ?>
<br><?php endif; ?>
                <?php if ($this->_tpl_vars['printingId']): ?>
                    <a  id="printContract"  serial_sal="<?php echo $this->_tpl_vars['printingId']; ?>
" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/pPrintSalePDF/<?php echo $this->_tpl_vars['printingId']; ?>
/1" target="_blank">Imprimir Ad&eacute;ndum</a> <br>
                    <?php if ($this->_tpl_vars['contract_logic']): ?>
                        <a  id="printNewContract"  serial_sal="<?php echo $this->_tpl_vars['printingId']; ?>
" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/pPrintContractPDF/<?php echo $this->_tpl_vars['printingId']; ?>
/1" target="_blank">Imprimir Contrato</a> <br>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['printInvoiceData']['total_number_inv'] != 0): ?>
                    <?php if ($this->_tpl_vars['printInvoiceData']['total_number_inv'] != 99): ?>
                                Su numero de factura es: <?php echo $this->_tpl_vars['printInvoiceData']['total_number_inv']; ?>

                    <?php else: ?>
                        El contrato no pudo ser facturado de manera autom&aacute;tica, el mismo ser&aacute; facturado manualmente. 
                     <?php endif; ?>   
                <?php endif; ?>    

                <?php if ($this->_tpl_vars['cupon_code']): ?><br> Su n&uacute;mero de cup&oacute;n es: <?php echo $this->_tpl_vars['cupon_code']; ?>
<?php endif; ?>				
            </div>
           <?php if ($this->_tpl_vars['serial_inv']): ?>
			   <?php if (( USE_PTP ) || $this->_tpl_vars['ptp_use'] == 'YES'): ?>
				   <div class="center">
					   <img src="http://pas-system.net/img/place_to_pay/placetoplay.png" alt="" style="height: 70px;width: 180px;">
					   <img src="http://pas-system.net/img/place_to_pay/logos_tarjetasANT.png" alt="">
				   </div>

				   <div class="center">
					   <a href="" id="pre">Preguntas frecuentes</a><br>
					   <br>
					   <input type="checkbox" id="checkme"/><label for="check">Acepto</label> <a href="" id="term">Términos y condiciones.</a><br /><br>

					   <div style="float: left">
						   <form>
							   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input title="Ejecutar pago desde computador" name="btnPtPS" id="btnPtPS" type="button" value="Pagar ahora" style="display: none" onclick="document.getElementById('checkme').disabled=true;document.getElementById('btnPtPM').disabled=true;this.disabled=true;window.open('<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/placeToPay/pPaymentPtoP.php?serial_inv=<?php echo $this->_tpl_vars['serial_inv']; ?>
','_blank')" disabled/>
						   </form>
					   </div>
					   <div style="float: left">
						   <form>
							   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input title="Ejecutar pago desde celular" name="btnPtPM" id="btnPtPM" type="button" value="Pago desde dispositivo cliente" onclick="document.getElementById('checkme').disabled=true;document.getElementById('btnPtPS').disabled=true;this.disabled=true;window.open('<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/placeToPay/pSendMailPaymentPtoP.php?serial_inv=<?php echo $this->_tpl_vars['serial_inv']; ?>
&mail=true','_blank')" disabled/>
						   </form>
					   </div>
				   </div>
			 <?php endif; ?>
			<?php endif; ?>

        <?php elseif ($this->_tpl_vars['error'] == 5): ?>
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente. <br>
                No se pudo enviar el mail con la informaci&oacute;n de ingreso al cliente.
                <?php if ($this->_tpl_vars['sale']['card_number_sal']): ?><br> Su n&uacute;mero de tarjeta es: <?php echo $this->_tpl_vars['sale']['card_number_sal']; ?>
<br><?php endif; ?>
                <?php if ($this->_tpl_vars['printingId']): ?>
                    <a  id="printContract"  serial_sal="<?php echo $this->_tpl_vars['printingId']; ?>
" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/pPrintSalePDF/<?php echo $this->_tpl_vars['printingId']; ?>
/1" target="_blank">Imprimir Ad&eacute;ndum</a> <br>
                    <?php if ($this->_tpl_vars['contract_logic']): ?>
                        <a  id="printNewContract"  serial_sal="<?php echo $this->_tpl_vars['printingId']; ?>
" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/pPrintContractPDF/<?php echo $this->_tpl_vars['printingId']; ?>
/1" target="_blank">Imprimir Contrato</a> <br>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['printInvoiceData']['total_number_inv'] != 0): ?>
                    <?php if ($this->_tpl_vars['printInvoiceData']['total_number_inv'] != 99): ?>
                                Su numero de factura es: <?php echo $this->_tpl_vars['printInvoiceData']['total_number_inv']; ?>

                    <?php else: ?>
                        El contrato no pudo ser facturado de manera autom&aacute;tica, el mismo ser&aacute; facturado manualmente. 
                     <?php endif; ?>   
                <?php endif; ?> 
				<?php if ($this->_tpl_vars['cupon_code']): ?><br> Su n&uacute;mero de cup&oacute;n es: <?php echo $this->_tpl_vars['cupon_code']; ?>
<?php endif; ?>
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 6): ?>
            <div class="span-10 error" align="center">
            	Errores al ingresar un nuevo cliente <br>
                No se ha realizado la venta.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 7): ?>
            <div class="span-10 error" align="center">
            	No se ha realizado la venta.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 8): ?>
            <div class="span-10 error" align="center">
            	Error al agregar Servicios.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 9): ?>
            <div class="span-10 error" align="center">
            	Error al agregar Extras.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 10): ?>
            <div class="span-10 error" align="center">
            	Error al ingresar Nuevo Cliente.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 11): ?>
            <div class="span-10 success" align="center">
            	La autorizaci&oacute;n de ventas se realiz&oacute; exitosamente.
				<?php if ($this->_tpl_vars['cupon_code']): ?><br> Su n&uacute;mero de cup&oacute;n es: <?php echo $this->_tpl_vars['cupon_code']; ?>
<?php endif; ?>
			</div>
        <?php elseif ($this->_tpl_vars['error'] == 12): ?>
            <div class="span-10 error" align="center">
            	Hubieron errores al autorizar las ventas.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 13): ?>
            <div class="span-10 error" align="center">
            	No se pudo cargar correctamente la informaci&oacute;n del comercializador.<br>
				Por favor comun&iacute;quese con el administrador.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 14): ?>
            <div class="span-10 error" align="center">
            	Los cambios se han registrado exitosamente! Sin embargo, <br>algunos de los e-mails de notificaci&oacute;n no se han podido enviar.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 15): ?>
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente.<br>
				<?php if ($this->_tpl_vars['sale']['card_number_sal']): ?>Su n&uacute;mero de tarjeta es: <?php echo $this->_tpl_vars['sale']['card_number_sal']; ?>
<br><?php endif; ?>
				<?php if ($this->_tpl_vars['printingId']): ?>
					<a href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/travelRegister/fRegisterTravel/<?php echo $this->_tpl_vars['printingId']; ?>
">Registro de Viajes</a> <br>
					<!--<a  id="printContract" serial_sal="<?php echo $this->_tpl_vars['printingId']; ?>
" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/pPrintSalePDF/<?php echo $this->_tpl_vars['printingId']; ?>
/1" target="_blank">Imprimir Contrato</a>-->
				<?php endif; ?>
                                <?php if ($this->_tpl_vars['printInvoiceData']['total_number_inv'] != 99): ?>
                        Su numero de factura es: <?php echo $this->_tpl_vars['printInvoiceData']['total_number_inv']; ?>

                <?php else: ?>
                    El contrato no pudo ser facturado de manera autom&aacute;tica, el mismo ser&aacute; facturado manualmente. 
                 <?php endif; ?> 
				<?php if ($this->_tpl_vars['cupon_code']): ?><br> Su n&uacute;mero de cup&oacute;n es: <?php echo $this->_tpl_vars['cupon_code']; ?>
<?php endif; ?>
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 16): ?>
            <div class="span-10 success" align="center">
            	La Venta ha sido realizada exitosamente.<br> Sin embargo, no se pudieron crear todas las alertas.
				<?php if ($this->_tpl_vars['cupon_code']): ?><br> Su n&uacute;mero de cup&oacute;n es: <?php echo $this->_tpl_vars['cupon_code']; ?>
<?php endif; ?>
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 17): ?>
            <div class="span-10 success" align="center">
            	La Venta Telefonica ha sido realizada exitosamente.
				<?php if ($this->_tpl_vars['cupon_code']): ?><br> Su n&uacute;mero de cup&oacute;n es: <?php echo $this->_tpl_vars['cupon_code']; ?>
<?php endif; ?>
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 18): ?>
            <div class="span-10 success" align="center">
            	El n&uacute;mero de tarjeta seleccionado ya ha sido asignado a otra venta.
            	Por favor vuelva a generar la venta.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 19): ?>
            <div class="span-10 success" align="center">
            	Ha sido imposible obtener El n&uacute;mero de tarjeta.
            	Por favor contacte al administrador.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 20): ?>
            <div class="span-10 error" align="center">
            	La sucursal a la que usted pertenece no tiene una cartera de productos asignada.
				Lo sentimos.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 21): ?>
            <div class="span-10 error" align="center">
            	<p>No existe información de declaración de salud para el cliente <?php echo $_SESSION['statement_name']; ?>
 con identificación N° <?php echo $_SESSION['statement_document']; ?>
.</p>
            	<p>La declaración de salud es requerida para ejecutar la venta.</p>
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'masiveSales'): ?>
		<?php if ($this->_tpl_vars['error'] == 'success'): ?>
			<div class="span-10 success" align="center">
            	La venta se ha registrado exitosamente!
				<?php if ($this->_tpl_vars['success'] == 'YES'): ?>
					<br /> Tenga presente que algunos viajeron no fueron inclu&iacute;dos por restricciones de destino en el producto.
				<?php endif; ?>
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'wrong_extension'): ?>
			<div class="span-10 error" align="center">
            	La extensi&oacute;n del archivo es err&oacute;nea. <br />
            	S&oacute;lo se admiten .xls y .xlsx
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'upload_error'): ?>
			<div class="span-10 error" align="center">
            	Hubo un error en la carga del archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'customer_secuence_error'): ?>
			<div class="span-10 error" align="center">
            	La secuencia de pasajeros en el archivo es incorrecta.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'extras_not_allowed'): ?>
			<div class="span-10 error" align="center">
            	El producto seleccionado no admite adicionales.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'customer_extras_secuence_error'): ?>
			<div class="span-10 error" align="center">
            	Existe un error en las relaciones entre clientes. Revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'wrong_numbers_format'): ?>
			<div class="span-10 error" align="center">
            	El formato para los numeros de tel&eacute;fono de los clientes es incorrecto.<br /> 
            	Por favor rev&iacute;selo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'seniors_not_allowed'): ?>
			<div class="span-10 error" align="center">
            	No se admiten adultos mayores como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>			
		<?php elseif ($this->_tpl_vars['error'] == 'only_children_allowed_for_extras'): ?>
			<div class="span-10 error" align="center">
            	Solamente se admiten menores como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'only_adults_allowed_for_extras'): ?>
			<div class="span-10 error" align="center">
            	Solamente se admiten adultos como adicionales en esta tarjeta. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'no_file_found'): ?>
			<div class="span-10 error" align="center">
            	No se pudo encontrar el archivo a cargar. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'no_price_found'): ?>
			<div class="span-10 error" align="center">
            	No se pudo encontrar un precio para uno de los pasajeros. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'duplicated_spouse'): ?>
			<div class="span-10 error" align="center">
            	Existe un registor duplicado de "Conyuge" para un cliente. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'extras_not_allowed'): ?>
			<div class="span-10 error" align="center">
            	Este producto no admite m&aacute;s adicionales. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'unknown_relationship'): ?>
			<div class="span-10 error" align="center">
            	Existe una relaci&oacute;n con el titular desconocida. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'destination_unknown'): ?>
			<div class="span-10 error" align="center">
            	El destino ingresado es desconocido. <br /> 
            	Por favor revise el archivo.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 'wrong_travel_dates'): ?>
            <div class="span-10 error" align="center">
            	Las fechas de viaje son incorrectas. <br /> 
            	Por favor revise el archivo.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 'travelerlog_error'): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al ingresar a los pasajeros. Por favor comun&iacute;quese con el administrador.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 'main_subsale_insert_error'): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al registrar el viaje. Por favor vuelva a intentarlo.
            </div>
        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['page'] == 'dealers'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 error" align="center">
            	Usted no tiene permiso para acceder a esta opci&oacute;n.
            </div>
        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['page'] == 'assistance'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 error" align="center">
            	Usted no tiene permiso para dar Asistencias.
            </div>
        <?php endif; ?>
    <?php elseif ($this->_tpl_vars['page'] == 'sales_modifications'): ?>
        <?php if ($this->_tpl_vars['error'] == 1): ?>
                <div class="span-10 success" align="center">
                    Se ha actualizado la informaci&oacute;n de la tarjeta!
                </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
                <div class="span-10 error" align="center">
                    Hubo errores en el registro de la solicitud.
                </div>
        <?php elseif ($this->_tpl_vars['error'] == 3): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al actualizar los datos de la tarjeta.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 4): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al cargar los datos de la tarjeta.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 5): ?>
            <div class="span-10 error" align="center">
            	Se ha solicitado el cambio a gerencia. Sin embargo, no se pudieron generar las alertas para <br>los usuarios en su pa&iacute;s. Lo sentimos.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 6): ?>
            <div class="span-10 error" align="center">
            	La tarjeta ya se encuentra en el estado al que se desea cambiar.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 7): ?>
            <div class="span-10 error" align="center">
            	La fecha de inicio debe ser mayor a la fecha anterior y a la fecha actual.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 8): ?>
            <div class="span-10 error" align="center">
            	EL n&uacute;mero de d&iacute;as debe ser igual o menor a los de la venta original.
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'editSale'): ?>
        <?php if ($this->_tpl_vars['error'] == 1): ?>
                <div class="span-10 success" align="center">
                   La solicitud se ingres&oacute; exitosamente.
                </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
                <div class="span-10 error" align="center">
                    Hubo errores al guardar el historial de la modificaci&oacute;n.
                </div>
        <?php elseif ($this->_tpl_vars['error'] == 3): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al actualizar los datos de la tarjeta.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 4): ?>
            <div class="span-10 error" align="center">
            	La solicitud de reembolso se ingres&oacute; exitosamente. Sin embargo, no se pudieron generar todas las alertas a los usuarios correspondientes.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 5): ?>
            <div class="span-10 error" align="center">
            	No se pudo registar al nuevo Cliente
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 6): ?>
            <div class="span-10 success" align="center">
            	 La solicitud se ingres&oacute; exitosamente. Sin embargo,<br>
                no se pudo enviar el mail con la informaci&oacute;n de ingreso al cliente.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 7): ?>
            <div class="span-10 error" align="center">
            	 No se pudo realizar la actualizaci&oacute; del cliente.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 8): ?>
            <div class="span-10 success" align="center">
				 La solicitud se ingres&oacute; exitosamente. Sin embargo,<br>
            	 no se pudo insertar o actualizar la informaci&oacute;n de las preguntas de adulto mayor de los extras.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 9): ?>
                <div class="span-10 success" align="center">
					La venta ha sido modificada existosamente
                </div>
        <?php endif; ?>

	<?php elseif ($this->_tpl_vars['page'] == 'travelerRegister'): ?>
		<div class="span-10 <?php if ($this->_tpl_vars['error'] == 5): ?>success<?php else: ?>error<?php endif; ?>" align="center">
			<?php if ($this->_tpl_vars['error'] == 0): ?>
				No se ha podido registrar el Viaje. <br/>
			<?php elseif ($this->_tpl_vars['error'] == 1): ?>
				Los Datos del Cliente no se pudieron actualizar. <br/>
			<?php elseif ($this->_tpl_vars['error'] == 2): ?>
				El cliente no pudo ser registrado. Intentelo nuevamente. <br/>
			<?php elseif ($this->_tpl_vars['error'] == 3): ?>
				Las preguntas de adulto mayor no se han podido registrar. Intentelo nuevamente. <br/>
			<?php elseif ($this->_tpl_vars['error'] == 4): ?>
				No se puede registrar el viaje ya que la tarjeta no est&aacute; activa.<br/>
			<?php elseif ($this->_tpl_vars['error'] == 5): ?>
				Se ha registrado el viaje Exitosamente!<br/>
				<a  id="printRegister" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/travelRegister/pPrintRegister/<?php echo $this->_tpl_vars['printingId']; ?>
" target="_blank">Imprimir Registro</a>
			<?php endif; ?>
		</div>
	<?php elseif ($this->_tpl_vars['page'] == 'travelerModification'): ?>
    	<?php if ($this->_tpl_vars['error'] != 0): ?>
            <div class="span-10 error" align="center">
            	<?php if ($this->_tpl_vars['error'] == 1): ?>
					Los Datos del Cliente no se pudieron actualizar. <br/>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['error'] == 2): ?>
					El cliente no pudo ser registrado. Intentelo nuevamente. <br/>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['error'] == 3): ?>
					Las preguntas de adulto mayor no se han podido registrar. Intentelo nuevamente. <br/>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['success'] == 0): ?>
					No se ha podido registrar la Modificaci&oacute;n del Viaje. <br/>
				<?php endif; ?>
            </div>
        <?php endif; ?>
		<?php if ($this->_tpl_vars['success'] == 1): ?>
			<div class="span-10 success" align="center">
                    La modificaci&oacute;n del Viaje se a realizado exitosamente!<br/>
					<a  id="printRegister" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/travelRegister/pPrintRegister/<?php echo $this->_tpl_vars['printingId']; ?>
" target="_blank">Imprimir Registro</a>
            </div>
		<?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'refund'): ?>
    	<?php if ($this->_tpl_vars['error'] != 0): ?>
            <div class="span-10 <?php if ($this->_tpl_vars['error'] == 1): ?>success<?php else: ?>error<?php endif; ?>" align="center">
            	<?php if ($this->_tpl_vars['error'] == 1): ?>
					La solicitud se ingres&oacute; exitosamente.
				<?php elseif ($this->_tpl_vars['error'] == 2): ?>
					Hubo errores en el registro de la solicitud.
				<?php elseif ($this->_tpl_vars['error'] == 3): ?>
					La solicitud de reembolso se ingres&oacute; exitosamente. Sin embargo, no se pudieron generar todas las alertas a los usuarios correspondientes.
				<?php endif; ?>
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'creditNote'): ?>
    	<?php if ($this->_tpl_vars['error'] != 0): ?>
            <div class="span-10 error" align="center">
            	<?php if ($this->_tpl_vars['error'] == 1): ?>
					La nota de cr&eacute;dito no existe.
				<?php elseif ($this->_tpl_vars['error'] == 2): ?>
					La nota de cr&eacute;dito ya fue impresa.
				<?php endif; ?>
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'printInvoice'): ?>
    	<?php if ($this->_tpl_vars['error'] != 0): ?>
            <div class="span-10 error" align="center">
            	<?php if ($this->_tpl_vars['error'] == 1): ?>
					No se tienen suficientes datos para imprimir la Factura.
				<?php endif; ?>
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'bonusLiquidation'): ?>
    	<?php if ($this->_tpl_vars['error'] != 1): ?>
            	 
            <div class="span-10 error" align="center">
            	<?php if ($this->_tpl_vars['error'] == 2): ?>
					Hubo Un error al registrar los Incentivos que han sido Liquidados.
				<?php endif; ?>
				<?php if ($this->_tpl_vars['error'] == 3): ?>
					Hubo un error al registrar los premios canjeados.
				<?php endif; ?>
				<?php if ($this->_tpl_vars['error'] == 4): ?>
					No se ha podido registrar la liquidaci&oacute;n.
				<?php endif; ?>
                                <?php if ($this->_tpl_vars['error'] == 5): ?>
					Ya existe un Canje de Incentivos previo en proceso, por favor contactese con su Asesor Comercial para mayor informaci&oacute;n
				<?php endif; ?>
                                <?php if ($this->_tpl_vars['error'] == 7): ?>
					La Liquidaci&oacute;n ya fue impresa
				<?php endif; ?>
            </div>    
                
            	<?php elseif ($this->_tpl_vars['error'] == 1): ?>
                    <div class="span-10 success" align="center">
			Su solicitud de Canje Numero <?php echo $this->_tpl_vars['success']; ?>
 fue ingresada exitosamente
                    </div>
		<?php endif; ?>
         
	<?php elseif ($this->_tpl_vars['page'] == 'comissions'): ?>
		<?php if ($this->_tpl_vars['error'] != 0): ?>
            <div class="span-10 error" align="center">
				<?php if ($this->_tpl_vars['error'] != 1): ?>
					Hubo errores al registrar la liquidaci&oacute;n, Vuelva a intentarlo.
				<?php endif; ?>
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'masive_sales'): ?>
		<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 success" align="center">
            	Contrase&ntilde;a cambiada exitosamente!
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
        	<div class="span-10 error" align="center">
            	Solamente un usuario Counter puede realizar ventas
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 3): ?>
            <div class="span-10 error" align="center">
            	Su comercializador actualmente no tiene asignado una cartera de productos. <br>
                Por favor comun&iacute;quese con el administrador.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 4): ?>
            <div class="span-10 success" align="center">
            	La Venta Masiva ha sido realizada exitosamente.<br>
				<?php if ($this->_tpl_vars['sale']['card_number_sal']): ?>Su n&uacute;mero de tarjeta es: <?php echo $this->_tpl_vars['sale']['card_number_sal']; ?>
<br><?php endif; ?>
				<a href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/fChooseMasiveSales">Registro de Viajeros</a> <br>
				<a  id="printContract"  serial_sal="<?php echo $this->_tpl_vars['printingId']; ?>
" href="<?php echo $this->_tpl_vars['document_root']; ?>
modules/sales/pPrintSalePDF/<?php echo $this->_tpl_vars['printingId']; ?>
/1" target="_blank">Imprimir Contrato</a>
            </div>
		<?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'pageError'): ?>
		<div class="span-10 error" align="center">
			Usted no tiene permisos para esta acceder a la p&aacute;gina seleccionada.
		</div>
	<?php elseif ($this->_tpl_vars['page'] == 'estimator'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 success" align="center">
				Su compra fue procesada exitosamente!
            </div>
        <?php endif; ?>
        <?php if ($this->_tpl_vars['error'] == 2): ?>
            <div class="span-10 error" align="center">
				No esta habilitado para realizar Ventas Telefonicas.<br/>
				Pese a que su perfil tiene asignada la opci&oacute;n de venta telef&oacute;nica, es necesario que tenga la habilitaci&oacute;n para realizar las mismas. 
				Por favor cont&aacute;ctese con el administrador.
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'myAccount'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 success" align="center">
				Sus datos fueron actualizados exitosamente.
            </div>
        <?php elseif ($this->_tpl_vars['error'] == 2): ?>
            <div class="span-10 error" align="center">
				Hubo errores al actualizar sus datos. Por favor comun&iacute;quese con el administrador.
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'firstAccessUpdate'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 error" align="center">
            	Lo sentimos, sus datos ya han sido actualizados. <br><br> 
				Para cambiar alguna informaci&oacute;n, haga uso de la opci&oacute;n "Mi Cuenta"
				ubicada en la esquina superior derecha de su pantalla o comun&iacute;quese con el adminsitrador.
            </div>
        <?php endif; ?>
	<?php elseif ($this->_tpl_vars['page'] == 'multi_sales'): ?>
    	<?php if ($this->_tpl_vars['error'] == 1): ?>
            <div class="span-10 success" align="center">
            	Las ventas de han registrado exitosamente!
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 2): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al recibir la confirmaci&oacute;n de las ventas. No se ha realizado ninguna operaci&oacute;n.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 3): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al realizar una de las ventas. <br><br>
				La operaci&oacute;n se ha interrumpido. Comun&iacute;quese con el administrador.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 4): ?>
            <div class="span-10 error" align="center">
            	Hubo errores al registrar los adicionales de una de las ventas. <br><br>
				La operaci&oacute;n se ha interrumpido. Comun&iacute;quese con el administrador.
            </div>
		<?php elseif ($this->_tpl_vars['error'] == 5): ?>
            <div class="span-10 error" align="center">
				Hubo errores al registrar una de las ventas por restricci&oacute;n en el destino. <br><br>
				La operaci&oacute;n se ha interrumpido. Comun&iacute;quese con el administrador.
            </div>
        <?php endif; ?>
        
        <?php if ($this->_tpl_vars['success'] == 'yes'): ?>
            <div class="span-10 notice" align="center">
                Uno o varios de los clientes en el archivo no fueron procesados por encontrarse en <br />
                lista negra. Por favor revise el reporte de venta de tarjetas para verificar su <br />
                informaci&oacute;n.
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div> 
	<?php if ($this->_tpl_vars['page'] == 'manteinance'): ?>
		<div class="prepend-2 span-20 append-2 last line center" align="center">
			<img alt="En Mantenimiento" src="<?php echo $this->_tpl_vars['document_root']; ?>
img/at_work.jpg" />
		</div>
	<?php endif; ?>
<?php else: ?>
<div class="prepend-5 span-19 last">
    <!--<img alt="mainLogo" src="<?php echo $this->_tpl_vars['document_root']; ?>
img/mainBackground.gif" />-->
</div>
<?php endif; ?>

<?php if ($this->_tpl_vars['myFavorites']): ?>
    <div class="prepend-1 span-22 append-1 last line">
        <div class="span-22 last favorites_background" style="background: url('<?php echo $this->_tpl_vars['document_root']; ?>
img/favorites_icons/favorites_background.png')">
            <div class="span-22 last line center fav_title">
                MIS FAVORITOS
            </div>

            <div class="span-22 last line">
                <?php $_from = $this->_tpl_vars['myFavorites']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['fav']):
?>
                    <?php if ($this->_tpl_vars['k']%5 == 0): ?>
                        <div class="span-22 last line">
                    <?php endif; ?>

                    <!--Catalogo de Beneicios y Tarifas 2019-->
                    <?php if ($this->_tpl_vars['fav']['digital_file'] == 'YES'): ?>
                        <div id="digitalFiles<?php echo $this->_tpl_vars['fav']['serial_opt']; ?>
" class="fav_button fav_pointer" onclick="openFilesPopup(<?php echo $this->_tpl_vars['fav']['serial_opt']; ?>
);">
                            <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/favorites_icons/<?php echo $this->_tpl_vars['fav']['icon_opt']; ?>
" />
                            <label class="fav_label"><?php echo $this->_tpl_vars['fav']['name_obl']; ?>
</label>
                        </div>
                    <?php elseif ($this->_tpl_vars['fav']['serial_opt'] == 372): ?>
                        <div class="fav_button fav_pointer">
                            <a href="<?php echo $this->_tpl_vars['urlQuote']; ?>
<?php echo $this->_tpl_vars['serial_usr']; ?>
" target="_blank">
                                <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/favorites_icons/<?php echo $this->_tpl_vars['fav']['icon_opt']; ?>
" />
                                <label><?php echo $this->_tpl_vars['fav']['name_obl']; ?>
</label>
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="fav_button fav_pointer">
                            <a href="<?php echo $this->_tpl_vars['document_root']; ?>
<?php echo $this->_tpl_vars['fav']['link_opt']; ?>
">
                                <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/favorites_icons/<?php echo $this->_tpl_vars['fav']['icon_opt']; ?>
" />
                                <label><?php echo $this->_tpl_vars['fav']['name_obl']; ?>
</label>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if (( $this->_tpl_vars['key']+1 ) % 5 == 0): ?>
                        </div><div class="span-22 last line">
                    <?php endif; ?>
               <?php endforeach; endif; unset($_from); ?>
                </div>
            </div>
        </div>
<?php endif; ?>

</div>

<textarea id="customPopUp2" style="display:none;">
		<div class="span-19 center">
		    <strong>Términos y condiciones</strong>
		</div>
		<div class="center">
		<p align="justify">CONDICIONES DE USO www.bluecard.com.ec Bienvenido a BlueCard. Si usted visita o compra a trav&eacute;s de la p&aacute;gina web de BlueCard est&aacute; aceptando las condiciones de la misma. Por favor lea detenidamente cada vez que usted solicite, ahora o en un futuro nuestros servicios, cada una de las tarjetas de asistencia en viajes est&aacute; sujeta a t&eacute;rminos y condiciones especiales que son aceptadas al momento de la compra.  COMUNICACIONES ELECTR&Oacute;NICAS  Cuando visita nuestro sitio web, usted est&aacute; comunic&aacute;ndose de manera electr&oacute;nica. Por lo cual usted est&aacute; consiente en recibir de nuestra parte correos electr&oacute;nicos de avisos, divulgaciones, acuerdos, promociones y otras comunicaciones estando de acuerdo en que estas satisfacen cualquier requerimiento legal en comunicaciones escritas.  COPYRIGHT Todo el contenido incluido en este sitio, como texto, gr&aacute;ficos, logotipos, iconos, im&aacute;genes, clips de audio, descargas digitales, compilaciones de datos, c&oacute;digo de fuente y software, es de propiedad de BLUE CARD basado en las leyes internacionales de Copyright.  LICENCIA Y ACCESO AL SITIO  BLUECARD le otorga una licencia limitada para acceder y hacer uso personal de este sitio y no descargar o modificar cualquier porci&oacute;n de &eacute;l, excepto con el consentimiento expreso y por escrito de BLUECARD.  Esta licencia no incluye cualquier reventa o uso comercial del sitio o de su contenido, la colecci&oacute;n y uso de cualquier listado de planes, descripciones o precios; cualquier uso derivado de este sitio o su contenido; cualquier descarga o copiado de informaci&oacute;n de la cuenta para el beneficio de otro comerciante, o cualquier uso de miner&iacute;a de datos, robots o similares recopilaciones de datos y herramientas de extracci&oacute;n. Este sitio o cualquier porci&oacute;n de &eacute;ste no puede ser reproducido, duplicado, copiado, vendido, revendido, visitado o explotado con fines comerciales sin consentimiento expreso y por escrito de BLUECARD. Usted no puede enmarcar o utilizar t&eacute;cnicas de enmarcado para incluir cualquier marca, logo u otra informaci&oacute;n propietaria (incluyendo im&aacute;genes, textos, dise&ntilde;o de p&aacute;gina, o forma) de BLUECARD sin consentimiento expreso y por escrito. No puede utilizar cualquier etiqueta o cualquier otro &quot;texto oculto&quot; utilizando el nombre de BLUECARD o marcas comerciales sin el consentimiento expreso y por escrito de BLUECARD. Cualquier uso no autorizado termina el permiso o licencia otorgado por BLUECARD. Se le concede una licencia limitada, revocable, y el derecho no exclusivo para crear un hiperv&iacute;nculo a la p&aacute;gina principal de BLUECARD, siempre y cuando el v&iacute;nculo no represente los planes o servicios de BLUECARD, de una manera falsa, enga&ntilde;osa, despectiva o de manera ofensiva. No puede utilizar ning&uacute;n logotipo de BLUECARD o cualquier otro gr&aacute;fico patentado o marca registrada como parte del enlace sin permiso expreso por escrito.  SU CUENTA  Si usted utiliza este sitio, es responsable de mantener la confidencialidad de su cuenta, su contrase&ntilde;a y de restringir el acceso a su computadora, y usted est&aacute; de acuerdo en aceptar la responsabilidad de todas las actividades que ocurran bajo su cuenta o contrase&ntilde;a. BLUECARD se reserva el derecho de rechazar prestar el servicio, cerrar cuentas, retirar o editar contenidos, o cancelar pedidos a su entera discreci&oacute;n.  INFORMACI&Oacute;N INGRESADA  BLUECARD no se responsabiliza por el mal ingreso de informaci&oacute;n de documentos de identidad, pasaportes, nombres, fechas de nacimiento, direcciones, tel&eacute;fonos, contactos y direcciones de correo electr&oacute;nico. Usted no puede utilizar una direcci&oacute;n falsa de correo electr&oacute;nico, suplantar a cualquier persona o entidad, o de otra manera inducir a un error en cuanto a la procedencia de una tarjeta u otro contenido. BLUECARD se reserva el derecho (pero no la obligaci&oacute;n) de eliminar o editar dicho contenido.  RIESGO DE P&Eacute;RDIDA  Al realizar una compra virtual usted recibir&aacute; un n&uacute;mero &uacute;nico de contrato v&iacute;a correo electr&oacute;nico, el cual est&aacute; registrado en nuestro sistema para el momento que usted requiera una asistencia. De darse el caso que usted no reciba el correo electr&oacute;nico puede solicitarlo a trav&eacute;s de nuestra p&aacute;gina web www.bluecard.com.ec en el men&uacute; cont&aacute;ctenos.</p>

<p>PLANES  BLUECARD tiene especificaciones y coberturas muy definidas dentro de sus planes, por lo tanto BLUECARD se asegura de cumplir con las especificaciones definidas para el plan adquirido dentro de las condiciones generales especificadas para el servicio brindado.  Planes Tradicionales.- Turista, Europa Plus, Senior, Platino, Estudiantil, Deportes y Competencias. Planes Elites.-Turista Elite, Usa Elite, Europa Elite, Platino Elite, Senior Elite, Estudiantil Elite, Estudiantil Elite Premium, Ejecutiva Premium, Deportes y Competencias Elite, Precompra Elite, Precompra Elite Primium. PRECIO  Excepto cuando se indique lo contrario, en nuestra p&aacute;gina web se muestra la lista de precios para los planes, estos representan el precio de venta incluido impuestos.  CAMBIOS DE FECHA Y STAND BY  Como parte de nuestro servicio al cliente brindamos la posibilidad de colocar su tarjeta en Stand by y realizar cambios en las fechas de viaje, amparados en las siguientes pol&iacute;ticas de BLUECARD. &bull;Para solicitar cambios en el estado de una tarjeta a stand by o realizar cambios en las fechas de cobertura de la misma, el cliente debe notificar con al menos 72 horas de anticipaci&oacute;n al inicio de la vigencia de la tarjeta, v&iacute;a correo electr&oacute;nico o a trav&eacute;s de las comunicaciones brindadas en nuestra p&aacute;gina web. (Cont&aacute;ctenos, Chat Support, Skype), el operador los podr&aacute; realizar siempre y cuando no haya iniciado cobertura. &bull;El estado Stand by de una tarjeta tiene una duraci&oacute;n de 180 d&iacute;as a partir de la fecha de inicio del primer viaje, luego de &eacute;sta, se caducar&aacute; autom&aacute;ticamente.  ANULACI&Oacute;N DE TARJETAS Y REEMBOLSOS BLUECARD buscar&aacute; siempre brindar el mejor servicio de calidad a sus clientes por tal motivo en compras realizadas a trav&eacute;s de nuestra p&aacute;gina web se aplican las siguientes pol&iacute;ticas de anulaci&oacute;n y reembolsos:  &bull; No se permite la anulaci&oacute;n de un contrato por fallas en los datos ingresados, ya que es responsabilidad del cliente el ingresar su informaci&oacute;n de la manera indicada.  &bull; Bajo ning&uacute;n concepto se proceder&aacute; con solicitudes de anulaci&oacute;n o reembolso de contratos con fecha posterior al inicio de su vigencia.  &bull; El cliente podr&aacute; solicitar el reembolso siempre y cuando notifique con 72 horas de anticipaci&oacute;n al inicio de la vigencia de su contrato, y solamente en casos de negaci&oacute;n de visa, y por causas de fuerza mayor. En estos casos BLUECARD reembolsar&aacute; el 70% del monto cancelado.  &bull; Toda persona que solicite un reembolso y se le haya enviado el contrato BlueCard deber&aacute; enviar la siguiente documentaci&oacute;n necesaria para tramitar el reintegro de los valores cancelados:   * Contrato original de BlueCard.  * Original de la carta de negativa de visa de la Embajada o Consulado con la firma y sello en la misma. Se debe solicitar a las embajadas que se realice la autentificaci&oacute;n de los documentos con firma y sello a color para evitar la falsificaci&oacute;n de las mismas.  * Copia a color del pasaporte con los sellos de recepci&oacute;n por parte de la Embajada o Consulado a la cual se solicit&oacute; el visado.  POL&Iacute;TICAS DEL SITIO, MODIFICACI&Oacute;N Y SEPARABILIDAD  Por favor, revise nuestras pol&iacute;ticas, condiciones generales, condiciones particulares y precios en este sitio. Estas pol&iacute;ticas tambi&eacute;n gobiernan en su visita a BLUECARD. Nos reservamos el derecho de hacer cambios en el sitio, sus pol&iacute;ticas, y las Condiciones de Uso en cualquier momento. Si cualquiera de estas condiciones se considerar&aacute; inv&aacute;lida, nula o inaplicable por cualquier raz&oacute;n, la condici&oacute;n se considerar&aacute; separable y no afectar&aacute; a la validez y la aplicabilidad de su contrato.  NUESTRA DIRECCI&Oacute;N  Gonzalo Serrano N37-13 y Jos&eacute; Correa. Quito &ndash; Ecuador  Este sitio y condiciones de uso, &copy; 2010 Blue Card. Todos los derechos reservados. </p>
		<p align="justify">
		TÉRMINOS Y CONDICIONES DE PAGO

                Debo y pagaré incondicionalmente sin protesto al Emisor de la tarjeta de crédito el total de los valores expresados en esta

                    Autorización de Orden de Cargo, en el lugar y fecha que se reconvenga. En caso de mora pagaré la tasa máxima autorizada

                    para el emisor de la tarjeta de crédito y las tarifas que esta institución establezca por gestiones de la cartera vencida.





                Eximo al emisor de la tarjeta de crédito, de cualquier responsabilidad por los valores reportados por BLUECARD Por lo

                    cual desde ya renuncio a cualquier reclamación y a iniciar cualquier acción legal en contra de las indicadas Instituciones, las

                    mismas que no requerirán de otro instrumento o documento para procesar en mi tarjeta de crédito, los valores generados,

                    los mismos que desde ya los acepto y reconozco como obligación.



                Me comprometo expresamente a enviar comunicación escrita con 60 días de anticipación, tanto a BLUECARD como a la oficina

                    Matriz del Emisor, en caso de revocación de la presente autorización, de lo contrario se entenderá vigente la orden de cargo

                    expresada por los servicios antes determinados.



                De igual manera autorizo que en caso de pérdida, hurto, robo o cualquier circunstancia por el que fuera cambiado el número

                    de la tarjeta de crédito antes singularizada, se cuenta con el nuevo número que se me asigne para efectuar todos los pagos

                    de manera incondicional dentro del período correspondiente, de tal manera que el cambio del número indicado no sea

                    causa para no cancelar los valores que adeude.





                Adicionalmente, declaro libre y voluntariamente que el Plan de Asistencia en Viajes solicitado a la compañía BLUECARD S.A,

                    ampara bienes/servicios de procedencia lícita y que los mismos no están ligados con actividades de narcotráfico, lavado de activos o

                    financiamiento del terrorismo; igualmente declaro que los fondos con los cuales se pagará la prima de este seguro tienen

                    origen lícito y no guarda ninguna relación con las actividades mencionadas anteriormente.



                Declaro que la información contenida en este formulario es verdadera, completa y proporciona la información de modo

                    confiable y actualizado; además declaro conocer y aceptar que es mi obligación actualizar anualmente mis datos personales,

                    así como el comunicar y documentar de manera inmediata a BLUECARD S.A cualquier cambio en la información que hubiere

                    proporcionado. Durante la vigencia de la relación con la compañía me comprometo a proveer de la documentación e

                    información que me sea solicitada.





                Acepto y certifico que estoy consciente del producto o servicio contratado y de que he leído detalladamente los términos y

                    condiciones a ser aplicados, por lo que los acepto libre y voluntariamente.





                ACUERDO DE RESPONSABILIDAD Y USO DE MEDIOS ELECTRÓNICOS



                El Cliente, a través de la suscripción de este acuerdo, manifiesta estar interesado en utilizar los mecanismos virtuales

                    establecidos, tales como pero no limitados a, correo electrónico, mensajes SMS, aplicativo móvil, redes sociales y el portal

                    web, para lo cual expresa su voluntad de utilizar de manera preferente el medio de notificaciones a través de la Internet,

                    en el buzón personal del correo electrónico proporcionado y otorgando su consentimiento para ello.

                    El Cliente conoce y acepta expresamente que la suscripción de este acuerdo no impide a BLUECARD, cuando las circunstancias

                    así lo requieran,realizar la notificación al Cliente por los otros medios establecidos en la ley o en este acuerdo.



                Responsabilidades del Cliente y/o Comercializador





                    El Cliente y/o Comercializador asume la responsabilidad total del uso, tanto de la clave de usuario, así como de la veracidad de la

                        información para la suscripción, renovación y gestión de contratos de Asistencia en Viajes y acceso a los

                        servicios que BLUECARD ponga a su disposición a través de Internet y aplicativos móviles, por lo que en caso de mal

                        uso, pérdida o disposición arbitraria de su clave personal, el Cliente libera de toda responsabilidad sobre los efectos,

                        consecuencias y/o daños y perjuicios tanto patrimoniales como morales que pueda sufrir.



                    El Cliente y/o Comercializador acepta que todas las transacciones realizadas a través de Internet o aplicativo móvil se garantizarán

                        mediante la clave de usuario del cliente y de ella se derivarán todas las responsabilidades de carácter comercial que

                        hoy se desprenden de la firma autógrafa, según señala la “Ley de Comercio Electrónico, Firmas Electrónicas y

                        Mensajes de Datos”, y en base al principio de libertad tecnológica estipulado en el mismo cuerpo legal, las partes

                        acuerdan que la clave proporcionada por BLUECARD al cliente, surtirá los mismos efectos que una firma electrónica,

                        por lo que, tanto su funcionamiento como su aplicación se entenderán como una completa equivalencia funcional,

                        técnica y jurídica.



                    El Cliente y/o Comercializador declara conocer y aceptar que las contraseñas personales constituyen uno de los componentes básicos

                        de la seguridad del proceso de contratación y utilización de los mecanismos virtuales que BLUECARD pone a

                        disposición de sus afiliados; la validez jurídica de la firma, y la seguridad en el acceso a las evidencias y otros datos

                        personales, y deben por tanto estar especialmente protegidas.



                    El Cliente y/o Comercializador declara conocer que la contraseña como llaves de acceso al sistema, deben ser estrictamente

                        confidenciales y personales, y cualquier incidencia que comprometa su confidencialidad debe ser inmediatamente

                        comunicada a BLUECARD para que sea subsanada en el menor plazo de tiempo posible, en caso de que esto no suceda

                        o el Cliente sea objeto de delitos informáticos producto de su falta de cuidado en el manejo de esta información y

                        sus claves, el Cliente libera de responsabilidad sobre los efectos, consecuencias y/o daños y perjuicios tanto

                        patrimoniales como morales que pueda sufrir.
</p>


		</div>
		<div class="span-12">
		</div>
</textarea>

<textarea id="customPopUp3" style="display:none;">
		<div class="span-19 center"><strong>Preguntas frecuentes</strong></div>
		<div class="center">
		<p align="justify">
		<p style="font-weight: bold">¿Qué es PlacetoPay el Boton de Pagos de BlueCard?</p>
		 PlacetoPay es la plataforma de pagos electrónicos que procesa en línea las transacciones generadas (Facturas - Contratos de Viajero) en BAS (BlueAssist System) con las formas de pago habilitadas para tal fin.
		<p style="font-weight: bold">¿Cómo puedo pagar?</p>
		  En el BAS usted podrá realizar su pago utilizando: Tarjetas de Crédito de las siguientes franquicias: Diners, Visa y MasterCard; de todos los bancos en lo que corresponde a pago corriente y en cuanto a diferido, únicamente por el momento las tarjetas emitidas por Banco Pichincha, Loja, BGR y Machala.
		<p style="font-weight: bold">¿Puedo realizar el pago cualquier día y a cualquier hora?</p>
		Sí, en nuestro sistema BAS podrás realizar tus compras en línea los 7 días de la semana, las 24 horas del día a sólo un clic de distancia.
		<p style="font-weight: bold">¿Puedo cambiar la forma de pago?</p>
		 Si aún no has finalizado tu pago, podrás volver al paso inicial y elegir la forma de pago que prefieras. Una vez finalizada la compra no es posible cambiar la forma de pago.
		<p style="font-weight: bold">¿Es seguro ingresar mis datos bancarios en este sitio web?</p>
		   Para proteger tus datos BlueCard delega en PlacetoPay la captura de la información sensible. Nuestra plataforma de pagos cumple con los estándares exigidos por la norma internacional PCI DSS de seguridad en transacciones con tarjeta de crédito. Además tiene certificado de seguridad SSL expedido por GeoTrust una compañía Verisign, el cual garantiza comunicaciones seguras mediante la encriptación de todos los datos hacia y desde el sitio; de esta manera te podrás sentir seguro a la hora de ingresar la información de su tarjeta.



                        Durante el proceso de pago, en el navegador se muestra el nombre de la organización autenticada, la autoridad que lo certifica y la barra de dirección cambia a color verde. Estas características son visibles de inmediato, dan garantía y confianza para completar la transacción en PlacetoPay.



                        PlacetoPay - BlueCard también cuenta con el monitoreo constante de McAfee Secure y la firma de mensajes electrónicos con Certicámara.
		<p style="font-weight: bold">¿Pagar electrónicamente tiene algún valor para mí como comprador?</p>
		 No, los pagos electrónicos realizados a través de PlacetoPay - BlueCard no generan costos adicionales para el comprador.
		 <p style="font-weight: bold">¿Qué debo hacer si mi transacción no concluyó?</p>
		    En primera instancia deberás revisar si llegó un mail de confirmación del pago en tu cuenta de correo electrónico (la inscrita en el momento de realizar el pago), en caso de no haberlo recibido, deberás contactar a soporte@placetopay.com para confirmar el estado de la transacción.
		<p style="font-weight: bold">¿Qué debo hacer si no recibí el comprobante de pago?</p>
		 Por cada transacción aprobada a través de PlacetoPay - BlueCard, recibirás un comprobante del pago con la

                        referencia de compra en la dirección de correo electrónico que indicaste al momento de pagar.

                        Si no lo recibes, podrás contactar a soporte@placetopay.com, para solicitar el reenvío del

                        comprobante a la misma dirección de correo electrónico registrada al momento de pagar.



                        Para mayor información comuníquese con nosotros al 593-2-3332253 o a través de nuestro correo electrónico: info@bluecard.com.ec
		</p>
		</div>
</textarea>

<?php $_from = $this->_tpl_vars['marketingFiles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['plan']):
?>
    <div id="digitalFilesPopUp<?php echo $this->_tpl_vars['plan']['serial_opt']; ?>
" style="display:none;">
        <div class="span-14 last line" >
            <div class="span-14">
                <!--Links para archivos de material digital-->
                <?php if ($this->_tpl_vars['plan']['file_type'] == 'material'): ?>
                    <div class="img-with-text">
                        <div class="link-content">
                            <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/material/CUADRIPTICO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                <img class="img" src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                <p class="file_text">Cuadr&iacute;ptico</p>
                            </a>
                        </div>

                        <div class="link-content">
                            <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/material/DIPTICO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                <p class="file_text">D&iacute;ptico</p>
                            </a>
                        </div>
                    </div>

                <!--Links para archivos de precios por producto-->
                <?php elseif ($this->_tpl_vars['plan']['file_type'] == 'rate'): ?>
                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/DEPORTES_COMPETENCIAS.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Deportes y Competencias</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/EJECUTIVO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Ejecutivo</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/ESTUDIANTIL.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/ESTUDIANTIL_ELITE.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil Elite</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/ESTUDIANTIL_ELITE_PREMIUM.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil Elite Premium</p>
                                </a>
                            </div>
                    </div>

                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/EUROPA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Europa</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/PLATINO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Platino</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/TURISTA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Turista</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/VIAJERO360.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Viajero 360</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/USA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Usa</p>
                                </a>
                            </div>
                       </div>

                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/ESTUDIANTIL_ESPANA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil España</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/rates/TURISTA_NACIONAL.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Turista Nacional</p>
                                </a>
                            </div>
                    </div>

                <!--Links para archivos de beneficios por producto-->
                <?php elseif ($this->_tpl_vars['plan']['file_type'] == 'benefit'): ?>
                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/DEPORTES_COMPETENCIAS.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Deportes y Competencias</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/EJECUTIVO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Ejecutivo</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/ESTUDIANTIL.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/ESTUDIANTIL_ELITE.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil Elite</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/ESTUDIANTIL_ELITE_PREMIUM.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil Elite Premium</p>
                                </a>
                            </div>
                    </div>

                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/EUROPA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Europa</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/PLATINO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Platino</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/TURISTA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Turista</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/VIAJERO_360.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Viajero 360</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/USA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Usa</p>
                                </a>
                            </div>
                    </div>

                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/TURISTA_NACIONAL.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Turista Nacional</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefits/ESTUDIANTIL_ESPANA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Estudiantil España</p>
                                </a>
                            </div>
                    </div>

                <!--Links para archivos de beneficios por producto ingles-->
                <?php elseif ($this->_tpl_vars['plan']['file_type'] == 'benefiten'): ?>
                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/ELITE_PREMIUM_STUDENT.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Elite Premium Student</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/ELITE_STUDENT.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Elite Student</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/EUROPE.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Europe</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/EXECUTIVE.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Executive</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/PLATINUM.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Platinum</p>
                                </a>
                            </div>
                    </div>

                    <div class="img-with-text">
                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/SPORTS_AND_COMPETITIONS.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Sports and Competitions</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/STUDENT.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Student</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/TOURIST.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Tourist</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/TRAVELER_360.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Traveler 360</p>
                                </a>
                            </div>

                            <div class="link-content">
                                <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/benefitsen/USA.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                    <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                    <p class="file_text">Usa</p>
                                </a>
                            </div>
                    </div>

                <!--Links para archivos de formularios-->
                <?php elseif ($this->_tpl_vars['plan']['file_type'] == 'form'): ?>
                    <div class="img-with-text">
                        <div class="link-content">
                            <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/forms/FORMULARIO_DECLARACION_DE_SALUD.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                <p class="file_text">Declaración de Salud</p>
                            </a>
                        </div>

                        <div class="link-content">
                            <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/forms/FORMULARIO_DE_REEMBOLSO.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                <p class="file_text">Reembolso de Asistencia</p>
                            </a>
                        </div>

                        <div class="link-content">
                            <a href="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/marketing/forms/FORMULARIO_DE_EMISION.pdf?r=<?php echo $this->_tpl_vars['random_number']; ?>
" target="_blank" style="text-decoration:none">
                                <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/pdf_icon_64x64.png" border="0" title="">
                                <p class="file_text">Emisión de Contrato</p>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; endif; unset($_from); ?>


<?php if ($this->_tpl_vars['activePopUp']): ?>
	<div id="customPopUp">
		<div class="span-19 center">
			<img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/customAdds/<?php echo $this->_tpl_vars['activePopUp']['image']; ?>
" alt="customPopUp" width="800" />
		</div>
		
		<?php if ($this->_tpl_vars['activePopUp']['description']): ?>
		<div class="span-12">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['activePopUp']['description'])) ? $this->_run_mod_handler('htmlall', true, $_tmp) : smarty_modifier_htmlall($_tmp)); ?>

		</div>
		<?php endif; ?>
		
		<div class="span-12">
			<input type="hidden" id="popTitle" value="<?php echo $this->_tpl_vars['activePopUp']['title']; ?>
" />
			<input type="hidden" id="popWebLink" value="<?php echo $this->_tpl_vars['activePopUp']['webLink']; ?>
" />
            <input type="hidden" id="pdfFile" value="<?php echo $this->_tpl_vars['document_root']; ?>
pdfs/customAdds/<?php echo $this->_tpl_vars['activePopUp']['pdfFile']; ?>
"/>
		</div>
	</div>
<?php endif; ?>