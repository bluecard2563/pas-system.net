<?php /* Smarty version 2.6.22, created on 2022-03-29 18:37:18
         compiled from templates/index.es.tpl */ ?>

<form name="frmLogin" id="frmLogin" method="post" action="<?php echo $this->_tpl_vars['document_root']; ?>
index" class="login_form">
	<div class="prepend-2 span-10 line" style="margin-top: 130px;">
		<div class="span-3 right">
			<span class="login-label">Usuario:</span>
		</div>
		<div class="span-6">
			<input type="text" name="txtSystemUser" id="txtSystemUser" class="text-background" title="El campo 'Usuario' es obligatorio." />
		</div>
	</div>
	
	<div class="prepend-2 span-10">
		<div class="span-3 right">
			<span class="login-label">Clave:</span>
		</div>
		<div class="span-6">
			<input type="password" name="txtSystemPassword" id="txtSystemPassword" class="text-background" title="El campo 'Contrase&ntilde;a ' es obligatorio."/>
		</div>
	</div>
	
	<div class="span-12 center" style="margin-bottom: 18px;">
		<label class="lostPass" id="lostPass">Olvid&eacute; mi clave</label>
	</div>

    
    <div class="span-12 center">
		<input type="button" id="btnSubmit" class="login_submit" value="Ingresar" />
		<input type="hidden" name="systemToken" value="<?php echo $this->_tpl_vars['systemToken']; ?>
" id="systemToken" />
		<input type="hidden" name="autoLogin" value="1" id="autoLogin" />
    </div>
              <img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/contract_logos/logoblue_contracts.jpg" style="height:30px;margin-top:30px;margin-left: 34%;">
</form>

<div class="span-12 last login-errors" style="margin-top: 100px;">
	<div class="prepend-1 span-9 append-1 last">
		<?php if ($this->_tpl_vars['error'] == 1): ?>
			<div class="span-9 last error" align="center">
				Contrase&ntilde;a incorrecta, por favor ingrese nuevamente!
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 2): ?>
			<div class="span-9 last error" align="center">
				El usuario no existe, por favor ingrese nuevamente!
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 3): ?>
			 <div class="span-9 last success" align="center">
				Contrase&ntilde;a cambiada exitosamente!
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 4): ?>
			<div class="span-9 last error" align="center">
				Hubo errores en el cambio de contrase&ntilde;a . Por favor vuelva a intentarlo.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 5): ?>
			<div class="span-9 last error" align="center">
				El usuario no esta activo.<br>
				Comun&iacute;quese con el Administrador.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 6): ?>
			<div class="span-9 last success" align="center">
				Bienvenido(a) al PAS! <br><br>
				La actualizaci&oacute;n de datos se realiz&oacute; con &eacute;xito!<br>
				Ingrese nuevamente con sus nuevos datos.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 7): ?>
			<div class="span-9 last error" align="center">
				Lo sentimos, hubo errores en la actualizaci&oacute;n de datos. Por favor vuelva a intentarlo o comun&iacute;quese con el adminsitrador.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 8): ?>
			<div class="span-9 last error" align="center">
				Hubo errores al configurar su primer acceso. Comun&iacute;quese con el administrador.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 9): ?>
			<div class="span-9 last error" align="center">
				Login Invalido, por favor ingrese sus datos para ingresar.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 10): ?>
			<div class="span-9 last success" align="center">
				Bienvenido(a) al PAS! <br><br>
				La actualizaci&oacute;n de datos se realiz&oacute; con &eacute;xito!<br>
				Su nombre de usuario fue modificado, por favor revise su correo para poder continuar.<br>
				Ingrese nuevamente con sus nuevos datos.
			</div>
		<?php elseif ($this->_tpl_vars['error'] == 11): ?>
			<div class="span-9 last success" align="center">
				Bienvenido(a) al PAS! <br><br>
				La actualizaci&oacute;n de datos se realiz&oacute; con &eacute;xito!<br>
				Su nombre de usuario fue modificado, sin emabrgo no se pudo enviar sus datos por correo, por favor contacte al administrador.<br>
				Ingrese nuevamente con sus nuevos datos.
			</div>
		<?php endif; ?>
	</div>
	
	<div class="span-12 last">
		<ul id="alerts" class="alerts"></ul>
	</div>
</div>