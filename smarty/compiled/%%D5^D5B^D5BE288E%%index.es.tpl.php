<?php /* Smarty version 2.6.22, created on 2020-01-22 22:06:59
         compiled from containers/index.es.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1" />
<link rel="shortcut icon" href="<?php echo $this->_tpl_vars['document_root']; ?>
img/favicon.ico" ></link>
<title><?php echo $this->_tpl_vars['global_system_name']; ?>
</title>

<!--GLOBAL CSS FILES-->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/screen.css" type="text/css" media="screen, projection" />
<!--[if IE 6]>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/src/ie.css" />
<![endif]-->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jquery-ui/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/system_styles.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/ie_system_styles.css" type="text/css" />
<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/ipad.css" type="text/css" />
<!--END OF THE GLOBAL CSS FILES-->
<?php if ($this->_tpl_vars['css_file']): ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/<?php echo $this->_tpl_vars['css_file']; ?>
" />
<?php endif; ?>
 
<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery-validation.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/global_vars.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/global_functions.js"></script>
<?php if ($this->_tpl_vars['js_file']): ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/<?php echo $this->_tpl_vars['js_file']; ?>
"></script>
<?php endif; ?>
</head>

<body class="inner_body container">
    <div id="login-body" class="span-13 last">
        <?php echo $this->_tpl_vars['buffer']; ?>

    </div>
    <div id="dialogChangePass" title="Olvido de Clave">
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "templates/fChangePass.es.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
</body>
</html>