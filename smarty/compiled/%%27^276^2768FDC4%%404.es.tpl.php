<?php /* Smarty version 2.6.22, created on 2020-01-22 22:07:22
         compiled from templates/404.es.tpl */ ?>
<?php $this->assign('title', 'PAGINA NO ENCONTRADA'); ?>
<br /><br />
<h1 align="center">P&aacute;gina no encontrada</h1>

<h3 align="center">
	Lo sentimos, no se ha encontrado la p&aacute;gina a la que trata de ingresar.
</h3>

<h5 align="center">
	<a href="<?php echo $this->_tpl_vars['document_root']; ?>
main">VOLVER</a>
</h5>