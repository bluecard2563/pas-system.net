<?php /* Smarty version 2.6.22, created on 2021-01-29 12:32:24
         compiled from containers/default.es.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'get_menu', 'containers/default.es.tpl', 126, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1" />
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<link rel="shortcut icon" href="<?php echo $this->_tpl_vars['document_root']; ?>
img/favicon.ico" />

		<!--GLOBAL CSS FILES-->
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/screen.css" type="text/css" media="screen, projection" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/stylish-select.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jScrollPane.css" />
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jquery.autocomplete.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/superfish-admin.css" media="screen" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/superfish-vertical.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jquery-ui/jquery-ui-datepicker.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/menu-style.css" />
		<link rel='stylesheet' type='text/css' href='<?php echo $this->_tpl_vars['document_root']; ?>
css/fullcalendar.css' />
		<link rel='stylesheet' type='text/css' href='<?php echo $this->_tpl_vars['document_root']; ?>
css/fullcalendar-theme.css' />
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/jquery.treeview.css" />
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/system_styles.css" type="text/css" />
		<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/ipad.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/ie_system_styles.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/menu1.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/paging.css" media="screen" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/dataTables/demo_table.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/dataTables/TableTools.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/dataTables/TableTools_JUI.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/dataTables/demo_table_jui.css" />
		<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/src/ie.css" />
		<![endif]-->
		<?php if ($this->_tpl_vars['css_file']): ?>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/<?php echo $this->_tpl_vars['css_file']; ?>
?r=<?php echo $this->_tpl_vars['random_number']; ?>
" />
		<?php endif; ?>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['document_root']; ?>
css/font-awesome/css/font-awesome.min.css?r=<?php echo $this->_tpl_vars['random_number']; ?>
">

		<!--END CSS FILES-->

		<!--GLOBAL JS FILES-->
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery.js" ></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery-validation.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jScrollPane.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery-ui.js"></script>
		<script type='text/javascript' src='<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery-datepicker.js'></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery-form.js"></script>
		<script type='text/javascript' src='<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery.autocomplete.js'></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jqueryLibrary/jquery.wizard.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/global_vars.js?r=<?php echo $this->_tpl_vars['random_number']; ?>
"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/global_functions.js?r=<?php echo $this->_tpl_vars['random_number']; ?>
"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/modules/estimator/estimator-commons.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/system.js"></script>
		<script type='text/javascript' src='<?php echo $this->_tpl_vars['document_root']; ?>
js/fullcalendar.js'></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/mbMenu.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/jquery.hoverIntent.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/paging.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/ajaxupload.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/json_parse.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/dataTables/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/dataTables/plugins.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/dataTables/TableTools.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/dataTables/ZeroClipboard.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/ckeditor/ckeditor_jquery_adapter.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>

		<?php if ($this->_tpl_vars['js_file']): ?>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['document_root']; ?>
js/<?php echo $this->_tpl_vars['js_file']; ?>
?r=<?php echo $this->_tpl_vars['random_number']; ?>
"></script>
		<?php endif; ?>
		<!--END JS FILES-->
		<title><?php echo $this->_tpl_vars['title']; ?>
</title>
	</head>


	<body class="inner_body container">
				<div class="span-24 last banner">			
			<div class="prepend-1 span-12">
				<img class="link" src="<?php echo $this->_tpl_vars['document_root']; ?>
img/banner/pas-logo.png" onclick="location.href='<?php echo $this->_tpl_vars['document_root']; ?>
main'" />
			</div>
			
			<div class="prepend-1 span-5">
				<div class="user_info">
					<div class="span-5 last line center">
						<label class="info_title">Bienvenido!</label>
					</div>
					
					<div class="span-5 last center">
						<label class="user_name"><?php echo $this->_tpl_vars['nombreUsuario']; ?>
</label>
					</div>

					<div class="span-5 last line center">
						<?php $this->assign('weekday', $this->_tpl_vars['date']['weekday']); ?>
					<?php $this->assign('month', $this->_tpl_vars['date']['month']); ?>
						<?php echo $this->_tpl_vars['global_weekDaysNumb'][$this->_tpl_vars['weekday']]; ?>
, <?php echo $this->_tpl_vars['date']['day']; ?>
 de <?php echo $this->_tpl_vars['global_weekMonths'][$this->_tpl_vars['month']]; ?>
 del <?php echo $this->_tpl_vars['date']['year']; ?>

						<div class="span-5" id="user_alerts_container" style="display: none;">
							  <a href="<?php echo $this->_tpl_vars['document_root']; ?>
myAlerts" style="color:white; text-decoration: none; cursor: pointer;"><label id="user_alerts_number"></label>&nbsp;<img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/alert.png"/></a>
						  </div>
					</div>
				</div>
			</div>
			
			<div class="span-4">
				<input type="button" class="myaccount" value="Mi Cuenta" onclick="location.href='<?php echo $this->_tpl_vars['document_root']; ?>
myAccount'"  title="Mi Cuenta"/>
				<input type="button" class="logout" value="Salir del Sistema" onclick="if(confirm('\u00BF Desea cerrar la sesi\u00F3n ?'))location.href='<?php echo $this->_tpl_vars['document_root']; ?>
index'" title="Cerrar Sesi&oacute;n" />
			</div>
		</div>

		<?php echo smarty_function_get_menu(array('menu' => $this->_tpl_vars['menu']), $this);?>

		
		<div class="span-24 last">
			<div class="span-24 last contents">
				<?php echo $this->_tpl_vars['buffer']; ?>

			</div>
		</div>

		<div class="span-24 last footer">
			<a href="http://www.planet-assist.net" target="_blank"><img src="<?php echo $this->_tpl_vars['document_root']; ?>
img/developedIcon.png" alt="Planet Assist Inc." border="0" /> </a> 
		</div>

		<div id="loader">
			<div id="ajax_loader"></div>
			<div class="ajax_loader_text">Cargando...</div>
		</div>

		<div id="JSWarning">
			<div id="ajax_loader"></div>
			<div class="disableJavascript"><div class="disableJavascriptIco"></div>Por favor active su Javascript </div>
		</div>
	</body>
</html>