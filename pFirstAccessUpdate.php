<?php
/* 
File: pMyAccount.php
Author: Esteban Angulo
Creation Date: 17/02/2010
Last Modified:
Modified By:
 */

$user = new User($db, $_SESSION['serial_usr']);
$user -> getData();
$user -> setDocument_usr(specialchars($_POST['txtDocument']));
$user -> setFirstname_usr(specialchars($_POST['txtFirstName']));
$user -> setLastname_usr(specialchars($_POST['txtLastName']));
$user -> setAddress_usr(specialchars($_POST['txtAddress']));
$user -> setPhone_usr(specialchars($_POST['txtPhone']));
$user -> setCellphone_usr(specialchars($_POST['txtCellphone']));
$user -> setEmail_usr(specialchars($_POST['txtMail']));
$user -> setPassword_usr(md5($_POST['txtPassword']));
$user -> setBirthdate_usr($_POST['txtBirthdate']);
$user -> setFirstAccess_usr('1');

$outcome = 6;//UPDATED OK
//IF THE USERNAME IS NUMERIC, REGEN A USERNAME AND NOTIFY:
if(is_numeric(substr($user  ->  getUsername_usr(), 0, 8))){
	
	$data = array(	'db' => $db,
					'serial' => $user->getSerial_usr(),
					'firstname' => $user->getFirstname_usr(),
					'lastname' => $user->getLastname_usr());
	$newUsername = GlobalFunctions::generateUser($data);
	$user -> setUsername_usr($newUsername);
	
	$misc['user'] = $user -> getFirstname_usr() . ' ' . $user -> getLastname_usr();
	$misc['username'] = $newUsername;
	$misc['pswd'] = $_POST['txtPassword'];
	$misc['email'] = $_POST['txtMail'];

	if(GlobalFunctions::sendMail($misc, 'username_updated')){
		User::updateUserName($db, $_SESSION['serial_usr'], $newUsername);

		$outcome = 10;//UPDATED OK, USERNAME CHANGED
	}else{
		$outcome = 11;//UPDATED OK, USERNAME CHANGED, EMAIL NOT SENT
	}
}

if($user  ->  update()){
	http_redirect('index/' . $outcome);
}else{
	http_redirect('index/7');
}
?>