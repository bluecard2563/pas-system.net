<?php
/*
    Document   : myAlerts.php
    Created on : Apr 21, 2010, 10:04:20 AM
    Author     : H3Dur4k
    Description:
        Show all the users pending alerts.
*/
	Request::setInteger('0:error');
	$pending_alerts = Alert::getUserPendingAlerts($db, $_SESSION['serial_usr']);
	$smarty -> register('pending_alerts,error');
	$smarty -> display();
?>