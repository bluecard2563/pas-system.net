<?php
/*
File: getFreeSalesObsAjax
Author: Santiago Borja Lopez
Creation Date: 16/02/2010 10:00

 * BUILDS THE DIALOG DATA OF A FREE SALES ALERT
 */
	Request::setString('alertId');

	$alert = new Alert($db, $alertId);
	$alert -> getData();
	
	$sale = new Sales($db, $alert -> getRemote_id());
	$sale -> getData();
	
	echo "	<div class='prepend-1 span-16'>
	    		<div class='span-4'><b>N&uacute;mero de tarjeta:</b></div>
	    		<div class='span-12 last'>{$sale -> getCardNumber_sal()}</div>
    		</div>
		    <div class='prepend-1 span-16'>&nbsp;</div>
		    <div class='prepend-1 span-16'>
		    	<div class='span-4'><b>Observaciones:</b></div>
		    	<div class='span-6'>{$sale -> getObservations_sal()}</div>
		    </div>";
?>