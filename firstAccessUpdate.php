<?php
/*
File: firstAccessUpdate.php
Author: Patricio Astudillo
Creation Date: 20/08/2010
Last Modified:
Modified By:
 */
Request::setString('0:error');

$user=new User($db, $_SESSION['serial_usr']);
$serial_usr=$_SESSION['serial_usr'];
if($user->getData()){
	if($user->getFirstAccess_usr()=='0'){
		$user=get_object_vars($user);
		unset($user['db']);

		$city=new City($db, $user['serial_cit']);
		$city->getData();

		$country=new Country($db, $city->getSerial_cou());
		$country->getData();
		$country=$country->getName_cou();
		$city=$city->getName_cit();

		$today=date('d/m/Y');

		$smarty->register('user,city,country,today,serial_usr,error');
		$smarty->assign('container','simple_container');
		$smarty->display();
	}else{
		http_redirect('main/firstAccessUpdate/1');
	}
}else{
	http_redirect('index/8');
}
?>