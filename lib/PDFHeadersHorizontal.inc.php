<?php
	set_time_limit(36000);
	ini_set('memory_limit','1024M');
	global $global_system_name; 
	$pdf =& new Cezpdf('A4','landscape');
	$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');

	$pdf -> ezSetMargins(80,80,50,50);
	global $date;

	//BEGIN HEADERS
	$date = utf8_decode(html_entity_decode($_SESSION['cityForReports'].', '.$global_weekDaysNumb[$date['weekday']].' '.$date['day'].' de '.$global_weekMonths[$date['month']].' de '.$date['year']));
	$all = $pdf->openObject();
	$pdf->saveState();
	
	$pdf->addJpegFromFile($global_system_logo,40,530,150);
	$pdf->addText(320, 550, 16, '<b>SISTEMA '.$global_system_name.'</b>');
	$pdf->addText(320, 530, 12, $date);
	$pdf->setStrokeColor(0,0,0,1);
	$pdf->line(40,520,800,520);//the top line

	$pdf->line(40,40,800,40);//the bottom line
	$pdf->addText(50,30,10,'SISTEMA '.$global_system_name);//bottom text

	$pdf->ezStartPageNumbers(780,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
	$pdf->restoreState();
	$pdf->closeObject();
	$pdf->addObject($all,'all');
	//END HEADERS

?>
