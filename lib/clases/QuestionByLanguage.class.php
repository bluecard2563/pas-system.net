<?php
/*
File: QuestionByLanguage.class.php
Author: Santiago Benítez
Creation Date: 21/01/2010 08:40
Last Modified: 22/01/2010 14:33
Modified By: Santiago Benítez
*/

class QuestionByLanguage{				
	var $db;
	var $serial_qbl;
	var $serial_que;
	var $serial_lang;
	var $text_qbl;
	
	function __construct($db, $serial_qbl=NULL, $serial_que=NULL, $serial_lang=NULL, $text_qbl=NULL ){
		$this -> db = $db;
		$this -> serial_qbl = $serial_qbl;
		$this -> serial_que = $serial_que;
		$this -> serial_lang = $serial_lang;
		$this -> text_qbl = $text_qbl;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_qbl
    ***********************************************/
	function getData(){
		if($this->serial_qbl!=NULL){
			$sql = 	"SELECT que.serial_qbl, que.serial_que, que.serial_lang, que.text_qbl
					FROM questions_by_language que
					WHERE que.serial_qbl='".$this->serial_qbl."'";
			//die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_qbl =$result->fields[0];
				$this -> serial_que =$result->fields[1];
				$this -> serial_lang = $result->fields[2];
				$this -> text_qbl = $result->fields[3];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * funcion questionByLanguangeExists
    * verifies if a QuestionByLanguange exists or not
    ***********************************************/
	function questionByLanguangeExists($text_qbl, $serial_qbl=NULL){
		$sql = 	"SELECT serial_qbl
				 FROM questions_by_language 
				 WHERE LOWER(text_qbl)= _utf8'".utf8_encode($text_qbl)."' collate utf8_bin";
		if($serial_qbl!=NULL){
			$sql.=" AND serial_qbl <> ".$serial_qbl;
		}
		//die($sql);
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	
	/** 
	@Name: getQuestionByLanguangesAutocompleter
	@Description: Returns an array of questionByLanguanges.
	@Params: QuestionByLanguange name or pattern.
	@Returns: questionByLanguanges array
	**/
	function getQuestionsByLanguangeAutocompleter($text_rst){
		$sql = "SELECT serial_que, serial_qbl, text_qbl
				FROM questions_by_language
				WHERE LOWER(text_qbl) LIKE _utf8'%".utf8_encode($text_rst)."%' collate utf8_bin AND serial_lang='".$this->serial_lang."'
				LIMIT 10";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/** 
	@Name: getQuestionsBySerial
	@Description: Returns an array of questionByLanguanges.
	@Params: QuestionByLanguange name or pattern.
	@Returns: questionByLanguanges array
	**/
	function getQuestionsBySerial(){
		$sql = "SELECT qbl.serial_qbl as 'serial', lang.name_lang as 'language', qbl.text_qbl as 'text',qbl.serial_qbl as 'update'
				FROM questions_by_language qbl, language lang
				WHERE lang.serial_lang = qbl.serial_lang AND qbl.serial_que='".$this->serial_que."'";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}

        /**
	@Name: getQuestionsByLanguage
	@Description: Returns an array of questionByLanguanges.
	@Params: Language serial.
	@Returns: questionByLanguanges array
	**/
	function getQuestionsByLanguage(){
		$sql = "SELECT  qbl.text_qbl, qbl.serial_qbl
				FROM questions_by_language qbl
				WHERE qbl.serial_lang='".$this->serial_lang."'";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}
	
	


	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO questions_by_language (
				serial_qbl,
				serial_que,
				serial_lang,
				text_qbl)
            VALUES (
                NULL,
				".$this->serial_que.",
				".$this->serial_lang.",
				'".$this->text_qbl."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
	/***********************************************
    * function delete
    * inserts a new register in DB 
    ***********************************************/
    function delete() {
        $sql = "
            DELETE from questions_by_language 
			WHERE serial_qbl = ".$this->serial_qbl;

        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if($result==true)
            return true;
        else
            return false;
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE questions_by_language SET
					text_qbl ='".$this->text_qbl."'
				WHERE serial_qbl = '".$this->serial_qbl."'";
		//die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }
	
	
	
	///GETTERS
	function getSerial_qbl(){
		return $this->serial_qbl;
	}
	function getSerial_que(){
		return $this->serial_que;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}
	function getText_qbl(){
		return $this->text_qbl;
	}
	

	///SETTERS	
	function setSerial_qbl($serial_qbl){
		$this->serial_qbl = $serial_qbl;
	}
	function setSerial_que($serial_que){
		$this->serial_que = $serial_que;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setText_qbl($text_qbl){
		$this->text_qbl = $text_qbl;
	}
}
?>