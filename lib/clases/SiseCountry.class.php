<?php
/*
File: SiseCountry.class.php
Author: Carlos Oberto
Creation Date: 04/09/2017
*/

Class SiseCountry {
    var $db;
	var $serial_sise_country;
    var $desc_sise_country;
	var $cod_uaf_sise_country;
	var $cod_uif_sise_country;
	var $cod_ats_sise_country;
	var $cod_super_nac_sise_country;
	var $cod_tipo_regimen_sise_country;
	var $cod_par_fiscal_sise_country;
    var $cod_pas_sise_country;
    
    function __construct($db, $serial_sise_country = NULL, $desc_sise_country=NULL, $cod_uaf_sise_country = NULL, $cod_uif_sise_country = NULL, $cod_ats_sise_country = NULL, $cod_super_nac_sise_country = NULL, $cod_tipo_regimen_sise_country = NULL, $cod_par_fiscal_sise_country = NULL,$cod_pas_sise_country=NULL){
		$this -> db = $db;
        $this -> serial_sise_country =  $serial_sise_country;
        $this -> desc_sise_country =  $desc_sise_country;
        $this -> cod_uaf_sise_country=  $cod_uaf_sise_country;
        $this -> cod_uif_sise_country =  $cod_uif_sise_country;
        $this -> cod_ats_sise_country =  $cod_ats_sise_country;
        $this -> cod_super_nac_sise_country =  $cod_super_nac_sise_country;
        $this -> cod_tipo_regimen_sise_country =  $cod_tipo_regimen_sise_country;
        $this -> cod_par_fiscal_sise_country =  $cod_par_fiscal_sise_country;
        $this -> cod_pas_sise_country =  $cod_pas_sise_country;
	}
    
    /***********************************************
        * function getData
        * gets all data
        ***********************************************/
	function getData(){
			$sql = 	"SELECT 
                        serial_sise_country,
                        desc_sise_country,
                        cod_uaf_sise_country,
                        cod_uif_sise_country,
                        cod_ats_sise_country,
                        cod_super_nac_sise_country,
                        cod_tipo_regimen_sise_country,
                        cod_par_fiscal_sise_country,
                        cod_pas_sise_country
					FROM sise_country";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;
			if($result->fields[0]){
				$this -> serial_sise_country =  $result->fields[0];
                $this -> desc_sise_country =  $result->fields[1];
                $this -> cod_uaf_sise_country=  $result->fields[2];
                $this -> cod_uif_sise_country =  $result->fields[3];
                $this -> cod_ats_sise_country =  $result->fields[4];
                $this -> cod_super_nac_sise_country =  $result->fields[5];
                $this -> cod_tipo_regimen_sise_country =  $result->fields[6];
                $this -> cod_par_fiscal_sise_country =  $result->fields[7];
                $this -> cod_pas_sise_country =  $result->fields[8];
				return true;
			}	
			else
				return false;

				
	}
    /***********************************************
        * Name: LoadCountries
        * Autor: Carlos Oberto
        * Date: 04/09/2017
        ***********************************************/
    public function LoadCountries($db){
        $sql = 	"SELECT 
                        serial_sise_country,
                        desc_sise_country,
                        cod_uaf_sise_country,
                        cod_uif_sise_country,
                        cod_ats_sise_country,
                        cod_super_nac_sise_country,
                        cod_tipo_regimen_sise_country,
                        cod_par_fiscal_sise_country,
                        cod_pas_sise_country
					FROM sise_country
                    ORDER BY desc_sise_country";
        //debug::print_r($sql); die();
        $result = $db -> GetAll($sql);
        if($result){
			return $result;
		}else{
			return false;
		}
    }
    /***********************************************
        * Name: LoadPro
        * Autor: Carlos Oberto
        * Date: 05/09/2017
        ***********************************************/
    public function LoadProvinces($db, $serialCou){
//        $sql = 	"select * from sise_departamento d
//                        join sise_country c on c.serial_sise_country=d.serial_sise_country
//                        where serial_sise_departamento = '".$serial_departamento."'";
        $sql = 	"select * from sise_departamento d
                         join sise_country c on c.serial_sise_country=d.serial_sise_country
                         where d.serial_sise_country= '".$serialCou."'
                         order by d.desc_sise_departamento";
        $result = $db -> GetAll($sql);
//        debug::print_r($sql); die();
        if($result){
			return $result;
		}else{
			return false;
		}
    }
    /***********************************************
        * Name: LoadMuni
        * Autor: Carlos Oberto
        * Date: 05/09/2017
        ***********************************************/
    public function LoadMuni($db, $serialCou){
//        $sql = 	"select * 
//                    from sise_municipio m
//                    join sise_departamento d on d.serial_sise_departamento = m.cod_sise_departamento
//                    join sise_country c on c.serial_sise_country = d.serial_sise_country
//                    where serial_sise_municipio = '".$serial_muni."'";
        $sql = 	"select * 
                    from sise_municipio m
                    join sise_departamento d on d.serial_sise_departamento = m.cod_sise_departamento
                    join sise_country c on c.serial_sise_country = d.serial_sise_country
                    where c.serial_sise_country = '".$serialCou."' 
                   order by m.desc_sise_municipio ";
        $result = $db -> GetAll($sql);
//        debug::print_r($sql); die();
        if($result){
			return $result;
		}else{
			return false;
		}
    }
    
}

?>