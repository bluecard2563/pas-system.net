<?php
/*
File: Question.class.php
Author: Santiago Benítez
Creation Date: 12/01/2010 10:50
Last Modified: 20/01/2010 09:34
Modified By: Santiago Benítez
*/

class Question{				
	var $db;
	var $serial_que;
	var $status_que;
	var $type_que;
	
	function __construct($db, $serial_que=NULL, $status_que=NULL, $type_que=NULL ){
		$this -> db = $db;
		$this -> serial_que = $serial_que;
		$this -> status_que = $status_que;
		$this -> type_que = $type_que;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_que
    ***********************************************/
	function getData(){
		if($this->serial_que!=NULL){
			$sql = 	"SELECT que.serial_que, que.status_que, que.type_que
					FROM questions que
					WHERE que.serial_que='".$this->serial_que."'";
			//die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_que =$result->fields[0];
				$this -> status_que = $result->fields[1];
				$this -> type_que = $result->fields[2];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	
	
	/***********************************************
    * funcion getQuestions
    * gets all the questions from the system
    ***********************************************/
	function getQuestions(){
		$lista=NULL;
		$sql = 	"SELECT que.serial_que, que.status_que, qbl.text_qbl, que.type_que
				FROM questions que, questions_by_language qbl 
				WHERE que.serial_que=qbl.serial_que";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}

	/** 
	@Name: getQuestionsAutocompleter
	@Description: Returns an array of questions.
	@Params: Question name or pattern.
	@Returns: questions array
	**/
	function getQuestionsAutocompleter($text_rst){
		$sql = "SELECT serial_que, serial_qbl, text_qbl
				FROM questions_by_language
				WHERE LOWER(text_qbl) LIKE _utf8'%".utf8_encode($text_rst)."%' collate utf8_bin
				LIMIT 10";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/***********************************************************************************
    * funcion getActiveQuestions
    * gets all the questions wich type is Active of the system with an specific language
    ************************************************************************************/
	public static function getActiveQuestions($db, $serial_lang, $serial_cus = NULL){
		$sql = 	"SELECT que.serial_que, qbl.serial_qbl, qbl.text_qbl, que.type_que, ans.serial_ans, ans.serial_cus, ans.answer_ans, ans.yesno_ans 
			FROM questions que 
			JOIN questions_by_language qbl
			ON que.serial_que=qbl.serial_que
			LEFT JOIN answers ans ON qbl.serial_que=ans.serial_que AND ans.serial_cus='".$serial_cus."' 
			WHERE  que.status_que='ACTIVE' AND qbl.serial_lang='".$serial_lang."'" ;
		
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr = array();
			$cont = 0;
			
			do{
				$arr[$cont] = $result -> GetRowAssoc(false);
				$result -> MoveNext();
				$cont ++;
			}while($cont < $num);
		}
		return $arr;
	}
	
	/***********************************************
    * function getMaxSerial
    * gets all the max serial from db
    ***********************************************/
	function getMaxSerial($serial_lang){
		$sql = 	"SELECT MAX(qbl.serial_qbl) as serial
				 FROM questions que, questions_by_language qbl
				 WHERE que.serial_que=qbl.serial_que AND que.status_que='ACTIVE' AND  qbl.serial_lang='".$serial_lang."'";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		return $arr[0]['serial'];
	}
	
	/***********************************************
    * function getLastSerial
    * gets all the last serial from db
    ***********************************************/
	function getLastSerial(){
		$sql = 	"SELECT MAX(serial_que) as serial
				 FROM questions";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		return $arr[0]['serial'];
	}
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO questions (
				serial_que,
				status_que,
				type_que)
            VALUES (
                NULL,
				'ACTIVE',
				'".$this->type_que."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE questions SET
					status_que ='".$this->status_que."',
					type_que ='".$this->type_que."'
				WHERE serial_que = '".$this->serial_que."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }
	
	/***********************************************
    * funcion getStatusValues
    * Returns the current values for the status field.
    ***********************************************/	
	function getStatusValues(){
		$sql = "SHOW COLUMNS FROM questions LIKE 'status_que'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
    * funcion getTypeValues
    * Returns the current values for the type field.
    ***********************************************/	
	function getTypeValues(){
		$sql = "SHOW COLUMNS FROM questions LIKE 'type_que'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	///GETTERS
	function getSerial_que(){
		return $this->serial_que;
	}
	function getStatus_que(){
		return $this->status_que;
	}
	function getType_que(){
		return $this->type_que;
	}
	

	///SETTERS	
	function setSerial_que($serial_que){
		$this->serial_que = $serial_que;
	}
	function setStatus_que($status_que){
		$this->status_que = $status_que;
	}
	function setType_que($type_que){
		$this->type_que = $type_que;
	}
}
?>