<?php
/**
 * File:  PendingDocumentsByFile.class.php
 * Author: Patricio Astudillo
 * Creation Date: 01-ago-2011, 15:04:35
 * Description:
 * Last Modified: 01-ago-2011, 15:04:35
 * Modified by:
 */

class PendingDocumentsByFile{
	var $db;
	var $serial_pdf;
	var $requester_usr;
	var $receptor_usr;
	var $serial_spv;
	var $serial_cus;
	var $serial_fle;
	var $name_pdf;
	var $creation_date_pdf;
	var $reception_date_pdf;
	var $status_pdf;
	
	function __construct($db, $serial_pdf = NULL, $requester_usr = NULL, $receptor_usr = NULL, 
						$serial_spv = NULL, $serial_cus = NULL, $serial_fle = NULL, $name_pdf = NULL, 
						$creation_date_pdf = NULL, $reception_date_pdf = NULL, $status_pdf = NULL) {
		$this->db = $db;
		$this->serial_pdf = $serial_pdf;
		$this->requester_usr = $requester_usr;
		$this->receptor_usr = $receptor_usr;
		$this->serial_spv = $serial_spv;
		$this->serial_cus = $serial_cus;
		$this->serial_fle = $serial_fle;
		$this->name_pdf = $name_pdf;
		$this->creation_date_pdf = $creation_date_pdf;
		$this->reception_date_pdf = $reception_date_pdf;
		$this->status_pdf = $status_pdf;
	}
	
	public function insert(){
		$sql = "INSERT INTO pending_documents_by_file (
						serial_pdf, 
						requester_usr, 
						receptor_usr, 
						serial_spv, 
						serial_cus, 
						serial_fle, 
						name_pdf, 
						creation_date_pdf, 
						reception_date_pdf, 
						status_pdf
				) VALUES (
						NULL,
						'{$this -> requester_usr}',";
		$sql.=($this -> receptor_usr)?"'{$this -> receptor_usr}',":"NULL,";						
		$sql.=($this -> serial_spv)?"'{$this -> serial_spv}',":"NULL,";
		$sql.=($this -> serial_cus)?"'{$this -> serial_cus}',":"NULL,";
		$sql.="	'{$this -> serial_fle}',
				'{$this -> name_pdf}',
				NOW(),
				NULL, 
				'PENDING'
				)";
				//echo $sql;
		$result = $this -> db -> Execute($sql);
		
		if($result){
			return $this->db->insert_ID();
		}else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
		}
	}
	
	/**
	 * @name getPendingDocumentsByFile
	 * @param type $db
	 * @param type $serial_fle
	 * @param type $serial_spv
	 * @param type $serial_cus 
	 * @return array Group of pending documents
	 */
	public static function getPendingDocumentsByFile($db, $serial_fle, $serial_spv = NULL, $serial_cus = NULL){
		$sql = "SELECT serial_pdf,serial_spv,serial_cus,name_pdf, creation_date_pdf, status_pdf , CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'requester_user'
				FROM pending_documents_by_file pdf
				JOIN user u ON u.serial_usr = pdf.requester_usr
				WHERE serial_fle = $serial_fle";
		
		if($serial_spv) $sql .= " AND pdf.serial_spv = $serial_spv";
		if($serial_cus) $sql .= " AND pdf.serial_cus = $serial_cus";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			if(!is_array($result)){
				ErrorLog::log($db, 'getPendingDocumentsByFile FAILED - '.get_class($this), $sql);
			}			
        	return false;
		}
	}
	
	/**
	 * @name receiveDocumentForFile
	 * @param type $db
	 * @param type $serial_pdf
	 * @param type $receptor_usr
	 * @return boolean TRUE if Ok, FALSE otherwise
	 */
	public static function receiveDocumentForFile($db, $serial_pdf, $receptor_usr,$multiple = FALSE){
		$sql = "UPDATE pending_documents_by_file SET 
					receptor_usr = $receptor_usr,
					reception_date_pdf = CURRENT_TIMESTAMP(),
					status_pdf = 'RECEIVED'
		WHERE serial_pdf ";
		if($multiple){
			$sql.=" IN($serial_pdf)";
		}else{
			$sql.=" = $serial_pdf";
		}
		
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'SERVE DOCUMENT FAILED - '.get_class($this), $sql);
        	return false;
		}
	}
	
	public static function getCountriesWithPendingDocuments($db){
		$sql = "SELECT DISTINCT cou.serial_cou, cou.name_cou
				FROM pending_documents_by_file pdf
				JOIN file f ON f.serial_fle = pdf.serial_fle AND pdf.status_pdf = 'PENDING'
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN country cou ON cou.serial_cou = pxc.serial_cou
				ORDER BY name_cou";
		
		$result = $db ->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getPendingDocumentsByCountry($db, $serial_cou = NULL){
		if($serial_cou != '') $country_filter = " AND cou.serial_cou = $serial_cou";
		$sql = "SELECT s.card_number_sal, f.serial_fle, CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
						 f.diagnosis_fle, f.cause_fle, DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal', 
						 DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal', 
						 DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') AS 'creation_date_fle', 
						 DATEDIFF(NOW(), f.creation_date_fle) AS 'days_elapsed',
						 GROUP_CONCAT(DISTINCT CAST(pdf.name_pdf AS CHAR) ORDER BY pdf.name_pdf SEPARATOR ', ') AS 'pending_documents',
						IF(spv.serial_spv IS NULL, 'PAX', spv.name_spv) AS 'sender_name'
				FROM pending_documents_by_file pdf
				JOIN file f ON f.serial_fle = pdf.serial_fle AND pdf.status_pdf = 'PENDING'
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN country cou ON cou.serial_cou = pxc.serial_cou $country_filter
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				LEFT JOIN service_provider spv ON spv.serial_spv = pdf.serial_spv
				GROUP BY f.serial_fle
				ORDER BY f.serial_fle";
		
		$result = $db ->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public function getSerial_pdf() {
		return $this->serial_pdf;
	}

	public function setSerial_pdf($serial_pdf) {
		$this->serial_pdf = $serial_pdf;
	}

	public function getRequester_usr() {
		return $this->requester_usr;
	}

	public function setRequester_usr($requester_usr) {
		$this->requester_usr = $requester_usr;
	}

	public function getReceptor_usr() {
		return $this->receptor_usr;
	}

	public function setReceptor_usr($receptor_usr) {
		$this->receptor_usr = $receptor_usr;
	}

	public function getSerial_spv() {
		return $this->serial_spv;
	}

	public function setSerial_spv($serial_spv) {
		$this->serial_spv = $serial_spv;
	}

	public function getSerial_cus() {
		return $this->serial_cus;
	}

	public function setSerial_cus($serial_cus) {
		$this->serial_cus = $serial_cus;
	}

	public function getSerial_fle() {
		return $this->serial_fle;
	}

	public function setSerial_fle($serial_fle) {
		$this->serial_fle = $serial_fle;
	}

	public function getName_pdf() {
		return $this->name_pdf;
	}

	public function setName_pdf($name_pdf) {
		$this->name_pdf = $name_pdf;
	}

	public function getCreation_date_pdf() {
		return $this->creation_date_pdf;
	}

	public function setCreation_date_pdf($creation_date_pdf) {
		$this->creation_date_pdf = $creation_date_pdf;
	}

	public function getReception_date_pdf() {
		return $this->reception_date_pdf;
	}

	public function setReception_date_pdf($reception_date_pdf) {
		$this->reception_date_pdf = $reception_date_pdf;
	}

	public function getStatus_pdf() {
		return $this->status_pdf;
	}

	public function setStatus_pdf($status_pdf) {
		$this->status_pdf = $status_pdf;
	}
}

?>
