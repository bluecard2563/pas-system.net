<?php
/*
File: ProductByDealer.class.php
Author: Edwin Salvador.
Creation Date: 24/02/2010
Last Modified:
Modified By:
*/

class ProductByDealer{
    var $db;
    var $serial_pbd;
    var $serial_dea;
    var $serial_pxc;
    var $status_pbd;

    function __construct($db, $serial_pbd=NULL, $serial_dea=NULL, $serial_pxc=NULL, $status_pbd = NULL){
        $this -> db = $db;
        $this -> serial_pbd = $serial_pbd;
        $this -> serial_dea = $serial_dea;
        $this -> serial_pxc = $serial_pxc;
        $this -> status_pbd = $status_pbd;
    }

    /***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
    function getData(){
        if($this->serial_pbd!=NULL){
            $sql = "SELECT serial_pbd,
                           serial_dea,
                           serial_pxc,
                           status_pbd
                    FROM product_by_dealer
                    WHERE serial_pbd='".$this->serial_pbd."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_pbd =$result->fields[0];
                $this -> serial_dea = $result->fields[1];
                $this -> serial_pxc = $result->fields[2];
                $this -> status_pbd = $result->fields[3];

                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO product_by_dealer (
                                serial_pbd,
                                serial_dea,
                                serial_pxc,
                                status_pbd
                                )
            VALUES (
                    NULL,
                    '".$this->serial_dea."',
                    '".$this->serial_pxc."',
                    '".$this->status_pbd."'
                    )";
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: update
    @Description: update a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function update(){
        if($this->status_pbd != NULL && ($this->serial_pxc || $this->serial_pbd)){
            $sql="UPDATE product_by_dealer
                  SET status_pbd = '".$this->status_pbd."'
                  WHERE ";

            if($this->serial_pxc){
                $sql .= "serial_pxc = '".$this->serial_pxc."'";
            }elseif($this->serial_pbd){
                $sql .= "serial_pbd = '".$this->serial_pbd."'";
            }
			//die($sql);
            $result = $this->db->Execute($sql);

            if ($result == true){
                return true;
            }else{
				ErrorLog::log($db, 'FAILED TO UPDATE PBD', $sql);
                return false;
            }
        }
    }

	    /**
    @Name: update
    @Description: update a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function updateStatus(){
            $sql="UPDATE product_by_dealer
                  SET status_pbd = '".$this->status_pbd."'
                  WHERE serial_pbd = '".$this->serial_pbd."'";

			//echo $sql.'<br/>';
            $result = $this->db->Execute($sql);

            if ($result == true){
                return true;
            }else{
                return false;
            }
    }

	/**
    @Name: deactivateProductsByDealer
    @Description: update a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    public static function deactivateProductsByDealer($db, $serial_dea){
		$sql="UPDATE product_by_dealer
			  SET status_pbd = 'INACTIVE'
			  WHERE serial_dea = $serial_dea";

		$result = $db->Execute($sql);

		if ($result == true){
			return true;
		}else{
			return false;
		}
    }

    /**
    @Name: delete
    @Description: delete a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function delete($used_dealers = NULL){
        if($this->serial_pxc != NULL){
            $sql="DELETE FROM product_by_dealer
                  WHERE serial_pxc='".$this->serial_pxc."'";
            if($used_dealers != ''){
                  $sql .= " AND serial_pbd NOT IN (".$used_dealers.")";
            }

            $result = $this->db->Execute($sql);
            //echo $sql." ";
            if ($result==true){
                return true;
            }else{
                return false;
            }
        }
    }

    function getIsUsed_dea(){
        if($this->serial_pbd != NULL){
            $sql="SELECT s.serial_pbd, pbd.serial_dea
                  FROM product_by_dealer pbd
                  JOIN sales s ON s.serial_pbd = pbd.serial_pbd
                  WHERE s.serial_pbd='".$this->serial_pbd."'";

            $result = $this->db->Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }else{
                return 0;
            }
            //Debug::print_r($arreglo);
            return $arreglo;
        }
    }

    function productByDealerExists(){
        if($this->serial_dea != NULL && $this->serial_pxc != NULL){
            $sql = "SELECT pbd.serial_pbd
                    FROM product_by_dealer pbd
                    WHERE pbd.serial_dea = '".$this->serial_dea."'
                    AND pbd.serial_pxc = '".$this->serial_pxc."'";
			//echo $sql;
            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }

	public static function pbdExists($db, $serial_pxc, $serial_dea){
            $sql = "SELECT 1
                    FROM product_by_dealer pbd
                    WHERE pbd.serial_dea = $serial_dea
                    AND pbd.serial_pxc = $serial_pxc AND pbd.status_pbd = 'ACTIVE'";
            $result = $db->Execute($sql);
            if($result->fields[0]){
                return 1;
            }
            return 0;
    }

 function getproductByDealer($serial_dea, $serial_lang, $masive=NULL, $status=NULL, $multiple_sales=NULL){
        $sql= "SELECT pbd.serial_pbd, pbd.serial_dea, pbd.serial_pxc, pbc.serial_pro, pbl.name_pbl, p.*
               FROM product_by_dealer pbd
               JOIN product_by_country pbc ON pbd.serial_pxc=pbc.serial_pxc
               JOIN product_by_language pbl ON pbl.serial_pro=pbc.serial_pro
               JOIN product p ON p.serial_pro=pbc.serial_pro
               WHERE pbd.serial_dea = '".$serial_dea."'
               AND pbl.serial_lang = '".$serial_lang."'";
			if($masive=='YES'){
				$sql.=" AND p.masive_pro='YES'";
			}
			else if($masive=='NO'){
				$sql.=" AND p.masive_pro='NO'";
			}
			if($status != NULL) {
				$sql.=" AND pbd.status_pbd='$status'
						AND pbc.status_pxc='$status'
						AND p.status_pro='$status'";
			}
			if($multiple_sales){
				$sql.=" AND p.flights_pro='1'
						AND p.calculate_pro='YES'";
			}
			
			$sql .= " ORDER BY name_pbl";

         $result = $this->db->Execute($sql);
         $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }
            //Debug::print_r($arreglo);
            return $arreglo;

    }

	public static function getPXCByDealerAndProductWeb($db, $serial_dea, $serial_pro){
        $sql= "	SELECT pbd.serial_pxc
                FROM product_by_dealer pbd
                JOIN product_by_country pbc ON pbd.serial_pxc = pbc.serial_pxc
                WHERE pbd.serial_dea = $serial_dea
                AND pbc.serial_pro = $serial_pro";
        //die($sql);
        $result = $db -> Execute($sql);
         if($result){
         	return $result -> fields['0'];
         }
         return false;
	}

	public static function getPBDbyPXC($db, $serial_dea, $serial_pxc){
        $sql= "	SELECT serial_pbd
        		FROM product_by_dealer
                WHERE serial_dea = $serial_dea
                AND serial_pxc = $serial_pxc";
         $result = $db->Execute($sql);
         $num = $result -> RecordCount();
         if($result){
         	return $result->fields[0];
         }
         return false;;
    }

	public static function getProductByDealerStatic($db, $serial_dea, $serial_lang){
        $sql= "SELECT pbd.serial_pbd, pbd.serial_dea, pbd.serial_pxc,pbc.serial_pro, pbl.name_pbl, p.*
                FROM product_by_dealer pbd
                JOIN product_by_country pbc ON pbd.serial_pxc=pbc.serial_pxc
                JOIN product_by_language pbl ON pbl.serial_pro=pbc.serial_pro
                JOIN product p ON p.serial_pro=pbc.serial_pro
                WHERE pbd.serial_dea = $serial_dea
                AND pbl.serial_lang = $serial_lang";

         $result = $db->getAll($sql);
         
		 if($result){
			 return $result;
		 }else{
			 return FALSE;
		 }
    }


    /**
    @Name: getMasiveProductByDealer
    @Description: returns an array with the masive products of a given dealer
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/

	function getMasiveProductByDealer($serial_dea, $serial_lang){
        $sql= "SELECT pbd.serial_pbd, pbd.serial_dea, pbd.serial_pxc,pbc.serial_pro, pbl.name_pbl, p.*
                FROM product_by_dealer pbd
                JOIN product_by_country pbc ON pbd.serial_pxc=pbc.serial_pxc
                JOIN product_by_language pbl ON pbl.serial_pro=pbc.serial_pro
                JOIN product p ON p.serial_pro=pbc.serial_pro
                WHERE pbd.serial_dea = '".$serial_dea."'
                AND pbl.serial_lang = '".$serial_lang."'
				AND p.masive_pro='YES'";

         $result = $this->db->Execute($sql);
         $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }
            //Debug::print_r($arreglo);
            return $arreglo;

    }

    function getDealerProductByCountry($serial_pxc){
        $sql= "SELECT pbd.serial_dea 
                FROM product_by_dealer pbd
                WHERE pbd.serial_pxc= '".$serial_pxc."'";
         $result = $this->db->Execute($sql);
         $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->fields[0];
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }else{
                return 0;
            } 
            //Debug::print_r($arreglo);die;
            return $arreglo;

    }


          /***********************************************
	* function getTaxes
	@Name: getTaxes
	@Description: get the taxes assigned to the product
	@Params:
         *       atribute $serial_pxc
	@Returns: Array: array with name and value of each tax
           *      null if there are no taxes.
	***********************************************/
    function getTaxes(){
             $sql= "SELECT tbc.name_tax,tbc.percentage_tax
                FROM taxes_by_product tbp
                JOIN tax_by_country tbc ON tbp.serial_tax=tbc.serial_tax
                WHERE tbp.serial_pxc = {$this->serial_pxc}
                AND tbc.status_tax='ACTIVE'";
         //echo $sql;
         $result = $this->db->getAll($sql);
         if($result)
            return $result;
         else
            return null;
    }

/*
	 * getProductsByDealerReport
	 * @param: $db - DB connection
	 * @param: $serial_cou - Country ID
	 * @param: $serial_mbc - ManagerByCountry ID
	 * @return: a list of products for all dealers in a specific country and a specific manager.
	 *			The list includes the number of sales made by each dealer.
	 */
	public static function getProductsByDealerReport($db, $serial_cou, $serial_mbc=NULL, $serial_dea=NULL){
		$sql="SELECT dea.serial_dea, pbd.serial_pbd, dea.name_dea, pbl.name_pbl, COUNT(s.serial_sal) AS 'items_sold',
					 c.name_cou, z.name_zon, m.name_man
			  FROM product_by_dealer pbd
			  JOIN dealer dea ON dea.serial_dea=pbd.serial_dea AND dea.status_dea='ACTIVE'";

		if($serial_dea!=NULL && $serial_dea!=''){
			$sql.=" AND dea.serial_dea='".$serial_dea."'";
		}

		$sql.=" JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN country c ON c.serial_cou=pxc.serial_cou AND c.serial_cou='".$serial_cou."'
				JOIN zone z ON z.serial_zon=c.serial_zon
				JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc";

		if($serial_mbc!=NULL && $serial_mbc!=''){
			$sql.=" AND mbc.serial_mbc='".$serial_mbc."'";
		}

		$sql.=" JOIN manager m ON m.serial_man=mbc.serial_man
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang='".$_SESSION['serial_lang']."'
				LEFT JOIN sales s ON s.serial_pbd=pbd.serial_pbd
				GROUP BY pbd.serial_pbd
				ORDER BY m.name_man, dea.name_dea";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/***********************************************
	* function getFligths
	@Name: getFligths
	@Description: get the fligths form a product
	@Params:
         *       atribute $serial_pbd
	@Returns: Number of Fligths
	***********************************************/
    function getFligths($serial_pbd){
             $sql= "SELECT pbd.serial_pbd,p.serial_pro,p.flights_pro
					FROM   product_by_dealer pbd
					JOIN   product_by_country pbc ON pbc.serial_pxc=pbd.serial_pxc
					JOIN   product p ON pbc.serial_pro=p.serial_pro
					WHERE  pbd.serial_pbd='".$serial_pbd."'";
         //echo $sql;
          $result = $this->db->Execute($sql);
         if($result->fields[0])
            return $result->fields[2];
         else
            return null;
    }

	/*
	 * getProductsByDealerByLanguage
	 * @param: $db - DB connection
	 * @param: $serial_pbd - Product By Dealer ID
	 * @param: $serial_lang - Language ID
	 * @return: product's name
	 */
	public static function getProductsByDealerByLanguage($db, $serial_pbd, $serial_lang){
		$sql="SELECT pbl.name_pbl
			  FROM product_by_dealer pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
			  JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = $serial_lang
			  WHERE pbd.serial_pbd = $serial_pbd";

		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

    public static function getProductByProductDealerId($db, $serial_pbd){
        $sql= "SELECT p.*
               FROM product_by_dealer pbd
               JOIN product_by_country pbc ON pbd.serial_pxc=pbc.serial_pxc
               JOIN product_by_language pbl ON pbl.serial_pro=pbc.serial_pro
               JOIN product p ON p.serial_pro=pbc.serial_pro
               WHERE pbd.serial_pbd = '".$serial_pbd."'
               AND pbl.serial_lang = '".$_SESSION['serial_lang']."'";
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if($num > 0){
            $arreglo = array();
            $cont = 0;
            do{
                $arreglo[$cont++] = $result->GetRowAssoc(false);
                $result->MoveNext();
            }while($cont < $num);
        }
        return $arreglo;

    }

    ///GETTERS
    function getSerial_pbd(){
        return $this->serial_pbd;
    }
    function getSerial_dea(){
        return $this->serial_dea;
    }
    function getSerial_pxc(){
        return $this->serial_pxc;
    }
    function getStatus_pbd(){
        return $this->status_pbd;
    }

    ///SETTERS
    function setSerial_pbd($serial_pbd){
        $this->serial_pbd = $serial_pbd;
    }
    function setSerial_dea($serial_dea){
        $this->serial_dea = $serial_dea;
    }
    function setSerial_pxc($serial_pxc){
        $this->serial_pxc = $serial_pxc;
    }
    function setStatus_pbd($status_pbd){
        $this->status_pbd = $status_pbd;
    }
}
?>