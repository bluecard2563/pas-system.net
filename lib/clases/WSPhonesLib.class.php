<?php

/**
 * File: WSPhonesLib
 * Author: David Rosales
 * Creation Date: 
 * Last Modified: 
 * Modified By: 
 */
class WSPhonesLib {
    
    public static function getCustomerByFile($db, $serial_fle) {
        $sql = "SELECT cus.serial_cus as 'id', cus.document_cus as 'document_con', cus.first_name_cus as 'firstname_con', cus.last_name_cus as 'lastname_con', cus.type_cus as 'type_con',
                IF(sal.serial_cus = cus.serial_cus, 'MAIN','EXTRA') as 'check_per', 
                sal.serial_sal, DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as 'begin_date', DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as 'end_date'
                FROM `file` fle
                JOIN customer cus ON fle.serial_cus = cus.serial_cus
                JOIN sales sal ON fle.serial_sal = sal.serial_sal
                WHERE fle.serial_fle = $serial_fle AND sal.status_sal <> 'EXPIRED'";
//                die($sql);

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $array = array();
            $count = 0;
            do {
                $array[$count] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $count++;
            } while ($count < $num);
        }
        return $array;
    }
    
    public static function getCustomerByDocument($db, $document) {
        $sql = "SELECT cus.serial_cus as 'id', cus.document_cus as 'document_con', cus.first_name_cus as 'firstname_con', cus.last_name_cus as 'lastname_con', cus.type_cus as 'type_con',
                IF(sal.serial_cus = cus.serial_cus, 'MAIN','EXTRA') as 'check_per', 
                sal.serial_sal, DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as 'begin_date', DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as 'end_date'
                FROM customer cus 
                JOIN sales sal ON sal.serial_cus = cus.serial_cus
                WHERE cus.document_cus = '$document' AND sal.status_sal <> 'EXPIRED'";
                //die($sql);

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $array = array();
            $count = 0;
            do {
                $array[$count] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $count++;
            } while ($count < $num);
        }
        return $array;
    }
    
    public static function getCustomerByCardNumber($db, $card_number) {
        $sql = "SELECT c.serial_cus as 'id', c.document_cus as 'document_con', c.first_name_cus as 'firstname_con', c.last_name_cus as 'lastname_con', c.type_cus as 'type_con',
                IF(trl.serial_cus, 'MULTI','SINGLE') as 'check_con',
                s.serial_sal, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as 'begin_date', DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as 'end_date',
                cus.serial_cus as 'id_ext', cus.document_cus as 'document_ext', cus.first_name_cus as 'firstname_ext', cus.last_name_cus as 'lastname_ext', cus.type_cus as 'type_cus'
                FROM sales s
                LEFT JOIN customer c ON s.serial_cus = c.serial_cus
                LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                LEFT JOIN customer cus ON cus.serial_cus = trl.serial_cus
                WHERE (s.card_number_sal = '" . $card_number . "'
                OR trl.card_number_trl = '" . $card_number . "') AND s.status_sal <> 'EXPIRED'";
		//die($sql);

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $array = array();
            $count = 0;
            do {
                $array[$count] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $count++;
            } while ($count < $num);
        }
        return $array;
    }
    
    public static function getMainCustomerBySale($db, $serial_sal){
        $sql = "SELECT cus.serial_cus as 'id', cus.document_cus as 'document_con', 
                cus.first_name_cus as 'firstname_con', cus.last_name_cus as 'lastname_con'
                FROM sales sal 
                JOIN customer cus ON sal.serial_cus = cus.serial_cus
                WHERE sal.serial_sal = $serial_sal";
//                die($sql);

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $array = array();
            $count = 0;
            do {
                $array[$count] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $count++;
            } while ($count < $num);
        }
        return $array;
    }
    
    public static function  getExtrasCustomersBySale($db, $serial_sal){
		$sql="	SELECT e.serial_cus as 'id', c.document_cus as 'document_con', c.first_name_cus as 'firstname_con', 
                c.last_name_cus as 'lastname_con', e.relationship_ext as 'relationship'
				FROM extras e
				JOIN customer c ON e.serial_cus=c.serial_cus
				WHERE e.serial_sal='".$serial_sal."'";
//		die($sql);
		$result = $db->Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
           $arreglo=array();
           $cont=0;

           do{
               $arreglo[$cont]=$result->GetRowAssoc(false);
               $result->MoveNext();
               $cont++;
           }while($cont<$num);
        }
        return $arreglo;
	}
    
}
