<?php
/*
File: benefitbyLanguage.class.php
Author: Miguel Ponce.
Creation Date: 28/12/2009 11:48
Last Modified: 29/12/2009
Modified By: Miguel Ponce
*/

class BenefitbyLanguage{				
	var $db;
	var $serial_bbl;
	var $serial_lang;
	var $serial_ben;
	var $description_bbl; 

	function __construct($db, $serial_bbl=NULL, $serial_lang=NULL, $serial_ben=NULL, $description_bbl=NULL ){
		$this -> db = $db;
		$this -> serial_bbl = $serial_bbl;
		$this -> serial_lang = $serial_lang;
		$this -> serial_ben = $serial_ben;
		$this -> description_bbl = $description_bbl;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
	function getData(){
		if($this->serial_bbl!=NULL){
			$sql = 	"SELECT serial_bbl, serial_lang, serial_ben, description_bbl
					FROM benefits_by_language
					WHERE serial_bbl='".$this->serial_bbl."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_bbl =$result->fields[0];
				$this -> serial_lang = $result->fields[1];
				$this -> serial_ben =$result->fields[2];
				$this -> description_bbl = $result->fields[3];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * funcion benefitExists
    * verifies if a Benefit exists or not
    ***********************************************/
	function benefitExists($description_bbl, $serial_lang, $serial_ben=NULL){
		$sql = 	"SELECT b.serial_ben
                         FROM benefits b
                         JOIN benefits_by_language bbl ON b.serial_ben=bbl.serial_ben AND bbl.serial_lang = '".utf8_encode($serial_lang)."'
			 WHERE LOWER(bbl.description_bbl)= _utf8'".utf8_encode($description_bbl)."' collate utf8_bin";
		if($serial_ben!=NULL){
			$sql.=" AND b.serial_ben <> ".$serial_ben;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	@Name: getTranslations
	@Description: Returns an array of all the translations for a specific benefit
	@Params: $serial_ben: Benefit ID
	@Returns: An array of translations
	**/
	function getTranslations($serial_ben,$serial_lang=NULL){
		$sql = 	"SELECT bbl.serial_bbl as 'serial', bbl.description_bbl, l.name_lang, ben.weight_ben, bbl.serial_bbl as 'update', l.serial_lang
                         FROM benefits_by_language bbl
                         JOIN language l ON l.serial_lang=bbl.serial_lang
                         JOIN benefits ben ON ben.serial_ben=bbl.serial_ben
                         WHERE bbl.serial_ben = '".$serial_ben."'";
                if($serial_lang!=NULL) {
                    $sql.=" AND l.serial_lang = '".$serial_lang."'";
                }

                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO benefits_by_language (
				serial_bbl,
				serial_lang,
				serial_ben,
				description_bbl)
            VALUES (
                NULL,
				'".$this->serial_lang."',				
				'".$this->serial_ben."',
				'".$this->description_bbl."')";
		//die($sql);
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE benefits_by_language SET
					serial_lang ='".$this->serial_lang."',
					serial_ben ='".$this->serial_ben."',
					description_bbl ='".$this->description_bbl."'										
				WHERE serial_bbl = '".$this->serial_bbl."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }
	
	/***********************************************
    * funcion getStatusValues
    * Returns the current values for the status field.
    ***********************************************/	
	function getStatusValues(){
		$sql = "SHOW COLUMNS FROM benefits LIKE 'status_ben'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	///GETTERS
	function getSerial_bbl(){
		return $this->serial_bbl;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}	
	function getSerial_ben(){
		return $this->serial_ben;
	}
	function getDescription_bbl(){
		return $this->description_bbl;
	}
	

	///SETTERS	
	
	function setSerial_bbl($serial_bbl){
		$this->serial_bbl = $serial_bbl;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setSerial_ben($serial_ben){
		$this->serial_ben = $serial_ben;
	}
	function setDescription_bbl($description_bbl){
		$this->description_bbl = $description_bbl;
	}
}
?>