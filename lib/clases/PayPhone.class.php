<?php

class PayPhone{
	
	public $transaction;
	
	/**
      @Name: constructor
      @Description: Creates a configuration manager
      @Params: private key, app public key, token, app id, refresh token, client id, client secret, api path.
      @Result: configuration manager object created
     **/
	function __construct($privateKey, $applicationPublicKey, $token, $applicationId, $refreshToken = null, $clientId, $clientSecret, $apiPath){
		try{
			$configurationManager = ConfigurationManager::Instance();
			$configurationManager->PrivateKey = $privateKey;
			$configurationManager->ApplicationPublicKey = $applicationPublicKey;
			$configurationManager->Token = $token;
			$configurationManager->ApplicationId = $applicationId;
			$configurationManager->RefreshToken = $refreshToken;
			$configurationManager->ClientId = $clientId;
			$configurationManager->ClientSecret = $clientSecret;
			$configurationManager->ApiPath = $apiPath;
			
			$this->transaction = new Transaction();
		}catch (Exception $e){
			$e->getMessage();
		}
	}
	
	/**
      @Name: validateRegionByCode
      @Description: validates if payphone code region is accepted
      @Params: $regionPrefixNumber
      @Return: array
     **/
	public function validateRegionByCode($regionPrefixNumber){
		$response['result'] = NULL;
		$response['msg'] = 'error';
		try{
			$availableRegions = $this->transaction->GetRegions();
			if (is_array($availableRegions)){
				foreach ($availableRegions as $it => $obj) {
					if ($obj->RegionPrefixNumber == $regionPrefixNumber){
						//if $regionPrefixNumber was found
						$response['result'] = $obj->RegionPrefixNumber;
						$response['msg'] = 'success';
						break;
					}
				}
				//if $regionPrefixNumber not found
				if ($response['msg'] == 'error'){
					$response['result'] = 'RegionCodeNotFound';
				}
			} else {
				//Response from WS is not as expected: obj array
				$response['result'] = 'API internal error';
			}
		} catch (Exception $e){
			$response['result'] = $e->getMessage();
		}
		return $response;
	}
	
	/**
      @Name: setPayPhoneRequest
      @Description: Esta etapa se envían los datos de la transacción a PayPhone y ésta devolverá los
					detalles del usuario que va a realizar el pago de forma que se verifique la identidad
		Podríamos decir que en este paso se reserva la transacción en PayPhone.
      @Params: $amount,$amountWithTax,$amountWithOutTax,$purchaseLanguage = NULL,$tax,$timeZone,$latitude,$longitude,$clientTransactionId,$clientUserId = NULL,$phoneNumber,$regionCode
      @Return: object array
     **/
	function setPayPhoneRequest($amount,$amountWithTax,$amountWithOutTax,$purchaseLanguage = NULL,$tax,$timeZone,$latitude,$longitude,$clientTransactionId = NULL,$clientUserId = NULL,$phoneNumber,$regionCode){
		try{
			//Validate if phone code is accepeted
			$validatePhoneRegion = $this->validateRegionByCode($regionCode);
			if ($validatePhoneRegion['msg'] == 'success'){
				$payphoneRequest = new PayPhoneRequest();
				$payphoneRequest->setAmount($amount);
				$payphoneRequest->setAmountWithTax($amountWithTax);
				$payphoneRequest->setAmountWithOutTax($amountWithOutTax);
				$payphoneRequest->setPurchaseLanguage($purchaseLanguage);
				$payphoneRequest->setTax($tax);
				$payphoneRequest->setTimeZone($timeZone);
				$payphoneRequest->setLatitude($latitude);
				$payphoneRequest->setLongitude($longitude);
				$payphoneRequest->setClientTransactionId($clientTransactionId);
				$payphoneRequest->setClientUserId($clientUserId);
				$payphoneRequestObjData = $payphoneRequest->getTransactionRequestModel();
				
				if ($payphoneRequestObjData['msg'] != 'error'){
					$data = (object) $payphoneRequestObjData['result'];
					$result = $this->transaction->SetTransaction($data, $phoneNumber, $regionCode);
					//Call to Transaction API
					$response['msg'] = 'success';
					$response['result'] = $result;
				} else{
					$response = $payphoneRequestObjData;
				}
			} else {
				$response = $validatePhoneRegion;
			}
		} catch(PayPhoneWebException $pex){
			$response['msg'] = 'exception_error';
			$response['result'] = $pex->ErrorList[0];
		}
		
		return $response;
	}
	
	/**
      @Name: doPayPhoneTransaction
      @Description: se toma el identificador de la transacción recibido en el paso anterior y se envía a PayPhone para que se le notifique al usuario de la solicitud de cobro
      @Params: $transactionId
      @Return: object array
     **/
	function doPayPhoneTransaction($transactionId){
		try{
			$response['msg'] = 'error';
			//Send transaction to payphone
			$result = $this->transaction->DoTransaction($transactionId);
			
			if ($result->success == false) {
				$response['msg'] = 'success';
				$response['result'] = 'PaymentForApproval';
			} else {
				$response['msg'] = 'success';
				$response['result'] = $result->Message;
			}
		} catch(PayPhoneWebException $pex){
			$response['msg'] = 'exception_error';
			$response['result'] = $pex->ErrorList[0];
		}
		
		return $response;
	}
	
	/**
      @Name: statusPayPhoneTransaction
      @Description: Con el siguiente método es posible obtener el estado de la transacción por su identificador.
      @Params: $transactionId
      @Return: object array
     **/
	function statusPayPhoneTransaction($transactionId){
		try{
			$response['msg'] = 'error';
			//Check transaction status
			$result = $this->transaction->GetTransactionById($transactionId);
			
			if ($result->Message) {
				$response['msg'] = 'success';
				$response['result'] = $result->Message->TransactionStatus;
				$response['message'] = $result->Message->Message;
			} else {
				$response['msg'] = 'success';
				$response['result'] = $result->Message->Message;
			}
		} catch(PayPhoneWebException $pex){
			$response['msg'] = 'exception_error';
			$response['result'] = $pex->ErrorList[0];
		}
		
		return $response;
	}
	
	/**
      @Name: annulmentPayPhoneTransaction
      @Description: Con el siguiente método es posible obtener el estado de la transacción por su identificador.
      @Params: $transactionId
      @Return: object array
     **/
	function annulmentPayPhoneTransaction($timeZone, $transactionId, $latitude = NULL, $longitude = NULL){
		try{
			$response['msg'] = 'error';
			//Call for annulment
			$payphoneAnnulment = new PayPhoneAnnulmentRequest();
			$payphoneAnnulment->setTimeZone($timeZone);
			$payphoneAnnulment->setLatitude($latitude);
			$payphoneAnnulment->setLongitude($longitude);
			$payphoneAnnulment->setTransactionId($transactionId);
			$payphoneAnnulmentObjData = $payphoneAnnulment->getAnnulmentRequestModel();
			
			if ($payphoneAnnulmentObjData['msg'] != 'error'){
				$data = (object) $payphoneAnnulmentObjData['result'];
				$result = $this->transaction->DoAnnulment($data);
				//Call to Transaction API
				$response['msg'] = 'success';
				$response['result'] = $result;
			} else{
				$response = $payphoneAnnulmentObjData;
			}
		} catch(PayPhoneWebException $pex){
			$response['msg'] = 'exception_error';
			$response['result'] = $pex->ErrorList[0];
		}
		
		return $response;
	}
}
