<?php
/*
File: Zone.class.php
Author: Pablo Puente
Creation Date: 28/12/2009 11:48
Last Modified: 06/01/2010 15:06
Modified By: David Bergmann
*/
class Zone {				
	var $db;
	var $serial_zon;
	var $name_zon;
	
	function __construct($db, $serial_zon = NULL, $name_zon = NULL){
		$this -> db = $db;
		$this -> serial_zon = $serial_zon;
		$this -> name_zon = $name_zon;
	}	
	
        /***********************************************
        * function getData
        * gets data by serial_zon
        ***********************************************/
	function getData(){
		if($this->serial_zon!=NULL){
			$sql = 	"SELECT serial_zon, name_zon
					FROM zone
					WHERE serial_zon='".$this->serial_zon."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_zon=$result->fields[0];
				$this->name_zon=$result->fields[1];	
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){		
		$sql="INSERT INTO zone (
					serial_zon, 
					name_zon					
					)
			  VALUES(NULL,
					'".$this->name_zon."'					
					);";
					
		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
 	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
		$sql="UPDATE zone 
		SET name_zon='".$this->name_zon."'
		WHERE serial_zon='".$this->serial_zon."'";
					
		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/***********************************************
        * funcion getZones
        * gets all the Zones of the system
        ***********************************************/
	function getZones(){
		$sql = 	"SELECT serial_zon, name_zon
				 FROM zone
				 ORDER BY name_zon";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

        /***********************************************
        * funcion getInvoiceLogZones
        * gets all the Zones of the system where are
        * pending invoice logs.
        ***********************************************/
	function getInvoiceLogZones(){
		$sql = 	"SELECT DISTINCT zon.serial_zon, zon.name_zon
                             FROM zone zon
                             JOIN country cou ON cou.serial_zon = zon.serial_zon
                             JOIN city cit ON cou.serial_cou=cit.serial_cou
                             JOIN sector sec ON sec.serial_cit=cit.serial_cit
                             JOIN dealer dea ON dea.serial_sec=sec.serial_sec
                             JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
                             JOIN sales s ON cnt.serial_cnt=s.serial_cnt
                             JOIN invoice inv ON inv.serial_inv=s.serial_inv
                             JOIN invoice_log inl ON inl.serial_inv=inv.serial_inv AND inl.status_inl='PENDING'
                             ORDER BY zon.name_zon";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /***********************************************
        * funcion getZones
        * gets all the Zones of the system
        ***********************************************/
	function getCreditNoteZones(){
		$sql = 	"SELECT DISTINCT z.serial_zon, z.name_zon
				 FROM zone z
                                 JOIN country cou ON cou.serial_zon = z.serial_zon
                                 JOIN city cit ON cou.serial_cou=cit.serial_cou
                                 JOIN sector sec ON sec.serial_cit=cit.serial_cit
                                 JOIN dealer dea ON dea.serial_sec=sec.serial_sec
                                 JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
                                 JOIN sales s ON cnt.serial_cnt=s.serial_cnt
                                 JOIN refund r ON r.serial_sal=s.serial_sal 
				 JOIN credit_note cn ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'
				 ORDER BY z.name_zon";
                                 //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
	/** 
	@Name: getZonesAutocompleter
	@Description: Returns an array of zones.
	@Params: Zone name or pattern.
	@Returns: Zone array
	**/
	function getZonesAutocompleter($name_zon){
		$sql = "SELECT serial_zon, name_zon
				FROM zone
				WHERE LOWER(name_zon) LIKE _utf8'%".utf8_encode($name_zon)."%' collate utf8_bin
				LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/***********************************************
    * funcion zoneExists
    * verifies if a Zone exists or not
    ***********************************************/
	function zoneExists($txtNameZone, $serial_zon=NULL){
		$sql = 	"SELECT serial_zon
				 FROM zone
				 WHERE LOWER(name_zon)= _utf8'".utf8_encode($txtNameZone)."' collate utf8_bin";	 
		if($serial_zon!=NULL){
			$sql.=" AND serial_zon <> ".$serial_zon;
		}
		$result = $this->db->Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

		/**
	@Name: getZonesByInvoice
	@Description: Retrieves the information of a specific zone list
	@Params:
	@Returns: Zone array
	**/
	function getZonesByInvoice($countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(zon.serial_zon), zon.name_zon
					 FROM sales s
					   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea
						JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou
					   JOIN zone zon ON zon.serial_zon=cou.serial_zon
					   LEFT JOIN invoice i ON i.serial_inv=s.serial_inv
					   WHERE cou.serial_cou IN (".$countriesID.")
					   ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY zon.name_zon";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			
			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

		/**
	@Name: getZonesByFreeCard
	@Description: Retrieves the information of a specific zone list
	@Params:
	@Returns: Zone array
	**/
	function getZonesByFreeCard($countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($countriesID){
			$sql =  " SELECT distinct(zon.serial_zon), zon.name_zon
						FROM sales sal
							JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
							JOIN dealer br ON cnt.serial_dea=br.serial_dea
							JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
							JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
							JOIN sector sec ON dea.serial_sec=sec.serial_sec
							JOIN city cit ON sec.serial_cit=cit.serial_cit
							JOIN country cou ON cit.serial_cou=cou.serial_cou
							JOIN zone zon ON zon.serial_zon=cou.serial_zon
						 WHERE sal.free_sal = 'YES' AND cou.serial_cou IN (".$countriesID.")
						 ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY zon.name_zon";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

	/***********************************************
        * function getAllowedZones
        * gets all the Zones
        ***********************************************/
	function getAllowedZones($countriesID){
		$arreglo=array();
		if($countriesID){
			$sql = 	"SELECT DISTINCT(zon.serial_zon), zon.name_zon
					 FROM zone zon
					 JOIN country cou ON cou.serial_zon=zon.serial_zon
					 WHERE cou.serial_cou IN (".$countriesID.")
					 ORDER BY zon.name_zon";

			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$cont=0;

				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);

				return $arreglo;
			}
		}

		return FALSE;		
	}

	/*
	 * @name: getDealerZones
	 * @returns: An array of all zones where there's at least one dealer.
	 */
	function getDealerZones($countriesID, $serial_mbc=NULL){
		$sql="SELECT DISTINCT(z.serial_zon) AS 'serial_zon', z.name_zon
			  FROM zone z
			  JOIN country cou ON cou.serial_zon=z.serial_zon AND cou.serial_cou IN (".$countriesID.") AND cou.serial_cou <> 1
			  JOIN city c ON c.serial_cou=cou.serial_cou
			  JOIN sector s ON s.serial_cit=c.serial_cit
			  JOIN dealer d ON d.serial_sec=s.serial_sec";

		if($serial_mbc){
			$sql.=" AND d.serial_mbc=".$serial_mbc;
		}
		
		$sql.=" ORDER BY z.name_zon";

		$result=$this->db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

		/**
	@Name: getZonesSalesAnalysis
	@Description: Retrieves the information of a specific zone list
	@Params:
	@Returns: Zone array
	**/
	function getZonesSalesAnalysis($countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
			$sql =  "SELECT distinct(zon.serial_zon), zon.name_zon
						  FROM sales sal
							JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
							JOIN dealer br ON cnt.serial_dea=br.serial_dea
							JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
							JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
							JOIN sector sec ON br.serial_sec=sec.serial_sec
      			       	    JOIN city cit ON sec.serial_cit=cit.serial_cit
						    JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou IN (".$countriesID.")
						    JOIN zone zon ON zon.serial_zon=cou.serial_zon
						    JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
						    JOIN payments pay ON pay.serial_pay=inv.serial_pay
                          WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')
								AND sal.serial_inv IS NOT NULL AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'
                          ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY zon.name_zon";//die($sql);
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		return $arreglo;
	}

	/**
	@Name: getZonesPricesByProduct
	@Description: Retrieves the information of a specific zone list
	@Params:
	@Returns: Zone array
	**/
	function getZonesPricesByProduct($countriesID, $serial_lang){
		$arreglo=array();
		$sql =  "SELECT DISTINCT (zon.serial_zon), zon.name_zon
						FROM product pro
						JOIN product_by_country pbc ON pbc.serial_pro = pro.serial_pro
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
						JOIN country cou ON cou.serial_cou = pbc.serial_cou
						JOIN zone zon ON zon.serial_zon = cou.serial_zon
					WHERE pbl.serial_lang = ".$serial_lang." AND cou.serial_cou IN (".$countriesID.")
					ORDER BY zon.name_zon
					  ";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getZonesBenefitsByProduct
	@Description: Retrieves the information of a specific zone list
	@Params:
	@Returns: Zone array
	**/
	function getZonesBenefitsByProduct($countriesID, $serial_lang){
		$arreglo=array();
		$sql =  "SELECT DISTINCT (zon.serial_zon), zon.name_zon
						FROM product pro
						JOIN product_by_country pbc ON pro.serial_pro=pbc.serial_pro AND pro.status_pro = 'ACTIVE'
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
						JOIN benefits_product bp ON bp.serial_pro=pro.serial_pro AND bp.status_bxp = 'ACTIVE'
						JOIN conditions c ON c.serial_con=bp.serial_con
						LEFT JOIN restriction_type rt ON rt.serial_rst=bp.serial_rst AND rt.status_rst = 'ACTIVE'
						LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = ".$serial_lang."
						JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
						JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang= ".$serial_lang."
						JOIN country cou ON cou.serial_cou = pbc.serial_cou
						JOIN zone zon ON zon.serial_zon = cou.serial_zon
					WHERE pbl.serial_lang = ".$serial_lang." AND cou.serial_cou IN (".$countriesID.")
					ORDER BY zon.name_zon
					  ";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/*
	 * @name: getZonesWithAppliedBonus
	 * @param:
	 */
	public static function getZonesWithAppliedBonus($db){
		$sql="SELECT DISTINCT z.serial_zon, z.name_zon
			  FROM zone z
			  JOIN country c on c.serial_zon=z.serial_zon
			  JOIN city cit ON cit.serial_cou=c.serial_cou
			  JOIN sector s ON s.serial_cit=cit.serial_cit
			  JOIN dealer d ON d.serial_sec=s.serial_sec
			  JOIN dealer b ON b.dea_serial_dea=d.serial_dea
			  JOIN counter cnt ON cnt.serial_dea=b.serial_dea
			  JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
			  JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

        public static function getZonesByCountry($db, $serial_cou){
		$sql="SELECT cou.serial_zon
                        FROM country cou
                        WHERE cou.serial_cou = '$serial_cou'";

		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	///GETTERS
	function getSerial_zon(){
		return $this->serial_zon;
	}
	function getName_zon(){
		return $this->name_zon;
	}


	///SETTERS	
	function setSerial_zon($serial_zon){
		$this->serial_zon = $serial_zon;
	}
	function setName_zon($name_zon){
		$this->name_zon = $name_zon;
	}
}
?>