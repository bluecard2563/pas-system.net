<?php
/*
File: City.class.php
Author: Pablo Puente
Creation Date: 28/12/2009 11:48
Last Modified: 05/01/2010 16:13
Modified By: David Bergmann
*/
class City {				
	var $db;
	var $serial_cit;
	var $serial_cou;
	var $name_cit;
        var $code_cit;
    var $tandi_cit;
	
	function __construct($db, $serial_cit = NULL, $serial_cou = NULL, $name_cit = NULL, $code_cit = NULL, $tandi_cit = NULL){
		$this -> db = $db;
		$this -> serial_cit = $serial_cit;
		$this -> serial_cou = $serial_cou;
		$this -> name_cit = $name_cit;
                $this -> code_cit = $code_cit;
        $this -> tandi_cit = $tandi_cit;
	}
	
	/***********************************************
        * function getData
        * gets data by serial_cit
        ***********************************************/
	function getData(){
		if($this->serial_cit!=NULL){
			$sql = 	"SELECT serial_cit, serial_cou, name_cit, code_cit
					FROM city
					WHERE serial_cit='".$this->serial_cit."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_cit=$result->fields[0];
				$this->serial_cou=$result->fields[1];	
				$this->name_cit=$result->fields[2];
                                $this->code_cit=$result->fields[3];
                $this->tandi_cit=$result->fields[4];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}

	/***********************************************
	* function getGeoData
    * gets geoData by serial_cit
    ***********************************************/
	function getGeoData($serial_cit){
		if($serial_cit){
			$sql = "SELECT cou.serial_cou, cou.serial_zon, cit.serial_cit
					FROM city cit
					JOIN country cou ON cou.serial_cou=cit.serial_cou
					WHERE cit.serial_cit='".$serial_cit."'";

			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			return $result->GetRowAssoc(false);
		}
	}

	/***********************************************
	* function insert
    * inserts a new register in DB
	***********************************************/
	function insert(){
		$sql = "INSERT INTO city (
					serial_cit,
					serial_cou,
					name_cit,
					code_cit,
					tandi_cit
				)VALUES(
					NULL,
					'".$this->serial_cou."',
					'".$this->name_cit."',
					'".$this->code_cit."',
                    '".$this->tandi_cit."'
				);";
					
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
	function update(){
		$sql="UPDATE city 
		SET name_cit='".$this->name_cit."',
                    code_cit='".$this->code_cit."'
		WHERE serial_cit='".$this->serial_cit."'";
					
		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/***********************************************
    * funcion getCities
	* gets all the Cities of the system
    ***********************************************/
	function getCities(){
		$sql = 	"SELECT serial_cit, name_cit, code_cit, tandi_cit
				 FROM city";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}	
	
	/***********************************************
    * funcion getCitiesByCountry
    * gets all the Cities in a specific country of the system
    ***********************************************/
	function getCitiesByCountry($serial_cou,$cityList=NULL){
		$sql =	"SELECT c.serial_cit, c.serial_cou, c.name_cit, c.code_cit,c.tandi_cit
				FROM city c
				WHERE c.serial_cou='".$serial_cou."' AND c.serial_cit NOT IN ('373')";
		if($cityList!=NULL) $sql.="AND c.serial_cit IN (".$cityList.")";
	   
	   	$sql.= "ORDER BY name_cit";
		
	   	$result = $this ->db -> Execute($sql);		
		
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
 	
 	/**
    @Name: getPaymentCitiesByCountryWithComission
    @Description: Get a list of cities with comission to be paid, based on who is going to be paid:
   				RESPONSIBLE OR DEALER
    @Params: $country serial
   			 $cityList:  list of cities which the user has access to.
   			 $comissions_to:comission type
   			 $is_report: parameter to get all comission or only the ones that are not paid yet
    @Returns: cities array
    **/
	function getPaymentCitiesByCountryWithComission($serial_cou,$comissions_to,$cityList=NULL,$is_report=false){
		if(!$serial_cou || !$comissions_to){
				return false;
			}else{
				switch($comissions_to){
					case 'RESPONSIBLE':
							 $sql =   "SELECT DISTINCT cit.serial_cit,cit.name_cit
									   FROM applied_comissions com
									   JOIN user_by_dealer ubd ON com.serial_ubd=ubd.serial_ubd
									   JOIN dealer dea ON dea.serial_dea=ubd.serial_dea
									   JOIN sector sec ON sec.serial_sec=dea.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   WHERE cit.serial_cou=$serial_cou
									   AND com.value_ubd_com>0";

							if($cityList){
								  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
					case 'DEALER':
							 $sql =   "SELECT DISTINCT cit.serial_cit,cit.name_cit
										   FROM applied_comissions com
										   JOIN dealer dea ON dea.serial_dea=com.serial_dea 
										   JOIN sector sec ON sec.serial_sec=dea.serial_sec
										   JOIN city cit ON cit.serial_cit=sec.serial_cit
										   WHERE cit.serial_cou=$serial_cou
										   AND com.value_dea_com>0";

							if($cityList){
							  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
					case 'SUBMANAGER':
							 $sql ="SELECT DISTINCT cit.serial_cit,cit.name_cit
									FROM applied_comissions com
									JOIN dealer dea ON dea.serial_dea=com.serial_dea 
										AND dea.manager_rights_dea = 'YES' 
										AND dea.dea_serial_dea IS NULL
									JOIN sector sec ON sec.serial_sec=dea.serial_sec
									JOIN city cit ON cit.serial_cit=sec.serial_cit
									WHERE cit.serial_cou=$serial_cou
									AND com.value_dea_com>0";

							if($cityList){
							  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
				}
				//echo $sql;
                $result = $this->db -> getAll($sql);
				if($result)
					return $result;
				else
					return false;
			}
	}

	/**
	@Name: getCitiesByCountryWithBonus
    @Description: 	Get a list of cities with bonus to be paid, based on who is going to be paid:
					COUNTER OR DEALER
	@Params: 	$serial_cou : Country Serial
				$cityList:  list of cities which the user has access to.
				$bonus_to:comission type
	@Returns: cities array
    **/
	function getCitiesByCountryWithBonus($serial_cou,$bonus_to,$serial_mbc,$cityList=NULL){
		if(!$serial_cou || !$bonus_to){
				return false;
			}else{
				switch($bonus_to){
					case 'COUNTER':
							 $sql =   "SELECT DISTINCT cit.serial_cit,cit.name_cit
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   WHERE apb.bonus_to_apb='COUNTER'
									   AND apb.status_apb='ACTIVE'
									   AND cit.serial_cou=$serial_cou";
							if($cityList){
								  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							if($serial_mbc!='1'){
								  $sql.=" AND dea.serial_mbc = ".$serial_mbc;
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
					case 'DEALER':
							 $sql =   "SELECT DISTINCT cit.serial_cit,cit.name_cit
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   WHERE apb.bonus_to_apb='DEALER'
									   AND apb.status_apb='ACTIVE'
									   AND cit.serial_cou=$serial_cou";
							if($cityList){
							  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							if($serial_mbc!='1'){
								  $sql.=" AND dea.serial_mbc = ".$serial_mbc;
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
				}
				//echo $sql;
                $result = $this->db -> getAll($sql);
				if($result)
					return $result;
				else
					return false;
			}
	}


	/**
    @Name: getCitiesByCountryWithBonusToAuthorize
    @Description: Get a list of cities with out of date bonus to be authorized, based on who is going to be paid:
					COUNTER OR DEALER
	@Params: $serial_cou : Country Serial
			$cityList:  list of cities which the user has access to.
			$bonus_to:comission type
	@Returns: cities array
	**/
	function getCitiesByCountryWithBonusToAuthorize($serial_cou,$bonus_to,$serial_mbc,$cityList=NULL){
		if(!$serial_cou || !$bonus_to){
				return false;
			}else{
				switch($bonus_to){
					case 'COUNTER':
							 $sql =   "SELECT DISTINCT cit.serial_cit,cit.name_cit
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   WHERE apb.bonus_to_apb='COUNTER'
									   AND apb.status_apb IN ('ACTIVE', 'REFUNDED')
									   AND apb.ondate_apb='NO'
									   AND apb.authorized_apb IS NULL
									   AND cit.serial_cou=$serial_cou";
							if($cityList){
								  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							if($serial_mbc!='1'){
								  $sql.=" AND dea.serial_mbc = ".$serial_mbc;
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
					case 'DEALER':
							 $sql =   "SELECT DISTINCT cit.serial_cit,cit.name_cit
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   WHERE apb.bonus_to_apb='DEALER'
									   AND apb.status_apb IN ('ACTIVE', 'REFUNDED')
									   AND apb.ondate_apb='NO'
									   AND apb.authorized_apb IS NULL
									   AND cit.serial_cou=$serial_cou";
							if($cityList){
							  $sql.=" AND cit.serial_cit IN (".$cityList.")";
							}
							if($serial_mbc!='1'){
								  $sql.=" AND dea.serial_mbc = ".$serial_mbc;
							}
							$sql.=" ORDER BY cit.name_cit";
						break;
				}
				//echo $sql;
                $result = $this->db -> getAll($sql);
				if($result)
					return $result;
				else
					return false;
			}
	}

    /***********************************************
    * funcion getCitiesByCountry
    * gets all the Cities in a specific country of the system
    ***********************************************/
	function getPaymentCitiesByCountry($serial_cou, $serial_mbc, $cityList=NULL){
		$sql =	"SELECT DISTINCT c.serial_cit, c.serial_cou, c.name_cit, c.code_cit
				FROM city c
				JOIN sector sec ON sec.serial_cit=c.serial_cit
				JOIN dealer dea ON dea.serial_sec=sec.serial_sec
				JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				JOIN sales s ON cnt.serial_cnt=s.serial_cnt
				JOIN invoice i ON i.serial_inv=s.serial_inv AND (i.status_inv='STAND-BY')
				WHERE c.serial_cou='".$serial_cou."'";
		
		if($serial_mbc!='1'){
			$sql.=" AND dea.serial_mbc = ".$serial_mbc;
		}

		if($cityList!=NULL){
			$sql.=" AND c.serial_cit IN (".$cityList.")";
		}

		$sql.= "ORDER BY name_cit";
        //echo $sql;
		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

    /***********************************************
    * funcion getCreditNoteCitiesByCountry
    * gets all the Cities in a specific country of the system
    ***********************************************/
	function getCreditNoteCitiesByCountry($serial_cou,$serial_mbc,$cityList=NULL){
		$sql =	"SELECT DISTINCT c.serial_cit, c.serial_cou, c.name_cit, c.code_cit
				FROM city c
				JOIN sector sec ON sec.serial_cit=c.serial_cit
				JOIN dealer dea ON dea.serial_sec=sec.serial_sec
				JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				JOIN sales s ON cnt.serial_cnt=s.serial_cnt
				JOIN refund r ON r.serial_sal=s.serial_sal
                JOIN credit_note cn ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'
				WHERE c.serial_cou='".$serial_cou."'";

		if($cityList!=NULL){
			$sql.="AND c.serial_cit IN (".$cityList.")";
		}
		
		if($serial_mbc!='1'){
			$sql.="AND dea.serial_mbc = ".$serial_mbc;
		}

		$sql.= "ORDER BY name_cit";
                //echo $sql;
		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	function getOwnCountriesByCity($cityList){
		if($cityList) {
		$sql = "SELECT DISTINCT serial_cou
				FROM city
				WHERE serial_cit IN (".$cityList.")";
                
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		
        if($num > 0){
        	$arr=array();
            $cont=0;
            do{
            	$arr[$cont]=$result->fields['0'];
                $result->MoveNext();
                $cont++;
                }while($cont<$num);
			}
		}
        return $arr;
	}
	
	/** 
	@Name: getCitiesAutocompleter
	@Description: Returns an array of cities.
	@Params: City name or pattern.
	@Returns: City array
	**/
	function getCitiesAutocompleter($name_cit){
		$sql = "SELECT c.serial_cit, c.name_cit, co.name_cou
				FROM city c
				JOIN country co ON co.serial_cou=c.serial_cou
				WHERE LOWER(c.name_cit) LIKE _utf8'%".utf8_encode($name_cit)."%' collate utf8_bin
                                OR LOWER(c.code_cit) LIKE _utf8'%".utf8_encode($name_cit)."%' collate utf8_bin
				LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/***********************************************
        * funcion cityExists
        * verifies if a City exists or not
        ***********************************************/
	function cityExists($txtNameCity, $serial_cou, $serial_cit=NULL){
		$sql = 	"SELECT serial_cit
				 FROM city
				 WHERE LOWER(name_cit)= _utf8'".utf8_encode($txtNameCity)."' collate utf8_bin
				 AND serial_cou = ".$serial_cou;
		if($serial_cit!=NULL){
			$sql.=" AND serial_cit <> ".$serial_cit;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

        /***********************************************
        * funcion cityCodeExists
        * verifies if a City exists or not
        ***********************************************/
	function cityCodeExists($txtCodeCity, $serial_cit=NULL){
		$sql = 	"SELECT serial_cit
				 FROM city
				 WHERE LOWER(code_cit)= _utf8'".utf8_encode($txtCodeCity)."' collate utf8_bin";
		if($serial_cit!=NULL){
			$sql.=" AND serial_cit <> ".$serial_cit;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}
	
	/***********************************************
        * funcion getCityAccessList
        * gets all cities which have a dealer
        ***********************************************/
	function getCityAccessList($countryList){
		$sql = 	"SELECT cit.serial_cit, cit.name_cit, cit.serial_cou, cou.name_cou
				FROM city cit
				JOIN country cou
				ON cou.serial_cou=cit.serial_cou
                                AND cou.serial_cou IN (".$countryList.")";

		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		
		//print_r($result);
		//print_r($result->fields['serial_cit']);
		
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				//$arreglo[$cont]=$result->GetRowAssoc(false);
				$arreglo[$result->fields['serial_cou']]['name_cou']=$result->fields['name_cou'];
				$arreglo[$result->fields['serial_cou']]['cities'][$result->fields['serial_cit']]=$result->fields['name_cit'];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		//debug::print_r($arreglo);
		return $arreglo;
	}

        /***********************************************
        * funcion getCityAccessListByManager
        * gets all cities where a manager has access
        ***********************************************/
	function getCityAccessListByManager($serial_man){
		$sql =" SELECT DISTINCT cit.serial_cit, cit.name_cit, cit.serial_cou, cou.name_cou 
				FROM manager_by_country mbc
				JOIN country cou ON cou.serial_cou=mbc.serial_cou
				JOIN city cit ON cit.serial_cou=cou.serial_cou
				WHERE mbc.serial_mbc=".$serial_man;

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				//$arreglo[$cont]=$result->GetRowAssoc(false);
				$arreglo[$result->fields['serial_cou']]['name_cou']=$result->fields['name_cou'];
				$arreglo[$result->fields['serial_cou']]['cities'][$result->fields['serial_cit']]=$result->fields['name_cit'];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		//debug::print_r($arreglo);
		return $arreglo;
	}

                /***********************************************
        * funcion getCityAccessListByDealer
        * gets all cities where a dealer has access
        ***********************************************/
	function getCityAccessListByDealer($serial_dea){
		$sql = 	"SELECT cit.serial_cit,  cit.name_cit, cit.serial_cou, cou.name_cou
				FROM dealer dea
				JOIN sector sec
				ON dea.serial_sec=sec.serial_sec
				JOIN city cit
				ON cit.serial_cit=sec.serial_cit
				JOIN country cou
				ON cou.serial_cou=cit.serial_cou
                                WHERE dea.serial_dea=".$serial_dea;
                $sql.= " OR dea.dea_serial_dea=".$serial_dea;

		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		//print_r($result);
		//print_r($result->fields['serial_cit']);

		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				//$arreglo[$cont]=$result->GetRowAssoc(false);
				$arreglo[$result->fields['serial_cou']]['name_cou']=$result->fields['name_cou'];
				$arreglo[$result->fields['serial_cou']]['cities'][$result->fields['serial_cit']]=$result->fields['name_cit'];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		//debug::print_r($arreglo);
		return $arreglo;
	}
	
	/***********************************************
        * funcion getCityAccessListbyUser
        * gets the list of all cities where a user has access
        ***********************************************/
	function getCityAccessListbyUser($serial_usr){
		$sql = "SELECT cit.serial_cit, cit.name_cit, cit.serial_cou, cou.name_cou
				FROM user_by_city ubc
				JOIN city cit ON cit.serial_cit=ubc.serial_cit
				JOIN country cou ON cou.serial_cou=cit.serial_cou
				WHERE ubc.serial_usr=".$serial_usr;
		
                //echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$result->fields['serial_cou']]['name_cou']=$result->fields['name_cou'];
				$arreglo[$result->fields['serial_cou']]['cities'][$result->fields['serial_cit']]=$result->fields['name_cit'];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		//debug::print_r($arreglo);		
		return $arreglo;
	}

	/***********************************************
	 * @Name: getCitiesWithExcessPayments
	 * @Description: Retrieves a list of countries with excess payments to dispatch.
	 * @Params: DB connection
	 * @Returns: an array of countries.
    ***********************************************/
	public static function getCitiesWithExcessPayments($db,$serial_cou, $serial_mbc){
		$sql = 	"SELECT DISTINCT cit.serial_cit, cit.name_cit
				 FROM city cit 
				 JOIN sector sec ON sec.serial_cit=cit.serial_cit
				 JOIN dealer b ON b.serial_sec=sec.serial_sec
				 JOIN counter c ON c.serial_dea=b.serial_dea
				 JOIN sales s ON s.serial_cnt=c.serial_cnt
				 JOIN invoice i ON i.serial_inv=s.serial_inv AND i.status_inv='PAID'
				 JOIN payments p ON p.serial_pay=i.serial_pay AND p.status_pay='EXCESS' AND p.excess_amount_available_pay > 0
				 WHERE cit.serial_cou='$serial_cou'";
		if($serial_mbc!='1'){
			$sql.=" AND b.serial_mbc=".$serial_mbc;
		}
		$sql.="  ORDER BY name_cit";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/***********************************************
	 * @Name: getCitiesWithExcessPayments
	 * @Description: Retrieves a list of cities with excess payments to dispatch.
	 * @Params: DB connection
	 * @Returns: an array of cities.
    ***********************************************/
	public static function getCitiesWithFreeSales($db,$serial_cou, $serial_mbc){
		$sql = 	"SELECT DISTINCT c.serial_cit, c.name_cit
				 FROM city c 
				 JOIN sector s ON s.serial_cit=c.serial_cit
				 JOIN dealer d ON d.serial_sec=s.serial_sec
				 JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				 JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt AND sal.status_sal='REQUESTED'
				 WHERE c.serial_cou = ".$serial_cou;
		
		if($serial_mbc!='1'){
			$sql.=" AND d.serial_mbc=".$serial_mbc;
		}		
		
		$sql.="	 ORDER BY name_cit";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

		/**
	@Name: getCitiesWithFreeCard
	@Description: Retrieves the information of a specific city list
	@Params: Serial number of a country
	@Returns: City array
	**/
	function getCitiesWithFreeCard($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(cit.name_cit), cit.serial_cit
					FROM sales s
					   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					   JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
						LEFT JOIN invoice i ON i.serial_inv=s.serial_inv
					WHERE cit.serial_cou=".$serial_cou."
				";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY cit.name_cit";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getCitiesByInvoice
	@Description: Retrieves the information of a specific city list
	@Params: Serial number of a country
	@Returns: City array
	**/
	function getCitiesByInvoice($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql ="SELECT distinct(cit.name_cit), cit.serial_cit
			   FROM sales sal
			   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
			   JOIN dealer br ON cnt.serial_dea=br.serial_dea
				JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
				JOIN sector sec ON br.serial_sec=sec.serial_sec
			   JOIN city cit ON sec.serial_cit=cit.serial_cit
			   JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
			   WHERE sal.free_sal = 'YES'";

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY cit.name_cit";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}
	/**
	@Name: getCitiesWithRefunds
	@Description: Retrieves the information of a specific city list
	@Params: Serial number of a country
	@Returns: City array
	**/
	function getCitiesWithRefunds($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(cit.serial_cit), cit.name_cit
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					JOIN sector sec ON br.serial_sec=sec.serial_sec
				   JOIN city cit ON sec.serial_cit=cit.serial_cit
				   JOIN country cou ON cit.serial_cou=cou.serial_cou
				   JOIN refund ref ON ref.serial_sal=s.serial_sal
				   WHERE cou.serial_cou=".$serial_cou;
		
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		
		$sql.=" ORDER BY cit.name_cit";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

		/**
	@Name: getCitiesSalesAnalysis
	@Description: Retrieves the information of a specific city list
	@Params: Serial number of a country
	@Returns: City array
	**/
	function getCitiesSalesAnalysis($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(cit.serial_cit), cit.name_cit
					FROM sales sal
						JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
						JOIN dealer br ON cnt.serial_dea=br.serial_dea
						JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
      				    JOIN sector sec ON br.serial_sec=sec.serial_sec
      			       	JOIN city cit ON sec.serial_cit=cit.serial_cit AND cit.serial_cou=".$serial_cou."
      				    JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
      				    JOIN payments pay ON pay.serial_pay=inv.serial_pay
                    WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')  AND sal.serial_inv IS NOT NULL
                          AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'
                    ";
		if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY cit.name_cit";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/*
	 * @name: getCitiesWithBonus
	 * @param: $db - DB connection
	 * @return: Array with cities
	 */
	public static function getCitiesWithBonus($db, $serial_cou, $bonus_to = NULL, $at_least_one_paid = NULL, $authorized = NULL){
		if($at_least_one_paid) $one_paid = " JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb";

		$sql = "SELECT DISTINCT cit.serial_cit, cit.name_cit
				FROM city cit
				JOIN sector s ON s.serial_cit=cit.serial_cit
				JOIN dealer d ON d.serial_sec=s.serial_sec
				JOIN dealer b ON b.dea_serial_dea=d.serial_dea
				JOIN counter cnt ON cnt.serial_dea=b.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal
				$one_paid
				WHERE cit.serial_cou=".$serial_cou;

		if($bonus_to) $sql .= " AND apb.bonus_to_apb = '$bonus_to'";
		if($authorized) $sql.=" AND apb.ondate_apb = 'NO'
								AND apb.authorized_apb = 'YES'";

		$result=$db->getAll($sql);

		if($result){
				return $result;
		}else{
				return NULL;
		}
	}

	/**
	 * @name getGeneralCityByCountry
	 * @param $db DB_connection
	 * @param $serial_cou Country_ID
	 * @return city_row
	 */
	public static function getGeneralCityByCountry($db, $serial_cou){
		
        $sql = "SELECT serial_cit
				FROM city
				WHERE serial_cou = $serial_cou
				AND name_cit = 'General'";
		
		$result = $db->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}		
	}

	/*
	 * @name: getCitiesWithSales
	 * @param: $db - DB connection
	 * @return: Array with cities
	 */
	public static function getCitiesWithSales($db, $serial_mbc, $serial_dea, $serial_cou){

		$sql = "SELECT DISTINCT cit.serial_cit, cit.name_cit
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea 
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				WHERE cit.serial_cou = $serial_cou
				";

		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_cit";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: getCitiesWithAssignedDealers
	 * @param: $db - DB connection
	 * @return: Array with cities
	 */
	public static function getCitiesWithAssignedDealers($db, $serial_mbc, $serial_dea, $serial_cou){

		$sql = "SELECT DISTINCT cit.serial_cit, cit.name_cit
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				WHERE cit.serial_cou = $serial_cou
				";

		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_cit";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	function getAllCityAccessList(){
		$sql = 	"SELECT cit.serial_cit, cit.name_cit, cit.serial_cou, cou.name_cou
				FROM city cit
				JOIN country cou ON cou.serial_cou=cit.serial_cou
				AND cou.serial_cou <> 1
				ORDER BY name_cou";

		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		
		//print_r($result);
		//print_r($result->fields['serial_cit']);
		
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				//$arreglo[$cont]=$result->GetRowAssoc(false);
				$arreglo[$result->fields['serial_cou']]['name_cou']=$result->fields['name_cou'];
				$arreglo[$result->fields['serial_cou']]['cities'][$result->fields['serial_cit']]=$result->fields['name_cit'];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		//debug::print_r($arreglo);
		return $arreglo;
	}
        
        /***********************************************VA
    * funcion getCitiesByCountry
    * gets all the Cities in a specific country of the system
    ***********************************************/
	function getCityGeneralByCountry($db,$serial_cou){
		$sql =	"SELECT c.serial_cit
				FROM city c
				WHERE c.serial_cou='$serial_cou' AND name_cit like '%General%'";		
                
                //echo $sql; die();
	   	$result = $db->getOne($sql);
		
		if ($result){
                    return $result;
                }
                else{
                    return false;
                }                  	
	}

	///GETTERS
	function getSerial_cit(){
		return $this->serial_cit;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getName_cit(){
		return $this->name_cit;
	}
        function getCode_cit(){
		return $this->code_cit;
	}


	///SETTERS	
	function setSerial_cit($serial_cit){
		$this->serial_cit = $serial_cit;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setName_cit($name_cit){
		$this->name_cit = $name_cit;
	}
        function setCode_cit($code_cit){
		$this->code_cit = $code_cit;
	}
}
?>
