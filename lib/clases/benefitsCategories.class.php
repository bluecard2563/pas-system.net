<?php
/*
File: BenefitsCategories.class.php
Author: Luis Salvador.
Creation Date: 16/11/2015 

*/

class benefitsCategories{				
	var $db;
	var $serial_bcat;
        var $description_bcat;
        var $priority_bcat;
        var $status_bcat;

	function __construct($db, $serial_bcat=NULL,$description_bcat=NULL,$priority_bcat=NULL, $status_bcat=NULL){
		$this -> db = $db;
		$this -> serial_bcat = $serial_bcat;
                $this -> description_bcat = $description_bcat;
                $this -> priority_bcat = $priority_bcat;
                $this -> status_bcat = $status_bcat;
	}
	
        /**
	@Name: getData
        @Description: Retrieves the data of a Benefits Categories object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_bcat!=NULL){
			$sql = 	"SELECT bc.serial_bcat, bc.description_bcat, bc.priority_bcat, bc.status_bcat
				 FROM benefits_categories bc
				 WHERE bc.serial_bcat='".$this->serial_bcat."'";
                        //echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_bcat =$result->fields[0];
                                $this -> description_bcat =$result->fields[1];
                                $this -> priority_bcat =$result->fields[2];
				$this -> status_bcat = $result->fields[3];
			
				return true;
			}else{
				return false;
                        }
		}else
			return false;		
	}
	
        /**
	@Name: getData
        @Description: Retrieves the data of a Benefits Categories object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getBenefitsCategoriesList(){
		
			$sql = 	"SELECT *
				 FROM benefits_categories
                                 where status_bcat ='ACTIVE'";
				 
                        //echo $sql;
			$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		//Debug::print_r($arreglo); die();
		return $arreglo;
                
	}
	
	
	/**
	@Name: insert
	@Description: Inserts a register on DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO benefits_categories (
                      serial_bcat,
                      description_bcat,
                      priority_bcat,
                      status_bcat)
                VALUES (
                NULL,
				'".$this->description_bcat."',				
				'".$this->priority_bcat.",'ACTIVE')";
				
		//die($sql);
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error
	**/
        function update() {
            $sql = "UPDATE benefits_categories SET
                           description_bca ='".$this->description_bcat."',
			   priority_bcat ='".$this->priority_bcat."',
			   status_cat ='".$this->status_bcat."'
                    WHERE serial_bcat = '".$this->serial_bcat."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

	
	///GETTERS
	function getSerial_bcat(){
		return $this->serial_bcat;
	}
        function getDescription_bcat(){
		return $this->description_bcat;
	}
        function getPriority_bcat(){
		return $this->priority_bcat;
	}
	function getStatus_bcat(){
		return $this->status_bcat;
	}
	

	///SETTERS	
	function setSerial_bcat($serial_bcat){
		$this->serial_bcat = $serial_bcat;
	}
        function setDescription_bcat($description_bcat){
		$this->description_bcat = $description_bcat;
	}
        function setPriority_bcat($priority_bcat){
		$this->priority_bcat = $priority_bcat;
	}
	function setStatus_bcat($status_bcat){
		$this->status_bcat = $status_bcat;
	}
}
?>