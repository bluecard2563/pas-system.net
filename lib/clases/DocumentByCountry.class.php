<?php
/*
File: DocumentByCountry.class.php
Author: Edwin Salvador
Creation Date: 15/02/2010
Last Modified: 27/04/2010
Modified By: Patricio Astudillo
*/
class DocumentByCountry {
    var $db;
    var $serial_dbc;
    var $serial_cou;
    var $serial_doc;
    var $type_dbc;
	var $purpose_dbc;

    function __construct($db, $serial_dbc = NULL, $serial_cou = NULL, $serial_doc = NULL, $type_dbc = NULL, $purpose_dbc=NULL ){
            $this -> db = $db;
            $this -> serial_dbc = $serial_dbc;
            $this -> serial_cou = $serial_cou;
            $this -> serial_doc = $serial_doc;
            $this -> type_dbc = $type_dbc;
			$this -> purpose_dbc=$purpose_dbc;
    }

    /**
    @Name: getData
    @Description: Retrieves the data of a DocumentByCountry object.
    @Params: DocumentByCountry ID
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getData(){
        if($this->serial_dbc!=NULL){
            $sql = 	"SELECT serial_dbc, serial_cou, serial_doc, type_dbc, purpose_dbc
                            FROM document_by_country
                            WHERE serial_dbc='".$this->serial_dbc."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_dbc=$result->fields[0];
                $this->serial_cou=$result->fields[1];
                $this->serial_doc=$result->fields[2];
                $this->type_dbc=$result->fields[3];
				$this->purpose_dbc=$result->fields[4];

                return true;
            }
            else
                return false;

        }else
            return false;
    }

    /**
    @Name: getSerialByCountry
    @Description: Retrieves the serial of a DocumentByCountry object.
    @Params: serial_cou, type_dbc
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getSerialByCountry($serial_cou,$type_dbc,$purpose){
            $sql = 	"SELECT serial_dbc
					FROM document_by_country
					WHERE serial_cou='".$serial_cou."' AND (type_dbc='".$type_dbc."' OR type_dbc='BOTH') AND purpose_dbc='".$purpose."'";
            //die($sql);
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                return $result->GetRowAssoc(false);
            }
            else
                return false;
    }

    /**
    @Name: insert
    @Description: Inserts a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function insert(){
        $sql="INSERT INTO document_by_country (
                                    serial_dbc,
                                    serial_cou,
                                    serial_doc,
                                    type_dbc,
									purpose_dbc
                                )
                  VALUES(NULL,
                        '".$this->serial_cou."',
                        '".$this->serial_doc."',
                        '".$this->type_dbc."',
						'".$this->purpose_dbc."'
                        );";
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: update
    @Description: updates a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function update(){
        $sql="UPDATE document_by_country
        SET serial_cou='".$this->serial_cou."',
            serial_doc='".$this->serial_doc."',
            type_dbc='".$this->type_dbc."',
			purpose_dbc='".$this->purpose_dbc."'
        WHERE serial_dbc='".$this->serial_dbc."'";

        $result = $this->db->Execute($sql);
        //echo $sql." ";
        if ($result==true)
            return true;
        else
            return false;
    }

    /**
    @Name: delete
    @Description: delete a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function delete(){
        if($this->serial_doc!=NULL){
            $sql="DELETE FROM document_by_country
                  WHERE serial_doc='".$this->serial_doc."'
                  AND serial_dbc NOT IN (SELECT i.serial_dbc FROM invoice i)";

            $result = $this->db->Execute($sql);
            //echo $sql." ";
            if ($result==true)
                return true;
            else
                return false;
        }
    }

    /**
    @Name: documentByCountryExists
    @Description: verifies if a Document by country exists or not
    @Params: $serial_cou(Country ID), $serial_doc(DocumentType serial), $serial_dbc(Document by country serial).
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function documentByCountryExists($serial_cou, $serial_doc, $serial_dbc=NULL){
            $sql = 	"SELECT serial_dbc
					 FROM document_by_country
					 WHERE serial_cou = ".$serial_cou."
					 AND serial_doc = ".$serial_doc;
            if($serial_dbc!=NULL){
                    $sql.=" AND serial_dbc <> ".$serial_dbc;
            }
            $result = $this->db->Execute($sql);

            if($result->fields[0]){
                    return true;
            }else{
                    return false;
            }
    }

    /**
	@Name: getDocumentsByCountry
	@Description: Returns an array of documents.
	@Params: country ID
	@Returns: DocumentType array
	**/
	function getDocumentsByCountry($serial_cou, $purpose=NULL){
		$sql =	"SELECT *
				FROM document_by_country
				WHERE purpose_dbc='$purpose' AND serial_cou=".$serial_cou;
		
		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/**
	* @Name: getCreditNoteDocumentsByCountry
	* @Description: Returns an array of documents that are used as credit notes in a specific
	*				country.
	* @Params: country ID
	* @Returns: DocumentType array
	**/
	public static function getCreditNoteDocumentsByCountry($db, $serial_cou=NULL){
		$sql = "SELECT dbc.serial_dbc, dbc.type_dbc, dbc.serial_cou, c.name_cou, dt.name_doc
				FROM document_by_country dbc
				JOIN country c ON c.serial_cou=dbc.serial_cou
				JOIN document_type dt ON dt.serial_doc=dbc.serial_doc
				WHERE dbc.purpose_dbc='CREDIT_NOTE'";
		if($serial_cou!=NULL){
			$sql.=" AND dbc.serial_cou=".$serial_cou;
		}				
		$sql.=' ORDER BY c.name_cou ASC';
		
		$result = $db -> getAll($sql);

		if($result)
			return $result;
		else
			return false;
	}

    /**
    @Name: getAssignedCountries
    @Description: Retrieves the data of a DocumentByCountry object.
    @Params: DocumentType ID
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getAssignedCountries(){
        if($this->serial_doc!=NULL){
            $sql = 	"SELECT dbc.serial_dbc, c.serial_zon, dbc.serial_cou, dbc.serial_doc, dbc.type_dbc, dbc.purpose_dbc
					FROM document_by_country dbc
					JOIN country c ON c.serial_cou = dbc.serial_cou
					WHERE dbc.serial_doc='".$this->serial_doc."'";

            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;

        }else{
            return false;
        }
    }

    /**
    @Name: checkIsUsed_dbc
    @Description: verifies if a Document by country is being used
    @Params: $serial_dbc(Document by country ID)
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function checkIsUsed_dbc($serial_dbc){
        $sql = "SELECT serial_dbc
                 FROM invoice
                 WHERE serial_dbc = ".$serial_dbc;

        $result = $this->db->Execute($sql);

        if($result->fields[0]){
            return 1;
        }else{
            return 0;
        }
    }

	/**
    *@Name: similarDocTypeExists
    *@Description: verifies if there is a similar document type in a specific
	*			  country so that the insert will not be completed.
    *@Params: $type: Type of the document: LEGAL_ENTITY/PERSON/BOTH
    *@Returns: TRUE if O.K. / FALSE on errors
    **/
    function similarDocTypeExists($serial_dbc = NULL){
        $sql = "SELECT serial_dbc
                FROM document_by_country
                WHERE serial_cou = ".$this->serial_cou."
				AND purpose_dbc='".$this->purpose_dbc."'";
		
		if( $this->type_dbc == 'LEGAL_ENTITY' || $this->type_dbc == 'PERSON'){
			$sql .= " AND (type_dbc = '".$this->type_dbc."'
					OR type_dbc = 'BOTH')";
		}elseif( $this->type_dbc == 'BOTH' ){
			$sql .= " AND (type_dbc = 'LEGAL_ENTITY'
					OR type_dbc = 'PERSON'
					OR type_dbc = 'BOTH')";
		}
		
		if($serial_dbc){
			$sql.=" AND serial_dbc <> ".$serial_dbc;
		}

        $result = $this->db->Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }

	/*
	 * @Name:getCreditNoteDocument
	 * @Description: retrieves the serial of the document used as credit note in a specific cuntry.
	 * @Params: $serial_cou, $type_dbc
	 * @Returns: The Serial for the document_by_country
	 */
	function getCreditNoteDocument($serial_cou, $type_dbc){
		$sql="SELECT serial_dbc
			  FROM document_by_country dbc
			  WHERE serial_cou='".$serial_cou."'
		      AND (type_dbc = '".$type_dbc."' OR type_dbc='BOTH')
			  AND dbc.purpose_dbc = 'CREDIT_NOTE'";

		$result = $this->db -> Execute($sql);

		if($result){
			return $result->fields['0'];
		}else{
			return false;
		}
	}

	/*
	 * @Name:getCreditNoteDocument
	 * @Description: retrieves the serial of the document used as credit note in a specific cuntry.
	 * @Params: $serial_cou, $type_dbc
	 * @Returns: The Serial for the document_by_country
	 */
	public static function getPurposeValues_dbc($db){
		$sql = "SHOW COLUMNS FROM document_by_country LIKE 'purpose_dbc'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

        /*
	 * @Name:getDocumentTypeValues_dbc
	 * @Description: retrieves the serial of the document used as credit note in a specific cuntry.
	 * @Params: $serial_cou, $type_dbc
	 * @Returns: The Serial for the document_by_country
	 */
	public static function getDocumentTypeValues_dbc($db){
            $sql = "SHOW COLUMNS FROM document_by_country LIKE 'type_dbc'";
            $result = $db -> Execute($sql);

            $type=$result->fields[1];
            $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
            return $type;
	}

    ///GETTERS
    function getSerial_dbc(){
        return $this->serial_dbc;
    }
    function getSerial_cou(){
        return $this->serial_cou;
    }
    function getSerial_doc(){
        return $this->serial_doc;
    }
    function getType_dbc(){
        return $this->type_dbc;
    }
	function getPurpose_dbc(){
        return $this->purpose_dbc;
    }


    ///SETTERS
    function setSerial_dbc($serial_dbc){
        $this->serial_dbc = $serial_dbc;
    }
    function setSerial_cou($serial_cou){
        $this->serial_cou = $serial_cou;
    }
    function setSerial_doc($serial_doc){
        $this->serial_doc = $serial_doc;
    }
    function setType_dbc($type_dbc){
        $this->type_dbc = $type_dbc;
    }
	function setPurpose_dbc($purpose_dbc){
        $this->purpose_dbc=$purpose_dbc;
    }
}
?>