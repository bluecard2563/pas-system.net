<?php
/*
File: CustomLiquidationFunctions.class
Author: David Bergmann
Creation Date: 09/04/2010
Last Modified: 11/06/2010
Modified By: David Bergmann
*/

class CustomLiquidationFunctions {
	
	public static function getUsedProvidersInLiquidatedFiles($db){
		$sql = "SELECT spv.serial_spv, name_spv
				FROM service_provider spv
				JOIN service_provider_by_file spf ON spf.serial_spv = spv.serial_spv
				JOIN file f ON f.serial_fle = spf.serial_fle AND f.status_fle IN ('in_liquidation')
				GROUP BY spv.serial_spv";
		
		//Debug::print_r($sql);
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}			
	}
	
	public static function getLiquidationFilesForProvider($db, $serial_spv){
		$sql = "SELECT f.serial_fle, s.card_number_sal, CONCAT(c.first_name_cus, ' ', c.last_name_cus) AS 'pax',
						f.diagnosis_fle, DATE_FORMAT(incident_date_fle, '%d/%m/%Y') AS 'incident_date_fle'
				FROM service_provider spv
				JOIN service_provider_by_file spf ON spf.serial_spv = spv.serial_spv
					AND spv.serial_spv = $serial_spv
				JOIN file f ON f.serial_fle = spf.serial_fle AND status_fle = 'in_liquidation'
				
				JOIN customer c ON c.serial_cus = f.serial_cus
				JOIN sales s ON s.serial_sal = f.serial_sal
				GROUP BY f.serial_fle
				";
		
		//Debug::print_r($sql);
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}			
	}
	
}

