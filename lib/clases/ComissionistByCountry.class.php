<?php
/*
File: ComissionistByCountry.class.php
Author: David Bergmann
Creation Date: 03/02/2010
Last Modified: 17/03/2010
Modified By: David Bergmann
*/

class ComissionistByCountry {
	var $db;
	var $serial_cbc;
	var $serial_cou;
	var $serial_usr;
	var $com_serial_cbc;
	var $percentage_cbc;
	var $begin_date_cbc;
	var $end_date_cbc;
	var $status_cbc;
	var $last_comission_paid_cbc;

	
	function __construct($db, $serial_cbc = NULL, $serial_cou = NULL, $serial_usr = NULL,
                             $com_serial_cbc = NULL, $percentage_cbc = NULL, $begin_date_cbc = NULL,
                             $end_date_cbc = NULL, $status_cbc = NULL, $last_comission_paid_cbc = NULL){
		$this -> db = $db;
		$this -> serial_cbc = $serial_cbc;
		$this -> serial_cou = $serial_cou;
		$this -> serial_usr = $serial_usr;
		$this -> com_serial_cbc = $com_serial_cbc;
		$this -> percentage_cbc = $percentage_cbc;
                $this -> begin_date_cbc = $begin_date_cbc;
                $this -> end_date_cbc = $end_date_cbc;
                $this -> status_cbc = $status_cbclast_comission_paid_cbc;
                $this -> last_comission_paid_cbc = $last_comission_paid_cbc;
	}	
	
	/***********************************************
        * function getData
        * gets data by serial_cbc
        ***********************************************/
	function getData(){
		if($this->serial_cbc!=NULL){
                    $sql = 	"SELECT serial_cbc, serial_cou, serial_usr, com_serial_cbc, percentage_cbc,
                                    begin_date_cbc, end_date_cbc, status_cbc, last_comission_paid_cbc
                                 FROM comissionist_by_country
                                 WHERE serial_cbc='".$this->serial_cbc."'";
                    //echo $sql." ";
                    $result = $this->db->Execute($sql);
                    if ($result === false) return false;

                    if($result->fields[0]){
                        $this->serial_cbc=$result->fields[0];
                        $this->serial_cou=$result->fields[1];
                        $this->serial_usr=$result->fields[2];
                        $this->com_serial_cbc=$result->fields[3];
                        $this->percentage_cbc=$result->fields[4];
                        $this->begin_date_cbc=$result->fields[5];
                        $this->end_date_cbc=$result->fields[6];
                        $this->status_cbc=$result->fields[7];
                        $this->last_comission_paid_cbc=$result->fields[8];
                        return true;
                    }
                    else
                        return false;

            }else
                return false;
	}
	
	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){		
		$sql=   "INSERT INTO comissionist_by_country (
					serial_cbc,
					serial_cou, 
					serial_usr,
					com_serial_cbc,
					percentage_cbc,
                                        begin_date_cbc,
                                        end_date_cbc,
                                        status_cbc,
                                        last_comission_paid_cbc
					)
			  VALUES(NULL,
					'".$this->serial_cou."',
					'".$this->serial_usr."',";
                              if($this->com_serial_cbc == "") {
                                  $sql.="NULL,";
                              }
                              else {
                                  $sql.="'".$this->com_serial_cbc."',";
                              }
                                  $sql.="'".$this->percentage_cbc."',
                                        NOW(),
                                        null,
                                        '".$this->status_cbc."',
                                        NOW()
					);";
		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
 	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
		$sql=   "UPDATE comissionist_by_country
                         SET percentage_cbc='".$this->percentage_cbc."'
                         WHERE serial_cbc='".$this->serial_cbc."'";
					
		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
                    return true;
		else
                    return false;
	}
	
	/***********************************************
        * funcion getComissionistByCountrySerial
        * gets information from Users
        ***********************************************/
	function getComissionistsByCountrySerial($serial_usr){
		$lista=NULL;
		$sql = 	"SELECT cbc.serial_cbc
                         FROM comissionist_by_country cbc
                         WHERE cbc.serial_usr ='".$serial_usr."'
                         AND cbc.status_ccbc='ACTIVE'";
		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}
	
	/***********************************************
        * funcion getAssignedComissionistsByCountry
        * gets information from Users
        ***********************************************/
	function getComissionistsByCountry($serial_usr, $serial_mbc){
		$sql = 	"SELECT cbc.serial_usr,u.first_name_usr,u.last_name_usr
				 FROM comissionist_by_country cbc
				 JOIN user u ON u.serial_usr = cbc.serial_usr
				 WHERE cbc.serial_usr <>'".$serial_usr."'
				 AND cbc.status_cbc='ACTIVE'";

		if($serial_mbc!='1'){
			$sql.=" AND u.serial_mbc=".$serial_mbc;
		}

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}

        /***********************************************
        * funcion getActiveComissionistsByCountry
        * gets information from Users
        ***********************************************/
	function getActiveComissionistsByCountry($serial_cou){
		$sql = 	"SELECT cbc.serial_usr,u.first_name_usr,u.last_name_usr
                         FROM comissionist_by_country cbc
                         JOIN user u ON u.serial_usr = cbc.serial_usr
                         WHERE cbc.serial_cou ='".$serial_cou."'
                         AND cbc.status_cbc='ACTIVE'";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
                            $arr[$cont]=$result->GetRowAssoc(false);
                            $result->MoveNext();
                            $cont++;
			}while($cont<$num);
		}

		return $arr;
	}
	
	/***********************************************
        * funcion comissionistExists
        * verifies if a Comissionist exists or not
        ***********************************************/
	function comissionistExists($serial_usr, $serial_cou){
		$sql = 	"SELECT serial_usr
				 FROM comissionist_by_country
				 WHERE serial_usr = ".$serial_usr."
				 AND serial_cou = ".$serial_cou."
                                 AND status_cbc='ACTIVE'";
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}
        
        /**********************************************
        * funcion deactivateComissionistsByCountry
        * deactivates the comissionists that are not
        * in the parameter list
        **********************************************/
        function deactivateComissionistsByCountry($serial_cou,$userList){
            $sql = "UPDATE comissionist_by_country
                    SET status_cbc='INACTIVE', end_date_cbc=NOW()
                    WHERE serial_cou='".$serial_cou."'
                    AND status_cbc='ACTIVE'";
            if($userList){
                $sql .= "AND serial_usr NOT IN (".$userList.")";
            }
            //die($sql);
            $result = $this->db->Execute($sql);
            if ($result==true)
                return true;
            else
                return false;
        }

	///GETTERS
	function getSerial_cbc(){
		return $this->serial_cbc;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getCom_serial_cbc(){
		return $this->com_serial_cbc;
	}
	function getPercentage_cbc(){
		return $this->percentage_cbc;
	}
        function getBegin_date_cbc(){
		return $this->begin_date_cbc;
	}
        function getEnd_date_cbc(){
		return $this->end_date_cbc;
	}
        function getStatus_cbc(){
		return $this->status_cbc;
	}
        function getPast_comission_paid_cbc(){
		return $this->last_comission_paid_cbc;
	}
	
	///SETTERS	
	function setSerial_cbc($serial_cbc){
		$this->serial_cbc = $serial_cbc;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setCom_serial_cbc($com_serial_cbc){
		$this->com_serial_cbc = $com_serial_cbc;
	}
	function setPercentage_cbc($percentage_cbc){
		$this->percentage_cbc = $percentage_cbc;
	}
        function setBegin_date_cbc($begin_date_cbc){
		$this->begin_date_cbc = $begin_date_cbc;
	}
	function setEnd_date_cbc($end_date_cbc){
		$this->end_date_cbc = $end_date_cbc;
	}function setStatus_cbc($status_cbc){
		$this->status_cbc = $status_cbc;
	}
	function setLast_comission_paid_cbc($last_comission_paid_cbc){
		$this->last_comission_paid_cbc = $last_comission_paid_cbc;
	}
}
?>