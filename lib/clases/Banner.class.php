<?php
/*
File: Banner.php
Author: Santiago Borja Lopez
Creation Date: 17/02/2010 10:00
Last Modified: 23/02/2010
Modified By: Santiago Borja
*/
class Banner{				
	var $db;
	var $serial_ban;
	var $name_ban;
	var $url_ban;
		
	function __construct($db, $serial_ban=NULL, $name_ban = NULL, $url_ban=NULL){
		$this -> db = $db;
		$this -> serial_ban = $serial_ban;
		$this -> name_ban = $name_ban;
		$this -> url_ban = $url_ban;
	}
	
	/***********************************************
    * funcion getData
    * sets data by serial_ban
    ***********************************************/
	function getData(){
		if($this->serial_ban!=NULL){
				$sql = 	"SELECT serial_ban, name_ban, url_ban
						FROM banner
						WHERE serial_ban ='".$this->serial_ban."'";
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ban =$result->fields[0];
				$this -> name_ban = $result->fields[1];
				$this -> url_ban = $result->fields[2];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * funcion getAllBanners
    * gets information from All Banners
    ***********************************************/
    function getAllBanners(){
        $sql = 	"SELECT u.serial_ban, u.name_ban, u.url_ban FROM banner u ";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getMyCountryBanners
    * gets information from All Banners that are assigned to the country
    ***********************************************/
    function getMyCountryBanners($country){
        $sql = 	"SELECT u.serial_ban, u.name_ban, u.url_ban 
				FROM banner u 
				LEFT JOIN banner_by_country c 
				ON u.serial_ban = c.serial_ban 
				WHERE c.serial_cou = ".$country;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getArrayBannersByCountry
    * gets an array with the codes of All Banners of a country
    ***********************************************/
    public static function getArrayBannersByCountry($db, $countryCode){
        $sql = 	"SELECT serial_ban FROM banner_by_country WHERE serial_cou = ".$countryCode;
        $result = $db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr="";
            $cont=0;
            do{
                $temp_rec = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
                $arr.= $temp_rec['serial_ban'].($cont<$num?",":"");
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion removeBanner
    * removes one instance of banner
    ***********************************************/
    function removeBanner($banns_dir){
        $sql = 	"DELETE FROM banner_by_country WHERE serial_ban = ".$this->serial_ban;
        $sql2 = "DELETE FROM banner WHERE serial_ban = ".$this->serial_ban;
        unlink(DOCUMENT_ROOT.$banns_dir.$this->url_ban);
        $this->db -> Execute($sql);
        $this->db -> Execute($sql2);
    }
    
	/***********************************************
    * funcion getUnusedBanners
    * retrieves a list of all banners that are not activated on any country
    ***********************************************/
    static function getUnusedBanners($db){
       $sql = 	"SELECT DISTINCT(serial_ban) FROM banner_by_country WHERE 1";
        $result = $db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr="";
            $cont=0;
            do{
                $temp_rec = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
                $arr.= $temp_rec['serial_ban'].($cont<$num?",":"");
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion toggleBannerCountry
    * If the relationship banner_by_country is found, it is deleted, otherwise it is created
    * Returns the new activated value (true = the relationship exists)
    ***********************************************/
    function toggleBannerCountry($country_code){
        $sql = 	"SELECT serial_ban FROM banner_by_country WHERE serial_ban = ".$this->serial_ban." AND serial_cou = ".$country_code;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $sql2 = 	"DELETE FROM banner_by_country WHERE serial_ban = ".$this->serial_ban." AND serial_cou = ".$country_code;
        	$result2 = $this->db -> Execute($sql2);
        	return false;
        }else{
        	$sql2 = 	"INSERT INTO banner_by_country VALUES (NULL ,  ".$this->serial_ban.",  ".$country_code.")";
        	$result2 = $this->db -> Execute($sql2);
        	return true;
        }
    }
    
/***********************************************
    * funcion toggleBannerCountry
    * If the relationship banner_by_country is found, it is deleted, otherwise it is created
    * Returns the new activated value (true = the relationship exists)
    ***********************************************/
    public static function activateBannerAllCountries($db,$banner_id){
        $countries = Country::getWebCountries($db);
        foreach($countries as $cou){
	        $sql = 	"SELECT serial_ban FROM banner_by_country WHERE serial_ban = ".$banner_id." AND serial_cou = ".$cou['serial_cou'];
	        $result = $db -> Execute($sql);
	        $num = $result -> RecordCount();
	        if($num == 0){
	        	$sql2 = 	"INSERT INTO banner_by_country VALUES (NULL ,  ".$banner_id.",  ".$cou['serial_cou'].")";
	        	$result2 = $db -> Execute($sql2);
	        }
        }
        return true;
    }
	
	/***********************************************
    * function insert
    * inserts a new register in the DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO banner (
        		name_ban,
                url_ban)
            VALUES(	'".$this->name_ban."',
            		'".$this->url_ban."')";
       	//echo $sql;
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in the DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE banner SET";
        $sql.=" name_ban			='".$this->name_ban."',
        		url_ban				='".$this->url_ban."'";
        $sql .=" WHERE serial_ban 	='".$this->serial_ban."'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        return $result;
    }
	
	///GETTERS
	function getSerial_ban(){
		return $this->serial_ban;
	}
	function getName_ban(){
		return $this->name_ban;
	}
	function getUrl_ban(){
		return $this->url_ban;
	}

	///SETTERS	
	function setSerial_ban($serial_ban){
		$this->serial_ban = $serial_ban;
	}
	function setName_ban($name_ban){
		$this->name_ban = $name_ban;
	}
	
	function setUrl_ban($url_ban){
		$this->url_ban = $url_ban;
	}
}
?>