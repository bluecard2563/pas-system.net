<?php
/**
 * File: WSAccountsLib
 * Author: Patricio Astudillo
 * Creation Date: 12-nov-2012
 * Last Modified: 12-nov-2012
 * Modified By: Patricio Astudillo
 */

class WSAccountsLib{
	
	public static function registerWSTravel($db, $serial_cus, $serial_sal, $begin_date, $end_date, $serial_cit){
		//TODO
	}
	
	/**
	 * @name getMyAllTravels
	 * @param RSC $db
	 * @param int $serial_cus
	 * @param int $serial_lang
	 * @param boolean $current_trips
	 * @return boolean 
	 */
	public static function getMyAllTravels($db, $serial_cus, $serial_lang = 2, $current_trips = FALSE){
		if($current_trips) $current_clause = " AND s.status_sal IN ('ACTIVE', 'REGISTERED', 'STANDBY')";
		
		$sql = "(
					SELECT	s.serial_sal, DATE_FORMAT(s.begin_date_sal, '%Y-%m-%d') AS 'begin_date', 
							DATE_FORMAT(s.end_date_sal, '%Y-%m-%d') AS 'end_date', 
							name_pbl, name_cit, name_cou, pxc.serial_pxc, 
							pxc.serial_pro, s.status_sal, s.card_number_sal
					FROM sales s
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = $serial_lang
					JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro = 1
					LEFT JOIN city cit ON cit.serial_cit = s.serial_cit
					LEFT JOIN country cou ON cou.serial_cou = cit.serial_cou
					WHERE s.serial_cus = $serial_cus
					$current_clause
		
				) UNION (
					SELECT	tl.serial_sal, DATE_FORMAT(tl.start_trl, '%Y-%m-%d') AS 'bagin_date', 
							DATE_FORMAT(tl.end_trl, '%Y-%m-%d') AS 'end_date',
							name_pbl, name_cit, name_cou, pxc.serial_pxc, pxc.serial_pro, s.status_sal, 
							s.card_number_sal
					FROM traveler_log tl
					JOIN sales s ON s.serial_sal = tl.serial_sal
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = $serial_lang
					JOIN city cit ON cit.serial_cit = tl.serial_cit
					JOIN country cou ON cou.serial_cou = cit.serial_cou
					WHERE s.serial_cus = $serial_cus
					$current_clause
				)";
		
		//echo $sql;
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function destinationMonth($serial_lang = 2){
		return array(	'image' => 'http://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Aruba_Palm_Beach.JPG/250px-Aruba_Palm_Beach.JPG',
						'title' => 'El Paraiso de Aruba',
						'content' => '<b>Lorem Ipsum is simply</b> dummy text of the printing and typesetting industry. <br />
									Lorem Ipsum has been the industry`s standard dummy text ever since the 1500s, 
									when an unknown printer took a galley of type and scrambled it to make a type specimen book.
									It has survived not only five centuries, but also the leap into electronic typesetting, 
									remaining essentially unchanged. It was popularised in the 1960s with the release of 
									Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing 
									software like Aldus PageMaker including versions of Lorem Ipsum.',
						'url' => 'http://www.planet-assist.net');
	}
	
	public static function getBaseCountries($db){
		$sql .= "SELECT serial_cou, name_cou, IFNULL(phone_cou, 'N/A') AS 'phone_cou', 
						IF(phone_cou IS NOT NULL, 'YES', 'NO') AS 'is_contact', z.name_zon
                 FROM country c
				 JOIN zone z ON c.serial_zon = z.serial_zon
                                 where c.serial_cou not in (236)
                 ORDER BY name_cou";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getLoginInformationForWS($db, $username_as_md5){
		$sql = 	"SELECT *
				FROM user u
				WHERE md5(username_usr) = '$username_as_md5'";

		$result = $db->getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getAllPriceListForSerialPXC($db, $serial_pxc, $days = NULL) {
		$sql = "SELECT ppc.*
				FROM price_by_product_by_country ppc
				WHERE ppc.serial_pxc = $serial_pxc";
		
		if ($days) {
			$sql .= " AND ppc.duration_ppc BETWEEN " . ($days - 10) . " AND " . ($days + 10);
		}

		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			ErrorLog::log($db, 'WS PRICE LIST FAILED FOR PXC - '.$serial_pxc, $sql);
			return FALSE;
		}
	}

	/**
	 * 
	 * @param type $db
	 * @param type $serial_cou
	 * @return boolean
	 */
	public static function getCitiesForCountry($db, $serial_cou){
		$sql = "SELECT serial_cit, name_cit
				FROM city
				WHERE serial_cou = $serial_cou";

		$result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
	
	public static function getProductForAccount($db, $serial_dea, $withBenefitsAndPricing = 'NO', $language = 2, $massive = 'NO', $status = 'ACTIVE', $multiple_sales = NULL){
		$pbd = new ProductByDealer($db);
		
		$productListByDealer = $pbd->getproductByDealer($serial_dea, $language, $massive, $status);
		
		if($productListByDealer){
			$temp_array = array();
			$productSet = array();
			
			foreach($productListByDealer as $p){
				$temp_array['serial_pxc'] = $p['serial_pxc'];
				$temp_array['serial_pro'] = $p['serial_pro'];
				$temp_array['name'] = utf8_encode($p['name_pbl']);
				$temp_array['schengen'] = $p['schengen_pro'];
				$temp_array['free_adults'] = $p['adults_pro'];
				$temp_array['free_children'] = $p['children_pro'];
				$temp_array['totalExtrasInCard'] = $p['max_extras_pro'];
				$temp_array['allow_seniors'] = $p['senior_pro'];
				$temp_array['aditionalsRestrictedTo'] = $p['extras_restricted_to_pro'];
				
				if($withBenefitsAndPricing == 'YES'){
					$temp_array['benefits'] = self::getProductBenefits($db, $p['serial_pro'], $language);
					$temp_array['pricing'] = self::getProductPricing($db, $p['serial_pxc']);
				}
				
				array_push($productSet, $temp_array);
			}
			
			return $productSet;
		}else{
			return FALSE;
		}
	}
	
	public static function getProductPricing($db, $serial_pxc){		
		$prodByCou = new ProductByCountry($db, $serial_pxc);
		$prodByCou -> getData();
		$spousePrc = $prodByCou -> getPercentage_spouse_pxc();
		$adultsPrc = $prodByCou -> getPercentage_extras_pxc();
		
		$prices = self::getAllPriceListForSerialPXC($db, $serial_pxc);
		if($prices){
			$priceList = array();
			
			foreach($prices as $price){
				$priceList[] = array(	"det_days" => $price['duration_ppc'],
										"det_fare" => $price['price_ppc'],
										"det_spouse" => ((1 - $spousePrc/100) * $price['price_ppc'] ),
										"det_extra" => ((1 - $adultsPrc/100) * $price['price_ppc'] ));
			}
			
			return $priceList;
		}
		
		return FALSE;
	}
	
	public static function getProductBenefits($db, $serial_pro, $language){
		$informationList = array();
		
		$benefitsByProduct = new BenefitsByProduct($db); 
		$benefitsByProduct -> setSerial_pro($serial_pro);
		$benefits = $benefitsByProduct -> getMyBenefits($language);
		
		if($benefits){
			foreach($benefits as $benefit){
				$informationList[] = array(	"description" => utf8_encode($benefit['description_bbl']),
											"coverage" => $benefit['price_bxp'].$benefit['symbol_cur'],
											"restriction" => utf8_encode($benefit['description_rbl']),
											"restriction_amount" => $benefit['restriction_price_bxp'].$benefit['symbol_cur'],
											"condition" => utf8_encode($benefit['description_cbl']));
			}
			
			return $informationList;
		}
		
		return FALSE;
	}
}


?>
