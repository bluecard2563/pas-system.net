<?php
/*
File: Promo.class.php
Author: Patricio Astudillo
Creation Date: 01/02/2010 09:48
Last Modified: 11/03/2010
Modified By: Santiago Benitez
*/
class Promo{
	var $db;
	var $serial_prm;
	var $creation_date_prm;
	var $begin_date_prm;
	var $end_date_prm;
	var $type_prm;
	var $period_type_prm;
        var $period_days_prm;
	var $repeat_prm;
	var $type_sale_prm;
	var $notifications_prm;
	var $name_prm;
	var $descripction_prm;
        var $weekends_prm;
        var $repeat_type_prm;
        var $applied_to_prm;
        var $aux;

	function __construct($db, $serial_prm=NULL, $creation_date_prm=NULL, $begin_date_prm=NULL, $end_date_prm=NULL, $type_prm=NULL,
                            $repeat_prm=NULL, $type_sale_prm=NULL, $notifications_prm=NULL, $name_prm=NULL, $descripction_prm=NULL,
                            $weekends_prm=NULL, $repeat_type_prm=NULL, $applied_to_prm=NULL, $period_days_prm=NULL, $period_type_prm=NULL){

		$this -> db = $db;
		$this -> serial_prm=$serial_prm;
		$this -> creation_date_prm=$creation_date_prm;
		$this -> begin_date_prm=$begin_date_prm;
		$this -> end_date_prm=$end_date_prm;
		$this -> type_prm=$type_prm;
		$this -> repeat_prm=$repeat_prm;
		$this -> type_sale_prm=$type_sale_prm;
		$this -> notifications_prm=$notifications_prm;
		$this -> name_prm=$name_prm;
		$this -> descripction_prm=$descripction_prm;
                $this -> weekends_prm=$weekends_prm;
                $this -> repeat_type_prm=$repeat_type_prm;
                $this -> applied_to_prm=$applied_to_prm;
                $this -> period_type_prm=$period_type_prm;
                $this -> period_days_prm=$period_days_prm;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a Promo object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_prm!=NULL){
			$sql = 	"SELECT p.serial_prm, p.creation_date_prm, DATE_FORMAT(p.begin_date_prm, '%d/%m/%Y'),
                                        DATE_FORMAT(p.end_date_prm, '%d/%m/%Y'), p.type_prm, p.period_type_prm, p.period_days_prm, p.repeat_prm,
                                        p.type_sale_prm, p.notifications_prm, p.name_prm, p.description_prm, p.weekends_prm, p.repeat_type_prm,
                                        p.applied_to_prm, pbc.serial_cit, pbm.serial_mbc, pbcc.serial_cou
                                FROM promo p
                                LEFT JOIN promo_by_city pbc ON pbc.serial_prm = p.serial_prm
                                LEFT JOIN promo_by_mbc pbm ON pbm.serial_prm = p.serial_prm
                                LEFT JOIN promo_by_country pbcc ON pbcc.serial_prm = p.serial_prm
                                LEFT JOIN promo_by_dealer pbd ON pbd.serial_prm = p.serial_prm
                                WHERE p.serial_prm='".$this->serial_prm."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_prm=$result->fields[0];
				$this -> creation_date_prm=$result->fields[1];
				$this -> begin_date_prm=$result->fields[2];
                                $this -> end_date_prm=$result->fields[3];
                                $this -> type_prm=$result->fields[4];
                                $this -> period_type_prm=$result->fields[5];
                                $this -> period_days_prm=$result->fields[6];
				$this -> repeat_prm=$result->fields[7];
				$this -> type_sale_prm=$result->fields[8];
				$this -> notifications_prm=$result->fields[9];
				$this -> name_prm=$result->fields[10];
				$this -> descripction_prm=$result->fields[11];
                                $this -> weekends_prm=$result->fields[12];
                                $this -> repeat_type_prm=$result->fields[13];
                                $this -> applied_to_prm=$result->fields[14];
                                $this -> aux['city']=$result->fields[15];
                                $this -> aux['manager']=$result->fields[16];
                                $this -> aux['country']=$result->fields[17];
                                

				return true;
			}else
				return false;
		}else
			return false;
	}
        /**
	@Name: getDataDealers
	@Description: Retrieves the data of a PromoByDealer object.
	@Returns: PromoByDealer array
	**/
        function getDataDealers(){
		if($this->serial_prm!=NULL){
			$sql = 	"SELECT pbd.serial_dea
                                FROM promo_by_dealer pbd 
                                WHERE pbd.serial_prm='".$this->serial_prm."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			$num = $result -> RecordCount();
                        if($num > 0){
                                $array=array();
                                $cont=0;

                                do{
                                        $array[$cont]=$result->GetRowAssoc(false);
                                        $result->MoveNext();
                                        $cont++;
                                }while($cont<$num);
                        }
                        return $array;
                }
	}
        /**
	@Name: getDataPrizes
	@Description: Retrieves the data of a PrizeByPromo object.
	@Returns: PrizeByPromo array
	**/
        function getDataPrizes(){
		if($this->serial_prm!=NULL){
			$sql = 	"SELECT pbp.serial_pbp, pbp.serial_pri, pbp.starting_value_pbp, pbp.end_value_pbp
                                FROM prize_by_promo pbp
                                WHERE pbp.serial_prm='".$this->serial_prm."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			$num = $result -> RecordCount();
                        if($num > 0){
                                $array=array();
                                $cont=0;

                                do{
                                        $array[$cont]=$result->GetRowAssoc(false);
                                        $result->MoveNext();
                                        $cont++;
                                }while($cont<$num);
                        }
                        return $array;
                }
	}
        /**
	@Name: getDataProductByCountryByPrice
	@Description: Retrieves the data of a ProductByCountryByPrize object.
        @Params: prize_by_promo serial
	@Returns: ProductByCountryByPrize array
	**/
        function getDataProductByCountryByPrice($serial_pbp){
		if($serial_pbp!=NULL){
			$sql = 	"SELECT pbcbp.serial_pbp, pro.serial_pro
                                FROM product_by_country_by_prize pbcbp
                                JOIN product_by_country pxc ON pbcbp.serial_pxc = pxc.serial_pxc
                                JOIN product pro ON pro.serial_pro = pxc.serial_pro
                                WHERE pbcbp.serial_pbp='".$serial_pbp."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			$num = $result -> RecordCount();
                        if($num > 0){
                                $array=array();
                                $cont=0;

                                do{
                                        $array[$cont]=$result->GetRowAssoc(false);
                                        $result->MoveNext();
                                        $cont++;
                                }while($cont<$num);
                        }
                        return $array;
                }
	}

        /**
	@Name: getProductsByPromo
	@Description: Retrieves the products by promo
        @Params: serial_prm
	@Returns: Products array
	**/
        function getProductsByPromo(){
		if($this->serial_prm!=NULL){
			$sql = "SELECT DISTINCT pro.serial_pro, pbl.name_pbl
					FROM product_by_language pbl
					JOIN product pro ON pbl.serial_pro=pro.serial_pro
					JOIN product_by_country pxc ON pxc.serial_pro=pro.serial_pro
					JOIN product_by_country_by_prize pbcbp ON pbcbp.serial_pxc = pxc.serial_pxc
					JOIN prize_by_promo pbp ON pbp.serial_pbp = pbcbp.serial_pbp
					WHERE pbp.serial_prm = $this->serial_prm
					AND pbl.serial_lang = {$_SESSION['serial_lang']}";
					//die($sql);
			$result = $this->db->Execute($sql);
			$num = $result -> RecordCount();
                        if($num > 0){
                                $array=array();
                                $cont=0;

                                do{
                                        $array[$cont]=$result->GetRowAssoc(false);
                                        $result->MoveNext();
                                        $cont++;
                                }while($cont<$num);
                        }
                        return $array;
                }
	}


	/**
	@Name: promoNameExists
	@Description: Verifies if another promo exist with the same Name.
	@Params: $name_prm (Promo Name), $serial_prm (Promo serialID)
	@Returns: TRUE if exist. / FALSE no match
	**/
	function promoNameExists($name_prm, $serial_prm){
		$sql = 	"SELECT serial_prm
			 FROM promo
			 WHERE LOWER(name_prm) = utf8_ LOWER('".$name_prm."') COLLATE utf8_bin ";

		if($serial_prm!=NULL){
			$sql.=" AND serial_prm <> ".$serial_prm;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO promo (
                    serial_prm,
                    creation_date_prm,
                    begin_date_prm,
                    end_date_prm,
                    type_prm,
                    period_type_prm,
                    period_days_prm,
                    repeat_prm,
                    type_sale_prm,
                    notifications_prm,
                    name_prm,
                    description_prm,
                    weekends_prm,
                    repeat_type_prm,
                    applied_to_prm)
                VALUES (
                    NULL,
                    CURDATE(),
                    STR_TO_DATE('".$this->begin_date_prm."','%d/%m/%Y'),
                    STR_TO_DATE('".$this->end_date_prm."','%d/%m/%Y'),
                    '".$this->type_prm."',
                    '".$this->period_type_prm."',
                    '".$this->period_days_prm."',
                    '".$this->repeat_prm."',
                    '".$this->type_sale_prm."',
                    '".$this->notifications_prm."',
                    '".$this->name_prm."',
                    '".$this->descripction_prm."',
                    '".$this->weekends_prm."',
                    '".$this->repeat_type_prm."',
                    '".$this->applied_to_prm."')";
            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "UPDATE promo SET
                    creation_date_prm=CURDATE(),
                    begin_date_prm=STR_TO_DATE('".$this->begin_date_prm."','%d/%m/%Y'),
                    end_date_prm= STR_TO_DATE('".$this->end_date_prm."','%d/%m/%Y'),
                    type_prm='".$this->type_prm."',
                    period_type_prm='".$this->period_type_prm."',
                    period_days_prm='".$this->period_days_prm."',
                    repeat_prm='".$this->repeat_prm."',
                    type_sale_prm='".$this->type_sale_prm."',
                    notifications_prm='".$this->notifications_prm."',
                    name_prm='".$this->name_prm."',
                    description_prm='".$this->descripction_prm."',
                    weekends_prm='".$this->weekends_prm."',
                    repeat_type_prm='".$this->repeat_type_prm."',
                    applied_to_prm='".$this->applied_to_prm."'
            WHERE serial_prm = '".$this->serial_prm."'";
            //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }

	/**
	@Name: getPromoAutocompleter
	@Description: Returns an array of promos.
	@Params: Promo name or pattern.
	@Returns: Dealer array
	**/
	function getPromoAutocompleter($name_prm){
		$sql = "SELECT serial_prm, name_prm
			FROM promo
			WHERE LOWER(name_prm) LIKE _utf8 LOWER('%".$name_prm."%') COLLATE utf8_bin
			LIMIT 10";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
	}

        /**
	* @Name: getPromoAutocompleter
	* @Description: Returns an array of promos.
	* @Params: $name_prm: Promo name or pattern.
        *          $beginDate_prm: Begin Date.
        *          $endDate_prm:End Date
	* @Returns: TRUE if exists / FALSE if not.
	**/
	function checkNameBetweenDates($name_prm, $beginDate_prm, $endDate_prm, $serial_prm=NULL){
		$sql = "SELECT serial_prm
			FROM promo
			WHERE LOWER(name_prm) = _utf8 '".$name_prm."' COLLATE utf8_bin
			AND (   begin_date_prm between STR_TO_DATE('".$beginDate_prm."','%d/%m/%Y') AND STR_TO_DATE('".$endDate_prm."','%d/%m/%Y') OR
					end_date_prm between STR_TO_DATE('".$beginDate_prm."','%d/%m/%Y') AND STR_TO_DATE('".$endDate_prm."','%d/%m/%Y') OR
					STR_TO_DATE('".$beginDate_prm."','%d/%m/%Y') BETWEEN begin_date_prm AND end_date_prm OR
					STR_TO_DATE('".$endDate_prm."','%d/%m/%Y') BETWEEN begin_date_prm AND  end_date_prm )";

		if($serial_prm!=NULL and $serial_prm!=''){
			$sql.=" AND serial_prm <> ".$serial_prm;
		}

		$result = $this->db -> Execute($sql);

                if($result->fields['0'])
                    return true;
                else
                    return false;
	}

	/*
	 * @name: getAvailablePrizesForDealerInAPromo
	 * @returns: An array of available prizes for a dealer's sales.
	 */
	public static function getPromoWinners($db, $serial_prm, $applied_to_prm, $repeat, $period_type, $finalDays, 
										$type_prm, $weekend_prm, $repeat_type_prm, $pxc_serials, $serial_dea=NULL, 
										$previousWinners=NULL){
//		$debug_string = "Serial_prm: ".$serial_prm."<br>";
//		$debug_string .= "applied_to_prm: ".$applied_to_prm."<br>";
//		$debug_string .= "repeat: ".$repeat."<br>";
//		$debug_string .= "period_type: ".$period_type."<br>";
//		$debug_string .= "finalDays: ".$finalDays."<br>";
//		$debug_string .= "type_prm: ".$type_prm."<br>";
//		$debug_string .= "weekend_prm: ".$weekend_prm."<br>";
//		$debug_string .= "repeat_type_prm: ".$repeat_type_prm."<br>";
//		$debug_string .= "serial_dea: ".$serial_dea."<br>";
//		$debug_string .= "pxc_serials: ".$pxc_serials."<br>";
//		$debug_string .= "previousWinners: ".$previousWinners."<br>";
//		echo $debug_string;
		
		if($type_prm == 'AMOUNT'){ //SET SALES AMOUNTS SPECIFICLY TO A VALUE
			$pbp = new PrizeByPromo($db);
			$prizes = $pbp->getPrizesByPromoToReclaim($serial_prm, $type_prm);
			
			//GET JUST THE FIRST PRIZE AND WORK WITH IT
			if($prizes){
				$amount_filter = " AND s.total_sal = {$prizes[0]['starting_value_pbp']}";
			}
			
		}
		
		/*Promo Applied To Section*/
		switch($applied_to_prm){
			case 'COUNTRY':
				$joinSection="  JOIN promo_by_country pbcou ON pbcou.serial_prm=p.serial_prm
								JOIN country c ON c.serial_cou=pbcou.serial_cou
								JOIN city cit ON cit.serial_cou=c.serial_cou
								JOIN sector sec ON sec.serial_cit=cit.serial_cit
								JOIN dealer dea ON dea.serial_sec=sec.serial_sec ";
				break;
			case 'CITY':
				$joinSection="  JOIN promo_by_city pbc ON pbc.serial_prm=p.serial_prm
								JOIN city cit ON cit.serial_cit=pbc.serial_cit
								JOIN sector sec ON sec.serial_cit=cit.serial_cit
								JOIN dealer dea ON dea.serial_sec=sec.serial_sec ";
				break;
			case 'MANAGER':
				$joinSection="  JOIN promo_by_mbc pbm ON pbm.serial_prm=p.serial_prm
								JOIN manager_by_country mbc ON mbc.serial_mbc=pbm.serial_mbc
								JOIN country c ON c.serial_cou=mbc.serial_cou
								JOIN city cit ON cit.serial_cou=c.serial_cou
								JOIN sector sec ON sec.serial_cit=cit.serial_cit
								JOIN dealer dea ON dea.serial_sec=sec.serial_sec AND dea.serial_mbc=mbc.serial_mbc ";
				break;
			case 'DEALER':
				$joinSection="  JOIN promo_by_dealer pbd ON pbd.serial_prm=p.serial_prm
								JOIN dealer dea ON dea.serial_dea=pbd.serial_dea
								JOIN sector sec ON sec.serial_sec=dea.serial_sec
								JOIN city cit ON cit.serial_cit=sec.serial_cit
								JOIN country c ON c.serial_cou=cit.serial_cou ";
				break;
		}
		/*END applied to section*/

		/*REPEAT TYPE SECTION*/
		if($repeat=='1'){ //If the promo repeats itself.
			/*PERIOD TYPE*/
			switch($period_type){
				case 'ONE_DAY':
					if($repeat_type_prm=='WEEKLY'){
						$repeatFilter=" AND DAYOFWEEK(s.emission_date_sal) = p.period_days_prm";
					}else{
						$repeatFilter=" AND DAYOFMONTH(s.emission_date_sal) = p.period_days_prm";
					}
					break;
				case 'RANDOM':
					if($repeat_type_prm=='WEEKLY'){
						$repeatFilter=" AND DAYOFWEEK(s.emission_date_sal) = p.period_days_prm";
					}else{
						$repeatFilter=" AND DAYOFMONTH(s.emission_date_sal) = p.period_days_prm";
					}
					break;
				case 'SEVERAL DAYS':
					if($repeat_type_prm=='WEEKLY'){
						$repeatFilter=" AND DAYOFWEEK(s.emission_date_sal) IN (".$finalDays.")";
					}else{
						$repeatFilter=" AND DAYOFMONTH(s.emission_date_sal) IN (".$finalDays.")";
					}
					break;
			}
			/*END PERIOD TYPE*/
		}else{//If the promo doesn'd repeat.
			$repeatFilter=" AND STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%Y-%m-%d'), '%Y-%m-%d') = STR_TO_DATE('".$finalDays."', '%d/%m/%Y')";
		}
		/*END REPEAT TYPE*/

		if($serial_dea!='' && $pxc_serials!=''){
			$sql="SELECT SUM(s.total_sal) AS 'amount_sold', i.applied_taxes_inv, (i.other_dscnt_inv+discount_prcg_inv) AS 'discounts', pxc.serial_pxc";
		}else{
			$sql="SELECT s.serial_sal, s.serial_inv, s.total_sal AS 'amount_available', pxc.serial_pxc,
				         b.serial_dea, b.name_dea, i.applied_taxes_inv, (i.other_dscnt_inv+discount_prcg_inv) AS 'discounts'";
		}
		
		$sql.="   FROM promo p ".$joinSection."
				  JOIN dealer b ON b.dea_serial_dea=dea.serial_dea
				  JOIN counter cnt ON cnt.serial_dea=b.serial_dea
				  JOIN sales s ON s.serial_cnt=cnt.serial_cnt AND s.status_sal NOT IN ('VOID','REFUNDED','BLOCKED','DENIED','REQUESTED')
				  JOIN invoice i ON s.serial_inv=i.serial_inv AND i.status_inv = 'PAID'
				  JOIN payments pay ON pay.serial_pay=i.serial_pay AND STR_TO_DATE(DATE_FORMAT(pay.date_pay, '%Y-%m-%d'),'%Y-%m-%d') <= p.end_date_prm
				  JOIN product_by_dealer pbde ON pbde.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbde.serial_pxc
				  WHERE p.serial_prm=".$serial_prm."
				  AND STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%Y-%m-%d'), '%Y-%m-%d') BETWEEN p.begin_date_prm AND p.end_date_prm 
				  $repeatFilter
				  $amount_filter
				  AND pxc.serial_pxc IN ($pxc_serials)";

		//Exlcluding weekends if they're not allowed.
		if($weekend_prm=='NO'){
			$sql.=" AND DAYOFWEEK(s.emission_date_sal) NOT IN (1,2)";
		}

		if($previousWinners!=''){
			$sql.=" AND b.serial_dea NOT IN (".$previousWinners.")";
		}

		if($serial_dea!='' && $pxc_serials!=''){
			$sql.=" AND b.serial_dea=".$serial_dea;
		}else{
			$sql.=" ORDER BY b.name_dea, b.serial_dea";
		}
		//Debug::print_r($sql); die;
		
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * getUsedAmountsInAPromo
	 * @param: $db - DB connection
	 * @param: $serial_dea - Dealer ID
	 * @param: $serial_prm - Promo ID
	 * @return: An array of items reclaimed in promo.
	 */
	public static function getUsedAmountsInAPromo($db, $serial_dea, $serial_prm){
		$sql="SELECT pw.serial_prm, pw.serial_pbp, pw.serial_dea, pw.amount_prw, DATE_FORMAT(pw.date_prw , '%Y/%m/%d') AS 'date_prw'
			  FROM promo p
			  JOIN promo_winner pw ON pw.serial_prm=p.serial_prm AND pw.serial_dea='".$serial_dea."'
			  WHERE p.serial_prm='".$serial_prm."'";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * @name: getAvailablePromos
	 * @param: $serial - element ID
	 * @param: $serial_dea - Dealer ID
	 * @returns: An array of available prizes for a dealer's sales.
	 */
	public static function getAvailablePromos($db, $applied_to_prm){
		/*Promo Applied To Section*/
		switch($applied_to_prm){
			case 'COUNTRY':
				$joinSection="  JOIN promo_by_country pbcou ON pbcou.serial_prm=p.serial_prm
								JOIN country c ON c.serial_cou=pbcou.serial_cou
								JOIN city cit ON cit.serial_cou=c.serial_cou
								JOIN sector sec ON sec.serial_cit=cit.serial_cit
								JOIN dealer dea ON dea.serial_sec=sec.serial_sec ";
				break;
			case 'CITY':
				$joinSection="  JOIN promo_by_city pbc ON pbc.serial_prm=p.serial_prm
								JOIN city cit ON cit.serial_cit=pbc.serial_cit
								JOIN sector sec ON sec.serial_cit=cit.serial_cit
								JOIN dealer dea ON dea.serial_sec=sec.serial_sec ";
				break;
			case 'MANAGER':
				$joinSection="  JOIN promo_by_mbc pbm ON pbm.serial_prm=p.serial_prm
								JOIN manager_by_country mbc ON mbc.serial_mbc=pbm.serial_mbc
								JOIN country c ON c.serial_cou=mbc.serial_cou
								JOIN city cit ON cit.serial_cou=c.serial_cou
								JOIN sector sec ON sec.serial_cit=cit.serial_cit
								JOIN dealer dea ON dea.serial_sec=sec.serial_sec AND dea.serial_mbc=mbc.serial_mbc ";
				break;
			case 'DEALER':
				$joinSection="  JOIN promo_by_dealer pbd ON pbd.serial_prm=p.serial_prm
								JOIN dealer dea ON dea.serial_dea=pbd.serial_dea
								JOIN sector sec ON sec.serial_sec=dea.serial_sec
								JOIN city cit ON cit.serial_cit=sec.serial_cit
								JOIN country c ON c.serial_cou=cit.serial_cou ";
				break;
		}
		/*END applied to section*/

		$sql="SELECT DISTINCT p.*
			  FROM promo p ".$joinSection."
			  WHERE CURDATE() > p.end_date_prm
			  AND p.serial_prm NOT IN (SELECT DISTINCT pw.serial_prm
									   FROM promo_winner pw
									   WHERE pw.serial_prm=p.serial_prm)";
		//Debug::print_r($sql);
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/**
	@Name: getStatusList
	@Description: Gets all promo types in DB.
	@Params: N/A
	@Returns: Types array
	**/
	function getTypeList(){
		$sql = "SHOW COLUMNS FROM promo LIKE 'type_prm'";
               // echo $sql;
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	@Name: getPeriodType
	@Description: Gets all period types in DB.
	@Params: N/A
	@Returns: Period Types array
	**/
	function getPeriodType(){
		$sql = "SHOW COLUMNS FROM promo LIKE 'period_type_prm'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	@Name: getTypeSale
	@Description: Gets all sale types in DB.
	@Params: N/A
	@Returns: Type Sale array
	**/
	function getTypeSale(){
		$sql = "SHOW COLUMNS FROM promo LIKE 'type_sale_prm'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

        /**
	@Name: getRepeatType
	@Description: Gets all repeat types in DB.
	@Params: N/A
	@Returns: Repeat Sale array
	**/
	function getRepeatType(){
		$sql = "SHOW COLUMNS FROM promo LIKE 'repeat_type_prm'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

        /**
	@Name: getAppliedType
	@Description: Gets all appliedTo types in DB.
	@Params: N/A
	@Returns: Applied To array
	**/
	function getAppliedType(){
		$sql = "SHOW COLUMNS FROM promo LIKE 'applied_to_prm'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	//Getters
	function getSerial_prm(){
		return $this->serial_prm;
	}
	function getCreationDate_prm(){
		return $this->creation_date_prm;
	}
	function getBeginDate_prm(){
		return $this->begin_date_prm;
	}
        function getEndDate_prm(){
		return $this->end_date_prm;
	}
	function getType_prm(){
		return $this->type_prm;
	}
	function getPeriodType_prm(){
		return $this->period_type_prm;
	}
        function getPeriodDays_prm(){
		return $this->period_days_prm;
	}
	function getRepeat_prm(){
		return $this->repeat_prm;
	}
	function getTypeSale_prm(){
		return $this->type_sale_prm;
	}
	function getNotifications_prm(){
		return $this->notifications_prm;
	}
	function getName_prm(){
		return $this->name_prm;
	}
	function getDescripction_prm(){
		return $this->descripction_prm;
	}
        function getWeekends_prm(){
		return $this->weekends_prm;
	}
        function getRepeatType_prm(){
		return $this->repeat_type_prm;
	}
        function getAppliedTo_prm(){
		return $this->applied_to_prm;
	}
        function getAux_prm(){
		return $this->aux;
	}

	//Setters
	function setSerial_prm($serial_prm){
		$this->serial_prm=$serial_prm;
	}
	function setCreationDate_prm($creation_date_prm){
		$this->creation_date_prm=$creation_date_prm;
	}
        function setEndDate_prm($end_date_prm){
		$this->end_date_prm=$end_date_prm;
	}
	function setBeginDate_prm($begin_date_prm){
		$this->begin_date_prm=$begin_date_prm;
	}
	function setType_prm($type_prm){
		$this->type_prm=$type_prm;
	}
	function setPeriodType_prm($period_type_prm){
		$this->period_type_prm=$period_type_prm;
	}
        function setPeriodDays_prm($period_days_prm){
		$this->period_days_prm=$period_days_prm;
	}
	function setRepeat_prm($repeat_prm){
		$this->repeat_prm=$repeat_prm;
	}
	function setTypeSale_prm($type_sale_prm){
		$this->type_sale_prm=$type_sale_prm;
	}
	function setNotifications_prm($notifications_prm){
		$this->notifications_prm=$notifications_prm;
	}
	function setName_prm($name_prm){
		$this->name_prm=$name_prm;
	}
	function setDescripction_prm($descripction_prm){
		$this->descripction_prm=$descripction_prm;
        }
        function setWeekends_prm($weekends_prm){
		$this->weekends_prm=$weekends_prm;
	}
        function setRepeatType_prm($repeat_type_prm){
		$this->repeat_type_prm=$repeat_type_prm;
	}
        function setAppliedTo_prm($appliedTo){
		$this->applied_to_prm=$appliedTo;
	}
}
?>