<?php
/*
File: Contracts.class
Author: David Rosales
Creation Date: 28/08/2015
Last modified:
Modified By: 
*/

class Contracts {
    protected $db;
    protected $serial_con;
    protected $serial_cof;
    protected $serial_cus;
    protected $number_con;
    protected $begin_date_con;
    protected $end_date_con;
    protected $status_con;
    
    function __construct($db, $serial_con) {
        $this->db = $db;
        $this->serial_con = $serial_con;
        
        if ($this->serial_con){
            $this->getData();
        }
    }
    
    function getSerial_con() {
        return $this->serial_con;
    }

    function getSerial_cof() {
        return $this->serial_cof;
    }

    function getSerial_cus() {
        return $this->serial_cus;
    }
    
    function getNumber_con() {
        return $this->number_con;
    }

    function getBegin_date_con() {
        return $this->begin_date_con;
    }

    function getEnd_date_con() {
        return $this->end_date_con;
    }

    function getStatus_con() {
        return $this->status_con;
    }

    
    
    function setSerial_cof($serial_cof) {
        $this->serial_cof = $serial_cof;
    }

    function setSerial_cus($serial_cus) {
        $this->serial_cus = $serial_cus;
    }
    
    function setNumber_con($number_con) {
        $this->number_con = $number_con;
    }

    function setBegin_date_con($begin_date_con) {
        $this->begin_date_con = $begin_date_con;
    }

    function setEnd_date_con($end_date_con) {
        $this->end_date_con = $end_date_con;
    }

    function setStatus_con($status_con) {
        $this->status_con = $status_con;
    }

    /*
     * gets Contract Data
     * returns true or false
     * 
     */
	function getData() {
		if ($this->serial_con != NULL) {
			$sql = "SELECT serial_con,
                        serial_cof,
                        serial_cus,
                        number_con,
                        begin_date_con,
                        end_date_con,
                        status_con
                    FROM contracts
                    WHERE serial_con =" . $this->serial_con;
			$result = $this->db->Execute($sql);
            
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_con = $result->fields[0];
				$this->serial_cof = $result->fields[1];
				$this->serial_cus = $result->fields[2];
                $this->number_con = $result->fields[3];
				$this->begin_date_con = $result->fields[4];
				$this->end_date_con = $result->fields[5];
				$this->status_con = $result->fields[6];
                
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
    
    
    function insert() {
		$sql = "INSERT INTO contracts (
                                serial_cof,
                                serial_cus,
                                number_con,
                                begin_date_con,
                                end_date_con,
                                status_con)
                VALUES( '" . $this->serial_cof . "',
                        '" . $this->serial_cus . "',
                        '" . $this->number_con . "',
                STR_TO_DATE('" . $this->begin_date_con . "','%d/%m/%Y'),
                STR_TO_DATE('" . $this->end_date_con . "','%d/%m/%Y'),
                '" . $this->status_con . "')";
//        Debug::print_r($sql);die();
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
    }
    
    
    function update() {
		$sql = "UPDATE contracts SET
                    serial_cof = " . $this->serial_cof . ",
			        serial_cus = " . $this->serial_cus . ",
                    nnumber_con = " . $this->number_con . ",
                    begin_date_con = STR_TO_DATE('" . $this->begin_date_con . "','%d/%m/%Y'),
                    end_date_con = STR_TO_DATE('" . $this->end_date_con . "','%d/%m/%Y'),
                    status_con = '" . $this->status_con . "'
                WHERE serial_con = " . $this->serial_con;
		$result = $this->db->Execute($sql);

		if ($result) {
			return true;
		} else {
			ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
			return false;
		}
	}
    
    
    static function getContractByCustomer($db, $serial_cus, $status_con) {
        $sql = "SELECT serial_con, serial_cof, serial_cus, number_con,
                DATE_FORMAT(begin_date_con, '%d/%m/%Y') AS 'begin_date', DATE_FORMAT(end_date_con, '%d/%m/%Y') AS 'end_date'
                FROM contracts
                WHERE serial_cus = $serial_cus AND status_con = '$status_con'
                ORDER BY begin_date_con";
        
        $result = $db->getAll($sql);
        
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
}