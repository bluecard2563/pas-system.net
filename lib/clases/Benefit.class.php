<?php
/*
File: Benefit.class.php
Author: Patricio Astudillo M.
Creation Date: 28/12/2009 11:48
Last Modified: 29/12/2009
Modified By: Patricio Astudillo
*/

class Benefit{				
	var $db;
	var $serial_ben;
        var $serial_bcat;
	var $status_ben;
        var $weight_ben;

	function __construct($db, $serial_ben=NULL,$serial_bcat=NULL, $status_ben=NULL, $weight_ben=NULL ){
		$this -> db = $db;
		$this -> serial_ben = $serial_ben;
                $this -> serial_bcat = $serial_bcat;
		$this -> status_ben = $status_ben;
                $this -> weight_ben = $weight_ben;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
	function getData(){
		if($this->serial_ben!=NULL){
			$sql = 	"SELECT serial_ben,serial_bcat ,status_ben, weight_ben
					FROM benefits
					WHERE serial_ben='".$this->serial_ben."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ben =$result->fields[0];
                                $this -> serial_bcat =$result->fields[1];
				$this -> status_ben = $result->fields[2];
                                $this -> weight_ben = $result->fields[3];

				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * funcion getData
    * verifies if a Benefit exists or not
    ***********************************************/
/*	function benefitExists($description_ben, $serial_ben=NULL){
		$sql = 	"SELECT serial_ben
				 FROM benefits 
				 WHERE LOWER(description_ben)='".$description_ben."'";
		if($serial_ben!=NULL){
			$sql.=" AND serial_ben <> ".$serial_ben;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}*/
	
	/***********************************************
    * funcion getBenefits
    * gets all the Benefits of the system
    ***********************************************/
	function getBenefits($code_lang){
		$lista=NULL;
		$sql = 	"SELECT b.serial_ben, b.serial_bcat, bbl.description_ben, b.status_ben
			 FROM benefits b
                         JOIN benefits_by_languaje bbl ON bbl.serial_ben = b.serial_ben
                         JOIN language l ON l.serial_lang=bbl.serial_lang AND l.code_lang=".$code_lang;

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}

	/** 
	@Name: getBenefitsAutocompleter
	@Description: Returns an array of benefits.
	@Params: Benefit name or pattern.
	@Returns: Benefits array
	**/
	function getBenefitsAutocompleter($description_bbl, $serial_lang){
		$sql = "SELECT b.serial_ben,b.serial_bcat ,bbl.description_bbl
			FROM benefits b
			JOIN benefits_by_language bbl ON b.serial_ben = bbl.serial_ben AND bbl.serial_lang = '".$serial_lang."'
			WHERE LOWER(description_bbl) LIKE _utf8'%".utf8_encode($description_bbl)."%' collate utf8_bin
			LIMIT 10";
//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	

        /***********************************************
        * funcion getActiveBenefits
        * gets all the Benefits wich status is Active of the system
        ***********************************************/
	function getActiveBenefits($code_lang){
		$lista=NULL;
		$sql = 	"SELECT b.serial_ben, bbl.description_bbl, b.status_ben
			 FROM benefits b
			 JOIN benefits_by_language bbl ON b.serial_ben = bbl.serial_ben
             JOIN language l ON l.serial_lang=bbl.serial_lang AND l.code_lang='".$code_lang."'
			 WHERE b.status_ben='ACTIVE'
                         ORDER BY b.serial_ben";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
			
			
		}
		return $arr;
	}
	
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO benefits (
				serial_ben,
                                serial_bcat,
				status_ben,
                                weight_ben)
            VALUES (
                NULL,
                '".$this->serial_bcat."',
				'".$this->status_ben."',
                                ".$this->weight_ben.")";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE benefits SET
                serial_bcat='".$this->serial_bcat."',
					status_ben ='".$this->status_ben."'
				WHERE serial_ben = '".$this->serial_ben."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }

 	/***********************************************
    * funcion updateWeight
    * updates a register in DB
    ***********************************************/
    function updateWeight() {
        $sql = "
                UPDATE benefits SET
					weight_ben ='".$this->weight_ben."'
				WHERE serial_ben = '".$this->serial_ben."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }


	/***********************************************
    * funcion getStatusValues
    * Returns the current values for the status field.
    ***********************************************/	
	function getStatusValues(){
		$sql = "SHOW COLUMNS FROM benefits LIKE 'status_ben'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	///GETTERS
	function getSerial_ben(){
		return $this->serial_ben;
	}
        function getSerial_bcat(){
		return $this->serial_bcat;
	}
		function getStatus_ben(){
		return $this->status_ben;
	}
        function getWeight_ben(){
            return $this->weight_ben;
        }
	

	///SETTERS	
	function setSerial_ben($serial_ben){
		$this->serial_ben = $serial_ben;
	}
        function setSerial_bcat($serial_bcat){
		$this->serial_bcat = $serial_bcat;
	}
	function setStatus_ben($status_ben){
		$this->status_ben = $status_ben;
	}
        function setWeight_ben($weight_ben){
                $this->weight_ben = $weight_ben;
        }
}
?>