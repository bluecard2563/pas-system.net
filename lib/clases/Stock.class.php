<?php
/*
 * Author:Nicolas Flores
 * Modified date:	06/01/2010
 * Last modified:	06/01/2010
 *					20/01/2011 - Patricio Astudillo
 */

//********************************** FIX: ON WEB REPLACE 'serial_usr' WITH 'serial_usr_web' ON CONSTRUCTOR 

class Stock {				
	var $db;
	var $serial_sto;
	var $serial_mbc;
	var $serial_dea;
	var $dea_serial_dea;
	var $serial_cnt;
	var $type_sto;
	var $from_sto;
	var $to_sto;
	var $date_sto;
	var $serial_usr;
	
	function __construct($db, $serial_sto = NULL, $serial_mbc = NULL, $serial_dea = NULL, $dea_serial_dea = NULL,$serial_cnt = NULL,$type_sto = NULL,$from_sto = NULL,$to_sto = NULL,$date_sto = NULL,$serial_usr = NULL){
		$this -> db = $db;
		$this -> serial_sto = $serial_sto;
		$this -> serial_mbc = $serial_mbc;
		$this -> serial_dea = $serial_dea;
		$this -> dea_serial_dea = $dea_serial_dea;
		$this -> serial_cnt = $serial_cnt;
		$this -> type_sto = $type_sto;
		$this -> from_sto = $from_sto;
		$this -> to_sto = $to_sto;
		$this -> date_sto = $date_sto;
		$this -> serial_usr = $_SESSION['serial_usr'];
	}


    /***********************************************
	* function stockAssign
	@Name: stockAssign
	@Description: Assigns stock based on parameters.
	@Params:
         *       $amount: amount of stock to assign
         *       $type: stock type (MANUAL,VIRTUAL,ETC)
         *       $to : who are you going to assign stock (GENERAL,COUNTRY,DEALER,BRANCH,SELLER)
         *       $serial_to:  serial of the element you are going to assign the stock, f.e. serial_mbc
         *       $create_session: boolean to create or not session var to show the ranges assigned.
	@Returns: 1: no errors
         *        2: error while assigning
         *        3: Insuficient stock.
	***********************************************/
	function stockAssign($amount, $type, $to, $serial_to = NULL, $create_session=false){
		$type = strtoupper($type);
		$this -> setType_sto($type);
		$insertedRanges = array();
		
		switch(strtoupper($to)){
	  		case 'GENERAL':
				$this -> serial_mbc = NULL;
	  			$this -> serial_dea = NULL;
	  			$this -> dea_serial_dea = NULL;
	  			$this -> serial_cnt = NULL;

		        if($this -> getMaxStock() != NULL){
		        	$this -> setFrom_sto($this -> getMaxStock() + 1);
				    $this -> setTo_sto($this -> getMaxStock() + $amount);
		        }else{// NO STOCK, FIRST TIME ASSIGNMENT:
		        	$this->setFrom_sto(1);
				    $this->setTo_sto($amount);
		        }
		        
		        self::updateStockMaxValue($this->db, $this->getTo_sto());
		        
				if($this -> insert()){
					$_SESSION['assigned_stock_general_ranges']['from'] = $this -> getFrom_sto();
					$_SESSION['assigned_stock_general_ranges']['to'] = $this -> getTo_sto();
			        return 1;
			    }else{
			    	ErrorLog::log($this->db, 'MAN STOCK INSERT FAULT, WAASTED', $_SESSION);
			        return 31; //ERROR IS ALREADY LOGGED ON CALLER
			    }
	   			break;
	      	
	      	case 'MANAGER':
	  			$this -> serial_dea = NULL;
	  			$this -> dea_serial_dea = NULL;
	  			$this -> serial_cnt = NULL;		        
		        
		        $available = $this -> getAvailableForManager($type);
		        //verify if desired amount is less than the available
	        	if($amount <= $available){ //Fill stock data
				    $this -> setSerial_mbc($serial_to);
					//RETRIEVE STOCK RANGES FOR MANAGER
					$stockRanges = $this -> getStockType($type);

					return $this->compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), NULL);
		        }else{ //insuficient stock
				      return 33; //ERROR IS ALREADY LOGGED ON CALLER
		        }
	      		break;
	      		
	  		case 'DEALER':
	  			$this -> dea_serial_dea = NULL;
	  			$this -> serial_cnt = NULL;
	  			
		        $dealer = new Dealer($this -> db, $serial_to);
		        $dealer -> getData();
		        $aux_data = $dealer -> getAuxData_dea();
		        $available = $this -> getAvailableForDealer($dealer -> getSerial_mbc(), $type);
		        //verify if desired amount is less than the available
	        	if($amount <= $available){ //Fill stock data
				    $this -> setSerial_mbc($dealer -> getSerial_mbc());
				    $this -> setSerial_dea($serial_to);
					//RETRIEVE STOCK RANGES FOR DEALER
					$stockRanges = $this -> getStockManagerType($dealer -> getSerial_mbc(), $type);
					
					return $this -> compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), $dealer -> getSerial_mbc());
		        }else{ //insuficient stock
				      return 34; //ERROR IS ALREADY LOGGED ON CALLER
		        }
	      		break;
	      		
	  		case 'BRANCH':
	  			$this -> serial_cnt = NULL;
	  			
		        $dealer = new Dealer($this -> db, $serial_to);
		        $dealer -> getData();
		        $aux_data = $dealer -> getAuxData_dea();
	          	$available = $this -> getAvailableForBranch($dealer->getDea_serial_dea(), $type);

	        	if($amount <= $available){
					$this -> setSerial_mbc($dealer -> getSerial_mbc());
				    $this -> setSerial_dea($dealer->getDea_serial_dea());
				    $this -> setDea_serial_dea($serial_to);
					//RETRIEVE STOCK RANGES FOR BRANCH
			    	$stockRanges = $this -> getStockDealerType($dealer->getDea_serial_dea(), $type); //get the list of ranges of total stock for the dealer by type

					return $this->compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), $dealer->getDea_serial_dea());
				}else{
					return 35;
				}
				break;
				
			case 'SELLER':
				$counter=new Counter($this->db,$serial_to);
				$counter->getData();
				$aux_data=$counter->getAux_cnt();
				$dealer=new Dealer($this->db,$aux_data['dea_serial_dea']);
				$dealer->getData();
				$dealerAuxData=$dealer->getAuxData_dea();
				$insertedRanges=Array();
				$available = $this -> getAvailableForCounter($counter->getSerial_dea(), $type);

				if($amount<=$available){
					$this -> setSerial_mbc($dealer -> getSerial_mbc());
					$this -> setDea_serial_dea($counter->getSerial_dea());
					$this -> setSerial_dea($aux_data['dea_serial_dea']);
					$this -> setSerial_cnt($serial_to);
					//RETRIEVE STOCK RANGES FOR SELLER
					$stockRanges = $this -> getStockBranchType($counter->getSerial_dea(), $type);
					
					return $this->compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), $counter->getSerial_dea());
				}else{//insuficient stock
					 return 36; 
				}
				break;
          }
	}

	function compare_and_assign_ranges(&$insertedRanges, $stockRanges, $amount, $stock_type, $stock_to, $remote_id=NULL, $execute_insert=TRUE){
		$successAssigning = 2;
		foreach($stockRanges as $sto){
			if($amount > 0){
				$maxInRange = $this -> getUniqueStockRanges($stock_to, $stock_type, $sto[1], $sto[2], $remote_id);

				if($maxInRange < $sto[2]){
					if($maxInRange == 0){//Check if there is stock in the range, else use the floor
						$maxInRange = $sto[1] - 1;
					}
					
					if(($sto[2] - $maxInRange) >= $amount){//check if there is enought stock in the range for the amount desired
						$this -> setFrom_sto($maxInRange + 1);
						$this -> setTo_sto($maxInRange + $amount);
						array_push($insertedRanges, array('from' => $maxInRange + 1,'to' => $maxInRange + $amount));
					}else{
						$this -> setFrom_sto($maxInRange + 1);
						$this -> setTo_sto($sto[2]);
						array_push($insertedRanges, array('from' => $maxInRange + 1,'to' => $sto[2]));
					}
					
					$assigned = ($insertedRanges[count($insertedRanges) - 1]['to'] - $insertedRanges[count($insertedRanges) - 1]['from']) + 1;
					
					if($execute_insert){
						if($this -> insert()){
							//ErrorLog::log($this->db, 'STOCK WAS CREATED ' . $amount, $stock_to);
							$successAssigning = 1 ;
							if($assigned < $amount){
								$amount -= $assigned;
							} else {
								break;
							}
						}
					}else{
						if($assigned < $amount){
							$amount -= $assigned;
						} else {
							return $insertedRanges;
						}						
					}					
				}
			}
		}
		
		if($successAssigning == 1){
			$_SESSION['assigned_stock_ranges'] = $insertedRanges;
		}
		return $successAssigning;		
	}

	function getUniqueStockRanges($stock_to, $stock_type, $from, $to, $remote_id=NULL){
		switch($stock_to){
			case 'MANAGER':
				$maxInRange = $this -> getMaxStockManager($stock_type, $from, $to);
				break;
			case 'DEALER':
				$maxInRange = $this ->getMaxStockDealer($remote_id, $stock_type, $from, $to);
				break;
			case 'BRANCH':
				$maxInRange = $this ->getMaxStockBranch($remote_id, $stock_type, $from, $to);
				break;
			case 'SELLER':
				$maxInRange = $this ->getMaxStockSeller($remote_id, $stock_type, $from, $to);
				break;
		}
		return $maxInRange;
	}

	/***********************************************
	* function getNextStockNumber
	@Name: getNextStockNumber
	@Description: Get the next card number available for a counter.
          * This is only for virtual Stock
	@Params:
         *       $serial_usr: serial of the user you want to know his stock
	@Returns: int : next card number available or,
         *        null: when not available stock
	***********************************************/
	public static function getNextStockNumber($db, $captureCardNumber){//IF TRUE, THE CARD NUMBER RETRIEVED IS TAKEN EXCLUSIVELY FOR THIS SALE.

		$cardNumber = self::retrieveNextStockValue($db, $captureCardNumber);
		
		//WE MUST GUARANTEE IT'S A VALID NUMBER FOR THE SALE
		if(!$cardNumber || 
			$cardNumber == NULL ||
			trim(strtolower($cardNumber)) == '' ||
			trim(strtolower($cardNumber)) == 'null' ||
			intval($cardNumber) < 1){
			
				ErrorLog::log($db, 'CORRUPT CARD NUMBER', $cardNumber);
				return false; //TODO HANDLE BAD CARD NUMBER DELIVERY
		}
		
		if(self::isCardNumberNotInUse($db, $cardNumber)){
			return $cardNumber;
		}else{
			ErrorLog::log($db, 'CARD NUMBER IS REPEATED!!!!', $cardNumber);
			return false; //TODO HANDLE REPEATED CARD NUMBER DELIVERY
		}
	}
	
	public static function retrieveNextStockValue($db, $captureCardNumber){
		global $globalSTOCK_MAX_VALUE_PARAMETER;
		$conditionString = '+1';
		if($captureCardNumber){
			$db -> Execute("UPDATE parameter set value_par = value_par +1 WHERE serial_par = $globalSTOCK_MAX_VALUE_PARAMETER");
			$conditionString = '';
		}
		
		$sql = "SELECT value_par$conditionString FROM parameter WHERE serial_par = $globalSTOCK_MAX_VALUE_PARAMETER";
		$nextValue = $db -> getOne($sql);
		
		return $nextValue;
	}

	public static function updateStockMaxValue($db, $new_value){
		global $globalSTOCK_MAX_VALUE_PARAMETER;
		ErrorLog::log($db, 'MAN STOCK SOLD TO', $new_value);
		$db -> Execute("UPDATE parameter set value_par = $new_value WHERE serial_par = $globalSTOCK_MAX_VALUE_PARAMETER");
		return $nextValue;
	}
				
	function isCardNumberNotInUse($db, $cardNumber){
		$sql = "SELECT 1 FROM sales WHERE card_number_sal = $cardNumber";
		$result = $db -> getOne($sql);
		return $result != 1;
	}
    
    function isContractNumberNotInUse($db, $contractNumber){
		$sql = "SELECT 1 FROM contracts WHERE number_con = $contractNumber";
		$result = $db -> getOne($sql);
		return $result != 1;
	}

    /***********************************************
	* function validateManualNumber
	@Name: validateManualNumber
	@Description: Checks if a given number is part of the stock of a given user.
          * This is only for manual Stock
	@Params:
         *       $serial_cnt: serial of the counter you want to know his stock
         *       $number: card number to check
	@Returns: 0 : is not the owner,
         *        1: is owner and is available
         *        2:is owner but is not available
	***********************************************/
	function validateManualNumber($number){
		//check if the number is in a range of the stock of the user
		$sql = " SELECT serial_cnt
				FROM stock
				WHERE $number
				BETWEEN from_sto
				AND to_sto
				AND serial_cnt IS NOT NULL
				AND type_sto = 'MANUAL'";
		
		$serial_cnt = $this->db->getOne($sql);
		$number_data = array();
   
		if($serial_cnt){
			//check if the number has not been used yet
			$sql = "SELECT 1
					FROM sales
					WHERE card_number_sal =$number";
		
			$is_used = $this->db->getOne($sql);
			
			$number_data['owner']='TRUE';
			$number_data['owner_id']=$serial_cnt;			
			($is_used)?$number_data['is_sold']='TRUE':$number_data['is_sold']='FALSE';
		}else{
			global $global_salesRanges;
			$sqlRanges=" AND (";
			$break="";
			foreach($global_salesRanges as $key=>$salesRanges ){
				$sqlRanges.= $break."(".$number." BETWEEN ".$salesRanges['from']." AND ".$salesRanges['to'].")";
				$break=" OR ";
			}
			$sqlRanges.=")";
			
			//check if the number is under the first MANUAL stock range.
			$sql ="SELECT 1 FROM stock WHERE type_sto = 'MANUAL' GROUP BY 1 HAVING ".$number." < MIN(from_sto)";
			if(is_array($global_salesRanges)){
				$sql .= $sqlRanges;
			}
			
			$is_old_range = $this->db->getOne($sql);
			
			$number_data['owner']='FALSE';
			if($is_old_range){
				$number_data['in_range']='TRUE';
				
				//check if the number has not been used yet
				$sql="SELECT 1 FROM sales WHERE card_number_sal =$number";
				$is_used = $this->db->getOne($sql);
				($is_used)?$number_data['is_sold']='TRUE':$number_data['is_sold']='FALSE';
			}else{
				$number_data['in_range']='FALSE';
				$number_data['is_sold']='FALSE';
			}
		}
		
		return $number_data;
	}
	
	function getData(){
		if($this->serial_sto!=NULL){
			$sql = 	"SELECT serial_sto, serial_mbc, serial_dea, dea_serial_dea, serial_cnt, type_sto, from_sto, to_sto, date_sto, serial_usr
					FROM stock
					WHERE serial_sto='".$this->serial_sto."'";
			$result = $this -> db -> Execute($sql);

			if($result -> fields[0]){
				$this -> serial_sto = $result -> fields[0];
				$this -> serial_mbc =$result -> fields[1];
				$this -> serial_dea = $result -> fields[2];
				$this -> dea_serial_dea = $result -> fields[3];
				$this -> serial_cnt = $result -> fields[4];
				$this -> type_sto = $result -> fields[5];
				$this -> from_sto = $result -> fields[6];
				$this -> to_sto = $result -> fields[7];
				$this -> date_sto = $result -> fields[8];
				$this -> serial_usr = $result -> fields[9];
				return true;
			}
		}
		return false;		
	}
	
	function insert(){
		if($this -> serial_mbc == null || strtolower($this -> serial_mbc) == 'null' || trim(strtolower($this -> serial_mbc) == '')){
			$this -> serial_mbc = NULL;
		}
		if($this -> serial_dea == null || strtolower($this -> serial_dea) == 'null' || trim(strtolower($this -> serial_dea) == '')){
			$this -> serial_dea = NULL;
		}
		if($this -> dea_serial_dea == null || strtolower($this -> dea_serial_dea) == 'null' || trim(strtolower($this -> dea_serial_dea) == '')){
			$this -> dea_serial_dea = NULL;
		}
		if($this -> serial_cnt == null || strtolower($this -> serial_cnt) == 'null' || trim(strtolower($this -> serial_cnt) == '')){
			$this -> serial_cnt = NULL;
		}
		
		$sql='INSERT INTO stock (
					serial_sto,
					serial_mbc,
					serial_dea,
					dea_serial_dea,
					serial_cnt,
					type_sto,
					from_sto,
					to_sto,
					date_sto,
					serial_usr
					)
			  VALUES(NULL,
					'.($this->serial_mbc != NULL?'"'.$this->serial_mbc.'"':'NULL').',
					'.($this->serial_dea != NULL?'"'.$this->serial_dea.'"':'NULL').',
					'.($this->dea_serial_dea != NULL?'"'.$this->dea_serial_dea.'"':'NULL').',
					'.($this->serial_cnt != NULL?'"'.$this->serial_cnt.'"':'NULL').',
					"'.$this->type_sto.'",
					'.$this->from_sto.',
					'.$this->to_sto.',
					NOW(),
					'.$this->serial_usr.'
					);';
		//die($sql);
		$result = $this->db->Execute($sql);
		
		if($result){
		return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/***********************************************
    * function getTypeValues
    * Returns the current values for the type field.
    ***********************************************/	
	function getTypeValues(){
		$sql = "SHOW COLUMNS FROM stock LIKE 'type_sto'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
	* function getMaxStock
	* Returns the max general stock 
	***********************************************/
	function getMaxStock(){
		global $globalSTOCK_MAX_VALUE_PARAMETER;
		$parameter=new Parameter($this -> db, $globalSTOCK_MAX_VALUE_PARAMETER);
		$parameter->getData();
		
		return $parameter->getValue_par();
	}

    /***********************************************
	* function getAvailableForCountry
	@Name: getAvailableForCountry
	@Description: Retrieves de available stock for a country by type.
	@Params: type of stock
	@Returns: number, available stock / FALSE on errors
	***********************************************/
	function getAvailableForManager($type){
		$sql="
			SELECT IFNULL(total_stock_value-total_stock_value_country,0)
			FROM (
				SELECT SUM(stock_list) AS total_stock_value
				FROM(
					SELECT (to_sto-from_sto+1) AS stock_list
					FROM stock
					WHERE type_sto='".$type."'
					AND dea_serial_dea IS NULL
					AND serial_mbc IS NULL
					AND serial_dea IS NULL
					AND serial_cnt IS NULL
				) AS stock_total
			) AS stock_total_final,(
				SELECT IFNULL(SUM(stock_list_cou),0) AS total_stock_value_country
				FROM(
					SELECT (to_sto-from_sto+1) AS stock_list_cou
					FROM stock
					WHERE type_sto='".$type."'
					AND serial_mbc IS NOT NULL
					AND serial_dea IS NULL
					AND serial_cnt IS NULL
					AND dea_serial_dea IS NULL
				) AS total_stock_country
			) AS stock_pai_final
		";
		$result = $this->db->Execute($sql);
		if($result -> fields[0] < 0){
			ErrorLog::log($this->db, '!!!STOCK DUPLICADO MANAGER!!! - ' . $result -> fields[0], $sql);
			return 0;
		}
		return $result -> fields[0];
	}

	/***********************************************
	* function getAvailableForDealer
	@Name: getAvailableForDealer
	@Description: Retrieves de available stock for a dealer by type.
	@Params: country id ,stock type
	@Returns: number, available stock / FALSE on errors
	***********************************************/
	function getAvailableForDealer($serial_mbc, $type){
		$sql="
		SELECT IFNULL(total_stock_value-total_stock_country_value,0)
		FROM (
			SELECT SUM(stock_list) AS total_stock_value
			FROM(
				SELECT (to_sto-from_sto+1) AS stock_list
				FROM stock
				WHERE type_sto='".$type."'
				AND serial_mbc='".$serial_mbc."'
				AND serial_dea IS NULL
				AND dea_serial_dea IS NULL
				AND serial_cnt IS NULL
			) AS total_stock
		) AS total_final_stock,(
			SELECT IFNULL(SUM(stock_list_country),0) AS total_stock_country_value
			FROM(
				SELECT (to_sto-from_sto+1) AS stock_list_country
				FROM stock
				WHERE type_sto='".$type."'
				AND serial_mbc='".$serial_mbc."'
				AND serial_dea IS NOT NULL
				AND dea_serial_dea IS NULL
				AND serial_cnt IS NULL
			) AS total_stock_country
		) AS final_stock_country
		";
		$result = $this->db->Execute($sql);
		if($result -> fields[0] < 0){
			ErrorLog::log($this->db, '!!!STOCK DUPLICADO DEALER!!! - ' . $result -> fields[0], $sql);
			return 0;
		}
		return $result -> fields[0];
	}

        /***********************************************
	* function getAvailableForBranch
	@Name: getAvailableForBranch
	@Description: Retrieves de available stock for a branch by type.
	@Params: dealer id ,stock type
	@Returns: number, available stock / FALSE on errors
	***********************************************/
	function getAvailableForBranch($serial_dea, $type){
		$sql="
			SELECT IFNULL(total_stock_value-total_stock_country_value,0)
			FROM (
				SELECT SUM(stock_list) AS total_stock_value
				FROM(
					SELECT (to_sto-from_sto+1) AS stock_list
					FROM stock
					WHERE type_sto='".$type."'
					AND serial_mbc IS NOT NULL
					AND serial_dea = ".$serial_dea."
					AND dea_serial_dea IS NULL
					AND serial_cnt IS NULL
				) AS total_stock
			) AS total_stock_final,(
				SELECT IFNULL(SUM(stock_list_country),0) AS total_stock_country_value
				FROM(
					SELECT (to_sto-from_sto+1) AS stock_list_country
					FROM stock
					WHERE type_sto='".$type."'
					AND serial_mbc IS NOT NULL
					AND serial_dea = ".$serial_dea."
					AND dea_serial_dea IS NOT NULL
					AND serial_cnt IS NULL
				) AS total_stock_country
			) AS final_stock_country
		";
		$result = $this->db->Execute($sql);
		if($result -> fields[0] < 0){
			ErrorLog::log($this->db, '!!!STOCK DUPLICADO BRANCH!!! - ' . $result -> fields[0], $sql);
			return 0;
		}
		return $result -> fields[0];
	}
	
	public static function getArrayAllAvailableForBranch($db, $serial_dea, $type){
		$sql = "SELECT from_sto, to_sto FROM stock WHERE type_sto='$type' AND dea_serial_dea = $serial_dea AND serial_dea IS NOT NULL AND serial_cnt IS NULL";
		
		$assignedManualStocks = $db -> Execute($sql);
		$num = $assignedManualStocks -> RecordCount();
		$allAvailableNumbers = ',';
		if($num > 0){
			$cont = 0;
			do{
				$singleStock = $assignedManualStocks -> GetRowAssoc(false);
				$fromSto = $singleStock['from_sto'];
				$toSto = $singleStock['to_sto'];
				for($i = $fromSto; $i <= $toSto; $i++){
					$allAvailableNumbers .= $i . ','; //WE BUILD A STRING SEPARATING ALL AVAILABLE NUMBERS BY % INCLUDING ONE % AT EACH SIDE!
				}
				$assignedManualStocks -> MoveNext();
			}while(++$cont < $num);
		}
		$allAvailableNumbersQuery =  substr($allAvailableNumbers, 1, strlen($allAvailableNumbers) - 2);
		
		if(!$allAvailableNumbersQuery){
			return array(); //NO ASSIGNED STOCK AT ALL
		}
		
		//HERE WE GET ALL EXISTING SALES INCLUDING THOSE THAT CANNOT BE EDITED (INVOICED)
		$sql = "SELECT card_number_sal, emission_date_sal, serial_inv
				FROM sales 
				WHERE card_number_sal IN($allAvailableNumbersQuery)";
		$stockSoldNotEditable = $db -> Execute($sql);
		$num = $stockSoldNotEditable -> RecordCount();
		
		$allAvailableNumbersArray = explode(',', $allAvailableNumbersQuery);
		//TRANSFORM THE CARD NUMBERS INTO THE KEYS FOR THE RETURN ARRAY, MAKING IT SIMPLER TO IDENTIFY EACH CARD AND CHANGE IT'S DATA
		$stockAvailableToVoid = array();
		foreach($allAvailableNumbersArray as $singleCardNumber){
			$stockAvailableToVoid[$singleCardNumber] = array( 'status' => 'UNSOLD',
																	  'date_sold' => '-');
		}
		
		//WE NOW ELIMINATE EACH CARD THAT CANNOT BE EDITED
		if($num > 0){
			$cont = 0;
			do{
				$singleCard = $stockSoldNotEditable -> GetRowAssoc(false);
				
				if($singleCard['serial_inv']){//THIS SALE CANNOT BE EDITED, REMOVE IT!
					unset($stockAvailableToVoid[$singleCard['card_number_sal']]);
				}else{
					$stockAvailableToVoid[$singleCard['card_number_sal']]['status'] = 'SOLD';
					$stockAvailableToVoid[$singleCard['card_number_sal']]['date_sold'] = $singleCard['emission_date_sal'];
				}
				$stockSoldNotEditable -> MoveNext();
			}while(++$cont < $num);
		}
		return $stockAvailableToVoid;
	}

	 /***********************************************
	* function getAvailableForCounter
	@Name: getAvailableForCounter
	@Description: IF THERE IS AVAILABLE STOCK INSIDE THE BRANCH TO BE ASSIGNED TO THE COUNTER
	@Params: branch id ,stock type
	@Returns: number, available stock / FALSE on errors
	***********************************************/
	function getAvailableForCounter($dea_serial_dea, $type){
		$sql="
			SELECT IFNULL(total_stock_value-total_stock_country_value,0)
			FROM (
				SELECT SUM(stock_list) AS total_stock_value
				FROM(
					SELECT (to_sto-from_sto+1) AS stock_list
					FROM stock
					WHERE type_sto='".$type."'
					AND dea_serial_dea='".$dea_serial_dea."'
					AND serial_cnt IS NULL
				) AS total_stock
			) AS final_total_stock,(
				SELECT IFNULL(SUM(stock_list_country),0) AS total_stock_country_value
				FROM(
					SELECT (to_sto-from_sto+1) AS stock_list_country
					FROM stock
					WHERE type_sto='".$type."'
					AND dea_serial_dea='".$dea_serial_dea."'
					AND serial_cnt IS NOT NULL
				) AS total_stock_country
			) AS final_stock_country
		";
		$result = $this->db->Execute($sql);
		if($result -> fields[0] < 0){
			ErrorLog::log($this->db, '!!!STOCK DUPLICADO COUNTER!!! - ' . $result -> fields[0], $sql);
			return 0;
		}
		return $result -> fields[0];
	}

	/***********************************************
	function getStockType
	@Name: getStockType
	@Description: Retrieves a list of stock ranges based on stock type.
	@Params: type of stock
	@Returns: array, stock by type / FALSE on errors
	***********************************************/
	function getStockType($type){
		$list=NULL;
		$sql="SELECT serial_sto, from_sto, to_sto
			  FROM stock
			  WHERE serial_mbc IS NULL
			  AND serial_dea IS NULL
			  AND serial_cnt IS NULL
			  AND type_sto='".$type."'
			  ORDER BY from_sto";
		    //echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false) die("failed getStockType");
			$cont=0;
			while (!$result->EOF) {
				for($i=0;$i<3;$i++){
					$list[$cont][$i]=$result->fields[$i];
				}
				$result->MoveNext();
				$cont++;
			}
		return $list;
	}
	
/***********************************************
	function getStockManagerType
	@Name: getStockCountryType
	@Description: Retrieves a list of stock ranges based on stock type for a country.
	@Params: country id, type of stock
	@Returns: array, country stock by type / FALSE on errors
	***********************************************/
	function getStockManagerType($serial_mbc, $type = NULL){
		$list = NULL;
		$typeSql = '';
		if($type != NULL){
			$typeSql = "AND type_sto='".$type."'";
		}
		
		$sql = "SELECT serial_sto, from_sto, to_sto, type_sto
				  FROM stock
				  WHERE serial_mbc = '$serial_mbc'
				  AND serial_dea IS NULL
				  AND dea_serial_dea IS NULL
				  AND serial_cnt IS NULL
				  $typeSql
				  ORDER BY from_sto";
				  
		$result = $this -> db -> Execute($sql);
		if ($result === false) die("failed getStockManagerType");
		
		$cont = 0;
		while(!$result -> EOF){
			for($i = 0; $i < 4; $i++){
				$list[$cont][$i] = $result -> fields[$i];
			}
			$result -> MoveNext();
			$cont++;
		}
		return $list;
	}

	/***********************************************
	function getStockDealerType
	@Name: getStockDealerType
	@Description: Retrieves a list of stock ranges based on stock type for a Dealer.
	@Params: dealer id, type of stock
	@Returns: array, dealer stock by type / FALSE on errors
	***********************************************/
	function getStockDealerType($serial_dea,$type=NULL){
		$list = NULL;
		
		$typeSql = '';
		if($type != NULL){
			$typeSql = "AND type_sto='".$type."'";
		}
		
		$sql="SELECT serial_sto, from_sto, to_sto, type_sto
				  FROM stock
				  WHERE serial_mbc IS NOT NULL 
				  AND serial_dea = '".$serial_dea."'
				  AND dea_serial_dea IS NULL
				  AND serial_cnt IS NULL
				  $typeSql
				  ORDER BY from_sto";
		
		$result = $this->db->Execute($sql);
		//echo $sql;
		if ($result === false) die("failed getStockDealerType");
		
		$cont=0;
		while (!$result->EOF) {
			for($i=0;$i<4;$i++){
				$list[$cont][$i]=$result->fields[$i];
			}
			$result->MoveNext();
			$cont++;
		}
		return $list;
	}

	 /***********************************************
	function getStockBranchType
	@Name: getStockBranchType
	@Description: Retrieves a list of stock ranges based on stock type for a Branch.
	@Params: branch id, type of stock
	@Returns: array, dealer stock by type / FALSE on errors
	***********************************************/
	function getStockBranchType($dea_serial_dea,$type=NULL){
		$list = NULL;
		
		$typeSql = '';
		if($type != NULL){
			$typeSql = "AND type_sto='".$type."'";
		}
		
		$sql="SELECT serial_sto, from_sto, to_sto, type_sto
				  FROM stock
				  WHERE serial_mbc IS NOT NULL
				  AND serial_dea IS NOT NULL    
				  AND dea_serial_dea = '".$dea_serial_dea."'
				  AND serial_cnt IS NULL
				  $typeSql
				  ORDER BY from_sto";
				  
		$result = $this->db->Execute($sql);
		//echo $sql;
		if ($result === false) die("failed getStockBranchType");
		
		$cont=0;
		while (!$result->EOF) {
			for($i=0;$i<4;$i++){
				$list[$cont][$i]=$result->fields[$i];
			}
			$result->MoveNext();
			$cont++;
		}
		return $list;
	}

   /***********************************************
	funcion getMaxStockCountry
	@Name: getMaxStockCountry
	@Description: Gets the max range of existing stock which is in use
	@Params: type of stock
	@Returns: array, stock by type / FALSE on errors
	***********************************************/
	function getMaxStockManager($type,$from_sto, $to_sto){
	$sql="SELECT max(to_sto)
		  FROM stock
		  WHERE serial_mbc IS NOT NULL
		  AND serial_dea IS NULL
		  AND dea_serial_dea IS NULL
		  AND serial_cnt IS NULL
		  AND type_sto='".$type."'
		  AND to_sto BETWEEN ".$from_sto." AND ".$to_sto."
		  ";
       // echo '<br/>'.$sql;
		$result = $this->db->Execute($sql);
		if ($result->fields[0])
			return $result->fields[0];
		else
			return 0;
	}

	/***********************************************
	funcion getMaxStockDealer
	@Name: getMaxStockDealer
	@Description: Gets the max range of existing country stock which is in use by dealers
	@Params: country id, stock type, from_sto, to_sto
	@Returns: array, stock by type / FALSE on errors
	***********************************************/
	function getMaxStockDealer($serial_mbc, $type,$from, $to){
	$sql="SELECT max(to_sto)
		  FROM stock
		  WHERE serial_mbc=".$serial_mbc."
		  AND serial_dea IS NOT NULL
		  AND dea_serial_dea IS NULL
		  AND serial_cnt IS NULL
		  AND type_sto='".$type."'
		  AND to_sto BETWEEN ".$from." AND ".$to."
		  ";
		$result = $this->db->Execute($sql);
		if ($result->fields[0])
			return $result->fields[0];
		else
			return 0;
	}

	 /***********************************************
	funcion getMaxStockBranch
	@Name: getMaxStockBranch
	@Description: Gets the max range of existing dealer stock which is in use by branches
	@Params: dealer id, stock type, from_sto, to_sto
	@Returns: array, stock by type / FALSE on errors
	***********************************************/
	function getMaxStockBranch($serial_dea,$type,$from, $to){
	$sql="SELECT max(to_sto)
		  FROM stock
		  WHERE serial_dea=".$serial_dea."
		  AND dea_serial_dea IS NOT NULL
		  AND serial_cnt IS NULL
		  AND type_sto='".$type."'
		  AND to_sto BETWEEN ".$from." AND ".$to."
		  ";
		$result = $this->db->Execute($sql);
		if ($result->fields[0])
			return $result->fields[0];
		else
			return 0;
	}

	/***********************************************
	funcion getMaxStockSeller
	@Name: getMaxStockSeller
	@Description: Gets the max range of existing branch stock which is in use by sellers
	@Params: branch id, stock type, from_sto, to_sto
	@Returns: array, stock by type / FALSE on errors
	***********************************************/
	function getMaxStockSeller($dea_serial_dea,$type,$from, $to){
	$sql="SELECT max(to_sto)
		  FROM stock
		  WHERE dea_serial_dea='".$dea_serial_dea."'
		  AND serial_cnt IS NOT NULL
		  AND type_sto='".$type."'
		  AND to_sto BETWEEN ".$from." AND ".$to."
		  ";
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result->fields[0])
			return $result->fields[0];
		else
			return 0;
	}

	/***********************************************
	funcion queryForGeneralReport
	@Name: queryForGeneralReport
	@Description: get all the rows wich fits with the filters
	@Params:
	    *    $zone: serial of zone - serial_zon
	    *    $country: serial of country - serial_cou
	    *    $manager: serial of manager by contry - serial_mbc
	    *    $dealer: serial of the dealer - serial_dea (row has not dea_serial_dea)
	    *    $branch: serial of the branch - serial_dea
	    *    $counter: serial of counter - serial_cnt
	    *    $type: type of stock (MANUAL/VIRTUAL)
	manager,dealer,branch,counter and type could be empty.
	@Returns: array of results or false
	***********************************************/
	function queryForGeneralReport($zone, $country, $manager, $responsible, $dealer, $branch, $counter, 
									$type, $date_from, $date_to){
		$fields_required='';
		$joins='';
		$where='';
		$orderBy='';
		if($counter!=''){
			$where.=" s.dea_serial_dea=$branch AND s.serial_cnt=$counter";
			$orderBy.=' s.date_sto DESC';
		}elseif($branch!=''){
			$fields_required.="CONCAT(usr.last_name_usr,' ',usr.first_name_usr) as counter_name,";
			$joins.="JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
					 JOIN user usr ON cnt.serial_usr=usr.serial_usr";
			$where.=" s.dea_serial_dea=$branch AND s.serial_cnt IS NOT NULL";
			$orderBy.=' counter_name,s.date_sto';
		}elseif($dealer!=''){
			$fields_required.="cou.name_cou,man.name_man,CONCAT(usr.last_name_usr,' ',usr.first_name_usr) as responsible_name,bra.name_dea as branch_name,";
			$joins.="JOIN dealer bra ON bra.serial_dea=s.dea_serial_dea
					 JOIN dealer dea ON bra.dea_serial_dea=dea.serial_dea
					 JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea
					 JOIN user usr ON usr.serial_usr=ubd.serial_usr
					 JOIN manager_by_country mbc ON dea.serial_mbc=mbc.serial_mbc
					 JOIN manager man ON man.serial_man=mbc.serial_man
					 JOIN country cou ON cou.serial_cou=mbc.serial_cou";
			$where.=" s.serial_dea=$dealer AND s.dea_serial_dea IS NOT NULL AND s.serial_cnt IS NULL";
			$orderBy.=' branch_name,s.date_sto';
		}elseif($responsible!=''){
			$fields_required.="cou.name_cou,man.name_man,CONCAT(usr.last_name_usr,' ',usr.first_name_usr) as responsible_name,dea.name_dea,";
			$joins.="JOIN dealer dea ON s.serial_dea=dea.serial_dea
					 JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea
					 JOIN user usr ON usr.serial_usr=ubd.serial_usr
					 JOIN manager_by_country mbc ON dea.serial_mbc=mbc.serial_mbc
					 JOIN manager man ON man.serial_man=mbc.serial_man
					 JOIN country cou ON cou.serial_cou=mbc.serial_cou";
			$where.=" ubd.serial_usr=$responsible AND dea.serial_mbc=$manager AND s.serial_dea IS NOT NULL AND s.dea_serial_dea IS NULL AND s.serial_cnt IS NULL";
			$orderBy.=' dea.name_dea,s.date_sto';
		}elseif($manager!=''){
			$fields_required.="cou.name_cou,man.name_man,CONCAT(usr.last_name_usr,' ',usr.first_name_usr) as responsible_name,dea.name_dea,";
			$joins.="JOIN dealer dea ON s.serial_dea=dea.serial_dea
					 JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea
					 JOIN user usr ON usr.serial_usr=ubd.serial_usr
					 JOIN manager_by_country mbc ON dea.serial_mbc=mbc.serial_mbc
					 JOIN manager man ON man.serial_man=mbc.serial_man
					 JOIN country cou ON cou.serial_cou=mbc.serial_cou";
			$where.=" mbc.serial_mbc=$manager AND cou.serial_cou=$country AND s.serial_dea IS NOT NULL AND s.dea_serial_dea IS NULL AND s.serial_cnt IS NULL";
			$orderBy.=' responsible_name,s.date_sto';
		}elseif($country!=''){
			$fields_required.="cou.name_cou,man.name_man,CONCAT(usr.last_name_usr,' ',usr.first_name_usr) as responsible_name,dea.name_dea,";
			$joins.="JOIN dealer dea ON s.serial_dea=dea.serial_dea
					 JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea
					 JOIN user usr ON usr.serial_usr=ubd.serial_usr
					 JOIN manager_by_country mbc ON dea.serial_mbc=mbc.serial_mbc
					 JOIN manager man ON man.serial_man=mbc.serial_man
					 JOIN country cou ON cou.serial_cou=mbc.serial_cou";
			$where.="s.serial_dea IS NOT NULL AND s.dea_serial_dea IS NULL AND s.serial_cnt IS NULL";
			$orderBy.=' man.name_man,s.date_sto';
		}else{
			$fields_required.="cou.name_cou,";
			$where.="cou.serial_zon=$zone AND s.serial_dea IS NULL AND s.dea_serial_dea IS NULL AND s.serial_cnt IS NULL";
			$orderBy.=' cou.name_cou,s.date_sto';
		}

		if($type){
			$where.=" AND s.type_sto='$type'";
		}
		
		$where .= " AND date_sto BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$date_to', '%d/%m/%Y'), INTERVAL +25 HOUR)";
		$sql="SELECT $fields_required s.from_sto,s.to_sto,((s.to_sto-s.from_sto)+1) as quantity,DATE_FORMAT(s.date_sto,'%d/%m/%Y %T')as date_sto,s.type_sto,CONCAT(usa.last_name_usr,' ',usa.first_name_usr) as assigned_by
			FROM stock s
			JOIN user usa ON usa.serial_usr=s.serial_usr
			$joins
			WHERE $where
			ORDER BY $orderBy";

		//die(Debug::print_r($sql));
		
		$result=$this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
		
	}

	/***********************************************
	funcion getManualStockRangesUsed
	@Name: getManualStockRangesUsed
	@Description: get all manual stock ranges which have been already used atleast once.
	@Params:
	    *    $manager: serial of manager by contry - serial_mbc
	    *    $responsible: serial of the responsible user
	    *	 $dealer: serial of the dealr
	    *    $branch: serial of the branch - serial_dea
	    *    $counter: serial of counter - serial_cnt
	    *    $txtDateFrom: starting date to compare in assigned date ranges
	    *    $txtDateTo: ending date to compare in assigned date ranges
	manager,dealer,branch,counter and type could be empty.
	@Returns: array of results or false
	***********************************************/
	function getManualStockRangesUsed($manager,$responsible,$dealer,$branch,$counter,$txtDateFrom,$txtDateTo){
		if(!$manager){
			return false;
		}
		$select="SELECT cit.name_cit,
			CONCAT(usrr.first_name_usr,' ',usrr.last_name_usr) as responsible_name,
			bra.name_dea,
			CONCAT(cou.code_cou,'-',cit.code_cit,'-',dea.code_dea,'-',bra.code_dea)as code_dea,
			CONCAT(usrc.first_name_usr,' ',usrc.last_name_usr) as counter_name,
			DATE_FORMAT(st.date_sto,'%d/%m/%Y') as date_sto,
			CONCAT(usra.first_name_usr,' ',usra.last_name_usr) as assigned_by,
			st.from_sto,
			st.to_sto ";

		$joins="FROM stock st
			JOIN dealer dea ON st.serial_dea=dea.serial_dea
			JOIN dealer bra ON st.dea_serial_dea = bra.serial_dea
			JOIN sector sec ON sec.serial_sec=bra.serial_sec
			JOIN city cit ON cit.serial_cit = sec.serial_cit
			JOIN user_by_dealer ubd ON ubd.serial_dea=bra.serial_dea AND ubd.status_ubd ='ACTIVE'
			JOIN user usrr ON usrr.serial_usr=ubd.serial_usr
			JOIN counter cnt ON cnt.serial_cnt=st.serial_cnt
			JOIN user usrc ON usrc.serial_usr=cnt.serial_usr
			JOIN user usra ON usra.serial_usr=st.serial_usr
			JOIN manager_by_country mbc ON mbc.serial_mbc=bra.serial_mbc AND mbc.serial_mbc=$manager";

		$where="WHERE st.serial_cnt IS NOT NULL
				AND st.type_sto='MANUAL'
				AND (st.to_sto<(SELECT MAX(s.card_number_sal)
						FROM sales s
						WHERE s.serial_cnt=st.serial_cnt
						AND s.stock_type_sal='MANUAL') OR (SELECT MAX(s.card_number_sal)
											FROM sales s
											WHERE s.serial_cnt=st.serial_cnt
											AND s.stock_type_sal='MANUAL') BETWEEN st.from_sto AND st.to_sto)";
		$orderby="ORDER BY name_cit,responsible_name,name_dea,counter_name";
		//check to apply filters
		if($counter){
			$where.=" AND st.serial_cnt=$counter";
		}elseif($branch){
			$where.=" AND st.dea_serial_dea=$branch";
		}elseif($dealer){
			$where.=" AND st.serial_dea=$dealer";
		}elseif($responsible){
			$where.=" AND ubd.serial_usr=$responsible";
		}
		//check if there are date filters
		if($txtDateFrom && $txtDateTo){
			$where.=" AND STR_TO_DATE(DATE_FORMAT(st.date_sto,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$txtDateFrom','%d/%m/%Y') AND STR_TO_DATE('$txtDateTo','%d/%m/%Y')";
		}elseif($txtDateFrom){
			$where.=" AND STR_TO_DATE(DATE_FORMAT(st.date_sto,'%d/%m/%Y'),'%d/%m/%Y') >= STR_TO_DATE('$txtDateFrom','%d/%m/%Y')";
		}elseif($txtDateTo){
			$where.=" AND STR_TO_DATE(DATE_FORMAT(st.date_sto,'%d/%m/%Y'),'%d/%m/%Y') <= STR_TO_DATE('$txtDateTo','%d/%m/%Y')";
		}
		$sql=$select.' '.$joins.' '.$where.' '.$orderby;
		//echo $sql;
		$result=$this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	function getStockRangesToBeAssigned($amount, $type, $to, $serial_to = NULL){
		$type = strtoupper($type);
		$this -> setType_sto($type);
		$insertedRanges = array();
		
		switch(strtoupper($to)){
	  		case 'GENERAL':	  			
		        if($this -> getMaxStock() != NULL){
		        	array_push($insertedRanges, array('from' => $this -> getMaxStock() + 1,'to' => $this -> getMaxStock() + $amount));
		        }else{// NO STOCK, FIRST TIME ASSIGNMENT:
		        	array_push($insertedRanges, array('from' => 1,'to' => $amount));
		        }
		        
		        return $insertedRanges;
	   			break;
	      	
	      	case 'MANAGER':		        
		        $available = $this -> getAvailableForManager($type);
		        //verify if desired amount is less than the available
	        	if($amount <= $available){ //Fill stock data
					//RETRIEVE STOCK RANGES FOR MANAGER
					$stockRanges = $this -> getStockType($type);
					
					return $this->compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), NULL, FALSE);
		        }else{ //insuficient stock
				      return 'no_stock';
		        }
	      		break;
	      		
	  		case 'DEALER':
		        $dealer = new Dealer($this -> db, $serial_to);
		        $dealer -> getData();
		        $aux_data = $dealer -> getAuxData_dea();
		        $available = $this -> getAvailableForDealer($dealer -> getSerial_mbc(), $type);
		        //verify if desired amount is less than the available
	        	if($amount <= $available){ //Fill stock data
					//RETRIEVE STOCK RANGES FOR DEALER
					$stockRanges = $this -> getStockManagerType($dealer -> getSerial_mbc(), $type);
					
					return $this -> compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), $dealer -> getSerial_mbc(), FALSE);
		        }else{ //insuficient stock
				      return 'no_stock';
		        }
	      		break;
	      		
	  		case 'BRANCH':	  			
		        $dealer = new Dealer($this -> db, $serial_to);
		        $dealer -> getData();
		        $aux_data = $dealer -> getAuxData_dea();
	          	$available = $this -> getAvailableForBranch($dealer->getDea_serial_dea(), $type);

	        	if($amount <= $available){
					//RETRIEVE STOCK RANGES FOR BRANCH
			    	$stockRanges = $this -> getStockDealerType($dealer->getDea_serial_dea(), $type); //get the list of ranges of total stock for the dealer by type

					return $this->compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), $dealer->getDea_serial_dea(), FALSE);
				}else{
					return 'no_stock';
				}
				break;
				
			case 'SELLER':
				$counter=new Counter($this->db,$serial_to);
				$counter->getData();
				$aux_data=$counter->getAux_cnt();
				$dealer=new Dealer($this->db,$aux_data['dea_serial_dea']);
				$dealer->getData();
				$dealerAuxData=$dealer->getAuxData_dea();
				$insertedRanges=Array();
				$available = $this -> getAvailableForCounter($counter->getSerial_dea(), $type);

				if($amount<=$available){
					//RETRIEVE STOCK RANGES FOR SELLER
					$stockRanges = $this -> getStockBranchType($counter->getSerial_dea(), $type);
					
					return $this->compare_and_assign_ranges($insertedRanges, $stockRanges, $amount, $type, strtoupper($to), $counter->getSerial_dea(), FALSE);
				}else{//insuficient stock
					 return 'no_stock'; 
				}
				break;
          }
	}
    
    /***********************************************
	*   function getNextStockNumber
        @Name: getNextStockNumber
        @Description: Get the next contract number
        @Params:
            $serial_usr: serial of the user you want to know his stock
        @Returns: int : next card number available or,
            null: when not available stock
	***********************************************/
	public static function getNextContractNumber($db){

		$contractNumber = null;
        
        global $globalCONTRACT_MAX_VALUE_PARAMETER;
		
        if ($db->Execute("UPDATE parameter set value_par = value_par +1 WHERE serial_par = $globalCONTRACT_MAX_VALUE_PARAMETER")) {
             $sql = "SELECT value_par FROM parameter WHERE serial_par = $globalCONTRACT_MAX_VALUE_PARAMETER";
            $contractNumber = $db -> getOne($sql);
        }
		
		if(!isset($contractNumber)){
				ErrorLog::log($db, 'CORRUPT CONTRACT NUMBER', $contractNumber);
				return false;
		}
		
		if(self::isContractNumberNotInUse($db, $contractNumber)){
			return $contractNumber;
		}else{
			ErrorLog::log($db, 'CONTRACT NUMBER IS REPEATED!!!!', $contractNumber);
			return false;
		}
	}
	
	
	///GETTERS
	function getSerial_sto(){
		return $this->serial_sto;
	}
	function getSerial_mbc(){
		return $this->serial_mbc;
	}
	function getSerial_dea(){
		return $this->serial_dea;
	}
	function getDea_serial_dea(){
		return $this->dea_serial_dea;
	}
	
	function getSerial_cnt(){
		return $this->serial_cnt;
	}
	function getType_sto(){
		return $this->type_sto;
	}
	function getFrom_sto(){
		return $this->from_sto;
	}
	function getTo_sto(){
		return $this->to_sto;
	}
	function getDate_sto(){
		return $this->date_sto;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}

	///SETTERS	
	
	function setSerial_sto($serial_sto){
		$this->serial_sto = $serial_sto;
	}
	function setSerial_mbc($serial_mbc){
		$this->serial_mbc = $serial_mbc;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea = $serial_dea;
	}
	function setDea_serial_dea($dea_serial_dea){
		$this->dea_serial_dea = $dea_serial_dea;
	}
	function setSerial_cnt($serial_cnt){
		$this->serial_cnt = $serial_cnt;
	}
	function setType_sto($type_sto){
		$this->type_sto = $type_sto;
	}
	function setFrom_sto($from_sto){
		$this->from_sto = $from_sto;
	}
	function setTo_sto($to_sto){
		$this->to_sto = $to_sto;
	}
	function setDate_sto($date_sto){
		$this->date_sto = $date_sto;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
}	
?>