<?php
/*
File: AppliedComission.class.php
Author: Miguel Ponce
Creation Date: 13/04/2010
*/

class AppliedComission{
	var $db;
	var $serial_com;
	var $serial_sal;
	var $serial_dea;
	var $serial_mbc;
	var $serial_fre;
	var $serial_ubd;
	var $value_ubd_com;
	var $value_mbc_com;
	var $value_fre_com;
	var $creation_date_com;
	var $type_com;

	function __construct($db,
							$serial_com=NULL,
							$serial_sal=NULL,
							$serial_dea=NULL,
							$serial_mbc=NULL,
							$serial_fre=NULL,
							$serial_ubd=NULL,
							$value_ubd_com=NULL,
							$value_mbc_com=NULL,
							$value_fre_com=NULL,
							$creation_date_com=NULL,
							$type_com=NULL)
	{
		$this -> db = $db;
		$this -> serial_com=$serial_com;
		$this -> serial_sal=$serial_sal;
		$this -> serial_dea=$serial_dea;
		$this -> serial_mbc=$serial_mbc;
		$this -> serial_fre=$serial_fre;
		$this -> serial_ubd=$serial_ubd;
		$this -> value_ubd_com=$value_ubd_com;
		$this -> value_mbc_com=$value_mbc_com;
		$this -> value_fre_com=$value_fre_com;
		$this -> creation_date_com=$creation_date_com;
		$this -> type_com=$type_com;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_ans
    ***********************************************/
	function getData(){
		if($this->serial_com!=NULL){
			$sql = 	"SELECT serial_com,
							serial_sal,
							serial_dea,
							serial_mbc,
							serial_fre,
							serial_ubd,
							value_ubd_com,
							value_mbc_com,
							value_fre_com,
							creation_date_com,
							type_com
					FROM applied_comissions
					WHERE serial_com='".$this->serial_com."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_com=$result->fields[0];
				$this -> serial_sal=$result->fields[1];
				$this -> serial_dea=$result->fields[2];
				$this -> serial_mbc=$result->fields[3];
				$this -> serial_fre=$result->fields[4];
				$this -> serial_ubd=$result->fields[5];
				$this -> value_ubd_com=$result->fields[6];
				$this -> value_mbc_com=$result->fields[7];
				$this -> value_fre_com=$result->fields[8];
				$this -> creation_date_com=$result->fields[9];
				$this -> type_com=$result->fields[10];

				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO applied_comissions (
				serial_com,
				serial_sal,
				serial_dea,
				serial_mbc,
				serial_ubd,
				value_dea_com,
				value_ubd_com,
				value_mbc_com,
				creation_date_com,
				type_com,
				serial_fre,
				value_fre_com
			)
            VALUES (
                NULL,
				'".$this->serial_sal."',
				'".$this->serial_dea."',
				'".$this->serial_mbc."',
				'".$this->serial_ubd."',
				'".$this->value_dea_com."',
				'".$this->value_ubd_com."',
				'".$this->value_mbc_com."',
				NOW(),
				'".$this->type_com."',";
				if($this->serial_fre && $this->value_fre_com){
					$sql.=	"'".$this->serial_fre."',
							'".$this->value_fre_com."'";
				}else{
					$sql.=	"NULL,
							 NULL";
				}

				$sql.=")";
        
		$result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	function getPercentages($serial_inv) {
		$sql = "SELECT
					sal.serial_sal,
					(sal.total_sal-(sal.total_sal*((IFNULL(i.discount_prcg_inv,0)+IFNULL(other_dscnt_inv,0))/100))) AS total_sal,
					dea.serial_dea,
					IFNULL(i.comision_prcg_inv, 0) AS 'percentage_dea',
					IF(IFNULL(i.comision_prcg_inv, 0) = 0, 'DISCOUNT', 'COMISSION') AS 'type_percentage_dea',
                                        i.applied_taxes_inv,
					mbc.serial_mbc,
					mbc.percentage_mbc,
					mbc.type_percentage_mbc,
					ubd.serial_ubd,
					ubd.percentage_ubd,
					sal.status_sal,
					parentDea.manager_rights_dea,
					parentDea.percentage_dea AS 'parent_percentage',
					parentDea.serial_dea AS 'parent_serial_dea',
					sal.total_sal AS 'base_sal_amount',
                    ubd_parent.serial_ubd AS 'parent_serial_ubd',
                    ubd_parent.percentage_ubd AS 'parent_percentage_ubd',
					i.other_dscnt_inv,
					i.comision_prcg_inv,
                    pay.status_pay
				FROM sales sal
				JOIN invoice i ON i.serial_inv=sal.serial_inv
				JOIN payments pay ON pay.serial_pay = i.serial_pay
				JOIN product_by_dealer pbd ON pbd.serial_pbd=sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro AND p.has_comision_pro='YES'
				JOIN counter cnt ON sal.serial_cnt = cnt.serial_cnt
				JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
				JOIN dealer parentDea ON parentDea.serial_dea=dea.dea_serial_dea
				JOIN manager_by_country mbc ON dea.serial_mbc=mbc.serial_mbc
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd='ACTIVE'
                                JOIN user_by_dealer ubd_parent ON ubd_parent.serial_dea = parentDea.serial_dea AND ubd_parent.status_ubd = 'ACTIVE'
				WHERE sal.serial_inv=$serial_inv";
		
                //Debug::print_r($sql); die;
                
		$result = $this->db -> Execute($sql);
		if($result){
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
			}
			return $array;
		}
		return array();
	}

	function getPercentagesSale() {

		$sql="	SELECT *
				FROM applied_comissions com
				WHERE com.serial_sal=$this->serial_sal
				AND com.type_com = 'IN'
				AND com.serial_sal NOT IN( SELECT serial_sal FROM applied_comissions com WHERE type_com='REFUND')";

		$result = $this->db -> Execute($sql);
		if($result){
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
			}
			return $array;
		}
		return array();
	}


	function refundSale($comission_was_paid=NULL) {
		$sql="INSERT INTO applied_comissions 
					(SELECT NULL,
							ubd_serial_usr,
							serial_sal,
							serial_dea,
							dea_serial_usr,
							mbc_serial_usr,
							serial_mbc,
							fre_serial_usr,
							serial_fre,
							serial_ubd,";
		
		if($comission_was_paid!=NULL){
			$sql.=" '0',";
		}else{
			$sql.=" value_dea_com,";
		}
		$sql.="			value_ubd_com,
						value_mbc_com,
						value_fre_com,
						NOW(),
						'REFUND',
						NULL,
						NULL,
						NULL,
						NULL
				FROM applied_comissions com
				WHERE com.serial_sal={$this->serial_sal}
				AND com.type_com = 'IN'
				AND com.serial_sal NOT IN( SELECT serial_sal FROM applied_comissions com WHERE type_com='REFUND')
				ORDER BY creation_date_com)";

		$result = $this->db -> Execute($sql);
		if($result){
			return true;
		}else{
			return false;
		}
	}

	 /*
	  * @Name: getDealersComissionableValues
	  * @Description: Returns all the sale's data which can be commisioned.
	  * @Params: $serial_dea
	  * @Returns: An array of sales registers for the dealer.
	  */
    function getDealersComissionableValues($serial_dea, $last_date_payed, $dateTo = NULL){
    	$endDateRestrictionSql = "";
     	if ($dateTo){
        	$endDateRestrictionSql = "AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com,'%d/%m/%Y'),'%d/%m/%Y') <= STR_TO_DATE('$dateTo','%d/%m/%Y')";
        }
        
		$sql = "SELECT 	apc.serial_com, 
						apc.value_dea_com, 
						apc.type_com,
						s.card_number_sal,
						inv.number_inv,CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
					    pbl.name_pbl,
					    DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
					    DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
					    days_sal,
					    dea.name_dea,
					    s.total_sal*(1 - (IFNULL(inv.discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) as base_value,
						apc.creation_date_com as report_date,
					  	dea.percentage_dea as fixed_comission_percentage,
                        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS 'date_inv',
                        mbc.serial_mbc,
                        inv.applied_taxes_inv
				FROM applied_comissions apc
				JOIN sales s ON s.serial_sal=apc.serial_sal
			    JOIN invoice inv ON s.serial_inv=inv.serial_inv
			    JOIN customer cus ON s.serial_cus=cus.serial_cus
			    JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
			    JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
                JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
			    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
			    JOIN product pr ON pr.serial_pro=pxc.serial_pro
			    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				WHERE apc.serial_dea='$serial_dea'
				AND apc.dea_payment_date_com IS NULL
				AND apc.value_dea_com <> 0.00
				$endDateRestrictionSql  
                ORDER BY s.serial_sal";
		//die('<pre>'.$sql.'</pre>');
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
    }
	 /*
	  * @Name: getManagersComissionableValues
	  * @Description: Returns all the sale's data which can be commisioned for a manager.
	  * @Params: $serial_mbc
	  * @Returns: An array of sales registers for the manager.
	  */
    function getManagersComissionableValues($serial_mbc, $last_date_payed, $dateTo = NULL){
    	$endDateRestrictionSql = "";
     	if ($dateTo){
        	$endDateRestrictionSql = "AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com,'%d/%m/%Y'),'%d/%m/%Y') <= STR_TO_DATE('$dateTo','%d/%m/%Y')";
        }
        
		$sql = "SELECT 	apc.serial_com, 
						apc.value_mbc_com, 
						apc.type_com,
						s.card_number_sal,
						inv.number_inv,
						CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
					    pbl.name_pbl,
					    DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
					    DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
					    days_sal,
					    dea.name_dea,
					    s.total_sal*(1 - (IFNULL(inv.discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) as base_value,
					  	apc.creation_date_com as report_date,
					  	mbc.percentage_mbc as fixed_comission_percentage,
                        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS 'date_inv',
                        mbc.serial_mbc,
                        inv.applied_taxes_inv
				FROM applied_comissions apc
				JOIN sales s ON s.serial_sal=apc.serial_sal
			    JOIN invoice inv ON s.serial_inv=inv.serial_inv
			    JOIN customer cus ON s.serial_cus=cus.serial_cus
			    JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
			    JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
			    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
			    JOIN product pr ON pr.serial_pro=pxc.serial_pro
			    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
			    JOIN manager_by_country mbc ON apc.serial_mbc = mbc.serial_mbc
				WHERE apc.serial_mbc='$serial_mbc'
				AND apc.mbc_payment_date_com IS NULL
				AND apc.value_mbc_com <> 0.00
				$endDateRestrictionSql";
		
		$result = $this -> db -> Execute($sql);
		if($result){
			$num = $result -> RecordCount();
			if($num > 0){
				$array = array();
				$count = 0;
				do{
					$array[$count++] = $result -> GetRowAssoc(false);
					$result -> MoveNext();
				}while($count < $num);
			}
			return $array;
		}
		return array();
    }
	
	/*
	  * @Name: getManagersComissionableValues
	  * @Description: Returns all the sale's data which can be commisioned for a manager.
	  * @Params: $serial_mbc
	  * @Returns: An array of sales registers for the manager.
	  */
    public static function getFreelanceComissionableValues($db, $serial_fre, $last_date_payed, $dateTo = NULL){
     	if ($dateTo){
        	$endDateRestrictionSql = "AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com,'%d/%m/%Y'),'%d/%m/%Y') <= STR_TO_DATE('$dateTo','%d/%m/%Y')";
        }
        
		$sql = "SELECT 	apc.serial_com, 
						apc.value_fre_com, 
						apc.type_com,
						s.card_number_sal,
						inv.number_inv,
						CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
					    pbl.name_pbl,
					    DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
					    DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
					    days_sal,
					    dea.name_dea,
					    s.total_sal*(1 - (IFNULL(inv.discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) as base_value,
					  	apc.creation_date_com as report_date,
					  	fre.comission_prctg_free as fixed_comission_percentage,
                        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS 'date_inv',
                        mbc.serial_mbc,
                        inv.applied_taxes_inv
				FROM applied_comissions apc
				JOIN sales s ON s.serial_sal=apc.serial_sal
			    JOIN invoice inv ON s.serial_inv=inv.serial_inv
			    JOIN customer cus ON s.serial_cus=cus.serial_cus
			    JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
			    JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
                JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
			    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
			    JOIN product pr ON pr.serial_pro=pxc.serial_pro
			    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
			    JOIN freelance fre ON apc.serial_fre = fre.serial_fre
				WHERE apc.serial_fre='$serial_fre'
				AND apc.fre_payment_date_com IS NULL
				AND apc.value_fre_com <> 0.00
				$endDateRestrictionSql";
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
    }
    
	 /*
	  * @Name: getUsersComissionableSales
	  * @Description: Returns an array of all the last commisionable sales of a user.
	  * @Params: $userID
	  */
     function getUsersComissionableSales($userID, $dateTo = NULL){
        $endDateRestrictionSql = "";
     	if ($dateTo){
        	$endDateRestrictionSql = "AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com,'%d/%m/%Y'),'%d/%m/%Y') <= STR_TO_DATE('$dateTo','%d/%m/%Y')";
        }
     	
     	$sql="SELECT 	apc.serial_com,
						ubd.serial_ubd,
         				apc.value_ubd_com, 
         				apc.value_dea_com,
         				apc.type_com,
         				s.card_number_sal,
         				inv.number_inv,
         				CONCAT(last_name_cus,' ',first_name_cus) as name_cus,
				      	pbl.name_pbl,
				      	DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
				      	DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
					  	days_sal,
					  	dea.name_dea,
					  	s.total_sal*(1 - (IFNULL(inv.discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) as base_value,
					  	apc.creation_date_com as report_date,
					  	ubd.percentage_ubd as fixed_comission_percentage,
                        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS 'date_inv',
                        mbc.serial_mbc,
                        inv.applied_taxes_inv
			   FROM user_by_dealer ubd
			   JOIN applied_comissions apc ON apc.serial_ubd = ubd.serial_ubd AND ubd.serial_usr = $userID AND ubd.status_ubd = 'ACTIVE'
			   JOIN sales s ON s.serial_sal = apc.serial_sal
			   JOIN invoice inv ON s.serial_inv = inv.serial_inv
			   JOIN customer cus ON s.serial_cus = cus.serial_cus
			   JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
			   JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
               JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
			   JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
			   JOIN product_by_country pxc ON pbd.serial_pxc = pxc.serial_pxc
			   JOIN product pr ON pr.serial_pro = pxc.serial_pro
			   JOIN product_by_language pbl ON pr.serial_pro = pbl.serial_pro 
			   								AND pbl.serial_lang = {$_SESSION['serial_lang']} 
			   WHERE apc.ubd_payment_date_com IS NULL
			   AND apc.value_ubd_com <> 0.00
			   $endDateRestrictionSql
			   ORDER BY s.card_number_sal ASC";
		 //Debug::print_r($sql); die;
		$result = $this->db -> Execute($sql);
		if($result){
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
			}
			return $array;
		}
		return array();
     }
	 
	 function getSubManagerComissionableValues($serial_dea, $dateTo = NULL){
    	$endDateRestrictionSql = "";
     	if ($dateTo){
        	$endDateRestrictionSql = "AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com,'%d/%m/%Y'),'%d/%m/%Y') <= STR_TO_DATE('$dateTo','%d/%m/%Y')";
        }
        
		$sql = "SELECT 	apc.serial_com, 
						apc.value_dea_com, 
						apc.type_com,
						s.card_number_sal,
						inv.number_inv,
						CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
					    pbl.name_pbl,
					    DATE_FORMAT(s.in_date_sal,'%d/%m/%Y')as in_date_sal,
					    DATE_FORMAT(apc.creation_date_com,'%d/%m/%Y') as creation_date_com,
					    days_sal,
					    br.name_dea,
						apc.creation_date_com as report_date,
					  	dea.percentage_dea as fixed_comission_percentage,
						inv.discount_prcg_inv,
						inv.comision_prcg_inv,
						inv.other_dscnt_inv,
						s.total_sal,
                        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS 'date_inv',
                        mbc.serial_mbc
				FROM applied_comissions apc
				JOIN sales s ON s.serial_sal=apc.serial_sal
			    JOIN invoice inv ON s.serial_inv=inv.serial_inv
			    JOIN customer cus ON s.serial_cus=cus.serial_cus
			    JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
			    JOIN dealer br ON cnt.serial_dea = br.serial_dea
				JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea AND dea.manager_rights_dea = 'YES'
                JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
			    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
			    JOIN product pr ON pr.serial_pro=pxc.serial_pro
			    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				WHERE apc.serial_dea='$serial_dea'
				AND apc.dea_payment_date_com IS NULL
				AND apc.value_dea_com <> 0.00
				$endDateRestrictionSql";
				
		//die('<pre>'.$sql.'</pre>');
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
    }


	 /*
	  * @Name: setPaidComission
	  * @Description: Update the rows in applied_comission, setting the:
	  *			 dea_payment_date_com , dea_and serial_usr
	  * OR		fre_payment_date_com , fre_and serial_usr
	  * OR		ubd_payment_date_com , ubd_and serial_usr
	  * OR		mbc_payment_date_com , mbc_and serial_usr
	  * @Params: $type: MANAGER,RESPONSIBLE,DEALER
	  *			 $id: manager, responsible,freelance or dealer serial
	  * @Returns: true: on success
	  *			  false:on fail
	  */
    function setPaidComission($type, $id, $user_id, $comissions_list){
		if(!$type || !$id || !$comissions_list)
			return false;

		switch($type){
			case 'MANAGER':
				$sql = "UPDATE applied_comissions apc
						SET
						apc.mbc_serial_usr ='$user_id',
						apc.mbc_payment_date_com=NOW()";
				break;
			case 'RESPONSIBLE':
				$sql = "UPDATE applied_comissions apc
						SET
						apc.ubd_serial_usr ='$user_id',
						apc.ubd_payment_date_com=NOW()";
				break;
			case 'DEALER':
			case 'SUBMANAGER':
				$sql = "UPDATE applied_comissions apc
						SET
						apc.dea_serial_usr ='$user_id',
						apc.dea_payment_date_com=NOW()";
				break;
			case 'FREELANCE':
				$sql = "UPDATE applied_comissions apc
						SET
						apc.fre_serial_usr ='$user_id',
						apc.fre_payment_date_com=NOW()";
				break;

		}

		$sql.= " WHERE apc.serial_com IN ($comissions_list)";

		$result = $this->db->Execute($sql);
        if ($result === false) return false;
        if($result==true)
            return true;
        else
            return false;
    }
 /*
	  * @Name: getComissionForReport
	  * @Description: Update the rows in applied_comission, setting the:
	  *			 dea_payment_date_com , dea_and serial_usr
	  * OR		fre_payment_date_com , fre_and serial_usr
	  * OR		ubd_payment_date_com , ubd_and serial_usr
	  * OR		mbc_payment_date_com , mbc_and serial_usr
	  * @Params: $type: MANAGER,RESPONSIBLE,DEALER
	  *			 $id: manager, responsible,freelance or dealer serial
	  * @Returns: true: on success
	  *			  false:on fail
	  */
	function getComissionForReport($comission_to, $serial, $type, $startDate, $endDate, $dealer_report=false, $misc=NULL){
		if(!$comission_to || !$type || (!$serial && $comission_to != 'DEALER')){
			return false;
		}else{
			switch($comission_to){
				case 'MANAGER':
						$sql="SELECT	apc.serial_com,
										apc.value_mbc_com,
										apc.value_dea_com,
										apc.type_com,
										s.card_number_sal,
										inv.number_inv,
										CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
										pbl.name_pbl,
										DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
										DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
										days_sal,
										dea.name_dea,
										s.total_sal*(1 - (inv.discount_prcg_inv + other_dscnt_inv)/100) as base_value,
										apc.creation_date_com as report_date,
										mbc.percentage_mbc as fixed_comission_percentage
						FROM applied_comissions apc
						JOIN manager_by_country mbc ON mbc.serial_mbc=apc.serial_mbc
						JOIN sales s ON s.serial_sal=apc.serial_sal
						JOIN invoice inv ON s.serial_inv=inv.serial_inv
						JOIN customer cus ON s.serial_cus=cus.serial_cus
						JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
						JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
						JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
						JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
						JOIN product pr ON pr.serial_pro=pxc.serial_pro
						JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
						WHERE apc.serial_mbc='$serial'
						AND apc.value_mbc_com > 0";
						if($type=='PENDING'){
							$sql.=" AND apc.creation_date_com BETWEEN mbc.last_commission_paid_mbc AND NOW()";
						}else{
							$sql.=" AND apc.creation_date_com<=mbc.last_commission_paid_mbc";
						}
						if($startDate && $endDate){
							$sql.=" AND apc.creation_date_com BETWEEN STR_TO_DATE('$startDate' , '%d/%m/%Y') 
										AND DATE_ADD(STR_TO_DATE('$endDate' , '%d/%m/%Y'), INTERVAL 23 HOUR)";
						}
					break;
				case 'RESPONSIBLE':
						 $sql="SELECT	apc.serial_com,
										apc.value_ubd_com,
										apc.value_dea_com,
										apc.type_com,
										s.card_number_sal,
										inv.number_inv, CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
										pbl.name_pbl,
										DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
										DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
										days_sal,
										dea.name_dea,
										s.total_sal*(1 - (inv.discount_prcg_inv + other_dscnt_inv)/100) as base_value,
										apc.creation_date_com as report_date,
										dea.percentage_dea as fixed_comission_percentage
						   FROM user_by_dealer ubd
						   JOIN applied_comissions apc ON apc.serial_ubd=ubd.serial_ubd
						   JOIN sales s ON s.serial_sal=apc.serial_sal
						   JOIN invoice inv ON s.serial_inv=inv.serial_inv
						   JOIN customer cus ON s.serial_cus=cus.serial_cus
						   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
						   JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
						   JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
						   JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
						   JOIN product pr ON pr.serial_pro=pxc.serial_pro
						   JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
						   WHERE ubd.serial_usr='".$serial."'
						   AND apc.value_ubd_com > 0";
							if($type=='PENDING'){
								$sql.=" AND apc.creation_date_com BETWEEN ubd.last_commission_paid_ubd AND IFNULL(ubd.end_date_ubd, NOW()) "
										. " AND AND apc.ubd_payment_date_com IS NULL";
							}else{
								$sql.=" AND apc.creation_date_com <= ubd.last_commission_paid_ubd 
										AND apc.ubd_payment_date_com IS NOT NULL";
							}
							if($startDate && $endDate){
								$sql.=" AND apc.creation_date_com BETWEEN STR_TO_DATE('$startDate' , '%d/%m/%Y') 
											AND DATE_ADD(STR_TO_DATE('$endDate' , '%d/%m/%Y'), INTERVAL 23 HOUR)";
							}
					break;
				case 'DEALER':
							$sql.=" SELECT	apc.serial_com,
											apc.value_dea_com, 
											apc.type_com,
											IFNULL(s.card_number_sal, 'N/A') AS 'card_number_sal',
											inv.number_inv,CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
											pbl.name_pbl,
											DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
											DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
											days_sal,
											dea.name_dea,
											s.total_sal*(1 - (IFNULL(inv.discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) as base_value,
											apc.creation_date_com as report_date,
											dea.percentage_dea as fixed_comission_percentage,
											dea.serial_dea
									FROM applied_comissions apc
									JOIN sales s ON s.serial_sal=apc.serial_sal
									JOIN invoice inv ON s.serial_inv=inv.serial_inv
									JOIN customer cus ON s.serial_cus=cus.serial_cus
									JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
									JOIN dealer dea ON dea.serial_dea=cnt.serial_dea AND dea.serial_dea = apc.serial_dea
									JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
									JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
									JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
									JOIN product pr ON pr.serial_pro=pxc.serial_pro
									JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
									WHERE apc.value_dea_com > 0";
									
									if($serial){
										$sql .= " AND apc.serial_dea='$serial'";
									}
									
									if($dealer_report){
										if($misc['card_number']){
											$sql.=" AND s.card_number_sal=".$misc['card_number'];
										}else{
											if($misc['status']=='PENDING'){
												$sql.=" AND apc.creation_date_com BETWEEN dea.last_commission_paid_dea AND NOW()";
											}else{
												$sql.=" AND apc.creation_date_com<=dea.last_commission_paid_dea";
											}
										}
									}else{
										if($type=='PENDING'){
											$sql.=" AND apc.creation_date_com BETWEEN dea.last_commission_paid_dea AND NOW()";
										}else{
											$sql.=" AND apc.creation_date_com<=dea.last_commission_paid_dea";
										}
									}

									if($startDate && $endDate){
										$sql.=" AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$startDate' , '%d/%m/%Y') 
														AND DATE_ADD(STR_TO_DATE('$endDate' , '%d/%m/%Y'), INTERVAL 23 HOUR)";
									}
									
									if($misc['serial_mbc']){
										$sql .= " AND dea.serial_mbc = ".$misc['serial_mbc'];
									}
									
									if($misc['serial_usr']){
										$sql .= " AND ubd.serial_usr = ".$misc['serial_usr'];
									}
									
									if($misc['serial_dea']){
										$sql .= " AND dea.dea_serial_dea = ".$misc['serial_dea'];
									}
									
									$final_statement = 'name_dea, ';
					break;
					
					case 'SUBMANAGER':
							$sql.=" SELECT	apc.serial_com,
											apc.value_dea_com, 
											apc.type_com,
											s.card_number_sal,
											inv.number_inv,CONCAT(IFNULL(last_name_cus,''),' ',first_name_cus) as name_cus,
											pbl.name_pbl,
											DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')as begin_date_sal,
											DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
											days_sal,
											b.name_dea,
											s.total_sal*(1 - (inv.discount_prcg_inv + other_dscnt_inv)/100) as base_value,
											apc.creation_date_com as report_date,
											dea.percentage_dea as fixed_comission_percentage
									FROM applied_comissions apc
									JOIN sales s ON s.serial_sal=apc.serial_sal
									JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
									JOIN dealer b ON b.serial_dea = cnt.serial_dea
									JOIN dealer dea ON dea.serial_dea = b.dea_serial_dea AND dea.manager_rights_dea = 'YES'
									JOIN invoice inv ON s.serial_inv=inv.serial_inv
									JOIN customer cus ON s.serial_cus=cus.serial_cus
									JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
									JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
									JOIN product pr ON pr.serial_pro=pxc.serial_pro
									JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
									WHERE apc.serial_dea='$serial'
									AND apc.value_dea_com > 0";
									
									if($dealer_report){
										if($misc['card_number']){
											$sql.=" AND s.card_number_sal=".$misc['card_number'];
										}else{
											if($misc['status']=='PENDING'){
												$sql.=" AND apc.creation_date_com BETWEEN dea.last_commission_paid_dea AND NOW()";
											}else{
												$sql.=" AND apc.creation_date_com<=dea.last_commission_paid_dea";
											}
										}
									}else{
										if($type=='PENDING'){
											$sql.=" AND apc.creation_date_com BETWEEN dea.last_commission_paid_dea AND NOW()";
										}else{
											$sql.=" AND apc.creation_date_com<=dea.last_commission_paid_dea";
										}
									}

									if($startDate && $endDate){
										$sql.=" AND STR_TO_DATE(DATE_FORMAT(apc.creation_date_com, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$startDate' , '%d/%m/%Y') 
														AND DATE_ADD(STR_TO_DATE('$endDate' , '%d/%m/%Y'), INTERVAL 23 HOUR)";
									}
					break;

			}
		}
		
		$sql.= " ORDER BY $final_statement s.card_number_sal";
		
		//die(Debug::print_r($sql));
		$result = $this->db -> Execute($sql);
		if($result){
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
				return $array;
			}else{
			return false;
			}
		}else{
			return false;
		}
	}

	function checkSalExist($serialSal){
		$sql="SELECT 1 FROM applied_comissions WHERE serial_sal=$serialSal";

		$result=$this->db->getOne($sql);
		if($result)
			return true;
		else
			return false;
	}
	/*
	  * @Name: hasPayedComissionsBySale
	  * @Description: Returns the row of the comission paid in this sale
	  * @Params: $serial_sal, $last_date_payed
	  * @Returns: the value paied
	  */
    public static function hasPayedComissionsBySale($db, $serial_sal, $last_date_payed){
		$sql = "SELECT apc.serial_com, apc.value_dea_com
				FROM applied_comissions apc
				WHERE apc.serial_sal = '$serial_sal'
				AND apc.creation_date_com <= STR_TO_DATE( '$last_date_payed', '%Y-%m-%d %T' )";

		$result = $db -> getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
    }

	/*
	 * @Name: getSalesStatus
	 * @Description: Returns an array of all the sale satatus in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of free sales.
	 */
    public static function getAppliedComissionsStatus($db) {
        $sql = "SHOW COLUMNS FROM applied_comissions LIKE 'status_com'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
	
	public static function getDealersManagersWithComissionsInCountry($db, $serial_cou, $serial_mbc){
		$sql = "SELECT DISTINCT mbc.serial_mbc, m.name_man
				FROM applied_comissions com
				JOIN dealer dea ON dea.serial_dea=com.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.serial_cou = $serial_cou
				JOIN manager m ON m.serial_man = mbc.serial_man
				WHERE com.value_dea_com>0";
				
		if($serial_mbc!='1'){
			$sql.=" AND dea.serial_mbc=".$serial_mbc;
		}
		
		$sql.=" ORDER BY m.name_man";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getDealersResponsiblesWithComissionsInMBC($db, $serial_mbc){
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_usr'
				FROM applied_comissions com
				JOIN dealer dea ON dea.serial_dea=com.serial_dea AND dea.serial_mbc = $serial_mbc
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr AND u.status_usr = 'ACTIVE'
				WHERE com.value_dea_com>0";
		
		$sql.=" ORDER BY name_usr";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getDealersWithComissions($db, $serial_mbc, $serial_cit, $subManager = FALSE, $serial_usr = NULL, $serial_dea = NULL){
		if($subManager){
			$sql = "SELECT DISTINCT dea.serial_dea, dea.code_dea, dea.name_dea
					FROM applied_comissions com
					JOIN dealer dea ON dea.serial_dea = com.serial_dea 
						AND manager_rights_dea = 'YES'
					JOIN sector sec ON sec.serial_sec = dea.serial_sec AND sec.serial_cit = $serial_cit
					JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
					WHERE com.value_dea_com > 0
					AND dea.phone_sales_dea = 'NO'";
		}else{
			$sql = "SELECT DISTINCT dea.serial_dea, dea.code_dea, dea.name_dea
					FROM applied_comissions com
					JOIN dealer brc ON brc.serial_dea=com.serial_dea AND brc.serial_mbc = $serial_mbc
					JOIN dealer dea ON dea.serial_dea=brc.dea_serial_dea
					JOIN user_by_dealer ubd ON ubd.serial_dea = brc.serial_dea AND ubd.status_ubd = 'ACTIVE'
					WHERE com.value_dea_com > 0
					AND dea.phone_sales_dea = 'NO'";
		}
		
		if($serial_usr){
			$sql .= " AND ubd.serial_usr = $serial_usr";
		}
		
		if($serial_dea){
			$sql .= " AND brc.serial_dea = $serial_dea";
		}
		
		$sql.=" ORDER BY name_dea";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function refundComission($db, $serial_com) {
		$sql="INSERT INTO applied_comissions 
					(SELECT NULL,
							ubd_serial_usr,
							serial_sal,
							serial_dea,
							dea_serial_usr,
							mbc_serial_usr,
							serial_mbc,
							fre_serial_usr,
							serial_fre,
							serial_ubd,
                            value_dea_com,
                            value_ubd_com,
                            value_mbc_com,
                            value_fre_com,
                            NOW(),
                            'REFUND',
                            NULL,
                            NULL,
                            NULL,
                            NULL
				FROM applied_comissions com
				WHERE com.serial_com={$serial_com}
				AND com.type_com = 'IN'
				ORDER BY creation_date_com)";
        
		$result = $db -> Execute($sql);
		if($result){
			return true;
		}else{
			return false;
		}
	}

    function getVerifyExistCommission($db, $serial_sal, $serial_dea){
        $sql = "SELECT * FROM applied_comissions
                WHERE serial_sal = $serial_sal
                AND serial_dea = $serial_dea
				AND type_com = 'IN' ";
        $result = $db->getAll($sql);
        if(count($result) > 0){
            // return TRUE if there is already a commission
            return TRUE;
        }
        else{
            return FALSE;
        }

    }

 	///GETTERS
	function getserial_com(){
		return $this->serial_com;
	}
	function getserial_sal(){
		return $this->serial_sal;
	}
	function getserial_dea(){
		return $this->serial_dea;
	}
	function getserial_mbc(){
		return $this->serial_mbc;
	}
	function getserial_fre(){
		return $this->serial_fre;
	}
	function getserial_ubd(){
		return $this->serial_ubd;
	}
	function getValue_dea_com(){
		return $this->value_dea_com;
	}
	function getValue_ubd_com(){
		return $this->value_ubd_com;
	}
	function getValue_mbc_com(){
		return $this->value_mbc_com;
	}
	function getValue_fre_com(){
		return $this->value_fre_com;
	}
	function getCreation_date_com(){
		return $this->creation_date_com;
	}
	function getType_com(){
		return $this->type_com;
	}

	function setSerial_com($serial_com){
		$this->serial_com = $serial_com;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal = $serial_sal;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea = $serial_dea;
	}
	function setSerial_mbc($serial_mbc){
		$this->serial_mbc = $serial_mbc;
	}
	function setSerial_fre($serial_fre){
		$this->serial_fre = $serial_fre;
	}
	function setSerial_ubd($serial_ubd){
		$this->serial_ubd = $serial_ubd;
	}
	function setValue_dea_com($value_dea_com){
		$this->value_dea_com = $value_dea_com;
	}
	function setValue_ubd_com($value_ubd_com){
		$this->value_ubd_com = $value_ubd_com;
	}
	function setValue_mbc_com($value_mbc_com){
		$this->value_mbc_com = $value_mbc_com;
	}
	function setValue_fre_com($value_fre_com){
		$this->value_fre_com = $value_fre_com;
	}
	function setCreation_date_com($creation_date_com){
		$this->creation_date_com = $creation_date_com;
	}
	function setType_com($type_com){
		$this->type_com = $type_com;
	}
	
}
?>