<?php
/*
File: ProfileByCountry.class.php
Author: Esteban Angulo
Creation Date: 28/12/2009 18:00
Last Modified: 03/02/2010
Modified By: David Bergmann
*/

class ProfileByCountry{
    var $db;
    var $serial_pbc;
    var $name_pbc;
	var $status_pbc;
    var $serial_cou;

    function __construct($db, $serial_pbc=NULL, $name_pbc=NULL, $serial_cou=NULL, $status_pbc=NULL)
    {
            $this -> db = $db;
            $this -> serial_pbc = $serial_pbc;
            $this -> name_pbc = $name_pbc;
			$this -> status_pbc = $status_pbc;
            $this -> serial_cou = $serial_cou;
    }

    /***********************************************
* funcion getData
* Returns all Data from a selected profile_by_country
***********************************************/
    function getData(){
            if($this->serial_pbc!=NULL){
                    $sql = 	"SELECT serial_pbc, name_pbc, serial_cou, status_pbc
                                    FROM profile_by_country
                                    WHERE serial_pbc='".$this->serial_pbc."'";
                    $result = $this->db->Execute($sql);

                    if ($result === false)
                            return false;

                    if($result->fields[0]){
                            $this -> serial_pbc = $result->fields[0];
                            $this -> name_pbc = $result-> fields[1];
                            $this -> serial_cou = $result ->fields[2];
							$this -> status_pbc = $result ->fields[3];
                            return true;
                    }
                    else
                            return false;

            }else
                return false;
    }

    function getParentsOptions($code_lang){
    	$allMenusSetup = array();
    	if($this -> serial_pbc != NULL){
			$sql = "SELECT pc.serial_opt
					  FROM opt_by_profcountry pp, options pc
					  WHERE pp.serial_opt = pc.serial_opt
					  AND serial_pbc = $this->serial_pbc
					  ORDER BY pc.serial_opt";
			
			$result = $this->db->Execute($sql);
			$numOptions = $result -> RecordCount();
			if($numOptions > 0){
                $childrenIds = array();
                $cont=1;
                do{
    				$value = $result->GetRowAssoc(false);
                	$childrenIds[$cont++] = $value['serial_opt'];
    				$result->MoveNext();
                }while($cont <= $numOptions);
        	}
        	
        	//CALL THIS RECURSIVE FUNCTION TO BUILDUP THE OPTIONS ARRAY
        	$this -> mergeOptions($allMenusSetup, $childrenIds, $code_lang);
		}
		$menuArray = array();
		foreach($allMenusSetup as $men){
			$menuArray[] = $men;
		}
		$this -> arrangeParents($menuArray);
		return $menuArray;
    }
    
	function mergeOptions(&$allMenusSetup, $childrenIds, $code_lang){
        $allMenusSetup = array_merge($allMenusSetup,$this -> getOptionsContent(implode(",", $childrenIds), $code_lang));
        $parentIds = $this -> getOptionsParents(implode(",", $childrenIds));
        if(count($parentIds) > 0){
        	$this -> mergeOptions($allMenusSetup, $parentIds, $code_lang);
        }else{
        	return $allMenusSetup;
        }
	}
    
	function getOptionsContent($optionsSerials, $code_lang){
		$optionsContent = array();
		$sql = "SELECT op.serial_opt, name_obl, op.link_opt, op.show_condition_opt, op.opt_serial_opt, op.weight_opt 
				FROM options op JOIN options_by_language obl ON obl.serial_opt=op.serial_opt 
				JOIN language l ON l.serial_lang=obl.serial_lang 
				AND l.code_lang='$code_lang' 
				WHERE op.serial_opt IN($optionsSerials) ORDER BY weight_opt";
		$result = $this -> db->Execute($sql);
		$numOptions = $result -> RecordCount();
		if($numOptions > 0){
             $cont = 1;
             do{
             	$record = $result->GetRowAssoc(false);
                $optionsContent['opt_' . $record['serial_opt']] = $record; //THE KEY MUST BE NON NUMERICAL TO ASSURE UNIQUENESS ON array_merge
                $cont++;
    			$result -> MoveNext();
             }while($cont <= $numOptions);
        }
        return $optionsContent;
	}
	
	function getOptionsParents($optionsSerials){
		$optionsIds = array();
		$sql = "SELECT DISTINCT p.opt_serial_opt FROM options p WHERE p.serial_opt IN ($optionsSerials)";
		$result = $this -> db->Execute($sql);
		$numOptions = $result -> RecordCount();
		if($numOptions > 0){
             $cont = 1;
             do{
                $value = $result->GetRowAssoc(false);
                if($value['opt_serial_opt'] != ''){
                	$optionsIds[$cont] = $value['opt_serial_opt'];
                }
                $cont++;
    			$result->MoveNext();
             }while($cont <= $numOptions);
        }
        return $optionsIds;
	}
	function arrangeParents(&$allMenusSetup) {
		if(is_array($allMenusSetup)) {
			for($i=0;$i<count($allMenusSetup)-1;$i++){
				for($j=$i+1;$j<count($allMenusSetup);$j++){
					if(!$allMenusSetup[$i]['opt_serial_opt'] && !$allMenusSetup[$j]['opt_serial_opt'] && $allMenusSetup[$i]['weight_opt']>$allMenusSetup[$j]['weight_opt']) {
						$aux = $allMenusSetup[$i];
						$allMenusSetup[$i] = $allMenusSetup[$j];
						$allMenusSetup[$j] = $aux;
					}
				}
			}
		}
	}
	
	/***********************************************
    * funcion getActivProfile_by_countryList
    * Returns all profile_by_countrys which Status is ACTIVE
    ***********************************************/
	function getActiveProfile_by_countryList($serial_cou){
	$sql = 	"SELECT p.serial_pbc, p.name_pbc, p.status_pbc
				 FROM profile_by_country p 
				 WHERE p.status_pbc='ACTIVE' AND (p.serial_cou=1 OR p.serial_cou IN(".$serial_cou."))";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
    * funcion getActivProfile_by_countryList
    * Returns all profile_by_countrys which Status is ACTIVE
    ***********************************************/
	function getProfile_by_countryList($serial_cou){
	$sql = 	"SELECT p.serial_pbc, p.name_pbc, p.status_pbc
				 FROM profile_by_country p 
				 WHERE p.serial_cou=1 OR p.serial_cou IN(".$serial_cou.")";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}

	/***********************************************
    * funcion getActiveProfilesForConter
    * Returns all profile_by_country which Status is ACTIVE and a name which contains counter
    ***********************************************/
	function getActiveProfilesForConter($serial_cou){
	$sql = 	"SELECT p.serial_pbc, p.name_pbc, p.status_pbc
				 FROM profile_by_country p
				 WHERE p.status_pbc='ACTIVE' AND 
				 p.name_pbc LIKE '%Counter%' AND
				(p.serial_cou=1 OR p.serial_cou ='".$serial_cou."')";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}

	/***********************************************
    * funcion getProfile_by_countrySerialByName
    * Returns the serial of  profile_by_country which 
	*name is a paramter
    ***********************************************/
	function getProfile_by_countrySerialByName($name_pbc){
	$sql = 	"SELECT p.serial_pbc
				 FROM profile_by_country p 
				 WHERE p.name_pbc='".$name_pbc."'";
		//echo $sql;
		$result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return $result->fields[0];
        else
            return false;
	}
	
	/***********************************************
    * funcion getprofile_by_countries
    * Returns all profile_by_countries
    ***********************************************/
	function getprofile_by_countrys($serial_cou=NULL){
	$sql = 	"SELECT p.serial_pbc, p.name_pbc, p.status_pbc
		     FROM profile_by_country p";

	if($serial_cou!='' && $serial_cou!=NULL){
		$sql.=" WHERE p.serial_cou=".$serial_cou;
	}

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
		/***********************************************
    * funcion getProfiles
    * Returns all Profiles
    ***********************************************/
	function getProfiles(){
	$sql = 	"SELECT p.serial_pbc, p.name_pbc, p.status_pbc
				 FROM profile_by_country p";
				 
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
    * funcion getAllStatus
    * Returns all kind of Status which exist in DB
    ***********************************************/
	function getAllStatus(){
		// Finds all possible status form ENUM column 
		$sql = "SHOW COLUMNS FROM profile_by_country LIKE 'status_pbc'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
    * function insert
    * inserts a new register en DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO profile_by_country (
				serial_cou,
				name_pbc,
				status_pbc
				)
            VALUES(
                '".$this->serial_cou."',
				'".$this->name_pbc."',";
        $sql.=	"'".$this->status_pbc."')";
		
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
	/***********************************************
    * function update
    * updates a  register in DB 
    ***********************************************/
	function update(){
		$sql = "
			UPDATE `profile_by_country` SET 
				`serial_pbc` = '".$this->serial_pbc."',
				`serial_cou` = '".$this->serial_cou."',
				`name_pbc` = '".$this->name_pbc."',
				`status_pbc` = '".$this->status_pbc."'";
				
			
		$sql.="	WHERE `profile_by_country`.`serial_pbc` = '".$this->serial_pbc."'";	
		//echo $sql;
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	/***********************************************
    * function delete
    * deletes a  register in DB 
    ***********************************************/
	function delete(){
		$sql = "
			DELETE FROM profile_by_country
			WHERE serial_pbc = '".$this->serial_pbc."'";	
		//echo $sql;		
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	/***********************************************
	* funcion insertopt_by_profcountry
	* inserts a new register of an option related to a profile_by_country 
	***********************************************/
	function insertOpt_by_profcountry($serial_pbc,$serial_opt){		
		$sql = "INSERT INTO opt_by_profcountry (
									serial_opt,
									serial_pbc)
				VALUES (
									'".$serial_opt."',
									'".$serial_pbc."')";

		$result = $this->db->Execute($sql);
		if ($result === false) return false;
		
		if($result==true)
			return true;
		else
			return false;
	}
	
		/***********************************************
    * funcion existsprofile_by_country
    * verifies if a profile exists or not
    ***********************************************/
	function existsprofile_by_country($name_pbc,$serial_cou,$serial_pbc){
		$sql = 	"SELECT name_pbc
                         FROM profile_by_country
			 WHERE name_pbc= '".utf8_encode($name_pbc)."'
                         AND (serial_cou = '".$serial_cou."'
                         OR serial_cou = '1')";
                

		if($serial_pbc != NULL && $serial_pbc != ''){
			$sql.= " AND serial_pbc <> ".$serial_pbc;
		}
		//echo $sql;
		$result = $this->db -> Execute($sql);
		
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	
	/***********************************************
	* funcion Optionsbyprofile_by_country
	* Recovers form DB all options for a profile_by_country
	***********************************************/
	function getOptionsbyProfile($serial_pbc){
		$sql = 	"SELECT p.serial_opt
				FROM opt_by_profcountry p
				WHERE p.serial_pbc=$serial_pbc";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
	* funcion verifyName
	* inserts a register from an option related to the profile_by_country 
	***********************************************/
	function verifyName(){
		if($this->nombre_pbc != NULL){
			$sql = 	"SELECT p.name_pbc
					FROM profile_by_country p
					WHERE p.name_pbc = '".$this->name_pbc."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields['name_pbc']){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/***********************************************
	* funcion deleteOptions_by_Profile
	* Deletes an Option for a selected profile_by_country
	***********************************************/
	function deleteOptions_by_Profile(){
		$sql = "
			DELETE FROM opt_by_profcountry
			WHERE serial_pbc='".$this->serial_pbc."'";
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false)return false;
		
		if($result==true)
			return true;
		else
			return false;
	}
	
	
	/***********************************************
	* funcion verifyNameUpdate
	* checks if a name already exists in DB
	***********************************************/
	function verifyNameUpdate(){
		if($this->nombre_pbc != NULL){
			$sql = 	"SELECT p.name_pbc
					FROM profile_by_country p
					WHERE p.name_pbc = '".$this->name_pbc."'
					AND p.serial_pbc <> ".$this->serial_pbc;
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields['name_pbc']){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

    /***********************************************
    @Name: show_options
    @Description: Returns all options from the sistem or all options for a selected profile
    @Params: code_lang -> Language code
    @Returns: arr_final -> array with all parent->child relationships of all the menu options
    ***********************************************/
	function show_options($code_lang){
	//this SQL returns all super parent options
			$sql = "SELECT *
					FROM (	SELECT 	 op.serial_opt, 
									 name_obl, 
									 op.link_opt, 
									 op.show_condition_opt, 
									 op.opt_serial_opt, 
									 op.weight_opt
							FROM options op 
							JOIN options_by_language obl ON obl.serial_opt=op.serial_opt
							JOIN language l ON l.serial_lang=obl.serial_lang 
											AND l.code_lang='es'
							WHERE op.serial_opt IN (
								SELECT op.opt_serial_opt
								FROM options op 
								JOIN options_by_language obl ON obl.serial_opt=op.serial_opt
								JOIN language l ON l.serial_lang=obl.serial_lang 
												AND l.code_lang='".$code_lang."'
								WHERE op.serial_opt IN (
										SELECT pn.opt_serial_opt
										FROM (
										  SELECT DISTINCT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
										  FROM options p JOIN options_by_language ol ON ol.serial_opt=p.serial_opt
										  JOIN language l ON l.serial_lang=ol.serial_lang AND l.code_lang='".$code_lang."'
										  WHERE p.serial_opt
										  IN (
												  SELECT pc.opt_serial_opt
												  FROM opt_by_profcountry pp, options pc
												  WHERE pp.serial_opt = pc.serial_opt";
												/* if($this->serial_pbc!='' && $this->serial_pbc!=NULL){
													 $sql.=" AND serial_pbc = ".$this->serial_pbc;
												 }*/
												  $sql.=" ORDER BY pc.serial_opt
												 )
								  		)pn
							 		)
							)
							UNION
							(	SELECT DISTINCT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
								FROM options p JOIN options_by_language obl ON obl.serial_opt=p.serial_opt
								JOIN language l ON l.serial_lang=obl.serial_lang 
											AND l.code_lang='".$code_lang."'
							 	WHERE p.serial_opt
							 		IN (
										SELECT p.opt_serial_opt
										FROM opt_by_profcountry pp, options p
										WHERE pp.serial_opt = p.serial_opt";
											if($this->serial_pbc!='' && $this->serial_pbc!=NULL){
												$sql.=" AND serial_pbc =".$this->serial_pbc;
											}
											$sql.=" ORDER BY p.serial_opt
											)
							)
						UNION
						(
							SELECT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
							FROM opt_by_profcountry pp, options p, options_by_language obl, language l
							WHERE pp.serial_opt = p.serial_opt";
								if($this->serial_pbc!='' && $this->serial_pbc!=NULL){
									$sql.=" AND serial_pbc =".$this->serial_pbc;
								}
								$sql.=" AND obl.serial_opt=pp.serial_opt
							AND l.serial_lang=obl.serial_lang
							AND l.code_lang='".$code_lang."'
							ORDER BY p.serial_opt
						)
						ORDER BY opt_serial_opt,weight_opt) AS alloptions
					WHERE alloptions.opt_serial_opt IS NULL";
		//echo $sql."<br/><br/>";
		//echo"****";
		$result = $this -> db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr = array();
			$cont = 0;

			do{
				$arr[$cont++] = $result -> fields['serial_opt'];
				$result -> MoveNext();
			}while($cont<$num);
		}

		if(is_array($arr)){
			$arr=implode(',', $arr); // modifing the array for use it in the next sql
		
		//this sql search parent (the child options of the Super Parent)
		$sql="SELECT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
			FROM opt_by_profcountry pp
			JOIN options p ON  p.serial_opt = pp.serial_opt
			JOIN options_by_language obl ON obl.serial_opt=pp.serial_opt
			JOIN language l ON l.serial_lang=obl.serial_lang
			WHERE p.serial_opt IN (".$arr.")
			AND l.code_lang='".$code_lang."'
			AND p.opt_serial_opt IS NULL
			ORDER BY p.serial_opt";
		//echo $sql.'<br/>';
		$rs = @mysql_query($sql);
		if($rs){
			while($cat = mysql_fetch_array($rs, MYSQL_ASSOC)){
					$arr_final[] = ProfileByCountry::show_suboptions($code_lang,$this->serial_pbc,$cat); // call the function to search for the childs of the parent
			}
			//Debug::print_r($arr_final);
			//echo'****';
			return $arr_final;
			
		}
	  }
	}

    /***********************************************
    @Name: show_suboptions
    @Description: Returns all sub options for a parent option
    @Params: $code_lang -> Language code
	 *		 $profile-> serial profile
	 *		 $parent-> serial of the parent option
    @Returns: arr_final -> array with all parent->child relationships of all the menu options
    ***********************************************/
	public static function show_suboptions($code_lang, $profile=NULL, $parent) {
		// sql that search the children of the parent option across all options of the system
		$sql = "SELECT *
				FROM ((SELECT op.serial_opt, name_obl, op.link_opt, op.show_condition_opt, op.opt_serial_opt, op.weight_opt
						FROM options op JOIN options_by_language obl ON obl.serial_opt=op.serial_opt
						JOIN language l ON l.serial_lang=obl.serial_lang AND l.code_lang='".$code_lang."'
						WHERE op.serial_opt
						IN (
								SELECT pn.opt_serial_opt
								FROM (
										  SELECT DISTINCT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
										  FROM options p JOIN options_by_language ol ON ol.serial_opt=p.serial_opt
										  JOIN language l ON l.serial_lang=ol.serial_lang AND l.code_lang='".$code_lang."'
										  WHERE p.serial_opt
										  IN (
												  SELECT pc.opt_serial_opt
												  FROM opt_by_profcountry pp, options pc
												  WHERE pp.serial_opt = pc.serial_opt";
												 if($profile!='' && $profile!=NULL){
													 $sql.=" AND serial_pbc = ".$profile;
												 }
												  $sql.=" ORDER BY pc.serial_opt
												 )
								  )pn
							 )
						)UNION
						(SELECT DISTINCT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
						FROM options p JOIN options_by_language obl ON obl.serial_opt=p.serial_opt
						JOIN language l ON l.serial_lang=obl.serial_lang AND l.code_lang='".$code_lang."'
						WHERE p.serial_opt
						IN (
								SELECT p.opt_serial_opt
								FROM opt_by_profcountry pp, options p
								WHERE pp.serial_opt = p.serial_opt";
								if($profile!='' && $profile!=NULL){
									$sql.=" AND serial_pbc =".$profile;
								}
								$sql.=" ORDER BY p.serial_opt
								)
						)
						UNION
						(SELECT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
						FROM opt_by_profcountry pp, options p, options_by_language obl, language l
						WHERE pp.serial_opt = p.serial_opt";
						if($profile!='' && $profile!=NULL){
							$sql.=" AND serial_pbc =".$profile;
						}
						$sql.=" AND obl.serial_opt=pp.serial_opt
						AND l.serial_lang=obl.serial_lang
						AND l.code_lang='".$code_lang."'
						 ORDER BY p.serial_opt)
						 ORDER BY opt_serial_opt,weight_opt) AS alloptions
					WHERE alloptions.opt_serial_opt='".$parent['serial_opt']."'";
						
		//echo $sql.'<br/><br/>';
		$result = mysql_query($sql);
		if($result) {
			while($subcat = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$parent['options'][]=ProfileByCountry::show_suboptions($code_lang,$profile,$subcat); //Recursive call
			}	
		}
		return $parent;
	}

	//END TEST

	//getters
	function getSerial_pbc(){
		return $this->serial_pbc;
	}
	function getName_pbc(){
		return $this->name_pbc;
	}
	function getStatus_pbc(){
		return $this->status_pbc;
	}
	function getSerial_cou(){
		 return $this->serial_cou;
	}
	
	//setters
	function setSerial_pbc($serial_pbc){
		 $this->serial_pbc = $serial_pbc;
	}
	function setName_pbc($name_pbc){
		 $this->name_pbc = $name_pbc;
	}
	function setStatus_pbc($status_pbc){
		 $this->status_pbc = $status_pbc;
	}
	function setSerial_cou($serial_cou){
		 $this->serial_cou = $serial_cou;
	}

}

?>