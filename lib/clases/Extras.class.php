<?php
/*
File:Extras.class.php
Author: Esteban Angulo
Creation Date: 17/03/2010
*/
class Extras{
    var $db;
    var $serial_ext;
    var $serial_sal;
    var $serial_cus;
    var $serial_trl;
    var $relationship_ext;
    var $fee_ext;
    var $cost_ext;

    function __construct($db, $serial_ext = NULL, $serial_sal = NULL, $serial_cus = NULL, $serial_trl = NULL,
            $relationship_ext = NULL, $fee_ext= NULL, $cost_ext= NULL){
		$this -> db = $db;
                $this -> serial_ext = $serial_ext;
                $this -> serial_sal = $serial_sal;
                $this -> serial_cus = $serial_cus;
                $this -> serial_trl = $serial_trl;
                $this -> relationship_ext = $relationship_ext;
                $this -> fee_ext = $fee_ext;
                $this -> cost_ext = $cost_ext;
    }
      /***********************************************
    * function getData
    @Name: getData
    @Description: Returns all data
    @Params:
     *       N/A
    @Returns: true
     *        false
    ***********************************************/

    function getData(){
        if($this -> serial_ext != NULL){
            $sql = 	"SELECT e.serial_ext,
                                e.serial_sal,
                                e.serial_cus,
                                e.serial_trl,
                                e.relationship_ext,
                                e.fee_ext,
                                e.cost_ext
                            FROM extras e
                            WHERE e.serial_ext ='".$this -> serial_ext."'";
        }else{
        	$sql = 	"SELECT e.serial_ext,
                                e.serial_sal,
                                e.serial_cus,
                                e.serial_trl,
                                e.relationship_ext,
                                e.fee_ext,
                                e.cost_ext
                            FROM extras e
                            WHERE e.serial_sal ='".$this -> serial_sal."' AND e.serial_cus = " . $this -> serial_cus;
        }
        $result = $this->db->Execute($sql);

        if($result->fields[0]){
			$this -> serial_ext =$result->fields[0];
            $this -> serial_sal = $result->fields[1];
            $this -> serial_cus = $result->fields[2];
            $this -> serial_trl = $result->fields[3];
            $this -> relationship_ext = $result->fields[4];
            $this -> fee_ext = $result->fields[5];
            $this -> cost_ext = $result->fields[6];
            return true;
        }
        return false;
    }

    /*******************************************
    @Name: getAllRelationships
    @Description: Returns an array of users.
    @Params: N/A
    @Returns: An Array with all types of relationships
    ********************************************/
    public static function getAllRelationships($db){
            // Finds all possible status form ENUM column
            $sql = "SHOW COLUMNS FROM extras LIKE 'relationship_ext'";
            $result = $db -> Execute($sql);

            $type = $result->fields[1];

            $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
            return $type;
    }
	
    /*******************************************
    @Name: insert
    @Description: Returns an array of users.
    @Params: N/A
    @Returns: An Array with all types of relationships
    ********************************************/
	function insert() {
        $sql = "
            INSERT INTO extras (
				serial_ext,
				serial_sal,
				serial_cus,
				serial_trl,
				relationship_ext,
				fee_ext,
				cost_ext)
            VALUES(
				NULL,";
					$sql.=($this->serial_sal)?"'".$this->serial_sal."',":"NULL,";
					$sql.=($this->serial_cus)?"'".$this->serial_cus."',":"NULL,";
					$sql.=($this->serial_trl)?"'".$this->serial_trl."',":"NULL,";
					$sql.=($this->relationship_ext)?"'".$this->relationship_ext."',":"NULL,";
					$sql.=($this->fee_ext!=0)?"'".$this->fee_ext."',":"0.0,";
					$sql.=($this->cost_ext!=0)?"'".$this->cost_ext."'":"0.0";
			$sql.=")";
        $result = $this->db->Execute($sql);
        
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	    /*******************************************
    @Name: delete
    @Description: Erases all extras from a sale
    @Params: N/A
    @Returns: N/A
    ********************************************/
	function delete($serial_sal) {
        $sql = "
				DELETE FROM extras
				WHERE serial_sal='".$serial_sal."'";
//echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if($result==true)
            return $this->db->insert_ID();
        else
            return false;
    }

	 /*******************************************
    @Name: getExtrasBySale
    @Description: Returns an array of customers who are extras in a sale
    @Params: serial_sal:
    @Returns: An Array with all types of relationships
    ********************************************/
	public static function  getExtrasBySale($db, $serial_sal){
		$sql="	SELECT e.serial_ext, e.serial_sal,e.serial_cus,c.document_cus,e.relationship_ext,
					   c.first_name_cus,c.last_name_cus,DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as birthdate_cus,
					   c.email_cus,e.fee_ext,e.cost_ext
				FROM extras e
				JOIN customer c ON e.serial_cus=c.serial_cus
				WHERE e.serial_sal='".$serial_sal."'";
		//die($sql);
		$result = $db->Execute($sql);
			 $num = $result -> RecordCount();
				if($num > 0){
					$arreglo=array();
					$cont=0;

					do{
						$arreglo[$cont]=$result->GetRowAssoc(false);
						$result->MoveNext();
						$cont++;
					}while($cont<$num);
				}
				return $arreglo;
	}

    //GETTERS
    function getSerial_ext(){
        return $this->serial_ext;
    }
    function getSerial_sal(){
        return $this->serial_sal;
    }
    function getSerial_cus(){
        return $this->serial_cus;
    }
    function getSerial_trl(){
        return $this->serial_trl;
    }
    function getRelationship_ext(){
        return $this->relationship_ext;
    }
    function getFee_ext(){
        return $this->fee_ext;
    }
    function getCost_ext(){
        return $this->cost_ext;
    }

    //SETTERS
    function setSerial_ext($serial_ext){
            $this->serial_ext = $serial_ext;
    }
    function setSerial_sal($serial_sal){
        $this->serial_sal = $serial_sal;
    }
    function setSerial_cus($serial_cus){
        $this->serial_cus = $serial_cus;
    }
    function setSerial_trl($serial_trl){
        $this->serial_trl = $serial_trl;
    }
    function setRelationship_ext($relationship_ext){
        $this->relationship_ext = $relationship_ext;
    }
    function setFee_ext($fee_ext){
        $this->fee_ext = $fee_ext;
    }
    function setCost_ext($cost_ext){
        $this->cost_ext = $cost_ext;
    }

}

?>
