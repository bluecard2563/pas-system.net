<?php
/*
    Document   : AssistanceGroupsByUsers.class.php
    Created on : Apr 28, 2010, 10:41:16 AM
    Author     : H3Dur4k
    Last Modified: 16-ago-2011, 10:00:00
    Modified by: Nicolas Flores
    Description:
        Purpose of the php follows.
*/
class AssistanceGroupsByUsers{
	var $db;
	var $serial_usr;
	var $serial_asg;
	var $status_agu;
	var $type_agu;

	function __construct($db, $serial_usr=NULL, $serial_asg = NULL, $status_agu=NULL,$type_agu=NULL) {
		$this -> db = $db;
		$this -> serial_usr = $serial_usr;
		$this -> serial_asg = $serial_asg;
		$this -> status_agu = $status_agu;
		$this -> type_agu = $type_agu;

	}

		/***********************************************
    * function insert
    * inserts a new register in the DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO assistance_groups_by_users (
        		serial_usr,
				serial_asg,
				status_agu,
                type_agu)
            VALUES(	'".$this->serial_usr."',
					'".$this->serial_asg."',
					'".$this->status_agu."',
            		'".$this->type_agu."')";
       	//die($sql);
        $result = $this->db->Execute($sql);
        
    	if($result){
            return true;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

 	/***********************************************
    * funcion update
    * updates a register in the DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE assistance_groups_by_users  SET
				status_agu ='{$this->status_agu}',
        		type_agu ='{$this->type_agu}'
				WHERE serial_usr ='{$this->serial_usr}' AND serial_asg = '{$this->serial_asg}'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        return $result;
    }
	/***********************************************
	* function getCurrentUserGroup
	@Name: getCurrentUserGroup
	@Description: get the current user for an operator user,It is only for OPERATORS
	@Params:
         *       object atribute $this->serial_usr
	@Returns: serial_asg/false
	***********************************************/
	function getCurrentUserGroup(){
		$sql="SELECT serial_asg
			FROM assistance_groups_by_users
			WHERE serial_usr={$this->serial_usr}
			AND status_agu='ACTIVE'";

		$result=$this->db->getOne($sql);
		if($result)
			return $result;
		else
			return false;
	}
	
	function autoLoadGroupInfo(){
		$sql="SELECT serial_asg, status_agu, type_agu
			FROM assistance_groups_by_users
			WHERE serial_usr={$this->serial_usr}
			AND status_agu='ACTIVE'";
		$result = $this -> db -> Execute($sql);
		if($result){
			$this -> serial_asg = $result -> fields['serial_asg'];
			$this -> status_agu = $result -> fields['status_agu'];
			$this -> type_agu = $result -> fields['type_agu'];
			return true;
		}else{
			return false;
		}
	}
	
	
	/***********************************************
	* function getUsersInGroup
	@Name: getUsersInGroup
	@Description: gets a list of users in a group
	@Params:
         *       object atribute $this->serial_asg
	@Returns: users array/false
	***********************************************/
	function getUsersInGroup(){
		$sql="SELECT agu.serial_usr,agu.status_agu,agu.type_agu
			  FROM assistance_groups_by_users agu
			WHERE agu.serial_asg={$this->serial_asg}";
			//echo $sql;
		$result=$this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getOpenedFilesByDate($db, $serial_asg, $date){
		$sql = "SELECT DATE_FORMAT(f.creation_date_fle,'%h:%i %p') as creation_hour,f.diagnosis_fle, f.status_fle, f.cause_fle, 
						CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'opening_usr',
						SUM(IFNULL(estimated_amount_spf, 0)) AS 'estimated_amount', f.serial_fle, s.card_number_sal
				FROM assistance_groups_by_users asg
				JOIN file f ON f.opening_usr = asg.serial_usr 
					AND asg.serial_asg = $serial_asg 
					AND DATE_FORMAT(f.creation_date_fle,'%d/%m/%Y') = '$date'
				JOIN user u ON u.serial_usr = asg.serial_usr
				JOIN sales s ON s.serial_sal = f.serial_sal
				LEFT JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle
				GROUP BY f.serial_fle
				ORDER BY opening_usr,creation_hour, f.serial_fle";
		//die(Debug::print_r($sql));
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getSerial_asg(){
		return $this->serial_asg;
	}
	function getStatus_agu(){
		return $this->status_agu;
	}
	function getType_agu(){
		return $this->type_agu;
	}

	///SETTERS
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setSerial_asg($serial_asg){
		$this->serial_asg = $serial_asg;
	}

	function setStatus_agu($status_agu){
		$this->status_agu = $status_agu;
	}
	function setType_agu($type_agu){
		$this->type_agu = $type_agu;
	}
}
?>
