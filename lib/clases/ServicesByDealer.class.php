<?php
/*
File: ServicesByDealer.class.php
Author: Esteban Angulo
Creation Date: 09/03/2010
*/

class ServicesByDealer{

    var $db;
    var $serial_sbd;
    var $serial_dea;
    var $serial_sbc;
    var $status_sbd;

    function __construct($db, $serial_sbd=NULL, $serial_dea=NULL, $serial_sbc=NULL, $status_sbd = NULL){
        $this -> db = $db;
        $this -> serial_sbd = $serial_sbd;
        $this -> serial_dea = $serial_dea;
        $this -> serial_sbc = $serial_sbc;
        $this -> status_sbd = $status_sbd;
    }


      /***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
    function getData(){
        if($this->serial_sbd!=NULL){
            $sql = "SELECT serial_sbd,
                           serial_dea,
                           serial_sbc,
                           status_sbd
                    FROM services_by_dealer
                    WHERE serial_sbd='".$this->serial_sbd."'";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_sbd =$result->fields[0];
                $this -> serial_dea = $result->fields[1];
                $this -> serial_sbc = $result->fields[2];
                $this -> status_sbd = $result->fields[3];

                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }

     function getDealerServiceByCountry($serial_sbc){
        $sql= "SELECT sbd.serial_dea
                FROM services_by_dealer sbd
                WHERE sbd.serial_sbc= '".$serial_sbc."'";
//echo $sql;
         $result = $this->db->Execute($sql);
         $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->fields[0];
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }else{
                return 0;
            } $sql= "SELECT sbd.serial_dea
                FROM services_by_dealer sbd
                WHERE sbd.serial_sbc= '".$serial_sbc."'";
            //Debug::print_r($arreglo);die;
            return $arreglo;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO services_by_dealer (
                                serial_sbd,
                                serial_dea,
                                serial_sbc,
                                status_sbd
                                )
            VALUES (
                    NULL,
                    '".$this->serial_dea."',
                    '".$this->serial_sbc."',
                    '".$this->status_sbd."'
                    )";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: update
    @Description: update a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function update(){
        if($this->status_sbd != NULL && ($this->serial_sbc || $this->serial_sbd)){
            $sql="UPDATE services_by_dealer
                  SET status_sbd = '".$this->status_sbd."'
                  WHERE ";

            if($this->serial_sbc){
                $sql .= "serial_sbc = '".$this->serial_sbc."'";
            }elseif($this->serial_sbd){
                $sql .= "serial_sbd = '".$this->serial_sbd."'";
            }

            $result = $this->db->Execute($sql);
            //echo $sql." ";
            if ($result == true){
                return true;
            }else{
                return false;
            }
        }
		else
			return true;
    }


        /**
    @Name: getServicesByDealer
    @Description: returns all services assosiated to a dealer
    @Params: serial_dea
    @Returns: an array with all the information
    **/
    function getServicesByDealer($serial_dea, $serial_lang, $serial_cou){
        $sql= "SELECT sbd.serial_sbd, sbd.serial_sbc, sbl.name_sbl, s.fee_type_ser, sbc.price_sbc, sbc.cost_sbc, sbl.serial_sbl
                FROM services_by_dealer sbd
                JOIN services_by_country sbc ON sbd.serial_sbc=sbc.serial_sbc
                JOIN services_by_language sbl ON sbl.serial_ser=sbc.serial_ser
                JOIN services s ON s.serial_ser=sbl.serial_ser
                WHERE sbd.serial_dea = '".$serial_dea."'
                AND sbl.serial_lang = '".$serial_lang."'
                AND sbd.status_sbd = 'ACTIVE'
                AND sbc.status_sbc = 'ACTIVE'
                AND s.status_ser = 'ACTIVE'
                AND sbc.serial_cou = ".$serial_cou;

         $result = $this->db->Execute($sql);
         $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }
            //Debug::print_r($arreglo);
            return $arreglo;

    }

    function serviceByDealerExists(){
        if($this->serial_dea != NULL && $this->serial_sbc != NULL){
            $sql = "SELECT sbd.serial_sbd
                    FROM services_by_dealer sbd
                    WHERE sbd.serial_dea = '".$this->serial_dea."'
                    AND sbd.serial_sbc = '".$this->serial_sbc."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return true;
        }
    }

    function getIsUsed_dea(){
        if($this->serial_sbd != NULL){
            $sql="SELECT scs.serial_sbd, sbd.serial_dea
                  FROM services_by_dealer sbd
                  JOIN services_by_country_by_sale scs
                  ON sbd.serial_sbd=scs.serial_sbd
                  JOIN sales s ON s.serial_sal = scs.serial_sal
                  WHERE sbd.serial_sbd='".$this->serial_sbd."'";
            $result = $this->db->Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }else{
                return 0;
            }
            //Debug::print_r($arreglo);
            return $arreglo;
        }
    }

    /**
    @Name: serviceByCountryExist
    @Description: verifies if there are any service by country assigned.
    @Params: serial_cou
    @Returns: TRUE if OK / FALSE otherwise.
    **/
    static function serviceByCountryExist($db, $serial_cou){
        $sql= "SELECT sbc.serial_sbc
                FROM services_by_country sbc
                WHERE sbc.status_sbc = 'ACTIVE'
                AND sbc.serial_cou = ".$serial_cou;

         $result = $db->Execute($sql);

         if($result->fields['0'])
             return true;
         else
             return false;
    }
	
	static function getServiceData($db, $serial_sbd){
		$sql= "SELECT s.fee_type_ser, spc.price_sbc, spc.cost_sbc
                FROM services s
				JOIN services_by_country spc ON s.serial_ser = spc.serial_ser
				JOIN services_by_dealer sbd ON sbd.serial_sbc = spc.serial_sbc AND 
                spc.status_sbc = 'ACTIVE' AND sbd.serial_sbd = ".$serial_sbd;

         $result = $db->getRow($sql);

         if($result)
             return $result;
         else
             return false;
	}
    
    /******************************VA
		 * @name: getServicesByDealer
		 * @return: array services by dealer
                 * POST: 
		 */
       
        public static function getServicesByDealerWS($db, $serial_cnt, $serial_lang, $serial_cou){
        $sql= " SELECT sbd.serial_sbc, sbd.serial_sbd, sbl.name_sbl, s.fee_type_ser, sbc.price_sbc, sbc.cost_sbc, sbl.description_sbl, sbc.coverage_sbc
                FROM services_by_dealer sbd
                JOIN services_by_country sbc ON sbd.serial_sbc=sbc.serial_sbc
                JOIN services_by_language sbl ON sbl.serial_ser=sbc.serial_ser
                JOIN services s ON s.serial_ser=sbl.serial_ser
                JOIN dealer dea ON dea.dea_serial_dea = sbd.serial_dea 
                JOIN counter cnt ON cnt.serial_dea = dea.serial_dea 
                WHERE cnt.serial_cnt = '$serial_cnt'
                AND sbl.serial_lang = '$serial_lang'
                AND sbd.status_sbd = 'ACTIVE'
                AND sbc.status_sbc = 'ACTIVE'
                AND s.status_ser = 'ACTIVE'
                AND sbc.serial_cou = '$serial_cou'";
                //echo $sql; die();
                $result=$db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}

    }
    
    ///GETTERS
    function getSerial_sbd(){
        return $this->serial_sbd;
    }
    function getSerial_dea(){
        return $this->serial_dea;
    }
    function getSerial_sbc(){
        return $this->serial_sbc;
    }
    function getStatus_sbd(){
        return $this->status_sbd;
    }

    ///SETTERS
    function setSerial_sbd($serial_sbd){
        $this->serial_sbd = $serial_sbd;
    }
    function setSerial_dea($serial_dea){
        $this->serial_dea = $serial_dea;
    }
    function setSerial_sbc($serial_sbc){
        $this->serial_sbc = $serial_sbc;
    }
    function setStatus_sbd($status_sbd){
        $this->status_sbd = $status_sbd;
    }

}

?>
