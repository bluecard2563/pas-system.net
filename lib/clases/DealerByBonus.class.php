<?php
/*
File: DealerByBonus.class.php
Author: Santiago Benitez
Creation Date: 17/03/2010 11:08
Last Modified: 18/03/2010
Modified By: David Bergmann
*/
class DealerByBonus {
	var $db;
	var $serial_dbb;
	var $serial_bon;
	var $serial_dea;
        var $serial_pxc;

	function __construct($db, $serial_dbb = NULL, $serial_bon = NULL, $serial_dea = NULL, $serial_pxc = NULL){
		$this -> db = $db;
		$this -> serial_dbb = $serial_dbb;
		$this -> serial_bon = $serial_bon;
		$this -> serial_dea = $serial_dea;
                $this -> serial_pxc = $serial_pxc;
	}

	/***********************************************
        * function getData
        * gets data by serial_dbb
        ***********************************************/
	function getData(){
		if($this->serial_dbb!=NULL){
			$sql = 	"SELECT serial_dbb, serial_bon, serial_dea, serial_pxc
					FROM dealer_by_bonus
					WHERE serial_dbb='".$this->serial_dbb."'";
			//echo $sql." ";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_dbb=$result->fields[0];
				$this->serial_bon=$result->fields[1];
				$this->serial_dea=$result->fields[2];
                                $this->serial_pxc=$result->fields[3];

				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
		$sql="INSERT INTO dealer_by_bonus (
					serial_dbb,
					serial_bon,
					serial_dea,
                                        serial_pxc
					)
			  VALUES(NULL,
					'".$this->serial_bon."',
					'".$this->serial_dea."',
                                        '".$this->serial_pxc."'
					);";

		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
		$sql=  "UPDATE dealer_by_bonus
                        SET serial_bon='".$this->serial_bon."',
                            serial_dea='".$this->serial_dea."',
                            serial_pxc='".$this->serial_pxc."'
                        WHERE serial_dbb='".$this->serial_dbb."'";

		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true)
			return true;
		else
			return false;
	}

	/***********************************************
        * funcion getDealersByBonus
        * gets all the DealersByBonus of the system
        ***********************************************/
	function getDealersByBonus(){
		$sql = 	"SELECT serial_dbb, serial_bon, serial_dea, serial_pxc
			 FROM dealer_by_bonus";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /***********************************************
        * funcion dealerByBonusExists
        * checks in db if there already exists a dealerByBonus
        * with the same data
        ***********************************************/
	function dealerByBonusExists() {
            $sql = "SELECT serial_dbb
                    FROM dealer_by_bonus
                    WHERE serial_dea = ".$this->serial_dea."
                    AND serial_pxc = ".$this->serial_pxc;
            $result = $this->db -> Execute($sql);

            if($result->fields[0]){
                    return $result->fields[0];
            }else{
                    return false;
            }
	}

        /***********************************************
        * funcion getDealersByBonus
        * checks in db if there already exists a dealerByBonus
        * with the same data
        ***********************************************/
	function getDealersByBonusTable($serial_cou,$serial_cit=NULL,$serial_dea=NULL) {
            $sql =  "SELECT dbb.serial_dbb, cou.name_cou, c.name_cit, d.name_dea, pbl.name_pbl, b.percentage_bon
                     FROM dealer_by_bonus dbb
                     JOIN bonus b ON  dbb.serial_bon = b.serial_bon
                     JOIN dealer d ON dbb.serial_dea = d.serial_dea
                     JOIN sector s ON s.serial_sec = d.serial_sec
                     JOIN city c ON c.serial_cit = s.serial_cit
                     JOIN country cou ON cou.serial_cou = c.serial_cou
                     JOIN product_by_country pbc ON dbb.serial_pxc = pbc.serial_pxc
                     JOIN product_by_language pbl ON pbc.serial_pro = pbl.serial_pro
                     JOIN language lang ON lang.serial_lang = pbl.serial_lang  AND lang.code_lang = '".$_SESSION['language']."'
                     WHERE cou.serial_cou = ".$serial_cou;
            if($serial_cit!=NULL){
                $sql .=" AND c.serial_cit = ".$serial_cit;
            }
            if($serial_dea!=NULL){
                $sql .=" AND dbb.serial_dea = ".$serial_dea;
            }
            $sql .=" ORDER BY d.name_dea, c.name_cit";
            //die($sql);
            $result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}


	///GETTERS
	function getSerial_dbb(){
		return $this->serial_dbb;
	}
	


	///SETTERS
	function setSerial_dbb($serial_dbb){
		$this->serial_dbb = $serial_dbb;
	}
	function setSerial_bon($serial_bon){
		$this->serial_bon = $serial_bon;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea = $serial_dea;
	}
        function setSerial_pxc($serial_pxc){
		$this->serial_pxc = $serial_pxc;
	}
}
?>
