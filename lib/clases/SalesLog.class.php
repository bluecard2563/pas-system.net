<?php
/*
File: SalesLog.class.php
Author: Patricio Astudillo
Creation Date: 08/04/2009 11:22
Last Modified: 08/04/2009
Modified By: Patricio Astudillo
*/

class SalesLog{
	var $db;
	var $serial_slg;
	var $serial_usr;
	var $usr_serial_usr;
	var $serial_sal;
	var $date_slg;
	var $description_slg;
	var $type_slg;
	var $status_slg;
        var $aux;

	function __construct($db, $serial_slg=NULL, $serial_usr=NULL,  $usr_serial_usr=NULL, $serial_sal=NULL, $description_slg=NULL, $type_slg=NULL, $status_slg=NULL ){
            $this -> db = $db;
            $this -> serial_slg = $serial_slg;
            $this -> serial_usr = $serial_usr;
            $this -> usr_serial_usr = $usr_serial_usr;
            $this -> serial_sal = $serial_sal;
            $this -> date_slg = $serial_slg;
            $this -> description_slg = $description_slg;
            $this -> type_slg = $type_slg;
            $this -> status_slg = $status_slg;
	}

	function getData(){
		if($this->serial_slg!=NULL){
			$sql="SELECT sl.serial_slg, sl.serial_usr, sl.usr_serial_usr, sl.serial_sal, sl.date_slg, sl.description_slg, sl.type_slg, sl.status_slg
				  FROM sales_log sl
				  WHERE sl.serial_slg='".$this->serial_slg."'";
			$result=$this->db->Execute($sql);

			if($result){
				$this->serial_slg=$result->fields['0'];
				$this->serial_usr=$result->fields['1'];
				$this->usr_serial_usr=$result->fields['2'];
				$this->serial_sal=$result->fields['3'];
				$this->date_slg=$result->fields['4'];
				$this->description_slg=$result->fields['5'];
				$this->type_slg=$result->fields['6'];
				$this->status_slg=$result->fields['7'];
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/***********************************************
	@Name: insert
	@Description: Inserts a register in DB
	@Params: A SaleLog object.
	@Returns: TRUE if OK / False on error.
	***********************************************/
    function insert() {
        $sql = "
            INSERT INTO sales_log (
				serial_slg,
				serial_usr,
				usr_serial_usr,
				serial_sal,
				date_slg,
				description_slg,
				type_slg,
				status_slg)
            VALUES (
				NULL,
                '$this->serial_usr',";
		if($this->usr_serial_usr != NULL) {
			$sql .= "'$this->usr_serial_usr',";
		} else {
			$sql .= "NULL,";
		}
				
		$sql .= "'$this->serial_sal',
				CURRENT_TIMESTAMP(),
				'$this->description_slg',
				'$this->type_slg',
				'$this->status_slg')";
        $result = $this->db->Execute($sql);
    	
        if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/***********************************************
	@Name: update
	@Description: Updates a register in DB
	@Params: A SaleLog object.
	@Returns: TRUE if OK / False on error.
	***********************************************/
    function update() {
        $sql = "
            UPDATE sales_log set
				serial_usr='$this->serial_usr',
				serial_sal='$this->serial_sal',
				description_slg='$this->description_slg',
				type_slg='$this->type_slg',
				status_slg='$this->status_slg'";
		if($this->usr_serial_usr != NULL) {
			$sql .= ", usr_serial_usr='$this->usr_serial_usr'";
		}
		$sql .= " WHERE serial_slg='$this->serial_slg'";
        $result = $this->db->Execute($sql);

    	if($result){
            return true;
        }else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
        	return false;
        }
    }


    /***********************************************
	@Name: voidSerial_inv
	@Description: Inserts a register in DB
	@Params: A SaleLog object.
	@Returns: TRUE if OK / False on error.
	***********************************************/
    function voidSerial_inv() {
        $sql = "
            UPDATE sales_log SET serial_inv
            VALUES (
                '".$this->serial_usr."',
				'".$this->serial_sal."',
				CURRENT_TIMESTAMP(),
				'".$this->description_slg."',
				'".$this->type_slg."')";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if($result==true)
            return true;
        else
            return false;
    }

	/***********************************************
	@Name: verifyAddDatesChanges
	@Description: Inserts a register in DB
	@Params: A SaleLog object.
	@Returns: TRUE if OK / False on error.
	***********************************************/
	public static function verifyAddDatesChanges($db,$serial_sal){
		$sql="SELECT serial_sal
			  FROM sales_log
			  WHERE serial_sal = ".$serial_sal."
			  AND type_slg = 'ADD_DAYS'";

		 $result = $db->Execute($sql);
		if ($result === false) return false;

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/***********************************************
	@Name: hasNotExecutive_CorporativeBehaviour
	@Description: Verifies if the card doesn't admit multiple travels
	 *			  or is like a masive.
	@Params: Sale ID
	@Returns: TRUE if has that behaviour / False otherwise.
	***********************************************/
	public static function hasNotExecutive_CorporativeBehaviour($db,$serial_sal){
		$sql="SELECT s.serial_sal
			  FROM sales s
			  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			  JOIN product p ON p.serial_pro=pxc.serial_pro AND (p.flights_pro <> 1 OR p.masive_pro = 'YES')
			  WHERE s.serial_sal=".$serial_sal;

		 $result = $db->getOne($sql);

		if($result){
			return true;
		}else{
			return false;
		}
	}

	/***********************************************
	@Name: verifyStandByChanges
	@Description: Inserts a register in DB
	@Params: A SaleLog object.
	@Returns: TRUE if OK / False on error.
	***********************************************/
	public static function verifyStandByChanges($db,$serial_sal){
		$data=array();
		$sql="SELECT s.status_sal, MIN(sl.date_slg)
			  FROM sales s
			  LEFT JOIN sales_log sl ON sl.serial_sal=s.serial_sal AND sl.type_slg = 'STAND_BY'
			  WHERE s.serial_sal = ".$serial_sal;

		 $result = $db->Execute($sql);

		if($result->fields[0]){
			$data['status_sal']=$result->fields['0'];
			$data['first_request']=$result->fields['1'];

			return $data;
		}else{
			return false;
		}
	}

	/***********************************************
	@Name: verifyBlockedChanges
	@Description: Inserts a register in DB
	@Params: A SaleLog object.
	@Returns: TRUE if OK / False on error.
	***********************************************/
	public static function verifyBlockedChanges($db,$serial_sal){
		$data=array();
		$sql="SELECT s.status_sal, MIN(sl.date_slg)
			  FROM sales s
			  LEFT JOIN sales_log sl ON sl.serial_sal=s.serial_sal AND sl.type_slg = 'BLOCKED'
			  WHERE s.serial_sal = ".$serial_sal;

		 $result = $db->Execute($sql);

		if($result->fields[0]){
			$data['status_sal']=$result->fields['0'];
			$data['first_request']=$result->fields['1'];

			return $data;
		}else{
			return false;
		}
	}

	/***********************************************
	@Name: getPreviousStatus
	@Description: Retireves the last status of a specific
	 *            sale.
	@Params: $serial_sal.
	@Returns: Last Status if OK / False on error.
	***********************************************/
	public static function getPreviousStatus($db,$serial_sal,$type){
		$data=array();
		$sql="SELECT s.description_slg, MAX(s.date_slg)
			  FROM sales_log s
			  WHERE s.serial_sal = ".$serial_sal."
			  AND s.type_slg= '$type'";

		 $result = $db->Execute($sql);

		if($result->fields[0]){
			$data=unserialize($result->fields['0']);
			return $data['old_status_sal'];
		}else{
			return false;
		}
	}

	/***********************************************
	@Name: hasPendingStandByApplication
	@Description: Verifies if there's any stand-by application pending.
	@Params: $serial_sal.
	@Returns: TRUE if OK / False on error.
	***********************************************/
	public static function hasPendingStandByApplication($db,$serial_sal){
		$sql="SELECT s.serial_slg
			  FROM sales_log s
			  WHERE s.serial_sal = ".$serial_sal."
			  AND s.type_slg = 'STAND_BY'
			  AND s.status_slg = 'PENDING'";

		 $result = $db->Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/***********************************************
	@Name: hasPendingBlockedApplication
	@Description: Verifies if there's any blocked application pending.
	@Params: $serial_sal.
	@Returns: TRUE if OK / False on error.
	***********************************************/
	public static function hasPendingBlockedApplication($db,$serial_sal){
		$sql="SELECT s.serial_slg
			  FROM sales_log s
			  WHERE s.serial_sal = ".$serial_sal."
			  AND s.type_slg = 'BLOCKED'
			  AND s.status_slg = 'PENDING'";

		 $result = $db->Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@Name: getSalesLogStatus
	@Description: Gets all Refund status in DB.
	@Params: N/A
	@Returns: Repeat Sale array
	**/
	public static function getSalesLogStatus($db){
		$sql = "SHOW COLUMNS FROM sales_log LIKE 'status_slg'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	@Name: getSalesLogPendingSaleModify
	@Description: Gets all Refund status in DB.
	@Params: N/A
	@Returns: Repeat Sale array
	**/
	public static function getSalesLogPendingSaleModify($db,$special_modifications=false){
		if($special_modifications){
			$sql_conditions=" sl.type_slg = 'SPECIAL_MODIFICATION'";
		}else{
			$sql_conditions=" sl.type_slg = 'MODIFICATION'";
		}

		$sql = "SELECT sl.serial_slg,sl.serial_usr,sl.serial_sal,sl.date_slg,sl.description_slg,sl.type_slg,sl.status_slg,CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'applicant',
				cou.name_cou,s.card_number_sal, d.name_dea
				FROM sales_log sl
				JOIN user u ON u.serial_usr=sl.serial_usr
				JOIN sales s ON s.serial_sal=sl.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN dealer d ON d.serial_dea=pbd.serial_dea
				JOIN sector sec ON sec.serial_sec=d.serial_sec
				JOIN city cit ON cit.serial_cit=sec.serial_cit
				JOIN country cou ON cou.serial_cou=cit.serial_cou
				WHERE $sql_conditions
				AND sl.status_slg = 'PENDING'";

		$result = $db->Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
				$arr=array();
				$cont=0;
				do{
						$arr[$cont]=$result->GetRowAssoc(false);
						$result->MoveNext();
						$cont++;
				}while($cont<$num);
			return $arr;
		}
		else{
			return false;
		}
	}
	
	/**
	@Name: getSalesLogBySale
	@Description: Gets all changes made in a sale
	@Params: $serial_sal and $type
	@Returns: array with sales log info
	**/
	function getSalesLogBySale($serial_sal, $type = null){
			$sql="SELECT sl.description_slg, sl.status_slg, us.first_name_usr, us.last_name_usr, sl.type_slg,
						 DATE_FORMAT(sl.date_slg, '%d/%m/%Y %H:%i:%s') AS 'date_slg',
						 CONCAT(us2.first_name_usr,' ',us2.last_name_usr) AS 'authorized_by'
				  FROM sales_log sl
				  JOIN user us ON sl.serial_usr = us.serial_usr
				  LEFT JOIN user us2 ON us2.serial_usr=sl.usr_serial_usr
				  WHERE sl.serial_sal='".$serial_sal."'";
			if($type){
				$sql .= " AND sl.type_slg='".$type."'";
			}
			$this->aux = NULL;
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			
			if($num > 0){
				$arr = array();
				$cont = 0;
				do{
					$arr[$cont++] = $result -> GetRowAssoc(false);
					$result -> MoveNext();
				}while($cont < $num);
			   $this -> aux = $arr;
			   return $arr;
			}
			return false;
	}

	/**
	@Name: getPendingSalesLogBySale
	@Description: Gets all changes made in a sale
	@Params: $serial_sal and $type
	@Returns: array with sales log info
	**/
	function getPendingSalesLogBySale($serial_sal, $type = NULL){
        $typeSql = '';
        if($type != null){
        	$typeSql = "AND sl.type_slg='" . $type . "'";
        }
		$sql="SELECT sl.date_slg, sl.description_slg, sl.status_slg, us.first_name_usr, us.last_name_usr, sl.serial_slg
              FROM sales_log sl
              JOIN user us ON sl.serial_usr = us.serial_usr
              WHERE sl.serial_sal='".$serial_sal."' $typeSql AND sl.status_slg= 'PENDING'";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr = array();
			$cont = 0;
			do{
				$arr[$cont++] = $result -> GetRowAssoc(false);
				$result -> MoveNext();
			}while($cont < $num);
	        $this -> aux = $arr;
	        return true;
		}
		return false;
	}

        /**
        @Name: getAuthorizadSalesLogBySale
		@Description: Gets all authorizad changes made in a sale
		@Params: $serial_sal
		@Returns: array with sales log info
		**/
        function getAuthorizadSalesLogBySale($serial_sal){
                $sql="SELECT *
                      FROM sales_log
                      WHERE serial_sal='".$serial_sal."'";
                //die($sql);
                $result = $this->db -> Execute($sql);
                $num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	public static function getSalesLogReport($db,$serial_mbc,$begin_date,$end_date,$type_slg,$status_slg){
		$sql = "SELECT IFNULL(sal.card_number_sal, trl.card_number_trl) as card_number,
					   DATE_FORMAT(slg.date_slg, '%d/%m/%Y') AS date_slg,
					   CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr, slg.description_slg,
					   IF(sal.begin_date_sal <= slg.date_slg,'YES','NO') as start_coverage,
					   slg.status_slg, CONCAT(usr2.first_name_usr,' ',usr2.last_name_usr) as usr_name_usr
				FROM sales_log slg
				JOIN sales sal ON sal.serial_sal = slg.serial_sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.serial_mbc = $serial_mbc
				JOIN user usr ON usr.serial_usr = slg.serial_usr
				JOIN user usr2 ON usr2.serial_usr = slg.usr_serial_usr
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
				WHERE slg.date_slg BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y')  AND STR_TO_DATE('$end_date','%d/%m/%Y')";
		if($status_slg) {
			$sql.=" AND slg.status_slg = '$status_slg'";
		}
		$sql.=" AND slg.type_slg = '$type_slg'
				ORDER BY slg.date_slg";

		$result = $db->getAll($sql);

		if($result) {
			return $result;
		} else {
			return false;
		}
	}

	/**
	 * @name registerLog
	 * @param <adodb> $db
	 * @param <string> $type
	 * @param <array> $data
	 * @param <sales_id> $serial_sal
	 */
	public static function registerLog($db, $type, $data, $serial_sal, $serial_usr){
		$sql = "INSERT INTO sales_log
					VALUES (NULL,
							$serial_usr,
							NULL,
							$serial_sal,
							CURRENT_TIMESTAMP(),
							'".serialize($data)."',
							'$type',
							'PENDING')";

		$result = $db -> Execute($sql);

		if($result){
            return $db -> insert_ID();
        }else{
			ErrorLog::log($db, 'REGISTER LOG FAILED - SALESLOG', $sql);
        	return false;
        }
	}

	/**
	 *
	 * @param <adodb> $db
	 * @param <serial_usr> $authorizing_usr
	 * @param <string> $status_slg
	 * @param <array> $observations
	 * @param <serial_slg> $serial_log
	 * @return <booloan>
	 */
	public static function serveLog($db, $authorizing_usr, $status_slg, $observations, $serial_log){
		$sql = "UPDATE sales_log SET
					usr_serial_usr = $authorizing_usr,
					description_slg = '".serialize($observations)."',
					status_slg = '$status_slg'
				WHERE serial_slg = $serial_log";
		
		$result = $db -> Execute($sql);

		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'SERVE LOG FAILED - SALESLOG', $sql);
			return FALSE;
		}
	}
	
	public static function getLastAuthorizedLogForSerial($db, $serial_sal, $type_slg){
		$sql = "SELECT serial_slg, serial_usr, usr_serial_usr, serial_sal, date_slg, description_slg, 
						type_slg, status_slg
				FROM sales_log 
				WHERE serial_sal = $serial_sal
				AND type_slg = '$type_slg'
				AND status_slg = 'AUTHORIZED'
				GROUP BY serial_sal
				HAVING serial_slg = MAX(serial_slg)";
		
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_slg(){
		return $this->serial_slg;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getUsr_serial_usr(){
		return $this->usr_serial_usr;
	}
	function getSerial_sal(){
		return $this->serial_sal;
	}
	function getDate_slg(){
		return $this->date_slg;
	}
	function getDescription_slg(){
		return $this->description_slg;
	}
	function getType_slg(){
		return $this->type_slg;
	}
	function getStatus_slg(){
		return $this->status_slg;
	}
    function getAux_slg(){
		return $this->aux;
	}

	

	///SETTERS	
	function setSerial_slg($serial_slg){
		$this->serial_slg = $serial_slg;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setUsr_serial_usr($usr_serial_usr){
		$this->usr_serial_usr = $usr_serial_usr;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal = $serial_sal;
	}
	function setDate_slg($date_slg){
		$this->date_slg = $date_slg;
	}
	function setDescription_slg($description_slg){
		$this->description_slg = $description_slg;
	}
	function setType_slg($type_slg){
		$this->type_slg = $type_slg;
	}
	function setStatus_slg($status_slg){
		$this->status_slg=$status_slg;
	}
}
?>