<?php
/*
File: DocumentByManager.class.php
Author: Santiago Borja
Creation Date: 02/10/2010
*/
class DocumentByManager {
    var $db;
    var $serial_dbm;
    var $serial_doc;
    var $serial_man;
    var $type_dbm;
	var $purpose_dbm;
	var $number_dbm;

    function __construct($db, $serial_dbm = NULL, $serial_doc = NULL, $serial_man = NULL, $type_dbm = NULL, $purpose_dbm = NULL, $number_dbm = NULL){
            $this -> db = $db;
            $this -> serial_dbm = $serial_dbm;
		    $this -> serial_doc = $serial_doc;
		    $this -> serial_man = $serial_man;
		    $this -> type_dbm = $type_dbm;
			$this -> purpose_dbm = $purpose_dbm;
			$this -> number_dbm = $number_dbm;
    }

    /**
    @Name: getData
    @Description: Retrieves the data of a DocumentByManager object.
    @Params: DocumentByManager ID
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getData(){
        if($this->serial_dbm!=NULL){
            $sql = 	"SELECT serial_dbm, serial_doc, serial_man, type_dbm, purpose_dbm, number_dbm
                            FROM document_by_manager
                            WHERE serial_dbm='".$this->serial_dbm."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if($result->fields[0]){
                $this->serial_dbm=$result->fields[0];
                $this->serial_doc=$result->fields[1];
                $this->serial_man=$result->fields[2];
                $this->type_dbm=$result->fields[3];
				$this->purpose_dbm=$result->fields[4];
				$this->purpose_dbm=$result->fields[5];
                return true;
            }
        }
        return false;
    }
    
    /**
    @Name: retrieveNextDocumentNumber
    @Description: Retrieves the number_dbm of a DocumentByManager object.
    @Params: serial_cou, type_dbm
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    public static function retrieveNextDocumentNumber($db, $serial_man, $type_dbm, $purpose, $moveToNextValue){
            $serial_dbm =  self::retrieveSelfDBMSerial($db,$serial_man, $type_dbm, $purpose);

    		$sql = 	"SELECT number_dbm 
				FROM document_by_manager
				WHERE serial_dbm = $serial_dbm";
            //die($sql);
            $result = $db->getOne($sql);

            if($result){
            	if($moveToNextValue){
            		self::moveToNextValue($db,$serial_dbm);
            	}
            	return $result;
            }
            return false;
    }
    
	/**
    @Name: retrieveSelfDBMSerial
    @Description: Retrieves the serial of a DocumentByManager object.
    @Params: 	$serial_man, 
    			$type_dbm: 'LEGAL_ENTITY', 'PERSON', 'BOTH',
    			$purpose: 'INVOICE', 'CREDIT_NOTE', 'PAYMENT_NOTE'
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    public static function retrieveSelfDBMSerial($db,$serial_man, $type_dbm, $purpose){
            $sql = 	"SELECT serial_dbm 
					FROM document_by_manager
					WHERE serial_man = $serial_man AND (type_dbm='".$type_dbm."' OR type_dbm='BOTH') AND purpose_dbm='".$purpose."'";
            $result = $db->getOne($sql);
			
            if($result)
                return $result;
            return false;
    }

	/**
    * @Name: isNumberUsed
    * @Description: Verifies if a number is used or no for a specific purpose
    * @Params: 	$db - DB Connection
    *			$serial_dbm: DocumentByManager ID
    *			$number: Number of the document.
	*			$purpose_dbm: INVOICE & CREDIT_NOTE are the possible values.
    * @Returns: TRUE if O.K. / FALSE on errors
    **/
    public static function isNumberUsed($db, $serial_dbm, $number_dbm, $purpose_dbm){
			if($purpose_dbm=='INVOICE'){
				$table='invoice';
				$field='serial_inv';
				$db_number='number_inv';
			}else{ //FOR CREDIT NOTE
				$table='credit_note';
				$field='serial_cn';
				$db_number='number_cn';
			}
			
            $sql = "SELECT $field
					FROM $table
					WHERE serial_dbm = $serial_dbm
					AND $db_number=$number_dbm";
            //die($sql);
            $result = $db->getOne($sql);

            if($result)
                return TRUE;
            return FALSE;
    }
    
 	/**
    @Name: moveToNextValue
    @Description: Retrieves the serial of a DocumentByManager object.
    @Params: serial_cou, type_dbm
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    public static function moveToNextValue($db, $serial_dbm, $fixed_next_value = NULL){
            $sql = 	"UPDATE document_by_manager SET number_dbm = number_dbm + 1
					 WHERE serial_dbm = $serial_dbm";
			
			if($fixed_next_value){
				$sql = 	"UPDATE document_by_manager SET number_dbm = $fixed_next_value + 1
						WHERE serial_dbm = $serial_dbm";
			}
            
            $result = $db->Execute($sql);
            if($result){
				return TRUE;
            }else{
				ErrorLog::log($db, 'UPDATE FAILED - '.get_class($this), $sql);
            }
            return FALSE;
    }

    /**
    @Name: insert
    @Description: Inserts a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function insert(){
        $sql="INSERT INTO document_by_manager (
                                    serial_dbm,
                                    serial_doc,
                                    serial_man,
                                    type_dbm,
									purpose_dbm,
									number_dbm
                                )
                  VALUES(NULL,
                        '".$this->serial_doc."',
                        '".$this->serial_man."',
                        '".$this->type_dbm."',
                        '".$this->purpose_dbm."',
						'".$this->number_dbm."'
                        );";
        $result = $this->db->Execute($sql);
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: update
    @Description: updates a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function update(){
        $sql="UPDATE document_by_manager
        SET serial_doc='".$this->serial_doc."',
        	serial_man='".$this->serial_man."',
            type_dbm='".$this->type_dbm."',
            purpose_dbm='".$this->purpose_dbm."',
			number_dbm='".$this->number_dbm."'
        WHERE serial_dbm='".$this->serial_dbm."'";

        $result = $this->db->Execute($sql);
        return $result==true;
    }

    /**
    @Name: delete
    @Description: delete a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function delete(){
        if($this->serial_doc!=NULL){
            $sql="DELETE FROM document_by_manager
                  WHERE serial_doc='".$this->serial_doc."'
                  AND serial_dbm NOT IN (SELECT DISTINCT i.serial_dbm FROM invoice i)
				  AND serial_dbm NOT IN (SELECT DISTINCT cn.serial_dbm FROM credit_note cn)
				  AND number_dbm = 1";

            $result = $this->db->Execute($sql);
            return $result==true;
        }
    }

    /**
    @Name: DocumentByManagerExists
    @Description: verifies if a Document by country exists or not
    @Params: $serial_cou(Country ID), $serial_doc(DocumentType serial), $serial_dbm(Document by country serial).
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function DocumentByManagerExists($serial_doc, $serial_dbm=NULL){
            $sql = 	"SELECT serial_dbm
					 FROM document_by_manager
					 WHERE serial_doc = ".$serial_doc;
            if($serial_dbm!=NULL){
                    $sql.=" AND serial_dbm <> ".$serial_dbm;
            }
            $result = $this->db->Execute($sql);

            if($result->fields[0]){
                    return true;
            }
            return false;
    }

    /**
	@Name: getDocumentsByManager
	@Description: Returns an array of documents.
	@Params: country ID
	@Returns: DocumentType array
	**/
	function getDocumentsByManager($serial_man,$purpose){
		$sql = "SELECT *
				FROM document_by_manager
				WHERE serial_man='$serial_man'
				AND purpose_dbm='$purpose'";
		$result = $this ->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getDocumentByManagerByType
	@Description: Returns an array of documents.
	@Params: country ID
	@Returns: DocumentType array
	**/
	function getDocumentByManagerByType($serial_man,$purpose,$type){
		$sql = "SELECT serial_dbm
				FROM document_by_manager
				WHERE serial_man='$serial_man'
				AND purpose_dbm='$purpose'
				AND type_dbm='$type'";

		$result = $this->db->getOne($sql);

		if($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

    /**
    @Name: checkIsUsed_dbm
    @Description: verifies if a Document by country is being used
    @Params: $serial_dbm(Document by country ID)
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function checkIsUsed_dbm($serial_dbm){
        $sql = "SELECT serial_dbm
                 FROM invoice
                 WHERE serial_dbm = ".$serial_dbm;

        $result = $this->db->Execute($sql);
        if($result->fields[0]){
            return 1;
        }
        return 0;
    }

	/*
	 * @Name:getPurposeValues_dbm
	 * @Description: retrieves a list of possible values for the purpose_dbm field.
	 * @Params: $db
	 * @Returns: a list with the possible values for the purpose_dbm field.
	 */
	public static function getPurposeValues_dbm($db){
		$sql = "SHOW COLUMNS FROM document_by_manager LIKE 'purpose_dbm'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
    *@Name: similarDocTypeExists
    *@Description: verifies if there is a similar document type for an specific
	*			  manager so that the insert will not be completed.
    *@Params: $type: Type of the document: LEGAL_ENTITY/PERSON/BOTH
    *@Returns: TRUE if O.K. / FALSE on errors
    **/
    function similarDocTypeExists($serial_dbm = NULL){
        $sql = "SELECT serial_dbm
                FROM document_by_manager
                WHERE serial_man = ".$this->serial_man."
				AND purpose_dbm='".$this->purpose_dbm."'";

		if( $this->type_dbm == 'LEGAL_ENTITY' || $this->type_dbm == 'PERSON'){
			$sql .= " AND (type_dbm = '".$this->type_dbm."'
					OR type_dbm = 'BOTH')";
		}elseif( $this->type_dbm == 'BOTH' ){
			$sql .= " AND (type_dbm = 'LEGAL_ENTITY'
					OR type_dbm = 'PERSON'
					OR type_dbm = 'BOTH')";
		}

		if($serial_dbm){
			$sql.=" AND serial_dbm <> ".$serial_dbm;
		}

		//echo $sql;

		$result = $this->db->Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }

    /**
    @Name: getAssignedManagers
    @Description: Retrieves the data of a DocumentByManager object.
    @Params: DocumentType ID
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getAssignedManagers(){
        if($this->serial_doc!=NULL){
            $sql = 	"SELECT dbm.serial_dbm, dbm.serial_man, dbm.serial_doc, dbm.type_dbm, dbm.purpose_dbm, c.serial_cou, c.serial_zon
					FROM document_by_manager dbm
					JOIN manager_by_country mbc
						ON mbc.serial_man = dbm.serial_man
					JOIN country c
						ON mbc.serial_cou = c.serial_cou
					WHERE dbm.serial_doc='".$this->serial_doc."'";

            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;

        }else{
            return false;
        }
    }

	public static function sameDocumentExists($db, $serial_doc, $serial_man, $purpose, $type){
		$sql = "SELECT serial_dbm
				FROM document_by_manager
				WHERE serial_doc = $serial_doc
				AND serial_man = $serial_man
				AND purpose_dbm = '$purpose'
				AND type_dbm = '$type'";

		$result = $db -> getOne($sql);

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

    ///GETTERS
    function getSerial_dbm(){
        return $this->serial_dbm;
    }
    function getSerial_doc(){
        return $this->serial_doc;
    }
 	function getSerial_man(){
        return $this->serial_man;
    }
    function getType_dbm(){
        return $this->type_dbm;
    }
	function getPurpose_dbm(){
        return $this->purpose_dbm;
    }
	function getNumber_dbm(){
        return $this->number_dbm;
    }
    
    ///SETTERS
    function setSerial_dbm($serial_dbm){
        $this->serial_dbm = $serial_dbm;
    }
    function setSerial_doc($serial_doc){
        $this->serial_doc = $serial_doc;
    }
	function setSerial_man($serial_man){
        $this->serial_man = $serial_man;
    }
    function setType_dbm($type_dbm){
        $this->type_dbm = $type_dbm;
    }
	function setPurpose_dbm($purpose_dbm){
        $this->purpose_dbm=$purpose_dbm;
    }
	function setNumber_dbm($number_dbm){
        $this->number_dbm=$number_dbm;
    }
}
?>