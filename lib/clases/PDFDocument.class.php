<?php
/*
File: PDFDocument.class.php
Author: Santiago Borja 
Creation Date: 23/03/2010
* DEPENDS ON FPDF.class.php
*/

class PDFDocument extends FPDF{
	global $global_system_name;
	/** DYNAMIC VARIABLES */
	var $header;
	var $columnWidth;
	var $title;
	
	/** STATIC VARIABLES */
	var $textSize = 12;
	var $textFont = 'Arial';
	var $mainHeader = 'SISTEMA DE '.$global_system_name
	
	//table
	var $tableTopY = 50;
	var $headerHeight = 10;
	var $cellHeight = 8;
	var $tableContentX = 10;
	var $tableContentY = 20;
	
	
	function __construct($title,$header, $columnWidth){
		parent::__construct('p','mm','A4');
		
		foreach($header as &$temp){
			$temp=utf8_decode($temp);
		}
		
		$this -> header=$header;
		$this -> columnWidth=$columnWidth;
		$this -> title = $title;
	}
	
	function printDataToTable($data){
		$this->SetFont($this -> textFont,'B',$this -> textSize);
		
		$this -> SetXY(25,19);
		$this -> MultiCell(	70,5,$this -> mainHeader,0,'L',0);
		
		$this -> Line(10,28,200,28);
		
		$this->SetFont($this -> textFont,'',$this -> textSize);
		$this -> SetXY(15,35);
		$this -> MultiCell(	80,5,$this -> title,0,'L',0);
		
		$this->SetFont($this -> textFont,'',$this -> textSize-4);
		$this -> SetXY(15,40);
		$this -> MultiCell(	80,5,"Fecha: ".strftime("%d/%m/%Y %H:%I",time()),0,'L',0);
		
		$this -> Image(DOCUMENT_ROOT.'img/logopas.jpg',130,10,0,0);
		
		$x = $this -> tableContentX;
		$y = $this -> tableTopY;
		
		$this->SetFont($this -> textFont,'B',$this -> textSize);
	    foreach($this -> header as $i => $col){
	    	$this -> SetXY($x,$y);
	        $this->Cell($this -> columnWidth[$i],$this -> headerHeight,$col,1,0,'C');
	        $x += $this -> columnWidth[$i];
	    }
	    $this->Ln();
	    
	    $x = $this -> tableContentX;
	    $y += $this -> headerHeight;
	    
	    $this->SetFont($this -> textFont,'',$this -> textSize);
		foreach($data as $row){
			$maxHeight = 1;
			$i = 0;
			foreach($row as $col){
	        	$currHeight = $this -> getCellContentAndHeight(split(" ",trim($col)),$this -> columnWidth[$i]);
	        	if($currHeight['alto'] > $maxHeight){
	        		$maxHeight = $currHeight['alto'];
	        	}
	        	$i++;
	        }
	        //echo '<br/>mh='.$maxHeight;
			$i =0;
	        foreach($row as $col){
	        	$this -> SetXY($x,$y);
	        	$myResult = $this -> getCellContentAndHeight(split(" ",trim($col)),$this -> columnWidth[$i]);
	        	$myHeight = ($this -> cellHeight * ($maxHeight/$myResult['alto']));
	        	//$myHeight = 45;
	        	
	        	//echo '<br/>para '.$myResult['alto'].' va '.$myHeight.' con max  '.$maxHeight;
	        	$this -> MultiCell(	$this -> columnWidth[$i],
	        						$myHeight,
	        						$myResult['cadena'],1,'C',0);
	        	$x += $this -> columnWidth[$i];
	        	$i++;
	        }
	        $y+= $this -> cellHeight * $maxHeight;
	        $x = $this -> tableContentX;
	        $this->Ln();
	    }
	}
	
	function getCellContentAndHeight($palabras,$ancho ){
		$cadena=""; //uso de comillas dobes debido a que '\n' solo funciona, para fpdf, si se halla entre estas.
		$subCadena="";
		$lineas=1;	
		$tamanio=sizeof($palabras);
		$cont=0;
		foreach($palabras as $palabra){
			$cont++;
			if($this ->GetStringWidth($subCadena.$palabra."  ")<$ancho){
				$subCadena.=$palabra;
				if($cont<$tamanio)
					$subCadena.=" ";
			}
			else{
				$cadena.=$subCadena."\n";
				$subCadena=$palabra;
				if($cont<$tamanio)
					$subCadena.=" ";
				$lineas++;
			}
		}
		$cadena.=$subCadena;
		$cadenaAlto=array('cadena'=>$cadena,'alto'=>$lineas);
		//echo '<br/>cadena: '.$cadenaAlto['cadena']. ' - va con '.$cadenaAlto['alto'];
		//echo '-----ademas: '.$this ->GetStringWidth($palabras[0]).' para '.$palabras[0].' debe ser menor que '.$ancho;
		return $cadenaAlto;
	}
}
?>