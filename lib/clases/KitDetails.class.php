<?php
/*
File: KitDetails.class.php
Author: Santiago Ben�tez
Creation Date: 22/02/2010 12:02
Creation Date: 12/08/2010
Modified By: David Bergmann
*/
class KitDetails{
	var $db;
	var $serial_kdt;
	var $serial_kbd;
	var $date_kdt;
	var $action_kdt;
	var $amount_kdt;

	function __construct($db, $serial_kdt=NULL, $serial_kbd=NULL, $date_kdt=NULL, $action_kdt=NULL, $amount_kdt=NULL){

		$this -> db = $db;
		$this -> serial_kdt=$serial_kdt;
		$this -> serial_kbd=$serial_kbd;
		$this -> date_kdt=$date_kdt;
		$this -> action_kdt=$action_kdt;
		$this -> amount_kdt=$amount_kdt;
	}
        
        /***********************************************
    * funcion getKitDetails
    * gets information from kitDetails
    ***********************************************/
	function getKitDetails($fromDate,$toDate){
		$sql = 	"SELECT serial_kdt,serial_kbd,DATE_FORMAT(date_kdt,'%d/%m/%Y') as date,action_kdt,amount_kdt
                         FROM kit_details
                         WHERE serial_kbd='".$this->serial_kbd."'";
                if($fromDate){
                    $sql.=" AND date_kdt >= STR_TO_DATE('".$fromDate."','%d/%m/%Y')";
                }
                if($toDate){
                    $sql.=" AND date_kdt <= STR_TO_DATE('".$toDate."','%d/%m/%Y')";
                }
		$sql.=" ORDER BY date_kdt";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	/**
	@First_name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: serial_kdt if OK. / FALSE on error
	**/
    function insert() {
        $sql = "
            INSERT INTO kit_details (
				serial_kdt,
				serial_kbd,
				date_kdt,
				action_kdt,
				amount_kdt)
            VALUES (
                NULL,
				'".$this->serial_kbd."',
				CURDATE(),
				'".$this->action_kdt."',
				'".$this->amount_kdt."')";
        //die($sql);
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/**
	@First_name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "
                UPDATE kit_details SET
					serial_kbd=".$this->serial_kbd."',
					date_kdt=CURDATE(),
					action_kdt='".$this->action_kdt."',
					amount_kdt='".$this->amount_kdt."'
				WHERE serial_kdt = '".$this->serial_kdt."'";
                                //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }
	
	/** 
	@Name: getAllActions
	@Description: Gets all kit actions in DB.
	@Params: N/A
	@Returns: Action array
	**/
	function getAllActions(){
		$sql = "SHOW COLUMNS FROM kit_details LIKE 'action_kdt'";
		$result = $this->db -> Execute($sql);
		
		$action=$result->fields[1];
		$action = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$action));
		return $action;
	}

	//Getters
	function getSerial_kdt(){
		return $this->serial_kdt;
	}
	function getSerial_kbd(){
		return $this->serial_kbd;
	}
	function getDate_kdt(){
		return $this->date_kdt;
	}
	function getAction_kdt(){
		return $this->action_kdt;
	}
	function getAmount_kdt(){
		return $this->amount_kdt;
	}

	//Setters
	function setSerial_kdt($serial_kdt){
		$this->serial_kdt=$serial_kdt;
	}
	function setSerial_kbd($serial_kbd){
		$this->serial_kbd=$serial_kbd;
	}
	function setDate_kdt($date_kdt){
		$this->date_kdt=$date_kdt;
	}
	function setAction_kdt($action_kdt){
		$this->action_kdt=$action_kdt;
	}
	function setAmount_kdt($amount_kdt){
		$this->amount_kdt=$amount_kdt;
	}
}
?>