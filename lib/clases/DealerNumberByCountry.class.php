<?php
/*
File: DealerNumberByCountry.class
Author: David Bergmann
Creation Date: 23/03/2010
Last Modified:
Modified By:
*/

class DealerNumberByCountry {
	var $db;
	var $serial_cou;
	var $number_dnc;

	function __construct($db, $serial_cou = NULL, $number_dnc = NULL){
		$this -> db = $db;
		$this -> serial_cou = $serial_cou;
		$this -> number_dnc = $number_dnc;
	}

        /***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
            $sql="INSERT INTO dealer_number_by_country (
                                    serial_cou,
                                    number_dnc
                                    )
                      VALUES('".$this->serial_cou."',
                             '".$this->number_dnc."');";

            $result = $this->db->Execute($sql);
            
		if($result){
            return TRUE;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

        /***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
		$sql="UPDATE dealer_number_by_country
		SET number_dnc='".$this->number_dnc."'
		WHERE serial_cou='".$this->serial_cou."'";

		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true)
			return true;
		else
			return false;
	}

        /***********************************************
        * funcion getLastNumberByCountry
        * return the last number of a Country
        ***********************************************/
	function getLastNumberByCountry(){
		$sql = 	"SELECT number_dnc
                         FROM dealer_number_by_country
                         WHERE serial_cou = ".$this->serial_cou;

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

        ///GETTERS
	function getSerial_cou(){
            return $this->serial_cou;
	}
        function getNumber_dnc(){
            return $this->number_dnc;
	}

	///SETTERS
	function setSerial_cou($serial_cou){
            $this->serial_cou = $serial_cou;
	}
	function setNumber_dnc($number_dnc){
            $this->number_dnc = $number_dnc;
	}
}
?>