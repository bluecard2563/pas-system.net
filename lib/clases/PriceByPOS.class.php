<?php
/*
  File: PriceByPOS.class.php
  Author: Patricio Astudillo
  Creation Date: 28/09/2012
  Last Modified:
  Modified By:
 */

class PriceByPOS {

	var $db;
	var $serial_pps;
	var $serial_pbd;
	var $duration_pps;
	var $price_pps;
	var $cost_pps;
	var $min_pps;
	var $max_pps;

	function __construct($db, $serial_pps = NULL, $serial_pbd = NULL, $duration_pps = NULL, $price_pps = NULL, $cost_pps = NULL, $min_pps = NULL, $max_pps = NULL) {
		$this->db = $db;
		$this->serial_pps = $serial_pps;
		$this->serial_pbd = $serial_pbd;
		$this->duration_pps = $duration_pps;
		$this->price_pps = $price_pps;
		$this->cost_pps = $cost_pps;
		$this->min_pps = $min_pps;
		$this->max_pps = $max_pps;
	}

	/**
	  @Name: getData
	  @Description: Retrieves the data of a Dealer object.
	  @Params: The ID of the object
	  @Returns: TRUE if O.K. / FALSE on errors
	 * */
	function getData() {
		if ($this->serial_pps != NULL) {
			$sql = "SELECT serial_pps, serial_pbd, duration_pps, price_pps, cost_pps, min_pps, max_pps
					FROM priceByPointOfSale
					WHERE serial_pps='" . $this->serial_pps . "'";

			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_pps = $result->fields[0];
				$this->serial_pbd = $result->fields[1];
				$this->duration_pps = $result->fields[2];
				$this->price_pps = $result->fields[3];
				$this->cost_pps = $result->fields[4];
				$this->min_pps = $result->fields[5];
				$this->max_pps = $result->fields[6];

				return true;
			}else
				return false;
		}else
			return false;
	}

	/*	 * *********************************************
	 * function insert
	 * inserts a new register in DB 
	 * ********************************************* */

	function insert() {
		$sql = "
            INSERT INTO priceByPointOfSale (
                serial_pps,
                serial_pbd,
                duration_pps,
                price_pps,
                cost_pps,
                min_pps,
                max_pps)
            VALUES (
                    NULL,
                    '" . $this->serial_pbd . "',
                    '" . $this->duration_pps . "',";

		if ($this->price_pps) {
			$sql.="'" . $this->price_pps . "',";
		} else {
			$sql.="NULL,";
		}

		$sql.= "'" . $this->cost_pps . "',";

		if ($this->min_pps) {
			$sql.= "'" . $this->min_pps . "',";
		} else {
			$sql.="NULL,";
		}
		if ($this->max_pps) {
			$sql.= "'" . $this->max_pps . "'";
		} else {
			$sql.="NULL";
		}


		$sql.= ")";
		
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion detele
	 * deletes a register in DB
	 * ********************************************* */

	function deleteByProduct($serial_pro) {
		$sql = "DELETE
				FROM priceByPointOfSale
				WHERE serial_pbd
					IN (    SELECT serial_pbd
							FROM product_by_country
							WHERE serial_pro='" . $serial_pro . "'
						    AND serial_cou=1)";

		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;

		if ($result == true) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion delete
	 * deletes a register in DB
	 * ********************************************* */

	function delete() {
		if ($this->serial_pbd != NULL) {
			$sql = "DELETE
                    FROM priceByPointOfSale
                    WHERE serial_pbd = '" . $this->serial_pbd . "'";


			//echo $sql;die;
			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result == true) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/*
	 * @function: getPricesByProductByCountry
	 * @Description: Retrieves the fees of all the
	 * @params: $serial_pbd: Product By Country ID
	 *          $serial_dea: Dealer ID
	 *          $serial_cou: Dealer Country ID
	 * @returns: Array of fees acording to a country.
	 */

	public static function getPricesByProductByPOS($db, $serial_pbd, $days = NULL) {
		$sql = "SELECT serial_pps, serial_pbd, duration_pps AS 'duration_ppc', 
						price_pps AS 'price_ppc', cost_pps AS 'cost_ppc', min_pps AS 'min_ppc',
						max_pps AS 'max_ppc'
				FROM priceByPointOfSale pps
				WHERE pps.serial_pbd = $serial_pbd";
		if ($days) {
			$sql .= " AND pps.duration_pps BETWEEN " . ($days - 10) . " AND " . ($days + 10);
		}
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	
	
	

	/*	 * *********************************************
	 * funcion getAssignedCountries
	 * gets all the Countries assigened to a product
	 * ********************************************* */

	function getPricesByProduct($serial_pro) {
		$sql = "SELECT  pps.duration_pps, pps.price_pps, pps.cost_pps, pps.min_pps, pps.max_pps, pps.serial_pbd
				FROM priceByPointOfSale pps
				JOIN product_by_dealer pbd ON pbd.serial_pbd = pps.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pbd = pps.serial_pbd
					AND pxc.serial_pro = '" . $serial_pro . "'
					AND pxc.serial_cou=1
				ORDER BY pps.duration_pps";
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	

	/*
	 * @function: getPricesByProductByCountryHigherThanDays
	 * @Description: Retrieves the prices for all the days given for this product, but only for days over than those selected. 
	 */

	function getPricesByProductByCountryHigherThanDays($serial_pro, $serial_cou, $days) {
		$sql = "SELECT pps.*
              FROM priceByPointOfSale pps
              WHERE pps.serial_pbd = (SELECT pxc.serial_pbd
                                    FROM product_by_country pxc
                                    WHERE pxc.serial_cou = " . $serial_cou . "
                                    AND pxc.serial_pro = " . $serial_pro . ") AND pps.duration_pps >= " . $days;
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		//Debug::print_r($arreglo);
		return $arreglo;
	}

	/*
	 * @function: pricesByProductExist
	 * @Description: Verifies if prices of a certain product for a certain country exists.
	 * @params: $serial_pbd: Product By Country ID
	 * @returns: Array of fees acording to a country.
	 */

	public static function pricesByProductExist($db, $serial_pbd) {
		$sql = "SELECT serial_pps
              FROM priceByPointOfSale pps
              WHERE pps.serial_pbd = " . $serial_pbd;
		//echo $sql;
		$result = $db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * @function: getMaxPricePosible
	 * @Description: Retrieves the next available fee according to a specific amount of days.
	 * @params: $serial_pbd: Product By Country ID
	 *          $serial_dea: Dealer ID
	 *          $serial_cou: Dealer Country ID
	 * @returns: The Price ID if OK / FALSE otherwise.
	 */

	public static function getMaxPricePosiblePOS($db, $serial_pbd, $days) {
		$sql = "SELECT MIN(duration_pps)
              FROM priceByPointOfSale pps
              WHERE pps.serial_pbd = $serial_pbd
              AND duration_pps > " . $days;
		
		$db_min_day = $db->getOne($sql);

		$sql = "SELECT serial_pps
                FROM priceByPointOfSale pps
				WHERE pps.serial_pbd = $serial_pbd
				AND duration_pps = " . $db_min_day;
		
		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*
	 * @function: getMasivePrice
	 * @Description: Retrieves the price of a masive.
	 * @returns: sets the first price of a given masive product.
	 */

	function getMasivePrice($serial_pro, $serial_cou) {
		if ($this->serial_pbd != NULL) {
			$sql = "SELECT serial_pps, serial_pbd, duration_pps, price_pps, cost_pps, min_pps, max_pps
					FROM priceByPointOfSale
					WHERE serial_pbd=(	SELECT pxc.serial_pbd
										FROM product_by_country pxc
										WHERE pxc.serial_cou = " . $serial_cou . "
										AND pxc.serial_pro = " . $serial_pro . "	)
					LIMIT 1";

			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_pps = $result->fields[0];
				$this->serial_pbd = $result->fields[1];
				$this->duration_pps = $result->fields[2];
				$this->price_pps = $result->fields[3];
				$this->cost_pps = $result->fields[4];
				$this->min_pps = $result->fields[5];
				$this->max_pps = $result->fields[6];

				return true;
			}else
				return false;
		}else
			return false;
	}
         /******************************VA
		 * @name: getPriceByDateAndPbD
                 * 
		 * @return: price 
                 * POST: total days and Serial ProductByDealer
		 */

        public static function getPriceForDays($db, $serial_pbd = null, $days, $serial_pxc= null  ){
		      
              $sql = " SELECT pbpc.price_ppc from price_by_product_by_country pbpc 
                    ";
                  
            if ($serial_pbd){
                $sql .= "  JOIN product_by_dealer pbd  ON pbpc.serial_pxc = pbd.serial_pxc
                        WHERE pbd.serial_pbd = '$serial_pbd' AND pbpc.duration_ppc = '$days' ";
            }
            
            if ($serial_pxc){
                $sql .= " WHERE pbpc.serial_pxc = '$serial_pxc' AND pbpc.duration_ppc = '$days' ";
            }  
            
        //echo $sql;
        //die();
                
                $result = $db->getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
        
        
        

	///GETTERS
	function getSerial_pps() {
		return $this->$serial_pps;
	}

	function getSerial_pbd() {
		return $this->serial_pbd;
	}

	function getDuration_pps() {
		return $this->duration_pps;
	}

	function getPrice_pps() {
		return $this->price_pps;
	}

	function getCost_pps() {
		return $this->cost_pps;
	}

	function getMin_pps() {
		return $this->min_pps;
	}

	function getMax_pps() {
		return $this->max_pps;
	}

	///SETTERS

	function setSerial_pps($serial_pps) {
		$this->serial_pps = $serial_pps;
	}

	function setSerial_pbd($serial_pbd) {
		$this->serial_pbd = $serial_pbd;
	}

	function setDuration_pps($duration_pps) {
		$this->duration_pps = $duration_pps;
	}

	function setPrice_pps($price_pps) {
		$this->price_pps = $price_pps;
	}

	function setCost_pps($cost_pps) {
		$this->cost_pps = $cost_pps;
	}

	function setMin_pps($min_pps) {
		$this->min_pps = $min_pps;
	}

	function setMax_pps($max_pps) {
		$this->max_pps = $max_pps;
	}

}
?>