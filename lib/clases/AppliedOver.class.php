<?php
/*
File: AppliedOver.class.php
Author: Santiago Borja
Creation Date: 17/03/2010
*/
class AppliedOver{
	var $db;
	var $serial_apo;
	var $serial_dea;
	var $serial_mbc;
	var $status_apo;

	function __construct($db, $serial_apo=NULL, $serial_dea=NULL, $serial_mbc=NULL, $status_apo=NULL){
		$this -> db = $db;
		$this -> serial_apo=$serial_apo;
		$this -> serial_dea=$serial_dea;
		$this -> serial_mbc=$serial_mbc;
		$this -> status_apo=$status_apo;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a Over object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_apo!=NULL){
			$sql = 	"SELECT p.serial_apo, p.serial_dea, p.serial_mbc, p.status_apo
                                FROM applied_overs p
                                WHERE p.serial_apo='".$this->serial_apo."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_apo=$result->fields[0];
				$this -> serial_dea=$result->fields[1];
				$this -> serial_mbc=$result->fields[2];
                $this -> status_apo=$result->fields[3];
				return true;
			}
		}
		return false;
	}
	
/**
	@Name: loadInstance
	@Description: Loads this instance serial and status based on its information
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function loadInstance(){
		if($this->serial_apo!=NULL){
			$sql = 	"SELECT p.serial_apo, p.serial_dea, p.serial_mbc, p.status_apo
                                FROM applied_overs p
                                WHERE p.serial_apo='".$this->serial_apo."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_apo=$result->fields[0];
				$this -> serial_dea=$result->fields[1];
				$this -> serial_mbc=$result->fields[2];
                $this -> status_apo=$result->fields[3];
				return true;
			}
		}
		return false;
	}
    
	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO applied_overs (
                    serial_apo,
                    serial_dea,
                    serial_mbc,
                    status_apo)
                VALUES (
                    NULL,
                    ".($this->serial_dea==NULL?'NULL':$this->serial_dea).",
                    ".($this->serial_mbc==NULL?'NULL':$this->serial_mbc).",
                    '".$this->status_apo."')";
            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK/ FALSE on error
	**/
    function update() {
        $sql = "UPDATE applied_overs SET
                    serial_dea=".$this->serial_dea.",
                    serial_mbc=".$this->serial_mbc.",
                    status_apo='".$this->status_apo."'
            WHERE serial_apo = '".$this->serial_apo."'";
            //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        return false;
    }

	function getPresentSales(){
		if($this -> serial_dea == NULL){
			$sqlNow = 	"SELECT sa.total_sal, sa.card_number_sal, CONCAT(cus.first_name_cus, ' ',IFNULL(cus.last_name_cus,'')) as name_cus, sa.total_sal as net_sal, inv.applied_taxes_inv
						FROM sales sa
						JOIN counter cnt
						JOIN dealer dea
						JOIN applied_overs apo
						JOIN info_overs inf
						JOIN customer cus
						JOIN invoice inv
						JOIN payments pay
						
						WHERE sa.serial_cnt = cnt.serial_cnt
						AND cnt.serial_dea = dea.serial_dea
						AND dea.serial_mbc = apo.serial_mbc
						AND inf.serial_apo = apo.serial_apo
						AND cus.serial_cus = sa.serial_cus
						AND sa.serial_inv = inv.serial_inv
						AND (sa.status_sal = 'ACTIVE' OR sa.status_sal = 'EXPIRED')
						AND pay.serial_pay = inv.serial_pay
						AND (pay.status_pay = 'PAID' OR pay.status_pay = 'EXCESS' OR pay.status_pay = 'ONLINE_EFECTIVE')
						AND inv.status_inv = 'PAID'
						AND apo.serial_apo = ".$this -> serial_apo;
			//AND sa.emission_date_sal BETWEEN inf.begin_date_ino AND inf.end_date_ino
		}else{
			$sqlNow = 	"SELECT sa.total_sal, sa.card_number_sal, CONCAT(cus.first_name_cus, ' ',IFNULL(cus.last_name_cus,'')) as name_cus, sa.total_sal as net_sal, inv.applied_taxes_inv
						FROM sales sa
						JOIN counter cnt
						JOIN applied_overs apo
						JOIN info_overs inf
						JOIN customer cus
						JOIN invoice inv
						JOIN payments pay
						
						WHERE sa.serial_cnt = cnt.serial_cnt
						AND cnt.serial_dea = apo.serial_dea
						AND inf.serial_apo = apo.serial_apo
						AND cus.serial_cus = sa.serial_cus
						AND sa.serial_inv = inv.serial_inv
						AND (sa.status_sal = 'ACTIVE' OR sa.status_sal = 'EXPIRED')
						AND pay.serial_pay = inv.serial_pay
						AND (pay.status_pay = 'PAID' OR pay.status_pay = 'EXCESS' OR pay.status_pay = 'ONLINE_EFECTIVE')
						AND inv.status_inv = 'PAID'
						and apo.serial_apo = ".$this -> serial_apo;
			//AND sa.emission_date_sal BETWEEN inf.begin_date_ino AND inf.end_date_ino
		}
		
		//die($sqlNow);
		$result = $this->db->Execute($sqlNow);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				
				$taxes = unserialize($arr[$cont]['applied_taxes_inv']);
				if(is_array($taxes)){
					$taxesToApply = 0;
					foreach($taxes as $tax){
						$taxesToApply += intval($tax['tax_percentage']);
					}
					$arr[$cont]['net_sal'] = $arr[$cont]['total_sal']* (1 + $taxesToApply/100);
				}
				$cont++;
			}while($cont<$num);
			return $arr;
		}
		return false;
    }

	function getPresentTotalSold(){
		if($this -> serial_dea == NULL){
			$sqlNow =  "SELECT SUM(total_sal)
						FROM sales sa
						JOIN counter cnt ON sa.serial_cnt = cnt.serial_cnt
						JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc
						JOIN applied_overs apo ON apo.serial_mbc=mbc.serial_mbc
						JOIN info_overs inf ON inf.serial_apo = apo.serial_apo AND inf.status_ino = 'ACTIVE'
						WHERE sa.emission_date_sal BETWEEN inf.begin_date_ino AND inf.end_date_ino
						AND apo.serial_apo = ".$this -> serial_apo;
		}else{
			$sqlNow =  "SELECT SUM(total_sal)
						FROM sales sa
						JOIN counter cnt ON sa.serial_cnt = cnt.serial_cnt
						JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
						JOIN applied_overs apo ON apo.serial_dea=dea.serial_dea
						JOIN info_overs inf ON inf.serial_apo = apo.serial_apo AND inf.status_ino = 'ACTIVE'
						WHERE sa.emission_date_sal BETWEEN inf.begin_date_ino AND inf.end_date_ino
						AND apo.serial_apo = ".$this -> serial_apo;
		}
		//die($sql);
		$result = $this->db -> Execute($sqlNow);
		
		if($result->fields[0]){
			return $result->fields[0];
		}
		return 0;
    }
    
	function getPastTotalSold(){
		if($this -> serial_dea == NULL){
			$sqlPast = 	"SELECT SUM(total_sal)
						FROM sales sa
						JOIN counter cnt ON sa.serial_cnt = cnt.serial_cnt
						JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc
						JOIN applied_overs apo ON apo.serial_mbc=mbc.serial_mbc
						JOIN info_overs inf ON inf.serial_apo = apo.serial_apo AND inf.status_ino = 'ACTIVE'
						WHERE sa.emission_date_sal BETWEEN DATE_ADD(inf.begin_date_ino, INTERVAL -1 YEAR)
						 	AND DATE_ADD(inf.end_date_ino, INTERVAL -1 YEAR)
						AND apo.serial_apo = ".$this -> serial_apo;
		}else{
			$sqlPast = 	"SELECT SUM(total_sal)
						FROM sales sa
						JOIN counter cnt ON sa.serial_cnt = cnt.serial_cnt
						JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
						JOIN applied_overs apo ON apo.serial_dea=dea.serial_dea
						JOIN info_overs inf ON inf.serial_apo = apo.serial_apo AND inf.status_ino = 'ACTIVE'
						WHERE sa.emission_date_sal BETWEEN DATE_ADD(inf.begin_date_ino, INTERVAL -1 YEAR)
						 	AND DATE_ADD(inf.end_date_ino, INTERVAL -1 YEAR)
						AND apo.serial_apo = ".$this -> serial_apo;
		}
		//die($sql);
		$result = $this->db -> Execute($sqlPast);
		if($result->fields[0]){
			return $result->fields[0];
		}
		return 0;
    }
    
	//Getters
	function getSerial_apo(){
		return $this->serial_apo;
	}
	function getSerial_dea(){
		return $this->serial_dea;
	}
	function getSerial_mbc(){
		return $this->serial_mbc;
	}
	function getStatus_apo(){
		return $this->status_apo;
	}

	//Setters
	function setSerial_apo($serial_apo){
		$this->serial_apo=$serial_apo;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea=$serial_dea;
	}
    function setStatus_apo($status_apo){
		$this->status_apo=$status_apo;
	}
	function setSerial_mbc($serial_mbc){
		$this->serial_mbc=$serial_mbc;
	}
}
?>