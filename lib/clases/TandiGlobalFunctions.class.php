<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TandiGlobalFunctions
{

    public static function getInvoiceByErp($db, $erp_id)
    {
        $sql = "SELECT * FROM invoice WHERE erp_id = $erp_id";
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }

    public static function getInvoicePayment($db, $serial_inv)
    {
        $sql = "SELECT * FROM 
                invoice inv 
                JOIN payments pay on pay.serial_pay = inv.serial_pay
                WHERE
                inv.serial_inv = $serial_inv 
                ";
        //        debug::print_r($sql);die();
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }

    public static function getSalesByInvoiceTandi($db, $card_number_sal, $serial_inv)
    {
        $sql = "SELECT s.serial_sal,s.card_number_sal,cus.first_name_cus as legal_entity_name,
					   CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,
					   s.total_sal,s.status_sal, s.total_cost_sal, s.serial_cnt,
					   inv.discount_prcg_inv, inv.other_dscnt_inv, inv.applied_taxes_inv, id_erp_sal, inv.serial_inv
				FROM sales s
				JOIN counter c ON s.serial_cnt=c.serial_cnt
				JOIN dealer d ON d.serial_dea=c.serial_dea
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				JOIN invoice inv ON s.serial_inv=inv.serial_inv
				WHERE s.card_number_sal=" . $card_number_sal . " AND s.serial_inv = " . $serial_inv . "
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";
        //        Debug::print_r($sql);die();
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        //        Debug::print_r($list);die();
        return $list;
    }

    public static function getInvoices($db, $number_inv, $serial_dbm)
    {
        $sql = "SELECT * from invoice WHERE number_inv = $number_inv AND serial_dbm = $serial_dbm";
        //        debug::print_r($sql);die();
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }
    public static function getWebSalesData($db, $serial_cus)
    {
        if (empty($serial_cus)) {
            //Debug::print_r("entro");die();
            $list = array();
            return $list;
            // Debug::print_r($result);die();

        } else {
            $sql = "SELECT * FROM sales sal
            JOIN customer cus on cus.serial_cus = sal.serial_cus 
            WHERE sal.serial_cus = $serial_cus AND sal.status_sal in ('REGISTERED', 'ACTIVE', 'STANDBY','RESERVED')";

            $result = $db->Execute($sql);

            $num = $result->RecordCount();
            if ($num > 0) {
                $list = array();
                $cont = 0;

                do {
                    $list[$cont] = $result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                } while ($cont < $num);
            }
            //Debug::print_r($list);die();
            return $list;
        }
    }

    /**
      @Name: dealerIDExists
      @Description: Verifies if another dealer exist with the same ID.
      @Params: $id_dea(dealerID), $serial_dea(dealer serialID)
      @Returns: TRUE if exist. / FALSE no match
     * */
    public static function dealerIDExists($db, $id_dea)
    {
        $sql = "SELECT serial_dea
				 FROM dealer
				 WHERE id_dea='" . $id_dea . "' AND dea_serial_dea is NOT NULL";

        //                Debug::print_r($sql);die();
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        //        Debug::print_r($list);die();
        return $list;
    }

    //Check Product Name
    public static function productName($db, $serial_pro, $tipo)
    {
        $sql = "SELECT *
				 FROM product_by_language
                                 WHERE serial_pro = '" . $serial_pro . "'";
        switch ($tipo) {
            case 'DENTAL';
                // $sql .= "AND serial_pro in (7,107,110,104,127,105,115,102,114,106,155,140,159,160,174,175,170,172,171,173,169,157,158,176,186,164,167,168,166,165,188,197,199)";
                $sql .= "AND serial_pro in (1000)";
                break;
            case 'VIDA';
                // $sql .= "AND serial_pro in (110,68,85,168,166,181,182,183,185)";
                $sql .= "AND serial_pro in (1000)";
                break;
            default:
                null;
        }

        //Debug::print_r($sql);die();
        $result = $db->getAll($sql);
        //        Debug::print_r($result[0]);die();
        if ($result[0])
            return $result[0];
        else
            return false;
    }

    public static function getUserByDocument($db, $document_usr)
    {
        $sql = "SELECT serial_usr, email_usr, CONCAT(first_name_usr,' ',last_name_usr) as name_usr
				FROM user
				WHERE document_usr = '$document_usr'";
        $result = $db->getAll($sql);
        if ($result[0])
            return $result[0];
        else
            return false;
    }
    public static function getExtrasByDocument($db, $document_usr)
    {
        $sql = "SELECT ext.serial_cus, ext.fee_ext
				FROM extras ext
                                join customer cus on cus.serial_cus = ext.serial_cus
				WHERE cus.document_cus = '$document_usr'";
        $result = $db->getAll($sql);
        if ($result[0])
            return $result[0];
        else
            return false;
    }

    public static function getDealerTandiId($id_dea)
    {
        global $db;
        $array = array();
        $array['identificacion'] = $id_dea;
        $array['tipo'] = 'Agente';

        if (USE_TANDI) {
            $tandiLog = TandiLog::insert($db, 'DEALER', $id_dea, $array);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.bluecard.com.ec/services/entidad/get",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 3000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic cm9zczpyb3Nz",
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response = json_decode($response, true);
            if ($response['idRespuesta'] == 0) {
                TandiLog::updateLog($db, $tandiLog, "SUCCESS", serialize($response), 420);
            } else {
                TandiLog::updateLog($db, $tandiLog, "Error", serialize($response), 420);
            }
            return $response;
        } else {
            return false;
        }
    }

    public static function getCardNumberMother($db, $serial_dea)
    {
        $sql = "SELECT card_number_mother
                FROM card_dealer
                WHERE serial_dea = '$serial_dea'";
        $result = $db->getAll($sql);
        if ($result[0])
            return $result[0];
        else
            return false;
    }
}
