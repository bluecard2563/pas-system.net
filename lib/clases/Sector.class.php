<?php
/*
File: Sector.class.php
Author: David Bergmann
Creation Date: 07/01/2010 12:11
Last Modified:
Modified By:
*/
class Sector {				
	var $db;
	var $serial_sec;
	var $serial_cit;
	var $code_sec;
	var $name_sec;
	
	function __construct($db, $serial_sec = NULL, $name_cit = NULL, $serial_zon = NULL, $name_zon = NULL){
		$this -> db = $db;
		$this -> serial_sec = $serial_sec;
		$this -> name_cit = $name_cit;		
		$this -> serial_zon = $serial_zon;
		$this -> name_zon = $name_zon;
	}	
	
	/***********************************************
    * function getData
    * gets data by serial_sec
    ***********************************************/
	function getData(){
		if($this->serial_sec!=NULL){
			$sql = 	"SELECT serial_sec, serial_cit, code_sec, name_sec
					FROM sector
					WHERE serial_sec='".$this->serial_sec."'";	
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_sec=$result->fields[0];
				$this->serial_cit=$result->fields[1];
				$this->code_sec=$result->fields[2];
				$this->name_sec=$result->fields[3];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
	function insert(){		
		$sql="INSERT INTO sector (
					serial_sec,
					serial_cit,
					code_sec,
					name_sec				
					)
			  VALUES(
					NULL,
					'".$this->serial_cit."',
					'".$this->code_sec."',
					'".$this->name_sec."'
					);";
		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
	function update(){
		$sql="UPDATE sector
		SET code_sec='".$this->code_sec."', name_sec='".$this->name_sec."'
		WHERE serial_sec='".$this->serial_sec."'";
					
		$result = $this->db->Execute($sql);

		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/***********************************************
    * funcion getZones
    * gets all the Zones of the system
    ***********************************************/
	function getZones(){
		$sql = 	"SELECT serial_sec, serial_cit, code_sec, name_sec
				 FROM sector";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}	
	
	/** 
	@Name: getSectorsByCity
	@Description: Returns an array of sectors in a specific sity.
	@Params: $serial_cit (City ID).
	@Returns: Sector array
	**/
	function getSectorsByCity($serial_cit){
		$list=NULL;
		$sql =	"SELECT s.serial_sec, s.serial_cit, s.name_sec
				FROM sector s
				WHERE s.serial_cit=".$serial_cit;
		$result = $this ->db -> Execute($sql);		
		
			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
			return $arreglo;
		}else{
			return FALSE;
		}
	}	
	
	/** 
	@Name: getSectorsAutocompleter
	@Description: Returns an array of zones.
	@Params: Zone name or pattern.
	@Returns: Zone array
	**/
	function getSectorsAutocompleter($name_sec){
		$sql = "SELECT s.serial_sec,s. name_sec, c.name_cit
				FROM sector s
				JOIN city c ON c.serial_cit=s.serial_cit
				WHERE LOWER(s.name_sec) LIKE _utf8'%".utf8_encode($name_sec)."%' collate utf8_bin
				LIMIT 10";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}
	
	/***********************************************
    * funcion sectorExists
    * verifies if a Sector exists or not
    ***********************************************/
	function sectorExists($txtNameSector, $serial_cit, $serial_sec=NULL){
		$sql = 	"SELECT serial_sec
				 FROM sector
				 WHERE LOWER(name_sec)= _utf8'".utf8_encode($txtNameSector)."' collate utf8_bin
				 AND serial_cit = ".$serial_cit;
		if($serial_sec!=NULL){
			$sql.=" AND serial_sec <> ".$serial_sec;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}
	
	/***********************************************
    * funcion sectorCodeExists
    * verifies if a Sector Code exists or not
    ***********************************************/
	function sectorCodeExists($txtCodeSector, $serial_cit, $serial_sec=NULL){
		$sql = 	"SELECT serial_sec
				 FROM sector
				 WHERE LOWER(code_sec)= _utf8'".utf8_encode($txtCodeSector)."' collate utf8_bin
				 AND serial_cit = ".$serial_cit;
		if($serial_sec!=NULL){
			$sql.=" AND serial_sec <> ".$serial_sec;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}
		
	///GETTERS
	function getSerial_sec(){
		return $this->serial_sec;
	}
	function getSerial_cit(){
		return $this->serial_cit;
	}
	function getCode_sec(){
		return $this->code_sec;
	}
	function getName_sec(){
		return $this->name_sec;
	}

	///SETTERS	
	function setSerial_sec($serial_sec){
		$this->serial_sec = $serial_sec;
	}
	function setSerial_cit($serial_cit){
		$this->serial_cit = $serial_cit;
	}
	function setCode_sec($code_sec){
		$this->code_sec = $code_sec;
	}
	function setName_sec($name_sec){
		$this->name_sec = $name_sec;
	}
}
?>