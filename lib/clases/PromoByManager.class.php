<?php
/*
File: PromoByManager.class.php
Author: Patricio Astudillo
Creation Date: 04/02/2010 17:03
Last Modified: 04/02/2010
Modified By: Patricio Astudillo
*/
class PromoByManager{
	var $db;
	var $serial_prm;
	var $serial_mbc;


	function __construct($db, $serial_prm=NULL, $serial_mbc=NULL){

		$this -> db = $db;
		$this -> serial_prm=$serial_prm;
		$this -> serial_mbc=$serial_mbc;
	}

	function getManagersByPromo($serial_prm){
		$lista=NULL;
		$sql = 	"SELECT pbm.serial_prm, pbm.serial_mbc, man.name_man
                         FROM promo_by_mbc pbm
                         JOIN manager_by_country mbc ON pbm.serial_mbc=mbc.serial_mbc
                         JOIN manager man ON man.serial_man=mbc.serial_man
                         WHERE serial_prm = ".$serial_prm;

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO promo_by_mbc (
                    serial_prm,
                    serial_mbc)
                VALUES (
                    '".$this->serial_prm."',
                    '".$this->serial_mbc."')";

            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function update() {
            $sql = "UPDATE promo_by_mbc SET
                        serial_mbc='".$this->serial_mbc."',
                WHERE serial_prm = '".$this->serial_prm."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

        /**
	* @Name: delete
	* @Description: Deletes some registers in DB
	* @Params: $serial_prm: serial of the promotion.
	* @Returns: TRUE if OK. / FALSE on error
	**/
        function delete($serial_prm) {
            $sql = "DELETE FROM promo_by_mbc
                    WHERE serial_prm = '".$serial_prm."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

        /**
	* @Name: getPromoAutocompleter
	* @Description: Retrieves all the promos acording a pattern
	* @Params: $name: the name of the promo, $serial: the serial of the city
	* @Returns: $array
	**/
        function getPromoAutocompleter($name, $serial){
            $sql = "SELECT p.name_prm, p.serial_prm
		    FROM promo_by_mbc pbm
                    JOIN promo p ON p.serial_prm=pbm.serial_prm AND LOWER(p.name_prm) LIKE _utf8 '%".($name)."%' collate utf8_bin
                    WHERE pbm.serial_mbc=".$serial." AND p.begin_date_prm > NOW()
		    LIMIT 10";

            //echo $sql;

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
        }

	/**
	* @Name: getPromos
	* @Description: Retrieves all the promos acording a pattern
	* @Params: $name: the name of the promo, $serial: the serial of the city
	* @Returns: $array
	**/
        function getPromos($apliedTo,$serial, $all=NULL){
            $sql = "SELECT p.name_prm, p.serial_prm
					FROM promo_by_mbc pbm
                    JOIN promo p ON p.serial_prm=pbm.serial_prm";
			if($apliedTo=='CITY'){
               $sql.=" WHERE pbm.serial_mbc=".$serial;
			}

			if(!$all){
				$sql.=" AND p.begin_date_prm > NOW()";
			}
			$sql.=" LIMIT 10";
//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
        }

	/**
	* @Name: getPromosForReport
	* @Description: Retrieves all the promos acording a pattern
	* @Params: $name: the name of the promo, $serial: the serial of the city
	* @Returns: $array
	**/
        function getPromosForReport($serial_mbc){
            $sql = "SELECT pbmbc.serial_prm,pbl.serial_pbl,pbcbp.serial_pxc,pr.name_prm,pbl.name_pbl,pr.type_prm,pbp.starting_value_pbp,pbp.end_value_pbp,pri.name_pri
					FROM   promo_by_mbc  pbmbc
					JOIN   prize_by_promo pbp ON pbp.serial_prm=pbmbc.serial_prm
					JOIN   prizes pri ON pri.serial_pri=pbp.serial_pri
					JOIN   promo pr ON pr.serial_prm=pbmbc.serial_prm
					JOIN   product_by_country_by_prize pbcbp ON pbcbp.serial_pbp= pbp.serial_pbp
					JOIN   product_by_country pxc ON pxc.serial_pxc= pbcbp.serial_pxc
					JOIN   product p ON p.serial_pro=pxc.serial_pro
					JOIN   product_by_language pbl ON pbl.serial_pro=p.serial_pro
					WHERE  pbmbc.serial_mbc='".$serial_mbc."'
					ORDER BY pbmbc.serial_prm";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
        }

	
	//Getters
	function getSerial_prm(){
		return $this->serial_prm;
	}
	function getSerial_mbc(){
		return $this->serial_mbc;
	}
	

	//Setters
	function setSerial_prm($serial_prm){
		$this->serial_prm=$serial_prm;
	}
	function setSerial_mbc($serial_mbc){
		$this->serial_mbc=$serial_mbc;
	}
 
}
?>