<?php
/*
File: ConditionsByProductType.class.php
Author: Miguel Ponce.
Creation Date: 07/01/2010
Last Modified: 07/01/2010
*/

class ConditionsByProduct
{
    var $db;
    var $serial_pro;
    var $serial_gcn;

    function __construct($db, $serial_pro = NULL, $serial_gcn = NULL)
    {
        $this->db = $db;
        $this->serial_pro = $serial_pro;
        $this->serial_gcn = $serial_gcn;
    }

    /***********************************************
     * funcion getBenefits
     * gets all the Benefits of the system
     ***********************************************/
    function getConditions($serial_cou = NULL, $serial_lang = NULL)
    {
        $lista = NULL;
        $sql = "SELECT cbp.serial_gcn, cbp.serial_pro, glc.url_glc, glc.name_glc
				 FROM conditions_by_product cbp
				 JOIN general_conditions gcn ON gcn.serial_gcn = cbp.serial_gcn AND  gcn.status_gcn = 'ACTIVE'
				 JOIN gconditions_by_language_by_country glc ON glc.serial_gcn=gcn.serial_gcn ";
        if ($serial_cou != NULL) {
            $sql .= " AND glc.serial_cou=$serial_cou";
        }
        if ($serial_cou != NULL) {
            $sql .= " AND glc.serial_lang=$serial_lang";
        }

        $sql .= " WHERE cbp.serial_pro = $this->serial_pro";
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;
            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;

            } while ($cont < $num);
        }
        return $arr;
    }

    function hasGeneralCondition($code_lang)
    {
        $sql = " SELECT cbp.serial_gcn
				FROM conditions_by_product cbp
				JOIN general_conditions gcn ON gcn.serial_gcn = cbp.serial_gcn AND gcn.status_gcn = 'ACTIVE'
				WHERE cbp.serial_pro = {$this->serial_pro}";

        $result = $this->db->Execute($sql);
        if ($result->fields[0]) {
            return $result->fields[0];
        } else {
            return false;
        }
    }

    /***********************************************
     * function insert
     * inserts a new register in DB
     ***********************************************/
    function insert()
    {

        $sql = "
            INSERT INTO conditions_by_product (
				serial_pro,
				serial_gcn)
            VALUES (
				'" . $this->serial_pro . "',
				'" . $this->serial_gcn . "')";
        // echo $sql;
        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /***********************************************
     * funcion update
     * updates a register in DB
     ***********************************************/
    function update()
    {
        $sql = "UPDATE conditions_by_product SET
				serial_gcn ='" . $this->serial_gcn . "'
				WHERE serial_pro = '" . $this->serial_pro . "'";

        //echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if ($result == true)
            return true;
        else
            return false;
    }

    public static function updateGConditionsToProduct($db, $serial_pro, $serial_gcn)
    {
        $sql = "DELETE FROM conditions_by_product WHERE serial_pro = $serial_pro";
        $result = $db->Execute($sql);

        $cbp = new ConditionsByProduct($db, $serial_pro, $serial_gcn);
        return $cbp->insert();
    }

    public static function getGeneralConditionsByProduct($db, $serial_pro, $serial_lang = null)
    {
        $sql = "SELECT 
                    cbp.serial_gcn, 
                    cbp.serial_pro, 
                    glc.serial_lang,
                    glc.url_glc, 
                    glc.name_glc
                FROM conditions_by_product cbp
                INNER JOIN general_conditions gcn ON gcn.serial_gcn = cbp.serial_gcn 
                INNER JOIN gconditions_by_language_by_country glc ON glc.serial_gcn=gcn.serial_gcn
                WHERE cbp.serial_pro = '$serial_pro'
                AND  gcn.status_gcn = 'ACTIVE';
        ";

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $array = array();
            $count = 0;
            do {
                $array[$count] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $count++;
            } while ($count < $num);
        }

        if ($array) {
            return $array;
        } else {
            return false;
        }
    }

    public static function getParticularConditionsByContract($db, $card_number)
    {
        $sql = "SELECT	
                    sal.serial_sal, 
                    sal.card_number_sal, 
                    sal.free_sal, 
                    sal.status_sal, 
		            sal.serial_cnt, 
		            MAX(trl.serial_trl) AS 'serial_trl', 
		            trl.card_number_trl,
		            sal.serial_con
                FROM sales sal
                LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal 
                WHERE sal.card_number_sal = '$card_number' OR trl.card_number_trl = '$card_number'
                AND sal.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED')
                AND trl.status_trl NOT IN ('DELETED')
        ";

        $result = $db->getRow($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }

    ///GETTERS
    function getSerial_pro()
    {
        return $this->serial_pro;
    }

    function getSerial_gcn()
    {
        return $this->serial_gcn;
    }

    ///SETTERS
    function setSerial_pro($serial_pro)
    {
        $this->serial_pro = $serial_pro;
    }

    function setSerial_gcn($serial_gcn)
    {
        $this->serial_gcn = $serial_gcn;
    }

}