<?php
/*
File: ServiceProvider.class.php
Author: Mario L�pez
Creation Date: 18/02/2010
Last Modified:
Modified By:
*/

class ServiceProvider{
    var $db;
    var $serial_spv;
    var $serial_cit;
    var $name_spv;
    var $type_spv;
    var $phone_spv;
    var $email_spv;
    var $address_spv;
    var $medical_simple_spv;
    var $medical_complex_spv;
    var $service_spv;
    var $technical_spv;
    var $status_spv;
    var $aux;

    function __construct($db, $serial_spv=NULL, $serial_cit=NULL, $name_spv=NULL, $type_spv=NULL, $phone_spv=NULL, $email_spv=NULL, $address_spv=NULL, $medical_simple_spv=NULL, $medical_complex_spv=NULL, $service_spv=NULL, $technical_spv=NULL, $status_spv=NULL ){
            $this -> db = $db;
            $this -> serial_spv = $serial_spv;
            $this -> serial_cit = $serial_cit;
            $this -> name_spv = $name_spv;
            $this -> type_spv = $type_spv;
            $this -> phone_spv = $phone_spv;
            $this -> email_spv = $email_spv;
            $this -> address_spv = $address_spv;
            $this -> medical_simple_spv = $medical_simple_spv;
            $this -> medical_complex_spv = $medical_complex_spv;
            $this -> service_spv = $service_spv;
            $this -> technical_spv = $technical_spv;
            $this -> status_spv = $status_spv;
    }

    /***********************************************
    * function getData
    * sets data by serial_spv
    ***********************************************/
    function getData(){
        if($this->serial_spv!=NULL){
            $sql = "SELECT sp.serial_spv, sp.serial_cit, sp.name_spv, sp.type_spv, sp.phone_spv, sp.email_spv, sp.address_spv, sp.medical_simple_spv,
                           sp.medical_complex_spv, sp.service_spv, sp.technical_spv, sp.status_spv, c.serial_cou
                    FROM service_provider sp
                    JOIN city c ON c.serial_cit=sp.serial_cit
                    WHERE sp.serial_spv='".$this->serial_spv."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_spv = $result->fields[0];
                $this -> serial_cit = $result->fields[1];
                $this -> name_spv = $result->fields[2];
                $this -> type_spv = $result->fields[3];
                $this -> phone_spv = $result->fields[4];
                $this -> email_spv = $result->fields[5];
                $this -> address_spv = $result->fields[6];
                $this -> medical_simple_spv = $result->fields[7];
                $this -> medical_complex_spv = $result->fields[8];
                $this -> service_spv = $result->fields[9];
                $this -> technical_spv = $result->fields[10];
                $this -> status_spv = $result->fields[11];
                $this -> aux['serial_cou']=$result->fields[12];

                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "INSERT INTO service_provider (
            			serial_spv,
						serial_cit,
						name_spv,
						type_spv,
						phone_spv,
						email_spv,
						address_spv,
						medical_simple_spv,
						medical_complex_spv,
						service_spv,
						technical_spv)
                VALUES (
                    NULL,
                    ".$this -> serial_cit.",
                    '".$this -> name_spv."',
                    '".$this -> type_spv."',
                    '".$this -> phone_spv."',
                    '".$this -> email_spv."',
                    '".$this -> address_spv."',";
				($this -> medical_simple_spv=='')?$sql.='0,':$sql.=$this -> medical_simple_spv.",";
				($this -> medical_complex_spv=='')?$sql.='0,':$sql.=$this -> medical_complex_spv.",";
				$sql.=	$this -> service_spv.",";
				($this -> technical_spv=='')?$sql.='0)':$sql.=$this -> technical_spv.")";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "UPDATE service_provider SET
                serial_cit=".$this->serial_cit.",
                name_spv='".$this->name_spv."',
                type_spv='".$this->type_spv."',
                phone_spv='".$this->phone_spv."',
                email_spv='".$this->email_spv."',
                address_spv='".$this->address_spv."',
                medical_simple_spv=".$this->medical_simple_spv.",
                medical_complex_spv=".$this->medical_complex_spv.",
                service_spv=".$this->service_spv.",
                technical_spv=".$this->technical_spv.",
                status_spv='".$this->status_spv."'
                WHERE serial_spv = '".$this->serial_spv."'";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true){
            return true;
        }else{
            return false;
        }
    }
    /***********************************************
    * function checkEmailAvailability
    * Checks if an email is available
    ***********************************************/
    function checkEmailAvailability($email_spv=NULL){
        $sql = "SELECT serial_spv
                FROM service_provider
                WHERE email_spv='".$email_spv."'";

        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if($result->fields[0]){
            //it exists so
            return false;
        }else{
            return true;
        }
    }
    /***********************************************
    * funcion getProvidersByCountry
    * gets all the providers in a specific country of the system
    ***********************************************/
        function getProvidersByCountry($serial_cou){
            $sql =  "SELECT p.serial_spv, p.name_spv
                     FROM service_provider p
                     JOIN service_provider_by_country spp ON spp.serial_spv=p.serial_spv AND spp.serial_cou ='".$serial_cou."'
                     ORDER BY name_spv";
            //die($sql);
            $result = $this ->db -> Execute($sql);

                    $num = $result -> RecordCount();
            if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                            $arreglo[$cont]=$result->GetRowAssoc(false);
                            $result->MoveNext();
                            $cont++;
                    }while($cont<$num);
            }

            return $arreglo;
    }

    /***********************************************
    * funcion getProvidersWithPendingClaims
    * gets all the providers with pending assistance claims
    ***********************************************/
    public static function getProvidersWithPendingClaims($db){
        $sql =  "SELECT DISTINCT spv.serial_spv, spv.name_spv
                 FROM service_provider spv
                 JOIN service_provider_by_file spf ON spf.serial_spv = spv.serial_spv
                 JOIN benefits_by_document bbd ON bbd.serial_spf = spf.serial_spf
                 JOIN payment_request prq ON prq.serial_dbf = bbd.serial_dbf
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE dispatched_prq = 'NO'
                 AND prq.status_prq ='APROVED'
				 AND dbf.document_to_dbf = 'PROVIDER'
                 ORDER BY name_spv";
        //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getProviders
    * gets all the providers of the system
    ***********************************************/
        function getProviders($serial_cou){
            $sql =	"SELECT p.serial_spv, p.name_spv
                     FROM service_provider p
					 JOIN city c ON c.serial_cit=p.serial_cit AND c.serial_cou ='".$serial_cou."'
					ORDER BY name_spv";
            //die($sql);
            $result = $this ->db -> Execute($sql);

                    $num = $result -> RecordCount();
            if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                            $arreglo[$cont]=$result->GetRowAssoc(false);
                            $result->MoveNext();
                            $cont++;
                    }while($cont<$num);
            }

            return $arreglo;
    }
    /***********************************************
    * function checkServiceNameAvailability
    * Checks if a name is available in a country
    ***********************************************/
    function checkServiceNameAvailability($name_spv=NULL){
        $sql = "SELECT sp.serial_spv
				FROM service_provider sp
				WHERE sp.name_spv='".$name_spv."'";

        $result = $this->db->Execute($sql);

        if($result->fields[0]){
            //it exists so
            return false;
        }else{
            return true;
        }
    }
    /***********************************************
    * function getAllTypes
    * Return all service types
    ***********************************************/
    function getAllTypes(){
            // Finds all possible status form ENUM column
            $sql = "SHOW COLUMNS FROM service_provider LIKE 'type_spv'";
            $result = $this->db -> Execute($sql);

            $types=$result->fields[1];

            $types = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$types));

            return $types;
    }
    /***********************************************
    * function getAllStates
    * Return all service provider states
    ***********************************************/
    function getAllStates(){
            // Finds all possible status form ENUM column
            $sql = "SHOW COLUMNS FROM service_provider LIKE 'status_spv'";
            $result = $this->db -> Execute($sql);

            $types=$result->fields[1];

            $types = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$types));

            return $types;
    }
    /***********************************************
    * funcion getServiceProviderAutocompleter
    * gets all the providers for the autocompleter
    ***********************************************/
        function getServiceProviderAutocompleter($name_spv){
            $list=NULL;
            $sql = "SELECT sp.serial_spv,
                        sp.name_spv,
                        ct.name_cou
                    FROM service_provider sp
                    JOIN city ci ON ci.serial_cit = sp.serial_cit
                    JOIN country ct ON ct.serial_cou = ci.serial_cou
                    WHERE (
                        LOWER( sp.name_spv ) LIKE _utf8 '%".$name_spv."%'
                        COLLATE utf8_bin
                    )
                    LIMIT 0, 10";
            //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                            $arreglo[$cont]=$result->GetRowAssoc(false);
                            $result->MoveNext();
                            $cont++;
                    }while($cont<$num);
            }
            return $arreglo;
    }
    /***********************************************
    * funcion getActiveServiceProviderAutocompleter
    * gets all the active providers for the autocompleter
    ***********************************************/
        function getActiveServiceProviderAutocompleter($name_spv){
            $list=NULL;
            $sql = "SELECT sp.serial_spv,
                        sp.name_spv,
                        ct.name_cou
                    FROM service_provider sp
                    JOIN city ci ON ci.serial_cit = sp.serial_cit
                    JOIN country ct ON ct.serial_cou = ci.serial_cou
                    WHERE (
                        LOWER( sp.name_spv ) LIKE _utf8 '%".$name_spv."%'
                        COLLATE utf8_bin
                    )
                    AND sp.status_spv = 'ACTIVE'
                    LIMIT 0, 10";
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                            $arreglo[$cont]=$result->GetRowAssoc(false);
                            $result->MoveNext();
                            $cont++;
                    }while($cont<$num);
            }
            return $arreglo;
    }


	 /***********************************************
	* function getAllServiceProviders
	@Name: getAllServiceProviders
	@Description: Get all the service providers in the system.
	@Params:
         None.
	@Returns: array with all service providers
	  *		  false if no results
	***********************************************/
        function getAllServiceProviders(){
            $sql =	"SELECT p.serial_spv, p.name_spv
                     FROM service_provider p
					 ORDER BY name_spv";
            //die($sql);
            $result = $this->db->getAll($sql);
			if($result){
				return $result;
			}else{
				return false;
			}
    }
	
	public static function getFilesByProvidedSummary($db, $serial_spv = NULL, $date_from = NULL, 
													$date_to = NULL, $summary = FALSE)
	{
		if($summary):
			$select_fields = "name_spv, count(f.serial_fle) AS 'files_number'";
			$footer = " GROUP BY spv.serial_spv
						ORDER BY files_number DESC";
		else:
			$select_fields = "spv.serial_spv, name_spv, DATE_FORMAT(f.incident_date_fle, '%m') AS 'month'";
			$footer = " ORDER BY serial_spv, month";
		endif;
		
		$sql = "SELECT $select_fields
				FROM service_provider spv
				JOIN view_coordinated_files vcf ON vcf.serial_spv = spv.serial_spv
				JOIN file f ON f.serial_fle = vcf.serial_fle 
				AND status_fle <> 'void'
				JOIN blog b ON b.serial_fle = f.serial_fle AND type_blg = 'CLIENT'";
		if($serial_spv): $sql .= " AND vcf.serial_spv = $serial_spv"; endif;
		if($date_from): 
			$sql .= " AND STR_TO_DATE(DATE_FORMAT(f.incident_date_fle, '%d/%m/%Y'), '%d/%m/%Y') >= 
							STR_TO_DATE('$date_from', '%d/%m/%Y')"; 
		endif;
		if($date_to): 
			$sql .= " AND STR_TO_DATE(DATE_FORMAT(f.incident_date_fle, '%d/%m/%Y'), '%d/%m/%Y') <= 
							STR_TO_DATE('$date_to', '%d/%m/%Y')"; 
		endif;
		$sql .= " $footer";
		
		//Debug::print_r($sql); die;
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

    //GETTERS
    function getSerial_spv(){
        return $this->serial_spv;
    }
    function getSerial_cit(){
        return $this->serial_cit;
    }
    function getName_spv(){
        return $this->name_spv;
    }
    function getType_spv(){
        return $this->type_spv;
    }
    function getPhone_spv(){
        return $this->phone_spv;
    }
    function getEmail_spv(){
        return $this->email_spv;
    }
    function getAddress_spv(){
        return $this->address_spv;
    }
    function getMedical_simple_spv(){
        return $this->medical_simple_spv;
    }
    function getMedical_complex_spv(){
        return $this->medical_complex_spv;
    }
    function getService_spv(){
        return $this->service_spv;
    }
    function getTechnical_spv(){
        return $this->technical_spv;
    }
    function getStatus_spv(){
        return $this->status_spv;
    }
    function getAux_spv(){
        return $this->aux;
    }

    //SETTERS
    function setSerial_spv($serial_spv){
        $this->serial_spv=$serial_spv;
    }
    function setSerial_cit($serial_cit){
        $this->serial_cit=$serial_cit;
    }
    function setName_spv($name_spv){
        $this->name_spv=$name_spv;
    }
    function setType_spv($type_spv){
        $this->type_spv=$type_spv;
    }
    function setPhone_spv($phone_spv){
        $this->phone_spv=$phone_spv;
    }
    function setEmail_spv($email_spv){
        $this->email_spv=$email_spv;
    }
    function setAddress_spv($adress_spv){
        $this->address_spv=$adress_spv;
    }
    function setMedical_simple_spv($medical_simple_spv){
        $this->medical_simple_spv=$medical_simple_spv;
    }
    function setMedical_complex_spv($medical_complex_spv){
        $this->medical_complex_spv=$medical_complex_spv;
    }
    function setService_spv($service_spv){
        $this->service_spv=$service_spv;
    }
    function setTechnical_spv($technical_spv){
        $this->technical_spv=$technical_spv;
    }
    function setStatus_spv($status_spv){
        $this->status_spv=$status_spv;
    }
}
?>
