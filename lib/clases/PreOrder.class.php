<?php
/*
File: PreOrder.class.php
Author: Santiago Borja
Creation Date: 12/04/2009 11:48
*/
class PreOrder {				
	var $db;
	var $serial_por;
	var $serial;
	var $type_por;
    var $date_por;
	
	function __construct($db, $serial_por = NULL, $serial = NULL, $type_por = NULL, $date_por = NULL){
		$this -> db = $db;
		$this -> serial_por = $serial_por;
		$this -> serial = $serial;
		$this -> type_por = $type_por;
        $this -> date_por = $date_por;
	}
	
	/***********************************************
        * function getData
        * gets data by serial_por
        ***********************************************/
	function getData(){
		if($this->serial_por!=NULL){
			$sql = 	"SELECT serial_por, serial, type_por, UNIX_TIMESTAMP(date_por)
					FROM pre_order
					WHERE serial_por='".$this->serial_por."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_por=$result->fields[0];
				$this->serial=$result->fields[1];	
				$this->type_por=$result->fields[2];
				$this->date_por=$result->fields[3];
				return true;
			}	
		}
		return false;		
	}

	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
		$sql="INSERT INTO pre_order (
					serial_por,
					serial,
					type_por,
                    date_por
					)
			  VALUES(NULL,
					'".$this->serial."',
					'".$this->type_por."',
					FROM_UNIXTIME('".$this->date_por."'));";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	public static function getMyPreOrderSerial($db, $serial){
		$sql="SELECT serial_por FROM pre_order WHERE serial = ".$serial;
		$result = $db -> Execute($sql);
		if($result->fields[0]){
			return $result->fields[0];
		}
		return false;
	}

	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
		$sql="UPDATE pre_order SET 
				type_por='".$this->type_por."',
                date_por=FROM_UNIXTIME(".$this->date_por.")
				WHERE serial_por='".$this->serial_por."'";
		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
			return true;
		return false;
	}
	
	/***********************************************
        * funcion getAllMyPreOrderItems
        * gets all the Pre Order items of this customer
        ***********************************************/
	function getAllMyCartProductItems($lang){
		$sql = 	"SELECT pod.*, pbl.name_pbl, 1 as quantity, pxc.serial_pro,
				    (SELECT cur.symbol_cur
				    FROM benefits_product bbp, currency cur 
				    WHERE bbp.serial_pro = pxc.serial_pro
				    AND bbp.serial_cur = cur.serial_cur
				    LIMIT 0,1)  as symbol_cur,
				    (SELECT cur.serial_cur
				    FROM benefits_product bbp, currency cur 
				    WHERE bbp.serial_pro = pxc.serial_pro
				    AND bbp.serial_cur = cur.serial_cur
				    LIMIT 0,1)  as serial_cur
		
				FROM pre_order_detail pod
				INNER JOIN pre_order por ON pod.serial_por = por.serial_por
				LEFT JOIN product_by_country pxc ON pxc.serial_pxc = pod.serial_pxc
				LEFT JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = ".$lang."
				WHERE por.serial = ".$this -> serial;
		$result = $this -> db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				
				
				//SET THE FORST BENEFIT:
				$benefitsByProduct = new BenefitsByProduct($this -> db); 
				$benefitsByProduct -> setSerial_pro($arr[$cont]['serial_pro']);
				$allBenefits = $benefitsByProduct -> getMyBenefits($lang);
				$ben = $allBenefits[0];
				$arr[$cont]['pro_benefit_name'] = $ben['description_bbl'];
				if($ben['price_bxp'] == '0.00'){
					$arr[$cont]['pro_benefit_cost'] = $ben['description_cbl'];
				}else{
					$arr[$cont]['pro_benefit_cost'] = $ben['symbol_cur'].' '.$ben['price_bxp'];
				}
				if($ben['restriction_price_bxp'] > 0){
					$arr[$cont]['pro_benefit_cost'] .= '('.$ben['symbol_cur'].' '.$ben['restriction_price_bxp'].' '.$ben['description_rbl'].')';
				}
				
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
			return $arr;
		}
		return array();
	}
	
	/***********************************************
        * funcion getAllMyPreOrderItems
        * gets all the Pre Order items of this customer
        ***********************************************/
	function isCartEmpty(){
		$sql = 	"SELECT pod.* FROM pre_order por, pre_order_detail pod WHERE por.serial_por = pod.serial_por AND por.serial = ".$this -> serial." AND por.type_por = '".$this -> type_por."'";
		$result = $this -> db -> Execute($sql);
		$num = $result -> RecordCount();
		
		if($num > 0){
			return false;
		}
		return true;
	}
	
	///GETTERS
	function getSerial_por(){
		return $this->serial_por;
	}
	function getSerial(){
		return $this->serial;
	}
	function getType_por(){
		return $this->type_por;
	}
    function getDate_por(){
		return $this->date_por;
	}

	///SETTERS	
	function setSerial_por($serial_por){
		$this->serial_por = $serial_por;
	}
	function setSerial($serial){
		$this->serial = $serial;
	}
	function setType_por($type_por){
		$this->type_por = $type_por;
	}
    function setDate_por($date_por){
		$this->date_por = $date_por;
	}
}
?>