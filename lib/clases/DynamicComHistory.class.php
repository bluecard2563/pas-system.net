<?php
/**
 * File: DynamicComHistory
 * Author: Patricio Astudillo
 * Creation Date: 18/09/2014
 * Last Modified: 18/09/2014
 * Modified By: Patricio Astudillo
*/

class DynamicComHistory{
	var $db;
	var $id;
	var $dynamicComissions_id;
	var $modifyingUserID;
	var $previousPercentage;
	var $newPercentage;
	var $changeDate;
	
	function __construct($db, $id = NULL) {
		$this->db = $db;
		$this->id = $id;
	}

	
}