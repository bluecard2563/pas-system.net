<?php
/**
 * File: KitsMovement
 * Author: Patricio Astudillo
 * Creation Date: 03-sep-2013
 * Last Modified: 03-sep-2013
 * Modified By: Patricio Astudillo
 */

class KitsMovement{
	var $db;
	var $id;
	var $moved_by;
	var $custodian;
	var $move_date;
	var $qty_moved;
	var $qty_available;
	var $type;
	var $serial_mbc;
	var $serial_dea;
	var $serial_bra;

	/**
	 * @author Patricio Astudillo <pastudillo@planet-assist.net>
	 * @param ADODB $db Data Base connection.
	 * @param INT $id Unique ID for each row.
	 */
	function __construct($db, $id = NULL) {
		$this->db = $db;
		$this->id = $id;
		
		if($this->id){
			$this->load();
		}
	}
	
	/**
	 * @author Patricio Astudillo <pastudillo@planet-assist.net>
	 * @return boolean Loading Process Outcome
	 */
	private function load(){
		$sql = "SELECT	id, moved_by, custodian, move_date, qty_moved, qty_available, type, 
						serial_mbc, serial_dea, serial_bra
				FROM kits_movement
				WHERE id = {$this->id}";
		
		$result = $this->db ->getRow($sql);
		
		if($result){
			$this->id = $result['id'];
			$this->moved_by = $result['moved_by'];
			$this->custodian = $result['custodian'];
			$this->move_date = $result['move_date'];
			$this->qty_moved = $result['qty_moved'];
			$this->qty_available = $result['qty_available'];
			$this->type = $result['type'];
			$this->serial_mbc = $result['serial_mbc'];
			$this->serial_dea = $result['serial_dea'];
			$this->serial_bra = $result['serial_bra'];
			
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @author Patricio Astudillo <pastudillo@planet-assist.net>
	 * @return boolean Process Outcom
	 */
	public function save(){
		if($this->id){ //UPDATE
			$sql = "UPDATE kits_movement SET (
						moved_by = {$this->moved_by}, 
						custodian = {$this->custodian}, 
						qty_available = {$this->qty_available}
					WHERE id = {$this->id}";
				
			$result = $this->db->Execute($sql);
			
			if($result){
				return $this->load();
			}else{
				ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
				return FALSE;
			}
			
		}else{ //INSERT
			$sql = "INSERT INTO kits_movement (
						moved_by, 
						custodian, 
						qty_moved, 
						qty_available, 
						type, 
						serial_mbc, 
						serial_dea, 
						serial_bra
					) VALUES (
						{$this->moved_by}, 
						{$this->custodian}, 
						{$this->qty_moved}, 
						{$this->qty_available}, 
						'{$this->type}',";
				$sql .= ($this->serial_mbc)?$this->serial_mbc.',':'NULL,'; 
				$sql .= ($this->serial_dea)?$this->serial_dea.',':'NULL,'; 
				$sql .= ($this->serial_bra)?$this->serial_bra:'NULL'; 
				$sql .=")";
				
			$result = $this->db->Execute($sql);
			
			if($result){
				$this->id = $this->db->insert_ID();
				return $this->load();
			}else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
				return FALSE;
			}
		}
	}
	
	/**
	 * @author Patricio Astudillo <pastudillo@planet-assist.net>
	 * @param ADODB $db Data_Base connection
	 * @param string $movementType [GLOBAL|MANAGER|DEALER|BRANCH] Table Name for listing available kit's movements. 
	 * @param int $remoteID Unique ID for $movementType table
	 * @return boolean
	 */
	public static function getKitsMovementSerial($db, $movementType, $remoteID = NULL){
		switch($movementType){
			case 'GLOBAL':
				$clause_sql = " WHERE serial_mbc IS NULL
								AND serial_dea IS NULL
								AND serial_bra IS NULL";
				break;
			case 'MANAGER':
				$clause_sql = " WHERE serial_mbc = $remoteID
								AND serial_dea IS NULL
								AND serial_bra IS NULL";
				break;
			
			case 'DEALER':
				$clause_sql = " WHERE serial_mbc IS NOT NULL
								AND serial_dea = $remoteID
								AND serial_bra IS NULL";
				break;
			
			case 'BRANCH':
				$clause_sql = " WHERE serial_mbc IS NOT NULL
								AND serial_dea IS NOT NULL
								AND serial_bra = $remoteID";
				break;
		}
		
		$sql = "SELECT id, qty_available, qty_moved
				FROM kits_movement
				$clause_sql
				AND qty_available > 0
				AND type = 'IN'";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getTotalStockAvailable($db, $movementType, $remoteID = NULL){
		switch($movementType){
			case 'GLOBAL':
				$clause_sql = " WHERE serial_mbc IS NULL
								AND serial_dea IS NULL
								AND serial_bra IS NULL";
				$field_name = " type";
				break;
			case 'MANAGER':
				$clause_sql = " WHERE serial_mbc = $remoteID
								AND serial_dea IS NULL
								AND serial_bra IS NULL";
				$field_name = " serial_mbc";
				break;
			
			case 'DEALER':
				$clause_sql = " WHERE serial_mbc IS NOT NULL
								AND serial_dea = $remoteID
								AND serial_bra IS NULL";
				$field_name = " serial_dea";
				break;
			
			case 'BRANCH':
				$clause_sql = " WHERE serial_mbc IS NOT NULL
								AND serial_dea IS NOT NULL
								AND serial_bra = $remoteID";
				$field_name = " serial_bra";
				break;
		}
		
		$sql = "SELECT id, SUM(qty_available) AS 'available', qty_moved
				FROM kits_movement
				$clause_sql
				AND qty_available > 0
				AND type = 'IN'
				GROUP BY $field_name";
		
		//Debug::print_r($sql);
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @author Patricio Astudillo <pastudillo@planet-assist.net>
	 * @param ADODB $db Data_Base connection
	 * @param int $serial_bra ID for selling Branch
	 * @return boolean Process Outcome
	 */
	public static function kitsStockMinusOne($db, $serial_bra){
		$kitID = self::getKitsMovementSerial($db, 'BRANCH', $serial_bra);
		
		if($kitID){
			$kitID = $kitID['0']['id']; //GETTING OLDEST KIT RANGE AVAILABLE
			
			$sql = "UPDATE kits_movement SET
						qty_available = qty_available - 1
					WHERE id = $kitID";

			$result = $db->Execute($sql);

			if($result){
				$sql = "SELECT qty_available 
						FROM kits_movement
						WHERE id = $kitID";

				$current_stock = $db->getOne($sql);
				
				self::evaluateMinimumStock($db, $serial_bra, $current_stock);
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	public static function evaluateMinimumStock($db, $serial_bra, $current_stock){
		$sql = "SELECT minimum_kits_dea
				FROM dealer
				WHERE serial_dea = $serial_bra";
		
		$minimum_stock = $db->getOne($sql);
		
		if($minimum_stock){
			if($minimum_stock <= $current_stock){
				$branch = new Dealer($db, $serial_bra);
				
				$ubd = UserByDealer::getResponsiblesByDealer($db, $serial_bra, TRUE);
				(isset($ubd))?$ubd = $ubd[0]:$ubd = NULL;
				
				if($ubd && $branch ->getData()){
					$email_data['subject'] = 'ALERTA DE STOCK';
					$email_data['branch'] = $branch->getName_dea().' - '.$branch->getCode_dea();
					$email_data['current_stock'] = $current_stock;
					$email_data['responsible'] = $ubd['name_usr'];
					$email_data['email'] = $ubd['email_usr'];

					if(!GlobalFunctions::sendMail($email_data, 'LOW_STOCK')){
						ErrorLog::log($db, 'MINIMUN STOCK - ALERT FAILED', $email_data);
					}
				}else{
					ErrorLog::log($db, 'MINIMUN STOCK - INSUFICIENT DATA', $serial_bra);
				}
			}
		}
	}
	
	public static function getCustodianListByParameters($db, $serial_mbc, $serial_dea = NULL, $serial_bra = NULL){
		if($serial_dea != '' || $serial_bra != ''){
			if($serial_dea){
				$clause = "JOIN dealer d ON d.serial_dea = u.serial_dea AND d.dea_serial_dea = $serial_dea";
			}else{
				$clause = "WHERE u.serial_dea = $serial_bra";
			}
			
		}else{
			$clause = "WHERE u.serial_mbc = $serial_mbc
						AND u.commission_percentage_usr IS NOT NULL";
		}
		
		$sql = "SELECT u.serial_usr, CONCAT(u.last_name_usr, ' ', u.first_name_usr) AS 'custodian', last_name_usr
				FROM user u
				 $clause
				AND u.status_usr = 'ACTIVE'
					
				UNION 
				
				SELECT u.serial_usr, CONCAT(u.last_name_usr, ' ',u.first_name_usr) AS 'custodian', last_name_usr
				FROM user u
				WHERE serial_mbc = 1
				AND u.status_usr = 'ACTIVE'
				AND u.serial_usr NOT IN (SELECT DISTINCT serial_usr
										FROM assistance_groups_by_users
										WHERE status_agu = 'ACTIVE')
				
				ORDER BY last_name_usr";
		//Debug::print_r($sql);
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getStockAvailableByParameters($db, $stock_level, $holder = NULL, $summarized = TRUE){
		switch($stock_level){
			case 'MANAGER':
				$filter_clause = " AND serial_mbc IS NULL
									AND serial_dea IS NULL
									AND serial_bra IS NULL";
				
				break;
			case 'DEALER':
				$filter_clause = " AND serial_mbc = $holder
									AND serial_dea IS NULL
									AND serial_bra IS NULL";
				
				break;
			case 'BRANCH':
				$filter_clause = " AND serial_mbc IS NOT NULL
									AND serial_dea = $holder
									AND serial_bra IS NULL";
				break;
		}
		
		if($summarized){
			$fields = " SUM(qty_available) AS 'total_available'";
		}else{
			$fields = " id, qty_available";
			$order_by = " ORDER BY id";
		}
		
		$sql = "SELECT $fields
				FROM kits_movement
				WHERE type = 'IN'
				AND qty_available > 0
				$filter_clause
				$order_by";
		
		$result = $db->getAll();
		
		if($result){
			if($summarized){
				return $result[0]['total_available'];
			}else{
				return $result;
			}
		}else{
			return FALSE;
		}
	}
	
	/**
	 * 
	 * @param ADODB $db DB resource
	 * @param String $stock_level ['MANAGER','DEALER','BRANCH'] level
	 * @param INT $stock_from DEA/BRA/MBC Id from which the stock will be taken
	 * @param INT $units QTY of items moved
	 * @param INT $custodian User ID for the person who is the custodian of the stock
	 * @param INT $responsibleForMovement User ID for person who makes the change
	 * @return boolean
	 */
	public static function hasStockAvailableInUpperLevelAndAssignIt($db, $stock_level, $stock_from, $units, $custodian, $responsibleForMovement){
		switch($stock_level){
			case 'MANAGER':
				$available_units = self::getStockAvailableByParameters($db, $stock_level);
				
				if($available_units >= $units){
					$all_availble_units = self::getStockAvailableByParameters($db, $stock_level, NULL, FALSE);
					$units_remaining = $units;
					
					foreach($all_availble_units as $line){
						$available_per_line = $line['qty_available'];
						
						if($available_per_line >= $units_remaining){
							$available_per_line -= $units_remaining;
							self::updateQtyAvailableForLine($db, $line['id'], $available_per_line, $custodian, $responsibleForMovement);
							
							break; //STOCK HAS BEEN REDUCED FOR ASIGNATION
						}else{
							$units_remaining -= $available_per_line;
							self::updateQtyAvailableForLine($db, $line['id'], 0, $custodian, $responsibleForMovement);
						}
					}
					
					return TRUE;
				}else{
					return FALSE;
				}
				break;
			case 'DEALER':
			case 'BRANCH':
				$available_units = self::getStockAvailableByParameters($db, $stock_level, $stock_from);
				
				if($available_units >= $units){
					$all_availble_units = self::getStockAvailableByParameters($db, $stock_level, $stock_from, FALSE);
					$units_remaining = $units;
					
					foreach($all_availble_units as $line){
						$available_per_line = $line['qty_available'];
						
						if($available_per_line >= $units_remaining){
							$available_per_line -= $units_remaining;
							self::updateQtyAvailableForLine($db, $line['id'], $available_per_line, $custodian, $responsibleForMovement);
							
							break; //STOCK HAS BEEN REDUCED FOR ASIGNATION
						}else{
							$units_remaining -= $available_per_line;
							self::updateQtyAvailableForLine($db, $line['id'], 0, $custodian, $responsibleForMovement);
						}
					}
					
					return TRUE;
				}else{
					return FALSE;
				}
				break;
		}
	}
	
	/**
	 * 
	 * @param ADODB $db DB resource
	 * @param INT $id KitsMovement line to be updated
	 * @param INT $qty_moved QTY of items moved
	 * @param INT $custodian User ID for the person who is the custodian of the stock
	 * @param INT $responsibleForMovement User ID for person who makes the change
	 */
	private static function updateQtyAvailableForLine($db, $id, $qty_moved, $custodian, $responsibleForMovement){
		//UPDATE LINE FROM WHICH THE STOCK WAS TAKEN
		$km = new KitsMovement($db, $id);
		$km_out = $km;
		$km->setQty_available($qty_moved);
		$km->save();
		
		//REGISTER OUT LINE FOR STOCK LOG
		$km_out = $km;
		$km_out->setId(NULL);
		$km_out->setMove_date(NULL);
		$km_out->setQty_moved($qty_moved);
		$km_out->setQty_available($qty_moved);
		$km_out->setType('OUT');
		$km_out->setCustodian($custodian);
		$km_out->setMoved_by($responsibleForMovement);
		$km_out->save();
	}

	//************************** GETTERS && SETTERS SECTION
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getMoved_by() {
		return $this->moved_by;
	}

	public function setMoved_by($moved_by) {
		$this->moved_by = $moved_by;
	}

	public function getCustodian() {
		return $this->custodian;
	}

	public function setCustodian($custodian) {
		$this->custodian = $custodian;
	}

	public function getMove_date() {
		return $this->move_date;
	}

	public function setMove_date($move_date) {
		$this->move_date = $move_date;
	}

	public function getQty_moved() {
		return $this->qty_moved;
	}

	public function setQty_moved($qty_moved) {
		$this->qty_moved = $qty_moved;
	}

	public function getQty_available() {
		return $this->qty_available;
	}

	public function setQty_available($qty_available) {
		$this->qty_available = $qty_available;
	}

	public function getType() {
		return $this->type;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function getSerial_mbc() {
		return $this->serial_mbc;
	}

	public function setSerial_mbc($serial_mbc) {
		$this->serial_mbc = $serial_mbc;
	}

	public function getSerial_dea() {
		return $this->serial_dea;
	}

	public function setSerial_dea($serial_dea) {
		$this->serial_dea = $serial_dea;
	}

	public function getSerial_bra() {
		return $this->serial_bra;
	}

	public function setSerial_bra($serial_bra) {
		$this->serial_bra = $serial_bra;
	}

}
?>
