<?php

/*
  Document   : commentsByServiceProvider.class
  Created on : 13/09/2011, 09:54:36 AM
  Author     : Crifa-516
 */

class CommentsByServiceProvider{
	var $db;
    var $serial_csp;
    var $serial_fle;
    var $serial_usr;
    var $serial_spv;
    var $date_csp;
    var $calification_csp;
    var $observations_csp;
    
	function __construct($db, $serial_csp = NULL, $serial_fle = NULL, $serial_usr = NULL, $serial_spv = NULL, $date_csp = NULL, $calification_csp = NULL, $observations_csp = NULL) {
		$this->db = $db;
		$this->serial_csp = $serial_csp;
		$this->serial_fle = $serial_fle;
		$this->serial_usr = $serial_usr;
		$this->serial_spv = $serial_spv;
		$this->date_csp = $date_csp;
		$this->calification_csp = $calification_csp;
		$this->observations_csp = $observations_csp;
	}
	
	/***********************************************
	* funcion getData
	* sets data by serial
	***********************************************/
	function getData(){
		if($this->serial_csp != NULL){
			$sql = 	"SELECT
						serial_csp,
						serial_fle,
						serial_usr,
						serial_spv,
						DATE_FORMAT(date_csp,'%d/%m/%Y') as date_csp,
						calification_csp,
						observations_csp
					FROM comments_by_service_provider
					WHERE serial_csp='".$this->serial_csp."'";

			$result = $this->db->getRow($sql);
			if ($result === false) return false;
			if($result){
				$this->serial_csp = $result['serial_csp'];
				$this->serial_fle = $result['serial_fle'];
				$this->serial_usr = $result['serial_usr'];
				$this->serial_spv = $result['serial_spv'];
				$this->date_csp = $result['date_csp'];
				$this->calification_csp = $result['calification_csp'];
				$this->observations_csp = $result['observations_csp'];
				return true;
			}	
			else
				return false;

		}else{
			return false;
		}
	}
	
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert(){
        $sql = "
            INSERT INTO comments_by_service_provider (
				serial_csp,
				serial_fle,
				serial_usr,
				serial_spv,
				date_csp,
				calification_csp,
				observations_csp
			)
            VALUES(				
				NULL,
				'{$this->serial_fle}',
				'{$this->serial_usr}',
				'{$this->serial_spv}',
				NOW(),
				'{$this->calification_csp}',
				'{$this->observations_csp}'
				)";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
	/***********************************************
    * function getCommentsByServiceProviderByFile
    * @Returns: array of comments by service provider 
	 @Params:
         *	$serial_fle: File ID
    ***********************************************/
	function getCommentsByServiceProviderByFile($serial_fle,$serial_spv = NULL){
		$sql = "SELECT	spf.serial_spv, 
						spv.name_spv, 
						spv.type_spv,	
						csp.serial_csp,
						csp.serial_fle,
						csp.serial_usr,
						DATE_FORMAT(csp.date_csp,'%d/%m/%Y') as date_csp,
						csp.calification_csp,
						csp.observations_csp
				FROM service_provider_by_file spf
				JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
				LEFT JOIN comments_by_service_provider csp ON csp.serial_spv = spf.serial_spv
				WHERE spf.serial_fle = $serial_fle";
		
		if($serial_spv){
			$sql.=" AND csp.serial_spv = $serial_spv";
			$result = $this->db->getRow($sql);
		}else{
			$result = $this->db->getAll($sql);
		}
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	/*
	 * @Name: getCalificationTypes
	 * @Description: Returns an array of all the calification options  DB.
	 * @Params: $db: DB connection
	 * @Returns: Array.
	 */
    public static function getCalificationTypes($db) {
        $sql = "SHOW COLUMNS FROM comments_by_service_provider LIKE 'calification_csp'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
	
	public static function getGradeForServiceProvider($db, $serial_fle, $serial_spv){
		$sql = "SELECT	spf.serial_spv, 
						spv.name_spv, 
						spv.type_spv,	
						csp.serial_csp,
						csp.serial_fle,
						csp.serial_usr,
						DATE_FORMAT(csp.date_csp,'%d/%m/%Y') as date_csp,
						csp.calification_csp,
						csp.observations_csp
					FROM service_provider_by_file spf
					JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
					JOIN comments_by_service_provider csp ON csp.serial_spv = spf.serial_spv
					WHERE csp.serial_fle = $serial_fle
					AND csp.serial_spv = $serial_spv";
		
			$result = $db->getRow($sql);
			
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getGradeStatusForProviders($db, $serial_fle){
		$sql = "SELECT	DISTINCT spf.serial_spv, 
						spv.name_spv, 
						spv.type_spv,
						IF(csp.serial_csp IS NULL, 'NO', 'YES') AS 'graded'
				FROM service_provider_by_file spf
				JOIN service_provider spv ON spv.serial_spv = spf.serial_spv AND spf.serial_fle = $serial_fle
				LEFT JOIN comments_by_service_provider csp ON csp.serial_spv = spf.serial_spv AND csp.serial_fle = spf.serial_fle";
		
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	//GETTERS
	public function getSerial_csp() {
		return $this->serial_csp;
	}

	public function getSerial_fle() {
		return $this->serial_fle;
	}

	public function getSerial_usr() {
		return $this->serial_usr;
	}

	public function getSerial_spv() {
		return $this->serial_spv;
	}

	public function getDate_csp() {
		return $this->date_csp;
	}

	public function getCalification_csp() {
		return $this->calification_csp;
	}

	public function getObservations_csp() {
		return $this->observations_csp;
	}
	//SETTERS
	public function setSerial_csp($serial_csp) {
		$this->serial_csp = $serial_csp;
	}

	public function setSerial_fle($serial_fle) {
		$this->serial_fle = $serial_fle;
	}

	public function setSerial_usr($serial_usr) {
		$this->serial_usr = $serial_usr;
	}

	public function setSerial_spv($serial_spv) {
		$this->serial_spv = $serial_spv;
	}

	public function setDate_csp($date_csp) {
		$this->date_csp = $date_csp;
	}

	public function setCalification_csp($calification_csp) {
		$this->calification_csp = $calification_csp;
	}

	public function setObservations_csp($observations_csp) {
		$this->observations_csp = $observations_csp;
	}




}

?>
