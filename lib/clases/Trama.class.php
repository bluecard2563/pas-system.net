<?php
/**
 * Created by PhpStorm.
 * User: leninpadilla
 * Date: 1/8/19
 * Time: 15:18
 */

class Trama
{
    var $db;
    var $idTrama;
    var $contenido;
    var $hora;
    var $fecha;
    var $idKiosko;
    var $estado;

    function __construct($db, $idTrama= NULL, $contenido=NULL, $hora=NULL, $fecha=NULL, $idKiosko=NULL, $estado=NULL){

        $this->db=$db;
        $this->idTrama=$idTrama;
        $this->contenido=$contenido;
        $this->hora=$hora;
        $this->fecha=$fecha;
        $this->idKiosko=$idKiosko;
        $this->estado=$estado;

    }

    function insert()
    {

        $sql = "INSERT INTO trama (
              contenido,
              hora,
              fecha,
              idKiosko,
              estado
              ) VALUES (
               '" . $this->contenido . "',
               '" . $this->hora . "',
               '" . $this->fecha . "',
               '" . $this->idKiosko . "',
			   '" . $this->estado . "')";

        //Debug::print_r($sql);die();

        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    function getAllData()
    {
        $sql = "SELECT * FROM trama";
        $result = $this->db->getAll($sql);
        if ($result) {
            $resultado = array();
            foreach ($result as $valor) {
                $array = array();
                $array['idTrama'] = $valor['idTrama'];
                $array['contenido'] = $valor['contenido'];
                $array['hora'] = $valor['hora'];
                $array['fecha'] = $valor['fecha'];
                $array['idKiosko'] = $valor['idKiosko'];
                $array['estado'] = $valor['estado'];
                array_push($resultado, $array);
            }
            return $resultado;
        }
    }

    function getData()
    {
        //Debug::print_r()
        if ($this->idTrama != NULL) {
            $sql = "SELECT idTrama,
                        contenido,
              hora,
              fecha,
              idKiosko,
              estado
                    FROM trama
                    WHERE idTrama ='" . $this->idTrama . "'";

            $result = $this->db->Execute($sql);
            if ($result === false)
                return false;

            if ($result->fields[0]) {
                $this->idTrama = $result->fields[0];
                $this->contenido = $result->fields[1];
                $this->hora = $result->fields[2];
                $this->fecha = $result->fields[3];
                $this->idKiosko = $result->fields[4];
                $this->estado = $result->fields[5];

                return true;
            }
        }
        return false;

    }

    function update() {
        $sql = "UPDATE trama SET
					contenido		=	 '" . $this->contenido . "',
					hora		=	 '" . $this->hora . "',
					fecha=		 '" . $this->fecha . "',
					idKiosko=		 '" . $this->idKiosko . "',
                    estado =        '".$this->estado ."'
                WHERE idTrama = '" . $this->idTrama . "'";

        $result = $this->db->Execute($sql);

        if ($result) {
            return true;
        } else {
            ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
            return false;
        }
    }


    //GETTERS

    function getIdTrama() {
        return $this->idTrama;
    }

    function getContenido() {
        return $this->contenido;
    }

    function  getHora() {
        return $this->hora;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getIdKiosko() {
        return $this->idKiosko;
    }

    function getEstado() {
        return $this->estado;
    }
    //SETTERS

    function setIdTrama($idTrama) {
        $this->idTrama=$idTrama;
    }

    function setContenido($contenido) {
        $this->contenido=$contenido;
    }

    function setHora($hora) {
        $this->hora=$hora;
    }

    function setFecha($fecha) {
        $this->fecha=$fecha;
    }

    function setIdKiosko($idKiosko) {
        $this->idKiosko=$idKiosko;
    }

    function setEstado($estado) {
        $this->estado=$estado;
    }
}