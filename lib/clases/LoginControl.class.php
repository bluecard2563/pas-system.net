<?php
/*
  Document   : LoginControl
  Created on : 28/06/2011, 10:38:54 AM
  Author     : Nicolás Flores
 */

class LoginControl{
	var $db;
	var $serial_lgn;
	var $serial_usr;
	var $ip_lgn;
	var $login_date_lgn;
	var $logout_date_lgn;
	
	function __construct($db, $serial_lgn = NULL, $serial_usr = NULL, $ip_lgn = NULL, $login_date_lgn = NULL,$logout_date_lgn = NULL){
		$this -> db = $db;
		$this -> serial_lgn = $serial_lgn;
		$this -> serial_usr = $serial_usr;
		$this -> ip_lgn = $ip_lgn;
		$this -> login_date_lgn = $login_date_lgn;
		$this -> logout_date_lgn = $logout_date_lgn;
	}
	
	/***********************************************
	* function getData
	* gets data by serial_lgn
	***********************************************/
	function getData(){
		if($this->serial_lgn!=NULL){
			$sql = 	"SELECT serial_lgn,
							serial_usr, 
							ip_lgn, 
							DATE_FORMAT(login_date_lgn,'%d/%m/%Y %T') as login_date_lgn, 
							DATE_FORMAT(logout_date_lgn,'%d/%m/%Y %T') as logout_date_lgn
					FROM login_control
					WHERE serial_lgn='".$this->serial_lgn."'";
			//echo $sql." ";		
			$result = $this->db->getRow($sql);
			if ($result === false) return false;

			if($result){
				$this -> serial_lgn = $result['serial_lgn'];
				$this -> serial_usr = $result['serial_usr'];
				$this -> ip_lgn = $result['ip_lgn'];
				$this -> login_date_lgn = $result['login_date_lgn'];
				$this -> logout_date_lgn = $result['logout_date_lgn'];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
	* function insert
    * inserts a new register in DB
	***********************************************/
	function insert(){
		$sql = "INSERT INTO login_control (
					serial_lgn,
					serial_usr,
					ip_lgn,
					login_date_lgn,
					logout_date_lgn
				)VALUES(
					NULL,
					'".$this->serial_usr."',
					'".$this->ip_lgn."',
                    NOW(),
					NULL
				);";
					
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/**
	 * @name registerLogout
	 * @param $db DB_connection
	 * @param $serial_lgn login_control ID
	 * @return true/false
	 */
	public static function registerLogout($db,$serial_lgn){
			$sql = "UPDATE login_control
					SET logout_date_lgn = NOW()
					WHERE serial_lgn = $serial_lgn";

			$result = $db->Execute($sql);
			if($result){
				return true;
			}else{
				ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
				return false;
			}
	}

	public static function getLoginAuditReport($db, $txtDateFrom, $txtDateTo,
												$selManager, $selDealer, $selBranch){

		if($dealer_filter) $dealer_filter = " AND b.dea_serial_dea = $selDealer";
		if($branch_filter) $branch_filter = " AND b.serial_dea = $selBranch";

		$date_filter = " AND lc.login_date_lgn BETWEEN
							STR_TO_DATE('$txtDateFrom', '%d/%m/%Y') AND
							DATE_ADD(STR_TO_DATE('$txtDateTo', '%d/%m/%Y'), INTERVAL 1 DAY)";
		
		if($selDealer || $selBranch){
			$sql = "SELECT CONCAT(u.first_name_usr,' ',u.last_name_usr) as name_usr, u.username_usr,
						lc.login_date_lgn, lc.logout_date_lgn, lc.ip_lgn, b.name_dea AS 'entity'
					FROM user u
					JOIN dealer b ON b.serial_dea = u.serial_dea $dealer_filter $branch_filter
					JOIN login_control lc ON lc.serial_usr=u.serial_usr
					$date_filter";
		}else{
			if($selManager == 1){
				$sql = "	SELECT CONCAT(u.first_name_usr,' ',u.last_name_usr) as name_usr, u.username_usr,
								lc.login_date_lgn, lc.logout_date_lgn, lc.ip_lgn, m.name_man AS 'entity'
							FROM user u
							JOIN manager_by_country mbc ON mbc.serial_mbc = u.serial_mbc
							JOIN manager m ON m.serial_man = mbc.serial_man
							JOIN login_control lc ON lc.serial_usr=u.serial_usr
							WHERE u.serial_mbc = $selManager
							$date_filter";
			}else{
				$sql = "(	SELECT CONCAT(u.first_name_usr,' ',u.last_name_usr) as name_usr, u.username_usr,
								lc.login_date_lgn, lc.logout_date_lgn, lc.ip_lgn, m.name_man AS 'entity'
							FROM user u
							JOIN manager_by_country mbc ON mbc.serial_mbc = u.serial_mbc
							JOIN manager m ON m.serial_man = mbc.serial_man
							JOIN login_control lc ON lc.serial_usr=u.serial_usr
							WHERE u.serial_mbc = $selManager
							$date_filter
						) UNION (
							SELECT CONCAT(u.first_name_usr,' ',u.last_name_usr) as name_usr, u.username_usr,
								lc.login_date_lgn, lc.logout_date_lgn, lc.ip_lgn, b.name_dea AS 'entity'
							FROM user u
							JOIN dealer b ON b.serial_dea = u.serial_dea $dealer_filter $branch_filter
							JOIN login_control lc ON lc.serial_usr=u.serial_usr
							$date_filter
						) ORDER BY entity, name_usr, login_date_lgn";
			}
		}

		//die('<pre>'.$sql.'</pre>');
        $result=$db->getAll($sql);

        if($result){
                return $result;
        }else{
                return false;
        }
	}
	
	///GETTERS
	function getSerial_lgn(){
		return $this->serial_lgn;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getIp_lgn(){
		return $this->ip_lgn;
	}
	function getLogin_date_lgn(){
		return $this->login_date_lgn;
	}
	function getLogout_date_lgn(){
		return $this->logout_date_lgn;
	}

	///SETTERS	
	function setSerial_lgn($serial_lgn){
		$this->serial_lgn = $serial_lgn;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setIp_lgn($ip_lgn){
		$this->ip_lgn = $ip_lgn;
	}
	function setLogin_date_lgn($login_date_lgn){
		$this->login_date_lgn = $login_date_lgn;
	}
	function setLogout_date_lgn($logout_date_lgn){
		$this->logout_date_lgn = $logout_date_lgn;
	}
	
}
?>
