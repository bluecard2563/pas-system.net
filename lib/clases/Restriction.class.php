<?php
/*
File: Restriction.class.php
Author: Patricio Astudillo M.
Creation Date: 29/12/2009 14:17
Last Modified: 21/01/2010
Modified By: Patricio Astudillo
*/

class Restriction{				
	var $db;
	var $serial_rst;
        var $status_rst;

	function __construct($db, $serial_rst=NULL, $status_rst=NULL){
		$this -> db = $db;
		$this -> serial_rst = $serial_rst;
                $this -> status_rst = $status_rst;
	}
	
        /**
	@Name: getData
	@Description: Retrieves the data of a Restriction object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_rst!=NULL){
			$sql = 	"SELECT r.serial_rst, r.status_rst
				 FROM restriction_type r
				 WHERE r.serial_rst='".$this->serial_rst."'";
//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_rst =$result->fields[0];
				$this -> status_rst = $result->fields[1];
			
				return true;
			}else{
				return false;
                        }
		}else
			return false;		
	}
	
	
	/**
	@Name: getRestrictions
	@Description: Returns an array of all the restriction for a specific language.
	@Params: $code_lang: the code of the language
	@Returns: An array of restrictions
	**/
	function getRestrictions($code_lang){
                $lista=NULL;
		$sql = 	"SELECT r.serial_rst, rbl.description_rbl
			 FROM restriction_type r
                         JOIN restriction_by_language rbl ON r.serial_rst = rbl.serial_rst
                         JOIN language l ON l.serial_lang = rbl.serial_lang AND l.code_lang='".$code_lang."'
                         WHERE r.status_rst = 'ACTIVE'";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}

        /**
	@Name: getTranslations
	@Description: Returns an array of all the translations for a specific restrictions
	@Params: $serial_rst: Restriction ID
	@Returns: An array of translations
	**/
	function getTranslations($serial_rst){
		$sql = 	"SELECT rbl.serial_rbl as 'serial', rbl.description_rbl, l.name_lang, rbl.serial_rbl as 'update', l.serial_lang
			 FROM restriction_type r
                         JOIN restriction_by_language rbl ON r.serial_rst = rbl.serial_rst
                         JOIN language l ON l.serial_lang = rbl.serial_lang
                         WHERE r.serial_rst = '".$serial_rst."'";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}
	
	/** 
	@Name: getRestrictionsAutocompleter
	@Description: Returns an array of restrictions.
	@Params: Restriction name or pattern.
	@Returns: Restriction array
	**/
	function getRestrictionsAutocompleter($description_rst, $serial_lang){
		$sql = "SELECT r.serial_rst, rbl.description_rbl, rbl.serial_rbl
			FROM restriction_type r
                        JOIN restriction_by_language rbl ON rbl.serial_rst=r.serial_rst AND rbl.serial_lang='".$serial_lang."'
			WHERE LOWER(rbl.description_rbl) LIKE LOWER(_utf8'%".utf8_encode($description_rst)."%' collate utf8_bin)
			LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/**
	@Name: insert
	@Description: Inserts a register on DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO restriction_type (
                      serial_rst,
                      status_rst)
                VALUES (
                    NULL,
                    'ACTIVE')";

            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }
	
 	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error
	**/
        function update() {
            $sql = "UPDATE restriction_type SET
                           status_rst ='".$this->status_rst."'
                    WHERE serial_rst = '".$this->serial_rst."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

        /**
	@Name: getStatusList
	@Description: Gets all credit days status in DB.
	@Params: N/A
	@Returns: Status array
	**/
	function getStatusList(){
		$sql = "SHOW COLUMNS FROM restriction_type LIKE 'status_rst'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	///GETTERS
	function getSerial_rst(){
		return $this->serial_rst;
	}
	function getStatus_rst(){
		return $this->status_rst;
	}
	

	///SETTERS	
	function setSerial_rst($serial_rst){
		$this->serial_rst = $serial_rst;
	}
	function setStatus_rst($status_rst){
		$this->status_rst = $status_rst;
	}
}
?>