<?php
/*
File: benefitbyLanguage.class.php
Author: Miguel Ponce.
Creation Date: 28/12/2009 11:48
Last Modified: 29/12/2009
Modified By: Miguel Ponce
*/

class PtypebyLanguage{				
	var $db;
	var $serial_ptl;
	var $serial_lang;
	var $serial_tpp;
	var $name_ptl;
	var $description_ptl; 

	function __construct($db, $serial_ptl=NULL, $serial_lang=NULL, $serial_tpp=NULL, $name_ptl=NULL, $description_ptl=NULL ){
	
		$this -> db = $db;
		$this -> serial_ptl = $serial_ptl;
		$this -> serial_lang = $serial_lang;
		$this -> serial_tpp = $serial_tpp;
		$this -> name_ptl = $name_ptl;
		$this -> description_ptl = $description_ptl;
		
	}
	
	/***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
	function getData(){
		if($this->serial_ptl!=NULL){
			$sql = 	"SELECT serial_ptl, serial_lang, serial_tpp, name_ptl, description_ptl
					FROM ptype_by_language
					WHERE serial_ptl='".$this->serial_ptl."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ptl =$result->fields[0];
				$this -> serial_lang = $result->fields[1];
				$this -> serial_tpp =$result->fields[2];
				$this -> name_ptl = $result->fields[3];
				$this -> description_ptl = $result->fields[4];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
        * funcion existProdutTypeName
        * verifies if a existProdutTypeName exists or not
        ***********************************************/
	function existProdutTypeName(){
		$sql = 	"SELECT serial_tpp
				 FROM ptype_by_language 
				 WHERE LOWER(name_ptl)= LOWER('".utf8_encode($this->name_ptl)."')";
		if($this->serial_tpp!=NULL){
			$sql.=" AND serial_tpp <> ".$this->serial_tpp;
		}
		$result = $this->db -> Execute($sql);
                
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
        function insert() {
            $sql = "
                INSERT INTO ptype_by_language (
                                    serial_ptl,
                                    serial_lang,
                                    serial_tpp,
                                    name_ptl,
                                    description_ptl)
                VALUES (
                    NULL,
                                    '".$this->serial_lang."',
                                    '".$this->serial_tpp."',
                                    '".$this->name_ptl."',
                                    '".$this->description_ptl."')";

            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }
	
 	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
        function update() {
            $sql = "
                    UPDATE ptype_by_language SET
                            name_ptl ='".$this->name_ptl."',
                            description_ptl ='".$this->description_ptl."'
                    WHERE serial_tpp = '".$this->serial_tpp."'
                        AND serial_lang='".$this->serial_lang."'";
            //echo $sql;
            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true){
                return true;
            }
            else{
                return false;
            }
        }

	
	///GETTERS
	function getSerial_ptl(){
		return $this->serial_ptl;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}	
	function getSerial_tpp(){
		return $this->serial_tpp;
	}
	function getName_ptl(){
		return $this->name_ptl;
	}
	function getDescription_ptl(){
		return $this->description_ptl;
	}	
	

	///SETTERS	
	
	function setSerial_ptl($serial_ptl){
		$this->serial_ptl = $serial_ptl;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setSerial_tpp($serial_tpp){
		$this->serial_tpp = $serial_tpp;
	}
	function setName_ptl($name_ptl){
		$this->name_ptl = $name_ptl;
	}
	function setDescription_ptl($description_ptl){
		$this->description_ptl = $description_ptl;
	}	
}
?>