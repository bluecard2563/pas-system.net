<?php

/*
  File: Customer.class.php
  Author: Patricio Astudillo
  Creation Date: 30/12/2009 11:32
  Last Modified: 11/01/2010
  Modified By: David Bergmann
 */

class Customer {

    var $db;
    var $serial_cus;
    var $serial_cit;
    var $type_cus;
    var $document_cus;
    var $first_name_cus;
    var $last_name_cus;
    var $birthdate_cus;
    var $phone1_cus;
    var $phone2_cus;
    var $cellphone_cus;
    var $email_cus;
    var $address_cus;
    var $relative_cus;
    var $relative_phone_cus;
    var $vip_cus;
    var $status_cus;
    var $password_cus;
    var $auxData;
    var $medical_background_cus;
    var $first_access_cus;
    var $gender_cus;
    var $segmentation_cus;
    var $id_erp;
    var $migrate_cus;
    var $document_type_cus;

    function __construct($db, $serial_cus = NULL, $serial_cit = NULL, $type_cus = NULL, $document_cus = NULL, $first_name_cus = NULL, $last_name_cus = NULL, $birthdate_cus = NULL, $phone1_cus = NULL,
                         $phone2_cus = NULL, $cellphone_cus = NULL, $email_cus = NULL, $address_cus = NULL, $relative_cus = NULL, $relative_phone_cus = NULL, $vip_cus = NULL, $status_cus = NULL,
                         $password_cus = NULL, $medical_background_cus = NULL, $first_access_cus = NULL, $gender_cus = NULL, $segmentation_cus = NULL, $id_erp=NULL, $migrate_cus = NULL, $document_type_cus = NULL ) {
        $this->db = $db;
        $this->serial_cus = $serial_cus;
        $this->serial_cit = $serial_cit;
        $this->type_cus = $type_cus;
        $this->document_cus = $document_cus;
        $this->first_name_cus = $first_name_cus;
        $this->last_name_cus = $last_name_cus;
        $this->birthdate_cus = $birthdate_cus;
        $this->phone1_cus = $phone1_cus;
        $this->phone2_cus = $phone2_cus;
        $this->cellphone_cus = $cellphone_cus;
        $this->email_cus = $email_cus;
        $this->address_cus = $address_cus;
        $this->relative_cus = $relative_cus;
        $this->relative_phone_cus = $relative_phone_cus;
        $this->vip_cus = $vip_cus;
        $this->status_cus = $status_cus;
        $this->password_cus = $password_cus;
        $this->medical_background_cus = $medical_background_cus;
        $this->first_access_cus = $first_access_cus;
        $this->gender_cus = $gender_cus;
        $this->segmentation_cus = $segmentation_cus;
        $this->id_erp = $id_erp;
        $this->migrate_cus = $migrate_cus;
        $this->document_type_cus = $document_type_cus;
    }

    /*     * *********************************************
     * funcion getData
     * sets data by serial
     * ********************************************* */

    function getData($useEntityCityValue = false) {
        //CRITICAL: ALL CUSTOMERS MUST HAVE A CITY!!!!
        if ($useEntityCityValue) {
            $citySql = "LEFT JOIN city cit ON cit.serial_cit = " . $this->serial_cit;
        } else {
            $citySql = "LEFT JOIN city cit ON cit.serial_cit = c.serial_cit";
        }
        if ($this->serial_cus != NULL) {
            $whereClause = "serial_cus='" . $this->serial_cus . "'";
        } elseif ($this->email_cus != NULL) {
            $whereClause = "email_cus='" . $this->email_cus . "'";
        } elseif ($this->document_cus != NULL) {
            $whereClause = "document_cus='" . $this->document_cus . "'";
        } else {
            return false;
        }
        $sql = "SELECT c.serial_cus, c.serial_cit, c.type_cus, c.document_type_cus, c.document_cus, c.first_name_cus, c.last_name_cus,
				DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y'), c.phone1_cus, c.phone2_cus, c.cellphone_cus, c.email_cus, c.address_cus, c.relative_cus,
				c.relative_phone_cus, c.vip_cus, c.status_cus, c.password_cus, c.medical_background_cus, cit.serial_cou, c.first_access_cus,
				c.gender_cus, c.segmentation_cus, c.id_erp, c.migrate_cus
				FROM customer c $citySql WHERE " . $whereClause;
        
        $result = $this->db->Execute($sql);
        if ($result === false)
            return false;

        if ($result->fields[0]) {
            $this->serial_cus = $result->fields[0];
            $this->serial_cit = $result->fields[1];
            $this->type_cus = $result->fields[2];
            $this->document_type_cus = $result->fields[3];
            $this->document_cus = $result->fields[4];
            $this->first_name_cus = $result->fields[5];
            $this->last_name_cus = $result->fields[6];
            $this->birthdate_cus = $result->fields[7];
            $this->phone1_cus = $result->fields[8];
            $this->phone2_cus = $result->fields[9];
            $this->cellphone_cus = $result->fields[10];
            $this->email_cus = $result->fields[11];
            $this->address_cus = $result->fields[12];
            $this->relative_cus = $result->fields[13];
            $this->relative_phone_cus = $result->fields[14];
            $this->vip_cus = $result->fields[15];
            $this->status_cus = $result->fields[16];
            $this->password_cus = $result->fields[17];
            $this->medical_background_cus = $result->fields[18];
            $this->auxData['serial_cou'] = $result->fields[19];
            $this->first_access_cus = $result->fields[20];
            $this->gender_cus = $result->fields[21];
            $this->segmentation_cus = $result->fields[22];
            $this->id_erp = $result->fields[23];
            $this->migrate_cus = $result->fields[24];
            return true;
        }
        return false;
    }

    /*     * *********************************************
     * funcion getCustomers
     * gets information from Customer
     * ********************************************* */

    function getCustomers($pattern = NULL) {
        $sql = "SELECT serial_cus, serial_cit, type_cus, document_cus, first_name_cus, last_name_cus, birthdate_cus, 
						phone1_cus, phone2_cus, cellphone_cus, email_cus, address_cus, relative_cus, relative_phone_cus, 
						vip_cus, status_cus, password_cus, first_access_cus, CONCAT('C',serial_cus) AS 'oo_code',
                        gender_cus,segmentation_cus, id_erp, migrate_cus
				 FROM customer ";

        if ($pattern != NULL && $pattern != '') {
            $sql.=" WHERE first_name_cus LIKE '%" . utf8_encode($pattern) . "%'
                       OR last_name_cus LIKE '%" . utf8_encode($pattern) . "%'
				       OR document_cus LIKE '%" . utf8_encode($pattern) . "%'
					   OR CONCAT(first_name_cus,' ',last_name_cus) LIKE '%" . utf8_encode($pattern) . "%'
					   OR CONCAT(last_name_cus,' ',first_name_cus) LIKE '%" . utf8_encode($pattern) . "%'
					   OR email_cus LIKE '%" . utf8_encode($pattern) . "%'";
        }

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }
   
    
        /*     * *********************************************
     * funcion getLastSerial
     * gets the serial from the last customer registered
     * ********************************************* */
    function getLastSerial() {
        $sql = "SELECT MAX(serial_cus) as serial 
				 FROM customer ";

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr[0]['serial'];
    }

    /*     * *********************************************
     * function insert
     * inserts a new register in DB 
     * ********************************************* */

    function insert() {
        $sql = "
            INSERT INTO customer (
				serial_cus, 
				serial_cit, 
				type_cus,
				document_type_cus, 
				document_cus, 
				first_name_cus, 
				last_name_cus, 
				birthdate_cus, 
				phone1_cus, 
				phone2_cus, 
				cellphone_cus, 
				email_cus, 
				address_cus, 
				relative_cus, 
				relative_phone_cus, 
				vip_cus, 
				status_cus,
				password_cus,
				first_access_cus, 
                gender_cus,
                segmentation_cus, 
                id_erp, 
                migrate_cus)
            VALUES(				
				NULL,";
        $sql.=($this->serial_cit) ? "'" . $this->serial_cit . "'," : "NULL,";
        $sql.=($this->type_cus) ? "'" . $this->type_cus . "'," : "'PERSON',";
        $sql.=($this->document_type_cus) ? "'" . $this->document_type_cus . "'," : "NULL,";
        $sql.=($this->document_cus) ? "'" . $this->document_cus . "'," : "NULL,";
        $sql.=($this->first_name_cus) ? "'" . $this->first_name_cus . "'," : "NULL,";
        $sql.=($this->last_name_cus) ? "'" . $this->last_name_cus . "'," : "NULL,";
        $sql.=($this->birthdate_cus) ? "STR_TO_DATE('" . $this->birthdate_cus . "','%d/%m/%Y')," : "NULL,";
        $sql.=($this->phone1_cus) ? "'" . $this->phone1_cus . "'," : "NULL,";
        $sql.=($this->phone2_cus) ? "'" . $this->phone2_cus . "'," : "NULL,";
        $sql.=($this->cellphone_cus) ? "'" . $this->cellphone_cus . "'," : "NULL,";
        $sql.=($this->email_cus) ? "'" . $this->email_cus . "'," : "NULL,";
        $sql.=($this->address_cus) ? "'" . $this->address_cus . "'," : "NULL,";
        $sql.=($this->relative_cus) ? "'" . $this->relative_cus . "'," : "NULL,";
        $sql.=($this->relative_phone_cus) ? "'" . $this->relative_phone_cus . "'," : "NULL,";
        $sql.=($this->vip_cus) ? "'" . $this->vip_cus . "'," : "NULL,";
        $sql.=($this->status_cus) ? "'" . $this->status_cus . "'," : "NULL,";
        $sql.=($this->password_cus) ? "'" . $this->password_cus . "'," : "NULL,";
        $sql.=($this->first_access_cus) ? "'" . $this->first_access_cus . "'," : "'YES',";
        $sql.=($this->gender_cus) ? "'" . $this->gender_cus . "'," : "NULL,";
        $sql.=($this->segmentation_cus) ? "'" . $this->segmentation_cus . "'," : "'ND',";
        $sql.=($this->id_erp) ? "'" . $this->id_erp . "'," : "NULL,";
        $sql.=($this->migrate_cus) ? "'" . $this->migrate_cus . "'" : "NULL";
        $sql.=")";
        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /*     * *********************************************
     * funcion update
     * updates a register in DB
     * ********************************************* */

    function update() {
        $sql = "UPDATE customer SET
					serial_cit		=	 '" . $this->serial_cit . "',
					type_cus		=	 '" . $this->type_cus . "',
					document_type_cus =  '" . $this->document_type_cus . "',
					document_cus=		 '" . $this->document_cus . "',
					first_name_cus=		 '" . $this->first_name_cus . "',
					last_name_cus	=	 '" . $this->last_name_cus . "',
					birthdate_cus=		 STR_TO_DATE('" . $this->birthdate_cus . "','%d/%m/%Y'),
					phone1_cus	=		 '" . $this->phone1_cus . "',
					phone2_cus	=		 '" . $this->phone2_cus . "',
					cellphone_cus=		 '" . $this->cellphone_cus . "',
					email_cus=			 '" . $this->email_cus . "',
					address_cus=		 '" . $this->address_cus . "',				
					relative_cus=		 '" . $this->relative_cus . "',
					relative_phone_cus=	 '" . $this->relative_phone_cus . "',
					vip_cus =			 '" . $this->vip_cus . "',
					status_cus=          '" . $this->status_cus . "',
					password_cus=        '" . $this->password_cus . "',
                    medical_background_cus= '" . $this->medical_background_cus . "',
                    first_access_cus=        '" . $this->first_access_cus . "',
                    gender_cus =        '" . $this->gender_cus . "',
                    segmentation_cus =        'ND'
                WHERE serial_cus = '" . $this->serial_cus . "'";
        
        $result = $this->db->Execute($sql);

        if ($result) {
            return true;
        } else {
            ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /*     * *********************************************
     * funcion getAllStatus
     * Returns all kind of Status which exist in DB
     * ********************************************* */

    function getAllStatus() {
        // Finds all possible status form ENUM column 
        $sql = "SHOW COLUMNS FROM customer LIKE 'status_cus'";
        $result = $this->db->Execute($sql);

        $type = $result->fields[1];

        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    /*     * *********************************************
     * funcion getAllTypes
     * Returns all kind of types which exist in DB
     * ********************************************* */

    function getAllTypes() {
        $sql = "SHOW COLUMNS FROM customer LIKE 'type_cus'";
        $result = $this->db->Execute($sql);

        $type = $result->fields[1];

        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    /**
      @Name: getCustomersAutocompleter
      @Description: Returns an array of cities.
      @Params: Customer name or pattern.
      @Returns: Customer array
     * */
    function getCustomersAutocompleter($name_cus) {
        $sql = "SELECT c.serial_cus, c.document_cus, c.first_name_cus, c.last_name_cus
				FROM customer c
				WHERE LOWER(c.first_name_cus) LIKE '%" . utf8_encode($name_cus) . "%'
                OR LOWER(c.last_name_cus) LIKE '%" . utf8_encode($name_cus) . "%'
				OR LOWER(CONCAT(c.first_name_cus,' ',c.last_name_cus)) LIKE '%" . utf8_encode($name_cus) . "%'
				OR LOWER(CONCAT(c.last_name_cus,' ',c.first_name_cus)) LIKE '%" . utf8_encode($name_cus) . "%'
				OR document_cus LIKE _utf8'%" . utf8_encode($name_cus) . "%'
				LIMIT 10";

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arreglo;
    }

    /**
      @Name: getCustomersForReport
      @Description: Returns an array of customers.
      @Params:Country serial.
     * 	City serial (optional)
     *  Stauts (optionl)
      @Returns: Customer array
     * 	  False : when there is any Customer.
     * */
    function getCustomersForReport($serial_cou, $serial_cit = NULL, $status_cus = NULL,$txtBeginDate,$txtEndDate) {
        $sql = "SELECT c.serial_cus, c.type_cus, c.document_cus,sal.card_number_sal as numero_de_contrato,sal.in_date_sal as fecha_venta,CONCAT(c.first_name_cus,' ',IFNULL(c.last_name_cus,'')) as complete_name,DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as birthdate_cus,c.phone1_cus,c.cellphone_cus,c.email_cus,c.relative_cus,c.relative_phone_cus,c.vip_cus,
c.status_cus,cou.serial_cou, cou.name_cou,cit.serial_cit,cit.name_cit
					FROM customer c
					JOIN sales sal ON sal.serial_cus=c.serial_cus
					JOIN city cit ON cit.serial_cit=c.serial_cit
					JOIN country  cou ON cou.serial_cou=cit.serial_cou
					WHERE (STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$txtBeginDate','%d/%m/%Y') AND STR_TO_DATE('$txtEndDate','%d/%m/%Y')) AND cou.serial_cou='" . $serial_cou . "'";
        if ($serial_cit != NULL && $serial_cit != '') {
            $sql.= "AND cit.serial_cit='" . $serial_cit . "'";
        }
        if ($status_cus != NULL && $status_cus != '') {
            $sql.= " AND c.status_cus='" . $status_cus . "'";
        }
        //Debug::print_r($sql);die($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    /**
      @Name: getCustomersByCity
      @Description: Returns an array of customers.
      @Params: City serial.
      @Returns: Customer array
     * */
    function getCustomersByCity($serial_cit) {
        $sql = "SELECT c.serial_cus, c.document_cus, c.first_name_cus, c.last_name_cus, CONCAT(CONCAT(c.first_name_cus,' '),c.last_name_cus) as complete_name
				FROM customer c
                                WHERE c.serial_cit='" . $serial_cit . "'";
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    /**
      @Name: getCreditNoteCustomersByCity
      @Description: Returns an array of customers.
      @Params: City serial.
      @Returns: Customer array
     * */
    function getCreditNoteCustomersByCity($serial_cit, $serial_mbc) {
        $sql = "SELECT distinct c.serial_cus, c.document_cus, c.first_name_cus, c.last_name_cus, CONCAT(CONCAT(c.first_name_cus,' '),c.last_name_cus) as complete_name
				FROM customer c
				JOIN invoice inv ON c.serial_cus=inv.serial_cus
				JOIN sales sal ON inv.serial_inv=sal.serial_inv
				JOIN refund r ON r.serial_sal=sal.serial_sal
				JOIN credit_note cn ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'
				JOIN counter cnt ON cnt.serial_cnt=sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
				JOIN sector sec ON  dea.serial_sec=sec.serial_sec
				WHERE sec.serial_cit='" . $serial_cit . "'";

        if ($serial_mbc != '1') {
            $sql = " AND dea.serial_mbc=" . $serial_mbc;
        }
        //die($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    /*     * *********************************************
     * funcion existsCustomer
     * verifies if a customer exists or not
     * ********************************************* */

    function existsCustomer($document_cus, $serial_cus = NULL) {
        $sql = "SELECT document_cus
				 FROM customer
				 WHERE document_cus= _utf8'" . utf8_encode($document_cus) . "' collate utf8_bin";

        if ($serial_cus != NULL && $serial_cus != '') {
            $sql.= " AND serial_cus <> " . $serial_cus;
        }
//echo $sql;
        $result = $this->db->Execute($sql);

        if ($result->fields[0]) {
            return true;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * funcion existsCustomerEmail
     * verifies if a customers email adrress exists or not
     * ********************************************* */

    function existsCustomerEmail($email_cus, $serial_cus) {
        $sql = "SELECT email_cus
				 FROM customer
				 WHERE email_cus= _utf8'" . utf8_encode($email_cus) . "' collate utf8_bin";
        if ($serial_cus != NULL && $serial_cus != '') {
            $sql.= " AND serial_cus <> " . $serial_cus;
        }
//echo $sql;
        $result = $this->db->Execute($sql);

        if ($result->fields[0]) {
            return true;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * funcion CustomerSerialbyDocument
     * verifies if a customer exists or not
     * ********************************************* */

    function getCustomerSerialbyDocument($document_cus) {
        $sql = "SELECT document_cus,serial_cus 
				 FROM customer
				 WHERE document_cus= _utf8'" . utf8_encode($document_cus) . "' collate utf8_bin";


//echo $sql;
        $result = $this->db->Execute($sql);

        if ($result->fields[0]) {
            return $result->fields[1];
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * funcion getCustomerSerialbyName
     * returns the customers with the name like the given
     * ********************************************* */

    function getCustomerSerialbyName($name_cus) {
        $name = explode(" ", $name_cus);
        $sql = "SELECT DISTINCT c.serial_cus, co.name_cou, cit.name_cit, c.document_cus, c.first_name_cus,
                                 c.last_name_cus, c.birthdate_cus, c.phone1_cus, c.cellphone_cus, c.email_cus, c.address_cus
                 FROM customer c
                 JOIN city cit ON cit.serial_cit = c.serial_cit
                 JOIN country co ON co.serial_cou = cit.serial_cou
                 WHERE LOWER(first_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin
                 OR LOWER(last_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin
				 OR (LOWER(first_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[0])) . "%' collate utf8_bin AND LOWER(last_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[1])) . "%' collate utf8_bin)
				 OR (LOWER(first_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[1])) . "%' collate utf8_bin AND LOWER(last_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[0])) . "%' collate utf8_bin)
                 OR LOWER(CONCAT(first_name_cus,' ',last_name_cus))LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin
                 OR LOWER(CONCAT(last_name_cus,' ',first_name_cus))LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin";

        //die($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $arreglo;
    }

    /*     * *********************************************
     * funcion getCustomerSerialWithFilesbyName
     * returns the customers with the name like the given
     * ********************************************* */

    function getCustomerSerialWithFilesbyName($name_cus) {
        $name = explode(" ", $name_cus);
        $sql = "SELECT DISTINCT c.serial_cus, co.name_cou, cit.name_cit, c.document_cus, c.first_name_cus,
                                 c.last_name_cus, c.birthdate_cus, c.phone1_cus, c.cellphone_cus, c.email_cus, c.address_cus
                 FROM customer c
                 JOIN city cit ON cit.serial_cit = c.serial_cit
                 JOIN country co ON co.serial_cou = cit.serial_cou
				 JOIN file fle ON fle.serial_cus = c.serial_cus
                 WHERE LOWER(first_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin
                 OR LOWER(last_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin
				 OR (LOWER(first_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[0])) . "%' collate utf8_bin AND LOWER(last_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[1])) . "%' collate utf8_bin)
				 OR (LOWER(first_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[1])) . "%' collate utf8_bin AND LOWER(last_name_cus)LIKE _utf8'%" . strtolower(utf8_encode($name[0])) . "%' collate utf8_bin)
                 OR LOWER(CONCAT(first_name_cus,' ',last_name_cus))LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin
                 OR LOWER(CONCAT(last_name_cus,' ',first_name_cus))LIKE _utf8'%" . strtolower(utf8_encode($name_cus)) . "%' collate utf8_bin";

        //die($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $arreglo;
    }

    /*     * *********************************************
     * funcion existsCustomer
     * verifies if a customer exists or not
     * ********************************************* */

    function getCustomersMasive($exist) {
        $sql = "SELECT *
				 FROM customer
				 WHERE serial_cus IN (";
        $ban = 1;
        foreach ($exist as $customer) {
            if (!$customer['serial_sal']) {
                if ($ban != 1) {
                    $sql .=",";
                }
                $ban = 0;
                $sql.="'" . $customer['serial_cus'] . "'";
            }
        }
        $sql .=")";
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $arreglo;
    }

    function loadData($data) {
        $emailIn = "";
        $documentIn = "";
        $documentCbmIn = "";
        $has_email = false;

        if (is_array($data['document_cus'])) {
            foreach ($data['document_cus'] as $row => $document_cus) {
                if ($data['document_cus'][$row]) {
                    if ($documentIn != '') {
                        $documentIn.=",";
                    }
                    $documentIn.="'" . $data['document_cus'][$row] . "'";
                }

                if ($data['email_cus'][$row]) {
                    $has_email = true;
                    if ($emailIn != '') {
                        $emailIn.=",";
                    }
                    $emailIn.="'" . $data['email_cus'][$row] . "'";
                }
            }

            $sqlDocument = " cus.document_cus IN(" . $documentIn . ")	";
            $sqlMail = " cus.email_cus IN(" . $emailIn . ")		";

            if ($documentIn || $emailIn) {
                $sql = " SELECT DISTINCT cus.document_cus, cus.email_cus, cus.serial_cus,
						   cus.serial_cit, cus.type_cus, cus.first_name_cus, cus.last_name_cus,
						   cus.birthdate_cus, cus.phone1_cus, cus.phone2_cus, cus.cellphone_cus,
						   cus.address_cus, cus.relative_cus, cus.relative_phone_cus, cus.vip_cus,
						   cus.status_cus
						FROM customer cus";

                if (sizeof($data['document_cus']) > 0) {
                    $sql.=" WHERE ";
                    $sql.=$sqlDocument;
                    if ($has_email) {
                        $sql.=' OR ';
                        $sql.=$sqlMail;
                    }
                } else {
                    if ($has_email) {
                        $sql.=$sqlMail;
                    }
                }
                ErrorLog::log($this->db, 'SQL multiple', $sql);
                $customer_in_db = $this->db->getAll($sql);
            }

            if ($customer_in_db) {
                $array = array();

                foreach ($customer_in_db as $cont => $line) {
                    $array['document_cus'][$cont] = $line[0];
                    $array['email_cus'][$cont] = $line[1];
                    $array['exist'][$cont]['document_cus'] = $line[0];
                    $array['exist'][$cont]['email_cus'] = $line[1];
                    $array['exist'][$cont]['serial_cus'] = $line[2];
                    $array['exist'][$cont]['serial_cit'] = $line[3];
                    $array['exist'][$cont]['type_cus'] = $line[4];
                    $array['exist'][$cont]['first_name_cus'] = $line[5];
                    $array['exist'][$cont]['last_name_cus'] = $line[6];
                    $array['exist'][$cont]['birthdate_cus'] = $line[7];
                    $array['exist'][$cont]['phone1_cus'] = $line[8];
                    $array['exist'][$cont]['phone2_cus'] = $line[9];
                    $array['exist'][$cont]['cellphone_cus'] = $line[10];
                    $array['exist'][$cont]['address_cus'] = $line[11];
                    $array['exist'][$cont]['relative_cus'] = $line[12];
                    $array['exist'][$cont]['relative_phone_cus'] = $line[13];
                    $array['exist'][$cont]['vip_cus'] = $line[14];
                    $array['exist'][$cont]['status_cus'] = $line[15];
                    $array['exist'][$cont]['serial_sal'] = $line[16];
                }

                $data2['email_cus'] = array_intersect($data['email_cus'], $array['email_cus']);
                $data2['document_cus'] = array_intersect($data['document_cus'], $array['document_cus']);

                foreach ($data2['email_cus'] as $key => $value) {
                    if (!$value) {
                        unset($data2['email_cus'][$key]);
                    }
                }

                $documents = array_flip($data2['document_cus']);
                $emails = array_flip($data2['email_cus']);

                foreach ($array['exist'] as $key => &$customerExist) {
                    if ($documents[$customerExist['document_cus']]) {
                        $customerExist['serial_cus_xls'] = $data['serial_cus_xls'][$documents[$customerExist['document_cus']]];
                        $customerExist['cus_serial_cus_xls'] = $data['cus_serial_cus_xls'][$documents[$customerExist['document_cus']]];
                        $customerExist['start_trl'] = $data['start_trl'][$documents[$customerExist['document_cus']]];
                        $customerExist['end_trl'] = $data['end_trl'][$documents[$customerExist['document_cus']]];
                        $customerExist['relationship_ext'] = $data['relationship_ext'][$documents[$customerExist['document_cus']]];
                        $customerExist['destination_cit'] = $data['destination_cit'][$documents[$customerExist['document_cus']]];
                    } else if ($emails[$customerExist['email_cus']]) {
                        $customerExist['serial_cus_xls'] = $data['serial_cus_xls'][$emails[$customerExist['email_cus']]];
                        $customerExist['cus_serial_cus_xls'] = $data['cus_serial_cus_xls'][$emails[$customerExist['email_cus']]];
                        $customerExist['start_trl'] = $data['start_trl'][$emails[$customerExist['email_cus']]];
                        $customerExist['end_trl'] = $data['end_trl'][$emails[$customerExist['email_cus']]];
                        $customerExist['relationship_ext'] = $data['relationship_ext'][$emails[$customerExist['email_cus']]];
                        $customerExist['destination_cit'] = $data['destination_cit'][$emails[$customerExist['email_cus']]];
                    }
                }

                //serial_cus refers to the serial in the excel file
                $insert['serial_cus_xls'] = array_diff_key(array_diff_key($data['serial_cus_xls'], $data2['document_cus']), $data2['email_cus']);
                $insert['cus_serial_cus_xls'] = array_diff_key(array_diff_key($data['cus_serial_cus_xls'], $data2['document_cus']), $data2['email_cus']);
                $insert['document_cus'] = array_diff_key(array_diff_key($data['document_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['first_name_cus'] = array_diff_key(array_diff_key($data['first_name_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['last_name_cus'] = array_diff_key(array_diff_key($data['last_name_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['phone1_cus'] = array_diff_key(array_diff_key($data['phone1_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['phone2_cus'] = array_diff_key(array_diff_key($data['phone2_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['cellphone_cus'] = array_diff_key(array_diff_key($data['cellphone_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['email_cus'] = array_diff_key(array_diff_key($data['email_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['address_cus'] = array_diff_key(array_diff_key($data['address_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['relative_cus'] = array_diff_key(array_diff_key($data['relative_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['relative_phone_cus'] = array_diff_key(array_diff_key($data['relative_phone_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['birthdate_cus'] = array_diff_key(array_diff_key($data['birthdate_cus'], $data2['document_cus']), $data2['email_cus']);
                $insert['serial_cit'] = array_diff_key(array_diff_key($data['serial_cit'], $data2['document_cus']), $data2['email_cus']);
                $insert['start_trl'] = array_diff_key(array_diff_key($data['start_trl'], $data2['document_cus']), $data2['email_cus']);
                $insert['end_trl'] = array_diff_key(array_diff_key($data['end_trl'], $data2['document_cus']), $data2['email_cus']);
                $insert['destination_cit'] = array_diff_key(array_diff_key($data['destination_cit'], $data2['document_cus']), $data2['email_cus']);
                $insert['relationship_ext'] = array_diff_key(array_diff_key($data['relationship_ext'], $data2['document_cus']), $data2['email_cus']);

                return array('exist' => $array['exist'], 'insert' => $insert);
            }

            return array('insert' => $data);
        }
    }

    function deleteData($data, $serial_sal) {
        if ($data != NULL) {
            $emailIn = "";
            $documentIn = "";
            $documentCbmIn = "";

            foreach ($data['document_cus'] as $row => $document_cus) {

                if ($data['document_cus'][$row]) {
                    if ($row - 1 != 1) {
                        $documentIn.=",";
                    }
                    $documentIn.="'" . $data['document_cus'][$row] . "'";
                }

                if ($data['email_cus'][$row]) {
                    if ($row - 1 != 1) {
                        $emailIn.=",";
                    }
                    $emailIn.="'" . $data['email_cus'][$row] . "'";
                }
            }

            $sql = " DELETE FROM traveler_log WHERE serial_cus IN
					(	SELECT cus.serial_cus
							FROM customer cus
							WHERE
								cus.email_cus IN($emailIn)
								OR
								cus.document_cus IN($documentIn)	)
						AND serial_sal='$serial_sal'";
            $result = $this->db->Execute($sql);
        } else {
            $sql = " DELETE
					FROM traveler_log
					WHERE serial_sal='$serial_sal'";
            $result = $this->db->Execute($sql);
        }
    }

    /**
      @Name: updateInfoSale
      @Description: Updates data without updating Birthdate, Document, Firstname and Lastname
      @Params: Customer serial
      @Returns: TRUE if Ok / FALSE
     * */
    function updateInfoSale($serial_cus) {
        $sql = "UPDATE customer SET
					serial_cit		=	 '" . $this->serial_cit . "',
					document_type_cus =	 '" . $this->document_type_cus . "',
					first_name_cus=		 '" . $this->first_name_cus . "',
					last_name_cus	=	 '" . $this->last_name_cus . "',
					phone1_cus	=		 '" . $this->phone1_cus . "',
					phone2_cus	=		 '" . $this->phone2_cus . "',
					cellphone_cus=		 '" . $this->cellphone_cus . "',
					email_cus=			 '" . $this->email_cus . "',
					address_cus=		 '" . $this->address_cus . "',
					relative_cus=		 '" . $this->relative_cus . "',
					relative_phone_cus=	 '" . $this->relative_phone_cus . "',
					status_cus=          '" . $this->status_cus . "',
					birthdate_cus=          STR_TO_DATE('" . $this->birthdate_cus . "','%d/%m/%Y'),
					gender_cus=          '" . $this->gender_cus . "'
                WHERE serial_cus = '" . $serial_cus . "'";
//echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false)
            return false;

        if ($result == true)
            return true;
        else
            return false;
    }

    /**
      @Name: loadSalesByCustomer
      @Description: Returns all the necesary information from a customer´s cards
      @Params: Customer serial
      @Returns: Array with the data
     * */
    function loadSalesByCustomer() {
        /*         $sql = "SELECT DISTINCT IFNULL(s.card_number_sal, tl.card_number_trl) as card_number_sal, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
          DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal, DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal, s.status_sal
          FROM customer c
          JOIN city ct ON ct.serial_cit = c.serial_cit
          JOIN country co ON co.serial_cou = ct.serial_cou
          LEFT JOIN extras e ON e.serial_cus = c.serial_cus
          LEFT JOIN traveler_log tl ON tl.serial_cus = c.serial_cus
          LEFT JOIN sales s ON s.serial_cus = c.serial_cus OR s.serial_sal = e.serial_sal OR s.serial_sal = tl.serial_sal
          WHERE c.serial_cus = '{$this->serial_cus}'
          OR e.serial_cus = '{$this->serial_cus}'"; */

        $sql = "SELECT DISTINCT IFNULL(tl.card_number_trl, s.card_number_sal) as card_number_sal, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
                           DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal, DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal, s.status_sal
                    FROM sales s
				    LEFT JOIN traveler_log tl ON tl.serial_sal = s.serial_sal
				    LEFT JOIN extras e ON s.serial_sal = e.serial_sal
					WHERE tl.serial_cus = '{$this->serial_cus}' OR s.serial_cus = '{$this->serial_cus}' OR e.serial_cus = '{$this->serial_cus}'";

        //echo '<pre>'.$sql.'</pre>';
        $result = $this->db->getAll($sql);
        if ($result)
            return $result;
        else
            return false;
    }

    /*     * *********************************************
     * function getRegisterCardByCustomer
      @Name: getRegisterCardByCustomer
      @Description: Get the  non expired cards in wich you can register travels by customer.
      @Params:
     *       $this->serial_usr: object atribute for serial_usr
      @Returns:     array of results
     *        3: false when no results
     * ********************************************* */

    function getRegisterCardByCustomer($for_report = FALSE, $for_register = FALSE) {
        $sql = "SELECT s.serial_sal,pbl.name_pbl,s.card_number_sal, s.observations_sal,
                    DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
                    DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
                    s.days_sal,IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal) as available_days
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
			JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
			LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal AND tl.status_trl <> 'DELETED'
			WHERE s.serial_cus={$this->serial_cus}";
        if (!$for_report) {
            $sql.="	AND s.end_date_sal >= CURDATE()";
        }
        if ($for_register) {
            $sql.="	AND s.status_sal IN ('ACTIVE','REGISTERED')";
        }
        $sql.="	GROUP BY s.serial_sal
				ORDER BY s.begin_date_sal";

        $result = $this->db->getAll($sql);
        if ($result)
            return $result;
        else
            return false;
    }

    /*     * *********************************************
     * function getRegisterCustomerAutocompleter
      @Name: getRegisterCustomerAutocompleter
      @Description: Get the customers who has cards where you can register travels.
      @Params:
     *       $this->serial_usr: object atribute for serial_usr
      @Returns:     array of results
     *        3: false when no results
     * ********************************************* */

    function getRegisterCustomerAutocompleter($name_cus) {
        $sql = "SELECT serial_cus, customer_name
				FROM view_corporative_card_holders
				WHERE LOWER( customer_name ) LIKE _utf8 '%" . utf8_encode($name_cus) . "%'
				COLLATE utf8_bin
				ORDER BY customer_name DESC";
        //echo $sql;
        $result = $this->db->getAll($sql);
        if ($result)
            return $result;
        else
            return false;
    }

    /**
     *
     * @param <ADODB> $db
     * @param <ID> $serial_cus
     * @return <ARRAY> Array for users data.
     */
    public static function getGeographicalCustomerAddress($db, $serial_cus) {
        $sql = "SELECT cou.name_cou, cou.serial_cou, cit.name_cit, cit.serial_cit, c.address_cus
			  FROM customer c
			  JOIN city cit ON cit.serial_cit=c.serial_cit
			  JOIN country cou ON cou.serial_cou=cit.serial_cou
			  WHERE c.serial_cus=$serial_cus";

        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function customerOnBlackList($db, $serial_cus) {
        $sql = "SELECT c.first_name_cus, c.last_name_cus, c.serial_cus, c.document_cus
			  FROM customer c
			  WHERE c.serial_cus = $serial_cus
			  AND c.status_cus = 'INACTIVE'";

        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getSerialCustomerByDocument($db, $document_cus){
		$sql = "SELECT cus.serial_cus FROM customer cus                        
                        where cus.document_cus = '$document_cus'";
//echo $sql; die();
                $result = $db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
    
    public static function updatePassword($db, $serial_cus, $newPassword){
		$sql = "UPDATE customer SET password_cus = md5('$newPassword') WHERE serial_cus =$serial_cus";
        //echo $sql; die();
                $result = $db->Execute($sql);

		if($result){
			return true;
		}else{
			return false;
		}		
	}
    public static function updateGender($db, $serial_cus, $cusGender){
		$sql = "UPDATE customer SET gender_cus = '$cusGender' WHERE serial_cus=$serial_cus";
//        echo $sql; die();
                $result = $db->Execute($sql);
     
		if($result){
			return true;
		}else{
			return false;
		}		
	}
     
    /** **************************** VA
     * getCustomerBySerial*
     Gets information user for new sale 
     * @Params: Customer name or pattern.
    */
    public static function getCustomerBySerial($db, $serial_cus){
		$sql = "SELECT cus.serial_cus,cus.document_cus, cus.first_name_cus, 
                        cus.last_name_cus, cus.birthdate_cus, 
                        cus.address_cus, cus.phone1_cus, 
                        cus.phone2_cus, cus.cellphone_cus, 
                        cus.email_cus, cus.relative_cus, 
                        cus.relative_phone_cus, cus.serial_cit, 
                        cit.name_cit, cou.name_cou, cit.serial_cit, cou.serial_cou FROM customer cus 
                        JOIN city cit oN cus.serial_cit = cit.serial_cit
                        JOIN country cou oN cit.serial_cou= cou.serial_cou 
                        where cus.serial_cus = '$serial_cus'";
//echo $sql; die();
                $result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
        
        /** **************************** VA
     * getExistCustomerByUser*
     Gets information mail by user 
     * @Params: email cus.
    */
    public static function getExistCustomerByUser($db, $user_cus){
		$sql = "SELECT cus.serial_cus FROM customer cus 
                        where cus.email_cus = '$user_cus'  ";
    //echo $sql; die();
                $result = $db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
       /** **************************** VA
     * getCustomerByUserAndPass
     Gets information user for new sale 
     * @Params: Customer name or pattern.
    */
    public static function getCustomerByUserAndPass($db, $user_cus, $pass_cus){
		$sql = "SELECT cus.serial_cus, cus.document_cus, cus.first_name_cus, 
                        cus.last_name_cus, cus.birthdate_cus, 
                        cus.address_cus, cus.phone1_cus, 
                        cus.phone2_cus, cus.cellphone_cus, 
                        cus.email_cus, cus.relative_cus, 
                        cus.relative_phone_cus, cus.serial_cit, 
                        cit.name_cit, cou.name_cou, cou.serial_cou  FROM customer cus 
                        JOIN city cit oN cus.serial_cit = cit.serial_cit
                        JOIN country cou oN cit.serial_cou= cou.serial_cou 
                        where cus.email_cus = '$user_cus' AND cus.password_cus='$pass_cus'";
//echo $sql; die();
                $result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
 /** **************************** VA
     * getCustomerByUserAndPass
     Gets information user for new sale 
     * @Params: Customer name or pattern.
  */
       
	public static function getMailList($db, $serial_cou, $serial_cit = NULL){
		$sql= "Select distinct concat(cus.first_name_cus, ' ', cus.last_name_cus) as nombre, 
				cus.email_cus as email 
                from customer cus join city cit on cit.serial_cit = cus.serial_cit 
                JOIN country  cou ON cou.serial_cou=cit.serial_cou where cou.serial_cou = ' $serial_cou ' and cus.email_cus is not null and cus.email_cus != ''" ;
		if($serial_cit != NULL)
		{
			 $sql.= "AND cit.serial_cit='". $serial_cit . "'";
		}
		$sql.=" union Select distinct dea.manager_name_dea nombre, dea.manager_email_dea email
				from dealer dea join manager_by_country mbc on mbc.serial_mbc = dea.serial_mbc  
				JOIN country cou2 ON cou2.serial_cou=mbc.serial_cou 
				JOIN city ci ON ci.serial_cou= cou2.serial_cou where cou2.serial_cou = ' $serial_cou ' and dea.manager_email_dea is not null and dea.manager_email_dea != '' ";

		if($serial_cit != NULL)
		{
			 $sql.= "AND ci.serial_cit='". $serial_cit . "'";
		}
//		Debug::print_r($sql);die();
		
		$result = $db->execute($sql);
		//die($result);
		//$result= $db->fetch_assoc($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
		
        return $arr;	
		
	}

	public function getCustomerTripLastWeek($selDateType, $txtDateFrom, $txtDateTo){
	    $field = "sal.begin_date_sal";
	    if ($selDateType === "endDate"){
            $field = "sal.end_date_sal";
        }
	    $sql = "SELECT 
                        concat(cus.first_name_cus,' ',cus.last_name_cus) as 'Cliente',
                        cus.cellphone_cus as 'Celular',
                        cus.phone1_cus as 'Telefono',
                        cus.email_cus as 'Correo',
                        cou.name_cou as 'Destino',
                        pbl.name_pbl as 'Producto',
                        cit2.name_cit as 'Ciudad Venta',
                        DATE_FORMAT(".$field.",'%d/%m/%Y') as 'Fecha',
                        sal.card_number_sal as 'Tarjeta'
                FROM sales sal 
                    JOIN customer cus on cus.serial_cus = sal.serial_cus
                    JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
                    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
                    JOIN product pr ON pr.serial_pro=pxc.serial_pro
                    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang=2
                    JOIN counter cnt on cnt.serial_cnt = sal.serial_cnt
                    JOIN dealer dea on dea.serial_dea = cnt.serial_dea
                    JOIN city cit on cit.serial_cit = sal.serial_cit
                    JOIN country cou on cit.serial_cou = cou.serial_cou
                    JOIN zone zon on zon.serial_zon = cou.serial_zon
                    JOIN sector sec on sec.serial_sec = dea.serial_sec
                    JOIN city cit2 on cit2.serial_cit = sec.serial_sec
                WHERE pbl.serial_lang = 2
                AND ".$field." between    '$txtDateFrom' and '$txtDateTo'
                AND dea.name_dea LIKE '%Blue Card%'
                AND dea.serial_dea NOT IN (5036,5037)";
        $result = $this->db->getAll($sql);
        if ($result){
            return $result;
        }
        else{
            return false;
        }
    }

    public function updateDocumentType($serialCustomer, $documentType){
        $sql = "UPDATE customer SET document_type_cus = '$documentType' WHERE serial_cus = $serialCustomer";
        $result = $this->db->Execute($sql);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function getInfoMessageCustomer($db, $cardNumberSal){
        $sql = "SELECT distinct( bbl.description_bbl) as 'beneficio', concat(cus.first_name_cus,' ',cus.last_name_cus ) as 'cliente', cus.cellphone_cus as 'celular' ,pbl.name_pbl as 'producto', bp.price_bxp as 'cobertura',
         date_format(s.begin_date_sal,'%Y-%m-%d') as 'inicio', date_format(s.end_date_sal,'%Y-%m-%d') as 'fin', pbl.serial_pro
                   FROM sales s
                   JOIN customer cus on s.serial_cus = cus.serial_cus
                  JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
                  JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc and pxc.serial_cou=62 and pxc.status_pxc= 'ACTIVE'
                  JOIN benefits_product bp ON bp.serial_pro = pxc.serial_pro AND bp.status_bxp = 'ACTIVE'
                  JOIN conditions c ON c.serial_con = bp.serial_con
                  JOIN conditions_by_language cbl ON cbl.serial_con = c.serial_con AND cbl.serial_lang = 2
                  JOIN product_by_language pbl ON pxc.serial_pro = pbl.serial_pro AND pbl.serial_lang = 2
                  LEFT JOIN restriction_type rt ON rt.serial_rst = bp.serial_rst AND rt.status_rst = 'ACTIVE'
                  LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = pbl.serial_lang
                  JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
                  JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang = 2
                  WHERE s.card_number_sal= $cardNumberSal and bbl.serial_bbl=2
                  ORDER BY b.weight_ben ";
    //echo $sql; die();
                $result = $db->getAll($sql);

    if($result){
      return $result;
    }else{
      return false;
    }   
    }

    ///GETTERS
    function getSerial_cus() {
        return $this->serial_cus;
    }

    function getSerial_cit() {
        return $this->serial_cit;
    }

    function getType_cus() {
        return $this->type_cus;
    }

    function getDocument_cus() {
        return $this->document_cus;
    }

    function getFirstname_cus() {
        return $this->first_name_cus;
    }

    function getLastname_cus() {
        return $this->last_name_cus;
    }

    function getBirthdate_cus() {
        return $this->birthdate_cus;
    }

    function getPhone1_cus() {
        return $this->phone1_cus;
    }

    function getPhone2_cus() {
        return $this->phone2_cus;
    }

    function getCellphone_cus() {
        return $this->cellphone_cus;
    }

    function getEmail_cus() {
        return $this->email_cus;
    }

    function getAddress_cus() {
        return ($this->address_cus);
    }

    function getRelative_cus() {
        return $this->relative_cus;
    }

    function getRelative_phone_cus() {
        return $this->relative_phone_cus;
    }

    function getVip_cus() {
        return $this->vip_cus;
    }

    function getStatus_cus() {
        return $this->status_cus;
    }

    function getPassword_cus() {
        return $this->password_cus;
    }

    function getMedical_background_cus() {
        return $this->medical_background_cus;
    }

    function getAuxData() {
        return $this->auxData;
    }

    function getFirst_access_cus() {
        return $this->first_access_cus;
    }

    function getSegmentation_cus() {
        return $this->segmentation_cus;
    }
    function getId_erp() {
        return $this->id_erp;
    }
    function getMigrate_cus() {
        return $this->migrate_cus;
    }
    function getGender_cus() {
        return $this->gender_cus;
    }
    function getDocumentType() {
	    return $this->document_type_cus;
    }


    ///SETTERS	
    function setserial_cus($serial_cus) {
        $this->serial_cus = $serial_cus;
    }

    function setSerial_cit($serial_cit) {
        $this->serial_cit = $serial_cit;
    }

    function setType_cus($type_cus) {
        $this->type_cus = $type_cus;
    }

    function setDocument_cus($document_cus) {
        $this->document_cus = $document_cus;
    }

    function setFirstname_cus($first_name_cus) {
        $this->first_name_cus = utf8_decode($first_name_cus);
    }

    function setLastname_cus($last_name_cus) {
        $this->last_name_cus = utf8_decode($last_name_cus);
    }

    function setBirthdate_cus($birthdate_cus) {
        $this->birthdate_cus = $birthdate_cus;
    }

    function setPhone1_cus($phone1_cus) {
        $this->phone1_cus = $phone1_cus;
    }

    function setPhone2_cus($phone2_cus) {
        $this->phone2_cus = $phone2_cus;
    }

    function setCellphone_cus($cellphone_cus) {
        $this->cellphone_cus = $cellphone_cus;
    }

    function setEmail_cus($email_cus) {
        $this->email_cus = utf8_decode($email_cus);
    }

    function setAddress_cus($address_cus) {
        $this->address_cus = addslashes(utf8_decode($address_cus));
    }

    function setRelative_cus($relative_cus) {
        $this->relative_cus = utf8_decode($relative_cus);
    }

    function setRelative_phone_cus($relative_phone_cus) {
        $this->relative_phone_cus = $relative_phone_cus;
    }

    function setVip_cus($vip_cus) {
        $this->vip_cus = $vip_cus;
    }

    function setStatus_cus($status_cus) {
        $this->status_cus = $status_cus;
    }

    function setPassword_cus($password_cus) {
        $this->password_cus = $password_cus;
    }

    function setMedical_background_cus($medical_background_cus) {
        $this->medical_background_cus = utf8_decode($medical_background_cus);
    }

    function setFirst_access_cus($first_access_cus) {
        return $this->first_access_cus = $first_access_cus;
    }
    function setSegmentation_cus($segmentation_cus) {
        $this->segmentation_cus = $segmentation_cus;
}
    function setId_erp($id_erp) {
        $this->id_erp = $id_erp;
    }
    function setMigrate_cus($migrate_cus) {
        $this->migrate_cus = $migrate_cus;
    }
    function setGender_cus($gender_cus) {
        $this->gender_cus = $gender_cus;
    }
    function setDocumentType($document_type_cus) {
	    $this->document_type_cus = $document_type_cus;
    }
}
?>
