<?php
/*
File: ObservationsByVisit.class
Author: David Bergmann
Creation Date: 12/03/2010
Last Modified:
Modified By:
*/

class ObservationsByVisit {
	var $db;
	var $serial_obv;
	var $serial_vis;
	var $host_obv;
        var $comments_obv;
	var $strategy_obv;

	function __construct($db, $serial_obv = NULL, $serial_vis = NULL, $host_obv = NULL,
                             $comments_obv = NULL, $strategy_obv = NULL){
		$this -> db = $db;
		$this -> serial_obv = $serial_obv;
		$this -> serial_vis = $serial_vis;
        $this -> host_obv = $host_obv;
		$this -> comments_obv = $comments_obv;
        $this -> strategy_obv = $strategy_obv;
	}

        /***********************************************
        * function getData
        * gets data by serial_obv
        ***********************************************/
	function getData(){
		if($this->serial_obv!=NULL){
			$sql = 	"SELECT serial_obv, serial_vis, host_obv, comments_obv, strategy_obv
					FROM observations_by_visit
					WHERE serial_zon='".$this->serial_obv."'";
			//echo $sql." ";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_obv=$result->fields[0];
				$this->serial_vis=$result->fields[1];
                                $this->host_obv=$result->fields[2];
				$this->comments_obv=$result->fields[3];
                                $this->strategy_obv=$result->fields[4];

				return true;
			}
			else
				return false;

		}else
			return false;
	}

        /***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
		$sql="INSERT INTO observations_by_visit (
					serial_obv,
                                        serial_vis,
                                        host_obv,
                                        comments_obv,
                                        strategy_obv
					)
			  VALUES(NULL,
					'".$this->serial_vis."',
                                        '".$this->host_obv."',
                                        '".$this->comments_obv."',
                                        '".$this->strategy_obv."'
					);";
                //echo $sql;
		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

        /***********************************************
        * function getData
        * gets data by serial_vis
        ***********************************************/
	function getDataByVisit(){
		if($this->serial_vis!=NULL){
			$sql = 	"SELECT serial_obv, serial_vis, host_obv, comments_obv, strategy_obv
					FROM observations_by_visit
					WHERE serial_vis='".$this->serial_vis."'";

			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$cont=0;
				do{
					$array[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
			return $array;
		}else
			return false;
	}

        ///GETTERS
	function getSerial_obv(){
		return $this->serial_obv;
	}
	function getSerial_vis(){
		return $this->serial_vis;
	}
        function getHost_obv(){
		return $this->host_obv;
	}
	function getComments_obv(){
		return $this->comments_obv;
	}
        function getStrategy_obv(){
		return $this->strategy_obv;
	}

	///SETTERS
	function setSerial_obv($serial_obv){
		$this->serial_obv = $serial_obv;
	}
	function setSerial_vis($serial_vis){
		$this->serial_vis = $serial_vis;
	}
        function setHost_obv($host_obv){
		$this->host_obv = $host_obv;
	}
	function setComments_obv($comments_obv){
		$this->comments_obv = $comments_obv;
	}
        function setStrategy_obv($strategy_obv){
		$this->strategy_obv = $strategy_obv;
	}
}
?>
