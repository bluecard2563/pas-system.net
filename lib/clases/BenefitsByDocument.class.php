<?php
/*
File: BenefitsByDocument.class
Author: David Bergmann
Creation Date: 04/06/2010
Last Modified: 22/06/2010
Modified By: David Bergmann
*/

class BenefitsByDocument {
    var $db;
    var $serial_spf;
    var $serial_dbf;
    var $amount_authorized_bbd;
	var $amount_presented_bbd;

    function __construct($db, $serial_spf = NULL, $serial_dbf = NULL, $amount_authorized_bbd = NULL, $amount_presented_bbd = NULL){
        $this -> db = $db;
        $this -> serial_spf = $serial_spf;
        $this -> serial_dbf = $serial_dbf;
        $this -> amount_authorized_bbd = $amount_authorized_bbd;
		$this -> amount_presented_bbd = $amount_presented_bbd;

    }

    /***********************************************
    * function getData
    * gets data by serial_zon
    ***********************************************/
    function getData(){
        if($this->serial_dbf!=NULL){
            $sql = "SELECT serial_spf, serial_dbf, amount_authorized_bbd, amount_presented_bbd
                    FROM benefits_by_document
                    WHERE serial_dbf='".$this->serial_dbf."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_spf=$result->fields[0];
                $this->serial_dbf=$result->fields[1];
                $this->amount_authorized_bbd==$result->fields[2];
				$this->amount_presented_bbd==$result->fields[3];
                return true;
            }
            else
                return false;

        }else
            return false;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert(){
        $sql=   "INSERT INTO benefits_by_document (
                                    serial_spf,
                                    serial_dbf,
                                    amount_authorized_bbd,
									amount_presented_bbd
                                    )
                 VALUES('$this->serial_spf',
                        '$this->serial_dbf',
                        '$this->amount_authorized_bbd',
						'$this->amount_presented_bbd');";
        
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/***********************************************
    * function update
    * updates a register in DB
    ***********************************************/
    function update(){
        $sql=  "UPDATE benefits_by_document
				SET amount_authorized_bbd = $this->amount_authorized_bbd,
					amount_presented_bbd = $this->amount_presented_bbd
                WHERE serial_spf = $this->serial_spf
				AND serial_dbf = $this->serial_dbf;";
				//die($sql);
        $result = $this->db->Execute($sql);

    	if($result){
            return true;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * function getBenefitsByDocuments
    * gets all the benefits included into a document
    ***********************************************/
    public static function getBenefitsByDocuments($db, $serial_dbf){
        $sql = "SELECT spf.serial_spf, bbd.amount_authorized_bbd, bbd.amount_presented_bbd,
					   bbl.serial_ben, bbl.description_bbl, bxp.price_bxp, bxp.restriction_price_bxp
				FROM benefits_by_document bbd
                JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf
                JOIN benefits_product bxp ON bxp.serial_bxp = spf.serial_bxp
                JOIN benefits_by_language bbl ON bbl.serial_ben = bxp.serial_ben AND bbl.serial_lang = '".$_SESSION['serial_lang']."'
                WHERE bbd.serial_dbf=".$serial_dbf;
		
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * function getServicesByDocuments
    * gets all the services included into a document
    ***********************************************/
    public static function getServicesByDocuments($db, $serial_dbf){
        $sql=   "SELECT spf.serial_spf, bbd.amount_authorized_bbd, bbd.amount_presented_bbd,
						sbl.serial_ser, sbl.name_sbl, sbc.coverage_sbc
                 FROM benefits_by_document bbd
                 JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf
                 JOIN services_by_country sbc ON sbc.serial_sbc = spf.serial_sbc
                 JOIN services_by_language sbl ON sbl.serial_ser = sbc.serial_ser and sbl.serial_lang = ".$_SESSION['serial_lang']."
                 WHERE serial_dbf=".$serial_dbf;
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /******************************************************
    * function getBenefitsByPaymentRequest
    * gets all the benefits included into a payment request
    *******************************************************/
    public static function getBenefitsByPaymentRequest($db, $serial_prq){
        $sql=   "SELECT sum(bbd.amount_authorized_bbd) as amount_authorized_bbd, bbl.serial_ben, bbl.description_bbl, bxp.price_bxp,
						bxp.restriction_price_bxp, spf.estimated_amount_spf, con.alias_con, sum(bbd.amount_presented_bbd) AS 'amount_presented_bbd'
                 FROM benefits_by_document bbd
                 JOIN payment_request prq ON prq.serial_dbf = bbd.serial_dbf
                 JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf
                 JOIN benefits_product bxp ON bxp.serial_bxp = spf.serial_bxp
                 JOIN benefits_by_language bbl ON bbl.serial_ben = bxp.serial_ben AND bbl.serial_lang = '".$_SESSION['serial_lang']."'
				 JOIN conditions con ON con.serial_con = bxp.serial_con
                 WHERE prq.serial_prq IN (".$serial_prq.")
                 GROUP BY bbl.description_bbl";
		//ErrorLog::log($db, 'liquidaciones antes de pdf', $sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /******************************************************
    * function getServicesByPaymentRequest
    * gets all the benefits included into a payment request
    *******************************************************/
    public static function getServicesByPaymentRequest($db, $serial_prq){
        $sql=   "SELECT sum(bbd.amount_authorized_bbd) as amount_authorized_bbd, sbc.serial_ser, sbl.name_sbl, spf.estimated_amount_spf,
						sbc.coverage_sbc, bbd.amount_presented_bbd
                 FROM benefits_by_document bbd
                 JOIN payment_request prq ON prq.serial_dbf = bbd.serial_dbf
                 JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf
                 JOIN services_by_country sbc ON sbc.serial_sbc = spf.serial_sbc
                 JOIN services_by_language sbl ON sbl.serial_ser = sbc.serial_ser and sbl.serial_lang = ".$_SESSION['serial_lang']."
                 WHERE prq.serial_prq IN (".$serial_prq.")
                 GROUP BY sbl.name_sbl";
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    ///GETTERS
    function getSerial_serial_spf(){
        return $this->serial_spf;
    }
    function getSerial_dbf(){
        return $this->serial_dbf;
    }
    function getAmount_authorized_bbd(){
        return $this->amount_authorized_bbd;
    }
	function getAmount_presented_bbd(){
        return $this->amount_presented_bbd;
    }

    ///SETTERS
    function setSerial_spf($serial_spf){
        $this->serial_spf = $serial_spf;
    }
    function setSerial_dbf($serial_dbf){
        $this->serial_dbf = $serial_dbf;
    }
    function setAmount_authorized_bbd($amount_authorized_bbd){
        $this->amount_authorized_bbd = $amount_authorized_bbd;
    }
	function setAmount_presented_bbd($amount_presented_bbd){
        $this->amount_presented_bbd = $amount_presented_bbd;
    }
}
?>
