<?php
/*
File: Refund.class.php
Author: Patricio Astudillo
Creation Date: 14/04/2010 09:18
Last Modified: 14/04/2010
Modified By: Patricio Astudillo
*/
class Refund{
	var $db;
	var $serial_ref;
	var $serial_sal;
	var $serial_usr;
	var $status_ref;
	var $total_to_pay_ref;
	var $request_date_ref;
	var $auth_date_ref;
    var $comments_ref;
	var $type_ref;
	var $discount_ref;
	var $document_ref;
	var $reason_ref;
	var $retained_type_ref;
	var $retained_value_ref;
	var $other_discount_ref;
	var $aux;

	function __construct($db, $serial_ref=NULL, $serial_sal=NULL, $serial_usr=NULL, $status_ref=NULL, $total_to_pay_ref=NULL, $request_date_ref=NULL,
                            $type_ref=NULL, $retained_type_ref=NULL, $retained_value_ref=NULL, $discount_ref=NULL, $document_ref=NULL,
							$reason_ref=NULL, $other_discount_ref=NULL){

		$this -> db = $db;
		$this -> serial_ref=$serial_ref;
		$this -> serial_sal=$serial_sal;
		$this -> serial_usr=$serial_usr;
		$this -> status_ref=$status_ref;
		$this -> total_to_pay_ref=$total_to_pay_ref;
		$this -> request_date_ref=$request_date_ref;
		$this -> type_ref=$type_ref;
		$this -> retained_type_ref=$retained_type_ref;
		$this -> retained_value_ref=$retained_value_ref;
		$this -> discount_ref=$discount_ref;
		$this -> document_ref=$document_ref;
		$this -> reason_ref=$reason_ref;
		$this -> other_discount_ref=$other_discount_ref;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a Promo object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_ref!=NULL){
			$sql = 	"SELECT r.serial_ref,
							r.serial_sal,
							r.status_ref,
							r.total_to_pay_ref,
							r.request_date_ref,
							r.auth_date_ref,
							r.comments_ref,
							r.type_ref,
							r.retained_type_ref,
							r.retained_value_ref,
							r.discount_ref,
							r.document_ref,
							r.reason_ref,
							r.other_discount_ref,
							r.serial_usr,
							CONCAT(u.first_name_usr,' ',u.last_name_usr),
							s.serial_cus,
							s.serial_inv
					FROM refund r
					JOIN user u ON u.serial_usr=r.serial_usr
					JOIN sales s ON s.serial_sal=r.serial_sal
					JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
					JOIN dealer d ON d.serial_dea=pbd.serial_dea
					JOIN sector sec ON sec.serial_sec=d.serial_sec
					JOIN city cit ON cit.serial_cit=sec.serial_cit
					JOIN country cou ON cou.serial_cou=cit.serial_cou
					WHERE r.serial_ref='".$this->serial_ref."'";
                                //echo $sql.'<br>';
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ref=$result->fields[0];
				$this -> serial_sal=$result->fields[1];
				$this -> status_ref=$result->fields[2];
                $this -> total_to_pay_ref=$result->fields[3];
                $this -> request_date_ref=$result->fields[4];
                $this -> auth_date_ref=$result->fields[5];
                $this -> comments_ref=$result->fields[6];
				$this -> type_ref=$result->fields[7];
				$this -> retained_type_ref=$result->fields[8];
				$this -> retained_value_ref=$result->fields[9];
				$this -> discount_ref=$result->fields[10];
				$this -> document_ref=$result->fields[11];
				$this -> reason_ref=$result->fields[12];
				$this -> other_discount_ref=$result->fields[13];
				$this -> serial_usr=$result->fields[14];
				$this -> aux['applicant']=$result->fields[15];
				$this -> aux['serial_cus']=$result->fields[16];
				$this -> aux['serial_inv']=$result->fields[17];

				return true;
			}else
				return false;
		}else
			return false;
	}

	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO refund (
                    serial_ref,
					serial_sal,
					serial_usr,
					status_ref,
					total_to_pay_ref,
					request_date_ref,
					auth_date_ref,
					comments_ref,
					type_ref,
					retained_type_ref,
					retained_value_ref,
					discount_ref,
					document_ref,
					reason_ref,
					other_discount_ref)
                VALUES (
                    NULL,
                    '".$this->serial_sal."',
					'".$this->serial_usr."',
                    '".$this->status_ref."',
                    '".$this->total_to_pay_ref."',
                    CURRENT_TIMESTAMP(),
                    NULL,
                    '".$this->comments_ref."',
                    '".$this->type_ref."',
                    '".$this->retained_type_ref."',
                    '".$this->retained_value_ref."',
                    '".$this->discount_ref."',
                    '".$this->document_ref."',
					'".$this->reason_ref."',
					'".$this->other_discount_ref."')";

            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "UPDATE refund SET
                    serial_sal='".$this->serial_sal."',
					serial_usr='".$this->serial_usr."',
                    status_ref='".$this->status_ref."',
                    total_to_pay_ref='".$this->total_to_pay_ref."',
                    request_date_ref='".$this->request_date_ref."',
                    auth_date_ref= CURRENT_TIMESTAMP(),
                    comments_ref='".$this->comments_ref."',
                    type_ref='".$this->type_ref."',
                    retained_type_ref='".$this->retained_type_ref."',
                    retained_value_ref='".$this->retained_value_ref."',
                    discount_ref='".$this->discount_ref."',
                    document_ref='".$this->document_ref."',
					reason_ref='".$this->reason_ref."',
					other_discount_ref='".$this->other_discount_ref."'
            WHERE serial_ref = '".$this->serial_ref."'";
            //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }

	/**
	* @Name: getPendingApplications
	* @Description: Retrieves all the pending refund applications in the system.
	* @Params: $db: Data Base conection.
	* @Returns: Pending applications array
	**/
	public static function getPendingApplications($db){
		$sql = 	"SELECT r.*, cou.name_cou, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'applicant', s.card_number_sal, d.name_dea
				 FROM refund r
				 JOIN alerts a ON r.serial_ref=a.remote_id 
				 		AND a.type_alt = 'REFUND' AND a.status_alt = 'PENDING' AND a.serial_usr = {$_SESSION['serial_usr']}
				 JOIN user u ON u.serial_usr=r.serial_usr
				 JOIN sales s ON s.serial_sal=r.serial_sal
				 JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				 JOIN dealer d ON d.serial_dea=pbd.serial_dea
				 JOIN sector sec ON sec.serial_sec=d.serial_sec
				 JOIN city cit ON cit.serial_cit=sec.serial_cit
				 JOIN country cou ON cou.serial_cou=cit.serial_cou
				 WHERE r.status_ref = 'STAND-BY'";

		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/**
	* @Name: hasRefundApplications
	* @Description: Verifies if a sale has any refund applications pending or approved.
	* @Params: $db: Data Base conection.
	*		   $serial_sal: Sale ID
	* @Returns: Type Sale array
	**/
	public static function hasRefundApplications($db, $serial_sal){
		$sql = 	"SELECT serial_ref
				 FROM refund r
				 WHERE r.serial_sal = '".$serial_sal."'
				 AND (r.status_ref = 'APROVED'
				      OR r.status_ref = 'STAND-BY')";
        //die($sql);
			$result = $db->Execute($sql);

			if($result->fields[0]){
				return true;
			}else
				return false;
	}

	/**
	@Name: getRetainedType
	@Description: Gets all retained type of a refund in DB.
	@Params: N/A
	@Returns: Type Sale array
	**/
	public static function getRetainedType($db){
		$sql = "SHOW COLUMNS FROM refund LIKE 'retained_type_ref'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	@Name: getTypeRefund
	@Description: Gets all refuns types in DB.
	@Params: N/A
	@Returns: Type Sale array
	**/
	public static function getTypeRefund($db){
		$sql = "SHOW COLUMNS FROM refund LIKE 'type_ref'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

        /**
	@Name: getRefundStatus
	@Description: Gets all Refund status in DB.
	@Params: N/A
	@Returns: Repeat Sale array
	**/
	public static function getRefundStatus($db){
		$sql = "SHOW COLUMNS FROM refund LIKE 'status_ref'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	@Name: getRefundReason
	@Description: Gets all Refund status in DB.
	@Params: N/A
	@Returns: Repeat Sale array
	**/
	public static function getRefundReason($db){
		$sql = "SHOW COLUMNS FROM refund LIKE 'reason_ref'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/***********************************************
    * function getRefundsByManager
    * gets information about refunds
    ***********************************************/
	function getRefundsByManager($serial_cou,$serial_mbc,$serial_mbc_user, $dea_serial_dea_user,$status_ref=NULL,$serial_cit=NULL,
									$serial_usr=NULL,$serial_dea=NULL,$serial_bra=NULL,$dateFrom=NULL,$dateTo=NULL){

		$citySQL =	''; $userSQL = ''; $dealerSQL = ''; $branchSQL = ''; $datesSQL = ''; $statusSQL = ''; $usermbcSQL = '';

		if($serial_cit){
			$citySQL = "AND cit.serial_cit=".$serial_cit;
		}
		if($serial_usr){
			$userSQL = "AND usr.serial_usr=".$serial_usr;
		}
		if($serial_dea){
			$dealerSQL = " AND dea.serial_dea = ".$serial_dea;
		}
		if($serial_bra){
			$branchSQL = " AND br.serial_dea = ".$serial_bra;
		}
		if($dateFrom && $dateTo){
			$datesSQL = " WHERE slg.date_slg BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND ADDDATE(STR_TO_DATE('$dateTo', '%d/%m/%Y'),1)";
		}
		if($status_ref){
			$statusSQL = " AND ref.status_ref = '".$status_ref."'";
		}
		if($_SESSION['serial_mbc']!='1'){
			$usermbcSQL =" AND mbc.serial_mbc=".$_SESSION['serial_mbc'];
		}
		$sql = "SELECT DISTINCT(ref.serial_ref), DATE_FORMAT(ref.request_date_ref,'%d/%m/%Y') AS request_date_ref, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) AS name_cus,
						dea.name_dea, br.name_dea AS name_bra, s.card_number_sal, IFNULL(cte.number_cn,'N/A') AS number_cn, s.total_sal, ref.retained_value_ref,
						ref.retained_type_ref, ref.total_to_pay_ref, DATE_FORMAT(ref.auth_date_ref,'%d/%m/%Y') AS auth_date_ref,
						cou.name_cou, cit.name_cit, man.name_man, ref.status_ref, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS name_usr, dea.serial_dea
				 FROM sales s
				JOIN sales_log slg ON slg.serial_sal = s.serial_sal AND slg.type_slg = 'REFUNDED'
				JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				JOIN dealer br ON cnt.serial_dea=br.serial_dea $branchSQL AND br.status_dea = 'ACTIVE'
				JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealerSQL AND dea.status_dea = 'ACTIVE'
				JOIN sector sec ON br.serial_sec=sec.serial_sec
				JOIN city cit ON sec.serial_cit=cit.serial_cit $citySQL
				JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND s.in_date_sal BETWEEN ubd.begin_date_ubd AND DATE_ADD(IFNULL(ubd.end_date_ubd, NOW()), INTERVAL 1 DAY)
				JOIN user usr ON usr.serial_usr=ubd.serial_usr $userSQL
				JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
				JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc AND mbc.serial_mbc=".$serial_mbc." $usermbcSQL
				JOIN manager man ON man.serial_man=mbc.serial_man
				JOIN refund ref ON ref.serial_sal=s.serial_sal $statusSQL
				JOIN credit_note cte ON cte.serial_ref=ref.serial_ref
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				$datesSQL";
		
		if($serial_mbc_user!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc_user;
		}
		if($dea_serial_dea_user){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea_user;
		}
		$sql.=" ORDER BY dea.name_dea, name_bra";
		//die(Debug::print_r($sql));
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/***********************************************
    * function getRefundsByCustomer
    * gets information about refunds
    ***********************************************/
	function getRefundsByCustomer($serial_cus, $serial_mbc, $dea_serial_dea){
		
		$sql = "SELECT DISTINCT(ref.serial_ref), DATE_FORMAT(ref.request_date_ref,'%d/%m/%Y') AS request_date_ref, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) AS name_cus,
						dea.name_dea, br.name_dea AS name_bra, s.card_number_sal, IFNULL(cte.number_cn,'N/A') AS number_cn, s.total_sal, ref.retained_value_ref,
						ref.retained_type_ref, ref.total_to_pay_ref, DATE_FORMAT(ref.auth_date_ref,'%d/%m/%Y') AS auth_date_ref,
						cou.name_cou, cit.name_cit,  ref.status_ref, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS name_usr, dea.serial_dea
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
				   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
				   JOIN sector sec ON br.serial_sec=sec.serial_sec
				   JOIN city cit ON sec.serial_cit=cit.serial_cit 
				   JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND ubd.status_ubd='ACTIVE'
				   JOIN user usr ON usr.serial_usr=ubd.serial_usr
				   JOIN country cou ON cit.serial_cou=cou.serial_cou
				   JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc 
				   JOIN manager man ON man.serial_man=mbc.serial_man
				   JOIN refund ref ON ref.serial_sal=s.serial_sal
				   JOIN credit_note cte ON cte.serial_ref=ref.serial_ref
				   JOIN customer cus ON cus.serial_cus=s.serial_cus AND cus.serial_cus=".$serial_cus."	
			";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY dea.name_dea, name_bra";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}
	
	/**
	 * @name getRefundsWithNoCNotes
	 * @param type $db
	 * @return boolean 
	 */
	public static function getRefundsWithNoCNotes($db){
		$sql = "SELECT serial_ref, reason_ref, s.card_number_sal, 
						IF(i.serial_cus IS NULL, 
							d.name_dea, 
							CONCAT(IFNULL(c.first_name_cus, ''), ' ', IFNULL(c.last_name_cus, ''))
						) AS 'holder'
				FROM refund r
				JOIN sales s ON s.serial_sal = r.serial_sal
				JOIN invoice i ON i.serial_inv = s.serial_inv
				LEFT JOIN customer c ON c.serial_cus = i.serial_cus
				LEFT JOIN dealer d ON d.serial_dea = i.serial_dea
				WHERE serial_ref NOT IN (SELECT DISTINCT serial_ref
										FROM credit_note
										WHERE serial_ref IS NOT NULL)
				AND status_ref = 'APROVED'";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	//Getters
	function getSerial_ref(){
		return $this->serial_ref;
	}
	function getSerial_sal(){
		return $this->serial_sal;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getStatus_ref(){
		return $this->status_ref;
	}
    function getTotalToPay_ref(){
		return $this->total_to_pay_ref;
	}
	function getRequestDate_ref(){
		return $this->request_date_ref;
	}
	function getAuthDate_ref(){
		return $this->auth_date_ref;
	}
    function getComments_ref(){
		return $this->comments_ref;
	}
	function getType_ref(){
		return $this->type_ref;
	}
	function getRetainedType_ref(){
		return $this->retained_type_ref;
	}
	function getRetainedValue_ref(){
		return $this->retained_value_ref;
	}
	function getDiscount_ref(){
		return $this->discount_ref;
	}
	function getDocument_ref(){
		return $this->document_ref;
	}
	function getReason_ref(){
		return $this->reason_ref;
	}
	function getOtherDiscount_ref(){
		return $this->other_discount_ref;
	}


	//Setters
	function setSerial_ref($serial_ref){
		$this->serial_ref=$serial_ref;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal=$serial_sal;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr=$serial_usr;
	}
    function setTotalToPay_ref($total_to_pay_ref){
		$this->total_to_pay_ref=$total_to_pay_ref;
	}
	function setStatus_ref($status_ref){
		$this->status_ref=$status_ref;
	}
	function setRequestDate_ref($request_date_ref){
		$this->request_date_ref=$request_date_ref;
	}
	function setAuthDate_ref($auth_date_ref){
		$this->auth_date_ref=$auth_date_ref;
	}
    function setComments_ref($comments_ref){
		$this->comments_ref=$comments_ref;
	}
	function setType_ref($type_ref){
		$this->type_ref=$type_ref;
	}
	function setRetainType_ref($admin_fee_ref){
		$this->retained_type_ref=$admin_fee_ref;
	}
	function setRetainValue_ref($direct_sale_percentage_ref){
		$this->retained_value_ref=$direct_sale_percentage_ref;
	}
	function setDiscount_ref($discount_ref){
		$this->discount_ref=$discount_ref;
	}
	function setDocument_ref($document_ref){
		$this->document_ref=$document_ref;
    }
	function setReason_ref($reason_ref){
		$this->reason_ref=$reason_ref;
    }
	function setOtherDiscount_ref($other_discount_ref){
		$this->other_discount_ref=$other_discount_ref;
	}
}
?>
