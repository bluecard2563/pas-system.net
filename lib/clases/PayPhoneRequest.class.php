<?php

class PayPhoneRequest{
	//Transaction request model
    private $amount;
    private $amountWithTax;
    private $amountWithOutTax;
    private $purchaseLanguage;
    private $tax;
    private $timeZone;
    private $latitude;
    private $longitude;
    private $clientTransactionId;
    private $clientUserId;
	var $validationError;
	
	function __construct() {
		$this->validationError = NULL;
	}
	
	public function setAmount($amount){
		if (is_numeric($amount)){
			$this->amount = $amount;
		} else {
			$this->validationError['amount'] = 'InvalidFormat';
		}
	}
	
	public function setAmountWithTax($amountWithTax){
		if (is_numeric($amountWithTax)){
			$this->amountWithTax = $amountWithTax;
		} else {
			$this->validationError['amountWithTax'] = 'InvalidFormat';
		}
	}
	
	public function setAmountWithOutTax($amountWithOutTax){
		if (is_numeric($amountWithOutTax)){
			$this->amountWithOutTax = $amountWithOutTax;
		} else {
			$this->validationError['amountWithOutTax'] = 'InvalidFormat';
		}
	}
	
	public function setPurchaseLanguage($purchaseLanguage){
		if (!isset($purchaseLanguage) || ctype_alnum($purchaseLanguage)){
			$this->purchaseLanguage = $purchaseLanguage;
		} else {
			$this->validationError['purchaseLanguage'] = 'InvalidFormat';
		}
	}
	
	public function setTax($tax){
		if (is_numeric($tax)){
			$this->tax = $tax;
		} else {
			$this->validationError['tax'] = 'InvalidFormat';
		}
	}
	
	public function setTimeZone($timeZone){
		if (is_int($timeZone)){
			$this->timeZone = $timeZone;
		} else {
			$this->validationError['timeZone'] = 'InvalidFormat';
		}
	}
	
	public function setLatitude($latitude){
		if (!isset($latitude) || is_numeric($latitude)){
			$this->latitude = $latitude;
		} else {
			$this->validationError['latitude'] = 'InvalidFormat';
		}
	}
	
	public function setLongitude($longitude){
		if (!isset($longitude) || is_numeric($longitude)){
			$this->longitude = $longitude;
		} else {
			$this->validationError['longitude'] = 'InvalidFormat';
		}
	}
	
	public function setClientTransactionId($clientTransactionId){
		if (isset($clientTransactionId)){
			$this->clientTransactionId = $clientTransactionId;
		}
	}
	
	public function setClientUserId($clientUserId){
		if (!isset($clientUserId) || ctype_alnum($clientUserId)){
			$this->clientUserId = $clientUserId;
		} else {
			$this->validationError['clientUserId'] = 'InvalidFormat';
		}
	}
	
	public function getTransactionRequestModel(){
		$transactionReqModel['msg'] = 'error';
		if (!isset($this->validationError)){
			$transactionReqModel['msg'] = 'success';
			$transactionReqModel['result'] = array(
				"amount" => $this->amount,
				"amountWithTax" => $this->amountWithTax,
				"amountWithOutTax" => $this->amountWithOutTax,
				"purchaseLanguage" => $this->purchaseLanguage,
				"tax" => $this->tax,
				"timeZone" => $this->timeZone,
				"latitude" => $this->latitude,
				"longitude" => $this->longitude,
				"clientTransactionId" => $this->clientTransactionId,
				"clientUserId" => $this->clientUserId
			);
		} else {
			$transactionReqModel['result'] = $this->validationError;
		}
		
		return $transactionReqModel;
	}
	
}
