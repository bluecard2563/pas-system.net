<?php
/*
File: TaxesByProduct.class.php
Author: Edwin Salvador.
Creation Date: 18/02/2010
Last Modified: 07/05/2010
Modified By: Santiago Benitez
*/

class TaxesByProduct{
    var $db;
    var $serial_pxc;
    var $serial_tax;
	
    function __construct($db, $serial_pxc=NULL, $serial_tax=NULL){
        $this -> db = $db;
        $this -> serial_pxc = $serial_pxc;
        $this -> serial_tax = $serial_tax;
    }
	
    /***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
    function getData(){
        if($this->serial_pxc!=NULL){
            $sql = "SELECT serial_pxc,
                           serial_tax
                    FROM taxes_by_product
                    WHERE serial_pxc='".$this->serial_pxc."'
                    AND serial_tax='".$this->serial_tax."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_pxc =$result->fields[0];
                $this -> serial_tax = $result->fields[1];

                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }
	
    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO taxes_by_product (
				serial_pxc,
				serial_tax
                                )
            VALUES (
                    '".$this->serial_pxc."',
                    '".$this->serial_tax."'
                    )";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return TRUE;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: delete
    @Description: delete a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function delete(){
        if($this->serial_pxc != NULL){
            $sql="DELETE FROM taxes_by_product
                  WHERE serial_pxc='".$this->serial_pxc."'";

            $result = $this->db->Execute($sql);
            //echo $sql." ";
            if ($result==true)
                return true;
            else
                return false;
        }
    }

    function taxByProductExists(){
        if($this->serial_tax != NULL && $this->serial_pxc != NULL){
            $sql = "SELECT 1
                    FROM taxes_by_product tbp
                    WHERE tbp.serial_tax = '".$this->serial_tax."'
                    AND tbp.serial_pxc = '".$this->serial_pxc."'";

            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }
    /***********************************************
    * funcion getTaxesByProduct
    * gets the Taxes that are applied in an specific product
    ***********************************************/
    function getTaxesByProduct(){
            $sql = 	"SELECT tax.serial_tax, tax.name_tax, percentage_tax
                             FROM taxes_by_product tbp
                             JOIN tax_by_country tax ON tbp.serial_tax=tax.serial_tax AND status_tax='ACTIVE'
                     WHERE tbp.serial_pxc = '".$this->serial_pxc."'
                     ORDER BY tax.serial_tax";
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                    $arr=array();
                    $cont=0;

                    do{
                            $arr[$cont]=$result->GetRowAssoc(false);
                            $result->MoveNext();
                            $cont++;
                    }while($cont<$num);
            }

            return $arr;
    }

    ///GETTERS
    function getSerial_pxc(){
        return $this->serial_pxc;
    }
    function getSerial_tax(){
        return $this->serial_tax;
    }

    ///SETTERS
    function setSerial_pxc($serial_pxc){
        $this->serial_pxc = $serial_pxc;
    }
    function setSerial_tax($serial_tax){
        $this->serial_tax = $serial_tax;
    }
}
?>