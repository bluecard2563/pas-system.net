<?php
/*
File: Language.class.php
Author: Santiago Benítez
Creation Date: 20/01/2010 11:50
Last Modified: 23/02/2010
Modified By: Santiago Borja
*/

class Language{				
	var $db;
	var $serial_lang;
	var $name_lang;
	var $code_lang;
	
	function __construct($db, $serial_lang=NULL, $name_lang=NULL, $code_lang=NULL ){
		$this -> db = $db;
		$this -> serial_lang = $serial_lang;
		$this -> name_lang = $name_lang;
		$this -> code_lang = $code_lang;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_lang
    ***********************************************/
	function getData(){
		if($this->serial_lang!=NULL){
			$sql = 	"SELECT lang.serial_lang, lang.name_lang, lang.code_lang
					FROM language lang
					WHERE lang.serial_lang='".$this->serial_lang."'";
			//die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_lang =$result->fields[0];
				$this -> name_lang = $result->fields[1];
				$this -> code_lang = $result->fields[2];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * function getDataByCode
    * sets data by serial_lang
    ***********************************************/
	function getDataByCode($code_lang){
		if($code_lang!=NULL){
			$sql = 	"SELECT lang.serial_lang, lang.name_lang, lang.code_lang
					FROM language lang
					WHERE lang.code_lang='".$code_lang."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_lang =$result->fields[0];
				$this -> name_lang = $result->fields[1];
				$this -> code_lang = $result->fields[2];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * function getDataByCode
    * sets data by serial_lang
    ***********************************************/
	public static function getSerialLangByCode($db,$code_lang){
		$sql = 	"SELECT serial_lang
				FROM language
				WHERE code_lang='".$code_lang."'";
        
		$result = $db->Execute($sql);
		if ($result === false) return false;
		return $result->fields[0];
	}
	
	
	public static function getCodeLangBySerial($db,$serial_lang){
		$sql = 	"SELECT code_lang
				FROM language
				WHERE serial_lang='".$serial_lang."'";
		$result = $db->Execute($sql);
		if ($result === false) return false;
		return $result->fields[0];
	}
	
	
	
	/***********************************************
    * funcion languageExists
    * verifies if a Language exists or not
    ***********************************************/
	function languageExists($name_lang=NULL, $serial_lang=NULL){
		$sql = 	"SELECT serial_lang
				 FROM language
				 WHERE LOWER(name_lang) = _utf8'".utf8_encode($name_lang)."' collate utf8_bin";
		if($serial_lang!=NULL){
			$sql.=" AND serial_lang <> ".$serial_lang;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
    * funcion codeExists
    * verifies if a Language exists or not
    ***********************************************/
	function codeExists($code_lang, $serial_lang=NULL){
		$sql = 	"SELECT serial_lang
				 FROM language
				 WHERE LOWER(code_lang)= _utf8'".utf8_encode($code_lang)."' collate utf8_bin";
		if($serial_lang!=NULL){
			$sql.=" AND serial_lang <> ".$serial_lang;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
        * funcion getLanguages
        * gets all the languages from the system
        ***********************************************/
	public static function getAllLanguages($db, $serial_lang = NULL){
		if($serial_lang) $exclusion_item = " WHERE serial_lang <> $serial_lang";
		
		$sql = "SELECT serial_lang, name_lang, code_lang
				FROM language 
				$exclusion_item
				ORDER BY name_lang asc";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr = array();
			$cont = 0;
			do{
				$arr[$cont++] = $result -> GetRowAssoc(false);
				$result -> MoveNext();
			}while($cont < $num);
		}
		return $arr;
	}
	
	/***********************************************
        * funcion getRemainingQuestionLanguages
        * gets all the languages that are not used 
		* yet in the translations from the system
    ***********************************************/
	function getRemainingQuestionLanguages($serial_que){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
				FROM language lang, questions_by_language qbl
				WHERE lang.serial_lang <> 
				ALL(SELECT qb.serial_lang 
					FROM questions_by_language qb
					WHERE qb.serial_que = ".$serial_que.")				
				ORDER BY lang.name_lang asc";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
        * funcion getRemainingDealerTypeLanguages
        * gets all the languages that are not used 
		* yet in the translations from the system
    ***********************************************/
	function getRemainingDealerTypeLanguages($serial_dlt){
		$sql = 	"SELECT lang.serial_lang, lang.name_lang
				FROM language lang
				WHERE NOT EXISTS (SELECT dt.serial_lang
					FROM dealer_ty_by_language dt
					WHERE dt.serial_dlt = ".$serial_dlt."
					AND lang.serial_lang=dt.serial_lang)				
				ORDER BY lang.name_lang asc";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}	
		return $arr;
	}

    /**
	@Name: getRemainingRestrictionLanguages
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
        function getRemainingRestrictionLanguages($serial_rst){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
                        FROM language lang
                        WHERE lang.serial_lang <> ALL(SELECT serial_lang
                                                      FROM restriction_by_language
                                                      WHERE serial_rst = ".$serial_rst.")
                        ORDER BY lang.name_lang asc";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

 /**
	@Name: getRemainingConditionLanguages
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
        function getRemainingConditionLanguages($serial_con){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
                        FROM language lang
                        WHERE lang.serial_lang <> ALL(SELECT serial_lang
                                                      FROM conditions_by_language
                                                      WHERE serial_con = ".$serial_con.")
                        ORDER BY lang.name_lang asc";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}
	
    /**
	@Name: getRemainingBenefitLanguages
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
        function getRemainingBenefitLanguages($serial_ben){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
                        FROM language lang
                        WHERE lang.serial_lang <> ALL(SELECT serial_lang
                                                      FROM benefits_by_language
                                                      WHERE serial_ben = ".$serial_ben.")
                        ORDER BY lang.name_lang asc";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}


    /**
	@Name: getRemainingBenefitLanguages
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
    function getRemainingProductTypeLanguages($serial_tpp){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
                        FROM language lang
                        WHERE lang.serial_lang NOT IN(SELECT serial_lang
                                                      FROM ptype_by_language
                                                      WHERE serial_tpp = ".$serial_tpp.")
                        ORDER BY lang.name_lang asc";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

	/**
	@Name: getRemainingServiceLanguages
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
    function getRemainingServiceLanguages($serial_ser){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
				 FROM language lang
				 WHERE lang.serial_lang NOT IN(SELECT serial_lang
											  FROM services_by_language
											  WHERE serial_ser = ".$serial_ser.")
				 ORDER BY lang.name_lang asc";

		$result = $this->db -> getAll($sql);

		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

          /**
	@Name: getRemainingBenefitLanguages
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
    function getRemainingProductLanguages($serial_pro){
		$sql = 	"SELECT DISTINCT lang.serial_lang, lang.name_lang
                        FROM language lang
                        WHERE lang.serial_lang NOT IN(SELECT serial_lang
                                                      FROM product_by_language
                                                      WHERE serial_pro = ".$serial_pro.")
                        ORDER BY lang.name_lang asc";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

	/** 
	@Name: getLanguagesAutocompleter
	@Description: Returns an array of languages.
	@Params: Language name or pattern.
	@Returns: languages array
	**/
	function getLanguagesAutocompleter($name_rst){
		$sql = "SELECT serial_lang, name_lang
				FROM language
				WHERE LOWER(name_lang)  LIKE _utf8'%".utf8_encode($name_rst)."%' collate utf8_bin
				LIMIT 10";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO language (
				serial_lang,
				name_lang,
				code_lang)
            VALUES (
                NULL,
				'".$this->name_lang."',
				'".$this->code_lang."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE language SET
					name_lang ='".$this->name_lang."',
					code_lang ='".$this->code_lang."'
				WHERE serial_lang = '".$this->serial_lang."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }

	///GETTERS
	function getSerial_lang(){
		return $this->serial_lang;
	}
	function getName_lang(){
		return $this->name_lang;
	}
	function getCode_lang(){
		return $this->code_lang;
	}
	

	///SETTERS	
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setName_lang($name_lang){
		$this->name_lang = $name_lang;
	}
	function setCode_lang($code_lang){
		$this->code_lang = $code_lang;
	}
}
?>