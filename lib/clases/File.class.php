<?php
/*
File: File.class
Author: David Bergmann
Creation Date:31/03/2010
Last Modified: 04/08/2016
Modified By: David Rosales
*/

class File {
	var $db;
	var $serial_fle;
    var $serial_cit;
	var $opening_usr;
	var $serial_trl;
    var $serial_cus;
	var $closing_usr;
	var $serial_sal;
	var $diagnosis_fle;
	var $cause_fle;
	var $status_fle;
	var $creation_date_fle;
	var $currency_fle;
	var $exchange_rate_fle;
	var $total_cost_fle;
	var $authorized_fle;
	var $observations_fle;
	var $end_date_fle;
	var $contanct_info_fle;
	var $type_fle;
	var $incident_date_fle;
	var $aux_data;

	function __construct($db, $serial_fle = NULL, $serial_cit = NULL, $opening_usr = NULL, $serial_trl = NULL,
						 $serial_cus = NULL, $closing_usr = NULL, $serial_sal = NULL, $diagnosis_fle = NULL,
						 $cause_fle = NULL, $status_fle = NULL, $creation_date_fle = NULL, $currency_fle = NULL,
						 $exchange_rate_fle = NULL, $total_cost_fle = NULL, $authorized_fle = NULL,
						 $observations_fle = NULL,  $end_date_fle = NULL, $contanct_info_fle = NULL, $type_fle = NULL,
						 $incident_date_fle = NULL){
		$this -> db = $db;
		$this -> serial_fle = $serial_fle;
		$this -> serial_cit = $serial_cit;
		$this -> opening_usr = $opening_usr;
		$this -> serial_trl = $serial_trl;
		$this -> serial_cus = $serial_cus;
		$this -> closing_usr = $closing_usr;
		$this -> serial_sal = $serial_sal;
		$this -> diagnosis_fle = $diagnosis_fle;
		$this -> cause_fle = $cause_fle;
		$this -> status_fle = $status_fle;
		$this -> creation_date_fle = $creation_date_fle;
		$this -> currency_fle = $currency_fle;
		$this -> exchange_rate_fle = $exchange_rate_fle;
		$this -> total_cost_fle = $total_cost_fle;
		$this -> authorized_fle = $authorized_fle;
		$this -> observations_fle = $observations_fle;
		$this -> end_date_fle = $end_date_fle;
		$this -> contanct_info_fle = $contanct_info_fle;
		$this -> type_fle = $type_fle;
		$this -> incident_date_fle = $incident_date_fle;
	}

	/*
	 * SELECT DISTINCT spv.type_spv
               FROM service_provider_by_file spf
               JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
               WHERE serial_fle='".$serial_fle."'
	 */
	/***********************************************
	* function getData
	* gets data by serial_fle
	***********************************************/
	function getData(){
            if($this->serial_fle!=NULL){
                $sql = "SELECT DISTINCT fle.serial_fle, fle.serial_cit, fle.opening_usr, fle.serial_trl, fle.serial_cus, fle.closing_usr,
							   fle.serial_sal, fle.diagnosis_fle, fle.cause_fle, fle.status_fle, DATE_FORMAT(fle.creation_date_fle, '%d/%m/%Y'),
							   fle.currency_fle, fle.exchange_rate_fle, fle.total_cost_fle, fle.authorized_fle, fle.observations_fle,
							   fle.end_date_fle, fle.contanct_info_fle, fle.type_fle, DATE_FORMAT(fle.incident_date_fle,'%d/%m/%Y'),
							   spf.medical_type_spf, spv.type_spv, cus.medical_background_cus, cit.name_cit, cou.serial_cou, cou.name_cou
                        FROM `file` fle
						LEFT JOIN service_provider_by_file spf ON spf.serial_fle = fle.serial_fle
						LEFT JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
						LEFT JOIN city cit ON cit.serial_cit = fle.serial_cit
						LEFT JOIN country cou ON cou.serial_cou = cit.serial_cou
						JOIN customer cus ON cus.serial_cus = fle.serial_cus
                        WHERE fle.serial_fle='".$this->serial_fle."'";
                //die($sql);
                $result = $this->db->Execute($sql);
                if ($result === false) return false;

                if($result->fields[0]){
                    $this->serial_fle=$result->fields[0];
                    $this->serial_cit=$result->fields[1];
					$this->opening_usr=$result->fields[2];
                    $this->serial_trl=$result->fields[3];
                    $this->serial_cus=$result->fields[4];
					$this->closing_usr=$result->fields[5];
                    $this->serial_sal=$result->fields[6];
                    $this->diagnosis_fle=$result->fields[7];
                    $this->cause_fle=$result->fields[8];
                    $this->status_fle=$result->fields[9];
                    $this->creation_date_fle=$result->fields[10];
                    $this->currency_fle=$result->fields[11];
                    $this->exchange_rate_fle=$result->fields[12];
                    $this->total_cost_fle=$result->fields[13];
                    $this->authorized_fle=$result->fields[14];
                    $this->observations_fle=$result->fields[15];
                    $this->end_date_fle=$result->fields[16];
                    $this->contanct_info_fle=$result->fields[17];
                    $this->type_fle=$result->fields[18];
					$this->incident_date_fle=$result->fields[19];
					$this->aux_data['medical_type']=$result->fields[20];
					$this->aux_data['type_spv']=$result->fields[21];
					$this->aux_data['medical_background_cus']=$result->fields[22];
					$this->aux_data['name_cit']=$result->fields[23];
					$this->aux_data['serial_cou']=$result->fields[24];
					$this->aux_data['name_cou']=$result->fields[25];
                    return true;
                }
                else
                    return false;
            }else
                return false;
	}

        /***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
		$sql = "INSERT INTO `file` (
						serial_fle,
						serial_cit,
						opening_usr,
						serial_trl,
						serial_cus,
						closing_usr,
						serial_sal,
						diagnosis_fle,
						cause_fle,
						status_fle,
						creation_date_fle,
						currency_fle,
						exchange_rate_fle,
						total_cost_fle,
						authorized_fle,
						observations_fle,
						end_date_fle,
						contanct_info_fle,
						type_fle,
						incident_date_fle
					) VALUES ( 
					NULL,";
			($this->serial_cit)?$sql.="'{$this->serial_cit}',":$sql.="NULL,";
			$sql .= "'$this->opening_usr',";
			($this->serial_trl)?$sql.="'{$this->serial_trl}',":$sql.="NULL,";
			$sql.="'$this->serial_cus',
					NULL,
				   '$this->serial_sal',
				   '$this->diagnosis_fle',
				   '$this->cause_fle',
				   'OPEN',";
			($this->creation_date_fle)?$sql.="STR_TO_DATE('$this->creation_date_fle','%Y-%m-%d'),":$sql.="NOW(),";
			$sql .= "'$this->currency_fle',
					 0,
					 0,
					 '$this->authorized_fle',
					 '$this->observations_fle',";
			($this->end_date_fle)?$sql.="STR_TO_DATE('$this->end_date_fle','%Y-%m-%d'),":$sql.="NULL,";
			$sql .="'$this->contanct_info_fle',";
			($this->type_fle)?$sql.="{$this->type_fle},":$sql.="NULL,";
			($this->incident_date_fle)?$sql.="STR_TO_DATE('$this->incident_date_fle','%Y-%m-%d'))":$sql.="NULL)";
		//die($sql);
		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

        /***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
            $sql="UPDATE `file`
            SET";
            if($this->serial_cus) {
                $sql.=" serial_cus='$this->serial_cus',";
            }
			if($this->closing_usr) {
                $sql.=" closing_usr='$this->closing_usr',";
            }
            if($this->serial_cit) {
                $sql.=" serial_cit='$this->serial_cit',";
            }
            if($this->serial_trl) {
                $sql.=" serial_trl='$this->serial_trl',";
            }
            if($this->serial_sal) {
                $sql.=" serial_sal='$this->serial_sal',";
            }
            $sql.=" diagnosis_fle='$this->diagnosis_fle',
                    cause_fle='$this->cause_fle',
                    status_fle='$this->status_fle',
                    currency_fle='$this->currency_fle',
                    exchange_rate_fle='$this->exchange_rate_fle',
                    total_cost_fle='$this->total_cost_fle',
                    authorized_fle='$this->authorized_fle',
                    observations_fle='$this->observations_fle',
                    contanct_info_fle='$this->contanct_info_fle'";
			($this->type_fle)?$sql.=",type_fle='$this->type_fle'":NULL;
			($this->end_date_fle)?$sql.=",end_date_fle = STR_TO_DATE('$this->end_date_fle','%Y-%m-%d')":NULL;
			($this->incident_date_fle)?$sql.=",incident_date_fle = STR_TO_DATE('$this->incident_date_fle','%d/%m/%Y')":NULL;
            $sql .= " WHERE serial_fle = '$this->serial_fle'";
			//die($sql);
            $result = $this->db->Execute($sql);
            //die($sql);
            if ($result==true){
				return true;
			}else{
				ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
				return false;
			}
	}


	/***********************************************
	* funcion getFileMedicalType
	* gets the medical Type of a file
	***********************************************/
	function getFileMedicalType() {
		if($this->serial_fle) {
			$sql = "SELECT DISTINCT medical_type_spf
					FROM service_provider_by_file
					WHERE serial_fle = {$this->serial_fle}";

			$result=$this->db->getOne($sql);

			if($result){
				return $result;
			}else{
				return false;
			}
		} else {
			return false;
		}
	}
	/***********************************************
	* funcion updateFileMedicalType
	* gets the medical Type of a file
	***********************************************/
	function updateFileMedicalType($medical_type) {
		if($this->serial_fle) {
			$sql = "UPDATE service_provider_by_file
					SET medical_type_spf = '$medical_type'
					WHERE serial_fle = {$this->serial_fle}";
			$result = $this->db->Execute($sql);

            if ($result==true)
                    return true;
            else
                    return false;
		} else {
			return false;
		}
	}

        /***********************************************
        * funcion getFilesByCard
        * gets all the Files belonging to a Card
        ***********************************************/
	function getFilesByCard($card_number){
            $sql = "SELECT DISTINCT f.serial_fle, cou.name_cou, cit.name_cit, f.serial_trl, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
                           f.serial_sal, f.diagnosis_fle, f.cause_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date_fle,
                           f.currency_fle, f.exchange_rate_fle, f.total_cost_fle, f.authorized_fle,
                           f.observations_fle, f.end_date_fle, f.type_fle, IF(al.serial_alt,'alert',NULL) as serial_alt
                    FROM `file` f
                    JOIN sales s ON s.serial_sal = f.serial_sal
                    JOIN customer cus ON cus.serial_cus = f.serial_cus
                    JOIN city cit ON cit.serial_cit = f.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    LEFT JOIN alerts al ON al.serial_fle = f.serial_fle AND al.status_alt='PENDING'
                    LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
                    WHERE (s.card_number_sal='".$card_number."'
                    OR trl.card_number_trl='".$card_number."')
					ORDER BY f.serial_fle asc";
                    //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}

	/***********************************************
	* funcion getMedicalCheckupFilesByCard
	* gets all the Files belonging to a Card
	***********************************************/
	function getMedicalCheckupFilesByCard($card_number){
            $sql = "SELECT DISTINCT fle.serial_fle, cou.name_cou, cit.name_cit, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
									fle.diagnosis_fle, DATE_FORMAT(fle.creation_date_fle, '%d/%m/%Y') as creation_date_fle, fle.status_fle,
									prq.serial_prq, DATE_FORMAT(prq.audited_date_prq, '%d/%m/%Y') as audited_date_prq
                    FROM `file` fle
                    JOIN sales sal ON sal.serial_sal = fle.serial_sal
					LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
                    JOIN customer cus ON cus.serial_cus = fle.serial_cus
                    JOIN city cit ON cit.serial_cit = fle.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
					JOIN payment_request prq ON prq.serial_fle = fle.serial_fle AND prq.status_prq = 'AUDITED'
                    WHERE (sal.card_number_sal='$card_number' OR trl.card_number_trl='$card_number')
					ORDER BY fle.serial_fle asc";
                    //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}

	 /***********************************************
        * funcion getFilesByDealer
        * gets all the Files belonging to a Dealer
        ***********************************************/
	function getFilesByDealer($serial_dea,$status_fle=NULL,$operator=NULL){
            $sql = "SELECT DISTINCT f.serial_fle, cou.name_cou, cit.name_cit, f.serial_trl, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
                           s.card_number_sal, f.serial_sal, f.diagnosis_fle, f.cause_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date_fle,
                           f.currency_fle, f.exchange_rate_fle, f.total_cost_fle, f.authorized_fle,
                           f.observations_fle, f.end_date_fle, f.type_fle, IF(al.serial_alt,'alert',NULL) as serial_alt
                    FROM `file` f
                    JOIN sales s ON s.serial_sal = f.serial_sal
                    JOIN customer cus ON cus.serial_cus = f.serial_cus
                    JOIN city cit ON cit.serial_cit = f.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    LEFT JOIN alerts al ON al.serial_fle = f.serial_fle AND al.status_alt='PENDING'
                    LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
                    WHERE (cnt.serial_dea='".$serial_dea."')";
            if($status_fle!=NULL && $operator!=NULL) {
                $sql.= " AND f.status_fle".$operator."'".$status_fle."'";
            }
            $sql.= " ORDER BY f.serial_fle asc";
                    //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}

        /***********************************************
        * funcion getAlertsByFile
        * gets all the Alerts belonging to a File
        ***********************************************/
	function getAlertsByFile(){
            $sql = "SELECT al.*
                    FROM `file` f
                    JOIN alerts al ON al.serial_fle = f.serial_fle
                    WHERE f.serial_fle=".$this->serial_fle."
                    AND al.status_alt='PENDING'";
                    //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}

        /*************************************************
        * funcion getCardNumberByFile
        * gets the card number acording to a serial_fle
        *************************************************/
	public static function getCardNumberByFile($db, $serial_fle){
            $sql = "SELECT IFNULL(s.card_number_sal,trl.card_number_trl) as card_number
                    FROM `file` f
                    JOIN sales s ON s.serial_sal = f.serial_sal
                    LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
                    WHERE f.serial_fle='$serial_fle'";
                    //die($sql);
            $result = $db -> Execute($sql);
            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return false;
            }
	}

        /***********************************************
        * funcion getOtherCardFiles
        * gets the card number acording to a serial_fle
        ***********************************************/
	function getOtherCardFilesByCustomer($serial_sal,$serial_cus){
            $sql = "SELECT DISTINCT IFNULL(s.card_number_sal,trl.card_number_trl) as card_number
                    FROM `file` f
                    JOIN sales s ON s.serial_sal = f.serial_sal
                    LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
                    WHERE f.serial_sal<>'".$serial_sal."'
                    AND f.serial_cus='".$serial_cus."'
                    and f.serial_cit IS NOT NULL";
                    //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}

	/***********************************************
	* funcion getFileReport
	* gets the report of the file
	***********************************************/
	function getFileReport(){
		$sql = "SELECT DISTINCT f.serial_fle, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
								IFNULL(sal.card_number_sal, trl.card_number_trl) as card_number,
								CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr, pbl.name_pbl,
								DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date, f.status_fle,
								IFNULL(f.cause_fle,'N/A') as cause_fle, IFNULL(f.diagnosis_fle,'N/A') as diagnosis_fle,
								f.contanct_info_fle, bcl.entry_blg as client_blog, bop.entry_blg as operator_blog
				FROM `file` f
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN sales sal ON sal.serial_sal = f.serial_sal
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
				JOIN user usr ON usr.serial_usr = f.opening_usr
				JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN blog bcl ON bcl.serial_fle = f.serial_fle AND bcl.type_blg = 'CLIENT'
				JOIN blog bop ON bop.serial_fle = f.serial_fle AND bop.type_blg = 'OPERATOR'
				WHERE f.serial_fle= {$this->serial_fle}";
		//die($sql);
		$result=$this->db->getAll($sql);

		if($result){
			return $result[0];
		}else{
			return NULL;
		}
	}

	/***********************************************
	* funcion getMedicalCheckupReport
	* gets the report of the medical checkup the file
	***********************************************/
	public static function getMedicalCheckupReport($db,$serial_prq){
		$sql = "SELECT DISTINCT f.serial_fle, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
								DATE_FORMAT(cus.birthdate_cus,'%d/%m/%Y') as birthdate_cus, cus.medical_background_cus,
								IFNULL(sal.card_number_sal, trl.card_number_trl) as card_number, pbl.name_pbl,
								DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
								DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
								DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal,
								f.type_fle, f.diagnosis_fle, f.cause_fle, prq.auditor_comments
				FROM `file` f
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN sales sal ON sal.serial_sal = f.serial_sal
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN payment_request prq ON prq.serial_fle = f.serial_fle
				WHERE prq.serial_prq = $serial_prq";
		//die($sql);
		$result=$db->getAll($sql);

		if($result){
			return $result[0];
		}else{
			return NULL;
		}
	}



	/*
	 * @name: getOldFilesByCustomer
	 * @param: $db - DB connection
	 * @param: $serial_cus - Customer ID
	 * @param: $excluded_file_id - All the files except this one.
	 */
	public static function getOldFilesByCustomer($db, $serial_cus, $excluded_file_id=NULL){
		$sql = "SELECT DISTINCT f.serial_fle, cou.name_cou, cit.name_cit, f.serial_trl, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
					   f.serial_sal, f.diagnosis_fle, f.cause_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle,'%d/%m/%Y') AS 'creation_date_fle',
					   f.currency_fle, f.exchange_rate_fle, f.total_cost_fle, f.authorized_fle,
					   f.observations_fle, f.end_date_fle, f.type_fle, IF(al.serial_alt,'alert',NULL) as serial_alt, IFNULL(s.card_number_sal,trl.card_number_trl) AS 'card_number'
				FROM `file` f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN city cit ON cit.serial_cit = f.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou
				LEFT JOIN alerts al ON al.serial_fle = f.serial_fle AND al.status_alt='PENDING'
				LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
				WHERE f.serial_cus='".$serial_cus."'";

		if($excluded_file_id!=NULL && $excluded_file_id!=''){
			$sql.=" AND f.serial_fle <> '".$excluded_file_id."'";
		}

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * saleHasFilesAssociated
	 * @param: $db - DB connection
	 * @param: $serial_sal - Sale ID
	 * @return: TRUE if has any file.
	 *          FALSE otherwise.
	 */
	public static function saleHasFilesAssociated($db, $serial_sal){
            $sql="SELECT serial_fle
                  FROM file
                  WHERE serial_sal='".$serial_sal."'
				  AND status_fle <> 'void'";
            $result = $db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                    return true;
            }else{
                    return false;
            }
	}

        /*
	 * nonPaidInvoiceAssistances
	 * @param: $db - DB connection
	 * @param: 
	 * @return: 
	 */
        public static function nonPaidInvoiceAssistances($db){
            $sql = "SELECT fle.*, cou.serial_cou as cou_serial_cou, couc.serial_cou, cus.*,
						   IFNULL(sal.card_number_sal,trl.card_number_trl) as card_number,
						   sal.serial_sal
                    FROM `file` fle
                    JOIN sales sal ON sal.serial_sal = fle.serial_sal
                    JOIN invoice inv ON inv.serial_inv = sal.serial_inv
                    JOIN city cit ON cit.serial_cit = sal.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    JOIN customer cus ON cus.serial_cus = fle.serial_cus
                    JOIN city citc ON citc.serial_cit = cus.serial_cit
                    JOIN country couc ON couc.serial_cou = citc.serial_cou
                    LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
                    WHERE NOW() > inv.due_date_inv
                    AND fle.creation_date_fle NOT BETWEEN inv.date_inv AND inv.due_date_inv
                    AND fle.serial_cit IS NOT NULL
                    AND inv.status_inv = 'STAND-BY'";

            $result=$db->getAll($sql);

            if($result){
                    return $result;
            }else{
                    return false;
            }
        }

	public static function getFilesBySaleReport($db, $serial_sal){
		$sql="SELECT DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') AS 'creation_date_fle', CONCAT(c.first_name_cus, ' ', c.last_name_cus) AS 'name_cus',
					 s.card_number_sal, f.status_fle, 
					 IFNULL(SUM(prq.amount_authorized_prq),'N/A') AS 'total_paid', 
					 IFNULL(SUM(spf.estimated_amount_spf),'N/A') AS 'reserved'
			  FROM file f
			  JOIN sales s ON s.serial_sal=f.serial_sal
			  JOIN customer c ON c.serial_cus=f.serial_cus
			  LEFT JOIN payment_request prq ON prq.serial_fle=f.serial_fle AND prq.status_prq='APROVED' AND prq.dispatched_prq='YES'
			  LEFT JOIN service_provider_by_file spf ON spf.serial_fle=f.serial_fle
			  WHERE f.serial_sal=".$serial_sal."
			  GROUP BY f.serial_fle";

		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);

			return $arreglo;
		}else{
			return FALSE;
		}		
	}

	public static function getFileByNumber($db,$serial_fle){
		$sql = "SELECT DISTINCT f.serial_fle, cou.name_cou, cit.name_cit, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
                           f.diagnosis_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date_fle
                    FROM `file` f
                    JOIN customer cus ON cus.serial_cus = f.serial_cus
                    JOIN city cit ON cit.serial_cit = f.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    WHERE f.serial_fle=".$serial_fle;
		//die($sql);
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);

			return $arreglo[0];
		}else{
			return FALSE;
		}
	}

	public static function getFileByCustomer($db,$serial_cus){
		$sql = "SELECT DISTINCT f.serial_fle, cou.name_cou, cit.name_cit, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
                           f.diagnosis_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date_fle
                    FROM `file` f
                    JOIN customer cus ON cus.serial_cus = f.serial_cus
                    JOIN city cit ON cit.serial_cit = f.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    WHERE f.serial_cus=".$serial_cus;
		//die($sql);
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);

			return $arreglo;
		}else{
			return FALSE;
		}
	}
	
	/**
	 *
	 * @param type $db
	 * @param type $serial_cou
	 * @param type $from_date
	 * @param type $to_date
	 * @param type $serial_mbc
	 * @param type $serial_usr
	 * @param type $serial_dea
	 * @param type $serial_bra
	 * @param type $serial_pro
	 * @return array Assistance_registers
	 */
	public static function getLossRateInformation($db, $serial_cou, $from_date, $to_date, $date_field, $serial_mbc = NULL, 
												$serial_usr = NULL, $serial_dea = NULL, $serial_bra = NULL,	
												$serial_pro = NULL ,$fileTypes = NULL, $fileStatus = NULL){
		if($serial_mbc) $manager_clause = " AND d.serial_mbc = ".$serial_mbc;
		if($serial_usr) $ubd_clause = " AND ubd.serial_usr = ".$serial_usr;
		if($serial_dea) $dea_clause = " AND d.dea_serial_dea = ".$serial_dea;
		if($serial_bra) $bra_clause = " AND d.serial_dea = ".$serial_bra;
		if($serial_pro) $pro_clause = " AND pxc.serial_pro = ".$serial_pro;
        if($fileTypes) $fileTypesClause = " AND f.type_fle IN({$fileTypes}) ";
		if($fileStatus) $fileStatusClause = " AND f.status_fle IN({$fileStatus}) ";
		$sql = "SELECT DISTINCT f.serial_fle, spf.serial_spf, dbf.serial_dbf, spf.serial_bxp, spf.serial_spv, DATE_FORMAT(f.incident_date_fle, '%d/%m/%Y') AS 'incident_date_fle', cou.name_cou AS 'sold_country',
						IFNULL(a_cou.name_cou, 'N/A') AS 'assistance_country',
						CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus', 
						IFNULL(s.card_number_sal, trl.card_number_trl) as 'card_number',
						pbl.name_pbl as 'name_product', f.type_fle, f.diagnosis_fle,
						f.cause_fle, IFNULL(spf.estimated_amount_spf, 'N/A') AS 'estimated_amount_spf', spv.name_spv, bbl.description_bbl,
						IFNULL(bbd.amount_presented_bbd, 'Pendiente') AS 'presented_amount', 
						IFNULL(bbd.amount_authorized_bbd, 'Pendiente') AS 'authorized_amount',
                        f.status_fle, 
						IF(prq.dispatched_date_prq IS NULL, 'N/A', DATE_FORMAT(prq.dispatched_date_prq, '%d/%m/%Y')) AS 'dispatched_date_prq',
						DATEDIFF(CURDATE(), f.creation_date_fle) AS 'elapsed_time'
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc $pro_clause
				JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer d ON d.serial_dea = cnt.serial_dea $manager_clause $dea_clause $bra_clause
				JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE' $ubd_clause
				JOIN sector sec ON sec.serial_sec = d.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou AND cou.serial_cou = $serial_cou
				JOIN customer cus ON cus.serial_cus = f.serial_cus
		
				LEFT JOIN city a_cit ON a_cit.serial_cit = f.serial_cit
				LEFT JOIN country a_cou  ON a_cou.serial_cou = a_cit.serial_cou
				LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl

				LEFT JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle
				LEFT JOIN benefits_by_document bbd ON bbd.serial_spf = spf.serial_spf
				LEFT JOIN documents_by_file dbf ON dbf.serial_dbf = bbd.serial_dbf
				LEFT JOIN payment_request prq ON prq.serial_dbf = dbf.serial_dbf AND status_prq = 'APROVED'

				LEFT JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
				LEFT JOIN benefits_product bxp ON spf.serial_bxp = bxp.serial_bxp
				LEFT JOIN benefits_by_language bbl ON bbl.serial_ben = bxp.serial_ben AND bbl.serial_lang = {$_SESSION['serial_lang']}

				
				WHERE $date_field BETWEEN STR_TO_DATE('$from_date', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$to_date', '%d/%m/%Y'), INTERVAL 1 DAY)
                $fileTypesClause
                $fileStatusClause
				ORDER BY spv.name_spv, f.incident_date_fle";
		//die('<pre>'.$sql.'</pre>');
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}		
	}
	
	public static function getLossRateInformationGlobal($db, $serial_zon, $from_date, $to_date, $filtered_field){
		$sql = "SELECT cou.name_cou, SUM(s.total_sal) AS 'total_sold',
						SUM(IFNULL(spf.estimated_amount_spf, '0')) AS 'estimated_amount_spf',
						SUM(IFNULL(bbd.amount_presented_bbd, '0')) AS 'presented_amount', 
						SUM(IFNULL(bbd.amount_authorized_bbd, '0')) AS 'authorized_amount'
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer d ON d.serial_dea = cnt.serial_dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = d.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou AND cou.serial_zon = $serial_zon
				JOIN customer cus ON cus.serial_cus = f.serial_cus
		
				LEFT JOIN city a_cit ON a_cit.serial_cit = f.serial_cit
				LEFT JOIN country a_cou  ON a_cou.serial_cou = a_cit.serial_cou		
				LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
		
				LEFT JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle
				LEFT JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
				LEFT JOIN benefits_product bxp ON spf.serial_bxp = bxp.serial_bxp
				LEFT JOIN benefits_by_language bbl ON bbl.serial_ben = bxp.serial_ben AND bbl.serial_lang = {$_SESSION['serial_lang']}
								
				LEFT JOIN documents_by_file dbf ON dbf.serial_fle = f.serial_fle
				LEFT JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf AND spf.serial_spf = bbd.serial_spf
				
				WHERE $filtered_field BETWEEN STR_TO_DATE('$from_date', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$to_date', '%d/%m/%Y'), INTERVAL 1 DAY)
				GROUP BY cou.serial_cou
				ORDER BY cou.name_cou";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
    /***********************************************
    * funcion getTypeValues
    * Returns the current values for the type field.
    ***********************************************/	
	public static function getTypeValues($db){
		$sql = "SHOW COLUMNS FROM file LIKE 'type_fle'";
		$result = $db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

    /***********************************************
    * funcion getStatusValues
    * Returns the current values for the status field.
    ***********************************************/
	public static function getStatusValues($db){
		$sql = "SHOW COLUMNS FROM file LIKE 'status_fle'";
		$result = $db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	public static function getFilesByStatusToManage($db, $status_fle){
		$sql = "SELECT f.serial_fle, diagnosis_fle, cou.name_cou, status_fle,DATE_FORMAT(f.creation_date_fle,'%d/%m/%Y') as creation_date_fle,
						CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'customer',
						SUM(IFNULL(spf.estimated_amount_spf, 0)) AS 'total_estimated',
						SUM(IFNULL(spf.real_amount_spf, 0)) AS 'total_real',
						CAST(IF(trl.card_number_trl IS NOT NULL, trl.card_number_trl, sa.card_number_sal) AS CHAR) AS 'card_number_sal'
				FROM file f
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN city cit ON cit.serial_cit = f.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou
				JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle
				JOIN sales sa ON sa.serial_sal = f.serial_sal
				LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
				WHERE status_fle IN ($status_fle)
				GROUP BY f.serial_fle
				ORDER BY f.serial_fle";
		
		//Debug::print_r($sql);
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getFilesToVoid($db, $card_number_sal){
		$sql = "SELECT f.serial_fle, diagnosis_fle, status_fle,
						CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'customer',
						DATE_FORMAT(creation_date_fle, '%d/%m/%Y') AS 'creation_date_fle'
				FROM file f
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN sales sa ON sa.serial_sal = f.serial_sal
				LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
				WHERE status_fle IN ('open', 'in_liquidation')
				AND (card_number_sal = '$card_number_sal' 
					OR card_number_trl = '$card_number_sal')
				ORDER BY f.serial_fle";
		
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	
	public static function voidFileRegistration($db, $serial_fle, $status_fle, $serial_usr, $observations){
		$sql = "UPDATE file SET 
					status_fle = '$status_fle',
					closing_usr = $serial_usr,
					observations_fle = CONCAT(observations_fle, ' ', '$observations'),
					end_date_fle = CURDATE()
				WHERE serial_fle = $serial_fle";
		
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'UPDATE STATUS FAILED - File', $sql);
			return FALSE;
		}
	}
	
	/**
	 *
	 * @param type $db
	 * @param type $serial_cou
	 * @param type $from_date
	 * @param type $to_date
	 * @param type $serial_mbc
	 * @param type $serial_usr
	 * @param type $serial_dea
	 * @param type $serial_bra
	 * @param type $serial_pro
	 * @return array Assistance_registers
	 */
	public static function getLossRateInformationByCard($db, $serial_cou, $from_date, $to_date, $date_field, $serial_mbc = NULL, 
												$serial_usr = NULL, $serial_dea = NULL, $serial_bra = NULL,	
												$serial_pro = NULL ){
		if($serial_mbc) $manager_clause = " AND d.serial_mbc = ".$serial_mbc;
		if($serial_usr) $ubd_clause = " AND ubd.serial_usr = ".$serial_usr;
		if($serial_dea) $dea_clause = " AND d.dea_serial_dea = ".$serial_dea;
		if($serial_bra) $bra_clause = " AND d.serial_dea = ".$serial_bra;
		if($serial_pro) $pro_clause = " AND pxc.serial_pro = ".$serial_pro;
		
		$sql = "SELECT	IFNULL(s.card_number_sal, 'N/A') AS 'card_number_sal', CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus', f.serial_fle, 
                            DATE_FORMAT(incident_date_fle, '%d/%m/%Y') AS 'incident_date', spf.serial_spv, diagnosis_fle, cause_fle, f.type_fle, status_fle,
                            cou.name_cou,   cus.birthdate_cus AS 'birthdate_cus',
                             s.total_sal, s.total_cost_sal, s.begin_date_sal AS 'begin_date', s.end_date_sal AS 'end_date', 
                            pbl.name_pbl, s.serial_sal, a_cou.name_cou AS 'asistance_country', a_cit.name_cit AS 'assistance_city'
                        FROM  file f 
                        JOIN sales s ON s.serial_sal = f.serial_sal
                        LEFT JOIN service_provider_by_file spf ON f.serial_fle = spf.serial_fle
                        LEFT JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
                        LEFT JOIN benefits_product bp ON bp.serial_bxp = spf.serial_bxp
                        LEFT JOIN benefits_by_language bbl ON bbl.serial_ben = bp.serial_ben
                        JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
                        JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc $pro_clause
                        JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
                        JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
                        JOIN dealer d ON d.serial_dea = cnt.serial_dea $manager_clause $dea_clause $bra_clause
                        JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE' $ubd_clause
                        JOIN sector sec ON sec.serial_sec = d.serial_sec
                        JOIN city cit ON cit.serial_cit = sec.serial_cit
                        JOIN country cou ON cou.serial_cou = cit.serial_cou AND cou.serial_cou = $serial_cou
                        JOIN customer cus ON cus.serial_cus = f.serial_cus
                        JOIN city a_cit ON a_cit.serial_cit = f.serial_cit
                        JOIN country a_cou ON a_cou.serial_cou = a_cit.serial_cou
                        WHERE $date_field BETWEEN STR_TO_DATE('$from_date', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$to_date', '%d/%m/%Y'), INTERVAL 1 DAY)
                        GROUP BY IF(spf.serial_spv IS NULL, f.serial_fle, spf.serial_spv), IF(spf.serial_fle IS NULL, f.serial_fle, spf.serial_fle)
                        ORDER BY name_cus, card_number_sal, serial_fle, incident_date";
		
//		Debug::print_r($sql); die;
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}		
	}
	
	public static function getAssistantFinancialInformationByCard($db, $serial_fle, $global_filter = FALSE){	
		$sqlcn = "SELECT SUM(IFNULL(amount_reclaimed_prq,0)) AS 'cnote_pres', SUM(IFNULL(amount_authorized_prq, 0)) AS 'cnote_auth'
							FROM file f
							JOIN payment_request pr ON pr.serial_fle = f.serial_fle
							AND f.serial_fle = $serial_fle
							AND pr.type_prq = 'CREDIT_NOTE'
							GROUP BY f.serial_fle";
		$resultcn = $db->getRow($sqlcn);
		if($global_filter){
			if($resultcn){
			$sql = "SELECT (SUM(b.authorized)-(IFNULL(p.cnote_auth,0)*2)) AS 'authorized_amount', (SUM(b.presented)-(IFNULL(p.cnote_pres,0)*2)) AS 'presented_amount',
							GROUP_CONCAT(DISTINCT b.provider ORDER BY b.provider DESC SEPARATOR ',') AS 'sprovider'
					FROM (
							SELECT	SUM(IFNULL(amount_authorized_bbd, 0)) AS 'authorized', 
									SUM(IFNULL(amount_presented_bbd, 0)) AS 'presented', 
									f.serial_fle,
									IFNULL(GROUP_CONCAT(DISTINCT name_spv ORDER BY name_spv DESC SEPARATOR ','), 'N/A') AS 'provider'
							FROM file f
							JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle AND f.serial_fle = $serial_fle
							JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
							LEFT JOIN benefits_by_document bbd ON bbd.serial_spf = spf.serial_spf
							GROUP BY spf.serial_spf
						) AS b,
						(
							$sqlcn
						) AS p
					GROUP BY b.serial_fle";	
			}else{
			$sql = "SELECT SUM(b.authorized) AS 'authorized_amount', SUM(b.presented) AS 'presented_amount',
							GROUP_CONCAT(DISTINCT b.provider ORDER BY b.provider DESC SEPARATOR ',') AS 'sprovider'
					FROM (
							SELECT	SUM(IFNULL(amount_authorized_bbd, 0)) AS 'authorized', 
									SUM(IFNULL(amount_presented_bbd, 0)) AS 'presented', 
									f.serial_fle,
									IFNULL(GROUP_CONCAT(DISTINCT name_spv ORDER BY name_spv DESC SEPARATOR ','), 'N/A') AS 'provider'
							FROM file f
							JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle AND f.serial_fle = $serial_fle
							JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
							LEFT JOIN benefits_by_document bbd ON bbd.serial_spf = spf.serial_spf
							GROUP BY spf.serial_spf
						) AS b
					GROUP BY b.serial_fle";	
			}

		}else{
			if($resultcn){
			$sql = "SELECT (SUM(b.total_auth)-(IFNULL(p.cnote_auth,0))) AS 'real_amount'
					FROM (
							SELECT SUM(IFNULL(amount_authorized_prq, 0)) AS 'total_auth'
							FROM file f
							JOIN payment_request pr ON pr.serial_fle = f.serial_fle
							AND f.serial_fle = $serial_fle
							AND pr.type_prq <> 'CREDIT_NOTE'
							GROUP BY f.serial_fle
						) AS b,
						(
							$sqlcn
						) AS p
					";	
			}else{
			$sql = " SELECT SUM(IFNULL(amount_authorized_prq, 0)) AS 'real_amount'
							FROM file f
							JOIN payment_request pr ON pr.serial_fle = f.serial_fle
							AND f.serial_fle = $serial_fle
							AND pr.type_prq <> 'CREDIT_NOTE'
							GROUP BY f.serial_fle
					";	
		}
			/*$sql = "SELECT (p.in_favor - p.to_pay) AS 'real_amount'
					FROM (
							SELECT	IF(type_prq = 'CREDIT_NOTE', SUM(amount_authorized_prq), 0) AS 'to_pay',
									IF(type_prq <> 'CREDIT_NOTE', SUM(amount_authorized_prq), 0) AS 'in_favor'
							FROM file f
							JOIN payment_request pr ON pr.serial_fle = f.serial_fle 
								AND f.serial_fle = $serial_fle
								AND pr.status_prq <> 'DENIED'
							GROUP BY f.serial_fle
						) AS p
					";*/
		}
		
		//Debug::print_r($sql); die;
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
        
        public static function getAssistantFinancialInformationByFileandProvider($db, $serial_fle, $serial_spv){
            $service_provider = isset($serial_spv)? " AND spv.serial_spv = $serial_spv" :  "";
           
            $sql = "SELECT serial_prq, SUM(IFNULL(amount_authorized_bbd, 0)) AS 'authorized', 
                            SUM(IFNULL(amount_presented_bbd, 0)) AS 'presented', 
                            f.serial_fle, name_spv AS 'provider'
                    FROM file f
                    JOIN payment_request prq ON prq.serial_fle = f.serial_fle AND f.serial_fle = $serial_fle
			AND prq.status_prq = 'APROVED'
                    JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                    JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf
                    JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf
                    JOIN service_provider spv ON spv.serial_spv = spf.serial_spv $service_provider";
            
//            Debug::print_r($sql); die;
            $result = $db->getRow($sql);

            if($result){
                    return $result;
            }else{
                    return FALSE;
            }
        }
	
	/*Funcion para obtener el monto estimado por expediente*/
	
	public static function getEstimatedAmountByCard($db, $serial_fle, $serial_spv){    
            $service_provider = isset($serial_spv)? " AND spf.serial_spv = $serial_spv" :  "";
            
            $sql = "SELECT SUM(IFNULL(estimated_amount_spf, 0)) AS 'estimated_amount'
                    FROM  file f 
                    JOIN service_provider_by_file spf ON f.serial_fle = spf.serial_fle AND f.serial_fle = $serial_fle $service_provider";

            //Debug::print_r($sql); die;
            $result = $db -> getRow($sql);

            if($result){
                    return $result;
            }else{
                    return FALSE;
            }
	}
	
	/*Funcion para obtener el monto del repricing de los documentos de un expediente*/
	public static function getRepricingInfo($db, $serial_fle){
		$sql = "SELECT SUM(IFNULL(amount_authorized_prq, 0)) AS 'repricing_amount'
					FROM file f
					JOIN payment_request prq ON prq.serial_fle = f.serial_fle AND f.serial_fle = $serial_fle
					and prq.status_prq = 'APROVED' and prq.type_prq = 'REPRICING'
				GROUP BY f.serial_fle";
		
		//Debug::print_r($sql); die;
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}		
	
	}
	
	/*Funcion para obtener el monto del FEE repricing de los documentos de un expediente por medio del campo observacion*/
	public static function getFeeRepricingInfo($db, $serial_fle){
		$sql = "SELECT SUM(IFNULL(amount_authorized_prq, 0)) AS 'fee_repricing'
					FROM file f
					JOIN payment_request prq ON prq.serial_fle = f.serial_fle AND f.serial_fle = $serial_fle
					and prq.status_prq = 'APROVED' and prq.type_prq = 'REPRICING' and prq.observations_prq = 'FEE REPRICING'
				GROUP BY f.serial_fle";
		
		//Debug::print_r($sql); die;
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}		
	
	}
	
	/**
	 * @name getAssistancesWithNonPaidInvoices
	 * @param type $db
	 * @param type $serial_cou
	 * @param type $serial_mbc
	 * @param type $serial_usr
	 * @param type $serial_dea
	 * @return array 
	 */
	public static function getAssistancesWithNonPaidInvoices($db, $details = false, $serial_cou = NULL, $serial_mbc = NULL, 
																$serial_usr = NULL, $serial_dea = NULL){
		if($serial_mbc) $mbc_filter = " AND mbc.serial_mbc = $serial_mbc";
		if($serial_usr) $ubd_filter = " AND ubd.serial_usr = $serial_usr";
		if($serial_dea) $dea_filter = " AND b.dea_serial_dea = $serial_dea";
		if($serial_cou) $cou_filter = " AND cou.serial_cou = $serial_cou";
		
		if($serial_cou || $details){
			$select_statement = "	f.serial_fle, s.card_number_sal, 
									CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
									DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
									DATE_FORMAT(s.end_date_sal , '%d/%m/%Y') AS 'end_date_sal',
									DATE_FORMAT(f.creation_date_fle , '%d/%m/%Y') AS 'creation_date_fle',
									DATE_FORMAT(i.date_inv , '%d/%m/%Y') AS 'date_inv',
									days_cdd, total_inv, b.name_dea, 
									CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_ubd'";
			$order_statement = "s.card_number_sal, f.serial_fle";
		}else{
			$select_statement = "DISTINCT cou.serial_cou, cou.name_cou";
			$order_statement = "cou.name_cou";
		}		
		
		$sql = "SELECT $select_statement
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv NOT IN ('PAID', 'VOID', 'UNCOLLECTABLE')
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea $dea_filter
				JOIN credit_day cdd ON cdd.serial_cdd = b.serial_cdd
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc $mbc_filter
				JOIN country cou ON cou.serial_cou = mbc.serial_cou $cou_filter
				JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE' $ubd_filter
				JOIN user u ON u.serial_usr = ubd.serial_usr
				GROUP BY f.serial_fle
				ORDER BY $order_statement";
		
		//Debug::print_r($sql); die;		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getDealerWithNonPaidInvoicesAndAssistance
	 * @param type $db
	 * @param type $serial_ubd
	 * @return boolean 
	 */
	public static function getDealerWithNonPaidInvoicesAndAssistance($db, $serial_usr){
		$sql = "SELECT DISTINCT b.dea_serial_dea, b.name_dea
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv NOT IN ('PAID', 'VOID', 'UNCOLLECTABLE')
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN credit_day cdd ON cdd.serial_cdd = b.serial_cdd
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN country cou ON cou.serial_cou = mbc.serial_cou 
				JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE' 
					AND ubd.serial_usr = $serial_usr
				JOIN user u ON u.serial_usr = ubd.serial_usr";
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 *
	 * @param type $db
	 * @param type $serial_sal
	 * @param type $serial_trl
	 * @return boolean 
	 */
	public static function getAssistancesWithNonPaidInvoicesBySale($db, $serial_sal, $serial_trl = false){
		if($serial_trl) $trl_filter = " AND f.serial_trl = $serial_trl";		
		
		$sql = "SELECT	f.serial_fle, s.card_number_sal, 
						CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
						DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
						DATE_FORMAT(s.end_date_sal , '%d/%m/%Y') AS 'end_date_sal',
						DATE_FORMAT(f.creation_date_fle , '%d/%m/%Y') AS 'creation_date_fle',
						DATE_FORMAT(i.date_inv , '%d/%m/%Y') AS 'date_inv',
						days_cdd, total_inv, b.name_dea, 
						CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_ubd'
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal AND s.serial_sal = $serial_sal
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv NOT IN ('PAID', 'VOID', 'UNCOLLECTABLE')
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN credit_day cdd ON cdd.serial_cdd = b.serial_cdd
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN country cou ON cou.serial_cou = mbc.serial_cou
				JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr
				LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl $trl_filter
				GROUP BY f.serial_fle
				ORDER BY s.card_number_sal, f.serial_fle";
		
		//Debug::print_r($sql); die;		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getFilesByCountryStatistics
	 * @param RSC $db
	 * @param int $serial_cou
	 * @param boolean $detailed
	 * @return boolean 
	 * @return array
	 */
	public static function getFilesByCountryStatistics($db, $serial_cou = NULL, $begin_date = NULL, 
														$end_date = NULL, $detailed = FALSE){
		$select_statement = "count(f.serial_fle) AS 'files_per_city', name_cit, f.serial_fle, cou.serial_cou, name_cou";
		
		if($detailed){
			$select_statement = "	f.serial_fle, s.card_number_sal, 
									CONCAT(first_name_cus, ' ', last_name_cus) AS 'name_pax',
									name_cit, name_cou, diagnosis_fle, status_fle, type_fle,
									DATE_FORMAT(incident_date_fle, '%d/%m/%Y') AS 'incident_date_fle',
									CONCAT(first_name_usr, ' ', u.last_name_usr) AS 'opening_usr'";
		}else{
			$grou_by_statement = "GROUP BY cit.serial_cit";
		}
		
		if($serial_cou) $and_country = " AND cou.serial_cou = $serial_cou";
		if($begin_date) $and_begin_date = " AND STR_TO_DATE(DATE_FORMAT(incident_date_fle, '%d/%m/%Y'), '%d/%m/%Y') >= STR_TO_DATE('$begin_date', '%d/%m/%Y')";
		if($end_date) $and_end_date = " AND STR_TO_DATE(DATE_FORMAT(incident_date_fle, '%d/%m/%Y'), '%d/%m/%Y') <= STR_TO_DATE('$end_date', '%d/%m/%Y')";
		
		$sql = "SELECT $select_statement
				FROM file f
				JOIN blog b ON b.serial_fle = f.serial_fle AND type_blg = 'OPERATOR'
				JOIN user u ON u.serial_usr = f.opening_usr
				JOIN sales s ON s.serial_sal = f.serial_sal $and_begin_date $and_end_date
				JOIN customer c ON c.serial_cus = f.serial_cus
				JOIN city cit ON cit.serial_cit = f.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou $and_country
				$grou_by_statement
				ORDER BY name_cou, name_cit, f.serial_fle";
		
		//Debug::print_r($sql); die;
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getBenefitsInFile($db, $serial_fle){
		$sql = "SELECT vbc.*
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal AND f.serial_fle = $serial_fle
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN view_benfits_in_cards vbc ON vbc.serial_pxc = pbd.serial_pxc";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getFilePaymentPercentage($db, $dateFrom, $dateTo, $serial_cou, $serial_lang, $expandBy = 'CUSTOMER', $uniqueId = NULL){
		switch($expandBy){
			case 'CUSTOMER':
				$display_fields = "CONCAT(c.first_name_cus, ' ', c.last_name_cus) AS 'PAX'";
				$groupBy_field = "f.serial_cus";
				
				break;
			case 'DESTINATION':
				$display_fields = "cou.name_cou AS 'destination'";
				$groupBy_field = "cou.serial_cou";
				
				break;
			case 'MANAGER':
				$display_fields = "m.name_man";
				$groupBy_field = "mbc.serial_mbc";
				
				break;
			case 'PRODUCT':
				$display_fields = "pbl.name_pbl";
				$groupBy_field = "pxc.serial_pxc";
				
				break;
			case 'DEALER_PRODUCT':
				$display_fields = "pbl.name_pbl, d.name_dea, pxc.serial_pro";
				$groupBy_field = "d.serial_dea, pxc.serial_pxc";
				$SQLclause = " AND d.serial_dea = $uniqueId";
				break;
			
			default: //DEALER
				$display_fields = "d.name_dea";
				$groupBy_field = "d.serial_dea";
				$SQLclause = " AND d.serial_dea = $uniqueId";
				break;
		}
		
		$sql = "SELECT	sum(IFNULL(amount_authorized_bbd, 0)) AS 'authorized', 
						sum(IFNULL(amount_presented_bbd, 0)) AS 'presented', 
						(sum(IFNULL(amount_authorized_bbd, 0))/ sum(IFNULL(amount_presented_bbd, 0)) ) AS 'payment_percentage', 
						$display_fields
				FROM file f
				JOIN payment_request prq ON prq.serial_fle = f.serial_fle
					AND f.status_fle <> 'void'
                                        AND prq.status_prq <> 'DENIED'
					AND f.incident_date_fle BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND STR_TO_DATE('$dateTo', '%d/%m/%Y')
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
				JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf

				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer c on c.serial_cus = f.serial_cus
				
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer d On d.serial_dea = cnt.serial_dea
					$SQLclause
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					AND pxc.serial_cou = $serial_cou
				JOIN product_by_language pbl on pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = $serial_lang

				JOIN city cit ON cit.serial_cit = f.serial_cit
				JOIN country cou on cou.serial_cou = cit.serial_cou
				GROUP BY $groupBy_field";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getAllFilesForBranchCard($db, $serial_bra, $dateFrom, $dateTo, $serial_lang){
		$sql = "SELECT	f.serial_fle, s.card_number_sal, pbl.name_pbl, CONCAT(c.first_name_cus, ' ', c.last_name_cus) AS 'name_cus',
						f.status_fle, sum(IFNULL(amount_authorized_bbd, 0)) AS 'authorized', 
						sum(IFNULL(amount_presented_bbd, 0)) AS 'presented', 
						DATE_FORMAT(creation_date_fle, '%d/%m/%Y') AS 'creation_date_fle'
				FROM file f
				JOIN payment_request prq ON prq.serial_fle = f.serial_fle
					AND f.status_fle <> 'void' 
					AND f.incident_date_fle BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND STR_TO_DATE('$dateTo', '%d/%m/%Y')
					AND prq.status_prq = 'APROVED'
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
				JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf

				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer c on c.serial_cus = f.serial_cus
				
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer d On d.serial_dea = cnt.serial_dea
					AND d.serial_dea = $serial_bra
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl on pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = $serial_lang
				GROUP BY f.serial_fle";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_fle(){
		return $this->serial_fle;
	}
	function getSerial_cit(){
		return $this->serial_cit;
	}
	function getOpening_usr(){
		return $this->opening_usr;
	}
	function getSerial_trl(){
		return $this->serial_trl;
	}
	function getSerial_cus(){
		return $this->serial_cus;
	}
	function getClosing_usr(){
		return $this->closing_usr;
	}
	function getSerial_sal(){
		return $this->serial_sal;
	}
	function getDiagnosis_fle(){
		return $this->diagnosis_fle;
	}
	function getCause_fle(){
		return $this->cause_fle;
	}
	function getStatus_fle(){
		return $this->status_fle;
	}
	function getCreation_date_fle(){
		return $this->creation_date_fle;
	}
	function getCurrency_fle(){
		return $this->currency_fle;
	}
	function getExchange_rate_fle(){
		return $this->exchange_rate_fle;
	}
	function getTotal_cost_fle(){
		return $this->total_cost_fle;
	}
	function getAuthorized_fle(){
		return $this->authorized_fle;
	}
	function getObservations_fle(){
		return $this->observations_fle;
	}
	function getEnd_date_fle(){
		return $this->end_date_fle;
	}
	function getContanct_info_fle(){
		return $this->contanct_info_fle;
	}
	function getType_fle(){
		return $this->type_fle;
	}
	function getIncident_date_fle(){
		return $this->incident_date_fle;
	}
	function getAux_data(){
		return $this->aux_data;
	}

	///SETTERS
	function setSerial_fle($serial_fle){
		$this->serial_fle = $serial_fle;
	}
	function setSerial_cit($serial_cit){
		$this->serial_cit = $serial_cit;
	}
	function setOpening_usr($opening_usr){
		$this->opening_usr = $opening_usr;
	}
	function setSerial_trl($serial_trl){
		$this->serial_trl = $serial_trl;
	}
	function setSerial_cus($serial_cus){
		$this->serial_cus = $serial_cus;
	}
	function setClosing_usr($closing_usr){
		$this->closing_usr = $closing_usr;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal = $serial_sal;
	}
	function setDiagnosis_fle($diagnosis_fle){
		$this->diagnosis_fle = utf8_decode($diagnosis_fle);
	}
	function setCause_fle($cause_fle){
		$this->cause_fle = utf8_decode($cause_fle);
	}
	function setStatus_fle($status_fle){
		$this->status_fle = $status_fle;
	}
	function setCreation_date_fle($creation_date_fle){
		$this->creation_date_fle = $creation_date_fle;
	}
	function setCurrency_fle($currency_fle){
		$this->currency_fle = $currency_fle;
	}
	function setExchange_rate_fle($exchange_rate_fle){
		$this->exchange_rate_fle = $exchange_rate_fle;
	}
	function setTotal_cost_fle($total_cost_fle){
		$this->total_cost_fle = $total_cost_fle;
	}
	function setAuthorized_fle($authorized_fle){
		$this->authorized_fle = $authorized_fle;
	}
	function setObservations_fle($observations_fle){
		$this->observations_fle = $observations_fle;
	}
	function setEnd_date_fle($end_date_fle){
		$this->end_date_fle = $end_date_fle;
	}
	function setContanct_info_fle($contanct_info_fle){
		$this->contanct_info_fle = $contanct_info_fle;
	}
	function setType_fle($type_fle){
		$this->type_fle = $type_fle;
	}
	function setIncident_date_fle($incident_date_fle){
		$this->incident_date_fle = $incident_date_fle;
	}
}
?>
