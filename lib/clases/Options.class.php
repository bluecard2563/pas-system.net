<?php
class Options {				
	var $db;
	var $serial_opt;
	var $name_opt;
	var $icon;
	
	function __construct($db, $serial_opt = NULL, $name_opt = NULL){
		$this -> db = $db;
		$this -> serial_opt = $serial_opt;
		$this -> name_opt = $name_opt;
	}
	
	function insert(){
	
		$sql = "
			INSERT INTO `options` (
				`serial_opt`, `name_opt`
			)VALUES (
				NULL,
				'".$this->name_opt."')";
				
		if($result = $this->db->Execute($sql))
			return 0;
		else{
			return mysql_errno();
		}
	}
	
	function update(){
		$sql = "
			UPDATE `options` SET 
				`serial_opt` = '".$this->serial_opt."',
				`name_opt` = '".$this->name_opt."'
			WHERE `options`.`serial_opt` = '".$this->serial_opt."'";	
		//echo $sql;		
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	function getData(){
		if($this->serial_opt!=NULL){
			$sql = 	"SELECT p.`serial_opt`, UPPER(p.`nombre_opt`)
					FROM `options` p
					WHERE p.serial_opt='".$this->serial_opt."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_opt=$result->fields[0];
				$this->name_opt=$result->fields[1];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	function getOptions(){
		$sql = 	"SELECT link_opt
				 FROM options";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->fields[0];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************************************************
	*Función getOptionsRelationship                                                       *
	*Returns a list of the parent options with an intern list with the child process
	********************************************************************************** */
	function getOptionsRelationship($code_lang){
		$this->totalOptions=0;
		//Se setea el modo en que el RecordSet recoje los datos
		//$this->db->SetFetchMode(ADODB_FETCH_NUM);

		$sql = "SELECT o.serial_opt, obl.name_obl
				FROM options o JOIN options_by_language obl
				ON o.serial_opt =  obl.serial_opt  
				JOIN language l 
				ON l.serial_lang=obl.serial_lang AND l.code_lang = '".$code_lang."'
				WHERE o.opt_serial_opt is NULL
				ORDER BY o.serial_opt";
		//echo $sql;
		$rs = $this->db -> Execute($sql);
		$numParent = $rs -> RecordCount();
		//print_r($numParent);
		if($numParent > 0){
			$arr=array();
			$cont=0;
			
			do{
				//Se almacena la información de cada registro en un renglón del arreglo.
				$arr[$cont]=$rs->GetRowAssoc(false);
				$rs->MoveNext();
				$cont++;
			}while($cont<$numParent);
		}
		
		//Debug::print_r($arr);
		//Se buscará los procesos hijos de cada parent y se los agregara como un arreglo dentro de cada proceso padre
		foreach($arr as &$a)
		{
				$sql = "SELECT o.serial_opt, obl.name_obl
				FROM options o JOIN options_by_language obl
				ON o.serial_opt =  obl.serial_opt  
				JOIN language l 
				ON l.serial_lang=obl.serial_lang AND l.code_lang = '".$code_lang."'
							
				 WHERE o.opt_serial_opt={$a['serial_opt']}
					ORDER BY o.serial_opt";
					//echo $sql;
			$rs = $this->db -> Execute($sql);
			$numChild = $rs -> RecordCount();
			
			if($numChild > 0){
				$arrC=array();
				$cont=0;
				
				do{
					//Se almacena los hijos en un arreglo llamado Child dentro de cada padre
					$arrC[$cont]=$rs->GetRowAssoc(false);
					$rs->MoveNext();
					$this->totalOptions++;
					$cont++;
				}while($cont<$numChild);
				foreach($arrC as &$ar)
					{
							$sql = "SELECT o.serial_opt, obl.name_obl
									FROM options o JOIN options_by_language obl
									ON o.serial_opt =  obl.serial_opt  
									JOIN language l 
									ON l.serial_lang=obl.serial_lang AND l.code_lang = '".$code_lang."'
								 	WHERE o.opt_serial_opt={$ar['serial_opt']}
									ORDER BY o.serial_opt";
							//	echo $sql;
						$rs = $this->db -> Execute($sql);
						$numChild2 = $rs -> RecordCount();
						
						if($numChild2 > 0){
							$arrD=array();
							$cont=0;
							$this->totalOptions--;
							do{
								//Se almacena los hijos en un arreglo llamado Child dentro de cada padre
								$arrD[$cont]=$rs->GetRowAssoc(false);
								$rs->MoveNext();
								$this->totalOptions++;
								$cont++;
							}while($cont<$numChild2);
							/********************************************************/

							foreach($arrD as &$ard)
							{
								$sql = "SELECT o.serial_opt, obl.name_obl
									FROM options o JOIN options_by_language obl
									ON o.serial_opt =  obl.serial_opt  
									JOIN language l 
									ON l.serial_lang=obl.serial_lang AND l.code_lang = '".$code_lang."'
								 	WHERE o.opt_serial_opt={$ard['serial_opt']}
									ORDER BY o.serial_opt";
							//	echo $sql;
								$rs = $this->db -> Execute($sql);
								$numChild3 = $rs -> RecordCount();

								if($numChild3 > 0){
									$arrCC=array();
									$cont2=0;
									$this->totalOptions--;
									do{
										//Se almacena los hijos en un arreglo llamado Child dentro de cada padre
										$arrCC[$cont2]=$rs->GetRowAssoc(false);
										$rs->MoveNext();
										$this->totalOptions++;
										$cont2++;
									}while($cont2<$numChild3);
									foreach($arrCC as &$arc)
									{
										$sql = "SELECT o.serial_opt, obl.name_obl
											FROM options o JOIN options_by_language obl
											ON o.serial_opt =  obl.serial_opt
											JOIN language l
											ON l.serial_lang=obl.serial_lang AND l.code_lang = '".$code_lang."'
											WHERE o.opt_serial_opt={$arc['serial_opt']}
											ORDER BY o.serial_opt";
									//	echo $sql;
										$rs = $this->db -> Execute($sql);
										$numChild4 = $rs -> RecordCount();

										if($numChild4 > 0){
											$arrCCC=array();
											$cont3=0;
											$this->totalOptions--;
											do{
												//Se almacena los hijos en un arreglo llamado Child dentro de cada padre
												$arrCCC[$cont3]=$rs->GetRowAssoc(false);
												$rs->MoveNext();
												$this->totalOptions++;
												$cont3++;
											}while($cont3<$numChild4);
											$arc['child4']=$arrCCC;
										}
									}
									$ard['child3']=$arrCC;
								}
							}
							$ar['child2']=$arrD;
						 }	 
					}
				$a['child']=$arrC;
		     }	 
		}
		return $arr;
	}
	
	function getOptionPermission($url,$profile){
		if($url){
			$sql = 	"SELECT pp.serial_opt
					FROM options_by_profile pp
					JOIN options p ON pp.serial_opt = p.serial_opt
					WHERE p.link_opt LIKE '%$url%'
					AND pp.serial_opt = $profile";
	
			$result = $this->db -> Execute($sql);
			//echo $sql;
			
			if($result->fields[0]){
				return true;
			}else{
				return false;	
			}
		}else{
			return false;	
		}
	}
	
	public static function getUsersAvailableFavoriteOptions($db, $serial_usr, $exception_options = NULL){
		$sql = "SELECT DISTINCT opt.serial_opt, obl.name_obl, opt.icon_opt
				FROM options opt
				JOIN options_by_language obl ON obl.serial_opt = opt.serial_opt AND opt.icon_opt IS NOT NULL
				JOIN opt_by_profcountry pbc ON pbc.serial_opt = opt.serial_opt
				JOIN user u ON u.serial_pbc = pbc.serial_pbc AND u.serial_usr = $serial_usr";
		
		if($exception_options){
			$sql .= " AND opt.serial_opt NOT IN ($exception_options)";
		}
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	
	///GETTERS
	function getSerial_opt(){
		return $this->serial_opt;
	}
	function getName_opt(){
		return $this->name_opt;
	}
	///SETTERS
	function setSerial_opt($serial_opt){
		$this->serial_opt = $serial_opt;
	}
	function setName_opt($name_opt){
		$this->name_opt = $name_opt;
	}
}
?>
