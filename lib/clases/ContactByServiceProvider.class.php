<?php
/*
File: ContactByServiceProvider.class.php
Author: Santiago Ben�tez
Creation Date: 18/02/2010 10:02
Creation Date: 18/02/2010 10:02
Modified By: Santiago Ben�tez
*/
class ContactByServiceProvider{
	var $db;
	var $serial_csp;
	var $serial_spv;
	var $serial_cit;	
	var $first_name_csp;
	var $last_name_csp;
	var $phone_csp;	
	var $email_csp;
	var $type_csp;
	var $status_csp;
	var $aux_data;
	var $email2_csp;

	function __construct($db, $serial_csp=NULL, $serial_spv=NULL, $serial_cit=NULL, $first_name_csp=NULL,
						 $last_name_csp=NULL, $phone_csp=NULL, $email_csp=NULL, $type_csp=NULL,
						 $status_csp=NULL, $email_csp=NULL){

		$this -> db = $db;
		$this -> serial_csp=$serial_csp;
		$this -> serial_spv=$serial_spv;
		$this -> serial_cit=$serial_cit;		
		$this -> first_name_csp=$first_name_csp;
		$this -> last_name_csp=$last_name_csp;
		$this -> phone_csp=$phone_csp;
		$this -> email_csp=$email_csp;
		$this -> type_csp=$type_csp;
		$this -> status_csp=$status_csp;
		$this -> email2_csp=$email2_csp;
	}

	/**
	@First_name: getData
	@Description: Retrieves the data of a Last_namesByServiceProvider object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_csp!=NULL){
			$sql = 	"SELECT csp.serial_csp, csp.serial_spv, csp.serial_cit, csp.first_name_csp, 
                                        csp.last_name_csp, csp.phone_csp, csp.email_csp, csp.type_csp, csp.status_csp,
                                        cou.serial_cou, z.serial_zon, coup.serial_cou as prov_serial_cou, 
										zonp.serial_zon as prov_serial_zon, csp.email2_csp
					FROM contacts_by_service_provider csp
					JOIN city c ON c.serial_cit=csp.serial_cit
					JOIN country cou ON cou.serial_cou=c.serial_cou
					JOIN zone z ON z.serial_zon=cou.serial_zon
					JOIN service_provider p ON p.serial_spv=csp.serial_spv
					JOIN city citp ON p.serial_cit=citp.serial_cit
					JOIN country coup ON citp.serial_cou=coup.serial_cou
					JOIN zone zonp ON zonp.serial_zon=coup.serial_zon
					WHERE serial_csp='".$this->serial_csp."'";
                        //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_csp=$result->fields[0];
				$this -> serial_spv=$result->fields[1];
				$this -> serial_cit=$result->fields[2];
				$this -> first_name_csp=$result->fields[3];
				$this -> last_name_csp=$result->fields[4];
				$this -> phone_csp=$result->fields[5];
				$this -> email_csp=$result->fields[6];
				$this -> type_csp=$result->fields[7];
				$this -> status_csp=$result->fields[8];
				$this -> aux_data['serial_cou']=$result->fields[9];
				$this -> aux_data['serial_zon']=$result->fields[10];
				$this -> aux_data['prov_serial_cou']=$result->fields[11];
				$this -> aux_data['prov_serial_zon']=$result->fields[12];
				$this -> email2_csp=$result->fields[13];

				return true;
			}else
				return false;
		}else
			return false;
	}
	/**
	@First_name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: serial_csp if OK. / FALSE on error
	**/
    function insert() {
        $sql = "
            INSERT INTO contacts_by_service_provider (
				serial_csp,				
				serial_spv,
				serial_cit,
				first_name_csp,
				last_name_csp,
				phone_csp,
				email_csp,
				type_csp,
				status_csp,
				email2_csp)
            VALUES (
                NULL,";
		if($this->serial_spv=='NULL'){
			$sql.="NULL,";
		}else{
			$sql.="'".$this->serial_spv."',";
		}
			$sql.="'".$this->serial_cit."',
				'".$this->first_name_csp."',
				'".$this->last_name_csp."',
				'".$this->phone_csp."',
				'".$this->email_csp."',
				'".$this->type_csp."',
				'ACTIVE',";
		if($this->email_csp){
			$sql.="NULL)";
		}else{
			$sql.="'".$this->serial_spv."')";
		}
				
        //die($sql);
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/**
	@First_name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "
                UPDATE contacts_by_service_provider SET
					serial_cit='".$this->serial_cit."',";
		$sql.="serial_spv=";
		if($this->serial_spv=='NULL' || $this->serial_spv==''){
			$sql.="NULL,";
		}else{
			$sql.="'".$this->serial_spv."',";
		}
		 $sql.="	first_name_csp='".$this->first_name_csp."',
					last_name_csp='".$this->last_name_csp."',
					phone_csp='".$this->phone_csp."',
					email_csp='".$this->email_csp."',
					type_csp='".$this->type_csp."',
					status_csp='".$this->status_csp."',
					email2_csp='".$this->email2_csp."'
				WHERE serial_csp = '".$this->serial_csp."'";
                                //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
    * funcion getProvidersByCountry
    * gets all the providers in a specific country of the system
    ***********************************************/
        function getContactsByServiceProvider($serial_spv){
            $sql =	"SELECT c.serial_csp, CONCAT( c.first_name_csp, ' ', c.last_name_csp ) name_csp
                     FROM contacts_by_service_provider c
					 JOIN service_provider p ON p.serial_spv=c.serial_spv AND p.serial_spv ='".$serial_spv."'
					ORDER BY name_csp";
            //die($sql);
            $result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
            if($num > 0){
				$arreglo=array();
				$cont=0;

				do{
						$arreglo[$cont]=$result->GetRowAssoc(false);
						$result->MoveNext();
						$cont++;
				}while($cont<$num);
            }

            return $arreglo;
    }

    /********************************************************
    * funcion getContactsByServiceProviderInfo
    * gets all the contacts from a specific Service Provider
    *********************************************************/
        function getContactsByServiceProviderInfo($serial_spv, $type_csp=NULL){
            $sql = "SELECT csp.serial_csp, cou.name_cou, cit.name_cit, CONCAT( csp.first_name_csp, ' ', csp.last_name_csp ) name_csp,
                           csp.phone_csp, csp.email_csp, csp.type_csp, csp.email2_csp
                    FROM contacts_by_service_provider csp
                    JOIN city cit ON cit.serial_cit = csp.serial_cit
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    WHERE csp.serial_spv='".$serial_spv."'
                    AND csp.status_csp='ACTIVE'";
            if($type_csp!=NULL) {
                $sql .= " AND csp.type_csp='".$type_csp."'";
            }
            $sql .= "ORDER BY cou.name_cou, cit.name_cit";
            //die($sql);
            $result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
            if($num > 0){
				$arreglo=array();
				$cont=0;

				do{
						$arreglo[$cont]=$result->GetRowAssoc(false);
						$result->MoveNext();
						$cont++;
				}while($cont<$num);
            }

            return $arreglo;
    }

	/***********************************************
    * funcion contactsByServiceProviderEmailExists
    * verifies if a contactsByServiceProvider's document exists or not
    ***********************************************/
	function contactByServiceProviderEmailExists($email_csp, $serial_spv, $serial_csp=NULL){
		$sql = 	"SELECT serial_csp
				 FROM contacts_by_service_provider
				 WHERE (LOWER (email_csp) = '".utf8_encode($email_csp)."'
				 OR LOWER (email2_csp) = '".utf8_encode($email_csp)."')
				 AND serial_spv <> $serial_spv";
		
		if($serial_csp){
			$sql.=" AND serial_csp <> ".$serial_csp;
		}
		
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@First_name: getContactsByServiceProvidersAutocompleter
	@Description: Returns an array of contacts_by_service_providers.
	@Params: Last_namesByServiceProvider first_name or pattern.
	@Returns: Last_namesByServiceProvider array
	**/
	function getContactsByServiceProviderAutocompleter($name_csp){
		$sql = "SELECT serial_csp, first_name_csp, last_name_csp, email_csp
			FROM contacts_by_service_provider
			WHERE (LOWER(first_name_csp) LIKE _utf8'%".utf8_encode($name_csp)."%' collate utf8_bin
			OR LOWER(last_name_csp) LIKE _utf8'%".utf8_encode($name_csp)."%' collate utf8_bin
			OR LOWER(email_csp) LIKE _utf8'%".utf8_encode($name_csp)."%' collate utf8_bin)
			LIMIT 10";
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/**
	@First_name: getAllTypes
	@Description: Gets all contacts_by_service_provider types in DB.
	@Params: N/A
	@Returns: Type array
	**/
	function getAllTypes(){
		$sql = "SHOW COLUMNS FROM contacts_by_service_provider LIKE 'type_csp'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

        /**
	@First_name: getAllStatus
	@Description: Gets all contacts_by_service_provider types in DB.
	@Params: N/A
	@Returns: Status array
	**/
	function getAllStatus(){
		$sql = "SHOW COLUMNS FROM contacts_by_service_provider LIKE 'status_csp'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	//Getters
	function getSerial_csp(){
		return $this->serial_csp;
	}
	function getSerial_spv(){
		return $this->serial_spv;
	}
	function getSerial_cit(){
		return $this->serial_cit;
	}
	function getFirst_name_csp(){
		return $this->first_name_csp;
	}
	function getLast_name_csp(){
		return $this->last_name_csp;
	}
	function getPhone_csp(){
		return $this->phone_csp;
	}
	function getEmail_csp(){
		return $this->email_csp;
	}
	function getEmail2_csp(){
		return $this->email2_csp;
	}
	function getType_csp(){
		return $this->type_csp;
	}
	function getStatus_csp(){
		return $this->status_csp;
	}
	function getAux_data(){
		return $this->aux_data;
	}

	//Setters
	function setSerial_csp($serial_csp){
		$this->serial_csp=$serial_csp;
	}
	function setSerial_spv($serial_spv){
		$this->serial_spv=$serial_spv;
	}
	function setSerial_cit($serial_cit){
		$this->serial_cit=$serial_cit;
	}
	function setFirst_name_csp($first_name_csp){
		$this->first_name_csp=$first_name_csp;
	}
	function setLast_name_csp($last_name_csp){
		$this->last_name_csp=$last_name_csp;
	}
	function setPhone_csp($phone_csp){
		$this->phone_csp=$phone_csp;
	}
	function setEmail_csp($email_csp){
		$this->email_csp=$email_csp;
	}
	function setEmail2_csp($email2_csp){
		$this->email2_csp=$email2_csp;
	}
	function setType_csp($type_csp){
		$this->type_csp=$type_csp;
	}
	function setStatus_csp($status_csp){
		$this->status_csp=$status_csp;
	}
}
?>