<?php

/**
 * Author: Patricio Astudillo
 * Modified date: 17/07/2012
 * Last modified: 17/07/2012
 * Modified BY: Nicolas Flores
 */
class Vcard {

    var $log;
    var $data;  //array of this vcard's contact data
    var $filename; //filename for download file naming
    var $class; //PUBLIC, PRIVATE, CONFIDENTIAL
    var $revision_date;
    var $card;

    function vcard() {
        $this->log = "New vcard() called<br />";
        $this->data = array(
            "display_name" => null
            , "first_name" => null
            , "last_name" => null
            , "additional_name" => null
            , "name_prefix" => null
            , "name_suffix" => null
            , "nickname" => null
            , "title" => null
            , "role" => null
            , "department" => null
            , "company" => null
            , "work_po_box" => null
            , "work_extended_address" => null
            , "work_address" => null
            , "work_city" => null
            , "work_state" => null
            , "work_postal_code" => null
            , "work_country" => null
            , "home_po_box" => null
            , "home_extended_address" => null
            , "home_address" => null
            , "home_city" => null
            , "home_state" => null
            , "home_postal_code" => null
            , "home_country" => null
            , "office_tel" => null
            , "home_tel" => null
            , "cell_tel" => null
            , "fax_tel" => null
            , "pager_tel" => null
            , "email1" => null
            , "email2" => null
            , "url" => null
            , "photo" => null
            , "birthday" => null
            , "timezone" => null
            , "sort_string" => null
            , "note" => null
        );
        return true;
    }

    /*
      build() method checks all the values, builds appropriate defaults for
      missing values, generates the vcard data string.
     */
    function build() {
        $this->card = "BEGIN:VCARD\r\n";
        $this->card .= "VERSION:3.0\r\n";
        $this->card .= "N:"
                . $this->data['last_name'] . ";"
                . $this->data['first_name'] . ";"
                . $this->data['additional_name'] . ";"
                . $this->data['name_prefix'] . ";"
                . $this->data['name_suffix'] . "\r\n";
        
        if ($this->data['title']) {
            $this->card .= "TITLE:" . $this->data['title'] . "\r\n";
        }
        if ($this->data['company']) {
            $this->card .= "ORG:" . $this->data['company'];
        }
        $this->card .= "\r\n";
        
       /* if ($this->data['work_address'] || $this->data['work_city']) {
            $this->card .= "ADR;WORK:;;"
            .$this->data['work_address'].";"
            .$this->data['work_city']."\r\n";
            $this->card .= "LABEL;WORK;ENCODING=QUOTED-PRINTABLE:"
            .$this->data['work_address'].","
            .$this->data['work_city']."\r\n";
        }*/

        if ($this->data['email1']) {
            $this->card .= "EMAIL;TYPE=internet,pref:" . $this->data['email1'] . "\r\n";
        }

        if ($this->data['office_tel']) {
            $this->card .= "TEL;TYPE=work,voice:" . $this->data['office_tel'] . "\r\n";
        }
        
        if ($this->data['cell_tel']) {
            $this->card .= "TEL;TYPE=cell,voice:" . $this->data['cell_tel'] . "\r\n";
        }
        if ($this->data['fax_tel']) {
            $this->card .= "TEL;TYPE=work,fax:" . $this->data['fax_tel'] . "\r\n";
        }
        
        if ($this->data['url']) {
            $this->card .= "URL;TYPE=work:" . $this->data['url'] . "\r\n";
        }
        if ($this->data['birthday']) {
            $this->card .= "BDAY:" . $this->data['birthday'] . "\r\n";
        }
        $this->card .= "END:VCARD\r\n";
    }

    /*
      download() method streams the vcard to the browser client.
     */
    function download() {
        $this->log .= "vcard download() called<br />";
        if (!$this->card) {
            $this->build();
        }
        if (!$this->filename) {
            $this->filename = trim($this->data['display_name']);
        }
        $this->filename = str_replace(" ", "_", $this->filename);
        header("Content-type: text/directory");
        header("Content-Disposition: attachment; filename=" . $this->filename . ".vcf");
        header("Pragma: public");
        echo $this->card;
        return true;
    }

}

?>
