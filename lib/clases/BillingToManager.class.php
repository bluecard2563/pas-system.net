<?php
/*
File: BillingToManager
Author: David Bergmann
Creation Date: 04/10/2010
Last Modified:
Modified By:
*/


class BillingToManager {
	var $db;
	var $serial_btm;
	var $serial_man;
	var $date_btm;
	var $amount_btm;
	var $billing_number_btm;
	var $discount_percentage_btm;
	var $other_discount_btm;
	var $status_btm;
	var $taxes_btm;
	var $type_btm;
	var $aux;
	var $billing_date_btm;
	var $serial_usr;
	var $invoiced_usr;

	function __construct($db, $serial_btm = NULL, $serial_man = NULL,$date_btm = NULL,
						 $amount_btm = NULL,$billing_number_btm = NULL,$discount_percentage_btm = NULL,
						 $other_discount_btm = NULL,$status_btm = NULL,$taxes_btm = NULL,$type_btm = NULL){
		$this -> db = $db;
		$this -> serial_btm = $serial_btm;
		$this -> serial_man = $serial_man;
		$this -> date_btm = $date_btm;
		$this -> amount_btm = $amount_btm;
		$this -> billing_number_btm = $billing_number_btm;
		$this -> discount_percentage_btm = $discount_percentage_btm;
		$this -> other_discount_btm = $other_discount_btm;
		$this -> status_btm = $status_btm;
		$this -> taxes_btm = $taxes_btm;
		$this -> type_btm = $type_btm;
	}

	/***********************************************
	* function getData
	* gets data by serial_btm
	***********************************************/
	function getData(){
		if($this->serial_btm!=NULL){
			$sql = "SELECT btm.serial_btm, btm.serial_man,  DATE_FORMAT(btm.date_btm,'%d/%m/%Y'), btm.amount_btm,
							btm.billing_number_btm, btm.discount_percentage_btm, btm.other_discount_btm,
							btm.status_btm, btm.taxes_btm, m.name_man,btm.type_btm,
							DATE_FORMAT(billing_date_btm,'%d/%m/%Y'), serial_usr, invoiced_usr
					FROM billing_to_manager btm
					JOIN manager m ON m.serial_man = btm.serial_man
					WHERE serial_btm='$this->serial_btm'";
			//echo $sql." ";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_btm=$result->fields[0];
				$this -> serial_man=$result->fields[1];
				$this -> date_btm=$result->fields[2];
				$this -> amount_btm=$result->fields[3];
				$this -> billing_number_btm=$result->fields[4];
				$this -> discount_percentage_btm=$result->fields[5];
				$this -> other_discount_btm=$result->fields[6];
				$this -> status_btm=$result->fields[7];
				$this -> taxes_btm=$result->fields[8];
				$this -> aux['name_man'] = $result->fields[9];
				$this -> type_btm = $result->fields[10];
				$this -> billing_date_btm = $result->fields[11];
				$this -> serial_usr = $result->fields[12];
				$this -> invoiced_usr = $result->fields[13];
				return true;
			}
			else
				return false;
		}else
			return false;
	}

	/***********************************************
	* function insert
	* inserts a new register in DB
	***********************************************/
	function insert(){

		$sql = "INSERT INTO billing_to_manager (
					serial_btm,
					serial_man,
					date_btm,
					billing_date_btm,
					amount_btm,
					billing_number_btm,
					discount_percentage_btm,
					other_discount_btm,
					status_btm,
					taxes_btm,
					type_btm,
					serial_usr, 
					invoiced_usr
					)
				VALUES(NULL,
					'$this->serial_man',";
		$sql .= ($this -> date_btm)?" STR_TO_DATE('{$this -> date_btm}', '%d/%m/%Y'),":"NOW(),";
		$sql.="		NULL,
					'$this->amount_btm',
					'$this->billing_number_btm',
					'$this->discount_percentage_btm',
					'$this->other_discount_btm',
					'PRINTED',
					'$this->taxes_btm', ";
		$sql.=($this -> type_btm)? "'{$this -> type_btm}',":"'MANAGER',";		
		$sql.="		'$this->serial_usr',
					NULL);";
		

		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

 	/***********************************************
	* funcion update
	* updates a register in DB
	***********************************************/
	function update(){
		$sql = "UPDATE billing_to_manager
				SET amount_btm='$this->amount_btm',
					billing_number_btm='$this->billing_number_btm',
					discount_percentage_btm='$this->discount_percentage_btm',
					other_discount_btm='$this->other_discount_btm',
					status_btm='$this->status_btm',
					taxes_btm='$this->taxes_btm',
					type_btm='$this->type_btm',
					billing_date_btm = CURRENT_TIMESTAMP(),
					invoiced_usr = '$this->invoiced_usr'
				WHERE serial_btm='$this->serial_btm'";

		//Debug::print_r($sql); die;
		$result = $this->db->Execute($sql);
		
		if ($result==true)
			return true;
		else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
			return false;
		}	
	}
	
	/***********************************************
	* funcion getLastBillToManager
	* returns last billing date if exists
	***********************************************/
	function getLastBillToManager($serial_man, $serial_btm = NULL){
		$sql = "SELECT DATE_FORMAT(MAX(date_btm), '%d/%m/%Y') as date_btm
				FROM billing_to_manager
				WHERE serial_man = '$serial_man'";

		if($serial_btm != NULL) {
			$sql .= " AND serial_btm <> '$serial_btm'";
		}
		$result = $this->db->getOne($sql);
		if($result){
			return $result;
		} else {
			ErrorLog::log($this->db, 'LastBillManager - '.get_class($this), $sql);
			return false;
		}
	}

	/***********************************************
	* funcion getBillingToManager
	* returns last billing info if exists
	***********************************************/
	public static function getBillingToManager($db, $serial_man){
		$sql = "SELECT serial_btm, DATE_FORMAT(date_btm,'%d/%m/%Y') as date_btm, amount_btm, billing_number_btm
				FROM billing_to_manager
				WHERE serial_man = '$serial_man'
				AND status_btm = 'PRINTED'
				AND type_btm = 'MANAGER'
				ORDER BY billing_number_btm";
		$result = $db->getAll($sql);

		if($result){
			return $result[0];
		} else {
			return false;
		}
	}
	/***********************************************
	* funcion getBillingToManagerByCountry
	* returns last billing info by country if exists
	***********************************************/
	public static function getBillingToManagerByCountry($db, $serial_cou){
		$sql = "SELECT btm.billing_number_btm,man.name_man,SUM(btm.amount_btm) as amount_btm,DATE_FORMAT(btm.date_btm,'%d/%m/%Y') as date_btm
				FROM billing_to_manager btm
				JOIN manager man ON man.serial_man = btm.serial_man
				JOIN city cit ON man.serial_cit = cit.serial_cit
				JOIN country cou ON cit.serial_cou = cou.serial_cou
				WHERE btm.status_btm = 'PRINTED'
				AND btm.type_btm = 'COUNTRY'
				AND cou.serial_cou= $serial_cou
				GROUP BY btm.billing_number_btm,man.name_man WITH ROLLUP";
		$result = $db->getAll($sql);
		
		if($result){
			unset($result[sizeof($result)-1]);
			return $result;
		} else {
			return false;
		}
	}
	/***********************************************
	* funcion getManagersWithPrintedInvoiceByCountry
	* returns managers and serial_btm of pending invoices by country 
	***********************************************/
	public static function getManagersWithPrintedInvoiceByCountry($db,$serial_cou,$billing_number_btm){
		$sql = "SELECT btm.serial_man,btm.serial_btm 
				FROM billing_to_manager btm
				JOIN manager man ON man.serial_man = btm.serial_man
				JOIN city cit ON man.serial_cit = cit.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou
				WHERE cou.serial_cou = {$serial_cou}
				AND btm.billing_number_btm = {$billing_number_btm}
				AND type_btm = 'COUNTRY'
				AND status_btm = 'PRINTED';";
		$result = $db->getAll($sql);
		if($result){
            return $result;
        }else{
            return false;
        }
	}

	/***********************************************
	* funcion hasPendingBills
	* checks if the manager has pending bills
	***********************************************/
	public static function hasPendingBills($db, $serial_man){
		$sql = "SELECT serial_man
				FROM billing_to_manager
				WHERE serial_man = '$serial_man'
				AND status_btm = 'PRINTED'";
		$result = $db->getOne($sql);

		if($result){
			return true;
		} else {
			return false;
		}
	}
	/***********************************************
	* funcion hasPendingBillsByCOuntry
	* checks if there are managers with pending bills in a country
	***********************************************/
	public static function hasPendingBillsByCountry($db, $serial_cou){
		$sql = "SELECT btm.serial_man, mbc.serial_cou
				FROM billing_to_manager btm
				JOIN manager_by_country mbc ON btm.serial_man = mbc.serial_man
				WHERE mbc.serial_cou = '$serial_cou'
				AND status_btm = 'PRINTED'";
		$result = $db->getOne($sql);

		if($result){
			return true;
		} else {
			return false;
		}
	}

	/***********************************************
	* funcion billingNumberExists
	* checks if the billing number already exists
	***********************************************/
	public static function billingNumberExists($db, $billing_number){
		$sql = "SELECT serial_btm
				FROM billing_to_manager
				WHERE billing_number_btm = '$billing_number'";

		$result = $db->getOne($sql);

		if($result){
			return true;
		} else {
			return false;
		}
	}
	
	/***********************************************
	* funcion checkLastBillingDate
	* checks if the recieved date is older than the last billing date of a manager.
	***********************************************/
	public static function checkLastBillingDate($db, $serial_man,$date){
		$sql = "SELECT 1
				FROM billing_to_manager
				WHERE serial_man = '$serial_man'
				AND date_btm > STR_TO_DATE('$date','%d/%m/%Y')";

		$result = $db->getOne($sql);

		if($result){
			return true;
		} else {
			return false;
		}
	}
	
	public static function checkBillForReprint($db, $serial_man, $invoice_number, $bill_to = 'MANAGER'){
		$sql = "SELECT serial_btm
				FROM billing_to_manager
				WHERE serial_man = {$serial_man}
				AND billing_number_btm = '$invoice_number'
				AND type_btm = '$bill_to'
				AND status_btm = 'PAID'";
		
		$result = $db -> getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getPreviousVoidedAndRefundedSales($db, $serial_man, $dateFrom, $dateTo, $orderBy){
		$sql = "	SELECT	dea.name_dea, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as in_date_sal, s.serial_sal, s.card_number_sal,
							CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
							pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal, DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
							s.days_sal, s.total_sal, CONCAT(usrC.first_name_usr,' ',usrC.last_name_usr) as name_usr,
							s.international_fee_amount, s.international_fee_status, s.status_sal
					FROM sales s 
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN user usrC ON usrC.serial_usr = cnt.serial_usr
					JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.serial_man = {$serial_man}
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal
					JOIN user usr ON usr.serial_usr = slg.usr_serial_usr
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}

					WHERE s.status_sal = 'VOID'
					AND s.international_fee_status IN ('TO_REFUND')
					AND (slg.type_slg = 'VOID_INVOICE' OR slg.type_slg = 'VOID_SALE')
					AND slg.status_slg = 'AUTHORIZED'
					AND STR_TO_DATE(DATE_FORMAT(slg.date_slg, '%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') 
						AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')
					GROUP BY slg.serial_sal
					
				UNION

					SELECT dea.name_dea, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as in_date_sal, s.serial_sal, s.card_number_sal,
							CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
							pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal, DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,
							s.days_sal, s.total_sal, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr,
							s.international_fee_amount, s.international_fee_status, s.status_sal
					FROM sales s 
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN user usr ON usr.serial_usr = cnt.serial_usr
					JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.serial_man = {$serial_man}
					JOIN refund r ON r.serial_sal = s.serial_sal
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal AND slg.type_slg = 'REFUNDED' AND slg.status_slg = 'AUTHORIZED'
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
						
					WHERE s.status_sal = 'REFUNDED'
					AND s.international_fee_status IN ('TO_REFUND')
					AND STR_TO_DATE(DATE_FORMAT(r.auth_date_ref, '%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') 
						AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')
					ORDER BY $orderBy";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_btm(){
		return $this->serial_btm;
	}
	function getSerial_man(){
		return $this->serial_man;
	}
	function getDate_btm(){
		return $this->date_btm;
	}
	function getAmount_btm(){
		return $this->amount_btm;
	}
	function getBilling_number_btm(){
		return $this->billing_number_btm;
	}
	function getDiscount_percentage_btm(){
		return $this->discount_percentage_btm;
	}
	function getOther_discount_btm(){
		return $this->other_discount_btm;
	}
	function getStatus_btm(){
		return $this->status_btm;
	}
	function getTaxes_btm(){
		return $this->taxes_btm;
	}
	function getType_btm(){
		return $this->type_btm;
	}
	
	public function setBilling_date_btm($billing_date_btm) {
		$this->billing_date_btm = $billing_date_btm;
	}
	
	public function setSerial_usr($serial_usr) {
		$this->serial_usr = $serial_usr;
	}
	
	public function setInvoiced_usr($invoiced_usr) {
		$this->invoiced_usr = $invoiced_usr;
	}
	
	///SETTERS
	function setSerial_btm($serial_btm){
		$this->serial_btm = $serial_btm;
	}
	function setSerial_man($serial_man){
		$this->serial_man = $serial_man;
	}
	function setDate_btm($date_btm){
		$this->date_btm = $date_btm;
	}
	function setAmount_btm($amount_btm){
		$this->amount_btm = $amount_btm;
	}
	function setBilling_number_btm($billing_number_btm){
		$this->billing_number_btm = $billing_number_btm;
	}
	function setDiscount_percentage_btm($discount_percentage_btm){
		$this->discount_percentage_btm = $discount_percentage_btm;
	}
	function setOther_discount_btm($other_discount_btm){
		$this->other_discount_btm = $other_discount_btm;
	}
	function setStatus_btm($status_btm){
		$this->status_btm = $status_btm;
	}
	function setTaxes_btm($taxes_btm){
		$this->taxes_btm = $taxes_btm;
	}
	function setType_btm($type_btm){
		$this->type_btm = $type_btm;
	}
	public function getAux() {
	 return $this->aux;
	}

	public function setAux($aux) {
	 $this->aux = $aux;
	}
	
	public function getBilling_date_btm() {
		return $this->billing_date_btm;
	}

	public function getSerial_usr() {
		return $this->serial_usr;
	}

	public function getInvoiced_usr() {
		return $this->invoiced_usr;
	}

}
?>