<?php
/*
File: Invoice.class.php
Author: Santiago Benitez
Creation Date: 19/03/2010 13:52
Last Modified: 02/10/2010 13:52
Modified By: Santiago Borja
*/
class Invoice{
	var $db;
	var $serial_inv;
	var $serial_cus;
	var $serial_pay;
	var $serial_dea;
	var $serial_dbm;
	var $date_inv;
	var $due_date_inv;
	var $number_inv;
	var $discount_prcg_inv;
	var $comision_prcg_inv;
	var $other_dscnt_inv;
	var $comments_inv;
	var $printed_inv;
	var $status_inv;
	var $subtotal_inv;
	var $total_inv;
	var $has_credit_note_inv;
	var $applied_taxes_inv;
	var $erp_id;
	var $auxData;
	var $payphone_id;

	function __construct($db, $serial_inv=NULL, $serial_cus=NULL, $serial_pay=NULL, $serial_dea=NULL, $serial_dbm=NULL, $date_inv=NULL, 
			             $due_date_inv=NULL, $number_inv=NULL, $discount_prcg_inv=NULL, $comision_prcg_inv=NULL, $other_dscnt_inv=NULL,
			             $comments_inv=NULL, $printed_inv=NULL, $status_inv=NULL, $subtotal_inv=NULL, $total_inv=NULL, $has_credit_note_inv=NULL,
			             $applied_taxes_inv=NULL ){
		$this -> db = $db;
		$this -> serial_inv = $serial_inv;
		$this -> serial_cus = $serial_cus;
		$this -> serial_pay = $serial_pay;
		$this -> serial_dea = $serial_dea;
		$this -> serial_dbm = $serial_dbm;
		$this -> date_inv = $date_inv;
		$this -> due_date_inv = $due_date_inv;
		$this -> number_inv = $number_inv;
		$this -> discount_prcg_inv = $discount_prcg_inv;
		$this -> comision_prcg_inv = $comision_prcg_inv;
		$this -> other_dscnt_inv = $other_dscnt_inv;
		$this -> comments_inv = $comments_inv;
		$this -> printed_inv = $printed_inv;
		$this -> status_inv = $status_inv;
		$this -> subtotal_inv = $subtotal_inv;
		$this -> total_inv = $total_inv;
		$this -> has_credit_note_inv = $has_credit_note_inv;
		$this -> applied_taxes_inv = $applied_taxes_inv;	
    }


	/***********************************************
	* function changeStatus
	* change the status of an specific invoice
	***********************************************/
	function changeStatus($status){
		$sql="UPDATE invoice SET status_inv='".$status."', has_credit_note_inv='YES'
			  WHERE serial_inv=".$this->serial_inv;
		$result = $this->db->Execute($sql);
		if ($result === false) return false;

		if($result==true)
			return true;
		else
			return false;
	}
        
	/***********************************************
    * funcion getData
    * sets data by serial
    ***********************************************/
	function getData(){
		if($this->serial_inv!=NULL){
			$sql = 	"SELECT c.serial_inv,
							c.serial_cus,
							c.serial_pay,
							c.serial_dea,
							c.serial_dbm,
							DATE_FORMAT(c.date_inv,'%d/%m/%Y'),
							DATE_FORMAT(c.due_date_inv,'%d/%m/%Y'),
							c.number_inv,
							c.discount_prcg_inv,
							c.comision_prcg_inv,
							c.other_dscnt_inv,
							c.comments_inv,
							c.printed_inv,
							c.status_inv,
							c.subtotal_inv,
							c.total_inv,
							c.has_credit_note_inv,
							c.applied_taxes_inv,
							cus.type_cus,
							IF(cus.serial_cus IS NULL, d.name_dea, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,''))),
							c.erp_id,
							dbm.serial_man,
							c.payphone_id
					FROM invoice c
					JOIN document_by_manager dbm ON dbm.serial_dbm = c.serial_dbm
					LEFT JOIN dealer d ON d.serial_dea=c.serial_dea
					LEFT JOIN customer cus ON cus.serial_cus=c.serial_cus
					WHERE c.serial_inv='".$this->serial_inv."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_inv =$result->fields[0];
				$this -> serial_cus = $result->fields[1];
				$this -> serial_pay = $result->fields[2];
				$this -> serial_dea = $result->fields[3];
				$this -> serial_dbm = $result->fields[4];
				$this -> date_inv = $result->fields[5];
				$this -> due_date_inv = $result->fields[6];
				$this -> number_inv = $result->fields[7];
				$this -> discount_prcg_inv = $result->fields[8];
				$this -> comision_prcg_inv = $result->fields[9];
				$this -> other_dscnt_inv = $result->fields[10];
				$this -> comments_inv = $result->fields[11];
				$this -> printed_inv = $result->fields[12];
				$this -> status_inv = $result->fields[13];
				$this -> subtotal_inv = $result->fields[14];
				$this -> total_inv = $result->fields[15];
				$this -> has_credit_note_inv = $result->fields[16];
				$this -> applied_taxes_inv = $result->fields[17];
				$this -> aux['type_cus'] = $result->fields[18];
				$this -> aux['name_cus'] = $result->fields[19];
				$this -> erp_id = $result->fields[20];
				$this -> aux['serial_man'] = $result->fields[21];
				$this -> payphone_id = $result->fields[22];
				
				return true;
			}
			else
				return false;

		}else
			return false;
	}

        /***********************************************
    * funcion getSerialMbc()
    * gets serial_mbc
    ***********************************************/
	function getSerialMbc(){
		if($this->serial_inv!=NULL){
			$sql="SELECT d.serial_mbc
					FROM invoice i
					JOIN sales s ON s.serial_inv=i.serial_inv
					JOIN counter c ON c.serial_cnt=s.serial_cnt
					JOIN dealer d ON d.serial_dea=c.serial_dea
					WHERE i.serial_inv='".$this->serial_inv."'";

			$result = $this->db->Execute($sql);
                        return $result->GetRowAssoc(false);
		}else
			return false;
	}

	/***********************************************
    * funcion getInvoices
    * gets information from Invoice
    ***********************************************/
	function getInvoices(){
		$sql = 	"SELECT serial_inv, serial_cus, serial_pay, serial_dea, serial_dbm, date_inv, due_date_inv,
						number_inv, discount_prcg_inv, comision_prcg_inv, other_dscnt_inv, comments_inv, printed_inv, status_inv,
						subtotal_inv, total_inv, erp_id
				 FROM invoice ";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}

    

	/***********************************************
    * funcion getLastSerial
    * gets the serial from the last invoice registered
    ***********************************************/
	function getLastSerial(){
		$sql = 	"SELECT MAX(serial_inv) as serial
				 FROM invoice ";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr[0]['serial'];
	}

/***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO invoice (
				serial_inv,
				serial_cus,
				serial_pay,
				serial_dea,
				serial_dbm,
				date_inv,
				due_date_inv,
				number_inv,
				discount_prcg_inv,
				comision_prcg_inv,
				other_dscnt_inv,
				comments_inv,
				printed_inv,
				status_inv,
				subtotal_inv,
				total_inv,
				has_credit_note_inv,
				applied_taxes_inv,
				erp_id,
                                payphone_id)
            VALUES(
				NULL,
        		".($this->serial_cus?"'".$this->serial_cus."'":"NULL").",
				".($this->serial_pay?"'".$this->serial_pay."'":"NULL").",
				".($this->serial_dea?"'".$this->serial_dea."'":"NULL").",
				'".$this->serial_dbm."',
				STR_TO_DATE('".$this->date_inv."','%d/%m/%Y'),
				STR_TO_DATE('".$this->due_date_inv."','%d/%m/%Y'),
				'".$this->number_inv."',
				".($this->discount_prcg_inv != NULL?"'".$this->discount_prcg_inv."'":'NULL').",
				".($this->comision_prcg_inv != NULL?"'".$this->comision_prcg_inv."'":'NULL').",
				".($this->other_dscnt_inv != NULL?"'".$this->other_dscnt_inv."'":'NULL').",
				'".$this->comments_inv."',
				'NO',
				".($this->status_inv?"'".$this->status_inv."'":"'STAND-BY'").",
				'".$this->subtotal_inv."',
				'".$this->total_inv."',
                'NO',";
			if($this->applied_taxes_inv){
				$sql.="'".$this->applied_taxes_inv."',";
			}else{
				$sql.="NULL,";
			}
				$sql .= ($this->erp_id?"'".$this->erp_id."'":"NULL");
				$sql .= ($this->payphone_id?",'".$this->payphone_id."'":",NULL");
			$sql.=")";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "UPDATE invoice SET 
        	serial_cus = ".($this->serial_cus?"'".$this->serial_cus."'":"NULL").",
			serial_pay = ".($this->serial_pay?"'".$this->serial_pay."'":"NULL").",
			serial_dea = ".($this->serial_dea?"'".$this->serial_dea."'":"NULL").",
			serial_dbm = '".$this->serial_dbm."',
			date_inv = STR_TO_DATE('".$this->date_inv."','%d/%m/%Y'),
			due_date_inv = STR_TO_DATE('".$this->due_date_inv."','%d/%m/%Y'),
			number_inv = '".$this->number_inv."',
			discount_prcg_inv = ".($this->discount_prcg_inv?"'".$this->discount_prcg_inv."'":"'0'").",
			comision_prcg_inv = '".$this->comision_prcg_inv."',
			other_dscnt_inv = ".($this->other_dscnt_inv?"'".$this->other_dscnt_inv."'":"'0'").",
			comments_inv = '".$this->comments_inv."',
			printed_inv = '".$this->printed_inv."',
			status_inv = '".$this->status_inv."',
			total_inv = '".$this->total_inv."',
            has_credit_note_inv = '".$this->has_credit_note_inv."',
        	applied_taxes_inv = ".($this->applied_taxes_inv?"'".$this->applied_taxes_inv."'":"NULL")." ,
			erp_id = ".($this->erp_id?"'".$this->erp_id."'":"NULL")." ,
			payphone_id = ".($this->payphone_id?"'".$this->payphone_id."'":"NULL")."
        	WHERE serial_inv = '".$this->serial_inv."'";

        $result = $this->db->Execute($sql);
    	if($result){
            return true;
        }else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	function updateStatus($serial_pay,$status_inv){
		$sql = "UPDATE invoice SET
				status_inv = '".$status_inv."'
				WHERE serial_pay = '".$serial_pay."'";

		$result = $this->db->Execute($sql);

        return $result==true;
	}
	/***********************************************
    * funcion getAllStatus
    * Returns all kind of Status which exist in DB
    ***********************************************/
	function getAllStatus(){
		// Finds all possible status form ENUM column
		$sql = "SHOW COLUMNS FROM invoice LIKE 'status_inv'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

    /*******************************************
    @Name: getInvoicesAutocompleter
    @Description: Returns an array of invoices.
    @Params: invoice number and country serial.
    @Returns: invoices array
    ********************************************/
    function getInvoicesAutocompleter($number_inv,$serial_mbc,$typeDbc){
		$sql = "SELECT i.serial_inv, i.number_inv
				FROM dealer dea
				JOIN dealer de ON de.dea_serial_dea = dea.serial_dea
				JOIN counter cnt ON cnt.serial_dea=de.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN invoice i ON i.serial_inv=sal.serial_inv AND lpad(i.number_inv, 8, '0') LIKE '%".$number_inv."%'
				JOIN document_by_manager dbm ON i.serial_dbm=dbm.serial_dbm AND (dbm.type_dbm='BOTH' OR dbm.type_dbm='".$typeDbc."')
				WHERE dea.serial_mbc='".$serial_mbc."'
				ORDER BY i.number_inv
				LIMIT 10";
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /*******************************************
    @Name: getInvoice
    @Description: Returns an array of invoices.
    @Params: invoice number and country serial.
    @Returns: invoices array
    ********************************************/
    function getInvoice($number_inv,$serial_dbm, $not_report=NULL){
		$sql = "SELECT i.serial_inv, i.number_inv, IFNULL(pay.total_payed_pay, 0) AS 'total_payed_pay', i.status_inv
				FROM invoice i 
				LEFT JOIN payments pay ON i.serial_pay = pay.serial_pay
				WHERE i.serial_dbm='".$serial_dbm."'
				AND i.number_inv='".intval($number_inv)."'
				AND i.serial_inv NOT IN(SELECT serial_inv
									   FROM invoice_log
									   WHERE status_inl='PENDING')";
		if($not_report){
			$sql.=" AND i.status_inv= 'STAND-BY'";
		}
		$result = $this->db -> Execute($sql);

		return $result->GetRowAssoc(false);
	}

	/*******************************************
    @Name: getInvoice
    @Description: Returns an array of invoices.
    @Params: invoice number and country serial.
    @Returns: invoices array
    ********************************************/
    function getInvoiceForCardReport($number_inv,$serial_mbc,$typeDbc){
		$sql = "SELECT i.serial_inv, i.number_inv, pay.total_payed_pay
				FROM dealer dea
				JOIN dealer de ON de.dea_serial_dea = dea.serial_dea
				JOIN counter cnt ON cnt.serial_dea=de.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN invoice i ON i.serial_inv=sal.serial_inv AND i.number_inv='".intval($number_inv)."'
				JOIN document_by_manager dbm ON i.serial_dbm=dbm.serial_dbm AND (dbm.type_dbm='BOTH' OR dbm.type_dbm='".$typeDbc."')
				LEFT JOIN payments pay ON i.serial_pay = pay.serial_pay
				WHERE dea.serial_mbc='".$serial_mbc."' AND i.serial_inv NOT IN(SELECT serial_inv
																			   FROM invoice_log
																			   WHERE status_inl='PENDING')";
                //die($sql);
		$result = $this->db -> Execute($sql);

		return $result->GetRowAssoc(false);
	}

	    /*******************************************
    @Name: getInvoice
    @Description: Returns an array of invoices.
    @Params: invoice number and country serial.
    @Returns: invoices array
    ********************************************/
    function getInvoiceExistToPrint($number_inv,$serial_mbc,$typeDbc){
		$sql = "SELECT i.serial_inv
				FROM dealer dea
				JOIN dealer de ON de.dea_serial_dea = dea.serial_dea
				JOIN counter cnt ON cnt.serial_dea=de.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN invoice i ON i.serial_inv=sal.serial_inv AND i.number_inv='".intval($number_inv)."' AND i.status_inv!= 'VOID'
				JOIN document_by_manager dbm ON i.serial_dbm=dbm.serial_dbm AND (dbm.type_dbm='BOTH' OR dbm.type_dbm='".$typeDbc."')
				WHERE dea.serial_mbc='".$serial_mbc."' AND i.serial_inv NOT IN(SELECT serial_inv FROM invoice_log WHERE status_inl='PENDING')";
		$result = $this->db -> getOne($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/***********************************************
    * funcion existsInvoice
    * verifies if a invoice exists or not
    ***********************************************/
	function existsInvoice($serial_dea,$serial_inv){
		$sql = 	"SELECT serial_dea
				 FROM invoice
				 WHERE serial_dea= _utf8'".utf8_encode($serial_dea)."' collate utf8_bin";

		if($serial_inv != NULL && $serial_inv != ''){
			$sql.= " AND serial_inv <> ".$serial_inv;
		}
//echo $sql;
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/***********************************************
    * funcion getSerialPayByNumber
    * verifies if a invoice exists or not
    ***********************************************/
	function getSerialPayByNumber($number_inv){
		$sql = "SELECT i.serial_pay
				FROM invoice i
				WHERE i.number_inv = '".$number_inv."'";

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

	/***********************************************
    * funcion getSalesStatusByInvoice
    * get the status of the sales by invoice
    ***********************************************/
	function getSalesStatusByInvoice($serial_pay){
		$sql = "SELECT s.status_sal
				FROM sales s
				JOIN invoice i ON i.serial_inv = s.serial_inv
				WHERE i.serial_pay = '".$serial_pay."'";

		$result = $this->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->fields[0];
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}


	/***********************************************
    * funcion getInvoicesByPayment
    * verifies if a invoice exists or not
    ***********************************************/
	function getInvoicesByPayment($serial_pay){
		$sql = "SELECT i.serial_inv, DATE_FORMAT(i.date_inv, '%d/%m/%Y') as date_inv, i.number_inv, i.total_inv, CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ) name_cus, cus.serial_cus,
			           d.name_dea, i.serial_pay
				FROM invoice i
				LEFT JOIN dealer d ON d.serial_dea = i.serial_dea
				LEFT JOIN customer cus ON cus.serial_cus = i.serial_cus
				WHERE i.serial_pay = '".$serial_pay."'";
		$result = $this->db-> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
	/***********************************************
    * funcion getInvoicesReport
    * verifies if a invoice exists or not
    ***********************************************/
	public static function getInvoicesReport($db,$selCountry, $selManager,$selDealer,$selBranch,$selResponsable,$selType,$txtStartDate,$txtEndDate,$selAmount,$txtAmount){
		//THIS SQL IS FOR INVOICES WITHOUT SALES, WE BYPASS THROUGH INVOICE_LOG
		$sqlNoSales = "	JOIN invoice_log ilog ON  ilog.serial_inv = i.serial_inv
						JOIN sales s ON  s.serial_sal = ilog.serial_sal";
		
		//THIS SQL IS FOR INVOICES WITH SALES
		$sqlSales = "	JOIN sales s ON  s.serial_inv = i.serial_inv";
		
		$sql = 	"SELECT * FROM (" . self::getInvoicesReportGeneralQuery($sqlNoSales, $selCountry, $txtStartDate, $txtEndDate, $selDealer, $selBranch, $selResponsable, $selType, $selAmount, $selManager)
				. " UNION " . self::getInvoicesReportGeneralQuery($sqlSales, $selCountry, $txtStartDate, $txtEndDate, $selDealer, $selBranch, $selResponsable, $selType, $selAmount, $selManager) 
				. ")as results
				  ORDER BY number_inv";
		
		$result = $db-> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$arreglo=array();
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
	public function getInvoicesReportGeneralQuery($specificMatch, $selCountry, $txtStartDate, $txtEndDate, $selDealer, $selBranch, $selResponsable, $selType, $selAmount, $selManager = NULL){
		$sql = "SELECT  i.serial_inv, i.number_inv,
						IFNULL(GROUP_CONCAT(DISTINCT s.card_number_sal ORDER BY s.card_number_sal SEPARATOR ', '),'N/A') card_number_sal,
						COUNT(*) as qty,
						DATE_FORMAT(i.date_inv, '%d/%m/%Y') as date_inv, 
						CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ) name_cus, 
						i.serial_cus,
						IF(i.serial_cus IS NULL,'Comercializador','Cliente') facturado_a,
						i.serial_dea,
						i.total_inv,
						i.status_inv,
						CONCAT( usr.first_name_usr, ' ', usr.last_name_usr ) name_rep,
						man.name_man,
						bra.name_dea as name_bra,
						IF(COUNT(*) = 1, CONCAT(cnt_user.first_name_usr, cnt_user.last_name_usr) ,'Varias Ventas') as counter_inv,
						d.name_doc,
						i.due_date_inv as fecha_vencimiento
				FROM 	invoice i
				$specificMatch
				JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
				JOIN document_type d ON d.serial_doc = dbm.serial_doc
				JOIN customer cus ON cus.serial_cus = s.serial_cus
				JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
				JOIN user cnt_user ON cnt.serial_usr = cnt_user.serial_usr
				JOIN dealer bra ON cnt.serial_dea = bra.serial_dea
				JOIN user_by_dealer ubd ON bra.serial_dea = ubd.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON ubd.serial_usr = usr.serial_usr
				JOIN manager_by_country mbc ON bra.serial_mbc = mbc.serial_mbc
					AND mbc.serial_cou = $selCountry
				JOIN manager man ON mbc.serial_man = man.serial_man";
			
		$sql .= " WHERE STR_TO_DATE(DATE_FORMAT(i.date_inv,'%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$txtStartDate' , '%d/%m/%Y') AND STR_TO_DATE('$txtEndDate' , '%d/%m/%Y') ";
		//THE DATE DOUBLE CONVERSION IS TO ELIMINATE THE TIME FROM THE DATE SO ALL RECORDS FROM THE BOUNDARY DAYS ARE CONSIDERED AS WELL
		
		if($selManager){
			$sql .= " AND mbc.serial_mbc = $selManager";
		}
		if($selDealer){
			$sql .= " AND bra.dea_serial_dea = ".$selDealer;
		}
		if($selBranch){
			$sql .= " AND bra.serial_dea = ".$selBranch;
		}
		if($selResponsable){
			$sql .= " AND usr.serial_usr = ".$selResponsable;
		}
		if($selType){
			$types = implode("','", $selType);
			$sql .= " AND i.status_inv IN('$types')";
		}
		if($selAmount){
			$sql .= " AND i.total_inv ".$selAmount." '".$txtAmount."'";
		}
		
		$sql.=" GROUP BY dbm.type_dbm, i.number_inv ";
		return $sql;
	}

	/*******************************************
	@Name: getDirectSalesByPayment
	@Description: Returns an array of all the sales of an invoice.
	@Params: serial invoice.
	@Returns: sales array
	********************************************/
	function getDirectSalesByPayment($serial_pay){
		$sql="	SELECT s.serial_sal,i.serial_inv,p.serial_pay,s.direct_sale_sal,s.serial_cus
				FROM  sales s
				JOIN invoice i ON i.serial_inv=s.serial_inv
				JOIN payments p ON p.serial_pay=i.serial_pay
				WHERE p.serial_pay='".$serial_pay."'
				AND s.direct_sale_sal='YES'";
		//echo $sql;
		$result = $this->db-> Execute($sql);
		$num = $result -> RecordCount();
		if($num==1){
			return $result->fields[4];
		}
		else{
			return false;
		}
	}

	/*******************************************
	@Name: getInvoiceSales
	@Description: Returns an array of all the sales of an invoice.
	@Params: serial invoice.
	@Returns: sales array
	********************************************/
	function getInvoiceSales($serial_inv, $sales_serials=NULL){
			$sql = "SELECT sal.card_number_sal, CONCAT(IFNULL(cus.last_name_cus,''),' ',cus.first_name_cus) as name, pbl.name_pbl, sal.total_sal,
						   inv.discount_prcg_inv, inv.other_dscnt_inv, inv.number_inv,applied_taxes_inv, pbc.serial_pxc, sal.serial_sal, d.serial_mbc
					FROM sales as sal
					JOIN invoice as inv ON inv.serial_inv=sal.serial_inv AND inv.serial_inv=$serial_inv
					JOIN customer as cus ON cus.serial_cus=sal.serial_cus
					JOIN product_by_dealer as pbd ON pbd.serial_pbd=sal.serial_pbd
                    JOIN dealer d ON d.serial_dea = pbd.serial_dea
					JOIN product_by_country as pbc ON pbc.serial_pxc=pbd.serial_pxc
					JOIN product as pro ON pro.serial_pro=pbc.serial_pro
					JOIN product_by_language as pbl ON pbl.serial_pro=pro.serial_pro AND pbl.serial_lang=".$_SESSION['serial_lang'];

			if($sales_serials!=NULL){
				$sql.=" WHERE sal.serial_sal IN(".$sales_serials.")";
			}
			$sql.=" ORDER BY sal.card_number_sal DESC";
//			echo $sql.'<br>';
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
					$arreglo=array();
					$cont=0;

					do{
							$arreglo[$cont]=$result->GetRowAssoc(false);
							$result->MoveNext();
							$cont++;
					}while($cont<$num);
			}

			return $arreglo;
	}
	
	/***********************************************
    * funcion checkHasNotPayedInvoices
    * validate if there is at least one invoice not payed yet
    ***********************************************/
	function checkHasNotPayedInvoices($serial_zon,$serial_cou,$serial_mbc,$serial_mbc_user, $dea_serial_dea_user,$serial_cit=NULL,
									$serial_dea=NULL,$serial_bra=NULL,$date_from=NULL,$date_to=NULL,$days=NULL,
									$category_dea=NULL,$comissionist=NULL){
		
       if($serial_mbc){
            $query = " AND mbc.serial_mbc = $serial_mbc
                      ";
            $query2 = ",zon.name_zon, cou.name_cou, man.name_man";
            
        }
		
		$city_filter = '';
		$dealer_filter = '';
		$branch_filter = '';
		$dates_filter = '';
		$category_filter = '';
		$unpaidDays_filter = '';

        if($serial_cit) $city_filter = "JOIN sector sec ON sec.serial_sec=dea.serial_sec AND sec.serial_cit = $serial_cit";
        if($serial_dea) $dealer_filter = " AND dea.serial_dea = $serial_dea";
        if($serial_bra) $branch_filter = " AND br.serial_dea = $serial_bra";
        if($date_from && $date_to) $dates_filter = " AND DATE_FORMAT(inv.date_inv,'%Y/%m/%d') BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND STR_TO_DATE('$date_to', '%d/%m/%Y')";
        if($category_dea) $category_filter = "AND dea.category_dea = '$category_dea'";
		if($comissionist) $ubd_filter.=" AND ubd.serial_usr = $comissionist";
		
		if($days){
			switch($days){
				case '91':
					$unpaidDays_filter = "AND diff > 90";
					break;
				case '61':
					$unpaidDays_filter = "AND diff > 60 AND diff < 91";
					break;
				case '31':
					$unpaidDays_filter = "AND diff > 30 AND diff < 61";
					break;
				case '1':
					$unpaidDays_filter = "AND diff < 30";
					break;
			}
		}

        $sql = "SELECT datediff(CURRENT_TIMESTAMP, inv.due_date_inv) AS diff , inv.number_inv
                FROM sales sal
                     JOIN counter AS cnt ON cnt.serial_cnt = sal.serial_cnt
                     JOIN dealer br ON cnt.serial_dea=br.serial_dea  $branch_filter
					 JOIN credit_day cdd ON cdd.serial_cdd = br.serial_cdd
                     JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealer_filter $category_filter
					 $city_filter
					 JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea $ubd_filter AND ubd.status_ubd = 'ACTIVE'
                     JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc ";
                     if($serial_mbc == 0){
                        }else{
                             $sql .=$query;

                    }
             $sql .= "JOIN manager AS man ON man.serial_man = mbc.serial_man
                     JOIN country AS cou ON mbc.serial_cou = cou.serial_cou AND cou.serial_zon = $serial_zon AND cou.serial_cou = $serial_cou
                     JOIN zone AS zon ON zon.serial_zon = cou.serial_zon";
        $sql .= "
                     JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
					 LEFT JOIN payments pay ON pay.serial_pay=inv.serial_pay
					 LEFT JOIN customer AS cus ON cus.serial_cus = inv.serial_cus
					 WHERE inv.status_inv = 'STAND-BY'
					 $dates_filter";

		if($serial_mbc_user != '1')$sql .= " AND mbc.serial_mbc = $serial_mbc_user"; //TODO HARDCODED PARAM
		if($dea_serial_dea_user)$sql.=" AND dea.serial_dea = $dea_serial_dea_user";
		
		$sql .= " GROUP BY inv.number_inv
				$unpaidDays_filter
				LIMIT 1";
		//Debug::print_r($sql);
		$result = $this -> db -> getRow($sql);
		if($result){
			return true;
		}else
			return false;
		
	}

	/***********************************************
    * funcion getNotPayedInvoices
    * gets information about invoices not payed yet
    ***********************************************/
    function getNotPayedInvoices($serial_zon,$serial_cou,$serial_mbc,$serial_mbc_user, $dea_serial_dea_user,$serial_cit=NULL,
									$serial_dea=NULL,$serial_bra=NULL,$date_from=NULL,$date_to=NULL,$days=NULL,
									$category_dea=NULL,$comissionist=NULL){
        
        if($serial_mbc){
            $query = "AND mbc.serial_mbc = $serial_mbc
                      ";
            $query2 = ",zon.name_zon, cou.name_cou, man.name_man";
            
        }
		$city_filter = '';
		$dealer_filter = '';
		$branch_filter = '';
		$dates_filter = '';
		$category_filter = '';
		$unpaidDays_filter = '';

        if($serial_cit) $city_filter = "JOIN sector sec ON sec.serial_sec=dea.serial_sec AND sec.serial_cit = $serial_cit";
        if($serial_dea) $dealer_filter = " AND dea.serial_dea = $serial_dea";
        if($serial_bra) $branch_filter = " AND br.serial_dea = $serial_bra";
        if($date_from && $date_to) $dates_filter = " AND DATE_FORMAT(inv.date_inv,'%Y/%m/%d') BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND STR_TO_DATE('$date_to', '%d/%m/%Y')";
        if($category_dea) $category_filter = "AND dea.category_dea = '$category_dea'";
		if($comissionist) $ubd_filter.=" AND ubd.serial_usr = $comissionist";
		
		if($days){
			switch($days){
				case '91':
					$unpaidDays_filter = "HAVING diff > 90";
					break;
				case '61':
					$unpaidDays_filter = "HAVING diff > 60 AND diff < 91";
					break;
				case '31':
					$unpaidDays_filter = "HAVING diff > 30 AND diff < 61";
					break;
				case '1':
					$unpaidDays_filter = "HAVING diff < 30";
					break;
			}
		}

        $sql = "SELECT datediff(CURRENT_TIMESTAMP, inv.due_date_inv) AS diff, sal.card_number_sal, inv.number_inv, 
					   sal.emission_date_sal, inv.serial_dea, inv.serial_cus, dea.phone1_dea,zon.name_zon, cou.name_cou, man.name_man, dea.name_dea,
					   IF(pay.total_payed_pay IS NOT NULL, pay.total_to_pay_pay - pay.total_payed_pay, inv.total_inv) AS total_inv,
					   br.name_dea as name_bra, br.phone1_dea as phone_bra, cus.first_name_cus,
					   cus.last_name_cus, cus.phone1_cus, GROUP_CONCAT( sal.card_number_sal ) AS card_group,
					   br.serial_dea as serial_bra, DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS 'date_inv',
					   cdd.days_cdd, 
						IF(datediff(CURRENT_TIMESTAMP, inv.date_inv) > cdd.days_cdd, 'OUT', 'IN') AS 'days_status',
						IF(pay.total_payed_pay IS NOT NULL, '(A)', '') AS 'payment_status', CONCAT(u.first_name_usr,' ',u.last_name_usr) AS name_usr
                FROM sales sal
                     JOIN counter AS cnt ON cnt.serial_cnt = sal.serial_cnt
                     JOIN dealer br ON cnt.serial_dea=br.serial_dea  $branch_filter
					 JOIN credit_day cdd ON cdd.serial_cdd = br.serial_cdd
                     JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealer_filter $category_filter
					 $city_filter
					 JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea $ubd_filter AND ubd.status_ubd = 'ACTIVE'
                     JOIN user u on u.serial_usr = ubd.serial_usr ";
             $sql .="JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc ";
                         if($serial_mbc == 0){
                         }else{
                             $sql .=$query;
                             
                         }
                     
            $sql .=" JOIN manager AS man ON man.serial_man = mbc.serial_man
                     JOIN country AS cou ON mbc.serial_cou = cou.serial_cou AND cou.serial_zon = $serial_zon AND cou.serial_cou = $serial_cou
                     JOIN zone AS zon ON zon.serial_zon = cou.serial_zon
                     JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
					 LEFT JOIN payments pay ON pay.serial_pay=inv.serial_pay
					 LEFT JOIN customer AS cus ON cus.serial_cus = inv.serial_cus
					 WHERE inv.status_inv = 'STAND-BY'
					 $dates_filter";

		if($serial_mbc_user != '1')$sql .= " AND mbc.serial_mbc = $serial_mbc_user"; //TODO HARDCODED PARAM
		if($dea_serial_dea_user)$sql.=" AND dea.serial_dea = $dea_serial_dea_user";
		
		$sql .= " GROUP BY sal.serial_inv
				 $unpaidDays_filter
				 ORDER BY man.name_man, name_bra,  serial_bra, inv.date_inv";
		
		//Debug::print_r($sql); die;
		$result = $this -> db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont = 0;

            do{
                $arr[$cont++] = $result -> GetRowAssoc(false);
                $result -> MoveNext();
            }while($cont < $num);
        }
        return $arr;
    }

   /*************************************************
    * funcion invoiceInCreditDays
    * gets if an invoice is still between creditdays
    *************************************************/
    function daysToDueDate() {
        $sql="SELECT DATEDIFF(due_date_inv,NOW()) as days_to_dueDate
              FROM invoice
              WHERE serial_inv=".$this->serial_inv;

        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
                return $result->fields[0];
        }else{
                return false;
        }
    }

	/*
	 * @name: getUncollectableInvoices
	 * @param: $db - DB connection
	 * @param: $min_days - Number of days that have to pass to declare a invoice
	 *		   as uncollectable.
	 * @param: $serial_mbc - Manager by Country ID to find it's invoices.
	 */
	public static function getUncollectableInvoices($db, $min_days,$serial_mbc){
		$sql="SELECT b.name_dea, i.due_date_inv, i.total_inv, DATEDIFF(NOW(), i.date_inv) AS 'uncollectable_time',
					 GROUP_CONCAT(DISTINCT CAST(s.card_number_sal AS CHAR) ORDER BY s.card_number_sal SEPARATOR ',<br />' ) AS 'cards',
					 mbc.serial_mbc, m.serial_man, m.name_man, i.serial_inv, DATE_FORMAT(i.date_inv, '%d/%m/%Y') AS 'date_inv',
					 DATE_FORMAT(i.due_date_inv, '%d/%m/%Y') AS 'due_date_inv', i.total_inv, IF(p.total_payed_pay IS NULL, i.total_inv, i.total_inv-p.total_payed_pay) AS 'uncollectable_amount'
			  FROM invoice i
			  JOIN sales s ON s.serial_inv=i.serial_inv
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer b ON b.serial_dea=cnt.serial_dea
			  JOIN manager_by_country mbc ON mbc.serial_mbc=b.serial_mbc AND mbc.serial_mbc='$serial_mbc'
			  JOIN manager m ON m.serial_man=mbc.serial_man
			  LEFT JOIN payments p ON p.serial_pay=i.serial_pay
			  WHERE DATEDIFF(NOW(), i.date_inv)>=$min_days
			  AND i.status_inv = 'STAND-BY'
			  GROUP BY s.serial_inv
			  ORDER BY b.name_dea, i.date_inv DESC";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * @name: getInvoiceDetailInformation
	 * @param: $db - DB connection
	 * @param: $serial_inv - Invoice ID
	 * @returns: An array of all invoice headers and in_date payments.
	 * @observation: The comparison to determine if the invoice was paid during the credit day, is made
	 *               with the margin of 2 days added as a politic.
	 */
	public static function getInvoiceDetailInformation($db, $serial_inv){
		$sql="SELECT IF(i.serial_dea IS NULL, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')), d.name_dea) AS 'invoiced_to',
					 IF(i.serial_dea IS NULL, cus.address_cus, d.address_dea) AS 'address',
					 IF(i.serial_dea IS NULL, cus.phone1_cus, d.phone1_dea) AS 'phone',
					 IF(i.serial_dea IS NULL, cus.document_cus, d.id_dea) AS 'document',
					 IF(i.serial_pay IS NOT NULL,
						 IF(i.status_inv = 'PAID' OR i.status_inv = 'VOID',
							IF( MAX(pd.date_pyf) < DATE_ADD(i.due_date_inv, INTERVAL 2 DAY),
								'Si',
								'No' ),
							'N/A'),
						 'N/A'
					 ) AS 'in_date_pay',
					MAX(pd.serial_pyf) AS 'serial_pyf'
			  FROM invoice i
			  LEFT JOIN dealer d ON d.serial_dea=i.serial_dea
			  LEFT JOIN customer cus ON cus.serial_cus=i.serial_cus
			  LEFT JOIN payments p ON p.serial_pay=i.serial_pay
			  LEFT JOIN payment_detail pd ON p.serial_pay=pd.serial_pay
			  WHERE i.serial_inv=".$serial_inv."
			  GROUP BY pd.serial_pay";

		$result=$db->getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getBasicInvoiceInformation($db, $serial_inv){
		$sql = "SELECT dbm.serial_man, d.serial_mbc, i.number_inv, 
					IF(i.serial_cus IS NULL, hd.id_dea, hc.document_cus) AS 'holder_document'
				FROM invoice i
				JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
					AND i.serial_inv = $serial_inv
				
				JOIN sales s ON s.serial_inv = i.serial_inv
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN dealer d on d.serial_dea = pbd.serial_dea
				
				LEFT JOIN dealer hd ON hd.serial_dea = i.serial_dea
				LEFT JOIN customer hc ON hc.serial_cus = i.serial_cus
				
				GROUP BY i.serial_inv ";
		
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getInvoiceByUncollectablePayments($db, $serial_pay){
		$sql = "SELECT i.serial_inv, s.serial_sal
				FROM invoice i 
				JOIN payments p ON p.serial_pay = i.serial_pay AND p.serial_pay = $serial_pay
					AND p.status_pay = 'UNCOLLECTABLE'
				JOIN sales s ON s.serial_inv = i.serial_inv";
		
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getInvoiceDueStatus($db, $serial_inv){
		$sql = "SELECT invoice_status, DATEDIFF(CURRENT_TIMESTAMP(), date_inv) AS 'days_due'
				FROM view_accounting_pending_accounts vpa
				WHERE serial_inv = $serial_inv";
		
		$result = $db->getRow($sql);
		
		if($sql){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getServicesToExcludeForFarmersTaxCalculation($db, $serial_sal){
		$sql = "SELECT IF(ser.fee_type_ser = 'TOTAL', sbc.price_sbc, sbc.price_sbc * s.days_sal) AS 'service',
					ser.fee_type_ser, s.total_sal, sbc.price_sbc, s.days_sal, s.serial_sal
				FROM sales s
				JOIN services_by_country_by_sale scs ON scs.serial_sal = s.serial_sal
				JOIN services_by_dealer sbd ON sbd.serial_sbd = scs.serial_sbd
				JOIN services_by_country sbc ON sbc.serial_sbc = sbd.serial_sbc
				JOIN services ser on ser.serial_ser = sbc.serial_ser
					AND ser.serial_ser = 13
					AND s.serial_sal = $serial_sal";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function replicateAutomaticInvoiceForSale($db, $serial_sal, $customInvoiceData = NULL, $sendingData, $type_dbm = 'BOTH'){
//            Debug::print_r($serial_sal);die();
			$tempSale = new Sales($db, $serial_sal);
			$tempSale->getData();
			
			//************************* GETTING INVOICE HOLDER *******************
			if($customInvoiceData && count($customInvoiceData) > 0){
				if($customInvoiceData['ID']){ //CUSTOMER EXIST
					$invoiceHolder = $customInvoiceData['ID'];
				}else{
					$tempCus = new Customer($db);
					$tempCus->setDocument_cus($customInvoiceData['document']);
					$tempCus->setFirstname_cus($customInvoiceData['first_name']);
					$tempCus->setLastname_cus($customInvoiceData['last_name']);
					$tempCus->setAddress_cus($customInvoiceData['address']);
					$tempCus->setPhone1_cus($customInvoiceData['phone1']);
					$tempCus->setPhone2_cus($customInvoiceData['phone2']);
					$tempCus->setEmail_cus($customInvoiceData['email']);
					$tempCus->setSerial_cit($customInvoiceData['cityID']);
					$password = GlobalFunctions::generatePassword(6, 8, false);
					$tempCus->setPassword_cus(md5($password));
					
					$invoiceHolder = $tempCus->insert();
				}
			}else{
				if($tempSale->getSerial_rep()){ //IF THE SALE IS FOR AN UNDERAGE CUSTOMER, TAKE REP ID FOR INVOICE
					$invoiceHolder = $tempSale->getSerial_rep();
				}else{
					$invoiceHolder = $tempSale->getSerial_cus();
				}
				
			}
			
			//******************* PREPARING INVOICE MINIMUM DATA *******************
			$invoiceData = ERPConnectionFunctions::getInvoiceBasicDataForSale($db, $serial_sal);
			$serial_dbm = DocumentByManager::retrieveSelfDBMSerial($db, $invoiceData['serial_man'], $type_dbm, 'INVOICE');
			$invoiceDate = date('d/m/Y');
			
			//******************* REPLICATE INVOICE TO ERP *************************
			$erp_misc_data['discount'] = 0;
			$erp_misc_data['other_discount'] = 0;
			$erp_misc_data['holder_id'] = $invoiceHolder;
			$erp_misc_data['holder_type'] = 'C';
			$erp_misc_data['sales_ids'] = array();
			$erp_misc_data['operation_type'] = 1; //SYSTEM SALE
			
//			$erp_id_for_sale = ERPConnectionFunctions::getSaleDataForOO($db, $serial_sal);
//			if(!$erp_id_for_sale){
//				ErrorLog::log($db, 'AUTOMATIC INVOICING - NO ERP IDS FOR SALE', $serial_sal);
//				return FALSE;
//			}
				
//			array_push($erp_misc_data['sales_ids'], $erp_id_for_sale['id_erp_sal']);
//			$erp_response = ERP_logActivity('INVOICE', $erp_misc_data, 'POST', $eInvoiceMail); //ERP ACTIVITY

//			if(!$erp_response){
//				ErrorLog::log($db, 'AUTOMATIC INVOICING - ERP REPLICATION FAILED', $erp_misc_data);
//				return FALSE;
//			}
                        $tandiResponse = TandiConnectionFunctions::tandiEmision($sendingData);
			
                        //Validate TANDI Invoice
                        if($tandiResponse == null){
                            return false;
                        }else{
                            
                            //Update Number Document by Manager
                            $nextAvailableNumber = DocumentByManager::retrieveNextDocumentNumber($db, $invoiceData['serial_man'], $type_dbm, 'INVOICE', TRUE);
                            DocumentByManager::moveToNextValue($db, $serial_dbm, $nextAvailableNumber);
                            
			//*********************** CREATE PAS INSTANCE OF INVOICE ****************
			$invoice = new Invoice($db);
			$invoice->setComments_inv($_POST['txtObservations']);
			$invoice->setDate_inv($invoiceDate);
                            if($invoiceData['type_percentage_dea'] == 'COMISSION'){
                                    $invoice->setComision_prcg_inv($invoiceData['percentage_dea']);
                            }else{
                                //check if the invoice is for dealer, in this case save as discount
                                if ($sendingData['bill_to_radio'] == 'DEALER') {
                                    $invoice->setDiscount_prcg_inv($invoiceData['percentage_dea']);
                                } else {
                                    $invoice->setComision_prcg_inv($invoiceData['percentage_dea']);
                                }
                            }
			
			$credDay = new CreditDay($db, $invoiceData['payterm']);
			$credDay->getData();
			$cd = strtotime(substr($invoiceDate, 3, 3) . substr($invoiceDate, 0, 3) . substr($invoiceDate, 6, 4));
			$dueDate = date('d/m/Y', mktime(0, 0, 0, date('m', $cd), date('d', $cd) + $credDay->getDays_cdd(), date('Y', $cd)));
			$invoice->setDue_date_inv($dueDate);
			
                            $invoice->setErp_id($tandiResponse['idFactura']);
                            $invoice->setNumber_inv($tandiResponse['numeroFactura']);
                            $invoice->setOther_dscnt_inv(0);
                            if($sendingData['bill_to_radio'] == 'DEALER'){
                                $invoice->setSerial_dea($sendingData['invHolder']);
                            }else{
                                $invoice->setSerial_cus($sendingData['invHolder']);
                            }

                            $invoice->setSerial_dbm($serial_dbm);
                            $key = array_search('SUBTOTAL', array_column($tandiResponse['componentes'], 'componente'));
                            $invoice->setSubtotal_inv($tandiResponse['componentes'][$key]['valor']);
                            $key = array_search('TOTAL', array_column($tandiResponse['componentes'], 'componente'));
                            $invoice->setTotal_inv($tandiResponse['componentes'][$key]['valor']);

                            //Taxes
                            $namesTax = $_POST['hdnNameTax'];
                            $percentagesTax = $_POST['hdnPercentageTax'];
                            if ($namesTax) {
                                    foreach ($namesTax as $k => $n) {
                                            $misc_taxes[$k]['tax_name'] = $n;
                                            $misc_taxes[$k]['tax_percentage'] = $percentagesTax[$k];
                                    }
                                    $invoice->setApplied_taxes_inv(serialize($misc_taxes));
                            }

                            $serial_inv = $invoice->insert();
                            if($serial_inv){
                                    $tempSale->setSerial_inv($serial_inv);
                                    if($tempSale->update()){
                                            return $serial_inv;
                                    }else{
                                            ErrorLog::log($db, 'AUTOMATIC INVOICING - SALE UPDATE FAILED ID: '.$tempSale->getSerial_sal(), $serial_inv);
                                            return FALSE;
                                    }
                            }else{
                                    ErrorLog::log($db, 'AUTOMATIC INVOICING - PAS REPLICATION FAILED', $invoice);
                                    return FALSE;
                            }
                        }
		
	}

	public static function voidSalesErp($db, $erp_id){
            $sql = "UPDATE invoice SET status_inv = 'VOID', has_credit_note_inv = 'YES' WHERE erp_id = $erp_id";
            $result = $db -> Execute($sql);
            
            if($result){
                    return $result;
            }else{
                    return FALSE;
            }
        }

    public static function getInvoiceByErp($db, $erp_id){
            $sql = "SELECT * FROM invoice WHERE erp_id = $erp_id";
            $result = $db->Execute($sql);
            $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
        }

    public static function getAgreementByInvoice($db, $serial_inv){
        $sql = "SELECT  inv.serial_inv, agr.serial_agr, iba.serial_iba, agr.name_agr
                FROM agreement agr 
                INNER JOIN invoice_by_agreement iba  on agr.serial_agr = iba.serial_agr
                INNER JOIN invoice inv on iba.serial_inv = inv.serial_inv
                WHERE inv.serial_inv = $serial_inv";
        $result = $db->getRow($sql);
        if($result){
            return $result;
        }else{
            return FALSE;
        }
    }

    public static function getBillingData($db,$selCountry, $managerName, $dateFrom, $dateTo){
        $andManager = "";
        $andDates = "";
        $andCreditNoteDates="";
        if (!empty($managerName)){
            $andManager = " AND man.name_man = '$managerName' ";
        }
        if (!empty($dateFrom) && !empty($dateTo)){
            //$andDates = " AND sal.in_date_sal BETWEEN '$dateFrom' AND '$dateTo'";
            $andDates = " AND inv.date_inv BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
            $andCreditNoteDates = "cn.date_cn BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
        }
        /*$sql = "
        
        SELECT
					inv.serial_inv,
					sal.serial_sal,
					man.name_man as Sucursal,
					'Asistencia Médica' as Ramo,
					sal.card_number_sal as Poliza,
					CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as Responsable,
					dea.code_dea as CodAgente,
					dea.name_dea as Agente,
					pbl.name_pbl as Producto,
					DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
                    '' as FechaCN,
					DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
					DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
					DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
					if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
					if(inv.serial_cus is null,dea2.name_dea,concat(cus.first_name_cus,' ',ifnull(cus.last_name_cus,' '))) as Cliente,
					if(inv.serial_cus is null,'',cus.birthdate_cus) as FechaNacimiento,
					if (inv.serial_cus is null,'Comercializador','Persona') as TipoCliente,
					'FACTURA' as TipoDocumento,
					inv.number_inv as SerieFactura,
					'' as NotaCredito,
					inv.comision_prcg_inv as Comision,
					sal.total_sal as ValorPrimaSC,
					inv.applied_taxes_inv as TaxesApplied,
					inv.discount_prcg_inv as Descuento,
					inv.other_dscnt_inv as OtroDescuento,
					inv.total_inv as ValorFactura,
                    COUNT(e.serial_ext)+1 as NClientes,
                    CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as Counter,
                    sal.days_sal as DiasComprados,
                    dea.id_dea as RucIntermediario,
                    ctr.name_cou as Destino
					FROM sales as sal
					LEFT JOIN invoice as inv ON sal.serial_inv = inv.serial_inv
                    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus   
					JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
					LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
					LEFT JOIN dealer dea2 on dea2.serial_dea = inv.serial_dea
					LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
					LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
					LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
					LEFT JOIN manager man ON man.serial_man = mbc.serial_man
					LEFT join counter cou on cou.serial_cnt = sal.serial_cnt
                    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
                    LEFT JOIN extras e on e.serial_sal = sal.serial_sal
                    LEFT join city cit on cit.serial_cit = sal.serial_cit
                    LEFT join country ctr on ctr.serial_cou = cit.serial_cou 
					JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
					JOIN product as pro ON pro.serial_pro = pbc.serial_pro
					JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro
				WHERE pbl.serial_lang = 2
				AND ubd.status_ubd = 'ACTIVE'
				$andDates  
				$andManager
				GROUP BY sal.serial_sal
				
			UNION
			
						SELECT
					inv.serial_inv,
					sal.serial_sal,
					man.name_man as Sucursal,
					'Asistencia Médica' as Ramo,
					sal.card_number_sal as Poliza,
					CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as Responsable,
					dea.code_dea as CodAgente,
					dea.name_dea as Agente,
					pbl.name_pbl as Producto,
					DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
					DATE_FORMAT(cn.date_cn, '%d/%m/%Y') as FechaCN,
					DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
					DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
					DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
					if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
					if(inv.serial_cus is null,dea2.name_dea,concat(cus.first_name_cus,' ',ifnull(cus.last_name_cus,' '))) as Cliente,
					if(inv.serial_cus is null,'',cus.birthdate_cus) as FechaNacimiento,
					if (inv.serial_cus is null,'Comercializador','Persona') as TipoCliente,
					'FACTURA' as TipoDocumento,
					inv.number_inv as SerieFactura,
					cn.number_cn as NotaCredito,
					inv.comision_prcg_inv as Comision,
					sal.total_sal as ValorPrimaSC,
					inv.applied_taxes_inv as TaxesApplied,
					inv.discount_prcg_inv as Descuento,
					inv.other_dscnt_inv as OtroDescuento,
					CONCAT('-',inv.total_inv) as ValorFactura,
                    count(e.serial_ext)+1 as NClientes,
					CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as Counter,
					sal.days_sal as DiasComprados,
                    dea.id_dea as RucIntermediario,
					ctr.name_cou as Destino
					FROM sales as sal
					LEFT JOIN invoice as inv ON sal.serial_inv = inv.serial_inv
					LEFT JOIN credit_note cn on cn.serial_inv = inv.serial_inv
                    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus   
					JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
					LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
					LEFT JOIN dealer dea2 on dea2.serial_dea = inv.serial_dea
					LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
					LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
					LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
					LEFT JOIN manager man ON man.serial_man = mbc.serial_man
					left join counter cou on cou.serial_cnt = sal.serial_cnt
                    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
                    LEFT JOIN extras e on e.serial_sal = sal.serial_sal
					LEFT join city cit on cit.serial_cit = sal.serial_cit
                    LEFT join country ctr on ctr.serial_cou = cit.serial_cou 
					JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
					JOIN product as pro ON pro.serial_pro = pbc.serial_pro
					JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro
				WHERE pbl.serial_lang = 2
				AND ubd.status_ubd = 'ACTIVE'
				$andCreditNoteDates  
				$andManager
				GROUP BY sal.serial_sal
				
		";*/

         $sql = "
         
           SELECT
					inv.serial_inv,
					sal.serial_sal,
					man.name_man as Sucursal,
					'Asistencia Médica' as Ramo,
					sal.card_number_sal as Poliza,
					if(usr.first_name_usr is null, 'Sin info', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) as Responsable,
					dea.code_dea as CodAgente,
					dea.name_dea as Agente,
					pbl.name_pbl as Producto,
					DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
                    '' as FechaCN,
					DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
					DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
					DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
					if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
					if(inv.serial_cus is null,dea2.name_dea,concat(cus.first_name_cus,' ',ifnull(cus.last_name_cus,' '))) as Cliente,
					if(inv.serial_cus is null,'',cus.birthdate_cus) as FechaNacimiento,
					if (inv.serial_cus is null,'Comercializador','Persona') as TipoCliente,
					'FACTURA' as TipoDocumento,
					inv.number_inv as SerieFactura,
					'' as NotaCredito,
					inv.comision_prcg_inv as Comision,
					sal.total_sal as ValorPrimaSC,
					inv.applied_taxes_inv as TaxesApplied,
					inv.discount_prcg_inv as Descuento,
					inv.other_dscnt_inv as OtroDescuento,
					inv.total_inv as ValorFactura,
                    COUNT(e.serial_ext)+1 as NClientes,
                    CONCAT(cusr.first_name_usr,' ',cusr.last_name_usr) as Counter,
                    sal.days_sal as DiasComprados,
                    dea.id_dea as RucIntermediario,
                    ctr.name_cou as Destino
					FROM sales as sal
					LEFT JOIN invoice as inv ON sal.serial_inv = inv.serial_inv
                    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus   
					JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
					LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
					LEFT JOIN dealer dea2 on dea2.serial_dea = inv.serial_dea
					LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea 
					LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
					LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
					LEFT JOIN manager man ON man.serial_man = mbc.serial_man
					LEFT join counter cou on cou.serial_cnt = sal.serial_cnt
                    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
                    LEFT JOIN extras e on e.serial_sal = sal.serial_sal
                    LEFT join city cit on cit.serial_cit = sal.serial_cit
                    LEFT join country ctr on ctr.serial_cou = cit.serial_cou 
					JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
					JOIN product as pro ON pro.serial_pro = pbc.serial_pro
					JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro
				WHERE pbl.serial_lang = 2
				 AND ubd.status_ubd = 'ACTIVE'
				$andDates  
				$andManager
				AND mbc.serial_cou = $selCountry
				GROUP BY sal.serial_sal
				
		  UNION
		  
		  SELECT
					inv.serial_inv,
					sal.serial_sal,
					man.name_man as Sucursal,
					'Asistencia Médica' as Ramo,
					sal.card_number_sal as Poliza,
					if(usr.first_name_usr is null, 'Sin info', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) as Responsable,
					dea.code_dea as CodAgente,
					dea.name_dea as Agente,
					pbl.name_pbl as Producto,
					DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
					DATE_FORMAT(cn.date_cn, '%d/%m/%Y') as FechaCN,
					DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
					DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
					DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
					if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
					if(inv.serial_cus is null,dea2.name_dea,concat(cus.first_name_cus,' ',ifnull(cus.last_name_cus,' '))) as Cliente,
					if(inv.serial_cus is null,'',cus.birthdate_cus) as FechaNacimiento,
					if (inv.serial_cus is null,'Comercializador','Persona') as TipoCliente,
					'NOTA DE CREDITO' as TipoDocumento,
					inv.number_inv as SerieFactura,
					cn.number_cn as NotaCredito,
					inv.comision_prcg_inv as Comision,
					sal.total_sal as ValorPrimaSC,
					inv.applied_taxes_inv as TaxesApplied,
					inv.discount_prcg_inv as Descuento,
					inv.other_dscnt_inv as OtroDescuento,
					CONCAT('-',cn.amount_cn) as ValorFactura,
                    '1' as NClientes,
					CONCAT(cusr.first_name_usr,' ',cusr.last_name_usr) as Counter,
					sal.days_sal as DiasComprados,
                    dea.id_dea as RucIntermediario,
					ctr.name_cou as Destino
					FROM credit_note cn
                    LEFT JOIN invoice inv on cn.serial_inv = inv.serial_inv
					LEFT JOIN sales as sal ON sal.serial_sal = cn.serial_sal
                    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus   
					LEFT JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
					 LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
					 LEFT JOIN dealer dea2 on dea2.serial_dea = inv.serial_dea
					 LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea and ubd.status_ubd = 'ACTIVE'
					 LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
					 LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
					 LEFT JOIN manager man ON man.serial_man = mbc.serial_man
					LEFT join counter cou on cou.serial_cnt = sal.serial_cnt
                    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
                    LEFT JOIN extras e on e.serial_sal = sal.serial_sal
					LEFT join city cit on cit.serial_cit = sal.serial_cit
                     LEFT join country ctr on ctr.serial_cou = cit.serial_cou 
					LEFT JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
					 LEFT JOIN product as pro ON pro.serial_pro = pbc.serial_pro
					 LEFT JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro and pbl.serial_lang = 2
			  WHERE 
				$andCreditNoteDates AND mbc.serial_cou = $selCountry
				 
				

         ";



        $result = $db->getAll($sql);
        //$result = $db->getRow($sql);
        if ($result){
            return $result;
        }
        else{
            return false;
        }
    }

	public static function getRedromData($db, $dateFrom, $dateTo){
		$dateFrom = "'$dateFrom'"; 
		$dateTo = "'$dateTo'"; 
		
		$sql = "(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
            cus.phone1_cus as 'PHONE', sal.card_number_sal AS 'CONTRACT', sal.in_date_sal AS 'FECHA', pbl.name_pbl as 'PRODUCTO', sal.status_sal as 'ESTADO'     
                FROM customer cus
                JOIN sales sal ON sal.serial_cus = cus.serial_cus and sal.status_sal in ('ACTIVE','REGISTERED','EXPIRED','STANDBY') AND date_format(sal.in_date_sal,'%Y-%m-%d') between $dateFrom and $dateTo and cus.type_cus = 'PERSON'               
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro and pro.third_party_register_pro = 'NO'
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2  
                WHERE 
                 pbl.serial_pro IN (7,102,105,140,107,110,111,112,115,176,186,159,160,174,175,170,172,171,173,164,167,168,166,157,158,169,165,188,177,161,162,184,190,189,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223))
                UNION
(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
            cus.phone1_cus as 'PHONE', trl.card_number_trl AS 'CONTRACT', trl.in_date_trl AS 'FECHA', pbl.name_pbl as 'PRODUCTO', sal.status_sal as 'ESTADO'
                FROM customer cus
                JOIN traveler_log trl ON trl.serial_cus = cus.serial_cus and cus.type_cus = 'PERSON'  and trl.days_trl > 4  AND date_format(trl.in_date_trl,'%Y-%m-%d') between $dateFrom and $dateTo
                JOIN sales sal ON sal.serial_sal = trl.serial_sal  and sal.status_sal in ('ACTIVE','REGISTERED','EXPIRED','STANDBY')             
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro and pro.third_party_register_pro = 'YES'
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2 
                WHERE 
                 pbl.serial_pro IN (7,102,105,140,107,110,111,112,115,176,186,159,160,174,175,170,172,171,173,164,167,168,166,157,158,169,165,188,177,161,162,184,190,189,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223))
				UNION
(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
            cus.phone1_cus as 'PHONE', sal.card_number_sal AS 'CONTRACT', sal.in_date_sal AS 'FECHA', pbl.name_pbl as 'PRODUCTO', sal.status_sal as 'ESTADO'
                FROM customer cus
                JOIN extras e on e.serial_cus=cus.serial_cus
                JOIN sales sal ON sal.serial_sal = e.serial_sal and sal.status_sal in ('ACTIVE','REGISTERED','EXPIRED','STANDBY') AND date_format(sal.in_date_sal,'%Y-%m-%d') between $dateFrom and $dateTo and cus.type_cus = 'PERSON'                       
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2 
                WHERE 
                 pbl.serial_pro IN (7,102,105,140,107,110,111,112,115,176,186,159,160,174,175,170,172,171,173,164,167,168,166,157,158,169,165,188,177,161,162,184,190,189,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223));
                 ";

				 $result = $db->getAll($sql);
				
        //$result = $db->getRow($sql);
        if ($result){
            return $result;
        }
        else{
            return false;
        }
	}
	public static function getSaleCreditNote($db){
		$sql = "
    SELECT
        inv.serial_inv,
        sal.serial_sal,
        man.name_man as Sucursal,
        'Asistencia Médica' as Ramo,
        sal.card_number_sal as Poliza,
        if(usr.first_name_usr is null, 'Sin info', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) as Responsable,
		usr.email_usr as EmailResponsable,
        dea.code_dea as CodAgente,
        dea.name_dea as Agente,
        pbl.name_pbl as Producto,
        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
        '' as FechaCN,
        DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
        DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
        DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
        if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
        if(inv.serial_cus is null, dea2.name_dea, CONCAT(cus.first_name_cus, ' ', ifnull(cus.last_name_cus, ' '))) as Cliente,
        if(inv.serial_cus is null, '', cus.birthdate_cus) as FechaNacimiento,
        if (inv.serial_cus is null, 'Comercializador', 'Persona') as TipoCliente,
        'FACTURA' as TipoDocumento,
        inv.number_inv as SerieFactura,
        '' as NotaCredito,
        inv.comision_prcg_inv as Comision,
        sal.total_sal as ValorPrimaSC,
        inv.applied_taxes_inv as TaxesApplied,
		ROUND((inv.total_inv * (inv.discount_prcg_inv/100)) ,2)  as Descuento,
		inv.discount_prcg_inv as DescuentoPorcentaje,
        inv.other_dscnt_inv  as OtroDescuentoPorcentaje,
        ROUND((inv.total_inv * (inv.other_dscnt_inv/100)) ,2)  as OtroDescuento,
		'0' as CuponDescuento,
        inv.total_inv as ValorFactura,
        COUNT(e.serial_ext) + 1 as NClientes,
        CONCAT(cusr.first_name_usr, ' ', cusr.last_name_usr) as Counter,
        sal.days_sal as DiasComprados,
        dea.id_dea as RucIntermediario,
        ctr.name_cou as Destino,
		dtl.description_dtl as TipoComercializador
    FROM sales as sal
    LEFT JOIN invoice as inv ON sal.serial_inv = inv.serial_inv
    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus
    JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
    LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
    LEFT JOIN dealer dea2 ON dea2.serial_dea = inv.serial_dea
	join dealer_type dtp on dea.serial_dlt = dtp.serial_dlt
	join dealer_ty_by_language dtl on dtp.serial_dlt = dtl.serial_dlt
    LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
    LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
    LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
    LEFT JOIN manager man ON man.serial_man = mbc.serial_man
    LEFT JOIN counter cou ON cou.serial_cnt = sal.serial_cnt
    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
    LEFT JOIN extras e ON e.serial_sal = sal.serial_sal
    LEFT JOIN city cit ON cit.serial_cit = sal.serial_cit
    LEFT JOIN country ctr ON ctr.serial_cou = cit.serial_cou
    JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
    JOIN product as pro ON pro.serial_pro = pbc.serial_pro
    JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro
    WHERE pbl.serial_lang = 2
      AND ubd.status_ubd = 'ACTIVE'
      AND mbc.serial_cou = 62
      AND inv.date_inv BETWEEN '2024-01-01 00:00:00' AND CURDATE()
    GROUP BY sal.serial_sal

    UNION

    SELECT
        inv.serial_inv,
        sal.serial_sal,
        man.name_man as Sucursal,
        'Asistencia Médica' as Ramo,
        sal.card_number_sal as Poliza,
        if(usr.first_name_usr is null, 'Sin info', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) as Responsable,
		usr.email_usr as EmailResponsable,
        dea.code_dea as CodAgente,
        dea.name_dea as Agente,
        pbl.name_pbl as Producto,
        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
        DATE_FORMAT(cn.date_cn, '%d/%m/%Y') as FechaCN,
        DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
        DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
        DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
        if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
        if(inv.serial_cus is null, dea2.name_dea, CONCAT(cus.first_name_cus, ' ', ifnull(cus.last_name_cus, ' '))) as Cliente,
        if(inv.serial_cus is null, '', cus.birthdate_cus) as FechaNacimiento,
        if (inv.serial_cus is null, 'Comercializador', 'Persona') as TipoCliente,
        'NOTA DE CREDITO' as TipoDocumento,
        inv.number_inv as SerieFactura,
        cn.number_cn as NotaCredito,
        inv.comision_prcg_inv as Comision,
        CONCAT('-',cn.amount_cn) as ValorPrimaSC,
        inv.applied_taxes_inv as TaxesApplied,
        CONCAT('-', ROUND((inv.total_inv * (inv.discount_prcg_inv/100)) ,2)) as Descuento,
		inv.discount_prcg_inv as DescuentoPorcentaje,
        inv.other_dscnt_inv  as OtroDescuentoPorcentaje,
        CONCAT('-', ROUND((inv.total_inv * (inv.other_dscnt_inv/100)) ,2))  as OtroDescuento,
		'0.00' as CuponDescuento,
        CONCAT('-', cn.amount_cn) as ValorFactura,
        '1' as NClientes,
        CONCAT(cusr.first_name_usr, ' ', cusr.last_name_usr) as Counter,
        sal.days_sal as DiasComprados,
        dea.id_dea as RucIntermediario,
        ctr.name_cou as Destino,
		dtl.description_dtl as TipoComercializador
    FROM credit_note cn
    LEFT JOIN invoice inv ON cn.serial_inv = inv.serial_inv
    LEFT JOIN sales as sal ON sal.serial_sal = cn.serial_sal
    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus
    LEFT JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
    LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
    LEFT JOIN dealer dea2 ON dea2.serial_dea = inv.serial_dea
	join dealer_type dtp on dea.serial_dlt = dtp.serial_dlt
	join dealer_ty_by_language dtl on dtp.serial_dlt = dtl.serial_dlt
    LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
    LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
    LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
    LEFT JOIN manager man ON man.serial_man = mbc.serial_man
    LEFT JOIN counter cou ON cou.serial_cnt = sal.serial_cnt
    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
    LEFT JOIN extras e ON e.serial_sal = sal.serial_sal
    LEFT JOIN city cit ON cit.serial_cit = sal.serial_cit
    LEFT JOIN country ctr ON ctr.serial_cou = cit.serial_cou
    LEFT JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
    LEFT JOIN product as pro ON pro.serial_pro = pbc.serial_pro
    LEFT JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
    WHERE mbc.serial_cou = 62
      AND cn.date_cn BETWEEN '2024-01-01 00:00:00' AND CURDATE()";


		$result = $db->getAll($sql);
        //$result = $db->getRow($sql);
        if ($result){
            return $result;
        }
        else{
            return false;
        }
	}

	public static function getSaleCreditNoteHistorical($db){
		$sql = "
    SELECT
        inv.serial_inv,
        sal.serial_sal,
        man.name_man as Sucursal,
        'Asistencia Médica' as Ramo,
        sal.card_number_sal as Poliza,
        if(usr.first_name_usr is null, 'Sin info', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) as Responsable,
		usr.email_usr as EmailResponsable,
        dea.code_dea as CodAgente,
        dea.name_dea as Agente,
        pbl.name_pbl as Producto,
        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
        '' as FechaCN,
        DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
        DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
        DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
        if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
        if(inv.serial_cus is null, dea2.name_dea, CONCAT(cus.first_name_cus, ' ', ifnull(cus.last_name_cus, ' '))) as Cliente,
        if(inv.serial_cus is null, '', cus.birthdate_cus) as FechaNacimiento,
        if (inv.serial_cus is null, 'Comercializador', 'Persona') as TipoCliente,
        'FACTURA' as TipoDocumento,
        inv.number_inv as SerieFactura,
        '' as NotaCredito,
        inv.comision_prcg_inv as Comision,
        sal.total_sal as ValorPrimaSC,
        inv.applied_taxes_inv as TaxesApplied,
		ROUND((inv.total_inv * (inv.discount_prcg_inv/100)) ,2)  as Descuento,
		inv.discount_prcg_inv as DescuentoPorcentaje,
        inv.other_dscnt_inv  as OtroDescuentoPorcentaje,
        ROUND((inv.total_inv * (inv.other_dscnt_inv/100)) ,2)  as OtroDescuento,
		'0' as CuponDescuento,
        inv.total_inv as ValorFactura,
        COUNT(e.serial_ext) + 1 as NClientes,
        CONCAT(cusr.first_name_usr, ' ', cusr.last_name_usr) as Counter,
        sal.days_sal as DiasComprados,
        dea.id_dea as RucIntermediario,
        ctr.name_cou as Destino,
		dtl.description_dtl as TipoComercializador
    FROM sales as sal
    LEFT JOIN invoice as inv ON sal.serial_inv = inv.serial_inv
    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus
    JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
    LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
    LEFT JOIN dealer dea2 ON dea2.serial_dea = inv.serial_dea
	join dealer_type dtp on dea.serial_dlt = dtp.serial_dlt
	join dealer_ty_by_language dtl on dtp.serial_dlt = dtl.serial_dlt
    LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
    LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
    LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
    LEFT JOIN manager man ON man.serial_man = mbc.serial_man
    LEFT JOIN counter cou ON cou.serial_cnt = sal.serial_cnt
    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
    LEFT JOIN extras e ON e.serial_sal = sal.serial_sal
    LEFT JOIN city cit ON cit.serial_cit = sal.serial_cit
    LEFT JOIN country ctr ON ctr.serial_cou = cit.serial_cou
    JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
    JOIN product as pro ON pro.serial_pro = pbc.serial_pro
    JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro
    WHERE pbl.serial_lang = 2
      AND ubd.status_ubd = 'ACTIVE'
      AND mbc.serial_cou = 62
      AND inv.date_inv BETWEEN '2023-01-01 00:00:00' AND '2023-12-31 23:59:59'
    GROUP BY sal.serial_sal

    UNION

    SELECT
        inv.serial_inv,
        sal.serial_sal,
        man.name_man as Sucursal,
        'Asistencia Médica' as Ramo,
        sal.card_number_sal as Poliza,
        if(usr.first_name_usr is null, 'Sin info', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) as Responsable,
		usr.email_usr as EmailResponsable,
        dea.code_dea as CodAgente,
        dea.name_dea as Agente,
        pbl.name_pbl as Producto,
        DATE_FORMAT(inv.date_inv, '%d/%m/%Y') as FechaContabilidad,
        DATE_FORMAT(cn.date_cn, '%d/%m/%Y') as FechaCN,
        DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') as FechaEmision,
        DATE_FORMAT(sal.begin_date_sal, '%d/%m/%Y') as FechaVigDesde,
        DATE_FORMAT(sal.end_date_sal, '%d/%m/%Y') as FechaVigHasta,
        if(inv.serial_cus is null, dea2.id_dea, cus.document_cus) as Documento,
        if(inv.serial_cus is null, dea2.name_dea, CONCAT(cus.first_name_cus, ' ', ifnull(cus.last_name_cus, ' '))) as Cliente,
        if(inv.serial_cus is null, '', cus.birthdate_cus) as FechaNacimiento,
        if (inv.serial_cus is null, 'Comercializador', 'Persona') as TipoCliente,
        'NOTA DE CREDITO' as TipoDocumento,
        inv.number_inv as SerieFactura,
        cn.number_cn as NotaCredito,
        inv.comision_prcg_inv as Comision,
        CONCAT('-',sal.total_sal) as ValorPrimaSC,
        inv.applied_taxes_inv as TaxesApplied,
        CONCAT('-', ROUND((inv.total_inv * (inv.discount_prcg_inv/100)) ,2)) as Descuento,
		inv.discount_prcg_inv as DescuentoPorcentaje,
        inv.other_dscnt_inv  as OtroDescuentoPorcentaje,
        CONCAT('-', ROUND((inv.total_inv * (inv.other_dscnt_inv/100)) ,2))  as OtroDescuento,
		'0.00' as CuponDescuento,
        CONCAT('-', cn.amount_cn) as ValorFactura,
        '1' as NClientes,
        CONCAT(cusr.first_name_usr, ' ', cusr.last_name_usr) as Counter,
        sal.days_sal as DiasComprados,
        dea.id_dea as RucIntermediario,
        ctr.name_cou as Destino,
		dtl.description_dtl as TipoComercializador
    FROM credit_note cn
    LEFT JOIN invoice inv ON cn.serial_inv = inv.serial_inv
    LEFT JOIN sales as sal ON sal.serial_sal = cn.serial_sal
    LEFT JOIN customer as cus ON cus.serial_cus = inv.serial_cus
    LEFT JOIN product_by_dealer as pbd ON pbd.serial_pbd = sal.serial_pbd
    LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
    LEFT JOIN dealer dea2 ON dea2.serial_dea = inv.serial_dea
	join dealer_type dtp on dea.serial_dlt = dtp.serial_dlt
	join dealer_ty_by_language dtl on dtp.serial_dlt = dtl.serial_dlt
    LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
    LEFT JOIN user usr ON ubd.serial_usr = usr.serial_usr
    LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
    LEFT JOIN manager man ON man.serial_man = mbc.serial_man
    LEFT JOIN counter cou ON cou.serial_cnt = sal.serial_cnt
    LEFT JOIN user cusr ON cou.serial_usr = cusr.serial_usr
    LEFT JOIN extras e ON e.serial_sal = sal.serial_sal
    LEFT JOIN city cit ON cit.serial_cit = sal.serial_cit
    LEFT JOIN country ctr ON ctr.serial_cou = cit.serial_cou
    LEFT JOIN product_by_country as pbc ON pbc.serial_pxc = pbd.serial_pxc
    LEFT JOIN product as pro ON pro.serial_pro = pbc.serial_pro
    LEFT JOIN product_by_language as pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
    WHERE mbc.serial_cou = 62
      AND cn.date_cn BETWEEN '2023-01-01 00:00:00' AND '2023-12-31 23:59:59'";


		$result = $db->getAll($sql);
        //$result = $db->getRow($sql);
        if ($result){
            return $result;
        }
        else{
            return false;
        }
	}

	public static function getActiveDealers($db){
		$sql = "
		SELECT dea.serial_dea as dealer_id, dlt.serial_dlt as dealer_type_id, dtl.description_dtl as dealer_type,
			dea.id_dea as document_number, dea.name_dea as name, dea.name_dea as short_name, usr.email_usr as email,
			dea.phone1_dea as phone, dea.address_dea as address, dea.contract_date_dea as contract_date,
			dea.bank_dea as bank, dea.account_type_dea as account_type, dea.account_number_dea as account_number,
			dea.status_dea as status, dea.manager_name_dea as manager, dea.manager_email_dea as manager_email,
			dea.manager_phone_dea as manager_phone, dea.manager_phone_dea as manager_mobile, dea.manager_birthday_dea as manager_birth_date,
			dea.pay_contact_dea as payment_contact, dea.email_contact_dea as billing_email, dea.percentage_dea as percentage,
			dea.bill_to_dea as bill_to, dea.type_percentage_dea as percentage_type, usr.document_usr as document_number_responsible,
			usr.first_name_usr as first_name, usr.last_name_usr as last_name, man.name_man as branch_name
		FROM dealer dea
		JOIN user_by_dealer ubd ON dea.serial_dea = ubd.serial_dea
		JOIN user usr ON ubd.serial_usr = usr.serial_usr
		JOIN dealer_type dlt ON dea.serial_dlt = dlt.serial_dlt
		JOIN dealer_ty_by_language dtl ON dlt.serial_dlt = dtl.serial_dlt
		JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
		JOIN manager man ON mbc.serial_man = man.serial_man
		WHERE ubd.status_ubd='ACTIVE' AND dea.status_dea = 'ACTIVE' AND dea.dea_serial_dea IS NULL;
	";

	
	$result = $db->getAll($sql);
	//$result = $db->getRow($sql);
	if ($result){
		return $result;
	}
	else{
		return false;
	}

	}

	public static function getDetailedIncentivesData($db, $selCountry, $selManager, $dateFrom, $dateTo, $daysFilter)
	{
		$andManager = "";
		$andInvoiceDates = "";
		$andCreditNoteDates = "";
		if (!empty($selManager)) {
			$andManager = "AND man.serial_man = $selManager";
		}
		if (!empty($dateFrom) && !empty($dateTo)) {
			$andInvoiceDates = "AND inv.date_inv BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
			$andCreditNoteDates = "AND cn.date_cn BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
		}

		$sql = "
			SELECT
			    sal.serial_sal,
				inv.serial_inv,
				inv.serial_pay,
				NULL as serial_cn,
				sal.card_number_sal AS Poliza,
				inv.number_inv AS Factura,
				NULL as NotaCredito,
				'FACTURA' AS Tipo,
				man.name_man AS Sucursal,
				dea.name_dea AS Comercializador,
				IF(inv.serial_cus IS NULL, dea2.name_dea, concat(cus.first_name_cus,' ', IFNULL(cus.last_name_cus,' '))) AS Cliente,
				pbl.name_pbl AS Producto,
				DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS FechaContabilidad,
				NULL as FechaNotaCredito,
				DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') AS FechaEmision,
				DATE_FORMAT(pay.date_pay, '%d/%m/%Y') AS FechaPago,
				DATEDIFF(pay.date_pay, sal.in_date_sal) AS Dias,
				inv.applied_taxes_inv as TaxesApplied,
				inv.discount_prcg_inv as Descuento,
				inv.other_dscnt_inv as OtroDescuento,
				sal.total_sal as ValorPrimaSC,
				inv.total_inv as ValorFactura
			FROM sales sal
			LEFT JOIN invoice inv ON sal.serial_inv = inv.serial_inv
			LEFT JOIN payments pay ON  pay.serial_pay = inv.serial_pay
			LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
			JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
			LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
			LEFT JOIN dealer dea2 ON dea2.serial_dea = inv.serial_dea
			LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
			LEFT JOIN manager man ON man.serial_man = mbc.serial_man
			JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
			JOIN product pro ON pro.serial_pro = pbc.serial_pro
			JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
			WHERE pbl.serial_lang = 2
			$andInvoiceDates
			$andManager
			AND mbc.serial_cou = $selCountry
			AND inv.status_inv in ('PAID', 'VOID')
			AND inv.serial_pay IS NOT NULL
			GROUP BY sal.serial_sal

			UNION

			SELECT
			    sal.serial_sal,
				inv.serial_inv,
				inv.serial_pay,
				cn.serial_cn,
				sal.card_number_sal AS Poliza,
				inv.number_inv AS Factura,
				cn.number_cn as NotaCredito,
				'NOTA DE CREDITO' AS Tipo,
				man.name_man AS Sucursal,
				dea.name_dea AS Comercializador,
				IF(inv.serial_cus IS NULL, dea2.name_dea, concat(cus.first_name_cus,' ', IFNULL(cus.last_name_cus,' '))) AS Cliente,
				pbl.name_pbl AS Producto,
				DATE_FORMAT(inv.date_inv, '%d/%m/%Y') AS FechaContabilidad,
				DATE_FORMAT(cn.date_cn, '%d/%m/%Y') as FechaNotaCredito,
				DATE_FORMAT(sal.in_date_sal, '%d/%m/%Y') AS FechaEmision,
				DATE_FORMAT(pay.date_pay, '%d/%m/%Y') AS FechaPago,
				NULL AS Dias,
				inv.applied_taxes_inv as TaxesApplied,
				inv.discount_prcg_inv as Descuento,
				inv.other_dscnt_inv as OtroDescuento,
				sal.total_sal as ValorPrimaSC,
				CONCAT('-',cn.amount_cn) as ValorFactura
			FROM credit_note cn
			LEFT JOIN invoice inv ON cn.serial_inv = inv.serial_inv
			LEFT JOIN sales sal ON sal.serial_sal = cn.serial_sal
			LEFT JOIN payments pay ON  pay.serial_pay = inv.serial_pay
			LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
			JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
			LEFT JOIN dealer dea ON pbd.serial_dea = dea.serial_dea
			LEFT JOIN dealer dea2 ON dea2.serial_dea = inv.serial_dea
			LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
			LEFT JOIN manager man ON man.serial_man = mbc.serial_man
			JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
			JOIN product pro ON pro.serial_pro = pbc.serial_pro
			JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
			WHERE pbl.serial_lang = 2
			$andCreditNoteDates
			$andManager
			AND mbc.serial_cou = $selCountry
			AND inv.serial_pay IS NOT NULL
		";

		$result = $db->getAll($sql);
		if ($result) {
			return $result;
		} else {
			return false;
		}

	}

	public static function getConsolidatedIncentivesData($db, $selCountry, $selManager, $dateFrom, $dateTo, $daysFilter)
	{
		$andManager = "";
		$andInvoiceDates = "";
		$andCreditNoteDates = "";
		if (!empty($selManager)) {
			$andManager = "AND man.serial_man = $selManager";
		}
		if (!empty($dateFrom) && !empty($dateTo)) {
			$andInvoiceDates = "AND inv.date_inv BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
			$andCreditNoteDates = "AND cn.date_cn BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
		}

		$sql = "
			SELECT
			    sal.serial_sal,
				sal.card_number_sal,
				inv.number_inv,
			    NULL as number_cn,
				man.serial_man,
				dea.serial_dea,
				man.name_man,
				dea.name_dea,
				inv.applied_taxes_inv as applied_taxes_inv,
				inv.discount_prcg_inv as discount_prcg_inv,
				inv.other_dscnt_inv as other_dscnt_inv,
				sal.total_sal as total_sal,
			    inv.total_inv as total_inv
			FROM sales sal
			LEFT JOIN invoice inv ON inv.serial_inv = sal.serial_inv
			LEFT JOIN payments pay ON  pay.serial_pay = inv.serial_pay
			JOIN counter cnt ON sal.serial_cnt = cnt.serial_cnt
			LEFT JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
			LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
			LEFT JOIN manager man ON man.serial_man = mbc.serial_man
			WHERE  mbc.serial_cou = $selCountry
			$andInvoiceDates
			$andManager
			AND (DATEDIFF(pay.date_pay, sal.in_date_sal) <= $daysFilter OR pay.date_pay IS NULL)
			AND inv.status_inv in ('PAID', 'VOID')
			AND inv.serial_pay IS NOT NULL
			GROUP BY sal.serial_sal

			UNION

			SELECT
			    sal.serial_sal,
				sal.card_number_sal,
				inv.number_inv,
				number_cn,
				man.serial_man,
				dea.serial_dea,
				man.name_man,
				dea.name_dea,
				inv.applied_taxes_inv as applied_taxes_inv,
				inv.discount_prcg_inv as discount_prcg_inv,
				inv.other_dscnt_inv as other_dscnt_inv,
				sal.total_sal as total_sal,
			    CONCAT('-',cn.amount_cn) as total_inv
			FROM credit_note cn
			LEFT JOIN invoice inv ON cn.serial_inv = inv.serial_inv
			LEFT JOIN sales sal ON sal.serial_sal = cn.serial_sal
			LEFT JOIN payments pay ON  pay.serial_pay = inv.serial_pay
			JOIN counter cnt ON sal.serial_cnt = cnt.serial_cnt
			LEFT JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
			LEFT JOIN manager_by_country mbc ON dea.serial_mbc = mbc.serial_mbc
			LEFT JOIN manager man ON man.serial_man = mbc.serial_man
			WHERE  mbc.serial_cou = $selCountry
			$andCreditNoteDates
			$andManager
			AND inv.serial_pay IS NOT NULL
		";

		$result = $db->getAll($sql);
		if ($result) {
			return $result;
		} else {
			return false;
		}

	}

	public static function getManagersTempData($db, $userSession)
	{
		$sql = "
			SELECT
			serial_usr,
			serial_man,
			name_man,
			SUM(TotalDealer) AS TotalManager
			FROM (
				SELECT serial_usr, serial_man, name_man, SUM(net_value - net_value_cn) as TotalDealer
				FROM temp_incentive_report
				WHERE serial_usr = $userSession
				GROUP BY serial_dea
				HAVING totalDealer >= 500
				ORDER BY name_man, name_dea
			) tbl
			WHERE serial_usr = $userSession
			GROUP BY serial_man
			ORDER BY name_man;
		";

		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$array = array();
			$cont = 0;
			do {
				$array[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $array;
	}

	public static function getDealersTempData($db, $managerId, $userSession)
	{
		$sql = "
			SELECT
			       serial_sal,
			       serial_man,
			       serial_dea,
			       name_man,
			       name_dea,
			       SUM(net_value) AS Invoice,
			       SUM(net_value_cn) AS CreditNote,
			       SUM(net_value - net_value_cn) AS TotalDealer
			FROM temp_incentive_report
			WHERE serial_man = $managerId
			AND serial_usr = $userSession
			GROUP BY serial_dea
			HAVING TotalDealer >= 500
			ORDER BY name_man, name_dea;
		";

		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$array = array();
			$cont = 0;
			do {
				$array[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $array;
	}

	public static function insertIncentivesTempData($db, $data)
	{

		if (!empty($data)) {

			$insertedCount = 0;
			foreach ($data as $value) {

				if (!empty($value['number_cn'])) {
					$netValueCreditNote = $value['net_value'];
					$netValue = 0.00;
				} else {
					$netValue = $value['net_value'];
					$netValueCreditNote = 0.00;
				}

				$sql = "
					INSERT INTO `temp_incentive_report`
					(
						`serial_usr`,
						`serial_sal`,
						`serial_man`,
						`serial_dea`,
						`card_number_sal`,
						`number_inv`,
						`number_cn`,
						`name_man`,
						`name_dea`,
						`net_value`,
						`net_value_cn`
					)
					VALUES (
						'" . $value['serial_usr'] . "',
						'" . $value['serial_sal'] . "',
						'" . $value['serial_man'] . "',
						'" . $value['serial_dea'] . "',
						'" . $value['card_number_sal'] . "',
						'" . $value['number_inv'] . "',
						'" . $value['number_cn'] . "',
						'" . $value['name_man'] . "',
						'" . $value['name_dea'] . "',
						'" . $netValue . "',
						'" . $netValueCreditNote . "'
					);
				";

				$result = $db->Execute($sql);
				if ($db->Affected_Rows() == 1) {
					$insertedCount++;
				}
			}
			return $insertedCount;
		}
	}

	public static function deleteIncentivesTempData($db, $userSession)
	{

		$sql = "DELETE FROM temp_incentive_report WHERE serial_usr = $userSession";
		$result = $db->Execute($sql);

		if ($result == true) {
			return true;
		} else {
			return false;
		}


	}

	public static function getSaleNetValue($invoiceData)
	{

		$serializedTaxesApplied = $invoiceData['applied_taxes_inv'];
		$netValue = "";

		if (!empty($serializedTaxesApplied)) {
			$taxes = unserialize($serializedTaxesApplied);
			if (!empty($taxes)) {
				foreach ($taxes as $tax) {
					$taxAppliedValue = isset($tax['tax_percentage']) ? $tax['tax_percentage'] : '0.00';
				}
			} else {
				$taxAppliedValue = "0.00";
			}
		} else {
			$taxAppliedValue = "0.00";
		}

		if (!empty($taxAppliedValue)) {
			if ($invoiceData['discount_prcg_inv'] > 0 || $invoiceData['other_dscnt_inv'] > 0) {
				$taxPrevValue = $invoiceData['total_sal'] - number_format(($invoiceData['total_sal'] * $taxAppliedValue) / 100, 2, '.', ',');
				$discountValue = number_format($taxPrevValue * (($invoiceData['discount_prcg_inv'] + $invoiceData['other_dscnt_inv']) / 100), 2, '.', ',');
				$netValue = number_format($taxPrevValue - $discountValue, 2, '.', '');
				$taxValue = number_format(($netValue * $taxAppliedValue) / 100, 2, '.', ',');
			} else {
				$taxValue = number_format(($invoiceData['total_sal'] * $taxAppliedValue) / 100, 2, '.', ',');
				$netValue = number_format($invoiceData['total_sal'] - $taxValue, 2, '.', '');
				$discountValue = number_format($invoiceData['total_sal'] * (($invoiceData['discount_prcg_inv'] + $invoiceData['other_dscnt_inv']) / 100), 2, '.', ',');
			}
		} else {
			$netValue = "0.00";
		}

		if (!empty($invoiceData['number_cn'])) {
			$invoiceValue = ($discountValue + ((-1) * $invoiceData['total_inv']));
			$taxPrevValue = ((-1) * $invoiceData['total_inv']) - number_format((((-1) * $invoiceData['total_inv']) * $taxAppliedValue) / 100, 2, '.', ',');
			$creditNoteDiscountValue = number_format($taxPrevValue * (($invoiceData['discount_prcg_inv'] + $invoiceData['other_dscnt_inv']) / 100), 2, '.', ',');
			$netValue = number_format($taxPrevValue, 2, '.', '');
			$taxValue = number_format(($netValue * $taxAppliedValue) / 100, 2, '.', ',');
		} else {
			$invoiceValue = $invoiceData['total_sal'];
			$taxValue = $taxValue;
		}

		return $netValue;

	}

	public static function detailedIncentiveExists($db, $serialSal, $type)
	{

		$sql = "
			SELECT serial_sal
			FROM incentive_detail
			WHERE serial_sal = " . $serialSal . "
			AND type = '" . $type . "'
		";

		$result = $db->Execute($sql);
		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}

	}

	public static function insertDetailedIncentivesData($db, $data)
	{
		if (!empty($data)) {
			$insertedCount = 0;
			foreach ($data as $value) {

				// Check if a detailed incentive record exists before insert
				$exists = self::detailedIncentiveExists($db, $value['serial_sal'], $value['type']);

				if (!$exists) {

					if (empty($value['serial_cn'])) {
						$serialCreditNote = 'NULL';
					} else {
						$serialCreditNote = $value['serial_cn'];
					}

					if (empty($value['number_cn'])) {
						$creditNoteNumber = 'NULL';
					} else {
						$creditNoteNumber = $value['number_cn'];
					}

					if (empty($value['date_cn'])) {
						$creditNoteDate = 'NULL';

					} else {
						$creditNoteDate = str_replace('/', '-', $value['date_cn']);
						$creditNoteDateFormat = date('Y-m-d', strtotime($creditNoteDate));
						$creditNoteDate = "'$creditNoteDateFormat'";
					}

					if (empty($value['date_pay'])) {
						$paymentDate = 'NULL';
					} else {
						$paymentDate = str_replace('/', '-', $value['date_pay']);
						$paymentDateFormat = date('Y-m-d', strtotime($paymentDate));
						$paymentDate = "'$paymentDateFormat'";
					}

					if ($value['paydays'] == '') {
						$payDays = 'NULL';
					} else {
						$payDays = $value['paydays'];
						$payDays = "'$payDays'";
					}

					$invoiceDate = str_replace('/', '-', $value['date_inv']);
					$invoiceDate = date('Y-m-d', strtotime($invoiceDate));

					$issueDate = str_replace('/', '-', $value['in_date_sal']);
					$issueDate = date('Y-m-d', strtotime($issueDate));

					$sql = "
						INSERT INTO `incentive_detail`
						(
							serial_usr,
							serial_sal,
							serial_inv,
							serial_cn,
							card_number_sal,
							number_inv,
							number_cn,
							type,
							name_man,
							name_dea,
							name_cus,
							name_pbl,
							date_inv,
							date_cn,
							in_date_sal,
							date_pay,
							paydays,
							net_value
						)
						VALUES (
							'" . $value['serial_usr'] . "',
							'" . $value['serial_sal'] . "',
							'" . $value['serial_inv'] . "',
							 " . $serialCreditNote . ",
							'" . $value['card_number_sal'] . "',
							'" . $value['number_inv'] . "',
							 " . $creditNoteNumber . ",
							'" . $value['type'] . "',
							'" . $value['name_man'] . "',
							'" . $value['name_dea'] . "',
							'" . $value['name_cus'] . "',
							'" . $value['name_pbl'] . "',
							'" . $invoiceDate . "',
							 " . $creditNoteDate . " ,
							'" . $issueDate . "',
							 " . $paymentDate . ",
							 " . $payDays . ",
							'" . $value['net_value'] . "'
						);
					";

					$result = $db->Execute($sql);
					if ($db->Affected_Rows() == 1) {
						$insertedCount++;
					}
				}
			}

			return $insertedCount;

		}

	}

	public function getIncentives($db, $selManager = NULL, $selDelivery= NULL)
	{

		$andManager = "";
		$andDelivery = "";
		if (!empty($selManager)) {
			$andManager = "AND idc.serial_man = $selManager";
		}

		if (!empty($selDelivery)) {
			if ($selDelivery == 1) {
				$delivery = 1;

			} else {
				$delivery = 0;

			}
			$andDelivery = "AND idc.delivered = $delivery";
		}

		$sql = "
			SELECT
				idc.serial_idc,
				idc.serial_sal,
				idc.serial_man,
				idc.serial_dea,
				idc.name_man,
				idc.name_dea,
				idc.total,
				idc.bonus,
				idc.delivered,
				idc.delivered_by,
				idc.delivery_date,
				IF(idc.delivered = 1, 'Entregado', 'No Entregado') as DeliveryStatus,
				CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as DeliveredByUser,
				idc.status,
				idc.created_by,
				idc.created_at
			FROM incentive_delivery_control idc
			LEFT JOIN user usr ON usr.serial_usr = idc.delivered_by
			WHERE status = 'ACTIVE'
			$andManager
			$andDelivery
			ORDER BY idc.name_man, idc.name_dea
		";

		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$array = array();
			$cont = 0;
			do {
				$array[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $array;

	}

	public function getIncentivesByManagerId($db, $managerId)
	{
		$sql = "
			SELECT
				serial_idc,
				serial_sal,
				serial_man,
				serial_dea,
				name_man,
				name_dea,
				total,
				bonus,
				delivered,
				status,
				created_by,
				created_at
			FROM incentive_delivery_control
			WHERE serial_man = $managerId
			AND status = 'ACTIVE'
		";

		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$array = array();
			$cont = 0;
			do {
				$array[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $array;

	}

	public static function incentiveDeliveryExists($db, $serialSal)
	{

		$sql = "
			SELECT serial_sal
			FROM incentive_delivery_control
			WHERE serial_sal = " . $serialSal . "
		";

		$result = $db->Execute($sql);
		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}

	}

	public static function insertIncentivesDeliveryData($db, $data)
	{
		if (!empty($data)) {
			$insertedCount = 0;
			foreach ($data as $value) {

				// Check if a incentive delivery control record exists before insert
				$exists = self::incentiveDeliveryExists($db, $value['serial_sal']);

				if (!$exists) {
					$sql = "
						INSERT INTO `incentive_delivery_control`
							(
								`serial_sal`,
								`serial_man`,
								`serial_dea`,
								`name_man`,
								`name_dea`,
								`total`,
								`bonus`,
								`created_by`
							)
						VALUES
							(
							 '" . $value['serial_sal'] . "',
							 '" . $value['serial_man'] . "',
							 '" . $value['serial_dea'] . "',
							 '" . $value['name_man'] . "',
							 '" . $value['name_dea'] . "',
							 '" . $value['total'] . "',
							 '" . $value['bonus'] . "',
							 '" . $value['serial_usr'] . "'
							);
					";

					$result = $db->Execute($sql);
					if ($db->Affected_Rows() == 1) {
						$insertedCount++;
					}
				}
			}

			return $insertedCount;

		}
	}

	public function updateIncentivesDelivery($db, $incentiveDeliveryId, $delivered, $deliveredBy)
	{
		if (empty($delivered)) {
			$deliveryDate = 'NULL';
			$deliveredBy = 'NULL';
		} else {
			$deliveryDate = 'NOW()';
		}

		$sql = "
			UPDATE incentive_delivery_control SET
				delivered = $delivered,
			    delivery_date = $deliveryDate,
			    delivered_by = $deliveredBy
			WHERE serial_idc = $incentiveDeliveryId
		";

		$result = $db->Execute($sql);
		if ($result) {
			return $db->Affected_Rows();
		} else {
			return false;
		}

	}

    ///GETTERS
    function getSerial_inv(){
            return $this->serial_inv;
    }
    function getSerial_cus(){
            return $this->serial_cus;
    }
    function getSerial_pay(){
            return $this->serial_pay;
    }
    function getSerial_dea(){
            return $this->serial_dea;
    }
    function getSerial_dbm(){
            return $this->serial_dbm;
    }
    function getDate_inv(){
            return $this->date_inv;
    }
    function getDue_date_inv(){
            return $this->due_date_inv;
    }
    function getNumber_inv(){
            return $this->number_inv;
    }
    function getDiscount_prcg_inv(){
            return $this->discount_prcg_inv;
    }
    function getComision_prcg_inv(){
            return $this->comision_prcg_inv;
    }
    function getOther_dscnt_inv(){
            return $this->other_dscnt_inv;
    }
    function getComments_inv(){
            return $this->comments_inv;
    }
    function getPrinted_inv(){
            return $this->printed_inv;
    }
    function getStatus_inv(){
            return $this->status_inv;
    }
    function getSubtotal_inv(){
            return $this->subtotal_inv;
    }
    function getTotal_inv(){
            return $this->total_inv;
    }
    function getHas_credit_note_inv(){
            return $this->has_credit_note_inv;
    }
    function getApplied_taxes_inv(){
            return $this->applied_taxes_inv;
    }
    function getAuxData(){
            return $this->aux;
    }
	public function getErp_id() {
		return $this->erp_id;
	}
	public function getPayphone_id() {
		return $this->payphone_id;
	}


    ///SETTERS
    function setSerial_inv($serial_inv){
            $this->serial_inv = $serial_inv;
    }
    function setSerial_cus($serial_cus){
            $this->serial_cus = $serial_cus;
    }
    function setSerial_pay($serial_pay){
            $this->serial_pay = $serial_pay;
    }
    function setSerial_dea($serial_dea){
            $this->serial_dea = $serial_dea;
    }
    function setSerial_dbm($serial_dbm){
            $this->serial_dbm = $serial_dbm;
    }
    function setDate_inv($date_inv){
            $this->date_inv = $date_inv;
    }
    function setDue_date_inv($due_date_inv){
            $this->due_date_inv = $due_date_inv;
    }
    function setNumber_inv($number_inv){
            $this->number_inv = $number_inv;
    }
    function setDiscount_prcg_inv($discount_prcg_inv){
            $this->discount_prcg_inv = $discount_prcg_inv;
    }
    function setComision_prcg_inv($comision_prcg_inv){
            $this->comision_prcg_inv = $comision_prcg_inv;
    }
    function setOther_dscnt_inv($other_dscnt_inv){
            $this->other_dscnt_inv = $other_dscnt_inv;
    }
    function setComments_inv($comments_inv){
            $this->comments_inv = $comments_inv;
    }
    function setPrinted_inv($printed_inv){
            $this->printed_inv = $printed_inv;
    }
    function setStatus_inv($status_inv){
            $this->status_inv = $status_inv;
    }
    function setSubtotal_inv($subtotal_inv){
            $this->subtotal_inv = $subtotal_inv;
    }
    function setTotal_inv($total_inv){
            $this->total_inv = $total_inv;
    }
    function setHas_credit_note_inv($has_credit_note_inv){
            $this->has_credit_note_inv = $has_credit_note_inv;
    }
    function setApplied_taxes_inv($applied_taxes_inv){
            $this->applied_taxes_inv = $applied_taxes_inv;
    }
	public function setErp_id($erp_id) {
		$this->erp_id = $erp_id;
	}
	public function setPayPhone_id($payphone_id) {
		$this->payphone_id = $payphone_id;
	}
}
?>
