<?php
/**
 * File: ERPConnectionFunctions
 * Author: Patricio Astudillo
 * Creation Date: 27-nov-2012
 * Last Modified: 27-nov-2012
 * Modified By: Patricio Astudillo
 */

class ERPConnectionFunctions {

	/**
	 * @name hasERPConnection
	 * @param type $serial_mbc
	 * @return boolean 
	 */
	public static function invoiceHasERPConnection($serial_mbc) {
		if (ERP_ON) {
			$mbc_with_erp = array(2, 3, 4, 5, 6, 31, 32,33,34,35,36,38,40);

			if (in_array($serial_mbc, $mbc_with_erp)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public static function saleHasERPConnection($db, $serial_sal) {
		$sql = "SELECT d.serial_mbc
					FROM sales s
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd AND s.serial_sal = $serial_sal
					JOIN dealer d ON d.serial_dea = pbd.serial_dea";

		$serial_mbc = $db->getOne($sql);

		if ($serial_mbc) {
			return self::invoiceHasERPConnection($serial_mbc);
		} else {
			return FALSE;
		}
	}

	public static function getSaleDataForOO($db, $serial_sal) {
		$sql = "SELECT s.card_number_sal, s.type_sal, s.total_cost_sal, s.total_sal, 
							pbd.serial_pxc, d.serial_mbc, pbl.name_pbl,
							IF(b.bill_to_dea = 'CUSTOMER', CONCAT('C', s.serial_cus), CONCAT('D', b.serial_dea)) AS 'client_id',
							end_date_sal, begin_date_sal, s.emission_date_sal, s.observations_sal, id_erp_sal,
							CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')) AS 'full_name_cus',
							u.username_usr AS 'responsible'
					FROM sales s 
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = 2
					JOIN dealer d ON d.serial_dea = pbd.serial_dea
			
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer b ON b.serial_dea = cnt.serial_dea
					JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
					JOIN user u ON u.serial_usr = ubd.serial_usr
					WHERE s.serial_sal = $serial_sal";
        //echo $sql; die();
		$result = $db->getRow($sql);
        
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getInvoiceDataForOO($db, $serial, $type) {
		if ($type == 'D') {
			$sql = "SELECT serial_cdd AS 'payterm', CONCAT('D', serial_dea) AS 'client_id'
					FROM dealer d
					WHERE serial_dea = $serial";

			$result = $db->getRow($sql);
		} else {
			$result = array('client_id' => 'C' . $serial, 'payterm' => '10');
		}


		return $result;
	}

	public static function translateSaleType($type) {
		$oo_id = NULL;

		switch ($oo_id) {
			case 'SYSTEM':
				$oo_id = "1";
				break;

			case 'WEB':
				$oo_id = "2";
				break;

			default: //PHONE
				$oo_id = "3";
				break;
		}

		return $oo_id;
	}

	public static function getStoreID($serial_mbc) {
		switch ($serial_mbc) {
			case 2:
			case 19:
			case 31:
				$store_id = "001";
				break;
			case 3:
				$store_id = "004";
				break;
			case 4:
				$store_id = "005";
				break;
			case 5:
				$store_id = "008";
				break;
			case 6:
				$store_id = "007";
				break;
			case 32:
				$store_id = "009";
				break;
			case 33:
					$store_id = "010";
					break;
			case 34:    
					$store_id = "011";
					break;
			case 35:    
					$store_id = "012";
					break;
			case 36:    
					$store_id = "013";
					break;
			case 38:    
					$store_id = "014";
					break;
					case 40:
                $store_id = "015";
                break;
			default:
				$store_id = FALSE;
		}

		return $store_id;
	}

	public static function getDepartmentID($serial_mbc) {
		switch ($serial_mbc) {
			case 19:
				$store_id = "003";
				break;
			case 31:
				$store_id = "004";
				break;
                        case 38:
                                $store_id = "001";
                                break;
			default:
				$store_id = "001";
		}

		return $store_id;
	}
	
	public static function getLabelTranslation($serial_mbc) {
		switch ($serial_mbc) {
			case 2:
			case 19:
			case 35:
			case 36:
			case 38:
			case 40:
			case 33:
			case 8:
			case 31: //UIO
				$store_id = "0000001";
				break;
			case 6:
			case 3: //GYE
				$store_id = "0000002";
				break;
			case 4: //CUE
				$store_id = "0000005";
				break;
			case 32: //MANTA
				$store_id = "0000003";
				break;
			case 34: //LOJA
					$store_id = "0000004";
					break;
			default:
				$store_id = "0000006";
		}

		return $store_id;
	}

	/**
	 * @name getInvoicesByPaymentToMigrate
	 * @param RSC $db
	 * @param int $serial_pay
	 * @return boolean 
	 * @return array registers
	 */
	public static function getInvoicesByPaymentToMigrate($db, $serial_pay) {
		$sql = "SELECT serial_pay, number_inv, serial_man, code_cus
					FROM view_ecuadorian_payments
					WHERE serial_pay = $serial_pay";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	 * @name creditNoteHasTaxes
	 * @param RSC $db
	 * @param int $serial_cn
	 * @return boolean 
	 */
	public static function getPenaltyCNForPaymentInInvoices($db, $invoices_array) {
		$sql = "SELECT cn.serial_cn, amount_cn, number_cn, DATE_FORMAT(date_cn, '%d/%m/%Y') AS 'date_cn'
					FROM credit_note cn
					JOIN sales s ON s.serial_sal = cn.serial_sal
					JOIN invoice i ON i.serial_inv = s.serial_inv AND i.serial_inv IN (" . implode(',', $invoices_array) . ")";

		$result_pay = $db->getAll($sql);

		if ($result_pay) {
			return $result_pay;
		} else {
			return FALSE;
		}
	}

	/**
	 * @name catcteTranslation
	 * @param type $serial_dlt
	 * @return string 
	 */
	public static function catcteTranslation($serial_dlt) {
		switch ($serial_dlt) {
			case 1:
				$catcte = "0010101";
				break;
			case 2:
				$catcte = "0010102";
				break;
			case 3:
				$catcte = "0010103";
				break;
			case 4:
				$catcte = "0010104";
				break;
			case 5:
				$catcte = "0010105";
				break;
			case 6:
				$catcte = "0010106";
				break;
			case 7:
				$catcte = "0010107";
				break;
			case 8:
				$catcte = "0010108";
				break;
			case 9:
				$catcte = "0010109";
				break;
			case 'CLI':
				$catcte = "00102";
				break;
		}

		return $catcte;
	}

	/**
	 * @name retrieveDocumentType
	 * @param type $document
	 * @return string|boolean 
	 */
	public static function retrieveDocumentType($document) {
		/* IDType: [NE]. Indica el tipo de documento. Opciones v�lidas:
		 * 	0: Ninguno
		 * 	1: RUC.
		 * 	2: C�dula.
		 * 	7: Pasaporte.
		 */

		$temp = (string)$document;
		
		if (is_numeric($document) || strlen($temp) < 10) {
			$document = str_split($document);

			if ($document[2] == 9) {
				if (GlobalFunctions::validate_ruc_9($document)) {
					return '1'; //'RUC';
				} else {
					return '0'; //'OTHER';
				}
			} elseif ($document[2] == 6) {
				if (GlobalFunctions::validate_ruc_6($document)) {
					return '1'; //'RUC';
				} else {
					return '0'; //'OTHER';
				}
			} elseif ($document[2] < 6) {
				$aux_ci = $document;
				unset($document[10]);
				unset($document[11]);
				unset($document[12]);

				if (GlobalFunctions::validate_ci(implode('', $document))) {
					if (sizeof($aux_ci) == 10) {
						return '2'; //'CI';
					} else {
						return '1'; //'RUC';
					}
				} else {
					return '0'; //'OTHER';
				}
			} else {
				return '0'; //'OTHER';
			}
		} else {
			return '7'; //'PASSPORT';
		}
	}

	public static function retrievePersonType($document) {
		if (is_numeric($document)) {
			$document = str_split($document);

			if (sizeof($document) == 13) {
				if ($document[2] == 9) {
					if (GlobalFunctions::validate_ruc_9($document)) {
						return '0';
					} else {
						return '1';
					}
				} elseif ($document[2] == 6) {
					if (GlobalFunctions::validate_ruc_6($document)) {
						return '7';
					} else {
						return '1';
					}
				} elseif ($document[2] < 6) {
					$aux_ci = $document;
					unset($document[10]);
					unset($document[11]);
					unset($document[12]);

					if (GlobalFunctions::validate_ci(implode('', $document))) {
						if (sizeof($aux_ci) == 10) {
							return '5';
						} else {
							return '5';
						}
					} else {
						return '13';
					}
				} else {
					return '13';
				}
			} else if (sizeof($document) == 10) {
				$aux_ci = $document;

				if (GlobalFunctions::validate_ci(implode('', $document))) {
					if (sizeof($aux_ci) == 10) {
						return '13';
					} else {
						return '13';
					}
				} else {
					return '13';
				}
			} else {
				return '13';
			}
		} else {
			return '13';
		}
	}

	/**
	 * @name payModeTranslation
	 * @param type $payment_detail
	 * @return int 
	 */
	public static function payModeTranslation($payment_detail) {
		switch ($payment_detail) {
			case 'CREDIT_CARD':
				$oo_code = 1; //'Tarjeta de Cr�dito'
				break;

			case 'CHECK':
				$oo_code = 2; //'Cheque'
				break;

			case 'TRANSFER':
				$oo_code = 3; //'Transferencia Bancaria'
				break;

			case 'RETENTION':
				$oo_code = 4; //'Retenci�n'
				break;

			case 'XXX':
				$oo_code = 5; //'Habitaci�n'
				break;

			case 'XXX':
				$oo_code = 7; //'Cheque Propio'
				break;

			case 'XXX':
				$oo_code = 8; //'Millesa'
				break;

			case 'XXX':
				$oo_code = 9; //'Retenci�n Sufrida'
				break;

			case 'XXX':
				$oo_code = 10; //'Cheque Simple'
				break;

			case 'XXX':
				$oo_code = 11; //'Tarjeta de Cr�dito Rapida'
				break;

			default:
				$oo_code = 0; //Efectivo
				break;
		}

		return $oo_code;
	}

	public static function translateCountryByCity($db, $serial_cit) {
		$sql = "SELECT code
					FROM oo_country cou
					JOIN city cit ON cit.serial_cou = cou.serial_cou
					WHERE serial_cou = $serial_cou";

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return 'XXXNOCOUNTRYXXX';
		}
	}

	public static function translateStateByCity($db, $serial_cit) {
		$sql = "SELECT province
					FROM oo_city
					WHERE serial_cit = $serial_cit";

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return 'XXXNOSTATEXXX';
		}
	}

	public static function translateCity($db, $serial_cit, $serial_sal = NULL) {
		$sql = "SELECT code
					FROM oo_city
					WHERE serial_cit = $serial_cit";

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			if($serial_sal){
				$sql = "SELECT sec.serial_cit
							FROM sales s
							JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd AND s.serial_sal = $serial_sal
							JOIN dealer d ON d.serial_dea = pbd.serial_dea
							JOIN sector sec ON sec.serial_sec = d.serial_sec";

				$result = $db->getOne($sql);

				if ($result) {
					return self::translateCity($db, $result);
				}
			}
			
			return '2170150'; //Default City Quito
		}
	}

	public static function emptyValue($value) {
		if ($value == 'NOT') {
			$value = '\N';
		}

		return $value;
	}

	//******************************** PROCESS WEB SERVICES ******************************
	public static function invoiceIsPaidOnERPWithCardNumber($db, $card_number) {
		$ch = curl_init();
		$url = URL_ERP . "api/itempayment/$card_number/?format=json";

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		curl_setopt($ch, CURLOPT_STDERR, $handle);
		curl_setopt($ch, CURLOPT_URL, $url);
		$response = curl_exec($ch);
		$curl_response_result = curl_getinfo($ch);
		$oo_ws_response = $curl_info['http_code']; //201 success code
		$response = json_decode($response);
		curl_close($ch);

		return $response;
	}

	public static function getPaymentsForInterval($db, $from_date = NULL, $to_date = NULL) {
		$update_timestamp = true;
		
		if ($from_date && $to_date) {
			$interval['last_payment'] = $from_date . ' 00:00:00';
			$interval['now'] = $to_date . ' 23:59:59';
			$update_timestamp = false;
		} else {
			$sql = "SELECT DATE_FORMAT(STR_TO_DATE( value_par, '%d/%m/%Y %H:%i:%s' ), '%d/%m/%Y %H:%i:%s') AS 'last_payment', 
								DATE_FORMAT(CURRENT_TIMESTAMP( ) , '%d/%m/%Y %H:%i:%s') AS 'now'
						FROM parameter
						WHERE serial_par = 43";

			$interval = $db->getRow($sql);
		}

		if ($interval) {
			$interval['last_payment'] = self::formatDateToISO8601($interval['last_payment']);
			$last_sync_date = $interval['now'];
			$interval['now'] = self::formatDateToISO8601($interval['now']);
			
			$final_array = array();
			$startingItem = 0;
			$count = 50;
			
			do{
				$ch = curl_init();
				$url = URL_ERP . "api/receipt/?format=json&fromDate={$interval['last_payment']}&toDate={$interval['now']}&extraFields=PayModes,Invoices&offset=$startingItem&limit=$count";

				//echo $url; die;
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
				curl_setopt($ch, CURLOPT_USERPWD, URL_USRPSWD);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_USERAGENT, 'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
				curl_setopt($ch, CURLOPT_URL, $url);

				$response = curl_exec($ch);
				$curl_info = curl_getinfo($ch);
				$oo_ws_response = $curl_info['http_code'];
				$response = json_decode($response);
				curl_close($ch);
				
				$final_array = array_merge($final_array, $response);
				$startingItem += $count;
			}while(count($response) > 0);

			if ($oo_ws_response == 200) {
				if($update_timestamp){
					//****************** UPDATE PARAMETER *****************
					$param = new Parameter($db, 43);
					$param->getData();
					$param->setValue_par($last_sync_date);

					if (!$param->update()) {
						ErrorLog::log($db, 'ERP SYNC - FAILED TO MOVE TO NEXT DATE', $log);
					}
				}
				
				return $final_array;
			} else {
				ErrorLog::log($db, 'WS_GET_PAYMENTS_FAILED', array('dates' => $interval,
					'response' => $response,
					'code' => $oo_ws_response));
				return FALSE;
			}
		}

		return FALSE;
	}

	/**
	 * @name getPaymentFormMatch
	 * @param string $payment_form
	 * @param string $observations
	 * @param int $serial_cn
	 * @return int 
	 */
	public static function translateOOPaymentsToPASCodes($oo_payment_code) {
		$type_pyf = NULL;

		switch ($oo_payment_code) {
			case 'CASH':
				$type_pyf = 'CASH';
				break;
			case 'CHECK':
			case 'CHPRO':
				$type_pyf = 'CHECK';
				break;
			case 'COMMI':
				$type_pyf = 'COMMISSION';
				break;
			case 'ELAVO':
				$type_pyf = 'ELAVON';
				break;
			case 'PAYPA':
				$type_pyf = 'PAYPAL';
				break;
			case 'TRANS':
			case 'TRPAC':
			case 'TRS31':
			case 'TRSIN':
				$type_pyf = 'TRANSFER';
				break;

			case 'RETEN':
			case 'RTIVA':
				$type_pyf = 'RETENTION';
				break;

			case 'AMEX':
			case 'DINER':
			case 'DISC':
			case 'TMBG':
			case 'TMBI':
			case 'TMBK':
			case 'TMBP':
			case 'TMPB':
			case 'TMPF':
			case 'TOTR':
			case 'TVBG':
			case 'TVBI':
			case 'TVBK':
			case 'TVBP':
			case 'TVPB':
			case 'TVPF':
			case 'VISA':
				$type_pyf = 'CREDIT_CARD';
				break;
			
			case 'DINERS':
				$type_pyf = '00001';
				break;
			
			case 'PAYPHONE':
				$type_pyf = '00001';
				break;
			
			default:
				$type_pyf = 'CASH';
				break;
		}

		return $type_pyf;
	}

	public static function translatePASCodesToOOPayments($pas_payment_codes) {
		$type_pyf = NULL;

		switch ($pas_payment_codes) {
			case 'CASH':
				$type_pyf = 'DP318';
				break;
			case 'CHECK':
				$type_pyf = 'DP318';
				break;
			case 'COMMISSION':
				$type_pyf = 'COMMI';
				break;
			case 'ELAVON':
				$type_pyf = 'ELAVO';
				break;
			case 'PAYPAL':
				$type_pyf = 'PAYPA';
				break;
			case 'TRANSFER':
				$type_pyf = 'TRANS';
				break;

			case 'RETENTION':
				$type_pyf = 'RETEN';
				break;

			case 'CREDIT_CARD':
				$type_pyf = 'TVBP';
				break;

			case 'EXCESS':
			case 'EXCESS_CHECK':
				$type_pyf = 'EXMIG';
				break;

			case 'UNCOLLECTABLE':
			case 'REFUND':
				$type_pyf = 'OTRNC';
				break;
			
			case 'PAYPHONE':
				$type_pyf = 'PAYPH';
				break;
			
			case 'DINERS':
				$type_pyf = 'DINER';
				break;

			default:
				$type_pyf = 'DP318';
				break;
		}

		return $type_pyf;
	}

	public static function fillPASWithERPPayments($db, $from_date = NULL, $to_date = NULL, $fixed_payments = NULL) {
		$today_payments = self::getPaymentsForInterval($db, $from_date, $to_date);
		
		//Debug::print_r($today_payments); die;
		if ($today_payments) {
			foreach ($today_payments as $payment) {
                $status_pay = null;
                
				if (is_array($fixed_payments) && count($fixed_payments) > 0 && !in_array($payment->SerNr, $fixed_payments))
					continue;
				
				if ($payment->Status) {
					$payment_timestamp = new DateTime($payment->ApprovedDate);
					$temp_transaction_date = $payment_timestamp;
					$migration_date = DateTime::createFromFormat('d/m/Y', '10/05/2013');
					$payment_timestamp = date_format($payment_timestamp, 'd/m/Y') . ' ' . $payment->TransTime;
					
					if ($temp_transaction_date >= $migration_date) {
						//******************** SWEEPING INVOICE ARRAY **************
						$invoices_array = $payment->Invoices;
						$total_to_pay = 0;
						$invoices_to_pay = array();
						$invoices_to_pay_erp = array();
						$serial_pay = NULL;
						$advances_array = array();
						$total_advanced = 0;

						if (is_array($invoices_array)):
							foreach ($invoices_array as $inv) {
								if (!$inv->OnAccNr && $inv->InvoiceAmount > 0) {
									array_push($invoices_to_pay, $inv->id);
									$invoices_to_pay_erp[$inv->id]['total_inv'] = $inv->InvoiceTotal;
									$invoices_to_pay_erp[$inv->id]['total_paid'] = $inv->Amount;
								}
							}
						endif;

						$invoices_to_pay = self::translateERPInvoiceIDToPASSerials($db, implode(',', $invoices_to_pay));
						if ($invoices_to_pay) {
							//******************** SWEEPING INVOICE ARRAY TO CREATE SINGLE PAYMENT **************
							foreach ($invoices_to_pay as $pas_invoice) {
								$paymentCreationProcessSucceeded = true;
								if (!isset($invoices_to_pay_erp[$pas_invoice['erp_id']])) {
									//NO INVOICE IN ERP FOR ARRAY
									continue;
								}

								$invoice = new Invoice($db, $pas_invoice['serial_inv']);
								$invoice->getData();
								//SKIP VOIDED AND PAID INVOICES TO PREVENT ERRORS IN DATA
								if ($invoice->getStatus_inv() == 'VOID' || $invoice->getStatus_inv() == 'PAID' || $invoice->getStatus_inv() == 'EXCESS')
									continue;

								$tempErpInvoice = $invoices_to_pay_erp[$pas_invoice['erp_id']];
								
								//EXACT PAYMENT SO JUST CREATE A NEW PAYMENT AND RECORD IT
								if ($tempErpInvoice['total_inv'] == $tempErpInvoice['total_paid']) {
									$status_pay = 'PAID';
									
									$payment_reg = new Payment($db);
									$payment_reg->setStatus_pay($status_pay);
									$payment_reg->setDate_pay($payment_timestamp);
									$payment_reg->setFull_date_pay($payment_timestamp);
									$payment_reg->setExcess_amount_available_pay(0);
									$payment_reg->setObservation_pay('ERP REPLICATION PAYMENT');
									$payment_reg->setErp_id($payment->SerNr);
									$payment_reg->setTotal_to_pay_pay($tempErpInvoice['total_inv']);
									$payment_reg->setTotal_payed_pay($tempErpInvoice['total_paid']);
									$serial_pay = $payment_reg->insert();

									if ($serial_pay) {
										$pd = new PaymentDetail($db);
										$pd->setSerial_pay($serial_pay);
										$pd->setSerial_usr(1); //PAS ADMIN USER
										$pd->setType_pyf('CASH');
										$pd->setAmount_pyf($tempErpInvoice['total_paid']);
										$pd->setDate_pyf($payment_timestamp);
										$pd->setComments_pyf($payment->User);
										$pd->setChecked_pyf('YES');
										$pd->setStatus_pyf('ACTIVE');
										$pd->setNumber_pyf($payment->SerNr);
										$pd->setDocumentNumber_pyf($payment->SerNr);
										$pd->insert();
									} else {
										$paymentCreationProcessSucceeded = false;
										ErrorLog::log($db, 'ERP SYNC - PAYMENT CREATION FAILED ON REG ' . $payment->SerNr, $payment_reg);
									}
								} else {
									//POSSIBLY A PARTIAL PAYMENT OR THE MISSING PART OF A PREVIOUS ONE. CHECK IF INVOICE HAS SERIAL_PAY
									$tempPayment = new Payment($db, $invoice->getSerial_pay());
									
									if($tempPayment->getData()){ //HAS A PREVIOUS PAYMENT
										$totalPaid = $tempPayment->getTotal_payed_pay() + $tempErpInvoice['total_paid'];
										
										$tempPayment->setExcess_amount_available_pay(0);
										$tempPayment->setTotal_payed_pay($totalPaid);

										if($totalPaid >= $tempErpInvoice['total_inv']){ //COMPLETE PAYMENT
											$status_pay = 'PAID';
											$tempPayment->setStatus_pay($status_pay);
											$tempPayment->setFull_date_pay($payment_timestamp);
										}
										
										$tempPayment->update();
										$serial_pay = $tempPayment->getSerial_pay();
									}else{ //NO PREVIOUS PAYMENT CREATED
										$status_pay = 'PARTIAL';
										$payment_reg = new Payment($db);
										$payment_reg->setStatus_pay($status_pay);
										$payment_reg->setDate_pay($payment_timestamp);
										$payment_reg->setFull_date_pay($payment_timestamp);
										$payment_reg->setExcess_amount_available_pay(0);
										$payment_reg->setObservation_pay('ERP REPLICATION PAYMENT');
										$payment_reg->setErp_id($payment->SerNr);
										$payment_reg->setTotal_to_pay_pay($tempErpInvoice['total_inv']);
										$payment_reg->setTotal_payed_pay($tempErpInvoice['total_paid']);
										$serial_pay = $payment_reg->insert();
									}
									
									if ($serial_pay) {
										$pd = new PaymentDetail($db);
										$pd->setSerial_pay($serial_pay);
										$pd->setSerial_usr(1); //PAS ADMIN USER
										$pd->setType_pyf('CASH');
										$pd->setAmount_pyf($tempErpInvoice['total_paid']);
										$pd->setDate_pyf($payment_timestamp);
										$pd->setComments_pyf($payment->User);
										$pd->setChecked_pyf('YES');
										$pd->setStatus_pyf('ACTIVE');
										$pd->setNumber_pyf($payment->SerNr);
										$pd->setDocumentNumber_pyf($payment->SerNr);
										$pd->insert();
									} else {
										$paymentCreationProcessSucceeded = false;
										ErrorLog::log($db, 'ERP SYNC - PAYMENT CREATION FAILED ON REG ' . $payment->SerNr, $payment_reg);
									}

								}
								

								if ($paymentCreationProcessSucceeded) {
									//IF THERE'S A PREVIOUS PAYMENT MADE, KEEP IT
									if (!$invoice->getSerial_pay()) {
										$invoice->setSerial_pay($serial_pay);
										
										if (!$invoice->update()) {
											ErrorLog::log($db, 'ERP SYNC - INVOICE LOG FAILED', $invoice);
										}
									}

									if ($status_pay == 'PAID') {
										$invoice->setStatus_inv('PAID'); //update the invoice status

										$temp_sale = new Sales($db);
										$saleList = $temp_sale->getSalesByInvoice($invoice->getSerial_inv());

										if (sizeof($saleList) > 0) {
											foreach ($saleList as $s) {
												if ($s['status_sal'] == 'REGISTERED') {
													$temp_sale->setSerial_sal($s['serial_sal']);

													if (!$temp_sale->changeStatus('ACTIVE')) { //THE SALES WEREN'T 
														ErrorLog::log($db, 'ERP SYNC - SALE DIDNT UPDATE STATUS ON REG ' . $s['serial_sal'], $temp_sale);
													}
												}
											}
										}

										if (!$invoice->update()) {
											ErrorLog::log($db, 'ERP SYNC - INVOICE LOG FAILED', $invoice);
										}

										$misc['db'] = $db;
										$misc['type_com'] = 'IN';
										$misc['serial_fre'] = NULL;
										$misc['percentage_fre'] = NULL;
										GlobalFunctions::calculateCommissions($misc, $pas_invoice['serial_inv']);
										GlobalFunctions::calculateBonus($misc, $pas_invoice['serial_inv']);
									}
								} else {
									ErrorLog::log($db, 'ERP SYNC - PAYMENT PROCESS FAILED', $payment->SerNr);
								}
							}
						} else {
							ErrorLog::log($db, 'ERP SYNC - NO INVOICES TO PAY', $payment->SerNr);
						}
					} else {
						ErrorLog::log($db, 'ERP SYNC - OLD PAYMENT NOT PASSED', $payment->SerNr);
					}
				} else {
					ErrorLog::log($db, 'ERP SYNC - UNAPPROVED PAYMENT REGISTER', $payment->SerNr);
				}
			}
		} else {
			ErrorLog::log($db, 'ERP_NO_PAYMENTS_TODAY', 'NO PAYMENTS');
		}
	}

	/**
	 *
	 * @param type $date 
	 */
	public static function formatDateToISO8601($date) {
		$formated_date = str_replace('/', '-', $date);

		return date("c", strtotime($formated_date));
	}

	public static function translateERPInvoiceIDToPASSerials($db, $erp_invoice_serials) {
		$sql = "SELECT serial_inv, number_inv, total_inv, serial_pay, erp_id
			FROM invoice 
			WHERE erp_id IN ($erp_invoice_serials)
			AND status_inv NOT IN ('PAID', 'UNCOLLECTABLE')";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			ErrorLog::log($db, 'NO PAS SERIALS FOR ERP INVOICE IDS', $erp_invoice_serials);
			return array();
		}
	}

	public static function logCreditNotePaymentLines($payment_detail) {
		$credit_notes_as_payments = array();

		if(is_array($payment_detail) && count($payment_detail) > 0){
			foreach ($payment_detail as $pDetail) {
				if ($pDetail['type_pyf'] != 'CREDIT_NOTE'):
					continue;
				endif;

				$credit_notes_as_payments[$pDetail['invoice']] += $pDetail['amount_pyf'];
			}
		}

		return $credit_notes_as_payments;
	}

	public static function getInvoicePaymentQuickInfo($db, $invoice) {
		$serial_pay = $invoice->getSerial_pay();
		$today_date = date('Y-m-d');

		if ($serial_pay) {
			$temp_pay = new Payment($db, $serial_pay);
			if ($temp_pay->getData()) {
				if ($temp_pay->getFull_date_pay()) {
					$today_date = $temp_pay->getFull_date_pay();
				} else {
					$today_date = $temp_pay->getDate_pay();
				}

				$today_date = split('/', $today_date);
				$today_date = $today_date['2'] . '-' . $today_date['1'] . '-' . $today_date['0'];
			}
		}

		return $today_date;
	}
	
	//******************************** PHONE SALES MIGRATION FUNCTIONS *************************
	public static function retrieveMissedInvoicesToReplicateOnERP($db, $onManagersIDs, $fromDate = NULL, $toDate = NULL, $withSerials = NULL){		
		if ($fromDate && $toDate) {
			$interval['last_payment'] = $from_date;
			$interval['now'] = $to_date;
		} else {
			$sql = "SELECT DATE_FORMAT(STR_TO_DATE( value_par, '%d/%m/%Y' ), '%d/%m/%Y') AS 'last_payment', 
							DATE_FORMAT(CURRENT_TIMESTAMP( ) , '%d/%m/%Y') AS 'now'
					FROM parameter
					WHERE serial_par = 44";

			$interval = $db->getRow($sql);
		}
		
		$sql = "SELECT i.serial_inv
				FROM invoice i
				JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm AND dbm.serial_man IN ($onManagersIDs)
				WHERE STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') >= STR_TO_DATE('{$interval['last_payment']}', '%d/%m/%Y')
				AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') <= STR_TO_DATE('{$interval['now']}', '%d/%m/%Y')
				AND i.erp_id IS NULL";
				
		if($withSerials){
			$sql .= " AND i.serial_inv IN ($withSerials)";
		}		
		
		$invoicesList = $db ->getAll($sql);
		
		if($invoicesList){
			return $invoicesList;
		}else{
			return FALSE;
		}
	}
	
	
	/**
	 * @name migrateWebInvoiceToERP
	 * @param type $db
	 * @param type $serial_inv 
	 */
	public static function migrateWebInvoiceToERP($db, $serial_inv) {
		$process_outcome = 'success';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($ch, CURLOPT_POST, 1);

		$temp_inv = new Invoice($db, $serial_inv);
		$temp_inv->getData();
		$inv_aux_data = $temp_inv->getAuxData();
		
		$sending_array['Items'] = array();
		$sales_in_invoice = $temp_inv->getInvoiceSales($temp_inv->getSerial_inv());

		if (!$sales_in_invoice):
			$il = new InvoiceLog($db);
			$sales_in_invoice = $il->getVoidInvoiceSales($temp_inv->getSerial_inv());
		endif;

		$dbm = new DocumentByManager($db, $temp_inv->getSerial_dbm());
		$dbm->getData();
		$serial_man = $dbm->getSerial_man();
        
        $salesErpIdArray = array();
		foreach ($sales_in_invoice AS $sales) {
			$temp_array = array(	'ArtCode' => $sales['serial_pxc'],
									'ArtName' => $sales['name_pbl'],
									'SerialNr' => $sales['card_number_sal'],
									'Qty' => '1',
									"Price" => $sales['total_sal']
							);

			array_push($sending_array['Items'], $temp_array);
			array_push($salesErpIdArray, $sales['serial_sal']);
            
            $serial_mbc = $sales['serial_mbc'];
		}
		
		if(ERP_EMISSION_RIGHTS):
			array_push($sending_array['Items'], ERPConnectionFunctions::addEmissionRightsItem($base_data));
		endif;
		
		$salesErpIdString = implode(',', $salesErpIdArray);
		$total_sold = ERPConnectionFunctions::getFuturePayTotalAmountForSales($db, $salesErpIdString, ERP_EMISSION_RIGHTS, ERP_FARMERS_TAX, TRUE);
		
		if ($temp_inv->getSerial_cus()) {
			$holder_id = $temp_inv->getSerial_cus();
			$holder_type = 'C';
		} else {
			$holder_id = $temp_inv->getSerial_dea();
			$holder_type = 'D';
		}

		$base_data = ERPConnectionFunctions::getInvoiceDataForOO($db, $holder_id, $holder_type);

		if (commitERPInvoicingCustomer($db, $base_data['client_id'], $sales_in_invoice[0]['serial_sal'])) {

			$invoice_number = self::getStoreID($serial_mbc).self::getDepartmentID($serial_mbc).str_pad($temp_inv->getNumber_inv(), 9, '0', STR_PAD_LEFT);
			
			$sending_array['CustCode'] = $base_data['client_id'];
			$sending_array['Office'] = self::getStoreID($serial_mbc);
			$sending_array['Departament'] = self::getDepartmentID($serial_mbc);
			$sending_array['Computer'] = self::getDepartmentID($serial_mbc);
			$sending_array['OperationType'] = "1";
			$sending_array['SerNr'] = $invoice_number;
			$sending_array['InvoiceDate'] = ERPConnectionFunctions::formatDateToISO8601($temp_inv->getDate_inv());
			$sending_array['DueDate'] = ERPConnectionFunctions::formatDateToISO8601($temp_inv->getDue_date_inv());
			$sending_array['PayTerm'] = $base_data['payterm'];
			$sending_array['Discount1'] = $temp_inv->getDiscount_prcg_inv();
			$sending_array['Discount2'] = $temp_inv->getOther_dscnt_inv();
			$sending_array['Labels'] = "005";
			$sending_array['FuturePayments'] = array(
				array(	'PayModeCode' => ERPConnectionFunctions::getFuturePaymentCodeForSales($db, $salesErpIdString),
						'Amount' => $total_sold,
						'Period' => '1',
						'TimeUnit' => 'dias'
					)
			);

			//Debug::print_r($sending_array); die;
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
			curl_setopt($ch, CURLOPT_URL, URL_ERP . "api/invoice/");

			$response = curl_exec($ch);
			$curl_response_result = curl_getinfo($ch);
			$oo_ws_response = $curl_response_result['http_code']; //200 success code
			$response = json_decode($response);
			curl_close($ch);

			$erp_id = $response->id;

			if ($oo_ws_response != 201) { //ERRORS ON CREATING INVOICE
				ErrorLog::log($db, 'EC PHONE SALE MIGRATION ERROR - INVOICE '.$temp_inv->getSerial_inv(), $response);
				$process_outcome = 'connection_error';
			} else {
				$temp_inv->setErp_id($erp_id);
				$temp_inv->update();
			}
		} else {
			ErrorLog::log($db, 'FAILED TO PASS WEB SALE CUSTOMER - INVOICE '.$temp_inv->getSerial_inv(), $sending_array);
			$process_outcome = 'client_migration_error';
		}
		
		return $process_outcome;
	}
	
	public static function retrieveMissedPaymentsToReplicateOnERP($db, $onManagersIDs, $fromDate = NULL, $toDate = NULL, $withSerials = NULL){
		if ($fromDate && $toDate) {
			$interval['last_payment'] = $from_date;
			$interval['now'] = $to_date;
		} else {
			$sql = "SELECT DATE_FORMAT(STR_TO_DATE( value_par, '%d/%m/%Y' ), '%d/%m/%Y') AS 'last_payment', 
							DATE_FORMAT(CURRENT_TIMESTAMP( ) , '%d/%m/%Y') AS 'now'
					FROM parameter
					WHERE serial_par = 44";

			$interval = $db->getRow($sql);
		}
		
		if($withSerials){
			$exclusions = " AND i.serial_inv IN ($withSerials)";
		}
		
		$sql = "SELECT p.serial_pay, dbm.serial_man
				FROM payments p
				JOIN invoice i ON p.serial_pay = i.serial_pay AND p.status_pay IN ('PAID','VOID','REFUNDED','EXCESS','UNCOLLECTABLE','ONLINE_EFECTIVE')
				JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm AND dbm.serial_man IN ($onManagersIDs)
				WHERE STR_TO_DATE(DATE_FORMAT(date_pay, '%d/%m/%Y'), '%d/%m/%Y') >= STR_TO_DATE('{$interval['last_payment']}', '%d/%m/%Y')
				AND STR_TO_DATE(DATE_FORMAT(date_pay, '%d/%m/%Y'), '%d/%m/%Y') <= STR_TO_DATE('{$interval['now']}', '%d/%m/%Y')
				$exclusions
				GROUP BY p.serial_pay
				ORDER BY p.serial_pay";
		
		$paymentsList = $db ->getAll($sql);
		
		if($paymentsList){
			return $paymentsList;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name migrateWebPaymentToERP
	 * @param type $db
	 * @param type $serial_pay 
	 */
	public static function migrateWebPaymentToERP($db, $serial_pay, $serial_mbc) {
		$process_outcome = 'success';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT,'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$payment = new Payment($db, $serial_pay);
		$payment->getData();
		$excess_amount = $payment->getTotal_payed_pay() - $payment->getTotal_to_pay_pay();

		$invoices_in_payment = Payment::getInvoicesInPayment($db, $payment->getSerial_pay());
		$payment_detail = Payment::getDetailsForPayment($db, $payment->getSerial_pay());

		$sending_array = array();
		$sending_array['TransDate'] = self::formatDateToISO8601($payment->getDate_pay());
		$sending_array['Office'] = self::getStoreID($serial_mbc);
		$sending_array['Departament'] = self::getDepartmentID($serial_mbc);
		$sending_array['Invoices'] = array();
		$sending_array['PayModes'] = array();
		$same_holder = true;
		$payment_lines = count($payment_detail);
		$credit_notes_as_payments = ERPConnectionFunctions::logCreditNotePaymentLines($payment_detail);

		if($invoices_in_payment){
			if($invoices_in_payment['0']['serial_dea'] != ''){
				$current_holder = 'D'.$invoices_in_payment['0']['serial_dea'];
			}else{
				$current_holder = 'C'.$invoices_in_payment['0']['serial_cus'];
			}

			foreach($invoices_in_payment as $invoice){
				if($invoice['serial_dea'] != ''){
					$temp_holder = 'D'.$invoice['serial_dea'];
				}else{
					$temp_holder = 'C'.$invoice['serial_cus'];
				}

				$invoice_number = self::getStoreID($serial_mbc).self::getDepartmentID($serial_mbc).str_pad($invoice['number_inv'], 9, '0', STR_PAD_LEFT);
				$total_for_invoice = $invoice['total_inv'];

				//IF CNOTE APPLY, DISCOUNT IT FROM INVOICE
				if(isset($credit_notes_as_payments[$invoice['serial_inv']])):
					$total_for_invoice -= $credit_notes_as_payments[$invoice['serial_inv']];
				endif;

				array_push($sending_array['Invoices'], array('InvoiceNr'=>$invoice_number,
																'Amount' => $total_for_invoice));

				if($current_holder != $temp_holder){
					$same_holder = false;
				}
			}

			if($excess_amount > 0){
				array_push($sending_array['Invoices'], array('Amount' => $excess_amount));
			}
		}

		if($payment_detail){
			if($same_holder){ // NEW RECEIPT
				foreach($payment_detail as $pDetail){
					//JUMP OVER CNOTES PAYMENTS DUE TO DEFAULT CROSS ON ERP
					if($pDetail['type_pyf'] == 'CREDIT_NOTE'): 
						if($pDetail['serial_ref']){
							$pDetail['type_pyf'] = 'REFUND';
						}else{
							continue;
						}
					endif;

					$temp_line = array();

					$temp_line['PayMode'] = self::translatePASCodesToOOPayments($pDetail['type_pyf']);
					$temp_line['Amount'] = $pDetail['amount_pyf'];

					if($pDetail['type_pyf'] == 'CREDIT_CARD' || $pDetail['type_pyf'] == 'DINERS' || $pDetail['type_pyf'] == 'PAYPHONE'){
						$temp_line['Cheque'] = array('ChequeNr'=> $pDetail['document_number_pyf'],
													'Amount'=> $pDetail['amount_pyf'],
													'Currency'=> 'USD',
													);
					}

					array_push($sending_array['PayModes'], $temp_line);
				}

				$sending_array['CustCode'] = $current_holder;
				curl_setopt($ch, CURLOPT_URL, URL_ERP."api/receipt/");
			}else{ //BANKING RECEIPT
				unset($sending_array['PayModes']); 

				if($payment_lines == 1):
					$sending_array['PayMode'] = self::translatePASCodesToOOPayments($payment_detail['0']['type_pyf']);
				else:
					$sending_array['PayMode'] = 'CAVAR';
				endif;

				$sending_array['RefStr'] = 'COBRO NRO. '.$payment->getSerial_pay();
				curl_setopt($ch, CURLOPT_URL, URL_ERP."api/bankreceipt/");
			}
		}

		//Debug::print_r($sending_array); echo json_encode($sending_array); die;
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars

		$response = curl_exec($ch);
		$curl_response_result = curl_getinfo($ch);
		$oo_ws_response = $curl_response_result['http_code']; //200 success code
		$response = json_decode($response);
		curl_close($ch);
		
		if($oo_ws_response != 201){ //ERRORS ON CREATING INVOICE
			ErrorLog::log($db, 'MIGRATION ERROR - PAYMENT: '.$payment->getSerial_pay(), array('response'=>$response,'array'=>$sending_array));
			$process_outcome = 'error';
		}
		
		return $process_outcome;
	}
	
	
	public static function commitERPInvoicingCustomer($db, $serial_client, $serial_sal = NULL){
		$curl_instance = curl_init();
		curl_setopt($curl_instance, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($curl_instance, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($curl_instance, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_instance, CURLOPT_USERAGENT,'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($curl_instance, CURLOPT_POST, 1);
		
		$prefix = substr($serial_client, 0, 1);
		$entity_id = substr($serial_client, 1, strlen($serial_client));
		
		switch($prefix){
			case 'C':
				$customer = new Customer($db, $entity_id);
				$customer ->getData();
				
				$sending_array['Code'] = 'C'.$customer->getSerial_cus();
				$sending_array['Name'] = utf8_encode($customer->getFirstname_cus().' '.$customer->getLastname_cus());
				$sending_array['TaxRegType'] = ERPConnectionFunctions::retrievePersonType($customer->getDocument_cus());
				$sending_array['TaxRegNr'] = $customer->getDocument_cus();
				$sending_array['IDType'] = ERPConnectionFunctions::retrieveDocumentType($customer->getDocument_cus());
				$sending_array['Phone'] = $customer->getPhone1_cus();
				$sending_array['Mobile'] = $customer->getCellphone_cus();
				$sending_array['City'] = ERPConnectionFunctions::translateCity($db, $customer->getSerial_cit(), $serial_sal);
				$sending_array['Address'] = utf8_encode($customer->getAddress_cus());
				$sending_array['GroupCode'] = '9999';
				
				break;
			case 'D':
				$dealer = new Dealer($db, $entity_id);
				$dealer ->getData();
				$geo_data = $dealer->getAuxData_dea();
				
				$sending_array['Code'] = 'D'.$dealer->getSerial_dea();
				$sending_array['Name'] = utf8_encode($dealer->getName_dea());
				$sending_array['TaxRegType'] = ERPConnectionFunctions::retrievePersonType($dealer->getId_dea());
				$sending_array['TaxRegNr'] = $dealer->getId_dea();
				$sending_array['IDType'] = ERPConnectionFunctions::retrieveDocumentType($dealer->getId_dea());
				$sending_array['Phone'] = $dealer->getPhone1_dea();
				$sending_array['Mobile'] = $dealer->getPhone2_dea();
				$sending_array['City'] = ERPConnectionFunctions::translateCity($db, $geo_data['serial_cit'], $serial_sal);
				$sending_array['Address'] = utf8_encode($dealer->getAddress_dea());
				$sending_array['GroupCode'] = $dealer->getCategory_dea();
				
				break;
		}
		
		$salesMan = self::getResponsibleDataForSale($db, $serial_sal);
		if($salesMan){
			$sending_array['SalesMan'] = $salesMan['username_usr'];
		}
		
		//echo $url; Debug::print_r($sending_array); echo json_encode($sending_array); die;
		$url = URL_ERP . "api/customer/create-or-update/?format=json&CustomerCode=".$serial_client;
		curl_setopt($curl_instance, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
		curl_setopt($curl_instance, CURLOPT_URL, $url);

		$response = curl_exec($curl_instance);
		$curl_response_result = curl_getinfo($curl_instance);
		$oo_ws_response = $curl_response_result['http_code']; //201 success code
		
		if($oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
			ErrorLog::log($db, 'ERP_WS_CLIENT_COMMIT_FAILED', array(	'case' => $prefix, 
																		'id' => $entity_id,
																		'code' => $oo_ws_response,
																		'message' => $response));
			
			return FALSE;
		}else{
			return TRUE;
		}
	}
	
	public static function getResponsibleDataForSale($db, $serial_sal){
		$sql = "SELECT u.username_usr, u.serial_usr
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN user_by_dealer ubd ON ubd.serial_dea = pbd.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr AND s.serial_sal = $serial_sal";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getResponsibleTranslationForVisitsInOO($username){
		switch($username){			
			case 'aalmeida1':
			case 'aalmeidauio':
				return 'aalmeida';
				break;
			case 'avallejo1':
			case 'avallejo2':
				return 'avallejo1';
			case 'sintipungo':
			case 'dalara':
				return 'xamelo';
				break;
			case 'sintipungo':
				return 'intipungo';
				break;
			case 'rromprad':
			case 'rromprad1':
			case 'rromprad2':
				return 'romprand';
				break;
			case 'yaproaño':
			case 'anormaza':
				return 'yaproano';
				break;
			case 'kajativa':
				return 'kjativa';
				break;
			case 'mjaramillo':
				return 'juponce';
				break;
                        case 'bzavala1':
				return 'bzavala';
				break;
			case 'vdirectas':
				return 'WEBSERVICE';
				break;
			default:
				return $username;
				break;
		}
	}
	
	public static function getMBCSerialForSale($db, $serial_sal){
		$sql = "SELECT mbc.serial_mbc
				FROM sales s 
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.serial_sal = $serial_sal
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc on mbc.serial_mbc = b.serial_mbc";
		
		$result = $db ->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getInvoiceBasicDataForSale($db, $serial_sal){
		$sql = "SELECT mbc.serial_man, b.type_percentage_dea, b.percentage_dea, s.total_sal AS 'subtotal', 
						s.total_sal AS 'total', b.serial_cdd AS 'payterm'
				FROM sales s 
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.serial_sal = $serial_sal
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc on mbc.serial_mbc = b.serial_mbc";
		
		$result = $db ->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getFuturePayTotalAmountForSales($db, $salesIds, $includeEmissionRights = FALSE, $includeFarmersTax = FALSE, $useIdSales = FALSE, $custom_discount = 0){
		global $global_EmissionRigths;
		global $globalFarmersTax;
		
		if($useIdSales){
			$evaluationField = " serial_sal ";
		}else{
			$evaluationField = " id_erp_sal ";
		}
		
		$sql = "SELECT sum(total_sal) AS 'total_sold'
				FROM sales 
				WHERE $evaluationField IN ($salesIds)";
		 
		$total_sold = $db->getOne($sql);
		
		if($total_sold){
			$total_sold = $total_sold - ($total_sold * ($custom_discount / 100));
			
			if($includeEmissionRights){
				$salesCount = explode(",", $salesIds);
				
				$totalEmissionsFee = ($global_EmissionRigths['price'] * count($salesCount)) * (1+ ($global_EmissionRigths['VAT']/100));
				$totalInvoice = $total_sold + $totalEmissionsFee; //VAT VALUE
			}else{
				$totalInvoice = $total_sold;
			}
			
			if($includeFarmersTax){
				return round($totalInvoice * (1 + $globalFarmersTax['tax']), 2);
			}else{
				return round($totalInvoice, 2);
			}
		}else{
			return FALSE;
		}
	}
	
	public static function getFuturePaymentCodeForSales($db, $erpIds){
		$sql = "SELECT future_payment_sal 
				FROM sales
				WHERE id_erp_sal in ($erpIds)";
		
		$result = $db->getAll($sql);
		
		$futurePayment = "DP318";
		
		if($result && count($result) == 1){
			if($result[0]['future_payment_sal'] != ''){
				$futurePayment = $result[0]['future_payment_sal'];
			}
		}

		return self::translatePASCodesToOOPayments($futurePayment);
	}
	
	public static function addEmissionRightsItem($base_data) {
		global $global_EmissionRigths;
	   
		$item = array(	
			'ArtCode' => $global_EmissionRigths['ArtCode'],
			'ArtName' => $global_EmissionRigths['ArtName'],
			'Name' => $global_EmissionRigths['Name'],
			'SerialNr' => $global_EmissionRigths['SerialNr'],
			'FromDate' => ERPConnectionFunctions::formatDateToISO8601($base_data['begin_date_sal']),
			'ToDate' => ERPConnectionFunctions::formatDateToISO8601($base_data['end_date_sal']),
			"Price" => number_format((float)$global_EmissionRigths['price'], 2, '.', ''),
			"Cost" => number_format((float)0, 2, '.', '')
		);
       
		return $item;
    }
	
	public static function getMbcIdForSalesToERP($db, $id_erp_sal){
		$sql = "SELECT DISTINCT d.serial_mbc
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN dealer d ON d.serial_dea = pbd.serial_dea
					AND s.id_erp_sal in ($id_erp_sal)";
		
		$result = $db->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
}
?>
