<?php
/**
 * File: BillingDetail
 * Author: Patricio Astudillo
 * Creation Date: 12/12/2013
 * Last Modified: 12/12/2013
 * Modified By: Patricio Astudillo
 */

class BillingDetail{
	var $db;
	var $id;
	var $serial_btm;
	var $serial_sal;
	var $type;
	var $amount;
	
	function __construct($db, $id = NULL) {
		$this->db = $db;
		$this->id = $id;
		
		if($this->id):
			$this->load();
		endif;
	}

	public function load(){
		$sql = "SELECT id, serial_btm, serial_sal, type, amount
				FROM billing_detail
				WHERE id = {$this->id}";
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$this->serial_btm = $result['serial_btm'];
			$this->serial_sal = $result['serial_sal'];
			$this->type = $result['type'];
			$this->amount = $result['amount'];
	
			return TRUE;
		}else{
			ErrorLog::log($this -> db, 'LOAD FAILED - '.get_class($this), $sql);
			return FALSE;
		}
	}
	
	public function save(){
		if($this->id){ //UPDATE
			$sql = "UPDATE billing_detail SET 
						serial_btm = {$this->serial_btm},
						serial_sal = {$this->serial_sal},
						type = '{$this->type}',
						amount = '{$this->amount}'
					WHERE id = {$this->id}";
			
			$result = $this->db->Execute($sql);
		
			if ($result == true){
				return $this->load();
			}else{
				ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
				return false;
			}
		
		}else{ //INSERT
			$sql = "INSERT INTO billing_detail ( 
						id,
						serial_btm,
						serial_sal,
						type,
						amount
					) VALUES (
						NULL,
						{$this->serial_btm},
						{$this->serial_sal},
						'{$this->type}',
						'{$this->amount}'
					)";
			
			$result = $this->db->Execute($sql);
		
			if ($result == true){
				$this->id = $this->db->insert_ID();
				return $this->load();
			}else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
				return false;
			}
		}
	}
	
	/**
	 * 
	 * @param ADODB $db DB connection
	 * @param INT $serial_btm Billing to Manager ID
	 * @param STR $order_by Sorting Field Name
	 * @return boolean 
	 */
	public static function getInvoiceFullDetailBySerialBtm($db, $serial_btm){
		$sql = "SELECT card_number_sal, status_sal, name_pbl, days_sal, total_sal,
						CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
						DATE_FORMAT(in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
						DATE_FORMAT(begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
						DATE_FORMAT(end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr,
						name_dea, bd.type AS 'international_fee_status', bd.amount AS 'international_fee_amount'
				FROM billing_detail bd
				JOIN sales sal ON sal.serial_sal = bd.serial_sal
				JOIN customer cus ON cus.serial_cus = sal.serial_cus
				
				JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				WHERE serial_btm = {$serial_btm}
				ORDER BY in_date_sal";
		
		//Debug::print_r($sql); die;
		$result = $db ->getAll($sql);
		
		if($result){
			return $result;
		} else {
			return false;
		}
	}
	
	public static function getInvoiceHeader($db, $serial_btm){
		$sql = "SELECT name_man, DATE_FORMAT(date_btm, '%d/%m/%Y') AS 'date_btm'
				FROM billing_detail bd
				JOIN billing_to_manager btm ON btm.serial_btm = bd.serial_btm
				JOIN manager m ON m.serial_man = btm.serial_man
				WHERE bd.serial_btm = {$serial_btm}";
		
		//Debug::print_r($sql); die;
		$result = $db ->getRow($sql);
		
		if($result){
			return $result;
		} else {
			return false;
		}
	}

	//************************* GETTERS & SETTERS **********************
	public function getId() {
		return $this->id;
	}

	public function getSerial_btm() {
		return $this->serial_btm;
	}

	public function getSerial_sal() {
		return $this->serial_sal;
	}

	public function getType() {
		return $this->type;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setSerial_btm($serial_btm) {
		$this->serial_btm = $serial_btm;
	}

	public function setSerial_sal($serial_sal) {
		$this->serial_sal = $serial_sal;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function setAmount($amount) {
		$this->amount = $amount;
	}
}
?>
