<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WSSales
 *
 * @author Valeria Andino 
 * 22/01/2016
 *  */
class WSSalesTandi {

	var $db;
	var $serial_sal;
	var $serial_con;
	var $serial_cus;
	var $sal_serial_sal;
	var $serial_inv;
	var $serial_cnt;
	var $serial_pbd;
	var $serial_cur;
	var $serial_cit;
	var $card_number_sal;
	var $emission_date_sal;
	var $begin_date_sal;
	var $end_date_sal;
	var $in_date_sal;
	var $days_sal;
	var $fee_sal;
	var $observations_sal;
	var $cost_sal;
	var $free_sal;
	var $id_erp_sal;
	var $status_sal;
	var $type_sal;
	var $stock_type_sal;
	var $total_sal;
	var $direct_sale_sal;
	var $total_cost_sal;
	var $change_fee_sal;
	var $international_fee_status;
	var $international_fee_used;
	var $international_fee_amount;
	var $aux;
	var $deliveredKit;
	var $bodyRep;
	var $rep;

	function __construct($db, $serial_sal = NULL, $serial_cus = NULL, $serial_pbd = NULL, $serial_cnt = NULL, $serial_inv = NULL, $serial_cur = NULL, $serial_cit = NULL, $card_number_sal = NULL, $emission_date_sal = NULL, $begin_date_sal = NULL, $end_date_sal = NULL, $in_date_sal = NULL, $days_sal = NULL, $fee_sal = NULL, $observations_sal = NULL, $cost_sal = NULL, $free_sal = NULL, $id_erp_sal = NULL, $status_sal = NULL, $type_sal = NULL, $stock_type_sal = NULL, $total_sal = NULL, $direct_sale_sal = NULL, $total_cost_sal = NULL, $change_fee_sal = NULL, $sal_serial_sal = NULL, $international_fee_status = NULL, $international_fee_used = NULL, $international_fee_amount = NULL, $serial_con = NULL,$rep='NO',$bodyRep=NULL) {
		$this->db = $db;
		$this->serial_sal = $serial_sal;
		$this->serial_con = $serial_con;
		$this->serial_cus = $serial_cus;
		$this->serial_pbd = $serial_pbd;
		$this->serial_cnt = $serial_cnt;
		$this->serial_inv = $serial_inv;
		$this->serial_cur = $serial_cur;
		$this->serial_cit = $serial_cit;
		$this->card_number_sal = $card_number_sal;
		$this->emission_date_sal = $emission_date_sal;
		$this->begin_date_sal = $begin_date_sal;
		$this->end_date_sal = $end_date_sal;
		$this->in_date_sal = $in_date_sal;
		$this->days_sal = $days_sal;
		$this->fee_sal = $fee_sal;
		$this->observations_sal = $observations_sal;
		$this->cost_sal = $cost_sal;
		$this->free_sal = $free_sal;
		$this->id_erp_sal = $id_erp_sal;
		$this->status_sal = $status_sal;
		$this->type_sal = $type_sal;
		$this->stock_type_sal = $stock_type_sal;
		$this->total_sal = $total_sal;
		$this->direct_sale_sal = $direct_sale_sal;
		$this->total_cost_sal = $total_cost_sal;
		$this->change_fee_sal = $change_fee_sal;
		$this->sal_serial_sal = $sal_serial_sal;
		$this->international_fee_status = $international_fee_status;
		$this->international_fee_used = $international_fee_used;
		$this->international_fee_amount = $international_fee_amount;
		$this->deliveredKit = $deliveredKit;
		$this->rep=$rep;
		$this->bodyRep=$bodyRep;
	}

	/*	 * *********************************************
	  @Name: getData
	  @Description: Returns all data
	  @Params: N/A
	  @Returns: true/false
	 * ********************************************* */

	function getData() {
		if ($this->serial_sal != NULL) {
			$sql = "SELECT s.serial_sal,
                        s.serial_cus,
                        s.serial_pbd,
                        s.serial_cnt,
                        s.serial_inv,
                        s.serial_cur,
                        s.serial_cit,
                        s.card_number_sal,
                        DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y %T'),
                        DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),
                        DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),
                        DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),
                        s.days_sal,
                        s.fee_sal,
                        s.observations_sal,
                        s.cost_sal,
                        s.free_sal,
                        s.id_erp_sal,
                        s.status_sal,
                        s.type_sal,
                        s.stock_type_sal,
                        s.total_sal,
                        s.direct_sale_sal,
                        s.total_cost_sal,
                        s.change_fee_sal,
                        s.sal_serial_sal,
                        s.international_fee_status,
                        s.international_fee_used,
                        s.international_fee_amount,
                        s.deliveredKit,
                        s.serial_con
                    FROM sales s
                    WHERE s.serial_sal ='" . $this->serial_sal . "'";
			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_sal = $result->fields[0];
				$this->serial_cus = $result->fields[1];
				$this->serial_pbd = $result->fields[2];
				$this->serial_cnt = $result->fields[3];
				$this->serial_inv = $result->fields[4];
				$this->serial_cur = $result->fields[5];
				$this->serial_cit = $result->fields[6];
				$this->card_number_sal = $result->fields[7];
				$this->emission_date_sal = $result->fields[8];
				$this->begin_date_sal = $result->fields[9];
				$this->end_date_sal = $result->fields[10];
				$this->in_date_sal = $result->fields[11];
				$this->days_sal = $result->fields[12];
				$this->fee_sal = $result->fields[13];
				$this->observations_sal = $result->fields[14];
				$this->cost_sal = $result->fields[15];
				$this->free_sal = $result->fields[16];
				$this->id_erp_sal = $result->fields[17];
				$this->status_sal = $result->fields[18];
				$this->type_sal = $result->fields[19];
				$this->stock_type_sal = $result->fields[20];
				$this->total_sal = $result->fields[21];
				$this->direct_sale_sal = $result->fields[22];
				$this->total_cost_sal = $result->fields[23];
				$this->change_fee_sal = $result->fields[24];
				$this->sal_serial_sal = $result->fields[25];
				$this->international_fee_status = $result->fields[26];
				$this->international_fee_used = $result->fields[27];
				$this->international_fee_amount = $result->fields[28];
				$this->deliveredKit = $result->fields[29];
				$this->serial_con = $result->fields[30];

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * function insert
	  @Name: insert
	  @Description: Inserts a new register in data base
	  @Params:
	 *       N/A
	  @Returns: insertedID
	 *          or
	 *        false
	 * ********************************************* */

	function insert() {
		$sql = "INSERT INTO sales (
                                serial_cus,
                                
                                serial_pbd,
                                serial_cnt,
                                serial_inv,
								serial_cur,
                                serial_cit,
                                card_number_sal,
                                emission_date_sal,
                                begin_date_sal,
                                end_date_sal,
                                in_date_sal,
                                days_sal,
                                fee_sal,
                                observations_sal,
                                cost_sal,
                                free_sal,
                                id_erp_sal,
                                status_sal,
                                type_sal,
                                stock_type_sal,
                                total_sal,
                                direct_sale_sal,
                                total_cost_sal,
                                change_fee_sal,
                                sal_serial_sal,
                                international_fee_status,
                                international_fee_used,
                                international_fee_amount,
                                deliveredKit)
                VALUES( '" . $this->serial_cus . "',
                          
                          '" . $this->serial_pbd . "',
                          '" . $this->serial_cnt . "',";
		if ($this->serial_inv != '') {
			$sql.= "'" . $this->serial_inv . "',";
		} else {
			$sql.= "NULL,";
		}
		if ($this->serial_cur != '') {
			$sql.= "'" . $this->serial_cur . "',";
		} else {
			$sql.= "1,";
		}
		if ($this->serial_cit != 'NULL') {
			$sql.="'" . $this->serial_cit . "',";
		} else {
			$sql.= "NULL,";
		}
		if ($this->card_number_sal == 'N/A') {
			$sql.="NULL,";
		} else {
			$sql.="'" . $this->card_number_sal . "',";
		}
		$sql.="STR_TO_DATE(CONCAT('" . $this->emission_date_sal . "',' ',(SELECT curtime())),'%d/%m/%Y %T'),
                        STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
                        STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y'),
                        CURRENT_TIMESTAMP(),
                        '" . $this->days_sal . "',
                        '" . ($this->fee_sal ? $this->fee_sal : "0.00") . "',
                        '" . $this->observations_sal . "',
                        '" . ($this->cost_sal ? $this->cost_sal : "0") . "',
                        '" . $this->free_sal . "',
                        " . ($this->id_erp_sal ? "'" . $this->id_erp_sal . "'" : "NULL") . ",
                        '" . $this->status_sal . "',
                        '" . $this->type_sal . "',
                        '" . $this->stock_type_sal . "',
                        '" . ($this->total_sal ? $this->total_sal : "0.00") . "',
                        " . ($this->direct_sale_sal ? "'" . $this->direct_sale_sal . "'" : "NULL") . ",
                        '" . ($this->total_cost_sal ? $this->total_cost_sal : "0") . "',
                        '" . $this->change_fee_sal . "',
                        " . ($this->sal_serial_sal ? "'" . $this->sal_serial_sal . "'" : "NULL") . ",
                        '" . $this->international_fee_status . "',
                        '" . $this->international_fee_used . "',
                        '" . ($this->international_fee_amount ? $this->international_fee_amount : "0.00") . "',
                        '" . $this->deliveredKit . "')";
		// echo $sql; die();             
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	//Max Card

	public static function getNextStockNumber($db, $captureCardNumber) {//IF TRUE, THE CARD NUMBER RETRIEVED IS TAKEN EXCLUSIVELY FOR THIS SALE.
		$cardNumber = self::retrieveNextStockValue($db, $captureCardNumber);

		//WE MUST GUARANTEE IT'S A VALID NUMBER FOR THE SALE
		if (!$cardNumber ||
				$cardNumber == NULL ||
				trim(strtolower($cardNumber)) == '' ||
				trim(strtolower($cardNumber)) == 'null' ||
				intval($cardNumber) < 1) {

			ErrorLog::log($db, 'CORRUPT CARD NUMBER', $cardNumber);
			return false; //TODO HANDLE BAD CARD NUMBER DELIVERY
		}

		if (self::isCardNumberNotInUse($db, $cardNumber)) {
			return $cardNumber;
		} else {
			ErrorLog::log($db, 'CARD NUMBER IS REPEATED!!!!', $cardNumber);
			return false; //TODO HANDLE REPEATED CARD NUMBER DELIVERY
		}
	}

	public static function retrieveNextStockValue($db, $captureCardNumber) {
		global $globalSTOCK_MAX_VALUE_PARAMETER;
		$conditionString = '+1';
		if ($captureCardNumber) {
			$db->Execute("UPDATE parameter set value_par = value_par +1 WHERE serial_par = $globalSTOCK_MAX_VALUE_PARAMETER");
			$conditionString = '';
		}

		$sql = "SELECT value_par$conditionString FROM parameter WHERE serial_par = $globalSTOCK_MAX_VALUE_PARAMETER";
		$nextValue = $db->getOne($sql);

		return $nextValue;
	}

	function isCardNumberNotInUse($db, $cardNumber) {
		$sql = "SELECT 1 FROM sales WHERE card_number_sal = $cardNumber";
		$result = $db->getOne($sql);
		return $result != 1;
	}

	public static function processCardInformationFromWebSale($db, $productArray, $counterSerial, $validityPeriod, $serial_lang, $rep,$bodyRep,$payment_form = 'DINERS MERCHANT', $serial_cup, $cupon_applied) {
//		Debug::print_r('hello');die();
		$purchase_array = array();
		if(is_array($productArray) && count($productArray)>0){
			foreach ($productArray as $prod) {
				$prod = get_object_vars($prod);
				ErrorLog::log($db, 'WEB SALES INIT 42', $prod);
				
				if (true) { //TODO: VALIDAR INFORMACION COMPLETA
					$prodByCou = new ProductByCountry($db, $prod['serialPxc']);
					$prodByCou->getData();

					$departureCou = $prod['departureCou'];
					$userPhoneSalesCounter = $counterSerial;

					global $phoneSalesDealer;
					if (!$userPhoneSalesCounter) {
						$phoneCountry = Dealer::getDealersCountrySerial($db, $phoneSalesDealer);
						$userPhoneSalesCounter = Counter::getCountryPhoneSalesCounter($db, $phoneCountry);
					}

					$serialDea = Counter::getCountersTopDealer($db, $userPhoneSalesCounter); //WARNING: DEPENDS ON WHICH COUNTER WE USED BEFORE
					$dealerPhone = new Dealer($db, $serialDea);
					$dealerPhone->getData();
					$counterPhone = new Counter($db, $userPhoneSalesCounter);
					$counterPhone->getData();

					$destination_city = City::getGeneralCityByCountry($db, $prod['serialCou']);

					$dealersCountry = Dealer::getDealersCountrySerial($db, $serialDea);
					$productInTransit = $prodByCou->getSerial_pro();
					//$serialPxc = ProductByCountry::getPxCSerial($db, $dealersCountry, $productInTransit);
                                        $serialPxc = $prod['serialPxc'];
					$serialPbd = ProductByDealer::getPBDbyPXC($db, $serialDea, $serialPxc);

					if (!$serialPbd) {
						ErrorLog::log($db, 'CMS SALE ' . $productInTransit . ' NO PBD ' . $serialPbd, $prod);
						return array(8, 'SYS-NO PBD, CHECK CART PRODUCTS!!' . $serialPxc); //DB FAIL LOG
					}

					//For several_flights = 0
					$validUntil = date("d/m/Y", strtotime($prod['beginDatePod'] . ' +' . $validityPeriod . ' days'));
					$prod['beginDatePod'] = date("d/m/Y", strtotime($prod['beginDatePod']));
					$prod['endDatePod'] = date("d/m/Y", strtotime($prod['endDatePod']));

					//Begin for each card:


                    $l=json_decode(json_encode($prod['cards']),TRUE);

					foreach ( $l as $card) {
                        //$card = get_object_vars($card);
                       // echo $card['holder']['documentCus'].";";

                        //Get the Document Number:
                        $tempMbc = new ManagerbyCountry($db, $dealerPhone->getSerial_mbc());
                        $tempMbc->getData();

                        //Generate the Card Number:
                        $cardNumber = Stock::getNextStockNumber($db, true);
                        Sales::insertLogWSnewSale($db,$counterSerial,$serial_lang,'no data no error',$cardNumber);

                        if (!$cardNumber) {
                            ErrorLog::log($db, 'CMS SALES - NO CARD NUMBER ' . $cardNumber, $prod);
                            Sales::insertLogWSnewSale($db,$counterSerial,$serial_lang,'no data with error',$cardNumber);
                            return array(9, 'STOCK NOT CREATED'); //DB FAIL LOG
                        }

                        //Calculate cost:
                        $totalCost = $prod['costPod'];
                        foreach ($card['extras'] as $extra) {
                           // $extra = get_object_vars($extra);
                            $totalCost += $extra['ownCost'];
                        }

                        //Code fix for serveral flights products:
                        if ($prod['severalFlights'] == 0) {
                            $redirectToFlightsLog = true;
                            $destination_city = 'NULL';
                            $prod['endDatePod'] = $validUntil;

                            if (!Product::generateNumberValidation($db, $productInTransit)) {
                                $cardNumber = 'N/A';
                            }
                        }


						//********************************************** THE HOLDER *************************************:
						$holder = $card['holder'];

						$customerH = new Customer($db, $holder['serialCus']);
						$customerH->setDocument_cus($holder['documentCus']);
						$customerH->setEmail_cus($holder['emailCus']);

						$customerH->setLastname_cus($holder['lastNameCus']);
						$customerH->setFirstname_cus($holder['firstNameCus']);
						$customerH->setType_cus($holder['typeCus']);
						$customerH->setBirthdate_cus($holder['birthdateCus']);
                                                $customerH->setSerial_cit($holder['serialCit']);
						$customerH->setPhone1_cus($holder['phone1Cus']);
						$customerH->setCellphone_cus($holder['cellphoneCus']);
						$customerH->setAddress_cus($holder['addressCus']);
						$customerH->setRelative_cus($holder['relativeCus']);
						$customerH->setRelative_phone_cus($holder['relativePhoneCus']);
						$customerH->setStatus_cus('ACTIVE');
                        $t=$customerH->getData();
                       // Debug::print_r($t);die();
                           if (empty($t)) {

                          // $customrHexists = false;
                               $serialHolder = $customerH->insert();
						} else {
                           //$customrHexists = true;

                               $customerH->update(); //Update previous entered data
                               $serialHolder = $customerH->getSerial_cus();

                            // print_r($t);
                             //print_r("Existe");
						}
						/*if ($customrHexists) {
							$customerH->update(); //Update previous entered data
							$serialHolder = $customerH->getSerial_cus();
						} else {
							$serialHolder = $customerH->insert();
                            print_r("Entro aqui");
						}*/

						if (!$serialHolder) {
							ErrorLog::log($db, 'CMS SALES - HOLDER FAILED TO INSERT', $customerH);
							return array(10, 'HOLDER FAILED TO INSERT', $customerH); //DB FAIL LOG
						}


						//THE ELDERLY QUESTIONS:
						if (isset($holder['answers'])) {
							$answersHolder = $holder['answers'];
							
							if(is_array($answersHolder) && count($answersHolder) > 0){
								$questionList = Question::getActiveQuestions($db, $serial_lang);

								Answer::deleteMyAnswers($db, $serialHolder);
								foreach ($questionList as $q) {
									$answer = new Answer($db);
									$answer->setSerial_cus($serialHolder);
									$answer->setSerial_que($q['serial_que']);
									$ans = $answersHolder[$q['serial_qbl']];
									
									if ($ans) {
										$answer->setYesno_ans($ans);
										if (!$answer->insert()) {
											ErrorLog::log($db, 'CMS SALES - ANSWERS INSERT FAILED', $answer);
											return array(35, 'ANSWERS INSERT FAILED', $answer); //DB FAIL LOG
										}
									}
								}
							}
						}

						//******************** MERGE FOR THE TOTAL TO PAY WITH THE DISCOUNT COUPON ***********************:
						//$fixed_amount_with_coupon: WILL BE THE REAL AMOUNT PAID BY THE CUSTOMER IF HE APPLIED A COUPON IN
						//							HIS PURCHASE.
						/*if ($serial_cup && $cupon_applied) {
							$data = Cupon::getCuponData($db, $serial_cup);
							$promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);
							$previous_limit = $data['limit_amount_cup'];

							if ($data['generating_sal'] != '') { //COUPON CAME FROM A PREVIOUS ONE
								$promo_info['discount_percentage_ccp'] = $promo_info['other_discount_ccp'];
								$promo_info['other_discount_ccp'] = 0;

								//IF THE LIMIT AMOUNT IS GREATER THAN THE SALE, WE USE THE PERCENTAGE OF THE COUPON
								if ($previous_limit > $card['cardPriceProd']) {
									$discount_of_product = number_format(($promo_info['discount_percentage_ccp'] / 100) * $card['cardPriceProd'], 2);
								} else { //IF NOT, WE USE THE LIMIT AMOUNT
									$discount_of_product = $previous_limit;
								}
							} else { //IF NOT
								$discount_of_product = number_format(($promo_info['discount_percentage_ccp'] / 100) * $card['cardPriceProd'], 2);
							}

							$fixed_comments = ' Coupon applied. ' . $promo_info['cupon_prefix_ccp'] . ' - ' . $data['number_cup'];

							$fixed_amount_with_coupon = number_format($card['cardPriceProd'] - $discount_of_product, 2);
						} else {
							$fixed_comments = '';
							$fixed_amount_with_coupon = $card['cardPriceProd'];
						}*/

						if ($serial_cup && $cupon_applied) {
							
                            $data = Cupon::getCuponData($db, $serial_cup);
                            
                            $promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);
                            
                            $previous_limit = $data['limit_amount_cup'];

                            if ($data['generating_sal'] != '') { //COUPON CAME FROM A PREVIOUS ONE
                                $promo_info['discount_percentage_ccp'] = $promo_info[0]['other_discount_ccp'];
                                $promo_info['other_discount_ccp'] = 0;

                                //IF THE LIMIT AMOUNT IS GREATER THAN THE SALE, WE USE THE PERCENTAGE OF THE COUPON
                                if ($previous_limit > $card['cardPriceProd']) {
                                    $discount_of_product = number_format(($promo_info['discount_percentage_ccp'] / 100) * $card['cardPriceProd'], 2);
                                } else { //IF NOT, WE USE THE LIMIT AMOUNT
                                    $discount_of_product = $previous_limit;
                                }
                            } else { //IF NOT
						

                                $discount_of_product =$promo_info[0]['discount_percentage_ccp'];
                                

                            }

                            $fixed_comments = ' Coupon applied. ' . $promo_info['cupon_prefix_ccp'] . ' - ' . $data['number_cup'];

                            $subTotalSalWeb = round((($card['cardPriceProd'] * 100)/100.50), 2, PHP_ROUND_HALF_UP);
                           
                            $subTotalSalDiscount = round(($promo_info[0]['discount_percentage_ccp'] / 100) * $subTotalSalWeb, 2);
                           
                            $totalSSC = $subTotalSalWeb - $subTotalSalDiscount;
                            
                            $subtotalSC = round($totalSSC * 0.005, 2, PHP_ROUND_HALF_UP);
                            
                            $totalSalSC = $totalSSC + $subtotalSC;
                            
                            $fixed_amount_with_coupon = $totalSalSC;
                            
                        } else {
                            $fixed_comments = '';
                            $fixed_amount_with_coupon = $card['cardPriceProd'];
                        }

						//********************************************** THE CONTRACT *************************************:
						//Contract
						if (CONTRACT_LOGIC) {
							$serial_con = null;
							$serial_cus = $serialHolder;
							$begin_date = new DateTime();
							$end_date = new DateTime();

							$contracts_cus = Contracts::getContractByCustomer($db, $serial_cus, 'ACTIVE');
							if ($contracts_cus && $contracts_num = count($contracts_cus) > 0) {
								$departure_date = DateTime::createFromFormat('d/m/Y', $prod['beginDatePod'])->getTimestamp();
								$arrival_date = DateTime::createFromFormat('d/m/Y', $prod['endDatePod'])->getTimestamp();
								$contracts_con = 0;

								foreach ($contracts_cus as $key => $con) {
									$contracts_con++;
									$contract_beg = DateTime::createFromFormat('d/m/Y', $con['begin_date'])->getTimestamp();
									$contract_end = DateTime::createFromFormat('d/m/Y', $con['end_date'])->getTimestamp();

									if ($contract_beg <= $departure_date && $contract_end >= $arrival_date) {
										$serial_con = $con['serial_con'];
										break;
									} elseif ($contracts_con == $contracts_num && $departure_date < $contract_end) {
										$begin_date->setTimestamp($departure_date);
										$end_date->setTimestamp($departure_date);
									}
								}
							}

							//Create Contract
							$contract = new Contracts($db, $serial_con);
							if (!$contract->getBegin_date_con() && !$contract->getEnd_date_con() && !$contract->getNumber_con()) {
								$contract->setSerial_cof(1);
								$contract->setSerial_cus($serial_cus);

								$nextAvailableContract = Stock::getNextContractNumber($db);
								$end_date->add(new DateInterval('P1Y'));

								$contract->setNumber_con($nextAvailableContract);
								$contract->setBegin_date_con($begin_date->format('d/m/Y'));
								$contract->setEnd_date_con($end_date->format('d/m/Y'));
								$contract->setStatus_con('ACTIVE');
								$serial_con = $contract->insert();
							}
						}

                        //***********************LEGAL REPRESENTATIVE*****************//

                        if($rep=="YES") {

                            if(is_array($bodyRep) && count($bodyRep)>0){
                                foreach ($bodyRep as $bdRep) {
                                    $bdRep = get_object_vars($bdRep);

                             //       Debug::print_r($rep);
                               //     Debug::print_r($bdRep['document_cus']);
                            //die();

                                    $customerRep = new Customer($db);
                                    //Debug::print_r($serial_cusRep);
                                    $customerRep->setDocument_cus($bdRep['document_cus']);
                                    $customerRep->setFirstname_cus($bdRep['first_name_cus']);
                                    $customerRep->setLastname_cus($bdRep['last_name_cus']);
                                    $customerRep->setBirthdate_cus($bdRep['birthdate_cus']);
                                    $customerRep->setEmail_cus($bdRep['email_cus']);
                                    // $customerRep->setSerial_cit($_POST['selCityRepresentative']);
                                    $customerRep->setAddress_cus($bdRep['address_cus']);
                                    $customerRep->setPhone1_cus($bdRep['phone1_cus']);

                                    $validateDocRep=$customerRep->existsCustomer($bdRep['document_cus']);

                                    if ($validateDocRep==1) {
                                        //Debug::print_r("Existe el cliente");die();

                                       //Debug::print_r("Si existe");die();
                                        $serial_cusRep = $customerRep->getCustomerSerialbyDocument($bdRep['document_cus']);
                                        $customerRep->setserial_cus($serial_cusRep);
                                        $customerRep->update(); //Update previous entered data
                                        $serialRep = $customerRep->getSerial_cus();
                                    }else{
                                        //Debug::print_r("No existe el cliente");die();
                                        $serialRep = $customerRep->insert();
                                    }

                                   // Debug::print_r($customerRep->getData());
                                   // die();

                                   // $validateDataCustomer=$customerRep->getData();

                                    //Debug::print_r($validateDataCustomer);die();


                                    //if ($customerRep->getData()) {
                                    //    Debug::print_r("Entro en yes");die();
                                    //    $customrHexists = true;
                                   // } else {
                                      //  $customrHexists = false;
                                     //   Debug::print_r("Entro en no");die();
                                   // }
                                   // if ($customrHexists) {
                                    //    $customerRep->update(); //Update previous entered data
                                  //      $serialRep = $customerRep->getSerial_cus();
                                   // } else {
                                   //     $serialRep = $customerRep->insert();
                                 //   }

                                }
                            }
                             //                       Debug::print_r($rep);
                               //                     Debug::print_r($bodyRep['document_cus']);
                                 //                   Debug::print_r($bodyRep['first_name_cus']);
                           // die();


                        }



						//********************************************** THE SALE *************************************:
						$todaysDate = date('d/m/Y');
						$sale = new Sales($db);
						$sale->setSerial_cus($serialHolder);
						$sale->setSerial_con($serial_con);
						$sale->setSerial_pbd($serialPbd);
						$sale->setSerial_cit($destination_city);
						$sale->setSerial_rep($serialRep);
						$sale->setSerial_cnt($counterSerial);
						$sale->setSerial_inv(NULL);
						$sale->setCardNumber_sal($cardNumber);
						$sale->setEmissionDate_sal($todaysDate);
						$sale->setBeginDate_sal($prod['beginDatePod']);
						$sale->setEndDate_sal($prod['endDatePod']);
						$sale->setInDate_sal($todaysDate);
						$sale->setDays_sal($prod['daysPod']);
						$sale->setFee_sal($holder['pricePod']);
						$sale->setObservations_sal("Venta Web " . $fixed_comments);
						$sale->setCost_sal($holder['costPod']);
						$sale->setFree_sal('NO');
						$sale->setIdErpSal_sal(NULL);
						$sale->setStatus_sal($prod['statusSale']);
						$sale->setType_sal('WEB');
						$sale->setTotal_sal($card['cardPriceProd']);
						$sale->setStockType_sal('VIRTUAL');
						$sale->setDirectSale_sal($sale->isDirectSale(NULL)); //TODO Make direct sale YES with default dealer in EC
						$sale->setChange_fee_sal(1);
						$sale->setTotalCost_sal($card['costPricePod']);
						$sale->setSerial_cur($prod['serial_cur']);
						$sale->setTransaction_Id($prod['transactionId']);

						if ($dealersCountry != 62) {
							$sale->setInternationalFeeStatus_sal('PAID');
							$sale->setInternationalFeeUsed_sal('YES');
						} else {
							$sale->setInternationalFeeStatus_sal('TO_COLLECT');
							$sale->setInternationalFeeUsed_sal('NO');
						}
						$sale->setInternationalFeeAmount_sal($totalCost);
						$sale->setFuture_payment_sal('CREDIT_CARD');
						$serialSale = $sale->insert();

						array_push($purchase_array, array('idSale'=>$serialSale, 'card'=>$cardNumber, 'holderID'=>$serialHolder));
						$holder['statementInfo']['cardNumber'] = $cardNumber;
						$statementS = self::sendStatement($holder['statementInfo']);
						$fileStatement = self::bindStatement('customerHasStatement', $holder['statementInfo']['customerDocument'], $holder['statementInfo']['cardNumber'], 'BAS_NEW_SALE');
						if (!$serialSale)
							return array(12, 'INSERT FAILED', $sale); //DB FAIL LOG

//						ERP_logActivity('SALES', $serialSale); //ERP ACTIVITY
//                                                $tandiResponse = TandiConnectionFunctions::tandiEmision($serialSale);
						$sale->setSerial_sal($serialSale);
						$sale->getData();
//						ErrorLog::log($db, 'WEB_ERP_SALEID', $sale->getIdErp_sal());
						$sale->update();
                                                
						/********************************************* CLAIM CUPON *************************************
						  if($serial_cup && $cupon_applied == 0){
						  $result = Cupon::reclaimCupon($db, $serial_cup, $serialSale, 4); //serial user 4 Juan Fernando Ponce
						  if($result['process_code'] == 'success' && $result['serial_cup'] != ''){
						  $_SESSION['serial_cup'] = $result['serial_cup'];
						  }
						  $cupon_applied = 1;
						  } */ //TODO: ADD COUPON LOGIC IN SALE
						//********************************************* ADICIONAL BENEFITS *************************************
						foreach ($card['services'] as $key => $services) {
							$service = get_object_vars($services);
							$services = new ServicesByCountryBySale($db);
							$services->setSerial_sbd($service['serialSbd']);
							$services->setSerial_sal($serialSale);
							$servicesInsert = $services->insert();

							if (!$servicesInsert) {
								return array(13, 'SERVICES SAVE FAILED', $services); //DB FAIL LOG
							}
						}

						//********************************************** THE EXTRAS *************************************:
						if (count($card['extras']) > 0) {
						   // Debug::print_r("Entro en extras");
							foreach ($card['extras'] as $key => $extras) {
								//$extra = get_object_vars($extras);
								$customerX = new Customer($db);
								//Debug::print_r($extra['idExtra']);die();
								$customerX->setDocument_cus($extras['idExtra']);
								$customerX->setSerial_cit($holder['serialCit']);

								if (!$customerX->existsCustomer($extras['idExtra'], NULL)) { //If exists we dont update
									$customerX->setType_cus('PERSON');
									$customerX->setFirstname_cus($extras['nameExtra']);
									$customerX->setLastname_cus($extras['lastnameExtra']);
									$customerX->setBirthdate_cus($extras['birthdayExtra']);
									$customerX->setEmail_cus($extras['emailExtra']);
									$customerX->setStatus_cus('ACTIVE');
									$serialCustomer = $customerX->insert();
								} else {
									$customerX->getData(true);
									$serialCustomer = $customerX->getSerial_cus();
								}

								if (!$serialCustomer) {
									return array(14, 'EXTRA SAVE FAILED', $customerX); //DB FAIL LOG
								}

								$extras['statementInfo']['cardNumber'] = $cardNumber;
								$statementES = self::sendStatement($extras['statementInfo']);
								$fileStatement = self::bindStatement('customerHasStatement', $extras['statementInfo']['customerDocument'], $extras['statementInfo']['cardNumber'], 'BAS_NEW_SALE');
								//THE ELDERLY QUESTIONS:
								if (isset($extras['answers'])) {
									$answersExtra = $extras['answers'];

									if(is_array($answersExtra) && count($answersExtra) > 0){
										$questionList = Question::getActiveQuestions($db, $serial_lang);

										Answer::deleteMyAnswers($db, $serialCustomer);
										foreach ($questionList as $q) {
											$answer = new Answer($db);
											$answer->setSerial_cus($serialCustomer);
											$answer->setSerial_que($q['serial_que']);
											$ans = $answersHolder[$q['serial_qbl']];

											if ($ans) {
												$answer->setYesno_ans($ans);
												if (!$answer->insert()) {
													ErrorLog::log($db, 'CMS SALE ERROR - 30', $payment);
												}
											}
										}
									}
								}

								if ($prod['thirdPartyRegisterPro'] == 'YES') {
									$travelerLog = new TravelerLog($db);
									$travelerLog->setSerial_cus($serialCustomer);
									$travelerLog->setSerial_sal($serialSale);
									$travelerLog->setCard_number_trl(NULL);
									$travelerLog->setStart_trl($prod['beginDatePod']);
									$travelerLog->setEnd_trl($prod['endDatePod']);
									$travelerLog->setDays_trl($prod['daysPod']);
									$travelerLog->setSerial_cnt($userPhoneSalesCounter);
									$travelerLog->setSerial_cit($destination_city);
									$travelerID = $travelerLog->insert();

									if (!$travelerID)
										return array(15, 'CMS - TRAVELER LOG INSERT FAILED', $travelerLog); //DB FAIL LOG
								}else {

									$newExtra = new Extras($db);
									$newExtra->setSerial_sal($serialSale);
									$newExtra->setSerial_cus($serialCustomer);
									$newExtra->setSerial_trl(NULL);
									$newExtra->setRelationship_ext($extras['selRelationship']);
									$newExtra->setFee_ext($extras['ownPrice']);
									$newExtra->setCost_ext($extras['ownCost']);
									$extraId = $newExtra->insert();

									if (!$extraId)
										return array(16, 'CMS - EXTRA INSERT FAILED', $newExtra); //DB FAIL LOG
								}
							}
						}
						
						if($prod['statusSale'] == 'REGISTERED'){
							/************************ CUSTOM INVOICING DATA ************************/
							if(isset($card['invoicingData'])){
								$card['invoicingData'] = $card['invoicingData'];
								
								$tempCux = new Customer($db);
								$customInvoiceID = Customer::getSerialCustomerByDocument($db, $card['invoicingData']['DNI']);
							
								if(!$customInvoiceID){
									$tempCux->setDocument_cus($card['invoicingData']['DNI']);
									
									$fullNameSet = explode(' ', $card['invoicingData']['NAMES']);
									switch(count($fullNameSet)){
										case 2:
											$tempCux->setFirstname_cus(utf8_decode($fullNameSet[0]));
											$tempCux->setLastname_cus(utf8_decode($fullNameSet[1]));
											
											break;
										case 3: 
											$tempCux->setFirstname_cus(utf8_decode($fullNameSet[0].' '.$fullNameSet[1]));
											$tempCux->setLastname_cus(utf8_decode($fullNameSet[2]));
											
											break;
										case 4:
											$tempCux->setFirstname_cus(utf8_decode($fullNameSet[0].' '.$fullNameSet[1]));
											$tempCux->setFirstname_cus(utf8_decode($fullNameSet[2].' '.$fullNameSet[3]));
											
											break;
										default:
											$tempCux->setFirstname_cus(utf8_decode($fullNameSet[0]));
											break;
									}
									
									
									$tempCux->setAddress_cus(utf8_decode($card['invoicingData']['ADDRESS']));
									$tempCux->setPhone1_cus($card['invoicingData']['PHONE']);
									$tempCux->setSerial_cit($card['invoicingData']['CITY']);
									$tempCux->setType_cus($card['PERSON']);
									$tempCux->setEmail_cus($card['invoicingData']['EMAIL']);

									$customInvoiceID = $tempCux->insert();
								}
								
								if($customInvoiceID){
									$serialHolder = $customInvoiceID;
								}
							}
							
							//********************************************** THE INVOICE *************************************:
							//*************** MAKE THE CALL FOR ERP CONNECTION AND REPLICATION ****************
							$erp_misc_data['serial_sal'] = $serialSale;
                                                        if($discount_of_product == null){
                                                            $erp_misc_data['txtDiscount'] = 0;
                                                        }else{
                                                            $erp_misc_data['txtDiscount'] = $discount_of_product;
                                                        }
							$erp_misc_data['txtOtherDiscount'] = 0;
							$erp_misc_data['hdnSerialCus'] = $serialHolder;
							$erp_misc_data['hdnSerialMan'] = 20;
							$erp_misc_data['bill_to_radio'] = 'CUSTOMER';
							$erp_misc_data['pago'] = $fixed_amount_with_coupon;
							$erp_misc_data['idpago'] = $prod['transactionId'];
                                                        $erp_misc_data['hdnSaleId'] = array();
							array_push($erp_misc_data['hdnSaleId'], $serialSale);
//                                                        Debug::print_r($erp_misc_data);die();
                            Sales::insertLogWSnewSale($db,$counterSerial,$serial_lang,serialize($erp_misc_data),$cardNumber);
                                                        $tandiResponse = TandiConnectionFunctions::tandiEmisionWeb($erp_misc_data);
							$number = $tandiResponse['numeroFactura'];

							if ($tandiResponse) {
								//taxes:
								$tbc = new TaxesByProduct($db, $prod['serialPxc']);
								$taxes = $tbc->getTaxesByProduct();
								$taxesApplied = 0;
								if ($taxes) {
									foreach ($taxes as $k => $n) {
										$misc[$k]['name_tax'] = $n['name_tax'];
										$misc[$k]['tax_percentage'] = $n['percentage_tax'];
										$taxesToApply += intval($n['percentage_tax']);
									}
								}

								//DOCUMENT_USED
								$serialDbm = DocumentByManager::retrieveSelfDBMSerial($db, $tempMbc->getSerial_man(), 'PERSON', 'INVOICE');

								$invoice = new Invoice($db);
								$invoice->setSerial_cus($serialHolder);
								$invoice->setSerial_pay(NULL);
								$invoice->setSerial_dea(NULL);
								$invoice->setSerial_dbm($serialDbm);
								$invoice->setDate_inv($todaysDate);
								$invoice->setDue_date_inv($todaysDate);
								$invoice->setNumber_inv($number);
								$invoice->setComision_prcg_inv($dealerPhone->getPercentage_dea());
								$invoice->setDiscount_prcg_inv(NULL);
								$invoice->setOther_dscnt_inv($discount_of_product);
								$invoice->setComments_inv("Factura Web Replicada al ERP");
								$invoice->setPrinted_inv('NO');
								$invoice->setStatus_inv('STAND-BY');
								$invoice->setSubtotal_inv($fixed_amount_with_coupon);
								$invoice->setTotal_inv($invoice->getSubtotal_inv() * (1 + $taxesToApply / 100));
								$invoice->setHas_credit_note_inv('NO');
								$invoice->setApplied_taxes_inv(serialize($misc));
//								$invoice->setErp_id($erp_response['erp_id']);
								$invoice->setErp_id($tandiResponse['idFactura']);
								$serialInv = $invoice->insert();
								if (!$serialInv)
									ErrorLog::log($db, 'CMS SALE ERROR - 34', $payment);
								$invoice->setSerial_inv($serialInv);

								$sale->setSerial_inv($serialInv);
								$sale->update();

								//********************************************** THE PAYMENT *************************************:
								$payment = new Payment($db);
								$payment->setTotal_to_pay_pay($fixed_amount_with_coupon);
								$payment->setTotal_payed_pay($fixed_amount_with_coupon);
								$payment->setStatus_pay('PAID');
								$payment->setDate_pay($todaysDate);
								$payment->setObservation_pay('Pago realizado por PayPal');
								$payment->setExcess_amount_available_pay(0);
								$paymentId = $payment->insert();
								if (!$paymentId)
									ErrorLog::log($db, 'CMS SALE ERROR - 31', $payment);

								Payment::registerEfectivePaymentDate($db, $paymentId);

								//UPDATE INVOICE STATUS
								$invoice->setStatus_inv('PAID');
								$invoice->setSerial_pay($paymentId);
								$invoice->update();

								//Update Sale
								$sale->setStatus_sal('ACTIVE');
								$sale->update();

								//THE PAYMENT DETAIL:
								$paymentDetail = new PaymentDetail($db);
								$paymentDetail->setSerial_pay($paymentId);
								$paymentDetail->setSerial_cn(NULL);
								$paymentDetail->setSerial_usr($counterPhone->getSerial_usr());
								$paymentDetail->setType_pyf('DINERS');
								$paymentDetail->setAmount_pyf($fixed_amount_with_coupon);
								$paymentDetail->setDate_pyf($todaysDate);
								$paymentDetail->setDocumentNumber_pyf($prod['transactionId']);
								$paymentDetail->setComments_pyf('');
								$paymentDetail->setStatus_pyf('ACTIVE');
								$paymentDetail->setObservation_pyf(NULL);
								$serialPaymentDetail = $paymentDetail->insert();
								if (!$serialPaymentDetail)
									ErrorLog::log($db, 'CMS SALE ERROR - 32', $paymentDetail);

//								$replicatePaymentResult = ERPConnectionFunctions::migrateWebPaymentToERP($db, $paymentId, $tempMbc->getSerial_mbc());
//								if ($replicatePaymentResult != 'success')
//									ErrorLog::log($db, 'CMS SALE ERROR - 33', $paymentId);


								//********************************************** THE PDF CONTRACT *************************************:
								$serial_generated_sale = $serialSale;
								$display = 0;
								$urlContract = 'contract_' . $serialSale . '.pdf';
								include(DOCUMENT_ROOT . 'modules/sales/pPrintSalePDF.php');

								//Send contract by mail:
								$misc['subject'] = 'NOTIFICACION DE COMPRA';
								$misc['customer'] = $customerH->getFirstname_cus() . ' ' . $customerH->getLastname_cus();
								$misc['cardNumber'] = $cardNumber;
								$misc['username'] = $customerH->getEmail_cus();
								$misc['email'] = $customerH->getEmail_cus();

								/* GENERAL CONDITIONS FILE */
								$country_of_sale = Sales::getCountryofSale($db, $serialSale);
								if ($country_of_sale != 62) {
									$country_of_sale = 1;
								}

								$misc['urlGeneralConditions'] = $sale->getGeneralConditionsbySale($serialPbd, $serialSale, $country_of_sale, $serial_lang);
								if (CONTRACT_LOGIC):
									//Printing Contract
									$misc['urlYearlyContract'] = 'yearlycontract_' . $serial_con . '.pdf';
									include(DOCUMENT_ROOT . 'modules/sales/pPrintContractPDF.php');
								endif;

								

								/* END GENERAL CONDITIONS FILE */
								$misc['urlContract'] = $urlContract;
								$password = $customerH->getPassword_cus();

								if ($password) {
									$password = 'Su clave es la misma de la &uacute;ltima vez.';
								} else {
									$password = GlobalFunctions::generatePassword(6, 8, false);
									$customerH->setPassword_cus($password);
									$customerH->update();
								}
								$misc['pswd'] = $password;

								if (Sales::isPlanetAssistSale($db, $serialSale)) {
									GlobalFunctions::sendMail($misc, 'newClient');
								} else {
									GlobalFunctions::sendMail($misc, 'new_sale');
								}

								
							}else{
								return array(18, 'NO ERP NUMBER FOR INVOICE');
							}
                            //Debug::print_r($v);die();

							//print_r($v);
								//return array(50, 'SUCCESS', $purchase_array);
						}


					} //END FOREACH CARDS FOREACH


				}//End IF
			}
			
			return array('50', 'SUCCESS', $purchase_array);
		}else{
			return array(7, 'NO INPUT DATA');
		}
	}
	public function sendStatement($statemenInfo)
	{


		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://23.239.12.79:8080/api/quiz/statements',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => json_encode($statemenInfo),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$res = json_decode($response, true);

		$statementFilter = new stdClass();
		$statementFilter->statementId = $res['data']['statementId'];
		$statementFilter->cardNumber = "";
		$statementFilter->customerDocument = "";

		$myJSON = json_encode($statementFilter);

		self::getStatementPreview($myJSON);
	}

	public function getStatementPreview($statementFilter)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://23.239.12.79:8080/api/quiz/statements/preview',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => $statementFilter,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		//echo $response;
		//Debug::print_r($response);
		//Debug::print_r('Statement detail');
		$res = json_decode($response, true);
		//Debug::print_r($res['statement']);

		$statementDataSource = json_encode($res['statement']);
		self::createPDF($statementDataSource);
	}

	public function createPDF($statementDataSource)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://23.239.12.79:8080/api/quiz/statements/pdf/create',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => $statementDataSource,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		//echo $response;
	}

	public function bindStatement($action, $customerDocument, $cardNumber, $systemOrigin)
	{
		if ($action === 'customerHasStatement') {
			$customerDocument = trim($customerDocument);
			$cardNumber = trim($cardNumber);
			$systemOrigin = $systemOrigin ? trim($systemOrigin) : null;
			$statement = new Statement();
			$response = $statement->existCustomerStatement($cardNumber, $customerDocument, $systemOrigin);
			//echo json_encode($response);
		}

		// 	if ($action === 'changeStatementStatus') {
		// 		$statementId = trim($statementId);
		// 		$statementStatus = trim($statementStatus);
		// 		$statement = new Statement();
		// 		$response = $statement->changeStatementStatus($statementId, $statementStatus);
		// 		//$statement->sendCustomerStatementEmail($response['statement']);
		// 		echo json_encode($response);
		// 		exit();
		// 	}

		// 	if ($action === 'sendCustomerStatementEmail') {
		// 		$statementId = trim($statementId);
		// 		$statement = new Statement();
		// 		$response = $statement->sendCustomerStatementEmail($statementId);
		// 		echo json_encode($response);
		// 		exit();
		// 	}

		// 	if ($action === 'getCustomerStatementsByDocument') {
		// 		$customerDocument = trim($customerDocument);
		// 		$statement = new Statement();
		// 		$response = $statement->getCustomerStatementsByDocument($customerDocument);
		// 		echo json_encode($response);
		// 		exit();
		// 	}

		// 	if ($action === 'getCustomerStatementDataTable') {
		// 		$statements = json_decode($_POST['statements'], true);
		// 		$customerStatements = array();
		// 		$i = 0;

		// 		// Show only the records that have an associated link from the statement file.
		// 		if (is_array($statements)) {
		// 			foreach ($statements as $statement) {
		// 				//Get customer section from full response and rebuild in a new array to template.
		// 				$customerStatements[$i] = $statement['customer'];
		// 				$customerStatements[$i]['system_origin_es'] ='VENTA WEB';
		// 				$customerStatements[$i]['disabled'] = $customerStatements[$i]['status'] === 'INACTIVE' || $customerStatements[$i]['statement_link'] === '' ? 'disabled' : '';
		// 				// $customerStatements[$i]['hidden'] = $customerStatements[$i]['status'] === 'INACTIVE' || $customerStatements[$i]['statement_link'] === '' ? 'hidden' : '';
		// 				$i++;
		// 			}
		// 		}

		// }
	}
	/*
	 * getCardsByCustomer
	 * @param: $db - DB connection
	 * @param: $serial_cus - Customer ID
	 * @return: An array of all customers that bought any Planet Assist card.
	 */

	public static function getCardsByCustomer($db, $serial_cus, $own_branch_cards_only = FALSE, $exclude_free = FALSE) {
		if ($own_branch_cards_only) {
			$restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
		}
		
		if($exclude_free):
			$exclude_free_statement = " AND s.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED')";
		endif;

        $sql = "SELECT DISTINCT * FROM 
				(
					(SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.serial_pro, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal', ifnull(DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y'),DATE_FORMAT(trl.in_date_trl,'%d/%m/%Y')) as 'emision'
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal, DATEDIFF(i.due_date_inv,NOW()) as days_to_dueDate, HOUR(TIMEDIFF(NOW(),s.emission_date_sal)) as days_since_emition,
						 s.serial_cnt, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con, s.serial_inv, i.number_inv, i.status_inv, DATE_FORMAT(i.due_date_inv,'%d/%m/%Y') as vencimiento, s.serial_cus, wrc.wr_id as 'serial_cou', cou.name_cou
				  FROM customer c
				  JOIN sales s ON s.serial_cus=c.serial_cus	
				  JOIN city cit on s.serial_cit = cit.serial_cit
				  LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
				  JOIN country cou on cit.serial_cou = cou.serial_cou
				  LEFT JOIN invoice i ON s.serial_inv = i.serial_inv
				  $restrict_own_cards
				  $exclude_free_statement
				  LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND trl.serial_cus = '" . $serial_cus . "'
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='2'
				  WHERE c.serial_cus = '" . $serial_cus . "') 

				  UNION

				  (SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.serial_pro, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal', 
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal, DATEDIFF(i.due_date_inv,NOW()) as days_to_dueDate, HOUR(TIMEDIFF(NOW(),s.emission_date_sal)) as days_since_emition,
						 s.serial_cnt, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con, s.serial_inv, i.number_inv, i.status_inv, DATE_FORMAT(i.due_date_inv,'%d/%m/%Y') as vencimiento, trl.serial_cus, wrc.wr_id as 'serial_cou', cou.name_cou
				  FROM customer c
				  LEFT JOIN traveler_log trl ON trl.serial_cus=c.serial_cus AND trl.serial_cus = '" . $serial_cus . "'
				  LEFT JOIN sales s ON trl.serial_sal = s.serial_sal
				  LEFT JOIN city cit on s.serial_cit = cit.serial_cit
				  LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
				  LEFT JOIN country cou on cit.serial_cou = cou.serial_cou
				  LEFT JOIN invoice i ON s.serial_inv = i.serial_inv
				  $restrict_own_cards
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='2'
				  WHERE c.serial_cus = '" . $serial_cus . "')
			
				  UNION
				  
				  (SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.serial_pro, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal',
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal, DATEDIFF(i.due_date_inv,NOW()) as days_to_dueDate, HOUR(TIMEDIFF(NOW(),s.emission_date_sal)) as days_since_emition,
						 s.serial_cnt, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con, s.serial_inv, i.number_inv, i.status_inv, DATE_FORMAT(i.due_date_inv,'%d/%m/%Y') as vencimiento, s.serial_cus, wrc.wr_id as 'serial_cou', cou.name_cou
				  FROM customer c
				  JOIN extras e ON e.serial_cus = c.serial_cus
				  JOIN sales s ON s.serial_sal=e.serial_sal
				  JOIN city cit on s.serial_cit = cit.serial_cit
				  LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
				  JOIN country cou on cit.serial_cou = cou.serial_cou
				  LEFT JOIN invoice i ON s.serial_inv = i.serial_inv
				  $restrict_own_cards
				  $exclude_free_statement
				  LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND trl.serial_cus = '" . $serial_cus . "'
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='2'
				  WHERE c.serial_cus = '" . $serial_cus . "') 

				) as tbl";
//die('<pre>'.$sql.'</pre>');
//        Debug::print_r($sql);die();
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
	
	public static function getCardFileByCardNumber($db, $card_number){
            $sql = "SELECT DISTINCT f.serial_fle, wrc.wr_id as 'serial_cou', cou.name_cou, cit.name_cit, f.serial_trl, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
                           f.serial_sal, f.diagnosis_fle, f.cause_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date_fle,
                           f.currency_fle, f.exchange_rate_fle, f.total_cost_fle, f.authorized_fle,
                           f.observations_fle, f.end_date_fle, f.type_fle, IF(al.serial_alt,'alert',NULL) as serial_alt
                    FROM `file` f
                    JOIN sales s ON s.serial_sal = f.serial_sal
                    JOIN customer cus ON cus.serial_cus = f.serial_cus
                    JOIN city cit ON cit.serial_cit = f.serial_cit
					LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
                    JOIN country cou ON cou.serial_cou = cit.serial_cou
                    LEFT JOIN alerts al ON al.serial_fle = f.serial_fle AND al.status_alt='PENDING'
                    LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
                    WHERE (s.card_number_sal='".$card_number."'
                    OR trl.card_number_trl='".$card_number."')
					ORDER BY f.serial_fle asc";
                    //die($sql);
				$result = $db->getAll($sql);
			if ($result)
				return $result;
			else
				return false;
	}
	
	public static function getCardInfoByNumber($db, $cardNumber) {
		$sql2 = "SELECT cit.serial_cit FROM sales s JOIN city cit on s.serial_cit = cit.serial_cit WHERE s.card_number_sal={$cardNumber}";
		$result2 = $db->getAll($sql2);
		if($result2)
			$city_statement = " JOIN city cit on s.serial_cit = cit.serial_cit";
		else
			$city_statement = " JOIN city cit on tl.serial_cit = cit.serial_cit";
		
		$sql = "SELECT s.serial_sal, s.serial_cus, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
					pbl.serial_pro, pbl.name_pbl, s.card_number_sal, s.serial_cnt, tl.serial_trl, tl.card_number_trl, 
					DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal, s.days_sal, 
					IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal) as available_days,
					HOUR(TIMEDIFF(NOW(),s.emission_date_sal)) as days_since_emition,
					DATEDIFF(i.due_date_inv,NOW()) as days_to_dueDate,
					s.status_sal, s.serial_con, s.free_sal, s.serial_inv, i.number_inv, i.status_inv,DATE_FORMAT(i.due_date_inv,'%d/%m/%Y') as vencimiento, wrc.wr_id as 'serial_cou', cou.name_cou, ifnull(DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y'),DATE_FORMAT(tl.in_date_trl,'%d/%m/%Y')) as 'emision'
				FROM sales s
				LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal AND tl.status_trl <> 'DELETED'
				$city_statement
				LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
				JOIN country cou on cit.serial_cou = cou.serial_cou
				LEFT JOIN invoice i ON s.serial_inv = i.serial_inv
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro and pbl.serial_lang = 2
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				WHERE (s.card_number_sal={$cardNumber}
					OR tl.card_number_trl={$cardNumber})
			ORDER BY s.begin_date_sal";
//		dEBUG::print_r($sql); die();
		$result = $db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}
	
	/*
	 * @Name: getCardBeneficiaries
	 * @Description: Returns all the beneficiaries covered by a card
	 * @Params: -
	 * @Returns: array
	 */

	public static function getCardBeneficiaryDependents($db, $card_number) {
		
		
		$sql.=" SELECT cus.serial_cus,cus.document_cus, cus.first_name_cus, 
						cus.last_name_cus, cus.birthdate_cus, 
						cus.address_cus, cus.phone1_cus, 
						cus.phone2_cus, cus.cellphone_cus, 
						cus.email_cus, cus.relative_cus, 
						cus.relative_phone_cus, cus.serial_cit, 
						cit.name_cit, cou.name_cou, cit.serial_cit, wrc.wr_id as 'serial_cou'
				FROM sales s 
				JOIN extras e ON s.serial_sal = e.serial_sal
                JOIN customer cus ON cus.serial_cus = e.serial_cus
				JOIN city cit ON cus.serial_cit = cit.serial_cit
				JOIN country cou ON cit.serial_cou= cou.serial_cou 
				LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
                WHERE s.card_number_sal = '$card_number'

				";
//		die(Debug::print_r($sql));
		$result = $db->getAssoc($sql);
		
		if ($result)
			return $result;
		else
			return false;
	}

	//GETTERS
	function getSerial_sal() {
		return $this->serial_sal;
	}

	function getSerial_con() {
		return $this->serial_con;
	}

	function getSerial_cus() {
		return $this->serial_cus;
	}

	function getSal_serial_sal() {
		return $this->sal_serial_sal;
	}

	function getSerial_pbd() {
		return $this->serial_pbd;
	}

	function getSerial_cur() {
		return $this->serial_cur;
	}

	function getSerial_cit() {
		return $this->serial_cit;
	}

	function getSerial_cnt() {
		return $this->serial_cnt;
	}

	function getSerial_inv() {
		return $this->serial_inv;
	}

	function getCardNumber_sal() {
		return $this->card_number_sal;
	}

	function getEmissionDate_sal() {
		return $this->emission_date_sal;
	}

	function getBeginDate_sal() {
		return $this->begin_date_sal;
	}

	function getEndDate_sal() {
		return $this->end_date_sal;
	}

	function getInDate_sal() {
		return $this->in_date_sal;
	}

	function getDays_sal() {
		return $this->days_sal;
	}

	function getFee_sal() {
		return $this->fee_sal;
	}

	function getObservations_sal() {
		return $this->observations_sal;
	}

	function getCost_sal() {
		return $this->cost_sal;
	}

	function getFree_sal() {
		return $this->free_sal;
	}

	function getIdErp_sal() {
		return $this->id_erp_sal;
	}

	function getStatus_sal() {
		return $this->status_sal;
	}

	function getType_sal() {
		return $this->type_sal;
	}

	function getTotal_sal() {
		return $this->total_sal;
	}

	function getStockType_sal() {
		return $this->stock_type_sal;
	}

	function getDirectSale_sal() {
		return $this->direct_sale_sal;
	}

	function getTotalCost_sal() {
		return $this->total_cost_sal;
	}

	function getChange_fee_sal() {
		return $this->change_fee_sal;
	}

	function getAux() {
		return $this->aux;
	}

	function getInternationalFeeStatus_sal() {
		return $this->international_fee_status;
	}

	function getInternationalFeeUsed_sal() {
		return $this->international_fee_used;
	}

	function getInternational_fee_amount() {
		return $this->international_fee_amount;
	}

	function getDeliveredKit() {
		return $this->deliveredKit;
	}

	//SETTERS
	function setSerialSale($serial_sal) {
		$this->serial_sal = $serial_sal;
	}

	function setSerialCon($serial_con) {
		$this->serial_con = $serial_con;
	}

	function setSerialCus($serial_cus) {
		$this->serial_cus = $serial_cus;
	}

	function setSalSerialSale($sal_serial_sal) {
		$this->sal_serial_sal = $sal_serial_sal;
	}

	function setSerialPbd($serial_pbd) {
		$this->serial_pbd = $serial_pbd;
	}

	function setSerialCur($serial_cur) {
		$this->serial_cur = $serial_cur;
	}

	function setSerialCit($serial_cit) {
		$this->serial_cit = $serial_cit;
	}

	function setSerialCnt($serial_cnt) {
		$this->serial_cnt = $serial_cnt;
	}

	function setSerialInv($serial_inv) {
		$this->serial_inv = $serial_inv;
	}

	function setCardNumberSale($card_number_sal) {
		$this->card_number_sal = $card_number_sal;
	}

	function setEmissionDateSale($emission_date_sal) {
		$this->emission_date_sal = $emission_date_sal;
	}

	function setBeginDateSale($begin_date_sal) {
		$this->begin_date_sal = $begin_date_sal;
	}

	function setEndDateSale($end_date_sal) {
		$this->end_date_sal = $end_date_sal;
	}

	function setInDateSale($in_date_sal) {
		$this->in_date_sal = $in_date_sal;
	}

	function setDaysSale($days_sal) {
		$this->days_sal = $days_sal;
	}

	function setFeeSale($fee_sal) {
		$this->fee_sal = $fee_sal;
	}

	function setObservationsSale($observations_sal) {
		$this->observations_sal = $observations_sal;
	}

	function setCostSale($cost_sal) {
		$this->cost_sal = $cost_sal;
	}

	function setFreeSale($free_sal) {
		$this->free_sal = $free_sal;
	}

	function setIdErpSalSale($id_erp_sal) {
		$this->id_erp_sal = $id_erp_sal;
	}

	function setStatusSale($status_sal) {
		$this->status_sal = $status_sal;
	}

	function setTypeSale($type_sal) {
		$this->type_sal = $type_sal;
	}

	function setTotalSale($total_sal) {
		$this->total_sal = $total_sal;
	}

	function setStockTypeSale($stock_type_sal) {
		$this->stock_type_sal = $stock_type_sal;
	}

	function setDirectSaleSale($direct_sale_sal) {
		$this->direct_sale_sal = $direct_sale_sal;
	}

	function setTotalCostSale($total_cost_sal) {
		$this->total_cost_sal = $total_cost_sal;
	}

	function setChangeFeeSale($change_fee_sal) {
		$this->change_fee_sal = $change_fee_sal;
	}

	function setInternationalFeeStatuSale($international_fee_status) {
		$this->international_fee_status = $international_fee_status;
	}

	function setInternationalFeeUsedSale($international_fee_used) {
		$this->international_fee_used = $international_fee_used;
	}

	function setInternationalFeeAmountSale($international_fee_amount) {
		$this->international_fee_amount = $international_fee_amount;
	}

	function setDeliveredKit($deliveredKit) {
		$this->deliveredKit = $deliveredKit;
	}

}
