<?php
/*
File: ServiceProviderByFile.class
Author: David Bergmann
Creation Date: 15/04/2010
Last Modified: 21/05/2010
Modified By: David Bergmann
*/

class ServiceProviderByFile {
    var $db;
    var $serial_spf;
    var $serial_bxp;
    var $serial_fle;
    var $serial_spv;
    var $serial_sbc;
    var $status_spf;
    var $estimated_amount_spf;
    var $real_amount_spf;
    var $medical_type_spf;

    function __construct($db, $serial_spf = NULL, $serial_bxp = NULL, $serial_fle = NULL, $serial_spv = NULL,
                         $serial_sbc = NULL, $status_spf = NULL, $estimated_amount_spf = NULL,
                         $real_amount_spf = NULL, $medical_type_spf = NULL){
        $this -> db = $db;
        $this -> serial_spf = $serial_spf;
        $this -> serial_bxp = $serial_bxp;
        $this -> serial_fle = $serial_fle;
        $this -> serial_spv = $serial_spv;
        $this -> serial_sbc = $serial_sbc;
        $this -> status_spf = $status_spf;
        $this -> estimated_amount_spf = $estimated_amount_spf;
        $this -> real_amount_spf = $real_amount_spf;
        $this -> medical_type_spf = $medical_type_spf;
    }

    /***********************************************
    * function getData
    * gets data by serial_zon
    ***********************************************/
    function getData(){
        if($this->serial_spf!=NULL){
            $sql = 	"SELECT serial_spf, serial_bxp, serial_fle, serial_spv, serial_sbc,
                         status_spf, estimated_amount_spf, real_amount_spf, medical_type_spf
                     FROM service_provider_by_file
                     WHERE serial_spf='".$this->serial_spf."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_spf=$result->fields[0];
                $this->serial_bxp=$result->fields[1];
                $this->serial_fle=$result->fields[2];
                $this->serial_spv=$result->fields[3];
                $this->serial_sbc=$result->fields[4];
                $this->status_spf=$result->fields[5];
                $this -> estimated_amount_spf = $result->fields[6];
                $this -> real_amount_spf = $result->fields[7];
                $this -> medical_type_spf = $result->fields[8];

                return true;
            }
            else
                return false;
        }else
            return false;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert(){
        $sql=   "INSERT INTO service_provider_by_file (
                                serial_spf,
                                serial_bxp,
                                serial_fle,
                                serial_spv,
                                serial_sbc,
                                status_spf,
                                estimated_amount_spf,
                                real_amount_spf,
                                medical_type_spf
                                )
                  VALUES(NULL,";
                  if($this->serial_bxp!=NULL) {
                        $sql.= "'".$this->serial_bxp."',";
                  } else {
                        $sql.= "NULL,";
                  }
                        $sql.= "'".$this->serial_fle."',
                                '".$this->serial_spv."',";
                  
                  if($this->serial_sbc!=NULL) {
                        $sql.= "'".$this->serial_sbc."',";
                  } else {
                        $sql.= "NULL,";
                  }
                        $sql.= "'INPROGRESS',
                                '".$this->estimated_amount_spf."',
                                '".$this->real_amount_spf."',
                                '".$this->medical_type_spf."'
                                );";
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update(){
        $sql=   "UPDATE service_provider_by_file
                 SET status_spf='".$this->status_spf."',
                     estimated_amount_spf='".$this->estimated_amount_spf."',
                     real_amount_spf='".$this->real_amount_spf."',
                     medical_type_spf='".$this->medical_type_spf."'
                 WHERE serial_spf='".$this->serial_spf."'";
        //die($sql);
        $result = $this->db->Execute($sql);

        if ($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
    * function getBenefitsByServiceProviderByFile
    * gets benefits by service Provider
    ***********************************************/
    function getBenefitsByServiceProviderByFile($serial_fle,$serial_spv){
        $sql= "SELECT bbl.description_bbl, spf.serial_spf, spf.serial_bxp, spf.serial_fle, spf.serial_spv, spf.status_spf,
                      spf.estimated_amount_spf, spf.real_amount_spf, spf.medical_type_spf, bxp.price_bxp, bxp.restriction_price_bxp
               FROM service_provider_by_file spf
               JOIN benefits_product bxp ON bxp.serial_bxp = spf.serial_bxp
               JOIN benefits_by_language bbl ON bbl.serial_ben = bxp.serial_ben AND serial_lang =".$_SESSION['serial_lang']."
               WHERE serial_fle='".$serial_fle."'
               AND serial_spv='".$serial_spv."'";
        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    /***********************************************
    * function getServicesByServiceProviderByFile
    * gets services by service Provider
    ***********************************************/
    function getServicesByServiceProviderByFile($serial_fle,$serial_spv){
        $sql= "SELECT sbl.name_sbl, spf.serial_spf, spf.serial_sbc, spf.serial_fle, spf.serial_spv, spf.status_spf,
                      spf.estimated_amount_spf, spf.real_amount_spf, spf.medical_type_spf, sbc.coverage_sbc
               FROM service_provider_by_file spf
               JOIN services_by_country sbc ON sbc.serial_sbc = spf.serial_sbc
               JOIN services_by_language sbl ON sbl.serial_ser = sbc.serial_ser and sbl.serial_lang = ".$_SESSION['serial_lang']."
               WHERE serial_fle='".$serial_fle."'
               AND serial_spv='".$serial_spv."'";
        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    /***********************************************
    * function getServiceProvidersByFile
    * gets serviceProviders by serial_fle
    ***********************************************/
    function getServiceProvidersByFile($serial_fle){
        $sql= "SELECT DISTINCT spf.serial_spv, spv.name_spv, spv.type_spv
               FROM service_provider_by_file spf
               JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
               WHERE serial_fle='".$serial_fle."'";
        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

	/***********************************************
    * function getServiceProviderTypeByFile
    * gets serviceProviders Type by serial_fle
    ***********************************************/
    public static function getServiceProviderTypeByFile($db,$serial_fle){
        $sql= "SELECT DISTINCT spv.type_spv
               FROM service_provider_by_file spf
               JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
               WHERE serial_fle='".$serial_fle."'";
        //die($sql);
        $result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return NULL;
		}
    }

    /***********************************************
    * funcion serviceProviderExists
    * verifies if a Service Provider exists or not
    ***********************************************/
    function serviceProviderExists($serial_fle,$serial_spv,$serial_bxp,$serial_sbc){
        $sql = 	"SELECT serial_spf
                 FROM service_provider_by_file
                 WHERE serial_fle = $serial_fle
                 AND serial_spv = $serial_spv
                 AND (serial_bxp = $serial_bxp
                 OR serial_sbc = $serial_sbc)";
        //die($sql);
        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
            return $result->fields[0];
        }else{
            return false;
        }
    }

    /***********************************************
    * getMedicalTypes
    * gets all Refund status in DB.
    ***********************************************/
    function getMedicalTypes(){
        $sql = "SHOW COLUMNS FROM service_provider_by_file LIKE 'medical_type_spf'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }

    /***********************************************
    * getUsedAmountByBenefit
    * gets the sum of all benefits.
    ***********************************************/
    public static function getUsedAmountByBenefit($db, $serial_fle,$serial_bxp){
        $sql = "SELECT sum(estimated_amount_spf)
                FROM service_provider_by_file
                WHERE serial_bxp = ".$serial_bxp."
                AND serial_fle =".$serial_fle."
                GROUP BY serial_bxp";
        $result = $db -> Execute($sql);

        if($result->fields[0]){
            return $result->fields[0];
        }else{
            return false;
        }
    }

    /***********************************************
    * getUsedAmountByBenefit
    * gets the sum of all benefits.
    ***********************************************/
    public static function getUsedAmountByService($db, $serial_fle,$serial_sbc){
        $sql = "SELECT sum(estimated_amount_spf)
                FROM service_provider_by_file
                WHERE serial_sbc = ".$serial_sbc."
                AND serial_fle =".$serial_fle."
                GROUP BY serial_sbc";
        //die($sql);
        $result = $db -> Execute($sql);

        if($result->fields[0]){
            return $result->fields[0];
        }else{
            return false;
        }
    }

    ///GETTERS
    function getSerial_spf(){
        return $this->serial_spf;
    }
    function getSerial_bxp(){
        return $this->serial_bxp;
    }
    function getSerial_fle(){
        return $this->serial_fle;
    }
    function getSerial_spv(){
        return $this->serial_spv;
    }
    function getSerial_sbc(){
        return $this->serial_sbc;
    }
    function getStatus_spf(){
        return $this->status_spf;
    }
    function getEstimated_amount_spf(){
        return $this->estimated_amount_spf;
    }
    function getReal_amount_spf(){
        return $this->real_amount_spf;
    }
    function getMedical_type_spf(){
        return $this->medical_type_spf;
    }

    ///SETTERS
    function setSerial_spf($serial_spf){
        $this->serial_spf = $serial_spf;
    }
    function setSerial_bxp($serial_bxp){
        $this->serial_bxp = $serial_bxp;
    }
    function setSerial_fle($serial_fle){
        $this->serial_fle = $serial_fle;
    }
    function setSerial_spv($serial_spv){
        $this->serial_spv = $serial_spv;
    }
    function setSerial_sbc($serial_sbc){
        $this->serial_sbc = $serial_sbc;
    }
    function setStatus_spf($status_spf){
        $this->status_spf = $status_spf;
    }
    function setEstimated_amount_spf($estimated_amount_spf){
        $this->estimated_amount_spf = $estimated_amount_spf;
    }
    function setReal_amount_spf($real_amount_spf){
        $this->real_amount_spf = $real_amount_spf;
    }
    function setMedical_type_spf($medical_type_spf){
        $this->medical_type_spf = $medical_type_spf;
    }
}
?>