<?php
/*
File:Potential Customers
Author: Esteban Angulo
Creation Date: 14/07/2010
*/
class PotentialCustomers{
	var $db;
	var $serial_pcs;
	var $serial_usr;
	var $serial_cit;
	var $first_name_pcs;
	var $last_name_pcs;
	var $document_pcs;
	var $birthdate_pcs;
	var $email_pcs;
	var $status_pcs;
	var $phone1_pcs;
	var $cellphone_pcs;
	var $in_date_pcs;
	var $comments_pcs;

	function __construct($db, $serial_pcs=NULL, $serial_usr=NULL, $serial_cit=NULL, $first_name_pcs=NULL, $last_name_pcs=NULL, $document_pcs=NULL, $birthdate_pcs=NULL, $email_pcs=NULL, $phone1_pcs=NULL, $cellphone_pcs=NULL, $in_date_pcs=NULL, $comments_pcs=NULL ){
		$this -> db = $db;
		$this -> serial_pcs = $serial_pcs;
		$this -> serial_usr = $serial_usr;
		$this -> serial_cit = $serial_cit;
		$this -> first_name_pcs = $first_name_pcs;
		$this -> last_name_pcs = $last_name_pcs;
		$this -> document_pcs = $document_pcs;
		$this -> birthdate_pcs = $birthdate_pcs;
		$this -> email_pcs = $email_pcs;
		$this -> status_pcs = $status_pcs;
		$this -> phone1_pcs = $phone1_pcs;
		$this -> cellphone_pcs = $cellphone_pcs;
		$this -> in_date_pcs = $in_date_pcs;
		$this -> comments_pcs = $comments_pcs;
	}
	
	/***********************************************
	* function getData
	@Name: getData
	@Description: Returns all Data from a potential customer
	@Params: N/A
	***********************************************/
	function getData(){
		$sql = 	"SELECT pcs.serial_pcs, pcs.serial_usr, pcs.serial_cit, pcs.first_name_pcs, pcs.last_name_pcs, pcs.document_pcs,
				DATE_FORMAT(pcs.birthdate_pcs,'%d/%m/%Y') as birthdate_pcs, pcs.email_pcs, pcs.status_pcs, pcs.phone1_pcs, pcs.cellphone_pcs, DATE_FORMAT(pcs.in_date_pcs,'%d/%m/%Y') as birthdate_pcs, pcs.comments_pcs
				FROM potential_customers pcs
                JOIN city cit ON pcs.serial_cit = cit.serial_cit
				WHERE pcs.serial_pcs='".$this->serial_pcs."'";
//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false) return false;

		if($result->fields[0]){
			$this -> serial_pcs =$result->fields[0];
			$this -> serial_usr = $result->fields[1];
			$this -> serial_cit = $result->fields[2];
			$this -> first_name_pcs = $result->fields[3];
			$this -> last_name_pcs = $result->fields[4];
			$this -> document_pcs = $result->fields[5];
			$this -> birthdate_pcs = $result->fields[6];
			$this -> email_pcs = $result->fields[7];
			$this -> status_pcs = $result->fields[8];
			$this -> phone1_pcs = $result->fields[9];
			$this -> cellphone_pcs = $result->fields[10];
			$this -> in_date_pcs = $result->fields[11];
			$this -> comments_pcs = $result->fields[12];
			return true;
		}
		return false;
	}

	/***********************************************
	* function insert
	@Name: insert
	@Description: Inserts a new potential customer register in the DB
	@Params: N/A
	@Returns: InsertedID
	***********************************************/
	function insert() {
        $sql = "
            INSERT INTO potential_customers (
				serial_usr,
				serial_cit,
				first_name_pcs,
				last_name_pcs,
				document_pcs,
				birthdate_pcs,
				email_pcs,
				status_pcs,
				phone1_pcs,
				cellphone_pcs,
				in_date_pcs,
				comments_pcs)
            VALUES(";
					$sql.=($this->serial_usr)?"'".$this->serial_usr."',":"NULL,";
					$sql.=($this->serial_cit)?"'".$this->serial_cit."',":"NULL,";
					$sql.=($this->first_name_pcs)?"'".$this->first_name_pcs."',":"NULL,";
					$sql.=($this->last_name_pcs)?"'".$this->last_name_pcs."',":"NULL,";
					$sql.=($this->document_pcs)?"'".$this->document_pcs."',":"NULL,";
					$sql.=($this->birthdate_pcs)?"STR_TO_DATE('".$this->birthdate_pcs."','%d/%m/%Y'),":"NULL,";
					$sql.=($this->email_pcs)?"'".$this->email_pcs."',":"NULL,";
					$sql.=($this->status_pcs)?"'".$this->status_pcs."',":"NULL,";
					$sql.=($this->phone1_pcs)?"'".$this->phone1_pcs."',":"NULL,";
					$sql.=($this->cellphone_pcs)?"'".$this->cellphone_pcs."',":"NULL,";
					$sql.="NOW(),";
					$sql.=($this->comments_pcs)?"'".$this->comments_pcs."'":"NULL";
			$sql.=")";
        $result = $this->db->Execute($sql);
        
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/***********************************************
	* function update
	@Name: insert
	@Description: Updates a register in the DB
	@Params: N/A
	@Returns: if OK -> true
	 *		  else -> false
	***********************************************/
    function update() {
        $sql = "UPDATE potential_customers SET
					serial_usr		=	 '".$this->serial_usr."',
					serial_cit		=	 '".$this->serial_cit."',
					first_name_pcs=		 '".$this->first_name_pcs."',
					last_name_pcs=		 '".$this->last_name_pcs."',
					document_pcs	=	 '".$this->document_pcs."',
					birthdate_pcs=		 STR_TO_DATE('".$this->birthdate_pcs."','%d/%m/%Y'),
					email_pcs	=		 '".$this->email_pcs."',
					status_pcs	=		 '".$this->status_pcs."',
					phone1_pcs=		 '".$this->phone1_pcs."',
					cellphone_pcs=			 '".$this->cellphone_pcs."',
					in_date_pcs=		 '".$this->in_date_pcs."',
					comments_pcs=		 '".$this->comments_pcs."'
                WHERE serial_pcs = '".$this->serial_pcs."'";
		//die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if($result==true)
            return true;
        else
            return false;
    }

	/***********************************************
	* function existsPotentialCustomer
	@Name: insert
	@Description: Verifies if there is another potential customer with the same ID
	@Params: N/A
	@Returns: if exists -> true
	 *		  else -> false
	***********************************************/
	function existsPotentialCustomer($document_pcs,$serial_pcs){
		$sql = 	"SELECT document_pcs
				 FROM potential_customers
				 WHERE document_pcs= _utf8'".utf8_encode($document_pcs)."' collate utf8_bin";

		if($serial_pcs != NULL && $serial_pcs != ''){
			$sql.= " AND serial_pcs <> ".$serial_pcs;
		}
//echo $sql;
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/***********************************************
	* function existsPotentialCustomer
	@Name: insert
	@Description: Verifies if there is another potential customer with the same email
	@Params: N/A
	@Returns: if exists -> true
	 *		  else -> false
	***********************************************/
	function existsPotentialCustomerEmail($email_pcs,$serial_pcs){
		$sql = 	"SELECT email_pcs
				 FROM potential_customers
				 WHERE email_pcs= _utf8'".utf8_encode($email_pcs)."' collate utf8_bin";
		if($serial_pcs != NULL && $serial_pcs != ''){
			$sql.= " AND serial_pcs <> ".$serial_pcs;
		}
//echo $sql;
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@Name: getCustomersAutocompleter
	@Description: Returns an array of customers acording to the pattern entered
	@Params: Customer name or pattern.
	@Returns: Customer array
	**/
	function getPotentialCustomersAutocompleter($name_cus){
		$sql = "SELECT pcs.serial_pcs, pcs.document_pcs, pcs.first_name_pcs, pcs.last_name_pcs
				FROM potential_customers pcs
				WHERE LOWER(pcs.first_name_pcs)LIKE _utf8'%".utf8_encode($name_cus)."%' collate utf8_bin
                                OR LOWER(pcs.last_name_pcs)LIKE _utf8'%".utf8_encode($name_cus)."%' collate utf8_bin
				OR document_pcs LIKE _utf8'%".utf8_encode($name_cus)."%' collate utf8_bin
				LIMIT 10";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/**
	@Name: getAllStatus
	@Description: Returns an array of status types
	@Params: N/A
	@Returns: array
	**/
	function getAllStatus(){
		// Finds all possible status form ENUM column
		$sql = "SHOW COLUMNS FROM customer LIKE 'status_cus'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/***********************************************
	* function getAllPotentialCustomers
	@Name: getAllPotentialCustomers
	@Description: Returns an array of potential customers acording of the selected country o city
	@Params: serial_cou: Country's serial
	 *		 serial_cit: City's serial
	@Returns: potential customers array
	***********************************************/
	function getAllPotentialCustomers($serial_cou,$serial_cit=NULL){
		$sql= "SELECT pcs.serial_pcs, pcs.serial_usr, pcs.serial_cit, pcs.first_name_pcs, pcs.last_name_pcs, pcs.document_pcs,
					DATE_FORMAT(pcs.birthdate_pcs,'%d/%m/%Y') as birthdate_pcs , pcs.email_pcs, pcs.phone1_pcs, pcs.comments_pcs, cit.name_cit
				FROM potential_customers pcs
				JOIN city cit ON pcs.serial_cit = cit.serial_cit
				JOIN country cou ON cit.serial_cou= cou.serial_cou
				WHERE cou.serial_cou='".$serial_cou."'";
		if($serial_cit!='' && $serial_cit!=NULL){
			$sql.=" AND cit.serial_cit='".$serial_cit."'";
		}

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}

	///GETTERS
	function getSerial_pcs(){
		return $this->serial_pcs;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getSerial_cit(){
		return $this->serial_cit;
	}
	function getFirstname_pcs(){
		return $this->first_name_pcs;
	}
	function getLastname_pcs(){
		return $this->last_name_pcs;
	}
	function getDocument_pcs(){
		return $this->document_pcs;
	}
	function getBirthdate_pcs(){
		return $this->birthdate_pcs;
	}
	function getEmail_pcs(){
		return $this->email_pcs;
	}
	function getStatus_pcs(){
		return $this->status_pcs;
	}
	function getPhone1_pcs(){
		return $this->phone1_pcs;
	}
	function getCellphone_pcs(){
		return $this->cellphone_pcs;
	}
	function getIn_date_pcs(){
		return $this->in_date_pcs;
	}
	function getComments_cus(){
		return $this->comments_pcs;
	}


	///SETTERS
	function setSerial_pcs($serial_pcs){
		$this->serial_pcs = $serial_pcs;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setSerial_cit($serial_cit){
		$this->serial_cit = $serial_cit;
	}
	function setFirstname_pcs($first_name_pcs){
		$this->first_name_pcs = $first_name_pcs;
	}
	function setLastname_pcs($last_name_pcs){
		$this->last_name_pcs = $last_name_pcs;
	}
	function setDocument_pcs($document_pcs){
		$this->document_pcs = $document_pcs;
	}
	function setBirthdate_pcs($birthdate_pcs){
		$this->birthdate_pcs = $birthdate_pcs;
	}
	function setEmail_pcs($email_pcs){
		$this->email_pcs = $email_pcs;
	}
	function setStatus_pcs($status_pcs){
		$this->status_pcs = $status_pcs;
	}
	function setPhone1_pcs($phone1_pcs){
		$this->phone1_pcs = $phone1_pcs;
	}
	function setCellphone_pcs($cellphone_pcs){
		$this->cellphone_pcs = $cellphone_pcs;
	}
	function setIn_date_pcs($in_date_pcs){
		$this->in_date_pcs = $in_date_pcs;
	}
	function setComments_pcs($comments_pcs){
		$this->comments_pcs = $comments_pcs;
	}
}
?>
