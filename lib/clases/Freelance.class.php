<?php
/*
File: Freelance.class.php
Author: Esteban Angulo
Creation Date: 30/12/2009 16:37
Last Modified: 05/01/2010 
*/

class Freelance{
	var $db;
	var $serial_fre;
	var $name_fre;
	var $document_fre;
	var $address_fre;
	var $phone1_fre;
	var $phone2_fre;
	var $email_fre;
	var $last_commission_paid_fre;
	var $comission_prctg_free;
	var $url_free;
	var $registration_date_fre;
	var $status_fre;
	
	function __construct($db, $serial_fre = NULL) {
		$this->db = $db;
		$this->serial_fre = $serial_fre;
	}	
	
	/***********************************************
    * funcion getData
    * returns all da form an object
    ***********************************************/
	function getData(){
		if($this->serial_fre!=NULL){
			$sql = 	"SELECT serial_fre, name_fre, document_fre, phone1_fre, phone2_fre, address_fre, 
							email_fre, last_commission_paid_fre, comission_prctg_free, url_free, 
							registration_date_fre, status_fre
					FROM freelance
					WHERE serial_fre='".$this->serial_fre."'";	
			//echo $sql;
			$result = $this->db->getRow($sql);

			if($result){
				$this -> serial_fre = $result['serial_fre'];
				$this -> name_fre = $result['name_fre'];
				$this -> document_fre = $result['document_fre'];
				$this -> address_fre = $result['address_fre'];
				$this -> phone1_fre = $result['phone1_fre'];
				$this -> phone2_fre = $result['phone2_fre'];
				$this -> email_fre = $result['email_fre'];
				$this -> last_commission_paid_fre = $result['last_commission_paid_fre'];
				$this -> comission_prctg_free = $result['comission_prctg_free'];
				$this -> url_free = $result['url_free'];
				$this -> registration_date_fre = $result['registration_date_fre'];
				$this -> status_fre = $result['status_fre'];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * function insert
    * inserts a new register en DB 
    ***********************************************/
    function insert() {
        $sql = "INSERT INTO freelance (
					serial_fre, 
					name_fre, 
					document_fre, 
					phone1_fre, 
					phone2_fre, 
					address_fre, 
					email_fre, 
					last_commission_paid_fre, 
					comission_prctg_free, 
					url_free, 
					registration_date_fre, 
					status_fre
				) VALUES (
					NULL,
					'".$this->name_fre."',
					'".$this->document_fre."',
					'".$this->phone1_fre."',
					'".$this->phone2_fre."',
					'".$this->address_fre."',
					'".$this->email_fre."',
					NOW(),
					'".$this->comission_prctg_free."',
					'".$this->url_free."',
					NOW(),
					'ACTIVE')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "UPDATE freelance SET
					name_fre=		 '".$this->name_fre."',
					document_fre=		 '".$this->document_fre."',
					address_fre=		 '".$this->address_fre."',	
					phone1_fre	=		 '".$this->phone1_fre."',
					phone2_fre	=		 '".$this->phone2_fre."',
					email_fre=			 '".$this->email_fre."',
					comission_prctg_free='".$this->comission_prctg_free."',
					url_free='".$this->url_free."',				
					status_fre='".$this->status_fre."'	
                WHERE serial_fre = '".$this->serial_fre."'";
        
		//echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;
		
        if($result==true)
            return true;
        else
            return false;
    }

	/***********************************************
    * funcion existsFreelance
    * verifies if a freelance exists or not
    ***********************************************/
	function existsFreelance(){
		if($this->document_fre != NULL){
			$sql = 	"SELECT f.serial_fre
					 FROM freelance f 
					 WHERE LOWER(f.document_fre)= _utf8'".utf8_encode($this->document_fre)."' collate utf8_bin";
			
			if($this->serial_fre != NULL && $this->serial_fre != ''){
				$sql.= " AND serial_fre == ".$this->serial_fre;
			}
			$result = $this->db -> Execute($sql);
			
			if($result->fields[0]){
				return $result->fields[0];//user already exists
			}else{
				return false;
			}
		}else{
			return true;//user already exists
		}
	}
	
	/***********************************************
    * funcion existsFreelanceDocument
    * verifies if a freelance exists or not
    ***********************************************/
	function existsFreelanceDocument($document_fre,$serial_fre){
		$sql = 	"SELECT document_fre
				 FROM freelance
				 WHERE LOWER(document_fre)= _utf8'".utf8_encode($document_fre)."' collate utf8_bin";

		if($serial_fre != NULL && $serial_fre != ''){
			$sql.= " AND serial_fre <> ".$serial_fre;
		}
		//die($sql);
		$result = $this->db -> Execute($sql);
		
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
    * funcion existsFreelanceEmail
    * verifies if a freelance exists or not
    ***********************************************/
	function existsFreelanceEmail($email_fre,$serial_fre){
		$sql = 	"SELECT email_fre
				 FROM freelance
				 WHERE LOWER(email_fre)= _utf8'".utf8_encode($email_fre)."' collate utf8_bin";

		if($serial_fre != NULL && $serial_fre != ''){
			$sql.= " AND serial_fre <> ".$serial_fre;
		}
		//die($sql);
		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
		
	/***********************************************
    * funcion getFreelances
    * gets information from Freelance
    ***********************************************/
	function getFreelances(){
		$sql = 	"SELECT serial_fre, name_fre, document_fre, phone1_fre, phone2_fre, address_fre, 
						email_fre, comission_prctg_free, url_free, status_fre,
						DATE_FORMAT(registration_date_fre, '%d/%m/%Y') AS 'registration_date_fre',
						DATE_FORMAT(last_commission_paid_fre, '%d/%m/%Y') AS 'last_commission_paid_fre'
				 FROM freelance ";
		//echo $sql;		 
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/******************************************* 
	@Name: getFreelanceAutocompleter
	@Description: Returns an array of users.
	@Params: User name or pattern.
	@Returns: User array
	********************************************/
	function getFreelanceAutocompleter($name_fre){
		$sql = "SELECT  name_fre,document_fre, serial_fre
				FROM freelance
				WHERE LOWER(name_fre) LIKE _utf8'%".utf8_encode($name_fre)."%' collate utf8_bin
				OR  LOWER(document_fre)   LIKE _utf8'%".utf8_encode($name_fre)."%' collate utf8_bin
				LIMIT 10";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	public static function validateFreelance($db, $md5_id, $remote_url){
		$sql = "SELECT serial_fre
				FROM freelance
				WHERE '$remote_url' LIKE CONCAT( url_free, '%' )
				AND md5(serial_fre) = '$md5_id'";
		
		$result = $db -> getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getFreelanceWithComissions($db){        
		$sql = "SELECT DISTINCT f.serial_fre, f.name_fre, document_fre, last_commission_paid_fre
				FROM applied_comissions apc
				JOIN freelance f ON f.serial_fre = apc.serial_fre AND apc.fre_payment_date_com IS NULL";
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function updateLastComissionPaid($db, $serial_fre) {
        $sql = "UPDATE freelance SET 
					last_commission_paid_fre = NOW() 
				WHERE serial_fre = {$serial_fre}";
        //echo $sql;
        $result = $db->Execute($sql);
        
        if($result==true)
            return true;
        else
            return false;
    }
	
	public static function getComissionsInformation($db, $serial_fre){
		$sql = "SELECT serial_fre, comission_prctg_free AS 'percentage_fre'
				FROM freelance
				WHERE serial_fre = $serial_fre
				AND status_fre = 'ACTIVE'";
		
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getAllStatus($db){
		// Finds all possible status form ENUM column 
		$sql = "SHOW COLUMNS FROM freelance LIKE 'status_fre'";
		$result = $db -> Execute($sql);
		$type = $result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	public function getSerial_fre() {
		return $this->serial_fre;
	}

	public function setSerial_fre($serial_fre) {
		$this->serial_fre = $serial_fre;
	}

	public function getName_fre() {
		return $this->name_fre;
	}

	public function setName_fre($name_fre) {
		$this->name_fre = $name_fre;
	}

	public function getDocument_fre() {
		return $this->document_fre;
	}

	public function setDocument_fre($document_fre) {
		$this->document_fre = $document_fre;
	}

	public function getAddress_fre() {
		return $this->address_fre;
	}

	public function setAddress_fre($address_fre) {
		$this->address_fre = $address_fre;
	}

	public function getPhone1_fre() {
		return $this->phone1_fre;
	}

	public function setPhone1_fre($phone1_fre) {
		$this->phone1_fre = $phone1_fre;
	}

	public function getPhone2_fre() {
		return $this->phone2_fre;
	}

	public function setPhone2_fre($phone2_fre) {
		$this->phone2_fre = $phone2_fre;
	}

	public function getEmail_fre() {
		return $this->email_fre;
	}

	public function setEmail_fre($email_fre) {
		$this->email_fre = $email_fre;
	}

	public function getLast_commission_paid_fre() {
		return $this->last_commission_paid_fre;
	}

	public function setLast_commission_paid_fre($last_commission_paid_fre) {
		$this->last_commission_paid_fre = $last_commission_paid_fre;
	}

	public function getComission_prctg_free() {
		return $this->comission_prctg_free;
	}

	public function setComission_prctg_free($comission_prctg_free) {
		$this->comission_prctg_free = $comission_prctg_free;
	}

	public function getUrl_free() {
		return $this->url_free;
	}

	public function setUrl_free($url_free) {
		$this->url_free = $url_free;
	}

	public function getRegistration_date_fre() {
		return $this->registration_date_fre;
	}

	public function setRegistration_date_fre($registration_date_fre) {
		$this->registration_date_fre = $registration_date_fre;
	}

	public function getStatus_fre() {
		return $this->status_fre;
	}

	public function setStatus_fre($status_fre) {
		$this->status_fre = $status_fre;
	}
}

?>