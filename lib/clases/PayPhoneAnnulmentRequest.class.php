<?php

class PayPhoneAnnulmentRequest{
	//Annulment request model
    private $timeZone;
    private $latitude;
    private $longitude;
    private $transactionId;
	var $validationError;
	
	function __construct() {
		$this->validationError = NULL;
	}
	
	public function setTimeZone($timeZone){
		if (is_numeric($timeZone)){
			$this->timeZone = $timeZone;
		} else {
			$this->validationError['timeZone'] = 'InvalidFormat';
		}
	}
	
	public function setLatitude($latitude){
		if (!isset($latitude) || is_numeric($latitude)){
			$this->latitude = $latitude;
		} else {
			$this->validationError['latitude'] = 'InvalidFormat';
		}
	}
	
	public function setLongitude($longitude){
		if (!isset($longitude) || is_numeric($longitude)){
			$this->longitude = $longitude;
		} else {
			$this->validationError['longitude'] = 'InvalidFormat';
		}
	}
	
	public function setTransactionId($transactionId){
		if (isset($transactionId)){
			$this->transactionId = $transactionId;
		}
	}
	
	public function getAnnulmentRequestModel(){
		$annulmentReqModel['msg'] = 'error';
		if (!isset($this->validationError)){
			$annulmentReqModel['msg'] = 'success';
			$annulmentReqModel['result'] = array(
				"timeZone" => $this->timeZone,
				"latitude" => $this->latitude,
				"longitude" => $this->longitude,
				"transactionId" => $this->transactionId
			);
		} else {
			$annulmentReqModel['result'] = $this->validationError;
		}
		
		return $annulmentReqModel;
	}
	
}
