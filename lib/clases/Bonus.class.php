<?php
/*
File: Bonus.class.php
Author: Santiago Benitez
Creation Date: 17/03/210 10:26
Last Modified: 17/03/210 10:26
Modified By: Santiago Benitez
*/
class Bonus {
	var $db;
	var $serial_bon;
    var $serial_dea;
    var $serial_pxc;
	var $percentage_bon;
    var $begin_date_bon;
    var $status_bon;

	function __construct($db, $serial_bon = NULL, $serial_dea = NULL, $serial_pxc = NULL,
                             $percentage_bon = NULL, $begin_date_bon=NULL, $status_bon=NULL){
		$this -> db = $db;
		$this -> serial_bon = $serial_bon;
                $this -> serial_dea = $serial_dea;
                $this -> serial_pxc = $serial_pxc;
		$this -> percentage_bon = $percentage_bon;
                $this -> begin_date_bon = $begin_date_bon;
                $this -> status_bon = $status_bon;
	}

        /***********************************************
        * function getData
        * gets data by serial_bon
        ***********************************************/
	function getData(){
            if($this->serial_bon!=NULL){
                    $sql =  "SELECT b.serial_bon, b.serial_dea, b.serial_pxc, b.percentage_bon,
                                    b.begin_date_bon, b.status_bon
                             FROM bonus b
                             JOIN dealer dea ON dea.serial_dea = b. serial_dea
                             JOIN product_by_country pbc ON pbc.serial_pxc = b.serial_pxc
                             JOIN product p ON p.serial_pro = pbc.serial_pro
                             WHERE serial_bon='".$this->serial_bon."'";
                    //echo $sql." ";
                    $result = $this->db->Execute($sql);
                    if ($result === false) return false;

                    if($result->fields[0]){
                        $this->serial_bon=$result->fields[0];
                        $this->serial_dea=$result->fields[1];
                        $this->serial_pxc=$result->fields[2];
                        $this->percentage_bon=$result->fields[3];
                        $this->begin_date_bon=$result->fields[4];
                        $this->status_bon=$result->fields[5];
                        return true;
                    }
                    else
                        return false;
            }else
                return false;
	}

	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
		$sql="INSERT INTO bonus (
					serial_bon,
                                        serial_dea,
                                        serial_pxc,
                                        percentage_bon,
                                        begin_date_bon,
                                        status_bon
					)
			  VALUES(NULL,
					'".$this->serial_dea."',
                                        '".$this->serial_pxc."',
                                        '".$this->percentage_bon."',
                                        NOW(),
                                        'ACTIVE'
					);";

		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

 	/***********************************************
        * funcion deactivate
        * updates a register in DB
        ***********************************************/
	function deactivate(){
		$sql="UPDATE bonus
		SET status_bon='INACTIVE'
		WHERE serial_bon='".$this->serial_bon."'";

		$result = $this->db->Execute($sql);
		//echo $sql;
		if ($result==true)
			return true;
		else
			return false;
	}

	/***********************************************
        * funcion getBonus
        * gets all the Bonus of the system
        ***********************************************/
	function getBonus(){
            $sql =  "SELECT serial_bon, serial_dea, serial_pxc, percentage_bon,
                            begin_date_bon, status_bon
                     FROM bonus
                     ORDER BY percentage_bon";

            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}


	/***********************************************
        * funcion bonusExists
        * verifies if a Bonus exists or not
        ***********************************************/
	function bonusExists(){
            $sql =  "SELECT serial_bon
                     FROM bonus
                     WHERE serial_dea = ".$this->serial_dea."
                     AND serial_pxc = ".$this->serial_pxc."
                     AND status_bon = 'ACTIVE'";
            //die($sql);
            $result = $this->db->Execute($sql);

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return false;
            }
	}

        /***********************************************
        * funcion getDealersByBonus
        * checks in db if there already exists a dealerByBonus
        * with the same data
        ***********************************************/
	function getBonusTable($serial_cou,$serial_cit=NULL,$serial_dea=NULL) {
            $sql =  "SELECT b.serial_bon, cou.name_cou, c.name_cit, d.name_dea, b.serial_dea,
                            pbl.name_pbl, b.serial_pxc, b.percentage_bon
                     FROM bonus b
                     JOIN dealer d ON b.serial_dea = d.serial_dea
                     JOIN sector s ON s.serial_sec = d.serial_sec
                     JOIN city c ON c.serial_cit = s.serial_cit
                     JOIN country cou ON cou.serial_cou = c.serial_cou
                     JOIN product_by_country pbc ON b.serial_pxc = pbc.serial_pxc
                     JOIN product_by_language pbl ON pbc.serial_pro = pbl.serial_pro
                     JOIN language lang ON lang.serial_lang = pbl.serial_lang  AND lang.code_lang = '".$_SESSION['language']."'
                     WHERE cou.serial_cou = ".$serial_cou."
                     AND status_bon='ACTIVE'";
            if($serial_cit!=NULL){
                $sql .=" AND c.serial_cit = ".$serial_cit;
            }
            if($serial_dea!=NULL){
                $sql .=" AND b.serial_dea = ".$serial_dea;
            }
            $sql .=" ORDER BY d.name_dea, c.name_cit";
            //die($sql);
            $result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                        $arreglo[$cont]=$result->GetRowAssoc(false);
                        $result->MoveNext();
                        $cont++;
                    }while($cont<$num);
		}
		return $arreglo;
	}

	public static function getBonusByPxcDea($db, $serial_pxc, $serial_dea, $sale_date){
		$sql =  "SELECT MAX(serial_bon)
				FROM bonus
				WHERE STR_TO_DATE('$sale_date', '%d/%m/%Y') >= begin_date_bon AND status_bon = 'ACTIVE'
				AND serial_pxc='$serial_pxc'
				AND serial_dea='$serial_dea'";
        //echo $sql; die();
		$result = $db -> getOne($sql);
                
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_bon(){
		return $this->serial_bon;
	}
	function getSerial_dea(){
		return $this->serial_dea;
	}
        function getSerial_pxc(){
		return $this->serial_pxc;
	}
	function getPercentage_bon(){
		return $this->percentage_bon;
	}
        function getBegin_date_bon(){
		return $this->begin_date_bon;
	}
        function getStatus_bon(){
		return $this->status_bon;
	}

	///SETTERS
        function setSerial_bon($serial_bon){
		$this->serial_bon = $serial_bon;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea = $serial_dea;
	}
        function setSerial_pxc($serial_pxc){
		$this->serial_pxc = $serial_pxc;
	}
	function setPercentage_bon($percentage_bon){
		$this->percentage_bon = $percentage_bon;
	}
        function setBegin_date_bon($begin_date_bon){
		$this->begin_date_bon = $begin_date_bon;
	}
	function setStatus_bon($status_bon){
		$this->status_bon = $status_bon;
	}
}
?>