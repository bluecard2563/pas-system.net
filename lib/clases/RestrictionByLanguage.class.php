<?php
/*
File: RestrictionByLanguage
Author: Patricio Astudillo
Creation Date: 21/01/2010 11:13
LastModified: 21/01/2010
Modified By: Patricio Astudillo
*/

class RestrictionByLanguage{
	var $db;
	var $serial_rbl;
        var $serial_lang;
        var $serial_rst;
        var $description_rbl;

	function __construct($db, $serial_rbl=NULL, $serial_lang=NULL, $serial_rst=NULL, $description_rbl=NULL){
		$this -> db = $db;
		$this -> serial_rbl = $serial_rbl;
                $this -> serial_lang = $serial_lang;
                $this -> serial_rst = $serial_rst;
                $this -> description_rbl = $description_rbl;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a RestrictionByLanguage object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_rbl!=NULL){
			$sql = 	"SELECT serial_rbl, serial_lang, serial_rst, description_rbl
				 FROM restriction_by_language
				 WHERE serial_rbl='".$this->serial_rbl."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_rbl =$result->fields[0];
                                $this -> serial_lang =$result->fields[1];
                                $this -> serial_rst =$result->fields[2];
				$this -> description_rst = $result->fields[3];

				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/**
	@Name: rblExists
	@Description: Verifies if a restriction exists in a specific language.
	@Params: $description_rbl: The search pattern.
                 $serial_lang: language ID
                 $serial_rbl: object ID
	@Returns: TRUE if exists / FALSE if NOT
	**/
	function rblExists($description_rbl, $serial_lang, $serial_rbl=NULL){
		$sql = 	"SELECT serial_rbl
                         FROM restriction_by_language
			 WHERE LOWER(description_rbl)= _utf8'".utf8_encode($description_rbl)."' collate utf8_bin
                         AND serial_lang = ".$serial_lang;

                if($serial_rbl!=NULL){
			$sql.=" AND serial_rbl <> ".$serial_rbl;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@Name: insert
	@Description: Inserts a RBL instance in DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error.
	**/
        function insert() {
            $sql = "INSERT INTO restriction_by_language (
                          serial_rbl,
                          serial_lang,
                          serial_rst,
                          description_rbl)
                    VALUES (
                          NULL,
                          '".$this->serial_lang."',
                          '".$this->serial_rst."',
                          '".$this->description_rbl."'
                          )";

            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

 	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error
	**/
        function update() {
            $sql = "UPDATE restriction_by_language SET
                        description_rbl ='".$this->description_rbl."'
                    WHERE serial_rbl = '".$this->serial_rbl."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

	///GETTERS
	function getSerial_rbl(){
		return $this->serial_rbl;
	}
        function getSerial_lang(){
		return $this->serial_lang;
	}
        function getSerial_rst(){
		return $this->serial_rst;
	}
	function getDescription_rbl(){
		return $this->description_rbl;
	}


	///SETTERS
	function setSerial_rbl($serial_rbl){
		$this->serial_rbl = $serial_rbl;
	}
        function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
        function setSerial_rst($serial_rst){
		$this->serial_rst = $serial_rst;
	}
	function setDescription_rbl($description_rbl){
		$this->description_rbl = $description_rbl;
	}
}
?>