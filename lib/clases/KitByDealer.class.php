<?php
/*
File: KitByDealer.class.php
Author: Santiago Ben�tez
Creation Date: 22/02/2010 16:12
Creation Date: 22/02/2010 16:12
Modified By: Santiago Ben�tez
*/
class KitByDealer{
	var $db;
	var $serial_kbd;
	var $serial_dea;
	var $total_kbd;

	function __construct($db, $serial_kbd=NULL, $serial_dea=NULL, $total_kbd=NULL){

		$this -> db = $db;
		$this -> serial_kbd=$serial_kbd;
		$this -> serial_dea=$serial_dea;
		$this -> total_kbd=$total_kbd;
	}

	/**
	@First_name: getData
	@Description: Retrieves the data of a KitByDealer object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_kbd!=NULL){
			$sql = 	"SELECT kbd.serial_kbd, kbd.serial_dea, kbd.total_kbd
					FROM kits_by_dealer kbd
					WHERE kbd.serial_kbd='".$this->serial_kbd."'";
                        //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){

				$this -> serial_kbd=$result->fields[0];
				$this -> serial_dea=$result->fields[1];
				$this -> total_kbd=$result->fields[2];

				return true;
			}else
				return false;
		}else
			return false;
	}

        /**
	@First_name: getDataByDealer
	@Description: Retrieves the data of a KitByDealer object.
	@Params: The ID of the dealer
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getDataByDealer(){
		if($this->serial_dea!=NULL){
			$sql = 	"SELECT kbd.serial_kbd, kbd.serial_dea, kbd.total_kbd
					FROM kits_by_dealer kbd
					WHERE kbd.serial_dea='".$this->serial_dea."'";
                        //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){

				$this -> serial_kbd=$result->fields[0];
				$this -> serial_dea=$result->fields[1];
				$this -> total_kbd=$result->fields[2];

				return true;
			}else
				return false;
		}else
			return false;
	}
	
	/**
	@First_name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: serial_kbd if OK. / FALSE on error
	**/
    function insert() {
        $sql = "
            INSERT INTO kits_by_dealer (
				serial_kbd,
				serial_dea,
				total_kbd)
            VALUES (
                NULL,
				'".$this->serial_dea."',
				'".$this->total_kbd."')";
        //die($sql);
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/**
	@First_name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "
                UPDATE kits_by_dealer SET
					serial_dea='".$this->serial_dea."',
					total_kbd='".$this->total_kbd."'
				WHERE serial_kbd = '".$this->serial_kbd."'";
                                //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }
	
	//Getters
	function getSerial_kbd(){
		return $this->serial_kbd;
	}
	function getSerial_dea(){
		return $this->serial_dea;
	}
	function getTotal_kbd(){
		return $this->total_kbd;
	}

	//Setters
	function setSerial_kbd($serial_kbd){
		$this->serial_kbd=$serial_kbd;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea=$serial_dea;
	}
	function setTotal_kbd($total_kbd){
		$this->total_kbd=$total_kbd;
	}
}
?>