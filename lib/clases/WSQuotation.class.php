<?php

/**
 * File: ProductByCountry.class.php
 * Author: Valeria Andino
 * Creation Date: 30/08/2016
 * Last Modified: 
 * Modified By: 
 */

/**
 * Description of WSQuotation
 *
 * @author AdminBC
 */
class WSQuotation {

    
    public static function getQuotationForTravelMotive($db,$data_set) {

        $countryDestination_Id = $data_set['id_Destination'];
        $travelMotive_Id = $data_set['id_TravelMotive'];
        $startDate = $data_set['startDate'];
        $endDate = $data_set['endDate'];
        $totalKids = $data_set['totalKids'];
        $totalAdults = $data_set['totalAdults'];
        $totalSeniorAllowed = $data_set['totalSeniorAllowed'];
        $language = $data_set['language'];
        $manyTravel = $data_set['manyTravel'];
        $countryOrigin_Id = $data_set['id_Origin'];
        $counter_Id = $data_set['id_Counter']; //Global Var
        $max_Days = $data_set['max_days']; //Global Var


        if ($totalSeniorAllowed > 0) {
            $seniorAllowed = 'YES';
        } else {
            $seniorAllowed = 'NO';
        }

        if ($manyTravel == 'TRUE') {
            $totalAdults = $totalAdults + $totalKids;
            $totalKids = 0;
        }
        if ($travelMotive_Id == 3 || $travelMotive_Id == 4) {
            $totalAdults = $totalAdults + $totalKids;
            $totalKids = 0;
        }

        if ($countryDestination_Id == $countryOrigin_Id) {
            $productNational = 'YES';
            $totalAdults = $totalAdults + $totalKids;
            $totalKids = 0;
        } else {
            $productNational = 'NO';
        }

        if ($totalKids > 0) {
            $kids = 'YES';
        } else {
            $kids = 'NO';
        }
        $days = GlobalFunctions::getDaysDiffDdMmYyyy($startDate, $endDate);
        $zone = Zone::getZonesByCountry($db, $countryDestination_Id);
        $product = WSTravelMotivesByProduct::getBestProduct($db, $language, $countryDestination_Id, $travelMotive_Id, $kids, $manyTravel, $seniorAllowed, $days, $productNational, $totalAdults, $zone, $counter_Id);

        global $global_maxAgeAdults;
        global $global_minAgeHolder;

        if ($days > 0 && $days <= $max_Days) {
            if ($product) {

                $productSet = array();
                $temp_Array = array();

                foreach ($product as $key => $p) {
                    $temp_Array['order'] = $key;
                    $temp_Array['idProByCou'] = $p['serial_pxc'];
                    $temp_Array['idPro'] = $p['serial_pro'];
                    $temp_Array['productName'] = utf8_encode($p['name_pbl']);
                    $temp_Array['description'] = utf8_encode($p['description_pbl']);
                    $percentageExtra = $p['percentage_extras_pxc'];
                    $totalkidsbyproduct = $p['children_pro'];
                    $proSenior = $p['senior_pro'];
                    $maxExtras = $p['max_extras_pro'];
                    //$spouse = $p['spouse_pro'];
                    $relative = $p['relative_pro'];
                    //$probydealer = $p['serial_pbd'];
                    $temp_Array['maxBeneficiaryExtrasByCard'] = $maxExtras;
                    $temp_Array['maxfreeKidsByCard'] = $totalkidsbyproduct;

                    if ($proSenior == 'YES') {
                        $totalAdults = $totalSeniorAllowed;
                        $totalKids = 0;
                        $global_maxAgeAdults = 100;
                        $global_minAgeHolder = 72; // Var use by minium age for holder         
                    }
                    //if ($totalKids > 0){
                    //   $extrasRestricted = 'CHILDREN';
                    //}    

                    $cardsWR = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $totalAdults, $totalKids, 0, $extrasRestricted, 0, $totalkidsbyproduct, $print = true);
                    $cards = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $totalAdults, $totalKids, 0, $extrasRestricted, 0, $totalkidsbyproduct, $print = false);
                    $totalCards = count($cards);
                    $totalSpouse = 0;
                    $totalChildren = 0;
                    $totalFree = 0;
                    $totalAdultsB = 0;
                    foreach ($cards as $key => $value) {
                        foreach ($value as $item) {
                            switch ($item) {

                                case 'S': $totalSpouse++;
                                    break;
                                case 'C': $totalChildren++;
                                    break;
                                case 'F': $totalFree++;
                                    break;
                                case 'A': $totalAdultsB++;
                                    break;
                            }
                        }
                    }

                    $temp_Array['totalCardsByProduct'] = $totalCards;
                    $temp_Array['totalFreeByProduct'] = $totalFree;
                    $temp_Array['totalExtrasbByProduct'] = $totalAdultsB + $totalChildren;
                    ;
                    $priceA = WSTravelMotivesByProduct::getPriceByProduct($db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, 1, 0, false, 0, $extrasRestricted, $language, $days, $wService = TRUE);
                    $temp_Array['priceHolderByCard'] = $priceA;
                    if ($percentageExtra > 0) {
                        $temp_Array['priceByExtrasB'] = $priceA - (($priceA) * ($percentageExtra / 100));
                    } else {
                        $temp_Array['priceByExtrasB'] = 0;
                    }
                    $totalprice = WSTravelMotivesByProduct::getPriceByProduct($db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, $totalAdults, $totalKids, false, 0, $extrasRestricted, $language, $days, $wService = TRUE);
                    $temp_Array['totalPriceByProduct'] = $totalprice;
                    $temp_Array['extraDataArrayByCard'] = $cardsWR;

                    array_push($productSet, $temp_Array);
                }
            }
        }

        
        return $productSet;
    }

    public static function getQuotationForProduct ($db,$data_set) {

        
        $countryDestination_Id = $data_set['id_Destination'];
        $startDate = $data_set['startDate'];
        $endDate = $data_set['endDate'];
        $totalKids = $data_set['totalKids'];
        $totalAdults = $data_set['totalAdults'];
        $totalSeniorAllowed = $data_set['totalSeniorAllowed'];
        $language = $data_set['language'];
        $countryOrigin_Id = $data_set['id_Origin'];
        $counter_Id = $data_set['id_Counter']; //Global Var
        $max_Days = $data_set['max_days']; //Global Var
        $productByCountry_Id = $data_set['id_Product'];


        $days = GlobalFunctions::getDaysDiffDdMmYyyy($startDate, $endDate);
        $product = Product::getProduct($db, $productByCountry_Id, $countryOrigin_Id, $countryDestination_Id, $language, $counter_Id, $totalKids, $totalAdults, $many_travel, $national, $totalSeniorAllowed, $days);
        global $global_maxAgeAdults;
        global $global_minAgeHolder;

        if ($days <= $max_Days) {
            if ($product) {


                $array = array();
                $temp_Array = array();

                foreach ($product as $key => $p) {
                    $temp_Array['order'] = $key;
                    $temp_Array['idProByCou'] = $p['serial_pxc'];
                    $temp_Array['idPro'] = $p['serial_pro'];
                    $temp_Array['productName'] = utf8_encode($p['name_pbl']);
                    $temp_Array['description'] = utf8_encode($p['description_pbl']);
                    $percentageExtra = $p['percentage_extras_pxc'];
                    $totalkidsbyproduct = $p['children_pro'];
                    $proSenior = $p['senior_pro'];
                    $maxExtras = $p['max_extras_pro'];
                    //$spouse = $p['spouse_pro'];
                    //$relative = $p['relative_pro'];                   
                    //$probydealer = $p['serial_pbd'];
                    $temp_Array['maxBeneficiaryExtrasByCard'] = $maxExtras;
                    $temp_Array['maxfreeKidsByCard'] = $totalkidsbyproduct;

                    if ($proSenior == 'YES') {
                        $totalAdults = $totalSeniorAllowed;
                        $totalKids = 0;
                        $global_maxAgeAdults = 100;
                        $global_minAgeHolder = 72;
                    }
                    //if ($totalKids > 0){
                    //   $extrasRestricted = 'CHILDREN';
                    //}    
                    $cardsWR = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $totalAdults, $totalKids, 0, $extrasRestricted, 0, $totalkidsbyproduct, $print = true);
                    $cards = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $totalAdults, $totalKids, 0, $extrasRestricted, 0, $totalkidsbyproduct, $print = false);
                    $totalCards = count($cards);
                    $totalSpouse = 0;
                    $totalChildren = 0;
                    $totalFree = 0;
                    $totalAdultsB = 0;
                    foreach ($cards as $key => $value) {
                        foreach ($value as $item) {
                            switch ($item) {

                                case 'S, 0-72': $totalSpouse++;
                                    break;
                                case 'C, 0-11': $totalChildren++;
                                    break;
                                case 'F, 0-11': $totalFree++;
                                    break;
                                case 'A, 0-72': $totalAdultsB++;
                                    break;
                            }
                        }
                    }

                    $temp_Array['totalCardsByProduct'] = $totalCards;
                    $temp_Array['totalFreeByProduct'] = $totalFree;
                    $temp_Array['totalExtrasbByProduct'] = $totalAdultsB + $totalChildren;
                    $priceA = WSTravelMotivesByProduct::getPriceByProduct($db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, 1, 0, false, 0, $extrasRestricted, $language, $days);
                    $temp_Array['priceHolderByCard'] = $priceA;
                    if ($percentageExtra > 0) {
                        $temp_Array['priceByExtrasB'] = $priceA - (($priceA) * ($percentageExtra / 100));
                    } else {
                        $temp_Array['priceByExtrasB'] = 0;
                    }
                    $totalprice = WSTravelMotivesByProduct::getPriceByProduct($db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, $totalAdults, $totalKids, false, 0, $extrasRestricted, $language, $days);
                    $temp_Array['totalPriceByProduct'] = $totalprice;
                    $temp_Array['extraDataArrayByCard'] = $cardsWR;
                    array_push($array, $temp_Array);                   
                }
                
            }
           
        }
        return $array;
    }

    /*     * VA
     * @name miniumDataSet
     * @param array $data
     * @return boolean 
     */
    public static function minimunDataRequiredWSQuotation($data_array) {
        $case_code = 200;

        if (!is_array($data_array)) {
            $case_code = 650;
        } elseif (!isset($data_array['id_Destination'])) {
            $case_code = 651;
        } elseif (!isset($data_array['id_TravelMotive'])) {
            $case_code = 652;
        } elseif (!isset($data_array['startDate'])) {
            $case_code = 653;
        } elseif (!isset($data_array['endDate'])) {
            $case_code = 654;
        } elseif (!isset($data_array['totalKids'])) {
            $case_code = 655;
        } elseif (!isset($data_array['totalAdults'])) {
            $case_code = 656;
        } elseif (!isset($data_array['totalSeniorAllowed'])) {
            $case_code = 657;
        } elseif (!isset($data_array['language'])) {
            $case_code = 658;
        } elseif (!isset($data_array['manyTravel'])) {
            $case_code = 659;
        } elseif (!isset($data_array['id_Origin'])) {
            $case_code = 660;
        } elseif (!isset($data_array['id_Counter'])) {
            $case_code = 659;
        }

        return $case_code;
    }

    public static function minimunDataRequiredWSQuotationByProduct($data_array) {
        $case_code = 200;

        if (!is_array($data_array)) {
            $case_code = 650;
        } elseif (!isset($data_array['id_Destination'])) {
            $case_code = 651;
        } elseif (!isset($data_array['id_Product'])) {
            $case_code = 652;
        } elseif (!isset($data_array['startDate'])) {
            $case_code = 653;
        } elseif (!isset($data_array['endDate'])) {
            $case_code = 654;
        } elseif (!isset($data_array['totalKids'])) {
            $case_code = 655;
        } elseif (!isset($data_array['totalAdults'])) {
            $case_code = 656;
        } elseif (!isset($data_array['totalSeniorAllowed'])) {
            $case_code = 657;
        } elseif (!isset($data_array['language'])) {
            $case_code = 658;
        } elseif (!isset($data_array['manyTravel'])) {
            $case_code = 659;
        } elseif (!isset($data_array['id_Origin'])) {
            $case_code = 660;
        } elseif (!isset($data_array['id_Counter'])) {
            $case_code = 659;
        }

        return $case_code;
    }

    /*     * VA
     * @name validateDataSet
     * @param array $data
     * @return boolean 
     */

    public static function validateDataWSQuotationByProduct($data_array) {

        $code_validation = 200;

      if (!is_numeric($data_array['totalKids'])) {
            $code_validation = 662;
        } elseif (!is_numeric($data_array['totalAdults'])) {
            $code_validation = 663;
        } elseif (!is_numeric($data_array['totalSeniorAllowed'])) {
            $code_validation = 664;
        } elseif (!strstr($data_array['startDate'], '/')) {
            $code_validation = 665;
        } elseif (!strstr($data_array['endDate'], '/')) {
            $code_validation = 666;
        }
        return $code_validation;
    }

    public static function validateDataWSQuotation($data_array) {

        $code_validation = 200;

        if (!is_numeric($data_array['totalKids'])) {
            $code_validation = 662;
        } elseif (!is_numeric($data_array['totalAdults'])) {
            $code_validation = 663;
        } elseif (!is_numeric($data_array['totalSeniorAllowed'])) {
            $code_validation = 664;
        } elseif (!strstr($data_array['startDate'], '/')) {
            $code_validation = 665;
        } elseif (!strstr($data_array['endDate'], '/')) {
            $code_validation = 666;
        }
        

        return $code_validation;
    }

}
