<?php

class Agreement
{
    var $db;
    var $serial_agr;
    var $name_agr;
    var $discount_value_agr;
    var $status_agr;
    var $serial_inv;

    function __construct($db, $serial_agr = NULL, $name_agr = NULL, $discount_value_agr = NULL, $status_agr = NULL){
        $this->db = $db;
        $this->serial_agr = $serial_agr;
        $this->name_agr = $name_agr;
        $this->discount_value_agr = $discount_value_agr;
        $this->status_agr = $status_agr;
    }


    function getData(){
        if($this->serial_agr != NULL){
            $sql = "SELECT
                    serial_agr,
                    name_agr,
                    discount_value_agr,
                    status_agr
                FROM
                    agreement
                WHERE serial_agr='".$this->serial_agr."'";
            $result = $this->db->Execute($sql);
            if ($result === false) {
                return false;
            }
            if ($result->fields[0]) {
                $this->serial_agr = $result->fields[0];
                $this->name_agr = $result->fields[1];
                $this->discount_value_agr = $result->fields[2];
                $this->status_agr = $result->fields[3];
                return true;
            }
            else {
                return false;
            }
        }
    }

    function getAllAgreementsStatus(){
        // Finds all possible status form ENUM column
        $sql = "SHOW COLUMNS FROM agreement LIKE 'status_agr'";
        $result = $this->db -> Execute($sql);
        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }

    function getAllAgreements(){
        $sql = 	"SELECT serial_agr, name_agr, discount_value_agr, status_agr
				 FROM agreement
				 ORDER BY name_agr";
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if($num > 0){
            $arreglo = array();
            $cont = 0;
            do{
                $arreglo[$cont++] = $result->GetRowAssoc(false);
                $result->MoveNext();
            }while($cont < $num);
        }
        return $arreglo;
    }

    function getAllActiveAgreements(){
        $sql = 	"SELECT serial_agr, name_agr, discount_value_agr, status_agr
				 FROM agreement
				 WHERE status_agr = 'ACTIVE'";
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if($num > 0){
            $arreglo = array();
            $cont = 0;
            do{
                $arreglo[$cont++] = $result->GetRowAssoc(false);
                $result->MoveNext();
            }while($cont < $num);
        }
        return $arreglo;
    }

    function insert(){
        $sql = "INSERT INTO agreement (name_agr, discount_value_agr, status_agr) VALUES ('{$this->name_agr}', '{$this->discount_value_agr}', '{$this->status_agr}')";
        $result = $this->db->Execute($sql);
        if($result){
            return $this->db->insert_ID();
        }
        else{
            ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
            return false;
        }
    }

    function update(){
        $sql="UPDATE agreement 
		      SET name_agr='".$this->name_agr."',
			      discount_value_agr='".$this->discount_value_agr."',
			      status_agr='".$this->status_agr."'
		      WHERE serial_agr='".$this->serial_agr."'";
        $result = $this->db->Execute($sql);
        if ($result==true){
            return true;
        }
        else{
            return false;
        }

    }

    function insertInvoiceByAgreement(){
        $sql = "INSERT INTO invoice_by_agreement (serial_inv, serial_agr, discount_applied_iba) VALUES ('{$this->serial_inv}', '{$this->serial_agr}', '{$this->discount_value_agr}')";
        $result = $this->db->Execute($sql);
        if($result){
            return $this->db->insert_ID();
        }
        else{
            ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
            return false;
        }
    }

    //GETTERS
    function getSerial_agr(){
        return $this->serial_agr;
    }

    function getName_agr(){
        return $this->name_agr;
    }

    function getDiscountValue_agr(){
        return $this->discount_value_agr;
    }

    function getStatus_agr(){
        return $this->status_agr;
    }

    function getSerial_inv(){
        return $this->serial_inv;
    }


    //SETTERS
    function setSerial_agr($serial_agr){
        $this->serial_agr = $serial_agr;
    }

    function setName_agr($name_agr){
        $this->name_agr = $name_agr;
    }

    function setDiscountValue_agr($discount_value_agr){
        $this->discount_value_agr = $discount_value_agr;
    }

    function setStatus_agr($status_agr){
        $this->status_agr = $status_agr;
    }

    function setSerial_inv($serial_inv){
        $this->serial_inv = $serial_inv;
    }


}