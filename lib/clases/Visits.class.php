<?php
/**
 * file: Visits.class.php
 * Author: Patricio Astudillo
 * Creation Date: 24/04/2012
 * Modification Date:  24/04/2012
 * Modified By: Patricio Astudillo
 */
class Visits {

	public static function generateDealerVisits($db, $serial_cou, $serial_usr = NULL, $limit = NULL, $step = NULL) {
		if($serial_usr):
			$usr_sql = " AND u.serial_usr = $serial_usr";
		endif;
		
		if($limit):
			$limit_sql = " LIMIT $limit";
		
			if($step):
				$limit_sql .= ", $step";
			endif;
		endif;
		
		$excluded_dealers = '4312,4062,4205,292,4050,953,2438,1917,17';
		
		$sql = "SELECT u.serial_usr, u.first_name_usr, u.last_name_usr, b.name_dea, b.code_dea, 
						b.visits_number_dea, b.serial_mbc, b.email_contact_dea, b.serial_dea, 'BRANCH' AS 'type',
						b.email1_dea, u.username_usr, CONCAT('D', b.serial_dea) AS 'erp_customer_id', b.id_dea,
						b.phone1_dea, mbc.serial_man, b.manager_rights_dea
				FROM user u
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr $usr_sql
					AND ubd.status_ubd = 'ACTIVE'
					AND ubd.end_date_ubd IS NULL
					AND u.status_usr = 'ACTIVE'
				JOIN dealer b ON b.serial_dea = ubd.serial_dea
					AND b.status_dea = 'ACTIVE'
					AND b.serial_dea NOT IN ($excluded_dealers)
				JOIN dealer dea ON dea.serial_dea = b.dea_serial_dea
					AND dea.manager_rights_dea = 'NO'
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc AND mbc.serial_cou = $serial_cou
		
				UNION
						
				SELECT u.serial_usr, u.first_name_usr, u.last_name_usr, d.name_dea, d.code_dea, 
						d.visits_number_dea, d.serial_mbc, d.email_contact_dea, d.serial_dea, 'DEALER' AS 'type',
						d.email1_dea, u.username_usr, CONCAT('D', d.serial_dea) AS 'erp_customer_id', d.id_dea,
						d.phone1_dea, mbc.serial_man, d.manager_rights_dea
				FROM user u
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr $usr_sql
					AND ubd.status_ubd = 'ACTIVE'
					AND ubd.end_date_ubd IS NULL
					AND u.status_usr = 'ACTIVE'
				JOIN dealer d ON d.serial_dea = ubd.serial_dea
					AND d.dea_serial_dea IS NULL
					AND d.manager_rights_dea = 'YES'
					AND d.status_dea = 'ACTIVE'
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc AND mbc.serial_cou = $serial_cou
				
				ORDER BY 4
				$limit_sql";

		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

}

?>
