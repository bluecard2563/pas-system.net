<?php
class TandiLog
{
    var $db;
    var $serial_tal;
    var $arrayids_tal;
    var $send_date_tal;
    var $json_tal;
    var $response_tal;
    var $type_tal;
    var $error_tal;
    var $code_tal;

    function __construct($db, $serial_tal = NULL, $arrayids_tal = NULL, $send_date_tal = NULL, $json_tal = NULL, $response_tal = NULL,$type_tal = NULL, $error_tal = NULL,$code_tal = NULL)
    {
        $this->db = $db;
        $this->serial_tal = $serial_tal;
        $this->arrayids_tal = $arrayids_tal;
        $this->send_date_tal = $send_date_tal;
        $this->json_tal = $json_tal;
        $this->response_tal = $response_tal;
        $this->type_tal = $type_tal;
        $this->error_tal = $error_tal;
        $this->code_tal = $code_tal;
    }

    /*	 * *********************************************
 * function getData
 * ********************************************* */

    function getData() {
        if ($this->serial_tal != NULL) {
            $sql = "SELECT * 
                            FROM tandi_Log
                            WHERE serial_tal ='" . $this->serial_tal . "'";

            $result = $this->db->Execute($sql);
            if ($result === false)
                return false;

            if ($result->fields[0]) {
                $this->serial_tal = $result->fields[0];
                $this->arrayids_tal = $result->fields[1];
                $this->send_date_tal = $result->fields[2];
                $this->json_tal = $result->fields[3];
                $this->response_tal = $result->fields[4];
                $this->type_tal = $result->fields[5];
                $this->error_tal = $result->fields[6];
                $this->code_tal = $result->fields[7];
                return true;
            }
        }
        return false;
    }
    /*	 * *********************************************
         * function insert
         * inserts a new register in DB
         * ********************************************* */

    function insert($db,$table_tal,$arrayids_tal = NULL,$json_tal = NULL,$response_tal = NULL, $type_tal =NULL,$error_tal = NULL,$code_tal = NULL) {
        if(is_object($json_tal)){
    		$log = get_object_vars($json_tal);
    		unset($json_tal['db']);
    	}
    	
    	$json_tal = serialize($json_tal);
    	$json_tal = str_replace("'","\"", $json_tal);
        $sql = "
            INSERT INTO tandi_Log  (
            serial_tal,
            table_tal, 
            arrayids_tal,
            send_date_tal, 
            json_tal, 
            response_tal, 
            type_tal, 
            error_tal,
            code_tal)
            VALUES (
                NULL,
				'" . $table_tal . "',
				'" . $arrayids_tal . "',
				NOW(),
                '" . $json_tal . "',
                '" . $response_tal . "',
                '" . $type_tal . "',
                '" . $error_tal . "',
				'" . $code_tal ."')";

//        Debug::print_r($sql); die();
        $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }
    
    public function updateLog ($db,$serial_tal, $type_tal, $error_tal, $code_tal){
            $sql = "UPDATE tandi_Log SET type_tal = '$type_tal', error_tal = '$error_tal', code_tal = '$code_tal' where serial_tal = '$serial_tal'";
//            Debug::print_r($sql);die();
            $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

}
?>