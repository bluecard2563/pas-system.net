<?php
/*
File: PaymentDetail.class.php
Author: Edwin Salvador
Creation Date:15/03/2010
Modified By:
Last Modified:
*/

class PaymentDetail{
    var $db;
    var $serial_pyf;
    var $serial_pay;
	var $serial_usr;
	var $serial_cn;
    var $type_pyf;
    var $amount_pyf;
    var $date_pyf;
    var $document_number_pyf;
    var $comments_pyf;
    var $status_pyf;
	var $observation_pyf;
	var $checked_pyf;
	var $number_pyf;

    function __construct($db, $serial_pyf = NULL, $serial_pay = NULL, $serial_usr=NULL, $serial_cn=NULL,
						 $type_pyf = NULL, $amount_pyf = NULL, $date_pyf = NULL, $document_number_pyf = NULL,
						 $comments_pyf = NULL, $status_pyf = NULL, $observation_pyf = NULL, $checked_pyf=NULL,
						 $number_pyf=NULL){
        $this -> db = $db;
        $this -> serial_pyf = $serial_pyf;
        $this -> serial_pay = $serial_pay;
		$this -> serial_usr=$serial_usr;
		$this -> serial_cn=$serial_cn;
        $this -> type_pyf = $type_pyf;
        $this -> amount_pyf = $amount_pyf;
        $this -> date_pyf = $date_pyf;
        $this -> document_number_pyf = $document_number_pyf;
        $this -> comments_pyf = $comments_pyf;
        $this -> status_pyf = $status_pyf;
		$this -> observation_pyf = $observation_pyf;
		$this -> checked_pyf = $checked_pyf;
		$this -> number_pyf = $number_pyf;
    }

    /***********************************************
    * function getData
    * sets data by serial_pyf
    ***********************************************/
    function getData(){
        if($this->serial_pyf != NULL){
            $sql = "SELECT serial_pyf, serial_pay, type_pyf, amount_pyf, date_pyf, document_number_pyf, comments_pyf, status_pyf, 
							observation_pyf, serial_cn, serial_usr, checked_pyf, number_pyf
                    FROM payment_detail
                    WHERE serial_pyf='".$this->serial_pyf."'";
            //die($sql);
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_pyf =$result->fields[0];
                $this -> serial_pay = $result->fields[1];
                $this -> type_pyf = $result->fields[2];
                $this -> amount_pyf = $result->fields[3];
                $this -> date_pyf = $result->fields[4];
                $this -> document_number_pyf = $result->fields[5];
                $this -> comments_pyf = $result->fields[6];
                $this -> status_pyf = $result->fields[7];
				$this -> observation_pyf = $result->fields[8];
				$this->serial_cn=$result->fields[9];
				$this->serial_usr=$result->fields[10];
				$this->checked_pyf=$result->fields[11];
				$this -> number_pyf = $result->fields[12];

                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }

    /***********************************************
    * function insert
    * insert a row in the DB
    ***********************************************/
    function insert(){
        $sql = "INSERT INTO payment_detail (
                                        serial_pyf,
                                        serial_pay,
										serial_usr,
										serial_cn,
                                        type_pyf,
                                        amount_pyf,
                                        date_pyf,
                                        document_number_pyf,
                                        comments_pyf,
                                        status_pyf,
										observation_pyf,
										checked_pyf,
										number_pyf
                                    )
                VALUES(NULL,
                    '".$this->serial_pay."',
					'".$this->serial_usr."',";
			if($this->serial_cn==''){
				$sql.=" NULL,";
			}else{
				$sql.=" '".$this->serial_cn."',";
			}
			
			$sql.=" '".$this->type_pyf."',
                    '".($this->amount_pyf?$this->amount_pyf:"0.00")."',";
			($this->date_pyf)?$sql.=" STR_TO_DATE('{$this->date_pyf}', '%d/%m/%Y %H:%i:%s'),":$sql.="NOW(),";
			$sql .="'".$this->document_number_pyf."',
                    '".$this->comments_pyf."',
                    'ACTIVE',
					NULL,";
			($this->status_pyf)?$sql.=" '{$this->status_pyf}',":$sql.=" 'NO',";
			
			$sql .=	"'".$this->number_pyf."');";
        
			$result = $this->db->Execute($sql);
			
	    	if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
    }

	/***********************************************
    * function update
    * update a row in the DB
    ***********************************************/
    function update(){
        $sql = "UPDATE payment_detail
				SET status_pyf =  '".$this->status_pyf."',
				observation_pyf = ";
		if($this->observation_pyf){
			$sql .= "'".$this->observation_pyf."'";
		}else{
			$sql .= "NULL";
		}
		$sql .= " WHERE serial_pyf = '".$this->serial_pyf."'";
        //die($sql);
        $result = $this->db->Execute($sql);

        if($result == true){
            return true;
        }else{
            return false;
        }
    }

    function getPaymentDetails($latest=FALSE){
        if($this->serial_pay){
			if($latest)
				$pay_note="AND number_pyf = ( SELECT MAX(p.number_pyf)
											  FROM payment_detail p
											  WHERE serial_pay={$this->serial_pay})";
			
            $sql = "SELECT serial_pyf, serial_pay, type_pyf, amount_pyf, DATE_FORMAT(date_pyf, '%d/%m/%Y') as date_pyf, 
						   document_number_pyf, comments_pyf, status_pyf, observation_pyf, serial_cn, serial_usr, number_pyf
                    FROM payment_detail
                    WHERE serial_pay='".$this->serial_pay."'
					$pay_note
					ORDER BY date_pyf, amount_pyf";
			
            $result = $this->db -> Execute($sql);
            if ($result == false) return false;

            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
        }else{
            return false;
        }
    }

    /**
    @Name: getStatusList
    @Description: Gets all status in DB for services.
    @Params: N/A
    @Returns: Status array
    **/
    function getPaymentForms(){
        $sql = "SHOW COLUMNS FROM payment_detail LIKE 'type_pyf'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }

    /**
    @Name: validateExcess
    @Description: Gets all status in DB for services.
    @Params: N/A
    @Returns: Status array
    **/
    function validateExcess(){
        $sql = "SHOW COLUMNS FROM payment_detail LIKE 'type_pyf'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
/***********************************************
    * function updateChecked
    * update a row in the DB
    ***********************************************/
    function updateChecked($serial_pyf, $value){
        $sql = "UPDATE payment_detail
				SET checked_pyf =  '".$value."'
				WHERE serial_pyf = '".$serial_pyf."'";
        //die($sql);
        $result = $this->db->Execute($sql);

        if($result == true){
            return true;
        }else{
            return false;
        }
    }
	/***********************************************
    * function getPayment
    * update a row in the DB
    ***********************************************/
	    function getPayment($serial_pyf){
            $sql = "SELECT serial_pyf, GROUP_CONCAT(DISTINCT(inv.number_inv)) AS invoice_group, type_pyf, amount_pyf, DATE_FORMAT(date_pyf, '%d/%m/%Y') as date_pyf,
						   document_number_pyf, IF(pyf.comments_pyf='Efectivo','N/A',pyf.comments_pyf) AS comments_pyf,
						   CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr, dea.name_dea, man.name_man,
						   cou.name_cou, checked_pyf, number_pyf
                    FROM payment_detail pyf
					JOIN user usr ON usr.serial_usr=pyf.serial_usr
					JOIN invoice inv ON inv.serial_pay=pyf.serial_pay
					JOIN sales sal ON sal.serial_inv=inv.serial_inv
					JOIN counter cnt ON cnt.serial_cnt=sal.serial_cnt
					JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc
					JOIN manager man ON man.serial_man=mbc.serial_man
					JOIN sector sec ON dea.serial_sec=sec.serial_sec
					JOIN city cit ON sec.serial_cit=cit.serial_cit
					JOIN country cou ON cit.serial_cou=cou.serial_cou
                    WHERE serial_pyf='".$serial_pyf."'
					GROUP BY serial_pyf";
            $result = $this->db -> Execute($sql);
			return $result->GetRowAssoc(true);
    }

	function getPaymentDetailSerial($serial_cn){
		$sql="SELECT serial_pyf, serial_pay
			  FROM payment_detail
			  WHERE serial_cn = ".$serial_cn;
		
		$result=$this->db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getPaymentInfoForReport($db, $serial_cou, $date_from, $date_to, $serial_mbc = NULL, 
													$serial_dea = NULL, $serial_bra = NULL){
		if($serial_mbc) $mbc_clause = " AND d.serial_mbc = $serial_mbc";
		if($serial_dea) $dea_clause = " AND d.dea_serial_dea = $serial_dea";
		if($serial_bra) $bra_clause = " AND d.serial_dea = $serial_bra";
		
		
		$sql = "SELECT	p.serial_pay, DATE_FORMAT(p.date_pay, '%d/%m/%Y') AS 'date_pay', 
						p.total_payed_pay, (p.total_payed_pay - p.total_to_pay_pay) AS 'excess',
						d.name_dea,	p.serial_pay AS number_pyf,
						GROUP_CONCAT(DISTINCT i.number_inv ORDER BY i.number_inv ASC SEPARATOR ', ') AS number_inv,
						GROUP_CONCAT(DISTINCT IFNULL(CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus, '')), 'N/A') SEPARATOR ', ') AS 'name_cus',(SELECT DATE_FORMAT(pyf2.date_pyf, '%d/%m/%Y') FROM payment_detail pyf2 WHERE pyf2.serial_pay = p.serial_pay ORDER BY pyf2.date_pyf DESC LIMIT 1) as 'last_payment'
				FROM payments p
				JOIN payment_detail pyf ON pyf.serial_pay = p.serial_pay
				JOIN invoice i ON i.serial_pay = p.serial_pay AND i.status_inv <> 'VOID'
				LEFT JOIN customer cus ON i.serial_cus = cus.serial_cus
				JOIN sales s ON s.serial_inv = i.serial_inv
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer d ON d.serial_dea = cnt.serial_dea $mbc_clause $dea_clause $bra_clause
				JOIN sector sec ON sec.serial_sec = d.serial_sec
				JOIN city c ON c.serial_cit = sec.serial_cit AND c.serial_cou = $serial_cou
				WHERE p.date_pay BETWEEN STR_TO_DATE('$date_from','%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$date_to', '%d/%m/%Y'), INTERVAL 1 DAY)
				GROUP BY p.serial_pay
				ORDER BY d.serial_mbc, d.name_dea, p.serial_pay";
		
//		Debug::print_r($sql);DIE();
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getPaymentDetailsForReport($db, $serial_pay){
		$sql = "SELECT	pyf.type_pyf, SUM(amount_pyf) AS 'amount'
				FROM payments p
				JOIN payment_detail pyf ON pyf.serial_pay = p.serial_pay AND p.serial_pay = $serial_pay
				GROUP BY pyf.type_pyf";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getPreDepositsForUncollectablePayments($db, $serial_pay, $exclude_cn = FALSE){
		if($exclude_cn){
			$exclude_cn_sql = " AND type_pyf <> 'CREDIT_NOTE'";
		}
		
		$sql = "SELECT SUM(amount_pyf) AS 'deposit'
				FROM payment_detail
				WHERE serial_pay = $serial_pay
				AND status_pyf = 'ACTIVE'
				AND type_pyf <> 'UNCOLLECTABLE'
				$exclude_cn_sql
				GROUP BY serial_pay";
		
		$result = $db->getOne($sql);

		if($result){
			return $result;
		}else{
			return 0;
		}
	}
    
    //GETTERS
    function getSerial_pay(){
        return $this->serial_pyf;
    }
    function getSerialPay_pyf(){
        return $this->serial_pay;
    }
	function getSerial_cn(){
        return $this->serial_cn;
    }
	function getSerial_usr(){
        return $this->serial_usr;
    }
    function getType_pyf(){
        return  $this->type_pyf;
    }
    function getAmount_pyf(){
        return $this->amount_pyf;
    }
    function getDate_pyf(){
        return $this->date_pyf;
    }
    function getDocumentNumber_pyf(){
        return $this->document_number_pyf;
    }
    function getComments_pyf(){
        return $this->comments_pyf;
    }
    function getStatus_pyf(){
        return $this->status_pyf;
    }
	function getObservation_pyf(){
        return $this->observation_pyf_pyf;
    }
	function getChecked_pyf(){
		return $this->checked_pyf;
	}
	function getNumber_pyf(){
		return $this->number_pyf;
	}
    

    //SETTERS
    function setSerial_pyf($serial_pyf){
        $this->serial_pyf = $serial_pyf;
    }
    function setSerial_pay($serial_pay){
        $this->serial_pay = $serial_pay;
    }
	function setSerial_cn($serial_cn){
        $this->serial_cn = $serial_cn;
    }
	function setSerial_usr($serial_usr){
        $this->serial_usr = $serial_usr;
    }
    function setType_pyf($type_pyf){
        $this->type_pyf = $type_pyf;
    }
    function setAmount_pyf($amount_pyf){
        $this->amount_pyf = $amount_pyf;
    }
    function setDate_pyf($date_pyf){
        $this->date_pyf = $date_pyf;
    }
    function setDocumentNumber_pyf($document_number_pyf){
        $this->document_number_pyf = $document_number_pyf;
    }
    function setComments_pyf($comments_pyf){
        $this->comments_pyf = $comments_pyf;
    }
    function setStatus_pyf($status_pyf){
        $this->status_pyf = $status_pyf;
    }
	function setObservation_pyf($observation_pyf){
        $this->observation_pyf = $observation_pyf;
    }
	function setChecked_pyf($checked_pyf){
		$this->checked_pyf=$checked_pyf;
	}
	function setNumber_pyf($number_pyf){
		$this->number_pyf=$number_pyf;
	}
}

?>