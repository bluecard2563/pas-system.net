<?php
/*
File: PromoWinner.class.php
Author: Patricio Astudillo
Creation Date: 15/06/2010 14:05
Last Modified: 15/06/2010
Modified By: Patricio Astudillo
*/
class PromoWinner{
	var $db;
	var $serial_prw;
	var $serial_prm;
	var $serial_usr;
	var $serial_dea;
	var $date_prw;
	var $act_number_prw;
	var $act_printing_date_prw;
	var $act_deviler_date_prw;

	function __construct($db, $serial_prw = NULL) {
		$this->db = $db;
		$this->serial_prw = $serial_prw;
		
		$this->getData();
	}
	
	function getData(){
		if($this->serial_prw){
			$sql = "SELECT serial_prw, serial_prm, serial_usr, serial_dea, 
							date_prw, act_number_prw, act_printing_date_prw, act_deviler_date_prw 
					FROM promo_winner 
					WHERE serial_prw = {$this->serial_prw}";
			
			$result = $this -> db -> getOne($sql);
			
			if($result){
				$this -> serial_prw = $result['serial_prw'];
				$this -> serial_prm = $result['serial_prm'];
				$this -> serial_dea = $result['serial_dea'];
				$this -> serial_usr = $result['serial_usr'];
				$this -> date_prw = $result['date_prw'];
				$this -> act_number_prw = $result['act_number_prw'];
				$this -> act_printing_date_prw = $result['act_printing_date_prw'];
				$this -> act_deviler_date_prw = $result['act_deviler_date_prw'];
	
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
	function insert() {
		$sql = "
			INSERT INTO promo_winner (
				serial_prw, 
				serial_prm, 
				serial_usr, 
				serial_dea, 
				act_number_prw
			) VALUES (
				NULL,
				'".$this->serial_prm."',
				'".$this->serial_usr."',
				'".$this->serial_dea."',
				'".$this->act_number_prw."'
			)";

		$result = $this->db->Execute($sql);

		if($result){
            return TRUE;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/**
	 * @name registerPrinting
	 * @param RSC $db
	 * @param int $serial_prw
	 * @param string $act_number_prw
	 * @return boolean 
	 */
	public static function registerPrinting($db, $serial_prw, $act_number_prw){
		$sql = "UPDATE promo_winner SET 
					act_number_prw = '$act_number_prw',
					act_printing_date_prw = CURRENT_TIMESTAMP(),
					act_deviler_date_prw = 'YES'
				WHERE $serial_prw = $serial_prw";
		
		$result = $db->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*
	 * hasWinners
	 * @param: $db - DB connection
	 * @param: $serial_prm - Promo ID
	 * @returns: An array of winners to a promo.
	 */
	public static function hasWinners($db, $serial_prm){
		$sql="SELECT serial_prw, serial_dea, serial_prm, serial_usr, date_prw
			  FROM promo_winner
			  WHERE serial_prm = ".$serial_prm;

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	/**
	 * @name registerPrizeInformationForWinner
	 * @param type $db
	 * @param type $serial_prw
	 * @param type $serial_pbp
	 * @param type $amount
	 * @return boolean 
	 */
	public static function registerPrizeInformationForWinner($db, $serial_prw, $serial_pbp, $amount){
		$sql = "INSERT INTO pwinner_prizes VALUES (NULL, $serial_prw, $serial_pbp, $amount)";
		
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name registerSalesForWinner
	 * @param type $db
	 * @param type $serial_prw
	 * @param type $sales_array
	 * @return boolean 
	 */
	public static function registerSalesForWinner($db, $serial_prw, $sales_array){
		if(is_array($sales_array)){
			foreach($sales_array as $serial_sal){
				$sql = "INSERT INTO pwinner_sales VALUES (NULL, $serial_prw, $serial_sal)";
				
				$result = $db -> Execute($sql);

				if(!$result){
					ErrorLog::log($db, 'SALES FOR WINNER COMMIT ERROR', array('serial_prw'=>$serial_prw, 'sales'=>$sales_array, 'sql'=>$sql));
					return FALSE;
				}
			}
			
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function getSerial_prw() {
		return $this->serial_prw;
	}

	public function setSerial_prw($serial_prw) {
		$this->serial_prw = $serial_prw;
	}

	public function getSerial_prm() {
		return $this->serial_prm;
	}

	public function setSerial_prm($serial_prm) {
		$this->serial_prm = $serial_prm;
	}

	public function getSerial_usr() {
		return $this->serial_usr;
	}

	public function setSerial_usr($serial_usr) {
		$this->serial_usr = $serial_usr;
	}

	public function getSerial_dea() {
		return $this->serial_dea;
	}

	public function setSerial_dea($serial_dea) {
		$this->serial_dea = $serial_dea;
	}

	public function getDate_prw() {
		return $this->date_prw;
	}

	public function setDate_prw($date_prw) {
		$this->date_prw = $date_prw;
	}

	public function getAct_number_prw() {
		return $this->act_number_prw;
	}

	public function setAct_number_prw($act_number_prw) {
		$this->act_number_prw = $act_number_prw;
	}

	public function getAct_printing_date_prw() {
		return $this->act_printing_date_prw;
	}

	public function setAct_printing_date_prw($act_printing_date_prw) {
		$this->act_printing_date_prw = $act_printing_date_prw;
	}

	public function getAct_deviler_date_prw() {
		return $this->act_deviler_date_prw;
	}

	public function setAct_deviler_date_prw($act_deviler_date_prw) {
		$this->act_deviler_date_prw = $act_deviler_date_prw;
	}
}
?>