<?php
	class Request
	{
// private:
		function parseList($var_list, $default, $func)
		{
			global $__REQUEST;
			$var_list = explode(',', $var_list);
			$values = array();
			if (func_num_args() <= 3)
				$argv = array();
			else
			{
				$argv = func_get_args();
				array_splice($argv, 0, 3);
			}

			foreach ($var_list as $var_name)
			{
				list($src_name, $dest_name) = explode(':', $var_name.':');
				if (!$dest_name) $dest_name = $src_name;
				
				if (!isset($__REQUEST[$src_name]))
				{
					$values[$dest_name] = $default;
				}
				else if (!is_array($__REQUEST[$src_name]))
				{
					if(sizeof($argv))
						$values[$dest_name] = call_user_func($func, $__REQUEST[$src_name], $argv);
					else
						$values[$dest_name] = call_user_func($func, $__REQUEST[$src_name]);
				}
				else
				{
					$values[$dest_name] = Request::parseArray($__REQUEST[$src_name], $default, $func, $argv);
				}
			}
			return $values;
		}

		function parseArray($var_list, $default, $func, $argv)
		{
			$values = array();
			foreach ($var_list as $key=>$val)
			{
				$values[$key] = !is_array($val) ? call_user_func($func, $val, $argv) : Request::parseArray($val, $default, $func, $argv);
			}
	
			return $values;
		}

		function setVars($vars)
		{
			foreach ($vars as $key=>$val)
			{
				$GLOBALS[$key] = $val;
			}
		}

		function castDate($date, $argv)
		{
			$ts = strtotime($date);
			return date($argv[0], $ts);
		}
		
		function castEnum($value, $argv)
		{
			return in_array($value, $argv[0]) ? $value : NULL;
		}

// public:
		function initialize()
		{
			$vars = array_merge($_GET, $_POST);
			// convert virtual folders to indexes in the $_GET array
			if (array_key_exists('PATH_INFO', $_SERVER))
			{
				$x = $_SERVER["PATH_INFO"];
				if (preg_match('/(.+)\.([A-Za-z0-9]+)/', $x, $matches))
				{
					$_GET['_ext'] = $matches[2];
					$x = $matches[1];
				}
				
				$argv = preg_split('/\//', $x, -1, PREG_SPLIT_NO_EMPTY);
				$_GET = array_merge($argv, $_GET);
				$_REQUEST = array_merge($argv, $_REQUEST);
				$vars = array_merge($argv, $vars);
			}
			$GLOBALS['__REQUEST'] = $vars;
		}	
		
		function getDateString($var_list, $format='m/d/Y', $default = NULL)
		{
			return Request::parseList($var_list, $default, array('Request', 'castDate'), $format);
		}

		function setDateString($var_list, $format='m/d/Y', $default = NULL)
		{
			Request::setVars(Request::getDateString($var_list, $format, $default));
		}
		
		function getEnum($var_list, $options, $default = NULL)
		{
			return Request::parseList($var_list, $default, array('Request', 'castEnum'), $options);
		}
		
		function setEnum($var_list, $options, $default = NULL)
		{
			Request::setVars(Request::getEnum($var_list, $options, $default));
		}

		function getFloat($var_list, $default = 0.0)
		{
			return Request::parseList($var_list, $default, 'floatval');
		}

		function setFloat($var_list, $default = 0.0)
		{
			Request::setVars(Request::getFloat($var_list, $default));
		}

		function getInteger($var_list, $default = 0)
		{
			return Request::parseList($var_list, $default, 'intval');
		}

		function setInteger($var_list, $default = 0)
		{
			Request::setVars(Request::getInteger($var_list, $default));
		}

		function getString($var_list, $default = '')
		{
			return Request::parseList($var_list, $default, 'strval');
		}

		function setString($var_list, $default = '')
		{
			Request::setVars(Request::getString($var_list, $default));
		}
	}
?>
