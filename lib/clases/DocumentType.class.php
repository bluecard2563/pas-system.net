<?php
/*
File: DocumentType.class.php
Author: Santiago Benítez
Creation Date: 11/01/2010 11:15
Last Modified: 11/01/2010 11:15
Modified By: Santiago Benítez
*/
class DocumentType {				
	var $db;
	var $serial_doc;
	var $name_doc;
	
	function __construct($db, $serial_doc = NULL, $name_doc = NULL){
		$this -> db = $db;
		$this -> serial_doc = $serial_doc;
		$this -> name_doc = $name_doc;
	}
		
	/** 
	@Name: getData
	@Description: Retrieves the data of a DocumentType object.
	@Params: DocumentType ID
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_doc!=NULL){
			$sql = 	"SELECT serial_doc, name_doc
					FROM document_type
					WHERE serial_doc='".$this->serial_doc."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_doc=$result->fields[0];
				$this->name_doc=$result->fields[1];	
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	

	/** 
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if O.K. / FALSE on errors
	**/	
	function insert(){		
		$sql="INSERT INTO document_type (
					serial_doc, 
					name_doc					
					)
			  VALUES(NULL,
					'".$this->name_doc."'					
					);";
					
		$result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/** 
	@Name: update
	@Description: updates a register in DB
	@Params: N/A
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function update(){
		$sql="UPDATE document_type 
		SET name_doc='".$this->name_doc."'
		WHERE serial_doc='".$this->serial_doc."'";
					
		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/** 
	@Name: getDocumentTypes
	@Description: Gets all Document Types
	@Params: N/A
	@Returns: DocumentTypes array
	**/
	function getDocumentTypes(){
		$list=NULL;
		$sql = 	"SELECT serial_doc, name_doc
				 FROM document_type";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}	
	
	/** 
	@Name: getDocumentTypesAutocompleter
	@Description: Returns an array of DocumentTypes.
	@Params: $name_doc (DocumentType name or pattern).
	@Returns: DocumentType array
	**/
	function getDocumentTypesAutocompleter($name_doc){
		$sql = "SELECT serial_doc, name_doc
				FROM document_type
				WHERE LOWER(name_doc) LIKE _utf8'%".utf8_encode($name_doc)."%' collate utf8_bin
				LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/** 
	@Name: existsDocumentType
	@Description: verifies if a Document Type exists or not
	@Params: $txtNameDocumentType(DocumentType name), $serial_doc(DocumentType serial).
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function documentTypeExists($txtNameDocumentType, $serial_doc=NULL){
		$sql = 	"SELECT serial_doc
				 FROM document_type
				 WHERE LOWER(name_doc)= _utf8'".utf8_encode($txtNameDocumentType)."' collate utf8_bin";	 
		if($serial_doc!=NULL){
			$sql.=" AND serial_doc <> ".$serial_doc;
		}
		
		$result = $this->db->Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
		
	///GETTERS
	function getSerial_doc(){
		return $this->serial_doc;
	}
	function getName_doc(){
		return $this->name_doc;
	}


	///SETTERS	
	function setSerial_doc($serial_doc){
		$this->serial_doc = $serial_doc;
	}
	function setName_doc($name_doc){
		$this->name_doc = $name_doc;
	}
}
?>