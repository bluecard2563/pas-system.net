<?php
/*
File: PrizeByPromo.class.php
Author: Santiago Benitez
Creation Date: 03/03/2010
Last Modified: 03/03/2010 09:40
Modified By: Santiago Benitez
*/
class PrizeByPromo {
	var $db;
	var $serial_pbp;
	var $serial_prm;
	var $serial_pri;
	var $starting_value_pbp;
	var $end_value_pbp;

	function __construct($db, $serial_pbp = NULL, $serial_prm = NULL, $serial_pri = NULL, $starting_value_pbp = NULL,$end_value_pbp = NULL){
		$this -> db = $db;
		$this -> serial_pbp = $serial_pbp;
		$this -> serial_prm = $serial_prm;
		$this -> serial_pri = $serial_pri;
		$this -> starting_value_pbp = $starting_value_pbp;
		$this -> end_value_pbp = $end_value_pbp;
	}

	/* function getData() 		*/
	/* set class parameters		*/

	function getData(){
		if($this->serial_pbp!=NULL){
			$sql = 	"SELECT serial_pbp, serial_prm,serial_pri,starting_value_pbp,end_value_pbp
					FROM prize_by_promo
					WHERE serial_pbp='".$this->serial_pbp."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_pbp=$result->fields[0];
				$this->serial_prm=$result->fields[1];
				$this->serial_pri=$result->fields[2];
				$this->starting_value_pbp=$result->fields[3];
				$this->end_value_pbp=$result->fields[4];
				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/* function insert() 				*/
	/* insert a new pbpze into database	*/

	function insert(){
		$sql="INSERT INTO prize_by_promo (
					serial_pbp,
					serial_prm,
					serial_pri,
					starting_value_pbp,
					end_value_pbp
					)
			  VALUES(NULL,
					'".$this->serial_prm."',
					".$this->serial_pri.",
					".$this->starting_value_pbp.",
					'".$this->end_value_pbp."'
					);";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

	/* function update() 						*/
	/* update a pbpze							*/

	function update(){

		if($this->serial_pbp!=NULL){
			$sql="UPDATE prize_by_promo
			SET serial_prm='".$this->serial_prm."',
				serial_pri=".$this->serial_pri.",
				starting_value_pbp=".$this->starting_value_pbp.",
				end_value_pbp='".$this->end_value_pbp."'
			WHERE serial_pbp='".$this->serial_pbp."'";
			$result = $this->db->Execute($sql);
			if ($result==true)
				return true;
			else
				return false;
		}else
			return false;
	}
        /**
	* @Name: delete
	* @Description: Deletes some registers in DB
	* @Params: $serial_prm: serial of the promotion.
	* @Returns: TRUE if OK. / FALSE on error
	**/
        function delete($serial_prm) {
            $sql = "DELETE FROM prize_by_promo
                    WHERE serial_prm = '".$serial_prm."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

        /**
	* @Name: getPrizesByPromo
	* @Description: Retrieves all the prizes acording a pattern
	* @Params: serial_prm
	* @Returns: $array
	**/
	function getPrizesByPromo($serial_prm, $available=NULL){
		$sql = "SELECT pbp.serial_pbp, pbp.starting_value_pbp, pbp.end_value_pbp, pri.name_pri, pri.stock_pri, pbl.name_pbl, pxc.serial_pxc
				FROM prize_by_promo pbp
				JOIN prizes pri ON pri.serial_pri=pbp.serial_pri
				JOIN product_by_country_by_prize pcp ON pcp.serial_pbp=pbp.serial_pbp
				JOIN product_by_country pxc ON pxc.serial_pxc=pcp.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang='".$_SESSION['serial_lang']."'
				WHERE pbp.serial_prm=".$serial_prm;

		if($available){
			$sql.=" AND pri.stock_pri > 0";
		}else{
			$sql.=" LIMIT 10";
		}

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
	}

	/**
	* @Name: getPrizesByPromo
	* @Description: Retrieves all the prizes acording a pattern
	* @Params: serial_prm
	* @Returns: $array
	**/
	function getPrizesByPromoToReclaim($serial_prm, $type_prm){
		$sql = "SELECT pbp.serial_pbp, pbp.starting_value_pbp, pbp.end_value_pbp, pri.name_pri, pri.stock_pri,
						GROUP_CONCAT(DISTINCT CAST(pbl.name_pbl AS CHAR) SEPARATOR ',<br>' ) AS 'name_pbl',
						GROUP_CONCAT(DISTINCT CAST(pcp.serial_pxc AS CHAR) SEPARATOR ',' ) AS 'serials',
						pri.serial_pri
				FROM prize_by_promo pbp
				JOIN prizes pri ON pri.serial_pri=pbp.serial_pri
				JOIN product_by_country_by_prize pcp ON pcp.serial_pbp=pbp.serial_pbp
				JOIN product_by_country pxc ON pxc.serial_pxc=pcp.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang='".$_SESSION['serial_lang']."'
				WHERE pbp.serial_prm=".$serial_prm."
				AND pri.stock_pri > 0
				GROUP BY pcp.serial_pbp";

		if($type_prm=='AMOUNT'){
			$sql.=" ORDER BY pbp.starting_value_pbp DESC";
		}				

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
	}
        

	///GETTERS
	function getSerial_pbp(){
		return $this->serial_pbp;
	}
	function getSerial_prm(){
		return $this->serial_prm;
	}
	function getSerial_pri(){
		return $this->serial_pri;
	}
	function getStarting_value_pbp(){
		return $this->starting_value_pbp;
	}
	function getEnd_value_pbp(){
		return $this->end_value_pbp;
	}

	///SETTERS
	function setSerial_pbp($serial_pbp){
		$this->serial_pbp = $serial_pbp;
	}
	function setSerial_prm($serial_prm){
		$this->serial_prm = $serial_prm;
	}
	function setSerial_pri($serial_pri){
		$this->serial_pri = $serial_pri;
	}
	function setStarting_value_pbp($starting_value_pbp){
		$this->starting_value_pbp = $starting_value_pbp;
	}
	function setEnd_value_pbp($end_value_pbp){
		$this->end_value_pbp = $end_value_pbp;
	}

}
?>