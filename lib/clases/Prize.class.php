<?php
/*
File: Prize.class.php
Author:Nicolas Flores
Creation Date: 04/01/2010
Last Modified: 12/01/2010 08:40
Modified By: Santiago Benítez
*/

class Prize {				
	var $db;
	var $serial_pri;
	var $serial_pro;
	var $serial_cou;
	var $name_pri;
	var $cost_pri;
	var $stock_pri;
	var $status_pri;
	var $type_pri;
	var $coverage_pri;
	var $amount_pri;
	var $auxData;
	
	function __construct($db, $serial_pri = NULL, $serial_pro = NULL, $serial_cou = NULL, $name_pri = NULL, $cost_pri = NULL, $stock_pri = NULL,$status_pri = NULL, $type_pri=NULL, $coverage_pri=NULL, $amount_pri=NULL){
		$this -> db = $db;
		$this -> serial_pri = $serial_pri;
		$this -> serial_pro = $serial_pro;
		$this -> serial_cou = $serial_cou;
		$this -> name_pri = $name_pri;
		$this -> cost_pri = $cost_pri;
		$this -> stock_pri = $stock_pri;
		$this -> status_pri = $status_pri;
		$this -> type_pri = $type_pri;
                $this -> coverage_pri = $coverage_pri;
                $this -> amount_pri = $amount_pri;
	}
	
	/* function getData() 		*/
	/* set class parameters		*/
	
	function getData(){
		if($this->serial_pri!=NULL){
			$sql = 	"SELECT pri.serial_pri, pri.serial_pro, pri.serial_cou, pri.name_pri, pri.cost_pri, pri.stock_pri, pri.status_pri, pri.type_pri, pri.coverage_pri, pri.amount_pri, cou.serial_zon
					FROM prizes pri
                    LEFT JOIN country cou ON pri.serial_cou=cou.serial_cou
					WHERE serial_pri='".$this->serial_pri."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_pri=$result->fields[0];
				$this->serial_pro=$result->fields[1];
				$this->serial_cou=$result->fields[2];
				$this->name_pri=$result->fields[3];
				$this->cost_pri=$result->fields[4];
				$this->stock_pri=$result->fields[5];
				$this->status_pri=$result->fields[6];
				$this->type_pri=$result->fields[7];
				$this->coverage_pri=$result->fields[8];
				$this->amount_pri=$result->fields[9];
				$this->auxData['serial_zon']=$result->fields[10];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/* function insert() 				*/
	/* insert a new prize into database	*/
	
	function insert(){
		$sql="INSERT INTO prizes (
					serial_pri,
                                        serial_pro,
                                        serial_cou,
					name_pri,
					cost_pri,
					stock_pri,
					status_pri,
					type_pri,
                                        coverage_pri,
                                        amount_pri
					)
			  VALUES(NULL,";
                          if($this->serial_pro){
					$sql.="'".$this->serial_pro."',";
                          }else{
                                        $sql.="NULL,";
                          }
                          if($this->serial_cou){
					$sql.="'".$this->serial_cou."',";
                          }else{
                                        $sql.="NULL,";
                          }
                                        $sql.="'".$this->name_pri."',
					".$this->cost_pri.",
					".$this->stock_pri.",
					'ACTIVE',
					'".$this->type_pri."',";
                          if($this->coverage_pri){
					$sql.="'".$this->coverage_pri."',";
                          }else{
                                        $sql.="NULL,";
                          }
                          if($this->amount_pri){
					$sql.="'".$this->amount_pri."')";
                          }else{
                                        $sql.="NULL)";
                          }
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/* function update() 						*/
	/* update a prize							*/

	function update(){
		if($this->serial_pri!=NULL){
			$sql="UPDATE prizes SET ";
		  if($this->serial_pro){
			$sql.=" serial_pro='".$this->serial_pro."',";
		  }else{
			$sql.=" serial_pro=NULL,";
		  }
		  if($this->serial_cou){
			$sql.=" serial_cou='".$this->serial_cou."',";
		  }else{
			$sql.=" serial_cou=NULL,";
		  }
			$sql.=" name_pri='".$this->name_pri."',
					cost_pri=".$this->cost_pri.",
					stock_pri=".$this->stock_pri.",
					status_pri='".$this->status_pri."',
					type_pri='".$this->type_pri."',";
		  if($this->coverage_pri){
			$sql.=" coverage_pri='".$this->coverage_pri."',";
		  }else{
			$sql.=" coverage_pri=NULL,";
		  }
		  if($this->amount_pri){
			$sql.=" amount_pri='".$this->amount_pri."'";
		  }else{
			$sql.=" amount_pri=NULL";
		  }
			$sql.=" WHERE serial_pri='".$this->serial_pri."'";
			//echo $sql.'<br>';
			$result = $this->db->Execute($sql);
			if ($result==true) 
				return true;
			else
				return false;
		}else
			return false;		
	}
	
	
	/** 
	@Name: getPrizesAutocompleter
	@Description: Returns an array of prizes.
	@Params: prize name or pattern.
	@Returns: prizes array
	**/
	function getPrizesAutocompleter($name_pri){
		$sql = "SELECT serial_pri, name_pri
				FROM prizes
				WHERE LOWER(name_pri) LIKE _utf8'%".utf8_encode($name_pri)."%' collate utf8_bin
				LIMIT 10";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

        /**
	@Name: getPrize
	@Description: Returns an array of prizes.
	@Params: $type_pri: Prize Type
	@Returns: prizes array
	**/
	function getPrize($type_pri){
		$sql = "SELECT serial_pri, name_pri
			FROM prizes
			WHERE type_pri = '".$type_pri."'
                        AND status_pri = 'ACTIVE'
                        and stock_pri > '0'
			LIMIT 10";
                //echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /**
	@Name: getPrizes
	@Description: Returns an array of prizes.
	@Params: $type_pri: Prize Type
	@Returns: prizes array
	**/
	function getPrizes($serial_cou,$type_pri){
		$sql = "SELECT serial_pri, name_pri
			FROM prizes
			WHERE ";
                if($type_pri){
                    $sql.="type_pri = '".$type_pri."'";
                }
                if($serial_cou){
                    $sql.="serial_cou= '".$serial_cou."'";
                }
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}

        /**
	* @Name: prizeExist
	* @Description: Verifies if a prize name exists for a specific type
	* @Params: $name_pri: prize name pattern
        *          $type_pri: prize type
	* @Returns: prizes array
	**/
	function prizeExist($name_pri, $type_pri, $serial_pri){
		$sql = "SELECT serial_pri
			FROM prizes
			WHERE LOWER(name_pri) = _utf8'".utf8_encode($name_pri)."' collate utf8_bin
			AND type_pri = '".$type_pri."'";

                if($serial_pri!=NULL and $serial_pri!=""){
                    $sql.=" AND serial_pri <> '".$serial_pri."'";
                }
                
		$result = $this->db -> Execute($sql);


		$result = $this->db->Execute($sql);

		if ($result->fields['0'])
			return true;
		else
            return false;
	}

	public static function substractStock($db, $serial_pri, $quntity){
		$sql="UPDATE prizes SET
			  stock_pri=(stock_pri-".$quntity.")
			  WHERE serial_pri =".$serial_pri;

		$result=$db->execute($sql);

		if($result){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
    * function getStatusValues
    * Returns the current costs for the status field.
    ***********************************************/	
	function getStatusValues(){
		$sql = "SHOW COLUMNS FROM prizes LIKE 'status_pri'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
    * function getTypeValues
    * Returns the current costs for the type field.
    ***********************************************/	
	function getTypeValues(){
		$sql = "SHOW COLUMNS FROM prizes LIKE 'type_pri'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

        /***********************************************
    * function getTypeValues
    * Returns the current costs for the type field.
    ***********************************************/
	function getCoverageValues(){
		$sql = "SHOW COLUMNS FROM prizes LIKE 'coverage_pri'";
		$result = $this->db -> Execute($sql);

		$coverage=$result->fields[1];
		$coverage = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$coverage));
		return $coverage;
	}
	/*
	* @Name: getBonusPrizesByCountry
	* @Description: get all the bonus prizes availables by country
	* @Params: $serial_cou: serialof the country to analize
	 *			$amountLessThan: to get only prizes with amount less than xxxx
	* @Returns: prizes array
	**/
	function getBonusPrizesByCountry($serial_cou,$amountLessThan,$bonusPoints = NULL){
		$sql = "SELECT pr.serial_pri,
						  pr.serial_cou,
						  pr.name_pri,
						  pr.cost_pri,
						  pr.stock_pri,
						  pr.status_pri,
						  pr.type_pri,
						  pr.coverage_pri,
						  pr.amount_pri,
                                                  (pr.amount_pri * $bonusPoints) as points_amount
				FROM prizes pr
				WHERE pr.serial_cou=$serial_cou
				AND pr.stock_pri>0
				AND pr.status_pri='ACTIVE'
				AND pr.type_pri='BONUS'
				AND pr.amount_pri<=$amountLessThan";
		$result=$this->db->getAll($sql);
		//echo $sql;
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	///GETTERS
	function getSerial_pri(){
		return $this->serial_pri;
	}
        function getSerial_pro(){
		return $this->serial_pro;
	}
        function getSerial_cou(){
		return $this->serial_cou;
	}
	function getName_pri(){
		return $this->name_pri;
	}
	function getCost_pri(){
		return $this->cost_pri;
	}
	function getStock_pri(){
		return $this->stock_pri;
	}
	function getStatus_pri(){
		return $this->status_pri;
	}
	function getType_pri(){
		return $this->type_pri;
	}
        function getCoverage_pri(){
		return $this->coverage_pri;
	}
	function getAmount_pri(){
		return $this->amount_pri;
	}
        function getAuxData(){
		return $this->auxData;
	}

        

	///SETTERS	
	function setSerial_pri($serial_pri){
		$this->serial_pri = $serial_pri;
	}
        function setSerial_pro($serial_pro){
		$this->serial_pro = $serial_pro;
	}
        function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setName_pri($name_pri){
		$this->name_pri = $name_pri;
	}
	function setCost_pri($cost_pri){
		$this->cost_pri = $cost_pri;
	}
	function setStock_pri($stock_pri){
		$this->stock_pri = $stock_pri;
	}
	function setStatus_pri($status_pri){
		$this->status_pri = $status_pri;
	}
	function setType_pri($type_pri){
		$this->type_pri = $type_pri;
	}
        function setCoverage_pri($coverage_pri){
		$this->coverage_pri = $coverage_pri;
	}
	function setAmount_pri($amount_pri){
		$this->amount_pri = $amount_pri;
	}
}
?>
