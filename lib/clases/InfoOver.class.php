<?php
/*
File: InfoOver.class.php
Author: Santiago Borja
Creation Date: 17/03/2010
*/
class InfoOver{
	var $db;
	var $serial_apo;
	var $serial_ove;
	var $begin_date_ino;
	var $end_date_ino;
	var $status_ino;
	var $amount_ino;
	var $percentage_ino;

	function __construct($db, $serial_apo=NULL, $serial_ove=NULL, $begin_date_ino=NULL, $end_date_ino=NULL, $status_ino=NULL, $amount_ino=NULL, $percentage_ino=NULL){
		$this -> db = $db;
		$this -> serial_apo=$serial_apo;
		$this -> serial_ove=$serial_ove;
		$this -> begin_date_ino=$begin_date_ino;
		$this -> end_date_ino=$end_date_ino;
		$this -> status_ino=$status_ino;
		$this -> amount_ino=$amount_ino;
		$this -> percentage_ino=$percentage_ino;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a Over object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_apo!=NULL){
			$sql = 	"SELECT p.serial_apo, p.serial_ove, DATE_FORMAT(p.begin_date_ino, '%d/%m/%Y'),DATE_FORMAT( p.end_date_ino, '%d/%m/%Y'), p.status_ino, p.amount_ino, p.percentage_ino
                                FROM info_overs p
                                WHERE p.serial_apo = '".$this->serial_apo."' AND
                                p.serial_ove = '".$this->serial_ove."'";
			if($this->begin_date_ino !='' && $this->end_date_ino!=''){
				$sql.="AND p.begin_date_ino = {$this->begin_date_ino}
					   AND p.end_date_ino = {$this->end_date_ino}";
			}
			$result = $this->db->Execute($sql);
			if ($result === false) return false;
			
			if($result->fields[0]){
				$this -> serial_apo=$result->fields[0];
				$this -> serial_ove=$result->fields[1];
				$this -> begin_date_ino=$result->fields[2];
                $this -> end_date_ino=$result->fields[3];
                $this -> status_ino=$result->fields[4];
                $this -> amount_ino=$result->fields[5];
				$this -> percentage_ino=$result->fields[6];
				return true;
			}
		}
		return false;
	}
    
	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
   function insert() {
       $sql = "INSERT INTO info_overs (
                    serial_apo,
                    serial_ove,
                    begin_date_ino,
                    end_date_ino,
                    status_ino,
                    amount_ino,
					percentage_ino)
                VALUES (
                    ".$this->serial_apo.",
                    ".$this->serial_ove.",
                    STR_TO_DATE('".$this->begin_date_ino."','%d/%m/%Y'),
                    STR_TO_DATE('".$this->end_date_ino."','%d/%m/%Y'),
                    '".$this->status_ino."',
					'".$this->amount_ino."',
            		'".$this->percentage_ino."')";
	   $result = $this->db->Execute($sql);

   		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
}

	/**
	@Name: insertNextOver
	@Description: Inserts a register in DB for a new over with the info of the previous one.
	@Params: NA
	@Returns: TRUE if OK. / FALSE on error
	*/
	function insertNextOver(){
		 $sql = "SELECT	serial_ove,
						serial_apo,
						DATE_FORMAT(DATE_ADD(MAX(begin_date_ino), INTERVAL +1 YEAR), '%d/%m/%Y') AS 'begin_date',
						DATE_FORMAT(DATE_ADD(MAX(end_date_ino), INTERVAL +1 YEAR), '%d/%m/%Y') AS 'end_date'
				FROM info_overs
				WHERE serial_apo = {$this->serial_apo}
				GROUP BY serial_ove,serial_apo";
						
            $result = $this->db->getRow($sql);
			
            if($result){
				$sql="INSERT INTO info_overs (
							serial_ove,
							serial_apo,
							begin_date_ino,
							end_date_ino,
							status_ino,
							amount_ino,
							percentage_ino)
						VALUES (
							".$result['serial_ove'].",
							".$result['serial_apo'].",
							STR_TO_DATE('".$result['begin_date']."','%d/%m/%Y'),
							STR_TO_DATE('".$result['end_date']."','%d/%m/%Y'),
							'ACTIVE',
							NULL,
							NULL)";
				
				$result = $this->db->Execute($sql);

				if($result)
					return TRUE;
				else
					return FALSE;

			}else{
				return false;
			}
	}

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK/ FALSE on error
	**/
    function update($active_condition = true) {
        $sql = "UPDATE info_overs SET
                    begin_date_ino = STR_TO_DATE('".$this->begin_date_ino."','%d/%m/%Y'),
                    end_date_ino = STR_TO_DATE('".$this->end_date_ino."','%d/%m/%Y'),
                    status_ino='".$this->status_ino."',
                    amount_ino='".$this->amount_ino."',
					percentage_ino='".$this->percentage_ino."'
            WHERE serial_apo = '".$this->serial_apo."'
			AND serial_ove = ".$this->serial_ove."
			AND begin_date_ino  = STR_TO_DATE('".$this->begin_date_ino."','%d/%m/%Y')
			AND end_date_ino = STR_TO_DATE('".$this->end_date_ino."','%d/%m/%Y')";
			;
		if($active_condition){
			$sql.=" AND status_ino='PENDING'";
		}			

        $result = $this->db->Execute($sql);

        if ($result === false)return false;

        if($result==true)
            return true;
		
        return false;
    }
    
	/**
	@Name: getAppliedType
	@Description: Gets all Info Overs whose end date is before current date and their status is ACTIVE
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getAllPastActive($db){
		$sql = "SELECT serial_ove, serial_apo, DATE_FORMAT(begin_date_ino,'%d/%m/%Y')as begin_date_ino,
						DATE_FORMAT(end_date_ino,'%d/%m/%Y')as end_date_ino, status_ino, amount_ino,
						percentage_ino
				FROM info_overs
				WHERE end_date_ino < CURDATE()
				AND status_ino = 'ACTIVE'";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}
	
	public static function getLasActiveInfoOver($db, $serial_ove){
		$sql = "SELECT serial_ove, serial_apo, DATE_FORMAT(begin_date_ino, '%d/%m/%Y') AS 'begin_date_ino', 
						DATE_FORMAT(end_date_ino, '%d/%m/%Y') AS 'end_date_ino', status_ino, amount_ino, percentage_ino
				FROM info_overs
				WHERE status_ove in ('ACTIVE','INACTIVE','PENDING','DENIED') and serial_ove = $serial_ove
				ORDER BY serial_ove DESC";
		
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function disablePreviousActiveInfoOver($db, $serial_ove){
		$sql = "UPDATE info_overs SET 
						status_ino = 'DENIED'
				WHERE status_ino = 'ACTIVE'
				AND serial_ove = $serial_ove";
		
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	//Getters
	function getSerial_apo(){
		return $this->serial_apo;
	}
	function getSerial_ove(){
		return $this->serial_ove;
	}
	function getBegin_date_ino(){
		return $this->begin_date_ino;
	}
	function getEnd_date_ino(){
		return $this->end_date_ino;
	}
	function getStatus_ino($status_ino){
		return $this->status_ino;
	}
	function getAmount_ino($amount_ino){
		return $this->amount_ino;
	}
	function getPercentage_ino($percentage_ino){
		return $this->percentage_ino;
	}

	//Setters
	function setSerial_apo($serial_apo){
		$this->serial_apo=$serial_apo;
	}
	function setSerial_ove($serial_ove){
		$this->serial_ove=$serial_ove;
	}
    function setEnd_date_ino($end_date_ino){
		$this->end_date_ino=$end_date_ino;
	}
	function setBegin_date_ino($begin_date_ino){
		$this->begin_date_ino=$begin_date_ino;
	}
	function setStatus_ino($status_ino){
		$this->status_ino=$status_ino;
	}
	function setAmount_ino($amount_ino){
		$this->amount_ino=$amount_ino;
	}
	function setPercentage_ino($percentage_ino){
		$this->percentage_ino=$percentage_ino;
	}
}
?>