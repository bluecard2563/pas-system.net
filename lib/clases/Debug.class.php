<?php
	class Debug
	{
		public static function print_r($hash, $name = "Display")
		{
			echo "<div style='font-size: 12px; align: left; text-align: left;'>";
			echo "<pre>";
			echo "<p><b>$name</b></p>";
			print_r($hash);
			echo "</pre>";
			echo "</div>";
		}

		public static function var_dump($var, $name = "Display")
		{
			echo "<div style='font-size: 12px; align: left; text-align: left;'>";
			echo "<pre>";
			echo "<p><b>$name</b></p>";
			var_dump($var);
			echo "</pre>";
			echo "</div>";
		}

		public function isDeveloper()
		{
			# note: add your ip to this array
			$ip = array('67.184.12.94','66.225.59.250');

			return in_array($_SERVER['REMOTE_ADDR'], $ip);
		}
	}
?>
