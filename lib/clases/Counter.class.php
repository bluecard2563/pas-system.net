<?php
/*
File: Counter.class.php
Author: Patricio Astudillo
Creation Date: 14/01/2010 11:48
Last Modified: 14/01/2010 11:36
Modified By: Patricio Astudillo
*/
class Counter {				
	var $db;
	var $serial_cnt;
	var $serial_dea;
	var $serial_usr;
	var $status_cnt;
    var $aux_cnt;
	
	
	function __construct($db, $serial_cnt = NULL, $serial_dea = NULL, $serial_usr = NULL, $status_cnt = NULL){
		$this -> db = $db;
		$this -> serial_cnt = $serial_cnt;
		$this -> serial_dea = $serial_dea;
		$this -> serial_usr = $serial_usr;
		$this -> status_cnt = $status_cnt;
	}
	
	/** 
	@Name: getData
	@Description: Retrieves the data of a Counter object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_cnt!=NULL){
			$sql = "SELECT c.serial_cnt, c.serial_dea, c.serial_usr, c.status_cnt,
                            b.name_dea, d.name_dea, CONCAT(u.first_name_usr,' ',u.last_name_usr),b.dea_serial_dea, u.sell_free_usr, d.serial_mbc, u.isLeader_usr
					FROM counter c
                    JOIN dealer b ON b.serial_dea=c.serial_dea
                    JOIN dealer d ON d.serial_dea=b.dea_serial_dea
                    JOIN user u ON u.serial_usr=c.serial_usr
				    WHERE c.serial_cnt='".$this->serial_cnt."'";
					
			$result = $this->db->Execute($sql);
			if ($result === false) return false;
			if($result->fields[0]){
				$this->serial_cnt=$result->fields[0];
				$this->serial_dea=$result->fields[1];
				$this->serial_usr=$result->fields[2];	
				$this->status_cnt=$result->fields[3];
				$this->aux_cnt['branch']=$result->fields[4];
				$this->aux_cnt['dealer']=$result->fields[5];
				$this->aux_cnt['counter']=$result->fields[6];
				$this->aux_cnt['dea_serial_dea']=$result->fields[7];
				$this->aux_cnt['sell_free_usr']=$result->fields[8];
				$this->aux_cnt['serial_mbc']=$result->fields[9];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}


	/** 
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
	function insert(){
		$sql="INSERT INTO counter (
					serial_cnt,
					serial_dea,
					serial_usr,
					status_cnt			
					)
			  VALUES(NULL,";
					 if($this->serial_dea != ''){
						$sql.="'".$this->serial_dea."',";
					}
					else{
						$sql.= "NULL,";
					}				
					$sql.="'".$this->serial_usr."',					
					'".$this->status_cnt."'			
					);";
		//echo $sql;
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/** 
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
	function update(){
		$sql="UPDATE counter
		SET serial_dea='".$this->serial_dea."',
			serial_usr='".$this->serial_usr."',
			status_cnt='".$this->status_cnt."'
		WHERE serial_cnt='".$this->serial_cnt."'";
//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/** 
	@Name: getCountersByDealer
	@Description: Returns an array of counters.
	@Params: $serial_dea (Dealer ID)
	@Returns: counter array
	**/
	function getCountersByDealer($serial_dea){
		$list=NULL;
		$sql = 	"SELECT serial_cnt, serial_dea, serial_usr, status_cnt
				 FROM counter
				 WHERE serial_dea=".$serial_dea;
				 
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

        /**
	@Name: getSuitableCountersByDealer
	@Description: Returns an array of cuitable counters.
	@Params: $serial_dea (Dealer ID)
	@Returns: users array
	**/
	function getSuitableCountersByDealer($serial_dea, $offSeller, $serial_mbc, $offManager){
            $sql = "SELECT u.serial_usr, u.document_usr, u.first_name_usr, u.last_name_usr
                     FROM user u
                     WHERE  u.serial_dea='".$serial_dea."'
                     AND u.serial_usr NOT IN (SELECT c.serial_usr
                                              FROM counter c
                                              WHERE c.serial_dea = '$serial_dea')
					 AND u.serial_usr <> 1
					 AND u.status_usr='ACTIVE'";

            if($offSeller=='YES'){
                $sql.=" UNION (SELECT u.serial_usr, u.document_usr, u.first_name_usr, u.last_name_usr
                               FROM user u
                               JOIN manager_by_country mbc ON mbc.serial_mbc=u.serial_mbc AND mbc.serial_mbc='$serial_mbc'
                               WHERE  u.serial_dea IS NULL
                               AND u.serial_usr NOT IN (SELECT c.serial_usr
                                                      FROM counter c
                                                      WHERE c.serial_dea = '$serial_dea')
							   AND u.serial_usr <> 1
							   AND u.status_usr='ACTIVE')";
                
            }
            if($offManager=='YES' && $offSeller=='YES'){
                $sql.=" UNION (SELECT u.serial_usr, u.document_usr, u.first_name_usr, u.last_name_usr
								FROM user u
								JOIN manager_by_country mbc ON mbc.serial_mbc=u.serial_mbc
								JOIN manager man ON mbc.serial_man=man.serial_man AND man.man_serial_man IS NULL
								JOIN manager man2 ON man.serial_man=man2.man_serial_man
								JOIN manager_by_country mbc2 ON mbc2.serial_man=man2.serial_man
								WHERE mbc2.serial_mbc='$serial_mbc'
								AND u.serial_usr NOT IN (SELECT c.serial_usr FROM counter c WHERE c.serial_dea = '$serial_dea' )
								AND u.serial_usr <> 1
								AND u.status_usr='ACTIVE')";
            }
           
//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
	/** 
	@Name: getCountersAutocompleter
	@Description: Returns an array of counters.
	@Params: $pattern, $serial_dea (Dealer ID)
	@Returns: counter array
	**/
	function getCountersAutocompleter($pattern, $serial_dea){
		$sql = "SELECT c.serial_cnt, c.serial_dea, c.serial_usr, u.first_name_usr, u.last_name_usr, u.document_usr
				FROM counter c
				JOIN user u ON u.serial_usr=c.serial_usr
				WHERE (LOWER(u.first_name_usr) LIKE _utf8'%".utf8_encode($pattern)."%' collate utf8_bin
				OR LOWER(u.last_name_usr) LIKE _utf8'%".utf8_encode($pattern)."%' collate utf8_bin
				OR LOWER(u.document_usr) LIKE _utf8'%".utf8_encode($pattern)."%' collate utf8_bin)
				AND c.serial_dea=".$serial_dea."
				AND u.serial_usr <> 1
				LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/** 
	@Name: counterExists
	@Description: Verifies if a user has already been assigned to 
				  a dealer as counter.
	@Params: $serial_usr (User ID), $serial_cnt (Counter ID)
	@Returns: TRUE if exists / FALSE if not.
	**/
	function counterExists($serial_usr, $serial_cnt=NULL){
		$sql = 	"SELECT serial_cnt
				 FROM counter
				 WHERE serial_usr='".$serial_usr."'";
		if($serial_cnt!=NULL){
			$sql.=" AND serial_cnt <> ".$serial_cnt;
		}
		//echo $sql;
		$result = $this->db -> Execute($sql);
		
		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

        /**
	@Name: getCounters
	@Description: Verifies if a user has already been assigned to
				  a dealer as counter.
	@Params: $serial_usr (User ID), $serial_cnt (Counter ID)
	@Returns: An array with all counters if exists / FALSE if not.
	**/
	function getCounters($serial_usr, $serial_cnt=NULL){
		$sql = 	"SELECT cnt.serial_cnt,dea.name_dea
				 FROM counter cnt
                 JOIN dealer dea ON dea.serial_dea=cnt.serial_dea AND phone_sales_dea='NO'
				 WHERE cnt.serial_usr='".$serial_usr."'
				 AND cnt.status_cnt='ACTIVE'";
		if($serial_cnt!=NULL){
			$sql.=" AND serial_cnt <> ".$serial_cnt;
		}
		
		$result = $this->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	        /**
	@Name: getCountersByBranch
	@Description: Returns all counters for the selected branch
	@Params: $serial_dea = Serial of the selected branch
			 * $all = true to get all, false get only active ones
	@Returns: An array with all counters if exists / FALSE if not.
	**/
	function getCountersByBranch($serial_dea,$all=false){
		$sql = 	"SELECT cnt.serial_cnt,dea.name_dea,cnt.serial_usr,CONCAT(u.first_name_usr,' ',u.last_name_usr) as cnt_name,
						u.birthdate_usr, u.username_usr, u.phone_usr, u.email_usr, u.sell_free_usr, cnt.status_cnt
				 FROM counter cnt
				 JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
				 JOIN user u ON u.serial_usr=cnt.serial_usr
				 WHERE cnt.serial_dea=".$serial_dea."
				 AND u.serial_usr <> 1";
		
		if($all==false)
				 $sql.=" AND cnt.status_cnt='ACTIVE'";
		$result = $this->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/*
	 * @Name: getCountersByCityWithBonus
	 * @Description: Returns the e-mail and the name of the counter.
	 * @Params: $serial_dea: serial of the branch
	 * @Returns: An array with name and e-mail.
	 */
	public function getCountersByCityWithBonus($serial_dea){
		$sql = 	"SELECT DISTINCT cnt.serial_cnt,usr.first_name_usr,usr.last_name_usr
				   FROM applied_bonus apb
				   JOIN sales s ON s.serial_sal=apb.serial_sal
				   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
				   JOIN user usr ON usr.serial_usr=cnt.serial_usr
				   WHERE cnt.serial_dea=$serial_dea
				   AND apb.bonus_to_apb='COUNTER'
				   AND apb.status_apb='ACTIVE'
				   AND usr.serial_usr <> 1";
		
		$result = $this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * @Name: getCountersMailInfo
	 * @Description: Returns the e-mail and the name of the counter.
	 * @Params: Serial_cnt
	 * @Returns: An array with name and e-mail.
	 */
	public static function getCountersMailInfo($db, $serial_cnt){
		$sql = 	"SELECT u.email_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS name
				 FROM user u
                 JOIN counter c ON c.serial_usr=u.serial_usr AND c.serial_cnt='".$serial_cnt."'";

		//die($sql);
		$result = $db -> getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

        /**
	@Name: getStatusTypes
	@Description: Retrieves all the status listed in DB
	@Params: N/A
	@Returns: Status array
	**/
        function getStatusTypes(){
		$sql = "SHOW COLUMNS FROM counter LIKE 'status_cnt'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	 /***********************************************
	* function getCounterForCorpStock
	@Name: getCounterForCorpStock
	@Description: Gets a random active counter from the manager with serial_man=NULL
	@Params:
         *     None
	@Returns: serial_cnt
	  *		  false if no results
	***********************************************/
	function getCounterForCorpStock(){
		$sql="SELECT serial_cnt
			FROM counter c
			JOIN user u ON c.serial_usr=u.serial_usr
			JOIN manager_by_country mbc ON mbc.serial_mbc=u.serial_mbc
			JOIN manager man ON man.serial_man = mbc.serial_man AND man_serial_man IS NULL
			WHERE c.status_cnt = 'ACTIVE'
			LIMIT 1";
		  //echo $sql;
        $result=$this->db->getRow($sql);
        if($result)
            return $result['serial_cnt'];
        else
            return false;
	}

	/*
	 * @function: getCountersDealer
	 * @Description: Returns the information of the branch and dealer for a specific counter.
	 * @params: $serial_cnt (Counter's ID)
	 * @returns: An array of data if OK / FALSE otherwise.
	 */
	public static function getCountersDealer($db, $serial_cnt){
		$sql="SELECT d.name_dea, de.name_dea, d.serial_dea
			FROM counter c
			JOIN dealer d ON d.serial_dea=c.serial_dea
			JOIN dealer de ON de.serial_dea=d.dea_serial_dea
			WHERE c.serial_cnt = ".$serial_cnt;

        $result=$db->Execute($sql);

        if($result){
			$data=array();
			$data['dealer_name']=$result->fields['1'];
			$data['branch_name']=$result->fields['0'];
			$data['serial_dea']=$result->fields['2'];
			
            return $data;
		}else{
            return false;
		}
	}
	
	/*
	 * @function: getCountersDealer
	 * @Description: Returns the information of the branch and dealer for a specific counter.
	 * @params: $serial_cnt (Counter's ID)
	 * @returns: An array of data if OK / FALSE otherwise.
	 */
	public static function getCountersTopDealer($db, $serial_cnt){
		$sql="SELECT  de.serial_dea
			FROM counter c
			JOIN dealer d ON d.serial_dea=c.serial_dea
			JOIN dealer de ON de.serial_dea=d.dea_serial_dea
			WHERE c.serial_cnt = ".$serial_cnt;
		$result=$db->Execute($sql);
        if($result){
			return $result->fields['0'];
		}
		return false;
	}

	public static function getCountryPhoneSalesCounter($db, $serial_cou, $serial_usr = NULL){
		$sql = 	"SELECT co.serial_cnt
                 FROM counter co
                 JOIN dealer dea2 ON co.serial_dea = dea2.serial_dea
                 JOIN dealer dea1 ON dea2.dea_serial_dea = dea1.serial_dea
                 JOIN sector sec ON dea1.serial_sec = sec.serial_sec
                 JOIN city cit ON sec.serial_cit = cit.serial_cit
                 WHERE dea2.phone_sales_dea = 'YES'
                 AND dea1.phone_sales_dea = 'YES'
                 AND cit.serial_cou = $serial_cou ";
		
		if($serial_usr){ //SINGLE RESULTS
			$sql .= "AND co.serial_usr = $serial_usr";
			$result = $db -> Execute($sql);
			if($result){
				return $result -> fields[0];
			}
		}else{ //MULTIPLE RESULTS, RETURN FIRST
			$result = $db -> getAll($sql);
			if($result){
					return $result[0]['serial_cnt'];
			}
		}
		return false;
	}
	
	public static function userIsPhoneSalesCounter($db, $serial_usr){
		$sql = 	"SELECT co.serial_cnt
                 FROM counter co
                 JOIN dealer dea2 ON co.serial_dea = dea2.serial_dea
                 JOIN dealer dea1 ON dea2.dea_serial_dea = dea1.serial_dea
                 WHERE dea2.phone_sales_dea = 'YES'
                 AND dea1.phone_sales_dea = 'YES'
                 AND co.serial_usr = $serial_usr";
		$result = $db -> getAll($sql);
		if($result){
			return true;
		}
		return false;
	}

	/**
	@Name: userExistsInPhoneSalesMainCounter
	@Description: Retrieves the information of a specific branch's list
	@Params: Serial number of a dealer
	@Returns: Dealers array
	**/
	public static function userExistsInPhoneSalesMainCounter($db,$serial_usr) {
		$sql = "SELECT cou.serial_usr
				FROM counter cou
				JOIN dealer dea ON dea.serial_dea = cou.serial_dea
				WHERE dea.serial_mbc = '1'
				AND cou.serial_usr = $serial_usr";
		$result = $db -> Execute($sql);
		if($result->fields[0]){
        	return $result->fields[0];
		}
		return false;
	}

	/**
	@Name: changePhoneCounterStatus
	@Description:
	@Params:
	@Returns:
	**/
	public static function changePhoneCounterStatus($db, $serial_usr, $status_cnt) {
		$sql="UPDATE counter
		SET status_cnt='".$status_cnt."'
		WHERE serial_usr='".$serial_usr."'";
		$result = $db->Execute($sql);
		if ($result==true)
			return true;
		else
			return false;
	}

	/*
	 * @name: getCountersWithBonus
	 * @param: $db - DB connection
	 * @return: Array with counters
	 */
	public static function getCountersWithBonus($db, $serial_dea, $bonus_to = NULL, $at_least_one_paid = NULL, $authorized = NULL){
		if($at_least_one_paid) $one_paid = " JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb";
		
		$sql = "SELECT DISTINCT cnt.serial_cnt, CONCAT(u.first_name_usr,' ',u.last_name_usr) as 'cnt_name'
				FROM counter cnt
				JOIN user u ON u.serial_usr=cnt.serial_usr
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal
				$one_paid
				WHERE cnt.serial_dea=".$serial_dea;

		if($bonus_to) $sql .= " AND apb.bonus_to_apb = '$bonus_to'";
		if($authorized) $sql.=" AND apb.ondate_apb = 'NO'
								AND apb.authorized_apb = 'YES'";

		$result=$db->getAll($sql);

		if($result){
				return $result;
		}else{
				return NULL;
		}
	}

	public static function isCounterByDealer($db, $serial_usr, $serial_dea){
		$sql = "SELECT cnt.serial_cnt
				FROM counter cnt
				WHERE cnt.serial_usr = $serial_usr
				AND cnt.serial_dea = $serial_dea";
		$result=$db->getOne($sql);

		if($result){
				return $result;
		}else{
				return NULL;
		}
	}
	
	public static function getTodaysBirthayCounters($db){
		$sql = "SELECT DISTINCT u.serial_usr, u.first_name_usr, u.last_name_usr, u.email_usr
				FROM counter c
				JOIN user u ON u.serial_usr = c.serial_usr AND u.status_usr = 'ACTIVE'
				WHERE u.serial_usr <> 1 AND
				DATE_FORMAT(birthdate_usr,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
    public static function getUserByCounter($db, $serial_cnt){
		$sql = "SELECT cnt.serial_usr
				FROM counter cnt
				WHERE cnt.serial_cnt = $serial_cnt";
		$result=$db->getOne($sql);
		if($result){
				return $result;
		}else{
				return NULL;
		}
	}
    
	///GETTERS
	function getSerial_cnt(){
		return $this->serial_cnt;
	}
	function getSerial_dea(){
		return $this->serial_dea;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getStatus_cnt(){
		return $this->status_cnt;
	}
        function getAux_cnt(){
		return $this->aux_cnt;
	}


	///SETTERS	
	function setSerial_cnt($serial_cnt){
		$this->serial_cnt = $serial_cnt;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea = $serial_dea;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setStatus_cnt($status_cnt){
		$this->status_cnt = $status_cnt;
	}
}
?>