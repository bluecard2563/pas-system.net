<?php
/*
File: Product.class
Author:Miguel Ponce
Creation Date:24/12/2009
Last modified:02/08/2010
Modified By: David Bergmann
*/

class ContractsByCustomer {				
	var $db;
	var $serial_cbc;
	var $serial_pro;
	var $serial_cus;
	var $contract_url_cbc;
	var $status_cbc;
	
	function __construct($db,	$serial_cbc = NULL){
		$this -> db = $db;
		$this -> serial_cbc = $serial_cbc;
	}

	function getData(){
		if($this -> serial_cbc != NULL){
			$sql = 	"SELECT serial_cbc,
							serial_pro,
							serial_cus,
							contract_url_cbc,
							status_cbc
					FROM contracts_by_customer
					WHERE serial_cbc = '{$this -> serial_cbc}'";
			$result = $this -> db -> Execute($sql);

			if($result -> fields[0]){
				$this -> serial_cbc = $result -> fields[0];
				$this -> serial_pro = $result -> fields[1];
				$this -> serial_cus = $result -> fields[2];
				$this -> contract_url_cbc = $result -> fields[3];
				$this -> status_cbc = $result -> fields[4];
				return true;
			}	
		}
		return false;		
	}
	
	/* function insert() 						*/
	/* insert a new contracts_by_customer type into database	*/
	
	function insert(){
		$sql="INSERT INTO contracts_by_customer (
					serial_cbc,
					serial_pro,
					serial_cus,
					contract_url_cbc,
					status_cbc
					)
			  VALUES(NULL,
					'{$this -> serial_pro}',
					'{$this -> serial_cus}',
					'{$this -> contract_url_cbc}',
					'{$this -> status_cbc}'
					);";
		$result = $this -> db -> Execute($sql);
		
		if($result){
            return $this -> db -> insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	function update(){
		if($this -> serial_cbc != NULL){
			$sql=	"UPDATE contracts_by_customer
					 SET serial_pro = '{$this -> serial_pro}',
						 serial_cus = '{$this -> serial_cus}',
						 contract_url_cbc = '{$this -> contract_url_cbc}',
						 status_cbc = '{$this -> status_cbc}'
						WHERE serial_cbc = '{$this -> serial_cbc}'";

			//die($sql);
			$result = $this -> db -> Execute($sql);

			if ($result == true){
				return true;
			}
		}
		return false;		
	}

	public static function verify_contract($db, $serial_sal){
		$sql = "SELECT cbc.contract_url_cbc
				FROM contracts_by_customer cbc
				JOIN product_by_country pxc ON pxc.serial_pro = cbc.serial_pro AND cbc.status_cbc = 'ACTIVE'
				JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
				JOIN sales s ON s.serial_pbd = pbd.serial_pbd 
						AND s.serial_cus = cbc.serial_cus
						AND s.serial_sal = $serial_sal";

		$result = $db -> getOne($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_cbc(){
		return $this -> serial_cbc;
	}
	function getSerial_pro(){
		return $this -> serial_pro;
	}
	function getSerial_cus(){
		return $this -> serial_cus;
	}
	function getContract_url_cbc(){
		return $this -> contract_url_cbc;
	}
	function getStatus_cbc(){
		return $this -> status_cbc;
	}
	
	///SETTERS	
	function setSerial_cbc($serial_cbc){
		$this -> serial_cbc = $serial_cbc;
	}
	function setSerial_pro($serial_pro){
		$this -> serial_pro = $serial_pro;
	}
	function setSerial_cus($serial_cus){
		$this -> serial_cus = $serial_cus;
	}
	function setContract_url_cbc($contract_url_cbc){
		$this -> contract_url_cbc = $contract_url_cbc;
	}
	function setStatus_cbc($status_cbc){
		$this -> status_cbc = $status_cbc;
	}	
}
?>