<?php
/*
File: TravelerLog.class
Author: Miguel Ponce
Creation Date: 22/03/2010
Last Modified: 14/04/2010
Modified By: Nicolas Flores
*/
class TravelerLog{
	var $db;
	var $serial_trl;
	var $serial_cus;
	var $serial_sal;
	var $serial_cit;
	var $card_number_trl;
	var $in_date_trl;
	var $start_trl;
	var $end_trl;
	var $days_trl;
	var $serial_cnt;
	var $status_trl;
	var $serial_usr;

	function __construct($db, $serial_trl=NULL,$serial_cus=NULL,$serial_sal=NULL,
						$serial_cit=NULL,$card_number_trl=NULL,$in_date_trl=NULL,$start_trl=NULL,$end_trl=NULL,$days_trl=NULL,
						$serial_cnt=NULL,$status_trl=NULL,$status_usr=NULL){
		$this -> db = $db;
		$this -> serial_trl=$serial_trl;
		$this -> serial_cus=$serial_cus;
		$this -> serial_sal=$serial_sal;
		$this -> serial_cit=$serial_cit;
		$this -> card_number_trl=$card_number_trl;
		$this -> in_date_trl=$in_date_trl;
		$this -> start_trl=$start_trl;
		$this -> end_trl=$end_trl;
		$this -> days_trl=$days_trl;
		$this -> serial_cnt=$serial_cnt;
		$this->status_trl=$status_trl;
		$this->serial_usr=$status_usr;
	}
	
	/***********************************************
        * funcion getData
        * sets data by serial
        ***********************************************/
	function getData(){
		if($this->serial_trl!=NULL){
			$sql = 	"SELECT
						serial_trl,
						serial_cus,
						serial_sal,
						serial_cit,
						card_number_trl,
						DATE_FORMAT(start_trl,'%d/%m/%Y'),
						DATE_FORMAT(end_trl,'%d/%m/%Y'),
						days_trl,
						serial_cnt,
						status_trl,
						serial_usr,
						DATE_FORMAT(in_date_trl,'%d/%m/%Y')
					FROM traveler_log
					WHERE serial_trl='".$this->serial_trl."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_trl =$result->fields[0];
				$this -> serial_cus =$result->fields[1];
				$this -> serial_sal =$result->fields[2];
				$this -> serial_cit =$result->fields[3];
				$this -> card_number_trl =$result->fields[4];
				$this -> start_trl =$result->fields[5];
				$this -> end_trl =$result->fields[6];
				$this -> days_trl =$result->fields[7];
				$this -> serial_cnt =$result->fields[8];
				$this->status_trl=$result->fields[9];
				$this->serial_usr=$result->fields[10];
				$this->in_date_trl=$result->fields[11];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}


    /***********************************************
    * funcion getLastCard_number_trl
    * returns the last inserted cadr number
    ***********************************************/
	function getLastCard_number_trl(){
		if($this->serial_sal!=NULL){
			$sql = 	"SELECT
						max(card_number_trl)
					FROM traveler_log
					WHERE serial_sal='".$this->serial_sal."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;
		
			return $result->fields[0];
			
		}else
			return false;
	}

    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert(){
        $sql = "
            INSERT INTO traveler_log (
				serial_trl,
				serial_cus,
				serial_sal,
				serial_cit,
				card_number_trl,
				in_date_trl,
				start_trl,
				end_trl,
				days_trl,
				serial_cnt,
				status_trl,
				serial_usr
			)
            VALUES(				
				NULL,
				'".$this->serial_cus."',
				'".$this->serial_sal."',";
				($this->serial_cit != NULL)?$sql.="'".$this->serial_cit."',":$sql.="NULL,";
		$sql .= ($this->card_number_trl==NULL)?'NULL,':"'{$this->card_number_trl}',";
		$sql .= "NOW(),";
		$sql .= "STR_TO_DATE('".$this->start_trl."','%d/%m/%Y'),
				STR_TO_DATE('".$this->end_trl."','%d/%m/%Y')";
				($this->days_trl)?$sql .= ",'{$this->days_trl}'":$sql .= ",ABS(DATEDIFF(STR_TO_DATE('".$this->end_trl."','%d/%m/%Y'),STR_TO_DATE('".$this->start_trl."','%d/%m/%Y')))";
		$sql .= ($this->serial_cnt==NULL)?',NULL':",'{$this->serial_cnt}'";
		$sql .= ",'ACTIVE',".$this->serial_usr.")";
		
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }


	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
	function update(){
		if($this->serial_cnt==''){
				$serial_cnt='NULL';
		}else{
				$serial_cnt=$this->serial_cnt;
		}
		if($this->card_number_trl==''){
			$card_number_trl='NULL';
		}else{
			$card_number_trl=$this->card_number_trl;
		}
		$sql="UPDATE traveler_log
		SET serial_cnt=".$serial_cnt.",
			serial_sal='".$this->serial_sal."',
			serial_usr='".$this->serial_usr."',
			serial_cus='".$this->serial_cus."',
			serial_cit='".$this->serial_cit."',
			card_number_trl=".$card_number_trl.",
			start_trl=STR_TO_DATE('".$this->start_trl."','%d/%m/%Y'),
			end_trl=STR_TO_DATE('".$this->end_trl."','%d/%m/%Y'),
			days_trl='".$this->days_trl."',
			status_trl='".$this->status_trl."'
		WHERE serial_trl='".$this->serial_trl."'";
		$result = $this->db->Execute($sql);
		//die($sql." ");
		if ($result==true)
			return true;
		else
			return false;
	}
	

	 /***********************************************
	* function getTravelLogBySale
	@Name: getTravelLogBySale
	@Description: get register travels by sale.
	@Params:
         *       attribute this->serial_sal
	     *		 $all: true to retrieve all the registers regardless of status
	@Returns: array with all travels linked to a sale
	  *		  false if no results
	***********************************************/
	function getTravelLogBySale($all=false){
		/*$fields="IF(
					(
					 (tl.card_number_trl IS NOT NULL) AND
					 (s.card_number_sal IS NOT NULL)
					),
					CONCAT(s.card_number_sal,'_',tl.card_number_trl),
					IF((s.card_number_sal IS NULL),
						tl.card_number_trl,s.card_number_sal)) as card_number,
						CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')
					) as customer_name,
				 cou.name_cou,
				 DATE_FORMAT(tl.start_trl,'%d/%m/%Y') as start_trl,
				 DATE_FORMAT(tl.end_trl,'%d/%m/%Y') as end_trl,
				 tl.days_trl,
				 tl.serial_trl,
				 tl.serial_usr,
				 CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'user_name'";*/

        $fields = "
                    IF((tl.card_number_trl IS NOT NULL), tl.card_number_trl,s.card_number_sal) as card_number,
                    CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'') ) as customer_name, 
                    cou.name_cou, 
                    DATE_FORMAT(tl.start_trl,'%d/%m/%Y') as start_trl, 
                    DATE_FORMAT(tl.end_trl,'%d/%m/%Y') as end_trl, 
                    tl.days_trl, 
                    tl.serial_trl, 
                    tl.serial_usr, 
                    CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'user_name'
                  ";
		if($all!=false){
			$fields.=',tl.status_trl';
		}
		 $sql="SELECT $fields
			 FROM sales s
			 JOIN traveler_log tl ON tl.serial_sal=s.serial_sal
			 JOIN user u ON u.serial_usr = tl.serial_usr
			 JOIN customer cus ON cus.serial_cus=tl.serial_cus
			 JOIN city ci ON ci.serial_cit = tl.serial_cit
			 JOIN country cou ON ci.serial_cou = cou.serial_cou
			 WHERE tl.serial_sal={$this->serial_sal}
			 AND tl.status_trl <> 'DELETED'";
			 if($all==false){
				" AND tl.status_trl='ACTIVE'";
			 }
			 $sql.=" ORDER BY card_number DESC";
        $result=$this->db->getAll($sql);
        if($result)
            return $result;
        else
            return false;
	}

	/***********************************************
	* function getActiveRegisterTravels
	@Name: getActiveRegisterTravels
	@Description: retrieve all travels register wich are active and havent started yet.
	@Params:
         *       attribute this->serial_sal
	@Returns: array with all travels linked to a sale
	  *		  false if no results
	***********************************************/
	function getActiveRegisterTravels(){
		 $sql="SELECT tl.serial_trl, IF(((tl.card_number_trl IS NOT NULL) AND (s.card_number_sal IS NOT NULL)),CONCAT(s.card_number_sal,'_',tl.card_number_trl),IF((s.card_number_sal IS NULL),tl.card_number_trl,s.card_number_sal)) as card_number,CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name, ci.name_cit,DATE_FORMAT(tl.start_trl,'%d/%m/%Y') as start_trl,DATE_FORMAT(tl.end_trl,'%d/%m/%Y') as end_trl,tl.days_trl, tl.serial_usr
			 FROM sales s
			 JOIN traveler_log tl ON tl.serial_sal=s.serial_sal
			 JOIN customer cus ON cus.serial_cus=tl.serial_cus
			 JOIN city ci ON ci.serial_cit = tl.serial_cit
			 WHERE tl.serial_sal={$this->serial_sal}
			 AND tl.status_trl='ACTIVE'
			 AND tl.start_trl>NOW()
			 ORDER BY end_trl DESC";
		  //echo $sql;
        $result=$this->db->getAll($sql);
        if($result)
            return $result;
        else
            return false;
	}
	/***********************************************
	* function getActiveRegisterTravels
	@Name: getActiveRegisterTravels
	@Description: retrieve all travels register wich are active and havent started yet.
	@Params:
         *       attribute this->serial_sal
	@Returns: array with all travels linked to a sale
	  *		  false if no results
	***********************************************/
	function getRegisterByCardNumber(){
		 $sql="SELECT tl.serial_trl, tl.card_number_trl as card_number,CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name, ci.name_cit,DATE_FORMAT(tl.start_trl,'%d/%m/%Y') as start_trl,DATE_FORMAT(tl.end_trl,'%d/%m/%Y') as end_trl,tl.days_trl, tl.serial_usr
			 FROM traveler_log tl 
			 JOIN customer cus ON cus.serial_cus=tl.serial_cus
			 JOIN city ci ON ci.serial_cit = tl.serial_cit
			 WHERE tl.card_number_trl={$this->card_number_trl}
			 AND tl.status_trl='ACTIVE'
			 AND tl.start_trl>NOW()";
		  //echo $sql;
        $result=$this->db->getAll($sql);
        if($result)
            return $result;
        else
            return false;
	}
	//GETTERS


        /***********************************************
        * funcion getDataByCardNumber
        * gets data by card number
        ***********************************************/
	function getDataByCardNumber($db, $card_number_trl){
            $sql = "SELECT 	serial_trl, 
            				serial_cus, 
            				serial_sal, 
            				card_number_trl, 
            				start_trl, 
            				end_trl,
            				serial_cit, 
            				status_trl, 
            				serial_usr
                    FROM traveler_log
                    WHERE card_number_trl = $card_number_trl";
            $result = $db -> Execute($sql);

            $travelerLog = new TravelerLog($db);
            if($result->fields[0]){
				$travelerLog -> setSerial_trl($result->fields[0]);
                $travelerLog -> setSerial_cus($result->fields[1]);
                $travelerLog -> setSerial_sal($result->fields[2]);
                $travelerLog -> setCard_number_trl($result->fields[3]);
                $travelerLog -> setStart_trl($result->fields[4]);
                $travelerLog -> setEnd_trl($result->fields[5]);
				$travelerLog -> setSerial_cit($result->fields[6]);
				$travelerLog -> setStatus_trl($result->fields[7]);
				$travelerLog -> setSerial_usr($result->fields[8]);
                return $travelerLog;
            }
            return false;
	}

	 /***********************************************
	* function existCrossingTravel
	@Name: existCrossingTravel
	@Description: Verify if the customer already has a travel registered between 2 dates.
	@Params:
         *       $serial_cus : identifier for customer
		 *		 $start_dt : lower limit of dates
	  *  *		 $end_dt : higher limit of dates
	     *       $exclude_travels: ids of traveler_logs to exclude, separated by commas
	@Returns: true/false
	***********************************************/
	function existCrossingTravel($serial_cus,$start_dt,$end_dt,$exclude_travels=''){
		$sql="SELECT 1
			FROM traveler_log tl
			WHERE tl.serial_cus='$serial_cus'
		    AND tl.status_trl = 'ACTIVE'
			AND(
				STR_TO_DATE('$start_dt','%d/%m/%Y') BETWEEN  SUBDATE(tl.start_trl,INTERVAL  59 MINUTE) AND tl.end_trl
				 OR STR_TO_DATE('$end_dt','%d/%m/%Y') BETWEEN  SUBDATE(tl.start_trl,INTERVAL  59 MINUTE) AND tl.end_trl
				 OR STR_TO_DATE('$start_dt','%d/%m/%Y') = tl.start_trl AND STR_TO_DATE('$end_dt','%d/%m/%Y') = tl.end_trl
				 OR tl.start_trl BETWEEN STR_TO_DATE('$start_dt','%d/%m/%Y') AND STR_TO_DATE('$end_dt','%d/%m/%Y')
				 OR tl.end_trl BETWEEN STR_TO_DATE('$start_dt','%d/%m/%Y') AND STR_TO_DATE('$end_dt','%d/%m/%Y')
			)";
		
		if($exclude_travels!=''){
			$sql.=" AND tl.serial_trl NOT IN($exclude_travels)";
		}
		//echo $sql;
		$result=$this->db->getOne($sql);
		if($result==1)
			return true;
		else
			return false;
	}
	
	public static function expireAllTravels($db, $serial_sal){
		$sql = "UPDATE traveler_log 
				SET status_trl='EXPIRED'
				WHERE serial_sal = $serial_sal";
		
		$result=$db->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'expireAllTravels FAILED - '.$serial_sal, $sql);
			return FALSE;
		}
	}
	
	public static function expireTravelerLogRegisters($db){
		$sql = "UPDATE traveler_log 
				SET status_trl='EXPIRED'
				WHERE end_trl < NOW()
				AND status_trl = 'ACTIVE'";
		
		$result=$db->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'expireTravelerLogRegisters FAILED - '.$serial_sal, $sql);
			return FALSE;
		}
	}
	
	public static function cardNumberExistInTravelerLog($db, $card_number){
		$sql = "SELECT serial_trl
				FROM traveler_log
				WHERE card_number_trl LIKE '$card_number'";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public static function getMaxCardNumberForSale($db, $serial_sal){
		$sql = "SELECT MAX(trl.serial_trl)
				FROM traveler_log trl
				JOIN sales s ON s.serial_sal = trl.serial_sal AND s.sal_serial_sal = $serial_sal";
		
		$result = $db -> getOne($sql);
		
		if($result){
			$trl = new TravelerLog($db, $result);
			$trl -> getData();
			$card_number = explode('_', $trl -> getCard_number_trl());
			
			return $card_number['1'];
		}else{
			return 1;
		}
	}
	
	public static function voidAllTravelsRegistered($db, $serial_sal){
		$sql = "UPDATE traveler_log 
				SET status_trl = 'DELETED'
				WHERE serial_sal = $serial_sal
				AND status_trl = 'ACTIVE'";
		
		$result=$db->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'voidAllTravelsRegistered FAILED - '.$serial_sal, $sql);
			return FALSE;
		}
	}
	
	public static function getCorporativeAssistanceInfo($db, $serial_sal){
		$sql = "SELECT cus.serial_cus, CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
						IFNULL(s.card_number_sal, tl.card_number_trl) AS 'card_number', 
						DATE_FORMAT(tl.in_date_trl, '%d/%m/%Y') AS 'in_date_trl',
						DATE_FORMAT(tl.start_trl, '%d/%m/%Y') AS 'start_trl',
						DATE_FORMAT(tl.end_trl, '%d/%m/%Y') AS 'end_trl',
						DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') AS 'creation_date_fle',							
						s.total_sal, f.serial_fle, s.serial_sal, f.status_fle, f.diagnosis_fle,
						spf.estimated_amount_spf, bd.amount_authorized_bbd, bd.amount_presented_bbd
				FROM sales s
				JOIN traveler_log tl ON tl.serial_sal = s.serial_sal AND s.serial_sal = $serial_sal
				JOIN file f ON f.serial_trl = tl.serial_trl
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle
				JOIN benefits_by_document bd ON spf.serial_spf = bd.serial_spf";
		
		//Debug::print_r($sql); die;
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	///GETTERS
	function getSerial_trl(){
		return $this->serial_trl;
	}
	function getSerial_cus(){
		return $this->serial_cus;
	}
	function getSerial_sal(){
		return $this->serial_sal;
	}
	function getCard_number_trl(){
		return $this->card_number_trl;
	}
	function getInDate_trl(){
		return $this->in_date_trl;
	}
	function getStart_trl(){
		return $this->start_trl;
	}
	function getEnd_trl(){
		return $this->end_trl;
	}
	function getDays_trl(){
		return $this->days_trl;
	}
	function getSerial_cnt(){
		return $this->serial_cnt;
	}
	function getSerial_cit(){
		return $this->serial_cit;
	}
	function getStatus_trl(){
		return $this->status_trl;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}

	///SETTERS	
	function setSerial_trl($serial_trl){
		$this->serial_trl = $serial_trl;
	}
	function setSerial_cus($serial_cus){
		$this->serial_cus = $serial_cus;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal = $serial_sal;
	}
	function setCard_number_trl($card_number_trl){
		$this->card_number_trl = $card_number_trl;
	}
	function setInDate_trl($in_date_trl){
		$this->in_date_trl = $in_date_trl;
	}
	function setStart_trl($start_trl){
		$this->start_trl = $start_trl;
	}
	function setEnd_trl($end_trl){
		$this->end_trl = $end_trl;
	}
	function setDays_trl($days_trl){
		$this->days_trl = $days_trl;
	}
	function setSerial_cnt($serial_cnt){
		$this->serial_cnt = $serial_cnt;
	}
	function setSerial_cit($serial_cit){
		$this->serial_cit = $serial_cit;
	}
	function setStatus_trl($status_trl){
		$this->status_trl=$status_trl;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr=$serial_usr;
	}
}
?>