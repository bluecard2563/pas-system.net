<?php
/*
File: Blog.class
Author: David Bergmann
Creation Date: 09/04/2010
Last Modified: 11/06/2010
Modified By: David Bergmann
*/

class Blog {
    var $db;
    var $serial_blg;
    var $serial_usr;
    var $serial_fle;
    var $type_blg;
    var $entry_blg;
    var $date_blg;
    var $status_blg;

    function __construct($db, $serial_blg = NULL, $serial_usr=NULL, $serial_fle = NULL,
                         $type_blg = NULL, $entry_blg = NULL, $date_blg = NULL, $status_blg = NULL){
        $this -> db = $db;
        $this -> serial_blg = $serial_blg;
        $this -> serial_usr = $serial_usr;
        $this -> serial_fle = $serial_fle;
        $this -> type_blg = $type_blg;
        $this -> entry_blg = $entry_blg;
        $this -> date_blg = $date_blg;
        $this -> status_blg = $status_blg;
    }

    /***********************************************
    * function getData
    * gets data by serial_blg
    ***********************************************/
    function getData(){
        if($this->serial_blg!=NULL){
            $sql = 	"SELECT serial_blg, serial_usr, serial_fle, type_blg, entry_blg, date_blg, status_blg
                     FROM blog
                     WHERE serial_blg='".$this->serial_blg."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_blg=$result->fields[0];
                $this->serial_usr=$result->fields[1];
                $this->serial_fle=$result->fields[2];
                $this->type_blg=$result->fields[3];
                $this->entry_blg=$result->fields[4];
                $this->date_blg=$result->fields[5];
                $this->status_blg=$result->fields[6];

                return true;
            }
            else
                return false;
        }else
            return false;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert(){
        $sql=   "INSERT INTO blog (
                                serial_blg,
                                serial_usr,
                                serial_fle,
                                type_blg,
                                entry_blg,
                                date_blg,
                                status_blg
                                )
                  VALUES(NULL,
                         '".$this->serial_usr."',
                         '".$this->serial_fle."',
                         '".$this->type_blg."',
                         '".mysql_real_escape_string($this->entry_blg)."',
                         STR_TO_DATE('".$this->date_blg."','%Y-%m-%d %T'),
                         'OPEN'
                         );";
        //die($sql);
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update(){
        $sql= "UPDATE blog
               SET entry_blg='".mysql_real_escape_string($this->entry_blg)."'
               WHERE serial_fle = '".$this->serial_fle."'
               AND type_blg = '".$this->type_blg."'";

        $result = $this->db->Execute($sql);
        //die($sql);
        if ($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
    * function getDataByFile
    * gets data by serial_fle
    ***********************************************/
    function getDataByFile($type_blg){
        if($this->serial_fle!=NULL){
            $sql = 	"SELECT serial_blg, serial_usr, serial_fle, type_blg, entry_blg, date_blg, status_blg
                    FROM blog
                    WHERE serial_fle='".$this->serial_fle."'
                    AND type_blg='".$type_blg."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_blg=$result->fields[0];
                $this->serial_usr=$result->fields[1];
                $this->serial_fle=$result->fields[2];
                $this->type_blg=$result->fields[3];
                $this->entry_blg=$result->fields[4];
                $this->date_blg=$result->fields[5];
                $this->status_blg=$result->fields[6];

                return true;
            }
            else
                return false;
        }else
            return false;
    }
	
	public static function getBlogForFile($db, $serial_fle, $type_blg){
		$sql = 	"SELECT serial_blg, serial_usr, serial_fle, type_blg, entry_blg, date_blg, status_blg
				FROM blog
				WHERE serial_fle='".$serial_fle."'
				AND type_blg='".$type_blg."'";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

    ///GETTERS
    function getSerial_blg(){
        return $this->serial_blg;
    }
    function getSerial_usr(){
        return $this->serial_usr;
    }
    function getSerial_fle(){
        return $this->serial_fle;
    }
    function getType_blg(){
        return $this->type_blg;
    }
    function getEntry_blg(){
        return $this->entry_blg;
    }
    function getDate_blg(){
        return $this->date_blg;
    }
    function getStatus_blg(){
        return $this->status_blg;
    }

    ///SETTERS
    function setSerial_blg($serial_blg){
        $this->serial_blg = $serial_blg;
    }
    function setSerial_usr($serial_usr){
        $this->serial_usr = $serial_usr;
    }
    function setSerial_fle($serial_fle){
        $this->serial_fle = $serial_fle;
    }
    function setType_blg($type_blg){
        $this->type_blg = $type_blg;
    }
    function setEntry_blg($entry_blg){
        $this->entry_blg = utf8_decode($entry_blg);
    }
    function setDate_blg($date_blg){
        $this->date_blg = $date_blg;
    }
    function setStatus_blg($status_blg){
        $this->status_blg = $status_blg;
    }
}
?>
