<?php
/*
File: ProductByCountry.class.php
Author: Edwin Salvador.
Creation Date: 18/02/2010
Last Modified: 
Modified By: 
*/

class ProductByCountry{
    var $db;
    var $serial_pxc;
    var $serial_pro;
    var $serial_cou;
    var $percentage_spouse_pxc;
    var $percentage_extras_pxc;
    var $percentage_children_pxc;
    var $aditional_day_pxc;
    var $status_pxc;
	

    function __construct($db, $serial_pxc=NULL, $serial_pro=NULL, $serial_cou=NULL, $price_pxc=NULL, $cost_percentage_pxc=NULL, $percentage_spouse_pxc=NULL, $percentage_extras_pxc=NULL, $percentage_children_pxc=NULL, $status_pxc = NULL, $aditional_day_pxc=NULL ){
        $this -> db = $db;
        $this -> serial_pxc = $serial_pxc;
        $this -> serial_pro = $serial_pro;
        $this -> serial_cou = $serial_cou;
        $this -> percentage_spouse_pxc = $percentage_spouse_pxc;
        $this -> percentage_extras_pxc = $percentage_extras_pxc;
        $this -> percentage_children_pxc = $percentage_children_pxc;
        $this -> aditional_day_pxc = $aditional_day_pxc;
        $this -> status_pxc = $status_pxc;

    }
	
    /***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
    function getData(){
        //echo ($this->serial_pxc);
        if($this->serial_pxc!=NULL){
            $sql = "SELECT serial_pxc,
                            serial_pro,
                            serial_cou,
                            percentage_spouse_pxc,
                            percentage_extras_pxc,
                            percentage_children_pxc,
                            aditional_day_pxc,
                            status_pxc
                    FROM product_by_country
                    WHERE serial_pxc='".$this->serial_pxc."'";
            //echo $sql;
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_pxc =$result->fields[0];
                $this -> serial_pro = $result->fields[1];
                $this -> serial_cou =$result->fields[2];
                $this -> percentage_spouse_pxc = $result->fields[3];
                $this -> percentage_extras_pxc =$result->fields[4];
                $this -> percentage_children_pxc = $result->fields[5];
                $this -> aditional_day_pxc = $result->fields[6];
                $this -> status_pxc = $result->fields[7];
                return true;
            }
        }
        return false;
    }

    /***********************************************
    * function getDataByCountryAndProduct
    * sets and returns an array with product's data 
    ***********************************************/
    function getDataByCountryAndProduct(){
        if($this->serial_pro!=NULL && $this->serial_cou!=NULL){
            $sql = "SELECT serial_pxc,
                            serial_pro,
                            serial_cou,
                            percentage_spouse_pxc,
                            percentage_extras_pxc,
                            percentage_children_pxc,
                            aditional_day_pxc,
                            status_pxc
                    FROM product_by_country
                    WHERE   serial_cou='".$this->serial_cou."' AND
                            serial_pro='".$this->serial_pro."'";
            $result = $this->db->Execute($sql);

            if ($result === false) return false;

            if($result->fields[0]){

                $this -> serial_pxc =$result->fields[0];
                $this -> serial_pro = $result->fields[1];
                $this -> serial_cou =$result->fields[2];
                $this -> percentage_spouse_pxc = $result->fields[3];
                $this -> percentage_extras_pxc =$result->fields[4];
                $this -> percentage_children_pxc = $result->fields[5];
                $this -> aditional_day_pxc = $result->fields[6];
                $this -> status_pxc = $result->fields[7];
                return $result->fields;
                
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }
	
    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO product_by_country (
				serial_pxc,
				serial_pro,
				serial_cou,
				percentage_spouse_pxc,
				percentage_extras_pxc,
				percentage_children_pxc,
                                aditional_day_pxc)
            VALUES (
                    NULL,
                    '".$this->serial_pro."',
                    '".$this->serial_cou."',";
                    if($this-> percentage_spouse_pxc){
                        $sql .= "'".$this->percentage_spouse_pxc."',";
                    }else{
                        $sql .= "0,";
                    }
                    if($this->percentage_extras_pxc){
                        $sql .= "'".$this->percentage_extras_pxc."',";
                    }else{
                        $sql .= "0,";
                    }
                    if($this->percentage_children_pxc){
                        $sql .= "'".$this->percentage_children_pxc."',";
                    }else{
                        $sql .= "0,";
                    }
                    if($this->aditional_day_pxc){
                        $sql .= "'".$this->aditional_day_pxc."'";
                    }else{
                        $sql .= "0";
                    }
                    $sql.=")";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        if($this->serial_pxc != NULL){
            $sql = "
                    UPDATE product_by_country SET
                            serial_cou = '".$this->serial_cou."',
                            percentage_spouse_pxc =";
                    if($this->percentage_spouse_pxc){
                        $sql .= "'".$this->percentage_spouse_pxc."',";
                    }else{
                        $sql .= "0,";
                    }

                    $sql .= "percentage_extras_pxc =";
                    if($this->percentage_extras_pxc){
                        $sql .= "'".$this->percentage_extras_pxc."',";
                    }else{
                        $sql .= "0,";
                    }

                    $sql .= "percentage_children_pxc =";
                    if($this->percentage_children_pxc){
                        $sql .= "'".$this->percentage_children_pxc."',";
                    }else{
                        $sql .= "0,";
                    }

                    $sql .= "aditional_day_pxc =";
                    if($this->aditional_day_pxc){
                        $sql .= "'".$this->aditional_day_pxc."',";
                    }else{
                        $sql .= "0,";
                    }

                    $sql .= "status_pxc ='".$this->status_pxc."'
                    WHERE serial_pxc = '".$this->serial_pxc."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    public static function updateAditionalDays($db,$serial_pro,$aditional_days) {
		$sql = "UPDATE product_by_country SET
					   aditional_day_pxc = '$aditional_days'
				WHERE serial_pro = '$serial_pro'
				AND serial_cou=1";

		$result = $db->Execute($sql);
		if ($result === false)return false;

		if($result==true){
			return true;
		}else{
			return false;
		}
    }

    /***********************************************
    * funcion detele
    * deletes a register in DB
    ***********************************************/
    function delete() {
        if($this->serial_pxc != NULL){
            $sql = "
                    DELETE
                    FROM product_by_country
                    WHERE serial_pxc = '".$this->serial_pxc."'";
//echo $sql;die;
            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getAssignedCountries
    * gets all the Countries assigened to a product
    ***********************************************/
    function getAssignedCountries($serial_pro){
        $sql = 	"SELECT pbc.serial_pxc,
                        pbc.serial_pro,
                        c.serial_zon,
                        pbc.serial_cou,
                        pbc.percentage_spouse_pxc,
                        pbc.percentage_extras_pxc,
                        pbc.percentage_children_pxc,
                        pbc.aditional_day_pxc,
                        pbc.status_pxc,
                        GROUP_CONCAT(  DISTINCT tbp.serial_tax ORDER BY tbp.serial_tax ASC SEPARATOR ',' ) assigned_taxes,
                        GROUP_CONCAT(  DISTINCT pbd.serial_pbd ORDER BY pbd.serial_pbd ASC SEPARATOR ',' ) assigned_dealers
                FROM product_by_country pbc
                LEFT JOIN taxes_by_product tbp ON pbc.serial_pxc = tbp.serial_pxc
                LEFT JOIN product_by_dealer pbd ON pbd.serial_pxc = pbc.serial_pxc
                AND pbd.status_pbd = 'ACTIVE'
                JOIN country c ON c.serial_cou = pbc.serial_cou
                WHERE serial_pro = '".$serial_pro."'
                AND pbc.serial_cou <> '1'
                GROUP BY pbc.serial_cou
                ORDER BY c.serial_zon";

        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arreglo;
    }

    /*
     * @Name: getProductByCountrySerials
     * @Description: Returns an array of product by country serials for a specific list of products and
     *               countries. This serials are going to be used in the Promotions module.
     * @Params: serial_cou: Specific Country
     *          productList: List of products that are sold in that country
     * @Returns: Country array
     */
    function getProductByCountrySerials($serial_cou, $productList){
        $sql = 	"SELECT pbc.serial_pxc
                FROM product_by_country pbc
                WHERE pbc.serial_cou = '".$serial_cou."'
                AND pbc.serial_pro IN (".$productList.")";
                //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    function productByCountryExists(){
        if($this->serial_pro != NULL && $this->serial_cou != NULL){
            $sql = "SELECT pbc.serial_pxc
                    FROM product_by_country pbc
                    WHERE pbc.serial_pro = '".$this->serial_pro."'
                    AND pbc.serial_cou = '".$this->serial_cou."'";
            //echo $sql;
            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }

     function getProductByCountry($serial_pro){
        $sql = "SELECT pxc.serial_pxc
                FROM product_by_country pxc
                WHERE pxc.serial_pro = $serial_pro
                AND serial_cou = 1";

        $result = $this->db->Execute($sql);

        if ($result === false)return false;

        if($result->fields[0]){
            return $result->fields[0];
        }else{
            return 0;
        }
    }

    /*
     * @Name: getProductByCountrySerials
     * @Description: Returns an array of product by country serials for a specific list of products and
     *               countries. This serials are going to be used in the Promotions module.
     * @Params: serial_cou: Specific Country
     *          productList: List of products that are sold in that country
     * @Returns: Country array
     */

	public static function getPercentages_by_Product($db, $serial_pro, $serial_cou){
		$sql="	SELECT pxc.serial_pxc, pxc.serial_pro, pxc.percentage_spouse_pxc, pxc.percentage_extras_pxc, pxc.percentage_children_pxc
				FROM product_by_country pxc
                WHERE pxc.serial_cou = ".$serial_cou."
                AND pxc.serial_pro = ".$serial_pro;
			$result = $db -> getRow($sql);

            if($result){
                return $result;
            }
            return false;
	}

    function getProductsByCountry($serial_cou, $serial_lang, $status_pxc=NULL, $masive = NULL){
        $sql = 	"SELECT pbc.serial_pxc, p.serial_pro, pbl.name_pbl, pbl.description_pbl, p.max_extras_pro, p.flights_pro,
                        p.children_pro, p.adults_pro, p.relative_pro, p.spouse_pro, p.emission_country_pro,
                        p.residence_country_pro, p.masive_pro, p.schengen_pro, p.senior_pro, p.has_services_pro
                 FROM product_by_country pbc
                 JOIN product p ON p.serial_pro = pbc.serial_pro
                 JOIN product_by_language pbl ON pbl.serial_lang = '".$serial_lang."' AND pbl.serial_pro = p.serial_pro
                 WHERE pbc.serial_cou = '".$serial_cou."'";

		if($masive){
			$sql.=" AND masive_pro ='".$masive."'";
		}

		if($status_pxc!=NULL) {
            $sql.=" AND status_pro ='".$status_pxc."'";
        }
		//die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    /**
    @Name: getStatusList
    @Description: Gets all status in DB.
    @Params: N/A
    @Returns: Status array
    **/
    function getStatusList(){
            $sql = "SHOW COLUMNS FROM product_by_country LIKE 'status_pxc'";
            $result = $this->db -> Execute($sql);

            $type=$result->fields[1];
            $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
            return $type;
    }

	/**
	@Name: getProductsPricesByCountry
	@Description: Retrieves the information of a specific product list
	@Params:
	@Returns: Product array
	**/
	public static function getProductsPricesByCountry($db, $serial_cou, $lang_code, $serial_pro = NULL){		
		$sql =  "SELECT DISTINCT (pro.serial_pro), pbl.name_pbl, 
						IF(pxc.percentage_spouse_pxc = 0, 'N/A', pxc.percentage_spouse_pxc) AS percentage_spouse_pxc,
						IF(pxc.percentage_extras_pxc = 0, 'N/A', pxc.percentage_extras_pxc) AS percentage_extras_pxc,
						IF(pxc.percentage_children_pxc = 0, 'N/A', pxc.percentage_children_pxc) AS percentage_children_pxc,
						pxc.aditional_day_pxc
				 FROM product pro
			     JOIN product_by_country pxc ON pxc.serial_pro = pro.serial_pro
				 JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
				 WHERE pxc.serial_cou = $serial_cou
				 AND pbl.serial_lang = " . Language::getSerialLangByCode($db, $lang_code);
		
		if($serial_pro):
			$sql .= " AND pro.serial_pro = $serial_pro";
		endif;
		
		$sql .= " ORDER BY pbl.name_pbl";
			
		//Debug::print_r($sql); die;
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	@Name: getProductsByCountryWithAsignedFlag
	@Description: Retrieves a list with the available products in a country, and a flag that indicates
	 			  if it is already been asigned.
	@Params: $db - DB connection
	         $serial_cou - Country ID
			 $serial_dea - Dealer ID
	@Returns: Product array
	**/
	public static function getProductsByCountryWithAsignedFlag($db, $serial_cou, $serial_dea){
		$sql =  "SELECT distinct(pro.serial_pro),
					   pbl.name_pbl, pxc.serial_pxc,
					   IF(pbd.serial_pbd IS NULL, 'NO','YES') AS 'asigned'
				FROM product pro
				JOIN product_by_country pxc ON pxc.serial_pro = pro.serial_pro 
					AND pxc.serial_cou = ".$serial_cou."
					AND pxc.status_pxc='ACTIVE'
				JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang=".$_SESSION['serial_lang']."
				LEFT JOIN product_by_dealer pbd ON pbd.serial_pxc=pxc.serial_pxc 
					AND pbd.serial_dea = ".$serial_dea."
					AND pbd.status_pbd='ACTIVE'
				WHERE pro.status_pro='ACTIVE'
				ORDER BY pbl.name_pbl";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	@Name: getProductsByCountryAvailableForMassiveAsign
	@Description: Retrieves a list with the available products in a country, that are not yet assigned.
	@Params: $db - DB connection
	         $serial_cou - Country ID
			 $serials - Dealers ID
	@Returns: Product array
	**/
	public static function getProductsByCountryAvailableForMassiveAsign($db, $serial_cou, $serials){
		$sql = "SELECT distinct(pro.serial_pro), 
					   pbl.name_pbl, pxc.serial_pxc
				FROM product pro 
				JOIN product_by_country pxc ON pxc.serial_pro = pro.serial_pro 
					AND pxc.serial_cou = ".$serial_cou."
					AND pxc.status_pxc='ACTIVE'
				JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang=".$_SESSION['serial_lang']."
				JOIN country cou ON cou.serial_cou = pxc.serial_cou
				WHERE pxc.serial_pxc NOT IN (SELECT serial_pxc
											 FROM product_by_dealer pbd
											 WHERE pbd.status_pbd='ACTIVE' 
											 AND pbd.serial_dea IN (".$serials."))
				AND pro.status_pro='ACTIVE'
				ORDER BY pbl.name_pbl";
		//echo "<pre>$sql</pre>";
		
		$result = $db -> getAll($sql);

		if($result){                    
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getPxCSerial($db, $serial_cou, $serial_pro){
		$sql="SELECT serial_pxc
			  FROM product_by_country
			  WHERE serial_pro=$serial_pro
			  AND serial_cou=$serial_cou";

		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getPxCDataByPbDSerial($db, $serial_pbd){
		$sql="SELECT pxc.serial_pxc, pxc.serial_cou
			  FROM product_by_country pxc
			  JOIN product_by_dealer pbd ON pbd.serial_pxc=pxc.serial_pxc AND pbd.serial_pbd=$serial_pbd";

		$result=$db->getRow($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

                
        
         
        
    ///GETTERS
    function getSerial_pxc(){
        return $this->serial_pxc;
    }
    function getSerial_pro(){
        return $this->serial_pro;
    }
    function getSerial_cou(){
        return $this->serial_cou;
    }
    function getPercentage_spouse_pxc(){
        return $this->percentage_spouse_pxc;
    }
    function getPercentage_extras_pxc(){
        return $this->percentage_extras_pxc;
    }
    function getPercentage_children_pxc(){
        return $this->percentage_children_pxc;
    }
    function getStatus_pxc(){
        return $this->status_pxc;
    }

    function getAditional_day_pxc(){
        return $this->aditional_day_pxc;
    }



    ///SETTERS

    function setSerial_pxc($serial_pxc){
        $this->serial_pxc = $serial_pxc;
    }
    function setSerial_pro($serial_pro){
        $this->serial_pro = $serial_pro;
    }
    function setSerial_cou($serial_cou){
        $this->serial_cou = $serial_cou;
    }
    function setPercentage_spouse_pxc($percentage_spouse_pxc){
        $this->percentage_spouse_pxc = $percentage_spouse_pxc;
    }
    function setPercentage_extras_pxc($percentage_extras_pxc){
        $this->percentage_extras_pxc = $percentage_extras_pxc;
    }
    function setPercentage_children_pxc($percentage_children_pxc){
        $this->percentage_children_pxc = $percentage_children_pxc;
    }
    function setStatus_pxc($status_pxc){
        $this->status_pxc = $status_pxc;
    }

    function setAditional_day_pxc($aditional_day_pxc){
        $this->aditional_day_pxc=$aditional_day_pxc;
    }
}
?>
