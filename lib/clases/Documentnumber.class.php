<?php
/*
File: Documentnumber.class.php
Author: Santiago Benítez
Creation Date: 22/03/2010 14:50
Last Modified: 27/04/2010
Modified By: Patricio Astudillo
*/

class Documentnumber{
	var $db;
	var $serial_dcn;
	var $serial_dbc;
	var $serial_mbc;
	var $number_dcn;
	var $to_dcn;

	function __construct($db, $serial_dcn=NULL, $serial_dbc=NULL, $serial_mbc=NULL, $number_dcn=NULL, $to_dcn=NULL ){
		$this -> db = $db;
		$this -> serial_dcn = $serial_dcn;
		$this -> serial_dbc = $serial_dbc;
		$this -> serial_mbc = $serial_mbc;
		$this -> number_dcn = $number_dcn;
		$this -> to_dcn = $to_dcn;
	}

        /***********************************************
    * function getData
    * sets data by serial_dcn
    ***********************************************/
	function getData(){
		if($this->serial_dcn!=NULL){
			$sql = 	"SELECT dcn.serial_dcn, dcn.serial_dbc, dcn.serial_mbc, dcn.number_dcn, dcn.to_dcn
					FROM documentnumber dcn
					WHERE dcn.serial_mbc='".$this->serial_dcn."'";
                                        //die($sql);

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_dcn =$result->fields[0];
				$this -> serial_dbc =$result->fields[1];
				$this -> serial_mbc =$result->fields[2];
				$this -> number_dcn = $result->fields[3];
				$this -> to_dcn = $result->fields[4];

				return true;
			}
			else
				return false;

		}else
			return false;
	}
        
	/***********************************************
    * function getData
    * sets data by serial_dcn
    ***********************************************/
	function getDataByManager($typeDbc,$purpose_dbc){
		if($this->serial_mbc!=NULL){
			$sql = 	"SELECT dcn.serial_dcn, dcn.serial_dbc, dcn.serial_mbc, dcn.number_dcn, dcn.to_dcn
					FROM documentnumber dcn
                    JOIN document_by_country dbc ON dcn.serial_dbc=dbc.serial_dbc AND (dbc.type_dbc='BOTH' OR dbc.type_dbc='".$typeDbc."') AND dbc.purpose_dbc='".$purpose_dbc."'
					WHERE dcn.serial_mbc='".$this->serial_mbc."'";
                                        //die($sql);

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_dcn =$result->fields[0];
				$this -> serial_dbc =$result->fields[1];
				$this -> serial_mbc =$result->fields[2];
				$this -> number_dcn = $result->fields[3];
				$this -> to_dcn = $result->fields[4];

				return true;
			}
			else
				return false;

		}else
			return false;
	}

        /***********************************************
    * function getData
    * sets data by serial_dcn
    ***********************************************/
	function getDataForCreditNote(){
		if($this->serial_mbc!=NULL and $this->serial_dbc!=NULL){
			$sql = "SELECT dcn.serial_dcn, dcn.serial_dbc, dcn.serial_mbc, dcn.number_dcn, dcn.to_dcn
					FROM documentnumber dcn
					WHERE dcn.serial_mbc='".$this->serial_mbc."'
					AND dcn.serial_dbc='".$this->serial_dbc."'";
            //die($sql);

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_dcn =$result->fields[0];
				$this -> serial_dbc =$result->fields[1];
				$this -> serial_mbc =$result->fields[2];
				$this -> number_dcn = $result->fields[3];
				$this -> to_dcn = $result->fields[4];

				return true;
			}else
				return false;

		}else
			return false;
	}

        /***********************************************
    * funcion existsUserDocument
    * verifies if a user exists or not
    ***********************************************/

    function existsDocumentNumber($number_inv,$typeDbc,$serial_mbc){
        $sql ="(SELECT i.number_inv, i.serial_inv, dea.serial_dea
                 FROM dealer dea
                 JOIN dealer de ON de.dea_serial_dea = dea.serial_dea
                 JOIN counter cnt ON cnt.serial_dea=de.serial_dea
                 JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
                 JOIN invoice i ON i.serial_inv=sal.serial_inv AND i.number_inv=".$number_inv."
                 JOIN document_by_country dbc ON i.serial_dbc=dbc.serial_dbc AND (dbc.type_dbc='BOTH' OR dbc.type_dbc='".$typeDbc."')    
                 WHERE dea.serial_mbc='".$serial_mbc."')
			  UNION
			  (SELECT i.number_inv, i.serial_inv, dea.serial_dea
                 FROM dealer dea
                 JOIN dealer de ON de.dea_serial_dea = dea.serial_dea
                 JOIN counter cnt ON cnt.serial_dea=de.serial_dea
                 JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
                 JOIN invoice_log il ON il.serial_sal=sal.serial_sal
				 JOIN invoice i ON i.serial_inv=il.serial_inv AND i.number_inv=".$number_inv."
                 JOIN document_by_country dbc ON i.serial_dbc=dbc.serial_dbc AND (dbc.type_dbc='BOTH' OR dbc.type_dbc='".$typeDbc."')
                 WHERE dea.serial_mbc='".$serial_mbc."')";

        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }


	/***********************************************
    * funcion getDocumentnumbers
    * gets all the Documentnumbers of the system
    ***********************************************/
	function getDocumentnumbers(){
		$lista=NULL;
		$sql = 	"SELECT serial_dcn, serial_dbc, serial_mbc, number_dcn, to_dcn
				 FROM documentnumber WHERE serial_dbc='".$this->serial_dbc."'";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}



	/***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO documentnumber (
				serial_dcn,
				serial_dbc,
				serial_mbc,
				number_dcn,
				to_dcn)
            VALUES (
                NULL,
				'".$this->serial_dbc."',
				'".$this->serial_mbc."',
				'".$this->number_dcn."',
				NULL)";
		
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE documentnumber SET
					serial_dbc ='".$this->serial_dbc."',
					serial_mbc ='".$this->serial_mbc."',
					number_dcn ='".$this->number_dcn."',
					to_dcn =NULL
				WHERE serial_dcn = '".$this->serial_dcn."'";
//echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
    }

	///GETTERS
	function getSerial_dcn(){
		return $this->serial_dcn;
	}
	function getSerial_dbc(){
		return $this->serial_dbc;
	}
	function getSerial_mbc(){
		return $this->serial_mbc;
	}
	function getNumber_dcn(){
		return $this->number_dcn;
	}
	function getTo_dcn(){
		return $this->to_dcn;
	}


	///SETTERS
	function setSerial_dcn($serial_dcn){
		$this->serial_dcn = $serial_dcn;
	}
	function setSerial_dbc($serial_dbc){
		$this->serial_dbc = $serial_dbc;
	}
	function setSerial_mbc($serial_mbc){
		$this->serial_mbc = $serial_mbc;
	}
	function setNumber_dcn($number_dcn){
		$this->number_dcn = $number_dcn;
	}
	function setTo_dcn($to_dcn){
		$this->to_dcn = $to_dcn;
	}
}
?>