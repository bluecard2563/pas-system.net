<?php
/*
File: ContractsFormats.class
Author: David Rosales
Creation Date: 28/08/2015
Last modified:
Modified By: 
*/

class ContractsFormats {				
	protected $db;
	protected $serial_cof;
	protected $name_cof;
	protected $description_cof;
	protected $file_cof;
    
    function __construct($db, $serial_cof) {
        $this->db = $db;
        $this->serial_cof = $serial_cof;
    }

    
    function getSerial_cof() {
        return $this->serial_cof;
    }

    function getName_cof() {
        return $this->name_cof;
    }

    function getDescription_cof() {
        return $this->description_cof;
    }

    function getFile_cof() {
        return $this->file_cof;
    }
    
    
    function setSerial_cof($serial_cof) {
        $this->serial_cof = $serial_cof;
    }

    function setName_cof($name_cof) {
        $this->name_cof = $name_cof;
    }

    function setDescription_cof($description_cof) {
        $this->description_cof = $description_cof;
    }

    function setFile_cof($file_cof) {
        $this->file_cof = $file_cof;
    }


    function getData(){
		if($this -> serial_cof != NULL){
			$sql = 	"SELECT serial_cof,
							name_cof,
							description_cof,
							file_cof
					FROM contracts_formats
					WHERE serial_cof = '{$this -> serial_cof}'";
			$result = $this -> db -> Execute($sql);

			if($result -> fields[0]){
				$this -> serial_cof = $result -> fields[0];
				$this -> name_cof = $result -> fields[1];
				$this -> description_cof = $result -> fields[2];
				$this -> file_cof = $result -> fields[3];
				return true;
			}	
		}
		return false;		
	}

    
}