<?php
/**
 * File: ParticularConditions
 * Author: Patricio Astudillo
 * Creation Date: 24-ago-2012
 * Last Modified: 24-ago-2012
 * Modified By: Patricio Astudillo
 */

class ParticularConditions{
	var $db;
	var $serial_prc;
	var $serial_lang;
	var $prc_serial_prc;
	var $name_prc;
	var $status_prc;
	
	function __construct($db, $serial_prc = NULL, $serial_lang = NULL, 
						$prc_serial_prc = NULL, $name_prc = NULL, $status_prc = 'ACTIVE') {
		$this->db = $db;
		$this->serial_prc = $serial_prc;
		$this->serial_lang = $serial_lang;
		$this->prc_serial_prc = $prc_serial_prc;
		$this->name_prc = $name_prc;
		$this->status_prc = $status_prc;
	}
	
	//******************************** PRIVATE FUNCTIONS ******************************
	
	/**
	 * @name getData
	 * @return boolean 
	 */
	function getData(){
		if($this->serial_prc){
			$sql = "SELECT serial_prc, serial_lang, prc_serial_prc, name_prc, status_prc
					FROM particular_conditions 
					WHERE serial_prc='".$this->serial_prc."'";
                                
			$result = $this->db->getRow($sql);

			if($result){
				$this->serial_prc = $result['serial_prc'];
				$this->serial_lang = $result['serial_lang'];
				$this->prc_serial_prc = $result['prc_serial_prc'];
				$this->name_prc = $result['name_prc'];
				$this->status_prc = $result['status_prc'];
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @name insert
	 * @return boolean 
	 */
	function insert(){
		$sql = "INSERT INTO particular_conditions (
					serial_prc, 
					serial_lang, 
					prc_serial_prc,
					name_prc,
					status_prc
				) VALUES (
					NULL,
					{$this->serial_lang},";
		$sql .= $this->prc_serial_prc?$this->prc_serial_prc:"NULL";
		$sql .= "	,'{$this->name_prc}',
					'{$this->status_prc}'
				)";
					
		$result = $this->db->Execute($sql);
            
		if($result){
			$this->serial_prc = $this->db->insert_ID();
			return $this->db->insert_ID();
		}else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
			return false;
		}
	}
	
	/**
	 * @name update
	 * @return boolean 
	 */
	function update(){
		$sql = "UPDATE particular_conditions SET 
					serial_lang = {$this->serial_lang},
					prc_serial_prc = ";
		$sql .= ($this->prc_serial_prc)?$this->prc_serial_prc:"NULL";
		$sql .= "	,name_prc = '{$this->name_prc}',
					status_prc = '{$this->status_prc}'
					WHERE serial_prc = {$this->serial_prc}";

		$result = $this->db->Execute($sql);
        
		if ($result === false)return false;
		return true;
	}
	
	//******************************** PUBLIC FUNCTIONS ******************************
	
	/**
	 * @name validateTranslationByCondition
	 * @param rsc $db
	 * @param int $serial_prc
	 * @param int $serial_lang 
	 * @return boolean true if exists / flase if available
	 */
	public static function validateTranslationByCondition($db, $serial_prc, $serial_lang){
		$sql = "SELECT 1
				FROM particular_conditions
				WHERE prc_serial_prc = $serial_prc
				AND serial_lang = $serial_lang";
		
		$result = $db -> getOne($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getLanguagesAvailableForTranslation
	 * @param RSC $db
	 * @param int $serial_prc
	 * @return boolean NO_Registers
	 * @return array Registers
	 * @
	 */
	public static function getLanguagesAvailableForTranslation($db, $serial_prc){
		$sql = "SELECT serial_lang, name_lang
				FROM language
				WHERE serial_lang NOT IN (	SELECT serial_lang
											FROM particular_conditions
											WHERE prc_serial_prc = $serial_prc
											
											UNION 
											
											SELECT 2)";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getAllConditionsByLanguage
	 * @param RSC $db
	 * @param int $serial_lang
	 * @return boolean 
	 * @return array
	 */
	public static function getAllConditionsByLanguage($db, $serial_lang){
		$sql = "SELECT *
				FROM particular_conditions
				WHERE serial_lang = $serial_lang";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @return conditionIsAllReadyAssigned
	 * @param RSC $db
	 * @param int $serial_prc
	 * @param int $serial_pxc
	 * @return boolean
	 */
	public static function conditionIsAllReadyAssigned($db, $serial_prc, $serial_pxc){
		$sql = "SELECT serial_pcp
				FROM pconditions_pxc
				WHERE serial_prc = $serial_prc
				AND serial_pxc = $serial_pxc
				AND status_pcp = 'ACTIVE'";
		
		$result = $db -> getOne($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getPConditionsForProduct
	 * @param RSC $db
	 * @param int $serial_pxc
	 * @param int $serial_lang
	 * @param string $status_pcp
	 * @return boolean
	 * @return array 
	 */
	public static function getPConditionsForProduct($db, $serial_pxc, $serial_lang, $status_pcp = 'ACTIVE'){
		$sql = "SELECT serial_pcp, name_prc, status_pcp, weight_pcp
				FROM pconditions_pxc pcp
				JOIN particular_conditions prc ON prc.serial_prc = pcp.serial_prc
					AND prc.serial_lang = $serial_lang
					AND pcp.serial_pxc = $serial_pxc
				";
		
		if($status_pcp != 'ALL'){
			$sql .= " WHERE status_pcp = '$status_pcp'";
		}
		
		$sql .= " ORDER BY weight_pcp, name_prc ASC";
		
		//Debug::print_r($sql);
		$result = $db ->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function translationExist($db, $serial_lang, $serial_prc){
		$sql = "SELECT serial_prc
				FROM particular_conditions
				WHERE serial_lang = $serial_lang
				AND prc_serial_prc = $serial_prc
				";
		
		//Debug::print_r($sql);
		$result = $db ->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getStatusValues($db){
		$sql = "SHOW COLUMNS FROM particular_conditions LIKE 'status_prc'";
		$result = $db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	
	//******************************** GETTERS AND SETTERS ******************************

	public function getSerial_prc() {
		return $this->serial_prc;
	}

	public function setSerial_prc($serial_prc) {
		$this->serial_prc = $serial_prc;
	}

	public function getSerial_lang() {
		return $this->serial_lang;
	}

	public function setSerial_lang($serial_lang) {
		$this->serial_lang = $serial_lang;
	}

	public function getPrc_serial_prc() {
		return $this->prc_serial_prc;
	}

	public function setPrc_serial_prc($prc_serial_prc) {
		$this->prc_serial_prc = $prc_serial_prc;
	}

	public function getName_prc() {
		return $this->name_prc;
	}

	public function setName_prc($name_prc) {
		$this->name_prc = $name_prc;
	}

	public function getStatus_prc() {
		return $this->status_prc;
	}

	public function setStatus_prc($status_prc) {
		$this->status_prc = $status_prc;
	}
}
?>
