<?php

/*
  File: CustomerPromo.class.php
  Author: Patricio Astudillo
  Creation Date: 01/02/2010 09:48
  Last Modified: 01/02/2010
  Modified By: Patricio Astudillo
 */

class CustomerPromo {
	var $db;
	var $serial_ccp;
	var $name_ccp;
	var $description_ccp;
	var $cupon_prefix_ccp;
	var $creation_date_ccp;
	var $begin_date_ccp;
	var $end_date_ccp;
	var $payment_form_ccp;
	var $discount_percentage_ccp;
	var $other_discount_ccp;
	var $type_ccp;
	var $scope_ccp;
	var $status_ccp;
	var $aux;

	function __construct($db, $serial_ccp = NULL, $name_ccp = NULL, $description_ccp = NULL,
						$cupon_prefix_ccp = NULL, $creation_date_ccp = NULL, $begin_date_ccp = NULL, $end_date_ccp = NULL,
						$payment_form_ccp = NULL, $discount_percentage_ccp = NULL, $other_discount_ccp = NULL, $type_ccp = NULL,
						$scope_ccp = NULL, $status_ccp = NULL) {
		$this->db = $db;
		$this->serial_ccp = $serial_ccp;
		$this->name_ccp = $name_ccp;
		$this->description_ccp = $description_ccp;
		$this->cupon_prefix_ccp = $cupon_prefix_ccp;
		$this->creation_date_ccp = $creation_date_ccp;
		$this->begin_date_ccp = $begin_date_ccp;
		$this->end_date_ccp = $end_date_ccp;
		$this->payment_form_ccp = $payment_form_ccp;
		$this->discount_percentage_ccp = $discount_percentage_ccp;
		$this->other_discount_ccp = $other_discount_ccp;
		$this->type_ccp = $type_ccp;
		$this->scope_ccp = $scope_ccp;
		$this->status_ccp = $status_ccp;
	}

	function getData(){
		if($this->serial_ccp){
			$sql = "SELECT name_ccp, description_ccp, cupon_prefix_ccp,
						DATE_FORMAT(creation_date_ccp, '%d/%m/%Y') AS 'creation_date_ccp',
						DATE_FORMAT(begin_date_ccp, '%d/%m/%Y') AS 'begin_date_ccp',
						DATE_FORMAT(end_date_ccp, '%d/%m/%Y') AS 'end_date_ccp',
						payment_form_ccp, discount_percentage_ccp, other_discount_ccp, type_ccp,
						scope_ccp, status_ccp, pxc.serial_cou
					FROM customer_promo ccp
					JOIN applied_products_by_customer_promo apc ON apc.serial_ccp = ccp.serial_ccp
					JOIN product_by_country pxc ON pxc.serial_pxc = apc.serial_pxc
					WHERE ccp.serial_ccp = {$this->serial_ccp}
					GROUP BY ccp.serial_ccp";
			
			$result = $this -> db -> getRow($sql);

			if($result){
				$this->name_ccp = $result['name_ccp'];
				$this->description_ccp = $result['description_ccp'];
				$this->cupon_prefix_ccp = $result['cupon_prefix_ccp'];
				$this->creation_date_ccp = $result['creation_date_ccp'];
				$this->begin_date_ccp = $result['begin_date_ccp'];
				$this->end_date_ccp = $result['end_date_ccp'];
				$this->payment_form_ccp = $result['payment_form_ccp'];
				$this->discount_percentage_ccp = $result['discount_percentage_ccp'];
				$this->other_discount_ccp = $result['other_discount_ccp'];
				$this->type_ccp = $result['type_ccp'];
				$this->scope_ccp = $result['scope_ccp'];
				$this->status_ccp = $result['status_ccp'];
				$this->aux['serial_cou'] = $result['serial_cou'];

				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}

	function insert() {
		$sql = "INSERT INTO customer_promo
					(`serial_ccp`,
					`name_ccp`,
					`description_ccp`,
					`cupon_prefix_ccp`,
					`creation_date_ccp`,
					`begin_date_ccp`,
					`end_date_ccp`,
					`payment_form_ccp`,
					`discount_percentage_ccp`,
					`other_discount_ccp`,
					`type_ccp`,
					`scope_ccp`,
					status_ccp
				) VALUES (
					NULL,
					'{$this -> name_ccp}',
					'{$this -> description_ccp}',
					'{$this -> cupon_prefix_ccp}',
					NOW(),
					STR_TO_DATE('{$this -> begin_date_ccp}', '%d/%m/%Y'),
					STR_TO_DATE('{$this -> end_date_ccp}', '%d/%m/%Y'),
					'{$this -> payment_form_ccp}',
					'{$this -> discount_percentage_ccp}',
					'{$this -> other_discount_ccp}',
					'{$this -> type_ccp}',
					'{$this -> scope_ccp}',
					'REGISTERED')";

		$result = $this -> db -> Execute($sql);

		if($result){
			return $this->db->insert_ID();
		}else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
		}
	}

	function update(){
		$sql = "UPDATE customer_promo SET
					name_ccp = '{$this -> name_ccp}',
					description_ccp = '{$this -> description_ccp}',
					cupon_prefix_ccp = '{$this -> cupon_prefix_ccp}',
					begin_date_ccp = STR_TO_DATE('{$this -> begin_date_ccp}', '%d/%m/%Y'),
					end_date_ccp = STR_TO_DATE('{$this -> end_date_ccp}', '%d/%m/%Y'),
					payment_form_ccp = '{$this -> payment_form_ccp}',
					discount_percentage_ccp = '{$this -> discount_percentage_ccp}',
					other_discount_ccp = '{$this -> other_discount_ccp}',
					type_ccp = '{$this -> type_ccp}',
					scope_ccp= '{$this -> scope_ccp}',
					status_ccp= '{$this -> status_ccp}'
				WHERE serial_ccp = {$this -> serial_ccp}";

		$result = $this -> db -> Execute($sql);

		if($result){
			return true;
		}else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
        	return false;
		}
	}

	/**
	 * @name nameExist
	 * @param <ADODB> $db
	 * @param <STRING> $promo_name
	 * @param <ID> $serial_ccp
	 * @return <BOOLEAN> TRUE/FALSE
	 */
	public static function nameExist($db, $promo_name, $begin_date, $end_date, $serial_ccp = NULL){
		$sql = "SELECT serial_ccp
				FROM customer_promo
				WHERE name_ccp = '$promo_name'
				AND (STR_TO_DATE('$begin_date', '%d/%m/%Y') BETWEEN begin_date_ccp AND end_date_ccp OR
					STR_TO_DATE('$end_date', '%d/%m/%Y') BETWEEN begin_date_ccp AND end_date_ccp OR
					begin_date_ccp BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y') OR
					end_date_ccp BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y'))";

		if($serial_ccp){
			$sql .= " AND serial_ccp <> $serial_ccp";
		}

		$result = $db -> getRow($sql);

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name codeExist
	 * @param <ADODB> $db
	 * @param <STRING> $code
	 * @param <ID> $serial_ccp
	 * @return <BOOLEAN> TRUE/FALSE
	 */
	public static function codeExist($db, $code, $serial_ccp = NULL){
		$sql = "SELECT serial_ccp
				FROM customer_promo
				WHERE cupon_prefix_ccp = '$code'";

		if($serial_ccp){
			$sql .= " AND serial_ccp <> $serial_ccp";
		}
		
		$result = $db -> getRow($sql);

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name getCustomerPromoByCountryAndStatus
	 * @param <adodb> $db
	 * @param <id> $serial_cou
	 * @param <id> $status_ccp
	 * @return <array> promo_list
	 */
	public static function getCustomerPromoByCountryAndStatus($db, $serial_cou, $status_ccp){
		$sql = "SELECT ccp.serial_ccp, apc.serial_pxc, name_ccp, description_ccp, cupon_prefix_ccp,
						DATE_FORMAT(creation_date_ccp, '%d/%m/%Y') AS 'creation_date_ccp',
						DATE_FORMAT(begin_date_ccp, '%d/%m/%Y') AS 'begin_date_ccp',
						DATE_FORMAT(end_date_ccp, '%d/%m/%Y') AS 'end_date_ccp',
						payment_form_ccp, discount_percentage_ccp, other_discount_ccp, type_ccp,
						scope_ccp, status_ccp
				FROM customer_promo ccp
				JOIN applied_products_by_customer_promo apc ON apc.serial_ccp = ccp.serial_ccp
				JOIN product_by_country pxc ON pxc.serial_pxc = apc.serial_pxc AND pxc.serial_cou = $serial_cou
				WHERE status_ccp = '$status_ccp'
				GROUP BY ccp.serial_ccp";
		
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}


	/**
	 * @name getCustomerPromoSerialByCountryAndStatus
	 * @param <adodb> $db
	 * @param <id> $serial_cou
	 * @param <id> $status_ccp
	 * @return <array> promo_list
	 */
	public static function getCustomerPromoSerialByCountryAndStatus($db, $serial_cou, $status_ccp){
		$sql = "SELECT ccp.serial_ccp
				FROM customer_promo ccp
				JOIN applied_products_by_customer_promo apc ON apc.serial_ccp = ccp.serial_ccp
				JOIN product_by_country pxc ON pxc.serial_pxc = apc.serial_pxc AND pxc.serial_cou = $serial_cou
				WHERE status_ccp = '$status_ccp'
				GROUP BY ccp.serial_ccp";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name getCustomerPromoSerialByCountryAndTwoStatus
	 * @param <adodb> $db
	 * @param <id> $serial_cou
	 * @param <id> $status_ccp1
         * @param <id> $status_ccp2
	 * @return <array> promo_list
	 */
	public static function getCustomerPromoSerialByCountryAndTwoStatus($db, $serial_cou, $status_ccp1, $status_ccp2){
		$sql = "SELECT ccp.serial_ccp
				FROM customer_promo ccp
				JOIN applied_products_by_customer_promo apc ON apc.serial_ccp = ccp.serial_ccp
				JOIN product_by_country pxc ON pxc.serial_pxc = apc.serial_pxc AND pxc.serial_cou = $serial_cou
				WHERE status_ccp = '$status_ccp1' OR status_ccp = '$status_ccp2'
				GROUP BY ccp.serial_ccp";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	 * getPromoInfoForCupon
	 * @param <ADODB> $db
	 * @param <Customer_id> $serial_ccp
	 * @return <type>
	 */
	public static function getPromoInfoForCupon($db, $serial_ccp){
		$sql = "   SELECT cup.serial_cup, csp.cupon_prefix_ccp, csp.end_date_ccp, csp.discount_percentage_ccp, csp.other_discount_ccp, csp.name_ccp,
					   csp.type_ccp, csp.scope_ccp, DATE_FORMAT(csp.end_date_ccp, '%d/%m/%Y') AS 'end_date_ccp',
                                            DATE_FORMAT(csp.begin_date_ccp, '%d/%m/%Y') AS 'begin_date_ccp',csp.payment_form_ccp
				FROM customer_promo csp
				join cupon cup on csp.serial_ccp= cup.serial_ccp
				WHERE csp.serial_ccp = $serial_ccp";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	 * getCountriesWithPromo
	 * @param <ADODB> $db
	 */
	function getCountriesWithPromo($db){
		$sql = "SELECT DISTINCT c.serial_cou, c.name_cou
				FROM customer_promo ccp
				JOIN applied_products_by_customer_promo apc ON apc.serial_ccp = ccp.serial_ccp
				JOIN product_by_country pxc ON pxc.serial_pxc = apc.serial_pxc
				JOIN country c ON c.serial_cou = pxc.serial_cou
				WHERE (ccp.status_ccp = 'ACTIVE'
				OR ccp.status_ccp = 'REGISTERED')";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getLastCreatedCuponByPromo($db, $serial_ccp){
		$sql = "SELECT MAX(number_cup) AS 'maximum', serial_cup
				FROM cupon
				WHERE serial_ccp = $serial_ccp";

		$result = $db -> getRow($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
        
        
        public static function countActiveCouponsByPromo($db, $serial_ccp){
		$sql = "SELECT COUNT(number_cup) FROM cupon 
                        WHERE serial_ccp = $serial_ccp AND status_cup='ACTIVE' ";

		$result = $db -> getOne($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}            
        }

        /**
	 * @name getActiveCuponsByPromo
	 * @param <ADODB> $db
	 * @return <ARRAY> Cupons
	 */
        public static function getActiveNumberCouponsByPromo($db, $serial_ccp){
		$sql = "SELECT number_cup FROM cupon
                        WHERE serial_ccp = $serial_ccp AND status_cup='ACTIVE' ";
                        
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
        }

        /**
	 * @name getCustomerPromoTypes
	 * @param <ADODB> $db
	 * @return <ARRAY> types
	 */
	public static function getCustomerPromoTypes($db){
		$sql = "SHOW COLUMNS FROM customer_promo LIKE 'type_ccp'";
		$result = $db -> Execute($sql);

		$type = $result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	 * @name getCustomerPromoPaymentForms
	 * @param <ADODB> $db
	 * @return <ARRAY> payment_forms
	 */
	public static function getCustomerPromoPaymentForms($db){
		$sql = "SHOW COLUMNS FROM customer_promo LIKE 'payment_form_ccp'";
		$result = $db -> Execute($sql);

		$type = $result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	 * @name getCustomerPromoScope
	 * @param <ADODB> $db
	 * @return <ARRAY> scopes
	 */
	public static function getCustomerPromoScope($db){
		$sql = "SHOW COLUMNS FROM customer_promo LIKE 'scope_ccp'";
		$result = $db -> Execute($sql);

		$type = $result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/**
	 * @name getCustomerPromoStatus
	 * @param <ADODB> $db
	 * @return <ARRAY> status
	 */
	public static function getCustomerPromoStatus($db){
		$sql = "SHOW COLUMNS FROM customer_promo LIKE 'status_ccp'";
		$result = $db -> Execute($sql);

		$type = $result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	public function getName_ccp() {
	 return $this->name_ccp;
	}

	public function setName_ccp($name_ccp) {
	 $this->name_ccp = $name_ccp;
	}

	public function getDescription_ccp() {
	 return $this->description_ccp;
	}

	public function setDescription_ccp($description_ccp) {
	 $this->description_ccp = $description_ccp;
	}

	public function getCupon_prefix_ccp() {
	 return $this->cupon_prefix_ccp;
	}

	public function setCupon_prefix_ccp($cupon_prefix_ccp) {
	 $this->cupon_prefix_ccp = $cupon_prefix_ccp;
	}

	public function getCreation_date_ccp() {
	 return $this->creation_date_ccp;
	}

	public function setCreation_date_ccp($creation_date_ccp) {
	 $this->creation_date_ccp = $creation_date_ccp;
	}

	public function getBegin_date_ccp() {
	 return $this->begin_date_ccp;
	}

	public function setBegin_date_ccp($begin_date_ccp) {
	 $this->begin_date_ccp = $begin_date_ccp;
	}

	public function getEnd_date_ccp() {
	 return $this->end_date_ccp;
	}

	public function setEnd_date_ccp($end_date_ccp) {
	 $this->end_date_ccp = $end_date_ccp;
	}

	public function getPayment_form_ccp() {
	 return $this->payment_form_ccp;
	}

	public function setPayment_form_ccp($payment_form_ccp) {
	 $this->payment_form_ccp = $payment_form_ccp;
	}

	public function getDiscount_percentage_ccp() {
	 return $this->discount_percentage_ccp;
	}

	public function setDiscount_percentage_ccp($discount_percentage_ccp) {
	 $this->discount_percentage_ccp = $discount_percentage_ccp;
	}

	public function getOther_discount_ccp() {
	 return $this->other_discount_ccp;
	}

	public function setOther_discount_ccp($other_discount_ccp) {
	 $this->other_discount_ccp = $other_discount_ccp;
	}

	public function getType_ccp() {
	 return $this->type_ccp;
	}

	public function setType_ccp($type_ccp) {
	 $this->type_ccp = $type_ccp;
	}

	public function getScope_ccp() {
	 return $this->scope_ccp;
	}

	public function setScope_ccp($scope_ccp) {
	 $this->scope_ccp = $scope_ccp;
	}

	public function getStatus_ccp() {
	 return $this->status_ccp;
	}

	public function setStatus_ccp($status_ccp) {
	 $this->status_ccp = $status_ccp;
	}

	public function getAux() {
	 return $this->aux;
	}

	public function setAux($aux) {
	 $this->aux = $aux;
	}

	public function getSerial_ccp() {
	 return $this->serial_ccp;
	}

	public function setSerial_ccp($serial_ccp) {
	 $this->serial_ccp = $serial_ccp;
	}
}
?>