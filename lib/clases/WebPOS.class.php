<?php
/**
 * File: WebPOS
 * Author: Patricio Astudillo
 * Creation Date: 28-sep-2012
 * Last Modified: 28-sep-2012
 * Modified By: Patricio Astudillo
 */

class WebPOS {
	var $db;
	var $serial_pos;
	var $serial_dea;
	var $username_pos;
	var $password_pos;
	var $ip_pos;
	var $status_pos;
	var $lastmodified_pos;
	
	public function __construct($db, $serial_dea) {
		$this->db = $db;
		$this->serial_dea = $serial_dea;
		
		$this->getData();
	}
	
	public function getData(){
		$sql = "SELECT	serial_pos, serial_dea, username_pos, password_pos, ip_pos, status_pos,
						lastmodified_pos
				FROM webPointOfSale
				WHERE serial_dea = {$this->serial_dea}";
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$this -> serial_pos = $result['serial_pos'];
			$this -> serial_dea = $result['serial_dea'];
			$this -> username_pos = $result['username_pos'];
			$this -> password_pos = $result['password_pos'];
			$this -> ip_pos = $result['ip_pos'];
			$this -> status_pos = $result['status_pos'];
			$this -> lastmodified_pos = $result['lastmodified_pos'];
		}else{
			return false;
		}
	}
	
	public function save(){
		if(!$this->serial_pos){
			$sql = "INSERT INTO webPointOfSale (
							serial_pos, 
							serial_dea, 
							username_pos, 
							password_pos, 
							ip_pos, 
							status_pos,
							lastmodified_pos
						) VALUES (
							NULL,
							{$this->serial_dea},
							'{$this->username_pos}',
							'{$this->password_pos}',
							'{$this->ip_pos}',
							'ACTIVE',
							CURRENT_TIMESTAMP()
						)";
			
			$result = $this->db->Execute($sql);
			
			if($result){
				$this->serial_pos = $this->db->insert_ID();
				$this->getData();
				return true;
			}else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
				return false;
			}
			
		}else{
			
			$sql = "UPDATE webPointOfSale SET
						serial_dea = '{$this->serial_dea}', 
						username_pos = '{$this->username_pos}', 
						password_pos = '{$this->password_pos}', 
						ip_pos = '{$this->ip_pos}', 
						status_pos = '{$this->status_pos}',
						lastmodified_pos = CURRENT_TIMESTAMP()
					WHERE serial_pos = {$this->serial_pos}";
					
			$result = $this->db->Execute($sql);
			
			if ($result==true) 
				return true;
			else
				return false;
		}
	}
	
	/**
	 * @name usernameExist
	 * @param type $db
	 * @param type $serial_dea
	 * @param type $username
	 * @return boolean 
	 */
	public static function usernameExist($db, $serial_dea, $username){
		$sql = "SELECT	serial_pos
				FROM webPointOfSale
				WHERE serial_dea <> {$serial_dea}
				AND username_pos = '$username'
				AND status_pos = 'ACTIVE'";
				
		$result = $db->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name ipExist
	 * @param type $db
	 * @param type $serial_dea
	 * @param type $ip
	 * @return boolean 
	 */
	public static function ipExist($db, $serial_dea, $ip){
		$sql = "SELECT	serial_pos
				FROM webPointOfSale
				WHERE serial_dea <> {$serial_dea}
				AND ip_pos = '$ip'";
				
		$result = $db->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name getStatusList
	 * @param SR $db
	 * @return type 
	 */
	public static function getStatusList($db) {
		$sql = "SHOW COLUMNS FROM webPointOfSale LIKE 'status_pos'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}
	
	/**
	 * @name getAvailablePOSByStatus
	 * @param SR $db
	 * @param stirng $status_pos
	 * @return boolean 
	 * @return array
	 */
	public static function getAvailablePOSByStatus($db, $status_pos = 'ACTIVE'){
		$sql = "SELECT	serial_pos, pos.serial_dea, username_pos, password_pos, ip_pos, status_pos,
						lastmodified_pos, d.name_dea, d.id_dea
				FROM webPointOfSale pos
				JOIN dealer d ON d.serial_dea = pos.serial_dea AND d.status_dea = '$status_pos'
				WHERE status_pos = '$status_pos'";
		
		$result = $db ->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function verifyWSDLLogin($db, $user, $pswd, $ip){
		$sql = "SELECT	serial_pos
				FROM webPointOfSale pos
				WHERE md5(username_pos) = '$user'
				AND password_pos = '$pswd'";
				//AND ip_pos = '$ip'";
		
		$result = $db ->getOne($sql);
		
		if($result){
			return $result;
		}else{
			ErrorLog::log($db, 'WSDL - LoginFailed', array($ip, $user, $pswd));
			return FALSE;
		}
	}
	
	/**
	 * @name getProductListByPOS
	 * @param RS $db
	 * @param int $serial_pos
	 * @param int $language
	 * @return boolean 
	 * @return array
	 */
	public static function getProductListByPOS($db, $serial_pos, $language){
		$sql = "SELECT name_pbl, pxc.serial_pxc, p.serial_pro, p.show_price_pro
				FROM webPointOfSale pos
				JOIN dealer d ON d.serial_dea = pos.serial_dea AND pos.serial_pos = $serial_pos
				JOIN product_by_dealer pbd ON pbd.serial_dea = d.serial_dea
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND in_web_pro = 'YES'
				JOIN product_by_language pbl ON pbl.serial_pro = p.serial_pro AND pbl.serial_lang = $language";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			ErrorLog::log($db, 'WSDL - getProductListByPOS Failed', $sql);
			return FALSE;
		}
	}
	
	
	public static function getBenefitsByProductPOS($db, $serial_pos, $product_id, $language){
		$sql = 	"	SELECT	bp.serial_ben, 
							bl.description_bbl, 
							price_bxp,	
							cr.symbol_cur, 
							bp.restriction_price_bxp,
							rl.description_rbl,
							cl.description_cbl
					FROM benefits_product bp
					JOIN product p ON p.serial_pro = bp.serial_pro AND in_web_pro = 'YES'
					JOIN product_by_country pxc ON pxc.serial_pro = p.serial_pro
					JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
					JOIN dealer d ON d.serial_dea = pbd.serial_dea AND d.status_dea = 'ACTIVE'
					JOIN webPointOfSale pos ON pos.serial_dea = d.serial_dea AND status_pos = 'ACTIVE' AND serial_pos = $serial_pos 
					LEFT JOIN benefits_by_language bl ON bp.serial_ben = bl.serial_ben AND bl.serial_lang = ".$language."
					LEFT JOIN conditions_by_language cl ON cl.serial_con = bp.serial_con AND cl.serial_lang = ".$language."
					LEFT JOIN currency cr ON cr.serial_cur = bp.serial_cur
					LEFT JOIN restriction_by_language rl ON rl.serial_rst = bp.serial_rst AND rl.serial_lang = ".$language."

					WHERE bp.serial_pro = $product_id
					AND bp.status_bxp = 'ACTIVE'";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			ErrorLog::log($db, 'WSDL - getBenefitsByProductPOS Failed', $sql);
			return FALSE;
		}
	}
	
	public static function getSerialPBDForPOSProduct($db, $serial_pos, $serial_pxc){
		$sql = "SELECT serial_pbd
				FROM webPointOfSale pos
				JOIN dealer d ON d.serial_dea = pos.serial_dea AND pos.serial_pos = $serial_pos
				JOIN product_by_dealer pbd ON pbd.serial_dea = d.serial_dea
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc AND pxc.serial_pxc = $serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND in_web_pro = 'YES'
				LIMIT 1";
		
		$serial_pbd = $db->getRow($sql);
		
		if($serial_pbd['serial_pbd']){
			return $serial_pbd['serial_pbd'];
		}else{
			return FALSE;
		}
	}
	
	public static function getCountryCodeFromWS($db, $name_string){
		$sql = "SELECT serial_cou
				FROM country c
				WHERE name_cou like '%$name_string%'";
		
		$result = $db ->getOne($sql);
		
		if($result){
			return $result;
		}else{
			ErrorLog::log($db, 'WSDL - getCountryCodeFromWS Failed', $sql);
			return 218;		//USA
		}
	}
	
	/**
	 * @name calculatePriceForPOSList
	 * @global type $phoneSalesDealer
	 * @param type $db
	 * @param type $singleProduct
	 * @param type $selTripDays
	 * @param type $extrasRestricted
	 * @param type $serial_pbd
	 * @return string 
	 */
	public static function calculatePriceForPOSList($db, &$singleProduct, $selTripDays, $extrasRestricted, $serial_pbd = NULL) {
        //Default Product Split:
        $departureCountry = $singleProduct['departure'];
        $targetDays = $singleProduct['days'];
        $txtOverAge = $singleProduct['adults'];
        $txtUnderAge = $singleProduct['children'];
        $checkPartner = $singleProduct['spouseTraveling'];

        //Initialize
        $productExtraDays = 0;
        $productBasePrice = 0;
        $productBaseCost = 0;

        //GET ALL THE "PRICES per DURATION" AVAILABLE (COUNTRY SPECIFIC OR ALL COUNTRY PRICES):
        $productByCountry = new ProductByCountry($db, $singleProduct['serial_pxc']);
        $productByCountry->getData();

        $product = new Product($db, $productByCountry->getSerial_pro());
        $product->getData();

        $serialCountry = $productByCountry->getSerial_cou();
        if (!PriceByProductByCountry::pricesByProductExist($db, $productByCountry->getSerial_pxc())) {
            global $phoneSalesDealer;
            $serialCountry = Dealer::getDealersCountrySerial($db, $phoneSalesDealer);
        }

        $filterDays = NULL;
        if (!$selTripDays) {
            $filterDays = $targetDays;
        }
        $priceObject = new PriceByProductByCountry($db);
        $prices = $priceObject->getPricesByProductByCountry($productByCountry->getSerial_pro(), $serialCountry, $filterDays);
		
		$prices_pbd = PriceByPOS::getPricesByProductByPOS($db, $serial_pbd, $filterDays);
		
		if($prices_pbd){
			$prices = $prices_pbd;
		}
		
        //IF THERE IS NOT A SUITABLE RANGE OF PRICES FOR THE AMOUNT OF DAYS SELECTED
//		if (!$prices) {
//			return "NO_PRICES";
//		}
        //CHOOSE THE BEST "PRICES per DURATION" (DURATION_PPC) TO APPLY, ACCORDING TO THE DAYS OF THE TRIP (TARGET DAYS):
        $closestDuration = 0;

        if (is_array($prices)) {
            if ($selTripDays && $product->getShow_price_pro() == 'YES') {
                foreach ($prices as $p) {
                    $currentDuration = $p['duration_ppc'];
                    if ($currentDuration >= $selTripDays) {
                        $closestDuration = $currentDuration;
                        $closestDurationPrice = $p['price_ppc'];
                        $closestDurationCost = $p['cost_ppc'];
                        break;
                    }
                }
                //WE SHOULD ALWAYS GET A CORRECT DURATION SINCE TRIP DAYS VALIDATOR TOOK CARE OF IT
                $productExtraDays = 0;
                $productBasePrice = $closestDurationPrice;
                $productBaseCost = $closestDurationCost;
            } else {
                foreach ($prices as $p) {
                    $currentDuration = $p['duration_ppc'];
                    if (($currentDuration <= $targetDays && $currentDuration + 10 >= $targetDays)//IS THIS DURATION ELIGIBLE?? [EQUAL OR LOWER BUT WITHIN RANGE]
                            || ($currentDuration > $targetDays && $closestDuration == 0)) { //IS THE SUPERIOR OUR BEST SHOT?, THEN USE IT
                        $closestDuration = $currentDuration;
                        $closestDurationPrice = $p['price_ppc'];
                        $closestDurationCost = $p['cost_ppc'];
                    }
                }
                //WE SHOULD ALWAYS GET A CORRECT DURATION SINCE TRIP DAYS VALIDATOR TOOK CARE OF IT
                $productExtraDays = $targetDays - $closestDuration;
                $productBasePrice = $closestDurationPrice;
                $productBaseCost = $closestDurationCost;
            }
        } else {
			$serial_pps = PriceByPOS::getMaxPricePosiblePOS($db, $serial_pbd, $filterDays);
			if($serial_pps){
				$priceObject = new PriceByPOS($db, $serial_pps);
				$priceObject->getData();
				$priceObject = get_object_vars($priceObject);

				$closestDurationPrice = $priceObject['price_pp2'];
				$closestDurationCost = $priceObject['cost_pp2'];
				$closestDurationRangeMax = $priceObject['max_pp2'];
				$closestDurationRangeMin = $priceObject['min_pp2'];
				$closestDurationPXC = $priceObject['serial_pbd'];

				$closestDurationPrice = number_format(round($closestDurationPrice, 2), 2, '.', '');
				$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');

				$closestDuration = $priceObject['duration_pps'];
				$productExtraDays = $targetDays - $closestDuration;
				$productBasePrice = $closestDurationPrice;
				$productBaseCost = $closestDurationCost;
				//have to check serial_pps no ppc
				
				
			}else{
				$serial_ppc = $priceObject->getMaxPricePosible($productByCountry->getSerial_pro(), $filterDays, $serialCountry);
				
				if ($serial_ppc) {
					$priceObject->setSerial_ppc($serial_ppc);
					$priceObject->getData();
					$priceObject = get_object_vars($priceObject);

					$closestDurationPrice = $priceObject['price_ppc'];
					$closestDurationCost = $priceObject['cost_ppc'];
					$closestDurationRangeMax = $priceObject['max_ppc'];
					$closestDurationRangeMin = $priceObject['min_ppc'];
					$closestDurationPXC = $priceObject['serial_pxc'];

					$closestDurationPrice = number_format(round($closestDurationPrice, 2), 2, '.', '');
					$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');

					$closestDuration = $priceObject['duration_ppc'];
					$productExtraDays = $targetDays - $closestDuration;
					$productBasePrice = $closestDurationPrice;
					$productBaseCost = $closestDurationCost;
				} else {//If there's not a fee, the operator can't make the sale for tha amount of days given.
					return "NO_PRICES";
				}
			}
            
        }//END OF ALGORYTHM
        //////////////////////////////////
        // 	WE NOW CALCULATE THE PRICE	//
        //////////////////////////////////
        //FIRST DECALRE SOME USEFUL VARIABLES:
        $maxExtras = $product->getMax_extras_pro();
        $singleProduct['max_extras'] = $maxExtras;
        $spouseTraveling = $checkPartner == 'true' ? true : false;

        //IF PRICE IS PER DAY, THEN MULTIPLY: -- DISABLED --
        if ($product->getPrice_by_day_pro() == 'YES') {
            //$productBasePrice *= $targetDays;
        }

        //WE GET THE PRICE PER EXTRA DAY:
        $productExtraDaysPrice = 0;
        if ($productExtraDays > 0) {
            $productExtraDaysPrice = $productByCountry->getAditional_day_pxc();
            $productBasePrice += ( $productExtraDays * $productExtraDaysPrice);
            $productBaseCost += ( $productExtraDays * $productExtraDaysPrice);
        }

        $prices = GlobalFunctions::productCalculatePrice($db, $productBasePrice, $productBaseCost, $product->getAdults_pro(), $productByCountry->getPercentage_extras_pxc(), $txtOverAge, $product->getChildren_pro(), $productByCountry->getPercentage_children_pxc(), $txtUnderAge, $spouseTraveling, $productByCountry->getPercentage_spouse_pxc(), $maxExtras, $extrasRestricted);
        //Debug:
        //echo 'final: <pre>';print_r($prices);echo '</pre>';
        //PRODUCT PRICE ASSIGNMENT:
        $singleProduct['price_pod'] = $prices['holderPrice'];
        $singleProduct['price_children_pod'] = $prices['childrenPrice'];
        $singleProduct['price_spouse_pod'] = $prices['spousePrice'];
        $singleProduct['price_extra_pod'] = $prices['extrasPrice'];

        $singleProduct['cost_pod'] = $prices['holderCost'];
        $singleProduct['cost_children_pod'] = $prices['childrenCost'];
        $singleProduct['cost_spouse_pod'] = $prices['spouseCost'];
        $singleProduct['cost_extra_pod'] = $prices['extrasCost'];

        $singleProduct['cards_pod'] = $prices['numCards'];
        $singleProduct['total_price_pod'] = $prices['totalPrice'];
        $singleProduct['days'] = $closestDuration + $productExtraDays;
        return $productTotalCost;
    }


	public function getSerial_pos() {
		return $this->serial_pos;
	}

	public function setSerial_pos($serial_pos) {
		$this->serial_pos = $serial_pos;
	}

	public function getSerial_dea() {
		return $this->serial_dea;
	}

	public function setSerial_dea($serial_dea) {
		$this->serial_dea = $serial_dea;
	}

	public function getUsername_pos() {
		return $this->username_pos;
	}

	public function setUsername_pos($username_pos) {
		$this->username_pos = $username_pos;
	}

	public function getPassword_pos() {
		return $this->password_pos;
	}

	public function setPassword_pos($password_pos) {
		$this->password_pos = $password_pos;
	}

	public function getIp_pos() {
		return $this->ip_pos;
	}

	public function setIp_pos($ip_pos) {
		$this->ip_pos = $ip_pos;
	}

	public function getStatus_pos() {
		return $this->status_pos;
	}

	public function setStatus_pos($status_pos) {
		$this->status_pos = $status_pos;
	}

}
?>
