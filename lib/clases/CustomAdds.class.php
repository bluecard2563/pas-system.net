<?php

/*
  Document   : Alert.class.php
  Created on : Apr 16, 2010, 10:58:24 AM
  Author     : H3Dur4k
  Description:
 */

class CustomAdds {

	var $db;
	var $id;
	var $createdBy;
	var $title;
	var $image;
	var $description;
	var $webLink;
	var $creationDate;
	var $publishDate;
	var $expireDate;
	var $pdfFile;

	function __construct($db, $id=NULL) {
		$this->db = $db;
		$this->id = $id;
		
		if($this->id){
			$this->getData();
		}
	}

	/*	 * *********************************************
	 * function getData
	  @Name: getData
	  @Description: Returns all data
	  @Params:
	 *       N/A
	  @Returns: true
	 *        false
	 * ********************************************* */

	function getData() {
		if ($this->id != NULL) {
			$sql = "SELECT	id, 
							createdBy, 
							title, 
							image, 
							description, 
							webLink, 
							DATE_FORMAT(creationDate, '%d/%m/%Y') AS 'creationDate',
							DATE_FORMAT(publishDate, '%d/%m/%Y') AS 'publishDate',
							DATE_FORMAT(expireDate, '%d/%m/%Y') AS 'expireDate',
							pdfFile
					FROM custom_adds
					WHERE id ='" . $this->id . "'";

			$result = $this->db->getRow($sql);
			if ($result === false)
				return false;

			if ($result['id']) {
				$this->createdBy = $result['createdBy'];
				$this->title = $result['title'];
				$this->image = $result['image'];
				$this->description = $result['description'];
				$this->webLink = $result['webLink'];
				$this->creationDate = $result['creationDate'];
				$this->publishDate = $result['publishDate'];
				$this->expireDate = $result['expireDate'];
				$this->pdfFile = $result['pdfFile'];
				return true;
			}
		}
		
		return false;
	}

	/*	 * *********************************************
	 * function insert
	 * inserts a new register in DB
	 * ********************************************* */

	function insert() {
		$sql = "
            INSERT INTO custom_adds (
						id, 
						createdBy, 
						title, 
						image, 
						description, 
						webLink, 
						creationDate, 
						publishDate, 
						expireDate, 
						pdfFile
			) VALUES (
                NULL,
				".$this->createdBy.",
				'" . $this->title . "',
				'" . $this->image . "',
				'" . $this->description . "',
				'" . $this->webLink . "',
				CURRENT_TIMESTAMP(),";
				$sql.= ( $this->publishDate == NULL) ? 'NULL,' : "STR_TO_DATE('" . $this->publishDate . "','%d/%m/%Y'),";
				$sql.= ( $this->expireDate == NULL) ? 'NULL,' : "STR_TO_DATE('" . $this->expireDate . " 23:59:59', '%d/%m/%Y %H:%i:%s'),";
				$sql.="'" . $this->pdfFile . "')";
           
		$result = $this->db->Execute($sql);

		if ($result) {
			$this->id = $this->db->insert_ID();
			return $this->getData();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion update
	 * updates a register in DB
	 * ********************************************* */

	function update() {
		$sql = "UPDATE custom_adds
				SET title = '" . specialchars($this->title) . "',
					image = '" . $this->image . "',
					description = '" . $this->description . "',
					webLink = '" . $this->webLink . "',
					pdfFile = '" . $this->pdfFile . "',";
		( $this->publishDate == NULL) ? null : $sql.= " publishDate = STR_TO_DATE('" . $this->publishDate . "','%d/%m/%Y'),";
		( $this->expireDate == NULL) ? null : $sql.= " expireDate = STR_TO_DATE('" . $this->expireDate . " 23:59:59', '%d/%m/%Y %H:%i:%s')";
		$sql .= " WHERE id='" . $this->id . "'";
                
		$result = $this->db->Execute($sql);

		if ($result) {
			return true;
		} else {
			ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
			return false;
		}
	}
	
	/**
	 * 
	 * @param type $db ADODB
	 * @param ENUM $type [ALL,CURRENT,UPCOMING,EXPIRED]
	 * @return boolean
	 * @return ARRAY Result Set
	 */
	public static function getCustomAdds($db, $type = 'ALL'){
		$sql = "SELECT ca.*, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'creatorName'
				FROM custom_adds ca
				JOIN user u ON u.serial_usr = ca.createdBy";
		
		switch($type){
			case 'CURRENT':
				$sql .= " WHERE CURRENT_TIMESTAMP() BETWEEN publishDate AND expireDate";
			case 'UPCOMING':
				$sql = " WHERE CURRENT_TIMESTAMP() < publishDate";
			case 'EXPIRED':
				$sql = " WHERE CURRENT_TIMESTAMP() > expireDate";
			default:
		}
		
		$result = $db-> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getActivePopUpForManager($db, $serial_mbc){
		$sql = "SELECT ca.*
				FROM custom_adds ca
				JOIN adds_scope ass ON ass.addsID = ca.id 
					AND ass.addType = 'MANAGER' 
					AND ass.serial_mbc = $serial_mbc
				AND CURRENT_TIMESTAMP() BETWEEN publishDate AND expireDate";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getActivePopUpForResponsible($db, $serial_usr){
		$sql = "SELECT ca.*
				FROM custom_adds ca
				JOIN adds_scope ass ON ass.addsID = ca.id 
					AND ass.addType = 'RESPONSIBLE' 
					AND ass.responsibleID = $serial_usr
					AND CURRENT_TIMESTAMP() BETWEEN publishDate AND expireDate";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	function getId() {
		return $this->id;
	}

	function getCreatedBy() {
		return $this->createdBy;
	}

	function getTitle() {
		return $this->title;
	}

	function getImage() {
		return $this->image;
	}

	function getDescription() {
		return $this->description;
	}

	function getWebLink() {
		return $this->webLink;
	}

	function getCreationDate() {
		return $this->creationDate;
	}

	function getPublishDate() {
		return $this->publishDate;
	}

	function getExpireDate() {
		return $this->expireDate;
	}

	function getPdfFile() {
		return $this->pdfFile;
	}

	function setId($id) {
		$this->id = $id;
	}

	function setCreatedBy($createdBy) {
		$this->createdBy = $createdBy;
	}

	function setTitle($title) {
		$this->title = utf8_decode($title);
	}

	function setImage($image) {
		$this->image = utf8_decode($image);
	}

	function setDescription($description) {
		$this->description = utf8_decode($description);
	}

	function setWebLink($webLink) {
		$this->webLink = utf8_decode($webLink);
	}

	function setCreationDate($creationDate) {
		$this->creationDate = $creationDate;
	}

	function setPublishDate($publishDate) {
		$this->publishDate = $publishDate;
	}

	function setExpireDate($expireDate) {
		$this->expireDate = $expireDate;
	}

	function setPdfFile($pdfFile) {
		$this->pdfFile = utf8_decode($pdfFile);
	}

}

?>