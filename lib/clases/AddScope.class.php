<?php

/*
  Document   : Alert.class.php
  Created on : Apr 16, 2010, 10:58:24 AM
  Author     : H3Dur4k
  Description:
 */

class AddScope {

	var $db;
	var $id;
	var $addsID;
	var $responsibleID;
	var $serial_mbc;
	var $addType;

	function __construct($db, $id=NULL) {
		$this->db = $db;
		$this->id = $id;
		
		if($this->id){
			$this->getData();
		}
	}

	/*	 * *********************************************
	 * function getData
	  @Name: getData
	  @Description: Returns all data
	  @Params:
	 *       N/A
	  @Returns: true
	 *        false
	 * ********************************************* */

	function getData() {
		if ($this->id != NULL) {
			$sql = "SELECT	id, 
							responsibleID, 
							addsID, 
							serial_mbc, 
							addType
					FROM adds_scope
					WHERE id ='" . $this->id . "'";

			$result = $this->db->getRow($sql);
			if ($result === false)
				return false;

			if ($result['id']) {
				$this->responsibleID = $result['responsibleID'];
				$this->addsID = $result['addsID'];
				$this->serial_mbc = $result['serial_mbc'];
				$this->addType = $result['addType'];
				
				return true;
			}
		}
		
		return false;
	}

	/*	 * *********************************************
	 * function insert
	 * inserts a new register in DB
	 * ********************************************* */

	function insert() {
		$sql = "
            INSERT INTO adds_scope (
						id, 
						responsibleID, 
						addsID, 
						serial_mbc, 
						addType
			) VALUES (
                NULL,";
		$sql.= ( $this->responsibleID == NULL) ? "NULL," : $this->responsibleID . ",";
		$sql .= "'" . $this->addsID . "',";
		$sql.= ( $this->serial_mbc == NULL) ? "NULL," : $this->serial_mbc . ",";
		$sql .= "'" . $this->addType . "')";

		//Debug::print_r($sql); die;
		$result = $this->db->Execute($sql);

		if ($result) {
			$this->id = $this->db->insert_ID();
			return $this->getData();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}
	
	public static function removeAllScope($db, $addsID){
		$sql = "DELETE FROM adds_scope WHERE addsID = $addsID";
		
		$result = $db->Execute($sql);

		if ($result) {
			return true;
		}else{
			return false;
		}
	}
	
	public static function getScopeForAdd($db, $addsID){
		$sql = "SELECT * FROM adds_scope WHERE addsID = $addsID";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	function getId() {
		return $this->id;
	}

	function getAddsID() {
		return $this->addsID;
	}

	function getResponsibleID() {
		return $this->responsibleID;
	}

	function getSerial_mbc() {
		return $this->serial_mbc;
	}

	function getAddType() {
		return $this->addType;
	}

	function setId($id) {
		$this->id = $id;
	}

	function setAddsID($addsID) {
		$this->addsID = $addsID;
	}

	function setResponsibleID($responsibleID) {
		$this->responsibleID = $responsibleID;
	}

	function setSerial_mbc($serial_mbc) {
		$this->serial_mbc = $serial_mbc;
	}

	function setAddType($addType) {
		$this->addType = $addType;
	}
}

?>