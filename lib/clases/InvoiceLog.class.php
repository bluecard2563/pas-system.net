<?php
/*
File: InvoiceLog.class.php
Author: Santiago Benitez
Creation Date: 08/04/2010
Last Modified: 08/04/2010
*/

class InvoiceLog{
	var $db;
	var $serial_inl;
	var $serial_inv;
	var $serial_usr;
	var $usr_serial_usr;
	var $serial_sal;
	var $in_date_inl;
	var $modification_date_inl;
	var $status_inl;
	var $penalty_fee_inl;
	var $observation_inl;
	var $void_sales_inl;
	var $authorizing_obs_inl;

	function __construct($db, $serial_inl=NULL, $serial_inv=NULL, $serial_usr=NULL, $usr_serial_usr=NULL,
						$serial_sal=NULL, $in_date_inl=NULL, $modification_date_inl=NULL, $status_inl=NULL,
						$penalty_fee_inl=NULL, $observation_inl=NULL, $void_sales_inl=NULL, $authorizing_obs_inl=NULL){
		$this -> db = $db;
		$this -> serial_inl = $serial_inl;
		$this -> serial_inv = $serial_inv;
		$this -> serial_usr = $serial_usr;
		$this -> usr_serial_usr = $usr_serial_usr;
		$this -> serial_sal = $serial_sal;
		$this -> in_date_inl = $in_date_inl;
		$this -> modification_date_inl = $modification_date_inl;
		$this -> status_inl = $status_inl;
		$this -> penalty_fee_inl = $penalty_fee_inl;
		$this -> observation_inl = $observation_inl;
		$this -> void_sales_inl = $void_sales_inl;
		$this -> authorizing_obs_inl = $authorizing_obs_inl;

	}      

	/***********************************************
    * funcion getBenefits
    * gets all the Benefits of the system
    ***********************************************/
	function getInvoiceLogs($status=NULL){
		if($this->serial_inv!=NULL){
			$sql = 	"SELECT serial_inl,
							serial_usr,
							usr_serial_usr,
							serial_inv,
							serial_sal,
							observation_inl,
							in_date_inl,
							modification_date_inl,
							status_inl,
							penalty_fee_inl,
							void_sales_inl,
							authorizing_obs_inl
					 FROM invoice_log
					 WHERE serial_inv = {$this->serial_inv}";
			if($status){
				$sql.=" AND status_inl='".$status."'";
			}
			$sql.=" ORDER BY serial_inl ";

			$result = $this->db -> getAll($sql);

			if($result){
				return $result;
			}else{
				return FALSE;
			}
		}
		else{
			return false;
		}
	}


        /***********************************************
        * function changeStatus
        * change the status of an specific invoice log
        ***********************************************/
        function changeStatus($status){
            $sql="UPDATE invoice_log SET status_inl = '$status',
                         modification_date_inl = NOW(),
						 usr_serial_usr = {$this -> usr_serial_usr},
						 authorizing_obs_inl = '{$this -> authorizing_obs_inl}'
                  WHERE status_inl='PENDING' AND serial_inv = {$this -> serial_inv}";
            //die($sql);
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result==true)
                return true;
            else
                return false;
        }


	/***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO invoice_log (
				serial_inl,
				serial_inv,
				serial_usr,
				usr_serial_usr,
				serial_sal,
				in_date_inl,
				modification_date_inl,
				status_inl,
				penalty_fee_inl,
				observation_inl,
				void_sales_inl,
				authorizing_obs_inl)
            VALUES (
				NULL,
				'".$this->serial_inv."',
				'".$this->serial_usr."',
				NULL,
				'".$this->serial_sal."',
				NOW(),
				NULL,
				'PENDING',
				'".$this->penalty_fee_inl."',
				'".$this->observation_inl."',
				'".$this->void_sales_inl."',
				NULL)";
		
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

        /***********************************************
        * funcion getInvoices
        * gets information from Invoice
        ***********************************************/
	function getInvoiceLogsByManager($serial_mbc,$type_dbm){
		$sql = 	"SELECT i.serial_inv, i.number_inv, dea.name_dea, DATE_FORMAT(i.date_inv,'%d/%m/%Y') as date_inv, DATE_FORMAT(i.due_date_inv,'%d/%m/%Y') as due_date_inv, i.total_inv, CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as customer_name,
				CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as user_name, DATE_FORMAT(inl.in_date_inl,'%d/%m/%Y %H:%i:%s') as in_date_inl, inl.serial_inl
				FROM dealer dea
				JOIN dealer de ON de.dea_serial_dea = dea.serial_dea
				JOIN counter cnt ON cnt.serial_dea=de.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN invoice i ON i.serial_inv=sal.serial_inv
				JOIN invoice_log inl ON i.serial_inv=inl.serial_inv AND inl.status_inl = 'PENDING'
				JOIN user usr ON inl.serial_usr=usr.serial_usr
				JOIN document_by_manager dbm ON i.serial_dbm=dbm.serial_dbm AND (dbm.type_dbm='BOTH' OR dbm.type_dbm='".$type_dbm."')
				LEFT JOIN customer cus ON cus.serial_cus=i.serial_cus
				WHERE dea.serial_mbc='".$serial_mbc."'
				GROUP BY i.serial_inv
				ORDER BY i.number_inv";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}

	/*******************************************
	@Name: getInvoiceSales
	@Description: Returns an array of all the sales of an invoice.
	@Params: serial invoice.
	@Returns: sales array
	********************************************/
	function getVoidInvoiceSales($serial_inv, $serial_sal=NULL){
			$sql = "SELECT DISTINCT sal.card_number_sal, CONCAT(IFNULL(cus.last_name_cus,''),' ',cus.first_name_cus) as customer_name, pbl.name_pbl, sal.total_sal,
						   inv.discount_prcg_inv, inv.other_dscnt_inv, inv.applied_taxes_inv, pbc.serial_pxc, sal.serial_sal
					FROM invoice_log il
					JOIN invoice inv ON inv.serial_inv=il.serial_inv
					JOIN sales as sal ON il.serial_sal=sal.serial_sal
					JOIN customer as cus ON cus.serial_cus=sal.serial_cus
					JOIN product_by_dealer as pbd ON pbd.serial_pbd=sal.serial_pbd
					JOIN product_by_country as pbc ON pbc.serial_pxc=pbd.serial_pxc
					JOIN product as pro ON pro.serial_pro=pbc.serial_pro
					JOIN product_by_language as pbl ON pbl.serial_pro=pro.serial_pro AND pbl.serial_lang=".$_SESSION['serial_lang']."
					WHERE il.serial_inv = '$serial_inv'";

			if($serial_sal!=NULL){
				$sql.=" AND sal.serial_sal = '".$serial_sal."'";
			}

			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
					$arreglo=array();
					$cont=0;

					do{
							$arreglo[$cont]=$result->GetRowAssoc(false);
							$result->MoveNext();
							$cont++;
					}while($cont<$num);
			}

			return $arreglo;
	}

	public static function getSerialLogByInvoice($db, $serial_inv){
		$sql = "SELECT serial_inl
				FROM invoice_log il
				WHERE serial_inv='$serial_inv'
				AND status_inl='AUTHORIZED'";

		//echo ($sql);
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public static function getLogsForAlertReport($db, $serial_mbc, $begin_date, $end_date, $status_slg){
		$sql = "SELECT IFNULL(sal.card_number_sal, trl.card_number_trl) as card_number,
						DATE_FORMAT(inl.in_date_inl, '%d/%m/%Y') AS date_inl,
						IF(modification_date_inl IS NULL, 'N/A', DATE_FORMAT(modification_date_inl, '%d/%m/%Y')) AS 'authorizing_date',
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr,
						inl.observation_inl,
						IFNULL(inl.authorizing_obs_inl, 'N/A') AS 'authorizing_obs_inl',
						IF(sal.begin_date_sal <= in_date_inl,'YES','NO') as start_coverage,
						inl.status_inl, CONCAT(usr2.first_name_usr,' ',usr2.last_name_usr) as usr_name_usr,
						i.number_inv
				FROM invoice_log inl
				JOIN invoice i ON i.serial_inv = inl.serial_inv
				JOIN sales sal ON sal.serial_sal = inl.serial_sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.serial_mbc = $serial_mbc
				JOIN user usr ON usr.serial_usr = inl.serial_usr
				JOIN user usr2 ON usr2.serial_usr = inl.usr_serial_usr
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
				WHERE inl.in_date_inl BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$end_date', '%d/%m/%Y'), INTERVAL 1 DAY)";

		if($status_slg) {
			$sql.=" AND inl.status_inl = '$status_slg'";
		}
		$sql.=" ORDER BY number_inv, serial_inl";
		//die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);

		if($result) {
			return $result;
		} else {
			return false;
		}
	}
	
	public static function didSaleHaveInvoice($db, $serial_sal){
		$sql = "SELECT serial_inl
				FROM invoice_log
				WHERE serial_sal = $serial_sal
				AND status_inl = 'AUTHORIZED'";
		
		$result = $db->getOne($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_inl(){
		return $this->serial_inl;
	}
	function getSerial_inv(){
		return $this->serial_inv;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getUsr_serial_usr(){
		return $this->usr_serial_usr;
	}
	function getSerial_sal(){
		return $this->serial_sal;
	}
	function getIn_date_inl(){
		return $this->in_date_inl;
	}
	function getStatus_inl(){
		return $this->status_inl;
	}
	function getModification_date_inl(){
		return $this->modification_date_inl;
	}
	function getPenalty_fee_inl(){
		return $this->penalty_fee_inl;
	}
	function getObservation_inl(){
		return $this->observation_inl;
	}
	function getVoid_sales_inl(){
		return $this->void_sales_inl;
	}


	///SETTERS
	function setSerial_inl($serial_inl){
		$this->serial_inl = $serial_inl;
	}
	function setSerial_inv($serial_inv){
		$this->serial_inv = $serial_inv;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setUsr_serial_usr($usr_serial_usr){
		$this->usr_serial_usr = $usr_serial_usr;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal = $serial_sal;
	}
	function setIn_date_inl($in_date_inl){
		$this->in_date_inl = $in_date_inl;
	}
	function setModification_date_inl($modification_date_inl){
		$this->modification_date_inl = $modification_date_inl;
	}
	function setStatus_inl($status_inl){
		$this->status_inl = $status_inl;
	}
	function setPenalty_fee_inl($penalty_fee_inl){
		$this->penalty_fee_inl = $penalty_fee_inl;
	}
	function setObservation_inl($observation_inl){
		$this->observation_inl = $observation_inl;
	}
	function setVoid_sales_inl($void_sales_inl){
		$this->void_sales_inl=$void_sales_inl;
	}
	public function getAuthorizing_obs_inl() {
	 return $this->authorizing_obs_inl;
	}
	public function setAuthorizing_obs_inl($authorizing_obs_inl) {
	 $this->authorizing_obs_inl = $authorizing_obs_inl;
	}
}
?>