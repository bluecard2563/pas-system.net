<?php

/*
  File: ServiceyLanguage.class.php
  Author: Edwin Salvador.
  Creation Date: 11/02/2010
  Last Modified:04/03/2010
  Modified By:
 */

class ServicesbyLanguage {

	var $db;
	var $serial_sbl;
	var $serial_lang;
	var $serial_ser;
	var $name_sbl;
	var $description_sbl;

	function __construct($db, $serial_sbl = NULL, $serial_lang = NULL, $serial_ser = NULL, $name_sbl = NULL, $description_sbl = NULL) {
		$this->db = $db;
		$this->serial_sbl = $serial_sbl;
		$this->serial_lang = $serial_lang;
		$this->serial_ser = $serial_ser;
		$this->name_ser = $name_ser;
		$this->description_sbl = $description_sbl;
	}

	/*	 * *********************************************
	 * function getData
	 * sets data by serial_sbl
	 * ********************************************* */

	function getData() {
		if ($this->serial_sbl != NULL) {
			$sql = "SELECT serial_sbl, serial_lang, serial_ser, name_sbl, description_sbl
                    FROM services_by_language
                    WHERE serial_sbl='" . $this->serial_sbl . "'";

			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_sbl = $result->fields[0];
				$this->serial_lang = $result->fields[1];
				$this->serial_ser = $result->fields[2];
				$this->name_sbl = $result->fields[3];
				$this->description_sbl = $result->fields[4];
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion serviceNameExists
	 * verifies if a service name exists or not
	 * ********************************************* */

	function serviceNameExists($name_sbl, $serial_lang, $serial_ser = NULL) {
		$sql = "SELECT s.serial_ser
               FROM services s
               JOIN services_by_language sbl ON s.serial_ser=sbl.serial_ser AND sbl.serial_lang = '" . $serial_lang . "'
               WHERE LOWER(sbl.name_sbl)= _utf8'" . utf8_encode($name_sbl) . "' collate utf8_bin";
		if ($serial_ser != NULL) {
			$sql.=" AND s.serial_ser <> " . $serial_ser;
		}

		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion serviceCodeExists
	 * verifies if a service code exists or not
	 * ********************************************* */

	function serviceCodeExists($name_ser, $serial_lang, $serial_ser = NULL) {
		$sql = "SELECT s.serial_ser
               FROM services s
               WHERE LOWER(s.name_ser)= _utf8'" . utf8_encode($name_ser) . "' collate utf8_bin";
		if ($serial_ser != NULL) {
			$sql.=" AND s.serial_ser <> " . $serial_ser;
		}

		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/**
	  @Name: getTranslations
	  @Description: Returns an array of all the translations for a specific Service
	  @Params: $serial_ser: Service ID
	  @Returns: An array of translations
	 * */
	function getTranslations($serial_ser) {
		$sql = "SELECT sbl.serial_sbl as 'serial', sbl.name_sbl, sbl.description_sbl, l.name_lang, sbl.serial_sbl as 'update', l.serial_lang
                 FROM services_by_language sbl
                 JOIN language l ON l.serial_lang = sbl.serial_lang
                 WHERE sbl.serial_ser = '" . $serial_ser . "'";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arr;
	}

	/*	 * *********************************************
	 * function insert
	 * inserts a new register in DB 
	 * ********************************************* */

	function insert() {
		$sql = "INSERT INTO services_by_language (
				serial_sbl,
				serial_lang,
				serial_ser,
                                name_sbl,
				description_sbl
                               )
                VALUES (
                        NULL,
			'" . $this->serial_lang . "',				
			'" . $this->serial_ser . "',
                        '" . $this->name_sbl . "',
			'" . $this->description_sbl . "'
                )";
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion update
	 * updates a register in DB
	 * ********************************************* */

	function update() {
		$sql = "UPDATE services_by_language SET
                name_sbl ='" . $this->name_sbl . "',
		description_sbl ='" . $this->description_sbl . "'
		WHERE serial_ser = '" . $this->serial_ser . "'
                AND serial_lang = '" . $this->serial_lang . "'";
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;

		if ($result == true) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion getReportByServices
	 * get Report By Services
	 * ********************************************* */

	public static function getReportByServices($db, $beginDate, $enddate, $serial_sbl = NULL, $serial_cou = NULL, $serial_mbc = NULL, $serial_dea = NULL) {
		if ($serial_sbl) {
			$service_restriction = " AND sbl.serial_sbl = '$serial_sbl'";
		}
		if ($serial_cou) {
			$country_restriction = " AND sbc.serial_cou = '$serial_cou'";
		}
		if ($serial_mbc) {
			$mbc_restriction = " AND dea.serial_mbc = '$serial_mbc'";
		}
		if ($serial_dea) {
			$dealer_restriction = " AND sbd.serial_dea = '$serial_dea'";
		}

		$sql = "SELECT sal.card_number_sal, sal.in_date_sal, CONCAT(cus.first_name_cus,' ', cus.last_name_cus) as name, 
						cus.document_cus, sal.begin_date_sal, sal.end_date_sal, cou.name_cou as country, sal.emission_date_sal,
						dea.name_dea, m.name_man, sbl.name_sbl
				FROM sales  sal
                JOIN customer cus ON sal.serial_cus = cus.serial_cus
                JOIN city cit ON cit.serial_cit = sal.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                JOIN services_by_country_by_sale sbs ON sal.serial_sal = sbs.serial_sal
                JOIN services_by_dealer sbd ON sbd.serial_sbd = sbs.serial_sbd
					$dealer_restriction
				JOIN dealer dea ON dea.serial_dea = sbd.serial_dea
					$mbc_restriction
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
                JOIN services_by_country sbc ON sbc.serial_sbc = sbd.serial_sbc
					$country_restriction
                JOIN services ser ON ser.serial_ser = sbc.serial_ser
                JOIN services_by_language sbl ON sbl.serial_ser = ser.serial_ser 
					$service_restriction
                WHERE sal.emission_date_sal BETWEEN 
											STR_TO_DATE('$beginDate','%d/%m/%Y') AND 
											STR_TO_DATE('$enddate', '%d/%m/%Y')";
		//Debug::print_r($sql); die();
		$result = $db->getAll($sql);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_sbl() {
		return $this->serial_sbl;
	}

	function getSerial_lang() {
		return $this->serial_lang;
	}

	function getSerial_ser() {
		return $this->serial_ser;
	}

	function getName_sbl() {
		return $this->name_sbl;
	}

	function getDescription_sbl() {
		return $this->description_sbl;
	}

	///SETTERS

	function setSerial_sbl($serial_sbl) {
		$this->serial_sbl = $serial_sbl;
	}

	function setSerial_lang($serial_lang) {
		$this->serial_lang = $serial_lang;
	}

	function setSerial_ser($serial_ser) {
		$this->serial_ser = $serial_ser;
	}

	function setName_sbl($name_sbl) {
		$this->name_sbl = $name_sbl;
	}

	function setDescription_sbl($description_sbl) {
		$this->description_sbl = $description_sbl;
	}
}
?>