<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TravelMotives
 *
 * @author Valeria Andino C�lleri
 */
class WSTravelMotives {
 
    			
	var $db;
	var $serial_trm;
        var $name_trm;
	var $status_trm;
	
	
	
	function __construct($db, $serial_trm = NULL, $name_trm=NULL, $status_trm = NULL){
		$this -> db = $db;
		$this -> serial_trm = $serial_trm;
		$this -> name_trm = $name_trm;
		$this -> status_trm = $status_trm;
		
	}
	
	/***********************************************
        * function getData
        * gets data by serial_trm
        ***********************************************/
        
	function getData(){
		if($this->serial_trm!=NULL){
			$sql = 	"SELECT serial_trm, name_trm, status_trm
					FROM travel_motives
					WHERE serial_trm='".$this->serial_trm."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;
			if($result->fields[0]){
				$this->serial_trm=$result->fields[0];
                                $this->name_trm=$result->fields[1];
				$this->status_trm=$result->fields[2];
				
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
        
        /***********************************************
        * function insert
        * insert a register in DB
        ***********************************************/
        
        function insert(){
		$sql="INSERT INTO travel_motives (
					serial_trm,
                                        name_trm,
					status_trm,
					)
			  VALUES(NULL,";
                                        $sql.="'".$this->name_trm."',
					'".$this->status_trm."";
                                        
                           
                            $result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
        
	function update(){
		$sql="UPDATE travel_motives
		SET name_trm='".$this->name_trm."',
			status_trm='".$this->status_trm."',";
               
		$sql.=" WHERE serial_trm='".$this->serial_trm."'";
		
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result==true) 
			return true;
		else
			return false;
	}
        
        /***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
        
        public static function getAllTravelMotives($db,$all=true){
            
		$sql = 	"SELECT t.serial_trm, t.name_trm
                 FROM travel_motives t";
			//echo ($sql); die();	 
			
		$result=$db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
}
