<?php
/*
File: News.php
Author: Santiago Borja Lopez
Creation Date: 16/02/2010 10:00
Last Modified: 23/02/2010
Modified By: Santiago Borja
*/
class News{				
	var $db;
	var $serial_new;
	var $serial_lang;
	var $new_serial_new;
	var $title_new;
	var $description_new;
	var $date_new;
		
	function __construct($db, $serial_new=NULL, $serial_lang = NULL, $new_serial_new=NULL, $title_new=NULL, $description_new=NULL, $date_new=NULL){
		$this -> db = $db;
		$this -> serial_new = $serial_new;
		$this -> serial_lang = $serial_lang;
		$this -> new_serial_new = $new_serial_new;
		$this -> title_new = $title_new;
		$this -> description_new = $description_new; 
		$this -> date_new = $date_new;
	}
	
	/***********************************************
    * funcion getData
    * sets data by serial_new
    ***********************************************/
	function getData(){
		if($this->serial_new!=NULL){
				$sql = 	"	SELECT n.serial_new, n.serial_lang, n.new_serial_new, n.title_new, n.description_new, n.date_new, a.name_lang as language
							FROM news n
							LEFT JOIN language a ON n.serial_lang = a.serial_lang
							WHERE n.serial_new = ".$this-> serial_new;
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_new =$result->fields[0];
				$this -> serial_lang = $result->fields[1];
				$this -> new_serial_new = $result->fields[2];
				$this -> title_new = $result->fields[3];
				$this -> description_new = $result->fields[4];
				$this -> date_new = $result->fields[5];
				$this -> serial_language = $result->fields[6];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * funcion getAllNews
    * gets information from All News
    ***********************************************/
    function getAllNews(){
        $sql = 	"SELECT u.serial_new, u.serial_lang, u.new_serial_new,u.title_new, u.description_new, UNIX_TIMESTAMP(u.date_new) AS date_new FROM news u ";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getMyLanguageNewsPaginated
    * gets information from All News in the language specified and using the pagination feature
    ***********************************************/
    function getMyLanguageNewsPaginated($language, $currentIndex, $limit){
        $sql = 	"SELECT SQL_CALC_FOUND_ROWS u.serial_new, u.title_new AS title_new, u.description_new AS description_new, UNIX_TIMESTAMP(u.date_new) AS date_new 
				FROM news u, language a 
				WHERE u.serial_lang = a.serial_lang AND a.code_lang = '".$language."' ORDER BY u.date_new DESC LIMIT ".$currentIndex.",".$limit;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        
        $sql = 	"SELECT FOUND_ROWS() as total";
        $result = $this->db -> Execute($sql);
        $totalRows = $result->fields[0];
        SmartyPaginate::setTotal($totalRows);
        return $arr;
    }
    
	/***********************************************
    * funcion getMyLanguageNews
    * gets information from the News in the language specified
    ***********************************************/
    function getMyLanguageNews($language){
        $sql = 	"SELECT u.serial_new, u.title_new AS title_new, u.description_new AS description_new, UNIX_TIMESTAMP(u.date_new) AS date_new 
				FROM news u, language a 
				WHERE u.serial_lang = a.serial_lang AND a.code_lang = '".$language."' ORDER BY u.date_new DESC LIMIT 0,3";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getMyTranslatedLanguages
    * gets information from the News in the language specified
    ***********************************************/
    function getMyTranslatedLanguages(){
        $sql = 	"SELECT serial_lang FROM news WHERE new_serial_new = ".$this -> serial_new;
        $result = $this-> db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr="";
            $cont=0;
            do{
                $temp_rec = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
                $arr.= $temp_rec['serial_lang'].($cont<$num?",":"");
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getShortText
    * retrieves a short version of the text field
    ***********************************************/
    public static function getShortText($textField,$shortLength){
    	return  strlen($textField)>$shortLength?substr($textField,0,$shortLength).'...':$textField;
    }
    
	/***********************************************
    * funcion getAllNewsTree
    * gets information from All News as a tree where translated news are children of the original (not a translation) News
    ***********************************************/
    function getAllNewsTree(){
        $sql = 	"SELECT serial_new, serial_lang, new_serial_new, title_new, description_new, UNIX_TIMESTAMP(date_new) AS date_new FROM news WHERE new_serial_new IS NULL ";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                
                //Get a short version of the title and text:
				$arr[$cont][description_new] = News::getShortText($arr[$cont][description_new],35);
				$arr[$cont][title_new] = News::getShortText($arr[$cont][title_new],35);
                
				//Get the Language name
				$language = new Language($this ->db,$arr[$cont]['serial_lang']);
				$language->getData();
				$arr[$cont][language] = $language -> getName_lang();
				
				//[Get Children] of this News
				$sqlChildren = 	"SELECT serial_new, serial_lang, new_serial_new, title_new, description_new, UNIX_TIMESTAMP(date_new) AS date_new FROM news WHERE new_serial_new = ".$arr[$cont][serial_new];
				$resultChildren = $this->db -> Execute($sqlChildren);
        		$numChildren = $resultChildren -> RecordCount();
        		if($numChildren > 0){
		            $arrChildren = array();
		            $contChildren=0;
		            do{
        				$arrChildren[$contChildren]=$resultChildren->GetRowAssoc(false);
		            	
		            	//Get a short version of the title and text:
						$arrChildren[$contChildren][description_new] = News::getShortText($arrChildren[$contChildren][description_new],35);
						$arrChildren[$contChildren][title_new] = News::getShortText($arrChildren[$contChildren][title_new],35);
		            	
						//Get the Language name
						$language = new Language($this ->db,$arrChildren[$contChildren]['serial_lang']);
						$language->getData();
						$arrChildren[$contChildren][language]=$language -> getName_lang();
						
						$resultChildren->MoveNext();
                		$contChildren++;
		            }while($contChildren<$numChildren);
		            $arr[$cont][children] = $arrChildren;
        		}
		        //End of [Get Children]
        		
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
/***********************************************
    * funcion getAllNewsTreePaginated
    * ABOVE FUNCTION USING PAGINATION
    ***********************************************/
    function getAllNewsTreePaginated($currentIndex, $limit){
        $sql = 	"SELECT SQL_CALC_FOUND_ROWS serial_new, serial_lang, new_serial_new, title_new, description_new, UNIX_TIMESTAMP(date_new) AS date_new 
        		FROM news WHERE new_serial_new IS NULL ORDER BY serial_new ASC LIMIT ".$currentIndex.",".$limit;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        
        $sqlPg = 	"SELECT FOUND_ROWS() as total";
        $resultPg = $this->db -> Execute($sqlPg);
        $totalRows = $resultPg->fields[0];
        SmartyPaginate::setTotal($totalRows);
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                
                //Get a short version of the title and text:
				$arr[$cont][title_new] = News::getShortText($arr[$cont][title_new],60);
                
				//Get the Language name
				$language = new Language($this ->db,$arr[$cont]['serial_lang']);
				$language->getData();
				$arr[$cont][language] = $language -> getName_lang();
				
				//[Get Children] of this News
				$sqlChildren = 	"SELECT serial_new, serial_lang, new_serial_new, title_new, description_new, UNIX_TIMESTAMP(date_new) AS date_new FROM news WHERE new_serial_new = ".$arr[$cont][serial_new];
				$resultChildren = $this->db -> Execute($sqlChildren);
        		$numChildren = $resultChildren -> RecordCount();
        		if($numChildren > 0){
		            $arrChildren = array();
		            $contChildren=0;
		            do{
        				$arrChildren[$contChildren]=$resultChildren->GetRowAssoc(false);
		            	
		            	//Get a short version of the title and text:
						$arrChildren[$contChildren][title_new] = News::getShortText($arrChildren[$contChildren][title_new],60);
		            	
						//Get the Language name
						$language = new Language($this ->db,$arrChildren[$contChildren]['serial_lang']);
						$language->getData();
						$arrChildren[$contChildren][language]=$language -> getName_lang();
						
						$resultChildren->MoveNext();
                		$contChildren++;
		            }while($contChildren<$numChildren);
		            $arr[$cont][children] = $arrChildren;
        		}
		        //End of [Get Children]
        		
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion removeNews
    * removes one instance of news
    ***********************************************/
    public static function removeNews($db, $serial){
        $sql = 	"DELETE FROM news WHERE serial_new = ".$serial;
        return $db -> Execute($sql);
    }
    
	/***********************************************
    * funcion linkTranslation
    * This news is no longer a translation of another news
    ***********************************************/
    function linkTranslation($serialFather){
        $sql = 	"UPDATE news SET new_serial_new = ".$serialFather." WHERE serial_new = ".$this -> serial_new;
        return $this -> db -> Execute($sql);
    }
    
	/***********************************************
    * funcion unlinkTranslation
    * This news is no longer a translation of another news
    ***********************************************/
    public static function unlinkTranslation($db, $serial){
        $sql = 	"UPDATE news SET new_serial_new = NULL WHERE serial_new = ".$serial;
        return $db -> Execute($sql);
    }
	
	/***********************************************
    * function insert
    * inserts a new register in the DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO news (
        		serial_lang,
                new_serial_new,
                title_new,
                description_new,
                date_new)
            VALUES(	'".$this->serial_lang."',
	                ".($this->new_serial_new==''?'NULL':$this->new_serial_new).",
	                '".$this->title_new."',
	                '".$this->description_new."',
	                FROM_UNIXTIME(".$this->date_new."))";
       	//echo $sql;
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in the DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE news SET";
        $sql.=" serial_lang			='".$this->serial_lang."',
        		title_new			='".$this->title_new."',
				description_new		='".$this->description_new."',
				date_new			=FROM_UNIXTIME(".$this->date_new.")".
        		($this->new_serial_new==''?'':", new_serial_new = ".$this->new_serial_new."");
        $sql .=" WHERE serial_new = '".$this->serial_new."'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        return $result;
    }
	
	///GETTERS
	function getSerial_new(){
		return $this->serial_new;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}
	function getDate_new(){
		return $this->date_new;
	}
	function getNew_serial_new(){
		return $this->new_serial_new;
	}
	function getTitle_new(){
		return $this->title_new;
	}
	function getDescription_new(){
		return $this->description_new;
	}

	///SETTERS	
	function setSerial_new($serial_new){
		$this->serial_new = $serial_new;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setDate_new($date_new){
		$this->date_new = $date_new;
	}
	function setNew_serial_new($new_serial_new){
		$this->new_serial_new = $new_serial_new;
	}
	function setTitle_new($title_new){
		$this->title_new = $title_new;
	}
	function setDescription_new($description_new){
		$this->description_new = $description_new;
	}
}
?>