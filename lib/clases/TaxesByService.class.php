<?php
/*
File: TaxesByService.class.php
Author: Miguel Ponce.
Creation Date: 17/03/2010
Last Modified: 
Modified By: 
*/

class TaxesByService{
    var $db;
    var $serial_sbc;
    var $serial_tax;
	
    function __construct($db, $serial_sbc=NULL, $serial_tax=NULL){
        $this -> db = $db;
        $this -> serial_sbc = $serial_sbc;
        $this -> serial_tax = $serial_tax;
    }
	
    /***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
    function getData(){
        if($this->serial_sbc!=NULL){
            $sql = "SELECT serial_sbc,
                           serial_tax
                    FROM taxes_by_service
                    WHERE serial_sbc='".$this->serial_sbc."'
                    AND serial_tax='".$this->serial_tax."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_sbc =$result->fields[0];
                $this -> serial_tax = $result->fields[1];

                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }
    }
	
    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO taxes_by_service (
				serial_sbc,
				serial_tax
                                )
            VALUES (
                    '".$this->serial_sbc."',
                    '".$this->serial_tax."'
                    )";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return TRUE;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: delete
    @Description: delete a register in DB
    @Params: N/A
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function delete(){
        if($this->serial_sbc != NULL){
            $sql="DELETE FROM taxes_by_service
                  WHERE serial_sbc='".$this->serial_sbc."'";

            $result = $this->db->Execute($sql);
            //echo $sql." ";
            if ($result==true)
                return true;
            else
                return false;
        }
    }

    function taxByServiceExists(){
        if($this->serial_tax != NULL && $this->serial_sbc != NULL){
            $sql = "SELECT 1
                    FROM taxes_by_service tbs
                    WHERE tbs.serial_tax = '".$this->serial_tax."'
                    AND tbs.serial_sbc = '".$this->serial_sbc."'";

            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }



    ///GETTERS
    function getSerial_sbc(){
        return $this->serial_sbc;
    }
    function getSerial_tax(){
        return $this->serial_tax;
    }

    ///SETTERS
    function setSerial_sbc($serial_sbc){
        $this->serial_sbc = $serial_sbc;
    }
    function setSerial_tax($serial_tax){
        $this->serial_tax = $serial_tax;
    }
}
?>