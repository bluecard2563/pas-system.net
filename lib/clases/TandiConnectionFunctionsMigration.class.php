<?php

class TandiConnectionFunctionsMigration
{

    const EC_MBC_ARRAY = '2,3,4,5,6,31,32,33,34,35,36,38,40';
    const EC_URL_ERP = 'http://18.234.9.61:8087/';
    const EC_USR = 'ross';
    const EC_PSWRD = 'ross';

    public static function defineERPConnectionForManager($serial_mbc)
    {
        if (in_array($serial_mbc, split(',', self::EC_MBC_ARRAY))) {
            define('URL_ERP', self::EC_URL_ERP);
            define('URL_USR', self::EC_USRD);
            define('URL_PSWD', self::EC_PSWRD);

            return TRUE;
        }
    }

    /*
      public static function getSaleDataForOO($db, $serial_sal) {
      $sql = "SELECT s.card_number_sal, s.type_sal, s.total_cost_sal, s.total_sal,
      pbd.serial_pxc, d.serial_mbc, pbl.name_pbl,
      IF(b.bill_to_dea = 'CUSTOMER', CONCAT('C', s.serial_cus), CONCAT('D', b.serial_dea)) AS 'client_id',
      end_date_sal, begin_date_sal, s.emission_date_sal, s.observations_sal, id_erp_sal,
      CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')) AS 'full_name_cus',
      u.username_usr AS 'responsible',
      DATE_FORMAT(end_date_sal, '%Y-%m-%d') AS 'endDate',
      DATE_FORMAT(begin_date_sal, '%Y-%m-%d') AS 'beginDate',
      DATE_FORMAT(s.emission_date_sal, '%Y-%m-%d') AS 'emissionDate',
      b.id_dea, s.serial_cus, cus.document_cus, cus.first_name_cus, cus.last_name_cus,
      DATE_FORMAT(birthdate_cus, '%Y-%m-%d') AS 'birthdate_cus', IF(cus.gender_cus = 'H', 'M', cus.gender_cus) as 'gender_cus',
      cus.address_cus, cus.phone1_cus, cus.email_cus, sc.sise_id,
      sg.pais_id, sg.dpto_id, sg.municipio_id
      FROM sales s
      JOIN customer cus ON cus.serial_cus = s.serial_cus
      JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
      JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
      JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = 2
      JOIN dealer d ON d.serial_dea = pbd.serial_dea
      JOIN city cit ON cit.serial_cit = cus.serial_cit
      JOIN country cou On cou.serial_cou = cit.serial_cou


      JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
      JOIN dealer b ON b.serial_dea = cnt.serial_dea
      JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
      JOIN user u ON u.serial_usr = ubd.serial_usr
      LEFT JOIN sise_country sc On sc.serial_cou = cou.serial_cou
      LEFT JOIN sise_geolocation_for_city sg On sg.serial_cit = cus.serial_cit
      WHERE s.serial_sal = $serial_sal";

      $result = $db->getRow($sql);

      if ($result) {
      return $result;
      } else {
      return FALSE;
      }
      }
     */
    /*
      public static function getInvoiceDataForOO($db, $serial, $type) {
      if ($type == 'D') {
      $sql = "SELECT serial_cdd AS 'payterm', CONCAT('D', serial_dea) AS 'client_id'
      FROM dealer d
      WHERE serial_dea = $serial";

      $result = $db->getRow($sql);
      } else {
      $result = array('client_id' => 'C' . $serial, 'payterm' => '10');
      }


      return $result;
      }
     */
    /* ++++++++++++++++OBTENER ID SUCURSAL DE TANDI A PARTIR DEL SERIAL MBC +++++++++++++++++++ */

    public static function getTandiSucursalID($serial_man)
    {
        switch ($serial_man) {
            case 2:
            case 19:
            case 20:
                $tandi_sucid = "88"; //Quito
                break;
            case 3:
                $tandi_sucid = "802684952"; //Guayaquil
                break;
            case 4:
                $tandi_sucid = "220377055233"; //Cuenca
                break;
            case 5:
                $tandi_sucid = "10724324278345"; //Quito Isla
                break;
            case 6:
                $tandi_sucid = "10724324278343"; //Guayaquil Isla
                break;
            case 21:
                $tandi_sucid = "802684955"; //Manta
                break;
            case 26:
                $tandi_sucid = "87"; //Ambato
                break;
            case 23:
                $tandi_sucid = "10724324278347"; //Loja
                break;
            case 27:
                $tandi_sucid = "10755949865177"; //Machala
                break;

            default:
                $tandi_sucid = FALSE;
        }

        return $tandi_sucid;
    }

    /* ++++++++++++++++OBTENER ID DEL PUNTO DE VENTA DE TANDI A PARTIR DEL SERIAL MBC +++++++++++++++++++ */

    public static function getTandiPtoID($serial_man)
    {
        switch ($serial_man) {
            case 2:
            case 19:
                $tandi_ptoid = "10724324278348"; //Quito
                break;
            case 3:
                $tandi_ptoid = "10724324278354"; //Guayaquil
                break;
            case 4:
                $tandi_ptoid = "10724324278362"; //Cuenca
                break;
            case 5:
                $tandi_ptoid = "10724324278352"; //Quito Isla
                break;
            case 6:
                $tandi_ptoid = "10724324278356"; //Guayaquil Isla
                break;
            case 20:
                $tandi_ptoid = "10724324278350"; //WEB
                break;
            case 21:
                $tandi_ptoid = "10724324278358"; //Manta
                break;
            case 26:
                $tandi_ptoid = "10724324278364"; //Ambato
                break;
            case 23:
                $tandi_ptoid = "10724324278360"; //Loja
                break;
            case 27:
                $tandi_ptoid = "10755949866707"; //Machala
                break;

            default:
                $tandi_ptoid = FALSE;
        }

        return $tandi_ptoid;
    }

    /* ++++++++++++++++OBTENER SERIAL MBC DEL BAS A PARTIR DEL ID DEL PUNTO DE VENTA TANDI +++++++++++++++++++ */

    public static function getBASMbc($tandi_id)
    {
        switch ($tandi_id) {
            case 10724324278348:

                $BASmbc = "2"; //Quito
                break;
            case 10724324278354:
                $BASmbc = "3"; //Guayaquil
                break;
            case 10724324278362:
                $BASmbc = "4"; //Cuenca
                break;
            case 10724324278352:
                $BASmbc = "5"; //Quito Isla
                break;
            case 10724324278356:
                $BASmbc = "6"; //Guayaquil Isla
                break;
            case 10724324278350:
                $BASmbc = "31"; //Web
                break;
            case 10724324278358:
                $BASmbc = "32"; //Manta
                break;
            case 10724324278364:
                $BASmbc = "38"; //Ambato
                break;
            case 10724324278360:
                $BASmbc = "34"; //Loja
                break;
            case 10755949866707:
                $BASmbc = "40"; //Machala
                break;

            default:
                $BASmbc = 2;
        }

        return $BASmbc;
    }

    /* +++++++++FUNCION PARA OBTENER EL TIPO DE DUCUMENTO +++++++++++++++++++ */

    public static function retrieveDocumentType($document)
    {
        /* IDType: [NE]. Indica el tipo de documento. Opciones v�lidas:
         * 	0: Ninguno
         * 	1: RUC.
         * 	2: C�dula.
         * 	7: Pasaporte.
         */

        $temp = (string) $document;

        if (is_numeric($document) || strlen($temp) < 10) {
            $document = str_split($document);

            if ($document[2] == 9) {
                if (GlobalFunctions::validate_ruc_9($document)) {
                    return '1'; //'RUC';
                } else {
                    return '0'; //'OTHER';
                }
            } elseif ($document[2] == 6) {
                if (GlobalFunctions::validate_ruc_6($document)) {
                    return '1'; //'RUC';
                } else {
                    return '0'; //'OTHER';
                }
            } elseif ($document[2] < 6) {
                $aux_ci = $document;
                unset($document[10]);
                unset($document[11]);
                unset($document[12]);

                if (GlobalFunctions::validate_ci(implode('', $document))) {
                    if (sizeof($aux_ci) == 10) {
                        return '2'; //'CI';
                    } else {
                        return '1'; //'RUC';
                    }
                } else {
                    return '0'; //'OTHER';
                }
            } else {
                return '0'; //'OTHER';
            }
        } else {
            return '7'; //'PASSPORT';
        }
    }

    /* +++++++++ Connect Tandi WS Emision +++++++++++++++++++ */

    public static function transformDate($date)
    {
        //        Debug::print_r($date);
        $formated_date = str_replace('/', '-', $date);
        $newDate = date_create($formated_date);
        $goodDate = date_format($newDate, "Y-m-d");
        //        Debug::print_r($goodDate);die();
        return $goodDate;
    }

    //Tipo de Documento//
    public static function documentType($document)
    {
        //        $documentType = self::retrieveDocumentType($document);
        switch (strlen($document)) {
            case '10':
                $document = 'c';
                return $document;
                //                $tipoEntidad = 'J';
                break;
            case '13':
                $document = 'r';
                return $document;
                //                $tipoEntidad = 'N';
                break;
                //            case '3':
                //                $document = 'p';
                //                return $document;
                //                $tipoEntidad = 'N';
                //                break;
            default:
                $document = 'p';
                return $document;
                //                $tipoEntidad = 'N';
        }
    }

    //Tipo de Entidad//
    public static function tipoEntidad($document)
    {
        //        $documentType = self::retrieveDocumentType($document);

        switch (strlen($document)) {
            case '10':
                $document = 'N';
                return $document;
                //                $tipoEntidad = 'J';
                break;
            case '13':
                $document = 'J';
                return $document;
                //                $tipoEntidad = 'N';
                break;
                //            case '3':
                //                $document = 'N';
                //                return $document;
                ////                $tipoEntidad = 'N';
                //                break;
            default:
                $document = 'N';
                return $document;
                //                $tipoEntidad = 'N';
        }
    }

    public static function redondeo($numero)
    {
        $redondeo = number_format((float) $numero, 2, '.', '');
        return $redondeo;
    }

    //Calcular Capitados Con Descuento
    public static function capitados($serial_pro, $prima, $days, $discount)
    {
        global $db;
        $capitado_dental = 3.97;
        $capitado_vida = 0.59;
        $capitado_planet = 45.625 / 365;
        $capitado_planet_corportivo = 0;

        $dental = TandiGlobalFunctions::productName($db, $serial_pro, 'DENTAL');
        //        Debug::print_r($prima);
        if ($dental) {

            $desc = (1 - ($discount / 100));
            $capitado_descontado = round(($capitado_dental * $desc), 2, PHP_ROUND_HALF_UP);
            $descuento_dental = round(($capitado_dental - $capitado_descontado), 2, PHP_ROUND_HALF_UP);
        }

        $vida = TandiGlobalFunctions::productName($db, $serial_pro, 'VIDA');
        if ($vida) {

            $desc = (1 - ($discount / 100));
            $capitado_vida_descontado = round(($capitado_vida * $desc), 2, PHP_ROUND_HALF_UP);
            $descuento_vida = round(($capitado_vida - $capitado_vida_descontado), 2, PHP_ROUND_HALF_UP);
        }

        //CAPITADO PLANET
        if ($days > 365) {
            $days = 365;
        }
        $capitado_tecnicos = round($days * $capitado_planet, 2, PHP_ROUND_HALF_UP);

        $desc = (1 - ($discount / 100));
        $capitado_planet_descontado = round(($capitado_tecnicos * $desc), 2, PHP_ROUND_HALF_UP);
        $descuento_tecnico = round(($capitado_tecnicos - $capitado_planet_descontado), 2, PHP_ROUND_HALF_UP);
        //        print_r($capitado_tecnicos * $desc);
        //        print_r('dental');
        //        print_r($descuento_dental);
        //        print_r('vida');
        //        print_r($descuento_vida);
        //        print_r('tecnico');
        //        print_r($capitado_planet_descontado);
        //        print_r($descuento_tecnico);die();
        $suma_descunetos_capitados = $descuento_dental + $descuento_vida + $descuento_tecnico;
        //        print_r('suma');
        //        print_r($suma_descunetos_capitados);
        //        Debug::print_r($capitado);die();
        return $suma_descunetos_capitados;
    }

    //Calcular PRIMA SIN CAPITADOS
    public static function prima_sin_capitados($serial_pro, $prima, $days, $discount)
    {
        global $db;
        $capitados = 0;
        $capitado_dental = 3.97;
        $capitado_vida = 0.59;
        $capitado_planet = 45.625 / 365;
        $capitado_planet_corporativo = 0;

        $dental = TandiGlobalFunctions::productName($db, $serial_pro, 'DENTAL');
        //        Debug::print_r($prima);
        if ($dental) {
            $capitados += $capitado_dental;
            //            Debug::print_r('dental: '.$capitados);
        }

        $vida = TandiGlobalFunctions::productName($db, $serial_pro, 'VIDA');
        if ($vida) {
            $capitados += $capitado_vida;
            //            Debug::print_r('vida: '.$capitados);
        }

        //CAPITADO PLANET
        if ($days > 365) {
            $days = 365;
        }
        $capitado_tecnicos = round(($days * $capitado_planet), 2, PHP_ROUND_HALF_UP);
        Debug::print_r('tenico: ' . $capitado_tecnicos);
        $capitados += $capitado_tecnicos;
        //        Debug::print_r('capitados: '.$capitados);
        //            Debug::print_r($capitados);
        $prima_sin_capitados = $prima - $capitados;
        //       print_r('suma');
        //        print_r($prima_sin_capitados);
        //        Debug::print_r($capitado);die();
        return $prima_sin_capitados;
    }

    //Calcular DESCUENTO A LA PRIMA SIN CAPITADOS
    public static function descuento_sin_capitados($serial_pro, $prima, $days, $discount, $prima_desc)
    {
        //        Debug::print_r('prueba');die();
        //        Debug::print_r($prima);
        //        Debug::print_r($prima_desc);
        //        Debug::print_r(self::capitados($serial_pro, $prima, $days, $discount));
        $descuento = (($prima - $prima_desc) - self::capitados($serial_pro, $prima, $days, $discount));
        //        Debug::print_r($descuento);die();
        return $descuento;
    }

    /* +++++++++ Connect Tandi WS Emision +++++++++++++++++++ */

    public static function tandiEmision($sales, $action = 'POST')
    {
        //        Debug::print_r($sales);
        $sending_data = array();
        $sending_data['hdnSaleId'] = array();
        $sending_data['hdnSaleId'][0] = $sales[0]['serial_sal'];
        $sending_data['txtDiscount'] = $sales[0]['discount'];
        $sending_data['txtOtherDiscount'] = $sales[0]['other_discount'];
        $sending_data['hdnSerialMan'] = $sales[0]['serial_man'];
        $sending_data['hdnSerialCus'] = $sales[0]['serial_cus'];
        $sending_data['hdnDealerDocument'] = $sales[0]['id_dea'];
        $sending_data['bill_to_radio'] = $sales[0]['bill_to_radio'];
        //Generate Sending Array
        $documentType = self::retrieveDocumentType($sending_data['txtCustomerDocument']);
        switch ($documentType) {
            case '1':
                $documentType = 'r';
                $tipoEntidad = 'J';
                break;
            case '2':
                $documentType = 'c';
                $tipoEntidad = 'N';
                break;
            case '3':
                $documentType = 'p';
                $tipoEntidad = 'N';
                break;
            default:
                $documentType = 'c';
                $tipoEntidad = 'N';
        }
        //        Debug::print_r($sending_data);die();
        //        Debug::print_r($_POST);die();
        $arraySales = array();
        $polizasArray = array();
        foreach ($sending_data['hdnSaleId'] as $sale) {
            $serialSal = $sale;
            array_push($arraySales, $serialSal);

            global $db;
            //Obtain Sales Data
            $card = new Sales($db, $sale);
            $card->getData();

            //Obtain Contract Data
            $contract = new Contracts($db, $card->getSerial_con());
            $contract->getData();

            //Obtain Product Data
            $pbd = new ProductByDealer($db, $card->getSerial_pbd());
            $pbd->getData();

            $pro = new ProductByCountry($db, $pbd->getSerial_pxc());
            $pro->getData();

            //            $pbl = new ProductbyLanguage($db, $pro->getSerial_pro());
            //            $pbl->getData();
            //            Debug::print_r($pbl);die();
            //Obtain CardHolder
            $cardHolder = new Customer($db, $card->getSerial_cus());
            $cardHolder->getData();

            //Obtain Counter
            $counter = new Counter($db, $card->getSerial_cnt());
            $counter->getData();

            //Obtain Dealer
            $dealer = new Dealer($db, $counter->getSerial_dea());
            $dealer->getData();

            //Obtain Extras (If Any)
            $extraArray = array();
            $extras = Extras::getExtrasBySale($db, $sale);

            //Obtain Discount
            $discount = $sending_data['txtDiscount'] + $sending_data['txtOtherDiscount'];

            if ($extras) {
                //                Debug::print_r($extras);
                foreach ($extras as $extra) {

                    $cusExtra = new customer($db, $extra['serial_cus']);
                    $cusExtra->getData();
                    //Datos del Cliente/Asegurado
                    $tempArray['apellidosAsegurado'] = utf8_encode($cusExtra->getLastname_cus());
                    $tempArray['correoAsegurado'] = $cusExtra->getEmail_cus();
                    $city = self::getTandiCity($db, $cusExtra->getSerial_cit());
                    if ($city) {
                        $tempArray['idCiudad'] = $city;
                    } else {
                        $tempArray['idCiudad'] = '1311768578';
                    }
                    $tempArray['direccionDomicilio'] = utf8_encode($cusExtra->getAddress_cus());
                    $tempArray['identificacion'] = trim($cusExtra->getDocument_cus());
                    $tempArray['tipoIdentificacion'] = self::documentType(trim($cusExtra->getDocument_cus()));
                    //                    $tempArray['tipoEntidad'] = self::tipoEntidad($cusExtra->getDocument_cus());
                    $tempArray['nombresAsegurado'] = utf8_encode($cusExtra->getFirstname_cus());
                    //                    $tempArray['nombresCompletos'] = utf8_encode($cusExtra->getLastname_cus() . ' ' . $cusExtra->getFirstname_cus());
                    $tempArray['idParentesco'] = '20';
                    //                    $tempArray['tipoTitularId'] = '811335687';
                    //                    $tempArray['tipoEntidad'] = self::documentType($cusExtra->getDocument_cus());
                    $tempArray['prima'] = $extra['fee_ext'];
                    $ClienteBirthDate = self::transformDate($cusExtra->getBirthdate_cus());
                    $tempArray['fechaNacimiento'] = $ClienteBirthDate;
                    if ($cusExtra->getGender_cus() == 'ND' || $cusExtra->getGender_cus() == null) {
                        $tempArray['genero'] = 'M';
                    } else {
                        $tempArray['genero'] = $cusExtra->getGender_cus();
                    }
                    array_push($extraArray, $tempArray);
                }
            }
            if ($pro->getSerial_pro() == '111' || $pro->getSerial_pro() == '112' || $pro->getSerial_pro() == '136' || $pro->getSerial_pro() == '137' || $pro->getSerial_pro() == '138' || $pro->getSerial_pro() == '139' || $pro->getSerial_pro() == '131' || $pro->getSerial_pro() == '88' || $pro->getSerial_pro() == '100') {
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = 'Final';
                $aseguradoTemp['correoAsegurado'] = 'consumidor@final.com';
                $aseguradoTemp['direccionDomicilio'] = 'Consumidor Final';
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['genero'] = 'M';
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = '9999999999';
                $aseguradoTemp['nombresAsegurado'] = 'Consumidor';
                //                $capitado = self::capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount);
                //                $prima = $card->getTotal_sal() - $capitado;
                $aseguradoTemp['prima'] = self::prima_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount);
                $aseguradoTemp['telefonoAsegurado'] = '9999999999';
                $aseguradoTemp['tipoIdentificacion'] = 'c';
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                //                Debug::print_r($city);die();
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = '2000-01-01';
                if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                }
            } else {
                //Obtain Asegurado
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = utf8_encode($cardHolder->getLastname_cus());
                $aseguradoTemp['correoAsegurado'] = utf8_encode($cardHolder->getEmail_cus());
                $aseguradoTemp['direccionDomicilio'] = utf8_encode($cardHolder->getAddress_cus());
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                if ($cardHolder->getGender_cus() == 'ND' || $cardHolder->getGender_cus() == null) {
                    $aseguradoTemp['genero'] = 'M';
                } else {
                    $aseguradoTemp['genero'] = $cardHolder->getGender_cus();
                }
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = trim($cardHolder->getDocument_cus());
                $aseguradoTemp['nombresAsegurado'] = utf8_encode($cardHolder->getFirstname_cus());
                //                $prima = $card->getTotal_sal() - $capitado;
                if ($extras) {
                    $aseguradoTemp['prima'] = self::prima_sin_capitados($pro->getSerial_pro(), $card->getFee_sal(), $card->getDays_sal(), $discount);
                } else {
                    $aseguradoTemp['prima'] = self::prima_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount);
                }
                $aseguradoTemp['telefonoAsegurado'] = $cardHolder->getPhone1_cus();
                $aseguradoTemp['tipoIdentificacion'] = self::documentType(trim($cardHolder->getDocument_cus()));
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                //                Debug::print_r($city);die();
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = self::transformDate($cardHolder->getBirthdate_cus());
                if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                }
            }
            //            $aseguradoTemp = $cliente;
            //            $aseguradoTemp['prima'] = $card->getTotal_sal();
            //            $cliente['prima'] = $card->getTotal_sal();
            //            $asegurado['prima'] = $card->getTotal_sal();
            array_push($asegurado, $aseguradoTemp);

            //            Debug::print_r($asegurado);die();
            //Polizas Array
            $polizasTempArray['asegurado'] = $asegurado;
            $polizasTempArray['comisionAgente'] = '0.00';
            $discount = $sending_data['txtDiscount'] + $sending_data['txtOtherDiscount'];
            //            if($extras){
            //                    $capitado = self::capitados($pro->getSerial_pro(), $card->getFee_sal(), $card->getDays_sal());
            //                }else{
            //                    $capitado = self::capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal());
            //                }
            $subTotal = ($card->getTotal_sal() - ($card->getTotal_sal() * $discount / 100));
            //            Debug::print_r($subTotal);die();
            $descuento = self::descuento_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount, $subTotal);
            //            $descuento = self::descuento_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount, $sending_data['txtSubTotal']);
            //            $descuento = ($aseguradoTemp['prima'] * $discount / 100);
            //            Debug::print_r($descuento);die();
            if ($discount > 0) {
                $polizasTempArray['descuento'] = $descuento;
            } else {
                $polizasTempArray['descuento'] = '0';
            }
            $polizasTempArray['fechaEmision'] = self::transformDate($card->getEmissionDate_sal());
            $polizasTempArray['fechaVigenciaDesde'] = self::transformDate($card->getBeginDate_sal());
            if ($contract->getSerial_con() == null) {
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($card->getBeginDate_sal());
            } else {
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($contract->getBegin_date_con());
            }
            if ($pro->getSerial_pro() == 7 || $pro->getSerial_pro() == 110 || $pro->getSerial_pro() == 100 || $pro->getSerial_pro() == 111 || $pro->getSerial_pro() == 112 || $pro->getSerial_pro() == 113 || $pro->getSerial_pro() == 115) {
                if ($card->getDays_sal() > 365) {
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + 364 days'));
                } else {
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + ' . $card->getDays_sal() . ' days - 1 days'));
                }
            } elseif ($card->getBeginDate_sal() == $card->getEndDate_sal()) {
                $date = self::transformDate($card->getBeginDate_sal());
                $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + 1 days'));
            } else {
                $polizasTempArray['fechaVigenciaHasta'] = self::transformDate($card->getEndDate_sal());
            }
            //Si el contrato no existe
            if ($contract->getSerial_con() == null) {
                //Verificar que la fecha inicio y fin sean distintas
                if ($card->getBeginDate_sal() == $card->getEndDate_sal()) {
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = date('Y-m-d', strtotime($date . ' + 1 days'));
                } else {
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($card->getEndDate_sal());
                }
            } else {
                $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($contract->getEnd_date_con());
            }
            $agente = self::getDealerTandiId($dealer->getId_dea());
            if ($agente['idRespuesta'] == 0) {
                $polizasTempArray['idAgente'] = $agente['idEntidad'];
            } else {
                $polizasTempArray['idAgente'] = '1';
            }
            $polizasTempArray['idPuntoVenta'] = self::getTandiPtoID($sending_data['hdnSerialMan']);
            $polizasTempArray['idSucursal'] = self::getTandiSucursalID($sending_data['hdnSerialMan']);
            $polizasTempArray['idUnidadNegocio'] = '10602427325341';
            $unidadProduccion = self::getResponsibleDataForSale($db, $card->getSerial_sal());
            $polizasTempArray['idUnidadProduccion'] = $unidadProduccion['document_usr'];
            if ($pro->getSerial_pro() == 24) {
                $polizasTempArray['idproductoHomologado'] = '9';
            } else {
                $polizasTempArray['idproductoHomologado'] = $pro->getSerial_pro();
            }
            if ($contract->getSerial_con() == null) {
                $polizasTempArray['numeroContenedor'] = $card->getSerial_sal();
            } else {
                $polizasTempArray['numeroContenedor'] = $contract->getSerial_con();
            }
            if ($card->getCardNumber_sal() == null || $card->getCardNumber_sal() == 0) {
                $polizasTempArray['numeroCertificado'] = $card->getSerial_sal();
                $polizasTempArray['numeroPoliza'] = $card->getSerial_sal();
            } else {
                $polizasTempArray['numeroCertificado'] = $card->getCardNumber_sal();
                $polizasTempArray['numeroPoliza'] = $card->getCardNumber_sal();
            }
            $polizasTempArray['porcentajeDescuento'] = $discount;
            $polizasTempArray['tipoContenedor'] = 'A';
            $polizasTempArray['usuarioEmision'] = $counter->getSerial_usr();
            array_push($polizasArray, $polizasTempArray);
            //            Debug::print_r($polizasArray);
        }
        //        die();
        //Obtain Payer
        if ($sending_data['bill_to_radio'] == 'CUSTOMER') {
            //Pagador es una persona natural
            $pagador = new Customer($db, $sending_data['hdnSerialCus']);
            $pagador->getData();

            $pagadorArray = array();
            $pagadorArray['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            if ($city) {
                $pagadorArray['ciudad'] = $city;
            } else {
                $pagadorArray['ciudad'] = '1311768578';
            }
            $pagadorArray['correoElectronico'] = $pagador->getEmail_cus();
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_cus());
            $pagadorArray['identificacion'] = trim($pagador->getDocument_cus());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($pagadorArray['tipoEntidad'] == 'J') {
                $pagadorArray['sectorEmpresaId'] = '1';
            }
            $pagadorArray['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $birthDate = self::transformDate($pagador->getBirthdate_cus());
            $pagadorArray['fechaNacimiento'] = $birthDate;
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $pagadorArray['genero'] = 'M';
            } else {
                $pagadorArray['genero'] = $pagador->getGender_cus();
            }

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = trim($pagador->getDocument_cus());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $cliente['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $cliente['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            //            Debug::print_r($city);die();
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }

            $cliente['correoElectronico'] = $pagador->getEmail_cus();
            $cliente['direccion'] = utf8_encode($pagador->getAddress_cus());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($cliente['tipoEntidad'] == 'J') {
                $cliente['sectorEmpresaId'] = '1';
            }
            $cliente['fechaNacimiento'] = self::transformDate($pagador->getBirthdate_cus());
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $cliente['genero'] = 'M';
            } else {
                $cliente['genero'] = $pagador->getGender_cus();
            }
        } else {
            //El pagador es un comercializador
            $dealer = TandiGlobalFunctions::dealerIDExists($db, $sending_data['hdnDealerDocument']);
            $pagador = new Dealer($db, $dealer[0]['serial_dea']);
            $pagador->getData();
            $pagadorArray = array();
            //get Sector
            $sector = new Sector($db, $pagador->getSerial_sec());
            $sector->getData();
            //get City
            $city = self::getTandiCity($db, $sector->getSerial_cit());

            $pagadorArray['ciudad'] = self::getTandiCity($db, $city);
            $pagadorArray['correoElectronico'] = $pagador->getEmail1_dea();
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_dea());
            $pagadorArray['identificacion'] = trim($pagador->getId_dea());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $pagadorArray['nombres'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['fechaNacimiento'] = date('Y-m-d');
            $pagadorArray['sectorEmpresaId'] = '1';

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = utf8_encode($pagador->getId_dea());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $cliente['nombres'] = utf8_encode($pagador->getName_dea());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }
            $cliente['correoElectronico'] = $pagador->getEmail1_dea();
            $cliente['direccion'] = utf8_encode($pagador->getAddress_dea());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $cliente['fechaNacimiento'] = date('Y-m-d');
            $cliente['sectorEmpresaId'] = '1';
        }
        $sending_array = array();

        //Build final Array
        $tempArray = array();
        $tempArray['cliente'] = $cliente;
        $tempArray['numeroPagos'] = '1';
        $tempArray['monedaId'] = '11141120';
        $tempArray['fechaPrimerpago'] = Date('Y-m-d', strtotime("+10 days"));
        $tempArray['pass'] = '';
        $tempArray['pagador'] = $pagadorArray;
        $tempArray['polizas'] = $polizasArray;
        $tempArray['puerto'] = '';
        $tempArray['esMigrado'] = '1';
        $tempArray['user'] = '';
        array_push($sending_array, $tempArray);
        $json = json_encode($tempArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        //        Debug::print_r($card->getTotal_sal());
        //        Debug::print_r($capitado);
        //        Debug::print_r($prima);
        //        Debug::print_r($cliente);
        //        Debug::print_r($polizasArray);
        //        Debug::print_r($pagadorArray);
        //        Debug::print_r($tempArray);
        //        Debug::print_r($json);die();
        //Tandi Log
        $tandiLog = TandiLog::insert($db, 'SALE', serialize($arraySales), $json);

        if (!function_exists('curl_init'))
            die('CURL No instalado. Por favor comun&iacute;quese con el administrador.');
        $curl = curl_init();



        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bluecard.com.ec/services/emision/put",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic cm9zczpyb3Nz",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        //        Debug::print_r($response);
        $jsonResponse = json_decode($response, true);
        curl_close($curl);
        //        Debug::print_r($json); 
        //        Debug::print_r($response);die();

        if ($jsonResponse['codigo'] == 1) {
            TandiLog::updateLog($db, $tandiLog, "ERROR", serialize($jsonResponse), 200);
            ////            Debug::print_r($json);
            //            return false;
            return $jsonResponse;
        } else {
            TandiLog::updateLog($db, $tandiLog, "SUCCESS", serialize($jsonResponse), 200);
            //            Debug::print_r($jsonResponse);
            return $jsonResponse;
        }
    }

    //Verified Migrated 
    public static function getMigrated($db, $card_number_sal)
    {
        $sql = "select * from tandi_Log where table_tal = 'SALE' and json_tal like '%$card_number_sal%' and type_tal = 'SUCCESS' ";
        //        Debug::print_r($sql);die();
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }

    //Get serial sal from tandi_log
    public static function getSerialSal($db)
    {
        $sql = 'SELECT * FROM tandi_Log where serial_tal > 1552 and serial_tal < 4243 and table_tal = \'SALE\' and type_tal = \'ERROR\'';

        $result = $db->getAll($sql);
        //        Debug::print_r($result[0]);die();
        if ($result)
            return $result;
        else
            return false;
    }

    //GET SALE DATA
    public static function getSaleData($db, $card_number_sal)
    {
        global $db;

        $sql = 'select
                    sal.serial_sal as \'serial_sal\',
                    inv.discount_prcg_inv as \'discount\',
                    inv.other_dscnt_inv as \'other_discount\',
                    dbm.serial_man as \'serial_man\',
                    inv.serial_cus as \'serial_cus\',
                    dea.id_dea as \'id_dea\',
                    if(inv.serial_cus is null,\'OTHER\', \'CUSTOMER\') as \'bill_to_radio\'
                    from sales sal
                    join invoice inv on inv.serial_inv = sal.serial_inv
                    join document_by_manager dbm on dbm.serial_dbm = inv.serial_dbm
                    left join dealer dea on dea.serial_dea = inv.serial_dea
                    where sal.serial_sal in (' . $card_number_sal . ')';
        //Debug::print_r($sql);die();
        $result = $db->getAll($sql);
        //        Debug::print_r($result[0]);die();
        if ($result)
            return $result;
        else
            return false;
    }

    // Get traveler log

    public static function getTravelerLogData($db){
        global $db;

        $sql = "select trl.serial_trl,inv.erp_id AS facturaId, DATE_FORMAT(trl.start_trl,'%d/%m/%Y') AS fechaViajeInicio, DATE_FORMAT(trl.end_trl,'%d/%m/%Y') AS fechaViajeFin, cus.gender_cus AS genero,
                DATE_FORMAT(cus.birthdate_cus,'%d/%m/%Y') AS fechaNacimiento,'811335685' AS tipoTitularId, cus.document_cus AS identificacion,
                cus.document_type_cus AS tipoIdentificacion, cus.first_name_cus AS nombresAsegurado, cus.last_name_cus AS apellidosAsegurado, cus.address_cus AS direccionDomicilio
                from sales sal 
                join invoice inv on sal.serial_inv = inv.serial_inv
                join traveler_log trl on sal.serial_sal = trl.serial_sal
                join customer cus on trl.serial_cus = cus.serial_cus
                where trl.status_trl='ACTIVE' AND (trl.migrado_trl IS NULL OR trl.migrado_trl = 'NO')";
        //Debug::print_r($sql);die();
        $result = $db->getAll($sql);
        //        Debug::print_r($result[0]);die();
        if ($result)
            return $result;
        else
            return false;
    }

    public static function getDealerTandiId($id_dea)
    {
        global $db;
        $array = array();
        $array['identificacion'] = $id_dea;
        $array['tipo'] = 'Agente';


        $tandiLog = TandiLog::insert($db, 'DEALER', $id_dea, $array);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bluecard.com.ec/services/entidad/get",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic cm9zczpyb3Nz",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        //        $err = curl_error($curl);

        curl_close($curl);
        $response = json_decode($response, true);
        if ($response['idRespuesta'] == 0) {
            TandiLog::updateLog($db, $tandiLog, "SUCCESS", serialize($response), 420);
        } else {
            TandiLog::updateLog($db, $tandiLog, "Error", serialize($response), 420);
        }
        //        if ($response['idRespuesta'] == 0) {
        //        } else {
        return $response;
        //        }
    }

    /* +++++++++OBTENER EL SERIAL MBC POR EL ID ERP +++++++++++++++++++ */

    public static function getMbcIdForSalesToERP($db, $id_erp_sal)
    {
        $sql = "SELECT DISTINCT d.serial_mbc
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN dealer d ON d.serial_dea = pbd.serial_dea
					AND s.id_erp_sal in ($id_erp_sal)";

        $result = $db->getOne($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /* +++++++++OBTENER INFORMACIÓN DE LA FACTURA POR EL ERP ID +++++++++++++++++++ */

    public static function translateERPInvoiceIDToPASSerials($db, $erp_invoice_serials)
    {
        $sql = "SELECT serial_inv, number_inv, total_inv, serial_pay, erp_id
			FROM invoice 
			WHERE erp_id IN ($erp_invoice_serials)
			AND status_inv NOT IN ('PAID', 'UNCOLLECTABLE')";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            ErrorLog::log($db, 'NO PAS SERIALS FOR ERP INVOICE IDS', $erp_invoice_serials);
            return array();
        }
    }

    /* +++++++++NOTA CREDITO +++++++++++++++++++ */

    public static function logCreditNotePaymentLines($payment_detail)
    {
        $credit_notes_as_payments = array();

        if (is_array($payment_detail) && count($payment_detail) > 0) {
            foreach ($payment_detail as $pDetail) {
                if ($pDetail['type_pyf'] != 'CREDIT_NOTE') :
                    continue;
                endif;

                $credit_notes_as_payments[$pDetail['invoice']] += $pDetail['amount_pyf'];
            }
        }

        return $credit_notes_as_payments;
    }

    /* +++++++++OBTENER LOS DATOS DEL RESPONABLE A PARTIR DE LA VENTA +++++++++++++++++++ */

    public static function getResponsibleDataForSale($db, $serial_sal)
    {
        $sql = "SELECT u.document_usr, u.username_usr, u.serial_usr
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN user_by_dealer ubd ON ubd.serial_dea = pbd.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr AND s.serial_sal = $serial_sal";
        //        Debug::print_r($sql);die();
        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /* +++++++++OBTENER EL ID DE LA CIUDAD DE TANDI A PARTIR DEL SERIAL CIT +++++++++++++++++++ */

    public static function getTandiCity($db, $serial_cit)
    {
        $sql = "SELECT c.tandi_cit
				FROM city c
				WHERE c.serial_cit = $serial_cit";
        $result = $db->getAll($sql);
        //        Debug::print_r($result[0][0]);die();

        if ($result[0][0]) {
            return $result[0][0];
        } else {
            return 1311768578; //Retorno Quito cuando no encuentra Ciudad
        }
    }

    public static function invoiceHasERPConnection($serial_mbc)
    {
        if (TANDI_ON) {
            $mbc_with_erp = array(2, 3, 4, 5, 6, 31, 32, 33, 34, 35, 36, 38, 40);

            if (in_array($serial_mbc, $mbc_with_erp)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }


    //migrar años anteriores
    /* +++++++++ Connect Tandi WS Emision +++++++++++++++++++ */

    public static function tandiEmisionOld($sales, $action = 'POST')
    {
        //        Debug::print_r($sales);
        $sending_data = array();
        $sending_data['hdnSaleId'] = array();
        $sending_data['hdnSaleId'][0] = $sales[0]['serial_sal'];
        $sending_data['txtDiscount'] = $sales[0]['discount'];
        $sending_data['txtOtherDiscount'] = $sales[0]['other_discount'];
        $sending_data['hdnSerialMan'] = $sales[0]['serial_man'];
        $sending_data['hdnSerialCus'] = $sales[0]['serial_cus'];
        $sending_data['hdnDealerDocument'] = $sales[0]['id_dea'];
        $sending_data['bill_to_radio'] = $sales[0]['bill_to_radio'];
        //Generate Sending Array
        $documentType = self::retrieveDocumentType($sending_data['txtCustomerDocument']);
        switch ($documentType) {
            case '1':
                $documentType = 'r';
                $tipoEntidad = 'J';
                break;
            case '2':
                $documentType = 'c';
                $tipoEntidad = 'N';
                break;
            case '3':
                $documentType = 'p';
                $tipoEntidad = 'N';
                break;
            default:
                $documentType = 'c';
                $tipoEntidad = 'N';
        }
        //        Debug::print_r($sending_data);die();
        //        Debug::print_r($_POST);die();
        $arraySales = array();
        $polizasArray = array();
        foreach ($sending_data['hdnSaleId'] as $sale) {
            $serialSal = $sale;
            array_push($arraySales, $serialSal);

            global $db;
            //Obtain Sales Data
            $card = new Sales($db, $sale);
            $card->getData();

            //Obtain Contract Data
            $contract = new Contracts($db, $card->getSerial_con());
            $contract->getData();

            //Obtain Product Data
            $pbd = new ProductByDealer($db, $card->getSerial_pbd());
            $pbd->getData();

            $pro = new ProductByCountry($db, $pbd->getSerial_pxc());
            $pro->getData();

            //            $pbl = new ProductbyLanguage($db, $pro->getSerial_pro());
            //            $pbl->getData();
            //            Debug::print_r($pbl);die();
            //Obtain CardHolder
            $cardHolder = new Customer($db, $card->getSerial_cus());
            $cardHolder->getData();

            //Obtain Counter
            $counter = new Counter($db, $card->getSerial_cnt());
            $counter->getData();

            //Obtain Dealer
            $dealer = new Dealer($db, $counter->getSerial_dea());
            $dealer->getData();

            //Obtain Extras (If Any)
            $extraArray = array();
            $extras = Extras::getExtrasBySale($db, $sale);

            //Obtain Discount
            $discount = $sending_data['txtDiscount'] + $sending_data['txtOtherDiscount'];

            if ($extras) {
                //                Debug::print_r($extras);
                foreach ($extras as $extra) {

                    $cusExtra = new customer($db, $extra['serial_cus']);
                    $cusExtra->getData();
                    //Datos del Cliente/Asegurado
                    $tempArray['apellidosAsegurado'] = utf8_encode($cusExtra->getLastname_cus());
                    $tempArray['correoAsegurado'] = $cusExtra->getEmail_cus();
                    $city = self::getTandiCity($db, $cusExtra->getSerial_cit());
                    if ($city) {
                        $tempArray['idCiudad'] = $city;
                    } else {
                        $tempArray['idCiudad'] = '1311768578';
                    }
                    $tempArray['direccionDomicilio'] = utf8_encode($cusExtra->getAddress_cus());
                    $tempArray['identificacion'] = trim($cusExtra->getDocument_cus());
                    $tempArray['tipoIdentificacion'] = self::documentType(trim($cusExtra->getDocument_cus()));
                    //                    $tempArray['tipoEntidad'] = self::tipoEntidad($cusExtra->getDocument_cus());
                    $tempArray['nombresAsegurado'] = utf8_encode($cusExtra->getFirstname_cus());
                    //                    $tempArray['nombresCompletos'] = utf8_encode($cusExtra->getLastname_cus() . ' ' . $cusExtra->getFirstname_cus());
                    $tempArray['idParentesco'] = '20';
                    //                    $tempArray['tipoTitularId'] = '811335687';
                    //                    $tempArray['tipoEntidad'] = self::documentType($cusExtra->getDocument_cus());
                    $tempArray['prima'] = $extra['fee_ext'];
                    $ClienteBirthDate = self::transformDate($cusExtra->getBirthdate_cus());
                    $tempArray['fechaNacimiento'] = $ClienteBirthDate;
                    if ($cusExtra->getGender_cus() == 'ND' || $cusExtra->getGender_cus() == null) {
                        $tempArray['genero'] = 'M';
                    } else {
                        $tempArray['genero'] = $cusExtra->getGender_cus();
                    }
                    array_push($extraArray, $tempArray);
                }
            }
            if ($pro->getSerial_pro() == '111' || $pro->getSerial_pro() == '112' || $pro->getSerial_pro() == '136' || $pro->getSerial_pro() == '137' || $pro->getSerial_pro() == '138' || $pro->getSerial_pro() == '139' || $pro->getSerial_pro() == '131' || $pro->getSerial_pro() == '88' || $pro->getSerial_pro() == '100') {
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = 'Final';
                $aseguradoTemp['correoAsegurado'] = 'consumidor@final.com';
                $aseguradoTemp['direccionDomicilio'] = 'Consumidor Final';
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['genero'] = 'M';
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = '9999999999';
                $aseguradoTemp['nombresAsegurado'] = 'Consumidor';
                //                $capitado = self::capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount);
                //                $prima = $card->getTotal_sal() - $capitado;
                $aseguradoTemp['prima'] = self::prima_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount);
                $aseguradoTemp['telefonoAsegurado'] = '9999999999';
                $aseguradoTemp['tipoIdentificacion'] = 'c';
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                //                Debug::print_r($city);die();
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = '2000-01-01';
                if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                }
            } else {
                //Obtain Asegurado
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = utf8_encode($cardHolder->getLastname_cus());
                $aseguradoTemp['correoAsegurado'] = utf8_encode($cardHolder->getEmail_cus());
                $aseguradoTemp['direccionDomicilio'] = utf8_encode($cardHolder->getAddress_cus());
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                if ($cardHolder->getGender_cus() == 'ND' || $cardHolder->getGender_cus() == null) {
                    $aseguradoTemp['genero'] = 'M';
                } else {
                    $aseguradoTemp['genero'] = $cardHolder->getGender_cus();
                }
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = trim($cardHolder->getDocument_cus());
                $aseguradoTemp['nombresAsegurado'] = utf8_encode($cardHolder->getFirstname_cus());
                //                $prima = $card->getTotal_sal() - $capitado;
                if ($extras) {
                    $aseguradoTemp['prima'] = self::prima_sin_capitados($pro->getSerial_pro(), $card->getFee_sal(), $card->getDays_sal(), $discount);
                } else {
                    $aseguradoTemp['prima'] = self::prima_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount);
                }
                $aseguradoTemp['telefonoAsegurado'] = $cardHolder->getPhone1_cus();
                $aseguradoTemp['tipoIdentificacion'] = self::documentType(trim($cardHolder->getDocument_cus()));
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                //                Debug::print_r($city);die();
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = self::transformDate($cardHolder->getBirthdate_cus());
                if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                }
            }
            //            $aseguradoTemp = $cliente;
            //            $aseguradoTemp['prima'] = $card->getTotal_sal();
            //            $cliente['prima'] = $card->getTotal_sal();
            //            $asegurado['prima'] = $card->getTotal_sal();
            array_push($asegurado, $aseguradoTemp);

            //            Debug::print_r($asegurado);die();
            //Polizas Array
            $polizasTempArray['asegurado'] = $asegurado;
            $polizasTempArray['comisionAgente'] = '0.00';
            $discount = $sending_data['txtDiscount'] + $sending_data['txtOtherDiscount'];
            //            if($extras){
            //                    $capitado = self::capitados($pro->getSerial_pro(), $card->getFee_sal(), $card->getDays_sal());
            //                }else{
            //                    $capitado = self::capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal());
            //                }
            $subTotal = ($card->getTotal_sal() - ($card->getTotal_sal() * $discount / 100));
            //            Debug::print_r($subTotal);die();
            $descuento = self::descuento_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount, $subTotal);
            //            $descuento = self::descuento_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $card->getDays_sal(), $discount, $sending_data['txtSubTotal']);
            //            $descuento = ($aseguradoTemp['prima'] * $discount / 100);
            //            Debug::print_r($descuento);die();
            if ($discount > 0) {
                $polizasTempArray['descuento'] = $descuento;
            } else {
                $polizasTempArray['descuento'] = '0';
            }
            $polizasTempArray['fechaEmision'] = self::transformDate($card->getEmissionDate_sal());
            $polizasTempArray['fechaVigenciaDesde'] = self::transformDate($card->getBeginDate_sal());
            if ($contract->getSerial_con() == null) {
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($card->getBeginDate_sal());
            } else {
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($contract->getBegin_date_con());
            }
            if ($pro->getSerial_pro() == 7 || $pro->getSerial_pro() == 110 || $pro->getSerial_pro() == 100 || $pro->getSerial_pro() == 111 || $pro->getSerial_pro() == 112 || $pro->getSerial_pro() == 113 || $pro->getSerial_pro() == 115) {
                if ($card->getDays_sal() > 365) {
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + 364 days'));
                } else {
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + ' . $card->getDays_sal() . ' days - 1 days'));
                }
            } elseif ($card->getBeginDate_sal() == $card->getEndDate_sal()) {
                $date = self::transformDate($card->getBeginDate_sal());
                $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + 1 days'));
            } else {
                $polizasTempArray['fechaVigenciaHasta'] = self::transformDate($card->getEndDate_sal());
            }
            //Si el contrato no existe
            if ($contract->getSerial_con() == null) {
                //Verificar que la fecha inicio y fin sean distintas
                if ($card->getBeginDate_sal() == $card->getEndDate_sal()) {
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = date('Y-m-d', strtotime($date . ' + 1 days'));
                } else {
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($card->getEndDate_sal());
                }
            } else {
                $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($contract->getEnd_date_con());
            }
            $agente = self::getDealerTandiId($dealer->getId_dea());
            if ($agente['idRespuesta'] == 0) {
                $polizasTempArray['idAgente'] = $agente['idEntidad'];
            } else {
                $polizasTempArray['idAgente'] = '1';
            }
            $polizasTempArray['idPuntoVenta'] = self::getTandiPtoID($sending_data['hdnSerialMan']);
            $polizasTempArray['idSucursal'] = self::getTandiSucursalID($sending_data['hdnSerialMan']);
            $polizasTempArray['idUnidadNegocio'] = '10602427325341';
            $unidadProduccion = self::getResponsibleDataForSale($db, $card->getSerial_sal());
            $polizasTempArray['idUnidadProduccion'] = '1724125636';
            if ($pro->getSerial_pro() == 24) {
                $polizasTempArray['idproductoHomologado'] = '9';
            } else {
                $polizasTempArray['idproductoHomologado'] = $pro->getSerial_pro();
            }
            if ($contract->getSerial_con() == null) {
                $polizasTempArray['numeroContenedor'] = $card->getSerial_sal();
            } else {
                $polizasTempArray['numeroContenedor'] = $contract->getSerial_con();
            }
            if ($card->getCardNumber_sal() == null || $card->getCardNumber_sal() == 0) {
                $polizasTempArray['numeroCertificado'] = $card->getSerial_sal();
                $polizasTempArray['numeroPoliza'] = $card->getSerial_sal();
            } else {
                $polizasTempArray['numeroCertificado'] = $card->getCardNumber_sal();
                $polizasTempArray['numeroPoliza'] = $card->getCardNumber_sal();
            }
            $polizasTempArray['porcentajeDescuento'] = $discount;
            $polizasTempArray['tipoContenedor'] = 'A';
            $polizasTempArray['usuarioEmision'] = $counter->getSerial_usr();
            array_push($polizasArray, $polizasTempArray);
            //            Debug::print_r($polizasArray);
        }
        //        die();
        //Obtain Payer
        if ($sending_data['bill_to_radio'] == 'CUSTOMER') {
            //Pagador es una persona natural
            $pagador = new Customer($db, $sending_data['hdnSerialCus']);
            $pagador->getData();

            $pagadorArray = array();
            $pagadorArray['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            if ($city) {
                $pagadorArray['ciudad'] = $city;
            } else {
                $pagadorArray['ciudad'] = '1311768578';
            }
            $pagadorArray['correoElectronico'] = $pagador->getEmail_cus();
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_cus());
            $pagadorArray['identificacion'] = trim($pagador->getDocument_cus());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($pagadorArray['tipoEntidad'] == 'J') {
                $pagadorArray['sectorEmpresaId'] = '1';
            }
            $pagadorArray['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $birthDate = self::transformDate($pagador->getBirthdate_cus());
            $pagadorArray['fechaNacimiento'] = $birthDate;
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $pagadorArray['genero'] = 'M';
            } else {
                $pagadorArray['genero'] = $pagador->getGender_cus();
            }

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = trim($pagador->getDocument_cus());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $cliente['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $cliente['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            //            Debug::print_r($city);die();
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }

            $cliente['correoElectronico'] = $pagador->getEmail_cus();
            $cliente['direccion'] = utf8_encode($pagador->getAddress_cus());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($cliente['tipoEntidad'] == 'J') {
                $cliente['sectorEmpresaId'] = '1';
            }
            $cliente['fechaNacimiento'] = self::transformDate($pagador->getBirthdate_cus());
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $cliente['genero'] = 'M';
            } else {
                $cliente['genero'] = $pagador->getGender_cus();
            }
        } else {
            //El pagador es un comercializador
            $dealer = TandiGlobalFunctions::dealerIDExists($db, $sending_data['hdnDealerDocument']);
            $pagador = new Dealer($db, $dealer[0]['serial_dea']);
            $pagador->getData();
            $pagadorArray = array();
            //get Sector
            $sector = new Sector($db, $pagador->getSerial_sec());
            $sector->getData();
            //get City
            $city = self::getTandiCity($db, $sector->getSerial_cit());

            $pagadorArray['ciudad'] = self::getTandiCity($db, $city);
            $pagadorArray['correoElectronico'] = $pagador->getEmail1_dea();
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_dea());
            $pagadorArray['identificacion'] = trim($pagador->getId_dea());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $pagadorArray['nombres'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['fechaNacimiento'] = date('Y-m-d');
            $pagadorArray['sectorEmpresaId'] = '1';

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = utf8_encode($pagador->getId_dea());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $cliente['nombres'] = utf8_encode($pagador->getName_dea());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }
            $cliente['correoElectronico'] = $pagador->getEmail1_dea();
            $cliente['direccion'] = utf8_encode($pagador->getAddress_dea());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $cliente['fechaNacimiento'] = date('Y-m-d');
            $cliente['sectorEmpresaId'] = '1';
        }
        $sending_array = array();

        //Build final Array
        $tempArray = array();
        $tempArray['cliente'] = $cliente;
        $tempArray['numeroPagos'] = '1';
        $tempArray['monedaId'] = '11141120';
        $tempArray['fechaPrimerpago'] = Date('Y-m-d', strtotime("+10 days"));
        $tempArray['pass'] = '';
        $tempArray['pagador'] = $pagadorArray;
        $tempArray['polizas'] = $polizasArray;
        $tempArray['puerto'] = '';
        $tempArray['esMigrado'] = '1';
        $tempArray['user'] = '';
        array_push($sending_array, $tempArray);
        $json = json_encode($tempArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        //        Debug::print_r($card->getTotal_sal());
        //        Debug::print_r($capitado);
        //        Debug::print_r($prima);
        //        Debug::print_r($cliente);
        //        Debug::print_r($polizasArray);
        //        Debug::print_r($pagadorArray);
        //        Debug::print_r($tempArray);
        //        Debug::print_r($json);die();
        //Tandi Log
        $tandiLog = TandiLog::insert($db, 'SALE', serialize($arraySales), $json);

        if (!function_exists('curl_init'))
            die('CURL No instalado. Por favor comun&iacute;quese con el administrador.');
        $curl = curl_init();



        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bluecard.com.ec/services/emision/put",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic cm9zczpyb3Nz",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        //        Debug::print_r($response);
        $jsonResponse = json_decode($response, true);
        curl_close($curl);
        //        Debug::print_r($json);
        //        Debug::print_r($response);die();

        if ($jsonResponse['codigo'] == 1) {
            TandiLog::updateLog($db, $tandiLog, "ERROR", serialize($jsonResponse), 200);
            ////            Debug::print_r($json);
            //            return false;
            return $jsonResponse;
        } else {
            TandiLog::updateLog($db, $tandiLog, "SUCCESS", serialize($jsonResponse), 200);
            //            Debug::print_r($jsonResponse);
            return $jsonResponse;
        }
    }
}
