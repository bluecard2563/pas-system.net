<?php
/*
File: BenefitsByProductType.class.php
Author: Miguel Ponce.
Creation Date: 07/01/2010
Last Modified: 07/01/2010
*/

class BenefitsByProductType{				
	var $db;
	var $serial_bpt;
	var $serial_rst;	
	var $serial_ben;
	var $serial_tpp;
	var $serial_con;
	var $price_bpt;	
	var $status_bpt;
	var $restriction_price_bpt;
	var $serial_cur;
		
	function __construct($db, $serial_bpt=NULL, $serial_rst=NULL, $serial_ben=NULL, $serial_tpp=NULL, $serial_con=NULL, $price_bpt=NULL, $status_bpt=NULL, $restriction_price_bpt=NULL, $serial_cur=NULL){
		$this -> db = $db;
		$this -> serial_bpt = $serial_bpt;
		$this -> serial_rst = $serial_rst;
		$this -> serial_ben = $serial_ben;
		$this -> serial_tpp = $serial_tpp;
		$this -> serial_con = $serial_con;
		$this -> price_bpt = $price_bpt;
		$this -> status_bpt = $status_bpt;
		$this -> restriction_price_bpt = $restriction_price_bpt;
		$this -> serial_cur = $serial_cur;

	}
	
	/***********************************************
    * funcion getBenefits
    * gets all the Benefits of the system
    ***********************************************/
	function getBenefits(){
		if($this->serial_tpp!=NULL){
			$sql = 	"SELECT serial_bpt,
						serial_rst,
						serial_ben,
						serial_cur,
						serial_tpp,
						serial_con,
						price_bpt,
						status_bpt,
						restriction_price_bpt 
					 FROM benefits_by_product_type 
					 WHERE serial_tpp = ".$this->serial_tpp."
                                         ORDER BY serial_ben ";
					 
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$arr=array();
				$cont=0;
				
				do{
					$arr[$result->fields[2]]=array('serial_bpt'=>$result->fields[0],
                                        'serial_rst'=>$result->fields[1],
                                        'serial_cur'=>$result->fields[3],
                                        'serial_tpp'=>$result->fields[4],
                                        'serial_con'=>$result->fields[5],
                                        'price_bpt'=>$result->fields[6],
                                        'status_bpt'=>$result->fields[7],
                                        'restriction_price_bpt'=>$result->fields[8]
                                        );
			
					$result->MoveNext();
					$cont++;    
	
				}while($cont<$num);
			}

			return $arr;
		}
		else{
				return false;
		}	
	}

	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {

        $sql = "
            INSERT INTO benefits_by_product_type (
				serial_bpt,
				serial_rst,				
				serial_ben,
				serial_tpp,
				serial_con,
				price_bpt,
				status_bpt,
				restriction_price_bpt,
				serial_cur)
            VALUES (
				NULL, ";
                if($this->serial_rst)
                    $sql.=	" '".$this->serial_rst."',";
                else
                    $sql.=	"NULL,";
		$sql.=		" '".$this->serial_ben."',
				'".$this->serial_tpp."',
				'".$this->serial_con."',
				'".$this->price_bpt."',
				'".$this->status_bpt."',";
                            if($this->restriction_price_bpt)
                                $sql.=  "'".$this->restriction_price_bpt."',";
                            else
                                $sql.=	"NULL,";
  		$sql.=	"'".$this->serial_cur."')";
//echo $sql;
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	

	/***********************************************
    * function delete
    * deletes a  register in DB 
    ***********************************************/
	function delete(){
		$sql = "
			DELETE FROM benefits_by_product_type
			WHERE serial_tpp = '".$this->serial_tpp."'";
				
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	/***********************************************/
	

	
	
	///GETTERS

	function getSerial_bpt(){
		return $this->serial_bpt;
	}
	function getSerial_rst(){
		return $this->serial_rst;
	}
	function getSerial_ben(){
		return $this->serial_ben;
	}
	function getSerial_tpp(){
		return $this->serial_tpp;
	}
	function getStatus_ben(){
		return $this->status_ben;
	}
	function getSerial_con(){
		return $this->serial_con;
	}
	function getPrice_bpt(){
		return $this->price_bpt;
	}
	function getRestriction_price_bpt(){
		return $this->restriction_price_bpt;
	}
	function getSerial_cur(){
		return $this->serial_cur;
	}


	///SETTERS	
	function setSerial_bpt($serial_bpt){
		$this->serial_bpt = $serial_bpt;
	}
	function setSerial_rst($serial_rst){
		$this->serial_rst = $serial_rst;
	}	
	function setSerial_ben($serial_ben){
		$this->serial_ben = $serial_ben;
	}
	function setSerial_tpp($serial_tpp){
		$this->serial_tpp = $serial_tpp;
	}
	function setSerial_con($serial_con){
		$this->serial_con = $serial_con;
	}
	function setPrice_bpt($price_bpt){
		$this->price_bpt = $price_bpt;
	}
	function setStatus_bpt($status_bpt){
		$this->status_bpt = $status_bpt;
	}
	function setRestriction_price_bpt($restriction_price_bpt){
		$this->restriction_price_bpt = $restriction_price_bpt;
	}
	function setSerial_cur($serial_cur){
		$this->serial_cur = $serial_cur;
	}
}
?>