<?php
/*
File: CurrenciesByCountry.class
Author: Patricio Astudillo
Creation Date: 23/03/2010 11:53am
Last Modified: 23/03/2010
Modified By: David Bergmann
*/
class CurrenciesByCountry {
	var $db;
	var $serial_cur;
	var $serial_cou;
	var $official_cbc;
	var $status_cbc;
	
	function __construct($db, $serial_cur = NULL, $serial_cou = NULL, $official_cbc = NULL,
                             $status_cbc = NULL){
            $this -> db = $db;
            $this -> serial_cur = $serial_cur;
            $this -> serial_cou = $serial_cou;
            $this -> official_cbc = $official_cbc;
            $this -> status_cbc = $status_cbc;
	}

        /***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
            $sql= "INSERT INTO currencies_by_countries  (
                                    serial_cur,
                                    serial_cou,
                                    official_cbc,
                                    status_cbc
                                    )
                   VALUES('".$this->serial_cur."',
                          '".$this->serial_cou."',
                          '".$this->official_cbc."',
                          '".$this->status_cbc."');";

            $result = $this->db->Execute($sql);

		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

        /***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
            $sql= "UPDATE currencies_by_countries
                   SET official_cbc='".$this->official_cbc."',
                       status_cbc='".$this->status_cbc."'
                   WHERE serial_cur='".$this->serial_cur."'
                   AND serial_cou='".$this->serial_cou."'";

            $result = $this->db->Execute($sql);
            
            if ($result==true)
                return true;
            else
                return false;
	}

        /***********************************************
        * funcion deactivateCurrencies
        * updates a register in DB
        ***********************************************/
	function deactivateCurrencies(){
            $sql= "UPDATE currencies_by_countries
                   SET official_cbc='NO'
                   WHERE serial_cou='".$this->serial_cou."'";

            $result = $this->db->Execute($sql);

            if ($result==true)
                return true;
            else
                return false;
	}

        /***********************************************
        * funcion deactivateCurrencies
        * updates a register in DB
        ***********************************************/
	function getNonAssignedCurrencyesByCountry(){
            $sql =  "SELECT cur.serial_cur, cur.name_cur, cur.symbol_cur
                     FROM currency cur
                     WHERE cur.serial_cur NOT IN (
                        SELECT cbc.serial_cur
                        FROM currencies_by_countries cbc
                        WHERE cbc.serial_cou = ".$this->serial_cou.")";

            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }
            return $arreglo;
	}

        /***********************************************
        * funcion getZones
        * gets all the Zones of the system
        ***********************************************/
	function getCurrenciesByCountry(){
            $sql =  "SELECT cbc.serial_cur, cur.name_cur, cur.symbol_cur, cbc.serial_cou, cou.name_cou,
                            cbc.official_cbc, cbc.status_cbc
                     FROM currencies_by_countries cbc
                     JOIN currency cur ON cur.serial_cur = cbc.serial_cur
                     JOIN country cou ON cou.serial_cou = cbc.serial_cou
                     WHERE cbc.serial_cou = ".$this->serial_cou."
                     ORDER BY cbc.serial_cur asc";

            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }
            return $arreglo;
	}

	/**
	 *
	 * @param <Object> $db
	 * @param <Country ID> $serial_cou
	 * @return <Array> Official Currency Information
	 */
	public static function getOfficialCurrencyData($db, $serial_cou){
		$sql="SELECT cbc.serial_cur, cur.name_cur, cur.symbol_cur, cbc.serial_cou, cou.name_cou,
                     cbc.official_cbc, cbc.status_cbc, cur.exchange_fee_cur
			 FROM currencies_by_countries cbc
			 JOIN currency cur ON cur.serial_cur = cbc.serial_cur
			 JOIN country cou ON cou.serial_cou = cbc.serial_cou
			 WHERE cbc.serial_cou = $serial_cou
			 AND cbc.official_cbc='YES'
			 AND cbc.status_cbc='ACTIVE'";

		$result=$db->getRow($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

        /*
        * @name: getCurrenciesDealersCountry
        * @description: get's the change fees of all the currencies applied to one
        *			    dealer's country.
        * @params: $serial_dea (Dealer's ID)
        * @returns: Currencies array.
        */
	function getCurrenciesDealersCountry($serial_dea,$serial_sal=NULL){
		if($serial_sal != NULL) {
			$a=", sal.serial_cur as sales_serial_cur";
			$b="LEFT JOIN sales sal ON sal.serial_cur = c.serial_cur AND sal.serial_sal = $serial_sal";
		}
            $sql = "SELECT c.serial_cur, c.exchange_fee_cur, c.name_cur, c.symbol_cur, cbc.official_cbc $a
                    FROM currencies_by_countries cbc
                    JOIN currency c ON c.serial_cur=cbc.serial_cur
                    JOIN country cou ON cou.serial_cou=cbc.serial_cou
                    JOIN city cit ON cit.serial_cou=cou.serial_cou
                    JOIN sector s ON s.serial_cit=cit.serial_cit
                    JOIN dealer d ON d.serial_sec=s.serial_sec AND d.serial_dea = $serial_dea
					$b
					WHERE cbc.status_cbc = 'ACTIVE'";
                    //die($sql);
                    $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arr=array();
                $cont=0;

                do{
                    $arr[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arr;
	}	

	/***********************************************
        * funcion getAllStatus
        * Returns all kind of Status which exist in DB
        ***********************************************/
	function getAllStatus(){
            // Finds all possible status form ENUM column
            $sql = "SHOW COLUMNS FROM currencies_by_countries LIKE 'status_cbc'";
            $result = $this->db -> Execute($sql);

            $type=$result->fields[1];

            $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
            return $type;
	}
	
	///GETTERS
	function getSerial_cur(){
		return $this->serial_cur;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getOfficial_cbc(){
		return $this->official_cbc;
	}
	function getStatus_cbc(){
		return $this->status_cbc;
	}

	///SETTERS	
	function setSerial_cur($serial_cur){
		$this->serial_cur = $serial_cur;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setOfficial_cbc($official_cbc){
		$this->official_cbc = $official_cbc;
	}
	function setStatus_cbc($status_cbc){
		$this->status_cbc = $status_cbc;
	}
}
?>
