<?php
/*
File: TemporaryCustomer.class
Author: David Bergmann
Creation Date: 01/04/2010
Last Modified: 10/06/2010
Modified By: David Bergmann
*/

class TemporaryCustomer {
    var $db;
    var $serial_tcu;
    var $serial_cou;
    var $serial_fle;
    var $cou_serial_cou;
    var $serial_usr;
    var $document_tcu;
    var $first_name_tcu;
    var $last_name_tcu;
    var $birthdate_tcu;
    var $agency_name_tcu;
    var $phone_tcu;
    var $contact_phone_tcu;
    var $contact_address_tcu;
    var $extra_tcu;
    var $problem_description_tcu;
    var $status_tcu;
    var $card_number_tcu;

    function __construct($db, $serial_tcu = NULL, $serial_cou = NULL, $serial_fle = NULL, $cou_serial_cou = NULL,$serial_usr = NULL,
                         $document_tcu = NULL,$first_name_tcu = NULL,$last_name_tcu = NULL,$birthdate_tcu = NULL,
                         $agency_name_tcu = NULL,$phone_tcu = NULL,$contact_phone_tcu = NULL,$contact_address_tcu = NULL,
                         $extra_tcu = NULL,$problem_description_tcu = NULL,$status_tcu = NULL,$card_number_tcu = NULL){
        $this -> db = $db;
        $this ->serial_tcu = $serial_tcu;
        $this ->serial_cou = $serial_cou;
        $this ->serial_fle = $serial_fle;
        $this ->cou_serial_cou = $cou_serial_cou;
        $this ->serial_usr = $serial_usr;
        $this ->document_tcu = $document_tcu;
        $this ->first_name_tcu = $first_name_tcu;
        $this ->last_name_tcu = $last_name_tcu;
        $this ->birthdate_tcu = $birthdate_tcu;
        $this ->agency_name_tcu = $agency_name_tcu;
        $this ->phone_tcu = $phone_tcu;
        $this ->contact_phone_tcu = $contact_phone_tcu;
        $this ->contact_address_tcu = $contact_address_tcu;
        $this ->extra_tcu = $extra_tcu;
        $this ->problem_description_tcu = $problem_description_tcu;
        $this ->status_tcu = $status_tcu;
        $this ->card_number_tcu = $card_number_tcu;
    }

    /***********************************************
    * function getData
    * gets data by serial_tcu
    ***********************************************/
    function getData(){
        if($this->serial_tcu!=NULL){
            $sql = 	"SELECT serial_tcu, serial_cou, serial_fle, cou_serial_cou, serial_usr, document_tcu,
                            first_name_tcu, last_name_tcu, birthdate_tcu, agency_name_tcu, phone_tcu,
                            contact_phone_tcu, contact_address_tcu, extra_tcu, problem_description_tcu,
                            status_tcu, card_number_tcu
                     FROM temporary_customer
                     WHERE serial_tcu='".$this->serial_tcu."'";
            //die($sql);
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this ->serial_tcu = $result->fields[0];
                $this ->serial_cou = $result->fields[1];
                $this ->serial_fle = $result->fields[2];
                $this ->cou_serial_cou = $result->fields[3];
                $this ->serial_usr = $result->fields[4];
                $this ->document_tcu = $result->fields[5];
                $this ->first_name_tcu = $result->fields[6];
                $this ->last_name_tcu = $result->fields[7];
                $this ->birthdate_tcu = $result->fields[8];
                $this ->agency_name_tcu = $result->fields[9];
                $this ->phone_tcu = $result->fields[10];
                $this ->contact_phone_tcu = $result->fields[11];
                $this ->contact_address_tcu = $result->fields[12];
                $this ->extra_tcu = $result->fields[13];
                $this ->problem_description_tcu = $result->fields[14];
                $this ->status_tcu = $result->fields[15];
                $this ->card_number_tcu = $result->fields[16];

                return true;
            }
            else
                return false;
        }else
            return false;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert(){
        $sql="INSERT INTO temporary_customer (
                                serial_tcu,
                                serial_cou,
                                serial_fle,
                                cou_serial_cou,
                                serial_usr,
                                document_tcu,
                                first_name_tcu,
                                last_name_tcu,
                                birthdate_tcu,
                                agency_name_tcu,
                                phone_tcu,
                                contact_phone_tcu,
                                contact_address_tcu,
                                extra_tcu,
                                problem_description_tcu,
                                status_tcu,
                                card_number_tcu
                                )
                  VALUES(NULL,
                         '".$this->serial_cou."',";
                if($this->serial_fle != NULL) {
                    $sql.="'".$this->serial_fle."',";
                } else {
                    $sql.="NULL,";
                }
                  $sql.="'".$this->cou_serial_cou."',
                         '".$this->serial_usr."',
                         '".$this->document_tcu."',
                         '".$this->first_name_tcu."',
                         '".$this->last_name_tcu."',
                         STR_TO_DATE('".$this->birthdate_tcu."','%d/%m/%Y'),
                         '".$this->agency_name_tcu."',
                         '".$this->phone_tcu."',
                         '".$this->contact_phone_tcu."',
                         '".$this->contact_address_tcu."',
                         '".$this->extra_tcu."',
                         '".$this->problem_description_tcu."',
                         'REQUESTED',";
                 if($this->card_number_tcu == NULL) {
                     $sql.="NULL);";
                 } else {
                     $sql.="'".$this->card_number_tcu."');";
                 }
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update(){
        $sql= "UPDATE temporary_customer
               SET status_tcu='".$this->status_tcu."'
               WHERE serial_tcu='".$this->serial_tcu."'";

        $result = $this->db->Execute($sql);
        //die($sql);
        if ($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
    * function getTemporaryCustomersByCard
    * gets all the Temporary Customers belonging to a Card
    ***********************************************/
    function getTemporaryCustomersByCard($card_number){
        $sql = "SELECT t.*, c.name_cou
                FROM temporary_customer t
                JOIN country c ON c.serial_cou = t.serial_cou
                WHERE t.card_number_tcu='".$card_number."'
                ORDER BY t.serial_tcu asc";
        //die($sql);
        $result=$this->db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /*******************************************************
     * function getTemporaryCustomersByCard
     * gets all the Temporary Customers belonging to a Card
     *******************************************************/
    public static function getTemporaryCustomersByCountry($db,$serial_cou){
        $sql = "SELECT t.*, c.name_cou, tc.name_cou as travel_name_cou, s.status_sal, CONCAT(u.first_name_usr,' ',u.last_name_usr) as user_name
                FROM temporary_customer t
                JOIN country c ON c.serial_cou = t.serial_cou
                JOIN country tc ON tc.serial_cou = t.cou_serial_cou
				JOIN sales s ON t.card_number_tcu = s.card_number_sal
				JOIN user u ON t.serial_usr=u.serial_usr 
                WHERE t.serial_cou='".$serial_cou."'
                AND t.status_tcu = 'REQUESTED'
                ORDER BY t.serial_tcu asc";
        //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /*******************************************************
     * function getTemporaryCustomersByCard
     * gets all the Temporary Customers belonging to a Card
     *******************************************************/
    public static function denyAppliancisOnCascade($db,$serial_fle){
        $sql =  "UPDATE temporary_customer
                 SET status_tcu='DENIED'
                 WHERE serial_fle='".$serial_fle."'
                 AND status_tcu='REQUESTED'";
        //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    ///GETTERS
    function getSerial_tcu(){
        return $this->serial_tcu;
    }
    function getSerial_cou(){
        return $this->serial_cou;
    }
    function getSerial_fle(){
        return $this->serial_fle;
    }
    function getCou_serial_cou(){
        return $this->cou_serial_cou;
    }
    function getSerial_usr(){
        return $this->serial_usr;
    }
    function getDocument_tcu(){
        return $this->document_tcu;
    }
    function getFirst_name_tcu(){
        return $this->first_name_tcu;
    }
    function getLast_name_tcu(){
        return $this->last_name_tcu;
    }
    function getBirthdate_tcu(){
        return $this->birthdate_tcu;
    }
    function getAgency_name_tcu(){
        return $this->agency_name_tcu;
    }
    function getPhone_tcu(){
        return $this->phone_tcu;
    }
    function getContact_phone_tcu(){
        return $this->contact_phone_tcu;
    }
    function getContact_address_tcu(){
        return $this->contact_address_tcu;
    }
    function getExtra_tcu(){
        return $this->extra_tcu;
    }
    function getProblem_description_tcu(){
        return $this->problem_description_tcu;
    }
    function getStatus_tcu(){
        return $this->status_tcu;
    }
    function getCard_number_tcu(){
        return $this->card_number_tcu;
    }


    ///SETTERS
    function setSerial_tcu($serial_tcu){
        $this->serial_tcu = $serial_tcu;
    }
    function setSerial_cou($serial_cou){
        $this->serial_cou = $serial_cou;
    }
    function setSerial_fle($serial_fle){
        $this->serial_fle = $serial_fle;
    }
    function setCou_serial_cou($cou_serial_cou){
        $this->cou_serial_cou = $cou_serial_cou;
    }
    function setSerial_usr($serial_usr){
        $this->serial_usr = $serial_usr;
    }
    function setDocument_tcu($document_tcu){
        $this->document_tcu = $document_tcu;
    }
    function setFirst_name_tcu($first_name_tcu){
        $this->first_name_tcu = $first_name_tcu;
    }
    function setLast_name_tcu($last_name_tcu){
        $this->last_name_tcu = $last_name_tcu;
    }
    function setBirthdate_tcu($birthdate_tcu){
        $this->birthdate_tcu = $birthdate_tcu;
    }
    function setAgency_name_tcu($agency_name_tcu){
        $this->agency_name_tcu = $agency_name_tcu;
    }
    function setPhone_tcu($phone_tcu){
        $this->phone_tcu = $phone_tcu;
    }
    function setContact_phone_tcu($contact_phone_tcu){
        $this->contact_phone_tcu = $contact_phone_tcu;
    }
    function setContact_address_tcu($contact_address_tcu){
        $this->contact_address_tcu = $contact_address_tcu;
    }
    function setExtra_tcu($extra_tcu){
        $this->extra_tcu = $extra_tcu;
    }
    function setProblem_description_tcu($problem_description_tcu){
        $this->problem_description_tcu = $problem_description_tcu;
    }
    function setStatus_tcu($status_tcu){
        $this->status_tcu = $status_tcu;
    }
    function setCard_number_tcu($card_number_tcu){
        $this->card_number_tcu = $card_number_tcu;
    }
}
?>
