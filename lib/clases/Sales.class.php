<?php

/*
  Author:Esteban Angulo
  Modified date:19/02/2010
  Last modified:
 * Nicolas Flores 04/05/2010
 */

class Sales
{

    var $db;
    var $serial_sal;
    var $serial_con;
    var $serial_cus;
    var $sal_serial_sal;
    var $serial_inv;
    var $serial_cnt;
    var $serial_pbd;
    var $serial_cur;
    var $serial_cit;
    var $card_number_sal;
    var $emission_date_sal;
    var $begin_date_sal;
    var $end_date_sal;
    var $in_date_sal;
    var $days_sal;
    var $fee_sal;
    var $observations_sal;
    var $cost_sal;
    var $free_sal;
    var $id_erp_sal;
    var $status_sal;
    var $type_sal;
    var $stock_type_sal;
    var $total_sal;
    var $direct_sale_sal;
    var $total_cost_sal;
    var $change_fee_sal;
    var $international_fee_status;
    var $international_fee_used;
    var $international_fee_amount;
    var $aux;
    var $deliveredKit;
    var $transation_id;
    var $serial_rep;
    var $future_payment_sal;
    //new variables
    var $name;
    var $surname;
    var $document;
    var $email;
    var $address;
    var $reference;
    var $taxes;
    var $total;
    var $observations_contract;





    function __construct($db, $serial_sal = NULL, $serial_cus = NULL, $serial_pbd = NULL, $serial_cnt = NULL, $serial_inv = NULL, $serial_cur = NULL, $serial_cit = NULL, $card_number_sal = NULL, $emission_date_sal = NULL, $begin_date_sal = NULL, $end_date_sal = NULL, $in_date_sal = NULL, $days_sal = NULL, $fee_sal = NULL, $observations_sal = NULL, $cost_sal = NULL, $free_sal = NULL, $id_erp_sal = NULL, $status_sal = NULL, $type_sal = NULL, $stock_type_sal = NULL, $total_sal = NULL, $direct_sale_sal = NULL, $total_cost_sal = NULL, $change_fee_sal = NULL, $sal_serial_sal = NULL, $international_fee_status = NULL, $international_fee_used = NULL, $international_fee_amount = NULL, $serial_con = NULL, $transaction_id = NULL,$name=NULL,$surname=NULL,$document=NULL,$email=NULL,$address=NULL,$reference=NULL,$taxes=NULL,$total=NULL,$observations_contract=NULL)
    {
        $this->db = $db;
        $this->serial_sal = $serial_sal;
        $this->serial_con = $serial_con;
        $this->serial_cus = $serial_cus;
        $this->serial_pbd = $serial_pbd;
        $this->serial_cnt = $serial_cnt;
        $this->serial_inv = $serial_inv;
        $this->serial_cur = $serial_cur;
        $this->serial_cit = $serial_cit;
        $this->card_number_sal = $card_number_sal;
        $this->emission_date_sal = $emission_date_sal;
        $this->begin_date_sal = $begin_date_sal;
        $this->end_date_sal = $end_date_sal;
        $this->in_date_sal = $in_date_sal;
        $this->days_sal = $days_sal;
        $this->fee_sal = $fee_sal;
        $this->observations_sal = $observations_sal;
        $this->cost_sal = $cost_sal;
        $this->free_sal = $free_sal;
        $this->id_erp_sal = $id_erp_sal;
        $this->status_sal = $status_sal;
        $this->type_sal = $type_sal;
        $this->stock_type_sal = $stock_type_sal;
        $this->total_sal = $total_sal;
        $this->direct_sale_sal = $direct_sale_sal;
        $this->total_cost_sal = $total_cost_sal;
        $this->change_fee_sal = $change_fee_sal;
        $this->sal_serial_sal = $sal_serial_sal;
        $this->international_fee_status = $international_fee_status;
        $this->international_fee_used = $international_fee_used;
        $this->international_fee_amount = $international_fee_amount;
        //$this->deliveredKit = $deliveredKit;
        $this->transation_id = $transaction_id;

        $this->name= $name;
        $this->surname= $surname;
        $this->document= $document;
        $this->email= $email;
        $this->address= $address;
        $this->reference= $reference;
        $this->taxes= $taxes;
        $this->total= $total;
        $this->observations_contract = $observations_contract;
    }

    /*     * *********************************************
      @Name: getData
      @Description: Returns all data
      @Params: N/A
      @Returns: true/false
     * ********************************************* */

    function getData()
    {
        if ($this->serial_sal != NULL) {
            $sql = "SELECT s.serial_sal,
                        s.serial_cus,
                        s.serial_pbd,
                        s.serial_cnt,
                        s.serial_inv,
                        s.serial_cur,
                        s.serial_cit,
                        s.card_number_sal,
                        DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y %T'),
                        DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),
                        DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),
                        DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),
                        s.days_sal,
                        s.fee_sal,
                        s.observations_sal,
                        s.cost_sal,
                        s.free_sal,
                        s.id_erp_sal,
                        s.status_sal,
                        s.type_sal,
                        s.stock_type_sal,
                        s.total_sal,
                        s.direct_sale_sal,
                        s.total_cost_sal,
                        s.change_fee_sal,
                        s.sal_serial_sal,
                        s.international_fee_status,
                        s.international_fee_used,
                        s.international_fee_amount,
                        s.deliveredKit,
                        s.serial_con,
                        s.transaction_id,
						s.serial_rep,
						future_payment_sal,
                        s.observations_contract
                    FROM sales s
                    WHERE s.serial_sal ='" . $this->serial_sal . "'";
            $result = $this->db->Execute($sql);
            if ($result === false)
                return false;

            if ($result->fields[0]) {
                $this->serial_sal = $result->fields[0];
                $this->serial_cus = $result->fields[1];
                $this->serial_pbd = $result->fields[2];
                $this->serial_cnt = $result->fields[3];
                $this->serial_inv = $result->fields[4];
                $this->serial_cur = $result->fields[5];
                $this->serial_cit = $result->fields[6];
                $this->card_number_sal = $result->fields[7];
                $this->emission_date_sal = $result->fields[8];
                $this->begin_date_sal = $result->fields[9];
                $this->end_date_sal = $result->fields[10];
                $this->in_date_sal = $result->fields[11];
                $this->days_sal = $result->fields[12];
                $this->fee_sal = $result->fields[13];
                $this->observations_sal = $result->fields[14];
                $this->cost_sal = $result->fields[15];
                $this->free_sal = $result->fields[16];
                $this->id_erp_sal = $result->fields[17];
                $this->status_sal = $result->fields[18];
                $this->type_sal = $result->fields[19];
                $this->stock_type_sal = $result->fields[20];
                $this->total_sal = $result->fields[21];
                $this->direct_sale_sal = $result->fields[22];
                $this->total_cost_sal = $result->fields[23];
                $this->change_fee_sal = $result->fields[24];
                $this->sal_serial_sal = $result->fields[25];
                $this->international_fee_status = $result->fields[26];
                $this->international_fee_used = $result->fields[27];
                $this->international_fee_amount = $result->fields[28];
                $this->deliveredKit = $result->fields[29];
                $this->serial_con = $result->fields[30];
                $this->transaction_id = $result->fields[31];
                $this->serial_rep = $result->fields[32];
                $this->future_payment_sal = $result->fields[33];
                $this->observations_contract = $result->fields[34];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * function insert
      @Name: insert
      @Description: Inserts a new register in data base
      @Params:
     *       N/A
      @Returns: insertedID
     *          or
     *        false
     * ********************************************* */

    function insert()
    {
        $sql = "INSERT INTO sales (
                                serial_cus,
                                serial_con,
                                serial_pbd,
                                serial_cnt,
                                serial_inv,
				serial_cur,
                                serial_cit,
                                card_number_sal,
                                emission_date_sal,
                                begin_date_sal,
                                end_date_sal,
                                in_date_sal,
                                days_sal,
                                fee_sal,
                                observations_sal,
                                cost_sal,
                                free_sal,
                                id_erp_sal,
                                status_sal,
                                type_sal,
                                stock_type_sal,
                                total_sal,
                                direct_sale_sal,
                                total_cost_sal,
                                change_fee_sal,
                                sal_serial_sal,
                                international_fee_status,
                                international_fee_used,
                                international_fee_amount,
                                deliveredKit, 
                                transaction_id,
								future_payment_sal,
								serial_rep,
                                observations_contract)
                VALUES( '" . $this->serial_cus . "',";

        if ($this->serial_con != '') {
            $sql .= "'" . $this->serial_con . "',";
        } else {
            $sql .= "NULL,";
        }

        $sql .= "
                          '" . $this->serial_pbd . "',
                          '" . $this->serial_cnt . "',";

        if ($this->serial_inv != '') {
            $sql .= "'" . $this->serial_inv . "',";
        } else {
            $sql .= "NULL,";
        }
        if ($this->serial_cur != '') {
            $sql .= "'" . $this->serial_cur . "',";
        } else {
            $sql .= "1,";
        }
        if ($this->serial_cit != 'NULL') {
            $sql .= "'" . $this->serial_cit . "',";
        } else {
            $sql .= "NULL,";
        }
        if ($this->card_number_sal == 'N/A') {
            $sql .= "NULL,";
        } else {
            $sql .= "'" . $this->card_number_sal . "',";
        }
        $sql .= "STR_TO_DATE(CONCAT('" . $this->emission_date_sal . "',' ',(SELECT curtime())),'%d/%m/%Y %T'),
                        STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
                        STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y'),
                        CURRENT_TIMESTAMP(),
                        '" . $this->days_sal . "',
                        '" . ($this->fee_sal ? $this->fee_sal : "0.00") . "',
                        '" . $this->observations_sal . "',
                        '" . ($this->cost_sal ? $this->cost_sal : "0") . "',
                        '" . $this->free_sal . "',
                        " . ($this->id_erp_sal ? "'" . $this->id_erp_sal . "'" : "NULL") . ",
                        '" . $this->status_sal . "',
                        '" . $this->type_sal . "',
                        '" . $this->stock_type_sal . "',
                        '" . ($this->total_sal ? $this->total_sal : "0.00") . "',
                        " . ($this->direct_sale_sal ? "'" . $this->direct_sale_sal . "'" : "NULL") . ",
                        '" . ($this->total_cost_sal ? $this->total_cost_sal : "0") . "',
                        '" . $this->change_fee_sal . "',
                        " . ($this->sal_serial_sal ? "'" . $this->sal_serial_sal . "'" : "NULL") . ",
                        '" . $this->international_fee_status . "',
                        '" . $this->international_fee_used . "',
                        '" . ($this->international_fee_amount ? $this->international_fee_amount : "0.00") . "',
                        '" . ($this->deliveredKit ? $this->deliveredKit : "0") . "',
                        " . ($this->transation_id ? "'" . $this->transation_id . "'," : "NULL,") . "
						" . ($this->future_payment_sal ? "'" . $this->future_payment_sal . "'," : "NULL,") . "
						" . ($this->serial_rep ? "'" . $this->serial_rep . "'" : "NULL") . ",'
                        " . $this->observations_contract . "')";
        //Debug::print_r($sql); die;
        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /*     * *********************************************
     * function insert
      @Name: insert
      @Description: Inserts a new register in data base
      @Params:
     *       N/A
      @Returns: insertedID
     *          or
     *        false
     * ********************************************* */

    function update()
    {
        $sql = "UPDATE sales SET
			        serial_cus=" . $this->serial_cus . ",
					serial_pbd=" . $this->serial_pbd . ",
					serial_cnt=" . $this->serial_cnt . ",";
        if ($this->serial_inv != '') {
            $sql .= " serial_inv=" . $this->serial_inv . ",";
        } else {
            $sql .= " serial_inv = NULL,";
        }
        if ($this->serial_con != '') {
            $sql .= " serial_con=" . $this->serial_con . ",";
        } else {
            $sql .= " serial_con = NULL,";
        }

        if ($this->serial_cit != '') {
            $sql .= " serial_cit=" . $this->serial_cit . ",";
        } else {
            $sql .= " serial_cit = NULL,";
        }
        $sql .= "	card_number_sal='" . $this->card_number_sal . "',
                        emission_date_sal=STR_TO_DATE('" . $this->emission_date_sal . "','%d/%m/%Y %T'),
                        begin_date_sal=STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
                        end_date_sal=STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y'),
                        in_date_sal=STR_TO_DATE('" . $this->in_date_sal . "','%d/%m/%Y'),
                        days_sal='" . $this->days_sal . "',
                        fee_sal='" . $this->fee_sal . "',
                        observations_sal='" . $this->observations_sal . "',
                        cost_sal='" . $this->cost_sal . "',
                        free_sal='" . $this->free_sal . "',";
        if ($this->id_erp_sal != '') {
            $sql .= " id_erp_sal=" . $this->id_erp_sal . ",";
        } else {
            $sql .= " id_erp_sal = NULL,";
        }
        $sql .= "	status_sal='" . $this->status_sal . "',
				type_sal='" . $this->type_sal . "',
				total_sal='" . $this->total_sal . "',
				stock_type_sal='" . $this->stock_type_sal . "',
				direct_sale_sal='" . $this->direct_sale_sal . "',
				total_cost_sal='" . $this->total_cost_sal . "',
				change_fee_sal='" . $this->change_fee_sal . "',
				international_fee_status='" . $this->international_fee_status . "',
				international_fee_used='" . $this->international_fee_used . "',
				international_fee_amount='" . $this->international_fee_amount . "',
				future_payment_sal='" . $this->future_payment_sal . "'
			WHERE serial_sal=" . $this->serial_sal;
        $result = $this->db->Execute($sql);

        if ($result) {
            return true;
        } else {
            ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /*     * *********************************************
     * function updateInvoice
      @Name: updateInvoice
      @Description: updates serial_inv
      @Params:
     *       $serial_inv
      @Returns: true if updated succesfully
     *          or
     *        false if not
     * ********************************************* */

    function updateInvoice($serial_inv)
    {
        $sql = "UPDATE sales SET serial_inv ='" . $serial_inv . "'
				WHERE serial_sal = '" . $this->serial_sal . "'";

        $result = $this->db->Execute($sql);
        if ($result === false
        )
            return false;

        if ($result == true)
            return true;
        else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /*     * *********************************************
     * function updateStatus
      @Name: updateStatus
      @Description: updates status_sal and free_sal
      @Params:
     *       none
      @Returns: true if updated succesfully
     *          or
     *        false if not
     * ********************************************* */

    function updateStatus()
    {
        $sql = "UPDATE sales SET status_sal ='$this->status_sal',
                                  free_sal='$this->free_sal',
								  begin_date_sal= STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
								  end_date_sal= STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y')
				WHERE serial_sal = '" . $this->serial_sal . "'";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false
        )
            return false;

        if ($result == true)
            return true;
        else
            return false;
    }

    /*     * *********************************************
     * function voidSerial_inv
      @Name: voidSerial_inv
      @Description: sets serial_inv null
      @Params:
     *       serial_inv
      @Returns: true if updated succesfully
     *          or
     *        false if not
     * ********************************************* */

    function voidSerial_inv($serial_inv)
    {
        $sql = "UPDATE sales SET serial_inv =NULL
            WHERE serial_inv=" . $serial_inv;
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)
            return false;

        if ($result == true)
            return true;
        else
            return false;
    }

    /*     * *********************************************
     * function updateStatusByInvoice
      @Name: updateStatusByInvoice
      @Description: updates status_sal of a given serial_inv
      @Params:
     *       none
      @Returns: true if updated succesfully
     *          or
     *        false if not
     * ********************************************* */

    function updateStatusByInvoice()
    {
        $sql = "UPDATE sales SET status_sal ='" . $this->status_sal . "'
				 WHERE serial_inv = '" . $this->serial_inv . "'
				 AND status_sal NOT IN ('VOID','BLOCKED')";

        $result = $this->db->Execute($sql);
        if ($result === false
        )
            return false;

        if ($result == true)
            return true;
        else
            return false;
    }

    /*     * *********************************************
     * function insertServices
      @Name: insertServices
      @Description: Inserts a new register in data base
      @Params:
     *       serial_sal , serial_sbc
      @Returns: true
     *          or
     *        false
     * ********************************************* */

    function insertServices($serial_sal, $serial_sbd)
    {
        $sbcbd = new ServicesByCountryBySale($this->db);
        $sbcbd->setSerial_sal($serial_sal);
        $sbcbd->setSerial_sbd($serial_sbd);
        if ($sbcbd->insert()) {
            return true;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * function getSalesTotal
      @Name: getSalesTotal
      @Description: Get the total sales using the sales ids recived.
      @Params:
     *       $sales: string with sales serial separated by ,
      @Returns: double with sales total.
     * ************************************************ */

    function getSalesTotal($sales)
    {
        if (!$sales)
            return NULL;
        $sql = "
		  SELECT SUM(s.total_sal)
		  	FROM sales s
		  WHERE s.serial_sal IN (" . $sales . ")
		";
        //echo $sql." ";
        $result = $this->db->getOne($sql);
        if ($result === false)
            die("failed getSalesTotal");
        if ($result)
            return $result;
        else
            return NULL;
    }

    /*     * *********************************************
     * function getSalesTotal
      @Name: getSalesTotal
      @Description: Get the total sales using the sales ids recived.
      @Params:
     *       $sales: string with sales serial separated by ,
      @Returns: double with sales total.
     * ************************************************ */

    function getCostTotal($sales)
    {
        if (!$sales)
            return NULL;
        $sql = "
		  SELECT SUM(s.total_cost_sal)
		  	FROM sales s
		  WHERE s.serial_sal IN (" . $sales . ")
		";
        //echo $sql." ";
        $result = $this->db->getOne($sql);
        if ($result === false)
            die("failed getCostTotal");
        if ($result)
            return $result;
        else
            return NULL;
    }

    /**
     * function getMasiveSales
     * @Name: getMasiveSales
     * @Description: Get the sales that corresponds to masive products
     * @Params:
     * @Returns: array with masive sales
     * */
    function getMasiveSales($code_lang)
    {
        $sql = "SELECT pbl.name_pbl, sal.serial_sal, sal.card_number_sal
				FROM sales sal
				JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product pro ON pxc.serial_pro=pro.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro
				JOIN language lang ON pbl.serial_lang = lang.serial_lang AND lang.code_lang = '$code_lang'
				WHERE (pro.third_party_register_pro = 'YES' OR pro.masive_pro = 'YES')
				AND (sal.status_sal IN ('ACTIVE','REGISTERED'))
				AND (sal.end_date_sal > CURDATE())";
        //die($sql);

        $result = $this->db->Execute($sql);
        if ($result === false)
            die("failed getMasiveSales");
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    /**
     * @Name: getMasiveSales
     * @Description: Returns an array of masive sales.
     * @Params: class atribute serial_dea
     * @Returns: Sales array
     * */
    function getSubMasiveSales()
    {
        $sql = "SELECT 	s.serial_pbd,
							s.serial_sal,
							s.card_number_sal,
							pbl.name_pbl,
							cus.first_name_cus as legal_entity_name,
 							CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
							DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,
							FORMAT(s.total_sal * s.change_fee_sal,2) as total_sal,
							cur.symbol_cur,
							s.status_sal,
							pxc.serial_pxc,
							IF(COUNT(tl.serial_trl) > 0, COUNT(tl.serial_trl), 'NO') AS 'registered_people'
                    FROM sales s
                    JOIN counter c ON s.serial_cnt=c.serial_cnt
                    JOIN dealer d ON d.serial_dea=c.serial_dea 
                    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
                    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
                    JOIN product pr ON pr.serial_pro=pxc.serial_pro AND pr.masive_pro='YES' AND s.sal_serial_sal=" . $this->serial_sal . "
                    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
                    JOIN customer cus ON cus.serial_cus=s.serial_cus
					JOIN currency cur ON cur.serial_cur = s.serial_cur
					LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal
					GROUP BY s.serial_sal
                    ORDER BY s.emission_date_sal DESC,s.card_number_sal
                    ";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result) {
            $num = $result->RecordCount();
            if ($num > 0) {
                $list = array();
                $cont = 0;

                do {
                    $list[$cont] = $result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                } while ($cont < $num);
            }
        }
        return $list;
    }

    /**
     * @Name: getSalesByInvoice
     * @Description: Returns an array of sales availables for invoice.
     * @Params: serial invoice
     * @Returns: Sales array
     * */
    function getSalesByInvoice($serial_inv)
    {
        $sql = "SELECT s.serial_pbd,s.serial_sal,s.card_number_sal,pbl.name_pbl,cus.first_name_cus as legal_entity_name,
					   CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,
					   s.total_sal,s.status_sal, s.total_cost_sal, s.serial_cnt,
					   inv.discount_prcg_inv, inv.other_dscnt_inv, inv.applied_taxes_inv, id_erp_sal, inv.applied_taxes_inv
				FROM sales s
				JOIN counter c ON s.serial_cnt=c.serial_cnt
				JOIN dealer d ON d.serial_dea=c.serial_dea
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
				JOIN product pr ON pr.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				JOIN invoice inv ON s.serial_inv=inv.serial_inv
				WHERE s.serial_inv=" . $serial_inv . "
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }

    /**
     * @Name: getSalesByInvoice
     * @Description: Returns an array of sales availables for invoice.
     * @Params: serial invoice
     * @Returns: Sales array
     * */
    function getSalesByInvoiceLog($serial_inv)
    {
        $sql = "SELECT s.serial_pbd, s.serial_sal, s.card_number_sal, pbl.name_pbl, cus.first_name_cus as legal_entity_name,
					   CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
					   DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date, s.total_sal, s.status_sal,
					   s.total_cost_sal, IF(s.begin_date_sal<=NOW(), 'YES', 'NO') AS 'international_fee_validation'
				FROM sales s
				JOIN invoice_log inl ON s.serial_sal=inl.serial_sal AND inl.void_sales_inl='YES' AND inl.status_inl='PENDING'
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
				JOIN product pr ON pr.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				WHERE inl.serial_inv=" . $serial_inv . "
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";
        //die($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }

    /**
     * @Name: getSaleProduct
     * @Description: Returns an array with the information of a specific sale.
     * @Params: none
     * @Returns: Sale array or False
     * */
    function getSaleProduct($serial_lang)
    {
        $sql = "SELECT pbl.name_pbl, d.name_dea, CONCAT(u.first_name_usr,' ', u.last_name_usr) as 'counter', cit.name_cit,
						   CONCAT(cou.code_cou,'-',cit.code_cit,'-',dea.code_dea,'-',d.code_dea) as 'code', cits.name_cit city_sal,
						   cous.name_cou country_sal, s.card_number_sal, DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y %T') AS 'emission_date_sal',
						   DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal', DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',s.emission_date_sal AS 'date_sal',
						   (s.total_sal * change_fee_sal) AS 'total_sal', 
						   (s.fee_sal * change_fee_sal) AS 'fee_sal', 
						   s.days_sal, p.flights_pro, p.masive_pro, d.logo_dea as logo, p.serial_pro, s.observations_contract
                    FROM sales s
                    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
                    JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
                    JOIN product p ON p.serial_pro=pxc.serial_pro
                    JOIN benefits_product bp ON bp.serial_pro=p.serial_pro
                    JOIN benefits b ON b.serial_ben=bp.serial_ben
                    JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben
                    JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = " . $serial_lang . "
                    JOIN counter c ON c.serial_cnt=s.serial_cnt
                    JOIN dealer d ON c.serial_dea=d.serial_dea
                    JOIN dealer dea ON dea.serial_dea=d.dea_serial_dea
                    JOIN sector sec ON sec.serial_sec=d.serial_sec
                    JOIN city cit ON cit.serial_cit=sec.serial_cit
                    JOIN country cou ON cou.serial_cou=cit.serial_cou
                    JOIN user u ON u.serial_usr=c.serial_usr
					LEFT JOIN city cits ON cits.serial_cit =  s.serial_cit
					LEFT JOIN country cous ON cous.serial_cou = cits.serial_cou
                    WHERE s.serial_sal='" . $this->serial_sal . "'";
//        Debug::print_r($sql);die();
        $result = $this->db->Execute($sql);
        if ($result) {
            return $result->GetRowAssoc(false);
        } else {
            return false;
        }
    }

    /**
     * @Name: getSaleServices
     * @Description: Returns an array with the services in a specific sale.
     * @Params: none
     * @Returns: Two-dimensional array or False
     * */
    function getSaleServices($serial_lang, $active_service_only = FALSE)
    {
        $sql = "SELECT s.serial_sal, sbd.serial_sbd, sbc.serial_sbc, sbc.status_sbc, sbc.price_sbc, sbc.cost_sbc, sbc.coverage_sbc, sbl.name_sbl, ser.fee_type_ser
				FROM sales s
						JOIN services_by_country_by_sale sbcbs ON s.serial_sal = sbcbs.serial_sal
						JOIN services_by_dealer sbd ON sbcbs.serial_sbd = sbd.serial_sbd
						JOIN services_by_country sbc ON sbd.serial_sbc = sbc.serial_sbc
						JOIN services ser ON sbc.serial_ser = ser.serial_ser
						JOIN services_by_language sbl ON ser.serial_ser = sbl.serial_ser
				WHERE s.serial_sal = '" . $this->serial_sal . "' AND sbl.serial_lang=" . $serial_lang . "";

        if ($active_service_only) {
            $sql .= " AND ser.status_ser = 'ACTIVE'";
        }

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;
            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
            return $arr;
        } else {
            return false;
        }
    }

    /**
     * @Name: getSaleBenefits
     * @Description: Returns an array with the benefits in a specific sale.
     * @Params: none
     * @Returns: Two-dimensional array or False
     * */
    function getSaleBenefits($serial_lang)
    {
        $sql = "SELECT bbl.description_bbl, 
						FORMAT(bp.price_bxp * change_fee_sal, 2) AS 'price_bxp', 
						FORMAT(bp.restriction_price_bxp * change_fee_sal, 2) AS 'restriction_price_bxp', 
						rbl.description_rbl,
						IF(c.serial_con = 4, FORMAT(bp.price_bxp * change_fee_sal, 2), cbl.description_cbl) as alias_con, 
						bbl.serial_lang, b.serial_bcat
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
			JOIN benefits_product bp ON bp.serial_pro = pxc.serial_pro AND bp.status_bxp = 'ACTIVE'
			JOIN conditions c ON c.serial_con = bp.serial_con
			JOIN conditions_by_language cbl ON cbl.serial_con = c.serial_con AND cbl.serial_lang = $serial_lang
			JOIN product_by_language pbl ON pxc.serial_pro = pbl.serial_pro AND pbl.serial_lang = $serial_lang
			LEFT JOIN restriction_type rt ON rt.serial_rst = bp.serial_rst AND rt.status_rst = 'ACTIVE'
			LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = pbl.serial_lang
			JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
			JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang = $serial_lang
			WHERE s.serial_sal='" . $this->serial_sal . "'
			ORDER BY b.weight_ben";

        //Debug::print_r($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;
            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
            return $arr;
        }
        return false;
    }

    /*
     * @Name: getFreeSales
     * @Description: Returns an array of all the free sales pending to be authorized.
     * @Params: N/A
     * @Returns: An array of free sales.
     */

    public static function getFreeSales($db, $serial_cou, $serial_cit = NULL, $serial_usr = NULL, $serial_dea = NULL, $dea_serial_dea = NULL)
    {
        $f1 = '';
        $f2 = '';
        $f3 = '';

        if ($serial_cit) {
            $f1 = "AND cit.serial_cit = $serial_cit";
        }
        if ($serial_usr) {
            $f2 = "AND ubd.serial_usr = $serial_usr";
        }
        if ($serial_dea) {
            $f3 = "AND dea.dea_serial_dea = $serial_dea";
        }
        if ($dea_serial_dea) {
            $f3 = "AND dea.serial_dea = $dea_serial_dea";
        }

        $sql = "SELECT s.*, pbl.name_pbl
                    FROM sales s
                    JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
                    JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
                    JOIN dealer dea ON dea.serial_dea=cnt.serial_dea $f3
                    JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea $f2
                    JOIN sector sec ON sec.serial_sec = dea.serial_sec
                    JOIN city cit ON cit.serial_cit = sec.serial_cit $f1
                    JOIN product_by_country pbc ON pbc.serial_pxc=pbd.serial_pxc
                    JOIN country cou ON cou.serial_cou = cit.serial_cou AND cou.serial_cou = $serial_cou
                    JOIN product_by_language pbl ON pbc.serial_pro=pbl.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
                    WHERE s.free_sal = 'YES'
                    AND s.status_sal = 'REQUESTED'
                    ORDER BY s.in_date_sal";

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    /*
     * @Name: getSalesStatus
     * @Description: Returns an array of all the sale satatus in DB.
     * @Params: $db: DB connection
     * @Returns: An array of free sales.
     */

    public static function getSalesStatus($db)
    {
        $sql = "SHOW COLUMNS FROM sales LIKE 'status_sal'";
        $result = $db->Execute($sql);

        $type = $result->fields[1];
        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    public static function checkSaleHaveOtherDiscount($db, $sId)
    {
        $sql = "SELECT 1 FROM cupon WHERE consuming_sal='$sId'";

        $result = $db->getOne($sql);
        if ($result == 1)
            return 1;
        else
            return 0;
    }

    /*
     * @Name: getSalesTypes
     * @Description: Returns an array of all the sale types in DB.
     * @Params: $db: DB connection
     * @Returns: An array of free sales.
     */

    public static function getSalesTypes($db)
    {
        $sql = "SHOW COLUMNS FROM sales LIKE 'type_sal'";
        $result = $db->Execute($sql);

        $type = $result->fields[1];
        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    /*
     * @Name: cardNumberExists
     * @Description: Returns true or false if the card exists or not
     * @Params: -
     * @Returns: True/False
     */

    function getDataByCardNumber()
    {
        if ($this->card_number_sal != NULL) {
            $sql = "SELECT DISTINCT s.serial_sal,
                                    s.serial_cus,
                                    s.serial_pbd,
                                    s.serial_cit,
                                    s.serial_cnt,
                                    s.serial_inv,
                                    IFNULL(s.card_number_sal,trl.card_number_trl) as card_number_sal,
                                    DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y %T'),
                                    DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y'),
                                    DATE_FORMAT(s.end_date_sal, '%d/%m/%Y'),
                                    DATE_FORMAT(s.in_date_sal, '%d/%m/%Y %T'),
                                    s.days_sal,
                                    s.fee_sal,
                                    s.observations_sal,
                                    s.cost_sal,
                                    s.free_sal,
                                    s.id_erp_sal,
                                    s.status_sal,
                                    s.type_sal,
                                    s.stock_type_sal,
                                    s.total_sal,
                                    s.direct_sale_sal,
                                    s.total_cost_sal,
                                    s.change_fee_sal,
                                    cit.name_cit,
                                    cou.name_cou,
                                    cit2.name_cit,
                                    cou2.name_cou,
                                    pbl.name_pbl,
                                    pbl.serial_pbl,
                                    d.name_dea,
									s.international_fee_status,
									s.international_fee_used,
									s.international_fee_amount,
									MAX(trl.serial_trl) AS 'serial_trl',
									serial_rep
                            FROM sales s
                            JOIN customer c ON c.serial_cus=s.serial_cus
                            LEFT JOIN city cit ON cit.serial_cit=s.serial_cit
                            JOIN city cit2 ON cit2.serial_cit=c.serial_cit
                            LEFT JOIN country cou ON cou.serial_cou=cit.serial_cou
                            JOIN country cou2 ON cou2.serial_cou=cit2.serial_cou
                            JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
                            JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
                            JOIN product p ON p.serial_pro=pxc.serial_pro
                            JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
                            LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                            JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
                            JOIN dealer d ON cnt.serial_dea=d.serial_dea
                            WHERE s.card_number_sal ='" . $this->card_number_sal . "'
                            OR trl.card_number_trl ='" . $this->card_number_sal . "'";


            $result = $this->db->Execute($sql);
            if ($result === false)
                return false;

            if ($result->fields[0]) {
                $this->serial_sal = $result->fields[0];
                $this->serial_cus = $result->fields[1];
                $this->serial_pbd = $result->fields[2];
                $this->serial_cit = $result->fields[3];
                $this->serial_cnt = $result->fields[4];
                $this->serial_inv = $result->fields[5];
                $this->card_number_sal = $result->fields[6];
                $this->emission_date_sal = $result->fields[7];
                $this->begin_date_sal = $result->fields[8];
                $this->end_date_sal = $result->fields[9];
                $this->in_date_sal = $result->fields[10];
                $this->days_sal = $result->fields[11];
                $this->fee_sal = $result->fields[12];
                $this->observations_sal = $result->fields[13];
                $this->cost_sal = $result->fields[14];
                $this->free_sal = $result->fields[15];
                $this->id_erp_sal = $result->fields[16];
                $this->status_sal = $result->fields[17];
                $this->type_sal = $result->fields[18];
                $this->stock_type_sal = $result->fields[19];
                $this->total_sal = $result->fields[20];
                $this->direct_sale_sal = $result->fields[21];
                $this->total_cost_sal = $result->fields[22];
                $this->change_fee_sal = $result->fields[23];
                $this->aux['destinationCity'] = $result->fields[24];
                $this->aux['destinationCountry'] = $result->fields[25];
                $this->aux['customerCity'] = $result->fields[26];
                $this->aux['customerCountry'] = $result->fields[27];
                $this->aux['product'] = $result->fields[28];
                $this->aux['serial_pbl'] = $result->fields[29];
                $this->aux['name_dea'] = $result->fields[30];
                $this->international_fee_status = $result->fields[31];
                $this->international_fee_used = $result->fields[32];
                $this->international_fee_amount = $result->fields[33];
                $this->aux['serial_trl'] = $result->fields[34];
                $this->serial_rep = $result->fields[35];

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function getAssistanceDataByCardNumber($db, $card_number)
    {
        if (TravelerLog::cardNumberExistInTravelerLog($db, $card_number)) {
            $join_sales = "JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd";
            $join_traveler_log = "JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal 
									AND trl.card_number_trl LIKE '$card_number'";
        } else {
            $join_sales = "JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd AND sal.card_number_sal LIKE '$card_number'";
            $join_traveler_log = "LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal";
        }

        $sql = "SELECT sal.serial_sal, trl.serial_trl, HOUR(TIMEDIFF(NOW(),sal.emission_date_sal)) as days_since_emition,
					   sal.status_sal, inv.status_inv, DATEDIFF(inv.due_date_inv,NOW()) as days_to_dueDate, pbl.name_pbl,
					   CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_cnt, dea.name_dea, dea.dea_serial_dea, cou.name_cou,
					   man.name_man
				FROM sales sal
				$join_traveler_log
				LEFT JOIN invoice inv ON inv.serial_inv = sal.serial_inv
				$join_sales
                JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou";
        //die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);

        if ($result[0]) {
            return $result[0];
        } else {
            return FALSE;
        }
    }

    /*
     * @Name: cardNumberExists
     * @Description: Returns true or false if the card exists or not
     * @Params: -
     * @Returns: True/False
     */

    public static function cardNumberExists($db, $card_number)
    {
        $sql = "SELECT IFNULL(s.card_number_sal,trl.card_number_trl) as card_number_sal
                 FROM sales s
                 LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                 WHERE s.card_number_sal = '" . $card_number . "'
                 OR trl.card_number_trl = '" . $card_number . "'";
        //die($sql);
        $result = $db->Execute($sql);

        if ($result->fields[0]) {
            return $result->fields[0];
        } else {
            return false;
        }
    }

    /*
     * @Name: getCardBenefits
     * @Description: Returns all the benefits that covers a card
     * @Params: -
     * @Returns: array
     */

    public static function getCardBenefits($db, $card_number)
    {
        $sql = "(
				SELECT DISTINCT bbp.*, cbl.description_cbl as alias_con, bbl.description_bbl, cur.symbol_cur,
						con.alias_con
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN benefits_product bbp ON bbp.serial_pro = pxc.serial_pro AND bbp.serial_cur='1' and bbp.status_bxp = 'ACTIVE'
				JOIN conditions con ON con.serial_con = bbp.serial_con
				JOIN benefits_by_language bbl ON bbl.serial_ben = bbp.serial_ben AND bbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN conditions_by_language cbl ON cbl.serial_con = bbp.serial_con AND cbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN currency cur ON cur.serial_cur = bbp.serial_cur
				WHERE s.card_number_sal = $card_number
			) UNION (
				SELECT DISTINCT bbp.*, cbl.description_cbl as alias_con, bbl.description_bbl, cur.symbol_cur,
						con.alias_con
				FROM sales s
				JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN benefits_product bbp ON bbp.serial_pro = pxc.serial_pro AND bbp.serial_cur='1' and bbp.status_bxp = 'ACTIVE'
				JOIN conditions con ON con.serial_con = bbp.serial_con
				JOIN benefits_by_language bbl ON bbl.serial_ben = bbp.serial_ben AND bbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN conditions_by_language cbl ON cbl.serial_con = bbp.serial_con AND cbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN currency cur ON cur.serial_cur = bbp.serial_cur
				WHERE trl.card_number_trl = $card_number
                AND bbp.status_bxp = 'ACTIVE'
			)";

        //die(Debug::print_r($sql));

        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arreglo;
    }

    /*
     * @Name: getCardBeneficiaries
     * @Description: Returns all the beneficiaries covered by a card
     * @Params: -
     * @Returns: array
     */

    public static function getCardBeneficiaries($db, $card_number)
    {

        $pos = strpos($card_number, '_');

        if ($pos === false) {
            $sql = "SELECT c.*, DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as b_date, cou.name_cou, cit.name_cit,
                       IF(tl.start_trl ,DATE_FORMAT(MAX(tl.start_trl) ,'%d/%m/%Y'),DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')) as start_date, IF(tl.end_trl,DATE_FORMAT(tl.end_trl,'%d/%m/%Y'),DATE_FORMAT(s.end_date_sal,'%d/%m/%Y')) as end_date,
                       IFNULL(cit_des.name_cit,cit_trl.name_cit) as city_des, IFNULL(cou_des.name_cou,cou_trl.name_cou) as country_des, tl.serial_trl
                FROM sales s
                JOIN customer c ON c.serial_cus = s.serial_cus
                JOIN city cit ON cit.serial_cit = c.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                LEFT JOIN city cit_des ON cit_des.serial_cit = s.serial_cit
                LEFT JOIN country cou_des ON cou_des.serial_cou = cit_des.serial_cou
                LEFT JOIN traveler_log tl ON tl.serial_sal= s.serial_sal
                LEFT JOIN city cit_trl ON cit_trl.serial_cit = tl.serial_cit
                LEFT JOIN country cou_trl ON cou_trl.serial_cou = cit_trl.serial_cou
                WHERE s.card_number_sal LIKE '$card_number'
                OR tl.card_number_trl LIKE '$card_number'
                GROUP BY c.serial_cus

                UNION";
        } else {
            $sql = "";
        }
        $sql .= " SELECT c.*, DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as b_date, cou.name_cou, cit.name_cit,
                       DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as start_date, DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date,
                       cit_des.name_cit as city_des, cou_des.name_cou as country_des, e.serial_trl
                FROM extras e
                JOIN customer c ON c.serial_cus = e.serial_cus
                JOIN city cit ON cit.serial_cit = c.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                JOIN sales s ON s.serial_sal = e.serial_sal
                LEFT JOIN city cit_des ON cit_des.serial_cit = s.serial_cit
                LEFT JOIN country cou_des ON cou_des.serial_cou = cit_des.serial_cou
                WHERE s.card_number_sal LIKE '$card_number'

				UNION

				SELECT c.*, DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as b_date, cou.name_cou, cit.name_cit,
					   DATE_FORMAT(MAX(tl.start_trl) ,'%d/%m/%Y') as start_date, DATE_FORMAT(tl.end_trl,'%d/%m/%Y') as end_date,
					   cit_trl.name_cit as city_des, cou_trl.name_cou as country_des, tl.serial_trl
				FROM sales s
				JOIN traveler_log tl ON tl.serial_sal= s.serial_sal
				JOIN customer c ON c.serial_cus = tl.serial_cus
				JOIN city cit ON cit.serial_cit = c.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou
				JOIN city cit_trl ON cit_trl.serial_cit = tl.serial_cit
				JOIN country cou_trl ON cou_trl.serial_cou = cit_trl.serial_cou
				WHERE tl.card_number_trl LIKE '$card_number'
				GROUP BY c.serial_cus";
        //die(Debug::print_r($sql));
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arreglo;
    }

    /*
     * @Name: daysSinceEmission
     * @Description: Checks how many hours have pased since card emission
     * @Params: -
     * @Returns: boolean
     */

    function daysSinceEmission()
    {
        $sql = "SELECT HOUR(TIMEDIFF(NOW(),s.emission_date_sal))
                FROM sales s
                LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                WHERE s.card_number_sal = '" . $this->card_number_sal . "'
                OR trl.card_number_trl = '" . $this->card_number_sal . "'";
        //die($sql);
        $result = $this->db->getOne($sql);
        if ($result === false)
            die("failed daysSinceEmission");
        if ($result)
            return $result;
        else
            return NULL;
    }

    /*     * *********************************************
     * function validateRegisterCard
      @Name: validateRegisterCard
      @Description: Checks if a card available for travels register
      @Params:
     *   $this->card_number_sal: object atribute for card_number_sal
      @Returns: true: when is executive
     *    false: when is not executive
     * ********************************************* */

    function validateRegisterCard()
    {
        $sql = "SELECT 1
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro
			LEFT JOIN traveler_log trl ON trl.serial_sal=s.serial_sal
			WHERE (s.card_number_sal={$this->card_number_sal}
				   OR trl.card_number_trl={$this->card_number_sal})
			AND p.flights_pro=0";
        //die($sql);
        $result = $this->db->getOne($sql);
        if ($result)
            return true;
        else
            return false;
    }

    /*     * *********************************************
     * function getCardInfo
      @Name: getCardInfo
      @Description: Returns an array with the info of a sale , based on card_number_sal
      @Params:
     *   object attribute: card_number_sal
      @Returns: array: with all info
     *    false: when there is no info for the card_number given
     * ********************************************* */

    function getCardInfo($register = true)
    {
        if (!$register) {
            $report_filter = "AND s.end_date_sal >= CURDATE()";
        }

        $sql = "SELECT s.serial_sal, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
					pbl.name_pbl, s.card_number_sal, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal, s.days_sal, 
					IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal) as available_days,
					s.status_sal
				FROM sales s
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro and pbl.serial_lang = 2
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal AND tl.status_trl <> 'DELETED'
				WHERE (s.card_number_sal={$this->card_number_sal}
					OR tl.card_number_trl={$this->card_number_sal})
			$report_filter 
			GROUP BY s.serial_sal
			ORDER BY s.begin_date_sal";
        //echo $sql;
        $result = $this->db->getRow($sql);
        if ($result)
            return $result;
        else
            return false;
    }

    /*     * *********************************************
     * function getAvailableDays
      @Name: getAvailableDays
      @Description: returns the available days for a card by serial_sal
      @Params:
     *   object attribute: serial_sal
      @Returns: int: number of days available
     * ********************************************* */

    function getAvailableDays()
    {
        $sql = "SELECT IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal)as available_days
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
			LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal AND status_trl <> 'DELETED'
			WHERE s.serial_sal={$this->serial_sal}
			GROUP BY s.serial_sal";
        //echo $sql;
        $result = $this->db->getOne($sql);
        if ($result >= 0)
            return $result;
        else
            return false;
    }

    /*     * *********************************************
     * function getSaleDates
      @Name: getSaleDates
      @Description: Returns an array with all sales of a customer and the begin and end dates
      @Params:
     *   $serial_cus: object atribute for card_number_sal
      @Returns: true: when is executive
     *    false: when is not executive
     * ********************************************* */

    function getSaleDates($serial_cus, $start_dt, $end_dt, $excludeSaleId = NULL)
    {
        if ($excludeSaleId && $excludeSaleId != "undefined") {
            $excludeSQL = " AND s.serial_sal <> $excludeSaleId";
        }

        $sql = "SELECT 1
			FROM sales s
			WHERE s.serial_cus='$serial_cus'
			$excludeSQL
			AND((STR_TO_DATE('$start_dt','%d/%m/%Y') BETWEEN  DATE_ADD(s.begin_date_sal,INTERVAL 1 DAY) AND SUBDATE(s.end_date_sal,INTERVAL 1 DAY))
		     OR (STR_TO_DATE('$end_dt','%d/%m/%Y') BETWEEN  DATE_ADD(s.begin_date_sal,INTERVAL 1 DAY) AND   SUBDATE(s.end_date_sal,INTERVAL 1 DAY))
			 OR (STR_TO_DATE('$start_dt','%d/%m/%Y')=s.begin_date_sal AND STR_TO_DATE('$end_dt','%d/%m/%Y')=s.end_date_sal)
		     OR (s.begin_date_sal BETWEEN DATE_ADD(STR_TO_DATE('$start_dt','%d/%m/%Y'),INTERVAL 1 DAY) AND SUBDATE(STR_TO_DATE('$end_dt','%d/%m/%Y'),INTERVAL 1 DAY))
			 OR (s.end_date_sal BETWEEN DATE_ADD(STR_TO_DATE('$start_dt','%d/%m/%Y'),INTERVAL 1 DAY) AND SUBDATE(STR_TO_DATE('$end_dt','%d/%m/%Y'),INTERVAL 1 DAY)))
			AND (s.status_sal='ACTIVE' OR s.status_sal='REGISTERED' OR s.status_sal='REQUESTED' OR s.status_sal='STANDBY')";

        //echo $sql;
        $result = $this->db->getOne($sql);
        if ($result == 1)
            return true;
        else
            return false;
    }

    /*     * *********************************************
     * @Name: isDirectSale
     * @Description: Verifies if the sale was made by a Planet Assist Official Dealer.
     * @Params: $serial_cnt
     * @Returns: true: when is Direct Sale
     *          false: otherwise.
     * ********************************************* */

    function isDirectSale($serial_cnt)
    {
        $sql = "SELECT mbc.serial_mbc
			  FROM counter c 
			  JOIN dealer b ON b.serial_dea=c.serial_dea
			  JOIN dealer d ON d.serial_dea=b.dea_serial_dea AND d.official_seller_dea='YES'
			  JOIN manager_by_country mbc ON mbc.serial_mbc=d.serial_mbc AND mbc.official_mbc='YES'
			  WHERE c.serial_cnt='" . $serial_cnt . "'";
        //echo $sql;
        $result = $this->db->Execute($sql);

        if ($result->fields[0]) {
            return 'YES';
        } else {
            return 'NO';
        }
    }

    /*     * *********************************************
     * @Name: isRefundable
     * @Description: Verifies if a card is refundable. The conditions for a card to be
     *               refundable is that the sale is completly paid.
     * @Params: $serial_sal: object atribute for serial_sal
     * @Returns: true: when is executive
     * 			false: when is not executive
     * ********************************************* */

    public static function isRefundable($db, $serial_sal)
    {
        $sql = "SELECT serial_sal
			FROM sales s
			WHERE s.serial_sal='" . $serial_sal . "'
			AND s.status_sal='STANDBY'";

        $result = $db->Execute($sql);

        if ($result->fields['0']) {
            return true;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * @Name: hasTravelsRegistered
     * @Description: Verifies if the card permits has any travel registered in the traveler´s log.
     * @Params: $serial_sal: object atribute for serial_sal
     * @Returns: true: if ther's any travel log.
     * 			false: otherwise.
     * ********************************************* */

    public static function hasTravelsRegistered($db, $serial_sal)
    {
        $sql = "SELECT s.serial_sal
				FROM sales s
				JOIN traveler_log trl ON trl.serial_sal=s.serial_sal
				WHERE s.serial_sal=" . $serial_sal . "
				AND trl.status_trl <> 'DELETED'";

        $result = $db->Execute($sql);

        if ($result->fields['0']) {
            return true;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * function changeStatus
     * change the status of an specific invoice
     * ********************************************* */

    function changeStatus($status)
    {
        $sql = "UPDATE sales SET status_sal='" . $status . "'
                  WHERE serial_sal=" . $this->serial_sal;
        $result = $this->db->Execute($sql);
        if ($result === false)
            return false;

        if ($result == true)
            return true;
        else
            return false;
    }

    /*     * *********************************************
     * function getGeneralConditionsbySale
      @Name: getGeneralConditionsbySale
      @Description: Returns the url where is the General Conditions file for the product sold
      @Params:
     *   $serial_pbd: serial product by dealer
     * 	 $serial_sal: serial of the sale
      @Returns: true: url
     *    false: when is not executive
     * ********************************************* */

    function getGeneralConditionsbySale($serial_pbd, $serial_sal, $serial_cou, $serial_lang)
    {
        /*$sql = "	SELECT DISTINCT cbp.serial_gcn, cbp.serial_pro, s.serial_pbd, glc.url_glc
					FROM conditions_by_product cbp
					JOIN general_conditions gcn ON gcn.serial_gcn = cbp.serial_gcn AND gcn.status_gcn = 'ACTIVE'
					JOIN product_by_country pxc ON pxc.serial_pro = cbp.serial_pro
					JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
					JOIN sales s ON s.serial_pbd = pbd.serial_pbd AND s.serial_pbd = $serial_pbd
						AND s.serial_sal = $serial_sal
					JOIN gconditions_by_language_by_country glc ON glc.serial_gcn = cbp.serial_gcn
						AND glc.serial_cou = $serial_cou
						AND glc.serial_lang = $serial_lang";*/

        $sql = " SELECT  DISTINCT cbp.serial_gcn, cbp.serial_pro, s.serial_pbd, glc.url_glc
					FROM conditions_by_product cbp
					JOIN general_conditions gcn ON gcn.serial_gcn = cbp.serial_gcn 
					JOIN product_by_country pxc ON pxc.serial_pro = cbp.serial_pro
					JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
					JOIN sales s ON s.serial_pbd = pbd.serial_pbd 
					JOIN gconditions_by_language_by_country glc ON glc.serial_gcn = cbp.serial_gcn
                    WHERE gcn.status_gcn = 'ACTIVE'
                    AND s.serial_pbd = $serial_pbd
                    AND s.serial_sal = $serial_sal
                    AND glc.serial_lang = $serial_lang";

//        die($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        $list = array();
        if ($num > 0) {
            $cont = 0;
            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $list;
    }

    /**
     * */
    function getSaleUserMail($serial_sal)
    {
        $sql = "
                    SELECT usr.email_usr
                    FROM sales sal
                    JOIN counter cnt ON sal.serial_cnt = cnt.serial_cnt
                    JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
                    JOIN sector sec ON dea.serial_sec = sec.serial_sec
                    JOIN city cit ON sec.serial_cit = cit.serial_cit
                    JOIN country cou ON cit.serial_cou = cou.serial_cou
                    JOIN comissionist_by_country cbc ON cou.serial_cou = cbc.serial_cou
                    JOIN user usr ON cbc.serial_usr = usr.serial_usr
                    WHERE sal.serial_sal ='" . $serial_sal . "'
                    LIMIT 0,1
                    ";

        $result = $this->db->getOne($sql);
        if ($result) {
            $this->aux = $result;
            return true;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * function validateCardForModify
      @Name: validateCardForModify
      @Description: Return if a card number is valid to register travels, or is a registered travel card
      @Params:
     *   $card_number: the card number to be checked
     *   $for_validation: when true returns true or false, otherwise returns,'sale', 'travel_log' OR false
      @Returns:
     *    for_validation = true:	true:when found
     * 										false:when not found
     * 							   false:	'sale': when found on sales table
     * 										'travel_log' when found on traveler_log table
     * 										false: when not found
     *
     * ********************************************* */

    function validateCardForModify($card_number, $for_validation = true)
    {
        $sql = "SELECT 1
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro
			WHERE s.card_number_sal=$card_number
			AND p.flights_pro=0
			AND p.masive_pro='NO'";
        $result = $this->db->getOne($sql);
        if ($result) {
            if ($for_validation) {
                return true;
            } else {
                return 'sale';
            }
        } else {
            $sql = "SELECT 1
			FROM traveler_log t
			WHERE t.card_number_trl=$card_number
			AND t.status_trl='ACTIVE'
			";
            $result = $this->db->getOne($sql);
            if ($result) {
                if ($for_validation) {
                    return true;
                } else {
                    return 'travel_log';
                }
            } else {
                return false;
            }
        }
    }

    /*     * *********************************************
     * function getCardDaysAvailability
      @Name: getCardDaysAvailability
      @Description: Get card availability data.
      @Params:
     *       $this->serial_sal: object atribute for serial_sal
      @Returns:     array of results
     *        false when no results
     * ********************************************* */

    function getCardDaysAvailability()
    {
        $sql = "SELECT s.serial_sal,pbl.name_pbl,s.card_number_sal,DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal,DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,s.days_sal,IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal)as available_days
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
			JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro
			LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal
			WHERE s.serial_sal={$this->serial_sal}
			AND s.end_date_sal >= CURDATE()
			GROUP BY s.serial_sal";
        $result = $this->db->getRow($sql);
        if ($result)
            return $result;
        else
            return false;
    }

    /*
     * getCardsByCustomerReport
     * @param: $db - DB connection
     * @param: $serial_cus - Customer ID
     * @return: An array of all customers that bought any Planet Assist card.
     */

    public static function getCardsByCustomerReport($db, $serial_cus, $own_branch_cards_only = FALSE, $exclude_free = FALSE)
    {
        if ($own_branch_cards_only) {
            $restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
        }

        if ($exclude_free):
            $exclude_free_statement = " AND s.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED')";
        endif;

        $sql = "SELECT DISTINCT * FROM 
				(
					(SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal',
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal, 
						 s.serial_cnt, s.serial_inv, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con
				  FROM customer c
				  JOIN sales s ON s.serial_cus=c.serial_cus	
				  $restrict_own_cards
				  $exclude_free_statement
				  LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND trl.serial_cus = '" . $serial_cus . "'
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='" . $_SESSION['serial_lang'] . "'
				  WHERE c.serial_cus = '" . $serial_cus . "') 


					UNION

					(SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal',
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal,
						 s.serial_cnt, s.serial_inv, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con
				  FROM customer c
				  LEFT JOIN traveler_log trl ON trl.serial_cus=c.serial_cus AND trl.serial_cus = '" . $serial_cus . "'
				  LEFT JOIN sales s ON trl.serial_sal = s.serial_sal
				  $restrict_own_cards
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='" . $_SESSION['serial_lang'] . "'
				  WHERE c.serial_cus = '" . $serial_cus . "')

				) as tbl";
//die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function getCardsByCustomerReportWeb($db, $serial_cus, $own_branch_cards_only = FALSE, $exclude_free = FALSE)
    {
        if ($own_branch_cards_only) {
            $restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
        }

        if ($exclude_free):
            $exclude_free_statement = " AND s.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED','VOID')";
        endif;

        $sql = "SELECT DISTINCT * FROM 
				(
					(SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal',
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal, 
						 s.serial_cnt, s.serial_inv, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con
				  FROM customer c
				  JOIN sales s ON s.serial_cus=c.serial_cus	and s.type_sal = 'WEB' 
				  
				  $exclude_free_statement
				  LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND trl.serial_cus = '" . $serial_cus . "'
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang=2
				  WHERE c.serial_cus = '" . $serial_cus . "' order by s.serial_sal desc) 

				) as tbl";
//die('<pre>'.$sql.'</pre>');

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    private static function convertDateValidQuery($d)
    {
        $date = str_replace('/', '-', $d);
        return date('Y-m-d', strtotime($date));

    }

    public static function getDifferentDates($begin_date, $end_date, $db, $countryId)
    {
        $transfornBeginDate = Sales::convertDateValidQuery($begin_date);
        $transfornEndDate = Sales::convertDateValidQuery($end_date);
//        $transfornBeginDate = "2017-01-01";
//        $transfornEndDate = "2017-01-05";
        $sql = "select sales.card_number_sal,sales_log.description_slg,sales.emission_date_sal, CONCAT(customer.first_name_cus,' ',customer.last_name_cus) AS Cliente, customer.document_cus, sales.total_sal, sales_log.date_slg, invoice.total_inv
from sales_log 
INNER JOIN sales ON sales_log.serial_sal = sales.serial_sal 
INNER JOIN counter
        ON counter.serial_cnt = sales.serial_cnt
INNER JOIN dealer
        ON dealer.serial_dea = counter.serial_dea
INNER JOIN manager_by_country
        ON manager_by_country.serial_mbc = dealer.serial_mbc
INNER JOIN customer 
        ON customer.serial_cus = sales.serial_cus
INNER JOIN invoice 
        ON invoice.serial_inv = sales.serial_inv
where ( sales.emission_date_sal BETWEEN '$transfornBeginDate' AND '$transfornEndDate') and manager_by_country.serial_cou = $countryId ORDER BY sales.card_number_sal, sales_log.date_slg;";
        $result = $db->getAll($sql);
        if ($result) {
            $differentes = array();
            foreach ($result as $valor) {
                $obj = unserialize($valor['description_slg']);
                $oldSaleBeginDate = $obj['old_sale']['begin_date_sal'];
                $newSaleBeginDate = $obj['new_sale']['begin_date_sal'];
                $oldSaleEndDate = $obj['old_sale']['end_date_sal'];
                $newSaleEndDate = $obj['new_sale']['end_date_sal'];
                $arrTmp = array();
                if (($oldSaleBeginDate != $newSaleBeginDate) || ($oldSaleEndDate != $newSaleEndDate)) {
                    $arrTmp['card_number_sal'] = $valor['card_number_sal'];
                    $arrTmp['emission_date_sal'] = $valor['emission_date_sal'];
                    $arrTmp['old_begin_date_sal'] = $oldSaleBeginDate;
                    $arrTmp['new_begin_date_sal'] = $newSaleBeginDate;
                    $arrTmp['old_end_date_sal'] = $oldSaleEndDate;
                    $arrTmp['new_end_date_sal'] = $newSaleEndDate;
                    $arrTmp['customer'] = $valor['Cliente'];
                    $arrTmp['document_cus'] = $valor['document_cus'];
                    $arrTmp['total_sal'] = $valor['total_sal'];
                    $arrTmp['date_slg'] = $valor['date_slg'];
                    $arrTmp['total_inv'] = $valor['total_inv'];
                    array_push($differentes, $arrTmp);
                }
            }
        }
//        var_dump($differentes);
        //Debug::print_r($differentes);die();
        return $differentes;
    }
    
    public static function getVoidSales($begin_date, $end_date, $db, $countryId)
    {
//        $transfornBeginDate = Sales::convertDateValidQuery($begin_date);
//        $transfornEndDate = Sales::convertDateValidQuery($end_date);
//        $transfornBeginDate = "2017-01-01";
//        $transfornEndDate = "2017-01-05";
        $sql = "(SELECT DISTINCT IFNULL(sal2.card_number_sal, 'N/A') as 'numero_tarjeta', 
                    DATE_FORMAT(sal2.in_date_sal,'%d/%m/%Y') as 'fecha_emision', DATE_FORMAT(slg.date_slg,'%d/%m/%Y') as 'fecha_anulacion',

        				CONCAT(cus.first_name_cus,' ',cus.last_name_cus) as 'nombre_cliente',
                     	pbl.name_pbl as 'nombre_producto',
                     	DATE_FORMAT(sal2.begin_date_sal,'%d/%m/%Y') as 'Inicio_Vigencia', 
                     	DATE_FORMAT(sal2.end_date_sal,'%d/%m/%Y') as 'Fin_Vigencia',
                     	DATEDIFF(sal2.end_date_sal,sal2.begin_date_sal)+1 as 'numero_dias',
                     	sal2.total_sal as 'total_ventas',
                     	IF(sal2.free_sal = 'YES', 'FREE', sal2.status_sal) as 'ESTADO_VENTA',
                     	d2.name_dea as 'Comercializador',
                        d2.id_dea as 'id_dea', d2.code_dea as 'code_dea',
                        m.name_man AS 'name_manager',
                        CONCAT(u2.first_name_usr,' ',u2.last_name_usr) as 'responsable',
                        slg.description_slg as 'description'
						FROM user u2
						JOIN user_by_dealer ubd2 ON ubd2.serial_usr = u2.serial_usr
						JOIN dealer d2 ON d2.serial_dea=ubd2.serial_dea
						JOIN counter cnt2 ON cnt2.serial_dea = d2.serial_dea 
						JOIN sales sal2 ON sal2.serial_cnt = cnt2.serial_cnt 
							AND STR_TO_DATE(DATE_FORMAT(sal2.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
								BETWEEN ubd2.begin_date_ubd AND 
								IFNULL(ubd2.end_date_ubd, CURRENT_DATE())
                        JOIN customer cus on sal2.serial_cus = cus.serial_cus
                        JOIN product_by_dealer pbd ON pbd.serial_pbd = sal2.serial_pbd
				        JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				        JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang=2
						JOIN manager_by_country mbc2 ON mbc2.serial_mbc = d2.serial_mbc
                        JOIN manager m on mbc2.serial_man = m.serial_man
						LEFT JOIN sales_log slg ON slg.serial_sal = sal2.serial_sal
						AND slg.type_slg IN ('VOID_INVOICE', 'VOID_SALE')
						AND slg.status_slg = 'AUTHORIZED'
						WHERE 
						mbc2.serial_cou = 62
						AND sal2.status_sal = 'VOID'
						AND (STR_TO_DATE(DATE_FORMAT(slg.date_slg,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))
						order by sal2.serial_sal
                        ) ";
        $result = $db->getAll($sql);
        if ($result) {
            $resultado= array();
            foreach ($result as $valor) {
                $obj = unserialize($valor['description']);
                $array = array();
                $array['tarjeta']= $valor['numero_tarjeta'];
                $array['fecha_emision']= $valor['fecha_emision'];
                $array['fecha_anulacion']= $valor['fecha_anulacion'];
                $array['nombre_cliente']= $valor['nombre_cliente'];
                $array['nombre_producto']= $valor['nombre_producto'];
                $array['inicio_vigencia']= $valor['Inicio_Vigencia'];
                $array['fin_vigencia']= $valor['Fin_Vigencia'];
                $array['numero_dias']= $valor['numero_dias'];
                $array['total_ventas']= $valor['total_ventas'];
                $array['estado_venta']= $valor['ESTADO_VENTA'];
                $array['comercializador']= $valor['Comercializador'];
                $array['id_dea']= $valor['id_dea'];
                $array['code_dea']= $valor['code_dea'];
                $array['name_manager']= $valor['name_manager'];
                $array['responsable']= $valor['responsable'];
                $array['motivo'] = $obj['global_info']['request_obs'];
                array_push($resultado, $array);
            }
            //Debug::print_r($resultado);die();
        }
    }

    public static function getMasiveCardsByCardNumber($db, $cardNumber, $own_branch_cards_only = FALSE)
    {
        if ($own_branch_cards_only) {
            $restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
        }
        $sql = "SELECT s.serial_sal, s.card_number_sal, pbl.name_pbl, s.sal_serial_sal, s.free_sal, s.status_sal
			    FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				$restrict_own_cards
				WHERE s.card_number_sal LIKE '$cardNumber'";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function getNormalCardsByCardNumber($db, $cardNumber, $own_branch_cards_only = FALSE)
    {
        if ($own_branch_cards_only) {
            $restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
        }

        $sql = "SELECT	s.serial_sal, s.card_number_sal, pbl.name_pbl, s.free_sal, s.status_sal, 
						s.serial_cnt, MAX(trl.serial_trl) AS 'serial_trl', s.serial_con
			    FROM sales s
				LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND status_trl <> 'DELETED'
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd AND s.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED')
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				$restrict_own_cards
				WHERE s.card_number_sal LIKE '$cardNumber' OR trl.card_number_trl LIKE '$cardNumber'";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /*
     * getCardReport
     * @param: $db - DB connection
     * @param: $serial_cus - Customer ID
     * @return: An array of all customers that bought any Planet Assist card.
     */

    public static function getCardReport($db, $serial_cou, $date_type, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL, $dea_serial_dea = NULL, $serial_dea = NULL, $serial_pro = NULL, $type_sal = NULL, $status_sal = NULL, $operator = NULL, $total_sal = NULL, $stock_type = NULL, $order_by = NULL)
    {

        if ($order_by == NULL || $order_by == "card_number") {
            $order_by = "col1";
        } else {
            $order_by = "col2";
        }

        $sql = "SELECT  DISTINCT s.serial_sal,
						IFNULL(card_number_sal, 'N/A') as col1,
        				DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y %H:%i:%s') as col2,
        				CONCAT(first_name_cus,' ',last_name_cus) as col3,
                     	pbl.name_pbl as col4,
                     	DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as col5, 
                     	DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as col6,
                     	DATEDIFF(s.end_date_sal,s.begin_date_sal)+1 as col7,
                     	s.days_sal as 'days_sal',
                     	s.total_sal as col8,
                     	IF(s.free_sal = 'YES', 'FREE', s.status_sal) as col9,
                     	dea.name_dea as col10,
                     	CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as col11,
                     	s.stock_type_sal as col12,
						cus.document_cus,
						DATE_FORMAT(cus.birthdate_cus, '%d/%m/%Y') AS 'birthdate_cus',
						cou.name_cou AS 'name_country',
                        m.name_man AS 'name_manager',
                        dea.id_dea as 'id_dea', dea.code_dea as 'code_dea',
                        CONCAT(u2.first_name_usr,' ',u2.last_name_usr) as 'responsable'
				FROM sales s
				LEFT JOIN city cit_dest ON cit_dest.serial_cit = s.serial_cit
				LEFT JOIN country cou ON cit_dest.serial_cou = cou.serial_cou
				JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
                JOIN manager_by_country mbc on mbc.serial_mbc = dea.serial_mbc
                JOIN manager m on m.serial_man = mbc.serial_man
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
                                JOIN user u2 ON u2.serial_usr = ubd.serial_usr AND ubd.status_ubd = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				$refund_join
				WHERE cit.serial_cou='$serial_cou'";

        if ($date_type == "insystem") {
            $sql .= " AND (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
        } else if ($date_type == "coverage") {
            $sql .= " AND STR_TO_DATE(DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')
                      ";
        }
        //AND STR_TO_DATE(DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')) Se 
        if ($serial_cit != NULL) {
            $sql .= " AND cit.serial_cit = '$serial_cit'";
        }
        if ($serial_mbc != NULL) {
            $sql .= " AND dea.serial_mbc = '$serial_mbc'";
        }
        if ($serial_usr != NULL) {
            $sql .= " AND ubd.serial_usr = '$serial_usr' AND ubd.status_ubd = 'ACTIVE'";
        }
        if ($dea_serial_dea != NULL) {
            $sql .= " AND dea.dea_serial_dea = '$dea_serial_dea'";
        }
        if ($serial_dea != NULL) {
            $sql .= " AND dea.serial_dea = '$serial_dea'";
        }
        if ($serial_pro != NULL) {
            $sql .= " AND pxc.serial_pro = '$serial_pro'";
        }
        if ($type_sal != NULL) {
            $sql .= " AND s.type_sal = '$type_sal'";
        }
        if ($total_sal != NULL) {
            $sql .= " AND s.total_sal " . $operator . $total_sal;
        }
        if ($stock_type != NULL) {
            $sql .= " AND s.stock_type_sal = '$stock_type'";
        }
        if ($status_sal != NULL) {
            $sql .= " AND s.status_sal IN ($status_sal)";
        }
        $sql .= " ORDER BY $order_by";
        //Debug::print_r($sql);die();
        //echo $sql;
        //die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function getKitsReport($db, $manager = NULL, $responsible = NULL, $dealer = null, $branch = null, $counter = NULL, $begin_date = null, $end_date = null)
    {
        $sql = "SELECT IFNULL(s.card_number_sal, 'N/A') AS 'card_number', CONCAT(first_name_cus,' ',last_name_cus) as 'customer', 
                concat(u.first_name_usr, ' ', u.last_name_usr) as 'responsible', d.name_dea as 'dealer', b.name_dea as 'branch', 
                concat(uc.first_name_usr, ' ', uc.last_name_usr) as 'counter',
                pbl.name_pbl as 'product', IF(s.deliveredKit, 'Si','No') as 'deliveredKit'
            FROM sales s
            JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
            JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt";
        $counter ? $sql .= " AND cnt.serial_cnt = '{$counter}' " : " ";
        $sql .= " JOIN user uc ON uc.serial_usr = cnt.serial_usr
            JOIN dealer b ON b.serial_dea = cnt.serial_dea AND b.status_dea = 'ACTIVE'";
        $manager ? $sql .= " AND b.serial_mbc = '{$manager}' " : " ";
        $branch ? $sql .= " AND b.serial_dea = '{$branch}' " : " ";
        $sql .= " JOIN dealer d ON d.serial_dea = b.dea_serial_dea AND d.status_dea = 'ACTIVE'";
        $dealer ? $sql .= " AND d.serial_dea = '{$dealer}' " : " ";
        $sql .= "JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
            JOIN user u ON u.serial_usr = ubd.serial_usr ";
        $responsible ? $sql .= " AND u.serial_usr = '{$responsible}' " : " ";
        $sql .= " JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
            JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
            JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = '{$_SESSION['serial_lang']}'
            WHERE (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')) ORDER BY s.emission_date_sal";

        //echo $sql;
//          die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /*     * *********************************************
     * function getFreeSAlesByZone
     * gets information about free sales by zone and country
     * ********************************************* */

    function getFreeSalesByZone($serial_zon, $serial_cou, $serial_mbc_user, $dea_serial_dea_user, $serial_mbc = NULL, $serial_cit = NULL, $serial_dea = NULL, $serial_bra = NULL, $date_from = NULL, $date_to = NULL, $status = NULL)
    {
        $managerSQL = '';
        $citySQL = '';
        $dealerSQL = '';
        $branchSQL = '';
        $datesSQL = '';
        $statusSQL = '';

        if ($serial_mbc) {
            $managerSQL = "AND mbc.serial_mbc =" . $serial_mbc;
        }
        if ($serial_cit) {
            $citySQL = "AND cit.serial_cit = '" . $serial_cit . "'";
        }
        if ($serial_dea) {
            $dealerSQL = " AND dea.serial_dea = '" . $serial_dea . "'";
        }
        if ($serial_bra) {
            $branchSQL = "AND br.serial_dea = '" . $serial_bra . "'";
        }
        if ($date_from && $date_to) {
            $datesSQL = " AND DATE_FORMAT(sal.in_date_sal,'%Y/%m/%d') BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND STR_TO_DATE('$date_to', '%d/%m/%Y')";
        }
        if ($status) {
            $statusSQL = "AND sal.status_sal = '" . $status . "'";
        }
        $sql = "SELECT sal.serial_sal, br.serial_dea AS serial_bra, zon.name_zon, cou.name_cou, cit.name_cit, man.name_man, dea.name_dea, br.name_dea AS name_bra,
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS name_usr, DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y') AS date_sal,
						IFNULL(sal.card_number_sal,'N/A') AS card_number_sal, sal.days_sal, sal.fee_sal,
						sal.total_cost_sal, IFNULL(co.name_cou,'N/A') AS destination, pbl.name_pbl
				 FROM sales sal
					   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea $branchSQL
					   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealerSQL AND dea.status_dea = 'ACTIVE'
					   JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea 
					   JOIN user usr ON usr.serial_usr = ubd.serial_usr AND ubd.status_ubd='ACTIVE'
					   JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc  $managerSQL
					   JOIN manager AS man ON man.serial_man = mbc.serial_man
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit $citySQL
					   LEFT JOIN city ci ON ci.serial_cit = sal.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=" . $serial_cou . "
					   LEFT JOIN country co ON co.serial_cou = ci.serial_cou
					   JOIN zone zon ON zon.serial_zon = cou.serial_zon AND zon.serial_zon=" . $serial_zon . "
					   JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
					   JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
					   JOIN product pro ON pxc.serial_pro=pro.serial_pro
					   JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				   WHERE sal.free_sal = 'YES' $datesSQL $statusSQL
				   ";
        if ($serial_mbc_user != '1') {
            $sql .= " AND mbc.serial_mbc=" . $serial_mbc_user;
        }
        if ($dea_serial_dea_user) {
            $sql .= " AND dea.serial_dea=" . $dea_serial_dea_user;
        }
        $sql .= " ORDER BY name_bra, serial_bra";
        //die('<pre>'.$sql.'</pre>');
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $arr;
    }

    /*
     * @name: expireCards
     * @param: $db - DB connection
     * @returns: TRUE - if all the active paied cards where updated to EXPIRED after their lifetime.
     *           FALSE- otherwise
     */

    public static function expireCards($db)
    {
        $sql = "UPDATE sales SET
				status_sal='EXPIRED'
			  WHERE DATE_FORMAT(end_date_sal,'%Y-%m-%d') < CURDATE()
			  AND (status_sal = 'ACTIVE'
			  OR status_sal = 'REGISTERED')
			  AND serial_inv IS NOT NULL";

        $sql_free = "UPDATE sales SET
						status_sal='EXPIRED'
					WHERE STR_TO_DATE(DATE_FORMAT(end_date_sal,'%Y-%m-%d'), '%Y-%m-%d') < CURDATE()
					AND status_sal = 'ACTIVE'
					AND free_sal = 'YES'";

        $sql_reserved = "UPDATE sales SET
							status_sal='VOID'
						WHERE DATE_ADD(emission_date_sal, INTERVAL 3 DAY) <= CURRENT_TIMESTAMP() 
						AND status_sal = 'RESERVED'";

        $result = $db->Execute($sql);
        $result_free = $db->Execute($sql_free);
        $result_reserved = $db->Execute($sql_reserved);

        if ($result && $result_free && $result_reserved) {
            return true;
        } else {
            return false;
        }
    }

    public static function alertValiditySales($db){

        $sql = "SELECT distinct usr.serial_usr 
        from invoice inv
        join sales sal on inv.serial_inv = sal.serial_inv
        join customer cus on sal.serial_cus = cus.serial_cus
        join product_by_dealer pbd on sal.serial_pbd = pbd.serial_pbd
        join dealer dea on pbd.serial_dea = dea.serial_dea
        join user_by_dealer ubd on dea.serial_dea = ubd.serial_dea
        join user usr on ubd.serial_usr = usr.serial_usr and ubd.status_ubd = 'ACTIVE'
        -- where inv.due_date_inv between '2022-10-01 00:00:00' and '2022-10-03 00:00:00' and inv.status_inv NOT IN ('PAID','VOID','UNCOLLECTABLE');
        where  inv.status_inv NOT IN ('PAID','VOID','UNCOLLECTABLE') and TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP(),sal.begin_date_sal ) between 0 and 3  and  CURRENT_TIMESTAMP() <= sal.begin_date_sal
        AND sal.status_sal not in ('VOID')";
        		$result = $db->getAll($sql);
                if($result){
                    return $result;
                }else{
                    return false;
                }
    }

    public static function getSalesStandby($db, $serial_usr) {
        $sql = "SELECT usr.username_usr, usr.serial_usr ,concat(usr.first_name_usr, ' ', last_name_usr) AS 'Responsable', usr.email_usr, dea.name_dea, sal.card_number_sal ,concat(cus.first_name_cus, ' ', cus.last_name_cus) as 'Cliente', sal.emission_date_sal , sal.begin_date_sal ,  inv.number_inv , inv.date_inv ,
        -- inv.due_date_inv ,DATE_SUB(inv.due_date_inv, INTERVAL 3 DAY) as 'restado tres dias',
         TIMESTAMPDIFF(DAY, sal.begin_date_sal, CURRENT_TIMESTAMP()) AS 'dias_transcurridos',
         inv.status_inv, sal.status_sal,
         inv.total_inv
        from invoice inv
        join sales sal on inv.serial_inv = sal.serial_inv
        join customer cus on sal.serial_cus = cus.serial_cus
        join product_by_dealer pbd on sal.serial_pbd = pbd.serial_pbd
        join dealer dea on pbd.serial_dea = dea.serial_dea
        join user_by_dealer ubd on dea.serial_dea = ubd.serial_dea
        join user usr on ubd.serial_usr = usr.serial_usr and ubd.status_ubd = 'ACTIVE'
        -- where inv.due_date_inv between '2022-10-01 00:00:00' and '2022-10-03 00:00:00' and inv.status_inv NOT IN ('PAID','VOID','UNCOLLECTABLE');
        where  inv.status_inv NOT IN ('PAID','VOID','UNCOLLECTABLE') and TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP(),sal.begin_date_sal ) between 0 and 3  and  CURRENT_TIMESTAMP() <= sal.begin_date_sal and usr.serial_usr=$serial_usr AND sal.status_sal not in ('VOID')";

        $result = $db->getAll($sql);
        

        if($result){
        return $result;
        }else{
            return false;
        }
    
}

    /*     * *********************************************
      funcion getSalesInRange
      @Name: getSalesInRange
      @Description: get all sales in a stock range
      @Params:
     *    $db: db object
     *    $from: range start
     *    $to: range end
      @Returns: array of card numbers or false
     * ********************************************* */

    public static function getSalesInRange($db, $from, $to)
    {
        if ($db && $from && $to) {
            $sql = "SELECT s.card_number_sal
				  FROM sales s
				  WHERE s.card_number_sal BETWEEN $from AND $to";
            //echo $sql;
            $result = $db->getCol($sql);
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * @name: getSalesStock
     * @param: $db - DB connection
     * @returns: An array of all the stock types values in DB
     */

    public static function getSalesStock($db)
    {
        $sql = "SHOW COLUMNS FROM sales LIKE 'stock_type_sal'";
        $result = $db->Execute($sql);

        $type = $result->fields[1];
        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    /*
     * @name: getDealerSalesForChartReport
     * @param: $db - DB connection
     * @param: $serial_dea - Dealer ID
     * @param: $begin_date - The begin of the analisys time as an array in the d/m/Y format
     * @param: $end_date - The end of the analisys time as an array in the d/m/Y format
     */

    public static function getDealerSalesForChartReport($db, $serial_dea, $begin_date, $end_date)
    {
        $sql = "SELECT s.serial_sal, s.total_sal, DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
					 (i.discount_prcg_inv + i.other_dscnt_inv) AS 'discounts', i.applied_taxes_inv, pbl.name_pbl,
					 IF(r.serial_ref IS NULL, 'NO', 'YES') AS 'has_refunds', p.serial_pro,
					 IF(STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y')
					 BETWEEN STR_TO_DATE('" . $begin_date['0'] . "/" . $begin_date['1'] . "/" . $begin_date['2'] . "', '%d/%m/%Y')
					 AND STR_TO_DATE('" . $end_date['0'] . "/" . $end_date['1'] . "/" . $end_date['2'] . "', '%d/%m/%Y'), 'CURRENT', 'PREVIOUS') AS 'type',
					 IFNULL(SUM(spf.estimated_amount_spf),'0') AS 'reserved',
					 IFNULL(SUM(prq.amount_authorized_prq),'0') AS 'total_paid'
			  FROM sales s
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer b ON b.serial_dea=cnt.serial_dea AND b.serial_dea='$serial_dea'
			  JOIN invoice i ON i.serial_inv=s.serial_inv
			  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			  JOIN product p ON p.serial_pro=pxc.serial_pro
			  JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro
			  LEFT JOIN refund r ON r.serial_sal=s.serial_sal AND r.status_ref='APROVED'
			  LEFT JOIN file f ON f.serial_sal=s.serial_sal
			  LEFT JOIN payment_request prq ON prq.serial_fle=f.serial_fle AND prq.status_prq='APROVED' AND prq.dispatched_prq='YES'
			  LEFT JOIN service_provider_by_file spf ON spf.serial_fle=f.serial_fle
			  WHERE STR_TO_DATE(DATE_FORMAT(s.in_date_sal , '%d/%m/%Y'), '%d/%m/%Y')
					BETWEEN STR_TO_DATE('" . $begin_date['0'] . "/" . $begin_date['1'] . "/" . $begin_date['2'] . "', '%d/%m/%Y')
					AND STR_TO_DATE('" . $end_date['0'] . "/" . $end_date['1'] . "/" . $end_date['2'] . "', '%d/%m/%Y')
			  OR STR_TO_DATE(DATE_FORMAT(s.in_date_sal , '%d/%m/%Y'), '%d/%m/%Y')
					BETWEEN STR_TO_DATE('" . $begin_date['0'] . "/" . $begin_date['1'] . "/" . ($begin_date['2'] - 1) . "', '%d/%m/%Y')
					AND STR_TO_DATE('" . $end_date['0'] . "/" . $end_date['1'] . "/" . ($end_date['2'] - 1) . "', '%d/%m/%Y')
			GROUP BY s.serial_sal
			ORDER BY type, emission_date_sal";
        //die($sql);
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*     * *********************************************
     * function getSalesAnalysis
     * gets information for the sale's analysis
     * ********************************************* */

    function getSalesAnalysis($serial_zon, $serial_cou, $date_from, $date_to, $user_serial_mbc, $user_serial_dea, $serial_mbc = NULL, $serial_cit = NULL, $serial_usr = NULL, $serial_dea = NULL, $serial_bra = NULL, $flag = null)
    {

        if ($serial_mbc)
            $managerSQL = "AND mbc.serial_mbc =" . $serial_mbc;
        if ($serial_cit)
            $citySQL = "AND cit.serial_cit = '" . $serial_cit . "'";
        if ($serial_dea)
            $dealerSQL = " AND dea.serial_dea = '" . $serial_dea . "'";
        if ($serial_bra)
            $branchSQL = "AND br.serial_dea = '" . $serial_bra . "'";
        if ($serial_usr)
            $userSQL = "AND ubd.serial_usr = '" . $serial_usr . "'";

        $sql = "SELECT DISTINCT(sal.serial_sal), zon.name_zon, cou.name_cou, man.name_man, cit.name_cit,
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS 'name_usr', dea.name_dea,
						br.name_dea AS name_bra, br.percentage_dea, br.type_percentage_dea, sal.total_sal,
						dea.serial_dea, br.serial_dea AS serial_bra, cit.serial_cit, inv.applied_taxes_inv,
						(inv.discount_prcg_inv + inv.other_dscnt_inv) AS 'discounts'
					FROM sales sal
					     JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					     JOIN dealer br ON cnt.serial_dea=br.serial_dea $branchSQL
					     JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealerSQL
					     JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc $managerSQL
					     JOIN manager man ON man.serial_man=mbc.serial_man
                         JOIN sector sec ON br.serial_sec=sec.serial_sec
      			       	 JOIN city cit ON sec.serial_cit=cit.serial_cit $citySQL
      			       	 JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND ubd.status_ubd='ACTIVE' $userSQL
                         JOIN user usr ON usr.serial_usr=ubd.serial_usr
				         JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=" . $serial_cou . "
					     JOIN zone zon ON zon.serial_zon=cou.serial_zon AND zon.serial_zon=" . $serial_zon . "
					     JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
					     JOIN payments pay ON pay.serial_pay=inv.serial_pay
                    WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')
					        AND sal.serial_inv IS NOT NULL AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'
							AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND STR_TO_DATE('$date_to', '%d/%m/%Y')";

        if ($user_serial_mbc != '1')
            $sql .= " AND mbc.serial_mbc=" . $user_serial_mbc;
        if ($user_serial_dea)
            $sql .= " AND dea.serial_dea=" . $user_serial_dea;

        $sql .= " ORDER BY man.serial_man, dea.serial_dea, br.serial_dea, cit.serial_cit";

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $arr;
    }

    /**
     * @Name: getMasiveSalesReport
     * @Description: Get the sales that corresponds to masive products
     * @Params:
     * @Returns: array with masive sales
     * */
    function getMasiveSalesReport($code_lang, $serial_cus, $sal_serial_sal = NULL)
    {
        $sql = "SELECT pbl.name_pbl, sal.serial_sal, sal.card_number_sal, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
					   DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					   DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal, sal.status_sal
				FROM sales sal
				JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product pro ON pxc.serial_pro=pro.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro
				JOIN language lang ON pbl.serial_lang = lang.serial_lang AND lang.code_lang = '$code_lang'
				WHERE (pro.third_party_register_pro = 'YES' OR pro.masive_pro = 'YES')
				AND (sal.status_sal IN ('ACTIVE','REGISTERED'))
				AND (sal.end_date_sal > CURDATE())
				AND sal.serial_cus = '$serial_cus'";
        if ($sal_serial_sal != NULL) {
            $sql .= " AND sal.sal_serial_sal = '$sal_serial_sal'";
        } else {
            $sql .= " AND sal.sal_serial_sal IS NULL";
        }
        //die($sql);

        $result = $this->db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name: getMasiveSalesCustomerReport
     * @Description: Get the customers that corresponds to a masive sale
     * @Params:
     * @Returns: array with masive sales
     * */
    public static function getMasiveSalesCustomerReport($db, $serial_sal)
    {
        $sql = "SELECT CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus, trl.card_number_trl, trl.serial_trl, trl.serial_sal, cus.document_cus, IFNULL(cus.email_cus,'-') as email_cus,
				IFNULL(cus.address_cus,'-') as address_cus, IFNULL(DATE_FORMAT(cus.birthdate_cus,'%d/%m/%Y'),'-') as birthdate_cus,
				IFNULL(cus.phone1_cus,'-') as phone1_cus, IFNULL(cus.cellphone_cus,'-') as cellphone_cus,
				IFNULL(cou.name_cou,'-') as name_cou, IFNULL(cit.name_cit,'-') as name_cit
				FROM customer cus
				JOIN traveler_log trl ON trl.serial_cus = cus.serial_cus
				LEFT JOIN city cit ON cit.serial_cit = cus.serial_cit
				LEFT JOIN country cou ON cou.serial_cou = cit.serial_cou
				WHERE trl.serial_sal = $serial_sal";
        //die($sql);

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name: getPhoneSalesReport
     * @Description: Get the sales that corresponds to a phone counter
     * @Params:
     * @Returns: array with masive sales
     * */
    public static function getPhoneSalesReport($db, $serial_usr, $beginDate, $endDate)
    {
        $sql = "SELECT pbl.name_pbl, IFNULL(sal.card_number_sal,trl.card_number_trl) as card_number, IFNULL(inv.number_inv,'-') as number_inv, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
                               sal.total_sal, IFNULL(inv.total_inv,'-') as total_inv
                    FROM sales sal
                    JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.phone_sales_dea = 'YES'
                    JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                    JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                    JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = " . $_SESSION['serial_lang'] . "
                    LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
                    LEFT JOIN invoice inv ON inv.serial_inv = sal.serial_inv
                    WHERE cnt.serial_usr = $serial_usr
                    AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$beginDate', '%d/%m/%Y') AND STR_TO_DATE('$endDate', '%d/%m/%Y')";
        //die(Debug::print_r($sql));

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name: getDealerSalesReport
     * @Description: Get the sales that corresponds to a phone counter
     * @Params:
     * @Returns: array with masive sales
     * */
    public static function getDealerSalesReport($db, $params, $serial_dea, $serial_cus = NULL, $beginDate = NULL, $endDate = NULL, $status_sal = NULL, $serial_pbd = NULL, $date_type = NULL, $serial_cnt = NULL)
    {
        $sql = "SELECT IFNULL(sal.card_number_sal,trl.card_number_trl) as card_number, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
					   CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus, pbl.name_pbl, DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					   DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal, sal.days_sal, sal.status_sal, CONCAT(usr.first_name_usr, ' ',
					   usr.last_name_usr) as name_usr, sal.total_sal
				FROM sales sal
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
				JOIN customer cus ON cus.serial_cus = sal.serial_cus
				JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				WHERE cnt.serial_dea = '$serial_dea'";
        if ($params == "CUSTOMER") {
            $sql .= " AND sal.serial_cus = '$serial_cus'";
        } elseif ($params == "PARAMETER") {
            if ($date_type == "emission") {
                $sql .= " AND (STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $beginDate . "','%d/%m/%Y') AND STR_TO_DATE('" . $endDate . "','%d/%m/%Y'))";
            } else if ($date_type == "coverage") {
                $sql .= " AND (STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $beginDate . "','%d/%m/%Y') AND STR_TO_DATE('" . $endDate . "','%d/%m/%Y'))";
            }

            if ($status_sal != NULL) {
                $sql .= " AND sal.status_sal = '$status_sal'";
            }
            if ($serial_pbd != NULL) {
                $sql .= " AND sal.serial_pbd = '$serial_pbd'";
            }
        }

        if ($serial_cnt) {
            $sql .= " AND sal.serial_cnt = '$serial_cnt'";
        }

        $sql .= " ORDER BY sal.emission_date_sal";
        //die(Debug::print_r($sql));
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
        return $arr;
    }

    /**
     * @Name: getSalesWithStandByChanges
     * @Description: Get the sales that have registers in sales-log table with STAND-BY type
     * @Params: $db
     * @Returns: array with sales
     * */
    public static function getSalesWithStandByChanges($db)
    {
        $parameter = new Parameter($db, 9);
        $parameter->getData();
        $limit = $parameter->getValue_par();

        $sql = "SELECT sal.serial_sal, IF(DATEDIFF(CURDATE(),MAX(slg.date_slg)) < ($limit*30),'NO','YES') as expire
                FROM sales sal
                JOIN sales_log slg ON slg.serial_sal = sal.serial_sal
                WHERE slg.type_slg = 'STAND_BY'
                GROUP BY sal.serial_sal";
        //die($sql);
        $result = $db->getAll($sql);

        if ($result)
            return $result;
        else
            return NULL;
    }

    /**
     * @Name: hasBeenReactivated
     * @Description: Verifies if a card has been reactivated at some point.
     * @Params: $db
     * @Returns: TRUE if has been reactivated
     * FALSE otherwise
     * */
    public static function hasBeenReactivated($db, $serial_sal)
    {
        $sql = "SELECT sal.serial_sal
                FROM sales sal
                JOIN sales_log slg ON slg.serial_sal = sal.serial_sal
                WHERE slg.type_slg = 'REACTIVATED'
				AND slg.status_slg IN ('PENDING', 'AUTHORIZED')
				AND sal.serial_sal = '$serial_sal'";
        //die($sql);
        $result = $db->getOne($sql);

        if ($result)
            return TRUE;
        else
            return FALSE;
    }

    /**
     * @Name: hasBeenReactivated
     * @Description: Verifies if a card has been reactivated at some point.
     * @Params: $db
     * @Returns: TRUE if has been reactivated
     * FALSE otherwise
     * */
    public static function getPendingReactivatedCards($db)
    {
        $sql = "SELECT slg.serial_slg, sal.card_number_sal, CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus
                FROM sales sal
                JOIN sales_log slg ON slg.serial_sal = sal.serial_sal
				JOIN customer cus ON cus.serial_cus = sal.serial_cus
                WHERE slg.type_slg = 'REACTIVATED'
				AND slg.status_slg IN ('PENDING')";
        //die($sql);
        $result = $db->getAll($sql);

        if ($result)
            return $result;
        else
            return FALSE;
    }

    /*
     * @name: isPlanetAssistSale
     * @description: Verifies if a sale is from any country other than EC or PE
     * @param: serial_sal - Sale's ID
     * @returns: TRUE if not EC/PU
     *           FALSE otherwise.
     */

    public static function isPlanetAssistSale($db, $serial_sal)
    {
        global $globalBlueCardCountryCodes;
        $sql = "SELECT s.serial_sal
			  FROM sales s
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer d ON d.serial_dea=cnt.serial_dea
			  JOIN sector sec ON sec.serial_sec=d.serial_sec
			  JOIN city c ON c.serial_cit=sec.serial_cit
			  JOIN country cou ON cou.serial_cou = c.serial_cou AND cou.serial_cou NOT IN($globalBlueCardCountryCodes)
			  WHERE s.serial_sal=" . $serial_sal;
        $result = $db->getOne($sql);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getCountryofSale
     * @param: $db - DB connection
     * @param: serial_sal - Sale's ID
     * @return: $serial_cou - The Country's ID where the sale took place.
     */

    public static function getCountryofSale($db, $serial_sal)
    {
        $sql = "SELECT c.serial_cou
			  FROM sales s
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer d ON d.serial_dea=cnt.serial_dea
			  JOIN sector sec ON sec.serial_sec=d.serial_sec
			  JOIN city c ON c.serial_cit=sec.serial_cit			  
			  WHERE s.serial_sal=" . $serial_sal;

        $result = $db->getOne($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name: getInternationalFeeStatus
     * @Description: Returns an array of all the sale international fee satatus in DB.
     * @Params: $db: DB connection
     * @Returns: An array of free sales.
     */
    public static function getInternationalFeeStatus($db)
    {
        $sql = "SHOW COLUMNS FROM sales LIKE 'international_fee_status'";
        $result = $db->Execute($sql);

        $type = $result->fields[1];
        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    /**
     * @Name: getInternationalFeeUsed
     * @Description: Returns an array of all the sale international fee used in DB.
     * @Params: $db: DB connection
     * @Returns: An array of free sales.
     */
    public static function getInternationalFeeUsed($db)
    {
        $sql = "SHOW COLUMNS FROM sales LIKE 'international_fee_used'";
        $result = $db->Execute($sql);

        $type = $result->fields[1];
        $type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
        return $type;
    }

    /**
     * @Name: getDaylyOnlySaleManagerSales
     * @Description: Returns an array of all the sales from a only sales managers
     * @Params: $db: DB connection
     * @Returns: An array of sales.
     */
    public static function getDaylyOnlySaleManagerSales($db)
    {
        $sql = "SELECT s.serial_sal, dea.serial_dea, man.serial_man, s.serial_pbd, dea.serial_cdd
				FROM sales s
				JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
				JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man AND man.sales_only_man = 'YES'
				WHERE s.serial_inv IS NULL
				AND s.free_sal = 'NO'
				AND s.status_sal = 'REGISTERED'";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name onCoverageTime
     * @Description Checks if a card available for travels register
     * @Params
     *   $this->card_number_sal: object atribute for card_number_sal
     * @Returns true: when is executive
     *    false: when is not executive
     * */
    function onCoverageTime()
    {
        $sql = "SELECT 1
			FROM sales s
			WHERE s.serial_sal={$this->serial_sal}
			AND begin_date_sal <= NOW()";
        $result = $this->db->getOne($sql);
        if ($result)
            return true;
        else
            return false;
    }

    /**
     * @name hasApplicationsPending
     * @param $db Database Connection
     * @param $serial_sal Sales ID
     * @return TRUE If there's any applicantios to modify the sales in any way.
     * @return FALSE If no application was found
     */
    public static function hasApplicationsPending($db, $serial_sal)
    {
        $sql = "SELECT s.serial_sal
			FROM sales s
			JOIN sales_log slg ON slg.serial_sal=s.serial_sal
			WHERE slg.serial_sal=" . $serial_sal . "
			AND slg.status_slg='PENDING'";

        $result = $db->getOne($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function payInternationaFee($serial_man, $serial_btm)
    {
        /* //Gets date
          $billingToManager = new BillingToManager($this->db, $serial_btm);
          $date = $billingToManager->getLastBillToManager($serial_man, $serial_btm);

          if ($billingToManager->getData()) {
          $date_btm = $billingToManager->getDate_btm();

          if ($date) {
          $extra_conditions = " AND (sal.emission_date_sal > STR_TO_DATE('$date','%d/%m/%Y %H:%i:%s')
          AND sal.emission_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY))";
          } else {
          $extra_conditions = " AND sal.emission_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)";
          }
          } else {
          if ($date) {
          $extra_conditions = " AND sal.emission_date_sal > STR_TO_DATE('$date','%d/%m/%Y %H:%i:%s')";
          }
          } */

        //Gets date
        $billingToManager = new BillingToManager($this->db);
        $date = $billingToManager->getLastBillToManager($serial_man, $serial_btm);

        if ($serial_btm) {
            $billingToManager = new BillingToManager($this->db, $serial_btm);
            $billingToManager->getData();

            $date_btm = $billingToManager->getDate_btm();

            if ($date) {
                $dates_restriction = " AND	(
												(	
													sal.in_date_sal > STR_TO_DATE('$date','%d/%m/%Y')
													AND sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
												) OR (
													 sl.date_slg >  STR_TO_DATE('$date','%d/%m/%Y')
													AND sl.date_slg < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
												)
											)";
            } else {
                $dates_restriction = " AND (
												sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
											OR 
												sl.date_slg < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
											)";
            }
        } else {
            if ($date) {
                $dates_restriction = " AND (
												sal.in_date_sal > STR_TO_DATE('$date','%d/%m/%Y')
											 OR
												sl.date_slg > STR_TO_DATE('$date','%d/%m/%Y')
											)";
            }
        }

        if ($date_to) {
            $dates_restriction .= " AND (
											sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_to','%d/%m/%Y'), INTERVAL 1 DAY)
										 OR
											sl.date_slg < DATE_ADD(STR_TO_DATE('$date_to','%d/%m/%Y'), INTERVAL 1 DAY)
										)";
        }

        $sql = "SELECT DISTINCT sal.serial_sal
					FROM manager man
					JOIN manager_by_country mbc ON mbc.serial_man = man.serial_man
					JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc
					JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
					JOIN user usr ON usr.serial_usr = cnt.serial_usr
					JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
					LEFT JOIN sales_log sl ON sl.serial_sal=sal.serial_sal AND type_slg = 'REFUNDED' AND status_slg = 'AUTHORIZED'
					JOIN customer cus ON cus.serial_cus = sal.serial_cus
					JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = " . $_SESSION['serial_lang'] . "
					WHERE man.serial_man = $serial_man
					AND sal.international_fee_used = 'NO'
					AND (sal.international_fee_status = 'TO_COLLECT' OR sal.international_fee_status = 'TO_REFUND')
					$dates_restriction";

        $result = $this->db->getAll($sql);
        $connector = '';

        if (count($result) > 0) {
            foreach ($result as $r) {
                $sales_ids .= $connector . $r['0'];
                $connector = ',';
            }
        } else {
            ErrorLog::log($this->db, 'SALES payInternationaFee - getSales ', $sql);
            return FALSE;
        }

        $sql = "UPDATE sales SET
				international_fee_used='YES',
				international_fee_status='PAID'
				WHERE serial_sal IN ($sales_ids)";

        $result = $this->db->Execute($sql);

        if ($result) {
            return TRUE;
        } else {
            ErrorLog::log($this->db, 'SALES payInternationaFee - updateSales', $sql);
            return FALSE;
        }
    }

    public static function isMassiveSale($db, $serial_sal)
    {
        $sql = " SELECT s.serial_sal
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND p.masive_pro = 'YES'
			  	WHERE s.serial_sal = $serial_sal";

        $result = $db->getOne($sql);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function flightRegisterGeneratesCardNumber($db, $serial_sal)
    {
        $sql = "SELECT s.serial_sal
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND p.generate_number_pro = 'NO'
			  	WHERE s.serial_sal = $serial_sal";

        $result = $db->getOne($sql);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function getAssistanceSaleInfoByCardNumber($db, $card_number, $masive)
    {

        $sql = "SELECT sal.serial_sal, sal.status_sal, sal.free_sal, sal.serial_inv
				FROM sales sal
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal";
        if ($masive == 0) {
            $sql .= " WHERE sal.card_number_sal LIKE '$card_number'
				OR trl.card_number_trl LIKE '$card_number'";
        } else {
            $sql .= " WHERE trl.card_number_trl LIKE '$card_number'";
        }
        //die($sql);
        $result = $db->getAll($sql);

        if ($result[0]) {
            return $result[0];
        } else {
            return FALSE;
        }
    }

    public static function getSalesInfoForCustomerPromo($db, $serial_sal)
    {
        $sql = "SELECT s.total_sal, pbd.serial_pxc, s.free_sal, s.type_sal
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				WHERE s.serial_sal = $serial_sal";

        return $db->getRow($sql);
    }

    public static function updateSaleTotalForCupon($db, $total_sal, $serial_sal)
    {
        $sql = "UPDATE sales SET
				total_sal = $total_sal
			  WHERE serial_sal = $serial_sal";

        $result = $db->Execute($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function getSoldProductInformation($db, $serial_sal)
    {
        $sql = "SELECT s.serial_pbd, pxc.serial_pxc, pxc.serial_pro
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				WHERE serial_sal = $serial_sal";

        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getGrossSalesForReport($db, $serial_cou, $date_field, $date_of_analisys, $order_field, $serial_cit = NULL, $serial_mbc = NULL, $serial_ubd = NULL, $serial_dea = NULL, $serial_bra = NULL, $stock_type = NULL, $sale_type = NULL, $product_list = NULL, $status_sal = NULL, $amount_clause = NULL, $effective_sales = false)
    {

        $date_of_analisys_arr = explode("/", $date_of_analisys);
        $lastDay = strftime("%d", mktime(0, 0, 0, $date_of_analisys_arr[0] + 1, 0, $date_of_analisys_arr[1]));

        ($date_field == 'insystem') ? $date_field = 's.in_date_sal' : $date_field = 's.begin_date_sal';
        ($order_field == 'card_number') ? $order_field = '2' : $order_field = '3';
        ($serial_cit) ? $city_clause = " AND cit.serial_cit = $serial_cit" : NULL;
        ($serial_mbc) ? $mbc_clause = " AND br.serial_mbc = $serial_mbc" : NULL;
        ($serial_ubd) ? $ubd_clause = " AND ubd.serial_usr = $serial_ubd" : NULL;
        ($serial_dea) ? $dea_clause = " AND br.dea_serial_dea = $serial_dea" : NULL;
        ($serial_bra) ? $bra_clause = " AND br.serial_dea = $serial_bra" : NULL;
        ($stock_type) ? $stock_clause = " AND s.stock_type_sal = '$stock_type'" : NULL;
        ($sale_type) ? $sale_type_clause = " AND s.type_sal  IN ($sale_type)" : NULL;
        ($product_list) ? $product_clause = " AND pxc.serial_pro IN ($product_list)" : NULL;
        ($amount_clause) ? $amount_clause = " AND s.total_sal $amount_clause" : NULL;
        $effective_clause = "WHERE $date_field BETWEEN STR_TO_DATE( '01/" . $date_of_analisys . "' ,'%d/%m/%Y') AND STR_TO_DATE('" . $lastDay . "/" . $date_of_analisys . "','%d/%m/%Y')"; //" WHERE STR_TO_DATE(DATE_FORMAT($date_field, '%m/%Y'), '%m/%Y') = STR_TO_DATE('$date_of_analisys', '%m/%Y')";

        $sql = "(
					SELECT  'PROCESSED' AS 'register_type',
							IF(s.card_number_sal IS NULL OR s.card_number_sal = 0, 'N/A', s.card_number_sal) AS 'card_number_sal',
							DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
							DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
							CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
							DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
							DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
							s.days_sal, s.total_sal, br.name_dea,
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_cnt', s.stock_type_sal,
							s.status_sal, 'YES' AS 'on_date', pbl.name_pbl, s.serial_sal
					FROM sales s
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd AND s.status_sal <> 'DENIED'
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc $product_clause
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN user u ON u.serial_usr = cnt.serial_usr
					JOIN dealer br ON br.serial_dea = cnt.serial_dea $mbc_clause $dea_clause $bra_clause  AND br.status_dea= 'ACTIVE'
					JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $ubd_clause 
						AND s.in_date_sal 
							BETWEEN DATE_FORMAT(ubd.begin_date_ubd, '%d/%m/%Y') AND 
							DATE_SUB(IFNULL(ubd.end_date_ubd, DATE_ADD(CURRENT_DATE(), INTERVAL 2 DAY)), INTERVAL 1 DAY)
					JOIN sector sec ON sec.serial_sec = br.serial_sec
					JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou $city_clause
					$effective_clause
					$stock_clause
					$sale_type_clause				
					$amount_clause
				) UNION (
					SELECT  IF(slg.type_slg = 'REFUNDED', 'REFUND', IF(s.status_sal = 'REFUNDED', 'REFUND', 'VOID')) AS 'register_type',
							IF(s.card_number_sal IS NULL OR s.card_number_sal = 0, 'N/A', s.card_number_sal) AS 'card_number_sal',
							DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
							DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
							CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
							DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
							DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
							s.days_sal, s.total_sal, br.name_dea,
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_cnt', s.stock_type_sal,
							s.status_sal,
							IF( STR_TO_DATE(DATE_FORMAT($date_field, '%m/%Y'), '%m/%Y') = STR_TO_DATE('$date_of_analisys', '%m/%Y'),
								'YES', 'NO') AS 'on_date', pbl.name_pbl, s.serial_sal
					FROM sales s
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc $product_clause
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN user u ON u.serial_usr = cnt.serial_usr
					JOIN dealer br ON br.serial_dea = cnt.serial_dea $mbc_clause $dea_clause $bra_clause AND br.status_dea= 'ACTIVE'
					JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $ubd_clause
						AND s.in_date_sal 
							BETWEEN DATE_FORMAT(ubd.begin_date_ubd, '%d/%m/%Y') AND 
							DATE_SUB(IFNULL(ubd.end_date_ubd, DATE_ADD(CURRENT_DATE(), INTERVAL 2 DAY)), INTERVAL 1 DAY)
					JOIN sector sec ON sec.serial_sec = br.serial_sec
					JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou $city_clause
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal
						AND slg.type_slg IN ('VOID_INVOICE','REFUNDED', 'VOID_SALE')
						AND slg.status_slg = 'AUTHORIZED'
						AND s.status_sal IN ('VOID','REFUNDED')
						AND s.status_sal <> 'DENIED'";
        if ($effective_sales) {
            $sql .= $effective_clause;
        } else {
            $sql .= "WHERE slg.date_slg BETWEEN STR_TO_DATE( '01/" . $date_of_analisys . "' ,'%d/%m/%Y') AND STR_TO_DATE('" . $lastDay . "/" . $date_of_analisys . "','%d/%m/%Y')";
        }

        $sql .= "	$stock_clause
					$sale_type_clause
					$amount_clause
			GROUP BY s.serial_sal) ORDER BY 1, $order_field";

        //die(Debug::print_r($sql));
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getInfoForManagerSalesAnalysisReport($db, $serial_cou, $serial_mbc, $begin_date, $end_date, $active_only = TRUE)
    {
        if ($active_only):
            $active_only_sql = " AND d.status_dea = 'ACTIVE'";
        endif;

        $sql = "SELECT	DISTINCT u.serial_usr, 
						CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr',
						SUM(sal.total_sal) as sales, 
						void_table.void, free_table.free, refunded_table.refunded
				FROM user u " .
            self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'VOID', $active_only) .
            self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'FREE', $active_only) .
            self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'REFUNDED', $active_only) . "
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
				JOIN dealer d ON d.serial_dea=ubd.serial_dea $active_only_sql
				JOIN dealer parentd ON parentd.serial_dea = d.dea_serial_dea
				JOIN counter cnt ON cnt.serial_dea = d.serial_dea
				JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt 
					AND sal.in_date_sal 
								BETWEEN ubd.begin_date_ubd AND 
								IFNULL(ubd.end_date_ubd, CURRENT_DATE())
                    
                    AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
					 	BETWEEN ubd.begin_date_ubd AND 
						 IFNULL(ubd.end_date_ubd, CURRENT_DATE())
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
				WHERE d.serial_mbc = $serial_mbc
				AND mbc.serial_cou = $serial_cou
				AND sal.status_sal <> 'DENIED' and u.status_usr='ACTIVE'
				AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y')
				GROUP BY u.serial_usr";
        //die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getInfoForManagerSalesAnalysisReportModify($db, $serial_cou, $serial_mbc, $dateFrom, $dateTo)
    {
        if (!empty($dateFrom) && !empty($dateTo)){
            //$andDates = " AND sal.in_date_sal BETWEEN '$dateFrom' AND '$dateTo'";
            $andDates = " AND inv.date_inv BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
            $andCreditNoteDates = "cn.date_cn BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59'";
        }

        $sql = "SELECT  DISTINCT u.serial_usr, 
                        CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr',
                     --     SUM(sal.total_sal) as sales, 
                       -- SUM(inv.total_inv) as total_inv,
                        round( sum((sal.total_sal-(sal.total_sal*(IFNULL(inv.discount_prcg_inv, 0) + IFNULL(inv.other_dscnt_inv,0))/100))-(ROUND(((sal.total_sal-(sal.total_sal*(IFNULL(inv.discount_prcg_inv, 0) + IFNULL(inv.other_dscnt_inv,0))/100)))*0.005,2))),2) as total_inv,
                        void_table.void,
                        SUM(  ROUND((inv.total_inv * (inv.discount_prcg_inv/100)) ,2) ) as total_discount_Comission,
                        SUM(  ROUND((inv.total_inv * (inv.other_dscnt_inv/100)) ,2) ) as total_other_discount
                  FROM user u  LEFT JOIN ( SELECT serial_usr, SUM(void) as void FROM
                                            (SELECT distinct sal2.card_number_sal, u2.serial_usr,round(cn.amount_cn,2) as void
                        FROM user u2
                        LEFT JOIN user_by_dealer ubd2 ON ubd2.serial_usr = u2.serial_usr  
                        LEFT JOIN dealer d2 ON d2.serial_dea=ubd2.serial_dea
                        LEFT JOIN counter cnt2 ON cnt2.serial_dea = d2.serial_dea 
                        LEFT JOIN sales sal2 ON sal2.serial_cnt = cnt2.serial_cnt 
                        LEFT JOIN manager_by_country mbc2 ON mbc2.serial_mbc = d2.serial_mbc
                         LEFT JOIN invoice inv on sal2.serial_inv = inv.serial_inv
                   left join credit_note cn on inv.serial_inv = cn.serial_inv and cn.serial_sal = sal2.serial_sal
                        WHERE d2.serial_mbc = $serial_mbc
                        AND mbc2.serial_cou = $serial_cou
                        and ubd2.status_ubd='ACTIVE'
                        and  $andCreditNoteDates
                        
                         )as t1
                        GROUP BY serial_usr
                )  as void_table ON u.serial_usr = void_table.serial_usr  
            LEFT     JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
            LEFT     JOIN dealer d ON d.serial_dea=ubd.serial_dea 
            LEFT     JOIN dealer parentd ON parentd.serial_dea = d.dea_serial_dea
            LEFT     JOIN counter cnt ON cnt.serial_dea = d.serial_dea
            LEFT     JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt 
                LEFT JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
                 LEFT JOIN invoice inv on sal.serial_inv = inv.serial_inv
                WHERE d.serial_mbc = $serial_mbc
                AND mbc.serial_cou = $serial_cou
                and ubd.status_ubd='ACTIVE' -- and u.status_usr='ACTIVE'  
                   $andDates
                GROUP BY u.serial_usr; ";
        //die('<pre>'.$sql.'</pre>');
       // Debug::print_r($sql);die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, $card_status = 'ALL', $active_only = TRUE)
    {
        if ($active_only):
            $active_only_sql = " AND d2.status_dea= 'ACTIVE'";
        endif;

        switch ($card_status) {
            case 'VOID':
                $sql_header = "LEFT JOIN ( SELECT serial_usr, SUM(void) as void FROM
											(SELECT DISTINCT sal2.card_number_sal,u2.serial_usr,sal2.total_sal as void";
                $and_statement = "sal2.status_sal = 'VOID'";
                $slg_type_statement = "AND slg.type_slg IN ('VOID_INVOICE', 'VOID_SALE')";
                $sql_end = " as void_table ON u.serial_usr = void_table.serial_usr";
                break;
            case 'FREE':
                $sql_header = "LEFT JOIN ( SELECT serial_usr, SUM(free) as free FROM
											(SELECT DISTINCT sal2.card_number_sal,u2.serial_usr,sal2.total_sal as free";
                $and_statement = "(sal2.free_sal = 'YES' AND sal2.status_sal in ('ACTIVE','EXPIRED','REGISTERED'))";
                $slg_type_statement = "AND slg.type_slg = 'FREE'";
                $sql_end = " as free_table ON u.serial_usr = free_table.serial_usr";
                break;
            case 'REFUNDED':
                $sql_header = "LEFT JOIN ( SELECT serial_usr, SUM(refunded) as refunded FROM
											(SELECT DISTINCT sal2.card_number_sal,u2.serial_usr,sal2.total_sal as refunded";
                $and_statement = "sal2.status_sal = 'REFUNDED'";
                $slg_type_statement = "AND slg.type_slg = 'REFUNDED'";
                $sql_end = " as refunded_table ON u.serial_usr = refunded_table.serial_usr";
                break;
        }

        $sql = " $sql_header
						FROM user u2
						JOIN user_by_dealer ubd2 ON ubd2.serial_usr = u2.serial_usr
						JOIN dealer d2 ON d2.serial_dea=ubd2.serial_dea
						JOIN counter cnt2 ON cnt2.serial_dea = d2.serial_dea $active_only_sql
						JOIN sales sal2 ON sal2.serial_cnt = cnt2.serial_cnt 
							AND sal2.in_date_sal 
								BETWEEN ubd2.begin_date_ubd AND 
								IFNULL(ubd2.end_date_ubd, CURRENT_DATE())
                            
                             AND STR_TO_DATE(DATE_FORMAT(sal2.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
								 BETWEEN ubd2.begin_date_ubd AND 
								 IFNULL(ubd2.end_date_ubd, CURRENT_DATE())
						JOIN manager_by_country mbc2 ON mbc2.serial_mbc = d2.serial_mbc
						JOIN sales_log slg ON slg.serial_sal = sal2.serial_sal
						$slg_type_statement
						AND slg.status_slg = 'AUTHORIZED'
						WHERE d2.serial_mbc = $serial_mbc
						AND mbc2.serial_cou = $serial_cou
						AND $and_statement
						AND (STR_TO_DATE(DATE_FORMAT(slg.date_slg,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y'))
						)as t1
						GROUP BY serial_usr
				) $sql_end ";

        return $sql;
    }

    /*
     * @name: getTotalCardsSoldResume
     * @param: $db - DB connection
     * @return: Resume of cards sold by stock type
     */

    public static function getTotalCardsSoldResume($db, $serial_cou, $serial_cit, $serial_mbc, $serial_usr, $begin_date, $end_date)
    {
        $sql = "SELECT stock_type_sal, COUNT(sal.card_number_sal) AS total_cards
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
		";
        $sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
        $sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
        $sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";
        $sql .= " GROUP BY stock_type_sal";

        $result = $db->getAssoc($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getCardsSoldByCountry
     * @param: $db - DB connection
     * @return: Resume of cards sold by stock type, filtered mandatory by country and beginning and ending date
     */

    public static function getCardsSoldByCountry($db, $serial_cou, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL)
    {

        $sql = "SELECT DISTINCT dea.serial_dea, stock_type_sal, COUNT(sal.card_number_sal) AS total_cards,
						CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr, cit.name_cit, man.name_man,
						CONCAT(dea.name_dea, ' - ', dea.code_dea) as name_dea, usr.serial_usr, man.serial_man, cit.serial_cit
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    AND sal.status_sal in ('STANDBY','ACTIVE','REGISTERED','EXPIRED')
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc 
				JOIN manager man ON man.serial_man = mbc.serial_man
				WHERE STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
				";
        $sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
        $sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";
        $sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";

        $sql .= " GROUP BY dea.serial_dea, stock_type_sal
				  ";
        if ($serial_usr) {
            $sql .= " ORDER BY name_dea, serial_dea";
        } else {
            if ($serial_mbc && $serial_cit) {
                $sql .= " ORDER BY  name_usr, serial_usr, name_dea, serial_dea";
            } else {
                $sql .= " ORDER BY name_cit, serial_cit, name_man, serial_man, name_usr, serial_usr, name_dea, serial_dea";
            }
        }

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getDealersSellingVirtualCards
     * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
     * @return: Returns the number of dealers selling cards in a period of time
     */

    public static function getDealersSellingCards($db, $serial_cou, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL)
    {
        $sql = "SELECT COUNT(DISTINCT dea.serial_dea) as sold
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    AND sal.status_sal in ('STANDBY','ACTIVE','REGISTERED','EXPIRED')
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE dea.status_dea = 'ACTIVE'
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
		";

        $sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
        $sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
        $sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

        $result = $db->getOne($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getCardsSoldByDealer (VIRTUAL CARDS)
     * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
     * @return: Returns a list of dealers who sold in a period of time by country
     */

    public static function getCardsSoldByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL)
    {
        $sql = "SELECT COUNT(sal.card_number_sal) AS total_cards, 
						CONCAT(dea.code_dea, ' - ', dea.name_dea) AS 'name_dea'
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    AND sal.status_sal in ('STANDBY','ACTIVE','REGISTERED','EXPIRED')
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE dea.status_dea = 'ACTIVE' AND sal.stock_type_sal = 'VIRTUAL'
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
		";

        $sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
        $sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
        $sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

        $sql .= " GROUP BY dea.serial_dea
					ORDER BY total_cards DESC
				";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getVirtualCardsSoldInfoByDealer
     * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
     * @return: Returns the number of virtual cards sold by dealer by responsible
     */

    public static function getVirtualCardsSoldInfoByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL, $nimusOneYear = false)
    {
        if ($nimusOneYear) {
            $dateClause = " AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN DATE(STR_TO_DATE('$begin_date','%d/%m/%Y') - INTERVAL 1 YEAR) AND DATE(STR_TO_DATE('$end_date', '%d/%m/%Y') - INTERVAL 1 YEAR)";
        } else {
            $dateClause = " AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') and STR_TO_DATE('$end_date', '%d/%m/%Y')";
        }

        $sql = "SELECT DISTINCT dea.serial_dea, IFNULL(COUNT(sal.card_number_sal) ,0) AS total_cards,
						CONCAT(dea.code_dea, ' - ', dea.name_dea) AS 'name_dea',
						cit.serial_cit, cit.name_cit, mbc.serial_mbc, man.name_man, usr.serial_usr,
						CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr,
						IFNULL(SUM(sal.total_sal) ,0) AS 'sales_amount'
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				LEFT JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
                    AND sal.status_sal in ('STANDBY','ACTIVE','REGISTERED','EXPIRED')
					$dateClause
				WHERE dea.status_dea = 'ACTIVE'";

        $sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
        $sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
        $sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

        $sql .= " GROUP BY dea.serial_dea
				  ORDER BY dea.serial_mbc, ubd.serial_usr, sales_amount DESC";
        //Debug::print_r($sql); die;
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getCardsSoldByDealer (VIRTUAL CARDS)
     * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
     * @return: Returns a list of dealers who sold in a period of time by country
     */

    public static function getTotalAmountsSoldByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL)
    {
        $sql = "SELECT SUM(sal.total_sal) AS total_sales, CONCAT(dea.code_dea, ' - ', dea.name_dea) AS 'name_dea'
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    AND sal.status_sal in ('STANDBY','ACTIVE','REGISTERED','EXPIRED')
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE dea.status_dea = 'ACTIVE'
				AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')";

        $sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
        $sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
        $sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

        $sql .= " GROUP BY dea.serial_dea
				ORDER BY total_sales DESC";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getRefundVoidSalesByManager
     * @param: $db, $serial_mbc,$dateFrom , $dateTo
     * @return: Returns a list of sales by manager available for international cost refund. 
     */

    public static function getVoidAndRefundedSalesByManager($db, $serial_mbc, $dateFrom, $dateTo)
    {
        $sql = "	SELECT	s.serial_sal, s.card_number_sal,dea.name_dea,s.status_sal, slg.description_slg,
							DATE_FORMAT(MAX(slg.date_slg),'%d/%m/%Y') as log_date, usr.first_name_usr,
							usr.last_name_usr,s.begin_date_sal, s.international_fee_amount,
							s.total_sal
					FROM sales s 
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal
					JOIN user usr ON usr.serial_usr = slg.usr_serial_usr
					WHERE dea.serial_mbc = {$serial_mbc}
					AND s.status_sal = 'VOID'
					AND s.international_fee_status IN ('TO_REFUND', 'TO_COLLECT')
					AND s.international_fee_amount > 0
					AND (slg.type_slg = 'VOID_INVOICE' OR slg.type_slg = 'VOID_SALE')
					AND slg.status_slg = 'AUTHORIZED'
					AND STR_TO_DATE(DATE_FORMAT(slg.date_slg, '%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') 
						AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')
					GROUP BY slg.serial_sal
					
				UNION

					SELECT s.serial_sal, s.card_number_sal, dea.name_dea, s.status_sal, slg.description_slg,
							DATE_FORMAT(r.auth_date_ref,'%Y-%m-%d') AS 'log_date', usr.first_name_usr,
							usr.last_name_usr,s.begin_date_sal, s.international_fee_amount,
							s.total_sal
					FROM sales s 
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
					JOIN refund r ON r.serial_sal = s.serial_sal
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal AND slg.type_slg = 'REFUNDED' AND slg.status_slg = 'AUTHORIZED'
					JOIN user usr ON usr.serial_usr = r.serial_usr
					WHERE dea.serial_mbc = {$serial_mbc}
					AND s.status_sal = 'REFUNDED'
					AND s.international_fee_amount > 0
					AND s.international_fee_status IN ('TO_REFUND', 'TO_COLLECT')
					AND STR_TO_DATE(DATE_FORMAT(r.auth_date_ref, '%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') 
						AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')";
        //Debug::print_r($sql); die;
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: getAuthorizedFreeSalesByManager
     * @param: $db, $serial_mbc,$dateFrom , $dateTo
     * @return: Returns a list of free authorized sales by manager available for international cost refund. 
     */

    public static function getAuthorizedFreeSalesByManager($db, $serial_mbc, $dateFrom, $dateTo)
    {
        $sql = "SELECT s.serial_sal, s.card_number_sal,dea.name_dea,s.status_sal, 
						slg.description_slg,usr.first_name_usr,usr.last_name_usr, 
						DATE_FORMAT(MAX(slg.date_slg),'%d/%m/%Y') as log_date,
						observations_sal, s.international_fee_amount, s.total_sal
				FROM sales s 
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				JOIN sales_log slg ON slg.serial_sal = s.serial_sal
				JOIN user usr ON usr.serial_usr = slg.usr_serial_usr
				WHERE dea.serial_mbc = {$serial_mbc}
				AND (s.status_sal = 'STANDBY' OR s.status_sal = 'ACTIVE' OR s.status_sal = 'EXPIRED')
				AND s.international_fee_status = 'TO_COLLECT'
				AND s.free_sal = 'YES'
				AND s.international_fee_used = 'NO'
				AND slg.type_slg = 'FREE'
				AND slg.status_slg = 'AUTHORIZED'
				AND s.international_fee_amount > 0
				AND STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y')  BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')
				GROUP BY slg.serial_sal ";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: updateInternationalFeeToRefund
     * @param: $db, $salesToUpdate
     * @return: Update sales changing international_fee_status to 
     */

    public static function updateInternationalFeeToRefund($db, $salesToUpdate, $resetValue = FALSE)
    {
        $sql = "UPDATE sales 
				SET international_fee_status = 'TO_REFUND' ";
        $sql .= ($resetValue) ? " , international_fee_amount = 0 " : "";
        $sql .= "	WHERE serial_sal IN({$salesToUpdate})";
        $result = $db->Execute($sql);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * @name: checkIfSaleIsMasiveByCardNumber
     * @param: $db, $cardNumber
     * @return: true or false
     */

    public static function checkIfSaleIsMasiveByCardNumber($db, $card_number)
    {
        $sql = "SELECT s.serial_sal
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
			JOIN product p ON pxc.serial_pro=p.serial_pro AND p.masive_pro='YES'
			WHERE s.card_number_sal LIKE '$card_number'";

        $result = $db->getOne($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @name totalSoldByProductByDates
     * @param type $db
     * @param type $begin_date
     * @param type $end_date
     * @param type $date_field
     * @param type $serial_cou
     * @param type $serial_mbc
     * @param type $serial_dea
     * @param type $serial_bra
     * @param type $serial_cit
     * @return array Data
     * @
     */
    public static function totalSoldByProductByDates($db, $begin_date, $end_date, $date_field, $serial_cou = NULL, $serial_mbc = NULL, $serial_dea = NULL, $serial_bra = NULL, $serial_cit = NULL)
    {
        $extra_params = "";
        if ($serial_cou)
            $extra_params .= " AND pxc.serial_cou = $serial_cou";
        if ($serial_mbc)
            $extra_params .= " AND bra.serial_mbc = $serial_mbc";
        if ($serial_dea)
            $extra_params .= " AND pbd.serial_dea = $serial_dea";
        if ($serial_bra)
            $extra_params .= " AND bra.serial_dea = $serial_bra";
        if ($serial_cit)
            $extra_params .= " AND sec.serial_cit = $serial_cit";

        if ($date_field == 'in_date_sal') {
            $date_field = 's.in_date_sal';
        } else {
            $date_field = 'f.creation_date_fle';
        }

        $sql = "SELECT i.serial_pro, 
                                                i.name_pbl,
                                                SUM(i.total_sal) AS 'total_sold',
                                                SUM(i.estimated_amount) AS 'estimated_amount',
						SUM(i.cost_sold) AS 'cost_sold',
						SUM(IFNULL(p_requests.unpaid_fees,0)) AS 'unpaid_fees',
						SUM(IFNULL(p_requests.paid_fees,0)) AS 'paid_fees',
						SUM(IFNULL(p_requests.unpaid_management,0)) AS 'unpaid_management',
						SUM(IFNULL(p_requests.paid_management,0)) AS 'paid_management',
						SUM(IFNULL(p_requests.unpaid_auditory,0)) AS 'unpaid_auditory',
						SUM(IFNULL(p_requests.paid_auditory,0)) AS 'paid_auditory',
						SUM(IFNULL(p_requests.unpaid_repricing,0)) AS 'unpaid_repricing',
						SUM(IFNULL(p_requests.paid_repricing,0)) AS 'paid_repricing',
						SUM(IFNULL(p_requests.unpaid_credit_note,0)) AS 'unpaid_credit_note',
						SUM(IFNULL(p_requests.paid_credit_note,0)) AS 'paid_credit_note'
				FROM (SELECT s.serial_sal,f.serial_fle, pxc.serial_pro, pbl.name_pbl, 
							s.total_sal,
							SUM(IF((f.status_fle = 'closed'),0,IFNULL(spf.estimated_amount_spf, 0))) AS 'estimated_amount',	
							s.total_cost_sal AS 'cost_sold'
					FROM sales s
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.status_sal in ('ACTIVE','EXPIRED', 'STANDBY', 'REGISTERED')
					JOIN dealer bra ON bra.serial_dea = cnt.serial_dea AND bra.status_dea = 'ACTIVE'
					JOIN sector sec ON sec.serial_sec = bra.serial_sec
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					LEFT JOIN file f ON f.serial_sal = s.serial_sal
					LEFT JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle	
					WHERE STR_TO_DATE(DATE_FORMAT($date_field,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN 
							STR_TO_DATE('$begin_date','%d/%m/%Y') AND 
							STR_TO_DATE('$end_date', '%d/%m/%Y')
					$extra_params
					GROUP BY pxc.serial_pro, s.serial_sal) AS i
					
                                        LEFT JOIN(SELECT s.serial_sal,
                                                        f.serial_fle, 
                                                        SUM(IF((prq.type_prq = 'FEE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_fees,
							SUM(IF((prq.type_prq = 'FEE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_fees,
							SUM(IF((prq.type_prq = 'MANAGEMENT' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_management,
							SUM(IF((prq.type_prq = 'MANAGEMENT' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_management,
							SUM(IF((prq.type_prq = 'AUDITORY' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_auditory,
							SUM(IF((prq.type_prq = 'AUDITORY' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_auditory,
							SUM(IF((prq.type_prq = 'REPRICING' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_repricing,
							SUM(IF((prq.type_prq = 'REPRICING' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_repricing,
							SUM(IF((prq.type_prq = 'CREDIT_NOTE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_credit_note,
							SUM(IF((prq.type_prq = 'CREDIT_NOTE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_credit_note
                                        FROM sales s
                                        JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
                                        JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                                        JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = 2
                                        LEFT JOIN file f ON f.serial_sal = s.serial_sal
                                        LEFT JOIN payment_request prq ON prq.serial_fle = f.serial_fle
                                        WHERE STR_TO_DATE(DATE_FORMAT($date_field,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN 
							STR_TO_DATE('$begin_date','%d/%m/%Y') AND 
							STR_TO_DATE('$end_date', '%d/%m/%Y')
                    $extra_params
                GROUP BY s.serial_sal)  as p_requests  ON i.serial_fle = p_requests.serial_fle AND i.serial_sal = p_requests.serial_sal
				GROUP BY i.serial_pro
				ORDER BY i.name_pbl";


//SQL ANTERIOR, SE DEJA PARA RESPALDO
//				$sql = "SELECT i.serial_pro, i.name_pbl, SUM(i.total_sal) AS 'total_sold',
//						SUM(i.estimated_amount) AS 'estimated_amount',
//						SUM(i.cost_sold) AS 'cost_sold',
//						SUM(IFNULL(p_requests.unpaid_fees,0)) AS 'unpaid_fees',
//						SUM(IFNULL(p_requests.paid_fees,0)) AS 'paid_fees',
//						SUM(IFNULL(p_requests.unpaid_management,0)) AS 'unpaid_management',
//						SUM(IFNULL(p_requests.paid_management,0)) AS 'paid_management',
//						SUM(IFNULL(p_requests.unpaid_auditory,0)) AS 'unpaid_auditory',
//						SUM(IFNULL(p_requests.paid_auditory,0)) AS 'paid_auditory',
//						SUM(IFNULL(p_requests.unpaid_repricing,0)) AS 'unpaid_repricing',
//						SUM(IFNULL(p_requests.paid_repricing,0)) AS 'paid_repricing',
//						SUM(IFNULL(p_requests.unpaid_credit_note,0)) AS 'unpaid_credit_note',
//						SUM(IFNULL(p_requests.paid_credit_note,0)) AS 'paid_credit_note'
//				FROM (SELECT s.serial_sal,f.serial_fle, pxc.serial_pro, pbl.name_pbl, 
//							s.total_sal,
//							SUM(IF((f.status_fle = 'closed'),0,IFNULL(spf.estimated_amount_spf, 0))) AS 'estimated_amount',	
//							s.total_cost_sal AS 'cost_sold'
//					FROM sales s
//					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.status_sal <> 'DENIED'
//					JOIN dealer bra ON bra.serial_dea = cnt.serial_dea AND bra.status_dea = 'ACTIVE'
//					JOIN sector sec ON sec.serial_sec = bra.serial_sec
//					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
//					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
//					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
//					LEFT JOIN file f ON f.serial_sal = s.serial_sal
//					LEFT JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle	
//					WHERE STR_TO_DATE(DATE_FORMAT($date_field,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN 
//							STR_TO_DATE('$begin_date','%d/%m/%Y') AND 
//							STR_TO_DATE('$end_date', '%d/%m/%Y')
//					$extra_params
//					GROUP BY pxc.serial_pro, s.serial_sal) AS i
//					
//				LEFT JOIN(SELECT s.serial_sal,f.serial_fle, SUM(IF((prq.type_prq = 'FEE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_fees,
//							SUM(IF((prq.type_prq = 'FEE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_fees,
//							SUM(IF((prq.type_prq = 'MANAGEMENT' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_management,
//							SUM(IF((prq.type_prq = 'MANAGEMENT' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_management,
//							SUM(IF((prq.type_prq = 'AUDITORY' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_auditory,
//							SUM(IF((prq.type_prq = 'AUDITORY' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_auditory,
//							SUM(IF((prq.type_prq = 'REPRICING' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_repricing,
//							SUM(IF((prq.type_prq = 'REPRICING' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_repricing,
//							SUM(IF((prq.type_prq = 'CREDIT_NOTE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_credit_note,
//							SUM(IF((prq.type_prq = 'CREDIT_NOTE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_credit_note
//                FROM sales s
//                JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
//                JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
//                JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = 2
//                LEFT JOIN file f ON f.serial_sal = s.serial_sal
//                LEFT JOIN payment_request prq ON prq.serial_fle = f.serial_fle
//               WHERE STR_TO_DATE(DATE_FORMAT($date_field,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN 
//							STR_TO_DATE('$begin_date','%d/%m/%Y') AND 
//							STR_TO_DATE('$end_date', '%d/%m/%Y')
//                    $extra_params
//                GROUP BY s.serial_sal)  as p_requests  ON i.serial_fle = p_requests.serial_fle AND i.serial_sal = p_requests.serial_sal
//				GROUP BY i.serial_pro
//				ORDER BY i.name_pbl";
        //die(Debug::print_r($sql));
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    //FUNCION QUE OBTIENE LAS ANULADAS, REEMBOLSOS, Y FREE DE UN PERIODO DETERMINADO PARA SINIESTRALIDAD

    public static function getInfoForLossReport($db, $begin_date, $end_date, $date_field, $serial_cou = NULL, $serial_mbc = NULL, $serial_dea = NULL, $serial_bra = NULL, $serial_cit = NULL)
    {
        $extra_params = "";
        if ($serial_cou)
            $extra_params .= " AND pxc.serial_cou = $serial_cou";
        if ($serial_mbc)
            $extra_params .= " AND bra.serial_mbc = $serial_mbc";
        if ($serial_dea)
            $extra_params .= " AND pbd.serial_dea = $serial_dea";
        if ($serial_bra)
            $extra_params .= " AND bra.serial_dea = $serial_bra";
        if ($serial_cit)
            $extra_params .= " AND sec.serial_cit = $serial_cit";

        if ($date_field == 'in_date_sal') {
            $date_field = 's.in_date_sal';
        } else {
            $date_field = 'f.creation_date_fle';
        }

        $sql = "SELECT distinct	p.serial_pro, void_table.void, free_table.free, refunded_table.refunded 
                                FROM  product p " .
            self::getVoidSalesSQL($begin_date, $end_date, 'VOID', $active_only, $serial_mbc, $serial_cou) .
            self::getVoidSalesSQL($begin_date, $end_date, 'FREE', $active_only, $serial_mbc, $serial_cou) .
            self::getVoidSalesSQL($begin_date, $end_date, 'REFUNDED', $active_only, $serial_mbc, $serial_cou) . "
                        Where void_table.void is not null || free_table.free is not null || refunded_table.refunded is not null";
        // echo $sql;      die();                 
        //die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getVoidSalesSQL($begin_date, $end_date, $card_status = 'ALL', $active_only = TRUE, $serial_mbc = NULL, $serial_cou = NULL)
    {
        if ($active_only):
            $active_only_sql = " AND d2.status_dea= 'ACTIVE'";
        endif;

        $extra_params = "";
        if ($serial_cou)
            $extra_params .= " AND mbc2.serial_cou = $serial_cou";
        if ($serial_mbc)
            $extra_params .= " AND d2.serial_mbc = $serial_mbc";


        switch ($card_status) {
            case 'VOID':
                $sql_header = "LEFT JOIN ( SELECT serial_pro,name_pbl, SUM(void) as void FROM
											(SELECT DISTINCT s2.serial_sal,pxc2.serial_pro, pbl2.name_pbl, s2.total_sal as void";
                $and_statement = "s2.status_sal = 'VOID'";
                $slg_type_statement = "AND slg2.type_slg IN ('VOID_INVOICE', 'VOID_SALE')";
                $sql_end = " as void_table ON p.serial_pro = void_table.serial_pro";
                break;
            case 'FREE':
                $sql_header = "LEFT JOIN ( SELECT serial_pro,name_pbl, SUM(free) as free FROM
											(SELECT DISTINCT s2.serial_sal,pxc2.serial_pro, pbl2.name_pbl, s2.total_sal as free";
                $and_statement = "(s2.free_sal = 'YES' AND s2.status_sal in ('ACTIVE','EXPIRED'))";
                $slg_type_statement = "AND slg2.type_slg = 'FREE'";
                $sql_end = " as free_table ON p.serial_pro = free_table.serial_pro";
                break;
            case 'REFUNDED':
                $sql_header = "LEFT JOIN ( SELECT serial_pro,name_pbl, SUM(refunded) as refunded FROM
											(SELECT DISTINCT s2.serial_sal,pxc2.serial_pro, pbl2.name_pbl, s2.total_sal as refunded";
                $and_statement = "s2.status_sal = 'REFUNDED'";
                $slg_type_statement = "AND slg2.type_slg = 'REFUNDED'";
                $sql_end = " as refunded_table ON p.serial_pro = refunded_table.serial_pro";
                break;
        }

        $sql = " $sql_header
						FROM sales s2
                                                JOIN counter cnt2 ON cnt2.serial_cnt = s2.serial_cnt
                                                JOIN dealer d2 ON d2.serial_dea = cnt2.serial_dea AND d2.status_dea $active_only_sql
                                                JOIN product_by_dealer pbd2 ON pbd2.serial_pbd = s2.serial_pbd
                                                JOIN product_by_country pxc2 ON pxc2.serial_pxc = pbd2.serial_pxc
                                                JOIN product_by_language pbl2 ON pbl2.serial_pro = pxc2.serial_pro AND pbl2.serial_lang = 2
                                                JOIN manager_by_country mbc2 ON mbc2.serial_mbc = d2.serial_mbc
                                                JOIN sales_log slg2 ON slg2.serial_sal = s2.serial_sal
						$slg_type_statement
						AND slg2.status_slg = 'AUTHORIZED'
						WHERE 
						$and_statement $extra_params
						AND (STR_TO_DATE(DATE_FORMAT(slg2.date_slg,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y'))
						)as t1
						GROUP BY serial_pro
				) $sql_end ";

        return $sql;
    }

    public static function updateErpId($db, $serial_sal, $erp_serial)
    {
        $sql = "UPDATE sales
			   SET id_erp_sal = $erp_serial
			   WHERE serial_sal = $serial_sal";

        $result = $db->Execute($sql);

        if (!$result)
            ErrorLog::log($db, 'ERP PROCESS FAILED - UPDATE ERPID SALES', $sql);
    }

    public static function getSalesBasicInfo($db, $serial_sal)
    {
        $sql = "SELECT	DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
						DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
						DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
						CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
						s.days_sal, pbl.name_pbl, s.total_sal
				FROM sales s 
				JOIN customer cus ON cus.serial_cus = s.serial_cus
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
				WHERE s.serial_sal = $serial_sal";

        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getDestinationsValues($db, $serial_cou, $begin_date, $end_date, $date_type, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL, $serial_dea = NULL, $serial_bra = NULL)
    {
        $city_filter = $serial_cit ? ' AND cit.serial_cit = ' . $serial_cit : '';
        $mbc_filter = $serial_mbc ? ' AND dea.serial_mbc = ' . $serial_mbc : '';
        $usr_filter = $serial_usr ? ' AND ubd.serial_usr = ' . $serial_usr : '';
        $dea_filter = $serial_dea ? ' AND dea.serial_dea = ' . $serial_dea : '';
        $bra_filter = $serial_bra ? ' AND br.serial_dea = ' . $serial_bra : '';


        $sql = "SELECT dest_cou.serial_cou, dest_cou.name_cou, dest_cit.name_cit, count(serial_sal) AS 'travels'
				FROm sales s
				JOIN city dest_cit On dest_cit.serial_cit = s.serial_cit
				JOIN country dest_cou ON dest_cou.serial_cou = dest_cit.serial_cou
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer br ON br.serial_dea = cnt.serial_dea $bra_filter
				JOIN dealer dea ON dea.serial_dea = br.dea_serial_dea $mbc_filter $dea_filter
				JOIN sector sec ON br.serial_sec = sec.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit $city_filter
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.serial_cou = $serial_cou
				JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $usr_filter
					";

        if ($date_type == "insystem") {
            $sql .= " WHERE (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
        } else if ($date_type == "coverage") {
            $sql .= " WHERE STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')";
        }

        $sql .= "	GROUP BY dest_cit.serial_cit
					ORDER BY travels DESC";

        //Debug::print_r($sql);

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function internationalFlight($db, $serial_sal, $serial_trl = NULL)
    {
        $sql = "SELECT pxc.serial_cou AS 'origin', c.serial_cou AS 'destination'
				FROM sales s 
				JOIN prodcut_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc";

        if ($serial_trl) {
            $sql .= " JOIN travel_register tr On tr.serial_sal = s.serial_sal AND tr.serial_trl = $serial_trl
					  JOIN city c ON c.serial_cit = tr.serial_cit";
        } else {
            $sql .= " JOIN city c ON c.serial_cit = s.serial_cit";
        }

        $result = $db->getRow($sql);

        if ($result) {
            if ($result['origin'] == $result['destination']) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;
        }
    }

    public static function getDestinationsByProduct($db, $serial_cou, $begin_date, $end_date, $date_type, $serial_mbc = NULL, $serial_pro = NULL, $serial_usr = NULL, $serial_dea = NULL, $serial_bra = NULL, $serial_type = NULL)
    {

        $product_filter = $serial_pro ? ' AND pbl.serial_pro = ' . $serial_pro : '';
        $mbc_filter = $serial_mbc ? ' AND dea.serial_mbc = ' . $serial_mbc : '';
        $usr_filter = $serial_usr ? ' AND ubd.serial_usr = ' . $serial_usr : '';
        $dea_filter = $serial_dea ? ' AND dea.serial_dea = ' . $serial_dea : '';
        $bra_filter = $serial_bra ? ' AND br.serial_dea = ' . $serial_bra : '';
        $type_filter = $serial_type ? ' AND br.serial_dlt = ' . $serial_type : '';

        $sql = "SELECT pbl.name_pbl, dest_cou.name_cou, count(s.serial_sal) AS 'travels'
                        FROM sales s 
                        JOIN city dest_cit ON dest_cit.serial_cit = s.serial_cit
                        JOIN country dest_cou ON dest_cou.serial_cou = dest_cit.serial_cou
                        JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
                        JOIN dealer br ON br.serial_dea = cnt.serial_dea $bra_filter
			JOIN dealer dea ON dea.serial_dea = br.dea_serial_dea $mbc_filter $dea_filter $type_filter
                        JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $usr_filter AND ubd.status_ubd = 'ACTIVE'
			JOIN sector sec ON br.serial_sec = sec.serial_sec
                        JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
                        JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
                        JOIN product_by_country pxc ON pbd.serial_pxc = pxc.serial_pxc 
                        JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro $product_filter AND pbl.serial_lang = 2";

        if ($date_type == "insystem") {
            $sql .= " WHERE (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
        } else if ($date_type == "coverage") {
            $sql .= " WHERE STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')";
        }

        $sql .= "	GROUP BY pbl.name_pbl, dest_cou.name_cou
				ORDER BY pbl.name_pbl, travels DESC";

        //Debug::print_r($sql);

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
     * getCardManagerReport
     * Report with cost informations
     */

    public static function getCardManagerReport($db, $serial_cou, $date_type, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL, $dea_serial_dea = NULL, $serial_dea = NULL, $serial_pro = NULL, $type_sal = NULL, $status_sal = NULL, $operator = NULL, $total_sal = NULL, $stock_type = NULL, $order_by = NULL)
    {

        if ($order_by == NULL || $order_by == "card_number") {
            $order_by = "col1";
        } else {
            $order_by = "col2";
        }
        if ($serial_usr != NULL) {
            $responsable = " JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.serial_usr = '$serial_usr' AND ubd.status_ubd = 'ACTIVE'";
        }
        $sql = "SELECT  s.serial_sal,
						IFNULL(card_number_sal, 'N/A') as col1,
        				DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as col2,
        				CONCAT(first_name_cus,' ',last_name_cus) as col3,
						(IFNULL(COUNT(e.serial_ext),0)+1) as total_pas,
                     	pbl.name_pbl as col4,
                     	DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as col5, 
                     	DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as col6,
                     	DATEDIFF(s.end_date_sal,s.begin_date_sal)+1 as col7,
                     	s.total_sal as col8,
                     	IF(s.free_sal = 'YES', 'FREE', s.status_sal) as col9,
                     	dea.name_dea as col10,
                     	CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as col11,
                     	s.total_cost_sal as total_cost,
						cus.document_cus,
						DATE_FORMAT(cus.birthdate_cus, '%d/%m/%Y') AS 'birthdate_cus',
						cou.name_cou AS 'name_country',
                                                i.discount_prcg_inv AS 'discount',
                                                i.other_dscnt_inv AS 'other_discount'
				FROM sales s
				LEFT JOIN city cit_dest ON cit_dest.serial_cit = s.serial_cit
				LEFT JOIN country cou ON cit_dest.serial_cou = cou.serial_cou
				LEFT JOIN extras e ON s.serial_sal = e.serial_sal
                                LEFT JOIN invoice i ON s.serial_inv = i.serial_inv
				JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				$responsable
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				$refund_join
				WHERE cit.serial_cou='$serial_cou'";

        if ($date_type == "insystem") {
            $sql .= " AND (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
        } else if ($date_type == "coverage") {
            $sql .= " AND STR_TO_DATE(DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')
                      ";
        }
        //AND STR_TO_DATE(DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')) Se 
        if ($serial_cit != NULL) {
            $sql .= " AND cit.serial_cit = '$serial_cit'";
        }
        if ($serial_mbc != NULL) {
            $sql .= " AND dea.serial_mbc = '$serial_mbc'";
        }

        if ($dea_serial_dea != NULL) {
            $sql .= " AND dea.dea_serial_dea = '$dea_serial_dea'";
        }
        if ($serial_dea != NULL) {
            $sql .= " AND dea.serial_dea = '$serial_dea'";
        }
        if ($serial_pro != NULL) {
            $sql .= " AND pxc.serial_pro = '$serial_pro'";
        }
        if ($type_sal != NULL) {
            $sql .= " AND s.type_sal = '$type_sal'";
        }
        if ($total_sal != NULL) {
            $sql .= " AND s.total_sal " . $operator . $total_sal;
        }
        if ($stock_type != NULL) {
            $sql .= " AND s.stock_type_sal = '$stock_type'";
        }
        if ($status_sal != NULL) {
            $sql .= " AND s.status_sal IN ($status_sal)";
        }
        $sql .= " group by s.serial_sal, pbl.name_pbl  ORDER BY $order_by";
//        Debug::print_r($sql);die();
        //echo $sql;
        //die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /* New Report for manager */

    public static function getInfoForManagerAnalysisReport($db, $serial_cou, $serial_mbc, $begin_date, $end_date)
    {
        if ($active_only):
            $active_only_sql = " AND d.status_dea = 'ACTIVE'";
        endif;

        $sql = "SELECT	DISTINCT u.serial_usr, 
						CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr',
						SUM(sal.total_sal) as sales, 
                                                void_table.void, free_table.free, refunded_table.refunded,
						SUM(sal.total_cost_sal) as cost,
						comissions_table.effective_comission AS 'effective_comission'
				FROM user u " .
            self::SQLComissions($serial_cou, $begin_date, $end_date, $serial_mbc) .
            self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'VOID', $active_only) .
            self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'FREE', $active_only) .
            self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'REFUNDED', $active_only) . "
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr 
				JOIN dealer d ON d.serial_dea=ubd.serial_dea $active_only_sql
				JOIN dealer parentd ON parentd.serial_dea = d.dea_serial_dea
				JOIN counter cnt ON cnt.serial_dea = d.serial_dea
				JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt 
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
						BETWEEN ubd.begin_date_ubd AND 
						IFNULL(ubd.end_date_ubd, CURRENT_DATE())
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
				WHERE d.serial_mbc = $serial_mbc
				AND mbc.serial_cou = $serial_cou
				AND sal.status_sal <> 'DENIED' AND u.status_usr='ACTIVE'
				AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y')
				GROUP BY u.serial_usr";
        //die('<pre>'.$sql.'</pre>');
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function SQLComissions($serial_cou, $begin_date, $end_date, $serial_mbc = NULL)
    {
        if ($serial_mbc)
            $mbc_clause = " AND va.serial_mbc = $serial_mbc";


        $sql = "LEFT JOIN (SELECT va.serial_usr,
							ROUND(AVG(global_comission), 2) AS 'theoric_comission',
							ROUND((SUM(comission_value)/SUM(total_sal)) * 100, 2) AS 'effective_comission'
							FROM view_average_comission va 
							JOIN user u ON u.serial_usr = va.serial_usr
							WHERE STR_TO_DATE(DATE_FORMAT(emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y') 
								BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
				AND serial_cou = $serial_cou
				$mbc_clause
				GROUP BY va.serial_usr
				 )  AS comissions_table ON comissions_table.serial_usr = u.serial_usr";

        //Debug::print_r($sql);
        //$result = $db->getAll($sql);

        return $sql;
    }

    public static function getSalesByCustomer($db, $serial_cus, $restrictList = true)
    {

        if ($restrictList = true) {
            $condition = "AND sal.status_sal NOT IN ('VOID','BLOCKED', 'DENIED')";
        } else {
            $condition = "AND sal.status_sal IN ('STANDBY','ACTIVE', 'REGISTERED')";
        }

        $sql = "SELECT sal.card_number_sal, mbc.serial_mbc, sal.in_date_sal,  pbl.name_pbl,cou.name_cou, 
            sal.begin_date_sal, sal.end_date_sal, sal.status_sal, sal.serial_sal, cit.serial_cit, cou.serial_cou,
            IF (inv.serial_dea IS NULL,cusinv.document_cus, dea.id_dea) as document, inv.number_inv 
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus AND cus.serial_cus = '$serial_cus'
                $condition
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
                LEFT JOIN city cit ON cit.serial_cit= sal.serial_cit
                LEFT JOIN country cou  ON cou.serial_cou = cit.serial_cou
                LEFT JOIN invoice inv ON sal.serial_inv = inv.serial_inv 		        
                LEFT JOIN document_by_manager dbm ON dbm.serial_dbm = inv.serial_dbm
                LEFT JOIN manager man ON man.serial_man = dbm.serial_man
                LEFT JOIN manager_by_country mbc ON mbc.serial_man = man.serial_man 
                LEFT JOIN dealer dea ON dea.serial_dea = inv.serial_dea
                LEFT JOIN customer cusinv ON cusinv.serial_cus = inv.serial_cus
                ";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getDataPdfBySale($db, $serial_sal)
    {
        $sql = "SELECT  sal.in_date_sal, sal.card_number_sal,cus.last_name_cus, cus.first_name_cus,cus.document_cus, cus.birthdate_cus,
                cus.phone1_cus, cus.email_cus, cus.address_cus, cus.relative_cus, cus.relative_phone_cus, pbl.name_pbl, sal.fee_sal, 
                sal.total_sal, dea.serial_dea as serialComercializador, dea.name_dea as nameComercializador, usr.serial_usr as serialCounter, 
                CONCAT (usr.first_name_usr,' ' , usr.last_name_usr) as nameCounter, cou.serial_cou, cou.name_cou, citcus.serial_cit as serialCitCus, 
                citcus.name_cit as nameCitCus, coucus.serial_cou as serialCouCus, coucus.name_cou as nameCouCus, pbl.description_pbl,
                CONCAT(coudea.code_cou,'-',citdea.code_cit,'-',d.code_dea,'-',dea.code_dea) as 'code', citdea.name_cit as citDea
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus AND sal.serial_sal = '$serial_sal'
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
                JOIN city cit ON cit.serial_cit= sal.serial_cit
                JOIN country cou  ON cou.serial_cou = cit.serial_cou 
                JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt 
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea 
                JOIN user usr ON usr.serial_usr = cnt.serial_usr  
                JOIN city citcus ON citcus.serial_cit= cus.serial_cit
                JOIN country coucus  ON coucus.serial_cou = citcus.serial_cou 
                JOIN dealer d ON d.serial_dea= dea.dea_serial_dea
                JOIN sector sec ON sec.serial_sec=dea.serial_sec
                JOIN city citdea ON citdea.serial_cit=sec.serial_cit
                JOIN country coudea ON coudea.serial_cou=citdea.serial_cou

                ";
//        echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getRegisterTravelByCustomer($db, $serial_cus)
    {
        $sql = "SELECT sal.serial_sal, sal.card_number_sal, pbl.name_pbl, sal.in_date_sal, cou.serial_cou, cou.name_cou, sal.begin_date_sal, sal.end_date_sal, sal.days_sal, IF ((sal.days_sal-SUM(tvl.days_trl)) IS NULL, sal.days_sal,(sal.days_sal-SUM(tvl.days_trl))) as days
                FROM sales sal
                JOIN customer cus ON cus.serial_cus = sal.serial_cus  AND cus.serial_cus = '$serial_cus' AND sal.status_sal IN ( 'ACTIVE', 'REGISTERED')
                LEFT JOIN traveler_log tvl ON sal.serial_sal = tvl.serial_sal 
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                                JOIN product pro ON pro.serial_pro = pbc.serial_pro AND pro.flights_pro = 0
                                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2                                
                LEFT JOIN city cit ON cit.serial_cit= tvl.serial_cit
                LEFT JOIN country cou  ON cou.serial_cou = cit.serial_cou
                GROUP BY sal.serial_sal";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getTravelsBySale($db, $serial_sal)
    {
        $sql = "SELECT  sal.card_number_sal, cus.first_name_cus, cus.last_name_cus, cou.name_cou, tvl.start_trl, tvl.end_trl, tvl.days_trl
                FROM sales sal 
                JOIN traveler_log tvl ON sal.serial_sal = tvl.serial_sal AND sal.serial_sal = '$serial_sal'
                JOIN customer cus ON cus.serial_cus = sal.serial_cus                   
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
                JOIN city cit ON cit.serial_cit= tvl.serial_cit
                JOIN country cou  ON cou.serial_cou = cit.serial_cou";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getTransactionByCustomer($db, $serial_Cus)
    {
        $sql = "SELECT transaction_id FROM sales WHERE serial_cus = $serial_Cus";
        //echo $sql;
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSalesByTransaction($db, $transaction_id)
    {
        $sql = "SELECT transaction_id, serial_sal, serial_cus FROM sales WHERE transaction_id = $transaction_id";
        //echo $sql;
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function getSalesByCustomerOrCardCompleteData($db, $documentCus = null, $cardNumberSal = null)
    {

        if ($documentCus) {
            $filter = "AND cus.document_cus = '$documentCus'";
        } else {
            $filter .= "AND sal.card_number_sal = '$cardNumberSal'";
        }
        $sql = "SELECT cus.serial_cus, concat(cus.first_name_cus,' ',cus.last_name_cus) as name, cus.document_cus, cus.address_cus, cus.phone1_cus, cus.phone2_cus, cus.cellphone_cus ,
            cus.birthdate_cus, cus.email_cus, cus.relative_cus, cus.relative_phone_cus,  sal.serial_sal,  sal.card_number_sal, sal.in_date_sal,   
            sal.begin_date_sal, sal.end_date_sal, sal.status_sal, concat(cusext.first_name_cus,' ',cusext.last_name_cus) as nameExt, cusext.document_cus as documentExt, ext.relationship_ext
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus $filter                
				LEFT JOIN extras ext ON ext.serial_sal = sal.serial_sal
                LEFT JOIN customer cusext ON cusext.serial_cus = ext.serial_cus
                ";

        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getSaleInformationbyCardNumber($db, $card_number)
    {
        $sql = "SELECT DISTINCT sal.emission_date_sal, sal.begin_date_sal, sal.end_date_sal,
                sal.card_number_sal, pbc.serial_pxc, pbl.name_pbl, IF (sal.serial_inv IS NULL, 'Pendiente', 'Facturado') as statusInvoice, 
                inv.number_inv, dea.serial_dea, dea.name_dea, concat (usr.first_name_usr, usr.last_name_usr) as nameUser, 
                IF (inv.serial_pay IS NULL, 'Pendiente', 'Pagado') as statusPayment, concat( ud.first_name_usr ,' ', ud.last_name_usr) as nameResp              
                FROM sales sal                   
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  AND sal.card_number_sal = '$card_number'
                JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea
                JOIN user_by_dealer ubd ON dea.serial_dea = ubd.serial_dea 
                JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
                JOIN user usr ON usr.serial_usr = cnt.serial_usr 
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro 
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2 
                JOIN user ud ON ubd.serial_usr = ud.serial_usr
                LEFT JOIN invoice inv ON inv.serial_inv = sal.serial_sal 
                LEFT JOIN payments pay ON inv.serial_pay = pay.serial_pay";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /*
      Function for WS Centinel
     *      */

    public static function getSalesByDate($db, $date)
    {
        /* $sql = "(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
           cus.phone1_cus as 'PHONE', sal.card_number_sal AS 'CONTRACT', sal.in_date_sal AS 'FECHA', pbl.name_pbl as 'PRODUCTO'

               FROM customer cus
               JOIN sales sal ON sal.serial_cus = cus.serial_cus and sal.status_sal not in ('VOID') AND date_format(sal.in_date_sal,'%Y-%m-%d') = '$date' and cus.type_cus = 'PERSON'
               JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
               JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
               JOIN product pro ON pro.serial_pro = pbc.serial_pro and pro.third_party_register_pro = 'NO'
               JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
               WHERE
                pbl.serial_pro IN (7,102,105,140,107,110,111,112,115))
               UNION
(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
           cus.phone1_cus as 'PHONE', trl.card_number_trl AS 'CONTRACT', trl.in_date_trl AS 'FECHA', pbl.name_pbl as 'PRODUCTO'

               FROM customer cus
               JOIN traveler_log trl ON trl.serial_cus = cus.serial_cus and cus.type_cus = 'PERSON'  and trl.days_trl > 4  AND date_format(trl.in_date_trl,'%Y-%m-%d') = '$date'
               JOIN sales sal ON sal.serial_sal = trl.serial_sal and sal.status_sal not in ('VOID')
               JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
               JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
               JOIN product pro ON pro.serial_pro = pbc.serial_pro and pro.third_party_register_pro = 'YES'
               JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
               WHERE
                pbl.serial_pro IN (7,102,105,140,107,110,111,112,115))
               UNION
(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
           cus.phone1_cus as 'PHONE', sal.card_number_sal AS 'CONTRACT', sal.in_date_sal AS 'FECHA', pbl.name_pbl as 'PRODUCTO'
               FROM customer cus
               JOIN extras e on e.serial_cus=cus.serial_cus
               JOIN sales sal ON sal.serial_sal = e.serial_sal and sal.status_sal in ('REGISTERED','ACTIVE','STANDBY','EXPIRED') AND date_format(sal.in_date_sal,'%Y-%m-%d') = '$date' and cus.type_cus = 'PERSON'
               JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
               JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
               JOIN product pro ON pro.serial_pro = pbc.serial_pro
               JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
               WHERE
                pbl.serial_pro IN (7,102,105,140,107,110,111,112,115))

";*/


        $sql="(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
            cus.phone1_cus as 'PHONE', sal.card_number_sal AS 'CONTRACT', sal.in_date_sal AS 'FECHA', pbl.name_pbl as 'PRODUCTO', sal.status_sal as 'ESTADO'
                        
                FROM customer cus
                -- JOIN sales sal ON sal.serial_cus = cus.serial_cus and sal.status_sal not in ('VOID') AND date_format(sal.in_date_sal,'%Y-%m-%d') = '2020-01-06' and cus.type_cus = 'PERSON'               
                JOIN sales sal ON sal.serial_cus = cus.serial_cus and sal.status_sal in ('ACTIVE','REGISTERED','REFUNDED','VOID','EXPIRED') AND date_format(sal.in_date_sal,'%Y-%m-%d') = '$date' and cus.type_cus = 'PERSON'               
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro and pro.third_party_register_pro = 'NO'
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2  
                WHERE 
                 pbl.serial_pro IN (7,102,105,140,107,110,111,112,115,176,186,159,160,174,175,170,172,171,173,164,167,168,166,157,158,169,165,188))
                UNION
(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
            cus.phone1_cus as 'PHONE', trl.card_number_trl AS 'CONTRACT', trl.in_date_trl AS 'FECHA', pbl.name_pbl as 'PRODUCTO', sal.status_sal as 'ESTADO'
                        
                FROM customer cus
                JOIN traveler_log trl ON trl.serial_cus = cus.serial_cus and cus.type_cus = 'PERSON'  and trl.days_trl > 4  AND date_format(trl.in_date_trl,'%Y-%m-%d') = '$date'
                -- JOIN sales sal ON sal.serial_sal = trl.serial_sal and sal.status_sal not in ('VOID')              
                JOIN sales sal ON sal.serial_sal = trl.serial_sal  and sal.status_sal in ('ACTIVE','REGISTERED','REFUNDED','VOID','EXPIRED')             
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro and pro.third_party_register_pro = 'YES'
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2 
                WHERE 
                 pbl.serial_pro IN (7,102,105,140,107,110,111,112,115,176,186,159,160,174,175,170,172,171,173,164,167,168,166,157,158,169,165,188))
				UNION
(SELECT DISTINCT cus.document_cus AS 'CEDULA', concat( cus.first_name_cus ,' ', cus.last_name_cus) as 'NOMBRE', cus.birthdate_cus as 'FEC_NACIMIENTO',
            cus.phone1_cus as 'PHONE', sal.card_number_sal AS 'CONTRACT', sal.in_date_sal AS 'FECHA', pbl.name_pbl as 'PRODUCTO', sal.status_sal as 'ESTADO'
                FROM customer cus
                JOIN extras e on e.serial_cus=cus.serial_cus
                JOIN sales sal ON sal.serial_sal = e.serial_sal and sal.status_sal in ('ACTIVE','REGISTERED','REFUNDED','VOID','EXPIRED') AND date_format(sal.in_date_sal,'%Y-%m-%d') = '$date' and cus.type_cus = 'PERSON'                       
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd  
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro -- and pro.third_party_register_pro = 'YES'
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2 
                WHERE 
                 pbl.serial_pro IN (7,102,105,140,107,110,111,112,115,176,186,159,160,174,175,170,172,171,173,164,167,168,166,157,158,169,165,188))";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getSerialSalByCardNumber($db, $cardNumber)
    {
        $sql = "SELECT serial_sal FROM sales WHERE card_number_sal = $cardNumber";

        $result = $db->getOne($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function updateDateByCardNumber($db, $serialSal, $beginDate, $endDate, $serialCit, $status, $days)
    {
        $sql = " UPDATE sales SET begin_date_sal = STR_TO_DATE('$beginDate','%d/%m/%Y'), 
         end_date_sal = STR_TO_DATE('$endDate','%d/%m/%Y'),  serial_cit = $serialCit, status_sal = '$status', days_sal = $days  WHERE serial_sal = $serialSal;";

        $result = $db->Execute($sql);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @Name: getSaleBenefitsWS
     * @Description: Returns an array with the benefits in a specific sale. Implement this method, because WebRatio dont admit - or _  in names
     * @Params: none
     * @Returns: Two-dimensional array or False
     * */
    function getSaleBenefitsWS($serial_lang)
    {
        $sql = "SELECT bbl.description_bbl as descriptionBbl, 
						FORMAT(bp.price_bxp * change_fee_sal, 2) AS 'priceBxp', 
						FORMAT(bp.restriction_price_bxp * change_fee_sal, 2) AS 'restrictionPriceBxp', 
						rbl.description_rbl as descriptionRbl,
						IF(c.serial_con = 4, FORMAT(bp.price_bxp * change_fee_sal, 2), cbl.description_cbl) as aliasCon,
                        b.serial_bcat as serialBcat,
						bbl.serial_lang as serialLang
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
			JOIN benefits_product bp ON bp.serial_pro = pxc.serial_pro AND bp.status_bxp = 'ACTIVE'
			JOIN conditions c ON c.serial_con = bp.serial_con
			JOIN conditions_by_language cbl ON cbl.serial_con = c.serial_con AND cbl.serial_lang = " . $serial_lang . "
			JOIN product_by_language pbl ON pxc.serial_pro = pbl.serial_pro AND pbl.serial_lang = " . $serial_lang . "
			LEFT JOIN restriction_type rt ON rt.serial_rst = bp.serial_rst AND rt.status_rst = 'ACTIVE'
			LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = pbl.serial_lang
			JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
			JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang = " . $serial_lang . "
			WHERE s.serial_sal='" . $this->serial_sal . "'
			ORDER BY b.weight_ben";

        //Debug::print_r($sql);
        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;
            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
            return $arr;
        }
        return false;
    }

    /**
     * @Name: getSaleServicesWS
     * @Description: Returns an array with the services in a specific sale. Implement this method because the WebRatio dont admit - or _ in names
     * @Params: none
     * @Returns: Two-dimensional array or False
     * */
    function getSaleServicesWS($serial_lang, $active_service_only = FALSE)
    {
        $sql = "SELECT s.serial_sal as serialSal, sbd.serial_sbd as serialSbd, sbc.serial_sbc as serialSbc, sbc.status_sbc as statusSbc, sbc.price_sbc as priceSbc,
            sbc.cost_sbc as costSbc, sbc.coverage_sbc as coverageSbc, sbl.name_sbl as nameSbl, ser.fee_type_ser as feeTypeSer
				FROM sales s
						JOIN services_by_country_by_sale sbcbs ON s.serial_sal = sbcbs.serial_sal
						JOIN services_by_dealer sbd ON sbcbs.serial_sbd = sbd.serial_sbd
						JOIN services_by_country sbc ON sbd.serial_sbc = sbc.serial_sbc
						JOIN services ser ON sbc.serial_ser = ser.serial_ser
						JOIN services_by_language sbl ON ser.serial_ser = sbl.serial_ser
				WHERE s.serial_sal = '" . $this->serial_sal . "' AND sbl.serial_lang=" . $serial_lang . "";

        if ($active_service_only) {
            $sql .= " AND ser.status_ser = 'ACTIVE'";
        }

        $result = $this->db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;
            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
            return $arr;
        } else {
            return false;
        }
    }

    public static function isMySale($db, $cardNumer, $serialMbc, $serialDea = NULL)
    {
        if ($serialMbc == 1) {
            $managerRestriction = '';
        } else {
            $managerRestriction = " AND b.serial_mbc = $serialMbc";
        }

        if ($serialDea) {
            $branch = new Dealer($db, $serialDea);
            $branch->getData();

            $subManager = new Dealer($db, $branch->getDea_serial_dea());
            $subManager->getData();

            if ($subManager->getManager_rights_dea() == 'NO') {
                $dealerRestriction = " AND b.serial_dea = $serialDea";
            }
        }

        $sql = "SELECT serial_sal
				FROM sales s
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					AND s.card_number_sal = $cardNumer
				JOIN dealer b On b.serial_dea = cnt.serial_dea $managerRestriction
					$dealerRestriction";

//        Debug::print_r($sql);die();
        $result = $db->getOne($sql);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function getAllStandBySalesToExpire($db, $serial_lang, $passed_days = 120, $max_days = 180)
    {
    $sql = "SELECT s.serial_sal, (sl.serial_slg), DATE_FORMAT(sl.date_slg, '%d/%m/%Y') AS 'standByDate', 
					DATEDIFF(current_timestamp(), sl.date_slg) AS 'daysPassed', DATE_ADD(sl.date_slg, INTERVAL $max_days DAY) AS 'dueDate',
					IFNULL(s.card_number_sal, 'N/A') AS 'cardNumber', CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'customer',
					b.email1_dea, pbl.name_pbl, s.serial_cus, cus.email_cus, cus.phone1_cus, cus.cellphone_cus
				FROM sales s
				JOIN sales_log sl ON sl.serial_sal = s.serial_sal 
					AND sl.status_slg = 'AUTHORIZED'
					AND sl.type_slg = 'STAND_BY'
					AND s.status_sal = 'STANDBY'
				JOIN counter cnt on cnt.serial_cnt = s.serial_cnt
				JOIN dealer b on b.serial_dea = cnt.serial_dea

				JOIN customer cus ON cus.serial_cus = s.serial_cus
				JOIN product_by_dealer pbd On pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc On pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = $serial_lang
				WHERE DATEDIFF(current_timestamp(), sl.date_slg) >= $passed_days";

    $result = $db->getAll($sql);

    if ($result) {
        return $result;
    } else {
        return FALSE;
    }
}
/**
     * @Name: getSalesByInvoiceTandi
     * @Description: Returns an array of sales availables for invoice.
     * @Params: serial invoice
     * @Returns: Sales array
     * */
    function getSalesByInvoiceTandi($db, $serial_inv)
    {
        $sql = "SELECT s.serial_sal,s.card_number_sal,cus.first_name_cus as legal_entity_name,
					   CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,
					   s.total_sal,s.status_sal, s.total_cost_sal, s.serial_cnt,
					   inv.discount_prcg_inv, inv.other_dscnt_inv, inv.applied_taxes_inv, id_erp_sal, inv.serial_inv
				FROM sales s
				JOIN counter c ON s.serial_cnt=c.serial_cnt
				JOIN dealer d ON d.serial_dea=c.serial_dea
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				JOIN invoice inv ON s.serial_inv=inv.serial_inv
				WHERE s.serial_inv=" . $serial_inv . "
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $list = array();
            $cont = 0;

            do {
                $list[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }
//        Debug::print_r($list);die();
        return $list;
    }

    /**
     * @Name: releaseSales
     * @Description: Releases Sales serial_inv and id_erp
     * @Params: serial invoice
     * @Returns: Sales array
     * */
    public static function releaseSale($db, $serial_sal)
    {
        $sql = "UPDATE sales
			   SET id_erp_sal = NULL,
                           serial_inv = NULL
			   WHERE serial_sal = $serial_sal";

        $result = $db->Execute($sql);

        if (!$result)
            ErrorLog::log($db, 'ERP PROCESS FAILED - UPDATE ERPID SALES', $sql);
    }


    /**
     * @name: getSalesCustomerExtras
     * @Description: Get SalesCustomerExtras
     * @params document customer , card number sal
     * @return true or false
     * @date: 06/08/2018
     */
    public static function getSalesCustomerExtras($db, $documentCus = null, $cardNumberSal = null)
    {
        $sql = "SELECT *
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus 
                AND cus.status_cus='ACTIVE' 
                AND sal.status_sal IN ('STANDBY', 'ACTIVE','REGISTERED')               
				LEFT JOIN extras ext ON ext.serial_sal = sal.serial_sal
                LEFT JOIN customer cusext ON cusext.serial_cus = ext.serial_cus
                WHERE cus.document_cus = $documentCus AND sal.card_number_sal =$cardNumberSal
                ";

        $result = $db->getAll($sql);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @name: getDataUserPaymentPtoP
     * @Description: Get information user for payment placetopay
     * @params document customer , card number sal
     * @return true or false
     * @date: 06/08/2018
     */
    public static function getDataUserPaymentPtoP($db, $serial_inv = null)
    {
        $sql = "SELECT 
                IF(inv.serial_cus IS NULL,dea.name_dea,cus.first_name_cus) AS 'name',
                IF(inv.serial_cus IS NULL,dea.name_dea,cus.last_name_cus) AS 'surname',
                IFNULL(cus.document_cus, dea.id_dea) AS 'document',
                IF(inv.serial_cus IS NULL,dea.email1_dea,cus.email_cus) AS 'email', 
                IF(inv.serial_cus IS NULL,dea.address_dea,cus.address_cus) AS 'address',
                IF(inv.serial_cus IS NULL,cit2.name_cit,cit.name_cit) AS 'city',
                IF(inv.serial_cus IS NULL,cou2.code_cou,cou.code_cou) AS 'country',
                inv.serial_inv  AS 'reference',
                inv.applied_taxes_inv AS 'taxes',
                inv.total_inv AS 'total',
                pr.serial_pro AS 'sku',
                pbl.name_pbl AS 'name_product',
                sal.serial_sal AS 'serial_sal',
                inv.erp_id AS 'erp_id',
				 inv.number_inv AS 'number_inv'
                FROM invoice inv
                LEFT JOIN dealer dea ON dea.serial_dea = inv.serial_dea
                LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
                LEFT JOIN city cit ON cit.serial_cit = cus.serial_cit
                LEFT JOIN sector sec ON sec.serial_sec = dea.serial_sec
                LEFT JOIN city cit2 ON cit2.serial_cit = sec.serial_cit
                LEFT JOIN country cou ON cou.serial_cou = cit.serial_cou
                LEFT JOIN country cou2 ON cou2.serial_cou = cit2.serial_cou
                JOIN sales sal ON sal.serial_inv = inv.serial_inv
                JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
                JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
                JOIN product pr ON pr.serial_pro=pxc.serial_pro
                JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang=2
                where inv.serial_inv in ($serial_inv)";
        $result = $db->Execute($sql);
       // Debug::print_r($result);die();
         $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    /**
     * @name: insertRecordPayment
     * @Description: Insert record paytment placetopay
     * @params document customer , card number sal
     * @return true or false
     * @date: 04/10/2018
     */

     public static function insertRecordPayment($db,
                                               $serial_inv,
                                               $name,
                                               $surname,
                                               $document,
                                               $email,
                                               $address,
                                               $reference,
                                               $taxes,
                                               $total,
                                                $shipping_status,
                                                $erp_id,$serial_user){
        $sql="INSERT INTO `record_payment` (`serial_inv`,
              `name`,
              `surname`,
              `document`,
              `email`,
              `address`,
              `reference`,
              `taxes`,
              `total`,
              `shipping_status`,
              `erp_id`,
			  `serial_user`
              ) VALUES (
               '" . $serial_inv . "',
               '" . $name . "',
               '" . $surname . "',
               '" . $document . "',
               '" . $email . "',
               '" . $address . "',
               '" . $reference . "',
               '" . $taxes . "',
               '" . $total . "',
               '" . $shipping_status . "',
               '" . $erp_id . "',
			   '" . $serial_user . "'
               )";
 
        $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
           // ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }
	
					 public static function getRecordPaymentDealer($db,$serial_user){
        $sql="SELECT r.serial_rp AS 'Id',
                r.serial_inv AS 'REFERENCIA',
                r.name AS 'NOMBRE',
                r.surname AS 'APELLIDO',
                r.document AS 'CEDULA',
                r.email AS 'EMAIL',
                r.address AS 'DIRECCION',
                r.total AS 'TOTAL',
                r.serial_sal AS 'ESTADO_DE_PAGO',
                r.shipping_status AS 'ESTADO_TANDI',
                r.erp_id AS 'ERP_ID'
                from record_payment r
                where serial_user=$serial_user
                order by Id desc";
        $result = $db->Execute($sql);
        // Debug::print_r($result);die();
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;

    }
	
	public static function getRecordPaymentDealerId($db,$serial_inv){
        $sql="SELECT r.serial_rp AS 'Id',
                r.serial_inv AS 'REFERENCIA',
                r.name AS 'NOMBRE',
                r.surname AS 'APELLIDO',
                r.document AS 'CEDULA',
                r.email AS 'EMAIL',
                r.address AS 'DIRECCION',
                r.total AS 'TOTAL',
                r.serial_sal AS 'ESTADO_DE_PAGO',
                r.shipping_status AS 'ESTADO_TANDI',
                r.erp_id AS 'ERP_ID'
                from record_payment r
                where serial_inv=$serial_inv
                order by Id desc";
        $result = $db->Execute($sql);
        // Debug::print_r($result);die();
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;

    }

    public static function getRecordPayment($db){
         $sql="SELECT r.serial_rp AS 'Id',
                r.serial_inv AS 'REFERENCIA',
                r.name AS 'NOMBRE',
                r.surname AS 'APELLIDO',
                r.document AS 'CEDULA',
                r.email AS 'EMAIL',
                r.address AS 'DIRECCION',
                r.total AS 'TOTAL',
                r.serial_sal AS 'ESTADO_DE_PAGO',
                r.shipping_status AS 'ESTADO_TANDI',
                r.erp_id AS 'ERP_ID'
                from record_payment r
                order by Id desc";
        $result = $db->Execute($sql);
        // Debug::print_r($result);die();
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;

                }
				
    public static function getRecordPaymentPending($db,$serial_user){
        $sql="SELECT r.serial_rp AS 'Id',
                r.serial_inv AS 'REFERENCIA',
                r.name AS 'NOMBRE',
                r.surname AS 'APELLIDO',
                r.document AS 'CEDULA',
                r.email AS 'EMAIL',
                r.address AS 'DIRECCION',
                r.total AS 'TOTAL',
                r.serial_sal AS 'ESTADO_DE_PAGO',
                r.shipping_status AS 'ESTADO_TANDI',
                r.erp_id AS 'ERP_ID',
                r.status_tandi AS 'ESTADO_DE_PAGO_TANDI',
                r.shipping_status
                from record_payment r
                where serial_user=$serial_user AND serial_sal='APPROVED' AND shipping_status='PENDING' OR status_tandi=NULL OR status_tandi='ERROR'
                order by Id desc";
        $result = $db->Execute($sql);
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;

    }

    public static function getRegisterUpdate($db,$serial_inv){
        $sql="SELECT rp.serial_rp AS 'serial_rp',
                     rp.serial_inv AS 'serial_inv' 
                     FROM record_payment rp 
                     WHERE serial_inv= '" . $serial_inv . "'";

        $result = $db->Execute($sql);
        // Debug::print_r($result);die();
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    public static function updateRecordPayment($db,$status_payment,$serial_rp){
        $sql="UPDATE `record_payment`
              SET `serial_sal`= '" . $status_payment . "'
              WHERE `serial_rp`='" . $serial_rp . "'";

        $result = $db->Execute($sql);
        if ($result === false
        )
            return false;

        if ($result == true)
            return true;
        else
            return false;

    }

   public static function updateRecordPaymentTandi($db,$status_payment,$serial_rp,$shipping_status,$json_Log_send,$json_Log_receive,$status_tandi,$json_Log_update_bas,$responseUpdateWSBAS){
        $sql="UPDATE `record_payment`
              SET `serial_sal`= '" . $status_payment . "',`shipping_status`= '" . $shipping_status . "',`json_sent`= '" . $json_Log_send . "',`json_response`= '" . $json_Log_receive . "',`status_tandi`= '" . $status_tandi . "',`json_Log_update_bas`= '" . $json_Log_update_bas . "',`responseUpdateWSBAS`= '" . $responseUpdateWSBAS . "'
              WHERE `serial_rp`='" . $serial_rp . "'";

        $result = $db->Execute($sql);
        if ($result === false
        )
            return false;

        if ($result == true)
            return true;
        else
            return false;

    }

    /*     * *********************************************
     * function validateDates
      @Name: validateDates
      @Description: Validates date for Reporte of Ejecutiva
     * ********************************************* */

    function validateDates()
    {

//        $sql = " SELECT serial_sal from sales WHERE card_number_sal = $this->card_number_sal AND serial_sal in (384098,384107,384108,384705,388956,388958,389684,390280,390378,390459,393683,394991,395360,396926,397011,397319,397322,397325,397467,397566,399220,399221,399223,400521,401295,401701,401821,402122,403199,404235,404599,405005,405913,405993,406651,407955,408200,408542,408739,408761,409341,409342,409897,410493,410653,410654,412296,412297,412846,413151,413192,413410,413516,414392,415129,395885,400339,400494,386211,387078,387497,390277,390383,390403,390931,396786,397444,397445,404087,410315,410516,411636,402384,384257,384258,392159,411607,400747,384228,392260,409911,412980,387767,387775,403590,385282,388525,389923,390407,395816,395899,397748,400565,408201,408202,388896,390819,405650,414083,408590,389894,411943,411944,413134,413157,393957,393958,393959,407009,408087,385235,387659,389037,389383,389391,389738,389739,390729,392013,392015,398432,400617,401564,403162,404668,404798,404804,405780,405985,407550,407809,407810,407811,408110,408199,408212,408477,409641,413160,413164,413369,414039,384854,384859,384865,384871,386042,388891,389266,389267,391894,393899,397464,408805,408807,411705,411706,404253,387144,411230,413018,387749,406321,410293,410295,404990,413411,413412,402468,393923,398609,403032,407874,408910,408240,408246,384394,385099,385101,386500,386973,386974,386987,387029,387139,387623,388064,389936,390693,390726,391252,391357,391875,392697,392808,393508,394406,395637,395638,396356,396361,397198,397212,397967,398995,401198,403353,404045,404787,405278,405356,405447,405531,407091,408118,408120,408135,408137,408159,408756,408773,409232,409349,409375,409712,410522,410545,411573,411780,412079,412080,412141,412639,413297,413555,413863,414103,414793,414865,414867,414901,414989,414990,414992,414994,415111,385658,385662,393442,395031,396988,398686,401766,401767,403461,391435,392141,406491,406492,392874,391394,396660,406067,384465,384471,384830,385330,385335,385553,390152,391486,392282,394890,394891,396376,396528,396529,398481,404707,404712,405808,406496,407556,407626,407627,408649,409373,409465,411205,413025,413129,414040,414041,414481,384970,384980,387638,387963,393927,405168,411226,384480,384797,385372,385769,385770,388143,389005,389211,391255,391999,395283,396555,403320,405209,407251,408128,409024,410135,410725,411539,411694,412556,412560,412644,412645,412646,412839,412840,413354,397130,411829,390057,407348,409389,413927,385259,389008,389009,389011,390030,390054,390743,391004,392726,395996,396545,405229,405233,405293,409391,409393,384150,404026,387923,387934,387941,388250,388252,395838,405542,405544,407949,409519,409559,412630,412992,383996,387028,387387,387552,388203,404892,405301,405313,406495,407478,408528,410805,413066,413067,415036,413255,413407,405511,397759,395895,395898,395900,395901,390332,386877,400394,413231,385590,388627,388635,397259,391362,393803,396894,396898,410589,410590,391581,384007,384008,384470,391860,389469,394775,402902,414724,406514,389183,389185,400817,401791,401793,401794,401795,401797,401798,403145,406842,406845,407959,408624,408721,409355,410663,410666,411856,396947,407435,403551,403552,410082,410083,412388,412390,393500,393501,404249,404308,389591,405086,389617,392094,393826,413624,413913,391173,394879,405255,384044,388759,390447,390450,390451,393537,393971,394475,400852,400890,403003,406135,406137,407307,407381,407740,407741,413123,413889,412860,385711,404610,404801,404802,406488,385934,391677,393867,393868,396686,400938,400940,401107,406003,412439,413940,413941,413520,405050,411488,389451,396254,396256,396257,396258,396259,396260,390713,388714,388716,389681,409464,410156,387446,413847,393647,385088,405445,405451,387366,406717,397728,401227,384886,384945,386351,386352,386968,386969,386985,387604,391552,391553,391770,393226,393668,393670,393998,394020,394360,394911,397238,400293,400295,400850,403026,404861,408527,408529,409938,410023,410024,410387,410388,411404,411513,412387,412389,412392,413456,413517,413689,399009,401823,402357,392526,394482,405288,404846,412008,390755,392759,392761,390107,390109,384627,414575,398999,405786,405706,409172,390088,397155,398390,398393,402592,403427,403429,410517,391254,403314,400651,406290,397584,398278,402242,398411,406664,399012,399791,412436,387380,389363,390362,392872,396008,406970,407654,407735,411294,411632,409855,409858,397312,397314,397315,399560,401290,401908,404402,390877,390879,406066,402102,402714,405655,405679,407370,410145,412398,413064,414002,414208,414892,411803,388842,388843,412660,412661,389468,400439,400440,386548,396450,403347,387838,403206,403616,392325,392344,394301,397638,400761,411307,412825,399813,396826,398018,402796,406612,384020,385245,400120,400121,405448,399628,400315,400316,413822,395888,400182,385936,405208,408964,408966,385867,394147,410565,410876,395687,395689,409646,410109,410228,410231,410501,402387,395543,389788,389789,393593,413726,414019,386166,386168,388850,392504,392505,392869,404711,404775,406266,410535,412219,392083,384953,394878,395329,415139,415140,406434,387722,384836,393977,397461,397462,397469,397565,410358,392820,406489,409101,395029,400416,407372,396249,412484,412487,413303,413304,399890,414195,414196,408594,408595,408596,413820,414585,414425)";
//        $sql = " SELECT serial_sal from sales WHERE card_number_sal = $this->card_number_sal AND serial_sal in (377052,377329,377330,377368,378240,378241,379056,379487,379721,379722,379958,380134,380791,381559,381560,381745,383398,383399,383897,384098,384107,384108,384705,388956,388958,389684,390280,390378,390459,393683,394991,395360,396926,397011,397319,397322,397325,397467,397566,399220,399221,399223,400521,401295,401701,401821,402122,403199,404235,404599,405005,405913,405993,406651,407955,408200,408542,408739,408761,409341,409342,409897,410493,410653,410654,395885,400339,400494,378412,378414,378416,378417,378431,380613,380618,381326,381472,381473,381474,381476,383226,383227,383716,386211,387078,387497,390277,390383,390403,390931,396786,397444,397445,404087,410315,410516,402384,384257,384258,392159,400747,383924,384228,392260,380926,381184,378922,382532,387767,387775,403590,385282,388525,389923,390407,395816,395899,397748,400565,408201,408202,388896,390819,405650,378917,408590,389894,380900,378980,378983,393957,393958,393959,408087,376955,377009,379283,379852,380305,385235,387659,389037,389383,389391,389738,390729,392013,392015,398432,400617,401564,403162,404668,404798,404804,405780,405985,407550,407809,407810,407811,408110,408199,408212,408477,409641,379393,380163,382466,382468,384854,384859,384865,384871,386042,388891,389266,389267,391894,393899,397464,408805,408807,404253,378897,378900,387144,387749,406321,410293,410295,379527,379531,382196,382250,382811,383583,404990,402468,393923,398609,403032,407874,408910,408240,408246,376941,377049,377759,377760,379096,379682,379754,379997,380182,380246,380269,380640,381578,381837,381838,381947,383012,383013,383014,383262,383320,383404,383653,383794,384394,384433,384435,385099,385101,386500,386973,386974,386987,387029,387139,387623,388064,389936,390693,390726,391252,391357,391689,391875,392697,392808,393508,394406,395637,395638,396356,396361,397198,397212,397967,398995,401198,403353,404045,404787,405278,405356,405447,405531,407091,408118,408120,408135,408137,408159,408756,408773,409232,409349,409375,409712,410522,410545,385658,385662,393442,395031,396988,398686,401766,401767,379509,382850,382851,382852,382853,403461,391435,392141,406491,406492,381367,392874,391394,396660,406067,377485,377487,378225,380258,380266,381075,381234,382597,384465,384471,384830,385330,385335,385553,390152,391486,392282,394890,394891,396376,396528,396529,398481,404707,404712,405808,406496,407556,407626,407627,408649,409373,409465,381360,383413,384970,384980,387638,387963,393927,405168,377757,377758,377768,379067,379133,380639,380826,380852,383640,384480,384797,385372,385769,385770,388143,389005,389211,391255,391999,395283,396555,403320,405209,407251,408128,409024,410135,410725,397130,376964,390057,407348,409389,377047,380264,381699,385259,389008,389009,389011,390030,390054,390743,391004,392726,395996,396545,405229,405293,384150,404026,387923,387934,387941,388250,388252,395838,405542,405544,407949,409519,409559,383996,387028,387387,387552,388203,404892,405301,405313,406495,407478,405511,397759,395895,395898,395900,395901,390332,386877,400394,385590,388627,388635,397259,391362,379305,393803,396894,396898,410589,410590,379125,391581,379806,384007,384008,384470,391860,389469,394775,402902,406514,389183,389185,400817,401791,401793,401794,401795,401797,401798,403145,406842,406845,407959,408624,408721,409355,410663,410666,382460,378178,383275,396947,407435,403551,403552,410082,410083,393500,393501,404249,404308,389591,405086,389617,392094,393826,380153,380677,380910,391173,394879,405255,378853,379090,380064,380642,380643,381705,382027,382028,383027,383322,383325,383573,384044,388759,390447,390450,390451,393537,393971,394475,400852,400890,403003,406135,406137,407307,407381,407740,407741,383594,385711,404610,404801,404802,406488,382879,385934,391677,393867,393868,396686,400938,400940,401107,406003,378246,379932,378144,405050,389451,396254,396256,396257,396258,396259,396260,381841,390713,376870,382866,383883,388714,388716,389681,409464,410156,387446,393647,385088,405445,405451,387366,406717,397728,401227,384886,377665,378109,379640,382041,382086,382447,382449,384945,386351,386352,386968,386969,386985,387604,391552,391553,391770,393226,393668,393670,393998,394020,394360,394911,397238,400293,400295,400850,403026,404861,408527,408529,409938,410023,410024,410387,410388,399009,401823,402357,378599,379071,392526,394482,405288,377474,404846,379227,377499,390755,392759,392761,390107,390109,384627,398999,405786,405706,409172,390088,397155,398390,398393,402592,403427,403429,410517,391254,403314,400651,406290,378990,397584,398278,402242,398411,406664,399012,399791,387380,389363,390362,392872,396008,406970,407654,407735,409855,409858,397312,397314,397315,399560,401290,401908,404402,390877,390879,406066,383866,402102,402714,377864,377865,382407,405655,405679,407370,410145,380281,378718,378719,388842,388843,380863,389468,400439,400440,380766,381088,386548,396450,403347,387838,403206,403616,392325,392344,394301,397638,400761,399813,396826,398018,402796,406612,378743,378768,384020,385245,400120,400121,405448,378995,399628,400315,400316,395888,400182,383706,383856,385936,405208,408964,408966,385867,394147,378691,379863,378863,395687,395689,409646,410501,402387,379023,379025,395543,381451,389788,389789,393593,386166,386168,388850,383805,383806,383807,383808,383809,392504,392505,392869,404711,404775,406266,410535,392083,384953,394878,395329,387722,384836,393977,397461,397462,397469,397565,410358,392820,406489,409101,395029,400416,396249,399890,408594,408595,408596)";
        $sql = " SELECT serial_sal from sales WHERE card_number_sal = $this->card_number_sal AND serial_sal in (406673,404935,399038,399595,402379,403260,409681,412853,387818,395350,403126,411326,392627,398793,406254,411847,369932,373131,399534,401414,410610,385239,390566,392595,407786,396729,399390,404575,394135,385663,406870,414010,377385,377393,410838,413804,377384,385409,412972,414442,403454,403561,399276,402641,403669,406557,409036,411137,412794,414545,415150,402643,403668,406555,409035,411136,412793,414544,415149,402864,403029,403272,395539,402537,413046,413206,398186,386449,402731,407788,409336,414168,415017,415018,405295,409448,414009,415077,402804,346029,385410,386726,413921,384695,413705,398949,400239,405025,405029,405072,400851,414283,406671,409534,384911,393487,413071,385629,410002,408627,367880,367881,399278,413421,412841,382265,397702,404779,404796,403902,408372,411177,411183,411184,411185,411186,413380,413386,414424,414428,414478,414479,414482,414486,414491,414496,414500,414505,414512,414513,414514,414523,414526,414527,414530,414532,414536,414537,414541,393682,413462,402407,414797,413007,413008,413009,413078,413081,415280,413910,412443,346605,399261,413130,398207,406733,399419,386843,386845,388457,410077,411703,412541,412542,399533,399658,399799,401064,401159,401257,401750,402513,402642,402688,402775,403202,403208,403224,403381,403466,403578,403609,404444,404747,405078,406293,406703,406968,407055,407318,407574,408744,409538,410532,410679,411406,411702,413045,413625,414843,399499,404638,404641,389802,402372,405416,382984,382987,400431,384641,402914,401724,402680,386356,394469,396304,398975,399897,401160,401161,402400,402649,404414,405176,409983,410431,412768,377672,384114,384115,384972,390135,390384,394398,394526,394528,394531,396191,396398,396464,396510,396934,397027,398016,398041,398116,398147,398345,398489,398997,399235,399317,399535,400541,400901,401722,401723,401866,402043,402415,402466,402640,402700,402797,403080,403340,403577,404872,405198,405873,405893,405991,406022,406274,406538,409764,410351,410799,410908,411105,412179,412305,412339,413275,413351,413595,413886,414353,414881,414937,399352,399376,394141,396229,396775,396777,398796,396247,401638,407653,409970,405020,404820,404822,402028,398455,400769,404176,393579,401583,402613,403671,403765,413387,412988,412990,398148,398212,398425,400939,403944,404546,404579,404868,407286,404611,406679,407155,410381,410477,413584,413588,414252,415058,401949,397614,402546,409767,415230,400158,395224,406923,406930,396887,413320,402093,403425,413191,392284,401644,390552,400283,405015,395723,397401,390017,384634,393787,399989,399992,375482,375485,380678,382325,382893,385595,386156,386261,387071,387209,387433,388033,389829,390333,390361,391494,391669,391781,392633,392866,393620,394043,398077,401712,400946,385243,393230,400028,403952,394177,399416,400265,400267,400268,400269,401415,402345,402346,408273,410003,412854,413346,413352,413552,414818,399464,399598,399943,402526,382885,391122,395054,395523,397065,398166,401283,402408,402732,403025,404616,407342,389386,387000,387001,387003,387004,397763,398825,401781,414152,402038,407789,402600,402608,408292,395645,398433,411149,411728,411742,395526,399403,405339,413091,400574,403222,404510,389683,412108,403481,403815,403820,394987,415001,386422,404656,413841,400286,381378,397318,399138,405746,384916,398317,403908,405600,411627,394310,401889,404145,407301,407302,403681,393495,395691,398703,398960,398966,401584,402622,403806,404002,407607,409649,410169,410187,410631,396373,385963,385965,386518,391854,393493,396339,397977,398176,399353,399354,399355,402328,409630,410782,412863,413170,413242,414124,414139,409997,410619,404322,406919,409685,412098,414560,380313,404709,380086,399700,400175,402897,398608,400270,399272,401643,401341,397335,398473,399018,401905,414859,398055,388194,400229,401988,407551,413014,402695,403263,396621,398379,402531,404682,405423,412511,410373,399002,394851,389313,400711,405224,412985,409697,401959,404680,404684,414187,406666,396105,380715,410826,405439,379381,384189,401091,402996,401819,403901,404456,404695,397186,397262,394981,398068,398783,399323,400381,410288,412983,414629,414678,397826,411083,411377,411487,414365,414423,410899,397555,405865,413982,413603,406403,398082,404013,402077,408814,411249,412699,413319,383498,400456,399602,401008,401009,404883,401102,402715,406054,406778,411626,414047,414411,414472,409974,388208,393270,403485,403572,403576,403562,413205,413617,413399,398935,401780,391046,390028,399269,399270,402348,406058,412652,412978,379757,396779,396780,402033,407322,378972,415168,415169,396589,380202,392953,393600,393960,398114,401366,401607,402116,405123,405799,406912,407628,410940,412855,413936,414795,415201,399927,379834,391256,391257,381059,400774,404563,394957,408769,398374,401037,404834,408180,410556,410917,399146,403897,404960,413333,398630,404262,397402,397193,384120,397446,394935,395525,399820,400125,402018,403380,404882,410579,412971,394496,396837,397606,409770,400126,394964,394971,400612,399428,413413,402076,402078,408920,414726,395426,410098,388794,405308,405404,410139,411882,392681,412338,413857,394687,400762,412278,405231,405232,407448,410915,411405,413295,400122,402819,394498,404372,397181,402297,412472,397778,412986,398184,405729,403423,413832,403323,406851,414143,413561,410755,411400,408927,397431,405181,406615,407272,397624,397625,397626,391783,410394,411908,415290,383699,385030,386157,386454,387093,387789,387790,387860,389083,389774,389977,389983,392140,395199,395486,395587,396340,396454,396727,396927,397388,398378,399325,399601,399639,399640,399778,399912,399949,399955,401072,401151,401234,401573,401721,401816,401887,401892,402032,402082,402083,402421,402480,402620,402740,402741,402827,402940,402951,403072,403078,403093,403556,403619,403704,403711,403716,403782,403792,404059,404140,404228,404409,404417,404422,404438,404542,404648,404664,404673,404703,404896,405017,405174,405205,405247,405318,405333,405471,405493,405508,405572,406047,406160,406649,406715,406882,406889,406904,407045,407321,407885,407945,407965,408234,408714,408722,408969,409202,410198,410743,410866,410942,411707,411828,412041,412503,412742,412744,413013,413213,413547,413609,413613,413714,413865,413972,413979,414000,414132,414133,414140,414174,414587,414882,415072,409860,389652,402013,403489,394428,394431,405938,409640,413147,400980,411368,402317,408114,412499,404626,406247,396969,406120,404620,404055,404420,404460,404596,405230,407366,407630,410776,411897,412670,403769,410027,404766,411281,393477,402484,398613,404963,387669,394050,403344,404005,404101,406285,407020,409753,410304,410879,398722,401331,402728,383840,394644,403186,403584,406891,407840,411690,412117,388553,393974,394677,408772,408113,403818,396732,402219,379854,395398,401982,402112,406031,409608,412452,415022,397363,383306,385434,387080,387982,388146,389466,390400,391408,392614,392639,395043,395544,396756,400411,400412,400572,401214,401318,401346,402946,403008,403228,404019,404069,404478,404688,404790,404941,405007,405170,406535,406643,407649,409535,409544,409658,410802,410950,411313,412140,412650,412772,413973,413994,414073,414079,414262,414292,414518,405163,399769,395032,392933,401891,396730,403675,403676,387496,387641,387643,393484,394538,394956,394974,397338,401777,405316,405719,406526,409013,414589,415232,407187,413155,400727,381118,385777,387009,392895,393282,399597,403250,411665,406095,407987,412095,390801,402377,402834,407568,409384,403030,403110,403595,398981,402174,408443,392967,400618,400794,402448,379515,407984,414831,384479,401286,410804,391304,400786,402685,404553,409959,414782,414814,414976,386618,403801,405911,407261,409527,400517,404068,401995,400549,405305,402646,395407,407208,407939,392826,384619,399476,399951,402520,403172,403351,403496,403565,404269,404972,405496,406212,408823,412832,414371,404312,396015,396784,401122,401568,404198,404877,412695,394070,406064,409298,414293,414551,403432,403832,388102,388106,392271,394430,395207,401628,401637,410368,411232,390043,392717,411428,394988,399713,405147,405393,406283,407488,410196,413186,399249,389400,394153,407379,390489,413751,392425,396479,400962,402906,405111,409776,389775,402505,404254,384252,403662,407262,407263,407268,411165,410660,379956,405703,393478,387010,406537,403057,404356,413345,413405,393190,396693,409887,414546,401669,405736,400756,402037,402269,405220,414977,403617,404250,406759,390366,400405,393686,402465,401148,405891,385466,401586,405563,410900,398420,392411,405506,407407,396449,399121,401961,405087,407225,407765,409797,405750,404278,400384,403894,403521,404426,405055,405218,414613,403405,403866,414471,410762,413525,337229,337279,399793,399798,413199,414114,415015,415116,415106,413110,415097,415098,415099,415100,415103,415105,413828,413541,413543,412634,411783,411813,413952,414896,414908,414982,414998,415114,413619,413620,413031,413032,414900,415218,414708,415047,415049,415096,413876,415287,414907,406480,406481,406482,415234,411732,413083,414871,410906,413963,415020,415046,415089,407423,407424,412204,410584,414875,414233,414238,411159,411222,412157,413553,414035,414245,415284,415007,402272,409352,409744,412835,413519,413523,413728,413836,413992,414016,414076,414379,414490,414528,414543,414627,414787,414932,413284,415108,413788,406967,413176,413180,411799,411800,414029,414449,407166,411577,411992,412000,412865,412974,413040,413105,413106,413505,413627,413991,414271,414313,414314,414784,415176,415182,415183,415184,415193,415219,415223,415265,415285,410347,409667,409670,411770,408355,413060,414688,411807,411808,415090,414361,414087,409291,412011,415165,413931,411837,414209,407865,407866,414045,412737,411250,414170,411881,412148,414659,415238,415115,414123,414125,414632,414448,413599,414003,412848,414725,413833,414956,413666,414178,414480,413947,414861,415124,413054,413057,413058,414873,415283,412799,412637,412638,413216,410681,412533,412697,413568,413984,414489,414975,413925,414026,411492,413261,413580,412464,414378,411593,411594,411596,413000,413023,413879,414652,414893,415277,414274,415289,414022,413051,415087,413222,414626,414722,415050,410076,412698,415025,413782,411375,413247,415249,413554,413704,414380,412063,412397,413163,412081,413534,411792,414101,414475,414944,413995,415268,412659,415143,415145,415026,415221,414408,414473,414617,411464,413342,414598,415220,414997,413201,413264,413266,414388,414437,414441,415079,414469,414835,413720,413725,415088,415198,415199,414599,414602,414604,414864,413137,415148,413259,413260,413262,410659,410661,410671,414856,414493,414494,415023,412067,414700,409542,415138,388956,388958,389684,390280,390378,390459,393683,394991,395360,396926,397011,397319,397322,397325,397467,397566,399220,399221,399223,400521,401295,401701,401821,402122,403199,404235,404599,405005,405913,405993,406651,407955,408200,408542,408739,408761,409341,409342,409897,410493,410653,410654,412296,412297,412846,413151,413192,413410,413516,414392,415129,395885,400339,400494,386211,387078,387497,390277,390383,390403,390931,396786,397444,397445,404087,410315,410516,411636,402384,392159,411607,400747,392260,409911,412980,387767,387775,403590,385282,388525,389923,390407,395816,395899,397748,400565,408201,408202,388896,390819,405650,414083,408590,389894,411943,411944,413134,413157,393957,393958,393959,407009,408087,385235,387659,389037,389383,389391,389738,389739,390729,392013,392015,398432,400617,401564,403162,404668,404798,404804,405780,405985,407550,407809,407810,407811,408110,408199,408212,408477,409641,413160,413164,413369,414039,384854,384859,384865,384871,386042,388891,389266,389267,391894,393899,397464,408805,408807,411705,411706,404253,387144,411230,413018,387749,406321,410293,410295,404990,413411,413412,402468,393923,398609,403032,407874,408910,408240,408246,385099,385101,386500,386973,386974,386987,387029,387139,387623,388064,389936,390693,390726,391252,391357,391875,392697,392808,393508,394406,395637,395638,396356,396361,397198,397212,397967,398995,401198,403353,404045,404787,405278,405356,405447,405531,407091,408118,408120,408135,408137,408159,408756,408773,409232,409349,409375,409712,410522,410545,411573,411780,412079,412080,412141,412639,413297,413555,413863,414103,414793,414865,414867,414901,414989,414990,414992,414994,415111,385658,385662,393442,395031,396988,398686,401766,401767,403461,391435,392141,406491,406492,392874,391394,396660,406067,384830,385330,385335,385553,390152,391486,392282,394890,394891,396376,396528,396529,398481,404707,404712,405808,406496,407556,407626,407627,408649,409373,409465,411205,413025,413129,414040,414041,414481,384970,384980,387638,387963,393927,405168,411226,384797,385372,385769,385770,388143,389005,389211,391255,391999,395283,396555,403320,405209,407251,408128,409024,410135,410725,411539,411694,412556,412560,412644,412645,412646,412839,412840,413354,397130,411829,390057,407348,409389,413927,385259,389008,389009,389011,390030,390054,390743,391004,392726,395996,396545,405229,405233,405293,409391,409393,404026,387923,387934,387941,388250,388252,395838,405542,405544,407949,409519,409559,412630,412992,387028,387387,387552,388203,404892,405301,405313,406495,407478,408528,410805,413066,413067,415036,413255,413407,405511,397759,395895,395898,395900,395901,390332,386877,400394,413231,385590,388627,388635,397259,391362,393803,396894,396898,410589,410590,391581,391860,389469,394775,402902,414724,406514,389183,389185,400817,401791,401793,401794,401795,401797,401798,403145,406842,406845,407959,408624,408721,409355,410663,410666,411856,396947,407435,403551,403552,410082,410083,412388,412390,393500,393501,404249,404308,389591,405086,389617,392094,393826,413624,413913,391173,394879,405255,388759,390447,390450,390451,393537,393971,394475,400852,400890,403003,406135,406137,407307,407381,407740,407741,413123,413889,412860,385711,404610,404801,404802,406488,385934,391677,393867,393868,396686,400938,400940,401107,406003,412439,413940,413941,413520,405050,411488,389451,396254,396256,396257,396258,396259,396260,390713,388714,388716,389681,409464,410156,387446,413847,393647,385088,405445,405451,387366,406717,397728,401227,384886,384945,386351,386352,386968,386969,386985,387604,391552,391553,391770,393226,393668,393670,393998,394020,394360,394911,397238,400293,400295,400850,403026,404861,408527,408529,409938,410023,410024,410387,410388,411404,411513,412387,412389,412392,413456,413517,413689,399009,401823,402357,392526,394482,405288,404846,412008,390755,392759,392761,390107,390109,414575,398999,405786,405706,409172,390088,397155,398390,398393,402592,403427,403429,410517,391254,403314,400651,406290,397584,398278,402242,398411,406664,399012,399791,412436,387380,389363,390362,392872,396008,406970,407654,407735,411294,411632,409855,409858,397312,397314,397315,399560,401290,401908,404402,390877,390879,406066,402102,402714,405655,405679,407370,410145,412398,413064,414002,414208,414892,411803,388842,388843,412660,412661,389468,400439,400440,386548,396450,403347,387838,403206,403616,392325,392344,394301,397638,400761,411307,412825,399813,396826,398018,402796,406612,385245,400120,400121,405448,399628,400315,400316,413822,395888,400182,385936,405208,408964,408966,385867,394147,410565,410876,395687,395689,409646,410109,410228,410231,410501,402387,395543,389788,389789,393593,413726,414019,386166,386168,388850,392504,392505,392869,404711,404775,406266,410535,412219,392083,384953,394878,395329,415139,415140,406434,387722,384836,393977,397461,397462,397469,397565,410358,392820,406489,409101,395029,400416,407372,396249,412484,412487,413303,413304,399890,414195,414196,408594,408595,408596,413820,414585,414425,405000,368663,397627,413321,414921,414656,414658,414661,413824,411235,413884,410290,410771,413659,415091,415092,414633,413669,413672,413677,413349,413724,414276,385747,415270,409584,414960,410820,415134,415164,413976,414017,413015,390121,390122,391067,394164,395036,403591,383581,400800,400399,397263,398391,386374,385250,397367,402097,394832,397231,383084,397451,401202,387025,406692,396597,402114,397423,401818,403360,385893,396697,395141,393704,395330,405752,403903,409824,409581,411978,413630,413579,414093,414754,414755,415288,413440,413442,407759,413090,413975,413977,414230,414577,414579,414580,413715,413716,415235,407052,414729,415039,415040,415042,415251,415253,414799,414970,411146,409792,412668,414216,414223,414731,414732,414075,411519,411520,411819,411823,411824,413204,414106,414702,414703,409430,409432,412070,412071,412072,413792,413793,412981,413428,413545,413578,414207,414294,414416,414571,414595,414603,414611,415013,415093,415137,410897,413128,413331,413339,414435,411871,413718,413721,413722,413869,414586,414592,413556,409494,413641,414860,413612,413318,415272,415275,409819,409821,409957,411700,413154,413332,413811,413812,413968,413986,413988,413989,413990,414119,414414,414870,414905,414950,414951,415121,415122,415123,415128,415257,415279,412435,411925,408106,410006,412213,413328,413398,413559,413723,413815,414436,414826,414922,414923,414986,415037,415043,414631,414025,414828,412214,412357,410500,411942,412555,413946,413948,414404,414405,415051,415179,415180,412979,413100,413746,414189,414407,414780,414880,415003,415008,415206,413141,414968,414969,414971,414972,415130,414832,410863,413521,413522,414806,414808,412827,414242,413033,411196,413370,414077,411233,407536,411781,413611,414816,414886,414894,414895,408857,412306,413397,413402,414340,414485,414636,414637,414666,414667,414715,413649,413652,413656,414104,412658,408228,411104,413493,411227,411261,411266,411268,411270,415278,414071,407842,413214,412861,412862,415041,414418,414426,414427,414429,414809,414811,414172,414384,413763,413764,413765,413767,413771,413774,413798,413799,413806,415073,412450,409338,413512,415132,413558,413562,414369,414370,414819,415263,415210,410616,410592,410595,412628,413165,413254,413551,414540,414542,413020,413052,413850,413817,413267,413268,414686,412776,414840,413993,413756,413758,413810,415156,413770,414288,413314,413316,408990,413743,413745,414610,412368,412369,412371,412701,412703,413166,414841,414842,414847,411491,413185,415292,414890,414981,408944,410770,413614,414501,414503,414510,414515,414516,414451,409274,414142,414959,415027,415028,413540,413542,414095,414794,411101,412030,414936,414938,410311,412754,414576,414877,414878,414946,412236,409031,414012,412112,411734,413707,413709,414061,414517,414519,413219,413221,412844,413585,414027,414028,414995,414996,409412,413296,414563,414088,412064,414282,414284,414115,409443,414406,413844,414296,413887,413888,414502,414931,414383,412035,412037,414999,413772,411371,414173,413768,413416,413803,411303,414391,415117,414312,414332,412061,412062,413706,413708,414031,414910,414118,413531,413870,415054,415057,407389,407396,407397,409192,409194,409195,409196,409197,413027,413028,413029,413717,414062,415162,413235,413236,413237,413238,413747,378421,384815,385566,387149,390114,390381,391017,391831,391849,392192,392976,397244,398376,398715,399834,399905,400933,403396,403735,403736,405082,405164,408710,409554,410597,410929,412738,412833,413184,414066,414830,409374,410348,413146,387437,390123,406932,387002)";
//        Debug::print_r($sql);die();
        $result = $this->db->getRow($sql);
        if ($result)
            return 'true';
        else
            return 'false';
    }



    public static function insertLogWSnewSale($db,
                                              $idCounter= NULL,
                                              $language= NULL,
                                              $cart_information= NULL,
                                              $card_number=NULL){
        $sql="INSERT INTO `wsnewsale_log` (
              `idCounter`,
              `language`,
              `date_request`,
              `cart_information`,
              `card_number`
              ) VALUES (
               '" . $idCounter . "',
               '" . $language . "',
              NOW(),
              '" . $cart_information . "',
			   '" . $card_number . "')";
        $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

        public static function insertLogQuotation($db,
                                              $serial_dea= NULL,
                                              $serial_usr= NULL,
                                              $serial_cus= NULL,
                                              $document_cus=NULL,
                                              $first_name_cus=NULL,
                                              $last_name_cus=NULL,
                                              $phone_cus=NULL,
                                              $email_cus=NULL) {
        $sql = "INSERT INTO `quotation_log` (`serial_dea`,
                                            `serial_usr`,
                                            `serial_cus`,
                                            `document_cus`,
                                            `first_name_cus`,
                                            `last_name_cus`, 
                                             `phone_cus`, 
                                             `email_cus`, 
                                             `date_quotation`
                                             ) VALUES (
                                             '" . $serial_dea . "',
                                              '" . $serial_usr . "',
                                              '" . $serial_cus . "',
                                             '" . $document_cus . "',
                                              '" . $first_name_cus . "',
                                               '" . $last_name_cus . "',
                                                '" . $phone_cus . "',
                                                 '" . $email_cus . "',
                                                  NOW())";
        $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    public static function getCustomerByDocumentPresale($db, $document_usr){
        $sql = "SELECT * FROM wsuser_ptp 
                        where document_usr = '$document_usr'";

        $result = $db->getOne($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public static function getDataCustomerPTP($db, $serial_usr) {
        $sql = "SELECT * FROM wsuser_ptp  
                        where serial_user_ptp = '$serial_usr'";
//echo $sql; die();
        $result = $db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public static function insertWSUserPTP($db, $document_usr = NULL, $name_usr = NULL, $password = NULL, $email_usr = NULL){
        $sql="INSERT INTO `wsuser_ptp` (
              `document_usr`,
              `name_usr`,
              `password_usr`,
              `email_usr`
              ) VALUES (
               '" . $document_usr . "',
              '" . $name_usr . "',
              '" . $password . "',
               '" . $email_usr . "')";
        $result = $db->Execute($sql);
        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    public static function insertWSsatusSale($db, $body_sale = NULL, $status_ptp = 'PENDING', $total_sal = NULL, $name_usr = NULL, $document_usr = NULL, $serial_ptpusr = NULL, $serial_cup = NULL){
        $sql="INSERT INTO `wswebstatus_sales` (
              `body_sale`,
              `date_sale`,
              `status_ptp`,
              `total_sal`,
              `name_usr`,
              `document_usr`,
              `serial_ptpusr`,
              `serial_cup`
              ) VALUES (
               '" . $body_sale . "',
              NOW(),
              '" . $status_ptp . "',
              '" . $total_sal . "',
              '" . $name_usr . "',
              '" . $document_usr . "',
              '" . $serial_ptpusr . "',
               '" . $serial_cup . "')";
        $result = $db->Execute($sql);
        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    public static function getDataPreSale($db, $reference = NULL) {
        $sql="SELECT body_sale
                     FROM wswebstatus_sales
                     WHERE serial_wss= '" . $reference . "'";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }

    }

     public static function getDataPreSaleSerialCup($db, $reference = NULL) {
        $sql="SELECT serial_cup
                     FROM wswebstatus_sales
                     WHERE serial_wss= '" . $reference . "'";


        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }

    }

    public static function updatePresale($db,$status_ptp,$serial_wss){
        $sql="UPDATE `wswebstatus_sales`
              SET `status_ptp`= '" . $status_ptp . "'
              WHERE `serial_wss`='" . $serial_wss . "'";

        $result = $db->Execute($sql);
        if ($result === false
        )
            return false;

        if ($result == true)
            return true;
        else
            return false;

    }

    public static function getCustomerLogin($db, $document_usr, $password){
        $sql = "SELECT * FROM wsuser_ptp 
                        where document_usr = '$document_usr' and password_usr = '$password'";

        $result = $db->Execute($sql);
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    public static function getCustomerSerialUsr($db, $document_usr){
        $sql = "SELECT * FROM wsuser_ptp 
                        where document_usr = '$document_usr'";

        $result = $db->getOne($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public static function getDataPreSalebyUser($db, $serial_usr = NULL) {
        $sql="SELECT *
                     FROM wswebstatus_sales
                     WHERE serial_ptpusr= '" . $serial_usr . "' order by serial_wss desc";

        $result = $db->Execute($sql);
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;

    }

    public static function getSaleByCardNumber($db, $cardNumber)
    {
        $sql = " SELECT cus.serial_cus, concat(cus.first_name_cus,' ',cus.last_name_cus) as name, cus.document_cus, cus.address_cus, cus.phone1_cus, cus.phone2_cus,                        cus.cellphone_cus , cus.birthdate_cus, cus.email_cus, cus.relative_cus, cus.relative_phone_cus, sal.serial_sal, sal.card_number_sal, sal.in_date_sal,                    sal.begin_date_sal, sal.end_date_sal, sal.status_sal, concat(cusext.first_name_cus,' ',cusext.last_name_cus) as nameExt, 
                   cusext.document_cus as documentExt, ext.relationship_ext 
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus 
                LEFT JOIN extras ext ON ext.serial_sal = sal.serial_sal 
                LEFT JOIN customer cusext ON cusext.serial_cus = ext.serial_cus
                WHERE sal.card_number_sal = $cardNumber ";

        $result = $db->Execute($sql);
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

     public static function insertLogStep($db,
                                              $step= NULL,
                                              $phoneCustomer= NULL,
                                              $emailCustomer= NULL,
                                              $startDate=NULL,
                                              $endDate=NULL,
                                              $today=NULL,
                                              $data=NULL,
                                              $days=NULL,
                                              $destiny=NULL,
                                              $totalKids=NULL,
                                              $totalAdults=NULL,
                                              $totalSeniorAllowed=NULL,
                                              $totalPrice=NULL) {
        $sql = "INSERT INTO `mail_log_step` (`step`,
                                            `phoneCustomer`,
                                            `emailCustomer`,
                                            `startDate`,
                                            `endDate`,
                                            `today`, 
                                             `data`, 
                                             `days`, 
                                             `destiny`, 
                                             `totalKids`, 
                                             `totalAdults`, 
                                             `totalSeniorAllowed`, 
                                             `totalPrice`
                                             ) VALUES (
                                             '" . $step . "',
                                              '" . $phoneCustomer . "',
                                              '" . $emailCustomer . "',
                                             '" . $startDate . "',
                                              '" . $endDate . "',
                                               '" . $today . "',
                                                '" . $data . "',
                                                '" . $days . "',
                                                '" . $destiny . "',
                                                '" . $totalKids . "',
                                                '" . $totalAdults . "',
                                                '" . $totalSeniorAllowed . "',
                                                 '" . $totalPrice . "')";
        $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    //GETTERS
    function getSerial_sal()
    {
        return $this->serial_sal;
    }

    function getSerial_con()
    {
        return $this->serial_con;
    }

    function getSerial_cus()
    {
        return $this->serial_cus;
    }

    function getSal_serial_sal()
    {
        return $this->sal_serial_sal;
    }

    function getSerial_pbd()
    {
        return $this->serial_pbd;
    }

    function getSerial_cur()
    {
        return $this->serial_cur;
    }

    function getSerial_cit()
    {
        return $this->serial_cit;
    }

    function getSerial_cnt()
    {
        return $this->serial_cnt;
    }

    function getSerial_inv()
    {
        return $this->serial_inv;
    }

    function getCardNumber_sal()
    {
        return $this->card_number_sal;
    }

    function getEmissionDate_sal()
    {
        return $this->emission_date_sal;
    }

    function getBeginDate_sal()
    {
        return $this->begin_date_sal;
    }

    function getEndDate_sal()
    {
        return $this->end_date_sal;
    }

    function getInDate_sal()
    {
        return $this->in_date_sal;
    }

    function getDays_sal()
    {
        return $this->days_sal;
    }

    function getFee_sal()
    {
        return $this->fee_sal;
    }

    function getObservations_sal()
    {
        return $this->observations_sal;
    }

    function getCost_sal()
    {
        return $this->cost_sal;
    }

    function getFree_sal()
    {
        return $this->free_sal;
    }

    function getIdErp_sal()
    {
        return $this->id_erp_sal;
    }

    function getStatus_sal()
    {
        return $this->status_sal;
    }

    function getType_sal()
    {
        return $this->type_sal;
    }

    function getTotal_sal()
    {
        return $this->total_sal;
    }

    function getStockType_sal()
    {
        return $this->stock_type_sal;
    }

    function getDirectSale_sal()
    {
        return $this->direct_sale_sal;
    }

    function getTotalCost_sal()
    {
        return $this->total_cost_sal;
    }

    function getChange_fee_sal()
    {
        return $this->change_fee_sal;
    }

    function getAux()
    {
        return $this->aux;
    }

    function getInternationalFeeStatus_sal()
    {
        return $this->international_fee_status;
    }

    function getInternationalFeeUsed_sal()
    {
        return $this->international_fee_used;
    }

    function getInternational_fee_amount()
    {
        return $this->international_fee_amount;
    }

    function getDeliveredKit()
    {
        return $this->deliveredKit;
    }

    function getTransaction_Id()
    {
        return $this->transation_id;
    }

    function getSerial_rep()
    {
        return $this->serial_rep;
    }

    function getFuture_payment_sal()
    {
        return $this->future_payment_sal;
    }

    function getObservations_contract()
    {
        return $this->observations_contract;
    }

    //SETTERS
    function setSerial_sal($serial_sal)
    {
        $this->serial_sal = $serial_sal;
    }

    function setSerial_con($serial_con)
    {
        $this->serial_con = $serial_con;
    }

    function setSerial_cus($serial_cus)
    {
        $this->serial_cus = $serial_cus;
    }

    function setSal_serial_sal($sal_serial_sal)
    {
        $this->sal_serial_sal = $sal_serial_sal;
    }

    function setSerial_pbd($serial_pbd)
    {
        $this->serial_pbd = $serial_pbd;
    }

    function setSerial_cur($serial_cur)
    {
        $this->serial_cur = $serial_cur;
    }

    function setSerial_cit($serial_cit)
    {
        $this->serial_cit = $serial_cit;
    }

    function setSerial_cnt($serial_cnt)
    {
        $this->serial_cnt = $serial_cnt;
    }

    function setSerial_inv($serial_inv)
    {
        $this->serial_inv = $serial_inv;
    }

    function setCardNumber_sal($card_number_sal)
    {
        $this->card_number_sal = $card_number_sal;
    }

    function setEmissionDate_sal($emission_date_sal)
    {
        $this->emission_date_sal = $emission_date_sal;
    }

    function setBeginDate_sal($begin_date_sal)
    {
        $this->begin_date_sal = $begin_date_sal;
    }

    function setEndDate_sal($end_date_sal)
    {
        $this->end_date_sal = $end_date_sal;
    }

    function setInDate_sal($in_date_sal)
    {
        $this->in_date_sal = $in_date_sal;
    }

    function setDays_sal($days_sal)
    {
        $this->days_sal = $days_sal;
    }

    function setFee_sal($fee_sal)
    {
        $this->fee_sal = $fee_sal;
    }

    function setObservations_sal($observations_sal)
    {
        $this->observations_sal = $observations_sal;
    }

    function setCost_sal($cost_sal)
    {
        $this->cost_sal = $cost_sal;
    }

    function setFree_sal($free_sal)
    {
        $this->free_sal = $free_sal;
    }

    function setIdErpSal_sal($id_erp_sal)
    {
        $this->id_erp_sal = $id_erp_sal;
    }

    function setStatus_sal($status_sal)
    {
        $this->status_sal = $status_sal;
    }

    function setType_sal($type_sal)
    {
        $this->type_sal = $type_sal;
    }

    function setTotal_sal($total_sal)
    {
        $this->total_sal = $total_sal;
    }

    function setStockType_sal($stock_type_sal)
    {
        $this->stock_type_sal = $stock_type_sal;
    }

    function setDirectSale_sal($direct_sale_sal)
    {
        $this->direct_sale_sal = $direct_sale_sal;
    }

    function setTotalCost_sal($total_cost_sal)
    {
        $this->total_cost_sal = $total_cost_sal;
    }

    function setChange_fee_sal($change_fee_sal)
    {
        $this->change_fee_sal = $change_fee_sal;
    }

    function setInternationalFeeStatus_sal($international_fee_status)
    {
        $this->international_fee_status = $international_fee_status;
    }

    function setInternationalFeeUsed_sal($international_fee_used)
    {
        $this->international_fee_used = $international_fee_used;
    }

    function setInternationalFeeAmount_sal($international_fee_amount)
    {
        $this->international_fee_amount = $international_fee_amount;
    }

    function setDeliveredKit($deliveredKit)
    {
        $this->deliveredKit = $deliveredKit;
    }

    function setTransaction_Id($transactionId)
    {
        $this->transation_id = $transactionId;
    }

    function setSerial_rep($serial_rep)
    {
        $this->serial_rep = $serial_rep;
    }

    function setFuture_payment_sal($future_payment_sal)
    {
        $this->future_payment_sal = $future_payment_sal;
    }

    function setObservations_contract($observations_contract)
    {
        $this->observations_contract = $observations_contract;
    }

}

?>
