<?php
/*
File: ConditionsByProductType.class.php
Author: Miguel Ponce.
Creation Date: 07/01/2010
Last Modified: 07/01/2010
*/

class ConditionsByProductType{				
	var $db;
	var $serial_tpp;
	var $serial_gcn;
		
	function __construct($db, $serial_tpp=NULL, $serial_gcn=NULL){
		$this -> db = $db;
		$this -> serial_tpp = $serial_tpp;
		$this -> serial_gcn = $serial_gcn;
	}
	
	/***********************************************
    * funcion getBenefits
    * gets all the Benefits of the system
    ***********************************************/
	function getConditions(){
		$lista=NULL;
		$sql = 	"SELECT serial_gcn,serial_tpp
				 FROM conditions_by_product_type
				 WHERE serial_tpp = ".$this->serial_tpp;

                //echo $sql;
				 
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}

        function hasGeneralCondition($code_lang){
		$sql =" SELECT gcn.serial_gcn
                        FROM conditions_by_product_type cbpt
                            JOIN general_conditions gcn
                            ON gcn.serial_gcn = cbpt.serial_gcn
                            JOIN language lang
                            ON lang.serial_lang = gcn.serial_lang AND lang.code_lang ='".$code_lang."'
			WHERE cbpt.serial_tpp = $this->serial_tpp  ";


		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}



	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {

        $sql = "
            INSERT INTO conditions_by_product_type (
				serial_tpp,
				serial_gcn)
            VALUES (
				'".$this->serial_tpp."',
				'".$this->serial_gcn."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE conditions_by_product_type SET
			serial_gcn ='".$this->serial_gcn."'
		WHERE serial_tpp = '".$this->serial_tpp."'";

        //echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }
	
	
	///GETTERS

	function getSerial_tpp(){
		return $this->serial_tpp;
	}
	function getSerial_gcn(){
		return $this->serial_gcn;
	}


	///SETTERS	
	function setSerial_tpp($serial_tpp){
		$this->serial_tpp = $serial_tpp;
	}
	function setSerial_gcn($serial_gcn){
		$this->serial_gcn = $serial_gcn;
	}	

}
?>