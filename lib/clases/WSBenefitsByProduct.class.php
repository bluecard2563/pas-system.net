<?php
/*
File: BenefitsByProductType.class.php
Author: Miguel Ponce.
Creation Date: 07/01/2010
Last Modified: 07/01/2010
*/

class WSBenefitsByProduct{				
	var $db;
	var $serial_bxp;
	var $serial_rst;	
	var $serial_ben;
	var $serial_pro;
	var $serial_con;
	var $price_bxp;
	var $status_bxp;
	var $restriction_price_bxp;
	var $serial_cur;
		
	function __construct($db, $serial_bxp=NULL, $serial_rst=NULL, $serial_ben=NULL, $serial_pro=NULL, $serial_con=NULL, $price_bxp=NULL, $status_bxp=NULL, $restriction_price_bxp=NULL, $serial_cur=NULL){
		$this -> db = $db;
		$this -> serial_bxp = $serial_bxp;
		$this -> serial_rst = $serial_rst;
		$this -> serial_ben = $serial_ben;
		$this -> serial_pro = $serial_pro;
		$this -> serial_con = $serial_con;
		$this -> price_bxp = $price_bxp;
		$this -> status_bxp = $status_bxp;
		$this -> restriction_price_bxp = $restriction_price_bxp;
		$this -> serial_cur = $serial_cur;

	}
/***********************************************
        * function getData
        * gets data by serial_zon
        ***********************************************/
	function getData(){
            if($this->serial_bxp!=NULL){
                $sql = 	"SELECT serial_bxp, serial_rst, serial_ben, serial_pro, serial_con,
                                price_bxp, status_bxp, restriction_price_bxp, serial_cur
                         FROM benefits_product
                         WHERE serial_bxp='".$this->serial_bxp."'";
                //echo $sql." ";
                $result = $this->db->Execute($sql);
                if ($result === false) return false;

                if($result->fields[0]){
                    $this->serial_bxp=$result->fields[0];
                    $this->serial_rst=$result->fields[1];
                    $this->serial_ben=$result->fields[2];
                    $this->serial_pro=$result->fields[3];
                    $this->serial_con=$result->fields[4];
                    $this -> price_bxp = $result->fields[5];
                    $this -> status_bxp = $result->fields[6];
                    $this -> restriction_price_bxp = $result->fields[7];
                    $this -> serial_cur = $result->fields[8];
                    return true;
                }
                else
                    return false;
            }else
                return false;
	}
	/***********************************************
    * funcion getBenefits
    * gets all the Benefits of the system
    ***********************************************/
	function getBenefits(){
		if($this->serial_pro!=NULL){
			$sql = 	"SELECT serial_bxp,
						serial_rst,
						serial_ben,
						serial_cur,
						serial_pro,
						serial_con,
						price_bxp,
						status_bxp,
						restriction_price_bxp
					 FROM benefits_product
					 WHERE serial_pro = ".$this->serial_pro."
					 AND status_bxp = 'ACTIVE'
                     ORDER BY serial_ben ";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$arr=array();
				$cont=0;
				
				do{
					$arr[$result->fields[2]]=array('serial_bxp'=>$result->fields[0],
                                        'serial_rst'=>$result->fields[1],
                                        'serial_cur'=>$result->fields[3],
                                        'serial_pro'=>$result->fields[4],
                                        'serial_con'=>$result->fields[5],
                                        'price_bxp'=>$result->fields[6],
                                        'status_bxp'=>$result->fields[7],
                                        'restriction_price_bxp'=>$result->fields[8]
                                        );
			
					$result->MoveNext();
					$cont++;    
	
				}while($cont<$num);
			}

			return $arr;
		}
		else{
				return false;
		}	
	}
	
	/***********************************************
    * funcion getMyBenefits
    * gets all the Benefits of the product by the language
    ***********************************************/
	function getMyBenefits($language){
		if($this->serial_pro!=NULL){
			$sql = 	"SELECT bp.serial_ben, 
					bl.description_bbl, 
					price_bxp,	
					cr.symbol_cur, 
					bp.restriction_price_bxp,
					rl.description_rbl,
					cl.description_cbl
					
					FROM benefits_product bp
					LEFT JOIN benefits_by_language bl ON bp.serial_ben = bl.serial_ben AND bl.serial_lang = ".$language."
					LEFT JOIN conditions_by_language cl ON cl.serial_con = bp.serial_con AND cl.serial_lang = ".$language."
					LEFT JOIN currency cr ON cr.serial_cur = bp.serial_cur
					LEFT JOIN restriction_by_language rl ON rl.serial_rst = bp.serial_rst AND rl.serial_lang = ".$language."
					
					WHERE bp.serial_pro = ".$this -> serial_pro."
					AND bp.status_bxp = 'ACTIVE'";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
	        if($num > 0){
	            $arr=array();
	            $cont=0;
	            do{
	                $arr[$cont]=$result->GetRowAssoc(false);
	                $result->MoveNext();
	                $cont++;
	            }while($cont<$num);
	        }
	        return $arr;
		}
		return false;
	}
	
/***********************************************
    * funcion getMyBenefits
    * gets all the Benefits of the product by the language
    ***********************************************/
	public static function getAllProductBenefits($db, $language){
		$sql = 	"SELECT bp.serial_ben, 
						cl.description_cbl, 
						bl.description_bbl, 
						cr.symbol_cur, 
						rl.description_rbl, 
						bp.restriction_price_bxp
				
				FROM benefits_product bp
				LEFT JOIN benefits_by_language bl ON bp.serial_ben = bl.serial_ben
				LEFT JOIN conditions_by_language cl ON cl.serial_con = bp.serial_con
				LEFT JOIN currency cr ON cr.serial_cur = bp.serial_cur
				LEFT JOIN restriction_by_language rl ON rl.serial_rst = bp.serial_rst
				
				WHERE bp.serial_cur = 1
				AND cl.serial_lang = ".$language."
				AND bl.serial_lang = cl.serial_lang
				AND rl.serial_lang = cl.serial_lang
				AND bp.status_bxp = 'ACTIVE'";
		//die($sql);
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
	}

	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {

        $sql = "
            INSERT INTO benefits_product (
				serial_bxp,
				serial_rst,				
				serial_ben,
				serial_pro,
				serial_con,
				price_bxp,
				status_bxp,
				restriction_price_bxp,
				serial_cur)
            VALUES (
				NULL,";
                            if($this->serial_rst)
				$sql .="'".$this->serial_rst."',";
                            else
                                $sql .="NULL,";

				$sql .="'".$this->serial_ben."',
				'".$this->serial_pro."',
				'".$this->serial_con."',
				'".$this->price_bxp."',
				'".$this->status_bxp."',";

                            if($this->restriction_price_bxp)
				$sql .="'".$this->restriction_price_bxp."',";
                            else
                                $sql .="NULL,";
                                
				$sql .="'".$this->serial_cur."')";
        
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	

	/***********************************************
    * function delete
    * deletes a  register in DB 
    ***********************************************/
	function delete(){
		$sql = "DELETE FROM benefits_product
				WHERE serial_pro = '".$this->serial_pro."'";
				
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}

	/***********************************************
    * function delete
    * deletes a  register in DB
    ***********************************************/
	function benefitByProductExists(){
		$sql = "SELECT serial_bxp
				FROM benefits_product
				WHERE serial_ben = {$this->serial_ben}
				AND serial_pro = {$this->serial_pro}";
				
		$result = $this->db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/***********************************************
    * function delete
    * deletes a  register in DB
    ***********************************************/
	function activate(){
		$sql = " UPDATE benefits_product
				 SET serial_rst = '{$this->serial_rst}',
					 serial_con= '{$this->serial_con}',
					 serial_cur = '{$this->serial_cur}',
					 status_bxp = '{$this->status_bxp}',
					 price_bxp = '{$this->price_bxp}',
					 restriction_price_bxp = '{$this->restriction_price_bxp}'
				 WHERE serial_bxp = '{$this->serial_bxp}'";
				 //die($sql);
		$result = $this->db->Execute($sql);
		if($result)
			return true;
		else
			return false;
	}

	/***********************************************
    * function delete
    * deletes a  register in DB
    ***********************************************/
	function deactivateAll(){
		$sql = " UPDATE benefits_product
				 SET status_bxp = 'INACTIVE'
				 WHERE serial_pro = '".$this->serial_pro."'";

		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;
	}
	
	/***********************************************
        * function delete
        * deletes a  register in DB
        ***********************************************/
	function getBenefitsByProduct($serial_pro,$serial_lang){
            $sql = "SELECT bbl.serial_ben, bbl.description_bbl, bbp.price_bxp, bbp.restriction_price_bxp 
                    FROM benefits_product bbp
                    JOIN benefits_by_language bbl ON bbl.serial_ben = bbp.serial_ben AND bbl.serial_lang = '".$serial_lang."'
                    WHERE bbp.serial_pro = '".$serial_pro."'";
                    //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
	}
	

	/**
	@Name: getBenefitsByProductReport
	@Description: Retrieves the information of a specific product list
	@Params:
	@Returns: Product array
	**/
	function getBenefitsByProductReport($serial_zon, $serial_cou, $serial_pro, $serial_lang){
		$arreglo=array();
		$sql =  "SELECT bbl.description_bbl, bp.price_bxp, bp.restriction_price_bxp, rbl.description_rbl, c.alias_con,
						pbl.name_pbl, cou.name_cou, zon.name_zon
					FROM product pro
						JOIN product_by_country pbc ON pro.serial_pro=pbc.serial_pro AND pro.status_pro = 'ACTIVE'
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
						JOIN benefits_product bp ON bp.serial_pro=pro.serial_pro AND bp.status_bxp = 'ACTIVE'
						JOIN conditions c ON c.serial_con=bp.serial_con
						LEFT JOIN restriction_type rt ON rt.serial_rst=bp.serial_rst AND rt.status_rst = 'ACTIVE'
						LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = ".$serial_lang."
						JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
						JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang= ".$serial_lang."
						JOIN country cou ON cou.serial_cou = pbc.serial_cou AND cou.serial_cou = ".$serial_cou."
						JOIN zone zon ON zon.serial_zon = cou.serial_zon AND zon.serial_zon = ".$serial_zon."
					WHERE pbl.serial_lang = ".$serial_lang."  AND pro.serial_pro = ".$serial_pro."
					 ORDER BY pbl.name_pbl ";		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}
	
	///GETTERS

	function getSerial_bxp(){
		return $this->serial_bxp;
	}
	function getSerial_rst(){
		return $this->serial_rst;
	}
	function getSerial_ben(){
		return $this->serial_ben;
	}
	function getSerial_pro(){
		return $this->serial_pro;
	}
	function getStatus_ben(){
		return $this->status_ben;
	}
	function getSerial_con(){
		return $this->serial_con;
	}
	function getPrice_bxp(){
		return $this->price_bxp;
	}
	function getRestriction_price_bxp(){
		return $this->restriction_price_bxp;
	}
	function getSerial_cur(){
		return $this->serial_cur;
	}


	///SETTERS	
	function setSerial_bxp($serial_bxp){
		$this->serial_bxp = $serial_bxp;
	}
	function setSerial_rst($serial_rst){
		$this->serial_rst = $serial_rst;
	}	
	function setSerial_ben($serial_ben){
		$this->serial_ben = $serial_ben;
	}
	function setSerial_pro($serial_pro){
		$this->serial_pro = $serial_pro;
	}
	function setSerial_con($serial_con){
		$this->serial_con = $serial_con;
	}
	function setPrice_bxp($price_bxp){
		$this->price_bxp = $price_bxp;
	}
	function setStatus_bxp($status_bxp){
		$this->status_bxp = $status_bxp;
	}
	function setRestriction_price_bxp($restriction_price_bxp){
		$this->restriction_price_bxp = $restriction_price_bxp;
	}
	function setSerial_cur($serial_cur){
		$this->serial_cur = $serial_cur;
	}
}
?>