<?php
/*
File: Payment.class.php
Author: Edwin Salvador
Creation Date:12/03/2010
Modified By: David Bergmann
Last Modified: 02/08/2010
*/


class Payment{
    var $db;
    var $serial_pay;
    var $total_to_pay_pay;
    var $total_payed_pay;
    var $status_pay;
    var $date_pay;
    var $observation_pay;
    var $excess_amount_available_pay;
	var $full_date_pay;
	var $erp_id;
    
    function __construct($db, $serial_pay = NULL, $total_to_pay_pay = NULL, $total_payed_pay = NULL, $status_pay = NULL, $date_pay = NULL, $observation_pay = NULL, $excess_amount_available_pay = NULL){
        $this -> db = $db;
        $this -> serial_pay = $serial_pay;
        $this -> total_to_pay_pay = $total_to_pay_pay;
        $this -> total_payed_pay = $total_payed_pay;
        $this -> status_pay = $status_pay;
        $this -> date_pay = $date_pay;
        $this -> observation_pay = $observation_pay;
    }

    /***********************************************
    * function insert
    * insert a row in the DB
    ***********************************************/
    function insert(){
        $sql = "INSERT INTO payments (
						serial_pay,
						total_to_pay_pay,
						total_payed_pay,
						status_pay,
						date_pay,
						full_date_pay,
						observation_pay,
						excess_amount_available_pay,
						erp_id
				) VALUES(
					NULL,
                    '".($this->total_to_pay_pay?$this->total_to_pay_pay:"0.00")."',
                    '".($this->total_payed_pay?$this->total_payed_pay:"0.00")."',
                    '".$this->status_pay."',";
			($this->date_pay)?$sql.=" STR_TO_DATE('{$this->date_pay}', '%d/%m/%Y %H:%i:%s'),":$sql.="NOW(),";
			($this->full_date_pay)?$sql.=" STR_TO_DATE('{$this->full_date_pay}', '%d/%m/%Y %H:%i:%s'),":$sql.="NULL,";
            $sql .= ($this->observation_pay?"'".$this->observation_pay."'":"NULL").",
					".$this->excess_amount_available_pay.",
					'".$this->erp_id."');";
		
		//Debug::print_r($sql); die;
        $result = $this->db->Execute($sql);
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/***********************************************
    * function update
    * update a row in the DB
    ***********************************************/
    function update(){
        $sql = "UPDATE payments
				SET total_payed_pay =  ".$this->total_payed_pay.",
                    status_pay =  '".$this->status_pay."',
                    excess_amount_available_pay=".$this->excess_amount_available_pay;
		if($this->observation_pay){
			$sql .= ", observation_pay =  '".$this->observation_pay."',";
		}
		if($this->erp_id){
			$sql.=" erp_id = '{$this->erp_id}'";
		}
			$sql .= " WHERE serial_pay = '".$this->serial_pay."'";
        //die($sql);
        $result = $this->db->Execute($sql);

        if($result == true){
            return true;
        }else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
            return false;
        }
    }
	/***********************************************
    * function update
    * update a row in the DB
    ***********************************************/
    function confirmPaidSale(){
        $sql = "UPDATE payments
				SET status_pay =  'ONLINE_EFECTIVE'
				WHERE serial_pay = '".$this->serial_pay."'";
        $result = $this->db->Execute($sql);
        if(mysql_affected_rows() < 1){
        	return 1;//PAYMENT ALREADY CONFIRMED
        }
        
        $sql = "UPDATE invoice
				SET status_inv =  'PAID'
				WHERE serial_pay = '".$this->serial_pay."'";
        $result = $this->db->Execute($sql);
    	if(mysql_affected_rows() < 1){
        	return 2;//NO INVOICE FOUND
        }
        
        $sql = "SELECT serial_inv 
        		FROM invoice
        		WHERE serial_pay = '".$this->serial_pay."'";
        $result = $this->db->Execute($sql);
        
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        
        $rawValues = array();
        foreach ($arr as $a){
        	$rawValues[] = $a['serial_inv'];
        }
        if($rawValues){
			$invoices = implode(",",$rawValues);
        }
        
        $sql = "UPDATE sales
				SET status_sal =  'ACTIVE'
				WHERE serial_inv IN(".$invoices.")";
        $result = $this->db->Execute($sql);
    	if(mysql_affected_rows() < 1){
        	return 3;//NO SALES FOUND
        }
        
        return -1;//OK
    }

    /***********************************************
    * function update
    * update a row in the DB
    ***********************************************/
    function updateExcess_amount_available_pay($value){
        $sql = "UPDATE payments
				SET excess_amount_available_pay =  excess_amount_available_pay-".$value."
                                WHERE serial_pay = '".$this->serial_pay."'";
        //die($sql);
        $result = $this->db->Execute($sql);

        if($result == true){
            return true;
        }else{
            return false;
        }
    }
   

    /***********************************************
    * function getData
    * sets data by serial_pyf
    ***********************************************/
    function getData(){		
        if($this->serial_pay){
            $sql = "SELECT	serial_pay, 
							total_to_pay_pay, 
							total_payed_pay, 
							status_pay, 
							DATE_FORMAT(date_pay,'%d/%m/%Y') as date_pay, 
							observation_pay, 
							excess_amount_available_pay,
							DATE_FORMAT(full_date_pay,'%d/%m/%Y') as full_date_pay,
							erp_id
                    FROM payments
                    WHERE serial_pay='".$this->serial_pay."'";
			         
            $result = $this->db->getRow($sql);
            if ($result === false) return false;

            if($result){
                $this -> serial_pay =$result['serial_pay'];
                $this -> total_to_pay_pay = $result['total_to_pay_pay'];
                $this -> total_payed_pay = $result['total_payed_pay'];
                $this -> status_pay = $result['status_pay'];
                $this -> date_pay = $result['date_pay'];
                $this -> observation_pay = $result['observation_pay'];
                $this -> excess_amount_available_pay = $result['excess_amount_available_pay'];
				$this -> full_date_pay = $result['full_date_pay'];
				$this -> erp_id = $result['erp_id'];
				return true;
            }
        }
		
        return false;
    }
	
	function getDataByERPId($erp_id){		
		$sql = "SELECT	serial_pay, 
						total_to_pay_pay, 
						total_payed_pay, 
						status_pay, 
						DATE_FORMAT(date_pay,'%d/%m/%Y') as date_pay, 
						observation_pay, 
						excess_amount_available_pay,
						DATE_FORMAT(full_date_pay,'%d/%m/%Y') as full_date_pay,
						erp_id
				FROM payments
				WHERE erp_id='$erp_id'";

		$result = $this->db->getRow($sql);
		if ($result === false) return false;

		if($result->fields['serial_pay']){
			$this -> serial_pay =$result['serial_pay'];
			$this -> total_to_pay_pay = $result['total_to_pay_pay'];
			$this -> total_payed_pay = $result['total_payed_pay'];
			$this -> status_pay = $result['status_pay'];
			$this -> date_pay = $result['date_pay'];
			$this -> observation_pay = $result['observation_pay'];
			$this -> excess_amount_available_pay = $result['excess_amount_available_pay'];
			$this -> full_date_pay = $result['full_date_pay'];
			$this -> erp_id = $result['erp_id'];
			return true;
		}
    }

    /***********************************************
    * funcion getPayedInvoices
    * gets information about payed invoices
    ***********************************************/
    function getPayedInvoices($serial_cou, $serial_cit = NULL, $serial_usr = NULL, $category_dea = NULL, $serial_dea = NULL, $dea_serial_dea = NULL, $status_pay = NULL){
        $f1 = ''; $f2 = ''; $f3 = ''; $f4 = ''; $f5 = '';

        if($serial_cit){
            $f1 = "AND cit.serial_cit = $serial_cit";
        }
        if($serial_usr){
            $f2 = "AND ud.serial_usr = $serial_usr";
        }
        if($category_dea){
            $f3 = "AND dea.category_dea='$category_dea'";
        }
        if($serial_dea){
            $f4 = "AND dea.dea_serial_dea = $serial_dea";
        }
        if($dea_serial_dea){
            $f4 = "AND dea.serial_dea = $dea_serial_dea";
        }
        if($status_pay){
            $f5 = "WHERE pay.status_pay = '$status_pay'";
        }

        $sql = "SELECT DISTINCT pay.serial_pay, LPAD(inv.number_inv,8,'00000000') as number_inv, pay.status_pay, pay.total_to_pay_pay,
                       pay.total_payed_pay, pay.date_pay, inv.date_inv, inv.due_date_inv, inv.total_inv, dt.name_doc,
                       (SELECT name_dea FROM dealer WHERE serial_dea=inv.serial_dea) AS dea_name,
                       (SELECT CONCAT(first_name_cus,' ',last_name_cus) FROM customer WHERE serial_cus=inv.serial_cus) AS cus_name
                FROM sales AS sal
                JOIN counter AS cou ON cou.serial_cnt=sal.serial_cnt
                JOIN dealer AS dea ON dea.serial_dea = cou.serial_dea $f3 $f4
                JOIN user_by_dealer AS ud ON ud.serial_dea = dea.serial_dea $f2
                JOIN sector AS sec ON sec.serial_sec = dea.serial_sec
                JOIN city AS cit ON cit.serial_cit = sec.serial_cit $f1
                JOIN country AS co ON co.serial_cou = cit.serial_cou AND co.serial_cou = ".$serial_cou."
				JOIN invoice AS inv ON sal.serial_inv=inv.serial_inv
				JOIN document_by_country AS dbc ON dbc.serial_dbc=inv.serial_dbc
                JOIN document_type AS dt ON dt.serial_doc=dbc.serial_doc
                LEFT JOIN payments AS pay ON pay.serial_pay=inv.serial_pay
				$f5";

        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }

    /***********************************************
    * funcion getDetailedPayedInvoices
    * gets information about payed invoices
    ***********************************************/
    function getDetailedPayedInvoices($serial_cou, $serial_cit = NULL, $serial_usr = NULL, $category_dea = NULL, $serial_dea = NULL, $dea_serial_dea = NULL, $status_pay = NULL){
        $f1 = ''; $f2 = ''; $f3 = ''; $f4 = ''; $f5 = '';

        if($serial_cit){
            $f1 = "AND cit.serial_cit = $serial_cit";
        }
        if($serial_usr){
            $f2 = "AND ud.serial_usr = $serial_usr";
        }
        if($category_dea){
            $f3 = "AND dea.category_dea='$category_dea'";
        }
        if($serial_dea){
            $f4 = "AND dea.dea_serial_dea = $serial_dea";
        }
        if($dea_serial_dea){
            $f4 = "AND dea.serial_dea = $dea_serial_dea";
        }
        if($status_pay){
            $f5 = "WHERE pay.status_pay = '$status_pay'";
        }

        $sql = 	"SELECT pay.serial_pay, CONCAT(LPAD(inv.number_inv,8,'00000000'),' - ',dt.name_doc) AS invoices, inv.total_inv, pay.status_pay, pd.type_pyf, pd.date_pyf, pd.amount_pyf
                FROM payments AS pay
                JOIN payment_detail AS pd ON pd.serial_pay = pay.serial_pay
                JOIN invoice AS inv ON inv.serial_pay = pay.serial_pay
                JOIN document_by_country AS dbc ON dbc.serial_dbc=inv.serial_dbc
                JOIN document_type AS dt ON dt.serial_doc=dbc.serial_doc
                JOIN sales AS sal ON sal.serial_inv=inv.serial_inv
                JOIN counter AS cou ON cou.serial_cnt=sal.serial_cnt
                JOIN dealer AS dea ON dea.serial_dea = cou.serial_dea $f3 $f4
                JOIN user_by_dealer AS ud ON ud.serial_dea = dea.serial_dea $f2
                JOIN sector AS sec ON sec.serial_sec = dea.serial_sec
                JOIN city AS cit ON cit.serial_cit = sec.serial_cit $f1 
                JOIN country AS co ON co.serial_cou = cit.serial_cou AND co.serial_cou = $serial_cou
		        $f5";
        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }

	public static function getExcessPaymentsByBranch($db,$serial_dea, $serial_cou){
		$sql="SELECT DISTINCT p.serial_pay, 
					 p.date_pay, 
					 p.excess_amount_available_pay, 
					(p.total_payed_pay-p.total_to_pay_pay) AS excess_amount,
					 IF(i.serial_dea IS NOT NULL, 'DEALER', 'CUSTOMER') AS in_favor_to,
					 IF(i.serial_dea IS NOT NULL, b.name_dea, CONCAT(c.first_name_cus,' ',c.last_name_cus)) AS receiver,
					 IF(i.serial_dea IS NOT NULL, i.serial_dea, i.serial_cus) AS serial,
					 p.total_to_pay_pay,
					 p.total_payed_pay,
					 IFNULL(
					   		GROUP_CONCAT( DISTINCT pd.number_pyf ORDER BY pd.number_pyf ASC SEPARATOR ',<br>' ),
							GROUP_CONCAT( DISTINCT pd.serial_pyf ORDER BY pd.serial_pyf ASC SEPARATOR ',<br>' )
					   		) AS 'payment_number'
			  FROM payments p
			  JOIN payment_detail pd ON pd.serial_pay = p.serial_pay
			  JOIN invoice i ON i.serial_pay=p.serial_pay
			  LEFT JOIN dealer b ON b.serial_dea=i.serial_dea
			  LEFT JOIN customer c ON c.serial_cus=i.serial_cus
			  JOIN sales s ON s.serial_inv=i.serial_inv
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer bsal ON bsal.serial_dea=cnt.serial_dea AND bsal.serial_dea='$serial_dea'
			  WHERE p.excess_amount_available_pay > 0
			  GROUP BY p.serial_pay";

		$result=$db->getAll($sql);

		if($result){
		  return $result;
		}else{
		  return false;
		}
	}

	/*
	 * @Name: getDispatchedPayments
	 * @Description: Selects the info of all the payments to be dispatched.
	 * @Params: The ID´s of the payments to be dispatched.
	 * @Returns: An array of payments info.
	 */
	function getDispatchedPayments($payments_serials){
		$sql = "SELECT DATE_FORMAT(MAX(pd.date_pyf), '%d/%m/%Y') AS date_pay,
				(p.excess_amount_available_pay ) AS excess_amount,
					   IF(i.serial_dea IS NOT NULL, 
					   b.name_dea, CONCAT(c.first_name_cus,' ',c.last_name_cus)) AS receiver,
					   DATE_FORMAT(i.date_inv, '%d/%m/%Y') AS date_inv,
					   GROUP_CONCAT(DISTINCT CAST(i.number_inv AS CHAR) ORDER BY i.number_inv SEPARATOR ',') AS number_inv,
					   IFNULL(
					   		GROUP_CONCAT( DISTINCT pd.number_pyf ORDER BY pd.number_pyf ASC SEPARATOR ',\n' ),
							GROUP_CONCAT( DISTINCT pd.serial_pyf ORDER BY pd.serial_pyf ASC SEPARATOR ',\n' )
					   		) AS 'payment_number', p.serial_pay,
					   p.total_to_pay_pay,
					   p.total_payed_pay
				FROM payments p
				JOIN payment_detail pd ON pd.serial_pay = p.serial_pay
				JOIN invoice i ON i.serial_pay=p.serial_pay
				LEFT JOIN dealer b ON b.serial_dea=i.serial_dea
				LEFT JOIN customer c ON c.serial_cus=i.serial_cus
				WHERE p.serial_pay IN ($payments_serials)
				GROUP BY p.serial_pay";

		$result=$this->db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

    /***********************************************
    * funcion getPaymentStates
    * gets information about payed invoices
    ***********************************************/
    function getPaymentStates(){
        $sql = "SHOW COLUMNS FROM payments LIKE 'status_pay'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
	
	/***********************************************
    * funcion getExcessPayments
    * gets information about excess payed invoices
    ***********************************************/
	public static function getExcessPayments($db, $serial_cou, $serial_mbc, $restricted_mbc, 
								$status = NULL, $serial_dea = NULL, $serial_bra = NULL, 
								$restricted_dea = NULL, $limited = FALSE){
		
        if($serial_dea) $dealerSQL = " AND br.dea_serial_dea = '".$serial_dea."'";
        if($serial_bra) $branchSQL = " AND br.serial_dea = '".$serial_bra."'";
		if($status){
			if($status == 'Available'){
				$statusSQL = " AND pay.excess_amount_available_pay > 0";
			}
			else{
				$statusSQL = " AND pay.excess_amount_available_pay = 0";
			}
        }
		
		if($limited):
			$limit_statement = "LIMIT 1";
		endif;

		$sql = "SELECT  pay.serial_pay, pay.total_payed_pay, total_to_pay_pay, excess_amount_available_pay,
						DATE_FORMAT(MAX(date_pyf), '%d/%m/%Y') AS date_pay, br.name_dea, 
						GROUP_CONCAT( DISTINCT inv.number_inv ORDER BY inv.number_inv ASC SEPARATOR ', \n') AS invoice_group,
						GROUP_CONCAT(DISTINCT IFNULL(pyf.number_pyf, 'N/A')) AS payments_group,
						cou.name_cou, IFNULL(epl.payment_date_xpl, 'N/A') AS 'liq_date', 
						IF(usr.first_name_usr IS NULL, 'N/A', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr)) AS 'liq_usr',
						(total_payed_pay - total_to_pay_pay) AS 'excess_amount'
				FROM sales sal
				JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
				JOIN dealer br ON cnt.serial_dea=br.serial_dea  $branchSQL $dealerSQL
				JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc  AND mbc.serial_mbc = $serial_mbc
				JOIN country cou ON cou.serial_cou = mbc.serial_cou AND cou.serial_cou=".$serial_cou."
				JOIN invoice inv ON inv.serial_inv = sal.serial_inv
				JOIN payments pay ON pay.serial_pay = inv.serial_pay $statusSQL
				JOIN payment_detail pyf ON pyf.serial_pay = pay.serial_pay AND pyf.status_pyf = 'ACTIVE'
				LEFT JOIN excess_payments_liquidation epl ON epl.serial_pay = pay.serial_pay
				LEFT JOIN user usr ON usr.serial_usr = epl.serial_usr
				WHERE pay.status_pay='EXCESS'";
		
		if($restricted_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$restricted_mbc;
		}
		if($restricted_dea){
			$sql.=" AND br.dea_serial_dea=".$restricted_dea;
		}
		
		$sql.=" GROUP BY pay.serial_pay 
				ORDER BY pay.serial_pay, br.name_dea
				$limit_statement";
		
		//Debug::print_r($sql); die;
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/***********************************************
    * function getPayments
    * gets information about payed invoices
    ***********************************************/
	function getPayments($dateFrom, $dateTo, $serial_mbc_user, $dea_serial_dea_user, $document_cus=NULL,
						 $card_number_sal=NULL, $serial_cou=NULL, $serial_mbc=NULL, $serial_dea=NULL,
						 $serial_bra=NULL, $type=NULL, $serial_comissionist = NULL, $serial_cit = NULL){
		$customerSQL = '';
		$cardSQL = '';
		$invoiceSQL = '';
		$countrySQL = '';
		$managerSQL = '';
		$dealerSQL = '';
		$comissionistSQL = '';
		$branchSQL = '';
		$typeSQL = '';
		
		global $phoneSalesDealer;
		global $webSalesDealer; 
		
		if($document_cus){
			$customerSQL = "AND cus.document_cus ='".$document_cus."'";
		}
		if($card_number_sal){
			$cardSQL = " AND sal.card_number_sal = ".$card_number_sal;
		}
		if($serial_cou){			
			$countrySQL = "	 AND cou.serial_cou=".$serial_cou;
		}
		if($serial_cit){
			$citySQL = " AND sec.serial_cit = $serial_cit";
		}
		if($serial_mbc){
			$managerSQL = " AND mbc.serial_mbc=".$serial_mbc;
		}
		if($serial_dea){
			$dealerSQL = " AND dea.serial_dea = ".$serial_dea;
		}
		if($serial_comissionist){
			$comissionistSQL = "JOIN user_by_dealer ubd ON br.serial_dea = ubd.serial_dea AND ubd.serial_usr = $serial_comissionist AND ubd.status_ubd = 'ACTIVE'";
		}
		if($serial_bra){
			$branchSQL = " AND br.serial_dea = ".$serial_bra;
		}
		if($type == 1){
			$typeSQL = " AND (pay.status_pay = 'PAID' OR pay.status_pay = 'EXCESS') ";
		}elseif($type == 2){
			$typeSQL = " AND pay.status_pay = 'PARTIAL'";
		}
		$sql = "SELECT DISTINCT(inv.serial_inv), pay.serial_pay, 
					    DATE_FORMAT(pay.date_pay, '%d/%m/%Y') AS date_pay,
						pay.total_payed_pay, dea.name_dea, 
						GROUP_CONCAT( DISTINCT CAST(inv.number_inv AS CHAR) SEPARATOR '\n') AS invoice_group,
						GROUP_CONCAT( DISTINCT CAST(IFNULL(sal.card_number_sal,'N/A') AS CHAR) SEPARATOR '\n') AS card_group,
						GROUP_CONCAT( DISTINCT CAST(IF(cus.first_name_cus IS NULL,'N/A', CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,''))) AS CHAR) SEPARATOR '\n') AS name_customer_group,
						GROUP_CONCAT( DISTINCT IFNULL(inv.serial_dea,'N/A') ) AS inv_serial_dea,
						cou.name_cou, man.name_man, 
						CONCAT(br.name_dea, ' - ',br.code_dea) AS name_bra,
						dea.serial_dea,
						IF(pay.total_payed_pay - pay.total_to_pay_pay <= 0, 'N/A', pay.total_payed_pay - pay.total_to_pay_pay) AS excess,
						IF(pay.status_pay = 'PARTIAL','PARTIAL','PAID') as type,
						GROUP_CONCAT( DISTINCT CAST(pd.number_pyf AS CHAR) SEPARATOR '\n') AS 'payment_numbers'
				FROM sales sal
				JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
				JOIN dealer br ON cnt.serial_dea=br.serial_dea $branchSQL
				JOIN sector sec ON sec.serial_sec = br.serial_sec $citySQL
				JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealerSQL
				$comissionistSQL
				JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc $managerSQL
				JOIN manager man ON man.serial_man=mbc.serial_man
				JOIN country cou ON mbc.serial_cou=cou.serial_cou $countrySQL
				JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
				JOIN payments pay ON pay.serial_pay=inv.serial_pay
				JOIN payment_detail pd ON pd.serial_pay = pay.serial_pay
				LEFT JOIN customer AS cus ON cus.serial_cus = inv.serial_cus
				WHERE DATE_FORMAT(pay.date_pay,'%Y/%m/%d') BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND STR_TO_DATE('$dateTo', '%d/%m/%Y')
				$cardSQL $customerSQL
				$typeSQL
				AND dea.serial_dea <> $phoneSalesDealer 
				AND dea.serial_dea <> $webSalesDealer";

		if($serial_mbc_user!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc_user;
		}

		if($dea_serial_dea_user){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea_user;
		}
		
		$sql.=" GROUP BY pay.serial_pay
				ORDER BY pay.serial_pay";
		//die(Debug::print_r($sql));
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/***********************************************
    * funcion getPayments
    * gets information about payed invoices
    ***********************************************/
	function getPaymentsInvoice($serial_cou,$serial_mbc,$numberInv,$type, $serial_mbc_user, $dea_serial_dea){
		$sql = "SELECT  DISTINCT(inv.serial_inv), pay.serial_pay,
						DATE_FORMAT(pay.date_pay, '%d/%m/%Y') AS date_pay,
						pay.total_payed_pay, dea.name_dea,
						GROUP_CONCAT( DISTINCT CAST(inv.number_inv AS CHAR) SEPARATOR ',' ) AS invoice_group,
						GROUP_CONCAT( DISTINCT CAST(IFNULL(sal.card_number_sal,'N/A') AS CHAR) SEPARATOR ',') AS card_group,
						GROUP_CONCAT( DISTINCT CAST(IFNULL(cus.first_name_cus,'N/A') AS CHAR) SEPARATOR ',') AS name_customer_group,
                        GROUP_CONCAT( DISTINCT CAST(IF(cus.last_name_cus IS NULL OR cus.last_name_cus='','N/A', cus.last_name_cus) AS CHAR) SEPARATOR ',') AS last_customer_group,
						GROUP_CONCAT( DISTINCT CAST(IFNULL(inv.serial_dea,'N/A') AS CHAR) SEPARATOR ',') AS inv_serial_dea,
						cou.name_cou, man.name_man, CONCAT(br.name_dea, ' - ',br.code_dea) AS name_bra, dea.serial_dea,
						IF(pay.total_payed_pay - pay.total_to_pay_pay <= 0, 'N/A', pay.total_payed_pay - pay.total_to_pay_pay) AS excess,
						GROUP_CONCAT( DISTINCT CAST(pd.number_pyf AS CHAR) SEPARATOR '\n') AS 'payment_numbers'
				FROM sales sal
				JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
				JOIN dealer br ON cnt.serial_dea=br.serial_dea
				JOIN dealer dea ON dea.serial_dea=br.serial_dea ";
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		
		$sql.=" JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc AND mbc.serial_mbc=$serial_mbc";

		if($serial_mbc_user!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc_user;
		}

		$sql.="	JOIN manager man ON man.serial_man=mbc.serial_man
				JOIN document_by_manager dbm ON dbm.serial_man=mbc.serial_man 
					AND (dbm.type_dbm = 'BOTH' OR dbm.type_dbm='$type')
					AND dbm.purpose_dbm='INVOICE'
				JOIN country cou ON mbc.serial_cou=cou.serial_cou AND cou.serial_cou=$serial_cou
				JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv AND inv.number_inv='".$numberInv."'
				JOIN payments pay ON pay.serial_pay=inv.serial_pay
				JOIN payment_detail pd ON pd.serial_pay = pay.serial_pay
				LEFT JOIN customer AS cus ON cus.serial_cus = inv.serial_cus
				GROUP BY pay.serial_pay";
		
		$sql.=" ORDER BY dea.name_dea, dea.serial_dea";
		
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/***********************************************
    * function getPaymentDetailByCountry
    * gets information about payments
    ***********************************************/
	function getPaymentDetailByCountry($serial_cou,$serial_mbc,$dateFrom = NULL,$dateTo = NULL){
		$dateSQL = "";
		if($dateFrom && $dateTo){
			$dateSQL = "WHERE DATE_FORMAT(pyf.date_pyf,'%d/%m/%Y')BETWEEN
								DATE_FORMAT( STR_TO_DATE('$dateFrom', '%d/%m/%Y'), '%d/%m/%Y')
								AND DATE_FORMAT( STR_TO_DATE('$dateTo', '%d/%m/%Y'), '%d/%m/%Y')";
		}

		$sql = " SELECT DISTINCT(pay.serial_pay),
						IF(pyf.number_pyf <> 0, pyf.number_pyf, pyf.serial_pyf) AS 'number_pyf',
						pyf.type_pyf,
						IF(pyf.comments_pyf='Efectivo','N/A',pyf.comments_pyf) AS comments_pyf,
						pyf.document_number_pyf,
						pyf.amount_pyf,
						DATE_FORMAT(pay.date_pay, '%d/%m/%Y') AS 'date_pay',
						dea.name_dea,
						GROUP_CONCAT(DISTINCT CAST(inv.number_inv AS CHAR) ORDER BY inv.number_inv SEPARATOR ', <br>' ) AS 'invoice_group',
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS name_usr
				 FROM sales s
				 JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				 JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
                 JOIN manager_by_country AS mbc ON mbc.serial_mbc = dea.serial_mbc  AND mbc.serial_mbc = $serial_mbc
				 JOIN sector sec ON dea.serial_sec=sec.serial_sec
				 JOIN city cit ON sec.serial_cit=cit.serial_cit
				 JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
				 JOIN invoice inv ON inv.serial_inv=s.serial_inv
                 JOIN payments pay ON pay.serial_pay=inv.serial_pay
				 JOIN payment_detail pyf ON pyf.serial_pay=pay.serial_pay AND checked_pyf='NO'
				 JOIN user usr ON usr.serial_usr=pyf.serial_usr
				 $dateSQL
				 GROUP BY pay.serial_pay";
		//die($sql);//
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}
	
	public static function getAccountingRangeSalesInformation($db, $serial_mbc, $date_from, $date_to, 
														$serial_dea = NULL, $serial_bra = NULL){
		
		if($serial_dea) $contidions = " AND aca.serial_dea = $serial_dea";
		if($serial_bra) $contidions .= " AND aca.serial_dea = $serial_bra";
		
		$sql = "	SELECT	dealer_id AS 'main_id', aca.name_dea AS 'main_name', 
							SUM(total_inv) AS 'pending_amounts',
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'seller'
					FROM view_accounting_pending_accounts aca
					JOIN dealer d ON d.serial_dea = aca.dealer_id AND d.manager_rights_dea = 'NO'
					JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
					JOIN user u ON u.serial_usr = ubd.serial_usr
					WHERE aca.serial_mbc = $serial_mbc
					AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN 
							STR_TO_DATE('$date_from', '%d/%m/%Y') AND
							STR_TO_DATE('$date_to', '%d/%m/%Y')
					$contidions
					GROUP BY dealer_id
					ORDER BY dealer_id
		
				";
		
		$sql_submanager = "	SELECT	branch_id AS 'main_id', branch_name AS 'main_name', 
									SUM(total_inv) AS 'pending_amounts',
									CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'seller'
							FROM view_accounting_pending_accounts aca
							JOIN dealer d ON d.serial_dea = aca.dealer_id AND d.manager_rights_dea = 'YES'
							JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
							JOIN user u ON u.serial_usr = ubd.serial_usr
							WHERE aca.serial_mbc = $serial_mbc
							AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN 
									STR_TO_DATE('$date_from', '%d/%m/%Y') AND
									STR_TO_DATE('$date_to', '%d/%m/%Y')
							$contidions
							GROUP BY branch_id
							ORDER BY branch_id";
		
		$result = $db -> getAll($sql);
		$result_submanager = $db -> getAll($sql_submanager);
		
		if($result || $result_submanager){
			$final_array = array();
			
			if($result){
				$final_array = $result;
			}
			
			if($result_submanager){
				foreach($result_submanager as $r){
					array_push($final_array, $r);
				}
			}
			
			return $final_array;
		}else{
			return FALSE;
		}
		
	}
	
	public static function getAccountingLimitedSalesInformation($db, $serial_mbc, $date_to, 
														$serial_dea = NULL, $serial_bra = NULL){
		
		if($serial_dea) $contidions = " AND main_id = $serial_dea";
		if($serial_bra) $contidions .= " AND main_id = $serial_bra";
                $condition_mbc = "WHERE aca.serial_mbc = ".$serial_mbc;

		$sql = "	SELECT	dealer_id AS 'main_id', aca.name_dea AS 'main_name', 
							SUM(IF(invoice_status = 'INDATE', total_inv, 0)) AS 'in_values',
							SUM(IF(invoice_status = 'DUE', total_inv, 0)) AS 'due_values',
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'seller'
					FROM view_accounting_pending_accounts aca
					JOIN dealer d ON d.serial_dea = aca.dealer_id AND d.manager_rights_dea = 'NO'
					JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
					JOIN user u ON u.serial_usr = ubd.serial_usr ";
//					WHERE aca.serial_mbc = $serial_mbc
                                        if($serial_mbc != null){
                                            $sql .=$condition_mbc;
                                        }
			$sql .= " AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') <= 
							STR_TO_DATE('$date_to', '%d/%m/%Y')
					$contidions
					GROUP BY dealer_id
					ORDER BY dealer_id";
		
		$sql_submanager = "	SELECT	branch_id AS 'main_id', branch_name AS 'main_name', 
									SUM(IF(invoice_status = 'INDATE', total_inv, 0)) AS 'in_values',
									SUM(IF(invoice_status = 'DUE', total_inv, 0)) AS 'due_values',
									CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'seller'
							FROM view_accounting_pending_accounts aca
							JOIN dealer d ON d.serial_dea = aca.dealer_id AND d.manager_rights_dea = 'YES'
							JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
							JOIN user u ON u.serial_usr = ubd.serial_usr ";
//							WHERE aca.serial_mbc = $serial_mbc
                                                     if($serial_mbc != null){
                                                            $sql_submanager .=$condition_mbc;
                                                        }
				$sql_submanager .="			 AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') <= 
									STR_TO_DATE('$date_to', '%d/%m/%Y') 
							$contidions
							GROUP BY branch_id
							ORDER BY branch_id
				";
		
		//Debug::print_r($sql); die;
		
		$result = $db -> getAll($sql);
		$result_submanager = $db -> getAll($sql_submanager);
		
		if($result || $result_submanager){
			$final_array = array();
			
			if($result){
				$final_array = $result;
			}
			
			if($result_submanager){
				foreach($result_submanager as $r){
					array_push($final_array, $r);
				}
			}
			
			return $final_array;
		}else{
			return FALSE;
		}
		
	}
	
	public static function getAccountingPaymentInformation($db, $serial_mbc, $date_from, $date_to, 
															$serial_dea = NULL, $serial_bra = NULL){
		
		if($serial_dea) $contidions = " AND d.serial_dea = $serial_dea";
		if($serial_bra) $contidions .= " AND b.serial_dea = $serial_bra";
		
		$sql = "	SELECT	p.serial_pay, p.total_payed_pay, pd.date_pyf, 
							d.serial_dea AS 'main_id', b.serial_mbc,
							d.name_dea AS 'main_name',
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'seller'
					FROM invoice i
					JOIN sales s ON s.serial_inv = i.serial_inv
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer b ON b.serial_dea = cnt.serial_dea
					JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
					JOIN user u ON u.serial_usr = ubd.serial_usr
					JOIN dealer d ON d.serial_dea = b.dea_serial_dea AND d.manager_rights_dea = 'NO'
					JOIN payments p ON p.serial_pay = i.serial_pay
					JOIN payment_detail pd ON pd.serial_pay = p.serial_pay
					WHERE b.serial_mbc = $serial_mbc
					AND STR_TO_DATE(DATE_FORMAT(date_pyf, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN 
							STR_TO_DATE('$date_from', '%d/%m/%Y') AND
							STR_TO_DATE('$date_to', '%d/%m/%Y')
					$contidions
					GROUP BY p.serial_pay
					ORDER BY main_id";
		
		$sql_submanager = "	SELECT	p.serial_pay, p.total_payed_pay, pd.date_pyf, 
									b.serial_dea AS 'main_id', b.serial_mbc,
									b.name_dea AS 'main_name',
									CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'seller'
							FROM invoice i
							JOIN sales s ON s.serial_inv = i.serial_inv
							JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
							JOIN dealer b ON b.serial_dea = cnt.serial_dea
							JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
							JOIN user u ON u.serial_usr = ubd.serial_usr
							JOIN dealer d ON d.serial_dea = b.dea_serial_dea AND d.manager_rights_dea = 'YES'
							JOIN payments p ON p.serial_pay = i.serial_pay
							JOIN payment_detail pd ON pd.serial_pay = p.serial_pay
							WHERE b.serial_mbc = $serial_mbc
							AND STR_TO_DATE(DATE_FORMAT(date_pyf, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN 
									STR_TO_DATE('$date_from', '%d/%m/%Y') AND
									STR_TO_DATE('$date_to', '%d/%m/%Y')
							$contidions
							GROUP BY p.serial_pay
							ORDER BY main_id";
		//Debug::print_r($sql); die;
		
		$result = $db -> getAll($sql);
		$result_submanager = $db -> getAll($sql_submanager);
		
		if($result || $result_submanager){
			$final_array = array();
			
			if($result){
				$final_array = $result;
			}
			
			if($result_submanager){
				foreach($result_submanager as $r){
					array_push($final_array, $r);
				}
			}
			
			return $final_array;
		}else{
			return FALSE;
		}
	}
	
	public static function getAccountingDisplacedInvoicePaymentInformation($db, $serial_mbc, $date_from, $date_to, 
															$serial_dea = NULL, $serial_bra = NULL){
		if($serial_dea){
			$dealer_filter = " AND dea.serial_dea = $serial_dea";
		}
		
		if($serial_bra){
			$branch_filter = " AND br.serial_dea = $serial_bra";
		}
		
		$sql = "SELECT dis.main_id, dis.name_dea, 
						SUM(dis.total_inv) AS 'total_inv', 
						dis.type_payment
				FROM (
						SELECT  dea.serial_dea AS 'main_id', dea.name_dea AS 'name_dea', 
								(total_payed_pay) AS 'total_inv', 
								IF(due_date_inv > date_pay, 'IN', 'DUE') AS 'type_payment'
						FROM sales sal
						JOIN counter AS cnt ON cnt.serial_cnt = sal.serial_cnt
						JOIN dealer br ON cnt.serial_dea=br.serial_dea $branch_filter
						JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea AND dea.manager_rights_dea = 'NO' $dealer_filter
						JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc AND mbc.serial_man = $serial_mbc
						JOIN invoice AS i ON i.serial_inv = sal.serial_inv
						JOIN payments pay ON pay.serial_pay=i.serial_pay
						WHERE i.status_inv = 'PAID'
						AND STR_TO_DATE(DATE_FORMAT(date_pay, '%d/%m/%Y'), '%d/%m/%Y') >= 
														STR_TO_DATE('$date_from', '%d/%m/%Y')
						AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') <= 
								STR_TO_DATE('$date_to', '%d/%m/%Y')
						GROUP BY pay.serial_pay ) AS dis
				GROUP BY main_id

				UNION

				SELECT br.serial_dea AS 'main_id', br.name_dea AS 'name_dea', 
						SUM(i.total_inv) AS 'total_inv', IF(due_date_inv > date_pay, 'IN', 'DUE') AS 'type_payment'
				FROM sales sal
				JOIN counter AS cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer br ON cnt.serial_dea=br.serial_dea $branch_filter
				JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea AND dea.manager_rights_dea = 'YES' $dealer_filter
				JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc AND mbc.serial_man = $serial_mbc
				JOIN invoice AS i ON i.serial_inv = sal.serial_inv
				JOIN payments pay ON pay.serial_pay=i.serial_pay
				WHERE i.status_inv = 'PAID'
				AND STR_TO_DATE(DATE_FORMAT(date_pay, '%d/%m/%Y'), '%d/%m/%Y') >= 
												STR_TO_DATE('$date_from', '%d/%m/%Y')
				AND STR_TO_DATE(DATE_FORMAT(date_inv, '%d/%m/%Y'), '%d/%m/%Y') <= 
						STR_TO_DATE('$date_to', '%d/%m/%Y')
				GROUP BY main_id";
		
		//Debug::print_r($sql); die;
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getDetailsForPayment($db, $serial_pay){
		$sql = "SELECT pd.*, cn.number_cn, IFNULL(cn.serial_inv, s.serial_inv) AS 'invoice',
						cn.serial_ref
				FROM payment_detail pd
				LEFT JOIN credit_note cn ON cn.serial_cn = pd.serial_cn
				LEFT JOIN sales s ON s.serial_sal = cn.serial_sal
				WHERE pd.serial_pay = $serial_pay";
		
		//echo $sql;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getInvoicesInPayment($db, $serial_pay){
		$sql = "SELECT i.*
				FROM invoice i
				WHERE serial_pay = $serial_pay";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function registerEfectivePaymentDate($db, $serial_pay, $full_date_pay = NULL){
		$full_pay_time = "CURRENT_TIMESTAMP()";
		
		if($full_date_pay) $full_pay_time = "'".$full_date_pay."'";
		
		$sql = "UPDATE payments SET 
					full_date_pay = $full_pay_time
				WHERE serial_pay = $serial_pay";
		
		$result = $db ->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'FAILED COMMIT FULL DATE PAYMENT', $sql);
			return FALSE;
		}
	}

	//GETTERS
    function getSerial_pay(){
        return $this->serial_pay;
    }
    function getTotal_to_pay_pay(){
        return  $this->total_to_pay_pay;
    }
    function getTotal_payed_pay(){
        return $this->total_payed_pay;
    }
    function getStatus_pay(){
        return $this->status_pay;
    }
    function getDate_pay(){
        return $this->date_pay;
    }
    function getObservation_pay(){
        return $this->observation_pay;
    }
    function getExcess_amount_available_pay(){
        return $this->excess_amount_available_pay;
    }

    //SETTERS
    function setSerial_pay($serial_pay){
        $this->serial_pay = $serial_pay;
    }
    function setTotal_to_pay_pay($total_to_pay_pay){
        $this->total_to_pay_pay = $total_to_pay_pay;
    }
    function setTotal_payed_pay($total_payed_pay){
        $this->total_payed_pay = $total_payed_pay;
    }
    function setStatus_pay($status_pay){
        $this->status_pay = $status_pay;
    }
    function setDate_pay($date_pay){
        $this->date_pay = $date_pay;
    }
    function setObservation_pay($observation_pay){
        $this->observation_pay = $observation_pay;
    }
    function setExcess_amount_available_pay($excess_amount_available_pay){
        $this->excess_amount_available_pay = $excess_amount_available_pay;
    }
	
	public function getFull_date_pay() {
		return $this->full_date_pay;
	}

	public function setFull_date_pay($full_date_pay) {
		$this->full_date_pay = $full_date_pay;
	}

	public function getErp_id() {
		return $this->erp_id;
	}

	public function setErp_id($erp_id) {
		$this->erp_id = $erp_id;
	}

}
?>