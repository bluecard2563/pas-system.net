<?php
/*
File: Service.class.php
Author: Edwin Salvador
Creation Date: 11/02/2010
Last Modified:04/03/2010
Modified By:Nicolas Flores
*/

class Service{
    var $db;
    var $serial_ser;
    var $status_ser;
    var $fee_type_ser;

    function __construct($db, $serial_ser=NULL, $status_ser=NULL, $fee_type_ser=NULL ){
            $this -> db = $db;
            $this -> serial_ser = $serial_ser;
            $this -> status_ser = $status_ser;
            $this -> fee_type_ser = $fee_type_ser;
    }

    /***********************************************
    * function getData
    * sets data by serial_ser
    ***********************************************/
    function getData(){
        if($this->serial_ser!=NULL){
            $sql = "SELECT serial_ser, status_ser,fee_type_ser
                    FROM services
                    WHERE serial_ser='".$this->serial_ser."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_ser =$result->fields[0];
                $this -> status_ser = $result->fields[1];
                $this -> fee_type_ser = $result->fields[2];

                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


    function getInfoService($code_lang){
        if($this->serial_ser!=NULL){
			$sql = 	"SELECT  ser.serial_ser,
							 sbl.name_sbl,
							 sbl.description_sbl
					FROM services ser
					JOIN services_by_language sbl ON ser.serial_ser= sbl.serial_ser
					JOIN language lang ON lang.serial_lang = sbl.serial_lang AND lang.code_lang='".$code_lang."'
					WHERE ser.serial_ser='".$this -> serial_ser."'";

					$result = $this-> db -> Execute($sql);
					$num = $result -> RecordCount();
					if($num > 0){
									$array=array();
									$count=0;
									do{
													$array[$count]=$result->GetRowAssoc(false);
													$result->MoveNext();
													$count++;
									}while($count<$num);
					}
					return $array;
            }else
				return false;
    }




    /***********************************************
    * funcion getServices
    * gets all the Services of the system
    ***********************************************/
    function getServices($code_lang){
        $lista=NULL;
        $sql = 	"SELECT s.serial_ser, sbl.description_sbl, sbl.name_sbl
                 FROM services s
                 JOIN services_by_language sbl ON sbl.serial_ser = s.serial_ser
                 JOIN language l ON l.serial_lang = sbl.serial_lang AND l.code_lang='".$code_lang."'";

        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();

		if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;

            }while($cont<$num);
			return $arr;
        }else{
			return false;
		}        
    }

    /**
    @Name: getServicesAutocompleter
    @Description: Returns an array of services.
    @Params: Service name or pattern.
    @Returns: Service array
    **/
    function getServicesAutocompleter($name_sbl, $serial_lang){
        $sql = "SELECT s.serial_ser, sbl.serial_sbl, sbl.name_sbl
                FROM services s
                JOIN services_by_language sbl ON s.serial_ser = sbl.serial_ser AND sbl.serial_lang = '".$serial_lang."'
                WHERE LOWER(name_sbl) LIKE _utf8'%".utf8_encode($name_sbl)."%' collate utf8_bin
                LIMIT 10";

        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "INSERT INTO services (
            			serial_ser,
				status_ser,
						fee_type_ser)
                VALUES (
                    NULL,
                    '".$this->status_ser."',
                    '".$this->fee_type_ser."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "UPDATE services SET
                status_ser ='".$this->status_ser."',
                fee_type_ser ='".$this->fee_type_ser."'
                WHERE serial_ser = '".$this->serial_ser."'";
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true){
            return true;
        }else{
            return false;
        }
    }
    /**
	@Name: getStatusList
	@Description: Gets all status in DB for services.
	@Params: N/A
	@Returns: Status array
	**/
	function getStatusList(){
		$sql = "SHOW COLUMNS FROM services LIKE 'status_ser'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

    ///GETTERS
    function getSerial_ser(){
        return $this->serial_ser;
    }
    function getStatus_ser(){
        return $this->status_ser;
    }
    function getFee_type_ser(){
        return $this->fee_type_ser;
    }


    ///SETTERS
    function setSerial_ser($serial_ser){
        $this->serial_ser = $serial_ser;
    }
    function setStatus_ser($status_ser){
        $this->status_ser = $status_ser;
    }
    function setFee_type_ser($fee_type_ser){
        $this->fee_type_ser = $fee_type_ser;
    }


}
?>