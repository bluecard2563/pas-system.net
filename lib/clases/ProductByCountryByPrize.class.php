<?php
/*
File: ProductByCountryByPrize.class.php
Author: Santiago Benitez
Creation Date: 04/03/2010 12:03
Last Modified: 04/03/2010
Modified By: Santiago Benitez
*/
class ProductByCountryByPrize{
	var $db;
	var $serial_pbp;
	var $serial_pxc;


	function __construct($db, $serial_pbp=NULL, $serial_pxc=NULL){

		$this -> db = $db;
		$this -> serial_pbp=$serial_pbp;
		$this -> serial_pxc=$serial_pxc;
	}

	function getProductsByCountryByPrize($serial_pbp){
		$lista=NULL;
		$sql = 	"SELECT pbd.serial_pbp, pbd.serial_pxc, d.name_pxc, cou.name_cou, d.id_pxc
                         FROM product_by_country_by_prize pbd
                         JOIN dealer d ON d.serial_pxc=pbd.serial_pxc
                         JOIN sector s ON s.serial_sec=d.serial_sec
                         JOIN city c ON c.serial_cit=s.serial_cit
                         JOIN country cou ON cou.serial_cou=c.serial_cou
                         WHERE pbd.serial_pbp = ".$serial_pbp;

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

        function getProductsByPromo($serial_pbp, $serial_lang){
		$lista=NULL;
		$sql = 	"SELECT pbl.serial_pbl, pbl.name_pbl
                         FROM product_by_country_by_prize pbd
                         JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
                         JOIN product pro ON pro.serial_pro=pxc.serial_pro
                         JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro
                         AND pbl.serial_lang=".$serial_lang."
                         WHERE pbd.serial_pbp = ".$serial_pbp;
                //echo $sql;

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function insert() {
            $sql = "
                INSERT INTO product_by_country_by_prize (
                    serial_pbp,
                    serial_pxc)
                VALUES (
                    '".$this->serial_pbp."',
                    '".$this->serial_pxc."')";
            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
        function update() {
            $sql = "UPDATE product_by_country_by_prize SET
                        serial_pxc='".$this->serial_pxc."',
                WHERE serial_pbp = '".$this->serial_pbp."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

        /**
	* @Name: delete
	* @Description: Deletes some registers in DB
	* @Params: $serial_pbp: serial of the promotion.
	* @Returns: TRUE if OK. / FALSE on error
	**/
        function delete($serial_pbp) {
            $sql = "DELETE FROM product_by_country_by_prize
                    WHERE serial_pbp = '".$serial_pbp."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

       	//Getters
	function getSerial_pbp(){
		return $this->serial_pbp;
	}
	function getSerial_pxc(){
		return $this->serial_pxc;
	}


	//Setters
	function setSerial_pbp($serial_pbp){
		$this->serial_pbp=$serial_pbp;
	}
	function setSerial_pxc($serial_pxc){
		$this->serial_pxc=$serial_pxc;
	}

}
?>