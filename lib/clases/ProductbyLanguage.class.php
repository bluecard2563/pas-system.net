<?php
/*
File: ProductByLanguage.php
Author: Santiago Borja
Creation Date: 28/12/2009 11:48
Last Modified: 23/02/2010
Modified By: Santiago Borja
*/

class ProductbyLanguage{
	var $db;
	var $serial_pbl;
	var $serial_lang;
	var $serial_pro;
	var $name_pbl;
	var $description_pbl;

	function __construct($db, $serial_pbl=NULL, $serial_lang=NULL, $serial_pro=NULL, $name_pbl=NULL, $description_pbl=NULL ){
	
		$this -> db = $db;
		$this -> serial_pbl = $serial_pbl;
		$this -> serial_lang = $serial_lang;
		$this -> serial_pro = $serial_pro;
		$this -> name_pbl = $name_pbl;
		$this -> description_pbl = $description_pbl;
		
	}
	
	/***********************************************
    * function getData
    * gets data by serial_pbl
    ***********************************************/
	function getData(){
		if($this->serial_pbl!=NULL){
			$sql = 	"SELECT serial_pbl, serial_lang, serial_pro, name_pbl, description_pbl
					FROM ptype_by_language
					WHERE serial_pbl='".$this->serial_pbl."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_pbl =$result->fields[0];
				$this -> serial_lang = $result->fields[1];
				$this -> serial_pro =$result->fields[2];
				$this -> name_pbl = $result->fields[3];
				$this -> description_pbl = $result->fields[4];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * function getDataByProductAndLanguage
    * Gets the data based on the product id and the language
    ***********************************************/
	function getDataByProductAndLanguage(){
		$sql = 	"SELECT serial_pbl, name_pbl, description_pbl
				FROM product_by_language
				WHERE serial_pro=".$this -> serial_pro." AND serial_lang = ".$this -> serial_lang;
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false) return false;

		if($result->fields[0]){
			$this -> serial_pbl =$result->fields[0];
			$this -> name_pbl = $result->fields[1];
			$this -> description_pbl = $result->fields[2];
			return true;
		}		
	}
	
/***********************************************
    * funcion getMyBenefits
    * gets all the Benefits of this product
    ***********************************************/
	function getMyBenefits(){
		$sql = 	"SELECT b.serial_ben, bl.description_bbl, bp.price_bxp
				FROM benefits b, benefits_by_language bl, benefits_product bp
				WHERE b.serial_ben = bl.serial_ben 
				AND b.serial_ben = bp.serial_ben
				AND bp.serial_pro  = ".$this -> serial_pro."
				AND bl.serial_lang = ".$this -> serial_lang."
				AND b.status_ben = 'ACTIVE'
				ORDER BY b.serial_ben";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$count=0;
			do{
				$array[$count]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$count++;
			}while($count<$num);
		}
		return $array;
	}
	
	/***********************************************
    * funcion existProdutTypeName
    * verifies if a existProdutTypeName exists or not
    ***********************************************/
	function existProductName(){
		$sql = 	"SELECT  serial_pro
				 FROM product_by_language
				 WHERE LOWER(name_pbl)= '".utf8_encode($this->name_pbl)."'";
		if($this->serial_pro!=NULL){
			$sql.=" AND serial_pro <> ".$this->serial_pro;
		}
//              echo $sql;
		$result = $this->db -> Execute($sql);
                
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO product_by_language (
				serial_pbl,
				serial_lang, 
				serial_pro,
				name_pbl,
				description_pbl)
            VALUES (
                NULL,
				'".$this->serial_lang."',				
				'".$this->serial_pro."',
				'".$this->name_pbl."',
				'".$this->description_pbl."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE product_by_language SET
			name_pbl ='".$this->name_pbl."',
			description_pbl ='".$this->description_pbl."'
		WHERE 	serial_pro = '".$this->serial_pro."' and  serial_lang = '".$this->serial_lang."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true){
            return true;
        }
        else{
            return false;
        }
    }

    function updateBySerial_pbl() {
        $sql = "
                UPDATE product_by_language SET
			name_pbl ='".$this->name_pbl."',
			description_pbl ='".$this->description_pbl."'
		WHERE 	serial_pbl = '".$this->serial_pbl."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true){
            return true;
        }
        else{
            return false;
        }
    }
    
    /***********************************************
    * function 
    ***********************************************/
	public static function getNamepbl($db,$serial_pro){
		$sql = 	"SELECT name_pbl
				FROM product_by_language
				WHERE serial_pro=".$serial_pro." AND serial_lang = 2";
		//echo $sql;
		$result = $db->getOne($sql);;
        if ($result) {
            return $result;
        } else {
            return false;
        }
		
	}


	///GETTERS
	function getSerial_pbl(){
		return $this->serial_pbl;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}	
	function getSerial_pro(){
		return $this->serial_pro;
	}
	function getName_pbl(){
		return $this->name_pbl;
	}
	function getDescription_pbl(){
		return $this->description_pbl;
	}	
	

	///SETTERS	
	
	function setSerial_pbl($serial_pbl){
		$this->serial_pbl = $serial_pbl;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setSerial_pro($serial_pro){
		$this->serial_pro = $serial_pro;
	}
	function setName_pbl($name_pbl){
		$this->name_pbl = $name_pbl;
	}
	function setDescription_pbl($description_pbl){
		$this->description_pbl = $description_pbl;
	}	
}
?>