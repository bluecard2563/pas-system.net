<?php

/**
 * File: Settlement
 * Author: Patricio Astudillo
 * Creation Date: 14/04/2014
 * Last Modified: 14/04/2014
 * Modified By: Patricio Astudillo
*/

class Settlement{
	var $db;
	var $id;
	var $creator;
	var $number;
	var $creation_date;
	var $total_requested;
	var $total_paid;
	var $total_refunded;
	
	function __construct($db, $id = NULL) {
		$this->db = $db;
		$this->id = $id;
		
		$this->load();
	}
	
	public function load(){
		$sql = "SELECT * FROM settlement WHERE id = $this->id";
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$this -> creator = $result['creator'];
			$this -> number = $result['number'];
			$this -> creation_date = $result['creation_date'];
			$this -> total_requested = $result['total_requested'];
			$this -> total_paid = $result['total_paid'];
			$this -> total_refunded = $result['total_refunded'];
			
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function save(){
		if($this->id){
			$sql = "UPDATE settlement SET"
					. "creator = $this->creator,"
					. "number = $this->number,"
					. "creation_date = STR_TO_DATE('$this->creation_date', '%d/%m/%Y'),"
					. "total_requested = $this->total_requested,"
					. "total_paid = $this->total_paid,"
					. "total_refunded = $this->total_refunded,"
					. " WHERE id = $this->id";
		}else{
			$sql = "INSERT INTO settlement (
						id,
						creator,
						number,
						creation_date,
						total_requested,
						total_paid,
						total_refunded
					) VALUES (
						NULL,
						{$this -> creator},
						{$this -> number},
						CURRENT_TIMESTAMP(),
						{$this -> total_requested},
						{$this -> total_paid},
						{$this -> total_refunded}
					)";
		}
		
		$result = $this->db->Execute($sql);
		
		if($result){
			if(!$this->id):
				$this->id = $this->db->insert_ID();
			endif;
			
			return $this->load();
		}else{
			ErrorLog::log($this -> db, 'SAVE FAILED - '.get_class($this), $sql);
			return FALSE;
		}
	}
	
	public static function saveSettlementDetails($db, $settlement_id, $requests_array){
		foreach($requests_array as $request_line){
			$sql = "INSERT INTO settlement_detail (
						settlement_id,
						serial_prq
					) VALUES (
						$settlement_id,
						{$request_line}
					)";
						
			$result = $db->Execute($sql);
		
			if(!$result){
				ErrorLog::log($db, 'LOG SETTLEMENT DETAILS FAILED', $sql);
				break;
			}
		}
		
		return $result;
	}
	
	public function getId() {
		return $this->id;
	}

	public function getCreator() {
		return $this->creator;
	}

	public function getNumber() {
		return $this->number;
	}

	public function getCreation_date() {
		return $this->creation_date;
	}

	public function getTotal_requested() {
		return $this->total_requested;
	}

	public function getTotal_paid() {
		return $this->total_paid;
	}

	public function getTotal_refunded() {
		return $this->total_refunded;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setCreator($creator) {
		$this->creator = $creator;
	}

	public function setNumber($number) {
		$this->number = $number;
	}

	public function setCreation_date($creation_date) {
		$this->creation_date = $creation_date;
	}

	public function setTotal_requested($total_requested) {
		$this->total_requested = $total_requested;
	}

	public function setTotal_paid($total_paid) {
		$this->total_paid = $total_paid;
	}

	public function setTotal_refunded($total_refunded) {
		$this->total_refunded = $total_refunded;
	}
}