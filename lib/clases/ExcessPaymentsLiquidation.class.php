<?php
/*
File: ExcessPaymentsLiquidation.class
Author: David Bergmann
Creation Date: 07/06/2010
Last Modified:
Modified By:
*/

class ExcessPaymentsLiquidation {
    var $db;
    var $serial_xpl;
    var $serial_pay;
    var $serial_usr;
    var $payment_date_xpl;
    var $amount_paid_xpl;

    function __construct($db, $serial_xpl = NULL, $serial_pay = NULL, $serial_usr = NULL,
                         $payment_date_xpl = NULL, $amount_paid_xpl = NULL){
            $this -> db = $db;
            $this -> serial_xpl = $serial_xpl;
            $this -> serial_pay = $serial_pay;
            $this -> serial_usr = $serial_usr;
            $this -> payment_date_xpl = $payment_date_xpl;
            $this -> amount_paid_xpl = $amount_paid_xpl;
    }

    /***********************************************
    * function getData
    * gets data by serial_xpl
    ***********************************************/
    function getData(){
        if($this->serial_xpl!=NULL){
            $sql = "SELECT serial_xpl, serial_pay, serial_usr, payment_date_xpl,
                           amount_paid_xpl
                    FROM excess_payments_liquidation
                    WHERE serial_xpl='".$this->serial_xpl."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_xpl=$result->fields[0];
                $this->serial_pay=$result->fields[1];
                $this->serial_usr=$result->fields[2];
                $this->payment_date_xpl=$result->fields[3];
                $this->serial_zon=$result->fields[4];
                $this->amount_paid_xpl=$result->fields[5];
                return true;
            }
            else
                return false;
        }else
            return false;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert(){
        $sql="INSERT INTO excess_payments_liquidation (
                                                       serial_xpl,
                                                       serial_pay,
                                                       serial_usr,
                                                       payment_date_xpl,
                                                       amount_paid_xpl
                                                       )
              VALUES(NULL,
                    '".$this->serial_pay."',
                    '".$this->serial_usr."',
                    NOW(),
                    '".$this->amount_paid_xpl."'
                    );";
                    //die($sql);
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }


    ///GETTERS
    function getSerial_xpl(){
            return $this->serial_xpl;
    }
    function getSerial_pay(){
            return $this->serial_pay;
    }
    function getSerial_usr(){
            return $this->serial_usr;
    }
    function getPayment_date_xpl(){
            return $this->payment_date_xpl;
    }
    function getAmount_paid_xpl(){
            return $this->amount_paid_xpl;
    }


    ///SETTERS
    function setSerial_xpl($serial_xpl){
            $this->serial_xpl = $serial_xpl;
    }
    function setSerial_pay($serial_pay){
            $this->serial_pay = $serial_pay;
    }
    function setSerial_usr($serial_usr){
            $this->serial_usr = $serial_usr;
    }
    function setPayment_date_xpl($payment_date_xpl){
            $this->payment_date_xpl = $payment_date_xpl;
    }
    function setAmount_paid_xpl($amount_paid_xpl){
            $this->amount_paid_xpl = $amount_paid_xpl;
    }
}
?>