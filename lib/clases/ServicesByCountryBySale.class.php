<?php
/*
File:ServicesByCountryBySale.class.php
Author: Esteban Angulo
Creation Date: 09/03/2010
Last Modified: 21/06/2010
Modified By: David Bergmann
*/
class ServicesByCountryBySale{
    var $db;
    var $serial_sbd;
    var $serial_sal;


    function __construct($db, $serial_sbd=NULL, $serial_sal = NULL){
        $this -> db = $db;
        $this -> serial_sbd = $serial_sbd;
        $this -> serial_sal= $serial_sal;
    }

    /***********************************************
    * function insert
    * inserts a new register en DB
    ***********************************************/
    function insert() {
        $sql = "INSERT INTO services_by_country_by_sale(serial_sbd,serial_sal)
                VALUES('".$this->serial_sbd."',
                       '".$this->serial_sal."')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /**
    @Name: getServicesBySale
    @Description: returns all services assosiated to a dealer
    @Params: * serial_dea: Dealer's serial
             * serial_sal: Serial of the sale
    @Returns: an array with all the information
    **/
	function getServicesBySale($serial_sal, $serial_dea) {
		$sql = "SELECT sbcbs.serial_sbd, sbd.serial_sbc, sbl.name_sbl, s.fee_type_ser, sbc.price_sbc,
                 sbc.cost_sbc, sbc.coverage_sbc
                 FROM services_by_country_by_sale sbcbs
                 JOIN services_by_dealer sbd ON sbcbs.serial_sbd=sbd.serial_sbd
                 JOIN services_by_country sbc ON sbd.serial_sbc=sbc.serial_sbc
                 JOIN services_by_language sbl ON sbl.serial_ser=sbc.serial_ser AND sbl.serial_lang = {$_SESSION['serial_lang']}
                 JOIN services s ON s.serial_ser=sbl.serial_ser
		 WHERE sbcbs.serial_sal='" . $serial_sal . "'";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}
	
	public static function deleteServicesBySale($db, $serial_sal) {
        $sql = "DELETE FROM services_by_country_by_sale
				WHERE serial_sal='".$serial_sal."'";

        $result = $db->Execute($sql);
        
		return $result;
    }
	
	public static function getFullServiceData($db, $serial_sbd){
		$sql = "SELECT sbd.serial_sbd, sbd.serial_sbc, sbl.name_sbl, s.fee_type_ser, sbc.price_sbc,
                 sbc.cost_sbc, sbc.coverage_sbc
                 FROM services_by_dealer sbd 
                 JOIN services_by_country sbc ON sbd.serial_sbc=sbc.serial_sbc
                 JOIN services_by_language sbl ON sbl.serial_ser=sbc.serial_ser AND sbl.serial_lang = {$_SESSION['serial_lang']}
                 JOIN services s ON s.serial_ser=sbl.serial_ser
		 WHERE sbd.serial_sbd='" . $serial_sbd . "'";
		
		//Debug::print_r($sql); die;
		
		return $db->getRow($sql);
	}
	
	public static function getFullServiceSalesData($db, $serial_sal){
		$sql = "SELECT s.serial_sal, cnt.serial_dea
				FROM services_by_country_by_sale scs
				JOIN sales s ON s.serial_sal = scs.serial_sal AND s.serial_sal = $serial_sal
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}


    //SETTERS
    function setSerial_sbd($serial_sbd){
        $this->serial_sbd = $serial_sbd;
    }
    function setSerial_sal($serial_sal){
        $this->serial_sal = $serial_sal;
    }

    //GETTERS
    function getSerial_sbd(){
        return $this->serial_sbd;
    }
    function getSerial_sal(){
        return $this->serial_sbd;
    }
}
?>
