<?php
/*
File: Currency.class.php
Author: Pablo Puente
Creation Date: 28/12/2009 11:48
Last Modified: 
Modified By: 
*/
class Currency {				
	var $db;
	var $serial_cur;
	var $name_cur;
	var $exchange_fee_cur;
	var $symbol_cur;
	var $status_cur;
	
	function __construct($db, $serial_cur = NULL, $name_cur = NULL, $exchange_fee_cur = NULL, $symbol_cur=NULL, $status_cur = NULL){
		$this -> db = $db;
		$this -> serial_cur = $serial_cur;
		$this -> name_cur = $name_cur;
		$this -> exchange_fee_cur = $exchange_fee_cur;
		$this -> symbol_cur = $symbol_cur;
		$this -> status_cur = $status_cur;
	}
	
	/***********************************************
    * funcion getAllStatus
    * Returns all kind of Status which exist in DB
    ***********************************************/
	function getAllStatus(){
		// Finds all possible status form ENUM column 
		$sql = "SHOW COLUMNS FROM currency LIKE 'status_cur'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
    * function getData
    * gets data by serial_cur
    ***********************************************/
	function getData(){
		if($this->serial_cur!=NULL){
			$sql = 	"SELECT serial_cur, name_cur, exchange_fee_cur, symbol_cur, status_cur
					FROM currency
					WHERE serial_cur='".$this->serial_cur."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_cur=$result->fields[0];
				$this->name_cur=$result->fields[1];	
				$this->exchange_fee_cur=$result->fields[2];
				$this->symbol_cur=$result->fields[3];
				$this->status_cur=$result->fields[4];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * function getCurrencyName
    * gets data by name
    ***********************************************/
	function getCurrencyName(){
		if($this->name_cur!=NULL){
			$sql = 	"SELECT serial_cur, name_cur, exchange_fee_cur
					FROM currency
					WHERE name_cur='".$this->name_cur."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_cur=$result->fields[0];
				$this->name_cur=$result->fields[1];		
				$this->exchange_fee_cur=$result->fields[1];	
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
	function insert(){
		$sql="INSERT INTO currency (
					serial_cur, 
					name_cur,
					exchange_fee_cur,
					symbol_cur
					)
			  VALUES(NULL,
					'".$this->name_cur."',
					'".$this->exchange_fee_cur."',
					'".$this->symbol_cur."'
					);";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
	function update(){
		$sql="UPDATE currency 
		SET name_cur='".$this->name_cur."',
			exchange_fee_cur='".$this->exchange_fee_cur."',
			symbol_cur='".$this->symbol_cur."',
			status_cur='".$this->status_cur."'
		WHERE serial_cur='".$this->serial_cur."'";
					
		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
			return true;
		else
			return false;
	}

	/***********************************************
    * funcion getCurrency
    * gets all the Currencies of the system
    ***********************************************/
	function getCurrency(){
		$list=NULL;
		$sql = 	"SELECT serial_cur, name_cur, exchange_fee_cur, symbol_cur
				 FROM currency";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}	
	
	/** 
	@Name: getCurrenciesAutocompleter
	@Description: Returns an array of currencies.
	@Params: Currency name or pattern.
	@Returns: Currency array
	**/
	function getCurrenciesAutocompleter($name_cur){
		$sql = "SELECT c.serial_cur, c.name_cur
				FROM currency c
				WHERE LOWER(c.name_cur) LIKE _utf8'%".utf8_encode($name_cur)."%' collate utf8_bin
				LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

	/***********************************************
    * funcion currencyExist
    * verifies if a Currency exists or not
    ***********************************************/
	function currencyExist($txtNameCurrency, $serial_cur = NULL){
		if($txtNameCurrency){
			$sql = 	"SELECT serial_cur
					 FROM currency
					 WHERE LOWER(name_cur)= _utf8'".utf8_encode($txtNameCurrency)."' collate utf8_bin";

			if($serial_cur != NULL){
				$sql.= " AND serial_cur <> ".$serial_cur;
			}
			$result = $this->db -> Execute($sql);
	
			if($result->fields[0]){
				return $result->fields[0];//ya existe la moneda
			}else{
				return false;
			}
		}
	}
	
	/***********************************************
    * funcion currencySymbolExist
    * verifies if a Currency Symbol exists or not
    ***********************************************/
	function currencySymbolExist($txtSymbol_cur, $serial_cur = NULL){
		if($txtSymbol_cur){
			$sql = 	"SELECT serial_cur
					 FROM currency
					 WHERE LOWER(symbol_cur)= _utf8'".utf8_encode($txtSymbol_cur)."' collate utf8_bin";

			if($serial_cur != NULL){
				$sql.= " AND serial_cur <> ".$serial_cur;
			} 
			$result = $this->db -> Execute($sql);
	
			if($result->fields[0]){
				return true;//ya existe la moneda
			}else{
				return false;
			}
		}
	}
	
	///GETTERS
	function getSerial_cur(){
		return $this->serial_cur;
	}
	function getName_cur(){
		return $this->name_cur;
	}
	function getExchange_fee_cur(){
		return $this->exchange_fee_cur;
	}
	function getSymbol_cur(){
		return $this->symbol_cur;
	}
	function getStatus_cur(){
		return $this->status_cur;
	}

	///SETTERS	
	function setSerial_cur($serial_cur){
		$this->serial_cur = $serial_cur;
	}
	function setName_cur($name_cur){
		$this->name_cur = $name_cur;
	}
	function setExchange_fee_cur($exchange_fee_cur){
		$this->exchange_fee_cur = $exchange_fee_cur;
	}
	function setSymbol_cur($symbol_cur){
		$this->symbol_cur = $symbol_cur;
	}
	function setStatus_cur($status_cur){
		$this->status_cur = $status_cur;
	}
}
?>
