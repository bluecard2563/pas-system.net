<?php
/*
File: AppliedComission.class.php
Author: Miguel Ponce
Creation Date: 13/04/2010
*/

class AppliedBonus{
	var $db;
	var $serial_apb;
	var $serial_sal;
	var $total_amount_apb;
	var $pending_amount_apb;
	var $on_date_apb;
	var $status_apb;
	var $bonus_to_apb;
	var $authorized_apb;
	var $payment_date_apb;
	var $authorized_date_apb;
	var $observations_apb;

	function __construct(	$db,
							$serial_apb=NULL,
							$serial_sal=NULL,
							$total_amount_apb=NULL,
							$pending_amount_apb=NULL,
							$on_date_apb=NULL,
							$status_apb=NULL,
							$bonus_to_apb=NULL,
							$authorized_apb=NULL,
							$payment_date_apb=NULL,
							$authorized_date_apb=NULL,
							$observations_apb=NULL
						)
	{
		$this -> db = $db;
		$this -> serial_apb = $serial_apb;
		$this -> serial_sal = $serial_sal;
		$this -> total_amount_apb = $total_amount_apb;
		$this -> pending_amount_apb = $pending_amount_apb;
		$this -> on_date_apb = $on_date_apb;
		$this -> status_apb = $status_apb;
		$this -> bonus_to_apb = $bonus_to_apb;
		$this -> authorized_apb = $authorized_apb;
		$this -> payment_date_apb = $payment_date_apb;
		$this -> authorized_date_apb = $authorized_date_apb;
		$this -> observations_apb = $observations_apb;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_apb
    ***********************************************/
	function getData(){
		if($this->serial_apb!=NULL){
			$sql = 	"SELECT
				serial_apb,
				serial_sal,
				total_amount_apb,
				pending_amount_apb,
				ondate_apb,
				status_apb,
				bonus_to_apb,
				authorized_apb,
				payment_date_apb,
				authorized_date_apb,
				observations_apb
			FROM applied_bonus
			WHERE serial_apb='".$this->serial_apb."'";
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){

				$this -> serial_apb=$result->fields[0];
				$this -> serial_sal=$result->fields[1];
				$this -> total_amount_apb=$result->fields[2];
				$this -> pending_amount_apb=$result->fields[3];
				$this -> on_date_apb=$result->fields[4];
				$this -> status_apb=$result->fields[5];
				$this -> bonus_to_apb=$result->fields[6];
				$this -> authorized_apb=$result->fields[7];
				$this -> payment_date_apb=$result->fields[8];
				$this -> authorized_date_apb=$result->fields[9];
				$this -> observations_apb = $result->fields[10];

				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO applied_bonus (
				serial_apb,
				serial_sal,
				total_amount_apb,
				pending_amount_apb,
				ondate_apb,
				status_apb,
				bonus_to_apb,
				observations_apb
			)
            VALUES (
				NULL,
				'".$this -> serial_sal."',
				'".$this -> total_amount_apb."',
				'".$this -> pending_amount_apb."',
				'".$this -> on_date_apb."',
				'".$this -> status_apb."',
				'".$this -> bonus_to_apb."',
				NULL)";
		//die(Debug::print_r($sql));
		$result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK/ FALSE on error
	**/
    function update() {
        $sql = "UPDATE applied_bonus SET
                    serial_sal=".$this->serial_sal.",
                    total_amount_apb=".$this->total_amount_apb.",
                    pending_amount_apb='".$this->pending_amount_apb."',
					ondate_apb='".$this->on_date_apb."',
					status_apb='".$this->status_apb."',
					bonus_to_apb='".$this->bonus_to_apb."'";
			if($this->payment_date_apb){
				$sql.=",payment_date_apb='".$this->payment_date_apb."'";
			}
			if($this->observations_apb){
				$sql.=",observations_apb='".$this -> observations_apb."'";
			}
			
           $sql.= " WHERE serial_apb = '".$this->serial_apb."'";
            
        $result = $this->db->Execute($sql);
        if ($result === false)return false;
        if($result==true)
            return true;
        return false;
    }
	
	 /*
	  * @Name: hasPayedBonusBySale
	  * @Description: Returns the row of the bonus paid in this sale
	  * @Params: $serial_sal, $last_date_payed
	  * @Returns: the value paid
	  */
    public static function hasPayedBonusBySale($db, $serial_sal){
		$sql = "SELECT apb.serial_apb, apb.total_amount_apb
				FROM applied_bonus apb
				WHERE apb.serial_sal = '$serial_sal'
				AND status_apb='PAID'";

		$result = $db -> getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
    }


	 /*
	  * @Name: getOnDateBonus
	  * @Description: Returns the total bonus wich are on Date, by dealer or counter
	  * @Params: $serial: serial of the COUNTER or DEALER
	  *			 $bonus_to: who is going to be paid COUNTER or DEALER
	  * @Returns: the value paid
	  */
    public function getOnDateBonus($serial,$bonus_to){
		if(!serial || !$bonus_to){
			return false;
		}else{
			$sql = " SELECT apb.serial_apb, apb.total_amount_apb, apb.pending_amount_apb, apb.status_apb,
							apb.bonus_to_apb, apb.ondate_apb, s.card_number_sal, i.number_inv, apb.observations_apb,
							CONCAT(cus.first_name_cus,' ',cus.last_name_cus) as 'name_cus'
					FROM applied_bonus apb
					JOIN sales s ON apb.serial_sal=s.serial_sal
					JOIN invoice i ON s.serial_inv = i.serial_inv
					JOIN customer cus ON s.serial_cus=cus.serial_cus
					JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt";
			if($bonus_to=='DEALER'){
				$sql.=" JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
						WHERE cnt.serial_dea=$serial
						AND apb.bonus_to_apb='DEALER'";
			}else{
				$sql.=" WHERE cnt.serial_cnt=$serial
						AND apb.bonus_to_apb='COUNTER'";
			}
			
			$sql .="	AND apb.status_apb != 'PAID'
						AND apb.ondate_apb='YES'
						AND pending_amount_apb > 0
						ORDER BY s.card_number_sal";
		}
		//echo '<pre>'.$sql.'</pre>';
		$result = $this->db -> getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
    }

	 /*
	  * @Name: getOutDateBonus
	  * @Description: Returns the total bonus wich are out of Date, by dealer or counter
	  * @Params: $serial: serial of the COUNTER or DEALER
	  *			 $bonus_to: who is going to be paid COUNTER or DEALER
	  * @Returns: the value paid
	  */
    public function getOutDateBonus($serial,$bonus_to){
		if(!serial || !$bonus_to){
			return false;
		}else{
			$sql = " SELECT apb.serial_apb, apb.total_amount_apb, apb.pending_amount_apb, apb.status_apb,
							apb.bonus_to_apb, apb.ondate_apb, s.card_number_sal, i.number_inv, apb.observations_apb,
							CONCAT(cus.first_name_cus,' ',cus.last_name_cus) as 'name_cus'
					FROM applied_bonus apb
					JOIN sales s ON apb.serial_sal=s.serial_sal
					JOIN invoice i ON s.serial_inv = i.serial_inv
					JOIN customer cus ON s.serial_cus=cus.serial_cus
					JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt";
			if($bonus_to=='DEALER'){
				$sql.=" JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
						WHERE cnt.serial_dea=$serial
						AND apb.bonus_to_apb='DEALER'";
			}else{
				$sql.=" WHERE cnt.serial_cnt=$serial
						AND apb.bonus_to_apb='COUNTER'";
			}
			
			$sql.=" AND apb.status_apb != 'PAID'
					AND apb.ondate_apb='NO'
					AND apb.authorized_apb='YES'
					AND pending_amount_apb > 0
					ORDER BY s.card_number_sal";
		}
		
		//Debug::print_r($sql);
		$result = $this->db -> getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
    }

	/*
	  * @Name: getOutDateBonus
	  * @Description: Returns the total bonus wich are out of Date, by dealer or counter
	  * @Params: $serial: serial of the DEALER
	  *			 $bonus_to: who is going to be paid COUNTER or DEALER
	  * @Returns: the value paid
	  */
    public function getOutDateBonusToAuthorize($serial,$bonus_to){
		if(!$serial || !$bonus_to){
			return false;
		}else{
			if($bonus_to=='DEALER'){
				$pay_to = ",b.name_dea as pay_to";
				$where_statement = " apb.bonus_to_apb='DEALER'";
			}else{
				$pay_to = ",CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as pay_to";
				$join_counter = " JOIN user usr ON cnt.serial_usr=usr.serial_usr";
				$where_statement = " apb.bonus_to_apb='COUNTER'";
			}			

			$sql = "	SELECT s.card_number_sal, apb.serial_apb, apb.total_amount_apb, apb.pending_amount_apb,
							apb.status_apb, apb.bonus_to_apb, apb.ondate_apb, apb.observations_apb, b.name_dea,
							DATE_FORMAT(due_date_inv, '%d/%m/%Y') AS 'due_date_inv', 
							DATE_FORMAT(MAX(date_pyf), '%d/%m/%Y') AS 'date_pyf'
							$pay_to
						FROM applied_bonus apb
						JOIN sales s ON apb.serial_sal=s.serial_sal
						JOIN invoice i ON i.serial_inv = s.serial_inv
						JOIN payment_detail pdt ON pdt.serial_pay = i.serial_pay
						JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
						JOIN dealer b ON b.serial_dea = cnt.serial_dea
						JOIN sector sec On sec.serial_sec = b.serial_sec
						$join_counter
						WHERE $where_statement
						AND sec.serial_cit=$serial
						AND apb.status_apb IN ('ACTIVE', 'REFUNDED')
						AND apb.ondate_apb='NO'
						AND apb.authorized_apb IS NULL
						GROUP BY apb.serial_apb
						ORDER BY b.name_dea, pay_to, s.card_number_sal";
		}
		
		//Debug::print_r($sql);
		$result = $this->db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
    }

/*
	  * @Name: authorizeBonus
	  * @Description: function to authorize or deny an out date bonus
	  * @Params: $serial: serial of the DEALER
	  *			 $bonus_to: who is going to be paid COUNTER or DEALER
	  * @Returns: the value paid
	  */
    public static function authorizeBonus($db, $serial_apb, $authorize, $authorized_by, $observations_apb){
		$sql = "UPDATE applied_bonus SET
					authorized_apb='" . $authorize . "',
					authorized_date_apb=NOW(),
					serial_usr='" . $authorized_by . "',
					observations_apb = '{$observations_apb}'
					WHERE serial_apb = '" . $serial_apb . "'";
		
		//Debug::print_r($sql);
		$result = $db->Execute($sql);
		
		if ($result){
			//echo 'Success: '.$serial_apb.'<br>';
			$serial_apb_refund = self::hasRefundAssociated($db, $serial_apb);
			
			if($serial_apb_refund){
				//echo "<br>Serial_apb_refund: $serial_apb_refund<br>";
				return self::authorizeBonus($db, $serial_apb_refund, $authorize, $authorized_by, $observations_apb);
			}else{
				//echo 'Final issue<br>';
				return TRUE;
			}			
		}else{
			//echo 'Error in Main SQL';
			return FALSE;
		}
    }

	function refundBonus($bonus_was_paid=NULL) {
		$sql="INSERT INTO applied_bonus (
						SELECT	NULL,
								NULL,
								serial_sal, 
								total_amount_apb, 
								pending_amount_apb, 
								ondate_apb, 
								'REFUNDED', 
								bonus_to_apb, 
								authorized_apb,
								CURRENT_TIMESTAMP(),
								NULL, 
								'SYSTEM_REFUND'
						FROM applied_bonus apb
						WHERE apb.serial_sal={$this->serial_sal}
						AND apb.status_apb = 'ACTIVE'
						AND apb.serial_sal NOT IN(	SELECT serial_sal 
													FROM applied_bonus apb 
													WHERE status_apb='REFUNDED')
						ORDER BY  serial_apb DESC 
						LIMIT 1)";

		$result = $this->db -> Execute($sql);
		if($result){
			return true;
		}else{
			return false;
		}
	}

	/*
	 * @name: getBonusInfoBySale
	 * @param: $db - DB connection
	 * @param: $serial_sal - Sale ID
	 */
	public static function getBonusInfoBySale ($db, $serial_sal){
		$sql="SELECT (apb.total_amount_apb-apb.pending_amount_apb) AS 'amount_paid', apb.pending_amount_apb
			  FROM applied_bonus apb
			  WHERE apb.serial_sal=".$serial_sal."
			  AND (apb.ondate_apb='YES' OR (apb.ondate_apb='NO' AND apb.authorized_apb='YES'))";
		//echo '<br>'.$sql.'<br>';
		
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: getBonusForReport
	 * @param: $db - DB connection
	 * @param: $misc - An array of three values that
	 */
	public static function getBonusForReport($db, $misc, $begin_date, $end_date, $status){
		$where_used=0;

		if($begin_date && $end_date){
			$where_clauses=" WHERE STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y')
								   BETWEEN STR_TO_DATE('".$begin_date."','%d/%m/%Y')
								   AND STR_TO_DATE('".$end_date."','%d/%m/%Y')";
			$where_used=1;
		}

		if($status){
			if($where_used==1){
				$where_clauses.=" AND apb.status_apb='".$status."'";
			}else{
				$where_clauses=" WHERE apb.status_apb='".$status."'";
			}
		}
		
		switch($misc['type']){
			case 'BRANCH':
				$and_branch=" AND b.serial_dea=".$misc['identifier'];
				break;
			case 'COUNTER':
				$and_counter=" AND cnt.serial_cnt=".$misc['identifier'];
				break;
			case 'CARD':
				$and_card=" AND s.card_number_sal=".$misc['identifier'];
				break;
		}
		
		$sql="SELECT cit.name_cit, b.name_dea, s.card_number_sal, pbl.name_pbl, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) AS 'customer_name',
					 s.total_sal, apb.total_amount_apb, apb.status_apb
			  FROM applied_bonus apb
			  JOIN sales s ON s.serial_sal=apb.serial_sal ".$and_card."
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt ".$and_counter."
			  JOIN dealer b ON b.serial_dea=cnt.serial_dea ".$and_branch."
			  JOIN sector sec ON sec.serial_sec=b.serial_sec
			  JOIN city cit ON cit.serial_cit=sec.serial_cit
			  JOIN customer cus ON cus.serial_cus=s.serial_cus
			  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pbd
			  JOIN product p ON p.serial_pro=pxc.serial_pro
			  JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang=".$_SESSION['serial_lang'].' '.$where_clauses;
		
		//echo '<pre>'.$sql.'</pre>';
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);

			return $arreglo;
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: getBonusForReport
	 * @param: $db - DB connection
	 * @param: $misc - An array of three values that
	 */
    public static function getBonusForDealerReport($db, $misc, $status, $card_number, $begin_date, $end_date) {
        switch ($misc['type']) {
            case 'DEALER':
                $and_clauses = " AND b.serial_dea=" . $misc['identifier'];
                break;
            case 'COUNTER':
                $and_clauses = " AND cnt.serial_cnt IN (" . $misc['identifier'] . ")";
                break;
        }

        $sql = "SELECT  IFNULL(s.card_number_sal,'N/A') AS 'card_number_sal', pbl.name_pbl, 
                        CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) AS 'customer_name',
                        apb.total_amount_apb, apb.status_apb, i.number_inv, b.serial_dea, b.name_dea,
                        CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'counter'
                FROM applied_bonus apb
                JOIN sales s ON s.serial_sal=apb.serial_sal
                JOIN invoice i ON i.serial_inv=s.serial_inv
                JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
                JOIN dealer b ON b.serial_dea=cnt.serial_dea
                JOIN sector sec ON sec.serial_sec=b.serial_sec
                JOIN city cit ON cit.serial_cit=sec.serial_cit
                JOIN customer cus ON cus.serial_cus=s.serial_cus
                JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
                JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
                JOIN product p ON p.serial_pro=pxc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang=" . $_SESSION['serial_lang'] . "
                JOIN user u ON u.serial_usr = cnt.serial_usr
                WHERE (apb.ondate_apb = 'YES'
                OR (apb.ondate_apb = 'NO' AND apb.authorized_apb='YES'))
                " . $and_clauses;

        if ($status) {
            $sql.=" AND apb.status_apb='" . $status . "'";
        }

        if ($card_number) {
            $sql.=" AND s.card_number_sal=" . $card_number;
        }

        if ($begin_date && $end_date) {
            $sql.=" AND STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y')
						BETWEEN STR_TO_DATE('" . $begin_date . "', '%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "', '%d/%m/%Y')";
        }

        $sql.=" ORDER BY b.serial_dea, s.card_number_sal";

        //echo '<pre>'.$sql.'</pre>';
        $result = $db->Execute($sql);
        $num = $result->RecordCount();
        if ($num > 0) {
            $arreglo = array();
            $cont = 0;

            do {
                $arreglo[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);

            return $arreglo;
        } else {
            return FALSE;
        }
    }

	/*
	 * @Name: getSalesStatus
	 * @Description: Returns an array of all the sale satatus in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of free sales.
	 */
    public static function getAppliedBonusStatus($db) {
        $sql = "SHOW COLUMNS FROM applied_bonus LIKE 'status_apb'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
    
	function fixUpdate() {
        $sql = "UPDATE applied_bonus SET
                    total_amount_apb=".$this->total_amount_apb.",
                    pending_amount_apb='".$this->pending_amount_apb."',
					status_apb='".$this->status_apb."'
				WHERE serial_apb = '".$this->serial_apb."'";
        
        $result = $this->db->Execute($sql);
       
        if($result==true)
            return true;
        return false;
    }

	public static function getBonusToPayForReport($db, $bonus_to, $serial_cou, $serial_cit = NULL, $serial_mbc = NULL, 
												$serial_dea = NULL, $serial_bra = NULL, $serial_cnt = NULL){
		if($serial_cit) $restriction_clause = "AND sec.serial_cit = $serial_cit";
		if($serial_mbc) $restriction_clause.= " AND mbc.serial_mbc = $serial_mbc";
		if($serial_dea) $restriction_clause.= " AND b.dea_serial_dea = $serial_dea";
		if($serial_bra) $restriction_clause.= " AND b.serial_dea = $serial_bra";
		if($serial_cnt) $restriction_clause.= " AND s.serial_cnt = $serial_cnt";

		$sql = "SELECT s.card_number_sal, i.number_inv, apb.total_amount_apb, apb.pending_amount_apb, apb.status_apb, apb.ondate_apb, apb.authorized_apb,
						CONCAT(b.name_dea, ' - ', b.code_dea) AS 'name_dea', m.name_man, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'counter', CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) AS 'responsible_name',
						s.serial_inv, s.total_sal, (i.discount_prcg_inv + i.other_dscnt_inv) AS 'invoice_discounts',
						i.comision_prcg_inv, b.serial_dea
				FROM applied_bonus apb
				JOIN sales s ON s.serial_sal = apb.serial_sal
				JOIN invoice i ON i.serial_inv = s.serial_inv
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea AND cnt.status_cnt = 'ACTIVE'
				JOIN dealer d ON d.serial_dea = b.dea_serial_dea
				JOIN sector sec ON sec.serial_sec = d.serial_sec
		        JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr
				JOIN user u ON u.serial_usr = cnt.serial_usr
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				WHERE apb.pending_amount_apb > 0
				AND (apb.authorized_apb IS NULL OR apb.authorized_apb = 'YES')
				AND apb.bonus_to_apb = '$bonus_to'
				AND mbc.serial_cou = $serial_cou
				$restriction_clause
				ORDER BY name_dea,counter, s.card_number_sal, apb.serial_apb";
		//die('<pre>'.$sql.'</pre>');
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getBonusPaidForReport($db, $bonus_to, $serial_cou, $serial_cit = NULL, $serial_mbc = NULL,
												$serial_dea = NULL, $serial_bra = NULL, $serial_cnt = NULL){
		if($serial_cit) $restriction_clause = "AND sec.serial_cit = $serial_cit";
		if($serial_mbc) $restriction_clause.= " AND mbc.serial_mbc = $serial_mbc";
		if($serial_dea) $restriction_clause.= " AND b.dea_serial_dea = $serial_dea";
		if($serial_bra) $restriction_clause.= " AND b.serial_dea = $serial_bra";
		if($serial_cnt) $restriction_clause.= " AND s.serial_cnt = $serial_cnt";

		$sql = "SELECT b.name_dea, CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) AS 'counter',
						SUM(apb.pending_amount_apb) AS 'pending_amount_apb', SUM(amount_used_abl) AS 'paid_amount', m.name_man,
						lqn.serial_lqn AS 'liquidation_number', DATE_FORMAT(lqn.date_lqn, '%d/%m/%Y') AS 'date_lqn'
				FROM applied_bonus apb
				JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb
				JOIN bonus_liquidation lqn ON lqn.serial_lqn = abl.serial_lqn
				JOIN sales s ON s.serial_sal = apb.serial_sal				
				JOIN counter c ON c.serial_cnt = s.serial_cnt
				JOIN user usr ON usr.serial_usr = c.serial_usr
				JOIN dealer b ON b.serial_dea = c.serial_dea
				JOIN sector sec ON sec.serial_sec = b.serial_sec
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				WHERE apb.bonus_to_apb = '$bonus_to'
				AND mbc.serial_cou = $serial_cou
				$restriction_clause
				GROUP BY lqn.serial_lqn
				ORDER BY lqn.serial_lqn, b.name_dea";
		//die(Debug::print_r($sql));
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getAuthorizedBonusForReport($db, $bonus_to, $serial_cou, $serial_cit = NULL, $serial_mbc = NULL,
												$serial_dea = NULL, $serial_bra = NULL, $serial_cnt = NULL, $status, $begin_date = NULL, $end_date = NULL){
		if($serial_cit) $restriction_clause = "AND sec.serial_cit = $serial_cit";
		if($serial_mbc) $restriction_clause.= " AND mbc.serial_mbc = $serial_mbc";
		if($serial_dea) $restriction_clause.= " AND b.dea_serial_dea = $serial_dea";
		if($serial_bra) $restriction_clause.= " AND b.serial_dea = $serial_bra";
		if($serial_cnt) $restriction_clause.= " AND s.serial_cnt = $serial_cnt";
		if ($status == ''){
			 $restriction_clause.= " AND apb.authorized_apb IS NOT NULL";
		}else $restriction_clause.= " AND apb.authorized_apb = '$status'";
		if($begin_date) $restriction_clause.= " AND apb.authorized_date_apb between STR_TO_DATE('".$begin_date."','%d/%m/%Y')
								   AND STR_TO_DATE('".$end_date."','%d/%m/%Y')";
		
		$sql = "SELECT s.card_number_sal, i.number_inv,
						i.due_date_inv, p.date_pay,
						DATEDIFF(payment_date_apb, date_inv) AS 'notPaid_days',
						IF(apb.status_apb = 'REFUNDED', CONCAT('- ', apb.total_amount_apb), apb.total_amount_apb) AS 'total_amount_apb',
						CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) AS 'authorized_by',
						DATE_FORMAT(apb.authorized_date_apb, '%d/%m/%Y') AS 'authorized_date_apb',
						IFNULL(observations_apb, 'N/A') AS 'observations_apb',
						CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'responsible_name',
						b.name_dea, m.name_man, s.total_sal, 
						(i.discount_prcg_inv + i.other_dscnt_inv) AS 'invoice_discounts', i.comision_prcg_inv,
						IF(apb.authorized_apb = 'YES', 'Autorizado', 'Negado') AS 'authorized_apb'
				FROM applied_bonus apb
				JOIN user usr ON usr.serial_usr = apb.serial_usr
				JOIN sales s ON s.serial_sal = apb.serial_sal
				JOIN invoice i ON i.serial_inv = s.serial_inv
				JOIN payments p ON i.serial_pay = p.serial_pay
				JOIN counter c ON c.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = c.serial_dea
				JOIN sector sec ON sec.serial_sec = b.serial_sec
				JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				WHERE apb.bonus_to_apb = '$bonus_to'
				AND apb.ondate_apb = 'NO'
				AND mbc.serial_cou = $serial_cou
				$restriction_clause
				ORDER BY apb.authorized_date_apb, b.name_dea";

		$result = $db -> getAll($sql);
		//Debug::print_r($sql);
		//die();
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name disableAppliedBonusInLiquidation
	 * @param type $db
	 * @param type $serial_apb 
	 */
	public static function disableAppliedBonusInLiquidation($db, $serial_apb, $serial_usr){
		$sql = "UPDATE applied_bonus SET 
					serial_usr = $serial_usr,
					pending_amount_apb = 0,
					observations_apb = CONCAT(observations_apb, ' - SYSTEM LIQUIDATION AND MATCH WITH REFUND REGISTER')
				WHERE serial_apb = $serial_apb";
		
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'DISABLE BONUS FAILED', $sql);
			return FALSE;
		}
	}
	
	public static function hasRefundAssociated($db, $serial_apb){
		$sql = "SELECT serial_sal
				FROM applied_bonus
				WHERE serial_apb = $serial_apb";
		
		$serial_sal = $db -> getOne($sql);
		
		if($serial_sal){
			$sql = "SELECT serial_apb
					FROM applied_bonus
					WHERE serial_sal = $serial_sal
					AND status_apb = 'REFUNDED'
					AND serial_apb <> $serial_apb";

			$result = $db -> getOne($sql);
			if($result){
				return $result;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}	
	}
	
	public static function getOutOfDateBonusesByPeriodInDays($db, $days_due){
		$sql = "SELECT apb.serial_apb, DATEDIFF(CURRENT_TIMESTAMP(), payment_date_apb) AS 'due_dates'
				FROM applied_bonus apb
				WHERE DATEDIFF(CURRENT_TIMESTAMP(), payment_date_apb) >= $days_due
				AND apb.status_apb IN ('ACTIVE', 'REFUNDED')
				AND apb.ondate_apb='NO'
				AND apb.authorized_apb IS NULL";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getAvailableBonusForUser($db, $serial_usr, $totalizeAmount = FALSE){
		$bonusData = self::getBonusTypeForUser($db, $serial_usr);
		
		if($totalizeAmount){
			$amountAvailable = " SUM(IF(status_apb = 'ACTIVE', pending_amount_apb, (pending_amount_apb * -1))) AS 'amount_available'";
		}else{
			$amountAvailable = " IF(status_apb = 'ACTIVE', pending_amount_apb, (pending_amount_apb * -1)) AS 'amount_available'";
		}
		
		if($bonusData['bonus_to_dea'] == 'DEALER' && $bonusData['isLeader_usr'] == 'YES'){
			$sql = "SELECT s.card_number_sal, ap.serial_apb, ap.serial_sal, $amountAvailable, ondate_apb, 
							status_apb, pending_amount_apb, CONCAT(c.first_name_cus,' ', c.last_name_cus) AS 'name_cus',
							i.number_inv, total_amount_apb
					FROM applied_bonus ap
					JOIN sales s ON s.serial_sal = ap.serial_sal
						AND ap.bonus_to_apb = 'DEALER'
						AND status_apb IN ('ACTIVE', 'REFUNDED')
						AND pending_amount_apb > 0
                                                AND (ap.ondate_apb = 'YES' || ap.authorized_apb = 'YES')
					JOIN customer c on c.serial_cus = s.serial_cus
					JOIN invoice i on i.serial_inv = s.serial_inv
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
						AND cnt.serial_dea = {$bonusData['serial_dea']}"
                                                . " ORDER BY ap.serial_apb DESC";
						
		}else if($bonusData['bonus_to_dea'] == 'COUNTER'){
			$sql = "SELECT s.card_number_sal, ap.serial_apb, ap.serial_sal, $amountAvailable, ondate_apb, 
							status_apb, pending_amount_apb, CONCAT(c.first_name_cus,' ', c.last_name_cus) AS 'name_cus',
							i.number_inv, total_amount_apb
					FROM applied_bonus ap
					JOIN sales s ON s.serial_sal = ap.serial_sal
						AND ap.bonus_to_apb = 'COUNTER'
						AND status_apb IN ('ACTIVE', 'REFUNDED')
						AND pending_amount_apb > 0
                                                AND (ap.ondate_apb = 'YES' || ap.authorized_apb = 'YES')
					JOIN customer c on c.serial_cus = s.serial_cus
					JOIN invoice i on i.serial_inv = s.serial_inv
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
						AND cnt.serial_usr = $serial_usr"
                                        . " ORDER BY ap.serial_apb DESC";
						
		}else{
			return FALSE;
		}
		
		//Debug::print_r($sql); die;
		
		if($totalizeAmount){
			$result = $db->getRow($sql);
		}else{
			$result = $db->getAll($sql);
		}
		
		if($result){
			return $result;
		}else{
			return false;
		}
		
	}
	
	private static function getBonusTypeForUser($db, $serial_usr){
		$sql = "SELECT d.bonus_to_dea, u.isLeader_usr, u.serial_dea
				FROM user u
				JOIN dealer d on d.serial_dea = u.serial_dea
				AND u.serial_usr = $serial_usr";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

 	///GETTERS
	function getSerial_apb(){
		return $this->serial_apb;
	}
	function getSerial_sal(){
		return $this->serial_sal;
	}
	function getTotal_amount_apb(){
		return $this->value_apb;
	}
	function getPending_amount_apb(){
		return $this->pending_amount_apb;
	}
	function getOn_date_apb(){
		return $this->on_date_apb;
	}
	function getStatus_apb(){
		return $this->status_apb;
	}
	function getBonus_to_apb(){
		return $this->bonus_to_apb;
	}
	function getAuthorized_apb(){
		return $this->authorized_apb;
	}
	function getPayment_date_apb(){
		return $this->payment_date_apb;
	}
	function getAuthorized_date_apb(){
		return $this->authorized_date_apb;
	}
	public function getObservations_apb() {
	 return $this->observations_apb;
	}


	function setSerial_apb($serial_apb){
		$this->serial_apb = $serial_apb;
	}
	function setSerial_sal($serial_sal){
		$this->serial_sal = $serial_sal;
	}
	function setTotal_amount_apb($total_amount_apb){
		$this->total_amount_apb = $total_amount_apb;
	}
	function setPending_amount_apb($pending_amount_apb){
		$this->pending_amount_apb = $pending_amount_apb;
	}
	function setOn_date_apb($on_date_apb){
		$this->on_date_apb = $on_date_apb;
	}
	function setStatus_apb($status_apb){
		$this->status_apb = $status_apb;
	}
	function setBonus_to_apb($bonus_to_apb){
		$this->bonus_to_apb = $bonus_to_apb;
	}
	function setAuthorized_apb($authorized_apb){
		$this->authorized_apb = $authorized_apb;
	}
	function setPayment_date_apb($payment_date_apb){
		$this->payment_date_apb = $payment_date_apb;
	}
	function setAuthorized_date_apb($authorized_date_apb){
		$this->authorized_date_apb = $authorized_date_apb;
	}
	public function setObservations_apb($observations_apb) {
	 $this->observations_apb = $observations_apb;
	}
}
?>