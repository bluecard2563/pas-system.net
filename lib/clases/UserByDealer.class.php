<?php
/*
File: UserByDealer.class
Author: David Bergmann
Creation Date:16/03/2010
Last Modified:
Modified By:
*/
 class UserByDealer{
     var $db;
     var $serial_ubd;
     var $serial_usr;
     var $serial_dea;
     var $status_ubd;
     var $begin_date_ubd;
     var $end_date_ubd;
     var $last_commission_paid_ubd;
	 var $percentage_ubd;

     function __construct($db, $serial_ubd = NULL, $serial_usr = NULL, $serial_dea = NULL,
                          $status_ubd = NULL, $begin_date_ubd = NULL , $end_date_ubd = NULL,
                          $last_commission_paid_ubd = NULL, $percentage_ubd=NULL){
         $this -> db = $db;
         $this -> serial_ubd = $serial_ubd;
         $this -> serial_usr = $serial_usr;
         $this -> serial_dea = $serial_dea;
         $this -> status_ubd = $status_ubd;
         $this -> begin_date_ubd = $begin_date_ubd;
         $this -> end_date_ubd = $end_date_ubd;
         $this -> last_commission_paid_ubd = $last_commission_paid_ubd;
		 $this -> percentage_ubd = $percentage_ubd;
     }

     /***********************************************
     * function getData
     * gets data by serial_zon
     ***********************************************/
     function getData(){
         if($this->serial_dea!=NULL){
             $sql = "SELECT serial_ubd, serial_usr, serial_dea, status_ubd, begin_date_ubd,
                     end_date_ubd, last_commission_paid_ubd, percentage_ubd
                     FROM user_by_dealer
                     WHERE serial_dea = '".$this->serial_dea."'
                     AND status_ubd = 'ACTIVE'";
             //die($sql);
             $result = $this->db->Execute($sql);

             if ($result === false) return false;
             if($result->fields[0]){
                $this->serial_ubd=$result->fields[0];
                $this->serial_usr=$result->fields[1];
                $this->serial_dea=$result->fields[2];
                $this->status_ubd=$result->fields[3];
                $this->begin_date_ubd=$result->fields[4];
                $this->end_date_ubd=$result->fields[5];
                $this->last_commission_paid_ubd=$result->fields[6];
				$this->percentage_ubd=$result->fields[7];

                 return true;
             }
             else
                return false;
         }else
            return false;
     }

     /***********************************************
     * function insert
     * inserts a new register in DB
     ***********************************************/
     function insert(){
        $sql="INSERT INTO user_by_dealer (
              serial_ubd,
              serial_usr,
              serial_dea,
              status_ubd,
              begin_date_ubd,
              end_date_ubd,
              last_commission_paid_ubd,
			  percentage_ubd
              )
              VALUES(NULL,
              '".$this->serial_usr."',
              '".$this->serial_dea."',
              '".$this->status_ubd."',
              NOW(),
              NULL,
              NOW(),
			  '".$this->percentage_ubd."'
              );";
        $result = $this->db->Execute($sql);
		
     	if($this->db->insert_ID()){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
     }

	 /***********************************************
     * function update
     * updates a register in DB
     ***********************************************/
	 function update() {
        $sql = "UPDATE user_by_dealer SET
				serial_ubd='".$this->serial_ubd."',
				serial_usr='".$this->serial_usr."',
				serial_dea='".$this->serial_dea."',
				begin_date_ubd= '".$this->begin_date_ubd."',
				end_date_ubd=NULL,
				last_commission_paid_ubd='".$this->last_commission_paid_ubd."',
				percentage_ubd='".$this->percentage_ubd."'
				WHERE serial_ubd='".$this->serial_ubd."'
			   ";//echo $sql;
		$result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;
        else
            return false;
	 }


     /***********************************************
     * funcion deactivateUser
     * makes a user inactive
     ***********************************************/
     function deactivateUser(){
         $sql="UPDATE user_by_dealer
               SET status_ubd = 'INACTIVE', end_date_ubd = NOW()
               WHERE serial_ubd = '".$this->serial_ubd."'";

         $result = $this->db->Execute($sql);
         if ($result==true)
            return true;
         else
            return false;
     }
	 /***********************************************
    * funcion updateLastComissionPaid
    * updates the last date a comission was paid by serial_usr (user).
	* Parameter: atribute serial_usr
    ***********************************************/
    function updateLastComissionPaid() {
        $sql = "
                UPDATE user_by_dealer SET last_commission_paid_ubd = NOW() WHERE serial_usr = '".$this->serial_usr."' AND status_ubd='ACTIVE'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;
        if($result==true)
            return true;
        else
            return false;
    }
	 /***********************************************
    * funcion getLast_commission_paid_by_usr
    * retireve the last comission paid date by serial_usr (user).
	* Parameter: atribute serial_usr
    ***********************************************/
    function getLast_commission_paid_by_usr($serial_usr) {
        $sql = "SELECT DATE_FORMAT(max(last_commission_paid_ubd),'%d/%m/%Y')
			 	FROM user_by_dealer
				WHERE serial_usr = $serial_usr
				AND status_ubd = 'ACTIVE'";
        //echo $sql;
        $result = $this->db->getOne($sql);
        if ($result)
			return $result;
        else
            return false;
    }

	/***********************************************
	 * @Name: getResponsiblesWithExcessPayments
	 * @Description: Retrieves a list of responsibles with excess payments to dispatch.
	 * @Params: DB connection
	 * @Returns: an array of responsibles.
    ***********************************************/
	public static function getResponsiblesWithExcessPayments($db, $serial_cit, $serial_mbc){
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
				FROM dealer d
				JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr AND u.status_usr = 'ACTIVE'
				JOIN sector s ON s.serial_sec = d.serial_sec
				JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				JOIN sales sal ON cnt.serial_cnt=sal.serial_cnt
				JOIN invoice i ON i.serial_inv=sal.serial_inv AND i.status_inv='PAID'
				JOIN payments p ON i.serial_pay=p.serial_pay AND p.status_pay='EXCESS' AND p.excess_amount_available_pay > 0
				AND s.serial_cit ='".$serial_cit."'";

		if($serial_mbc!='1'){
			$sql.=" WHERE d.serial_mbc=".$serial_mbc;
		}

		$sql.="	 ORDER BY name_usr";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/***********************************************
	 * @Name: getResponsiblesWithFreeSales
	 * @Description: Retrieves a list of user_by_dealer with excess payments to dispatch.
	 * @Params: DB connection
	 * @Returns: an array of user_by_dealer.
    ***********************************************/
	public static function getResponsiblesWithFreeSales($db,$serial_cit, $serial_mbc){
		$sql = 	"SELECT DISTINCT u.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr'
				 FROM user u
				 JOIN user_by_dealer ubd ON ubd.serial_usr=u.serial_usr
				 JOIN dealer d ON d.serial_dea=ubd.serial_dea
				 JOIN sector s ON s.serial_sec=d.serial_sec
				 JOIN city c ON c.serial_cit=s.serial_cit AND c.serial_cit='$serial_cit'
				 JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				 JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt AND sal.status_sal='REQUESTED' AND sal.free_sal='YES'";

		if($serial_mbc!='1'){
			$sql.=" WHERE d.serial_mbc=".$serial_mbc;
		}

		$sql.="	 ORDER BY name_usr";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

        /***********************************************
	 * @Name: getResponsiblesWithFreeSales
	 * @Description: Retrieves a list of user_by_dealer with excess payments to dispatch.
	 * @Params: DB connection
	 * @Returns: an array of user_by_dealer.
    ***********************************************/
	public static function getResponsiblesByDealer($db, $serial_dea, $active_only = FALSE){
		$sql = 	"SELECT DISTINCT u.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr',
						email_usr
				 FROM user u
				 JOIN user_by_dealer ubd ON ubd.serial_usr=u.serial_usr ";
		
		if($active_only){
			$sql.= " AND ubd.status_ubd = 'ACTIVE' AND u.status_usr = 'ACTIVE'";
		}
		
		$sql .= "ORDER BY name_usr";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

    /***********************************************
     * @Name: getResponsiblesByCity
     * @Description: Retrieves a list of user_by_dealer in a specific city
     * @Params: DB connection
     * @Returns: an array of user_by_dealer.
    ***********************************************/
    public static function getResponsiblesByCity($db,$serial_cit, $serial_mbc){
            $sql = "SELECT DISTINCT u.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr'
                    FROM user u
                    JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
					JOIN dealer d ON d.serial_dea=ubd.serial_dea
                    WHERE u.serial_cit = '".$serial_cit."'";
			if($serial_mbc!='1'){
				$sql.=" AND d.serial_mbc=".$serial_mbc;
			}
			$sql.=" ORDER BY name_usr";

            $result = $db -> getAll($sql);

            if($result){
                    return $result;
            }else{
                    return false;
            }
    }

	/***********************************************
     * @Name: getResponsiblesByCity
     * @Description: Retrieves a list of user_by_dealer in a specific city
     * @Params: DB connection
     * @Returns: an array of user_by_dealer.
    ***********************************************/
     function getResponsibles($serial_usr){
            $sql = "SELECT  ubd.serial_ubd,u.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr', ubd.serial_dea, ubd.status_ubd
                    FROM user u
                    JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
                    WHERE u.serial_usr = '".$serial_usr."'
                    AND ubd.status_ubd='ACTIVE'";

            $result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$arr=array();
				$cont=0;

				do{
					$arr[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);

				return $arr;
			}else{
				return FALSE;
			}
			
    }

	/*
     * @name: getResponsiblesByManager
     * @param: DB connection
     * @return: an array of user_by_dealer.
     */
     public static function getResponsiblesByManager($db, $serial_mbc){
		$sql = "SELECT DISTINCT u.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr'
				FROM user u
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
				JOIN dealer d ON d.serial_dea=ubd.serial_dea AND d.serial_mbc=$serial_mbc AND d.dea_serial_dea IS NOT NULL
				WHERE ubd.status_ubd='ACTIVE'";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
    }
	/**
	@Name: getResponsiblesByCityWithRefunds
	@Description: Retrieves the information of a specific user's list
	@Params: serial of a city
	@Returns: User array
	**/
	function getResponsiblesByManagerWithRefunds($serial_mbc){
		$arreglo = array();
		$sql =  "SELECT DISTINCT (usr.serial_usr), CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS 'name_usr'
				 FROM sales sal
				   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					JOIN sector sec ON br.serial_sec=sec.serial_sec
				   JOIN city cit ON sec.serial_cit=cit.serial_cit
				   JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea AND ubd.status_ubd='ACTIVE'
				   JOIN user usr ON usr.serial_usr=ubd.serial_usr
				   JOIN refund ref ON ref.serial_sal=sal.serial_sal
				   WHERE br.serial_mbc = ".$serial_mbc;
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY name_usr";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

	/**
	@Name: getResponsiblesSalesAnalysis
	@Description: Retrieves the information of a specific user's list
	@Params: serial of a city
	@Returns: User array
	**/
	function getResponsiblesSalesAnalysis($serial_cit, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($serial_cit){
			$sql =  "SELECT DISTINCT (usr.serial_usr), CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS 'name_usr'
						FROM sales sal
							JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
							JOIN dealer br ON cnt.serial_dea=br.serial_dea
							JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
							JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
							JOIN sector sec ON br.serial_sec=sec.serial_sec
							JOIN city cit ON sec.serial_cit=cit.serial_cit
							JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND ubd.status_ubd='ACTIVE'
                            JOIN user usr ON usr.serial_usr=ubd.serial_usr
							JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
							JOIN payments pay ON pay.serial_pay=inv.serial_pay
                        WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')  AND sal.serial_inv IS NOT NULL
                               AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID' AND cit.serial_cit = ".$serial_cit;
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY name_usr";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}
	/**
	 * @name getResponsibleByDeaSerialDea
	 * @param <String> $db
	 * @param <String> $dea_serial_dea
	 * @return <array>
	 */
	public static function getResponsibleByDeaSerialDea($db, $dea_serial_dea) {
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr
				FROM user_by_dealer ubd
				JOIN user usr ON usr.serial_usr = ubd.serial_usr
				JOIN dealer dea ON dea.serial_dea = ubd.serial_dea
				WHERE dea.dea_serial_dea = $dea_serial_dea";
		
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/**
	 * @name getResponsibleByDeaSerialDea
	 * @param <String> $db
	 * @param <String> $dea_serial_dea
	 * @return <array>
	 */
	public static function getResponsibleByDeaByMbc($db, $serial_mbc) {
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr
				FROM user_by_dealer ubd
				JOIN user usr ON usr.serial_usr = ubd.serial_usr
				JOIN dealer dea ON dea.serial_dea = ubd.serial_dea
				WHERE ubd.status_ubd = 'ACTIVE' AND dea.serial_mbc = $serial_mbc";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/**
	 * @name getResponsibleByCity
	 * @param <String> $db
	 * @param <String> $serial_cit
	 * @return <array>
	 */
	public static function getResponsibleByCity($db, $serial_cit) {
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr
				FROM user_by_dealer ubd
				JOIN user usr ON usr.serial_usr = ubd.serial_usr
				JOIN dealer dea ON dea.serial_dea = ubd.serial_dea
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				WHERE sec.serial_cit = $serial_cit";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function getActiveResponsibleForBranch($db, $serial_dea){
		$sql = "SELECT * 
				FROM user_by_dealer ubd
				WHERE serial_dea = $serial_dea
				AND status_ubd = 'ACTIVE'";

		$result=$db->getRow($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public static function getUserByDealerName($db, $serial_usr){
		$sql = "SELECT DISTINCT CONCAT(u.first_name_usr, ' ', u.last_name_usr)
				FROM user_by_dealer ubd
				JOIN user u ON u.serial_usr = ubd.serial_usr
				WHERE u.serial_usr = $serial_usr";
		
		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * @name: getResponsiblesWithSales
	 * @param: $db - DB connection
	 * @return: Array with responsibles
	 */
	public static function getResponsiblesWithSales($db, $serial_mbc, $serial_dea, $serial_cit){

		$sql = "SELECT DISTINCT usr.serial_usr, CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt 
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.serial_mbc = $serial_mbc
					AND dea.dea_serial_dea IS NOT NULL
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit 
				WHERE cit.serial_cit = $serial_cit
				";

		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_usr";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: getResponsiblesWithAssignedDealers
	 * @param: $db - DB connection
	 * @return: Array with responsibles
	 */
	public static function getResponsiblesWithAssignedDealers($db, $serial_mbc, $serial_dea){

		$sql = "SELECT DISTINCT usr.serial_usr, CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				WHERE dea.serial_mbc = $serial_mbc AND dea.dea_serial_dea IS NOT NULL
				";

		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_usr";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getResponsiblesWithNonPaidInvoicesAndAssistance
	 * @param type $db
	 * @param type $serial_mbc
	 * @return boolean 
	 */
	public static function getResponsiblesWithNonPaidInvoicesAndAssistance($db, $serial_mbc){
		$sql = "SELECT DISTINCT u.serial_usr, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_usr'
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv NOT IN ('PAID', 'VOID', 'UNCOLLECTABLE')
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN credit_day cdd ON cdd.serial_cdd = b.serial_cdd
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc AND mbc.serial_mbc = $serial_mbc
				JOIN country cou ON cou.serial_cou = mbc.serial_cou 
				JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr";
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getDealersByUser($db, $serial_usr){
		$sql = "SELECT dea.serial_dea, dea.name_dea, COUNT(br.serial_dea) AS 'branch_count'
				FROM user_by_dealer ubd 
				JOIN dealer dea ON dea.serial_dea = ubd.serial_dea AND dea.dea_serial_dea IS NULL
				AND ubd.serial_usr = $serial_usr AND ubd.status_ubd = 'ACTIVE' AND dea.status_dea = 'ACTIVE'
				JOIN dealer br ON br.dea_serial_dea = dea.serial_dea AND br.status_dea = 'ACTIVE'
				GROUP BY dea.serial_dea";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getResponsiblesByCountry($db, $serial_cou){
            $sql = "SELECT DISTINCT u.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr'
                    FROM user u
                    JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr 
						AND ubd.status_ubd = 'ACTIVE' 
						AND u.status_usr = 'ACTIVE'
					JOIN dealer d ON d.serial_dea = ubd.serial_dea
					JOIN sector sec ON sec.serial_sec = d.serial_sec
					JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
					ORDER BY name_usr";

            $result = $db -> getAll($sql);

            if($result){
                    return $result;
            }else{
                    return false;
            }
    }

	public static function getUserByDealerByCard($db, $serial_sal){
		$sql = "SELECT concat(usr.first_name_usr,' ',usr.last_name_usr) as responsable
		from sales sal 
		join counter cnt on sal.serial_cnt = cnt.serial_cnt
		join dealer dea on cnt.serial_dea = dea.serial_dea
		join user_by_dealer ubd on dea.serial_dea = ubd.serial_dea
		join user usr on ubd.serial_usr = usr.serial_usr
		where sal.serial_sal= $serial_sal and ubd.status_ubd='ACTIVE'";
		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}


     ///GETTERS
     function getSerial_ubd(){
        return $this->serial_ubd;
     }
     function getSerial_usr(){
        return $this->serial_usr;
     }
     function getSerial_dea(){
        return $this->serial_dea;
     }
     function getStatus_ubd(){
        return $this->status_ubd;
     }
     function getBegin_date_ubd(){
        return $this->begin_date_ubd;
     }
     function getEnd_date_ubd(){
        return $this->end_date_ubd;
     }
     function getLast_commission_paid_ubd(){
        return $this->last_commission_paid_ubd;
     }
	 function getPercentage_ubd(){
        return $this->percentage_ubd;
     }

     ///SETTERS
     function setSerial_ubd($serial_ubd){
        $this->serial_ubd = $serial_ubd;
     }
     function setSerial_usr($serial_usr){
        $this->serial_usr = $serial_usr;
     }
     function setSerial_dea($serial_dea){
        $this->serial_dea = $serial_dea;
     }
     function setStatus_ubd($status_ubd){
        $this->status_ubd = $status_ubd;
     }
     function setBegin_date_ubd($begin_date_ubd){
        $this->begin_date_ubd = $begin_date_ubd;
     }
     function setEnd_date_ubd($end_date_ubd){
        $this->end_date_ubd = $end_date_ubd;
     }
     function setLast_commission_paid_ubd($last_commission_paid_ubd){
        $this->last_commission_paid_ubd = $last_commission_paid_ubd;
     }
     function setPercentage_ubd($percentage_ubd){
        $this->percentage_ubd = $percentage_ubd;
     }
 }
?>
