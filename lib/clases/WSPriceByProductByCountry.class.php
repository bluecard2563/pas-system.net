<?php
/*
File: ProductByCountry.class.php
Author: Edwin Salvador.
Creation Date: 18/02/2010
Last Modified: 
Modified By: 
*/

class WSPriceByProductByCountry{
    var $db;
    var $serial_ppc;
    var $serial_pxc;
    var $duration_ppc;
    var $price_ppc;
    var $cost_ppc;
    var $min_ppc;
    var $max_ppc;

    function __construct($db, $serial_ppc=NULL, $serial_pxc=NULL, $duration_ppc=NULL, $price_ppc=NULL, $cost_ppc=NULL, $min_ppc=NULL, $max_ppc=NULL){
        $this -> db = $db;
        $this -> serial_ppc = $serial_ppc;
        $this -> serial_pxc = $serial_pxc;
        $this -> duration_ppc = $duration_ppc;
        $this -> price_ppc = $price_ppc;
        $this -> cost_ppc = $cost_ppc;
        $this -> min_ppc = $min_ppc;
        $this -> max_ppc = $max_ppc;
    }
    
    /** 
    @Name: getData
    @Description: Retrieves the data of a Dealer object.
    @Params: The ID of the object
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getData(){
        if($this->serial_ppc!=NULL){
            $sql = 	"SELECT serial_ppc, serial_pxc, duration_ppc, price_ppc, cost_ppc, min_ppc, max_ppc
                            FROM price_by_product_by_country
                            WHERE serial_ppc='".$this->serial_ppc."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_ppc = $result->fields[0];
                $this -> serial_pxc = $result->fields[1];
                $this -> duration_ppc = $result->fields[2];
                $this -> price_ppc = $result->fields[3];
                $this -> cost_ppc = $result->fields[4];
                $this -> min_ppc = $result->fields[5];
                $this -> max_ppc = $result->fields[6];

                return true;
            }else
                    return false;
        }else
            return false;		
    }
	
    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO price_by_product_by_country (
                serial_ppc,
                serial_pxc,
                duration_ppc,
                price_ppc,
                cost_ppc,
                min_ppc,
                max_ppc)
            VALUES (
                    NULL,
                    '".$this->serial_pxc."',
                    '".$this->duration_ppc."',";

        if($this->price_ppc){                    
            $sql.="'".$this->price_ppc."',";
        }
        else{
            $sql.="NULL,";
        }

        $sql.=  "'".$this->cost_ppc."',";

        if($this->min_ppc){
            $sql.=  "'".$this->min_ppc."',";
        }
        else{
            $sql.="NULL,";
        }
        if($this->max_ppc){
            $sql.=  "'".$this->max_ppc."'";
        }
        else{
            $sql.="NULL";
        }


        $sql.=  ")";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion detele
    * deletes a register in DB
    ***********************************************/
    function deleteByProduct($serial_pro) {
		$sql="  DELETE
				FROM price_by_product_by_country
				WHERE serial_pxc
					IN (    SELECT serial_pxc
							FROM product_by_country
							WHERE serial_pro='".$serial_pro."'
						    AND serial_cou=1)";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true){
            return true;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion delete
    * deletes a register in DB
    ***********************************************/
    function delete() {
        if($this->serial_pxc != NULL){
            $sql = "DELETE
                    FROM price_by_product_by_country
                    WHERE serial_pxc = '".$this->serial_pxc."'";


            //echo $sql;die;
            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true){
                return true;
            }else{
                return false;
            }
        }
    }

    /***********************************************
    * funcion getAssignedCountries
    * gets all the Countries assigened to a product
    ***********************************************/
    function getPricesByProduct($serial_pro){
        $sql = 	"   SELECT  ppc.duration_ppc, ppc.price_ppc, ppc.cost_ppc, ppc.min_ppc, ppc.max_ppc, ppc.serial_pxc
                    FROM price_by_product_by_country ppc
                        JOIN product_by_country pxc
                            ON pxc.serial_pxc = ppc.serial_pxc
                            AND pxc.serial_pro = '".$serial_pro."'
							AND pxc.serial_cou=1
                    ORDER BY ppc.duration_ppc";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arreglo;
    }

    /*
     * @function: getPricesByProductByCountry
     * @Description: Retrieves the fees of all the
     * @params: $serial_pxc: Product By Country ID
     *          $serial_dea: Dealer ID
     *          $serial_cou: Dealer Country ID
     * @returns: Array of fees acording to a country.
     */
    function getPricesByProductByCountry($serial_pro, $serial_cou, $days=NULL){
        $sql="SELECT ppc.*
              FROM price_by_product_by_country ppc
              WHERE ppc.serial_pxc = (SELECT pxc.serial_pxc
                                    FROM product_by_country pxc
                                    WHERE pxc.serial_cou = ".$serial_cou."
                                    AND pxc.serial_pro = ".$serial_pro.")";
        if($days){
              $sql .= " AND ppc.duration_ppc BETWEEN ".($days-10)." AND ".($days+10);
         }
        $result = $this->db->Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        //Debug::print_r($arreglo);
        return $arreglo;
    }
    
/*
     * @function: getPricesByProductByCountryHigherThanDays
     * @Description: Retrieves the prices for all the days given for this product, but only for days over than those selected. 
     */
    function getPricesByProductByCountryHigherThanDays($serial_pro, $serial_cou, $days){
        $sql="SELECT ppc.*
              FROM price_by_product_by_country ppc
              WHERE ppc.serial_pxc = (SELECT pxc.serial_pxc
                                    FROM product_by_country pxc
                                    WHERE pxc.serial_cou = ".$serial_cou."
                                    AND pxc.serial_pro = ".$serial_pro.") AND ppc.duration_ppc >= ".$days;
		//echo $sql;
        $result = $this->db->Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        //Debug::print_r($arreglo);
        return $arreglo;
    }

    /*
     * @function: pricesByProductExist
     * @Description: Verifies if prices of a certain product for a certain country exists.
     * @params: $serial_pxc: Product By Country ID
     * @returns: Array of fees acording to a country.
     */
    public static function pricesByProductExist($db, $serial_pxc){
        $sql="SELECT serial_ppc
              FROM price_by_product_by_country ppc
              WHERE ppc.serial_pxc = ".$serial_pxc;
		//echo $sql;
        $result = $db->Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }

    /*
     * @function: getMaxPricePosible
     * @Description: Retrieves the next available fee according to a specific amount of days.
     * @params: $serial_pxc: Product By Country ID
     *          $serial_dea: Dealer ID
     *          $serial_cou: Dealer Country ID
     * @returns: The Price ID if OK / FALSE otherwise.
     */
    function getMaxPricePosible($serial_pro, $days, $serial_cou){
        $sql="SELECT MIN(duration_ppc), serial_ppc
              FROM price_by_product_by_country ppc
              WHERE ppc.serial_pxc = (SELECT pxc.serial_pxc
                                      FROM product_by_country pxc
                                      WHERE pxc.serial_cou = ".$serial_cou."
                                      AND pxc.serial_pro = ".$serial_pro.")
              AND duration_ppc > ".$days;

        //echo $sql;
        $result = $this->db->Execute($sql);
        
        if($result->fields[0]){
            return $result->fields[1];
        }else{
            return false;
        }
    }

    	 /*
     * @function: getMasivePrice
     * @Description: Retrieves the price of a masive.
     * @returns: sets the first price of a given masive product.
     */
	function getMasivePrice($serial_pro, $serial_cou){
		if($this->serial_pxc!=NULL){
			$sql = 	"SELECT serial_ppc, serial_pxc, duration_ppc, price_ppc, cost_ppc, min_ppc, max_ppc
					FROM price_by_product_by_country
					WHERE serial_pxc=(	SELECT pxc.serial_pxc
										FROM product_by_country pxc
										WHERE pxc.serial_cou = ".$serial_cou."
										AND pxc.serial_pro = ".$serial_pro."	)
					LIMIT 1";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ppc = $result->fields[0];
				$this -> serial_pxc = $result->fields[1];
				$this -> duration_ppc = $result->fields[2];
				$this -> price_ppc = $result->fields[3];
				$this -> cost_ppc = $result->fields[4];
				$this -> min_ppc = $result->fields[5];
				$this -> max_ppc = $result->fields[6];

				return true;
			}else
				return false;
		}else
			return false;
    }

    /*
     * @Name: getProductByCountrySerials
     * @Description: Returns an array of product by country serials for a specific list of products and
     *               countries. This serials are going to be used in the Promotions module.
     * @Params: serial_cou: Specific Country
     *          productList: List of products that are sold in that country
     * @Returns: Country array
     *
    function getProductByCountrySerials($serial_cou, $productList){
        $sql = 	"SELECT pbc.serial_pxc
                FROM product_by_country pbc
                WHERE pbc.serial_cou = '".$serial_cou."'
                AND pbc.serial_pro IN (".$productList.")";
                //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    function productByCountryExists(){
        if($this->serial_pro != NULL && $this->serial_cou != NULL){
            $sql = "SELECT pbc.serial_pxc
                    FROM product_by_country pbc
                    WHERE pbc.serial_pro = '".$this->serial_pro."'
                    AND pbc.serial_cou = '".$this->serial_cou."'";

            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }*/
    ///GETTERS

    function getSerial_ppc(){
        return $this->$serial_ppc;
    }
    function getSerial_pxc(){
        return $this->serial_pxc;
    }
    function getDuration_ppc(){
        return $this->duration_ppc;
    }
    function getPrice_ppc(){
        return $this->price_ppc;
    }
    function getCost_ppc(){
        return $this->cost_ppc;
    }
    function getMin_ppc(){
        return $this->min_ppc;
    }
    function getMax_ppc(){
        return $this->max_ppc;
    }


    ///SETTERS

    function setSerial_ppc($serial_ppc){
        $this->serial_ppc = $serial_ppc;
    }
    function setSerial_pxc($serial_pxc){
        $this->serial_pxc = $serial_pxc;
    }
    function setDuration_ppc($duration_ppc){
        $this->duration_ppc = $duration_ppc;
    }
    function setPrice_ppc($price_ppc){
        $this->price_ppc = $price_ppc;
    }
    function setCost_ppc($cost_ppc){
        $this->cost_ppc = $cost_ppc;
    }
    function setMin_ppc($min_ppc){
        $this->min_ppc = $min_ppc;
    }
    function setMax_ppc($max_ppc){
        $this->max_ppc = $max_ppc;
    }
}
?>