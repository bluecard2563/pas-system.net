<?php
/*
File: UserbyCity.class.php
Author: Esteban Angulo
Creation Date: 27/01/2010 17:00
Last Modified:
*/

class UserbyCity{	
	var $db;
	var $serial_ubc;
	var $serial_usr;
	var $serial_cit;
	
	function __construct($db, $serial_ubc=NULL, $serial_usr = NULL, $serial_cit=NULL){
		$this -> db = $db;
		$this -> serial_ubc = $serial_ubc;
		$this -> serial_usr = $serial_usr;
		$this -> serial_cit = $serial_cit;
	
	}
	
		function getData(){
		if($this->serial_usr!=NULL){
			$sql = 	"SELECT serial_ubc, serial_usr, serial_cit
					FROM user_by_city 
					WHERE serial_usr ='".$this->serial_usr."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ubc =$result->fields[0];
				$this -> serial_usr = $result->fields[1];
				$this -> serial_cit = $result->fields[2];
				
				return true;
			}	
			else
				return false;
		}else
			return false;		
	}
	
	/***********************************************
    * function insert
    * inserts a new register en DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO user_by_city (
                serial_usr,
				serial_cit)
            VALUES(
                '".$this->serial_usr."',";
        $sql.=	"'".$this->serial_cit."')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return 1;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
	/***********************************************
    * function update
    * updates a  register en DB 
    ***********************************************/
    function update() {
        $sql = "
            UPDATE user_by_country SET 
				serial_cit='".$this->serial_cit."'";
		$sql .="WHERE serial_usr = '".$this->serial_usr."'";
       //echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true)
            return true;

        else
            return false;
    }
	function delete(){
			$sql = "
			DELETE FROM user_by_city
			WHERE serial_usr = '".$this->serial_usr."'";	
		//echo $sql;
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	/***********************************************
        * funcion getComissionistsByCountry
        * gets information from Users
        ***********************************************/
	function getComissionistsByCountry($serial_cou, $serial_mbc){
		$sql = 	"SELECT ubc.serial_usr,u.first_name_usr,u.last_name_usr,
				 IF(GROUP_CONCAT(ubd.serial_dea),'true','false') AS is_comissionist
				 FROM user_by_city ubc
				 JOIN user u ON u.serial_usr = ubc.serial_usr
				 LEFT JOIN user_by_dealer ubd ON ubd.serial_usr=ubc.serial_usr AND ubd.status_ubd = 'ACTIVE'
				 JOIN city cit ON cit.serial_cit = ubc.serial_cit
				 WHERE cit.serial_cou='".$serial_cou."'
				 AND u.belongsto_usr='MANAGER'
				 AND u.serial_usr <> 1
				 AND u.serial_usr <> 2";
		if($serial_mbc!='1'){
			$sql.=" AND u.serial_mbc=".$serial_mbc;
		}
		$sql.="  GROUP BY ubc.serial_usr";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}

        function getCitiesByUser($serial_usr){
            $lista=NULL;
            $sql = "SELECT serial_cit
                     FROM user_by_city
                     WHERE serial_usr='".$serial_usr."'";

            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arr=array();
                $cont=0;
                do{
                    $arr[$cont]=$result->fields['0'];
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }
            return $arr;
	}
	
	//GETTERS
	function getSerial_ubc(){
		return $this->serial_ubc;
	}
		function getSerial_usr(){
		return $this->serial_usr;
	}
		function getSerial_cit(){
		return $this->serial_cit;
	}
	
	
	//SETTERS
	function setSerial_ubc($serial_ubc){
		$this->serial_ubc = $serial_ubc;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setSerial_cit($serial_cit){
		$this->serial_cit = $serial_cit;
	}
}


?>