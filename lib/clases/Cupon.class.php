<?php
/*
 * File: Cupon
 * Author: Patricio Astudillo
 * Creation Date: Feb 24, 2011, 11:02:29 AM
 * Description:
 * Modified By:
 * Last Modified:
 */

class Cupon{
	var $db;
	var $serial_cup;
	var $generating_sal;
	var $consuming_sal;
	var $serial_ccp;
	var $expire_date_cup;
	var $limit_amount_cup;
	var $number_cup;
	var $status_cup;

	function __construct($db, $serial_cup = NULL, $generating_sal = NULL, $consuming_sal = NULL, $serial_ccp = NULL,
						$expire_date_cup = NULL, $limit_amount_cup = NULL, $number_cup = NULL, $status_cup = NULL) {
		$this->db = $db;
		$this->serial_cup = $serial_cup;
		$this->generating_sal = $generating_sal;
		$this->consuming_sal = $consuming_sal;
		$this->serial_ccp = $serial_ccp;
		$this->expire_date_cup = $expire_date_cup;
		$this->limit_amount_cup = $limit_amount_cup;
		$this->number_cup = $number_cup;
		$this->status_cup = $status_cup;
	}

	public function insert(){
		$sql = "INSERT INTO cupon(
					serial_cup,
					generating_sal,
					consuming_sal,
					serial_ccp,
					expire_date_cup,
					limit_amount_cup,
					number_cup,
					status_cup
				) VALUES (
					NULL,";
		($this -> generating_sal)?$sql.="{$this -> generating_sal},":$sql.="NULL,";
		($this -> consuming_sal)?$sql.="{$this -> consuming_sal},":$sql.="NULL,";
			$sql.=	"{$this -> serial_ccp},
					STR_TO_DATE('{$this -> expire_date_cup}', '%d/%m/%Y'),
					'{$this -> limit_amount_cup}',
					'{$this -> number_cup}',
					'{$this -> status_cup}')";

		$result = $this -> db -> Execute($sql);

		if($result){
			return $this->db->insert_ID();
		}else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
		}
	}

	public static function getCuponData($db, $serial_cup){
		$sql = "SELECT	generating_sal,
						consuming_sal,
						serial_ccp,
						expire_date_cup,
						limit_amount_cup,
						number_cup,
						status_cup
				FROM cupon
				WHERE serial_cup = {$serial_cup}";

		$result = $db->getRow($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getSerialCuponData($db, $number_cup){
		$sql = "SELECT	generating_sal,
						consuming_sal,
						serial_ccp,
						expire_date_cup,
						limit_amount_cup,
						number_cup,
						status_cup
				FROM cupon
				WHERE number_cup = '$number_cup'";

		$result = $db->getRow($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name consumeCupon
	 * @param <ADODB> $db
	 * @param <cupon_id> $serial_cup
	 * @param <sales_id> $serial_sal
	 * @return <boolean>
	 */
	public static function consumeCupon($db, $serial_cup, $serial_sal){
		$sql = "UPDATE cupon SET
					status_cup = 'USED',
					consuming_sal = $serial_sal
				WHERE serial_cup = $serial_cup
				AND status_cup = 'ACTIVE'";
		
		$result = $db -> Execute($sql);

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * @name reclaimCupon
	 * @param <ADODB> $db
	 * @param <string> $number_cup
	 * @param <sale_id> $serial_sal
	 * @return <Boolean>
	 */
	public static function reclaimCupon($db, $serial_cup, $serial_sal, $serial_usr){
		$sale_data = Sales::getSalesInfoForCustomerPromo($db, $serial_sal);
		$total_sal = $sale_data['total_sal'];
		
		$cupon_data = self::getCuponData($db, $serial_cup);
		$previous_limit = $cupon_data['limit_amount_cup'];

		if(is_array($sale_data) && is_array($cupon_data)){
			$promo_info = CustomerPromo::getPromoInfoForCupon($db, $cupon_data['serial_ccp']);
			
			if($cupon_data['generating_sal'] != ''){ //COUPON CAME FROM A PREVIOUS ONE
				$promo_info['discount_percentage_ccp'] = $promo_info['other_discount_ccp'];
				$promo_info['other_discount_ccp'] = 0;			

				//IF THE LIMIT AMOUNT IS GREATER THAN THE SALE, WE USE THE PERCENTAGE OF THE COUPON
				if($previous_limit > $total_sal){
					$sale_discount = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_sal, 2);
				}else{ //IF NOT, WE USE THE LIMIT AMOUNT
					$sale_discount = $previous_limit;
				}						

				$next_discount = number_format(0, 2);			
			}else{ //IF NOT
				$sale_discount = number_format(($promo_info['discount_percentage_ccp'] / 100) * $total_sal, 2);
				$next_discount = number_format(($promo_info['other_discount_ccp'] / 100) * $total_sal, 2);
			}
			
			$sale_new_total = $total_sal - $sale_discount;
			
			$log_misc['old_sale']['total_sal'] = $total_sal;
			$log_misc['new_sale']['total_sal'] = $sale_new_total;
			
			$log_misc['global_info']['requester_usr'] = $serial_usr;
			$log_misc['global_info']['request_date'] = date('d/m/Y');
			$log_misc['global_info']['request_obs'] = 'Customer Promo - '.$promo_info['name_ccp'].'.  ';
			$log_misc['global_info']['request_obs'] .= html_entity_decode('Cup&oacute;n: ').$promo_info['cupon_prefix_ccp'].' - '.$cupon_data['number_cup'];

			$serial_slg = SalesLog::registerLog($db, 'CUSTOMER_PROMO', $log_misc, $serial_sal, $serial_usr);
			
			if($serial_slg){ //SALES LOG CREATED
				$log_misc['global_info']['authorizing_usr'] = $serial_usr;
				$log_misc['global_info']['authorizing_date'] = date('d/m/Y');
				$log_misc['global_info']['authorizing_obs'] = 'PAS automatic authorization';

				if(SalesLog::serveLog($db, $serial_usr, 'AUTHORIZED', $log_misc, $serial_slg)){ //AUTHORIZE THE SALES_LOG
					if(Sales::updateSaleTotalForCupon($db, $sale_new_total, $serial_sal)){ //UPDATE SALE TOTAL_SAL VALUE
						if(self::consumeCupon($db, $serial_cup, $serial_sal)){ //KILL CUPON
							
							//IF THE CUPON GENERATES ANOTHER ONE, AND IT DOESN'T COME FROM ANOTHER COUPON
							if($next_discount > 0 && $cupon_data['generating_sal'] == ''){
								return self::generateCupon($db, $cupon_data['serial_ccp'], $next_discount, $serial_sal); //GENERATE NEW CUPON
							}
							
							return array('process_code' => 'success');
						}else{
							return array('process_code' => 'consume_cupon_error');
						}
					}else{
						return array('process_code' => 'sales_update_error');
					}
				}else{
					return array('process_code' => 'log_served_error');
				}
			}else{
				return array('process_code' => 'log_creation_error');
			}
		}	
	}

	/**
	 * @name generateCupon
	 * @param <ADODB> $db
	 * @param <customer_id> $serial_ccp
	 * @param <generatingSale_id> $serial_sal
	 */
	public static function generateCupon($db, $serial_ccp, $limit_amount, $serial_sal = NULL){
		$promo_info = CustomerPromo::getPromoInfoForCupon($db, $serial_ccp);

                $coupon_number = self::generateCuponNumber($db, $serial_ccp);

		$xCupon = new Cupon($db);
		$xCupon -> setExpire_date_cup($promo_info['end_date_ccp']);
		$xCupon -> setSerial_ccp($serial_ccp);
		$xCupon -> setStatus_cup('ACTIVE');
		$xCupon -> setLimit_amount_cup($limit_amount);
		$xCupon -> setNumber_cup($coupon_number);
		if($serial_sal) $xCupon -> setGenerating_sal($serial_sal);
		$serial_cup = $xCupon -> insert();
		
		if($serial_cup){ //INSERT FAILED
			return array('process_code' => 'success',
						 'serial_cup' => $serial_cup);
		}else{
			return array('process_code' => 'new_cupon_error');
		}
	}

	public static function validateCupon($db, $number_cup, $cupon_prefix_ccp, $serial_pxc, $scope_ccp = 'SYSTEM',
										$payment_form_ccp = 'CASH'){
		$sql = "SELECT c.serial_cup
				FROM cupon c
				JOIN customer_promo cp ON cp.serial_ccp = c.serial_ccp
					AND cp.status_ccp = 'ACTIVE'
					AND cp.cupon_prefix_ccp = '$cupon_prefix_ccp'
				JOIN applied_products_by_customer_promo apc ON apc.serial_ccp = cp.serial_ccp
					AND apc.serial_pxc = $serial_pxc
				WHERE c.number_cup = '$number_cup'
				AND c.status_cup = 'ACTIVE'
				AND cp.scope_ccp LIKE '%$scope_ccp%'
				AND cp.payment_form_ccp LIKE '%$payment_form_ccp%'";

		$result = $db -> getOne($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getCouponCompleteCode($db, $serial_cup){
		$sql = "SELECT ccp.cupon_prefix_ccp, cup.number_cup
				FROM cupon cup
				JOIN customer_promo ccp ON ccp.serial_ccp = cup.serial_ccp
				WHERE cup.serial_cup = $serial_cup";
		
		$result = $db -> getRow($sql);

		if($result){
			return $result['cupon_prefix_ccp'].' - '.str_pad($result['number_cup'], 8, "0", STR_PAD_LEFT);;
		}else{
			return FALSE;
		}
	}

	public static function generateCuponNumber($db, $serial_ccp, $number = NULL){
		if(!$number){
			$number = rand();
		}

		$generated_number = substr(strtoupper(md5($number)), 0, 11);
		if(!self::couponNumberExist($db, $serial_ccp, $generated_number)){
			return $generated_number;
		}else{
			return self::generateCuponNumber($db, $serial_ccp);
		}
	}

	public static function couponNumberExist($db, $serial_ccp, $number_cup){
		$sql = "SELECT cup.serial_cup
				FROM cupon cup
				WHERE cup.number_cup = '$number_cup'
				AND cup.serial_ccp = $serial_ccp";

		$result = $db -> getOne($sql);

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function getSerial_cup() {
	 return $this->serial_cup;
	}

	public function setSerial_cup($serial_cup) {
	 $this->serial_cup = $serial_cup;
	}

	public function getGenerating_sal() {
	 return $this->generating_sal;
	}

	public function setGenerating_sal($generating_sal) {
	 $this->generating_sal = $generating_sal;
	}

	public function getConsuming_sal() {
	 return $this->consuming_sal;
	}

	public function setConsuming_sal($consuming_sal) {
	 $this->consuming_sal = $consuming_sal;
	}

	public function getSerial_ccp() {
	 return $this->serial_ccp;
	}

	public function setSerial_ccp($serial_ccp) {
	 $this->serial_ccp = $serial_ccp;
	}

	public function getExpire_date_cup() {
	 return $this->expire_date_cup;
	}

	public function setExpire_date_cup($expire_date_cup) {
	 $this->expire_date_cup = $expire_date_cup;
	}

	public function getLimit_amount_cup() {
	 return $this->limit_amount_cup;
	}

	public function setLimit_amount_cup($limit_amount_cup) {
	 $this->limit_amount_cup = $limit_amount_cup;
	}

	public function getNumber_cup() {
	 return $this->number_cup;
	}

	public function setNumber_cup($number_cup) {
	 $this->number_cup = $number_cup;
	}

	public function getStatus_cup() {
	 return $this->status_cup;
	}

	public function setStatus_cup($status_cup) {
	 $this->status_cup = $status_cup;
	}
}
?>
