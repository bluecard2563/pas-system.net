<?php
	/* File : Rest.inc.php
	 * Author : Arun Kumar Sekar
	*/
	class Rest {
		
		public $_allow = array();
		public $_content_type = "application/json";
		public $_request = array();
		
		private $_method = "";		
		private $_code = 200;
		
		//******************* CUSTOM ATTRIBUTES FOR CLASS
		protected $_serial_usr = NULL;
		protected $_db = NULL;
		
		public function __construct(){
			$this->inputs();
			
			//CUSTOM CODE
			global $db;
			$this->_db = $db;
		}
		
		public function get_referer(){
			return $_SERVER['HTTP_REFERER'];
		}
		
		public function response($data, $status){
			$this->_code = ($status)?$status:200;
			$this->set_headers();
			
			echo $this->encodeAsJson($data);
			exit;
		}
		
		private function get_status_message(){
            $status = array(
                100 => 'Continue',
                101 => 'Switching Protocols',
                200 => 'OK',
                201 => 'Created',
                202 => 'Accepted',
                203 => 'Non-Authoritative Information',
                204 => 'No Content',
                205 => 'Reset Content',
                206 => 'Partial Content',
                300 => 'Multiple Choices',
                301 => 'Moved Permanently',
                302 => 'Found',
                303 => 'See Other',
                304 => 'Not Modified',
                305 => 'Use Proxy',
                306 => '(Unused)',
                307 => 'Temporary Redirect',
                400 => 'Bad Request',
                401 => 'Unauthorized',
                402 => 'Payment Required',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Method Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                422 => 'Unprocessable Entity',
                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported',
                600 => 'Days does not match',
                601 => 'No price for this parameters',
                602 => 'No exis product',
                800 => 'No Available Quote',
                850 => 'No Products Available For Given Credentials',
                851 => 'Loading Dealer Data not Possible',
                852 => 'Multiple Counter Registers For credentials.',
                853 => 'No Branch loaded.'
            );
			return ($status[$this->_code])?$status[$this->_code]:$status[500];
		}
		
		public function get_request_method(){
			return $_SERVER['REQUEST_METHOD'];
		}
		
		private function inputs(){
			switch($this->get_request_method()){
				case "POST":
					$this->_request = $this->cleanInputs($_POST);
					break;
				case "GET":
				case "DELETE":
					$this->_request = $this->cleanInputs($_GET);
					break;
				case "PUT":
					parse_str(file_get_contents("php://input"),$this->_request);
					$this->_request = $this->cleanInputs($this->_request);
					break;
				default:
					$this->response('',406);
					break;
			}
		}		
		
		private function cleanInputs($data){
			$clean_input = array();
			if(is_array($data)){
				foreach($data as $k => $v){
					$clean_input[$k] = $this->cleanInputs($v);
				}
			}else{
				if(get_magic_quotes_gpc()){
					$data = trim(stripslashes($data));
				}
				$data = strip_tags($data);
				$clean_input = trim($data);
			}
			return $clean_input;
		}		
		
		private function set_headers(){
			header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
			header("Content-Type:".$this->_content_type);
		}
		
		//********************* SUPPORTING FUNCTIONS *******************
		/**
		 * @access private
		 * @author Patricio Astudillo <pastudillo@planet-assist.net>
		 * @param object $data Data as String or Array to be json encoded
		 * @return JSON
		 */
		public function encodeAsJson($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
		
		/**
		 * @access private
		 * @author Patricio Astudillo <pastudillo@planet-assist.net>
		 * @param String $data Line of text to be cleaned
		 * @return String
		 */
		public function cleanField($data){
			return addslashes(mysql_real_escape_string($data));
		}
		
		//********************** CUSTOM FUNCTION FOR HERENCY *****************
		
		/**
		 * @access private
		 * @author Patricio Astudillo <pastudillo@planet-assist.net>
		 * @param String $PHP_AUTH_USER Key in $_SERVER array.
		 * @param String $PHP_AUTH_PW Key in $_SERVER array.
		 * @return bool YES if Compiles / NO on error.
		 */
		public function validate_login_attempt(){
			$username = $_SERVER['PHP_AUTH_USER'];
			$password = $_SERVER['PHP_AUTH_PW'];

			$pas_user = WSAccountsLib::getLoginInformationForWS($this->_db, $username);
			
			if($pas_user){
				if($password == $pas_user['password_usr']){
					$this -> _serial_usr = $pas_user['serial_usr'];
					return TRUE;
				}
			}
			
			return FALSE;
		}
	}	
?>