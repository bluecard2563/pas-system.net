<?php
/**
 * @author Nicolas
 */
class AppliedBonusLiquidation {

	var $db;
	var $serial_lqn;
	var $serial_apb;
	var $amount_used_abl;

	function __construct($db, $serial_apb = NULL, $serial_sal = NULL, $total_amount_apb = NULL
	) {
		$this->db = $db;
		$this->serial_lqn = $serial_lqn;
		$this->serial_apb = $serial_apb;
		$this->amount_used_abl = $amount_used_abl;
	}

	/*	 * *********************************************
	 * function insert
	 * inserts a new register in DB
	 * ********************************************* */

	function insert() {
		$sql = "
            INSERT INTO applied_bonus_liquidation (
				serial_lqn,
				serial_apb,
				amount_used_abl
			)
            VALUES (
				'" . $this->serial_lqn . "',
				'" . $this->serial_apb . "',
				'" . $this->amount_used_abl . "')";
		// echo $sql;
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}
	
	public static function getLiquidationActInfo($db, $serial_sal){
		$sql = "SELECT GROUP_CONCAT(DISTINCT CAST(
												CONCAT(cou.code_cou, '-', cit.code_cit, '-INC-', LPAD(apl.serial_lqn, 8, '0')) 
												AS CHAR) ORDER BY apl.serial_lqn SEPARATOR ', <br />') AS 'liquitadion_number'		
				FROM applied_bonus_liquidation apl
				JOIN applied_bonus apb ON apb.serial_apb = apl.serial_apb
				JOIN sales s ON s.serial_sal = apb.serial_sal AND s.serial_sal = $serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN dealer d ON d.serial_dea = pbd.serial_dea
				JOIN sector sec ON sec.serial_sec = d.serial_sec
				JOIN city c ON c.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = c.serial_cou
				
				JOIN bonus_liquidation bl ON bl.serial_lqn = apl.serial_lqn
				JOIN user u ON u.serial_usr = bl.serial_usr
				JOIN city cit ON cit.serial_cit = u.serial_cit";
		
		//Debug::print_r($sql);
		$liquidations = $db -> getOne($sql);
		
		if($liquidations){
			return $liquidations;
		}else{
			return 'N/A';
		}
	}
        
        public static function getInfo($db, $serial_lqn){
		$sql = "SELECT * FROM applied_bonus_liquidation where serial_lqn = $serial_lqn";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	function getSerial_lqn() {
		return $this->serial_lqn;
	}

	function getSerial_apb() {
		return $this->serial_apb;
	}

	function getAmount_used_abl() {
		return $this->amount_used_abl;
	}

	function setSerial_lqn($serial_lqn) {
		$this->serial_lqn = $serial_lqn;
	}

	function setSerial_apb($serial_apb) {
		$this->serial_apb = $serial_apb;
	}

	function setAmount_used_abl($amount_used_abl) {
		$this->amount_used_abl = $amount_used_abl;
	}

}
?>
