<?php
/*
File: ProductType.class
Author:Miguel Ponce
Creation Date:24/12/2009
Last modified:02/08/2010
Modified By: David Bergmann
*/

class ProductType {				
	var $db;
	var $serial_tpp;
	var $name_tpp;
	var $has_comision_tpp;	
	var $schengen_tpp;
	var $in_web_tpp;	
	var $flights_tpp;
	var $max_extras_tpp;	
	var $children_tpp;
	var $adults_tpp;
	var $spouse_tpp;
	var $relative_tpp;
	var $third_party_register_tpp;
	var $masive_tpp;
	var $emission_country_tpp;	
	var $residence_country_tpp;	
	var $image_tpp;
	var $senior_tpp;
	var $destination_restricted_tpp;
	var $extras_restricted_to_tpp;
	
	function __construct($db, $serial_tpp = NULL, $name_tpp = NULL, $has_comision_tpp = NULL, $schengen_tpp = NULL,
						 $in_web_tpp = NULL, $flights_tpp= NULL, $max_extras_tpp= NULL, $children_tpp= NULL, $spouse_tpp= NULL,
						 $relative_tpp= NULL, $third_party_register_tpp= NULL, $masive_tpp= NULL, $emission_country_tpp= NULL,
						 $residence_country_tpp= NULL, $image_tpp= NULL, $senior_tpp=NULL, $spouse_tpp=NULL,
						 $destination_restricted_tpp=NULL, $extras_restricted_to_tpp=NULL){
		$this -> db = $db;
		$this -> serial_tpp = $serial_tpp;
		$this -> name_tpp = $name_tpp;
		$this -> has_comision_tpp = $has_comision_tpp;
		$this -> schengen_tpp = $schengen_tpp;
		$this -> in_web_tpp = $in_web_tpp;
		$this -> flights_tpp = $flights_tpp;
		$this -> max_extras_tpp = $max_extras_tpp;
		$this -> children_tpp = $children_tpp;
		$this -> adults_tpp = $children_tpp;
		$this -> spouse_tpp = $spouse_tpp;
		$this -> relative_tpp = $relative_tpp;
		$this -> third_party_register_tpp = $third_party_register_tpp;
		$this -> masive_tpp = $masive_tpp;
		$this -> emission_country_tpp = $emission_country_tpp;
		$this -> residence_country_tpp = $residence_country_tpp;
		$this -> image_tpp = $image_tpp;
		$this -> senior_tpp = $senior_tpp;
		$this -> destination_restricted_tpp = $destination_restricted_tpp;
		$this -> extras_restricted_to_tpp = $extras_restricted_to_tpp;
	}
	
	/* function getData() 		*/
	/* set class parameters		*/
	
	function getData(){
		if($this->serial_tpp!=NULL){
			$sql = 	"SELECT serial_tpp,
							name_tpp,
							has_comision_tpp,
							schengen_tpp,
							in_web_tpp,
							flights_tpp,
							max_extras_tpp,
							children_tpp,
							adults_ttp,
							spouse_tpp,
							relative_tpp,
							third_party_register_tpp,
							masive_tpp,
							emission_country_tpp,
							residence_country_tpp,
							image_tpp,
							destination_restricted_tpp,
							extras_restricted_to_tpp
					FROM product_type
					WHERE serial_tpp='".$this->serial_tpp."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_tpp=$result->fields[0];
				$this->name_tpp=$result->fields[1];
				$this->has_comision_tpp=$result->fields[2];
				$this->schengen_tpp=$result->fields[3];
				$this->in_web_tpp=$result->fields[4];
				$this->flights_tpp=$result->fields[5];
				$this->max_extras_tpp=$result->fields[6];
				$this->children_tpp=$result->fields[7];
				$this->adults_tpp=$result->fields[8];
				$this->spouse_tpp=$result->fields[9];
				$this->relative_tpp=$result->fields[10];
				$this->third_party_register_tpp=$result->fields[11];
				$this->masive_tpp=$result->fields[12];
				$this->emission_country_tpp=$result->fields[13];
				$this->residence_country_tpp=$result->fields[16];
				$this->image_tpp=$result->fields[14];
				$this->senior_tpp=$result->fields[15];
                $this->destination_restricted_tpp=$result[16];
				$this->extras_restricted_to_tpp=$result[17];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/* function insert() 						*/
	/* insert a new product type into database	*/
	
	function insert(){
		$sql="INSERT INTO product_type (
					serial_tpp, 
					has_comision_tpp,
					schengen_tpp,
					in_web_tpp,
					flights_tpp, 
					max_extras_tpp, 
					children_tpp,
					adults_tpp,
					spouse_tpp, 
					relative_tpp, 
					third_party_register_tpp,
					masive_tpp, 
					emission_country_tpp, 
					residence_country_tpp,
					image_tpp,
					senior_tpp,
					destination_restricted_tpp,
					extras_restricted_to_tpp)
			  VALUES(NULL,
					'$this->has_comision_tpp',
					'$this->schengen_tpp',
					'$this->in_web_tpp',
					'$this->flights_tpp',
					'$this->max_extras_tpp',
					'$this->children_tpp',
					'$this->adults_tpp',
					'$this->spouse_tpp',
					'$this->relative_tpp',					
					'$this->third_party_register_tpp',
					'$this->masive_tpp',
					'$this->emission_country_tpp',
					'$this->residence_country_tpp',
					'$this->image_tpp',
					'$this->senior_tpp',
					'$this->destination_restricted_tpp',
					'$this->extras_restricted_to_tpp');";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/* function update() 						*/
	/* update a product type 					*/
	function update(){
		if($this->serial_tpp!=NULL){
			$sql = "UPDATE product_type
					SET has_comision_tpp='$this->has_comision_tpp',
						schengen_tpp='$this->schengen_tpp',
						in_web_tpp='$this->in_web_tpp',
						flights_tpp='$this->flights_tpp',
						max_extras_tpp='$this->max_extras_tpp',
						children_tpp='$this->children_tpp',
						adults_tpp='$this->adults_tpp',
						spouse_tpp='$this->spouse_tpp',
						relative_tpp='$this->relative_tpp',
						third_party_register_tpp='$this->third_party_register_tpp',
						masive_tpp='$this->masive_tpp',
						emission_country_tpp='$this->emission_country_tpp',
						residence_country_tpp='$this->residence_country_tpp',
						image_tpp='$this->image_tpp',
						senior_tpp='$this->senior_tpp',";
			if($this->extras_restricted_to_tpp != NULL) {
				 $sql.="extras_restricted_to_tpp='$this->extras_restricted_to_tpp',";
			}
				 $sql.="destination_restricted_tpp='$this->destination_restricted_tpp'
					WHERE serial_tpp='$this->serial_tpp'";
    			$result = $this->db->Execute($sql);
			if ($result==true) 
				return true;
			else
				return false;
		}else
			return false;		
	}
	
	
	/* function getProductTypes() 				*/
	/* return all the produc types				*/
	
	function getProductTypes($code_lang){
		$list=NULL;
		$sql = 	"SELECT tpp.serial_tpp,
						ptl.name_ptl,
						ptl.description_ptl,
						tpp.has_comision_tpp,
						tpp.schengen_tpp,
						tpp.in_web_tpp,
						tpp.flights_tpp,
						tpp.max_extras_tpp,
						tpp.children_tpp,
						tpp.adults_tpp,
						tpp.spouse_tpp,
						tpp.relative_tpp,
						tpp.third_party_register_tpp,
						tpp.masive_tpp,
						tpp.emission_country_tpp,
						tpp.residence_country_tpp,
						tpp.image_tpp,
						tpp.senior_tpp,
						tpp.destination_restricted_tpp,
						tpp.extras_restricted_to_tpp
				 FROM product_type tpp
				 JOIN ptype_by_language ptl ON tpp.serial_tpp= ptl.serial_tpp
				 JOIN language lang ON lang.serial_lang=ptl.serial_lang AND lang.code_lang='".$code_lang."'";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$count=0;
			
			do{
				$array[$count]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$count++;
			}while($count<$num);
		}
		
		return $array;
	}	
	
	/* function existProdutTypeName() 			*/
	/* returns true if name_tpp already exists	*/

	function existProdutTypeName(){
		if($this->name_tpp != NULL){
			$sql = 	"SELECT serial_tpp
					 FROM product_type
					 WHERE name_tpp= _utf8'".utf8_encode($this->name_tpp)."' collate utf8_bin";
					 
			if($this->serial_tpp != NULL && $this->serial_tpp != ''){
				$sql.= " AND serial_tpp <> ".$this->serial_tpp;
			}		
							 
			$result = $this->db -> Execute($sql);
			if($result->fields[0]){
				return $result->fields[0];
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	
		/* function getProductTypes() 				*/
	/* return all the produc types				*/
	
	function getConditionsByProductType(){
 		if($this->serial_tpp != NULL){
			$sql = 	"SELECT tpp.serial_tpp as 'Serial', name_gcn as 'Condition'
					 FROM product_type tpp 
					 JOIN conditions_by_product_type cbp
					 ON tpp.serial_tpp = cbp.serial_tpp
					 JOIN general_conditions gc
					 ON cbp.serial_gcn = gc.serial_gcn
					 WHERE tpp.serial_tpp=".$this->serial_tpp;

			$result = $this->db->Execute($sql);
			
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
			}
			
			return $array;
		}
		return false;	
	}


	function getProductTypesAutocompleter($code_lang,$name_ptl){
		$list=NULL;
		$sql = 	"SELECT tpp.serial_tpp, ptl.serial_lang, ptl.serial_ptl, ptl.name_ptl, ptl.description_ptl
                            FROM ptype_by_language ptl
                            JOIN product_type tpp ON ptl.serial_tpp = tpp.serial_tpp
                            JOIN language lang ON lang.serial_lang = ptl.serial_lang
                            AND lang.code_lang = '".$code_lang."'
                            WHERE (
                            LOWER( ptl.name_ptl ) LIKE _utf8 '%".$name_ptl."%'
                            COLLATE utf8_bin
                            )";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}
	
	function getInfoProductType($code_lang){
		if($this->serial_tpp!=NULL){
			$sql = "SELECT tpp.serial_tpp,
						   ptl.name_ptl,
						   ptl.description_ptl,
						   tpp.has_comision_tpp,
						   tpp.schengen_tpp,
						   tpp.in_web_tpp,
						   tpp.flights_tpp,
						   tpp.max_extras_tpp,
						   tpp.children_tpp,
						   tpp.adults_tpp,
						   tpp.spouse_tpp,
						   tpp.relative_tpp,
						   tpp.third_party_register_tpp,
						   tpp.masive_tpp,
						   tpp.emission_country_tpp,
						   tpp.residence_country_tpp,
						   tpp.image_tpp,
						   tpp.senior_tpp,
						   tpp.destination_restricted_tpp,
						   tpp.extras_restricted_to_tpp
					FROM product_type tpp
					JOIN ptype_by_language ptl ON tpp.serial_tpp= ptl.serial_tpp
					JOIN language lang ON lang.serial_lang = ptl.serial_lang AND lang.code_lang='".$code_lang."'
					WHERE tpp.serial_tpp='".$this->serial_tpp."'";



			       	$result = $this->db -> Execute($sql);
					$num = $result -> RecordCount();
					if($num > 0){
							$array=array();
							$count=0;
							do{
									$array[$count]=$result->GetRowAssoc(false);
									$result->MoveNext();
									$count++;
							}while($count<$num);
					}
					return $array;

		}else
			return false;
	}
	
	/**
	@Name: getTranslations
	@Description: Returns an array of all the translations for a specific benefit
	@Params: $serial_ben: Benefit ID
	@Returns: An array of translations
	**/
	function getTranslations($serial_ben){
		$sql = 	"	SELECT 	ptl.serial_ptl as 'serial', 
							ptl.name_ptl, 
							l.name_lang, 
							ptl.serial_ptl as 'update', 
							l.serial_lang 
					FROM ptype_by_language ptl 
						JOIN language l 
							ON l.serial_lang=ptl.serial_lang 
					WHERE ptl.serial_tpp = '".$this->serial_tpp."'";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}

	/*
	 * @name: getExtraOptions
	 * @param: $db - DB connection
	 * @returns: An array of all the extra types values in DB
	 */
	public static function getExtraTypes($db){
		$sql = "SHOW COLUMNS FROM product_type LIKE 'extras_restricted_to_tpp'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
	}
	

	///GETTERS
	function getSerial_tpp(){
		return $this->serial_tpp;
	}
	function getName_tpp(){
		return $this->name_tpp;
	}
	function getHas_comision_tpp(){
		return $this->has_comision_tpp;
	}
	function getSchengen_tpp(){
		return $this->schengen_tpp;
	}
	function getIn_web_tpp(){
		return $this->in_web_tpp;
	}
	function getFlights_tpp(){
		return $this->flights_tpp;
	}
	function getMax_extras_tpp(){
		return $this->max_extras_tpp;
	}
	function getChildren_tpp(){
		return $this->children_tpp;
	}
	function getAdults_tpp(){
		return $this->adults_tpp;
	}
	function getSpouse_tpp(){
		return $this->spouse_tpp;
	}
	function getRelative_tpp(){
		return $this->relative_tpp;
	}
	function getThird_party_register_tpp(){
		return $this->third_party_register_tpp;
	}
	function getMasive_tpp(){
		return $this->masive_tpp;
	}
	function getEmission_country_tpp(){
		return $this->emission_country_tpp;
	}
	function getResidence_country_tpp(){
		return $this->residence_country_tpp;
	}
	function getImage_tpp(){
		return $this->image_tpp;
	}
	function getSenior_tpp(){
		return $this->senior_tpp;
	}
	function getDestination_restricted_tpp() {
		return $this->destination_restricted_tpp;
	}
	function getExtras_restricted_to_tpp() {
		return $this->extras_restricted_to_tpp;
	}

	///SETTERS
	function setSerial_tpp($serial_tpp){
		$this->serial_tpp = $serial_tpp;
	}
	function setName_tpp($name_tpp){
		$this->name_tpp = $name_tpp;
	}
	function setHas_comision_tpp($has_comision_tpp){
		$this->has_comision_tpp = $has_comision_tpp;
	}
	function setSchengen_tpp($schengen_tpp){
		$this->schengen_tpp = $schengen_tpp;
	}
	function setIn_web_tpp($in_web_tpp){
		$this->in_web_tpp = $in_web_tpp;
	}	
	function setFlights_tpp($flights_tpp){
		$this->flights_tpp = $flights_tpp;
	}
	function setMax_extras_tpp($max_extras_tpp){
		$this->max_extras_tpp = $max_extras_tpp;
	}
	function setChildren_tpp($children_tpp){
		$this->children_tpp = $children_tpp;
	}
	function setAdults_tpp($adults_tpp){
		$this->adults_tpp = $adults_tpp;
	}
	function setSpouse_tpp($spouse_tpp){
		$this->spouse_tpp = $spouse_tpp;
	}
	function setRelative_tpp($relative_tpp){
		$this->relative_tpp = $relative_tpp;
	}
	function setThird_party_register_tpp($third_party_register_tpp){
		$this->third_party_register_tpp = $third_party_register_tpp;
	}
	function setMasive_tpp($masive_tpp){
		$this->masive_tpp = $masive_tpp;
	}
	function setEmission_country_tpp($emission_country_tpp){
		$this->emission_country_tpp = $emission_country_tpp;
	}
	function setResidence_country_tpp($residence_country_tpp){
		$this->residence_country_tpp = $residence_country_tpp;
	}
    function setImage_tpp($image_tpp){
		$this->image_tpp = $image_tpp;
	}   
	function setSenior_tpp($senior_tpp){
		$this->senior_tpp = $senior_tpp;
	}
	function setDestination_restricted_tpp($destination_restricted_tpp) {
		$this->destination_restricted_tpp = $destination_restricted_tpp;
	}
	function setExtras_restricted_to_tpp($extras_restricted_to_tpp) {
		$this->extras_restricted_to_tpp = $extras_restricted_to_tpp;
	}
}
?>