<?php
/**
 * File: Favorites
 * Author: Patricio Astudillo M.
 * Creation Date: 28/02/2012 
 * Last Modified: 28/02/2012
 * Modified By: Patricio Astudillo
 */

class Favorites {
	var $db;

	function __construct($db)
	{
		$this->db = $db;
	}


	/**
	 *
	 * @param type $db
	 * @param type $serial_usr
	 * @param type $serial_opt
	 * @return type 
	 */
	public static function saveFavorite($db, $serial_usr, $serial_opt){
		 $sql = "INSERT INTO favorites (
					serial_fav,
					serial_usr,
					serial_opt)
				VALUES (
					NULL,
					$serial_usr,
					$serial_opt)";

        $result = $db->Execute($sql);
        
    	if($result){
            return $db->insert_ID();
        }else{
			ErrorLog::log($db, 'INSERT FAILED - Favorites', $sql);
        	return false;
        }
	}
	
	/**
	 *
	 * @param type $db
	 * @param type $serial_usr
	 * @return type 
	 */
	public static function removeFavorites($db, $serial_usr){
		$sql = "DELETE FROM favorites WHERE serial_usr = $serial_usr;";

        $result = $db->Execute($sql);
        
    	if($result){
            return TRUE;
        }else{
        	return false;
        }
	}
	
	/**
	 *
	 * @param type $db
	 * @param type $serial_usr
	 * @return type
	 */
	public static function getMyFavorites($db, $serial_usr){
		$serial_lang = Language::getSerialLangByCode($db, $_SESSION['language']);
		
		$sql = "SELECT distinct opt.serial_opt, opt.link_opt, opt.icon_opt, obl.name_obl, opt.serial_opt, opt.digital_file
				FROM options opt
				JOIN favorites fav ON fav.serial_opt = opt.serial_opt 
				JOIN options_by_language obl ON obl.serial_opt = opt.serial_opt 
				WHERE obl.serial_lang = $serial_lang
				AND fav.serial_usr = $serial_usr
				/*AND opt.show_condition_opt = 1*/
				ORDER BY fav.serial_fav";
		
		//Debug::print_r($sql);
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

    public static function getMarketingPlansFiles($db) {
        $sql = "SELECT *
				FROM marketing_files
				WHERE status_mfl = 'ACTIVE' ";
        $result = $db->getAll($sql);
        if($result){
            return $result;
        }else{
            return FALSE;
        }
    }

	public function hasFavorites($serial_usr, $serial_opt)
	{
		$sql = "SELECT serial_fav
				FROM favorites
				WHERE serial_usr = $serial_usr
				AND serial_opt = $serial_opt";
		$result = $this->db->getOne($sql);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function addUserFavorites($serial_usr, $favorite)
	{
		if (!empty($serial_usr)) {
			// For each favorite option we check if the user has already been added favorites before a new insert
			$hasFavorite = (int)$this->hasFavorites($serial_usr, $favorite);
			if (empty($hasFavorite)) {
				// Insert each favorite option to the new user.
				$sql = "INSERT INTO favorites (serial_usr, serial_opt) VALUES ($serial_usr, $favorite);";
				$result = $this->db->Execute($sql);
				if ($result) {
					return true;
				} else {
					ErrorLog::log($this->db, 'INSERT THE NEW USER FAVORITES FAILED - ' . get_class($this), $sql);
					return false;
				}
			}
		}
	}
}
