<?php
/*
File: ManagerbyCountry.class.php
Author: Esteban Angulo
Creation Date: 07/01/2010 15:30
Last Modified:07/01/2010
*/
class ManagerbyCountry {
	var $db;
	var $serial_mbc;
	var $serail_cou;
	var $serial_man;
	var $percentage_mbc;
	var $type_percentage_mbc;
	var $status_mbc;
	var $payment_deadline_mbc;
	var $invoice_number_mbc;
	var $exclusive_mbc;
	var $official_mbc;
	var $last_commission_paid_mbc;

	function __construct($db, $serial_mbc = NULL, $serial_cou = NULL, $serial_man=NULL, $percentage_mbc=NULL, $type_percentage_mbc=NULL, $status_mbc=NULL, $payment_deadline_mbc=NULL, $invoice_number_mbc=NULL, $exclusive_mbc=NULL, $official_mbc=NULL, $last_commission_paid_mbc=NULL){
		$this -> db = $db;
		$this -> serial_mbc = $serial_mbc;
		$this -> serial_cou = $serial_cou;
		$this -> serial_man = $serial_man;
		$this -> percentage_mbc = $percentage_mbc;
		$this -> type_percentage_mbc = $type_percentage_mbc;
		$this -> status_mbc = $status_mbc;
		$this -> payment_deadline_mbc = $payment_deadline_mbc;
		$this -> invoice_number_mbc = $invoice_number_mbc;
		$this -> exclusive_mbc = $exclusive_mbc;
		$this -> official_mbc = $official_mbc;
		$this -> last_commission_paid_mbc = $last_commission_paid_mbc;
	}

	function getData(){
		if($this->serial_mbc!=NULL){
			$sql = 	"SELECT serial_mbc, serial_cou, serial_man, percentage_mbc, type_percentage_mbc, 
							status_mbc, payment_deadline_mbc, invoice_number_mbc, exclusive_mbc, official_mbc,
							DATE_FORMAT(last_commission_paid_mbc, '%d/%m/%Y %T')
					FROM manager_by_country
					WHERE serial_mbc='".$this->serial_mbc."'";

			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_mbc =$result->fields[0];
				$this -> serial_cou = $result->fields[1];
				$this -> serial_man = $result->fields[2];
				$this -> percentage_mbc = $result->fields[3];
				$this -> type_percentage_mbc = $result->fields[4];
				$this -> status_mbc = $result->fields[5];
				$this -> payment_deadline_mbc = $result->fields[6];
				$this -> invoice_number_mbc = $result->fields[7];
				$this -> exclusive_mbc = $result->fields[8];
				$this -> official_mbc = $result->fields[9];
				$this -> last_commission_paid_mbc = $result->fields[10];
				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/***********************************************
	* function getGeoData
	* gets geoData by $serial_mbc
	***********************************************/
	function getGeoData($serial_mbc){
		if($serial_mbc){
			$sql = 	"SELECT cou.serial_cou, cou.serial_zon, mbc.serial_mbc
					 FROM manager_by_country mbc
					 JOIN country cou ON cou.serial_cou=mbc.serial_cou
					 WHERE mbc.serial_mbc='".$serial_mbc."'";

			$result = $this->db -> Execute($sql);
                        $num = $result -> RecordCount();
                        return $result->GetRowAssoc(false);
                }
        }

	/***********************************************
    * funcion getAllPaymentDeadline
    * Returns all kind of Types which exist in DB
    ***********************************************/
	function getAllPaymentDeadline(){
		// Finds all possible status form ENUM column
		$sql = "SHOW COLUMNS FROM manager_by_country LIKE 'payment_deadline_mbc'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/***********************************************
    * funcion getAllStatus
    * Returns all kind of Status which exist in DB
    ***********************************************/
	function getAllStatus(){
		// Finds all possible status form ENUM column
		$sql = "SHOW COLUMNS FROM manager_by_country LIKE 'status_mbc'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/***********************************************
    * funcion getAllInvoiceNumber
    * Returns all kind of InvoiceNumber which exist in DB
    ***********************************************/
	function getAllInvoiceNumber(){
		// Finds all possible status form ENUM column
		$sql = "SHOW COLUMNS FROM manager_by_country LIKE 'invoice_number_mbc'";
		$result = $this->db -> Execute($sql);

		$number=$result->fields[1];

		$number = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$number));
		return $number;
	}

	/***********************************************
    * funcion isExclusive
    * verifies if a manager by country is exclusive or not
    ***********************************************/

	function isExclusive($serial_mbc,$serial_cou){
			$sql = 	"SELECT mbc.serial_mbc,mbc.serial_cou,mbc.exclusive_mbc
					 FROM manager_by_country mbc
					 WHERE mbc.exclusive_mbc ='1'
					 AND serial_cou = ".$serial_cou;

			if($serial_mbc != NULL && $serial_mbc != ''){
				$sql.= " AND serial_mbc <> ".$serial_mbc;
			}
			//echo $sql;
			$result = $this->db -> Execute($sql);

			if($result->fields[0]){
			return $this->db->fields[0];
		}else{
			return false;
		}
	}

	/*********************************************************************
	* funcion getExclusiveCountries
	* gets all countries with an exclusive manager or an official manager
	*********************************************************************/

	function getExclusiveCountries(){
		$sql =	"SELECT cou.name_cou, cou.serial_cou, mbc.serial_exclusive_mbc, mbc.exclusive_mbc, mbc.official_mbc, mbc.serial_official_mbc
				 FROM (SELECT mb.exclusive_mbc, mb.serial_mbc AS serial_exclusive_mbc, mb.serial_cou, mc.official_mbc, mc.serial_mbc AS serial_official_mbc
					   FROM manager_by_country mb
					   RIGHT JOIN manager_by_country mc ON mb.serial_cou = mc.serial_cou
					   WHERE mb.exclusive_mbc = 'YES'
					   AND mc.official_mbc = 'YES'
					   GROUP BY serial_cou
					   UNION
					   SELECT mb.exclusive_mbc, mb.serial_mbc AS serial_exclusive_mbc, mb.serial_cou, mc.official_mbc, mc.serial_mbc AS serial_official_mbc
					   FROM manager_by_country mb
					   RIGHT JOIN manager_by_country mc ON mb.serial_cou = mc.serial_cou
					   WHERE mb.serial_cou <> ALL(SELECT mb.serial_cou
												  FROM manager_by_country mb
												  RIGHT JOIN manager_by_country mc ON mb.serial_cou = mc.serial_cou
												  WHERE mb.exclusive_mbc = 'YES'
												  AND mc.official_mbc = 'YES')
					   AND mc.official_mbc = 'YES'
					   GROUP BY serial_cou
					   UNION
					   SELECT mb.exclusive_mbc, mb.serial_mbc AS serial_exclusive_mbc, mb.serial_cou, mc.official_mbc, mc.serial_mbc AS serial_official_mbc
					   FROM manager_by_country mb
					   RIGHT JOIN manager_by_country mc ON mb.serial_cou = mc.serial_cou
					   WHERE mb.exclusive_mbc = 'YES'
					   AND mb.serial_cou <> ALL(SELECT mb.serial_cou
												FROM manager_by_country mb
												RIGHT JOIN manager_by_country mc ON mb.serial_cou = mc.serial_cou
												WHERE mb.exclusive_mbc = 'YES'
												AND mc.official_mbc = 'YES')
					   GROUP BY serial_cou
                      ) mbc
				 RIGHT JOIN country cou ON cou.serial_cou=mbc.serial_cou
				 WHERE cou.serial_cou<>1
				 GROUP BY serial_cou
				 ORDER BY name_cou";
		//die($sql);
		$result = $this->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arr;
	}

	/***********************************************
    * funcion getSerialCountriesByUser
    * Returns all countries'serials from an specific
	***********************************************/

	function getSerialCountriesByUser($serial_man){
		$sql = 	"SELECT DISTINCT mbc.serial_cou, mbc.serial_man
		FROM manager_by_country mbc
		WHERE mbc.serial_man='".$serial_man."'
		";
		//echo $sql;
		$result = $this->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arr;
	}

	/***********************************************
    * funcion getAllManagers
    * Returns all Managers whose status is Active
    ***********************************************/
	function getAllManagers($serial_cou,$serial_man){
		$sql = "SELECT  u.first_name_usr,u.last_name_usr, serial_mbc, mbc.serial_cou
				FROM user u JOIN manager_by_country mbc ON u.serial_man=mbc.serial_man
				WHERE mbc.serial_cou =".$serial_cou;
		$sql.= " AND u.serial_man <> ".$serial_man;
//echo $sql;
		$result = $this->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}

	/***********************************************
    * funcion getAllPercentageTypes
    * Returns all kind of Percentage Types which exist in DB
    ***********************************************/
	function getAllPercentageTypes(){
		// Finds all possible status form ENUM column
		$sql = "SHOW COLUMNS FROM manager_by_country LIKE 'type_percentage_mbc'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/***********************************************
    * funcion updateManager
    * updates a register in DB
    ***********************************************/
    function updateStatus_mbc($serial_mbc,$status_mbc) {
        $sql = "UPDATE manager_by_country SET status_mbc = '".$status_mbc."'";
        $sql .=" WHERE serial_mbc = '".$serial_mbc."'";
       //echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;
        if($result==true)
            return true;
        else
            return false;
    }


	/***********************************************
    * function insert
    * inserts a new register en DB
    ***********************************************/
	function insert() {
        $sql = "
            INSERT INTO manager_by_country (
                serial_cou,
        		serial_man,
                percentage_mbc,
                type_percentage_mbc,
                status_mbc,
                payment_deadline_mbc,
                invoice_number_mbc,
                exclusive_mbc,
                official_mbc,
                last_commission_paid_mbc)
            VALUES(
                '".$this->serial_cou."',";
        $sql.=	"'".$this->serial_man."',
		'".$this->percentage_mbc."',
		'".$this->type_percentage_mbc."',
                '".$this->status_mbc."',
                '".$this->payment_deadline_mbc."',
                '".$this->invoice_number_mbc."',
                '".$this->exclusive_mbc."',
                '".$this->official_mbc."',
                NOW())";
        //die($sql);
        $result = $this->db->Execute($sql);
        
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE manager_by_country SET";
        $sql.=" serial_cou		='".$this->serial_cou."',
				serial_man		='".$this->serial_man."',
				percentage_mbc	='".$this->percentage_mbc."',
                type_percentage_mbc	='".$this->type_percentage_mbc."',
                status_mbc	='".$this->status_mbc."',
                payment_deadline_mbc		='".$this->payment_deadline_mbc."',
                invoice_number_mbc		='".$this->invoice_number_mbc."',
				exclusive_mbc = '".$this->exclusive_mbc."',
                official_mbc = '".$this->official_mbc."'";
        $sql .="WHERE serial_mbc = '".$this->serial_mbc."'";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false) return false;
        if($result==true)
            return true;
        else
            return false;
    }
	/***********************************************
    * funcion updateLastComissionPaid
    * updates the last date a comission was paid.
	 * * Parameter: atribute serial_mbc
    ***********************************************/
    function updateLastComissionPaid() {
        $sql = "
                UPDATE manager_by_country SET last_commission_paid_mbc = NOW() WHERE serial_mbc = '".$this->serial_mbc."'";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false) return false;
        if($result==true)
            return true;
        else
            return false;
    }

	/**
	@Name: getManagerByCountry
	@Description: Returns an array of managers.
	@Params: country ID
	@Returns: ManagerID array
	**/
	function getManagerByCountry($serial_cou, $serial_mbc){
		$sql =	"SELECT mbc.serial_mbc, m.serial_man, m.name_man, mbc.official_mbc, m.contact_email_man,
                                m.contact_man, mbc.type_percentage_mbc, mbc.percentage_mbc,
                                mbc.payment_deadline_mbc, mbc.exclusive_mbc, mbc.official_mbc, mbc.serial_man
				FROM manager_by_country mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				WHERE mbc.serial_cou=".$serial_cou;

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		$result = $this ->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /**
	@Name: getManagerByCountry
	@Description: Returns an array of managers.
	@Params: country ID
	@Returns: ManagerID array
	**/
	function getPaymentManagerByCountry($serial_cou){
		$sql =	"SELECT DISTINCT mbc.serial_mbc, m.name_man, mbc.official_mbc, m.contact_email_man,
                                m.contact_man, mbc.type_percentage_mbc, mbc.percentage_mbc,
                                mbc.payment_deadline_mbc, mbc.exclusive_mbc, mbc.official_mbc
				FROM manager_by_country mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				JOIN dealer dea ON dea.serial_mbc=mbc.serial_mbc
				JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				JOIN sales s ON cnt.serial_cnt=s.serial_cnt
				JOIN invoice i ON i.serial_inv=s.serial_inv AND (i.status_inv='STAND-BY' OR i.status_inv='VOID')
				WHERE mbc.serial_cou=".$serial_cou;
		$result = $this ->db -> Execute($sql);

		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	/**
	@Name: getManagerWithComissionByCountry
	@Description: Returns an array of managers.
	@Params: country ID
	 *		$is_report:
	@Returns: managers array
	**/
	function getManagerWithComissionByCountry($serial_cou, $serial_mbc, $is_report=false){
		$sql =	"SELECT DISTINCT mbc.serial_mbc, m.name_man, mbc.official_mbc, m.contact_email_man
				FROM manager_by_country mbc
				JOIN manager m ON m.serial_man=mbc.serial_man
				JOIN applied_comissions acm ON acm.serial_mbc=mbc.serial_mbc
				WHERE mbc.serial_cou=$serial_cou";

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
			//echo $sql;
		$result = $this ->db -> getAll($sql);
		if($result)
			return $result;
		else
			return false;
	}

    /**
	@Name: getInvoiceManagersByCountry
	@Description: Returns an array of managers.
	@Params: country ID
	@Returns: ManagerID array
	**/
	function getInvoiceManagersByCountry($serial_cou, $serial_mbc){
		$sql =	"SELECT DISTINCT mbc.serial_mbc, m.name_man
				 FROM manager_by_country mbc
				 JOIN manager m ON m.serial_man=mbc.serial_man
				 JOIN dealer dea ON dea.serial_mbc=mbc.serial_mbc
				 JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				 JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' AND s.free_sal ='NO' AND s.serial_inv IS NULL
				 JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				 JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				 JOIN product p ON p.serial_pro=pxc.serial_pro AND ((p.masive_pro='NO') OR (p.masive_pro='YES' AND s.sal_serial_sal IS NOT NULL))
				 WHERE mbc.serial_cou=".$serial_cou;

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		$sql.=" ORDER BY m.name_man";

		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /**
	@Name: getInvoiceLogManagersByCountry
	@Description: Returns an array of managers.
	@Params: country ID
	@Returns: ManagerID array
	**/
	function getInvoiceLogManagersByCountry($serial_cou, $serial_mbc){
		$sql =	"SELECT DISTINCT mbc.serial_mbc, m.name_man
				FROM manager_by_country mbc
				 JOIN manager m ON m.serial_man=mbc.serial_man
				 JOIN dealer dea ON dea.serial_mbc=mbc.serial_mbc
				 JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				 JOIN sales s ON cnt.serial_cnt=s.serial_cnt
				 JOIN invoice inv ON inv.serial_inv=s.serial_inv
				 JOIN invoice_log inl ON inl.serial_inv=inv.serial_inv AND inl.status_inl='PENDING'
				 WHERE mbc.serial_cou=".$serial_cou;

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		$sql.=" ORDER BY m.name_man";

		//echo $sql;
		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/**
	@Name: getManagersByManager
	@Description: Returns an array of managers.
	@Returns: Manager array
	**/
	function getManagersByManager(){
		$list=NULL;
		$sql =	"SELECT mbc.serial_mbc, man.name_man, cou.name_cou, mbc.serial_cou, mbc.serial_man, mbc.percentage_mbc , mbc.status_mbc, mbc.payment_deadline_mbc, mbc.type_percentage_mbc, mbc.invoice_number_mbc, mbc.exclusive_mbc, mbc.official_mbc
				FROM manager_by_country mbc
				JOIN manager man ON man.serial_man=mbc.serial_man
				JOIN country cou ON cou.serial_cou=mbc.serial_cou
				WHERE mbc.serial_man=".$this->serial_man;
		//die($sql);
		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/**
	@Name: getMaxSerial
	@Description: Returns max serial
	@Params: nothing
	@Returns: max serial
	**/
	function getMaxSerial(){
		$list=NULL;
		$sql =	"SELECT MAX(mbc.serial_mbc) as max_serial
				FROM manager_by_country mbc";
		//die($sql);
		$result = $this->db -> Execute($sql);

			if($result->fields[0]){
				return $result->fields[0];
			}else{
				return false;
			}
	}

	/**
	@Name: getManagerByCountry
	@Description: Returns an array of country ID's.
	@Params: user ID
	@Returns: CountryID array
	**/
	function getCountriesByManager($serial_man){
		$list=NULL;
		$sql =	"SELECT mbc.serial_cou
				FROM manager_by_country mbc
				WHERE mbc.serial_man=".$serial_man;

		//echo $sql;

		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /**
	@Name: getMainManager
	@Description: Returns the ID of the main manager.
	@Params: N/A
	@Returns: ManagerByCountry ID
	**/
	function getMainManager(){
		$sql =	"SELECT mbc.serial_cou
				 FROM manager_by_country mbc
				 JOIN manager m ON m.serial_man=mbc.serial_man
				 WHERE m.man_serial_man IS NULL";
		//echo $sql;

		$result = $this ->db -> Execute($sql);

		if($result==true){
                    return $result->fields['0'];
                }else{
                    return false;
                }
	}

	/***********************************************
	* funcion hasOfficialSeller
	* verifies if a Manager has an official seller
	***********************************************/
	function hasOfficialSeller(){
		$sql = 	"SELECT mbc.serial_mbc
				 FROM manager_by_country mbc
				 LEFT JOIN dealer d ON d.serial_mbc=mbc.serial_mbc
				 WHERE d.official_seller_dea = 'YES'
				 AND mbc.serial_mbc = ".$this->serial_mbc;
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

	/***********************************************
	* funcion getOfficialSeller
	* gets the official seller from a Manager
	***********************************************/
	public static function getOfficialSeller($db, $serial_mbc){
		$sql = 	"SELECT d.serial_dea
				 FROM manager_by_country mbc
				 LEFT JOIN dealer d ON d.serial_mbc=mbc.serial_mbc
				 WHERE d.official_seller_dea = 'YES'
				 AND mbc.serial_mbc = $serial_mbc";
		$result = $db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

	/***********************************************
	* function getResponsablesByManager
	@Name: getResponsablesByManager
	@Description: get the users wich are responsables of at least one branch.
	@Params:
                None.(uses object serial_mbc atribute)
	@Returns: responsables: array.
         *        false on errors
	***********************************************/
	public static function getResponsablesByManager($db, $serial_mbc){
		$sql = "SELECT DISTINCT u.serial_usr, u.first_name_usr, u.last_name_usr
				FROM user u
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
				AND ubd.status_ubd = 'ACTIVE'
				AND u.status_usr = 'ACTIVE'
				JOIN dealer d ON d.serial_mbc =$serial_mbc
				AND ubd.serial_dea = d.serial_dea";

       // die($sql);
		$result=$db->getAll($sql);

		if($result){
				return $result;
		}else{
				return NULL;
		}
	}

    /***********************************************
	* function getInvoiceResponsablesByManager
	@Name: getInvoiceResponsablesByManager
	@Description: get the users wich are responsables of at least one branch.
	@Params:
                None.(uses object serial_mbc atribute)
	@Returns: responsables: array.
         *        false on errors
	***********************************************/
        function getInvoiceResponsablesByManager(){
		$sql="SELECT DISTINCT u.serial_usr,u.first_name_usr,u.last_name_usr
			  FROM user u
			  JOIN user_by_dealer ubd ON ubd.serial_usr=u.serial_usr AND ubd.status_ubd='ACTIVE'
			  JOIN dealer dea ON dea.serial_dea=ubd.serial_dea AND dea.serial_mbc=".$this->serial_mbc."
			  JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
			  JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' AND s.free_sal ='NO'
			  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			  JOIN product p ON p.serial_pro=pxc.serial_pro AND ((p.masive_pro='NO') OR (p.masive_pro='YES' AND s.sal_serial_sal IS NOT NULL))
			  AND s.serial_inv IS NULL";

                 //die($sql);
		$result = $this->db->Execute($sql);
		if ($result === false) die("failed");
			$num = $result -> RecordCount();
		if($num > 0){
			$list=array();
			$cont=0;

			do{
				$list[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $list;
        }

		/**
	@Name: getManagersByCountryWithInvoices
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersByCountryWithInvoices($serial_cou, $serial_mbc, $dea_serial_dea) {
		$sql = "SELECT DISTINCT mbc.serial_mbc, man.name_man
				FROM sales s
				JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
				JOIN dealer br ON cnt.serial_dea = br.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = br.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv = 'STAND-BY'
				WHERE mbc.serial_cou=" . $serial_cou ;

		if ($serial_mbc != '1') {
			$sql.=" AND mbc.serial_mbc=" . $serial_mbc;
		}
		if ($dea_serial_dea) {
			$sql.=" AND br.dea_serial_dea=" . $dea_serial_dea;
		}		
		$sql.=" ORDER BY man.name_man";
		
		//Debug::print_r($sql); die;
		
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	@Name: getManagersWithExcessPayments
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersWithExcessPayments($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(mbc.serial_mbc), man.name_man
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
                   JOIN manager man ON man.serial_man=mbc.serial_man
				   JOIN sector sec ON br.serial_sec=sec.serial_sec
				   JOIN city cit ON sec.serial_cit=cit.serial_cit
				   JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
				   LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
                   JOIN payments pay ON pay.serial_pay=inv.serial_pay AND pay.status_pay='EXCESS'";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY man.name_man";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getManagersWithPayments
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersWithPayments($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(mbc.serial_mbc), man.name_man
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
                   JOIN manager man ON man.serial_man=mbc.serial_man
				   JOIN sector sec ON br.serial_sec=sec.serial_sec
				   JOIN city cit ON sec.serial_cit=cit.serial_cit
				   JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
				   LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
                   JOIN payments pay ON pay.serial_pay=inv.serial_pay";

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.="  ORDER BY man.name_man";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getManagersWithFreeCard
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersWithFreeCard($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(mbc.serial_mbc), man.name_man
				 FROM sales sal
					   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					   JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					   JOIN manager man ON man.serial_man=mbc.serial_man
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
				   WHERE sal.free_sal = 'YES'";
		if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY man.name_man";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

		/**
	@Name: getManagersWithPaymentDetail
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersWithPaymentDetail($serial_cou, $serial_mbc){
		$sql =  "SELECT distinct(man.serial_man), man.name_man
				 FROM sales s
				 JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				 JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
                 JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc
                 JOIN manager man ON man.serial_man=mbc.serial_man
				 JOIN sector sec ON dea.serial_sec=sec.serial_sec
				 JOIN city cit ON sec.serial_cit=cit.serial_cit
				 JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=".$serial_cou."
				 LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
                 JOIN payments pay ON pay.serial_pay=inv.serial_pay
				 JOIN payment_detail pyf ON pyf.serial_pay=pay.serial_pay AND checked_pyf='NO'";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}

		$sql.=" ORDER BY man.name_man";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/*
	 * getManagersWithUncollectableInvoices
	 * @param: $db - DB connection
	 * @param: $param - Time in days to take the uncollectable invoices.
	 * @return: An array of all the managers with uncollerctable invoices.
	 */
	public static function getManagersWithUncollectableInvoices($db, $param){
		$sql="SELECT DISTINCT mbc.serial_mbc, m.serial_man, m.name_man
			  FROM manager_by_country mbc
			  JOIN manager m ON m.serial_man=mbc.serial_man
			  JOIN dealer b ON b.serial_mbc=mbc.serial_mbc
			  JOIN counter cnt ON cnt.serial_dea=b.serial_dea
			  JOIN sales s ON s.serial_cnt=cnt.serial_cnt
			  JOIN invoice i ON i.serial_inv=s.serial_inv AND i.status_inv = 'STAND-BY'
			  WHERE DATEDIFF(NOW(), i.date_inv) >= $param";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * @name: getManagersWithDealers
	 * @param: $db - DB connection
	 * @param: $serial_cou - Country ID
	 * @returns: An array of all managers_by_country that have dealers under their command.
	 */
	public static function getManagersWithDealers($db, $serial_cou, $serial_mbc){
		$sql="SELECT DISTINCT mbc.serial_mbc, m.serial_man, m.name_man
			  FROM manager_by_country mbc
			  JOIN manager m ON m.serial_man=mbc.serial_man
			  JOIN dealer d ON d.serial_mbc=mbc.serial_mbc AND d.dea_serial_dea IS NULL
			  WHERE mbc.serial_cou=".$serial_cou;

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc IN (".$serial_mbc.")";
		}
		
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	/**
	@Name: getManagersByCountryWithRefunds
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersByCountryWithRefunds($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(mbc.serial_mbc), man.name_man
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					JOIN manager man ON man.serial_man=mbc.serial_man
					JOIN sector sec ON br.serial_sec=sec.serial_sec
				   JOIN city cit ON sec.serial_cit=cit.serial_cit
				   JOIN country cou ON cit.serial_cou=cou.serial_cou                   
				   JOIN refund ref ON ref.serial_sal=s.serial_sal
				   WHERE cou.serial_cou=".$serial_cou;

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY man.name_man";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}


    /**
	@Name: getMasiveManagersByCountry
	@Description: Returns an array of managers.
	@Params: country ID
	@Returns: ManagerID array
	**/
	function getMasiveManagersByCountry($serial_cou, $serial_mbc){
		$sql =	"SELECT DISTINCT mbc.serial_mbc, m.name_man
				 FROM manager_by_country mbc
				 JOIN manager m ON m.serial_man=mbc.serial_man
				 JOIN dealer dea ON dea.serial_mbc=mbc.serial_mbc
				 JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				 JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED'
				 JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				 JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				 JOIN product p ON p.serial_pro=pxc.serial_pro AND p.masive_pro='YES' AND s.sal_serial_sal IS NULL
				 WHERE mbc.serial_cou=".$serial_cou.
				 " AND NOW() < s.end_date_sal";
		if($serial_mbc!="1"){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}

		$sql.=" ORDER BY m.name_man";

		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/* function getMasiveResponsablesByManager
	@Name: getMasiveResponsablesByManager
	@Description: get the users wich are responsables of at least one branch.
	@Params:
                None.(uses object serial_mbc atribute)
	@Returns: responsables: array.
         *        false on error*/
        function getMasiveResponsablesByManager(){
		$sql="SELECT DISTINCT u.serial_usr,u.first_name_usr,u.last_name_usr
			  FROM user u
			  JOIN user_by_dealer ubd ON ubd.serial_usr=u.serial_usr AND ubd.status_ubd='ACTIVE'
			  JOIN dealer dea ON dea.serial_dea=ubd.serial_dea AND dea.serial_mbc=".$this->serial_mbc."
			  JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
			  JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED'
			  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			  JOIN product p ON p.serial_pro=pxc.serial_pro
			  	AND p.masive_pro='YES'
				AND s.sal_serial_sal IS NULL";

                 //die($sql);
		$result = $this->db->Execute($sql);
		if ($result === false) die("failed");
			$num = $result -> RecordCount();
		if($num > 0){
			$list=array();
			$cont=0;

			do{
				$list[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $list;
        }

	/**
	@Name: getManagersSalesAnalysis
	@Description: Retrieves the information of a specific manager list
	@Params:
	@Returns: Manager array
	**/
	function getManagersSalesAnalysis($serial_cou, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT distinct(mbc.serial_mbc), man.name_man
					FROM sales sal
						JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
						JOIN dealer br ON cnt.serial_dea=br.serial_dea
						JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
                        JOIN manager man ON man.serial_man=mbc.serial_man
      				    JOIN sector sec ON br.serial_sec=sec.serial_sec
      			       	JOIN city cit ON sec.serial_cit=cit.serial_cit
						JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou = ".$serial_cou."
						JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
						JOIN payments pay ON pay.serial_pay=inv.serial_pay
                    WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')  AND sal.serial_inv IS NOT NULL
                           AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY man.name_man";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getOfficialManagersByCountry
	@Description: Retrieves a list of official managers
	@Params:
	@Returns: Manager array
	**/
	public static function getOfficialManagersByCountry($db, $serial_cou = NULL){
		$sql = "SELECT mbc.serial_mbc, man.name_man, cou.name_cou
				FROM manager_by_country mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				JOIN country cou ON cou.serial_cou = mbc.serial_cou
				WHERE mbc.official_mbc = 'YES'";
		if($serial_cou != NULL) {
			$sql .= " AND mbc.serial_cou = $serial_cou";
		}
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return NULL;
		}
	}

	/**
	@Name: getUsersByManagerByCountry
	@Description: Retrieves a list of official managers
	@Params:
	@Returns: Manager array
	**/
	public static function getUsersByManagerByCountry($db, $serial_mbc, $serial_dea){
		$sql = "SELECT DISTINCT usr.serial_usr, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr
				FROM user usr
				WHERE usr.serial_mbc = $serial_mbc
				AND usr.status_usr = 'ACTIVE'
				AND usr.serial_usr NOT IN(SELECT cnt.serial_usr
										  FROM counter cnt
										  JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
										  WHERE dea.serial_dea = $serial_dea)
				AND usr.serial_usr <> '1'
		
				UNION
		
				SELECT DISTINCT usr.serial_usr, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr
				FROM user usr
				LEFT JOIN dealer dea ON dea.serial_dea = usr.serial_dea AND dea.official_seller_dea = 'YES'
				WHERE dea.serial_mbc = $serial_mbc
				AND usr.status_usr = 'ACTIVE'
				AND usr.serial_usr NOT IN(SELECT cnt.serial_usr
										  FROM counter cnt
										  JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
										  WHERE dea.serial_dea = $serial_dea)
				AND usr.serial_usr <> '1'
				ORDER BY 2";
		//die($sql);
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return NULL;
		}
	}

	/*
	 * @name: getManagersWithBonus
	 * @param: $db - DB connection
	 * @return: Array with cities
	 */
	public static function getManagersWithBonus($db, $serial_cou, $serial_mbc, $bonus_to = NULL, 
												$at_least_one_paid = NULL, $authorized = NULL){
		$sql = "SELECT DISTINCT mbc.serial_mbc, man.name_man
				FROM manager man
				JOIN manager_by_country mbc ON mbc.serial_man=man.serial_man AND mbc.serial_cou=$serial_cou
				JOIN dealer b ON b.serial_mbc=mbc.serial_mbc
				JOIN counter cnt ON cnt.serial_dea=b.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal";

		if($serial_mbc != '1') $sql .= " AND mbc.serial_mbc = $serial_mbc";
		if($bonus_to) $sql .= " AND apb.bonus_to_apb = '$bonus_to'";
		if($at_least_one_paid) $sql .= " JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb";
		if($authorized) $sql.=" AND apb.ondate_apb = 'NO'
								AND apb.authorized_apb = 'YES'";

		$result=$db->getAll($sql);

		if($result){
				return $result;
		}else{
				return NULL;
		}
	}

	public static function getManagerByCountryName($db, $serial_mbc){
		$sql = "SELECT man.name_man
				FROM manager_by_country mbc
				JOIN manager man ON man.serial_man=mbc.serial_mbc
				WHERE mbc.serial_mbc=".$serial_mbc;

		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getManagerCommonData($db, $serial_man){
		$sql = "SELECT DISTINCT mbc.percentage_mbc, mbc.type_percentage_mbc, mbc.payment_deadline_mbc,
					   mbc.invoice_number_mbc
				FROM manager_by_country mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				WHERE man.serial_man=".$serial_man;

		$result=$db->getAll($sql);

		if($result){
			return $result[0];
		}else{
			return FALSE;
		}
	}

	public static function getManagerByCountryGeneralData($db, $serial_mbc){
		$sql = "SELECT man.serial_man, man.name_man
				FROM manager_by_country mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				WHERE mbc.serial_mbc=".$serial_mbc;

		$result=$db->getRow($sql);

		if($result){
			return $result[0];
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: loadManagersWithSales
	 * @param: $db - DB connection
	 * @return: Array with managers
	 */
	public static function loadManagersWithSales($db, $serial_mbc, $serial_dea, $serial_cou){

		$sql = "SELECT DISTINCT mbc.serial_mbc, man.name_man
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea 
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.status_mbc = 'ACTIVE'
				JOIN manager man ON man.serial_man = mbc.serial_man
				WHERE cit.serial_cou = $serial_cou
				";

		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_man";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: loadManagersWithAssignedDealers
	 * @param: $db - DB connection
	 * @return: Array with managers
	 */
	public static function loadManagersWithAssignedDealers($db, $serial_mbc, $serial_dea, $serial_cou){

		$sql = "SELECT DISTINCT mbc.serial_mbc, man.name_man
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.status_mbc = 'ACTIVE'
				JOIN manager man ON man.serial_man = mbc.serial_man
				WHERE cit.serial_cou = $serial_cou
				";

		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_man";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name getManagersWithNonPaidInvoices
	 * @param type $db
	 * @param type $serial_cou
	 * @param type $own_countries 
	 */
	public static function getManagersWithNonPaidInvoicesAndAssistance($db, $serial_cou){
		$sql = "SELECT DISTINCT mbc.serial_mbc, man.name_man
				FROM file f
				JOIN sales s ON s.serial_sal = f.serial_sal
				JOIN customer cus ON cus.serial_cus = f.serial_cus
				JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv NOT IN ('PAID', 'VOID', 'UNCOLLECTABLE')
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN credit_day cdd ON cdd.serial_cdd = b.serial_cdd
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc AND mbc.serial_cou = $serial_cou
				JOIN manager man ON man.serial_man = mbc.serial_man
				JOIN country cou ON cou.serial_cou = mbc.serial_cou 
				JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr";
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getMbcWithComissionsForSubManager($db, $serial_cou, $for_report = 'NO')
	{
		$sql = "SELECT DISTINCT mbc.serial_mbc, man.name_man
				FROM manager_by_country mbc
				JOIN manager man ON man.serial_man = mbc.serial_man AND mbc.serial_cou = $serial_cou
				JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc 
					AND dea.dea_serial_dea IS NULL 
					AND dea.manager_rights_dea = 'YES'
					AND dea.status_dea = 'ACTIVE'
				JOIN applied_comissions apc ON apc.serial_dea = dea.serial_dea";
		
		if($for_report == 'NO'):
			$sql .= " AND apc.dea_payment_date_com IS NULL";
		endif;		
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getSubManagersWithComissions($db, $serial_mbc, $serial_dea = NULL, $for_report = 'NO')
	{
		$sql = "SELECT DISTINCT dea.serial_dea, dea.name_dea
				FROM dealer dea 
				JOIN applied_comissions apc ON apc.serial_dea = dea.serial_dea AND dea.serial_mbc = $serial_mbc
				WHERE dea.dea_serial_dea IS NULL 
				AND dea.manager_rights_dea = 'YES'
				AND dea.status_dea = 'ACTIVE'";
		
		if($serial_dea):
			$sql .= " AND dea.serial_dea = $serial_dea";
		endif;
		
		if($for_report == 'NO'):
			$sql .= " AND apc.dea_payment_date_com IS NULL";
		endif;		
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getManagersWithPhonePOS($db){
		$sql = "SELECT mbc.serial_mbc, m.name_man, cou.name_cou
				FROM manager_by_country mbc
				JOIN country cou ON cou.serial_cou = mbc.serial_cou
				JOIN manager m ON m.serial_man = mbc.serial_man AND mbc.status_mbc = 'ACTIVE'
				JOIN dealer d ON d.serial_mbc = mbc.serial_mbc AND d.phone_sales_dea = 'YES'
				JOIN dealer b ON b.dea_serial_dea = d.serial_dea 
					AND b.status_dea = 'ACTIVE'
					AND b.phone_sales_dea = 'YES'
				ORDER BY m.name_man";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
    //**VA    
    function getManagerbcByCounter($db, $serial_cnt){
		$sql =	"SELECT mbc.serial_mbc FROM counter cnt 
                JOIN  dealer dea ON dea.serial_dea = cnt.serial_dea AND serial_cnt = '$serial_cnt' 
                JOIN manager man ON man.document_man = dea.id_dea 
                JOIN manager_by_country mbc ON mbc.serial_man = man.serial_man";
        //echo $sql; die();
		$result = $db -> getOne($sql);
		if($result)
			return $result;
		else
			return false;
	}
	
	  public static function getSerialMbc($db,$serial_inv){
        $sql="select mbc.serial_mbc 
            from manager_by_country mbc 
            join manager man on man.serial_man = mbc.serial_man
            join document_by_manager dbm on dbm.serial_man = man.serial_man
            join invoice inv on inv.serial_dbm = dbm.serial_dbm
            where inv.serial_inv = '" . $serial_inv . "' and mbc.serial_cou=62";

        $result = $db->Execute($sql);
        // Debug::print_r($result);die();
        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

    public static function getTandiId($serial_mbc) {
        switch ($serial_mbc) {
            case 2:
                $tandi_Id = "10724324278348"; //Quito
                break;
            case 3:
                $tandi_Id = "10724324278354"; //Guayaquil
                break;
            case 4:
                $tandi_Id = "10724324278362"; //Cuenca
                break;
            case 5:
                $tandi_Id = "10724324278352"; //Quito Isla
                break;
            case 6:
                $tandi_Id = "10724324278356"; //Guayaquil Isla
                break;
            case 31:
                $tandi_Id = "10724324278350"; //Web
                break;
            case 32:
                $tandi_Id = "10724324278358"; //Manta
                break;
            case 38:
                $tandi_Id = "10724324278364"; //Ambato
                break;
            case 34:
                $tandi_Id = "10724324278360"; //Loja
                break;

            default:
                $tandi_Id = "10724324278348";
        }
        return $tandi_Id;
    }
	
    
	//GETTERS
	function getSerial_mbc(){
		return $this->serial_mbc;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getSerial_man(){
		return $this->serial_man;
	}
	function getPercentage_mbc(){
		return $this->percentage_mbc;
	}
	function getType_percentage_mbc(){
		return $this->type_percentage_mbc;
	}
	function getStatus_mbc(){
		return $this->status_mbc;
	}
	function getPayment_deadline_mbc(){
		return $this->payment_deadline_mbc;
	}
	function getInvoice_number_mbc(){
		return $this->invoice_number_mbc;
	}
	function getExclusive_mbc(){
		return $this->exclusive_mbc;
	}
	function getOfficial_mbc(){
		return $this->official_mbc;
	}
	function getLast_commission_paid_mbc(){
		return $this->last_commission_paid_mbc;
	}

	//SETTERS
	function setSerial_mbc($serial_mbc){
		$this->serial_mbc = $serial_mbc;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setSerial_man($serial_man){
		$this->serial_man = $serial_man;
	}
	function setPercentage_mbc($percentage_mbc){
		$this->percentage_mbc = $percentage_mbc;
	}
	function setType_percentage_mbc($type_percentage_mbc){
		$this->type_percentage_mbc = $type_percentage_mbc;
	}
	function setStatus_mbc($status_mbc){
		$this->status_mbc = $status_mbc;
	}
	function setPayment_deadline_mbc($payment_deadline_mbc){
		$this->payment_deadline_mbc = $payment_deadline_mbc;
	}
	function setInvoice_number_mbc($invoice_number_mbc){
		$this->invoice_number_mbc = $invoice_number_mbc;
	}
	function setExclusive_mbc($exclusive_mbc){
		$this->exclusive_mbc = $exclusive_mbc;
	}
	function setOfficial_mbc($official_mbc){
		$this->official_mbc = $official_mbc;
	}
	function setLast_commission_paid_mbc($last_commission_paid_mbc){
		$this->last_commission_paid_mbc = $last_commission_paid_mbc;
	}
}
?>