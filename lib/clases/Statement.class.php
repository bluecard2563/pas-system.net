<?php
/**
 * Created by Santi Albuja
 * User: sistemas5
 * Date: 23/10/2019
 * Time: 12:06
 */

class Statement
{
    const CURL_TIMEOUT = 3600;
    const CONNECT_TIMEOUT = 30;
    const STATEMENT_MONTH_LIMIT = 3;

    private $apiBaseUrl;
    private $apiBaseUrlDev;
    private $ch;
    private $payload;
    private $method;
    private $statementId;
    private $statementLinkUrl;
    private $statementFileName;


    function __construct()
    {
        //api url for production:
        $this->apiBaseUrl = 'http://23.239.12.79:8080/api';

        //api ur for sandbox:
        //$this->apiBaseUrl = 'http://23.239.12.79:3031/api';

        //api ur for local:
        //$this->apiBaseUrlDev = 'http://192.168.0.76/health-statement/health-statement-backend/public/index.php/api';

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_ENCODING, "");
        curl_setopt($this->ch, CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
    }

    public function getAllCustomerStatement()
    {

        $url = $this->apiBaseUrl . "/quiz/statements/customer/all";
        $this->method = "GET";

        curl_setopt($this->ch, CURLOPT_URL, $url);
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        curl_close($this->ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            return $response;
        }

    }

    public function existCustomerStatement($cardNumber, $customerDocument, $systemOrigin = null)
    {
        if (!empty($cardNumber) && !empty($customerDocument)) {

            //Build Json Data
            $data = array('customer' => array('cardNumber' => $cardNumber, 'customerDocument' => $customerDocument, 'systemOrigin' => $systemOrigin));
            $payload = json_encode($data);
            $url = $this->apiBaseUrl . "/quiz/statements/customer/exist";

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            curl_close($this->ch);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {

                $response = json_decode($response, true);

                //Donwload pdf file from statement to bas server.
                if ($response['exist']) {
                    if (!empty($response['statement']['statement_id'])) {
                        $this->statementId = $response['statement']['statement_id'];
                        $this->statementLinkUrl = $response['statement']['statement_link'];
                        $this->statementFileName = $response['statement']['statement_file_name'];
                        if (!empty($response['statement']['statement_file_name'])) {
                            $this->downloadFile();
                        }
                    }
                }
                return $response;
            }
        }
    }

    public function getCustomerStatementFile($cardNumber, $customerDocument)
    {
        if (!empty($cardNumber) && !empty($customerDocument)) {
            //Build Json Data
            $data = array('cardNumber' => $cardNumber, 'customerDocument' => $customerDocument);
            $payload = json_encode($data);
            $url = $this->apiBaseUrl . "/quiz/statements/customer/file";

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            curl_close($this->ch);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $response = json_decode($response, true);
                return $response;
            }

        }
    }

    private function downloadFile()
    {
        $url = $this->statementLinkUrl;
        $fileName = $this->statementFileName;
        $path = DOCUMENT_ROOT . 'modules/sales/statements/' . $fileName;
        //$fileName = 'statement-' . $this->statementId . '.pdf';

        $newfname = $path;
        $file = fopen($url, 'rb');
        if ($file) {
            $newf = fopen($newfname, 'wb');
            if ($newf) {
                while (!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }
        if ($file) {
            fclose($file);
        }
        if ($newf) {
            fclose($newf);
        }

    }

    public function getCustomerStatementFileById($statementId)
    {
        if (!empty($statementId)) {
            //Build Json Data
            $data = array('statementId' => $statementId);
            $payload = json_encode($data);
            $url = $this->apiBaseUrl . "/quiz/statements/customer/file/retrieve";

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            curl_close($this->ch);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $response = json_decode($response, true);
                return $response;
            }
        }
    }

    public function changeStatementStatus($statementId, $statementStatus)
    {
        if (!empty($statementId)) {
            //Build Json Data
            $data = array('statement' => array('statementId' => $statementId, 'statementStatus' => $statementStatus));
            $payload = json_encode($data);
            $url = $this->apiBaseUrl . "/quiz/statements/change/status";

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            curl_close($this->ch);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {

                $response = json_decode($response, true);
                return $response;
            }
        }
    }

    public function getCustomerStatementById($statementId)
    {
        if (!empty($statementId)) {
            //Build Json Data
            $data = array('statementId' => $statementId);
            $payload = json_encode($data);
            $url = $this->apiBaseUrl . "/quiz/statements/customer/retrieve";

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            curl_close($this->ch);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $response = json_decode($response, true);
                return $response;
            }
        }

    }

    public function sendCustomerStatementEmail($statementId)
    {
        if (!empty($statementId)) {
            $customerStatement = $this->getCustomerStatementById($statementId);

            if (!empty($customerStatement)) {
                if ($customerStatement['exist']) {
                    $misc['card_number'] = $customerStatement['statement']['card_number'];
                    $misc['customer_name'] = $customerStatement['statement']['customer_name'];
                    $misc['customer_email'] = $customerStatement['statement']['customer_email'];
                    $misc['statement_file'] = $customerStatement['statement']['statement_file_name'];
                    if (!empty($customerStatement['statement']['statement_file_name'])) {
                        return GlobalFunctions::sendMail($misc, 'healthStatementNotification');
                    }
                }
            }
        }
    }

    public function getCustomerStatementByMonthLimit()
    {
        //Build Json Data
        $data = array('monthLimit' => self::STATEMENT_MONTH_LIMIT);
        $payload = json_encode($data);
        $url = $this->apiBaseUrl . "/quiz/statements/customer/month-limit";

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        curl_close($this->ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            return $response;
        }
    }

    public function getCustomerStatementsByDocument($customerDocument)
    {
        //Build Json Data
        $data = array('customerDocument' => $customerDocument);
        $payload = json_encode($data);
        $url = $this->apiBaseUrl . "/quiz/statements/customer/document";

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        curl_close($this->ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            return $response;
        }
    }

    public function getCustomerStatementFileByDocument($customerDocument, $cardNumber = null, $updateCard = false)
    {
        //Build Json Data
        $data = array('customerDocument' => $customerDocument, 'cardNumber' => $cardNumber, 'updateCard' => $updateCard);
        $payload = json_encode($data);
        $url = $this->apiBaseUrl . "/quiz/statements/file/document";

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        curl_close($this->ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            return $response;
        }
    }

    public function updateCardNumber($statementId, $cardNumber)
    {
        //Build Json Data
        $data = array('statementId' => $statementId, 'cardNumber' => $cardNumber);
        $payload = json_encode($data);
        $url = $this->apiBaseUrl . "/quiz/statements/update/card";

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        curl_close($this->ch);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            return $response;
        }
    }

}
