<?php
/*
File: WebContentByLanguage.php
Author: Santiago Borja Lopez
Creation Date: 04/03/2010
Last Modified: 04/03/2010
Modified By: Santiago Borja
*/
class WebContentByLanguage{				
	var $db;
	var $serial_wct;
	var $serial_lang;
	var $title_wcl;
	var $text_wcl;
		
	function __construct($db, $serial_wct, $serial_lang, $title_wcl=NULL, $text_wcl=NULL){
		$this -> db = $db;
		$this -> serial_wct = $serial_wct;
		$this -> serial_lang = $serial_lang;
		$this -> title_wcl = $title_wcl;
		$this -> text_wcl = $text_wcl;
	}
	
	/***********************************************
    * funcion getData
    * sets data by serial_wct
    ***********************************************/
	function getData(){
		if($this->serial_wct!=NULL){
				$sql = 	"	SELECT n.serial_wct, n.serial_lang, n.title_wcl, n.text_wcl, UNIX_TIMESTAMP(w.date_wct) as date_wct
							FROM web_content_by_language n
							LEFT JOIN language a ON n.serial_lang = a.serial_lang
							LEFT JOIN web_content w ON w.serial_wct = n.serial_wct 
							WHERE n.serial_wct = ".$this-> serial_wct."
							and n.serial_lang = ".$this-> serial_lang;
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_wct =$result->fields[0];
				$this -> serial_lang = $result->fields[1];
				$this -> title_wcl = $result->fields[2];
				$this -> text_wcl = $result->fields[3];
				$this -> date_wct = $result->fields[3]; //TRANSIENT FIELD
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * funcion getMyTranslatedLanguages
    * gets information from the Content in the language specified
    ***********************************************/
    function getMyTranslatedLanguages(){
        $sql = 	"SELECT serial_lang FROM web_content_by_language WHERE serial_wct = ".$this -> serial_wct;
        $result = $this-> db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr="";
            $cont=0;
            do{
                $temp_rec = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
                $arr.= $temp_rec['serial_lang'].($cont<$num?",":"");
            }while($cont<$num);
        }
        return $arr;
    }
	
	/***********************************************
    * function insert
    * inserts a new register in the DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO web_content_by_language (
        	serial_wct,	
            serial_lang,
                title_wcl,
                text_wcl)
            VALUES(	".$this->serial_wct.",
            		".$this->serial_lang.",
	                '".$this->title_wcl."',
	                '".$this->text_wcl."')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in the DB
    ***********************************************/
    function update() {
        $sql = "UPDATE web_content_by_language SET 
        		title_wcl			='".$this->title_wcl."',
        		text_wcl			='".$this->text_wcl."'
        		WHERE serial_wct = '".$this->serial_wct."'
        		AND serial_lang	=".$this->serial_lang;
        $result = $this->db->Execute($sql);
        return $result;
    }
	
	///GETTERS
	function getSerial_wct(){
		return $this->serial_wct;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}
	function getTitle_wcl(){
		return $this->title_wcl;
	}
	function getText_wcl(){
		return $this->text_wcl;
	}

	///SETTERS	
	function setSerial_wct($serial_wct){
		$this->serial_wct = $serial_wct;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setTitle_wcl($title_wcl){
		$this->title_wcl = $title_wcl;
	}
	function setText_wcl($text_wcl){
		$this->text_wcl = $text_wcl;
	}
}
?>