<?php
/*
File: Over.class.php
Author: Santiago Borja
Creation Date: 17/03/2010
*/
class Over{
	var $db;
	var $serial_ove;
	var $creation_date_ove;
	var $begin_date_ove;
	var $end_date_ove;
	var $status_ove;
	var $percentage_increase_ove;
    var $percentage_comission_ove;
	var $name_ove;
	var $applied_to_ove;

	function __construct($db, $serial_ove=NULL, $creation_date_ove=NULL, $begin_date_ove=NULL, $end_date_ove=NULL, $status_ove=NULL,
                            $name_ove=NULL, $applied_to_ove = NULL){
		$this -> db = $db;
		$this -> serial_ove=$serial_ove;
		$this -> creation_date_ove=$creation_date_ove;
		$this -> begin_date_ove=$begin_date_ove;
		$this -> end_date_ove=$end_date_ove;
		$this -> status_ove=$status_ove;
		$this -> name_ove=$name_ove;
		$this -> applied_to_ove=$applied_to_ove;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a Over object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_ove!=NULL){
			$sql = 	"SELECT p.serial_ove, DATE_FORMAT(p.creation_date_ove, '%d/%m/%Y'), DATE_FORMAT(p.begin_date_ove, '%d/%m/%Y'),
                                        DATE_FORMAT(p.end_date_ove, '%d/%m/%Y'), p.status_ove, p.percentage_increase_ove, p.percentage_comission_ove, p.name_ove, p.applied_to_ove
                                FROM overs p
                                WHERE p.serial_ove='".$this->serial_ove."'";
                                //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ove=$result->fields[0];
				$this -> creation_date_ove=$result->fields[1];
				$this -> begin_date_ove=$result->fields[2];
                $this -> end_date_ove=$result->fields[3];
                $this -> status_ove=$result->fields[4];
                $this -> percentage_increase_ove=$result->fields[5];
                $this -> percentage_comission_ove=$result->fields[6];
				$this -> name_ove=$result->fields[7];
				$this -> applied_to_ove=$result->fields[8];
				return true;
			}
		}
		return false;
	}
    
	/**
	@Name: overNameExists
	@Description: Verifies if another over exists with the same Name.
	@Params: $name_ove (over Name), $serial_ove (over serialID)
	@Returns: TRUE if exist. / FALSE no match
	**/
	function overNameExists($name_ove, $serial_ove){
		$sql = 	"SELECT serial_ove
			 FROM overs
			 WHERE LOWER(name_ove) = utf8_ LOWER('".$name_ove."') COLLATE utf8_bin ";

		if($serial_ove!=NULL){
			$sql.=" AND serial_ove <> ".$serial_ove;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}
		return false;
	}
	
	/**
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function insert() {
            $sql = "
                INSERT INTO overs (
                    serial_ove,
                    creation_date_ove,
                    begin_date_ove,
                    end_date_ove,
                    status_ove,
                    percentage_increase_ove,
                    percentage_comission_ove,
                    name_ove,
                    applied_to_ove)
                VALUES (
                    NULL,
                    CURDATE(),
                    STR_TO_DATE('".$this->begin_date_ove."','%d/%m/%Y'),
                    STR_TO_DATE('".$this->end_date_ove."','%d/%m/%Y'),
                    '".$this->status_ove."',
                    '".$this->percentage_increase_ove."',
                    '".$this->percentage_comission_ove."',
                    '".$this->name_ove."',
                    '".$this->applied_to_ove."')";
            $result = $this->db->Execute($sql);
            
	    	if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK/ FALSE on error
	**/
    function update() {
        $sql = "UPDATE overs SET
                    creation_date_ove=CURDATE(),
                    begin_date_ove=STR_TO_DATE('".$this->begin_date_ove."','%d/%m/%Y'),
                    end_date_ove= STR_TO_DATE('".$this->end_date_ove."','%d/%m/%Y'),
                    status_ove='".$this->status_ove."',
                    percentage_increase_ove='".$this->percentage_increase_ove."',
                    percentage_comission_ove='".$this->percentage_comission_ove."',
                    name_ove='".$this->name_ove."',
                    applied_to_ove='".$this->applied_to_ove."'
            WHERE serial_ove = '".$this->serial_ove."'";
        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		return true;
    }

	/**
	@Name: getOverAutocompleter
	@Description: Returns an array of Overs.
	@Params: Over name or pattern.
	@Returns: Dealer array
	**/
	function getOverAutocompleter($name_ove,$for_payment = false){
		$sql2 = "SELECT serial_ove, name_ove
			FROM overs
			WHERE LOWER(name_ove) LIKE LOWER('%".$name_ove."%')";
		$sql2.=($for_payment)?" AND status_ove NOT IN('PENDING','DENIED') ":" ";
		$sql2.=" LIMIT 10";
		$result = $this->db -> Execute($sql2);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;

			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
	}
	
	/**
	@Name: getOverDealers
	@Description: Returns an array of dealers.
	@Params: Over name or pattern.
	@Returns: Dealer array
	**/
	function getOverDealers($all=false){
		$sql = "SELECT c.serial_cou, s.serial_cit, d.serial_dea, d.dea_serial_dea, name_dea, i.status_ino,d.serial_mbc,ubd.serial_usr,d.code_dea
				FROM city c, sector s, dealer d, applied_overs a, info_overs i, overs o , user_by_dealer ubd
				WHERE c.serial_cit = s.serial_cit
				AND s.serial_sec = d.serial_sec
				AND d.serial_dea = a.serial_dea
				AND a.serial_apo = i.serial_apo
				AND i.serial_ove = o.serial_ove
				AND ubd.serial_dea = d.serial_dea
				AND ubd.status_ubd = 'ACTIVE'
				AND o.serial_ove = ". $this-> serial_ove;
		if(!$all){
			$sql.= " and a.status_apo = 'ACTIVE'";
		}
		//fix to eliminate duplicates
		$sql.= " GROUP BY d.serial_dea";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}
	
	/**
	@Name: setAllAppliedOversInactive
	@Description: deactivates all applied overs
	**/
	function setAllAppliedOversInactive(){
		$sql = "UPDATE applied_overs SET status_apo = 'INACTIVE'
				WHERE serial_apo IN(
				SELECT serial_apo
				FROM info_overs io
				WHERE io.serial_ove = ".$this-> serial_ove.")";
		$result = $this->db -> Execute($sql);
		if ($result === false) return false;
		return true;
	}
	
	/**
	@Name: setDealerOverActive
	@Description: activates a specific applied over
	**/
	function setDealerOverActive($appOverId){
		$sql = "UPDATE applied_overs SET status_apo = 'ACTIVE'
				WHERE serial_apo IN(
				SELECT serial_apo
				FROM info_overs io
				WHERE io.serial_ove = ".$this-> serial_ove.")
				AND serial_dea = ".$appOverId;
		$result = $this->db -> Execute($sql);
		if ($result === false) return false;
		return true;
	}
	
	/**
	@Name: setDealerOverActive
	@Description: activates a specific applied over
	**/
	function setOverInfoStatus($appOverId, $newStatus){
		if($this -> applied_to_ove == 'MANAGER'){
			$appClause = "serial_mbc";
		}else{
			$appClause = "serial_dea";
		}
		
		$sql = "UPDATE info_overs SET status_ino = '".$newStatus."'
				WHERE serial_ove = ".$this -> serial_ove."
				AND serial_apo IN (	SELECT serial_apo 
									FROM applied_overs 
									WHERE ".$appClause." = ".$appOverId." 
									AND status_apo = 'ACTIVE')";
		$result = $this->db -> Execute($sql);
		if ($result === false) return false;
		return true;
	}
	
/**
	@Name: setDealerOverActive
	@Description: activates a specific applied over
	**/
	function setManagerOverActive($appOverId){
		$sql = "UPDATE applied_overs SET status_apo = 'ACTIVE'
				WHERE serial_apo IN(
				SELECT serial_apo
				FROM info_overs io
				WHERE io.serial_ove = ".$this-> serial_ove.")
				AND serial_mbc = ".$appOverId;
		$result = $this->db -> Execute($sql);
		if ($result === false) return false;
		return true;
	}
	
	/**
	@Name: getOverManager
	@Description: Returns the manager
	@Params: Over name or pattern.
	@Returns: manager
	**/
	function getOverManager($all=false){
		$sql = "SELECT m.serial_man, mc.serial_cou, mc.serial_mbc, m.name_man, i.status_ino
				FROM manager m, manager_by_country mc, applied_overs a, info_overs i, overs o
				WHERE m.serial_man = mc.serial_man
				AND mc.serial_mbc = a.serial_mbc
				AND a.serial_apo = i.serial_apo
				AND i.serial_ove = o.serial_ove
				AND o.serial_ove = ". $this-> serial_ove;
		if(!$all){
			$sql.= " and a.status_apo = 'ACTIVE'";
		}
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $array;
	}
	
	 /**
	@Name: getAppliedType
	@Description: Gets all appliedTo types in DB.
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getFilteredOversToPay($db,$overId,$appliedTo,$countrySel){
		$innerClause = "";
		$outerClause = "";
		
		//SEARCH BY NAME, ELSE = SEARCH BY TYPE, COUNTRY AND CITY
		if($overId){
			$innerClause = " AND o.serial_ove = ".$overId;
		}else{
			if($appliedTo){
				if($appliedTo == 'DEALER'){
					$innerClause = " AND a.serial_dea IS NOT NULL ";
				}else{
					$innerClause = " AND a.serial_mbc IS NOT NULL ";
				}
			}
			if($countrySel){
				$outerClause = " AND country = ".$countrySel;
			}
		}
		
		//THIS QUERY GETS ONE RESULT FOR EACH APPLIED_OVER
		//MATCHES IT'S INFO_OVER AND OVER INFORMATION
		//AND GETS THE 'APPLIED TO' INFORMATION DEPENDING ON IF IT IS APPLIED TO A DEALER OR A MANAGER
		//FINALLY THIS RESULT (total_results) IS FILTERED BY COUNTRY NAME AND CITY NAME 
		$sql = "SELECT * FROM (
					SELECT a.serial_apo, o.serial_ove,
						IF(serial_dea IS NOT NULL,
							(SELECT name_dea FROM dealer d WHERE d.serial_dea = a.serial_dea),
							(SELECT name_man FROM manager m, manager_by_country mc WHERE m.serial_man = mc.serial_man AND mc.serial_mbc = a.serial_mbc))
						AS name_app,
						IF(serial_dea IS NOT NULL,
							(SELECT c.serial_cou FROM city c, sector s, dealer d
							WHERE c.serial_cit = s.serial_cit
							AND s.serial_sec = d.serial_sec
							AND d.serial_dea = a.serial_dea),
							(SELECT serial_cou FROM manager_by_country mc WHERE mc.serial_mbc = a.serial_mbc))
						AS country,
						IF(serial_dea IS NOT NULL,
							(SELECT s.serial_cit FROM sector s, dealer d
							WHERE s.serial_sec = d.serial_sec
							AND d.serial_dea = a.serial_dea),
							(SELECT 'none'))
						AS city,
						o.applied_to_ove, i.begin_date_ino, i.end_date_ino, o.percentage_increase_ove,i.percentage_ino, i.amount_ino
					FROM applied_overs a 
					LEFT JOIN info_overs i ON a.serial_apo = i.serial_apo
					LEFT JOIN overs o ON i.serial_ove = o.serial_ove
					WHERE i.amount_ino > 0 
					AND i.status_ino = 'PENDING'
					AND a.status_apo = 'ACTIVE'
					AND o.status_ove = 'ACTIVE'
					".$innerClause." 
					ORDER BY serial_apo ASC) as total_results
				WHERE 1 ".$outerClause;
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}
	
	public static function getFilteredOversReport($db,$serial_mbc, $serialDea,$countrySel){
		$innerClause = "";
		$outerClause = "";
		
		//SEARCH BY NAME, ELSE = SEARCH BY TYPE, COUNTRY AND CITY
		if($serialDea){
			$innerClause = " AND a.serial_dea = ".$serialDea;
		}
		if($serial_mbc){
			$innerClause = " AND a.serial_mbc = ".$serial_mbc;
		}
		if($countrySel){
			$outerClause = " AND country = ".$countrySel;
		}
		
		//THIS QUERY GETS ONE RESULT FOR EACH APPLIED_OVER
		//MATCHES IT'S INFO_OVER AND OVER INFORMATION
		//AND GETS THE 'APPLIED TO' INFORMATION DEPENDING ON IF IT IS APPLIED TO A DEALER OR A MANAGER
		//FINALLY THIS RESULT (total_results) IS FILTERED BY COUNTRY NAME AND CITY NAME 
		$sql = "SELECT * FROM (
					SELECT a.serial_apo, o.serial_ove,
						IF(serial_dea IS NOT NULL,
							(SELECT name_dea FROM dealer d WHERE d.serial_dea = a.serial_dea),
							(SELECT name_man FROM manager m, manager_by_country mc WHERE m.serial_man = mc.serial_man AND mc.serial_mbc = a.serial_mbc))
						AS name_app,
						IF(serial_dea IS NOT NULL,
							(SELECT c.serial_cou FROM city c, sector s, dealer d
							WHERE c.serial_cit = s.serial_cit
							AND s.serial_sec = d.serial_sec
							AND d.serial_dea = a.serial_dea),
							(SELECT serial_cou FROM manager_by_country mc WHERE mc.serial_mbc = a.serial_mbc))
						AS country,
						IF(serial_dea IS NOT NULL,
							(SELECT s.serial_cit FROM sector s, dealer d
							WHERE s.serial_sec = d.serial_sec
							AND d.serial_dea = a.serial_dea),
							(SELECT 'none'))
						AS city,
						o.applied_to_ove, i.begin_date_ino, i.end_date_ino, o.percentage_increase_ove, i.amount_ino
					FROM applied_overs a 
					LEFT JOIN info_overs i ON a.serial_apo = i.serial_apo
					LEFT JOIN overs o ON i.serial_ove = o.serial_ove
					WHERE i.amount_ino > 0 
					AND i.status_ino = 'PENDING'
					AND a.status_apo = 'ACTIVE'
					".$innerClause."  
					ORDER BY serial_apo ASC) as total_results
				WHERE 1 ".$outerClause;
		
		//die($sql);
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}
	
	public static function getFilteredPendingOversReport($db, $selAppTo, $serialMan, $serialDealer, $serialCountry, $serialResp,$serialBranch){
		$innerClause = "";
		$outerClause = "";
		
		$sql = "SELECT * FROM (";
	
		switch($selAppTo){
			case 'MANAGER':
				$sql = "SELECT ove.name_ove, inf.begin_date_ino, inf.end_date_ino , man.name_man as dea_name, apo.serial_apo, ove.serial_ove
						FROM overs ove, info_overs inf, applied_overs apo, manager_by_country mbc, manager man
						WHERE inf.serial_ove = ove.serial_ove
						AND inf.serial_apo = apo.serial_apo
						AND apo.serial_mbc IS NOT NULL
						AND apo.serial_mbc = ".$serialMan."
						AND mbc.serial_mbc = apo.serial_mbc						
						AND mbc.serial_cou = ".$serialCountry."
						AND man.serial_man = mbc.serial_man";
				break;
			case 'DEALER':
				$sql = "SELECT ove.name_ove, inf.begin_date_ino, inf.end_date_ino , dea.name_dea  as dea_name, apo.serial_apo, ove.serial_ove, dea.serial_dea
						FROM overs ove, info_overs inf, applied_overs apo, dealer dea, sector sec, city cit
						WHERE inf.serial_ove = ove.serial_ove
						AND inf.serial_apo = apo.serial_apo
						AND apo.serial_dea IS NOT NULL
						
						AND dea.serial_dea = apo.serial_dea
						AND dea.serial_mbc = ".$serialMan."
						AND sec.serial_sec = dea.serial_sec
						AND cit.serial_cit = sec.serial_cit
						AND cit.serial_cou = ".$serialCountry."
						";
				
				if($serialResp){
					$sql .= ""; 
				}
				if($serialDealer){
					$sql2 .= "TODO:
						AND apo.serial_dea = ".$serialDealer;
				}
				if($serialBranch){
					
				}
				break;
		}
		
		//die($sql);
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}
	
 	/**
	@Name: getFilteredOversToPayResults
	@Description: Same function as above but only to retrieve search results
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getFilteredOversToPayResults($db,$serialApo,$serialOve){
		//Same query as above 
		$sql = "SELECT * FROM (
					SELECT a.serial_apo, o.serial_ove,
						IF(serial_dea IS NOT NULL,
							(SELECT name_dea FROM dealer d WHERE d.serial_dea = a.serial_dea),
							(SELECT name_man FROM manager m, manager_by_country mc WHERE m.serial_man = mc.serial_man AND mc.serial_mbc = a.serial_mbc))
						AS name_app,
						IF(serial_dea IS NOT NULL,
							(SELECT c.serial_cou FROM city c, sector s, dealer d
							WHERE c.serial_cit = s.serial_cit
							AND s.serial_sec = d.serial_sec
							AND d.serial_dea = a.serial_dea),
							(SELECT serial_cou FROM manager_by_country mc WHERE mc.serial_mbc = a.serial_mbc))
						AS country,
						IF(serial_dea IS NOT NULL,
							(SELECT s.serial_cit FROM sector s, dealer d
							WHERE s.serial_sec = d.serial_sec
							AND d.serial_dea = a.serial_dea),
							(SELECT 'none'))
						AS city,
						o.applied_to_ove, i.begin_date_ino, i.end_date_ino, o.percentage_increase_ove,i.percentage_ino ,i.amount_ino
					FROM applied_overs a 
					LEFT JOIN info_overs i ON a.serial_apo = i.serial_apo
					LEFT JOIN overs o ON i.serial_ove = o.serial_ove
					WHERE i.amount_ino > 0 
					AND i.status_ino = 'AUTHORIZED'
					AND a.status_apo = 'ACTIVE'
					AND a.serial_apo = ".$serialApo." 
					AND o.serial_ove = ".$serialOve." 
					ORDER BY serial_apo ASC) as total_results
				WHERE 1";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array[0];
	}
	
    /**
	@Name: getAppliedType
	@Description: Gets all appliedTo types in DB.
	@Params: N/A
	@Returns: Applied To array
	**/
	function getAppliedType(){
		$sql = "SHOW COLUMNS FROM overs LIKE 'applied_to_ove'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/**
	@Name: getOversByStatus
	@Description: Gets all info.
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getOversByStatus($db,$status){
		$sql = "SELECT serial_ove,
                    DATE_FORMAT(creation_date_ove, '%d/%m/%Y') AS creation_date_ove,
					DATE_FORMAT(begin_date_ove, '%d/%m/%Y') AS begin_date_ove,
					DATE_FORMAT(end_date_ove, '%d/%m/%Y') AS end_date_ove,
                    status_ove,
                    percentage_increase_ove,
                    percentage_comission_ove,
                    name_ove,
                    applied_to_ove FROM overs WHERE status_ove = '".$status."'";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}

	/**
	@Name: getOvers
	@Description: Gets all info.
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getOvers($db){
		$sql = "SELECT serial_ove,
                    DATE_FORMAT(creation_date_ove, '%d/%m/%Y') AS creation_date_ove,
					DATE_FORMAT(begin_date_ove, '%d/%m/%Y') AS begin_date_ove,
					DATE_FORMAT(end_date_ove, '%d/%m/%Y') AS end_date_ove,
                    status_ove,
                    percentage_increase_ove,
                    percentage_comission_ove,
                    name_ove,
                    applied_to_ove FROM overs WHERE status_ove <> 'DENIED'";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$cont=0;
			do{
				$array[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $array;
	}
/**
	@Name: getOversByCountry
	@Description: Gets all Overs for a country.
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getCountryByOvers($db, $overto){
		if ($overto == 'MANAGER') {
			$sql = "SELECT DISTINCT co.name_cou, co.serial_cou
					FROM country co
					JOIN manager_by_country mb ON mb.serial_cou = co.serial_cou
					JOIN applied_overs a ON mb.serial_mbc = a.serial_mbc
					JOIN info_overs i ON a.serial_apo = i.serial_apo
					JOIN overs o ON i.serial_ove = o.serial_ove
					AND o.status_ove = 'ACTIVE'
					AND o.applied_to_ove = '".$overto."'";
		} else {
			$sql = "SELECT DISTINCT co.name_cou, co.serial_cou
					FROM country co
					JOIN city c ON c.serial_cou = co.serial_cou
					JOIN sector s ON c.serial_cit = s.serial_cit
					JOIN dealer d ON s.serial_sec = d.serial_sec
					JOIN applied_overs a ON d.serial_dea = a.serial_dea
					JOIN info_overs i ON a.serial_apo = i.serial_apo
					JOIN overs o ON i.serial_ove = o.serial_ove
					AND o.status_ove = 'ACTIVE'
					AND o.applied_to_ove = '".$overto."'";
		}

		$result = $db -> getAll($sql);

		if($result)
			return $result;
		else
			return FALSE;

	}

	/**
	@Name: getOversByCountry
	@Description: Gets all Overs for a country.
	@Params: N/A
	@Returns: Applied To array
	**/
	public static function getOversByCountry($db, $country, $overto){
		if ($overto == 'MANAGER') {
			$sql = "SELECT DISTINCT o.name_ove, o.serial_ove
					FROM overs o
					JOIN info_overs i ON i.serial_ove = o.serial_ove
					JOIN applied_overs a ON a.serial_apo = i.serial_apo
					JOIN manager_by_country mb ON mb.serial_mbc = a.serial_mbc
					AND o.status_ove = 'ACTIVE'
					AND mb.serial_cou = '".$country."'";
		} else {
			$sql = "SELECT DISTINCT o.name_ove, o.serial_ove
					FROM overs o
					JOIN info_overs i ON i.serial_ove = o.serial_ove
					JOIN applied_overs a ON a.serial_apo = i.serial_apo
					JOIN dealer d ON d.serial_dea = a.serial_dea
					JOIN sector s ON s.serial_sec = d.serial_sec
					JOIN city c ON c.serial_cit = s.serial_cit
					AND o.status_ove = 'ACTIVE'
					AND c.serial_cou = '".$country."'";
		}
		
		$result = $db -> getAll($sql);

		if($result)
			return $result;
		else
			return FALSE;
		
	}
	
	public static function stillGenerateOvers($db, $serial_ove,$serial_apo){
		$sql1 = "SELECT	serial_ove,
						serial_apo,
						DATE_FORMAT(DATE_ADD(MAX(begin_date_ino), INTERVAL +1 YEAR), '%d/%m/%Y') AS 'begin_date',
						DATE_FORMAT(DATE_ADD(MAX(end_date_ino), INTERVAL +1 YEAR), '%d/%m/%Y') AS 'end_date'
				FROM info_overs
				WHERE serial_apo = {$serial_apo}
				GROUP BY serial_ove,serial_apo";
						
		$resultDates = $db->getRow($sql1);
		if($resultDates){	
			$sql2= "SELECT serial_ove
					FROM overs
					WHERE STR_TO_DATE('{$resultDates['begin_date']}','%d/%m/%Y') BETWEEN begin_date_ove AND end_date_ove
					AND STR_TO_DATE('{$resultDates['end_date']}','%d/%m/%Y') BETWEEN begin_date_ove AND end_date_ove
					AND serial_ove = $serial_ove";

			$result = $db -> getOne($sql2);
			if($result){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
		
	}

	//Getters
	function getSerial_ove(){
		return $this->serial_ove;
	}
	function getCreationDate_ove(){
		return $this->creation_date_ove;
	}
	function getBeginDate_ove(){
		return $this->begin_date_ove;
	}
        function getEndDate_ove(){
		return $this->end_date_ove;
	}
	function getStatus_ove(){
		return $this->status_ove;
	}
	function getPercentageIncrease_ove(){
		return $this->percentage_increase_ove;
	}
    function getPercentageComission_ove(){
		return $this->percentage_comission_ove;
	}
	function getName_ove(){
		return $this->name_ove;
	}
	function getAppliedTo_ove(){
		return $this->applied_to_ove;
	}
	
	//Setters
	function setSerial_ove($serial_ove){
		$this->serial_ove=$serial_ove;
	}
	function setCreationDate_ove($creation_date_ove){
		$this->creation_date_ove=$creation_date_ove;
	}
    function setEndDate_ove($end_date_ove){
		$this->end_date_ove=$end_date_ove;
	}
	function setBeginDate_ove($begin_date_ove){
		$this->begin_date_ove=$begin_date_ove;
	}
	function setStatus_ove($status_ove){
		$this->status_ove=$status_ove;
	}
	function setPercentageIncrease_ove($percentage_increase_ove){
		$this->percentage_increase_ove=$percentage_increase_ove;
	}
    function setPercentageComission_ove($percentage_comission_ove){
		$this->percentage_comission_ove=$percentage_comission_ove;
	}
	function setName_ove($name_ove){
		$this->name_ove=$name_ove;
	}
	function setAppliedTo_ove($applied_to_ove){
		return $this->applied_to_ove=$applied_to_ove;
	}
}
?>