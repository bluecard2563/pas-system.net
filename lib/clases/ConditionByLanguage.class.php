<?php
/*
File: ConditionByLanguage
Author: Miguel Ponce
Creation Date: 06/04/2010
*/

class ConditionByLanguage{
	var $db;
	var $serial_cbl;
	var $serial_lang;
	var $serial_con;
	var $description_cbl;

	function __construct($db, $serial_cbl=NULL, $serial_lang=NULL, $serial_con=NULL, $description_cbl=NULL){
		$this -> db = $db;
		$this -> serial_cbl = $serial_cbl;
		$this -> serial_lang = $serial_lang;
		$this -> serial_con = $serial_con;
		$this -> description_cbl = $description_cbl;
	}

	/**
	@Name: getData
	@Description: Retrieves the data of a conditionByLanguage object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_cbl!=NULL){
			$sql = "SELECT serial_cbl, serial_lang, serial_con, description_cbl
					FROM conditions_by_language
					WHERE serial_cbl='".$this->serial_cbl."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_cbl =$result->fields[0];
				$this -> serial_lang =$result->fields[1];
				$this -> serial_con =$result->fields[2];
				$this -> description_con = $result->fields[3];

				return true;
			}
			else
				return false;

		}else
			return false;
	}


	/**
	@Name: getData
	@Description: Retrieves the data of a conditionByLanguage object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getDataByConAndLang(){
		if($this->serial_con!=NULL && $this->serial_lang!=NULL){
			$sql = "SELECT serial_cbl, serial_lang, serial_con, description_cbl
					FROM conditions_by_language
					WHERE serial_con='".$this->serial_con."'
					AND serial_lang='".$this->serial_lang."'";

			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_cbl =$result->fields[0];
				$this -> serial_lang =$result->fields[1];
				$this -> serial_con =$result->fields[2];
				$this -> description_con = $result->fields[3];

				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/**
	@Name: cblExists
	@Description: Verifies if a condition exists in a specific language.
	@Params: $description_cbl: The search pattern.
                 $serial_lang: language ID
                 $serial_cbl: object ID
	@Returns: TRUE if exists / FALSE if NOT
	**/
	function cblExists($description_cbl, $serial_lang, $serial_cbl=NULL){
		$sql = 	"SELECT serial_cbl
                 FROM conditions_by_language
				 WHERE LOWER(description_cbl) = '".$description_cbl."'
                 AND serial_lang = ".$serial_lang;

        if($serial_cbl!=NULL){
			$sql.=" AND serial_cbl <> ".$serial_cbl;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@Name: cblExists
	@Description: Verifies if a condition exists in a specific language.
	@Params: $description_cbl: The search pattern.
                 $serial_lang: language ID
                 $serial_cbl: object ID
	@Returns: TRUE if exists / FALSE if NOT
	**/
	function cblAliasExists($alias, $serial_lang, $serial_con=NULL){
		$sql = 	"SELECT serial_con
                 FROM conditions
				 WHERE LOWER(alias_con) = '".$alias."'";

        if($serial_con!=NULL){
			$sql.=" AND serial_cbl <> ".$serial_con;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/**
	@Name: insert
	@Description: Inserts a cbl instance in DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error.
	**/
        function insert() {
            $sql = "INSERT INTO conditions_by_language (
                          serial_cbl,
                          serial_lang,
                          serial_con,
                          description_cbl)
                    VALUES (
                          NULL,
                          '".$this->serial_lang."',
                          '".$this->serial_con."',
                          '".$this->description_cbl."'
                          )";


            $result = $this->db->Execute($sql);
            
	        if($result){
	            return $this->db->insert_ID();
	        }else{
				ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
	        	return false;
	        }
        }

 	/**
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK / FALSE on error
	**/
        function update() {
            $sql = "UPDATE conditions_by_language SET
                        description_cbl ='".$this->description_cbl."'
                    WHERE serial_cbl = '".$this->serial_cbl."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true)
                return true;
            else
                return false;
        }

	///GETTERS
	function getSerial_cbl(){
		return $this->serial_cbl;
	}
    function getSerial_lang(){
		return $this->serial_lang;
	}
    function getSerial_con(){
		return $this->serial_con;
	}
	function getDescription_cbl(){
		return $this->description_cbl;
	}


	///SETTERS
	function setSerial_cbl($serial_cbl){
		$this->serial_cbl = $serial_cbl;
	}
    function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
    function setSerial_con($serial_con){
		$this->serial_con = $serial_con;
	}
	function setDescription_cbl($description_cbl){
		$this->description_cbl = $description_cbl;
	}
}
?>