<?php
/*
File: Product.class
Author:Miguel Ponce
Creation Date:24/12/2009
Last modified:08/09/2017
Modified By: Carlos Oberto
*/

class Product {				
	var $db;
	var $serial_pro;
	var $has_comision_pro;
	var $schengen_pro;
	var $in_web_pro;
	var $flights_pro;
	var $max_extras_pro;
	var $children_pro;
	var $adults_pro;
	var $spouse_pro;
	var $relative_pro;
	var $third_party_register_pro;
	var $masive_pro;
	var $emission_country_pro;
	var $residence_country_pro;
    var $image_pro;
    var $senior_pro;
    var $price_by_range_pro;
    var $calculate_pro;
    var $generate_number_pro;
    var $status_pro;
    var $price_by_day_pro;
    var $has_services_pro;
	var $limit_pro;
    var $show_price_pro;
	var $destination_restricted_pro;
	var $extras_restricted_to_pro;
    var $representative_pro;
    var $restricted_children_pro;
    var $individual_pro;
	
	function __construct($db,	$serial_pro = NULL,
								$name_pro = NULL,
								$has_comision_pro = NULL,
								$schengen_pro = NULL,
								$in_web_pro = NULL,
								$flights_pro= NULL,
								$max_extras_pro= NULL,
								$children_pro= NULL,
								$adults_pro= NULL,
								$spouse_pro= NULL,
								$relative_pro= NULL,
								$third_party_register_pro= NULL,
								$masive_pro= NULL,
								$emission_country_pro= NULL,
								$residence_country_pro= NULL,
								$image_pro= NULL,
								$serial_tpp=NULL,
								$senior_pro=NULL,
								$price_by_range_pro=NULL,
								$calculate_pro=NULL,
								$generate_number_pro=NULL,
								$status_pro=NULL,
								$price_by_day_pro=NULL,
								$has_services_pro=NULL,
								$limit_pro=NULL,
								$show_price_pro=NULL,
								$destination_restricted_pro=NULL,
                                $extras_restricted_to_pro=NULL,
                                $representatative_pro =NULL,
                                $restricted_children_pro=NULL,
						 		$individual_pro=NULL){

									$this -> db = $db;
									$this -> serial_pro = $serial_pro;
									$this -> name_pro = $name_pro;
									$this -> has_comision_pro = $has_comision_pro;
									$this -> schengen_pro = $schengen_pro;
									$this -> in_web_pro = $in_web_pro;
									$this -> flights_pro = $flights_pro;
									$this -> max_extras_pro = $max_extras_pro;
									$this -> children_pro = $children_pro;
									$this -> adults_pro = $adults_pro;
									$this -> spouse_pro = $spouse_pro;
									$this -> relative_pro = $relative_pro;
									$this -> third_party_register_pro = $third_party_register_pro;
									$this -> masive_pro = $masive_pro;
									$this -> emission_country_pro = $emission_country_pro;
									$this -> residence_country_pro = $residence_country_pro;
									$this -> image_pro = $image_pro;
									$this -> serial_tpp = $serial_tpp;
									$this -> senior_pro = $senior_pro;
									$this -> price_by_range_pro = $price_by_range_pro;
									$this -> calculate_pro = $calculate_pro;
									$this -> generate_number_pro = $generate_number_pro;
									$this -> status_pro = $status_pro;
        $this -> price_by_day_pro = $price_by_day_pro;
        $this -> has_services_pro = $has_services_pro;
		$this -> limit_pro=$limit_pro;
		$this -> show_price_pro=$show_price_pro;
		$this -> destination_restricted_pro=$destination_restricted_pro;
		$this -> extras_restricted_to_pro = $extras_restricted_to_pro;
        $this->representative_pro = $representatative_pro;
        $this->restricted_children_pro=$restricted_children_pro;
        $this->individual_pro=$individual_pro;
	}

	/* function getData() 		*/
	/* set class parameters		*/
	
	function getData(){
		if($this->serial_pro!=NULL){
			$sql = 	"SELECT serial_pro,
							has_comision_pro,
							schengen_pro,
							in_web_pro,
							flights_pro,
							max_extras_pro,
							children_pro,
							adults_pro,
							spouse_pro,
							relative_pro,
							masive_pro,
							emission_country_pro,
							residence_country_pro,
							image_pro,
							serial_tpp,
							senior_pro,
							price_by_range_pro,
							calculate_pro,
							generate_number_pro,
							status_pro,
							price_by_day_pro,
							has_services_pro,
							third_party_register_pro,
							limit_pro,
							show_price_pro,
							destination_restricted_pro,
							extras_restricted_to_pro,
                            representative_pro,
                            restricted_children_pro,
                            individual_pro
					FROM product
					WHERE serial_pro='".$this->serial_pro."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_pro=$result->fields[0];
				$this->has_comision_pro=$result->fields[1];
				$this->schengen_pro=$result->fields[2];
				$this->in_web_pro=$result->fields[3];
				$this->flights_pro=$result->fields[4];
				$this->max_extras_pro=$result->fields[5];
				$this->children_pro=$result->fields[6];
				$this->adults_pro=$result->fields[7];
				$this->spouse_pro=$result->fields[8];
				$this->relative_pro=$result->fields[9];
				$this->masive_pro=$result->fields[10];
				$this->emission_country_pro=$result->fields[11];
				$this->residence_country_pro=$result->fields[12];
                $this->image_pro=$result->fields[13];
                $this->serial_tpp=$result->fields[14];
                $this->senior_pro=$result->fields[15];
                $this->price_by_range_pro = $result->fields[16];
                $this->calculate_pro = $result->fields[17];
                $this->generate_number_pro = $result->fields[18];
                $this->status_pro = $result->fields[19];
                $this->price_by_day_pro = $result->fields[20];
                $this->has_services_pro = $result->fields[21];
				$this->third_party_register_pro =$result->fields[22];
				$this->limit_pro =$result->fields[23];
				$this->show_price_pro =$result->fields[24];
				$this->destination_restricted_pro=$result->fields[25];
				$this->extras_restricted_to_pro=$result->fields[26];
                $this->representative_pro = $result->fields[27];
                $this->restricted_children_pro=$result->fields[28];
                $this->individual_pro=$result->fields[29];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/* function insert() 						*/
	/* insert a new product type into database	*/
	
	function insert(){
		$sql="INSERT INTO product (
					serial_pro,
					has_comision_pro,
					schengen_pro,
					in_web_pro,
					flights_pro,
					max_extras_pro,
					children_pro,
					adults_pro,
					spouse_pro,
					relative_pro,
					third_party_register_pro,
					masive_pro,
					emission_country_pro,
					residence_country_pro,
                    image_pro,
                    serial_tpp,
                    senior_pro,
                    price_by_range_pro,
                    calculate_pro,
                    generate_number_pro,
                    status_pro,
                    price_by_day_pro,
                    has_services_pro,
					limit_pro,
					show_price_pro,
					destination_restricted_pro,
					extras_restricted_to_pro,
                    representative_pro,
                    restricted_children_pro,
                    individual_pro
					)
			  VALUES(NULL,
					'$this->has_comision_pro',
					'$this->schengen_pro',
					'$this->in_web_pro',
					'$this->flights_pro',
					'$this->max_extras_pro',
					'$this->children_pro',
					'$this->adults_pro',
					'$this->spouse_pro',
					'$this->relative_pro',
					'$this->third_party_register_pro',
					'$this->masive_pro',
					'$this->emission_country_pro',
					'$this->residence_country_pro',
					'$this->image_pro',
					'$this->serial_tpp',
					'$this->senior_pro',
					'$this->price_by_range_pro',
					'$this->calculate_pro',
					'$this->generate_number_pro',
					'$this->status_pro',
					'$this->price_by_day_pro',
					'$this->has_services_pro',
					'$this->limit_pro',
					'$this->show_price_pro',
					'$this->destination_restricted_pro',
					'$this->extras_restricted_to_pro',
                    '$this->representative_pro',
                    '$this->restricted_children_pro',
                    '$this->individual_pro'
					);";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/*
	 * function update()
	 * update a product type
	 */
	function update(){
		if($this->serial_pro!=NULL){
			$sql=	"UPDATE product
					 SET has_comision_pro='$this->has_comision_pro',
						 schengen_pro='$this->schengen_pro',
						 in_web_pro='$this->in_web_pro',
						 flights_pro='$this->flights_pro',
						 max_extras_pro='$this->max_extras_pro',
						 children_pro='$this->children_pro',
						 adults_pro='$this->adults_pro',
						 spouse_pro='$this->spouse_pro',
						 relative_pro='$this->relative_pro',
						 third_party_register_pro='$this->third_party_register_pro',
						 masive_pro='$this->masive_pro',
						 emission_country_pro='$this->emission_country_pro',
						 residence_country_pro='$this->residence_country_pro',
						 image_pro='$this->image_pro',";
			 if($this->serial_tpp != NULL || $this->serial_tpp != '') {
				$sql .= "serial_tpp='$this->serial_tpp',";
			 }
			   $sql .= "senior_pro='$this->senior_pro',
			            restricted_children_pro='$this->restricted_children_pro',
			            individual_pro='$this->individual_pro',
						price_by_range_pro='$this->price_by_range_pro',
						calculate_pro='$this->calculate_pro',
						generate_number_pro='$this->generate_number_pro',
						status_pro='$this->status_pro',
						price_by_day_pro ='$this->price_by_day_pro',
						has_services_pro ='$this->has_services_pro',
						limit_pro ='$this->limit_pro',
						show_price_pro ='$this->show_price_pro',
						destination_restricted_pro = '$this->destination_restricted_pro',
						extras_restricted_to_pro = '$this->extras_restricted_to_pro',
                        representative_pro = '$this->representative_pro'
						WHERE serial_pro='$this->serial_pro'";

			//Debug::print_r($sql);die();
			$result = $this->db->Execute($sql);

			if ($result==true)
			return true;
			else
			return false;
		}else
			return false;		
	}

	/*
	 * function updateStatus()
	 * updates a product status
	 */
	function updateStatus(){
		if($this->serial_pro!=NULL){
			$sql= "UPDATE product
				   SET status_pro='".$this->status_pro."'
				   WHERE serial_pro='".$this->serial_pro."'";

			//die($sql);
			$result = $this->db->Execute($sql);

			if ($result==true)
			return true;
			else
			return false;
		}else
			return false;
	}
	
	/***********************************************
    * function getWebEstimatorProducts
    * gets all products to display on the web estimator given a language and a country
    ***********************************************/
	public static function getWebEstimatorProducts($db, $language){
		global $webSalesDealer;
		$countryOfDealerWeb = Dealer::getDealersCountrySerial($db, $webSalesDealer);
		$sql = 	"SELECT 		p.serial_pro,
                                p.serial_tpp,
                                pl.name_pbl,
                                pl.description_pbl,
                                p.children_pro,
                                p.has_comision_pro,
                                p.schengen_pro,
                                p.image_pro,
                                p.show_price_pro,
                                pbc.serial_pxc,
                                pbd.serial_dea
				FROM product p
				JOIN product_by_language pl ON p.serial_pro = pl.serial_pro AND pl.serial_lang = $language
				
				JOIN product_by_country pbc ON pbc.serial_pro = p.serial_pro AND pbc.serial_cou = $countryOfDealerWeb
				JOIN product_by_dealer pbd ON pbd.serial_pxc = pbc.serial_pxc
				
				WHERE p.in_web_pro = 1 
				AND pbd.serial_dea = $webSalesDealer
				AND p.calculate_pro = 'YES'
				AND p.status_pro = 'ACTIVE'
				AND pbd.status_pbd = 'ACTIVE'";
		$result = $db -> Execute($sql);
		if($result){
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
				return $array;
			}
		}
		return array();
	}
	
	/* function getProductTypes() 				*/
	/* return all the produc types				*/
	
	function getProducts($code_lang){
		$list=NULL;
		$sql = 	"SELECT pro.serial_pro,
                                pbl.name_pbl,
                                pbl.description_pbl,
                                pro.has_comision_pro,
                                pro.schengen_pro,
                                pro.in_web_pro,
                                pro.flights_pro,
                                pro.max_extras_pro,
                                pro.children_pro,
                                pro.adults_pro,
                                pro.spouse_pro,
                                pro.relative_pro,
                                pro.third_party_register_pro,
                                pro.masive_pro,
                                pro.emission_country_pro,
                                pro.residence_country_pro,
                                pro.image_pro,
                                pro.serial_tpp,
                                pro.senior_pro,
                                pro.price_by_range_pro,
                                pro.calculate_pro,
                                pro.generate_number_pro,
                                pro.status_pro,
                                pro.price_by_day_pro,
                                pro.has_services_pro,
								pro.limit_pro,
								pro.show_price_pro,
                                pro.representative_pro,
                                pro.restricted_children_pro,
                                pro.individual_pro
				FROM product pro
				JOIN product_by_language pbl
				ON pro.serial_pro= pbl.serial_pro
				JOIN language lang
				ON lang.serial_lang=pbl.serial_lang
				AND lang.code_lang='".$code_lang."'
				ORDER BY name_pbl";

                
                //die($sql);
                $result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$array=array();
			$count=0;
			
			do{
				$array[$count]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$count++;
			}while($count<$num);
		}
		
		return $array;
	}	


	public static function getMassiveProducts($db, $codeLang){
		$list = NULL;
		$sql = 	"SELECT pro.serial_pro,
                                pbl.name_pbl,
                                pbl.description_pbl,
                                pro.has_comision_pro,
                                pro.schengen_pro,
                                pro.in_web_pro,
                                pro.flights_pro,
                                pro.max_extras_pro,
                                pro.children_pro,
                                pro.adults_pro,
                                pro.spouse_pro,
                                pro.relative_pro,
                                pro.third_party_register_pro,
                                pro.masive_pro,
                                pro.emission_country_pro,
                                pro.residence_country_pro,
                                pro.image_pro,
                                pro.serial_tpp,
                                pro.senior_pro,
                                pro.price_by_range_pro,
                                pro.calculate_pro,
                                pro.generate_number_pro,
                                pro.status_pro,
                                pro.price_by_day_pro,
                                pro.has_services_pro,
						pro.limit_pro,
						pro.show_price_pro,
                        pro.representative_pro,
                        pro.restricted_children_pro,
                        pro.individual_pro

                                FROM product pro
                                    JOIN product_by_language pbl
                                    ON pro.serial_pro= pbl.serial_pro
                                    JOIN language lang
                                    ON lang.serial_lang=pbl.serial_lang
                                    AND lang.code_lang='$codeLang'
                                WHERE pro.masive_pro = 1";

            $result = $db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$array = array();
				$count = 0;
				do{
					$array[$count++] = $result -> GetRowAssoc(false);
					$result -> MoveNext();
				}while($count < $num);
			}
		return $array;
	} 
	
	/* function existProdutTypeName() 			*/
	/* returns true if name_pro already exists	*/

	function existProductName(){
		if($this->name_pro != NULL){
			$sql = 	"SELECT p.serial_pro
					 FROM p.product
					 JOIN product_by_language pbl ON pbl.serial_pro = p.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					 WHERE LOWER(name_pro)= '".utf8_encode($this->name_pro)."'";
					 
			if($this->serial_pro != NULL && $this->serial_pro != ''){
				$sql.= " AND p.serial_pro <> ".$this->serial_pro;
			}		
			//echo $sql;
			$result = $this->db -> Execute($sql);
			if($result->fields[0]){
				return $result->fields[0];
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	
		/* function getProductTypes() 				*/
	/* return all the produc types				*/
	
	function getConditionsByProductType(){
 		if($this->serial_pro != NULL){
			$sql = 	"SELECT pro.serial_pro as 'Serial', name_gcn as 'Condition'
					 FROM product_type pro
					 JOIN conditions_by_product_type cbp ON pro.serial_pro = cbp.serial_pro
					 JOIN general_conditions gc ON cbp.serial_gcn = gc.serial_gcn
					 WHERE pro.serial_pro=".$this->serial_pro;

			$result = $this->db->Execute($sql);
			
			$num = $result -> RecordCount();
			if($num > 0){
				$array=array();
				$count=0;
				
				do{
					$array[$count]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$count++;
				}while($count<$num);
			}
			
			return $array;
		}
		return false;	
	}


	function getProductAutocompleter($code_lang,$name_pbl,$allow_inactive=false){
		$sql = 	"SELECT pro.serial_pro, 
                                pbl.serial_lang,
                                pbl.serial_pbl,
                                pbl.name_pbl,
                                pbl.description_pbl
				 FROM product_by_language pbl
				 JOIN product pro ON pbl.serial_pro = pro.serial_pro
				 JOIN language lang ON lang.serial_lang = pbl.serial_lang AND lang.code_lang = '".$code_lang."'
				 WHERE pbl.name_pbl LIKE '%".$name_pbl."%'";
		if(!$allow_inactive) {
			$sql .= " AND pro.status_pro = 'ACTIVE'";
		}
				 //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}


	
	function getInfoProduct($code_lang){
		if($this->serial_pro!=NULL){
			$sql = 	"SELECT  pro.serial_pro,
                                        pbl.name_pbl,
                                        pbl.description_pbl,
                                        pro.has_comision_pro,
                                        pro.schengen_pro,
                                        pro.in_web_pro,
                                        pro.flights_pro,
                                        pro.max_extras_pro,
                                        pro.children_pro,
                                        pro.adults_pro,
                                        pro.spouse_pro,
                                        pro.relative_pro,
                                        pro.third_party_register_pro,
                                        pro.masive_pro,
                                        pro.emission_country_pro,
                                        pro.residence_country_pro,
                                        pro.image_pro,
                                        pro.serial_tpp,
                                        pro.senior_pro,
										pro.destination_restricted_pro,
                                        pro.price_by_range_pro,
                                        pro.calculate_pro,
                                        pro.generate_number_pro,
                                        pro.status_pro,
                                        pro.price_by_day_pro,
                                        pro.has_services_pro,
										pro.limit_pro,
										pro.show_price_pro,
										pro.extras_restricted_to_pro,
                                        pro.representative_pro,
                                        pro.restricted_children_pro,
                                        pro.individual_pro

                                FROM product pro
                                JOIN product_by_language pbl ON pro.serial_pro= pbl.serial_pro
                                JOIN language lang ON lang.serial_lang = pbl.serial_lang AND lang.code_lang='".$code_lang."'
                                WHERE pro.serial_pro='".$this->serial_pro."'";

			       	$result = $this->db -> Execute($sql);
					$num = $result -> RecordCount();
					if($num > 0){
							$array=array();
							$count=0;
							do{
									$array[$count]=$result->GetRowAssoc(false);
									$result->MoveNext();
									$count++;
							}while($count<$num);
					}
					return $array;

		}
		return false;
	}
	
	/**
	@Name: getTranslations
	@Description: Returns an array of all the translations for a specific benefit
	@Params: $serial_ben: Benefit ID
	@Returns: An array of translations
	**/
	function getTranslations(){
		$sql = 	"	SELECT 	pbl.serial_pbl as 'serial',
							pbl.name_pbl,
							l.name_lang, 
							pbl.serial_pbl as 'update',
							l.serial_lang 
					FROM product_by_language pbl
						JOIN language l 
							ON l.serial_lang=pbl.serial_lang
					WHERE pbl.serial_pro = '".$this->serial_pro."'";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}
	
	function getProductsByCountry($serial_cou, $serial_lang){
            $sql = "SELECT pbc.serial_pxc, pbl.name_pbl, p.serial_pro
                    FROM product p
                    JOIN product_by_country pbc ON pbc.serial_pro = p.serial_pro
                    AND pbc.serial_cou = '".$serial_cou."'
                    JOIN product_by_language pbl ON pbl.serial_pro = p.serial_pro
                    AND pbl.serial_lang ='".$serial_lang."'
					ORDER BY name_pbl";
            //echo $sql;
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arr=array();
                $cont=0;

                do{
                    $arr[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;

                }while($cont<$num);
            }

            return $arr;
        }
	
	function getEstimatorProductsListByCountry($serial_cou, $serial_lang){
		$productList = self::getProductsByCountryEstimator($serial_cou, $serial_lang);
		
		if(!$productList){ //WE GET THE PRODUCTS LIST FROM THE DEFAULT SALES COUNTER
			 global $phoneSalesDealer;
		     $phoneCountry = Dealer::getDealersCountrySerial($this -> db, $phoneSalesDealer);
			 $productList = self::getProductsByCountryEstimator($phoneCountry, $_SESSION['serial_lang']);
    	}
    	
		if($productList){
		    foreach($productList as &$pl){
		        $pl['name_pbl'] = utf8_encode($pl['name_pbl']);
		    }
		}
    	
    	return $productList;
	}
        
        function getProductsByCountryEstimator($serial_cou, $serial_lang){
            $sql = "SELECT pbc.serial_pxc, pbl.name_pbl, p.serial_pro, p.show_price_pro
                    FROM product p
                    JOIN product_by_country pbc ON pbc.serial_pro = p.serial_pro 
                    JOIN product_by_language pbl ON pbl.serial_pro = p.serial_pro
                    JOIN product_by_dealer pbd ON pbc.serial_pxc = pbd.serial_pxc
                    JOIN dealer dea ON dea.serial_dea = pbd.serial_dea 
                    JOIN sector sec ON dea.serial_sec = sec.serial_sec
                    JOIN city cit ON sec.serial_cit = cit.serial_cit
                         
                    WHERE pbc.serial_cou = '".$serial_cou."'
                    AND pbl.serial_lang ='".$serial_lang."'
                    AND dea.phone_sales_dea = 'YES'
                    AND cit.serial_cou = pbc.serial_cou
                    AND dea.phone_sales_dea = 'YES'
                    AND pbc.status_pxc = 'ACTIVE'
                    AND pbd.status_pbd = 'ACTIVE'
                    AND p.status_pro = 'ACTIVE'
                    AND p.masive_pro = 'NO'
                    AND p. generate_number_pro ='YES'
                    AND (SELECT distinct(ppc.serial_pxc) FROM price_by_product_by_country ppc WHERE ppc.serial_pxc = pbc.serial_pxc) IS NOT NULL";
            //echo $sql;
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $productList = array();
                $cont = 0;
                do{
                    $productList[$cont++] = $result -> GetRowAssoc(false);
                    $result -> MoveNext();
                }while($cont < $num);
            }
			return $productList;
        }
        
	function getProductByCountrySerial($serial_cou){
    	$sql = "SELECT serial_pxc
            	FROM product_by_country
                WHERE serial_pro = ".$this -> serial_pro." AND serial_cou = ".$serial_cou;
        //echo $sql;
		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return $result->fields[0];
		}
		return false;
    }

	/**
	@Name: getProductsPricesByProduct
	@Description: Retrieves the information of a specific product list
	@Params:
	@Returns: Product array
	**/
	function getProductsPricesByProduct($serial_cou, $serial_mbc, $dea_serial_dea, $serial_lang){
		$arreglo=array();
		$langCode = Language::getSerialLangByCode($this -> db, $serial_lang);
		$sql =  "SELECT distinct(pro.serial_pro), pbl.name_pbl
				   FROM product pro
						JOIN product_by_country pbc ON pbc.serial_pro = pro.serial_pro
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
						JOIN country cou ON cou.serial_cou = pbc.serial_cou
						JOIN zone zon ON zon.serial_zon = cou.serial_zon
						JOIN manager_by_country mbc ON mbc.serial_cou = cou.serial_cou
						JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc
					WHERE cou.serial_cou = $serial_cou
					AND pbl.serial_lang = $langCode";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.dea_serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY pbl.name_pbl";//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getProductsBenefitsByProduct
	@Description: Retrieves the information of a specific product list
	@Params:
	@Returns: Product array
	**/
	function getProductsBenefitsByProduct($serial_cou, $serial_lang, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		$sql =  "SELECT distinct(pro.serial_pro), pbl.name_pbl
					FROM product pro
						JOIN product_by_country pbc ON pro.serial_pro=pbc.serial_pro AND pro.status_pro = 'ACTIVE'
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang=$serial_lang
						JOIN benefits_product bp ON bp.serial_pro=pro.serial_pro AND bp.status_bxp = 'ACTIVE'
						JOIN conditions c ON c.serial_con=bp.serial_con
						LEFT JOIN restriction_type rt ON rt.serial_rst=bp.serial_rst AND rt.status_rst = 'ACTIVE'
						LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = ".$serial_lang."
						JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
						JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang= ".$serial_lang."
						JOIN country cou ON cou.serial_cou = pbc.serial_cou
						JOIN manager_by_country mbc ON mbc.serial_cou = cou.serial_cou
						JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc
					WHERE cou.serial_cou = ".$serial_cou."
					  ";
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.dea_serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY pbl.name_pbl";//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/*
	 * @name: getReferentialAdditionalDayFee
	 * @param: $db - DB connection
	 * @param: $serial_pro - Product ID
	 * @returns: The referential additional day fee for all countries.
	 */
	public static function getReferentialAdditionalDayFee($db, $serial_pro){
		$sql="SELECT pxc.aditional_day_pxc
			  FROM product p
			  JOIN product_by_country pxc ON pxc.serial_pro=p.serial_pro AND pxc.serial_cou='1'
			  WHERE p.serial_pro='$serial_pro'";

		$result=$db->getOne($sql);
		if($result){
			return $result;
		}else{
			return false;
		}

	}

	/*
	* @name: alreadySold
	* @param: $db - DB connection
	* @param: $serial_pro - Product ID
	* @returns: The referential additional day fee for all countries.
	*/
	public static function alreadySold($db, $serial_pro){
		$sql= "SELECT pxc.aditional_day_pxc
			   FROM product pro
			   JOIN product_by_country pxc ON pxc.serial_pro = pro.serial_pro
			   JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
			   JOIN sales sal ON sal.serial_pbd = pbd.serial_pbd
			   WHERE pro.serial_pro='$serial_pro'";
		//die($sql);
		$result=$db->getAll($sql);

		if($result){
				return true;
		}else{
				return false;
		}

	}

	/*
	* @name: checkEmissionCountry
	* @param: $db - DB connection
	* @param: $serial_pro - Product ID
	* @returns: The referential additional day fee for all countries.
	*/
	public static function checkEmissionCountry($db, $serial_sal, $serial_cou){
		$sql= "SELECT pro.serial_pro
			   FROM sales sal
			   JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
			   JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
			   JOIN product pro ON pro.serial_pro = pxc.serial_pro
			   JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
			   JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
			   JOIN sector sec ON sec.serial_sec = dea.serial_sec
			   JOIN city cit ON cit.serial_cit = sec.serial_cit
			   WHERE sal.serial_sal = $serial_sal
			   AND emission_country_pro = 'NO'
			   AND cit.serial_cou = $serial_cou";
		//die($sql);
		$result=$db->getOne($sql);

		if($result){
				return true;
		}else{
				return false;
		}
	}

	/*
	* @name: checkResidenceCountry
	* @param: $db - DB connection
	* @param: $serial_pro - Product ID
	* @returns: The referential additional day fee for all countries.
	*/
	//residence_country_pro
	public static function checkResidenceCountry($db, $serial_sal, $serial_cou, $cus_serial_cou){
		$sql= "SELECT pro.serial_pro
			   FROM sales sal
			   JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
			   JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
			   JOIN product pro ON pro.serial_pro = pxc.serial_pro
			   JOIN customer cus
			   WHERE sal.serial_sal = $serial_sal
			   AND emission_country_pro = 'NO'
			   AND $serial_cou = $cus_serial_cou";
		//die($sql);
		$result=$db->getOne($sql);
		if($result){
				return true;
		}else{
				return false;
		}
	}

	/*
	 * @name: getExtraOptions
	 * @param: $db - DB connection
	 * @returns: An array of all the extra types values in DB
	 */
	public static function getExtraTypes($db){
		$sql = "SHOW COLUMNS FROM product LIKE 'extras_restricted_to_pro'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
	}
	
	public static function generateNumberValidation($db, $serial_pro){
		$sql = "SELECT generate_number_pro
				FROM product 
				WHERE serial_pro = $serial_pro";
		
		$result = $db ->getOne($sql);
		
		if($result){
			if($result == 'YES'){
				return TRUE;
			}else{
				return FALSE;
			}			
		}else{
			return FALSE;
		}
	}

	public static function updateErpId($db, $serial_pro, $erp_serial){
		echo $erp_serial;
	}
	
	public static function getProductInformationForERP($db, $serial_inv){
		$sql_product = "SELECT	number_inv, date_inv, total_sal, applied_taxes_inv,
							(discount_prcg_inv + other_dscnt_inv) AS 'discounts',
							CAST(IF(i.serial_cus IS NULL, CONCAT('D',i.serial_dea), CONCAT('C', i.serial_cus)) AS CHAR) AS 'code_cus',
							CAST(CONCAT('0', pxc.serial_pxc, '0', dbm.serial_man ) AS CHAR) AS 'code_product',
							s.serial_sal, i.serial_inv
					FROM invoice i
					JOIN sales s ON s.serial_inv = i.serial_inv
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
					WHERE i.serial_inv = $serial_inv
					ORDER BY i.number_inv";
	
		$products = $db ->getAll($sql_product);
		
		if($products){
			return $products;
		}else{
			return FALSE;
		}
	}
	
	public static function getCorporativeProducts($db, $serial_lang){
		$sql = 	"SELECT pro.serial_pro,
						pbl.name_pbl,
						pbl.description_pbl,
						pro.has_comision_pro,
						pro.schengen_pro,
						pro.in_web_pro,
						pro.flights_pro,
						pro.max_extras_pro,
						pro.children_pro,
						pro.adults_pro,
						pro.spouse_pro,
						pro.relative_pro,
						pro.third_party_register_pro,
						pro.masive_pro,
						pro.emission_country_pro,
						pro.residence_country_pro,
						pro.image_pro,
						pro.serial_tpp,
						pro.senior_pro,
						pro.price_by_range_pro,
						pro.calculate_pro,
						pro.generate_number_pro,
						pro.status_pro,
						pro.price_by_day_pro,
						pro.has_services_pro,
						pro.limit_pro,
						pro.show_price_pro,
						pro.restricted_children_pro,
						pro.individual_pro
				FROM product pro
				JOIN product_by_language pbl ON pro.serial_pro= pbl.serial_pro
				JOIN language lang ON lang.serial_lang=pbl.serial_lang AND lang.serial_lang='$serial_lang'
				WHERE pro.third_party_register_pro = 'YES'";

		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		$array = array();
		
		if($num > 0){
			$count = 0;
			do{
				$array[$count++] = $result -> GetRowAssoc(false);
				$result -> MoveNext();
			}while($count < $num);
		}
		
		return $array;
	}

    public static function getProductByDealerByPBbC($db, $serial_pbc , $serial_coun  ) {
          $sql ="  SELECT pbd.serial_pbd FROM product_by_country pbc
                        JOIN product_by_dealer pbd ON pbd.serial_pxc = pbc.serial_pxc                          
                        JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                        JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_coun'
                        WHERE pbc.serial_pxc = '$serial_pbc'";
          //echo $sql; die();
          $result = $db->getOne($sql);
          
		if($result){
			return $result;
		}else{
			return false;
		}	
        }
        
        public static function getBenefitsByProduct($db,$serial_pro  ) {
          $sql ="SELECT bc.description_bcat,bp.serial_ben, 
					bl.description_bbl, 
					price_bxp,					
					bp.restriction_price_bxp,
					rl.description_rbl,
					cl.description_cbl					
					FROM benefits_product bp
					LEFT JOIN benefits_by_language bl ON bp.serial_ben = bl.serial_ben AND bl.serial_lang = 2
					LEFT JOIN conditions_by_language cl ON cl.serial_con = bp.serial_con AND cl.serial_lang = 2
					LEFT JOIN currency cr ON cr.serial_cur = bp.serial_cur
					LEFT JOIN restriction_by_language rl ON rl.serial_rst = bp.serial_rst AND rl.serial_lang = 2
					LEFT JOIN benefits ben ON ben.serial_ben = bp.serial_ben
                    LEFT JOIN benefits_categories bc ON bc.serial_bcat = ben.serial_bcat
					WHERE bp.serial_pro = '$serial_pro'					
                    ORDER BY bc.description_bcat";
          //echo $sql; die();
          $result = $db->getAll($sql);
          
		if($result){
			return $result;
		}else{
			return false;
		}	
        }
        
         public static function getListProductsByTravelMotiveAndDestination($db, $serial_mot, $serial_cnt, $language, $serial_cou, $serial_orig ){
           
            $sql = "SELECT pxc.serial_pxc,  pxl.name_pbl, pro.in_web_pro, pro.children_pro, pro.senior_pro, pro.flights_pro, pro.emission_country_pro, pxl.description_pbl,
                pro.max_extras_pro, pro.spouse_pro, pro.relative_pro, pro.has_services_pro,pro.restricted_children_pro,pro.individual_pro
                FROM product pro
                JOIN product_by_country pxc ON pro.serial_pro = pxc.serial_pro AND pro.in_web_pro = 'YES' 
                AND pro.status_pro = 'ACTIVE'
                JOIN product_by_language pxl ON pro.serial_pro = pxl.serial_pro AND pxl.serial_lang = '$language'
                JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
                JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                JOIN allowed_destinations ald ON ald.serial_pxc = pxc.serial_pxc 
                JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_cnt'
                JOIN travelmotives_by_product tmbp ON tmbp.serial_pxc = pxc.serial_pxc AND tmbp.serial_trm = '$serial_mot' AND 
                ald.serial_cou = '$serial_cou'";
            
             if ($serial_cou == $serial_orig){
                $sql.= "AND pro.emission_country_pro = 'YES'";               
            } else {
                $sql.= "AND pro.emission_country_pro = 'NO'";}
            
            $sql.= "ORDER BY  pro.value_pro DESC" ;            
            // echo $sql; die();  
             $result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
    
      
        /******************************VA
		 * @name: getListProducts
                 * 
		 * @return: array with the products have this Country Origen - Destination
                 * POST: Country Origi Country Destination 
		 */

        public static function getListProducts($db,$serial_cou, $language ){
           
            $sql = "SELECT pxc.serial_pxc,  pxl.name_pbl, pro.in_web_pro, pro.children_pro, pro.senior_pro, pro.flights_pro, pro.emission_country_pro, pxl.description_pbl,
                pro.max_extras_pro, pro.spouse_pro, pro.relative_pro, pro.has_services_pro,pro.restricted_children_pro,pro.individual_pro
                FROM product pro
                JOIN product_by_country pxc ON pro.serial_pro = pxc.serial_pro AND pro.in_web_pro = 'YES' 
                AND pro.status_pro = 'ACTIVE'
                JOIN product_by_language pxl ON pro.serial_pro = pxl.serial_pro AND pxl.serial_lang = '$language'
                JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
                JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_cou'";
            
            $result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
    
      /******************************VA
		 * @name: getProductsByCountryOriginAndDestination
                 * 
		 * @return: array with the products have this Country Origen - Destination
                 * POST: Country Origi Country Destination 
		 */

        public static function getProductListByCountry($db, $serial_cou_pxc , $serial_cou_ald  , $language ){
		
            if ($language == NULL)
           {
               $language = 2;
           }
           
                   
            $sql = " SELECT DISTINCT  pxc.serial_pxc,  pxl.name_pbl, pro.in_web_pro, pro.children_pro, pro.senior_pro, pro.flights_pro, pro.emission_country_pro, pxl.description_pbl,
                pro.max_extras_pro, pro.spouse_pro, pro.relative_pro, pro.has_services_pro,pro.restricted_children_pro,pro.individual_pro
                FROM product pro
                JOIN product_by_country pxc ON pro.serial_pro = pxc.serial_pro AND pro.in_web_pro = 'YES' 
                AND pro.status_pro = 'ACTIVE'
                JOIN product_by_language pxl ON pro.serial_pro = pxl.serial_pro AND pxl.serial_lang = '2'
                JOIN allowed_destinations ald ON ald.serial_pxc = pxc.serial_pxc 
                JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
                WHERE  pxc.serial_cou ='$serial_cou_pxc' AND ald.serial_cou = '$serial_cou_ald'   
        ";           
            if ($serial_cou_pxc == $serial_cou_ald){
                $sql.= "AND pro.emission_country_pro = 'YES'";               
            } else {
                $sql.= "AND pro.emission_country_pro = 'NO'";}
             //echo $sql; die();   
                $result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
    
        
        /******************************VA
		 * @name: getListProductsByTravelMotive
                 * 
		 * @return: array with the products have this Country Origen - Destination
                 * POST: Country Origi Country Destination 
		 */

        public static function getListProductsByTravelMotive($db,$serial_mot, $serial_cnt, $language ){
           
            $sql = "SELECT pxc.serial_pxc,  pxl.name_pbl, pro.in_web_pro, pro.children_pro, pro.senior_pro, pro.flights_pro, pro.emission_country_pro, pxl.description_pbl,
                pro.max_extras_pro, pro.spouse_pro, pro.relative_pro, pro.has_services_pro,pro.restricted_children_pro,pro.individual_pro
                FROM product pro
                JOIN product_by_country pxc ON pro.serial_pro = pxc.serial_pro AND pro.in_web_pro = 'YES' 
                AND pro.status_pro = 'ACTIVE'
                JOIN product_by_language pxl ON pro.serial_pro = pxl.serial_pro AND pxl.serial_lang = '$language'
                JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
                JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_cnt'
                JOIN travelmotives_by_product tmbp ON tmbp.serial_pxc = pxc.serial_pxc AND tmbp.serial_trm = '$serial_mot'";
               
            $result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
    
         /*
	* @name: alreadySold
	* @param: $db - DB connection
	* @param: $serial_pro - Product ID
	* @returns: The referential additional day fee for all countries.
	*/
	public static function getProduct($db, $serial_pxc,$originCountry, $destinationCountry, $language){
		$sql= " SELECT DISTINCT pro.serial_pro, pbc.serial_pxc, pbl.name_pbl, pbl.description_pbl, pro.senior_pro, pro.children_pro, pro.max_extras_pro,
                        pro.spouse_pro, pro.relative_pro, pbc.percentage_extras_pxc,pro.restricted_children_pro,pro.individual_pro
                        FROM product pro
                        JOIN product_by_language pbl ON pro.serial_pro = pbl.serial_pro AND pbl.serial_lang = '$language'
                        JOIN product_by_country pbc ON pbc.serial_pro = pro.serial_pro AND pbc.serial_cou = '$originCountry'
                        JOIN allowed_destinations ad ON ad.serial_pxc = pbc.serial_pxc AND ad.serial_cou = '$destinationCountry'  
                        WHERE pbc.serial_pxc = '$serial_pxc' ";
		//die($sql);
		$result=$db->getAll($sql);

		if($result){
				return $result;
		}else{
				return false;
		}

	}
	///GETTERS
	function getSerial_pro(){
		return $this->serial_pro;
	}
	function getName_pro(){
		return $this->name_pro;
	}
	function getHas_comision_pro(){
		return $this->has_comision_pro;
	}
	function getSchengen_pro(){
		return $this->schengen_pro;
	}
	function getIn_web_pro(){
		return $this->in_web_pro;
	}
	function getFlights_pro(){
		return $this->flights_pro;
	}
	function getMax_extras_pro(){
		return $this->max_extras_pro;
	}
	function getChildren_pro(){
		return $this->children_pro;
	}
	function getAdults_pro(){
		return $this->adults_pro;
	}
	function getSpouse_pro(){
		return $this->spouse_pro;
	}
	function getRelative_pro(){
		return $this->relative_pro;
	}
	function getThird_party_register_pro(){
		return $this->third_party_register_pro;
	}
	function getMasive_pro(){
		return $this->masive_pro;
	}
	function getEmission_country_pro(){
		return $this->emission_country_pro;
	}
	function getResidence_country_pro(){
		return $this->residence_country_pro;
	}
	function getImage_pro(){
		return $this->image_pro;
	}
	function getSerial_tpp(){
		return $this->serial_tpp;
	}
	function getSenior_pro(){
		return $this->senior_pro;
	}
	function getPrice_by_range_pro(){
		return $this->price_by_range_pro;
	}
	function getCalculate_pro(){
		return $this->calculate_pro;
	}
	function getGenerate_number_pro(){
		return $this->generate_number_pro;
	}
  	function getStatus_pro(){
		return $this->status_pro;
	}
  	function getPrice_by_day_pro(){
		return $this->price_by_day_pro;
	}
  	function getHas_Services_pro(){
		return $this->has_services_pro;
	}
  	function getLimit_pro(){
		return $this->limit_pro;
	}
  	function getShow_price_pro(){
		return $this->show_price_pro;
	}
	function getDestination_restricted_pro(){
		return $this->destination_restricted_pro;
	}
	function getExtras_restricted_to_pro(){
		return $this->extras_restricted_to_pro;
	}
	function getRepresentative_pro(){
		return $this->representative_pro;
	}
	function getRestricted_children_pro(){
	    return $this->restricted_children_pro;
    }
    function getIndividual_pro(){
	    return $this->individual_pro;
    }
	
	///SETTERS	
	function setSerial_pro($serial_pro){
		$this->serial_pro = $serial_pro;
	}
	function setName_pro($name_pro){
		$this->name_pro = $name_pro;
	}
	function setHas_comision_pro($has_comision_pro){
		$this->has_comision_pro = $has_comision_pro;
	}
	function setSchengen_pro($schengen_pro){
		$this->schengen_pro = $schengen_pro;
	}
	function setIn_web_pro($in_web_pro){
		$this->in_web_pro = $in_web_pro;
	}	
	function setFlights_pro($flights_pro){
		$this->flights_pro = $flights_pro;
	}
	function setMax_extras_pro($max_extras_pro){
		$this->max_extras_pro = $max_extras_pro;
	}
	function setChildren_pro($children_pro){
		$this->children_pro = $children_pro;
	}
	function setAdults_pro($adults_pro){
		$this->adults_pro = $adults_pro;
	}
	function setSpouse_pro($spouse_pro){
		$this->spouse_pro = $spouse_pro;
	}
	function setRelative_pro($relative_pro){
		$this->relative_pro = $relative_pro;
	}
	function setThird_party_register_pro($third_party_register_pro){
		$this->third_party_register_pro = $third_party_register_pro;
	}
	function setMasive_pro($masive_pro){
		$this->masive_pro = $masive_pro;
	}
	function setEmission_country_pro($emission_country_pro){
		$this->emission_country_pro = $emission_country_pro;
	}
	function setResidence_country_pro($residence_country_pro){
		$this->residence_country_pro = $residence_country_pro;
	}
    function setImage_pro($image_pro){
		$this->image_pro = $image_pro;
	}
    function setSerial_tpp($serial_tpp){
		$this->serial_tpp = $serial_tpp;
	}
    function setSenior_pro($senior_pro){
		$this->senior_pro = $senior_pro;
	}
    function setPrice_by_range_pro($price_by_range_pro){
		$this->price_by_range_pro = $price_by_range_pro;
	}
    function setCalculate_pro($calculate_pro){
		$this->calculate_pro = $calculate_pro;
	}
	function setGenerate_number_pro($generate_number_pro){
		$this->generate_number_pro = $generate_number_pro;
	}
    function setStatus_pro($status_pro){
		$this->status_pro = $status_pro;
	}
    function setPrice_by_day_pro($price_by_day_pro){
		$this->price_by_day_pro = $price_by_day_pro;
	}
    function setHas_services_pro($has_services_pro){
		$this->has_services_pro = $has_services_pro;
	}
	function setLimit_pro($limit_pro){
		$this->limit_pro = $limit_pro;
	}
	function setShow_price_pro($show_price_pro){
		$this->show_price_pro = $show_price_pro;
	}
	function setDestination_restricted_pro($destination_restricted_pro){
		$this->destination_restricted_pro = $destination_restricted_pro;
	}
	function setExtras_restricted_to_pro($extras_restricted_to_pro){
		$this->extras_restricted_to_pro = $extras_restricted_to_pro;
	}
	function setRepresentative_pro($representatative_pro){
		$this->representative_pro = $representatative_pro;
	}
	function setRestricted_children_pro($restricted_children_pro){
	    $this->restricted_children_pro= $restricted_children_pro;
    //Debug::print_r($this->restricted_children_pro);die();
	}
	function setIndividual_pro($individual_pro){
	    $this->individual_pro= $individual_pro;
    //Debug::print_r($this->restricted_children_pro);die();
	}
}
?>
