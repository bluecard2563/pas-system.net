<?php
/*
File: DealerType.class.php
Author: Patricio Astudillo 
Creation Date: 06/01/2010 12:32
Last Modified: 13/05/2010
Modified By: David Bergmann
*/
class DealerType {				
	var $db;
        var $serial_dlt;
	var $serial_dtl;
	var $serial_lang;	
	var $description_dtl;
	var $name_dlt;
	var $status_dlt;
	
	function __construct($db, $serial_dlt = NULL, $serial_dtl = NULL, $serial_lang = NULL, $description_dtl = NULL, $name_dlt = NULL, $status_dlt = NULL){
		$this -> db = $db;
                $this -> serial_dlt = $serial_dlt;
		$this -> serial_dtl = $serial_dtl;
		$this -> serial_lang = $serial_lang;
		$this -> description_dtl = $description_dtl;
		$this -> name_dlt = $name_dlt;
		$this -> status_dlt = $status_dlt;
	}
	
	/** 
	@Name: getData
	@Description: Retrieves the data of a dealer_type object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
            if($this->serial_dlt!=NULL){
                $sql =  "SELECT dt.serial_dlt, dt.name_dlt, dt.status_dlt
                         FROM dealer_type dt
                         WHERE dt.serial_dlt='".$this->serial_dlt."'";
                $result = $this->db->Execute($sql);
                if ($result === false) return false;

                if($result->fields[0]){
                    $this->serial_dlt=$result->fields[0];
                    $this->name_dlt=$result->fields[1];
                    $this->status_dlt=$result->fields[2];
                    return true;
                }
                else
                    return false;
            }else
                return false;
	}
	
	/** 
	@Name: getDataByLanguage
	@Description: Retrieves the data of a dealer_type object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getDataByLanguage($serial_dtl,$serial_lang=NULL){
		if($serial_dtl!=NULL){
			$sql = 	"SELECT dtl.serial_dtl, dtl.serial_lang, dtl.serial_dlt, dt.name_dlt, dtl.description_dtl, dt.status_dlt
                                 FROM dealer_ty_by_language dtl
                                 JOIN dealer_type dt ON dtl.serial_dlt=dt.serial_dlt
                                 WHERE dtl.serial_dtl='$serial_dtl'";
                        if($serial_lang != NULL) {
                            $sql .= " dtl.AND serial_lang = '$serial_lang'";
                        }
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_dtl=$result->fields[0];
				$this->serial_lang=$result->fields[1];
				$this->serial_dlt=$result->fields[2];
				$this->name_dlt=$result->fields[3];	
				$this->description_dtl=$result->fields[4];
				$this->status_dlt=$result->fields[5];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}

        /**
	@Name: getDataByLanguage
	@Description: Retrieves the data of a dealer_type object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getDealerTypeByLanguage($serial_lang=NULL){
		if($this->serial_dlt!=NULL){
			$sql = 	"SELECT dtl.serial_dtl, dtl.serial_lang, dtl.serial_dlt, dt.name_dlt, dtl.description_dtl, dt.status_dlt
                                 FROM dealer_ty_by_language dtl
                                 JOIN dealer_type dt ON dtl.serial_dlt=dt.serial_dlt
                                 WHERE dtl.serial_dlt='$this->serial_dlt'
                                 AND serial_lang = '$serial_lang'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_dtl=$result->fields[0];
				$this->serial_lang=$result->fields[1];
				$this->serial_dlt=$result->fields[2];
				$this->name_dlt=$result->fields[3];
				$this->description_dtl=$result->fields[4];
				$this->status_dlt=$result->fields[5];
				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/** 
	@Name: insertDealerType
	@Description: Inserts a new register in DB
	@Params: N/A
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function insertDealerType(){
		$sql="INSERT INTO dealer_type (
					serial_dlt,
					name_dlt,
					status_dlt)
			  VALUES(NULL,
					'".$this->name_dlt."',
					'".$this->status_dlt."');";
		$result = $this->db->Execute($sql);
		//echo $sql;
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/** 
	@Name: insertDealerTypeByLanguage
	@Description: Inserts a new register in DB
	@Params: N/A
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function insertDealerTypeByLanguage(){
		$sql="INSERT INTO dealer_ty_by_language (
					serial_dtl,
					serial_lang,
					serial_dlt,
					description_dtl)
			  VALUES(NULL,
					'".$this->serial_lang."',
					'".$this->serial_dlt."',
					'".$this->description_dtl."');";
		$result = $this->db->Execute($sql);
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/** 
	@Name: update
	@Description: Updates a register in DB
	@Params: ID
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function update(){
		$sql="UPDATE dealer_ty_by_language
			  SET description_dtl='".$this->description_dtl."'
			  WHERE serial_dtl='".$this->serial_dtl."'";
		$result = $this->db->Execute($sql);

		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/** 
	@Name: updateDealerTypeStatus
	@Description: Updates the status from a dealerType a register in DB
	@Params: ID
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function updateDealerTypeStatus(){
		$sql="UPDATE dealer_type
			  SET status_dlt='".$this->status_dlt."'
			  WHERE serial_dlt='".$this->serial_dlt."'";
					
		$result = $this->db->Execute($sql);

		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/** 
	@Name: getDealerTypesAutocompleter
	@Description: Retrieves all dealer types from DB
	@Params: $name_dlt (search pattern for alias)
	@Returns: Array of dealer types.
	**/
	function getDealerTypesAutocompleter($name_dlt){
		$list=NULL;
		$sql = 	"SELECT dtl.serial_dtl, dtl.serial_lang, dtl.serial_dlt, dt.name_dlt, dtl.description_dtl
				FROM dealer_ty_by_language dtl
				JOIN dealer_type dt ON dtl.serial_dlt=dt.serial_dlt
				WHERE (LOWER(dt.name_dlt) LIKE _utf8'%".utf8_encode($name_dlt)."%' collate utf8_bin
				OR LOWER(dtl.description_dtl) LIKE _utf8'%".utf8_encode($name_dlt)."%' collate utf8_bin)
				AND dtl.serial_lang = '".$this->serial_lang."'";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}	
	
	/** 
	@Name: getDealerTypes
	@Description: Retrieves all dealer types from DB
	@Params: N/A
	@Returns: Array of dealer types.
	**/
	function getDealerTypes($serialDea=NULL,$serial_lang=NULL){
		$sql = 	"SELECT dtl.serial_dtl as 'serial', dt.name_dlt as 'alias', dtl.description_dtl as 'name', lang.name_lang as 'language', lang.serial_lang, dtl.serial_dtl as 'update'";
		if($serial_lang != NULL) {
			$sql.=" ,dtl.serial_dlt";
		}
		 $sql.=" FROM dealer_type dt
		 JOIN dealer_ty_by_language dtl ON dtl.serial_dlt=dt.serial_dlt
		 JOIN language lang ON lang.serial_lang=dtl.serial_lang";
		if($serialDea != NULL) {
			$sql.=" AND dt.serial_dlt = ".$serialDea;
		}
		if($serial_lang != NULL) {
			$sql.=" AND dtl.serial_lang = ".$serial_lang;
		}
		$sql.=" ORDER BY dtl.description_dtl asc";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}	
	
	/** 
	@Name: existDealerType
	@Description: Verifies the existance of a dealer_type in DB
	@Params: $name_dlt (alias), $serial_dlt (ID)
	@Returns: TRUE if exists / FALSE if not
	**/
	function existDealerType($name_dlt,$serial_dlt=NULL){
		$sql = 	"SELECT serial_dlt
				 FROM dealer_type
				 WHERE LOWER(name_dlt)= _utf8'".utf8_encode($name_dlt)."' collate utf8_bin";
		if($serial_dlt!=NULL){
			$sql.=" AND serial_dlt <> ".$serial_dlt;
		}
				 
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
		
	/** 
	@Name: existDealerTypeByLanguage
	@Description: Verifies the existance of a dealer_ty_by_language in DB
	@Params: $serial_dlt (ID), $serial_lang (language)
	@Returns: TRUE if exists / FALSE if not
	**/
	function existDealerTypeByLanguage($description_dtl,$serial_lang,$serial_dtl=NULL){
		$sql = 	"SELECT serial_dtl
				 FROM dealer_ty_by_language
				 WHERE LOWER(description_dtl)= _utf8'".($description_dtl)."' collate utf8_bin
				 AND serial_lang = ".$serial_lang;
		if($serial_dtl!=NULL){
			$sql.=" AND serial_dtl <> ".$serial_dtl;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/** 
	@Name:isUsedDealerType
	@Description: Verifies the if a dealer_ty_by_language is been used by a dealer
	@Params: $serial_dlt (ID)
	@Returns: TRUE if it's been used / FALSE if not
	**/
	function isUsedDealerType($serial_dlt){
		$sql = 	"SELECT dlt.serial_dlt
				 FROM dealer_type dlt
				 JOIN dealer dea on dea.serial_dlt = dlt.serial_dlt
				 WHERE dlt.serial_dlt='".$serial_dlt."'";
		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/** 
	@Name: getDealerTypeSerial
	@Description: Returns the serial acording to the name
	@Params: $name_dlt (alias)
	@Returns: $serial_dlt
	**/
	function getDealerTypeSerial($name_dlt){
		$sql = 	"SELECT serial_dlt
				 FROM dealer_type
				 WHERE name_dlt='".$name_dlt."'";
				 
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}
	}
        
        function getDealerTypeInfo(){
		$sql = 	"SELECT dtl.serial_dtl, dtl.serial_lang, dtl.serial_dlt, dt.name_dlt, dtl.description_dtl, dt.status_dlt
                                 FROM dealer_ty_by_language dtl
                                 JOIN dealer_type dt ON dtl.serial_dlt=dt.serial_dlt";
                        
			//echo ($sql);
                        $result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$arreglo = array();
				$cont = 0;

				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
			return $arreglo;
		 
        }

	///GETTERS
	function getSerial_dtl(){
		return $this->serial_dtl;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}
	function getSerial_dlt(){
		return $this->serial_dlt;
	}
	function getDescription_dtl(){
		return $this->description_dtl;
	}
	function getName_dlt(){
		return $this->name_dlt;
	}
	function getStatus_dlt(){
		return $this->status_dlt;
	}
	///SETTERS
	function setSerial_dtl($serial_dtl){
		$this->serial_dtl = $serial_dtl;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang=$serial_lang;
	}
	function setSerial_dlt($serial_dlt){
		$this->serial_dlt = $serial_dlt;
	}
	function setDescription_dtl($description_dtl){
		$this->description_dtl = $description_dtl;
	}
	function setName_dlt($name_dlt){
		$this->name_dlt = $name_dlt;
	}
	function setStatus_dlt($status_dlt){
		$this->status_dlt = $status_dlt;
	}
}
?>
