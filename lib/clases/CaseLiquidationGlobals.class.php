<?php
/**
 * File: CaseLiquidationGlobals
 * Author: Patricio Astudillo
 * Creation Date: 07/03/2014
 * Last Modified: 07/03/2014
 * Modified By: Patricio Astudillo
*/

class CaseLiquidationGlobals {
	/**
	 * @param ADODB $db DB connection
	 * @param int $creator_id User ID
	 * @param array $default_data Array set for settlement header
	 * @return boolean Process outcome
	 */
	public static function registerSettlement($db, $creator_id, $default_data){
		$sql = "INSERT INTO settlement
				(
					id,
					creator,
					number,
					total_requested,
					total_paid,
					total_refunded
				) VALUES (
					NULL,
					$creator_id,
					{$default_data['number']},
					{$default_data['total_requested']},
					{$default_data['total_paid']},
					{$default_data['total_refunded']}
				)";
					
		$result = $db->Excecute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'Insert Failed - Settlement', $sql);
			return FALSE;
		}
	}
	
	/**
	 * @param ADODB $db DB connection
	 * @param int $settlement_id Settlement unique id
	 * @param array $prq_array Array set for all paymentRequest to settle
	 * @return boolean Process outcome
	 */
	public static function registerSettlementDetail($db, $settlement_id, $prq_array){
		foreach($prq_array as $prequest){
			$sql = "INSERT INTO settlement
					(
						id,
						settlement_id,
						serial_prq
					) VALUES (
						NULL,
						$settlement_id,
						{$prequest['serial_prq']}
					)";

			$result = $db->Excecute($sql);

			if(!$result){
				ErrorLog::log($db, 'Insert Failed - Settlement Detail', $sql);
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	/**
	 * 
	 * @param ADODB $db DB connection
	 * @param string $fromDate Date in dd/mm/YYYY format. Default 01/01/2014
	 * @return boolean Process failed
	 * @return array If  succecc, array with selection data
	 */
	public static function retrieveNonSettledCases($db, $fromDate = '01/01/2014'){
		$sql = "SELECT f.serial_fle, IFNULL(tl.card_number_trl, s.card_number_sal) AS 'policy', 
						CONCAT(ben.first_name_cus, ' ', ben.last_name_cus) AS 'beneficiary'
				FROM payment_request prq
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
					AND dbf.document_to_dbf <> 'NONE'
				JOIN file f ON f.serial_fle = prq.serial_fle 
					AND prq.settled = 'NO' 
					AND STR_TO_DATE(DATE_FORMAT(creation_date_fle, '%d/%m/%Y'), '%d/%m/%Y') >= STR_TO_DATE('$fromDate', '%d/%m/%Y')
					AND status_prq = 'APROVED'
				JOIN sales s ON s.serial_sal = f.serial_sal
				
				JOIN customer ben ON ben.serial_cus = f.serial_cus
				LEFT JOIN traveler_log tl ON tl.serial_trl = f.serial_trl
				GROUP BY f.serial_fle";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * 
	 * @param ADODB $db DB connection
	 * @param int $serial_fle File unique ID
	 * @return boolean Process failed
	 * @return array If  succecc, array with selection data
	 */
	public static function retrieveNonSettledInvoicesForCase($db, $serial_fle){
		$sql = "SELECT	prq.serial_prq, prq.serial_dbf, dbf.name_dbf, dbf.comment_dbf, 
						dbf.document_to_dbf, 
						DATE_FORMAT(dbf.document_date_dbf, '%d/%m/%Y') AS 'document_date'
				FROM payment_request prq
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
					AND dbf.document_to_dbf <> 'NONE'
					AND dbf.serial_fle = $serial_fle
				JOIN file f ON f.serial_fle = prq.serial_fle 
					AND prq.settled = 'NO' 
					AND status_prq = 'APROVED'";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * 
	 * @param ADODB $db DB Connection
	 * @param int $serial_sal
	 * @return boolean Process failed
	 * @return array If  succecc, array with selection data
	 */
	public static function getSaleDataForSettlement($db, $serial_sal){
		$sql = "SELECT vsi.*, vbc.*
				FROM view_settlements_info  vsi
				JOIN view_benfits_in_cards vbc ON vbc.serial_pxc = vsi.serial_pxc
				WHERE serial_sal = $serial_sal";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function settleCaseInvoices($db, $prq_list){
		$sql = "UPDATE payment_request SET settled = 'YES' WHERE serial_prq IN ($prq_list)";
		
		$result = $db->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}

