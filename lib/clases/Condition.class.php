<?php
/*
File: Condition.class.php
Author: Miguel Ponce.
Creation Date: 04/01/2010
Last Modified: 04/01/2010
Modified By: Miguel Ponce
*/

class Condition{				
	var $db;
	var $serial_con;
	var $alias_con;

	function __construct($db, $serial_con=NULL, $alias_con=NULL){
		$this -> db = $db;
		$this -> serial_con = $serial_con;
		$this -> alias_con = $alias_con;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_ben
    ***********************************************/
	function getData(){
		if($this->serial_con!=NULL){
			$sql = 	"SELECT serial_con,alias_con
					FROM conditions
					WHERE serial_con='".$this->serial_con."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_con =$result->fields[0];
				$this -> alias_con = $result->fields[1];

			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	/***********************************************
    * funcion getData
    * verifies if a Conditions exists or not
    ***********************************************/
/*	function benefitExists($description_con, $serial_con=NULL){
		$sql = 	"SELECT serial_con
				 FROM conditions 
				 WHERE LOWER(description_con)='".$description_con."'";
		if($serial_con!=NULL){
			$sql.=" AND serial_con <> ".$serial_con;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}*/
	
	/***********************************************
    * funcion getConditions
    * gets all the Conditions of the system
    ***********************************************/
	function getConditions($code_lang){
		$lista=NULL;
		$sql = 	"SELECT c.serial_con, c.alias_con, cbl.description_cbl
				 FROM conditions c 
				 JOIN conditions_by_language cbl 
				 ON c.serial_con=cbl.serial_con
                                 JOIN language l ON l.serial_lang = cbl.serial_lang
                                 AND l.code_lang='".$code_lang."'";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}
	
		
		
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO conditions (
				serial_con,alias_con)
            VALUES (
                NULL,'".
				$this->alias_con
				."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 		/** 
	@Name: getRestrictionsAutocompleter
	@Description: Returns an array of restrictions.
	@Params: Restriction name or pattern.
	@Returns: Restriction array
	**/
	function getConditionsAutocompleter($description_con, $serial_lang){
		$sql = "SELECT c.serial_con, cbl.description_cbl, cbl.serial_cbl
			FROM conditions c
                        JOIN conditions_by_language cbl ON cbl.serial_con=c.serial_con AND cbl.serial_lang='".$serial_lang."'
			WHERE LOWER(cbl.description_cbl) LIKE LOWER(_utf8'%".utf8_encode($description_con)."%' collate utf8_bin)
			LIMIT 10";

		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}



    /**
	@Name: getTranslations
	@Description: Returns an array of all the translations for a specific restrictions
	@Params: $serial_rst: Restriction ID
	@Returns: An array of translations
	**/
	function getTranslations($serial_con){
		$sql = "SELECT cbl.serial_cbl as 'serial', cbl.description_cbl, l.name_lang, cbl.serial_cbl as 'update', l.serial_lang
				FROM conditions c
				JOIN conditions_by_language cbl ON c.serial_con = cbl.serial_con
				JOIN language l ON l.serial_lang = cbl.serial_lang
				WHERE c.serial_con = '".$serial_con."'";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}


	///GETTERS
	function getSerial_con(){
		return $this->serial_con;
	}

	function getAlias_con(){
		return $this->alias_con;
	}

	///SETTERS	
	function setSerial_con($serial_con){
		$this->serial_con = $serial_con;
	}

	function setAlias_con($alias_con){
		$this->alias_con = $alias_con;
	}
}
?>