<?php
/**
 * File: PConditionsByProductCountry
 * Author: Patricio Astudillo
 * Creation Date: 27-ago-2012
 * Last Modified: 27-ago-2012
 * Modified By: Patricio Astudillo
 */

class PConditionsByProductCountry{
	var $db;
	var $serial_pcp;
	var $serial_pxc;
	var $serial_prc;
	var $status_pcp;
	var $weight_pcp;
	
	function __construct($db, $serial_pcp = NULL, $serial_pxc = NULL, $serial_prc = NULL, 
							$status_pcp = 'ACTIVE', $weight_pcp = '99') {
		$this->db = $db;
		$this->serial_pcp = $serial_pcp;
		$this->serial_pxc = $serial_pxc;
		$this->serial_prc = $serial_prc;
		$this->status_pcp = $status_pcp;
		$this->weight_pcp = $weight_pcp;
		
		$this->getData();
	}

	public function getData(){
		if($this->serial_pcp){
			$sql = "SELECT	serial_pcp, 
							serial_pxc, 
							serial_prc, 
							status_pcp, 
							weight_pcp 
					FROM pconditions_pxc 
					WHERE serial_pcp = {$this->serial_pcp}";
			
			$result = $this->db->getRow($sql);
			
			if($result){
				$this->serial_pcp = $result['serial_pcp'];
				$this->serial_pxc = $result['serial_pxc'];
				$this->serial_prc = $result['serial_prc'];
				$this->status_pcp = $result['status_pcp'];
				$this->weight_pcp = $result['weight_pcp'];
				
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	/**
	 * @name insert
	 * @return boolean 
	 */
	function insert(){
		$sql = "INSERT INTO pconditions_pxc (
					serial_pcp, 
					serial_pxc, 
					serial_prc, 
					status_pcp, 
					weight_pcp 
				) VALUES (
					NULL,
					{$this->serial_pxc},
					{$this->serial_prc},
					'{$this->status_pcp}',
					'{$this->weight_pcp}'
				)";
					
		$result = $this->db->Execute($sql);
            
		if($result){
			$this->serial_pcp = $this->db->insert_ID();
			return $this->db->insert_ID();
		}else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
			return false;
		}
	}
	
	/**
	 * @name update
	 * @return boolean 
	 */
	function update(){
		$sql = "UPDATE pconditions_pxc SET 
					serial_pxc = {$this->serial_pxc},
					serial_prc = {$this->serial_prc},
					status_pcp = '{$this->status_pcp}',
					weight_pcp = '{$this->weight_pcp}'
					WHERE serial_pcp = {$this->serial_pcp}";
		
		$result = $this->db->Execute($sql);
        
		if ($result === false)return false;
		return true;
	}
	
	/**
	 * @name getPConditionsBySale
	 * @param RS $db
	 * @param int $serial_sal
	 * @param int $serial_lang
	 * @return boolean 
	 * @return array
	 */
	public static function getPConditionsBySale($db, $serial_sal, $serial_lang = 2){
		/*$sql = "SELECT name_prc
				FROM pconditions_pxc pcp
				JOIN particular_conditions prc ON prc.serial_prc = pcp.serial_prc 
					AND prc.status_prc = 'ACTIVE'
					AND prc.serial_lang = $serial_lang
				JOIN product_by_dealer pbd ON pbd.serial_pxc = pcp.serial_pxc
				JOIN sales s ON s.serial_pbd = pbd.serial_pbd
					AND s.serial_sal = $serial_sal
				ORDER BY weight_pcp";*/

        $sql= "SELECT prc.name_prc
				FROM pconditions_pxc pcp
				JOIN particular_conditions prc ON prc.serial_prc = pcp.serial_prc
        		AND prc.status_prc = 'ACTIVE' and pcp.status_pcp = 'ACTIVE'
        		AND prc.serial_lang = $serial_lang
				JOIN product_by_dealer pbd ON pbd.serial_pxc = pcp.serial_pxc
				JOIN sales s ON s.serial_pbd = pbd.serial_pbd
        		AND s.serial_sal = $serial_sal
				ORDER BY weight_pcp";

		//Debug::print_r($sql); die;
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function relationshipExists($db, $serial_prc, $serial_pxc){
		$sql = "SELECT serial_pcp
				FROM pconditions_pxc pcp
				WHERE serial_prc = $serial_prc
				AND serial_pxc = $serial_pxc";
		
		//Debug::print_r($sql); die;
		$result = $db -> getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

    public static function getPxcBySerialPrc($db,$serial_prc){
        $sql="select serial_pxc from particular_conditions prc 
			  join pconditions_pxc pcp on pcp.serial_prc=prc.prc_serial_prc
			  where prc.serial_prc=$serial_prc
			  and prc.status_prc='ACTIVE'
			  and pcp.status_pcp='ACTIVE'";
        $result = $db->getRow($sql);

        if($result){
            return $result;
        }else{
            return FALSE;
        }
    }

    public static function associateTranslation($db, $serial_pxc, $serial_prc){
        $sql = " INSERT INTO pconditions_pxc (
 					serial_pxc, 
 					serial_prc,
 					weight_pcp) 
 					VALUES 
 					($serial_pxc, $serial_prc, '1')
		";
        $result = $db->Execute($sql);
        if ($result === false){
            return false;
        }
        else{
            return true;
        }
    }

    public static function existTranslationAssociation($db, $serial_pxc, $serial_prc) {
    	$sql = "
    		SELECT * FROM pconditions_pxc WHERE serial_pxc = $serial_pxc AND $serial_prc = $serial_prc
    	";
        $result = $db->getOne($sql);
        if ($result)
            return true;
        else
            return false;
	}
	
	public function getSerial_pcp() {
		return $this->serial_pcp;
	}

	public function setSerial_pcp($serial_pcp) {
		$this->serial_pcp = $serial_pcp;
	}

	public function getSerial_pxc() {
		return $this->serial_pxc;
	}

	public function setSerial_pxc($serial_pxc) {
		$this->serial_pxc = $serial_pxc;
	}

	public function getSerial_prc() {
		return $this->serial_prc;
	}

	public function setSerial_prc($serial_prc) {
		$this->serial_prc = $serial_prc;
	}

	public function getStatus_pcp() {
		return $this->status_pcp;
	}

	public function setStatus_pcp($status_pcp) {
		$this->status_pcp = $status_pcp;
	}

	public function getWeight_pcp() {
		return $this->weight_pcp;
	}

	public function setWeight_pcp($weight_pcp) {
		$this->weight_pcp = $weight_pcp;
	}
}
?>
