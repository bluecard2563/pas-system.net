<?php

/*
  File: GlobalFunctions.class.php
  Author: Patricio Astudillo M.
  Creation Date: 19/02/2010 12:09
  Last Modified: 13/04/2010
  Modified By: Santiago Borja
 */

class GlobalFunctions {

    public static function getMyAge($birthDate, $todaysDate = NULL) {
		if(!$todaysDate){
			$todaysDate = date("d") . '/' . date("m") . '/' . date("Y");
		}
		
        $days = GlobalFunctions::getDaysDiffDdMmYyyy($birthDate, $todaysDate);
        $days--;
        $years = $days / 365;
        return (int) ($years);
    }

    public static function getDaysDiffDdMmYyyy($startDate, $endDate) {
        if (!$startDate)
            return 0;
        if (!$endDate)
            return 0;
        $startParts = split('[/.-]', $startDate);
        $endParts = split('[/.-]', $endDate);

        if ($startParts[0] && $startParts[1] && $startParts[2] &&
                $endParts[0] && $endParts[1] && $endParts[2]) {
            $dateDiff = strtotime($endParts[2] . '/' . $endParts[1] . '/' . $endParts[0])
                    - strtotime($startParts[2] . '/' . $startParts[1] . '/' . $startParts[0]);
            //WE ADD 3600 AS 1 HOUR DUE TO PHP DAYLIGHT SAVINGS TIME POLICY OF HAVING MARCH 14TH AS A 23 HOUR DAY
            $returnValue = floor(($dateDiff + 3600) / (60 * 60 * 24));
            $returnValue++;
        } else {
            $returnValue = 0;
        }

        //A -1 TRIP CANNOT SHOW AS A ZERO DAY TRIP
        if ($returnValue == 0) {
            return -1;
        }
        return $returnValue;
    }

    public static function strToTimeDdMmYyyy($dateDdMmYyyy) {
        if (!$dateDdMmYyyy)
            return 0;
        $partsDdMmYyyy = split('[/.-]', $dateDdMmYyyy);
        $aux = explode(' ', $partsDdMmYyyy[2]);
        $partsDdMmYyyy[2] = $aux[0];

        return strtotime($partsDdMmYyyy[2] . '/' . $partsDdMmYyyy[1] . '/' . $partsDdMmYyyy[0]);
    }

    /*
     * @Name: getUsersCountryOfWork
     * @Description: Retrieves the country serial of the workplace of a user.
     * @Params: $db, $serial_usr
     * @Returns: Country ID
     */

    public static function getUsersCountryOfWork($db, $serial_usr) {
        $user = new User($db, $serial_usr);

        if ($user->getData()) {
            if ($user->getBelongsto_usr() == 'MANAGER') {
                $serial_mbc = $user->getSerial_mbc();
            } else {
                $dealer = new Dealer($db, $user->getSerial_dea());
                $dealer->getData();
                $serial_mbc = $dealer->getSerial_mbc();
            }

            $mbc = new ManagerbyCountry($db, $serial_mbc);
            $mbc->getData();
            return $mbc->getSerial_cou();
        } else {
            return false;
        }
    }

    public static function mergeArraysNoRepeat($original, $newValues) {
        $newValuesSingles = array();
        foreach ($newValues as $newVal) {
            $newValuesSingles[$newVal['duration_ppc']] = $newVal['duration_ppc'];
        }

        //THIS FUNCTION WORKS ONLY ON PHP 5.3 OR NEWER:
        //$newTotal = array_replace($original,$newValuesSingles);
        //INSTEAD:

        $newTotal = $original;
        foreach ($newValuesSingles as $key => $newVal) {
            $newTotal[$key] = $newVal;
        }


        //echo '<pre>';print_r($original);echo '</pre> and ';
        //echo '<pre>';print_r($newValues);echo '</pre> give';
        //echo '<pre>';print_r($newTotal);echo '</pre>';

        return $newTotal;
    }

    public static function stringStartsWith($string, $subString) {
        return (substr($string, 0, strlen($subString)) == $subString);
    }

    public static function sendMail($misc, $type) {

        global $db, $global_pType, $global_weekDays, $global_yes_no, $global_typeManager, $global_invoice_number, $global_salesStatus, $global_system_name;
        //Declare a new PHPMailer Object
        $mail = new PHPMailer(true);

        switch ($type) {
            case 'new_user':
                $subject = "REGISTRO DE NUEVO USUARIO DE BAS";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">Estimado Sr.(a): ' . $misc['user'] . '</div><br>
									<div style="font-size:12px;">' . $misc['textForEmail'] . ' <a href="https://www.pas-system.net/" target="_blank">www.pas-system.net</a></div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> ' . $misc['username'] . '
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> ' . $misc['pswd'] . '
                                                    </div><br>
                                    <div><br/><br/></div>
									<div style="font-size:12px;"><br/><br/>
									Le recordamos que su usuario y clave son de uso estrictamente personal y confidencial. <br>
									</div>

									<div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									</div>
									<div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                            <img src="'.URL.'img/contract_logos/logoblue_contracts.jpg" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['email']);
                break;

            case 'happyBirthday':
                $subject = "FELIZ DIA " . $misc['fullname'];
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
	                            <tr><td>
	                            	<img src="' . URL . 'img/happyBirthday.jpg" border="0" />
	                            </td></tr>
                        	</table>';
                $mail->AddAddress($misc['email']);
                break;

            case 'username_updated':
                $subject = "ACTUALIZACION DE NOMBRE DE USUARIO DE PAS";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">Estimado Sr.(a): ' . $misc['user'] . '</div><br>
									<div style="font-size:12px;">Su nombre de usuario ha sido actualizado, de ahora en adelante, para ingresar a <a href="www.planet-assist.net" target="_blank"> www.planet-assist.net</a> deber&aacute; hacerlo utilizando los siguientes datos:</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> ' . $misc['username'] . '
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> ' . $misc['pswd'] . '
                                                    </div><br>
                                    <div><br/><br/></div>
									<div style="font-size:12px;"><br/><br/>
									Le recordamos que su usuario y clave son de uso estrictamente personal y confidencial. <br>
									</div>

									<div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									</div>
									<div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['email']);
                break;

            case 'file_report':
                $subject = "REPORTE EXPEDIENTE";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:14px;"><center><b>REPORTE EXPEDIENTE</b></center></div>
									<div style="font-size:12px;"><br/>Se ha reportado el siguiente expediente:<br/></div>
									<div style="font-size:12px;">
										<center>
											<table border="0">
												<tr bgcolor="#284787">
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Tarjeta n&uacute;mero
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Expediente n&uacute;mero
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Cliente
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Motivo
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Producto
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Reporte generado por
													</td>
												</tr>
												<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e8f2fb">
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['card_number'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['file_number'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['customer'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['diagnosis'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['product'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['user'] . '</td>
												</tr>
											</table>
										</center>
									</div>
                                    <div><br/><br/></div>
									<div style="font-size:12px;">
										<center>
											<table border="0">
												<tr bgcolor="#284787">
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Expediente creado por
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Fecha
													</td>
													<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
														Hora
													</td>
												</tr>
												<tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e8f2fb">
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['counter'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['date'] . '</td>
													<td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $misc['time'] . '</td>
												</tr>
											</table>
										</center>
									</div>
									<div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									</div>
									<div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                foreach ($misc['email_receivers'] as $l) {
                    $mail->AddAddress($l);
                }
                break;

            case 'newClient':
                if ($misc['subject']) {
                    $subject = $misc['subject'];
                } else {
                    $subject = "REGISTRO DE NUEVO CLIENTE";
                }
                $paramBody = new Parameter($db, '10');
                $paramBody->getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
													<div style="font-size:12px;">Estimado Sr.(a): ' . $misc['customer'] . '</div><br>
													<div style="font-size:12px;">Agradecemos su confianza en nosotros.</div><br>';
                if ($misc['cardNumber']) {
                    if ($misc['cardNumber'] == 'N/A') {
                        $message.='<div style="font-size:12px;">Su tarjeta Corporativa fue emitida de manera exitosa, adjunto encontrar&aacute; su contrato y condiciones generales.</div><br>';
                    } else {
                        $message.='<div style="font-size:12px;">Su tarjeta n&uacute;mero ' . $misc['cardNumber'] . ' fue emitida de manera exitosa, adjunto encontrar&aacute; su contrato y condiciones generales.</div><br>';
                    }
                }
                $message.='							<div style="font-size:12px;">' . $paramBody->getValue_par() . '</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> ' . $misc['username'] . '
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> ' . $misc['pswd'] . '
                                                    </div><br>';
                if ($misc['rememberPass']) {
                    $message.='					<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
													<br>' . $misc['rememberPass'] . '
                                                    </div><br>';
                }
                $message.='          <br/>
									<div style="font-size:12px;"><br/><br/>
									Le recordamos que su usuario y clave son de uso estrictamente personal y confidencial. <br>
									</div>

									<div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									<br></div>
									<div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . '</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail->AddAddress($misc['email']);
                break;
                case 'newClient_ptp':
                if ($misc['subject']) {
                    $subject = $misc['subject'];
                } else {
                    $subject = "REGISTRO DE NUEVO CLIENTE";
                }
                global $global_system_logo;

                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                                    <div style="font-size:12px;">Estimado Sr.(a): ' . $misc['client'] . '</div><br>
                                                    <div style="font-size:12px;">Agradecemos su confianza en nosotros.</div><br>';

                $message.='                         <div style="font-size:12px;">Sus usuario y clave son las siguientes:</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> ' . $misc['user'] . '
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> ' . $misc['password'] . '
                                                    </div><br>';

                $message.='          <br/>
                                    <div style="font-size:12px;"><br/><br/>
                                    Le recordamos que su usuario y clave son de uso estrictamente personal y confidencial. <br>
                                    </div>
                                    <div style="font-size:12px;"><br/><br/>
                                    Para revisar su historial de compras por favor hacer click en "Historial de compras" <br>
                                    </div>
                                    <a href="'.$misc['link'].'" class="btn" style="font-size: x-large">Historial de compras</a><br/>
                                    <tr>
                                        <td>Para mayor informaci&oacute;n visite <a>www.bluecard.com.ec</a> o 
                                            comun&iacute;quese al 1800 - BLUECARD (258322) o a los siguientes n&uacute;meros: Quito (02)3332253, Guayaquil: (593-4) 370 3120, 
                                            Cuenca: (07)2850030.</td>
                                    </tr>   
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>Este correo ha sido enviado de forma autom&aacute;tica por su seguridad y no requiere respuesta.</td>
                                    </tr>   
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>Atentamente,</td>
                                    </tr>   
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>   
                        </table>';

                $mail->AddAddress($misc['email']);
                break;

            case 'resetPassword':
                $subject = "REESTABLECER CLAVE DE INGRESO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">' . $misc['textForEmail'] . '</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> ' . $misc['username'] . '
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> ' . $misc['pswd'] . '
                                                    </div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail->AddAddress($misc['email']);
                break;

            case 'mailQuotation':
                $subject = utf8_decode("COTIZACIÓN CLIENTE");
                $message = '<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" style="border-collapse: collapse; height: 1335px; margin: 0px; padding: 0px; width: 1204px; background-color: rgb(250, 250, 250); color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;" width="100%">
  <tbody>
    <tr>
      <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; height: 1335px; padding: 0px; width: 1204px; border-top: 0px;" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" width="100%">
          <tbody>
            <tr>
              <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; background-color: rgb(250, 250, 250); background-image: none; background-repeat: no-repeat; background-position: center center; background-size: cover; border-top: 0px; border-bottom: 0px; padding-top: 9px; padding-bottom: 9px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098templateContainer" style="border-collapse: collapse; max-width: 600px !important; width: 600px !important;" width="100%">
                  <tbody>
                    <tr>
                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                        <br>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; background-color: rgb(255, 255, 255); background-image: none; background-repeat: no-repeat; background-position: center center; background-size: cover; border-top: 0px; border-bottom: 0px; padding-top: 9px; padding-bottom: 0px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098templateContainer" style="border-collapse: collapse; max-width: 600px !important; width: 600px !important;" width="100%">
                  <tbody>
                    <tr>
                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding: 0px 9px;" valign="top">
                                        <img align="right" alt="" src="https://www.bluecard.com.ec/assets/images/logoBlue.png" width="291" class="m_389461600039680098mcnImage CToWUd a6T" tabindex="0" style="cursor: pointer; outline: none; border: 0px; height: auto; text-decoration: none; max-width: 291px; padding-bottom: 0px;">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding-top: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098mcnTextContentContainer" style="border-collapse: collapse; max-width: 100%; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <div style="text-align: right;">
                                          <br>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <div style="text-align: left;">
                                          <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: right;"><span style="color: rgb(0, 0, 0);">' . $misc['today'] . '</span>
                                            <br><span style="color: rgb(0, 0, 0);"></span>
                                            <br>&nbsp;
                                          </p>
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; background-color: rgb(255, 255, 255); background-image: none; background-repeat: no-repeat; background-position: center center; background-size: cover; border-top: 0px; border-bottom: 0px; padding-top: 9px; padding-bottom: 9px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098templateContainer" style="border-collapse: collapse; max-width: 600px !important; width: 600px !important;" width="100%">
                  <tbody>
                    <tr>
                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding-top: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098mcnTextContentContainer" style="border-collapse: collapse; max-width: 100%; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Estimado/a</strong></span>
                                          <br><span style="color: rgb(0, 0, 0);"><strong>' . $misc['name'] . '</strong></span>
                                          <br><span style="color: rgb(0, 0, 0);"><strong>&nbsp;</strong></span>
                                          <br><span style="color: rgb(0, 0, 0);">Reciba un cordial saludo a nombre de todos quienes hacemos BLUECARD ECUADOR.&nbsp;</span><span style="color: rgb(0, 0, 0);">Queremos poner&nbsp;a su consideración nuestras tarifas en base a su&nbsp;cotización&nbsp; en nuestro portal web:</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Fechas de viaje:</strong> ' . $misc['startDate'] . ' - ' . $misc['endDate'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Destino:</strong> ' . $misc['destiny'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Niños:</strong> ' . $misc['totalKids'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Adultos:</strong> ' . $misc['totalAdults'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Tercera Edad:</strong> ' . $misc['totalSeniorAllowed'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><strong>*Validez de la oferta sujeta a cambios.</strong></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><strong>*Las promociones aplican únicamente para emisión dentro del mes o día de la misma.</strong></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Elige el Plan a tu medida</strong></span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">&nbsp;</p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                                <div class="m_389461600039680098mcnTextContent" style="word-break: break-word; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">
                                  <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">
                                    <br>
                                  </p>
                                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border: 1px solid rgb(156, 189, 204);" width="100%">
                                    <thead>
                                      <tr>
                                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Producto</strong></td>
                                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Días</strong></td>
                                        <td bgcolor="#FFFFFF" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Tarifa</strong></td>
                                      </tr>
                                    </thead>
                                    <tbody bgcolor="#F2F2F2" style="background: rgb(242, 242, 242); border: 1px solid rgb(191, 191, 191);">';
                                    foreach ($misc['data'] as $rows){
                                         $message .= '<tr bgcolor="#F2F2F2" style="border: 1px solid rgb(191, 191, 191);">
                                        <td bgcolor="#F2F2F2" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['productName'] . '</td>
                                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $misc['days'] . '</td>
                                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">Usd. ' . $rows['totalPriceByProduct'] . '</td>
                                      </tr>';
                                    }
                $message .= '</tbody>
                                  </table>
                                  <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">
                                    <br>
                                  </p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding-top: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098mcnTextContentContainer" style="border-collapse: collapse; max-width: 100%; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <br>
                                        <div style="text-align: justify;"><strong>
                                            <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">
                                              <br>
                                            </p>
                                          </strong></div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding-top: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098mcnTextContentContainer" style="border-collapse: collapse; max-width: 100%; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <br>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">A su vez le recordamos que nuestros planes Platino, Viajero 360°, Turista, Ejecutivo, Estudiantil Elite y Estudiantil Elite Premium, le brindan los siguientes beneficios:</p>
                                        <ul>
                                          <li style="margin-left: 15px;">Mayor cobertura ante cualquier eventualidad</li>
                                          <li style="margin-left: 15px;">Plan dental en Ecuador previo a su viaje, incluye examen clínico y profilaxis gratis</li>
                                          <li style="margin-left: 15px;">Reembolso por robo o pérdida de pasaporte</li>
                                        </ul>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);">De antemano expresamos nuestro sincero agradecimiento por la confianza depositada en nuestra empresa, y estamos seguros que nuestra propuesta le resultará de su interés.</span>
                                          <br>&nbsp;
                                        </p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);">Saludos cordiales,</span></p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>';
                $mail->AddAddress($misc['email']);
                $mail->AddBCC($misc['emailUsr']);
                break;

            case 'mailQuotationStep':
                $subject = utf8_decode("COTIZACIÓN BC (PASO ".$misc['step'].")");
                $message = '<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" style="border-collapse: collapse; height: 1335px; margin: 0px; padding: 0px; width: 1204px; background-color: rgb(250, 250, 250); color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;" width="100%">
  <tbody>
    <tr>
      <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; height: 1335px; padding: 0px; width: 1204px; border-top: 0px;" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" width="100%">
          <tbody>
            <tr>
              <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; background-color: rgb(250, 250, 250); background-image: none; background-repeat: no-repeat; background-position: center center; background-size: cover; border-top: 0px; border-bottom: 0px; padding-top: 9px; padding-bottom: 9px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098templateContainer" style="border-collapse: collapse; max-width: 600px !important; width: 600px !important;" width="100%">
                  <tbody>
                    <tr>
                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                        <br>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; background-color: rgb(255, 255, 255); background-image: none; background-repeat: no-repeat; background-position: center center; background-size: cover; border-top: 0px; border-bottom: 0px; padding-top: 9px; padding-bottom: 0px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098templateContainer" style="border-collapse: collapse; max-width: 600px !important; width: 600px !important;" width="100%">
                  <tbody>
                    <tr>
                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding: 0px 9px;" valign="top">
                                        <img align="right" alt="" src="https://www.bluecard.com.ec/assets/images/logoBlue.png" width="291" class="m_389461600039680098mcnImage CToWUd a6T" tabindex="0" style="cursor: pointer; outline: none; border: 0px; height: auto; text-decoration: none; max-width: 291px; padding-bottom: 0px;">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding-top: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098mcnTextContentContainer" style="border-collapse: collapse; max-width: 100%; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <div style="text-align: right;">
                                          <br>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <div style="text-align: left;">
                                          <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: right;"><span style="color: rgb(0, 0, 0);">' . $misc['today'] . '</span>
                                            <br><span style="color: rgb(0, 0, 0);"></span>
                                            <br>&nbsp;
                                          </p>
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; background-color: rgb(255, 255, 255); background-image: none; background-repeat: no-repeat; background-position: center center; background-size: cover; border-top: 0px; border-bottom: 0px; padding-top: 9px; padding-bottom: 9px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098templateContainer" style="border-collapse: collapse; max-width: 600px !important; width: 600px !important;" width="100%">
                  <tbody>
                    <tr>
                      <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px;" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; min-width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; padding-top: 9px;" valign="top">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="m_389461600039680098mcnTextContentContainer" style="border-collapse: collapse; max-width: 100%; min-width: 100%;" width="100%">
                                  <tbody>
                                    <tr>
                                      <td class="m_389461600039680098mcnTextContent" style="font-family: Helvetica; margin: 0px; word-break: break-word; color: rgb(32, 32, 32); font-size: 16px; line-height: 24px; text-align: left; padding: 0px 18px 9px;" valign="top">
                                        <!--<p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Estimado/a</strong></span>-->
                                          <br><span style="color: rgb(0, 0, 0);"><strong>COTIZACI&Oacute;N BLUECARD PASO ' . $misc['step'] . '</strong></span>
                                          <br><span style="color: rgb(0, 0, 0);"><strong>&nbsp;</strong></span>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Email Cliente:</strong> ' . $misc['emailCustomer'] . ' .</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Tel&eacute;fono Cliente:</strong> ' . $misc['phoneCustomer'] . ' .</span></p>  
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Fechas de viaje:</strong> ' . $misc['startDate'] . ' - ' . $misc['endDate'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>D&iacute;as:</strong> ' . $misc['days'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Destino:</strong> ' . $misc['destiny'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Ni&ntilde;os:</strong> ' . $misc['totalKids'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Adultos:</strong> ' . $misc['totalAdults'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Tercera Edad:</strong> ' . $misc['totalSeniorAllowed'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Producto seleccionado:</strong> ' . $misc['data'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;"><span style="color: rgb(0, 0, 0);"><strong>Precio:</strong> ' . $misc['totalPrice'] . '.</span></p>
                                        <p style="margin: 10px 0px; padding: 0px; color: rgb(32, 32, 32); font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left;">&nbsp;</p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>';
                $mail->AddAddress('cotizaciones@bluecard.com.ec');
                break;    

            case 'newManager':
                $subject = "PAS --- INFORMATIVO";
                $paramBody = new Parameter($db, '20');
                $paramBody->getData();
                $table = '<table border="0" align=center width="80%" bgcolor="#FFFFFF">
                          <tr bgcolor="#284787">';
                foreach ($misc['country_table_titles'] as $ct) {
                    $table .= '<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                    ' . $ct . '
                               </td>';
                }
                $table .= '</tr>';
                foreach ($misc['countryInfo'] as $c) {
                    $table .= '<tr  style="padding:5px 5px 5px 5px; text-align:left;"" bgcolor="#e8f2fb">
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $c['countryName'] . '</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $c['percentage'] . '%</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $global_pType[$c['percentageType']] . '</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $global_weekDays[$c['paymentDeadline']] . '</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $global_invoice_number[$c['invoice']] . '</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $global_yes_no[$c['exclusive']] . '</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">' . $global_yes_no[$c['official']] . '</td>
                               </tr>';
                }
                $table .= '</table>';

                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">' . $misc['textForEmail'] . '</div><br>
                                                    <table border="1" align="center" cellspacing=0 bordercolor="#000000">
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>DATOS DEL REPRESENTANTE</strong></td>
														 </tr>
														<tr style="padding:5px 5px 5px 5px;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																 <strong>Documento:</strong></td>
															 <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['document'] . '</td>
														 </tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;  padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Nombre:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['name'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Direcci&oacute;n:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['address'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['phone'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fecha del Contrato:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['contractDate'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['contact'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono del Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['contactPhone'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>E-mail del Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['contactEmail'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tipo:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $global_typeManager[$misc['type']] . '</td>
														</tr>
													</table>
                                    <div><br/><br/></div>
                                    <div style="font-size:12px;">' . $misc['textCountryData'] . '</div><br>
                                        ' . $table . '
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>' . $misc['textInfo'] . '</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';


                $email_adresses = explode(",", str_replace(" ", "", $paramBody->getValue_par()));

                foreach ($email_adresses as $e) {
                    $mail->AddAddress($e);
                }
                break;

            case 'updateDealer':
                $subject = "PAS --- INFORMATIVO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                                <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                        <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">' . $misc['textForEmail'] . '</div><br>
                                        <center><table border="0" width="100%">
                                            <tr bgcolor="#284787">';


                foreach ($misc['titles'] as $t) {
                    $message .= '<td align="center" style="border: solid 1px white; padding:0 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                                 ' . $t . '
                                             </td>';
                }
                $message .= '   </tr>
                                            <tr  style="padding:5px 5px 5px 5px; text-align:left;" bgcolor="#e8f2fb">';
                if ($misc['data']) {
                    foreach ($misc['data'] as $d) {
                        $message .= '<td style="border: solid 1px white;  padding:5px 5px 5px 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; text-align:center;">
													 ' . $d . '
												 </td>';
                    }
                }
                $message .= '    </tr>
                                        </table></center>
                                        <div><br/><br/></div>
                                        <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>' . $misc['textInfo'] . '</strong></b></div>
                                        <div><br/><br/></div>
                                        <div style="color:#000000;">
                                            <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                        </div>
                                        <div><br/><br/></div>
                                </td></tr>
                                <tr><td>
                                   <img src="' . URL . 'img/logopas.png" border="0" />
                                </td></tr>
                            </table>';

                $mail->AddAddress($misc['managerMail']);
                $mail->AddAddress($misc['comissionistMail']);
                break;
            case 'card_reactivation':
                $subject = "PAS --- INFORMATIVO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                                <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                        <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
											El usuario ' . $misc['user_name'] . ' ha solicitado la reactivaci&oacute;n de la tarjeta No.' . $misc['card_number'] . ' perteneciente a ' . $misc['customer_name'] . '.
										</div><br>
                                        <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>' . $misc['textInfo'] . '</strong></b></div>
                                        <div>
											Para atender la solicitud por favor ingresar al <a href="' . URL . '" target="blank">PAS</a><br/><br/>
										</div>
										<div><br/><br/></div>
                                        <div style="color:#000000;">
                                            <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                        </div>
                                        <div><br/><br/></div>
                                </td></tr>
                                <tr><td>
                                   <img src="' . URL . 'img/logopas.png" border="0" />
                                </td></tr>
                            </table>';
                foreach ($misc['email'] as $e) {
                    $mail->AddAddress($e);
                }
                break;
            case 'newBranch':
                $subject = "PAS --- INFORMATIVO";
                $paramBody = new Parameter($db, '20');
                $paramBody->getData();

                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">' . $misc['textForEmail'] . '</div><br>
											<table border="1" align="center" cellspacing=0 bordercolor="#000000">
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>DATOS DE LA SUCURSAL</strong></td>
														 </tr>
														 <tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>Informaci&oacute;n General</strong></td>
														 </tr>
														 <tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>ID:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['id'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Nombre:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['name'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Sector:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['sector'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Ciudad:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['city'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Pa&iacute;s:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['country'] . '</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Representante:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['manager'] . '</td>
														</tr>';
                if ($misc['official'] == 'YES') {
                    $message .= '<tr style="padding:5px 5px 5px 5px; text-align:left;">
																			<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																				<strong>Es Oficial:</strong></td>
																			<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">SI</td>
																	</tr>';
                }
                $message .= '				<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Responsable:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['comissionist'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Categor&iacute;a:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['category'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono 1:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['phone1'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono 2:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">';
                if ($misc['phone2'] != '') {
                    $message.=$misc['phone2'];
                } else {
                    $message.='-';
                }
                $message.='</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fax:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">';
                if ($misc['fax'] != '') {
                    $message.=$misc['fax'];
                } else {
                    $message.='-';
                }
                $message.='</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fecha de contrato:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['contract_date'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico 1:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['email1'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico 2:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['email2'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['contact'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono de contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['phone_contact'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico de contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['email_contact'] . '</td>
													</tr>

													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Nombre del Gerente:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['manager_name'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono del Gerente:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['manager_phone'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Email del Gerente:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['manager_email'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fecha de Nac. del Gerente:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['manager_birthday'] . '</td>
													</tr>

													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>Informaci&oacute;n de Pagos</strong></td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto de pago:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['pay_contact'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Factura a nombre de:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['bill_to'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>D&iacute;a de pago:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $global_weekDays[$misc['payment_deadline']] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>D&iacute;a de cr&eacute;dito:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['credit_day'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Porcentaje:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['percentage'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tipo de porcentaje:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $global_pType[$misc['percentage_type']] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Incentivo a:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['bonus_to'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>Informaci&oacute;n de Asistencia</strong></td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto de asistencias:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['assistance'] . '</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico de asistencias:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">' . $misc['email_assistance'] . '</td>
													</tr>
												</table>';
                $message .= '
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>' . $misc['textInfo'] . '</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['sendToManager']);
                $mail->AddAddress($misc['sendToComissionist']);
                break;

            case 'medicalCheckup':
                $subject = "PAS --- INFORMATIVO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
										Estimado Sr.(a): ' . $misc['userName'] . '<br/><br/>
										La auditor&iacute;a del expediente #' . $misc['serial_fle'] . ' ha finalizado. Para proceder por favor ingresar al <a href="' . URL . '" target="blank">PAS</a>
									</div><br>
                                        
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . '</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                foreach ($misc['email'] as $e) {
                    $mail->AddAddress($e);
                }
                break;

            case 'medicalCheckupRequest':
                $subject = "PAS --- INFORMATIVO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
										Estimado Sr.(a): ' . $misc['userName'] . '<br/><br/>
										Se le ha asignado el expediente #' . $misc['serial_fle'] . ' para que sea auditado. Por favor ingrese a <a href="' . URL . '" target="blank">PAS</a> para visualizar el caso.<br><br>
										El n&uacute;mero de la tarjeta asociada es: ' . $misc['card_number'] . '
									</div><br>

                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . '</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail->AddAddress($misc['email']);
                break;
            case 'assistaceLiquidation':
                $subject = utf8_decode('LIQUIDACIÃ“N DE RECLAMOS');
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">' . $misc['textForEmail'] . '</div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAttachment($misc['filePath']);

                foreach ($misc['email'] as $e) {
                    $mail->AddAddress($e);
                }
                break;

            case 'closeAssistance':
                $subject = 'CIERRE DE EXPEDIENTE';
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">
									Se ha cerrado el expediente <b>' . $misc['serial_fle'] . '</b> de tipo <b>' . $misc['type_fle'] . '</b> perteneciente a la tarjeta <b>' . $misc['card_number'] . '</b> con la siguiente informaci&oacute;n:<br><br>
									<b>Cliente: </b>' . $misc['nameCus'] . '<br><br>
									<b>Fecha de ocurrencia: </b>' . $misc['incident_date_fle'] . '<br><br>
									<b>Fecha inicio de viaje de la tarjeta: </b>' . $misc['begin_date'] . '<br><br>
									<b>Fecha fin de viaje de la tarjeta: </b>' . $misc['end_date'] . ' <br><br>
									<b>Pais de venta de la tarjeta: </b>' . $misc['countryCard'] . '<br><br>
									<b>Diagn&oacute;stico: </b>' . $misc['diagnosis'] . '<br><br>

									</div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                foreach ($misc['email'] as $e) {
                    $mail->AddAddress($e);
                }
                break;

            case 'refund_application':
                $subject = "AUTORIZACION DE REEMBOLSO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">
									Su solicitud de reembolso de la tarjeta ' . $misc['card_number'] . ' perteneciente al comercializador ' . $misc['dealer_name'] . ' ha sido <strong>' . $misc['resolution'] . '</strong> <br>

									<br><i>Observaciones:</i><br>
									' . $misc['observations'] . '
									</div><br>

                                    <div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									</div>

                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail->AddAddress($misc['email']);
                break;

            case 'newUserWeb':
                $subject = "BIENVENIDO A " . $global_system_name . "!";
                $paramBody = new Parameter($misc['db'], '10');
                $paramBody->getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">	Estimado Sr(a). ' . $misc['fullname'] . '</div><br>
									<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-right:25px; widht:300px; float:left;">
									Usted ha sido registrado como usuario adminsitrativo del sitio web de ' . $global_system_name . '. Para ingresar al portal, por favor ingrese a
									<a href="' . URL . 'administration/">www.planet-assist.net</a> y reg&iacute;strese con estas credenciales:<br><br>
									<strong>Usuario:</strong> ' . $misc['username'] . '<br/>
									<strong>Contrase&ntilde;a:</strong> ' . $misc['pswd'] . '<br/><br/>
									</div><br>
                                    <div><br/><br/></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['email']);
                break;

            case 'newPromo':
                $participants = implode(', ', $misc['participants']);
                $days = explode(',', $misc['periodDays']);
                if ($misc['repeatType'] == 'WEEKLY') {
                    foreach ($days as &$d) {
                        switch ($d) {
                            case 1: $d = 'lunes';
                                break;
                            case 2: $d = 'mart&eacute;s';
                                break;
                            case 3: $d = 'mi&eacute;rcoles';
                                break;
                            case 4: $d = 'jueves';
                                break;
                            case 5: $d = 'viernes';
                                break;
                            case 6: $d = 's&aacute;bado';
                                break;
                            case 7: $d = 'domingo';
                                break;
                        }
                    }
                }
                $days = implode(', ', $days);

                $subject = "PAS --- INFORMATIVO PROMOCIONES CLIENTES";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">' . $misc['textForEmail'] . '</div>
                                    <div><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>Nombre de la Promoci&oacute;n: </b>' . $misc['name'] . '</div>
                                    <div><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>Descripci&oacute;n: </b>' . $misc['description'] . '</div>
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>PREMIOS: </b></div>
                                    <div><br/></div>';
                foreach ($misc['conditions'] as $c) {
                    foreach ($c['products'] as &$cp) {
                        $cp = $cp['name_pbl'];
                    }
                    $products = implode(', ', $c['products']);
                    if ($misc['type'] == 'AMOUNT') {
                        $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Venda un monto igual o superior a $' . $c['starting_value_pbp'] . ' y reciba gratis un: <b>' . $c['name_pri'] . ' </b></div>
                                    <div></div>';
                    } else if ($misc['type'] == 'PERCENTAGE') {
                        $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Venda un porcentaje igual o superior a ' . $c['starting_value_pbp'] . '% de las ventas y reciba gratis un: <b>' . $c['name_pri'] . ' </b></div>
                                    <div></div>';
                    } else {
                        $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Venda un monto que este entre $' . $c['starting_value_pbp'] . ' y  $' . $c['starting_value_pbp'] . ' y reciba gratis un: <b>' . $c['name_pri'] . ' </b></div>
                                    <div></div>';
                    }
                    $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Productos que aplican a la promoci&oacute;n: ' . $products . '</div>
                                    <div><br/></div>';
                }

                $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b>Condiciones de la promoci&oacute;n</b></div>
                                    <div></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">Fecha de inicio: ' . $misc['beginDate'] . '</div>
                                    <div></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">Fecha de finalizaci&oacute;n: ' . $misc['endDate'] . '</div>
                                    <div></div>';

                if ($misc['repeat'] == 1) {
                    if ($misc['repeatType'] == 'MONTHLY') {
                        $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">D&iacute;as que aplica la promoci&oacute;n: ' . $days . ' de cada mes.</div>
                                    <div></div>';
                    } else {
                        $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">D&iacute;as que aplica la promoci&oacute;n: ' . $days . ' de cada semana.</div>
                                    <div></div>';
                    }
                } else {
                    $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">D&iacute;a que aplica la promoci&oacute;n: ' . $days . '</div>
                                    <div></div>';
                }
                $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">Promoci&oacute;n v&aacute;lida solo para: ' . $participants . '</div>
                                    <div><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>' . $misc['textInfo'] . '</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['emails']);
                break;

            case 'change_standBy':
                $subject = "CAMBIO A STAND-BY";
                $message = '
                    <table align=center width="100%" bgcolor="#FFFFFF">
                            <tr>
                              <td>
                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                    Se ha cambiado el estado de la tarjeta No.- <strong>' . $misc['cardNumber'] . '</strong>, a Stand-By
                                    del cliente <strong>' . $misc['customerName'] . '</strong>.<br/><br/>
                                    Este cambio fue autorizado por <strong>' . $misc['userName'] . '</strong>.<br/><br/><br/>
                                    <strong>
                                    Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.<br/>
                                    </strong>
                                    </div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                     </table>
                            ';
                $mail->AddAddress($misc['email']);
                break;
            case 'forgotPassword':
                $subject = "OLVIDO DE CLAVE - " . $global_system_name . "!";
                $paramBody = new Parameter($db, '20');
                $paramBody->getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    Estimado Sr(A). ' . $misc['fullname'] . '<br/><br/>
                                    Ante su solicitud de cambio de clave, ahora puede registrarse con las siguientes credenciales:
                                    <div style="font-size:12px;"></div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Usuario:</strong> ' . $misc['username'] . '
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> ' . $misc['pswd'] . '
                                                    </div><br/><br/>
                                                    Le recordamos que su usuario y clave es de uso estrictamente personal y confidencial.<br/><br/>
                                                    <strong>
                                                    Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.
                                                    </strong>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['email']);
                break;
            case 'newAssistance':
                $subject = utf8_decode("PAS --- " . $misc['title']);
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                                <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                        ' . $misc['text'] . '
                                        <table border="0" width="80%">
                                            <thead>
                                                <tr bgcolor="#284787">
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Tarjeta n&uacute;mero
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Fecha de Emisi&oacute;n Tarjeta
                                                    </td>
													<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Fecha de Inicio
                                                    </td>
													<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Fecha Fin
                                                    </td>
													<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Pa&iacute;s
                                                    </td>
													<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Estado de la Tarjeta
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Fecha de Pago Factura
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Expediente n&uacute;mero
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Cliente
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Producto
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Usuario
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Comercializador
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Fecha
                                                    </td>
                                                    <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color:#FFFFFF;">
                                                        Hora
                                                    </td>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            <tr  style="padding:5px 5px 5px 5px; text-align:left;"" {if $smarty.foreach.fileList.iteration is even} bgcolor="#d7e8f9" {else} bgcolor="#e8f2fb" {/if}>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 10px 5px; text-align:center;">
                                                    ' . $misc['cardNumber'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['emission'] . '
                                                </td>
												<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['begin_date'] . '
                                                </td>
												<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['end_date'] . '
                                                </td>
												<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['country'] . '
                                                </td>
												<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $global_salesStatus[$misc['status']] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['payment'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['serialFile'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['customer'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['product'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['user'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['dealer'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['date'] . '
                                                </td>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">
                                                    ' . $misc['time'] . '
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td></tr>

                                <tr><td>
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b>' . $misc['textInfo'] . '</b></div>
                                </td></td>
                                <tr><td>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                        <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                                </td></tr>
                                <tr><td>
                                   <img src="' . URL . 'img/logopas.png" border="0" />
                                </td></tr>
                            </table>';

                foreach ($misc['email'] as $e) {
                    $mail->AddBCC($e);
                }
                break;
            case 'free_application':
                $subject = "PAS --- AUTORIZACI&Oacute;N DE VENTA FREE";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
								<tr><td>
										<div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
										Estimado Sr(A). ' . $misc['user'] . '<br/><br/>
										<div style="font-size:12px;"></div><br>
														' . $misc['textForMail'] . '<br/><br/>
														<strong>
															Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.
														</strong>
										<div><br/><br/></div>
										<div style="color:#000000;">
										<strong>SITIO WEB DE ' . $global_system_name . ' </strong>
										</div>
										<div><br/><br/></div>
								</td></tr>
								<tr><td>
								   <img src="' . URL . 'img/logopas.png" border="0" />
								</td></tr>
							</table>';
                break;
            case 'sendContractToClient':
                $subject = "CONTRATO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:13px;">A continuaci&oacute;n ponemos en su conocimiento el contrato de compra<br>Adjunto encontrar&aacute; el contrato</div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAttachment(DOCUMENT_ROOT . $misc['attachment']);
                $mail->AddAddress($misc['email']);
                break;
            case 'newTemporaryCustomer':
                $subject = "PAS --- INFORMATIVO SOLICITUD DE ASISTENCIA";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    Se ha realizado una solicitud de asistencia para la <u>tarjeta #' . $misc['cardNumber'] . '</u> con la siguiente informaci&oacute;n: <br/><br/>
                                </div>
                                    <table border="0" width="80%" align="center" cellspacing=0 bordercolor="#000000">
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Usuario:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['user'] . '
                                            </td>
                                        </tr>
										<tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Documento:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['document'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Nombre:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['name'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Fecha de nacimiento:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['birthDate'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Tel&eacute;fono:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['phone'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Tel&eacute;fono de contacto:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['contactPhone'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Direcci&oacute;n de contacto:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['contactAddress'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Pa&iacute;s de solicitud:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['country'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Nombre de la agencia:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['agency'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Pa&iacute;s del viaje:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['travelCountry'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong> Descripci&oacute;n del problema:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['problem'] . '
                                            </td>
                                        </tr>
                                        <tr style="padding:5px 5px 5px 5px;">
                                            <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:left; ">
                                                <strong>Comentarios:</strong>
                                            </td>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:left; ">
                                                ' . $misc['extra'] . '
                                            </td>
                                        </tr>
                                    </table>
                            </td></tr>
                            <tr><td>
                                <div><br/><br/></div>
                                <center>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>' . $misc['textForEmail'] . '</b></div>
                                </center>
                            </td></tr>';
                if ($misc['cardInfo']) {
                    $message .= '<tr><td>
                                <table border="0" align="center" >
                                    <tr bgcolor="#284787">
                                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                            Tarjeta
                                        </td>
                                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                            Fecha Emisi&oacute;n
                                        </td>
                                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                            Fecha Inicio
                                        </td>
                                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                            Fecha Fin
                                        </td>
                                        <td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                            Estado de la tarjeta
                                        </td>
                                    </tr>
                                    <tr  style="padding:5px 5px 5px 5px; text-align:left;"" bgcolor="#d7e8f9">
                                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;">
                                            ' . $misc['cardNumber'] . '
                                        </td>
                                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;">
                                            ' . $misc['emission'] . '
                                        </td>
                                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;">
                                            ' . $misc['begin'] . '
                                        </td>
                                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;">
                                            ' . $misc['end'] . '
                                        </td>
                                        <td style="border: solid 1px white; padding:5px 5px 5px 5px; text-align:center; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;">
                                            ' . $global_salesStatus[$misc['status']] . '
                                        </td>
                                    </tr>
                                </table>
                            </td></tr>';
                }
                $message .= '<tr><td>
                                <div><br/><br/></div>
                                <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>' . $misc['textInfo'] . '</strong></b></div>
                                <div><br/><br/></div>
                                <div style="color:#000000;">
                                <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                </div>
                                <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                foreach ($misc['email'] as $e) {
                    $mail->AddAddress($e);
                }
                break;

            case 'webSellPA':
                $subject = $misc['subject'];
                $message = $misc['message'];
                $mail->AddAddress($misc['email']);
                break;
            case 'login':
                $loginControl = new LoginControl($db, $misc['serial_lgn']);
                $loginControl->getData();
                $user = new User($db, $loginControl->getSerial_usr());
                $user->getData();
                $subject = "PAS -- Ingreso al Sistema - Usuario: " . $user->getLastname_usr() . " " . $user->getFirstname_usr();

                $message = "<table align=center width='100%' bgcolor='#FFFFFF'>
	                            <tr>
									<td>El Usuario {$user->getLastname_usr()} {$user->getFirstname_usr()}
										ha ingresado al sistema. Mayor detalle a continuaci&oacute;n:
									</td>
								</tr>
								<tr>
									<td>
									<b>Nombre de Usuario:</b> {$user->getUsername_usr()}
									</td>
								</tr>
								<tr>
									<td>
									<b>Fecha:</b> {$loginControl->getLogin_date_lgn()}
									</td>
								</tr>	
								<tr>
									<td>
									<b>Ip:</b> {$loginControl->getIp_lgn()}
									</td>
								</tr>	
				</table>";
                $parameter = new Parameter($db, '38');
                $parameter->getData();
                $sendMail = $parameter->getValue_par();
                $mail->AddAdmedicalCheckupRequestdress($sendMail);
                break;

	    case 'newRIMACSale':
				$subject = $misc['subject'];
				
				$message ='<table align=center width="100%" bgcolor="#FFFFFF">
							<tr><td>
									<img src="http://viajesegurob.rimachablaclaro.com/img/banner.gif" border="0" />
							</td></tr>

							<tr><td>
								<div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
									<div style="font-size:12px;">Estimado(a)<br /> ' . $misc['customer'] . '</div><br />
									<div style="font-size:12px;">Gracias por confiar en RIMAC Seguros.</div><br>
									<div style="font-size:12px;">Hemos recibido oportunamente tu solicitud de Seguro de Viaje, 
									por lo cual adjuntamos su p&oacute;liza y las condiciones generales de seguros en el 
									presente correo.</div><br>';
				
				foreach($misc['sale_data'] as $line){
					$message.='	<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								<strong>'.$line['label'].':</strong> ' . $line['text'] . '<br />
							</div>';
				}
				
				$message.=' <div style="font-size:12px;"><br/>
								Recuerda que:
							</div><br />
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								* Tu p&oacute;liza entra en vigencia luego de enviar a tu correo la p&oacute;liza de seguro de viaje y el condicionado general de seguros.
							</div><br>
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								* Para modificar tu solicitud o resolver alguna duda de tu seguro de vije comun&iacute;cate con nosotros al 411-1000 anexo 6031 para atender tu solicitud.
							</div><br>
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna.</b>
							</div><br /><br />
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								Nos despedimos no sin antes reiterarte nuestro compromiso de brindarte siempre el mejor servicio.<br /><br />
							</div>
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								Atentamente,<br /><br />
								<img src="http://viajesegurob.rimachablaclaro.com/img/firma.gif" border="0" /><br /><br />
							</div>
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								<i>Este mensaje se encuentra dirigido exclusivamente para uso del destinatario previsto y contiene informaci&oacute;n confidencial y/o privilegiada.<br />
								Si no eres el destinatario al que se dirigi? el mensaje, debes tomar en cuenta que la divulgaci?n, copia, distribuci&oacute;n o cualquier actividad tomada a partir del contenido del mismo se encuentran terminantemente prohibidas y es sancionada por la ley. Si por error recibiste este mensaje, por favor procede a notificar inmediatamente dicha situaci&oacute;n al remitente y eliminarlo.</i>
							</div><br /><br />
                            </td></tr>
                        </table>';

				$mail->AddAddress($misc['email']);
				$mail->AddBCC('ventasweb@rimac.com.pe');
				$mail->AddBCC('ccespedesl@rimac.com.pe');
				break;
			
			case 'newRIMACSalePAS':
				$subject = $misc['subject'];
				
				$message ='<table align=center width="100%" bgcolor="#FFFFFF">
							<tr><td>
									<img src="http://viajesegurob.rimachablaclaro.com/img/banner.gif" border="0" />
							</td></tr>

							<tr><td>
								<div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
									<div style="font-size:12px;">Estimado(a)<br /> ' . $misc['customer'] . '</div><br />
									<div style="font-size:12px;">Gracias por confiar en RIMAC Seguros.</div><br>
									<div style="font-size:12px;">Hemos recibido oportunamente tu solicitud de Seguro de Viaje, 
									por lo cual adjuntamos su p&oacute;liza y las condiciones generales de seguros en el 
									presente correo.</div><br>';
				
				foreach($misc['sale_data'] as $line){
					$message.='	<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								<strong>'.$line['label'].':</strong> ' . $line['text'] . '<br />
							</div>';
				}
				
				$message.=' <div style="font-size:12px;"><br/>
								Recuerda que:
							</div><br />
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								* Tu p&oacute;liza entra en vigencia luego de enviar a tu correo la p&oacute;liza de seguro de viaje y el condicionado general de seguros.
							</div><br>
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								* Para modificar tu solicitud o resolver alguna duda de tu seguro de vije comun&iacute;cate con nosotros al 411-1000 anexo 6031 para atender tu solicitud.
							</div><br>
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna.</b>
							</div><br /><br />
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								Nos despedimos no sin antes reiterarte nuestro compromiso de brindarte siempre el mejor servicio.<br /><br />
							</div>
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								Atentamente,<br /><br />
								<img src="http://viajesegurob.rimachablaclaro.com/img/firma.gif" border="0" /><br /><br />
							</div>
							
							<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
								<i>Este mensaje se encuentra dirigido exclusivamente para uso del destinatario previsto y contiene informaci&oacute;n confidencial y/o privilegiada.<br />
								Si no eres el destinatario al que se dirigi? el mensaje, debes tomar en cuenta que la divulgaci?n, copia, distribuci&oacute;n o cualquier actividad tomada a partir del contenido del mismo se encuentran terminantemente prohibidas y es sancionada por la ley. Si por error recibiste este mensaje, por favor procede a notificar inmediatamente dicha situaci&oacute;n al remitente y eliminarlo.</i>
							</div><br /><br />
                            </td></tr>
                        </table>';

				$mail->AddAddress($misc['email']);
				$mail->AddBCC('ventasweb@rimac.com.pe');
				break;
			
	case 'INACTIVE_USER_WARNING':
                $subject = "Notificacion de Bloqueo de Usuario Inactivo ";

                $message = "<table align=center width='100%' bgcolor='#FFFFFF'>
	                            <tr>
									<td>Estimado {$misc['manager']}:</td>
								</tr>
								<tr>
									<td>
									Se comunica que el usuario {$misc['username']}/{$misc['countername']}, no ha ingresado al sistema PAS en los ?ltimos 20 d&iacute;as.<br />
									Le recordamos que si el periodo de inactividad se extiende durante los pr&oacute;ximos 10 d&iacute;as, por seguridad el usuario ser&aacute; inhabilitado autom&aacute;ticamente.
									</td>
								</tr>
								<tr>
									<td>
									Nota: <i>Si el usuario ya no labora en su empresa, por favor hacer caso omiso al presente correo.</i>
									</td>
								</tr>	
								<tr>
									<td>
									Agradecemos su compresi?n.
									</td>
								</tr>	
				</table>";
                
                $mail->AddAddress($misc['email']);
                break;

		case 'due_assistance':
					$subject = "PAS -- NUEVA ASISTENCIA PARA TARJETA FUERA DE CR?DITO";

					$message = "<table align=center width='100%' bgcolor='#FFFFFF'>
									<tr>
										<td>Se ha creado un expediente para la tarjeta no. {$misc['card_number']}, 
											la cual no ha sido pagada y se encuentra fuera de los d&iacute;as de 
											cr&eacute;dito. Los datos de la tarjeta son:
										</td>
									</tr>
									<tr>
										<td>
										<b>Producto:</b> {$misc['product_name']}
										</td>
									</tr>
									<tr>
										<td>
										<b>Cliente:</b> {$misc['holder_name']}
										</td>
									</tr>
									<tr>
										<td>
										<b>Responsable:</b> {$misc['responsible_name']}
										</td>
									</tr>
									<tr>
										<td>
										<b>Agencia:</b> {$misc['dealer_name']}
										</td>
									</tr>	
									<tr>
										<td>
										<b>D&iacute;as Fuera de Cr&eacute;dito:</b> {$misc['days_due']}
										</td>
									</tr>	
					</table>";
										
				foreach ($misc['email'] as $e) {
                    $mail->AddAddress($e);
                }
                break;
		
	case 'bonuspreliquidation':
                $subject = 'CANJE DE INCENTIVOS';
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">
				     Ha creado la liquidaci&oacute;n de incentivos  <b>' . $misc['serial_lqn'] . '</b> por un total de <b>' . $misc['points'] . '</b> puntos <b></b> con la siguiente informaci&oacute;n:<br><br>
			             <b>Fecha de solicitud: </b>' . $misc['date'] . '<br><br>
			             </div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE ' . $global_system_name . ' </strong>
                                    </div>
                                    
                            </td></tr>
                            <tr><td>
                               <img src="' . URL . 'img/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail->AddAddress($misc['email']);
                break;
                
		case 'new_sale':
					$subject = $misc['subject'];
					global $global_system_logo;

					$message = "<table align=center width='100%' bgcolor='#FFFFFF'>
									<tr>
										<td>Estimado (a): {$misc['customer']}</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>BLUECARD le informa que su compra realizada fue procesada exitosamente.</td>
									</tr>";
					if($misc['cardNumber'] != 'N/A'){
						$message .= "<tr>
										<td>Su contrato de asistencia es el No. {$misc['cardNumber']}</td>
									</tr>";
					}
									
						$message .= "<tr>
										<td>Adjunto al presente correo podr&aacute; encontrar el respectivo contrato y condiciones generales.</td>
									</tr>
									<tr>
										<td>Gracias por preferirnos.</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>Para mayor informaci&oacute;n visite <a href='".BLUE_URL."' target='_blank'>".BLUE_URL."</a> o 
											comun&iacute;quese al 1800 - BLUECARD (258322) o a los siguientes n&uacute;meros: Quito (02)3332253, Guayaquil: (593-4) 370 3120, 
											Cuenca: (07)2850030.</td>
									</tr>	
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>Este correo ha sido enviado de forma autom&aacute;tica por su seguridad y no requiere respuesta.</td>
									</tr>	
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>Atentamente,</td>
									</tr>	
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>
										<img src='$global_system_logo' alt='BLUECARD' />
										</td>
									</tr>
                                    <tr>
                                    <td style='text-align: justify;'>
                                        En BLUECARD ECUADOR S.A. tratamos tus datos personales acorde a la Ley Org&aacute;nica de Protecci&oacute;n de Datos Personales. 
                                        Ello nos permite cumplir con nuestra prestaci&oacute;n de servicios y gestionar la relaci&oacute;n comercial contigo como cliente, 
                                        sobre todo para informarte sobre nuestros servicios y planes, en beneficio de tu salud.
                                        Como nuestro cliente puedes ejercer el derecho de acceso, rectificaci&oacute;n, actualizaci&oacute;n, supresi&oacute;n, eliminaci&oacute;n y 
                                        oposici&oacute;n al tratamiento de datos. Para m&aacute;s informaci&oacute;n cont&aacute;ctanos al correo electr&oacute;nico: protecciondedatos@bluecard.com.ec; o,
                                         revisa nuestra Pol&iacute;tica de <a href='https://bluecard.com.ec/assets/documents/politicaprotecciondedatos.pdf' target='_blank'>Protecci&oacute;n de Datos</a>.</td>
                                </tr>	
					</table>";
										
                $mail->AddAddress($misc['email']);
                
                break;

		case 'standby_warning':
				$subject = $misc['subject'];
				global $global_system_logo;

				$message = '<table align=center width="100%">
								<tr>
									<td style="text-align: center"><b>CONTRATO EN STAND-BY</b></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td style="text-align: justify">
										BLUECARD le recuerda que el contrato No. '.$misc['cardNumber'].', se encuentra en estado Stand By, el mismo deber&aacute; ser activado hasta el '.$misc['activationDate'].'. <br />
										Es importante se&ntilde;alar que de acuerdo a nuestras condiciones generales, luego de transcurridos 180 d&iacute;as contados a partir de la fecha de colocaci&oacute;n, el contrato caducar&aacute; autom&aacute;ticamente.
										Para realizar la activaci&oacute;n puede ingresar al sistema PAS, opci&oacute;n "ventas/modificaciones" o comunicarse con nuestras oficinas a nivel nacional.<br />
										<br />Este correo ha sido enviado de forma autom&aacute;tica y no requiere respuesta.
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Atentamente,<br /><b>BLUECARD ECUADOR S.A.</b></td>
								</tr>
							</table>';

			$mail->AddAddress($misc['email']);
			break;
			
            case 'paymentPtoP':
                //Debug::print_r($misc['link']);die();
                $subject = "PAGO PLACETOPAY";
                global $global_system_logo;

                $message = '<table align=center width="100%">
								<tr>
									
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td style="text-align: justify">
									Estimado Cliente,<br> <br>
									Para BLUECARD S.A es un gusto tenerlo como nuestro cliente. <br> <br>
									A continuación usted podrá realizar el pago de su Contrato de Asistencia en Viajes <br>
                                    A nombre de <strong>'.$misc['name'].' '.$misc['surname'].'</strong> <br>
                                    de forma segura a traves del siguiente link:
								<tr>
									<td style="text-align: center">
									<div class="center">
                                              <img src="http://sandbox.pas-system.net/img/place_to_pay/placetoplay.png" alt="" style="height: 70px;width: 180px;"> <br>
                                                <img src="http://sandbox.pas-system.net/img/place_to_pay/logos_tarjetasANT.png" alt="">
			                        </div>
			                        <a href="'.$misc['link'].'" class="btn" style="font-size: x-large">PAGAR AHORA</a><br/>
									(Tiempo de validez 10 minutos)
									</td>
								     </tr>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Atentamente,<br /><b>BLUE CARD ECUADOR S.A.</b></td>
								</tr>
							</table>';

                $mail->AddAddress($misc['email']);
                break;

            case 'healthStatementNotification':
                $subject = utf8_decode("NOTIFICACIÓN DE DECLRACIÓN DE SALUD");
                $message.= "
                <table width='100%' bgcolor='#FFFFFF'>
                    <tbody>
                        <tr>
                            <td><img style='float: left;' tabindex='0' src='https://ci4.googleusercontent.com/proxy/kSYSUZWcy1g1PevD2p_ZA597QcFxGY94humDU36JiqWHoXExahcwMRKYtsc5wV8g4vO77_36filgvSLBn7HjgsxwjR6aqwaDZMmMOeCTkOudEct3I_E=s0-d-e1-ft#https://www.bluecard.com.ec:443/BlueCard/mailScources/LOGO-BLUE.jpg' alt='' width='291' /></td>
                        </tr>
                        <tr>
                            <td style='padding-left: 15px;'><strong>Estimado (a): {$misc['customer_name']}</strong></td>
                        </tr>
                        <tr style='padding-left: 15px;'>
                            <td style='padding-left: 15px;'>&nbsp;</td>
                        </tr>
                        <tr style='padding-left: 15px;'>
                            <td style='padding-left: 15px;'>Adjunto al presente correo se encuentra adjunto el archivo de declaraci&oacute;n de salud para su contrato de asistencia No. {$misc['card_number']}.</td>
                        </tr>
                        <tr style='padding-left: 15px;'>
                            <td style='padding-left: 15px;'>&nbsp;</td>
                        </tr>
                        <tr style='padding-left: 15px;'>
                            <td style='padding-left: 15px;'>Este correo ha sido enviado de forma autom&aacute;tica por su seguridad y no requiere respuesta.</td>
                        </tr>
                        <tr style='padding-left: 15px;'>
                            <td style='padding-left: 15px;'>&nbsp;</td>
                        </tr>
                        <tr style='padding-left: 15px;'>
                            <td style='padding-left: 15px;'>Saludos Cordiales,</td>
                        </tr>
                    </tbody>
                </table> 
                ";

                $mail->AddAddress($misc['customer_email']);
                $mail->AddAttachment(DOCUMENT_ROOT . "modules/sales/statements/" . $misc['statement_file']);
                break;

                case 'alertValidity':
                    $subject = utf8_decode("ALERTA POLIZAS IMPAGAS");
                    $message.=' 
                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                    Estimado Comercial:<br/><br/>
                    A continuación se detalla las tarjetas que se encuentran impagas, que están próximas a iniciar vigencia.
                </div><br>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border: 1px solid rgb(156, 189, 204);" width="100%">
                    <thead>
                      <tr>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Contrato</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Factura</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Cliente</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Comercializador</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Responsable</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Fecha Emisión</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Inicio Vigencia</strong></td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);"><strong>Valor Factura</strong></td>
                      </tr>
                    </thead>
                    <tbody bgcolor="#F2F2F2" style="background: rgb(242, 242, 242); border: 1px solid rgb(191, 191, 191);">';
                    foreach ($misc as $rows){
                         $message .= '<tr bgcolor="#F2F2F2" style="border: 1px solid rgb(191, 191, 191);">
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['card_number_sal'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['number_inv'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['Cliente'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['name_dea'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['Responsable'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['emission_date_sal'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['begin_date_sal'] . '</td>
                        <td style="font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; margin: 0px; border: 1px solid rgb(191, 191, 191);">' . $rows['total_inv'] . '</td>
                      </tr>';
                    }
                    $message .= '</tbody>
                         </table> 
                         <br>
                         <br>
                         <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                         Saludos Cordiales,
                         <br>
                         <b>BLUECARD ECUADOR S.A.</b>
                     </div>
                         ';

                    $mail->AddAddress($misc[0]['email_usr']);

                    break;

        }

        try {
            
            /* BACKUP MAIL COPY FOR ALL OPERATIONS IN SYSTEM */
            global $system_bck_email;
            $mail->AddBCC('website@bluecardassistance.info');
            $mail->AddBCC($system_bck_email);
            /* BACKUP MAIL COPY FOR ALL OPERATIONS IN SYSTEM */
            ErrorLog::log($db, 'ENTRO AL TRY',$system_bck_email);
            $mail->From = FROM_MAIL;
            $mail->FromName = NAME;
            if (isset($misc['urlGeneralConditions'])) {
                $pathGeneralConditions = DOCUMENT_ROOT . 'pdfs/generalConditions/';
                foreach ($misc['urlGeneralConditions'] as $singleCondition) {
                    $realPath = $pathGeneralConditions . $singleCondition['url_glc'];
                    $pathCard = $pathGeneralConditions . 'card_bc.pdf';
                    $mail->AddAttachment($realPath, $singleCondition['url_glc']);
                    $mail->AddAttachment($pathCard, 'card_bc.pdf');
                    $mail->AddAttachment($pathGeneralConditions, $singleCondition['url_glc']);
                }
                //Anexo Condiciones Planet 402
                // 12-Nov-2019: Por jefatura de operaciones se procede a retirar condición 402.
                //$realPathPA = $pathGeneralConditions . 'general_condition_402.pdf';
                //$mail->AddAttachment($realPathPA, 'general_condition_402.pdf');
                
            }
            if (isset($misc['urlContract'])) {
                global $global_contracts_dir;
                $pathContract = DOCUMENT_ROOT . $global_contracts_dir;
                $realPath = $pathContract . $misc['urlContract'];
                $mail->AddAttachment($realPath, $misc['urlContract']);
                $fileUrl = 'https://www.pas-system.net/modules/sales/pPrintSalePDF/'. $misc['saleID'] . '/1/1' ;
                // Download a copy of the generated contract pdf file.
                $path = DOCUMENT_ROOT . 'modules/sales/contracts/contractING_' . $misc['saleID'] . '.pdf';
                $newFileName = $path;
                $file = fopen($fileUrl, 'rb');
                if ($file) {
                    $newFile = fopen($newFileName, 'wb');
                    if ($newFile) {
                        while(!feof($file)) {
                            fwrite($newFile, fread($file, 1024 * 8), 1024 * 8);
                        }
                    }
                }
                if ($file) {
                    fclose($file);
                }
                if ($newFile) {
                    fclose($newFile);
                }

                $arr = 'contractING_' . $misc['saleID'] . '.pdf';

                $mail->AddAttachment(DOCUMENT_ROOT . 'modules/sales/contracts/' . $arr);
            }

            if (isset($misc['urlYearlyContract'])) {
                global $global_yearly_contracts;
                $pathContract = DOCUMENT_ROOT . $global_yearly_contracts;
                $realPath = $pathContract . $misc['urlYearContract'];
                $mail->AddAttachment($realPath, $misc['urlYearContract']);
            }

            //Attach statement file to contract email, this process attachs the statement file of main customer and extra customer with the same card number.
            if (isset($misc['urlContract'])) {
                $sale = new Sales($db);
                $cardNumber = $misc['cardNumber'];
                //Get customer by card number.
                $customers = $sale->getSaleByCardNumber($db, $cardNumber);
                //Add into a new array only the customer document number.
                $documents = array();
                foreach ($customers as $customerSale) {
                    $documents[] = $customerSale["document_cus"];
                    //Add extras if they exist.
                    if (!empty($customerSale["documentext"])) {
                        $documents[] = $customerSale["documentext"];
                    }
                }
                //Delete duplicate customer document number and save it into a new array.
                $customerDocuments = array_values(array_unique($documents));
                $updateCard = true;
                foreach ($customerDocuments as $documentNumber) {
                    $statement = new Statement();
                    $customerStatementFile = $statement->getCustomerStatementFileByDocument($documentNumber, $cardNumber, $updateCard);
                    if ($customerStatementFile["exist"]) {
                        $mail->AddAttachment(DOCUMENT_ROOT . 'modules/sales/statements/' . $customerStatementFile['statement']['file_name']);
                    }
                }
            }

	    set_time_limit(36000);

            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->IsHTML(true);
            $mail->IsSMTP();
            $mail->Host = HOST;
            $mail->Timeout = 50;
            $mail->Port = 25;
            //$mail->SMTPAuth = true;
            //$mail->Username = USUARIO_MAIL;
            //$mail->Password = PSWD_MAIL;
            $mail -> Send();
            $result = true;
        } catch (Exception $e) {
            ErrorLog::log($db, 'ERROR send mail',$e);
            $result = 'ERROR';
        }

        return $result;
    }

    public static function generatePassword($fSize, $linSize, $type = true) {
        srand(self::create_sequence());
        $password = "";
        $max_chars = round(rand($fSize, $linSize));
        $chars = array();
        for ($i = "a"; $i < "z"; $i++)
            $chars[] = $i;
        $chars[] = "z";
        for ($i = 0; $i < $max_chars; $i++) {
            $letra = round(rand(0, 1));
            if ($type == false)
                $password .= $chars[round(rand(0, count($chars) - 1))];
            else
                $password .= round(rand(0, 9));
        }

        $password = md5($password);
        $password = substr($password, 0, 7);

        return $password;
    }

    public static function create_sequence() {
        list($usec, $sec) = explode(' ', microtime());
        return (float) $sec + ((float) $usec * 100000);
    }

    public function generateUser($misc) {
        $a = array(html_entity_decode("&aacute;"), html_entity_decode("&eacute;"), html_entity_decode("&iacute;"), html_entity_decode("&oacute;"), html_entity_decode("&uacute;"), html_entity_decode("&ntilde;"), '.');
        $b = array('a', 'e', 'i', 'o', 'u', 'n', '');
        $lastName = str_replace($a, $b, strtolower(trim($misc['lastname'])));
        $firstName = str_replace($a, $b, strtolower(trim($misc['firstname'])));
        $lastName = explode(" ", $lastName);

        //Puts together names like 'de la Torre' or 'del Pozo'
        if ($lastName[0] == "del") {
            $lastName = $lastName[0] . $lastName[1];
        } elseif ($lastName[0] == "de") {
            if ($lastName[1] == "la" || $lastName[1] == "el") {
                $lastName = $lastName[0] . $lastName[1] . $lastName[2];
            } else {
                $lastName = $lastName[0] . $lastName[1];
            }
        } else {
            $lastName = $lastName[0];
        }
        return self::generateUniqueUsername($misc['db'], substr(trim($firstName), 0, 1) . $lastName, $misc['serial']);
    }

    public function generateUniqueUsername($db, $uName, $serial_usr = NULL, $aux = NULL) {
        $sql = "SELECT DISTINCT username_usr
				FROM user
				WHERE username_usr = '$uName$aux'";

        if ($serial_usr) {
            $sql .= " AND serial_usr <> $serial_usr";
        }

        if ($db->getOne($sql)) {
            return self::generateUniqueUsername($db, $uName, $serial_usr, $aux + 1);
        } else {
            return $uName . $aux;
        }
    }

    public static function refundCommissions($misc, $serial_sal) {
        $appliedComission = new AppliedComission($misc['db']);
        $appliedComission->setSerial_sal($serial_sal);
        $appliedComission->refundSale();
    }

    public static function verifyExistCommission($db, $serial_sal, $serial_dea){
        $appliedComission = new AppliedComission($db);
        return $appliedComission->getVerifyExistCommission($db, $serial_sal,$serial_dea);
    }

    public static function calculateCommissions($misc, $serial_inv) {
        if (!isset($misc['serial_fre']) && !isset($misc['percentage_fre'])) {
            $misc['serial_fre'] = NULL;
            $misc['percentage_fre'] = NULL;
        }
        else {
            $percentage_fre = $misc['percentage_fre'];
        }

        if (!isset($misc['type_com'])) {
            $misc['type_com'] = 'IN';
        }

        $appliedComission = new AppliedComission($misc['db']);
        $appliedComissions = $appliedComission->getPercentages($serial_inv);
        
        if (is_array($appliedComissions))
        {
            foreach ($appliedComissions as $key => $comissions)
            {
                if($comissions['status_pay'] == 'PARTIAL'):
                    ErrorLog::log($misc['db'], 'PARTIAL COMISSION ITEM SKIPED', $comissions);
                    continue;
                endif;
                
                if ($comissions['status_sal'] == 'ACTIVE' || $comissions['status_sal'] == 'STANDBY' || $comissions['status_sal'] == 'EXPIRED')
                {
                    $serial_sal = $comissions['serial_sal'];
                    
                    $total_sal = $comissions['total_sal'];
                    //Check Taxes
                    if($comissions['applied_taxes_inv'] != NULL)
                    {
                        $taxes = unserialize($comissions['applied_taxes_inv']);
                        if($taxes)
                        {
                            foreach($taxes as $tax)
                            {
                                //$tx = (($comissions['total_sal'] * $tax['tax_percentage']) /100);
                                $taxesTotalSC = round((($comissions['total_sal'] * 100)/100.50),2,PHP_ROUND_HALF_UP);
                                $total_sal = $taxesTotalSC;
                            }
                        }
                        else{
                            $total_sal = $comissions['total_sal'];
                        }
                    }
                    else{
                        $total_sal = $comissions['total_sal'];
                    }

                    //if (!$appliedComission->checkSalExist($comissions['serial_sal'])) {					
                    if ($comissions['manager_rights_dea'] == 'NO')
                    { //COMISSIONS FOR REGULAR DEALERS
                        /* Branch comissions */
                        $serial_dea = $comissions['serial_dea'];
                        $percentage_dea = $comissions['percentage_dea'];
                        $type_percentage_dea = $comissions['type_percentage_dea'];
                        /* Manager comissions */
                        $serial_mbc = $comissions['serial_mbc'];
                        $percentage_mbc = $comissions['percentage_mbc'];
                        $type_percentage_mbc = $comissions['type_percentage_mbc'];
                        /* Branch Representative comissions */
                        $serial_ubd = $comissions['serial_ubd'];
                        $percentage_ubd = $comissions['percentage_ubd'];
                        
                        //******* DYNAMIC COMISSION FIX - ONLY APPLIES FOR THOSE WHO HAVE IT
			            $dynamicPercentage = DynamicCom::getComissionPercentageForUserOnInvoice($misc['db'], $serial_ubd, $serial_inv);
                        if($dynamicPercentage){
                            $percentage_ubd = $dynamicPercentage;
                        }

                        $comission_dea = round($total_sal * ($percentage_dea / 100), 2);
                        $comission_mbc = 0;
                        $comission_ubd = 0;
                        $comission_fre = 0;

                        $total_sal -= $comission_dea;

                        if ($type_percentage_mbc == 'COMISSION') {
                            $comission_mbc = round($total_sal * ($percentage_mbc / 100), 2);
                        }

                        $comission_ubd = round($total_sal * ($percentage_ubd / 100), 2);
                        $comission_fre = round($total_sal * ($percentage_fre / 100), 2);

                        $appliedComission->setSerial_sal($serial_sal);

                        $appliedComission->setSerial_dea($serial_dea);
                        $appliedComission->setValue_dea_com($comission_dea);

                        $appliedComission->setSerial_mbc($serial_mbc);
                        $appliedComission->setValue_mbc_com($comission_mbc);

                        $appliedComission->setSerial_ubd($serial_ubd);
                        $appliedComission->setValue_ubd_com($comission_ubd);

                        if (isset($misc['serial_fre']) && isset($misc['percentage_fre']))
                        {
                            $appliedComission->setSerial_fre($misc['serial_fre']);
                            $appliedComission->setValue_fre_com($comission_fre);
                        }
                        $appliedComission->setType_com($misc['type_com']);

                        //Check if a comision already exist before insert to avoid duplicated records
                        $existCommission = self::verifyExistCommission($misc['db'], $serial_sal, $serial_dea);
                        if (!$existCommission){
                            $appliedComission->insert();
                        }


                    }
                    else
                    { //COMISSIONS FOR DEALERS'S WITH MANAGER RIGHTS
                        /****************** BRANCH SECTION **********************
                         * - dea: REGULAR PAYMENT
                         * - ubd: N/A
                         * - mbc: N/A
                         * - free: N/A
                         */

                        /* Branch comissions */
                        $serial_dea = $comissions['serial_dea'];
                        $percentage_dea = $comissions['percentage_dea'];

                        /* Manager comissions */
                        $serial_mbc = $comissions['serial_mbc'];

                        /* Branch Representative comissions */
                        $serial_ubd = $comissions['serial_ubd'];

                        $comission_dea = round($total_sal * ($percentage_dea / 100), 2);
                        $comission_mbc = 0;
                        $comission_ubd = 0;
                        $comission_fre = 0;

                        $appliedComission->setSerial_sal($serial_sal);

                        $appliedComission->setSerial_dea($serial_dea);
                        $appliedComission->setValue_dea_com($comission_dea);

                        $appliedComission->setSerial_mbc($serial_mbc);
                        $appliedComission->setValue_mbc_com($comission_mbc);

                        $appliedComission->setSerial_ubd($serial_ubd);
                        $appliedComission->setValue_ubd_com($comission_ubd);

                        if (isset($misc['serial_fre']) && isset($misc['percentage_fre'])) {
                            $appliedComission->setSerial_fre($misc['serial_fre']);
                            $appliedComission->setValue_fre_com($comission_fre);
                        }
                        $appliedComission->setType_com($misc['type_com']);

                        //Check if a comision already exist before insert to avoid duplicated records
                        $existCommission = self::verifyExistCommission($misc['db'], $serial_sal, $serial_dea);
                        if (!$existCommission){
                            $appliedComission->insert();
                        }
                        /****************** BRANCH SECTION ********************* */

						
                        /****************** DEALER SECTION **********************
                         * - dea: CALCULATED WITH (% DEA - % BRA)
                         * - ubd: CALCULATED WITH DEALER BASE
                         * - mbc: CALCULATED WITH DEALER BASE
                         * - free: CALCULATED WITH DEALER BASE								
                         */

                        /* DEALER comissions */
                        $serial_dea = $comissions['parent_serial_dea'];
                        $percentage_parent_dea = $comissions['parent_percentage'];

                        if ($comissions['type_percentage_dea'] == 'COMISSION') {
							//*************** FORMULA **************
							// COM = TOTAL_SAL * (1 - %OTHER_DISC) * (%DEALER - %BRANCH)

							$comission_dea = $comissions['base_sal_amount'] * (1 - ($comissions['other_dscnt_inv'] / 100)) * (($comissions['parent_percentage'] - $comissions['percentage_dea']) / 100);

                            $other_comission_base_amount = $comissions['base_sal_amount'] - $comission_dea;
                        } else { //DISCOUNT PERCENTAGE ON BRANCH
                            $parent_ideal_comission = $comissions['base_sal_amount'] * ($percentage_parent_dea / 100);

                            //COMISSION = (TOTAL_SAL * %DEALER) - (INVOICE DISCOUNTS GIVEN)
                            $comission_dea = $parent_ideal_comission - ($comissions['base_sal_amount'] - $comissions['total_sal']);
                            $other_comission_base_amount = $comissions['base_sal_amount'] - $parent_ideal_comission;
                        }
						
                        /* Manager comissions */
                        $serial_mbc = $comissions['serial_mbc'];
                        $percentage_mbc = $comissions['percentage_mbc'];
                        $type_percentage_mbc = $comissions['type_percentage_mbc'];

                        /* Branch Representative comissions */
                        $serial_ubd = $comissions['parent_serial_ubd'];
                        $percentage_ubd = $comissions['parent_percentage_ubd'];
                        
                        //******* DYNAMIC COMISSION FIX - ONLY APPLIES FOR THOSE WHO HAVE IT
                        $dynamicPercentage = DynamicCom::getComissionPercentageForUserOnInvoice($misc['db'], $serial_ubd, $serial_inv);
                        if($dynamicPercentage){
                            $percentage_ubd = $dynamicPercentage;
                        }

                        $comission_mbc = 0;
                        $comission_ubd = 0;
                        $comission_fre = 0;

                        if ($type_percentage_mbc == 'COMISSION') {
                            $comission_mbc = round($other_comission_base_amount * ($percentage_mbc / 100), 2);
                        }

                        $comission_ubd = round($other_comission_base_amount * ($percentage_ubd / 100), 2);
                        $comission_fre = round($other_comission_base_amount * ($percentage_fre / 100), 2);

                        $appliedComission->setSerial_sal($serial_sal);

                        $appliedComission->setSerial_dea($serial_dea);
                        $appliedComission->setValue_dea_com($comission_dea);

                        $appliedComission->setSerial_mbc($serial_mbc);
                        $appliedComission->setValue_mbc_com($comission_mbc);

                        $appliedComission->setSerial_ubd($serial_ubd);
                        $appliedComission->setValue_ubd_com($comission_ubd);

                        if (isset($misc['serial_fre']) && isset($misc['percentage_fre'])) {
                            $appliedComission->setSerial_fre($misc['serial_fre']);
                            $appliedComission->setValue_fre_com($comission_fre);
                        }
                        $appliedComission->setType_com($misc['type_com']);

                        //Check if a comision already exist before insert to avoid duplicated records
                        $existCommission = self::verifyExistCommission($misc['db'], $serial_sal, $serial_dea);
                        if (!$existCommission){
                            $appliedComission->insert();
                        }

                        /*                         * **************** DEALER SECTION ********************* */
                    }
                    //}
                }
            }
        }
    }

    public static function refundBonus($misc, $serial_sal) {
        $appliedBonus = new AppliedBonus($misc['db']);
        $appliedBonus->setSerial_sal($serial_sal);
        return $appliedBonus->refundBonus();
    }

    public static function verifyExistBonus($db, $serial_sal){
        $appliedBonus = new AppliedBonus($db);
        return $appliedBonus->getVerifyExistBonus($db, $serial_sal);
    }

    public static function calculateBonus($misc, $serial_inv) {
        $invoice = new Invoice($misc['db'], $serial_inv);
        $invoice->getData();
        $due_date_inv = $invoice->getDue_date_inv();
        $date = explode('/', $due_date_inv);
        $invoice_discounts = ($invoice->getDiscount_prcg_inv() + $invoice->getOther_dscnt_inv()) / 100;
        $dealer_comission_percentage = $invoice->getComision_prcg_inv() / 100;

        $sale = new Sales($misc['db']);
        $productByDealer = new ProductByDealer($misc['db']);

        $bonus = new Bonus($misc['db']);
        $sales = $sale->getSalesByInvoice($serial_inv);
        
        $counter = new Counter($misc['db']);
        $dealer = new Dealer($misc['db']);
        $appliedBonus = new AppliedBonus($misc['db']);

        //check for grace period parameter
        $paramBody = new Parameter($misc['db'], '27');
        $paramBody->getData();
        $due_date = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
        $due_date += ( intval($paramBody->getValue_par()) * 24 * 60 * 60);

        foreach ($sales as $key => $sale) {
            
            if ($sale['status_sal'] == 'ACTIVE' || $sale['status_sal'] == 'STANDBY' || $sale['status_sal'] == 'EXPIRED') {
                
                $total_sal = $sale['total_sal'];
                //Check Taxes
                if($sale['applied_taxes_inv'] != NULL){
                    $taxes = unserialize($sale['applied_taxes_inv']);
                    if($taxes){
                        foreach($taxes as $tax){
                            $tx = (($sale['total_sal'] * $tax['tax_percentage']) /100);
                            $total_sal -= $tx;
                        }
                    }else{
                        $total_sal = $sale['total_sal'];
                    }
                }else{
                    $total_sal = $sale['total_sal'];
                }
                
                $productByDealer->setSerial_pbd($sale['serial_pbd']);
                $productByDealer->getData();
                
                $serial_pxc = $productByDealer->getSerial_pxc();
                $dea_serial_dea = $productByDealer->getSerial_dea();
                

                $counter->setSerial_cnt($sale['serial_cnt']);
                $counter->getData();

                $dealer->setSerial_dea($counter->getSerial_dea());
                $dealer->getData();
                
                $bonus_to_dea = $dealer->getBonus_to_dea();
               
                /******************** codigo para sucursales de Subrepresentante  ***********************/
                $manager_rights = Dealer::branchWithManagerRightsParent($misc['db'], $dealer->getSerial_dea());
                if ($manager_rights['manager_rights_dea'] == 'YES'){
                    $dea_serial_dea = $dealer->getSerial_dea();
                }
                
                $serial_bon = Bonus::getBonusByPxcDea($misc['db'], $serial_pxc, $dea_serial_dea, $sale['emission_date']);
                $bonus->setSerial_bon($serial_bon);
                $bonus->getData();
                $percentage_bon = $bonus->getPercentage_bon();
                                
                if ($serial_bon) {
                    /*                     * ************************* BONUS VALUE CALCULATION *************************************************
                      VALUE = [TOTAL_SAL - (TOTAL_DISCOUNTS_APPLIED ) - DEALER_COMISSION_PERCENTAGE] * BONUS_PERCENTAGE */
                    $value_apb = $total_sal - ($total_sal * $invoice_discounts);
                    $value_apb = $value_apb - ($value_apb * $dealer_comission_percentage);
                    $value_apb = $value_apb * ($percentage_bon / 100);
                    $value_apb = round($value_apb, 2);

                    $appliedBonus->setSerial_sal($sale['serial_sal']);
                    $appliedBonus->setTotal_amount_apb($value_apb);
                    $appliedBonus->setPending_amount_apb($value_apb);
                    $today = ERPConnectionFunctions::getInvoicePaymentQuickInfo($misc['db'], $invoice);
                    $today_array = explode('-', $today);
                    $today_time = mktime(0, 0, 0, $today_array[1], $today_array[2], $today_array[0]);

                    if ($misc['ondate_apb'] == 'NO') {
                        $appliedBonus->setOn_date_apb('NO');
                    } else {
                        if ($due_date < $today_time) {
                            $appliedBonus->setOn_date_apb('NO');
                        } else {
                            $appliedBonus->setOn_date_apb('YES');
                        }
                    }

                    $appliedBonus->setStatus_apb('ACTIVE');
                    $appliedBonus->setBonus_to_apb($bonus_to_dea);

                    //Check if a comision already exist before insert to avoid duplicated records
                    $existBonus = self::verifyExistBonus($misc['db'], $sale['serial_sal']);
                    if (!$existBonus){
                        $appliedBonus->insert();
                    }
                }
            }
        }
    }

    /*
     * @name: deleteFile
     * @Description: Deletes an specific file acording to its location.
     * @Params: $urlAndFile: The path of the document
     * @Creator: Mario Lopez
     */

    public static function deleteFile($urlAndFile) {
        return unlink($urlAndFile);
    }

    /*
     * @Name: generateCreditNote
     * @Description: generates a Credit note for a refund or a void invoice.
     * @Params: An array of variables with the following structure
     *          ['type']:  REFUND/INVOICE
     * 			['serial']: The serial of the refund or invoice according to it's type.
     *          ['serial_mbc']: The serial of the manager to take the current number of the Credit Note.
     * 			['serial_cou']: The coutnry of the current manager.
     * 			['person_type']: The type of person to whom the credit note will be delivered.
     * 			['amount']: The amount for the credit Note.
     *          ['db']: the Conection of the DB.
     * @Returns: TRUE if OK
     *           FALSE otherwise.
     */

    public static function generateCreditNote($misc) {
        //Get the Document_by_country serial to use.
        $serial_dbm = DocumentByManager::retrieveSelfDBMSerial($misc['db'], $misc['serial_man'], $misc['person_type'], 'CREDIT_NOTE');
		if(!$misc['cnote_number']){
			$number = DocumentByManager::retrieveNextDocumentNumber($misc['db'], $misc['serial_man'], $misc['person_type'], 'CREDIT_NOTE', FALSE);
		}else{
			$number = $misc['cnote_number'];
		}
        
        if ($number && $serial_dbm) {
            $cNote = new CreditNote($misc['db']);
            $cNote->setSerial_dbm($serial_dbm);

            if ($misc['type'] == 'REFUND') {
                $cNote->setSerial_ref($misc['serial']);
            } else if ($misc['type'] == 'INVOICE') {
                $cNote->setSerial_inv($misc['serial']);
			} else if ($misc['type'] == 'PAYMENT') {
				$cNote->setSerial_pay($misc['serial']);
            } else {
                $cNote->setSerial_sal($misc['serial']);
            }
            $cNote->setStatus_cn('ACTIVE');
            $cNote->setAmount_cn($misc['amount']);
            $cNote->setNumber_cn($number);
            $cNote->setPayment_status_cn($misc['paymentStatus']);
            if ($misc['penalty'] != '') {
                $cNote->setPenalty_fee_cn($misc['penalty']);
            }
            $cNoteID = $cNote->insert();

            if ($cNoteID) {
                if (DocumentByManager::moveToNextValue($misc['db'], $serial_dbm, $number)) {
                    $cNoteLog['error'] = 'success';
                    $cNoteLog['id'] = $cNoteID;
                    $cNoteLog['number'] = $number;
                    return $cNoteLog;
                } else {
                    $cNoteLog['error'] = 'update_error';
                    $cNoteLog['id'] = $cNoteID;
                    $cNoteLog['number'] = $number;
                    return $cNoteLog;
                }
            } else {
                $cNoteLog['error'] = 'cNote_error';
                return $cNoteLog;
            }
        } else {
            $cNoteLog['error'] = 'no_document';
            return $cNoteLog;
        }
    }

    ///////////////////////////
    //	ESTIMATOR GLOBALS	 //
    ///////////////////////////

    public static function productCalculatePrice($db, $productBasePrice, $productBaseCost, $adultsFree, // #
                                                 $adultPercentage, // int
                                                 $adultsTraveling, // # - Including the Holder
                                                 $childrenFree, // #
                                                 $childrenPercentage, // int
                                                 $childrenTraveling, // #
                                                 $spouseTraveling, //true - false
                                                 $spousePercentage, // int
                                                 $maxExtras, // #
                                                 $extrasRestricted, $serial_pxc) {//Extras restricted to CHILDREN, ADULTS, BOTH
        //FORMULAS:
        $spouseBasePrice = ($productBasePrice * (100 - $spousePercentage)) / 100;
        $spouseCost = ($productBaseCost * (100 - $spousePercentage)) / 100;

        $adultSinglePrice = ($productBasePrice * (100 - $adultPercentage)) / 100;
        $adultSingleCost = ($productBaseCost * (100 - $adultPercentage)) / 100;

        $childSinglePrice = ($productBasePrice * (100 - $childrenPercentage)) / 100;
        $childSingleCost = ($productBaseCost * (100 - $childrenPercentage)) / 100;
        //Get percentage adult senior >70
        $pxcObject = new ProductByCountry($db, $serial_pxc);
        $pxcObject -> getData();
        $pxcObject->getPercentage_senior_pxc();
        $percentageAdultSenior=$pxcObject->getPercentage_senior_pxc();
        //Debug::print_r($percentageAdultSenior);
        //CALCULATE THE AMOUNT OF CARDS AND THE CARD VACANCY --SPECIAL CONSIDERATION FOR 'EXTRAS CHILDREN ONLY' CARDS:
        $numCards = GlobalFunctions::getNumberOfCards($maxExtras, $adultsTraveling, $childrenTraveling, $extrasRestricted);
        $cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);


        //Get Cards Layout:
        $cardsDistribution = GlobalFunctions::getCardsTravelersDistribution($maxExtras, $adultsTraveling, $childrenTraveling, $spouseTraveling, $extrasRestricted, $adultsFree, $childrenFree);

        $productTotalPrice = 0;
        foreach ($cardsDistribution as $cardD) {
            $cardTotalPrice = 0;
            foreach ($cardD as $traveler) {
                $travelerPrice = 0;
                switch ($traveler) {
                    case 'H':
                        $travelerPrice = $productBasePrice;
                        break;
                    case 'C':
                        $travelerPrice = $childSinglePrice;
                        break;
                    case 'A':
                        $travelerPrice = $adultSinglePrice;
                        break;
                    case 'S':
                        $travelerPrice = $spouseBasePrice;
                        break;
                    default: //case 'F' - Free
                        $travelerPrice = 0;
                        break;
                }
                $cardTotalPrice += $travelerPrice;
            }
            $productTotalPrice += $cardTotalPrice;
        }

        $priceArray = array(
            'holderPrice' => $productBasePrice,
            'childrenPrice' => $childSinglePrice,
            'spousePrice' => $spouseBasePrice,
            'extrasPrice' => $adultSinglePrice,
            'holderCost' => $productBaseCost,
            'childrenCost' => $childSingleCost,
            'spouseCost' => $spouseCost,
            'extrasCost' => $adultSingleCost,
            'numCards' => $numCards,
            'totalPrice' => $productTotalPrice,
            'percentageSeniorAdult'=>$percentageAdultSenior
        );

        //echo 'holder price: '.$productBasePrice.' <br/>';
        //echo 'spouse price: '.$spousePrice.' <br/>';
        //echo 'adults price: '.$adultExtrasPrice.' <br/>';
        //echo 'children price: '.$childrenExtrasPrice.' <br/>';
        //echo 'holder cost: '.$productBaseCost.' <br/>';
        //echo 'spouse cost: '.$spouseCost.' <br/>';
        //echo 'adults single cost: '.$adultSingleCost.' <br/>';
        //echo 'children single cost: '.$childSingleCost.' <br/>';
        //echo 'total price: '.$productTotalPrice.' <br/>';

        return $priceArray;
    }

    public static function productCalculatePriceWS($db, $productBasePrice, $productBaseCost, $adultsFree, // #
                                                   $adultPercentage, // int
                                                   $adultsTraveling, // # - Including the Holder
                                                   $childrenFree, // #
                                                   $childrenPercentage, // int
                                                   $childrenTraveling, // #
                                                   $spouseTraveling, //true - false
                                                   $spousePercentage, // int
                                                   $maxExtras, // #
                                                   $extrasRestricted) {//Extras restricted to CHILDREN, ADULTS, BOTH
        //FORMULAS:
        $spouseBasePrice = ($productBasePrice * (100 - $spousePercentage)) / 100;
        $spouseCost = ($productBaseCost * (100 - $spousePercentage)) / 100;

        $adultSinglePrice = ($productBasePrice * (100 - $adultPercentage)) / 100;
        $adultSingleCost = ($productBaseCost * (100 - $adultPercentage)) / 100;

        $childSinglePrice = ($productBasePrice * (100 - $childrenPercentage)) / 100;
        $childSingleCost = ($productBaseCost * (100 - $childrenPercentage)) / 100;

        //CALCULATE THE AMOUNT OF CARDS AND THE CARD VACANCY --SPECIAL CONSIDERATION FOR 'EXTRAS CHILDREN ONLY' CARDS:
        $numCards = GlobalFunctions::getNumberOfCards($maxExtras, $adultsTraveling, $childrenTraveling, $extrasRestricted);
        $cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);


        //Get Cards Layout:
        $cardsDistribution = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $adultsTraveling, $childrenTraveling, $spouseTraveling, $extrasRestricted, $adultsFree, $childrenFree, $print = false);

        $productTotalPrice = 0;
        foreach ($cardsDistribution as $cardD) {
            $cardTotalPrice = 0;
            foreach ($cardD as $traveler) {
                $travelerPrice = 0;
                switch ($traveler) {
                    case 'H':
                        $travelerPrice = $productBasePrice;
                        break;
                    case 'C':
                        $travelerPrice = $childSinglePrice;
                        break;
                    case 'A':
                        $travelerPrice = $adultSinglePrice;
                        break;
                    case 'S':
                        $travelerPrice = $spouseBasePrice;
                        break;
                    default: //case 'F' - Free
                        $travelerPrice = 0;
                        break;
                }
                $cardTotalPrice += $travelerPrice;
            }
            $productTotalPrice += $cardTotalPrice;
        }

        $priceArray = array(
            'holderPrice' => $productBasePrice,
            'childrenPrice' => $childSinglePrice,
            'spousePrice' => $spouseBasePrice,
            'extrasPrice' => $adultSinglePrice,
            'holderCost' => $productBaseCost,
            'childrenCost' => $childSingleCost,
            'spouseCost' => $spouseCost,
            'extrasCost' => $adultSingleCost,
            'numCards' => $numCards,
            'totalPrice' => $productTotalPrice
        );

//        echo 'holder price: '.$productBasePrice.' <br/>';
//        echo 'spouse price: '.$spousePrice.' <br/>';
//        echo 'adults price: '.$adultExtrasPrice.' <br/>';
//        echo 'children price: '.$childrenExtrasPrice.' <br/>';
//        echo 'holder cost: '.$productBaseCost.' <br/>';
//        echo 'spouse cost: '.$spouseCost.' <br/>';
//        echo 'adults single cost: '.$adultSingleCost.' <br/>';
//        echo 'children single cost: '.$childSingleCost.' <br/>';
//        echo 'total price: '.$productTotalPrice.' <br/>';

        return $priceArray;
    }

    
    public static function assignProductCalculatedPrices($db, &$singleProduct, $selTripDays, $extrasRestricted) {
        //Default Product Split:
        $departureCountry = $singleProduct['departure'];
        $targetDays = $singleProduct['days'];
        $txtOverAge = $singleProduct['adults'];
        $txtUnderAge = $singleProduct['children'];
        $checkPartner = $singleProduct['spouseTraveling'];

        //Initialize
        $productExtraDays = 0;
        $productBasePrice = 0;
        $productBaseCost = 0;

        //GET ALL THE "PRICES per DURATION" AVAILABLE (COUNTRY SPECIFIC OR ALL COUNTRY PRICES):
        $productByCountry = new ProductByCountry($db, $singleProduct['serial_pxc']);
        $productByCountry->getData();

        $product = new Product($db, $productByCountry->getSerial_pro());
        $product->getData();

        $serialCountry = $productByCountry->getSerial_cou();
        if (!PriceByProductByCountry::pricesByProductExist($db, $productByCountry->getSerial_pxc())) {
            global $phoneSalesDealer;
            $serialCountry = Dealer::getDealersCountrySerial($db, $phoneSalesDealer);
        }

        $filterDays = NULL;
        if (!$selTripDays) {
            $filterDays = $targetDays;
        }
        $priceObject = new PriceByProductByCountry($db);
        $prices = $priceObject->getPricesByProductByCountry($productByCountry->getSerial_pro(), $serialCountry, $filterDays);

        //IF THERE IS NOT A SUITABLE RANGE OF PRICES FOR THE AMOUNT OF DAYS SELECTED
//		if (!$prices) {
//			return "NO_PRICES";
//		}
        //CHOOSE THE BEST "PRICES per DURATION" (DURATION_PPC) TO APPLY, ACCORDING TO THE DAYS OF THE TRIP (TARGET DAYS):
        $closestDuration = 0;

        if (is_array($prices)) {
            if ($selTripDays && $product->getShow_price_pro() == 'YES') {
                foreach ($prices as $p) {
                    $currentDuration = $p['duration_ppc'];
                    if ($currentDuration >= $selTripDays) {
                        $closestDuration = $currentDuration;
                        $closestDurationPrice = $p['price_ppc'];
                        $closestDurationCost = $p['cost_ppc'];
                        break;
                    }
                }
                //WE SHOULD ALWAYS GET A CORRECT DURATION SINCE TRIP DAYS VALIDATOR TOOK CARE OF IT
                $productExtraDays = 0;
                $productBasePrice = $closestDurationPrice;
                $productBaseCost = $closestDurationCost;
            } else {
                foreach ($prices as $p) {
                    $currentDuration = $p['duration_ppc'];
                    if (($currentDuration <= $targetDays && $currentDuration + 10 >= $targetDays)//IS THIS DURATION ELIGIBLE?? [EQUAL OR LOWER BUT WITHIN RANGE]
                            || ($currentDuration > $targetDays && $closestDuration == 0)) { //IS THE SUPERIOR OUR BEST SHOT?, THEN USE IT
                        $closestDuration = $currentDuration;
                        $closestDurationPrice = $p['price_ppc'];
                        $closestDurationCost = $p['cost_ppc'];
                    }
                }
                //WE SHOULD ALWAYS GET A CORRECT DURATION SINCE TRIP DAYS VALIDATOR TOOK CARE OF IT
                $productExtraDays = $targetDays - $closestDuration;
                $productBasePrice = $closestDurationPrice;
                $productBaseCost = $closestDurationCost;
            }
        } else {
            $serial_ppc = $priceObject->getMaxPricePosible($productByCountry->getSerial_pro(), $filterDays, $serialCountry);

            if ($serial_ppc) {
                $priceObject->setSerial_ppc($serial_ppc);
                $priceObject->getData();
                $priceObject = get_object_vars($priceObject);

                $closestDurationPrice = $priceObject['price_ppc'];
                $closestDurationCost = $priceObject['cost_ppc'];
                $closestDurationRangeMax = $priceObject['max_ppc'];
                $closestDurationRangeMin = $priceObject['min_ppc'];
                $closestDurationPXC = $priceObject['serial_pxc'];

                $closestDurationPrice = number_format(round($closestDurationPrice, 2), 2, '.', '');
                $fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');

                $closestDuration = $priceObject['duration_ppc'];
                $productExtraDays = $targetDays - $closestDuration;
                $productBasePrice = $closestDurationPrice;
                $productBaseCost = $closestDurationCost;
            } else {//If there's not a fee, the operator can't make the sale for tha amount of days given.
                return "NO_PRICES";
            }
        }//END OF ALGORYTHM
        //////////////////////////////////
        // 	WE NOW CALCULATE THE PRICE	//
        //////////////////////////////////
        //FIRST DECALRE SOME USEFUL VARIABLES:
        $maxExtras = $product->getMax_extras_pro();
        $singleProduct['max_extras'] = $maxExtras;
        $spouseTraveling = $checkPartner == 'true' ? true : false;

        //IF PRICE IS PER DAY, THEN MULTIPLY: -- DISABLED --
        if ($product->getPrice_by_day_pro() == 'YES') {
            //$productBasePrice *= $targetDays;
        }

        //WE GET THE PRICE PER EXTRA DAY:
        $productExtraDaysPrice = 0;
        if ($productExtraDays > 0) {
            $productExtraDaysPrice = $productByCountry->getAditional_day_pxc();
            $productBasePrice += ( $productExtraDays * $productExtraDaysPrice);
            $productBaseCost += ( $productExtraDays * $productExtraDaysPrice);
        }

        $prices = GlobalFunctions::productCalculatePrice($db, $productBasePrice, $productBaseCost, $product->getAdults_pro(), $productByCountry->getPercentage_extras_pxc(), $txtOverAge, $product->getChildren_pro(), $productByCountry->getPercentage_children_pxc(), $txtUnderAge, $spouseTraveling, $productByCountry->getPercentage_spouse_pxc(), $maxExtras, $extrasRestricted);
        //Debug:
        //echo 'final: <pre>';print_r($prices);echo '</pre>';
        //PRODUCT PRICE ASSIGNMENT:
        $singleProduct['price_pod'] = $prices['holderPrice'];
        $singleProduct['price_children_pod'] = $prices['childrenPrice'];
        $singleProduct['price_spouse_pod'] = $prices['spousePrice'];
        $singleProduct['price_extra_pod'] = $prices['extrasPrice'];

        $singleProduct['cost_pod'] = $prices['holderCost'];
        $singleProduct['cost_children_pod'] = $prices['childrenCost'];
        $singleProduct['cost_spouse_pod'] = $prices['spouseCost'];
        $singleProduct['cost_extra_pod'] = $prices['extrasCost'];

        $singleProduct['cards_pod'] = $prices['numCards'];
        $singleProduct['total_price_pod'] = $prices['totalPrice'];
        $singleProduct['days'] = $closestDuration + $productExtraDays;
        return $productTotalCost;
    }

    public static function saveCartProductToDB($db, $sessProd, $customerSerial) {
        //We first check if there is a matching pre_order for this customer:
        $orderId = PreOrder::getMyPreOrderSerial($db, $customerSerial);
        if (!$orderId) {
            $preOrder = new PreOrder($db, NULL, $customerSerial, 'CUSTOMER', time());
            $orderId = $preOrder->insert();
        }

        if ($orderId) {
            $preOrderDetail = new PreOrderDetail($db, NULL);
            $preOrderDetail->setName_pro($sessProd['name_pbl']);
            $preOrderDetail->setSerial_por($orderId);
            $preOrderDetail->setSerial_cou($sessProd['destination']);
			$preOrderDetail -> setDeparture_cou($sessProd['departure']);
            $preOrderDetail->setSerial_pxc($sessProd['serial_pxc']);
            $preOrderDetail->setBegin_date_pod($sessProd['beginDate']);
            $preOrderDetail->setEnd_date_pod($sessProd['endDate']);
            $preOrderDetail->setDays_pod($sessProd['days']);
            $preOrderDetail->setAdults_pod($sessProd['adults']);
            $preOrderDetail->setChildren_pod($sessProd['children']);
            $preOrderDetail->setSpouse_pod($sessProd['spouseTraveling'] == 'true' ? 1 : 0);

            $preOrderDetail->setPrice_pod($sessProd['price_pod']);
            $preOrderDetail->setPrice_children_pod($sessProd['price_children_pod']);
            $preOrderDetail->setPrice_spouse_pod($sessProd['price_spouse_pod']);
            $preOrderDetail->setPrice_extra_pod($sessProd['price_extra_pod']);

            $preOrderDetail->setCost_pod($sessProd['cost_pod']);
            $preOrderDetail->setCost_children_pod($sessProd['cost_children_pod']);
            $preOrderDetail->setCost_spouse_pod($sessProd['cost_spouse_pod']);
            $preOrderDetail->setCost_extra_pod($sessProd['cost_extra_pod']);

            $preOrderDetail->setTotal_Price_pod($sessProd['total_price_pod']);
            $preOrderDetail->setCards_pod($sessProd['cards_pod']);

            if (!$preOrderDetail->insert()) {
                return 3;
            }
        } else {
            return 2;
        }
        return 5;
    }

    public static function getAvailablePagesForProfile($db, $serial_pbc) {
        $sql = "(SELECT DISTINCT apo.url_apo
			   FROM allowed_pages_by_option apo
			   JOIN options opt ON opt.serial_opt=apo.serial_opt
			   JOIN opt_by_profcountry pbc on pbc.serial_opt=opt.serial_opt AND pbc.serial_pbc=" . $serial_pbc . ")
			UNION (
			   SELECT DISTINCT opt.link_opt
			   FROM options opt
			   JOIN opt_by_profcountry pbc on pbc.serial_opt=opt.serial_opt AND pbc.serial_pbc=" . $serial_pbc . ")";

        $result = $db->getAll($sql);

        if ($result) {
            $aux = array();
            foreach ($result as $r) {
                array_push($aux, $r['url_apo']);
            }
            return $aux;
        } else {
            return false;
        }
    }

    public static function getCardsTravelersDistribution($maxExtras, $adultsNum, $childrenNum, $spouseTraveling, $extrasRestricted, $adultsFree, $childrenFree) {
        //RETURNS AN ARRAY OF CARDS WITH THE FOLLOWING CODING:
        //H - HOLDER
        //C- CHILD
        //A - ADULT
        //S - SPOUSE
        //F - FREE

        $cardsContent = array();
        $numberOfCards = GlobalFunctions::getNumberOfCards($maxExtras, $adultsNum, $childrenNum, $extrasRestricted);
        $cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);

        for ($i = 0; $i < $numberOfCards; $i++) {
            //POPULATE THE CARD: PRIORITIES ARE: SPOUSE, ADULTS, CHILDREN - UNLESS RESTRICTED -
            $cardsContent[$i] = array();
            $cardPeople = 0;
            $cardPlaceIndex = 0;
            $adultsCardFree = $adultsFree;
            $childrenCardFree = $childrenFree;

            $cardsContent[$i][$cardPlaceIndex++] = 'H';
            $cardPeople++;
            if ($adultsNum > 0) {
                $adultsNum--;
            } else {
                $childrenNum--;
            }

            //IF NO EXTRAS RESTRICTION APPLIES, CONTINUE FILLING THE ADULTS:
            if ($extrasRestricted != 'CHILDREN') {

                // !! If spouse is checked but extrasRestriction applies,									!!
                // !! spouse section will not be reached even though more than one adult is traveling		!!
                // !! This is normal since in this case the only option for the spouse is to be a holder	!!
                if ($spouseTraveling && $adultsNum > 0) {//Log spouse
                    if ($adultsCardFree-- <= 0) {
                        $cardsContent[$i][$cardPlaceIndex++] = 'S';
                    } else {//Travels for free
                        $cardsContent[$i][$cardPlaceIndex++] = 'F';
                    }
                    $cardPeople++;
                    $adultsNum--;
                }

                //Fill the other adults
                while ($cardPeople < $cardVacancy && $adultsNum > 0) {
                    if ($adultsCardFree-- <= 0) {
                        $cardsContent[$i][$cardPlaceIndex++] = 'A';
                    } else {//Travels for free
                        $cardsContent[$i][$cardPlaceIndex++] = 'F';
                    }
                    $cardPeople++;
                    $adultsNum--;
                }
            }

            //Fill the children
            while ($cardPeople < $cardVacancy && $childrenNum > 0) {
                if ($childrenCardFree-- <= 0) {
                    $cardsContent[$i][$cardPlaceIndex++] = 'C';
                } else {//Travels for free
                    $cardsContent[$i][$cardPlaceIndex++] = 'F';
                }
                $cardPeople++;
                $childrenNum--;
            }
        }

        //Debug:
        //echo '<pre>';print_r($cardsContent);echo '</pre>';
        return $cardsContent;
    }

    public static function getNumberOfCards($maxExtras, $adults, $children, $extrasRestricted) {
        $anyoneAllowedInEachCard = ceil(($adults + $children) / (GlobalFunctions::getCardVacancy($maxExtras))); //Adults & Children allowed indistinctively
        $onlyOneAdultInEachCard = $adults;

        if ($extrasRestricted == 'CHILDREN') {
            return max($anyoneAllowedInEachCard, $onlyOneAdultInEachCard);
        }
        return $anyoneAllowedInEachCard;
    }

    public static function getCardVacancy($maxExtras) {
        return $maxExtras + 1; //Card vacancy is max extras plus the holder
    }

    //****************************************************************************************************************************
    //****************************************  pPrintSalePDF.php MOVED FUNCTIONS  ***********************************************
    //****************************************************************************************************************************

    public static function header1($pdf, $dataInfo, $langCode, $verticalPointer = null, $saleContract = null, $date = null) {
        global $leftMostX;
        global $blue;
        global $lightBlue;
        global $gray;
        global $red;
        global $black;
        global $smallTextSize;
        global $medTextSize;
        global $stdTextSize;
        global $largeTextSize;
		
        $pdf->setColor($gray[0], $gray[1], $gray[2]);
        $pdf->addTextWrap($leftMostX, $verticalPointer, 350, $largeTextSize, '' . $date);
		$verticalPointer -= 14;
		
        $widthBranch = $pdf->getTextWidth($stdTextSize, $saleContract['header1']['branch']);
        $widthCounter = $pdf->getTextWidth($stdTextSize, $saleContract['header1']['counter']);

        $pdf->setColor($black[0], $black[1], $black[2]);
        $pdf->addTextWrap($widthBranch + 40, $verticalPointer, 538, $stdTextSize - 2, '' . $dataInfo['name_dea']); //dealer
		$pdf->setColor($blue[0], $blue[1], $blue[2]);
        $pdf->addTextWrap($leftMostX, $verticalPointer, 350, $stdTextSize, '<b>' . $saleContract['header1']['branch'] . '</b>');
		$verticalPointer -= 15;
		
		$pdf->setColor($black[0], $black[1], $black[2]);
        $pdf->addTextWrap($widthCounter + 70, $verticalPointer, 538, $stdTextSize, '' . $dataInfo['counter']); //counter
		$pdf->setColor($blue[0], $blue[1], $blue[2]);
        $pdf->addTextWrap($leftMostX, $verticalPointer, 350, $stdTextSize, '<b>' . $saleContract['header1']['counter'] . '</b>');
        
        if($dataInfo['serial_pro']==197){
            $pdf -> setStrokeColor(0.9, 0.93, 0.96);
            $pdf -> setLineStyle(16);
            $pdf -> line(415 + 2,770,415 + 280,770);
            $pdf -> setColor($red[0],$red[1],$red[2]);
            $pdf->addTextWrap(270, 765, 250, 8, '' . $saleContract['title']['deducible1']);
            $pdf -> line(500 + 2,754,415 + 280,754);
            $pdf->addTextWrap(270, 756, 250, 8, '' . $saleContract['title']['deducible2']);
            $pdf->addTextWrap(270, 748, 250, 8, '' . $saleContract['title']['deducible3']);
        }

        $pdf -> setStrokeColor(0.9, 0.93, 0.96);
        $pdf -> setLineStyle(16);
        $pdf -> line(415 + 2,770,415 + 280,770);
        $pdf -> setColor($red[0],$red[1],$red[2]);
        $pdf->addTextWrap(420, 765, 250, 8, '' . $saleContract['title']['covid']);
        $pdf -> line(415 + 2,754,415 + 280,754);
        $pdf->addTextWrap(420, 756, 250, 8, '' . $saleContract['title']['covidC']);
		$verticalPointer -= 10;
		
        $pdf->setLineStyle(1.4, 'square');
        $pdf->setStrokeColor($gray[0], $gray[1], $gray[2]);
        $pdf->line($leftMostX - 5, $verticalPointer, $leftMostX + 528, $verticalPointer);
    }

    //Calculate age function
    public static function calculateAge($birthDate) {
        list($year, $month, $day) = explode("/", $birthDate);
        $year_diff = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff = date("d") - $day;
        if ($month_diff < 0)
            $year_diff--;
        elseif (($month_diff == 0) && ($day_diff < 0))
            $year_diff--;
        return $year_diff;
    }

    //Display Extras Function 
    public static function displayExtras($pdf, $data, $verticalPointer, $band, $dataInfo, $langCode, $saleContract = null) {
        global $global_extraRelationshipType;
        global $leftMostX;
        global $blue;
        global $lightBlue;
        global $gray;
        global $red;
        global $black;
        global $smallTextSize;
        global $medTextSize;
        global $stdTextSize;
        global $largeTextSize;
        $cont = 1;

        if ($band == 2) {
            $pdf->ezNewPage();
            GlobalFunctions::header1($pdf, $dataInfo, $langCode);
            $verticalPointer = 755;
        }

        //Extras title
        $pdf->setStrokeColor($lightBlue[0], $lightBlue[1], $lightBlue[2]);
        $pdf->setLineStyle(16);
        $pdf->line($leftMostX + 2, $verticalPointer, $leftMostX + 519, $verticalPointer);
        $pdf->setColor($red[0], $red[1], $red[2]);
        $pdf->addTextWrap($leftMostX, $verticalPointer - 4, 200, $largeTextSize, '<b>' . $saleContract['client']['extras'] . '</b>');

        //Table header:
        $verticalPointer -= 20;
        $tempX = $leftMostX;
        $pdf->setColor($gray[0], $gray[1], $gray[2]);
        $pdf->addTextWrap($tempX + 35, $verticalPointer, 547, $stdTextSize, '     ' . $saleContract['client']['table']['0'] . '                                               ' .
                $saleContract['client']['table']['1'] . '                                                                ' .
                $saleContract['client']['table']['2'] . '                    ' .
                $saleContract['client']['table']['3']);

        //Extras content
        $verticalPointer -= 15;
        if ($data) {
            $pdf->setColor($black[0], $black[1], $black[2]);
            foreach ($data as $d) {
                //Document:
                $pdf->addTextWrap($tempX, $verticalPointer, 120, $medTextSize, '' . $d['document_cus'], 'center');

                //Name:
                $fullName = $d['first_name_cus'] . ' ' . $d['last_name_cus'];
                GlobalFunctions::writeTextField($pdf, $tempX + 120, $verticalPointer + 5, $medTextSize, $fullName, 240);

                //Age
                list($day, $month, $year) = explode("/", $d['birthdate_cus']);
                $d['birthdate_cus'] = $year . '/' . $month . '/' . $day;
                $age = GlobalFunctions::calculateAge($d['birthdate_cus']);
                $pdf->addTextWrap($tempX + 360, $verticalPointer, 80, $medTextSize, $age, 'center');

                //Fee
                $pdf->addTextWrap($tempX + 440, $verticalPointer, 88, $medTextSize, number_format($d['fee_ext'], 2, '.', ''), 'center');

                //lines
                $pdf->setLineStyle(0.2, 'square');
                $pdf->setStrokeColor($gray[0], $gray[1], $gray[2]);
                $pdf->line($tempX, $verticalPointer - 2, $tempX + 528, $verticalPointer - 2);
                $pdf->line($tempX, $verticalPointer + 10, $tempX, $verticalPointer - 2);
                $pdf->line($tempX + 120, $verticalPointer + 10, $tempX + 120, $verticalPointer - 2);
                $pdf->line($tempX + 360, $verticalPointer + 10, $tempX + 360, $verticalPointer - 2);
                $pdf->line($tempX + 440, $verticalPointer + 10, $tempX + 440, $verticalPointer - 2);
                $pdf->line($tempX + 528, $verticalPointer + 10, $tempX + 528, $verticalPointer - 2);

                $verticalPointer -= 20;
                if ($verticalPointer <= 100) {
                    $pdf->ezNewPage();
                    $verticalPointer = 755;
                }
            }
        }
        $pdf->setColor($gray[0], $gray[1], $gray[2]);
        return $verticalPointer;
    }

    public static function writeTextField(&$pdf, $xValue, $verticalPointer, $textSize, $string, $maxWidth) {
        $stringWidth = $pdf->getTextWidth($textSize, $string);

        if ($stringWidth <= $maxWidth) {
            $pdf->addTextWrap($xValue, $verticalPointer - 5, $maxWidth, $textSize, '' . $string, 'center');
        } else {
            $remainder = $pdf->addTextWrap($xValue, $verticalPointer, $maxWidth, $textSize, '' . $string, 'center');
            $pdf->addTextWrap($xValue, $verticalPointer - 8, $maxWidth, $textSize, '' . $remainder);
        }
    }

   /**
     * @name refundInternationalFee
     * @param $db DB connection
     * @param $serial_sal Sales ID
     * @param $allow_refund TRUE/FALSE
     * @return boolean
	 * @version 2.0 DECISION MADE BY JFPONCE ON DEC-2 2013
     */
    public static function refundInternationalFee($db, $serial_sal) {
        $sale = new Sales($db, $serial_sal);
        $sale->getData();

        //***************** VERIFY IF THE CARD IS ALREADY IN COVERGARE TIME *************
        $begin_coverage = split('/', $sale->getBeginDate_sal());
        $begin_coverage = strtotime($begin_coverage['2'] . '/' . $begin_coverage['1'] . '/' . $begin_coverage['0']);
        $today_time = strtotime(date('Y/m/d'));

        if ($begin_coverage <= $today_time) {
            $began_coverage = TRUE;
        } else {
            $began_coverage = FALSE;
        }
        
        if ($began_coverage) { //IF THE COVERAGE BEGAN, ALWAYS CHARGE
            if ($sale->GetInternationalFeeUsed_sal() == 'YES') { //IF THE FEE WAS ALREADY BEEN PAID
				$sale->setInternationalFeeStatus_sal('TO_REFUND');
				$sale->setInternationalFeeUsed_sal('NO');
				if ($sale->update()) {
					return TRUE;
				} else {
					return FALSE;
				}
            }
			
        } else {
			//IF THE FEE WAS ALREADY BEEN PAID, MARK THE SALE AS 'TO_REFUND'
            if ($sale->getInternationalFeeUsed_sal() == 'YES') {
                $sale->setInternationalFeeStatus_sal('TO_REFUND');
                $sale->setInternationalFeeUsed_sal('NO');
                if ($sale->update()) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else { //DON'T CHARGE FEE
                $sale->setInternationalFeeStatus_sal('TO_COLLECT');
                $sale->setInternationalFeeUsed_sal('NO');
				$sale->setInternationalFeeAmount_sal('0');
                if ($sale->update()) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
    }

    public static function assignStockKits($db, $serial_dea, $serial_kbd, $action, $amount) {
        $kit = new KitByDealer($db, $serial_kbd);
        $kitDetails = new KitDetails($db);

        if ($kit->getData()) {
            $current_amount = $kit->getTotal_kbd();
            ($action == 'GIVEN') ? $current_amount+=$amount : $current_amount-=$amount;
            $kit->setTotal_kbd($current_amount);

            if ($kit->update()) {
                $kitDetails->setSerial_kbd($serial_kbd);
                $kitDetails->setAction_kdt($action);
                $kitDetails->setAmount_kdt($amount);

                if ($kitDetails->insert()) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        }
    }

    public static function specialCharFix($text) {
        return html_entity_decode(htmlentities($text, ENT_COMPAT, 'UTF-8'), ENT_COMPAT, 'iso-8859-1');
    }

    public static function ExcelRowIsEmpty($sheet, $row) {
        if ($sheet->getCell(A . $row)->getValue() != "" || //serial_cus_xls
                $sheet->getCell(B . $row)->getValue() != "" || //cus_serial_cus_xls
                $sheet->getCell(C . $row)->getValue() != "" || //document_cus
                $sheet->getCell(D . $row)->getValue() != "" || //first_name_cus
                $sheet->getCell(E . $row)->getValue() != "" || //last_name_cus
                $sheet->getCell(F . $row)->getValue() != "" || //phone1_cus
                $sheet->getCell(G . $row)->getValue() != "" || //phone2_cus
                $sheet->getCell(H . $row)->getValue() != "" || //cellphone_cus
                $sheet->getCell(I . $row)->getValue() != "" || //email_cus
                $sheet->getCell(J . $row)->getValue() != "" || //address_cus
                $sheet->getCell(K . $row)->getValue() != "" || //relative_cus
                $sheet->getCell(L . $row)->getValue() != "" || //relative_phone_cus
                $sheet->getCell(M . $row)->getValue() != "" || //birthdate_cus
                $sheet->getCell(N . $row)->getValue() != "" || //serial_cit
                $sheet->getCell(O . $row)->getValue() != "" || //start_trl
                $sheet->getCell(P . $row)->getValue() != "" || //end_trl
                $sheet->getCell(Q . $row)->getValue() != "" //relationship_ext
        ) {
            return false;
        } else {
            return true;
        }
    }

    public static function MasiveCalculateAge($date) {
        //actual date
        $day = date(j);
        $month = date(n);
        $year = date(Y);

        //birthdate
        $bDay = date("d", $date);
        $bMonth = date("m", $date);
        $bYear = date("Y", $date);


        //If bMonth is the same as month but bDay is smaller than day, we substract one year
        if (($bMonth == $month) && ($bDay > $day)) {
            $year = ($year - 1);
        }

        //if bMonth is biger than month, we substract one year
        if ($bMonth > $month) {
            $year = ($year - 1);
        }

        //we calculate the diference between year and bYear
        $age = ($year - $bYear);
        return $age;
    }

    public static function masiveSaleProcess($db, $file, $serial_pbd) {
		$special_chars = array(html_entity_decode("&aacute;"),
			html_entity_decode("&eacute;"),
			html_entity_decode("&iacute;"),
			html_entity_decode("&oacute;"),
			html_entity_decode("&uacute;"),
			html_entity_decode("&ntilde;"),
			'.',
			html_entity_decode("&Aacute;"),
			html_entity_decode("&Eacute;"),
			html_entity_decode("&Iacute;"),
			html_entity_decode("&Oacute;"),
			html_entity_decode("&Uacute;"),
			html_entity_decode("&Ntilde;"));

		$replace_with = array('é', 'í', 'ó', 'ú', 'ñ', 'á', '', '�?', 'É', '�?', 'Ó', 'Ú', 'Ñ',);

		$childAgeParam = new Parameter($db, '7');
		$childAgeParam->getData();
		$childAge = $childAgeParam->getValue_par();

		$productByDealer = new ProductByDealer($db, $serial_pbd);
		$productByDealer->getData();
		$productByCountry = new ProductByCountry($db, $productByDealer->getSerial_pxc());
		$productByCountry->getData();
		$product = new Product($db, $productByCountry->getSerial_pro());
		$product->getData();

		/* INFO FOR PRICES */
		$serial_pxc = $productByCountry->getSerial_pxc();
		$serial_pro = $product->getSerial_pro();
		$serial_cou = $productByCountry->getSerial_cou();
		$priceObject = new PriceByProductByCountry($db);
		/* INFO FOR PRICES */

		if ($file['uploadFile']['name']) {
			$original_file_name = $file['uploadFile']['name'];
			$original_temp_name = $file['uploadFile']['tmp_name'];
		} elseif ($file['file_source']['name']) {
			$original_file_name = $file['file_source']['name'];
			$original_temp_name = $file['file_source']['tmp_name'];
		} else {
			$original_file_name = $file['travelers_list']['name'];
			$original_temp_name = $file['travelers_list']['tmp_name'];
		}

		if ($original_file_name) {
			$path_info = pathinfo($original_file_name);
			$ext = $path_info['extension'];

			if ($ext != 'xls' && $ext != 'xlsx') { //verifies if it's an Excel file
				$result_array = self::masive_fixed_error_log(2);
			} else {
				$prefix = substr(md5(uniqid(rand())), 0, 6);
				$fileName = $prefix . '_' . $original_file_name;
				$path = DOCUMENT_ROOT . '/system_temp_files/' . $fileName;

				if (copy($original_temp_name, $path)) {
					require_once ("PHPExcel.php");
					require_once ("PHPExcel/Reader/Excel5.php");
					require_once ("PHPExcel/Reader/Excel2007.php");

					if ($ext == 'xls') {
						$objReader = new PHPExcel_Reader_Excel5();
					} else {
						$objReader = new PHPExcel_Reader_Excel2007();
					}

					$objReader->setReadDataOnly(true);

					ini_set('memory_limit', '512M');
					ini_set('max_execution_time', '3600');
					if (file_exists($path)) {
						$objPHPExcel = @$objReader->load($path);
					}

					/* READ ALL THE EXCEL FILE AT ONCE */

					//BEGIN AT ROW 1, BECAUSE ROW 0 IS FOR TITLES ONLY.
					$row = 1;
					$result_array['0'] = 'success';
					while (!GlobalFunctions::ExcelRowIsEmpty($objPHPExcel->getActiveSheet(), ++$row)) {
						/* VERIFY XLS SECUENCE FOR THE USERS */
						if ($objPHPExcel->getActiveSheet()->getCell(A . $row)->getValue() == ($row - 1)) {
							$data['serial_cus_xls'][$row] = GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(A . $row)->getValue());
						} else { //SECUENCE ERROR
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - SEQUENCE row ' . $row, $objPHPExcel->getActiveSheet()->getCell(A . $row)->getValue());
							$result_array = self::masive_fixed_error_log(4, $row);
							break;
						}
						/* END SECUENCE */

						/* VERIFY XLS SECUENCE FOR THE USERS EXTRAS */
						if ($objPHPExcel->getActiveSheet()->getCell(B . $row)->getValue() == "" || $objPHPExcel->getActiveSheet()->getCell(B . $row)->getValue() < ($row - 1)) {
							if ($objPHPExcel->getActiveSheet()->getCell(B . $row)->getValue() != "") {
								if ($product->getMax_extras_pro() > 0) {
									$data['cus_serial_cus_xls'][$row] = $objPHPExcel->getActiveSheet()->getCell(B . $row)->getValue();
								} else {  // extras not acepted
									ErrorLog::log($db, 'PROCESS UPLOAD ERROR - MAX EXTRAS = 0 row ' . $row, $product);
									$result_array = self::masive_fixed_error_log(5, $row);
									break;
								}
							} else {
								$data['cus_serial_cus_xls'][$row] = $objPHPExcel->getActiveSheet()->getCell(B . $row)->getValue();
							}
						} else { //SECUENCE EXTRAS ERROR
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - SEQUENCE extras row ' . $row, $data);
							$result_array = self::masive_fixed_error_log(6, $row);
							break;
						}
						/* END SECUENCE EXTRAS */

						//USER DOCUMENT
						$data['document_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(C . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(C . $row)->getValue()) : NULL;
						if (!$data['document_cus'][$row]) {
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - DOCUMENT MISSING row ' . $row, '');
							$result_array = self::masive_fixed_error_log(19, $row);
							break;
						}

						/* NAME AND LASTNAME VALIDATION */
						$first_name_cus = str_replace($special_chars, $replace_with, utf8_decode($objPHPExcel->getActiveSheet()->getCell(D . $row)->getValue()));
						if (eregi('^[a-zA-Z??????? ]+$', $first_name_cus)) {
							$data['first_name_cus'][$row] = utf8_decode($first_name_cus);
						} else {// first name error
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - FNAME row ' . $row, $first_name_cus);
							$result_array = self::masive_fixed_error_log(7, $row);
							break;
						}
						$last_name_cus = str_replace($special_chars, $replace_with, utf8_decode($objPHPExcel->getActiveSheet()->getCell(E . $row)->getValue()));
						if (eregi('^[a-zA-Z??????? ]+$', $last_name_cus)) {
							$data['last_name_cus'][$row] = utf8_decode($last_name_cus);
						} else {// last name error
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - LNAME row ' . $row, $last_name_cus);
							$result_array = self::masive_fixed_error_log(7, $row);
							break;
						}
						/* NAME AND LASTNAME VALIDATION */

						/* PHONES VALIDATION: ONLY DIGITS ACCEPTED */
						$phone1_cus = $objPHPExcel->getActiveSheet()->getCell(F . $row)->getValue();
						$data['phone1_cus'][$row] = $phone1_cus;
						$phone2_cus = $objPHPExcel->getActiveSheet()->getCell(G . $row)->getValue();
						$data['phone2_cus'][$row] = $phone2_cus;
						$cellphone_cus = $objPHPExcel->getActiveSheet()->getCell(H . $row)->getValue();
						$data['cellphone_cus'][$row] = $cellphone_cus;
						/* PHONES VALIDATION: ONLY DIGITS ACCEPTED */

						/* EMAIL ADDRESS & RELATIVE INFO */
						$email_cus = $objPHPExcel->getActiveSheet()->getCell(I . $row)->getValue();
						$data['email_cus'][$row] = $email_cus;
						$data['address_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(J . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(J . $row)->getValue()) : NULL;
						$data['relative_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(K . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(K . $row)->getValue()) : NULL;
						$data['relative_phone_cus'][$row] = $objPHPExcel->getActiveSheet()->getCell(L . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(L . $row)->getValue()) : NULL;
						/* EMAIL ADDRESS & RELATIVE INFO */

						/* AGE VALIDATION FOR SENIOR AND CHILDREN */
						$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(M . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(M . $row)->getValue()) : NULL;
						$timeStamp = mktime(0, 0, 0, 1, $excelWinDate - 1, 1900);

						if ($excelWinDate != NULL) {
							$age = GlobalFunctions::MasiveCalculateAge($timeStamp);
							$param = new Parameter($db, '1');
							$param->getData();
							$seniorAgeParam = $param->getValue_par();

							if ($product->getSenior_pro() == 'NO' && $age >= $seniorAgeParam) {//seniors not accepted
								ErrorLog::log($db, 'PROCESS UPLOAD ERROR - SENIOR UNACC row ' . $row . ' , age ' . $age, $product);
								$result_array = self::masive_fixed_error_log(9, $row);
								break;
							} else {
								if ($product->getExtras_restricted_to_pro() == "CHILDREN") {
									if ($age < $childAge) {
										$data['birthdate_cus'][$row] = date("Y-m-d", $timeStamp);
									} else {//only children accepted
										ErrorLog::log($db, 'PROCESS UPLOAD ERROR - CHILD ONLY row ' . $row . ' , age ' . $age, $childAge);
										$result_array = self::masive_fixed_error_log(10, $row);
										break;
									}
								} elseif ($product->getExtras_restricted_to_pro() == "ADULT") {
									if ($age >= $childAge) {
										$data['birthdate_cus'][$row] = date("Y-m-d", $timeStamp);
									} else {//only adults accepted
										ErrorLog::log($db, 'PROCESS UPLOAD ERROR - ADULTS ONLY row ' . $row . ' , age ' . $age, $childAge);
										$result_array = self::masive_fixed_error_log(11, $row - 1);
										break;
									}
								} else {
									$data['birthdate_cus'][$row] = date("Y-m-d", $timeStamp);
								}
							}
						} else {
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - BIRTHDATE MISSING row ' . $row, '');
							$result_array = self::masive_fixed_error_log(20, $row);
							break;
						}
						/* AGE VALIDATION FOR SENIOR AND CHILDREN */

						/* CUSTOMER'S CITY */
						$customer_cit = $objPHPExcel->getActiveSheet()->getCell(N . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(N . $row)->getValue()) : NULL;
						if ($customer_cit != '') {
							$data['serial_cit'][$row] = $customer_cit;
						} else {
							//destination unknown
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - CUST CIT UNK row ' . $row, $customer_cit);
							$result_array = self::masive_fixed_error_log(22, $row);
							break;
						}
						/* CUSTOMER'S CITY */

						/* TRAVEL DATES */
						$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(O . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(O . $row)->getValue()) : NULL;
						$timeStamp = mktime(0, 0, 0, 1, $excelWinDate - 1, 1900);
						$data['start_trl'][$row] = date("Y-m-d", $timeStamp);
						$excelWinDate = $objPHPExcel->getActiveSheet()->getCell(P . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(P . $row)->getValue()) : NULL;
						$timeStamp = mktime(0, 0, 0, 1, $excelWinDate - 1, 1900);
						$data['end_trl'][$row] = date("Y-m-d", $timeStamp);

						/* EXTRAS RELATIONSHIP */
						$data['relationship_ext'][$row] = $objPHPExcel->getActiveSheet()->getCell(Q . $row)->getValue() != "" ? GlobalFunctions::specialCharFix($objPHPExcel->getActiveSheet()->getCell(Q . $row)->getValue()) : NULL;
						/* EXTRAS RELATIONSHIP */

						/* DESTINATION */
						$destination_cit = $objPHPExcel->getActiveSheet()->getCell(R . $row)->getValue();
						if ($destination_cit != '') {
							$data['destination_cit'][$row] = $destination_cit;
						} else {
							//destination unknown
							ErrorLog::log($db, 'PROCESS UPLOAD ERROR - DEST UNK row ' . $row, $destination_cit);
							$result_array = self::masive_fixed_error_log(17, $row);
							break;
						}
						/* DESTINATION */
					}
					/* READ ALL THE EXCEL FILE AT ONCE */

					if ($result_array['0'] == 'success') {
						/* FORM THE GENERAL CUSTOMER ARRAY */
						$customer = new Customer($db);
						$array = $customer->loadData($data, NULL);
						$toShow = array();
						if (is_array($array['exist'])) {
							foreach ($array['exist'] as $key => $existCustomer) {
								$toShow[$existCustomer['serial_cus_xls']] =
										array(
											'serial_cus' => $existCustomer['serial_cus'],
											'document_usu' => $existCustomer['document_cus'],
											'first_name_cus' => htmlentities($existCustomer['first_name_cus']),
											'last_name_cus' => htmlentities($existCustomer['last_name_cus']),
											'phone1_cus' => $existCustomer['phone1_cus'],
											'email_cus' => $existCustomer['email_cus'],
											'birthdate_cus' => $existCustomer['birthdate_cus'],
											'serial_cus_xls' => $existCustomer['serial_cus_xls'],
											'cus_serial_cus_xls' => $existCustomer['cus_serial_cus_xls'],
											'start_trl' => $existCustomer['start_trl'],
											'end_trl' => $existCustomer['end_trl'],
											'relationship_ext' => $existCustomer['relationship_ext'],
											'serial_cit' => $existCustomer['serial_cit'],
											'destination_cit' => $existCustomer['destination_cit']
								);
							}
						}

						if (is_array($array['insert']['document_cus'])) {
							foreach ($array['insert']['document_cus'] as $key => $document_cus) {
								$count++;
								$customer->setDocument_cus($array['insert']['document_cus'][$key]);
								$customer->setFirstname_cus($array['insert']['first_name_cus'][$key]);
								$customer->setLastname_cus($array['insert']['last_name_cus'][$key]);
								$customer->setPhone1_cus($array['insert']['phone1_cus'][$key]);
								$customer->setPhone2_cus($array['insert']['phone2_cus'][$key]);
								$customer->setCellphone_cus($array['insert']['cellphone_cus'][$key]);
								$customer->setEmail_cus($array['insert']['email_cus'][$key]);
								$customer->setAddress_cus($array['insert']['address_cus'][$key]);
								$customer->setRelative_cus($array['insert']['relative_cus'][$key]);
								$customer->setRelative_phone_cus($array['insert']['relative_phone_cus'][$key]);
								$customer->setBirthdate_cus(date('d/m/Y', strtotime($array['insert']['birthdate_cus'][$key])));
								$customer->setSerial_cit($array['insert']['serial_cit'][$key]);
								$customer->setType_cus('PERSON');
								//Inserts customers that aren't in the DB yet.
								$serial_cus = $customer->insert();

								ERP_logActivity('new', $customer); //ERP ACTIVITY

								$toShow[$array['insert']['serial_cus_xls'][$key]] =
										array(
											'serial_cus' => $serial_cus,
											'document_usu' => $array['insert']['document_cus'][$key],
											'first_name_cus' => htmlentities($array['insert']['first_name_cus'][$key]),
											'last_name_cus' => htmlentities($array['insert']['last_name_cus'][$key]),
											'phone1_cus' => $array['insert']['phone1_cus'][$key],
											'email_cus' => $array['insert']['email_cus'][$key],
											'birthdate_cus' => $array['insert']['birthdate_cus'][$key],
											'serial_cus_xls' => $array['insert']['serial_cus_xls'][$key],
											'cus_serial_cus_xls' => $array['insert']['cus_serial_cus_xls'][$key],
											'start_trl' => $array['insert']['start_trl'][$key],
											'end_trl' => $array['insert']['end_trl'][$key],
											'relationship_ext' => $array['insert']['relationship_ext'][$key],
											'serial_cit' => $array['insert']['serial_cit'][$key],
											'destination_cit' => $array['insert']['destination_cit'][$key]
								);
							}
						}
						/* FORM THE GENERAL CUSTOMER ARRAY */

						foreach ($toShow as &$item) {
							$start_trl = strtotime($item['start_trl']);
							$end_trl = strtotime($item['end_trl']);
							$item['travel_days'] = round(abs($end_trl - $start_trl) / (60 * 60 * 24)) + 1;
						}

						/* SUCCESS CASE */
						$result_array = self::masive_fixed_error_log(1, NULL, $toShow, $path);
						/* SUCCESS CASE */
					} else {
						if (!$result_array['0'])
							$result_array = self::masive_fixed_error_log(21);
					}
				}else {//File upload error
					$result_array = self::masive_fixed_error_log(3);
				}
			}
		} else {
			$result_array = self::masive_fixed_error_log(12);
		}

		return $result_array;
	}

    public static function masive_fixed_error_log($error_code, $iteration = NULL, $extra_data = NULL, $path = NULL) {
        switch ($error_code) {
            case 1: $cause = 'success';
                //ERASE THE FILE
                unlink($path);
                break;
            case 2:
                $cause = 'wrong_extension';
                break;
            case 3:
                $cause = 'upload_error';
                break;
            case 4:
                $cause = 'customer_secuence_error';
                break;
            case 5:
                $cause = 'extras_not_allowed';
                break;
            case 6:
                $cause = 'customer_extras_secuence_error';
                break;
            case 7:
                $cause = 'wrong_name_format';
                break;
            case 8:
                $cause = 'wrong_numbers_format';
                break;
            case 9:
                $cause = 'seniors_not_allowed';
                break;
            case 10:
                $cause = 'only_children_allowed_for_extras';
                break;
            case 11:
                $cause = 'only_adults_allowed_for_extras';
                break;
            case 12:
                $cause = 'no_file_found';
                break;
            case 13:
                $cause = 'no_price_found';
                break;
            case 14:
                $cause = 'duplicated_spouse';
                break;
            case 15:
                $cause = 'extras_not_allowed';
                break;
            case 16:
                $cause = 'unknown_relationship';
                break;
            case 17:
                $cause = 'destination_unknown';
                break;
            case 18:
                $cause = 'wrong_travel_dates';
                break;
            case 19:
                $cause = 'document_missing';
                break;
            case 20:
                $cause = 'birthdate_missing';
                break;
            case 21:
                $cause = 'reading_errors';
                break;
            case 22:
                $cause = 'customer_city_unknown';
                break;
        }

        return array($cause, $iteration, $extra_data);
    }

    public static function getMainPrice($days, $serial_pxc, $serial_pro, $serial_cou, $priceObject) {
        Global $db;

        if (PriceByProductByCountry::pricesByProductExist($db, $serial_pxc)) { //If there is a price for the product in the country of analisis.
            $prices = $priceObject->getPricesByProductByCountry($serial_pro, $serial_cou, $days);
        } else { //If not, I take the prices for ALL COUNTRIES
            $prices = $priceObject->getPricesByProductByCountry($serial_pro, '1', $days);
            $serial_cou = 1;
        }

        //Debug::print_r($prices);
        if (is_array($prices)) { //Evaluation of the
            $minDay['duration_ppc'] = 16;  //Sets the min value as the gratest posible at the begining of the loop.
            $oldBetweenDays = 0;

            foreach ($prices as $p) {
                $oldBetweenDays = abs($betweenDays);
                $betweenDays = (int) ($p['duration_ppc'] - $days);
                //echo $betweenDays.'<br>';

                if ($p['duration_ppc'] == $days) {//Evaluate if we have a specific fee for the number of days given.
                    $minDay = $p;
                    $minDay['extraDays'] = 0;
                    break;
                } else {
                    if (abs($betweenDays) < $minDay['duration_ppc']) {//If we found a new posible fee.
                        if ($oldBetweenDays == 0) { //If it's only one fee available.
                            $minDay = $p;
                            if ($betweenDays < 0) {
                                $minDay['extraDays'] = abs($betweenDays);
                            } else {
                                $minDay['extraDays'] = 0;
                            }
                        } else { //If we have two posible fees in range, we take the lower one.
                            if (abs($betweenDays) < $oldBetweenDays) {
                                $minDay = $p;
                                if ($betweenDays < 0) {
                                    $minDay['extraDays'] = abs($betweenDays);
                                } else {
                                    $minDay['extraDays'] = 0;
                                }
                            }
                        }
                    } elseif (abs($betweenDays) == $minDay['duration_ppc']) { //If we have a tie between two fees, we take the lower one.
                        if ($p['duration_ppc'] < $minDay['duration_ppc']) {
                            $minDay = $p;
                        }

                        if ($betweenDays < 0) {
                            $minDay['extraDays'] = abs($betweenDays);
                        }
                    }
                }
            }
        } else { //A set of fees doesn't exist. We retrieve the next fee available.
            $serial_ppc = $priceObject->getMaxPricePosible($serial_pro, $days, $serial_cou);

            if ($serial_ppc) {
                $priceObject->setSerial_ppc($serial_ppc);
                $priceObject->getData();
                $minDay = get_object_vars($priceObject);
            } else {//If there's not a fee, the operator can't make the sale for tha amount of days given.
                $message = 'No existe una tarifa en este producto para los ' . $days . ' d&iacute;as seleccionados.';
            }
        }

        if (is_array($minDay)) {
            $pxcObject = new ProductByCountry($db, $minDay['serial_pxc']);
            $pxcObject->getData();

            $minDay['price_ppc'] = number_format(round($minDay['price_ppc'] + ($minDay['extraDays'] * $pxcObject->getAditional_day_pxc()), 2), 2, '.', '');
            $minDay['FixPrice'] = number_format(round($minDay['price_ppc'] * $change_fee, 2), 2, '.', '');
        }

        return $minDay;
    }

    /**
     * @name changeDateFormat
     * @param $date YYYY-mm-dd
     * @return string dd/mm/YYYY
     */
    public static function changeDateFormat($date) {
        $explode_date = explode('-', $date);

        return $explode_date['2'] . '/' . $explode_date['1'] . '/' . $explode_date['0'];
    }

    /**
     * @name getCurrentIp
     * @return string ip
     */
    public static function getCurrentIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function validate_ruc($document, $ruc_only = FALSE) {
        $document = str_split($document);

        if ($document[2] == 9) {
			return self::validate_ruc_9($document);
        } elseif($document[2] == 6) {
            return self::validate_ruc_6($document);
        } elseif($document[2] < 6) {
		if($ruc_only){
				if(sizeof($document) != 13){
					return false;
				}
			}

			unset($document[10]);
			unset($document[11]);
			unset($document[12]);
			
			return self::validate_ci(implode('', $document));
		} else{
			return false;
		}
    }

    public static function validate_ruc_9($document) {
        $coeficient_array = array(4, 3, 2, 7, 6, 5, 4, 3, 2);
        $module11 = 11;
        $suma = 0;

        foreach ($coeficient_array as $key => $c) {
            $suma += $c * $document[$key];
        }

        $mode_digit = $suma % $module11;
		
		if($mode_digit != 0):
			$digit_to_verify = $module11 - $mode_digit;
		else:
			$digit_to_verify = $mode_digit;
		endif;

        $digit_to_verify = ($digit_to_verify == 10) ? 0 : $digit_to_verify;

        if ($digit_to_verify == $document[9])
            return true;
        else
            return FALSE;
    }

    public static function validate_ruc_6($document) {
        $coeficientes = array(3, 2, 7, 6, 5, 4, 3, 2);

        $sumatoria = 0;
        $modulo = 0;
        $digito = 0;
        $sustraendo = 0;

        foreach ($coeficientes as $key => $c) {
            $sumatoria += $c * $document[$key];
        }

        $v9 = $document[8];

        $modulo = $sumatoria % 11;
        if($modulo == 0){
            $digito = 0;
        }else{
            $digito = 11 - $modulo;
        }
        
        if ($digito == $v9) {
            return true;
        }else
            return false;
    }

    public static function validate_ci($document) {
        $final_digit_sum = 0;
        $valid_document = true;
        $document = str_split($document);

        foreach ($document as $key => $val) {
            $temp = 0;

            if ($key == 9) { //FINAL DIGIT
                $upper_decen = ((int) ($final_digit_sum / 10) + 1) * 10;

                $temp_dig = $upper_decen - $final_digit_sum;
                if ($temp_dig == 10) {
                    $temp_dig = 0;
                }

                if ($temp_dig != $val)
                    $valid_document = false;
                break;
            }elseif (($key + 1) % 2 == 0) { //EVEN DIGIT
                $final_digit_sum += $val;
            } else { //ODD DIGIT
                $temp = $val * 2;
                if ($temp > 9):
                    $temp -= 9;
                endif;

                $final_digit_sum += $temp;
            }
        }

        return $valid_document;
    }

    public static function isMyCard($db, $serial_sal, $serial_mbc) {
        $sql = "SELECT s.serial_sal
                FROM sales s
                JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.serial_sal = $serial_sal
                JOIN dealer b ON b.serial_dea = cnt.serial_dea AND b.serial_mbc = $serial_mbc";
        //die($sql);
        if($serial_mbc <> 1){
            $result = $db->getOne($sql);

            if ($result) {
                return TRUE;
            } else {
                return FALSE;
            }
        }else{
            return TRUE;
        }
    }

   /* public static function getCardsTravelersDistribution2($maxExtras, $adultsNum, $childrenNum, $spouseTraveling, 
                            $extrasRestricted, $adultsFree, $childrenFree){
    	//RETURNS AN ARRAY OF CARDS WITH THE FOLLOWING CODING:
    	//H - HOLDER
    	//C- CHILD
    	//A - ADULT
    	//S - SPOUSE
    	//F - FREE
    	$spouseTraveling =0;
    	$cardsContent = array();
		$numberOfCards = GlobalFunctions::getNumberOfCards($maxExtras, $adultsNum, $childrenNum, $extrasRestricted);
		$cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);
		//echo $numberOfCards; echo $cardVacancy; die();
		for($i = 0; $i < $numberOfCards; $i++){
			//POPULATE THE CARD: PRIORITIES ARE: SPOUSE, ADULTS, CHILDREN - UNLESS RESTRICTED -
			$cardsContent[$i] = array();
			$cardPeople = 0;
			$cardPlaceIndex = 0;
			$adultsCardFree = $adultsFree;
			$childrenCardFree = $childrenFree;
			
			$cardsContent[$i][$cardPlaceIndex++] = ('H, 0 - 72');
			$cardPeople++;
			if($adultsNum > 0){$adultsNum --;}else{$childrenNum--;}
			
			//IF NO EXTRAS RESTRICTION APPLIES, CONTINUE FILLING THE ADULTS:
			if($extrasRestricted != 'CHILDREN'){
				
				// !! If spouse is checked but extrasRestriction applies,									!! 
				// !! spouse section will not be reached even though more than one adult is traveling		!!
				// !! This is normal since in this case the only option for the spouse is to be a holder	!!
				if($spouseTraveling && $adultsNum > 0){//Log spouse					
                                    if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'S, 0-72';
					}else{//Travels for free
						$cardsContent[$i][$cardPlaceIndex++] = 'F, 0-11';
					}
					$cardPeople++;
					$adultsNum--;
				} 
				//Fill the children
                                while($cardPeople < $cardVacancy && $childrenNum > 0){
				if($childrenCardFree-- <= 0){
					$cardsContent[$i][$cardPlaceIndex++] = 'C, 0-11';
				}else{//Travels for free
					$cardsContent[$i][$cardPlaceIndex++] = 'F, 0-11';
				}
				$cardPeople++;
				$childrenNum--;
			}
				//Fill the other adults
				while($cardPeople < $cardVacancy && $adultsNum > 0){
					if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'A, 0-72';
					}else{//Travels for free
						$cardsContent[$i][$cardPlaceIndex++] = 'F, 0-11';
					}
					$cardPeople++;
					$adultsNum--;
				}
			}
			
			
		}
		
		//Debug:
		//echo '<pre>';print_r($cardsContent);echo '</pre>';
		return $cardsContent;
    }*/
	public static function getCardsTravelersDistribution2($maxExtras, $adultsNum, $childrenNum, $spouseTraveling, 
														 $extrasRestricted, $adultsFree, $childrenFree, $print = False){
    	//RETURNS AN ARRAY OF CARDS WITH THE FOLLOWING CODING:
    	//H - HOLDER
    	//C- CHILD
    	//A - ADULT
    	//S - SPOUSE
    	//F - FREE
        // Range ages Global Vars 
       global $global_maxAgeAdults ;
       global $global_minAgeAdults ;
       global $global_maxAgeChildren;
       global $global_minAgeChildren ;
       global $global_minAgeHolder; 
       
    	$spouseTraveling = 0;
    	$cardsContent = array();
		$numberOfCards = GlobalFunctions::getNumberOfCards($maxExtras, $adultsNum, $childrenNum, $extrasRestricted);
		$cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);
		//echo $numberOfCards; echo $cardVacancy; die();
        $beneficiaries = array();
        //$beneficiarie = array();
		for($i = 0; $i < $numberOfCards; $i++){
			//POPULATE THE CARD: PRIORITIES ARE: SPOUSE, ADULTS, CHILDREN - UNLESS RESTRICTED -
			$cardsContent[$i] = array();            
            $cardsWS[$i] = array ();  
            $cardH = array();
            $cardPeople = 0;
			$cardPlaceIndex = 0;
			$adultsCardFree = $adultsFree;
			$childrenCardFree = $childrenFree;           
			$cardsContent[$i][$cardPlaceIndex++] = ('H');
            $cardsWS[$i] = ['maxAge' =>$global_maxAgeAdults,'minAge'=> $global_minAgeHolder,'letter' => 'H' ];
			$cardPeople++;	
            array_push($cardH, $cardsWS[$i]);
            $beneficiaries[$i]['beneficiaries'] = $cardH;
            
            if($adultsNum > 0){$adultsNum --;}else{$childrenNum--;}
			
			//IF NO EXTRAS RESTRICTION APPLIES, CONTINUE FILLING THE ADULTS:
			if($extrasRestricted != 'CHILDREN'){
				
				// !! If spouse is checked but extrasRestriction applies,									!! 
				// !! spouse section will not be reached even though more than one adult is traveling		!!
				// !! This is normal since in this case the only option for the spouse is to be a holder	!!
				
                if($spouseTraveling && $adultsNum > 0){//Log spouse					
                                    if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'S';
                        $cardsWS[$i] = ['maxAge' =>$global_maxAgeAdults,'minAge'=> $global_minAgeAdults,'letter' => 'S' ];
                        array_push($cardH, $cardsWS[$i]);
                        $beneficiaries[$i]['beneficiaries'] = $cardH;
					}else{//Travels for free
                		$cardsContent[$i][$cardPlaceIndex++] = 'F';
                        $cardsWS[$i] = ['maxAge' =>$global_maxAgeChildren,'minAge'=> $global_minAgeChildren,'letter' => 'F' ];
                        array_push($cardH, $cardsWS[$i]);
                        $beneficiaries[$i]['beneficiaries'] = $cardH;
					}
					$cardPeople++;
					$adultsNum--;
				} 
                
				//Fill the children
                                while($cardPeople < $cardVacancy && $childrenNum > 0){
				if($childrenCardFree-- <= 0){
					$cardsContent[$i][$cardPlaceIndex++] = 'C';
                    $cardsWS[$i] = ['maxAge' =>$global_maxAgeChildren,'minAge'=> $global_minAgeChildren,'letter' => 'C' ];
                    array_push($cardH, $cardsWS[$i]);
                    $beneficiaries[$i]['beneficiaries'] = $cardH;
				}else{//Travels for free
					$cardsContent[$i][$cardPlaceIndex++] = 'F';
                    $cardsWS[$i] = ['maxAge' =>$global_maxAgeChildren,'minAge'=> $global_minAgeChildren,'letter' => 'F' ];
                    array_push($cardH, $cardsWS[$i]);
                    $beneficiaries[$i]['beneficiaries'] = $cardH;
				}
				$cardPeople++;
				$childrenNum--;
			}
           
				//Fill the other adults
				while($cardPeople < $cardVacancy && $adultsNum > 0){
					if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'A';
                        $cardsWS[$i] = ['maxAge' =>$global_maxAgeAdults,'minAge'=> $global_minAgeAdults,'letter' => 'A' ]; 
                        array_push($cardH, $cardsWS[$i]);
                        $beneficiaries[$i]['beneficiaries'] = $cardH;					}else{//Travels for free
						$cardsWS[$i] = ['maxAge' =>$global_maxAgeChildren,'minAge'=> $global_minAgeChildren,'letter' => 'F' ];
                        array_push($cardH, $cardsWS[$i]);
                        $beneficiaries[$i]['beneficiaries'] = $cardH;
					}
					$cardPeople++;
					$adultsNum--;
                }                   
			}            
		}     
        
		//Debug:
		//echo '<pre>';print_r($beneficiaries);echo '</pre>';
        if ($print == true){
        return ($beneficiaries);                   
        }else{
        return $cardsContent;}
    }   
	
	
}

?>
