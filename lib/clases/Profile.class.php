<?php
/*
File: Profile.class.php
Author: Esteban Angulo
Creation Date: 28/12/2009 18:00
Last Modified:  
*/

class Profile{
	var $db;
	var $serial_prf;
	var $name_prf;
	var $status_prf;
	
	function __construct($db, $serial_prf=NULL, $name_prf=NULL, $status_prf=NULL )
	{
		$this -> db = $db;
		$this -> serial_prf = $serial_prf;
		$this -> name_prf = $name_prf;
		$this -> status_prf = $status_prf;
	}
	
	/***********************************************
    * funcion getData
    * Returns all Data from a selected Profile
    ***********************************************/
	function getData(){
		if($this->serial_prf!=NULL){
			
			$sql = 	"SELECT p.serial_prf,p.name_prf,p.status_prf
					FROM profile p
					WHERE serial_prf='".$this->serial_prf."'";
			$result = $this->db->Execute($sql);

			if ($result === false) 
				return false;

			if($result->fields[0]){	
				$this -> serial_prf = $result->fields[0];
				$this -> name_prf = $result-> fields[1];
				$this -> status_prf = $result ->fields[2];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	function getParentsOptions($code_lang){

		if($this->serial_prf!=NULL){
		$sql = "(SELECT op.serial_opt, name_obl, op.link_opt, op.show_condition_opt, op.opt_serial_opt, op.weight_opt
					FROM options op JOIN options_by_language obl ON obl.serial_opt=op.serial_opt 
					JOIN language l ON l.serial_lang=obl.serial_lang AND l.code_lang='".$code_lang."'  
					WHERE op.serial_opt
					IN (
						SELECT pn.opt_serial_opt
						FROM (
							  SELECT DISTINCT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
							  FROM options p JOIN options_by_language ol ON ol.serial_opt=p.serial_opt 
		  					  JOIN language l ON l.serial_lang=ol.serial_lang AND l.code_lang='".$code_lang."'  
							  WHERE p.serial_opt
							  IN (
								  SELECT pc.opt_serial_opt
								  FROM options_by_profile pp, options pc
								  WHERE pp.serial_opt = pc.serial_opt
								  AND serial_prf = ".$this->serial_prf."
								  ORDER BY pc.serial_opt
								 )
					          )pn
					     )
					)
					UNION     
					(SELECT DISTINCT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
					FROM options p JOIN options_by_language obl ON obl.serial_opt=p.serial_opt 
					JOIN language l ON l.serial_lang=obl.serial_lang AND l.code_lang='".$code_lang."'  
					WHERE p.serial_opt 
					IN (
						SELECT p.opt_serial_opt
						FROM options_by_profile pp, options p
						WHERE pp.serial_opt = p.serial_opt
						AND serial_prf =".$this->serial_prf."
						ORDER BY p.serial_opt
						)
					)
					UNION
					(SELECT p.serial_opt, name_obl, p.link_opt, p.show_condition_opt, p.opt_serial_opt, p.weight_opt
					FROM options_by_profile pp, options p, options_by_language obl, language l 
					WHERE pp.serial_opt = p.serial_opt
					AND serial_prf =".$this->serial_prf."
					AND obl.serial_opt=pp.serial_opt 
					AND l.serial_lang=obl.serial_lang 
					AND l.code_lang='".$code_lang."'  
					ORDER BY p.serial_opt)
					ORDER BY opt_serial_opt,weight_opt";


			$result = $this->db->Execute($sql);
			if ($result === false)  die("failed getParentsOptions Profile");
				$numOptions = $result -> RecordCount();
				//echo $numOptions.' '.$sql;			
				if($numOptions > 0){
					$array=array();
					$cont=1;
					
					do{
						//Se almacena la información de cada registro en un renglón del arreglo.
						$array[$cont]=$result->GetRowAssoc(false);


						$result->MoveNext();
						$cont++;
					}while($cont<=$numOptions);
				}
				//Se vuelve al modo por defecto del RecordSet
				//$this->db->SetFetchMode(ADODB_FETCH_ASSOC);

				return $array;
		}else
			return NULL;		
	}
	/***********************************************
    * funcion getActiveProfileList
    * Returns all Profiles which Status is ACTIVE
    ***********************************************/
	function getActiveProfileList(){
	$sql = 	"SELECT p.serial_prf, p.name_prf, p.status_prf
				 FROM profile p 
				 WHERE p.status_prf='ACTIVE'";
		//die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
    * funcion getProfiles
    * Returns all Profiles
    ***********************************************/
	function getProfiles(){
	$sql = 	"SELECT p.serial_prf, p.name_prf, p.status_prf
				 FROM profile p";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
    * funcion getAllStatus
    * Returns all kind of Status which exist in DB
    ***********************************************/
	function getAllStatus(){
		// Finds all possible status form ENUM column 
		$sql = "SHOW COLUMNS FROM profile LIKE 'status_prf'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];

		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
    * function insert
    * inserts a new register en DB 
    ***********************************************/
    function insert() {
	//echo $name_prf;
        $sql = "
            INSERT INTO profile (
				name_prf,
				status_prf
				)
            VALUES(
                '".$this->name_prf."',";
        $sql.=	"'".$this->status_prf."')";
		
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	/***********************************************
    * function update
    * updates a  register in DB 
    ***********************************************/
	function update(){
		$sql = "
			UPDATE `profile` SET 
				`serial_prf` = '".$this->serial_prf."',
				`name_prf` = '".$this->name_prf."',
				`status_prf` = '".$this->status_prf."'";
				
			
		$sql.="	WHERE `profile`.`serial_prf` = '".$this->serial_prf."'";	
		//echo $sql;		
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	/***********************************************
    * function delete
    * deletes a  register in DB 
    ***********************************************/
	function delete(){
		$sql = "
			DELETE FROM profile
			WHERE serial_prf = '".$this->serial_prf."'";	
		//echo $sql;		
		$result = $this->db->Execute($sql);
		if($result==true)
			return true;
		else
			return false;		
	}
	
	/***********************************************
	* funcion insertOptions_by_profile
	* inserts a new register of an option related to a profile 
	***********************************************/
	function insertOptions_by_profile($serial_prf,$serial_opt){
		$sql = "
			INSERT INTO options_by_profile (
				serial_opt,
				serial_prf)	
			VALUES (
				'".$serial_opt."',
				'".$serial_prf."')";
		//echo $sql;				
		$result = $this->db->Execute($sql);
		if ($result === false) return false;
		
		if($result==true)
			return true;
		else
			return false;
	}
	
		/***********************************************
    * funcion existsProfile
    * verifies if a freelance exists or not
    ***********************************************/
	function existsProfile($name_prf,$serial_prf){
		$sql = 	"SELECT name_prf
				 FROM profile
				 WHERE name_prf= _utf8'".utf8_encode($name_prf)."' collate utf8_bin";

		if($serial_prf != NULL && $serial_prf != ''){
			$sql.= " AND serial_prf <> ".$serial_prf;
		}
		$result = $this->db -> Execute($sql);
		
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	
	/***********************************************
	* funcion OptionsbyProfile
	* Recovers form DB all options for a profile
	***********************************************/
	function getOptionsbyProfile($serial_prf){
		$sql = 	"SELECT p.serial_opt
				FROM options_by_profile p
				WHERE p.serial_prf=$serial_prf";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
	* funcion verifyName
	* inserts a register from an option related to the profile 
	***********************************************/
	function verifyName(){
		if($this->nombre_prf != NULL){
			$sql = 	"SELECT p.name_prf
					FROM profile p
					WHERE p.name_prf = '".$this->name_prf."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields['name_prf']){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/***********************************************
	* funcion deleteOptions_by_Profile
	* Deletes an Option for a selected Profile
	***********************************************/
	function deleteOptions_by_Profile(){
		$sql = "
			DELETE FROM options_by_profile
			WHERE serial_prf='".$this->serial_prf."'";
		//echo $sql;				
		$result = $this->db->Execute($sql);
		if ($result === false)return false;
		
		if($result==true)
			return true;
		else
			return false;
	}
	
	
	/***********************************************
	* funcion verifyNameUpdate
	* checks if a name already exists in DB
	***********************************************/
	function verifyNameUpdate(){
		if($this->nombre_prf != NULL){
			$sql = 	"SELECT p.name_prf
					FROM profile p
					WHERE p.name_prf = '".$this->name_prf."'
					AND p.serial_prf <> ".$this->serial_prf;
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields['name_prf']){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	
	//getters
	function getSerial_prf(){
		return $this->serial_prf;
	}
	function getName_prf(){
		return $this->name_prf;
	}
	function getStatus_prf(){
		return $this->status_prf;
	}
	
	//setters
	function setSerial_prf($serial_prf){
		 $this->serial_prf = $serial_prf;
	}
	function setName_prf($name_prf){
		 $this->name_prf = $name_prf;
	}
	function setStatus_prf($status_prf){
		 $this->status_prf = $status_prf;
	}

}

?>
