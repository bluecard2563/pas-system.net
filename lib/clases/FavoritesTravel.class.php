<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of favorite_travel
 *
 * @author Valeria Andino C�lleri
 */
class FavoritesTravel {
    var $serial_fav;
    var $serial_main_cus;
    var $serial_cus_fav;
    var $status_fav;
    var $relative_fav;

    function __construct($db, $serial_fav = NULL, $serial_main_cus = NULL, $serial_cus_fav = NULL, $status_fav = NULL, $relative_fav=NULL){
		$this -> db = $db;
                $this -> serial_fav = $serial_fav;
                $this -> serial_main_cus = $serial_main_cus;
                $this -> serial_cus_fav = $serial_cus_fav;
                $this -> status_fav = $status_fav;
                $this -> relative_fav = $relative_fav;
    }
      /***********************************************
    * function getData
    @Name: getData
    @Description: Returns all data
    @Params:
     *       N/A
    @Returns: true
     *        false
    ***********************************************/

    function getData($useEntityCityValue = false){     
        if($this->serial_fav!=NULL){
			$sql = 	"SELECT serial_fav, serial_main_cus, serial_cus_fav, status_fav, relative_fav
					FROM favorites_travel
					WHERE serial_fav='".$this->serial_fav."'";
			//echo $sql." ";		
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_fav=$result->fields[0];
				$this->serial_main_cus=$result->fields[1];	
                $this->serial_cus_fav=$result->fields[2];
                $this->status_fav=$result->fields[3];
                $this->relative_fav=$result->fields[4];
				return true;
			}	
			else
				return false;

		}else
			return false;	
    }

    
    /*******************************************
    @Name: insert
    @Description: Returns an array of users.
    @Params: N/A
    @Returns: An Array with all types of relationships
    ********************************************/
	function insert() {
        $sql = "
            INSERT INTO favorites_travel (
				serial_fav,
				serial_main_cus,
				serial_cus_fav, 
                status_fav, 
                relative_fav)
            VALUES(
				NULL,
					'$this->serial_main_cus',
					'$this->serial_cus_fav',
                    '$this->status_fav',
                    '$this->relative_fav')";
        
     $result = $this->db->Execute($sql);
        
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	    /*******************************************
    @Name: delete
    @Description: Erases all extras from a sale
    @Params: N/A
    @Returns: N/A
    ********************************************/
	function delete($serial_sal) {
        $sql = "
				DELETE FROM favorite_travel
				WHERE serial_fav='".$serial_fav."'";
//echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        if($result==true)
            return $this->db->insert_ID();
        else
            return false;
    }
    
    /*
	 * @name: getFavoritesByCustomer
	 * @param:
	 */
	public static function getFavoritesByCustomer($db, $serialCus){
		$sql="SELECT cus.serial_cus,cus.document_cus, cus.first_name_cus, 
                        cus.last_name_cus, cus.birthdate_cus, 
                        cus.address_cus, cus.phone1_cus, 
                        cus.phone2_cus, cus.cellphone_cus, 
                        cus.email_cus, cus.relative_cus, 
                        cus.relative_phone_cus, cus.serial_cit, ft.relative_fav,
                        cit.name_cit, cou.name_cou, cou.serial_cou FROM customer cus 
                        JOIN city cit oN cus.serial_cit = cit.serial_cit
                        JOIN country cou oN cit.serial_cou= cou.serial_cou
                        JOIN favorites_travel ft ON ft.serial_cus_fav = cus.serial_cus
                        WHERE cus.serial_cus IN (
            (SELECT ft.serial_cus_fav                      
			  FROM favorites_travel ft
			  JOIN customer cus ON ft.serial_main_cus = cus.serial_cus AND ft.serial_main_cus = '$serialCus' AND ft.status_fav = 'ACTIVE'))";
//echo $sql; die();
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
    
     public static function getSerialCustomerFavoriteByHolder($db, $serial_H, $serial_F){
		$sql = "SELECT serial_fav FROM favorites_travel WHERE serial_main_cus = '$serial_H' AND serial_cus_fav = '$serial_F'";
        //echo $sql; die();
                $result = $db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
   
    
    public static function inactiveFavorite($db,  $serial_Fav){
		$sql = "UPDATE favorites_travel SET status_fav = 'INACTIVE' WHERE serial_fav = '$serial_Fav'";
        //echo $sql; die();
               $result = $db -> Execute($sql);

		if($result){
			return true;
		}else{
			return false;
		}		
	}
    
    ///GETTERS
	function getSerial_fav(){
		return $this->serial_fav;
	}
	function getSerial_main_cus(){
		return $this->serial_main_cus;
	}
    function getSerial_cus_fav(){
            return $this->serial_cus_fav;
        }
    function getStatus_fav(){
            return $this->status_fav;
        }
        function getRelative_fav(){
            return $this->relative_fav;
        }


	///SETTERS	
	function setSerial_fav(){
		return $this->serial_fav;
	}
	function setSerial_main_cus($serial_main_cus){
		return $this->serial_main_cus= $serial_main_cus;
	}
    function setSerial_cus_fav($serial_cus_fav){
            return $this->serial_cus_fav = $serial_cus_fav;
        }
    function setStatus_fav($status_fav){
            return $this->status_fav = $status_fav;
        }
    function setRelative_fav($relative_fav){
            return $this->relative_fav = $relative_fav;
        }
}

