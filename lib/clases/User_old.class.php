<?php
/*
File: User.class.php
Author: Esteban Angulo
Creation Date: 22/12/2009 11:00
Last Modified:30/12/2009 
*/
class User{				
    var $db;
    var $serial_usr;
    var $serial_pbc;
    var $serial_mbc;
    var $serial_cit;
    var $serial_dea;
    var $username_usr;
    var $document_usr;
    var $first_name_usr;
    var $last_name_usr;
    var $address_usr;
    var $phone_usr;
    var $cellphone_usr;
    var $birthdate_usr;
    var $email_usr;
    var $position_usr;
    var $password_usr;
    var $first_access_usr;
    var $status_usr;
    var $belongsto_usr;
    var $visits_number_usr;
    var $sell_free_usr;
    var $in_assistance_group_usr;
    var $aux_data;
    var $commission_percentage_usr;
    var $isLeader_usr;
		
    function __construct($db, $serial_usr=NULL, $serial_pbc=NULL, $serial_mbc=NULL, $serial_cit=NULL, $serial_dea=NULL, $username_usr = NULL, 
					     $document_usr=NULL, $first_name_usr=NULL, $last_name_usr=NULL, $address_usr=NULL, $phone_usr=NULL, $cellphone_usr=NULL,
						 $birthdate_usr=NULL, $email_usr=NULL, $position_usr=NULL, $password_usr=NULL, $first_access_usr=NULL, $status_usr=NULL,
						 $belongsto_usr=NULL, $visits_number_usr=NULL, $sell_free_usr=NULL, $in_assistance_group_usr=NULL, $commission_percentage_usr=NULL, $isLeader_usr=NULL){
        $this -> db = $db;
        $this -> serial_usr = $serial_usr;
        $this -> serial_pbc = $serial_pbc;
        $this -> serial_mbc = $serial_mbc;
        $this -> serial_cit = $serial_cit;
        $this -> serial_dea = $serial_dea;
        $this -> username_usr = $username_usr;
        $this -> document_usr = $document_usr;
        $this -> first_name_usr = $first_name_usr;
        $this -> last_name_usr = $last_name_usr;
        $this -> address_usr = $address_usr;
        $this -> phone_usr = $phone_usr;
        $this -> cellphone_usr = $cellphone_usr;
        $this -> birthdate_usr = $birthdate_usr;
        $this -> email_usr = $email_usr;
        $this -> position_usr = $position_usr;
        $this -> password_usr = $password_usr;
        $this -> first_access_usr = $first_access_usr;
        $this -> status_usr = $status_usr;
        $this -> belongsto_usr = $belongsto_usr;
        $this -> visits_number_usr = $visits_number_usr;
	$this -> sell_free_usr = $sell_free_usr;
	$this -> in_assistance_group_usr=$in_assistance_group_usr;
	$this -> commission_percentage_usr = $commission_percentage_usr;
        $this -> isLeader_usr = $isLeader_usr;
    }
	
    /***********************************************
    * funcion getData
    * sets data by username
    ***********************************************/
    function getData($activeUser = false){ //SEND TRUE TO FILTER ONLY ACTIVE USERS
		$sql = 	"SELECT u.serial_usr,
                                u.serial_pbc,
                                u.serial_mbc, 
                                u.serial_cit,
                                u.serial_dea,
                                u.username_usr,
                                u.document_usr,
                                u.first_name_usr,
                                u.last_name_usr,
                                u.address_usr,
                                u.phone_usr,
                                u.cellphone_usr,
                                DATE_FORMAT(u.birthdate_usr, '%d/%m/%Y'),
                                u.email_usr,
                                u.position_usr,
                                u.password_usr,
                                u.first_access_usr,
                                u.status_usr,
                                u.belongsto_usr,
                                u.visits_number_usr,
				u.sell_free_usr,
				u.in_assistance_group_usr,
                                cou.serial_cou,
                                z.serial_zon,
                                dea2.serial_dea,
				u.commission_percentage_usr,
                                u.isLeader_usr
                            FROM user u
                                JOIN city c ON c.serial_cit = u.serial_cit
                                JOIN country cou ON cou.serial_cou = c.serial_cou
                                JOIN zone z ON z.serial_zon = cou.serial_zon
                                LEFT JOIN dealer dea ON dea.serial_dea = u.serial_dea
                                LEFT JOIN dealer dea2 ON dea2.serial_dea = dea.dea_serial_dea";

		if($this->serial_usr != NULL){
			$sql .= " WHERE u.serial_usr = '".$this->serial_usr."'";
		}elseif($this->email_usr != NULL){
			$sql .= " WHERE u.email_usr = '".$this->email_usr."'";
		}else{
			return false;	
		}
		
		if($activeUser){
			$sql .= " AND u.status_usr = 'ACTIVE'";
		}
		
		$result = $this->db->Execute($sql);

		if($result->fields[0]){
                $this -> serial_usr =$result->fields[0];
                $this -> serial_pbc = $result->fields[1];
                $this -> serial_mbc = $result->fields[2];
                $this -> serial_cit = $result->fields[3];
                $this -> serial_dea = $result->fields[4];
                $this -> username_usr = $result->fields[5];
                $this -> document_usr = $result->fields[6];
                $this -> first_name_usr = $result->fields[7];
                $this -> last_name_usr = $result->fields[8];
                $this -> address_usr = $result->fields[9];
                $this -> phone_usr = $result->fields[10];
                $this -> cellphone_usr = $result->fields[11];
                $this -> birthdate_usr = $result->fields[12];
                $this -> email_usr = $result->fields[13];
                $this -> position_usr = $result->fields[14];
                $this -> password_usr = $result->fields[15];
                $this -> first_access_usr = $result->fields[16];
                $this -> status_usr = $result->fields[17];
                $this -> belongsto_usr = $result->fields[18];
                $this -> visits_number_usr = $result->fields[19];
				$this -> sell_free_usr = $result->fields[20];
				$this -> in_assistance_group_usr = $result->fields[21];
                $this -> aux_data['serial_cou']=$result->fields[22];
                $this -> aux_data['serial_zon']=$result->fields[23];
                $this -> aux_data['serial_dea2']=$result->fields[24];
		$this -> commission_percentage_usr = $result->fields[25];
                $this -> isLeader_usr = $result->fields[26];
                return true;
		}
		return false;
    }
	
	function getDataLogin(){
            if($this->username_usr!=NULL){
                $sql = 	"SELECT u.serial_usr,
                                u.serial_pbc,
                                u.serial_mbc,
                                u.serial_cit,
                                u.serial_dea,
                                u.username_usr,
                                u.document_usr,
                                u.first_name_usr,
                                u.last_name_usr,
                                u.address_usr,
                                u.phone_usr,
                                u.cellphone_usr,
                                DATE_FORMAT(u.birthdate_usr, '%m/%d/%Y'),
                                u.email_usr,
                                u.position_usr,
                                u.password_usr,
                                u.first_access_usr,
                                u.status_usr,
                                u.belongsto_usr,
                                u.visits_number_usr,
				u.sell_free_usr,
                                u.isLeader_usr
                            FROM user u
                            WHERE username_usr='".$this->username_usr."'";
//                die($sql);
                $result = $this->db->Execute($sql);
                if ($result === false) return false;

                if($result->fields[0]){
                    $this -> serial_usr =$result->fields[0];
                    $this -> serial_pbc = $result->fields[1];
                    $this -> serial_mbc = $result->fields[2];
                    $this -> serial_cit = $result->fields[3];
                    $this -> serial_dea = $result->fields[4];
                    $this -> username_usr = $result->fields[5];
                    $this -> document_usr = $result->fields[6];
                    $this -> first_name_usr = $result->fields[7];
                    $this -> last_name_usr = $result->fields[8];
                    $this -> address_usr = $result->fields[9];
                    $this -> phone_usr = $result->fields[10];
                    $this -> cellphone_usr = $result->fields[11];
                    $this -> birthdate_usr = $result->fields[12];
                    $this -> email_usr = $result->fields[13];
                    $this -> position_usr = $result->fields[14];
                    $this -> password_usr = $result->fields[15];
                    $this -> first_access_usr = $result->fields[16];
                    $this -> status_usr = $result->fields[17];
                    $this -> belongsto_usr = $result->fields[18];
                    $this -> visits_number_usr = $result->fields[19];
		    $this -> sell_free_usr = $result->fields[20];
                    $this -> isLeader_usr = $result->fields[21];

                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
	}
	
    /***********************************************
    * funcion existsUser
    * verifies if a user exists or not
    ***********************************************/
    function existsUser($serial_usr){
        if($this->username_usr != NULL){
            $sql = "SELECT u.serial_usr
                     FROM user u
                     WHERE LOWER(u.username_usr)= _utf8'".utf8_encode($this->username_usr)."' collate utf8_bin";

            if($this->serial_usr != NULL && $this->serial_usr != ''){
                $sql.= " AND serial_usr == ".$this->serial_usr;
            }
            //echo $sql;
            $result = $this->db -> Execute($sql);
            if($result->fields[0]){
                return	true;
                    //$result->fields[0];//user already exists
            }else{
                return false;
            }
        }else{
            return true;//user already exists
        }
    }
	
    /***********************************************
    * funcion existsUserDocument
    * verifies if a user exists or not
    ***********************************************/
	
    function existsUserDocument($document_usr,$serial_usr){
        $sql = 	"SELECT u.document_usr
                 FROM user u
                 WHERE LOWER(u.document_usr)= _utf8'".utf8_encode($document_usr)."' collate utf8_bin
			     AND status_usr='ACTIVE'";

        if($this->serial_usr != NULL && $this->serial_usr != ''){
            $sql.= " AND serial_usr <> ".$this->serial_usr;
        }
        //echo $sql;
        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }
	
    /***********************************************
    * funcion existsUserEmail
    * verifies if a user exists or not
    ***********************************************/
    function existsUserEmail($email_usr,$serial_usr){
        $sql = 	"SELECT email_usr
                 FROM user
                 WHERE LOWER(email_usr)= _utf8'".utf8_encode($email_usr)."' collate utf8_bin
				 AND status_usr='ACTIVE'";

        if($serial_usr != NULL && $serial_usr != ''){
            $sql.= " AND serial_usr <> ".$serial_usr;
        }
        //echo $sql;
        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }
	
    /***********************************************
    * funcion hasDealers
    * verifies if a user has dealers or not
    ***********************************************/
    function hasDealers($serial_usr){
        $sql = 	"SELECT u.serial_usr,dea.serial_dea
                FROM user u
                JOIN dealer dea ON dea.serial_usr=u.serial_usr
                WHERE u.serial_usr=".$serial_usr;

        //die($sql);
        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }
	
    /***********************************************
    * funcion hasDealersByManager
    * verifies if a manager has dealers or not
    ***********************************************/
    function hasDealersByManager($serial_mbc){
        $sql = 	"SELECT dea.serial_dea
                FROM dealer dea
                WHERE dea.serial_mbc=".$serial_mbc;

        //die($sql);
        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
            return true;
        }else{
            return false;
        }
    }
	
    /***********************************************
     * funcion getUsers
     * gets information from Users
	 * except for the admin and CRIFA users
    ***********************************************/
    function getUsers(){
        $lista=NULL;
        $sql = 	"SELECT u.serial_usr,u.first_name_usr,u.last_name_usr, u.document_usr, u.username_usr
                 FROM user u
				 WHERE u.serial_usr <> 1";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arr;
    }

    /***********************************************
    * funcion getUsersByManager
    * gets information from Users
	* except for the admin and CRIFA users
    ***********************************************/
    function getUsersByManager($serial_mbc, $serial_usr=NULL, $comissionists_only = TRUE){
        $sql = 	"SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
				 FROM user u
				 WHERE u.serial_mbc =".$serial_mbc."
				 AND u.belongsto_usr='MANAGER'
				 AND u.serial_usr <> 1 and u.status_usr = 'ACTIVE'";

		if($comissionists_only) $sql.=" AND u.commission_percentage_usr IS NOT NULL";

		if($serial_usr != NULL) {
            $sql.=" AND u.serial_usr <> ".$serial_usr;
        }
		//die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }

	/***********************************************
    * funcion getUsersByManagerForAssistances
    * gets information from Users
	 * except for admin and CRIFA users
    ***********************************************/
    function getUsersByManagerForAssistances($serial_mbc, $serial_usr=NULL){
        $sql = 	"SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
				 FROM user u
				 WHERE u.serial_mbc =".$serial_mbc."
				 AND u.belongsto_usr='MANAGER'
				 AND AND u.serial_usr <> 1";

        if($serial_usr != NULL) {
            $sql.=" AND u.serial_usr NOT IN (".$serial_usr.")";
        }

        $result = $this->db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
    }

    /***********************************************
    * funcion getMainManagerUsers
    * gets information from Users
	 * except admin and CRIFA users
    ***********************************************/
    function getMainManagerUsers($not_in=NULL,$for_groups=false, $comissionist=NULL){
        $sql = 	"SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
				 FROM user u
				 JOIN manager_by_country mbc ON u.serial_mbc=mbc.serial_mbc
				 JOIN manager m ON m.serial_man = mbc.serial_man
				 WHERE m.man_serial_man IS NULL
				 AND u.serial_usr <> 1
                 AND u.status_usr = 'ACTIVE'";
		if($comissionist){
			$sql.=" AND commission_percentage_usr IS NOT NULL";
		}
			
        if($not_in){
			$sql.=" AND u.serial_usr NOT IN ($not_in)";
		}
		if($for_groups){
			$sql.=" AND u.in_assistance_group_usr = 'NO'";
		}
        $sql.=" ORDER BY u.first_name_usr";
		//echo $sql;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
	/***********************************************
	* function getUsersByDealer
	@Name: getUsersByDealer
	@Description: gets a list of users by dealer
	@Params:
         * $serial_dea serial of the dealer
		 * $not_in string with serial_usr list, separated by ,
	@Returns: users array except for admin and crifa users/false
	***********************************************/
	function getUsersByDealer($serial_dea,$not_in=NULL,$for_groups=false){
		$sql="SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
              FROM user u
			  WHERE u.belongsto_usr='DEALER'
			  AND serial_dea = $serial_dea
			  AND u.serial_usr <> 1";

		if($not_in){
			$sql.=" AND u.serial_usr NOT IN ($not_in)";
		}
		if($for_groups){
			$sql.=" AND u.in_assistance_group_usr = 'NO'";
		}
		$result=$this->db->getAll($sql);
		if($result)
			return $result;
		else
			return false;
	}
    /***********************************************
    * funcion isMainManagerUser
    * verifies if a User belongs to a main manager
    ***********************************************/
    function isMainManagerUser($serial_usr){
            $sql = 	"SELECT u.serial_usr
                         FROM user u
                         JOIN manager_by_country mbc ON u.serial_mbc=mbc.serial_mbc
                         JOIN manager m ON m.serial_man = mbc.serial_man
                         WHERE u.serial_usr = '".$serial_usr."'
                         AND m.man_serial_man IS NULL";
            $result = $this->db -> Execute($sql);

            if($result->fields[0]){
                    return $result->fields[0];
            }else{
                    return false;
            }
    }

    /***********************************************
    * function insert
    * inserts a new register en DB 
    ***********************************************/
    function insert() {
        $sql = "INSERT INTO user (
                                serial_pbc,
                                serial_mbc,
                                serial_cit,
                                serial_dea,
                                username_usr,
                                document_usr,
                                first_name_usr,
                                last_name_usr,
                                address_usr,
                                phone_usr,
                                cellphone_usr,
                                birthdate_usr,
                                email_usr,
                                position_usr,
                                password_usr,
                                first_access_usr,
                                status_usr,
                                belongsto_usr,
                                visits_number_usr,
				sell_free_usr,
				in_assistance_group_usr,
                                isLeader_usr
                                
                                )
                VALUES( '".$this->serial_pbc."',";
                        if($this->serial_mbc != ''){
                            $sql.= "'".$this->serial_mbc."',";
                        }else{
                            $sql.= "NULL,";
                        }
                        $sql.= "'".$this->serial_cit."',";
                        if($this->serial_dea != ''){
                            $sql.="'".$this->serial_dea."',";
                        }else{
                            $sql.= "NULL,";
                        }
                        $sql.= "'".$this->username_usr."',
                                '".$this->document_usr."',
                                '".$this->first_name_usr."',
                                '".$this->last_name_usr."',
                                '".$this->address_usr."',
                                '".$this->phone_usr."',
                                '".$this->cellphone_usr."',
                                STR_TO_DATE('".$this->birthdate_usr."','%d/%m/%Y'),
                                '".$this->email_usr."',
                                '".$this->position_usr."',
                                '".md5($this->password_usr)."',
                                0,
                                'ACTIVE',
                                '".$this->belongsto_usr."',
                                '".$this->visits_number_usr."',
				'".$this->sell_free_usr."',
                                '".$this->in_assistance_group_usr."',
				'".$this->isLeader_usr."')";
              
                        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "UPDATE user SET
                    document_usr = '".$this->document_usr."',
                    first_name_usr = '".$this->first_name_usr."',
                    last_name_usr = '".$this->last_name_usr."',
                    address_usr	='".$this->address_usr."',
                    phone_usr = '".$this->phone_usr."',
                    cellphone_usr = '".$this->cellphone_usr."',
                    birthdate_usr = STR_TO_DATE('".$this->birthdate_usr."','%d/%m/%Y'),
                    email_usr = '".$this->email_usr."',
                    position_usr = '".$this->position_usr."',";
                if($this->password_usr != NULL && $this->password_usr != ''){
                    $sql .= "password_usr = '".$this->password_usr."',";
                }

                if($this->first_access_usr != NULL && $this->first_access_usr != ''){
                    $sql .="first_access_usr ='".$this->first_access_usr."',";
                }
				$sql .="status_usr= '".$this->status_usr."',
						serial_pbc = '".$this->serial_pbc."',";
                    
                if($this->serial_mbc != NULL && $this->serial_mbc != ''){
                    $sql.="serial_mbc = '".$this->serial_mbc."',";
                }else{
                    $sql.="serial_mbc = NULL,";
                }
                
                if($this->serial_dea != NULL && $this->serial_dea != ''){
                    $sql.="serial_dea = '".$this->serial_dea."',";
				}else{
                    $sql.= "serial_dea = NULL,";
				}
		    $sql .="belongsto_usr = '".$this->belongsto_usr."',
		    visits_number_usr = " . ($this->visits_number_usr? "'" .$this->visits_number_usr . "'":'NULL') . ",
		    sell_free_usr= '".$this->sell_free_usr."',
		    in_assistance_group_usr= '".$this->in_assistance_group_usr."',
                    isLeader_usr= '".$this->isLeader_usr."',";            
				if($this->commission_percentage_usr!= NULL && $this->commission_percentage_usr!=''){
					$sql.="commission_percentage_usr='".$this->commission_percentage_usr."'";
				}
				else{
					$sql.="commission_percentage_usr=NULL";
				}
        $sql .=" WHERE username_usr = '".$this->username_usr."'";
        $result = $this->db->Execute($sql);

    	if($result){
            return true;
        }else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * funcion updatePassword
    * Sets the new password for the user
    ***********************************************/
    function updatePassword($serial_usr) {
        $sql = "UPDATE user SET
                password_usr = '".$this->password_usr."'
                WHERE serial_usr = '".$serial_usr."'";
        //echo $sql;
        $result = $this->db->Execute($sql);

        if ($result === false) return false;

        if($result==true)
            return true;
        else
            return false;
    }
    /***********************************************
    * funcion getAllStatus
    * Returns all kind of Status which exist in DB
    ***********************************************/
    function getAllStatus(){
        // Finds all possible status form ENUM column
        $sql = "SHOW COLUMNS FROM user LIKE 'status_usr'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];

        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
	
    /***********************************************
    * funcion getAllTypes
    * Returns all kind of Status which exist in DB
    ***********************************************/
    function getAllTypes(){
            // Finds all possible status form ENUM column
            $sql = "SHOW COLUMNS FROM user LIKE 'type_usr'";
            $result = $this->db -> Execute($sql);

            $type=$result->fields[1];

            $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
            return $type;
    }
	
    /*******************************************
    @Name: getUserAutocompleter
    @Description: Returns an array of users.
    @Params: User name or pattern.
    @Returns: User array
    ********************************************/
    function getUsersAutocompleter($name_pattern){
        $sql = "SELECT serial_usr,
                       first_name_usr,
                       last_name_usr,
                       username_usr
                FROM user
                WHERE (first_name_usr LIKE '%".utf8_encode($name_pattern)."%'
                OR  last_name_usr LIKE '%".utf8_encode($name_pattern)."%'
				OR  last_name_usr LIKE '%".utf8_encode($name_pattern)."%'
				OR CONCAT(first_name_usr,' ',last_name_usr) LIKE '%".utf8_encode($name_pattern)."%'
				OR CONCAT(last_name_usr,' ',first_name_usr) LIKE '%".utf8_encode($name_pattern)."%'
                OR  username_usr  LIKE '%".utf8_encode($name_pattern)."%'
				OR  document_usr  LIKE '%".utf8_encode($name_pattern)."%')
				AND serial_usr <> 1
                LIMIT 10";

        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }
	
    /*******************************************
    @Name: getComissionistAutocompleter
    @Description: Returns an array of users.
    @Params: User name or pattern.
    @Returns: User array
    ********************************************/
    function getComissionistAutocompleter($name_usr,$serial_cit, $serial_mbc){
        $sql = "SELECT usr.serial_usr, usr.first_name_usr,usr.last_name_usr,usr.username_usr,usr.serial_cit,
                        IF(GROUP_CONCAT(cbc.serial_cou),'true','false') AS is_comissionist
                FROM user usr
				LEFT JOIN comissionist_by_country cbc ON cbc.serial_usr=usr.serial_usr
                WHERE (usr.first_name_usr LIKE '%".$name_usr."%'
                OR  usr.last_name_usr  LIKE '%".$name_usr."%'
                OR  usr.username_usr   LIKE '%".$name_usr."%')
                AND usr.serial_cit='".$serial_cit."'
                AND usr.status_usr='ACTIVE'
				AND usr.commission_percentage_usr IS NOT NULL
				AND usr.serial_usr <> 1";

		if($serial_mbc!='1'){
			$sql.=" AND usr.serial_mbc=".$serial_mbc;
		}

		$sql.=" GROUP BY usr.serial_usr
                LIMIT 10";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }


	/***********************************************
    * funcion getCitiesByCountry
    * gets all the Cities in a specific country of the system
    ***********************************************/
	function getComissionistByCity($serial_cit,$cityList=NULL){

	$sql="  SELECT DISTINCT ubd.serial_usr, CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
            FROM dealer d
            JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd = 'ACTIVE'
            JOIN user u ON u.serial_usr = ubd.serial_usr AND u.status_usr = 'ACTIVE'
            JOIN sector s ON s.serial_sec = d.serial_sec
            AND s.serial_cit ='".$serial_cit."'
			AND u.serial_usr <> 1";

			
		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/***********************************************
    * funcion getComissionistByCityByDealer
    * gets all the Cities in a specific country of the system
    ***********************************************/
	function getComissionistByCityByManager($serial_mbc,$serial_cit,$cityList=NULL){

	$sql="  SELECT DISTINCT ubd.serial_usr, CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
            FROM dealer d
            JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea
            JOIN user u ON u.serial_usr = ubd.serial_usr
            JOIN sector s ON s.serial_sec = d.serial_sec
            AND s.serial_cit = $serial_cit
			AND u.serial_usr <> 1
			WHERE d.serial_mbc = $serial_mbc";


		$result = $this ->db -> Execute($sql);

			$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
	/***********************************************
    * funcion getComissionistsByManager
    * gets all the Comissionists for a specific manager
    ***********************************************/
	public static function getComissionistsByManager($db, $serial_mbc, $filterToComissionistsWithPayments = false){
		$sql = "SELECT	DISTINCT u.serial_usr, 
						CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
            	FROM user u
            	JOIN user_by_dealer ubd ON u.serial_usr = ubd.serial_usr AND ubd.status_ubd = 'ACTIVE' AND u.status_usr = 'ACTIVE'
            	JOIN dealer dea ON ubd.serial_dea = dea.serial_dea AND dea.status_dea = 'ACTIVE' AND dea.serial_mbc = $serial_mbc";
					
         if($filterToComissionistsWithPayments){
         	$sql .= " JOIN dealer dea_comm ON dea.dea_serial_dea = dea_comm.serial_dea
	            	JOIN product_by_dealer pbd ON dea_comm.serial_dea = pbd.serial_dea
	            	JOIN sales sal ON sal.serial_pbd = pbd.serial_pbd
	            	JOIN invoice inv ON inv.serial_inv = sal.serial_inv AND inv.status_inv = 'PAID' ";
         }
            	
         $sql .= " WHERE u.serial_usr <> 1";
		
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$resultArray = array();
			$cont=0;
			do{
				$resultArray[$cont ++]=$result->GetRowAssoc(false);
				$result->MoveNext();
			}while($cont < $num);
		}
		return $resultArray;
	}
	
    /*******************************************
    @Name: getCounterUsers
    @Description: Returns an array of users that live in a specific city
                              and are related to a specific dealer
    @Params: User name or pattern.
    @Returns: User array
    ********************************************/
    function getCounterUsers($pattern, $serial_cit, $serial_dea){
        $sql = "SELECT u.serial_usr, u.first_name_usr, u.last_name_usr, u.document_usr
                FROM user u
                WHERE (u.first_name_usr LIKE '%".$pattern."%'
                OR  u.last_name_usr  LIKE '%".$pattern."%'
                OR  u.document_usr   LIKE '%".$pattern."%')
                AND u.serial_dea = ".$serial_dea."
				AND u.serial_usr <> 1
                LIMIT 10";

        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    /*******************************************
    * @Name: isPLANNETASSISTUSER
    * @Description: Returns true if the user belongst to the PLANETASSIST Headquarters.
    * @Params: User name
    * @Returns: true or false
    ********************************************/
    function isPlanetAssistUser(){
        $sql = "SELECT u.serial_usr
                FROM user u
                JOIN manager_by_country mbc ON mbc.serial_mbc=u.serial_mbc
                JOIN manager m ON m.serial_man=mbc.serial_mbc AND m.man_serial_man IS NULL
                WHERE serial_usr=".$this->serial_usr."
                AND m.man_serial_man IS NULL";
//echo $sql;
        $result = $this->db->Execute($sql);

        if ($result === false) return false;

        $num = $result -> RecordCount();
        if($num>0){
            return true;
        }else{
            return false;
        }
    }
	
    /*******************************************
    @Name: isCounter
    @Description: Returns true if the user is counter or flase if it is not.
    @Params: User name
    @Returns: true or false
    ********************************************/
    function isCounter($serial_usr){
        $sql = "SELECT u.serial_usr,c.serial_cnt,c.status_cnt
                FROM user u
                JOIN counter c ON c.serial_usr=u.serial_usr
                WHERE u.serial_usr=".$serial_usr."";
        //echo $sql;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }
	
    /***********************************************
    * funcion getCountryListUser
    *returns a list with all countrys to which a manager has access 
    ***********************************************/
    function getCountryListUser($serial_usu) {
        $lista=NULL;

        $sql = 	"SELECT serial_cou
                 FROM user_by_country
                 WHERE serial_usr ='".$serial_usr."'
                 ORDER BY 1";
        //echo $sql;
        $result = $this->db->Execute($sql);
        if ($result === false) return false;

        $cont=0;
        while (!$result->EOF) {
            $lista[$cont]=$result->fields[0];
            $result->MoveNext();
            $cont++;
        }
        return $lista;
    }

    /***********************************************
    * funcion getSellersByBranch
    *returns a list with all users who could be sellers from a branch
    ***********************************************/
    function getSellersByBranch($serial_dea) {
        $sql = 	"SELECT DISTINCT c.serial_cnt,CONCAT(u.first_name_usr,' ',u.last_name_usr) as name
                 FROM user u
                 JOIN counter c ON c.serial_usr=u.serial_usr AND c.serial_dea ='".$serial_dea."'
                 AND c.status_cnt = 'ACTIVE'
				 AND u.serial_usr <> 1
                 ORDER BY name";
        //echo $sql;
       $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $list=array();
            $cont=0;

            do{
                $list[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $list;
    }

	/***********************************************
    * funcion getUsersByManagerByCity
    * gets information from Users
    ***********************************************/
    function getUsersByManagerByCity($serial_mbc,$serial_cit, $planet='NO'){
        $sql = 	"(SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
				 FROM user u
				 WHERE u.serial_mbc =".$serial_mbc." AND u.serial_cit =".$serial_cit."
				 AND u.serial_usr <> 1)";

		if($planet=='YES'){
			$sql.=" UNION ( SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
						    FROM user u
						    WHERE u.serial_mbc=1)";
		}
		
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        $arr=array();
        if($num > 0){
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
/***********************************************
    * funcion getUsersByCity
    * gets information from Users
    ***********************************************/
    function getUsersByCity($serial_cit, $planet='NO'){
        $sql = 	"(SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
					FROM user u
					WHERE u.serial_cit =".$serial_cit."
					AND u.serial_usr <> 1 
					AND serial_mbc IS NOT NULL
					AND u.status_usr = 'ACTIVE'
				 )";

		if($planet=='YES'){
			$sql.=" UNION ( SELECT u.serial_usr,u.first_name_usr,u.last_name_usr
						    FROM user u
						    WHERE u.serial_mbc = 1
							AND u.status_usr = 'ACTIVE')";
		}
		$sql .= " ORDER BY 3"; 
		
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        $arr=array();
        if($num > 0){
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
	
			/**
	@Name: getUsersByInvoice
	@Description: Retrieves the information of a specific user list
	@Params: Serial number of a city
	@Returns: Users array
	**/
	function getUsersByInvoice($serial_cit, $serial_mbc, $dea_serial_dea){
		$sql =  "SELECT DISTINCT (us.serial_usr),us.first_name_usr, us.last_name_usr
					FROM sales s
					JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
					JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					JOIN sector sec ON br.serial_sec = sec.serial_sec
					JOIN city cit ON sec.serial_cit = cit.serial_cit
					JOIN user us ON cnt.serial_usr = us.serial_usr
					LEFT JOIN invoice i ON i.serial_inv = s.serial_inv
					WHERE cit.serial_cit =".$serial_cit;
		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}
		if($dea_serial_dea){
			$sql.=" AND dea.serial_dea=".$dea_serial_dea;
		}
		$sql.=" ORDER BY us.first_name_usr";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

    /**
    @Name: getBirthdateReport
    @Description: Retrieves the birthdate information of a specific user list
    @Params: Serial number of a country
    @Returns: Users array
    **/
    public static function getBirthdateReport($db,$serial_cou, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL, $month=NULL){
        $sql =  "SELECT usr.document_usr as col1, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as col2,
						IFNULL(dea.name_dea, man.name_man) as col3, IFNULL(dea.address_dea, man.address_man) as col4,
						DATE_FORMAT(usr.birthdate_usr, '%d/%m/%Y') as col5, CONCAT(u2.first_name_usr, ' ', u2.last_name_usr) as col6 
                 FROM user usr
                 JOIN city cit ON cit.serial_cit = usr.serial_cit
				 LEFT JOIN dealer dea ON dea.serial_dea = usr.serial_dea AND dea.status_dea = 'ACTIVE' 
				 LEFT JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea and ubd.status_ubd='ACTIVE'
				 JOIN user u2 on u2.serial_usr= ubd.serial_usr
				 LEFT JOIN manager_by_country mbc ON mbc.serial_mbc = usr.serial_mbc 
				 LEFT JOIN manager man ON man.serial_man = mbc.serial_man 
                 WHERE cit.serial_cou =".$serial_cou." 
				 AND usr.serial_usr <> 1
				 AND usr.status_usr = 'ACTIVE'";
		
        if($serial_cit != NULL) {
            $sql .= " AND usr.serial_cit =".$serial_cit;
        }
        if($serial_mbc != NULL) {
            $sql .= " AND (usr.serial_mbc = $serial_mbc OR dea.serial_mbc = $serial_mbc)";
        }
		if($serial_usr != NULL) {
            $sql .= " AND ubd.serial_usr =".$serial_usr;
        }
        if($month != NULL) {
            $sql .= " AND MONTH(usr.birthdate_usr) =".$month;
        }
        $sql .= " ORDER BY MONTH(usr.birthdate_usr), DAY(usr.birthdate_usr)";
        //die($sql);
        $result = $db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
			$arreglo=array();
			$cont=0;
			do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
			}while($cont<$num);
        }
        return $arreglo;
    }
	/**
	@Name: getUsersByBranch
	@Description: Retrieves the information of a specific user list
	@Params: Serial number of a branch
	@Returns: Users array
	**/
	function getUsersByBranch($serial_bra, $offSeller, $serial_mbc, $offManager){
		$sql = "SELECT DISTINCT(u.serial_usr), u.document_usr, u.first_name_usr, u.last_name_usr
				 FROM excess_payments_liquidation epl
				 JOIN user u ON u.serial_usr = epl.serial_usr
				 WHERE  u.serial_dea='".$serial_bra."'";

		if($offManager=='YES' && $offSeller=='YES'){
			$sql.=" UNION (SELECT DISTINCT(u.serial_usr), u.document_usr, u.first_name_usr, u.last_name_usr
							FROM excess_payments_liquidation epl
							JOIN user u ON u.serial_usr = epl.serial_usr
							JOIN manager_by_country mbc ON mbc.serial_mbc=u.serial_mbc
							JOIN manager man ON mbc.serial_man=man.serial_man AND man.man_serial_man IS NULL
							JOIN manager man2 ON man.serial_man=man2.man_serial_man
							JOIN manager_by_country mbc2 ON mbc2.serial_man=man2.serial_man
							WHERE mbc2.serial_mbc='$serial_mbc')";
		}

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		$arreglo=array();
		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

		 /***********************************************
    * funcion getUsersForAssignPercentage
    * gets information from Users
    ***********************************************/
    function getUsersForAssignPercentage($serial_mbc,$general=NULL){
        $sql = 	"SELECT u.serial_usr,u.document_usr,u.first_name_usr,u.last_name_usr
				 FROM user u
				 WHERE u.serial_mbc =".$serial_mbc."
				 AND u.belongsto_usr='MANAGER'
				 AND u.serial_usr <> 1
				 AND status_usr = 'ACTIVE'";

		if($general!=NULL && $general!=''){
			$sql.=" AND u.commission_percentage_usr IS NOT NULL";
		}
		else{
			$sql.=" AND u.commission_percentage_usr IS NULL";
		}
		
		$sql .= " ORDER BY last_name_usr, first_name_usr";
		//echo $sql;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }

	/***********************************************
	* function getUserByDealer
	@Name: getUserByDealer
	@Description: gets a list of users by dealer
	@Params:
         * $serial_dea serial of the dealer
	@Returns: serial of the user founded
	***********************************************/
	function getUserByDealer($serial_dea, $type){
		$sql="SELECT ubd.serial_usr
              FROM dealer dea
			  JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
			  ";
		if($type=='new'){
			$sql.="WHERE dea.dea_serial_dea = ".$serial_dea." AND dea.branch_number_dea = 1";
		}else{
			$sql.="WHERE dea.serial_dea = ".$serial_dea;
		}
			
		$result = $this->db->getOne($sql);
		return $result;
	}

		/***********************************************
	* function getCityLoggedUser
	@Name: getUserByDealer
	@Description: gets a list of users by dealer
	@Params:
         * $serial_dea serial of the dealer
	@Returns: serial of the user founded
	***********************************************/
	function getCityLoggedUser(){
				$sql="SELECT cit.serial_cit, cit.name_cit ";
			if($this->serial_dea!='' && $this->serial_dea!=NULL){
				$sql.="FROM dealer dea
					   JOIN sector sec ON sec.serial_sec=dea.serial_sec
					   JOIN city cit ON cit.serial_cit=sec.serial_cit
					   WHERE dea.serial_dea='".$this->serial_dea."'";

			}
			if($this->serial_mbc!='' && $this->serial_mbc!=NULL){
				$sql.="FROM manager_by_country mbc
					   JOIN manager man ON man.serial_man=mbc.serial_man
					   JOIN city cit ON cit.serial_cit=man.serial_cit
					   WHERE mbc.serial_mbc='".$this->serial_mbc."'";

			}		

			if($this->serial_mbc=='1'){
				return 'Quito';
			}
			else{
				$result = mysql_fetch_row(mysql_query($sql));
				return $result[1];
			}
	}

	/***********************************************
	* function checkDeactivateUser
	@Name: checkDeactivateUser
	@Description: checks if a user that we want to deactivate is not in User by Dealer or in Commissionist by Country or in PaymentRequest.
	@Params:
         * $serial_usr serial of the dealer
	@Returns: serial of the user founded
	***********************************************/
	function checkDeactivateUser($serial_usr){
		$sql="SELECT SUM(ifnull(ubd.serial_ubd, '0')) as 'responsible', SUM(IFNULL(cbc.serial_cbc, '0')) AS 'comissionist', SUM(IFNULL(prq.serial_prq, '0')) AS 'audit'
			 FROM user u
			 LEFT JOIN user_by_dealer ubd ON ubd.serial_usr=u.serial_usr AND ubd.status_ubd='ACTIVE'
			 LEFT JOIN comissionist_by_country cbc ON cbc.serial_usr=u.serial_usr AND cbc.status_cbc='ACTIVE'
			 LEFT JOIN payment_request prq ON prq.usr_serial_usr=u.serial_usr AND prq.status_prq='AUDIT_REQUEST'
			 WHERE u.serial_usr='".$serial_usr."'
			 GROUP BY u.serial_usr";
		//$result = $this->db -> Execute($sql);
		$result = mysql_fetch_row(mysql_query($sql));
		return $result;
	}

	public static function updateUserName($db, $serial_usr, $newUsername){
		$sql="UPDATE user SET
				username_usr='".$newUsername."'
			  WHERE serial_usr=".$serial_usr;

		 $result = $db -> Execute($sql);

		 if($result){
			 return TRUE;
		 }else{
			 return FALSE;
		 }
	}

	public static function getUserByEmail($db, $email_usr){
		$sql = "SELECT email_usr, CONCAT(first_name_usr,' ',last_name_usr) as name_usr
				FROM user
				WHERE email_usr = '$email_usr'";
		$result = $db->getAll($sql);
        if($result[0])
            return $result[0];
        else
            return false;
	}

	public static function getUserByMbc($db, $serial_mbc, $not_in = NULL){
		$sql = "SELECT DISTINCT email_usr, CONCAT(first_name_usr,' ',last_name_usr) as name_usr
				FROM user
				WHERE (serial_mbc = $serial_mbc
				OR serial_mbc = 1)
				AND status_usr = 'ACTIVE'";
		if($not_in != NULL) {
			$sql .= " AND email_usr NOT IN($not_in)";
		}
		$sql .= " ORDER BY name_usr";
		//die($sql);
		$result = $db->getAll($sql);
        if($result)
            return $result;
        else
            return false;
	}

	/*
	 * @Name: getManagerByUser
	 * @Description: Retrieves the manager serial of the workplace of a user.
	 * @Params: $db, $serial_usr
	 * @Returns: Country ID
	 */
	public static function getManagerByUser($db, $serial_usr){
		$user=new User($db, $serial_usr);

		if($user->getData()){
			if($user->getBelongsto_usr()=='MANAGER'){
				$serial_mbc=$user->getSerial_mbc();
			}else{
				$dealer= new Dealer($db, $user->getSerial_dea());
				$dealer->getData();
				$serial_mbc=$dealer->getSerial_mbc();
			}

			return $serial_mbc;
		}else{
			return false;
		}
	}
	
	public static function getBasicInformationForUser($db, $serial_usr){
		$sql = "SELECT serial_usr, first_name_usr, last_name_usr
				FROM user
				WHERE serial_usr = $serial_usr";
		
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function isSubManagerUser($db, $serial_usr){
		$sql = "SELECT u.serial_usr
				FROM user u 
				JOIN dealer b ON b.serial_dea = u.serial_dea AND official_seller_dea = 'YES'
				JOIN dealer d ON d.serial_dea = b.dea_serial_dea AND d.manager_rights_dea = 'YES'
				AND u.serial_usr = $serial_usr";
		
		//Debug::print_r($sql); die;
		
		$result = $db -> getOne($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name deactivateUsersInPAS
	 * @param type $db
	 * @param type $serial_bra
	 * @param type $serial_dea
	 * @return boolean 
	 */
	public static function changeUserStatus($db, $status, $level, $serial, $counterTOO = TRUE){
		if($level == 'DEALER'){ //ALL USER'S ON A DEALER
			$sql_user = "UPDATE user 
						SET status_usr = '$status'
						WHERE serial_dea IN (SELECT serial_dea 
											FROM dealer 
											WHERE dea_serial_dea = $serial)";
			
			if($counterTOO){
				$sql_counter = "UPDATE counter SET status_cnt = '$status'
								WHERE serial_usr IN (SELECT serial_usr
													FROM user
													WHERE serial_dea IN (SELECT serial_dea 
																		FROM dealer 
																		WHERE dea_serial_dea = $serial))";
			}
		}else{
			$sql = "UPDATE user SET status_usr = '$status'
					WHERE serial_dea = $serial";
			
			if($counterTOO){
				$sql_counter = "UPDATE counter 
								SET status_cnt = '$status'
								WHERE serial_usr IN (SELECT serial_usr
													FROM user
													WHERE serial_dea = $serial)";
			}
		}
		
		$in_user = $db->Execute($sql_user);
		$in_cnt = $db->Execute($sql_counter);

		if($in_user && $in_cnt){
			return TRUE;
		}else{
			ErrorLog::log($db, "MODIFY $level USER - $serial", $sql_user.'<br />'.$sql_counter);
			return FALSE;
		}
		
	}
	
	public static function retriveActiveUsersAfterNDaysInactive($db, $inactiveDays, $secondWarning = TRUE){
		if($secondWarning):
			$second_notification = $inactiveDays + 20;
			$having_claus = " = $inactiveDays 
							OR inactivePeriod = $second_notification"; 
		else:
			$having_claus = " >= $inactiveDays";
		endif;
		
		$sql = "SELECT u.serial_usr, IF(u.serial_dea IS NULL, 'MANAGER','DEALER') AS 'level',
						DATEDIFF( CURDATE( ) , MAX( login_date_lgn ) ) AS 'inactivePeriod',
						IF(u.serial_dea IS NULL, m.contact_email_man, d.manager_email_dea) AS 'contact_mail',
						IF(u.serial_dea IS NULL, m.contact_man, d.manager_name_dea) AS 'contact_name',
						u.username_usr, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'user_name'
				FROM user u
				JOIN login_control lc ON lc.serial_usr = u.serial_usr
				LEFT JOIN dealer d ON d.serial_dea = u.serial_dea
				LEFT JOIN manager_by_country mbc ON mbc.serial_mbc = u.serial_mbc
				LEFT JOIN manager m ON m.serial_man = mbc.serial_man
				WHERE status_usr = 'ACTIVE'
				GROUP BY u.serial_usr
				HAVING inactivePeriod $having_claus";
		
		//Debug::print_r($sql); die;
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/**
    @Name: getBirthdateReport
    @Description: Retrieves the birthdate information of a specific user list
    @Params: Serial number of a country
    @Returns: Users array
    **/
    public static function getBirthdateReportManager($db, $serial_cou, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL, $month = NULL){
        $sql =  "SELECT 'Gerente' as col1, dea.manager_name_dea as col2,
						dea.name_dea as col3, dea.address_dea as col4,
						DATE_FORMAT(dea.manager_birthday_dea, '%d/%m/%Y') as col5,
						CONCAT(u.first_name_usr, ' ', u.last_name_usr) as col6
                 FROM dealer dea 
				 JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				 JOIN user u ON u.serial_usr = ubd.serial_usr and u.status_usr='ACTIVE'
				 
				 JOIN sector sec ON sec.serial_sec = dea.serial_sec
				 JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				 LEFT JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				 LEFT JOIN manager man ON man.serial_man = mbc.serial_man
                 WHERE dea.status_dea = 'ACTIVE' 
				 AND dea.dea_serial_dea IS NOT NULL";
		
        if($serial_cit != NULL) {
            $sql .= " AND sec.serial_cit =".$serial_cit;
        }
        if($serial_mbc != NULL) {
            $sql .= " AND (dea.serial_mbc = $serial_mbc)";
        }
		if($serial_usr != NULL) {
            $sql .= " AND ubd.serial_usr =".$serial_usr;
        }
        if($month != NULL) {
            $sql .= " AND MONTH(dea.manager_birthday_dea) =".$month;
        }
        $sql .= " ORDER BY MONTH(dea.manager_birthday_dea), DAY(dea.manager_birthday_dea)";
        
		//Debug::print_r($sql); die;
        $result = $db -> getAll($sql);
//        $num = $result -> RecordCount();
//        if($num > 0){
//			$arreglo=array();
//			$cont=0;
//			do{
//					$arreglo[$cont]=$result->GetRowAssoc(false);
//					$result->MoveNext();
//					$cont++;
//			}while($cont<$num);
//        }
        return $result;
    }
    
        /***********************************************
    * funcion hasLeader
    ***********************************************/
    function hasLeader($serial_dea){
            $sql = 	"SELECT u.serial_usr
                         FROM user u
                         WHERE u.isLeader_usr = 'YES' AND u.status_usr = 'ACTIVE' AND u.serial_dea = '".$serial_dea."'";
            $result = $this->db -> Execute($sql);
            //echo $sql; die();
            if($result->fields[0]){
                    return $result->fields[0];
            }else{
                    return false;
            }
    }

	///GETTERS
    function getSerial_usr(){
        return $this->serial_usr;
    }
    function getUsername_usr(){
        return $this->username_usr;
    }
    function getPassword_usr(){
        return $this->password_usr;
    }
    function getFirstname_usr(){
        return $this->first_name_usr;
    }
    function getLastname_usr(){
        return $this->last_name_usr;
    }
    function getDocument_usr(){
        return $this->document_usr;
    }
    function getAddress_usr(){
        return $this->address_usr;
    }
    function getPhone_usr(){
        return $this->phone_usr;
    }
    function getCellphone_usr(){
        return $this->cellphone_usr;
    }
    function getBirthdate_usr(){
        return $this->birthdate_usr;
    }
    function getEmail_usr(){
        return $this->email_usr;
    }
    function getPosition_usr(){
        return $this->position_usr;
    }
    function getStatus_usr(){
        return $this->status_usr;
    }
    function getSerial_pbc(){
        return $this->serial_pbc;
    }
    function getSerial_mbc(){
        return $this->serial_mbc;
    }
    function getSerial_cit(){
        return $this->serial_cit;
    }
    function getBelongsto_usr(){
        return $this->belongsto_usr;
    }
    function getVisitsNumber_usr(){
        return $this->visits_number_usr;
    }
    function getSerial_dea(){
        return $this->serial_dea;
    }
	function getSellFree_usr(){
        return $this->sell_free_usr;
    }
    function getAuxData_usr(){
        return $this->aux_data;
    }
	function getIn_assistance_group_usr(){
        return $this->in_assistance_group_usr;
    }
	function getCommission_percentage_usr(){
        return $this->commission_percentage_usr;
    }
	function getFirstAccess_usr(){
		return $this->first_access_usr;
	}
    function getIsLeader_usr(){
        return $this->isLeader_usr;
    }
    ///SETTERS
    function setSerial_usr($serial_usr){
        $this->serial_usr = $serial_usr;
    }
    function setUsername_usr($username_usr){
        $this->username_usr = $username_usr;
    }
    function setPassword_usr($password_usr){
        $this->password_usr = $password_usr;
    }
    function setFirstname_usr($first_name_usr){
        $this->first_name_usr = utf8_decode($first_name_usr);
    }
    function setLastname_usr($last_name_usr){
        $this->last_name_usr = utf8_decode($last_name_usr);
    }
    function setDocument_usr($document_usr){
        $this->document_usr = $document_usr;
    }
    function setAddress_usr($address_usr){
        $this->address_usr = utf8_decode($address_usr);
    }
    function setPhone_usr($phone_usr){
        $this->phone_usr = $phone_usr;
    }
    function setCellphone_usr($cellphone_usr){
        $this->cellphone_usr = $cellphone_usr;
    }
    function setBirthdate_usr($birthdate_usr){
        $this->birthdate_usr = $birthdate_usr;
    }
    function setEmail_usr($email_usr){
        $this->email_usr = $email_usr;
    }
    function setPosition_usr($position_usr){
        $this->position_usr = $position_usr;
    }
    function setSerial_pbc($serial_pbc){
        $this->serial_pbc = $serial_pbc;
    }
    function setSerial_mbc($serial_mbc){
        $this->serial_mbc = $serial_mbc;
    }
    function setSerial_cit($serial_cit){
        $this->serial_cit = $serial_cit;
    }
    function setStatus_usr($status_usr){
        $this->status_usr = $status_usr;
    }
    function setBelongsto_usr($belongsto_usr){
        $this->belongsto_usr = $belongsto_usr;
    }
    function setVisitsNumber_usr($visits_number_usr){
        $this->visits_number_usr = $visits_number_usr;
    }
    function setSerial_dea($serial_dea){
        $this->serial_dea = $serial_dea;
    }
	function setSellFree_usr($sell_free_usr){
        $this->sell_free_usr = $sell_free_usr;
    }
	function setIn_assistance_group_usr($in_assistance_group_usr){
        $this->in_assistance_group_usr = $in_assistance_group_usr;
    }
	function setCommission_percentage_usr($commission_percentage_usr){
        $this->commission_percentage_usr = $commission_percentage_usr;
    }
	function setFirstAccess_usr($first_access_usr){
		$this->first_access_usr=$first_access_usr;
    }
    	function setIsLeader_usr($isLeader_usr){
        $this->isLeader_usr = $isLeader_usr;
    }
}
?>
