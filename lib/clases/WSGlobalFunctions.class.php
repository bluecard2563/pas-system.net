<?php
/*
File: GlobalFunctions.class.php
Author: Patricio Astudillo M.
Creation Date: 19/02/2010 12:09
Last Modified: 13/04/2010
Modified By: Santiago Borja
*/

class WSGlobalFunctions{
	public static function getMyAge($birthDate){
		$todaysDate = date("d").'/'.date("m").'/'.date("Y");
	   	$days = WSGlobalFunctions::getDaysDiffDdMmYyyy($birthDate,$todaysDate);
		$days --;
	   	$years = $days/365;
		return $years;
	}

	public static function getDaysDiffDdMmYyyy($startDate, $endDate){
		if(!$startDate) return 0;
		if(!$endDate) return 0;
		$startParts = split('[/.-]', $startDate);
		$endParts = split('[/.-]', $endDate);
		
		if( $startParts[0] && $startParts[1] && $startParts[2] &&
			$endParts[0] && $endParts[1] && $endParts[2]){
				$dateDiff = strtotime($endParts[2] . '/' . $endParts[1] . '/' . $endParts[0])
						  - strtotime($startParts[2] . '/' . $startParts[1] . '/' . $startParts[0]);
				//WE ADD 3600 AS 1 HOUR DUE TO PHP DAYLIGHT SAVINGS TIME POLICY OF HAVING MARCH 14TH AS A 23 HOUR DAY
				$returnValue = floor(($dateDiff + 3600)/(60*60*24));
				$returnValue ++;
		}else{
			$returnValue = 0;
		}
		
		//A -1 TRIP CANNOT SHOW AS A ZERO DAY TRIP
		if($returnValue == 0){ return -1; }
		return $returnValue;
	}

	/*
	 * @Name: getUsersCountryOfWork
	 * @Description: Retrieves the country serial of the workplace of a user.
	 * @Params: $db, $serial_usr
	 * @Returns: Country ID
	 */
	public static function getUsersCountryOfWork($db, $serial_usr){
		$user=new User($db, $serial_usr);

		if($user->getData()){
			if($user->getBelongsto_usr()=='MANAGER'){
				$serial_mbc=$user->getSerial_mbc();
			}else{
				$dealer= new Dealer($db, $user->getSerial_dea());
				$dealer->getData();
				$serial_mbc=$dealer->getSerial_mbc();
			}

			$mbc=new ManagerbyCountry($db, $serial_mbc);
			$mbc->getData();
			return $mbc->getSerial_cou();
		}else{
			return false;
		}
	}
	
	public static function mergeArraysNoRepeat($original,$newValues){
		$newValuesSingles = array();
		foreach ($newValues as $newVal){
			$newValuesSingles[$newVal['duration_ppc']] = $newVal['duration_ppc'];
		}
		
		//THIS FUNCTION WORKS ONLY ON PHP 5.3 OR NEWER:
		//$newTotal = array_replace($original,$newValuesSingles);
		//INSTEAD:
		$newTotal = $original;
		foreach ($newValuesSingles as $key => $newVal){
			$newTotal[$key] = $newVal;
		}
		
		//echo '<pre>';print_r($original);echo '</pre> and ';
		//echo '<pre>';print_r($newValues);echo '</pre> give';
		//echo '<pre>';print_r($newTotal);echo '</pre>';
		
		return $newTotal;
	}
	
	public static function stringStartsWith($string,$subString){
		return (substr($string, 0, strlen($subString)) == $subString);
	}
	
public static function sendMail($misc, $type){

        global $db,$global_pType,$global_weekDays,$global_yes_no,$global_typeManager,$global_invoice_number,$global_system_name;
        //Declare a new PHPMailer Object
        $mail = new PHPMailer(true);

        switch($type){
			case 'new_user':
                $subject = "REGISTRO DE NUEVO USUARIO DE BAS";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">Estimado Sr.(a): '.$misc['user'].'</div><br>
									<div style="font-size:12px;">'.$misc['textForEmail'].' <a href="https://www.pas-system.net/" target="_blank">www.pas-system.net</a></div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> '.$misc['username'].'
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> '.$misc['pswd'].'
                                                    </div><br>
                                    <div><br/><br/></div>
									<div style="font-size:12px;"><br/><br/>
									Le recordamos que su usuario y clave son de uso estrictamente personal y confidencial. <br>
									</div>

									<div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									</div>
									<div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/contract_logos/logoblue_contracts.jpg" border="0" />
                            </td></tr>
                        </table>';

                $mail -> AddAddress ($misc['email']);
                break;

            case 'newClient':
                if($misc['subject']){
					$subject = $misc['subject'];
				}
				else{
					$subject = "REGISTRO DE NUEVO CLIENTE";
				}
                $paramBody = new Parameter($db,'10');
                $paramBody->getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
													<div style="font-size:12px;">Estimado Sr.(a): '.$misc['customer'].'</div><br>
													<div style="font-size:12px;">Agradecemos su confianza en nosotros.</div><br>';
				if($misc['cardNumber']){
						if($misc['cardNumber']=='N/A'){
							$message.='<div style="font-size:12px;">Su tarjeta Corporativa fue emitida de manera exitosa, adjunto encontrar&aacute; su contrato y condiciones generales.</div><br>';
						}
						else{
							$message.='<div style="font-size:12px;">Su tarjeta n&uacute;mero '.$misc['cardNumber'].' fue emitida de manera exitosa, adjunto encontrar&aacute; su contrato y condiciones generales.</div><br>';
						}
				}
				$message.='							<div style="font-size:12px;">'.$paramBody -> getValue_par().'</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> '.$misc['username'].'
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> '.$misc['pswd'].'
                                                    </div><br>';
				if($misc['rememberPass']){
						$message.='					<div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
													<br>'.$misc['rememberPass'].'
                                                    </div><br>';
				}
               $message.='          <br/>
									<div style="font-size:12px;"><br/><br/>
									Le recordamos que su usuario y clave son de uso estrictamente personal y confidencial. <br>
									</div>

									<div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									<br></div>
									<div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail -> AddAddress ($misc['email']);
                break;

            case 'resetPassword':
                $subject = "REESTABLECER CLAVE DE INGRESO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">'.$misc['textForEmail'].'</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Username:</strong> '.$misc['username'].'
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> '.$misc['pswd'].'
                                                    </div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail -> AddAddress ($misc['email']);
                break;
            
            case 'contactUsForm':
                $subject = "FORMULARIO DE CONTACTO RECIBIDO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">'.$misc['textForEmail'].'</div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Pa&iacute;s donde se encuentra:</strong> '.$misc['country'].'
                                                    </div><br><div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Nombre:</strong> '.$misc['fullname'].'
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>E-mail:</strong> '.$misc['emailUser'].'
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Comentario:</strong> '.$misc['txtMessage'].'
                                                    </div><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail -> AddAddress ($misc['email']);
                break;
        	
            case 'newManager':
                $subject = "PAS --- INFORMATIVO";
                $paramBody = new Parameter($db,'20');
                $paramBody->getData();
                $table = '<table border="0" align=center width="80%" bgcolor="#FFFFFF">
                          <tr bgcolor="#284787">'; 
                foreach($misc['country_table_titles'] as $ct) {
                    $table .= '<td align="center" style="border: solid 1px white; padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center; color: #FFFFFF;">
                                    '.$ct.'
                               </td>';
                }
                $table .= '</tr>';
                foreach ($misc['countryInfo'] as $c) {
                    $table .= '<tr  style="padding:5px 5px 5px 5px; text-align:left;"" bgcolor="#e8f2fb">
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$c['countryName'].'</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$c['percentage'].'%</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$global_pType[$c['percentageType']].'</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$global_weekDays[$c['paymentDeadline']].'</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$global_invoice_number[$c['invoice']].'</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$global_yes_no[$c['exclusive']].'</td>
                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; border: solid 1px white; padding:5px 5px 5px 5px; text-align:center;">'.$global_yes_no[$c['official']].'</td>
                               </tr>';
                }
                $table .= '</table>';

                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">'.$misc['textForEmail'].'</div><br>
                                                    <table border="1" align="center" cellspacing=0 bordercolor="#000000">
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>DATOS DEL REPRESENTANTE</strong></td>
														 </tr>
														<tr style="padding:5px 5px 5px 5px;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																 <strong>Documento:</strong></td>
															 <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['document'].'</td>
														 </tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;  padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Nombre:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['name'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Direcci&oacute;n:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['address'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['phone'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fecha del Contrato:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['contractDate'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['contact'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono del Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['contactPhone'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>E-mail del Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['contactEmail'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tipo:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$global_typeManager[$misc['type']].'</td>
														</tr>
													</table>
                                    <div><br/><br/></div>
                                    <div style="font-size:12px;">'.$misc['textCountryData'].'</div><br>
                                        '.$table.'
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>'.$misc['textInfo'].'</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';


                $email_adresses = explode(",", str_replace(" ", "", $paramBody->getValue_par()));

                foreach ($email_adresses as $e) {
                    $mail -> AddAddress ($e);
                }
                break;

            case 'newBranch':
                $subject = "PAS --- INFORMATIVO";
                $paramBody = new Parameter($db,'20');
                $paramBody->getData();

                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">'.$misc['textForEmail'].'</div><br>
											<table border="1" align="center" cellspacing=0 bordercolor="#000000">
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>DATOS DE LA SUCURSAL</strong></td>
														 </tr>
														 <tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>Informaci&oacute;n General</strong></td>
														 </tr>
														 <tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>ID:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['id'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Nombre:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['name'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Sector:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['sector'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Ciudad:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['city'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Pa&iacute;s:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['country'].'</td>
														</tr>
														<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Representante:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['manager'].'</td>
														</tr>';
														 if($misc['official'] == 'YES'){
                                                            $message .= '<tr style="padding:5px 5px 5px 5px; text-align:left;">
																			<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																				<strong>Es Oficial:</strong></td>
																			<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">SI</td>
																	</tr>';
                                                        }
						$message .= '				<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Responsable:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['comissionist'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Categor&iacute;a:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['category'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono 1:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['phone1'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono 2:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">';
																if($misc['phone2']!=''){
																	$message.=$misc['phone2'];
																}
																else{
																	$message.='-';
																}
														$message.='</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fax:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">';
																if($misc['fax']!=''){
																	$message.=$misc['fax'];
																}
																else{
																	$message.='-';
																}
														$message.='</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Fecha de contrato:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['contract_date'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico 1:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['email1'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico 2:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['email2'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['contact'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tel&eacute;fono de contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['phone_contact'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico de contacto:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['email_contact'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>Informaci&oacute;n de Pagos</strong></td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto de pago:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['pay_contact'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Factura a nombre de:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['bill_to'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>D&iacute;a de pago:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['payment_deadline'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>D&iacute;a de cr&eacute;dito:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['credit_day'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Porcentaje:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['percentage'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Tipo de porcentaje:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$global_pType[$misc['percentage_type']].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Incentivo a:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['bonus_to'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px;">
															<td align="center" colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; color: #FFFFFF;" bordercolor="#00000" bgcolor="#284787">
																 <strong>Informaci&oacute;n de Asistencia</strong></td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Contacto de asistencias:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['assistance'].'</td>
													</tr>
													<tr style="padding:5px 5px 5px 5px; text-align:left;">
															<td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; padding:5px 5px 5px 5px; text-align:center; " bordercolor="#00000" bgcolor="#e8f2fb">
																<strong>Correo electr&oacute;nico de asistencias:</strong></td>
															<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;  padding:5px 5px 5px 5px; text-align:center;" bordercolor="#00000" bgcolor="#e8f2fb">'.$misc['email_assistance'].'</td>
													</tr>
												</table>';
                                       $message .= '
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>'.$misc['textInfo'].'</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail -> AddAddress ($misc['sendToManager']);
                $mail -> AddAddress ($misc['sendToComissionist']);
                break;

           case 'medicalCheckup':
                $subject = "PAS --- INFORMATIVO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">'.$misc['textForEmail'].'</div><br>
                                        
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>'.$misc['textInfo'].'</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.'</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail -> AddAddress ($misc['email']);
                break;

            case 'refund_application':
            	$subject = "AUTORIZACION DE REEMBOLSO";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-size:12px;">
									Su solicitud de reembolso de la tarjeta '.$misc['card_number'].' perteneciente al comercializador '.$misc['dealer_name'].' ha sido <strong>'.$misc['resolution'].'</strong> <br>

									<br><i>Observaciones:</i><br>
									'.$misc['observations'].'
									</div><br>

                                    <div style="font-size:12px;"><br/><br/>
									<b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
									</div>

                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.'</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';

                $mail -> AddAddress ($misc['email']);
                break;
                
        	case 'newUserWeb':
        		global $systemBaseURL;
            	$subject = "BIENVENIDO A $global_system_name!";
            	$paramBody = new Parameter($misc['db'],'10');
            	$paramBody -> getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">	Estimado Sr(a). '.$misc['fullname'].'</div><br>
									<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-right:25px; widht:300px; float:left;">
									Usted ha sido registrado como usuario adminsitrativo del sitio web de '.$global_system_name.'. Para ingresar al portal, por favor ingrese a
									<a href="'.URL.'administration/">'.$systemBaseURL.'</a> y reg&iacute;strese con estas credenciales:<br><br>
									<strong>Usuario:</strong> '.$misc['username'].'<br/>
									<strong>Contrase&ntilde;a:</strong> '.$misc['pswd'].'<br/><br/>
									</div><br>
                                    <div><br/><br/></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail -> AddAddress ($misc['email']);
                break;

           case 'userWebNewPassword':
            	$subject = "CAMBIO DE CLAVE - '.$global_system_name.'!";
            	$paramBody = new Parameter($misc['db'],'10');
            	$paramBody -> getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">	Estimado Sr(a). '.$misc['fullname'].'</div><br>
									<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-right:25px; widht:300px; float:left;">
									Su clave de acceso al portal Web de Planet Assist ha sido cambiada, utilice los datos a continuaci&oacute;n para poder acceder a su cuenta.
									Por favor ingrese a <a href="'.URL.'administration/">www.planet-assist.net</a> y reg&iacute;strese con estas credenciales:<br><br>
									<strong>Usuario:</strong> '.$misc['username'].'<br/>
									<strong>Contrase&ntilde;a:</strong> '.$misc['pswd'].'<br/><br/>
									</div><br>
                                    <div><br/><br/></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail -> AddAddress ($misc['email']);
                break;
                
           case 'newPromo':
                $participants=implode(', ', $misc['participants']);
                $days=explode(',', $misc['periodDays']);
                if($misc['repeatType']=='WEEKLY'){
                    foreach($days as &$d){
                        switch($d){
                            case 1: $d='lunes';
                                    break;
                            case 2: $d='mart&eacute;s';
                                break;
                            case 3: $d='mi&eacute;rcoles';
                                break;
                            case 4: $d='jueves';
                                break;
                            case 5: $d='viernes';
                                break;
                            case 6: $d='s&aacute;bado';
                                break;
                            case 7: $d='domingo';
                                break;
                        }
                    }
                }
                $days=implode(', ',$days);

            	$subject = "PAS --- INFORMATIVO PROMOCIONES CLIENTES";
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">'.$misc['textForEmail'].'</div>
                                    <div><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>Nombre de la Promoci&oacute;n: </b>'.$misc['name'].'</div>
                                    <div><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>Descripci&oacute;n: </b>'.$misc['description'].'</div>
                                    <div><br/><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;"><b>PREMIOS: </b></div>
                                    <div><br/></div>';
                foreach($misc['conditions'] as $c){
                   foreach($c['products'] as &$cp){
                       $cp=$cp['name_pbl'];
                   }
                   $products=implode(', ', $c['products']);
                                   if($misc['type']=='AMOUNT'){
                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Venda un monto igual o superior a $'.$c['starting_value_pbp'].' y reciba gratis un: <b>'.$c['name_pri'].' </b></div>
                                    <div></div>';
                                   }else if($misc['type']=='PERCENTAGE'){
                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Venda un porcentaje igual o superior a '.$c['starting_value_pbp'].'% de las ventas y reciba gratis un: <b>'.$c['name_pri'].' </b></div>
                                    <div></div>';
                                   }else{
                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Venda un monto que este entre $'.$c['starting_value_pbp'].' y  $'.$c['starting_value_pbp'].' y reciba gratis un: <b>'.$c['name_pri'].' </b></div>
                                    <div></div>';
                                   }
                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Productos que aplican a la promoci&oacute;n: '.$products.'</div>
                                    <div><br/></div>';

                }

                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b>Condiciones de la promoci&oacute;n</b></div>
                                    <div></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">Fecha de inicio: '.$misc['beginDate'].'</div>
                                    <div></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">Fecha de finalizaci&oacute;n: '.$misc['endDate'].'</div>
                                    <div></div>';
                                    
                if($misc['repeat']==1){
                    if($misc['repeatType']=='MONTHLY'){                         
                            $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">D&iacute;as que aplica la promoci&oacute;n: '.$days.' de cada mes.</div>
                                    <div></div>';
                    }else{
                            $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">D&iacute;as que aplica la promoci&oacute;n: '.$days.' de cada semana.</div>
                                    <div></div>';
                    }

                }else{
                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">D&iacute;a que aplica la promoci&oacute;n: '.$days.'</div>
                                    <div></div>';
                }
                         $message.='<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">Promoci&oacute;n v&aacute;lida solo para: '.$participants.'</div>
                                    <div><br/></div>
                                    <div style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><b><strong>'.$misc['textInfo'].'</strong></b></div>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.'</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail -> AddAddress ($misc['emails']);
                break;
           case 'change_standBy':
                $subject = "CAMBIO A STAND-BY";
                $message = '
                    <table align=center width="100%" bgcolor="#FFFFFF">
                            <tr>
                              <td>
                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                    Se ha cambiado el estado de la tarjeta No.- <strong>'.$misc['cardNumber'].'</strong>, a Stand-By
                                    del cliente <strong>'.$misc['customerName'].'</strong>.<br/><br/>
                                    Este cambio fue autorizado por <strong>'.$misc['userName'].'</strong>.<br/><br/><br/>
                                    <strong>
                                    Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.<br/>
                                    </strong>
                                    </div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.' </strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                     </table>
                            ';
                $mail -> AddAddress ($misc['email']);
                break;
           case 'forgotPassword':
            	$subject = "OLVIDO DE CLAVE - '.$global_system_name.'!";
            	$paramBody = new Parameter($db,'20');
            	$paramBody -> getData();
                $message = '<table align=center width="100%" bgcolor="#FFFFFF">
                            <tr><td>
                                    <div style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    Estimado Sr(A). '.$misc['fullname'].'<br/><br/> 
                                    Ante su solicitud de cambio de clave, ahora puede registrarse con las siguientes credenciales:
                                    <div style="font-size:12px;"></div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Usuario:</strong> '.$misc['username'].'
                                                    </div><br>
                                                    <div style="font-size:12px; margin-right:25px; widht:300px; float:left;">
                                                    <strong>Password:</strong> '.$misc['pswd'].'
                                                    </div><br/><br/>
                                                    Le recordamos que su usuario y clave es de uso estrictamente personal y confidencial.<br/><br/>
                                                    <b>Por favor no responda este correo, este buz&oacute;n no recibe supervisi&oacute;n alguna. Para cualquier inquietud comun&iacute;quese con las oficinas en su pa&iacute;s.</b><br>
                                    <div><br/><br/></div>
                                    <div style="color:#000000;">
                                    <strong>SITIO WEB DE '.$global_system_name.'</strong>
                                    </div>
                                    <div><br/><br/></div>
                            </td></tr>
                            <tr><td>
                               <img src="'.URL.'img/system_specific/logopas.png" border="0" />
                            </td></tr>
                        </table>';
                $mail -> AddAddress ($misc['email']);
                break;
        	case 'webSellPA':
        		$subject = $misc['subject'];
        		$message = $misc['message'];
        		$mail -> AddAddress ($misc['email']);
        		break;
        }
        try{
                $mail -> From = FROM_MAIL;
                $mail -> FromName = NAME;
				if($misc['urlGeneralConditions']){
					$pathGeneralConditions = DOCUMENT_ROOT.'pdfs/generalConditions/';
					foreach($misc['urlGeneralConditions'] as $singleCondition){
						$realPath = $pathGeneralConditions.$singleCondition['url_glc'];
						$mail -> AddAttachment($realPath, $singleCondition['url_glc']);
					}
				}
				if($misc['urlContract']){
					global $global_contracts_dir;
					$pathContract = DOCUMENT_ROOT . $global_contracts_dir;
					$realPath = $pathContract.$misc['urlContract'];
					$mail -> AddAttachment($realPath, $misc['urlContract']);
				}

				set_time_limit(36000);
				
                $mail -> Subject = $subject;
                $mail -> Body = $message;
                $mail -> IsHTML (true);
                $mail -> IsSMTP();
                $mail -> Host = HOST;
                $mail -> Timeout = 1;
                $mail -> Port = 25;
                $mail -> SMTPAuth = true;
                $mail -> Username = USUARIO_MAIL;
                $mail -> Password = PSWD_MAIL;
                //$mail -> Send();
                $result = true;
        }catch (Exception $e) {
        	$result = 'ERROR';
        }
		
        return $result;
    }

	    public static function generatePassword($fSize,$linSize,$type=true){
        srand(self::create_sequence());
        $password="";
        $max_chars = round(rand($fSize,$linSize));
        $chars = array();
        for ($i="a"; $i<"z"; $i++) $chars[] = $i;
        $chars[] = "z";
        for ($i=0; $i<$max_chars; $i++) {
          $letra = round(rand(0, 1));
          if ($type == false)
                $password .= $chars[round(rand(0, count($chars)-1))];
          else
                $password .= round(rand(0, 9));
        }

        $password=md5($password);
        $password=substr($password,0,7);

        return $password;
    }

    public static function create_sequence() {
       list($usec, $sec) = explode(' ', microtime());
       return (float) $sec + ((float) $usec * 100000);
    }



    public function generateUser($misc){
        //setlocale(LC_CTYPE, 'es_ES');//para que las funciones strtolower o strtoupper puedan convertir las tildes y &#241;
        $exists_user = 0;
            if($misc['firstname'] != '' && $misc['lastname'] != ''){
                $a = array('&#225;', '&#233;', '&#237;', '&#243;', '&#250;', '&#241;', ' ');
                $b = array('a', 'e', 'i', 'o', 'u', 'n', '');

                $misc['firstname'] = str_replace($a, $b,strtolower($misc['firstname']));
                $misc['lastname'] = str_replace($a, $b,strtolower($misc['lastname']));
                do{
                    $username = substr($misc['firstname'],0,2).substr($misc['lastname'],0,6);
                    if($exists_user != 0){// if there is a user with the same name, the function adds a number
                        $username .= $exists_user;
                    }
                    $sql = "SELECT ".$misc['fields']." FROM ".$misc['class']." WHERE ".$misc['fields']." = '$username'";
                    //echo $sql;
                    $result = $misc['db']->Execute($sql);
                    if($result->fields[0]){
                        $exists_user ++;//user exists
                    }
                    else{
                        return $username;
                    }
                }while($exists_user != 0);
            }else{
                    return false;
            }
    }


	public static function refundCommissions($misc,$serial_sal) {
		$appliedComission= new AppliedComission($misc['db']);
		$appliedComissions->setSerial_sal($serial_sal);
		$appliedComissions->refundSale();
	}

	public static function calculateCommissions($misc, $serial_inv) {
		if(!isset($misc['serial_fre']) && !isset($misc['percentage_fre'])){
			$misc['serial_fre'] = NULL;
			$misc['percentage_fre'] = NULL;
		}else{
			$percentage_fre = $misc['percentage_fre'];
		}	

		if (!isset($misc['type_com'])) {
			$misc['type_com'] = 'IN';
		}

		$appliedComission = new AppliedComission($misc['db']);
		$appliedComissions = $appliedComission->getPercentages($serial_inv);

		if (is_array($appliedComissions)) {
			foreach ($appliedComissions as $key => $comissions) {
				if ($comissions['status_sal'] == 'ACTIVE' || $comissions['status_sal'] == 'STANDBY' || $comissions['status_sal'] == 'EXPIRED') {
					$serial_sal = $comissions['serial_sal'];
					$total_sal = $comissions['total_sal'];

					if (!$appliedComission->checkSalExist($comissions['serial_sal'])) {

						/* Branch comissions */
						$serial_dea = $comissions['serial_dea'];
						$percentage_dea = $comissions['percentage_dea'];
						$type_percentage_dea = $comissions['type_percentage_dea'];
						/* Manager comissions */
						$serial_mbc = $comissions['serial_mbc'];
						$percentage_mbc = $comissions['percentage_mbc'];
						$type_percentage_mbc = $comissions['type_percentage_mbc'];
						/* Branch Representative comissions */
						$serial_ubd = $comissions['serial_ubd'];
						$percentage_ubd = $comissions['percentage_ubd'];

						$comission_dea = $total_sal * ($percentage_dea / 100);
						$comission_mbc = 0;
						$comission_ubd = 0;
						$comission_fre = 0;

						$total_sal -= $comission_dea;

						if ($type_percentage_mbc == 'COMISSION') {
							$comission_mbc = $total_sal * ($percentage_mbc / 100);
						}

						$comission_ubd = $total_sal * ($percentage_ubd / 100);
						$comission_fre = $total_sal * ($percentage_fre / 100);

						$appliedComission->setSerial_sal($serial_sal);

						$appliedComission->setSerial_dea($serial_dea);
						$appliedComission->setValue_dea_com($comission_dea);

						$appliedComission->setSerial_mbc($serial_mbc);
						$appliedComission->setValue_mbc_com($comission_mbc);

						$appliedComission->setSerial_ubd($serial_ubd);
						$appliedComission->setValue_ubd_com($comission_ubd);

						if (isset($misc['serial_fre']) && isset($misc['percentage_fre'])) {
							$appliedComission->setSerial_fre($misc['serial_fre']);
							$appliedComission->setValue_fre_com($comission_fre);
						}
						$appliedComission->setType_com($misc['type_com']);
						$appliedComission->insert();
					}
				}
			}
		}
	}

	public static function calculateBonus($misc, $serial_inv) {
		$invoice = new Invoice($misc['db'], $serial_inv);
		$invoice->getData();
		$due_date_inv = $invoice->getDue_date_inv();
		$date = explode('/', $due_date_inv);
		$invoice_discounts = ($invoice->getDiscount_prcg_inv() + $invoice->getOther_dscnt_inv()) / 100;
		$dealer_comission_percentage = $invoice->getComision_prcg_inv() / 100;

		$sale = new Sales($misc['db']);
		$productByDealer = new ProductByDealer($misc['db']);

		$bonus = new Bonus($misc['db']);
		$sales = $sale->getSalesByInvoice($serial_inv);

		$counter = new Counter($misc['db']);
		$dealer = new Dealer($misc['db']);
		$appliedBonus = new AppliedBonus($misc['db']);

		//check for grace period parameter
		$paramBody = new Parameter($misc['db'], '27');
		$paramBody->getData();
		$due_date = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
		$due_date += ( intval($paramBody->getValue_par()) * 24 * 60 * 60);

		foreach ($sales as $key => $sale) {
			if ($sale['status_sal'] == 'ACTIVE' || $sale['status_sal'] == 'STANDBY' || $sale['status_sal'] == 'EXPIRED') {
				$productByDealer->setSerial_pbd($sale['serial_pbd']);
				$productByDealer->getData();

				$serial_pxc = $productByDealer->getSerial_pxc();
				$dea_serial_dea = $productByDealer->getSerial_dea();

				$counter->setSerial_cnt($sale['serial_cnt']);
				$counter->getData();

				$dealer->setSerial_dea($counter->getSerial_dea());
				$dealer->getData();
				$bonus_to_dea = $dealer->getBonus_to_dea();

				$serial_bon = Bonus::getBonusByPxcDea($misc['db'], $serial_pxc, $dea_serial_dea, $sale['emission_date']);
				$bonus->setSerial_bon($serial_bon);
				$bonus->getData();
				$percentage_bon = $bonus->getPercentage_bon();

				if ($serial_bon) {
					/*					 * ************************* BONUS VALUE CALCULATION *************************************************
					  VALUE = [TOTAL_SAL - (TOTAL_DISCOUNTS_APPLIED ) - DEALER_COMISSION_PERCENTAGE] * BONUS_PERCENTAGE */
					$value_apb = $sale['total_sal'] - ($sale['total_sal'] * $invoice_discounts);
					$value_apb = $value_apb - ($value_apb * $dealer_comission_percentage);
					$value_apb = $value_apb * ($percentage_bon / 100);
					$value_apb = round($value_apb, 2);

					$appliedBonus->setSerial_sal($sale['serial_sal']);
					$appliedBonus->setTotal_amount_apb($value_apb);
					$appliedBonus->setPending_amount_apb($value_apb);
					$today = date('Y-m-d');
					$today_array = explode('-', $today);
					$today_time = mktime(0, 0, 0, $today_array[1], $today_array[2], $today_array[0]);

					if ($misc['ondate_apb'] == 'NO') {
						$appliedBonus->setOn_date_apb('NO');
					} else {
						if ($due_date < $today_time) {
							$appliedBonus->setOn_date_apb('NO');
						} else {
							$appliedBonus->setOn_date_apb('YES');
						}
					}

					$appliedBonus->setStatus_apb('ACTIVE');
					$appliedBonus->setBonus_to_apb($bonus_to_dea);
					$appliedBonus->insert();
				}
			}
		}
	}


	

	/*
	 * @name: createCreditNotePDF
	 * @Description: Creates a PDF File for a credit note.
	 * @Params: $db.- Connection to the DB
	 *          $invoiceName.- The name of the reciever.
	 *			$dateInv.- The creation date of the invoice.
	 * @Creator: Mario Lopez
	 * @Modified by: Patricio Astudillo
	 */
	public static function createCreditNotePDF($misc, $cNoteInfo){
		global $global_weekDaysNumb, $global_weekMonths, $date, $global_refundTypes;
		$invoice = new Invoice($misc['db'], $cNotePDF['serial_inv']);
		$invoice->getData();
		$reportTitle=utf8_decode('Nota de crédito No. '.$misc['cNoteNumber']);

		$totals=array();
		$totals[0]['col1'] = 'Suman:';
		$totals[0]['col2'] = 0;

		$description=array();
		$description[0]=array('col1'=>'<b>Factura a: </b>','col2'=>$cNoteInfo['name'],'col3'=>'<b>RUC/CI: </b>','col4'=>$cNoteInfo['document']);
		$description[1]=array('col1'=>'<b>Fecha: </b>','col2'=>$cNoteInfo['date_inv'],'col3'=>utf8_decode('<b>Teléfono: </b>'),'col4'=>$cNoteInfo['phone']);
		$description[2]=array('col1'=>utf8_decode('<b>Dirección: </b>'),'col2'=>$cNoteInfo['address'],'col3'=>'<b>Factura vence: </b>','col4'=>$cNoteInfo['due_date_inv']);

		//HEADER
			$pdf =& new Cezpdf('A4','portrait');
			$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
			$pdf -> ezSetMargins(90,90,60,50);
		//END HEADERS

		//BEGIN DESCRIPTION
			$options = array(
					'showLines'=>0,
					'showHeadings'=>0,
					'shaded'=>0,
					'fontSize' => 8,
					'textCol' =>array(0,0,0),
					'titleFontSize' => 8,
					'rowGap' => 2,
					'colGap' => 5,
					'xPos'=>'center',
					'xOrientation'=>'center',
					'maxWidth' => 420,
					'cols' =>array(
						  'col1'=>array('justification'=>'left','width'=>80),
						  'col2'=>array('justification'=>'left','width'=>130),
						  'col3'=>array('justification'=>'left','width'=>80),
						  'col4'=>array('justification'=>'left','width'=>130)
					),
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8
			);

			$pdf->ezSetDy(-60);
			$pdf->ezText($reportTitle, 8, array('justification' =>'center'));
			$pdf->ezSetDy(-10);
			$pdf->ezTable($description, array('col1'=>'Col1','col2'=>'Col2','col3'=>'Col3','col4'=>'Col4'), NULL, $options);
		//END DESCRIPTION

		if($cNoteInfo['type']=='INVOICE'){
			$salesRaw = $invoice->getInvoiceSales($cNoteInfo['serial_inv']);

			foreach($salesRaw as $sale){
				$totals[0]['col2'] = $totals[0]['col2'] + $sale['total_sal'];
			}
			$totals[0]['col2'] = number_format($totals[0]['col2'], 2, '.', '');

			$totals[1]['col1'] = 'Descuento ('.$salesRaw[0]['discount_prcg_inv'].'%):';
			$totals[1]['col2'] = $totals[0]['col2']*$salesRaw[0]['discount_prcg_inv']/100;
			$totals[1]['col2'] = number_format($totals[1]['col2'], 2, '.', '');

			$totals[2]['col1'] = 'Otro descuento ('.$salesRaw[0]['other_dscnt_inv'].'%):';
			$totals[2]['col2'] = $totals[0]['col2']*$salesRaw[0]['other_dscnt_inv']/100;
			$totals[2]['col2'] = number_format($totals[2]['col2'], 2, '.', '');

			$totals[3]['col1'] = 'Subtotal:';
			$totals[3]['col2'] = $totals[0]['col2']-$totals[1]['col2']-$totals[2]['col2'];
			$totals[3]['col2'] = number_format($totals[3]['col2'], 2, '.', '');

			$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
			$taxes=0;
			if(is_array($taxesArr)){
				foreach($taxesArr as $t){
						$taxes+=$t['tax_percentage'];
				}
			}
			$totals[4]['col1'] = 'Impuestos ('.$taxes.'%):';
			$totals[4]['col2'] = $taxes*$totals[3]['col2']/100;
			$totals[4]['col2'] = number_format($totals[4]['col2'], 2, '.', '');

			$totals[5]['col1'] = 'Valor Total:';
			$totals[5]['col2'] = $totals[2]['col2']+$totals[3]['col2']+$totals[4]['col2'];
			$totals[5]['col2'] = number_format($totals[5]['col2'], 2, '.', '');

			//BEGIN SALES TABLE
				$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'shaded'=>2,
						'shadeCol'=>array(0.9,0.9,0.9),
						'shadeCol2'=>array(0.77,0.87,0.91),
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezSetDy(-10);
				$pdf->ezTable($salesRaw, array(
											'card_number_sal'=>'<b>Tarjeta No.</b>',
											'name'=>'<b>Nombre</b>',
											'name_pbl'=>'<b>Tipo de Tarjeta</b>',
											'total_sal'=>'<b>Valor</b>'),
								NULL, $options);
			//END SALES TABLE

			//BEGIN TOTALS
				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				$pdf->ezSetDy(-10);
				$textFooter=utf8_decode('Nota de Crédito por Anulación de Factura');
				$pdf->ezText($textFooter, 8, array('justification' =>'center'));
			//END TOTALS
		}else{//If it's a REFUND credit note
			$sale=new Sales($misc['db'], $cNoteInfo['serial_sal']);
			$sale->getData();
			$customer=new Customer($misc['db'], $sale->getSerial_cus());
			$customer->getData();
			$cutomer_name=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
			$card_number=$sale->getCardNumber_sal();
			$refund=new Refund($misc['db'], $cNoteInfo['serial_ref']);
			$refund->getData();

			$salesRaw=$invoice->getInvoiceSales($cNoteInfo['serial_inv'], $sale->getSerial_sal());

			//BEGIN SALES TABLE
				$totals[0]['col2'] = $sale->getTotal_sal();
				
				if($refund->getRetainedType_ref()=='PERCENTAGE'){
					$totals[1]['col1'] = 'Penalidad ('.$refund->getRetainedValue_ref().'%):';
					$totals[1]['col2'] = $totals[0]['col2']*($refund->getRetainedValue_ref()/100);
					$totals[1]['col2'] = number_format($totals[1]['col2'], 2, '.', '');
				}else{
					$totals[1]['col1'] = 'Penalidad (Valor Fijo):';
					$totals[1]['col2'] = $refund->getRetainedValue_ref();
				}

				if($refund->getType_ref()=='REGULAR'){
					$totals[2]['col1'] = 'Descuento ('.$invoice->getDiscount_prcg_inv().'%):';
					$totals[2]['col2'] = $totals[0]['col2']*($invoice->getDiscount_prcg_inv()/100);
					$totals[2]['col2'] = number_format($totals[2]['col2'], 2, '.', '');

					$totals[3]['col1'] = 'Otro descuento ('.$invoice->getOther_dscnt_inv().'%):';
					$totals[3]['col2'] = $totals[0]['col2']*($invoice->getOther_dscnt_inv()/100);
					$totals[3]['col2'] = number_format($totals[3]['col2'], 2, '.', '');
				}				

				$totals[4]['col1'] = 'Subtotal:';
				$totals[4]['col2'] = $totals[0]['col2']-($totals[1]['col2']+$totals[2]['col2']+$totals[3]['col2']);
				$totals[4]['col2'] = number_format($totals[4]['col2'], 2, '.', '');

				$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
				$taxes=0;
				if(is_array($taxesArr)){
					foreach($taxesArr as $t){
							$taxes+=$t['tax_percentage'];
					}
				}
				$totals[5]['col1'] = 'Impuestos ('.$taxes.'%):';
				$totals[5]['col2'] = $taxes*$totals[4]['col2']/100;
				$totals[5]['col2'] = number_format($totals[5]['col2'], 2, '.', '');

				$totals[6]['col1'] = 'Valor Total:';
				$totals[6]['col2'] = $totals[4]['col2']+$totals[5]['col2'];
				$totals[6]['col2'] = number_format($totals[6]['col2'], 2, '.', '');

				//Debug::print_r($totals);


				$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'shaded'=>2,
						'shadeCol'=>array(0.9,0.9,0.9),
						'shadeCol2'=>array(0.77,0.87,0.91),
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezSetDy(-10);
				$pdf->ezTable($salesRaw, array(
											'card_number_sal'=>'<b>Tarjeta No.</b>',
											'name'=>'<b>Nombre</b>',
											'name_pbl'=>'<b>Tipo de Tarjeta</b>',
											'total_sal'=>'<b>Valor</b>'),
								NULL, $options);
			//END SALES TABLE

			//BEGIN TOTALS
				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				$pdf->ezSetDy(-10);
				$textFooter=utf8_decode('Nota de Crédito de Reembolso por: ').$global_refundTypes[$refund->getType_ref()];
				$pdf->ezText($textFooter, 8, array('justification' =>'center'));
			//END TOTALS
		}

		
		//$pdf->ezStream();
		$pdfcode = $pdf->ezOutput();
		$fp=fopen('pdfReports/credit_note_'.$misc['cNoteID'].'.pdf','wb');
		fwrite($fp,$pdfcode);
		fclose($fp);
	}

        /*
	 * @name: createPaymentNotePDF
	 * @Description: Creates a PDF File for a credit note.
	 * @Params: $db.- Connection to the DB
	 *          $invoiceName.- The name of the reciever.
	 *			$dateInv.- The creation date of the invoice.
	 * @Creator: Santiago Benitez
	 */
	public static function createPaymentNotePDF($misc, $cNoteInfo){
		global $global_weekDaysNumb, $global_weekMonths, $date, $global_refundTypes;
                $payment=new Payment($misc['db'],$pNotePDF['serial_inv']);
                $payment->getData();
                $reportTitle='Nota de Pago';
                $receivedPayment='<b>Se ha recibido como abono:  </b>'.$pNoteInfo['receivedPayment'];

                $description=array();
		$description[0]=array('col1'=>'<b>Pago de: </b>','col2'=>$pNoteInfo['name'],'col3'=>'<b>RUC/CI: </b>','col4'=>$pNoteInfo['document']);
		$description[1]=array('col1'=>'<b>Fecha: </b>','col2'=>$pNoteInfo['date_inv'],'col3'=>utf8_decode('<b>Teléfono: </b>'),'col4'=>$pNoteInfo['phone']);
		$description[2]=array('col1'=>utf8_decode('<b>Dirección: </b>'),'col2'=>$pNoteInfo['address'],'col3'=>' ','col4'=>' ');

		//HEADER
			$pdf =& new Cezpdf('A4','portrait');
			$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
			$pdf -> ezSetMargins(90,90,60,50);
		//END HEADERS
                //BEGIN DESCRIPTION
			$options = array(
					'showLines'=>0,
					'showHeadings'=>0,
					'shaded'=>0,
					'fontSize' => 8,
					'textCol' =>array(0,0,0),
					'titleFontSize' => 8,
					'rowGap' => 2,
					'colGap' => 5,
					'xPos'=>'center',
					'xOrientation'=>'center',
					'maxWidth' => 420,
					'cols' =>array(
						  'col1'=>array('justification'=>'left','width'=>80),
						  'col2'=>array('justification'=>'left','width'=>130),
						  'col3'=>array('justification'=>'left','width'=>80),
						  'col4'=>array('justification'=>'left','width'=>130)
					),
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8
			);

			$pdf->ezSetDy(-60);
			$pdf->ezText($reportTitle, 8, array('justification' =>'center'));
			$pdf->ezSetDy(-10);
			$pdf->ezTable($description, array('col1'=>'Col1','col2'=>'Col2','col3'=>'Col3','col4'=>'Col4'), NULL, $options);
                        $pdf->ezSetDy(-10);
			$pdf->ezText($receivedPayment, 8, array('justification' =>'left'));
		//END DESCRIPTION

			$totals[0]['col2'] = number_format($totals[0]['col2'], 2, '.', '');

			$totals[1]['col1'] = 'Descuento ('.$salesRaw[0]['discount_prcg_inv'].'%):';
			$totals[1]['col2'] = $totals[0]['col2']*$salesRaw[0]['discount_prcg_inv']/100;
			$totals[1]['col2'] = number_format($totals[1]['col2'], 2, '.', '');

			$totals[2]['col1'] = 'Otro descuento ('.$salesRaw[0]['other_dscnt_inv'].'%):';
			$totals[2]['col2'] = $totals[0]['col2']*$salesRaw[0]['other_dscnt_inv']/100;
			$totals[2]['col2'] = number_format($totals[2]['col2'], 2, '.', '');

			$totals[3]['col1'] = 'Subtotal:';
			$totals[3]['col2'] = $totals[0]['col2']-$totals[1]['col2']-$totals[2]['col2'];
			$totals[3]['col2'] = number_format($totals[3]['col2'], 2, '.', '');

			$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
			$taxes=0;
			if(is_array($taxesArr)){
				foreach($taxesArr as $t){
						$taxes+=$t['tax_percentage'];
				}
			}
			$totals[4]['col1'] = 'Impuestos ('.$taxes.'%):';
			$totals[4]['col2'] = $taxes*$totals[3]['col2']/100;
			$totals[4]['col2'] = number_format($totals[4]['col2'], 2, '.', '');

			$totals[5]['col1'] = 'Valor Total:';
			$totals[5]['col2'] = $totals[2]['col2']+$totals[3]['col2']+$totals[4]['col2'];
			$totals[5]['col2'] = number_format($totals[5]['col2'], 2, '.', '');

			//BEGIN SALES TABLE
				$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'shaded'=>2,
						'shadeCol'=>array(0.9,0.9,0.9),
						'shadeCol2'=>array(0.77,0.87,0.91),
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezSetDy(-10);
				$pdf->ezTable($salesRaw, array(
											'card_number_sal'=>'<b>Tarjeta No.</b>',
											'name'=>'<b>Nombre</b>',
											'name_pbl'=>'<b>Tipo de Tarjeta</b>',
											'total_sal'=>'<b>Valor</b>'),
								NULL, $options);
			//END SALES TABLE

			//BEGIN TOTALS
				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				$pdf->ezSetDy(-10);
				$textFooter=utf8_decode('Nota de Crédito por Anulación de Factura');
				$pdf->ezText($textFooter, 8, array('justification' =>'center'));
//lo del mario
		$invoice = new Invoice($misc['db'], $cNotePDF['serial_inv']);
		$invoice->getData();
		$reportTitle=utf8_decode('Nota de crédito No. '.$misc['cNoteNumber']);

		$totals=array();
		$totals[0]['col1'] = 'Suman:';
		$totals[0]['col2'] = 0;

		$description=array();
		$description[0]=array('col1'=>'<b>Factura a: </b>','col2'=>$cNoteInfo['name'],'col3'=>'<b>RUC/CI: </b>','col4'=>$cNoteInfo['document']);
		$description[1]=array('col1'=>'<b>Fecha: </b>','col2'=>$cNoteInfo['date_inv'],'col3'=>utf8_decode('<b>Teléfono: </b>'),'col4'=>$cNoteInfo['phone']);
		$description[2]=array('col1'=>utf8_decode('<b>Dirección: </b>'),'col2'=>$cNoteInfo['address'],'col3'=>'<b>Factura vence: </b>','col4'=>$cNoteInfo['due_date_inv']);

		//HEADER
			$pdf =& new Cezpdf('A4','portrait');
			$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
			$pdf -> ezSetMargins(90,90,60,50);
		//END HEADERS

		//BEGIN DESCRIPTION
			$options = array(
					'showLines'=>0,
					'showHeadings'=>0,
					'shaded'=>0,
					'fontSize' => 8,
					'textCol' =>array(0,0,0),
					'titleFontSize' => 8,
					'rowGap' => 2,
					'colGap' => 5,
					'xPos'=>'center',
					'xOrientation'=>'center',
					'maxWidth' => 420,
					'cols' =>array(
						  'col1'=>array('justification'=>'left','width'=>80),
						  'col2'=>array('justification'=>'left','width'=>130),
						  'col3'=>array('justification'=>'left','width'=>80),
						  'col4'=>array('justification'=>'left','width'=>130)
					),
					'innerLineThickness' => 0.8,
					'outerLineThickness' => 0.8
			);

			$pdf->ezSetDy(-60);
			$pdf->ezText($reportTitle, 8, array('justification' =>'center'));
			$pdf->ezSetDy(-10);
			$pdf->ezTable($description, array('col1'=>'Col1','col2'=>'Col2','col3'=>'Col3','col4'=>'Col4'), NULL, $options);
		//END DESCRIPTION

		if($cNoteInfo['type']=='INVOICE'){
			$salesRaw = $invoice->getInvoiceSales($cNoteInfo['serial_inv']);

			foreach($salesRaw as $sale){
				$totals[0]['col2'] = $totals[0]['col2'] + $sale['total_sal'];
			}
			$totals[0]['col2'] = number_format($totals[0]['col2'], 2, '.', '');

			$totals[1]['col1'] = 'Descuento ('.$salesRaw[0]['discount_prcg_inv'].'%):';
			$totals[1]['col2'] = $totals[0]['col2']*$salesRaw[0]['discount_prcg_inv']/100;
			$totals[1]['col2'] = number_format($totals[1]['col2'], 2, '.', '');

			$totals[2]['col1'] = 'Otro descuento ('.$salesRaw[0]['other_dscnt_inv'].'%):';
			$totals[2]['col2'] = $totals[0]['col2']*$salesRaw[0]['other_dscnt_inv']/100;
			$totals[2]['col2'] = number_format($totals[2]['col2'], 2, '.', '');

			$totals[3]['col1'] = 'Subtotal:';
			$totals[3]['col2'] = $totals[0]['col2']-$totals[1]['col2']-$totals[2]['col2'];
			$totals[3]['col2'] = number_format($totals[3]['col2'], 2, '.', '');

			$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
			$taxes=0;
			if(is_array($taxesArr)){
				foreach($taxesArr as $t){
						$taxes+=$t['tax_percentage'];
				}
			}
			$totals[4]['col1'] = 'Impuestos ('.$taxes.'%):';
			$totals[4]['col2'] = $taxes*$totals[3]['col2']/100;
			$totals[4]['col2'] = number_format($totals[4]['col2'], 2, '.', '');

			$totals[5]['col1'] = 'Valor Total:';
			$totals[5]['col2'] = $totals[2]['col2']+$totals[3]['col2']+$totals[4]['col2'];
			$totals[5]['col2'] = number_format($totals[5]['col2'], 2, '.', '');

			//BEGIN SALES TABLE
				$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'shaded'=>2,
						'shadeCol'=>array(0.9,0.9,0.9),
						'shadeCol2'=>array(0.77,0.87,0.91),
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezSetDy(-10);
				$pdf->ezTable($salesRaw, array(
											'card_number_sal'=>'<b>Tarjeta No.</b>',
											'name'=>'<b>Nombre</b>',
											'name_pbl'=>'<b>Tipo de Tarjeta</b>',
											'total_sal'=>'<b>Valor</b>'),
								NULL, $options);
			//END SALES TABLE

			//BEGIN TOTALS
				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				$pdf->ezSetDy(-10);
				$textFooter=utf8_decode('Nota de Crédito por Anulación de Factura');
				$pdf->ezText($textFooter, 8, array('justification' =>'center'));
			//END TOTALS
		}else{//If it's a REFUND credit note
			$sale=new Sales($misc['db'], $cNoteInfo['serial_sal']);
			$sale->getData();
			$customer=new Customer($misc['db'], $sale->getSerial_cus());
			$customer->getData();
			$cutomer_name=$customer->getFirstname_cus().' '.$customer->getLastname_cus();
			$card_number=$sale->getCardNumber_sal();
			$refund=new Refund($misc['db'], $cNoteInfo['serial_ref']);
			$refund->getData();

			$salesRaw=$invoice->getInvoiceSales($cNoteInfo['serial_inv'], $sale->getSerial_sal());

			//BEGIN SALES TABLE
				$totals[0]['col2'] = $sale->getTotal_sal();

				if($refund->getRetainedType_ref()=='PERCENTAGE'){
					$totals[1]['col1'] = 'Penalidad ('.$refund->getRetainedValue_ref().'%):';
					$totals[1]['col2'] = $totals[0]['col2']*($refund->getRetainedValue_ref()/100);
					$totals[1]['col2'] = number_format($totals[1]['col2'], 2, '.', '');
				}else{
					$totals[1]['col1'] = 'Penalidad (Valor Fijo):';
					$totals[1]['col2'] = $refund->getRetainedValue_ref();
				}

				if($refund->getType_ref()=='REGULAR'){
					$totals[2]['col1'] = 'Descuento ('.$invoice->getDiscount_prcg_inv().'%):';
					$totals[2]['col2'] = $totals[0]['col2']*($invoice->getDiscount_prcg_inv()/100);
					$totals[2]['col2'] = number_format($totals[2]['col2'], 2, '.', '');

					$totals[3]['col1'] = 'Otro descuento ('.$invoice->getOther_dscnt_inv().'%):';
					$totals[3]['col2'] = $totals[0]['col2']*($invoice->getOther_dscnt_inv()/100);
					$totals[3]['col2'] = number_format($totals[3]['col2'], 2, '.', '');
				}

				$totals[4]['col1'] = 'Subtotal:';
				$totals[4]['col2'] = $totals[0]['col2']-($totals[1]['col2']+$totals[2]['col2']+$totals[3]['col2']);
				$totals[4]['col2'] = number_format($totals[4]['col2'], 2, '.', '');

				$taxesArr=unserialize($salesRaw[0]['applied_taxes_inv']);
				$taxes=0;
				if(is_array($taxesArr)){
					foreach($taxesArr as $t){
							$taxes+=$t['tax_percentage'];
					}
				}
				$totals[5]['col1'] = 'Impuestos ('.$taxes.'%):';
				$totals[5]['col2'] = $taxes*$totals[4]['col2']/100;
				$totals[5]['col2'] = number_format($totals[5]['col2'], 2, '.', '');

				$totals[6]['col1'] = 'Valor Total:';
				$totals[6]['col2'] = $totals[4]['col2']+$totals[5]['col2'];
				$totals[6]['col2'] = number_format($totals[6]['col2'], 2, '.', '');

				//Debug::print_r($totals);


				$options = array(
						'showLines'=>2,
						'showHeadings'=>1,
						'shaded'=>2,
						'shadeCol'=>array(0.9,0.9,0.9),
						'shadeCol2'=>array(0.77,0.87,0.91),
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>'center',
						'xOrientation'=>'center',
						'maxWidth' => 440,
						'cols' =>array(
							  'card_number_sal'=>array('justification'=>'center','width'=>70),
							  'name'=>array('justification'=>'left','width'=>190),
							  'name_pbl'=>array('justification'=>'center','width'=>105),
							  'total_sal'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezSetDy(-10);
				$pdf->ezTable($salesRaw, array(
											'card_number_sal'=>'<b>Tarjeta No.</b>',
											'name'=>'<b>Nombre</b>',
											'name_pbl'=>'<b>Tipo de Tarjeta</b>',
											'total_sal'=>'<b>Valor</b>'),
								NULL, $options);
			//END SALES TABLE

			//BEGIN TOTALS
				$options = array(
						'showLines'=>2,
						'showHeadings'=>0,
						'shaded'=>0,
						'fontSize' => 8,
						'textCol' =>array(0,0,0),
						'titleFontSize' => 8,
						'rowGap' => 5,
						'colGap' => 5,
						'lineCol'=>array(0.8,0.8,0.8),
						'xPos'=>433,
						'xOrientation'=>'center',
						'maxWidth' => 180,
						'cols' =>array(
							  'col1'=>array('justification'=>'right','width'=>105),
							  'col2'=>array('justification'=>'right','width'=>75)
						),
						'innerLineThickness' => 0.8,
						'outerLineThickness' => 0.8
				);

				$pdf->ezTable($totals, array('col1'=>'Col1','col2'=>'Col2'), NULL, $options);

				$pdf->ezSetDy(-10);
				$textFooter=utf8_decode('Nota de Crédito de Reembolso por: ').$global_refundTypes[$refund->getType_ref()];
				$pdf->ezText($textFooter, 8, array('justification' =>'center'));
			//END TOTALS
		}


		//$pdf->ezStream();
		$pdfcode = $pdf->ezOutput();
		$fp=fopen('pdfReports/credit_note_'.$misc['cNoteID'].'.pdf','wb');
		fwrite($fp,$pdfcode);
		fclose($fp);
	}

	/*
	 * @name: deleteFile
	 * @Description: Deletes an specific file acording to its location.
	 * @Params: $urlAndFile: The path of the document
	 * @Creator: Mario Lopez
	 */
	public static function deleteFile($urlAndFile){
		return unlink($urlAndFile);
	}

	/*
	 * @Name: generateCreditNote
	 * @Description: generates a Credit note for a refund or a void invoice.
	 * @Params: An array of variables with the following structure
	 *          ['type']:  REFUND/INVOICE
	 *			['serial']: The serial of the refund or invoice according to it's type.
	 *          ['serial_mbc']: The serial of the manager to take the current number of the Credit Note.
	 *          ['serial_dbc']: The serial of the document used.
	 *			['serial_cou']: The coutnry of the current manager.
	 *			['person_type']: The type of person to whom the credit note will be delivered.
	 *			['amount']: The amount for the credit Note.
	 *          ['db']: the Conection of the DB.
	 * @Returns: TRUE if OK
	 *           FALSE otherwise.
	 */
	public static function generateCreditNote($misc){
		//Get the Document_by_country serial to use.
		$serial_dbm = DocumentByManager::retrieveSelfDBMSerial($misc['serial_mbc'], $misc['person_type'], $misc['type']);
		$number = DocumentByManager::retrieveNextDocumentNumber($misc['serial_mbc'], $misc['person_type'], $misc['type']);
		
		if($number){
			$cNote = new CreditNote($misc['db']);
			if($misc['type']=='REFUND'){
				$cNote->setSerial_ref($misc['serial']);
			}else{
				$cNote->setSerial_inv($misc['serial']);
			}
			$cNote->setStatus_cn('PAID');
			$cNote->setAmount_cn($misc['amount']);
			$cNote->setNumber_cn($number);
			$cNoteID = $cNote -> insert();
			
			if($cNoteID){
				if($number==1){
					//TODO DO WE HAVE TO INSERT OR A DBM IS ALWAYS SUPPOSED TO EXIST??
					
					if($documentnumber->insert()){
						$cNoteLog['error']='success';
						$cNoteLog['id']=$cNoteID;
						$cNoteLog['number']=$number;
						return $cNoteLog;
					}else{
						$cNoteLog['error']='insert_error';
						$cNoteLog['id']=$cNoteID;
						$cNoteLog['number']=$number;
						return $cNoteLog;
					}
				}else{
					if(DocumentByManager::moveToNextValue($serial_dbm)){
						$cNoteLog['error'] = 'success';
						$cNoteLog['id'] = $cNoteID;
						$cNoteLog['number'] = $number;
						return $cNoteLog;
					}else{
						$cNoteLog['error'] = 'update_error';
						$cNoteLog['id'] = $cNoteID;
						$cNoteLog['number'] = $number;
						return $cNoteLog;
					}
				}
			}else{
				$cNoteLog['error']='cNote_error';
				return $cNoteLog;
			}
		}else{
			$cNoteLog['error']='no_document';
			return $cNoteLog;
		}
	}
        
    public static function PDFContract($data, $serial_sal, $db){
        include(DOCUMENT_ROOT.'salesContractPDF.php');
    }
    
	///////////////////////////
    //	ESTIMATOR GLOBALS	 //
    ///////////////////////////
    
	public static function productCalculatePrice($db,	$productBasePrice,
														$productBaseCost,
														$adultsFree,
														$adultPercentage,
														$adultsTraveling,
														$childrenFree,
														$childrenPercentage,
														$childrenTraveling,
														$spouseTraveling,
														$spousePercentage,
														$maxExtras,
														$extrasRestricted)
			{
		//FORMULAS:
		$spouseBasePrice = ($productBasePrice * (100-$spousePercentage))/100;
		$spouseCost = ($productBaseCost * (100-$spousePercentage))/100;
			
		$adultSinglePrice = ($productBasePrice * (100-$adultPercentage))/100;
		$adultSingleCost = ($productBaseCost * (100-$adultPercentage))/100;
		
		$childSinglePrice = ($productBasePrice * (100-$childrenPercentage))/100;
		$childSingleCost = ($productBaseCost * (100-$childrenPercentage))/100;
		
		
		//CALCULATE THE AMOUNT OF CARDS AND THE CARD VACANCY --SPECIAL CONSIDERATION FOR 'EXTRAS CHILDREN ONLY' CARDS:
		$numCards = GlobalFunctions::getNumberOfCards($maxExtras,$adultsTraveling, $childrenTraveling, $extrasRestricted);
		$cardVacancy = GlobalFunctions::getCardVacancy($maxExtras);
		
		
		//Get Cards Layout:
		$cardsDistribution = WSGlobalFunctions::getCardsTravelersDistribution($maxExtras, $adultsTraveling, $childrenTraveling, $spouseTraveling,
																				$extrasRestricted, $adultsFree, $childrenFree);
		
		$productTotalPrice = 0;
		foreach($cardsDistribution as $cardD){
			$cardTotalPrice = 0;
			foreach($cardD as $traveler){
				$travelerPrice = 0;
				switch($traveler){
					case 'H':
						$travelerPrice = $productBasePrice;
						break;
					case 'C':
						$travelerPrice = $childSinglePrice;
						break;
					case 'A':
						$travelerPrice = $adultSinglePrice;
						break;
					case 'S':
						$travelerPrice = $spouseBasePrice;
						break;
					default: //case 'F' - Free
						$travelerPrice = 0;
						break;
				}
				$cardTotalPrice += $travelerPrice;
			}
			$productTotalPrice += $cardTotalPrice;
		}
		
    	$priceArray = array(
	    	'holderPrice' => $productBasePrice,
	    	'childrenPrice' => $childSinglePrice,
	    	'spousePrice' => $spouseBasePrice,
	    	'extrasPrice' => $adultSinglePrice,
    	
    		'holderCost' => $productBaseCost,
	    	'childrenCost' => $childSingleCost,
	    	'spouseCost' => $spouseCost,
	    	'extrasCost' => $adultSingleCost,
    	
    		'numCards' => $numCards,
    		'totalPrice' => $productTotalPrice
		);
    	
//echo 'holder price: '.$productBasePrice.' <br/>';
//echo 'spouse price: '.$spousePrice.' <br/>';
//echo 'adults price: '.$adultExtrasPrice.' <br/>';
//echo 'children price: '.$childrenExtrasPrice.' <br/>';
//echo 'holder cost: '.$productBaseCost.' <br/>';
//echo 'spouse cost: '.$spouseCost.' <br/>';
//echo 'adults single cost: '.$adultSingleCost.' <br/>';
//echo 'children single cost: '.$childSingleCost.' <br/>';
//echo 'total price: '.$productTotalPrice.' <br/>';
		
		return $priceArray;
    }
    
	public static function getCardsTravelersDistribution($maxExtras, $adultsNum, $childrenNum, $spouseTraveling, 
														 $extrasRestricted, $adultsFree, $childrenFree){
    	//RETURNS AN ARRAY OF CARDS WITH THE FOLLOWING CODING:
    	//H - HOLDER
    	//C- CHILD
    	//A - ADULT
    	//S - SPOUSE
    	//F - FREE
    	$spouseTraveling =0;
    	$cardsContent = array();
		$numberOfCards = WSGlobalFunctions::getNumberOfCards($maxExtras, $adultsNum, $childrenNum, $extrasRestricted);
		$cardVacancy = WSGlobalFunctions::getCardVacancy($maxExtras);
		//echo $numberOfCards; echo $cardVacancy; die();
		for($i = 0; $i < $numberOfCards; $i++){
			//POPULATE THE CARD: PRIORITIES ARE: SPOUSE, ADULTS, CHILDREN - UNLESS RESTRICTED -
			$cardsContent[$i] = array();
			$cardPeople = 0;
			$cardPlaceIndex = 0;
			$adultsCardFree = $adultsFree;
			$childrenCardFree = $childrenFree;
			
			$cardsContent[$i][$cardPlaceIndex++] = ('H');
			$cardPeople++;
			if($adultsNum > 0){$adultsNum --;}else{$childrenNum--;}
			
			//IF NO EXTRAS RESTRICTION APPLIES, CONTINUE FILLING THE ADULTS:
			if($extrasRestricted != 'CHILDREN'){
				
				// !! If spouse is checked but extrasRestriction applies,									!! 
				// !! spouse section will not be reached even though more than one adult is traveling		!!
				// !! This is normal since in this case the only option for the spouse is to be a holder	!!
				if($spouseTraveling && $adultsNum > 0){//Log spouse					
                                    if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'S';
					}else{//Travels for free
						$cardsContent[$i][$cardPlaceIndex++] = 'F';
					}
					$cardPeople++;
					$adultsNum--;
				} 
				//Fill the children
                                while($cardPeople < $cardVacancy && $childrenNum > 0){
				if($childrenCardFree-- <= 0){
					$cardsContent[$i][$cardPlaceIndex++] = 'C';
				}else{//Travels for free
					$cardsContent[$i][$cardPlaceIndex++] = 'F';
				}
				$cardPeople++;
				$childrenNum--;
			}
				//Fill the other adults
				while($cardPeople < $cardVacancy && $adultsNum > 0){
					if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'A';
					}else{//Travels for free
						$cardsContent[$i][$cardPlaceIndex++] = 'F';
					}
					$cardPeople++;
					$adultsNum--;
				}
			}
			
			
		}
		
		//Debug:
		//echo '<pre>';print_r($cardsContent);echo '</pre>';
		return $cardsContent;
    }
    public static function getCardsTravelersDistribution2($maxExtras, $adultsNum, $childrenNum, $spouseTraveling, 
														 $extrasRestricted, $adultsFree, $childrenFree){
    	//RETURNS AN ARRAY OF CARDS WITH THE FOLLOWING CODING:
    	//H - HOLDER
    	//C- CHILD
    	//A - ADULT
    	//S - SPOUSE
    	//F - FREE
    	
    	$cardsContent = array();
       
                
		$numberOfCards = WSGlobalFunctions::getNumberOfCards($maxExtras, $adultsNum, $childrenNum, $extrasRestricted);	
                $cardVacancy = WSGlobalFunctions::getCardVacancy($maxExtras);
		//echo $numberOfCards; echo $cardVacancy; die();
		for($i = 0; $i < $numberOfCards; $i++){
			//POPULATE THE CARD: PRIORITIES ARE: SPOUSE, ADULTS, CHILDREN - UNLESS RESTRICTED -
			$cardsContent[$i] = array();
			$cardPeople = 0;
			$cardPlaceIndex = 0;
			$adultsCardFree = $adultsFree;
			$childrenCardFree = $childrenFree;
			$maxChildrenFree = $childrenCardFree;
                        
			$cardsContent[$i][$cardPlaceIndex++] = 'H';
			$cardPeople++;
			if($adultsNum > 0){$adultsNum --;}else{$childrenNum--;}
			
			//IF NO EXTRAS RESTRICTION APPLIES, CONTINUE FILLING THE ADULTS:
			if($extrasRestricted != 'CHILDREN'){
				
				// !! If spouse is checked but extrasRestriction applies,									!! 
				// !! spouse section will not be reached even though more than one adult is traveling		!!
				// !! This is normal since in this case the only option for the spouse is to be a holder	!!
				if($spouseTraveling && $adultsNum > 0){//Log spouse
					if($adultsCardFree-- <= 0){
						$cardsContent[$i][$cardPlaceIndex++] = 'S';
                                                
					}else{//Travels for free
						$cardsContent[$i][$cardPlaceIndex++] = 'F';
                                                
					}
					$cardPeople++;
					$adultsNum--;
				} 
				
                                //Fill the children
                                while($cardPeople < $cardVacancy && $maxChildrenFree > 0 && $childrenNum > 0){
                                        if($childrenCardFree-- >= 0){
                                                $cardsContent[$i][$cardPlaceIndex++] = 'F';

                                        }/*else{//Travels for free
                                                $cardsContent[$i][$cardPlaceIndex++] = 'Children';

                                        }*/
                                        $cardPeople++;
                                        $childrenNum--;
                                        $maxChildrenFree--;
                                        
                                }
				//Fill the other adults
                                if ($childrenNum <= 0){
                                    while($cardPeople < $cardVacancy && $adultsNum > 0 ){
                                            if($adultsCardFree-- <= 0){
                                                    $cardsContent[$i][$cardPlaceIndex++] = 'A';

                                            }else{//Travels for free
                                                    $cardsContent[$i][$cardPlaceIndex++] = 'F';

                                            }
                                            $cardPeople++;
                                            $adultsNum--;
                                    }
                                }
			}
			
			
		}
		
		//Debug:
		//echo '<pre>';print_r($cardsContent);echo '</pre>';
                
		return $cardsContent;
    }
    
    
 	public static function getNumberOfCards($maxExtras, $adults, $children, $extrasRestricted){
        
        /*    if ($extrasRestricted == 'FREE')
        {
            $var = fmod($children,$adults);
            if ($var == 0){
                $anyoneAllowedInEachCard = $adults; 
                return $anyoneAllowedInEachCard;
            }
            if ($adults < $children){
                $total = $adults+$children;
                while ($children > 0):{
                  $children = $children-$totalkidsbyproduct;
                  if ($children >= 0){
                  $free = $free+$totalkidsbyproduct;}
                  elseif($totalKids < 0 )  {$free++;}
                  if ($totalKids > 1 && $totalAdults == 0) {$totalAdults++; $totalKids--;}     
                  elseif ($totalKids == 0 && $totalAdults > 0) {$totalcard = $totalcard+$totalAdults;}
                  $totalcard++;
                    }                   
                endwhile;
            }
        }
        */
        $anyoneAllowedInEachCard = ceil(($adults + $children) / (WSGlobalFunctions::getCardVacancy($maxExtras))); //Adults & Children allowed indistinctively
    	$onlyOneAdultInEachCard = $adults;    	 
    	if($extrasRestricted == 'CHILDREN'){
    		return max($anyoneAllowedInEachCard, $onlyOneAdultInEachCard);
    	}
        
        return $anyoneAllowedInEachCard;
      
    }
    
	public static function getCardVacancy($maxExtras){
		return $maxExtras + 1;//Card vacancy is max extras plus the holder
    }
    
    public static function assignProductCalculatedPrices($db,&$singleProduct,$selTripDays, $extrasRestricted){
    	//Default Product Split:
    	$departureCountry = $singleProduct['departure'];
    	$targetDays = $singleProduct['days'];
    	$txtOverAge = $singleProduct['adults'];
    	$txtUnderAge = $singleProduct['children'];
    	$checkPartner = $singleProduct['spouseTraveling'];
    	
    	//Initialize
    	$productExtraDays = 0;
    	$productBasePrice = 0;
    	$productBaseCost = 0;
		
		//GET ALL THE "PRICES per DURATION" AVAILABLE (COUNTRY SPECIFIC OR ALL COUNTRY PRICES):
		//**************************************** FIX ONLY FOR WEB SALES *****************************************************
		//WE NEED THE PRODUCT:
		$productByCountry = new ProductByCountry($db, $singleProduct['serial_pxc']);
		$productByCountry -> getData();
		
		$product = new WSProduct($db, $productByCountry -> getSerial_pro());
		$product -> getData();
		
		//IF THERE IS A PXC FOR THE DEPARTURE COUNTRY WE WANT THOSE PRICES BUT WE STILL USE THE PXC FOR COUNTRY 1 FOR THE REST OF THE SALE:
		$departurePXC = ProductByCountry::getPxCSerial($db, $departureCountry, $product -> getSerial_pro());
                $productByCountry = new ProductByCountry($db, $departurePXC);
                if(!$productByCountry -> getData()){
    		$productByCountry -> setSerial_pxc($singleProduct['serial_pxc']);
    		$productByCountry -> getData();
    	}
    	
    	//**************************************** END ONLY FOR WEB SALES *****************************************************
    	
	    $serialCountry = $productByCountry -> getSerial_cou();		
		if(!WSPriceByProductByCountry::pricesByProductExist($db, $productByCountry -> getSerial_pxc())){
			global $webSalesDealer;
			$serialCountry = Dealer::getDealersCountrySerial($db, $webSalesDealer);
		}
		
		$filterDays = NULL;
		if(!$selTripDays){
			$filterDays = $targetDays;
		}
		
		$priceObject= new WSPriceByProductByCountry($db);
		$prices = $priceObject -> getPricesByProductByCountry($productByCountry -> getSerial_pro(), $serialCountry, $filterDays);
		
		//IF THERE IS NOT A SUITABLE RANGE OF PRICES FOR THE AMOUNT OF DAYS SELECTED
//		if(!$prices){
//			return "NO_PRICES";
//		}
		
		
		//CHOOSE THE BEST "PRICES per DURATION" (DURATION_PPC) TO APPLY, ACCORDING TO THE DAYS OF THE TRIP (TARGET DAYS):
		$closestDuration = 0;
		
		if(is_array($prices)){
			if($selTripDays && $product -> getShow_price_pro() == 'YES'){ //WHEN WE HAVE SELTRIPDAYS
				foreach($prices as $p){
					$currentDuration = $p['duration_ppc'];
					if($currentDuration >= $selTripDays){
						$closestDuration = $currentDuration;
						$closestDurationPrice = $p['price_ppc'];
						$closestDurationCost = $p['cost_ppc'];
						break;
					}
				}
				//WE SHOULD ALWAYS GET A CORRECT DURATION SINCE TRIP DAYS VALIDATOR TOOK CARE OF IT
				$productExtraDays = 0;
				$productBasePrice = $closestDurationPrice;
				$productBaseCost = $closestDurationCost;
			}else{
				foreach($prices as $p){
					$currentDuration = $p['duration_ppc'];
					if(	($currentDuration <= $targetDays && $currentDuration + 10 >= $targetDays)//IS THIS DURATION ELIGIBLE?? [EQUAL OR LOWER BUT WITHIN RANGE]
						|| ($currentDuration > $targetDays && $closestDuration == 0)){ //IS THE SUPERIOR OUR BEST SHOT?, THEN USE IT
							$closestDuration = $currentDuration;
							$closestDurationPrice = $p['price_ppc'];
							$closestDurationCost = $p['cost_ppc'];
					}
				}
				//WE SHOULD ALWAYS GET A CORRECT DURATION SINCE TRIP DAYS VALIDATOR TOOK CARE OF IT
				$productExtraDays = $targetDays - $closestDuration;
				$productBasePrice = $closestDurationPrice;
				$productBaseCost = $closestDurationCost;
			}
		} else{
			$serial_ppc = $priceObject -> getMaxPricePosible($productByCountry->getSerial_pro(), $filterDays, $serialCountry);

			if($serial_ppc){
				$priceObject -> setSerial_ppc($serial_ppc);
				$priceObject -> getData();
				$priceObject = get_object_vars($priceObject);

				$closestDurationPrice = $priceObject['price_ppc'];
				$closestDurationCost = $priceObject['cost_ppc'];
				$closestDurationRangeMax = $priceObject['max_ppc'];
				$closestDurationRangeMin = $priceObject['min_ppc'];
				$closestDurationPXC = $priceObject['serial_pxc'];

				$closestDurationPrice = number_format(round($closestDurationPrice, 2), 2, '.', '');
				$fixedPrice = number_format(round($closestDurationPrice * $change_fee, 2), 2, '.', '');

				$closestDuration = $priceObject['duration_ppc'];
				$productExtraDays = $targetDays - $closestDuration;
				$productBasePrice = $closestDurationPrice;
				$productBaseCost = $closestDurationCost;

			}else{//If there's not a fee, the operator can't make the sale for tha amount of days given.
				return "NO_PRICES";
			}
		}//END OF ALGORYTHM
		
		//////////////////////////////////
		// 	WE NOW CALCULATE THE PRICE	//
		//////////////////////////////////
		
		//FIRST DECALRE SOME USEFUL VARIABLES: 
		$maxExtras = $product -> getMax_extras_pro();
		$singleProduct['max_extras'] = $maxExtras;
		$spouseTraveling = $checkPartner == 'true' ? true : false;
		
		//IF PRICE IS PER DAY, THEN MULTIPLY: -- DISABLED --
		if($product -> getPrice_by_day_pro() == 'YES'){
			//$productBasePrice *= $targetDays;
		}
		
		//WE GET THE PRICE PER EXTRA DAY:
		$productExtraDaysPrice = 0;
		if($productExtraDays > 0){
			$productExtraDaysPrice = $productByCountry -> getAditional_day_pxc();
			$productBasePrice += ($productExtraDays*$productExtraDaysPrice);
			$productBaseCost += ($productExtraDays*$productExtraDaysPrice);
		}
		
		$prices = WSGlobalFunctions::productCalculatePrice($db,	$productBasePrice,
																$productBaseCost,
																$product -> getAdults_pro(),
																$productByCountry -> getPercentage_extras_pxc(),
																$txtOverAge,
																$product -> getChildren_pro(),
																$productByCountry -> getPercentage_children_pxc(),
																$txtUnderAge,
																$spouseTraveling,
																$productByCountry -> getPercentage_spouse_pxc(),
																$maxExtras,
																$extrasRestricted);
		//Debug:
		//echo 'final: <pre>';print_r($prices);echo '</pre>';
		
		//PRODUCT PRICE ASSIGNMENT:
	$singleProduct['price_pod'] = $prices['holderPrice'];
    	$singleProduct['price_children_pod'] = $prices['childrenPrice'];
    	$singleProduct['price_spouse_pod'] = $prices['spousePrice'];
    	$singleProduct['price_extra_pod'] = $prices['extrasPrice'];
																
    	$singleProduct['cost_pod'] = $prices['holderCost'];
    	$singleProduct['cost_children_pod'] = $prices['childrenCost'];
    	$singleProduct['cost_spouse_pod'] = $prices['spouseCost'];
    	$singleProduct['cost_extra_pod'] = $prices['extrasCost'];
    	
    	$singleProduct['cards_pod'] = $prices['numCards'];
    	$singleProduct['total_price_pod'] = $prices['totalPrice'];
    	$singleProduct['days'] = $closestDuration + $productExtraDays;
		
        return TRUE;
    }
     
	public static function saveCartProductToDB($db, $sessProd, $customerSerial){
		//We first check if there is a matching pre_order for this customer:
		$orderId = PreOrder::getMyPreOrderSerial($db,$customerSerial);
		if(!$orderId){
			$preOrder = new PreOrder($db,NULL,$customerSerial,'CUSTOMER',time());
			$orderId = $preOrder -> insert();
		}
	
		if($orderId){
			$preOrderDetail = new PreOrderDetail($db,NULL);
			$preOrderDetail -> setName_pro($sessProd['name_pbl']);
			$preOrderDetail -> setSerial_por($orderId);
			$preOrderDetail -> setSerial_cou($sessProd['destination']);
			$preOrderDetail -> setDeparture_cou($sessProd['departure']);
			$preOrderDetail -> setSerial_pxc($sessProd['serial_pxc']);
			$preOrderDetail -> setBegin_date_pod($sessProd['beginDate']);
			$preOrderDetail -> setEnd_date_pod($sessProd['endDate']);
			$preOrderDetail -> setDays_pod($sessProd['days']);
			$preOrderDetail -> setAdults_pod($sessProd['adults']);
			$preOrderDetail -> setChildren_pod($sessProd['children']);
			$preOrderDetail -> setSpouse_pod($sessProd['spouseTraveling']=='true'?1:0);
			
			$preOrderDetail -> setPrice_pod($sessProd['price_pod']);
			$preOrderDetail -> setPrice_children_pod($sessProd['price_children_pod']);
			$preOrderDetail -> setPrice_spouse_pod($sessProd['price_spouse_pod']);
			$preOrderDetail -> setPrice_extra_pod($sessProd['price_extra_pod']);
			
			$preOrderDetail -> setCost_pod($sessProd['cost_pod']);
			$preOrderDetail -> setCost_children_pod($sessProd['cost_children_pod']);
			$preOrderDetail -> setCost_spouse_pod($sessProd['cost_spouse_pod']);
			$preOrderDetail -> setCost_extra_pod($sessProd['cost_extra_pod']);
			
			$preOrderDetail -> setTotal_Price_pod($sessProd['total_price_pod']);
		  	$preOrderDetail -> setCards_pod($sessProd['cards_pod']);
			
			if(!$preOrderDetail -> insert()){
				return 3;
			}
		}else{
			return 2;
		}
		return 5;
	}
	
	//****************************************************************************************************************************
	//****************************************  pPrintSalePDF.php MOVED FUNCTIONS  ***********************************************
	//****************************************************************************************************************************
	
	public static function header1($pdf, $dataInfo, $langCode){
		global $saleContract;
		global $date;
		global $leftMostX;
		global $blue;
		global $lightBlue;
		global $gray;
		global $red;
		global $black;
		global $smallTextSize;
		global $medTextSize;
		global $stdTextSize;
		global $largeTextSize;
		
		$pdf -> setColor($gray[0],$gray[1],$gray[2]);
		$pdf -> addTextWrap($leftMostX,807,350,$largeTextSize,''.$date);
	
		$widthBranch=$pdf -> getTextWidth($stdTextSize,$saleContract['header1']['branch']);
		$widthCounter=$pdf -> getTextWidth($stdTextSize,$saleContract['header1']['counter']);
	
		$pdf -> setColor($black[0], $black[1], $black[2]);
		$pdf -> addTextWrap($widthBranch+70,793,538,$stdTextSize,''.$dataInfo['name_dea']);//dealer
		$pdf -> addTextWrap($widthCounter+70,778,538,$stdTextSize,''.$dataInfo['counter']);//counter
	
		$pdf -> setColor($blue[0],$blue[1],$blue[2]);
		$pdf -> addTextWrap($leftMostX,793,350,$stdTextSize,'<b>'.$saleContract['header1']['branch'].'</b>');
		$pdf -> addTextWrap($leftMostX,778,350,$stdTextSize,'<b>'.$saleContract['header1']['counter'].'</b>');
	
		$pdf -> setLineStyle(1.4,'square');
		$pdf -> setStrokeColor($gray[0],$gray[1],$gray[2]);
		$pdf -> line($leftMostX - 5,768,$leftMostX + 528,768);
	}

	//Calculate age function
	public static function calculateAge($birthDate){
		list($year, $month, $day) = explode("/", $birthDate);
		$year_diff  = date("Y") - $year;
		$month_diff = date("m") - $month;
		$day_diff   = date("d") - $day;
		if ($month_diff < 0) $year_diff--;
		elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
		return $year_diff;
	}

	//Display Extras Function 
	public static function displayExtras($pdf,$data,$verticalPointer,$band,$dataInfo, $langCode){
		global $saleContract;
		global $global_extraRelationshipType;
		global $leftMostX;
		global $blue;
		global $lightBlue;
		global $gray;
		global $red;
		global $black;
		global $smallTextSize;
		global $medTextSize;
		global $stdTextSize;
		global $largeTextSize;
		$cont=1;
		
		if($band==2){
			$pdf -> ezNewPage();
			GlobalFunctions::header1($pdf, $dataInfo, $langCode);
			$verticalPointer = 755;
		}
		
		//Extras title
		$pdf -> setStrokeColor($lightBlue[0],$lightBlue[1],$lightBlue[2]);
		$pdf -> setLineStyle(16);
		$pdf -> line($leftMostX + 2,$verticalPointer,$leftMostX + 519,$verticalPointer);
		$pdf -> setColor($red[0],$red[1],$red[2]);
		$pdf -> addTextWrap($leftMostX,$verticalPointer - 4,200,$largeTextSize,'<b>'.$saleContract['client']['extras'].'</b>');
	
		//Table header:
		$verticalPointer -= 20;
		$tempX = $leftMostX;
		$pdf -> setColor($gray[0],$gray[1],$gray[2]);
		$pdf -> addTextWrap($tempX + 35,$verticalPointer,547,$stdTextSize,'     '.$saleContract['client']['table']['0'].'                                               '.
							$saleContract['client']['table']['1'].'                                                                '.
							$saleContract['client']['table']['2'].'                    '.
							$saleContract['client']['table']['3']);
		
		//Extras content
		$verticalPointer -= 15;
		if($data){
			$pdf -> setColor($black[0], $black[1], $black[2]);
			foreach($data as $d){
				//Document:
				$pdf -> addTextWrap($tempX,$verticalPointer,120,$medTextSize,''.$d['document_cus'],'center');

				//Name:
				$fullName = $d['first_name_cus'].' '.$d['last_name_cus'];
				GlobalFunctions::writeTextField($pdf, $tempX + 120, $verticalPointer + 5, $medTextSize, $fullName, 240);
				
				//Age
				list($day,$month,$year) = explode("/", $d['birthdate_cus']);
				$d['birthdate_cus']=$year.'/'.$month.'/'.$day;
				$age = GlobalFunctions::calculateAge($d['birthdate_cus']);
				$pdf -> addTextWrap($tempX + 360,$verticalPointer,80,$medTextSize,$age,'center');

				//Fee
				$pdf -> addTextWrap($tempX + 440,$verticalPointer,88,$medTextSize,number_format($d['fee_ext'], 2, '.', ''),'center');
	
				//lines
				$pdf -> setLineStyle(0.2,'square');
				$pdf -> setStrokeColor($gray[0],$gray[1],$gray[2]);
				$pdf -> line($tempX,$verticalPointer - 2, $tempX + 528,$verticalPointer - 2);
				$pdf -> line($tempX,$verticalPointer + 10, $tempX,$verticalPointer - 2);
				$pdf -> line($tempX + 120,$verticalPointer + 10 , $tempX + 120,$verticalPointer - 2);
				$pdf -> line($tempX + 360,$verticalPointer + 10, $tempX + 360,$verticalPointer - 2);
				$pdf -> line($tempX + 440,$verticalPointer + 10, $tempX + 440,$verticalPointer - 2);
				$pdf -> line($tempX + 528,$verticalPointer + 10, $tempX + 528,$verticalPointer - 2);
	
				$verticalPointer -= 20;
				if($verticalPointer <= 100){
					$pdf -> ezNewPage();
					$verticalPointer = 755;
				}
			}
		}
		$pdf -> setColor($gray[0],$gray[1],$gray[2]);
		return $verticalPointer;
	}
	
	public static function writeTextField(&$pdf, $xValue, $verticalPointer, $textSize, $string, $maxWidth){
		$stringWidth = $pdf -> getTextWidth($textSize, $string);
		
		if($stringWidth <= $maxWidth){
			$pdf -> addTextWrap($xValue,$verticalPointer - 5, $maxWidth, $textSize,''.$string,'center');
		}
		else{
			$remainder = $pdf -> addTextWrap($xValue,$verticalPointer, $maxWidth, $textSize,''.$string,'center');
			$pdf -> addTextWrap($xValue, $verticalPointer - 8, $maxWidth, $textSize,''.$remainder);
		}
	} 

	public static function strToTimeDdMmYyyy($dateDdMmYyyy){
		if(!$dateDdMmYyyy) return 0;
		$partsDdMmYyyy = split('[/.-]', $dateDdMmYyyy);
		$aux=explode(' ', $partsDdMmYyyy[2]);
		$partsDdMmYyyy[2]=$aux[0];
		
		return strtotime($partsDdMmYyyy[2] . '/' . $partsDdMmYyyy[1] . '/' . $partsDdMmYyyy[0]);
	}
	
	/**
     * @name changeDateFormat
     * @param $date YYYY-mm-dd
     * @return string dd/mm/YYYY
     */
    public static function changeDateFormat($date) {
        $explode_date = explode('-', $date);

        return $explode_date['2'] . '/' . $explode_date['1'] . '/' . $explode_date['0'];
    }

    /**
     * @name getCurrentIp
     * @return string ip
     */
    public static function getCurrentIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function validate_ruc($document, $ruc_only = FALSE) {
        $document = str_split($document);

        if ($document[2] == 9) {
			return self::validate_ruc_9($document);
        } elseif($document[2] == 6) {
            return self::validate_ruc_6($document);
        } elseif($document[2] < 6) {
			if($ruc_only){
				if(sizeof($document) != 13){
					return false;
				}
			}
			unset($document[10]);
			unset($document[11]);
			unset($document[12]);
			
			return self::validate_ci(implode('', $document));
		} else{
			return false;
		}
    }

    public static function validate_ruc_9($document) {
        $coeficient_array = array(4, 3, 2, 7, 6, 5, 4, 3, 2);
        $module11 = 11;
        $suma = 0;

        foreach ($coeficient_array as $key => $c) {
            $suma += $c * $document[$key];
        }

        $mode_digit = $suma % $module11;
		
		if($mode_digit != 0):
			$digit_to_verify = $module11 - $mode_digit;
		else:
			$digit_to_verify = $mode_digit;
		endif;

        $digit_to_verify = ($digit_to_verify == 10) ? 0 : $digit_to_verify;

        if ($digit_to_verify == $document[9])
            return true;
        else
            return FALSE;
    }

    public static function validate_ruc_6($document) {
        $coeficientes = array(3, 2, 7, 6, 5, 4, 3, 2);

        $sumatoria = 0;
        $modulo = 0;
        $digito = 0;
        $sustraendo = 0;

        foreach ($coeficientes as $key => $c) {
            $sumatoria += $c * $document[$key];
        }

        $v9 = $document[8];

        $modulo = $sumatoria % 11;
        if($modulo == 0){
            $digito = 0;
        }else{
            $digito = 11 - $modulo;
        }
        
        if ($digito == $v9) {
            return true;
        }else
            return false;
    }

    public static function validate_ci($document) {
        $final_digit_sum = 0;
        $valid_document = true;
        $document = str_split($document);

        foreach ($document as $key => $val) {
            $temp = 0;

            if ($key == 9) { //FINAL DIGIT
                $upper_decen = ((int) ($final_digit_sum / 10) + 1) * 10;

                $temp_dig = $upper_decen - $final_digit_sum;
                if ($temp_dig == 10) {
                    $temp_dig = 0;
                }

                if ($temp_dig != $val)
                    $valid_document = false;
                break;
            }elseif (($key + 1) % 2 == 0) { //EVEN DIGIT
                $final_digit_sum += $val;
            } else { //ODD DIGIT
                $temp = $val * 2;
                if ($temp > 9):
                    $temp -= 9;
                endif;

                $final_digit_sum += $temp;
            }
        }

        return $valid_document;
    }

    public static function isMyCard($db, $serial_sal, $serial_mbc) {
        $sql = "SELECT s.serial_sal
                FROM sales s
                JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.serial_sal = $serial_sal
                JOIN dealer b ON b.serial_dea = cnt.serial_dea AND b.serial_mbc = $serial_mbc";
        //die($sql);
        if($serial_mbc <> 1){
            $result = $db->getOne($sql);

            if ($result) {
                return TRUE;
            } else {
                return FALSE;
            }
        }else{
            return TRUE;
        }
    }
	
}
?>