<?php
/*
File: DocumentsByFile.class
Author: David Bergmann
Creation Date: 21/05/2010
Last Modified: 07/06/2010
Modified By: David Bergmann
*/

class DocumentsByFile {
    var $db;
    var $serial_dbf;
    var $serial_fle;
    var $name_dbf;
    var $comment_dbf;
    var $date_dbf;
    var $url_dbf;
    var $document_to_dbf;
    var $document_date_dbf;

    function __construct($db, $serial_dbf = NULL, $serial_fle = NULL,$name_dbf = NULL,
                         $comment_dbf = NULL, $date_dbf = NULL,$url_dbf = NULL,$document_to_dbf=NULL){
        $this -> db = $db;
        $this -> serial_dbf = $serial_dbf;
        $this -> serial_fle = $serial_fle;
        $this -> name_dbf = $name_dbf;
        $this -> comment_dbf = $comment_dbf;
        $this -> date_dbf = $date_dbf;
        $this -> url_dbf = $url_dbf;
        $this -> document_to_dbf = $document_to_dbf;
    }

    /***********************************************
    * function getData
    * gets data by serial_zon
    ***********************************************/
    function getData(){
        if($this->serial_dbf!=NULL){
            $sql = "SELECT serial_dbf, serial_fle, name_dbf, comment_dbf, date_dbf, url_dbf, 
                            document_to_dbf, document_date_dbf
                    FROM documents_by_file
                    WHERE serial_dbf='".$this->serial_dbf."'";
            //echo $sql." ";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this->serial_dbf=$result->fields[0];
                $this->serial_fle=$result->fields[1];
                $this->name_dbf=$result->fields[2];
                $this->comment_dbf=$result->fields[3];
                $this->date_dbf=$result->fields[4];
                $this->url_dbf=$result->fields[5];
                $this->document_to_dbf=$result->fields[6];
                $this->document_date_dbf=$result->fields[7];
                return true;
            }
            else
                return false;
        }else
            return false;
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert(){
        $sql="INSERT INTO documents_by_file (
                                serial_dbf,
                                serial_fle,
                                name_dbf,
                                comment_dbf,
                                date_dbf,
                                url_dbf,
                                document_to_dbf,
                                document_date_dbf
                                )
                  VALUES(NULL,
                                '".$this->serial_fle."',
                                '".$this->name_dbf."',
                                '".$this->comment_dbf."',
                                NOW(),
                                '".$this->url_dbf."',
                                '".$this->document_to_dbf."',
                                STR_TO_DATE('$this->document_date_dbf', '%d/%m/%Y')
                                );";
                                //die($sql);
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/***********************************************
    * function delete
    * deletes a register in DB
    ***********************************************/
	function delete(){
		if($this->serial_dbf) {
			if($this->deleteBenefitsByDocument() && $this->deletePaymentRequest()) {
				$sql = "DELETE FROM documents_by_file
						WHERE serial_dbf = {$this->serial_dbf}";
				$result = $this->db->Execute($sql);

				if($result){
					return true;
				}else{
					ErrorLog::log($this -> db, 'DELETE FAILED - '.get_class($this), $sql);
					return false;
				}
			}
		} else {
			return false;
		}
    }

	/***********************************************
    * function deleteBenefitsByDocument
    * deletes all benefits from a document
    ***********************************************/
	function deleteBenefitsByDocument(){
		if($this->serial_dbf) {
			$sql = "DELETE FROM benefits_by_document
					WHERE serial_dbf = {$this->serial_dbf}";
			$result = $this->db->Execute($sql);

			if($result){
				return true;
			}else{
				ErrorLog::log($this -> db, 'DELETE BENEFITS FAILED - '.get_class($this), $sql);
				return false;
			}
		} else {
			return false;
		}
	}

	/***********************************************
    * function deletePaymentRequest
    * deletes the payment request from a document
    ***********************************************/
	function deletePaymentRequest(){
		if($this->serial_dbf) {
			$sql = "DELETE FROM payment_request
					WHERE serial_dbf = {$this->serial_dbf}";
			$result = $this->db->Execute($sql);

			if($result){
				return true;
			}else{
				ErrorLog::log($this -> db, 'DELETE PRQ FAILED - '.get_class($this), $sql);
				return false;
			}
		} else {
			return false;
		}
	}

    /*
     * getDocumentsInfoByFile
     * @param: $db - DB connection
     * @param: $serial_fle - File ID
     * @return: an array of documents uploaded to a specific file.
     */
    public static function getDocumentsInfoByFile($db, $serial_fle, $serial_spv=NULL, $non_paid_only = FALSE){
        $sql=  "SELECT DISTINCT dbf.serial_dbf, prq.serial_prq, dbf.serial_fle, dbf.name_dbf,  dbf.comment_dbf,
                       DATE_FORMAT(dbf.date_dbf, '%d/%m/%Y') AS date_dbf, dbf.url_dbf,
                       dbf.document_to_dbf ,sp.name_spv, prq.amount_reclaimed_prq, prq.amount_authorized_prq,
                       prq.status_prq, prq.observations_prq, prq.type_prq,
					   dispatched_prq, file_url_prq,
					   DATE_FORMAT(dispatched_date_prq, '%d/%m/%Y') AS 'dispatched_date_prq',
                                           DATE_FORMAT(document_date_dbf, '%d/%m/%Y') AS 'document_date_dbf'
                FROM documents_by_file dbf
                JOIN payment_request prq ON prq.serial_dbf = dbf.serial_dbf
				JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf
                JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf
                JOIN service_provider sp ON sp.serial_spv = spf.serial_spv
                WHERE dbf.serial_fle=".$serial_fle;
        if($serial_spv!=NULL){
            $sql.=" AND spf.serial_spv =".$serial_spv;
        }
		
		if($non_paid_only){
			$sql .= " AND dispatched_prq = 'NO' 
					  AND (file_url_prq IS NULL OR file_url_prq = '')";
		}
		
		//Debug::print_r($sql); die;
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getDocumentToList
    * gets all the Types of document in DB.
    ***********************************************/
    public static function getDocumentToList($db){
        $sql = "SHOW COLUMNS FROM documents_by_file LIKE 'document_to_dbf'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }

    ///GETTERS
    function getSerial_dbf(){
        return $this->serial_dbf;
    }
    function getSerial_fle(){
        return $this->serial_fle;
    }
    function getName_dbf(){
        return $this->name_dbf;
    }
    function getComment_dbf(){
        return $this->comment_dbf;
    }
    function getDate_dbf(){
        return $this->date_dbf;
    }
    function getUrl_dbf(){
        return $this->url_dbf;
    }
    function getDocument_to_dbf(){
        return $this->document_to_dbf;
    }
    public function getDocument_date_dbf() {
        return $this->document_date_dbf;
    }

    ///SETTERS
    function setSerial_dbf($serial_dbf){
        $this->serial_dbf = $serial_dbf;
    }
    function setSerial_fle($serial_fle){
        $this->serial_fle = $serial_fle;
    }
    function setName_dbf($name_dbf){
        $this->name_dbf = $name_dbf;
    }
    function setComment_dbf($comment_dbf){
        $this->comment_dbf = $comment_dbf;
    }
    function setDate_dbf($date_dbf){
        $this->date_dbf = $date_dbf;
    }
    function setUrl_dbf($url_dbf){
        $this->url_dbf = $url_dbf;
    }
    function setDocument_to_dbf($document_to_dbf){
        $this->document_to_dbf = $document_to_dbf;
    }
    public function setDocument_date_dbf($document_date_dbf) {
        $this->document_date_dbf = $document_date_dbf;
    }
}
?>