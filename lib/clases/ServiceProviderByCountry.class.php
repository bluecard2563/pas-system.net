<?php
/*
File: ServiceProviderByCountry.class.php
Author: Mario L�pez
Creation Date: 22/02/2010
Last Modified: 19/04/2010
Modified By: David Bergmann
*/

class ServiceProviderByCountry{
    var $db;
    var $serial_spc;
    var $serial_spv;
    var $serial_cou;
    var $status_spc;

    function __construct($db, $serial_spc = NULL, $serial_spv=NULL, $serial_cou=NULL, $status_spc=NULL){
            $this -> db = $db;
            $this -> serial_spc = $serial_spc;
            $this -> serial_spv = $serial_spv;
            $this -> serial_cou = $serial_cou;
            $this -> status_spc = $status_spc;
    }

    /***********************************************
    * function getData
    * sets data by serial_spv
    ***********************************************/
    function getData(){
        if($this->serial_spc!=NULL){
            $sql = "SELECT serial_spc,serial_spv, serial_cou, status_spc
                    FROM service_provider_by_country
                    WHERE serial_spc='".$this->serial_spc."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_spc = $result->fields[0];
                $this -> serial_spv = $result->fields[1];
                $this -> serial_cou = $result->fields[2];
                $this -> status_spc = $result->fields[3];

                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "INSERT INTO service_provider_by_country (
            			serial_spv,
				serial_cou,
                                status_spc
                                )
                VALUES (
                    ".$this -> serial_spv.",
                    ".$this -> serial_cou.",
                    '".$this -> status_spc."'
                )";

        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * function update
    * update a register in the DB
    ***********************************************/
    function update() {
        if($this -> serial_spv){
            $sql = "UPDATE service_provider_by_country
                    SET status_spc = '".$this->status_spc."'
                    WHERE serial_spv ='".$this -> serial_spv."'
                    AND serial_cou = '".$this -> serial_cou."'";

            $result = $this->db->Execute($sql);
//echo $sql.'<br>';
            if($result==true)
                return true;
            else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    /***********************************************
    * function delete
    * delete a register from the DB
    ***********************************************/
    function delete() {
        if($this -> serial_spv){
            $sql = "DELETE FROM service_provider_by_country
                    WHERE serial_spv ='".$this -> serial_spv."'";

            $result = $this->db->Execute($sql);

            if($result==true)
                return true;
            else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    /***********************************************
    * function getServiceProviderByCountry
    * 
    ***********************************************/
    function getServiceProviderByCountry($status_spc=NULL){
        if($this->serial_spv){
            $sql = "SELECT spc.serial_cou, c.serial_zon, c.name_cou, spc.status_spc
                    FROM country c
                    JOIN service_provider_by_country spc ON spc.serial_cou = c.serial_cou
                    WHERE spc.serial_spv = '".$this->serial_spv."'";
            if($status_spc){
                $sql .= " AND spc.status_spc = '".$status_spc."'";
            }
            //die($sql);
            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();

            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
        }else{
            return false;
        }
    }

    /***********************************************
    * function loadServiceProviderByCountry
    ***********************************************/
    function loadServiceProviderByCountry($serial_cou,$type_spv){
        $sql = "SELECT spv.serial_spv, spv.name_spv, spv.type_spv
                FROM service_provider_by_country spc
                JOIN service_provider spv ON spc.serial_spv = spv.serial_spv
                WHERE spc.serial_cou = '".$serial_cou."'
                AND (spv.type_spv ='".$type_spv."' OR spv.type_spv ='BOTH')
                AND spc.status_spc = 'ACTIVE'";
        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();

        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    /***********************************************
    * funcion cityExists
    * verifies if a City exists or not
    ***********************************************/
    function existServiceProviders(){
            $sql = "SELECT serial_spv
                    FROM service_provider_by_country";
            $result = $this->db -> Execute($sql);

            if($result->fields[0]){
                    return $result->fields[0];
            }else{
                    return false;
            }
    }
    
    //GETTERS
    function getSerial_spv(){
        return $this->serial_spv;
    }
    function getSerial_cou(){
        return $this->serial_cou;
    }
    function getStatus_spc(){
        return $this->status_spc;
    }
    
    //SETTERS
    function setSerial_spv($serial_spv){
        $this->serial_spv=$serial_spv;
    }
    function setSerial_cou($serial_cou){
        $this->serial_cou=$serial_cou;
    }
    function setStatus_spc($status_spc){
        $this->status_spc=$status_spc;
    }
}
?>
