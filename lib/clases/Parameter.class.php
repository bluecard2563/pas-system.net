<?php
/*
File: Parameter.class.php
Author: Santiago Benítez
Creation Date: 13/01/2009 08:03
Last Modified: 13/01/2009 08:03
Modified By: Santiago Benítez
*/

class Parameter{				
	var $db;
	var $serial_par;
	var $name_par;
	var $value_par;
	var $type_par;
	var $description_par;
	var $condition_par;
	
	function __construct($db, $serial_par=NULL, $name_par=NULL, $value_par=NULL, $type_par=NULL, $description_par=NULL, $condition_par=NULL ){
		$this -> db = $db;
		$this -> serial_par = $serial_par;
		$this -> name_par = $name_par;
		$this -> value_par = $value_par;
		$this -> type_par = $type_par;
		$this -> description_par = $description_par;
		$this -> condition_par = $condition_par;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_par
    ***********************************************/
	function getData(){
		if($this->serial_par!=NULL){
			$sql = 	"SELECT serial_par, name_par, value_par, type_par, description_par, condition_par
					FROM parameter
					WHERE serial_par='".$this->serial_par."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_par =$result->fields[0];
				$this -> name_par = $result->fields[1];
				$this -> value_par = $result->fields[2];
				$this -> type_par = $result->fields[3];
				$this -> description_par = $result->fields[4];
				$this -> condition_par = $result->fields[5];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * function parameterExists
    * verifies if a Parameter exists or not
    ***********************************************/
	function parameterExists($name_par, $serial_par=NULL){
		$sql = 	"SELECT serial_par
				 FROM parameter 
				 WHERE LOWER(name_par)= _utf8'".utf8_encode($name_par)."' collate utf8_bin";
		if($serial_par!=NULL){
			$sql.=" AND serial_par <> ".$serial_par;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/***********************************************
    * function getParameters
    * gets all the parameters of the system
    ***********************************************/
	function getParameters(){
		$lista=NULL;
		$sql = 	"SELECT serial_par, name_par, value_par, type_par, description_par, condition_par
				 FROM parameter ";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return arr;
	}

	/** 
	@Name: getParametersAutocompleter
	@Description: Returns an array of parameters.
	@Params: Parameter name or pattern.
	@Returns: parameters array
	**/
	function getParametersAutocompleter($value_rst){
		$sql = "SELECT serial_par, name_par
				FROM parameter
				WHERE LOWER(name_par) LIKE _utf8'%".utf8_encode($value_rst)."%' collate utf8_bin
				LIMIT 10";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO parameter (
				serial_par,
				name_par,
				value_par,
				type_par,
				description_par,
				condition_par)
            VALUES (
                NULL,
				'".$this->name_par."',
				'".$this->value_par."',
				'".$this->type_par."',
				'".$this->description_par."',
				'".$this->condition_par."')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE parameter SET
					name_par ='".$this->name_par."',
					value_par ='".$this->value_par."',
					type_par ='".$this->type_par."',
					description_par ='".$this->description_par."',
					condition_par ='".$this->condition_par."'
				WHERE serial_par = '".$this->serial_par."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }
	
	
	/***********************************************
    * funcion getTypeValues
    * Returns the current values for the type field.
    ***********************************************/	
	function getTypeValues(){
		$sql = "SHOW COLUMNS FROM parameter LIKE 'type_par'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/***********************************************
    * funcion getConditionValues
    * Returns the current values for the condition field.
    ***********************************************/	
	function getConditionValues(){
		$sql = "SHOW COLUMNS FROM parameter LIKE 'condition_par'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	public static function setNextValue($db, $serial_par){
		$sql = "UPDATE parameter SET value_par = value_par + 1
				WHERE serial_par = $serial_par";
		
		$result = $db->Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}


	///GETTERS
	function getSerial_par(){
		return $this->serial_par;
	}
	function getName_par(){
		return $this->name_par;
	}
	function getValue_par(){
		return $this->value_par;
	}
	function getType_par(){
		return $this->type_par;
	}
	function getDescription_par(){
		return $this->description_par;
	}
	function getCondition_par(){
		return $this->condition_par;
	}
	

	///SETTERS	
	function setSerial_par($serial_par){
		$this->serial_par = $serial_par;
	}
	function setName_par($name_par){
		$this->name_par = $name_par;
	}
	function setValue_par($value_par){
		$this->value_par = $value_par;
	}
	function setType_par($type_par){
		$this->type_par = $type_par;
	}
	function setDescription_par($description_par){
		$this->description_par = $description_par;
	}
	function setCondition_par($condition_par){
		$this->condition_par = $condition_par;
	}
}
?>