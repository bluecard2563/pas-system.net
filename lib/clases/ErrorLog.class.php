<?php
class ErrorLog{
	var $db;
	var $id;
	var $title;
	var $log;

	function __construct($db, $id=NULL, $title=NULL, $log=NULL){
		$this -> db = $db;
		$this -> title = $title;
		$this -> log = $log;
	}

	public static function retrieve($db, $id){
			$sql = 	"SELECT title, log FROM error_log WHERE id=".$id;

			$result = $db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				echo '--TITLE--';
				echo '<pre>';print_r($result->fields[0]);echo '</pre>';
				echo '--LOG--';
				echo '<pre>';print_r(unserialize($result->fields[1]));echo '</pre>';return;
			}else{
				echo '--NO LOG FOUND--';
			}
			return false;
	}

	public static function log($db, $title, $log) {
    	//$title = serialize($title);
    	if(is_object($log)){
    		$log = get_object_vars($log);
    		unset($log['db']);
    	}
    	
    	$log = serialize($log);
    	$log = str_replace("'","\"", $log);
        $sql = "
            INSERT INTO error_log (id,
				title,
				log)
            VALUES(
				NULL, '".$title."', '".$log."')";
        $result = $db->Execute($sql);
        
        if($result==true)
            return $db->insert_ID();
        else
            return false;
    }
	
	public static function retrieveByTitle($db, $title){
		$sql = 	"SELECT title, log, date, id FROM error_log WHERE title = '$title'";

		$result = $db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function deleteLog($db, $id){
		$sql = 	"DELETE FROM error_log WHERE id = '$id'";
		
		$result = $db -> Execute($sql);

		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public static function load($db, $id){
		$sql = 	"SELECT title, log, date, id FROM error_log WHERE id = '$id'";

		$result = $db->getRow($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
}
?>