<?php
/**
 * File: CreditRanges
 * Author: Patricio Astudillo
 * Creation Date: 18/09/2014
 * Last Modified: 18/09/2014
 * Modified By: Patricio Astudillo
*/

class CreditRanges{
	var $db;
	var $id;
	var $from;
	var $to;
	
	function __construct($db, $id = NULL) {
		$this->db = $db;
		$this->id = $id;
		
		$this->load();
	}

	private function load(){
		if($this->id){
			$sql = "SELECT * FROM creditRanges WHERE id = {$this->id}";
			
			$result = $this->db->getRow($sql);
			
			if($result){
				$this->from = $result['from'];
				$this->to = $result['to'];		
			}
		}
	}
	
	public function save(){
		if($this->id){
			$sql = "UPDATE creditRanges SET 
						from = {$this -> from},
						to = {$this -> to}
					WHERE `id` = {$this->id}";
			
			$result = $this->db->Execute($sql);
			
			if($result){
				return TRUE;
			}else{
				ErrorLog::log($this->db, 'FAILED TO UPDATE CREDIT RANGES - '.$this->id, $this);
				return FALSE;
			}
		}else{ //CREATE
			$sql = "INSERT INTO creditRanges (
								`id`,
								`from`,
								`to`
							) VALUES (
								NULL,
								{$this -> from},
								{$this -> to}
							)";

			//echo $sql; die;
			$result = $this->db->Execute($sql);
			
			if($result){
				$this->id = $this->db->insert_ID();
				$this->load();
				
				return TRUE;
			}else{
				ErrorLog::log($this->db, 'FAILED TO INSERT CREDIT RANGES', $this);
				return FALSE;
			}
		}
	}
	
	public static function getAllCreditRanges($db){
		$sql = "SELECT * FROM creditRanges ORDER BY `from`";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public function getId() {
		return $this->id;
	}

	public function getFrom() {
		return $this->from;
	}

	public function getTo() {
		return $this->to;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setFrom($from) {
		$this->from = $from;
	}

	public function setTo($to) {
		$this->to = $to;
	}
}