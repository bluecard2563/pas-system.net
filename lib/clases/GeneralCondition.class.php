<?php
/*
File: GeneralCondition.class
Author: Patricio Astudillo 
Modified date: 29/12/2009
Last Modified: 30/08/2010
Modified By: David Bergmann
*/

class GeneralCondition {
	var $db;
	var $serial_gcn;
	var $status_gcn;
	
	function __construct($db, $serial_gcn = NULL, $status_gcn = NULL) {
		$this -> db = $db;
		$this -> serial_gcn = $serial_gcn;
		$this -> status_gcn = $status_gcn;
	}
	
	/* function getData() 		*/
	/* set class parameters		*/
	function getData(){
		if($this->serial_gcn!=NULL) {
			$sql = "SELECT serial_gcn, status_gcn
					FROM general_conditions
					WHERE serial_gcn='$this->serial_gcn'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_gcn=$result->fields[0];
				$this->status_gcn=$result->fields[1];
				return true;
			} else
				return false;
		} else
			return false;		
	}
	
	/* Function insert() 						        */
	/* Insert a new General condition into the database	*/
	
	function insert() {
		$sql="INSERT INTO general_conditions (
					serial_gcn,
					status_gcn)
			  VALUES(NULL,
				    '{$this->status_gcn}');";

		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/* function update() 						*/
	/* update a General Conditions				*/
	function update() {
		if($this->serial_gcn!=NULL) {
			$sql = "UPDATE general_conditions SET
						status_gcn='$this->status_gcn'
					WHERE serial_gcn='$this->serial_gcn'";

			$result = $this->db->Execute($sql);

			if ($result) 
				return true;
			else
				return false;
		}else
			return false;		
	}
	
	
	/* function getGeneralConditions() 				*/
	/* return all the General Conditions			*/

	function getGeneralConditions($serial_lang) {
		$sql = "SELECT gc.serial_gcn, glc.name_glc, glc.url_glc as url_planetassist, glc2.url_glc as url_ecuador
				FROM general_conditions gc
				LEFT JOIN gconditions_by_language_by_country glc ON glc.serial_gcn=gc.serial_gcn AND glc.serial_lang=$serial_lang AND glc.serial_cou = '1'
				LEFT JOIN gconditions_by_language_by_country glc2 ON glc2.serial_gcn=gc.serial_gcn AND glc2.serial_lang=$serial_lang AND glc2.serial_cou = '62'
				WHERE gc.status_gcn='ACTIVE'
				GROUP BY gc.serial_gcn";
		//die($sql);
		$result = $this->db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	function getLastInserted() {
	    $sql = "SELECT MAX(serial_gcn) FROM general_conditions";
	 
		$result = $this->db -> Execute($sql);
		if($result->fields[0]) {
			return $result->fields[0];
        } else {
			return false;
		}
	}

	/*
	 * @Name: getGConditionStatus
	 * @Description: Returns an array of all the general_conditions satatus in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of status
	 */
    public static function getGConditionStatus($db) {
        $sql = "SHOW COLUMNS FROM general_conditions LIKE 'status_gcn'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
	
	///GETTERS
	function getSerial_gcn() {
		return $this->serial_gcn;
	}
	function getStatus_gcn() {
		return $this->status_gcn;
	}

	///SETTERS	
	function setSerial_gcn($serial_gcn) {
		$this->serial_gcn = $serial_gcn;
	}
	function setStatus_gcn($status_gcn) {
		$this->status_gcn = $status_gcn;
	}
}
?>