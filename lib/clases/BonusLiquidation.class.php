<?php

/**
 * Description of BonusLiquidation
 *
 * @author Nicolas
 */
class BonusLiquidation {

    var $db;
    var $serial_lqn;
    var $serial_usr;
    var $date_lqn;
    var $status_lqn;
    var $request_date_lqn;
    var $request_usr;
    var $request_dea;
    var $serial_mbc;
    var $total_lqn;

    function __construct($db, $serial_lqn = NULL, $serial_usr = NULL, $date_lqn = NULL, $status_lqn = NULL, $request_date_lqn = NULL, $request_usr = NULL, $request_dea = NULL, $serial_mbc = NULL, $total_lqn = NULL
    ) {
        $this->db = $db;
        $this->serial_lqn = $serial_lqn;
        $this->serial_usr = $serial_usr;
        $this->date_lqn = $date_lqn;
        $this->status_lqn = $status_lqn;
        $this->request_date_lqn = $request_date_lqn;
        $this->request_usr = $request_usr;
        $this->request_dea = $request_dea;
        $this->serial_mbc = $serial_mbc;
        $this->total_lqn = $total_lqn;
    }
	function getData(){
		if($this->serial_lqn!=NULL){
			$sql = 	"SELECT *
					FROM bonus_liquidation
					WHERE serial_lqn='".$this->serial_lqn."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_lqn=$result->fields[0];
				$this -> serial_usr=$result->fields[1];
				$this -> date_lqn=$result->fields[2];
				$this -> status_lqn=$result->fields[3];
				$this -> request_date_usr=$result->fields[4];
				$this -> request_usr=$result->fields[5];
				$this -> request_dea=$result->fields[6];
				$this -> serial_mbc=$result->fields[7];
				$this -> total_lqn=$result->fields[8];

				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
    /*     * *********************************************
     * function insert
     * inserts a new register in DB
     * ********************************************* */

    function insert() {
        $sql = "
            INSERT INTO bonus_liquidation (
				serial_lqn,
				serial_usr,
				date_lqn,
                status_lqn,
                request_date_lqn,
                request_usr,
                request_dea,
                serial_mbc,
                total_lqn
            ) VALUES (
				NULL,";
        if ($this->serial_usr != '') {
            $sql .= "'" . $this->serial_usr . "',";
        } else {
            $sql .= "NULL,";
        }
        if ($this->date_lqn != '') {
            $sql .= "'" . $this->date_lqn . "',";
        } else {
            $sql .= "NULL,";
        }
        $sql .= "
                    '" . $this->status_lqn . "',
                    NOW(),
                    '" . $this->request_usr . "',
                    '" . $this->request_dea . "',
                    '" . $this->serial_mbc . "',
                    '" . $this->total_lqn . "'
				)";
        //die($sql);
        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    /*     * *********************************************
     * funcion pending Liquidation
     * ********************************************* */

    function hasPendingLiquidation($db, $serial_dea, $serial_usr = NULL) {
       
        
        $sql = "SELECT *
            FROM bonus_liquidation
            WHERE status_lqn = 'PENDING' 
            AND request_dea ='" . $serial_dea . "'";
        
//         if ($serial_usr != NULL) {
//            $sql .= "OR request_usr ='" . $this->serial_usr . "',";
//        }
        $result = $db->getOne($sql);
//        echo $sql; die();
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getAllPendingBonusLiquidation($db, $serial_mbc) {
        $sql = "SELECT bl.serial_lqn, bl.request_usr AS 'requester', DATE_FORMAT(bl.request_date_lqn, '%d/%m/%Y') AS 'request_date',
                    bl.total_lqn, d.name_dea
                FROM bonus_liquidation bl
                JOIN dealer d ON d.serial_dea = bl.request_dea
                WHERE status_lqn = 'PENDING'";

        if ($serial_mbc != 1) {
            $sql .= " AND d.serial_mbc = $serial_mbc";
        }

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function voidLiquidation($db, $serial_lqn) {
        $sql = "UPDATE bonus_liquidation SET
                    status_lqn = 'VOID'
                WHERE serial_lqn = $serial_lqn";

        $result = $db->Execute($sql);

        if ($result) {
            return true;
        } else {
            ErrorLog::log($db, 'VOID FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    public static function getDataLiq($db, $serial_lqn) {
        $sql = "Select b.total_lqn as 'total_lqn', CONCAT (u.first_name_usr, ' ', u.last_name_usr) AS 'request_user', u.serial_dea as 'dealer' FROM bonus_liquidation b
            JOIN user u ON u.serial_usr = b.request_usr
                WHERE serial_lqn = $serial_lqn";
        
        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
    public static function getPrizeResumeForLiquidations($db, $liquidationIDArray) {
        $sql = "SELECT pli.serial_lqn, name_pri, SUM(pli.quantity_pli) AS 'qty', SUM(amount_pli) AS 'prize',
                        SUM(cost_pli) AS 'cost', pli.serial_pri
                FROM prizes_liquidation pli
                JOIN bonus_liquidation bl ON bl.serial_lqn = pli.serial_lqn
                    AND pli.serial_lqn in ($liquidationIDArray)
                JOIN prizes p ON p.serial_pri = pli.serial_pri
                GROUP BY pli.serial_lqn, pli.serial_pri";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }
    
    public static function getAllSalesByLqn($db, $liquidationIds){
        $sql = "select s.card_number_sal as 'tarjeta', concat(c.first_name_cus, ' ',c.last_name_cus ) as 'customer', i.number_inv as 'invoice', lqn.amount_used_abl as 'amount' from applied_bonus_liquidation lqn
join applied_bonus apb on apb.serial_apb = lqn.serial_apb
join sales s on s.serial_sal = apb.serial_sal
join customer c on c.serial_cus = s.serial_cus
join invoice i on i.serial_inv = s.serial_inv
where serial_lqn in ($liquidationIds)";
        
        $result = $db->getAll($sql);
        
//        Debug::print_r($result);die();

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }
    
    function updateliquidation($serial_lqn, $serial_usr, $date ) {
		$sql = "UPDATE bonus_liquidation SET serial_usr=$serial_usr, date_lqn='".$date."', status_lqn='".PAID."' WHERE serial_lqn=$serial_lqn";

                //Debug::print_r($sql); die;
		$result = $this->db->Execute($sql);
		if ($result === false

			)return false;

		if ($result == true)
			return true;
		else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}
    
            function getSerial_lqn() {
        return $this->serial_lqn;
    }

    function getSerial_usr() {
        return $this->serial_usr;
    }

    function getDate_lqn() {
        return $this->date_lqn;
    }

    function getStatus_lqn() {
        return $this->status_lqn;
    }

    function getRequest_date_lqn() {
        return $this->request_date_lqn;
    }

    function getRequest_usr() {
        return $this->request_usr;
    }

    function getRequest_dea() {
        return $this->request_dea;
    }

    function getSerial_mbc() {
        return $this->serial_mbc;
    }

    function getTotal_lqn() {
        return $this->total_lqn;
    }

    function setSerial_lqn($serial_lqn) {
        $this->serial_lqn = $serial_lqn;
    }

    function setSerial_usr($serial_usr) {
        $this->serial_usr = $serial_usr;
    }

    function setDate_lqn($date_lqn) {
        $this->date_lqn = $date_lqn;
    }

    function setStatus_lqn($status_lqn) {
        $this->status_lqn = $status_lqn;
    }

    function setRequest_date_lqn($request_date_lqn) {
        $this->request_date_lqn = $request_date_lqn;
    }

    function setRequest_usr($request_usr) {
        $this->request_usr = $request_usr;
    }

    function setRequest_dea($request_dea) {
        $this->request_dea = $request_dea;
    }

    function setSerial_mbc($serial_mbc) {
        $this->serial_mbc = $serial_mbc;
    }

    function setTotal_lqn($total_lqn) {
        $this->total_lqn = $total_lqn;
    }
}
?>
