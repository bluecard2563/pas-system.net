<?php
/**
 * File: DynamicCom
 * Author: Patricio Astudillo
 * Creation Date: 18/09/2014
 * Last Modified: 18/09/2014
 * Modified By: Patricio Astudillo
*/

class DynamicCom{
	var $db;
	var $id;
	var $credit_id;
	var $responsibleID;
	var $percentage;
	var $modifyingUsrID;

	function __construct($db, $id = NULL) {
		$this->db = $db;
		$this->load();
	}

	public function load(){
		if($this->id){
			$sql = "SELECT * FROM dynamicComissions WHERE id = {$this->id}";
			
			$result = $this->db->getRow($sql);
			
			if($result){
				$this -> credit_id = $result['credit_id'];
				$this -> responsibleID = $result['responsibleID'];
				$this -> percentage = $result['percentage'];
				$this -> modifyingUsrID = $result['modifyingUsrID'];
			}
		}
	}
	
	public function save(){
		if($this->id){ //UPDATE
			$sql = "UPDATE dynamicComissions SET 
						credit_id = {$this -> credit_id},
						responsibleID = {$this -> responsibleID},
						percentage = '{$this -> percentage}',
						modifyingUsrID = {$this -> modifyingUsrID}
					WHERE id = {$this->id}";
			
			$result = $this->db->Excecute($sql);
			
			if($result){
				return TRUE;
			}else{
				ErrorLog::log($db, 'FAILED TO UPDATE DYNAMICCOM - '.$this->id, $this);
				return FALSE;
			}
		}else{ //CREATE
			$sql = "INSERT INTO dynamicComissions (
								id,
								credit_id,
								responsibleID,
								percentage,
								modifyingUsrID
							) VALUES (
								NULL,
								{$this -> credit_id},
								{$this -> responsibleID},
								'{$this -> percentage}',
								{$this -> modifyingUsrID}
							)";
								
			$result = $this->db->Excecute($sql);
			
			if($result){
				$this->id = $this->db->insert_ID();
				$this->load();
				
				return TRUE;
			}else{
				ErrorLog::log($db, 'FAILED TO INSERT DYNAMICCOM - '.$this->id, $this);
				return FALSE;
			}
		}
	}
	
	public static function getComissionPercentageForUserOnInvoice($db, $serial_ubd, $invoiceID){
		$sql = "SELECT effectiveDaysSinceEmission
			FROM view_effectivePaymentDays WHERE serial_inv = $invoiceID";
				
		$invoiceData = $db->getRow($sql);
			
		if($invoiceData){
			$sql = "SELECT percentage 
					FROM dynamicComissions  dc
					JOIN user_by_dealer ubd ON ubd.serial_usr = dc.responsibleID AND ubd.serial_ubd = $serial_ubd
					JOIN creditRanges cr ON cr.id = dc.credit_id 
						AND {$invoiceData['effectiveDaysSinceEmission']} BETWEEN cr.from AND cr.to";
			
			$percentageForResponsible = $db->getOne($sql);
			
			if($percentageForResponsible){
				return $percentageForResponsible;
			}else{
				return FALSE;
			}
		}else{
			ErrorLog::log($db, 'DYNAMICCOM - NO INVOICE ON VIEW TO BEGIN PROCESS', $invoiceID);
			return FALSE;
		}
	}
}