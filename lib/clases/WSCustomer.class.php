<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WSCustomer
 *
 * @author Valeria Andino
 */
class WSCustomer {
    
    var $db;
    var $serial_cus;
    var $serial_cit;
    var $type_cus;
    var $document_cus;
    var $first_name_cus;
    var $last_name_cus;
    var $birthdate_cus;
    var $phone1_cus;
    var $phone2_cus;
    var $cellphone_cus;
    var $email_cus;
    var $address_cus;
    var $relative_cus;
    var $relative_phone_cus;
    var $vip_cus;
    var $status_cus;
    var $password_cus;
    var $auxData;
    var $medical_background_cus;
    var $first_access_cus;

    function __construct($db, $serial_cus = NULL, $serial_cit = NULL, $type_cus = NULL, $document_cus = NULL, $first_name_cus = NULL, $last_name_cus = NULL, $birthdate_cus = NULL, $phone1_cus = NULL, $phone2_cus = NULL, $cellphone_cus = NULL, $email_cus = NULL, $address_cus = NULL, $relative_cus = NULL, $relative_phone_cus = NULL, $vip_cus = NULL, $status_cus = NULL, $password_cus = NULL, $medical_background_cus = NULL, $first_access_cus = NULL) {
        $this->db = $db;
        $this->serial_cus = $serial_cus;
        $this->serial_cit = $serial_cit;
        $this->type_cus = $type_cus;
        $this->document_cus = $document_cus;
        $this->first_name_cus = $first_name_cus;
        $this->last_name_cus = $last_name_cus;
        $this->birthdate_cus = $birthdate_cus;
        $this->phone1_cus = $phone1_cus;
        $this->phone2_cus = $phone2_cus;
        $this->cellphone_cus = $cellphone_cus;
        $this->email_cus = $email_cus;
        $this->address_cus = $address_cus;
        $this->relative_cus = $relative_cus;
        $this->relative_phone_cus = $relative_phone_cus;
        $this->vip_cus = $vip_cus;
        $this->status_cus = $status_cus;
        $this->password_cus = $password_cus;
        $this->medical_background_cus = $medical_background_cus;
        $this->first_access_cus = $first_access_cus;
    }
    
     /*     * *********************************************
     * funcion getData
     * sets data by serial
     * ********************************************* */

    function getData($useEntityCityValue = false) {
        //CRITICAL: ALL CUSTOMERS MUST HAVE A CITY!!!!
        if ($useEntityCityValue) {
            $citySql = "LEFT JOIN city cit ON cit.serial_cit = " . $this->serial_cit;
        } else {
            $citySql = "LEFT JOIN city cit ON cit.serial_cit = c.serial_cit";
        }
        if ($this->serial_cus != NULL) {
            $whereClause = "serial_cus='" . $this->serial_cus . "'";
        } elseif ($this->email_cus != NULL) {
            $whereClause = "email_cus='" . $this->email_cus . "'";
        } elseif ($this->document_cus != NULL) {
            $whereClause = "document_cus='" . $this->document_cus . "'";
        } else {
            return false;
        }
        $sql = "SELECT c.serial_cus, c.serial_cit, c.type_cus, c.document_cus, c.first_name_cus, c.last_name_cus,
				DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y'), c.phone1_cus, c.phone2_cus, c.cellphone_cus, c.email_cus, c.address_cus, c.relative_cus,
				c.relative_phone_cus, c.vip_cus, c.status_cus, c.password_cus, c.medical_background_cus, cit.serial_cou, c.first_access_cus
				FROM customer c $citySql WHERE " . $whereClause;
        $result = $this->db->Execute($sql);
        if ($result === false)
            return false;

        if ($result->fields[0]) {
            $this->serial_cus = $result->fields[0];
            $this->serial_cit = $result->fields[1];
            $this->type_cus = $result->fields[2];
            $this->document_cus = $result->fields[3];
            $this->first_name_cus = $result->fields[4];
            $this->last_name_cus = $result->fields[5];
            $this->birthdate_cus = $result->fields[6];
            $this->phone1_cus = $result->fields[7];
            $this->phone2_cus = $result->fields[8];
            $this->cellphone_cus = $result->fields[9];
            $this->email_cus = $result->fields[10];
            $this->address_cus = $result->fields[11];
            $this->relative_cus = $result->fields[12];
            $this->relative_phone_cus = $result->fields[13];
            $this->vip_cus = $result->fields[14];
            $this->status_cus = $result->fields[15];
            $this->password_cus = $result->fields[16];
            $this->medical_background_cus = $result->fields[17];
            $this->auxData['serial_cou'] = $result->fields[18];
            $this->first_access_cus = $result->fields[19];
            return true;
        }
        return false;
    }

    /*     * *********************************************
     * funcion getCustomers
     * gets information from Customer
     * ********************************************* */

    function getCustomers($pattern = NULL) 
	{
        $sql = "SELECT serial_cus, type_cus, document_cus, first_name_cus, last_name_cus, birthdate_cus, 
						phone1_cus, phone2_cus, cellphone_cus, email_cus, address_cus, relative_cus, relative_phone_cus, 
						vip_cus, status_cus, password_cus, first_access_cus, CONCAT('C',serial_cus) AS 'oo_code', 
						cit.name_cit, cou.name_cou, cit.serial_cit, wrc.wr_id as 'serial_cou'
				 FROM customer
				 JOIN city cit ON customer.serial_cit = cit.serial_cit
                 JOIN country cou ON cit.serial_cou= cou.serial_cou
				 LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou";

        if ($pattern != NULL && $pattern != '') {
            $sql.=" WHERE first_name_cus LIKE '%" . utf8_encode($pattern) . "%'
                       OR last_name_cus LIKE '%" . utf8_encode($pattern) . "%'
				       OR document_cus LIKE '%" . utf8_encode($pattern) . "%'
					   OR CONCAT(first_name_cus,' ',last_name_cus) LIKE '%" . utf8_encode($pattern) . "%'
					   OR CONCAT(last_name_cus,' ',first_name_cus) LIKE '%" . utf8_encode($pattern) . "%'
					   OR email_cus LIKE '%" . utf8_encode($pattern) . "%'";
        }
		$sql.= " LIMIT 400";
//print_r($sql);die();
        $result = $this->db->getAssoc($sql);
		if($result){
			return $result;
		}else{
			return false;
		}	
		
    }
	
	    /** **************************** VA
     * getCustomerBySerialorDocument*
     Gets information user for new sale 
     * @Params: Customer name or pattern.
    */
    public function getCustomerBySerialorDocument($db){
		$sql = "SELECT cus.serial_cus,cus.document_cus, cus.first_name_cus, 
				cus.last_name_cus, cus.birthdate_cus, 
				cus.address_cus, cus.phone1_cus, 
				cus.phone2_cus, cus.cellphone_cus, 
				cus.email_cus, cus.relative_cus, 
				cus.relative_phone_cus, cus.serial_cit, 
				cit.name_cit, cou.name_cou, cit.serial_cit, wrc.wr_id as 'serial_cou'  
				FROM customer cus 
				JOIN city cit oN cus.serial_cit = cit.serial_cit
				JOIN country cou oN cit.serial_cou= cou.serial_cou 
				LEFT JOIN webratio_country wrc ON wrc.serial_cou = cit.serial_cou
				WHERE 1 ";
		if ($this->serial_cus)
			$sql .= " AND cus.serial_cus = '$this->serial_cus' ";
		if ($this->document_cus)
			$sql .= " AND cus.document_cus = '$this->document_cus'";
		//echo $sql; die();
		$result = $db->getAll($sql);

		if($result){
			$this->serial_cus = $result[0]['serial_cus'];
			$this->document_cus = $result[0]['document_cus'];
			return $result;
		}else{
			return false;
		}		
	}
    
    function insert() {
        $sql = "
            INSERT INTO customer (
				serial_cus, 
				serial_cit, 
				type_cus, 
				document_cus, 
				first_name_cus, 
				last_name_cus, 
				birthdate_cus, 
				phone1_cus, 
				phone2_cus, 
				cellphone_cus, 
				email_cus, 
				address_cus, 
				relative_cus, 
				relative_phone_cus, 
				vip_cus, 
				status_cus,
				password_cus,
				first_access_cus)
            VALUES(				
				NULL,";
        $sql.=($this->serial_cit) ? "'" . $this->serial_cit . "'," : "NULL,";
        $sql.=($this->type_cus) ? "'" . $this->type_cus . "'," : "'PERSON',";
        $sql.=($this->document_cus) ? "'" . $this->document_cus . "'," : "NULL,";
        $sql.=($this->first_name_cus) ? "'" . $this->first_name_cus . "'," : "NULL,";
        $sql.=($this->last_name_cus) ? "'" . $this->last_name_cus . "'," : "NULL,";
        $sql.=($this->birthdate_cus) ? "STR_TO_DATE('" . $this->birthdate_cus . "','%d/%m/%Y')," : "NULL,";
        $sql.=($this->phone1_cus) ? "'" . $this->phone1_cus . "'," : "NULL,";
        $sql.=($this->phone2_cus) ? "'" . $this->phone2_cus . "'," : "NULL,";
        $sql.=($this->cellphone_cus) ? "'" . $this->cellphone_cus . "'," : "NULL,";
        $sql.=($this->email_cus) ? "'" . $this->email_cus . "'," : "NULL,";
        $sql.=($this->address_cus) ? "'" . $this->address_cus . "'," : "NULL,";
        $sql.=($this->relative_cus) ? "'" . $this->relative_cus . "'," : "NULL,";
        $sql.=($this->relative_phone_cus) ? "'" . $this->relative_phone_cus . "'," : "NULL,";
        $sql.=($this->vip_cus) ? "'" . $this->vip_cus . "'," : "NULL,";
        $sql.=($this->status_cus) ? "'" . $this->status_cus . "'," : "NULL,";
        $sql.=($this->password_cus) ? "'" . $this->password_cus . "'," : "NULL,";
        $sql.=($this->first_access_cus) ? "'" . $this->first_access_cus . "'" : "'YES'";
        $sql.=")";

        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

     function update() {
        $sql = "UPDATE customer SET
					serial_cit		=	 '" . $this->serial_cit . "',
					type_cus		=	 '" . $this->type_cus . "',
					document_cus=		 '" . $this->document_cus . "',
					first_name_cus=		 '" . $this->first_name_cus . "',
					last_name_cus	=	 '" . $this->last_name_cus . "',
					birthdate_cus=		 STR_TO_DATE('" . $this->birthdate_cus . "','%d/%m/%Y'),
					phone1_cus	=		 '" . $this->phone1_cus . "',
					phone2_cus	=		 '" . $this->phone2_cus . "',
					cellphone_cus=		 '" . $this->cellphone_cus . "',
					email_cus=			 '" . $this->email_cus . "',
					address_cus=		 '" . $this->address_cus . "',				
					relative_cus=		 '" . $this->relative_cus . "',
					relative_phone_cus=	 '" . $this->relative_phone_cus . "',
					vip_cus =			 '" . $this->vip_cus . "',
					status_cus=          '" . $this->status_cus . "',
					password_cus=        '" . $this->password_cus . "',
                    medical_background_cus= '" . $this->medical_background_cus . "',
                    first_access_cus=        '" . $this->first_access_cus . "'
                WHERE serial_cus = '" . $this->serial_cus . "'";
        $result = $this->db->Execute($sql);
echo $result; die();
        if ($result) {
            return true;
        } else {
            ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
            return false;
        }
    }
    
    /*     * *********************************************
     * funcion existsCustomer
     * verifies if a customer exists or not
     * ********************************************* */

    function existsCustomer($document_cus, $serial_cus = NULL) {
        $sql = "SELECT document_cus
				 FROM customer
				 WHERE document_cus= _utf8'" . utf8_encode($document_cus) . "' collate utf8_bin";

        if ($serial_cus != NULL && $serial_cus != '') {
            $sql.= " AND serial_cus <> " . $serial_cus;
        }
//echo $sql;
//die();
        $result = $this->db->Execute($sql);

        if ($result->fields[0]) {
            return true;
        } else {
            return false;
        }
    }
    public static function getSerialCustomerByDocument($db, $document_cus){
		$sql = "SELECT cus.serial_cus FROM customer cus 
                        JOIN city cit oN cus.serial_cit = cit.serial_cit
                        JOIN country cou oN cit.serial_cou= cou.serial_cou 
                        where cus.document_cus = '$document_cus'";
//echo $sql; die();
                $result = $db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}		
	}
    
        
     ///GETTERS
    function getSerial_cus() {
        return $this->serial_cus;
    }

    function getSerial_cit() {
        return $this->serial_cit;
    }

    function getType_cus() {
        return $this->type_cus;
    }

    function getDocument_cus() {
        return $this->document_cus;
    }

    function getFirstname_cus() {
        return $this->first_name_cus;
    }

    function getLastname_cus() {
        return $this->last_name_cus;
    }

    function getBirthdate_cus() {
        return $this->birthdate_cus;
    }

    function getPhone1_cus() {
        return $this->phone1_cus;
    }

    function getPhone2_cus() {
        return $this->phone2_cus;
    }

    function getCellphone_cus() {
        return $this->cellphone_cus;
    }

    function getEmail_cus() {
        return $this->email_cus;
    }

    function getAddress_cus() {
        return ($this->address_cus);
    }

    function getRelative_cus() {
        return $this->relative_cus;
    }

    function getRelative_phone_cus() {
        return $this->relative_phone_cus;
    }

    function getVip_cus() {
        return $this->vip_cus;
    }

    function getStatus_cus() {
        return $this->status_cus;
    }

    function getPassword_cus() {
        return $this->password_cus;
    }

    function getMedical_background_cus() {
        return $this->medical_background_cus;
    }

    function getAuxData() {
        return $this->auxData;
    }

    function getFirst_access_cus() {
        return $this->first_access_cus;
    }

    ///SETTERS	
    function setserialCustomer($serial_cus) {
        $this->serial_cus = $serial_cus;
    }

    function setSerialCity($serial_cit) {
        $this->serial_cit = $serial_cit;
    }

    function setTypeCustomer($type_cus) {
        $this->type_cus = $type_cus;
    }

    function setDocumentCustomer($document_cus) {
        $this->document_cus = $document_cus;
    }

    function setFirstNameCustomer($first_name_cus) {
        $this->first_name_cus = utf8_decode($first_name_cus);
    }

    function setLastNameCustomer($last_name_cus) {
        $this->last_name_cus = utf8_decode($last_name_cus);
    }

    function setBirthdateCustomer($birthdate_cus) {
        $this->birthdate_cus = $birthdate_cus;
    }

    function setPhone1Customer($phone1_cus) {
        $this->phone1_cus = $phone1_cus;
    }

    function setPhone2Customer($phone2_cus) {
        $this->phone2_cus = $phone2_cus;
    }

    function setCellphoneCustomer($cellphone_cus) {
        $this->cellphone_cus = $cellphone_cus;
    }

    function setEmailCustomer($email_cus) {
        $this->email_cus = utf8_decode($email_cus);
    }

    function setAddressCustomer($address_cus) {
        $this->address_cus = addslashes(utf8_decode($address_cus));
    }

    function setRelativeCustomer($relative_cus) {
        $this->relative_cus = utf8_decode($relative_cus);
    }

    function setRelativephoneCustomer($relative_phone_cus) {
        $this->relative_phone_cus = $relative_phone_cus;
    }

    function setVipCustomer($vip_cus) {
        $this->vip_cus = $vip_cus;
    }

    function setStatusCustomer($status_cus) {
        $this->status_cus = $status_cus;
    }

    function setPasswordCustomer($password_cus) {
        $this->password_cus = $password_cus;
    }

    function setMedicalbackgroundCustomer($medical_background_cus) {
        $this->medical_background_cus = utf8_decode($medical_background_cus);
    }

    function setFirstaccessCustomer($first_access_cus) {
        return $this->first_access_cus = $first_access_cus;
    }
}
?>