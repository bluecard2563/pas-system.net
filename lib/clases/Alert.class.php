<?php

/*
  Document   : Alert.class.php
  Created on : Apr 16, 2010, 10:58:24 AM
  Author     : H3Dur4k
  Description:
 */

class Alert {

	var $db;
	var $serial_alt;
	var $message_alt;
	var $type_alt;
	var $status_alt;
	var $in_date_alt;
	var $alert_time_alt;
	var $remote_id;
	var $table_name;
	var $serial_usr;
	var $served_date_alt;
	var $serial_fle;
	var $requester_usr;

	function __construct($db, $serial_alt=NULL, $message_alt=NULL, $type_alt=NULL, $status_alt=NULL, $in_date_alt=NULL, $alert_time_alt=NULL, $remote_id=NULL, $table_name=NULL, $serial_usr=NULL, $served_date_alt=NULL, $serial_fle=NULL, $requester_usr = NULL) {
		$this->db = $db;
		$this->serial_alt = $serial_alt;
		$this->message_alt = $message_alt;
		$this->type_alt = $type_alt;
		$this->status_alt = $status_alt;
		$this->in_date_alt = $in_date_alt;
		$this->alert_time_alt = $alert_time_alt;
		$this->remote_id = $remote_id;
		$this->table_name = $table_name;
		$this->serial_usr = $serial_usr;
		$this->served_date_alt = $served_date_alt;
		$this->serial_fle = $serial_fle;
		$this->requester_usr = $requester_usr;
	}

	/*	 * *********************************************
	 * function getData
	  @Name: getData
	  @Description: Returns all data
	  @Params:
	 *       N/A
	  @Returns: true
	 *        false
	 * ********************************************* */

	function getData() {
		if ($this->serial_alt != NULL) {
			$sql = "SELECT serial_alt,
							message_alt,
							type_alt,
							status_alt,
							in_date_alt,
							DATE_FORMAT(alert_time_alt,'%d/%m/%Y %H:%i'),
							remote_id,
							table_name,
							serial_usr,
							DATE_FORMAT(served_date_alt,'%d/%m/%Y %H:%i'),
							serial_fle,
							requester_usr
                            FROM alerts
                            WHERE serial_alt ='" . $this->serial_alt . "'";

			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_alt = $result->fields[0];
				$this->message_alt = $result->fields[1];
				$this->type_alt = $result->fields[2];
				$this->status_alt = $result->fields[3];
				$this->in_date_alt = $result->fields[4];
				$this->alert_time_alt = $result->fields[5];
				$this->remote_id = $result->fields[6];
				$this->table_name = $result->fields[7];
				$this->serial_usr = $result->fields[8];
				$this->served_date_alt = $result->fields[9];
				$this->serial_fle = $result->fields[10];
				$this->requester_usr = $result->fields[11];
				return true;
			}
		}
		return false;
	}

	/*	 * *********************************************
	 * function insert
	 * inserts a new register in DB
	 * ********************************************* */

	function insert() {
		$sql = "
            INSERT INTO alerts (
				serial_alt,
				message_alt,
				type_alt,
				status_alt,
				in_date_alt,
				alert_time_alt,
				remote_id,
				table_name,
				serial_usr,
				served_date_alt,
				serial_fle,
				requester_usr)
            VALUES (
                NULL,
				'" . specialchars($this->message_alt) . "',
				'" . $this->type_alt . "',
				'" . $this->status_alt . "',
				NOW(),";
		$sql.= ( $this->alert_time_alt == NULL) ? 'NULL,' : "STR_TO_DATE('" . $this->alert_time_alt . "','%d/%m/%Y %H:%i'),";
		$sql.= ( $this->remote_id == NULL) ? 'NULL,' : "'{$this->remote_id}',";
		$sql.= ( $this->table_name == NULL) ? 'NULL,' : "'{$this->table_name}',";
		$sql.="'" . $this->serial_usr . "',";
		$sql.= ( $this->served_date_alt == NULL) ? 'NULL' : "STR_TO_DATE('" . $this->served_date_alt . "','%d/%m/%Y %H:%i')";
		$sql.= ( $this->serial_fle == NULL) ? ',NULL' : ",{$this->serial_fle}";
		$sql.= ( $this->requester_usr == NULL) ? ',NULL)' : ",{$this->requester_usr})";

		//echo $sql;
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion update
	 * updates a register in DB
	 * ********************************************* */

	function update() {
		$sql = "UPDATE alerts
		SET message_alt='" . specialchars($this->message_alt) . "',
			status_alt='" . $this->status_alt . "',
			in_date_alt= STR_TO_DATE('" . $this->in_date_alt . "','%d/%m/%Y %H:%i'),
			alert_time_alt=STR_TO_DATE('" . $this->alert_time_alt . "','%d/%m/%Y %H:%i'),
			remote_id='" . $this->remote_id . "',
			table_name='" . $this->table_name . "',
			serial_usr='" . $this->serial_usr . "',
			served_date_alt=STR_TO_DATE('" . $this->served_date_alt . "','%d/%m/%Y %H:%i'),
			serial_fle='" . $this->serial_fle . "',
			requester_usr='" . $this->requester_usr . "'
		WHERE serial_alt='" . $this->serial_alt . "'";
		//die($sql);
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * function getUserPendingAlerts
	  @Name: getUserPendingAlerts
	  @Description: get all the alerts which haven't been served.
	  @Params:
	 *       attribute this->serial_usr
	  @Returns: array with alerts
	 * 		  false if no results
	 * ********************************************* */

	public static function getUserPendingAlerts($db, $serial_usr) {
		$sql = self::getUserAlertsQuery($serial_usr);
		$result = $db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	public static function getUserAlertsQuery($serial_usr) { //THIS QUERY RETURNS ALL THE ALERTS THAT HAVE BEEN SENT TO THE USER OWNING THE ALERT
		$selectStatement = "SELECT 	a.serial_alt,
						a.message_alt,
						a.type_alt,
						a.status_alt,
						DATE_FORMAT(a.in_date_alt,'%d/%m/%Y %H:%i') as in_date_alt,
						DATE_FORMAT(a.alert_time_alt,'%d/%m/%Y %H:%i') as alert_time_alt,
						a.remote_id,
						a.table_name,
						a.serial_usr,
						a.serial_fle,
						u.first_name_usr,
						u.last_name_usr,
						(	SELECT man.name_man
							FROM manager man
							WHERE man.serial_man =
								IFNULL(	(SELECT mm.serial_man FROM manager_by_country mm WHERE mm.serial_mbc = u.serial_mbc),
										(SELECT mm.serial_man FROM manager_by_country mm, dealer dd WHERE mm.serial_mbc = dd.serial_mbc AND dd.serial_dea = u.serial_dea))) as requester_manager";

		$sql = "($selectStatement
			FROM alerts a
			JOIN user u ON u.serial_usr = a.requester_usr
			WHERE a.status_alt='PENDING'
				AND a.serial_usr = $serial_usr
				AND a.type_alt !='FILE')

			UNION(
				$selectStatement
				FROM alerts a
				JOIN user u ON u.serial_usr = a.requester_usr
				JOIN assistance_groups_by_users agu ON a.remote_id=agu.serial_asg
					AND a.type_alt='FILE'
					AND agu.serial_usr=$serial_usr
					AND agu.status_agu ='ACTIVE'
				WHERE a.status_alt='PENDING')";
		return $sql;
	}

	/*	 * *********************************************
	 * function getNumberPendingAlerts
	  @Name: getNumberPendingAlerts
	  @Description: get the number of alerts by user which are pending..
	  @Params:
	 *       attribute this->serial_usr
	  @Returns: int with number of alerts
	 * 		  false if no results
	 * ********************************************* */

	public static function getNumberPendingAlerts($db, $serial_usr) {
		$innerSql = self::getUserAlertsQuery($serial_usr);
		$sql = "SELECT COUNT(*) FROM ($innerSql) as alerts";
		$result = $db->getOne($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/*	 * *********************************************
	 * function setServeAlert
	  @Name: setServeAlert
	  @Description: set an alert served.
	  @Params:
	 *       type: alert type ('FILE','INVOICE','SALE','REFUND','CREDIT_NOTE')
	 * 			 remote_id: serial of the element affected, for example invoice would recive serial_inv, for file it would recive the alert id
	  @Returns: int with number of alerts
	 * 		  false if no results
	 * ********************************************* */

	function setServeAlert($type, $remote_id) {
		if (!Alert::isValidAlertType($this->db, strtoupper($type))) {
			return false;
		}

		//Define the 'where' clause here:
		$whereClause = "WHERE  status_alt='PENDING' AND ";
		$whereClause .= ( $type == 'FILE') ? " serial_alt=$remote_id " : "type_alt='$type' AND remote_id='$remote_id'";

		$querySQL = "SELECT * FROM alerts $whereClause";
		$result = $this->db->Execute($querySQL);
		if (!$result->fields) {
			ErrorLog::log($this->db, 'ALERT SERVE FAILED - ' . $remote_id, $type);
			return false;
		}

		$sql = "UPDATE alerts
			  	SET status_alt = 'DONE', served_date_alt=NOW() $whereClause";
		$result = $this->db->Execute($sql);
		return true;
	}

	public static function isValidAlertType($db, $type) {
		global $global_AlertTypes;
		$isValid = false;
		foreach ($global_AlertTypes as $key => $singleType) {
			if ($key == $type) {
				$isValid = true;
			}
		}
		return $isValid;
	}

	/*	 * *********************************************
	 * @Name: getStandByRequest
	 * @Description: Get all the standBy requests in the system.
	 * @Params: $db: DB connection
	 * 			$userID: Owner of the requests.
	 * @Returns: Array of pending applications.
	 * ********************************************* */

	public static function getStandByRequest($db, $serial_usr) {
		$sql = "SELECT a.*, s.card_number_sal, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'applicant', d.name_dea, cou.name_cou, sl.date_slg, sl.serial_slg
			  FROM alerts a
			  JOIN sales_log sl ON sl.serial_slg=a.remote_id
			  JOIN sales s ON s.serial_sal=sl.serial_sal
			  JOIN user u ON u.serial_usr=sl.serial_usr
			  JOIN counter c ON c.serial_cnt=s.serial_cnt
			  JOIN dealer d ON d.serial_dea=c.serial_dea
			  JOIN sector sec ON sec.serial_sec=d.serial_sec
			  JOIN city cit ON cit.serial_cit=sec.serial_cit
			  JOIN country cou ON cou.serial_cou=cit.serial_cou
			  WHERE a.type_alt = 'STANDBY'
			  AND a.status_alt = 'PENDING'
			  AND a.serial_usr = '" . $serial_usr . "'";

		//echo $sql;
		$result = $db->getAll($sql);
		//echo $sql." ";
		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * @Name: getBlockedRequest
	 * @Description: Get all the blocked requests in the system.
	 * @Params: $db: DB connection
	 * 			$userID: Owner of the requests.
	 * @Returns: Array of pending applications.
	 * ********************************************* */

	public static function getBlockedRequest($db, $serial_usr) {
		$sql = "SELECT a.*, s.card_number_sal, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'applicant', d.name_dea, cou.name_cou, sl.date_slg, sl.serial_slg
			  FROM alerts a
			  JOIN sales_log sl ON sl.serial_slg=a.remote_id
			  JOIN sales s ON s.serial_sal=sl.serial_sal
			  JOIN user u ON u.serial_usr=sl.serial_usr
			  JOIN counter c ON c.serial_cnt=s.serial_cnt
			  JOIN dealer d ON d.serial_dea=c.serial_dea
			  JOIN sector sec ON sec.serial_sec=d.serial_sec
			  JOIN city cit ON cit.serial_cit=sec.serial_cit
			  JOIN country cou ON cou.serial_cou=cit.serial_cou
			  WHERE a.type_alt = 'BLOCKED'
			  AND a.status_alt = 'PENDING'
			  AND a.serial_usr = '" . $serial_usr . "'";

		//echo $sql;
		$result = $db->getAll($sql);
		//echo $sql." ";
		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	function autoSend($serialUsrRequester) {
		$this->requester_usr = $serialUsrRequester;
		$parameterValue = self::getParameterValueByAlertType($this->getType_alt());
		$destinationUsers = self::getAlertUsers($this->db, $parameterValue, $serialUsrRequester);
		
		$alerstInserted = true;
		if ($parameterValue == 'FILE') {
			$this->setSerial_usr($serialUsrRequester);
			if (!$this->insert()) {
				$tempObj = get_object_vars($this);
				unset($tempObj['db']);
				ErrorLog::log($this->db, 'Alert insert Fault', $tempObj);
				$alerstInserted = false;
			}
		} else {
			foreach ($destinationUsers as $user) {
				$this->setSerial_usr($user);
				if (!$this->insert()) {
					$tempObj = get_object_vars($this);
					unset($tempObj['db']);
					ErrorLog::log($this->db, 'Alert insert Fault', $tempObj);
					$alerstInserted = false;
				}
			}
		}
		return $alerstInserted;
	}

	public static function getParameterValueByAlertType($alertType) {
		$parameterValue = '0';
		switch ($alertType) {
			case 'INVOICE':
				$parameterValue = '15';
				break;
			case 'STANDBY':
				$parameterValue = '16';
				break;
			case 'VOID_SALE':
				$parameterValue = '36';
				break;
			case 'SALE':
				$parameterValue = '17';
				break;
			case 'REFUND':
				$parameterValue = '18';
				break;
			case 'CREDIT_NOTE':
				$parameterValue = '19';
				break;
			case 'BLOCKED':
				$parameterValue = '24';
				break;
			case 'REACTIVATED':
				$parameterValue = '30';
				break;
			case 'SPECIAL_SALE_MODIFICATION':
				$parameterValue = '33';
				break;
			case 'FREE':
				$parameterValue = '35';
				break;
			case 'FILE':
				$parameterValue = 'FILE'; //Special Case
				break;
			case 'SPECIAL_REFUND':
				$parameterValue = '40';
			break;
		}
		return $parameterValue;
	}

	public static function getAlertUsers($db, $parameterValue, $serialUsrRequester) {
		$requester = new User($db, $serialUsrRequester);
		$requester->getData();

		$serialMbc = $requester->getSerial_mbc();
		if (!$serialMbc) {
			$tempDealer = new Dealer($db, $requester->getSerial_dea());
			$tempDealer->getData();
			$serialMbc = $tempDealer->getSerial_mbc();
		}

		global $globalBlueCardQuitoMbc;
		$filterOnlyToPASUsers = false;
		if ($serialMbc == '1') {//IS A PLANETASSIST USER
			$serialMbc = $globalBlueCardQuitoMbc; // BLUE CARD QUITO
			$filterOnlyToPASUsers = true;
		}

		if ($parameterValue != 'FILE') {
			$parameter = new Parameter($db, $parameterValue);
			$parameter->getData();
			$destinationManagers = unserialize($parameter->getValue_par());
			$destinationUsers = explode(',', $destinationManagers[$serialMbc]);
		} else { //FILE: SEND TO ASSISTANCE GROUP USERS
			$myGroup = new AssistanceGroupsByUsers($db, $serialUsrRequester);
			$myGroup->autoLoadGroupInfo();

			$usersInGroup = $myGroup->getUsersInGroup();
			$destinationUsers = array();
			foreach ($usersInGroup as $userInfo) {
				$destinationUsers[] = $userInfo['serial_usr'];
			}
		}

		if (count($destinationUsers) < 1) {
			ErrorLog::log($db, $serialMbc . ' MBC NO ALERT USERS FOR PARAM ' . $parameterValue, $destinationManagers);
		}

		if ($filterOnlyToPASUsers) {//PAS USERS ONLY
			foreach ($destinationUsers as $key => &$user) {
				$tempUser = new User($db, $user);
				$tempUser->getData();

				if ($tempUser->getSerial_mbc() != '1') {
					unset($destinationUsers[$key]);
				}
			}
		}

		if (count($destinationUsers) < 1) {
			ErrorLog::log($db, $serialMbc . ' ALERT USERS LIST IS EMPTY ' . $parameterValue . ' WITH ' . $filterOnlyToPASUsers, $destinationManagers);
		}

		return $destinationUsers;
	}

	public static function getAlertedUsers($db, $alertType, $serialUsrRequester) {
		$parameterValue = self::getParameterValueByAlertType($alertType);
		$destinationUsers = self::getAlertUsers($db, $parameterValue, $serialUsrRequester);
		foreach ($destinationUsers as $key => $user) {
			$tempUser = new User($db, $user);
			$tempUser->getData();
			$tempUser = get_object_vars($tempUser);
			unset($tempUser['db']);
			$destinationUsers[$key] = $tempUser;
		}
		return $destinationUsers;
	}

	public static function checkImAuthorizedToAttend($db, $alertId) {
		$tempAlert = new Alert($db, $alertId);
		$tempAlert->getData();
		$userAlertRequester = $tempAlert->getSerial_usr();

		$alertedUsers = self::getAlertedUsers($db, $tempAlert->getType_alt(), $userAlertRequester);

		$userIsAuthorized = false;
		foreach ($alertedUsers as $authorizedUser) {
			if ($authorizedUser['serial_usr'] == $_SESSION['serial_usr']) { //LOGGED USER
				$userIsAuthorized = true;
			}
		}
		return $userIsAuthorized;
	}

	public static function getAlertHistoryForUserReport($db, $serial_usr, $begin_date, $end_date, $status_slg = NULL) {
		if ($status_slg)
			$status_restriction = " AND sl.status_slg = '$status_slg'";

		$sql = "SELECT CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'requester_usr',
						CONCAT(u2.first_name_usr, ' ', u2.last_name_usr) AS 'authorizing_usr',
						s.card_number_sal,
						DATE_FORMAT(sl.date_slg, '%d/%m/%Y') AS 'date_slg',
						sl.description_slg,
						sl.type_slg,
						sl.status_slg,
						IFNULL(i.number_inv, 'N/A') AS 'number_inv',
						IFNULL(name_doc, 'N/A') AS 'name_doc',
						IF( NOW( ) BETWEEN s.begin_date_sal AND s.end_date_sal , 'YES', 'NO' ) AS 'start_coverage',
						NULL AS 'authorizing_obs',
						NULL AS 'authorizing_date'
				FROM sales_log sl
				JOIN user u ON u.serial_usr = sl.serial_usr
				JOIN user u2 ON u2.serial_usr = sl.usr_serial_usr AND sl.usr_serial_usr = $serial_usr
				JOIN sales s ON s.serial_sal = sl.serial_sal
				LEFT JOIN invoice i ON i.serial_inv = s.serial_inv
				LEFT JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
				LEFT JOIN document_type d ON d.serial_doc = dbm.serial_doc
				WHERE sl.date_slg BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') 
						AND DATE_ADD(STR_TO_DATE('$end_date', '%d/%m/%Y'), INTERVAL 1 DAY)
				AND sl.type_slg <> 'VOID_INVOICE'

				UNION

				SELECT CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'requester_usr',
						CONCAT(u2.first_name_usr, ' ', u2.last_name_usr) AS 'authorizing_usr',
						s.card_number_sal,
						DATE_FORMAT(in_date_inl, '%d/%m/%Y') AS 'date_slg',
						observation_inl AS description_slg,
						'VOID_INVOICE' AS type_slg,
						status_inl AS status_slg,
						IFNULL(i.number_inv, 'N/A') AS 'number_inv',
						IFNULL(name_doc, 'N/A') AS 'name_doc',
						IF( NOW( ) BETWEEN s.begin_date_sal AND s.end_date_sal , 'YES', 'NO' ) AS 'start_coverage',
						authorizing_obs_inl AS 'authorizing_obs',
						DATE_FORMAT(modification_date_inl, '%d/%m/%Y') AS 'authorizing_date'
				FROM invoice_log inl
				JOIN invoice i ON i.serial_inv = inl.serial_inv
				JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
				JOIN document_type d ON d.serial_doc = dbm.serial_doc
				JOIN sales s ON s.serial_sal = inl.serial_sal
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				JOIN user u ON u.serial_usr = inl.serial_usr
				JOIN user u2 ON u2.serial_usr = inl.usr_serial_usr  AND inl.usr_serial_usr = $serial_usr
				LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
				WHERE inl.in_date_inl BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$end_date', '%d/%m/%Y'), INTERVAL 1 DAY)";

		//echo '<pre>'.$sql.'</pre>';

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getAssistanceAlertUsers($db) {
		$sql = "SELECT DISTINCT a.serial_usr, CONCAT(u.first_name_usr,' ',u.last_name_usr) as name_usr
				FROM alerts a
				JOIN user u ON a.serial_usr=u.serial_usr
				WHERE a.type_alt='FILE' ORDER BY name_usr ";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getAssistanceAlertByUsersORGroup($db, $assistanceGroup, $alertOwner, $radioChoice, $radioChoiceDate, $from, $to) {
		$sql = "SELECT a.in_date_alt, sal.card_number_sal, 
						CONCAT(cus.first_name_cus,' ',cus.last_name_cus ) as 'PAX', a.status_alt, 
						a.message_alt, a.served_date_alt, ag.name_asg,
						CONCAT(req.first_name_usr, ' ', req.last_name_usr ) as 'requester',
						CONCAT(solv.first_name_usr, ' ', solv.last_name_usr ) as 'solver'
				FROM alerts a
				JOIN file fle ON a.serial_fle=fle.serial_fle
				JOIN sales sal ON sal.serial_sal=fle.serial_sal
				JOIN customer cus ON cus.serial_cus=fle.serial_cus
				
				JOIN assistance_group ag ON ag.serial_asg = a.remote_id
				
				JOIN user req ON req.serial_usr = a.requester_usr
				LEFT JOIN user solv ON solv.serial_usr = a.serial_usr
				WHERE a.type_alt = 'FILE'";

		if ($radioChoice == 'group')
			$sql.=" AND a.remote_id=$assistanceGroup";
		else
		if ($radioChoice == 'owner')
			$sql.=" AND a.serial_usr=$alertOwner";

		if ($radioChoiceDate == 'create')
			$sql.=" AND a.in_date_alt BETWEEN STR_TO_DATE('$from','%d/%m/%Y') AND ADDDATE(STR_TO_DATE('$to','%d/%m/%Y'),INTERVAL 1 DAY)";
		else
		if ($radioChoiceDate == 'served')
			$sql.=" AND a.served_date_alt BETWEEN STR_TO_DATE('$from','%d/%m/%Y') AND ADDDATE(STR_TO_DATE('$to','%d/%m/%Y'),INTERVAL 1 DAY)";

		//die('<pre>' . $sql . '</pre>');
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	///GETTERS
	function getSerial_alt() {
		return $this->serial_alt;
	}

	function getMessage_alt() {
		return $this->message_alt;
	}

	function getType_alt() {
		return $this->type_alt;
	}

	function getStatus_alt() {
		return $this->status_alt;
	}

	function getIn_date_alt() {
		return $this->in_date_alt;
	}

	function getAlert_time_alt() {
		return $this->alert_time_alt;
	}

	function getRemote_id() {
		return $this->remote_id;
	}

	function getTable_name() {
		return $this->table_name;
	}

	function getSerial_usr() {
		return $this->serial_usr;
	}

	function getServed_date_alt() {
		return $this->served_date_alt;
	}

	function getSerial_fle() {
		return $this->serial_fle;
	}

	function getRequester_usr() {
		return $this->requester_usr;
	}

	///SETTERS
	function setSerial_alt($serial_alt) {
		$this->serial_alt = $serial_alt;
	}

	function setMessage_alt($message_alt) {
		$this->message_alt = $message_alt;
	}

	function setType_alt($type_alt) {
		$this->type_alt = $type_alt;
	}

	function setStatus_alt($status_alt) {
		$this->status_alt = $status_alt;
	}

	function setIn_date_alt($in_date_alt) {
		$this->in_date_alt = $in_date_alt;
	}

	function setAlert_time_alt($alert_time_alt) {
		$this->alert_time_alt = $alert_time_alt;
	}

	function setRemote_id($remote_id) {
		$this->remote_id = $remote_id;
	}

	function setTable_name($table_name) {
		$this->table_name = $table_name;
	}

	function setSerial_usr($serial_usr) {
		$this->serial_usr = $serial_usr;
	}

	function setServed_date_alt($served_date_alt) {
		$this->served_date_alt = $served_date_alt;
	}

	function setSerial_fle($serial_fle) {
		$this->serial_fle = $serial_fle;
	}

	function setRequester_usr($requester_usr) {
		$this->requester_usr = $requester_usr;
	}

}

?>