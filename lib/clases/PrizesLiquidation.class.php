<?php
/**
 * @author Nicolas
 */
class PrizesLiquidation{
    var $db;
	var $serial_pli;
	var $serial_pri;
	var $serial_lqn;
	var $quantity_pli;
	var $amount_pli;
	var $cost_pli;

function __construct($db,
					 $serial_pli=NULL,
					 $serial_pri=NULL,
					 $serial_lqn=NULL,
					 $quantity_pli=NULL,
					 $amount_pli=NULL,
					 $cost_pli=NULL
						)
	{
		$this -> db = $db;
		$this -> serial_pli=$serial_pli;
		$this -> serial_pri=$serial_pri;
		$this -> serial_lqn=$serial_lqn;
		$this -> quantity_pli=$quantity_pli;
		$this -> amount_pli=$amount_pli;
		$this -> cost_pli=$cost_pli;
	}

/***********************************************
    * function insert
    * inserts a new register in DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO prizes_liquidation (
				serial_pli,
				serial_pri,
				serial_lqn,
				quantity_pli,
				amount_pli,
				cost_pli
			)
            VALUES (
				NULL,
				'".$this->serial_pri."',
				'".$this->serial_lqn."',
				'".$this->quantity_pli."',
				'".$this->amount_pli."',
				'".$this->cost_pli."')";

		$result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
    	function getPrizesbyLiquidation($serial_lqn){
		$sql = "SELECT pl.*, p.name_pri FROM prizes_liquidation pl, prizes p WHERE p.serial_pri = pl.serial_pri and pl.serial_lqn='".$serial_lqn."'";
                $result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arr;
	}
	/*Getters*/
	function getSerial_pli(){
		return $this->serial_pli;
	}
	function getSerial_pri(){
		return $this->serial_pri;
	}
	function getSerial_lqn(){
		return $this->serial_lqn;
	}
	function getQuantity_pli(){
		return $this->quantity_pli;
	}
	function getAmount_pli(){
		return $this->amount_pli;
	}
	function getCost_pli(){
		return $this->cost_pli;
	}

	/*Setters*/
	function setSerial_pli($serial_pli){
		$this->serial_lqn = $serial_lqn;
	}
	function setSerial_pri($serial_pri){
		$this->serial_pri = $serial_pri;
	}
	function setSerial_lqn($serial_lqn){
		$this->serial_lqn = $serial_lqn;
	}
	function setQuantity_pli($quantity_pli){
		$this->quantity_pli = $quantity_pli;
	}
	function setAmount_pli($amount_pli){
		$this->amount_pli = $amount_pli;
	}
	function setCost_pli($cost_pli){
		$this->cost_pli = $cost_pli;
	}
}
?>
