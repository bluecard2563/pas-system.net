<?php
/**
 * File:  customer_by_freelance
 * Author: Patricio Astudillo
 * Creation Date: 24-ago-2011, 14:37:56
 * Description:
 * Last Modified: 24-ago-2011, 14:37:56
 * Modified by:
 */

class CustomerByFreelance{
	var $db;
	var $serial_cbf;
	var $serial_fre;
	var $serial_cus;
	var $status_cbf;
	var $link_date_cbf;
	var $unlink_date_cbf;
	
	function __construct($db, $serial_cbf = NULL) {
		$this->db = $db;
		$this->serial_cbf = $serial_cbf;
	}
	
	public static function saveRelationship($db, $serial_fre, $serial_cus){
		$sql = "INSERT INTO customer_by_freelance (
						serial_cbf, 
						serial_fre, 
						serial_cus, 
						status_cbf, 
						link_date_cbf, 
						unlink_date_cbf
					) VALUES (
						NULL,
						$serial_fre,
						$serial_cus,
						'ACTIVE',
						NOW(),
						NULL
					)";
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'INSERT FAILED - CustomerByFreelance', $sql);
			return FALSE;
		}
	}
	
	/**
	 * @name clearRelationships
	 * @param type $db
	 * @param type $serial_cus
	 * @return boolean
	 * If the customer has a relationship, update it. 
	 * If not, don't do anything
	 */
	public static function clearRelationships($db, $serial_cus, $serial_fre){
		$serial_cbf = self::hasRelationship($db, $serial_cus, $serial_fre);
		
		if($serial_cbf){
			$sql = "UPDATE customer_by_freelance SET 
						status_cbf = 'INACTIVE',
						unlink_date_cbf = NOW()
					WHERE serial_cbf = $serial_cbf;
					AND serial_cus = $serial_cus";
			
			$result = $db -> Execute($sql);
			
			if($result){
				return self::saveRelationship($db, $serial_fre, $serial_cus);
			}else{
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	public static function hasRelationship($db, $serial_cus, $serial_fre = NULL, $full_information = FALSE){
		$sql = "SELECT serial_cbf
				FROM customer_by_freelance
				WHERE serial_cus = $serial_cus
				AND status_cbf = 'ACTIVE'";
		
		if($serial_fre){
			$sql .= " AND serial_fre <> $serial_fre";
		}
		ErrorLog::log($db, 'Verify Freelance Relationship', $sql);
		$result = $db -> getOne($sql);
		
		if($result){
			if($full_information){
				return Freelance::getComissionsInformation($db, $result);
			}else{
				return $result;
			}			
		}else{
			return FALSE;
		}
	}
	
	public function getSerial_cbf() {
		return $this->serial_cbf;
	}

	public function setSerial_cbf($serial_cbf) {
		$this->serial_cbf = $serial_cbf;
	}

	public function getSerial_fre() {
		return $this->serial_fre;
	}

	public function setSerial_fre($serial_fre) {
		$this->serial_fre = $serial_fre;
	}

	public function getSerial_cus() {
		return $this->serial_cus;
	}

	public function setSerial_cus($serial_cus) {
		$this->serial_cus = $serial_cus;
	}

	public function getStatus_cbf() {
		return $this->status_cbf;
	}

	public function setStatus_cbf($status_cbf) {
		$this->status_cbf = $status_cbf;
	}

	public function getLink_date_cbf() {
		return $this->link_date_cbf;
	}

	public function setLink_date_cbf($link_date_cbf) {
		$this->link_date_cbf = $link_date_cbf;
	}

	public function getUnlink_date_cbf() {
		return $this->unlink_date_cbf;
	}

	public function setUnlink_date_cbf($unlink_date_cbf) {
		$this->unlink_date_cbf = $unlink_date_cbf;
	}
}
?>
