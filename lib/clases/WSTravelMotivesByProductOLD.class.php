<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TravelMotivesByProduct
 *
 * @author Valeria Andino C�lleri
 */
class WSTravelMotivesByProduct {
    
    
        var $db;
	var $serial_tbp;
        var $serial_trm;
	var $serial_pxc;
        var $status_tbp;
	
	
	function __construct($db, $serial_tbp = NULL, $serial_trm=NULL, $serial_pxc = NULL, $status_tbp = NULL){
		$this -> db = $db;
		$this -> serial_tbp = $serial_tbp;
		$this -> serial_trm = $serial_trm;
		$this -> serial_pxc = $serial_pxc;
                $this -> status_tbp = $status_tbp;
		
	}
	
	/***********************************************
        * function getData
        * gets data by serial_trm
        ***********************************************/
        
	function getData(){
		if($this->serial_tbp!=NULL){
			$sql = 	"SELECT serial_trm, serial_pxc, status_tbp
					FROM travelmotives_by_product
					WHERE serial_tbp='".$this->serial_trm."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;
			if($result->fields[0]){
				$this->serial_tbp=$result->fields[0];
                                $this->serial_trm=$result->fields[1];
				$this->serial_pxc=$result->fields[2];
                                $this->status_tbp=$result->fields[3];
				
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
        
        /***********************************************
        * function insert
        * insert a register in DB
        ***********************************************/
        
        function insert(){
		$sql="INSERT INTO travelmotives_by_product (
					serial_trm, 
                                        serial_pxc,
                                        status_tbp
					
					)
			  VALUES(NULL,";
                                        $sql.="'".$this->serial_trm."',
					'".$this->serial_pxc."',
                                        '".$this->status_tbp."'";
                           
                            $result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
        
	function update(){
		$sql="UPDATE travelmotives_by_product
		SET serial_trm='".$this->serial_trm."',
			serial_pxc='".$this->serial_pxc."',
                        status_tbp='".$this->status_tbp."',";
              
		$sql.=" WHERE serial_tbp='".$this->serial_tbp."'";
		
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result==true) 
			return true;
		else
			return false;
	}
        
        /***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
        
        public static function getProductTravelByMotivesByDealer($db,$language,$serial_trm, $serial_cnt){
            
		$sql = 	"SELECT DISTINCT pbl.name_pbl, p.serial_pro
                FROM travel_motives tm 
                JOIN travelmotives_by_product tbp ON tbp.serial_trm = tm.serial_trm
                JOIN product_by_country pbc ON pbc.serial_pxc = tbp.serial_pxc
                JOIN product p ON p.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pbc.serial_pro AND pbl.serial_lang = '$language' 
                JOIN product_by_dealer pbd ON pbd.serial_pxc = pbc.serial_pxc   
                JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_cnt'
                WHERE tm.serial_trm = '$serial_trm'";
		//echo $sql; die();
		$result=$db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
        /******************************VA
		 * @name: getProductsByEcuador
		 * @return: array product by Ecuador
                 * POST: 
		 */
        
        public static function getProductByOriginEqualDEstination($db,$serial_cou, $seniorAllowed){
            
		$sql = 	"SELECT pbl.name_pbl 
                        FROM product_by_country pbc 
                        JOIN product_by_language pbl ON pbc.serial_pro = pbl.serial_pro AND serial_lang = 2
                        WHERE pbc.serial_cou = '$serial_cou'";
                        
                if  ($Olderadults != null)      
                {
                    $sql.= "and pbc.serial_pro = 92";
                }
                else
                {
                    $sql.= "and pbc.serial_pro = 91";
                }    
                
		//echo($sql); die();
                
		$result=$db->getOne($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
         /******************************VA
		 * @name: getProductsByEcuador
		 * @return: array product by Ecuador
                 * POST: 
		 */
        
        public static function getProductByMotiveAndCountry($db, $motive, $destination, $language){
            
		$sql = 	"SELECT DISTINCT pbl.name_pbl , pbl.description_pbl
                        FROM product_by_country pbc 
                        JOIN allowed_destinations ad ON ad.serial_pxc = pbc.serial_pxc AND ad.serial_cou = '$destination'  
                        JOIN travelmotives_by_product tbp ON tbp.serial_pxc = pbc.serial_pxc AND tbp.serial_trm = '$motive'
                        JOIN product pro ON pbc.serial_pro = pro.serial_pro AND pro.in_web_pro = 'YES' AND pro.status_pro = 'ACTIVE'
                        JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
                        where pbl.serial_lang = $language";
		//echo($sql); die();
                $result=$db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
        
        /******************************VA
		 * @name: getBEstProduct
		 * @return: array product by Ecuador
                 * POST: 
		 */
        
        public static function getBestProduct ($db,$language, $serial_cou, $serial_motive, $kids, $many_travel, $senior_allowed, $days, $national, $totalAdults, $zone, $serial_cnt)
        {
                 $days = $days-10;
                 $sql = "SELECT DISTINCT  pbl.name_pbl, pbl.description_pbl, pbc.serial_pxc, pro.serial_pro, pro.children_pro, pro.senior_pro, pro.max_extras_pro, pro.spouse_pro, pro.relative_pro,  pbc.percentage_extras_pxc, pbd.serial_pbd ";
                 
                 $sql2 = " FROM product_by_country pbc
                        JOIN product pro ON pbc.serial_pro = pro.serial_pro  AND pro.in_web_pro = 'YES'
                        AND pro.status_pro = 'ACTIVE'
                        JOIN travelmotives_by_product tmp ON tmp.serial_pxc = pbc.serial_pxc AND tmp.serial_trm = '$serial_motive'
                        JOIN allowed_destinations ad ON ad.serial_pxc = pbc.serial_pxc AND ad.serial_cou = '$serial_cou'   
                        JOIN product_by_language pbl ON pro.serial_pro = pbl.serial_pro AND pbl.serial_lang = '$language'
                        JOIN price_by_product_by_country ppc ON ppc.serial_pxc = pbc.serial_pxc
                        JOIN product_by_dealer pbd ON pbd.serial_pxc = pbc.serial_pxc                          
                        JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                        JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_cnt'
                        AND ppc.duration_ppc >= $days ";
            
                if ($kids=='YES'){
                    $sql2.= "AND pro.children_pro > 0";                  
                } 
                else {
                        $sql2.= "AND pro.children_pro = 0";
                }
                
                if ($totalAdults>=1 || $kids=='YES'){
                    $sql2.= " AND pro.senior_pro = 'NO'";                  
                } 
                //Descomentar para cotizacion con recomendacion
//                if ($many_travel == 'TRUE')
//                {
//                    $sql2.= " AND pro.flights_pro = 0";
//                }else{
//                    $sql2.= " AND pro.flights_pro = 1";
//                }
 
                if ($national == 'YES'){
                     $sql2.= " AND pro.emission_country_pro = 'YES'";
                 }else{
                     $sql2.= " AND pro.emission_country_pro = 'NO'";
                 }       

                if ( $senior_allowed == 'YES' && $totalAdults == 0 && $kids=='NO')
                {   
                    $sql2.= " AND pro.senior_pro = 'YES'"; 
                }  
                   
                elseif (($totalAdults >= 1 || $kids=='YES') && $senior_allowed == 'YES')   {
                    $sql3= " UNION SELECT DISTINCT pbl.name_pbl, pbl.description_pbl, pbc.serial_pxc, pro.serial_pro, pro.children_pro, pro.senior_pro, pro.max_extras_pro, pro.spouse_pro, pro.relative_pro,  pbc.percentage_extras_pxc, pbd.serial_pbd
                            FROM product_by_country pbc JOIN product pro ON pbc.serial_pro = pro.serial_pro 
                            AND pro.in_web_pro = 'YES' AND pro.status_pro = 'ACTIVE' JOIN travelmotives_by_product tmp
                            ON tmp.serial_pxc = pbc.serial_pxc JOIN allowed_destinations ad 
                            ON ad.serial_pxc = pbc.serial_pxc AND ad.serial_cou = '$serial_cou' JOIN product_by_language pbl 
                            ON pro.serial_pro = pbl.serial_pro AND pbl.serial_lang = '$language' 
                            JOIN price_by_product_by_country ppc ON ppc.serial_pxc = pbc.serial_pxc
                            JOIN product_by_dealer pbd ON pbd.serial_pxc = pbc.serial_pxc
                            JOIN dealer dea ON dea.dea_serial_dea = pbd.serial_dea 
                            JOIN counter cnt ON cnt.serial_dea = dea.serial_dea AND cnt.serial_cnt = '$serial_cnt'
                            AND ppc.duration_ppc >= $days AND pro.senior_pro = 'YES'";
                    
                            if ($national == 'YES'){
                             $sql3.= " AND pro.emission_country_pro = 'YES'";
                            }else{
                             $sql3.= " AND pro.emission_country_pro = 'NO'";
                            } 
                }
                
                if ($zone == 1 || $zone == 4 ){
                $sql4 = " AND pro.value_pro = (SELECT Max(pro.value_pro)$sql2)  ORDER BY pbl.name_pbl";}
                else 
                { $sql4 = " AND pro.value_pro = (SELECT Min(pro.value_pro)$sql2)  ORDER BY pbl.name_pbl ";}
                
                //  COTIZACION CON RECOMENDACION
                $sqltotal = $sql.$sql2.$sql4.$sql3;
//                echo $sqltotal; die();
                $result=$db->getAll($sqltotal);
		
		if($result){
			return $result;
		}else{
			return false;
		}
        }
        
         public static function getPriceByProduct ($db, $selProduct, $selDeparture, $selDestination,$selTripDays, $checkPartner, 
                                            $txtBeginDate, $txtEndDate, $txtOverAge, $txtUnderAge, $useDatabase, 
                                            $destinationRestricted, $extrasRestricted, $language, $days, $wService = FALSE){
         
        /*    $Request::setString('selProduct'); $serial_pxc
	Request::setString('selDeparture'); $countryorigin_Id
	Request::setString('selDestination'); $countrydestination_Id
	Request::setString('checkPartner'); $true or false
	Request::setString('selTripDays'); NC
	Request::setString('txtBeginDate'); $startDate
	Request::setString('txtEndDate'); $endDate
	Request::setString('txtOverAge'); $adults
	Request::setString('txtUnderAge'); $kids
	Request::setString('useDatabase'); true false   
	Request::setString('destinationRestricted'); 0 
	Request::setString('extrasRestricted'); SPOUSE
         */ 
	
	unset($errorMessage);
	
	$targetDays = GlobalFunctions::getDaysDiffDdMmYyyy($txtBeginDate, $txtEndDate);
	$useDatabase = $useDatabase == 'true' ? 1 : 0;
	
	if($selTripDays == 'null'){
		$selTripDays = false;
	}
	
	//BUILD UP THE PRODUCT(S) LIST WITH ALL BENEFITS, ALL READY TO BE DISPLAYED:
	$allProducts= array();
        //THIS VALIDATION IS TO AVOID RE INSERTS WHEN GOING THRU NEXT AND PREVIOUS WITHIN THE ESTIMATOR DUE TO PERSISTENT POST DATA.
	if(!$useDatabase){
		if(isset($_SESSION['web_estimator_shopping_cart_products'])){
			$currentCartProducts = $_SESSION['web_estimator_shopping_cart_products'];
			foreach($currentCartProducts as $prod){$allProducts[] = $prod;}
		}
	}
	
	//BUILD UP THE PRODUCT(S) LIST WITH ALL BENEFITS, ALL READY TO BE DISPLAYED:
	$allEstimatorProducts = array();
	//THIS VALIDATION IS TO AVOID RE INSERTS WHEN GOING THRU NEXT AND PREVIOUS WITHIN THE ESTIMATOR DUE TO PERSISTENT POST DATA.
	if(!(isset($_SESSION['data_already_added']) && $useDatabase)){
		if($selProduct == "-1"){
                        if($selDeparture){
			    $product = new Product($db);
			    $products = $product -> getEstimatorProductsListByCountry($selDeparture,$language); //TODO STATIC !!
			}	
                    $allEstimatorProducts = $products;
                        
		}elseif($selProduct){
			$prodByCou = new ProductByCountry ($db, $selProduct);
			$prodByCou -> getData();
			$prodP = new Product($db, $prodByCou -> getSerial_pro());
			$prod = $prodP -> getInfoProduct($language);
			$prod[0]['serial_pxc'] = $selProduct;
			$prod[0]['serial_pro'] = $prodByCou -> getSerial_pro();
			$allEstimatorProducts = $prod;
		}
	}
	
	$newProducts = array();
	
	foreach ($allEstimatorProducts as $prod){
		if($destinationRestricted != '0'){
			$selDestination = $selDeparture;
		}
		switch($extrasRestricted){
			case 'ADULTS':
				$txtUnderAge = 0;
				break;
			case 'CHILDREN':
				//LEAVE FORM INTACT
				break;
			default: //BOTH
				break;
		}
		
		$singleProduct = array(
			'serial_pxc' => $selProduct,
			'name_pbl' => $prod['name_pbl'],
			'departure'=> $selDeparture,
			'destination'=> $selDestination,
			'spouseTraveling'=> $checkPartner,
			'beginDate'=> $txtBeginDate,
			'endDate'=> $txtEndDate,
			'days' => $targetDays,
			'adults' => $txtOverAge,
			'children' => $txtUnderAge,
		);
		
		if(GlobalFunctions::assignProductCalculatedPrices($db, $singleProduct, $selTripDays, $extrasRestricted, $wService) === 'NO_PRICES'){
			$errorMessage = 'NO_PRICES_ERROR'; 
		}
		
		if($useDatabase){
			$error = GlobalFunctions::saveCartProductToDB($db, $singleProduct, $_SESSION['serial_customer_web']);
			if($error != 5){
				ErrorLog::log($db_estimator, 'PRE ORDER SAVE ERROR ' . $error, $singleProduct);
				echo 'Error # ' . $error;
			}
		}
		$newProducts[] = $singleProduct;
	}
	
	$showDates = true;
	if($useDatabase){
		$preOrder = new PreOrder($db_estimator, NULL, $_SESSION['serial_customer_web'], 'CUSTOMER', time());
		$allProducts = $preOrder -> getAllMyCartProductItems ($language);
		$_SESSION['data_already_added'] = true;
		$referenceBeginDate = $allProducts[0]['begin_date_pod'];
		$referenceEndDate = $allProducts[0]['end_date_pod'];
		foreach($allProducts as &$prodMy){
			$prodByCou = new ProductByCountry($db_estimator, $selProductXC);
			$prodByCou -> getData();
			$product = new Product($db_estimator, $prodByCou -> getSerial_pro());
			$product -> getData();
			$prodMy['schengen'] =  $product -> getSchengen_pro();
			
			if($prodMy['begin_date_pod'] != $referenceBeginDate || $prodMy['end_date_pod'] != $referenceEndDate){
				$showDates = false;
			}
		}
		$txtBeginDate = $allProducts[0]['begin_date_pod'];
		$txtEndDate = $allProducts[0]['end_date_pod'];
		$targetDays = $allProducts[0]['days_pod'];
	}else{
		//WE MIX CART & SESSION PRODUCTS ONLY TO BE DISPLAYED BUT BEHIND WE STILL HAVE 2 LISTS!
		foreach ($newProducts as &$newP){
			$tempPXC = new ProductByCountry($db,$selProductXC);
			$tempPXC -> getData();
			
			$prodSc = new Product($db, $selProduct);
			$prodSc -> getData();
			
			//WE SET THE FIRST BENFIT TO BE DISPLAYED:
			$benefitsByProduct = new BenefitsByProduct($db); 
			$benefitsByProduct -> setSerial_pro($prodSc -> getSerial_pro());
			$allBenefits = $benefitsByProduct -> getMyBenefits($language);
			$ben = $allBenefits[0];
			
			$newP['schengen'] =  $prodSc -> getSchengen_pro();
			$newP['symbol_cur'] = $ben['symbol_cur'];
			$newP['pro_benefit_name'] = $ben['description_bbl'];
			if($ben['price_bxp'] == '0.00'){
				$newP['pro_benefit_cost'] = $ben['description_cbl'];
			}else{
				$newP['pro_benefit_cost'] = $ben['symbol_cur'].' '.$ben['price_bxp'];
			}
			if($ben['restriction_price_bxp'] > 0){
				$newP['pro_benefit_cost'] .= '(' . $ben['symbol_cur'] . ' ' . $ben['restriction_price_bxp'] . ' ' . $ben['description_rbl'] . ')';
			}
			$allProducts[] = $newP;
		}
		$targetDays = $allProducts[0]['days'];
		$referenceBeginDate = $allProducts[0]['beginDate'];
		$referenceEndDate = $allProducts[0]['endDate'];
		foreach($allProducts as $prodMy){
			if($prodMy['beginDate'] != $referenceBeginDate || $prodMy['endDate'] != $referenceEndDate){
				$showDates = false;
			}
		}
		$txtBeginDate = $allProducts[0]['beginDate'];
		$txtEndDate = $allProducts[0]['endDate'];
		//SAVE SESSION ARRAY:
		$_SESSION['web_estimator_session_products'] = $newProducts;
	}
	
	//SUM ALL PRICES:
	//$totalEstimatorPrice = 0;
	foreach($allProducts as $prodSingle){
		$totalEstimatorPrice += $prodSingle['total_price_pod'];
	}
        
        return $totalEstimatorPrice;                                  
        }
        
        
}
