<?php
/*
 * File: GraphicPDFFucnctions
 * Author: Santiago Borja
 * Creation Date: 02/05/2011
*/

class GraphicPDFFunctions{
	var $pdf;
	var $boxTop;
	
	 function __construct($pdf){
	 	$this -> pdf = $pdf;
	 }
	 
	 function setBoxTop($boxTop){
	 	$this -> boxTop = $boxTop;
	 }
	 
	function write_red_box($x, $y){
		global $red;
		
		$this -> pdf -> setColor($red[0], $red[1], $red[2]);
		$this -> pdf -> filledRectangle($x, $y - 5, 10, 10);
	}
	
	function write_title_box($title, $x, $y){
		self::_write_general_title_box($title, $x, $y);
	}
	
	function write_gray_title_box($title, $x, $y){
		self::_write_general_title_box($title, $x + 5, $y);
	}
	
	function _write_general_title_box($title, $x, $y){
		global $rimacBckGnd;
		global $smallTextSize;
		global $white;
		
		$this -> pdf -> setColor($rimacBckGnd[0], $rimacBckGnd[1], $rimacBckGnd[2]);
		$this -> pdf -> filledRectangle($x - 5, $y - 5, 600 - $x - 50, 10);
		$this -> pdf -> setColor($white[0], $white[1], $white[2]);
		$this -> pdf -> addTextWrap($x, $y - 3, 200, $smallTextSize, html_entity_decode($title));
	}
	
	
	//********************************** DOUBLE TEXT COLUMNS ********************************//
	function write_single_column($title, $text, $y){
		self::_write_general_column($title, $text, 15, $y, TRUE);
	}
	
	function write_single_column_left($title, $text, $y){
		self::_write_general_column($title, $text, 15, $y);
	}
	
	function write_single_column_right($title, $text, $y){
		self::_write_general_column($title, $text, 250, $y);
	}
	
	function _write_general_column($title, $text, $x, $y, $textUnlimited = false){
		global $leftMostX;
		global $smallTextSize;
		
		$textOffset = $textUnlimited ? 250 : 100;
		
		$this -> pdf -> setColor($black[0], $black[1], $black[2]);
		$this -> pdf -> addTextWrap($leftMostX + $x, $y, 100 + $textOffset, $smallTextSize, html_entity_decode("<b>$title</b>"));
		$this -> pdf -> addTextWrap($leftMostX + $x + $textOffset, $y, 200, $smallTextSize, $text);
	}
	
	//********************************** TEXT COLUMNS ********************************//
	function write_signature_lines($y){
		global $black;
		$this -> pdf -> setStrokeColor($black[0], $black[1], $black[2]);
		$this -> pdf -> setLineStyle(0.2, 'square');
		$this -> pdf -> line(100, $y, 230, $y);
		$this -> pdf -> line(300, $y, 430, $y);
	}
	
	function write_left_column($text, $y){
		self::_write_general_single_column($text, $y, TRUE);
	}
	
	function write_right_column($text, $y){
		self::_write_general_single_column($text, $y);
	}
	
	function _write_general_single_column($text, $y, $leftColumn = false){
		global $leftMostX;
		global $smallTextSize;
		
		$textOffset = $leftColumn ? 0 : 200;
		
		$this -> pdf -> setColor($black[0], $black[1], $black[2]);
		$this -> pdf -> addTextWrap($leftMostX + $textOffset, $y, 250, $smallTextSize, html_entity_decode($text), 'center');
	}
	
	//********************************** BENEFITS TABLE COLUMNS ********************************//
	function write_wide_title_left($title, $y, $useRed = false, $center = false){
		self::_write_general_title($title, 0, $y, 265, $useRed, true, $center);
	}
	
	function write_short_title_right($title, $y, $useRed = false, $center = false){
		self::_write_general_title($title, 270, $y, 225, $useRed, true, $center);
	}
	
	function write_wide_text_left($title, $y, $useRed = false){
		self::_write_general_title($title, 0, $y, 200, $useRed, false);
	}
	
	function write_short_text_right($title, $y, $useRed = false){
		self::_write_general_title($title, 270, $y, 150, $useRed, false, true);
	}
	
	function _write_general_title($title, $x, $y, $topRight, $useRed = false, $paintBackground = true, $center = false){
		global $rimacBckGnd;
		global $red;
		global $black;
		global $white;
		global $smallTextSize;
		global $leftMostX;
		
		if($useRed){ $this -> pdf -> setColor($red[0], $red[1], $red[2]);
		}else{ $this -> pdf -> setColor($rimacBckGnd[0], $rimacBckGnd[1], $rimacBckGnd[2]); }
		
		if($paintBackground){
			$this -> pdf -> filledRectangle($leftMostX + $x + 5, $y - 5, $topRight, 10);
			$this -> pdf -> setColor($white[0], $white[1], $white[2]);
		}else{
			$this -> pdf -> setColor($black[0], $black[1], $black[2]);
		}
		
		$alignText = $center ? 'center' : 'full';
		$this -> pdf -> addTextWrap($leftMostX + $x + 10, $y - 3, 200, $smallTextSize, html_entity_decode($title), $alignText);
	}
	
	
	//********************************** PLAIN TEXT BLOCK ********************************//
	function write_text_block($title, $verticalPointer, $alignment){
		global $black;
		global $leftMostX;
		global $smallTextSize;
		global $max_height;
		
		$this -> pdf -> setColor($black[0], $black[1], $black[2]);
		
		while(strlen($title)){
			$title = $this -> pdf -> addTextWrap($leftMostX + 5, $verticalPointer, 490, $smallTextSize, html_entity_decode($title), $alignment);
			$verticalPointer -= 10;
			if($verticalPointer <= 100){
				self::write_box($verticalPointer);
				$this -> pdf -> ezNewPage();
				$verticalPointer = $max_height;
				$this -> boxTop = $verticalPointer;
			}
		}
		return $verticalPointer;
	}


	//********************************** GRAY BOXES ********************************//
	function write_box($boxBottom){
		global $leftMostX;
		global $rimacBckGnd;
		
		$this -> pdf -> setLineStyle(0.1,'square');
		$this -> pdf -> setStrokeColor($rimacBckGnd[0], $rimacBckGnd[1], $rimacBckGnd[2]); 
		
		// |_|
		$boxBottom -= 5;
		$this -> pdf -> line($leftMostX, $this -> boxTop, $leftMostX, $boxBottom);// |.
		$this -> pdf -> line($leftMostX, $boxBottom, $leftMostX + 505, $boxBottom);// _
		$this -> pdf -> line($leftMostX + 505, $this -> boxTop, $leftMostX + 505, $boxBottom);//  .|
	}
}
?>