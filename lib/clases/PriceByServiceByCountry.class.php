<?php
/*
File: PriceByServiceByCountry.class.php
Author: Miguel Ponce.
Creation Date: 15/03/2010
Last Modified: 
Modified By: 
*/

class PriceByServiceByCountry{
    var $db;
    var $serial_psc;
    var $serial_sbc;
    var $price_psc;
    var $cost_psc;
    
    function __construct($db, $serial_psc=NULL, $serial_sbc=NULL, $price_psc=NULL, $cost_psc=NULL){
        $this -> db = $db;
        $this -> serial_psc = $serial_psc;
        $this -> serial_sbc = $serial_sbc;
        $this -> price_psc = $price_psc;
        $this -> cost_psc = $cost_psc;
    }
    
    /** 
    @Name: getData
    @Description: Retrieves the data of a Dealer object.
    @Params: The ID of the object
    @Returns: TRUE if O.K. / FALSE on errors
    **/
    function getData(){
        if($this->serial_ppc!=NULL){
            $sql = "SELECT serial_ppc, serial_pxc, duration_ppc, price_ppc, cost_ppc, min_ppc, max_ppc
                            FROM price_by_product_by_country
                            WHERE serial_ppc='".$this->serial_ppc."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_ppc = $result->fields[0];
                $this -> serial_pxc = $result->fields[1];
                $this -> duration_ppc = $result->fields[2];
                $this -> price_ppc = $result->fields[3];
                $this -> cost_ppc = $result->fields[4];
                $this -> min_ppc = $result->fields[5];
                $this -> max_ppc = $result->fields[6];

                return true;
            }else
                    return false;
        }else
            return false;		
    }

    function getDataBySerial_sbc(){
        if($this->serial_sbc!=NULL){
            $sql = "SELECT  serial_psc,
                            serial_sbc,
                            price_psc,
                            cost_psc
                        FROM price_by_service_by_country
                        WHERE serial_sbc = '".$this->serial_sbc."'";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_psc = $result->fields[0];
                $this -> serial_sbc = $result->fields[1];
                $this -> price_psc = $result->fields[2];
                $this -> cost_psc = $result->fields[3];
                return true;
            }else
                    return false;
        }else
            return false;
    }


    /***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO price_by_service_by_country (
                serial_psc,
                serial_sbc,
                price_psc,
                cost_psc)
            VALUES (
                    NULL,
                    '".$this->serial_sbc."',
                    '".$this->price_psc."',
                    '".$this->cost_psc."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
    /***********************************************
    * funcion update
    * updates a register in DB
    **********************************************
    function update() {
        if($this->serial_pxc != NULL){
            $sql = "
                    UPDATE product_by_country SET
                            serial_cou = '".$this->serial_cou."',
                            percentage_spouse_pxc ='".$this->percentage_spouse_pxc."',
                            percentage_extras_pxc ='".$this->percentage_extras_pxc."',
                            percentage_children_pxc ='".$this->percentage_children_pxc."'
                    WHERE serial_pxc = '".$this->serial_pxc."'";

            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }*/

    /***********************************************
    * funcion detele
    * deletes a register in DB
    ***********************************************/
    function deleteByProduct($serial_pro) {
        $sql = "    DELETE
                    FROM price_by_product_by_country
                    WHERE serial_pxc
                        IN (    SELECT serial_pxc
                                FROM product_by_country 
                                WHERE serial_pro='".$serial_pro."'
								AND serial_cou=1)";
        
		//echo $sql;die;
        $result = $this->db->Execute($sql);
        if ($result === false)return false;

        if($result==true){
            return true;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion delete
    * deletes a register in DB
    ***********************************************/
    function delete() {
        if($this->serial_pxc != NULL){
            $sql = "DELETE
                    FROM price_by_product_by_country
                    WHERE serial_pxc = '".$this->serial_pxc."'";


            //echo $sql;die;
            $result = $this->db->Execute($sql);
            if ($result === false)return false;

            if($result==true){
                return true;
            }else{
                return false;
            }
        }
    }

    /***********************************************
    * funcion getAssignedCountries
    * gets all the Countries assigened to a product
    ***********************************************/
    function getPricesByProduct($serial_pro){
        $sql = 	"   SELECT  ppc.duration_ppc, ppc.price_ppc, ppc.cost_ppc, ppc.min_ppc, ppc.max_ppc, ppc.serial_pxc
                    FROM price_by_product_by_country ppc
                        JOIN product_by_country pxc
                            ON pxc.serial_pxc = ppc.serial_pxc
                            AND pxc.serial_pro = '".$serial_pro."'
                    ORDER BY ppc.duration_ppc";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arreglo;
    }

    /*
     * @function: getPricesByProductByCountry
     * @Description: Retrieves the fees of all the
     * @params: $serial_pxc: Product By Country ID
     *          $serial_dea: Dealer ID
     *          $serial_cou: Dealer Country ID
     * @returns: Array of fees acording to a country.
     */
    function getPricesByProductByCountry($serial_pro, $serial_cou, $days=NULL){
        $sql="SELECT ppc.*
              FROM price_by_product_by_country ppc
              WHERE ppc.serial_pxc = (SELECT pxc.serial_pxc
                                    FROM product_by_country pxc
                                    WHERE pxc.serial_cou = ".$serial_cou."
                                    AND pxc.serial_pro = ".$serial_pro.")";
        if($days){
              $sql .= " AND ppc.duration_ppc BETWEEN ".($days-10)." AND ".($days+10);
         }
//echo $sql;
        $result = $this->db->Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        //Debug::print_r($arreglo);
        return $arreglo;
    }

    /*
     * @function: getMaxPricePosible
     * @Description: Retrieves the next available fee according to a specific amount of days.
     * @params: $serial_pxc: Product By Country ID
     *          $serial_dea: Dealer ID
     *          $serial_cou: Dealer Country ID
     * @returns: The Price ID if OK / FALSE otherwise.
     */
    function getMaxPricePosible($serial_pro, $days, $serial_cou){
        $sql="SELECT MIN(duration_ppc), serial_ppc
              FROM price_by_product_by_country ppc
              WHERE ppc.serial_pxc = (SELECT pxc.serial_pxc
                                      FROM product_by_country pxc
                                      WHERE pxc.serial_cou = ".$serial_cou."
                                      AND pxc.serial_pro = ".$serial_pro.")
              AND duration_ppc > ".$days;

        //echo $sql;
        $result = $this->db->Execute($sql);
        
        if($result->fields[0]){
            return $result->fields[1];
        }else{
            return false;
        }
    }

    /*
     * @Name: getProductByCountrySerials
     * @Description: Returns an array of product by country serials for a specific list of products and
     *               countries. This serials are going to be used in the Promotions module.
     * @Params: serial_cou: Specific Country
     *          productList: List of products that are sold in that country
     * @Returns: Country array
     *
    function getProductByCountrySerials($serial_cou, $productList){
        $sql = 	"SELECT pbc.serial_pxc
                FROM product_by_country pbc
                WHERE pbc.serial_cou = '".$serial_cou."'
                AND pbc.serial_pro IN (".$productList.")";
                //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

    function productByCountryExists(){
        if($this->serial_pro != NULL && $this->serial_cou != NULL){
            $sql = "SELECT pbc.serial_pxc
                    FROM product_by_country pbc
                    WHERE pbc.serial_pro = '".$this->serial_pro."'
                    AND pbc.serial_cou = '".$this->serial_cou."'";

            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }*/
    ///GETTERS

    function getSerial_psc(){
        return $this->serial_psc;
    }
    function getSerial_sbc(){
        return $this->serial_sbc;
    }
    function getPrice_psc(){
        return $this->price_psc;
    }
    function getCost_psc(){
        return $this->cost_psc;
    }
   

    ///SETTERS

    function setSerial_psc($serial_psc){
        $this->serial_psc = $serial_psc;
    }
    function setSerial_sbc($serial_sbc){
        $this->serial_sbc = $serial_sbc;
    }
    function setPrice_psc($price_psc){
        $this->price_psc = $price_psc;
    }
    function setCost_psc($cost_psc){
        $this->cost_psc = $cost_psc;
    }
}
?>