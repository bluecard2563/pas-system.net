<?php
/**
 * File: AllowedDestinations
 * Author: Patricio Astudillo
 * Creation Date: 18-feb-2013
 * Last Modified: 18-feb-2013
 * Modified By: Patricio Astudillo
 */


class AllowedDestinations{	
	/**
	 * @name enableAccess
	 * @param type $db
	 * @param type $serial_pxc
	 * @param type $asignation
	 * @param array $country_array
	 * @return boolean 
	 */
	public static function enableAccess($db, $serial_pxc, $asignation = TRUE, $country_array = NULL){
		self::removeAllAccess($db, $serial_pxc);
		
		if(!$country_array){
			$sql = "SELECT serial_cou 
					FROM country";
			
			if(!$asignation):
				$sql .= " WHERE serial_cou <> 1";
			endif;
			
			$temp_array = $db ->getAll($sql);
			$country_array = array();
			
			foreach($temp_array as $t){
				array_push($country_array, $t['serial_cou']);
			}
		}
		
		if(is_array($country_array)){
			foreach($country_array as $serial_cou){
				$sql_insert = "INSERT INTO allowed_destinations (serial_cou, serial_pxc) 
									VALUES ($serial_cou, $serial_pxc)";

				$result = $db->Execute($sql_insert);

				if(!$result){
					ErrorLog::log($db, 'REGISTER COUNTRY FAILED', $sql_insert);
					break;
				}
			}

			return $result;
		}else{
			ErrorLog::log($db, 'REGISTER COUNTRY FAILED', 'NO COUNTRY ARRAY');
			return FALSE;
		}
	}
	
	/**
	 * @name removeAllAccess
	 * @param type $db
	 * @param type $serial_pxc
	 * @return boolean 
	 */
	public static function removeAllAccess($db, $serial_pxc){
		$sql = "DELETE FROM allowed_destinations WHERE serial_pxc = $serial_pxc";
		
		$result = $db -> Execute($sql);
		
		if($result){
			return TRUE;
		}else{
			ErrorLog::log($db, 'REVOKE SALE ACCESS FAILED', $sql);
			return FALSE;
		}
	}
	
	/**
	 * @name canSellProductToDestination
	 * @param type $db
	 * @param type $serial_pxc
	 * @param type $serial_cou
	 * @return boolean 
	 */
	public static function canSellProductToDestination($db, $serial_pxc, $serial_cou){
		$sql = "SELECT serial_cou
				FROM allowed_destinations
				WHERE serial_cou = $serial_cou
				AND serial_pxc = $serial_pxc";
		
		$result = $db ->getOne($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	/**
	 * @name canProceedMultiSaleWithDestinationRestriction
	 * @param type $db
	 * @param type $serial_pbd
	 * @param type $serial_cit
	 * @return boolean 
	 */
	public static function canProceedMultiSaleWithDestinationRestriction($db, $serial_pbd, $serial_cit){
		$sql = "SELECT cit.serial_cou
				FROM allowed_destinations ald
				JOIN product_by_dealer pbd ON pbd.serial_pxc = ald.serial_pxc AND pbd.serial_pbd = $serial_pbd
				JOIN city cit ON cit.serial_cou = ald.serial_cou AND cit.serial_cit = $serial_cit";
		
		$result = $db ->getOne($sql);
		
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public static function getCurrentDestinations($db, $serial_pxc, $serials_only = TRUE){
		$sql = "SELECT ad.serial_cou, name_cou
				FROM allowed_destinations ad
				JOIN country c ON c.serial_cou = ad.serial_cou
				WHERE ad.serial_pxc = $serial_pxc";
		
		$result = $db -> getAll($sql);
		
		if($result){
			if($serials_only){
				$serials_array = array();
				foreach($result as $r){
					array_push($serials_array, $r['serial_cou']);
				}
				
				$result = $serials_array;
			}
			
			return $result;
		}else{
			return FALSE;
		}
	}
}
?>
