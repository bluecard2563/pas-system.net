<?php
/*
Author: Santiago Borja
Creation date: 12/04/2009
*/

class PreOrderDetail {
	var $db;
	var $serial_pod;
	var $serial_por;
	var $serial_cou;
	var $serial_pxc;
	var $begin_date_pod;
	var $end_date_pod;
	var $days_pod;
	var $adults_pod;
	var $children_pod;
	var $spouse_pod;
	var $cost_pod;
	var $cost_children_pod;
	var $cost_spouse_pod;
	var $cost_extra_pod;
	var $price_pod;
	var $price_children_pod;
	var $price_spouse_pod;
	var $price_extra_pod;
	var $total_price_pod;
    var $cards_pod;
	var $departure_cou;
	
	function __construct($db,	$serial_pod = NULL,
								$name_pro = NULL,
								$serial_por = NULL,
								$serial_cou = NULL,
								$serial_pxc = NULL,
								$begin_date_pod = NULL,
								$end_date_pod= NULL,
								$days_pod= NULL,
								$adults_pod= NULL,
								$children_pod= NULL,
								$spouse_pod= NULL,
								$cost_pod= NULL,
								$cost_children_pod= NULL,
								$cost_spouse_pod= NULL,
								$cost_extra_pod= NULL,
								$price_pod= NULL,
								$price_children_pod = NULL,
								$price_spouse_pod = NULL,
								$price_extra_pod = NULL,
								$total_price_pod = NULL,
								$cards_pod = NULL,
								$departure_cou = NULL){

		$this -> db = $db;
		$this -> serial_pod = $serial_pod;
		$this -> name_pro = $name_pro;
		$this -> serial_por = $serial_por;
		$this -> serial_cou = $serial_cou;
		$this -> serial_pxc = $serial_pxc;
		$this -> begin_date_pod = $begin_date_pod;
		$this -> end_date_pod = $end_date_pod;
		$this -> days_pod = $days_pod;
		$this -> adults_pod = $adults_pod;
		$this -> children_pod = $children_pod;
		$this -> spouse_pod = $spouse_pod;
		$this -> cost_pod = $cost_pod;
		$this -> cost_children_pod = $cost_children_pod;
		$this -> cost_spouse_pod = $cost_spouse_pod;
		$this -> cost_extra_pod = $cost_extra_pod;
		$this -> price_pod = $price_pod;
		$this -> price_children_pod = $price_children_pod;
		$this -> price_spouse_pod = $price_spouse_pod;
		$this -> price_extra_pod = $price_extra_pod;
		$this -> total_price_pod = $total_price_pod;
		$this -> cards_pod = $cards_pod;
		$this -> departure_cou = $departure_cou;
	}

	/* function getData() 		*/
	/* set class parameters		*/
	
	function getData(){
		if($this->serial_pod!=NULL){
			$sql = 	"SELECT     serial_pod,
                                            serial_por,
                                            serial_cou,
                                            serial_pxc,
                                            UNIX_TIMESTAMP(begin_date_pod),
                                            UNIX_TIMESTAMP(end_date_pod),
                                            days_pod,
                                            adults_pod,
                                            children_pod,
                                            spouse_pod,
                                            cost_pod,
                                            cost_spouse_pod,
                                            cost_extra_pod,
                                            price_pod,
                                            price_children_pod,
											price_spouse_pod,
											price_extra_pod,
											total_price_pod,
											cards_pod,
											departure_cou
					FROM pre_order_detail
					WHERE serial_pod='".$this->serial_pod."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_pod=$result->fields[0];
				$this->serial_por=$result->fields[1];
				$this->serial_cou=$result->fields[2];
				$this->serial_pxc=$result->fields[3];
				$this->begin_date_pod=$result->fields[4];
				$this->end_date_pod=$result->fields[5];
				$this->days_pod=$result->fields[6];
				$this->adults_pod=$result->fields[7];
				$this->children_pod=$result->fields[8];
				$this->spouse_pod=$result->fields[9];
				$this->cost_pod=$result->fields[10];
				$this->cost_spouse_pod=$result->fields[11];
				$this->cost_extra_pod=$result->fields[12];
				$this->price_pod=$result->fields[13];
				$this->price_children_pod=$result->fields[14];
				$this->price_spouse_pod=$result->fields[15];
				$this->price_extra_pod=$result->fields[16];
				$this->total_price_pod=$result->fields[17];
                $this->cards_pod=$result->fields[18];
				$this->departure_cou=$result->fields[19];
				return true;
			}	
		}
		return false;		
	}
	
	/* function insert() 						*/
	/* insert a new pre_order_detail type into database	*/
	
	function insert(){
		$sql="INSERT INTO pre_order_detail (
					serial_pod,
					serial_por,
					serial_cou,
					departure_cou,
					serial_pxc,
					begin_date_pod,
					end_date_pod,
					days_pod,
					adults_pod,
					children_pod,
					spouse_pod,
					cost_pod,
					cost_children_pod,
					cost_spouse_pod,
					cost_extra_pod,
					price_pod,
					price_children_pod,
					price_spouse_pod,
					price_extra_pod,
					total_price_pod,
					cards_pod)
			  VALUES(NULL,
					'".$this -> serial_por."',
					'".$this -> serial_cou."',
					'".$this -> departure_cou."',
					'".$this -> serial_pxc."',
					STR_TO_DATE('".$this->begin_date_pod."','%d/%m/%Y'),
					STR_TO_DATE('".$this->end_date_pod."','%d/%m/%Y'),
					'".$this -> days_pod."',
					'".$this -> adults_pod."',
					'".$this -> children_pod."',
					'".$this -> spouse_pod."',
					".($this -> cost_pod == NULL?"NULL":"'".$this -> cost_pod."'").",
					".($this -> cost_children_pod == NULL?"NULL":"'".$this -> cost_children_pod."'").",
					".($this -> cost_spouse_pod == NULL?"NULL":"'".$this -> cost_spouse_pod."'").",
					".($this -> cost_extra_pod == NULL?"NULL":"'".$this -> cost_extra_pod."'").",
					".($this -> price_pod == NULL?"NULL":"'".$this -> price_pod."'").",
					'".$this -> price_children_pod."',
					'".$this -> price_spouse_pod."',
					'".$this -> price_extra_pod."',
					'".$this -> total_price_pod."',
					'".$this -> cards_pod."');";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/* function update() 						*/
	/* update a pre_order_detail type 					*/

	function update(){
	
		if($this->serial_pod!=NULL){

			$sql="UPDATE pre_order_detail
			SET serial_por='".$this->serial_por."',
				serial_cou='".$this->serial_cou."',
				departure_cou='".$this->departure_cou."'
				serial_pxc='".$this->serial_pxc."',
				begin_date_pod=FROM_UNIXTIME('".$this->begin_date_pod."'),
				end_date_pod=FROM_UNIXTIME('".$this->end_date_pod."'),
				days_pod='".$this->days_pod."',
				adults_pod='".$this->adults_pod."',
				children_pod='".$this->children_pod."',
				spouse_pod='".$this->spouse_pod."',
				cost_pod='".$this->cost_pod."',
				cost_children_pod='".$this->cost_children_pod."',
				cost_spouse_pod='".$this->cost_spouse_pod."',
				cost_extra_pod='".$this->cost_extra_pod."',
				price_pod='".$this->price_pod."',
				price_pod='".$this->price_children_pod."',
				price_pod='".$this->price_spouse_pod."',
				price_pod='".$this->price_extra_pod."',
				price_pod='".$this->total_price_pod."',
				price_pod='".$this->cards_pod."'
			WHERE serial_pod='".$this->serial_pod."'";
    			$result = $this->db->Execute($sql);
			if ($result==true) 
				return true;
		}
		return false;		
	}
	
	public static function deletePOD($db, $serial_pod){
		$sql="DELETE FROM pre_order_detail WHERE serial_pod='".$serial_pod."'";
		$result = $db->Execute($sql);
		return $result;
	}
	
	///GETTERS
	function getSerial_pod(){
		return $this->serial_pod;
	}
	function getName_pro(){
		return $this->name_pro;
	}
	function getSerial_por(){
		return $this->serial_por;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getSerial_pxc(){
		return $this->serial_pxc;
	}
	function getBegin_date_pod(){
		return $this->begin_date_pod;
	}
	function getEnd_date_pod(){
		return $this->end_date_pod;
	}
	function getDays_pod(){
		return $this->days_pod;
	}
	function getAdults_pod(){
		return $this->adults_pod;
	}
	function getChildren_pod(){
		return $this->children_pod;
	}
	function getSpouse_pod(){
		return $this->spouse_pod;
	}
	function getCost_pod(){
		return $this->cost_pod;
	}
	function getCost_children_pod(){
		return $this->cost_children_pod;
	}
	function getCost_spouse_pod(){
		return $this->cost_spouse_pod;
	}
	function getCost_extra_pod(){
		return $this->cost_extra_pod;
	}
	function getPrice_pod(){
		return $this->price_pod;
	}
	function getPrice_children_pod(){
		return $this->price_children_pod;
	}
	function getPrice_spouse_pod(){
		return $this->price_spouse_pod;
	}
	function getPrice_extra_pod(){
		return $this->price_extra_pod;
	}
	function getTotal_price_pod(){
		return $this->total_price_pod;
	}
	function getCards_pod(){
		return $this->cards_pod;
	}

	///SETTERS	
	function setSerial_pod($serial_pod){
		$this->serial_pod = $serial_pod;
	}
	function setName_pro($name_pro){
		$this->name_pro = $name_pro;
	}
	function setSerial_por($serial_por){
		$this->serial_por = $serial_por;
	}
	function setSerial_cou($serial_por){
		$this->serial_cou = $serial_por;
	}
	function setSerial_pxc($serial_pxc){
		$this->serial_pxc = $serial_pxc;
	}
	function setBegin_date_pod($begin_date_pod){
		$this->begin_date_pod = $begin_date_pod;
	}	
	function setEnd_date_pod($end_date_pod){
		$this->end_date_pod = $end_date_pod;
	}
	function setDays_pod($days_pod){
		$this->days_pod = $days_pod;
	}
	function setAdults_pod($adults_pod){
		$this->adults_pod = $adults_pod;
	}
	function setChildren_pod($children_pod){
		$this->children_pod = $children_pod;
	}
	function setSpouse_pod($spouse_pod){
		$this->spouse_pod = $spouse_pod;
	}
	function setCost_pod($cost_pod){
		$this->cost_pod = $cost_pod;
	}
	function setCost_children_pod($cost_children_pod){
		$this->cost_children_pod = $cost_children_pod;
	}
	function setCost_spouse_pod($cost_spouse_pod){
		$this->cost_spouse_pod = $cost_spouse_pod;
	}
	function setCost_extra_pod($cost_extra_pod){
		$this->cost_extra_pod = $cost_extra_pod;
	}
	function setPrice_pod($price_pod){
		$this->price_pod = $price_pod;
	}
	function setPrice_children_pod($price_children_pod){
		return $this->price_children_pod = $price_children_pod;
	}
	function setPrice_spouse_pod($price_spouse_pod){
		return $this->price_spouse_pod = $price_spouse_pod;
	}
	function setPrice_extra_pod($price_extra_pod){
		return $this->price_extra_pod = $price_extra_pod;
	}
	function setTotal_price_pod($total_price_pod){
		return $this->total_price_pod = $total_price_pod;
	}
	function setCards_pod($cards_pod){
		return $this->cards_pod = $cards_pod;
	}
	public function getDeparture_cou() {
		return $this->departure_cou;
	}

	public function setDeparture_cou($departure_cou) {
		$this->departure_cou = $departure_cou;
	}
}
?>