<?php
/*
File: WebContent.php
Author: Santiago Borja Lopez
Creation Date: 04/03/2010
Last Modified: 04/03/2010
Modified By: Santiago Borja Lopez
*/
class WebContent{				
	var $db;
	var $serial_wct;
	var $date_wct;
		
	function __construct($db, $serial_wct=NULL, $date_wct = NULL){
		$this -> db = $db;
		$this -> serial_wct = $serial_wct;
		$this -> date_wct = $date_wct;
	}
	
	/***********************************************
    * funcion getData
    * sets data by serial_wct
    ***********************************************/
	function getData(){
		if($this->serial_wct!=NULL){
				$sql = 	"	SELECT n.serial_wct, n.date_wct
							FROM web_content
							WHERE n.serial_wct = ".$this-> serial_wct;
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_wct =$result->fields[0];
				$this -> date_wct = $result->fields[1];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * funcion getAllWebContents
    * gets information from All WebContents 
    ***********************************************/
    function getAllWebContents(){
        $sql = 	"SELECT serial_wct, date_wct 
        		 FROM web_content";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getAllWebContentsTree
    * gets information from All Web contents as a tree where translated web_content are children of the original Contentes
    * THE DEFAULT VALUE FOR PARENT CONTENTS IS SPANISH, IT CAN BE CHANGED BUT IT MUST HAVE A VALUE
    ***********************************************/
    function getAllWebContentsTree(){
    	//DEFAULT PARENT LANGUAGE
    	$parentLang = 'es';
        $sql = 	"SELECT wc.serial_wct, UNIX_TIMESTAMP(wc.date_wct) AS date_wct, title_wcl, text_wcl, la.name_lang as language, la.serial_lang
        		FROM web_content wc
        		LEFT JOIN web_content_by_language wcl ON wc.serial_wct = wcl.serial_wct
        		LEFT JOIN language la ON wcl.serial_lang = la.serial_lang
        		WHERE la.code_lang = '".$parentLang."'";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
				
				//[Get Children] of this Content
				$sqlChildren = 	"SELECT wc.serial_wct, UNIX_TIMESTAMP(wc.date_wct) AS date_wct, title_wcl, text_wcl, la.name_lang as language, la.serial_lang
        		FROM web_content wc
        		LEFT JOIN web_content_by_language wcl ON wc.serial_wct = wcl.serial_wct
        		LEFT JOIN language la ON wcl.serial_lang = la.serial_lang
        		WHERE la.code_lang <> '".$parentLang."'
        		AND wc.serial_wct = ".$arr[$cont][serial_wct];
				$resultChildren = $this->db -> Execute($sqlChildren);
        		$numChildren = $resultChildren -> RecordCount();
        		if($numChildren > 0){
		            $arrChildren = array();
		            $contChildren=0;
		            do{
        				$arrChildren[$contChildren]=$resultChildren->GetRowAssoc(false);
        				
						$resultChildren->MoveNext();
                		$contChildren++;
		            }while($contChildren<$numChildren);
		            $arr[$cont][children] = $arrChildren;
        		}
		        //End of [Get Children]
        		
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
/***********************************************
    * funcion getAllWebContentsTreePaginated
    * SAME ABOVE FUNCTION USING PAGINATION
    ***********************************************/
    function getAllWebContentsTreePaginated($currentIndex, $limit){
    	//DEFAULT PARENT LANGUAGE
    	$parentLang = 'es';
        $sql = 	"SELECT SQL_CALC_FOUND_ROWS wc.serial_wct, UNIX_TIMESTAMP(wc.date_wct) AS date_wct, title_wcl, la.name_lang as language, la.serial_lang
        		FROM web_content wc
        		LEFT JOIN web_content_by_language wcl ON wc.serial_wct = wcl.serial_wct
        		LEFT JOIN language la ON wcl.serial_lang = la.serial_lang
        		WHERE la.code_lang = '".$parentLang."' ORDER BY wc.serial_wct ASC LIMIT ".$currentIndex.",".$limit;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        
        $sqlPg = 	"SELECT FOUND_ROWS() as total";
        $resultPg = $this->db -> Execute($sqlPg);
        $totalRows = $resultPg->fields[0];
        SmartyPaginate::setTotal($totalRows);
        
        if($num > 0){
            $arr=array();
            $cont=0;
            do{
                $arr[$cont]=$result->GetRowAssoc(false);
				
                //Get a short version of the title and text:
				$arr[$cont][title_wcl] = WebContent::getShortText($arr[$cont][title_wcl],70);
                
				//[Get Children] of this Content
				$sqlChildren = 	"SELECT wc.serial_wct, UNIX_TIMESTAMP(wc.date_wct) AS date_wct, title_wcl, la.name_lang as language, la.serial_lang
        		FROM web_content wc
        		LEFT JOIN web_content_by_language wcl ON wc.serial_wct = wcl.serial_wct
        		LEFT JOIN language la ON wcl.serial_lang = la.serial_lang
        		WHERE la.code_lang <> '".$parentLang."'
        		AND wc.serial_wct = ".$arr[$cont][serial_wct];
				$resultChildren = $this->db -> Execute($sqlChildren);
        		$numChildren = $resultChildren -> RecordCount();
        		if($numChildren > 0){
		            $arrChildren = array();
		            $contChildren=0;
		            do{
        				$arrChildren[$contChildren]=$resultChildren->GetRowAssoc(false);
        				
        				//Get a short version of the title and text:
						$arrChildren[$contChildren][title_wcl] = News::getShortText($arrChildren[$contChildren][title_wcl],70);
        				
						$resultChildren->MoveNext();
                		$contChildren++;
		            }while($contChildren<$numChildren);
		            $arr[$cont][children] = $arrChildren;
        		}
		        //End of [Get Children]
        		
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arr;
    }
    
	/***********************************************
    * funcion getShortText
    * retrieves a short version of the text field
    ***********************************************/
    public static function getShortText($textField,$shortLength){
    	return  strlen($textField)>$shortLength?substr($textField,0,$shortLength).'...':$textField;
    }
	
	///GETTERS
	function getSerial_wct(){
		return $this->serial_wct;
	}
	function getDate_wct(){
		return $this->date_wct;
	}

	///SETTERS	
	function setSerial_wct($serial_wct){
		$this->serial_wct = $serial_wct;
	}
	function setDate_wct($date_wct){
		$this->date_wct = $date_wct;
	}
}
?>