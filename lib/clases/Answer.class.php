<?php
/*
File: Answer.class.php
Author: Santiago Benítez
Creation Date: 12/01/2009 10:50
Last Modified: 27/01/2009 11:07
Modified By: Santiago Benítez
*/

class Answer{				
	var $db;
	var $serial_ans;
	var $serial_cus;
	var $serial_que;
	var $answer_ans;
	var $yesno_ans;
	var $observations_ans;
	
	function __construct($db, $serial_ans=NULL, $serial_cus=NULL, $serial_que=NULL, $answer_ans=NULL, $yesno_ans=NULL, $observations_ans=NULL ){
		$this -> db = $db;
		$this -> serial_ans = $serial_ans;
		$this -> serial_cus = $serial_cus;
		$this -> serial_que = $serial_que;
		$this -> answer_ans = $answer_ans;
		$this -> yesno_ans = $yesno_ans;
		$this -> observations_ans = $observations_ans;
	}
	
	/***********************************************
    * function getData
    * sets data by serial_ans
    ***********************************************/
	function getData(){
		if($this->serial_ans!=NULL){
			$sql = 	"SELECT serial_ans, serial_cus, serial_que, answer_ans, yesno_ans, observations_ans
					FROM answers
					WHERE serial_ans='".$this->serial_ans."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_ans =$result->fields[0];
				$this -> serial_cus =$result->fields[1];
				$this -> serial_que =$result->fields[2];
				$this -> answer_ans = $result->fields[3];
				$this -> yesno_ans = $result->fields[4];
				$this -> observations_ans = $result->fields[5];
			
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	
	/***********************************************
    * funcion getAnswers
    * gets all the Answers of the system
    ***********************************************/
	function getAnswers(){
		$lista=NULL;
		$sql = 	"SELECT serial_ans, serial_cus, serial_que, answer_ans, yesno_ans, observations_ans
				 FROM answers WHERE serial_cus='".$this->serial_cus."'
				 ORDER BY serial_que ASC ";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
    * funcion deleteMyAnswers
    * gets all the Answers of the system
    ***********************************************/
	public static function deleteMyAnswers($db, $serialCus){
		$sql = 	"DELETE FROM answers WHERE serial_cus = '".$serialCus."'";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		return true;
	}
	
	
	
	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO answers (
				serial_ans,
				serial_cus,
				serial_que,
				answer_ans,
				yesno_ans,
				observations_ans)
            VALUES (
                NULL,
				'".$this->serial_cus."',
				'".$this->serial_que."',
				'".$this->answer_ans."',
				'".$this->yesno_ans."',
				'".$this->observations_ans."')";
       // die($sql);
        $result = $this->db->Execute($sql);

    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE answers SET
					serial_cus ='".$this->serial_cus."',
					serial_que ='".$this->serial_que."',
					answer_ans ='".$this->answer_ans."',
					yesno_ans ='".$this->yesno_ans."',
					observations_ans ='".$this->observations_ans."'
				WHERE serial_ans = '".$this->serial_ans."'";
    	if($result){
            return true;
        }else if($result != 0){
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

	/*
	 * getQuestionsAnswers
	 * @param: $db - DB connections
	 * @param: $serial_cus - Customer ID
	 * @returns: An array of all the answer
	 */
	public static function getQuestionsAnswers($db, $serial_cus){
		$sql = 	"SELECT a.serial_ans, a.answer_ans, a.yesno_ans, a.observations_ans, qbl.text_qbl
				 FROM answers a
				 JOIN questions q ON q.serial_que=a.serial_que
				 JOIN questions_by_language qbl ON q.serial_que=qbl.serial_que AND qbl.serial_lang='".$_SESSION['serial_lang']."'
				 WHERE a.serial_cus='".$serial_cus."'
				 ORDER BY qbl.serial_que ASC ";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}	
	}
	
	///GETTERS
	function getSerial_ans(){
		return $this->serial_ans;
	}
	function getSerial_cus(){
		return $this->serial_cus;
	}
	function getSerial_que(){
		return $this->serial_que;
	}
	function getAnswer_ans(){
		return $this->answer_ans;
	}
	function getYesno_ans(){
		return $this->yesno_ans;
	}
	function getObservations_ans(){
		return $this->observations_ans;
	}
	

	///SETTERS	
	function setSerial_ans($serial_ans){
		$this->serial_ans = $serial_ans;
	}
	function setSerial_cus($serial_cus){
		$this->serial_cus = $serial_cus;
	}
	function setSerial_que($serial_que){
		$this->serial_que = $serial_que;
	}
	function setAnswer_ans($answer_ans){
		$this->answer_ans = $answer_ans;
	}
	function setYesno_ans($yesno_ans){
		$this->yesno_ans = $yesno_ans;
	}
	function setObservations_ans($observations_ans){
		$this->observations_ans = $observations_ans;
	}
}
?>